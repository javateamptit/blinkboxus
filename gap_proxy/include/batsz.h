#ifndef __BATSZ_H__
#define __BATSZ_H__

#include "gap_proxy.h"

#define BATSZ_HEADER_COUNT_INDEX 2
#define BATSZ_UNIT_HEADER_LEN 8

#define BATSZ_CLIENT_MSG_HEARTBEAT 			0x00
#define BATSZ_CLIENT_MSG_LOGIN 				0x01
#define BATSZ_CLIENT_MSG_GAP_REQUEST		0x03
#define BATSZ_MSG_TYPE_LOGIN_RESPONSE 		0x02
#define BATSZ_MSG_TYPE_GAP_RESPONSE 		0x04

typedef struct t_BATSZGapRequest
{
	unsigned char 	unitId;
	unsigned int	beginSeq;
	unsigned short 	count;
	int 			sock;
} t_BATSZGapRequest;

typedef struct t_BATSZ_Config
{
	// Connection parameters from config file
	char sessionSubId[32];
	char userName[32];
	char password[32];
	char ip[32];
	int port;
	
	// Data during processing
	int sock;
	pthread_mutex_t mutex;
} t_BATSZ_Config;

int InitializeBATSZ();
void *Thread_BATSZ_Proxy(void *arg);
void *ThreadHandleBATSZClientRequest(void *arg);
int LoadBATSZConfigurations(const char* fileName);
void DisconnectBATSZClient(t_ClientInfo *clientInfo);
void *BATSZ_TalkToTcpServerThread(void *args);
int BATSZ_ReceiveClientMessage(int sock, char *message);
void DisconnectBATSZ_Book();
int BATSZ_BuildAndSendGapLoginMsg();
int BATSZ_BuildAndSendGapHeartbeatMsgToServer();
int BATSZ_BuildAndSendGapRequestMsg(unsigned char unitId, unsigned int beginSeq, unsigned short count);
int BATSZ_BuildAndSendGapResponseToClient(unsigned char unitId, unsigned int beginSeq, unsigned short count, char status);
int BATSZ_BuildAndSendLoginResponseToClient(int sock, char status);
int BATSZ_BuildAndSendGapHeartbeatMsgToClient(int sock);
int BATSZ_RemoveGapRequestFromPool(unsigned char unitId, unsigned short beginSeq, unsigned short count);
int BATSZ_AddGapRequestToPool(int sock, unsigned char unitId, unsigned short beginSeq, unsigned short count);
int BATSZ_AddClientToList(int sock);
#endif
