#ifndef __GAP_PROXY_H__
#define __GAP_PROXY_H__

#include <stdio.h>
#include <sys/time.h>
#include <time.h>
#include <netinet/in.h>
#include <poll.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h> 
#include <sys/socket.h> 
#include <netdb.h>
#include <arpa/inet.h> 
#include <pthread.h>
#include <signal.h>

#define VERSION "20140109"

#define ROOT_CONF_DIR "/home/hbi/blink/config/gap_proxy"

#define TraceLog(LVL, FMT, PARMS...)                              \
  do {                                                 \
		LrcPrintfLogFormat(LVL, FMT, ## PARMS); }  \
  while (0)
  
#define NO_LOG_LEVEL	-1
#define DEBUG_LEVEL		1
#define NOTE_LEVEL		2
#define WARN_LEVEL		3
#define ERROR_LEVEL		4
#define FATAL_LEVEL		5
#define USAGE_LEVEL		6
#define STATS_LEVEL		7

#define DEBUG_STR		"debug: "
#define NOTE_STR		"note: "
#define WARN_STR		"warn: "
#define ERROR_STR		"error: "
#define FATAL_STR		"fatal: "
#define USAGE_STR		"usage: "
#define STATS_STR		"stats: "

#define ERROR 		-1
#define SUCCESS 	0
#define CONNECTED	1
#define DISCONNECTED 0
#define MSG_HEADER_LEN 6
#define STATUS_SOCK_CLOSED -99
#define STATUS_SOCK_TIMEOUT -98

#define MAX_GAP_REQUEST_IN_POOL 100
#define MAX_CLIENT_CONNECTIONS 	30
#define CLIENT_RECV_TIMEOUT		1

#define MAX_BOOK_CONNECTIONS 1
#define BOOK_BATSZ 0

/**********************************************************************************/
typedef struct t_ClientInfo
{
	int sock;
} t_ClientInfo;

typedef struct t_Misc_Config
{
	int batszPort;
} t_Misc_Config;

typedef union t_ShortConverter
{
	short value;
	char c[2];
} t_ShortConverter;

typedef union t_IntConverter
{
	int value;
	char c[4];
} t_IntConverter;

typedef union t_LongConverter
{
	long value;
	char c[8];
} t_LongConverter;

typedef union t_DoubleConverter
{
	double value;
	char c[8];
} t_DoubleConverter;

/**********************************************************************************/
int ProcessUserInput(int argc, char *argv[]);
void PrintHelp();
int LoadMiscConfigurations(const char* fileName);
int LoadConfigurations();
char *GetFullConfigPath(const char *fileName, char *fullConfigPath);
char *GetLocalTimeString(char *timeString);
int ListenToClient(int port, int maxConcurrentConnections);
int Connect2Server(char *ip, int port, const char* serverName);
char *RemoveCharacters(char *buffer, int bufferLen);
int SetSocketRecvTimeout(int socket, int seconds);
void LrcPrintfLogFormat(int level, char *fmt, ...);
void PrintErrStr(int _level, const char *_str, int _errno);
#endif
