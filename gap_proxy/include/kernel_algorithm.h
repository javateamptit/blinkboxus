#ifndef __KERNEL_ALGORITHM_H__
#define __KERNEL_ALGORITHM_H__

typedef struct t_ThreadAlgorithm
{
	char threadDescription[80];				//Meaningful, human understandable
	int cpuPosition;						// 0 <= cpuPosition < maxAvailableCore
	int schedulerType;						//OTHER, FIFO
	int priority;							//priority for FIFO or nice value for OTHER
}t_ThreadAlgorithm;

typedef struct t_KernelConfig
{
	int maxSystemCPU;
	int numberOfThread;	
	t_ThreadAlgorithm threadAlgorithm[100];
} t_KernelConfig;

extern t_KernelConfig KernelConfig;

int ConvertToCpuMask(const int x);
int InitAndLoadKernelAlgorithmConfigFromFile(const char *fileName);
void InitKernelAlgorithmConfig(void);
int SetKernelAlgorithm(const char *threadDescription);

#endif
