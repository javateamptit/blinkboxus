#include <unistd.h>
#include <getopt.h>

#include "gap_proxy.h"
#include "batsz.h"
#include "kernel_algorithm.h"

t_Misc_Config Misc_Config;
unsigned char ConnectionStatus[MAX_BOOK_CONNECTIONS];

/****************************************************************************
- Function name:	
- Input:				
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int main(int argc, char *argv[])
{
	// Process User Input
	ProcessUserInput(argc, argv);
	
	InitAndLoadKernelAlgorithmConfigFromFile("kernel_algorithm");
	SetKernelAlgorithm("[GAP_PROXY_BASE]");	
	
	//--------------------------------------------------------------
	//	Initialize data
	//--------------------------------------------------------------
	int i;
	for (i = 0; i < MAX_BOOK_CONNECTIONS; i++)
	{
		ConnectionStatus[i] = DISCONNECTED;
	}
	
	if (InitializeBATSZ() == ERROR)
	{
		return ERROR;
	}
		
	//--------------------------------------------------------------
	//	Load configuration files
	//--------------------------------------------------------------
	if (LoadConfigurations() == ERROR)
	{
		return ERROR;
	}
	
	pthread_t batsz_proxy_thread_id;
	pthread_create(&batsz_proxy_thread_id, NULL, (void*)&Thread_BATSZ_Proxy, NULL);
	pthread_join(batsz_proxy_thread_id, NULL);
	TraceLog(DEBUG_LEVEL, "Thread for BATSZ proxy has terminated\n");
				
	return 0;
}

/****************************************************************************
- Function name:	ProcessUserInput
- Input:			
					+ argc
					+ argv
- Output:			N/A
- Return:			Success or Failure
- Description:		
- Usage:			The routine will be called by main routine
****************************************************************************/
int ProcessUserInput(int argc, char *argv[])
{
    const char *optString = "hv";
	const struct option long_opt[] = {
		{"help", 0, NULL, 'h'},
		{"version", 0 , NULL, 'v'},		
		{NULL, 0, NULL, 0}
	};
	
	//Parse the commandline Opstring
	int nextOption;

	do
	{
		nextOption = getopt_long(argc, argv, optString, long_opt, NULL);
		
		switch(nextOption)
		{
			case 'h':
				PrintHelp();
				exit(0);
				break;
				
			case 'v':
					TraceLog(DEBUG_LEVEL, "************************************************************\n");
					TraceLog(DEBUG_LEVEL, "Version of Gap Proxy: '%s' \n", VERSION);
					TraceLog(DEBUG_LEVEL, "************************************************************\n");
				exit(0) ;
				
			case -1 :
				//Option done
				break;
				
			default:
				TraceLog(WARN_LEVEL, "Invalid option\n");
				return ERROR;
		}
	} while(nextOption != -1);
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	PrintHelp
- Input:				
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
void PrintHelp()
{
	TraceLog(NO_LOG_LEVEL, "\n");
	TraceLog(USAGE_LEVEL, "-h --help      for help\n");
	TraceLog(USAGE_LEVEL, "-v --version   for show version of Gap Proxy\n\n");
}

/****************************************************************************
- Function name:	
- Input:				
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int LoadConfigurations()
{
	if (LoadMiscConfigurations("misc") == ERROR) return ERROR;
	
	if (LoadBATSZConfigurations("batsz") == ERROR) return ERROR;
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	
- Input:				
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int LoadMiscConfigurations(const char* fileName)
{
	char fullPath[512];
	
	if (GetFullConfigPath(fileName, fullPath) == NULL)
	{
		return ERROR;
	}
	
	FILE* fileDesc;
	
	fileDesc = fopen(fullPath, "r");
	
	if (fileDesc == NULL)
	{
		TraceLog(ERROR_LEVEL, "Could not open configuration file (%s) to read\n", fullPath);
		return ERROR;
	}
	
	char line[1024];
	int lineCount = 0;
	while (fgets(line, 1023, fileDesc) != NULL)
	{	
		RemoveCharacters(line, strlen(line));
		if (!strchr(line, '#') && (strlen(line) != 0))
		{	
			if (strlen(line) > 31)
			{
				TraceLog(ERROR_LEVEL, "Miscellaneous configuration file error, value is too long.\n");
				fclose(fileDesc);
				return ERROR;
			}
			
			lineCount ++;
			
			switch (lineCount)
			{
				case 1:
					Misc_Config.batszPort = atoi(line);	//Port for BATSZ proxy
					break;
				default:
					TraceLog(ERROR_LEVEL, "Miscellaneous configuration file has more number of values than expected (expected 1 value)\n");
					fclose(fileDesc);
					return ERROR;
			}
		}
	}
	
	fclose(fileDesc);
	
	if (lineCount != 1)
	{
		TraceLog(ERROR_LEVEL, "Miscellaneous configuration file has not enough number of values than expected (expected 1 value)\n");
		return ERROR;
	}
	
	return SUCCESS;
}

void _____UTILITIES______(){return;}
/****************************************************************************
- Function name:	
- Input:				
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int SetSocketRecvTimeout(int socket, int seconds)
{
	struct timeval tv;
	tv.tv_sec = seconds;
	tv.tv_usec = 0;
	
	return setsockopt(socket, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv, sizeof(tv));
}

/****************************************************************************
- Function name:	
- Input:				
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
char *RemoveCharacters(char *buffer, int bufferLen)
{
	int i;

	for (i = bufferLen -1 ; i > -1; i--)
	{
		if (buffer[i] <= 32)
		{	
			buffer[i] = 0;
		}
		else
		{
			break;
		}
	}
	
	return buffer;
}

/****************************************************************************
- Function name:	
- Input:				
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
char *GetFullConfigPath(const char *fileName, char *fullConfigPath)
{
	sprintf(fullConfigPath, "%s", ROOT_CONF_DIR);
	
	int len = strlen(fullConfigPath);
	
	if (fullConfigPath[len-1] == '/')
	{
		sprintf(fullConfigPath, "%s%s", ROOT_CONF_DIR, fileName);
	}
	else
	{
		sprintf(fullConfigPath, "%s/%s", ROOT_CONF_DIR, fileName);
	}
	
	return fullConfigPath;
}

/****************************************************************************
- Function name:	
- Input:				
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
char *GetLocalTimeString(char *timeString)
{
	time_t now;

	time(&now);

	struct tm *l_time;
	
	// Convert to local time
	l_time = (struct tm *)localtime(&now);

	// Get time string
	strftime(timeString, 16, "%H:%M:%S", l_time);
	timeString[8] = 0;

	return timeString;
}

/****************************************************************************
- Function name:	Connect2Server
- Input:			+ ipAddress
					+ port
- Output:			N/A
- Return:			Socket descriptor or Failure
- Description:		
- Usage:			N/A
****************************************************************************/
int Connect2Server(char *ip, int port, const char* serverName)
{
	int sock, rc, valopt;
	struct sockaddr_in localAddr, servAddr; 
	struct hostent *h; 
	long arg;
	fd_set myset;
	socklen_t lon;
	
	h = gethostbyname(ip);
	
	if (h == NULL) 
	{ 
		TraceLog(ERROR_LEVEL, "%s: Unknown host '%s'\n", serverName, ip);
		return ERROR; 
	} 
	
	servAddr.sin_family = h->h_addrtype; 
	memcpy((char *) &servAddr.sin_addr.s_addr, h->h_addr_list[0], h->h_length); 
	servAddr.sin_port = htons(port); 
	
	//Create socket
	sock = socket (AF_INET, SOCK_STREAM, 0); 
	
	if (sock < 0) 
	{ 
		PrintErrStr(ERROR_LEVEL, "Inside Connect2Server(), calling socket(): ", errno);
		return ERROR; 
	} 
	
	// Set non-blocking 
	arg = fcntl(sock, F_GETFL, NULL); 
	arg |= O_NONBLOCK; 
	fcntl(sock, F_SETFL, arg);
	
	//Bind to any port number
	localAddr.sin_family = AF_INET; 
	localAddr.sin_addr.s_addr = htonl(INADDR_ANY); 
	localAddr.sin_port = htons(0);

	struct timeval tv;
	tv.tv_sec = 120000;
	tv.tv_usec = 0;
	
	int test = setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv, sizeof(tv));
	
	if (test < 0)
	{
		PrintErrStr(ERROR_LEVEL, "Inside Connect2Server(), calling setsockopt(): ", errno);
		
		return ERROR;
	}
	
	rc = bind (sock, (struct sockaddr *) &localAddr, sizeof(localAddr)); 
	
	if (rc < 0) 
	{ 
		TraceLog(ERROR_LEVEL, "%s: Cannot bind to TCP port %u\n", serverName, port);
		close(sock);
		return ERROR; 
	} 
	
	//Connect to server
	rc = connect(sock, (struct sockaddr *) &servAddr, sizeof(servAddr)); 
	
	if (rc < 0) 
	{ 
		sleep(1);

		if (errno == EINPROGRESS)
		{ 
			tv.tv_sec = 15; 
			tv.tv_usec = 0;
			FD_ZERO(&myset); 
			FD_SET(sock, &myset); 
			
			if (select(sock + 1, NULL, &myset, NULL, &tv) > 0)
			{ 
			   lon = sizeof(int);
			   
			   getsockopt(sock, SOL_SOCKET, SO_ERROR, (void*)(&valopt), &lon); 
			   
			   if (valopt)
			   { 
				  TraceLog(ERROR_LEVEL, "%s: No route to host\n", serverName); 

				  return ERROR; 
			   } 
			} 
			else
			{ 
			   TraceLog(ERROR_LEVEL, "%s: Connection timeout\n", serverName);

			   return ERROR; 
			}
		}
		else
		{ 
			TraceLog(ERROR_LEVEL, "%s: No route to host\n", serverName);

			return ERROR;
		}
	}

	// Set to blocking mode again... 
	arg = fcntl(sock, F_GETFL, NULL); 
	arg &= (~O_NONBLOCK);
	fcntl(sock, F_SETFL, arg);
	
	TraceLog(DEBUG_LEVEL, "%s: Connection established.\n", serverName);
	return sock; 
}

/****************************************************************************
- Function name:	ListenToClient
- Input:			
- Return:		
- Description:		
- Usage:			N/A
****************************************************************************/
int ListenToClient(int port, int maxConcurrentConnections)
{
	// Check valid port
	if (port < 1024 || port > 65535)
	{
		TraceLog(ERROR_LEVEL, "ListenToClient: Invalid port value (%d)\n", port);
		return ERROR;
	}
	
    int serverSocket;
	struct sockaddr_in servAddr;

	// Create socket
	serverSocket = socket(AF_INET, SOCK_STREAM, 0);
	
	if (serverSocket < 0) 
	{
		PrintErrStr(ERROR_LEVEL, "Inside ListenToClient(), calling socket(): ", errno);
		return ERROR;
	}
	
	// Set SO_REUSEADDR on a socket to true (1):
	int optval = 1;
	
	if (setsockopt(serverSocket, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof optval) != SUCCESS)
	{
		TraceLog(ERROR_LEVEL, "Cannot set SO_REUSEADDR for socket (%d)\n", port);
		return ERROR;
	}

	// Bind server port
	servAddr.sin_family = AF_INET;
	servAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servAddr.sin_port = htons(port);
	
	if (bind(serverSocket, (struct sockaddr *) &servAddr, sizeof(servAddr)) < 0)
	{
		PrintErrStr(ERROR_LEVEL, "Inside ListenToClient(), calling bind(): ", errno);
		close(serverSocket);
		return ERROR;
	}

	// We allow at most concurrent clients
	if (listen(serverSocket, maxConcurrentConnections) == -1)
	{
		return ERROR;
	}
	
	return serverSocket;
}

/****************************************************************************
- Function name:	LrcPrintfLogFormat
- Input:
- Output:			N/A
- Return:			N/A
- Description:
- Usage:			N/A
****************************************************************************/
void LrcPrintfLogFormat(int level, char *fmt, ...)
{
	char formatedStr[5120];
	switch (level)
	{
		case DEBUG_LEVEL:
			strcpy(formatedStr, DEBUG_STR);
			break;
		case NOTE_LEVEL:
			strcpy(formatedStr, NOTE_STR);
			break;
		case WARN_LEVEL:
			strcpy(formatedStr, WARN_STR);
			break;
		case ERROR_LEVEL:
			strcpy(formatedStr, ERROR_STR);
			break;
		case FATAL_LEVEL:
			strcpy(formatedStr, FATAL_STR);
			break;
		case USAGE_LEVEL:
			strcpy(formatedStr, USAGE_STR);
			break;
		case STATS_LEVEL:
			strcpy(formatedStr, STATS_STR);
			break;
		default:
			formatedStr[0] = 0;
			break;
	}
	strcat(formatedStr, fmt);
	
	va_list ap;
	va_start (ap, fmt);
	
	// vprintf (fmt, ap);
	vprintf (formatedStr, ap);

	va_end (ap);

	fflush(stdout);
}

/****************************************************************************
- Function name:	PrintErrStr
- Input:			
- Output:		
- Return:		
- Description:		Print on stdout the following:
					level description, then the input string _str, then the 
					string description of errno _errno, and a new line character
					After that, flush stdout
- Usage:			N/A
****************************************************************************/
void PrintErrStr(int _level, const char *_str, int _errno)
{
	char errStr[128];
	strerror_r(_errno, errStr, 128);
	switch (_level)
	{
		case DEBUG_LEVEL:
			printf("%s%s%s\n", DEBUG_STR, _str, errStr);
			break;
		case NOTE_LEVEL:
			printf("%s%s%s\n", NOTE_STR, _str, errStr);
			break;
		case WARN_LEVEL:
			printf("%s%s%s\n", WARN_STR, _str, errStr);
			break;
		case ERROR_LEVEL:
			printf("%s%s%s\n", ERROR_STR, _str, errStr);
			break;
		case FATAL_LEVEL:
			printf("%s%s%s\n", FATAL_STR, _str, errStr);
			break;
		case USAGE_LEVEL:
			printf("%s%s%s\n", USAGE_STR, _str, errStr);
			break;
		case STATS_LEVEL:
			printf("%s%s%s\n", STATS_STR, _str, errStr);
			break;
		default:
			printf("%s%s\n", _str, errStr);
			break;
	}
	
	fflush(stdout);
}
