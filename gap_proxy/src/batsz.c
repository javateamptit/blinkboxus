#include "gap_proxy.h"
#include "batsz.h"
#include "kernel_algorithm.h"

t_BATSZ_Config BATSZ_Config;
t_ClientInfo BATSZClientInfo[MAX_CLIENT_CONNECTIONS];

pthread_mutex_t batszClientMgmtMutex;
t_BATSZGapRequest BATSZGapRequestPool[MAX_GAP_REQUEST_IN_POOL];

extern t_Misc_Config Misc_Config;
extern unsigned char ConnectionStatus[MAX_BOOK_CONNECTIONS];
/****************************************************************************
- Function name:	
- Input:				
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int InitializeBATSZ()
{
	BATSZ_Config.sock = -1;
	
	if (pthread_mutex_init (&batszClientMgmtMutex, NULL) != 0)
	{
		TraceLog(ERROR_LEVEL, "Could not initialize mutex lock for BATSZ\n");
		return ERROR;
	}
	
	if (pthread_mutex_init (&BATSZ_Config.mutex, NULL) != 0)
	{
		TraceLog(ERROR_LEVEL, "Could not initialize mutex lock for BATSZ\n");
		return ERROR;
	}
	
	int i;
	
	for (i = 0; i < MAX_CLIENT_CONNECTIONS; i++)
	{
		BATSZClientInfo[i].sock = -1;
	}
	
	for (i = 0; i < MAX_GAP_REQUEST_IN_POOL; i++)
	{
		BATSZGapRequestPool[i].sock = -1;
		BATSZGapRequestPool[i].unitId = 0;
		BATSZGapRequestPool[i].beginSeq = 0;
		BATSZGapRequestPool[i].count = 0;
	}
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	
- Input:				
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int LoadBATSZConfigurations(const char* fileName)
{
	char fullPath[512];
	
	if (GetFullConfigPath(fileName, fullPath) == NULL)
	{
		return ERROR;
	}
	
	FILE* fileDesc;
	
	fileDesc = fopen(fullPath, "r");
	
	if (fileDesc == NULL)
	{
		TraceLog(ERROR_LEVEL, "Could not open BATSZ configuration file (%s) to read\n", fullPath);
		return ERROR;
	}
	
	char line[1024];
	int lineCount = 0;
	while (fgets(line, 1023, fileDesc) != NULL)
	{	
		RemoveCharacters(line, strlen(line));
		if (!strchr(line, '#') && (strlen(line) != 0))
		{	
			if (strlen(line) > 31)
			{
				TraceLog(ERROR_LEVEL, "BATSZ configuration file error, value is too long.\n");
				fclose(fileDesc);
				return ERROR;
			}
			
			lineCount ++;
			
			switch (lineCount)
			{
				case 1:
					strcpy(BATSZ_Config.ip, line);
					break;
				case 2:
					BATSZ_Config.port = atoi(line);
					break;
				case 3:
					strcpy(BATSZ_Config.sessionSubId, line);
					break;
				case 4:
					strcpy(BATSZ_Config.userName, line);
					break;
				case 5:
					strcpy(BATSZ_Config.password, line);
					break;
				default:
					TraceLog(ERROR_LEVEL, "BATSZ Configuration file has more number of values than expected (expected 5 values)\n");
					fclose(fileDesc);
					return ERROR;
			}
		}
	}
	
	fclose(fileDesc);
	
	if (lineCount != 5)
	{
		TraceLog(ERROR_LEVEL, "BATSZ Configuration file has not enough number of values than expected (expected 5 values)\n");
		return ERROR;
	}
	
	BATSZ_Config.sock = -1;
	return SUCCESS;
}

/****************************************************************************
- Function name:	
- Input:				
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
void *Thread_BATSZ_Proxy(void *arg)
{
	SetKernelAlgorithm("[BATSZ_PROXY_BASE]");
	struct timeval beginTime, endTime;
	gettimeofday (&beginTime, NULL);
	
	//--------------------------------------------------------------
	//	Connect to BATSZ Gap Request server
	//	BATSZ Gap request server will send us heartbeat if there is
	//	no data within 1 second
	//--------------------------------------------------------------
	BATSZ_Config.sock = Connect2Server(BATSZ_Config.ip, BATSZ_Config.port,	"BATSZ Book Retransmission Request Server");
	if (BATSZ_Config.sock == -1)
	{
		TraceLog(ERROR_LEVEL, "Could not make connection to BATSZ Retransmission Request Server at %s:%d\n", BATSZ_Config.ip, BATSZ_Config.port);
		DisconnectBATSZ_Book();
		return NULL;
	}
	
	if (SetSocketRecvTimeout(BATSZ_Config.sock, 3) == -1)	//Set socket receive timeout to 3 seconds
	{
		TraceLog(ERROR_LEVEL, "(BATSZ): Could not set socket receive-timeout for connection with the GPR server\n");
		DisconnectBATSZ_Book();
		return NULL;
	}
	
	pthread_t BATSZ_Thread_ID;
	pthread_create(&BATSZ_Thread_ID, NULL, (void*)&BATSZ_TalkToTcpServerThread, NULL);
	
	// ------------------------------------------------------------
	// Check if BATS Connected within 4 seconds
	// ------------------------------------------------------------
	sleep(1);
	gettimeofday (&endTime, NULL);
	while (((endTime.tv_sec * 1000000 + endTime.tv_usec) - (beginTime.tv_sec * 1000000 + beginTime.tv_usec)) < 4000000)
	{
		if (ConnectionStatus[BOOK_BATSZ] == CONNECTED)
		{
			break;	//connected
		}
		
		sleep(1);
		
		gettimeofday (&endTime, NULL);
	}
	
	if (ConnectionStatus[BOOK_BATSZ] == DISCONNECTED)
	{
		TraceLog(ERROR_LEVEL, "Could not connect to BATSZ within 4 seconds\n");
		DisconnectBATSZ_Book();
		return NULL;
	}
	
	//--------------------------------------------------------------
	//	Listen for connections
	//--------------------------------------------------------------
	int serverSock = ListenToClient(Misc_Config.batszPort, 10);
	if (serverSock == ERROR)
	{
		TraceLog(ERROR_LEVEL, "(BATSZ): Could not create socket connection (port: %d)\n", Misc_Config.batszPort);
		DisconnectBATSZ_Book();
		return NULL;
	}
	
	TraceLog(DEBUG_LEVEL, "Listening for clients on port: %d\n", Misc_Config.batszPort);
	
	int errCount = 0;
	while (1)
	{
		errno = 0;
		
		int sock = accept(serverSock, NULL, NULL);
		
		if (sock == -1)
		{
			errCount++;
			TraceLog(ERROR_LEVEL, "(BATSZ): could not accept client connection\n");
			PrintErrStr(ERROR_LEVEL, "(BATSZ) Calling accept(): ", errno);
			
			if (errCount >= 10) return NULL;
			else continue;
		}
		
		TraceLog(DEBUG_LEVEL, "BATSZ: New client connected (socket id: %d)\n", sock);
		
		if (SetSocketRecvTimeout(sock, CLIENT_RECV_TIMEOUT) == -1)
		{
			TraceLog(ERROR_LEVEL, "(BATSZ): Could not set socket receive-timeout for socket %d, this connection is closed now\n", sock);
			close(sock);
			continue;
		}
		
		int clientIndex = BATSZ_AddClientToList(sock);
		if (clientIndex == ERROR)
		{
			TraceLog(WARN_LEVEL, "(BATSZ): Connection pool is full, allowed only %d connections at a time, the newly connected client's connection will be closed now.\n", MAX_CLIENT_CONNECTIONS);
			close(sock);
			continue;
		}
		
		pthread_t client_Thread_ID;
		pthread_create(&client_Thread_ID, NULL, (void*)&ThreadHandleBATSZClientRequest, (void *)&BATSZClientInfo[clientIndex]);
	}
	
	DisconnectBATSZ_Book();
	return NULL;
}


/****************************************************************************
- Function name:	
- Input:				
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
void *ThreadHandleBATSZClientRequest(void *arg)
{
	SetKernelAlgorithm("[BATSZ_CLIENT_CONNECTION]");
	
	t_ClientInfo *clientInfo = (t_ClientInfo *)arg;
	char msg[256];
	int ret;
	
	while(1)
	{
		/* Msg format:	Offet	Type		Description
					0		binary(1)		MsgType
					1		binary(2)		Size of msg (included MsgType)
					3		.........		Contents
		*/
		if (ConnectionStatus[BOOK_BATSZ] == DISCONNECTED)
		{
			TraceLog(WARN_LEVEL, "BATSZ: Connection to BATSZ was disconnected, so client %d will be disconnected\n", clientInfo->sock);
			DisconnectBATSZClient(clientInfo);
			return NULL;
		}
		
		ret = BATSZ_ReceiveClientMessage(clientInfo->sock, msg);
		
		if (ConnectionStatus[BOOK_BATSZ] == DISCONNECTED)
		{
			TraceLog(WARN_LEVEL, "BATSZ: Connection to BATSZ was disconnected, so client %d will be disconnected\n", clientInfo->sock);
			DisconnectBATSZClient(clientInfo);
			return NULL;
		}
		
		if (ret == SUCCESS)
		{
			unsigned len;
			memcpy(&len, msg, 2);
			if (len == 8)	//Heartbeat
			{
				continue;
			}
			
			char msgType = msg[9];
			if (msgType == BATSZ_CLIENT_MSG_GAP_REQUEST)
			{
				unsigned char unitId = msg[10];
				unsigned int beginSeq;
				unsigned short count;
				memcpy(&beginSeq, &msg[11], 4);
				memcpy(&count, &msg[15], 2);
				
				int ret = BATSZ_AddGapRequestToPool(clientInfo->sock, unitId, beginSeq, count);
				
				if (ret == ERROR)
				{
					TraceLog(WARN_LEVEL, "BATSZ Gap request pool is full\n");
					BATSZ_BuildAndSendGapResponseToClient(unitId, beginSeq, count, '1');
				}
				else
				{
					if (BATSZ_BuildAndSendGapRequestMsg(unitId, beginSeq, count) == ERROR)
					{
						BATSZ_BuildAndSendGapResponseToClient(unitId, beginSeq, count, '2');
						DisconnectBATSZ_Book();
						return NULL;
					}
				}
			}
			else if (msgType == BATSZ_CLIENT_MSG_LOGIN)
			{
				if (ConnectionStatus[BOOK_BATSZ] == CONNECTED)
				{
					BATSZ_BuildAndSendLoginResponseToClient(clientInfo->sock, 'A');	//Accepted
				}
				else
				{
					BATSZ_BuildAndSendLoginResponseToClient(clientInfo->sock, 'S');	//Invalid session, since gap_proxy has not connected to BATSZ
					
					DisconnectBATSZClient(clientInfo);
					return NULL;
				}
			}
			else
			{
				TraceLog(WARN_LEVEL, "Unknown msg (%d) from client %d\n", msgType, clientInfo->sock);
				DisconnectBATSZClient(clientInfo);
				return NULL;
			}
		}
		else if (ret == STATUS_SOCK_CLOSED)
		{
			TraceLog(WARN_LEVEL, "Client %d is disconnected\n", clientInfo->sock);
			DisconnectBATSZClient(clientInfo);
			return NULL;
		}
		else if (ret == STATUS_SOCK_TIMEOUT)
		{
			TraceLog(WARN_LEVEL, "Could not receive heartbeat from client %d, its connection is now closed.\n", clientInfo->sock);
			
			DisconnectBATSZClient(clientInfo);
			return NULL;
		}
	}
	return NULL;
}

/****************************************************************************
- Function name:	BATSZ_TalkToTcpServerThread
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
void *BATSZ_TalkToTcpServerThread(void *args)
{
	SetKernelAlgorithm("[BATSZ_SERVER_CONNECTION]");

	char buffer[100000];
	int offset = 0;
	unsigned short msgSize;
	int retval;
	int shouldReconnect = 0;

BEGIN_CONNECTION:
	
	if (BATSZ_BuildAndSendGapLoginMsg() == ERROR)
	{
		DisconnectBATSZ_Book();
		return NULL;
	}
	
	while (1)
	{
		// Reset index
		offset = 0;
		
		// Get message header
		while (offset < BATSZ_UNIT_HEADER_LEN)
		{
			errno = 0;
			retval = recv(BATSZ_Config.sock, &buffer[offset], BATSZ_UNIT_HEADER_LEN - offset, 0);
			
			if (retval > 0)
			{
				offset += retval;
			}
			else
			{
				if ((retval == -1) && (errno == EAGAIN))
				{
					TraceLog(ERROR_LEVEL, "BATSZ: Cannot receive anything from server (timeout) %s:%d\n", BATSZ_Config.ip, BATSZ_Config.port);
					DisconnectBATSZ_Book();
					goto RECONNECT_BATSZ;
				}
				else if ((retval == 0) && (errno == 0))
				{
					TraceLog(ERROR_LEVEL, "BATSZ: Server was disconnected (%s:%d)\n", BATSZ_Config.ip, BATSZ_Config.port);
					DisconnectBATSZ_Book();
					goto RECONNECT_BATSZ;
				}
				else
				{
					TraceLog(ERROR_LEVEL, "BATSZ: Server connection problem (%s:%d)\n", BATSZ_Config.ip, BATSZ_Config.port);
					PrintErrStr(ERROR_LEVEL, "BATSZ: Receiving header, calling recv(): ", errno);
					DisconnectBATSZ_Book();
					goto RECONNECT_BATSZ;
				}
			}
		}
		
		memcpy(&msgSize, buffer, 2);
		
		if (msgSize > BATSZ_UNIT_HEADER_LEN)
		{
			while (offset < msgSize)
			{
				errno = 0;
				retval = recv(BATSZ_Config.sock, &buffer[offset], msgSize - offset, 0);
				
				if (retval > 0)
				{
					offset += retval;
				}
				else
				{
					if ((retval == -1) && (errno == EAGAIN))
					{
						TraceLog(ERROR_LEVEL, "BATSZ: Cannot receive anything from server (timeout) %s:%d\n", BATSZ_Config.ip, BATSZ_Config.port);
						DisconnectBATSZ_Book();
						goto RECONNECT_BATSZ;
					}
					else if ((retval == 0) && (errno == 0))
					{
						TraceLog(ERROR_LEVEL, "BATSZ: Server was disconnected (%s:%d)\n", BATSZ_Config.ip, BATSZ_Config.port);
						DisconnectBATSZ_Book();
						goto RECONNECT_BATSZ;
					}
					else
					{
						TraceLog(ERROR_LEVEL, "BATSZ: Server connection problem (%s:%d)\n", BATSZ_Config.ip, BATSZ_Config.port);
						PrintErrStr(ERROR_LEVEL, "BATSZ: Receiving header, calling recv(): ", errno);
						DisconnectBATSZ_Book();
						goto RECONNECT_BATSZ;
					}
				}
			}
		}
		else
		{
			if (BATSZ_BuildAndSendGapHeartbeatMsgToServer() == ERROR)
			{
				DisconnectBATSZ_Book();
				goto RECONNECT_BATSZ;
			}
			
			continue;
		}
		
		// Begin parse and handle message received
		
		// Get number of messages
		int numOfMessage = buffer[BATSZ_HEADER_COUNT_INDEX];
		
		int index = BATSZ_UNIT_HEADER_LEN;
		
		while (numOfMessage > 0)
		{
			switch ((unsigned char)buffer[index + 1])
			{
				case BATSZ_MSG_TYPE_LOGIN_RESPONSE:
					if (buffer[index + 2] != 'A')
					{
						if (buffer[index + 2] == 'N')
						{
							TraceLog(WARN_LEVEL, "BATSZ (Login Response): Login failed, not authorized (Invalid Username/Password)\n");
						}
						else if (buffer[index + 2] == 'B')
						{
							TraceLog(WARN_LEVEL, "BATSZ (Login Response): Login failed, session in use\n");
						}
						else if (buffer[index + 2] == 'S')
						{
							TraceLog(WARN_LEVEL, "BATSZ (Login Response): Login failed, invalid session\n");
						}
						else
						{
							TraceLog(WARN_LEVEL, "BATSZ (Login Response): Login failed, unknown reason code: %c (%d)\n", buffer[index + 2], buffer[index + 2]);
						}
						
						DisconnectBATSZ_Book();
						goto RECONNECT_BATSZ;
					}
					else 
					{
						TraceLog(DEBUG_LEVEL, "BATSZ (Login Response): Login accepted.\n");
						ConnectionStatus[BOOK_BATSZ] = CONNECTED;
						shouldReconnect = 1;
					}
					break;
				case BATSZ_MSG_TYPE_GAP_RESPONSE:
					{
						unsigned char unitId = buffer[index + 2];
						unsigned int beginSeq;
						unsigned short count;
						
						memcpy(&beginSeq, &buffer[index+3], 4);
						memcpy(&count, &buffer[index + 7], 2);
						
						BATSZ_BuildAndSendGapResponseToClient(unitId, beginSeq, count, buffer[index + 9]);
						
						if (buffer[index + 9] != 'A')
						{
							TraceLog(WARN_LEVEL, "BATSZ (Gap Response): Rejected, status code = %c\n", buffer[index + 9]);
						}
						else
						{
							TraceLog(DEBUG_LEVEL, "BATSZ (Gap Response): Accepted, unit: %d, fist sequence: %d, count: %d.\n", unitId, beginSeq, count);
						}
					}
					break;
				default:
					TraceLog(ERROR_LEVEL, "(BATSZ): Received unknown msg(%d)\n", buffer[index + 1]);
					DisconnectBATSZ_Book();
					goto RECONNECT_BATSZ;
			}
			
			// Decrease numOfMessage
			numOfMessage--;
			
			// Update index
			index += buffer[index];
		}
	}

RECONNECT_BATSZ:
	if (shouldReconnect == 0)
	{
		return NULL;
	}
	
	TraceLog(WARN_LEVEL, "Going to re-connect to BATSZ after 3 seconds...\n");
	sleep(3);
	
	// ----------------------------------------------------
	//	Reconnect BATSZ Server in case it's disconnected
	// ----------------------------------------------------
	BATSZ_Config.sock = Connect2Server(BATSZ_Config.ip, BATSZ_Config.port,	"BATSZ Book Retransmission Request Server");
	if (BATSZ_Config.sock == -1)
	{
		TraceLog(ERROR_LEVEL, "Could not make connection to BATSZ Retransmission Request Server at %s:%d\n", BATSZ_Config.ip, BATSZ_Config.port);
		DisconnectBATSZ_Book();
		goto RECONNECT_BATSZ;
	}
	
	if (SetSocketRecvTimeout(BATSZ_Config.sock, 3) == -1)	//Set socket receive timeout to 3 seconds
	{
		TraceLog(ERROR_LEVEL, "(BATSZ): Could not set socket receive-timeout for connection with the GPR server\n");
		DisconnectBATSZ_Book();
		goto RECONNECT_BATSZ;
	}
	
	// Everything is OK, we will begin to send logon request
	goto BEGIN_CONNECTION;
}

/****************************************************************************
- Function name:	BATSZ_BuildAndSendGapLoginMsg
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BATSZ_BuildAndSendGapLoginMsg()
{
	t_ShortConverter shortValue;
	
	char msgToSend[30];
	memset(msgToSend, 0, 30);
	
	// Header length
	shortValue.value = 30;
	msgToSend[0] = shortValue.c[0];
	msgToSend[1] = shortValue.c[1];
	
	// Header count = 1, header unit = 0, header sequence = 0
	msgToSend[2] = 1;
	
	// Build login message
	/*
	Length			0	1	Binary
	Message type	1	1	0x01
	SessionSubId	2	4	Alphanumeric
	Username 		6	4	Alphanumeric
	Filter			10	2	Alphanumeric (space filled)
	Password		12	10	Alphanumeric
	*/
	
	// Length
	msgToSend[8] = 22;
	
	// Msg type
	msgToSend[9] = 0x01;
	
	// Session Sub Id
	strncpy(&msgToSend[10], BATSZ_Config.sessionSubId, 4);
	
	// Username
	strncpy(&msgToSend[14], BATSZ_Config.userName, 4);
	
	// Filter (space filled)
	memset(&msgToSend[18], 32, 2);
	
	// Password
	int len = strlen(BATSZ_Config.password);
	strncpy(&msgToSend[20], BATSZ_Config.password, len);
	if (len < 10)
	{
		memset(&msgToSend[20 + len], 32, 10 - len);
	}
	
	pthread_mutex_lock(&BATSZ_Config.mutex);

	int retval = send(BATSZ_Config.sock, msgToSend, 30, 0);
	
	pthread_mutex_unlock(&BATSZ_Config.mutex);
	
	if (retval != 30)
	{
		TraceLog(ERROR_LEVEL, "BATSZ: Cannot send Login message.\n");
		DisconnectBATSZ_Book();
		return ERROR;
	}
	else
	{
		TraceLog(DEBUG_LEVEL, "BATSZ: Successful sent Login message\n");
	}
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	BATSZ_BuildAndSendGapHeartbeatMsgToServer
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BATSZ_BuildAndSendGapHeartbeatMsgToServer()
{
	// Build message and send Heartbeat message to TCP server
	t_ShortConverter shortValue;
	
	char msgToSend[BATSZ_UNIT_HEADER_LEN];
	memset(msgToSend, 0, BATSZ_UNIT_HEADER_LEN);
	
	// Header length
	shortValue.value = BATSZ_UNIT_HEADER_LEN;
	msgToSend[0] = shortValue.c[0];
	msgToSend[1] = shortValue.c[1];
	
	// Header count = 0, header unit = 0, header sequence = 0

	pthread_mutex_lock(&BATSZ_Config.mutex);

	int retval = send(BATSZ_Config.sock, msgToSend, BATSZ_UNIT_HEADER_LEN, 0);
	
	pthread_mutex_unlock(&BATSZ_Config.mutex);
	
	if (retval != BATSZ_UNIT_HEADER_LEN)
	{
		TraceLog(ERROR_LEVEL, "BATSZ: Cannot send Heartbeat message to server\n");
		DisconnectBATSZ_Book();
		return ERROR;
	}
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	BATSZ_BuildAndSendGapHeartbeatMsgToClient
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BATSZ_BuildAndSendGapHeartbeatMsgToClient(int sock)
{
	if (sock == -1) return ERROR;
	
	// Build message and send Heartbeat message to TCP server
	t_ShortConverter shortValue;
	
	char msgToSend[BATSZ_UNIT_HEADER_LEN];
	memset(msgToSend, 0, BATSZ_UNIT_HEADER_LEN);
	
	// Header length
	shortValue.value = BATSZ_UNIT_HEADER_LEN;
	msgToSend[0] = shortValue.c[0];
	msgToSend[1] = shortValue.c[1];
	
	// Header count = 0, header unit = 0, header sequence = 0

	int retval = send(sock, msgToSend, BATSZ_UNIT_HEADER_LEN, 0);
	
	if (retval != BATSZ_UNIT_HEADER_LEN)
	{
		TraceLog(ERROR_LEVEL, "BATSZ: Cannot send Heartbeat message to client %d\n", sock);
		
		return ERROR;
	}
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	BATSZ_BuildAndSendGapResponseToClient
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BATSZ_BuildAndSendGapResponseToClient(unsigned char unitId, unsigned int beginSeq, unsigned short count, char status)
{
	char msgToSend[18];
	memset(msgToSend, 0, 18);
	
	unsigned short pckLen = 18;
	memcpy(msgToSend, &pckLen, 2);
	
	// Header count = 1, header unit = 0, header sequence = 0
	msgToSend[2] = 1;
	
	// msgLen
	msgToSend[8] = 10;
	
	// msgType
	msgToSend[9] = BATSZ_MSG_TYPE_GAP_RESPONSE;
	
	// unitId
	msgToSend[10] = unitId;
	
	// begin-seq requested
	memcpy(&msgToSend[11], &beginSeq, 4);
	
	// count
	memcpy(&msgToSend[15], &count, 2);
	
	// status
	msgToSend[17] = status;
	
	//Remove request from pool and send to client the response
	int sock = BATSZ_RemoveGapRequestFromPool (unitId, beginSeq, count);

	if (sock == -1)
	{
		return ERROR;
	}
	
//	pthread_mutex_lock	
	int retval = send(sock, msgToSend, 18, 0);
//	pthread_mutex_unlock
	
	if (retval != 18)
	{
		TraceLog(ERROR_LEVEL, "BATSZ: Could not send Gap Request Response to client %d\n", sock);
		return ERROR;
	}

	return SUCCESS;
}


/****************************************************************************
- Function name:	BATSZ_BuildAndSendLoginResponseToClient
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BATSZ_BuildAndSendLoginResponseToClient(int sock, char status)
{
	char msgToSend[11];
	memset(msgToSend, 0, 11);
	
	unsigned short pckLen = 11;
	memcpy(msgToSend, &pckLen, 2);
	
	// Header count = 1, header unit = 0, header sequence = 0
	msgToSend[2] = 1;
	
	// msgLen
	msgToSend[8] = 3;
	
	// msgType
	msgToSend[9] = BATSZ_MSG_TYPE_LOGIN_RESPONSE;
	
	// status
	msgToSend[10] = status;
	
	if (sock == -1)
	{
		return ERROR;
	}
	
//	pthread_mutex_lock	
	int retval = send(sock, msgToSend, 11, 0);
//	pthread_mutex_unlock
	
	if (retval != 11)
	{
		TraceLog(ERROR_LEVEL, "BATSZ: Could not send Login Response to client %d\n", sock);
		return ERROR;
	}

	return SUCCESS;
}

/****************************************************************************
- Function name:	BATSZ_BuildAndSendGapRequestMsg
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BATSZ_BuildAndSendGapRequestMsg(unsigned char unitId, unsigned int beginSeq, unsigned short count)
{
	// Build message and send Gap Request message to TCP server
	t_ShortConverter shortValue;
	
	char msgToSend[17];
	memset(msgToSend, 0, 17);
	
	// Header length
	shortValue.value = 17;
	msgToSend[0] = shortValue.c[0];
	msgToSend[1] = shortValue.c[1];
	
	// Header count = 1, header unit = 0, header sequence = 0
	msgToSend[2] = 1;
	
	// Build Gap Request message
	/*
	Length			0	1	Binary
	Message type	1	1	0x01
	Unit			2	1	Binary
	Sequence		3	4	Binary -> Sequence of first message
	Count			7	2	Binary
	*/
	
	// Length
	msgToSend[8] = 9;
	
	// Msg type
	msgToSend[9] = BATSZ_CLIENT_MSG_GAP_REQUEST;
	
	// Unit
	msgToSend[10] = unitId;
	
	// Sequence
	memcpy(&msgToSend[11], &beginSeq, 4);
	
	// Count
	memcpy(&msgToSend[15], &count, 2);
	
	// Send Gap Request message to GRP
	// Try to get lock before send message to TCP server
	pthread_mutex_lock(&BATSZ_Config.mutex);

	int retval = send(BATSZ_Config.sock, msgToSend, 17, 0);
	
	pthread_mutex_unlock(&BATSZ_Config.mutex);
	
	if (retval != 17)
	{
		TraceLog(ERROR_LEVEL, "BATSZ: Cannot send Gap Request message to server\n");
		DisconnectBATSZ_Book();
		return ERROR;
	}
	else
	{
		TraceLog(DEBUG_LEVEL, "BATSZ: Successful sent Gap Request message to server: Unit %d, Sequence %d, Count %d\n",
					unitId, beginSeq, count);
	}
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	
- Input:				
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
void DisconnectBATSZ_Book()
{
	ConnectionStatus[BOOK_BATSZ] = DISCONNECTED;
	
	if (BATSZ_Config.sock > 0)
	{
		close(BATSZ_Config.sock);
		BATSZ_Config.sock = -1;
	}
	
	//Reset gap request pool
	int i;
	for (i = 0; i < MAX_GAP_REQUEST_IN_POOL; i++)
	{
		BATSZGapRequestPool[i].sock = -1;
		BATSZGapRequestPool[i].unitId = 0;
		BATSZGapRequestPool[i].beginSeq = 0;
		BATSZGapRequestPool[i].count = 0;
	}
}

/****************************************************************************
- Function name:	
- Input:				
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
void DisconnectBATSZClient(t_ClientInfo *clientInfo)
{
	pthread_mutex_lock(&batszClientMgmtMutex);
	
	int i;
	for (i = 0; i < MAX_GAP_REQUEST_IN_POOL; i++)
	{
		if (BATSZGapRequestPool[i].sock == clientInfo->sock)
		{
			BATSZGapRequestPool[i].sock = -1;
			BATSZGapRequestPool[i].unitId = 0;
			BATSZGapRequestPool[i].beginSeq = 0;
			BATSZGapRequestPool[i].count = 0;
		}
	}
	
	close(clientInfo->sock);
	clientInfo->sock = -1;
	
	pthread_mutex_unlock(&batszClientMgmtMutex);
}

/****************************************************************************
- Function name:	BATSZ_ReceiveClientMessage
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int BATSZ_ReceiveClientMessage(int sock, char *message)
{
	int receivedBytes;
	int timeoutCounter = 0;
	unsigned short msgSize = 0;
	int len = 0;
	
	while (len < 8)
	{
		receivedBytes = recv(sock, &message[len], 8 - len, 0);
		
		if (receivedBytes > 0)
		{
			timeoutCounter = 0;
			len += receivedBytes;
		}
		else
		{
			if ((receivedBytes == 0) && (errno == 0))
			{
				return STATUS_SOCK_CLOSED;
			}
			else if ((receivedBytes == -1) && (errno = EAGAIN))
			{
				BATSZ_BuildAndSendGapHeartbeatMsgToClient(sock);
				if (++timeoutCounter > 2)
				{
					return STATUS_SOCK_TIMEOUT;
				}
			}
			else
			{
				TraceLog(WARN_LEVEL, "Could not receive anything from client %d\n", sock);
				return STATUS_SOCK_CLOSED;
			}
		}
	}
	
	// Get message body length
	memcpy(&msgSize, message, 2);

	// Try to receive message body
	while (len < msgSize)
	{
		receivedBytes = recv(sock, &message[len], msgSize - len, 0);
		
		if (receivedBytes > 0)
		{
			timeoutCounter = 0;
			len += receivedBytes;
		}
		else
		{
			if ((receivedBytes == 0) && (errno == 0))
			{
				return STATUS_SOCK_CLOSED;
			}
			else if ((receivedBytes == -1) && (errno = EAGAIN))
			{
				if (++timeoutCounter > 2)
				{
					return STATUS_SOCK_TIMEOUT;
				}
			}
			else
			{
				TraceLog(WARN_LEVEL, "Could not receive anything from client %d \n", sock);
				return STATUS_SOCK_CLOSED;
			}
		}
	}

	return SUCCESS;
}

/****************************************************************************
- Function name:	BATSZ_AddClientToList
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int BATSZ_AddClientToList(int sock)
{
	register unsigned int i;
	
	pthread_mutex_lock(&batszClientMgmtMutex);
	for (i = 0; i < MAX_CLIENT_CONNECTIONS; i++)
	{
		if (BATSZClientInfo[i].sock == -1)
		{
			BATSZClientInfo[i].sock = sock;
			pthread_mutex_unlock(&batszClientMgmtMutex);
			return i;
		}
	}
	
	pthread_mutex_unlock(&batszClientMgmtMutex);
	return ERROR;
}

/****************************************************************************
- Function name:	BATSZ_AddGapRequestToPool
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int BATSZ_AddGapRequestToPool(int sock, unsigned char unitId, unsigned short beginSeq, unsigned short count)
{
	register unsigned int i;
	
	pthread_mutex_lock(&batszClientMgmtMutex);
	for (i = 0; i < MAX_GAP_REQUEST_IN_POOL; i++)
	{
		if (BATSZGapRequestPool[i].sock == -1)
		{
			BATSZGapRequestPool[i].sock = sock;
			BATSZGapRequestPool[i].unitId = unitId;
			BATSZGapRequestPool[i].beginSeq = beginSeq;
			BATSZGapRequestPool[i].count = count;
			pthread_mutex_unlock(&batszClientMgmtMutex);
			return SUCCESS;
		}
	}

	pthread_mutex_unlock(&batszClientMgmtMutex);
	return ERROR;
}

/****************************************************************************
- Function name:	BATSZ_RemoveGapRequestFromPool
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int BATSZ_RemoveGapRequestFromPool(unsigned char unitId, unsigned short beginSeq, unsigned short count)
{
	register unsigned int i;
	int sock = -1;
	
	pthread_mutex_lock(&batszClientMgmtMutex);
	for (i = 0; i < MAX_GAP_REQUEST_IN_POOL; i++)
	{
		if ((BATSZGapRequestPool[i].unitId == unitId) &&
			(BATSZGapRequestPool[i].beginSeq == beginSeq) &&
			(BATSZGapRequestPool[i].count == count))
		{
			sock = BATSZGapRequestPool[i].sock;
			BATSZGapRequestPool[i].sock = -1;
			BATSZGapRequestPool[i].unitId = 0;
			BATSZGapRequestPool[i].beginSeq = 0;
			BATSZGapRequestPool[i].count = 0;
			break;
		}
	}
	pthread_mutex_unlock(&batszClientMgmtMutex);
	
	return sock;
}
