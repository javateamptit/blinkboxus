#include "ouch_42_protocol.h"

t_OrderIdMgmt OrderIdMgmt;

static long timeStamp()
{
	struct timeval tv;
	gettimeofday(&tv, NULL);
	struct tm local;
	localtime_r(&tv.tv_sec, &local);
			
	long l1 = local.tm_hour * 3600 + local.tm_min * 60 + local.tm_sec;
	long l2 = l1 * 1000000000;
	long l3 = l2 + (tv.tv_usec * 1000);
	return l3;
}

unsigned long NEXT_SEQUENCE_NUMBER(t_ClientConnection *client)
{
	unsigned long res = 0;
	pthread_mutex_lock(&client->userInfo->OutgoingLock);
	client->userInfo->OutgoingSequenceNumber++;
	res = client->userInfo->OutgoingSequenceNumber;
	pthread_mutex_unlock(&client->userInfo->OutgoingLock);
	return res;
}

int BuildAndSend_OUCH_42_HeartBeat(t_ClientConnection* client)
{
	t_ShortConverter shortUnion;
	char message[3];
	memset(message, 0, 3);
	
	shortUnion.value = 1;
	message[0] = shortUnion.c[1];
	message[1] = shortUnion.c[0];

	message[2] = 'H';

	return SendPacket_OUCH_42(client->socket, &client->lock, message, 3);
}

int BuildAndSend_OUCH_42_LogonResponse(t_ClientConnection* client)
{
	t_ShortConverter shortUnion;

	char message[33];
	memset(message, ' ', 33);
	
	//Packet Length
	shortUnion.value = 33 - 2;
	message[0] = shortUnion.c[1];
	message[1] = shortUnion.c[0];

	//Packet Type
	message[2] = 'A';
	
	// Session
	memcpy(&message[3], "000006608B", 10);
	
	//Sequence Number
	__iToStrWithLeftPad(client->userInfo->IncomingSequenceNumber, ' ', 20, &message[13]);
	

	return SendPacket_OUCH_42(client->socket, &client->lock, message, 33);
}

int BuildAndSend_OUCH_42_ACK(t_ClientConnection* client, t_OrderEntry *orderEntry)
{
	t_ShortConverter shortUnion;
	t_IntConverter intUnion;
	t_LongConverter longUnion;
	
	char message[69];
	memset(message, ' ', 69); 
	
	// packet length
	shortUnion.value = 69 - 2;
	message[0] = shortUnion.c[1];
	message[1] = shortUnion.c[0];
	
	// packet type
	message[2] = 'S';
	
	// Message Type
	message[3] = 'A';
	
	// Timestamp
	longUnion.value = timeStamp();
	message[4] = longUnion.c[7];
	message[5] = longUnion.c[6];
	message[6] = longUnion.c[5];
	message[7] = longUnion.c[4];
	message[8] = longUnion.c[3];
	message[9] = longUnion.c[2];
	message[10] = longUnion.c[1];
	message[11] = longUnion.c[0];
	
	//Order Token
	memcpy(&message[12], orderEntry->clOrdID, 14);
	
	//side
	message[26] = orderEntry->side;
	
	//share
	intUnion.value = orderEntry->quantity;
	message[27] = intUnion.c[3];
	message[28] = intUnion.c[2];
	message[29] = intUnion.c[1];
	message[30] = intUnion.c[0];
	
	//symbol
	memcpy(&message[31], orderEntry->symbol, 8);
	
	//price
	intUnion.value = orderEntry->price;
	message[39] = intUnion.c[3];
	message[40] = intUnion.c[2];
	message[41] = intUnion.c[1];
	message[42] = intUnion.c[0];
	
	//Time in Force
	intUnion.value = orderEntry->tif;
	message[43] = intUnion.c[3];
	message[44] = intUnion.c[2];
	message[45] = intUnion.c[1];
	message[46] = intUnion.c[0];
	
	// Firm
	memcpy(&message[47], "HBIF", 4);
	
	// Display
	message[51] = 'N';
	
	//Order Reference Number
	longUnion.value = __sync_add_and_fetch(&client->orderID, 1);;
	message[52] = longUnion.c[7];
	message[53] = longUnion.c[6];
	message[54] = longUnion.c[5];
	message[55] = longUnion.c[4];
	message[56] = longUnion.c[3];
	message[57] = longUnion.c[2];
	message[58] = longUnion.c[1];
	message[59] = longUnion.c[0];
	
	// Capacity
	message[60] = 'P';
	
	// Intermarket Sweep Eligibility
	message[61] = 'Y';
	
	// Minimum Quantity
	intUnion.value = 0;
	message[62] = intUnion.c[3];
	message[63] = intUnion.c[2];
	message[64] = intUnion.c[1];
	message[65] = intUnion.c[0];
	
	// Cross Type 
	message[66]= 'N';
	
	//Order State
	message[67] = orderEntry->orderState;
	
	//BBO Weight indicator
	message[68] = '1';
	
	return SendPacket_OUCH_42(client->socket, &client->lock, message, 69);

}
	
int BuildAndSend_OUCH_42_FILL(t_ClientConnection *client, t_OrderEntry *orderEntry)
{
	t_ShortConverter shortUnion;
	t_IntConverter intUnion;
	t_LongConverter longUnion;
	
	char message[43];
	memset(message, ' ', 43); 
	
	// Packet length
	shortUnion.value = 43 - 2;
	message[0] = shortUnion.c[1];
	message[1] = shortUnion.c[0];
	
	// Packet type
	message[2] = 'S';
	
	// Message Type
	message[3] = 'E';
	
	// Timestamp
	longUnion.value = timeStamp();
	message[4] = longUnion.c[7];
	message[5] = longUnion.c[6];
	message[6] = longUnion.c[5];
	message[7] = longUnion.c[4];
	message[8] = longUnion.c[3];
	message[9] = longUnion.c[2];
	message[10] = longUnion.c[1];
	message[11] = longUnion.c[0];
	
	// Order Token
	memcpy(&message[12], orderEntry->clOrdID, 14);
	
	// Executed Shares
	intUnion.value = orderEntry->quantity;
	message[26] = intUnion.c[3];
	message[27] = intUnion.c[2];
	message[28] = intUnion.c[1];
	message[29] = intUnion.c[0];
	
	// Execution Price
	intUnion.value = orderEntry->price;
	message[30] = intUnion.c[3];
	message[31] = intUnion.c[2];
	message[32] = intUnion.c[1];
	message[33] = intUnion.c[0];
	
	// Liquidity Flag
	message[34] = 'R';
	
	//Match Number
	longUnion.value = __sync_add_and_fetch(&client->executionNumber, 1);
	message[35] = longUnion.c[7];
	message[36] = longUnion.c[6];
	message[37] = longUnion.c[5];
	message[38] = longUnion.c[4];
	message[39] = longUnion.c[3];
	message[40] = longUnion.c[2];
	message[41] = longUnion.c[1];
	message[42] = longUnion.c[0];
	
	return SendPacket_OUCH_42(client->socket, &client->lock, message, 43);
}

int BuildAndSend_OUCH_42_Canceled(t_ClientConnection* client, t_OrderEntry *orderEntry)
{
	t_ShortConverter shortUnion;
	t_IntConverter intUnion;
	t_LongConverter longUnion;
	
	char message[31];
	memset(message, ' ', 31); 
	
	// Packet length
	shortUnion.value = 31 - 2;
	message[0] = shortUnion.c[1];
	message[1] = shortUnion.c[0];
	
	// Packet type
	message[2] = 'S';
	
	// Message Type
	message[3] = 'C';
	
	// Timestamp
	longUnion.value = timeStamp();
	message[4] = longUnion.c[7];
	message[5] = longUnion.c[6];
	message[6] = longUnion.c[5];
	message[7] = longUnion.c[4];
	message[8] = longUnion.c[3];
	message[9] = longUnion.c[2];
	message[10] = longUnion.c[1];
	message[11] = longUnion.c[0];
	
	// Order Token
	memcpy(&message[12], orderEntry->clOrdID, 14);
	
	// Decrement Shares
	intUnion.value = orderEntry->quantity;
	message[26] = intUnion.c[3];
	message[27] = intUnion.c[2];
	message[28] = intUnion.c[1];
	message[29] = intUnion.c[0];
	
	// Reason
	message[30] = 'I';
	
	return SendPacket_OUCH_42(client->socket, &client->lock, message, 31);

}

int BuildAndSend_OUCH_42_REJECT(t_ClientConnection* client, t_OrderEntry *orderEntry, char reason)
{
	t_ShortConverter shortUnion;
	t_LongConverter longUnion;
	char message[27];
	memset(message, ' ', 27); 
	
	// Packet length
	shortUnion.value = 27 - 2;
	message[0] = shortUnion.c[1];
	message[1] = shortUnion.c[0];
	
	// Packet type
	message[2] = 'S';
	
	// Message Type
	message[3] = 'J';
	
	// Timestamp
	longUnion.value = timeStamp();
	message[4] = longUnion.c[7];
	message[5] = longUnion.c[6];
	message[6] = longUnion.c[5];
	message[7] = longUnion.c[4];
	message[8] = longUnion.c[3];
	message[9] = longUnion.c[2];
	message[10] = longUnion.c[1];
	message[11] = longUnion.c[0];
	
	// Order Token
	memcpy(&message[12], orderEntry->clOrdID, 14);
	
	// Reason
	message[26] = reason;
	
	return SendPacket_OUCH_42(client->socket, &client->lock, message, 27);
}

int SendPacket_OUCH_42(int sockfd, pthread_spinlock_t *lock, char* buff, int len)
{
	pthread_spin_lock(lock);
	write(sockfd, buff, len);
	pthread_spin_unlock(lock);
	
	return SUCCESS;
}

int ReceiveMsg_OUCH_42(int sockfd, t_DataBlock* dataBlock)
{
	int numReceivedBytes = 0;
	int recvBytes = 0;

	// receive header bytes
	while(numReceivedBytes < 2)
	{
		errno = 0;
		recvBytes = recv(sockfd, &dataBlock->msgContent[numReceivedBytes], 2 - numReceivedBytes, 0);

		if(recvBytes > 0)
		{
			numReceivedBytes += recvBytes;
		}
		else if ( (recvBytes == -1) && (errno == EAGAIN) ) //Timeout
		{
			TraceLog(ERROR_LEVEL, "NYX CCG: Socket receive-timeout\n");
			return ERROR;
		}
		else if ( (recvBytes == 0) && (errno == 0) )	//Server Disconnected
		{
			TraceLog(ERROR_LEVEL, "NYX CCG: Connection is terminated\n");
			return ERROR;
		}
		else
		{
			TraceLog(ERROR_LEVEL, "NYX CCG: recv() returned %d, errno: %d\n", numReceivedBytes, errno);
			return ERROR;
		}
	}

	//receive body message bytes
	t_ShortConverter msgLen;
	msgLen.c[0] = dataBlock->msgContent[1];
	msgLen.c[1] = dataBlock->msgContent[0];
	dataBlock->msgLen = (unsigned short)msgLen.value + 2;

	while(numReceivedBytes < dataBlock->msgLen)
	{
		errno = 0;
		recvBytes = recv(sockfd, &dataBlock->msgContent[numReceivedBytes], dataBlock->msgLen - numReceivedBytes, 0);

		if(recvBytes > 0)
		{
			numReceivedBytes += recvBytes;
		}
		else if ( (recvBytes == -1) && (errno == EAGAIN) ) //Timeout
		{
			TraceLog(ERROR_LEVEL, "NYX CCG: Socket receive-timeout.\n");
			return ERROR;
		}
		else if ( (recvBytes == 0) && (errno == 0) )	//Server Disconnected
		{
			TraceLog(ERROR_LEVEL, "NYX CCG: Connection is terminated\n");
			return ERROR;
		}
		else
		{
			TraceLog(ERROR_LEVEL, "NYX CCG: recv() returned %d, errno: %d\n", numReceivedBytes, errno);
			return ERROR;
		}
	}

	return SUCCESS;
}
