#include <math.h>
#include "ouch_42_proc.h"
#include "order_token_queue.h"

extern t_OrderIdMgmt OrderIdMgmt;

double OUCH_42_PRICE_PORTION[5];

t_UserInfo UserAccounts[] =
{
	{"BLINKB1", 0, 0, PTHREAD_MUTEX_INITIALIZER, 0},
	{"BLINKB2", 0, 0, PTHREAD_MUTEX_INITIALIZER, 0},
	{"BLINKB2", 0, 0, PTHREAD_MUTEX_INITIALIZER, 0},
	{"BLINKB2", 0, 0, PTHREAD_MUTEX_INITIALIZER, 0},
	{"BLINKB2", 0, 0, PTHREAD_MUTEX_INITIALIZER, 0},
	{"BLINKB2", 0, 0, PTHREAD_MUTEX_INITIALIZER, 0},
	{"BLINKB2", 0, 0, PTHREAD_MUTEX_INITIALIZER, 0},
	{"BLINKB2", 0, 0, PTHREAD_MUTEX_INITIALIZER, 0},
	{"BLINKB2", 0, 0, PTHREAD_MUTEX_INITIALIZER, 0},
	{"BLINKB2", 0, 0, PTHREAD_MUTEX_INITIALIZER, 0},
	{"BLINKB2", 0, 0, PTHREAD_MUTEX_INITIALIZER, 0},
	{"BLINKB2", 0, 0, PTHREAD_MUTEX_INITIALIZER, 0},
	{"BLINKB2", 0, 0, PTHREAD_MUTEX_INITIALIZER, 0},
};
void InitUserAccountLock()
{
	OUCH_42_PRICE_PORTION[0] = 1.0;
	OUCH_42_PRICE_PORTION[1] = 0.1;
	OUCH_42_PRICE_PORTION[2] = 0.01;
	OUCH_42_PRICE_PORTION[3] = 0.001;
	OUCH_42_PRICE_PORTION[4] = 0.0001;
	
	int i;
	for(i=0;i<sizeof(UserAccounts)/sizeof(t_UserInfo);i++)
	{
		pthread_mutex_init(&UserAccounts[i].OutgoingLock, NULL);
	}
}

int OUCH_42_LogonProc(t_ClientConnection* client, t_DataBlock* dataBlock)
{
	client->userInfo = &UserAccounts[client->clientIndex];
	client->userInfo->OutgoingSequenceNumber = -1;
	char seq[21] = "\0";
	memcpy(seq, &dataBlock->msgContent[29], 20);
	client->userInfo->IncomingSequenceNumber = atol(seq);
	BuildAndSend_OUCH_42_LogonResponse(client);
	client->userInfo->CurrentOrderToken = 0;
	return SUCCESS;
}

extern double FillFraction;
int OUCH_42_NewOrderProc(t_ClientConnection* client, t_DataBlock* dataBlock)
{	
	t_OrderEntry orderEntry;
	memset(&orderEntry, 0, sizeof(t_OrderEntry));
	
	memcpy(orderEntry.clOrdID, &dataBlock->msgContent[4], 14);
	
	orderEntry.side = dataBlock->msgContent[18];
	orderEntry.quantity = __builtin_bswap32(*(unsigned int*)(&dataBlock->msgContent[19]));
	
	memcpy(orderEntry.symbol, &dataBlock->msgContent[23], 8);
	
	orderEntry.price = __builtin_bswap32(*(unsigned int*)(&dataBlock->msgContent[31]));
	
	orderEntry.tif = __builtin_bswap32(*(unsigned int*)(&dataBlock->msgContent[35]));
	
	// printf("debug: New order: symbol(%.8s), clOrdID(%lu), quantity(%d), price(%lf) side(%c) tif(%d)\n", 
			// orderEntry.symbol, orderEntry.clOrdID, 
			// orderEntry.quantity, orderEntry.price * 0.0001, 
			// orderEntry.side, orderEntry.tif);
	
	if(orderEntry.tif != 0 && orderEntry.tif != 9999)
	{
		TraceLog(DEBUG_LEVEL, "Reject by TIF\n");
		//BuildAndSend_OUCH_42_REJECT(client, clOrdID, orderEntry, ' ');
	}
	else if(orderEntry.side != 'B' && orderEntry.side != 'S' && orderEntry.side != 'T' && orderEntry.side != 'E')
	{
		TraceLog(DEBUG_LEVEL, "Reject by Side\n");
		//BuildAndSend_OUCH_42_REJECT(client, orderEntry, ' ');
	}
	else if((orderEntry.quantity < 1) || (orderEntry.quantity > 1000000))
	{
		BuildAndSend_OUCH_42_REJECT(client, &orderEntry, 'Z');
	}
	else
	{
		if((rand() % 100) < lrint(FillFraction * 100.00))
		{
			orderEntry.orderState = 'L';
			BuildAndSend_OUCH_42_ACK(client, &orderEntry);
			
			int numExecute = 1;
				if(orderEntry.quantity > 50)
					numExecute = 1+rand()%3;
				int shareRemain = orderEntry.quantity;
				
				int hasFill = 0;
				while(numExecute > 1)
				{
					numExecute--;
					
					hasFill = 1;
					
					int execShare;
					if(shareRemain == 0)
						break;
					else if(shareRemain == 1)
						execShare = 1;
					else
						execShare = 1+ rand() % (shareRemain/2);
						
					orderEntry.quantity = execShare;
						
					shareRemain -= execShare;
					BuildAndSend_OUCH_42_FILL(client, &orderEntry); //partially fill
					// printf("E:%d, ", execShare);
				}
				
				//last round
				if(shareRemain != 0)
				{
					orderEntry.quantity = shareRemain;
					
					if(rand() % 2 && hasFill == 1)
					{
						BuildAndSend_OUCH_42_Canceled(client, &orderEntry);
						// printf("C:%d\n", shareRemain);
					}
					else
					{
						BuildAndSend_OUCH_42_FILL(client, &orderEntry);
						// printf("E:%d\n", shareRemain);
					}
				}
		}
		else
		{
			orderEntry.orderState = 'D';
			BuildAndSend_OUCH_42_ACK(client, &orderEntry);
		}
	}
	
	return SUCCESS;
}
