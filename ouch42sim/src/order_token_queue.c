#include "order_token_queue.h"

int InitOrderTokenQueue(t_OrderTokenQueue *queue)
{
	queue->curIndex = 0;
	int res = pthread_mutex_init(&queue->mutex, NULL);
	if(res != 0)
	{
		TraceLog(ERROR_LEVEL, "unable to init queue mutex\n");
		return ERROR;
	}
	
	return SUCCESS;
}

int AddOrderToken(t_OrderTokenQueue *queue, t_QueueItem *item)
{
	int res;
	res = pthread_mutex_lock(&queue->mutex);
	if(res != 0)
	{
		TraceLog(ERROR_LEVEL, "unable to lock queue mutex\n");
		return ERROR;
	}
	
	if(queue->curIndex >= QUEUE_BUFFER_SIZE)
	{
		TraceLog(ERROR_LEVEL, "queue buffer full\n");
		pthread_mutex_unlock(&queue->mutex);	
		return ERROR;
	}
	//queue->buffer[queue->curIndex] = item;
	memcpy(&queue->buffer[queue->curIndex], item, sizeof(t_QueueItem));
	//TraceLog(DEBUG_LEVEL, "Add queue symbol %s, side %d\n", item.Symbol, item.Side);
	//TraceLog(DEBUG_LEVEL, "Add queue symbol %s, side %d\n", queue->buffer[queue->curIndex].Symbol, queue->buffer[queue->curIndex].Side);
	queue->curIndex++;
	
	pthread_mutex_unlock(&queue->mutex);	
	return SUCCESS;
}

int SearchAndRemoveToken(t_OrderTokenQueue *queue, t_QueueItem *item)
{
	int res;
	res = pthread_mutex_lock(&queue->mutex);
	if(res != 0)
	{
		TraceLog(ERROR_LEVEL, "unable to lock queue mutex\n");
		return ERROR;
	}
	
	int ret = -2; // not found
	int i;
	for(i=0; i<queue->curIndex; i++)
	{
		if(strncmp(queue->buffer[i].Symbol, item->Symbol, 12) != 0)
		{
			ret = -1;
		}
		else if(queue->buffer[i].orderID != item->orderID)
		{
			ret = -2;
		}
		else
		{
			ret = 0;
			//copy other info
			memcpy(item, &queue->buffer[i], sizeof(t_QueueItem));
			
			//remove item by replace it with final item, no need to move all item after it
			//queue->buffer[i] = queue->buffer[queue->curIndex-1];
			memcpy(&queue->buffer[i], &queue->buffer[queue->curIndex-1], sizeof(t_QueueItem));
			
			queue->curIndex--;
			break;
		}
	}
	
	pthread_mutex_unlock(&queue->mutex);

	return ret;
}

int PickRandomAndRemoveToken(t_OrderTokenQueue *queue, t_QueueItem *item)
{
	int res;
	res = pthread_mutex_lock(&queue->mutex);
	if(res != 0)
	{
		TraceLog(ERROR_LEVEL, "unable to lock queue mutex\n");
		return ERROR;
	}
	
	if(queue->curIndex > 0)
	{
		int bad_luck = 0;
		if(queue->curIndex>1)
			bad_luck = rand()%(queue->curIndex-1);
			
		//*item = queue->buffer[bad_luck];
		memcpy(item, &queue->buffer[bad_luck], sizeof(t_QueueItem));
		
		//move items ahead to remove current item
		/* for(; bad_luck<queue->curIndex-1; bad_luck++)
		{
			queue->buffer[bad_luck] = queue->buffer[bad_luck+1];
		} */
		
		//remove item by replace it with final item, no need to move all item after it
		//queue->buffer[bad_luck] = queue->buffer[queue->curIndex-1];
		memcpy(&queue->buffer[bad_luck], &queue->buffer[queue->curIndex-1], sizeof(t_QueueItem));
		
		queue->curIndex--;
		
		pthread_mutex_unlock(&queue->mutex);
		return SUCCESS;
	}
	else
	{
		pthread_mutex_unlock(&queue->mutex);
		return ERROR;
	}	
}

int PickRandomToken(t_OrderTokenQueue *queue, t_QueueItem *item)
{
	if(queue->curIndex > 0)
	{
		int bad_luck = 0;
		if(queue->curIndex>1)
			bad_luck = rand()%(queue->curIndex-1);
			
		//*item = queue->buffer[bad_luck];
		memcpy(item, &queue->buffer[bad_luck], sizeof(t_QueueItem));
		TraceLog(DEBUG_LEVEL, "random token %d\n", bad_luck);
		return bad_luck;
	}
	return ERROR;
}
int RemoveToken(t_OrderTokenQueue *queue, int index)
{
	int res;
	res = pthread_mutex_lock(&queue->mutex);
	if(res != 0)
	{
		TraceLog(ERROR_LEVEL, "unable to lock queue mutex\n");
		return ERROR;
	}
	
	if(queue->curIndex > 0)
	{
		//remove item by replace it with final item, no need to move all item after it
		//queue->buffer[index] = queue->buffer[queue->curIndex-1];		
		memcpy(&queue->buffer[index], &queue->buffer[queue->curIndex-1], sizeof(t_QueueItem));
		queue->curIndex--;		
		pthread_mutex_unlock(&queue->mutex);
		return SUCCESS;
	}
	else
	{
		pthread_mutex_unlock(&queue->mutex);
		return ERROR;
	}
}
int UpdateToken(t_OrderTokenQueue *queue, int index, t_QueueItem item)
{
	int res;
	res = pthread_mutex_lock(&queue->mutex);
	if(res != 0)
	{
		TraceLog(ERROR_LEVEL, "unable to lock queue mutex\n");
		return ERROR;
	}
	
	if(queue->curIndex > 0 && index < queue->curIndex )
	{
		//queue->buffer[index] = item;
		memcpy(&queue->buffer[index], &item, sizeof(t_QueueItem));
		pthread_mutex_unlock(&queue->mutex);
		return SUCCESS;
	}
	else
	{
		TraceLog(ERROR_LEVEL, "Invalid index\n");
		pthread_mutex_unlock(&queue->mutex);
		return ERROR;
	}
}
int SearchToken(t_OrderTokenQueue *queue, t_QueueItem *item)
{
	int res;
	res = pthread_mutex_lock(&queue->mutex);
	if(res != 0)
	{
		TraceLog(ERROR_LEVEL, "unable to lock queue mutex\n");
		return ERROR;
	}
	
	int i, found=0;
	for(i=0; i<queue->curIndex; i++)
	{
		if(queue->buffer[i].OrderToken == item->OrderToken)
		{
			//*item = queue->buffer[i];
			memcpy(item, &queue->buffer[i], sizeof(t_QueueItem));
			found = 1;
			break;
		}
	}
	
	pthread_mutex_unlock(&queue->mutex);
	if(found)
		return i;
	return ERROR;
}
