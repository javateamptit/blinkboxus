#include "ouch_42_main.h"
#include "global.h"
#include "ouch_42_proc.h"

int serverSocket = -1;
t_ClientConnection CLIENTS[MAX_TS_CONNECTION];
int ServerPort;
double FillFraction;

int main(int argc, char** argv)
{	
	if(argc < 3)
	{
		PrintHelp(argv[0]);
		return 0;
	}
	
	ServerPort = atoi(argv[1]);
	FillFraction = atof(argv[2]);
	
	if(ServerPort <= 0 || FillFraction <= 0)
	{
		PrintHelp(argv[0]);
		return 0;
	}
	
	TraceLog(DEBUG_LEVEL, "Running at port %d with fill fraction %lf\n", ServerPort, FillFraction);
	fflush(stdout);
	
	srand (time(NULL));
	signal(SIGPIPE, SIG_IGN);
	
	int res;
	struct sockaddr_in serverAddress;
	
	serverSocket = socket(AF_INET, SOCK_STREAM, 0);
	if(serverSocket == -1)
	{
		TraceLog(ERROR_LEVEL, "create serverSocket");
		return ERROR;
	}
	
	const int optVal = 1;
	const socklen_t optLen = sizeof(optVal);
	res = setsockopt(serverSocket, SOL_SOCKET, SO_REUSEADDR, (void*) &optVal, optLen);
		
	memset(&serverAddress, 0, sizeof(serverAddress));
	serverAddress.sin_family = AF_INET;
	serverAddress.sin_addr.s_addr = htonl(INADDR_ANY);
	serverAddress.sin_port = htons(ServerPort);
	
	res = bind(serverSocket, (struct sockaddr*)&serverAddress, sizeof(serverAddress));
	if(res == -1)
	{
		TraceLog(ERROR_LEVEL, "bind serverSocket\n");
		return 0;
	}
		
	res = listen(serverSocket, NUM_CONNECT_LISTEN);
	if(res == -1)
	{
		TraceLog(ERROR_LEVEL, "listen serverSocket\n");
		return 0;
	}
	
	pthread_attr_t attDetach;
	pthread_attr_init(&attDetach);
	pthread_attr_setdetachstate(&attDetach, PTHREAD_CREATE_DETACHED);
	
	InitUserAccountLock();
	
	while(1)
	{
		int clientSocket;
		struct sockaddr_in clientAddress;
		socklen_t clientAddrLen = sizeof(clientAddress);
		memset(&clientAddress, 0, clientAddrLen);
		
		clientSocket = accept(serverSocket, (struct sockaddr*)&clientAddress, &clientAddrLen);
				
		if(clientSocket != -1)
		{
			size_t client;
			for(client=0; client<MAX_TS_CONNECTION; client++)//search for unused pool
			{
				if(CLIENTS[client].isAlive == 0)
				{
					CLIENTS[client].clientIndex = client;
					CLIENTS[client].isAlive = 1;
					CLIENTS[client].socket = clientSocket;
					CLIENTS[client].userInfo = NULL;
					CLIENTS[client].executionNumber = 0;
					CLIENTS[client].orderID = 0;
					CLIENTS[client].disableHeartbeat = 0;
					InitOrderTokenQueue(&CLIENTS[client].orderQueue);
					pthread_spin_init(&CLIENTS[client].lock, 0);
					break;
				}
			}
			if(client == MAX_TS_CONNECTION)
			{
				TraceLog(DEBUG_LEVEL, "Pool is full\n");
				close(clientSocket);
				continue;
			}
			
			int iVal = 1;
			setsockopt( clientSocket, IPPROTO_TCP, TCP_NODELAY, (void *)&iVal, sizeof(iVal));
			
			char clientIP[INET_ADDRSTRLEN];
			inet_ntop(AF_INET, &(clientAddress.sin_addr), clientIP, INET_ADDRSTRLEN);
			int port = ntohs(clientAddress.sin_port);
			TraceLog(DEBUG_LEVEL, "client connect: %s:%d, to pool %d, sock %d\n", clientIP, port, client, clientSocket);

			pthread_t tClient;
			pthread_create(&tClient, &attDetach, ClientThread, (void*)client);			
		}
		else
			TraceLog(ERROR_LEVEL, "accept clientSocket\n");
	}
	close(serverSocket);
	
	return 0;
}

void* ClientThread(void* arg)
{
	size_t clientIndex = (size_t)arg;
	t_ClientConnection* client = &CLIENTS[clientIndex];
	
	int res;
	t_DataBlock dataBlock;
	
	pthread_attr_t attDetach;
	pthread_attr_init(&attDetach);
	pthread_attr_setdetachstate(&attDetach, PTHREAD_CREATE_DETACHED);
	
	while(client->isAlive)
	{
		res = ReceiveMsg_OUCH_42(client->socket, &dataBlock);
		if(res != SUCCESS)
		{
			TraceLog(ERROR_LEVEL, "%s\n", __func__);
			client->isAlive = 0;
			break;
		}
						
		time(&client->lastClientBeat);
		
		switch(dataBlock.msgContent[2])
		{
			case 'L':
				TraceLog(DEBUG_LEVEL, "Received login message from client\n");
				if(OUCH_42_LogonProc(client, &dataBlock) == SUCCESS)
				{
					pthread_t tHeartbeat;
					pthread_create(&tHeartbeat, &attDetach, HeartbeatThread, (void*)clientIndex);
				}
				break;
				
			case 'O':
				TraceLog(DEBUG_LEVEL, "Received logout message from client\n");
				goto close;
				break;
				
			case 'R':
				//TraceLog(DEBUG_LEVEL, "Received heartbeat from client\n");
				break;
			
			case 'U':
				if(dataBlock.msgContent[3] == 'O')
				{
					OUCH_42_NewOrderProc(client, &dataBlock);
				}
				else
				{
				}
			break;
				
			default:
				TraceLog(ERROR_LEVEL, "Unknown MsgType(%d)\n", dataBlock.msgContent[0]);
				break;
		}
	}
	
close:
	TraceLog(DEBUG_LEVEL, "Close ClientThread\n");
	close(client->socket);
	return NULL;
}

void* HeartbeatThread(void* arg)
{
	size_t clientIndex = (size_t)arg;
	t_ClientConnection* client = &CLIENTS[clientIndex];	
	
	while(client->isAlive)
	{
		if(!client->disableHeartbeat)
			BuildAndSend_OUCH_42_HeartBeat(client);
		sleep(1);
	}
	
	TraceLog(DEBUG_LEVEL, "Close HeartbeatThread\n");
	return NULL;
}

void PrintHelp(char* progname)
{
	printf("Usage:\t%s <port> <fill fraction> [manual]\n"\
					"\tport:			string type, port to listen on, e.g. 30021\n"\
					"\tfill fraction:		double type, range 0.00 to 1\n"\
					"\tmanual:			manual mode with DAY order (optional)\n"\
					"Note: one simulator can be used for multiple client (connect to same port)\n"\
		, progname);
}
