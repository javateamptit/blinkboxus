#include "utility.h"

char *GetLocalTimeString(char *timeString)
{
	time_t now;

	time(&now);

	struct tm *l_time;

	// Convert to local time
	l_time = (struct tm *)localtime(&now);

	// Get time string
	strftime(timeString, 16, "%H:%M:%S", l_time);
	timeString[8] = 0;

	return timeString;
}

void LrcPrintfLogFormat(int level, char *fmt, ...)
{
	char formatedStr[5120];
#ifdef RELEASE_MODE
	switch (level)
	{
		case DEBUG_LEVEL:
			strcpy(formatedStr, DEBUG_STR);
			break;
		case NOTE_LEVEL:
			strcpy(formatedStr, NOTE_STR);
			break;
		case WARN_LEVEL:
			strcpy(formatedStr, WARN_STR);
			break;
		case ERROR_LEVEL:
			strcpy(formatedStr, ERROR_STR);
			break;
		case FATAL_LEVEL:
			strcpy(formatedStr, FATAL_STR);
			break;
		case USAGE_LEVEL:
			strcpy(formatedStr, USAGE_STR);
			break;
		case STATS_LEVEL:
			strcpy(formatedStr, STATS_STR);
			break;
		default:
			formatedStr[0] = 0;
			break;
	}
#else //DEBUG MODE
	GetLocalTimeString(formatedStr);
	strcat(formatedStr, " ");
	
	switch (level)
	{
		case DEBUG_LEVEL:
			strcat(formatedStr, DEBUG_STR);
			break;
		case NOTE_LEVEL:
			strcat(formatedStr, NOTE_STR);
			break;
		case WARN_LEVEL:
			strcat(formatedStr, WARN_STR);
			break;
		case ERROR_LEVEL:
			strcat(formatedStr, ERROR_STR);
			break;
		case FATAL_LEVEL:
			strcat(formatedStr, FATAL_STR);
			break;
		case USAGE_LEVEL:
			strcat(formatedStr, USAGE_STR);
			break;
		case DEV_LEVEL:
			strcat(formatedStr, DEV_STR);
			break;
		case STATS_LEVEL:
			strcat(formatedStr, STATS_STR);
			break;
		case VERBOSE_LEVEL:
			strcat(formatedStr, VERBOSE_STR);
			break;
		default:
			formatedStr[0] = 0;
			break;
	}
#endif

	strcat(formatedStr, fmt);
	
	va_list ap;
	va_start (ap, fmt);
	
	// vprintf (fmt, ap);
	vprintf (formatedStr, ap);

	va_end (ap);

	fflush(stdout);
}


/****************************************************************************
- Function name:	GetByteNumberBigEndianV1
- Input:			+ buffer
- Output:		N/A
- Return:		Integer number
- Description:	+ The routine hides the following facts
				  - Convert 2 bytes in buffer to short value
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
char GetByteNumberBigEndianV1(const unsigned char *buffer, int *index)
{
	*index += 1;

	return buffer[0];
}
/****************************************************************************
- Function name:	PutByteNumberBigEndianV1
- Input:			+ buffer
- Output:		N/A
- Return:		Integer number
- Description:	+ The routine hides the following facts
				  - Convert 2 bytes in buffer to short value
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
int PutByteNumberBigEndianV1(char num, unsigned char *buffer, int *index)
{
	*index += 1;

	buffer[0] = num;
	return 1;
}
/****************************************************************************
- Function name:	GetShortNumberBigEndianV1
- Input:			+ buffer
- Output:		N/A
- Return:		Integer number
- Description:	+ The routine hides the following facts
				  - Convert 2 bytes in buffer to short value
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
short GetShortNumberBigEndianV1(const unsigned char *buffer, int *index)
{
	t_ShortConverter converter;

	converter.c[1] = buffer[0];
	converter.c[0] = buffer[1];

	*index += 2;

	return converter.value;
}

/****************************************************************************
- Function name:	PutShortNumberBigEndianV1
- Input:			+ buffer
- Output:		N/A
- Return:		Integer number
- Description:	+ The routine hides the following facts
				  - Convert 2 bytes in buffer to short value
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
int PutShortNumberBigEndianV1(short num, unsigned char *buffer, int *index)
{
	t_ShortConverter converter;

	converter.value = num;

	buffer[0] = converter.c[1];
	buffer[1] = converter.c[0];

	*index += 2;

	return 2;
}
/****************************************************************************
- Function name:	GetIntNumberBigEndianV1
- Input:			+ buffer
- Output:		N/A
- Return:		Integer number
- Description:	+ The routine hides the following facts
				  - Convert 4 bytes in buffer to short value
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
int GetIntNumberBigEndianV1(const unsigned char *buffer, int *index)
{
	t_IntConverter converter;
	converter.c[3] = buffer[0];
	converter.c[2] = buffer[1];
	converter.c[1] = buffer[2];
	converter.c[0] = buffer[3];

	*index += 4;

	return converter.value;
}

/****************************************************************************
- Function name:	PutIntNumberBigEndianV1
- Input:			+ buffer
- Output:		N/A
- Return:		Integer number
- Description:	+ The routine hides the following facts
				  - Convert 4 bytes in buffer to short value
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
int PutIntNumberBigEndianV1(int num, unsigned char *buffer, int *index)
{
	t_IntConverter converter;

	converter.value = num;

	/* converter.c[3] = buffer[0];
	converter.c[2] = buffer[1];
	converter.c[1] = buffer[2];
	converter.c[0] = buffer[3]; */
	buffer[0] = converter.c[3];
	buffer[1] = converter.c[2];
	buffer[2] = converter.c[1];
	buffer[3] = converter.c[0];

	*index += 4;

	return 4;
}
/****************************************************************************
- Function name:	GetLongNumberBigEndianV1
- Input:			+ buffer
- Output:		N/A
- Return:		Long number
- Description:	+ The routine hides the following facts
				  - Convert 8 bytes in buffer to long value
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
long GetLongNumberBigEndianV1(const unsigned char *buffer, int *index)
{
	t_LongConverter converter;

	converter.c[0] = buffer[7];
	converter.c[1] = buffer[6];
	converter.c[2] = buffer[5];
	converter.c[3] = buffer[4];
	converter.c[4] = buffer[3];
	converter.c[5] = buffer[2];
	converter.c[6] = buffer[1];
	converter.c[7] = buffer[0];

	*index += 8;

	return converter.value;
}
/****************************************************************************
- Function name:	PutLongNumberBigEndianV1
- Input:			+ buffer
- Output:		N/A
- Return:		Long number
- Description:	+ The routine hides the following facts
				  - Convert 8 bytes in buffer to long value
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
int PutLongNumberBigEndianV1(long num, unsigned char *buffer, int *index)
{
	t_LongConverter converter;

	converter.value = num;

	buffer[7] = converter.c[0];
	buffer[6] = converter.c[1];
	buffer[5] = converter.c[2];
	buffer[4] = converter.c[3];
	buffer[3] = converter.c[4];
	buffer[2] = converter.c[5];
	buffer[1] = converter.c[6];
	buffer[0] = converter.c[7];

	*index += 8;

	return 8;
}
/****************************************************************************
- Function name:	GetDoubleNumberBigEndianV1
- Input:			+ buffer
- Output:		N/A
- Return:		Integer number
- Description:	+ The routine hides the following facts
				  - Convert 8 bytes in buffer to double value
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
double GetDoubleNumberBigEndianV1(const unsigned char *buffer, int *index)
{
	t_DoubleConverter doubleConverter;

	doubleConverter.c[0] = buffer[7];
	doubleConverter.c[1] = buffer[6];
	doubleConverter.c[2] = buffer[5];
	doubleConverter.c[3] = buffer[4];
	doubleConverter.c[4] = buffer[3];
	doubleConverter.c[5] = buffer[2];
	doubleConverter.c[6] = buffer[1];
	doubleConverter.c[7] = buffer[0];

	*index += 8;

	return doubleConverter.value;
}
/****************************************************************************
- Function name:	PutDoubleNumberBigEndianV1
- Input:			+ buffer
- Output:		N/A
- Return:		Integer number
- Description:	+ The routine hides the following facts
				  - Convert 8 bytes in buffer to double value
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
int PutDoubleNumberBigEndianV1(double num,  unsigned char *buffer, int *index)
{
	t_DoubleConverter doubleConverter;

	doubleConverter.value = num;

	buffer[7] = doubleConverter.c[0];
	buffer[6] = doubleConverter.c[1];
	buffer[5] = doubleConverter.c[2];
	buffer[4] = doubleConverter.c[3];
	buffer[3] = doubleConverter.c[4];
	buffer[2] = doubleConverter.c[5];
	buffer[1] = doubleConverter.c[6];
	buffer[0] = doubleConverter.c[7];

	*index += 8;

	return 8;
}


void GetByteArrayBigEndianV1(const unsigned char *buffer, int *index, void* target, size_t len)
{
	*index += len;
	memcpy(target, buffer, len);
}
int PutByteArrayBigEndianV1(unsigned char *bytearray, int len, unsigned char *buffer, int *index)
{
	memcpy(buffer, bytearray, len);
	*index += len;
	return len;
}


long GetTimeFromEpoch()
{
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return (tv.tv_sec * 1000000 + tv.tv_usec);
}
int Lrc_itoa(int value, char *strResult)
{
	char temp[11];
	register int index = 0;
	register int i = 0;

	if (value > 0)
	{
		while (value != 0)
		{
			temp[index] = (48 + value % 10);
			value = value / 10;
			index++;
		}

		for (i = index - 1; i > -1; i--)
		{
			*strResult++ = (char)temp[i];
		}

		*strResult = 0;

		return index;
	}
	else if (value < 0)
	{
		*strResult++ = '-';

		value = -1 * value;

		while (value != 0)
		{
			temp[index] = (char)(48 + value % 10);
			value = value / 10;
			index++;
		}

		for (i = index - 1; i > -1; i--)
		{
			*strResult++ = (char)temp[i];
		}

		*strResult = 0;

		return (index + 1);
	}
	else
	{
		strResult[0] = 48;
		strResult[1] = 0;

		return 1;
	}
}

int Lrc_itoaf(int value, const char pad, const int len, char *strResult)
{
	register int i = 0;
	if (value == 0)
	{
		for (i = 0; i < len; i++)
		{
			strResult[i] = pad;
		}
		strResult[len] = 0;

		return len;
	}

	register int index = 0;

	while (value != 0)
	{
		strResult[len - index - 1] = 48 + value % 10;
		value = value / 10;
		index++;
	}

	for (i = 0; i < len - index; i++)
	{
		strResult[i] = pad;
	}

	strResult[len] = 0;

	return len;
}

int Lrc_itoafl(int value, const char pad, const int len, char * strResult)
{
	char temp[len + 1];

	register int i = 0;
	if (value == 0)
	{
		strResult[0] = '0';

		for (i = 1; i < len; i++)
		{
			strResult[i] = pad;
		}
		strResult[len] = 0;

		return len;
	}

	register int index = 0;

	while (value != 0)
	{
		temp[index] = 48 + value % 10;
		value = value / 10;	
		index++;
	}

	for (i = 0; i < index; i++)
	{
		strResult[i] = temp[index - i - 1];
	}
	
	for (i = index; i < len; i++)
	{
		strResult[i] = pad;
	}

	strResult[len] = 0;
	
	return len;
}

void __iToStrWithLeftPad(int value, const char pad, const int len, char *strResult)
{
	register int i = 0;
	if (value == 0)
	{
		while (i < len) strResult[i++] = pad;
		strResult[len - 1] = '0';
		return;
	}

	register int index = 0;

	while (value != 0)
	{
		strResult[len - index - 1] = 48 + value % 10;
		value = value / 10;
		index++;
	}

	while (i < len-index) strResult[i++] = pad;
	
	return;
}
