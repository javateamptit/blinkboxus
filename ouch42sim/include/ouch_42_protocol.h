#ifndef _OUCH_42_PROTOCOL
#define _OUCH_42_PROTOCOL

#include "global.h"

#include <stdlib.h>
#include <string.h>
#include <sys/errno.h>
#include <sys/socket.h>
#include "utility.h"

typedef struct t_OrderEntry
{
	char clOrdID[21];
	char side;
	char symbol[8];
	int tif;
	int quantity;
	int price;
	int orderState;
}t_OrderEntry;

int BuildAndSend_OUCH_42_HeartBeat(t_ClientConnection* client);
int BuildAndSend_OUCH_42_LogonResponse(t_ClientConnection* client);
int BuildAndSend_OUCH_42_ACK(t_ClientConnection* client, t_OrderEntry *orderEntry);
int BuildAndSend_OUCH_42_REJECT(t_ClientConnection* client, t_OrderEntry *orderEntry, char reason);
int BuildAndSend_OUCH_42_FILL(t_ClientConnection *client, t_OrderEntry *orderEntry);
int BuildAndSend_OUCH_42_Canceled(t_ClientConnection* client, t_OrderEntry *orderEntry);
		
int SendPacket_OUCH_42(int sockfd, pthread_spinlock_t *lock, char* buff, int len);
int ReceiveMsg_OUCH_42(int sockfd, t_DataBlock* dataBlock);

#define TIF_IOC		'3'
#define TIF_DAY		'0'
#define SIDE_BUY	'1'
#define SIDE_SELL	'2'

#define MAX_ORDER 100000

typedef struct t_OrderToken
{
	long clOrderId;
	long orderId;
}t_OrderToken;

typedef struct t_OrderIdMgmt
{
	int count;
	t_OrderToken orderToken[MAX_ORDER];
}t_OrderIdMgmt;

#endif
