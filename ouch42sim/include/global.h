#ifndef _GLOBAL_
#define _GLOBAL_

#define NUM_CONNECT_LISTEN 	3
#define MAX_TS_CONNECTION	10
#define CLIENT_HEARTBEAT_PERIOD_s 15

#include <time.h>
#include <pthread.h>
#include "order_token_queue.h"

typedef struct t_UserInfo
{
	char *Username;
	unsigned long IncomingSequenceNumber;
	unsigned long OutgoingSequenceNumber;
	pthread_mutex_t OutgoingLock;
	unsigned long CurrentOrderToken;
} t_UserInfo;

typedef struct t_StoredMsg
{
	unsigned long SequenceNumber;
	unsigned int MsgLen;
} t_StoredMsg;
#define MAX_STORED_MSG	1024

typedef struct t_ClientConnection
{
	int clientIndex;
	int socket;
	int isAlive;
	int disableHeartbeat;
	time_t lastClientBeat;
	unsigned long executionNumber;
	unsigned long orderID;
	
	t_OrderTokenQueue orderQueue;
	pthread_spinlock_t lock;
	
	t_UserInfo *userInfo;
	
	//t_StoredMsg StoredMsg[MAX_STORED_MSG];
} t_ClientConnection;


#endif
