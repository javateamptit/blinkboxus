#ifndef _OUCH_42
#define _OUCH_42

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <netinet/in.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>
#include "global.h"
#include "utility.h"
#include "ouch_42_protocol.h"

void* ClientThread(void* arg);
void* HeartbeatThread(void* arg);
void* HeartbeatClientThread(void* arg);
void PrintHelp(char* progname);

#endif
