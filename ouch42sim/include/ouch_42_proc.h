#ifndef _OUCH_42_PROC_
#define _OUCH_42_PROC_

#include "global.h"
#include "ouch_42_protocol.h"

int OUCH_42_LogonProc(t_ClientConnection* client, t_DataBlock* dataBlock);
int OUCH_42_NewOrderProc(t_ClientConnection* client, t_DataBlock* dataBlock);

void InitUserAccountLock();

#endif
