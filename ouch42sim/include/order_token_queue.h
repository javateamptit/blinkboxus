#ifndef _ORDER_TOKEN_QUEUE
#define _ORDER_TOKEN_QUEUE

#include <stdio.h>
#include "utility.h"
#include <pthread.h>

#define QUEUE_BUFFER_SIZE		1024*10

typedef struct t_QueueItem
{
	long OrderToken;
	long orderID;
	unsigned int Share;
	unsigned int CumShare;//total share filled, accumulated
	char PriceScale;
	int Price;
	char Side;
	char Symbol[20];
	
} t_QueueItem;

typedef struct t_OrderTokenQueue
{
	t_QueueItem buffer[QUEUE_BUFFER_SIZE];
	unsigned int curIndex;
	
	pthread_mutex_t mutex;
} t_OrderTokenQueue;

int InitOrderTokenQueue(t_OrderTokenQueue *queue);
int AddOrderToken(t_OrderTokenQueue *queue, t_QueueItem *item);
int SearchAndRemoveToken(t_OrderTokenQueue *queue, t_QueueItem *item);
int PickRandomAndRemoveToken(t_OrderTokenQueue *queue, t_QueueItem *item);

int PickRandomToken(t_OrderTokenQueue *queue, t_QueueItem *item);
int RemoveToken(t_OrderTokenQueue *queue, int index);
int UpdateToken(t_OrderTokenQueue *queue, int index, t_QueueItem item);

int SearchToken(t_OrderTokenQueue *queue, t_QueueItem *item);
#endif
