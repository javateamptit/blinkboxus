#ifndef __UTILITY_H__
#define __UTILITY_H__

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <math.h>
#include <errno.h>
#include <sys/time.h>

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#define TraceLog(LVL, FMT, PARMS...)                              \
  do {                                                 \
		LrcPrintfLogFormat(LVL, FMT, ## PARMS); }  \
  while (0)

#define NO_LOG_LEVEL	-1
#define DEBUG_LEVEL		1
#define NOTE_LEVEL		2
#define WARN_LEVEL		3
#define ERROR_LEVEL		4
#define FATAL_LEVEL		5
#define USAGE_LEVEL		6
#define STATS_LEVEL		7
#define VERBOSE_LEVEL	8

#define DEBUG_STR		"debug: "
#define NOTE_STR		"note: "
#define WARN_STR		"warn: "
#define ERROR_STR		"error: "
#define FATAL_STR		"fatal: "
#define USAGE_STR		"usage: "
#define STATS_STR		"stats: "
#define VERBOSE_STR		"verbose: "

#define DEV_LEVEL		0	//development level
#define DEV_STR			"dev: "  

#define ERROR -1
#define SUCCESS 0

typedef union t_ShortConverter
{
	unsigned short value;
	char c[2];
} t_ShortConverter;

typedef union t_IntConverter
{
	unsigned int value;
	char c[4];
} t_IntConverter;

typedef union t_LongConverter
{
	unsigned long value;
	char c[8];
} t_LongConverter;

typedef union t_DoubleConverter
{
	double value;
	char c[8];
} t_DoubleConverter;

typedef struct t_DataBlock
{
	// Length of block data excluded block length
	int blockLen;
	
	// Length of message content
	int msgLen;
	
	/*
	Length of additional information 
	such as timestamp, Entry/Exit indicator...
	*/
	int addLen;
	
	// Content of message will be stored here
	char msgContent[512];
	
	// Content of additional information
	char addContent[100];
} t_DataBlock;

void LrcPrintfLogFormat(int level, char *fmt, ...);
char GetByteNumberBigEndianV1(const unsigned char *buffer, int *index);
int PutByteNumberBigEndianV1(char num, unsigned char *buffer, int *index);
short GetShortNumberBigEndianV1(const unsigned char *buffer, int *index);
int PutShortNumberBigEndianV1(short num, unsigned char *buffer, int *index);
int GetIntNumberBigEndianV1(const unsigned char *buffer, int *index);
int PutIntNumberBigEndianV1(int num, unsigned char *buffer, int *index);
long GetLongNumberBigEndianV1(const unsigned char *buffer, int *index);
int PutLongNumberBigEndianV1(long num, unsigned char *buffer, int *index);
double GetDoubleNumberBigEndianV1(const unsigned char *buffer, int *index);
int PutDoubleNumberBigEndianV1(double num,  unsigned char *buffer, int *index);

void GetByteArrayBigEndianV1(const unsigned char *buffer, int *index, void* target, size_t len);
int PutByteArrayBigEndianV1(unsigned char *bytearray, int len, unsigned char *buffer, int *index);

long GetTimeFromEpoch();

int Lrc_itoaf(int value, const char pad, const int len, char *strResult);
int Lrc_itoa(int value, char *strResult);
int Lrc_itoafl(int value, const char pad, const int len, char * strResult);
void __iToStrWithLeftPad(int value, const char pad, const int len, char *strResult);
#endif
