#!/bin/bash
# Copyright (C) and/or used under license by RGM Advisors, LLC. All Rights Reserved Worldwide.

md_interface=vlan892
#md_rx_interface=vlan892
md_rx_interface=vlan892_b
oe_interface=vlan894

md_block=224.0.251

oe_address=10.8.144.12 # berblinkbox2
unused_block=239.10.1
db_hostname=berblinkbox6
db_name=bbsb_$(whoami)
twt_db_frefix=twt
remote_dir=/home/$(whoami)/bbsb_us

as_address=10.8.136.16 # berblinkbox6
as_listen_port=30010
ts_address=10.8.136.11 # berblinkbox1
ts_listen_port=30002
pm_address=10.8.136.12 # berblinkbox2
pm_listen_port=30002
twt_listen_port=30018
ouch42_fillrate=0.99
oec_fillrate=0.9

vbot_address=10.8.136.12 # berblinkbox2
lr_address=10.8.136.13 # berblinkbox3

# this directory exists on berblinkbox3
md_tm="2014/05/15 8:28"
md_location=/home/larion/logreplay/new_bcf_data

hbi_root=/home/hbi/hbi

sim_type=evbot

ts_wrapper="onload --profile=latency-blink"
kxsim_wrapper=

