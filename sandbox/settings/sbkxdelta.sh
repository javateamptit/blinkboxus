#!/bin/bash
# Copyright (C) and/or used under license by RGM Advisors, LLC. All Rights Reserved Worldwide.

md_interface=vlan1243
md_rx_interface=vlan1243
oe_interface=vlan1243

md_block=224.0.251

oe_address=10.4.34.69 # sbkxdelta4 vlan1243
unused_block=239.10.1
db_hostname=sbkxdelta1
db_name=bbsb_$(whoami)
twt_db_frefix=twt
#remote_dir=/home/$(whoami)/bbsb
remote_dir=/tmp/bbsb_$(whoami)

as_address=10.4.4.18 # sbkxdelta1
as_listen_port=30010
ts_address=10.4.4.19 # sbkxdelta2
ts_listen_port=32000
pm_address=10.4.4.20 # sbkxdelta3
pm_listen_port=31000
twt_listen_port=30018

vbot_address=10.4.4.21 # sbkxdelta4
lr_address=10.4.4.21 # sbkxdelta4
#kxsim_address=10.4.4.21 # sbkxdelta4

# this directory exists on sbkxdelta4
md_tm="2013/09/30 8:15"
md_location=/data-cache/raw/bcf_data

# make this a symlink to my corresponding directory if necessary
hbi_root=/home/$(whoami)/work/trunk/rts/ship

sim_type=evbot
#sim_type=kxsim

ts_wrapper=
kxsim_wrapper=
