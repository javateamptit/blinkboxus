#!/bin/bash
# Copyright (C) and/or used under license by RGM Advisors, LLC. All Rights Reserved Worldwide.

md_interface=vlan1242
md_rx_interface=vlan1242
oe_interface=vlan1242

md_block=224.0.252

oe_address=10.4.34.4 # sbkxgamma4 vlan1242
unused_block=239.10.1
db_hostname=sbkxgamma1
db_name=bbsb_$(whoami)
twt_db_frefix=twt
#remote_dir=/home/$(whoami)/bbsb
remote_dir=/tmp/bbsb_$(whoami)

as_address=10.4.4.14 # sbkxgamma11
as_listen_port=30010
ts_address=10.4.4.15 # sbkxgamma2
ts_listen_port=32000
pm_address=10.4.4.16 # sbkxgamma3
pm_listen_port=31000
twt_listen_port=30018

vbot_address=10.4.4.17 # sbkxgamma4
lr_address=10.4.4.17 # sbkxgamma4
kxsim_address=10.4.4.17 # sbkxgamma4

# this directory exists on sbkxgamma4
md_tm="2014/05/02 8:25"
md_location=/data-cache/raw/bcf_data

# make this a symlink to my corresponding directory if necessary
hbi_root=/home/$(whoami)/work/rts/ship

# sim_type=evbot
sim_type=kxsim
ouch42_fillrate=0.99
oec_fillrate=0.9

ts_wrapper="onload --profile=latency-blink"
kxsim_wrapper="onload"
