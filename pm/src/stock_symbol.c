/****************************************************************************
** Project name:	Equity Arbitrage Application
** File name: 		stock_symbol.c
** Description: 	This file contains function definitions that were declared
					in stock_symbol.h
** Author: 			Sang Nguyen-Minh
** First created on 18 September 2007
** Last updated on 18 September 2007
****************************************************************************/

/****************************************************************************
** Include files and define several variables
****************************************************************************/
#define _GNU_SOURCE
#include "stock_symbol.h"

t_SymbolMgmt symbolMgmt;
pthread_mutex_t updateStockSymbolMutex = PTHREAD_MUTEX_INITIALIZER;
/****************************************************************************
** Function definitions
****************************************************************************/

/****************************************************************************
- Function name:	InitStockSymbolDataStructure
- Input:			N/A
- Output:			+ Stock Symbol data structures will be updated if don't
				have any problems
				+ Update some global variables
- Return:			N/A
- Description:		+ The routine hides the following facts
				  - Initialize default value for data structures
				  - Load all stock symbols from files
				  - Insert all stock symbols to data structures
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A 
****************************************************************************/
int InitStockSymbolDataStructure(void)
{
	/*
	Initialize default value for stock symbol related data structure
	*/
	memset(&symbolMgmt, 0, sizeof(t_SymbolMgmt));
	
		
	// Initialize default value for stockSymbolIndexArray data structure
	int zeroIndex, oneIndex, twoIndex, threeIndex, fourIndex, longIndex;
	
	for (zeroIndex = 0; zeroIndex < MAX_CHARSET_MEMBER_NO; zeroIndex++)
	{
		for (oneIndex = 0; oneIndex < MAX_CHARSET_MEMBER_NO; oneIndex++)
		{
			for (twoIndex = 0; twoIndex < MAX_CHARSET_MEMBER_NO; twoIndex++)
			{
				for (threeIndex = 0; threeIndex < MAX_CHARSET_MEMBER_NO; threeIndex++)
				{
					for (fourIndex = 0; fourIndex < MAX_CHARSET_MEMBER_NO; fourIndex++)
					{
						symbolMgmt.symbolIndexArray[zeroIndex][oneIndex][twoIndex][threeIndex][fourIndex] = -1;
					}
				}
			}			
		}
	}
	
	for (longIndex = 0; longIndex < MAX_STOCK_SYMBOL; longIndex++)
	{
		for (zeroIndex = 0; zeroIndex < MAX_CHARSET_MEMBER_NO; zeroIndex++)
		{
			for (oneIndex = 0; oneIndex < MAX_CHARSET_MEMBER_NO; oneIndex++)
			{
					symbolMgmt.longSymbolIndexArray[longIndex][zeroIndex][oneIndex] = -1;
			}
		}
	}
	
	for (longIndex = 0; longIndex < MAX_STOCK_SYMBOL; longIndex++)
	{
		for (zeroIndex = 0; zeroIndex < MAX_CHARSET_MEMBER_NO; zeroIndex++)
		{
			symbolMgmt.extraLongSymbolIndexArray[longIndex][zeroIndex] = -1;
		}
	}
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	UpdateStockSymbolIndex
- Input:			+ stockSymbol
- Output:		+ Update stock symbol index for stockSymbol in
				symbolIndexArray data structure
- Return:		Success or Failure
- Description:	+ The routine hides the following facts
				  - Check valid stock symbol
				  - Update stockSymbolIndex for stockSymbol
				+ There are preconditions guaranteed to the routine
				  - stock symbol index must be greater than -1
- Usage:			N/A 
****************************************************************************/
int UpdateStockSymbolIndex(const char *stockSymbol)
{
	if (symbolMgmt.nextSymbolIndex == MAX_STOCK_SYMBOL)
	{
		TraceLog(ERROR_LEVEL, "Cannot update Index for stock symbol '%s'\n", stockSymbol);
		return -1;
	}
	
	int i, j, k, n, m, t, u, v;
	int indexOfNewSymbol = -1;

	//character 1: Only in the range A-Z
	if ((stockSymbol[0] > 64) && (stockSymbol[0] < 91))
	{
		i = stockSymbol[0] - INDEX_OF_A_CHAR;	
	}
	else
	{
		//Invalid stock symbol
		return -1;
	}

	//character 2: 0, $, ., A-Z
	if ((stockSymbol[1] > 64) && (stockSymbol[1] < 91))// A-Z
	{
		j = stockSymbol[1] - INDEX_OF_A_CHAR;
	}
	else if ((stockSymbol[1] == 32) || (stockSymbol[1] == 0))
	{
		pthread_mutex_lock(&updateStockSymbolMutex);
		indexOfNewSymbol = symbolMgmt.symbolIndexArray[i][0][0][0][0];
		if (indexOfNewSymbol == -1)
		{
			indexOfNewSymbol = symbolMgmt.nextSymbolIndex++;
			symbolMgmt.symbolIndexArray[i][0][0][0][0] = indexOfNewSymbol;
			memcpy(symbolMgmt.symbolList[indexOfNewSymbol].symbol, stockSymbol, 1);
		}
		pthread_mutex_unlock(&updateStockSymbolMutex);
		return indexOfNewSymbol;
	}
	else if (stockSymbol[1] == '-')
	{
		j = 3;
	}
	else if (stockSymbol[1] == '.')
	{
		j = 4;
	}
	else if (stockSymbol[1] == '$')
	{
		j = 1;
	}
	else if (stockSymbol[1] == '+')
	{
		j = 2;
	}
	else
	{
		//Invalid stock symbol
		return -1;
	}

	//character 3: 0, $, ., A-Z
	if ((stockSymbol[2] > 64) && (stockSymbol[2] < 91))// A-Z
	{
		k = stockSymbol[2] - INDEX_OF_A_CHAR;
	}
	else if ((stockSymbol[2] == 32) || (stockSymbol[2] == 0))
	{
		pthread_mutex_lock(&updateStockSymbolMutex);
		indexOfNewSymbol = symbolMgmt.symbolIndexArray[i][j][0][0][0];
		if (indexOfNewSymbol == -1)
		{
			indexOfNewSymbol = symbolMgmt.nextSymbolIndex++;
			symbolMgmt.symbolIndexArray[i][j][0][0][0] = indexOfNewSymbol;
			memcpy(symbolMgmt.symbolList[indexOfNewSymbol].symbol, stockSymbol, 2);
		}
		pthread_mutex_unlock(&updateStockSymbolMutex);
		return indexOfNewSymbol;
	}
	else if (stockSymbol[2] == '-')
	{
		k = 3;
	}
	else if (stockSymbol[2] == '.')
	{
		k = 4;
	}
	else if (stockSymbol[2] == '$')
	{
		k = 1;
	}
	else if (stockSymbol[2] == '+')
	{
		k = 2;
	}
	else
	{
		//Invalid stock symbol
		return -1;
	}

	//character 4: 0, $, ., A-Z
	if ((stockSymbol[3] > 64) && (stockSymbol[3] < 91))// A-Z
	{
		n = stockSymbol[3] - INDEX_OF_A_CHAR;		
	}
	else if ((stockSymbol[3] == 32) || (stockSymbol[3] == 0))
	{
		pthread_mutex_lock(&updateStockSymbolMutex);
		indexOfNewSymbol = symbolMgmt.symbolIndexArray[i][j][k][0][0];
		if (indexOfNewSymbol == -1)
		{
			indexOfNewSymbol = symbolMgmt.nextSymbolIndex++;
			symbolMgmt.symbolIndexArray[i][j][k][0][0] = indexOfNewSymbol;
			memcpy(symbolMgmt.symbolList[indexOfNewSymbol].symbol, stockSymbol, 3);
		}
		pthread_mutex_unlock(&updateStockSymbolMutex);
		return indexOfNewSymbol;
	}
	else if (stockSymbol[3] == '-')
	{
		n = 3;
	}
	else if (stockSymbol[3] == '.')
	{
		n = 4;
	}
	else if (stockSymbol[3] == '$')
	{
		n = 1;
	}
	else if (stockSymbol[3] == '+')
	{
		n = 2;
	}
	else
	{
		//Invalid stock symbol
		return -1;
	}

	//character 5: 0, $, ., A-Z
	//A-Z
	if ((stockSymbol[4] > 64) && (stockSymbol[4] < 91))
	{
		m = stockSymbol[4] - INDEX_OF_A_CHAR;
	}
	else if ((stockSymbol[4] == 32) || (stockSymbol[4] == 0))
	{
		pthread_mutex_lock(&updateStockSymbolMutex);
		indexOfNewSymbol = symbolMgmt.symbolIndexArray[i][j][k][n][0];
		if (indexOfNewSymbol == -1)
		{
			indexOfNewSymbol = symbolMgmt.nextSymbolIndex++;
			symbolMgmt.symbolIndexArray[i][j][k][n][0] = indexOfNewSymbol;
			memcpy(symbolMgmt.symbolList[indexOfNewSymbol].symbol, stockSymbol, 4);
		}
		pthread_mutex_unlock(&updateStockSymbolMutex);
		return indexOfNewSymbol;
	}
	else if (stockSymbol[4] == '-')
	{
		m = 3;
	}
	else if (stockSymbol[4] == '.')
	{
		m = 4;
	}
	else if (stockSymbol[4] == '$')
	{
		m = 1;
	}
	else if (stockSymbol[4] == '+')
	{
		m = 2;
	}
	else
	{
		//Invalid stock symbol
		return -1;
	}
	
	//character 6 0, $, ., A-Z
	if ((stockSymbol[5] > 64) && (stockSymbol[5] < 91))// A-Z
	{
		t = stockSymbol[5] - INDEX_OF_A_CHAR;		
	}
	else if ((stockSymbol[5] == 32) || (stockSymbol[5] == 0))
	{
		pthread_mutex_lock(&updateStockSymbolMutex);
		indexOfNewSymbol = symbolMgmt.symbolIndexArray[i][j][k][n][m];
		if (indexOfNewSymbol == -1)
		{
			indexOfNewSymbol = symbolMgmt.nextSymbolIndex++;
			symbolMgmt.symbolIndexArray[i][j][k][n][m] = indexOfNewSymbol;
			memcpy(symbolMgmt.symbolList[indexOfNewSymbol].symbol, stockSymbol, 5);
		}
		pthread_mutex_unlock(&updateStockSymbolMutex);
		return indexOfNewSymbol;
	}
	else if (stockSymbol[5] == '-')
	{
		t = 3;
	}
	else if (stockSymbol[5] == '.')
	{
		t = 4;
	}
	else if (stockSymbol[5] == '$')
	{
		t = 1;
	}
	else if (stockSymbol[5] == '+')
	{
		t = 2;
	}
	else
	{
		//Invalid stock symbol
		return -1;
	}
	
	//character 7 0, $, ., A-Z
	if ((stockSymbol[6] > 64) && (stockSymbol[6] < 91))// A-Z
	{
		u = stockSymbol[6] - INDEX_OF_A_CHAR;		
	}
	else if ((stockSymbol[6] == 32) || (stockSymbol[6] == 0))
	{
		pthread_mutex_lock(&updateStockSymbolMutex);
		if(symbolMgmt.symbolIndexArray[i][j][k][n][m] == -1)
		{
			// Insert symbol that included only 5 (in 6 characters).
			indexOfNewSymbol = symbolMgmt.nextSymbolIndex++;	
			symbolMgmt.symbolIndexArray[i][j][k][n][m] = indexOfNewSymbol;				
			memcpy(symbolMgmt.symbolList[indexOfNewSymbol].symbol, stockSymbol, 5);
			
			// Get symbol index in new map table				
			symbolMgmt.longSymbolIndexArray[indexOfNewSymbol][t][0] = symbolMgmt.nextSymbolIndex;
			indexOfNewSymbol = symbolMgmt.nextSymbolIndex++;
			memcpy(symbolMgmt.symbolList[indexOfNewSymbol].symbol, stockSymbol, 6);
		}
		else
		{
			indexOfNewSymbol = symbolMgmt.symbolIndexArray[i][j][k][n][m];
			
			if (symbolMgmt.longSymbolIndexArray[indexOfNewSymbol][t][0] == -1)
			{
				symbolMgmt.longSymbolIndexArray[indexOfNewSymbol][t][0] = symbolMgmt.nextSymbolIndex;
				indexOfNewSymbol = symbolMgmt.nextSymbolIndex++;
				memcpy(symbolMgmt.symbolList[indexOfNewSymbol].symbol, stockSymbol, 6);
			}
			else
			{
				indexOfNewSymbol = symbolMgmt.longSymbolIndexArray[indexOfNewSymbol][t][0];
			}
			
		}
		pthread_mutex_unlock(&updateStockSymbolMutex);
		
		// Successfully inserted a stock symbol	
		return indexOfNewSymbol;
	}
	else if (stockSymbol[6] == '-')
	{
		u = 3;
	}
	else if (stockSymbol[6] == '.')
	{
		u = 4;
	}
	else if (stockSymbol[6] == '$')
	{
		u = 1;
	}
	else if (stockSymbol[6] == '+')
	{
		u = 2;
	}
	else
	{
		//Invalid stock symbol
		return -1;
	}
	
	//character 8 0, $, ., A-Z
	if ((stockSymbol[7] > 64) && (stockSymbol[7] < 91))// A-Z
	{
		v = stockSymbol[7] - INDEX_OF_A_CHAR;		
	}
	else if ((stockSymbol[7] == 32) || (stockSymbol[7] == 0))
	{
		pthread_mutex_lock(&updateStockSymbolMutex);
		if(symbolMgmt.symbolIndexArray[i][j][k][n][m] == -1)
		{
			// Insert symbol that included only 5 (in 6 characters).
			indexOfNewSymbol = symbolMgmt.nextSymbolIndex++;	
			symbolMgmt.symbolIndexArray[i][j][k][n][m] = indexOfNewSymbol;				
			memcpy(symbolMgmt.symbolList[indexOfNewSymbol].symbol, stockSymbol, 5);
			
			// Get symbol index in new map table				
			symbolMgmt.longSymbolIndexArray[indexOfNewSymbol][t][u] = symbolMgmt.nextSymbolIndex;
			indexOfNewSymbol = symbolMgmt.nextSymbolIndex++;
			memcpy(symbolMgmt.symbolList[indexOfNewSymbol].symbol, stockSymbol, 7);
		}
		else
		{
			indexOfNewSymbol = symbolMgmt.symbolIndexArray[i][j][k][n][m];
			
			if (symbolMgmt.longSymbolIndexArray[indexOfNewSymbol][t][u] == -1)
			{
				symbolMgmt.longSymbolIndexArray[indexOfNewSymbol][t][u] = symbolMgmt.nextSymbolIndex;
				indexOfNewSymbol = symbolMgmt.nextSymbolIndex++;
				memcpy(symbolMgmt.symbolList[indexOfNewSymbol].symbol, stockSymbol, 7);
			}
			else
			{
				indexOfNewSymbol = symbolMgmt.longSymbolIndexArray[indexOfNewSymbol][t][u];
			}
			
		}
		pthread_mutex_unlock(&updateStockSymbolMutex);
		// Successfully inserted a stock symbol	
		return indexOfNewSymbol;
	}
	else if (stockSymbol[7] == '-')
	{
		v = 3;
	}
	else if (stockSymbol[7] == '.')
	{
		v = 4;
	}
	else if (stockSymbol[7] == '$')
	{
		v = 1;
	}
	else if (stockSymbol[7] == '+')
	{
		v = 2;
	}
	else
	{
		//Invalid stock symbol
		return -1;
	}
	
	//This is where we update the symbols with 8 characters long
	pthread_mutex_lock(&updateStockSymbolMutex);
	if(symbolMgmt.symbolIndexArray[i][j][k][n][m] == -1)
	{
		// Insert symbol that included only 5
		indexOfNewSymbol = symbolMgmt.nextSymbolIndex++;	
		symbolMgmt.symbolIndexArray[i][j][k][n][m] = indexOfNewSymbol;				
		memcpy(symbolMgmt.symbolList[indexOfNewSymbol].symbol, stockSymbol, 5);
		
		// Update index of symbol with 7 characters.
		// We do not get index of symbol with 7 characters since we know that it's not available for sure
		// --> since symbol with 5 chars does not exist, so how can symbol with 7 chars exist? ;)
		
		symbolMgmt.longSymbolIndexArray[indexOfNewSymbol][t][u] = symbolMgmt.nextSymbolIndex;
		indexOfNewSymbol = symbolMgmt.nextSymbolIndex++;
		memcpy(symbolMgmt.symbolList[indexOfNewSymbol].symbol, stockSymbol, 7);
		
		// Index of symbol in new map table (where the 8 chars long symbol comes!)
		symbolMgmt.extraLongSymbolIndexArray[indexOfNewSymbol][v] = symbolMgmt.nextSymbolIndex;
		indexOfNewSymbol = symbolMgmt.nextSymbolIndex++;
		memcpy(symbolMgmt.symbolList[indexOfNewSymbol].symbol, stockSymbol, 8);
	}
	else
	{
		//This is index of symbol with 5 chars
		indexOfNewSymbol = symbolMgmt.symbolIndexArray[i][j][k][n][m];
		
		// This is index of symbol with 7 chars
		if (symbolMgmt.longSymbolIndexArray[indexOfNewSymbol][t][u] == -1)
		{
			symbolMgmt.longSymbolIndexArray[indexOfNewSymbol][t][u] = symbolMgmt.nextSymbolIndex;
			indexOfNewSymbol = symbolMgmt.nextSymbolIndex++;
			memcpy(symbolMgmt.symbolList[indexOfNewSymbol].symbol, stockSymbol, 7);
		}
		else
		{
			indexOfNewSymbol = symbolMgmt.longSymbolIndexArray[indexOfNewSymbol][t][u];
		}
		
		// This is index of symbol with 8 characters
		if (symbolMgmt.extraLongSymbolIndexArray[indexOfNewSymbol][v] == -1)
		{
			symbolMgmt.extraLongSymbolIndexArray[indexOfNewSymbol][v] = symbolMgmt.nextSymbolIndex;
			indexOfNewSymbol = symbolMgmt.nextSymbolIndex++;
			memcpy(symbolMgmt.symbolList[indexOfNewSymbol].symbol, stockSymbol, 8);
		}
		else
		{
			indexOfNewSymbol = symbolMgmt.extraLongSymbolIndexArray[indexOfNewSymbol][v];
		}
	}
	pthread_mutex_unlock(&updateStockSymbolMutex);
	
	// Successfully inserted a stock symbol	
	return indexOfNewSymbol;
}

/****************************************************************************
- Function name:	GetStockSymbolIndex
- Input:			+ stockSymbol
- Output:		N/A
- Return:		ERROR or stock symbol index
- Description:	+ The routine hides the following facts
				  - Check valid stock symbol
				  - Return stockSymbolIndex for stockSymbol
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A 
****************************************************************************/
int GetStockSymbolIndex(const char *stockSymbol)
{
	int a, b, c, d, e, f, g, h, indexOfLongSymbol;
	//Character 1: in range A-Z, or $
	//65 is the ASCII code of 'A'
	//90 is the ASCII code of 'Z'
	//36 is the ASCII code of '$'

	if ((stockSymbol[0] > 64) && (stockSymbol[0] < 91))
	{
		a = stockSymbol[0] - INDEX_OF_A_CHAR;	
	}
	else
	{
		//Invalid stock symbol
		return -1;
	}
	
	//Character 2: 0, A-Z
	//65 is the ASCII code of 'A'
	//90 is the ASCII code of 'Z'

	//The order of 'if' expressions are important for faster speed
	//This is experience, right?

	if ((stockSymbol[1] > 64) && (stockSymbol[1] < 91))
	{
	    //A-Z
		b = stockSymbol[1] - INDEX_OF_A_CHAR;	
	}
	else if ((stockSymbol[1] == 0) || (stockSymbol[1] == 32))
	{
		//Stock symbol's length is 1, no need to do more
		//This also guarantee fast speed
		//We use the variable a in the array index instead of
		//stockSymbol[0] - INDEX_OF_A_CHAR so as not to calculate anything
		return symbolMgmt.symbolIndexArray[a][0][0][0][0];	
	}
	else if (stockSymbol[1] == '-')
	{
		b = 3;
	}
	else if (stockSymbol[1] == '.')
	{
		b = 4;
	}
	else if (stockSymbol[1] == '$')
	{
		b = 1;
	}
	else if (stockSymbol[1] == '+')
	{
		b = 2;
	}
	else
	{
		return -1;
	}

	//Character 3: 0, ., A-Z
	//65 is the ASCII code of 'A'
	//90 is the ASCII code of 'Z'

	//The order of 'if' expressions are important for faster speed

	if ((stockSymbol[2] > 64) && (stockSymbol[2] < 91))
	{
	    //A-Z
		c = stockSymbol[2] - INDEX_OF_A_CHAR;	
	}

	//Not in range A-Z. We have 244 zeros, and 119 dot
	//Maybe we need to count how many $ and dot at the third position
	//for even better 'if' order

	//244 zeros check first
	//0: NULL; 32: SPACE
	else if ((stockSymbol[2] == 0) || (stockSymbol[2] == 32))
	{	
		//Stock symbol's length is 2, no need to do more
		//This also guarantee fast speed
		//We use the variable a, b in the array index instead of
		//stockSymbol[0] - INDEX_OF_A_CHAR, stockSymbol[1] - INDEX_OF_A_CHAR so as not to calculate anything

		return symbolMgmt.symbolIndexArray[a][b][0][0][0];
	}
	else if (stockSymbol[2] == '-')
	{
		c = 3;
	}
	else if (stockSymbol[2] == '.')
	{
		c = 4;
	}
	else if (stockSymbol[2] == '$')
	{
		c = 1;
	}
	else if (stockSymbol[2] == '+')
	{
		c = 2;
	}
	else
	{
		return -1;
	}

	//Character 4: 0, ., A-Z
	//65 is the ASCII code of 'A'
	//91 is the ASCII code of 'Z'

	if ((stockSymbol[3] > 64) && (stockSymbol[3] < 91))
	{
	    //A-Z
		d = stockSymbol[3] - INDEX_OF_A_CHAR;	
	}

	//Not in range A-Z. We have 3802 zeros, and 119 dot
	//Maybe we need to count how many $ and dot at the fourth position
	//for even better 'if' order

	//Zero is checked first
	//0: NULL; 32:SPACE
	else if ((stockSymbol[3] == 0) || (stockSymbol[3] == 32))
	{
		return symbolMgmt.symbolIndexArray[a][b][c][0][0];
	}
	else if (stockSymbol[3] == '-')
	{
		d = 3;
	}
	else if (stockSymbol[3] == '.')
	{
		d = 4;
	}
	else if (stockSymbol[3] == '$')
	{
		d = 1;
	}
	else if (stockSymbol[3] == '+')
	{
		d = 2;
	}
	else
	{
		return -1;
	}

	//Character 5: 0, ., A-Z
	if ((stockSymbol[4] > 64) && (stockSymbol[4] < 91))
	{
	    //A-Z
		e = stockSymbol[4] - INDEX_OF_A_CHAR;	
	}
	//Not in range A-Z. We have 3248 zeros, 1 dot (for the stock symbol ZZZZ.T only)
	//Zero is checked first
    //NULL Because: 4: 3248, 32: SPACE
	else if ((stockSymbol[4] == 0) || (stockSymbol[4] == 32))
	{
		return symbolMgmt.symbolIndexArray[a][b][c][d][0];
	}
	else if (stockSymbol[4] == '-')
	{
		e = 3;
	}
	else if (stockSymbol[4] == '.')
	{
		e = 4;
	}
	else if (stockSymbol[4] == '$')
	{
		e = 1;
	}
	else if (stockSymbol[4] == '+')
	{
		e = 2;
	}
	else
	{
		return -1;
	}
	
	//Character 6: 0, ., A-Z
	if ((stockSymbol[5] > 64) && (stockSymbol[5] < 91))
	{
	    //A-Z
		f = stockSymbol[5] - INDEX_OF_A_CHAR;	
	}
	else if ((stockSymbol[5] == 0) || (stockSymbol[5] == 32))
	{
		return symbolMgmt.symbolIndexArray[a][b][c][d][e];
	}
	//Not in range A-Z. We have 3248 zeros, 1 dot (for the stock symbol ZZZZ.T only)
	//Zero is checked first
    //NULL Because: 4: 3248, 32: SPAC
	else if (stockSymbol[5] == '-')
	{
		f = 3;
	}
	else if (stockSymbol[5] == '.')
	{
		f = 4;
	}
	else if (stockSymbol[5] == '$')
	{
		f = 1;
	}
	else if (stockSymbol[5] == '+')
	{
		f = 2;
	}
	else
	{
		return -1;
	}
	
	//Character 7: 0, ., A-Z
	if ((stockSymbol[6] > 64) && (stockSymbol[6] < 91))
	{
	    //A-Z
		g = stockSymbol[6] - INDEX_OF_A_CHAR;	
	}
	else if ((stockSymbol[6] == 0) || (stockSymbol[6] == 32))
	{
		indexOfLongSymbol = symbolMgmt.symbolIndexArray[a][b][c][d][e];
		if (indexOfLongSymbol == -1)
		{
			return -1;
		}
		return symbolMgmt.longSymbolIndexArray[indexOfLongSymbol][f][0];
	}
	//Not in range A-Z. We have 3248 zeros, 1 dot (for the stock symbol ZZZZ.T only)
	//Zero is checked first
    //NULL Because: 4: 3248, 32: SPAC
	else if (stockSymbol[6] == '-')
	{
		g = 3;
	}
	else if (stockSymbol[6] == '.')
	{
		g = 4;
	}
	else if (stockSymbol[6] == '$')
	{
		g = 1;
	}
	else if (stockSymbol[6] == '+')
	{
		g = 2;
	}
	else
	{
		return -1;
	}
	
	//Character 8: 0, ., A-Z
	if ((stockSymbol[7] > 64) && (stockSymbol[7] < 91))
	{
	    //A-Z
		h = stockSymbol[7] - INDEX_OF_A_CHAR;	
	}
	else if ((stockSymbol[7] == 0) || (stockSymbol[7] == 32))
	{
		indexOfLongSymbol = symbolMgmt.symbolIndexArray[a][b][c][d][e];
		if (indexOfLongSymbol == -1)
		{
			return -1;
		}
		return symbolMgmt.longSymbolIndexArray[indexOfLongSymbol][f][g];
	}
	//Not in range A-Z. We have 3248 zeros, 1 dot (for the stock symbol ZZZZ.T only)
	//Zero is checked first
    //NULL Because: 4: 3248, 32: SPAC
	else if (stockSymbol[7] == '-')
	{
		h = 3;
	}
	else if (stockSymbol[7] == '.')
	{
		h = 4;
	}
	else if (stockSymbol[7] == '$')
	{
		h = 1;
	}
	else if (stockSymbol[7] == '+')
	{
		h = 2;
	}
	else
	{
		return -1;
	}

	//This is where the symbol with 8 characters!
	indexOfLongSymbol = symbolMgmt.symbolIndexArray[a][b][c][d][e];						//Index of 5 chars
	if (indexOfLongSymbol == -1)
	{
		return -1;
	}
	
	indexOfLongSymbol = symbolMgmt.longSymbolIndexArray[indexOfLongSymbol][f][g];		//Index of 7 chars
	if (indexOfLongSymbol == -1)
	{
		return -1;
	}
	return symbolMgmt.extraLongSymbolIndexArray[indexOfLongSymbol][h];					//Index of 8 chars
}

/****************************************************************************
- Function name:	ConvertCMSSymbolToComstock
- Input:			
- Output:			symbol in comstock format
- Return:			
- Description:		
- Usage:			Used for ARCA Book and NYSE Book
****************************************************************************/
int ConvertCMSSymbolToComstock(const char *cms, char *symbol)
{
	/* Special Note: char* cms must be NULL terminated before calling this function
		After conversion, currently we support following 5 special characters: $ + - _ .
	*/
	
	int isSuffix = 0, i = 0, j = 0;
	char tmpSymbol[32];
	
	/*******************************************************
						copy root part
	*******************************************************/
	while ((cms[i] != 0) && (cms[i] != ' '))
	{
		tmpSymbol[j++] = cms[i++];
	}
	
	if (i > SYMBOL_LEN)
	{
		// Invalid symbol: symbol is too long
		symbol[0] = 0;
		return ERROR;
	}
	
	if (cms[i] == 0)
	{
		//End of symbol
		tmpSymbol[j] = 0;
		strncpy(symbol, tmpSymbol, SYMBOL_LEN);
		return SUCCESS;
	}
	
	if (i == SYMBOL_LEN)
	{
		// Invalid symbol: root is 8 char, plus suffix will be > 8 chars --> so symbol is too long
		symbol[0] = 0;
		return ERROR;
	}
	
	/*******************************************************
						Convert suffix
	*******************************************************/
	i++;	//Bypass space character
	int suffixLen = strlen(&cms[i]);
	if (suffixLen == 0)
	{
		tmpSymbol[j] = 0;
		strncpy(symbol, tmpSymbol, SYMBOL_LEN);
		return SUCCESS;
	}
	
	while (cms[i] != 0)
	{
		if ((cms[i] == 'P') && (cms[i+1] == 'R'))
		{
			tmpSymbol[j++] = '-';
			i += 2;
			isSuffix = 1;
		}
		else if ((cms[i] == 'W') && (cms[i+1] == 'D'))
		{
			tmpSymbol[j++] = '$';
			i += 2;
			isSuffix = 1;
		}
		else if ((cms[i] == 'W') && (cms[i+1] == 'S'))
		{
			tmpSymbol[j++] = '+';
			i += 2;
			isSuffix = 1;
		}
		else if ((cms[i] == 'C') && (cms[i+1] == 'L'))
		{
			/*tmpSymbol[j++] = '*';
			i += 2;
			isSuffix = 1;*/
			symbol[0] = 0;	//We do not support character *
			return ERROR;
		}
		else if ((cms[i] == 'E') && (cms[i+1] == 'C'))
		{
			/*tmpSymbol[j++] = '!';
			i += 2;
			isSuffix = 1;*/
			symbol[0] = 0;	//We do not support character !
			return ERROR;
		}
		else if ((cms[i] == 'P') && (cms[i+1] == 'P'))
		{
			/*tmpSymbol[j++] = '@';
			i += 2;
			isSuffix = 1;*/
			symbol[0] = 0;	//We do not support character @
			return ERROR;
		}
		else if ((cms[i] == 'C') && (cms[i+1] == 'V'))
		{
			/*tmpSymbol[j++] = '%';
			i += 2;
			isSuffix = 1;*/
			symbol[0] = 0;	//We do not support character %
			return ERROR;
		}
		else if ((cms[i] == 'R') && (cms[i+1] == 'T'))
		{
			/*tmpSymbol[j++] = '^';
			i += 2;
			isSuffix = 1;*/
			symbol[0] = 0;	//We do not support character ^
			return ERROR;
		}
		else if ((cms[i] == 'W') && (cms[i+1] == 'I'))
		{
			/*tmpSymbol[j++] = '#';
			i += 2;
			isSuffix = 1;*/
			symbol[0] = 0;	//We do not support character #
			return ERROR;
		}
		else
		{
			if (isSuffix == 0)
			{
				tmpSymbol[j++] = '.';	//Class
				tmpSymbol[j++] = cms[i];
				i++;
				isSuffix = 1;
			}
			else 
			{
				tmpSymbol[j++] = cms[i];
				i++;
			}
		}
	}
	
	if (j <= SYMBOL_LEN)
	{
		tmpSymbol[j] = 0;
		strncpy(symbol, tmpSymbol, SYMBOL_LEN);
		return SUCCESS;
	}
	else
	{
		// After conversion, the symbol is too long
		symbol[0] = 0;
		return ERROR;
	}
}

/****************************************************************************
- Function name:	ConvertCQSSymbolToComstock
- Input:			
- Output:			symbol in comstock format
- Return:			
- Description:		
- Usage:			This function is used for CQS and UQDF
****************************************************************************/
int ConvertCQSSymbolToComstock(char *cqs, char *comstock)
{
	/*
		Warning: char *cqs must be NULL terminated before calling this function
		After conversion, currently we support following 5 special characters: $ + - _ .
	*/
	char tmpSymbol[32];
	
	int len = strlen(cqs);
	int i = 0, j = 0;
	int flag = 0;
	while ((cqs[i] != 0) && (cqs[i] != ' '))
	{
		switch (cqs[i])
		{
			case 'p':
				tmpSymbol[j++] = '-';
				i++;
				flag = 1;
				break;
			case 'r':
				/*tmpSymbol[j++] = '^';
				i++;
				flag = 1;
				*/
				comstock[0] = 0;		//We do not support this character
				return ERROR;
				break;
			case 'w':
				/*tmpSymbol[j++] = '#';
				i++;
				flag = 1;*/
				comstock[0] = 0;		//We do not support this character
				return ERROR;
				break;
			case '/':
				if ((i+2 < len) && (cqs[i+1] == 'W') && (cqs[i+2] == 'D'))
				{
					tmpSymbol[j++] = '$';
					flag = 1;
					i += 3;
				}
				else if ((i+2 < len) && (cqs[i+1] == 'W') && (cqs[i+2] == 'S'))
				{
					tmpSymbol[j++] = '+';
					flag = 1;
					i += 3;
				}
				else if ((i+2 < len) && (cqs[i+1] == 'C') && (cqs[i+2] == 'L'))
				{
					/*tmpSymbol[j++] = '*';
					flag = 1;
					i += 3;*/
					comstock[0] = 0;		//We do not support this character
					return ERROR;
				}
				else if ((i+2 < len) && (cqs[i+1] == 'E') && (cqs[i+2] == 'C'))
				{
					/*tmpSymbol[j++] = '!';
					flag = 1;
					i += 3;*/
					comstock[0] = 0;		//We do not support this character
					return ERROR;
				}
				else if ((i+2 < len) && (cqs[i+1] == 'P') && (cqs[i+2] == 'P'))
				{
					/*tmpSymbol[j++] = '@';
					flag = 1;
					i += 3;*/
					comstock[0] = 0;		//We do not support this character
					return ERROR;
				}
				else if ((i+2 < len) && (cqs[i+1] == 'C') && (cqs[i+2] == 'V'))
				{
					/*tmpSymbol[j++] = '%';
					flag = 1;
					i += 3;*/
					comstock[0] = 0;		//We do not support this character
					return ERROR;
				}
				else
				{
					if (flag == 0)
					{
						flag = 1;
						tmpSymbol[j++] = '.';
					}
					i++;
				}
				break;
			default:
				tmpSymbol[j++] = cqs[i++];
		}
	}
	
	if (j <= SYMBOL_LEN)
	{
		tmpSymbol[j] = 0;
		strncpy(comstock, tmpSymbol, SYMBOL_LEN);
		return SUCCESS;
	}
	else
	{
		// After conversion, symbol is too long
		comstock[0] = 0;
		return ERROR;
	}
}
