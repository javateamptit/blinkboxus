/****************************************************************************
** Project name:	Equity Arbitrage Application
** File name: 		utdf_proc.c
** Description: 	This file contains function definitions that were declared
					in utdf_proc.h
** Author: 			Luan Vo-Kinh
** First created on May 02, 2008
** Last updated on May 02, 2008
****************************************************************************/

#define _BSD_SOURCE

#include <pthread.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>

#include "utility.h"
#include "book_mgmt.h"
#include "socket_util.h"
#include "trading_mgmt.h"
#include "utdf_proc.h"
#include "kernel_algorithm.h"
#include "aggregation_server_proc.h"

#define MAX_RECV_UTDF_TIMEOUT 150

int arrUTDF_DenominatorValue[3];

typedef struct t_UTDF_arg
{
	int lineIndex;
	char type;
} t_UTDF_arg;

extern t_MonitorUTDF MonitorUTDF[MAX_UTDF_CHANNELS];
extern t_DBLMgmt DBLMgmt;
//****************************************************************************
/****************************************************************************
- Function name:	InitializeUTDF_DenominatorValue
- Input:			
- Output:											
- Return:			
- Description:						 							  				
- Usage:			
****************************************************************************/
int InitializeUTDF_DenominatorValue(void)
{
	arrUTDF_DenominatorValue[0] = 100; //B
	arrUTDF_DenominatorValue[1] = 1000; //C
	arrUTDF_DenominatorValue[2] = 10000; //D
	return SUCCESS;
}

/****************************************************************************
- Function name:	InitUTDF_DataStructure
- Input:			
- Output:											
- Return:			
- Description:						 							  				
- Usage:			
****************************************************************************/
void InitUTDF_DataStructure(void)
{	
	TraceLog(DEBUG_LEVEL, "Initialize UTDF ...\n");

	InitializeUTDF_DenominatorValue();
		
	int i;

	UTDF_Conf.dev = NULL;

	for (i = 0; i < MAX_UTDF_CHANNELS; i++)
	{
		MonitorUTDF[i].recvCounter = 0;
		MonitorUTDF[i].procCounter = 0;
		
		UTDF_Conf.group[i].dataChannel = NULL;
		
		// Channel index
		UTDF_Conf.group[i].index = i;

		// For primary data feed processing
		// Initialize semaphore
		sem_init(&UTDF_Conf.group[i].dataInfo.sem, 0, 0);
		strcpy(UTDF_Conf.group[i].dataInfo.bookName, "UTDF");
		
		memset(UTDF_Conf.group[i].dataInfo.buffer, 0, MAX_BOOK_MSG_IN_BUFFER * MAX_MTU);
		UTDF_Conf.group[i].dataInfo.DisconnectBook = &DisconnectUTDF_Multicast;

		// Initialize counter variables
		UTDF_Conf.group[i].primaryLastSequenceNumber = -1;
		UTDF_Conf.group[i].dataInfo.currentReceivedIndex = 0;
		UTDF_Conf.group[i].dataInfo.currentReadIndex = 0;
		UTDF_Conf.group[i].dataInfo.connectionFlag = 0;
	}
}

/****************************************************************************
- Function name:	Start_UTDF_Thread
- Input:			
- Output:											
- Return:			
- Description:						 							  				
- Usage:			
****************************************************************************/
void *Start_UTDF_Thread(void *agrs)
{
	SetKernelAlgorithm("[UTDF_BASE]");
	InitUTDF_DataStructure();
	
	int i;

	while (UTDF_StatusMgmt.shouldConnect == YES)
	{
		int ret = CreateDBLDeviceV2(UTDF_Conf.NICName, &UTDF_Conf.dev);

		if (ret == ERROR)
		{
			TraceLog(ERROR_LEVEL, "UTDF: Can't create DBL device\n");
			DisconnectUTDF_Multicast();
			return NULL;
		}

		for (i = 0; i < MAX_UTDF_CHANNELS; i++)
		{
		
			/* Start threads to process data from primary data feed */
			if (pthread_create(&UTDF_Conf.group[i].primaryProcessThreadID, NULL, (void*)&ThreadProcessUTDF_PrimaryData, (void*) &UTDF_Conf.group[i]) != SUCCESS)
			{
				TraceLog(ERROR_LEVEL, "UTDF: Cannot create thread to process packets\n");

				DisconnectUTDF_Multicast();
				
				return NULL;
			}
			
			TraceLog(DEBUG_LEVEL, "UTDF: MulticastRecvPort(%s:%d) group(%d): connecting\n", UTDF_Conf.group[i].dataIP, UTDF_Conf.group[i].dataPort, (i+1));
			/* Join to primary multicast group */
			ret = JoinMulticastGroupUsingExistingDeviceV2(UTDF_Conf.dev, UTDF_Conf.group[i].dataIP, UTDF_Conf.group[i].dataPort, &UTDF_Conf.group[i].dataChannel, UTDF_Conf.NICName, (void *) &UTDF_Conf.group[i].dataInfo);

			if (ret == ERROR)
			{
				TraceLog(ERROR_LEVEL, "UTDF: Can't connect to Multicast group (%s:%d)\n", UTDF_Conf.group[i].dataIP, UTDF_Conf.group[i].dataPort);

				DisconnectUTDF_Multicast();

				return NULL;
			}
			
			usleep(100);
		}

		// Notify that UTDF was connected
		UTDF_StatusMgmt.isConnected = CONNECTED;
		for (i = 0; i < MAX_UTDF_CHANNELS; i++)
		{
			UTDF_Conf.group[i].dataInfo.connectionFlag = 1;
		}
		
		// Wait for ALL threads completed
		for (i = 0; i < MAX_UTDF_CHANNELS; i++)
		{
			pthread_join(UTDF_Conf.group[i].primaryProcessThreadID, NULL);
		}
		
		// Release UTDF Multicast Resource
		ReleaseUTDF_MulticastResource();

		InitUTDF_DataStructure();
	}
	
	// Notify UTDF was disconnected
	UTDF_StatusMgmt.isConnected = DISCONNECTED;
	SendPMDisconnectedBookOrderAlertToAS(SERVICE_UTDF_FEED);
	
	TraceLog(DEBUG_LEVEL, "UTDF was disconnected\n");
	
	return NULL;
}

/****************************************************************************
- Function name:	ThreadProcessUTDF_PrimaryData
- Input:			
- Output:											
- Return:			
- Description:						 							  				
- Usage:			
****************************************************************************/
void *ThreadProcessUTDF_PrimaryData(void *args)
{
	SetKernelAlgorithm("[UTDF_PROCESS_MSGS]");

	t_UTDF_Group *workingGroup = (t_UTDF_Group *)args;

	// Temp buffer need to store data from receive buffer
	char *tempBuffer;	
	int index;
	
	while (UTDF_StatusMgmt.shouldConnect == YES)
	{
		// Wait for new message
		if (sem_wait(&workingGroup->dataInfo.sem) != SUCCESS)
		{
			TraceLog(ERROR_LEVEL, "Cannot sem_wait. UTDF will be stopped now (%s %d)\n", workingGroup->dataIP, workingGroup->dataPort);
			DisconnectUTDF_Multicast();
			
			return NULL;
		}
		
		if (UTDF_StatusMgmt.shouldConnect == NO)
		{
			return NULL;
		}
		
		index = workingGroup->dataInfo.currentReadIndex;
		tempBuffer = workingGroup->dataInfo.buffer[index];
		
		if (tempBuffer[MAX_MTU - 1] != STORED)
		{
			TraceLog(ERROR_LEVEL, "UTDF: Receive buffer of primary channel %d is full: received index = %d, read index = %d\n",
				workingGroup->index + 1, workingGroup->dataInfo.currentReceivedIndex, workingGroup->dataInfo.currentReadIndex);
			
			DisconnectUTDF_Multicast();
			
			return NULL;
		}

		ProcessUTDF_MsgFromPrimaryData(workingGroup, tempBuffer);
		
		// Update status of buffer
		tempBuffer[MAX_MTU - 1] = READ;
		
		/*
		If we have Current_Read_Index greater than LIMITATION of INTEGER data type
		that is very dangerous
		*/
		if(workingGroup->dataInfo.currentReadIndex == MAX_BOOK_MSG_IN_BUFFER - 1)
		{
			workingGroup->dataInfo.currentReadIndex = 0;
		}
		else
		{
			workingGroup->dataInfo.currentReadIndex++;
		}
	}
	
	return NULL;	
}

/****************************************************************************
- Function name:	ProcessUTDF_MsgFromPrimaryData
- Input:			
- Output:											
- Return:			
- Description:						 							  				
- Usage:			
****************************************************************************/
int ProcessUTDF_MsgFromPrimaryData(void *args, char buffer[MAX_MTU])
{
	t_UTDF_Group *workingGroup = (t_UTDF_Group *)args;

	int index = 0, icount;
	t_UTDF_Msg msgTmp;

	if (buffer[index] != SOH_CHAR)
	{
		TraceLog(ERROR_LEVEL, "UTDF: Invalid packet from primary data feed, msg = %s\n", buffer);
		return ERROR;
	}

	// Increasing index
	index += 1;

	while ((index < MAX_MTU - 1) && (buffer[index] != ETX_CHAR))
	{
		icount = index + MAX_UTDF_HEADER_LEN;
		while ((icount < MAX_MTU - 1) && (buffer[icount] != US_CHAR) && (buffer[icount] != ETX_CHAR))
		{
			icount++;
		}

		if (icount == MAX_MTU - 1)
		{
			TraceLog(ERROR_LEVEL, "UTDF: Invalid packet from primary data feed, msg = %s\n", buffer);

			return ERROR;
		}
		
		// Check if this is the needed packet: Retransmission Requester is original
		if ( (buffer[index + 4] == 32)  && 
			((buffer[index + 3] == 'O') || (buffer[index + 3] == 'R')) )
		{
			// Get message header
			msgTmp.msgCategory = buffer[index];
			msgTmp.msgType = buffer[index + 1];
			msgTmp.msgSeqNum = atoi(&buffer[index + 5]);

			if (workingGroup->primaryLastSequenceNumber == -1)
			{
				workingGroup->primaryLastSequenceNumber = msgTmp.msgSeqNum;
			}
			else
			{
				/*if (msgTmp.msgSeqNum > workingGroup->primaryLastSequenceNumber + 1)
				{
					if ((msgTmp.msgCategory != 'C') || (msgTmp.msgType != 'L'))
					{
						// Process recovery message
						//ProcessUTDF_RecoveryMessage(workingGroup->primaryLastSequenceNumber + 1, msgTmp.msgSeqNum - 1, workingGroup);
					}
				}
				else if (msgTmp.msgSeqNum < workingGroup->primaryLastSequenceNumber)
				{
					if ((msgTmp.msgCategory != 'C') || (msgTmp.msgType != 'L'))
					{
						//TraceLog(DEBUG_LEVEL, "Primary chanel %d: Why? msgSeqNum (%d) < lastMsgSeqNum (%d), msg = %s\n", workingGroup->index + 1, msgTmp.msgSeqNum, workingGroup->primaryLastSequenceNumber, buffer);
						//AddErrorsToFile("Primary chanel %d: Why? msgSeqNum (%d) < lastMsgSeqNum (%d), msg = %s\n", workingGroup->index + 1, msgTmp.msgSeqNum, workingGroup->primaryLastSequenceNumber, buffer);					
					}
				}*/

				workingGroup->primaryLastSequenceNumber = msgTmp.msgSeqNum;
			}

			if (msgTmp.msgCategory == 'T')
			{
				if (msgTmp.msgType == 'W')
				{
					// Equity long trade
					memcpy(msgTmp.msgHeader, &buffer[index], MAX_UTDF_HEADER_LEN);
					memcpy(msgTmp.msgBody, &buffer[index + MAX_UTDF_HEADER_LEN], icount - (index + MAX_UTDF_HEADER_LEN));
					msgTmp.msgBody[icount - (index + MAX_UTDF_HEADER_LEN)] = 0;

					ProcessUTDF_LongTradeMsg(&msgTmp);
				}
				else if (msgTmp.msgType == 'A')
				{
					// Equity short trade
					memcpy(msgTmp.msgHeader, &buffer[index], MAX_UTDF_HEADER_LEN);
					memcpy(msgTmp.msgBody, &buffer[index + MAX_UTDF_HEADER_LEN], icount - (index + MAX_UTDF_HEADER_LEN));
					msgTmp.msgBody[icount - (index + MAX_UTDF_HEADER_LEN)] = 0;

					ProcessUTDF_ShortTradeMsg(&msgTmp);
				}
				/*else if (msgTmp.msgType == 'Y')
				{
					
					// Trade correction
					memcpy(msgTmp.msgHeader, &buffer[index], MAX_UTDF_HEADER_LEN);
					memcpy(msgTmp.msgBody, &buffer[index + MAX_UTDF_HEADER_LEN], icount - (index + MAX_UTDF_HEADER_LEN));
					msgTmp.msgBody[icount - (index + MAX_UTDF_HEADER_LEN)] = 0;

					ProcessUTDF_TradeCorrectionMsg(&msgTmp);
					
				}
				else if (msgTmp.msgType == 'H')
				{
					// Prior day as-of trade message
				}
				else if (msgTmp.msgType == 'Z')
				{
					// Trade cancel/error message
				}*/
			}
			else if (msgTmp.msgCategory == 'A')
			{
				/*if (msgTmp.msgType == 'H')
				{
					// Equity trading status
				}
				else if (msgTmp.msgType == 'A')
				{
					// General Administrative Message
				}
				else if (msgTmp.msgType == 'B')
				{
					// Issue Symbol Directory Message
				}
				else if (msgTmp.msgType == 'Z')
				{
					// Closing Trade Summary Report
				}
				else if (msgTmp.msgType == 'K')
				{
					// Non-Regulatory Market Center Action
				}*/
			}
			else if (msgTmp.msgCategory == 'C')
			{
				if (msgTmp.msgType == 'L')
				{
					// Sequence number reset
					TraceLog(ERROR_LEVEL, "UTDF: Received message sequence number reset of primary channecl %d, we will reset sequence number now\n", workingGroup->index + 1);

					workingGroup->primaryLastSequenceNumber = msgTmp.msgSeqNum;
				}
				else if (msgTmp.msgType == 'T')
				{
					// Line Integrity
					/*memcpy(buffTemp, &buffer[index], icount - index);
					buffTemp[icount - index] = 0;*/

					//TraceLog(DEBUG_LEVEL, "Line integrity message: %s\n", buffTemp);
				}
				/*else if (msgTmp.msgType == 'I')
				{
					// Start of Day
				}
				else if (msgTmp.msgType == 'J')
				{
					// Stop of Day
				}
				else if (msgTmp.msgType == 'O')
				{
					// Market Session Open
				}
				else if (msgTmp.msgType == 'C')
				{
					// Market Session Close
				}
				else if (msgTmp.msgType == 'K')
				{
					// End of Retransmission Requests
				}
				else if (msgTmp.msgType == 'Z')
				{
					// End of Transmissions
				}
				else if (msgTmp.msgType == 'X')
				{
					// End of Trade Reporting
				}
				else
				{
					// Do nothing				
				}*/
			}
			else if ((msgTmp.msgCategory == 'V') && (msgTmp.msgType == 'M'))
			{
				// Total Consolidated & Market Center Volume
			}
			else
			{
				// Other message
				/*memcpy(buffTemp, &buffer[index], icount - index);
				buffTemp[icount - index] = 0;*/

				//TraceLog(DEBUG_LEVEL, "UTDF other message: category = %c, msgType = %c, msg = %s\n", msgTmp.msgCategory, msgTmp.msgType, buffTemp);
			}
		}
		
		if (buffer[icount] == US_CHAR)
		{
			icount++;
		}

		// Increasing index
		index = icount;
	}

	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessUTDF_LongTradeMsg
- Input:			
- Output:											
- Return:			
- Description:						 							  				
- Usage:			
****************************************************************************/
int ProcessUTDF_LongTradeMsg(t_UTDF_Msg *msg)
{
	t_TradeInfo trade;

	msg->msgHeader[MAX_UTDF_HEADER_LEN] = 0;

	// Data Feed
	trade.dataFeed = UTDF_DATA;
	
	// PARTICIPANT ID
	trade.participantId = msg->msgHeader[13];

	/*if (trade.participantId == 'D')
	{
		trade.participantId = msg->msgHeader[23];
	}*/

	// TIME STAMP
	trade.timestamp = ((Lrc_atoi(&msg->msgHeader[14], 2) - TIMESTAMP_ADJUST_HOUR) * 3600 + Lrc_atoi(&msg->msgHeader[16], 2) * 60 + Lrc_atoi(&msg->msgHeader[18], 2)) * 1000 + Lrc_atoi(&msg->msgHeader[20], 3);

	// SECURITY SYMBOL: 11 bytes
	char cqsSymbol[12];

	strncpy(cqsSymbol, msg->msgBody, 11);
	cqsSymbol[11] = 0;

	char symbol[SYMBOL_LEN];
	if (ConvertCQSSymbolToComstock(cqsSymbol, symbol) == ERROR)
	{
		return SUCCESS;
	}
	
	if (IsSymbolIgnored(symbol) == 1)
	{
		return SUCCESS;
	}
	
	// PRICE DENOMINATOR INDICATOR: 1 byte
	int numerator = msg->msgBody[18] - 'B';
	
	if ((numerator < 0) || (numerator > 2))
	{
		TraceLog(ERROR_LEVEL, "Something wrong in parsing the Long Trade Message\n");
	}
	
	// TRADE PRICE: 10 bytes
	int whole = Lrc_atoi(&msg->msgBody[19], 10);

	trade.price = (double)whole / arrUTDF_DenominatorValue[numerator];

	// TRADE VOLUME: 9 bytes
	trade.shares = Lrc_atoi(&msg->msgBody[32], 9);

	// Add trade to list
	AddTradeForTradeSymbolList(symbol, &trade);

	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessUTDF_ShortTradeMsg
- Input:			
- Output:											
- Return:			
- Description:						 							  				
- Usage:			
****************************************************************************/
int ProcessUTDF_ShortTradeMsg(t_UTDF_Msg *msg)
{
	t_TradeInfo trade;

	msg->msgHeader[MAX_UTDF_HEADER_LEN] = 0;

	// Data Feed
	trade.dataFeed = UTDF_DATA;
	
	// PARTICIPANT ID
	trade.participantId = msg->msgHeader[13];

	/*if (trade.participantId == 'D')
	{
		trade.participantId = msg->msgHeader[23];
	}*/

	// TIME STAMP
	trade.timestamp = ((Lrc_atoi(&msg->msgHeader[14], 2) - TIMESTAMP_ADJUST_HOUR) * 3600 + Lrc_atoi(&msg->msgHeader[16], 2) * 60 + Lrc_atoi(&msg->msgHeader[18], 2)) * 1000 + Lrc_atoi(&msg->msgHeader[20], 3);

	// SECURITY SYMBOL: 5 bytes
	char cqsSymbol[6];
	
	strncpy(cqsSymbol, msg->msgBody, 5);
	cqsSymbol[5] = 0;
	
	char symbol[SYMBOL_LEN];
	if (ConvertCQSSymbolToComstock(cqsSymbol, symbol) == ERROR)
	{
		return SUCCESS;
	}

	if (IsSymbolIgnored(symbol) == 1)
	{
		return SUCCESS;
	}
	
	// PRICE DENOMINATOR INDICATOR: 1 byte
	int numerator = msg->msgBody[6] - 'B';
	
	if ((numerator < 0) || (numerator > 2))
	{
		TraceLog(ERROR_LEVEL, "Something wrong in parsing the Short Trade Message\n");
	}
	
	// TRADE PRICE: 6 bytes
	int whole = Lrc_atoi(&msg->msgBody[7], 6);

	trade.price = (double)whole / arrUTDF_DenominatorValue[numerator];

	// TRADE VOLUME: 6 bytes
	trade.shares = Lrc_atoi(&msg->msgBody[13], 6);

	// Add trade to list
	AddTradeForTradeSymbolList(symbol, &trade);

	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessUTDF_TradeCorrectionMsg
- Input:			
- Output:											
- Return:			
- Description:						 							  				
- Usage:			
****************************************************************************/
int ProcessUTDF_TradeCorrectionMsg(t_UTDF_Msg *msg)
{
	//t_TradeInfo trade;

	//msg->msgHeader[MAX_UTDF_HEADER_LEN] = 0;

	// PARTICIPANT ID
	//trade.participantId = msg->msgHeader[13];

	/*if (trade.participantId == 'D')
	{
		trade.participantId = msg->msgHeader[23];
	}*/

	// TIME STAMP
	//trade.timestamp = ((Lrc_atoi(&msg->msgHeader[14], 2) - TIMESTAMP_ADJUST_HOUR) * 3600 + Lrc_atoi(&msg->msgHeader[16], 2) * 60 + Lrc_atoi(&msg->msgHeader[18], 2)) * 1000 + Lrc_atoi(&msg->msgHeader[20], 3);

	/*
	// SECURITY SYMBOL: 11 bytes
	char symbol[SYMBOL_LEN];

	strncpy(symbol, &msg->msgBody[8], SYMBOL_LEN);

	// CONSOLIDATED HIGH PRICE DENOMINATOR: 1 byte
	int numerator = msg->msgBody[79] - 'A';

	// CONSOLIDATED HIGH PRICE: 10 bytes
	int whole = Lrc_atoi(&msg->msgBody[80], 10);

	double highPrice = (double)whole / arrUTDF_DenominatorValue[numerator];

	// CONSOLIDATED LOW PRICE DENOMINATOR: 1 byte
	numerator = msg->msgBody[90] - 'A';

	// CONSOLIDATED LOW PRICE: 10 bytes
	whole = Lrc_atoi(&msg->msgBody[91], 10);

	double lowPrice = (double)whole / arrUTDF_DenominatorValue[numerator];

	// CONSOLIDATED LAST SALE PRICE DENOMINATOR: 1 byte
	numerator = msg->msgBody[101] - 'A';

	// CONSOLIDATED LAST SALE PRICE: 10 bytes
	whole = Lrc_atoi(&msg->msgBody[102], 10);

	double lastSalePrice = (double)whole / arrUTDF_DenominatorValue[numerator];

	// CONSOLIDATED VOLUME: 11 bytes
	int shares = Lrc_atoi(&msg->msgBody[116], 11);

	*/

	return SUCCESS;
}

/****************************************************************************
- Function name:	DisconnectUTDF_Multicast
- Input:			
- Output:											
- Return:			
- Description:						 							  				
- Usage:			
****************************************************************************/
int DisconnectUTDF_Multicast(void)
{
	UTDF_StatusMgmt.shouldConnect = NO;
	UTDF_StatusMgmt.isConnected = DISCONNECTED;

	int i;

	for (i = 0; i < MAX_UTDF_CHANNELS; i++)
	{
		UTDF_Conf.group[i].dataInfo.connectionFlag = 0;
		sem_post(&UTDF_Conf.group[i].dataInfo.sem);
	}

	return SUCCESS;
}

/****************************************************************************
- Function name:	ReleaseUTDF_MulticastResource
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ReleaseUTDF_MulticastResource(void)
{
	int i, ret;
	int isError = 0;
	pthread_mutex_lock(&DBLMgmt.mutexLock);
	int needToRelease = (IsDBLTimedOut(UTDF_Conf.dev) == 0) ? 1:0;

	for (i = 0; i < MAX_UTDF_CHANNELS; i++)
	{
		if (UTDF_Conf.group[i].dataChannel != NULL)
		{
			if (needToRelease == 1)
			{
				if (isError == 0)
				{
					ret = LeaveDBLMulticastGroup(UTDF_Conf.group[i].dataChannel, UTDF_Conf.group[i].dataIP);
					if (ret != EADDRNOTAVAIL)
						dbl_unbind(UTDF_Conf.group[i].dataChannel);
					else
						isError = 1;
				}
			}

			UTDF_Conf.group[i].dataChannel = NULL;
		}
		
		sem_destroy(&UTDF_Conf.group[i].dataInfo.sem);
	}

	if (UTDF_Conf.dev != NULL)
	{
		UTDF_Conf.dev = NULL;
	}

	pthread_mutex_unlock(&DBLMgmt.mutexLock);
	return SUCCESS;
}
