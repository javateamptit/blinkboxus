/*****************************************************************************
**	Project:		Equity Arbitrage Application
**	Filename:		position_manager.c
**	Description:	This file contains function definitions that were declared
					in position_manager.h	
**	Author:			Luan Vo-Kinh
**	First created:	13-Sep-2007
**	Last updated:	-----------
*****************************************************************************/

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include <stdio.h>
#include <string.h>
#include <execinfo.h>

#include "position_manager.h"
#include "global_definition.h"
#include "socket_util.h"
#include "utility.h"
#include "aggregation_server_proc.h"
#include "raw_data_mgmt.h"
#include "order_mgmt_proc.h"
#include "nasdaq_rash_data.h"
#include "nasdaq_rash_proc.h"
#include "arca_direct_proc.h"
#include "configuration.h"
#include "book_mgmt.h"
#include "trading_mgmt.h"
#include "cts_feed_proc.h"
#include "uqdf_feed_proc.h"
#include "utdf_proc.h"
#include "cqs_feed_proc.h"
#include "arca_book_proc.h"
#include "nasdaq_book_proc.h"
#include "kernel_algorithm.h"
#include "environment_proc.h"
#include "hbitime.h"
#include "logging.h"

#include <fcntl.h>
#include <unistd.h>

#define VALUE_OF_OPTION_FOR_HELP 'h'
#define VALUE_OF_OPTION_FOR_VERSION 'v'

#define VALUE_OF_OPTION_FOR_ARCA_BOOK 'A'
#define VALUE_OF_OPTION_FOR_NASDAQ_BOOK 'N'

#define VALUE_OF_OPTION_FOR_ARCA_DIRECT 'a'
#define VALUE_OF_OPTION_FOR_NDAQ_RASH 'r'

#define VALUE_OF_OPTION_FOR_CTS 'c'
#define VALUE_OF_OPTION_FOR_UTDF 'u'
#define VALUE_OF_OPTION_FOR_CQS 'C'
#define VALUE_OF_OPTION_FOR_UQDF 'U'

#define VALUE_OF_OPTION_FOR_ALL_BOOK 17
#define VALUE_OF_OPTION_FOR_ALL_ORDER 18
#define VALUE_OF_OPTION_FOR_ALL_FEED 19

#define VALUE_OF_OPTION_FOR_NO_RECOVERY 20

/****************************************************************************
** Global variables definition
****************************************************************************/
int isAutoStart = 0;

t_AutoStart numSecondsAutoStartECNOrders[2];
t_AutoStart numSecondsAutoStartECNBooks[2];
t_AutoStart numSecondsAutoStartFeeds[4];

t_MonitorArcaBook MonitorArcaBook[MAX_ARCA_GROUPS];
t_MonitorNasdaqBook MonitorNasdaqBook;
t_MonitorCQS MonitorCQS[MAX_CQS_MULTICAST_LINES];
t_MonitorUQDF MonitorUQDF[MAX_UQDF_CHANNELS];
t_MonitorCTS MonitorCTS[MAX_MULTICAST_LINES];
t_MonitorUTDF MonitorUTDF[MAX_UTDF_CHANNELS];
t_DBLMgmt DBLMgmt;
/****************************************************************************
** Function declarations
****************************************************************************/
/****************************************************************************
- Function name:	InstallCoreDump
- Input:			
- Output:			N/A
- Return:			Success or Failure
- Description:		
- Usage:			This will create a core file if error such as Segmentation fault
****************************************************************************/
#include <sys/resource.h>
void InstallCoreDump(void)
{
	struct rlimit debugLimit;
	debugLimit.rlim_cur = -1; // no limit
	debugLimit.rlim_max = -1; //no lim
	setrlimit(RLIMIT_CORE, &debugLimit);
	return;
}

/****************************************************************************
- Function name:	main
- Input:			+ argc
					+ argv
- Output:			N/A
- Return:			Success or Failure
- Description:		
- Usage:			The routine will be called by runtime environment
****************************************************************************/
int main (int argc, char *argv[])
{
	DEVLOGF("Running with extra debugging output");	
	
	// Create data path
	CreateDataPath();

	//Initialize DBL Library for using thoughout the application
	int retval = dbl_init(DBL_VERSION_API);
	if (retval != 0)
	{
		TraceLog(ERROR_LEVEL, "dbl_init failed (%d)\n", retval);
		return ERROR;
	}

	SaveLoginInfo();

	RegisterSignalHandlers();

	// Check user input
	if (ProcessUserInput(argc, argv) == ERROR)
	{
		return ERROR;
	}

	InitKernelAlgorithmConfig();
	if (LoadKernelAlgorithmConfigFromFile("kernel_algorithm") == ERROR)
	{
		TraceLog(ERROR_LEVEL, "Cannot load Kernel Algorithm configuration\n");
		return ERROR;
	}
	SetKernelAlgorithm("[POSITION_MANAGER_BASE]");
	
	// Initialize data configs
	InitializeDataConfs();

	// Initialize data structures
	InitializeDataStructures();
	
	InitializePositionCllection();

	if (LoadDataConfig() == ERROR)
	{
		return ERROR;
	}
	
	// Load data into data structures
	if (LoadDataStructures() == ERROR)
	{
		return ERROR;
	}

	// Update status for symbol ignored list
	UpdateStatusSymbolIgnoredList();
	
	// Process raw data entries
	ProcessRawDataEntries();

	if (isAutoStart == 1)
	{
		// Create thread to automatic connect to the servers
		pthread_t threadAutoStart_id;
		pthread_create (&threadAutoStart_id, NULL, (void*) &ProcessAutoStartThreads, NULL);
	}

	// Start up main thread
	StartUpMainThreads();

	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessUserInput
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessUserInput(int argc, char *argv[])
{
	//const char *optString = "hva:n:f:r:c:u:C:U:";
	const char *optString = "hvA:N:f:r:c:u:C:U:";
	const char* program_name;
	
	program_name = argv[0];
	
	static struct option long_opt[] = {
					{"help", 0, NULL, VALUE_OF_OPTION_FOR_HELP}, 
					{"arca-book", 1, 0, VALUE_OF_OPTION_FOR_ARCA_BOOK}, 
					{"ndaq-book", 1, 0, VALUE_OF_OPTION_FOR_NASDAQ_BOOK}, 
					
					{"arca-direct", 1, 0, VALUE_OF_OPTION_FOR_ARCA_DIRECT}, 
					{"ndaq-rash", 1, 0, VALUE_OF_OPTION_FOR_NDAQ_RASH}, 
					
					{"cts", 1, 0, VALUE_OF_OPTION_FOR_CTS}, 
					{"utdf", 1, 0, VALUE_OF_OPTION_FOR_UTDF}, 
					{"cqs", 1, 0, VALUE_OF_OPTION_FOR_CQS}, 
					{"uqdf", 1, 0, VALUE_OF_OPTION_FOR_UQDF}, 
					
					{"all-order", 1, NULL, VALUE_OF_OPTION_FOR_ALL_ORDER}, 
					{"all-book", 1, NULL, VALUE_OF_OPTION_FOR_ALL_BOOK}, 
					{"all-feed", 1, NULL, VALUE_OF_OPTION_FOR_ALL_FEED},
					{"no-gap-request", 0, NULL, VALUE_OF_OPTION_FOR_NO_RECOVERY},
					{"version", 0 , NULL , VALUE_OF_OPTION_FOR_VERSION},
					{NULL, 0, NULL, 0}
				};

	int valOpt, option_index = -1;
	int i;

	// Initilize numSecondsAutoStartECNOrders structure
	for (i = 0; i < 2; i++)
	{
		numSecondsAutoStartECNOrders[i].isProcess = -1;
		numSecondsAutoStartECNOrders[i].value = -1;
	}
	
	for (i = 0; i < 2; i++)
	{
		numSecondsAutoStartECNBooks[i].isProcess = -1;
		numSecondsAutoStartECNBooks[i].value = -1;
	}
	
	for (i = 0; i < 4; i++)
	{
		numSecondsAutoStartFeeds[i].isProcess = -1;
		numSecondsAutoStartFeeds[i].value = -1;
	}

	while (1)
	{
		valOpt = getopt_long(argc, argv, optString, long_opt, &option_index);

		if (valOpt == -1)
		{
			break;
		}

		switch (valOpt)
		{
			case VALUE_OF_OPTION_FOR_HELP:
				Print_Help(program_name);
				return ERROR;	//This will terminate the programe.
				break;

			case VALUE_OF_OPTION_FOR_NDAQ_RASH:
				if (GetOptionForECNOrder(1, optarg) == ERROR)
				{
					return ERROR;
				}
				else
				{
					isAutoStart = 1;
				}
				break;

			case VALUE_OF_OPTION_FOR_ARCA_DIRECT:
				if (GetOptionForECNOrder(0, optarg) == ERROR)
				{
					return ERROR;
				}
				else
				{
					isAutoStart = 1;
				}
				break;
			
			case VALUE_OF_OPTION_FOR_ARCA_BOOK:
				if (GetOptionForECNBook(0, optarg) == ERROR)
				{
					return ERROR;
				}
				else
				{
					isAutoStart = 1;
				}
				break;

			case VALUE_OF_OPTION_FOR_NASDAQ_BOOK:
				if (GetOptionForECNBook(1, optarg) == ERROR)
				{
					return ERROR;
				}
				else
				{
					isAutoStart = 1;
				}
				break;

			case VALUE_OF_OPTION_FOR_CTS:
				if (GetOptionForFeed(0, optarg) == ERROR)
				{
					return ERROR;
				}
				else
				{
					isAutoStart = 1;
				}
				break;
			
			case VALUE_OF_OPTION_FOR_UTDF:
				if (GetOptionForFeed(1, optarg) == ERROR)
				{
					return ERROR;
				}
				else
				{
					isAutoStart = 1;
				}
				break;
			
			case VALUE_OF_OPTION_FOR_CQS:
				if (GetOptionForFeed(2, optarg) == ERROR)
				{
					return ERROR;
				}
				else
				{
					isAutoStart = 1;
				}
				break;
			
			case VALUE_OF_OPTION_FOR_UQDF:
				if (GetOptionForFeed(3, optarg) == ERROR)
				{
					return ERROR;
				}
				else
				{
					isAutoStart = 1;
				}
				break;
				
			case VALUE_OF_OPTION_FOR_ALL_BOOK:
				if (GetOptionForAllECNBooks(optarg) == ERROR)
				{
					return ERROR;
				}
				else
				{
					isAutoStart = 1;
				}
				break;

			case VALUE_OF_OPTION_FOR_ALL_ORDER:
				if (GetOptionForAllECNOrders(optarg) == ERROR)
				{
					return ERROR;
				}
				else
				{
					isAutoStart = 1;
				}
				break;
			
			case VALUE_OF_OPTION_FOR_ALL_FEED:
				if (GetOptionForAllFeeds(optarg) == ERROR)
				{
					return ERROR;
				}
				else
				{
					isAutoStart = 1;
				}
				break;
			case VALUE_OF_OPTION_FOR_NO_RECOVERY:
				NoArcaGapRequest = 1;
				NoNasdaqGapRequest = 1;
				break;
			case VALUE_OF_OPTION_FOR_VERSION:
				TraceLog(DEBUG_LEVEL, "************************************************************\n");
				TraceLog(DEBUG_LEVEL, "Version of PM: '%s' \n", VERSION);
				TraceLog(DEBUG_LEVEL, "************************************************************\n");
				
				return ERROR;	
			default:
				TraceLog(ERROR_LEVEL, "Invalid commands. \"-h\" or \"--help\" for list commands\n\n");
				return ERROR;
		}
	}

	return SUCCESS;
}

/****************************************************************************
- Function name:	Print_Help
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int Print_Help(const char *program_name)
{
	TraceLog(USAGE_LEVEL, "**********************************************************************\n");
	TraceLog(USAGE_LEVEL, "* -h --help for list commands\n");
	TraceLog(USAGE_LEVEL, "* -A --arca-book <hh:mm>: time to connect to ARCA Book\n");
	TraceLog(USAGE_LEVEL, "* -N --ndaq-book <hh:mm>: time to connect to NASDAQ Book\n\n");

	TraceLog(USAGE_LEVEL, "* -f --arca-direct <hh:mm>: time to connect to ARCA DIRECT Order\n");
	TraceLog(USAGE_LEVEL, "* -r --ndaq-rash <hh:mm>: time to connect to NASDAQ RASH Order\n\n");

	TraceLog(USAGE_LEVEL, "* -c --cts <hh:mm>: time to connect to CTS\n");
	TraceLog(USAGE_LEVEL, "* -u --utdf <hh:mm>: time to connect to UTDF\n");
	TraceLog(USAGE_LEVEL, "* -C --cqs <hh:mm>: time to connect to CQS\n");
	TraceLog(USAGE_LEVEL, "* -U --uqdf <hh:mm>: time to connect to UQDF\n\n");
	TraceLog(USAGE_LEVEL, "* -v --version for show PM's version \n\n");
	
	TraceLog(USAGE_LEVEL, "* --all-book <hh:mm>: time to connect to all ECN Books\n");
	TraceLog(USAGE_LEVEL, "* --all-order <hh:mm>: time to connect to all ECN Orders\n");
	TraceLog(USAGE_LEVEL, "* --all-feed <hh:mm>: time to connect to all Consolidated Feeds\n");
	TraceLog(USAGE_LEVEL, "* --no-gap-request: Do not connect to book retransmission server, if gap is detected, flush book\n\n");

	TraceLog(USAGE_LEVEL, "* Example: To set time to connect to all ECN Orders at 6:30 AM:\n");
	TraceLog(USAGE_LEVEL, "*\t$%s --all-order 06:30\n", program_name);
	TraceLog(USAGE_LEVEL, "**********************************************************************\n\n");

	return SUCCESS;
}

/****************************************************************************
- Function name:	GetOptionForECNBook
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int GetOptionForECNBook(int ecnIndex, const char *arg)
{
	t_Parameters paraList;
	int tmpHour, tmpMin;

	char ecnName[20];

	switch (ecnIndex)
	{
		case 0:
			strcpy(ecnName, "ARCA Book");
			break;
		case 1:
			strcpy(ecnName, "NASDAQ Book");
			break;
	}

	// Parse ...
	paraList.countParameters = Lrc_Split(arg, ':', &paraList);
	if (paraList.countParameters != 2)
	{
		TraceLog(ERROR_LEVEL, "Invalid time format for connecting to %s, time format = %s\n", ecnName, arg);
		TraceLog(DEBUG_LEVEL, "-h --help for list commands\n");

		return ERROR;
	}

	tmpHour = atoi(paraList.parameterList[0]);
	if ((tmpHour < 0) || (tmpHour > 23))
	{
		TraceLog(ERROR_LEVEL, "Invalid time for connecting to %s, hour(s) = %s\n", ecnName, paraList.parameterList[0]);
		TraceLog(DEBUG_LEVEL, "-h --help for list commands\n");

		return ERROR;
	}
	else if (tmpHour == 0)
	{
		if ((paraList.parameterList[0][0] < 48) || (paraList.parameterList[0][1] > 57))
		{
			TraceLog(ERROR_LEVEL, "Invalid time for connecting to %s, hour(s) = %s\n", ecnName, paraList.parameterList[0]);
			TraceLog(DEBUG_LEVEL, "-h --help for list commands\n");

			return ERROR;
		}
	}
	
	tmpMin = atoi(paraList.parameterList[1]);
	if ((tmpMin < 0) || (tmpMin > 59))
	{
		TraceLog(ERROR_LEVEL, "Invalid time for connecting to %s, minutes(s) = %s\n", ecnName, paraList.parameterList[1]);
		TraceLog(DEBUG_LEVEL, "-h --help for list commands\n");

		return ERROR;
	}
	else if (tmpMin == 0)
	{
		if ((paraList.parameterList[1][0] < 48) || (paraList.parameterList[1][1] > 57))
		{
			TraceLog(ERROR_LEVEL, "Invalid time for connecting to %s, minutes(s) = %s\n", ecnName, paraList.parameterList[1]);
			TraceLog(DEBUG_LEVEL, "-h --help for list commands\n");

			return ERROR;
		}
	}

	TraceLog(DEBUG_LEVEL, "PM will automatic connect to %s at %02d:%02d\n", ecnName, tmpHour, tmpMin);

	numSecondsAutoStartECNBooks[ecnIndex].value = tmpHour * 60 + tmpMin;

	return SUCCESS;
}

/****************************************************************************
- Function name:	GetOptionForECNOrder
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int GetOptionForECNOrder(int ecnIndex, const char *arg)
{
	t_Parameters paraList;
	int tmpHour, tmpMin;

	char ecnName[20];

	switch (ecnIndex)
	{
		case 0:
			strcpy(ecnName, "ARCA DIRECT");
			break;
		case 1:
			strcpy(ecnName, "NASDAQ RASH");
			break;
	}

	// Parse ...
	paraList.countParameters = Lrc_Split(arg, ':', &paraList);
	if (paraList.countParameters != 2)
	{
		TraceLog(ERROR_LEVEL, "Invalid time format for connecting to %s Order, time format = %s\n", ecnName, arg);
		TraceLog(DEBUG_LEVEL, "-h --help for list commands\n");

		return ERROR;
	}

	tmpHour = atoi(paraList.parameterList[0]);
	if ((tmpHour < 0) || (tmpHour > 23))
	{
		TraceLog(ERROR_LEVEL, "Invalid time for connecting to %s Order, hour(s) = %s\n", ecnName, paraList.parameterList[0]);
		TraceLog(DEBUG_LEVEL, "-h --help for list commands\n");

		return ERROR;
	}
	else if (tmpHour == 0)
	{
		if ((paraList.parameterList[0][0] < 48) || (paraList.parameterList[0][1] > 57))
		{
			TraceLog(ERROR_LEVEL, "Invalid time for connecting to %s Order, hour(s) = %s\n", ecnName, paraList.parameterList[0]);
			TraceLog(DEBUG_LEVEL, "-h --help for list commands\n");

			return ERROR;
		}
	}
	
	tmpMin = atoi(paraList.parameterList[1]);
	if ((tmpMin < 0) || (tmpMin > 59))
	{
		TraceLog(ERROR_LEVEL, "Invalid time for connecting to %s Order, minutes(s) = %s\n", ecnName, paraList.parameterList[1]);
		TraceLog(DEBUG_LEVEL, "-h --help for list commands\n");

		return ERROR;
	}
	else if (tmpMin == 0)
	{
		if ((paraList.parameterList[1][0] < 48) || (paraList.parameterList[1][1] > 57))
		{
			TraceLog(ERROR_LEVEL, "Invalid time for connecting to %s Order, minutes(s) = %s\n", ecnName, paraList.parameterList[1]);
			TraceLog(DEBUG_LEVEL, "-h --help for list commands\n");

			return ERROR;
		}
	}

	TraceLog(DEBUG_LEVEL, "PM will automatic connect to %s Order at %02d:%02d\n", ecnName, tmpHour, tmpMin);

	numSecondsAutoStartECNOrders[ecnIndex].value = tmpHour * 60 + tmpMin;

	return SUCCESS;
}

/****************************************************************************
- Function name:	GetOptionForFeed
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int GetOptionForFeed(int index, const char *arg)
{
	t_Parameters paraList;
	int tmpHour, tmpMin;

	char feedName[20];

	switch (index)
	{
		case 0:
			strcpy(feedName, "CTS");
			break;
		case 1:
			strcpy(feedName, "UTDF");
			break;
		case 2:
			strcpy(feedName, "CQS");
			break;
		case 3:
			strcpy(feedName, "UQDF");
			break;
	}

	// Parse ...
	paraList.countParameters = Lrc_Split(arg, ':', &paraList);
	if (paraList.countParameters != 2)
	{
		TraceLog(ERROR_LEVEL, "Invalid time format for connecting to %s, time format = %s\n", feedName, arg);
		TraceLog(DEBUG_LEVEL, "-h --help for list commands\n");

		return ERROR;
	}

	tmpHour = atoi(paraList.parameterList[0]);
	if ((tmpHour < 0) || (tmpHour > 23))
	{
		TraceLog(ERROR_LEVEL, "Invalid time for connecting to %s, hour(s) = %s\n", feedName, paraList.parameterList[0]);
		TraceLog(DEBUG_LEVEL, "-h --help for list commands\n");

		return ERROR;
	}
	else if (tmpHour == 0)
	{
		if ((paraList.parameterList[0][0] < 48) || (paraList.parameterList[0][1] > 57))
		{
			TraceLog(ERROR_LEVEL, "Invalid time for connecting to %s, hour(s) = %s\n", feedName, paraList.parameterList[0]);
			TraceLog(DEBUG_LEVEL, "-h --help for list commands\n");

			return ERROR;
		}
	}
	
	tmpMin = atoi(paraList.parameterList[1]);
	if ((tmpMin < 0) || (tmpMin > 59))
	{
		TraceLog(ERROR_LEVEL, "Invalid time for connecting to %s, minutes(s) = %s\n", feedName, paraList.parameterList[1]);
		TraceLog(DEBUG_LEVEL, "-h --help for list commands\n");

		return ERROR;
	}
	else if (tmpMin == 0)
	{
		if ((paraList.parameterList[1][0] < 48) || (paraList.parameterList[1][1] > 57))
		{
			TraceLog(ERROR_LEVEL, "Invalid time for connecting to %s, minutes(s) = %s\n", feedName, paraList.parameterList[1]);
			TraceLog(DEBUG_LEVEL, "-h --help for list commands\n");

			return ERROR;
		}
	}

	TraceLog(DEBUG_LEVEL, "PM will automatic connect to %s at %02d:%02d\n", feedName, tmpHour, tmpMin);

	numSecondsAutoStartFeeds[index].value = tmpHour * 60 + tmpMin;

	return SUCCESS;
}

/****************************************************************************
- Function name:	GetOptionForAllECNBooks
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int GetOptionForAllECNBooks(const char *arg)
{
	t_Parameters paraList;
	int tmpHour, tmpMin;

	// Parse ...
	paraList.countParameters = Lrc_Split(arg, ':', &paraList);
	if (paraList.countParameters != 2)
	{
		TraceLog(ERROR_LEVEL, "Invalid time format for connecting to all ECN Books, optarg = %s\n", arg);
		TraceLog(DEBUG_LEVEL, "-h --help for list commands\n");

		return ERROR;
	}

	tmpHour = atoi(paraList.parameterList[0]);
	if ((tmpHour < 0) || (tmpHour > 23))
	{
		TraceLog(ERROR_LEVEL, "Invalid time format for connecting to all ECN Books, hour(s) = %s\n", paraList.parameterList[0]);
		TraceLog(DEBUG_LEVEL, "-h --help for list commands\n");

		return ERROR;
	}
	else if (tmpHour == 0)
	{
		if ((paraList.parameterList[0][0] < 48) || (paraList.parameterList[0][1] > 57))
		{
			TraceLog(ERROR_LEVEL, "Invalid time format for connecting to all ECN Books, hour(s) = %s\n", paraList.parameterList[0]);
			TraceLog(DEBUG_LEVEL, "-h --help for list commands\n");

			return ERROR;
		}
	}
	
	tmpMin = atoi(paraList.parameterList[1]);
	if ((tmpMin < 0) || (tmpMin > 59))
	{
		TraceLog(ERROR_LEVEL, "Invalid time format for connecting to all ECN Books, minute(s) = %s\n", paraList.parameterList[1]);
		TraceLog(DEBUG_LEVEL, "-h --help for list commands\n");

		return ERROR;
	}
	else if (tmpMin == 0)
	{
		if ((paraList.parameterList[1][0] < 48) || (paraList.parameterList[1][1] > 57))
		{
			TraceLog(ERROR_LEVEL, "Invalid time format for connecting to all ECN Books, minute(s) = %s\n", paraList.parameterList[1]);
			TraceLog(DEBUG_LEVEL, "-h --help for list commands\n");

			return ERROR;
		}
	}

	TraceLog(DEBUG_LEVEL, "PM will automatic connect to all ECN Books at %02d:%02d\n", tmpHour, tmpMin);

	// PM is having only ARCA Book and NASDAQ Book
	numSecondsAutoStartECNBooks[0].value = tmpHour * 60 + tmpMin;	// ARCA
	numSecondsAutoStartECNBooks[1].value = tmpHour * 60 + tmpMin;	// NASDAQ

	return SUCCESS;
}

/****************************************************************************
- Function name:	GetOptionForAllECNOrders
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int GetOptionForAllECNOrders(const char *arg)
{
	t_Parameters paraList;
	int tmpHour, tmpMin;

	// Parse ...
	paraList.countParameters = Lrc_Split(arg, ':', &paraList);
	if (paraList.countParameters != 2)
	{
		TraceLog(ERROR_LEVEL, "Invalid time format for connecting to all ECN Orders, optarg = %s\n", arg);
		TraceLog(DEBUG_LEVEL, "-h --help for list commands\n");

		return ERROR;
	}

	tmpHour = atoi(paraList.parameterList[0]);
	if ((tmpHour < 0) || (tmpHour > 23))
	{
		TraceLog(ERROR_LEVEL, "Invalid time format for connecting to all ECN Orders, hour(s) = %s\n", paraList.parameterList[0]);
		TraceLog(DEBUG_LEVEL, "-h --help for list commands\n");

		return ERROR;
	}
	else if (tmpHour == 0)
	{
		if ((paraList.parameterList[0][0] < 48) || (paraList.parameterList[0][1] > 57))
		{
			TraceLog(ERROR_LEVEL, "Invalid time format for connecting to all ECN Orders, hour(s) = %s\n", paraList.parameterList[0]);
			TraceLog(DEBUG_LEVEL, "-h --help for list commands\n");

			return ERROR;
		}
	}
	
	tmpMin = atoi(paraList.parameterList[1]);
	if ((tmpMin < 0) || (tmpMin > 59))
	{
		TraceLog(ERROR_LEVEL, "Invalid time format for connecting to all ECN Orders, minute(s) = %s\n", paraList.parameterList[1]);
		TraceLog(DEBUG_LEVEL, "-h --help for list commands\n");

		return ERROR;
	}
	else if (tmpMin == 0)
	{
		if ((paraList.parameterList[1][0] < 48) || (paraList.parameterList[1][1] > 57))
		{
			TraceLog(ERROR_LEVEL, "Invalid time format for connecting to all ECN Orders, minute(s) = %s\n", paraList.parameterList[1]);
			TraceLog(DEBUG_LEVEL, "-h --help for list commands\n");

			return ERROR;
		}
	}

	TraceLog(DEBUG_LEVEL, "PM will automatic connect to all ECN Orders at %02d:%02d\n", tmpHour, tmpMin);

	// PM is having only ARCA DIRECT and NASDAQ RASH
	numSecondsAutoStartECNOrders[0].value = tmpHour * 60 + tmpMin;
	numSecondsAutoStartECNOrders[1].value = tmpHour * 60 + tmpMin;

	return SUCCESS;
}

/****************************************************************************
- Function name:	GetOptionForAllFeeds
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int GetOptionForAllFeeds(const char *arg)
{
	t_Parameters paraList;
	int tmpHour, tmpMin;

	// Parse ...
	paraList.countParameters = Lrc_Split(arg, ':', &paraList);
	if (paraList.countParameters != 2)
	{
		TraceLog(ERROR_LEVEL, "Invalid time format for connecting to all Consolidated Feeds, optarg = %s\n", arg);
		TraceLog(DEBUG_LEVEL, "-h --help for list commands\n");

		return ERROR;
	}

	tmpHour = atoi(paraList.parameterList[0]);
	if ((tmpHour < 0) || (tmpHour > 23))
	{
		TraceLog(ERROR_LEVEL, "Invalid time format for connecting to all Consolidated Feeds, hour(s) = %s\n", paraList.parameterList[0]);
		TraceLog(DEBUG_LEVEL, "-h --help for list commands\n");

		return ERROR;
	}
	else if (tmpHour == 0)
	{
		if ((paraList.parameterList[0][0] < 48) || (paraList.parameterList[0][1] > 57))
		{
			TraceLog(ERROR_LEVEL, "Invalid time format for connecting to all Consolidated Feeds, hour(s) = %s\n", paraList.parameterList[0]);
			TraceLog(DEBUG_LEVEL, "-h --help for list commands\n");

			return ERROR;
		}
	}
	
	tmpMin = atoi(paraList.parameterList[1]);
	if ((tmpMin < 0) || (tmpMin > 59))
	{
		TraceLog(ERROR_LEVEL, "Invalid time format for connecting to all Consolidated Feeds, minute(s) = %s\n", paraList.parameterList[1]);
		TraceLog(DEBUG_LEVEL, "-h --help for list commands\n");

		return ERROR;
	}
	else if (tmpMin == 0)
	{
		if ((paraList.parameterList[1][0] < 48) || (paraList.parameterList[1][1] > 57))
		{
			TraceLog(ERROR_LEVEL, "Invalid time format for connecting to all Consolidated Feeds, minute(s) = %s\n", paraList.parameterList[1]);
			TraceLog(DEBUG_LEVEL, "-h --help for list commands\n");

			return ERROR;
		}
	}

	TraceLog(DEBUG_LEVEL, "PM will automatic connect to all Consolidated Feeds at %02d:%02d\n", tmpHour, tmpMin);

	// PM is having only CTS, UTDF, CQS, UQDF
	numSecondsAutoStartFeeds[0].value = tmpHour * 60 + tmpMin;	// CTS
	numSecondsAutoStartFeeds[1].value = tmpHour * 60 + tmpMin;	// UTDF
	numSecondsAutoStartFeeds[2].value = tmpHour * 60 + tmpMin;	// CQS
	numSecondsAutoStartFeeds[3].value = tmpHour * 60 + tmpMin;	// UQDF
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	LoadDataConfig
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int LoadDataConfig(void)
{
	// Load Aggregation Server configuration
	if (LoadAggregationServerConf() == ERROR)
	{
		return ERROR;
	}

	// Load ignore stock list
	if (LoadIgnoreStockList() == ERROR)
	{
		return ERROR;
	}

	// Load Position Manager global configuration
	if (LoadPMGlobalConf() == ERROR)
	{
		return ERROR;
	}	

	// Load PM Orders configuration
	if (LoadPMOrdersConf() == ERROR)
	{
		return ERROR;
	}

	// Load PM Books configuration
	if (LoadAllBooksConfig() == ERROR)
	{
		return ERROR;
	}

	// Load CTS Feed configuration
	if (LoadCTS_Config("cts_conf") == ERROR)
	{
		return ERROR;
	}

	// Load UTDF configuration
	if (LoadUTDF_Config("utdf_conf") == ERROR)
	{
		return ERROR;
	}

	// Load CQS Feed configuration
	if (LoadCQS_Config("cqs_conf") == ERROR)
	{
		return ERROR;
	}	

	// Load UQDF Feed configuration
	if (LoadUQDF_Config("uqdf_conf") == ERROR)
	{
		return ERROR;
	}
	
	// Load Exchange Configuration
	if (LoadExchangesConf() == ERROR)
	{
		return ERROR;
	}
	
	//Load trading account
	if (LoadTradingAccountFromFile("trading_account", &TradingAccount) == ERROR)
	{
		TraceLog(ERROR_LEVEL, "Cannot load Trading Account configuration!\n");
		return ERROR;
	}
	
	return 0;
}

/****************************************************************************
- Function name:	LoadDataStructures
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int LoadDataStructures(void)
{
	// Get number of raw data entries
	if (LoadRawDataEntries() == ERROR)
	{
		return ERROR;
	}

	return SUCCESS;
}

/****************************************************************************
- Function name:	InitializeDataConfs
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int InitializeDataConfs(void)
{
	// Initialize file name prefix list
	InitializeFileNamePrefix();

	// Initialize Aggregation Server information
	InitializeAggregationServerInfo();

	// Initialize ignore stock list
	InitializeIgnoreStockList();

	// Initialize order status index
	InitializeOrderStatusIndex();

	// Initialize Exchange Mapping
	InitializeExchangeMapping();
	
	// Initialize Exchange Status List
	InitializeExchangeStatusList();
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	InitializeDataStructures
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int InitializeDataStructures(void)
{
	InitMonitorArcaBook();
	InitMonitorNasdaqBook();
	InitMonitorCTS();
	InitMonitorUTDF();
	InitMonitorCQS();
	InitMonitorUQDF();
	
	InitStockSymbolDataStructure();
	
	// Initialize book management data structure
	InitBookMgmtDataStructure();
	
	// Initialize BBO Quotes management data structure
	InitBBOMgmtDataStructure();
	
	// Initialize symbol index list
	InitializeSymbolIndexList();

	// Initilize order id index
	InitilizeOrderIdIndexList();

	// Initialize trade symbol structure
	InitializeTradeSymbolStructure();

	// Initialize raw data management structures
	InitializeRawDataManegementStructures();

	// Initialize AS open order Structures
	InitializeASOpenOrderStructures();

	// Initialize symbol trading muxtex
	InitializeSymbolTradingMutex();
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	RegisterSignalHandlers
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int RegisterSignalHandlers(void)
{
    signal(SIGPIPE, SIG_IGN);

	signal(SIGINT, Process_INTERRUPT_Signal);

	// Segmentation fault handling:
	//signal(SIGSEGV, Process_SEGV_Signal);
	struct sigaction act;
	memset (&act, 0, sizeof(act));
	act.sa_sigaction = &Process_SEGV_Signal;
	sigfillset(&act.sa_mask);
	act.sa_flags = SA_SIGINFO;
	sigaction (SIGSEGV, &act, NULL);

	return SUCCESS;
}

/****************************************************************************
- Function name:	Process_SIGPIPE_Signal
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
void Process_SIGPIPE_Signal(int sig_num)
{	
	TraceLog(ERROR_LEVEL, "SIGPIPE signal is catched and ignored.\n");

	SaveARCA_DIRECT_SeqNumToFile();
	SaveNASDAQ_RASH_SeqNumToFile();

	signal(SIGPIPE, Process_SIGPIPE_Signal);
}


/****************************************************************************
- Function name:	Process_INTERRUPT_Signal
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
void Process_INTERRUPT_Signal(int sig_num)
{		
	TraceLog(DEBUG_LEVEL, "Processing Interrupt Signal \n");
	
	SaveARCA_DIRECT_SeqNumToFile();
	SaveNASDAQ_RASH_SeqNumToFile();
	
	signal(SIGINT, Process_INTERRUPT_Signal);
	
	exit(0);
}

/****************************************************************************
- Function name:	DumpCurrentStackFrames
- Input:			N/A
- Output:			N/A
- Return:			N/A
- Description:		Show information about current stack frames
- Usage:			N/A
****************************************************************************/
void DumpCurrentStackFrames()
{
    void *bt[80];
	size_t btsize;

	// get void*'s for all entries on the stack
	btsize = backtrace(bt, 80);

	fprintf(stderr, "\nSTACK BACKTRACKING INFORMATION:\n");

	backtrace_symbols_fd(bt, btsize, 2);

	fprintf(stderr, "*********************************\n");
}

/****************************************************************************
- Function name:	Process_SEGV_Signal
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
void Process_SEGV_Signal(int signalno, siginfo_t *siginfo, void *context)
{
	SaveARCA_DIRECT_SeqNumToFile();
	SaveNASDAQ_RASH_SeqNumToFile();
	
	DumpCurrentStackFrames();

	fflush(stderr);
	
	signal(SIGSEGV, SIG_DFL);
}

/****************************************************************************
- Function name:	ProcessAutoStartThreads
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
void *ProcessAutoStartThreads(void *arg)
{
	time_t now;
	struct tm *timeNow;
	int numSeconds, i;

	while (1)
	{
		//Get local Time
		now = hbitime_seconds();

		timeNow = (struct tm *)localtime(&now);

		numSeconds = timeNow->tm_hour * 60 + timeNow->tm_min;

		// Check and start ECN Book threads ...
		for (i = 0; i < 2; i++)
		{
			if ((numSecondsAutoStartECNBooks[i].value != -1) && (numSecondsAutoStartECNBooks[i].isProcess == -1))
			{
				if (numSeconds >= numSecondsAutoStartECNBooks[i].value)
				{
					if (i == 0)
					{
						TraceLog(DEBUG_LEVEL, "AUTO: PM begins to connect to ARCA Book...\n");
						numSecondsAutoStartECNBooks[0].isProcess = 0;
						
						if (bookStatusMgmt[BOOK_ARCA].isConnected == DISCONNECTED && bookStatusMgmt[BOOK_ARCA].shouldConnect == NO)
						{
							pthread_t bookThread;
							bookStatusMgmt[BOOK_ARCA].shouldConnect = YES;
							
							// Create and start thread to talk with ARCA Book server
							pthread_create(&bookThread, NULL, (void*)&ARCA_Book_Thread, NULL);
						}
					}
					else
					{
						TraceLog(DEBUG_LEVEL, "AUTO: PM begins to connect to NASDAQ Book...\n");
						numSecondsAutoStartECNBooks[1].isProcess = 0;

						if (bookStatusMgmt[BOOK_NASDAQ].isConnected == DISCONNECTED && bookStatusMgmt[BOOK_NASDAQ].shouldConnect == NO)
						{
							pthread_t bookThread;
							bookStatusMgmt[BOOK_NASDAQ].shouldConnect = YES;

							// Create and start thread to talk with NASDAQ Book server
							pthread_create(&bookThread, NULL, (void*)&NDAQ_Book_Thread, NULL);
						}
					}
				}
			}
		}
		
		// Check and start ECN Order threads ...
		for (i = 0; i < 2; i++)
		{
			if ((numSecondsAutoStartECNOrders[i].value != -1) && (numSecondsAutoStartECNOrders[i].isProcess == -1))
			{
				if (numSeconds >= numSecondsAutoStartECNOrders[i].value)
				{
					if (i == 0)
					{
						TraceLog(DEBUG_LEVEL, "AUTO: PM begins to connect to ARCA DIRECT...\n");
						numSecondsAutoStartECNOrders[0].isProcess = 0;
						ProcessPMConnectECNOrder(ARCA_DIRECT_INDEX);
					}
					else
					{
						TraceLog(DEBUG_LEVEL, "AUTO: PM begins to connect to NASDAQ RASH...\n");
						numSecondsAutoStartECNOrders[1].isProcess = 0;
						ProcessPMConnectECNOrder(NASDAQ_RASH_INDEX);
					}
				}
			}
		}
		
		// Check and start Feed threads ...
		for (i = 0; i < 4; i++)
		{
			if ((numSecondsAutoStartFeeds[i].value != -1) && (numSecondsAutoStartFeeds[i].isProcess == -1))
			{
				if (numSeconds >= numSecondsAutoStartFeeds[i].value)
				{
					if (i == 0)
					{
						TraceLog(DEBUG_LEVEL, "AUTO: PM begins to connect to CTS...\n");
						numSecondsAutoStartFeeds[0].isProcess = 0;
						if (CTS_StatusMgmt.isConnected == DISCONNECTED &&
							CTS_StatusMgmt.shouldConnect == NO)
						{
							pthread_t feedThread;
							CTS_StatusMgmt.shouldConnect = YES;
							
							// Create and start thread to talk with CTS Feed
							pthread_create(&feedThread, NULL, (void*)&CTS_Feed_Thread, NULL);
						}
					}
					else if (i == 1)
					{
						TraceLog(DEBUG_LEVEL, "AUTO: PM begins to connect to UTDF...\n");
						numSecondsAutoStartFeeds[1].isProcess = 0;

						if (UTDF_StatusMgmt.isConnected == DISCONNECTED &&
							UTDF_StatusMgmt.shouldConnect == NO)
						{
							pthread_t feedThread;
							UTDF_StatusMgmt.shouldConnect = YES;
							
							// Create and start thread to talk with UTDF
							pthread_create(&feedThread, NULL, (void*)&Start_UTDF_Thread, NULL);
						}
					}
					else if (i == 2)
					{
						TraceLog(DEBUG_LEVEL, "AUTO: PM begins to connect to CQS...\n");
						numSecondsAutoStartFeeds[2].isProcess = 0;

						if (CQS_StatusMgmt.isConnected == DISCONNECTED &&
							CQS_StatusMgmt.shouldConnect == NO)
						{
							pthread_t feedThread;
							CQS_StatusMgmt.shouldConnect = YES;
							
							// Create and start thread to talk with CQS Feed
							pthread_create(&feedThread, NULL, (void*)&CQS_Feed_Thread, NULL);
						}
					}
					else if (i == 3)
					{
						TraceLog(DEBUG_LEVEL, "AUTO: PM begins to connect to UQDF...\n");
						numSecondsAutoStartFeeds[3].isProcess = 0;

						if (UQDF_StatusMgmt.isConnected == DISCONNECTED &&
							UQDF_StatusMgmt.shouldConnect == NO)
						{
							pthread_t feedThread;
							UQDF_StatusMgmt.shouldConnect = YES;

							// Create and start thread to talk with UQDF Feed
							pthread_create(&feedThread, NULL, (void*)&UQDF_Feed_Thread, NULL);
						}
					}
				}
			}
		}

		// Sleep in second(s)
		sleep(15);
	}

	return NULL;
}

/****************************************************************************
- Function name:	StartUpMainThreads
- Input:			+ argc
					+ argv
- Output:			N/A
- Return:			Success or Failure
- Description:		
- Usage:			The routine will be called by runtime environment
****************************************************************************/
int StartUpMainThreads(void)
{	
	// Create thread to talk to Aggregation Server
	pthread_t threadAggregationServerManagement_id;
	pthread_create (&threadAggregationServerManagement_id, 
					NULL, 
					(void*) &ProcessAggregationServer, 
					NULL);
		
	//pthread_t simBook_id;
	//pthread_create(&simBook_id, NULL, (void*)&Test_ECN_Books_Thread, NULL);

	pthread_t monitorLiquidateOrder_id;
	pthread_create(&monitorLiquidateOrder_id, NULL, (void*)&Monitoring_Liquidate_Order_Thread, NULL);

	pthread_t monitorOrdersGoAwayOrSpreadTooWide;
	pthread_create(&monitorOrdersGoAwayOrSpreadTooWide, NULL, (void*)&Monitoring_Orders_GoAway_Or_Spread_Too_Wide_Thread, NULL);
	
	pthread_t monitorThread;
	pthread_create(&monitorThread, NULL, (void *)MonitorThread, NULL);
	
	//LoadAndProcessCTSPacketFromFile();	
	//LoadAndProcessUTDFPacketFromFile();
	
	//LoadAndProcessCQSPacketFromFile();

	/*
	sleep(2);
	
	CTS_StatusMgmt.shouldConnect = YES;
	
	pthread_t threadCTS_id;
	pthread_create (&threadCTS_id, NULL, (void*) &CTS_Feed_Thread, NULL);
	*/

	/*
	sleep(2);
	
	UTDF_StatusMgmt.shouldConnect = YES;
	
	pthread_t threadUTDF_id;
	pthread_create (&threadUTDF_id, NULL, (void*) &Start_UTDF_Thread, NULL);
	*/
	
	/*
	sleep(2);
	
	CQS_StatusMgmt.shouldConnect = YES;
	
	pthread_t threadCQS_id;
	pthread_create (&threadCQS_id, NULL, (void*) &CQS_Feed_Thread, NULL);
	*/
	
	// Wait for threads completion	
	pthread_join (threadAggregationServerManagement_id, NULL);

	return 0;
}

/****************************************************************************
- Function name:	PrintCmdHelp
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
void PrintCmdHelp(void)
{
	TraceLog(USAGE_LEVEL, "\n***********************************************************************\n");
	TraceLog(USAGE_LEVEL, "* help: for display command list\n");
	TraceLog(USAGE_LEVEL, "* exit: for kill this application\n");
	
	// For CTS
	TraceLog(USAGE_LEVEL, "*\n* For CTS, line_id = 0..4, group_name = A or B; (line_id = 0: mean is apply all multicast line)\n");
	TraceLog(USAGE_LEVEL, "* join CTS <group_name><line_id>: for join multicast line <line_id> of group <group_name>\n");
	TraceLog(USAGE_LEVEL, "* leave CTS <group_name><line_id>: for leave multicast line <line_id> of group <group_name>\n");
	TraceLog(USAGE_LEVEL, "* join CTS: for join all multicast line of CTS\n");
	TraceLog(USAGE_LEVEL, "* leave CTS: for leave all multicast line of CTS\n");
	TraceLog(USAGE_LEVEL, "* status CTS: for connection status of multicast lines of CTS\n");
	
	// For UTDF
	TraceLog(USAGE_LEVEL, "*\n* For UTDF, channel_id = 0..3, group_type = P or S; (channel_id = 0 mean is apply all multicast channel)\n");
	TraceLog(USAGE_LEVEL, "* join UTDF <group_type><channel_id>: for join multicast channel <channel_id> of <group_type> feed\n");
	TraceLog(USAGE_LEVEL, "* leave UTDF <group_type><channel_id>: for leave multicast channel <channel_id> of <group_type> feed\n");
	TraceLog(USAGE_LEVEL, "* join UTDF: for join all multicast channel of UTDF\n");
	TraceLog(USAGE_LEVEL, "* leave UTDF: for leave all multicast channel of UTDF\n");
	TraceLog(USAGE_LEVEL, "* status UTDF: for connection status of multicast channels of UTDF\n");
	
	// For CQS
	TraceLog(USAGE_LEVEL, "*\n* For CQS, line_id = 0..5, group_name = A or B; (line_id = 0: mean is apply all multicast line)\n");
	TraceLog(USAGE_LEVEL, "* join CQS <group_name><line_id>: for join multicast line <line_id> of group <group_name>\n");
	TraceLog(USAGE_LEVEL, "* leave CQS <group_name><line_id>: for leave multicast line <line_id> of group <group_name>\n");
	TraceLog(USAGE_LEVEL, "* join CQS: for join all multicast line of CQS\n");
	TraceLog(USAGE_LEVEL, "* leave CQS: for leave all multicast line of CQS\n");
	TraceLog(USAGE_LEVEL, "* status CQS: for connection status of multicast lines of CQS\n");
	
	// Example
	TraceLog(USAGE_LEVEL, "*\n* Example:\n");
	TraceLog(USAGE_LEVEL, "* 1. To join multicast line 2 of group A (CTS):\n* > join CTS A2\n");
	TraceLog(USAGE_LEVEL, "* 2. To join all multicast line of group B (CTS):\n* > join CTS B0\n");
	TraceLog(USAGE_LEVEL, "* 3. To join multicast channel 1 of primary feed (UTDF):\n* > join UTDF P1\n");
	TraceLog(USAGE_LEVEL, "* 4. To leave all multicast channel of secondary feed (UTDF):\n* > leave UTDF S0\n");
	TraceLog(USAGE_LEVEL, "* 5. To join multicast line 3 of group A (CQS):\n* > join CQS A3\n");
	TraceLog(USAGE_LEVEL, "* 6. To leave multicast line 3 of group A (CQS):\n* > leave CQS A3\n");
	TraceLog(USAGE_LEVEL, "***********************************************************************\n");
}

/****************************************************************************
- Function name:	InitMonitorArcaBook
- Input:		N/A
- Output:		N/A
- Return:		N/A
- Description:	N/A
- Usage:		N/A
****************************************************************************/
void InitMonitorArcaBook(void)
{
	int i;
	for (i = 0; i < MAX_ARCA_GROUPS; i++)
	{
		MonitorArcaBook[i].lastReceivedIndex = -99;
		MonitorArcaBook[i].lastReadIndex = -99;
		MonitorArcaBook[i].recvCounter = 0;
		MonitorArcaBook[i].procCounter = 0;
	}
}

void InitMonitorNasdaqBook(void)
{
	MonitorNasdaqBook.lastReceivedIndex = -99;
	MonitorNasdaqBook.lastReadIndex = -99;
	MonitorNasdaqBook.recvCounter = 0;
	MonitorNasdaqBook.procCounter = 0;
}

void InitMonitorCQS(void)
{
	int i;
	for (i = 0; i < MAX_CQS_MULTICAST_LINES; i++)
	{
		MonitorCQS[i].lastReceivedIndex = -99;
		MonitorCQS[i].lastReadIndex = -99;
		MonitorCQS[i].recvCounter = 0;
		MonitorCQS[i].procCounter = 0;
	}
}

void InitMonitorUQDF(void)
{
	int i;
	for (i = 0; i < MAX_UQDF_CHANNELS; i++)
	{
		MonitorUQDF[i].lastReceivedIndex = -99;
		MonitorUQDF[i].lastReadIndex = -99;
		MonitorUQDF[i].recvCounter = 0;
		MonitorUQDF[i].procCounter = 0;
	}
}

void InitMonitorCTS(void)
{
	int i;
	for (i = 0; i < MAX_MULTICAST_LINES; i++)
	{
		MonitorCTS[i].lastReceivedIndex = -99;
		MonitorCTS[i].lastReadIndex = -99;
		MonitorCTS[i].recvCounter = 0;
		MonitorCTS[i].procCounter = 0;
	}
}

void InitMonitorUTDF(void)
{
	int i;
	for (i = 0; i < MAX_UTDF_CHANNELS; i++)
	{
		MonitorUTDF[i].lastReceivedIndex = -99;
		MonitorUTDF[i].lastReadIndex = -99;
		MonitorUTDF[i].recvCounter = 0;
		MonitorUTDF[i].procCounter = 0;
	}
}
/****************************************************************************
- Function name:	MonitorThread
- Input:		N/A
- Output:		N/A
- Return:		N/A
- Description:	N/A
- Usage:		N/A
****************************************************************************/
void *MonitorThread(void* args)
{
	SetKernelAlgorithm("[BOOK_IDLE_MONITORING]");
	
	int i;
	const int TIME_TO_ALERT_DATA_UNHANDLED = 60;
	
	while(1)
	{
		//***********************************************************************************************
		//					MONITORING ARCA BOOK IDLE STATUS
		//***********************************************************************************************
		if (bookStatusMgmt[BOOK_ARCA].isConnected == CONNECTED)
		{
			for (i = 0; i < MAX_ARCA_GROUPS; i++)
			{
				if (ARCA_Book_Conf.group[i].dataInfo.currentReceivedIndex == MonitorArcaBook[i].lastReceivedIndex)
				{
					MonitorArcaBook[i].recvCounter++;
					if (MonitorArcaBook[i].recvCounter > 30)
					{
						MonitorArcaBook[i].recvCounter = 0;
						if (SendPMBookIdleWarningToAS(BOOK_ARCA, i) == ERROR)
						{
							TraceLog(ERROR_LEVEL, "Can not send message to AS for warning Arca group (%d).\n", i+1);
						}
					}
				}
				else
				{
					MonitorArcaBook[i].lastReceivedIndex = ARCA_Book_Conf.group[i].dataInfo.currentReceivedIndex;
					MonitorArcaBook[i].recvCounter = 0;
					
					//this group received data, so we check if it processed data or not
					if (MonitorArcaBook[i].lastReadIndex != ARCA_Book_Conf.group[i].dataInfo.currentReadIndex)
					{
						MonitorArcaBook[i].lastReadIndex = ARCA_Book_Conf.group[i].dataInfo.currentReadIndex;
						MonitorArcaBook[i].procCounter = 0; //reset counter
					}
					else
					{
						if (MonitorArcaBook[i].procCounter < TIME_TO_ALERT_DATA_UNHANDLED)
						{
							MonitorArcaBook[i].procCounter++;
						}
						else
						{
							if (asCollection.connection.socket != -1)
							{
								SendPMDataUnhandledOnGroup(SERVICE_ARCA_BOOK, i);
							}
							MonitorArcaBook[i].procCounter = 0;
						}
					}
				}
			}
		}
		
		//***********************************************************************************************
		//					MONITORING NASDAQ BOOK IDLE STATUS
		//***********************************************************************************************
		if (bookStatusMgmt[BOOK_NASDAQ].isConnected == CONNECTED)
		{
			if (NDAQ_MoldUDP_Book.dataInfo.currentReceivedIndex == MonitorNasdaqBook.lastReceivedIndex)
			{
				MonitorNasdaqBook.recvCounter++;
				if (MonitorNasdaqBook.recvCounter > 2)
				{
					MonitorNasdaqBook.recvCounter = 0;
					TraceLog(ERROR_LEVEL, "NASDAQ Book: could not received anything within 2 secs\n");
					DisconnectNASDAQBook();
				}
			}
			else
			{
				MonitorNasdaqBook.lastReceivedIndex = NDAQ_MoldUDP_Book.dataInfo.currentReceivedIndex;
				MonitorNasdaqBook.recvCounter = 0;
				
				//this group received data, so we check if it processed data or not
				if (MonitorNasdaqBook.lastReadIndex != NDAQ_MoldUDP_Book.dataInfo.currentReadIndex)
				{
					MonitorNasdaqBook.lastReadIndex = NDAQ_MoldUDP_Book.dataInfo.currentReadIndex;
					MonitorNasdaqBook.procCounter = 0; //reset counter
				}
				else
				{
					if (MonitorNasdaqBook.procCounter < TIME_TO_ALERT_DATA_UNHANDLED)
					{
						MonitorNasdaqBook.procCounter++;
					}
					else
					{
						if (asCollection.connection.socket != -1)
						{
							SendPMDataUnhandledOnGroup(SERVICE_NASDAQ_BOOK, 0);
						}
						MonitorNasdaqBook.procCounter = 0;
					}
				}
			}
		}

		//***********************************************************************************************
		//					MONITORING CQS BOOK IDLE STATUS
		//***********************************************************************************************
		if (CQS_StatusMgmt.isConnected == CONNECTED)
		{
			for (i = 0; i < MAX_CQS_MULTICAST_LINES; i++)
			{
				if (CQS_Feed_Conf.group[i].dataInfo.currentReceivedIndex == MonitorCQS[i].lastReceivedIndex)
				{
					MonitorCQS[i].recvCounter++;
					if (MonitorCQS[i].recvCounter > 61)
					{
						MonitorCQS[i].recvCounter = 0;
						TraceLog(ERROR_LEVEL, "CQS GROUP %d not received anything within 60 secs\n", i+1 );
						DisconnectCQS_Feed();
					}
				}
				else
				{
					MonitorCQS[i].recvCounter = 0;
					MonitorCQS[i].lastReceivedIndex = CQS_Feed_Conf.group[i].dataInfo.currentReceivedIndex;
					
					//this group received data, so we check if it processed data or not
					if (MonitorCQS[i].lastReadIndex != CQS_Feed_Conf.group[i].dataInfo.currentReadIndex)
					{
						MonitorCQS[i].lastReadIndex = CQS_Feed_Conf.group[i].dataInfo.currentReadIndex;
						MonitorCQS[i].procCounter = 0; //reset counter
					}
					else
					{
						if (MonitorCQS[i].procCounter < TIME_TO_ALERT_DATA_UNHANDLED)
						{
							MonitorCQS[i].procCounter++;
						}
						else
						{
							if (asCollection.connection.socket != -1)
							{
								SendPMDataUnhandledOnGroup(SERVICE_CQS_FEED, i);
							}
							MonitorCQS[i].procCounter = 0;
						}
					}
				}
			}
		}
		
		//***********************************************************************************************
		//					MONITORING UQDF IDLE STATUS
		//***********************************************************************************************
		if (UQDF_StatusMgmt.isConnected == CONNECTED)
		{
			for (i = 0; i < MAX_UQDF_CHANNELS; i++)
			{
				if (UQDF_Conf.group[i].dataInfo.currentReceivedIndex == MonitorUQDF[i].lastReceivedIndex)
				{
					MonitorUQDF[i].recvCounter++;
					if (MonitorUQDF[i].recvCounter > 61)
					{
						MonitorUQDF[i].recvCounter = 0;
						TraceLog(ERROR_LEVEL, "UQDF GROUP %d not received anything within 60 secs\n", i+1 );
						DisconnectUQDF_Multicast();
					}
				}
				else
				{
					MonitorUQDF[i].recvCounter = 0;
					MonitorUQDF[i].lastReceivedIndex = UQDF_Conf.group[i].dataInfo.currentReceivedIndex;
					
					//this group received data, so we check if it processed data or not
					if (MonitorUQDF[i].lastReadIndex != UQDF_Conf.group[i].dataInfo.currentReadIndex)
					{
						MonitorUQDF[i].lastReadIndex = UQDF_Conf.group[i].dataInfo.currentReadIndex;
						MonitorUQDF[i].procCounter = 0; //reset counter
					}
					else
					{
						if (MonitorUQDF[i].procCounter < TIME_TO_ALERT_DATA_UNHANDLED)
						{
							MonitorUQDF[i].procCounter++;
						}
						else
						{
							if (asCollection.connection.socket != -1)
							{
								SendPMDataUnhandledOnGroup(SERVICE_UQDF_FEED, i);
							}
							MonitorUQDF[i].procCounter = 0;
						}
					}
				}
			}
		}
		
		//***********************************************************************************************
		//					MONITORING CTS IDLE STATUS
		//***********************************************************************************************
		if (CTS_StatusMgmt.isConnected == CONNECTED)
		{
			for (i = 0; i < MAX_MULTICAST_LINES; i++)
			{
				if (CTS_Feed_Conf.group[i].dataInfo.currentReceivedIndex == MonitorCTS[i].lastReceivedIndex)
				{
					MonitorCTS[i].recvCounter++;
					if (MonitorCTS[i].recvCounter > 61)
					{
						MonitorCTS[i].recvCounter = 0;
						TraceLog(ERROR_LEVEL, "CTS GROUP %d not received anything within 60 secs\n", i+1 );
						DisconnectCTS_Feed();
					}
				}
				else
				{
					MonitorCTS[i].recvCounter = 0;
					MonitorCTS[i].lastReceivedIndex = CTS_Feed_Conf.group[i].dataInfo.currentReceivedIndex;
					
					//this group received data, so we check if it processed data or not
					if (MonitorCTS[i].lastReadIndex != CTS_Feed_Conf.group[i].dataInfo.currentReadIndex)
					{
						MonitorCTS[i].lastReadIndex = CTS_Feed_Conf.group[i].dataInfo.currentReadIndex;
						MonitorCTS[i].procCounter = 0; //reset counter
					}
					else
					{
						if (MonitorCTS[i].procCounter < TIME_TO_ALERT_DATA_UNHANDLED)
						{
							MonitorCTS[i].procCounter++;
						}
						else
						{
							if (asCollection.connection.socket != -1)
							{
								SendPMDataUnhandledOnGroup(SERVICE_CTS_FEED, i);
							}
							MonitorCTS[i].procCounter = 0;
						}
					}
				}
			}
		}
		
		//***********************************************************************************************
		//					MONITORING UTDF IDLE STATUS
		//***********************************************************************************************
		if (UTDF_StatusMgmt.isConnected == CONNECTED)
		{
			for (i = 0; i < MAX_UTDF_CHANNELS; i++)
			{
				if (UTDF_Conf.group[i].dataInfo.currentReceivedIndex == MonitorUTDF[i].lastReceivedIndex)
				{
					MonitorUTDF[i].recvCounter++;
					if (MonitorUTDF[i].recvCounter > 61)
					{
						MonitorUTDF[i].recvCounter = 0;
						TraceLog(ERROR_LEVEL, "UTDF GROUP %d not received anything within 60 secs\n", i+1 );
						DisconnectUTDF_Multicast();
					}
				}
				else
				{
					MonitorUTDF[i].recvCounter = 0;
					MonitorUTDF[i].lastReceivedIndex = UTDF_Conf.group[i].dataInfo.currentReceivedIndex;
					
					//this group received data, so we check if it processed data or not
					if (MonitorUTDF[i].lastReadIndex != UTDF_Conf.group[i].dataInfo.currentReadIndex)
					{
						MonitorUTDF[i].lastReadIndex = UTDF_Conf.group[i].dataInfo.currentReadIndex;
						MonitorUTDF[i].procCounter = 0; //reset counter
					}
					else
					{
						if (MonitorUTDF[i].procCounter < TIME_TO_ALERT_DATA_UNHANDLED)
						{
							MonitorUTDF[i].procCounter++;
						}
						else
						{
							if (asCollection.connection.socket != -1)
							{
								SendPMDataUnhandledOnGroup(SERVICE_UTDF_FEED, i);
							}
							MonitorUTDF[i].procCounter = 0;
						}
					}
				}
			}
		}
		
		sleep (1);	//sleep for 1 second resolution
	}
	return NULL;
}
/***************************************************************************/

/****************************************************************************
- Function name:	SaveLoginInfo
- Input:			+ 
					+ 
- Output:			N/A
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int SaveLoginInfo()
{
	//Initialize version
	#ifdef VERSION
		TraceLog(DEBUG_LEVEL, "Current version: %s\n", VERSION);
	#else	
		TraceLog(ERROR_LEVEL, "Current version: UNKNOWN\n");
	#endif
	
	int hour, min, sec, date, year, month;
	time_t now = hbitime_seconds();
	struct tm *timeNow = (struct tm *) localtime(&now);
	
	char fullPath[MAX_PATH_LEN] = "\0";
	Environment_get_data_filename("version_log.txt", fullPath, MAX_PATH_LEN);
	int file_desc;
	
	struct flock region_to_lock;
	int res;
	char byte_write[80];
	memset(byte_write, 0, 80);
	
	//Get time when PM started
	hour	= timeNow->tm_hour;
	min		= timeNow->tm_min;
	sec		= timeNow->tm_sec;
	date 	= timeNow->tm_mday;
	month 	= 1 + timeNow->tm_mon;
	year 	= 1900 + timeNow->tm_year;
	
	//Initialize locking file
	region_to_lock.l_type = F_WRLCK;//Exclusive lock
	region_to_lock.l_whence = SEEK_SET;//Lock from the beginning of the file
	file_desc = open(fullPath, O_RDWR | O_CREAT | O_APPEND, 0666);
	if (!file_desc)
	{
		TraceLog(ERROR_LEVEL, "Cannot open file '%s' to read or we don't have permission to create\n", fullPath);
		return -1;
	}
	res = fcntl(file_desc, F_SETLKW, &region_to_lock);
	if (res == -1) 
	{
		fprintf(stderr, "Failed to lock region \n");
	}
	sprintf(byte_write,"%d-%.2d-%.2d %.2d:%.2d:%.2d - PM \t VERSION: %s \n", year, month, date, hour, min, sec, VERSION);
	write(file_desc, byte_write, strlen(byte_write));
	close(file_desc);
	
	return 0;
}
