/*****************************************************************************
**	Project:		Equity Arbitrage Application
**	Filename:		configuration.c
**	Description:	This file contains function definitions that were declared
					in configuration.h	
**	Author:			Luan Vo-Kinh
**	First created:	30-August-2010
**	Last updated:	-----------
*****************************************************************************/

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include <pthread.h>

#include "utility.h"
#include "raw_data_mgmt.h"
#include "order_mgmt_proc.h"
#include "nasdaq_rash_data.h"
#include "nasdaq_rash_proc.h"
#include "arca_direct_proc.h"
#include "nasdaq_book_proc.h"
#include "arca_book_proc.h"
#include "configuration.h"
#include "bbo_mgmt.h"

/****************************************************************************
** Global variables definition
****************************************************************************/
// File name of raw data profix
char fileNamePrefix[MAX_ORDER_CONNECTIONS][MAX_LINE_LEN];

// Trader Tools information
t_ASCollection asCollection;

// Ignore symbol list
t_IgnoreStockList IgnoreStockList;

char symbolIgnoreStatus[MAX_STOCK_SYMBOL];

t_OrderStatus orderStatusMgmt[MAX_ORDER_CONNECTIONS];

int clientOrderID;

pthread_mutex_t nextOrderID_Mutex;

t_BookStatus bookStatusMgmt[MAX_BOOK_CONNECTIONS];

t_BookStatus CTS_StatusMgmt;
t_CTS_Feed_Conf CTS_Feed_Conf;

t_BookStatus CQS_StatusMgmt;
t_CQS_Feed_Conf CQS_Feed_Conf;

// For UQDF
t_BookStatus UQDF_StatusMgmt;
t_UQDF_Conf UQDF_Conf;

t_BookStatus UTDF_StatusMgmt;
t_UTDF_Conf UTDF_Conf;

int maxMilisecondToSleep = 200;
int maxShareNeedSleep = 10000;

t_TradingAccount TradingAccount;
extern t_DBLMgmt DBLMgmt;
/****************************************************************************
** Function declarations
****************************************************************************/
/****************************************************************************
- Function name:	InitializeFileNamePrefix
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int InitializeFileNamePrefix(void)
{
	strcpy(fileNamePrefix[NASDAQ_RASH_INDEX], "nasdaq_rash");
	strcpy(fileNamePrefix[ARCA_DIRECT_INDEX], "arca_direct");

	return SUCCESS;
}

/****************************************************************************
- Function name:	InitializeIgnoreStockList
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int InitializeIgnoreStockList()
{
	IgnoreStockList.countStock = 0;

	memset(IgnoreStockList.stockList, 0, MAX_IGNORE_SYMBOL * SYMBOL_LEN);

	memset(symbolIgnoreStatus, ENABLE, MAX_STOCK_SYMBOL);

	return SUCCESS;
}

/****************************************************************************
- Function name:	InitializeASInfo
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int InitializeAggregationServerInfo(void)
{
	asCollection.config.port = -1;
		
	asCollection.connection.status = DISCONNECTED;
	asCollection.connection.socket = -1;
	pthread_mutex_init(&asCollection.connection.socketMutex, NULL);

	Init_ARCA_DIRECT_DataStructure();
	Initialize_NASDAQ_RASH();

	// ECN Orders connection status
	memset(&asCollection.orderConnection, 0, sizeof(t_ConnectionStatus) * MAX_ORDER_CONNECTIONS);

	// ECN Books connection status
	memset(&asCollection.bookConnection, 0, sizeof(t_ConnectionStatus) * MAX_BOOK_CONNECTIONS);

	// ECN global configuration
	memset(&asCollection.globalConf, 0, sizeof(t_PMGlobalConf));

	// Current status
	asCollection.currentStatus.tradingStatus = DISABLED;

	pthread_mutex_init(&nextOrderID_Mutex, NULL);

	return 0;
}

/****************************************************************************
- Function name:	LoadAggregationServerInfo
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int LoadAggregationServerConf(void)
{
	// Get full raw data path
	char fullPath[MAX_PATH_LEN] = "\0";
	
	if (GetFullConfigPath(FILE_NAME_OF_AS_CONF, CONF_MISC, fullPath) == NULL_POINTER)
	{
		TraceLog(ERROR_LEVEL, "Have some problem in get full raw data path operation (%s)\n", FILE_NAME_OF_AS_CONF);

		return ERROR;
	}

	TraceLog(DEBUG_LEVEL, "Load Trader Tool config: %s\n", fullPath);

	//Try to open Trader Tool configuration file
	//Please be aware of the requirements for the configuration file here
	FILE *fileDesc = fopen(fullPath, "r");
	if (fileDesc == NULL)
	{
		TraceLog(ERROR_LEVEL, "Cannot open file '%s' to read or we don't have permission to create\n", fullPath);

		return ERROR;
	}
	else
	{
		//Buffer to store each line in the configuration file
		char line[MAX_PATH_LEN];

		//Number of lines read from the configuration file
		int  count = 0;

		//Clear the buffer pointed to by line
		memset(line, 0, MAX_PATH_LEN);

		//Get each line from the configuration file
		while (fgets(line, MAX_PATH_LEN, fileDesc) != NULL) 
		{
			if ((line[0] != '#') && (line[0] > 32))
			{
				count ++;

				if (count == 1)
				{
					asCollection.config.port = atoi(line);
				}
			}

			//Clear the buffer pointed to by line
			memset(line, 0, MAX_PATH_LEN);
		}

		//Close the file
		fclose(fileDesc);		

		if (count == 1)
		{
			//Indicate a successful operation
			TraceLog(DEBUG_LEVEL, "Listen Aggregation Server on the port = %d\n", asCollection.config.port);

			return SUCCESS;
		}
		else
		{
			//Configuration file is invalid!
			TraceLog(ERROR_LEVEL, "Invalid configuration file '%s'\n", fullPath);

			return ERROR;
		}		
	}
}

/****************************************************************************
- Function name:	SaveASConf
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int SaveASConf()
{
	// Get full raw data path
	char fullPath[MAX_PATH_LEN] = "\0";
	
	if (GetFullConfigPath(FILE_NAME_OF_AS_CONF, CONF_MISC, fullPath) == NULL_POINTER)
	{
		TraceLog(ERROR_LEVEL, "Have some problem in get full raw data path operation (%s)\n", FILE_NAME_OF_AS_CONF);

		return ERROR;
	}

	TraceLog(DEBUG_LEVEL, "Set Aggregation Server config: %s\n", fullPath);
	TraceLog(DEBUG_LEVEL, "Listen Aggregation Server on the port = %d\n", asCollection.config.port);

	//Try to open Trader Tool configuration file
	//Please be aware of the requirements for the configuration file here
	FILE *fileDesc = fopen(fullPath, "w+");
	if (fileDesc == NULL)
	{
		TraceLog(ERROR_LEVEL, "Cannot open file '%s' to read or we don't have permission to create\n", fullPath);

		return ERROR;
	}
	else
	{
		/*
		# Port: Which port will Position Manager listen on? This value\n");
		# fprintf(fileDesc, "# must be in range 1024 and 65535
		2340
		*/
		fprintf(fileDesc, "# Port: Which port will Position Manager listen on? This value\n");
		fprintf(fileDesc, "# must be in range 1024 and 65535\n");
		fprintf(fileDesc, "%d\n", asCollection.config.port);
		
		fflush(fileDesc);

		//Close the file
		fclose(fileDesc);		
		
		TraceLog(DEBUG_LEVEL, "Configuration of Aggregation Server is saved successfully!\n");		

		return SUCCESS;
	}

	return ERROR;
}

/****************************************************************************
- Function name:	LoadPMGlobalConf
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int LoadPMGlobalConf(void)
{
	// Get full raw data path
	char fullPath[MAX_PATH_LEN] = "\0";
	
	if (GetFullConfigPath(FILE_NAME_OF_PM_GLOBAL_CONF, CONF_MISC, fullPath) == NULL_POINTER)
	{
		TraceLog(ERROR_LEVEL, "Have some problem in get full raw data path operation (%s)\n", FILE_NAME_OF_PM_GLOBAL_CONF);

		return ERROR;
	}

	TraceLog(DEBUG_LEVEL, "Load PM global config: %s\n", fullPath);

	//Try to open global configuration file
	//Please be aware of the requirements for the configuration file here
	FILE *fileDesc = fopen(fullPath, "r");
	if (fileDesc == NULL)
	{
		TraceLog(ERROR_LEVEL, "Cannot open file '%s' to read or we don't have permission to create\n", fullPath);

		return ERROR;
	}
	else
	{
		//Buffer to store each line in the configuration file
		char line[MAX_PATH_LEN];

		//Number of lines read from the configuration file
		int  count = 0;

		//Clear the buffer pointed to by line
		memset(line, 0, MAX_PATH_LEN);

		//Get each line from the configuration file
		while (fgets(line, MAX_PATH_LEN, fileDesc) != NULL) 
		{
			if ((line[0] != '#') && (line[0] > 32))
			{
				count ++;

				if (count == 1)
				{
					asCollection.globalConf.autoSpread = atof(line);
				}
				else if (count == 2)
				{
					asCollection.globalConf.maxSpread = atof(line);
				}
				else if (count == 3)
				{
					asCollection.globalConf.maxLoserSpread = atof(line);
				}
				else if (count == 4)
				{
					asCollection.globalConf.profitSpread = atof(line);
				}
				else if (count == 5)
				{
					asCollection.globalConf.maxProfitSpread = atof(line);
				}
				else if (count == 6)
				{
					asCollection.globalConf.bigLoserAmount = atof(line);
				}
				else if (count == 7)
				{
					asCollection.globalConf.bigLoserSpread = atof(line);
				}
				else if (count == 8)
				{
					asCollection.globalConf.maxBigLoserSpread = atof(line);
				}
				else if (count == 9) 
				{
					asCollection.globalConf.fm_maxSpread = atof(line);
				}
				else if (count == 10)
				{
					asCollection.globalConf.fm_maxLoserSpread = atof(line);
				}
				else if (count == 11)
				{
					asCollection.globalConf.fm_maxBigLoserSpread = atof(line);
				}
				else if(count == 12)
				{
					asCollection.globalConf.highExecutionRate = atoi(line);
				}
				else if(count == 13)
				{
					asCollection.globalConf.movingAveragePeriod = atoi(line);
				}
				else if (count == 14) 
				{
					maxShareNeedSleep = atoi(line);
				}
				else if (count == 15)
				{
					maxMilisecondToSleep = atoi(line);
				}
			}

			//Clear the buffer pointed to by line
			memset(line, 0, MAX_PATH_LEN);
		}

		//Close the file
		fclose(fileDesc);

		if (count == 15)
		{
			//Indicate a successful operation
			TraceLog(DEBUG_LEVEL, "Auto spread = %lf\n", asCollection.globalConf.autoSpread);
			TraceLog(DEBUG_LEVEL, "Maximum spread = %lf\n", asCollection.globalConf.maxSpread);
			TraceLog(DEBUG_LEVEL, "Maximum loser spread = %lf\n", asCollection.globalConf.maxLoserSpread);
			TraceLog(DEBUG_LEVEL, "Profit spread = %lf\n", asCollection.globalConf.profitSpread);
			TraceLog(DEBUG_LEVEL, "Maximum profit spread = %lf\n", asCollection.globalConf.maxProfitSpread);
			TraceLog(DEBUG_LEVEL, "Big loser amount = %lf\n", asCollection.globalConf.bigLoserAmount);
			TraceLog(DEBUG_LEVEL, "Big loser spread = %lf\n", asCollection.globalConf.bigLoserSpread);
			TraceLog(DEBUG_LEVEL, "Maximum big loser spread = %lf\n", asCollection.globalConf.maxBigLoserSpread);
			TraceLog(DEBUG_LEVEL, "Fast market max spread = %lf\n", asCollection.globalConf.fm_maxSpread);
			TraceLog(DEBUG_LEVEL, "Fast market max loser spread = %lf\n", asCollection.globalConf.fm_maxLoserSpread);
			TraceLog(DEBUG_LEVEL, "Fast market max big loser spread = %lf\n", asCollection.globalConf.fm_maxBigLoserSpread);
			TraceLog(DEBUG_LEVEL, "High execution rate = %d\n", asCollection.globalConf.highExecutionRate);
			TraceLog(DEBUG_LEVEL, "Moving average period (in secs) = %d\n", asCollection.globalConf.movingAveragePeriod);
			TraceLog(DEBUG_LEVEL, "Maximum share to sleep = %d\n", maxShareNeedSleep);
			TraceLog(DEBUG_LEVEL, "Miliseconds for sleep between orders = %d\n", maxMilisecondToSleep);
			return SUCCESS;
		}
		else
		{
			//Configuration file is invalid!
			TraceLog(ERROR_LEVEL, "Invalid configuration file '%s'\n", fullPath);

			return ERROR;
		}
	}
}

/****************************************************************************
- Function name:	SavePMGlobalConf
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int SavePMGlobalConf(void)
{
	// Get full raw data path
	char fullPath[MAX_PATH_LEN] = "\0";
	
	if (GetFullConfigPath(FILE_NAME_OF_PM_GLOBAL_CONF, CONF_MISC, fullPath) == NULL_POINTER)
	{
		TraceLog(ERROR_LEVEL, "Have some problem in get full raw data path operation (%s)\n", FILE_NAME_OF_PM_GLOBAL_CONF);

		return ERROR;
	}

	TraceLog(DEBUG_LEVEL, "Set global config of PM: %s\n", fullPath);
	TraceLog(DEBUG_LEVEL, "Auto spread = %lf\n", asCollection.globalConf.autoSpread);
	TraceLog(DEBUG_LEVEL, "Maximum spread = %lf\n", asCollection.globalConf.maxSpread);
	TraceLog(DEBUG_LEVEL, "Maximum loser spread = %lf\n", asCollection.globalConf.maxLoserSpread);
	TraceLog(DEBUG_LEVEL, "Profit spread = %lf\n", asCollection.globalConf.profitSpread);
	TraceLog(DEBUG_LEVEL, "Maximum profit spread = %lf\n", asCollection.globalConf.maxProfitSpread);
	TraceLog(DEBUG_LEVEL, "Big loser amount = %lf\n", asCollection.globalConf.bigLoserAmount);
	TraceLog(DEBUG_LEVEL, "Big loser spread = %lf\n", asCollection.globalConf.bigLoserSpread);
	TraceLog(DEBUG_LEVEL, "Maximum big loser spread = %lf\n", asCollection.globalConf.maxBigLoserSpread);
	TraceLog(DEBUG_LEVEL, "Fast market max spread = %lf\n", asCollection.globalConf.fm_maxSpread);
	TraceLog(DEBUG_LEVEL, "Fast market max loser spread = %lf\n", asCollection.globalConf.fm_maxLoserSpread);
	TraceLog(DEBUG_LEVEL, "Fast market max big loser spread = %lf\n", asCollection.globalConf.fm_maxBigLoserSpread);
	TraceLog(DEBUG_LEVEL, "High Execution Rate = %d\n", asCollection.globalConf.highExecutionRate);
	TraceLog(DEBUG_LEVEL, "Moving Average Period = %d\n", asCollection.globalConf.movingAveragePeriod);
	//Try to open global configuration file
	//Please be aware of the requirements for the configuration file here
	FILE *fileDesc = fopen(fullPath, "w+");
	if (fileDesc == NULL)
	{
		TraceLog(ERROR_LEVEL, "Cannot open file '%s' to read or we don't have permission to create\n", fullPath);

		return ERROR;
	}
	else
	{
		/*
		# Maximum stuck positions: Default is 5
		6
		# Maximum consecutive losers: Default is 5
		6
		*/
		fprintf(fileDesc, "# Auto spread\n");
		fprintf(fileDesc, "%lf\n", asCollection.globalConf.autoSpread);
		fprintf(fileDesc, "# Maximum spread\n");
		fprintf(fileDesc, "%lf\n", asCollection.globalConf.maxSpread);
		fprintf(fileDesc, "# Maximum loser spread\n");
		fprintf(fileDesc, "%lf\n", asCollection.globalConf.maxLoserSpread);
		fprintf(fileDesc, "# Profit spread\n");
		fprintf(fileDesc, "%lf\n", asCollection.globalConf.profitSpread);
		fprintf(fileDesc, "# Maximum profit spread\n");
		fprintf(fileDesc, "%lf\n", asCollection.globalConf.maxProfitSpread);
		fprintf(fileDesc, "# Big loser amount\n");
		fprintf(fileDesc, "%lf\n", asCollection.globalConf.bigLoserAmount);
		fprintf(fileDesc, "# Big loser spread\n");
		fprintf(fileDesc, "%lf\n", asCollection.globalConf.bigLoserSpread);
		fprintf(fileDesc, "# Maximum big loser spread\n");
		fprintf(fileDesc, "%lf\n", asCollection.globalConf.maxBigLoserSpread);
		fprintf(fileDesc, "# Fast market max spread\n");
		fprintf(fileDesc, "%lf\n", asCollection.globalConf.fm_maxSpread);
		fprintf(fileDesc, "# Fast market max loser spread\n");
		fprintf(fileDesc, "%lf\n", asCollection.globalConf.fm_maxLoserSpread);
		fprintf(fileDesc, "# Fast market max big loser spread\n");
		fprintf(fileDesc, "%lf\n", asCollection.globalConf.fm_maxBigLoserSpread);
		fprintf(fileDesc, "# High execution rate\n");
		fprintf(fileDesc, "%d\n", asCollection.globalConf.highExecutionRate);
		fprintf(fileDesc, "# Moving average period in secs\n");
		fprintf(fileDesc, "%d\n", asCollection.globalConf.movingAveragePeriod);
		fprintf(fileDesc, "# Maximum share to sleep\n");
		fprintf(fileDesc, "%d\n", maxShareNeedSleep);
		fprintf(fileDesc, "# Miliseconds for sleep between orders\n");
		fprintf(fileDesc, "%d\n", maxMilisecondToSleep);
		
		fflush(fileDesc);

		//Close the file
		fclose(fileDesc);
		
		TraceLog(DEBUG_LEVEL, "Global configuration of Position Manager is saved successfully!\n");

		return SUCCESS;
	}

	return ERROR;
}

/****************************************************************************
- Function name:	LoadPMOrdersConf
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int LoadPMOrdersConf(void)
{
	// Load NASDAQ RASH configuration
	if (LoadNASDAQ_RASHConf("nasdaq_rash_conf", &NASDAQ_RASH_Config) == ERROR)
	{
		return ERROR;
	}

	// Load ARCA DIRECT configuration
	if (LoadARCA_DIRECTConf("arca_direct_conf", &ARCA_DIRECT_Config) == ERROR)
	{
		return ERROR;
	}
	
	// Load next order id from file
	clientOrderID = LoadClientOrderID("next_order_id");

	TraceLog(DEBUG_LEVEL, "Client Order ID = %d\n", clientOrderID);

	if (clientOrderID == ERROR)
	{
		return ERROR;
	}	
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	LoadNASDAQ_RASHConf
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int LoadNASDAQ_RASHConf(char *fileName, void *ECNOrderConf)
{
	t_NASDAQ_RASH_Config *orderConf = (t_NASDAQ_RASH_Config *)ECNOrderConf;

	// Get full raw data path
	char fullPath[MAX_PATH_LEN] = "\0";
	
	if (GetFullConfigPath(fileName, CONF_ORDER, fullPath) == NULL_POINTER)
	{
		TraceLog(ERROR_LEVEL, "Have some problem in get full raw data path operation (%s)\n", fileName);

		return ERROR;
	}

	TraceLog(DEBUG_LEVEL, "Load NASDAQ RASH config: %s\n", fullPath);

	//Try to open ignore stock file
	//Please be aware of the requirements for the configuration file here
	FILE *fileDesc = fopen(fullPath, "r+");
	if (fileDesc == NULL)
	{
		TraceLog(ERROR_LEVEL, "Cannot open file '%s' to read or we don't have permission to create\n", fullPath);

		return ERROR;
	}
	else
	{
		//Buffer to store each line in the configuration file
		char line[MAX_LINE_LEN];

		//Number of lines read from the configuration file
		int  count = 0;

		//Clear the buffer pointed to by line
		memset(line, 0, MAX_LINE_LEN);

		//Get each line from the configuration file
		while (fgets(line, MAX_LINE_LEN, fileDesc) != NULL) 
		{
			if ((line[0] != '#') && (line[0] > 32))
			{
				count ++;

				if (count == 1)
				{
					RemoveCharacters(line, strlen(line));

					memset(orderConf->ipAddress, 0, MAX_LINE_LEN);
					strncpy(orderConf->ipAddress, line, strlen(line));
				}
				else if (count == 2)
				{
					orderConf->port = atoi(line);
				}
				else if (count == 3)
				{
					RemoveCharacters(line, strlen(line));

					memset(orderConf->userName, 0, MAX_LINE_LEN);
					strncpy(orderConf->userName, line, strlen(line));
				}
				else if (count == 4)
				{
					RemoveCharacters(line, strlen(line));

					memset(orderConf->password, 0, MAX_LINE_LEN);
					strncpy(orderConf->password, line, strlen(line));
				}
			}

			//Clear the buffer pointed to by line
			memset(line, 0, MAX_LINE_LEN);
		}

		//Close the file
		fclose(fileDesc);
		
		if (count == 4)
		{
			//Indicate a successful operation
			TraceLog(DEBUG_LEVEL, "ipAddress = %s\n", orderConf->ipAddress);
			TraceLog(DEBUG_LEVEL, "port = %d\n", orderConf->port);
			TraceLog(DEBUG_LEVEL, "userName = %s\n", orderConf->userName);
			TraceLog(DEBUG_LEVEL, "password = %s\n", orderConf->password);

			return SUCCESS;
		}
		else
		{
			//Configuration file is invalid!
			TraceLog(ERROR_LEVEL, "Invalid configuration file '%s'\n", fullPath);

			return ERROR;
		}
	}
}

/****************************************************************************
- Function name:	LoadARCA_DIRECTConf
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int LoadARCA_DIRECTConf(char *fileName, void *ECNOrderConf)
{
	t_ARCA_DIRECT_Config *orderConf = (t_ARCA_DIRECT_Config *)ECNOrderConf;

	// Get full raw data path
	char fullPath[MAX_PATH_LEN] = "\0";
	
	if (GetFullConfigPath(fileName, CONF_ORDER, fullPath) == NULL_POINTER)
	{
		TraceLog(ERROR_LEVEL, "Have some problem in get full raw data path operation (%s)\n", fileName);

		return ERROR;
	}

	TraceLog(DEBUG_LEVEL, "Load ARCA DIRECT config: %s\n", fullPath);

	//Try to open ignore stock file
	//Please be aware of the requirements for the configuration file here
	FILE *fileDesc = fopen(fullPath, "r+");
	if (fileDesc == NULL)
	{
		TraceLog(ERROR_LEVEL, "Cannot open file '%s' to read or we don't have permission to create\n", fullPath);

		return ERROR;
	}
	else
	{
		//Buffer to store each line in the configuration file
		char line[MAX_LINE_LEN];

		//Number of lines read from the configuration file
		int  count = 0;

		memset(line, 0, MAX_LINE_LEN);

		//Get each line from the configuration file
		while (fgets(line, MAX_LINE_LEN, fileDesc) != NULL) 
		{
			if (!strchr(line, '#'))
			{	
				RemoveCharacters(line, strlen(line));
				if (strlen(line) == 0)
				{
					continue;
				}
				
				count++;
				
				if (count == 1)
				{
					strncpy(orderConf->ipAddress, line, MAX_LINE_LEN);
				}
				else if (count == 2)
				{
					orderConf->port = atoi(line);
				}
				else if (count == 3)
				{
					strncpy(orderConf->userName, line, MAX_LINE_LEN);
				}
				else if (count == 4)
				{
					strncpy(orderConf->password, line, MAX_LINE_LEN);
				}
				else if (count == 5)
				{
					strncpy(orderConf->compGroupID, line, MAX_COMP_GROUP_ID_LEN);
				}
				else if (count == 6)
				{
					orderConf->autoCancelOnDisconnect = line[0] - 48;
				}
				else if (count > 6)
				{
					fclose(fileDesc);
					TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format. Trade Server can not continue.\n", fullPath);
					sleep(3); exit(0);
					return ERROR;
				}
				
				memset(line, 0, MAX_LINE_LEN);
			}
		}

		//Close the file
		fclose(fileDesc);

		if (count == 6)
		{
			//Indicate a successful operation
			TraceLog(DEBUG_LEVEL, "ipAddress = %s\n", orderConf->ipAddress);
			TraceLog(DEBUG_LEVEL, "port = %d\n", orderConf->port);
			TraceLog(DEBUG_LEVEL, "userName = %s\n", orderConf->userName);
			TraceLog(DEBUG_LEVEL, "password = %s\n", orderConf->password);
			TraceLog(DEBUG_LEVEL, "compGroupID = %s\n", orderConf->compGroupID);
			TraceLog(DEBUG_LEVEL, "auto-cancel-on-disconnect = %d\n", orderConf->autoCancelOnDisconnect);

			return SUCCESS;
		}
		else
		{
			//Configuration file is invalid!
			TraceLog(ERROR_LEVEL, "Invalid configuration file '%s'\n", fullPath);

			return ERROR;
		}
	}
}

/****************************************************************************
- Function name:	ProcessSavePMOrderConf
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessSavePMOrderConf(int ECNIndex)
{
	switch (ECNIndex)
	{
		// Save NASDAQ RASH configuration
		case NASDAQ_RASH_INDEX:
			if (SaveNASDAQ_RASHConf("nasdaq_rash_conf", &NASDAQ_RASH_Config) == ERROR)
			{
				return ERROR;
			}
			break;

		// Save ARCA DIRECT configuration
		case ARCA_DIRECT_INDEX:
			if (SaveARCA_DIRECTConf("arca_direct_conf", &ARCA_DIRECT_Config) == ERROR)
			{
				return ERROR;
			}
			break;
	}

	return SUCCESS;
}

/****************************************************************************
- Function name:	SaveNASDAQ_RASHConf
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int SaveNASDAQ_RASHConf(char *fileName, void *ECNOrderConf)
{
	t_NASDAQ_RASH_Config *orderConf = (t_NASDAQ_RASH_Config *)ECNOrderConf;

	// Get full raw data path
	char fullPath[MAX_PATH_LEN] = "\0";
	
	if (GetFullConfigPath(fileName, CONF_ORDER, fullPath) == NULL_POINTER)
	{
		TraceLog(ERROR_LEVEL, "Have some problem in get full raw data path operation (%s)\n", fileName);

		return ERROR;
	}

	TraceLog(DEBUG_LEVEL, "Save NASDAQ RASH configuration: %s\n", fullPath);
	TraceLog(DEBUG_LEVEL, "ipAddress = %s\n", orderConf->ipAddress);
	TraceLog(DEBUG_LEVEL, "port = %d\n", orderConf->port);
	TraceLog(DEBUG_LEVEL, "userName = %s\n", orderConf->userName);
	TraceLog(DEBUG_LEVEL, "password = %s\n", orderConf->password);

	//Try to open ignore stock file
	//Please be aware of the requirements for the configuration file here
	FILE *fileDesc = fopen(fullPath, "w+");
	if (fileDesc == NULL)
	{
		TraceLog(ERROR_LEVEL, "Cannot open file '%s' to read or we don't have permission to create\n", fullPath);

		return ERROR;
	}
	else
	{
		/*
		#IP Address of the NASDAQ RASH Server
		172.18.17.233
		#Port number of the NASDAQ RASH Server to connect to
		1111
		#userName to log in to NASDAQ RASH Server
		PFS01
		#Password to log in to NASDAQ RASH Server
		P123456789
		*/
		fprintf(fileDesc, "# IP Address of the NASDAQ RASH Server\n");
		fprintf(fileDesc, "%s\n", orderConf->ipAddress);
		fprintf(fileDesc, "# Port number of the NASDAQ RASH Server to connect to\n");
		fprintf(fileDesc, "%d\n", orderConf->port);
		fprintf(fileDesc, "# userName to log in to NASDAQ RASH Server\n");
		fprintf(fileDesc, "%s\n", orderConf->userName);
		fprintf(fileDesc, "# Password to log in to NASDAQ RASH Server\n");
		fprintf(fileDesc, "%s\n", orderConf->password);
		
		fflush(fileDesc);
		
		//Close the file
		fclose(fileDesc);
		
		TraceLog(DEBUG_LEVEL, "Order configuration of PM is saved successfully!\n");
			
		return SUCCESS;
	}
}

/****************************************************************************
- Function name:	SaveARCA_DIRECTConf
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int SaveARCA_DIRECTConf(char *fileName, void *ECNOrderConf)
{
	t_ARCA_DIRECT_Config *orderConf = (t_ARCA_DIRECT_Config *)ECNOrderConf;

	// Get full raw data path
	char fullPath[MAX_PATH_LEN] = "\0";
	
	if (GetFullConfigPath(fileName, CONF_ORDER, fullPath) == NULL_POINTER)
	{
		TraceLog(ERROR_LEVEL, "Have some problem in get full raw data path operation (%s)\n", fileName);

		return ERROR;
	}

	TraceLog(DEBUG_LEVEL, "Save ARCA DIRECT configuration: %s\n", fullPath);
	TraceLog(DEBUG_LEVEL, "ipAddress = %s\n", orderConf->ipAddress);
	TraceLog(DEBUG_LEVEL, "port = %d\n", orderConf->port);
	TraceLog(DEBUG_LEVEL, "userName = %s\n", orderConf->userName);
	TraceLog(DEBUG_LEVEL, "password = %s\n", orderConf->password);
	TraceLog(DEBUG_LEVEL, "auto-cancel-on-disconnect = %d\n", orderConf->autoCancelOnDisconnect);
	
	//Try to open ignore stock file
	//Please be aware of the requirements for the configuration file here
	FILE *fileDesc = fopen(fullPath, "w+");
	if (fileDesc == NULL)
	{
		TraceLog(ERROR_LEVEL, "Cannot open file '%s' to read or we don't have permission to create\n", fullPath);

		return ERROR;
	}
	else
	{
		fprintf(fileDesc, "#IP Address\n");
		fprintf(fileDesc, "%s\n", orderConf->ipAddress);
		fprintf(fileDesc, "#Port\n");
		fprintf(fileDesc, "%d\n", orderConf->port);
		fprintf(fileDesc, "#UserName\n");
		fprintf(fileDesc, "%s\n", orderConf->userName);
		fprintf(fileDesc, "#Password\n");
		fprintf(fileDesc, "%s\n", orderConf->password);
		fprintf(fileDesc, "#Company Group ID\n");
		fprintf(fileDesc, "%s\n", orderConf->compGroupID);
		fprintf(fileDesc, "#Auto Cancel on Disconnect: 0 or 1\n");
		fprintf(fileDesc, "%d\n", orderConf->autoCancelOnDisconnect);
		
		fflush(fileDesc);
		
		//Close the file
		fclose(fileDesc);
		
		TraceLog(DEBUG_LEVEL, "Order configuration of PM is saved successfully!\n");
			
		return SUCCESS;
	}
}

/****************************************************************************
- Function name:	LoadIgnoreStockList
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int LoadIgnoreStockList(void)
{
	// Get full raw data path
	char fullPath[MAX_PATH_LEN] = "\0";
	
	if (GetFullConfigPath(FILE_NAME_OF_IGNORE_STOCK_LIST, CONF_SYMBOL, fullPath) == NULL_POINTER)
	{
		TraceLog(ERROR_LEVEL, "Have some problem in get config path operation (%s)\n", FILE_NAME_OF_IGNORE_STOCK_LIST);

		return ERROR;
	}

	TraceLog(DEBUG_LEVEL, "Load ignored symbol list: %s\n", fullPath);

	//Try to open ignore stock file
	//Please be aware of the requirements for the configuration file here
	FILE *fileDesc = fopen(fullPath, "r+");
	if (fileDesc == NULL)
	{
		TraceLog(ERROR_LEVEL, "Cannot open file '%s' to read or we don't have permission to create\n", fullPath);

		return ERROR;
	}
	else
	{
		// Reset number of symbol
		IgnoreStockList.countStock = 0;

		//Get each line from the configuration file
		while (fread(&IgnoreStockList.stockList[IgnoreStockList.countStock], SYMBOL_LEN, 1, fileDesc) == 1) 
		{
			// Increase number of stock
			IgnoreStockList.countStock ++;
		}

		//Close the file
		fclose(fileDesc);

		TraceLog(DEBUG_LEVEL, "Count symbol = %d\n", IgnoreStockList.countStock);	

		return IgnoreStockList.countStock;
	}
}

/****************************************************************************
- Function name:	SaveIgnoreStockList
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int SaveIgnoreStockList(void)
{
	// Get full raw data path
	char fullPath[MAX_PATH_LEN] = "\0";
	
	if (GetFullConfigPath(FILE_NAME_OF_IGNORE_STOCK_LIST, CONF_SYMBOL, fullPath) == NULL_POINTER)
	{
		TraceLog(ERROR_LEVEL, "Have some problem in get full config path operation (%s)\n", FILE_NAME_OF_IGNORE_STOCK_LIST);

		return ERROR;
	}

	TraceLog(DEBUG_LEVEL, "Save ignore stock list: %s\n", fullPath);

	//Try to open ignore stock file
	//Please be aware of the requirements for the configuration file here
	FILE *fileDesc = fopen(fullPath, "w+");
	if (fileDesc == NULL)
	{
		TraceLog(ERROR_LEVEL, "Cannot open file '%s' to read or we don't have permission to create\n", fullPath);

		return ERROR;
	}
	else
	{
		TraceLog(DEBUG_LEVEL, "Number of symbol = %d\n", IgnoreStockList.countStock);

		int i;
		
		for (i = 0; i < IgnoreStockList.countStock; i++)
		{
			if (fwrite(&IgnoreStockList.stockList[i], SYMBOL_LEN, 1, fileDesc) != 1)
			{
				TraceLog(ERROR_LEVEL, "Cannot save ignore symbol list to file\n");

				fclose(fileDesc);
				return ERROR;
			}
		}
		
		fflush(fileDesc);

		//Close the file
		fclose(fileDesc);	

		TraceLog(DEBUG_LEVEL, "Ignore symbol list is saved successfully!\n");

		return IgnoreStockList.countStock;
	}
}

/****************************************************************************
- Function name:	UpdateStatusSymbolIgnoredList
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int UpdateStatusSymbolIgnoredList(void)
{
	int i, symbolIndex;
		
	for (i = 0; i < IgnoreStockList.countStock; i++)
	{
		symbolIndex = GetStockSymbolIndex(IgnoreStockList.stockList[i]);

		if (symbolIndex != -1)
		{
			symbolIgnoreStatus[symbolIndex] = DISABLE;
		}
		else
		{
			// Try to add it to Symbol List
			symbolIndex = UpdateStockSymbolIndex(IgnoreStockList.stockList[i]);

			if ( symbolIndex != ERROR)
			{
				symbolIgnoreStatus[symbolIndex] = DISABLE;
			}
			else
			{
				TraceLog(ERROR_LEVEL, "PM doesn't support the symbol %.8s\n", IgnoreStockList.stockList[i]);
				
				return ERROR;
			}
		}
	}

	return SUCCESS;
}

/****************************************************************************
- Function name:	UpdateStatusSymbolIgnored
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int UpdateStatusSymbolIgnored(char symbol[SYMBOL_LEN], char status)
{
	int symbolIndex;		

	symbolIndex = GetStockSymbolIndex(symbol);

	if (symbolIndex != -1)
	{
		symbolIgnoreStatus[symbolIndex] = status;
	}
	else
	{
		// Try to add it to Symbol List
		symbolIndex = UpdateStockSymbolIndex(symbol);

		if ( symbolIndex != ERROR)
		{
			symbolIgnoreStatus[symbolIndex] = status;
		}
		else
		{
			TraceLog(ERROR_LEVEL, "PM doesn't support the symbol %.8s\n", symbol);
			
			return ERROR;
		}
	}

	return SUCCESS;
}

/****************************************************************************
- Function name:	LoadSequenceNumber
- Input:			+ fileName
- Output:		+ configInfo
- Return:		+ Success or Failure
- Description:	+ The routine hides the following facts
				  - Check valid file name
				  - Load and check valid configuration information
				+ There are preconditions guaranteed to the routine
				  - Data structure to contain configuration information
					  must be allocated before it is passed to the routine
				+ The routine guarantees that the return value will 
					have a value of either
				  - Success
					or
				  - Failure
- Usage:			N/A
****************************************************************************/
int LoadSequenceNumber(const char *fileName, int type, void *configInfo)
{
	char fullPath[MAX_PATH_LEN];
	
	if (GetFullConfigPath(fileName, CONF_ORDER, fullPath) == NULL_POINTER)
	{
		return ERROR;
	}
	
	switch (type)
	{
		case NASDAQ_RASH_INDEX:
			return LoadIncomingSeqNum(fullPath, type, configInfo);
		case ARCA_DIRECT_INDEX:
			return LoadIncomingAndOutgoingSeqNum(fullPath, type, configInfo);
		default:
			return ERROR;			
	}
}

/****************************************************************************
- Function name:	LoadIncomingAndOutgoingSeqNum
- Input:			+ fullPath
				+ type
- Output:		+ configInfo
- Return:		+ Success or Failure
- Description:	+ The routine hides the following facts
				  - Check valid file name
				  - Load and check valid configuration information
				+ There are preconditions guaranteed to the routine
				  - Data structure to contain configuration information
					  must be allocated before it is passed to the routine
				+ The routine guarantees that the return value will 
					have a value of either
				  - Success
					or
				  - Failure
- Usage:			N/A
****************************************************************************/
int LoadIncomingAndOutgoingSeqNum(const char *fullPath, int type, void *configInfo)
{
	FILE *fileDesc = fopen(fullPath, "r");
	char tempBuffer[MAX_LINE_LEN];
	int fieldIndex = 0;
	
	if (fileDesc == NULL)
	{
		return ERROR;
	}
	
	while (fgets(tempBuffer, MAX_LINE_LEN, fileDesc) != NULL)
	{
		RemoveCharacters(tempBuffer, strlen(tempBuffer));

		// Skip comment lines
		if ((tempBuffer[0] == '#') || (tempBuffer[0] < USED_CHAR_CODE))
		{
			continue;
		}
		
		// Base on TYPE, we will cast to appropriate data type
		switch(type)	
		{
			case ARCA_DIRECT_INDEX:
				if (fieldIndex == 0)
				{
					((t_ARCA_DIRECT_Config *)configInfo)->incomingSeqNum = atoi(tempBuffer);
				}
				else if (fieldIndex == 1)
				{
					((t_ARCA_DIRECT_Config *)configInfo)->outgoingSeqNum = atoi(tempBuffer);
				}
				else
				{
					TraceLog(ERROR_LEVEL, "Invalid configuration file '%s'\n", fullPath);
					
					// Close file and release resource
					fclose(fileDesc);
				}
								
				break;
			default:
				TraceLog(ERROR_LEVEL, "Unsupported order type '%d'\n", type);
				fclose(fileDesc);
				return ERROR;
		}
		
		fieldIndex++;
	}
	
	// Close file and release resource
	fclose(fileDesc);
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	LoadIncomingSeqNum
- Input:			+ fullPath
				+ type
- Output:		+ configInfo
- Return:		+ Success or Failure
- Description:	+ The routine hides the following facts
				  - Check valid file name
				  - Load and check valid configuration information
				+ There are preconditions guaranteed to the routine
				  - Data structure to contain configuration information
					  must be allocated before it is passed to the routine
				+ The routine guarantees that the return value will 
					have a value of either
				  - Success
					or
				  - Failure
- Usage:			N/A
****************************************************************************/
int LoadIncomingSeqNum(const char *fullPath, int type, void *configInfo)
{
	FILE *fileDesc = fopen(fullPath, "r");
	char tempBuffer[MAX_LINE_LEN];
	int fieldIndex = 0;
	
	if (fileDesc == NULL)
	{
		return ERROR;
	}
	
	while (fgets(tempBuffer, MAX_LINE_LEN, fileDesc) != NULL)
	{
		RemoveCharacters(tempBuffer, strlen(tempBuffer));

		// Skip comment lines
		if ((tempBuffer[0] == '#') || (tempBuffer[0] < USED_CHAR_CODE))
		{
			continue;
		}
		
		// Base on TYPE, we will cast to appropriate data type
		switch(type)	
		{
			case NASDAQ_RASH_INDEX:
				if (fieldIndex == 0)
				{
					((t_NASDAQ_RASH_Config *)configInfo)->incomingSeqNum = atoi(tempBuffer);
				}
				else
				{
					TraceLog(ERROR_LEVEL, "Invalid configuration file '%s'\n", fullPath);
					
					// Close file and release resource
					fclose(fileDesc);
				}
				break;
			default:
				TraceLog(ERROR_LEVEL, "Unsupported order type '%d'\n", type);
				fclose(fileDesc);
				return ERROR;
		}
		
		fieldIndex++;
	}
	
	// Close file and release resource
	fclose(fileDesc);
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	SaveSeqNumToFile
- Input:			+ fileName
				+ type
				+ configInfo
- Output:		N/A
- Return:		Success or Failure
- Description:	+ The routine hides the following facts

				+ There are no preconditions guaranteed to the routine
				+ The routine guarantees that the status value will 
					have a value of either
				  - Success
					or
				  - Failure
- Usage:			N/A
****************************************************************************/
int SaveSeqNumToFile(const char *fileName, int type, void *configInfo)
{
	char fullPath[MAX_PATH_LEN];
	
	if (GetFullConfigPath(fileName, CONF_ORDER, fullPath) == NULL_POINTER)
	{
		return ERROR;
	}
	
	switch (type)
	{
		case NASDAQ_RASH_INDEX:
			return SaveIncomingSeqNumToFile(fullPath, type, configInfo);
		case ARCA_DIRECT_INDEX:
			return SaveIncomingAndOutgoingSeqNumToFile(fullPath, type, configInfo);
		default:			
			TraceLog(ERROR_LEVEL, "Unsupported order type '%d'\n", type);
			
			return ERROR;
	}
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	SaveIncomingAndOutgoingSeqNumToFile
- Input:			+ fileName
				+ type
				+ configInfo
- Output:		N/A
- Return:		Success or Failure
- Description:	+ The routine hides the following facts

				+ There are no preconditions guaranteed to the routine
				+ The routine guarantees that the status value will 
					have a value of either
				  - Success
					or
				  - Failure
- Usage:			N/A
****************************************************************************/
int SaveIncomingAndOutgoingSeqNumToFile(const char *fileName, int type, void *configInfo)
{
	FILE *fileDesc = fopen(fileName, "w");
	
	if (fileDesc == NULL)
	{
		TraceLog(ERROR_LEVEL, "Cannot open file '%s' to write\n", fileName);
		return ERROR;
	}
	else
	{
		// Truncate file content to zero offset
		if (truncate(fileName, 0) == ERROR)
		{
			return ERROR;
		}
		
		switch (type)
		{
			case ARCA_DIRECT_INDEX:
			
				fprintf(fileDesc, "#Incoming Sequence Number\n");
				fprintf(fileDesc, "%d\n", ((t_ARCA_DIRECT_Config *)configInfo)->incomingSeqNum);
				
				fprintf(fileDesc, "#Outgoing Sequence Number\n");
				fprintf(fileDesc, "%d\n", ((t_ARCA_DIRECT_Config *)configInfo)->outgoingSeqNum);
				
				fflush(fileDesc);
				fclose(fileDesc);
				break;
		}
	}
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	SaveIncomingSeqNumToFile
- Input:			+ fileName
				+ type
				+ configInfo
- Output:		N/A
- Return:		Success or Failure
- Description:	+ The routine hides the following facts

				+ There are no preconditions guaranteed to the routine
				+ The routine guarantees that the status value will 
					have a value of either
				  - Success
					or
				  - Failure
- Usage:			N/A
****************************************************************************/
int SaveIncomingSeqNumToFile(const char *fileName, int type, void *configInfo)
{
	FILE *fileDesc = fopen(fileName, "w");
	
	if (fileDesc == NULL)
	{
		TraceLog(ERROR_LEVEL, "Cannot open file '%s' to write\n", fileName);
		return ERROR;
	}
	else
	{
		// Truncate file content to zero offset
		if (truncate(fileName, 0) == ERROR)
		{
			return ERROR;
		}
		
		switch (type)
		{
			case NASDAQ_RASH_INDEX:
				fprintf(fileDesc, "#Incoming Sequence Number\n");
				fprintf(fileDesc, "%d\n", ((t_NASDAQ_RASH_Config *)configInfo)->incomingSeqNum);
				
				fflush(fileDesc);
				fclose(fileDesc);
				break;
		}
	}
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	LoadClientOrderID
- Input:			+ fileName
- Output:		N/A
- Return:		client order ID
- Description:	+ The routine hides the following facts
				  - Check valid file name
				  - Load and check valid configuration information
				+ There are no preconditions guaranteed to the routine
				+ The routine guarantees that the return value will 
					have a value of either
				  - Success
					or
				  - Failure
- Usage:			N/A
****************************************************************************/
int LoadClientOrderID(const char *fileName)
{
	char fullPath[MAX_PATH_LEN];
	
	if (GetFullConfigPath(fileName, CONF_MISC, fullPath) == NULL_POINTER)
	{
		return ERROR;
	}
	
	FILE *fileDesc = fopen(fullPath, "r");
	char tempBuffer[MAX_LINE_LEN];
	int tempClientOrderID = -1;
	
	if (fileDesc == NULL)
	{
		return ERROR;
	}

	//Number of lines read from the configuration file
	int count = 0;

	//Clear the buffer pointed to by line
	memset(tempBuffer, 0, MAX_LINE_LEN);
	
	while (fgets(tempBuffer, MAX_LINE_LEN, fileDesc) != NULL)
	{
		// Skip comment lines
		if (tempBuffer[0] != '#')
		{
			count ++;

			if (count == 1)
			{
				tempClientOrderID = atoi(tempBuffer);
			}
		}

		//Clear the buffer pointed to by line
		memset(tempBuffer, 0, MAX_LINE_LEN);
	}
	
	// Close file and release resource
	fclose(fileDesc);

	if (count == 1)
	{		
		return tempClientOrderID;
	}
	else
	{
		//Configuration file is invalid!
		TraceLog(ERROR_LEVEL, "Invalid configuration file '%s'\n", fullPath);

		return ERROR;
	}
}

/****************************************************************************
- Function name:	SaveClientOrderID
- Input:			+ fileName
- Output:		N/A
- Return:		client order ID
- Description:	+ The routine hides the following facts
				  - Check valid file name
				  - Load and check valid configuration information
				+ There are no preconditions guaranteed to the routine
				+ The routine guarantees that the return value will 
					have a value of either
				  - Success
					or
				  - Failure
- Usage:			N/A
****************************************************************************/
int SaveClientOrderID(const char *fileName, int clientOrderID)
{
	char fullPath[MAX_PATH_LEN];
	
	if (GetFullConfigPath(fileName, CONF_MISC, fullPath) == NULL_POINTER)
	{
		return ERROR;
	}
	
	FILE *fileDesc = fopen(fullPath, "w");
	
	if (fileDesc == NULL)
	{
		return ERROR;
	}
	
	fprintf(fileDesc, "%d\n", clientOrderID);
	
	// Close file and release resource
	fclose(fileDesc);
	
	return clientOrderID;
}

/****************************************************************************
- Function name:	LoadAllBooksConfig
- Input:			N/A
- Output:		N/A
- Return:		+ Success or Failure
- Description:	+ The routine hides the following facts
				  - load all books configuration 
				+ There are no preconditions guaranteed to the routine
				+ The routine guarantees that the return value will 
					have a value of either
				  - Success
					or
				  - Failure
- Usage:			N/A
****************************************************************************/
int LoadAllBooksConfig(void)
{
	if (LoadNASDAQ_MoldUDP_Config("nasdaq") == ERROR)
	{
		return ERROR;
	}
	
	if (LoadARCA_Book_Config("arca_switch", ARCA_Book_Conf.group) == ERROR)
	{
		return ERROR;
	}
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	LoadARCA_Book_Config
- Input:			+ fileName
- Output:			+ configInfo
- Return:			+ Success or Failure
- Description:		+ The routine hides the following facts
					  - Check valid file name
					  - Load and check valid configuration information
					+ There are preconditions guaranteed to the routine
					  - Data structure to contain configuration information
						  must be allocated before it is passed to the routine
					+ The routine guarantees that the return value will 
						have a value of either
					  - Success
						or
					  - Failure
- Usage:			N/A
****************************************************************************/
int LoadARCA_Book_Config(const char *fileName, t_ARCA_Book_GroupInfo group[MAX_ARCA_GROUPS])
{
	char fullPath[MAX_PATH_LEN];
	FILE  *f;
	char line[1024];
	int numLine = 0;
	int groupIndex = 0;
	char feedName = 0;
	
	//------------------------------------------------------------------
	// Get value indicating A or B Feed from first file (arca_switch)
	//------------------------------------------------------------------
	if (GetFullConfigPath(fileName, CONF_BOOK, fullPath) == NULL_POINTER)
	{
		TraceLog(ERROR_LEVEL, "Can not get the config path of file %s\n", fileName);
		return ERROR;
	}
	
    f = fopen(fullPath, "r");
    if (f == NULL)
    {
        TraceLog(ERROR_LEVEL, "can not open file '%s' to read\n", fullPath);
        return ERROR;
    }

    memset(line, 0, 1024);
	while (fgets(line, 1024, f) != NULL)
	{		
		if (!strchr(line, '#'))
		{	
			if (strlen(line) == 0)
			{
				continue;
			}
    
			RemoveCharacters(line, strlen(line));
			
			if (strlen(line) == 0)
			{
				continue;
			}
			
			//Read the contents
			feedName = line[0];
		}
	}
	
	fclose(f);
	f = NULL;
			

	//------------------------------------------------------------------
	// Load the actual config from file depending on what read above
	//		arca.xdp.afeed 
	//or 	arca.xdp.bfeed
	//------------------------------------------------------------------
	char feedFile[32];
	if ((feedName == 'a') || (feedName == 'A'))
	{
		ARCA_Book_Conf.currentFeedName = 'A';
		strcpy(feedFile, "arca.xdp.afeed");
	}
	else
	{
		ARCA_Book_Conf.currentFeedName = 'B';
		strcpy(feedFile, "arca.xdp.bfeed");
	}

	TraceLog(DEBUG_LEVEL, "Loading ARCA Book config from '%s'...\n", feedFile);
	
	if (GetFullConfigPath(feedFile, CONF_BOOK, fullPath) == NULL_POINTER)
	{
		TraceLog(ERROR_LEVEL, "Can not get the config path of file %s\n", feedFile);
		return ERROR;
	}
	
	// Open file configuration
	f = fopen(fullPath, "r");

	// Check file pointer
	if (f == NULL)
	{
		TraceLog(ERROR_LEVEL, "can not open file '%s' to read\n", fullPath);
		return ERROR;
	}
	
	// Opened file sucessfully
	memset(line, 0, 1024);

	// Get each line from the configuration file
	while (fgets(line, 1024, f) != NULL)
	{		
		if (!strchr(line, '#'))
		{	
			/*
			File structure:
			- Retransmission Request IP
			- Retransmission Request Port
			- Retransmission Source Name (user name)
			- 4 groups info: each has the following info
				+ IP Address Primary data feed UDP
				+ Port Primary data feed UDP
				+ IP Address Retransmission UDP
				+ Port Retransmission UDP
				+ IP Address Refresh Data Feed UDP
				+ Port Refresh Data Feed UDP
			*/
			
			RemoveCharacters(line, strlen(line));
			
			if (strlen(line) == 0)
			{
				continue;
			}
			
			numLine++;
			if (numLine == 1)	//Interface Name
			{
				strcpy(ARCA_Book_Conf.NICName, line);
				continue;
			}

			if (numLine == 2)	//Retransmission Request IP
			{
				strcpy(ARCA_Book_Conf.RetransmissionRequestIP, line);
				continue;
			}

			if (numLine == 3)	//Retransmission Request Port
			{
				ARCA_Book_Conf.RetransmissionRequestPort = atoi(line);
				continue;
			}

			if (numLine == 4)	//RetransmissionRequestIP username
			{
				strcpy(ARCA_Book_Conf.RetransmissionRequestSource, line);
				continue;
			}
			
			if (groupIndex == MAX_ARCA_GROUPS)
			{
				TraceLog(ERROR_LEVEL, "ARCA Book configuration is invalid\n");
				fclose(f);
				return ERROR;
			}
			
			// Primary data feed multicast group
			if (numLine == 5)
			{
				strcpy(group[groupIndex].dataIP, line);
			}
			else if (numLine == 6)
			{
				group[groupIndex].dataPort = atoi(line);
			}
			// Retransmission data feed multicast group
			else if (numLine == 7)
			{
				strcpy(group[groupIndex].retranIP, line);
			}
			else if (numLine == 8)
			{
				group[groupIndex].retranPort = atoi(line);
			}
			// Refresh multicase (for receiving symbol mapping message)
			else if (numLine == 9)
			{
				strcpy(group[groupIndex].refreshIP, line);
			}
			else if (numLine == 10)
			{
				group[groupIndex].refreshPort = atoi(line);
				groupIndex++;
				numLine = 4;
			}
		}
	}
		
	// Close the file
	fclose(f);

	return SUCCESS;
}

/****************************************************************************
- Function name:	LoadNASDAQ_MoldUDP_Config
- Input:			+ fileName
- Output:		+ configInfo
- Return:		+ Success or Failure
- Description:	+ The routine hides the following facts
				  - Check valid file name
				  - Load and check valid configuration information
				+ There are preconditions guaranteed to the routine
				  - Data structure to contain configuration information
					  must be allocated before it is passed to the routine
				+ The routine guarantees that the return value will 
					have a value of either
				  - Success
					or
				  - Failure
- Usage:			N/A
****************************************************************************/
int LoadNASDAQ_MoldUDP_Config(const char *fileName)
{
	char fullPath[MAX_PATH_LEN];
	
	if (GetFullConfigPath(fileName, CONF_BOOK, fullPath) == NULL_POINTER)
	{
		return ERROR;
	}
	
	FILE *f;
	
	//Buffer to store each line in the configuration file
	char line[MAX_LINE_LEN];
	
	//Number of lines read from the configuration file
	int  numLine = 0;

	//Open file configuration
	f = fopen(fullPath, "r");

	//Check file pointer
	if(f == NULL)
	{
		TraceLog(ERROR_LEVEL, "can not open file %s to read\n", fullPath);
		return -1;
	}

	//Opened file sucessfully
	memset(line, 0, MAX_LINE_LEN);

	//Get each line from the configuration file
	while (fgets(line, MAX_LINE_LEN, f) != NULL)
	{		
		if (!strchr(line, '#'))
		{	
			RemoveCharacters(line, strlen(line));
			
			if (line[0] <= 32)
			{
				continue;
			}
			
			numLine ++;
			
			if (numLine == 1)
			{
				strcpy(NDAQ_MoldUDP_Book.Data.NICName, line);
				strcpy(NDAQ_MoldUDP_Book.Recovery.NICName, line);
			}
			else if (numLine == 2)
			{
				strcpy(NDAQ_MoldUDP_Book.Data.ip, line);
			}
			else if (numLine == 3)
			{
				NDAQ_MoldUDP_Book.Data.port = atoi(line);
			}
			else if (numLine == 4)
			{
				strcpy(NDAQ_MoldUDP_Book.Recovery.ip, line);
			}
			else if (numLine == 5)
			{
				NDAQ_MoldUDP_Book.Recovery.port = atoi(line);
			}
		}
	}
		
	//Close the file
	fclose(f);
	
	if (numLine != 5)
	{
		//Configuration file is invalid!
		TraceLog(ERROR_LEVEL, "Nasdaq Book configuration is invalid\n");
		return ERROR;
	}
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	LoadCTS_Config
- Input:			+ fileName
- Output:			+ configInfo
- Return:			+ Success or Failure
- Description:		+ The routine hides the following facts
					  - Check valid file name
					  - Load and check valid configuration information
					+ There are preconditions guaranteed to the routine
					  - Data structure to contain configuration information
						  must be allocated before it is passed to the routine
					+ The routine guarantees that the return value will 
						have a value of either
					  - Success
						or
					  - Failure
- Usage:			N/A
****************************************************************************/
int LoadCTS_Config(const char *fileName)
{
	char fullPath[MAX_PATH_LEN];
	
	if (GetFullConfigPath(fileName, CONF_BOOK, fullPath) == NULL_POINTER)
	{
		return ERROR;
	}
	
	FILE  *f;
	
	// Buffer to store each line in the configuration file
	char line[MAX_LINE_LEN];
	
	//Number of lines read from the configuration file
	int numLine = 0;
	
	// Open file configuration
	f = fopen(fullPath, "r");

	// Check file pointer
	if (f == NULL)
	{
		TraceLog(ERROR_LEVEL, "can not open file '%s' to read\n", fullPath);
		return ERROR;
	}

	// Opened file sucessfully
	memset(line, 0, MAX_LINE_LEN);
	
	// Get each line from the configuration file
	while (fgets(line, MAX_LINE_LEN, f) != NULL)
	{		
		if (!strchr(line, '#'))
		{	
			RemoveCharacters(line, strlen(line));
			
			if (strlen(line) == 0)
			{
				continue;
			}
			
			numLine++;

			if (numLine == 1)
			{
				strcpy(CTS_Feed_Conf.NICName, line);
				continue;
			}

			// Primary data feed multicast group
			if (numLine <= MAX_MULTICAST_LINES * 2 + 1)
			{
				if ((numLine % 2) == 0)
				{
					strcpy(CTS_Feed_Conf.group[numLine/2 - 1].dataIP, line);
				}
				else
				{
					CTS_Feed_Conf.group[numLine/2 - 1].dataPort = atoi(line);
				}
			}
			else
			{
				//We got enough information
				break;
			}

			// Initialize buffer
			memset(line, 0, MAX_LINE_LEN);
		}
	}
	
	if (numLine < MAX_MULTICAST_LINES * 2 + 1)
	{
		TraceLog(ERROR_LEVEL, "CTS Feed configuration is invalid\n");
		// Close the file
		fclose(f);
		return ERROR;
	}
		
	// Close the file
	fclose(f);

	return SUCCESS;
}

/****************************************************************************
- Function name:	LoadUTDF_Config
- Input:			+ fileName
- Output:			+ configInfo
- Return:			+ Success or Failure
- Description:		+ The routine hides the following facts
					  - Check valid file name
					  - Load and check valid configuration information
					+ There are preconditions guaranteed to the routine
					  - Data structure to contain configuration information
						  must be allocated before it is passed to the routine
					+ The routine guarantees that the return value will 
						have a value of either
					  - Success
						or
					  - Failure
- Usage:			N/A
****************************************************************************/
int LoadUTDF_Config(const char *fileName)
{
	char fullPath[MAX_PATH_LEN];
	
	if (GetFullConfigPath(fileName, CONF_BOOK, fullPath) == NULL_POINTER)
	{
		return ERROR;
	}
	
	FILE  *f;
	
	// Buffer to store each line in the configuration file
	char line[MAX_LINE_LEN];
	
	//Number of lines read from the configuration file
	int numLine = 0;
	
	// Open file configuration
	f = fopen(fullPath, "r");

	// Check file pointer
	if (f == NULL)
	{
		TraceLog(ERROR_LEVEL, "can not open file '%s' to read\n", fullPath);
		return ERROR;
	}

	// Opened file sucessfully
	memset(line, 0, MAX_LINE_LEN);
	
	// Get each line from the configuration file
	while (fgets(line, MAX_LINE_LEN, f) != NULL)
	{		
		if (!strchr(line, '#'))
		{	
			RemoveCharacters(line, strlen(line));
			
			if (strlen(line) == 0)
			{
				continue;
			}
			
			numLine++;

			if (numLine == 1)
			{
				strcpy(UTDF_Conf.NICName, line);
				continue;
			}

			// Primary data feed multicast group
			if (numLine <= MAX_UTDF_CHANNELS * 2 + 1)
			{
				if ((numLine % 2) == 0)
				{
					strcpy(UTDF_Conf.group[numLine/2 - 1].dataIP, line);
				}
				else
				{
					UTDF_Conf.group[numLine/2 - 1].dataPort = atoi(line);
				}
			}
			else
			{
				//We got enough information
				break;
			}
			
			// Initialize buffer
			memset(line, 0, MAX_LINE_LEN);
		}
	}
	
	if (numLine < MAX_UTDF_CHANNELS * 2 + 1)
	{
		TraceLog(ERROR_LEVEL, "UTDF configuration is invalid\n");
		fclose(f);
		return ERROR;
	}
	
	// Close the file
	fclose(f);

	return SUCCESS;
}


/****************************************************************************
- Function name:	LoadCQS_Config
- Input:			+ fileName
- Output:			+ configInfo
- Return:			+ Success or Failure
- Description:		+ The routine hides the following facts
					  - Check valid file name
					  - Load and check valid configuration information
					+ There are preconditions guaranteed to the routine
					  - Data structure to contain configuration information
						  must be allocated before it is passed to the routine
					+ The routine guarantees that the return value will 
						have a value of either
					  - Success
						or
					  - Failure
- Usage:			N/A
****************************************************************************/
int LoadCQS_Config(const char *fileName)
{
	char fullPath[MAX_PATH_LEN];
	
	if (GetFullConfigPath(fileName, CONF_BOOK, fullPath) == NULL_POINTER)
	{
		return ERROR;
	}
	
    FILE  *f;
	
   	// Buffer to store each line in the configuration file
	char line[MAX_LINE_LEN];
	
	//Number of lines read from the configuration file
	int numLine = 0;
	
	// Open file configuration
	f = fopen(fullPath, "r");

	// Check file pointer
	if (f == NULL)
	{
		TraceLog(ERROR_LEVEL, "can not open file '%s' to read\n", fullPath);
		return ERROR;
	}

	// Opened file sucessfully
	memset(line, 0, MAX_LINE_LEN);
	
	// Get each line from the configuration file
	while (fgets(line, MAX_LINE_LEN, f) != NULL)
	{		
		if (!strchr(line, '#'))
		{	
			RemoveCharacters(line, strlen(line));
			
			if (strlen(line) == 0)
			{
				continue;
			}
			
			numLine++;

			if (numLine == 1)
			{
				strcpy(CQS_Feed_Conf.NICName, line);
				continue;
			}

			// Primary data feed multicast group
			if (numLine <= MAX_CQS_MULTICAST_LINES * 2 + 1)
			{
				if ((numLine % 2) == 0)
				{
					strcpy(CQS_Feed_Conf.group[numLine/2 - 1].dataIP, line);
				}
				else
				{
					CQS_Feed_Conf.group[numLine/2 - 1].dataPort = atoi(line);
				}
			}
			else
			{
				//We got enough information now
				break;
			}
			// Initialize buffer
			memset(line, 0, MAX_LINE_LEN);
		}
	}
		
	if (numLine < MAX_CQS_MULTICAST_LINES * 2 + 1)
	{
		TraceLog(ERROR_LEVEL, "CQS Feed configuration is invalid\n");
		fclose(f);
		return ERROR;
	}
	
	fclose(f);
	return SUCCESS;
}


/****************************************************************************
- Function name:	LoadUQDF_Config
- Input:			+ fileName
- Output:			+ configInfo
- Return:			+ Success or Failure
- Description:		+ The routine hides the following facts
					  - Check valid file name
					  - Load and check valid configuration information
					+ There are preconditions guaranteed to the routine
					  - Data structure to contain configuration information
						  must be allocated before it is passed to the routine
					+ The routine guarantees that the return value will
						have a value of either
					  - Success
						or
					  - Failure
- Usage:			N/A
****************************************************************************/
int LoadUQDF_Config(const char *fileName)
{
	char fullPath[MAX_PATH_LEN];

	if (GetFullConfigPath(fileName, CONF_BOOK, fullPath) == NULL_POINTER)
	{
		return ERROR;
	}

	FILE  *f;

	// Buffer to store each line in the configuration file
	char line[MAX_LINE_LEN];

	//Number of lines read from the configuration file
	int numLine = 0;

	// Open file configuration
	f = fopen(fullPath, "r");

	// Check file pointer
	if (f == NULL)
	{
		TraceLog(ERROR_LEVEL, "can not open file '%s' to read\n", fullPath);
		return ERROR;
	}

	// Opened file sucessfully
	memset(line, 0, MAX_LINE_LEN);

	// Get each line from the configuration file
	while (fgets(line, MAX_LINE_LEN, f) != NULL)
	{
		if (!strchr(line, '#'))
		{
			RemoveCharacters(line, strlen(line));

			if (strlen(line) == 0)
			{
				continue;
			}

			numLine++;

			if (numLine == 1)
			{
				strcpy (UQDF_Conf.NICName, line);
				continue;
			}

			// Primary data feed multicast group
			if (numLine <= MAX_UQDF_CHANNELS * 2 + 1)	//Interface name + COUNT *(ip:port)
			{
				if (numLine % 2 == 0)
				{
					strcpy(UQDF_Conf.group[numLine/2 - 1].dataIP, line);
				}
				else
				{
					UQDF_Conf.group[numLine/2 - 1].dataPort = atoi(line);
				}
			}
			else
			{
				//We got enough information now
				break;
			}
			// Initialize buffer
			memset(line, 0, MAX_LINE_LEN);
		}
	}

	if (numLine < MAX_UQDF_CHANNELS * 2 + 1)
	{
		TraceLog(ERROR_LEVEL, "UQDF Feed configuration is invalid\n");
		fclose(f);
		return ERROR;
	}

	fclose(f);
	return SUCCESS;
}


/****************************************************************************
- Function name:		LoadExchangesConf
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int LoadExchangesConf(void)
{
	char fullPath[MAX_PATH_LEN];

	if (GetFullConfigPath(FILE_NAME_OF_EXCHANGES_CONF, CONF_MISC, fullPath) == NULL_POINTER)
	{
		return ERROR;
	}

	FILE  *f;

	// Buffer to store each line in the configuration file
	char line[MAX_LINE_LEN];

	//Number of lines read from the configuration file
	int numLine = 0;

	// Open file configuration
	f = fopen(fullPath, "r");

	// Check file pointer
	if (f == NULL)
	{
		TraceLog(ERROR_LEVEL, "can not open file '%s' to read\n", fullPath);
		return ERROR;
	}

	// Opened file sucessfully
	memset(line, 0, MAX_LINE_LEN);
	int participant = 0;
	// Get each line from the configuration file
	while (fgets(line, MAX_LINE_LEN, f) != NULL)
	{
		if (!strchr(line, '#'))
		{
			RemoveCharacters(line, strlen(line));

			if (strlen(line) == 0)
			{
				continue;
			}

			numLine++;

			/* File foramat should be:
				- Participant ID
				- ENABLE/DISABLE for CQS
				- ENABLE/DISABLE for UQDF
				- ECN to place
				
				Total line should be MAX_EXCHANGE(16 * 4)
			*/
			
			// Primary data feed multicast group
			if (numLine <= MAX_EXCHANGE * 4)
			{
				if ((numLine - 1) % 4 == 0)
				{
					participant = line[0] - 65;
					if ((participant < 0) || (participant > 25))
					{
						TraceLog(ERROR_LEVEL, "Exchanges configuration file (%s) is invalid\n", fullPath);

						// Close the file
						fclose(f);

						return ERROR;
					}
					
					participant = ParticipantToExchangeIndexMapping[line[0] - 65];
				}
				else if ((numLine - 1) % 4 == 1)
				{
					// CQS status
					if (strncmp(line, "ENABLE", 6) == 0)
					{
						ExchangeStatusList[participant].status[CQS_SOURCE].visible = TRUE;
						ExchangeStatusList[participant].status[CQS_SOURCE].enable = TRUE;
					}
					else if (strncmp(line, "DISABLE", 7) == 0)
					{
						ExchangeStatusList[participant].status[CQS_SOURCE].visible = TRUE;
						ExchangeStatusList[participant].status[CQS_SOURCE].enable = FALSE;
					}
					else
					{
						ExchangeStatusList[participant].status[CQS_SOURCE].visible = FALSE;
						ExchangeStatusList[participant].status[CQS_SOURCE].enable = FALSE;
					}
				}
				else if ((numLine - 1) % 4 == 2)
				{
					// UQDF status
					if (strncmp(line, "ENABLE", 6) == 0)
					{
						ExchangeStatusList[participant].status[UQDF_SOURCE].visible = TRUE;
						ExchangeStatusList[participant].status[UQDF_SOURCE].enable = TRUE;
					}
					else if (strncmp(line, "DISABLE", 7) == 0)
					{
						ExchangeStatusList[participant].status[UQDF_SOURCE].visible = TRUE;
						ExchangeStatusList[participant].status[UQDF_SOURCE].enable = FALSE;
					}
					else
					{
						ExchangeStatusList[participant].status[UQDF_SOURCE].visible = FALSE;
						ExchangeStatusList[participant].status[UQDF_SOURCE].enable = FALSE;
					}
				}
				else
				{
					RemoveCharacters(line, strlen(line));
					if (strncmp(line, "RASH", 4) == 0)
					{
						ExchangeStatusList[participant].ECNToPlace = TYPE_NASDAQ_RASH;
					}
					else
					{
						ExchangeStatusList[participant].ECNToPlace = TYPE_ARCA_DIRECT;
					}
				}
			}
			else
			{
				TraceLog(ERROR_LEVEL, "Exchanges configuration file (%s) is invalid\n", fullPath);

				// Close the file
				fclose(f);

				return ERROR;
			}

			// Initialize buffer
			memset(line, 0, MAX_LINE_LEN);
		}
	}

	// Close the file
	fclose(f);
	
	if (numLine != MAX_EXCHANGE * 4)
	{
		TraceLog(ERROR_LEVEL, "Exchanges configuration file (%s) is invalid\n", fullPath);
		return ERROR;
	}

	return SUCCESS;
}

int LoadTradingAccountFromFile(const char *fileName, void *_tradingAccount)
{
	t_TradingAccount *tradingAccount = (t_TradingAccount *)_tradingAccount;
	
	/*
	Get full configuration path. If have any problem ERROR status will be returned
	for calling routine
	*/
	
	char fullPath[MAX_PATH_LEN];
	
	if (GetFullConfigPath(fileName, CONF_MISC, fullPath) == NULL_POINTER)
	{
		return ERROR;
	}

	/*
	Try to open file with plain text mode to read. If we cannot open, report ERROR
	otherwise try to read configuration info
	*/
	FILE *fileDesc = fopen(fullPath, "r");
	
	if (fileDesc == NULL)
	{
		TraceLog(ERROR_LEVEL, "can not open this %s to read\n", fullPath);
		return ERROR;
	}
	
	/*
	Create and initialize temporary buffer to 
	contain a line of configuration information
	*/
	char tempBuffer[1024] = "\0";
	int currentReadLine = 0;
	
	// Read configuration info and check valid configuration format
	while (fgets(tempBuffer, 1024, fileDesc) != NULL)
	{
		// Skip comment lines
		if (tempBuffer[0] == '#')
		{
			continue;
		}
		
		// Remove used characters
		RemoveCharacters(tempBuffer, strlen(tempBuffer));
		if (strlen(tempBuffer) == 0) continue;
		
		if (currentReadLine == 0)
		{
			strcpy(tradingAccount->ArcaDirectAccount[FIRST_ACCOUNT], tempBuffer);
		}
		else if (currentReadLine == 1)
		{
			strcpy(tradingAccount->ArcaDirectAccount[SECOND_ACCOUNT], tempBuffer);
		}
		else if (currentReadLine == 2)
		{
			strcpy(tradingAccount->ArcaDirectAccount[THIRD_ACCOUNT], tempBuffer);
		}
		else if (currentReadLine == 3)
		{
			strcpy(tradingAccount->ArcaDirectAccount[FOURTH_ACCOUNT], tempBuffer);
		}
		else if (currentReadLine == 4)
		{
			strcpy(tradingAccount->RASHAccount[FIRST_ACCOUNT], tempBuffer);
		}
		else if (currentReadLine == 5)
		{
			strcpy(tradingAccount->RASHAccount[SECOND_ACCOUNT], tempBuffer);
		}
		else if (currentReadLine == 6)
		{
			strcpy(tradingAccount->RASHAccount[THIRD_ACCOUNT], tempBuffer);
		}
		else if (currentReadLine == 7)
		{
			strcpy(tradingAccount->RASHAccount[FOURTH_ACCOUNT], tempBuffer);
		}
		else
		{
			// Close file to release resources
			fclose(fileDesc);
			TraceLog(ERROR_LEVEL, "%s has invalid format (has %d values, expect 8 values)!\n", fullPath, currentReadLine);
			return ERROR;
		}
		
		currentReadLine++;
	}
	
	// Close file to release resources
	fclose(fileDesc);
	
	if (currentReadLine != 8)
	{
		TraceLog(ERROR_LEVEL, "%s has invalid format (has %d values, expect 8 values)\n", fullPath, currentReadLine);
		return ERROR;
	}
	
	TraceLog(DEBUG_LEVEL, "Trade first account for Arca DIRECT: %s\n", tradingAccount->ArcaDirectAccount[FIRST_ACCOUNT]);
	TraceLog(DEBUG_LEVEL, "Trade second account for Arca DIRECT: %s\n", tradingAccount->ArcaDirectAccount[SECOND_ACCOUNT]);
	TraceLog(DEBUG_LEVEL, "Trade third account for Arca DIRECT: %s\n", tradingAccount->ArcaDirectAccount[THIRD_ACCOUNT]);
	TraceLog(DEBUG_LEVEL, "Trade fourth account for Arca DIRECT: %s\n", tradingAccount->ArcaDirectAccount[FOURTH_ACCOUNT]);
	
	TraceLog(DEBUG_LEVEL, "Trade first account for RASH: %s\n", tradingAccount->RASHAccount[FIRST_ACCOUNT]);
	TraceLog(DEBUG_LEVEL, "Trade second account for RASH: %s\n", tradingAccount->RASHAccount[SECOND_ACCOUNT]);
	TraceLog(DEBUG_LEVEL, "Trade third account for RASH: %s\n", tradingAccount->RASHAccount[THIRD_ACCOUNT]);
	TraceLog(DEBUG_LEVEL, "Trade fourth account for RASH: %s\n", tradingAccount->RASHAccount[FOURTH_ACCOUNT]);
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	IsSymbolIgnored
- Input:			+ symbol
- Output:			N/A
- Return:			+ status 1 for ignored, 0 for not ignored
- Description:		+ N/A
- Usage:			N/A
****************************************************************************/
int IsSymbolIgnored(char *symbol)
{
	int index = -1;
	index = GetStockSymbolIndex(symbol);
	if (index == -1)
	{
		index = UpdateStockSymbolIndex(symbol);
		if (index == -1)
			return 0;	//not found, considered as not ignored
	}
	
	if (symbolIgnoreStatus[index] == DISABLE)
	{
		return 1; //ignored
	}
	
	return 0; //not ignored
}


/****************************************************************************
- Function name:		SaveExchangesConf
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int SaveExchangesConf(void)
{
	char fullPath[MAX_PATH_LEN];

	if (GetFullConfigPath(FILE_NAME_OF_EXCHANGES_CONF, CONF_MISC, fullPath) == NULL_POINTER)
	{
		return ERROR;
	}

	FILE  *f;

	// Open file configuration
	f = fopen(fullPath, "w+");

	// Check file pointer
	if (f == NULL)
	{
		TraceLog(ERROR_LEVEL, "can not open file '%s' to read\n", fullPath);
		return ERROR;
	}

	/*
	# Checkmark list of exchanges
	# Exchange name
	# Status: ENABLE, DISABLE, N/A
	# ECN To Place
	*/
	fprintf(f, "# Format should be:\n");
	fprintf(f, "# Exchange name (1 character in CQS/UQDF format)\n");
	fprintf(f, "# Status for CQS:  ENABLE, DISABLE, N/A\n");
	fprintf(f, "# Status for UQDF: ENABLE, DISABLE, N/A\n");
	fprintf(f, "# ECN To Place\n\n");

	int i;
	for (i = 0; i < MAX_EXCHANGE; i++)
	{
		switch (i)
		{
			case BOOK_ARCA:
				fprintf(f, "# P  NYSE Arca Exchange\n");
				break;

			case BOOK_NASDAQ:
				fprintf(f, "# Q/T  Nasdaq Stock Exchange\n");
				break;

			case BOOK_BATSZ:
				fprintf(f, "# Z  BATSZ Exchange Inc\n");
				break;

			case NYSE_Amex:
				fprintf(f, "# A  American Stock Exchange\n");
				break;

			case Boston_Stock_Exchange:
				fprintf(f, "# B  Boston Stock Exchange\n");
				break;

			case National_Stock_Exchange:
				fprintf(f, "# C  National Stock Exchange\n");
				break;

			case FINRA_ADF:
				fprintf(f, "# D  FINRA\n");
				break;

			case Market_Independent:
				fprintf(f, "# E  CQS: Consolidated Quote System\n");
				fprintf(f, "# UQDF: Market Independent (Generated by SIP) \n");
				break;

			case International_Securities_Exchange:
				fprintf(f, "# I  International Securities Exchange\n");
				break;

			case EDGA_Exchange:
				fprintf(f, "# J   EDGA Exchange, Inc\n");
				break;

			case BOOK_EDGX:
				fprintf(f, "# K  EDGX Exchange, Inc\n");
				break;

			case Chicago_Stock_Exchange:
				fprintf(f, "# M  Chicago Stock Exchange\n");
				break;

			case BOOK_NYSE:
				fprintf(f, "# # N  CQS: New York Stock Exchange - UQDF: NYSE Euronext\n");
				break;

			case Chicago_Board_Options_Exchange:
				fprintf(f, "# W  Chicago Board Options Exchange\n");
				break;

			case Philadelphia_Stock_Exchange:
				fprintf(f, "# X  Philadelphia Stock Exchange\n");
				break;

			case BATS_Y_Exchange:
				fprintf(f, "# Y  BATS Y-Exchange, Inc\n");
				break;
		}

		fprintf(f, "%c\n", ExchangeStatusList[i].exchangeId);
		if (ExchangeStatusList[i].status[CQS_SOURCE].visible == TRUE)
		{
			if (ExchangeStatusList[i].status[CQS_SOURCE].enable == TRUE)
			{
				fprintf(f, "ENABLE\n");
			}
			else
			{
				fprintf(f, "DISABLE\n");
			}
		}
		else
		{
			fprintf(f, "N/A\n");
		}
		
		if (ExchangeStatusList[i].status[UQDF_SOURCE].visible == TRUE)
		{
			if (ExchangeStatusList[i].status[UQDF_SOURCE].enable == TRUE)
			{
				fprintf(f, "ENABLE\n");
			}
			else
			{
				fprintf(f, "DISABLE\n");
			}
		}
		else
		{
			fprintf(f, "N/A\n");
		}

		if (ExchangeStatusList[i].ECNToPlace == TYPE_NASDAQ_RASH)
		{
			fprintf(f, "%s\n\n", "RASH");
		}
		else
		{
			fprintf(f, "%s\n\n", "ARCA_DIRECT");
		}
	}

	// Close the file
	fclose(f);

	return SUCCESS;
}

t_DBLInstance* GetDBLInstance(dbl_device_t dev)
{
	int i;
	for (i = 0; i < MAX_DBL_INSTANCE; i++)
	{
		if (DBLMgmt.dblInstance[i].dev == dev)
		{
			return &DBLMgmt.dblInstance[i];
		}
	}
	
	return NULL;
}

int IsDBLTimedOut(dbl_device_t dev)
{
	t_DBLInstance *instance = GetDBLInstance(dev);
	if (instance == NULL) return 0;
	return instance->isTimedOut;
}

/****************************************************************************/
