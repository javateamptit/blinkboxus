/****************************************************************************
** Project name:	Equity Arbitrage Application
** File name: 		uqdf_feed_proc.c
** Description: 	This file contains function definitions that were declared
					in uqdf_feed_proc.h
** Author:			ThoHN
** First created on August 30, 2010
** Last updated on August 30, 2010
****************************************************************************/

#define _BSD_SOURCE

#include <pthread.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>

#include "utility.h"
#include "book_mgmt.h"
#include "socket_util.h"
#include "trading_mgmt.h"
#include "uqdf_feed_proc.h"
#include "stock_symbol.h"
#include "bbo_mgmt.h"
#include "kernel_algorithm.h"
#include "aggregation_server_proc.h"

#define MAX_RECV_UQDF_TIMEOUT 62

int arrUQDF_DenominatorValue[3];

extern t_MonitorUQDF MonitorUQDF[MAX_UQDF_CHANNELS];
extern t_DBLMgmt DBLMgmt;
/****************************************************************************
- Function name:	InitializeUQDF_DenominatorValue
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int InitializeUQDF_DenominatorValue(void)
{
	arrUQDF_DenominatorValue[0] = 100; //B
	arrUQDF_DenominatorValue[1] = 1000; //C
	arrUQDF_DenominatorValue[2] = 10000; //D

	return SUCCESS;
}


/****************************************************************************
- Function name:	InitUQDF_Quote_DataStructure
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
void InitUQDF_Quote_DataStructure(void)
{	
	int symbolIndex, exchangeIndex;
	for (symbolIndex = 0; symbolIndex < MAX_STOCK_SYMBOL; symbolIndex++)
	{
		for (exchangeIndex = 0; exchangeIndex < MAX_EXCHANGE; exchangeIndex++)
		{
			memset(&BBOQuoteMgmt[symbolIndex].BBOQuoteInfo[UQDF_SOURCE][exchangeIndex], 0, sizeof(t_BBOQuoteInfo));
		}
		
		memset(&BBOQuoteMgmt[symbolIndex].nationalBBO[UQDF_SOURCE], 0, sizeof(t_NationalAppendage));
		
		if (BestBBO[symbolIndex][ASK_SIDE].sourceId == UQDF_SOURCE)
		{
			pthread_mutex_lock(&UpdateBestBBOMutex[symbolIndex][ASK_SIDE]);
			
			BestBBO[symbolIndex][ASK_SIDE].participantId = -1;
			BestBBO[symbolIndex][ASK_SIDE].sourceId = -1;
			BestBBO[symbolIndex][ASK_SIDE].price = 0.0;
			BestBBO[symbolIndex][ASK_SIDE].size = 0;
			
			pthread_mutex_unlock(&UpdateBestBBOMutex[symbolIndex][ASK_SIDE]);
		}
		
		if (BestBBO[symbolIndex][BID_SIDE].sourceId == UQDF_SOURCE)
		{
			pthread_mutex_lock(&UpdateBestBBOMutex[symbolIndex][BID_SIDE]);
			
			BestBBO[symbolIndex][BID_SIDE].participantId = -1;
			BestBBO[symbolIndex][BID_SIDE].sourceId = -1;
			BestBBO[symbolIndex][BID_SIDE].price = 0.0;
			BestBBO[symbolIndex][BID_SIDE].size = 0;
			
			pthread_mutex_unlock(&UpdateBestBBOMutex[symbolIndex][BID_SIDE]);
		}
	}
}

/****************************************************************************
- Function name:	InitUQDF_DataStructure
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
void InitUQDF_DataStructure(void)
{
	TraceLog(DEBUG_LEVEL, "Initialize UQDF ...\n");

	InitializeUQDF_DenominatorValue();
	InitUQDF_Quote_DataStructure();
	
	int i;

	UQDF_Conf.dev = NULL;

	for (i = 0; i < MAX_UQDF_CHANNELS; i++)
	{
		MonitorUQDF[i].recvCounter = 0;
		MonitorUQDF[i].procCounter = 0;
		
		UQDF_Conf.group[i].dataChannel = NULL;
		
		// Channel index
		UQDF_Conf.group[i].index = i;

		// For primary data feed processing
		// Initialize semaphore
		sem_init(&UQDF_Conf.group[i].dataInfo.sem, 0, 0);
		strcpy(UQDF_Conf.group[i].dataInfo.bookName, "UQDF");

		memset(UQDF_Conf.group[i].dataInfo.buffer, 0, MAX_BOOK_MSG_IN_BUFFER * MAX_MTU);
		UQDF_Conf.group[i].dataInfo.DisconnectBook = &DisconnectUQDF_Multicast;
		
		// Initialize counter variables
		UQDF_Conf.group[i].primaryLastSequenceNumber = -1;
		UQDF_Conf.group[i].dataInfo.currentReceivedIndex = 0;
		UQDF_Conf.group[i].dataInfo.currentReadIndex = 0;
		UQDF_Conf.group[i].dataInfo.connectionFlag = 0;

		// For testing
		/*sprintf(UQDF_Conf.group[i].primaryFileName, "primary_channel_%d.txt", i + 1);
		UQDF_Conf.group[i].primaryFileDesc = NULL;*/
	}
}

/****************************************************************************
- Function name:	UQDF_Feed_Thread
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
void *UQDF_Feed_Thread(void *agrs)
{
	SetKernelAlgorithm("[UQDF_BASE]");
	
	InitUQDF_DataStructure();

	int i;

	while (UQDF_StatusMgmt.shouldConnect == YES)
	{
		int ret = CreateDBLDeviceV2(UQDF_Conf.NICName, &UQDF_Conf.dev);

		if (ret == ERROR)
		{
			TraceLog(ERROR_LEVEL, "UQDF: Can't create DBL device\n");
			DisconnectUQDF_Multicast();
			return NULL;
		}

		for (i = 0; i < MAX_UQDF_CHANNELS; i++)
		{
			/* Start threads to receive and process data from primary data feed */
			if (pthread_create(&UQDF_Conf.group[i].primaryProcessThreadID, NULL, (void*)&ThreadProcessUQDF_PrimaryData, (void*) &UQDF_Conf.group[i]) != SUCCESS)
			{
				TraceLog(ERROR_LEVEL, "UQDF: Cannot create thread to process packets\n");

				DisconnectUQDF_Multicast();

				return NULL;
			}
			
			TraceLog(DEBUG_LEVEL, "UQDF FEED: MulticastRecvPort(%s:%d) group(%d): connecting\n", UQDF_Conf.group[i].dataIP, UQDF_Conf.group[i].dataPort, (i+1));
			// Join to primary multicast group
			ret = JoinMulticastGroupUsingExistingDeviceV2(UQDF_Conf.dev, UQDF_Conf.group[i].dataIP, UQDF_Conf.group[i].dataPort, &UQDF_Conf.group[i].dataChannel, UQDF_Conf.NICName, (void *) &UQDF_Conf.group[i].dataInfo);
			
			if (ret == ERROR)
			{
				TraceLog(ERROR_LEVEL, "UQDF: Can't connect to Multicast group (%s:%d)\n", UQDF_Conf.group[i].dataIP, UQDF_Conf.group[i].dataPort);

				DisconnectUQDF_Multicast();

				return NULL;
			}
			
			// Sleep 100 micro-seconds
			usleep(100);
		}
		
		// Notify that UQDF was connected
		UQDF_StatusMgmt.isConnected = CONNECTED;
		for (i = 0; i < MAX_UQDF_CHANNELS; i++)
		{
			UQDF_Conf.group[i].dataInfo.connectionFlag = 1;
		}
		
		// Wait for ALL threads completed
		for (i = 0; i < MAX_UQDF_CHANNELS; i++)
		{
			pthread_join(UQDF_Conf.group[i].primaryProcessThreadID, NULL);
		}

		// Release UQDF Multicast Resource
		ReleaseUQDF_MulticastResource();

		InitUQDF_DataStructure();
	}

	// Notify UQDF was disconnected
	UQDF_StatusMgmt.isConnected = DISCONNECTED;
	SendPMDisconnectedBookOrderAlertToAS(SERVICE_UQDF_FEED);

	TraceLog(DEBUG_LEVEL, "UQDF was disconnected\n");

	return NULL;
}

/****************************************************************************
- Function name:	ThreadProcessUQDF_PrimaryData
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
void *ThreadProcessUQDF_PrimaryData(void *args)
{
	SetKernelAlgorithm("[UQDF_PROCESS_MSGS]");

	t_UQDF_Group *workingGroup = (t_UQDF_Group *)args;

	// Temp buffer need to store data from receive buffer
	char *tempBuffer;
	int index;

	while (UQDF_StatusMgmt.shouldConnect == YES)
	{
		// Wait for new message
		if (sem_wait(&workingGroup->dataInfo.sem) != SUCCESS)
		{
			TraceLog(ERROR_LEVEL, "Cannot sem_wait. UQDF will be stopped now (%s %d)\n", workingGroup->dataIP, workingGroup->dataPort);

			DisconnectUQDF_Multicast();

			return NULL;
		}

		if (UQDF_StatusMgmt.shouldConnect == NO)
		{
			return NULL;
		}

		index = workingGroup->dataInfo.currentReadIndex;

		tempBuffer = workingGroup->dataInfo.buffer[index];

		if (tempBuffer[MAX_MTU - 1] != STORED)
		{
			TraceLog(ERROR_LEVEL, "UQDF: Receive buffer of primary channel %d is full: received index = %d, read index = %d\n",
				workingGroup->index + 1, workingGroup->dataInfo.currentReceivedIndex, workingGroup->dataInfo.currentReadIndex);

			DisconnectUQDF_Multicast();

			return NULL;
		}

		// Save UQDF packet to file
		//SaveUQDFPacketToFile(workingUQDF_Conf->primaryFileDesc, tempBuffer);

		// Add code to process transmission block
		// ...
		ProcessUQDF_MsgFromPrimaryData(workingGroup, tempBuffer);

		// Update status of buffer
		tempBuffer[MAX_MTU - 1] = READ;

		/*
		If we have Current_Read_Index greater than LIMITATION of INTEGER data type
		that is very dangerous
		*/
		if(workingGroup->dataInfo.currentReadIndex == MAX_BOOK_MSG_IN_BUFFER - 1)
		{
			workingGroup->dataInfo.currentReadIndex = 0;
		}
		else
		{
			workingGroup->dataInfo.currentReadIndex++;
		}
	}

	return NULL;
}

/****************************************************************************
- Function name:	ProcessUQDF_MsgFromPrimaryData
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessUQDF_MsgFromPrimaryData(void *args, char buffer[MAX_MTU])
{
	t_UQDF_Group *workingGroup = (t_UQDF_Group *)args;

	int index = 0, icount, isSkip = 0;
	t_UQDF_Msg msgTmp;

	if (buffer[index] != SOH_CHAR)
	{
		TraceLog(ERROR_LEVEL, "UQDF: Invalid packet from primary data feed, msg = %s\n", buffer);

		return ERROR;
	}

	// Increasing index
	index += 1;

	while ((index < MAX_MTU - 1) && (buffer[index] != ETX_CHAR))
	{
		icount = index + MAX_UQDF_HEADER_LEN;
		while ((icount < MAX_MTU - 1) && (buffer[icount] != US_CHAR) && (buffer[icount] != ETX_CHAR))
		{
			icount++;
		}

		// Check if this is the needed packet: Retransmission Requester is original
		if ( (buffer[index + 4] == 32)  &&
			((buffer[index + 3] == 'O') || (buffer[index + 3] == 'R')) )
		{
			// Get message header
			msgTmp.msgCategory = buffer[index];
			msgTmp.msgType = buffer[index + 1];
			msgTmp.msgSeqNum = atoi(&buffer[index + 5]);

			if (icount == MAX_MTU - 1)
			{
				TraceLog(ERROR_LEVEL, "UQDF: Invalid packet from primary data feed, msg = %s\n", buffer);
				return ERROR;
			}

			if (workingGroup->primaryLastSequenceNumber == -1)
			{
				workingGroup->primaryLastSequenceNumber = msgTmp.msgSeqNum;
			}
			else
			{
				if ((msgTmp.msgCategory == 'C') && (msgTmp.msgType == 'L'))
				{
					TraceLog(DEBUG_LEVEL, "UQDF: Received message sequence number reset of channel %d, we will reset sequence number now\n", workingGroup->index + 1);
				}
				else
				{
					if (msgTmp.msgSeqNum > workingGroup->primaryLastSequenceNumber + 1)
					{
						TraceLog(WARN_LEVEL, "UQDF: Loss %d message(s) from channel %d: fromMsgSeqNum = %d, toMsgSeqNum = %d\n", msgTmp.msgSeqNum - workingGroup->primaryLastSequenceNumber - 1, workingGroup->index + 1, workingGroup->primaryLastSequenceNumber + 1, msgTmp.msgSeqNum - 1);
					}
					else if (msgTmp.msgSeqNum < workingGroup->primaryLastSequenceNumber)
					{
						isSkip = 1;
					}
				}

				if (isSkip == 0)
				{
					workingGroup->primaryLastSequenceNumber = msgTmp.msgSeqNum;
				}
			}

			if (isSkip == 1)
			{
				return ERROR;
			}

			switch (msgTmp.msgCategory)
			{
				case 'Q':
					// Q - C: UTP Participant BBO Quote Short Form (retired)
					// Q - D: UTP Participant BBO Quote Long Form (retired)
					// Q - E: UTP Participant BBO Short Form
					// Q - F: UTP Participant BBO Long Form
					switch (msgTmp.msgType)
					{
						case 'E':
							if ((icount - (index + MAX_UQDF_HEADER_LEN)) < 0)
							{
								return SUCCESS;		// Something wrong in the message format then exit
							}

							memcpy(msgTmp.msgHeader, &buffer[index], MAX_UQDF_HEADER_LEN);
							memcpy(msgTmp.msgBody, &buffer[index + MAX_UQDF_HEADER_LEN], icount - (index + MAX_UQDF_HEADER_LEN));
							msgTmp.msgBody[icount - (index + MAX_UQDF_HEADER_LEN)] = 0;
							
							ProcessUQDF_BBOShort(&msgTmp);
							break;
						case 'F':
							if ((icount - (index + MAX_UQDF_HEADER_LEN)) < 0)
							{
								return SUCCESS;		// Something wrong in the message format then exit
							}

							memcpy(msgTmp.msgHeader, &buffer[index], MAX_UQDF_HEADER_LEN);
							memcpy(msgTmp.msgBody, &buffer[index + MAX_UQDF_HEADER_LEN], icount - (index + MAX_UQDF_HEADER_LEN));
							msgTmp.msgBody[icount - (index + MAX_UQDF_HEADER_LEN)] = 0;
							
							ProcessUQDF_BBOLong(&msgTmp);
							break;
							
						case 'C':	// UTP Participant BBO Quote Short Form (retired)
							if ((icount - (index + MAX_UQDF_HEADER_LEN)) < 0)
							{
								return SUCCESS;		// Something wrong in the message format then exit
							}

							memcpy(msgTmp.msgHeader, &buffer[index], MAX_UQDF_HEADER_LEN);
							memcpy(msgTmp.msgBody, &buffer[index + MAX_UQDF_HEADER_LEN], icount - (index + MAX_UQDF_HEADER_LEN));
							msgTmp.msgBody[icount - (index + MAX_UQDF_HEADER_LEN)] = 0;

							ProcessUQDF_ShortQuote(&msgTmp);
							break;
						case 'D':	// UTP Participant BBO Quote Long Form (retired)
							if ((icount - (index + MAX_UQDF_HEADER_LEN)) < 0)
							{
								return SUCCESS;		// Something wrong in the message format then exit
							}

							memcpy(msgTmp.msgHeader, &buffer[index], MAX_UQDF_HEADER_LEN);
							memcpy(msgTmp.msgBody, &buffer[index + MAX_UQDF_HEADER_LEN], icount - (index + MAX_UQDF_HEADER_LEN));
							msgTmp.msgBody[icount - (index + MAX_UQDF_HEADER_LEN)] = 0;

							ProcessUQDF_LongQuote(&msgTmp);
							break;
							
						default:
							TraceLog(ERROR_LEVEL, "UQDF: Unknown msgType (%c)\n", msgTmp.msgType);
							break;
					}
					break;		//End of category Q
				
				case 'C':
					// C: Control Messages contain only the header!
					// C - I: Start of Day
					// C - J: End of Day
					// C - O: Market Session Open
					// C - C: Market Session Close
					// C - A: Emergency Market � Halt
					// C - R: Emergency Market � Quote Resume
					// C - B: Emergency Market � Trade Resume
					// C - K: End of Retransmission Requests
					// C - Z: End of Transmissions
					// C - T: Line Integrity
					// C - L: Sequence Number Reset
					// C - P: Quote Wipe-Out
					break;		//End of category C
			
				case 'A':
					// A - A: General administrator message
					// A - H: Cross SRO Trading Action Message
					// A - R: Session Close Recap Message
					// A - K: Market Center Trading Action
					// A - B: Issue Symbol Directory Message
					// A - V: Regulation SHO Short Sale Price Test Restricted Indicator
					// A - C: Market Wide Circuit Breaker Decline Level Message
					// A - D: Market Wide Circuit Breaker Status
					// A - P: Price Band Message
					switch (msgTmp.msgType)
					{
						case 'A':	//General Administrative message
							if ((icount - (index + MAX_UQDF_HEADER_LEN)) < 0)
							{
								return SUCCESS; // Something wrong in the message format then exit
							}
							
							// As in UQDF spec Administrative Msg is a free text having maximum 300 characters!
							TraceLog(DEBUG_LEVEL, "UQDF Admin Msg: %.300s\n", &buffer[index + MAX_UQDF_HEADER_LEN]);
							break;

						default:
							//Ignore other msgs
							break;
					}
					break;		//End of category A
					
				default:
					// !Other categories ??? There should be no other categories. So this block should never be executed!!!
					break;
			}	//End of switch for Category
		}

		if (buffer[icount] == US_CHAR)
		{
			icount++;
		}

		// Increasing index
		index = icount;
	}

	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessUQDF_BBOLong
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessUQDF_BBOLong(t_UQDF_Msg *msg)
{
	t_BBOQuoteInfo quoteInfo;

	msg->msgHeader[MAX_UQDF_HEADER_LEN] = 0;

	// PARTICIPANT ID
	quoteInfo.participantId = msg->msgHeader[13];

	// TIME STAMP
	quoteInfo.timeStamp = ((Lrc_atoi(&msg->msgHeader[14], 2) - TIMESTAMP_ADJUST_HOUR) * 3600 + Lrc_atoi(&msg->msgHeader[16], 2) * 60 + Lrc_atoi(&msg->msgHeader[18], 2)) * 1000 + Lrc_atoi(&msg->msgHeader[20], 3);

	// SECURITY SYMBOL: 11 bytes
	char cqsSymbol[12];

	strncpy(cqsSymbol, msg->msgBody, 11);
	cqsSymbol[11] = 0;

	char symbol[SYMBOL_LEN];
	if (ConvertCQSSymbolToComstock(cqsSymbol, symbol) == ERROR)
	{
		return SUCCESS;
	}
	
	// Get stock symbol index
	int symbolIndex = GetStockSymbolIndex(symbol);

	if (symbolIndex == -1)
	{
		symbolIndex = UpdateStockSymbolIndex(symbol);
		if (symbolIndex == -1)
		{
			return SUCCESS;
		}
	}

	if (IsSymbolIgnored(symbol) == 1)
	{
		return SUCCESS;
	}

	// QUOTE CONDITIONS: 1 byte
	quoteInfo.quoteCondition = msg->msgBody[13];

	// Bid Price Denominator: 1 byte
	int numerator = msg->msgBody[16] - 'B';

	// Bid Price 10 bytes
	int whole = Lrc_atoi(&msg->msgBody[17], 10);
	quoteInfo.bidPrice = (double)whole / arrUQDF_DenominatorValue[numerator];

	// Bid Size: 7 bytes
	quoteInfo.bidSize = Lrc_atoi(&msg->msgBody[27], 7);

	// Offer Price Denominator: 1 byte
	numerator = msg->msgBody[34] - 'B';

	// Offer Price 10 bytes
	whole = Lrc_atoi(&msg->msgBody[35], 10);
	quoteInfo.offerPrice = (double)whole / arrUQDF_DenominatorValue[numerator];

	// Offer Size: 7 bytes
	quoteInfo.offerSize = Lrc_atoi(&msg->msgBody[45], 7);

	t_NationalAppendage nationalAppendage;

	// Initialize ...
	nationalAppendage.timeStamp = 0;
	nationalAppendage.bidSize = -1;
	nationalAppendage.offerSize = -1;

	// National BBO Appendage Indicator
	int nationalIndicator = msg->msgBody[55] - 48;
	if (nationalIndicator == 2)
	{
		/* 2:	Short Form National BBO Appendage Attached � A new National BBO
				was generated as a result of the UTP participant�s quote update and the new
				National BBO information is contained in the attached short form appendage.*/
		nationalAppendage.timeStamp = quoteInfo.timeStamp;
		ProcessUQDF_ShortNationalBBOAppendage(&msg->msgBody[58], &nationalAppendage);
	}
	else if (nationalIndicator == 3)
	{
		/* 3:	Long Form National BBO Appendage Attached � A new National BBO
				was generated as a result of the UTP participant�s quote update and the new
				information is contained in the attached long form appendage.*/
		nationalAppendage.timeStamp = quoteInfo.timeStamp;
		ProcessUQDF_LongNationalBBOAppendage(&msg->msgBody[58], &nationalAppendage);
	}
	else if (nationalIndicator == 4)
	{
		/* 4:	Quote Contains All National BBO Information � Current UTP
				participant�s quote is itself the National BBO. Vendors should update
				National BBO to reflect this new quote and single market center. (National
				Best Bid Market Center and National Best Ask Market Center fields should be
				updated to reflect the Market Center Originator ID value in the UQDF
				message header.) No appendage is required.*/
		nationalAppendage.timeStamp = quoteInfo.timeStamp;
		nationalAppendage.bidParticipantId = quoteInfo.participantId;
		nationalAppendage.bidPrice = quoteInfo.bidPrice;
		nationalAppendage.bidSize = quoteInfo.bidSize;

		nationalAppendage.offerParticipantId = quoteInfo.participantId;
		nationalAppendage.offerPrice = quoteInfo.offerPrice;
		nationalAppendage.offerSize = quoteInfo.offerSize;
	}
	else if (nationalIndicator == 1)
	{
		/* 1: 	No National BBO Can be Calculated � The National BBO cannot be
				calculated therefore vendors should show National BBO fields as blank. No
				appendage is required.*/
		memset (&nationalAppendage, 0, sizeof(t_NationalAppendage));
	}
	else
	{
		/* 0:	No National BBO Change � The UTP participant�s quote does not affect the
				National BBO. Vendors should continue to show the existing National BBO.
				No appendage is required.*/
	}

	// FINRA ADF MPID Appendage Indicator

	// Add trade to list
	AddQuoteToBBOQuoteMgmt(symbol, &quoteInfo, &nationalAppendage, UQDF_SOURCE);

	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessUQDF_LongQuote
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessUQDF_LongQuote(t_UQDF_Msg *msg)
{
	t_BBOQuoteInfo quoteInfo;

	msg->msgHeader[MAX_UQDF_HEADER_LEN] = 0;

	// PARTICIPANT ID
	quoteInfo.participantId = msg->msgHeader[13];

	// TIME STAMP
	quoteInfo.timeStamp = ((Lrc_atoi(&msg->msgHeader[14], 2) - TIMESTAMP_ADJUST_HOUR) * 3600 + Lrc_atoi(&msg->msgHeader[16], 2) * 60 + Lrc_atoi(&msg->msgHeader[18], 2)) * 1000 + Lrc_atoi(&msg->msgHeader[20], 3);

	// SECURITY SYMBOL: 11 bytes
	char cqsSymbol[12];

	strncpy(cqsSymbol, msg->msgBody, 11);
	cqsSymbol[11] = 0;

	char symbol[SYMBOL_LEN];
	if (ConvertCQSSymbolToComstock(cqsSymbol, symbol) == ERROR)
	{
		return SUCCESS;
	}

	// Get stock symbol index
	int symbolIndex = GetStockSymbolIndex(symbol);

	if (symbolIndex == -1)
	{
		symbolIndex = UpdateStockSymbolIndex(symbol);
		if (symbolIndex == -1)
		{
			return SUCCESS;
		}
	}

	if (IsSymbolIgnored(symbol) == 1)
	{
		return SUCCESS;
	}


	// QUOTE CONDITIONS: 1 byte
	quoteInfo.quoteCondition = msg->msgBody[11];

	// Bid Price Denominator: 1 byte
	int numerator = msg->msgBody[12] - 'B';

	// Bid Price 10 bytes
	int whole = Lrc_atoi(&msg->msgBody[13], 10);
	quoteInfo.bidPrice = (double)whole / arrUQDF_DenominatorValue[numerator];

	// Bid Size: 7 bytes
	quoteInfo.bidSize = Lrc_atoi(&msg->msgBody[23], 7);

	// Offer Price Denominator: 1 byte
	numerator = msg->msgBody[30] - 'B';

	// Offer Price 10 bytes
	whole = Lrc_atoi(&msg->msgBody[31], 10);
	quoteInfo.offerPrice = (double)whole / arrUQDF_DenominatorValue[numerator];

	// Offer Size: 7 bytes
	quoteInfo.offerSize = Lrc_atoi(&msg->msgBody[41], 7);

	t_NationalAppendage nationalAppendage;

	// Initialize ...
	nationalAppendage.timeStamp = 0;
	nationalAppendage.bidSize = -1;
	nationalAppendage.offerSize = -1;

	// National BBO Appendage Indicator
	int nationalIndicator = msg->msgBody[51] - 48;
	if (nationalIndicator == 2)
	{
		// 2  Short Form National BBO Appendage Attached
		nationalAppendage.timeStamp = quoteInfo.timeStamp;
		ProcessUQDF_ShortNationalBBOAppendage(&msg->msgBody[53], &nationalAppendage);
	}
	else if (nationalIndicator == 3)
	{
		// 3  Long Form National BBO Appendage Attached
		nationalAppendage.timeStamp = quoteInfo.timeStamp;
		ProcessUQDF_LongNationalBBOAppendage(&msg->msgBody[53], &nationalAppendage);
	}
	else if (nationalIndicator == 4)
	{
		// 4  Quote Contains All National BBO Information
		nationalAppendage.timeStamp = quoteInfo.timeStamp;
		nationalAppendage.bidParticipantId = quoteInfo.participantId;
		nationalAppendage.bidPrice = quoteInfo.bidPrice;
		nationalAppendage.bidSize = quoteInfo.bidSize;

		nationalAppendage.offerParticipantId = quoteInfo.participantId;
		nationalAppendage.offerPrice = quoteInfo.offerPrice;
		nationalAppendage.offerSize = quoteInfo.offerSize;
	}
	else if (nationalIndicator == 1)
	{
		// 1  No National BBO Can be Calculated (should remove the existing one)
		memset (&nationalAppendage, 0, sizeof(t_NationalAppendage));
	}
	else
	{
		// 0  No National BBO Change (should keep the existing one)
	}

	// FINRA ADF MPID Appendage Indicator

	// Add trade to list
	AddQuoteToBBOQuoteMgmt(symbol, &quoteInfo, &nationalAppendage, UQDF_SOURCE);

	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessUQDF_BBOShort
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessUQDF_BBOShort(t_UQDF_Msg *msg)
{
	t_BBOQuoteInfo quoteInfo;

	msg->msgHeader[MAX_UQDF_HEADER_LEN] = 0;

	// PARTICIPANT ID
	quoteInfo.participantId = msg->msgHeader[13];

	// TIME STAMP
	quoteInfo.timeStamp = ((Lrc_atoi(&msg->msgHeader[14], 2) - TIMESTAMP_ADJUST_HOUR) * 3600 + Lrc_atoi(&msg->msgHeader[16], 2) * 60 + Lrc_atoi(&msg->msgHeader[18], 2)) * 1000 + Lrc_atoi(&msg->msgHeader[20], 3);

	// SECURITY SYMBOL: 5 bytes
	char cqsSymbol[6];

	strncpy(cqsSymbol, msg->msgBody, 5);
	cqsSymbol[5] = 0;

	char symbol[SYMBOL_LEN];
	if (ConvertCQSSymbolToComstock(cqsSymbol, symbol) == ERROR)
	{
		return SUCCESS;
	}

	// Get stock symbol index
	int symbolIndex = GetStockSymbolIndex(symbol);

	if (symbolIndex == -1)
	{
		symbolIndex = UpdateStockSymbolIndex(symbol);
		if (symbolIndex == -1)
		{
			return SUCCESS;
		}
	}

	if (IsSymbolIgnored(symbol) == 1)
	{
		return SUCCESS;
	}

	// QUOTE CONDITIONS: 1 byte
	quoteInfo.quoteCondition = msg->msgBody[7];

	// Bid Price Denominator: 1 byte
	int numerator = msg->msgBody[9] - 'B';

	// Bid Price 6 bytes
	int whole = Lrc_atoi(&msg->msgBody[10], 6);
	quoteInfo.bidPrice = (double)whole / arrUQDF_DenominatorValue[numerator];

	// Bid Size: 2 bytes
	quoteInfo.bidSize = Lrc_atoi(&msg->msgBody[16], 2);

	// Offer Price Denominator: 1 byte
	numerator = msg->msgBody[18] - 'B';

	// Offer Price 6 bytes
	whole = Lrc_atoi(&msg->msgBody[19], 6);
	quoteInfo.offerPrice = (double)whole / arrUQDF_DenominatorValue[numerator];

	// Offer Size: 2 bytes
	quoteInfo.offerSize = Lrc_atoi(&msg->msgBody[25], 2);

	t_NationalAppendage nationalAppendage;

	// Initialize ...
	nationalAppendage.timeStamp = 0;
	nationalAppendage.bidSize = -1;
	nationalAppendage.offerSize = -1;

	// National BBO Appendage Indicator
	int nationalIndicator = msg->msgBody[27] - 48;
	if (nationalIndicator == 2)
	{
		/* 2:	Short Form National BBO Appendage Attached � A new National BBO
				was generated as a result of the UTP participant�s quote update and the new
				National BBO information is contained in the attached short form appendage*/
		nationalAppendage.timeStamp = quoteInfo.timeStamp;
		ProcessUQDF_ShortNationalBBOAppendage(&msg->msgBody[30], &nationalAppendage);
	}
	else if (nationalIndicator == 3)
	{
		/* 3:	Long Form National BBO Appendage Attached � A new National BBO
				was generated as a result of the UTP participant�s quote update and the new
				information is contained in the attached long form appendage.*/
		nationalAppendage.timeStamp = quoteInfo.timeStamp;
		ProcessUQDF_LongNationalBBOAppendage(&msg->msgBody[30], &nationalAppendage);
	}
	else if (nationalIndicator == 4)
	{
		/* 4:	Quote Contains All National BBO Information � Current UTP
				participant�s quote is itself the National BBO. Vendors should update
				National BBO to reflect this new quote and single market center. (National
				Best Bid Market Center and National Best Ask Market Center fields should be
				updated to reflect the Market Center Originator ID value in the UQDF
				message header.) No appendage is required.*/
		nationalAppendage.timeStamp = quoteInfo.timeStamp;
		nationalAppendage.bidParticipantId = quoteInfo.participantId;
		nationalAppendage.bidPrice = quoteInfo.bidPrice;
		nationalAppendage.bidSize = quoteInfo.bidSize;

		nationalAppendage.offerParticipantId = quoteInfo.participantId;
		nationalAppendage.offerPrice = quoteInfo.offerPrice;
		nationalAppendage.offerSize = quoteInfo.offerSize;
	}
	else if (nationalIndicator == 1)
	{
		/* 1: 	No National BBO Can be Calculated � The National BBO cannot be
				calculated therefore vendors should show National BBO fields as blank. No
				appendage is required.*/
		memset (&nationalAppendage, 0, sizeof(t_NationalAppendage));
	}
	else
	{
		/* 0:	No National BBO Change � The UTP participant�s quote does not affect the
				National BBO. Vendors should continue to show the existing National BBO.
				No appendage is required.*/
	}

	// FINRA ADF MPID Appendage Indicator

	// Add trade to list
	AddQuoteToBBOQuoteMgmt(symbol, &quoteInfo, &nationalAppendage, UQDF_SOURCE);

	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessUQDF_ShortQuote
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessUQDF_ShortQuote(t_UQDF_Msg *msg)
{
	t_BBOQuoteInfo quoteInfo;

	msg->msgHeader[MAX_UQDF_HEADER_LEN] = 0;

	// PARTICIPANT ID
	quoteInfo.participantId = msg->msgHeader[13];

	// TIME STAMP
	quoteInfo.timeStamp = ((Lrc_atoi(&msg->msgHeader[14], 2) - TIMESTAMP_ADJUST_HOUR) * 3600 + Lrc_atoi(&msg->msgHeader[16], 2) * 60 + Lrc_atoi(&msg->msgHeader[18], 2)) * 1000 + Lrc_atoi(&msg->msgHeader[20], 3);

	// SECURITY SYMBOL: 5 bytes
	char cqsSymbol[6];

	strncpy(cqsSymbol, msg->msgBody, 5);
	cqsSymbol[5] = 0;

	char symbol[SYMBOL_LEN];
	if (ConvertCQSSymbolToComstock(cqsSymbol, symbol) == ERROR)
	{
		return SUCCESS;
	}

	// Get stock symbol index
	int symbolIndex = GetStockSymbolIndex(symbol);

	if (symbolIndex == -1)
	{
		symbolIndex = UpdateStockSymbolIndex(symbol);
		if (symbolIndex == -1)
		{
			return SUCCESS;
		}
	}

	if (IsSymbolIgnored(symbol) == 1)
	{
		return SUCCESS;
	}

	// QUOTE CONDITIONS: 1 byte
	quoteInfo.quoteCondition = msg->msgBody[5];

	// Bid Price Denominator: 1 byte
	int numerator = msg->msgBody[6] - 'B';

	// Bid Price 6 bytes
	int whole = Lrc_atoi(&msg->msgBody[7], 6);
	quoteInfo.bidPrice = (double)whole / arrUQDF_DenominatorValue[numerator];

	// Bid Size: 2 bytes
	quoteInfo.bidSize = Lrc_atoi(&msg->msgBody[13], 2);

	// Offer Price Denominator: 1 byte
	numerator = msg->msgBody[15] - 'B';

	// Offer Price 6 bytes
	whole = Lrc_atoi(&msg->msgBody[16], 6);
	quoteInfo.offerPrice = (double)whole / arrUQDF_DenominatorValue[numerator];

	// Offer Size: 2 bytes
	quoteInfo.offerSize = Lrc_atoi(&msg->msgBody[22], 2);

	t_NationalAppendage nationalAppendage;

	// Initialize ...
	nationalAppendage.timeStamp = 0;
	nationalAppendage.bidSize = -1;
	nationalAppendage.offerSize = -1;

	// National BBO Appendage Indicator
	int nationalIndicator = msg->msgBody[24] - 48;
	if (nationalIndicator == 2)
	{
		// 2  Short Form National BBO Appendage Attached
		nationalAppendage.timeStamp = quoteInfo.timeStamp;
		ProcessUQDF_ShortNationalBBOAppendage(&msg->msgBody[26], &nationalAppendage);
	}
	else if (nationalIndicator == 3)
	{
		// 3  Long Form National BBO Appendage Attached
		nationalAppendage.timeStamp = quoteInfo.timeStamp;
		ProcessUQDF_LongNationalBBOAppendage(&msg->msgBody[26], &nationalAppendage);
	}
	else if (nationalIndicator == 4)
	{
		// 4  Quote Contains All National BBO Information
		nationalAppendage.timeStamp = quoteInfo.timeStamp;
		nationalAppendage.bidParticipantId = quoteInfo.participantId;
		nationalAppendage.bidPrice = quoteInfo.bidPrice;
		nationalAppendage.bidSize = quoteInfo.bidSize;

		nationalAppendage.offerParticipantId = quoteInfo.participantId;
		nationalAppendage.offerPrice = quoteInfo.offerPrice;
		nationalAppendage.offerSize = quoteInfo.offerSize;
	}
	else
	{
		// 0  No National BBO Change
		// 1  No National BBO Can be Calculated
	}

	// FINRA ADF MPID Appendage Indicator

	// Add trade to list
	AddQuoteToBBOQuoteMgmt(symbol, &quoteInfo, &nationalAppendage, UQDF_SOURCE);

	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessUQDF_ShortNationalBBOAppendage
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
void ProcessUQDF_ShortNationalBBOAppendage(char *msg, t_NationalAppendage *nationalAppendage)
{
	//BEST BID PARTICIPANT ID: 1 Byte
	nationalAppendage->bidParticipantId = msg[1];

	// Bid Price Denominator: 1 byte
	int numerator = msg[2] - 'B';

	// Bid Price 6 bytes
	int whole = Lrc_atoi(&msg[3], 6);
	nationalAppendage->bidPrice = (double)whole / arrUQDF_DenominatorValue[numerator];

	// Bid Size: 2 bytes
	nationalAppendage->bidSize = Lrc_atoi(&msg[9], 2);

	//RESERVED: 1

	//BEST Offer PARTICIPANT ID: 1 Byte
	nationalAppendage->offerParticipantId = msg[12];

	// Offer Price Denominator: 1 byte
	numerator = msg[13] - 'B';

	// Offer Price 6 bytes
	whole = Lrc_atoi(&msg[14], 6);
	nationalAppendage->offerPrice = (double)whole / arrUQDF_DenominatorValue[numerator];

	// Offer Size: 2 bytes
	nationalAppendage->offerSize = Lrc_atoi(&msg[20], 2);

	//RESERVED : 1
	return;
}

/****************************************************************************
- Function name:	ProcessUQDF_LongNationalBBOAppendage
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
void ProcessUQDF_LongNationalBBOAppendage(char *msg, t_NationalAppendage *nationalAppendage)
{
	//BEST BID PARTICIPANT ID: 1 Byte
	nationalAppendage->bidParticipantId = msg[1];

	// Bid Price Denominator: 1 byte
	int numerator = msg[2] - 'B';

	// Bid Price 10 bytes
	int whole = Lrc_atoi(&msg[3], 10);
	nationalAppendage->bidPrice = (double)whole / arrUQDF_DenominatorValue[numerator];

	// Bid Size: 7 bytes
	nationalAppendage->bidSize = Lrc_atoi(&msg[13], 7);

	//RESERVED: 1

	//BEST Offer PARTICIPANT ID: 1 Byte
	nationalAppendage->offerParticipantId = msg[21];

	// Offer Price Denominator: 1 byte
	numerator = msg[22] - 'B';

	// Offer Price 10 bytes
	whole = Lrc_atoi(&msg[23], 10);
	nationalAppendage->offerPrice = (double)whole / arrUQDF_DenominatorValue[numerator];

	// Offer Size: 7 bytes
	nationalAppendage->offerSize = Lrc_atoi(&msg[33], 7);

	return;
}

/****************************************************************************
- Function name:	DisconnectUQDF_Multicast
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int DisconnectUQDF_Multicast(void)
{
	UQDF_StatusMgmt.shouldConnect = NO;
	UQDF_StatusMgmt.isConnected = DISCONNECTED;

	int i;

	for (i = 0; i < MAX_UQDF_CHANNELS; i++)
	{
		UQDF_Conf.group[i].dataInfo.connectionFlag = 0;
		sem_post(&UQDF_Conf.group[i].dataInfo.sem);
	}

	return SUCCESS;
}

/****************************************************************************
- Function name:	ReleaseUQDF_MulticastResource
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ReleaseUQDF_MulticastResource()
{
	int i, ret;
	int isError = 0;
	pthread_mutex_lock(&DBLMgmt.mutexLock);
	int needToRelease = (IsDBLTimedOut(UQDF_Conf.dev) == 0) ? 1:0;

	for (i = 0; i < MAX_UQDF_CHANNELS; i++)
	{
		if (UQDF_Conf.group[i].dataChannel != NULL)
		{
			if (needToRelease == 1)
			{
				if (isError == 0)
				{
					ret = LeaveDBLMulticastGroup(UQDF_Conf.group[i].dataChannel, UQDF_Conf.group[i].dataIP);
					if (ret != EADDRNOTAVAIL)
						dbl_unbind(UQDF_Conf.group[i].dataChannel);
					else
						isError = 1;
				}
			}
			
			UQDF_Conf.group[i].dataChannel = NULL;
		}

		sem_destroy(&UQDF_Conf.group[i].dataInfo.sem);
	}

	if (UQDF_Conf.dev != NULL)
	{
		UQDF_Conf.dev = NULL;
	}

	pthread_mutex_unlock(&DBLMgmt.mutexLock);
	return SUCCESS;
}
