/*****************************************************************************
**	Project:		Equity Arbitrage Application
**	Filename:		raw_data_mgmt.c
**	Description:	This file contains function definitions that were declared
					in raw_data_mgmt.h	
**	Author:			Luan Vo-Kinh
**	First created:	13-Sep-2007
**	Last updated:	-----------
*****************************************************************************/

/****************************************************************************
** Include files and define several constants
****************************************************************************/
//#include <stdlib.h>
//#include <sys/types.h>
//#include <sys/stat.h>
//#include <unistd.h>
#define _XOPEN_SOURCE
#include <time.h>

#include "configuration.h"
#include "nasdaq_rash_proc.h"
#include "arca_direct_proc.h"
#include "order_mgmt_proc.h"
#include "nasdaq_rash_data.h"
#include "raw_data_mgmt.h"

unsigned char currentTimestamp[MAX_TIMESTAMP];

/****************************************************************************
** Global variables definition
****************************************************************************/
t_RawDataMgmt pmRawDataMgmt[MAX_ORDER_CONNECTIONS];
int CrossIDCollection[MAX_STOCK_SYMBOL];

/****************************************************************************
** Function declarations
****************************************************************************/
/****************************************************************************
- Function name:	InitializeRawDataManegementStructures
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int InitializeRawDataManegementStructures(void)
{
	int orderIndex;

	// Raw data at Aggregration Server
	for (orderIndex = 0; orderIndex < MAX_ORDER_CONNECTIONS; orderIndex++)
	{
		pmRawDataMgmt[orderIndex].fileDesc = NULL;
		pmRawDataMgmt[orderIndex].orderIndex = orderIndex;
		pmRawDataMgmt[orderIndex].tsId = MANUAL_ORDER;
		pmRawDataMgmt[orderIndex].lastUpdatedIndex = -1;
		pmRawDataMgmt[orderIndex].countRawDataBlock = 0;
		pmRawDataMgmt[orderIndex].totalSentRawData = 0;
		pmRawDataMgmt[orderIndex].isProcessed = NOT_YET;
		sprintf(pmRawDataMgmt[orderIndex].fileName, "%s_rawdata_pm.txt", fileNamePrefix[orderIndex]);

		pthread_mutex_init(&pmRawDataMgmt[orderIndex].updateMutex, NULL);

		memset(&pmRawDataMgmt[orderIndex].dataBlockCollection, 0, MAX_RAW_DATA_ENTRIES * sizeof(t_DataBlock));
	}

	memset(CrossIDCollection, 0, sizeof(CrossIDCollection));
	
	return 0;
}

/****************************************************************************
- Function name:	LoadRawDataEntries
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int LoadRawDataEntries(void)
{
	int orderIndex;

	// Raw data at Aggregration Server
	for (orderIndex = 0; orderIndex < MAX_ORDER_CONNECTIONS; orderIndex++)
	{
		if (LoadRawDataEntriesFromFile(pmRawDataMgmt[orderIndex].fileName, &pmRawDataMgmt[orderIndex]) == ERROR)
		{
			return ERROR;
		}
	}

	return SUCCESS;
}

/****************************************************************************
- Function name:	LoadRawDataEntriesFromFile
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int LoadRawDataEntriesFromFile(const char *fileName, void *orderRawDataMgmt)
{
	t_RawDataMgmt *workingOrderMgmt = (t_RawDataMgmt *)orderRawDataMgmt;
	
	// Get full raw data path
	char fullPath[MAX_PATH_LEN] = "\0";
	
	if (GetFullRawDataPath(fileName, fullPath) == NULL_POINTER)
	{
		TraceLog(ERROR_LEVEL, "Have some problem in get full raw data path operation (%s)\n", fileName);

		return ERROR;
	}

	TraceLog(DEBUG_LEVEL, "Load raw data: %s\n", fullPath);
	
	if (IsFileTimestampToday(fullPath) == 0)
	{
		TraceLog(ERROR_LEVEL, "%s is old, please check correctness of data files\n", fullPath);
		exit(0);
	}
	
	/* 
	Open file to read in BINARY MODE, 
	if it is not existed, we will create it
	*/
	FILE *fileDesc = fopen(fullPath, "ab+");
	if (fileDesc == NULL)
	{
		TraceLog(ERROR_LEVEL, "Cannot open file '%s' to read or we don't have permission to create\n", fullPath);

		return ERROR;
	}
	else
	{
		// Store pointer to file that will be used after
		workingOrderMgmt->fileDesc = fileDesc;
		
		struct stat fileStat;
		int retValue = stat(fullPath, &fileStat);
		
		if (retValue == SUCCESS)
		{
			// Get file size
			int fileSizeInBytes = fileStat.st_size;

			// Check file size
			if (fileSizeInBytes % sizeof(t_DataBlock) != 0)
			{
				TraceLog(ERROR_LEVEL, "INVALID FILE SIZE: fileName = %s, fileSize = %d\n", fullPath, fileSizeInBytes);

				return ERROR;
			}
			
			t_DataBlock dataBlock;

			workingOrderMgmt->lastUpdatedIndex = -1;
			workingOrderMgmt->countRawDataBlock = 0;

			int countRawData = fileSizeInBytes / sizeof(t_DataBlock);

			while (fread(&dataBlock, sizeof( t_DataBlock ), 1, workingOrderMgmt->fileDesc) == 1)
			{
				AddDataBlockToCollection(&dataBlock, workingOrderMgmt);
			}

			if (countRawData != workingOrderMgmt->countRawDataBlock)
			{
				TraceLog(ERROR_LEVEL, "Cannot load raw data, fileName = %s\n", fullPath);
			}

			TraceLog(DEBUG_LEVEL, "Count raw data entries: %d\n", workingOrderMgmt->countRawDataBlock);
		}
		else
		{
			return ERROR;
		}
	}	
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	BuildPMRawDataFromFile
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int BuildPMRawDataFromFile( t_ASMessage *message, t_RawDataMgmt *rawDataMgmt )
{
	t_RawDataMgmt *workingOrderMgmt = rawDataMgmt;
	t_ASMessage *asMessage = message;

	t_IntConverter intConvert;
	int numDataBlockSend = 0;
	t_DataBlock dataBlock;

	
	// Set cursor position for file at begin of specific data block
	fseek(workingOrderMgmt->fileDesc, workingOrderMgmt->totalSentRawData * sizeof(t_DataBlock), SEEK_SET);	

	while (fread(&dataBlock, sizeof( t_DataBlock ), 1, workingOrderMgmt->fileDesc) == 1)
	{
		numDataBlockSend ++;
		if (numDataBlockSend >= MAX_OPEN_ORDER_SEND)
		{
			break;
		}

		//dataBlock.blockLen
		intConvert.value = dataBlock.blockLen;
		asMessage->msgContent[asMessage->msgLen] = intConvert.c[0];
		asMessage->msgContent[asMessage->msgLen + 1] = intConvert.c[1];
		asMessage->msgContent[asMessage->msgLen + 2] = intConvert.c[2];
		asMessage->msgContent[asMessage->msgLen + 3] = intConvert.c[3];

		// Update index
		asMessage->msgLen += 4;

		//dataBlock.msgLen
		intConvert.value = dataBlock.msgLen;
		asMessage->msgContent[asMessage->msgLen] = intConvert.c[0];
		asMessage->msgContent[asMessage->msgLen + 1] = intConvert.c[1];
		asMessage->msgContent[asMessage->msgLen + 2] = intConvert.c[2];
		asMessage->msgContent[asMessage->msgLen + 3] = intConvert.c[3];

		// Update index
		asMessage->msgLen += 4;

		//dataBlock.msgContent
		memcpy(&asMessage->msgContent[asMessage->msgLen], dataBlock.msgContent, dataBlock.msgLen);

		// Update index
		asMessage->msgLen += dataBlock.msgLen;
		
		//dataBlock.addLen
		intConvert.value = dataBlock.addLen;
		asMessage->msgContent[asMessage->msgLen] = intConvert.c[0];
		asMessage->msgContent[asMessage->msgLen + 1] = intConvert.c[1];
		asMessage->msgContent[asMessage->msgLen + 2] = intConvert.c[2];
		asMessage->msgContent[asMessage->msgLen + 3] = intConvert.c[3];

		// Update index
		asMessage->msgLen += 4;

		//dataBlock.addContent
		memcpy(&asMessage->msgContent[asMessage->msgLen], dataBlock.addContent, dataBlock.addLen);

		// Update index
		asMessage->msgLen += dataBlock.addLen;

		// Update totalSentRawData
		workingOrderMgmt->totalSentRawData += 1;
	}

	return asMessage->msgLen;
}

/****************************************************************************
- Function name:	AddDataBlockToCollection
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int AddDataBlockToCollection(t_DataBlock *dataBlock, t_RawDataMgmt *orderRawDataMgmt)
{
	t_RawDataMgmt *workingOrderMgmt = orderRawDataMgmt;

	pthread_mutex_lock(&workingOrderMgmt->updateMutex);
			
	// We will store new data block at last updated index + 1
	workingOrderMgmt->lastUpdatedIndex++;
	
	// Add new data block to collection
	workingOrderMgmt->dataBlockCollection[workingOrderMgmt->lastUpdatedIndex % MAX_RAW_DATA_ENTRIES] = *dataBlock;
	
	// Increase total of data blocks
	workingOrderMgmt->countRawDataBlock++;

	pthread_mutex_unlock(&workingOrderMgmt->updateMutex);
	
	return 0;
}

/****************************************************************************
- Function name:	SaveDataBlockToFile
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int SaveDataBlockToFile(t_RawDataMgmt *orderRawDataMgmt)
{
	t_RawDataMgmt *workingOrderMgmt = orderRawDataMgmt;

	// Save new data blocks at last updated index
	if (fwrite(&workingOrderMgmt->dataBlockCollection[workingOrderMgmt->lastUpdatedIndex % MAX_RAW_DATA_ENTRIES], sizeof(t_DataBlock), 1, workingOrderMgmt->fileDesc) != 1)
	{
		TraceLog(ERROR_LEVEL, "Cannot save block data to file\n");

		return ERROR;
	}
	
	// Flush all data from buffer to file
	fflush(workingOrderMgmt->fileDesc);
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	LoadRawDataEntries
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessRawDataEntries(void)
{
	int orderIndex;

	// Raw data at PM
	for (orderIndex = 0; orderIndex < MAX_ORDER_CONNECTIONS; orderIndex++)
	{
		if (pmRawDataMgmt[orderIndex].countRawDataBlock < MAX_RAW_DATA_ENTRIES)
		{
			ProcessRawDataEntriesFromCollecion(&pmRawDataMgmt[orderIndex]);
		}
		else
		{
			ProcessRawDataEntriesFromFile(&pmRawDataMgmt[orderIndex]);
		}
	}

	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessRawDataEntriesFromCollecion
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessRawDataEntriesFromCollecion(t_RawDataMgmt *orderRawDataMgmt)
{
	t_RawDataMgmt *workingOrderMgmt = orderRawDataMgmt;
	
	int i;
	char fieldMsgType;
	char fieldStatus;

	for (i = 0; i < workingOrderMgmt->countRawDataBlock; i++)
	{
		switch (workingOrderMgmt->orderIndex)
		{
			case NASDAQ_RASH_INDEX:
				// Get message type of current message
				fieldMsgType = workingOrderMgmt->dataBlockCollection[i].msgContent[0];
				
				if (fieldMsgType == 'U')
				{
					fieldStatus = workingOrderMgmt->dataBlockCollection[i].msgContent[1];

					if (fieldStatus == 'O')
					{
						ProcessNewOrderOfRASH(&workingOrderMgmt->dataBlockCollection[i]);
					}
				}
				if (fieldMsgType == 'S')
				{
					fieldStatus = workingOrderMgmt->dataBlockCollection[i].msgContent[9];

					if (fieldStatus == 'A')
					{
						ProcessAcceptedOrderOfRASH(workingOrderMgmt->dataBlockCollection[i], NO);
					}
					else if (fieldStatus == 'E')
					{
						ProcessExecutedOrderOfRASH(workingOrderMgmt->dataBlockCollection[i]);
					}
					else if (fieldStatus == 'C')
					{
						ProcessCanceledOrderOfRASH(workingOrderMgmt->dataBlockCollection[i]);
					}
					else if (fieldStatus == 'J')
					{
						ProcessRejectedOrderOfRASH(workingOrderMgmt->dataBlockCollection[i]);
					}
				}

				break;
			
			case ARCA_DIRECT_INDEX:
				// Get message type of current message
				fieldMsgType = workingOrderMgmt->dataBlockCollection[i].msgContent[0];

				if (fieldMsgType == NEW_ORDER_TYPE)
				{
					if(workingOrderMgmt->dataBlockCollection[i].msgContent[1] == 1)
					{
						ProcessARCA_DIRECT_NewOrder_V1(&workingOrderMgmt->dataBlockCollection[i]);
					}
					else
					{
						ProcessARCA_DIRECT_NewOrder_V3(&workingOrderMgmt->dataBlockCollection[i]);
					}
				}
				else if (fieldMsgType == ORDER_ACK_TYPE)
				{
					ProcessARCA_DIRECT_OrderAckMsg(&workingOrderMgmt->dataBlockCollection[i], NO);
				}
				else if(fieldMsgType == ORDER_FILLED_TYPE)
				{
					ProcessARCA_DIRECT_OrderFilledMsg(&workingOrderMgmt->dataBlockCollection[i]);
				}
				else if(fieldMsgType == ORDER_KILLED_TYPE)
				{
					ProcessARCA_DIRECT_OrderKilledMsg(&workingOrderMgmt->dataBlockCollection[i]);
				}
				else if(fieldMsgType == ORDER_REJECTED_TYPE)
				{
					ProcessARCA_DIRECT_OrderRejectedMsg(&workingOrderMgmt->dataBlockCollection[i]);
				}
				break;
			
			default:
				TraceLog(ERROR_LEVEL, "ProcessRawDataOfPMFromCollection: INVALID ECN ORDER, orderIndex = %d\n", workingOrderMgmt->orderIndex);
				break;		
		}
	}
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessRawDataEntriesFromFile
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessRawDataEntriesFromFile(t_RawDataMgmt *orderRawDataMgmt)
{
	t_RawDataMgmt *workingOrderMgmt = orderRawDataMgmt;	
			
	t_DataBlock dataBlock;
	char fieldMsgType;
	char fieldStatus;
	
	rewind(workingOrderMgmt->fileDesc);

	while (fread(&dataBlock, sizeof(t_DataBlock), 1, workingOrderMgmt->fileDesc) == 1)
	{
		switch (workingOrderMgmt->orderIndex)
		{
			case NASDAQ_RASH_INDEX:
				// Get message type of current message
				fieldMsgType = dataBlock.msgContent[0];
				
				if (fieldMsgType == 'U')
				{
					fieldStatus = dataBlock.msgContent[1];

					if (fieldStatus == 'O')
					{
						ProcessNewOrderOfRASH(&dataBlock);
					}
				}
				if (fieldMsgType == 'S')
				{
					fieldStatus = dataBlock.msgContent[9];

					if (fieldStatus == 'A')
					{
						ProcessAcceptedOrderOfRASH(dataBlock, NO);
					}
					else if (fieldStatus == 'E')
					{
						ProcessExecutedOrderOfRASH(dataBlock);
					}
					else if (fieldStatus == 'C')
					{
						ProcessCanceledOrderOfRASH(dataBlock);
					}
					else if (fieldStatus == 'J')
					{
						ProcessRejectedOrderOfRASH(dataBlock);
					}
				}

				break;

			case ARCA_DIRECT_INDEX:
			
				fieldMsgType = dataBlock.msgContent[0];

				if (fieldMsgType == NEW_ORDER_TYPE)
				{
					if(dataBlock.msgContent[1] == 1)
					{
						ProcessARCA_DIRECT_NewOrder_V1(&dataBlock);
					}
					else
					{
						ProcessARCA_DIRECT_NewOrder_V3(&dataBlock);
					}
				}
				else if (fieldMsgType == ORDER_ACK_TYPE)
				{
					ProcessARCA_DIRECT_OrderAckMsg(&dataBlock, NO);
				}
				else if(fieldMsgType == ORDER_FILLED_TYPE)
				{
					ProcessARCA_DIRECT_OrderFilledMsg(&dataBlock);
				}
				else if(fieldMsgType == ORDER_KILLED_TYPE)
				{
					ProcessARCA_DIRECT_OrderKilledMsg(&dataBlock);
				}
				else if(fieldMsgType == ORDER_REJECTED_TYPE)
				{
					ProcessARCA_DIRECT_OrderRejectedMsg(&dataBlock);
				}

				break;
			
			default:
				TraceLog(ERROR_LEVEL, "ProcessRawDataEntriesFromFile: INVALID ECN ORDER, orderIndex = %d\n", workingOrderMgmt->orderIndex);
				break;		
		}
	}
	
	return SUCCESS;
}

/***************************************************************************/
