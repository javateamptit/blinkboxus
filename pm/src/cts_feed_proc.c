/****************************************************************************
** Project name:	Equity Arbitrage Application
** File name: 		cts_feed_proc.c
** Description: 	This file contains function definitions that were declared
					in cts_feed_proc.h
** Author: 			Luan Vo-Kinh
** First created on May 02, 2008
** Last updated on May 02, 2008
****************************************************************************/

#define _BSD_SOURCE

#include <pthread.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>

#include "utility.h"
#include "book_mgmt.h"
#include "socket_util.h"
#include "trading_mgmt.h"
#include "cts_feed_proc.h"
#include "kernel_algorithm.h"
#include "aggregation_server_proc.h"

#define MAX_RECV_CTS_TIMEOUT 150

int arrCTS_DenominatorValue[26];
int arrCTS_NumeratorByte[26];

typedef struct t_CTS_arg
{
	int lineIndex;
	char type;
} t_CTS_arg;

extern t_MonitorCTS MonitorCTS[MAX_MULTICAST_LINES];
extern t_DBLMgmt DBLMgmt;
//****************************************************************************
/****************************************************************************
- Function name:	InitializeDenominatorValue
- Input:			
- Output:											
- Return:			
- Description:						 							  				
- Usage:			
****************************************************************************/
int InitializeDenominatorValue(void)
{
	arrCTS_DenominatorValue[1] = 1;
	arrCTS_DenominatorValue[2] = 1;
	arrCTS_DenominatorValue[9] = 1;
	arrCTS_DenominatorValue[10] = 1;
	arrCTS_DenominatorValue[11] = 1;
	arrCTS_DenominatorValue[12] = 1;
	arrCTS_DenominatorValue[13] = 1;
	arrCTS_DenominatorValue[14] = 1;
	arrCTS_DenominatorValue[15] = 1;
	arrCTS_DenominatorValue[16] = 1;
	arrCTS_NumeratorByte[1] = 1;
	arrCTS_NumeratorByte[2] = 1;
	arrCTS_NumeratorByte[9] = 1;
	arrCTS_NumeratorByte[10] = 1;
	arrCTS_NumeratorByte[11] = 1;
	arrCTS_NumeratorByte[12] = 1;
	arrCTS_NumeratorByte[13] = 1;
	arrCTS_NumeratorByte[14] = 1;
	arrCTS_NumeratorByte[15] = 1;
	arrCTS_NumeratorByte[16] = 1;
	
	arrCTS_DenominatorValue[0] = 1;
	arrCTS_DenominatorValue[3] = 8;
	arrCTS_DenominatorValue[4] = 16;
	arrCTS_DenominatorValue[5] = 32;
	arrCTS_DenominatorValue[6] = 64;
	arrCTS_DenominatorValue[7] = 128;
	arrCTS_DenominatorValue[8] = 256;
	arrCTS_DenominatorValue[17] = 10;
	arrCTS_DenominatorValue[18] = 100;
	arrCTS_DenominatorValue[19] = 1000;
	arrCTS_DenominatorValue[20] = 10000;
	arrCTS_DenominatorValue[21] = 100000;
	arrCTS_DenominatorValue[22] = 1000000;
	arrCTS_DenominatorValue[23] = 10000000;
	arrCTS_DenominatorValue[24] = 100000000;
	arrCTS_DenominatorValue[25] = 1;
	
	arrCTS_NumeratorByte[0] = 1;
	arrCTS_NumeratorByte[3] = 1;
	arrCTS_NumeratorByte[4] = 2;
	arrCTS_NumeratorByte[5] = 2;
	arrCTS_NumeratorByte[6] = 2;
	arrCTS_NumeratorByte[7] = 3;
	arrCTS_NumeratorByte[8] = 3;
	arrCTS_NumeratorByte[17] = 1;
	arrCTS_NumeratorByte[18] = 2;
	arrCTS_NumeratorByte[19] = 3;
	arrCTS_NumeratorByte[20] = 4;
	arrCTS_NumeratorByte[21] = 5;
	arrCTS_NumeratorByte[22] = 6;
	arrCTS_NumeratorByte[23] = 7;
	arrCTS_NumeratorByte[24] = 8;
	arrCTS_NumeratorByte[25] = 0;
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	InitCTS_Feed_DataStructure
- Input:			
- Output:											
- Return:			
- Description:						 							  				
- Usage:			
****************************************************************************/
void InitCTS_Feed_DataStructure(void)
{	
	TraceLog(DEBUG_LEVEL, "Initialize CTS Feed\n");

	// Initialize denominator value
	InitializeDenominatorValue();
		
	// Get full raw data path
	//char fullPath[MAX_PATH_LEN] = "\0";
	
	int i;

	CTS_Feed_Conf.dev = NULL;

	for (i = 0; i < MAX_MULTICAST_LINES; i++)
	{
		MonitorCTS[i].recvCounter = 0;
		MonitorCTS[i].procCounter = 0;
		
		CTS_Feed_Conf.group[i].dataChannel = NULL;

		CTS_Feed_Conf.group[i].index = i;

		sem_init(&CTS_Feed_Conf.group[i].dataInfo.sem, 0, 0);

		// Initialize buffer that will be used to contain packets from server
		memset(CTS_Feed_Conf.group[i].dataInfo.buffer, 0, MAX_BOOK_MSG_IN_BUFFER * MAX_MTU);
		strcpy(CTS_Feed_Conf.group[i].dataInfo.bookName, "CTS");
		CTS_Feed_Conf.group[i].dataInfo.DisconnectBook = &DisconnectCTS_Feed;
		
		// Initialize counter variables
		CTS_Feed_Conf.group[i].primaryLastSequenceNumber = -1;
		CTS_Feed_Conf.group[i].dataInfo.currentReceivedIndex = 0;
		CTS_Feed_Conf.group[i].dataInfo.currentReadIndex = 0;
		CTS_Feed_Conf.group[i].dataInfo.connectionFlag = 0;
	}
}

/****************************************************************************
- Function name:	CTS_Feed_Thread
- Input:			
- Output:											
- Return:			
- Description:						 							  				
- Usage:			
****************************************************************************/
void *CTS_Feed_Thread(void *agrs)
{
	SetKernelAlgorithm("[CTS_BASE]");
	
	InitCTS_Feed_DataStructure();
	
	int i;

	while (CTS_StatusMgmt.shouldConnect == YES)
	{
		int ret = CreateDBLDeviceV2(CTS_Feed_Conf.NICName, &CTS_Feed_Conf.dev);

		if (ret == ERROR)
		{
			TraceLog(ERROR_LEVEL, "CTS FEED: Can't create DBL device\n");

			DisconnectCTS_Feed();
			return NULL;
		}

		for (i = 0; i < MAX_MULTICAST_LINES; i++)
		{
			/* Start threads to receive and process data from primary data feed */
			if (pthread_create(&CTS_Feed_Conf.group[i].primaryProcessThreadID, NULL, (void*)&ThreadProcessCTS_PrimaryData, (void*) &CTS_Feed_Conf.group[i]) != SUCCESS)
			{
				TraceLog(ERROR_LEVEL, "Cannot create thread to process packets\n");
				
				DisconnectCTS_Feed();
				
				return NULL;
			}
			
			TraceLog(DEBUG_LEVEL, "CTS FEED: MulticastRecvPort(%s:%d) group(%d): connecting\n", CTS_Feed_Conf.group[i].dataIP, CTS_Feed_Conf.group[i].dataPort, (i+1));
			/* Join to Primary multicast group */
			ret = JoinMulticastGroupUsingExistingDeviceV2(CTS_Feed_Conf.dev, CTS_Feed_Conf.group[i].dataIP, CTS_Feed_Conf.group[i].dataPort, &CTS_Feed_Conf.group[i].dataChannel, CTS_Feed_Conf.NICName, (void*) &CTS_Feed_Conf.group[i].dataInfo);

			if (ret == ERROR)
			{
				TraceLog(ERROR_LEVEL, "CTS: Can't connect to Multicast group (%s:%d)\n", CTS_Feed_Conf.group[i].dataIP, CTS_Feed_Conf.group[i].dataPort);

				DisconnectCTS_Feed();

				return NULL;
			}
			
			// Sleep 1000 micro-seconds
			usleep(1000);

		}

		// Notify that CTS Feed was connected
		CTS_StatusMgmt.isConnected = CONNECTED;
		for (i = 0; i < MAX_MULTICAST_LINES; i++)
		{
			CTS_Feed_Conf.group[i].dataInfo.connectionFlag = 1;
		}
		
		// Wait for ALL threads completed
		for (i = 0; i < MAX_MULTICAST_LINES; i++)
		{
			pthread_join(CTS_Feed_Conf.group[i].primaryProcessThreadID, NULL);
		}

		// Release CTS Feed Resource
		ReleaseCTS_FeedResource();

		InitCTS_Feed_DataStructure();
	}
	
	// Notify CTS Feed was disconnected
	CTS_StatusMgmt.isConnected = DISCONNECTED;
	SendPMDisconnectedBookOrderAlertToAS(SERVICE_CTS_FEED);
	
	TraceLog(DEBUG_LEVEL, "CTS Feed was disconnected\n");
	
	return NULL;
}

/****************************************************************************
- Function name:	ThreadProcessCTS_PrimaryData
- Input:			
- Output:											
- Return:			
- Description:						 							  				
- Usage:			
****************************************************************************/
void *ThreadProcessCTS_PrimaryData(void *args)
{
	SetKernelAlgorithm("[CTS_PROCESS_MSGS]");

	t_CTS_Feed_Group *workingGroup = (t_CTS_Feed_Group *)args;

	// Temp buffer need to store data from receive buffer
	char *tempBuffer;	
	int index;
	
	while (CTS_StatusMgmt.shouldConnect == YES)
	{
		// Wait for new message
		if (sem_wait(&workingGroup->dataInfo.sem) != SUCCESS)
		{
			TraceLog(ERROR_LEVEL, "Cannot sem_wait. CTS Feed will be stopped now (%s %d)\n", workingGroup->dataIP, workingGroup->dataPort);
			DisconnectCTS_Feed();
			
			return NULL;
		}
		
		if (CTS_StatusMgmt.shouldConnect == NO)
		{
			return NULL;
		}

		index = workingGroup->dataInfo.currentReadIndex;
		tempBuffer = workingGroup->dataInfo.buffer[index];

		if (tempBuffer[MAX_MTU - 1] != STORED)
		{
			TraceLog(ERROR_LEVEL, "CTS: Receive buffer of line %d (group A) is full: received index = %d, read index = %d\n",
				workingGroup->index + 1, workingGroup->dataInfo.currentReceivedIndex, workingGroup->dataInfo.currentReadIndex);

			DisconnectCTS_Feed();
			
			return NULL;
		}

		ProcessCTS_MsgFromPrimaryData(workingGroup, tempBuffer);

		// Update status of buffer
		tempBuffer[MAX_MTU - 1] = READ;

		/*
		If we have Current_Read_Index greater than LIMITATION of INTEGER data type
		that is very dangerous
		*/
		if(workingGroup->dataInfo.currentReadIndex == MAX_BOOK_MSG_IN_BUFFER - 1)
		{
			workingGroup->dataInfo.currentReadIndex = 0;
		}
		else
		{
			workingGroup->dataInfo.currentReadIndex++;
		}
	}
	
	return NULL;	
}

/****************************************************************************
- Function name:	ProcessCTS_MsgFromPrimaryData
- Input:			
- Output:											
- Return:			
- Description:						 							  				
- Usage:			
****************************************************************************/
int ProcessCTS_MsgFromPrimaryData(void *args, char buffer[MAX_MTU])
{
	t_CTS_Feed_Group *workingGroup = (t_CTS_Feed_Group *)args;

	int index, icount;
	t_CTS_Msg msgTmp;
	char buffTemp[MAX_MTU];

	if (buffer[0] != SOH_CHAR)
	{
		TraceLog(ERROR_LEVEL, "CTS: Invalid packet from primary data feed, msg = %s\n", buffer);

		return ERROR;
	}

	// Update index
	index = 1;

	while ((index < MAX_MTU - 1) && 
			(buffer[index] != ETX_CHAR))
	{
		icount = index + MAX_CTS_HEADER_LEN;

		while ((icount < MAX_MTU - 1) && (buffer[icount] != US_CHAR) && (buffer[icount] != ETX_CHAR))
		{
			icount++;
		}

		if (icount == MAX_MTU - 1)
		{
			TraceLog(ERROR_LEVEL, "Invalid packet from primary data feed, msg = %s\n", buffer);

			return ERROR;
		}
		
		// Check if this is the needed packet: Retransmission Requester is original
		if ( (buffer[index + 4] == 32)  && 
			((buffer[index + 3] == 'O') || (buffer[index + 3] == 'V')) )
		{
			// Get message header
			msgTmp.msgCategory = buffer[index];
			msgTmp.msgType = buffer[index + 1];
			msgTmp.msgSeqNum = atoi(&buffer[index + 8]);

			if (workingGroup->primaryLastSequenceNumber == -1)
			{
				workingGroup->primaryLastSequenceNumber = msgTmp.msgSeqNum;
			}
			else
			{
				if (msgTmp.msgSeqNum > workingGroup->primaryLastSequenceNumber + 1)
				{
					
				}
				else if (msgTmp.msgSeqNum < workingGroup->primaryLastSequenceNumber)
				{
					if ((msgTmp.msgCategory != 'C') || (msgTmp.msgType != 'L')) 
					{
						
					}
				}

				workingGroup->primaryLastSequenceNumber = msgTmp.msgSeqNum;
			}

			if ((msgTmp.msgCategory == 'A') || (msgTmp.msgCategory == 'B') || (msgTmp.msgCategory == 'L') || (msgTmp.msgCategory == 'M') || (msgTmp.msgCategory == 'Y'))
			{
				// Do nothing ...
			}
			else if (msgTmp.msgCategory == 'C')
			{
				if (msgTmp.msgType == 'L')
				{
					// Sequence number reset
					TraceLog(DEBUG_LEVEL, "CTS: Received message sequence number reset of line %d (group A), we will reset sequence number now\n", workingGroup->index + 1);

					workingGroup->primaryLastSequenceNumber = msgTmp.msgSeqNum;
				}
				else if (msgTmp.msgType == 'T')
				{
					// Line Integrity
				}
				else
				{
					// Do nothing ...
					// msgType = 'I' for "Start of Day"
					// msgType = 'M' for "Start of Test Cycle"
					// msgType = 'N' for "End of Test Cycle"
					// msgType = 'Z' for "End of Transmission"
				}
			}
			else if (msgTmp.msgCategory == 'E')
			{
				if (msgTmp.msgType == 'B')
				{
					// Equity long trade
					memcpy(msgTmp.msgHeader, &buffer[index], MAX_CTS_HEADER_LEN);
					memcpy(msgTmp.msgBody, &buffer[index + MAX_CTS_HEADER_LEN], icount - (index + MAX_CTS_HEADER_LEN));
					msgTmp.msgBody[icount - (index + MAX_CTS_HEADER_LEN)] = 0;

					ProcessCTS_LongTradeMsg(&msgTmp);
				}
				else if (msgTmp.msgType == 'I')
				{
					// Equity short trade
					memcpy(msgTmp.msgHeader, &buffer[index], MAX_CTS_HEADER_LEN);
					memcpy(msgTmp.msgBody, &buffer[index + MAX_CTS_HEADER_LEN], icount - (index + MAX_CTS_HEADER_LEN));
					msgTmp.msgBody[icount - (index + MAX_CTS_HEADER_LEN)] = 0;

					ProcessCTS_ShortTradeMsg(&msgTmp);
				}
				/*else if (msgTmp.msgType == 'F')
				{
				}
				else
				{
					// Do nothing ...
				}*/
			}
			else
			{
				// Other message
				memcpy(buffTemp, &buffer[index], icount - index);
				buffTemp[icount - index] = 0;

				TraceLog(WARN_LEVEL, "CTS unknown message: category = %c, msgType = %c, msg = %s\n", msgTmp.msgCategory, msgTmp.msgType, buffTemp);
			}
		}
		
		if (buffer[icount] == US_CHAR)
		{
			icount++;
		}

		// Increasing index
		index = icount;
	}

	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessCTS_LongTradeMsg
- Input:			
- Output:											
- Return:			
- Description:						 							  				
- Usage:			
****************************************************************************/
int ProcessCTS_LongTradeMsg(t_CTS_Msg *msg)
{
	t_TradeInfo trade;

	msg->msgHeader[MAX_CTS_HEADER_LEN] = 0;

	// Data Feed
	trade.dataFeed = CTS_DATA;
	
	// PARTICIPANT ID
	trade.participantId = msg->msgHeader[17];

	// TIME STAMP
	trade.timestamp = (((msg->msgHeader[18] - 48) - TIMESTAMP_ADJUST_HOUR) * 3600 + (msg->msgHeader[19] - 48) * 60 + (msg->msgHeader[20] - 48)) * 1000 + Lrc_atoi(&msg->msgHeader[21], 3);

	// SECURITY SYMBOL: 11 bytes
	char cqsSymbol[12];
	strncpy(cqsSymbol, msg->msgBody, 11);
	cqsSymbol[11] = 0;
	
	char symbol[SYMBOL_LEN];
	if (ConvertCQSSymbolToComstock(cqsSymbol, symbol) == ERROR)
	{
		return SUCCESS;
	}
	
	if (IsSymbolIgnored(symbol) == 1)
	{
		return SUCCESS;
	}

	// PRICE DENOMINATOR INDICATOR: 1 byte
	int priceIndicator = msg->msgBody[32] - '0';
	if ((priceIndicator > 25) || (priceIndicator < 0))
	{
		TraceLog(ERROR_LEVEL, "Something wrong in parsing Long Trade message\n");
		return SUCCESS;
	}
	
	// TRADE PRICE: 12 bytes
	int numberOfByteOfNumerator = arrCTS_NumeratorByte[priceIndicator];
	int whole = Lrc_atoi(&msg->msgBody[33], 12 - numberOfByteOfNumerator);
	int numerator = Lrc_atoi(&msg->msgBody[33 + 12 - numberOfByteOfNumerator], numberOfByteOfNumerator);
	trade.price = whole + (double) numerator / arrCTS_DenominatorValue[priceIndicator];

	// TRADE VOLUME: 9 bytes
	trade.shares = Lrc_atoi(&msg->msgBody[45], 9);

	// Add trade to list
	AddTradeForTradeSymbolList(symbol, &trade);

	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessCTS_ShortTradeMsg
- Input:			
- Output:											
- Return:			
- Description:						 							  				
- Usage:			
****************************************************************************/
int ProcessCTS_ShortTradeMsg(t_CTS_Msg *msg)
{
	t_TradeInfo trade;

	msg->msgHeader[MAX_CTS_HEADER_LEN] = 0;

	// Data Feed
	trade.dataFeed = CTS_DATA;
	
	// PARTICIPANT ID
	trade.participantId = msg->msgHeader[17];

	// TIME STAMP
	trade.timestamp = (((msg->msgHeader[18] - 48) - TIMESTAMP_ADJUST_HOUR) * 3600 + (msg->msgHeader[19] - 48) * 60 + (msg->msgHeader[20] - 48)) * 1000 + Lrc_atoi(&msg->msgHeader[21], 3);

	// SECURITY SYMBOL: 3 bytes
	char cqsSymbol[4];
	
	strncpy(cqsSymbol, msg->msgBody, 3);
	cqsSymbol[3] = 0;
	
	char symbol[SYMBOL_LEN];
	if (ConvertCQSSymbolToComstock(cqsSymbol, symbol) == ERROR)
	{
		return SUCCESS;
	}
	
	if (IsSymbolIgnored(symbol) == 1)
	{
		return SUCCESS;
	}
	
	// SALE CONDITION: 1 byte

	// TRADE VOLUME: 4 bytes
	trade.shares = Lrc_atoi(&msg->msgBody[4], 4);

	// PRICE DENOMINATOR INDICATOR: 1 byte
	int priceIndicator = msg->msgBody[8] - '0';
	if ((priceIndicator > 25) || (priceIndicator < 0))
	{
		TraceLog(ERROR_LEVEL, "Something wrong in parsing Short Trade message\n");
		return SUCCESS;
	}
	
	int numberOfByteOfNumerator = arrCTS_NumeratorByte[priceIndicator];
	
	int whole = Lrc_atoi(&msg->msgBody[9], 8 - numberOfByteOfNumerator);
	int numerator = Lrc_atoi(&msg->msgBody[9 + 8 - numberOfByteOfNumerator], numberOfByteOfNumerator);
	trade.price  = whole + (double) numerator / arrCTS_DenominatorValue[priceIndicator];

	// Add trade to list
	AddTradeForTradeSymbolList(symbol, &trade);

	return SUCCESS;
}

/****************************************************************************
- Function name:	DisconnectCTS_Feed
- Input:			
- Output:											
- Return:			
- Description:						 							  				
- Usage:			
****************************************************************************/
int DisconnectCTS_Feed(void)
{
	CTS_StatusMgmt.shouldConnect = NO;
	CTS_StatusMgmt.isConnected = DISCONNECTED;

	int i;

	for (i = 0; i < MAX_MULTICAST_LINES; i++)
	{
		CTS_Feed_Conf.group[i].dataInfo.connectionFlag = 0;
		sem_post(&CTS_Feed_Conf.group[i].dataInfo.sem);
	}

	return SUCCESS;
}

/****************************************************************************
- Function name:	ReleaseCTS_FeedResource
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ReleaseCTS_FeedResource(void)
{
	int i, ret;
	int isError = 0;
	pthread_mutex_lock(&DBLMgmt.mutexLock);
	int needToRelease = (IsDBLTimedOut(CTS_Feed_Conf.dev) == 0) ? 1:0;
	
	for (i = 0; i < MAX_MULTICAST_LINES; i++)
	{
		if (CTS_Feed_Conf.group[i].dataChannel != NULL)
		{
			if (needToRelease == 1)
			{
				if (isError == 0)
				{
					ret = LeaveDBLMulticastGroup(CTS_Feed_Conf.group[i].dataChannel, CTS_Feed_Conf.group[i].dataIP);
					if (ret != EADDRNOTAVAIL)
						dbl_unbind(CTS_Feed_Conf.group[i].dataChannel);
					else
						isError = 1;
				}
			}

			CTS_Feed_Conf.group[i].dataChannel = NULL;
		}

		sem_destroy(&CTS_Feed_Conf.group[i].dataInfo.sem);
	}

	if (CTS_Feed_Conf.dev != NULL)
	{
		CTS_Feed_Conf.dev = NULL;
	}
	
	pthread_mutex_unlock(&DBLMgmt.mutexLock);
	return SUCCESS;
}
