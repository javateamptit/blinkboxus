#define _BSD_SOURCE

#include <pthread.h>
#include <time.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>

#include "utility.h"
#include "book_mgmt.h"
#include "configuration.h"
#include "socket_util.h"
#include "trading_mgmt.h"
#include "arca_book_proc.h"
#include "kernel_algorithm.h"
#include "aggregation_server_proc.h"

//****************************************************************************
//****************************************************************************

t_ARCA_Book_Conf ARCA_Book_Conf;
t_Symbol_Index_Mapping symbolIndexMappingTable[MAX_ARCA_STOCK_SYMBOL];
int StockSymbolIndex_to_ArcaSymbolIndex_Mapping[MAX_STOCK_SYMBOL];

char ArcaSymbolIndexIgnored[MAX_ARCA_STOCK_SYMBOL];

pthread_mutex_t disconnectArcaBookMutex;
int TEN_RAISE_TO[10];
int NoArcaGapRequest;

extern t_MonitorArcaBook MonitorArcaBook[MAX_ARCA_GROUPS];
extern t_DBLMgmt DBLMgmt;
//****************************************************************************
//****************************************************************************

void CloseAndResetSocket(int *sock)
{
	if (*sock > 0)
	{
		shutdown(*sock, SHUT_RDWR);
		close(*sock);
	}
	
	*sock = -1;
}

/****************************************************************************
- Function name:	InitARCA_BOOK_DataStructure
- Input:			
- Output:											
- Return:			
- Description:						 							  				
- Usage:			
****************************************************************************/
int InitARCA_BOOK_DataStructure(void)
{
	//Init lookup table
	TEN_RAISE_TO[0] = 1;
	TEN_RAISE_TO[1] = 10;
	TEN_RAISE_TO[2] = 100;
	TEN_RAISE_TO[3] = 1000;
	TEN_RAISE_TO[4] = 10000;
	TEN_RAISE_TO[5] = 100000;
	TEN_RAISE_TO[6] = 1000000;
	TEN_RAISE_TO[7] = 10000000;
	TEN_RAISE_TO[8] = 100000000;
	TEN_RAISE_TO[9] = 1000000000;
	
	
	FlushECNStockBook(BOOK_ARCA);
	
	bookStatusMgmt[BOOK_ARCA].isFlushBook = NO;
	
	// Initialize variables for disconnect ARCA book
	pthread_mutex_init(&disconnectArcaBookMutex, NULL);
	
	TraceLog(DEBUG_LEVEL,"Initialize ARCA Book\n");
	/*This structure contain is a single dimentional array, index is arca symbol index mapping
		it contains status of all current arca symbol, value = 1 means ignore (invalid symbol should be ignore
		like too long symbol, symbol with space)
	*/
	memset(ArcaSymbolIndexIgnored, 0, MAX_ARCA_STOCK_SYMBOL);	//Default enable all arca symbol index
	
	// Initialize symbol Index mapping table
	memset(symbolIndexMappingTable, 0, MAX_ARCA_STOCK_SYMBOL * sizeof(t_Symbol_Index_Mapping));
	
	int i;
	for (i = 0; i < MAX_ARCA_STOCK_SYMBOL; i++)
	{
		symbolIndexMappingTable[i].stockSymbolIndex = -1;
	}

	for (i = 0; i < MAX_STOCK_SYMBOL; i++)
	{
		StockSymbolIndex_to_ArcaSymbolIndex_Mapping[i] = -1;
	}
	
	for (i = 0; i < MAX_ARCA_GROUPS; i++)
	{
		MonitorArcaBook[i].recvCounter = 0;
		MonitorArcaBook[i].procCounter = 0;
		ARCA_Book_Conf.dataDev = NULL;
		ARCA_Book_Conf.group[i].retranDev = NULL;

		ARCA_Book_Conf.group[i].dataChannel = NULL;
		ARCA_Book_Conf.group[i].retranChannel = NULL;
		ARCA_Book_Conf.group[i].refreshChannel = NULL;

		ARCA_Book_Conf.group[i].isGotResponeFromTCP = NO;

		sem_init(&ARCA_Book_Conf.group[i].dataInfo.sem, 0, 0);
		strcpy(ARCA_Book_Conf.group[i].dataInfo.bookName, "ARCA Book");
		ARCA_Book_Conf.group[i].dataInfo.DisconnectBook = &DisconnectARCABook;
		
		memset(ARCA_Book_Conf.group[i].dataInfo.buffer, READ, MAX_BOOK_MSG_IN_BUFFER * MAX_MTU);
		
		// Initialize counter variables
		ARCA_Book_Conf.group[i].lastSequenceNumber = -1;
		ARCA_Book_Conf.group[i].dataInfo.currentReceivedIndex = 0;
		ARCA_Book_Conf.group[i].dataInfo.currentReadIndex = 0;
		ARCA_Book_Conf.group[i].dataInfo.connectionFlag = 0;
	}
	
	pthread_mutex_init(&ARCA_Book_Conf.retranMutex, NULL);
	CloseAndResetSocket(&ARCA_Book_Conf.retranRequestSocket);

	// Initialize ARCA partial hashing
	int divisorIndex;
	int remainderIndex;
	
	for (divisorIndex = 0; divisorIndex < ARCA_MAX_DIVISOR; divisorIndex++)
	{
		for (remainderIndex = 0; remainderIndex < ARCA_MAX_SAME_REMAINDER; remainderIndex++)
		{
			ARCA_PartialHashing[divisorIndex][remainderIndex].quotient = -1;
			ARCA_PartialHashing[divisorIndex][remainderIndex].side = 0;
			ARCA_PartialHashing[divisorIndex][remainderIndex].stockSymbolIndex = -1;
			ARCA_PartialHashing[divisorIndex][remainderIndex].node = NULL;
			
			ARCA_HashingBackup[divisorIndex][remainderIndex].quotient = -1;
			ARCA_HashingBackup[divisorIndex][remainderIndex].tradeSession = 0;
		}
	}
	
	if (LoadAllSymbolMappings("ARCASymbolMapping.txt") == ERROR)
	{
		return ERROR;
	}
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	ARCA_Book_Thread
- Input:			
- Output:											
- Return:			
- Description:						 							  				
- Usage:			
****************************************************************************/
void *ARCA_Book_Thread(void *agrs)
{
	SetKernelAlgorithm("[ARCA_BOOK_BASE]");
	if (InitARCA_BOOK_DataStructure() == ERROR)
	{
		bookStatusMgmt[BOOK_ARCA].isConnected = DISCONNECTED;
		TraceLog(DEBUG_LEVEL,"ARCA Book was disconnected\n");
		return NULL;
	}

	int i;
	
	while (bookStatusMgmt[BOOK_ARCA].shouldConnect == YES)
	{
		// ---------------------------------------------------------------------------
		// Connect to TCP Retrans server, ARCA XDP uses only 1 TCP retrans server
		// ---------------------------------------------------------------------------
		if (NoArcaGapRequest == 0)
		{
			TraceLog(DEBUG_LEVEL,"ARCA Book: Connecting to Retransmission Request Server...\n");
			ARCA_Book_Conf.retranRequestSocket = Connect2Server(ARCA_Book_Conf.RetransmissionRequestIP, ARCA_Book_Conf.RetransmissionRequestPort, "ARCA Retransmission Request");

			if (ARCA_Book_Conf.retranRequestSocket != -1)
			{
				// Start thread to talk to TCP server
				pthread_create(&ARCA_Book_Conf.retranRequestID, NULL, (void*)&TalkToTCPServer, NULL);
				
				usleep(500000);
			}
		}
		// ---------------------------------------------------------------------------

		int ret = CreateDBLDeviceV2(ARCA_Book_Conf.NICName, &ARCA_Book_Conf.dataDev);

		if (ret == ERROR)
		{
			TraceLog(ERROR_LEVEL, "ARCA Book: Can't create DBL device\n");

			bookStatusMgmt[BOOK_ARCA].isConnected = DISCONNECTED;
			DisconnectARCABook();
			return NULL;
		}

		for (i = 0; i < MAX_ARCA_GROUPS; i++)
		{
			ARCA_Book_Conf.group[i].groupIndex = i;
			
			/* Start threads to process data from primary data feed */
			if (pthread_create(&ARCA_Book_Conf.group[i].primaryProcessThreadID, NULL, (void*)&ThreadProcessData, (void*) &ARCA_Book_Conf.group[i]) != SUCCESS)
			{
				TraceLog(ERROR_LEVEL, "ARCA Book: Can't create thread for processing data from multicast index %d\n", i);
				DisconnectARCABook();
				return NULL;
			}
			
			/* Join to Primary multicast group */
			TraceLog(DEBUG_LEVEL, "ARCA Book: MulticastRecvPort(%s:%d) group(%d): connecting\n", ARCA_Book_Conf.group[i].dataIP, ARCA_Book_Conf.group[i].dataPort, (i+1));

			ret = JoinMulticastGroupUsingExistingDeviceV2(ARCA_Book_Conf.dataDev, ARCA_Book_Conf.group[i].dataIP, ARCA_Book_Conf.group[i].dataPort, &ARCA_Book_Conf.group[i].dataChannel, ARCA_Book_Conf.NICName, (void *) &ARCA_Book_Conf.group[i].dataInfo);
			if (ret == ERROR)
			{
				TraceLog(ERROR_LEVEL, "ARCA Book: Can't connect to Multicast group (%s:%d)\n", ARCA_Book_Conf.group[i].dataIP, ARCA_Book_Conf.group[i].dataPort);

				bookStatusMgmt[BOOK_ARCA].isConnected = DISCONNECTED;
				DisconnectARCABook();
				return NULL;
			}

			usleep(10);
		}

		// Notify that ARCA Book was connected
		bookStatusMgmt[BOOK_ARCA].isConnected = CONNECTED;
		for (i = 0; i < MAX_ARCA_GROUPS; i++)
		{
			ARCA_Book_Conf.group[i].dataInfo.connectionFlag = 1;
		}
		
		// Wait for ALL threads completed
		for (i = 0; i < MAX_ARCA_GROUPS; i++)
		{
			pthread_join(ARCA_Book_Conf.group[i].primaryProcessThreadID, NULL);
			TraceLog(DEBUG_LEVEL,"ARCA Book: Thread for processing data from group index %d returned!\n", i);
		}

		if (NoArcaGapRequest == 0)
		{
			if (ARCA_Book_Conf.retranRequestSocket != -1)
			{
				pthread_join(ARCA_Book_Conf.retranRequestID, NULL);
				TraceLog(NOTE_LEVEL, "ARCA Book: Thread for processing Retransmission TcpPort(%s:%d) returned\n", ARCA_Book_Conf.RetransmissionRequestIP, ARCA_Book_Conf.RetransmissionRequestPort);
			}
		}

		// Release ARCA Book resource
		DisconnectARCABook();
		ReleaseARCABookResouce();

		int isContinue = bookStatusMgmt[BOOK_ARCA].isFlushBook;
		
		if (isContinue == YES)
		{
			bookStatusMgmt[BOOK_ARCA].shouldConnect = YES;
		}
		
		// Initialize ARCA Book data structure
		TraceLog(DEBUG_LEVEL,"ARCA Book: Begin to flush book\n");

		InitARCA_BOOK_DataStructure();
		
		FlushECNStockBook(BOOK_ARCA);
		
		TraceLog(DEBUG_LEVEL,"ARCA Book: Initialize ARCA Book completed\n");
		
		flushBookRequestCollection[BOOK_ARCA].isFlushBook = NO;
		
		// If we received flush book request, try to join to restart all threads again
		if (isContinue == YES)
		{
			TraceLog(DEBUG_LEVEL,"ARCA Book: Trying to restart ARCA Book...\n");
		}
	}
	
	// Notify ARCA Book was disconnected
	DisconnectARCABook();
	ReleaseARCABookResouce();
	bookStatusMgmt[BOOK_ARCA].isConnected = DISCONNECTED;
	
	SendPMDisconnectedBookOrderAlertToAS(SERVICE_ARCA_BOOK);
	CheckAndSendAlertDisableTrading(1);
	
	TraceLog(DEBUG_LEVEL,"ARCA Book was disconnected\n");
	
	return NULL;
}

/****************************************************************************
- Function name:	ThreadProcessData
- Input:			
- Output:											
- Return:			
- Description:						 							  				
- Usage:			
****************************************************************************/
void *ThreadProcessData(void *args)
{
	SetKernelAlgorithm("[ARCA_BOOK_PROCESS_MSGS]");
	
	t_ARCA_Book_GroupInfo *workingGroup = (t_ARCA_Book_GroupInfo *)args;

	// Temp buffer need to store data from receive buffer
	unsigned char *tempBuffer;
	
	// Message type
	unsigned char packetType;	

	int index;
	
	while (bookStatusMgmt[BOOK_ARCA].shouldConnect == YES)
	{
		// Wait for new message
		if (sem_wait(&workingGroup->dataInfo.sem) != SUCCESS)
		{
			TraceLog(ERROR_LEVEL, "ARCA Book: Call to sem_wait() system call failed.\n");
			
			DisconnectARCABook();
			
			return NULL;
		}
		
		if (bookStatusMgmt[BOOK_ARCA].shouldConnect == NO)
		{
			return NULL;
		}
		
		index = workingGroup->dataInfo.currentReadIndex;
		tempBuffer = (unsigned char*)workingGroup->dataInfo.buffer[index];
		
		if (tempBuffer[MAX_MTU - 1] != STORED)
		{
			TraceLog(ERROR_LEVEL, "ARCA Book: Buffer is full!\n");
			DisconnectARCABook();
			return NULL;
		}
		
		FlushECNSymbolStockBook(BOOK_ARCA);
		
		// Get message type
		packetType = tempBuffer[2];
		
		// Get message Sequence number
		int seqNum = GetIntNumber(&tempBuffer[4]);
		int msgCount = tempBuffer[3];
		
		if (seqNum == workingGroup->lastSequenceNumber)
		{
			//Sequence number is OK
			workingGroup->lastSequenceNumber = seqNum + msgCount;
		}
		else if (workingGroup->lastSequenceNumber == -1)
		{
			//Has just received first packet, so start counting Sequence Number
			workingGroup->lastSequenceNumber = seqNum + msgCount;
		}
		else if (seqNum > workingGroup->lastSequenceNumber)
		{
			TraceLog(WARN_LEVEL, "ARCA Book: Gap detected, Last received sequence(%d), current received sequence(%d)\n", workingGroup->lastSequenceNumber, seqNum);
			if (NoArcaGapRequest == 1)
			{
				TraceLog(WARN_LEVEL, "ARCA Book: PM will flush book, since it is running with no recovery mode\n");
				bookStatusMgmt[BOOK_ARCA].isFlushBook = YES;
				DisconnectARCABook();
				return NULL;
			}
			
			TraceLog(DEBUG_LEVEL, "ARCA Book: Start recovery...\n");
			if (RecoveryProcessing(workingGroup->lastSequenceNumber, seqNum -1, workingGroup) == ERROR)
			{
				bookStatusMgmt[BOOK_ARCA].isFlushBook = YES;
				TraceLog(ERROR_LEVEL, "ARCA Book: Recovery failed\n");
				DisconnectARCABook();
				return NULL;
			}
			
			workingGroup->lastSequenceNumber = seqNum + msgCount;
			TraceLog(DEBUG_LEVEL, "ARCA Book: Recovery successfully\n");
		}
		else if (seqNum < workingGroup->lastSequenceNumber)
		{
			//received sequence number less than current processed sequence number
			//we will ignore the packet
			if (tempBuffer[2] == ARCA_SEQ_RESET_PACKET)
			{
				if (ProcessResetSeqNumMessage(tempBuffer, workingGroup) == ERROR)
				{
					DisconnectARCABook();
				
					return NULL;
				}
			}
		}

		if (packetType == ARCA_ORIGINAL_PACKET)
		{				
			if (ProcessBookMessage(tempBuffer, workingGroup) == ERROR)
			{
				TraceLog(ERROR_LEVEL, "ARCA: Have problem in process book message (%s:%d)\n", workingGroup->dataIP, workingGroup->dataPort);

				DisconnectARCABook();
				
				return NULL;
			}	
		}
		else if (packetType == ARCA_SEQ_RESET_PACKET)
		{
			if(ProcessResetSeqNumMessage(tempBuffer, workingGroup) == ERROR)
			{
				DisconnectARCABook();
				
				return NULL;
			}
		}
		
		// Update status of buffer
		tempBuffer[MAX_MTU - 1] = READ;
		
		/*
		If we have Current_Read_Index greater than LIMITATION of INTEGER data type
		that is very dangerous
		*/
		if(workingGroup->dataInfo.currentReadIndex == MAX_BOOK_MSG_IN_BUFFER - 1)
		{
			workingGroup->dataInfo.currentReadIndex = 0;
		}
		else
		{
			workingGroup->dataInfo.currentReadIndex++;
		}
	}
	
	return NULL;	
}

/****************************************************************************
- Function name:	ProcessResetSeqNumMessage
- Input:			
- Output:											
- Return:			
- Description:			 				  				
- Usage:			
****************************************************************************/
int ProcessResetSeqNumMessage(unsigned char *buffer, t_ARCA_Book_GroupInfo *workingGroup)
{
	// Get Sequence Number
	int seqNum = GetIntNumber(&buffer[4]) + buffer[3];
	
	workingGroup->lastSequenceNumber = seqNum;
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessBookMessage
- Input:			
- Output:											
- Return:			
- Description:			 				  				
- Usage:			
****************************************************************************/
int ProcessBookMessage(unsigned char *inputBuffer, t_ARCA_Book_GroupInfo *workingGroup)
{
	int numOfBodyEntries = (unsigned char)inputBuffer[3];
	
	inputBuffer += 16;	// Ignore message header
	
	union book_message_union_binary dstMsg;
	while (numOfBodyEntries > 0)
	{
		ParseBookMsg(&dstMsg, (unsigned char*)inputBuffer);
		
		switch (dstMsg.mHeader.msgType)
		{
			case ARCA_ADD_MSG_BODY:
				if (ProcessDecodedAddMessage(workingGroup, &(dstMsg.mAdd), YES) == ERROR)
				{
					TraceLog(ERROR_LEVEL, "ARCA Book: Problem in processing 'Add' msg\n");
					
					return ERROR;
				}
				break;
			case ARCA_MOD_MSG_BODY:
				if (ProcessDecodedModifyMessage(workingGroup, &(dstMsg.mModify)) == ERROR)
				{
					TraceLog(ERROR_LEVEL, "ARCA Book: Problem in processing 'Modify' msg\n");
					
					return ERROR;
				}
				break;
			case ARCA_DEL_MSG_BODY:
				if (ProcessDecodedDeleteMessage(workingGroup, &(dstMsg.mDelete)) == ERROR)
				{
					TraceLog(ERROR_LEVEL, "ARCA Book: Problem in processing 'Delete' msg\n");
					
					return ERROR;
				}
				break;
			case ARCA_EXEC_MSG_BODY:
				if (ProcessDecodedExecuteMessage(workingGroup, &(dstMsg.mExecute)) == ERROR)
				{
					TraceLog(ERROR_LEVEL, "ARCA Book: Problem in processing 'Execute' msg\n");
					
					return ERROR;
				}
				break;
			case ARCA_SYMBOL_CLEAR_MSG:
				if (ProcessDecodedSymbolClearMessage(workingGroup, &(dstMsg.mSymbolClear)) == ERROR)
				{
					TraceLog(ERROR_LEVEL, "ARCA Book: Problem in processing 'Symbol Clear' msg\n");
					return ERROR;
				}
				break;
			case ARCA_IMB_MSG_BODY:
				break;
			case ARCA_VENDOR_MAPPING_MSG:
				break;
			case ARCA_TRADING_SESSION_CHANGE_MSG:
				if (ProcessTradingSessionChangeMessage(workingGroup, &(dstMsg.mTradingSessionChange)) == ERROR)
				{
					TraceLog(ERROR_LEVEL, "ARCA Book: Problem in processing 'Trading Session Change' msg\n");
					return ERROR;
				}
				break;
			case ARCA_SECURITY_STATUS_MSG:
				break;
			case ARCA_SOURCE_TIME_REF_MSG:
				break;
			case ARCA_SYMBOL_INDEX_MAPPING_MSG:
				if (ProcessSymbolIndexMappingMessage(workingGroup, &(dstMsg.mSymbolMapping)) == ERROR)
				{
					TraceLog(ERROR_LEVEL, "ARCA Book: Problem in processing 'Symbol Index Mapping' msg\n");
					return ERROR;
				}
				break;
			case ARCA_TRADE_MSG: 
				break;
			case ARCA_TRADE_CANCELED_MSG: 
				break;
			case ARCA_TRADE_CORRECTION_MSG: 
				break;
			case ARCA_STOCK_SUMMARY_MSG: 
				break;
			case ARCA_PBBO_MSG: 
				break;
			case ARCA_ADD_REFRESH_MSG: 
				break;
			default:
				TraceLog(ERROR_LEVEL, "ARCA Book: Unknown message type(%d)\n", dstMsg.mHeader.msgType);
				break;
		}
			
		// While numOfBodyEntries > 1 then try to decode next Entry Body			
		inputBuffer += dstMsg.mHeader.msgSize;
			
		numOfBodyEntries--;
	}
			
	return SUCCESS;
}

/****************************************************************************
- Function name:	TalkToTCPServer
- Input:			
- Output:											
- Return:			
- Description:						 							  				
- Usage:			
****************************************************************************/
void *TalkToTCPServer(void *args)
{
	SetKernelAlgorithm("[ARCA_BOOK_RETRANS_TCP]");
	
	unsigned char buffer[100000];
	int headerLen = 16;
	int currentIndex = 0;
	
	int msgSize;
	
	// Set receive time out for socket
	if (SetSocketRecvTimeout(ARCA_Book_Conf.retranRequestSocket, 61) != SUCCESS)
	{
		TraceLog(ERROR_LEVEL, "ARCA Book: Can't set socket receive timeout for Retransmission Request socket\n");
		close(ARCA_Book_Conf.retranRequestSocket);
		ARCA_Book_Conf.retranRequestSocket = -1;
		return NULL;
	}
	
	while (bookStatusMgmt[BOOK_ARCA].shouldConnect == YES)
	{
		// Get message header
		while (currentIndex < headerLen)
		{
			int retval = recv(ARCA_Book_Conf.retranRequestSocket, &buffer[currentIndex], headerLen - currentIndex, 0);
			
			if(retval > 0)
			{
				currentIndex += retval;
			}
			else if(errno == EAGAIN)
			{;}
			else
			{
				TraceLog(ERROR_LEVEL, "ARCA Book : Cannot receive anything from TCP %s:%d, res(%d) errno(%d)\n", ARCA_Book_Conf.RetransmissionRequestIP, ARCA_Book_Conf.RetransmissionRequestPort, retval, errno);
				close(ARCA_Book_Conf.retranRequestSocket);
				ARCA_Book_Conf.retranRequestSocket = -1;
				return NULL;
			}
		}
		
		// Get message size
		msgSize = GetShortNumber(&buffer[0]);
		
		if (msgSize > 0)
		{

			while (currentIndex < msgSize)
			{
								
				int retval = recv(ARCA_Book_Conf.retranRequestSocket, &buffer[currentIndex], msgSize - currentIndex, 0);
				
				if(retval > 0)
				{
					currentIndex += retval;
				}
				else if(errno == EAGAIN)
				{;}
				else
				{
					TraceLog(ERROR_LEVEL, "ARCA Book : Cannot receive anything from TCP %s:%d, res(%d) errno(%d)\n", ARCA_Book_Conf.RetransmissionRequestIP, ARCA_Book_Conf.RetransmissionRequestPort, retval, errno);
					close(ARCA_Book_Conf.retranRequestSocket);
					ARCA_Book_Conf.retranRequestSocket = -1;
					return NULL;
				}
			}
		}
		
		// Get message body
		int packetType = buffer[2];
		if (packetType == ARCA_HEARTBEAT_PACKET)
		{
			if (BuildAndSendHeartbeatResponse() == ERROR)
			{
				close(ARCA_Book_Conf.retranRequestSocket);
				ARCA_Book_Conf.retranRequestSocket = -1;
				return NULL;
			}
			else
			{
				currentIndex = 0;
			}
		}
		else if (packetType == ARCA_ORIGINAL_PACKET)
		{	
			int msgType = GetShortNumber(&buffer[18]);
			
			// RESTRAN_RESPONSE_MSG
			if (msgType == ARCA_REQUEST_RESPONSE_MSG)
			{
				int channelID = buffer[43];
				if ((channelID < 1) || (channelID > MAX_ARCA_GROUPS))
				{
					TraceLog(ERROR_LEVEL, "ARCA Book: Received invalid channel ID (%d) from TCP Request Server\n", channelID);
					DisconnectARCABook();
					return NULL;
				}
				
				ARCA_Book_Conf.group[channelID - 1].isGotResponeFromTCP = 1;	//Telling the UDP thread that we got the response, so it will start counting for garbage msg
				TraceLog(DEBUG_LEVEL,"ARCA Book: Received Retransmission Response message:\n");
				TraceLog(DEBUG_LEVEL,"BeginSeqNum %d\n", GetIntNumber(&buffer[24]));
				TraceLog(DEBUG_LEVEL,"EndSeqNum %d\n", GetIntNumber(&buffer[28]));
				TraceLog(DEBUG_LEVEL,"SourceID %s\n", &buffer[32]);
				TraceLog(DEBUG_LEVEL,"Status = %c\n", buffer[44]);
				TraceLog(DEBUG_LEVEL,"Product ID: %d\n", buffer[42]);
				TraceLog(DEBUG_LEVEL,"Channel ID: %d\n", channelID);
			
			
				if (buffer[44] != '0')
				{
					TraceLog(ERROR_LEVEL, "ARCA XDP TCP: Request rejected by server, status code: %c\n", buffer[44]);
					DisconnectARCABook();
					return NULL;
				}
			
				currentIndex = 0;
			}		
			else if (msgType == ARCA_SYMBOL_INDEX_MAPPING_MSG)	//Currently not available on TCP/IP
			{			
				TraceLog(DEBUG_LEVEL,"ARCA Book: Receive Symbol Index Mapping message\n");
				currentIndex = 0;
				
				//Currently we use symbol index mapping from downloaded file, so we ignore all symbol index mapping msgs
				/*
				if (DecodeAndUpdateSymbolDataStructure((unsigned char *)buffer) == ERROR)
				{
					TraceStdError("ARCA Book: Cannot decode Symbol Index Mapping Message from Server\n");
				}*/
			}
		}
		else
		{
			TraceLog(ERROR_LEVEL, "ARCA Book: Unknown message type %d\n", packetType);
			close(ARCA_Book_Conf.retranRequestSocket);
			ARCA_Book_Conf.retranRequestSocket = -1;
			return NULL;
		}
	}
	
	return NULL;
}

/****************************************************************************
- Function name:	CheckSymbolIndex
- Input:			
- Output:											
- Return:			
- Description:			 				  				
- Usage:			
****************************************************************************/
int CheckSymbolIndex(int symbolIndex, int *stockSymbolIndex)
{
	if (ArcaSymbolIndexIgnored[symbolIndex] == 1) return AB_IGNORE;	//Ignore invalid/disabled symbol
	
	*stockSymbolIndex = symbolIndexMappingTable[symbolIndex].stockSymbolIndex;
	if ( *stockSymbolIndex >= 0)
	{
		return SUCCESS;
	}
	else //We have not received any symbol for this symbol index
	{
		ArcaSymbolIndexIgnored[symbolIndex] = 1;
		TraceLog(WARN_LEVEL, "New ARCA symbol detected, ARCA symbol index mapping: %d, this symbol will be ignored.\n", symbolIndex);
		return AB_IGNORE;
	}
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	RecoveryProcessing
- Input:			
- Output:											
- Return:			
- Description:			 				  				
- Usage:			
****************************************************************************/
int RecoveryProcessing(int beginSeqNum, int endSeqNum, t_ARCA_Book_GroupInfo *workingGroup)
{
	if (ARCA_Book_Conf.retranRequestSocket == -1)
	{
		TraceLog(WARN_LEVEL, "ARCA Recovery: Retransmission Request server is currently not available\n");
		
		return ERROR;
	}
	
	int countMsgToRecover = endSeqNum - beginSeqNum + 1;
	
	if (countMsgToRecover > MAX_ARCA_RETRAN_MSG_IN_BUFFER)
	{
		TraceLog(ERROR_LEVEL, "ARCA Recovery: Too many lost packets %d (max = %d). We shouldn't recover\n", countMsgToRecover, MAX_ARCA_RETRAN_MSG_IN_BUFFER);
		return ERROR;
	}
	
	//Connect to Multicast Retran Feed
	int ret = CreateDBLDevice(ARCA_Book_Conf.NICName, &workingGroup->retranDev);

	if (ret == ERROR)
	{
		TraceLog(ERROR_LEVEL, "ARCA Recovery: Can't connect to Retransmission Multicast group: (%s:%d)\n", workingGroup->retranIP, workingGroup->retranPort);
		return ERROR;
	}
	
	ret = JoinMulticastGroupUsingExistingDevice(workingGroup->retranDev, workingGroup->retranIP, workingGroup->retranPort, &workingGroup->retranChannel);
	if (ret == ERROR)
	{
		TraceLog(ERROR_LEVEL, "ARCA Recovery: Can't connect to Retransmission Multicast group: (%s:%d)\n", workingGroup->retranIP, workingGroup->retranPort);
		return ERROR;
	}
	
	// Try to send retransmission Request
	workingGroup->isGotResponeFromTCP = 0;
	if (SendRetransmissionRequest(beginSeqNum, endSeqNum, workingGroup) == ERROR)
	{
		return ERROR;
	}
	
	/*
	Receive response from retransmission multicast group
	*/

	int numOfReceivedByte;
	char status[MAX_ARCA_RETRAN_MSG_IN_BUFFER];
	unsigned char buff[MAX_MTU];
	char retranMsgCollection[MAX_ARCA_RETRAN_MSG_IN_BUFFER][MAX_MTU];
	
	memset(status, NO, MAX_ARCA_RETRAN_MSG_IN_BUFFER);
	
	int numberOfPacketToRecover = endSeqNum - beginSeqNum + 1;
	int numOfReceivedMessage = 0;
	int countTryTime = 0;
	int MAX_RECOVERY_PACKETS_RECEIVED = 400;

	struct pollfd pfd;
	pfd.events = POLLIN;

	pfd.fd = dbl_device_handle(workingGroup->retranDev);

	struct dbl_recv_info rInfo;

	while (bookStatusMgmt[BOOK_ARCA].shouldConnect == YES && numberOfPacketToRecover > 0)
	{
		ret = poll(&pfd, 1, 1500);	//timeout = 1.5 second

		if (ret < 0)
		{
			PrintErrStr(ERROR_LEVEL, "ARCA Recovery poll():", errno);
			return ERROR;
		}
		else if (ret == 0)	//Timed out
		{
			TraceLog(ERROR_LEVEL, "ARCA Recovery : Cannot receive anything from Retransmission Multicast (%s:%d)\n", workingGroup->retranIP, workingGroup->retranPort);
			return ERROR;
		}
		
		// Receive some thing from refresh multicast
		ret = dbl_recvfrom(workingGroup->retranDev, DBL_RECV_NONBLOCK,
								buff, MAX_MTU, &rInfo);

		if (ret != 0)
		{
			TraceLog(ERROR_LEVEL, "ARCA Recovery: Cannot receive anything from retransmission MC\n");
			return ERROR;
		}

		numOfReceivedByte = rInfo.msg_len;
	
		if (numOfReceivedByte <= 0)
		{
			TraceLog(ERROR_LEVEL, "ARCA Recovery: Cannot receive anything from Retransmission MC\n");
			return ERROR;
		}
		
		// Get Packet Type
		int packetType = buff[2];

		// If an Message Unavailable is received, we should check 
		if (packetType == ARCA_MESSAGE_UNAVAILABLE_PACKET)
		{
			TraceLog(ERROR_LEVEL, "ARCA Recovery: Message unavailable\n");
			
			// Get Begin Sequence Number
			int beginSeqNumRetVal = GetIntNumber(&buff[20]);
			
			// Get End Sequence Number
			int endSeqNumRetVal = GetIntNumber(&buff[24]);
			
			if (beginSeqNum == beginSeqNumRetVal && endSeqNum == endSeqNumRetVal)
			{
				TraceLog(ERROR_LEVEL, "ARCA Recovery: We sent recovery message with invalid range\n");
				return ERROR;
			}
			
			continue;
		}
		
		// Get Message Sequence Number
		int msgSeqNum = GetIntNumber(&buff[4]);
		

		if (msgSeqNum >= beginSeqNum && msgSeqNum <= endSeqNum)
		{
			int countMsgs = buff[3];
			int offset = 16;
			while ( countMsgs > 0 )
			{
				countMsgs--;
				if (status[msgSeqNum % MAX_ARCA_RETRAN_MSG_IN_BUFFER] == NO)
				{
					int msgSize = GetUnsignedShortNumber((unsigned char*) &buff[offset]);
					memcpy(retranMsgCollection[numOfReceivedMessage++], &buff[offset], msgSize);
					offset += msgSize; 
					numberOfPacketToRecover--;
					status[msgSeqNum % MAX_ARCA_RETRAN_MSG_IN_BUFFER] = YES;
					msgSeqNum++;
					continue;
				}
			}
			
			// Check receive timeout for current recovery time
			if (workingGroup->isGotResponeFromTCP == 1)
			{
				countTryTime++;
			}
		
			if (countTryTime >= MAX_RECOVERY_PACKETS_RECEIVED)
			{
				TraceLog(ERROR_LEVEL, "ARCA Recovery: We recovered %d (in %d) packet(s) in the first %d packets received from the m.c retransmission!\n", numOfReceivedMessage, endSeqNum - beginSeqNum + 1, MAX_RECOVERY_PACKETS_RECEIVED);
				return ERROR;
			}
		}
		else
		{
			// Check receive timeout for current recovery time
			if (workingGroup->isGotResponeFromTCP == 1)
			{
				countTryTime++;
			}
			
			if (countTryTime >= MAX_RECOVERY_PACKETS_RECEIVED)
			{
				TraceLog(ERROR_LEVEL, "ARCA Recovery: We recovered %d (in %d) packet(s) in the first %d packets received from the m.c retransmission!\n", numOfReceivedMessage, endSeqNum - beginSeqNum + 1, MAX_RECOVERY_PACKETS_RECEIVED);
				return ERROR;
			}
		}
	}
	
	//********************************************************************************
	// If we reach here, it means all recovery messages were received successfully
	//********************************************************************************
	//Close Gap Group connection

	if (workingGroup->retranChannel != NULL)
	{
		ret = LeaveDBLMulticastGroup(workingGroup->retranChannel, workingGroup->retranIP);
		if (ret == SUCCESS)
		{
			TraceLog(DEBUG_LEVEL, "ARCA Book: Left retransmission multicast group index (%d)\n", workingGroup->groupIndex);
		}

		if (ret != EADDRNOTAVAIL)
			dbl_unbind(workingGroup->retranChannel);
		workingGroup->retranChannel = NULL;
	}

	if (workingGroup->retranDev != NULL)
	{
		dbl_close(workingGroup->retranDev);
		workingGroup->retranDev = NULL;
	}
	
	if (numOfReceivedMessage > 0)
	{
		TraceLog(DEBUG_LEVEL, "ARCA Recovery: Recovery completed. Processing received retransmission message...\n");
		int i;
		for (i = 0; i < numOfReceivedMessage; i++)
		{
			if (ProcessRetransmissionMessage(retranMsgCollection[i], workingGroup) == ERROR)
			{
				return ERROR;
			}
		}

		return SUCCESS;
	}
	
	return ERROR;
}

/****************************************************************************
- Function name:	ProcessRetransmissionMessage
- Input:			
- Output:											
- Return:			
- Description:			 				  				
- Usage:			
****************************************************************************/
int ProcessRetransmissionMessage(char *retransmissionMsg, t_ARCA_Book_GroupInfo *workingGroup)
{
	union book_message_union_binary dstMsg;		

	char *inputBuffer = retransmissionMsg;

	ParseBookMsg(&dstMsg, (unsigned char*)inputBuffer);

	switch (dstMsg.mHeader.msgType)
	{
		case ARCA_ADD_MSG_BODY:
			if (ProcessDecodedAddMessage(workingGroup, &(dstMsg.mAdd), YES) == ERROR)
			{
				TraceLog(ERROR_LEVEL, "ARCA Book: Problem in processing 'Add' msg\n");
				
				return ERROR;
			}
			
			break;
		case ARCA_MOD_MSG_BODY:
			if (ProcessDecodedModifyMessage(workingGroup, &(dstMsg.mModify)) == ERROR)
			{
				TraceLog(ERROR_LEVEL, "ARCA Book: Problem in processing 'Modify' msg\n");
				
				return ERROR;
			}
					
			break;
		case ARCA_DEL_MSG_BODY:
			if (ProcessDecodedDeleteMessage(workingGroup, &(dstMsg.mDelete)) == ERROR)
			{
				TraceLog(ERROR_LEVEL, "ARCA Book: Problem in processing 'Delete' msg\n");
				
				return ERROR;
			}
	
			break;
		case ARCA_EXEC_MSG_BODY:
			if (ProcessDecodedExecuteMessage(workingGroup, &(dstMsg.mExecute)) == ERROR)
			{
				TraceLog(ERROR_LEVEL, "ARCA Book: Problem in processing 'Execute' msg\n");
				
				return ERROR;
			}
	
			break;
		case ARCA_SYMBOL_CLEAR_MSG:
			if (ProcessDecodedSymbolClearMessage(workingGroup, &(dstMsg.mSymbolClear)) == ERROR)
			{
				TraceLog(ERROR_LEVEL, "ARCA Book: Problem in processing 'Symbol Clear' msg\n");
				return ERROR;
			}
			break;
		case ARCA_IMB_MSG_BODY:
			break;
		case ARCA_VENDOR_MAPPING_MSG:
			break;
		case ARCA_TRADING_SESSION_CHANGE_MSG:
			if (ProcessTradingSessionChangeMessage(workingGroup, &(dstMsg.mTradingSessionChange)) == ERROR)
			{
				TraceLog(ERROR_LEVEL, "ARCA Book: Problem in processing 'Trading Session Change' msg\n");
				return ERROR;
			}
			break;
		case ARCA_SECURITY_STATUS_MSG:
			break;
		case ARCA_SOURCE_TIME_REF_MSG:
			break;
		case ARCA_SYMBOL_INDEX_MAPPING_MSG:
			if (ProcessSymbolIndexMappingMessage(workingGroup, &(dstMsg.mSymbolMapping)) == ERROR)
			{
				TraceLog(ERROR_LEVEL, "ARCA Book: Problem in processing 'Symbol Index Mapping' msg\n");
				return ERROR;
			}
			break;
		case ARCA_TRADE_MSG: 
			break;
		case ARCA_TRADE_CANCELED_MSG: 
			break;
		case ARCA_TRADE_CORRECTION_MSG: 
			break;
		case ARCA_STOCK_SUMMARY_MSG: 
			break;
		case ARCA_PBBO_MSG: 
			break;
		case ARCA_ADD_REFRESH_MSG: 
			break;
		default:
			TraceLog(ERROR_LEVEL, "ARCA Book: Unknown message type(%d)\n", dstMsg.mHeader.msgType);
			break;
	}

	return SUCCESS;
}

/****************************************************************************
- Function name:	SendRetransmissionRequest
- Input:			
- Output:											
- Return:			
- Description:			 				  				
- Usage:			
****************************************************************************/
int SendRetransmissionRequest(int beginSeqNum, int endSeqNum, t_ARCA_Book_GroupInfo *workingGroup)
{
	// Build message and send Retransmission Request message to TCP server
	t_ShortConverter shortValue;
	t_IntConverter intValue;
	
	char msgToSend[80];
	memset(msgToSend, 0, 80);
	
	/* 
	Packet Header
	*/
	// Packet Size
	shortValue.value = 40;
	msgToSend[0] = shortValue.c[0];
	msgToSend[1] = shortValue.c[1];
	
	// Packet Type
	msgToSend[2] = ARCA_ORIGINAL_PACKET;
	
	// Number Msgs
	msgToSend[3] = 1;
	
	// Msg Seq Num
	intValue.value = 2;
	msgToSend[4] = intValue.c[0];
	msgToSend[5] = intValue.c[1];
	msgToSend[6] = intValue.c[2];
	msgToSend[7] = intValue.c[3];
	
	// Send Time
	intValue.value = 0;
	msgToSend[8] = intValue.c[0];
	msgToSend[9] = intValue.c[1];
	msgToSend[10] = intValue.c[2];
	msgToSend[11] = intValue.c[3];
	
	// Send Time NS
	intValue.value = 0;
	msgToSend[12] = intValue.c[0];
	msgToSend[13] = intValue.c[1];
	msgToSend[14] = intValue.c[2];
	msgToSend[15] = intValue.c[3];
	
	/*
	Message body
	*/
	// Msg size
	shortValue.value = 24;
	msgToSend[16] = shortValue.c[0];
	msgToSend[17] = shortValue.c[1];
	
	// Msg Type
	shortValue.value = ARCA_RETRANS_REQUEST_MSG;
	msgToSend[18] = shortValue.c[0];
	msgToSend[19] = shortValue.c[1];
	
	// Begin Sequence Number
	intValue.value = beginSeqNum;
	msgToSend[20] = intValue.c[0];
	msgToSend[21] = intValue.c[1];
	msgToSend[22] = intValue.c[2];
	msgToSend[23] = intValue.c[3];
	
	// End Sequence Number
	intValue.value = endSeqNum;
	msgToSend[24] = intValue.c[0];
	msgToSend[25] = intValue.c[1];
	msgToSend[26] = intValue.c[2];
	msgToSend[27] = intValue.c[3];

	// SourceID
	strncpy(&msgToSend[28], ARCA_Book_Conf.RetransmissionRequestSource, 10);
	
	// ProductID
	msgToSend[38] = ARCA_PRODUCT_ID;
	
	// ChannelID
	msgToSend[39] = (workingGroup->groupIndex + 1);	//groupIndex starts from 0, but Channel ID starts from 1
	
	// Send message to server
	// Try to get lock before send message to TCP server
	pthread_mutex_lock(&ARCA_Book_Conf.retranMutex);

	int retval = send(ARCA_Book_Conf.retranRequestSocket, msgToSend, 40, 0);
	
	pthread_mutex_unlock(&ARCA_Book_Conf.retranMutex);
	
	if (retval != 40)
	{
		TraceLog(ERROR_LEVEL, "ARCA Recovery: Cannot send Retransmission Request message to server\n");
		DisconnectARCABook();
		return ERROR;
	}

	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessDecodedAddMessage
- Input:			
- Output:											
- Return:			
- Description:			 				  				
- Usage:			
****************************************************************************/
int ProcessDecodedAddMessage(t_ARCA_Book_GroupInfo *workingGroup, struct gsxBook_Add_Message_Body *msg, char isCheckCross)
{
	/*
	Check current stock symbol index in current session
	*/	

	int stockSymbolIndex;
	if (CheckSymbolIndex(msg->symbolIndex, &stockSymbolIndex) == AB_IGNORE)
	{
		return SUCCESS;
	}

	/* ARCA XDP now send orderID in 4 bytes, and also result in duplicate order ID
		So we need to find a way to avoid this duplication
		--> construct a new reference number base on the orderID + ARCA symbol index
		refNum = <4Bytes symbolIndex><4Bytes OrderID>
	*/
	long _refNum = msg->symbolIndex; 
	_refNum = ((_refNum << 32) | msg->orderID);
	
	t_OrderDetail order;
	order.shareVolume = msg->volume;
	order.sharePrice = (double)msg->priceNumerator/TEN_RAISE_TO[msg->priceScaleCode];
	order.refNum = _refNum;
	order.tradeSession = msg->tradeSession;
	
	// Ask order
	int side = (msg->side == 'S')?ASK_SIDE:BID_SIDE;
	
	t_TopOrder _bestQuote;
	long refNum = CheckSelfCrossTrade(&order, side, stockSymbolIndex, BOOK_ARCA, &_bestQuote);
	int isCanceled = ERROR;
	
	if (refNum > 0)
	{
		t_PartialHashingItem *ARCA_Hashing;
		ARCA_Hashing = FindPartialHashingItem(BOOK_ARCA, refNum);
	
		if (ARCA_Hashing != NULL)
		{
			int tempSide = !side;
			t_OrderDetailNode *oldNode = ARCA_Hashing->node;
		
			pthread_mutex_lock(&deleteQuotesMgmtMutex[BOOK_ARCA][stockSymbolIndex][tempSide]);
			DeletePartialHashingItem(BOOK_ARCA, refNum);
			if (oldNode != NULL)
			{
				if (GeneralDelete(stockBook[BOOK_ARCA][tempSide][stockSymbolIndex], oldNode, tempSide, stockSymbolIndex) == ERROR)
				{
					TraceLog(ERROR_LEVEL, "ARCA Book processing: Cannot delete node\n");
				}
			}
		
			pthread_mutex_unlock(&deleteQuotesMgmtMutex[BOOK_ARCA][stockSymbolIndex][tempSide]);
		}
		else
		{
			if (side == ASK_SIDE)
			{
				// New Order Side is ASK -> Other side is BID
				if (order.shareVolume > MAX_ODD_LOT_SHARE)
				{
					isCanceled = UpdateMaxBidRoundLot(stockSymbolIndex, 1, YES, -1);
				}
			
				if (isCanceled == SUCCESS)
					UpdateMaxBid(stockSymbolIndex, 0, -1);
				else
					UpdateMaxBid(stockSymbolIndex, 1, -1);
			}
			else
			{
				// Otherwise
				if (order.shareVolume > MAX_ODD_LOT_SHARE)
				{
					isCanceled = UpdateMinAskRoundLot(stockSymbolIndex, 1, YES, -1);
				}
			
				if (isCanceled == SUCCESS)
					UpdateMinAsk(stockSymbolIndex, 0, -1);
				else
					UpdateMinAsk(stockSymbolIndex, 1, -1);
			}
		
			TraceLog(DEBUG_LEVEL, "ARCA Book processing: Cannot find node (refNum %ld). Order(%d, %lf, %ld). BestQuote(%d: %d, %lf)\n",
					refNum, order.shareVolume, order.sharePrice, order.refNum,
					_bestQuote.ecnIndicator, _bestQuote.info.shareVolume, _bestQuote.info.sharePrice);
		}
		
		DeleteBackupPartialHashingItem(BOOK_ARCA, refNum);
	}

	// Ignore own quote
	int quoteIndex = FindQuoteIgnore(stockSymbolIndex, msg->orderID);	

	if (quoteIndex != -1)
	{
		TraceLog(DEBUG_LEVEL, "ARCA Book processing: Ignore own quote, refNum = %ld (orderID = %d)\n", order.refNum, msg->orderID);

		return SUCCESS;
	}

	pthread_mutex_lock(&deleteQuotesMgmtMutex[BOOK_ARCA][stockSymbolIndex][side]);
	
	// Find position of partial hashing item to insert
	int rowIndex, columnIndex;
	int retval = SUCCESS;
	if (RegisterPartialHashingItem_ARCA(order.refNum, &rowIndex, &columnIndex) == ERROR)
	{
		TraceLog(ERROR_LEVEL, "ARCA Book processing: We can't find free slot in PartialHash to insert\n");
		retval = ERROR;
	}
	
	t_OrderDetailNode *retNode = GeneralInsert(stockBook[BOOK_ARCA][side][stockSymbolIndex], &order, side, stockSymbolIndex);
	
	if (retNode == NULL)
	{
		TraceLog(ERROR_LEVEL, "ARCA Book processing: We can't insert this order into Stock Book\n");
		retval = ERROR;
	}

	if (InsertPartialHashingItem_ARCA(retNode, order.refNum, stockSymbolIndex, msg->side, rowIndex, columnIndex) == ERROR)
	{
		TraceLog(ERROR_LEVEL, "ARCA Book processing: We can't insert order into partial hashing list\n");
		retval = ERROR;
	}

	pthread_mutex_unlock(&deleteQuotesMgmtMutex[BOOK_ARCA][stockSymbolIndex][side]);

	return retval;
}

/****************************************************************************
- Function name:	ProcessDecodedModifyMessage
- Input:			
- Output:											
- Return:			
- Description:			 				  				
- Usage:			
****************************************************************************/
int ProcessDecodedModifyMessage(t_ARCA_Book_GroupInfo *workingGroup, struct gsxBook_Modify_Message_Body *msg)
{
	/*
	Check current stock symbol index in current session
	*/
	int stockSymbolIndex;
	if (CheckSymbolIndex(msg->symbolIndex, &stockSymbolIndex) == AB_IGNORE)
	{
		return SUCCESS;
	}

	/* ARCA XDP now send orderID in 4 bytes, and also result in duplicate order ID
		So we need to find a way to avoid this duplication
		--> construct a new reference number base on the orderID + ARCA symbol index
		refNum = <4Bytes symbolIndex><4Bytes OrderID>
	*/
	long _refNum = msg->symbolIndex; 
	_refNum = ((_refNum << 32) | msg->orderID);

	t_OrderDetail order;
	order.shareVolume = msg->volume;
	order.sharePrice = (double)msg->priceNumerator/TEN_RAISE_TO[msg->priceScaleCode];
	order.refNum = _refNum;
	order.tradeSession = 0;
	
	//Going to delete old quote, and then insert with new quote
	int side = (msg->side == 'S') ? ASK_SIDE:BID_SIDE;
	
	t_PartialHashingItem *ARCA_Hashing = FindPartialHashingItem(BOOK_ARCA, order.refNum);
	int retval = SUCCESS;
	
	if (ARCA_Hashing != NULL)
	{
		stockSymbolIndex = ARCA_Hashing->stockSymbolIndex;
		pthread_mutex_lock(&deleteQuotesMgmtMutex[BOOK_ARCA][stockSymbolIndex][side]);
		ARCA_Hashing = FindPartialHashingItem(BOOK_ARCA, order.refNum);
		
		if (ARCA_Hashing != NULL)
		{
			t_OrderDetailNode *oldNode = ARCA_Hashing->node;

			DeletePartialHashingItem(BOOK_ARCA, order.refNum);
			
			if (oldNode != NULL)
			{
				order.tradeSession = oldNode->info.tradeSession;
				
				if (GeneralDelete(stockBook[BOOK_ARCA][side][stockSymbolIndex], oldNode, side, stockSymbolIndex) != SUCCESS)
				{
					TraceLog(ERROR_LEVEL, "ARCA Book processing: Cannot delete order %d %lf\n", order.refNum, order.sharePrice);
					retval = ERROR;
				}
			}
		}
		
		pthread_mutex_unlock(&deleteQuotesMgmtMutex[BOOK_ARCA][stockSymbolIndex][side]);
	}
	
	// Add new node
	pthread_mutex_lock(&deleteQuotesMgmtMutex[BOOK_ARCA][stockSymbolIndex][side]);
	
	if (order.tradeSession == 0)
	{
		//Find trade session from backup list since it's missing
		t_ArcaHashingBackupItem *backupItem = FindBackupPartialHashingItem(BOOK_ARCA, order.refNum);
		if (backupItem != NULL)
		{
			order.tradeSession = backupItem->tradeSession;
			DeleteBackupPartialHashingItem(BOOK_ARCA, order.refNum);
		}
	}
	
	// Find position of partial hashing item to insert
	int rowIndex, columnIndex;
	if (RegisterPartialHashingItem_ARCA(order.refNum, &rowIndex, &columnIndex) == ERROR)
	{
		TraceLog(ERROR_LEVEL, "ARCA Book processing: We can't find free slot in PartialHash to insert\n");
		retval = ERROR;
	}
		
	t_OrderDetailNode *retNode = GeneralInsert(stockBook[BOOK_ARCA][side][stockSymbolIndex], &order, side, stockSymbolIndex);
		
	if (retNode == NULL)
	{
		TraceLog(ERROR_LEVEL, "ARCA Book processing: We can not insert this order into Stock Book\n");
		retval = ERROR;
	}

	// Insert partial hashing item
	if (InsertPartialHashingItem_ARCA(retNode, order.refNum, stockSymbolIndex, msg->side, rowIndex, columnIndex) == ERROR)
	{
		TraceLog(ERROR_LEVEL, "ARCA Book processing: We can't insert order into partial hashing list\n");
		retval = ERROR;
	}

	pthread_mutex_unlock(&deleteQuotesMgmtMutex[BOOK_ARCA][stockSymbolIndex][side]);

	return retval;
}

/****************************************************************************
- Function name:	ProcessDecodedDeleteMessage
- Input:			
- Output:											
- Return:			
- Description:			 				  				
- Usage:			
****************************************************************************/
int ProcessDecodedDeleteMessage(t_ARCA_Book_GroupInfo *workingGroup, struct gsxBook_Delete_Message_Body *msg)
{
	/*
	Check current stock symbol index in current session
	*/
	int stockSymbolIndex;
	if ( CheckSymbolIndex(msg->symbolIndex, &stockSymbolIndex) == AB_IGNORE)
	{
		return SUCCESS;
	}

	/* ARCA XDP now send orderID in 4 bytes, and also result in duplicate order ID
		So we need to find a way to avoid this duplication
		--> construct a new reference number base on the orderID + ARCA symbol index
		refNum = <4Bytes symbolIndex><4Bytes OrderID>
	*/
	
	long _refNum = msg->symbolIndex; 
	_refNum = ((_refNum << 32) | msg->orderID);
	
	t_OrderDetail order;
	order.refNum = _refNum;
	
	int retval = SUCCESS;
	
	t_PartialHashingItem *ARCA_Hashing = FindPartialHashingItem(BOOK_ARCA, order.refNum);
	
	if (ARCA_Hashing != NULL)
	{
		// Get stock symbol index from symbol string
		stockSymbolIndex = ARCA_Hashing->stockSymbolIndex;
		
		int side = (msg->side == 'S')?ASK_SIDE:BID_SIDE;

		pthread_mutex_lock(&deleteQuotesMgmtMutex[BOOK_ARCA][stockSymbolIndex][side]);
		ARCA_Hashing = FindPartialHashingItem(BOOK_ARCA, order.refNum);
		
		if (ARCA_Hashing != NULL)
		{
			t_OrderDetailNode *oldNode = ARCA_Hashing->node;
			
			// Delete old node
			DeletePartialHashingItem(BOOK_ARCA, order.refNum);
			
			if (oldNode != NULL)
			{
				if (GeneralDelete(stockBook[BOOK_ARCA][side][stockSymbolIndex], oldNode, side, stockSymbolIndex) != SUCCESS)
				{
					TraceLog(ERROR_LEVEL, "ARCA Book processing: Cannot delete node %d %lf\n", order.refNum, oldNode->info.sharePrice);
					retval = ERROR;
				}
			}
		}

		pthread_mutex_unlock(&deleteQuotesMgmtMutex[BOOK_ARCA][stockSymbolIndex][side]);
	}
	
	DeleteBackupPartialHashingItem(BOOK_ARCA, order.refNum);
	
	return retval;
}


/****************************************************************************
- Function name:	ProcessDecodedExecuteMessage
- Input:			
- Output:											
- Return:			
- Description:			 				  				
- Usage:			
****************************************************************************/
int ProcessDecodedExecuteMessage(t_ARCA_Book_GroupInfo *workingGroup, struct gsxBook_Execute_Message_Body *msg)
{
	//Check reason code from message and do accordingly
	if (msg->reasonCode == 0)	//Default: 0 � Not yet implemented, we ignore the message
	{
		return SUCCESS;
	}
	
	/*
	Check current stock symbol index in current session
	*/
	int stockSymbolIndex;
	if (CheckSymbolIndex(msg->symbolIndex, &stockSymbolIndex) == AB_IGNORE) 
	{
		return SUCCESS;
	}

	
	/* ARCA XDP now send orderID in 4 bytes, and also result in duplicate order ID
		So we need to find a way to avoid this duplication
		--> construct a new reference number base on the orderID + ARCA symbol index
		refNum = <4Bytes symbolIndex><4Bytes OrderID>
	*/
	long refNum = msg->symbolIndex; 
	refNum = ((refNum << 32) | msg->orderID);
	
	int retval = SUCCESS;
	
	t_PartialHashingItem *ARCA_Hashing = FindPartialHashingItem(BOOK_ARCA, refNum);
	
	if (ARCA_Hashing != NULL)
	{
		char _side = ARCA_Hashing->side;
		int side = (_side == 'S')?ASK_SIDE:BID_SIDE;
		stockSymbolIndex = ARCA_Hashing->stockSymbolIndex;
		pthread_mutex_lock(&deleteQuotesMgmtMutex[BOOK_ARCA][stockSymbolIndex][side]);
		ARCA_Hashing = FindPartialHashingItem(BOOK_ARCA, refNum);
		
		if (ARCA_Hashing != NULL)
		{
			t_OrderDetailNode *oldNode = ARCA_Hashing->node;
			
			if (oldNode == NULL)
			{
				DeletePartialHashingItem(BOOK_ARCA, refNum);
			}
			else
			{
				//Check reason code from message and do accordingly
				if (msg->reasonCode == 7) //7 � Partial Fill (Did not lose position)
				{
					t_OrderDetail order;
					order.shareVolume = oldNode->info.shareVolume - msg->volume;
					order.sharePrice = oldNode->info.sharePrice;
					order.tradeSession = oldNode->info.tradeSession;
					order.refNum = refNum;
	
					if (order.shareVolume <= 0)
					{
						DeletePartialHashingItem(BOOK_ARCA, refNum);
						
						if (GeneralDelete(stockBook[BOOK_ARCA][side][stockSymbolIndex], oldNode, side, stockSymbolIndex) != SUCCESS)
						{
							TraceLog(ERROR_LEVEL, "ARCA Book processing: Cannot delete order %d %lf\n", refNum, order.sharePrice);
							retval = ERROR;
						}
						
						DeleteBackupPartialHashingItem(BOOK_ARCA, refNum);
					}
					else
					{
						if (GeneralDelete(stockBook[BOOK_ARCA][side][stockSymbolIndex], oldNode, side, stockSymbolIndex) != SUCCESS)
						{
							TraceLog(ERROR_LEVEL, "ARCA Book processing: Cannot delete order %d %lf\n", order.refNum, order.sharePrice);
							retval = ERROR;
						}
				
						// Add new node
						t_OrderDetailNode *retNode = GeneralInsert(stockBook[BOOK_ARCA][side][stockSymbolIndex], &order, side, stockSymbolIndex);
							
						if (retNode == NULL)
						{
							TraceLog(ERROR_LEVEL, "ARCA Book processing: We can not insert this order into Stock Book\n");
							
							// Delete old node
							DeletePartialHashingItem(BOOK_ARCA, refNum);
						
							retval = ERROR;
						}

						if (UpdatePartialHashingItem_ARCA(retNode, refNum) == ERROR)
						{
							TraceLog(ERROR_LEVEL, "ARCA Book processing: We can't update order into partial hashing list, refNum = %ld\n", refNum);
							retval = ERROR;
						}
					}
				}
				else if (msg->reasonCode == 3)	//3 � Filled
				{
					//We will delete the symbol from Book
					DeletePartialHashingItem(BOOK_ARCA, refNum);
					DeleteBackupPartialHashingItem(BOOK_ARCA, refNum);
					
					if (GeneralDelete(stockBook[BOOK_ARCA][side][stockSymbolIndex], oldNode, side, stockSymbolIndex) != SUCCESS)
					{
						TraceLog(ERROR_LEVEL, "ARCA Book processing: Cannot delete order %d %lf\n", refNum, oldNode->info.sharePrice);
						retval = ERROR;
					}
				}
				else
				{
					//Reason code problem!!!
					TraceLog(ERROR_LEVEL, "ARCA Book processing: Execute Msg, unknown reason code: %d\n", msg->reasonCode);
					retval = ERROR;
				}
			}
		}
		
		pthread_mutex_unlock(&deleteQuotesMgmtMutex[BOOK_ARCA][stockSymbolIndex][side]);
	}

	return retval;
}

/****************************************************************************
- Function name:	ProcessDecodedSymbolClearMessage
- Input:			
- Output:											
- Return:			
- Description:			 				  				
- Usage:			
****************************************************************************/
int ProcessDecodedSymbolClearMessage(t_ARCA_Book_GroupInfo *workingGroup, struct gsxBook_Symbol_Clear_Message_Body *msg)
{
	int stockSymbolIndex;
	if (CheckSymbolIndex(msg->symbolIndex, &stockSymbolIndex) == AB_IGNORE) 
	{
		return SUCCESS;
	}
		
	//Flush ASK SIDE
	pthread_mutex_lock(&deleteQuotesMgmtMutex[BOOK_ARCA][stockSymbolIndex][ASK_SIDE]);
		DeleteAllQuoteOnASide(BOOK_ARCA, stockSymbolIndex, ASK_SIDE);
	pthread_mutex_unlock(&deleteQuotesMgmtMutex[BOOK_ARCA][stockSymbolIndex][ASK_SIDE]);
		
	//Flush BID SIDE
	pthread_mutex_lock(&deleteQuotesMgmtMutex[BOOK_ARCA][stockSymbolIndex][BID_SIDE]);
		DeleteAllQuoteOnASide(BOOK_ARCA, stockSymbolIndex, BID_SIDE);
	pthread_mutex_unlock(&deleteQuotesMgmtMutex[BOOK_ARCA][stockSymbolIndex][BID_SIDE]);
		
	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessTradingSessionChangeMessage
- Input:			
- Output:											
- Return:			
- Description:			 				  				
- Usage:			
****************************************************************************/
int ProcessTradingSessionChangeMessage(t_ARCA_Book_GroupInfo *workingGroup, struct gsxBook_Trading_Session_Change_Message_Body *msg)
{
	//Check current stock symbol index in current session
	int stockSymbolIndex;
	if ( CheckSymbolIndex(msg->symbolIndex, &stockSymbolIndex) == AB_IGNORE)
	{
		return SUCCESS;
	}

	// ------------------------------------------------
	//		Delete all expired order from book
	// ------------------------------------------------
	//Delete expired orders on ASK SIDE
	pthread_mutex_lock(&deleteQuotesMgmtMutex[BOOK_ARCA][stockSymbolIndex][ASK_SIDE]);
		
	if (DeleteAllExpiredArcaOrders(stockSymbolIndex, msg->tradeSession, ASK_SIDE) == ERROR)
	{
		pthread_mutex_unlock(&deleteQuotesMgmtMutex[BOOK_ARCA][stockSymbolIndex][ASK_SIDE]);
		return ERROR;
	}
		
	pthread_mutex_unlock(&deleteQuotesMgmtMutex[BOOK_ARCA][stockSymbolIndex][ASK_SIDE]);
	//Delete expired orders on BID SIDE
	pthread_mutex_lock(&deleteQuotesMgmtMutex[BOOK_ARCA][stockSymbolIndex][BID_SIDE]);
		
	if (DeleteAllExpiredArcaOrders(stockSymbolIndex, msg->tradeSession, BID_SIDE) == ERROR)
	{
		pthread_mutex_unlock(&deleteQuotesMgmtMutex[BOOK_ARCA][stockSymbolIndex][BID_SIDE]);
		return ERROR;
	}
		
	pthread_mutex_unlock(&deleteQuotesMgmtMutex[BOOK_ARCA][stockSymbolIndex][BID_SIDE]);
		
	return SUCCESS;
}


/****************************************************************************
- Function name:	ProcessSymbolIndexMappingMessage
- Input:			
- Output:											
- Return:			
- Description:						 							  				
- Usage:			
****************************************************************************/
int ProcessSymbolIndexMappingMessage(t_ARCA_Book_GroupInfo *workingARCA_Book_Group, struct gsxBook_Symbol_Mapping_Message_Body *msg)
{
	/*	msg->
	unsigned int symbolIndex;
	unsigned char symbol[12];
	unsigned char priceScaleCode;
	unsigned char _padding[3];
	*/
	
	/*if (msg->symbolIndex <= 0 || msg->symbolIndex >= MAX_ARCA_STOCK_SYMBOL)
	{
		TraceStdError("ARCA symbol mapping: stock symbol (%.8s) index (%d) less than 0 or greater than defined const MAX_ARCA_STOCK_SYMBOL (%d). We will ignore it.\n",
						msg->symbol, msg->symbolIndex, MAX_ARCA_STOCK_SYMBOL);
		return SUCCESS;
	}*/
		
	
	char symbol[SYMBOL_LEN] = "\0";
	int stockSymbolIndex = symbolIndexMappingTable[msg->symbolIndex].stockSymbolIndex;
	if (stockSymbolIndex == -1)	//Dont have it, we will add it
	{
		if (ConvertCMSSymbolToComstock((char*)msg->symbol, symbol) == SUCCESS)
		{
			// Update Symbol String
			stockSymbolIndex = GetStockSymbolIndex(symbol);
				
			// If stock symbol is not in SymbolList
			if (stockSymbolIndex == -1)
			{
				// Try to add it to Symbol List
				stockSymbolIndex = UpdateStockSymbolIndex(symbol);
			}
				
			// Check duplicate stock symbol
			if (stockSymbolIndex != -1)
			{
				strncpy(symbolIndexMappingTable[msg->symbolIndex].symbol, symbol, SYMBOL_LEN);
				symbolIndexMappingTable[msg->symbolIndex].stockSymbolIndex = stockSymbolIndex;
				symbolIndexMappingTable[msg->symbolIndex].priceScaleCode = msg->priceScaleCode;
				StockSymbolIndex_to_ArcaSymbolIndex_Mapping[stockSymbolIndex] = msg->symbolIndex;
			}
		}
		else
		{
			ArcaSymbolIndexIgnored[msg->symbolIndex] = 1;
			TraceLog(DEBUG_LEVEL, "New ARCA Symbol: Ignored invalid symbol: %s\n", msg->symbol);
		}
	}

	return SUCCESS;
}

/****************************************************************************
- Function name:	ReleaseARCABookResouce
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ReleaseARCABookResouce(void)
{
	int i, ret;
	int isError1 = 0;	//data channels
	pthread_mutex_lock(&DBLMgmt.mutexLock);
	int needToRelease = (IsDBLTimedOut(ARCA_Book_Conf.dataDev) == 0) ? 1:0;
	
	for (i = 0; i < MAX_ARCA_GROUPS; i++)
	{
		//data channels (one device, all channels)
		sem_destroy(&ARCA_Book_Conf.group[i].dataInfo.sem);

		if (ARCA_Book_Conf.group[i].dataChannel != NULL)
		{
			if (needToRelease == 1)
			{
				if (isError1 == 0)
				{
					ret = LeaveDBLMulticastGroup(ARCA_Book_Conf.group[i].dataChannel, ARCA_Book_Conf.group[i].dataIP);
					if (ret == SUCCESS)
					{
						TraceLog(DEBUG_LEVEL, "ARCA Book: Left data multicast group index (%d)\n", i);
					}
				
					if (ret != EADDRNOTAVAIL)
						dbl_unbind(ARCA_Book_Conf.group[i].dataChannel);
					else
						isError1 = 1;
				}
			}
			ARCA_Book_Conf.group[i].dataChannel = NULL;
		}

		//retran channels (one device - one channel)
		if (ARCA_Book_Conf.group[i].retranChannel != NULL)
		{
			ret = LeaveDBLMulticastGroup(ARCA_Book_Conf.group[i].retranChannel, ARCA_Book_Conf.group[i].retranIP);
			if (ret == SUCCESS)
			{
				TraceLog(DEBUG_LEVEL, "ARCA Book: Left retransmission multicast group index (%d)\n", i);
			}

			if (ret != EADDRNOTAVAIL)
				dbl_unbind(ARCA_Book_Conf.group[i].retranChannel);
			
			ARCA_Book_Conf.group[i].retranChannel = NULL;
		}

		if (ARCA_Book_Conf.group[i].retranDev != NULL)
		{
			dbl_close(ARCA_Book_Conf.group[i].retranDev);
			ARCA_Book_Conf.group[i].retranDev = NULL;
		}
		
		// Refresh: one dev, 1 channel
		if (ARCA_Book_Conf.group[i].refreshChannel != NULL)
		{
			ret = LeaveDBLMulticastGroup(ARCA_Book_Conf.group[i].refreshChannel, ARCA_Book_Conf.group[i].refreshIP);
			if (ret == SUCCESS)
			{
				TraceLog(DEBUG_LEVEL, "ARCA Book: Left refresh multicast group index (%d)\n", i);
			}

			if (ret != EADDRNOTAVAIL)
				dbl_unbind(ARCA_Book_Conf.group[i].refreshChannel);
			ARCA_Book_Conf.group[i].refreshChannel = NULL;
		}
	}

	if (ARCA_Book_Conf.dataDev != NULL)
	{
		//dbl_close(ARCA_Book_Conf.dataDev);
		ARCA_Book_Conf.dataDev = NULL;
	}

	if (ARCA_Book_Conf.retranRequestSocket != -1)
	{
		CloseAndResetSocket(&ARCA_Book_Conf.retranRequestSocket);
		ARCA_Book_Conf.retranRequestSocket = -1;
	}
	
	pthread_mutex_unlock(&DBLMgmt.mutexLock);
	
	usleep(200); // ensure receiving data thread is aware this leaving

	return SUCCESS;
}

/****************************************************************************
- Function name:	DisconnectARCABook
- Input:			
- Output:											
- Return:			
- Description:						 							  				
- Usage:			
****************************************************************************/
int DisconnectARCABook(void)
{
	bookStatusMgmt[BOOK_ARCA].shouldConnect = NO;	
	manFlushBookFlag[BOOK_ARCA] = NO;
	
	int i;
	
	pthread_mutex_lock(&disconnectArcaBookMutex);
	
	for (i = 0; i < MAX_ARCA_GROUPS; i++)
	{
		ARCA_Book_Conf.group[i].dataInfo.connectionFlag = 0;
		sem_post(&ARCA_Book_Conf.group[i].dataInfo.sem);
	}
	
	pthread_mutex_unlock(&disconnectArcaBookMutex);
	return SUCCESS;
}

/****************************************************************************
- Function name:	BuildAndSendHeartbeatResponse
- Input:			
- Output:											
- Return:			
- Description:						 							  				
- Usage:			
****************************************************************************/
int BuildAndSendHeartbeatResponse()
{
	t_ShortConverter shortValue;
	t_IntConverter intValue;
	
	char msgToSend[80];
	memset(msgToSend, 0, 80);
	
	/* 
	Packet Header
	*/
	// Packet Size
	shortValue.value = 30;
	msgToSend[0] = shortValue.c[0];
	msgToSend[1] = shortValue.c[1];
	
	// Packet Type
	msgToSend[2] = ARCA_ORIGINAL_PACKET;
	
	// Number Msgs
	msgToSend[3] = 1;
	
	// Msg Seq Num
	intValue.value = 0;
	msgToSend[4] = intValue.c[0];
	msgToSend[5] = intValue.c[1];
	msgToSend[6] = intValue.c[2];
	msgToSend[7] = intValue.c[3];
	
	// Send Time
	intValue.value = 0;
	msgToSend[8] = intValue.c[0];
	msgToSend[9] = intValue.c[1];
	msgToSend[10] = intValue.c[2];
	msgToSend[11] = intValue.c[3];
	
	// Send Time NS
	intValue.value = 0;
	msgToSend[12] = intValue.c[0];
	msgToSend[13] = intValue.c[1];
	msgToSend[14] = intValue.c[2];
	msgToSend[15] = intValue.c[3];
	
	/*
	Message body
	*/
	// Msg size
	shortValue.value = 14;
	msgToSend[16] = shortValue.c[0];
	msgToSend[17] = shortValue.c[1];
	
	// Msg Type
	shortValue.value = ARCA_HEARTBEAT_RESPONSE_MSG;
	msgToSend[18] = shortValue.c[0];
	msgToSend[19] = shortValue.c[1];
				
	// SourceID
	strncpy(&msgToSend[20], ARCA_Book_Conf.RetransmissionRequestSource, 10);  
	
	// Send message to server
	pthread_mutex_lock(&ARCA_Book_Conf.retranMutex);
	
	int retval = send(ARCA_Book_Conf.retranRequestSocket, msgToSend, 30, 0);
	
	pthread_mutex_unlock(&ARCA_Book_Conf.retranMutex);
	
	if (retval != 30)
	{	
		TraceLog(ERROR_LEVEL, "ARCA Book: Cannot send heartbeat response to server\n");
		return ERROR;
	}
	
	return SUCCESS;
}

/************************************************************************************
- Function name:	ParseBookMsg
- Input:
- Output:
- Return:
- Description:
************************************************************************************/
int32_t ParseBookMsg(union book_message_union_binary *dstMsg, unsigned char *tempBuffer)
{
	/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	BEGIN: Parse all fields inside Message Body Header
	@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/
	
	// Msg Size
	//dstMsg->mHeader.msgSize = GetUnsignedShortNumber(tempBuffer);
	memcpy(&dstMsg->mHeader.msgSize, tempBuffer, 2);
		
	// Msg Type
	//dstMsg->mHeader.msgType = GetUnsignedShortNumber(&tempBuffer[2]);
	memcpy(&dstMsg->mHeader.msgType, &tempBuffer[2], 2);
		
	/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	END: Parse all fields inside Message Body Header
	@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/
	switch (dstMsg->mHeader.msgType)
	{
		/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		Parse all fields inside ADD Message Body
		@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/
		case ARCA_ADD_MSG_BODY:
		{
			// Symbol Index
			//dstMsg->mAdd.symbolIndex = GetUnsignedIntNumber(&tempBuffer[8]);
			memcpy(&dstMsg->mAdd.symbolIndex, &tempBuffer[8], 4);
			/*if (dstMsg->mAdd.symbolIndex >= MAX_ARCA_STOCK_SYMBOL)
			{
				TraceStdError("ARCA Book: (Buffer overflow) symbolIndex(%d) too large\n", dstMsg->mAdd.symbolIndex);
				return ERROR;
			}*/
			// Source Sequence Number
			//dstMsg->mAdd.sourceSeqNum = GetUnsignedIntNumber(&tempBuffer[12]);
	
			// Order ID
			memcpy(&dstMsg->mAdd.orderID, &tempBuffer[16], 4);
			//dstMsg->mAdd.orderID = GetUnsignedIntNumber(&tempBuffer[16]);
							
			// Price Numerator
			memcpy(&dstMsg->mAdd.priceNumerator, &tempBuffer[20], 4);
			//dstMsg->mAdd.priceNumerator = GetUnsignedIntNumber(&tempBuffer[20]);
		
			// Volume
			memcpy(&dstMsg->mAdd.volume, &tempBuffer[24], 4);
			//dstMsg->mAdd.volume = GetUnsignedIntNumber(&tempBuffer[24]);
							
			// Side
			dstMsg->mAdd.side = tempBuffer[28];
							
			// Price scale code => Use the Price scale from the symbol-mapping index.
			dstMsg->mAdd.priceScaleCode = symbolIndexMappingTable[dstMsg->mAdd.symbolIndex].priceScaleCode;
			
			// GTCIndicator
			//dstMsg->mAdd.GTCIndicator = tempBuffer[29];
			
			// TradeSession
			dstMsg->mAdd.tradeSession = tempBuffer[30];

			break;
		}
		/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		Parse all fields inside MODIFY Message Body
		@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/
		case ARCA_MOD_MSG_BODY:
		{
			// Symbol Index
			memcpy(&dstMsg->mModify.symbolIndex, &tempBuffer[8], 4);
			//dstMsg->mModify.symbolIndex = GetUnsignedIntNumber(&tempBuffer[8]);
			/*if (dstMsg->mModify.symbolIndex >= MAX_ARCA_STOCK_SYMBOL)
			{
				TraceStdError("ARCA Book: (Buffer overflow) symbolIndex(%d) too large\n", dstMsg->mModify.symbolIndex);
				return ERROR;
			}*/
			// Source Sequence Number
			//dstMsg->mModify.sourceSeqNum = GetUnsignedIntNumber(&tempBuffer[12]);
	
			// Order ID
			memcpy(&dstMsg->mModify.orderID, &tempBuffer[16], 4);
			//dstMsg->mModify.orderID = GetUnsignedIntNumber(&tempBuffer[16]);
							
			// Price Numerator
			memcpy(&dstMsg->mModify.priceNumerator, &tempBuffer[20], 4);
			//dstMsg->mModify.priceNumerator = GetUnsignedIntNumber(&tempBuffer[20]);
		
			// Volume
			memcpy(&dstMsg->mModify.volume, &tempBuffer[24], 4);
			//dstMsg->mModify.volume = GetUnsignedIntNumber(&tempBuffer[24]);
							
			// Side
			dstMsg->mModify.side = tempBuffer[28];
							
			// Price scale code => Use the Price scale from the symbol-mapping index.
			dstMsg->mModify.priceScaleCode = symbolIndexMappingTable[dstMsg->mModify.symbolIndex].priceScaleCode;
			
			// GTCIndicator
			//dstMsg->mModify.GTCIndicator = tempBuffer[29];
			
			// ReasonCode
			//dstMsg->mModify.reasonCode = tempBuffer[30];
			
			break;
		}
		/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		Parse all fields inside DELETE Message Body
		@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/
		case ARCA_DEL_MSG_BODY:
		{
			// Symbol Index
			memcpy(&dstMsg->mDelete.symbolIndex, &tempBuffer[8], 4);
			//dstMsg->mDelete.symbolIndex = GetUnsignedIntNumber(&tempBuffer[8]);
			
			// Source Sequence Number
			//dstMsg->mDelete.sourceSeqNum = GetUnsignedIntNumber(&tempBuffer[12]);
	
			// Order ID
			//dstMsg->mDelete.orderID = GetUnsignedIntNumber(&tempBuffer[16]);
			memcpy(&dstMsg->mDelete.orderID, &tempBuffer[16], 4);
			
			// Side
			dstMsg->mDelete.side = tempBuffer[20];
			
			// GTCIndicator
			//dstMsg->mDelete.GTCIndicator = tempBuffer[21];
			
			// SessionID
			//dstMsg->mDelete.reasonCode = tempBuffer[22];
			
			break;
		}
		/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		Parse all fields inside EXECUTE Message Body
		@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/
		case ARCA_EXEC_MSG_BODY:
		{
			// Symbol Index
			//dstMsg->mExecute.symbolIndex = GetUnsignedIntNumber(&tempBuffer[8]);
			memcpy(&dstMsg->mExecute.symbolIndex, &tempBuffer[8], 4);
			
			// Source Sequence Number
			//dstMsg->mExecute.sourceSeqNum = GetUnsignedIntNumber(&tempBuffer[12]);
	
			// Order ID
			//dstMsg->mExecute.orderID = GetUnsignedIntNumber(&tempBuffer[16]);
			memcpy(&dstMsg->mExecute.orderID, &tempBuffer[16], 4);
							
			// Price Numerator
			//dstMsg->mExecute.priceNumerator = GetUnsignedIntNumber(&tempBuffer[20]);
		
			// Volume
			//dstMsg->mExecute.volume = GetUnsignedIntNumber(&tempBuffer[24]);
			memcpy(&dstMsg->mExecute.volume, &tempBuffer[24], 4);
							
			// Price scale code => Use the Price scale from the symbol-mapping index.
			//dstMsg->mExecute.priceScaleCode = symbolIndexMappingTable[dstMsg->mExecute.symbolIndex].priceScaleCode;
			
			// GTCIndicator
			//dstMsg->mExecute.GTCIndicator = tempBuffer[28];
			
			// ReasonCode
			dstMsg->mExecute.reasonCode = tempBuffer[29];
			
			// TradeId
			//dstMsg->mExecute.tradeId = GetUnsignedIntNumber(&tempBuffer[30]);
			
			break;
		}
		/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		Parse all fields inside IMBALANCE Message Body
		@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/
		case ARCA_IMB_MSG_BODY:
		{	
			break;
		}
		/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		Parse all fields inside VENDOR_MAPPING Message Body
		@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/
		case ARCA_VENDOR_MAPPING_MSG:
		{	
			break;
		}
		/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		Parse all fields inside SYMBOL_CLEAR Message Body
		@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/
		case ARCA_SYMBOL_CLEAR_MSG:
		{
			// Symbol Index
			memcpy(&dstMsg->mSymbolClear.symbolIndex, &tempBuffer[12], 4);
			//dstMsg->mSymbolClear.symbolIndex = GetUnsignedIntNumber(&tempBuffer[12]);
			break;
		}
		/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		Parse all fields inside TRADING_SESSION_CHANGE Message Body
		@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/
		case ARCA_TRADING_SESSION_CHANGE_MSG:
		{	
			memcpy(&dstMsg->mTradingSessionChange.symbolIndex, &tempBuffer[12], 4);
			//dstMsg->mTradingSessionChange.symbolIndex = GetUnsignedIntNumber(&tempBuffer[12]);
			dstMsg->mTradingSessionChange.tradeSession = tempBuffer[20];
			break;
		}
		/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		Parse all fields inside SECURITY_STATUS Message Body
		@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/
		case ARCA_SECURITY_STATUS_MSG:
			break;
		case ARCA_SYMBOL_INDEX_MAPPING_MSG:
		{
			// Symbol Index
			//dstMsg->mSymbolMapping.symbolIndex = GetUnsignedIntNumber(&tempBuffer[4]);
			memcpy(&dstMsg->mSymbolMapping.symbolIndex, &tempBuffer[4], 4);
			
			//Symbol
			memcpy(dstMsg->mSymbolMapping.symbol, &tempBuffer[8], 11);
			dstMsg->mSymbolMapping.symbol[11] = 0;
			
			//Price scale code
			dstMsg->mSymbolMapping.priceScaleCode = tempBuffer[24];
			break;
		}
		case ARCA_SOURCE_TIME_REF_MSG:
			break;
		case ARCA_TRADE_MSG: 
			break;
		case ARCA_TRADE_CANCELED_MSG: 
			break;
		case ARCA_TRADE_CORRECTION_MSG: 
			break;
		case ARCA_STOCK_SUMMARY_MSG: 
			break;
		case ARCA_PBBO_MSG: 
			break;
		case ARCA_ADD_REFRESH_MSG: 
			break;
		default:
			TraceLog(ERROR_LEVEL, "Parse ARCA Book message: Unknown message type (%d)\n", dstMsg->mHeader.msgType);
			return SUCCESS;
	}
	
	return SUCCESS;
}


/****************************************************************************
- Function name:	LoadAllSymbolMappings
- Input:			fileName
- Output:			
- Return:			
- Description:						 							  				
- Usage:			
****************************************************************************/
int LoadAllSymbolMappings(char *fileName)
{
	char fullPath[MAX_PATH_LEN] = "\0";
	
	if (GetFullConfigPath(fileName, CONF_SYMBOL, fullPath) == NULL_POINTER)
	{
		TraceLog(ERROR_LEVEL, "Have some problem in getting config path for file: '%s'\n", fileName);
		return ERROR;
	}
	
	//Check if file already existed
	if (access(fullPath, F_OK) != 0)
	{
		TraceLog(ERROR_LEVEL, "Could not find symbol mapping file: %s\n", fullPath);
		return ERROR;
	}
	
	//---------------------------------------------------------------------------------
	// Check if file is downloaded today, if not, we should not connect to ARCA Book
	//---------------------------------------------------------------------------------
	if (IsFileTimestampToday(fullPath) == 0)
	{
		TraceLog(ERROR_LEVEL, "ARCA Book: %s is old, please redownload the file\n", fullPath);
		return ERROR;
	}

	//Process the file for index mapping
	TraceLog(DEBUG_LEVEL, "Processing Arca symbol index mapping file '%s'...\n", fullPath);
	
	FILE *fp = fopen(fullPath, "r");
	if (fp == NULL)
	{
		TraceLog(ERROR_LEVEL, "Could not open Arca symbol index mapping file.\n");
		return ERROR;
	}
	
	char line[512];
	memset(line, 0, 512);
	
	char *b, *e;
	
	while (fgets(line, 512, fp) != NULL)
	{
		//Parse the line
		/* 	Line format:
			   Symbol|CQS symbol|SymbolIndex|NYSE Market|Listed Market|TickerDesignation|UOT|PriceScaleCode|SystemID|Bloomberg BSID|Bloomberg Global ID
			Example of a line:
		       ALP PRP|ALPpP|4421|N|N|A|100|4|0|627065504986|BBG000007MM8
			We need only following info: CQS symbol, Symbol Index + PriceScaleCode
		*/
		
		/*Get CQS symbol*/
		b = strchr(line, '|');
		if (b == NULL)
		{
			TraceLog(ERROR_LEVEL, "ARCA Symbol Mapping file: Wrong line format: '%s'\n", line);
			memset(line, 0, 512);
			continue;
		}
		b++;
		
		//CQS symbol|SymbolIndex
		e = strchr(b, '|');
		if (e == NULL)
		{
			TraceLog(ERROR_LEVEL, "ARCA Symbol Mapping file: Wrong line format: '%s'\n", line);
			memset(line, 0, 512);
			continue;
		}
		*e = 0;
		
		//Now take the CQS format symbol
		char tmpSymbol[32];
		memset (tmpSymbol, 0, 32);
		strncpy(tmpSymbol, b, 31);
		
		char symbol[SYMBOL_LEN];
		memset(symbol, 0, SYMBOL_LEN);
		strncpy(symbol, tmpSymbol, SYMBOL_LEN);
		
		*e = '|';
	
		/*Get symbol index*/
		b = ++e;
		e = strchr(b, '|');
		if (e == NULL)
		{
			TraceLog(ERROR_LEVEL, "ARCA Symbol Mapping file: Wrong line format: '%s'\n", line);
			memset(line, 0, 512);
			continue;
		}
		*e=0;
		int symbolIndex = atoi(b);
		*e='|';
		
		/*bypass NYSE Market|Listed Market|TickerDesignation|UOT|*/
		e++;
		b = strchr(e, '|');
		if (b == NULL)	{TraceLog(ERROR_LEVEL, "ARCA Symbol Mapping file: Wrong line format: '%s'\n", line); memset(line, 0, 512); continue;}
		b++;
		
		b = strchr(b, '|');
		if (b == NULL)	{TraceLog(ERROR_LEVEL, "ARCA Symbol Mapping file: Wrong line format: '%s'\n", line); memset(line, 0, 512); continue;}
		b++;
		
		b = strchr(b, '|');
		if (b == NULL)	{TraceLog(ERROR_LEVEL, "ARCA Symbol Mapping file: Wrong line format: '%s'\n", line); memset(line, 0, 512); continue;}
		b++;
		
		b = strchr(b, '|');
		if (b == NULL)	{TraceLog(ERROR_LEVEL, "ARCA Symbol Mapping file: Wrong line format: '%s'\n", line); memset(line, 0, 512); continue;}
		b++;
		
		/*Get Price Scale Code*/
		e = strchr(b, '|');
		if (e == NULL)	{TraceLog(ERROR_LEVEL, "ARCA Symbol Mapping file: Wrong line format: '%s'\n", line); memset(line, 0, 512); continue;}
		*e = 0;
		int priceScale = atoi(b);
		*e = '|';
		
		/*****************************
			Add to data structure
		*****************************/
		if (symbolIndex <= 0 || symbolIndex >= MAX_ARCA_STOCK_SYMBOL)
		{
			TraceLog(ERROR_LEVEL, "ARCA symbol mapping: stock symbol (%.8s) index (%d) less than 0 or greater than defined const MAX_ARCA_STOCK_SYMBOL (%d)\n", symbol, symbolIndex, MAX_ARCA_STOCK_SYMBOL);
			memset(line, 0, 512);
			continue;
		}
		
		if (strlen(tmpSymbol) > SYMBOL_LEN)
		{
			TraceLog(DEBUG_LEVEL, "ARCA XDP: Ignore symbol: '%s'\n", tmpSymbol);
			ArcaSymbolIndexIgnored[symbolIndex] = 1;
			memset(line, 0, 512);
			continue;
		}
		
		// Update Symbol String
		int stockSymbolIndex = GetStockSymbolIndex(symbol);
		
		// If stock symbol is not in SymbolList
		if (stockSymbolIndex == -1)
		{
			// Try to add it to Symbol List
			stockSymbolIndex = UpdateStockSymbolIndex(symbol);
		}

		// Check duplicate stock symbol
		if (stockSymbolIndex != -1)
		{
			StockSymbolIndex_to_ArcaSymbolIndex_Mapping[stockSymbolIndex] = symbolIndex;
		}
		else
		{
			TraceLog(DEBUG_LEVEL, "ARCA XDP: Ignore symbol '%s'\n", symbol);
			ArcaSymbolIndexIgnored[symbolIndex] = 1;
			memset(line, 0, 512);
			continue;
		}
		
		strncpy(symbolIndexMappingTable[symbolIndex].symbol, symbol, SYMBOL_LEN);
		symbolIndexMappingTable[symbolIndex].priceScaleCode = priceScale;		
		symbolIndexMappingTable[symbolIndex].stockSymbolIndex = stockSymbolIndex;

		memset(line, 0, 512);
	}

	fclose(fp);
	TraceLog(DEBUG_LEVEL, "Arca symbol index mapping file has been processed.\n");
	return SUCCESS;
}
