/*****************************************************************************
**	Project:		Equity Arbitrage Application
**	Filename:		bbo_mgmt.c
**	Description:	This file contains function definitions that were declared
					in bbo_mgmt.h	
**	Author:			ThoHN
**	First created:	Sept-2010
**	Last updated:	-----------
*****************************************************************************/

#include "bbo_mgmt.h"
#include "cqs_feed_proc.h"
#include "uqdf_feed_proc.h"
#include "trading_mgmt.h"

t_BBOQuoteList BBOQuoteMgmt[MAX_STOCK_SYMBOL];
t_ExchangeInfo ExchangeStatusList[MAX_EXCHANGE];
int ExchangeIndexList[MAX_BBO_SOURCE][26];
char ParticipantToExchangeIndexMapping[26];

t_BestBBO BestBBO[MAX_STOCK_SYMBOL][MAX_SIDE];
pthread_mutex_t UpdateBestBBOMutex[MAX_STOCK_SYMBOL][MAX_SIDE];

/****************************************************************************
- Function name:	InitBBOMgmtDataStructure
- Input:			
- Output:		N/A
- Return:		Success or Failure
- Description:	+ The routine hides the following facts
				  - Initialize all stock books
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
void InitBBOMgmtDataStructure(void)
{
	int sideIndex;
	int stockSymbolIndex;
	
	for (sideIndex = 0; sideIndex < MAX_SIDE; sideIndex++)
	{
		for (stockSymbolIndex = 0; stockSymbolIndex < MAX_STOCK_SYMBOL; stockSymbolIndex++)
		{
			// Initialize best BBO quote
			BestBBO[stockSymbolIndex][sideIndex].participantId = -1;
			BestBBO[stockSymbolIndex][sideIndex].sourceId = -1;
			BestBBO[stockSymbolIndex][sideIndex].price = 0.0;
			BestBBO[stockSymbolIndex][sideIndex].size = 0;
			
			// Initialize Mutex Locks
			pthread_mutex_init(&UpdateBestBBOMutex[stockSymbolIndex][sideIndex], NULL);
		}
	}
}

/****************************************************************************
- Function name:	InitializeExchangeStatusList
- Input:			
- Output:		
- Return:		
- Description:
- Usage:		
****************************************************************************/
void InitializeExchangeStatusList(void)
{
	int i, j;
	for (i = 0; i < MAX_EXCHANGE; i++)
	{
		for (j = 0; j < MAX_BBO_SOURCE; j++)
		{
			ExchangeStatusList[i].status[j].visible = FALSE;
			ExchangeStatusList[i].status[j].enable = FALSE;
		}
	}
	
	ExchangeStatusList[0].exchangeId = 'P';
	ExchangeStatusList[1].exchangeId = 'Q';
	ExchangeStatusList[2].exchangeId = 'N';
	ExchangeStatusList[3].exchangeId = 'Z';
	ExchangeStatusList[4].exchangeId = 'K';
	
	ExchangeStatusList[5].exchangeId = 'A';
	ExchangeStatusList[6].exchangeId = 'B';
	ExchangeStatusList[7].exchangeId = 'C';
	ExchangeStatusList[8].exchangeId = 'D';
	ExchangeStatusList[9].exchangeId = 'E';
	ExchangeStatusList[10].exchangeId = 'I';
	ExchangeStatusList[11].exchangeId = 'J';
	ExchangeStatusList[12].exchangeId = 'M';
	ExchangeStatusList[13].exchangeId = 'W';
	ExchangeStatusList[14].exchangeId = 'X';
	ExchangeStatusList[15].exchangeId = 'Y';
}

/****************************************************************************
- Function name:	InitializeExchangeMapping
- Input:			
- Output:		
- Return:		
- Description:
- Usage:		
****************************************************************************/
void InitializeExchangeMapping(void)
{	
	int i, j;
	for (i = 0; i < MAX_BBO_SOURCE; i++)		
	{
		for (j = 0; j < 26; j++)
		{
			ExchangeIndexList[i][j] = -1;
		}
	}
	
	/*
	CQS data feed
	A  NYSE AMEX
	B  NASDAQ OMX BX
	C  National Stock Exchange 
	D  FINRA 
	E  Consolidated Quote System 
	I  International Securities Exchange
	J  EDGA Exchange, Inc (Newly added)
	K  EDGX Exchange, Inc (Newly added)
	M  Chicago Stock Exchange 
	N  New York Stock Exchange 
	P  NYSE Arca SM 
	T  Nasdaq Stock Exchange 
	W  CBOE Stock Exchange 
	X  Philadelphia Stock Exchange 
	Y  BATS Y-Exchange, Inc (Newly added)
	Z  BATS Trading
	*/	
	ExchangeIndexList[CQS_SOURCE]['P' - 65] = BOOK_ARCA;
	ExchangeIndexList[CQS_SOURCE]['T' - 65] = BOOK_NASDAQ;
	ExchangeIndexList[CQS_SOURCE]['N' - 65] = BOOK_NYSE;
	ExchangeIndexList[CQS_SOURCE]['Z' - 65] = BOOK_BATSZ;
	ExchangeIndexList[CQS_SOURCE]['K' - 65] = BOOK_EDGX;
	
	ExchangeIndexList[CQS_SOURCE]['A' - 65] = NYSE_Amex;
	ExchangeIndexList[CQS_SOURCE]['B' - 65] = Boston_Stock_Exchange;
	ExchangeIndexList[CQS_SOURCE]['C' - 65] = National_Stock_Exchange;
	ExchangeIndexList[CQS_SOURCE]['D' - 65] = FINRA_ADF;
	ExchangeIndexList[CQS_SOURCE]['E' - 65] = Market_Independent;
	ExchangeIndexList[CQS_SOURCE]['I' - 65] = International_Securities_Exchange;
	ExchangeIndexList[CQS_SOURCE]['J' - 65] = EDGA_Exchange;
	ExchangeIndexList[CQS_SOURCE]['M' - 65] = Chicago_Stock_Exchange;
	ExchangeIndexList[CQS_SOURCE]['W' - 65] = Chicago_Board_Options_Exchange;
	ExchangeIndexList[CQS_SOURCE]['X' - 65] = Philadelphia_Stock_Exchange;
	ExchangeIndexList[CQS_SOURCE]['Y' - 65] = BATS_Y_Exchange;
	
	/*
	UQDF data feed	
	A  NYSE Amex 
	B  NASDAQ OMX BX 
	C  National Stock Exchange 
	D  FINRA ADF 
	E  Market Independent (Generated by SIP) 
	I  International Securities Exchange 
	J  EDGA Exchange, Inc 
	K  EDGX Exchange, Inc 
	M  Chicago Stock Exchange 
	N  NYSE Euronext 
	P  NYSE Arca Exchange 
	Q  NASDAQ OMX 
	W  Chicago Board Options Exchange 
	X  NASDAQ OMX PHLX 
	Y  BATS Y-Exchange, Inc 
	Z  BATS Exchange Inc 
	*/
	ExchangeIndexList[UQDF_SOURCE]['P' - 65] = BOOK_ARCA;
	ExchangeIndexList[UQDF_SOURCE]['Q' - 65] = BOOK_NASDAQ;
	ExchangeIndexList[UQDF_SOURCE]['N' - 65] = BOOK_NYSE;
	ExchangeIndexList[UQDF_SOURCE]['Z' - 65] = BOOK_BATSZ;
	ExchangeIndexList[UQDF_SOURCE]['K' - 65] = BOOK_EDGX;
	
	ExchangeIndexList[UQDF_SOURCE]['A' - 65] = NYSE_Amex;
	ExchangeIndexList[UQDF_SOURCE]['B' - 65] = Boston_Stock_Exchange;
	ExchangeIndexList[UQDF_SOURCE]['C' - 65] = National_Stock_Exchange;
	ExchangeIndexList[UQDF_SOURCE]['D' - 65] = FINRA_ADF;
	ExchangeIndexList[UQDF_SOURCE]['E' - 65] = Market_Independent;
	ExchangeIndexList[UQDF_SOURCE]['I' - 65] = International_Securities_Exchange;
	ExchangeIndexList[UQDF_SOURCE]['J' - 65] = EDGA_Exchange;
	ExchangeIndexList[UQDF_SOURCE]['M' - 65] = Chicago_Stock_Exchange;
	ExchangeIndexList[UQDF_SOURCE]['W' - 65] = Chicago_Board_Options_Exchange;
	ExchangeIndexList[UQDF_SOURCE]['X' - 65] = Philadelphia_Stock_Exchange;
	ExchangeIndexList[UQDF_SOURCE]['Y' - 65] = BATS_Y_Exchange;
	
	ParticipantToExchangeIndexMapping['P' - 65] = BOOK_ARCA;	//ARCA
	ParticipantToExchangeIndexMapping['T' - 65] = BOOK_NASDAQ;	//NASDAQ
	ParticipantToExchangeIndexMapping['Q' - 65] = BOOK_NASDAQ;	//NASDAQ
	ParticipantToExchangeIndexMapping['N' - 65] = BOOK_NYSE;	//NYSE
	ParticipantToExchangeIndexMapping['Z' - 65] = BOOK_BATSZ;	//BATSZ
	ParticipantToExchangeIndexMapping['K' - 65] = BOOK_EDGX;
	
	ParticipantToExchangeIndexMapping['A' - 65] = 5;
	ParticipantToExchangeIndexMapping['B' - 65] = 6;
	ParticipantToExchangeIndexMapping['C' - 65] = 7;
	ParticipantToExchangeIndexMapping['D' - 65] = 8;
	ParticipantToExchangeIndexMapping['E' - 65] = 9;
	ParticipantToExchangeIndexMapping['I' - 65] = 10;
	ParticipantToExchangeIndexMapping['J' - 65] = 11;
	ParticipantToExchangeIndexMapping['M' - 65] = 12;
	ParticipantToExchangeIndexMapping['W' - 65] = 13;
	ParticipantToExchangeIndexMapping['X' - 65] = 14;
	ParticipantToExchangeIndexMapping['Y' - 65] = 15;
	
}

/****************************************************************************
- Function name:	DeleteBBOAtPrice
- Input:			
- Output:		
- Return:		
- Description:
- Usage:		
****************************************************************************/
void DeleteBBOAtPrice(int symbolIndex, int side, double price)
{
	t_BBOQuoteList *symbolInfo;
	int exchangeIndex;
	
	symbolInfo = &BBOQuoteMgmt[symbolIndex];

	if (side == ASK_SIDE)
	{
		for (exchangeIndex = 0; exchangeIndex < MAX_EXCHANGE; exchangeIndex++)
		{
			if ( fge(price, symbolInfo->BBOQuoteInfo[CQS_SOURCE][exchangeIndex].offerPrice) &&
				(symbolInfo->BBOQuoteInfo[CQS_SOURCE][exchangeIndex].offerSize > 0) )
			{
				symbolInfo->BBOQuoteInfo[CQS_SOURCE][exchangeIndex].offerPrice = 0.0;
				symbolInfo->BBOQuoteInfo[CQS_SOURCE][exchangeIndex].offerSize = 0;

				if ( (BestBBO[symbolIndex][ASK_SIDE].participantId == symbolInfo->BBOQuoteInfo[CQS_SOURCE][exchangeIndex].participantId) &&
					 (BestBBO[symbolIndex][ASK_SIDE].sourceId == CQS_SOURCE) )
				{
					UpdateBestBBO(symbolIndex, ASK_SIDE, CQS_SOURCE, NO);
				}
			}
			
			if ( fge(price, symbolInfo->BBOQuoteInfo[UQDF_SOURCE][exchangeIndex].offerPrice) && 
				(symbolInfo->BBOQuoteInfo[UQDF_SOURCE][exchangeIndex].offerSize > 0) )
			{
				symbolInfo->BBOQuoteInfo[UQDF_SOURCE][exchangeIndex].offerPrice = 0.0;
				symbolInfo->BBOQuoteInfo[UQDF_SOURCE][exchangeIndex].offerSize = 0;
				
				if ( (BestBBO[symbolIndex][ASK_SIDE].participantId == symbolInfo->BBOQuoteInfo[UQDF_SOURCE][exchangeIndex].participantId) &&
					 (BestBBO[symbolIndex][ASK_SIDE].sourceId == UQDF_SOURCE) )
				{
					UpdateBestBBO(symbolIndex, ASK_SIDE, UQDF_SOURCE, NO);
				}
			}
		}
	}
	else
	{
		for (exchangeIndex = 0; exchangeIndex < MAX_EXCHANGE; exchangeIndex++)
		{
			if ( fle(price, symbolInfo->BBOQuoteInfo[CQS_SOURCE][exchangeIndex].bidPrice) &&
				(symbolInfo->BBOQuoteInfo[CQS_SOURCE][exchangeIndex].bidSize > 0) )
			{
				symbolInfo->BBOQuoteInfo[CQS_SOURCE][exchangeIndex].bidPrice = 0.0;
				symbolInfo->BBOQuoteInfo[CQS_SOURCE][exchangeIndex].bidSize = 0;
				
				if ( (BestBBO[symbolIndex][BID_SIDE].participantId == symbolInfo->BBOQuoteInfo[CQS_SOURCE][exchangeIndex].participantId) &&
					 (BestBBO[symbolIndex][BID_SIDE].sourceId == CQS_SOURCE) )
				{
					UpdateBestBBO(symbolIndex, BID_SIDE, CQS_SOURCE, NO);
				}
			}
			
			if ( fle(price, symbolInfo->BBOQuoteInfo[UQDF_SOURCE][exchangeIndex].bidPrice) && 
				(symbolInfo->BBOQuoteInfo[UQDF_SOURCE][exchangeIndex].bidSize > 0) )
			{
				symbolInfo->BBOQuoteInfo[UQDF_SOURCE][exchangeIndex].bidPrice = 0.0;
				symbolInfo->BBOQuoteInfo[UQDF_SOURCE][exchangeIndex].bidSize = 0;
				
				if ( (BestBBO[symbolIndex][BID_SIDE].participantId == symbolInfo->BBOQuoteInfo[UQDF_SOURCE][exchangeIndex].participantId) &&
					 (BestBBO[symbolIndex][BID_SIDE].sourceId == UQDF_SOURCE) )
				{
					UpdateBestBBO(symbolIndex, BID_SIDE, UQDF_SOURCE, NO);
				}
			}
		}
	}
}

/****************************************************************************
- Function name:	AddQuoteToBBOQuoteMgmt
- Input:			
- Output:		
- Return:		
- Description:
- Usage:		
****************************************************************************/
int AddQuoteToBBOQuoteMgmt(char *symbol, t_BBOQuoteInfo *quote, t_NationalAppendage *nationalAppendage, int sourceId)
{
	// Get stock symbol index

	int symbolIndex = GetStockSymbolIndex(symbol);

	if (symbolIndex == -1)
	{
		symbolIndex = UpdateStockSymbolIndex(symbol);
		if (symbolIndex == -1) return SUCCESS;
	}
	
	// Check exchange status
	int exchangeIndex = ExchangeIndexList[sourceId][quote->participantId - 65];
	if (ExchangeStatusList[exchangeIndex].status[sourceId].enable == FALSE)
	{
		return SUCCESS;
	}
	
	// Check connection status of ARCA
	// We don't get quote if ARCA Book already
	if (exchangeIndex == BOOK_ARCA)
	{
		if (bookStatusMgmt[BOOK_ARCA].isConnected == CONNECTED &&
			bookStatusMgmt[BOOK_ARCA].shouldConnect == YES)
		{	
			memset(&BBOQuoteMgmt[symbolIndex].BBOQuoteInfo[sourceId][exchangeIndex], 0, sizeof(t_BBOQuoteInfo));
			
			return SUCCESS;
		}
	}
	
	// Check connection status of NASDAQ
	// We don't get quote if NASDAQ Book already
	if (exchangeIndex == BOOK_NASDAQ)
	{
		if (bookStatusMgmt[BOOK_NASDAQ].isConnected == CONNECTED &&
			bookStatusMgmt[BOOK_NASDAQ].shouldConnect == YES)
		{
			memset(&BBOQuoteMgmt[symbolIndex].BBOQuoteInfo[sourceId][exchangeIndex], 0, sizeof(t_BBOQuoteInfo));
			
			return SUCCESS;
		}
	}

	nationalAppendage->bidSize = nationalAppendage->bidSize * 100;
	nationalAppendage->offerSize = nationalAppendage->offerSize * 100;

	// Get current quote information of current stock symbol
	t_BBOQuoteList *symbolInfo;
	symbolInfo = &BBOQuoteMgmt[symbolIndex];

	if (fge(nationalAppendage->bidSize, 0.0))
	{
		memcpy(&symbolInfo->nationalBBO[sourceId], nationalAppendage, sizeof(t_NationalAppendage));
	}
	
	quote->bidSize = quote->bidSize * 100;
	quote->offerSize = quote->offerSize * 100;
	
	// Process both qualified and non-qualified quotes
	// memcpy(&symbolInfo->BBOQuoteInfo[sourceId][exchangeIndex], quote, sizeof(t_BBOQuoteInfo));	
		
	if (sourceId == CQS_SOURCE)
	{
		// BBO-Eligible Quotes 
		// Quote Condition A - Slow Quote on the Offer Side
		// Quote Condition B - Slow Quote on the Bid Side	
		// Quote Condition H - Slow Quote on the Bid and Offer Sides
		// Quote Condition O - Opening Quote
		// Quote Condition R - Regular (for FINRA - Market Maker Open)
		// Quote Condition W - Slow Quote Due to Set Slow List on Both the Bid and Offer Sides
		
		// BBO-Ineligible Quotes
		// Quote Condition C - Closing Quote
		// Quote Condition L - Market Maker Quotes Closed
		// Quote Condition N - Non-Firm Quote
		// Quote Condition Y - Sub-Penny Trading (Non-Regulatory)
		// Quote Condition E - Slow Quote Due to a Liquidity Replenishment Point (LRP) or Gap Quote on the Bid Side
		// Quote Condition F - Slow Quote Due to a Liquidity Replenishment Point (LRP) or Gap Quote on the Offer Side
		// Quote Condition G - Trading Range Indication		
		// Quote Condition D - News Dissemination
		// Quote Condition J - Due to Related Security-News Dissemination
		// Quote Condition K - Due to Related Security-News Pending
		// Quote Condition M - Additional Information
		// Quote Condition P - News Pending
		// Quote Condition Q - Additional Information-Due to Related Security
		// Quote Condition V - In View of Common
		// Quote Condition Z - No Open/No Resume			
		// Quote Condition I - Order Imbalance (Non-Regulatory)
		// Quote Condition S - Due to Related Security (Non-Regulatory)
		// Quote Condition U - Slow Quote Due to a NYSE Liquidity Replenishment Point (LRP), Amex Tolerance Breach (Spread, Momentum or Gap Trade Tolerance), or Gap Quote on Both the Bid and Offer Sides
		// Quote Condition X - Equipment Changeover (Non-Regulatory)
		
		if ((quote->quoteCondition == 'A') || 
			(quote->quoteCondition == 'B') || 
			(quote->quoteCondition == 'H') || 
			(quote->quoteCondition == 'O') || 
			(quote->quoteCondition == 'R') || 
			(quote->quoteCondition == 'W'))
		{
			// Process  qualified quotes:  Each new quote that is receives replaces the previous quote.
			memcpy(&symbolInfo->BBOQuoteInfo[sourceId][exchangeIndex], quote, sizeof(t_BBOQuoteInfo));
		}
		else	
		{
			// Remove any quotes we have for that market in our BBO calculation when a non-qualified quote is received
			memset(&symbolInfo->BBOQuoteInfo[sourceId][exchangeIndex], 0, sizeof(t_BBOQuoteInfo));
		}
		
		if (UpdateBestBBO(symbolIndex, ASK_SIDE, sourceId, YES) == SUCCESS)
		{
			UpdateBestBBO(symbolIndex, BID_SIDE, sourceId, NO);
		}
		else
		{
			UpdateBestBBO(symbolIndex, BID_SIDE, sourceId, YES);
		}
	}
	else //UQDF_SOURCE
	{
		// A - Manual Ask, automated Bid *
		// B - Manual Bid, automated Ask *
		// F - Fast trading
		// H - Manual Bid and Ask *
		// I - Order imbalance
		// L - Closed quote
		// N - Non-firm quote
		// O - Opening quote automated *
		// R - Regular, two-sided open quote automated *
		// U - Manual Bid and Ask (non-firm)
		// Y - automated bid, no offer; or automated offer, no bid (one-sided automated) *
		// X - Order influx
		// Z - No open/no resume

		if ((quote->quoteCondition == 'A') || 
			(quote->quoteCondition == 'B') || 
			(quote->quoteCondition == 'H') || 
			(quote->quoteCondition == 'O') || 
			(quote->quoteCondition == 'R') ||
			(quote->quoteCondition == 'Y'))
		{
			// Process  qualified quotes:  Each new quote that is receives replaces the previous quote.
			memcpy(&symbolInfo->BBOQuoteInfo[sourceId][exchangeIndex], quote, sizeof(t_BBOQuoteInfo));
		}
		else
		{
			// Remove any quotes we have for that market in our BBO calculation when a non-qualified quote is received
			memset(&symbolInfo->BBOQuoteInfo[sourceId][exchangeIndex], 0, sizeof(t_BBOQuoteInfo));
		}
		
		if (UpdateBestBBO(symbolIndex, ASK_SIDE, sourceId, YES) == SUCCESS)
		{
			UpdateBestBBO(symbolIndex, BID_SIDE, sourceId, NO);
		}
		else
		{
			UpdateBestBBO(symbolIndex, BID_SIDE, sourceId, YES);
		}
	}
	
	return SUCCESS;
}


/****************************************************************************
- Function name:	GetBestBBO
- Input:		N/A
- Output:		N/A
- Return:		N/A
- Description:		+ The routine hides the following facts
			+ There are no preconditions guaranteed to the routine
- Usage:		N/A
****************************************************************************/
int GetBestBBO(int symbolIndex, int side, t_BestBBO *bestBBO, int sourceId)
{
	int exchangeIndex;
	
	t_BBOQuoteList *symbolInfo;
	symbolInfo = &BBOQuoteMgmt[symbolIndex];
	
	bestBBO->participantId = -1;
	bestBBO->price = 0.0;
	bestBBO->size = 0;
	
	if (side == ASK_SIDE)
	{
		for (exchangeIndex = 0; exchangeIndex < MAX_EXCHANGE; exchangeIndex++)
		{
			if ( (symbolInfo->BBOQuoteInfo[sourceId][exchangeIndex].participantId != -1) &&
				 (symbolInfo->BBOQuoteInfo[sourceId][exchangeIndex].offerSize > 0) )
			{
				if (bestBBO->participantId == -1)
				{
					bestBBO->participantId = symbolInfo->BBOQuoteInfo[sourceId][exchangeIndex].participantId;
					bestBBO->price = symbolInfo->BBOQuoteInfo[sourceId][exchangeIndex].offerPrice;
					bestBBO->size = symbolInfo->BBOQuoteInfo[sourceId][exchangeIndex].offerSize;
					continue;
				}
				else
				{
					if (fgt(bestBBO->price, symbolInfo->BBOQuoteInfo[sourceId][exchangeIndex].offerPrice))
					{
						bestBBO->participantId = symbolInfo->BBOQuoteInfo[sourceId][exchangeIndex].participantId;
						bestBBO->price = symbolInfo->BBOQuoteInfo[sourceId][exchangeIndex].offerPrice;
						bestBBO->size = symbolInfo->BBOQuoteInfo[sourceId][exchangeIndex].offerSize;
					}
					else if (feq(bestBBO->price, symbolInfo->BBOQuoteInfo[sourceId][exchangeIndex].offerPrice))
					{
						if (bestBBO->size < symbolInfo->BBOQuoteInfo[sourceId][exchangeIndex].offerSize)
						{
							bestBBO->participantId = symbolInfo->BBOQuoteInfo[sourceId][exchangeIndex].participantId;
							bestBBO->price = symbolInfo->BBOQuoteInfo[sourceId][exchangeIndex].offerPrice;
							bestBBO->size = symbolInfo->BBOQuoteInfo[sourceId][exchangeIndex].offerSize;
						}
					}
				}
			}
		}
	}
	else	// BID_SIDE
	{
		for (exchangeIndex = 0; exchangeIndex < MAX_EXCHANGE; exchangeIndex++)
		{
			if ((symbolInfo->BBOQuoteInfo[sourceId][exchangeIndex].participantId != -1) && 
				(symbolInfo->BBOQuoteInfo[sourceId][exchangeIndex].bidSize > 0))
			{
				if (bestBBO->participantId == -1)
				{
					bestBBO->participantId = symbolInfo->BBOQuoteInfo[sourceId][exchangeIndex].participantId;
					bestBBO->price = symbolInfo->BBOQuoteInfo[sourceId][exchangeIndex].bidPrice;
					bestBBO->size = symbolInfo->BBOQuoteInfo[sourceId][exchangeIndex].bidSize;
					continue;
				}
				else
				{
					if (flt(bestBBO->price, symbolInfo->BBOQuoteInfo[sourceId][exchangeIndex].bidPrice))
					{
						bestBBO->participantId = symbolInfo->BBOQuoteInfo[sourceId][exchangeIndex].participantId;
						bestBBO->price = symbolInfo->BBOQuoteInfo[sourceId][exchangeIndex].bidPrice;
						bestBBO->size = symbolInfo->BBOQuoteInfo[sourceId][exchangeIndex].bidSize;
					}
					else if (feq(bestBBO->price, symbolInfo->BBOQuoteInfo[sourceId][exchangeIndex].bidPrice))
					{
						if (bestBBO->size < symbolInfo->BBOQuoteInfo[sourceId][exchangeIndex].bidSize)
						{
							bestBBO->participantId = symbolInfo->BBOQuoteInfo[sourceId][exchangeIndex].participantId;
							bestBBO->price = symbolInfo->BBOQuoteInfo[sourceId][exchangeIndex].bidPrice;
							bestBBO->size = symbolInfo->BBOQuoteInfo[sourceId][exchangeIndex].bidSize;
						}
					}
				}
			}
		}
	}
	
	// Check before return for calling the routine
	if (bestBBO->participantId != -1)
	{
		bestBBO->sourceId = sourceId;
		return SUCCESS;
	}

	bestBBO->sourceId = -1;
	return ERROR;
}

/****************************************************************************
- Function name:	UpdateBestBBO
- Input:			N/A
- Output:		N/A
- Return:		N/A
- Description:	+ The routine hides the following facts
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
int UpdateBestBBO(int symbolIndex, int side, char sourceId, int isCheckBBO)
{
	t_BestBBO tmpBBO;
	int isBestChange = 0;

	t_BestBBO *currentBestBBO = &BestBBO[symbolIndex][side];

	// Get best BBO
	int retValue = GetBestBBO(symbolIndex, side, &tmpBBO, sourceId);

	if (retValue == SUCCESS)	// We can get the best BBO!
	{
		pthread_mutex_lock(&UpdateBestBBOMutex[symbolIndex][side]);

		if (!feq(currentBestBBO->price, tmpBBO.price))
		{
			isBestChange = 1;
		}
		
		currentBestBBO->participantId = tmpBBO.participantId;
		currentBestBBO->price = tmpBBO.price;
		currentBestBBO->size = tmpBBO.size;
		currentBestBBO->sourceId = tmpBBO.sourceId;
	
		pthread_mutex_unlock(&UpdateBestBBOMutex[symbolIndex][side]);
		
		if (isBestChange == 1)
		{
			positionCollection[symbolIndex].holdQuote[side].isHold = YES;
		}
	}
	else	// Best BBO is currently not available
	{
		pthread_mutex_lock(&UpdateBestBBOMutex[symbolIndex][side]);

		currentBestBBO->participantId = -1; 
		currentBestBBO->sourceId = -1;
		currentBestBBO->price = 0.0;
		currentBestBBO->size = 0;

		pthread_mutex_unlock(&UpdateBestBBOMutex[symbolIndex][side]);
		
		isBestChange = 1;
	}
		
	if (isCheckBBO == YES && isBestChange == 1)
	{
		int exchangeIndex = -1;
		if (currentBestBBO->participantId != -1)
		{
			exchangeIndex = ExchangeIndexList[(int)sourceId][currentBestBBO->participantId - 65];
		}
		return CheckBBOOrder(symbolIndex, side, exchangeIndex, 0, currentBestBBO->size, currentBestBBO->price, ROUND_LOT_TYPE);
	}
	
	return ERROR;
}

/****************************************************************************
- Function name:	GetBestBBOOrder
- Input:			N/A
- Output:		N/A
- Return:		N/A
- Description:	+ The routine hides the following facts
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
int GetBestBBOOrder(t_BestBBO *bestBBOOrder, int side, int stockSymbolIndex)
{
	pthread_mutex_lock(&UpdateBestBBOMutex[stockSymbolIndex][side]);
		*bestBBOOrder = BestBBO[stockSymbolIndex][side];
	pthread_mutex_unlock(&UpdateBestBBOMutex[stockSymbolIndex][side]);
	return SUCCESS;
}

/****************************************************************************
- Function name:	FlushBBOQuotes
- Input:			
- Output:		
- Return:		
- Description:
- Usage:		
****************************************************************************/
int FlushBBOQuotes(int exchangeIndex, int exchangeID, int sourceId)
{
	int symbolIndex, isBestAsk, isBestBid;
	
	for (symbolIndex = 0; symbolIndex < MAX_STOCK_SYMBOL; symbolIndex++)
	{
		isBestAsk = 0;
		isBestBid = 0;
		
		memset(&BBOQuoteMgmt[symbolIndex].BBOQuoteInfo[sourceId][exchangeIndex], 0, sizeof(t_BBOQuoteInfo));

		//Check to see if we need to update best quotes for this symbol
		if ( (BestBBO[symbolIndex][ASK_SIDE].participantId == exchangeID) &&
			 (BestBBO[symbolIndex][ASK_SIDE].sourceId == sourceId) )
		{
			isBestAsk = 1;
		}
		
		if ( (BestBBO[symbolIndex][BID_SIDE].participantId == exchangeID) &&
			 (BestBBO[symbolIndex][BID_SIDE].sourceId == sourceId) )
		{
			isBestBid = 1;
		}
		
		if (isBestAsk == 1 && isBestBid == 1)
		{
			UpdateBestBBO(symbolIndex, ASK_SIDE, sourceId, NO);
			UpdateBestBBO(symbolIndex, BID_SIDE, sourceId, YES);
		}
		else if (isBestAsk == 1)
		{
			UpdateBestBBO(symbolIndex, BID_SIDE, sourceId, YES);
		}
		else if (isBestBid == 1)
		{
			UpdateBestBBO(symbolIndex, BID_SIDE, sourceId, YES);
		}
	}
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	FlushPMBBOQuotesByFeed
- Input:			
- Output:		
- Return:		
- Description:
- Usage:		
****************************************************************************/
int FlushPMBBOQuotesByFeed(int exchangeID, int sourceId)
{
	int exchangeIndex = 0;
	int symbolIndex = 0;
	int isBestAsk, isBestBid;
	
	exchangeIndex = ExchangeIndexList[sourceId][exchangeID - 65];
	if ((exchangeIndex >= 0) && (exchangeIndex < MAX_EXCHANGE))
	{
		//Start to flush:
		for (symbolIndex = 0; symbolIndex < MAX_STOCK_SYMBOL; symbolIndex++)
		{
			isBestAsk = 0;
			isBestBid = 0;
		
			memset(&BBOQuoteMgmt[symbolIndex].BBOQuoteInfo[sourceId][exchangeIndex], 0, sizeof(t_BBOQuoteInfo));
			
			//Check to see if we need to update best quotes for this symbol
			if ( (BestBBO[symbolIndex][ASK_SIDE].participantId == exchangeID) &&
				(BestBBO[symbolIndex][ASK_SIDE].sourceId == sourceId) )
			{
				isBestAsk = 1;
			}
			
			if ( (BestBBO[symbolIndex][BID_SIDE].participantId == exchangeID) &&
				 (BestBBO[symbolIndex][BID_SIDE].sourceId == sourceId) )
			{
				isBestBid = 1;
			}
			
			if (isBestAsk == 1 && isBestBid == 1)
			{
				UpdateBestBBO(symbolIndex, ASK_SIDE, sourceId, NO);
				UpdateBestBBO(symbolIndex, BID_SIDE, sourceId, YES);
			}
			else if (isBestAsk == 1)
			{
				UpdateBestBBO(symbolIndex, BID_SIDE, sourceId, YES);
			}
			else if (isBestBid == 1)
			{
				UpdateBestBBO(symbolIndex, BID_SIDE, sourceId, YES);
			}
		}
	}
	
	TraceLog(DEBUG_LEVEL, "Flushed all BBO quotes in %s where exchange = %c\n", (sourceId==CQS_SOURCE)?"CQS":"UQDF", exchangeID);
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	FlushPMBBOQuotes
- Input:			
- Output:		
- Return:		
- Description:
- Usage:		
****************************************************************************/
int FlushPMBBOQuotes(int exchangeID)
{
	// Flush Both CQS + UQDF
	int exchangeIndex = 0;
	int isFlush = 0;
	
	exchangeIndex = ExchangeIndexList[CQS_SOURCE][exchangeID - 65];
	if ((exchangeIndex >= 0) && (exchangeIndex < MAX_EXCHANGE))
	{
		isFlush = 1;
	}
	
	exchangeIndex = ExchangeIndexList[UQDF_SOURCE][exchangeID - 65];
	if ((exchangeIndex >= 0) && (exchangeIndex < MAX_EXCHANGE))
	{
		isFlush = 1;
	}
	
	if (isFlush == 1)
	{
		FlushBBOQuotes(exchangeIndex, exchangeID, CQS_SOURCE);
		FlushBBOQuotes(exchangeIndex, exchangeID, UQDF_SOURCE);
	}
	
	TraceLog(DEBUG_LEVEL, "Flushed BBO quotes from AS, exchange = %c\n", exchangeID);
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	ShowFeedsDataForSymbol
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
void ShowFeedsDataForSymbol (int symbolIndex, int sourceId)
{
	//Get current quote information of current stock symbol
	t_BBOQuoteList *symbolInfo;
	symbolInfo = &BBOQuoteMgmt[symbolIndex];
		
	t_NationalAppendage *nationalBBO;
	nationalBBO = &symbolInfo->nationalBBO[sourceId];
	
	//National BBO Information
	if ((nationalBBO->bidSize > 0) || (nationalBBO->offerSize > 0))
	{
		
		TraceLog(DEBUG_LEVEL, "National BBO(%02d:%02d:%02d:%03d): BID %c %d %.2lf; OFFER %c %d %.2lf\n",
				nationalBBO->timeStamp / 3600000, (nationalBBO->timeStamp % 3600000) / 60000, ((nationalBBO->timeStamp % 3600000) % 60000) / 1000, nationalBBO->timeStamp % 1000,
				nationalBBO->bidParticipantId,
				nationalBBO->bidSize,
				nationalBBO->bidPrice,
				nationalBBO->offerParticipantId,
				nationalBBO->offerSize,
				nationalBBO->offerPrice);
	}

	int i;
	t_BBOQuoteInfo *quoteInfo;
	
	for (i = 0; i < MAX_EXCHANGE; i++)
	{
		quoteInfo = &symbolInfo->BBOQuoteInfo[sourceId][i];

		/*if (ExchangeStatusList[i].status[sourceId].enable == FALSE)
		{
			continue;
		}*/

		if ( (quoteInfo->bidSize > 0) || (quoteInfo->offerSize > 0) )
		{
			TraceLog(DEBUG_LEVEL, "BBO (%02d:%02d:%02d:%03d): %c BID %d %.2lf; OFFER %d %.2lf\n",
						quoteInfo->timeStamp / 3600000, (quoteInfo->timeStamp % 3600000) / 60000, ((quoteInfo->timeStamp % 3600000) % 60000) / 1000, quoteInfo->timeStamp % 1000,
						quoteInfo->participantId,
						quoteInfo->bidSize,
						quoteInfo->bidPrice,
						quoteInfo->offerSize,
						quoteInfo->offerPrice);
		}
	}
}
