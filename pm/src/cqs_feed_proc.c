/****************************************************************************
** Project name:	Equity Arbitrage Application
** File name: 		cqs_feed_proc.c
** Description: 	This file contains function definitions that were declared
					in cqs_feed_proc.h
** Author: 			Luan Vo-Kinh
** First created on Oct 03, 2008
** Last updated on Oct 03, 2008
****************************************************************************/
#define _BSD_SOURCE

#include <pthread.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>

#include "utility.h"
#include "book_mgmt.h"
#include "socket_util.h"
#include "trading_mgmt.h"
#include "cqs_feed_proc.h"
#include "stock_symbol.h"
#include "bbo_mgmt.h"
#include "kernel_algorithm.h"
#include "aggregation_server_proc.h"

#define MAX_RECV_CQS_TIMEOUT 62

int arrCQS_DenominatorValue[26];
int arrCQS_NumeratorByte[26];

t_BBOQuoteList BBOQuoteMgmt[MAX_STOCK_SYMBOL];
t_ExchangeInfo ExchangeStatusList[MAX_EXCHANGE];
int ExchangeIndexList[MAX_BBO_SOURCE][26];

extern t_MonitorCQS MonitorCQS[MAX_CQS_MULTICAST_LINES];
extern t_DBLMgmt DBLMgmt;
/****************************************************************************
- Function name:	InitializeCQS_DenominatorValue
- Input:			
- Output:											
- Return:			
- Description:						 							  				
- Usage:			
****************************************************************************/
int InitializeCQS_DenominatorValue(void)
{
	arrCQS_DenominatorValue[1] = 1;
	arrCQS_DenominatorValue[2] = 1;
	arrCQS_DenominatorValue[9] = 1;
	arrCQS_DenominatorValue[10] = 1;
	arrCQS_DenominatorValue[11] = 1;
	arrCQS_DenominatorValue[12] = 1;
	arrCQS_DenominatorValue[13] = 1;
	arrCQS_DenominatorValue[14] = 1;
	arrCQS_DenominatorValue[15] = 1;
	arrCQS_DenominatorValue[16] = 1;
	arrCQS_NumeratorByte[1] = 1;
	arrCQS_NumeratorByte[2] = 1;
	arrCQS_NumeratorByte[9] = 1;
	arrCQS_NumeratorByte[10] = 1;
	arrCQS_NumeratorByte[11] = 1;
	arrCQS_NumeratorByte[12] = 1;
	arrCQS_NumeratorByte[13] = 1;
	arrCQS_NumeratorByte[14] = 1;
	arrCQS_NumeratorByte[15] = 1;
	arrCQS_NumeratorByte[16] = 1;
	
	arrCQS_DenominatorValue[0] = 1;
	arrCQS_DenominatorValue[3] = 8;
	arrCQS_DenominatorValue[4] = 16;
	arrCQS_DenominatorValue[5] = 32;
	arrCQS_DenominatorValue[6] = 64;
	arrCQS_DenominatorValue[7] = 128;
	arrCQS_DenominatorValue[8] = 256;
	arrCQS_DenominatorValue[17] = 10;
	arrCQS_DenominatorValue[18] = 100;
	arrCQS_DenominatorValue[19] = 1000;
	arrCQS_DenominatorValue[20] = 10000;
	arrCQS_DenominatorValue[21] = 100000;
	arrCQS_DenominatorValue[22] = 1000000;
	arrCQS_DenominatorValue[23] = 10000000;
	arrCQS_DenominatorValue[24] = 100000000;
	arrCQS_DenominatorValue[25] = 1;
	
	arrCQS_NumeratorByte[0] = 1;
	arrCQS_NumeratorByte[3] = 1;
	arrCQS_NumeratorByte[4] = 2;
	arrCQS_NumeratorByte[5] = 2;
	arrCQS_NumeratorByte[6] = 2;
	arrCQS_NumeratorByte[7] = 3;
	arrCQS_NumeratorByte[8] = 3;
	arrCQS_NumeratorByte[17] = 1;
	arrCQS_NumeratorByte[18] = 2;
	arrCQS_NumeratorByte[19] = 3;
	arrCQS_NumeratorByte[20] = 4;
	arrCQS_NumeratorByte[21] = 5;
	arrCQS_NumeratorByte[22] = 6;
	arrCQS_NumeratorByte[23] = 7;
	arrCQS_NumeratorByte[24] = 8;
	arrCQS_NumeratorByte[25] = 0;
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	InitCQS_Quote_DataStructure
- Input:			
- Output:											
- Return:			
- Description:						 							  				
- Usage:			
****************************************************************************/
void InitCQS_Quote_DataStructure(void)
{	
	int symbolIndex, exchangeIndex;
	for (symbolIndex = 0; symbolIndex < MAX_STOCK_SYMBOL; symbolIndex++)
	{
		for (exchangeIndex = 0; exchangeIndex < MAX_EXCHANGE; exchangeIndex++)
		{
			memset(&BBOQuoteMgmt[symbolIndex].BBOQuoteInfo[CQS_SOURCE][exchangeIndex], 0, sizeof(t_BBOQuoteInfo));
		}
		
		memset(&BBOQuoteMgmt[symbolIndex].nationalBBO[CQS_SOURCE], 0, sizeof(t_NationalAppendage));
		
		
		if (BestBBO[symbolIndex][ASK_SIDE].sourceId == CQS_SOURCE)
		{
			pthread_mutex_lock(&UpdateBestBBOMutex[symbolIndex][ASK_SIDE]);
			
			BestBBO[symbolIndex][ASK_SIDE].participantId = -1;
			BestBBO[symbolIndex][ASK_SIDE].sourceId = -1;
			BestBBO[symbolIndex][ASK_SIDE].price = 0.0;
			BestBBO[symbolIndex][ASK_SIDE].size = 0;
			
			pthread_mutex_unlock(&UpdateBestBBOMutex[symbolIndex][ASK_SIDE]);
		}
		
		if (BestBBO[symbolIndex][BID_SIDE].sourceId == CQS_SOURCE)
		{
			pthread_mutex_lock(&UpdateBestBBOMutex[symbolIndex][BID_SIDE]);
			
			BestBBO[symbolIndex][BID_SIDE].participantId = -1;
			BestBBO[symbolIndex][BID_SIDE].sourceId = -1;
			BestBBO[symbolIndex][BID_SIDE].price = 0.0;
			BestBBO[symbolIndex][BID_SIDE].size = 0;
			
			pthread_mutex_unlock(&UpdateBestBBOMutex[symbolIndex][BID_SIDE]);
		}
	}
}

/****************************************************************************
- Function name:	InitCQS_Feed_DataStructure
- Input:			
- Output:											
- Return:			
- Description:						 							  				
- Usage:			
****************************************************************************/
void InitCQS_Feed_DataStructure(void)
{	
	TraceLog(DEBUG_LEVEL, "Initialize CQS Feed\n");

	// Initialize denominator value
	InitializeCQS_DenominatorValue();
	
	// Initialize quote list
	InitCQS_Quote_DataStructure();
	
	int i;

	CQS_Feed_Conf.dev = NULL;

	for (i = 0; i < MAX_CQS_MULTICAST_LINES; i++)
	{
		MonitorCQS[i].recvCounter = 0;
		MonitorCQS[i].procCounter = 0;
		strcpy(CQS_Feed_Conf.group[i].dataInfo.bookName, "CQS");
		
		CQS_Feed_Conf.group[i].dataChannel = NULL;

		CQS_Feed_Conf.group[i].index = i;

		// For primary data feed processing
		// Initialize semaphore
		sem_init(&CQS_Feed_Conf.group[i].dataInfo.sem, 0, 0);

		// Initialize buffer that will be used to contain packets from server
		memset(CQS_Feed_Conf.group[i].dataInfo.buffer, 0, MAX_BOOK_MSG_IN_BUFFER * MAX_MTU);
		
		// Initialize counter variables
		CQS_Feed_Conf.group[i].primaryLastSequenceNumber = -1;
		CQS_Feed_Conf.group[i].dataInfo.currentReceivedIndex = 0;
		CQS_Feed_Conf.group[i].dataInfo.currentReadIndex = 0;
		CQS_Feed_Conf.group[i].dataInfo.DisconnectBook = &DisconnectCQS_Feed;
		CQS_Feed_Conf.group[i].dataInfo.connectionFlag = 0;
	}
}

/****************************************************************************
- Function name:	CQS_Feed_Thread
- Input:			
- Output:											
- Return:			
- Description:						 							  				
- Usage:			
****************************************************************************/
void *CQS_Feed_Thread(void *agrs)
{
	SetKernelAlgorithm("[CQS_BASE]");
	
	InitCQS_Feed_DataStructure();
	
	int i;

	while (CQS_StatusMgmt.shouldConnect == YES)
	{

		int ret = CreateDBLDeviceV2(CQS_Feed_Conf.NICName, &CQS_Feed_Conf.dev);

		if (ret == ERROR)
		{
			TraceLog(ERROR_LEVEL, "CQS: Can't create DBL device\n");

			DisconnectCQS_Feed();
			return NULL;
		}

		for (i = 0; i < MAX_CQS_MULTICAST_LINES; i++)
		{
			/* Start threads to receive and process data from primary data feed */
			if (pthread_create(&CQS_Feed_Conf.group[i].primaryProcessThreadID, NULL, (void*)&ThreadProcessCQS_PrimaryData, (void*) &CQS_Feed_Conf.group[i]) != SUCCESS)
			{
				TraceLog(ERROR_LEVEL, "Cannot create thread to process packets\n");

				DisconnectCQS_Feed();
				
				return NULL;
			}
			
			TraceLog(DEBUG_LEVEL, "CQS: MulticastRecvPort(%s:%d) group(%d): connecting\n", CQS_Feed_Conf.group[i].dataIP, CQS_Feed_Conf.group[i].dataPort, (i+1));
			/* Join to Primary multicast group */
			ret = JoinMulticastGroupUsingExistingDeviceV2(CQS_Feed_Conf.dev, CQS_Feed_Conf.group[i].dataIP, CQS_Feed_Conf.group[i].dataPort, &CQS_Feed_Conf.group[i].dataChannel, CQS_Feed_Conf.NICName, (void *) &CQS_Feed_Conf.group[i].dataInfo);
			if (ret == ERROR)
			{
				TraceLog(ERROR_LEVEL, "CQS: Can't connect to Multicast group (%s:%d)\n", CQS_Feed_Conf.group[i].dataIP, CQS_Feed_Conf.group[i].dataPort);

				DisconnectCQS_Feed();

				return NULL;
			}
			
			// Sleep 1000 micro-seconds
			usleep(1000);
		}
		
		// Notify that CQS Feed was connected
		CQS_StatusMgmt.isConnected = CONNECTED;
		for (i = 0; i < MAX_CQS_MULTICAST_LINES; i++)
		{
			CQS_Feed_Conf.group[i].dataInfo.connectionFlag = 1;
		}
		
		// Wait for ALL threads completed
		for (i = 0; i < MAX_CQS_MULTICAST_LINES; i++)
		{
			pthread_join(CQS_Feed_Conf.group[i].primaryProcessThreadID, NULL);
		}

		// Release CQS Feed Resource
		ReleaseCQS_FeedResource();

		InitCQS_Feed_DataStructure();
	}
	
	// Notify CQS Feed was disconnected
	CQS_StatusMgmt.isConnected = DISCONNECTED;
	SendPMDisconnectedBookOrderAlertToAS(SERVICE_CQS_FEED);
	
	TraceLog(DEBUG_LEVEL, "CQS Feed was disconnected\n");
	
	return NULL;
}

/****************************************************************************
- Function name:	ThreadProcessCQS_PrimaryData
- Input:			
- Output:											
- Return:			
- Description:						 							  				
- Usage:			
****************************************************************************/
void *ThreadProcessCQS_PrimaryData(void *args)
{
	SetKernelAlgorithm("[CQS_PROCESS_MSGS]");

	t_CQS_Feed_Group *workingGroup = (t_CQS_Feed_Group *)args;

	// Temp buffer need to store data from receive buffer
	char *tempBuffer;	
	int index;
	
	while (CQS_StatusMgmt.shouldConnect == YES)
	{
		// Wait for new message
		if (sem_wait(&workingGroup->dataInfo.sem) != SUCCESS)
		{
			TraceLog(ERROR_LEVEL, "Cannot sem_wait. CQS Feed will be stopped now (%s %d)\n",
					workingGroup->dataIP, workingGroup->dataPort);

			DisconnectCQS_Feed();
			
			return NULL;
		}
		
		if (CQS_StatusMgmt.shouldConnect == NO)
		{
			return NULL;
		}

		index = workingGroup->dataInfo.currentReadIndex;
		tempBuffer = workingGroup->dataInfo.buffer[index];

		if (tempBuffer[MAX_MTU - 1] != STORED)
		{
			TraceLog(ERROR_LEVEL, "CQS: Receive buffer of line %d is full: received index = %d, read index = %d\n",
				workingGroup->index + 1, workingGroup->dataInfo.currentReceivedIndex, index);

			DisconnectCQS_Feed();
			
			return NULL;
		}

		ProcessCQS_MsgFromPrimaryData(workingGroup, tempBuffer);

		// Update status of buffer
		tempBuffer[MAX_MTU - 1] = READ;

		/*
		If we have Current_Read_Index greater than LIMITATION of INTEGER data type
		that is very dangerous
		*/
		if(index == MAX_BOOK_MSG_IN_BUFFER - 1)
		{
			workingGroup->dataInfo.currentReadIndex = 0;
		}
		else
		{
			workingGroup->dataInfo.currentReadIndex++;
		}
	}
	
	return NULL;	
}

/****************************************************************************
- Function name:	ProcessCQS_MsgFromPrimaryData
- Input:			
- Output:											
- Return:			
- Description:						 							  				
- Usage:			
****************************************************************************/
int ProcessCQS_MsgFromPrimaryData(void *args, char buffer[MAX_MTU])
{
	t_CQS_Feed_Group *workingGroup = (t_CQS_Feed_Group *)args;

	int index, icount, isSkip = 0;
	t_CQS_Msg msgTmp;
	char buffTemp[MAX_MTU];

	if (buffer[0] != SOH_CHAR)
	{
		TraceLog(ERROR_LEVEL, "CQS: Invalid packet from primary data feed, msg = %s\n", buffer);

		return ERROR;
	}

	// Update index
	index = 1;

	while (	(index < MAX_MTU - 1) &&	(buffer[index] != ETX_CHAR)	)
	{
		//Get the full message body
		icount = index + MAX_CQS_HEADER_LEN;
		while ((icount < MAX_MTU - 1) && (buffer[icount] != US_CHAR) && (buffer[icount] != ETX_CHAR))
		{
			icount++;
		}
		
		if (icount == MAX_MTU - 1)
		{
			TraceLog(ERROR_LEVEL, "CQS: Invalid packet from primary data feed, msg = %s\n", buffer);
			return ERROR;
		}
		
		// Check if this is the needed packet: Retransmission Requester is original
		if ( (buffer[index + 4] == 32)  && 
			((buffer[index + 3] == 'O') || (buffer[index + 3] == 'V')) )
		{
			// Get message header
			msgTmp.msgCategory = buffer[index];
			msgTmp.msgType = buffer[index + 1];
			msgTmp.msgSeqNum = atoi(&buffer[index + 8]);
			
			// If we in middle of something, continue with the current msgSeq Number
			if (workingGroup->primaryLastSequenceNumber == -1)
			{
				workingGroup->primaryLastSequenceNumber = msgTmp.msgSeqNum;
			}
			else
			{
				if (msgTmp.msgSeqNum > workingGroup->primaryLastSequenceNumber + 1)
				{
					if (msgTmp.msgType != 'L' && msgTmp.msgCategory == 'C')
					{
						
					}
				}
				else if (msgTmp.msgSeqNum < workingGroup->primaryLastSequenceNumber)
				{
					if (!((msgTmp.msgCategory == 'C') &&
						(msgTmp.msgType == 'L' || msgTmp.msgType == 'M' || msgTmp.msgType == 'I')))
					{
						if ((buffer[index + 3] == 'O') || (buffer[index + 3] == ' ')) //Original mess, if this case happen we got something
						{
			
						}
						else //This indicate re-transmited mess, ignore it
						{
							isSkip = 1;
						}
					}
					
				}
				//else
				//{
					//Same sequence number, e.g "Line Integrity" so we do nothing here. So we comment this else block
				//}
				if (isSkip == 0)
				{
					workingGroup->primaryLastSequenceNumber = msgTmp.msgSeqNum;
				}
			}
			
			if (isSkip == 0)
			{
				switch (msgTmp.msgCategory)
				{
					case 'A':
					case 'B':
					case 'M':
					case 'L':
						//Do nothing...
						break;
					case 'C':
						if (msgTmp.msgType == 'L')
						{
							//Reset sequence number
							TraceLog(DEBUG_LEVEL, "CQS: Received message sequence number reset of line %d, we will reset sequence number now\n", workingGroup->index + 1);
							workingGroup->primaryLastSequenceNumber = msgTmp.msgSeqNum;
						}
						else if (msgTmp.msgType == 'C')
						{
							// FINRA Close message --> Remove quotes

							// Get participant Id
							int participantId = buffer[index + 17];
							int exchangeIndex = ExchangeIndexList[CQS_SOURCE][participantId - 65];

							TraceLog(DEBUG_LEVEL, "CQS: Received FINRA Close message from line %d, we will close all Market Maker quotes with exchange = %c\n",
									workingGroup->index + 1,
									participantId);

							if ((exchangeIndex >= 0 ) && (exchangeIndex < MAX_EXCHANGE))
							{
								FlushBBOQuotes(exchangeIndex, participantId, CQS_SOURCE);
							}
						}
						else
						{
							// Do nothing ...
							// msgType = 'T' for "Line integrity"
							// msgType = 'I' for "Start of Day"
							// msgType = 'M' for "Start of Test Cycle"
							// msgType = 'N' for "End of Test Cycle"
							// msgType = 'Z' for "End of Transmission"
						}
						
						break;
					case 'E':
						if (msgTmp.msgType == 'B')
						{
							//Equity long quote
							
							//if we have something wrong in the message format then exit
							if ((icount - (index + MAX_CQS_HEADER_LEN)) < 1)
							{
								return SUCCESS;
							}
							
							memcpy(msgTmp.msgHeader, &buffer[index], MAX_CQS_HEADER_LEN);
							memcpy(msgTmp.msgBody, &buffer[index + MAX_CQS_HEADER_LEN], icount - (index + MAX_CQS_HEADER_LEN));
							msgTmp.msgBody[icount - (index + MAX_CQS_HEADER_LEN)] = 0;
							
							ProcessCQS_LongQuoteMsg(&msgTmp);
						}
						else if (msgTmp.msgType == 'D')
						{
							// Equity price short quote
							
							//if we have something wrong in the message format then exit
							if ((icount - (index + MAX_CQS_HEADER_LEN)) < 1)
							{
								return SUCCESS;
							}
							
							memcpy(msgTmp.msgHeader, &buffer[index], MAX_CQS_HEADER_LEN);
							memcpy(msgTmp.msgBody, &buffer[index + MAX_CQS_HEADER_LEN], icount - (index + MAX_CQS_HEADER_LEN));
							msgTmp.msgBody[icount - (index + MAX_CQS_HEADER_LEN)] = 0;

							ProcessCQS_ShortQuoteMsg(&msgTmp);
						}
						else
						{
							TraceLog(ERROR_LEVEL, "CQS: Unknown msgType (%c)\n", msgTmp.msgType);
						}
						
						break;
					default:
						// Other message in other category that is not documented !
						memcpy(buffTemp, &buffer[index], icount - index);
						buffTemp[icount - index] = 0;

						TraceLog(WARN_LEVEL, "CQS unkown message: category = %c, msgType = %c, msg = %s\n", msgTmp.msgCategory, msgTmp.msgType, buffTemp);
				}
			}
		}
		
		//if we get some more message in the current block, we will continue to process it
		if (buffer[icount] == US_CHAR)
		{
			icount++;
		}

		// Increasing index
		index = icount;
	}

	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessCQS_LongQuoteMsg
- Input:			
- Output:											
- Return:			
- Description:						 							  				
- Usage:			
****************************************************************************/
int ProcessCQS_LongQuoteMsg(t_CQS_Msg *msg)
{
	t_BBOQuoteInfo quote;
	
	msg->msgHeader[MAX_CQS_HEADER_LEN] = 0;

	// PARTICIPANT ID
	quote.participantId = msg->msgHeader[17];
	
	// TIME STAMP
	quote.timeStamp = (((msg->msgHeader[18] - 48) - TIMESTAMP_ADJUST_HOUR) * 3600 + (msg->msgHeader[19] - 48) * 60 + (msg->msgHeader[20] - 48)) * 1000 + Lrc_atoi(&msg->msgHeader[21], 3);

	// Parse message body	
	
	// SECURITY SYMBOL: 11 bytes
	char cqsSymbol[12];
	char symbol[SYMBOL_LEN];

	strncpy(cqsSymbol, msg->msgBody, 11);
	cqsSymbol[11] = 0;

	if (ConvertCQSSymbolToComstock(cqsSymbol, symbol) == ERROR)
	{
		return SUCCESS;
	}
	
	// Get stock symbol index
	int symbolIndex = GetStockSymbolIndex(symbol);
	
	if (symbolIndex == -1)
	{
		symbolIndex = UpdateStockSymbolIndex(symbol);
		if (symbolIndex == -1)
		{
			return SUCCESS;
		}
	}
	
	if (IsSymbolIgnored(symbol) == 1)
	{
		return SUCCESS;
	}
	
	//TEMPORARY SUFFIX: 1 Byte
		
	//TEST MESSAGE INDICATOR: 1 Byte
		
	//PRIMARY LISTING MARKET PARTICIPANT IDENTIFIER: 1 Byte
	//char primaryListingMarketParticipantIdentifier = msg->msgBody[13];
	
	//RESERVED: 2 Bytes
		
	//FINANCIAL STATUS: 1 byte
	//int financialStatus = msg->msgBody[16] - 48; //Convert from character to numeric
	
	//CURRENCY INDICATOR: 3 Bytes
		
	//INSTRUMENT TYPE: 1 Byte
		
	//CANCEL/CORRECTION INDICATOR: 1 Byte
	//char cancelCorrectionIndicator = msg->msgBody[21];

	//SETTLEMENT CONDITION: 1 Byte
	//char settlementCondition = msg->msgBody[22];
	
	//MARKET CONDITION: 1 Byte
	//char marketCondition = msg->msgBody[23];

	//QUOTE CONDITION: 1 Byte
	quote.quoteCondition = msg->msgBody[24];

	//RESERVED: 2 Bytes

	//BID PRICE DENOMINATOR INDICATOR: 1 Byte
	int priceIndicator = msg->msgBody[27] - '0';
	if (priceIndicator > 25)
	{
		return SUCCESS;
	}
	
	//BID PRICE: 12 Bytes
	int numberOfByteOfNumerator = arrCQS_NumeratorByte[priceIndicator];
	int whole = Lrc_atoi(&msg->msgBody[28], 12 - numberOfByteOfNumerator);
	int numerator = Lrc_atoi(&msg->msgBody[28 + 12 - numberOfByteOfNumerator], numberOfByteOfNumerator);
	quote.bidPrice = whole + (double) numerator / arrCQS_DenominatorValue[priceIndicator];

	
	//BID SIZE IN UNITS OF TRADE: 7 Bytes
	quote.bidSize = Lrc_atoi(&msg->msgBody[40], 7);

	//OFFER PRICE DENOMINATOR INDICATOR: 1 Byte
	priceIndicator = msg->msgBody[47] - '0';
	if (priceIndicator > 25)
	{
		return SUCCESS;
	}
	
	//OFFER PRICE: 12 Bytes
	numberOfByteOfNumerator = arrCQS_NumeratorByte[priceIndicator];
	whole = Lrc_atoi(&msg->msgBody[48], 12 - numberOfByteOfNumerator);
	numerator = Lrc_atoi(&msg->msgBody[48 + 12 - numberOfByteOfNumerator], numberOfByteOfNumerator);
	quote.offerPrice = whole + (double) numerator / arrCQS_DenominatorValue[priceIndicator];

	//OFFER SIZE IN UNITS OF TRADE: 7 Bytes
	quote.offerSize = Lrc_atoi(&msg->msgBody[60], 7);
	
	//NASD MARKET MAKER ID: 4 Bytes
	/*
	char nasdMarketMakerID[5];
	nasdMarketMakerID[0] = msg->msgBody[67];
	nasdMarketMakerID[1] = msg->msgBody[68];
	nasdMarketMakerID[2] = msg->msgBody[69];
	nasdMarketMakerID[3] = msg->msgBody[70];
	nasdMarketMakerID[4] = 0;
	*/
	//NASD MARKET MAKER GEOGRAPHICAL LOCATION: 2 Bytes

	//NASD MARKET MAKER DESK LOCATION: 1 Byte

	//RESERVED: 2 Bytes

	//NATIONAL BBO INDICATOR: 1 Byte
	int nationalIndicator = msg->msgBody[76] - 48; //Convert from character to numeric

	//FINRA BBO INDICATOR: 1 Byte
	//int finraIndicator = msg->msgBody[77] - 48; //Convert from character to numeric
	
	t_NationalAppendage nationalAppendage;
	
	//Initialize nationalAppendage
	nationalAppendage.timeStamp = 0;
	nationalAppendage.bidSize = -1;
	nationalAppendage.offerSize = -1;
	
	// National BBO Appendage
	switch (nationalIndicator)
	{
		case 0: //no national BBO changes
		case 2: //no national BBO
			break;
		case 1: //quote contains all national BBO information
			nationalAppendage.timeStamp = quote.timeStamp;
			nationalAppendage.bidParticipantId = quote.participantId;
			nationalAppendage.bidPrice = quote.bidPrice;
			nationalAppendage.bidSize = quote.bidSize;
			
			nationalAppendage.offerParticipantId = quote.participantId;
			nationalAppendage.offerPrice = quote.offerPrice;
			nationalAppendage.offerSize = quote.offerSize;

			break;
		case 4: // Long format appendage
			nationalAppendage.timeStamp = quote.timeStamp;
			ProcessCQS_LongNationalBBOAppendage(&msg->msgBody[78], &nationalAppendage);
			break;
		case 6: // Short format appendage
			nationalAppendage.timeStamp = quote.timeStamp;
			ProcessCQS_ShortNationalBBOAppendage(&msg->msgBody[78], &nationalAppendage);
			break;
	}
	
	// FINRA BBO Appendage

	//Add quote to list
	AddQuoteToBBOQuoteMgmt(symbol, &quote, &nationalAppendage, CQS_SOURCE);
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessCQS_ShortQuoteMsg
- Input:			
- Output:											
- Return:			
- Description:						 							  				
- Usage:			
****************************************************************************/
int ProcessCQS_ShortQuoteMsg(t_CQS_Msg *msg)
{
	t_BBOQuoteInfo quote;
	
	msg->msgHeader[MAX_CQS_HEADER_LEN] = 0;

	// PARTICIPANT ID
	quote.participantId = msg->msgHeader[17];
	
	// TIME STAMP
	quote.timeStamp = (((msg->msgHeader[18] - 48) - TIMESTAMP_ADJUST_HOUR) * 3600 + (msg->msgHeader[19] - 48) * 60 + (msg->msgHeader[20] - 48)) * 1000 + Lrc_atoi(&msg->msgHeader[21], 3);

	// Parse message body	
	
	// SECURITY SYMBOL: 3 bytes
	char cqsSymbol[4];
	char symbol[SYMBOL_LEN];
	
	strncpy(cqsSymbol, msg->msgBody, 3);
	cqsSymbol[3] = 0;
	
	if (ConvertCQSSymbolToComstock(cqsSymbol, symbol) == ERROR)
	{
		return SUCCESS;
	}
	
	// Get stock symbol index
	int symbolIndex = GetStockSymbolIndex(symbol);
	
	if (symbolIndex == -1)
	{
		symbolIndex = UpdateStockSymbolIndex(symbol);
		if (symbolIndex == -1)
		{
			return SUCCESS;
		}
	}
	
	if (IsSymbolIgnored(symbol) == 1)
	{
		return SUCCESS;
	}
	
	// QUOTE CONDITIONS: 1 byte
	quote.quoteCondition = msg->msgBody[3];
	// RESERVED: 2 Bytes
	
	//BID PRICE DENOMINATOR INDICATOR: 1 byte
	int priceIndicator = msg->msgBody[6] - '0';
	if (priceIndicator > 25)
	{
		return SUCCESS;
	}
	
	//BID SHORT PRICE: 8 Bytes
	int numberOfByteOfNumerator = arrCQS_NumeratorByte[priceIndicator];
	
	int whole = Lrc_atoi(&msg->msgBody[7], 8 - numberOfByteOfNumerator);
	int numerator = Lrc_atoi(&msg->msgBody[7 + 8 - numberOfByteOfNumerator], numberOfByteOfNumerator);
	quote.bidPrice = whole + (double) numerator / arrCQS_DenominatorValue[priceIndicator];
	
	//BID SIZE IN UNITS OF TRADE: 3 Bytes
	quote.bidSize = Lrc_atoi(&msg->msgBody[15], 3);
	
	//RESERVED: 1 Byte
	
	//OFFER PRICE DENOMINATOR INDICATOR: 1 Byte
	priceIndicator = msg->msgBody[19] - '0';
	if (priceIndicator > 25)
	{
		return SUCCESS;
	}
	
	//OFFER SHORT PRICE: 8 Bytes
	numberOfByteOfNumerator = arrCQS_NumeratorByte[priceIndicator];
	whole = Lrc_atoi(&msg->msgBody[20], 8 - numberOfByteOfNumerator);
	numerator = Lrc_atoi(&msg->msgBody[20 + 8 - numberOfByteOfNumerator], numberOfByteOfNumerator);
	quote.offerPrice = whole + (double) numerator / arrCQS_DenominatorValue[priceIndicator];
	
	//OFFER SIZE IN UNITS OF TRADE: 3 Bytes
	quote.offerSize = Lrc_atoi(&msg->msgBody[28], 3);
	
	//RESERVED: 1 Byte
	
	//NATIONAL BBO INDICATOR: 1 Byte
	int nationalIndicator = msg->msgBody[32] - 48; //Convert from character to numeric

	//FINRA BBO INDICATOR: 1 Byte
	//int finraIndicator = msg->msgBody[33] - 48; //Convert from character to numeric
	
	t_NationalAppendage nationalAppendage;
	
	//Initialize nationalAppendage
	nationalAppendage.timeStamp = 0;
	nationalAppendage.bidSize = -1;
	nationalAppendage.offerSize = -1;
	
	// National BBO Appendage
	switch (nationalIndicator)
	{
		case 0: //no national BBO changes
		case 2: //no national BBO
			break;
		case 1: //quote contains all national BBO information
			nationalAppendage.timeStamp = quote.timeStamp;
			nationalAppendage.bidParticipantId = quote.participantId;
			nationalAppendage.bidPrice = quote.bidPrice;
			nationalAppendage.bidSize = quote.bidSize;
			
			nationalAppendage.offerParticipantId = quote.participantId;
			nationalAppendage.offerPrice = quote.offerPrice;
			nationalAppendage.offerSize = quote.offerSize;
			break;
		case 4: // Long format appendage
			nationalAppendage.timeStamp = quote.timeStamp;
			ProcessCQS_LongNationalBBOAppendage(&msg->msgBody[34], &nationalAppendage);
			break;
		case 6: // Short format appendage
			nationalAppendage.timeStamp = quote.timeStamp;
			ProcessCQS_ShortNationalBBOAppendage(&msg->msgBody[34], &nationalAppendage);
			break;
	}
	
	// FINRA BBO Appendage
	
	// Add quote to list
	AddQuoteToBBOQuoteMgmt(symbol, &quote, &nationalAppendage, CQS_SOURCE);
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessCQS_ShortNationalBBOAppendage
- Input:			
- Output:											
- Return:			
- Description:						 							  				
- Usage:			
****************************************************************************/
void ProcessCQS_ShortNationalBBOAppendage(char *msg, t_NationalAppendage *nationalAppendage)
{
	//BEST BID PARTICIPANT ID: 1 Byte
	nationalAppendage->bidParticipantId = msg[0];
	
	//BEST BID PRICE DENOMINATOR INDICATOR: 1
	int priceIndicator = msg[1] - '0';
	
	//BEST SHORT BID PRICE: 8
	int numberOfByteOfNumerator = arrCQS_NumeratorByte[priceIndicator];
	int whole = Lrc_atoi(&msg[2], 8 - numberOfByteOfNumerator);
	int numerator = Lrc_atoi(&msg[2 + 8 - numberOfByteOfNumerator], numberOfByteOfNumerator);
	nationalAppendage->bidPrice = whole + (double) numerator / arrCQS_DenominatorValue[priceIndicator];

	//BEST BID SIZE IN UNITS OF TRADE: 3
	nationalAppendage->bidSize = Lrc_atoi(&msg[10], 3);
	
	//RESERVED: 1
	//BEST OFFER PARTICIPANT ID: 1
	nationalAppendage->offerParticipantId = msg[14];
	
	//BEST OFFER PRICE DENOMINATOR INDICATOR: 1
	priceIndicator = msg[15] - '0';
	
	//BEST SHORT OFFER PRICE: 8
	numberOfByteOfNumerator = arrCQS_NumeratorByte[priceIndicator];
	whole = Lrc_atoi(&msg[16], 8 - numberOfByteOfNumerator);
	numerator = Lrc_atoi(&msg[16 + 8 - numberOfByteOfNumerator], numberOfByteOfNumerator);
	nationalAppendage->offerPrice = whole + (double) numerator / arrCQS_DenominatorValue[priceIndicator];

	//BEST OFFER SIZE IN UNITS OF TRADE: 3
	nationalAppendage->offerSize = Lrc_atoi(&msg[24],3);
	
	//RESERVED : 1
	return;
}

/****************************************************************************
- Function name:	ProcessCQS_LongNationalBBOAppendage
- Input:			
- Output:											
- Return:			
- Description:						 							  				
- Usage:			
****************************************************************************/
void ProcessCQS_LongNationalBBOAppendage(char *msg, t_NationalAppendage *nationalAppendage)
{
	//RESERVED 2
	//BEST BID PRICE PARTICIPANT ID 1
	nationalAppendage->bidParticipantId = msg[2];
	
	//BEST BID PRICE DENOMINATOR INDICATOR 1
	int priceIndicator = msg[3] - '0';
	
	//BEST BID PRICE 12
	int numberOfByteOfNumerator = arrCQS_NumeratorByte[priceIndicator];
	int whole = Lrc_atoi(&msg[4], 12 - numberOfByteOfNumerator);
	int numerator = Lrc_atoi(&msg[4 + 12 - numberOfByteOfNumerator], numberOfByteOfNumerator);
	nationalAppendage->bidPrice = whole + (double) numerator / arrCQS_DenominatorValue[priceIndicator];

	//BEST BID SIZE IN UNITS OF TRADE 7
	nationalAppendage->bidSize = Lrc_atoi(&msg[16], 7);
	
	//BEST BID FINRA MARKET MAKER ID 4
	//BEST BID FINRA MARKET MAKER GEOGRAPHICAL LOCATION 2
	//BEST BID FINRA MARKET MAKER DESK LOCATION 1
	//BEST OFFER PARTICIPANT ID 1
	nationalAppendage->offerParticipantId = msg[30];
	
	//BEST OFFER PRICE DENOMINATOR INDICATOR 1
	priceIndicator = msg[31] - '0';
	
	//BEST OFFER PRICE 12
	numberOfByteOfNumerator = arrCQS_NumeratorByte[priceIndicator];
	whole = Lrc_atoi(&msg[32], 12 - numberOfByteOfNumerator);
	numerator = Lrc_atoi(&msg[32 + 12 - numberOfByteOfNumerator], numberOfByteOfNumerator);
	nationalAppendage->offerPrice = whole + (double) numerator / arrCQS_DenominatorValue[priceIndicator];

	//BEST OFFER SIZE IN UNITS OF TRADE 7
	nationalAppendage->offerSize = Lrc_atoi(&msg[44], 7);
	
	//BEST OFFER FINRA MARKET MAKER ID 4
	//BEST OFFER FINRA MARKET MAKER GEOGRAPHICAL LOCATION 2
	//BEST OFFER FINRA  MARKET MAKER LOCATION 1
	return;
}

/****************************************************************************
- Function name:	DisconnectCQS_Feed
- Input:			
- Output:											
- Return:			
- Description:						 							  				
- Usage:			
****************************************************************************/
int DisconnectCQS_Feed(void)
{
	CQS_StatusMgmt.shouldConnect = NO;
	CQS_StatusMgmt.isConnected = DISCONNECTED;

	int i;

	for (i = 0; i < MAX_CQS_MULTICAST_LINES; i++)
	{
		CQS_Feed_Conf.group[i].dataInfo.connectionFlag = 0;
		sem_post(&CQS_Feed_Conf.group[i].dataInfo.sem);
	}

	return SUCCESS;
}

/****************************************************************************
- Function name:	ReleaseCQS_FeedResource
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ReleaseCQS_FeedResource(void)
{
	int i, ret, isError = 0;
	pthread_mutex_lock(&DBLMgmt.mutexLock);
	int needToRelease = (IsDBLTimedOut(CQS_Feed_Conf.dev) == 0) ? 1:0;
	
	for (i = 0; i < MAX_CQS_MULTICAST_LINES; i++)
	{
		if (CQS_Feed_Conf.group[i].dataChannel != NULL)
		{
			if (needToRelease == 1)
			{
				if (isError == 0)
				{
					ret = LeaveDBLMulticastGroup(CQS_Feed_Conf.group[i].dataChannel, CQS_Feed_Conf.group[i].dataIP);
					if (ret != EADDRNOTAVAIL)
						dbl_unbind(CQS_Feed_Conf.group[i].dataChannel);
					else
						isError = 1;
				}
			}

			CQS_Feed_Conf.group[i].dataChannel = NULL;
		}

		sem_destroy(&CQS_Feed_Conf.group[i].dataInfo.sem);
	}

	if (CQS_Feed_Conf.dev != NULL)
	{
		CQS_Feed_Conf.dev = NULL;
	}
	
	pthread_mutex_unlock(&DBLMgmt.mutexLock);

	return SUCCESS;
}
