/*****************************************************************************
**	Project:		Equity Arbitrage Application
**	Filename:		order_mgmt_proc.c
**	Description:	This file contains function definitions that were declared
					in order_mgmt_proc.h	
**	Author:			Luan Vo-Kinh
**	First created:	13-Sep-2007
**	Last updated:	-----------
*****************************************************************************/

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#define _GNU_SOURCE
#include <unistd.h>
#include <math.h>

#include "configuration.h"
#include "nasdaq_rash_proc.h"
#include "arca_direct_proc.h"
#include "trading_mgmt.h"
#include "order_mgmt_proc.h"
#include "aggregation_server_proc.h"
#include "raw_data_mgmt.h"
#include "hbitime.h"

#define PRE_TRADING_SESSION 0
#define IN_TRADING_SESSION 1
#define POST_TRADING_SESSION 2

/****************************************************************************
** Global variables definition
****************************************************************************/
t_OrderStatusIndex orderStatusIndex;

t_ASOpenOrderMgmt asOpenOrder;

t_TradeSymbol tradeSymbolList;

short symboIndexlList[MAX_TYPE_CHAR][MAX_TYPE_CHAR][MAX_TYPE_CHAR][MAX_TYPE_CHAR][MAX_TYPE_CHAR];
short longSymbolIndexList[MAX_SYMBOL_TRADE][MAX_TYPE_CHAR][MAX_TYPE_CHAR];
short extraLongSymbolIndexList[MAX_SYMBOL_TRADE][MAX_TYPE_CHAR];
int orderIdIndexList[MAX_ORDER_ID_RANGE];

char accountMapping[MAX_ORDER_PLACEMENT];

pthread_mutex_t symbolIndexMutex;
extern t_OrderInfo LastNasdaqOrder[MAX_STOCK_SYMBOL];
extern int StockSymbolIndex_to_ArcaSymbolIndex_Mapping[MAX_STOCK_SYMBOL];
t_LiquidateOrderCollection LiquidateOrderCollection;
t_BBOOrderSleepingCollection BBOOrderSleepingCollection;

pthread_mutex_t CancelRequestMutex[MAX_ORDER_PLACEMENT];

t_AutoCancelOrder AutoCancelOrder;
/****************************************************************************
** Function declarations
****************************************************************************/


/****************************************************************************
- Function name:	AddToLiquidateCollection
- Input:		index of order summary from open order collection	
- Output:		N/A	
- Return:		SUCCESS or ERROR	
- Description:		Add order summary index into collection
- Usage:		When looping through open order collection to filter
			out Liquidate order, we do not need to loop though the
			large open order collection, just loop through this collection
			So it makes thing faster!	
****************************************************************************/
int AddToLiquidateCollection(const int orderSummaryIndex)
{
	int i;
	for (i = 0; i < MAX_LIQUIDATE_ITEM; i++)
	{
		if (LiquidateOrderCollection.collection[i] == -1)
		{
			LiquidateOrderCollection.collection[i] = orderSummaryIndex;
			
			LiquidateOrderCollection.count++;
			return SUCCESS;
		}
		else if (LiquidateOrderCollection.collection[i] == orderSummaryIndex)
		{
			return SUCCESS;	//No need to add, it's already in the collection!
		}
	}
	return ERROR;
}

/****************************************************************************
- Function name:	AddToBBOOrderSleepingCollection
- Input:		index of open order
- Output:		N/A	
- Return:		SUCCESS or ERROR	
- Description:
****************************************************************************/
int AddToBBOOrderSleepingCollection(const int indexOpenOrder)
{
	int i, j;
	
	//Check if the item was in the collection, we do not need to add it 
	for (i = 0; i < MAX_BBO_ORDER_SLEEPING_ITEM; i++)
	{
		if (BBOOrderSleepingCollection.collection[i] == indexOpenOrder)
		{
			return SUCCESS;
		}
	}
	
	//Find to add to collection
	for (j = 0; j < MAX_BBO_ORDER_SLEEPING_ITEM; j++)
	{
		if (BBOOrderSleepingCollection.collection[j] == -1)
		{
			if ((BBOOrderSleepingCollection.count + 1) == MAX_BBO_ORDER_SLEEPING_ITEM)
			{
				return ERROR;
			}
	
			BBOOrderSleepingCollection.collection[j] = indexOpenOrder;
			BBOOrderSleepingCollection.count++;
		
			//Check if thread exited or is not created, we will create a new thread for BBO order sleeping
			if (BBOOrderSleepingCollection.threadIsCreated == NO)
			{
				pthread_t monitorBBOOrderSleeping_id;
				if (pthread_create(&monitorBBOOrderSleeping_id, NULL, (void*)&Monitoring_BBO_Order_Sleeping_Thread, NULL) != 0)
				{
					TraceLog(ERROR_LEVEL, "Couldn't create thread for Monitoring BBO Order Sleeping\n");
					return SUCCESS;
				}
				
				BBOOrderSleepingCollection.threadIsCreated = YES;
			}
	
			return SUCCESS;
		}	
	}
	
	return ERROR;
}

/****************************************************************************
- Function name:	InitializeASOpenOrderStructures
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int InitializeASOpenOrderStructures(void)
{
	memset(&asOpenOrder, 0, sizeof(t_ASOpenOrderMgmt));
	int i, j;
	for (i = 0; i < MAX_LIQUIDATE_ITEM; i++)
	{
		LiquidateOrderCollection.collection[i] = -1;
	}
	LiquidateOrderCollection.count = 0;
	
	for (j = 0; j < MAX_BBO_ORDER_SLEEPING_ITEM; j++)
	{
		BBOOrderSleepingCollection.collection[j] = -1;
	}
	BBOOrderSleepingCollection.count = 0;
	BBOOrderSleepingCollection.threadIsCreated = NO;
	
	for(i = 0; i < MAX_ORDER_PLACEMENT; i++)
	{
		asOpenOrder.orderSummary[i].cancelRequest = 0;
		pthread_mutex_init(&CancelRequestMutex[i], NULL);
	}
	
	memset(&AutoCancelOrder, 0, sizeof(t_AutoCancelOrder));
	
	return 0;
}

/****************************************************************************
- Function name:	InitializeOrderStatusIndex
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int InitializeOrderStatusIndex(void)
{
	orderStatusIndex.Sent = 1;
	orderStatusIndex.Live = 2;
	orderStatusIndex.Rejected = 3;
	orderStatusIndex.Closed = 4;
	orderStatusIndex.BrokenTrade = 5;
	orderStatusIndex.CanceledByECN = 6;
	orderStatusIndex.CXLSent = 7;
	orderStatusIndex.CancelACK = 8;
	orderStatusIndex.CancelRejected = 9;
	orderStatusIndex.CancelPending = 10;
	orderStatusIndex.CanceledByUser = 11;
	orderStatusIndex.Sleeping = 12;
	return 0;
}

/****************************************************************************
- Function name:	InitializeTradeSymbolStructure
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int InitializeTradeSymbolStructure(void)
{
	pthread_mutex_init(&symbolIndexMutex, NULL);

	memset(&tradeSymbolList, 0, sizeof(t_TradeSymbol));

	int i;
	for (i = 0; i < MAX_SYMBOL_TRADE; i++)
	{
		tradeSymbolList.symbolInfo[i].haltStatusSent = YES;
		tradeSymbolList.symbolInfo[i].isAutoResume = 1;
	}
	
	return 0;
}

/****************************************************************************
- Function name:	InitializeSymbolIndexList
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int InitializeSymbolIndexList(void)
{
	int i, j, k, n, m;

	for (i = 0; i < MAX_TYPE_CHAR; i++)
	{
		for (j = 0; j < MAX_TYPE_CHAR; j++)
		{
			for (k = 0; k < MAX_TYPE_CHAR; k++)
			{
				for (m = 0; m < MAX_TYPE_CHAR; m++)
				{
					for (n = 0; n < MAX_TYPE_CHAR; n++)
					{
						//Index of each stock symbol now will be -1, meaning 
						//'not a valid index yet'
						symboIndexlList[i][j][k][m][n] = -1;
					}
				}
			}
		}
	}
	
	for (i = 0; i < MAX_SYMBOL_TRADE; i++)
	{
		for (j = 0; j < MAX_TYPE_CHAR; j++)
		{
			for (k = 0; k < MAX_TYPE_CHAR; k++)
			{
				longSymbolIndexList[i][j][k] = -1;
			}
		}
	}
	
	for (i = 0; i < MAX_SYMBOL_TRADE; i++)
	{
		for (j = 0; j < MAX_TYPE_CHAR; j++)
		{
			extraLongSymbolIndexList[i][j] = -1;
		}
	}

	return 0;
}

/****************************************************************************
- Function name:	InitilizeOrderIdIndex
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int InitilizeOrderIdIndexList(void)
{
	int i;

	for (i = 0; i < MAX_ORDER_ID_RANGE; i++)
	{
		orderIdIndexList[i] = -1;
	}
	
	// Initial account suffix list for AS Orders
	for (i = 0; i < MAX_ORDER_PLACEMENT; i++)
	{
		accountMapping[i] = -1;
	}

	return SUCCESS;
}

/****************************************************************************
- Function name:	GetOrderIdIndex
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int GetOrderIdIndex(int clOrdId)
{
	//asOpenOrder.countOpenOrder
	if (orderIdIndexList[clOrdId % MAX_ORDER_ID_RANGE] == -1)
	{
		orderIdIndexList[clOrdId % MAX_ORDER_ID_RANGE] = asOpenOrder.countOpenOrder;
	}

	return orderIdIndexList[clOrdId % MAX_ORDER_ID_RANGE];
}

/****************************************************************************
- Function name:	GetTradeSymbolIndex
- Input:			+ stockSymbol
- Output:		N/A
- Return:		ERROR or stock symbol index
- Description:	+ The routine hides the following facts
				  - Check valid stock symbol
				  - Return stockSymbolIndex for stockSymbol
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A 
****************************************************************************/
int GetTradeSymbolIndex(const char *stockSymbol)
{
	int i, j, k, n, m, t, u, v;
	int indexOfNewSymbol = -1;
	
	//character 1: Only in the range A-Z
	if ((stockSymbol[0] > 64) && (stockSymbol[0] < 91))
	{
		i = stockSymbol[0] - INDEX_OF_A_CHAR;	
	}
	else
	{
		//Invalid stock symbol
		return -1;
	}

	//character 2: 0, $, ., A-Z
	if ((stockSymbol[1] > 64) && (stockSymbol[1] < 91))// A-Z
	{
		j = stockSymbol[1] - INDEX_OF_A_CHAR;
	}
	else if ((stockSymbol[1] == 32) || (stockSymbol[1] == 0))
	{
		indexOfNewSymbol = symboIndexlList[i][0][0][0][0];
		if (indexOfNewSymbol == -1)
		{
			pthread_mutex_lock(&symbolIndexMutex);
			indexOfNewSymbol = tradeSymbolList.countSymbol++;
			symboIndexlList[i][0][0][0][0] = indexOfNewSymbol;
			
			memcpy(tradeSymbolList.symbolInfo[indexOfNewSymbol].symbol, stockSymbol, 1);
			pthread_mutex_unlock(&symbolIndexMutex);
		}
		return indexOfNewSymbol;
	}
	else if (stockSymbol[1] == '-')
	{
		j = 3;
	}
	else if (stockSymbol[1] == '.')
	{
		j = 4;
	}
	else if (stockSymbol[1] == '$')
	{
		j = 1;
	}
	else if (stockSymbol[1] == '+')
	{
		j = 2;
	}
	else
	{
		//Invalid stock symbol
		return -1;
	}

	//character 3: 0, $, ., A-Z
	if ((stockSymbol[2] > 64) && (stockSymbol[2] < 91))// A-Z
	{
		k = stockSymbol[2] - INDEX_OF_A_CHAR;
	}
	else if ((stockSymbol[2] == 32) || (stockSymbol[2] == 0))
	{
		indexOfNewSymbol = symboIndexlList[i][j][0][0][0];
		if (indexOfNewSymbol == -1)
		{
			pthread_mutex_lock(&symbolIndexMutex);
			indexOfNewSymbol = tradeSymbolList.countSymbol++;
			symboIndexlList[i][j][0][0][0] = indexOfNewSymbol;
			memcpy(tradeSymbolList.symbolInfo[indexOfNewSymbol].symbol, stockSymbol, 2);
			pthread_mutex_unlock(&symbolIndexMutex);
		}
		return indexOfNewSymbol;
	}
	else if (stockSymbol[2] == '-')
	{
		k = 3;
	}
	else if (stockSymbol[2] == '.')
	{
		k = 4;
	}
	else if (stockSymbol[2] == '$')
	{
		k = 1;
	}
	else if (stockSymbol[2] == '+')
	{
		k = 2;
	}
	else
	{
		//Invalid stock symbol
		return -1;
	}

	//character 4: 0, $, ., A-Z
	if ((stockSymbol[3] > 64) && (stockSymbol[3] < 91))// A-Z
	{
		n = stockSymbol[3] - INDEX_OF_A_CHAR;		
	}
	else if ((stockSymbol[3] == 32) || (stockSymbol[3] == 0))
	{
		indexOfNewSymbol = symboIndexlList[i][j][k][0][0];
		if (indexOfNewSymbol == -1)
		{
			pthread_mutex_lock(&symbolIndexMutex);
			indexOfNewSymbol = tradeSymbolList.countSymbol++;
			symboIndexlList[i][j][k][0][0] = indexOfNewSymbol;
			memcpy(tradeSymbolList.symbolInfo[indexOfNewSymbol].symbol, stockSymbol, 3);
			pthread_mutex_unlock(&symbolIndexMutex);
		}
		return indexOfNewSymbol;
	}
	else if (stockSymbol[3] == '-')
	{
		n = 3;
	}
	else if (stockSymbol[3] == '.')
	{
		n = 4;
	}
	else if (stockSymbol[3] == '$')
	{
		n = 1;
	}
	else if (stockSymbol[3] == '+')
	{
		n = 2;
	}
	else
	{
		//Invalid stock symbol
		return -1;
	}

	//character 5: 0, $, ., A-Z
	//A-Z
	if ((stockSymbol[4] > 64) && (stockSymbol[4] < 91))
	{
		m = stockSymbol[4] - INDEX_OF_A_CHAR;
	}
	else if ((stockSymbol[4] == 32) || (stockSymbol[4] == 0))
	{
		indexOfNewSymbol = symboIndexlList[i][j][k][n][0];
		if (indexOfNewSymbol == -1)
		{
			pthread_mutex_lock(&symbolIndexMutex);
			indexOfNewSymbol = tradeSymbolList.countSymbol++;
			symboIndexlList[i][j][k][n][0] = indexOfNewSymbol;
			memcpy(tradeSymbolList.symbolInfo[indexOfNewSymbol].symbol, stockSymbol, 4);
			pthread_mutex_unlock(&symbolIndexMutex);
		}
		return indexOfNewSymbol;
	}
	else if (stockSymbol[4] == '-')
	{
		m = 3;
	}
	else if (stockSymbol[4] == '.')
	{
		m = 4;
	}
	else if (stockSymbol[4] == '$')
	{
		m = 1;
	}
	else if (stockSymbol[4] == '+')
	{
		m = 2;
	}
	else
	{
		//Invalid stock symbol
		return -1;
	}
	
	//character 6 0, $, ., A-Z
	if ((stockSymbol[5] > 64) && (stockSymbol[5] < 91))// A-Z
	{
		t = stockSymbol[5] - INDEX_OF_A_CHAR;		
	}
	else if ((stockSymbol[5] == 32) || (stockSymbol[5] == 0))
	{
		indexOfNewSymbol = symboIndexlList[i][j][k][n][m];
		if (indexOfNewSymbol == -1)
		{
			pthread_mutex_lock(&symbolIndexMutex);
			indexOfNewSymbol = tradeSymbolList.countSymbol++;
			symboIndexlList[i][j][k][n][m] = indexOfNewSymbol;
			memcpy(tradeSymbolList.symbolInfo[indexOfNewSymbol].symbol, stockSymbol, 5);
			pthread_mutex_unlock(&symbolIndexMutex);
		}
		return indexOfNewSymbol;
	}
	else if (stockSymbol[5] == '-')
	{
		t = 3;
	}
	else if (stockSymbol[5] == '.')
	{
		t = 4;
	}
	else if (stockSymbol[5] == '$')
	{
		t = 1;
	}
	else if (stockSymbol[5] == '+')
	{
		t = 2;
	}
	else
	{
		//Invalid stock symbol
		return -1;
	}
	
	//character 7 0, $, ., A-Z
	if ((stockSymbol[6] > 64) && (stockSymbol[6] < 91))// A-Z
	{
		u = stockSymbol[6] - INDEX_OF_A_CHAR;		
	}
	else if ((stockSymbol[6] == 32) || (stockSymbol[6] == 0))
	{
		pthread_mutex_lock(&symbolIndexMutex);
		if(symboIndexlList[i][j][k][n][m] == -1)
		{
			// Insert symbol that included only 5 (in 6 characters).
			indexOfNewSymbol = tradeSymbolList.countSymbol++;	
			symboIndexlList[i][j][k][n][m] = indexOfNewSymbol;				
			memcpy(tradeSymbolList.symbolInfo[indexOfNewSymbol].symbol, stockSymbol, 5);
			
			// Get symbol index in new map table
			longSymbolIndexList[indexOfNewSymbol][t][0] = tradeSymbolList.countSymbol;
			indexOfNewSymbol = tradeSymbolList.countSymbol++;
			memcpy(tradeSymbolList.symbolInfo[indexOfNewSymbol].symbol, stockSymbol, 6);
		}
		else
		{
			indexOfNewSymbol = symboIndexlList[i][j][k][n][m];
			
			if (longSymbolIndexList[indexOfNewSymbol][t][0] == -1)
			{
				longSymbolIndexList[indexOfNewSymbol][t][0] = tradeSymbolList.countSymbol;
				indexOfNewSymbol = tradeSymbolList.countSymbol++;
				memcpy(tradeSymbolList.symbolInfo[indexOfNewSymbol].symbol, stockSymbol, 6);
			}
			else
			{
				indexOfNewSymbol = longSymbolIndexList[indexOfNewSymbol][t][0];
			}
			
		}
		pthread_mutex_unlock(&symbolIndexMutex);
		
		// Successfully inserted a stock symbol	
		return indexOfNewSymbol;
	}
	else if (stockSymbol[6] == '-')
	{
		u = 3;
	}
	else if (stockSymbol[6] == '.')
	{
		u = 4;
	}
	else if (stockSymbol[6] == '$')
	{
		u = 1;
	}
	else if (stockSymbol[6] == '+')
	{
		u = 2;
	}
	else
	{
		//Invalid stock symbol
		return -1;
	}
	
	//character 8 0, $, ., A-Z
	if ((stockSymbol[7] > 64) && (stockSymbol[7] < 91))// A-Z
	{
		v = stockSymbol[7] - INDEX_OF_A_CHAR;		
	}
	else if ((stockSymbol[7] == 32) || (stockSymbol[7] == 0))
	{
		pthread_mutex_lock(&symbolIndexMutex);
		if(symboIndexlList[i][j][k][n][m] == -1)
		{
			// Insert symbol that included only 5 (in 6 characters).
			indexOfNewSymbol = tradeSymbolList.countSymbol++;	
			symboIndexlList[i][j][k][n][m] = indexOfNewSymbol;				
			memcpy(tradeSymbolList.symbolInfo[indexOfNewSymbol].symbol, stockSymbol, 5);
			
			// Get symbol index in new map table				
			longSymbolIndexList[indexOfNewSymbol][t][u] = tradeSymbolList.countSymbol;
			indexOfNewSymbol = tradeSymbolList.countSymbol++;
			memcpy(tradeSymbolList.symbolInfo[indexOfNewSymbol].symbol, stockSymbol, 7);
		}
		else
		{
			indexOfNewSymbol = symboIndexlList[i][j][k][n][m];
			
			if (longSymbolIndexList[indexOfNewSymbol][t][u] == -1)
			{
				longSymbolIndexList[indexOfNewSymbol][t][u] = tradeSymbolList.countSymbol;
				indexOfNewSymbol = tradeSymbolList.countSymbol++;
				memcpy(tradeSymbolList.symbolInfo[indexOfNewSymbol].symbol, stockSymbol, 7);
			}
			else
			{
				indexOfNewSymbol = longSymbolIndexList[indexOfNewSymbol][t][u];
			}
			
		}
		pthread_mutex_unlock(&symbolIndexMutex);
		// Successfully inserted a stock symbol	
		return indexOfNewSymbol;
	}
	else if (stockSymbol[7] == '-')
	{
		v = 3;
	}
	else if (stockSymbol[7] == '.')
	{
		v = 4;
	}
	else if (stockSymbol[7] == '$')
	{
		v = 1;
	}
	else if (stockSymbol[7] == '+')
	{
		v = 2;
	}
	else
	{
		//Invalid stock symbol
		return -1;
	}
	
	//This is where we update the symbols with 8 characters long
	pthread_mutex_lock(&symbolIndexMutex);
	if(symboIndexlList[i][j][k][n][m] == -1)
	{
		// Insert symbol that included only 5
		indexOfNewSymbol = tradeSymbolList.countSymbol++;	
		symboIndexlList[i][j][k][n][m] = indexOfNewSymbol;				
		memcpy(tradeSymbolList.symbolInfo[indexOfNewSymbol].symbol, stockSymbol, 5);
		
		// Update index of symbol with 7 characters.
		// We do not get index of symbol with 7 characters since we know that it's not available for sure
		// --> since symbol with 5 chars does not exist, so how can symbol with 7 chars exist? ;)
		
		longSymbolIndexList[indexOfNewSymbol][t][u] = tradeSymbolList.countSymbol;
		indexOfNewSymbol = tradeSymbolList.countSymbol++;
		memcpy(tradeSymbolList.symbolInfo[indexOfNewSymbol].symbol, stockSymbol, 7);
		
		// Index of symbol in new map table (where the 8 chars long symbol comes!)
		extraLongSymbolIndexList[indexOfNewSymbol][v] = tradeSymbolList.countSymbol;
		indexOfNewSymbol = tradeSymbolList.countSymbol++;
		memcpy(tradeSymbolList.symbolInfo[indexOfNewSymbol].symbol, stockSymbol, 8);
	}
	else
	{
		//This is index of symbol with 5 chars
		indexOfNewSymbol = symboIndexlList[i][j][k][n][m];
		
		// This is index of symbol with 7 chars
		if (longSymbolIndexList[indexOfNewSymbol][t][u] == -1)
		{
			longSymbolIndexList[indexOfNewSymbol][t][u] = tradeSymbolList.countSymbol;
			indexOfNewSymbol = tradeSymbolList.countSymbol++;
			memcpy(tradeSymbolList.symbolInfo[indexOfNewSymbol].symbol, stockSymbol, 7);
		}
		else
		{
			indexOfNewSymbol = longSymbolIndexList[indexOfNewSymbol][t][u];
		}
		
		// This is index of symbol with 8 characters
		if (extraLongSymbolIndexList[indexOfNewSymbol][v] == -1)
		{
			extraLongSymbolIndexList[indexOfNewSymbol][v] = tradeSymbolList.countSymbol;
			indexOfNewSymbol = tradeSymbolList.countSymbol++;
			memcpy(tradeSymbolList.symbolInfo[indexOfNewSymbol].symbol, stockSymbol, 8);
		}
		else
		{
			indexOfNewSymbol = extraLongSymbolIndexList[indexOfNewSymbol][v];
		}
	}
	pthread_mutex_unlock(&symbolIndexMutex);
	
	// Successfully inserted a stock symbol	
	return indexOfNewSymbol;
}

/****************************************************************************
- Function name:	ProcessPMConnectECNOrder
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessPMConnectECNOrder(int ECNIndex)
{	
	switch (ECNIndex)
	{
		case NASDAQ_RASH_INDEX:
			if (orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == DISCONNECTED &&
				orderStatusMgmt[NASDAQ_RASH_INDEX].shouldConnect != YES)
			{
				orderStatusMgmt[NASDAQ_RASH_INDEX].shouldConnect = YES;
				
				pthread_t nasdaq_rash_Thread;
				pthread_create(&nasdaq_rash_Thread, NULL, (void *)&StartUp_NASDAQ_RASH_Module, NULL);
			}
			break;
		case ARCA_DIRECT_INDEX:
			if (orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == DISCONNECTED &&
				orderStatusMgmt[ARCA_DIRECT_INDEX].shouldConnect != YES)
			{
				orderStatusMgmt[ARCA_DIRECT_INDEX].shouldConnect = YES;
				
				pthread_t arca_direct_Thread;
				pthread_create(&arca_direct_Thread, NULL, (void *)&StartUp_ARCA_DIRECT_Module, NULL);
			}
			break;
		default:
			TraceLog(ERROR_LEVEL, "received request to connect to ECN Order with unknown ID (%d)\n", ECNIndex);
			break;
	}
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessPMDisconnectECNOrder
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessPMDisconnectECNOrder(int ECNIndex)
{
	switch (ECNIndex)
	{
		case NASDAQ_RASH_INDEX:
			if (orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == CONNECTED)
			{
				orderStatusMgmt[NASDAQ_RASH_INDEX].shouldConnect = NO;	
			}
			else
			{
				asCollection.orderConnection[NASDAQ_RASH_INDEX].status = DISCONNECTED;
			}
			break;
		case ARCA_DIRECT_INDEX:
			
			orderStatusMgmt[ARCA_DIRECT_INDEX].shouldConnect = NO;
			shutdown(ARCA_DIRECT_Config.socket, SHUT_RDWR);
			close(ARCA_DIRECT_Config.socket);
			ARCA_DIRECT_Config.socket = -1;
			break;
		default:
			break;
	}

	return SUCCESS;
}

/****************************************************************************
- Function name:	SendNewOrderToNASDAQ_RASHOrder
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int SendNewOrderToNASDAQ_RASHOrder(t_SendNewOrder *tmpOrder)
{
	t_NASDAQ_RASH_NewOrder newOrder;
	
	//orderToken
	char orderToken[16];
	/*
	code added for safety
	*/
	memset(orderToken, 0, 16);
	sprintf(orderToken, "%d", tmpOrder->orderId);

	int len = strlen(orderToken);

	memset(&orderToken[len], ' ', 14 - len);
	orderToken[14] = 0;

	newOrder.orderToken = orderToken;
	
	//side
	if (tmpOrder->side == BUY_TO_CLOSE_TYPE)
	{
		newOrder.side = 'B';
		newOrder.buyLaunch = BUY_TO_CLOSE;
	}
	else if (tmpOrder->side == SELL_TYPE)
	{
		newOrder.side = 'S';
	}
	else if (tmpOrder->side == SHORT_SELL_TYPE)
	{
		newOrder.side = 'S';
	}
	else 
	{
		newOrder.side = 'B';
		newOrder.buyLaunch = BUY_TO_OPEN;
	}
	
	//share
	newOrder.share = tmpOrder->shares;
	
	//stock
	char symbol[SYMBOL_LEN];
	strncpy(symbol, tmpOrder->symbol, SYMBOL_LEN);

	newOrder.stock = symbol;
	
	//price
	newOrder.price = tmpOrder->price;

	// pegDef
	newOrder.pegDef = tmpOrder->pegDef;
	
	//timeInForce
	if (tmpOrder->tif == DAY_TYPE)
	{
		newOrder.timeInForce = 99999;
	}
	else
	{
		newOrder.timeInForce = 0;
	}

	//firm
	newOrder.firm = MPID_NASDAQ_RASH;

	//display
	if (tmpOrder->maxFloor == 0)
	{
		newOrder.display = 'N';
	}
	else
	{
		newOrder.display = 'Y';
	}

	/*
	Max Floor
	...
	*/
	newOrder.maxFloor = tmpOrder->maxFloor;

	TraceLog(DEBUG_LEVEL, "Begin build and send NASDAQ RASH order...\n");
	
	int retval = BuildAndSend_NASDAQ_RASH_OrderMsg(&newOrder, tmpOrder);
	
	if (retval == SUCCESS)
	{
		TraceLog(DEBUG_LEVEL, "Send Order to NASDAQ RASH Server successfully\n");
	}
	else
	{
		TraceLog(ERROR_LEVEL, "Failed to send Order to NASDAQ RASH Server\n");

		return ERROR;
	}
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	SendManualOrder
- Input:			Order info
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int SendManualOrder(t_ManualOrder *order)
{
	if (order->ECNId == TYPE_ARCA_DIRECT)
	{
		if (orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == DISCONNECTED)
		{
			return ERROR;
		}
	}
	else if(order->ECNId == TYPE_NASDAQ_RASH)
	{
		if (orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == DISCONNECTED)
		{
			return ERROR;
		}
	}
	
  int index = order->orderId % MAX_MANUAL_ORDER;
  memset(&AutoCancelOrder.orderInfo[index], 0, sizeof(t_CancelOrderInfo));
  
  AutoCancelOrder.orderInfo[index].ttl = order->tif;
  AutoCancelOrder.orderInfo[index].ecnType = order->ECNId;
  memcpy(AutoCancelOrder.orderInfo[index].symbol, order->symbol, SYMBOL_LEN);
  AutoCancelOrder.orderInfo[index].clOrderId = order->orderId;
  AutoCancelOrder.orderInfo[index].leftShares = order->shares;
  AutoCancelOrder.orderInfo[index].side = order->side;
    
	if(order->tif > 1 && order->tif < 86400)
	{
		if(AutoCancelOrder.threadStarted == 0)
		{
			AutoCancelOrder.threadStarted = 1;
			pthread_t cancelOrderThread;
			pthread_create(&cancelOrderThread, NULL, (void *)&StartSendCancelOrderThread, NULL);
		}
	}
	
	if (order->ECNId == TYPE_ARCA_DIRECT)
	{
		int ret = ERROR;
		TraceLog(DEBUG_LEVEL, "Send manual ARCA DIRECT order...\n");
		
		if(order->maxFloor >= 100 && (order->maxFloor % 100 == 0) )
		{
			ret = BuildAndSend_ARCA_DIRECT_ManualOrder_V3(order);
		}
		else
		{
			ret = BuildAndSend_ARCA_DIRECT_ManualOrder_V1(order);
		}
		
		if (ret == ERROR)
		{
			TraceLog(ERROR_LEVEL, "Failed to send manual ARCA DIRECT order\n");
			return ERROR;
		}
		else
		{
			TraceLog(DEBUG_LEVEL, "Sent manual ARCA DIRECT order successfully\n");
		}
	}
	else if(order->ECNId == TYPE_NASDAQ_RASH)
	{
		TraceLog(DEBUG_LEVEL, "Send manual RASH order...\n");
		if (BuildAndSendRASHManualOrder(order) == ERROR)
		{
			TraceLog(ERROR_LEVEL, "Failed to send manual RASH order\n");
			return ERROR;
		}
		else
		{
			TraceLog(DEBUG_LEVEL, "Sent manual RASH order successfully\n");
		}
	}
	else
	{
		TraceLog(ERROR_LEVEL, "Invalid manual order ECN: %d\n", order->ECNId);
		return ERROR;
	}
	
	return SUCCESS;
}

void* StartSendCancelOrderThread(void *arg)
{
	TraceLog(DEBUG_LEVEL, "Start Thread to send cancel-order\n");
	int i;

	while(1)
	{
		for(i = 0; i < MAX_MANUAL_ORDER; i++)
		{
			SendManualCancelOrder(i, 0);
		}
		
		sleep(1);
	}
	
	AutoCancelOrder.threadStarted = 0;
	
	TraceLog(DEBUG_LEVEL, "Return thread to send cancel-order.\n");
	
	return NULL;
}

int SendManualCancelOrder(int index, int force)
{
  if(AutoCancelOrder.orderInfo[index].marked == 1 && AutoCancelOrder.orderInfo[index].leftShares > 0)
  {
    int ecn = ARCA_DIRECT_INDEX;
    if(AutoCancelOrder.orderInfo[index].ecnType == TYPE_NASDAQ_RASH)
    {
      ecn = NASDAQ_RASH_INDEX;
    }
    
    if(orderStatusMgmt[ecn].isOrderConnected == DISCONNECTED)
    {
       return SUCCESS;
    }
    
    AutoCancelOrder.orderInfo[index].ttl--;
    if(AutoCancelOrder.orderInfo[index].ttl == 0 || force == 1)
    {
      t_ManualCancelOrder cancelOrder;
      memset(&cancelOrder, 0, sizeof(t_ManualCancelOrder));
      
      AutoCancelOrder.orderInfo[index].marked = 0;
      
      // for ARCA DIRECT
      cancelOrder.origClOrdID = AutoCancelOrder.orderInfo[index].clOrderId;
      cancelOrder.ecnOrderID = AutoCancelOrder.orderInfo[index].ecnOrderID;
      memcpy(cancelOrder.symbol, AutoCancelOrder.orderInfo[index].symbol, SYMBOL_LEN);
      cancelOrder.side = AutoCancelOrder.orderInfo[index].side;
      
      // for RASH
      Lrc_itoa(AutoCancelOrder.orderInfo[index].clOrderId, cancelOrder.orderToken);
      
      ProcessSendManualCancelOrder(&cancelOrder, AutoCancelOrder.orderInfo[index].ecnType);
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:	SendNewOrderToARCA_DIRECTOrder
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int SendNewOrderToARCA_DIRECTOrder(t_SendNewOrder *tmpOrder)
{
/*
	int maxFloor;
*/
	t_ARCA_DIRECT_NewOrder newOrder;

	newOrder.clOrdID = tmpOrder->orderId;

	
	/*
	Side values:
	Buy=�1�
	Sell=�2�
	Sell Short=�5� (client affirms ability to borrow)
	*/
	if (tmpOrder->side == BUY_TO_CLOSE_TYPE)
	{
		newOrder.side = '1';
		newOrder.buyLaunch = BUY_TO_CLOSE;
	}
	else if (tmpOrder->side == SELL_TYPE)
	{
		newOrder.side = '2';
	}
	else if (tmpOrder->side == SHORT_SELL_TYPE)
	{
		newOrder.side = '5';
	}
	else 
	{
		newOrder.side = '1';
		newOrder.buyLaunch = BUY_TO_OPEN;
	}
	
	// Share Volume
	newOrder.orderQty = tmpOrder->shares;
	
	// Stock Symbol
	newOrder.symbol = tmpOrder->symbol;

	
	// Price
	newOrder.price = tmpOrder->price;
	
	/*
	Time In Force values
	�0� = DAY (expires at end of day)
	�1� = GTC (allowed, but treated same as Day)
	�3� = IOC (portion not filled immediately is cancelled. Market orders are implicitly IOC)
	�6� = GTD (expires at earlier of specified ExpireTime or end of day)
	*/
	if (tmpOrder->tif == DAY_TYPE)
	{
		newOrder.timeInForce = '0';
	}
	else // IOC
	{
		newOrder.timeInForce = '3';
	}
	
	/*
	Max Floor
	...
	*/
	newOrder.maxFloor = tmpOrder->maxFloor;
	
	/*
	Build and send ARCA DIRECT Order server
	*/
	TraceLog(DEBUG_LEVEL, "Begin build and send ARCA DIRECT order...\n");
	
	int retval = ERROR;
	
	if(newOrder.maxFloor >= 100 && (newOrder.maxFloor % 100 == 0) )
	{
		retval = BuildAndSend_ARCA_DIRECT_OrderMsg_V3(&newOrder, tmpOrder);
	}
	else
	{
		retval = BuildAndSend_ARCA_DIRECT_OrderMsg_V1(&newOrder, tmpOrder);
	}
	
	if (retval == SUCCESS)
	{
		TraceLog(DEBUG_LEVEL, "Send Order to ARCA DIRECT Server successfully\n");
	}
	else
	{
		TraceLog(ERROR_LEVEL, "Failed to send Order to ARCA DIRECT Server\n");

		return ERROR;
	}
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessSendCancelRequestToECNOrder
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessSendCancelRequestToECNOrder(int clOrdId, long ecnOrderId)
{
	int i, indexOpenOrder = -1;

	for (i = asOpenOrder.countOpenOrder - 1; i >= 0; i--)
	{
		if (clOrdId == asOpenOrder.orderSummary[i].clOrdId)
		{
			indexOpenOrder = i;

			break;
		}
	}

	if (indexOpenOrder == -1)
	{
		TraceLog(ERROR_LEVEL, "Invalid order id, clOrdId = %d\n", clOrdId);

		return ERROR;
	}

	switch (asOpenOrder.orderSummary[indexOpenOrder].ECNId)
	{
		case TYPE_NASDAQ_RASH:
			return ProcessSendCancelRequestToNASDAQ_RASH(indexOpenOrder);
			break;

		case TYPE_ARCA_DIRECT:
			return ProcessSendCancelRequestToARCA_DIRECT(indexOpenOrder, ecnOrderId);
			break;
		default:
			return ERROR;
			break;
	}

	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessSendCancelRequestToNASDAQ_RASH
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessSendCancelRequestToNASDAQ_RASH(int indexOpenOrder)
{
	t_NASDAQ_RASH_CancelOrder cancelOrder;
	
	//orderToken
	char orderToken[16] = "\0";
	sprintf(orderToken, "%d", asOpenOrder.orderSummary[indexOpenOrder].clOrdId);
	
	int len = strlen(orderToken);

	memset(&orderToken[len], ' ', 14 - len);
	orderToken[14] = 0;

	cancelOrder.orderToken = orderToken;
	
	//stock
	memset(cancelOrder.symbol, 0, SYMBOL_LEN);
	strncpy(cancelOrder.symbol, asOpenOrder.orderSummary[indexOpenOrder].symbol, SYMBOL_LEN);
	
	//share
	cancelOrder.share = asOpenOrder.orderSummary[indexOpenOrder].shares;
	
	// Account
	cancelOrder.accountSuffix = asOpenOrder.orderSummary[indexOpenOrder].accountSuffix;
	
	// Call function to send cancel request
	TraceLog(DEBUG_LEVEL, "Begin build and send cancel request to NASDAQ RASH server  ...\n");

	int retval = BuildAndSend_NASDAQ_RASH_CancelMsg(&cancelOrder);

	if (retval == SUCCESS)
	{
		TraceLog(DEBUG_LEVEL, "Send cancel request to NASDAQ RASH Server successfully\n");

		return SUCCESS;
	}
	else
	{
		TraceLog(ERROR_LEVEL, "Failed to send cancel request to NASDAQ RASH Server\n");

		return ERROR;
	}
}

/****************************************************************************
- Function name:	ProcessSendManualCancelRequestToNASDAQ_RASH
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessSendManualCancelRequestToNASDAQ_RASH(t_ManualCancelOrder *cancelOrder)
{
	TraceLog(DEBUG_LEVEL, "Begin build and send manual cancel request to NASDAQ RASH server...\n");

	int retval = BuildAndSend_NASDAQ_RASH_ManualCancelMsg(cancelOrder);

	if (retval == SUCCESS)
	{
		TraceLog(DEBUG_LEVEL, "Send manual cancel request to NASDAQ RASH Server successfully\n");

		return SUCCESS;
	}
	else
	{
		TraceLog(ERROR_LEVEL, "Failed to send manual cancel request to NASDAQ RASH Server\n");

		return ERROR;
	}
}

/****************************************************************************
- Function name:	ProcessSendManualCancelOrder
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessSendManualCancelOrder(t_ManualCancelOrder *cancelOrder, int ECNId)
{
	if (ECNId == TYPE_NASDAQ_RASH)
	{
		return ProcessSendManualCancelRequestToNASDAQ_RASH(cancelOrder);
	}
	else if (ECNId == TYPE_ARCA_DIRECT)
	{
		return ProcessSendManualCancelRequestToARCA_DIRECT(cancelOrder);
	}
	else
	{
		TraceLog(ERROR_LEVEL, "Invalid ECN to send manual cancel order(%d)\n", ECNId);
		return ERROR;
	}
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessSendCancelRequestToARCA_DIRECT
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessSendCancelRequestToARCA_DIRECT(int indexOpenOrder, long ecnOrderId)
{	
	t_ARCA_DIRECT_CancelOrder cancelOrder;

	// ARCA DIRECT Order Id
	cancelOrder.ecnOrderID = ecnOrderId;
	
	// Original Client order Id
	cancelOrder.origClOrdID = asOpenOrder.orderSummary[indexOpenOrder].clOrdId;

	// shares
	cancelOrder.orderQty = asOpenOrder.orderSummary[indexOpenOrder].shares;

	// Symbol
	strncpy(cancelOrder.symbol, asOpenOrder.orderSummary[indexOpenOrder].symbol, SYMBOL_LEN);

	// Side
	if (asOpenOrder.orderSummary[indexOpenOrder].side == BUY_TO_CLOSE_TYPE)
	{
		cancelOrder.side = '1';
	}
	else if (asOpenOrder.orderSummary[indexOpenOrder].side == SELL_TYPE)
	{
		cancelOrder.side = '2';
	}
	else if (asOpenOrder.orderSummary[indexOpenOrder].side == SHORT_SELL_TYPE)
	{
		cancelOrder.side = '5';
	}
	else 
	{
		cancelOrder.side = '6';
	}
	
	// Account
	cancelOrder.accountSuffix = asOpenOrder.orderSummary[indexOpenOrder].accountSuffix;

	// Call function to send cancel request
	TraceLog(DEBUG_LEVEL, "Begin build and send cancel request to ARCA DIRECT server  ...\n");

	pthread_mutex_lock(&CancelRequestMutex[indexOpenOrder % MAX_ORDER_PLACEMENT]);
	if(asOpenOrder.orderSummary[indexOpenOrder % MAX_ORDER_PLACEMENT].cancelRequest == 1)
	{
		pthread_mutex_unlock(&CancelRequestMutex[indexOpenOrder % MAX_ORDER_PLACEMENT]);
		return SUCCESS;
	}

	int retval = BuildAndSend_ARCA_DIRECT_CancelMsg(&cancelOrder);
	

	if (retval == SUCCESS)
	{
		asOpenOrder.orderSummary[indexOpenOrder % MAX_ORDER_PLACEMENT].cancelRequest = 1;
		TraceLog(DEBUG_LEVEL, "Send cancel request to ARCA DIRECT Server successfully\n");
	}
	else
	{
		TraceLog(ERROR_LEVEL, "Failed to send cancel request to ARCA DIRECT Server\n");
	}
	
	pthread_mutex_unlock(&CancelRequestMutex[indexOpenOrder % MAX_ORDER_PLACEMENT]);
	
	return retval;
}

/****************************************************************************
- Function name:	ProcessSendManualCancelRequestToARCA_DIRECT
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessSendManualCancelRequestToARCA_DIRECT(t_ManualCancelOrder *cancelOrder)
{
	// Call function to send cancel request
	TraceLog(DEBUG_LEVEL, "Begin build and send manual cancel request to ARCA DIRECT server...\n");

	int retval = BuildAndSend_ARCA_DIRECT_ManualCancelMsg(cancelOrder);

	if (retval == SUCCESS)
	{
		TraceLog(DEBUG_LEVEL, "Send manual cancel request to ARCA DIRECT Server successfully\n");

		return SUCCESS;
	}
	else
	{
		TraceLog(ERROR_LEVEL, "Failed to send manual cancel request to ARCA DIRECT Server\n");

		return ERROR;
	}

}

/****************************************************************************
- Function name:	ProcessAddNewOrder
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessAddNewOrder(t_SendNewOrder *order)
{
	t_OpenOrderInfo openOrder;
	
	openOrder.price = order->price;
	openOrder.avgPrice = openOrder.price;
	strncpy(openOrder.symbol, order->symbol, SYMBOL_LEN);
	openOrder.accountSuffix = order->accountSuffix;
	openOrder.symbolIndex = order->symbolIndex;
	openOrder.side = order->side;
	openOrder.shares = order->shares;
	openOrder.leftShares = openOrder.shares;
	openOrder.canceledShares = 0;
	openOrder.cancelRequest = 0;
	openOrder.ECNId = order->ECNId;
	openOrder.tif = order->tif;
	openOrder.clOrdId = order->orderId;
	openOrder.status = orderStatusIndex.Sent;
	openOrder.type = order->type;
	openOrder.requester = TRADER_REQUEST;
	openOrder.numSeconds = -1;
	openOrder.numMicroseconds = -1;

	int indexOpenOrder = GetOrderIdIndex(openOrder.clOrdId);

	if (indexOpenOrder == -1)
	{
		TraceLog(ERROR_LEVEL, "GetOrderIdIndex: orderId = %d\n", openOrder.clOrdId);

		return ERROR;
	}
	
	positionCollection[order->symbolIndex].isCalculatingInformation = NO;

	if (indexOpenOrder == asOpenOrder.countOpenOrder)
	{
		if (asOpenOrder.countOpenOrder + 1 == MAX_ORDER_PLACEMENT)
		{
			int i;
			for(i = 0; i < MAX_ORDER_PLACEMENT; i++)
			{
				asOpenOrder.orderSummary[i].cancelRequest = 0;
			}
			
			TraceLog(ERROR_LEVEL, "Orders placement collection is full, MAX_ORDER_PLACEMENT = %d\n", MAX_ORDER_PLACEMENT);

			return ERROR;
		}

		// Order summary	
		memcpy(&asOpenOrder.orderSummary[indexOpenOrder], &openOrder, sizeof(t_OpenOrderInfo));
		
		// Increase count open order
		asOpenOrder.countOpenOrder ++;
		
		if (openOrder.type == LIQUIDATE_ORDER)
		{
			if (AddToLiquidateCollection (indexOpenOrder) == ERROR)
			{
				TraceLog(ERROR_LEVEL, "Liquidate order collection is full!\n");
				return ERROR;
			}
		}
	}

	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessOrderAccepted
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessOrderAccepted(int orderId, long ecnOrderId, int numSeconds, int numMicroseconds, int isTrading)
{
	int indexOpenOrder = GetOrderIdIndex(orderId);

	if (indexOpenOrder == -1)
	{
		TraceLog(ERROR_LEVEL, "GetOrderIdIndex: orderId = %d\n", orderId);

		return ERROR;
	}
	
	asOpenOrder.orderSummary[indexOpenOrder].ecnOrderId = ecnOrderId;
	asOpenOrder.orderSummary[indexOpenOrder].status = orderStatusIndex.Live;
	asOpenOrder.orderSummary[indexOpenOrder].numSeconds = numSeconds;
	asOpenOrder.orderSummary[indexOpenOrder].numMicroseconds = numMicroseconds;
	
	int stockSymbolIndex = asOpenOrder.orderSummary[indexOpenOrder].symbolIndex;
	int isSkipChecking = 0;
	
	if ( (isTrading == YES) && (asOpenOrder.orderSummary[indexOpenOrder].ECNId == TYPE_ARCA_DIRECT) )
	{		
		long refNum = ecnOrderId;

		//Note: refNum is a long number combined by concatenated values: <GTC Indicator><SystemID><MarketID><OrderID>
		int _orderID = (refNum & 4294967295L);
		AddQuoteIgnore(stockSymbolIndex, _orderID, asOpenOrder.orderSummary[indexOpenOrder].shares, asOpenOrder.orderSummary[indexOpenOrder].price);

		TraceLog(DEBUG_LEVEL, "Process ARCA ACK message, orderId = %d, ARCAOrderId = %ld (orderId = %d)\n", orderId, refNum, _orderID);

		t_PartialHashingItem *ARCA_Hashing;
		t_OrderDetailNode *oldNode;
		
		long _refNum = StockSymbolIndex_to_ArcaSymbolIndex_Mapping[stockSymbolIndex]; 
		_refNum = ((_refNum << 32) | _orderID);
	
		ARCA_Hashing = FindPartialHashingItem(BOOK_ARCA, _refNum);
		
		if (ARCA_Hashing != NULL)
		{
			int tempSide;

			if (asOpenOrder.orderSummary[indexOpenOrder].side == BUY_TO_CLOSE_TYPE)
			{
				tempSide = BID_SIDE;
			}
			else
			{
				tempSide = ASK_SIDE;
			}			
			
			pthread_mutex_lock(&deleteQuotesMgmtMutex[BOOK_ARCA][stockSymbolIndex][tempSide]);
			ARCA_Hashing = FindPartialHashingItem(BOOK_ARCA, _refNum);

			if (ARCA_Hashing != NULL)
			{
				oldNode = ARCA_Hashing->node;

				TraceLog(DEBUG_LEVEL, "Own quote was added into book, refNum = %ld (orderId = %d), side = %d, shares = %d, price = %lf\n", oldNode->info.refNum, _orderID, tempSide, oldNode->info.shareVolume, oldNode->info.sharePrice);
				isSkipChecking = 1;
				
				// Delete old node
				if (GeneralDelete(stockBook[BOOK_ARCA][tempSide][stockSymbolIndex], oldNode, tempSide, stockSymbolIndex) == ERROR)
				{
					TraceLog(ERROR_LEVEL, "ARCA: Cannot delete node\n");
				}
				else
				{
					DeletePartialHashingItem(BOOK_ARCA, _refNum);
					DeleteBackupPartialHashingItem(BOOK_ARCA, _refNum);
				}
			}	

			pthread_mutex_unlock(&deleteQuotesMgmtMutex[BOOK_ARCA][stockSymbolIndex][tempSide]);
		}		
	}

	positionCollection[asOpenOrder.orderSummary[indexOpenOrder].symbolIndex].isTrading = YES;
	
	if (positionCollection[stockSymbolIndex].typePlacedOrder == BBO_ORDER)
	{
		int side = !positionCollection[stockSymbolIndex].stuck.side;
		if (isSkipChecking == 1)
		{
			// Set iHold = NO, so PM will not cancel and replace the order
			// this happens for ARCA Book only (when own Arca quote comes to book first, and Arca order ack comes later)!
			// So isHold = NO will prevent order replacement in case own quote comes first, ACK comes later,
			// if we do not do this, PM will cancel and replace new order which is the same as the current order
			positionCollection[stockSymbolIndex].holdQuote[side].isHold = NO;
			positionCollection[stockSymbolIndex].holdQuote[side].price = positionCollection[stockSymbolIndex].bestQuote[side].info.sharePrice;
		}
		
		// Check current inside in time from sent new order to received ACK
		if ((positionCollection[stockSymbolIndex].holdQuote[side].isHold == YES) &&
			(!feq(positionCollection[stockSymbolIndex].holdQuote[side].price, positionCollection[stockSymbolIndex].bestQuote[side].info.sharePrice)))
		{
			TraceLog(DEBUG_LEVEL, "The current inside was changed while PM waiting to receive ACK\n");
			
			CheckBBOOrder(stockSymbolIndex, side, 
						positionCollection[stockSymbolIndex].holdQuote[side].ecnBookID, 
						positionCollection[stockSymbolIndex].holdQuote[side].refNum, 
						positionCollection[stockSymbolIndex].holdQuote[side].shares, 
						positionCollection[stockSymbolIndex].holdQuote[side].price, 
						ROUND_LOT_TYPE);
		}
	}

	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessOrderFilled
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessOrderFilled(int orderId, int shares)
{
	int indexOpenOrder = GetOrderIdIndex(orderId);

	if (indexOpenOrder == -1)
	{
		TraceLog(ERROR_LEVEL, "GetOrderIdIndex: orderId = %d\n", orderId);

		return ERROR;
	}	

	// Reset number of Consecutive Reject to 0
	positionCollection[asOpenOrder.orderSummary[indexOpenOrder].symbolIndex].consecutiveRejectNumber = 0;
	
	if (asOpenOrder.orderSummary[indexOpenOrder].leftShares >= shares)
	{
		asOpenOrder.orderSummary[indexOpenOrder].leftShares -= shares;
	}
	else
	{
		TraceLog(ERROR_LEVEL, "Why filled shares(%d) > left shares(%d), orderId = %d\n", shares, asOpenOrder.orderSummary[indexOpenOrder].leftShares, orderId);
		
		asOpenOrder.orderSummary[indexOpenOrder].status = orderStatusIndex.Closed;
		
		//This stuck is fully processed, clear its cross id
		CrossIDCollection[asOpenOrder.orderSummary[indexOpenOrder].symbolIndex] = 0;
		
		//Remove the stuck from list
		positionCollection[asOpenOrder.orderSummary[indexOpenOrder].symbolIndex].stuck.shares = 0;

		return ERROR;
	}
	
	if (asOpenOrder.orderSummary[indexOpenOrder].leftShares == 0)
	{
		asOpenOrder.orderSummary[indexOpenOrder].status = orderStatusIndex.Closed;

		positionCollection[asOpenOrder.orderSummary[indexOpenOrder].symbolIndex].isTrading = NO;

		if (asOpenOrder.orderSummary[indexOpenOrder].ECNId == TYPE_ARCA_DIRECT)
		{
			long _refNum = asOpenOrder.orderSummary[indexOpenOrder].ecnOrderId;
			int _orderID = (_refNum & 4294967295L);
			DeleteQuoteIgnore(asOpenOrder.orderSummary[indexOpenOrder].symbolIndex, _orderID);
		}
		else if(asOpenOrder.orderSummary[indexOpenOrder].ECNId == TYPE_NASDAQ_RASH)
		{
			LastNasdaqOrder[asOpenOrder.orderSummary[indexOpenOrder].symbolIndex].side = -1;	//Remove own NASDAQ order
		}
		
		//This stuck is fully processed, clear its cross id
		CrossIDCollection[asOpenOrder.orderSummary[indexOpenOrder].symbolIndex] = 0;
		
		//Remove the stuck from list
		positionCollection[asOpenOrder.orderSummary[indexOpenOrder].symbolIndex].stuck.shares = 0;

		return SUCCESS;
	}

	if ((shares > 0) && (asOpenOrder.orderSummary[indexOpenOrder].shares - asOpenOrder.orderSummary[indexOpenOrder].leftShares == shares))
	{
		if (asOpenOrder.orderSummary[indexOpenOrder].type == LIQUIDATE_ORDER)
		{			
			if (asOpenOrder.orderSummary[indexOpenOrder].requester == TRADER_REQUEST)
			{
				// Send a canccel request
				asOpenOrder.orderSummary[indexOpenOrder].requester = PM_REQUEST;
				ProcessSendCancelRequestToECNOrder(orderId, asOpenOrder.orderSummary[indexOpenOrder].ecnOrderId);
			}
		}
	}

	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessOrderCanceled
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessOrderCanceled(int orderId, int canceledShares)
{
	int indexOpenOrder = GetOrderIdIndex(orderId);

	if (indexOpenOrder == -1)
	{
		TraceLog(ERROR_LEVEL, "GetOrderIdIndex: orderId = %d\n", orderId);

		return ERROR;
	}

	// Check to see if PM is re-calculating stuck information
	asOpenOrder.orderSummary[indexOpenOrder].status = orderStatusIndex.CanceledByUser;	
	positionCollection[asOpenOrder.orderSummary[indexOpenOrder].symbolIndex].isTrading = NO;
	
	// Reset number of Consecutive Reject to 0
	positionCollection[asOpenOrder.orderSummary[indexOpenOrder].symbolIndex].consecutiveRejectNumber = 0;

	if (asOpenOrder.orderSummary[indexOpenOrder].ECNId == TYPE_ARCA_DIRECT)
	{
		long _refNum = asOpenOrder.orderSummary[indexOpenOrder].ecnOrderId;
		int _orderID = (_refNum & 4294967295L);
		DeleteQuoteIgnore(asOpenOrder.orderSummary[indexOpenOrder].symbolIndex, _orderID);
	}
	else if(asOpenOrder.orderSummary[indexOpenOrder].ECNId == TYPE_NASDAQ_RASH)
	{
		LastNasdaqOrder[asOpenOrder.orderSummary[indexOpenOrder].symbolIndex].side = -1;	//Remove own NASDAQ order
	}

	// --------------------------------------------------------------------------
	// If the order is currently handle by PM algorithm
	// It will check to see if left shares > 0. If so, it continue to place order
	// --------------------------------------------------------------------------	
	if (asOpenOrder.orderSummary[indexOpenOrder].leftShares > 0)
	{	
		// Prepare information for new order to be going to send
		t_Position stuck;
		strncpy(stuck.symbol, asOpenOrder.orderSummary[indexOpenOrder].symbol, SYMBOL_LEN);
		stuck.accountSuffix = asOpenOrder.orderSummary[indexOpenOrder].accountSuffix;
		
		if(canceledShares == -1) // ARCA-DIRECT
		{
			stuck.shares = asOpenOrder.orderSummary[indexOpenOrder].leftShares;
			asOpenOrder.orderSummary[indexOpenOrder].canceledShares = asOpenOrder.orderSummary[indexOpenOrder].leftShares;
		}
		else // NASDAQ-RASH
		{
			stuck.shares = canceledShares;
			asOpenOrder.orderSummary[indexOpenOrder].canceledShares += canceledShares;
		}

		stuck.price = asOpenOrder.orderSummary[indexOpenOrder].price;
		stuck.referencePrice = positionCollection[asOpenOrder.orderSummary[indexOpenOrder].symbolIndex].stuck.referencePrice;
		
		if (asOpenOrder.orderSummary[indexOpenOrder].side == SELL_TYPE)
		{
			stuck.side = BID_SIDE;
		}
		else
		{
			stuck.side = ASK_SIDE;
		}
		
		if (positionCollection[asOpenOrder.orderSummary[indexOpenOrder].symbolIndex].isCalculatingInformation == YES)
		{
			// PM is re-calculating stuck information! Cancel any existing order on that symbol ...

			return SUCCESS;
		}
			
		if (asOpenOrder.orderSummary[indexOpenOrder].requester == PM_REQUEST)
		{
			//Continue to place order for this stuck
			CheckAndPlaceOrder(&stuck, 0, YES, NO, indexOpenOrder);
		}
		else
		{
			SendPMDisengageAlertToAS(&stuck, DISENGAGE_REASON_TRADER_REQUESTED_DISENGAGE);
		}
	}

	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessCanceleRejected
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessCanceleRejected(int orderId)
{
	int indexOpenOrder = GetOrderIdIndex(orderId);

	if (indexOpenOrder == -1)
	{
		TraceLog(ERROR_LEVEL, "GetOrderIdIndex: orderId = %d\n", orderId);

		return ERROR;
	}

	asOpenOrder.orderSummary[indexOpenOrder].status = orderStatusIndex.CancelRejected;
	positionCollection[asOpenOrder.orderSummary[indexOpenOrder].symbolIndex].isTrading = NO;

	// Reset number of Consecutive Reject to 0
	positionCollection[asOpenOrder.orderSummary[indexOpenOrder].symbolIndex].consecutiveRejectNumber = 0;
	
	if (asOpenOrder.orderSummary[indexOpenOrder].leftShares > 0)
	{
		if (asOpenOrder.orderSummary[indexOpenOrder].ECNId == TYPE_ARCA_DIRECT)
		{
			long _refNum = asOpenOrder.orderSummary[indexOpenOrder].ecnOrderId;
			int _orderID = (_refNum & 4294967295L);
			
			DeleteQuoteIgnore(asOpenOrder.orderSummary[indexOpenOrder].symbolIndex, _orderID);
		}
		else if(asOpenOrder.orderSummary[indexOpenOrder].ECNId == TYPE_NASDAQ_RASH)
		{
			LastNasdaqOrder[asOpenOrder.orderSummary[indexOpenOrder].symbolIndex].side = -1;	//Remove own NASDAQ order
		}

		ProcessDisengageSymbol(asOpenOrder.orderSummary[indexOpenOrder].symbol, TRADER_REQUEST);
	}
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessOrderRejected
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessOrderRejected(int orderId)
{
	int indexOpenOrder = GetOrderIdIndex(orderId);

	if (indexOpenOrder == -1)
	{
		TraceLog(ERROR_LEVEL, "GetOrderIdIndex: orderId = %d\n", orderId);

		return ERROR;
	}

	asOpenOrder.orderSummary[indexOpenOrder].status = orderStatusIndex.Rejected;
	positionCollection[asOpenOrder.orderSummary[indexOpenOrder].symbolIndex].isTrading = NO;
	
	// ------------------------------------------------------
	//		Re-send the order for up to 3 times
	// ------------------------------------------------------
	
	positionCollection[asOpenOrder.orderSummary[indexOpenOrder].symbolIndex].consecutiveRejectNumber++;
	
	// Get current stuck information
	t_Position stuck;
	strncpy(stuck.symbol, asOpenOrder.orderSummary[indexOpenOrder].symbol, SYMBOL_LEN);
	stuck.accountSuffix = asOpenOrder.orderSummary[indexOpenOrder].accountSuffix;
	stuck.shares = asOpenOrder.orderSummary[indexOpenOrder].leftShares;
	stuck.price = asOpenOrder.orderSummary[indexOpenOrder].price;
	stuck.referencePrice = positionCollection[asOpenOrder.orderSummary[indexOpenOrder].symbolIndex].stuck.referencePrice;
	
	if (asOpenOrder.orderSummary[indexOpenOrder].side == SELL_TYPE)
	{
		stuck.side = BID_SIDE;
	}
	else
	{
		stuck.side = ASK_SIDE;
	}
	
	if (positionCollection[asOpenOrder.orderSummary[indexOpenOrder].symbolIndex].consecutiveRejectNumber > 2)
	{
		TraceLog(DEBUG_LEVEL, "Order(%.8s) rejected %d time(s). Disengage!\n", stuck.symbol, positionCollection[asOpenOrder.orderSummary[indexOpenOrder].symbolIndex].consecutiveRejectNumber);
		
		positionCollection[asOpenOrder.orderSummary[indexOpenOrder].symbolIndex].consecutiveRejectNumber = 0;
		
		SendPMDisengageAlertToAS(&stuck, DISENGAGE_REASON_CONSECUTIVE_REJECTS);
		return SUCCESS;
	}
	
	// Continue to place order for this stuck
	TraceLog(DEBUG_LEVEL, "Order(%.8s) rejected %d time(s). Re-assess the stuck...\n", stuck.symbol, positionCollection[asOpenOrder.orderSummary[indexOpenOrder].symbolIndex].consecutiveRejectNumber);
	
	CheckAndPlaceOrder(&stuck, 0, NO, NO, -1);
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessDisengageSymbol
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessDisengageSymbol(char *symbol, int requester)
{
	// Update symbol index
	int symbolIndex = GetStockSymbolIndex(symbol);

	// If stock symbol is not in SymbolList
	if (symbolIndex == -1)
	{
		symbolIndex = UpdateStockSymbolIndex(symbol);
		if (symbolIndex == -1)
		{
			TraceLog(ERROR_LEVEL, "Cannot find symbol = %.8s in SymbolList\n", symbol);
			return ERROR;
		}
	}

	// Check symbol trading
	int i;
	
	positionCollection[symbolIndex].willReplace = NO;

	for (i = 0; i < asOpenOrder.countOpenOrder; i++)
	{
		if ((asOpenOrder.orderSummary[i].status == orderStatusIndex.Live) && (asOpenOrder.orderSummary[i].symbolIndex == symbolIndex))
		{
			if (asOpenOrder.orderSummary[i].requester == TRADER_REQUEST)
			{
				asOpenOrder.orderSummary[i].requester = requester;

				// Cancel any existing order on that symbol
				TraceLog(DEBUG_LEVEL, "Cancel any orders on this symbol = %.8s, orderId = %d\n", symbol, asOpenOrder.orderSummary[i].clOrdId);
				
				ProcessSendCancelRequestToECNOrder(asOpenOrder.orderSummary[i].clOrdId, asOpenOrder.orderSummary[i].ecnOrderId);
			}
			else
			{
				asOpenOrder.orderSummary[i].requester = requester;
			}
		}
		else if (asOpenOrder.orderSummary[i].symbolIndex == symbolIndex)
		{
			asOpenOrder.orderSummary[i].requester = TRADER_REQUEST;
		}
	}

	return SUCCESS;
}

/****************************************************************************
- Function name:	AddTradeForTradeSymbolList
- Input:			
- Output:		
- Return:		
- Description:
- Usage:		
****************************************************************************/
int AddTradeForTradeSymbolList(char *symbol, t_TradeInfo *trade)
{	
	t_TradeList *symbolInfo;
		
	// Get stock symbol index
	int symbolIndex = GetTradeSymbolIndex(symbol);	
	
	if (tradeSymbolList.countSymbol >= MAX_SYMBOL_TRADE)
	{
		TraceLog(ERROR_LEVEL, "Trade symbol list is full, countSymbol = %d, MAX_SYMBOL = %d\n", tradeSymbolList.countSymbol, MAX_SYMBOL_TRADE);

		return -1;
	}

	if (symbolIndex == -1)
	{
		return -1;
	}

	// Get current trade information of current stock symbol
	symbolInfo = &tradeSymbolList.symbolInfo[symbolIndex];

	// Update Daily High Price
	if (fgt(trade->price, symbolInfo->highPrice))
	{
		symbolInfo->highPrice = trade->price;
	}

	// Update Daily Low Price
	if (flt(trade->price, symbolInfo->lowPrice) || fle(symbolInfo->lowPrice, 0.00))
	{
		symbolInfo->lowPrice = trade->price;
	}

	// Add trade ...
	memcpy(&symbolInfo->tradeList[symbolInfo->countTrade % MAX_LAST_TRADE], trade, sizeof(t_TradeInfo));	
	symbolInfo->countTrade++;

	return SUCCESS;
}

/****************************************************************************
- Function name:	GetExecutionRatePerSecond
- Input:			
- Output:		
- Return:		
- Description:
- Usage:		
****************************************************************************/
int GetExecutionRatePerSecond(int symbolIndex)
{
	t_TradeList *symbolInfo;
	symbolInfo = &tradeSymbolList.symbolInfo[symbolIndex];
	
	int totalTrade = symbolInfo->countTrade;
	int i, totalTrades = 0;
	
	int startIndex = totalTrade - MAX_LAST_TRADE;
	if (startIndex < 0)
	{
		startIndex = 0;
	}
	
	double timeTmp;
	
	// Get current time
	int currentTime =  GetLocalTimeInTradingTime();
	for (i = totalTrade - 1; i >= startIndex; i--)
	{
		timeTmp = (double)(currentTime * 1000 - symbolInfo->tradeList[i % MAX_LAST_TRADE].timestamp) / 1000;

		if (fle(timeTmp, (asCollection.globalConf.movingAveragePeriod * 1.0)))
		{
			totalTrades++;
		}
		else
		{
			break;
		}
	}
	
	return totalTrades;

}

/****************************************************************************
- Function name:	ShowTradedInfo
- Input:			
- Output:		
- Return:		
- Description:
- Usage:		
****************************************************************************/
int ShowTradedInfo(char *symbol)
{
	t_TradeList *symbolInfo;
		
	// Get stock symbol index
	int symbolIndex = GetTradeSymbolIndex(symbol);	
	
	if (tradeSymbolList.countSymbol >= MAX_SYMBOL_TRADE)
	{
		TraceLog(ERROR_LEVEL, "Trade symbol list is full, countSymbol = %d, MAX_SYMBOL = %d\n", tradeSymbolList.countSymbol, MAX_SYMBOL_TRADE);

		return -1;
	}

	if (symbolIndex == -1)
	{
		TraceLog(ERROR_LEVEL, "Invalid symbol (%.8s), symbolIndex = %d\n", symbol, symbolIndex);

		return -1;
	}

	// Get current trade information of current stock symbol
	symbolInfo = &tradeSymbolList.symbolInfo[symbolIndex];

	int totalTrade = symbolInfo->countTrade;
	//int totalShares = symbolInfo->totalShares;
	double highPrice = symbolInfo->highPrice;
	double lowPrice = symbolInfo->lowPrice;

	int i, countTrade = 0;

	// Initialize ...

	TraceLog(DEBUG_LEVEL, "Symbol = %.8s, high price = %.2lf, low price = %.2lf\n", symbol, highPrice, lowPrice);
	
	int startIndex = totalTrade - MAX_LAST_TRADE;
	if (startIndex < 0)
	{
		startIndex = 0;
	}

	// Find first item in last 30 seconds
	double timeTmp;
	
	// Get current time
	int currentTime = GetLocalTimeInTradingTime();
	
	for (i = totalTrade - 1; i >= startIndex; i--)
	{
		timeTmp = (double)(currentTime * 1000 - symbolInfo->tradeList[i % MAX_LAST_TRADE].timestamp) / 1000;

		if (timeTmp <= MAX_SECONDS_FOR_TRADED_INFO)
		{
			if (countTrade == 0)
			{
				TraceLog(DEBUG_LEVEL, "Sale data for %.8s received from CTS/UTDF in last 30 seconds: current time = %02d:%02d:%02d\n", symbol, 
					currentTime / 3600, (currentTime % 3600) / 60, ((currentTime % 3600) % 60));
			}

			TraceLog(DEBUG_LEVEL, "%02d:%02d:%02d.%03d: %c %d %.2lf\n", 
				symbolInfo->tradeList[i % MAX_LAST_TRADE].timestamp / 3600000, (symbolInfo->tradeList[i % MAX_LAST_TRADE].timestamp % 3600000) / 60000, 
				((symbolInfo->tradeList[i % MAX_LAST_TRADE].timestamp % 3600000) % 60000) / 1000, symbolInfo->tradeList[i % MAX_LAST_TRADE].timestamp % 1000, 
				symbolInfo->tradeList[i % MAX_LAST_TRADE].participantId, symbolInfo->tradeList[i % MAX_LAST_TRADE].shares, symbolInfo->tradeList[i % MAX_LAST_TRADE].price);

			countTrade += 1;

		}
		else
		{
			break;
		}
	}

	if (countTrade == 0)
	{
		TraceLog(DEBUG_LEVEL, "Sale data received for %.8s from CTS/UTDF in last 30 seconds: Nothing. Current time = %02d:%02d:%02d\n", symbol, 
				currentTime / 3600, (currentTime % 3600) / 60, ((currentTime % 3600) % 60));
	}

	return SUCCESS;
}

/****************************************************************************
- Function name:	CheckManualOrderPriceWithLastTrade
- Input:			New order info
- Output:			N/A
- Return:			1 if price is valid, 0 if invalid
- Description:		Use to check new order price with last trade
					from CTS/UTDF
- Usage:
****************************************************************************/
int CheckManualOrderPriceWithLastTrade(t_ManualOrder *newOrder)
{
	int symbolIndex = GetTradeSymbolIndex(newOrder->symbol);

	if (symbolIndex == -1)
	{
		return 0;
	}
	
	int executionRate = GetExecutionRatePerSecond(symbolIndex);
	if (executionRate > asCollection.globalConf.highExecutionRate)
	{
		//Fast market mode, we will not check with last trade
		//Will send order right away
		return 1;
	}
	
	t_TradeList *symbolInfo = &tradeSymbolList.symbolInfo[symbolIndex];

	int latestIndex = symbolInfo->countTrade - 1;
	if (latestIndex < 0)
	{
		//There is no last trade
		return 0;
	}
	
	double lastPrice = symbolInfo->tradeList[latestIndex % MAX_LAST_TRADE].price;
	double guidanceRange;
	int intraday = 0;
	
	if (GetCurrentTradingSession() == IN_TRADING_SESSION)
	{
		intraday = 1;
	}

	if ((fgt(newOrder->price, 0.00)) && fle(newOrder->price, 25.00))
	{
		if (intraday == 1)
			guidanceRange = (lastPrice * 0.1); //10%
		else
			guidanceRange = (lastPrice * 0.2); //20%
	}
	else if ((fgt(newOrder->price, 25.00)) && fle(newOrder->price, 50.00))
	{
		if (intraday == 1)
			guidanceRange = (lastPrice * 0.05); //5%
		else
			guidanceRange = (lastPrice * 0.1); //10%
	}
	else if (fgt(newOrder->price, 50.00))
	{
		if (intraday == 1)
			guidanceRange = (lastPrice * 0.03); //3%
		else
			guidanceRange = (lastPrice * 0.06); //6%
	}
	else
	{
		//Invalid price (less/equal to zero)
		return 0;
	}

	if(fgt(fabs(newOrder->price - lastPrice), guidanceRange))
	{
		//Fall outside guidance range
		return 0;
	}

	return 1;
}

/****************************************************************************
- Function name:	GetCurrentTradingSession
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:
****************************************************************************/
int GetCurrentTradingSession(void)
{
	// Get current time
	time_t now = hbitime_seconds();

	struct tm local;
	
	// Convert to local time
	localtime_r(&now, &local);

	if ( (local.tm_hour < PRE_MARKET_END_HOUR) || 
		(local.tm_hour == PRE_MARKET_END_HOUR && local.tm_min < PRE_MARKET_END_MIN)
		)
		return PRE_TRADING_SESSION;
	else if (local.tm_hour < INTRADAY_END_HOUR)
		return IN_TRADING_SESSION;
	else
		return POST_TRADING_SESSION;
}
/***************************************************************************/
