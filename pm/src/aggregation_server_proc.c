/*****************************************************************************
**	Project:		Equity Arbitrage Application
**	Filename:		aggregation_server_proc.c
**	Description:	This file contains function definitions that were declared
					in aggregation_server_proc.h	
**	Author:			Luan Vo-Kinh
**	First created:	13-Sep-2007
**	Last updated:	-----------
*****************************************************************************/

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include <string.h>

#include "global_definition.h"
#include "socket_util.h"
#include "configuration.h"
#include "order_mgmt_proc.h"
#include "raw_data_mgmt.h"
#include "nasdaq_rash_proc.h"
#include "arca_direct_proc.h"
#include "nasdaq_book_proc.h"
#include "arca_book_proc.h"
#include "cts_feed_proc.h"
#include "cqs_feed_proc.h"
#include "uqdf_feed_proc.h"
#include "utdf_proc.h"
#include "trading_mgmt.h"
#include "aggregation_server_proc.h"
#include "bbo_mgmt.h"
#include "kernel_algorithm.h"

#define ADD_SYMBOL 0
#define REMOVE_SYMBOL 1
#define UPDATE_SYMBOL 2

#define MANUAL_ORDER_RESPONSE_SENT 1
#define MANUAL_ORDER_RESPONSE_FAILED_TO_SEND 2
#define MANUAL_ORDER_RESPONSE_FAILED_TO_CHECK 3
/****************************************************************************
** Global variables definition
****************************************************************************/

/*
- Added date: 3/19/2008
- By luanvk
- Purpose: send/receive PM message
*/
#define MSG_TYPE_FOR_SEND_PM_CURRENT_STATUS_TO_AS 41000
#define MSG_TYPE_FOR_SEND_PM_RAW_DATA_TO_AS 41001
#define MSG_TYPE_FOR_SEND_PM_END_OF_RAW_DATA_TO_AS 41002

#define MSG_TYPE_FOR_SET_PM_CONF_TO_AS 40500

#define MSG_TYPE_FOR_SEND_PM_GLOBAL_SETTING_TO_AS 41501
#define MSG_TYPE_FOR_SET_PM_GLOBAL_SETTING_FROM_AS 40501

#define MSG_TYPE_FOR_SEND_PM_BOOK_CONF_TO_AS 41502
#define MSG_TYPE_FOR_SET_PM_BOOK_CONF_FROM_AS 40502

#define MSG_TYPE_FOR_SEND_PM_ORDER_CONF_TO_AS 41503
#define MSG_TYPE_FOR_SET_PM_ORDER_CONF_FROM_AS 40503

#define MSG_TYPE_FOR_SEND_PM_CQS_SERVER_TO_AS 41511
#define MSG_TYPE_FOR_SET_PM_CQS_SERVER_FROM_AS 40511

#define MSG_TYPE_FOR_SEND_PM_IGNORE_SYMBOL_TO_AS 41505
#define MSG_TYPE_FOR_SET_PM_IGNORE_SYMBOL_FROM_AS 40505

#define MSG_TYPE_FOR_SEND_PM_SYMBOL_STATUS_TO_AS 41506
#define MSG_TYPE_FOR_GET_PM_SYMBOL_STATUS_TFROM_AS 40506

#define MSG_TYPE_FOR_SEND_HALT_STOCK_LIST_TO_AS 41507
#define MSG_TYPE_FOR_SEND_A_HALTED_STOCK_TO_AS 41508

#define MSG_TYPE_FOR_SEND_TRADED_LIST_TO_AS 41509
#define MSG_TYPE_FOR_GET_TRADED_LIST_FROM_AS 40509

#define MSG_TYPE_FOR_SEND_DISENGAGE_ALERT_TO_AS 41510

#define MSG_TYPE_FOR_SEND_BOOK_IDLE_WARNING_TO_AS 41512
#define MSG_TYPE_FOR_SEND_MANUAL_ORDER_RESPONSE_TO_AS 41515

#define MSG_TYPE_FOR_SEND_DATA_UNHANDLED_TO_AS 41532

#define MSG_TYPE_FOR_REQUEST_ENABLE_PM_FROM_AS 40000
#define MSG_TYPE_FOR_REQUEST_DISABLE_PM_FROM_AS 40001

#define MSG_TYPE_FOR_REQUEST_CONNECT_A_PM_BOOK_FROM_AS 40002
#define MSG_TYPE_FOR_REQUEST_DISCONNECT_A_PM_BOOK_FROM_AS 40003

#define MSG_TYPE_FOR_REQUEST_CONNECT_ALL_PM_BOOK_FROM_AS 40004
#define MSG_TYPE_FOR_REQUEST_DISCONNECT_ALL_PM_BOOK_FROM_AS 40005

#define MSG_TYPE_FOR_REQUEST_CONNECT_A_PM_ORDER_FROM_AS 40006
#define MSG_TYPE_FOR_REQUEST_DISCONNECT_A_PM_ORDER_FROM_AS 40007

#define MSG_TYPE_FOR_REQUEST_CONNECT_ALL_PM_ORDER_FROM_AS 40008
#define MSG_TYPE_FOR_REQUEST_DISCONNECT_ALL_PM_ORDER_FROM_AS 40009

#define MSG_TYPE_FOR_REQUEST_CONNECT_CTS_FROM_AS 40010
#define MSG_TYPE_FOR_REQUEST_DISCONNECT_CTS_FROM_AS 40011

#define MSG_TYPE_FOR_REQUEST_CONNECT_CQS_FROM_AS 40021
#define MSG_TYPE_FOR_REQUEST_DISCONNECT_CQS_FROM_AS 40022

#define MSG_TYPE_FOR_RECEIVE_RAW_DATA_STATUS_FROM_AS 40012
#define MSG_TYPE_FOR_RECEIVE_OPEN_POSITION_FROM_AS 40013
#define MSG_TYPE_FOR_RECEIVE_STUCK_FROM_AS 40014
#define MSG_TYPE_FOR_REQUEST_DISENGAGE_SYMBOL_FROM_AS 40015
#define MSG_TYPE_FOR_RECEIVE_CANCEL_REQUEST_FROM_AS 40016
#define MSG_TYPE_FOR_RECEIVE_MANUAL_ORDER_FROM_AS 40029
#define MSG_TYPE_FOR_RECEIVE_RASH_SEQUENCE_NUMBER_FROM_AS 40030
#define MSG_TYPE_FOR_RECEIVE_MANUAL_CANCEL_ORDER_FROM_AS 40031
#define MSG_TYPE_FOR_RECEIVE_HALT_STOCK_LIST_FROM_AS 40032
#define MSG_TYPE_FOR_RECEIVE_HALT_SYMBOL_FROM_AS 40019

#define MSG_TYPE_FOR_RECEIVE_PRICE_UDPATE_REQUEST_FROM_AS 40033
#define MSG_TYPE_FOR_SEND_PRICE_UPDATE_TO_AS 41533

#define MSG_TYPE_FOR_REQUEST_CONNECT_UTDF_FROM_AS 40017
#define MSG_TYPE_FOR_REQUEST_DISCONNECT_UTDF_FROM_AS 40018
#define MSG_TYPE_FOR_RECEIVE_HEARTBEAT_FROM_AS 40020

//For UQDF
#define MSG_TYPE_FOR_REQUEST_CONNECT_UQDF_FROM_AS 40023
#define MSG_TYPE_FOR_REQUEST_DISCONNECT_UQDF_FROM_AS 40024

//For Exchange Configuration
#define MSG_TYPE_FOR_SET_EXCHANGE_CONF_FROM_AS 41504
#define MSG_TYPE_FOR_SEND_EXCHANGE_CONF_TO_AS 40504

//For BBO Quotes Information
#define MSG_TYPE_FOR_GET_BBO_QUOTE_LIST_FROM_AS 40025
#define MSG_TYPE_FOR_SEND_BBO_QUOTE_LIST_TO_AS 40026

//Flush BBO Quotes For a exchange
#define MSG_TYPE_FOR_REQUEST_FLUSH_BBO_QUOTES_FROM_AS 40027

#define MSG_TYPE_FOR_REQUEST_RELOAD_OEC_CONFIG_FROM_AS 40028

//For flush PM book
#define AS_REQ_FLUSH_BOOK_OF_A_SYMBOL 32012

//For flush PM all symbol book
#define AS_REQ_FLUSH_BOOK_OF_ALL_SYMBOLS 32013

//For send AS message to get stuck info for resume handling stuck
#define MSG_TYPE_FOR_SEND_AS_MESSAGE_TO_GET_STUCK_INFO 41514

//For send RASH incomming sequence number to AS
#define MSG_TYPE_FOR_SEND_RASH_SEQ_TO_AS 40510

//For switch ARCA book feed
#define AS_REQ_SET_ARCA_BOOK_FEED 32017

#define MSG_TYPE_FOR_SEND_DISCONNECTED_BOOK_ORDER_ALERT_TO_AS 	41530
#define MSG_TYPE_FOR_SEND_DISABLED_TRADING_ALERT_TO_AS 			41531

// Does not accept
#define MSG_TYPE_FOR_SEND_DOES_NOT_ACCEPT_ALERT_TO_AS 43510

// For Sleeping Symbol
#define MSG_TYPE_FOR_SEND_SLEEPING_SYMBOL_TO_AS 41003

#define PM_TRADING_DISABLED_BY_EMERGENCY_STOP -6

/****************************************************************************
** Function declarations
****************************************************************************/
int isUpdatedRawDataStatus = 0;


/****************************************************************************
- Function name:	ProcessAggregationServer
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
void *ProcessAggregationServer(void *pthread_arg)
{
	SetKernelAlgorithm("[AS_BASE]");
	
	while (1)
	{
		// Initialize socket to listen connection from ASs
		int serverSocket = -1;
		serverSocket = Listen2Client(asCollection.config.port, 1);
		
		if (serverSocket == ERROR)
		{
			// We cannot use the provided port
			// Listen2Client has given the reason, so we do nothing
			return NULL;
		}
		
		// Waiting for connection from ASs
		while (1)
		{
			TraceLog(DEBUG_LEVEL, "***************************************************************\n");
			TraceLog(DEBUG_LEVEL, "Waiting for connection from AS client...\n");
			
			int clientSocket = -1;
			clientSocket = accept(serverSocket, NULL, NULL);
			
			if (clientSocket != ERROR)
			{
				TraceLog(DEBUG_LEVEL, "***New AS client was accepted at socket descriptor = %d ***\n", clientSocket);				
				
				// Set timeout for client socket
				struct timeval tv;
				tv.tv_sec = 3;
				tv.tv_usec = 0;
				
				socklen_t optlen = sizeof(tv);
				
				if (setsockopt(clientSocket, SOL_SOCKET, SO_SNDTIMEO, &tv, optlen) == ERROR)
				{
					TraceLog(ERROR_LEVEL, "Cannot set send-timeout for AS socket %d\n", clientSocket);
				}
				
				if (getsockopt(clientSocket, SOL_SOCKET, SO_SNDTIMEO, &tv, &optlen) == ERROR)
				{
					TraceLog(ERROR_LEVEL, "Cannot get send-timeout for AS socket %d\n", clientSocket);
				}
				else
				{
					TraceLog(DEBUG_LEVEL, "getsockopt with SO_SNDTIMEO for socket descriptor %d: %d(s) + %d(us)\n", clientSocket, tv.tv_sec, tv.tv_usec);
				}
				
				if (SetSocketRecvTimeout(clientSocket, 3) == ERROR)
				{
					TraceLog(ERROR_LEVEL, "Cannot set receive-timeout for AS socket %d\n", clientSocket);
				}
				
				// Update AS conf
				asCollection.connection.status = CONNECTED;
				asCollection.connection.socket = clientSocket;

				isUpdatedRawDataStatus = NOT_YET;

				// Thread ids
				pthread_t threadReceiveMsgsASTool_id;
				pthread_t threadSendMsgsToAS_id;
				
				// Create threads
				pthread_create (&threadReceiveMsgsASTool_id, 
								NULL, 
								(void *)&ReceiveMsgsASTool, 
								(void *)&asCollection.connection);

				pthread_create (&threadSendMsgsToAS_id, 
								NULL, 
								(void *)&SendMsgsToAS, 
								(void *)&asCollection.connection);

				// Waiting for create thread completed
				sleep(1);
				
			}
		}
		
		// Close the socket
		shutdown(serverSocket, 2);
		close(serverSocket);
	}
	
    return NULL;
}

/****************************************************************************
- Function name:	SendMsgsToAS
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
void *SendMsgsToAS(void *pConnection)
{
	SetKernelAlgorithm("[SEND_MSGS_TO_AS]");
	t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;
	
	// Delay 1 second to receive raw data indexs from AS
	sleep(1);

	// Send PM Books configuration to AS	
	if (SendPMAllBookConfToAS(connection) == ERROR)
	{
		CloseConnection(connection);

		return NULL;
	}
		
	// Send PM Orders configuration to AS
	if (SendPMAllOrderConfToAS(connection) == ERROR)
	{
		CloseConnection(connection);

		return NULL;
	}

	// Send PM global settings to AS
	if (SendPMGlobalConfToAS(connection) == ERROR)
	{
		CloseConnection(connection);

		return NULL;
	}

	// Send Exchange conf to AS
	if (SendExchangeConfToAS(connection) == ERROR)
	{
		CloseConnection(connection);

		return NULL;
	}
	

	// Send ignore stock list to AS
	if (SendIgnoreStockListToAS(connection) == ERROR)
	{
		CloseConnection(connection);

		return NULL;
	}

	//Send halted stock list
	if (SendHaltStockListToAS(connection) == ERROR)
	{
		CloseConnection(connection);

		return NULL;
	}
	
	while (1)
	{
		// Check connection between AS and PM
		if ((connection->status == DISCONNECTED) || (connection->socket == -1))
		{
			return NULL;
		}

		// Send PM current status to AS
		if (SendPMCurrentStatusToAS(connection) == ERROR)
		{
			CloseConnection(connection);

			return NULL;
		}

		if (isUpdatedRawDataStatus == UPDATED)
		{
			// Send raw data to AS
			if (SendAllRawDataToAS(connection) == ERROR)
			{
				CloseConnection(connection);

				return NULL;
			}
		}
		
		// Send sleeping symbol list to AS
		if (SendPMSleepingSymbolsToAS() == ERROR)
		{
			CloseConnection(connection);

			return NULL;
		}

		// TT send delay in in microseconds
		usleep(AS_SEND_DELAY);
	}

	return NULL;
}

/****************************************************************************
- Function name:	SendPMAllOrderConfToAS
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int SendPMAllOrderConfToAS(void *pConnection)
{
	t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

	int ECNIndex, result;

	for (ECNIndex = 0; ECNIndex < MAX_ORDER_CONNECTIONS; ECNIndex++)
	{
		// Send PM Order configuration message to Send PM
		result = SendPMOrderConfToAS(connection, ECNIndex);

		if (result == ERROR)
		{
			return result;
		}
	}
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	SendPMOrderConfToAS
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int SendPMOrderConfToAS(void *pConnection, int ECNIndex)
{
	t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

	// Check connection between AS and PM
	if ((connection->status == DISCONNECTED) || (connection->socket == -1))
	{
		return ERROR;
	}

	TraceLog(DEBUG_LEVEL, "Send PM order conf to AS, ECNIndex = %d ...\n", ECNIndex);

	int result;
	t_ASMessage asMessage;
	
	// Build PM Order configuration message to Send PM
	BuildASMessageForPMOrderConf(&asMessage, ECNIndex);

	// Send message to AS
	result = SendASMessage(connection, &asMessage);

	if (result == ERROR)
	{
		TraceLog(ERROR_LEVEL, "Cannot Send PM Order configuration to AS, socket = %d\n", connection->socket);
	}
	
	return result;
}

/****************************************************************************
- Function name:	SendPMGlobalConfToAS
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int SendPMGlobalConfToAS(void *pConnection)
{
	t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

	// Check connection between AS and PM
	if ((connection->status == DISCONNECTED) || (connection->socket == -1))
	{
		return ERROR;
	}

	TraceLog(DEBUG_LEVEL, "Send PM global conf to AS ...\n");

	int result;
	t_ASMessage asMessage;

	// Build Aggregation Sever global configuration message to Send PM
	BuildASMessageForPMGlobalConf(&asMessage);

	// Send message to AS
	result = SendASMessage(connection, &asMessage);

	if (result == ERROR)
	{
		TraceLog(ERROR_LEVEL, "Cannot Send PM global configuration to AS, socket = %d\n", connection->socket);
	}
	
	return result;
}

/****************************************************************************
- Function name:	SendIgnoreStockListToAS
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int SendIgnoreStockListToAS(void *pConnection)
{
	t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

	// Check connection between AS and PM
	if ((connection->status == DISCONNECTED) || (connection->socket == -1))
	{
		return ERROR;
	}

	TraceLog(DEBUG_LEVEL, "Send ignored stock list to AS ...\n");

	int result;
	t_ASMessage asMessage;

	// Build ignore stock list message to Send PM
	BuildASMessageForIgnoreStockList(&asMessage);

	// Send message to AS
	result = SendASMessage(connection, &asMessage);

	if (result == ERROR)
	{
		TraceLog(ERROR_LEVEL, "Cannot send irnoged symbol list to AS, socket = %d\n", connection->socket);
	}
	
	return result;
}

/****************************************************************************
- Function name:	SendPMCurrentStatusToAS
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int SendPMCurrentStatusToAS(void *pConnection)
{
	t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

	// Check connection between AS and PM
	if ((connection->status == DISCONNECTED) || (connection->socket == -1))
	{
		return ERROR;
	}

	int result;
	t_ASMessage asMessage;
	
	// Build AS current status message to Send PM 
	BuildASMessageForPMCurrentStatus(&asMessage);

	// Send message to AS
	result = SendASMessage(connection, &asMessage);

	if (result == ERROR)
	{
		TraceLog(ERROR_LEVEL, "Cannot Send PM current status to AS, socket = %d\n", connection->socket);
	}
	
	return result;
}

/****************************************************************************
- Function name:	ReceiveMsgsASTool
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
void *ReceiveMsgsASTool(void *pConnection)
{
	SetKernelAlgorithm("[PROCESS_AS_MSGS]");
	
	t_ASMessage asMessage;

	t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

	while (1) 
	{		
		// Check connection between AS and PM
		if ((connection->status == DISCONNECTED) || (connection->socket == -1))
		{
			break;
		}

		// Receive a message from AS
		if (ReceiveASMessage(connection->socket, &asMessage) != ERROR)
		{			
			// Message type
			int msgType = GetShortNumber(asMessage.msgContent);

			switch (msgType)
			{
				// Receive heartbeat message
				case MSG_TYPE_FOR_RECEIVE_HEARTBEAT_FROM_AS:
					break;

				// Receive Long/Short from AS
				case MSG_TYPE_FOR_RECEIVE_OPEN_POSITION_FROM_AS: 
					break;

				// Receive request enable PM
				case MSG_TYPE_FOR_REQUEST_ENABLE_PM_FROM_AS:
					ProcessASMessageForEnablePMFromAS(&asMessage);
					break;

				// Receive request disable PM
				case MSG_TYPE_FOR_REQUEST_DISABLE_PM_FROM_AS:
					ProcessASMessageForDisablePMFromAS(&asMessage);
					break;

				// Receive request PM connect to a ECN Book
				case MSG_TYPE_FOR_REQUEST_CONNECT_A_PM_BOOK_FROM_AS:
					ProcessASMessageForPMConnectECNBookFromAS(&asMessage);
					break;

				// Receive request PM disconnect to a ECN Book
				case MSG_TYPE_FOR_REQUEST_DISCONNECT_A_PM_BOOK_FROM_AS:
					ProcessASMessageForPMDisconnectECNBookFromAS(&asMessage);
					break;

				// Receive request PM connect to a ECN order
				case MSG_TYPE_FOR_REQUEST_CONNECT_A_PM_ORDER_FROM_AS:
					ProcessASMessageForPMConnectECNOrderFromAS(&asMessage);
					break;

				// Receive request PM disconnect to a ECN order
				case MSG_TYPE_FOR_REQUEST_DISCONNECT_A_PM_ORDER_FROM_AS:
					ProcessASMessageForPMDisconnectECNOrderFromAS(&asMessage);
					break;

				// Receive request PM connect to CTS
				case MSG_TYPE_FOR_REQUEST_CONNECT_CTS_FROM_AS:
					ProcessASMessageForPMConnectCTSFromAS(&asMessage);
					break;

				// Receive request PM disconnect to CTS
				case MSG_TYPE_FOR_REQUEST_DISCONNECT_CTS_FROM_AS:
					ProcessASMessageForPMDisconnectCTSFromAS(&asMessage);
					break;				

				// Receive request PM connect to UTDF
				case MSG_TYPE_FOR_REQUEST_CONNECT_UTDF_FROM_AS:
					ProcessASMessageForPMConnectUTDFFromAS(&asMessage);
					break;

				// Receive request PM disconnect to UTDF
				case MSG_TYPE_FOR_REQUEST_DISCONNECT_UTDF_FROM_AS:
					ProcessASMessageForPMDisconnectUTDFFromAS(&asMessage);
					break;
				
				// Receive request PM connect to CQS
				case MSG_TYPE_FOR_REQUEST_CONNECT_CQS_FROM_AS:
					ProcessASMessageForPMConnectCQSFromAS(&asMessage);
					break;

				// Receive request PM disconnect to CQS
				case MSG_TYPE_FOR_REQUEST_DISCONNECT_CQS_FROM_AS:
					ProcessASMessageForPMDisconnectCQSFromAS(&asMessage);
					break;

				// Receive request PM connect to UQDF
				case MSG_TYPE_FOR_REQUEST_CONNECT_UQDF_FROM_AS:
					ProcessASMessageForPMConnectUQDF_FromAS(&asMessage);
					break;

				// Receive request PM disconnect to UQDF
				case MSG_TYPE_FOR_REQUEST_DISCONNECT_UQDF_FROM_AS:
					ProcessASMessageForPMDisconnectUQDF_FromAS(&asMessage);
					break;

				// Receive set AS configuration message
				case MSG_TYPE_FOR_SET_PM_CONF_TO_AS:
					ProcessASMessageForSetASConf(&asMessage);
					break;
				
				// Receive set PM BOOK configuration
				case MSG_TYPE_FOR_SET_PM_BOOK_CONF_FROM_AS:
					ProcessASMessageForSetPMBook(&asMessage);
					break;
				
				// Receive set PM Order configuration
				case MSG_TYPE_FOR_SET_PM_ORDER_CONF_FROM_AS:
					ProcessASMessageForSetPMOrder(&asMessage);
					break;

				// Receive set PM global configuration message
				case MSG_TYPE_FOR_SET_PM_GLOBAL_SETTING_FROM_AS:
					ProcessASMessageForSetPMGlobalConf(&asMessage);
					break;

				// Receive ignore symbol list message
				case MSG_TYPE_FOR_SET_PM_IGNORE_SYMBOL_FROM_AS:
					ProcessASMessageForSetIgnoreStock(&asMessage);
					break;

				// Receive raw data status
				case MSG_TYPE_FOR_RECEIVE_RAW_DATA_STATUS_FROM_AS:
					ProcessASMessageForRawDataStatus(&asMessage);
					break;
				
				//Receive price update request
				case MSG_TYPE_FOR_RECEIVE_PRICE_UDPATE_REQUEST_FROM_AS:
					ProcessASMessageForPriceUpdateRequest(&asMessage);
					break;

				// Receive symbol status
				case MSG_TYPE_FOR_GET_PM_SYMBOL_STATUS_TFROM_AS:
					ProcessASMessageForGetStockStatus(&asMessage);
					break;

				case MSG_TYPE_FOR_RECEIVE_HALT_STOCK_LIST_FROM_AS:
					ProcessASMessageForHaltStockList(&asMessage);
					break;
					
				//Receive to halt a symbol (because reject order is received from ecn)
				case MSG_TYPE_FOR_RECEIVE_HALT_SYMBOL_FROM_AS:
					ProcessASMessageForHaltStock(&asMessage);
					break;
				
				// Receive get traded list of a symbol
				case MSG_TYPE_FOR_GET_TRADED_LIST_FROM_AS:
					ProcessASMessageForGetTradedList(&asMessage);
					break;

				// Receive stuck info
				case MSG_TYPE_FOR_RECEIVE_STUCK_FROM_AS:
					ProcessASMessageForStuckInfo(&asMessage);
					break;

				// Receive Cancel Request
				case MSG_TYPE_FOR_RECEIVE_CANCEL_REQUEST_FROM_AS:
					ProcessASMessageForCancelRequest(&asMessage);
					break;
				
				//Receive Manual New Order
				case MSG_TYPE_FOR_RECEIVE_MANUAL_ORDER_FROM_AS:
					ProcessASMessageForManualOrder(&asMessage);
					break;
				
				//Receive Manual Cancel Order
				case MSG_TYPE_FOR_RECEIVE_MANUAL_CANCEL_ORDER_FROM_AS:
					ProcessASMessageForManualCancelOrder(&asMessage);
					break;
				
				//Receive sequence number of RASH
				case MSG_TYPE_FOR_RECEIVE_RASH_SEQUENCE_NUMBER_FROM_AS:
					ProcessASMessageForRASHSeqNumber(&asMessage);
					break;

				// Receive stuck info
				case MSG_TYPE_FOR_REQUEST_DISENGAGE_SYMBOL_FROM_AS:
					ProcessASMessageForDisengageSymbol(&asMessage);
					break;

				// For Exchange Configuration
				case MSG_TYPE_FOR_SET_EXCHANGE_CONF_FROM_AS:
					ProcessSetExchangeConfFromAS(&asMessage);
					break;
				// For BBO Quotes
				case MSG_TYPE_FOR_GET_BBO_QUOTE_LIST_FROM_AS:
					ProcessGetBBOQuoteListFromAS(&asMessage);
					break;

				// For Flush BBO Quotes for a exchange
				case MSG_TYPE_FOR_REQUEST_FLUSH_BBO_QUOTES_FROM_AS:
					ProcessFlushBBOQuotesFromAS(&asMessage);
					break;
					
				// For flush PM A symbol
				case AS_REQ_FLUSH_BOOK_OF_A_SYMBOL:
					ProcessFlushBookOfASymbol(&asMessage);
					break;
					
				case AS_REQ_FLUSH_BOOK_OF_ALL_SYMBOLS:
					ProcessFlushBookOfAllSymbols(&asMessage);
					break;
				
				//For switching ARCA feed
				case AS_REQ_SET_ARCA_BOOK_FEED:
					ProcessSetArcaFeedMessage(&asMessage);
					break;
					
				// For Reload Order Config
				case MSG_TYPE_FOR_REQUEST_RELOAD_OEC_CONFIG_FROM_AS:
					ProcessASMessageForReloadConfig(&asMessage);
					break;
					
				// Invalid message
				default:
					TraceLog(ERROR_LEVEL, "Position Manager not support message from AS, msgType = %d\n", msgType);
					break;
			}
		}
		else
		{			
			TraceLog(DEBUG_LEVEL, "AS is disconnected\n");

			CloseConnection(connection);			

			break;
		}
	}

	int symbolIndex;
	for (symbolIndex = 0; symbolIndex < tradeSymbolList.countSymbol; symbolIndex++)
	{
		tradeSymbolList.symbolInfo[symbolIndex].haltStatusSent = NO;
	}
	
	// Update trading status
	asCollection.currentStatus.tradingStatus = DISABLED;

	return NULL;
}

/****************************************************************************
- Function name:	ReceiveASMessage
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ReceiveASMessage(int sock, void *message)
{
	int receivedBytes;

	t_ASMessage *asMessage = (t_ASMessage *)message;
	
	// Timeout counter
	int timeoutCounter = 0;
	
	// Current messsae body length
	int currentMsgBodyLen = 0;
	
	// Initialize message
	asMessage->msgLen = 0;
	
	/*
	Try to receive message header
	
	If cannot receive message header when reached timeout, return and report error
	
	otherwise calculate message body
	
	If message body has length 0, return Success
	
	Otherwise try to receive message body
	
	If cannot receive message body when reached timeout, return and report error
	
	Otherwise return Success
	 */
	
	// Try to receive message header
	while (asMessage->msgLen < MSG_HEADER_LEN)
	{
		receivedBytes = recv(sock, &asMessage->msgContent[asMessage->msgLen], MSG_HEADER_LEN - asMessage->msgLen, 0);

		if (receivedBytes > 0)
		{
			timeoutCounter = 0;
			
			asMessage->msgLen += receivedBytes;
		}
		else
		{
			// Check socket alive
			if ((receivedBytes == 0) && (errno == 0))
			{
				TraceLog(ERROR_LEVEL, "AS: Socket is close\n");
				
				return ERROR;
			}
			else
			{
				// Check time out
				if (timeoutCounter < 3)
				{
					timeoutCounter += 1;					
					TraceLog(DEBUG_LEVEL, "AS: Waiting. Sleep num = %d\n", timeoutCounter);
				}
				else
				{
					TraceLog(ERROR_LEVEL, "AS: TIME OUT!\n");
					
					return ERROR;
				}
			}
		}
	}
	
	// Get message body length
	currentMsgBodyLen = GetIntNumber(&asMessage->msgContent[MSG_BODY_LEN_INDEX]);

	if (currentMsgBodyLen == 0)
	{
		// Body is NULL

		return SUCCESS;
	}
	
	// Try to receive message body
	while (asMessage->msgLen < currentMsgBodyLen + MSG_HEADER_LEN)
	{
		receivedBytes = recv(sock, &asMessage->msgContent[asMessage->msgLen], currentMsgBodyLen - (asMessage->msgLen - MSG_HEADER_LEN), 0);
		
		if (receivedBytes > 0)
		{
			timeoutCounter = 0;
			asMessage->msgLen += receivedBytes;
		}
		else
		{
			if ((receivedBytes == 0) && (errno == 0))
			{
				TraceLog(ERROR_LEVEL, "AS: Socket is close\n");
				
				return ERROR;
			}
			else
			{				
				// Check time out
				if (timeoutCounter < 3)
				{
					timeoutCounter += 1;					
					TraceLog(DEBUG_LEVEL, "AS: Waiting. Sleep num = %d\n", timeoutCounter);						
				}
				else
				{
					TraceLog(ERROR_LEVEL, "AS: TIME OUT!\n");
					return ERROR;
				}
			}
		}
	}

	// Succesfully received
	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessASMessageForPMConnectECNBookFromAS
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessASMessageForPMConnectECNBookFromAS(void *message)
{
	t_ASMessage *asMessage = (t_ASMessage *)message;

	int currentIndex = MSG_HEADER_LEN;

	// Update current index
	currentIndex += 4;

	// ECN Book Index (4 bytes)
	int ECNIndex = GetIntNumber(&asMessage->msgContent[currentIndex]);

	TraceLog(DEBUG_LEVEL, "Received request to PM connect to ECN Book from AS, ECNIndex = %d ...\n", ECNIndex);

	pthread_t bookThread;
	
	switch (ECNIndex)
	{
		/*
		Process request connect to Books
		*/
		case BOOK_ARCA:
			if (bookStatusMgmt[BOOK_ARCA].isConnected == DISCONNECTED &&
				bookStatusMgmt[BOOK_ARCA].shouldConnect == NO &&
				bookStatusMgmt[BOOK_ARCA].isFlushBook == NO)
			{
				bookStatusMgmt[BOOK_ARCA].shouldConnect = YES;
				
				// Create and start thread to talk with ARCA Book server
				pthread_create(&bookThread, NULL, (void*)&ARCA_Book_Thread, NULL);
			}
			TraceLog(DEBUG_LEVEL, "Received request to start ARCA BOOK\n");
			break;
			
		case BOOK_NASDAQ:
			if (bookStatusMgmt[BOOK_NASDAQ].isConnected == DISCONNECTED &&
				bookStatusMgmt[BOOK_NASDAQ].shouldConnect == NO &&
				bookStatusMgmt[BOOK_NASDAQ].isFlushBook == NO)
			{
				bookStatusMgmt[BOOK_NASDAQ].shouldConnect = YES;
				
				// Create and start thread to talk with NASDAQ Book server
				pthread_create(&bookThread, NULL, (void*)&NDAQ_Book_Thread, NULL);
			}			
			TraceLog(DEBUG_LEVEL, "Received request to start NASDAQ BOOK\n");
			break;
		
		default:
			/*
			Some error
			*/
			return ERROR;
	}

	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessASMessageForPMDisconnectECNBookFromAS
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessASMessageForPMDisconnectECNBookFromAS(void *message)
{
	t_ASMessage *asMessage = (t_ASMessage *)message;

	int currentIndex = MSG_HEADER_LEN;

	// Update current index
	currentIndex += 4;

	// ECN Book Index (4 bytes)
	int ECNIndex = GetIntNumber(&asMessage->msgContent[currentIndex]);

	TraceLog(DEBUG_LEVEL, "Received request to PM disconnect to ECN Book from AS, ECNIndex = %d ...\n", ECNIndex);

	switch (ECNIndex)
	{
		/*
		Process request disconnect to Books 
		*/
		case BOOK_ARCA:
			if ((bookStatusMgmt[BOOK_ARCA].isConnected == CONNECTED) || (bookStatusMgmt[BOOK_ARCA].shouldConnect == YES))
			{
				DisconnectARCABook();
			}

			TraceLog(DEBUG_LEVEL, "Received request to disconnect ARCA BOOK\n");

			break;
			
		case BOOK_NASDAQ:
			if ((bookStatusMgmt[BOOK_NASDAQ].isConnected == CONNECTED) || (bookStatusMgmt[BOOK_NASDAQ].shouldConnect == YES))
			{
				DisconnectNASDAQBook();
			}
			
			TraceLog(DEBUG_LEVEL, "Received request to disconnect NASDAQ BOOK\n");

			break;
		/*
		Some error
		*/
		default:
			return ERROR;
	}

	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessASMessageForPMConnectECNOrderFromAS
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessASMessageForPMConnectECNOrderFromAS(void *message)
{
	t_ASMessage *asMessage = (t_ASMessage *)message;

	int currentIndex = MSG_HEADER_LEN;

	// Update current index
	currentIndex += 4;

	// ECN Order Index (4 bytes)
	int ECNIndex = GetIntNumber(&asMessage->msgContent[currentIndex]);

	TraceLog(DEBUG_LEVEL, "Received request to PM connect to ECN Order from AS, ECNIndex = %d ...\n", ECNIndex);

	// Call function to PM connect to ECN Order
	ProcessPMConnectECNOrder(ECNIndex);

	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessASMessageForPMDisconnectECNOrderFromAS
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessASMessageForPMDisconnectECNOrderFromAS(void *message)
{
	t_ASMessage *asMessage = (t_ASMessage *)message;

	int currentIndex = MSG_HEADER_LEN;

	// Update current index
	currentIndex += 4;

	// ECN Order Index (4 bytes)
	int ECNIndex = GetIntNumber(&asMessage->msgContent[currentIndex]);

	TraceLog(DEBUG_LEVEL, "Received request to PM disconnect to ECN Order from AS, ECNIndex = %d ...\n", ECNIndex);

	// Call function to PM disconnect to ECN Order
	ProcessPMDisconnectECNOrder(ECNIndex);

	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessASMessageForSetASConf
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessASMessageForSetASConf(void *message)
{
	t_ASMessage *asMessage = (t_ASMessage *)message;

	int currentIndex = MSG_HEADER_LEN;

	// Update current index
	currentIndex += 4;

	// Block content = ASs configuration
	memcpy((unsigned char *)&asCollection.config.port, &asMessage->msgContent[currentIndex], 4);

	// Save data to the file
	SaveASConf();

	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessASMessageForSetPMOrder
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessASMessageForSetPMOrder(void *message)
{
	t_ASMessage *asMessage = (t_ASMessage *)message;

	int currentIndex = MSG_HEADER_LEN;

	// Update current index
	currentIndex += 4;

	// Block content = Max stuck positions + Max consecutive losers
	// ECN Order Index (4 bytes)
	int ECNIndex = GetIntNumber(&asMessage->msgContent[currentIndex]);

	TraceLog(DEBUG_LEVEL, "ECNIndex = %d\n", ECNIndex);

	// Update current index
	currentIndex += 4;

	// ECN Order conf
	switch (ECNIndex)
	{
		case NASDAQ_RASH_INDEX:
			// ipAddress
			memcpy(NASDAQ_RASH_Config.ipAddress, &asMessage->msgContent[currentIndex], MAX_LINE_LEN);
			currentIndex += MAX_LINE_LEN;

			// port
			NASDAQ_RASH_Config.port = GetIntNumber(&asMessage->msgContent[currentIndex]);
			currentIndex += 4;

			// userName
			memcpy(NASDAQ_RASH_Config.userName, &asMessage->msgContent[currentIndex], MAX_LINE_LEN);
			currentIndex += MAX_LINE_LEN;

			// password
			memcpy(NASDAQ_RASH_Config.password, &asMessage->msgContent[currentIndex], MAX_LINE_LEN);
			currentIndex += MAX_LINE_LEN;
			break;
		case ARCA_DIRECT_INDEX:
			// ipAddress
			memcpy(ARCA_DIRECT_Config.ipAddress, &asMessage->msgContent[currentIndex], MAX_LINE_LEN);
			currentIndex += MAX_LINE_LEN;

			// port
			ARCA_DIRECT_Config.port = GetIntNumber(&asMessage->msgContent[currentIndex]);
			currentIndex += 4;

			// userName
			memcpy(ARCA_DIRECT_Config.userName, &asMessage->msgContent[currentIndex], MAX_LINE_LEN);
			currentIndex += MAX_LINE_LEN;

			// password
			memcpy(ARCA_DIRECT_Config.password, &asMessage->msgContent[currentIndex], MAX_LINE_LEN);
			currentIndex += MAX_LINE_LEN;
			break;
		default:
			TraceLog(ERROR_LEVEL, "Invalid message format of set PM Order conf, ECNIndex = \n", ECNIndex);
			break;
	}

	// Call function save PM Order to file
	ProcessSavePMOrderConf(ECNIndex);

	// Send PM Order configuration message to AS
	SendPMOrderConfToAS(&asCollection.connection, ECNIndex);

	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessASMessageForReloadConfig
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessASMessageForReloadConfig(void *message)
{
	//--------------------------------------------------------------------------
	//						Message structure
	//		+ Message type		2 bytes
	//		+ Body Length		4 bytes
	//		+ Block Length		4 bytes
	//		+ Config type		4 bytes
	//		+ MDC/OEC index		4 bytes (if need)
	//--------------------------------------------------------------------------
	
	t_ASMessage *asMessage = (t_ASMessage *)message;

	// Configuration type
	int type = *(int*)(&asMessage->msgContent[10]);
	
	switch (type)
	{
		case GLOBAL_CONFIGURATION:
			TraceLog(DEBUG_LEVEL, "Received request to reload global configuration\n");
			if (LoadPMGlobalConf() == ERROR)
			{
				TraceLog(ERROR_LEVEL, "LoadPMGlobalConf: ERROR\n");
				return ERROR;
			}
			
			SendPMGlobalConfToAS((void *)&asCollection.connection);
			break;
		case ALGORITHM_CONFIGURATION:
			TraceLog(DEBUG_LEVEL, "Received request to reload algorithm configuration\n");
			break;
		case MDC_CONFIGURATION:
			TraceLog(DEBUG_LEVEL, "Received request to reload MDC configuration\n");
			int bookIndex = *(int*)(&asMessage->msgContent[14]);
			switch (bookIndex)
			{
				case BOOK_ARCA:
					TraceLog(DEBUG_LEVEL, "Reload ARCA BOOK config from file\n");
					if (LoadARCA_Book_Config("arca_switch", ARCA_Book_Conf.group) == ERROR)
					{
						return ERROR;
					}
					TraceLog(DEBUG_LEVEL, "Load ARCA Book Configuration successfully\n");
					
					// Send ARCA Book configuration info
					SendPMBookConfToAS(&asCollection.connection, BOOK_ARCA);
					break;
				case BOOK_NASDAQ:
					TraceLog(DEBUG_LEVEL, "Reload NASDAQ BOOK config from file\n");
					if (LoadNASDAQ_MoldUDP_Config("nasdaq") == ERROR)
					{
						return ERROR;
					}
					TraceLog(DEBUG_LEVEL, "Load NASDAQ Book Configuration successfully\n");
					
					// Send NASDAQ Book configurtion info
					SendPMBookConfToAS(&asCollection.connection, BOOK_NASDAQ);
					break;
				default:
					TraceLog(ERROR_LEVEL, "Invalid book index %d\n", bookIndex);
					break;
			}
			break;
		case OEC_CONFIGURATION:
			TraceLog(DEBUG_LEVEL, "Received request to reload OEC configuration\n");
			int orderIndex = *(int*)(&asMessage->msgContent[14]);
			switch (orderIndex)
			{
				case NASDAQ_RASH_INDEX:
					TraceLog(DEBUG_LEVEL, "Reload NASDAQ RASH config\n");
					if (LoadNASDAQ_RASHConf("nasdaq_rash_conf", &NASDAQ_RASH_Config) == ERROR)
					{
						return ERROR;
					}
					TraceLog(DEBUG_LEVEL, "Load NASDAQ RASH Configuration successfully\n");
					
					// Send NASDAQ RASH configuration info
					SendPMOrderConfToAS((void *)&asCollection.connection, NASDAQ_RASH_INDEX);
					break;
				case ARCA_DIRECT_INDEX:
					TraceLog(DEBUG_LEVEL, "Reload ARCA DIRECT config\n");
					if (LoadARCA_DIRECTConf("arca_direct_conf", &ARCA_DIRECT_Config) == ERROR)
					{
						return ERROR;
					}
					
					TraceLog(DEBUG_LEVEL, "Load ARCA DIRECT Configuration successfully\n");
					
					// Send ARCA DIRECT configuration info
					SendPMOrderConfToAS((void *)&asCollection.connection, ARCA_DIRECT_INDEX);
					break;
				default:
					return ERROR;
			}
			break;
		case MIN_SPREAD_CONFIGURATION:
			TraceLog(DEBUG_LEVEL, "Received request to reload Min Spread configuration\n");
			break;
		case SHORTABILITY_SCHEDULE:
			TraceLog(DEBUG_LEVEL, "Received request to reload Shortability configuration\n");
			break;
		default:
			TraceLog(DEBUG_LEVEL, "Unknown configuration type %d\n", type);
			break;
	}
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessASMessageForSetPMGlobalConf
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessASMessageForSetPMGlobalConf(void *message)
{
	t_ASMessage *asMessage = (t_ASMessage *)message;

	int currentIndex = MSG_HEADER_LEN;

	// Update current index
	currentIndex += 4;

	// Block content = Max stuck positions + Max consecutive losers
	// Max stuck positions
	memcpy((unsigned char *)&asCollection.globalConf.autoSpread, &asMessage->msgContent[currentIndex], 8);
	
	// Update current index
	currentIndex += 8;

	// Maximum spread
	memcpy((unsigned char *)&asCollection.globalConf.maxSpread, &asMessage->msgContent[currentIndex], 8);

	// Update current index
	currentIndex += 8;

	// Maximum loser spread
	memcpy((unsigned char *)&asCollection.globalConf.maxLoserSpread, &asMessage->msgContent[currentIndex], 8);

	// Update current index
	currentIndex += 8;

	// Profit spread
	memcpy((unsigned char *)&asCollection.globalConf.profitSpread, &asMessage->msgContent[currentIndex], 8);

	// Update current index
	currentIndex += 8;

	// Maximum profit spread
	memcpy((unsigned char *)&asCollection.globalConf.maxProfitSpread, &asMessage->msgContent[currentIndex], 8);

	// Update current index
	currentIndex += 8;

	// Big loser amount
	memcpy((unsigned char *)&asCollection.globalConf.bigLoserAmount, &asMessage->msgContent[currentIndex], 8);

	// Update current index
	currentIndex += 8;

	// Big loser spread
	memcpy((unsigned char *)&asCollection.globalConf.bigLoserSpread, &asMessage->msgContent[currentIndex], 8);

	// Update current index
	currentIndex += 8;

	// Maximum big loser spread
	memcpy((unsigned char *)&asCollection.globalConf.maxBigLoserSpread, &asMessage->msgContent[currentIndex], 8);
	currentIndex += 8;
	
	// Fast market max spread
	memcpy((unsigned char *)&asCollection.globalConf.fm_maxSpread, &asMessage->msgContent[currentIndex], 8);
	currentIndex += 8;
	
	//Fast market max loser spread
	memcpy((unsigned char *)&asCollection.globalConf.fm_maxLoserSpread, &asMessage->msgContent[currentIndex], 8);
	currentIndex += 8;
	
	//Fast market max big loser spread
	memcpy((unsigned char *)&asCollection.globalConf.fm_maxBigLoserSpread, &asMessage->msgContent[currentIndex], 8);
	currentIndex += 8;
	
	//High Execution Rate
	memcpy((unsigned char *)&asCollection.globalConf.highExecutionRate, &asMessage->msgContent[currentIndex], 4);
	currentIndex += 4;
	
	//Moving Average Period
	memcpy((unsigned char *)&asCollection.globalConf.movingAveragePeriod, &asMessage->msgContent[currentIndex], 4);
	
	
	// Save data to the file
	SavePMGlobalConf();

	// Re-send PM global conf to AS
	SendPMGlobalConfToAS(&asCollection.connection);

	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessASMessageForSetIgnoreStock
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessASMessageForSetIgnoreStock(void *message)
{
	t_ASMessage *asMessage = (t_ASMessage *)message;

	int currentIndex = MSG_HEADER_LEN;

	// Update current index
	currentIndex += 4;

	// Block content = type(4 bytes: Add = 0, Remove = 1) + a symbol (8 bytes)
	
	// Type
	int type = GetIntNumber(&asMessage->msgContent[currentIndex]);

	// Update current index
	currentIndex += 4;

	// Symbol
	char symbol[SYMBOL_LEN];

	strncpy(symbol, (char*) &asMessage->msgContent[currentIndex], SYMBOL_LEN);

	// Update current index
	currentIndex += SYMBOL_LEN;

	char tmpSymbolList[MAX_IGNORE_SYMBOL][SYMBOL_LEN];
	memset (tmpSymbolList, 0, sizeof(tmpSymbolList));

	if (type == ADD_SYMBOL)
	{
		memcpy(tmpSymbolList, symbol, SYMBOL_LEN);
		memcpy(&tmpSymbolList[1], &IgnoreStockList.stockList, IgnoreStockList.countStock * SYMBOL_LEN);

		// Update PM ignored symbol list
		IgnoreStockList.countStock += 1;
		memcpy(&IgnoreStockList.stockList, &tmpSymbolList, IgnoreStockList.countStock * SYMBOL_LEN);

		UpdateStatusSymbolIgnored(symbol, DISABLE);

		ProcessDisengageSymbol(symbol, TRADER_REQUEST);
	}
	else if (type == REMOVE_SYMBOL)
	{
		int i, countSymbol = 0;

		for (i = 0; i < IgnoreStockList.countStock; i++)
		{
			if (strncmp(IgnoreStockList.stockList[i], symbol, SYMBOL_LEN) != 0)
			{
				memcpy(tmpSymbolList[countSymbol], IgnoreStockList.stockList[i], SYMBOL_LEN);
				countSymbol ++;
			}
		}

		// Update ignored symbol list
		IgnoreStockList.countStock = 0;

		for (i = 0; i < countSymbol; i++)
		{
			memcpy(IgnoreStockList.stockList[IgnoreStockList.countStock], tmpSymbolList[i], SYMBOL_LEN);
			IgnoreStockList.countStock ++;
		}

		UpdateStatusSymbolIgnored(symbol, ENABLE);
	}
	else
	{
		TraceLog(ERROR_LEVEL, "Invalid set symbol ignore, symbol = %.8s, type = %d\n", symbol, type);

		return ERROR;
	}

	// Save data to the file
	SaveIgnoreStockList();

	// Re-send PM ignore symbol to AS
	SendIgnoreStockListToAS(&asCollection.connection);

	return SUCCESS;
}

/****************************************************************************
- Function name:	BuildASMessageForPMOrderConf
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int BuildASMessageForPMOrderConf(void *message, int ECNIndex)
{
	t_ShortConverter shortConvert;
	t_IntConverter intConvert;

	t_ASMessage *asMessage = (t_ASMessage *)message;
	
	// Initialize message
	asMessage->msgLen = 0;
	
	// Message type
	shortConvert.value = MSG_TYPE_FOR_SEND_PM_ORDER_CONF_TO_AS;
	asMessage->msgContent[0] = shortConvert.c[0];
	asMessage->msgContent[1] = shortConvert.c[1];
	asMessage->msgLen += 2;

	// Update index of body length
	asMessage->msgLen += 4;

	// Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
	// Update index of block length
	asMessage->msgLen += 4;

	// Block content = ECN Order Index (4 bytes) + PM Order conf
	// ECN Order Index (4 bytes)
	intConvert.value = ECNIndex;
	asMessage->msgContent[asMessage->msgLen] = intConvert.c[0];
	asMessage->msgContent[asMessage->msgLen + 1] = intConvert.c[1];
	asMessage->msgContent[asMessage->msgLen + 2] = intConvert.c[2];
	asMessage->msgContent[asMessage->msgLen + 3] = intConvert.c[3];

	// Update index of body length
	asMessage->msgLen += 4;

	// PM Order configuration
	switch (ECNIndex)
	{
		case NASDAQ_RASH_INDEX:
			// ipAddress
			memcpy(&asMessage->msgContent[asMessage->msgLen], NASDAQ_RASH_Config.ipAddress, MAX_LINE_LEN);
			asMessage->msgLen += MAX_LINE_LEN;

			// port
			memcpy(&asMessage->msgContent[asMessage->msgLen], (unsigned char *)&NASDAQ_RASH_Config.port, 4);
			asMessage->msgLen += 4;

			// userName
			memcpy(&asMessage->msgContent[asMessage->msgLen], NASDAQ_RASH_Config.userName, MAX_LINE_LEN);
			asMessage->msgLen += MAX_LINE_LEN;

			// password
			memcpy(&asMessage->msgContent[asMessage->msgLen], NASDAQ_RASH_Config.password, MAX_LINE_LEN);
			asMessage->msgLen += MAX_LINE_LEN;
			break;
		case ARCA_DIRECT_INDEX:
			// ipAddress
			memcpy(&asMessage->msgContent[asMessage->msgLen], ARCA_DIRECT_Config.ipAddress, MAX_LINE_LEN);
			asMessage->msgLen += MAX_LINE_LEN;

			// port
			memcpy(&asMessage->msgContent[asMessage->msgLen], (unsigned char *)&ARCA_DIRECT_Config.port, 4);
			asMessage->msgLen += 4;

			// userName
			memcpy(&asMessage->msgContent[asMessage->msgLen], ARCA_DIRECT_Config.userName, MAX_LINE_LEN);
			asMessage->msgLen += MAX_LINE_LEN;

			// password
			memcpy(&asMessage->msgContent[asMessage->msgLen], ARCA_DIRECT_Config.password, MAX_LINE_LEN);
			asMessage->msgLen += MAX_LINE_LEN;
			break;
	}

	// Block length field
	intConvert.value = (3 * MAX_LINE_LEN + 4) + 8;
	asMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
	asMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
	asMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
	asMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
	
	// Body length field
	intConvert.value = asMessage->msgLen - MSG_HEADER_LEN;
	asMessage->msgContent[2] = intConvert.c[0];
	asMessage->msgContent[3] = intConvert.c[1];
	asMessage->msgContent[4] = intConvert.c[2];
	asMessage->msgContent[5] = intConvert.c[3];

	return asMessage->msgLen;
}

/****************************************************************************
- Function name:	BuildASMessageForPMGlobalConf
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int BuildASMessageForPMGlobalConf(void *message)
{
	t_ShortConverter shortConvert;
	t_IntConverter intConvert;

	t_ASMessage *asMessage = (t_ASMessage *)message;
	
	// Initialize message
	asMessage->msgLen = 0;
	
	// Message type
	shortConvert.value = MSG_TYPE_FOR_SEND_PM_GLOBAL_SETTING_TO_AS;
	asMessage->msgContent[0] = shortConvert.c[0];
	asMessage->msgContent[1] = shortConvert.c[1];
	asMessage->msgLen += 2;

	// Update index of body length
	asMessage->msgLen += 4;

	// Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
	// Update index of block length
	asMessage->msgLen += 4;

	// Block content = Max stuck positions + Max consecutive losers
	// Auto spread
	memcpy(&asMessage->msgContent[asMessage->msgLen], (unsigned char *)&asCollection.globalConf.autoSpread, 8);
	asMessage->msgLen += 8;

	// Maximum spread
	memcpy(&asMessage->msgContent[asMessage->msgLen], (unsigned char *)&asCollection.globalConf.maxSpread, 8);
	asMessage->msgLen += 8;

	// Maximum loser spread
	memcpy(&asMessage->msgContent[asMessage->msgLen], (unsigned char *)&asCollection.globalConf.maxLoserSpread, 8);
	asMessage->msgLen += 8;

	// Profit spread
	memcpy(&asMessage->msgContent[asMessage->msgLen], (unsigned char *)&asCollection.globalConf.profitSpread, 8);
	asMessage->msgLen += 8;

	// Maximum profit spread
	memcpy(&asMessage->msgContent[asMessage->msgLen], (unsigned char *)&asCollection.globalConf.maxProfitSpread, 8);
	asMessage->msgLen += 8;

	// Big loser amount
	memcpy(&asMessage->msgContent[asMessage->msgLen], (unsigned char *)&asCollection.globalConf.bigLoserAmount, 8);
	asMessage->msgLen += 8;

	// Big loser spread
	memcpy(&asMessage->msgContent[asMessage->msgLen], (unsigned char *)&asCollection.globalConf.bigLoserSpread, 8);
	asMessage->msgLen += 8;

	// Maximum big loser spread
	memcpy(&asMessage->msgContent[asMessage->msgLen], (unsigned char *)&asCollection.globalConf.maxBigLoserSpread, 8);
	asMessage->msgLen += 8;
	
	//Fast market maxspread
	memcpy(&asMessage->msgContent[asMessage->msgLen], (unsigned char *)&asCollection.globalConf.fm_maxSpread, 8);
	asMessage->msgLen += 8;
	
	//fast market max loser spread
	memcpy(&asMessage->msgContent[asMessage->msgLen], (unsigned char *)&asCollection.globalConf.fm_maxLoserSpread, 8);
	asMessage->msgLen += 8;
	
	//fast market max big loser spread
	memcpy(&asMessage->msgContent[asMessage->msgLen], (unsigned char *)&asCollection.globalConf.fm_maxBigLoserSpread, 8);
	asMessage->msgLen += 8;
	
	//High Execution Rate
	memcpy(&asMessage->msgContent[asMessage->msgLen], (unsigned char *)&asCollection.globalConf.highExecutionRate, 4);
	asMessage->msgLen += 4;
	
	//Moving Average Period
	memcpy(&asMessage->msgContent[asMessage->msgLen], (unsigned char *)&asCollection.globalConf.movingAveragePeriod, 4);
	asMessage->msgLen += 4;

	// Block length field
	intConvert.value = 20;
	asMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
	asMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
	asMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
	asMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
	
	// Body length field
	intConvert.value = asMessage->msgLen - MSG_HEADER_LEN;
	asMessage->msgContent[2] = intConvert.c[0];
	asMessage->msgContent[3] = intConvert.c[1];
	asMessage->msgContent[4] = intConvert.c[2];
	asMessage->msgContent[5] = intConvert.c[3];

	return asMessage->msgLen;
}

/****************************************************************************
- Function name:	BuildASMessageForIgnoreStockList
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int BuildASMessageForIgnoreStockList(void *message)
{
	t_ShortConverter shortConvert;
	t_IntConverter intConvert;

	t_ASMessage *asMessage = (t_ASMessage *)message;
	
	// Initialize message
	asMessage->msgLen = 0;
	
	// Message type
	shortConvert.value = MSG_TYPE_FOR_SEND_PM_IGNORE_SYMBOL_TO_AS;
	asMessage->msgContent[0] = shortConvert.c[0];
	asMessage->msgContent[1] = shortConvert.c[1];
	asMessage->msgLen += 2;

	// Update index of body length
	asMessage->msgLen += 4;

	// Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
	// Update index of block length
	asMessage->msgLen += 4;

	// Block content = N * symbol
	memcpy(&asMessage->msgContent[asMessage->msgLen], &IgnoreStockList.stockList, IgnoreStockList.countStock * SYMBOL_LEN);
	asMessage->msgLen += IgnoreStockList.countStock * SYMBOL_LEN;

	// Block length field
	intConvert.value = IgnoreStockList.countStock * SYMBOL_LEN + 4;
	asMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
	asMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
	asMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
	asMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
	
	// Body length field
	intConvert.value = asMessage->msgLen - MSG_HEADER_LEN;
	asMessage->msgContent[2] = intConvert.c[0];
	asMessage->msgContent[3] = intConvert.c[1];
	asMessage->msgContent[4] = intConvert.c[2];
	asMessage->msgContent[5] = intConvert.c[3];

	return asMessage->msgLen;
}

/****************************************************************************
- Function name:	BuildASMessageForPMCurrentStatus
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int BuildASMessageForPMCurrentStatus(void *message)
{
	t_ShortConverter shortConvert;
	t_IntConverter intConvert;

	t_ASMessage *asMessage = (t_ASMessage *)message;
	
	// Initialize message
	asMessage->msgLen = 0;
	
	// Message type
	shortConvert.value = MSG_TYPE_FOR_SEND_PM_CURRENT_STATUS_TO_AS;
	asMessage->msgContent[0] = shortConvert.c[0];
	asMessage->msgContent[1] = shortConvert.c[1];
	asMessage->msgLen += 2;

	// Update index of body length
	asMessage->msgLen += 4;

	// Message body = blockLength (4 bytes) + blockContent (blockLength bytes)

	// Update index of block length
	asMessage->msgLen += 4;

	// Block content = AS current status + ECN order connection status

	int i;

	// Current status
	asMessage->msgContent[asMessage->msgLen] = asCollection.currentStatus.tradingStatus;
	asMessage->msgLen += 1;	

	// ECN Books connection status
	for (i = 0; i < MAX_BOOK_CONNECTIONS; i++)
	{
		asMessage->msgContent[asMessage->msgLen] = bookStatusMgmt[i].isConnected;
		asMessage->msgLen ++;
	}

	// ECN Orders connection status
	for (i = 0; i < MAX_ORDER_CONNECTIONS; i++)
	{
		asMessage->msgContent[asMessage->msgLen] = asCollection.orderConnection[i].status;
		asMessage->msgLen ++;
	}

	// CTS status
	asMessage->msgContent[asMessage->msgLen] = CTS_StatusMgmt.isConnected;
	asMessage->msgLen += 1;
	
	//CQS status
	asMessage->msgContent[asMessage->msgLen] = CQS_StatusMgmt.isConnected;
	asMessage->msgLen += 1;
	
	//UQDF status
	asMessage->msgContent[asMessage->msgLen] = UQDF_StatusMgmt.isConnected;
	asMessage->msgLen += 1;
	
	// UTDF status
	asMessage->msgContent[asMessage->msgLen] = UTDF_StatusMgmt.isConnected;
	asMessage->msgLen += 1;

	// Block length field
	intConvert.value = 1 + MAX_BOOK_CONNECTIONS + MAX_ORDER_CONNECTIONS + 2 + 4;
	asMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
	asMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
	asMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
	asMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
	
	// Body length field
	intConvert.value = asMessage->msgLen - MSG_HEADER_LEN;
	asMessage->msgContent[2] = intConvert.c[0];
	asMessage->msgContent[3] = intConvert.c[1];
	asMessage->msgContent[4] = intConvert.c[2];
	asMessage->msgContent[5] = intConvert.c[3];

	return asMessage->msgLen;
}

/****************************************************************************
- Function name:	SendASMessage
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int SendASMessage(void *pConnection, void *message)
{
	int result;

	t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;	

	t_ASMessage *asMessage = (t_ASMessage *)message;

	pthread_mutex_lock(&(connection->socketMutex));
	
	// Check connection between AS and PM
	if ((connection->status != DISCONNECTED) && (connection->socket != -1))
	{
		// Send message to AS
		result = send(connection->socket, asMessage->msgContent, asMessage->msgLen, 0);
		
		if (result != asMessage->msgLen)
		{ 		
			TraceLog(ERROR_LEVEL, "Cannot send message to AS: socket = %d, numBytesSent = %d, numBytesExpected = %d\n", connection->socket, result, asMessage->msgLen);
			
			// Close the socket
			connection->status = DISCONNECTED;
			
			shutdown(connection->socket, 2);
			close(connection->socket);
			
			// Initialize connection status
			connection->socket = -1;
			
			pthread_mutex_unlock(&(connection->socketMutex));

			return ERROR;
		}
	}
	else
	{
		pthread_mutex_unlock(&(connection->socketMutex));
		
		return ERROR;
	}

	pthread_mutex_unlock(&(connection->socketMutex));

	

	//Successfully sent
	return SUCCESS;
}

/****************************************************************************
- Function name:	SendPMAllBookConfToAS
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int SendPMAllBookConfToAS(void *pConnection)
{
	t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

	int ECNIndex, result;

	for (ECNIndex = 0; ECNIndex < MAX_BOOK_CONNECTIONS; ECNIndex++)
	{
		// Send PM Book configuration message to Send PM
		result = SendPMBookConfToAS(connection, ECNIndex);

		if (result == ERROR)
		{
			return result;
		}
	}
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	SendPMBookConfToAS
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int SendPMBookConfToAS(void *pConnection, int ECNIndex)
{
	t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

	// Check connection between AS and PM
	if ((connection->status == DISCONNECTED) || (connection->socket == -1))
	{
		return ERROR;
	}

	TraceLog(DEBUG_LEVEL, "Send PM Book conf to AS, ECNIndex = %d ...\n", ECNIndex);

	int result;
	t_ASMessage asMessage;
	
	// Build PM Book configuration message to Send PM
	BuildASMessageForPMBookConf(&asMessage, ECNIndex);

	// Send message to AS
	result = SendASMessage(connection, &asMessage);

	if (result == ERROR)
	{
		TraceLog(ERROR_LEVEL, "Cannot Send PM Book configuration to AS, socket = %d\n", connection->socket);
	}
	
	return result;
}

/****************************************************************************
- Function name:	BuildASMessageForPMBookConf
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int BuildASMessageForPMBookConf(void *message, int ECNIndex)
{
	t_ShortConverter shortConvert;
	t_IntConverter intConvert;

	t_ASMessage *asMessage = (t_ASMessage *)message;
	
	// Initialize message
	asMessage->msgLen = 0;
	
	// Message type
	shortConvert.value = MSG_TYPE_FOR_SEND_PM_BOOK_CONF_TO_AS;
	asMessage->msgContent[0] = shortConvert.c[0];
	asMessage->msgContent[1] = shortConvert.c[1];
	asMessage->msgLen += 2;

	// Update index of body length
	asMessage->msgLen += 4;

	// Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
	// Update index of block length
	asMessage->msgLen += 4;

	// Block content = ECN Book Index (4 bytes) + PM Book conf
	// ECN Book Index (4 bytes)
	intConvert.value = ECNIndex;
	asMessage->msgContent[asMessage->msgLen] = intConvert.c[0];
	asMessage->msgContent[asMessage->msgLen + 1] = intConvert.c[1];
	asMessage->msgContent[asMessage->msgLen + 2] = intConvert.c[2];
	asMessage->msgContent[asMessage->msgLen + 3] = intConvert.c[3];

	// Update index of body length
	asMessage->msgLen += 4;
	
	int size = 0;

	// PM Book configuration
	switch (ECNIndex)
	{
		case BOOK_ARCA:
			// Current Feed (A or B)
			asMessage->msgContent[asMessage->msgLen] = ARCA_Book_Conf.currentFeedName;
			asMessage->msgLen += 1;
	
			// SourceID
			memcpy(&asMessage->msgContent[asMessage->msgLen], ARCA_Book_Conf.RetransmissionRequestSource, MAX_LINE_LEN);
			asMessage->msgLen += MAX_LINE_LEN;

			int groupIndex;
			for (groupIndex = 0; groupIndex < MAX_ARCA_GROUPS; groupIndex ++)
			{
				// Retransmission Request TCP/IP
				// Index
				memcpy(&asMessage->msgContent[asMessage->msgLen], &groupIndex, 4);
				asMessage->msgLen += 4;

				// IP Address
				memcpy(&asMessage->msgContent[asMessage->msgLen], ARCA_Book_Conf.RetransmissionRequestIP, MAX_LINE_LEN);
				asMessage->msgLen += MAX_LINE_LEN;
				
				// Port
				memcpy(&asMessage->msgContent[asMessage->msgLen], &ARCA_Book_Conf.RetransmissionRequestPort, 4);
				asMessage->msgLen += 4;
			}
			
			size = 1 + MAX_LINE_LEN + MAX_ARCA_GROUPS * (MAX_LINE_LEN + 8);
			break;

		case BOOK_NASDAQ:
			// UDP multicast info
			// IP Address
			memcpy(&asMessage->msgContent[asMessage->msgLen], NDAQ_MoldUDP_Book.Data.ip, MAX_LINE_LEN);
			asMessage->msgLen += MAX_LINE_LEN;
			
			// Port
			memcpy(&asMessage->msgContent[asMessage->msgLen], &NDAQ_MoldUDP_Book.Data.port, 4);
			asMessage->msgLen += 4;			
			
			// TCP retransmission info
			// IP Address
			memcpy(&asMessage->msgContent[asMessage->msgLen], NDAQ_MoldUDP_Book.Recovery.ip, MAX_LINE_LEN);
			asMessage->msgLen += MAX_LINE_LEN;
			
			// Port
			memcpy(&asMessage->msgContent[asMessage->msgLen], &NDAQ_MoldUDP_Book.Recovery.port, 4);
			asMessage->msgLen += 4;

			size = 2 * (MAX_LINE_LEN + 4);
			break;
	}

	// Block length field
	intConvert.value = size + 8;
	asMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
	asMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
	asMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
	asMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
	
	// Body length field
	intConvert.value = asMessage->msgLen - MSG_HEADER_LEN;
	asMessage->msgContent[2] = intConvert.c[0];
	asMessage->msgContent[3] = intConvert.c[1];
	asMessage->msgContent[4] = intConvert.c[2];
	asMessage->msgContent[5] = intConvert.c[3];

	return asMessage->msgLen;
}

/****************************************************************************
- Function name:	ProcessASMessageForEnablePMFromAS
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessASMessageForEnablePMFromAS(void *message)
{
	TraceLog(DEBUG_LEVEL, "Received message to request enable PM from AS\n");

	// Update trading status
	asCollection.currentStatus.tradingStatus = ENABLED;
	
	// Check connections of MDCs & OECs
	CheckAndSendAlertDisableTrading(0);

	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessASMessageForDisablePMFromAS
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessASMessageForDisablePMFromAS(void *message)
{
	TraceLog(DEBUG_LEVEL, "Received message to request disable PM from AS\n");

	// Update trading status
	asCollection.currentStatus.tradingStatus = DISABLED;
	
	t_ASMessage *asMessage = (t_ASMessage *)message;

	short reason;
	memcpy(&reason, &asMessage->msgContent[10], 2);

  if(reason == PM_TRADING_DISABLED_BY_EMERGENCY_STOP)
  {
    // Cancel all open orders handle by PM
    int i;
    for (i = 0; i < asOpenOrder.countOpenOrder; i++)
    {
      if ( (asOpenOrder.orderSummary[i].status == orderStatusIndex.Live) || (asOpenOrder.orderSummary[i].status == orderStatusIndex.Sent) )
      {
        ProcessSendCancelRequestToECNOrder(asOpenOrder.orderSummary[i].clOrdId, asOpenOrder.orderSummary[i].ecnOrderId);
      }
    }
    
    // Cancel all open orders by manual
    for(i = 0; i < MAX_MANUAL_ORDER; i++)
    {
      SendManualCancelOrder(i, 1);
    }
  }
  
	SendPMDisabledTradingAlertToAS(reason);

	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessASMessageForRawDataStatus
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessASMessageForRawDataStatus(void *message)
{
	t_ASMessage *asMessage = (t_ASMessage *)message;

	int currentIndex = MSG_HEADER_LEN;

	// Message body = n * (blockLength (4 bytes) + blockContent (blockLength bytes))

	if ((asMessage->msgLen - MSG_HEADER_LEN) % 12 != 0)
	{
		TraceLog(ERROR_LEVEL, "Invalid received raw data status message, bodyLen = %d\n", asMessage->msgLen);
		return ERROR;
	}

	// Message body = n * (blockLength (4 bytes) + blockContent (blockLength bytes))
	int ECNIndex, countRawData;

	TraceLog(DEBUG_LEVEL, "Received raw data count from AS:\n");

	while (currentIndex < (asMessage->msgLen - MSG_HEADER_LEN))
	{	
		// Update index of block length
		currentIndex += 4;

		// Block content = ECN Order Id (1 byte) + Number of entries (4 bytes)
		// ECN Order Id
		ECNIndex = GetIntNumber(&asMessage->msgContent[currentIndex]);
		currentIndex += 4;

		// Number of entries
		countRawData = GetIntNumber(&asMessage->msgContent[currentIndex]);
		currentIndex += 4;

		TraceLog(DEBUG_LEVEL, "ECNIndex = %d, countRawData = %d\n", ECNIndex, countRawData);
		pmRawDataMgmt[ECNIndex].totalSentRawData = countRawData;
		
		if (pmRawDataMgmt[ECNIndex].totalSentRawData > pmRawDataMgmt[ECNIndex].countRawDataBlock)
		{
			TraceLog(DEBUG_LEVEL, "Resynchronize rawdata counting-index between AS and PM, from %d to %d\n", pmRawDataMgmt[ECNIndex].totalSentRawData, pmRawDataMgmt[ECNIndex].countRawDataBlock);
			pmRawDataMgmt[ECNIndex].totalSentRawData = pmRawDataMgmt[ECNIndex].countRawDataBlock;
		}
	}

	// Update status
	isUpdatedRawDataStatus = UPDATED;

	return SUCCESS;
}

/****************************************************************************
- Function name:	SendAllRawDataToAS
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int SendAllRawDataToAS(void *pConnection)
{
	t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

	int ECNIndex, result;

	for (ECNIndex = 0; ECNIndex < MAX_ORDER_CONNECTIONS; ECNIndex++)
	{
		// Send PM Order configuration message to Send PM
		result = SendRawDataToAS(connection, ECNIndex);

		if (result == ERROR)
		{
			return result;
		}
	}
	
	return SendEndOfRawDataToAS(connection);
}

/****************************************************************************
- Function name:	SendRawDataToAS
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int SendRawDataToAS(void *pConnection, int ECNIndex)
{
	t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

	// Check connection between AS and PM
	if ((connection->status == DISCONNECTED) || (connection->socket == -1))
	{
		return ERROR;
	}

	int result;
	t_ASMessage asMessage;
	
	// Build raw data message to Send PM 
	result = BuildASMessageForPMRawData(&asMessage, ECNIndex);

	if (result == 0)
	{
		return SUCCESS;
	}

	// Send message to AS
	result = SendASMessage(connection, &asMessage);

	TraceLog(DEBUG_LEVEL, "Sent raw data to AS, ecnIndex: %d, countRawDataBlock: %d\n", ECNIndex, pmRawDataMgmt[ECNIndex].countRawDataBlock);
	
	if (result == ERROR)
	{
		TraceLog(ERROR_LEVEL, "Cannot Send PM raw data to AS, socket = %d\n", connection->socket);
	}
	
	return result;
}

/****************************************************************************
- Function name:	BuildASMessageForPMRawData
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int BuildASMessageForPMRawData(void *message, int ECNIndex)
{
	t_ShortConverter shortConvert;
	t_IntConverter intConvert;

	t_ASMessage *asMessage = (t_ASMessage *)message;
	
	// Initialize message
	asMessage->msgLen = 0;
	
	// Message type
	shortConvert.value = MSG_TYPE_FOR_SEND_PM_RAW_DATA_TO_AS;
	asMessage->msgContent[0] = shortConvert.c[0];
	asMessage->msgContent[1] = shortConvert.c[1];
	asMessage->msgLen += 2;

	// Update index of body length
	asMessage->msgLen += 4;

	// Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
	// Update index of block length
	asMessage->msgLen += 4;

	// Block content = ECN Order Index (4 bytes) + PM Order conf
	// ECN Order Index (4 bytes)
	intConvert.value = ECNIndex;
	asMessage->msgContent[asMessage->msgLen] = intConvert.c[0];
	asMessage->msgContent[asMessage->msgLen + 1] = intConvert.c[1];
	asMessage->msgContent[asMessage->msgLen + 2] = intConvert.c[2];
	asMessage->msgContent[asMessage->msgLen + 3] = intConvert.c[3];

	// Update index
	asMessage->msgLen += 4;

	// Add code here
	t_RawDataMgmt *workingOrderMgmt = (t_RawDataMgmt *)&pmRawDataMgmt[ECNIndex];
	int numDataBlocks, startIndex, i, rawDataIndex, numDataBlockSend;

	pthread_mutex_lock(&workingOrderMgmt->updateMutex);
			
	// Number of data blocks need process
	numDataBlocks = workingOrderMgmt->countRawDataBlock - workingOrderMgmt->totalSentRawData;

	// Raw data index will start process
	startIndex = workingOrderMgmt->lastUpdatedIndex - numDataBlocks + 1;

	pthread_mutex_unlock(&workingOrderMgmt->updateMutex);

	if (numDataBlocks >= MAX_RAW_DATA_ENTRIES)
	{
		// Build raw data from file
		numDataBlockSend = BuildPMRawDataFromFile(asMessage, workingOrderMgmt);
	}
	else
	{
		numDataBlockSend = 0;

		for (i = 0; i < numDataBlocks; i++)
		{
			numDataBlockSend ++;

			if (numDataBlockSend >= MAX_OPEN_ORDER_SEND)
			{
				break;
			}

			rawDataIndex = (startIndex + i) % MAX_RAW_DATA_ENTRIES;

			//workingOrderMgmt->dataBlockCollection[rawDataIndex].blockLen
			intConvert.value = workingOrderMgmt->dataBlockCollection[rawDataIndex].blockLen;
			asMessage->msgContent[asMessage->msgLen] = intConvert.c[0];
			asMessage->msgContent[asMessage->msgLen + 1] = intConvert.c[1];
			asMessage->msgContent[asMessage->msgLen + 2] = intConvert.c[2];
			asMessage->msgContent[asMessage->msgLen + 3] = intConvert.c[3];

			// Update index
			asMessage->msgLen += 4;

			//workingOrderMgmt->dataBlockCollection[rawDataIndex].msgLen
			intConvert.value = workingOrderMgmt->dataBlockCollection[rawDataIndex].msgLen;
			asMessage->msgContent[asMessage->msgLen] = intConvert.c[0];
			asMessage->msgContent[asMessage->msgLen + 1] = intConvert.c[1];
			asMessage->msgContent[asMessage->msgLen + 2] = intConvert.c[2];
			asMessage->msgContent[asMessage->msgLen + 3] = intConvert.c[3];

			// Update index
			asMessage->msgLen += 4;

			//workingOrderMgmt->dataBlockCollection[rawDataIndex].msgContent
			memcpy(&asMessage->msgContent[asMessage->msgLen], workingOrderMgmt->dataBlockCollection[rawDataIndex].msgContent, workingOrderMgmt->dataBlockCollection[rawDataIndex].msgLen);

			// Update index
			asMessage->msgLen += workingOrderMgmt->dataBlockCollection[rawDataIndex].msgLen;
			
			//workingOrderMgmt->dataBlockCollection[rawDataIndex].addLen
			intConvert.value = workingOrderMgmt->dataBlockCollection[rawDataIndex].addLen;
			asMessage->msgContent[asMessage->msgLen] = intConvert.c[0];
			asMessage->msgContent[asMessage->msgLen + 1] = intConvert.c[1];
			asMessage->msgContent[asMessage->msgLen + 2] = intConvert.c[2];
			asMessage->msgContent[asMessage->msgLen + 3] = intConvert.c[3];

			// Update index
			asMessage->msgLen += 4;

			//workingOrderMgmt->dataBlockCollection[rawDataIndex].addContent
			memcpy(&asMessage->msgContent[asMessage->msgLen], workingOrderMgmt->dataBlockCollection[rawDataIndex].addContent, workingOrderMgmt->dataBlockCollection[rawDataIndex].addLen);

			// Update index
			asMessage->msgLen += workingOrderMgmt->dataBlockCollection[rawDataIndex].addLen;

			// Update totalSentRawData
			workingOrderMgmt->totalSentRawData += 1;
		}
	}
	
	// Block length field
	intConvert.value = asMessage->msgLen - MSG_HEADER_LEN;
	asMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
	asMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
	asMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
	asMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
	
	// Body length field
	intConvert.value = asMessage->msgLen - MSG_HEADER_LEN;
	asMessage->msgContent[2] = intConvert.c[0];
	asMessage->msgContent[3] = intConvert.c[1];
	asMessage->msgContent[4] = intConvert.c[2];
	asMessage->msgContent[5] = intConvert.c[3];

	return numDataBlockSend;
}

/****************************************************************************
- Function name:	SendEndOfRawDataToAS
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int SendEndOfRawDataToAS(void *pConnection)
{
	t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

	// Check connection between AS and PM
	if ((connection->status == DISCONNECTED) || (connection->socket == -1))
	{
		return ERROR;
	}

	int result;
	t_ASMessage asMessage;

	// Build raw data message to Send PM
	result = BuildASMessageForPMEndOfRawData(&asMessage);

	// Send message to AS
	result = SendASMessage(connection, &asMessage);

	if (result == ERROR)
	{
		TraceLog(ERROR_LEVEL, "Cannot Send PM raw data to AS, socket = %d\n", connection->socket);
	}

	return result;
}

/****************************************************************************
- Function name:	BuildASMessageForPMEndOfRawData
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildASMessageForPMEndOfRawData(void *message)
{
	t_ShortConverter shortConvert;
	t_IntConverter intConvert;

	t_ASMessage *asMessage = (t_ASMessage *)message;

	// Initialize message
	asMessage->msgLen = 0;

	// Message type
	shortConvert.value = MSG_TYPE_FOR_SEND_PM_END_OF_RAW_DATA_TO_AS;
	asMessage->msgContent[0] = shortConvert.c[0];
	asMessage->msgContent[1] = shortConvert.c[1];
	asMessage->msgLen += 2;

	// Update index of body length
	asMessage->msgLen += 4;

	// Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
	// Update index of block length
	asMessage->msgLen += 4;

	// Block length field
	intConvert.value = asMessage->msgLen - MSG_HEADER_LEN;
	asMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
	asMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
	asMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
	asMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];

	// Body length field
	intConvert.value = asMessage->msgLen - MSG_HEADER_LEN;
	asMessage->msgContent[2] = intConvert.c[0];
	asMessage->msgContent[3] = intConvert.c[1];
	asMessage->msgContent[4] = intConvert.c[2];
	asMessage->msgContent[5] = intConvert.c[3];

	return SUCCESS;
}

int ProcessASMessageForPriceUpdateRequest(void *message)
{
	t_ASMessage *asMessage = (t_ASMessage *)message;

	int currentIndex = MSG_HEADER_LEN;

	// Update current index
	currentIndex += 4;

	// Block content = totalSymbol (4 bytes) + Symbol List (8 * totalSymbol)
	
	int totalSymbol = GetIntNumber(&asMessage->msgContent[currentIndex]);
	currentIndex += 4;

	const int maxSymbol = totalSymbol;
	char symbolList[maxSymbol][SYMBOL_LEN];
	memcpy(symbolList, &asMessage->msgContent[currentIndex], SYMBOL_LEN * maxSymbol);
	currentIndex += SYMBOL_LEN * maxSymbol;

	// TraceLog(DEBUG_LEVEL, "Received message price update request for %d %s\n",
			// maxSymbol, maxSymbol == 1 ? "symbol":"symbols");

	SendPriceUpdateResponseToAS(maxSymbol, symbolList);

	return SUCCESS;
}

int SendPriceUpdateResponseToAS(int maxSymbol, char symbolList[][SYMBOL_LEN])
{
	t_ConnectionStatus *connection = (t_ConnectionStatus *)&asCollection.connection;

	if ((connection->status == DISCONNECTED) || (connection->socket == -1))
	{
		return ERROR;
	}

	int result;
	t_ASMessage asMessage;
	memset(&asMessage, 0, sizeof(t_ASMessage));
	
	result = BuildASMessageForPriceUpdate(&asMessage, maxSymbol, symbolList);

	if (result == 0)
	{
		return SUCCESS;
	}

	result = SendASMessage(connection, &asMessage);

	if (result == ERROR)
	{
		TraceLog(ERROR_LEVEL, "Cannot send price update message to AS, socket = %d\n", connection->socket);
	}
	
	return result;
}

int BuildASMessageForPriceUpdate(void *message, int maxSymbol, char symbolList[][SYMBOL_LEN])
{
	t_ShortConverter shortNum;
	t_IntConverter intNum;

	t_ASMessage *asMessage = (t_ASMessage *)message;
	
	// Initialize message
	asMessage->msgLen = 0;
	
	// Message type
	shortNum.value = MSG_TYPE_FOR_SEND_PRICE_UPDATE_TO_AS;
	asMessage->msgContent[0] = shortNum.c[0];
	asMessage->msgContent[1] = shortNum.c[1];
	asMessage->msgLen += 2;

	// Update index of body length
	asMessage->msgLen += 4;
	
	// Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
	// Update index of block length
	asMessage->msgLen += 4;
	
	// Block content = Num Symbol(4 bytes) + Symbol (8 bytes) + ask quote (4 bytes shares, 8 bytes price) + bid quote (12 bytes)
	
	// Number of Symbol
	int countSentSymbol = 0;
	int numSymbolIndex = asMessage->msgLen;
	asMessage->msgLen += 4;
	
	int i;
	int updateFlag = 0; // ask flag 1, bid flag 11
	char symbol[SYMBOL_LEN + 1] = "\0";
	int askShares, bidShares;
	unsigned long askPrice, bidPrice;
	int stockSymbolIndex, bookIndex;
	t_OrderDetailNode *node;
	
	for (i = 0; i < maxSymbol; i++)
	{
		memcpy(symbol, symbolList[i], SYMBOL_LEN);
		askShares = bidShares = 0;
		askPrice = bidPrice = 0;
		updateFlag = 0;
		
		stockSymbolIndex = GetStockSymbolIndex(symbol);
		if(stockSymbolIndex == -1)
		{
			// TraceLog(ERROR_LEVEL, "Symbol %s is unavailable in stock book\n", symbol);
			continue;
		}
		
		// Get ask quote
		for(bookIndex = 0; bookIndex < MAX_BOOK_CONNECTIONS; bookIndex++)
		{
			node = stockBook[bookIndex][ASK_SIDE][stockSymbolIndex]->head->forward[0];
		
			if (node)
			{
				askShares = node->info.shareVolume;
				askPrice = node->info.sharePrice * 10000;
				updateFlag = 1;
				break;
			}
			
			// if (updateFlag == 1) break;
		}
		
		// Get bid quote
		for(bookIndex = 0; bookIndex < MAX_BOOK_CONNECTIONS; bookIndex++)
		{
			node = stockBook[bookIndex][BID_SIDE][stockSymbolIndex]->head->forward[0];

			if (node)
			{
				bidShares = node->info.shareVolume;
				bidPrice = node->info.sharePrice * 10000;
				updateFlag += 10;
				break;
			}
			
			// if (updateFlag >= 10) break;
		}
		
		if (updateFlag != 0)
		{
			// Add symbol to message
			memcpy(&asMessage->msgContent[asMessage->msgLen], symbol, SYMBOL_LEN);
			asMessage->msgLen += SYMBOL_LEN;
			countSentSymbol++;
			
			// Add ask quote info
			memcpy(&asMessage->msgContent[asMessage->msgLen], &askShares, 4);
			asMessage->msgLen += 4;
			memcpy(&asMessage->msgContent[asMessage->msgLen], &askPrice, 8);
			asMessage->msgLen += 8;
			
			// Add bid quote info
			memcpy(&asMessage->msgContent[asMessage->msgLen], &bidShares, 4);
			asMessage->msgLen += 4;
			memcpy(&asMessage->msgContent[asMessage->msgLen], &bidPrice, 8);
			asMessage->msgLen += 8;
		}
	}
	
	// Update Number of Symbol to sent
	if (countSentSymbol == 0) return 0;
	memcpy(&asMessage->msgContent[numSymbolIndex], &countSentSymbol, 4);
	
	// Block length field
	intNum.value = asMessage->msgLen - MSG_HEADER_LEN;
	memcpy(&asMessage->msgContent[MSG_HEADER_LEN], intNum.c, 4);
	
	// Body length field
	intNum.value = asMessage->msgLen - MSG_HEADER_LEN;
	memcpy(&asMessage->msgContent[2], intNum.c, 4);
	
	return asMessage->msgLen;
}

/****************************************************************************
- Function name:	ProcessASMessageForGetStockStatus
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessASMessageForGetStockStatus(void *message)
{
	t_ASMessage *asMessage = (t_ASMessage *)message;

	int currentIndex = MSG_HEADER_LEN;

	// Update current index
	currentIndex += 4;

	// Block content = senderId(4 bytes) + a symbol (8 bytes)
	
	// senderId
	int senderId = GetIntNumber(&asMessage->msgContent[currentIndex]);

	// Update current index
	currentIndex += 4;

	// Symbol
	char symbol[SYMBOL_LEN];

	strncpy(symbol, (char*) &asMessage->msgContent[currentIndex], SYMBOL_LEN);
	currentIndex += SYMBOL_LEN;

	char listMdc[MAX_BOOK_CONNECTIONS];
	memcpy(listMdc, &asMessage->msgContent[currentIndex], MAX_BOOK_CONNECTIONS);
	currentIndex += MAX_BOOK_CONNECTIONS;
	
	/* TraceLog(DEBUG_LEVEL, "Received message request to get stock symbol status (%.8s) for ARCA(%d), NASDAQ(%d)\n",
				symbol, listMdc[0], listMdc[1]); */


	// Send PM Symbol Status To AS
	SendPMSymbolStatusToAS(senderId, symbol, listMdc);

	return SUCCESS;
}

/****************************************************************************
- Function name:	SendPMSymbolStatusToAS
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int SendPMSymbolStatusToAS(int ttIndex, char *stockSymbol, char *listMdc)
{
	t_ConnectionStatus *connection = (t_ConnectionStatus *)&asCollection.connection;

	// Check connection between AS and PM
	if ((connection->status == DISCONNECTED) || (connection->socket == -1))
	{
		return ERROR;
	}

	int result;
	t_ASMessage asMessage;
	
	// Build stock symbol status message to Send PM 
	result = BuildASMessageForPMSymbolStatus(&asMessage, ttIndex, stockSymbol, listMdc);

	if (result == 0)
	{
		return SUCCESS;
	}

	// Send message to AS
	result = SendASMessage(connection, &asMessage);

	if (result == ERROR)
	{
		TraceLog(ERROR_LEVEL, "Cannot send PM stock symbol status to AS, socket = %d\n", connection->socket);
	}
	
	return result;
}

/****************************************************************************
- Function name:	BuildASMessageForPMSymbolStatus
- Input:			N/A
- Output:		N/A
- Return:		Success or ERROR
- Description:	+ The routine hides the following facts
				  - [To be done] 	
				+ There are preconditions guaranteed to the routine
				+ The routine guarantees that the status value will 
					have a value of either
				  - Success
					or
				  - ERROR
- Usage:			N/A
*********************************************************************************/
int BuildASMessageForPMSymbolStatus(void *message, int ttIndex, char *stockSymbol, char *listMdc)
{
	//index of symbol in Stock Book
	int stockSymbolIndex = GetStockSymbolIndex(stockSymbol);
	
	if(stockSymbolIndex == -1)
	{
		TraceLog(ERROR_LEVEL, "stock Symbol %.8s did not exist in stock book\n", stockSymbol);
	} 	
	
	t_ShortConverter shortNum;
	t_IntConverter intNum;
	t_DoubleConverter doubleNum;

	t_ASMessage *asMessage = (t_ASMessage *)message;
	
	// Initialize message
	asMessage->msgLen = 0;
	
	// Message type
	shortNum.value = MSG_TYPE_FOR_SEND_PM_SYMBOL_STATUS_TO_AS;
	asMessage->msgContent[0] = shortNum.c[0];
	asMessage->msgContent[1] = shortNum.c[1];
	asMessage->msgLen += 2;

	// Update index of body length
	asMessage->msgLen += 4;

	// Block 1: TT and Stock symbol information
	// Change ttIndex from 4 bytes to 1 byte
	intNum.value = 13;	//Block size: 4(block len) + 1(tt) + 8(symbol)
	memcpy(&asMessage->msgContent[asMessage->msgLen], intNum.c, 4);
	asMessage->msgLen += 4;
	
	asMessage->msgContent[asMessage->msgLen] = ttIndex;
	asMessage->msgLen += 1; // change ttIndex from 4 bytes to 1 byte
	
	memcpy(&asMessage->msgContent[asMessage->msgLen], stockSymbol, SYMBOL_LEN);
	asMessage->msgLen += SYMBOL_LEN;
	
	//Other blocks
	int numOfAskEntries;
	int numOfBidEntries;
	int askBlockSize = 0;
	int bidBlockSize = 0;
	int bookIndex;
	
	t_OrderDetailNode *node;
	
	//ASK Block
	char askBlock[4 + (1 + 4 + 8) * (10 * MAX_BOOK_CONNECTIONS)];	//block len + ECN + shares + price
	int askBlockIndex = 0;
	
	if(stockSymbolIndex != -1)
	{
		for(bookIndex = 0; bookIndex < MAX_BOOK_CONNECTIONS; bookIndex++)
		{
			if (listMdc[bookIndex] != 1)
			{
				// This book is not requested
				continue;
			}
			
			node = stockBook[bookIndex][ASK_SIDE][stockSymbolIndex]->head->forward[0];
			
			for(numOfAskEntries = 0; numOfAskEntries < 10; 	numOfAskEntries++)
			{
				if (node != NULL)
				{
					// ECN ID
					askBlock[askBlockIndex] = bookIndex;
					askBlockIndex += 1;
					
					// Share
					intNum.value = node->info.shareVolume;
					memcpy(&askBlock[askBlockIndex], intNum.c, 4);
					askBlockIndex += 4;
					
					// Price
					doubleNum.value = node->info.sharePrice;
					memcpy(&askBlock[askBlockIndex], doubleNum.c, 8);
					askBlockIndex += 8;
					
					// Next node
					node = node->forward[0];
					
					askBlockSize += 1 + 4 + 8;
				}
				else
				{
					break;
				}
			}
		}
	}
	
	//BID Block
	unsigned char bidBlock[4 + (1 + 4 + 8) * (10 * MAX_BOOK_CONNECTIONS)];	//block len + ECN + shares + price
	int bidBlockIndex = 0;
	
	if(stockSymbolIndex != -1)
	{
		for(bookIndex = 0; bookIndex < MAX_BOOK_CONNECTIONS; bookIndex++)
		{
			if (listMdc[bookIndex] != 1)
			{
				// This book is not requested
				continue;
			}
			
			node = stockBook[bookIndex][BID_SIDE][stockSymbolIndex]->head->forward[0];
			
			for(numOfBidEntries = 0; numOfBidEntries < 10; 	numOfBidEntries++)
			{
				if (node != NULL)
				{
					//ECN ID
					bidBlock[bidBlockIndex] = bookIndex;
					bidBlockIndex += 1;
					
					//Share
					intNum.value = node->info.shareVolume;
					memcpy(&bidBlock[bidBlockIndex], intNum.c, 4);
					bidBlockIndex += 4;
					
					//Price
					doubleNum.value = node->info.sharePrice;
					memcpy(&bidBlock[bidBlockIndex], doubleNum.c, 8);				
					bidBlockIndex += 8;
					
					//Next node
					node = node->forward[0];
					
					bidBlockSize += 1 + 4 + 8;	
				}
				else
				{
					break;
				}
			}
		}
	}
	
	//Block Length 2
	intNum.value = askBlockSize + 4;
	memcpy(&asMessage->msgContent[asMessage->msgLen], intNum.c, 4);
	asMessage->msgLen += 4;
	
	//ASK BLOCK
	memcpy(&asMessage->msgContent[asMessage->msgLen], askBlock, askBlockSize);
	asMessage->msgLen += askBlockSize;
	
	//Block Length 3
	intNum.value = bidBlockSize + 4;
	memcpy(&asMessage->msgContent[asMessage->msgLen], intNum.c, 4);
	asMessage->msgLen += 4;
	
	//BID BLOCK
	memcpy(&asMessage->msgContent[asMessage->msgLen], bidBlock, bidBlockSize);
	asMessage->msgLen += bidBlockSize;
	
	// Update Body Length
	intNum.value = 13 + askBlockSize + 4 + bidBlockSize + 4; // change ttIndex from 4 bytes to 1 byte
	memcpy(&asMessage->msgContent[2], intNum.c, 4);
	
	return asMessage->msgLen;
}

/****************************************************************************
- Function name:	ProcessASMessageForStuckInfo
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessASMessageForStuckInfo(void *message)
{
	t_ASMessage *asMessage = (t_ASMessage *)message;

	int currentIndex = MSG_HEADER_LEN;

	// Update current index
	currentIndex += 4;

	// Block content = SYMBOL(8 bytes) + CrossID(4 bytes) + NUMBER OF SHARES (4 bytes) + STUCK PRICE (8 bytes) + SIDE (1 byte)
	
	t_Position stuckInfo;

	// SYMBOL(8 bytes)
	strncpy(stuckInfo.symbol, (char*) &asMessage->msgContent[currentIndex], SYMBOL_LEN);
	currentIndex += SYMBOL_LEN;

	//CROSS ID (4 bytes)
	int crossId = GetIntNumber(&asMessage->msgContent[currentIndex]);
	currentIndex += 4;
	
	// NUMBER OF SHARES (4 bytes)
	stuckInfo.shares = GetIntNumber(&asMessage->msgContent[currentIndex]);
	currentIndex += 4;

	// STUCK PRICE (8 bytes)
	stuckInfo.price = GetDoubleNumber(&asMessage->msgContent[currentIndex]);
	currentIndex += 8;
	
	// Reference price (8 bytes)
	stuckInfo.referencePrice = GetDoubleNumber(&asMessage->msgContent[currentIndex]);
	currentIndex += 8;
	
	// SIDE (1 byte)
	stuckInfo.side = asMessage->msgContent[currentIndex];
	currentIndex ++;
	
	// Account (1 byte)
	stuckInfo.accountSuffix = asMessage->msgContent[currentIndex];
	currentIndex ++;

	TraceLog(WARN_LEVEL, "Received stuck info from AS: account = %d, symbol = %.8s, side = %d, shares = %d, price = %lf, reference Price = %lf, corssId = %d\n", 
			stuckInfo.accountSuffix, stuckInfo.symbol, stuckInfo.side, stuckInfo.shares, stuckInfo.price, stuckInfo.referencePrice, crossId);

	// Detect quote delay in microseconds
	//usleep(DETECT_QUOTE_DELAY);

	if (CheckOpenPosition(&stuckInfo) == SUCCESS)
	{
		if(crossId <= 0)
		{
			stuckInfo.price = 0.0;
		}
		
		// Check and Place Order To ECN Orders
		CheckAndPlaceOrder(&stuckInfo, crossId, NO, NO, -1);
	}

	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessASMessageForManualOrder
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessASMessageForManualOrder(void *message)
{
	t_ASMessage *asMessage = (t_ASMessage *)message;

	t_ManualOrder newOrder;
	int currentIndex = MSG_HEADER_LEN;

	// Update current index to bypass Block Length
	currentIndex += 4;

	//Symbol
	strncpy(newOrder.symbol, (char *)&asMessage->msgContent[currentIndex], SYMBOL_LEN);
	currentIndex += SYMBOL_LEN;
	
	//Side
	newOrder.side = GetIntNumber(&asMessage->msgContent[currentIndex]);
	currentIndex += 4;
	
	//shares
	newOrder.shares = GetIntNumber(&asMessage->msgContent[currentIndex]);
	currentIndex += 4;
	
	//maxFloor
	newOrder.maxFloor = GetIntNumber(&asMessage->msgContent[currentIndex]);
	currentIndex += 4;
	
	//price
	newOrder.price = GetDoubleNumber(&asMessage->msgContent[currentIndex]);
	currentIndex += 8;
	
	//ECNId
	newOrder.ECNId = GetIntNumber(&asMessage->msgContent[currentIndex]);
	currentIndex += 4;
	
	//Tif
	newOrder.tif = GetIntNumber(&asMessage->msgContent[currentIndex]);
	currentIndex += 4;
	
	//SendId
	newOrder.sendId = GetIntNumber(&asMessage->msgContent[currentIndex]);
	currentIndex += 4;
	
	//Peg difference
	newOrder.pegDef = GetDoubleNumber(&asMessage->msgContent[currentIndex]);
	currentIndex += 8;
	
	//Order ID
	newOrder.orderId = GetIntNumber(&asMessage->msgContent[currentIndex]);
	currentIndex += 4;
	
	// Account
	newOrder.accountSuffix = asMessage->msgContent[currentIndex];
	currentIndex += 1;
	
	//isForced flag
	newOrder.isForced = GetIntNumber(&asMessage->msgContent[currentIndex]);
	currentIndex += 4;
	
	//ttIndex
	newOrder.ttIndex = GetIntNumber(&asMessage->msgContent[currentIndex]);
	currentIndex += 4;
	
	//RouteDest (for RASH Order only)
	memcpy(newOrder.routeDest, &asMessage->msgContent[currentIndex], 4);
	currentIndex += 4;
	
	TraceLog(DEBUG_LEVEL, "Received request to send manual order from AS\n");
	TraceLog(DEBUG_LEVEL, "\tOrder ID: %d, Account: %d, Symbol: %.8s, Shares: %d, Price: %lf, Side: %d, MaxFloor: %d, TIF: %d, ECN ID: %d, Peg Difference: %lf, From TT index: %d, Send ID (from TT): %d, Forced?: %d\n",
		newOrder.orderId, newOrder.accountSuffix, newOrder.symbol, newOrder.shares, newOrder.price, newOrder.side, newOrder.maxFloor, newOrder.tif,
		newOrder.ECNId, newOrder.pegDef, newOrder.ttIndex, newOrder.sendId, newOrder.isForced);
	
	if (newOrder.isForced == 0)
	{
		int ret = CheckManualOrderPriceWithLastTrade(&newOrder);
		if (ret == 1)	//Price is Valid or fast market mode
		{
			if (SendManualOrder(&newOrder) == SUCCESS)
			{
				SendManualOrderResponseToAS(&newOrder, MANUAL_ORDER_RESPONSE_SENT);
			}
			else
			{
				//AS will check if it can send
				SendManualOrderResponseToAS(&newOrder, MANUAL_ORDER_RESPONSE_FAILED_TO_SEND);
			}
		}
		else //Price is Invalid --> cannot send
		{
			SendManualOrderResponseToAS(&newOrder, MANUAL_ORDER_RESPONSE_FAILED_TO_CHECK);
		}
	}
	else	//isForced == 1, send manual order now
	{
		if (SendManualOrder(&newOrder) == SUCCESS)
		{
			SendManualOrderResponseToAS(&newOrder, MANUAL_ORDER_RESPONSE_SENT);
		}
		else
		{
			//AS will check if it can send
			SendManualOrderResponseToAS(&newOrder, MANUAL_ORDER_RESPONSE_FAILED_TO_SEND);
		}
	}
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessASMessageForManualCancelOrder
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessASMessageForManualCancelOrder(void *message)
{
	t_ASMessage *asMessage = (t_ASMessage *)message;

	t_ManualCancelOrder cancelOrder;
	int currentIndex = MSG_HEADER_LEN;

	// Update current index to bypass Block Length
	currentIndex += 4;

	//EcnID
	int ECNId = GetIntNumber(&asMessage->msgContent[currentIndex]);
	currentIndex += 4;
	
	//order token
	memcpy(cancelOrder.orderToken, &asMessage->msgContent[currentIndex], 16);
	currentIndex += 16;
	
	//order quantity
	cancelOrder.orderQty = GetIntNumber(&asMessage->msgContent[currentIndex]);
	currentIndex += 4;
	
	//ECN Order ID
	char _ecnOrderID[25];
	memcpy(_ecnOrderID, &asMessage->msgContent[currentIndex], 24);
	_ecnOrderID[20] = 0;
	currentIndex += 24;
	cancelOrder.ecnOrderID = atol(_ecnOrderID);
	
	//Symbol
	memcpy(cancelOrder.symbol, &asMessage->msgContent[currentIndex], SYMBOL_LEN);
	currentIndex += SYMBOL_LEN;
	
	//origClOrdID
	char _origClOrdID[21];
	memcpy(_origClOrdID, &asMessage->msgContent[currentIndex], 20);
	_origClOrdID[20] = 0;
	currentIndex += 20;
	cancelOrder.origClOrdID = atol(_origClOrdID);
	
	//newClOrdID
	memcpy(cancelOrder.newClOrdID, &asMessage->msgContent[currentIndex], 20);
	currentIndex += 20;
	
	//side
	cancelOrder.side = asMessage->msgContent[currentIndex];
	
	TraceLog(DEBUG_LEVEL, "Received request to send manual cancel order from AS\n");
	
	ProcessSendManualCancelOrder(&cancelOrder, ECNId);
	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessASMessageForRASHSeqNumber
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessASMessageForRASHSeqNumber(void *message)
{
	t_ASMessage *asMessage = (t_ASMessage *)message;

	int currentIndex = MSG_HEADER_LEN;

	// Update current index to bypass block length
	currentIndex += 4;

	// RASH Sequence number
	int seqNumber = GetIntNumber(&asMessage->msgContent[currentIndex]);
		
	TraceLog(DEBUG_LEVEL, "Received RASH sequence number (%d) from AS\n", seqNumber);

	if (NASDAQ_RASH_Config.incomingSeqNum < seqNumber)
	{
		NASDAQ_RASH_Config.incomingSeqNum = seqNumber;
		SaveNASDAQ_RASH_SeqNumToFile();
	}
	else if (NASDAQ_RASH_Config.incomingSeqNum > seqNumber)
	{
		// Do not update in this case.
		// PM has newer seq number, so PM will tell AS for new sequence number
		SendNASDAQ_RASH_SeqToAS();
	}
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessASMessageForCancelRequest
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessASMessageForCancelRequest(void *message)
{
	t_ASMessage *asMessage = (t_ASMessage *)message;

	int currentIndex = MSG_HEADER_LEN;

	// Update current index to bypass block length
	currentIndex += 4;

	// clOrdId
	int clOrdId = GetIntNumber(&asMessage->msgContent[currentIndex]);
	currentIndex += 4;
	
	TraceLog(DEBUG_LEVEL, "Received request to send cancel request to ECN order from AS, clOrdId = %d\n", clOrdId);

	// ECNOrderId
	char ECNOrderId[21];
	strncpy(ECNOrderId, (char *)&asMessage->msgContent[currentIndex], 20);
	ECNOrderId[20] = 0;
	currentIndex += 20;

	int indexOpenOrder = GetOrderIdIndex(clOrdId);

	// Call function to send cancel request to ECN Order
	asOpenOrder.orderSummary[indexOpenOrder].requester = TRADER_REQUEST;

	if (asOpenOrder.orderSummary[indexOpenOrder].status == orderStatusIndex.Live)
	{
		int encOrderId = atol(ECNOrderId);
		ProcessSendCancelRequestToECNOrder(clOrdId, encOrderId);
	}
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessASMessageForDisengageSymbol
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessASMessageForDisengageSymbol(void *message)
{
	t_ASMessage *asMessage = (t_ASMessage *)message;

	int currentIndex = MSG_HEADER_LEN;

	// Update current index
	currentIndex += 4;

	// Symbol
	char symbol[SYMBOL_LEN];	
	strncpy(symbol, (char *)&asMessage->msgContent[currentIndex], SYMBOL_LEN);
	currentIndex += 4;
	
	// Account
	//char account = asMessage->msgContent[currentIndex];
	//currentIndex += 1;

	TraceLog(DEBUG_LEVEL, "Received request to disengage symbol = %.8s from AS ...\n", symbol);

	// Update symbol index
	int symbolIndex = GetStockSymbolIndex(symbol);

	// If stock symbol is not in SymbolList
	if (symbolIndex == -1)
	{
		symbolIndex = UpdateStockSymbolIndex(symbol);
		if (symbolIndex == -1)
		{
			TraceLog(ERROR_LEVEL, "ProcessASMessageForDisengageSymbol: Cannot find symbol = %.8s\n", symbol);
			return ERROR;
		}
	}
	
	//If PM receive disengage action, PM will release stuck sleeping
	positionCollection[symbolIndex].releaseStuckSleeping = YES;
	
	ProcessDisengageSymbol(symbol, TRADER_REQUEST);
	
	//If requester is trader then we mark this symbol is halted by user
	symbolIndex = GetTradeSymbolIndex(symbol);
	if (symbolIndex == -1)
	{
		TraceLog(ERROR_LEVEL, "(Disengage symbol): Invalid symbol (%.8s), symbolIndex = %d\n", symbol, symbolIndex);

		return -1;
	}
	
	return SUCCESS;
}


/****************************************************************************
- Function name:	ProcessASMessageForSetPMBook
- Input:			+ message
- Output:		N/A
- Return:		Success or Failure
- Description:	+ The routine hides the following facts
				  - Process all Set book messages from AS	
				+ There are preconditions guaranteed to the routine	
				  - message must be allocated and content some value before
				+ The routine guarantees that the status value will 
					have a value of either
				  - Success or Failure	
- Usage:			N/A
****************************************************************************/
int ProcessASMessageForSetPMBook(void *message)
{
	t_ASMessage *asMessage = (t_ASMessage *)message;

	//ECN ID
	int ecnID = GetIntNumber(&asMessage->msgContent[10]);
	
	switch(ecnID)
	{
		case BOOK_ARCA:
			return ProcessSet_ARCA_Book_Configuration(asMessage->msgContent);
		case BOOK_NASDAQ:
			return ProcessSet_NASDAQ_Book_Configuration(asMessage->msgContent);
		default:
			break;
	}
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessSet_ARCA_Book_Configuration
- Input:			+ message
- Output:		N/A
- Return:		Success or Failure
- Description:	
- Usage:			N/A
****************************************************************************/
int ProcessSet_ARCA_Book_Configuration(unsigned char *message)
{
	char sourceID[80];
	int index = 14;
	
	int type = GetIntNumber(&message[index]);
	index += 4;

	char configBuff[300][1000];
	memset(configBuff, 0, 300 * 1000);
	int count = 0, i;

	char fullPath[MAX_PATH_LEN];
	
	if (ARCA_Book_Conf.currentFeedName == 'A')
	{
		GetFullConfigPath("arca.xdp.afeed", CONF_BOOK, fullPath);
	}
	else
	{
		GetFullConfigPath("arca.xdp.bfeed", CONF_BOOK, fullPath);
	}
	
	FILE *fileDesc = fopen(fullPath, "r");
	
	if (fileDesc != NULL)
	{
		while (fgets(configBuff[count], 1000, fileDesc) != NULL)
		{
			RemoveCharacters(configBuff[count], strlen(configBuff[count]));
			count++;
		}
		
		// Close file
		fclose(fileDesc);
	}
	else
	{
		TraceLog(ERROR_LEVEL, "Cannot change ARCA Book configuration\n");

		return ERROR;
	}

	if (type == UPDATE_SOURCE_ID)
	{
		memcpy(sourceID, &message[index], 80);
	
		TraceLog(DEBUG_LEVEL, "Received set message request for ARCA Book SourceID = %s\n", sourceID);

		fileDesc = fopen(fullPath, "w");
		if (fileDesc != NULL)
		{
			for (i = 0; i < count; i++)
			{
				if (strncmp(configBuff[i], ARCA_Book_Conf.RetransmissionRequestSource, strlen(ARCA_Book_Conf.RetransmissionRequestSource)) == 0)
				{
					strcpy(configBuff[i], sourceID);
				}
				
				fprintf(fileDesc, "%s\n", configBuff[i]);
			}
			
			// Close file
			fclose(fileDesc);
			
			// Update source ID to current using memory data structure
			strcpy(ARCA_Book_Conf.RetransmissionRequestSource, sourceID);
		}
		else
		{
			TraceLog(ERROR_LEVEL, "Cannot change ARCA Book configuration\n");

			return ERROR;
		}
	}
	else if (type == UPDATE_RETRANSMISSION_REQUEST)
	{
		//Now ARCA Book XDP use only 1 retransmission request server, so we do not update all groups
		//So, some of following lines of code should be removed (AS and TT needs to be updated)
		//For now, we have not changed AS and TT, so we just take what we want and ignore other things
		
		//int groupIndex = GetIntNumber(&message[index]);
		index += 4;

		// Retransmission Request TCP/IP
		memcpy(ARCA_Book_Conf.RetransmissionRequestIP, &message[index], MAX_LINE_LEN);
		index += MAX_LINE_LEN;

		ARCA_Book_Conf.RetransmissionRequestPort = GetIntNumber(&message[index]);
		index += 4;

		TraceLog(DEBUG_LEVEL, "Received set message request for ARCA Book, retranIP = %s, retranPort = %d\n", ARCA_Book_Conf.RetransmissionRequestIP, ARCA_Book_Conf.RetransmissionRequestPort);

		int found = 0;
		int pos;
		for (i = 0; i < count; i++)
		{
			if (strncmp(configBuff[i], ARCA_Book_Conf.RetransmissionRequestSource, strlen(ARCA_Book_Conf.RetransmissionRequestSource)) == 0)
			{
				found = 1;
				pos = i;
				break;
			}
		}

		if (found == 1)
		{
			strcpy(configBuff[pos - 2], ARCA_Book_Conf.RetransmissionRequestIP);

			sprintf(configBuff[pos - 1], "%d", ARCA_Book_Conf.RetransmissionRequestPort);
			
			fileDesc = fopen(fullPath, "w");
			if (fileDesc != NULL)
			{
				for (i = 0; i < count; i++)
				{
					fprintf(fileDesc, "%s\n", configBuff[i]);
				}
				
				// Close file
				fclose(fileDesc);
			}
			else
			{
				TraceLog(ERROR_LEVEL, "Cannot change ARCA Book configuration\n");

				return ERROR;
			}
		}
		else
		{
			TraceLog(ERROR_LEVEL, "Cannot set ARCA Book config\n");

			return ERROR;
		}
	}
	else
	{
		TraceLog(ERROR_LEVEL, "Set ARCA Book config, type = %d\n", type);

		return ERROR;
	}
	
	SendPMBookConfToAS(&asCollection.connection, BOOK_ARCA);
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessSetArcaFeedMessage
- Input:			+ message
- Output:			N/A
- Return:			Success or Failure
- Description:		
- Usage:			N/A
****************************************************************************/
int ProcessSetArcaFeedMessage(void *message)
{
	t_ASMessage *asMessage = (t_ASMessage *)message;
	
	char feedName = asMessage->msgContent[10];
	if ((feedName == 'A') || (feedName == 'a'))
	{
		ARCA_Book_Conf.currentFeedName = 'A';
	}
	else if ((feedName == 'B') || (feedName == 'b'))
	{
		ARCA_Book_Conf.currentFeedName = 'B';
	}
	else
	{
		ARCA_Book_Conf.currentFeedName = 'B';
		TraceLog(ERROR_LEVEL, "Received invalid ARCA feed value from TT (ASCII value: %d), assigned to feed 'B' as default\n", feedName);
	}
	
	//Save new config to file (arca_switch)
	char fullPath[MAX_PATH_LEN];
	if (GetFullConfigPath("arca_switch", CONF_BOOK, fullPath) == NULL_POINTER)
	{
		return ERROR;
	}
	
	FILE *fp = fopen(fullPath, "w+");
	if (fp == NULL)
	{
		TraceLog(ERROR_LEVEL, "Could not open file to write (%s)\n", fullPath);
		return ERROR;
	}

	fprintf(fp, "# ARCA Integrated XDP Book Feed Name to use (Correct value is a single character A or B)\n%c\n", feedName);
	fclose(fp);
	
	//Reload the config
	if (LoadARCA_Book_Config("arca_switch", ARCA_Book_Conf.group) == ERROR)
	{
		return ERROR;
	}
	
	//Send back the config to AS and TT
	SendPMBookConfToAS(&asCollection.connection, BOOK_ARCA);
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessSet_NASDAQ_Book_Configuration
- Input:			+ message
- Output:		N/A
- Return:		Success or Failure
- Description:	+ The routine hides the following facts
				  - Process all Set ARCA FAST BOOK messages from AS	
				+ There are preconditions guaranteed to the routine	
				  - message must be allocated and content some value before
				+ The routine guarantees that the status value will 
					have a value of either
				  - Success or Failure	
- Usage:			N/A
****************************************************************************/
int ProcessSet_NASDAQ_Book_Configuration(unsigned char *message)
{
	int index = 14;
	
	/*
	IP and Port
	*/
	memcpy(&NDAQ_MoldUDP_Book.Data.ip, &message[index], 80);
	index += 80;
	
	memcpy(&NDAQ_MoldUDP_Book.Data.port, &message[index], 4);
	index += 4;
	
	memcpy(&NDAQ_MoldUDP_Book.Recovery.ip, &message[index], 80);
	index += 80;
	
	memcpy(&NDAQ_MoldUDP_Book.Recovery.port, &message[index], 4);
	index += 4;

	// Update ARCA FAST Book configuration
	char fullPath[MAX_PATH_LEN];
	
	GetFullConfigPath("nasdaq", CONF_BOOK, fullPath);
	
	FILE *fileDesc = fopen(fullPath, "w");
	
	if (fileDesc == NULL)
	{
		TraceLog(ERROR_LEVEL, "Cannot open file '%s' to write\n", fullPath);
		return SUCCESS;
	}
	
	/*
	IP and Port
	*/
	fprintf(fileDesc, "%s\n", NDAQ_MoldUDP_Book.Data.NICName);
	
	fprintf(fileDesc, "%s\n", NDAQ_MoldUDP_Book.Data.ip);
	fprintf(fileDesc, "%d\n", NDAQ_MoldUDP_Book.Data.port);
	
	fprintf(fileDesc, "%s\n", NDAQ_MoldUDP_Book.Recovery.ip);
	fprintf(fileDesc, "%d\n", NDAQ_MoldUDP_Book.Recovery.port);
	
	// Close file
	fclose(fileDesc);

	SendPMBookConfToAS(&asCollection.connection, BOOK_NASDAQ);
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	SendExchangeConfToAS
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int SendExchangeConfToAS(void *pConnection)
{
	t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

	// Check connection between AS and PM
	if ((connection->status == DISCONNECTED) || (connection->socket == -1))
	{
		return ERROR;
	}

	TraceLog(DEBUG_LEVEL, "Send Exchange conf to AS ...\n");

	int result;
	t_ASMessage asMessage;
	
	// Build Exchange configuration message to send to AS
	BuildASMessageForExchangeConf(&asMessage);

	// Send message to AS
	result = SendASMessage(connection, &asMessage);

	if (result == ERROR)
	{
		TraceLog(ERROR_LEVEL, "Cannot send Exchange configuration to AS, socket = %d\n", connection->socket);
	}
	
	return result;
}

/****************************************************************************
- Function name:	BuildASMessageForExchangeConf
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int BuildASMessageForExchangeConf(void *message)
{
	t_ShortConverter shortConvert;
	t_IntConverter intConvert;

	t_ASMessage *asMessage = (t_ASMessage *)message;
	
	// Initialize message
	asMessage->msgLen = 0;
	
	// Message type
	shortConvert.value = MSG_TYPE_FOR_SEND_EXCHANGE_CONF_TO_AS;
	asMessage->msgContent[0] = shortConvert.c[0];
	asMessage->msgContent[1] = shortConvert.c[1];
	asMessage->msgLen += 2;

	// Update index of body length
	asMessage->msgLen += 4;

	// Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
	// Update index of block length
	asMessage->msgLen += 4;

	// Block content = Exchange configuration
	
	int size = 0;
	
	int i;
	for (i = 0; i < MAX_EXCHANGE; i++)
	{
		// exchangeId
		asMessage->msgContent[asMessage->msgLen] = ExchangeStatusList[i].exchangeId;
		asMessage->msgLen += 1;

		// ECN To Place
		if (ExchangeStatusList[i].ECNToPlace == TYPE_NASDAQ_RASH)
		{
			strncpy((char*)&asMessage->msgContent[asMessage->msgLen], "RASH", MAX_LINE_LEN);
		}
		else
		{
			strncpy((char*) &asMessage->msgContent[asMessage->msgLen], "ARCA_DIRECT", MAX_LINE_LEN);
		}
		asMessage->msgLen += MAX_LINE_LEN;

		// CQS_Status{visible + Status}
		asMessage->msgContent[asMessage->msgLen] = ExchangeStatusList[i].status[CQS_SOURCE].visible;
		asMessage->msgContent[asMessage->msgLen + 1] = ExchangeStatusList[i].status[CQS_SOURCE].enable;
		asMessage->msgLen += 2;

		// UQDF_Status{visible + Status}
		asMessage->msgContent[asMessage->msgLen] = ExchangeStatusList[i].status[UQDF_SOURCE].visible;
		asMessage->msgContent[asMessage->msgLen + 1] = ExchangeStatusList[i].status[UQDF_SOURCE].enable;
		asMessage->msgLen += 2;
	}

	size = MAX_EXCHANGE * (1 + MAX_LINE_LEN + 2 + 2);

	// Block length field
	intConvert.value = size + 4;
	asMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
	asMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
	asMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
	asMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
	
	// Body length field
	intConvert.value = asMessage->msgLen - MSG_HEADER_LEN;
	asMessage->msgContent[2] = intConvert.c[0];
	asMessage->msgContent[3] = intConvert.c[1];
	asMessage->msgContent[4] = intConvert.c[2];
	asMessage->msgContent[5] = intConvert.c[3];

	return asMessage->msgLen;
}

/****************************************************************************
- Function name:	ProcessASMessageForPMConnectCTSFromAS
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessASMessageForPMConnectCTSFromAS(void *message)
{
	pthread_t feedThread;

	TraceLog(DEBUG_LEVEL, "Received message to request PM connect to CTS Feed from AS\n");

	if (CTS_StatusMgmt.isConnected == DISCONNECTED)
	{
		CTS_StatusMgmt.shouldConnect = YES;
		
		// Create and start thread to talk with CTS Feed
		pthread_create(&feedThread, NULL, (void*)&CTS_Feed_Thread, NULL);
	}

	TraceLog(DEBUG_LEVEL, "Received request to start CTS Feed\n");

	return SUCCESS;
}


/****************************************************************************
- Function name:	ProcessASMessageForPMDisconnectCTSFromAS
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessASMessageForPMDisconnectCTSFromAS(void *message)
{
	TraceLog(DEBUG_LEVEL, "Received message to request PM disconnect to CTS Feed from AS\n");

	if ((CTS_StatusMgmt.isConnected == CONNECTED) || (CTS_StatusMgmt.shouldConnect == YES))
	{
		DisconnectCTS_Feed();
	}

	TraceLog(DEBUG_LEVEL, "Received request to disconnect CTS Feed\n");

	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessASMessageForPMConnectCQSFromAS
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessASMessageForPMConnectCQSFromAS(void *message)
{
	pthread_t feedThread;

	TraceLog(DEBUG_LEVEL, "Received message to request PM connect to CQS Feed from AS\n");

	if (CQS_StatusMgmt.isConnected == DISCONNECTED)
	{
		CQS_StatusMgmt.shouldConnect = YES;
		
		// Create and start thread to talk with CQS Feed
		pthread_create(&feedThread, NULL, (void*)&CQS_Feed_Thread, NULL);
	}

	TraceLog(DEBUG_LEVEL, "Received request to start CQS Feed\n");

	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessASMessageForPMDisconnectCQSFromAS
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessASMessageForPMDisconnectCQSFromAS(void *message)
{
	TraceLog(DEBUG_LEVEL, "Received message to request PM disconnect to CQS Feed from AS\n");

	if ((CQS_StatusMgmt.isConnected == CONNECTED) || (CQS_StatusMgmt.shouldConnect == YES))
	{
		DisconnectCQS_Feed();
	}

	TraceLog(DEBUG_LEVEL, "Received request to disconnect CQS Feed\n");

	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessASMessageForPMConnectUQDF_FromAS
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessASMessageForPMConnectUQDF_FromAS(void *message)
{
	pthread_t UQDF_Thread;

	TraceLog(DEBUG_LEVEL, "Received message to request PM connect to UQDF Feed from AS\n");
	
	if (UQDF_StatusMgmt.isConnected == DISCONNECTED)
	{
		UQDF_StatusMgmt.shouldConnect = YES;

		// Create and start thread to talk with UQDF Feed
		pthread_create(&UQDF_Thread, NULL, (void*)&UQDF_Feed_Thread, NULL);
	}

	TraceLog(DEBUG_LEVEL, "Received request to start UQDF Feed\n");

	return SUCCESS;
}


/****************************************************************************
- Function name:	ProcessASMessageForPMDisconnectUQDF_FromAS
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessASMessageForPMDisconnectUQDF_FromAS(void *message)
{
	TraceLog(DEBUG_LEVEL, "Received message to request PM disconnect to UQDF Feed from AS\n");
	
	if ((UQDF_StatusMgmt.isConnected == CONNECTED) || (UQDF_StatusMgmt.shouldConnect == YES))
	{
		DisconnectUQDF_Multicast();
	}

	TraceLog(DEBUG_LEVEL, "Received request to disconnect UQDF Feed\n");

	return SUCCESS;
}

/****************************************************************************
- Function name:	SendHaltStockListToAS
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int SendHaltStockListToAS(void *pConnection)
{
	t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

	// Check connection between AS and PM
	if ((connection->status == DISCONNECTED) || (connection->socket == -1))
	{
		return ERROR;
	}

	t_ASMessage asMessage;

	t_ShortConverter shortConvert;
	t_IntConverter intConvert;

	// Initialize message
	asMessage.msgLen = 0;
	
	// Message type
	shortConvert.value = MSG_TYPE_FOR_SEND_HALT_STOCK_LIST_TO_AS;
	asMessage.msgContent[0] = shortConvert.c[0];
	asMessage.msgContent[1] = shortConvert.c[1];
	asMessage.msgLen += 2;

	// Update index of body length
	asMessage.msgLen += 4;

	// Update index of block length
	asMessage.msgLen += 4;

	// Number of symbols (will be updated later)
	asMessage.msgLen += 4;
	
	//--------------------------------------------------------------------------
	//		Message structure
	//		+ Message type		2 bytes
	//		+ Body Length		4 bytes
	//		+ Block Length		4 bytes
	//		+ Number of symbol  4 bytes
	//		+ Halt status for each symbol:
	//			+ 1st Symbol				8 bytes (SYMBOL_LEN)
	//				- ARCA Halt				1 byte binary
	//				- ARCA update time		4 bytes int
	//				- NASDAQ Halt			1 byte binary
	//				- NASDAQ update time 	4 bytes int
	//				- .... other books
	//			+ 2nd Symbol
	//				-	....
	//				-	....
	//			+ nth symbol
	//				- .....
	//			
	//--------------------------------------------------------------------------

	int symbolIndex;
	int numSymbol = 0;
	int backupIndex[MAX_STOCK_SYMBOL];
	
	for (symbolIndex = 0; symbolIndex < tradeSymbolList.countSymbol; symbolIndex++)
	{
		if (tradeSymbolList.symbolInfo[symbolIndex].haltStatusSent == NO)
		{
			backupIndex[numSymbol++] = symbolIndex;
			
			memcpy(&asMessage.msgContent[asMessage.msgLen], tradeSymbolList.symbolInfo[symbolIndex].symbol, SYMBOL_LEN);
			asMessage.msgLen += SYMBOL_LEN;
			
			//ARCA book
			asMessage.msgContent[asMessage.msgLen++] = tradeSymbolList.symbolInfo[symbolIndex].haltStatus[BOOK_ARCA];
				
			memcpy(&asMessage.msgContent[asMessage.msgLen], &tradeSymbolList.symbolInfo[symbolIndex].haltTime[BOOK_ARCA], 4);
			asMessage.msgLen += 4;
			
			//Nasdaq book
			asMessage.msgContent[asMessage.msgLen++] = tradeSymbolList.symbolInfo[symbolIndex].haltStatus[BOOK_NASDAQ];
				
			memcpy(&asMessage.msgContent[asMessage.msgLen], &tradeSymbolList.symbolInfo[symbolIndex].haltTime[BOOK_NASDAQ], 4);
			asMessage.msgLen += 4;
		}
	}

	if (numSymbol == 0) return SUCCESS;	//There is nothing to send
	
	// Update number of symbols field
	memcpy(&asMessage.msgContent[10], &numSymbol, 4);
	
	// Block length field
	intConvert.value = asMessage.msgLen - MSG_HEADER_LEN;
	asMessage.msgContent[MSG_HEADER_LEN] = intConvert.c[0];
	asMessage.msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
	asMessage.msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
	asMessage.msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
	
	// Body length field
	intConvert.value = asMessage.msgLen - MSG_HEADER_LEN;
	asMessage.msgContent[2] = intConvert.c[0];
	asMessage.msgContent[3] = intConvert.c[1];
	asMessage.msgContent[4] = intConvert.c[2];
	asMessage.msgContent[5] = intConvert.c[3];

	TraceLog(DEBUG_LEVEL, "Send halt stock list to AS ...\n");
	
	// Send message to AS
	if (SendASMessage(connection, &asMessage) == SUCCESS)
	{
		int i;
		for (i = 0; i < numSymbol; i++)
		{
			tradeSymbolList.symbolInfo[backupIndex[i]].haltStatusSent = YES;
		}
		
		return SUCCESS;
	}

	TraceLog(ERROR_LEVEL, "Cannot send halt stock list to AS, socket = %d\n", connection->socket);
		
	return ERROR;
}

/****************************************************************************
- Function name:	SendAHaltedStockToAS
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int SendAHaltedStockToAS(t_ConnectionStatus *pConnection, const int symbolIndex, const int willApplyToAllVenues)
{
	t_ConnectionStatus *connection = pConnection;

	// Check connection between AS and PM
	if ((connection->status == DISCONNECTED) || (connection->socket == -1))
	{
		return ERROR;
	}

	int result;
	t_ASMessage asMessage;

	// Build a halted stock message to Send PM
	BuildASMessageForAHaltedStock(&asMessage, symbolIndex, willApplyToAllVenues);

	// Send message to AS
	result = SendASMessage(connection, &asMessage);

	if (result == ERROR)
	{
		TraceLog(ERROR_LEVEL, "Cannot send halted stock (%.8s) to AS, socket = %d\n", tradeSymbolList.symbolInfo[symbolIndex].symbol, connection->socket);
	}
	else
	{
		TraceLog(DEBUG_LEVEL, "Sent a halted stock (%.8s) to AS\n", tradeSymbolList.symbolInfo[symbolIndex].symbol);
		tradeSymbolList.symbolInfo[symbolIndex].haltStatusSent = YES;
	}
	
	return result;
}

/****************************************************************************
- Function name:	BuildASMessageForAHaltedStock
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int BuildASMessageForAHaltedStock(void *message, const int symbolIndex, const int willApplyToAllVenues)
{
	t_ShortConverter shortConvert;
	t_IntConverter intConvert;

	t_ASMessage *asMessage = (t_ASMessage *)message;
	
	// Initialize message
	asMessage->msgLen = 0;
	
	// Message type
	shortConvert.value = MSG_TYPE_FOR_SEND_A_HALTED_STOCK_TO_AS;
	asMessage->msgContent[0] = shortConvert.c[0];
	asMessage->msgContent[1] = shortConvert.c[1];
	asMessage->msgLen += 2;

	// Update index of body length
	asMessage->msgLen += 4;

	// Update index of block length
	asMessage->msgLen += 4;

	asMessage->msgContent[asMessage->msgLen++] = willApplyToAllVenues;
	
	//symbol
	memcpy(&asMessage->msgContent[asMessage->msgLen], tradeSymbolList.symbolInfo[symbolIndex].symbol, SYMBOL_LEN);
	asMessage->msgLen += SYMBOL_LEN;
	
	// Arca
	asMessage->msgContent[asMessage->msgLen++] = tradeSymbolList.symbolInfo[symbolIndex].haltStatus[BOOK_ARCA];
	memcpy(&asMessage->msgContent[asMessage->msgLen], &tradeSymbolList.symbolInfo[symbolIndex].haltTime[BOOK_ARCA], 4);
	asMessage->msgLen += 4;
	
	// Nasdaq
	asMessage->msgContent[asMessage->msgLen++] = tradeSymbolList.symbolInfo[symbolIndex].haltStatus[BOOK_NASDAQ];
	memcpy(&asMessage->msgContent[asMessage->msgLen], &tradeSymbolList.symbolInfo[symbolIndex].haltTime[BOOK_NASDAQ], 4);
	asMessage->msgLen += 4;
	
	// Block length field
	intConvert.value = asMessage->msgLen - MSG_HEADER_LEN;
	asMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
	asMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
	asMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
	asMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
	
	// Body length field
	intConvert.value = asMessage->msgLen - MSG_HEADER_LEN;
	asMessage->msgContent[2] = intConvert.c[0];
	asMessage->msgContent[3] = intConvert.c[1];
	asMessage->msgContent[4] = intConvert.c[2];
	asMessage->msgContent[5] = intConvert.c[3];

	return asMessage->msgLen;
}

/****************************************************************************
- Function name:	ProcessASMessageForHaltStockList
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessASMessageForHaltStockList(void *message)
{
	t_ASMessage *asMessage = (t_ASMessage *)message;

	int currentIndex = MSG_HEADER_LEN;

	// Update current index
	currentIndex += 4;

	// number of symbol
	int countSymbol;
	memcpy(&countSymbol, &asMessage->msgContent[currentIndex], 4);
	currentIndex += 4;
	
	int i, flag;
	int symbolIndex;
	unsigned int haltTime;
	char haltString[128];
	char symbol[SYMBOL_LEN];
	
	for (i = 0; i < countSymbol; i++)
	{
		flag = 0;
		haltString[0] = 0;
		
		strncpy(symbol, (char*) &asMessage->msgContent[currentIndex], SYMBOL_LEN);
		currentIndex += SYMBOL_LEN;
		
		symbolIndex = GetTradeSymbolIndex(symbol);
		
		if (symbolIndex == -1)
		{
			currentIndex += (5 * MAX_BOOK_CONNECTIONS + 1);
			continue;
		}
		
		int oldStatus = tradeSymbolList.symbolInfo[symbolIndex].haltStatus[BOOK_ARCA] + tradeSymbolList.symbolInfo[symbolIndex].haltStatus[BOOK_NASDAQ];
		
		tradeSymbolList.symbolInfo[symbolIndex].isAutoResume = asMessage->msgContent[currentIndex];
		currentIndex++;
		
		// Arca
		memcpy(&haltTime, &asMessage->msgContent[currentIndex + 1], 4);
		if ((haltTime > tradeSymbolList.symbolInfo[symbolIndex].haltTime[BOOK_ARCA]) ||
			 ((haltTime == tradeSymbolList.symbolInfo[symbolIndex].haltTime[BOOK_ARCA]) &&
			  (tradeSymbolList.symbolInfo[symbolIndex].haltStatus[BOOK_ARCA] != asMessage->msgContent[currentIndex])) )//Just get the latest status
		{
			tradeSymbolList.symbolInfo[symbolIndex].haltTime[BOOK_ARCA] = haltTime;
			tradeSymbolList.symbolInfo[symbolIndex].haltStatus[BOOK_ARCA] = asMessage->msgContent[currentIndex];
					
			if (asMessage->msgContent[currentIndex] != SYMBOL_STATUS_RESUME)
			{
				flag = 1;
				strcat(haltString, "ARCA ");
			}
		}
		currentIndex += 5;
		
		// Nasdaq
		memcpy(&haltTime, &asMessage->msgContent[currentIndex + 1], 4);
		if ( (haltTime > tradeSymbolList.symbolInfo[symbolIndex].haltTime[BOOK_NASDAQ]) ||
			 ((haltTime == tradeSymbolList.symbolInfo[symbolIndex].haltTime[BOOK_NASDAQ]) &&
			  (tradeSymbolList.symbolInfo[symbolIndex].haltStatus[BOOK_NASDAQ] != asMessage->msgContent[currentIndex])) )//Just get the latest status
		{
			tradeSymbolList.symbolInfo[symbolIndex].haltTime[BOOK_NASDAQ] = haltTime;
			tradeSymbolList.symbolInfo[symbolIndex].haltStatus[BOOK_NASDAQ] = asMessage->msgContent[currentIndex];
					
			if (asMessage->msgContent[currentIndex] != SYMBOL_STATUS_RESUME)
			{
				flag = 1;
				strcat(haltString, "NASDAQ ");
			}
		}
		currentIndex += 5;
		
		if (flag == 1)
		{
			TraceLog(DEBUG_LEVEL, "Receive a symbol halted (%.8s) at %sfrom AS\n", symbol, haltString);
		}
		else
		{
			int newStatus = tradeSymbolList.symbolInfo[symbolIndex].haltStatus[BOOK_ARCA] + tradeSymbolList.symbolInfo[symbolIndex].haltStatus[BOOK_NASDAQ];
			if ((oldStatus != SYMBOL_STATUS_RESUME) && (newStatus == SYMBOL_STATUS_RESUME))
			{
				// Trading resumed
				TraceLog(DEBUG_LEVEL, "Receive a symbol resumed (%.8s) from AS\n", symbol);
				
				if (tradeSymbolList.symbolInfo[symbolIndex].isAutoResume == 1)
				{
					CheckAndPlaceOrderWhenSymbolResume(symbol);
				}
			}
		}
	}

	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessASMessageForHaltStock
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessASMessageForHaltStock(void *message)
{
	t_ASMessage *asMessage = (t_ASMessage *)message;

	int currentIndex = MSG_HEADER_LEN;

	// Update current index
	currentIndex += 4;
	
	int symbolIndex;
	char symbol[SYMBOL_LEN];
	
	strncpy(symbol, (char*) &asMessage->msgContent[currentIndex], SYMBOL_LEN);
	currentIndex += SYMBOL_LEN;
	
	symbolIndex = GetTradeSymbolIndex(symbol);
		
	if (symbolIndex == -1)
	{
		return SUCCESS;
	}
	
	int oldStatus = tradeSymbolList.symbolInfo[symbolIndex].haltStatus[BOOK_ARCA] + tradeSymbolList.symbolInfo[symbolIndex].haltStatus[BOOK_NASDAQ];
	int flag;
	unsigned int haltTime;
	char haltString[128];
	
	flag = 0;
	haltString[0] = 0;
	
	tradeSymbolList.symbolInfo[symbolIndex].isAutoResume = asMessage->msgContent[currentIndex];
	currentIndex++;
	
	// Arca
	memcpy(&haltTime, &asMessage->msgContent[currentIndex + 1], 4);
	if ( (haltTime > tradeSymbolList.symbolInfo[symbolIndex].haltTime[BOOK_ARCA])	||
		 ((haltTime == tradeSymbolList.symbolInfo[symbolIndex].haltTime[BOOK_ARCA]) && 
		  (tradeSymbolList.symbolInfo[symbolIndex].haltStatus[BOOK_ARCA] != asMessage->msgContent[currentIndex])) ) //Just get the latest status
	{
		tradeSymbolList.symbolInfo[symbolIndex].haltTime[BOOK_ARCA] = haltTime;
		tradeSymbolList.symbolInfo[symbolIndex].haltStatus[BOOK_ARCA] = asMessage->msgContent[currentIndex];
					
		if (asMessage->msgContent[currentIndex] != SYMBOL_STATUS_RESUME)
		{
			flag = 1;
			strcat(haltString, "ARCA ");
		}
	}
	currentIndex += 5;
		
	// Nasdaq
	memcpy(&haltTime, &asMessage->msgContent[currentIndex + 1], 4);
	if ( (haltTime > tradeSymbolList.symbolInfo[symbolIndex].haltTime[BOOK_NASDAQ]) ||
		 ((haltTime == tradeSymbolList.symbolInfo[symbolIndex].haltTime[BOOK_NASDAQ]) && 
		  (tradeSymbolList.symbolInfo[symbolIndex].haltStatus[BOOK_NASDAQ] != asMessage->msgContent[currentIndex])) )//Just get the latest status
	{
		tradeSymbolList.symbolInfo[symbolIndex].haltTime[BOOK_NASDAQ] = haltTime;
		tradeSymbolList.symbolInfo[symbolIndex].haltStatus[BOOK_NASDAQ] = asMessage->msgContent[currentIndex];
				
		if (asMessage->msgContent[currentIndex] != SYMBOL_STATUS_RESUME)
		{
			flag = 1;
			strcat(haltString, "NASDAQ ");
		}
	}
	
	if (flag == 1)
	{
		TraceLog(DEBUG_LEVEL, "Receive a symbol halted (%.8s) at %sfrom AS\n", symbol, haltString);
	}
	else
	{
		int newStatus = tradeSymbolList.symbolInfo[symbolIndex].haltStatus[BOOK_ARCA] + tradeSymbolList.symbolInfo[symbolIndex].haltStatus[BOOK_NASDAQ];
		if ((oldStatus != SYMBOL_STATUS_RESUME) && (newStatus == SYMBOL_STATUS_RESUME))
		{
			// Trading resumed
			TraceLog(DEBUG_LEVEL, "Receive a symbol resumed (%.8s) from AS\n", symbol);
			
			if (tradeSymbolList.symbolInfo[symbolIndex].isAutoResume == 1)
			{
				CheckAndPlaceOrderWhenSymbolResume(symbol);
			}
		}
	}
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessASMessageForGetTradedList
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessASMessageForGetTradedList(void *message)
{
	t_ASMessage *asMessage = (t_ASMessage *)message;

	int currentIndex = MSG_HEADER_LEN;

	// Update current index
	currentIndex += 4;

	// Block content = senderId(4 bytes) + a symbol (8 bytes)
	
	// senderId
	int senderId = GetIntNumber(&asMessage->msgContent[currentIndex]);

	// Update current index
	currentIndex += 4;

	// Symbol
	char symbol[SYMBOL_LEN];

	strncpy(symbol, (char*) &asMessage->msgContent[currentIndex], SYMBOL_LEN);

	TraceLog(DEBUG_LEVEL, "Received message to request get traded list from AS, symbol = %.8s\n", symbol);

	// Send traded list To AS
	SendTradedListToAS(senderId, symbol);

	return SUCCESS;
}

/****************************************************************************
- Function name:	SendTradedListToAS
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int SendTradedListToAS(int ttIndex, char *stockSymbol)
{	
	t_ConnectionStatus *connection = (t_ConnectionStatus *)&asCollection.connection;

	// Check connection between AS and PM
	if ((connection->status == DISCONNECTED) || (connection->socket == -1))
	{
		return ERROR;
	}

	int result;
	t_ASMessage asMessage;
	
	// Build traded list message to Send PM 
	result = BuildASMessageForTradedList(&asMessage, ttIndex, stockSymbol);

	if (result == ERROR)
	{
		return SUCCESS;
	}

	TraceLog(DEBUG_LEVEL, "Send traded list of symbol = %.8s to AS ...\n", stockSymbol);

	// Send message to AS
	result = SendASMessage(connection, &asMessage);

	if (result == ERROR)
	{
		TraceLog(ERROR_LEVEL, "Cannot Send traded list to AS, socket = %d\n", connection->socket);
	}
	
	return result;
}

/****************************************************************************
- Function name:	SendManualOrderResponseToAS
- Input:			Order Info + Reason
- Output:			N/A
- Return:			SUCCESS/ERROR
- Description:	
- Usage:			
*********************************************************************************/
int SendManualOrderResponseToAS(t_ManualOrder *newOrder, int reason)
{
	t_ConnectionStatus *connection = (t_ConnectionStatus *)&asCollection.connection;

	// Check connection between AS and PM
	if ((connection->status == DISCONNECTED) || (connection->socket == -1))
	{
		return ERROR;
	}

	int result;
	t_ASMessage asMessage;
	
	// Build response message to Send PM 
	result = BuildASMessageForManualOrderResponse(&asMessage, newOrder, reason);

	if (result == ERROR)
	{
		return SUCCESS;
	}

	// Send message to AS
	result = SendASMessage(connection, &asMessage);

	if (result == ERROR)
	{
		TraceLog(ERROR_LEVEL, "Cannot send manual order response to AS (response: %d)\n", reason);
	}
	else
	{
		TraceLog(DEBUG_LEVEL, "Sent manual order response to AS (response: %d)\n", reason);
	}
	
	return result;
}

/****************************************************************************
- Function name:	BuildASMessageForTradedList
- Input:			N/A
- Output:		
- Return:		
- Description:	
- Usage:			
*********************************************************************************/
int BuildASMessageForTradedList(void *message, int ttIndex, char *stockSymbol)
{
	t_ShortConverter shortNum;
	t_IntConverter intNum;
	t_DoubleConverter doubleNum;

	t_ASMessage *asMessage = (t_ASMessage *)message;
	
	// Initialize message
	asMessage->msgLen = 0;
	
	// Message type
	shortNum.value = MSG_TYPE_FOR_SEND_TRADED_LIST_TO_AS;
	asMessage->msgContent[0] = shortNum.c[0];
	asMessage->msgContent[1] = shortNum.c[1];
	asMessage->msgLen += 2;

	// Update index of body length
	asMessage->msgLen += 4;

	// Index of symbol in stock Book
	int symbolIndex = GetTradeSymbolIndex(stockSymbol);
	
	if (symbolIndex == -1)
	{
		TraceLog(ERROR_LEVEL, "Trade symbol list is full, can't add new symbol (%.8s)!\n", stockSymbol);
		
		return ERROR;
	}	
		
	// Update index of block length
	asMessage->msgLen += 4;

	// TT sender	
	intNum.value = ttIndex;
	asMessage->msgContent[asMessage->msgLen] = intNum.c[0];
	asMessage->msgContent[asMessage->msgLen + 1] = intNum.c[1];
	asMessage->msgContent[asMessage->msgLen + 2] = intNum.c[2];
	asMessage->msgContent[asMessage->msgLen + 3] = intNum.c[3];
	asMessage->msgLen += 4;
	
	// Symbol
	memcpy(&asMessage->msgContent[asMessage->msgLen], stockSymbol, SYMBOL_LEN);
	asMessage->msgLen += SYMBOL_LEN;
	
	// Get traded list ....
	
	// Get current trade information of current stock symbol
	t_TradeList *symbolInfo = &tradeSymbolList.symbolInfo[symbolIndex];

	// Get last index
	int lastIndex = symbolInfo->countTrade - 1;

	if (lastIndex >= 0)
	{
		int tradeIndex = lastIndex;

		for (tradeIndex = lastIndex; (tradeIndex > (lastIndex - MAX_LAST_TRADE)) && (tradeIndex >= 0); tradeIndex--)
		{
			// Data feed
			asMessage->msgContent[asMessage->msgLen] = symbolInfo->tradeList[tradeIndex % MAX_LAST_TRADE].dataFeed;
			asMessage->msgLen += 1;
			
			// Participant Id
			asMessage->msgContent[asMessage->msgLen] = symbolInfo->tradeList[tradeIndex % MAX_LAST_TRADE].participantId;
			asMessage->msgLen += 1;

			// Timestamp
			intNum.value = symbolInfo->tradeList[tradeIndex % MAX_LAST_TRADE].timestamp;
			memcpy(&asMessage->msgContent[asMessage->msgLen], intNum.c, 4);
			asMessage->msgLen += 4;

			// Share
			intNum.value = symbolInfo->tradeList[tradeIndex % MAX_LAST_TRADE].shares;
			memcpy(&asMessage->msgContent[asMessage->msgLen], intNum.c, 4);
			asMessage->msgLen += 4;
			
			// Price
			doubleNum.value = symbolInfo->tradeList[tradeIndex % MAX_LAST_TRADE].price;
			memcpy(&asMessage->msgContent[asMessage->msgLen], doubleNum.c, 8);
			asMessage->msgLen += 8;
		}
	}

	// Block length field
	intNum.value = asMessage->msgLen - MSG_HEADER_LEN;
	asMessage->msgContent[MSG_HEADER_LEN] = intNum.c[0];
	asMessage->msgContent[MSG_HEADER_LEN + 1] = intNum.c[1];
	asMessage->msgContent[MSG_HEADER_LEN + 2] = intNum.c[2];
	asMessage->msgContent[MSG_HEADER_LEN + 3] = intNum.c[3];
	
	// Body length field
	intNum.value = asMessage->msgLen - MSG_HEADER_LEN;
	asMessage->msgContent[2] = intNum.c[0];
	asMessage->msgContent[3] = intNum.c[1];
	asMessage->msgContent[4] = intNum.c[2];
	asMessage->msgContent[5] = intNum.c[3];
	
	return asMessage->msgLen;
}

/****************************************************************************
- Function name:	ProcessASMessageForPMConnectUTDFFromAS
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessASMessageForPMConnectUTDFFromAS(void *message)
{
	pthread_t feedThread;

	TraceLog(DEBUG_LEVEL, "Received message to request PM connect to UTDF from AS\n");

	if (UTDF_StatusMgmt.isConnected == DISCONNECTED)
	{
		UTDF_StatusMgmt.shouldConnect = YES;
		
		// Create and start thread to talk with UTDF
		pthread_create(&feedThread, NULL, (void*)&Start_UTDF_Thread, NULL);
	}

	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessASMessageForPMDisconnectUTDFFromAS
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessASMessageForPMDisconnectUTDFFromAS(void *message)
{
	TraceLog(DEBUG_LEVEL, "Received message to request PM disconnect to UTDF from AS\n");

	if ((UTDF_StatusMgmt.isConnected == CONNECTED) || (UTDF_StatusMgmt.shouldConnect == YES))
	{
		DisconnectUTDF_Multicast();
	}

	return SUCCESS;
}

/****************************************************************************
- Function name:	SendPMDisengageAlertToAS
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int SendPMDisengageAlertToAS(t_Position *stuck, int reason)
{
	t_ConnectionStatus *connection = (t_ConnectionStatus *)&asCollection.connection;

	// Check connection between AS and PM
	if ((connection->status == DISCONNECTED) || (connection->socket == -1))
	{
		return ERROR;
	}

	TraceLog(DEBUG_LEVEL, "Send PM disengage alert to AS, reason code = %d...\n", reason);

	int result;
	t_ASMessage asMessage;

	// Build PM disengage alert message to send to AS
	BuildASMessageForPMDisengageAlert(&asMessage, stuck, reason);

	// Send message to AS
	result = SendASMessage(connection, &asMessage);

	if (result == ERROR)
	{
		TraceLog(ERROR_LEVEL, "Cannot send PM disengage alert to AS, socket = %d\n", connection->socket);
	}
	
	return result;
}

/****************************************************************************
- Function name:	BuildASMessageForPMDisengageAlert
- Input:			N/A
- Output:		
- Return:		
- Description:	
- Usage:			
*********************************************************************************/
int BuildASMessageForPMDisengageAlert(void *message, t_Position *stuck, int reason)
{
	t_ShortConverter shortNum;
	t_IntConverter intNum;
	t_DoubleConverter doubleNum;

	t_ASMessage *asMessage = (t_ASMessage *)message;
	
	// Initialize message
	asMessage->msgLen = 0;
	
	// Message type
	shortNum.value = MSG_TYPE_FOR_SEND_DISENGAGE_ALERT_TO_AS;
	asMessage->msgContent[0] = shortNum.c[0];
	asMessage->msgContent[1] = shortNum.c[1];
	asMessage->msgLen += 2;

	// Update index of body length
	asMessage->msgLen += 4;
	
	// Update index of block length
	asMessage->msgLen += 4;
	
	// Symbol
	memcpy(&asMessage->msgContent[asMessage->msgLen], stuck->symbol, SYMBOL_LEN);
	asMessage->msgLen += SYMBOL_LEN;
	
	// Price
	doubleNum.value = stuck->price;
	memcpy(&asMessage->msgContent[asMessage->msgLen], doubleNum.c, 8);
	asMessage->msgLen += 8;
	
	// Share
	intNum.value = stuck->shares;
	memcpy(&asMessage->msgContent[asMessage->msgLen], intNum.c, 4);
	asMessage->msgLen += 4;

	// Side
	asMessage->msgContent[asMessage->msgLen] = stuck->side;
	asMessage->msgLen += 1;
	
	// Account
	asMessage->msgContent[asMessage->msgLen] = stuck->accountSuffix;
	asMessage->msgLen += 1;
	
	// Reason code, explains why the stuck is disengaged
	asMessage->msgContent[asMessage->msgLen] = reason;
	asMessage->msgLen += 1;
	
	// Block length field
	intNum.value = asMessage->msgLen - MSG_HEADER_LEN;
	asMessage->msgContent[MSG_HEADER_LEN] = intNum.c[0];
	asMessage->msgContent[MSG_HEADER_LEN + 1] = intNum.c[1];
	asMessage->msgContent[MSG_HEADER_LEN + 2] = intNum.c[2];
	asMessage->msgContent[MSG_HEADER_LEN + 3] = intNum.c[3];
	
	// Body length field
	intNum.value = asMessage->msgLen - MSG_HEADER_LEN;
	asMessage->msgContent[2] = intNum.c[0];
	asMessage->msgContent[3] = intNum.c[1];
	asMessage->msgContent[4] = intNum.c[2];
	asMessage->msgContent[5] = intNum.c[3];
	
	return asMessage->msgLen;
}

/****************************************************************************
- Function name:	BuildASMessageForManualOrderResponse
- Input:		Order info
- Output:		message
- Return:		N/A
- Description:	N/A
- Usage:		N/A
****************************************************************************/
int BuildASMessageForManualOrderResponse(void *message, t_ManualOrder *newOrder, int reason)
{
	t_ShortConverter shortConvert;
	t_IntConverter intConvert;
	t_DoubleConverter doubleConvert;
	
	t_ASMessage *asMessage = (t_ASMessage *)message;
	
	// Initialize message
	asMessage->msgLen = 0;
	
	// Message type
	shortConvert.value = MSG_TYPE_FOR_SEND_MANUAL_ORDER_RESPONSE_TO_AS;
	asMessage->msgContent[0] = shortConvert.c[0];
	asMessage->msgContent[1] = shortConvert.c[1];
	asMessage->msgLen += 2;

	// Update index of body length
	asMessage->msgLen += 4;

	// Update index of block length
	asMessage->msgLen += 4;

	//Symbol
	strncpy((char*)&asMessage->msgContent[asMessage->msgLen], newOrder->symbol, SYMBOL_LEN);
	asMessage->msgLen += SYMBOL_LEN;
	
	//side
	intConvert.value = newOrder->side;
	asMessage->msgContent[asMessage->msgLen] = intConvert.c[0];
	asMessage->msgContent[asMessage->msgLen + 1] = intConvert.c[1];
	asMessage->msgContent[asMessage->msgLen + 2] = intConvert.c[2];
	asMessage->msgContent[asMessage->msgLen + 3] = intConvert.c[3];
	asMessage->msgLen += 4;
	
	//shares
	intConvert.value = newOrder->shares;
	asMessage->msgContent[asMessage->msgLen] = intConvert.c[0];
	asMessage->msgContent[asMessage->msgLen + 1] = intConvert.c[1];
	asMessage->msgContent[asMessage->msgLen + 2] = intConvert.c[2];
	asMessage->msgContent[asMessage->msgLen + 3] = intConvert.c[3];
	asMessage->msgLen += 4;
	
	//maxFloor
	intConvert.value = newOrder->maxFloor;
	asMessage->msgContent[asMessage->msgLen] = intConvert.c[0];
	asMessage->msgContent[asMessage->msgLen + 1] = intConvert.c[1];
	asMessage->msgContent[asMessage->msgLen + 2] = intConvert.c[2];
	asMessage->msgContent[asMessage->msgLen + 3] = intConvert.c[3];
	asMessage->msgLen += 4;
	
	//price
	doubleConvert.value = newOrder->price;
	asMessage->msgContent[asMessage->msgLen] = doubleConvert.c[0];
	asMessage->msgContent[asMessage->msgLen + 1] = doubleConvert.c[1];
	asMessage->msgContent[asMessage->msgLen + 2] = doubleConvert.c[2];
	asMessage->msgContent[asMessage->msgLen + 3] = doubleConvert.c[3];
	asMessage->msgContent[asMessage->msgLen + 4] = doubleConvert.c[4];
	asMessage->msgContent[asMessage->msgLen + 5] = doubleConvert.c[5];
	asMessage->msgContent[asMessage->msgLen + 6] = doubleConvert.c[6];
	asMessage->msgContent[asMessage->msgLen + 7] = doubleConvert.c[7];
	asMessage->msgLen += 8;
	
	//ECNId
	intConvert.value = newOrder->ECNId;
	asMessage->msgContent[asMessage->msgLen] = intConvert.c[0];
	asMessage->msgContent[asMessage->msgLen + 1] = intConvert.c[1];
	asMessage->msgContent[asMessage->msgLen + 2] = intConvert.c[2];
	asMessage->msgContent[asMessage->msgLen + 3] = intConvert.c[3];
	asMessage->msgLen += 4;
	
	//TIF
	intConvert.value = newOrder->tif;
	asMessage->msgContent[asMessage->msgLen] = intConvert.c[0];
	asMessage->msgContent[asMessage->msgLen + 1] = intConvert.c[1];
	asMessage->msgContent[asMessage->msgLen + 2] = intConvert.c[2];
	asMessage->msgContent[asMessage->msgLen + 3] = intConvert.c[3];
	asMessage->msgLen += 4;
	
	//SendId
	intConvert.value = newOrder->sendId;
	asMessage->msgContent[asMessage->msgLen] = intConvert.c[0];
	asMessage->msgContent[asMessage->msgLen + 1] = intConvert.c[1];
	asMessage->msgContent[asMessage->msgLen + 2] = intConvert.c[2];
	asMessage->msgContent[asMessage->msgLen + 3] = intConvert.c[3];
	asMessage->msgLen += 4;
	
	//Peg Difference
	doubleConvert.value = newOrder->pegDef;
	asMessage->msgContent[asMessage->msgLen] = doubleConvert.c[0];
	asMessage->msgContent[asMessage->msgLen + 1] = doubleConvert.c[1];
	asMessage->msgContent[asMessage->msgLen + 2] = doubleConvert.c[2];
	asMessage->msgContent[asMessage->msgLen + 3] = doubleConvert.c[3];
	asMessage->msgContent[asMessage->msgLen + 4] = doubleConvert.c[4];
	asMessage->msgContent[asMessage->msgLen + 5] = doubleConvert.c[5];
	asMessage->msgContent[asMessage->msgLen + 6] = doubleConvert.c[6];
	asMessage->msgContent[asMessage->msgLen + 7] = doubleConvert.c[7];
	asMessage->msgLen += 8;
	
	//OrderID
	intConvert.value = newOrder->orderId;
	asMessage->msgContent[asMessage->msgLen] = intConvert.c[0];
	asMessage->msgContent[asMessage->msgLen + 1] = intConvert.c[1];
	asMessage->msgContent[asMessage->msgLen + 2] = intConvert.c[2];
	asMessage->msgContent[asMessage->msgLen + 3] = intConvert.c[3];
	asMessage->msgLen += 4;
	
	// Account
	asMessage->msgContent[asMessage->msgLen] = newOrder->accountSuffix;
	asMessage->msgLen += 1;
	
	//was forced?
	intConvert.value = newOrder->isForced;
	asMessage->msgContent[asMessage->msgLen] = intConvert.c[0];
	asMessage->msgContent[asMessage->msgLen + 1] = intConvert.c[1];
	asMessage->msgContent[asMessage->msgLen + 2] = intConvert.c[2];
	asMessage->msgContent[asMessage->msgLen + 3] = intConvert.c[3];
	asMessage->msgLen += 4;
	
	//TT Index
	intConvert.value = newOrder->ttIndex;
	asMessage->msgContent[asMessage->msgLen] = intConvert.c[0];
	asMessage->msgContent[asMessage->msgLen + 1] = intConvert.c[1];
	asMessage->msgContent[asMessage->msgLen + 2] = intConvert.c[2];
	asMessage->msgContent[asMessage->msgLen + 3] = intConvert.c[3];
	asMessage->msgLen += 4;
	
	//reason
	intConvert.value = reason;
	asMessage->msgContent[asMessage->msgLen] = intConvert.c[0];
	asMessage->msgContent[asMessage->msgLen + 1] = intConvert.c[1];
	asMessage->msgContent[asMessage->msgLen + 2] = intConvert.c[2];
	asMessage->msgContent[asMessage->msgLen + 3] = intConvert.c[3];
	asMessage->msgLen += 4;
	
	// Block length field
	intConvert.value = asMessage->msgLen - MSG_HEADER_LEN;
	asMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
	asMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
	asMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
	asMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
	
	// Body length field
	intConvert.value = asMessage->msgLen - MSG_HEADER_LEN;
	asMessage->msgContent[2] = intConvert.c[0];
	asMessage->msgContent[3] = intConvert.c[1];
	asMessage->msgContent[4] = intConvert.c[2];
	asMessage->msgContent[5] = intConvert.c[3];

	return asMessage->msgLen;
}

/****************************************************************************
- Function name:	BuildASMessageForBookIdleWarning
- Input:		N/A
- Output:		N/A
- Return:		N/A
- Description:	N/A
- Usage:		N/A
****************************************************************************/
int BuildASMessageForBookIdleWarning(void *message, int ecnIndex, int groupIndex)
{
	t_ShortConverter shortConvert;
	t_IntConverter intConvert;

	t_ASMessage *asMessage = (t_ASMessage *)message;
	
	// Initialize message
	asMessage->msgLen = 0;
	
	// Message type
	shortConvert.value = MSG_TYPE_FOR_SEND_BOOK_IDLE_WARNING_TO_AS;
	asMessage->msgContent[0] = shortConvert.c[0];
	asMessage->msgContent[1] = shortConvert.c[1];
	asMessage->msgLen += 2;

	// Update index of body length
	asMessage->msgLen += 4;

	// Update index of block length
	asMessage->msgLen += 4;

	// ECN Book Index (4 bytes)
	intConvert.value = ecnIndex;
	asMessage->msgContent[asMessage->msgLen] = intConvert.c[0];
	asMessage->msgContent[asMessage->msgLen + 1] = intConvert.c[1];
	asMessage->msgContent[asMessage->msgLen + 2] = intConvert.c[2];
	asMessage->msgContent[asMessage->msgLen + 3] = intConvert.c[3];
	asMessage->msgLen += 4;

	// Group Index (4 bytes)
	intConvert.value = groupIndex;
	asMessage->msgContent[asMessage->msgLen] = intConvert.c[0];
	asMessage->msgContent[asMessage->msgLen + 1] = intConvert.c[1];
	asMessage->msgContent[asMessage->msgLen + 2] = intConvert.c[2];
	asMessage->msgContent[asMessage->msgLen + 3] = intConvert.c[3];
	asMessage->msgLen += 4;
		
	// Block length field
	intConvert.value = asMessage->msgLen - MSG_HEADER_LEN;
	asMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
	asMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
	asMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
	asMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
	
	// Body length field
	intConvert.value = asMessage->msgLen - MSG_HEADER_LEN;
	asMessage->msgContent[2] = intConvert.c[0];
	asMessage->msgContent[3] = intConvert.c[1];
	asMessage->msgContent[4] = intConvert.c[2];
	asMessage->msgContent[5] = intConvert.c[3];

	return asMessage->msgLen;
}

/****************************************************************************
- Function name:	SendPMBookIdleWarningToAS
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int SendPMBookIdleWarningToAS(int ecnIndex, int goupIndex)
{
	t_ConnectionStatus *connection = (t_ConnectionStatus *)&asCollection.connection;

	// Check connection between AS and PM
	if ((connection->status == DISCONNECTED) || (connection->socket == -1))
	{
		return ERROR;
	}

	int result;
	t_ASMessage asMessage;
	
	// Build stock symbol status message to Send PM 
	BuildASMessageForBookIdleWarning(&asMessage, ecnIndex, goupIndex);

	// Send message to AS
	result = SendASMessage(connection, &asMessage);

	if (result == ERROR)
	{
		TraceLog(ERROR_LEVEL, "Cannot send Book idle warning status to AS, socket = %d\n", connection->socket);
	}
	
	return result;
}

/****************************************************************************
- Function name:	BuildASMessageForDataUnhandledWarning
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int BuildASMessageForDataUnhandledWarning(void *message, int serviceIndex, int groupIndex)
{
	//--------------------------------------------------------------------------
	//						Message structure
	//		+ Message type		2 bytes
	//		+ Body Length		4 bytes
	//		+ Block Length		4 bytes
	//		+ Service Index		4 bytes
	//		+ Group Index		4 bytes
	//--------------------------------------------------------------------------
	
	t_ShortConverter shortConvert;
	t_IntConverter intConvert;

	t_ASMessage *asMessage = (t_ASMessage *)message;
	
	// Initialize message
	asMessage->msgLen = 0;
	
	// Message type
	shortConvert.value = MSG_TYPE_FOR_SEND_DATA_UNHANDLED_TO_AS;
	asMessage->msgContent[0] = shortConvert.c[0];
	asMessage->msgContent[1] = shortConvert.c[1];
	asMessage->msgLen += 2;

	// Update index of body length
	asMessage->msgLen += 4;

	// Update index of block length
	asMessage->msgLen += 4;

	// Service Index (4 bytes)
	intConvert.value = serviceIndex;
	asMessage->msgContent[asMessage->msgLen] = intConvert.c[0];
	asMessage->msgContent[asMessage->msgLen + 1] = intConvert.c[1];
	asMessage->msgContent[asMessage->msgLen + 2] = intConvert.c[2];
	asMessage->msgContent[asMessage->msgLen + 3] = intConvert.c[3];
	asMessage->msgLen += 4;

	// Group Index (4 bytes)
	intConvert.value = groupIndex;
	asMessage->msgContent[asMessage->msgLen] = intConvert.c[0];
	asMessage->msgContent[asMessage->msgLen + 1] = intConvert.c[1];
	asMessage->msgContent[asMessage->msgLen + 2] = intConvert.c[2];
	asMessage->msgContent[asMessage->msgLen + 3] = intConvert.c[3];
	asMessage->msgLen += 4;
		
	// Block length field
	intConvert.value = asMessage->msgLen - MSG_HEADER_LEN;
	asMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
	asMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
	asMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
	asMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
	
	// Body length field
	intConvert.value = asMessage->msgLen - MSG_HEADER_LEN;
	asMessage->msgContent[2] = intConvert.c[0];
	asMessage->msgContent[3] = intConvert.c[1];
	asMessage->msgContent[4] = intConvert.c[2];
	asMessage->msgContent[5] = intConvert.c[3];

	return asMessage->msgLen;
}

/****************************************************************************
- Function name:	SendPMDataUnhandledOnGroup
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int SendPMDataUnhandledOnGroup(int serviceIndex, int groupIndex)
{
	t_ConnectionStatus *connection = (t_ConnectionStatus *)&asCollection.connection;

	// Check connection between AS and PM
	if ((connection->status == DISCONNECTED) || (connection->socket == -1))
	{
		return ERROR;
	}

	int result;
	t_ASMessage asMessage;
	
	// Build stock symbol status message to Send PM 
	BuildASMessageForDataUnhandledWarning(&asMessage, serviceIndex, groupIndex);

	// Send message to AS
	result = SendASMessage(connection, &asMessage);

	if (result == ERROR)
	{
		TraceLog(ERROR_LEVEL, "Cannot send Data Unhandled warning status to AS, socket = %d\n", connection->socket);
	}
	
	return result;
}


/****************************************************************************
- Function name:	ProcessSetExchangeConfFromAS
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessSetExchangeConfFromAS(void *message)
{
	t_ASMessage *asMessage = (t_ASMessage *)message;
	
	TraceLog(DEBUG_LEVEL, "Received message to set exchange conf from AS\n");
	
	int currentIndex = MSG_HEADER_LEN;

	// Update current index
	currentIndex += 4;

	// AS Message: MsgType(2) + BodyLen(4) + BlockLen(4) + ExchangeStatusList

	int i, arrayIndex;
	int isFlush = 0;
	for (i = 0; i < MAX_EXCHANGE; i++)
	{
		//exchangeId
		char exchangeId = asMessage->msgContent[currentIndex];
		arrayIndex = ParticipantToExchangeIndexMapping[exchangeId - 65];
		ExchangeStatusList[arrayIndex].exchangeId = exchangeId;
		currentIndex += 1;
		
		//ECNToPlace
		if (strncmp((char *)&asMessage->msgContent[currentIndex], "RASH", 4) == 0)
		{
			ExchangeStatusList[arrayIndex].ECNToPlace = TYPE_NASDAQ_RASH;
		}
		else
		{
			ExchangeStatusList[arrayIndex].ECNToPlace = TYPE_ARCA_DIRECT;
		}
		currentIndex += MAX_LINE_LEN;
		
		// ------------------------------
		// 			CQS Status
		// ------------------------------
		isFlush = ExchangeStatusList[arrayIndex].status[CQS_SOURCE].enable;
		ExchangeStatusList[arrayIndex].status[CQS_SOURCE].visible = asMessage->msgContent[currentIndex];
		ExchangeStatusList[arrayIndex].status[CQS_SOURCE].enable = asMessage->msgContent[currentIndex + 1];
		
		if ((isFlush == TRUE) && (ExchangeStatusList[arrayIndex].status[CQS_SOURCE].enable == FALSE))
		{
			// Status changed from ENABLED --> DISABLE, we need to flush all quotes for this exchange.
			FlushPMBBOQuotesByFeed(ExchangeStatusList[arrayIndex].exchangeId, CQS_SOURCE);
		}
		currentIndex += 2;
		
		// ------------------------------
		// 			UQDF Status
		// ------------------------------
		isFlush = ExchangeStatusList[arrayIndex].status[UQDF_SOURCE].enable;
		ExchangeStatusList[arrayIndex].status[UQDF_SOURCE].visible = asMessage->msgContent[currentIndex];
		ExchangeStatusList[arrayIndex].status[UQDF_SOURCE].enable = asMessage->msgContent[currentIndex + 1];
		if ((isFlush == TRUE) && (ExchangeStatusList[arrayIndex].status[UQDF_SOURCE].enable == FALSE))
		{
			// Status changed from ENABLED --> DISABLE, we need to flush all quotes for this exchange.
			FlushPMBBOQuotesByFeed(ExchangeStatusList[arrayIndex].exchangeId, UQDF_SOURCE);
		}
		currentIndex += 2;
	}
	
	SaveExchangesConf();
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessGetBBOQuoteListFromAS
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessGetBBOQuoteListFromAS(void *message)
{
	t_ASMessage *asMessage = (t_ASMessage *)message;

	int currentIndex = MSG_HEADER_LEN;

	//Update current index
	currentIndex += 4;

	//Get TT ID
	int ttIndex = GetIntNumber(&asMessage->msgContent[currentIndex]);
	currentIndex += 1;

	//Get Stock Symbol
	char symbol[SYMBOL_LEN];
	strncpy(symbol, (char *)&asMessage->msgContent[currentIndex], SYMBOL_LEN);

	TraceLog(DEBUG_LEVEL, "Received message to get BBO quotes from AS, symbol = %.8s\n", symbol);

	int symbolIndex = GetStockSymbolIndex(symbol);
	if (symbolIndex == -1)
	{
		TraceLog(ERROR_LEVEL, "Symbol (%.8s) is not in the list --> can't get BBO quotes!\n", symbol);
		return SUCCESS;
	}

	if (SendBBOQuotesToAS(ttIndex, symbol) == ERROR)
	{
		return ERROR;
	}

	return SUCCESS;
}

/****************************************************************************
- Function name:	SendBBOQuotesToAS
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int SendBBOQuotesToAS(int ttIndex, char *stockSymbol)
{
	t_ConnectionStatus *connection = (t_ConnectionStatus *)&asCollection.connection;

	//Check connection between AS and PM
	if (connection->status == DISCONNECTED || connection->socket == -1)
	{
		return ERROR;
	}

	int result;
	t_ASMessage asMessage;

	//Build message BBO Quote to send AS
	result = BuildASMessageForBBOQuotesList(&asMessage, ttIndex, stockSymbol);

	if (result == 0)
	{
		// There is nothing to send (result = 0)!
		return SUCCESS;
	}

	//Send Message to AS
	result = SendASMessage(connection, &asMessage);

	if (result == ERROR)
	{
		TraceLog(ERROR_LEVEL, "Can not send BBO Quotes List to AS, socket = %d\n", connection->socket);
	}

	return result;
}


/****************************************************************************
- Function name:	BuildASMessageForBBOQuotesList
- Input:			N/A
- Output:		N/A
- Return:		Success or ERROR
- Description:	+ The routine hides the following facts
				  - [To be done]
				+ There are preconditions guaranteed to the routine
				+ The routine guarantees that the status value will
					have a value of either
				  - Success
					or
				  - ERROR
- Usage:			N/A
*********************************************************************************/
int BuildASMessageForBBOQuotesList(void *message, int ttIndex, char *stockSymbol)
{
	t_ShortConverter shortNum;
	t_IntConverter intNum;
	t_DoubleConverter doubleNum;

	t_ASMessage *asMessage = (t_ASMessage *)message;

	//Initialize message
	asMessage->msgLen = 0;

	//Message Type
	shortNum.value = MSG_TYPE_FOR_SEND_BBO_QUOTE_LIST_TO_AS;
	asMessage->msgContent[asMessage->msgLen] = shortNum.c[0];
	asMessage->msgContent[asMessage->msgLen + 1] = shortNum.c[1];
	asMessage->msgLen += 2;

	//Update body length
	asMessage->msgLen += 4;

	//Stock Symbol
	int symbolIndex = GetStockSymbolIndex(stockSymbol);
	if (symbolIndex == -1) return 0;
	
	// Block content = Block 1 + Block 2 + Block 3

	// Block 1 = ttIndex + Stock Symbol Information
	intNum.value = 4 + 1 + SYMBOL_LEN;
	memcpy(&asMessage->msgContent[asMessage->msgLen], intNum.c, 4);
	asMessage->msgLen += 4;

	//ttIndex
	asMessage->msgContent[asMessage->msgLen] = ttIndex;
	asMessage->msgLen += 1;

	//Stock Symbol
	strncpy((char *)&asMessage->msgContent[asMessage->msgLen], stockSymbol, SYMBOL_LEN);
	asMessage->msgLen += SYMBOL_LEN;

	//Block 2 = ASK Block
	//Block 3 = BID Block

	// Get current quote information of current stock symbol
	// ASK Block
	char askBlock[(1 + 4 + 8) * MAX_BBO_SOURCE * MAX_EXCHANGE];
	int askBlockIndex = 0;
	int askBlockSize = 0;

	// BID Block
	char bidBlock[(1 + 4 + 8) * MAX_BBO_SOURCE * MAX_EXCHANGE];
	int bidBlockIndex = 0;
	int bidBlockSize = 0;

	t_BBOQuoteList *symbolInfo;
	symbolInfo = &BBOQuoteMgmt[symbolIndex];

	int sourceId, i;
	for (sourceId = 0; sourceId < MAX_BBO_SOURCE; sourceId++)
	{
		for (i = 0; i < MAX_EXCHANGE; i++)
		{
			if (symbolInfo->BBOQuoteInfo[sourceId][i].participantId != 0)
			{
				if (symbolInfo->BBOQuoteInfo[sourceId][i].offerSize > 0)
				{
					//participantID
					askBlock[askBlockIndex] = symbolInfo->BBOQuoteInfo[sourceId][i].participantId;
					askBlockIndex += 1;

					//offerSize
					intNum.value = symbolInfo->BBOQuoteInfo[sourceId][i].offerSize;
					memcpy(&askBlock[askBlockIndex], intNum.c, 4);
					askBlockIndex += 4;

					//offerPrice
					doubleNum.value = symbolInfo->BBOQuoteInfo[sourceId][i].offerPrice;
					memcpy(&askBlock[askBlockIndex], doubleNum.c, 8);
					askBlockIndex += 8;

					askBlockSize += 1 + 4 + 8;
				}

				if (symbolInfo->BBOQuoteInfo[sourceId][i].bidSize > 0)
				{
					//participantID
					bidBlock[bidBlockIndex] = symbolInfo->BBOQuoteInfo[sourceId][i].participantId;
					bidBlockIndex += 1;

					//bidSize
					intNum.value = symbolInfo->BBOQuoteInfo[sourceId][i].bidSize;
					memcpy(&bidBlock[bidBlockIndex], intNum.c, 4);
					bidBlockIndex += 4;

					//bidPrice
					doubleNum.value = symbolInfo->BBOQuoteInfo[sourceId][i].bidPrice;
					memcpy(&bidBlock[bidBlockIndex], doubleNum.c, 8);
					bidBlockIndex += 8;

					bidBlockSize += 1 + 4 + 8;
				}
			}
		}
	}

	if ((askBlockSize == 0) && (bidBlockSize == 0))
	{
		//There is nothing to send!
		return 0;
	}

	// Block length 2 = ASK Block Size
	intNum.value = askBlockSize + 4;
	memcpy(&asMessage->msgContent[asMessage->msgLen], intNum.c, 4);
	asMessage->msgLen += 4;

	// ASK Block
	if (askBlockSize > 0)
	{
		memcpy(&asMessage->msgContent[asMessage->msgLen], askBlock, askBlockSize);
		asMessage->msgLen += askBlockSize;
	}

	// Block length 3 = BID Block Size
	intNum.value = bidBlockSize + 4;
	memcpy(&asMessage->msgContent[asMessage->msgLen], intNum.c, 4);
	asMessage->msgLen += 4;

	// BID Block
	if (bidBlockSize > 0)
	{
		memcpy(&asMessage->msgContent[asMessage->msgLen], bidBlock, bidBlockSize);
		asMessage->msgLen += bidBlockSize;
	}

	//Update Body length
	intNum.value = 4 + 1 + SYMBOL_LEN + askBlockSize + 4 + bidBlockSize + 4;
	memcpy(&asMessage->msgContent[2], intNum.c, 4);

	return asMessage->msgLen;
}


/****************************************************************************
- Function name:	ProcessFlushBBOQuotesFromAS
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessFlushBBOQuotesFromAS(void *message)
{
	t_ASMessage *asMessage = (t_ASMessage *)message;

	// Offset 10: Ignore MsgType, BodyLen, BlockLen
	char exchangeCode = asMessage->msgContent[10];

	TraceLog(DEBUG_LEVEL, "Receive message to flush BBO quotes from AS, Exchange = %c\n", exchangeCode);

	FlushPMBBOQuotes(exchangeCode);
	
	return SUCCESS;
}



/****************************************************************************
- Function name:	ProcessFlushBookOfASymbol
- Input:			+ message
- Output:		N/A
- Return:		Success or Failure
- Description:	+ The routine hides the following facts
				  - Process disconnect message from AS	
				+ There are preconditions guaranteed to the routine	
				  - message must be allocated and content some value before
				+ The routine guarantees that the status value will 
					have a value of either
				  - Success or Failure	
- Usage:			N/A
****************************************************************************/
int ProcessFlushBookOfASymbol(void *message)
{
	t_ASMessage *asMessage = (t_ASMessage *)message;
	
	int currentIndex = MSG_HEADER_LEN;
	
	//Update block length
	currentIndex += 4;
	
	//ECN ID
	int ecnID = asMessage->msgContent[currentIndex];
	currentIndex++;
	
	//Stocksymbol
	char stockSymbol[SYMBOL_LEN];
	strncpy(stockSymbol, (char*) &asMessage->msgContent[currentIndex], SYMBOL_LEN);
	
	//Flush of Stock Symbol
	TraceLog(DEBUG_LEVEL, "Received request to flush book of symbol %.8s of ECN Book %d\n", stockSymbol, ecnID);
	int stockSymbolIndex = GetStockSymbolIndex(stockSymbol);
	
	if (stockSymbolIndex < 0)
	{
		TraceLog(DEBUG_LEVEL, "PM is not support stock symbol (%.8s)\n", stockSymbol);
		return SUCCESS;
	}
	
	if (manFlushBookFlag[ecnID] == NO && bookStatusMgmt[ecnID].shouldConnect == YES)
	{
		manFlushBookStockSymbolIndex[ecnID] = stockSymbolIndex;
		manFlushBookFlag[ecnID] = YES;
	}
		
	return SUCCESS;
}


/****************************************************************************
- Function name:	ProcessFlushBookOfAllSymbol
- Input:			+ message
- Output:		N/A
- Return:		Success or Failure
- Description:	+ The routine hides the following facts
				  - Process disconnect message from AS	
				+ There are preconditions guaranteed to the routine	
				  - message must be allocated and content some value before
				+ The routine guarantees that the status value will 
					have a value of either
				  - Success or Failure	
- Usage:			N/A
****************************************************************************/
int ProcessFlushBookOfAllSymbols(void *message)
{
	t_ASMessage *asMessage = (t_ASMessage *)message;
	
	int currentIndex = MSG_HEADER_LEN;
	
	//Update block length
	currentIndex += 4;
	
	//ECN Book ID
	int ecnID = asMessage->msgContent[currentIndex];
	currentIndex++;
	
	//Flush Books of all stock symbols
	TraceLog(DEBUG_LEVEL, "Received request to flush (%d) BOOK\n", ecnID);
	
	switch (ecnID)
	{
		case BOOK_ARCA:
			TraceLog(DEBUG_LEVEL, "Received request to flush ARCA BOOK\n");
			// Turn ON isFlushBook
			bookStatusMgmt[BOOK_ARCA].isFlushBook = YES;
			
			DisconnectARCABook();
			
			break;
		case BOOK_NASDAQ:
			TraceLog(DEBUG_LEVEL, "Received request to flush NASDAQ BOOK\n");
			// Turn ON isFlushBook
			bookStatusMgmt[BOOK_NASDAQ].isFlushBook = YES;
			
			DisconnectNASDAQBook();
			
			break;
		default:
			break;
	}
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	SendASMessageToRequestSendStuckInfo
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int SendASMessageToRequestSendStuckInfo(t_Position *stuck)
{
	t_ConnectionStatus *connection = (t_ConnectionStatus *)&asCollection.connection;

	// Check connection between AS and PM
	if ((connection->status == DISCONNECTED) || (connection->socket == -1))
	{
		return ERROR;
	}

	int result;
	t_ASMessage asMessage;
	
	result = BuildASMessageForRequestSendStuckInfo(&asMessage, stuck);

	if (result == 0)
	{
		return SUCCESS;
	}

	// Send message to AS
	result = SendASMessage(connection, &asMessage);

	if (result == ERROR)
	{
		TraceLog(ERROR_LEVEL, "Cannot send AS message to get stuck info for resume handling stuck, socket = %d\n", connection->socket);
	}
	
	return result;
}

/****************************************************************************
- Function name:	BuildASMessageForRequestSendStuckInfo
- Input:			N/A
- Output:		
- Return:		
- Description:	
- Usage:			
*********************************************************************************/
int BuildASMessageForRequestSendStuckInfo(void *message, t_Position *stuck)
{
	t_ShortConverter shortNum;
	t_IntConverter intNum;

	t_ASMessage *asMessage = (t_ASMessage *)message;
	
	// Initialize message
	asMessage->msgLen = 0;
	
	// Message type
	shortNum.value = MSG_TYPE_FOR_SEND_AS_MESSAGE_TO_GET_STUCK_INFO;
	asMessage->msgContent[0] = shortNum.c[0];
	asMessage->msgContent[1] = shortNum.c[1];
	asMessage->msgLen += 2;

	// Update index of body length
	asMessage->msgLen += 4;
	
	// Update index of block length
	asMessage->msgLen += 4;
	
	// Symbol
	memcpy(&asMessage->msgContent[asMessage->msgLen], stuck->symbol, SYMBOL_LEN);
	asMessage->msgLen += SYMBOL_LEN;
	
	//shares
	intNum.value = stuck->shares;
	memcpy(&asMessage->msgContent[asMessage->msgLen], intNum.c, 4);
	asMessage->msgLen += 4;
	
	// Side
	asMessage->msgContent[asMessage->msgLen] = stuck->side;
	asMessage->msgLen += 1;
	
	// Account
	asMessage->msgContent[asMessage->msgLen] = stuck->accountSuffix;
	asMessage->msgLen += 1;
	
	// Block length field
	intNum.value = asMessage->msgLen - MSG_HEADER_LEN;
	asMessage->msgContent[MSG_HEADER_LEN] = intNum.c[0];
	asMessage->msgContent[MSG_HEADER_LEN + 1] = intNum.c[1];
	asMessage->msgContent[MSG_HEADER_LEN + 2] = intNum.c[2];
	asMessage->msgContent[MSG_HEADER_LEN + 3] = intNum.c[3];
	
	// Body length field
	intNum.value = asMessage->msgLen - MSG_HEADER_LEN;
	asMessage->msgContent[2] = intNum.c[0];
	asMessage->msgContent[3] = intNum.c[1];
	asMessage->msgContent[4] = intNum.c[2];
	asMessage->msgContent[5] = intNum.c[3];
	
	return asMessage->msgLen;
}

/****************************************************************************
- Function name:	SendNASDAQ_RASH_SeqToAS
- Input:			N/A
- Output:		
- Return:		
- Description:	
- Usage:			
*********************************************************************************/
int SendNASDAQ_RASH_SeqToAS()
{
	t_ConnectionStatus *connection = (t_ConnectionStatus *)&asCollection.connection;

	// Check connection between AS and PM
	if ((connection->status == DISCONNECTED) || (connection->socket == -1))
	{
		return ERROR;
	}

	int result;
	t_ASMessage asMessage;
	
	BuildASMessageForRASHSeqNumber(&asMessage);

	// Send message to AS
	result = SendASMessage(connection, &asMessage);

	if (result == ERROR)
	{
		TraceLog(ERROR_LEVEL, "Cannot send RASH sequence number to AS\n");
	}
	return result;
}


/****************************************************************************
- Function name:	BuildASMessageForRASHSeqNumber
- Input:			N/A
- Output:		
- Return:		
- Description:	
- Usage:			
*********************************************************************************/
int BuildASMessageForRASHSeqNumber(void *message)
{
	t_ShortConverter shortNum;
	t_IntConverter intNum;

	t_ASMessage *asMessage = (t_ASMessage *)message;
	
	// Initialize message
	asMessage->msgLen = 0;
	
	// Message type
	shortNum.value = MSG_TYPE_FOR_SEND_RASH_SEQ_TO_AS;
	asMessage->msgContent[0] = shortNum.c[0];
	asMessage->msgContent[1] = shortNum.c[1];
	asMessage->msgLen += 2;

	// Update index of body length
	asMessage->msgLen += 4;
	
	// Update index of block length
	asMessage->msgLen += 4;
	
	// Incomming sequence number
	intNum.value = NASDAQ_RASH_Config.incomingSeqNum;
	memcpy(&asMessage->msgContent[asMessage->msgLen], intNum.c, 4);
	asMessage->msgLen += 4;
	
	// Block length field
	intNum.value = asMessage->msgLen - MSG_HEADER_LEN;
	asMessage->msgContent[MSG_HEADER_LEN] = intNum.c[0];
	asMessage->msgContent[MSG_HEADER_LEN + 1] = intNum.c[1];
	asMessage->msgContent[MSG_HEADER_LEN + 2] = intNum.c[2];
	asMessage->msgContent[MSG_HEADER_LEN + 3] = intNum.c[3];
	
	// Body length field
	intNum.value = asMessage->msgLen - MSG_HEADER_LEN;
	asMessage->msgContent[2] = intNum.c[0];
	asMessage->msgContent[3] = intNum.c[1];
	asMessage->msgContent[4] = intNum.c[2];
	asMessage->msgContent[5] = intNum.c[3];
	
	return asMessage->msgLen;
}

/****************************************************************************
- Function name:	SendPMDisconnectedBookOrderAlertToAS
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int SendPMDisconnectedBookOrderAlertToAS(int typeServer)
{
	t_ConnectionStatus *connection = (t_ConnectionStatus *)&asCollection.connection;

		// Check connection between AS and PM
	if ((connection->status == DISCONNECTED) || (connection->socket == -1))
	{
		return ERROR;
	}

	TraceLog(DEBUG_LEVEL, "Send PM disconnected book/order alert to AS\n");

	int result;
	t_ASMessage asMessage;

	// Build PM disengage alert message to send to AS
	BuildASMessageForPMDisconnetedBookOrderAlert(&asMessage, typeServer);

	// Send message to AS
	result = SendASMessage(connection, &asMessage);

	if (result == ERROR)
	{
		TraceLog(ERROR_LEVEL, "Cannot send PM book/order disconnect alert to AS, socket = %d\n", connection->socket);
	}

	return result;
}

/****************************************************************************
- Function name:	BuildASMessageForPMDisconnetBookOrderAlert
- Input:			N/A
- Output:
- Return:
- Description:
- Usage:
*********************************************************************************/
int BuildASMessageForPMDisconnetedBookOrderAlert(void *message, int serviceType)
{
	t_ShortConverter shortNum;
	t_IntConverter intNum;

	t_ASMessage *asMessage = (t_ASMessage *)message;

	// Initialize message
	asMessage->msgLen = 0;

	// Message type
	shortNum.value = MSG_TYPE_FOR_SEND_DISCONNECTED_BOOK_ORDER_ALERT_TO_AS;
	asMessage->msgContent[0] = shortNum.c[0];
	asMessage->msgContent[1] = shortNum.c[1];
	asMessage->msgLen += 2;

	// Update index of body length
	asMessage->msgLen += 4;

	// Update index of block length
	asMessage->msgLen += 4;

	//service type
	intNum.value = serviceType;
	asMessage->msgContent[asMessage->msgLen] = intNum.c[0];
	asMessage->msgContent[asMessage->msgLen + 1] = intNum.c[1];
	asMessage->msgContent[asMessage->msgLen + 2] = intNum.c[2];
	asMessage->msgContent[asMessage->msgLen + 3] = intNum.c[3];
	asMessage->msgLen += 4;

	// Block length field
	intNum.value = asMessage->msgLen - MSG_HEADER_LEN;
	asMessage->msgContent[MSG_HEADER_LEN] = intNum.c[0];
	asMessage->msgContent[MSG_HEADER_LEN + 1] = intNum.c[1];
	asMessage->msgContent[MSG_HEADER_LEN + 2] = intNum.c[2];
	asMessage->msgContent[MSG_HEADER_LEN + 3] = intNum.c[3];

	// Body length field
	intNum.value = asMessage->msgLen - MSG_HEADER_LEN;
	asMessage->msgContent[2] = intNum.c[0];
	asMessage->msgContent[3] = intNum.c[1];
	asMessage->msgContent[4] = intNum.c[2];
	asMessage->msgContent[5] = intNum.c[3];

	return asMessage->msgLen;
}

/****************************************************************************
- Function name:	SendPMTradingDisabledAlertToAS
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int SendPMDisabledTradingAlertToAS(short reason)
{
	t_ConnectionStatus *connection = (t_ConnectionStatus *)&asCollection.connection;

		// Check connection between AS and PM
	if ((connection->status == DISCONNECTED) || (connection->socket == -1))
	{
		return ERROR;
	}

	int result;
	t_ASMessage asMessage;

	// Build PM trading disabled alert message to send to AS
	BuildASMessageForPMDiabledTradingAlert(&asMessage, reason);

	// Send message to AS
	result = SendASMessage(connection, &asMessage);

	if (result == ERROR)
	{
		TraceLog(ERROR_LEVEL, "Cannot send PM trading disabled alert to AS, socket = %d\n", connection->socket);
	}

	return result;
}

/****************************************************************************
- Function name:	BuildASMessageForPMDiabledTradingAlert
- Input:			N/A
- Output:
- Return:
- Description:
- Usage:
*********************************************************************************/
int BuildASMessageForPMDiabledTradingAlert(void *message, short reason)
{
	t_ShortConverter shortNum;
	t_IntConverter intNum;

	t_ASMessage *asMessage = (t_ASMessage *)message;

	// Initialize message
	asMessage->msgLen = 0;

	// Message type
	shortNum.value = MSG_TYPE_FOR_SEND_DISABLED_TRADING_ALERT_TO_AS;
	asMessage->msgContent[0] = shortNum.c[0];
	asMessage->msgContent[1] = shortNum.c[1];
	asMessage->msgLen += 2;

	// Update index of body length
	asMessage->msgLen += 4;

	// Update index of block length
	asMessage->msgLen += 4;

	//service type
	memcpy(&asMessage->msgContent[asMessage->msgLen], &reason, 2);
    asMessage->msgLen += 2;

	// Block length field
	intNum.value = asMessage->msgLen - MSG_HEADER_LEN;
	asMessage->msgContent[MSG_HEADER_LEN] = intNum.c[0];
	asMessage->msgContent[MSG_HEADER_LEN + 1] = intNum.c[1];
	asMessage->msgContent[MSG_HEADER_LEN + 2] = intNum.c[2];
	asMessage->msgContent[MSG_HEADER_LEN + 3] = intNum.c[3];

	// Body length field
	intNum.value = asMessage->msgLen - MSG_HEADER_LEN;
	asMessage->msgContent[2] = intNum.c[0];
	asMessage->msgContent[3] = intNum.c[1];
	asMessage->msgContent[4] = intNum.c[2];
	asMessage->msgContent[5] = intNum.c[3];

	return asMessage->msgLen;
}

/****************************************************************************
- Function name:	CheckOpenPosition
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int CheckOpenPosition(t_Position *stuckInfo)
{
	if (asCollection.currentStatus.tradingStatus == DISABLED)
	{
		SendPMStuckStatusAlertToAS(stuckInfo, NO, STUCK_STATUS_REASON_DISABLED_TRADING);
		return ERROR;
	}
	
	int symbolIndex = GetStockSymbolIndex(stuckInfo->symbol);
	if (symbolIndex == -1)
	{
		symbolIndex = UpdateStockSymbolIndex(stuckInfo->symbol);
		if (symbolIndex == -1)
		{
			SendPMStuckStatusAlertToAS(stuckInfo, NO, STUCK_STATUS_REASON_CAN_NOT_FIND_SYMBOL);
			return ERROR;
		}
	}
	
	if (symbolIgnoreStatus[symbolIndex] == DISABLE)
	{
		SendPMStuckStatusAlertToAS(stuckInfo, NO, STUCK_STATUS_REASON_SYMBOL_IGNORED);
		return ERROR;
	}
	
	int tradeSymbolStatusIndex = GetTradeSymbolIndex(stuckInfo->symbol);
	if (tradeSymbolStatusIndex == -1)
	{
		TraceLog(ERROR_LEVEL, "ProcessASMessageForStuckInfo: Cannot update symbol = %.8s to TradeSymbolList\n", stuckInfo->symbol);
		return ERROR;
	}

	SendPMStuckStatusAlertToAS(stuckInfo, YES, STUCK_STATUS_REASON_NONE);

	return SUCCESS;
}

/****************************************************************************
- Function name:	SendPMStuckStatusAlertToAS
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int SendPMStuckStatusAlertToAS(t_Position *stuck, int status, int reason)
{
	t_ConnectionStatus *connection = (t_ConnectionStatus *)&asCollection.connection;

	// Check connection between AS and PM
	if ((connection->status == DISCONNECTED) || (connection->socket == -1))
	{
		return ERROR;
	}

	int result;
	t_ASMessage asMessage;

	// Build PM does not accepted alert message to send to AS
	BuildASMessageForPMStuckStatusAlert(&asMessage, stuck, status, reason);

	// Send message to AS
	result = SendASMessage(connection, &asMessage);

	if (result == ERROR)
	{
		TraceLog(ERROR_LEVEL, "Cannot send PM stuck status alert to AS, socket = %d\n", connection->socket);
	}

	return result;
}

/****************************************************************************
- Function name:	BuildASMessageForPMStuckStatusAlert
- Input:			N/A
- Output:
- Return:
- Description:
- Usage:
*********************************************************************************/
int BuildASMessageForPMStuckStatusAlert(void *message, t_Position *stuck, int status, int reason)
{
	t_ShortConverter shortNum;
	t_IntConverter intNum;
	t_DoubleConverter doubleNum;

	t_ASMessage *asMessage = (t_ASMessage *)message;

	// Initialize message
	asMessage->msgLen = 0;

	// Message type
	shortNum.value = MSG_TYPE_FOR_SEND_DOES_NOT_ACCEPT_ALERT_TO_AS;
	asMessage->msgContent[0] = shortNum.c[0];
	asMessage->msgContent[1] = shortNum.c[1];
	asMessage->msgLen += 2;

	// Update index of body length
	asMessage->msgLen += 4;

	// Update index of block length
	asMessage->msgLen += 4;

	// Symbol
	memcpy(&asMessage->msgContent[asMessage->msgLen], stuck->symbol, SYMBOL_LEN);
	asMessage->msgLen += SYMBOL_LEN;

	// Price
	doubleNum.value = stuck->price;
	memcpy(&asMessage->msgContent[asMessage->msgLen], doubleNum.c, 8);
	asMessage->msgLen += 8;

	// Share
	intNum.value = stuck->shares;
	memcpy(&asMessage->msgContent[asMessage->msgLen], intNum.c, 4);
	asMessage->msgLen += 4;

	// Side
	asMessage->msgContent[asMessage->msgLen] = stuck->side;
	asMessage->msgLen += 1;
	
	// Account
	asMessage->msgContent[asMessage->msgLen] = stuck->accountSuffix;
	asMessage->msgLen += 1;

	//Status
	asMessage->msgContent[asMessage->msgLen] = (unsigned char)status;
	asMessage->msgLen += 1;

	// Reason code, explains why the stuck is disengaged
	asMessage->msgContent[asMessage->msgLen] = (unsigned char)reason;
	asMessage->msgLen += 1;

	// Block length field
	intNum.value = asMessage->msgLen - MSG_HEADER_LEN;
	asMessage->msgContent[MSG_HEADER_LEN] = intNum.c[0];
	asMessage->msgContent[MSG_HEADER_LEN + 1] = intNum.c[1];
	asMessage->msgContent[MSG_HEADER_LEN + 2] = intNum.c[2];
	asMessage->msgContent[MSG_HEADER_LEN + 3] = intNum.c[3];

	// Body length field
	intNum.value = asMessage->msgLen - MSG_HEADER_LEN;
	asMessage->msgContent[2] = intNum.c[0];
	asMessage->msgContent[3] = intNum.c[1];
	asMessage->msgContent[4] = intNum.c[2];
	asMessage->msgContent[5] = intNum.c[3];

	return asMessage->msgLen;
}


/****************************************************************************
- Function name:	CheckDisconnecOrdersAndSendAlertDisableTrading
- Input:			N/A
- Output:			N/A
- Return:			N/A
- Description:		+ The routine hides the following facts
					+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
int CheckAndSendAlertDisableTrading(int disconnectType)
{
	// Check all books is disconnected or not, if true then disable trading
	int allBooksDisconnected = 1;
	
	if ((bookStatusMgmt[BOOK_ARCA].isConnected == CONNECTED) ||
		(bookStatusMgmt[BOOK_ARCA].shouldConnect == YES))
	{
		allBooksDisconnected = 0;
	}
	
	if ((bookStatusMgmt[BOOK_NASDAQ].isConnected == CONNECTED) ||
		(bookStatusMgmt[BOOK_NASDAQ].shouldConnect == YES))
	{
		allBooksDisconnected = 0;
	}
	
	// Check all orders is disconnected or not, if true then disable trading
	int allOrdersDisconnected = 1;
	
	if ((orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == CONNECTED) ||
		(orderStatusMgmt[ARCA_DIRECT_INDEX].shouldConnect == YES))
	{
		allOrdersDisconnected = 0;
	}
	
	if ((orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == CONNECTED) ||
		(orderStatusMgmt[NASDAQ_RASH_INDEX].shouldConnect == YES))
	{
		allOrdersDisconnected = 0;
	}

	// Books
	if (disconnectType == 1)
	{
		if (allBooksDisconnected == 1)
		{
			if (asCollection.currentStatus.tradingStatus == ENABLE)
			{
				SendPMDisabledTradingAlertToAS(DISABLE_TRADING_BY_ALL_BOOK_DISCONNECTED);
			}

			asCollection.currentStatus.tradingStatus = DISABLED;
			TraceLog(DEBUG_LEVEL, "All MDCs were disconnected. Disable trading on PM!\n");
		}
	} 
	// Orders
	else if (disconnectType == 2)
	{
		if (allOrdersDisconnected == 1)
		{
			if (asCollection.currentStatus.tradingStatus == ENABLE)
			{
				SendPMDisabledTradingAlertToAS(DISABLE_TRADING_BY_ALL_ORDER_DISCONNECTED);
			}

			asCollection.currentStatus.tradingStatus = DISABLED;
			TraceLog(DEBUG_LEVEL, "All OECs were disconnected. Disable trading on PM!\n");
		}
	}
	// Both books & orders
	else
	{
		if ((allBooksDisconnected == 1) && (allOrdersDisconnected == 1))
		{
			asCollection.currentStatus.tradingStatus = DISABLED;
			TraceLog(DEBUG_LEVEL, "All MDCs and OECs were disconnected. Disable trading on PM!\n");
		}
	}

	return SUCCESS;
}

/****************************************************************************
- Function name:	SendPMStuckStatusAlertToAS
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int SendPMSleepingSymbolsToAS()
{
	t_ConnectionStatus *connection = (t_ConnectionStatus *)&asCollection.connection;

	// Check connection between AS and PM
	if ((connection->status == DISCONNECTED) || (connection->socket == -1))
	{
		return ERROR;
	}

	int result;
	t_ASMessage asMessage;

	// Build PM does not accepted alert message to send to AS
	BuildASMessageForPMSleepingSymbols(&asMessage);

	// Send message to AS
	result = SendASMessage(connection, &asMessage);

	if (result == ERROR)
	{
		TraceLog(ERROR_LEVEL, "Cannot send PM sleeping symbols to AS, socket = %d\n", connection->socket);
	}

	return result;
}

/****************************************************************************
- Function name:	BuildASMessageForPMSleepingSymbols
- Input:			N/A
- Output:		
- Return:		
- Description:	
- Usage:			
*********************************************************************************/
int BuildASMessageForPMSleepingSymbols(void *message)
{
	short shortValue;
	int intValue;

	t_ASMessage *asMessage = (t_ASMessage *)message;
	
	// Initialize message
	asMessage->msgLen = 0;
	
	// Message type
	shortValue = MSG_TYPE_FOR_SEND_SLEEPING_SYMBOL_TO_AS;
	memcpy(asMessage->msgContent, &shortValue, 2);
	asMessage->msgLen += 2;

	// Update index of body length
	asMessage->msgLen += 4;
	
	// Update index of block length
	asMessage->msgLen += 4;
	
	// index of number of symbol
	asMessage->msgLen += 4;
	
	int i, symbolIndex;
	int numOfSymbol = 0;
	
	char numOfSleepingSymbols[MAX_STOCK_SYMBOL];
	memset(numOfSleepingSymbols, 1, MAX_STOCK_SYMBOL);
	
	for (i = 0; i < asOpenOrder.countOpenOrder; i++)
	{
		symbolIndex = asOpenOrder.orderSummary[i].symbolIndex;

		if(positionCollection[symbolIndex].replaceAfterSleeping == YES && numOfSleepingSymbols[symbolIndex] == 1)
		{
			numOfSymbol++;
			
			numOfSleepingSymbols[symbolIndex] = 0;
			
			// symbol
			memcpy(&asMessage->msgContent[asMessage->msgLen], positionCollection[symbolIndex].stuck.symbol, SYMBOL_LEN);
			asMessage->msgLen += SYMBOL_LEN;
			
			// account
			asMessage->msgContent[asMessage->msgLen] = positionCollection[symbolIndex].stuck.accountSuffix;
			asMessage->msgLen += 1;
		}
	}
	
	// number of symbol
	memcpy(&asMessage->msgContent[10], &numOfSymbol, 4);
	
	// Block length field
	intValue = asMessage->msgLen - MSG_HEADER_LEN;
	memcpy(&asMessage->msgContent[6], &intValue, 4);
	
	// Body length field
	intValue = asMessage->msgLen - MSG_HEADER_LEN;
	memcpy(&asMessage->msgContent[2], &intValue, 4);
	
	return asMessage->msgLen;
}
/***************************************************************************/
