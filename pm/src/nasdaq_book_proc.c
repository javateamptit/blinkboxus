/****************************************************************************
** Project name:	Equity Arbitrage Application
** File name: 		nasdaq_book_proc.c
** Description: 	

** Author: 		Danh Thai - Hoang
** First created on 29 September 2007
** Last updated on 01 October 2007
****************************************************************************/
#define _GNU_SOURCE

#define MAX_MSG_IN_PACKET 35
#define MAX_ALLOWED_RECOVER_GAPPED_PACKET 30

#include <pthread.h>
#include <time.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <math.h>
#include <semaphore.h>
#include <errno.h>
	   
#include "arca_book_proc.h"
#include "configuration.h"
#include "utility.h"
#include "nasdaq_book_proc.h"
#include "trading_mgmt.h"
#include "book_mgmt.h"
#include "kernel_algorithm.h"
#include "socket_util.h"
#include "aggregation_server_proc.h"

t_NDAQ_MoldUDP_Book NDAQ_MoldUDP_Book;

pthread_mutex_t disconnectNasdaqBookMutex;

struct sockaddr_in udpServer;
int serverLen;

int NoNasdaqGapRequest;

extern t_OrderInfo LastNasdaqOrder[MAX_STOCK_SYMBOL];
extern t_MonitorNasdaqBook MonitorNasdaqBook;
extern t_DBLMgmt DBLMgmt;
/****************************************************************************
- Function name:	InitNASDAQ_BOOK_DataStructure
- Input:			
- Output:											
- Return:			
- Description:						 							  				
- Usage:			
****************************************************************************/
void InitNASDAQ_BOOK_DataStructure(void)
{
	// Initialize semaphore
	TraceLog(DEBUG_LEVEL, "NASDAQ Book: Initialize Book\n");
	
	if (sem_init(&NDAQ_MoldUDP_Book.dataInfo.sem, 0, 0) != 0)
	{
		TraceLog(ERROR_LEVEL, "NASDAQ Book: Couldn't initialize semaphore\n");
	}
	
	bookStatusMgmt[BOOK_NASDAQ].isFlushBook = NO;
	
	memset(NDAQ_MoldUDP_Book.dataInfo.buffer, READ, MAX_BOOK_MSG_IN_BUFFER * MAX_MTU);
	strcpy(NDAQ_MoldUDP_Book.dataInfo.bookName, "NASDAQ Book");
	NDAQ_MoldUDP_Book.dataInfo.DisconnectBook = &DisconnectNASDAQBook;
	
	NDAQ_MoldUDP_Book.Data.channel = NULL;
	NDAQ_MoldUDP_Book.Data.dev = NULL;
	NDAQ_MoldUDP_Book.Data.sendHandle = NULL;

	// Intialize Recovery
	NDAQ_MoldUDP_Book.Recovery.channel = NULL;
	NDAQ_MoldUDP_Book.Recovery.dev = NULL;
	NDAQ_MoldUDP_Book.Recovery.sendHandle = NULL;

	// Intialize counter variables
	MonitorNasdaqBook.recvCounter = 0;
	MonitorNasdaqBook.procCounter = 0;
	NDAQ_MoldUDP_Book.lastSequenceNumber = -1;
	NDAQ_MoldUDP_Book.dataInfo.currentReceivedIndex = 0;
	NDAQ_MoldUDP_Book.dataInfo.currentReadIndex = 0;
	NDAQ_MoldUDP_Book.dataInfo.connectionFlag = 0;
	
	// Flush book
	FlushECNStockBook(BOOK_NASDAQ);
	
	// Initialize variables for disconnect NASDAQ book
	pthread_mutex_init(&disconnectNasdaqBookMutex, NULL);

	// Initialize NASDAQ partial hashing
	int divisorIndex;
	int remainderIndex;
	
	for (divisorIndex = 0; divisorIndex < NASDAQ_MAX_DIVISOR; divisorIndex++)
	{
		for (remainderIndex = 0; remainderIndex < NASDAQ_MAX_SAME_REMAINDER; remainderIndex++)
		{
			NASDAQ_PartialHashing[divisorIndex][remainderIndex].quotient = -1;
			NASDAQ_PartialHashing[divisorIndex][remainderIndex].side = 0;
			NASDAQ_PartialHashing[divisorIndex][remainderIndex].stockSymbolIndex = -1;
			NASDAQ_PartialHashing[divisorIndex][remainderIndex].node = NULL;
			
			NASDAQ_HashingBackup[divisorIndex][remainderIndex].quotient = -1;
			NASDAQ_HashingBackup[divisorIndex][remainderIndex].side = 0;
			NASDAQ_HashingBackup[divisorIndex][remainderIndex].stockSymbolIndex = -1;
		}
	}
}

/****************************************************************************
- Function name:	InitRecoveryDBLConnection
- Input:
- Output:
- Return:
- Description:		Ininitalize connection to Unicast Server for Nasdaq Book recovery
- Usage:
****************************************************************************/
int InitRecoveryDBLConnection(void)
{
	/*Note: Remember to use RECEIVE TIMEOUT = 1s at the time of using the dev*/
	int ret;
	struct sockaddr_in sin_remote;

	/* build local socket address */
	ret = dbl_open_if(NDAQ_MoldUDP_Book.Recovery.NICName, DBL_OPEN_THREADSAFE, &NDAQ_MoldUDP_Book.Recovery.dev);
	if (ret != 0)
	{
		TraceLog(ERROR_LEVEL, "Nasdaq Recovery: dbl_open_if() failed (%d)\n", ret);
		NDAQ_MoldUDP_Book.Recovery.dev = NULL;
		return ERROR;
	}

	int flags = 0;		//DBL_BIND_REUSEADDR, DBL_BIND_DUP_TO_KERNEL, DBL_BIND_NO_UNICAST, DBL_BIND_BROADCAST
	ret = dbl_bind(NDAQ_MoldUDP_Book.Recovery.dev, flags, NDAQ_MoldUDP_Book.Recovery.port, NULL, &NDAQ_MoldUDP_Book.Recovery.channel);
	if (ret != 0)
	{
		TraceLog(ERROR_LEVEL, "Nasdaq Recovery: dbl_bind() failed (%d)\n", ret);
		dbl_close (NDAQ_MoldUDP_Book.Recovery.dev);
		NDAQ_MoldUDP_Book.Recovery.dev = NULL;
		return ERROR;
	}

	memset(&sin_remote, 0, sizeof(sin_remote));
	sin_remote.sin_family = AF_INET;
	sin_remote.sin_port = htons(NDAQ_MoldUDP_Book.Recovery.port);
	inet_aton(NDAQ_MoldUDP_Book.Recovery.ip, &sin_remote.sin_addr);

	ret = dbl_send_connect(NDAQ_MoldUDP_Book.Recovery.channel, &sin_remote, 0, 0, &NDAQ_MoldUDP_Book.Recovery.sendHandle);
	if (ret != 0)
	{
		TraceLog(ERROR_LEVEL, "Nasdaq Recovery: dbl_send_connect() failed (%d)\n", ret);
		ret = LeaveDBLMulticastGroup(NDAQ_MoldUDP_Book.Recovery.channel, NDAQ_MoldUDP_Book.Recovery.ip);
		if (ret != EADDRNOTAVAIL)
			dbl_unbind(NDAQ_MoldUDP_Book.Recovery.channel);
		NDAQ_MoldUDP_Book.Recovery.channel = NULL;
		dbl_close (NDAQ_MoldUDP_Book.Recovery.dev);
		NDAQ_MoldUDP_Book.Recovery.dev = NULL;
		return ERROR;
	}

	return SUCCESS;
}

/****************************************************************************
- Function name:	NDAQ_Book_Thread
- Input:			
- Output:											
- Return:			
- Description:						 							  				
- Usage:			
****************************************************************************/
void *NDAQ_Book_Thread(void *args)
{
	SetKernelAlgorithm("[NASDAQ_BOOK_BASE]");
	InitNASDAQ_BOOK_DataStructure();
	
	pthread_t threadNDAQProcessDataId;	
	
	// For local testing
/*
	bookStatusMgmt[BOOK_NASDAQ].shouldConnect = YES;
	bookStatusMgmt[BOOK_NASDAQ].isConnected = CONNECTED;

	return NULL;
*/
	int ret;
	while (bookStatusMgmt[BOOK_NASDAQ].shouldConnect == YES)
	{
		// Initialize Re-Request socket
		TraceLog(DEBUG_LEVEL, "NASDAQ Book: Initializing Re-Request connection...\n");

		if (NoNasdaqGapRequest == 0)
		{
			if (InitRecoveryDBLConnection() == ERROR)
			{
				TraceLog(ERROR_LEVEL, "NASDAQ Book: Couldn't initialze Re-Request socket\n");
				break;
			}
		}
		else
		{
			TraceLog(DEBUG_LEVEL, "NASDAQ Book: Starting with no recovery mode...\n");
		}

		ret = CreateDBLDeviceV2(NDAQ_MoldUDP_Book.Data.NICName, &NDAQ_MoldUDP_Book.Data.dev);
		if (ret == ERROR)
		{
			TraceLog(ERROR_LEVEL, "NASDAQ Book: Cannot create dbl device for receiving data from Data multicast\n");
			break;
		}

		if (pthread_create(&threadNDAQProcessDataId, NULL, (void*) &ThreadProcessNDAQData, (void*) &NDAQ_MoldUDP_Book) != SUCCESS)
		{
			TraceLog(ERROR_LEVEL, "NASDAQ Book: Couldn't create thread for processing data\n");
			break;
		}
		
		TraceLog(DEBUG_LEVEL, "NASDAQ Book: MulticastRecvPort(%s:%d): connecting\n", NDAQ_MoldUDP_Book.Data.ip, NDAQ_MoldUDP_Book.Data.port);

		ret = JoinMulticastGroupUsingExistingDeviceV2(NDAQ_MoldUDP_Book.Data.dev,
													NDAQ_MoldUDP_Book.Data.ip, NDAQ_MoldUDP_Book.Data.port,
													&NDAQ_MoldUDP_Book.Data.channel, NDAQ_MoldUDP_Book.Data.NICName, (void*) &NDAQ_MoldUDP_Book.dataInfo);
		if (ret == ERROR)
		{
			TraceLog(ERROR_LEVEL, "NASDAQ Book: Can't connect to Multicast group (%s:%d)\n", NDAQ_MoldUDP_Book.Data.ip, NDAQ_MoldUDP_Book.Data.port);
			break;
		}
		
		bookStatusMgmt[BOOK_NASDAQ].isConnected = CONNECTED;
		NDAQ_MoldUDP_Book.dataInfo.connectionFlag = 1;
				
		/* Wait thread completed */
		pthread_join(threadNDAQProcessDataId, NULL);

		TraceLog(DEBUG_LEVEL, "NASDAQ Book: Threads for processing data from Multicast group returned\n");

		// Release NASDAQ Book Resource
		DisconnectNASDAQBook();
		ReleaseNASDAQBookResource();

		int isContinue = bookStatusMgmt[BOOK_NASDAQ].isFlushBook;
		
		if (isContinue == YES)
		{
			bookStatusMgmt[BOOK_NASDAQ].shouldConnect = YES;
		}
		
		// Initialize NASDAQ Book data structure
		TraceLog(DEBUG_LEVEL, "NASDAQ Book: Begin to flush book\n");
		
		InitNASDAQ_BOOK_DataStructure();
		FlushECNStockBook(BOOK_NASDAQ);
		
		TraceLog(DEBUG_LEVEL, "NASDAQ Book: Initialize NASDAQ Book Completed\n");
	
		flushBookRequestCollection[BOOK_NASDAQ].isFlushBook = NO;
		
		// If we received flush book request, try to join to restart all threads again
		if (isContinue == YES)
		{
			TraceLog(DEBUG_LEVEL, "NASDAQ Book: Trying to restart NASDAQ Book\n");
		}
	}
	
	// Update flag is used to indicate NASDAQ Book was "disconnected"
	DisconnectNASDAQBook();
	ReleaseNASDAQBookResource();
	bookStatusMgmt[BOOK_NASDAQ].isConnected = DISCONNECTED;
	
	SendPMDisconnectedBookOrderAlertToAS(SERVICE_NASDAQ_BOOK);
	CheckAndSendAlertDisableTrading(1);
	
	TraceLog(DEBUG_LEVEL, "NASDAQ Multicast ITCH was disconnected\n");
	
	return NULL;
}

/****************************************************************************
- Function name:	ThreadProcessData
- Input:			
- Output:											
- Return:			
- Description:						 							  				
- Usage:			
****************************************************************************/
void *ThreadProcessNDAQData(void *args)
{
	SetKernelAlgorithm("[NASDAQ_BOOK_PROCESS_MSGS]");
	
	t_NDAQ_MoldUDP_Book *workingNDAQ_MoldUDP_Book = (t_NDAQ_MoldUDP_Book *)args;
	
	// Index of reading buffer
	int currentIndex;

	// Message Sequence Number
	unsigned long seqNumberFirstMessage, seqNumberLastMessage;

	// Number of messages in packet
	int messageCount;

	// Session
	char session[12];
	session[10] = 0;
	unsigned char *buffer;
	
	while (bookStatusMgmt[BOOK_NASDAQ].shouldConnect == YES)
	{
		// Wait for new packet from server
		if (sem_wait(&workingNDAQ_MoldUDP_Book->dataInfo.sem) != SUCCESS)
		{
			TraceLog(ERROR_LEVEL, "NASDAQ Book: call to sem_wait() system call failed!\n");
			
			DisconnectNASDAQBook();
			return NULL;
		}
		
		if (bookStatusMgmt[BOOK_NASDAQ].shouldConnect == NO)
		{
			DisconnectNASDAQBook();
			return NULL;
		}
		
		// Get index that indicates a new packet in buffer
		currentIndex = workingNDAQ_MoldUDP_Book->dataInfo.currentReadIndex;
		buffer = (unsigned char*)workingNDAQ_MoldUDP_Book->dataInfo.buffer[currentIndex];
		
		// Check flag to sure no problem before parse packet
		if (buffer[MAX_MTU - 1] == READ)
		{	
			TraceLog(ERROR_LEVEL, "NASDAQ Book: The packet was processed before\n");
			DisconnectNASDAQBook();
			return NULL;
		}
		
		// Check to flush book
		FlushECNSymbolStockBook(BOOK_NASDAQ);
		
		/*
		Get sequence number of first message in current packet
		*/		
		seqNumberFirstMessage = (unsigned long)GetLongNumberBigEndian(&buffer[10]);
		
		// Get message count from current packet
		messageCount = (unsigned short)GetShortNumberBigEndian(&buffer[18]);
		
		if (messageCount == 0)
		{
			
		}
		else
		{
			// Get sequence number of last message in current packet
			seqNumberLastMessage = messageCount + seqNumberFirstMessage - 1;
			
			/*
			If this is first packet received, 
			we should update last sequence number which check 'Multicast problems'
			*/
			if (workingNDAQ_MoldUDP_Book->lastSequenceNumber == -1)
			{				
				workingNDAQ_MoldUDP_Book->lastSequenceNumber = seqNumberLastMessage;
					
				// Begin to Process packet
				if (ProcessPacketWithoutMissing(buffer, currentIndex) == ERROR)
				{
					TraceLog(ERROR_LEVEL, "NASDAQ Book: Have some problem in process NASDAQ packets\n");
					
					DisconnectNASDAQBook();
					
					return NULL;
				}
			}
			else
			{
				/*
				This is NOT first packet received in this session
				*/
				if (seqNumberFirstMessage > workingNDAQ_MoldUDP_Book->lastSequenceNumber + 1)
				{
					memcpy(session, &buffer[0], 10);
					
					int countGappedMsg = seqNumberFirstMessage - workingNDAQ_MoldUDP_Book->lastSequenceNumber - 1;
					TraceLog(WARN_LEVEL, "NASDAQ Book: Gap detected, Last received sequence(%d), number of packets missed(%d), session(%s)\n", workingNDAQ_MoldUDP_Book->lastSequenceNumber, countGappedMsg, session);
					
					if (NoNasdaqGapRequest == 1)
					{
						TraceLog(WARN_LEVEL, "NASDAQ Book: PM will flush book, since it is running with no recovery mode\n");
						bookStatusMgmt[BOOK_NASDAQ].isFlushBook = YES;
						DisconnectNASDAQBook();
						return NULL;
					}

					if (countGappedMsg / MAX_MSG_IN_PACKET >= MAX_ALLOWED_RECOVER_GAPPED_PACKET)
					{
						TraceLog(ERROR_LEVEL, "NASDAQ Book: Lost too many packets %d (max = %d). We should flush book and restart NASDAQ Book\n", countGappedMsg, MAX_MSG_IN_PACKET * MAX_ALLOWED_RECOVER_GAPPED_PACKET);
						bookStatusMgmt[BOOK_NASDAQ].isFlushBook = YES;
						DisconnectNASDAQBook();
						return NULL;
					}
					else
					{
						TraceLog(DEBUG_LEVEL, "NASDAQ Book: Session (%s), Start recovery...\n", session);
						if (ProcessMissedPacket(workingNDAQ_MoldUDP_Book->lastSequenceNumber + 1, countGappedMsg, session) == ERROR)
						{
							TraceLog(ERROR_LEVEL, "NASDAQ Book: Recovery failed\n");
							bookStatusMgmt[BOOK_NASDAQ].isFlushBook = YES;
							DisconnectNASDAQBook();
							return NULL;
						}
						else
						{
							TraceLog(DEBUG_LEVEL, "NASDAQ Book: Recovery successfully\n");
							
							// Begin to Process packet
							if (ProcessPacketWithoutMissing(buffer, currentIndex) == ERROR)
							{
								TraceLog(ERROR_LEVEL, "NASDAQ Recovery: Have problem in process NASDAQ packet\n");
								DisconnectNASDAQBook();
								return NULL;
							}
						}
					}
					
					TraceLog(DEBUG_LEVEL, "NASDAQ Book: End the recovery process, session (%s)\n", session);
					
					workingNDAQ_MoldUDP_Book->lastSequenceNumber = seqNumberLastMessage;
				}
				else
				{
					workingNDAQ_MoldUDP_Book->lastSequenceNumber = seqNumberLastMessage;
					
					// Begin to Process packet
					if (ProcessPacketWithoutMissing(buffer, currentIndex) == ERROR)
					{
						TraceLog(ERROR_LEVEL, "NASDAQ Book: Have problem in process NASDAQ packet\n");
						DisconnectNASDAQBook();
						return NULL;
					}
				}
			}
		}

		// Update status of buffer is READ
		buffer[MAX_MTU - 1] = READ;
	
		/*
		If we have Current_Read_Index greater than LIMITATION of INTEGER data type
		that is very dangerous
		*/
		if (workingNDAQ_MoldUDP_Book->dataInfo.currentReadIndex == MAX_BOOK_MSG_IN_BUFFER - 1)
		{
			workingNDAQ_MoldUDP_Book->dataInfo.currentReadIndex = 0;
		}
		else
		{
			workingNDAQ_MoldUDP_Book->dataInfo.currentReadIndex++;
		}
	}
	
	TraceLog(DEBUG_LEVEL, "NASDAQ Book: We received request to stop NASDAQ Book processing\n");
	
	DisconnectNASDAQBook();
	
	return NULL;
}

/****************************************************************************
- Function name:	ProcessPacketWihoutMissing
- Input:			
- Output:											
- Return:			
- Description:						 							  				
- Usage:			
****************************************************************************/
int ProcessPacketWithoutMissing(unsigned char *buffer, int currentReadIndex)
{
	// Index that start each message
	int startIndex = 20;

	// Message Count
	int messageCount;

	// Length of message
	int messageLength;

	// Message Type
	char messageType;

	// Get message count
	messageCount = (unsigned short)GetShortNumberBigEndian(&buffer[startIndex - 2]);

	while(messageCount > 0)
	{
		// Get length for first message in packet
		messageLength = (unsigned short)GetShortNumberBigEndian(&buffer[startIndex]);
		
		/*
		2 bytes for length of message
		8 bytes for timestamp length
		*/
		startIndex += 2;

		// Get message type
		messageType = buffer[startIndex];

		switch(messageType)
		{
			// Add order message
			case NDAQ_ADD_MSG:
				if(ProcessAddOrderMessage(&buffer[startIndex]) == ERROR)
				{
					return ERROR;
				}
				
				startIndex += messageLength;
				messageCount--;
				
				break;
			
			// Add order with MPID attribution message 
			case NDAQ_ADD_MPID_MSG:
				if(ProcessAddOrderMessage(&buffer[startIndex]) == ERROR)
				{
					return ERROR;
				}
				
				startIndex += messageLength;
				messageCount--;
				
				break;

			// Order executed message
			case NDAQ_EXECUTED_MSG:
				if(ProcessExecutedOrderMessage(&buffer[startIndex]) == ERROR)
				{
					return ERROR;
				}
				
				startIndex += messageLength;
				messageCount--;
				
				break;

			// Order executed with price message
			case NDAQ_EXECUTED_PRICE_MSG:
				if(ProcessExecutedOrderMessage(&buffer[startIndex]) == ERROR)
				{
					return ERROR;
				}
				
				startIndex += messageLength;
				messageCount--;
				
				break;
			// Order cancel message
			case NDAQ_CANCEL_MSG:
				if(ProcessCancelOrderMessage(&buffer[startIndex]) == ERROR)
				{
					return ERROR;
				}
				
				startIndex += messageLength;
				messageCount--;
				
				break;
			
			// Order delete message
			case NDAQ_DELETE_MSG:
				if(ProcessDeleteOrderMessage(&buffer[startIndex]) == ERROR)
				{
					return ERROR;
				}
				
				startIndex += messageLength;
				messageCount--;
				
				break;
			
			// Order replace message
			case NDAQ_REPLACE_MSG:
				if(ProcessReplaceOrderMessage(&buffer[startIndex]) == ERROR)
				{
					return ERROR;
				}
				
				startIndex += messageLength;
				messageCount--;
				
				break;
			
			/*
			// System event message (S, 5 bytes)
			// Timestamp - Second message (T, 5 bytes)
			// Stock directory message (R, 18 bytes)
			// Stock trading action (H, 17 bytes)
			// Market participant position message (L, 18 bytes)
			// Trade message (Non-cross) (P, 36 bytes)
			// Cross trade message (Q, 32 bytes)
			// Broken trade / Order exection message (B, 13 bytes)
			// Net order imbalance indicator message (T, 42 bytes)
			*/			
			default:			
				startIndex += messageLength;
				messageCount--;
				
				break;
		}
	}
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessMissingPacket
- Input:			
- Output:											
- Return:			
- Description:		This function is used to process missed packet.
					Amount of messages in packet is less than maximun limit 
					per packet
- Usage:			
****************************************************************************/
int ProcessMissedPacket(long firstMsgSeq, int msgCount, char *session)
{
	t_NDAQ_MoldUDP_Book *workingNDAQ_MoldUDP_Book = &(NDAQ_MoldUDP_Book);
	
	/* 
	Build packet and send it to server in order to recover missing packet
	Connection was establish via UPD socket
	*/
	
	//TraceStdError("NASDAQ: Build and send Gap Request message\n");
	if (BuildAndSendReRequestPacket(firstMsgSeq, msgCount, session) == ERROR)
	{
		return ERROR;
	}
	
	unsigned char bufferToReceive[MAX_MTU * MAX_ALLOWED_RECOVER_GAPPED_PACKET];
	
	// Number of missed message
	int numOfMissedMessage = msgCount;
	
	long tempFirstMsgSeq = firstMsgSeq;
	int numOfReceivedByte;
	int receivedIndex = 0;
	int processedIndex = 0;

	struct dbl_recv_info rInfo;
	int ret;

	//set receive timeout!
	struct pollfd pfd;
	pfd.events = POLLIN;

	pfd.fd = dbl_device_handle(workingNDAQ_MoldUDP_Book->Recovery.dev);

	// Wait for receiving response from server
	while (numOfMissedMessage > 0)
	{
		if (bookStatusMgmt[BOOK_NASDAQ].shouldConnect == NO)
		{
			return ERROR;
		}

		ret = poll(&pfd, 1, 1500);	//timeout = 1.5s
		if (ret < 0)
		{
			PrintErrStr(ERROR_LEVEL, "Nasdaq Recovery, poll(): ", errno);
			return ERROR;
		}
		else if (ret == 0)	//Timed out
		{
			TraceLog(ERROR_LEVEL, "Nasdaq Recovery: We have some problems in receiving response (timeout)\n");
			return ERROR;
		}

		ret = dbl_recvfrom(workingNDAQ_MoldUDP_Book->Recovery.dev, DBL_RECV_NONBLOCK, &bufferToReceive[receivedIndex], MAX_MTU, &rInfo);

		if (ret != 0)
		{
			TraceLog(ERROR_LEVEL, "Nasdaq Recovery: We have some problems in receiving response\n");
			return ERROR;
		}

		numOfReceivedByte = rInfo.msg_len;

		if (numOfReceivedByte < 1)
		{
			TraceLog(ERROR_LEVEL, "NASDAQ Recovery: We have some problems in receiving response\n");
			return ERROR;
		}
		
		receivedIndex += numOfReceivedByte;
		
		/*
		FILE *fileDesc = fopen("re_request_packet.txt", "w+");
			fwrite(bufferToReceive, 1, numOfReceivedByte, fileDesc);
			fclose(fileDesc);
		*/
		
		//TraceStdError("NASDAQ: Received Gap (%d)\n", numOfReceivedByte);
		
		/*
		We should process data after we had full data
		Try to receive the first packet
		We need check whether data is full
		*/
		unsigned long firstMessageSeq = (unsigned long)GetLongNumberBigEndian(&bufferToReceive[processedIndex + 10]);
		
		if (firstMessageSeq == tempFirstMsgSeq)
		{
			int messageCount = (unsigned short)GetShortNumberBigEndian(&bufferToReceive[processedIndex + 18]);
			//TraceStdError("NASDAQ: messageCount = %d\n", messageCount);
			
			int packetLen = ProcessRecoveryPacket(&bufferToReceive[processedIndex]);
			//TraceStdError("NASDAQ: Processed Gap message, packetLen = %d\n", packetLen);
			
			// Received data fully
			numOfMissedMessage -= messageCount;
			
			if (numOfMissedMessage == 0)
			{
				if (packetLen < 0)
				{
					// We should flush ECN book
					FlushECNStockBook(BOOK_NASDAQ);
					return ERROR;
				}
				else
				{
					return SUCCESS;
				}
			}
			else
			{
				processedIndex += packetLen;
				tempFirstMsgSeq = firstMessageSeq + messageCount;
				
				if (BuildAndSendReRequestPacket(tempFirstMsgSeq, numOfMissedMessage, session) == ERROR)
				{
					return ERROR;
				}
			}
		}
		else
		{
			return ERROR;
		}
	}

	return SUCCESS;
}

/****************************************************************************
- Function name:	BuildAndSendReRequestPacket
- Input:			
- Output:											
- Return:			
- Description:						 							  				
- Usage:			
****************************************************************************/
int BuildAndSendReRequestPacket(long firstMsgSeq, int msgCount, char *session)
{
	// Clear buffer need to store packet to send re-request server
	char bufferToSend[MAX_MTU];
	memset(bufferToSend, 0, MAX_MTU);

	/*
	Build UDP packet
	*/
	// Session
	strncpy(bufferToSend, session, 10);

	// Sequence Number
	char seqBuff[8];
	memcpy(seqBuff, &firstMsgSeq, 8);
	//memcpy(&bufferToSend[10], &firstMsgSeq, 8);
	bufferToSend[10] = seqBuff[7];
	bufferToSend[11] = seqBuff[6];
	bufferToSend[12] = seqBuff[5];
	bufferToSend[13] = seqBuff[4];
	bufferToSend[14] = seqBuff[3];
	bufferToSend[15] = seqBuff[2];
	bufferToSend[16] = seqBuff[1];
	bufferToSend[17] = seqBuff[0];
	
	// Number of missed messages
	short messageCount = msgCount;
	char msgCountBuff[2];
	memcpy(msgCountBuff, &messageCount, 2);
	//memcpy(&bufferToSend[18], &messageCount, 2);
	bufferToSend[18] = msgCountBuff[1];
	bufferToSend[19] = msgCountBuff[0];
	
	// Send packet to server
	int ret = dbl_send(NDAQ_MoldUDP_Book.Recovery.sendHandle, bufferToSend, 20, 0);

	if (ret != 0)
	{
		TraceLog(ERROR_LEVEL, "NASDAQ Recovery: We have problem in sending packet to re-request server\n");
		return ERROR;
	}
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessRecoveryPacket
- Input:			
- Output:											
- Return:			
- Description:						 							  				
- Usage:			
****************************************************************************/
int ProcessRecoveryPacket(unsigned char *buffer)
{
	//Index that start each message
	int startIndex = 20;

	//Buffer need to store message count
	unsigned char messageCountBuff[3];

	//Message Count
	int messageCount;

	//Length of message
	unsigned char messageLengthBuff[3];
	int messageLength;

	//Message Type
	char messageType;

	//Get message count
	messageCountBuff[2] = 0;

	memcpy(messageCountBuff, &buffer[startIndex - 2], 2);
	messageCount = (unsigned short)GetShortNumberBigEndian(messageCountBuff);

	while(messageCount > 0)
	{
		//Get length for first message in packet
		messageLengthBuff[2] = 0;

		memcpy(messageLengthBuff, &buffer[startIndex], 2);
		messageLength = (unsigned short)GetShortNumberBigEndian(messageLengthBuff);
		
		//2 bytes for length of message
		//8 bytes for timestamp length
		startIndex += 2;

		//Get message type
		messageType = buffer[startIndex];

		switch(messageType)
		{
			//Add order message
			case NDAQ_ADD_MSG:
				if(ProcessAddOrderMessage(&buffer[startIndex]) == -1)
				{
					return -1;
				}
				startIndex += messageLength;
				messageCount--;
				
				break;
			
			// Add order with MPID attribution message 
			case NDAQ_ADD_MPID_MSG:
				if(ProcessAddOrderMessage(&buffer[startIndex]) == -1)
				{
					return -1;
				}
				
				startIndex += messageLength;
				messageCount--;
				
				break;

			//Order executed message
			case NDAQ_EXECUTED_MSG:
				if(ProcessExecutedOrderMessage(&buffer[startIndex]) == -1)
				{
					return -1;
				}
				startIndex += messageLength;
				messageCount--;
				
				break;

			// Order executed with price message
			case NDAQ_EXECUTED_PRICE_MSG:
				if(ProcessExecutedOrderMessage(&buffer[startIndex]) == -1)
				{
					return -1;
				}
				
				startIndex += messageLength;
				messageCount--;
				
				break;

			//Order cancel message
			case NDAQ_CANCEL_MSG:
				if(ProcessCancelOrderMessage(&buffer[startIndex]) == -1)
				{
					return -1;
				}
				startIndex += messageLength;
				messageCount--;
				break;
			
			// Order delete message
			case NDAQ_DELETE_MSG:
				if(ProcessDeleteOrderMessage(&buffer[startIndex]) == ERROR)
				{
					return ERROR;
				}
				
				startIndex += messageLength;
				messageCount--;
				
				break;
			
			// Order replace message
			case NDAQ_REPLACE_MSG:
				if(ProcessReplaceOrderMessage(&buffer[startIndex]) == ERROR)
				{
					return ERROR;
				}
				
				startIndex += messageLength;
				messageCount--;
				
				break;
			
			/*
			// System event message (S, 5 bytes)
			// Timestamp - Second message (T, 5 bytes)
			// Stock directory message (R, 18 bytes)
			// Stock trading action (H, 17 bytes)
			// Market participant position message (L, 18 bytes)
			// Trade message (Non-cross) (P, 36 bytes)
			// Cross trade message (Q, 32 bytes)
			// Broken trade / Order exection message (B, 13 bytes)
			// Net order imbalance indicator message (T, 42 bytes)
			*/			
			default:			
				startIndex += messageLength;
				messageCount--;
				break;
		}
	}

	return startIndex;
}

/****************************************************************************
- Function name:	ProcessAddOrderMessage
- Input:			
- Output:											
- Return:			
- Description:						 							  				
- Usage:			
****************************************************************************/
int ProcessAddOrderMessage(unsigned char *buffer)
{
	t_OrderDetail NDAQ_Order;
	t_OrderDetailNode *retNode;
	
	// Buy Sell indicator
	char buySellIndicator;
    
	// Get index of Stock Symbol
	int stockSymbolIndex = GetStockSymbolIndex((char *)&buffer[24]);
	
	if(stockSymbolIndex == -1)
	{
		// Try to insert stock symbol to data structure
		if ( (stockSymbolIndex = UpdateStockSymbolIndex((char*)&buffer[24])) == ERROR)
		{
			/*
			Cannot insert stock symbol into data structure 
			because stock symbol format is not supported.
			Ignore the 'add order' but we must continue process anothers
			*/
			return SUCCESS;
		}
	}
	
	// Get order reference number
	NDAQ_Order.refNum = (unsigned long)GetLongNumberBigEndian(&buffer[11]) % NASDAQ_MAX_REF_NUM_DIVISOR;
    
    // Get buy sell indicator
    buySellIndicator = buffer[19];
	
    // Get shares
	NDAQ_Order.shareVolume = (unsigned int)GetIntNumberBigEndian(&buffer[20]);
	
    // Get Price
	NDAQ_Order.sharePrice = (double)((unsigned int)GetIntNumberBigEndian(&buffer[32]))/10000;
	
	/*
	Detect cross trade before add new order to skip list
	*/
	int side;
	
	if (buySellIndicator == 'S')
	{
		side = ASK_SIDE;
	}
	else
	{
		side = BID_SIDE;
	}
	
	t_TopOrder _bestQuote;
	long refNum = CheckSelfCrossTrade(&NDAQ_Order, side, stockSymbolIndex, BOOK_NASDAQ, &_bestQuote);
	
	int isCanceled = ERROR;

	if (refNum > 0)
	{
		int tempSide;
		tempSide = !side;
		
		t_PartialHashingItem *NASDAQ_Hashing;
		t_OrderDetailNode *oldNode;
		
		NASDAQ_Hashing = FindPartialHashingItem(BOOK_NASDAQ, refNum);
		
		if (NASDAQ_Hashing != NULL)
		{
			oldNode = NASDAQ_Hashing->node;
			
			pthread_mutex_lock(&deleteQuotesMgmtMutex[BOOK_NASDAQ][stockSymbolIndex][tempSide]);

			// Delete old node
			DeletePartialHashingItem(BOOK_NASDAQ, refNum);		
			
			if (oldNode != NULL)
			{
				if (GeneralDelete(stockBook[BOOK_NASDAQ][tempSide][stockSymbolIndex], oldNode, tempSide, stockSymbolIndex) == ERROR)
				{
					TraceLog(ERROR_LEVEL, "NASDAQ Book processing: Couldn't delete node from SkipList\n");
				}
			}
			
			pthread_mutex_unlock(&deleteQuotesMgmtMutex[BOOK_NASDAQ][stockSymbolIndex][tempSide]);
		}
		else
		{
			if (side == ASK_SIDE)
			{
				// New Order Side is ASK -> Other side is BID
				if (NDAQ_Order.shareVolume > MAX_ODD_LOT_SHARE)
				{
					isCanceled = UpdateMaxBidRoundLot(stockSymbolIndex, 1, YES, -1);
				}					
				
				if (isCanceled == SUCCESS)
					UpdateMaxBid(stockSymbolIndex, 0, -1);
				else
					UpdateMaxBid(stockSymbolIndex, 1, -1);
			}
			else
			{
				// Otherwise
				if (NDAQ_Order.shareVolume > MAX_ODD_LOT_SHARE)
				{
					isCanceled = UpdateMinAskRoundLot(stockSymbolIndex, 1, YES, -1);
				}

				if(isCanceled == SUCCESS)
					UpdateMinAsk(stockSymbolIndex, 0, -1);
				else
					UpdateMinAsk(stockSymbolIndex, 1, -1);
			}
			
			TraceLog(DEBUG_LEVEL, "NASDAQ Book processing: Couldn't find node from SkipList (refNum %ld). Order(%.8s, %d, %lf, %ld). BestQuote(%d: %d, %lf)\n",
							refNum, &buffer[24], NDAQ_Order.shareVolume, NDAQ_Order.sharePrice, NDAQ_Order.refNum,
							_bestQuote.ecnIndicator, _bestQuote.info.shareVolume, _bestQuote.info.sharePrice);
		}
		
		DeleteBackupPartialHashingItem(BOOK_NASDAQ, refNum);
	}
	
	// Check if own quote is adding to book
	if ( (side == LastNasdaqOrder[stockSymbolIndex].side) &&
		 flt(fabs(NDAQ_Order.sharePrice - LastNasdaqOrder[stockSymbolIndex].price), 0.0001) &&
		 (NDAQ_Order.shareVolume == LastNasdaqOrder[stockSymbolIndex].shares) )
	{
			TraceLog(DEBUG_LEVEL, "Nasdaq Book: ignore own quote, ref = %ld, orderID = %d\n", NDAQ_Order.refNum, LastNasdaqOrder[stockSymbolIndex].orderID);
			LastNasdaqOrder[stockSymbolIndex].side = -1;	//Remove from list!
			return SUCCESS;
	}
	
	int retval = SUCCESS;
		
	// Check Buy Sell indicator to insert it to Stock Book
	pthread_mutex_lock(&deleteQuotesMgmtMutex[BOOK_NASDAQ][stockSymbolIndex][side]);

	retNode = GeneralInsert(stockBook[BOOK_NASDAQ][side][stockSymbolIndex], &NDAQ_Order, side, stockSymbolIndex);
		
	if (retNode == NULL)
	{
		TraceLog(ERROR_LEVEL, "NASDAQ Book processing: Couldn't insert this order into Stock Book\n");
		retval = ERROR;
	}
	
	if (InsertPartialHashingItem(retNode, BOOK_NASDAQ, NDAQ_Order.refNum, stockSymbolIndex, buySellIndicator) == ERROR)
	{
		TraceLog(ERROR_LEVEL, "NASDAQ Book processing: Couldn't insert order into PartialHash\n");
		retval = ERROR;
	}
	
	pthread_mutex_unlock(&deleteQuotesMgmtMutex[BOOK_NASDAQ][stockSymbolIndex][side]);

	return retval;
}

/****************************************************************************
- Function name:	ProcessExecutedOrderMessage
- Input:			
- Output:											
- Return:			
- Description:						 							  				
- Usage:			
****************************************************************************/
int ProcessExecutedOrderMessage(unsigned char *buffer)
{
	t_PartialHashingItem *NDAQ_Hashing;
	t_OrderDetailNode *oldNode;
	
	// Side 
	int side = 0;
	int stockSymbolIndex;
	
	// Get Order Reference Number
	unsigned long orderRefNumber = (unsigned long)GetLongNumberBigEndian(&buffer[11]) % NASDAQ_MAX_REF_NUM_DIVISOR;
		
	NDAQ_Hashing = FindPartialHashingItem(BOOK_NASDAQ, orderRefNumber);
	
	if (NDAQ_Hashing != NULL)
	{
		// Get Executed Shares
		unsigned int executedShares = (unsigned int)GetIntNumberBigEndian(&buffer[19]);
			
		stockSymbolIndex = NDAQ_Hashing->stockSymbolIndex;
		
		if (NDAQ_Hashing->side == 'S')
		{
			side = ASK_SIDE;
		}
		else if (NDAQ_Hashing->side == 'B')
		{
			side = BID_SIDE;
		}

		pthread_mutex_lock(&deleteQuotesMgmtMutex[BOOK_NASDAQ][stockSymbolIndex][side]);
		NDAQ_Hashing = FindPartialHashingItem(BOOK_NASDAQ, orderRefNumber);

		if (NDAQ_Hashing != NULL)
		{		
			oldNode = NDAQ_Hashing->node;
			
			if (oldNode == NULL)
			{
				DeletePartialHashingItem(BOOK_NASDAQ, orderRefNumber);
			}
			else
			{
				if (executedShares == oldNode->info.shareVolume)
				{
					DeletePartialHashingItem(BOOK_NASDAQ, orderRefNumber);

					GeneralDelete(stockBook[BOOK_NASDAQ][side][stockSymbolIndex], oldNode, side, stockSymbolIndex);
					DeleteBackupPartialHashingItem(BOOK_NASDAQ, orderRefNumber);
				}
				else
				{
					GeneralModify(oldNode, oldNode->info.shareVolume - executedShares, side, stockSymbolIndex);
				}
			}
		}
		
		pthread_mutex_unlock(&deleteQuotesMgmtMutex[BOOK_NASDAQ][stockSymbolIndex][side]);
	}

	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessCancelOrderMessage
- Input:			
- Output:											
- Return:			
- Description:						 							  				
- Usage:			
****************************************************************************/
int ProcessCancelOrderMessage(unsigned char *buffer)
{
	t_PartialHashingItem *NDAQ_Hashing;
	t_OrderDetailNode *oldNode;
	
	int side = 0;
	int stockSymbolIndex;
	
	// Get Order Reference Number
	unsigned long orderRefNumber = (unsigned long)GetLongNumberBigEndian(&buffer[11]) % NASDAQ_MAX_REF_NUM_DIVISOR;
		
	NDAQ_Hashing = FindPartialHashingItem(BOOK_NASDAQ, orderRefNumber);
	
	if (NDAQ_Hashing != NULL)
	{
		// Get Cancel Shares
		unsigned int canceledShares = (unsigned int)GetIntNumberBigEndian(&buffer[19]);
			
		stockSymbolIndex = NDAQ_Hashing->stockSymbolIndex;
				
		
		if (NDAQ_Hashing->side == 'S')
		{
			side = ASK_SIDE;
		}
		else if (NDAQ_Hashing->side == 'B')
		{
			side = BID_SIDE;
		}
		pthread_mutex_lock(&deleteQuotesMgmtMutex[BOOK_NASDAQ][stockSymbolIndex][side]);
		NDAQ_Hashing = FindPartialHashingItem(BOOK_NASDAQ, orderRefNumber);
		
		if (NDAQ_Hashing != NULL)
		{
			oldNode = NDAQ_Hashing->node;
			
			if (oldNode == NULL)
			{
				DeletePartialHashingItem(BOOK_NASDAQ, orderRefNumber);
			}
			else
			{
				if (canceledShares == oldNode->info.shareVolume)
				{
					DeletePartialHashingItem(BOOK_NASDAQ, orderRefNumber);
					
					GeneralDelete(stockBook[BOOK_NASDAQ][side][stockSymbolIndex], oldNode, side, stockSymbolIndex);
					
					DeleteBackupPartialHashingItem(BOOK_NASDAQ, orderRefNumber);
				}
				else
				{
					GeneralModify(oldNode, oldNode->info.shareVolume - canceledShares, side, stockSymbolIndex);
				}
			}
		}
		
		pthread_mutex_unlock(&deleteQuotesMgmtMutex[BOOK_NASDAQ][stockSymbolIndex][side]);
	}
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessDeleteOrderMessage
- Input:			
- Output:											
- Return:			
- Description:						 							  				
- Usage:			
****************************************************************************/
int ProcessDeleteOrderMessage(unsigned char *buffer)
{
	t_PartialHashingItem *NDAQ_Hashing;
	t_OrderDetailNode *oldNode;
	
	int side = 0;
	
	// Get Order Reference Number
	unsigned long orderRefNumber = (unsigned long)GetLongNumberBigEndian(&buffer[11]) % NASDAQ_MAX_REF_NUM_DIVISOR;
	
	NDAQ_Hashing = FindPartialHashingItem(BOOK_NASDAQ, orderRefNumber);
	
	if (NDAQ_Hashing != NULL)
	{
		int stockSymbolIndex = NDAQ_Hashing->stockSymbolIndex;
		
		if(NDAQ_Hashing->side == 'S')
		{
			side = ASK_SIDE;
		}
		else
		{
			side = BID_SIDE;
		}
				
		pthread_mutex_lock(&deleteQuotesMgmtMutex[BOOK_NASDAQ][stockSymbolIndex][side]);
		NDAQ_Hashing = FindPartialHashingItem(BOOK_NASDAQ, orderRefNumber);

		if (NDAQ_Hashing != NULL)
		{
			oldNode = NDAQ_Hashing->node;
			
			DeletePartialHashingItem(BOOK_NASDAQ, orderRefNumber);
			if (oldNode != NULL)
			{
				GeneralDelete(stockBook[BOOK_NASDAQ][side][stockSymbolIndex], oldNode, side, stockSymbolIndex);
			}
		}

		pthread_mutex_unlock(&deleteQuotesMgmtMutex[BOOK_NASDAQ][stockSymbolIndex][side]);
	}
	
	DeleteBackupPartialHashingItem(BOOK_NASDAQ, orderRefNumber);
	return SUCCESS;
}



/****************************************************************************
- Function name:	ProcessReplaceOrderMessage
- Input:			
- Output:											
- Return:			
- Description:						 							  				
- Usage:			
****************************************************************************/
int ProcessReplaceOrderMessage(unsigned char *buffer)
{
	t_PartialHashingItem *NDAQ_Hashing;
	t_OrderDetailNode *oldNode;
	
	int isCanceled = ERROR;
	int retval = SUCCESS;
	int side = -1;
	int stockSymbolIndex = 0;
	
	// Get Order Reference Number
	unsigned long orderRefNumber = (unsigned long)GetLongNumberBigEndian(&buffer[11]) % NASDAQ_MAX_REF_NUM_DIVISOR;
	
	t_OrderDetail NDAQ_Order;
	
	// Get order reference number
	NDAQ_Order.refNum = (unsigned long)GetLongNumberBigEndian(&buffer[19]);
	
	// Get shares
	NDAQ_Order.shareVolume = (unsigned int)GetIntNumberBigEndian(&buffer[27]);
	
	// Get Price
	NDAQ_Order.sharePrice = (double)((unsigned int)GetIntNumberBigEndian(&buffer[31]))/10000;
	
	/* We will delete old quote */

	NDAQ_Hashing = FindPartialHashingItem(BOOK_NASDAQ, orderRefNumber);
	
	if (NDAQ_Hashing != NULL)
	{
		stockSymbolIndex = NDAQ_Hashing->stockSymbolIndex;
		
		if (NDAQ_Hashing->side == 'S')
		{
			side = ASK_SIDE;
		}
		else
		{
			side = BID_SIDE;
		}
		
		pthread_mutex_lock(&deleteQuotesMgmtMutex[BOOK_NASDAQ][stockSymbolIndex][side]);
		NDAQ_Hashing = FindPartialHashingItem(BOOK_NASDAQ, orderRefNumber);
	
		if (NDAQ_Hashing != NULL)
		{
			oldNode = NDAQ_Hashing->node;
			
			DeletePartialHashingItem(BOOK_NASDAQ, orderRefNumber);
			
			if (oldNode != NULL)
			{
				GeneralDelete(stockBook[BOOK_NASDAQ][side][stockSymbolIndex], oldNode, side, stockSymbolIndex);
			}
		}
		
		pthread_mutex_unlock(&deleteQuotesMgmtMutex[BOOK_NASDAQ][stockSymbolIndex][side]);
	}
	
	/*	
		Finished delete old quote 
		We will now detect cross trade
		Then add new replaced quote to book
		(Before detecting cross trade, we need to get missing information from backup list if needed)
	*/
	
	if (side == -1)
	{
		//Need to get missing information from backup list
		t_NasdaqHashingBackupItem *backupItem = FindBackupPartialHashingItem(BOOK_NASDAQ, orderRefNumber);
		if (backupItem != NULL)
		{
			 side = backupItem->side;
			 stockSymbolIndex = backupItem->stockSymbolIndex;
			 
			 DeleteBackupPartialHashingItem(BOOK_NASDAQ, orderRefNumber);
		}
		else
		{
			//OOps, still could not find missing info from backup list, so we will return
			return SUCCESS;
		}
	}
	
	/* We have all information needed for cross detection and adding new info to book */
	
	t_TopOrder _bestQuote;
	long refNum = CheckSelfCrossTrade(&NDAQ_Order, side, stockSymbolIndex, BOOK_NASDAQ, &_bestQuote);

	if (refNum > 0)
	{
		int tempSide;
		tempSide = !side;
		
		NDAQ_Hashing = FindPartialHashingItem(BOOK_NASDAQ, refNum);
		
		if (NDAQ_Hashing != NULL)
		{
			oldNode = NDAQ_Hashing->node;
		
			
			// Delete old node
			pthread_mutex_lock(&deleteQuotesMgmtMutex[BOOK_NASDAQ][stockSymbolIndex][tempSide]);
		
			DeletePartialHashingItem(BOOK_NASDAQ, refNum);
			
			if (oldNode != NULL)
			{
				if (GeneralDelete(stockBook[BOOK_NASDAQ][tempSide][stockSymbolIndex], oldNode, tempSide, stockSymbolIndex) == ERROR)
				{
					TraceLog(ERROR_LEVEL, "NASDAQ Book processing: Couldn't delete node from SkipList\n");
				}
			}
			
			pthread_mutex_unlock(&deleteQuotesMgmtMutex[BOOK_NASDAQ][stockSymbolIndex][tempSide]);
		}
		else
		{

			if (side == ASK_SIDE)
			{
				// New Order Side is ASK -> Other side is BID
				if (NDAQ_Order.shareVolume > MAX_ODD_LOT_SHARE)
				{
					isCanceled = UpdateMaxBidRoundLot(stockSymbolIndex, 1, YES, -1);
				}					
			
				if (isCanceled == SUCCESS)
					UpdateMaxBid(stockSymbolIndex, 0, -1);
				else
					UpdateMaxBid(stockSymbolIndex, 1, -1);
			}
			else
			{
				// Otherwise
				if (NDAQ_Order.shareVolume > MAX_ODD_LOT_SHARE)
				{
					isCanceled = UpdateMinAskRoundLot(stockSymbolIndex, 1, YES, -1);
				}

				if(isCanceled == SUCCESS)
					UpdateMinAsk(stockSymbolIndex, 0, -1);
				else
					UpdateMinAsk(stockSymbolIndex, 1, -1);
			}
		
			TraceLog(DEBUG_LEVEL, "NASDAQ: Couldn't node from PartialHash(refNum %ld). Order(%d, %lf, %ld). BestQuote(%d: %d, %lf)\n",
							refNum, NDAQ_Order.shareVolume, NDAQ_Order.sharePrice, NDAQ_Order.refNum,
							_bestQuote.ecnIndicator, _bestQuote.info.shareVolume, _bestQuote.info.sharePrice);
		}
		
		DeleteBackupPartialHashingItem(BOOK_NASDAQ, refNum);
	}
				
	/* Add new quote to book */
	pthread_mutex_lock(&deleteQuotesMgmtMutex[BOOK_NASDAQ][stockSymbolIndex][side]);
		
	t_OrderDetailNode *retNode;
	
	retNode = GeneralInsert(stockBook[BOOK_NASDAQ][side][stockSymbolIndex], &NDAQ_Order, side, stockSymbolIndex);
		
	if (retNode == NULL)
	{
		TraceLog(ERROR_LEVEL, "NASDAQ Book processing: Couldn't insert this order into Stock Book\n");
		retval = ERROR;
	}
	
	if (InsertPartialHashingItem(retNode, BOOK_NASDAQ, NDAQ_Order.refNum, stockSymbolIndex, (side == ASK_SIDE)?'S':'B') == ERROR)
	{
		TraceLog(ERROR_LEVEL, "NASDAQ Book processing: Couldn't insert order into PartialHash\n");
		retval = ERROR;
	}
	
	pthread_mutex_unlock(&deleteQuotesMgmtMutex[BOOK_NASDAQ][stockSymbolIndex][side]);
	
	
	return retval;
}


/****************************************************************************
- Function name:	DisconnectNASDAQBook
- Input:			
- Output:											
- Return:			
- Description:						 							  				
- Usage:			
****************************************************************************/
int DisconnectNASDAQBook(void)
{
	pthread_mutex_lock(&disconnectNasdaqBookMutex);
	
	bookStatusMgmt[BOOK_NASDAQ].shouldConnect = NO;
	manFlushBookFlag[BOOK_NASDAQ] = NO;
		
	sem_post(&NDAQ_MoldUDP_Book.dataInfo.sem);
	
	pthread_mutex_unlock(&disconnectNasdaqBookMutex);
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	ReleaseNASDAQBookResource
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ReleaseNASDAQBookResource(void)
{
	int ret;
	pthread_mutex_lock(&DBLMgmt.mutexLock);
	int needToRelease = (IsDBLTimedOut(NDAQ_MoldUDP_Book.Data.dev) == 0) ? 1:0;

	if (NDAQ_MoldUDP_Book.Data.sendHandle != NULL)
	{
		dbl_send_disconnect (NDAQ_MoldUDP_Book.Data.sendHandle);
		NDAQ_MoldUDP_Book.Data.sendHandle = NULL;
	}

	if (NDAQ_MoldUDP_Book.Data.channel != NULL)
	{
		if (needToRelease == 1)
		{
			ret = LeaveDBLMulticastGroup(NDAQ_MoldUDP_Book.Data.channel, NDAQ_MoldUDP_Book.Data.ip);
			if (ret != EADDRNOTAVAIL)
				dbl_unbind(NDAQ_MoldUDP_Book.Data.channel);
		}
		NDAQ_MoldUDP_Book.Data.channel = NULL;
	}

	if (NDAQ_MoldUDP_Book.Data.dev != NULL)
	{
		NDAQ_MoldUDP_Book.Data.dev = NULL;

		sem_destroy(&NDAQ_MoldUDP_Book.dataInfo.sem);
	}

	if (NDAQ_MoldUDP_Book.Recovery.sendHandle != NULL)
	{
		dbl_send_disconnect (NDAQ_MoldUDP_Book.Recovery.sendHandle);
		NDAQ_MoldUDP_Book.Recovery.sendHandle = NULL;
	}

	if (NDAQ_MoldUDP_Book.Recovery.channel != NULL)
	{
		ret = LeaveDBLMulticastGroup(NDAQ_MoldUDP_Book.Recovery.channel, NDAQ_MoldUDP_Book.Recovery.ip);

		if (ret != EADDRNOTAVAIL)
			dbl_unbind(NDAQ_MoldUDP_Book.Recovery.channel);
		
		NDAQ_MoldUDP_Book.Recovery.channel = NULL;
	}

	if (NDAQ_MoldUDP_Book.Recovery.dev != NULL)
	{
		dbl_close(NDAQ_MoldUDP_Book.Recovery.dev);
		NDAQ_MoldUDP_Book.Recovery.dev = NULL;
	}
	
	pthread_mutex_unlock(&DBLMgmt.mutexLock);
	
	usleep(200); // ensure receiving data thread is aware this leaving

	return SUCCESS;
}
