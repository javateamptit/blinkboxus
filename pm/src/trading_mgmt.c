/****************************************************************************
** Project name:	Equity Arbitrage Application
** File name: 		trading_mgmt.c
** Description: 	This file contains function definitions that were declared
					in trading_mgmt.h
** Author: 			Sang Nguyen-Minh
** First created on 08 October 2007
** Last updated on 08 October 2007
****************************************************************************/

/****************************************************************************
** Include files and define several variables
****************************************************************************/
#define _ISOC9X_SOURCE

#include <stdarg.h>
#include <math.h>

#include "configuration.h"
#include "trading_mgmt.h"
#include "book_mgmt.h"
#include "order_mgmt_proc.h"
#include "arca_direct_proc.h"
#include "nasdaq_rash_proc.h"
#include "aggregation_server_proc.h"
#include "cqs_feed_proc.h"
#include "uqdf_feed_proc.h"
#include "raw_data_mgmt.h"
#include "bbo_mgmt.h"
#include "kernel_algorithm.h"
#include "hbitime.h"
#include "logging.h"

t_PositionInfo positionCollection[MAX_STOCK_SYMBOL];

pthread_mutex_t clientOrderID_Mutex;

t_TradingStatus tradingStatus;

int currentCrossID = 0;
pthread_mutex_t currentCrossIDMutex;

int currentOrderID = 0;
pthread_mutex_t currentOrderIDMutex;

pthread_mutex_t symbolTradingMutex[MAX_STOCK_SYMBOL];

int NumOfOrderHasInsideGoAwayOrSpreadTooWide = 0;

extern int StockSymbolIndex_to_ArcaSymbolIndex_Mapping[MAX_STOCK_SYMBOL];

/****************************************************************************
** Function definitions
****************************************************************************/

/****************************************************************************
- Function name:	InitializePositionCllection
- Input:			
- Output:		
- Return:			
- Description:	
- Usage:			
****************************************************************************/
int InitializePositionCllection(void)
{
	int i;

	for (i = 0; i < MAX_STOCK_SYMBOL; i++)
	{
		positionCollection[i].isTrading = NO;
		positionCollection[i].typePlacedOrder = LIQUIDATE_ORDER;
		positionCollection[i].willReplace = YES;
		positionCollection[i].isCalculatingInformation = NO;
		positionCollection[i].consecutiveRejectNumber = 0;
		positionCollection[i].releaseStuckSleeping = NO;
		positionCollection[i].replaceAfterSleeping = NO;
		positionCollection[i].startThread = NO;
		
		memset(&positionCollection[i].stuck, 0, sizeof(t_Position));
		memset(&positionCollection[i].bestQuote, 0, sizeof(t_TopOrder) * MAX_SIDE);
		memset(&positionCollection[i].holdQuote, 0, sizeof(t_QuoteHolding) * MAX_SIDE);
	}

	return SUCCESS;
}

/****************************************************************************
- Function name:	InitializeSymbolTradingMutex
- Input:		
- Output:		
- Return:		
- Description:	
- Usage:		
****************************************************************************/
int InitializeSymbolTradingMutex()
{
	int i;
	for (i = 0; i < MAX_STOCK_SYMBOL; i++)
	{
		pthread_mutex_init (&symbolTradingMutex[i], NULL);
	}

	return SUCCESS;
}

/****************************************************************************
- Function name:	CheckSelfCrossTrade
- Input:			+ newOrder
				+ side
				+ stock Symbol Index
- Output:		N/A
- Return:		N/A
- Description:	+ The routine hides the following facts
				 in stock book and pre-set configuration to determite has cross or not
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A 
****************************************************************************/
long CheckSelfCrossTrade(t_OrderDetail *newOrder, int newOrderSide, int stockSymbolIndex, int ecnLaunchID, t_TopOrder *_bestQuote)
{		
	t_TopOrder bestQuote;
	
	double currentSpread;
		
	/*
	Get best order.
	If side == ASK_SIDE then we will get best quote on BID_SIDE
	otherwise if side == BID_SIDE then we will get best quote on ASK_SIDE
	*/
	if (newOrder->shareVolume > MAX_ODD_LOT_SHARE)
	{
		// Get mutex lock to synchronize update/access on current Best Quote
		pthread_mutex_lock(&updateAskBidMutex[stockSymbolIndex]);

		GetBestOrderRoundLot(&bestQuote, !newOrderSide, stockSymbolIndex);

		// Release mutex lock
		pthread_mutex_unlock(&updateAskBidMutex[stockSymbolIndex]);
	}
	else
	{
		// Get mutex lock to synchronize update/access on current Best Quote
		pthread_mutex_lock(&updateAskBidMutex[stockSymbolIndex]);

		GetBestOrder(&bestQuote, !newOrderSide, stockSymbolIndex);

		// Release mutex lock
		pthread_mutex_unlock(&updateAskBidMutex[stockSymbolIndex]);
	}
	
	if ((bestQuote.ecnIndicator == -1) || flt(bestQuote.info.sharePrice, 0.01))
	{
		return SUCCESS;
	}
		
	// Calculate current spread
	if (newOrderSide == BID_SIDE)
	{
		currentSpread = newOrder->sharePrice - bestQuote.info.sharePrice;
	}
	else// if (newOrderSide == ASK_SIDE)
	{
		currentSpread = bestQuote.info.sharePrice - newOrder->sharePrice;
	}
	
	if (fge(currentSpread, 0.0))
	{
		if (ecnLaunchID == bestQuote.ecnIndicator)
		{
			// _bestQuote is for debugging. We need to know if there is any problem with the price and shares of _bestQuote!
			memcpy(_bestQuote, &bestQuote, sizeof(t_TopOrder));
			
			return bestQuote.info.refNum;
		}
	}
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	CheckAndPlaceOrderWhenSymbolResume
- Input:			symbol name
- Output:			N/A
- Return:			N/A
- Description:		Check to see if there is any existing stuck for the symbol
					If so, call CheckAndPlaceOrder()
- Usage:			Called when symbol status changed from HALTED to RESUMED
****************************************************************************/
void CheckAndPlaceOrderWhenSymbolResume (char *symbol)
{
	// Get symbol index
	int symbolIndex = GetStockSymbolIndex(symbol);
	
	if (symbolIndex == -1)
	{
		symbolIndex = UpdateStockSymbolIndex(symbol);
		if (symbolIndex == -1)
			return;
	}
	
	// Check if there is any stuck for this symbol currently. If there is, Resume handling it.
	if (positionCollection[symbolIndex].isTrading == NO)
	{
		if (positionCollection[symbolIndex].stuck.shares > 0)
		{
			TraceLog(DEBUG_LEVEL, "Symbol '%.8s' is resumed, PM will request AS to get the stuck info...\n", symbol);
			SendASMessageToRequestSendStuckInfo(&positionCollection[symbolIndex].stuck);
		}
		else
		{
			// Prepare stuck information for send to AS
			t_Position stuck;
			memset(&stuck, 0, sizeof(t_Position));
			
			strncpy(stuck.symbol, symbol, SYMBOL_LEN);
			
			TraceLog(DEBUG_LEVEL, "Symbol '%.8s' is resumed, PM will request AS to get the stuck info.\n", symbol);
			
			stuck.accountSuffix = FIRST_ACCOUNT;
			TraceLog(DEBUG_LEVEL, "Get the stuck info of symbol '%.8s' for account = %d.\n", symbol, stuck.accountSuffix);
			SendASMessageToRequestSendStuckInfo(&stuck);
			
			stuck.accountSuffix = SECOND_ACCOUNT;
			TraceLog(DEBUG_LEVEL, "Get the stuck info of symbol '%.8s' for account = %d.\n", symbol, stuck.accountSuffix);
			SendASMessageToRequestSendStuckInfo(&stuck);
			
			stuck.accountSuffix = THIRD_ACCOUNT;
			TraceLog(DEBUG_LEVEL, "Get the stuck info of symbol '%.8s' for account = %d.\n", symbol, stuck.accountSuffix);
			SendASMessageToRequestSendStuckInfo(&stuck);
			
			stuck.accountSuffix = FOURTH_ACCOUNT;
			TraceLog(DEBUG_LEVEL, "Get the stuck info of symbol '%.8s' for account = %d.\n", symbol, stuck.accountSuffix);
			SendASMessageToRequestSendStuckInfo(&stuck);
		}
	}
	
	return;
}

/****************************************************************************
- Function name:	CheckHaltedECNForSymbol
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int CheckHaltedECNForSymbol(int tradeSymbolIndex, int ecnIndex)
{
	if(ecnIndex < 0 || ecnIndex >= MAX_BOOK_CONNECTIONS)
	{
		return -1;
	}
	
	if(tradeSymbolIndex < 0 || tradeSymbolIndex >= MAX_SYMBOL_TRADE)
	{
		return -1;
	}
	
	if(tradeSymbolList.symbolInfo[tradeSymbolIndex].haltStatus[ecnIndex] == SYMBOL_STATUS_HALT)
	{
		return ecnIndex; // ecn halted
	}
	else
	{
		return -1;
	}
}

/****************************************************************************
- Function name:	CheckAndPlaceOrder
- Input:			Stuck information, Cross ID	
- Output:			N/A
- Return:			The returned value has no-use
- Description:		Handling an open position.
					This function is called when:
						- position arrives from AS (TT engages/TS trade output log)
						- PM algorithm, canceled an order and call this function
- Note: 			The mutex lock is used to avoid more than one order for a symbol
					are to be placed at a time
- Usage:			
****************************************************************************/
int CheckAndPlaceOrder(t_Position *stuck, int crossId, int needSleep, int replaceAfterSleeping, int indexOpenOrder)
{
	/* 	If stuck->price is NOT 0.00: Stuck is from TS Output Log
		If stuck->price is 0.00: Stuck is from:
			- TT engages
			- PM requests to resume handling from halted state
			- Auto enqueue another stuck after a stuck has been liquidated
	*/
	
	int hasStuckSleeping = 0;
	
	if(replaceAfterSleeping == YES && indexOpenOrder != -1)
	{
		if (asOpenOrder.orderSummary[indexOpenOrder].status == orderStatusIndex.Sleeping)
		{
			asOpenOrder.orderSummary[indexOpenOrder].status = orderStatusIndex.CanceledByUser;
		}
	}
	
	// Check symbol index
	stuck->symbolIndex = GetStockSymbolIndex(stuck->symbol);

	// If stock symbol is not in SymbolList
	if (stuck->symbolIndex == -1)
	{
		stuck->symbolIndex = UpdateStockSymbolIndex(stuck->symbol);
		if (stuck->symbolIndex == -1)
		{
			TraceLog(ERROR_LEVEL, "Cannot find symbol = %.8s in SymbolList\n", stuck->symbol);
			SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_CAN_NOT_FIND_SYMBOL);
			return ERROR;
		}
	}
	
	// If there is a new cross ID, use it
	// Otherwise, use the old cross ID
	if (crossId != 0)
	{
		CrossIDCollection[stuck->symbolIndex] = crossId;
	}
	
	int tradeSymbolStatusIndex = GetTradeSymbolIndex(stuck->symbol);
	if (tradeSymbolStatusIndex == -1)
	{
		TraceLog(ERROR_LEVEL, "Cannot update symbol = %.8s to TradeSymbolList\n", stuck->symbol);
		SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_CAN_NOT_FIND_SYMBOL);
		return ERROR;
	}
	
	// Check symbol ignored
	if (symbolIgnoreStatus[stuck->symbolIndex] == DISABLE)
	{
		TraceLog(ERROR_LEVEL, "Symbol (%.8s) was ignored. Cannot place order\n", stuck->symbol);
		
		SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_SYMBOL_IGNORED);
		return ERROR;
	}

	pthread_mutex_lock(&symbolTradingMutex[stuck->symbolIndex]);
	
	int i;
	
	if(replaceAfterSleeping == YES)
	{
		int isSent = NO;
		for (i = 0; i < asOpenOrder.countOpenOrder; i++)
		{
			if ( ((asOpenOrder.orderSummary[i].status == orderStatusIndex.Live) || (asOpenOrder.orderSummary[i].status == orderStatusIndex.Sent))
				&& (asOpenOrder.orderSummary[i].symbolIndex == stuck->symbolIndex) )
			{
				isSent = YES;
				break;
			}
		}
		
		if (isSent == YES)
		{
			TraceLog(DEBUG_LEVEL, "Release Stuck Sleeping\n");
			pthread_mutex_unlock(&symbolTradingMutex[stuck->symbolIndex]);
			return ERROR;
		}
	}
	
	int forceEngage = 0;
	
	//Check if symbol is halted or enable trading
	if ((tradeSymbolList.symbolInfo[tradeSymbolStatusIndex].haltStatus[BOOK_NASDAQ] == SYMBOL_STATUS_HALT) && 
		(tradeSymbolList.symbolInfo[tradeSymbolStatusIndex].haltStatus[BOOK_ARCA] == SYMBOL_STATUS_HALT) )
	{
		// Symbol is halted so PM saves the stuck to the list. PM will resume handling when symbol is resumed
		TraceLog(DEBUG_LEVEL, "PM: symbol (%.8s) was halted on both ARCA and NASDAQ. PM will resume handling automatically when the symbol is resumed\n", stuck->symbol);
		
		memcpy(&positionCollection[stuck->symbolIndex].stuck, stuck, sizeof(t_Position));
		
		if (!feq(stuck->price, 0.00))	// Position from TS
		{
			TraceLog(DEBUG_LEVEL, "Symbol (%.8s), index %d, was halted. Cannot place order.\n", stuck->symbol, tradeSymbolStatusIndex);
			
			SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_SYMBOL_HALTED);
			pthread_mutex_unlock(&symbolTradingMutex[stuck->symbolIndex]);
			return ERROR;
		}
		else	// Position from TT
		{
			/* 
				Auto-resume value:
					isAutoResume = 0 --> add by user or default value when load list. PM can not resume this position
					isAutoResume = 1 --> add by ECN or user engage. PM can resume this position
					isAutoResume = 2 --> user disengage. PM can not resume this position
			*/
			if (tradeSymbolList.symbolInfo[tradeSymbolStatusIndex].isAutoResume != 1)
			{
				tradeSymbolList.symbolInfo[tradeSymbolStatusIndex].isAutoResume = 1;
				
				SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_SYMBOL_HALTED);
				pthread_mutex_unlock(&symbolTradingMutex[stuck->symbolIndex]);
				return ERROR;
			}
			else //isAutoResume = 1
			{
				forceEngage = 1;
				TraceLog(DEBUG_LEVEL, "Symbol (%.8s), index %d, was halted. But engaged by user, so allow trading on it.\n", stuck->symbol, tradeSymbolStatusIndex);
			}
		}
	}

	
	/* Mark:
		For multiple stucks on the same side (Long/Long or Short/Short):
			- If in the same account, aggregate the position and handle it
			- If in separate accounts, when the new position arrives, if it has a size larger than the previous stuck position,
				PM should disengage the previous position, engage the new position, and enqueue the previous one for handling 
				after the new position is liquidated.

		For multiple stucks on opposite sides (Long/Short):
			- If in the same account, aggregate the position and recalculate side/shares.
			- If in separate accounts, when the new position arrives, evaluate for which position's price is
				more profitable (or less of a loser), and engage that position first, enqueuing the other one.
	*/
	
	int indexToBeCanceled = -1;
	int lastReplaceStatus = positionCollection[stuck->symbolIndex].willReplace;
	positionCollection[stuck->symbolIndex].willReplace = NO;

	// Start to evaluate current position which is being traded
	int action = 0;		//action: 0 - Keep current trading position, do not aggregate current (do nothing), but engage arrival, so it updates trading status on AS
						//action: 1 - Aggregate (when same account)
						//action: 2 - Disengage current, replace by new one
						

	for (i = 0; i < asOpenOrder.countOpenOrder; i++)
	{
		if ( ((asOpenOrder.orderSummary[i].status == orderStatusIndex.Live) || (asOpenOrder.orderSummary[i].status == orderStatusIndex.Sent)
				|| (replaceAfterSleeping == NO && asOpenOrder.orderSummary[i].status == orderStatusIndex.Sleeping))
					&& (asOpenOrder.orderSummary[i].symbolIndex == stuck->symbolIndex) )
		{
			indexToBeCanceled = i;
			
			if(replaceAfterSleeping == NO && asOpenOrder.orderSummary[i].status == orderStatusIndex.Sleeping)
			{
				hasStuckSleeping = 1;
			}
			
			break;
		}
	}
	
	if (indexToBeCanceled != -1)																//There is order trading
	{
		if (asOpenOrder.orderSummary[indexToBeCanceled].accountSuffix != stuck->accountSuffix)	//New position and current position belongs to different account
		{
			if (asOpenOrder.orderSummary[indexToBeCanceled].side == stuck->side)				//same side
			{
				if (asOpenOrder.orderSummary[indexToBeCanceled].leftShares < stuck->shares)		//new one has lager shares
				{
					action = 2;																	//disengage current, replace by new position
				}
			}
			else																				//different side
			{
				/* evaluate which position takes priority */
				// Get last trade from UTDF/CTS
				
				t_TradeList *symbolInfo = &tradeSymbolList.symbolInfo[tradeSymbolStatusIndex];

				int latestTradeIndex = symbolInfo->countTrade - 1;
				if (latestTradeIndex < 0)
				{
					//There is no last trade, cannot evaluate
					positionCollection[stuck->symbolIndex].willReplace = lastReplaceStatus;
					TraceLog(DEBUG_LEVEL, "New position arrives, but there is no last sale (from CTS/UTDF) to evaluate, PM will not handle this position\n");
					SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_REJECTED_BY_PM);
					pthread_mutex_unlock(&symbolTradingMutex[stuck->symbolIndex]);
					return ERROR;
				}
				
				double lastSalePrice = symbolInfo->tradeList[latestTradeIndex % MAX_LAST_TRADE].price;
				
				// Calculate profit
				double currentProfit, newProfit;
				if (stuck->side == BID_SIDE)	//New position stuck LONG
				{
					newProfit = (lastSalePrice - stuck->referencePrice) * stuck->shares;
					currentProfit = (positionCollection[stuck->symbolIndex].stuck.referencePrice - lastSalePrice) * asOpenOrder.orderSummary[indexToBeCanceled].leftShares;
				}
				else
				{
					newProfit = (stuck->referencePrice - lastSalePrice) * stuck->shares;
					currentProfit = (lastSalePrice - positionCollection[stuck->symbolIndex].stuck.referencePrice) * asOpenOrder.orderSummary[indexToBeCanceled].leftShares;
				}
				
				// Check to see action should be 0 or 2
				if (fgt(newProfit, currentProfit))
				{
					if(asOpenOrder.orderSummary[indexToBeCanceled].status == orderStatusIndex.Sleeping)
					{
						asOpenOrder.orderSummary[indexToBeCanceled].status = orderStatusIndex.CanceledByUser;
					}
					
					action = 2;
					
					if(hasStuckSleeping == 1)
					{
						TraceLog(DEBUG_LEVEL, "Release Stuck Sleeping due to newProfit(%lf) > currentProfit(%lf)\n", newProfit, currentProfit);
						positionCollection[stuck->symbolIndex].releaseStuckSleeping = YES;
					}
				}
				else
				{
					action = 0;
				}
			}
		}
		else											//same account
		{
			action = 1;									//aggregate <current> and <new> to create a new position
		}
		
		if (action == 0)
		{
			positionCollection[stuck->symbolIndex].willReplace = lastReplaceStatus;
			SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_REJECTED_BY_PM);
			pthread_mutex_unlock(&symbolTradingMutex[stuck->symbolIndex]);
			return ERROR;
		}
	}
	
	
	if (indexToBeCanceled != -1)	//There is order trading
	{
		// Set .isCalculatingInformation = YES to tell "Order canceled" process that PM is re-calculating stuck information
		// so that, when the order is cancelling, PM will not going to place any order!
		positionCollection[stuck->symbolIndex].isCalculatingInformation = YES;
	
		/* When execution comes here, action will be 1 or 2
			1: Aggregate (when same account)
			2: Disengage current, replace by new one
			
			Both the cases need to cancel existing trading orders
		*/

		// ------------------------------------------------------------------------------------------------------
		// Cancel any existing order on that symbol
		//	- if order is being placed on NASDAQ, we can send cancel request now
		//	- if order is being placed on ARCA DIRECT, we must check if we have ECN Order ID (when received ACK),
		//		if not, we must wait for ACK before sending cancel request
		// ------------------------------------------------------------------------------------------------------
		
		int timeOut = 0;
		
		if(hasStuckSleeping == 0)
		{
			if (asOpenOrder.orderSummary[indexToBeCanceled].requester == TRADER_REQUEST)
			{
				int isAbleToSendCancel = NO;
				
				if (asOpenOrder.orderSummary[indexToBeCanceled].ECNId == TYPE_ARCA_DIRECT)
				{
					// Must wait for ACK to get ECN Order ID
					// wait for at most 5000*100us = 0.5 seconds 

					while ((isAbleToSendCancel == NO) && (timeOut < 5000))
					{
						if (asOpenOrder.orderSummary[indexToBeCanceled].status != orderStatusIndex.Sent)
						{
							isAbleToSendCancel = YES;
							break;
						}
					
						timeOut++;
						usleep(100);
					}
				}
				else
				{
					isAbleToSendCancel = YES;
				}
				
				if (isAbleToSendCancel == YES)
				{
					TraceLog(DEBUG_LEVEL, "Cancel existing order on symbol '%.8s', orderId = %d\n", stuck->symbol, asOpenOrder.orderSummary[indexToBeCanceled].clOrdId);
					ProcessSendCancelRequestToECNOrder(asOpenOrder.orderSummary[indexToBeCanceled].clOrdId, asOpenOrder.orderSummary[indexToBeCanceled].ecnOrderId);
				}	//else .... should stop ???
			}
			else
			{
				asOpenOrder.orderSummary[indexToBeCanceled].requester = TRADER_REQUEST;
			}		
		
			// ------------------------------------------------------------------------------------------
			// PM will wait for it to be fully canceled. Wait for at most 5000*100us = 0.5 seconds
			// ------------------------------------------------------------------------------------------
			int isCanceled = NO;
			timeOut = 0;
			
			while ((isCanceled == NO) && (timeOut < 5000))
			{
				//if (asOpenOrder.orderSummary[indexToBeCanceled].status > orderStatusIndex.Live)
				if (asOpenOrder.orderSummary[indexToBeCanceled].canceledShares >= asOpenOrder.orderSummary[indexToBeCanceled].leftShares)
				{
					isCanceled = YES;
					break;
				}
				
				timeOut ++;
				if ((timeOut % 100) == 0)
				{
					TraceLog(DEBUG_LEVEL, "Cancelling existing order, please wait (%d)...\n", timeOut/100);
				}

				// Delay 100 micro-seconds
				usleep(100);
			}

			if (isCanceled == NO)
			{
				TraceLog(DEBUG_LEVEL, "Could not cancel existing orders, PM will disengage trading of this symbol\n");
				positionCollection[stuck->symbolIndex].willReplace = lastReplaceStatus;
				SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_CANT_CANCEL_EXISTING_ORDER);
				positionCollection[stuck->symbolIndex].isCalculatingInformation = NO;
				pthread_mutex_unlock(&symbolTradingMutex[stuck->symbolIndex]);
				return ERROR;
			}
		}

		// -----------------------------------------------------
		// All orders have been canceled successfully
		// -----------------------------------------------------
		
		if (action == 1)	// Aggregate (when same account)
		{
			if(asOpenOrder.orderSummary[indexToBeCanceled].status == orderStatusIndex.Sleeping)
			{
				asOpenOrder.orderSummary[indexToBeCanceled].status = orderStatusIndex.CanceledByUser;
			}
			positionCollection[stuck->symbolIndex].releaseStuckSleeping = YES;
			
			/* Aggregate shares to determine new position (size & side) */
			//int buyShares = 0, sellShares = 0;
			int leftShares = 0;
			
			if (asOpenOrder.orderSummary[indexToBeCanceled].leftShares > 0)
			{
				leftShares = asOpenOrder.orderSummary[indexToBeCanceled].leftShares;
			}

			// --------------------------------------------------------------------
			// Depending information from last canceled order (shares, prices)
			// PM will adjust the stuck side, shares, price
			// --------------------------------------------------------------------
			if (stuck->side == ASK_SIDE)
			{
				if (asOpenOrder.orderSummary[indexToBeCanceled].side == SELL_TYPE)		// Current stuck and new stuck are of DIFFERENT side
				{
					if (stuck->shares >= leftShares)
					{
						stuck->shares -= leftShares;	//side does not change, just reduce shares
					}
					else
					{
						stuck->side = BID_SIDE;			//side and shares changed
						stuck->shares = (leftShares - stuck->shares);
						stuck->price = positionCollection[stuck->symbolIndex].stuck.price;
					}
				}
				else																	// Current stuck and new stuck are of the SAME side
				{
					if (flt(stuck->price, 0.01))
					{
						stuck->price = positionCollection[stuck->symbolIndex].stuck.price;
					}
					else
					{
						stuck->price = (stuck->shares * stuck->price + leftShares * positionCollection[stuck->symbolIndex].stuck.price) / (stuck->shares + leftShares);
					}
					
					stuck->shares += leftShares;
				}
			}
			else
			{
				if (asOpenOrder.orderSummary[indexToBeCanceled].side == SELL_TYPE)		// Current stuck and new stuck are of SAME side
				{
					if (flt(stuck->price, 0.01))
					{
						stuck->price = positionCollection[stuck->symbolIndex].stuck.price;
					}
					else
					{
						stuck->price = (stuck->shares * stuck->price + leftShares * positionCollection[stuck->symbolIndex].stuck.price) / (stuck->shares + leftShares);
					}
					
					stuck->shares += leftShares;
				}
				else																	// Current stuck and new stuck are of DIFFERENT side
				{
					if (stuck->shares >= leftShares)
					{
						//side does not change, just reduce shares
						stuck->shares -= leftShares;
					}
					else
					{
						// side and shares changed
						// use the old stuck price
						stuck->side = ASK_SIDE;
						stuck->shares = (leftShares - stuck->shares);
						stuck->price = positionCollection[stuck->symbolIndex].stuck.price;
					}
				}
			}
			
			TraceLog(DEBUG_LEVEL, "Re-Calculated stuck info: side:%d, shares:%d, price:%.2lf\n", stuck->side, stuck->shares, stuck->price);

			if (stuck->shares == 0)
			{
				TraceLog(DEBUG_LEVEL, "Stuck shares:0, can not place order\n");
				positionCollection[stuck->symbolIndex].willReplace = lastReplaceStatus;
				SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_ZERO_SHARES);
				positionCollection[stuck->symbolIndex].isCalculatingInformation = NO;
				pthread_mutex_unlock(&symbolTradingMutex[stuck->symbolIndex]);
				return ERROR;
			}
		}
		
		positionCollection[stuck->symbolIndex].isCalculatingInformation = NO;
	}
	
	
	/****************************************************************************************************
	Get best quotes
	+ Bid Quote
	+ Ask Quote
	****************************************************************************************************/
	
	int haltedECN_ASK = -1,
		haltedECN_BID = -1;
	
	t_TopOrder bestQuote[MAX_SIDE];

	if (stuck->shares > MAX_ODD_LOT_SHARE)
	{
		TraceLog(DEBUG_LEVEL, "Ignore any quoted odd lot!\n");
		
		// Get mutex lock to synchronize update/access on current Best Quote
		pthread_mutex_lock(&updateAskBidMutex[stuck->symbolIndex]);

		GetBestOrderRoundLot(&bestQuote[ASK_SIDE], ASK_SIDE, stuck->symbolIndex);
		GetBestOrderRoundLot(&bestQuote[BID_SIDE], BID_SIDE, stuck->symbolIndex);
		
		if(forceEngage == 0)
		{
			haltedECN_ASK = CheckHaltedECNForSymbol(tradeSymbolStatusIndex, bestQuote[ASK_SIDE].ecnIndicator);
			haltedECN_BID = CheckHaltedECNForSymbol(tradeSymbolStatusIndex, bestQuote[BID_SIDE].ecnIndicator);
		}
		
		//khanhnd: Save best quote that get was -> initialize hold quote
		positionCollection[stuck->symbolIndex].holdQuote[ASK_SIDE].isHold = NO;
		positionCollection[stuck->symbolIndex].holdQuote[BID_SIDE].isHold = NO;
		
		// Release mutex lock
		pthread_mutex_unlock(&updateAskBidMutex[stuck->symbolIndex]);
	}
	else
	{
		// Get mutex lock to synchronize update/access on current Best Quote
		pthread_mutex_lock(&updateAskBidMutex[stuck->symbolIndex]);

		GetBestOrder(&bestQuote[ASK_SIDE], ASK_SIDE, stuck->symbolIndex);
		GetBestOrder(&bestQuote[BID_SIDE], BID_SIDE, stuck->symbolIndex);
		
		if(forceEngage == 0)
		{
			haltedECN_ASK = CheckHaltedECNForSymbol(tradeSymbolStatusIndex, bestQuote[ASK_SIDE].ecnIndicator);
			haltedECN_BID = CheckHaltedECNForSymbol(tradeSymbolStatusIndex, bestQuote[BID_SIDE].ecnIndicator);
		}
		
		//khanhnd: Save best quote that get was -> initialize hold quote
		positionCollection[stuck->symbolIndex].holdQuote[ASK_SIDE].isHold = NO;
		positionCollection[stuck->symbolIndex].holdQuote[BID_SIDE].isHold = NO;

		// Release mutex lock
		pthread_mutex_unlock(&updateAskBidMutex[stuck->symbolIndex]);
	}

	// Trying update MinAsk and get best order
	if (flt(bestQuote[ASK_SIDE].info.sharePrice, 0.01) || haltedECN_ASK != -1)
	{
		if (stuck->shares > MAX_ODD_LOT_SHARE)
		{
			// Update MinAsk
			UpdateMinAskRoundLot(stuck->symbolIndex, 0, YES, haltedECN_ASK);

			// Get mutex lock to synchronize update/access on current Best Quote
			pthread_mutex_lock(&updateAskBidMutex[stuck->symbolIndex]);

			GetBestOrderRoundLot(&bestQuote[ASK_SIDE], ASK_SIDE, stuck->symbolIndex);
			
			//khanhnd: Save best quote that get was -> initialize hold quote
			positionCollection[stuck->symbolIndex].holdQuote[ASK_SIDE].isHold = NO;
			
			// Release mutex lock
			pthread_mutex_unlock(&updateAskBidMutex[stuck->symbolIndex]);
		}
		else
		{
			// Update MinAsk
			UpdateMinAsk(stuck->symbolIndex, 0, haltedECN_ASK);

			// Get mutex lock to synchronize update/access on current Best Quote
			pthread_mutex_lock(&updateAskBidMutex[stuck->symbolIndex]);

			GetBestOrder(&bestQuote[ASK_SIDE], ASK_SIDE, stuck->symbolIndex);
			
			//khanhnd: Save best quote that get was -> initialize hold quote
			positionCollection[stuck->symbolIndex].holdQuote[ASK_SIDE].isHold = NO;

			// Release mutex lock
			pthread_mutex_unlock(&updateAskBidMutex[stuck->symbolIndex]);
		}
	}
		
	/****************************************************************************************************
	Get best BBO quotes
	+ Bid Quote
	+ Ask Quote
	****************************************************************************************************/
	t_BestBBO bestBBO[MAX_SIDE];
	
	bestBBO[ASK_SIDE].price = 0.0;
	bestBBO[BID_SIDE].price = 0.0;

	GetBestBBOOrder(&bestBBO[ASK_SIDE], ASK_SIDE, stuck->symbolIndex);
	GetBestBBOOrder(&bestBBO[BID_SIDE], BID_SIDE, stuck->symbolIndex);
	
	// Check to get best ASK
	if (bestBBO[ASK_SIDE].participantId != -1)
	{
		if(haltedECN_ASK != ParticipantToExchangeIndexMapping[bestBBO[ASK_SIDE].participantId - 65])
		{
			if (fgt(bestQuote[ASK_SIDE].info.sharePrice, 0.01))
			{
				if (fgt(bestQuote[ASK_SIDE].info.sharePrice, bestBBO[ASK_SIDE].price))
				{
					bestQuote[ASK_SIDE].info.sharePrice = bestBBO[ASK_SIDE].price;
					bestQuote[ASK_SIDE].info.shareVolume = bestBBO[ASK_SIDE].size;
					bestQuote[ASK_SIDE].ecnIndicator = bestBBO[ASK_SIDE].participantId;
				}
			}
			else
			{
				bestQuote[ASK_SIDE].info.sharePrice = bestBBO[ASK_SIDE].price;
				bestQuote[ASK_SIDE].info.shareVolume = bestBBO[ASK_SIDE].size;
				bestQuote[ASK_SIDE].ecnIndicator = bestBBO[ASK_SIDE].participantId;
			}
		}
	}
	
	if ((bestQuote[ASK_SIDE].ecnIndicator == -1) || flt(bestQuote[ASK_SIDE].info.sharePrice, 0.01))
	{
		if(replaceAfterSleeping == YES)
		{
			t_SendNewOrder newOrder;
			GetNewOrder(&newOrder, stuck); 
			ReplaceOrderAfterSleeping(&newOrder, stuck);
		}
		else if(needSleep == YES)
		{
			NumOfOrderHasInsideGoAwayOrSpreadTooWide++;
			if(indexOpenOrder != -1)
			{
				asOpenOrder.orderSummary[indexOpenOrder].status = orderStatusIndex.Sleeping;
			}
			t_PositionInfo *positionItem = &positionCollection[stuck->symbolIndex];
			positionItem->replaceAfterSleeping = YES;
			memcpy(&positionItem->stuck, stuck, sizeof(t_Position));
		}
		else
		{
			TraceLog(DEBUG_LEVEL, "GetBestOrder: Best ASK empty!\n");
			SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_BEST_ASK_NOTHING);
		}
		
		positionCollection[stuck->symbolIndex].willReplace = lastReplaceStatus;
		pthread_mutex_unlock(&symbolTradingMutex[stuck->symbolIndex]);
		return ERROR;
	}
	
	if (bestQuote[ASK_SIDE].ecnIndicator < 10)	// ARCA Or Nasdaq
	{
		TraceLog(DEBUG_LEVEL, "Ask quote: ECN(%d) %d %.2lf\n", bestQuote[ASK_SIDE].ecnIndicator, bestQuote[ASK_SIDE].info.shareVolume, bestQuote[ASK_SIDE].info.sharePrice);
	}
	else
	{
		TraceLog(DEBUG_LEVEL, "Ask quote: Exchange(%c) %d %.2lf\n", bestQuote[ASK_SIDE].ecnIndicator, bestQuote[ASK_SIDE].info.shareVolume, bestQuote[ASK_SIDE].info.sharePrice);
	}

	// Trying update MaxBid and get best order
	if (flt(bestQuote[BID_SIDE].info.sharePrice, 0.01) || haltedECN_BID != -1)
	{
		if (stuck->shares > MAX_ODD_LOT_SHARE)
		{
			// Update MaxBid
			UpdateMaxBidRoundLot(stuck->symbolIndex, 0, YES, haltedECN_BID);

			// Get mutex lock to synchronize update/access on current Best Quote
			pthread_mutex_lock(&updateAskBidMutex[stuck->symbolIndex]);

			GetBestOrderRoundLot(&bestQuote[BID_SIDE], BID_SIDE, stuck->symbolIndex);
			
			//khanhnd: Save best quote that get was -> initialize hold quote
			positionCollection[stuck->symbolIndex].holdQuote[BID_SIDE].isHold = NO;
			
			// Release mutex lock
			pthread_mutex_unlock(&updateAskBidMutex[stuck->symbolIndex]);
		}
		else
		{
			// Update MaxBid
			UpdateMaxBid(stuck->symbolIndex, 0, haltedECN_BID);

			// Get mutex lock to synchronize update/access on current Best Quote
			pthread_mutex_lock(&updateAskBidMutex[stuck->symbolIndex]);

			GetBestOrder(&bestQuote[BID_SIDE], BID_SIDE, stuck->symbolIndex);
			
			//khanhnd: Save best quote that get was -> initialize hold quote
			positionCollection[stuck->symbolIndex].holdQuote[BID_SIDE].isHold = NO;
			
			// Release mutex lock
			pthread_mutex_unlock(&updateAskBidMutex[stuck->symbolIndex]);
		}
	}
	
	// Check to get best BID
	if (flt(bestQuote[BID_SIDE].info.sharePrice, bestBBO[BID_SIDE].price))
	{
		if(haltedECN_BID != ParticipantToExchangeIndexMapping[bestBBO[BID_SIDE].participantId - 65])
		{
			bestQuote[BID_SIDE].info.sharePrice = bestBBO[BID_SIDE].price;
			bestQuote[BID_SIDE].info.shareVolume = bestBBO[BID_SIDE].size;
			bestQuote[BID_SIDE].ecnIndicator = bestBBO[BID_SIDE].participantId;
		}
	}
	
	if ((bestQuote[BID_SIDE].ecnIndicator == -1) || flt(bestQuote[BID_SIDE].info.sharePrice, 0.01))
	{
		if(replaceAfterSleeping == YES)
		{
			t_SendNewOrder newOrder;
			GetNewOrder(&newOrder, stuck); 
			ReplaceOrderAfterSleeping(&newOrder, stuck);
		}
		else if(needSleep == YES)
		{
			NumOfOrderHasInsideGoAwayOrSpreadTooWide++;
			if(indexOpenOrder != -1)
			{
				asOpenOrder.orderSummary[indexOpenOrder].status = orderStatusIndex.Sleeping;
			}
			t_PositionInfo *positionItem = &positionCollection[stuck->symbolIndex];
			positionItem->replaceAfterSleeping = YES;
			memcpy(&positionItem->stuck, stuck, sizeof(t_Position));
		}
		else
		{
			TraceLog(DEBUG_LEVEL, "GetBestOrder: Best BID empty!\n");
			SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_BEST_BID_NOTHING);
		}
		
		positionCollection[stuck->symbolIndex].willReplace = lastReplaceStatus;
		pthread_mutex_unlock(&symbolTradingMutex[stuck->symbolIndex]);
		return ERROR;
	}
	
	if (bestQuote[BID_SIDE].ecnIndicator < 10)
	{
		TraceLog(DEBUG_LEVEL, "Bid quote: ECN(%d) %d %.2lf\n", bestQuote[BID_SIDE].ecnIndicator, bestQuote[BID_SIDE].info.shareVolume, bestQuote[BID_SIDE].info.sharePrice);
	}
	else
	{
		TraceLog(DEBUG_LEVEL, "Bid quote: Exchange(%c) %d %.2lf\n", bestQuote[BID_SIDE].ecnIndicator, bestQuote[BID_SIDE].info.shareVolume, bestQuote[BID_SIDE].info.sharePrice);
	}
		
	if (flt(stuck->price, 0.01))
	{
		stuck->price = (bestQuote[ASK_SIDE].info.sharePrice + bestQuote[BID_SIDE].info.sharePrice) / 2;
		stuck->price = (double)(floor(stuck->price * 100)) / 100;
		
		TraceLog(DEBUG_LEVEL, "Treat the stuck price = %.2lf\n", stuck->price);
	}
		
	double insideSpread = bestQuote[ASK_SIDE].info.sharePrice - bestQuote[BID_SIDE].info.sharePrice;
	
	TraceLog(DEBUG_LEVEL, "Inside spread = %.2lf\n", insideSpread);

	//We should not place an order if the stock is crossed
	if (fle(insideSpread, 0.0))
	{
		if (bestQuote[BID_SIDE].ecnIndicator == bestQuote[ASK_SIDE].ecnIndicator)
		{
			if (bestQuote[BID_SIDE].ecnIndicator < 10)
				TraceLog(DEBUG_LEVEL, "BID >= ASK on the same ECN(%d)\n", bestQuote[BID_SIDE].ecnIndicator);
			else
				TraceLog(DEBUG_LEVEL, "BID >= ASK on the same Exchange(%c)\n", bestQuote[BID_SIDE].ecnIndicator);
			
			if(replaceAfterSleeping == YES)
			{
				t_SendNewOrder newOrder;
				GetNewOrder(&newOrder, stuck); 
				ReplaceOrderAfterSleeping(&newOrder, stuck);
			}
			else if(needSleep == YES)
			{
				NumOfOrderHasInsideGoAwayOrSpreadTooWide++;
				if(indexOpenOrder != -1)
				{
					asOpenOrder.orderSummary[indexOpenOrder].status = orderStatusIndex.Sleeping;
				}
				t_PositionInfo *positionItem = &positionCollection[stuck->symbolIndex];
				positionItem->replaceAfterSleeping = YES;
				memcpy(&positionItem->stuck, stuck, sizeof(t_Position));
			}
			else
			{
				SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_BID_GT_ASK);
			}
			
			positionCollection[stuck->symbolIndex].willReplace = lastReplaceStatus;
			pthread_mutex_unlock(&symbolTradingMutex[stuck->symbolIndex]);
			return ERROR;
		}
	}
	
	/*
	Aligning Inside: The best bid/best ask on the same side of the book as our position 
	(Long =	Ask , Short = Bid).
	*/
	int aligningSide = !stuck->side;

	/*
	Opposing Inside: The best bid/best ask on the opposing side of the book as our position
	(Long = Bid , Short = Ask).
	*/
	double opposingSpread;

	if (stuck->side == ASK_SIDE)
	{
		opposingSpread = stuck->price - bestQuote[ASK_SIDE].info.sharePrice;
	}
	else
	{
		opposingSpread = bestQuote[BID_SIDE].info.sharePrice - stuck->price;
	}

	TraceLog(DEBUG_LEVEL, "Opposing spread = %.2lf\n", opposingSpread);

	// Calculation tight spread, as defined by [<autospread> * <stuck price>, not to exceed <maxspread>]	
	double tightSpread = asCollection.globalConf.autoSpread * stuck->price;

	if (flt(tightSpread, 0.01))
	{
		tightSpread = 0.01;
	}

	TraceLog(DEBUG_LEVEL, "Tight spread = %lf\n", tightSpread);

	// Build order
	t_SendNewOrder newOrder;
	strncpy(newOrder.symbol, stuck->symbol, SYMBOL_LEN);
	newOrder.accountSuffix = stuck->accountSuffix;
	newOrder.symbolIndex = stuck->symbolIndex;
	newOrder.shares = stuck->shares;
	newOrder.maxFloor = 0;
	newOrder.tif = DAY_TYPE;
	newOrder.pegDef = 0;
	
	if (stuck->shares >= 100)
	{
		newOrder.maxFloor = 100;
	}

	if (stuck->side == ASK_SIDE)
	{
		newOrder.side = BUY_TO_CLOSE_TYPE;
	}
	else
	{
		newOrder.side = SELL_TYPE;
	}
	
	int executionRate = 0;
	executionRate = GetExecutionRatePerSecond(tradeSymbolStatusIndex);
	
	if (fle(insideSpread, tightSpread))
	{
		TraceLog(DEBUG_LEVEL, "The inside spread (%.2lf) <= tight spread (%lf).\n", insideSpread, tightSpread);

		if (fgt(insideSpread, asCollection.globalConf.maxSpread))
		{
			if ( executionRate > asCollection.globalConf.highExecutionRate)
			{
				TraceLog(DEBUG_LEVEL, "The inside spread (%.2lf) > max spread (%lf), but execution rate (%d) is high so moving to fast market situation.\n", insideSpread, asCollection.globalConf.maxSpread, executionRate);
				int ret = CheckAndPlaceOrderForFastMarket(stuck, needSleep, replaceAfterSleeping, indexOpenOrder, forceEngage);
				if (ret == ERROR)
				{
					positionCollection[stuck->symbolIndex].willReplace = lastReplaceStatus;
				}
				
				pthread_mutex_unlock(&symbolTradingMutex[stuck->symbolIndex]);
				return ret;
			}
			else
			{
				if(replaceAfterSleeping == YES)
				{
					t_SendNewOrder newOrder;
					GetNewOrder(&newOrder, stuck); 
					ReplaceOrderAfterSleeping(&newOrder, stuck);
				}
				else if(needSleep == YES)
				{
					NumOfOrderHasInsideGoAwayOrSpreadTooWide++;
					if(indexOpenOrder != -1)
					{
						asOpenOrder.orderSummary[indexOpenOrder].status = orderStatusIndex.Sleeping;
					}
					t_PositionInfo *positionItem = &positionCollection[stuck->symbolIndex];
					positionItem->replaceAfterSleeping = YES;
					memcpy(&positionItem->stuck, stuck, sizeof(t_Position));
				}
				else
				{
					TraceLog(DEBUG_LEVEL, "The inside spread (%.2lf) > max spread (%lf), and execution rate (%d) is low so disengage the symbol.\n", insideSpread, asCollection.globalConf.maxSpread, executionRate);
					SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_INSIDE_SPREAD_GT_MAX_SPREAD_EXEC_LOW);
				}
				
				positionCollection[stuck->symbolIndex].willReplace = lastReplaceStatus;
				pthread_mutex_unlock(&symbolTradingMutex[stuck->symbolIndex]);
				return ERROR;
			}
		}
		else
		{
			// Send a liquidate order with checking the execution rate
			newOrder.price = bestQuote[(int)stuck->side].info.sharePrice;

			if (bestQuote[(int)stuck->side].ecnIndicator == BOOK_NASDAQ)
			{
				if (orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == CONNECTED)
				{
					newOrder.ECNId = TYPE_NASDAQ_RASH;
					newOrder.maxFloor = 0;
				}
				else if (orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == CONNECTED)
				{
					newOrder.ECNId = TYPE_ARCA_DIRECT;
				}
				else
				{
					positionCollection[stuck->symbolIndex].willReplace = lastReplaceStatus;
					SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN);
					pthread_mutex_unlock(&symbolTradingMutex[stuck->symbolIndex]);
					return ERROR;
				}
			}
			else if (bestQuote[(int)stuck->side].ecnIndicator == BOOK_ARCA)
			{
				if (orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == CONNECTED)
				{
					newOrder.ECNId = TYPE_ARCA_DIRECT;
				}
				else if (orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == CONNECTED)
				{
					newOrder.ECNId = TYPE_NASDAQ_RASH;
					newOrder.maxFloor = 0;
				}
				else
				{
					positionCollection[stuck->symbolIndex].willReplace = lastReplaceStatus;
					SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN);
					pthread_mutex_unlock(&symbolTradingMutex[stuck->symbolIndex]);
					return ERROR;
				}
			}
			else	// Away market, need to check the config to see where to place order! new Order Price = BBO quote price!
			{
				// All orders sent to capture liquidity at EXCHANGES we DON'T DIRECTLY CONNECT TO will use the
				// RESERVE METHOD WITH 100 SHARES VISIBLE.
				// ---> NewOrder.MaxFloor = 100
				// If stuck share < 100 ---> NewOrder.MaxFloor = 0
				newOrder.ECNId = ExchangeStatusList[(int)ParticipantToExchangeIndexMapping[bestQuote[(int)stuck->side].ecnIndicator - 65]].ECNToPlace;
				
				if(newOrder.ECNId == TYPE_ARCA_DIRECT && StockSymbolIndex_to_ArcaSymbolIndex_Mapping[stuck->symbolIndex] == -1)
				{
					newOrder.ECNId = TYPE_NASDAQ_RASH;
				}
				
				if (newOrder.ECNId == TYPE_ARCA_DIRECT && orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == DISCONNECTED)
				{
					if (orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == DISCONNECTED)
					{
						positionCollection[stuck->symbolIndex].willReplace = lastReplaceStatus;
						SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN);
						pthread_mutex_unlock(&symbolTradingMutex[stuck->symbolIndex]);
						return ERROR;
					}
					else
					{
						newOrder.ECNId = TYPE_NASDAQ_RASH;
					}
				}
				else if (newOrder.ECNId == TYPE_NASDAQ_RASH && orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == DISCONNECTED)
				{	
					if (orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == DISCONNECTED || StockSymbolIndex_to_ArcaSymbolIndex_Mapping[stuck->symbolIndex] == -1)
					{
						positionCollection[stuck->symbolIndex].willReplace = lastReplaceStatus;
						SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN);
						pthread_mutex_unlock(&symbolTradingMutex[stuck->symbolIndex]);
						return ERROR;
					}
					else
					{
						newOrder.ECNId = TYPE_ARCA_DIRECT;
					}
				}
			}

			if (SendLiquidateOrder(&newOrder, stuck, 1, executionRate, 0, needSleep, replaceAfterSleeping, indexOpenOrder, forceEngage) == ERROR)
			{
				positionCollection[stuck->symbolIndex].willReplace = lastReplaceStatus;
				pthread_mutex_unlock(&symbolTradingMutex[stuck->symbolIndex]);
				return ERROR;
			}
		}
	}
	else if (fge(opposingSpread, 0.0))
	{
		TraceLog(DEBUG_LEVEL, "The opposing inside price indicates that the stuck is currently a profit: opposingSpread = %.2lf\n", opposingSpread);

		// Calculation tight profit spread, as defined by [<profitspread> * <stuck price>, not to exceed <maxprofitspread>]
		double tightProfitSpread = asCollection.globalConf.profitSpread * stuck->price;

		TraceLog(DEBUG_LEVEL, "Tight profit spread = %lf\n", tightProfitSpread);

		if (fle(insideSpread, tightProfitSpread))
		{
			TraceLog(DEBUG_LEVEL, "The inside spread (%.2lf) <= tight profit spread (%lf)\n", insideSpread, tightProfitSpread);
			
			if (fgt(insideSpread, asCollection.globalConf.maxProfitSpread))
			{
				if(replaceAfterSleeping == YES)
				{
					t_SendNewOrder newOrder;
					GetNewOrder(&newOrder, stuck); 
					ReplaceOrderAfterSleeping(&newOrder, stuck);
				}
				else if(needSleep == YES)
				{
					NumOfOrderHasInsideGoAwayOrSpreadTooWide++;
					if(indexOpenOrder != -1)
					{
						asOpenOrder.orderSummary[indexOpenOrder].status = orderStatusIndex.Sleeping;
					}
					t_PositionInfo *positionItem = &positionCollection[stuck->symbolIndex];
					positionItem->replaceAfterSleeping = YES;
					memcpy(&positionItem->stuck, stuck, sizeof(t_Position));
				}
				else
				{
					TraceLog(DEBUG_LEVEL, "The inside spread (%.2lf) > max profit spread (%lf). Disengage the symbol.\n", insideSpread, asCollection.globalConf.maxProfitSpread);	
					SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_INSIDE_SPREAD_GT_MAX_PROFIT_SPREAD);
				}
				
				positionCollection[stuck->symbolIndex].willReplace = lastReplaceStatus;
				pthread_mutex_unlock(&symbolTradingMutex[stuck->symbolIndex]);
				return ERROR;
			}
			else
			{
				// Send a liquidate order with checking the execution rate
				newOrder.price = bestQuote[(int)stuck->side].info.sharePrice;

				if (bestQuote[(int)stuck->side].ecnIndicator == BOOK_NASDAQ)
				{
					if (orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == CONNECTED)
					{
						newOrder.ECNId = TYPE_NASDAQ_RASH;
						newOrder.maxFloor = 0;
					}
					else if (orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == CONNECTED)
					{
						newOrder.ECNId = TYPE_ARCA_DIRECT;
					}
					else
					{
						positionCollection[stuck->symbolIndex].willReplace = lastReplaceStatus;
						SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN);
						pthread_mutex_unlock(&symbolTradingMutex[stuck->symbolIndex]);
						return ERROR;
					}
				}
				else if (bestQuote[(int)stuck->side].ecnIndicator == BOOK_ARCA)
				{
					if (orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == CONNECTED)
					{
						newOrder.ECNId = TYPE_ARCA_DIRECT;
					}
					else if (orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == CONNECTED)
					{
						newOrder.ECNId = TYPE_NASDAQ_RASH;
						newOrder.maxFloor = 0;
					}
					else
					{
						positionCollection[stuck->symbolIndex].willReplace = lastReplaceStatus;
						SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN);
						pthread_mutex_unlock(&symbolTradingMutex[stuck->symbolIndex]);
						return ERROR;
					}
				}
				else	// Away market, need to check the config to see where to place order! new Order Price = BBO quote price!
				{
					// All orders sent to capture liquidity at EXCHANGES we DON'T DIRECTLY CONNECT TO will use the
					// RESERVE METHOD WITH 100 SHARES VISIBLE.
					// ---> NewOrder.MaxFloor = 100
					newOrder.ECNId = ExchangeStatusList[(int)ParticipantToExchangeIndexMapping[bestQuote[(int)stuck->side].ecnIndicator - 65]].ECNToPlace;
					
					if(newOrder.ECNId == TYPE_ARCA_DIRECT && StockSymbolIndex_to_ArcaSymbolIndex_Mapping[stuck->symbolIndex] == -1)
					{
						newOrder.ECNId = TYPE_NASDAQ_RASH;
					}
					
					if (newOrder.ECNId == TYPE_ARCA_DIRECT && orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == DISCONNECTED)
					{
						if (orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == DISCONNECTED)
						{
							positionCollection[stuck->symbolIndex].willReplace = lastReplaceStatus;
							SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN);
							pthread_mutex_unlock(&symbolTradingMutex[stuck->symbolIndex]);
							return ERROR;
						}
						else
						{
							newOrder.ECNId = TYPE_NASDAQ_RASH;
						}
					}
					else if (newOrder.ECNId == TYPE_NASDAQ_RASH && orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == DISCONNECTED)
					{
						if (orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == DISCONNECTED || StockSymbolIndex_to_ArcaSymbolIndex_Mapping[stuck->symbolIndex] == -1)
						{
							positionCollection[stuck->symbolIndex].willReplace = lastReplaceStatus;
							SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN);
							pthread_mutex_unlock(&symbolTradingMutex[stuck->symbolIndex]);
							return ERROR;
						}
						else
						{
							newOrder.ECNId = TYPE_ARCA_DIRECT;
						}
					}
				}

				if (SendLiquidateOrder(&newOrder, stuck, 1, executionRate, 0, needSleep, replaceAfterSleeping, indexOpenOrder, forceEngage) == ERROR)
				{
					positionCollection[stuck->symbolIndex].willReplace = lastReplaceStatus;
					pthread_mutex_unlock(&symbolTradingMutex[stuck->symbolIndex]);
					return ERROR; //do not print sale data from CQS/CTS/UTDF
				}
			}
		}
		else
		{
			TraceLog(DEBUG_LEVEL, "The inside spread (%.2lf) > tight profit spread (%lf)\n", insideSpread, tightProfitSpread);

			// Send a BBO order with checking the executio rate
			if (bestQuote[aligningSide].ecnIndicator == BOOK_NASDAQ)
			{
				if (orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == CONNECTED)
				{
					newOrder.ECNId = TYPE_NASDAQ_RASH;
					newOrder.maxFloor = 0;
				}
				else if (orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == CONNECTED)
				{
					newOrder.ECNId = TYPE_ARCA_DIRECT;
				}
				else
				{
					positionCollection[stuck->symbolIndex].willReplace = lastReplaceStatus;
					SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN);
					pthread_mutex_unlock(&symbolTradingMutex[stuck->symbolIndex]);
					return ERROR;
				}
			}
			else if (bestQuote[aligningSide].ecnIndicator == BOOK_ARCA)
			{
				if (orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == CONNECTED)
				{
					newOrder.ECNId = TYPE_ARCA_DIRECT;
				}
				else if (orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == CONNECTED)
				{
					newOrder.ECNId = TYPE_NASDAQ_RASH;
					newOrder.maxFloor = 0;
				}
				else
				{
					positionCollection[stuck->symbolIndex].willReplace = lastReplaceStatus;
					SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN);
					pthread_mutex_unlock(&symbolTradingMutex[stuck->symbolIndex]);
					return ERROR;
				}
			}
			else	// Away market, need to check the config to see where to place order! new Order Price = BBO quote price!
			{
				// All orders sent to capture liquidity at EXCHANGES we DON'T DIRECTLY CONNECT TO will use the
				// RESERVE METHOD WITH 100 SHARES VISIBLE.
				// ---> NewOrder.MaxFloor = 100
				newOrder.ECNId = ExchangeStatusList[(int)ParticipantToExchangeIndexMapping[bestQuote[(int)stuck->side].ecnIndicator - 65]].ECNToPlace;
				
				if(newOrder.ECNId == TYPE_ARCA_DIRECT && StockSymbolIndex_to_ArcaSymbolIndex_Mapping[stuck->symbolIndex] == -1)
				{
					newOrder.ECNId = TYPE_NASDAQ_RASH;
				}
				
				if (newOrder.ECNId == TYPE_ARCA_DIRECT && orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == DISCONNECTED)
				{
					if (orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == DISCONNECTED)
					{
						positionCollection[stuck->symbolIndex].willReplace = lastReplaceStatus;
						SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN);
						pthread_mutex_unlock(&symbolTradingMutex[stuck->symbolIndex]);
						return ERROR;
					}
					else
					{
						newOrder.ECNId = TYPE_NASDAQ_RASH;
					}
				}
				else if (newOrder.ECNId == TYPE_NASDAQ_RASH && orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == DISCONNECTED)
				{
					if (orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == DISCONNECTED || StockSymbolIndex_to_ArcaSymbolIndex_Mapping[stuck->symbolIndex] == -1)
					{
						positionCollection[stuck->symbolIndex].willReplace = lastReplaceStatus;
						SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN);
						pthread_mutex_unlock(&symbolTradingMutex[stuck->symbolIndex]);
						return ERROR;
					}
					else
					{
						newOrder.ECNId = TYPE_ARCA_DIRECT;
					}
				}
			}
			
			if (aligningSide == ASK_SIDE)
			{
				bestQuote[aligningSide].info.sharePrice -= 0.01;
			}
			else
			{
				bestQuote[aligningSide].info.sharePrice += 0.01;
			}
			
			newOrder.price = bestQuote[aligningSide].info.sharePrice;

			if (SendBBOOrder(&newOrder, stuck, bestQuote, 1, executionRate, 0, needSleep, replaceAfterSleeping, indexOpenOrder, forceEngage) == ERROR)
			{
				positionCollection[stuck->symbolIndex].willReplace = lastReplaceStatus;
				pthread_mutex_unlock(&symbolTradingMutex[stuck->symbolIndex]);
				return ERROR; //do not print sale information form CQS/CTS/UTDF
			}
		}
	}
	else if (flt((opposingSpread + asCollection.globalConf.bigLoserAmount / stuck->shares), 0.0))
	{
		TraceLog(DEBUG_LEVEL, "The opposing inside price indicates that the stuck is currently a big loser: opposingSpread = %.2lf, bigLoserAmount = %lf\n", opposingSpread, asCollection.globalConf.bigLoserAmount);

		// Calculation tight big loser spread, as defined by [<bigloserspread> * <stuck price>, not to exceed <maxbigloserspread>]
		double tightBigLosertSpread = asCollection.globalConf.bigLoserSpread * stuck->price;

		TraceLog(DEBUG_LEVEL, "Tight big loser spread = %lf\n", tightBigLosertSpread);

		if (fle(insideSpread, tightBigLosertSpread))
		{
			TraceLog(DEBUG_LEVEL, "The inside spread (%.2lf) <= tight big loser spread (%lf)\n", insideSpread, tightBigLosertSpread);

			if (fgt(insideSpread, asCollection.globalConf.maxBigLoserSpread))
			{
				if (executionRate > asCollection.globalConf.highExecutionRate)
				{
					TraceLog(DEBUG_LEVEL, "The inside spread (%.2lf) > max big loser spread (%lf). But execution rate (%d) is high, so moving to Fast Market situation.\n", insideSpread, asCollection.globalConf.maxBigLoserSpread, executionRate);
					int ret = CheckAndPlaceOrderForFastMarket(stuck, needSleep, replaceAfterSleeping, indexOpenOrder, forceEngage);
					if (ret == ERROR)
						positionCollection[stuck->symbolIndex].willReplace = lastReplaceStatus;
					pthread_mutex_unlock(&symbolTradingMutex[stuck->symbolIndex]);
					return ret;
				}
				else
				{
					if(replaceAfterSleeping == YES)
					{
						t_SendNewOrder newOrder;
						GetNewOrder(&newOrder, stuck); 
						ReplaceOrderAfterSleeping(&newOrder, stuck);
					}
					else if(needSleep == YES)
					{
						NumOfOrderHasInsideGoAwayOrSpreadTooWide++;
						if(indexOpenOrder != -1)
						{
							asOpenOrder.orderSummary[indexOpenOrder].status = orderStatusIndex.Sleeping;
						}
						t_PositionInfo *positionItem = &positionCollection[stuck->symbolIndex];
						positionItem->replaceAfterSleeping = YES;
						memcpy(&positionItem->stuck, stuck, sizeof(t_Position));
					}
					else
					{
						TraceLog(DEBUG_LEVEL, "The inside spread (%.2lf) > max big loser spread (%lf). But execution rate (%d) is low, so disengage.\n", insideSpread, asCollection.globalConf.maxBigLoserSpread, executionRate);
						SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_INSIDE_SPREAD_GT_MAX_BIG_LOSER_SPREAD_EXEC_LOW);
					}
					
					positionCollection[stuck->symbolIndex].willReplace = lastReplaceStatus;
					pthread_mutex_unlock(&symbolTradingMutex[stuck->symbolIndex]);
					return ERROR;
				}
			}
			else
			{
				// Send a liquidate order with executionRate checking
				newOrder.price = bestQuote[(int)stuck->side].info.sharePrice;

				if (bestQuote[(int)stuck->side].ecnIndicator == BOOK_NASDAQ)
				{
					if (orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == CONNECTED)
					{
						newOrder.ECNId = TYPE_NASDAQ_RASH;
						newOrder.maxFloor = 0;
					}
					else if (orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == CONNECTED)
					{
						newOrder.ECNId = TYPE_ARCA_DIRECT;
					}
					else
					{
						positionCollection[stuck->symbolIndex].willReplace = lastReplaceStatus;
						SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN);
						pthread_mutex_unlock(&symbolTradingMutex[stuck->symbolIndex]);
						return ERROR;
					}
				}
				else if(bestQuote[(int)stuck->side].ecnIndicator == BOOK_ARCA)
				{
					if (orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == CONNECTED)
					{
						newOrder.ECNId = TYPE_ARCA_DIRECT;
					}
					else if (orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == CONNECTED)
					{
						newOrder.ECNId = TYPE_NASDAQ_RASH;
						newOrder.maxFloor = 0;
					}
					else
					{
						positionCollection[stuck->symbolIndex].willReplace = lastReplaceStatus;
						SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN);
						pthread_mutex_unlock(&symbolTradingMutex[stuck->symbolIndex]);
						return ERROR;
					}
				}
				else	// Away market, need to check the config to see where to place order! new Order Price = BBO quote price!
				{
					// All orders sent to capture liquidity at EXCHANGES we DON'T DIRECTLY CONNECT TO will use the
					// RESERVE METHOD WITH 100 SHARES VISIBLE.
					// ---> NewOrder.MaxFloor = 100

					newOrder.ECNId = ExchangeStatusList[(int)ParticipantToExchangeIndexMapping[bestQuote[(int)stuck->side].ecnIndicator - 65]].ECNToPlace;
					
					if(newOrder.ECNId == TYPE_ARCA_DIRECT && StockSymbolIndex_to_ArcaSymbolIndex_Mapping[stuck->symbolIndex] == -1)
					{
						newOrder.ECNId = TYPE_NASDAQ_RASH;
					}
				
					if (newOrder.ECNId == TYPE_ARCA_DIRECT && orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == DISCONNECTED)
					{
						if (orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == DISCONNECTED)
						{
							positionCollection[stuck->symbolIndex].willReplace = lastReplaceStatus;
							SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN);
							pthread_mutex_unlock(&symbolTradingMutex[stuck->symbolIndex]);
							return ERROR;
						}
						else
						{
							newOrder.ECNId = TYPE_NASDAQ_RASH;
						}
					}
					else if (newOrder.ECNId == TYPE_NASDAQ_RASH && orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == DISCONNECTED)
					{
						if (orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == DISCONNECTED || StockSymbolIndex_to_ArcaSymbolIndex_Mapping[stuck->symbolIndex] == -1)
						{
							positionCollection[stuck->symbolIndex].willReplace = lastReplaceStatus;
							SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN);
							pthread_mutex_unlock(&symbolTradingMutex[stuck->symbolIndex]);
							return ERROR;
						}
						else
						{
							newOrder.ECNId = TYPE_ARCA_DIRECT;
						}
					}
				}
				
				if (SendLiquidateOrder(&newOrder, stuck, 1 , executionRate, 0, needSleep, replaceAfterSleeping, indexOpenOrder, forceEngage) == ERROR)
				{
					positionCollection[stuck->symbolIndex].willReplace = lastReplaceStatus;
					pthread_mutex_unlock(&symbolTradingMutex[stuck->symbolIndex]);
					return ERROR; //do not print sale infor from CQS/CTS/UTDF
				}
			}
		}
		else
		{
			TraceLog(DEBUG_LEVEL, "The inside spread (%.2lf) > tight big loser spread (%lf)\n", insideSpread, tightBigLosertSpread);

			// Send a BBO order with execution rate checking
			if (bestQuote[aligningSide].ecnIndicator == BOOK_NASDAQ)
			{
				if (orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == CONNECTED)
				{
					newOrder.ECNId = TYPE_NASDAQ_RASH;
					newOrder.maxFloor = 0;
				}
				else if (orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == CONNECTED)
				{
					newOrder.ECNId = TYPE_ARCA_DIRECT;
				}
				else
				{
					positionCollection[stuck->symbolIndex].willReplace = lastReplaceStatus;
					SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN);
					pthread_mutex_unlock(&symbolTradingMutex[stuck->symbolIndex]);
					return ERROR;
				}
			}
			else if (bestQuote[aligningSide].ecnIndicator == BOOK_ARCA)
			{
				if (orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == CONNECTED)
				{
					newOrder.ECNId = TYPE_ARCA_DIRECT;
				}
				else if (orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == CONNECTED)
				{
					newOrder.ECNId = TYPE_NASDAQ_RASH;
					newOrder.maxFloor = 0;
				}
				else
				{
					positionCollection[stuck->symbolIndex].willReplace = lastReplaceStatus;
					SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN);
					pthread_mutex_unlock(&symbolTradingMutex[stuck->symbolIndex]);
					return ERROR;
				}
			}
			else	// Away market, need to check the config to see where to place order! new Order Price = BBO quote price!
			{
				// All orders sent to capture liquidity at EXCHANGES we DON'T DIRECTLY CONNECT TO will use the
				// RESERVE METHOD WITH 100 SHARES VISIBLE.
				// ---> NewOrder.MaxFloor = 100

				newOrder.ECNId = ExchangeStatusList[(int)ParticipantToExchangeIndexMapping[bestQuote[aligningSide].ecnIndicator - 65]].ECNToPlace;
				
				if(newOrder.ECNId == TYPE_ARCA_DIRECT && StockSymbolIndex_to_ArcaSymbolIndex_Mapping[stuck->symbolIndex] == -1)
				{
					newOrder.ECNId = TYPE_NASDAQ_RASH;
				}
				
				if (newOrder.ECNId == TYPE_ARCA_DIRECT && orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == DISCONNECTED)
				{				
					if (orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == DISCONNECTED)
					{
						positionCollection[stuck->symbolIndex].willReplace = lastReplaceStatus;
						SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN);
						pthread_mutex_unlock(&symbolTradingMutex[stuck->symbolIndex]);
						return ERROR;
					}
					else
					{
						newOrder.ECNId = TYPE_NASDAQ_RASH;
					}
				}
				else if (newOrder.ECNId == TYPE_NASDAQ_RASH && orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == DISCONNECTED)
				{
					if (orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == DISCONNECTED || StockSymbolIndex_to_ArcaSymbolIndex_Mapping[stuck->symbolIndex] == -1)
					{
						positionCollection[stuck->symbolIndex].willReplace = lastReplaceStatus;
						SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN);
						pthread_mutex_unlock(&symbolTradingMutex[stuck->symbolIndex]);
						return ERROR;
					}
					else
					{
						newOrder.ECNId = TYPE_ARCA_DIRECT;
					}
				}
			}
			
			if (aligningSide == ASK_SIDE)
			{
				bestQuote[aligningSide].info.sharePrice -= 0.01;
			}
			else
			{
				bestQuote[aligningSide].info.sharePrice += 0.01;
			}
			
			newOrder.price = bestQuote[aligningSide].info.sharePrice;
			
			if (SendBBOOrder(&newOrder, stuck, bestQuote, 1, executionRate, 0, needSleep, replaceAfterSleeping, indexOpenOrder, forceEngage) == ERROR)
			{
				positionCollection[stuck->symbolIndex].willReplace = lastReplaceStatus;
				pthread_mutex_unlock(&symbolTradingMutex[stuck->symbolIndex]);
				return ERROR; //Do not print sale infor from CQS/CTS/UTDF)
			}
		}
	}
	else
	{
		TraceLog(DEBUG_LEVEL, "The inside spread (%.2lf) > tight spread (%lf). But the opposing inside price indicates that the stuck is neither a profit nor a big loser: opposingSpread = %.2lf, bigLoserAmount = %lf\n", 
				insideSpread, tightSpread, opposingSpread, asCollection.globalConf.bigLoserAmount);
		
		// Send a BBO order
		if (bestQuote[aligningSide].ecnIndicator == BOOK_NASDAQ)
		{
			if (orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == CONNECTED)
			{
				newOrder.ECNId = TYPE_NASDAQ_RASH;
				newOrder.maxFloor = 0;
			}
			else if (orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == CONNECTED)
			{
				newOrder.ECNId = TYPE_ARCA_DIRECT;
			}
			else
			{
				positionCollection[stuck->symbolIndex].willReplace = lastReplaceStatus;
				SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN);
				pthread_mutex_unlock(&symbolTradingMutex[stuck->symbolIndex]);
				return ERROR;
			}
		}
		else if (bestQuote[aligningSide].ecnIndicator == BOOK_ARCA)
		{
			if (orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == CONNECTED)
			{
				newOrder.ECNId = TYPE_ARCA_DIRECT;
			}
			else if (orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == CONNECTED)
			{
				newOrder.ECNId = TYPE_NASDAQ_RASH;
				newOrder.maxFloor = 0;
			}
			else
			{
				positionCollection[stuck->symbolIndex].willReplace = lastReplaceStatus;
				SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN);
				pthread_mutex_unlock(&symbolTradingMutex[stuck->symbolIndex]);
				return ERROR;
			}
		}
		else	// Away market, need to check the config to see where to place order! new Order Price = BBO quote price!
		{
			// All orders sent to capture liquidity at EXCHANGES we DON'T DIRECTLY CONNECT TO will use the
			// RESERVE METHOD WITH 100 SHARES VISIBLE.
			// ---> NewOrder.MaxFloor = 100
			newOrder.ECNId = ExchangeStatusList[(int)ParticipantToExchangeIndexMapping[bestQuote[aligningSide].ecnIndicator - 65]].ECNToPlace;
			
			if(newOrder.ECNId == TYPE_ARCA_DIRECT && StockSymbolIndex_to_ArcaSymbolIndex_Mapping[stuck->symbolIndex] == -1)
			{
				newOrder.ECNId = TYPE_NASDAQ_RASH;
			}
			
			if (newOrder.ECNId == TYPE_ARCA_DIRECT && orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == DISCONNECTED)
			{
				if (orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == DISCONNECTED)
				{
					positionCollection[stuck->symbolIndex].willReplace = lastReplaceStatus;
					SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN);
					pthread_mutex_unlock(&symbolTradingMutex[stuck->symbolIndex]);
					return ERROR;
				}
				else
				{
					newOrder.ECNId = TYPE_NASDAQ_RASH;
				}
			}
			else if (newOrder.ECNId == TYPE_NASDAQ_RASH && orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == DISCONNECTED)
			{
				if (orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == DISCONNECTED || StockSymbolIndex_to_ArcaSymbolIndex_Mapping[stuck->symbolIndex] == -1)
				{
					positionCollection[stuck->symbolIndex].willReplace = lastReplaceStatus;
					SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN);
					pthread_mutex_unlock(&symbolTradingMutex[stuck->symbolIndex]);
					return ERROR;
				}
				else
				{
					newOrder.ECNId = TYPE_ARCA_DIRECT;
				}
			}
		}
		
		if (aligningSide == ASK_SIDE)
		{
			bestQuote[aligningSide].info.sharePrice -= 0.01;
		}
		else
		{
			bestQuote[aligningSide].info.sharePrice += 0.01;
		}
		
		newOrder.price = bestQuote[aligningSide].info.sharePrice;

		if (SendBBOOrder(&newOrder, stuck, bestQuote, 0, 0, 0, needSleep, replaceAfterSleeping, indexOpenOrder, forceEngage) == ERROR)
		{
			positionCollection[stuck->symbolIndex].willReplace = lastReplaceStatus;
			pthread_mutex_unlock(&symbolTradingMutex[stuck->symbolIndex]);
			return ERROR;
		}
	}

	pthread_mutex_unlock(&symbolTradingMutex[stuck->symbolIndex]);
	
	return SUCCESS;
}


/****************************************************************************
- Function name:	CheckAndPlaceOrderForFastMarket
- Input:			
- Output:		N/A
- Return:		The returned value has no-use
- Description:	+ The routine hides the following facts
				  - Check and place order normally	
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
int CheckAndPlaceOrderForFastMarket(t_Position *stuck, int needSleep, int replaceAfterSleeping, int indexOpenOrder, int forceEngage)
{
	/****************************************************************************************************
	Get best quotes
	+ Bid Quote
	+ Ask Quote
	****************************************************************************************************/
	t_TopOrder bestQuote[MAX_SIDE];

	int haltedECN_ASK = -1,
		haltedECN_BID = -1;
		
	int tradeSymbolStatusIndex = GetTradeSymbolIndex(stuck->symbol);
		
	if (stuck->shares > MAX_ODD_LOT_SHARE)
	{
		TraceLog(DEBUG_LEVEL, "Ignore any quoted odd lot!\n");
		
		// Get mutex lock to synchronize update/access on current Best Quote
		pthread_mutex_lock(&updateAskBidMutex[stuck->symbolIndex]);

		GetBestOrderRoundLot(&bestQuote[ASK_SIDE], ASK_SIDE, stuck->symbolIndex);
		GetBestOrderRoundLot(&bestQuote[BID_SIDE], BID_SIDE, stuck->symbolIndex);
		
		if(forceEngage == 0)
		{
			haltedECN_ASK = CheckHaltedECNForSymbol(tradeSymbolStatusIndex, bestQuote[ASK_SIDE].ecnIndicator);
			haltedECN_BID = CheckHaltedECNForSymbol(tradeSymbolStatusIndex, bestQuote[BID_SIDE].ecnIndicator);
		}

		//khanhnd: Save best quote that get was -> initialize hold quote
		positionCollection[stuck->symbolIndex].holdQuote[ASK_SIDE].isHold = NO;
		positionCollection[stuck->symbolIndex].holdQuote[BID_SIDE].isHold = NO;

		// Release mutex lock
		pthread_mutex_unlock(&updateAskBidMutex[stuck->symbolIndex]);
	}
	else
	{
		// Get mutex lock to synchronize update/access on current Best Quote
		pthread_mutex_lock(&updateAskBidMutex[stuck->symbolIndex]);

		GetBestOrder(&bestQuote[ASK_SIDE], ASK_SIDE, stuck->symbolIndex);
		GetBestOrder(&bestQuote[BID_SIDE], BID_SIDE, stuck->symbolIndex);
		
		if(forceEngage == 0)
		{
			haltedECN_ASK = CheckHaltedECNForSymbol(tradeSymbolStatusIndex, bestQuote[ASK_SIDE].ecnIndicator);
			haltedECN_BID = CheckHaltedECNForSymbol(tradeSymbolStatusIndex, bestQuote[BID_SIDE].ecnIndicator);
		}

		//khanhnd: Save best quote that get was -> initialize hold quote
		positionCollection[stuck->symbolIndex].holdQuote[ASK_SIDE].isHold = NO;
		positionCollection[stuck->symbolIndex].holdQuote[BID_SIDE].isHold = NO;

		// Release mutex lock
		pthread_mutex_unlock(&updateAskBidMutex[stuck->symbolIndex]);
	}
	
	// Trying update MinAsk and get best order
	if (bestQuote[ASK_SIDE].info.sharePrice < 0.01)
	{
		if (stuck->shares > MAX_ODD_LOT_SHARE)
		{
			// Update MinAsk
			UpdateMinAskRoundLot(stuck->symbolIndex, 0, YES, haltedECN_ASK);

			// Get mutex lock to synchronize update/access on current Best Quote
			pthread_mutex_lock(&updateAskBidMutex[stuck->symbolIndex]);

			GetBestOrderRoundLot(&bestQuote[ASK_SIDE], ASK_SIDE, stuck->symbolIndex);

			//khanhnd: Save best quote that get was -> initialize hold quote
			positionCollection[stuck->symbolIndex].holdQuote[ASK_SIDE].isHold = NO;

			// Release mutex lock
			pthread_mutex_unlock(&updateAskBidMutex[stuck->symbolIndex]);
		}
		else
		{
			// Update MinAsk
			UpdateMinAsk(stuck->symbolIndex, 0, haltedECN_ASK);

			// Get mutex lock to synchronize update/access on current Best Quote
			pthread_mutex_lock(&updateAskBidMutex[stuck->symbolIndex]);

			GetBestOrder(&bestQuote[ASK_SIDE], ASK_SIDE, stuck->symbolIndex);

			//khanhnd: Save best quote that get was -> initialize hold quote
			positionCollection[stuck->symbolIndex].holdQuote[ASK_SIDE].isHold = NO;

			// Release mutex lock
			pthread_mutex_unlock(&updateAskBidMutex[stuck->symbolIndex]);
		}
	}
	
	/****************************************************************************************************
	Get best BBO quotes
	+ Bid Quote
	+ Ask Quote
	****************************************************************************************************/
	t_BestBBO bestBBO[MAX_SIDE];
	
	bestBBO[ASK_SIDE].price = 0.0;
	bestBBO[BID_SIDE].price = 0.0;

	GetBestBBOOrder(&bestBBO[ASK_SIDE], ASK_SIDE, stuck->symbolIndex);
	GetBestBBOOrder(&bestBBO[BID_SIDE], BID_SIDE, stuck->symbolIndex);
	
	// Check to get best ASK
	if (bestBBO[ASK_SIDE].participantId != -1)
	{
		if(haltedECN_ASK != ParticipantToExchangeIndexMapping[bestBBO[ASK_SIDE].participantId - 65])
		{
			if (fgt(bestQuote[ASK_SIDE].info.sharePrice, 0.01))
			{
				if (fgt(bestQuote[ASK_SIDE].info.sharePrice, bestBBO[ASK_SIDE].price))
				{
					bestQuote[ASK_SIDE].info.sharePrice = bestBBO[ASK_SIDE].price;
					bestQuote[ASK_SIDE].info.shareVolume = bestBBO[ASK_SIDE].size;
					bestQuote[ASK_SIDE].ecnIndicator = bestBBO[ASK_SIDE].participantId;
				}
			}
			else
			{
				bestQuote[ASK_SIDE].info.sharePrice = bestBBO[ASK_SIDE].price;
				bestQuote[ASK_SIDE].info.shareVolume = bestBBO[ASK_SIDE].size;
				bestQuote[ASK_SIDE].ecnIndicator = bestBBO[ASK_SIDE].participantId;
			}
		}
	}
	
	if ((bestQuote[ASK_SIDE].ecnIndicator == -1) || flt(bestQuote[ASK_SIDE].info.sharePrice, 0.01))
	{
		if(replaceAfterSleeping == YES)
		{
			t_SendNewOrder newOrder;
			GetNewOrder(&newOrder, stuck); 
			ReplaceOrderAfterSleeping(&newOrder, stuck);
		}
		else if(needSleep == YES)
		{
			NumOfOrderHasInsideGoAwayOrSpreadTooWide++;
			if(indexOpenOrder != -1)
			{
				asOpenOrder.orderSummary[indexOpenOrder].status = orderStatusIndex.Sleeping;
			}
			t_PositionInfo *positionItem = &positionCollection[stuck->symbolIndex];
			positionItem->replaceAfterSleeping = YES;
			memcpy(&positionItem->stuck, stuck, sizeof(t_Position));
		}
		else
		{
			TraceLog(DEBUG_LEVEL, "GetBestOrder: Best ASK empty!\n");
			SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_BEST_ASK_NOTHING);
		}
		return ERROR;
	}
	
	if (bestQuote[ASK_SIDE].ecnIndicator < 10)
		TraceLog(DEBUG_LEVEL, "Ask quote: ECN(%d) %d %.2lf\n", bestQuote[ASK_SIDE].ecnIndicator, bestQuote[ASK_SIDE].info.shareVolume, bestQuote[ASK_SIDE].info.sharePrice);
	else
		TraceLog(DEBUG_LEVEL, "Ask quote: Exchange(%c) %d %.2lf\n", bestQuote[ASK_SIDE].ecnIndicator, bestQuote[ASK_SIDE].info.shareVolume, bestQuote[ASK_SIDE].info.sharePrice);

	// Trying update MaxBid and get best order
	if (flt(bestQuote[BID_SIDE].info.sharePrice, 0.01))
	{
		if (stuck->shares > MAX_ODD_LOT_SHARE)
		{
			// Update MaxBid
			UpdateMaxBidRoundLot(stuck->symbolIndex, 0, YES, haltedECN_BID);

			// Get mutex lock to synchronize update/access on current Best Quote
			pthread_mutex_lock(&updateAskBidMutex[stuck->symbolIndex]);

			GetBestOrderRoundLot(&bestQuote[BID_SIDE], BID_SIDE, stuck->symbolIndex);

			//khanhnd: Save best quote that get was -> initialize hold quote
			positionCollection[stuck->symbolIndex].holdQuote[BID_SIDE].isHold = NO;

			// Release mutex lock
			pthread_mutex_unlock(&updateAskBidMutex[stuck->symbolIndex]);
		}
		else
		{
			// Update MaxBid
			UpdateMaxBid(stuck->symbolIndex, 0, haltedECN_BID);

			// Get mutex lock to synchronize update/access on current Best Quote
			pthread_mutex_lock(&updateAskBidMutex[stuck->symbolIndex]);

			GetBestOrder(&bestQuote[BID_SIDE], BID_SIDE, stuck->symbolIndex);

			//khanhnd: Save best quote that get was -> initialize hold quote
			positionCollection[stuck->symbolIndex].holdQuote[BID_SIDE].isHold = NO;

			// Release mutex lock
			pthread_mutex_unlock(&updateAskBidMutex[stuck->symbolIndex]);
		}
	}
	
	// Check to get best BID
	if (flt(bestQuote[BID_SIDE].info.sharePrice, bestBBO[BID_SIDE].price))
	{
		if(haltedECN_BID != ParticipantToExchangeIndexMapping[bestBBO[BID_SIDE].participantId - 65])
		{
			bestQuote[BID_SIDE].info.sharePrice = bestBBO[BID_SIDE].price;
			bestQuote[BID_SIDE].info.shareVolume = bestBBO[BID_SIDE].size;
			bestQuote[BID_SIDE].ecnIndicator = bestBBO[BID_SIDE].participantId;
		}
	}
	
	if ((bestQuote[BID_SIDE].ecnIndicator == -1) || flt(bestQuote[BID_SIDE].info.sharePrice, 0.01))
	{
		if(replaceAfterSleeping == YES)
		{
			t_SendNewOrder newOrder;
			GetNewOrder(&newOrder, stuck); 
			ReplaceOrderAfterSleeping(&newOrder, stuck);
		}
		else if(needSleep == YES)
		{
			NumOfOrderHasInsideGoAwayOrSpreadTooWide++;
			if(indexOpenOrder != -1)
			{
				asOpenOrder.orderSummary[indexOpenOrder].status = orderStatusIndex.Sleeping;
			}
			t_PositionInfo *positionItem = &positionCollection[stuck->symbolIndex];
			positionItem->replaceAfterSleeping = YES;
			memcpy(&positionItem->stuck, stuck, sizeof(t_Position));
		}
		else
		{
			TraceLog(DEBUG_LEVEL, "GetBestOrder: Best BID empty!\n");
			SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_BEST_BID_NOTHING);
		}
		return ERROR;
	}
	
	if (bestQuote[BID_SIDE].ecnIndicator < 10)
		TraceLog(DEBUG_LEVEL, "Bid quote: ECN(%d) %d %.2lf\n", bestQuote[BID_SIDE].ecnIndicator, bestQuote[BID_SIDE].info.shareVolume, bestQuote[BID_SIDE].info.sharePrice);
	else
		TraceLog(DEBUG_LEVEL, "Bid quote: Exchange(%c) %d %.2lf\n", bestQuote[BID_SIDE].ecnIndicator, bestQuote[BID_SIDE].info.shareVolume, bestQuote[BID_SIDE].info.sharePrice);

	if (flt(stuck->price, 0.01))
	{
		stuck->price = (bestQuote[ASK_SIDE].info.sharePrice + bestQuote[BID_SIDE].info.sharePrice) / 2;
		stuck->price = (double)(floor(stuck->price * 100)) / 100;

		TraceLog(DEBUG_LEVEL, "Treat the stuck price = %.2lf\n", stuck->price);
	}

	double insideSpread = bestQuote[ASK_SIDE].info.sharePrice - bestQuote[BID_SIDE].info.sharePrice;

	TraceLog(DEBUG_LEVEL, "Inside spread = %.2lf\n", insideSpread);

	//We should not place an order if the stock is crossed
	if (fle(insideSpread, 0.0))
	{
		if (bestQuote[BID_SIDE].ecnIndicator == bestQuote[ASK_SIDE].ecnIndicator)
		{
			if (bestQuote[BID_SIDE].ecnIndicator < 10)
				TraceLog(DEBUG_LEVEL, "BID >= ASK on the same ECN(%d)\n", bestQuote[BID_SIDE].ecnIndicator);
			else
				TraceLog(DEBUG_LEVEL, "BID >= ASK on the same Exchange(%c)\n", bestQuote[BID_SIDE].ecnIndicator);

			if(replaceAfterSleeping == YES)
			{
				t_SendNewOrder newOrder;
				GetNewOrder(&newOrder, stuck); 
				ReplaceOrderAfterSleeping(&newOrder, stuck);
			}
			else if(needSleep == YES)
			{
				NumOfOrderHasInsideGoAwayOrSpreadTooWide++;
				if(indexOpenOrder != -1)
				{
					asOpenOrder.orderSummary[indexOpenOrder].status = orderStatusIndex.Sleeping;
				}
				t_PositionInfo *positionItem = &positionCollection[stuck->symbolIndex];
				positionItem->replaceAfterSleeping = YES;
				memcpy(&positionItem->stuck, stuck, sizeof(t_Position));
			}
			else
			{
				SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_BID_GT_ASK);
			}
			return ERROR;
		}
	}
	
	/*
	Aligning Inside: The best bid/best ask on the same side of the book as our position (Long =
	Ask / Short = Bid).
	*/
	int aligningSide = !stuck->side;

	/*
	Opposing Inside: The best bid/best ask on the opposing side of the book as our position
	(Long = Bid / Short = Ask).
	*/
	double opposingSpread;

	if (stuck->side == ASK_SIDE)
	{
		opposingSpread = stuck->price - bestQuote[ASK_SIDE].info.sharePrice;
	}
	else
	{
		opposingSpread = bestQuote[BID_SIDE].info.sharePrice - stuck->price;
	}

	TraceLog(DEBUG_LEVEL, "Opposing spread = %.2lf\n", opposingSpread);

	// Calculation tight spread, as defined by [<autospread> * <stuck price>, not to exceed <maxspread>]	
	double tightSpread = asCollection.globalConf.autoSpread * stuck->price;

	if (flt(tightSpread, 0.01))
	{
		tightSpread = 0.01;
	}

	TraceLog(DEBUG_LEVEL, "Tight spread = %lf\n", tightSpread);

	// Build order
	t_SendNewOrder newOrder;
	strncpy(newOrder.symbol, stuck->symbol, SYMBOL_LEN);
	newOrder.accountSuffix = stuck->accountSuffix;
	newOrder.symbolIndex = stuck->symbolIndex;
	newOrder.shares = stuck->shares;
	newOrder.maxFloor = 0;
	newOrder.tif = DAY_TYPE;
	newOrder.pegDef = 0;

	if (stuck->shares >= 100)
	{
		newOrder.maxFloor = 100;
	}

	if (stuck->side == ASK_SIDE)
	{
		newOrder.side = BUY_TO_CLOSE_TYPE;
	}
	else
	{
		newOrder.side = SELL_TYPE;
	}

	if (fle(insideSpread, tightSpread))
	{
		TraceLog(DEBUG_LEVEL, "The inside spread (%.2lf) <= tight spread (%lf).\n", insideSpread, tightSpread);

		if (fgt(insideSpread, asCollection.globalConf.fm_maxSpread))
		{
			if(replaceAfterSleeping == YES)
			{
				t_SendNewOrder newOrder;
				GetNewOrder(&newOrder, stuck); 
				ReplaceOrderAfterSleeping(&newOrder, stuck);
			}
			else if(needSleep == YES)
			{
				NumOfOrderHasInsideGoAwayOrSpreadTooWide++;
				if(indexOpenOrder != -1)
				{
					asOpenOrder.orderSummary[indexOpenOrder].status = orderStatusIndex.Sleeping;
				}
				t_PositionInfo *positionItem = &positionCollection[stuck->symbolIndex];
				positionItem->replaceAfterSleeping = YES;
				memcpy(&positionItem->stuck, stuck, sizeof(t_Position));
			}
			else
			{
				TraceLog(DEBUG_LEVEL, "Fast market situation: The inside spread (%.2lf) > max spread (%lf) so disengage the symbol.\n", insideSpread, asCollection.globalConf.maxSpread);
				SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_INSIDE_SPREAD_GT_MAX_SPREAD_FM);
			}
			return ERROR;
		}
		else
		{
			// Send a liquidate order or disengage
			newOrder.price = bestQuote[(int)stuck->side].info.sharePrice;

			if (bestQuote[(int)stuck->side].ecnIndicator == BOOK_NASDAQ)
			{
				if (orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == CONNECTED)
				{
					newOrder.ECNId = TYPE_NASDAQ_RASH;
					newOrder.maxFloor = 0;
				}
				else if (orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == CONNECTED)
				{
					newOrder.ECNId = TYPE_ARCA_DIRECT;
				}
				else
				{
					SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN);
					return ERROR;
				}
			}
			else if (bestQuote[(int)stuck->side].ecnIndicator == BOOK_ARCA)
			{
				if (orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == CONNECTED)
				{
					newOrder.ECNId = TYPE_ARCA_DIRECT;
				}
				else if (orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == CONNECTED)
				{
					newOrder.ECNId = TYPE_NASDAQ_RASH;
					newOrder.maxFloor = 0;
				}
				else
				{
					SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN);
					return ERROR;
				}
			}
			else	// Away market, need to check the config to see where to place order! new Order Price = BBO quote price!
			{
				// All orders sent to capture liquidity at EXCHANGES we DON'T DIRECTLY CONNECT TO will use the
				// RESERVE METHOD WITH 100 SHARES VISIBLE.
				// ---> NewOrder.MaxFloor = 100
				// If stuck share < 100 ---> NewOrder.MaxFloor = 0
				newOrder.ECNId = ExchangeStatusList[(int)ParticipantToExchangeIndexMapping[bestQuote[(int)stuck->side].ecnIndicator - 65]].ECNToPlace;
				
				if(newOrder.ECNId == TYPE_ARCA_DIRECT && StockSymbolIndex_to_ArcaSymbolIndex_Mapping[stuck->symbolIndex] == -1)
				{
					newOrder.ECNId = TYPE_NASDAQ_RASH;
				}
					
				if (newOrder.ECNId == TYPE_ARCA_DIRECT && orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == DISCONNECTED)
				{
					if (orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == DISCONNECTED)
					{
						SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN);
						return ERROR;
					}
					else
					{
						newOrder.ECNId = TYPE_NASDAQ_RASH;
					}
				}
				else if (newOrder.ECNId == TYPE_NASDAQ_RASH && orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == DISCONNECTED)
				{
					if (orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == DISCONNECTED || StockSymbolIndex_to_ArcaSymbolIndex_Mapping[stuck->symbolIndex] == -1)
					{
						SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN);
						return ERROR;
					}
					else
					{
						newOrder.ECNId = TYPE_ARCA_DIRECT;
					}
				}
			}

			return SendLiquidateOrder(&newOrder, stuck, 0, 0, 1, needSleep, replaceAfterSleeping, indexOpenOrder, forceEngage);
		}
	}
	else if (fge(opposingSpread, 0.0))
	{
		TraceLog(DEBUG_LEVEL, "The opposing inside price indicates that the stuck is currently a profit: opposingSpread = %.2lf\n", opposingSpread);

		// Calculation tight profit spread, as defined by [<profitspread> * <stuck price>, not to exceed <maxprofitspread>]
		double tightProfitSpread = asCollection.globalConf.profitSpread * stuck->price;

		TraceLog(DEBUG_LEVEL, "Tight profit spread = %lf\n", tightProfitSpread);

		if (fle(insideSpread, tightProfitSpread))
		{
			TraceLog(DEBUG_LEVEL, "The inside spread (%.2lf) <= tight profit spread (%lf)\n", insideSpread, tightProfitSpread);

			if (fgt(insideSpread, asCollection.globalConf.maxProfitSpread))
			{
				if(replaceAfterSleeping == YES)
				{
					t_SendNewOrder newOrder;
					GetNewOrder(&newOrder, stuck); 
					ReplaceOrderAfterSleeping(&newOrder, stuck);
				}
				else if(needSleep == YES)
				{
					NumOfOrderHasInsideGoAwayOrSpreadTooWide++;
					if(indexOpenOrder != -1)
					{
						asOpenOrder.orderSummary[indexOpenOrder].status = orderStatusIndex.Sleeping;
					}
					t_PositionInfo *positionItem = &positionCollection[stuck->symbolIndex];
					positionItem->replaceAfterSleeping = YES;
					memcpy(&positionItem->stuck, stuck, sizeof(t_Position));
				}
				else
				{
					TraceLog(DEBUG_LEVEL, "The inside spread (%.2lf) > max profit spread (%lf). Disengage the symbol.\n", insideSpread, asCollection.globalConf.maxProfitSpread);
					SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_INSIDE_SPREAD_GT_MAX_PROFIT_SPREAD);
				}
				return ERROR;
			}
			else
			{
				// Send a liquidate order / or disengage
				newOrder.price = bestQuote[(int)stuck->side].info.sharePrice;

				if (bestQuote[(int)stuck->side].ecnIndicator == BOOK_NASDAQ)
				{
					if (orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == CONNECTED)
					{
						newOrder.ECNId = TYPE_NASDAQ_RASH;
						newOrder.maxFloor = 0;
					}
					else if (orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == CONNECTED)
					{
						newOrder.ECNId = TYPE_ARCA_DIRECT;
					}
					else
					{
						SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN);
						return ERROR;
					}
				}
				else if (bestQuote[(int)stuck->side].ecnIndicator == BOOK_ARCA)
				{
					if (orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == CONNECTED)
					{
						newOrder.ECNId = TYPE_ARCA_DIRECT;
					}
					else if (orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == CONNECTED)
					{
						newOrder.ECNId = TYPE_NASDAQ_RASH;
						newOrder.maxFloor = 0;
					}
					else
					{
						SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN);
						return ERROR;
					}
				}
				else	// Away market, need to check the config to see where to place order! new Order Price = BBO quote price!
				{
					// All orders sent to capture liquidity at EXCHANGES we DON'T DIRECTLY CONNECT TO will use the
					// RESERVE METHOD WITH 100 SHARES VISIBLE.
					// ---> NewOrder.MaxFloor = 100
					newOrder.ECNId = ExchangeStatusList[(int)ParticipantToExchangeIndexMapping[bestQuote[(int)stuck->side].ecnIndicator - 65]].ECNToPlace;
					
					if(newOrder.ECNId == TYPE_ARCA_DIRECT && StockSymbolIndex_to_ArcaSymbolIndex_Mapping[stuck->symbolIndex] == -1)
					{
						newOrder.ECNId = TYPE_NASDAQ_RASH;
					}
					
					if (newOrder.ECNId == TYPE_ARCA_DIRECT && orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == DISCONNECTED)
					{	
						if (orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == DISCONNECTED)
						{
							SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN);
							return ERROR;
						}
						else
						{
							newOrder.ECNId = TYPE_NASDAQ_RASH;
						}
					}
					else if (newOrder.ECNId == TYPE_NASDAQ_RASH && orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == DISCONNECTED)
					{
						if (orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == DISCONNECTED || StockSymbolIndex_to_ArcaSymbolIndex_Mapping[stuck->symbolIndex] == -1)
						{
							SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN);
							return ERROR;
						}
						else
						{
							newOrder.ECNId = TYPE_ARCA_DIRECT;
						}
					}
				}

				return SendLiquidateOrder(&newOrder, stuck, 0, 0, 1, needSleep, replaceAfterSleeping, indexOpenOrder, forceEngage);
			}
		}
		else
		{
			TraceLog(DEBUG_LEVEL, "The inside spread (%.2lf) > tight profit spread (%lf)\n", insideSpread, tightProfitSpread);

			// Send a BBO order / or disengage
			if (bestQuote[aligningSide].ecnIndicator == BOOK_NASDAQ)
			{
				if (orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == CONNECTED)
				{
					newOrder.ECNId = TYPE_NASDAQ_RASH;
					newOrder.maxFloor = 0;
				}
				else if (orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == CONNECTED)
				{
					newOrder.ECNId = TYPE_ARCA_DIRECT;
				}
				else
				{
					SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN);
					return ERROR;
				}
			}
			else if (bestQuote[aligningSide].ecnIndicator == BOOK_ARCA)
			{
				if (orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == CONNECTED)
				{
					newOrder.ECNId = TYPE_ARCA_DIRECT;
				}
				else if (orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == CONNECTED)
				{
					newOrder.ECNId = TYPE_NASDAQ_RASH;
					newOrder.maxFloor = 0;
				}
				else
				{
					SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN);
					return ERROR;
				}
			}
			else	// Away market, need to check the config to see where to place order! new Order Price = BBO quote price!
			{
				// All orders sent to capture liquidity at EXCHANGES we DON'T DIRECTLY CONNECT TO will use the
				// RESERVE METHOD WITH 100 SHARES VISIBLE.
				// ---> NewOrder.MaxFloor = 100
				newOrder.ECNId = ExchangeStatusList[(int)ParticipantToExchangeIndexMapping[bestQuote[aligningSide].ecnIndicator - 65]].ECNToPlace;
				
				if(newOrder.ECNId == TYPE_ARCA_DIRECT && StockSymbolIndex_to_ArcaSymbolIndex_Mapping[stuck->symbolIndex] == -1)
				{
					newOrder.ECNId = TYPE_NASDAQ_RASH;
				}
				
				if (newOrder.ECNId == TYPE_ARCA_DIRECT && orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == DISCONNECTED)
				{
					if (orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == DISCONNECTED)
					{
						SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN);
						return ERROR;
					}
					else
					{
						newOrder.ECNId = TYPE_NASDAQ_RASH;
					}
				}
				else if (newOrder.ECNId == TYPE_NASDAQ_RASH && orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == DISCONNECTED)
				{
					if (orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == DISCONNECTED || StockSymbolIndex_to_ArcaSymbolIndex_Mapping[stuck->symbolIndex] == -1)
					{
						SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN);
						return ERROR;
					}
					else
					{
						newOrder.ECNId = TYPE_ARCA_DIRECT;
					}
				}
			}

			if (aligningSide == ASK_SIDE)
			{
				bestQuote[aligningSide].info.sharePrice -= 0.01;
			}
			else
			{
				bestQuote[aligningSide].info.sharePrice += 0.01;
			}

			newOrder.price = bestQuote[aligningSide].info.sharePrice;

			return SendBBOOrder(&newOrder, stuck, bestQuote, 0, 0, 1, needSleep, replaceAfterSleeping, indexOpenOrder, forceEngage);
		}
	}
	else if (flt((opposingSpread + asCollection.globalConf.bigLoserAmount / stuck->shares), 0.0))
	{
		TraceLog(DEBUG_LEVEL, "The opposing inside price indicates that the stuck is currently a big loser: opposingSpread = %.2lf, bigLoserAmount = %lf\n", opposingSpread, asCollection.globalConf.bigLoserAmount);

		// Calculation tight big loser spread, as defined by [<bigloserspread> * <stuck price>, not to exceed <maxbigloserspread>]
		double tightBigLosertSpread = asCollection.globalConf.bigLoserSpread * stuck->price;

		TraceLog(DEBUG_LEVEL, "Tight big loser spread = %lf\n", tightBigLosertSpread);

		if (fle(insideSpread, tightBigLosertSpread))
		{
			TraceLog(DEBUG_LEVEL, "The inside spread (%.2lf) <= tight big loser spread (%lf)\n", insideSpread, tightBigLosertSpread);

			if (fgt(insideSpread, asCollection.globalConf.fm_maxBigLoserSpread))
			{
				if(replaceAfterSleeping == YES)
				{
					t_SendNewOrder newOrder;
					GetNewOrder(&newOrder, stuck); 
					ReplaceOrderAfterSleeping(&newOrder, stuck);
				}
				else if(needSleep == YES)
				{
					NumOfOrderHasInsideGoAwayOrSpreadTooWide++;
					if(indexOpenOrder != -1)
					{
						asOpenOrder.orderSummary[indexOpenOrder].status = orderStatusIndex.Sleeping;
					}
					t_PositionInfo *positionItem = &positionCollection[stuck->symbolIndex];
					positionItem->replaceAfterSleeping = YES;
					memcpy(&positionItem->stuck, stuck, sizeof(t_Position));
				}
				else
				{
					TraceLog(DEBUG_LEVEL, "Fast market situation: The inside spread (%.2lf) > max big loser spread (%lf) so disengage.\n", insideSpread, asCollection.globalConf.maxBigLoserSpread);
					SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_INSIDE_SPREAD_GT_MAX_BIG_LOSER_SPREAD_FM);
				}
				return ERROR;
			}
			else
			{
				// Send a liquidate order or disengage
				newOrder.price = bestQuote[(int)stuck->side].info.sharePrice;

				if (bestQuote[(int)stuck->side].ecnIndicator == BOOK_NASDAQ)
				{
					if (orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == CONNECTED)
					{
						newOrder.ECNId = TYPE_NASDAQ_RASH;
						newOrder.maxFloor = 0;
					}
					else if (orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == CONNECTED)
					{
						newOrder.ECNId = TYPE_ARCA_DIRECT;
					}
					else
					{
						SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN);
						return ERROR;
					}
				}
				else if (bestQuote[(int)stuck->side].ecnIndicator == BOOK_ARCA)
				{
					if (orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == CONNECTED)
					{
						newOrder.ECNId = TYPE_ARCA_DIRECT;
					}
					else if (orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == CONNECTED)
					{
						newOrder.ECNId = TYPE_NASDAQ_RASH;
						newOrder.maxFloor = 0;
					}
					else
					{
						SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN);
						return ERROR;
					}
				}
				else	// Away market, need to check the config to see where to place order! new Order Price = BBO quote price!
				{
					// All orders sent to capture liquidity at EXCHANGES we DON'T DIRECTLY CONNECT TO will use the
					// RESERVE METHOD WITH 100 SHARES VISIBLE.
					// ---> NewOrder.MaxFloor = 100

					newOrder.ECNId = ExchangeStatusList[(int)ParticipantToExchangeIndexMapping[bestQuote[(int)stuck->side].ecnIndicator - 65]].ECNToPlace;
					
					if(newOrder.ECNId == TYPE_ARCA_DIRECT && StockSymbolIndex_to_ArcaSymbolIndex_Mapping[stuck->symbolIndex] == -1)
					{
						newOrder.ECNId = TYPE_NASDAQ_RASH;
					}
					
					if (newOrder.ECNId == TYPE_ARCA_DIRECT && orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == DISCONNECTED)
					{
						if (orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == DISCONNECTED)
						{
							SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN);
							return ERROR;
						}
						else
						{
							newOrder.ECNId = TYPE_NASDAQ_RASH;
						}
					}
					else if (newOrder.ECNId == TYPE_NASDAQ_RASH && orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == DISCONNECTED)
					{
						if (orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == DISCONNECTED || StockSymbolIndex_to_ArcaSymbolIndex_Mapping[stuck->symbolIndex] == -1)
						{
							SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN);
							return ERROR;
						}
						else
						{
							newOrder.ECNId = TYPE_ARCA_DIRECT;
						}
					}
				}

				return SendLiquidateOrder(&newOrder, stuck, 0 , 0, 1, needSleep, replaceAfterSleeping, indexOpenOrder, forceEngage);
			}
		}
		else
		{
			TraceLog(DEBUG_LEVEL, "The inside spread (%.2lf) > tight big loser spread (%lf)\n", insideSpread, tightBigLosertSpread);

			// Send a BBO order / or disengage
			if (bestQuote[aligningSide].ecnIndicator == BOOK_NASDAQ)
			{
				if (orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == CONNECTED)
				{
					newOrder.ECNId = TYPE_NASDAQ_RASH;
					newOrder.maxFloor = 0;
				}
				else if (orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == CONNECTED)
				{
					newOrder.ECNId = TYPE_ARCA_DIRECT;
				}
				else
				{
					SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN);
					return ERROR;
				}
			}
			else if (bestQuote[aligningSide].ecnIndicator == BOOK_ARCA)
			{
				if (orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == CONNECTED)
				{
					newOrder.ECNId = TYPE_ARCA_DIRECT;
				}
				else if (orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == CONNECTED)
				{
					newOrder.ECNId = TYPE_NASDAQ_RASH;
					newOrder.maxFloor = 0;
				}
				else
				{
					SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN);
					return ERROR;
				}
			}
			else	// Away market, need to check the config to see where to place order! new Order Price = BBO quote price!
			{
				// All orders sent to capture liquidity at EXCHANGES we DON'T DIRECTLY CONNECT TO will use the
				// RESERVE METHOD WITH 100 SHARES VISIBLE.
				// ---> NewOrder.MaxFloor = 100

				newOrder.ECNId = ExchangeStatusList[(int)ParticipantToExchangeIndexMapping[bestQuote[aligningSide].ecnIndicator - 65]].ECNToPlace;
				
				if(newOrder.ECNId == TYPE_ARCA_DIRECT && StockSymbolIndex_to_ArcaSymbolIndex_Mapping[stuck->symbolIndex] == -1)
				{
					newOrder.ECNId = TYPE_NASDAQ_RASH;
				}
				
				if (newOrder.ECNId == TYPE_ARCA_DIRECT && orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == DISCONNECTED)
				{
					if (orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == DISCONNECTED)
					{
						SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN);
						return ERROR;
					}
					else
					{
						newOrder.ECNId = TYPE_NASDAQ_RASH;
					}
				}
				else if (newOrder.ECNId == TYPE_NASDAQ_RASH && orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == DISCONNECTED)
				{
					if (orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == DISCONNECTED || StockSymbolIndex_to_ArcaSymbolIndex_Mapping[stuck->symbolIndex] == -1)
					{
						SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN);
						return ERROR;
					}
					else
					{
						newOrder.ECNId = TYPE_ARCA_DIRECT;
					}
				}
			}

			if (aligningSide == ASK_SIDE)
			{
				bestQuote[aligningSide].info.sharePrice -= 0.01;
			}
			else
			{
				bestQuote[aligningSide].info.sharePrice += 0.01;
			}

			newOrder.price = bestQuote[aligningSide].info.sharePrice;

			return SendBBOOrder(&newOrder, stuck, bestQuote, 0, 0, 1, needSleep, replaceAfterSleeping, indexOpenOrder, forceEngage);
		}
	}
	else
	{
		TraceLog(DEBUG_LEVEL, "The inside spread (%.2lf) > tight spread (%lf). But the opposing inside price indicates that the stuck isn't a profit or a big loser: opposingSpread = %.2lf, bigLoserAmount = %lf\n", 
			insideSpread, tightSpread, opposingSpread, asCollection.globalConf.bigLoserAmount);

		// Send a BBO order
		if (bestQuote[aligningSide].ecnIndicator == BOOK_NASDAQ)
		{
			if (orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == CONNECTED)
			{
				newOrder.ECNId = TYPE_NASDAQ_RASH;
				newOrder.maxFloor = 0;
			}
			else if (orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == CONNECTED)
			{
				newOrder.ECNId = TYPE_ARCA_DIRECT;
			}
			else
			{
				SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN);
				return ERROR;
			}
		}
		else if (bestQuote[aligningSide].ecnIndicator == BOOK_ARCA)
		{
			if (orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == CONNECTED)
			{
				newOrder.ECNId = TYPE_ARCA_DIRECT;
			}
			else if (orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == CONNECTED)
			{
				newOrder.ECNId = TYPE_NASDAQ_RASH;
				newOrder.maxFloor = 0;
			}
			else
			{
				SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN);
				return ERROR;
			}
		}
		else	// Away market, need to check the config to see where to place order! new Order Price = BBO quote price!
		{
			// All orders sent to capture liquidity at EXCHANGES we DON'T DIRECTLY CONNECT TO will use the
			// RESERVE METHOD WITH 100 SHARES VISIBLE.
			// ---> NewOrder.MaxFloor = 100
			newOrder.ECNId = ExchangeStatusList[(int)ParticipantToExchangeIndexMapping[bestQuote[aligningSide].ecnIndicator - 65]].ECNToPlace;
			
			if(newOrder.ECNId == TYPE_ARCA_DIRECT && StockSymbolIndex_to_ArcaSymbolIndex_Mapping[stuck->symbolIndex] == -1)
			{
				newOrder.ECNId = TYPE_NASDAQ_RASH;
			}
			
			if (newOrder.ECNId == TYPE_ARCA_DIRECT && orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == DISCONNECTED)
			{
				if (orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == DISCONNECTED)
				{
					SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN);
					return ERROR;
				}
				else
				{
					newOrder.ECNId = TYPE_NASDAQ_RASH;
				}
			}
			else if (newOrder.ECNId == TYPE_NASDAQ_RASH && orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == DISCONNECTED)
			{
				if (orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == DISCONNECTED || StockSymbolIndex_to_ArcaSymbolIndex_Mapping[stuck->symbolIndex] == -1)
				{
					SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN);
					return ERROR;
				}
				else
				{
					newOrder.ECNId = TYPE_ARCA_DIRECT;
				}
			}
		}
		if (aligningSide == ASK_SIDE)
		{
			bestQuote[aligningSide].info.sharePrice -= 0.01;
		}
		else
		{
			bestQuote[aligningSide].info.sharePrice += 0.01;
		}

		newOrder.price = bestQuote[aligningSide].info.sharePrice;

		return SendBBOOrder(&newOrder, stuck, bestQuote, 0, 0, 1, needSleep, replaceAfterSleeping, indexOpenOrder, forceEngage);
	}
	
	return ERROR;
}

/****************************************************************************
- Function name:	SendLiquidateOrder
- Input:			
- Output:		
- Return:			
- Description:	
- Usage:			
****************************************************************************/
int SendLiquidateOrder(t_SendNewOrder *newOrder, t_Position *stuck, int isNeedCheckExecutionRate, int executionRate, int useFastMarketParameters, int needSleep, int replaceAfterSleeping, int indexOpenOrder, int forceEngage)
{
	// Update order type, LIQUIDATE_ORDER or BBO_ORDER
	newOrder->type = LIQUIDATE_ORDER;
	
	if (useFastMarketParameters == 0)
	{
		//Check price to send order with MAX LOSER SPREAD
		if (newOrder->side == BUY_TO_CLOSE_TYPE)
		{
			if(fgt(newOrder->price, (stuck->price + asCollection.globalConf.maxLoserSpread)))
			{
				if (isNeedCheckExecutionRate == 0)
				{
					TraceLog(DEBUG_LEVEL, "Bad price to exit SHORT: insidePrice = %.2lf, stuckPrice = %.2lf, maxLoserSpread = %lf. Disengage.\n", newOrder->price, stuck->price, asCollection.globalConf.maxLoserSpread);
					SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_BAD_PRICE_EXIT_SHORT_LIQUIDATE);
					return ERROR;
				}
				else
				{
					if (executionRate > asCollection.globalConf.highExecutionRate)
					{
						TraceLog(DEBUG_LEVEL, "Bad price to exit SHORT: insidePrice = %.2lf, stuckPrice = %.2lf, maxLoserSpread = %lf. But execution rate (%d) is high so moving to Fast Market situation.\n", newOrder->price, stuck->price, asCollection.globalConf.maxLoserSpread, executionRate);
						return CheckAndPlaceOrderForFastMarket(stuck, needSleep, replaceAfterSleeping, indexOpenOrder, forceEngage);
					}
					else
					{
						TraceLog(DEBUG_LEVEL, "Bad price to exit SHORT: insidePrice = %.2lf, stuckPrice = %.2lf, maxLoserSpread = %lf. And execution rate (%d) is low so disengage.\n", newOrder->price, stuck->price, asCollection.globalConf.maxLoserSpread, executionRate);
						SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_BAD_PRICE_EXIT_SHORT_LIQUIDATE);
						return ERROR;
					}
				}
			}
		}
		else
		{
			if (flt(newOrder->price, (stuck->price - asCollection.globalConf.maxLoserSpread)))
			{
				if (isNeedCheckExecutionRate == 0)
				{
					TraceLog(DEBUG_LEVEL, "Bad price to exit LONG: insidePrice = %.2lf, stuckPrice = %.2lf, maxLoserSpread = %lf. Disengage.\n", newOrder->price, stuck->price, asCollection.globalConf.maxLoserSpread);
					SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_BAD_PRICE_EXIT_LONG_LIQUIDATE);
					return ERROR;
				}
				else
				{
					if (executionRate > asCollection.globalConf.highExecutionRate)
					{
						TraceLog(DEBUG_LEVEL, "Bad price to exit LONG: insidePrice = %.2lf, stuckPrice = %.2lf, maxLoserSpread = %lf. But execution rate (%d) is high so moving to Fast Market situation.\n", newOrder->price, stuck->price, asCollection.globalConf.maxLoserSpread, executionRate);
						return CheckAndPlaceOrderForFastMarket(stuck, needSleep, replaceAfterSleeping, indexOpenOrder, forceEngage);
					}
					else
					{
						TraceLog(DEBUG_LEVEL, "Bad price to exit LONG: insidePrice = %.2lf, stuckPrice = %.2lf, maxLoserSpread = %lf. And execution rate (%d) is low so disengage.\n", newOrder->price, stuck->price, asCollection.globalConf.maxLoserSpread, executionRate);
						SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_BAD_PRICE_EXIT_LONG_LIQUIDATE);
						return ERROR;
					}
				}
			}
		}
	}
	else //use fast market parameter
	{
		//Check price to send order with MAX LOSER SPREAD
		if (newOrder->side == BUY_TO_CLOSE_TYPE)
		{
			if (fgt(newOrder->price, (stuck->price + asCollection.globalConf.fm_maxLoserSpread)))
			{
				if (isNeedCheckExecutionRate == 0)
				{
					TraceLog(DEBUG_LEVEL, "Bad price to exit SHORT: insidePrice = %.2lf, stuckPrice = %.2lf, fm_maxLoserSpread = %lf. Disengage.\n", newOrder->price, stuck->price, asCollection.globalConf.fm_maxLoserSpread);
					SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_BAD_PRICE_EXIT_SHORT_LIQUIDATE);
					return ERROR;
				}
				else
				{
					if (executionRate > asCollection.globalConf.highExecutionRate)
					{
						TraceLog(DEBUG_LEVEL, "Bad price to exit SHORT: insidePrice = %.2lf, stuckPrice = %.2lf, fm_maxLoserSpread = %lf. But execution rate (%d) is high so moving to Fast Market situation.\n", newOrder->price, stuck->price, asCollection.globalConf.fm_maxLoserSpread, executionRate);
						return CheckAndPlaceOrderForFastMarket(stuck, needSleep, replaceAfterSleeping, indexOpenOrder, forceEngage);
					}
					else
					{
						TraceLog(DEBUG_LEVEL, "Bad price to exit SHORT: insidePrice = %.2lf, stuckPrice = %.2lf, fm_maxLoserSpread = %lf. And execution rate (%d) is low so disengage.\n", newOrder->price, stuck->price, asCollection.globalConf.fm_maxLoserSpread, executionRate);
						SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_BAD_PRICE_EXIT_SHORT_LIQUIDATE);
						return ERROR;
					}
				}
			}
		}
		else
		{
			if (flt(newOrder->price, (stuck->price - asCollection.globalConf.fm_maxLoserSpread)))
			{
				if (isNeedCheckExecutionRate == 0)
				{
					TraceLog(DEBUG_LEVEL, "Bad price to exit LONG: insidePrice = %.2lf, stuckPrice = %.2lf, fm_maxLoserSpread = %lf. Disengage.\n", newOrder->price, stuck->price, asCollection.globalConf.fm_maxLoserSpread);
					SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_BAD_PRICE_EXIT_LONG_LIQUIDATE);
					return ERROR;
				}
				else
				{
					if (executionRate > asCollection.globalConf.highExecutionRate)
					{
						TraceLog(DEBUG_LEVEL, "Bad price to exit LONG: insidePrice = %.2lf, stuckPrice = %.2lf, fm_maxLoserSpread = %lf. But execution rate (%d) is high so moving to Fast Market situation.\n", newOrder->price, stuck->price, asCollection.globalConf.fm_maxLoserSpread, executionRate);
						return CheckAndPlaceOrderForFastMarket(stuck, needSleep, replaceAfterSleeping, indexOpenOrder, forceEngage);
					}
					else
					{
						TraceLog(DEBUG_LEVEL, "Bad price to exit LONG: insidePrice = %.2lf, stuckPrice = %.2lf, fm_maxLoserSpread = %lf. And execution rate (%d) is low so disengage.\n", newOrder->price, stuck->price, asCollection.globalConf.fm_maxLoserSpread, executionRate);
						SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_BAD_PRICE_EXIT_LONG_LIQUIDATE);
						return ERROR;
					}
				}
			}
		}
	}
	
	TraceLog(DEBUG_LEVEL, "------------------------------------------\n");
	TraceLog(DEBUG_LEVEL, "Send Liquidate Order to ECN: %s:\n", (newOrder->ECNId == TYPE_NASDAQ_RASH) ? "RASH":"ARCA DIRECT");
	TraceLog(DEBUG_LEVEL, "symbol = %.8s, side = %d (0: Sell, 1: Buy)\n", newOrder->symbol, !stuck->side);
	TraceLog(DEBUG_LEVEL, "shares = %d, maxFloor = %d\n", newOrder->shares, newOrder->maxFloor);
	TraceLog(DEBUG_LEVEL, "price = %.2lf, tif = %d (0: DAY, 1: IOC)\n", newOrder->price, newOrder->tif);
	TraceLog(DEBUG_LEVEL, "------------------------------------------\n");

	/****************************************************************************************************
	Check some preconditions
	+ Trading Status
	+ Symbol is valid or not
	****************************************************************************************************/
	// Trading status must be enabled to continue send order
	if (asCollection.currentStatus.tradingStatus == DISABLED)
	{
		TraceLog(DEBUG_LEVEL, "DISABLED TRADING! Cannot place order\n");
		
		SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_DISABLED_TRADING);
		
		return ERROR;
	}
	
	/****************************************************************************************************
	We are ready to send order
	****************************************************************************************************/
	pthread_mutex_lock(&nextOrderID_Mutex);	
	
	//assign new value to OrderNextID
	newOrder->orderId = clientOrderID;

	//Increase next order id by 1
	clientOrderID += 1;
	
	SaveClientOrderID("next_order_id", clientOrderID);
	
	pthread_mutex_unlock(&nextOrderID_Mutex);
	
	int result = ERROR;
	switch (newOrder->ECNId)
	{
		case TYPE_ARCA_DIRECT:
			// Call function to place order at ARCA DIRECT
			result = SendNewOrderToARCA_DIRECTOrder(newOrder);
			break;
		
		case TYPE_NASDAQ_RASH:
			// Place order at RASH
			result = SendNewOrderToNASDAQ_RASHOrder(newOrder);
			break;
	}

	if (result == SUCCESS)
	{
		//ProcessAddNewOrder(newOrder); //Removed by KhanhND, instead, this will be called when send new order to ECN
		t_PositionInfo *positionItem = &positionCollection[newOrder->symbolIndex];
		positionItem->ECNIndex = newOrder->ECNId;
		positionItem->typePlacedOrder = LIQUIDATE_ORDER;
		positionItem->willReplace = YES;
		memcpy(&positionItem->stuck, stuck, sizeof(t_Position));
		
		ShowTradedInfo(newOrder->symbol);
		ShowFeedsDataForSymbol(newOrder->symbolIndex, CQS_SOURCE);
		ShowFeedsDataForSymbol(newOrder->symbolIndex, UQDF_SOURCE);
		return SUCCESS;
	}

	return ERROR;
}

/****************************************************************************
- Function name:	SendBBOOrder
- Input:			
- Output:		
- Return:			
- Description:	
- Usage:			
****************************************************************************/
int SendBBOOrder(t_SendNewOrder *newOrder, t_Position *stuck, t_TopOrder bestQuote[MAX_SIDE], int isNeedCheckExecutionRate, int executionRate, int useFastMarketParameters, int needSleep, int replaceAfterSleeping, int indexOpenOrder, int forceEngage)
{
	// Update order type, LIQUIDATE_ORDER or BBO_ORDER
	newOrder->type = BBO_ORDER;
	
	if (useFastMarketParameters == 0)
	{
		//Check price to send order with MAX LOSER SPREAD
		if (newOrder->side == BUY_TO_CLOSE_TYPE)
		{
			if (fgt(newOrder->price, (stuck->price + asCollection.globalConf.maxLoserSpread)))
			{
				if (isNeedCheckExecutionRate == 0)
				{
					TraceLog(DEBUG_LEVEL, "Bad price to exit SHORT: insidePrice = %.2lf, stuckPrice = %.2lf, maxLoserSpread = %lf. Disengage.\n", newOrder->price, stuck->price, asCollection.globalConf.maxLoserSpread);
					SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_BAD_PRICE_EXIT_SHORT_BBO);
					return ERROR;
				}
				else
				{
					if (executionRate > asCollection.globalConf.highExecutionRate)
					{
						TraceLog(DEBUG_LEVEL, "Bad price to exit SHORT: insidePrice = %.2lf, stuckPrice = %.2lf, maxLoserSpread = %lf. But execution rate (%d) is high, so moving to Fast Market situation.\n", newOrder->price, stuck->price, asCollection.globalConf.maxLoserSpread, executionRate);
						return CheckAndPlaceOrderForFastMarket(stuck, needSleep, replaceAfterSleeping, indexOpenOrder, forceEngage);
					}
					else
					{
						TraceLog(DEBUG_LEVEL, "Bad price to exit SHORT: insidePrice = %.2lf, stuckPrice = %.2lf, maxLoserSpread = %lf. And execution rate (%d) is low so disengage.\n", newOrder->price, stuck->price, asCollection.globalConf.maxLoserSpread, executionRate);
						SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_BAD_PRICE_EXIT_SHORT_BBO);
						return ERROR;
					}
				}
			}
		}
		else
		{
			if (flt(newOrder->price, (stuck->price - asCollection.globalConf.maxLoserSpread)))
			{
				if (isNeedCheckExecutionRate == 0)
				{
					TraceLog(DEBUG_LEVEL, "Bad price to exit LONG: insidePrice = %.2lf, stuckPrice = %.2lf, maxLoserSpread = %lf. Disengage.\n", newOrder->price, stuck->price, asCollection.globalConf.maxLoserSpread);
					SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_BAD_PRICE_EXIT_LONG_BBO);
					return ERROR;
				}
				else
				{
					if (executionRate > asCollection.globalConf.highExecutionRate)
					{
						TraceLog(DEBUG_LEVEL, "Bad price to exit LONG: insidePrice = %.2lf, stuckPrice = %.2lf, maxLoserSpread = %lf. But execution rate (%d) is high, so moving to Fast Market situation.\n", newOrder->price, stuck->price, asCollection.globalConf.maxLoserSpread, executionRate);
						return CheckAndPlaceOrderForFastMarket(stuck, needSleep, replaceAfterSleeping, indexOpenOrder, forceEngage);
					}
					else
					{
						TraceLog(DEBUG_LEVEL, "Bad price to exit LONG: insidePrice = %.2lf, stuckPrice = %.2lf, maxLoserSpread = %lf. And execution rate (%d) is low, so disengage.\n", newOrder->price, stuck->price, asCollection.globalConf.maxLoserSpread, executionRate);
						SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_BAD_PRICE_EXIT_LONG_BBO);
						return ERROR;
					}
				}
			}
		}
	}
	else // use fast market value
	{
		//Check price to send order with MAX LOSER SPREAD
		if (newOrder->side == BUY_TO_CLOSE_TYPE)
		{
			if (fgt(newOrder->price, (stuck->price + asCollection.globalConf.fm_maxLoserSpread)))
			{
				if (isNeedCheckExecutionRate == 0)
				{
					TraceLog(DEBUG_LEVEL, "Bad price to exit SHORT: insidePrice = %.2lf, stuckPrice = %.2lf, fm_maxLoserSpread = %lf. Disengage.\n", newOrder->price, stuck->price, asCollection.globalConf.fm_maxLoserSpread);
					SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_BAD_PRICE_EXIT_SHORT_BBO);
					return ERROR;
				}
				else
				{
					if (executionRate > asCollection.globalConf.highExecutionRate)
					{
						TraceLog(DEBUG_LEVEL, "Bad price to exit SHORT: insidePrice = %.2lf, stuckPrice = %.2lf, fm_maxLoserSpread = %lf. But execution rate (%d) is high, so moving to Fast Market situation.\n", newOrder->price, stuck->price, asCollection.globalConf.fm_maxLoserSpread, executionRate);
						return CheckAndPlaceOrderForFastMarket(stuck, needSleep, replaceAfterSleeping, indexOpenOrder, forceEngage);
					}
					else
					{
						TraceLog(DEBUG_LEVEL, "Bad price to exit SHORT: insidePrice = %.2lf, stuckPrice = %.2lf, fm_maxLoserSpread = %lf. And execution rate (%d) is low so disengage.\n", newOrder->price, stuck->price, asCollection.globalConf.fm_maxLoserSpread, executionRate);
						SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_BAD_PRICE_EXIT_SHORT_BBO);
						return ERROR;
					}
				}
			}
		}
		else
		{
			if (flt(newOrder->price, (stuck->price - asCollection.globalConf.fm_maxLoserSpread)))
			{
				if (isNeedCheckExecutionRate == 0)
				{
					TraceLog(DEBUG_LEVEL, "Bad price to exit LONG: insidePrice = %.2lf, stuckPrice = %.2lf, fm_maxLoserSpread = %lf. Disengage.\n", newOrder->price, stuck->price, asCollection.globalConf.fm_maxLoserSpread);
					SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_BAD_PRICE_EXIT_LONG_BBO);
					return ERROR;
				}
				else
				{
					if (executionRate > asCollection.globalConf.highExecutionRate)
					{
						TraceLog(DEBUG_LEVEL, "Bad price to exit LONG: insidePrice = %.2lf, stuckPrice = %.2lf, fm_maxLoserSpread = %lf. But execution rate (%d) is high, so moving to Fast Market situation.\n", newOrder->price, stuck->price, asCollection.globalConf.fm_maxLoserSpread, executionRate);
						return CheckAndPlaceOrderForFastMarket(stuck, needSleep, replaceAfterSleeping, indexOpenOrder, forceEngage);
					}
					else
					{
						TraceLog(DEBUG_LEVEL, "Bad price to exit LONG: insidePrice = %.2lf, stuckPrice = %.2lf, fm_maxLoserSpread = %lf. And execution rate (%d) is low, so disengage.\n", newOrder->price, stuck->price, asCollection.globalConf.fm_maxLoserSpread, executionRate);
						SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_BAD_PRICE_EXIT_LONG_BBO);
						return ERROR;
					}
				}
			}
		}
	}
	
	TraceLog(DEBUG_LEVEL, "-----------------------------------------\n");
	TraceLog(DEBUG_LEVEL, "Send BBO Order to ECN: %s:\n", (newOrder->ECNId == TYPE_NASDAQ_RASH) ? "RASH":"ARCA DIRECT");
	TraceLog(DEBUG_LEVEL, "symbol = %.8s, side = %d (0: Sell, 1: Buy)\n", newOrder->symbol, !stuck->side);
	TraceLog(DEBUG_LEVEL, "shares = %d, maxFloor = %d\n", newOrder->shares, newOrder->maxFloor);
	TraceLog(DEBUG_LEVEL, "price = %.2lf, tif = %d (0: DAY, 1: IOC)\n", newOrder->price, newOrder->tif);
	TraceLog(DEBUG_LEVEL, "-----------------------------------------\n");

	/****************************************************************************************************
	Check some preconditions
	+ Trading Status
	+ Symbol is valid or not
	****************************************************************************************************/
	// Trading status must be enabled to continue send order
	if (asCollection.currentStatus.tradingStatus == DISABLED)
	{
		TraceLog(DEBUG_LEVEL, "DISABLED TRADING! Cannot place order\n");
		
		SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_DISABLED_TRADING);			
		
		return ERROR;
	}
	
	/****************************************************************************************************
	We are ready to send order
	****************************************************************************************************/
	pthread_mutex_lock(&nextOrderID_Mutex);	
	
	//assign new value to OrderNextID
	newOrder->orderId = clientOrderID;

	//Increase next order id by 1
	clientOrderID += 1;
	
	SaveClientOrderID("next_order_id", clientOrderID);
	
	pthread_mutex_unlock(&nextOrderID_Mutex);
	
	int result = ERROR;
	switch (newOrder->ECNId)
	{
		case TYPE_ARCA_DIRECT:
			// Call function to place order at ARCA DIRECT
			result = SendNewOrderToARCA_DIRECTOrder(newOrder);
			break;
		
		case TYPE_NASDAQ_RASH:
			// Place order at RASH
			result = SendNewOrderToNASDAQ_RASHOrder(newOrder);
			break;
	}

	if (result == SUCCESS)
	{
		//ProcessAddNewOrder(newOrder); //Removed by KhanhND, instead, this will be called when send new order to ECN

		t_PositionInfo *positionItem = &positionCollection[newOrder->symbolIndex];
		positionItem->ECNIndex = newOrder->ECNId;
		positionItem->typePlacedOrder = BBO_ORDER;
		positionItem->willReplace = YES;
		memcpy(&positionItem->stuck, stuck, sizeof(t_Position));
		memcpy(&positionItem->bestQuote, bestQuote, sizeof(t_TopOrder) * MAX_SIDE);

		TraceLog(DEBUG_LEVEL, "Save best orders: side = %d, best price = %.2lf\n", !stuck->side, bestQuote[!stuck->side].info.sharePrice);
		
		ShowTradedInfo(newOrder->symbol);
		ShowFeedsDataForSymbol(newOrder->symbolIndex, CQS_SOURCE);
		ShowFeedsDataForSymbol(newOrder->symbolIndex, UQDF_SOURCE);
		return SUCCESS;
	}

	return ERROR;
}

/****************************************************************************
- Function name:	CheckBBOOrder
- Input:			
- Output:		
- Return:			
- Description:	
- Usage:			
****************************************************************************/
int CheckBBOOrder(int stockSymbolIndex, int side, int ecnBookID, long refNum, int shares, double price, int isType)
{
	// skip if liquidate order or the symbol is not trading
	if ((positionCollection[stockSymbolIndex].isTrading == NO) || (positionCollection[stockSymbolIndex].typePlacedOrder == LIQUIDATE_ORDER))
	{	
		return ERROR;
	}
	
	if ((ecnBookID != -1) && (fgt(price, 0.0)) && (shares > 0))
	{
		// Save best quote that get was -> Add hold quote	
		positionCollection[stockSymbolIndex].holdQuote[side].shares = shares;
		positionCollection[stockSymbolIndex].holdQuote[side].ecnBookID = ecnBookID;
		positionCollection[stockSymbolIndex].holdQuote[side].refNum = refNum;
		positionCollection[stockSymbolIndex].holdQuote[side].price = price;
	}	
	
	if (ecnBookID >= MAX_BOOK_CONNECTIONS)
	{
		DEVLOGF("BBO Quotes: %s, %d, %d, %d, %lf\n", positionCollection[stockSymbolIndex].stuck.symbol, ecnBookID, side, shares, price);
	}
	
	if (IsSleeping(stockSymbolIndex) == SUCCESS)
	{
		return ERROR;
	}

	if (side == positionCollection[stockSymbolIndex].stuck.side)		// Quote change occurs on the opposing inside price
	{
		if (CheckQuoteForBBOOrder(&positionCollection[stockSymbolIndex].stuck) == SUCCESS)
		{
			if (positionCollection[stockSymbolIndex].willReplace == YES)
			{
				positionCollection[stockSymbolIndex].willReplace = NO;

				// We should cancel and replace our order based on the algorithmís criteria.
				TraceLog(DEBUG_LEVEL, "Our order is not beaten but a quote change occurs on the opposing inside price: side = %d, shares = %d, best price = %.2lf, ecn = %d\n", side, shares, price, ecnBookID);

				return ProcessDisengageSymbol(positionCollection[stockSymbolIndex].stuck.symbol, PM_REQUEST);
			}
			/*else
			{
				TraceLog(DEBUG_LEVEL, "CheckBBOOrder: (%.8s), willReplace=NO\n", positionCollection[stockSymbolIndex].stuck.symbol);
			}*/
		}
		/*else
		{
			TraceLog(DEBUG_LEVEL, "CheckBBOOrder: (%.8s), CheckQuoteForBBOOrder=ERROR\n", positionCollection[stockSymbolIndex].stuck.symbol);
		}*/
	}
	else	// Our order is beaten
	{
		if (!feq(price, positionCollection[stockSymbolIndex].bestQuote[side].info.sharePrice))
		{
			if (positionCollection[stockSymbolIndex].willReplace == YES)
			{
				positionCollection[stockSymbolIndex].willReplace = NO;

				// We should cancel and replace our order based on the algorithmís criteria.
				TraceLog(DEBUG_LEVEL, "Our order is beaten: side = %d, shares = %d, best price = %.2lf, ecn = %d. We will cancel and replace our order based on the algorithm's criteria.\n", side, shares, price, ecnBookID);

				return ProcessDisengageSymbol(positionCollection[stockSymbolIndex].stuck.symbol, PM_REQUEST);
			}
			else
			{
				//TraceLog(DEBUG_LEVEL, "CheckBBOOrder: (%s), willReplace=NO.\n", positionCollection[stockSymbolIndex].stuck.symbol);
			}
		}
		else
		{
			// Book incoming is the same price with our order was placed
			if (positionCollection[stockSymbolIndex].bestQuote[side].ecnIndicator == ecnBookID)
			{
				if (ecnBookID == BOOK_ARCA)
				{
					// ARCA book is the same price with our order placed at ARCA DIRECT
					/* we constructed ref from ARCA XDP as following:
						refNum = <4Bytes symbolIndex><4Bytes OrderID>
						so we need to take out the <4 Bytes OrderID> to look up
					*/
					int _orderID = (refNum & 4294967295L);
					
					int quoteIndex = FindQuoteIgnore(stockSymbolIndex, _orderID);

					if (quoteIndex == -1)
					{
						if (positionCollection[stockSymbolIndex].willReplace == YES)
						{
							positionCollection[stockSymbolIndex].willReplace = NO;

							// We should cancel and replace our order based on the algorithmís criteria.
							
							TraceLog(DEBUG_LEVEL, "Our order is beaten: side = %d, shares = %d, best price = %.2lf, ecn = %d. We will cancel and replace our order based on the algorithm's criteria\n", side, shares, price, ecnBookID);

							return ProcessDisengageSymbol(positionCollection[stockSymbolIndex].stuck.symbol, PM_REQUEST);
						}
						else
						{
							//TraceLog(DEBUG_LEVEL, "CheckBBOOrder: (%s), willReplace=NO..\n", positionCollection[stockSymbolIndex].stuck.symbol);
						}
					}
					else
					{
						// PM will do nothing ...
						//TraceLog(DEBUG_LEVEL, "CheckBBOOrder: (%s) PM do nothing\n", positionCollection[stockSymbolIndex].stuck.symbol);
					}
				}
				else
				{
					//Nasdaq Book with same price
					if (positionCollection[stockSymbolIndex].willReplace == YES)
					{
						positionCollection[stockSymbolIndex].willReplace = NO;

						// We should cancel and replace our order based on the algorithmís criteria.
							
						TraceLog(DEBUG_LEVEL, "Our order is beaten: side = %d, shares = %d, best price = %.2lf, ecn = %d. We will cancel and replace our order based on the algorithm's criteria.\n", side, shares, price, ecnBookID);

						return ProcessDisengageSymbol(positionCollection[stockSymbolIndex].stuck.symbol, PM_REQUEST);
					}
					else
					{
						//TraceLog(DEBUG_LEVEL, "CheckBBOOrder: (%s), willReplace=NO..\n", positionCollection[stockSymbolIndex].stuck.symbol);
					}
				}
			}
			else
			{
				// Received book from ARCA book but our order was placed at NDAQ RASH. 
				// OR: Received book from NDAQ book but our order was placed at ARCA DIRECT. 
				// PM will cancel and replace our order based on the algorithm's criteria.
				if (positionCollection[stockSymbolIndex].willReplace == YES)
				{
					positionCollection[stockSymbolIndex].willReplace = NO;

					// We should cancel and replace our order based on the algorithmís criteria.
					TraceLog(DEBUG_LEVEL, "Our order is beaten: side = %d, shares = %d, best price = %.2lf, ecn = %d. We will cancel and replace our order based on the algorithm's criteria..\n", side, shares, price, ecnBookID);

					return ProcessDisengageSymbol(positionCollection[stockSymbolIndex].stuck.symbol, PM_REQUEST);
				}
				else
				{
					//TraceLog(DEBUG_LEVEL, "CheckBBOOrder: (%s), willReplace=NO...\n", positionCollection[stockSymbolIndex].stuck.symbol);
				}
			}
		}
	}

	return ERROR;
}

/****************************************************************************
- Function name:	CheckQuoteForBBOOrder
- Input:			
- Output:		
- Return:		
- Description:	
- Usage:			
****************************************************************************/
int CheckQuoteForBBOOrder(t_Position *stuck)
{		
	// if returned value = ERROR --> Will not "Cancel and Place order again"
	// if returned value = SUCCESS --> Cancel existing orders and CheckAndPlaceOrder again
	
	// Update symbol index
	stuck->symbolIndex = GetStockSymbolIndex(stuck->symbol);

	// If stock symbol is not in SymbolList
	if (stuck->symbolIndex == -1)
	{
		TraceLog(ERROR_LEVEL, "Can't find symbol = %.8s in SymbolList\n", stuck->symbol);
		return ERROR;
	}

	// Check symbol ignored
	if (symbolIgnoreStatus[stuck->symbolIndex] == DISABLE)
	{
		TraceLog(ERROR_LEVEL, "Symbol %.8s was ignored. Cannot place order\n", stuck->symbol);
		return ERROR;
	}	

	/****************************************************************************************************
	Get best quotes
	+ Bid Quote
	+ Ask Quote
	****************************************************************************************************/
		
	// -----------------------------------
	// Best Quotes from Stock Book
	// -----------------------------------
	t_TopOrder bestQuote[MAX_SIDE];
	if (stuck->shares > MAX_ODD_LOT_SHARE)
	{
		// Get mutex lock to synchronize update/access on current Best Quote
		pthread_mutex_lock(&updateAskBidMutex[stuck->symbolIndex]);

		GetBestOrderRoundLot(&bestQuote[ASK_SIDE], ASK_SIDE, stuck->symbolIndex);
		GetBestOrderRoundLot(&bestQuote[BID_SIDE], BID_SIDE, stuck->symbolIndex);

		// Release mutex lock
		pthread_mutex_unlock(&updateAskBidMutex[stuck->symbolIndex]);
	}
	else
	{
		// Get mutex lock to synchronize update/access on current Best Quote
		pthread_mutex_lock(&updateAskBidMutex[stuck->symbolIndex]);

		GetBestOrder(&bestQuote[ASK_SIDE], ASK_SIDE, stuck->symbolIndex);
		GetBestOrder(&bestQuote[BID_SIDE], BID_SIDE, stuck->symbolIndex);

		// Release mutex lock
		pthread_mutex_unlock(&updateAskBidMutex[stuck->symbolIndex]);
	}
	
	// -----------------------------------
	// Best BBO Quotes from CQS and UQDF
	// -----------------------------------
	t_BestBBO bestBBO[MAX_SIDE];
	
	bestBBO[ASK_SIDE].price = 0.0;
	bestBBO[BID_SIDE].price = 0.0;

	GetBestBBOOrder(&bestBBO[ASK_SIDE], ASK_SIDE, stuck->symbolIndex);
	GetBestBBOOrder(&bestBBO[BID_SIDE], BID_SIDE, stuck->symbolIndex);

	// -----------------------------------
	// Check to get best ASK
	// -----------------------------------
	if (bestBBO[ASK_SIDE].participantId != -1)
	{
		if (fgt(bestQuote[ASK_SIDE].info.sharePrice, 0.01))
		{
			if (fgt(bestQuote[ASK_SIDE].info.sharePrice, bestBBO[ASK_SIDE].price))
			{
				bestQuote[ASK_SIDE].info.sharePrice = bestBBO[ASK_SIDE].price;
				bestQuote[ASK_SIDE].info.shareVolume = bestBBO[ASK_SIDE].size;
				bestQuote[ASK_SIDE].ecnIndicator = bestBBO[ASK_SIDE].participantId;
			}
		}
		else
		{
			bestQuote[ASK_SIDE].info.sharePrice = bestBBO[ASK_SIDE].price;
			bestQuote[ASK_SIDE].info.shareVolume = bestBBO[ASK_SIDE].size;
			bestQuote[ASK_SIDE].ecnIndicator = bestBBO[ASK_SIDE].participantId;
		}
	}
	
	if ((bestQuote[ASK_SIDE].ecnIndicator == -1) || flt(bestQuote[ASK_SIDE].info.sharePrice, 0.01))
	{
		// Best Ask is nothing
		ProcessDisengageSymbol(stuck->symbol, TRADER_REQUEST);
		SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_BEST_ASK_NOTHING);
		return ERROR;
	}
	
	// -----------------------------------
	// Check to get best BID
	// -----------------------------------
	if (flt(bestQuote[BID_SIDE].info.sharePrice, bestBBO[BID_SIDE].price))
	{
		bestQuote[BID_SIDE].info.sharePrice = bestBBO[BID_SIDE].price;
		bestQuote[BID_SIDE].info.shareVolume = bestBBO[BID_SIDE].size;
		bestQuote[BID_SIDE].ecnIndicator = bestBBO[BID_SIDE].participantId;
	}
	
	if ((bestQuote[BID_SIDE].ecnIndicator == -1) || (flt(bestQuote[BID_SIDE].info.sharePrice, 0.01)))
	{
		// Best Bid is nothing
		ProcessDisengageSymbol(stuck->symbol, TRADER_REQUEST);
		SendPMDisengageAlertToAS(stuck, DISENGAGE_REASON_BEST_BID_NOTHING);
		return ERROR;
	}
	
	// -----------------------------------------------------------------------------------------
	// Check algorithm condition to see if we should cancel and re-place order
	// The algorithm of this section should look like the same as the algorithm in
	// CheckAndPlaceOrder() function.
	// !!! However, If this function is called, then a change in aligning side occurred.
	// So the condition is check to see if we can cancel and replace order using Liquidate order!
	// -----------------------------------------------------------------------------------------
	double insideSpread = bestQuote[ASK_SIDE].info.sharePrice - bestQuote[BID_SIDE].info.sharePrice;
		
	//We should not place an order if the stock is crossed
	if (fle(insideSpread, 0.0))
	{
		if (bestQuote[BID_SIDE].ecnIndicator == bestQuote[ASK_SIDE].ecnIndicator)
		{
			return ERROR;
		}
	}

	/*
	Opposing Inside: The best bid/best ask on the opposing side of the book as our position
	(Long = Bid / Short = Ask).
	*/
	double opposingSpread;

	if (stuck->side == ASK_SIDE)
	{
		opposingSpread = stuck->price - bestQuote[ASK_SIDE].info.sharePrice;
	}
	else
	{
		opposingSpread = bestQuote[BID_SIDE].info.sharePrice - stuck->price;
	}

	// Calculation tight spread, as defined by [<autospread> * <stuck price>, not to exceed <maxspread>]	
	double tightSpread = asCollection.globalConf.autoSpread * stuck->price;

	if (flt(tightSpread, 0.01))
	{
		tightSpread = 0.01;
	}

	if (fle(insideSpread, tightSpread))
	{
		if (fgt(insideSpread, asCollection.globalConf.maxSpread))
		{
			return ERROR;
		}

		return SUCCESS;
	}
	else if (fge(opposingSpread, 0.0))
	{
		// Calculation tight profit spread, as defined by [<profitspread> * <stuck price>, not to exceed <maxprofitspread>]
		double tightProfitSpread = asCollection.globalConf.profitSpread * stuck->price;

		if (fle(insideSpread, tightProfitSpread))
		{
			if (fgt(insideSpread, asCollection.globalConf.maxProfitSpread))
			{
				return ERROR;
			}

			return SUCCESS;
		}
		else
		{
			return ERROR;
		}
	}
	else if ((opposingSpread + asCollection.globalConf.bigLoserAmount / stuck->shares) < 0)
	{
		// Calculation tight big loser spread, as defined by [<bigloserspread> * <stuck price>, not to exceed <maxbigloserspread>]
		double tightBigLosertSpread = asCollection.globalConf.bigLoserSpread * stuck->price;

		if (fle(insideSpread, tightBigLosertSpread))
		{
			if (fgt(insideSpread, asCollection.globalConf.maxBigLoserSpread))
			{
				return ERROR;
			}

			return SUCCESS;
		}
		else
		{
			return ERROR;
		}
	}
	else
	{
		return ERROR;
	}
}

/****************************************************************************
- Function name:	AddQuoteIgnore
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int AddQuoteIgnore(int symbolIndex, int orderID, int shares, double price)
{
	int quoteIndex, isUpdate = 0;

	for (quoteIndex = 0; quoteIndex < MAX_QUOTE_IGNORE; quoteIndex ++)
	{
		if (quoteIgnoreList[symbolIndex][quoteIndex].refNum == -1)
		{
			quoteIgnoreList[symbolIndex][quoteIndex].shareVolume = shares;
			quoteIgnoreList[symbolIndex][quoteIndex].sharePrice = price;
			quoteIgnoreList[symbolIndex][quoteIndex].refNum = orderID;
			isUpdate = 1;

			break;
		}
	}

	if (isUpdate == 0)
	{
		TraceLog(ERROR_LEVEL, "quoteIgnoreList is full! Cannot add quote, orderID = %d\n", orderID);
		return ERROR;
	}

	return SUCCESS;
}

/****************************************************************************
- Function name:	DeleteQuoteIgnore
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int DeleteQuoteIgnore(int symbolIndex, int orderID)
{
	int quoteIndex;

	for (quoteIndex = 0; quoteIndex < MAX_QUOTE_IGNORE; quoteIndex ++)
	{
		if (quoteIgnoreList[symbolIndex][quoteIndex].refNum == orderID)
		{
			quoteIgnoreList[symbolIndex][quoteIndex].shareVolume = 0;
			quoteIgnoreList[symbolIndex][quoteIndex].sharePrice = 0.00;
			quoteIgnoreList[symbolIndex][quoteIndex].refNum  = -1;

			break;
		}
	}

	return SUCCESS;
}

/****************************************************************************
- Function name:	FindQuoteIgnore
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int FindQuoteIgnore(int symbolIndex, int orderID)
{
	int quoteIndex;

	for (quoteIndex = 0; quoteIndex < MAX_QUOTE_IGNORE; quoteIndex ++)
	{
		if (quoteIgnoreList[symbolIndex][quoteIndex].refNum == orderID)
		{
			return quoteIndex;
		}
	}

	return -1;
}

/****************************************************************************
- Function name:	Monitoring_Liquidate_Order_Thread
- Input:			N/A
- Output:		
- Return:			N/A
- Description:	
- Usage:			N/A 
****************************************************************************/
void *Monitoring_Liquidate_Order_Thread(void *agrs)
{	
	SetKernelAlgorithm("[LIQUIDATE_ORDER_MONITORING]");
	TraceLog(DEBUG_LEVEL, "Start thread to monitor liquidate orders placed ...\n");

	int i, index;
	long numUsec;
	int microSeconds, seconds;
	int ecnBookID, symbolIndex, tmpSide;
	
	while(1)
	{
		// Get time of day
		hbitime_micros_parts(&seconds, &microSeconds);

		// Monitor liquidate orders placed
		for (index = 0; index < LiquidateOrderCollection.count; index++)
		{
			i = LiquidateOrderCollection.collection[index];
			// Check liquidate order placed
			if ((asOpenOrder.orderSummary[i].status == orderStatusIndex.Live) && (asOpenOrder.orderSummary[i].type == LIQUIDATE_ORDER))
			{
				if ((asOpenOrder.orderSummary[i].numSeconds != -1) && (asOpenOrder.orderSummary[i].numMicroseconds != -1))
				{
					numUsec = (seconds - asOpenOrder.orderSummary[i].numSeconds) * 1000000 + (microSeconds - asOpenOrder.orderSummary[i].numMicroseconds);

					if ((asOpenOrder.orderSummary[i].shares >= maxShareNeedSleep) && (asOpenOrder.orderSummary[i].requester == TRADER_REQUEST))
					{
						if (numUsec < maxMilisecondToSleep * 1000)
						{
							continue;
						}
					}
					else
					{
						if (numUsec < NUM_MICROSECOND_TO_CANCEL)
						{
							continue;
						}
					}
					
					if ((asOpenOrder.orderSummary[i].leftShares == asOpenOrder.orderSummary[i].shares) && (asOpenOrder.orderSummary[i].requester == TRADER_REQUEST))
					{
						TraceLog(DEBUG_LEVEL, "Liquidate order (%d): no fill, begin send a cancel to the order\n", asOpenOrder.orderSummary[i].clOrdId);

						// Send a canccel request
						asOpenOrder.orderSummary[i].requester = PM_REQUEST;
						ProcessSendCancelRequestToECNOrder(asOpenOrder.orderSummary[i].clOrdId, asOpenOrder.orderSummary[i].ecnOrderId);

						// Remove this quote
						ecnBookID = -1;

						switch (asOpenOrder.orderSummary[i].ECNId)
						{
							case TYPE_ARCA_DIRECT:
								ecnBookID = BOOK_ARCA;
								break;
							case TYPE_NASDAQ_RASH:
								ecnBookID = BOOK_NASDAQ;
								break;
						}

						if (ecnBookID != -1)
						{
							symbolIndex = asOpenOrder.orderSummary[i].symbolIndex;

							if (asOpenOrder.orderSummary[i].side == BUY_TO_CLOSE_TYPE)
							{
								tmpSide = ASK_SIDE;
							}
							else
							{
								tmpSide = BID_SIDE;
							}

							quoteToDeleteCollection[ecnBookID].stockSymbolIndex = symbolIndex;
							quoteToDeleteCollection[ecnBookID].side = tmpSide;
							quoteToDeleteCollection[ecnBookID].sharePrice = asOpenOrder.orderSummary[i].price;

							TraceLog(DEBUG_LEVEL, "Delete quotes, symbol = %.8s, side = %d, price = %.2lf, ecnBookID = %d\n", asOpenOrder.orderSummary[i].symbol, tmpSide, asOpenOrder.orderSummary[i].price, ecnBookID);

							// Delete from Book
							pthread_mutex_lock(&deleteQuotesMgmtMutex[ecnBookID][symbolIndex][tmpSide]);
							
							DeleteQuotes(ecnBookID, &quoteToDeleteCollection[ecnBookID]);
							
							pthread_mutex_unlock(&deleteQuotesMgmtMutex[ecnBookID][symbolIndex][tmpSide]);
							
							// Delete from BBO Quote
							DeleteBBOAtPrice(symbolIndex, tmpSide, asOpenOrder.orderSummary[i].price);
							
						}
					}
				}
			}
		}

		// Delay in microsecond
		usleep(MONITOR_ORDER_DELAY);
	}

	return NULL;
}

/****************************************************************************
- Function name:	Monitoring_BBO_Order_Sleeping_Thread
- Input:			N/A
- Output:		
- Return:			N/A
- Description:	
- Usage:			N/A 
****************************************************************************/
void *Monitoring_BBO_Order_Sleeping_Thread(void *agrs)
{
	SetKernelAlgorithm("[BBO_ORDER_MONITORING]");
	
	TraceLog(DEBUG_LEVEL, "Start thread to monitor BBO orders sleeping ...\n");
	
	int i, indexOpenOrder;
	long numUsec;
	int microSeconds, seconds;
	
	while(1)
	{
		if (BBOOrderSleepingCollection.count > 0)
		{
			// Get time of day
			hbitime_micros_parts(&seconds, &microSeconds);

			// Monitor BBO orders are sleeping
			for (i = 0; i < MAX_BBO_ORDER_SLEEPING_ITEM; i++)
			{
				if (BBOOrderSleepingCollection.collection[i] == -1)
				{
					continue;
				}
				
				indexOpenOrder = BBOOrderSleepingCollection.collection[i];
				
				if ((asOpenOrder.orderSummary[indexOpenOrder].status == orderStatusIndex.Live) && (asOpenOrder.orderSummary[indexOpenOrder].type == BBO_ORDER))
				{
					numUsec = (seconds - asOpenOrder.orderSummary[indexOpenOrder].numSeconds) * 1000000 + (microSeconds - asOpenOrder.orderSummary[indexOpenOrder].numMicroseconds);

					if (numUsec < maxMilisecondToSleep * 1000)
					{
						//TraceLog(DEBUG_LEVEL, "BBO order (%d) is sleeping for %ld ms.\n", asOpenOrder.orderSummary[indexOpenOrder].clOrdId, numUsec / 1000);
					}
					else
					{
						//Begin re-check the best quote to replace
						int bestQuoteSide = (asOpenOrder.orderSummary[indexOpenOrder].side == BUY_TO_CLOSE_TYPE) ? BID_SIDE : ASK_SIDE;
						if (CheckOurBBOOrderWithCurrentBestquote(asOpenOrder.orderSummary[indexOpenOrder].price,
																 asOpenOrder.orderSummary[indexOpenOrder].symbolIndex,
																 bestQuoteSide,
																 asOpenOrder.orderSummary[indexOpenOrder].shares) == SUCCESS)
						{
							TraceLog(DEBUG_LEVEL, "BBO order has finished sleeping. We will re-check for best quote\n");
							ProcessDisengageSymbol(asOpenOrder.orderSummary[indexOpenOrder].symbol, PM_REQUEST);
						}
						
						//Remove BBO order from collection
						BBOOrderSleepingCollection.collection[i] = -1;
						BBOOrderSleepingCollection.count--;
					}
				}
				else
				{
					//Remove BBO order from collection
					BBOOrderSleepingCollection.collection[i] = -1;
					BBOOrderSleepingCollection.count--;
				}
			}
		}
		else
		{
			BBOOrderSleepingCollection.threadIsCreated = NO;
			
			return NULL;
		}
		
		// Delay in microsecond
		usleep(MONITOR_BBO_ORDER_SLEEPING_DELAY);
	}

	return NULL;
	
}

/****************************************************************************
- Function name:	IsSleeping
- Input:			
- Output:		
- Return:			
- Description:	
- Usage:			
****************************************************************************/
int IsSleeping(int symbolIndex)
{
	int i;
	long numUsec;
	int microSeconds, seconds;
	
	// Get time of day
	hbitime_micros_parts(&seconds, &microSeconds);
	
	for (i = 0; i < asOpenOrder.countOpenOrder; i++)
	{
		if ((asOpenOrder.orderSummary[i].status == orderStatusIndex.Live) && (asOpenOrder.orderSummary[i].symbolIndex == symbolIndex))
		{
			if ((asOpenOrder.orderSummary[i].numSeconds != -1) && (asOpenOrder.orderSummary[i].numMicroseconds != -1))
			{
				numUsec = (seconds - asOpenOrder.orderSummary[i].numSeconds) * 1000000 + (microSeconds - asOpenOrder.orderSummary[i].numMicroseconds);
				
				if (asOpenOrder.orderSummary[i].shares >= maxShareNeedSleep)
				{
					if (numUsec < maxMilisecondToSleep * 1000)
					{
						//TraceLog(DEBUG_LEVEL, "BBO order (%d) is sleeping for %ld ms\n", asOpenOrder.orderSummary[i].clOrdId, numUsec / 1000);
						
						//Add this order to BBO sleeping collection
						if (AddToBBOOrderSleepingCollection(i) == ERROR)
						{
							TraceLog(ERROR_LEVEL, "BBO order sleeping collection is full!\n");
							return SUCCESS;
						}
						
						return SUCCESS;
					}
					else
					{
						TraceLog(DEBUG_LEVEL, "BBO order (%d) is resumed, inside changes after it sleeps for %d ms.\n", asOpenOrder.orderSummary[i].clOrdId, numUsec / 1000);
						return ERROR;
					}
				}
			}
		}
	}
	
	return ERROR;
}

/****************************************************************************
- Function name:	CheckOurBBOOrderWithCurrentBestquote
- Input:			
- Output:		
- Return:			SUCCESS or ERROR
- Description:		Get current best quote (including BBO from away market)
					And then check with our current order's price to see if
					we should cancel and replace the order with new price
- Usage:			-If return value is SUCCESS, it means best quote has changed
					since the time we placed our order, so we will cancel and 
					replace our order with new price
					-If return value is ERROR, it means best quote price is SAME
					as best quote price at the time we placed our order, so we
					will not cancel our order.
****************************************************************************/
int CheckOurBBOOrderWithCurrentBestquote(double orderPrice, int symbolIndex, int side, int orderShares)
{
	/*side: ASK_SIDE or BID_SIDE*/
	
	// Get best quote from our books
	t_TopOrder bestQuote;
	if (orderShares > MAX_ODD_LOT_SHARE)
	{
		pthread_mutex_lock(&updateAskBidMutex[symbolIndex]);
			GetBestOrderRoundLot(&bestQuote, side, symbolIndex);
		pthread_mutex_unlock(&updateAskBidMutex[symbolIndex]);
	}
	else
	{
		pthread_mutex_lock(&updateAskBidMutex[symbolIndex]);
			GetBestOrder(&bestQuote, side, symbolIndex);
		pthread_mutex_unlock(&updateAskBidMutex[symbolIndex]);
	}
	
	if (flt(bestQuote.info.sharePrice, 0.01))
	{
		if (orderShares > MAX_ODD_LOT_SHARE)
		{
			if (side == ASK_SIDE)
				UpdateMinAskRoundLot(symbolIndex, 0, YES, -1);
			else
				UpdateMaxBidRoundLot(symbolIndex, 0, YES, -1);

			pthread_mutex_lock(&updateAskBidMutex[symbolIndex]);
				GetBestOrderRoundLot(&bestQuote, side, symbolIndex);
			pthread_mutex_unlock(&updateAskBidMutex[symbolIndex]);
		}
		else
		{
			if (side == ASK_SIDE)
				UpdateMinAsk(symbolIndex, 0, -1);
			else
				UpdateMaxBid(symbolIndex, 0, -1);
			
			pthread_mutex_lock(&updateAskBidMutex[symbolIndex]);
				GetBestOrder(&bestQuote, side, symbolIndex);
			pthread_mutex_unlock(&updateAskBidMutex[symbolIndex]);
		}
	}
		
	// Get BBO from away market
	t_BestBBO bestBBO;
	bestBBO.price = 0.0;
	GetBestBBOOrder(&bestBBO, side, symbolIndex);
	
	// compare between book quote and BBO from away, take the best
	if (side == ASK_SIDE)
	{
		if (bestBBO.participantId != -1)
		{
			if (fgt(bestQuote.info.sharePrice, 0.01))
			{
				if (fgt(bestQuote.info.sharePrice, bestBBO.price))
				{
					bestQuote.info.sharePrice = bestBBO.price;
					bestQuote.ecnIndicator = bestBBO.participantId;
				}
			}
			else
			{
				bestQuote.info.sharePrice = bestBBO.price;
				bestQuote.ecnIndicator = bestBBO.participantId;
			}
		}
	}
	else
	{
		if (flt(bestQuote.info.sharePrice, bestBBO.price))
		{
			bestQuote.info.sharePrice = bestBBO.price;
			bestQuote.ecnIndicator = bestBBO.participantId;
		}
	}
	
	if ((bestQuote.ecnIndicator == -1) || (flt(bestQuote.info.sharePrice, 0.01)))
	{
		return ERROR;	//Will not cancel current order, since there is no best quote found
	}
	
	// Check if current best quote changes as compare to best quote at the time we placed the order
	if (side == ASK_SIDE)
	{
		if (flt(fabs(orderPrice + 0.01 - bestQuote.info.sharePrice), 0.0001))	//floating point tolerance = 0.0001
			return ERROR;	//Will not cancel current order, since there is no best quote found
	}
	else	//BID_SIDE
	{
		if (flt(fabs(orderPrice - 0.01 - bestQuote.info.sharePrice), 0.0001))	//floating point tolerance = 0.0001
			return ERROR;	//Will not cancel current order, since there is no best quote found
	}
	
	return SUCCESS;	//Will cancel current order, since best quote price has changed
}

/****************************************************************************
- Function name:	GetNewOrder
- Input:			
- Output:		
- Return:			
- Description:	
- Usage:			
****************************************************************************/
int GetNewOrder(t_SendNewOrder *newOrder, t_Position *stuck)
{
	strncpy(newOrder->symbol, stuck->symbol, SYMBOL_LEN);
	newOrder->accountSuffix = stuck->accountSuffix;
	newOrder->symbolIndex = stuck->symbolIndex;
	newOrder->shares = stuck->shares;
	newOrder->price = stuck->price;
	newOrder->maxFloor = 0;
	newOrder->tif = DAY_TYPE;
	newOrder->pegDef = 0;
	
	if (stuck->shares >= 100)
	{
		newOrder->maxFloor = 100;
	}

	if (stuck->side == ASK_SIDE)
	{
		newOrder->side = BUY_TO_CLOSE_TYPE;
	}
	else
	{
		newOrder->side = SELL_TYPE;
	}
	
	newOrder->ECNId = positionCollection[stuck->symbolIndex].ECNIndex;
	
	if(newOrder->ECNId == TYPE_NASDAQ_RASH)
	{
		newOrder->maxFloor = 0;
	}
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	ReplaceOrderAfterSleeping
- Input:			
- Output:		
- Return:			
- Description:	We will replace order when inside goes away or spread opens too wide 
				after sleeping duration (1 second)
- Usage:			
****************************************************************************/
int ReplaceOrderAfterSleeping(t_SendNewOrder *newOrder, t_Position *stuck)
{
	pthread_mutex_lock(&nextOrderID_Mutex);	
	
	//assign new value to OrderNextID
	newOrder->orderId = clientOrderID;

	//Increase next order id by 1
	clientOrderID += 1;
	
	SaveClientOrderID("next_order_id", clientOrderID);
	
	pthread_mutex_unlock(&nextOrderID_Mutex);
	
	int result = ERROR;
	switch (newOrder->ECNId)
	{
		case TYPE_ARCA_DIRECT:
			// Call function to place order at ARCA DIRECT
			if (orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected == CONNECTED)
			{
				result = SendNewOrderToARCA_DIRECTOrder(newOrder);
			}
			else
			{
				result = ERROR;
			}
			break;
		
		case TYPE_NASDAQ_RASH:
			// Place order at RASH
			if (orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected == CONNECTED)
			{
				result = SendNewOrderToNASDAQ_RASHOrder(newOrder);
			}
			else
			{
				result = ERROR;
			}
			break;
	}

	if (result == SUCCESS)
	{
		//ProcessAddNewOrder(newOrder); //Removed by KhanhND, instead, this will be called when send new order to ECN

		t_PositionInfo *positionItem = &positionCollection[newOrder->symbolIndex];
		positionItem->willReplace = YES;
		memcpy(&positionItem->stuck, stuck, sizeof(t_Position));
		
		ShowTradedInfo(newOrder->symbol);
		ShowFeedsDataForSymbol(newOrder->symbolIndex, CQS_SOURCE);
		ShowFeedsDataForSymbol(newOrder->symbolIndex, UQDF_SOURCE);
		return SUCCESS;
	}

	return ERROR;
}

/****************************************************************************
- Function name:	Monitoring_Order_GoAway_Or_Spread_Too_Wide_Theard
- Input:			
- Output:		
- Return:			
- Description:	
- Usage:			
****************************************************************************/
void *Monitoring_Orders_GoAway_Or_Spread_Too_Wide_Thread(void *agrs)
{
	TraceLog(DEBUG_LEVEL, "Start Monitoring_Orders_GoAway_Or_Spread_Too_Wide_Thread\n");

	while(1)
	{
		if(NumOfOrderHasInsideGoAwayOrSpreadTooWide > 0)
		{
			int i, symbolIndex, numThread;
			numThread = 0;
			for (i = 0; i < asOpenOrder.countOpenOrder; i++)
			{
				symbolIndex = asOpenOrder.orderSummary[i].symbolIndex;

				if(positionCollection[symbolIndex].replaceAfterSleeping == YES
					&& positionCollection[symbolIndex].startThread == NO)
				{
					NumOfOrderHasInsideGoAwayOrSpreadTooWide--;
					numThread++;

					t_PositionInfo *positionItem = &positionCollection[symbolIndex];
					positionItem->releaseStuckSleeping = NO;
					positionItem->startThread = YES;
					
					pthread_t sleepThread;
					if(pthread_create(&sleepThread, NULL, (void *)&Monitoring_Sleep_And_Replace_Order_Thread, positionItem) != 0)
					{
						TraceLog(ERROR_LEVEL, "Couldn't create thread for Monitoring Sleep And Replace Order\n");
						
						// Releae all stuck sleeping
						for (i = 0; i < asOpenOrder.countOpenOrder; i++)
						{
							symbolIndex = asOpenOrder.orderSummary[i].symbolIndex;
							positionCollection[symbolIndex].replaceAfterSleeping = NO;
							positionCollection[symbolIndex].releaseStuckSleeping = NO;
							positionCollection[symbolIndex].startThread = NO;
						}
						
						return NULL;
					}
				}
			}

			TraceLog(DEBUG_LEVEL, "Num of thread(%d) to monitor order when inside goes away or spread opens too wide\n", numThread);
		}

		usleep(100000); // sleep 0.1 second
	}

	return NULL;
}

/****************************************************************************
- Function name:	Monitoring_Sleep_And_Replace_Order_Thread
- Input:			
- Output:		
- Return:			
- Description:	
- Usage:			
****************************************************************************/
void *Monitoring_Sleep_And_Replace_Order_Thread(void *agrs)
{
	TraceLog(DEBUG_LEVEL, "Start thread to monitor sleeping and replacing order\n");
	t_PositionInfo *positionItem = (t_PositionInfo *)agrs;
	t_Position *stuck = &positionItem->stuck;
	TraceLog(DEBUG_LEVEL, "Symbol(%s) shares(%d) price(%lf)\n", stuck->symbol, stuck->shares, stuck->price);
	
	int indexOpenOrder = -1;
	int i;
	for (i = 0; i < asOpenOrder.countOpenOrder; i++)
	{
		if ( (asOpenOrder.orderSummary[i].status == orderStatusIndex.Sleeping)
			&& (asOpenOrder.orderSummary[i].symbolIndex == stuck->symbolIndex) )
		{
			indexOpenOrder = i;
			break;
		}
	}
	
	int countTime = 0;
	
	//sleep 1 second: 1000 * 1000ms
	while(countTime < 1000)
	{
		if(positionItem->releaseStuckSleeping == YES 
			|| asCollection.currentStatus.tradingStatus == DISABLED)
		{
			if(indexOpenOrder != -1)
			{
				asOpenOrder.orderSummary[indexOpenOrder].status = orderStatusIndex.CanceledByUser;
			}
			positionItem->replaceAfterSleeping = NO;
			positionItem->releaseStuckSleeping = NO;
			SendPMSleepingSymbolsToAS();
			
			if(asCollection.currentStatus.tradingStatus == DISABLED)
			{
				TraceLog(DEBUG_LEVEL, "Released stuck sleeping due to disable trading\n");
				SendPMStuckStatusAlertToAS(stuck, NO, STUCK_STATUS_REASON_DISABLED_TRADING);
			}
			positionItem->startThread = NO;
			
			return NULL;
		}
		
		countTime++;
		usleep(1000);
	}
	
	positionItem->replaceAfterSleeping = NO;
	positionItem->releaseStuckSleeping = NO;
		
	CheckAndPlaceOrder(stuck, 0, NO, YES, indexOpenOrder);
	
	positionItem->startThread = NO;
	return NULL;
}
