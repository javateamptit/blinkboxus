/****************************************************************************
** Project name:	Equity Arbitrage Application
** File name: 	nasdaq_rash_proc.c
** Description: 	This file contains function definitions that were declared
				in nasdaq_rash_proc.h

** Author: 		Sang Nguyen-Minh
** First created on 26 September 2007
** Last updated on 26 September 2007
****************************************************************************/

/****************************************************************************
** Include files and define several variables
****************************************************************************/
#define _GNU_SOURCE
#include "configuration.h"
#include "socket_util.h"
#include "raw_data_mgmt.h"
#include "nasdaq_rash_proc.h"
#include "nasdaq_rash_data.h"
#include "kernel_algorithm.h"

#include <unistd.h>
#include <math.h>
#include <errno.h>

t_NASDAQ_RASH_Config NASDAQ_RASH_Config;
extern t_OrderInfo LastNasdaqOrder[MAX_STOCK_SYMBOL];
/****************************************************************************
** Function definitions
****************************************************************************/

/****************************************************************************
- Function name:	Initialize_NASDAQ_RASH
- Input:			N/A
- Output:		N/A
- Return:		N/A
- Description:	+ The routine hides the following facts
				  - Set default value for NASDAQ RASH Configuration
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
void Initialize_NASDAQ_RASH(void)
{
	/*
	int incomingSeqNum;
	int outgoingSeqNum;
	int enableTrading;
	int isConnected;

	pthread_mutex_t outgoingSeqNum_Mutex;
	
	int socket;
	int port;
	char ipAddress[80];
	
	char userName[80];
	char password[80];
	
	char buffer[2 * HALF_MAX_BUFFER_LEN];
	
	int remainingBytes;
	int numOfByteReceived;
	int numOfBytesPerRecv;
	int numOfBytesToRecv;
	int startIndex;
	
	char isUnCompletedMsg;
	*/

	// Load incoming sequence number from file
	if (LoadSequenceNumber("nasdaq_rash_seqs", NASDAQ_RASH_INDEX, &NASDAQ_RASH_Config) == ERROR)
	{
		TraceLog(ERROR_LEVEL, "Cannot load nasdaq_rash_seqs file\n");

		return;
	}

	NASDAQ_RASH_Config.enableTrading = -1;
	NASDAQ_RASH_Config.isConnected = DISCONNECTED;

	pthread_mutex_init(&NASDAQ_RASH_Config.outgoingSeqNum_Mutex, NULL);

	NASDAQ_RASH_Config.socket = -1;
	NASDAQ_RASH_Config.port = -1;

	memset(NASDAQ_RASH_Config.ipAddress, 0, MAX_LINE_LEN);
	memset(NASDAQ_RASH_Config.userName, 0, MAX_LINE_LEN);
	memset(NASDAQ_RASH_Config.password, 0, MAX_LINE_LEN);


	memset(NASDAQ_RASH_Config.buffer, 0, 2 * HALF_MAX_BUFFER_LEN);
	NASDAQ_RASH_Config.numOfByteReceived = 0;
	NASDAQ_RASH_Config.isUnCompletedMsg = NO;
	NASDAQ_RASH_Config.startIndex = 0;
}

/****************************************************************************
- Function name:	StartUp_NASDAQ_RASH_Module
- Input:			+ threadArgs
- Output:		N/A
- Return:		Success or Failure
- Description:	+ The routine hides the following facts
				+ There are no preconditions guaranteed to the routine
				+ The routine guarantees that the return value will 
						have a value of either
				  - Success
					or
				  - Failure
- Usage:			N/A
****************************************************************************/
void *StartUp_NASDAQ_RASH_Module(void *threadArgs)
{
	SetKernelAlgorithm("[RASH_RECEIVE_PROCESS_MSGS]");
	
	// Initialize several parameters
	memset(NASDAQ_RASH_Config.buffer, 0, 2 * HALF_MAX_BUFFER_LEN);
	NASDAQ_RASH_Config.numOfByteReceived = 0;
	NASDAQ_RASH_Config.isUnCompletedMsg = NO;
	NASDAQ_RASH_Config.startIndex = 0;
	
	// Load incoming sequence number from file
	if (LoadSequenceNumber("nasdaq_rash_seqs", NASDAQ_RASH_INDEX, &NASDAQ_RASH_Config) == ERROR)
	{
		TraceLog(ERROR_LEVEL, "Cannot load nasdaq_rash_seqs file\n");

		return NULL;
	}
	
	TraceLog(DEBUG_LEVEL, "Trying connect to NASDAQ RASH Server...\n");

	// Connect to server
	NASDAQ_RASH_Config.socket = Connect2Server(NASDAQ_RASH_Config.ipAddress, NASDAQ_RASH_Config.port, "NASDAQ RASH");
	
	if (NASDAQ_RASH_Config.socket == -1)
	{
		TraceLog(ERROR_LEVEL, "Cannot connect to NASDAQ RASH Server\n");
		orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected = DISCONNECTED;
		
		asCollection.orderConnection[NASDAQ_RASH_INDEX].status = DISCONNECTED;
		return NULL;
	}
	
	// Talk to server
	TalkTo_NASDAQ_RASH_Server();
	
	// Close connection and exit the module
	if (NASDAQ_RASH_Config.socket > 0)
	{
		close(NASDAQ_RASH_Config.socket);
		NASDAQ_RASH_Config.socket = -1;
	}
	
	orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected = DISCONNECTED;
	orderStatusMgmt[NASDAQ_RASH_INDEX].shouldConnect = NO;
	
	asCollection.orderConnection[NASDAQ_RASH_INDEX].status = DISCONNECTED;
	
	SendPMDisconnectedBookOrderAlertToAS(SERVICE_NASDAQ_RASH);
	CheckAndSendAlertDisableTrading(2);
	
	TraceLog(DEBUG_LEVEL, "Connection to NASDAQ RASH Server was disconnected\n");
	
	return NULL;
}

/****************************************************************************
- Function name:	TalkTo_NASDAQ_RASH_Server
- Input:			N/A
- Output:		N/A
- Return:		N/A
- Description:	+ The routine hides the following facts
				  - Receive message from server and send response appropriately
				  - When the routine returns, meaning has some problems with 
				  socket
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
void TalkTo_NASDAQ_RASH_Server(void)
{
	t_DataBlock dataBlock;
	int msgType;
	char fieldToken[15];
	int orderID;
	int isManualOrder;
	
	TraceLog(DEBUG_LEVEL, "Talking to NASDAQ RASH Server...\n");
	
	// Send Logon message to server
	if (BuildAndSend_NASDAQ_RASH_LogonRequest() == ERROR)
	{
		return;
	}
	
	while( ((msgType = Get_NASDAQ_RASH_CompletedMessage()) != ERROR) && (orderStatusMgmt[NASDAQ_RASH_INDEX].shouldConnect == YES) )
	{
		/*
		Base on message type we will call appropriate functions
		*/
		
		memset (&dataBlock, 0, sizeof(t_DataBlock));
		switch (msgType)
		{
			case NASDAQ_RASH_ORDER_ACCEPTED:
			
				// Call function to check cross trade here
				
				// Update data block collection
				dataBlock.msgLen = NASDAQ_RASH_ORDER_ACCEPTED_LEN;
				
				memcpy(dataBlock.msgContent, 
						&NASDAQ_RASH_Config.buffer[NASDAQ_RASH_Config.startIndex - NASDAQ_RASH_ORDER_ACCEPTED_LEN], 
						dataBlock.msgLen);
						
				// Get Order ID to check if it's manual order
				// Prepend Token with last two digits of account number
				memcpy(fieldToken, &dataBlock.msgContent[12], 12);
				fieldToken[12] = 0;
				
				orderID = atoi(fieldToken);
				isManualOrder = 0;
				if (orderID < 500000) 
				{
					isManualOrder = 1;
					
					// Add suffix
					dataBlock.addContent[ACCOUNT_SUFFIX_INDEX] = accountMapping[orderID % MAX_ORDER_PLACEMENT];
				}
				else 
				{
					// Add suffix
					int indexOpenOrder = GetOrderIdIndex(orderID);
					if (indexOpenOrder != -1) {
						dataBlock.addContent[ACCOUNT_SUFFIX_INDEX] = asOpenOrder.orderSummary[indexOpenOrder].accountSuffix;
					}
				}
				
				Add_NASDAQ_RASH_DataBlockToCollection(&dataBlock, isManualOrder);
				
				// Save incoming sequence number to file
				SaveNASDAQ_RASH_SeqNumToFile();
				SendNASDAQ_RASH_SeqToAS();

				ProcessAcceptedOrderOfRASH(dataBlock, YES);
				
				break;
				
			case NASDAQ_RASH_ORDER_EXECUTED:
			
				// Call function to check cross trade here
				
				// Update data block collection
				dataBlock.msgLen = NASDAQ_RASH_ORDER_EXECUTED_LEN;
				
				memcpy(dataBlock.msgContent, 
						&NASDAQ_RASH_Config.buffer[NASDAQ_RASH_Config.startIndex - NASDAQ_RASH_ORDER_EXECUTED_LEN], 
						dataBlock.msgLen);
				
				// Get Order ID to check if it's manual order
				// Prepend Token with last two digits of account number
				memcpy(fieldToken, &dataBlock.msgContent[12], 12);
				fieldToken[12] = 0;
				
				orderID = atoi(fieldToken);
				isManualOrder = 0;
				if (orderID < 500000) 
				{
					isManualOrder = 1;
					
					// Add suffix
					dataBlock.addContent[ACCOUNT_SUFFIX_INDEX] = accountMapping[orderID % MAX_ORDER_PLACEMENT];
				}
				else 
				{
					// Add suffix
					int indexOpenOrder = GetOrderIdIndex(orderID);
					if (indexOpenOrder != -1) {
						dataBlock.addContent[ACCOUNT_SUFFIX_INDEX] = asOpenOrder.orderSummary[indexOpenOrder].accountSuffix;
					}
				}
	
				Add_NASDAQ_RASH_DataBlockToCollection(&dataBlock, isManualOrder);
				
				// Save incoming sequence number to file
				SaveNASDAQ_RASH_SeqNumToFile();
				SendNASDAQ_RASH_SeqToAS();

				ProcessExecutedOrderOfRASH(dataBlock);
				
				break;
				
			case NASDAQ_RASH_ORDER_CANCELED:
			
				// Call function to check cross trade here
				
				// Update data block collection
				dataBlock.msgLen = NASDAQ_RASH_ORDER_CANCELED_LEN;
				
				memcpy(dataBlock.msgContent, 
						&NASDAQ_RASH_Config.buffer[NASDAQ_RASH_Config.startIndex - NASDAQ_RASH_ORDER_CANCELED_LEN], 
						dataBlock.msgLen);
				
				//Get order id to check if it's manual order
				// Prepend Token with last two digits of account number
				memcpy(fieldToken, &dataBlock.msgContent[12], 12);
				fieldToken[12] = 0;
				
				orderID = atoi(fieldToken);
				isManualOrder = 0;
				if (orderID < 500000) 
				{
					isManualOrder = 1;
					
					// Add suffix
					dataBlock.addContent[ACCOUNT_SUFFIX_INDEX] = accountMapping[orderID % MAX_ORDER_PLACEMENT];
				}
				else 
				{
					// Add suffix
					int indexOpenOrder = GetOrderIdIndex(orderID);
					if (indexOpenOrder != -1) {
						dataBlock.addContent[ACCOUNT_SUFFIX_INDEX] = asOpenOrder.orderSummary[indexOpenOrder].accountSuffix;
					}
				}
				
				Add_NASDAQ_RASH_DataBlockToCollection(&dataBlock, isManualOrder);
				
				// Save incoming sequence number to file
				SaveNASDAQ_RASH_SeqNumToFile();
				SendNASDAQ_RASH_SeqToAS();
				
				ProcessCanceledOrderOfRASH(dataBlock);
				
				break;
			
			case NASDAQ_RASH_ORDER_REJECTED:
				
				// Call function to check cross trade here
				
				// Update data block collection
				dataBlock.msgLen = NASDAQ_RASH_ORDER_REJECTED_LEN;
				
				memcpy(dataBlock.msgContent, 
						&NASDAQ_RASH_Config.buffer[NASDAQ_RASH_Config.startIndex - NASDAQ_RASH_ORDER_REJECTED_LEN], 
						dataBlock.msgLen);
			
				//Get order id to check if it's manual order
				// Prepend Token with last two digits of account number
				memcpy(fieldToken, &dataBlock.msgContent[12], 12);
				fieldToken[12] = 0;
				
				orderID = atoi(fieldToken);
				isManualOrder = 0;
				if (orderID < 500000) 
				{
					isManualOrder = 1;
					
					// Add suffix
					dataBlock.addContent[ACCOUNT_SUFFIX_INDEX] = accountMapping[orderID % MAX_ORDER_PLACEMENT];
				}
				else 
				{
					// Add suffix
					int indexOpenOrder = GetOrderIdIndex(orderID);
					if (indexOpenOrder != -1) {
						dataBlock.addContent[ACCOUNT_SUFFIX_INDEX] = asOpenOrder.orderSummary[indexOpenOrder].accountSuffix;
					}
				}
				
				Add_NASDAQ_RASH_DataBlockToCollection(&dataBlock, isManualOrder);
				
				// Save incoming sequence number to file
				SaveNASDAQ_RASH_SeqNumToFile();
				SendNASDAQ_RASH_SeqToAS();
				ProcessRejectedOrderOfRASH(dataBlock);
				
				break;
			
			case NASDAQ_RASH_BROKEN_TRADE:
				
				// Call function to check cross trade here
				
				// Update data block collection
				dataBlock.msgLen = NASDAQ_RASH_BROKEN_TRADE_LEN;
				
				//Get order id to check if it's manual order
				// Prepend Token with last two digits of account number
				memcpy(fieldToken, &dataBlock.msgContent[12], 12);
				fieldToken[12] = 0;
				
				orderID = atoi(fieldToken);
				isManualOrder = 0;
				memcpy(dataBlock.msgContent, 
						&NASDAQ_RASH_Config.buffer[NASDAQ_RASH_Config.startIndex - NASDAQ_RASH_BROKEN_TRADE_LEN], 
						dataBlock.msgLen);
						
				if (orderID < 500000) 
				{	
					// Add suffix
					dataBlock.addContent[ACCOUNT_SUFFIX_INDEX] = accountMapping[orderID % MAX_ORDER_PLACEMENT];
				}
				else 
				{
					// Add suffix
					int indexOpenOrder = GetOrderIdIndex(orderID);
					if (indexOpenOrder != -1) {
						dataBlock.addContent[ACCOUNT_SUFFIX_INDEX] = asOpenOrder.orderSummary[indexOpenOrder].accountSuffix;
					}
				}
				
				/*Currently we don't process broken trade message of RASH
				so we dont care if it's manual order or PM's order*/
				Add_NASDAQ_RASH_DataBlockToCollection(&dataBlock, 0);
				
				// Save incoming sequence number to file
				SaveNASDAQ_RASH_SeqNumToFile();
				SendNASDAQ_RASH_SeqToAS();
				
				break;
			
			case NASDAQ_RASH_SYSTEM_EVENT:
				break;
				
			case NASDAQ_RASH_HEARTBEAT:
				if (BuildAndSend_NASDAQ_RASH_Heartbeat() == ERROR)
				{
					return;
				}
				break;
			
			case NASDAQ_RASH_DEBUG:
				break;
				
			case NASDAQ_RASH_LOGIN_ACCEPTED_RETURN:
				// Update flag to indicate that we connected to server successfully
				
				TraceLog(DEBUG_LEVEL, "Received logon accepted message from NASDAQ RASH Server\n");
				
				orderStatusMgmt[NASDAQ_RASH_INDEX].isOrderConnected = CONNECTED;
				asCollection.orderConnection[NASDAQ_RASH_INDEX].status = CONNECTED;
				break;
			
			case NASDAQ_RASH_LOGIN_REJECTED_RETURN:
			
				// Update flag to indicate that we cannot connect to server
				TraceLog(DEBUG_LEVEL, "Received logon rejected message from NASDAQ RASH Server\n");
				
				return;				
			default:
				break;
		}
	}
}

/****************************************************************************
- Function name:	BuildAndSend_NASDAQ_RASH_LogonRequest
- Input:			N/A
- Output:		N/A
- Return:		Success or Failure
- Description:	+ The routine hides the following facts
				  - Build logon message with using some global variables
				  - Calculate message length and return through return value
				+ There are preconditions guaranteed to the routine
				  - 	message must be allocated before
				+ The routine guarantees that the return value will 
						have a value of either
				  - Success
					or 
				  - Failure
- Usage:			N/A
****************************************************************************/
int BuildAndSend_NASDAQ_RASH_LogonRequest(void)
{
	char message[38];
			
	int currentIndex;
	
	// len is used to store the length of various strings
	int len = 0;
	
	/*
	Both fields username and password are insensitive, 
	and must be filled with spaces on the right
	*/
	memset(message, ' ', 38);
	
	// First character is the message type. 'L' means login request
	message[0] = 'L';
	
	/*
	Next is the 6 bytes which store the username
	We assume the length of the username is OK
	*/
	len = strlen(NASDAQ_RASH_Config.userName);

	for (currentIndex = 0; currentIndex < len; currentIndex++)	
	{
		if (currentIndex < 6)
		{
			message[1 + currentIndex] = NASDAQ_RASH_Config.userName[currentIndex];
		}
	}
	
	/*
	Next is the 10 bytes which store the password
	We assume the length of the password is OK
	*/
	len = strlen(NASDAQ_RASH_Config.password);

	for (currentIndex = 0; currentIndex < len; currentIndex++)
	{
		if (currentIndex < 10)
		{
			message[7 + currentIndex] = NASDAQ_RASH_Config.password[currentIndex];
		}
	}

	// All zero will do me good
	memset(&message[27], ' ', 10);

	char temp[10];

	sprintf(temp, "%10d", NASDAQ_RASH_Config.incomingSeqNum);
	
	memcpy(&message[27], temp, 10);	

	// Terminating linefeed
	message[37] = 10;
	
	return Send_NASDAQ_RASH_Msg(message, 38);
}

/****************************************************************************
- Function name:	BuildAndSendRASHManualOrder
- Input:			+ newOrder
- Output:		N/A
- Return:		Success or Failure
- Description:	Order format for manual order. Must as the same as AS!
- Usage:			N/A
****************************************************************************/
int BuildAndSendRASHManualOrder (t_ManualOrder *tmpOrder)
{
	/* Convert into RASH order format */
	t_NASDAQ_RASH_NewOrder newOrder;
	
	//orderToken
	char orderToken[16];
	
	/*	code added for safety	*/
	memset(orderToken, 0, 16);
	sprintf(orderToken, "%d", tmpOrder->orderId);

	int len = strlen(orderToken);

	memset(&orderToken[len], ' ', 14 - len);
	orderToken[14] = 0;

	newOrder.orderToken = orderToken;

	//side
	newOrder.buyLaunch = 0;
	if (tmpOrder->side == BUY_TO_CLOSE_TYPE)
	{
		newOrder.side = 'B';
		newOrder.buyLaunch = BUY_TO_CLOSE;
	}
	else if (tmpOrder->side == SELL_TYPE)
	{
		newOrder.side = 'S';
	}
	else if (tmpOrder->side == SHORT_SELL_TYPE)
	{
		newOrder.side = 'S';
	}
	else 
	{
		newOrder.side = 'B';
		newOrder.buyLaunch = BUY_TO_OPEN;
	}
	
	//share
	newOrder.share = tmpOrder->shares;
	
	//stock
	char symbol[SYMBOL_LEN] = "\0";
	strncpy(symbol, tmpOrder->symbol, SYMBOL_LEN);
	newOrder.stock = symbol;
	
	//price
	newOrder.price = tmpOrder->price;

	// pegDef
	newOrder.pegDef = tmpOrder->pegDef;
	
	//timeInForce
	if(tmpOrder->tif == 0 || tmpOrder->tif == 1)
	{
		newOrder.timeInForce = 0;
	}
	else
	{
		newOrder.timeInForce = 99999;
	}

	//firm
	newOrder.firm = MPID_NASDAQ_RASH;

	//display
	if (tmpOrder->maxFloor == 0)
	{
		newOrder.display = 'N';
	}
	else
	{
		newOrder.display = 'Y';
	}

	//Max Floor
	newOrder.maxFloor = tmpOrder->maxFloor;
	
	
	/* Build And Send Message */
	char message[NASDAQ_RASH_NEW_ORDER_MSG_LEN];
	
	memset(message, ' ', NASDAQ_RASH_NEW_ORDER_MSG_LEN);
	
	// Unsequence data packet
	message[0] = 'U';
	
	// Order message type
	message[1] = 'O';
	
	// Order token
	//strncpy(&message[2], newOrder.orderToken, 14);
	// Order token (2 digit account + orderID --> 14 char)
	message[2] = TradingAccount.RASHAccount[tmpOrder->accountSuffix][0];
	message[3] = TradingAccount.RASHAccount[tmpOrder->accountSuffix][1];
	
	strncpy(&message[4], newOrder.orderToken, 12);
	
	// Buy/Sell indicator	
	message[16] = newOrder.side;
	
	// Share		
	Lrc_itoaf(newOrder.share, '0', 6, &message[17]);
	
	// Stock
	memset(&message[23], 32, SYMBOL_LEN);
	strncpy(&message[23], newOrder.stock, strnlen(newOrder.stock, SYMBOL_LEN));
	
	// Price
	Lrc_itoaf(lrint(newOrder.price * 10000), '0', 10, &message[31]);

	// Time in Force
	Lrc_itoaf(newOrder.timeInForce, '0', 5, &message[41]);

	// Firm
	strncpy(&message[46], newOrder.firm, 4);

	// Display
	message[50] = newOrder.display;

	/***************************************************************
	- MinQty: Minimum Fill Amount Allowed. 
	If a non-zero value is put in this field, TIF must be IOC (or 0)
	***************************************************************/
	Lrc_itoaf(0, '0', 6, &message[51]);

	/***************************************************************
	- Max Floor: Shares to Display. If zero this field will default 
	to the order qty. Use the display field to specify a hidden order.
	***************************************************************/
	if ((newOrder.maxFloor > 0) && (newOrder.maxFloor < newOrder.share))
	{
		Lrc_itoaf(newOrder.maxFloor, '0', 6, &message[57]);

		TraceLog(DEBUG_LEVEL, "maxFloor = %d\n", newOrder.maxFloor);
	}
	else
	{
		Lrc_itoaf(0, '0', 6, &message[57]);
	}
	
	/***************************************************************
	- Peg Type: N ? No Peg
				P ? Market
				R ? Primary
	***************************************************************/
	message[63] = 'N';

	if (flt(newOrder.price, 0.0001))
	{
		if (fgt(newOrder.pegDef, 0.0))
		{
			message[63] = 'R';

			TraceLog(DEBUG_LEVEL, "Peg Difference = %lf\n", newOrder.pegDef);
		}
		else
		{
			message[63] = 'P';
		}		
	}

	/***************************************************************
	- Peg Difference Sign:	+
							-
	If peg type is set to ?N?, specify ?+?
	***************************************************************/
	message[64] = '+';

	if ((message[63] == 'R') && (newOrder.side != 'B'))
	{
		message[64] = '-';		
	}
	
	/***************************************************************
	- Peg Difference: Amount; 6.4 (implied decimal). If peg type is 
	set to ?N?, specify 0
	***************************************************************/
	Lrc_itoaf(0, '0', 10, &message[65]);

	if (message[63] == 'R')
	{
		Lrc_itoaf(lrint(newOrder.pegDef * 10000), '0', 10, &message[65]);
	}
	
	/***************************************************************
	- Discretion Price: Discretion Price for Discretionary Order. 
	If set to 0, then this order does not have discretion.
	***************************************************************/
	Lrc_itoaf(0, '0', 10, &message[75]);
	
	/***************************************************************
	- Discretion Peg Type:	N ? No Peg
							P ? Market
							R ? Primary
	***************************************************************/
	message[85] = 'N';

	/***************************************************************
	- Discretion Peg Difference Sign:	+
										-
	If peg type is set to ?N?, specify ?+?
	***************************************************************/
	message[86] = '+';
	
	/***************************************************************
	- Discretion Peg Difference: Amount; 6.4 (implied decimal). 
	If peg type is set to ?N?, specify 0
	***************************************************************/ 
	Lrc_itoaf(0, '0', 10, &message[87]);

	/***************************************************************
	- Capacity/Rule 80A Indicator: Capacity Code
	***************************************************************/
	message[97] = 'P';

	/***************************************************************
	- Random Reserve: Shares to do random reserve with
	***************************************************************/
	Lrc_itoaf(0, '0', 6, &message[98]);

	/***************************************************************
	- Route Dest / Exec Broker: Target ID (INET, DOTN, DOTA, DOTM, 
	DOTP, STGY and SCAN)
	***************************************************************/
	memcpy(&message[104], tmpOrder->routeDest, 4);

	/***************************************************************
	- Cust / Terminal ID / Sender SubID: Client Initiated; Pass-thru. 
	Must be left justified and may not start with a space.
	***************************************************************/

	// Terminating linefeed
	message[140] = 10;
	
	// Send Enter Order Message
	
	int retValue = send(NASDAQ_RASH_Config.socket, message, NASDAQ_RASH_NEW_ORDER_MSG_LEN, 0);
	
	if (retValue != NASDAQ_RASH_NEW_ORDER_MSG_LEN)
	{
		return ERROR;
	}
	
	// Register account suffix
	accountMapping[tmpOrder->orderId % MAX_ORDER_PLACEMENT] = tmpOrder->accountSuffix;

	t_DataBlock dataBlock;
	memset (&dataBlock, 0, sizeof(t_DataBlock));
	
	dataBlock.msgLen = NASDAQ_RASH_NEW_ORDER_MSG_LEN;
	
	memcpy(dataBlock.msgContent, message, NASDAQ_RASH_NEW_ORDER_MSG_LEN);
	
	// Add Buy Launch
	dataBlock.addContent[BUY_LAUNCH_INDEX] = newOrder.buyLaunch;	
	
	// Add suffix
	dataBlock.addContent[ACCOUNT_SUFFIX_INDEX] = tmpOrder->accountSuffix;
	
	//printf("Testing: Send RASH Manual Order with account = %d\n", tmpOrder->accountSuffix);
	
	//Add 1 to indicate it's manual order
	Add_NASDAQ_RASH_DataBlockToCollection(&dataBlock, 1);

	return SUCCESS;

}

/****************************************************************************
- Function name:	BuildAndSend_NASDAQ_RASH_OrderMsg
- Input:			+ newOrder
- Output:		N/A
- Return:		Success or Failure
- Description:	+ The routine hides the following facts
				  - Build new order message with using some global variables
				+ There are preconditions guaranteed to the routine
				  - 	message must be allocated before
				+ The routine guarantees that the return value will 
						have a value of either
				  - Success
					or 
				  - Failure 
- Usage:			N/A
****************************************************************************/
int BuildAndSend_NASDAQ_RASH_OrderMsg(t_NASDAQ_RASH_NewOrder *newOrder, t_SendNewOrder *originalOrder)
{
	char message[NASDAQ_RASH_NEW_ORDER_MSG_LEN];
	
	memset(message, ' ', NASDAQ_RASH_NEW_ORDER_MSG_LEN);
	
	// Unsequence data packet
	message[0] = 'U';
	
	// Order message type
	message[1] = 'O';
		
	// Order token
	//strncpy(&message[2], newOrder->orderToken, 14);
	// Order token (2 digit account + orderID --> 14 char)
	message[2] = TradingAccount.RASHAccount[originalOrder->accountSuffix][0];
	message[3] = TradingAccount.RASHAccount[originalOrder->accountSuffix][1];
	
	strncpy(&message[4], newOrder->orderToken, 12);
	
	// Buy/Sell indicator	
	message[16] = newOrder->side;
	
	// Share		
	Lrc_itoaf(newOrder->share, '0', 6, &message[17]);
	
	// Stock
	memset(&message[23], 32, SYMBOL_LEN);
	strncpy(&message[23], newOrder->stock, strnlen(newOrder->stock, SYMBOL_LEN));
	
	// Price
	Lrc_itoaf(lrint(newOrder->price * 10000), '0', 10, &message[31]);

	// Time in Force
	Lrc_itoaf(newOrder->timeInForce, '0', 5, &message[41]);

	// Firm
	strncpy(&message[46], newOrder->firm, 4);

	// Display
	message[50] = newOrder->display;

	/***************************************************************
	- MinQty: Minimum Fill Amount Allowed. 
	If a non-zero value is put in this field, TIF must be IOC (or 0)
	***************************************************************/
	Lrc_itoaf(0, '0', 6, &message[51]);

	/***************************************************************
	- Max Floor: Shares to Display. If zero this field will default 
	to the order qty. Use the display field to specify a hidden order.
	***************************************************************/
	if ((newOrder->maxFloor > 0) && (newOrder->maxFloor < newOrder->share))
	{
		Lrc_itoaf(newOrder->maxFloor, '0', 6, &message[57]);

		TraceLog(DEBUG_LEVEL, "maxFloor = %d\n", newOrder->maxFloor);
	}
	else
	{
		Lrc_itoaf(0, '0', 6, &message[57]);
	}
	
	/***************************************************************
	- Peg Type: N � No Peg
				P � Market
				R � Primary
	***************************************************************/
	message[63] = 'N';

	if (flt(newOrder->price, 0.0001))
	{
		if (fgt(newOrder->pegDef, 0.0))
		{
			message[63] = 'R';

			TraceLog(DEBUG_LEVEL, "Peg Difference = %lf\n", newOrder->pegDef);
		}
		else
		{
			message[63] = 'P';
		}		
	}

	/***************************************************************
	- Peg Difference Sign:	+
							-
	If peg type is set to �N�, specify �+�
	***************************************************************/
	message[64] = '+';

	if ((message[63] == 'R') && (newOrder->side != 'B'))
	{
		message[64] = '-';		
	}
	
	/***************************************************************
	- Peg Difference: Amount; 6.4 (implied decimal). If peg type is 
	set to �N�, specify 0
	***************************************************************/
	Lrc_itoaf(0, '0', 10, &message[65]);

	if (message[63] == 'R')
	{
		Lrc_itoaf(lrint(newOrder->pegDef * 10000), '0', 10, &message[65]);
	}
	
	/***************************************************************
	- Discretion Price: Discretion Price for Discretionary Order. 
	If set to 0, then this order does not have discretion.
	***************************************************************/
	Lrc_itoaf(0, '0', 10, &message[75]);
	
	/***************************************************************
	- Discretion Peg Type:	N � No Peg
							P � Market
							R � Primary
	***************************************************************/
	message[85] = 'N';

	/***************************************************************
	- Discretion Peg Difference Sign:	+
										-
	If peg type is set to �N�, specify �+�
	***************************************************************/
	message[86] = '+';
	
	/***************************************************************
	- Discretion Peg Difference: Amount; 6.4 (implied decimal). 
	If peg type is set to �N�, specify 0
	***************************************************************/ 
	Lrc_itoaf(0, '0', 10, &message[87]);

	/***************************************************************
	- Capacity/Rule 80A Indicator: Capacity Code
	***************************************************************/
	message[97] = 'P';

	/***************************************************************
	- Random Reserve: Shares to do random reserve with
	***************************************************************/
	Lrc_itoaf(0, '0', 6, &message[98]);

	/***************************************************************
	- Route Dest / Exec Broker: Target ID (INET, DOTN, DOTA, DOTM, 
	DOTP, STGY and SCAN)
	***************************************************************/
	//memcpy(&message[102], "MOPP", 4);
	memcpy(&message[104], "SCAN", 4);

	/***************************************************************
	- Cust / Terminal ID / Sender SubID: Client Initiated; Pass-thru. 
	Must be left justified and may not start with a space.
	***************************************************************/

	// Terminating linefeed
	message[140] = 10;
	
	// Send Enter Order Message
	
	int retValue = send(NASDAQ_RASH_Config.socket, message, NASDAQ_RASH_NEW_ORDER_MSG_LEN, 0);
	
	if (retValue != NASDAQ_RASH_NEW_ORDER_MSG_LEN)
	{
		return ERROR;
	}
	
	if (newOrder->display == 'N')
	{
		LastNasdaqOrder[originalOrder->symbolIndex].side = -1;
	}
	else
	{
		// Add our order to the list, so we can check to ignore our order from the book.
		LastNasdaqOrder[originalOrder->symbolIndex].price = newOrder->price;
		LastNasdaqOrder[originalOrder->symbolIndex].shares = newOrder->share>100?100:newOrder->share;	//Our order uses MaxFloor = 100!
		LastNasdaqOrder[originalOrder->symbolIndex].orderID = originalOrder->orderId;
		LastNasdaqOrder[originalOrder->symbolIndex].side = newOrder->side == 'B'?BID_SIDE:ASK_SIDE;
	}
	
	ProcessAddNewOrder(originalOrder); //Added by KhanhND
	
	t_DataBlock dataBlock;
	memset (&dataBlock, 0, sizeof(t_DataBlock));
	
	dataBlock.msgLen = NASDAQ_RASH_NEW_ORDER_MSG_LEN;
	
	memcpy(dataBlock.msgContent, message, NASDAQ_RASH_NEW_ORDER_MSG_LEN);

	// Add Buy Launch
	dataBlock.addContent[BUY_LAUNCH_INDEX] = newOrder->buyLaunch;
	
	//Add Cross ID
	memcpy(&dataBlock.addContent[CROSS_ID_INDEX], &(CrossIDCollection[originalOrder->symbolIndex]), 4);
	
	// Add suffix
	dataBlock.addContent[ACCOUNT_SUFFIX_INDEX] = originalOrder->accountSuffix;
	
	//printf("Testing: Send RASH Order with account = %d\n", originalOrder->accountSuffix);
	
	//0 indicates it's not manual order
	Add_NASDAQ_RASH_DataBlockToCollection(&dataBlock, 0);

	return SUCCESS;

}

/****************************************************************************
- Function name:	BuildAndSend_NASDAQ_RASH_CancelMsg
- Input:			+ cancelOrder
- Output:		N/A
- Return:		Success or Failure
- Description:	+ The routine hides the following facts
				  - Build cancel order message with using some global variables
				+ There are preconditions guaranteed to the routine
				  - 	message must be allocated before
				+ The routine guarantees that the return value will 
						have a value of either
				  - Success
					or 
				  - Failure 
- Usage:			N/A
****************************************************************************/
int BuildAndSend_NASDAQ_RASH_CancelMsg(t_NASDAQ_RASH_CancelOrder *cancelOrder)
{
	int index;		
	
	char message[CANCEL_MSG_LEN];
	memset(message, ' ', CANCEL_MSG_LEN);	
	
	// Unsequence data packet
	message[0] = 'U';
	
	// Order message type
	message[1] = 'X';
	
	// Order token
	//memcpy(&message[2], cancelOrder->orderToken, 14);
	// Order token (2 digit account + orderID --> 14 char)
	message[2] = TradingAccount.RASHAccount[cancelOrder->accountSuffix][0];
	message[3] = TradingAccount.RASHAccount[cancelOrder->accountSuffix][1];
	
	strncpy(&message[4], cancelOrder->orderToken, 12);
	
	for (index = 2; index < 16; index++)
	{
		if (message[index] == 0)
		{
			message[index] = ' ';		
		}		
	}
	
	// Share
	char temp[10];	
	
	memset(temp, 0, 10);
	
	sprintf(temp, "%6d", 0);
	
	for (index = 0; index < 6; index++)
	{
		if (temp[index] == ' ')
		{
			temp[index] = '0';		
		}		
	}
	
	memcpy(&message[16], temp, 6);	
		
	// Terminating linefeed
	message[22] = 10;
	
	// Send Cancel Order Request Message
	t_DataBlock dataBlock;
	memset (&dataBlock, 0, sizeof(t_DataBlock));
	
	int retValue = send(NASDAQ_RASH_Config.socket, message, CANCEL_MSG_LEN, 0);
	
	GetTimeFormatTime(dataBlock.addContent);
	if (retValue != CANCEL_MSG_LEN)
	{
		return ERROR;
	}
		
	dataBlock.msgLen = CANCEL_MSG_LEN;
	
	memcpy(dataBlock.msgContent, message, CANCEL_MSG_LEN);
	
	// Update Block Length
	dataBlock.blockLen = 4 + 4 + CANCEL_MSG_LEN + 4 + MAX_ADD_INFO_LEN;
	
	// Update additional content 
	dataBlock.addLen = MAX_ADD_INFO_LEN;
	
	// Add suffix
	dataBlock.addContent[ACCOUNT_SUFFIX_INDEX] = cancelOrder->accountSuffix;
	
	//printf("Testing: Send RASH Cancel Request with account = %d\n", cancelOrder->accountSuffix);
	
	// Add data block to collection
	AddDataBlockToCollection(&dataBlock, &pmRawDataMgmt[NASDAQ_RASH_INDEX]);
	
	// Save data block to file
	SaveDataBlockToFile(&pmRawDataMgmt[NASDAQ_RASH_INDEX]);

	return SUCCESS;
}

/****************************************************************************
- Function name:	BuildAndSend_NASDAQ_RASH_ManualCancelMsg
- Input:			+ cancelOrder
- Output:		
- Return:			Success or Failure
- Description:	 
- Usage:			N/A
****************************************************************************/
int BuildAndSend_NASDAQ_RASH_ManualCancelMsg(t_ManualCancelOrder *cancelOrder)
{
	int index;		
	
	char message[CANCEL_MSG_LEN];
	memset(message, ' ', CANCEL_MSG_LEN);	
	
	// Unsequence data packet
	message[0] = 'U';
	
	// Order message type
	message[1] = 'X';
	
	// Get the account index
	int accountIndex = accountMapping[atoi(cancelOrder->orderToken) % MAX_ORDER_PLACEMENT];
	
	// Order token
	//memcpy(&message[2], cancelOrder->orderToken, 14);
	// Order token (2 digit account + orderID --> 14 char)
	message[2] = TradingAccount.RASHAccount[accountIndex][0];
	message[3] = TradingAccount.RASHAccount[accountIndex][1];
	
	strncpy(&message[4], cancelOrder->orderToken, 12);
	
	for (index = 2; index < 16; index++)
	{
		if (message[index] == 0)
		{
			message[index] = ' ';		
		}		
	}
	
	// Share
	sprintf(&message[16], "%s", "000000");
		
	// Terminating linefeed
	message[22] = 10;
	
	// Send Cancel Order Request Message
	t_DataBlock dataBlock;
	memset (&dataBlock, 0, sizeof(t_DataBlock));
	
	int retValue = send(NASDAQ_RASH_Config.socket, message, CANCEL_MSG_LEN, 0);
	
	GetTimeFormatTime(dataBlock.addContent);
	
	if (retValue != CANCEL_MSG_LEN)
	{
		return ERROR;
	}

	dataBlock.msgLen = CANCEL_MSG_LEN;
	memcpy(dataBlock.msgContent, message, CANCEL_MSG_LEN);
	dataBlock.blockLen = 4 + 4 + CANCEL_MSG_LEN + 4 + MAX_ADD_INFO_LEN;
	
	// Update additional content 
	dataBlock.addLen = MAX_ADD_INFO_LEN;
	int manualOrderMarking = -9876;
	memcpy(&dataBlock.addContent[CROSS_ID_INDEX], &manualOrderMarking, 4);
	
	// Add suffix
	dataBlock.addContent[ACCOUNT_SUFFIX_INDEX] = accountIndex;
	
	//printf("Testing: Send RASH Manual Cancel Request with account = %d\n", accountIndex);
	
	// Add data block to collection
	AddDataBlockToCollection(&dataBlock, &pmRawDataMgmt[NASDAQ_RASH_INDEX]);
	
	// Save data block to file
	SaveDataBlockToFile(&pmRawDataMgmt[NASDAQ_RASH_INDEX]);
	return SUCCESS;
}

/****************************************************************************
- Function name:	BuildAndSend_NASDAQ_RASH_Heartbeat
- Input:			N/A
- Output:		N/A
- Return:		Success or Failure
- Description:	+ The routine hides the following facts
				  - Build heartbeat message and send it to server
				+ There are no preconditions guaranteed to the routine
				+ The routine guarantees that the return value will 
						have a value of either
				  - Success
					or 
				  - Failure 
- Usage:			N/A
****************************************************************************/
int BuildAndSend_NASDAQ_RASH_Heartbeat(void)
{
	char message[2];		
	message[0] = 'R';
	message[1] = 10;
	
	return Send_NASDAQ_RASH_Msg(message, 2);
}

/****************************************************************************
- Function name:	Receive_NASDAQ_RASH_Msg
- Input:			N/A
- Output:		N/A
- Return:		Success or Failure
- Description:	+ The routine hides the following facts
				  - Receive message from server
				+ There are preconditions guaranteed to the routine
				  - 	dataBlock must be allocated and initialized before
				+ The routine guarantees that the return value will 
						have a value of either
				  - Success
					or 
				  - Failure
- Usage:			N/A
****************************************************************************/
int Receive_NASDAQ_RASH_Msg()
{
	if (NASDAQ_RASH_Config.numOfByteReceived < HALF_MAX_BUFFER_LEN)
	{
		NASDAQ_RASH_Config.numOfBytesPerRecv = recv(NASDAQ_RASH_Config.socket, 
										&NASDAQ_RASH_Config.buffer[NASDAQ_RASH_Config.numOfByteReceived], 
										NASDAQ_RASH_Config.numOfBytesToRecv, 0);
		
		// We use errno to indicate the socket is live or die
		if (NASDAQ_RASH_Config.numOfBytesPerRecv > 0)
		{
			NASDAQ_RASH_Config.numOfByteReceived += NASDAQ_RASH_Config.numOfBytesPerRecv;

			return SUCCESS;
		}
		else
		{
			TraceLog(ERROR_LEVEL, "Cannot receive NASDAQ RASH msg-header, res(%d) errno(%d)\n", NASDAQ_RASH_Config.numOfBytesPerRecv, errno);
			return ERROR;
		}
	}
	else
	{	
		if (NASDAQ_RASH_Config.isUnCompletedMsg == YES)
		{
			NASDAQ_RASH_Config.numOfBytesPerRecv = recv(NASDAQ_RASH_Config.socket, 
					&NASDAQ_RASH_Config.buffer[NASDAQ_RASH_Config.numOfByteReceived], 
					NASDAQ_RASH_Config.numOfBytesToRecv, 0);
			
			// We use errno to indicate the socket is live or die
			if (NASDAQ_RASH_Config.numOfBytesPerRecv > 0)
			{
				NASDAQ_RASH_Config.numOfByteReceived += NASDAQ_RASH_Config.numOfBytesPerRecv;

				return SUCCESS;
			}
			else
			{
				TraceLog(ERROR_LEVEL, "Cannot receive NASDAQ RASH msg-body, res(%d) errno(%d)\n", NASDAQ_RASH_Config.numOfBytesPerRecv, errno);
				return ERROR;
			}
		}
		else if (NASDAQ_RASH_Config.isUnCompletedMsg == NO)
		{
			NASDAQ_RASH_Config.numOfBytesPerRecv = recv(NASDAQ_RASH_Config.socket, 
					NASDAQ_RASH_Config.buffer, 
					NASDAQ_RASH_Config.numOfBytesToRecv, 0);
			
			// We use errno to indicate the socket is live or die
			if (NASDAQ_RASH_Config.numOfBytesPerRecv > 0)
			{
				NASDAQ_RASH_Config.numOfByteReceived = NASDAQ_RASH_Config.numOfBytesPerRecv;
				NASDAQ_RASH_Config.startIndex = 0;

				return SUCCESS;
			}
			else
			{
				TraceLog(ERROR_LEVEL, "Cannot receive NASDAQ RASH msg-body, res(%d) errno(%d)\n", NASDAQ_RASH_Config.numOfBytesPerRecv, errno);
				return ERROR;
			}
		}
	}
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	Get_NASDAQ_RASH_CompletedMessage
- Input:			N/A
- Output:		N/A
- Return:		Success or Failure
- Description:	+ The routine hides the following facts
				  - Try to get a completed message to process
				+ There are no preconditions guaranteed to the routine
				+ The routine guarantees that the return value will 
						have a value of either
				  - Success
					or 
				  - Failure
- Usage:			N/A
****************************************************************************/
int Get_NASDAQ_RASH_CompletedMessage(void)
{
	int i;

	while (1)
	{		
		NASDAQ_RASH_Config.remainingBytes = NASDAQ_RASH_Config.numOfByteReceived - NASDAQ_RASH_Config.startIndex;

		if (NASDAQ_RASH_Config.remainingBytes > 0)
		{			
			switch (NASDAQ_RASH_Config.buffer[NASDAQ_RASH_Config.startIndex])
			{
				case 'S':
					if (NASDAQ_RASH_Config.remainingBytes < 10)
					{
						if ((NASDAQ_RASH_Config.remainingBytes == 2) && 
						(NASDAQ_RASH_Config.buffer[NASDAQ_RASH_Config.startIndex + 1] == 10))
						{							
							//Sequenced Data Packet	
							NASDAQ_RASH_Config.startIndex = 2;
							return 8;
						}
						
						if (Receive_NASDAQ_RASH_CompleteMessage(12) == ERROR)
						{
							return ERROR;
						}
					}
					else
					{
						switch (NASDAQ_RASH_Config.buffer[NASDAQ_RASH_Config.startIndex + 9])
						{
							//Accepted Order Message
							case NASDAQ_RASH_ORDER_ACCEPTED:
								if (NASDAQ_RASH_Config.remainingBytes >= NASDAQ_RASH_ORDER_ACCEPTED_LEN)
								{
									NASDAQ_RASH_Config.incomingSeqNum++;

									NASDAQ_RASH_Config.startIndex += NASDAQ_RASH_ORDER_ACCEPTED_LEN;
									
									return NASDAQ_RASH_ORDER_ACCEPTED;
								}
								else
								{
									if (Receive_NASDAQ_RASH_CompleteMessage(NASDAQ_RASH_ORDER_ACCEPTED_LEN) == ERROR)
									{
										return ERROR;
									}
								}															
								break;
							
							//Executed Order Message
							case NASDAQ_RASH_ORDER_EXECUTED:
								if (NASDAQ_RASH_Config.remainingBytes >= NASDAQ_RASH_ORDER_EXECUTED_LEN)
								{
									NASDAQ_RASH_Config.incomingSeqNum++;

									NASDAQ_RASH_Config.startIndex += NASDAQ_RASH_ORDER_EXECUTED_LEN;
									
									return NASDAQ_RASH_ORDER_EXECUTED;
								}
								else
								{
									if (Receive_NASDAQ_RASH_CompleteMessage(NASDAQ_RASH_ORDER_EXECUTED_LEN) == ERROR)
									{
										return ERROR;
									}
								}
								break;
							
							//Cancel Order Message
							case NASDAQ_RASH_ORDER_CANCELED:
								if (NASDAQ_RASH_Config.remainingBytes >= NASDAQ_RASH_ORDER_CANCELED_LEN)
								{
									NASDAQ_RASH_Config.incomingSeqNum++;

									NASDAQ_RASH_Config.startIndex += NASDAQ_RASH_ORDER_CANCELED_LEN;
									
									return NASDAQ_RASH_ORDER_CANCELED;
								}
								else
								{
									if (Receive_NASDAQ_RASH_CompleteMessage(NASDAQ_RASH_ORDER_CANCELED_LEN) == ERROR)
									{
										return ERROR;
									}
								}
								break;

							//Rejected Order Message
							case NASDAQ_RASH_ORDER_REJECTED:
								if (NASDAQ_RASH_Config.remainingBytes >= NASDAQ_RASH_ORDER_REJECTED_LEN)
								{
									NASDAQ_RASH_Config.incomingSeqNum++;

									NASDAQ_RASH_Config.startIndex += NASDAQ_RASH_ORDER_REJECTED_LEN;
									
									return NASDAQ_RASH_ORDER_REJECTED;
								}
								else
								{
									if (Receive_NASDAQ_RASH_CompleteMessage(NASDAQ_RASH_ORDER_REJECTED_LEN) == ERROR)
									{
										return ERROR;
									}
								}
								break;

							//Broken Trade Order Message
							case NASDAQ_RASH_BROKEN_TRADE:
								if (NASDAQ_RASH_Config.remainingBytes >= NASDAQ_RASH_BROKEN_TRADE_LEN)
								{
									NASDAQ_RASH_Config.incomingSeqNum++;

									NASDAQ_RASH_Config.startIndex += NASDAQ_RASH_BROKEN_TRADE_LEN;
									
									return NASDAQ_RASH_BROKEN_TRADE;
								}
								else
								{
									if (Receive_NASDAQ_RASH_CompleteMessage(NASDAQ_RASH_BROKEN_TRADE_LEN) == ERROR)
									{
										return ERROR;
									}
								}
								break;

							//System Event Message
							case NASDAQ_RASH_SYSTEM_EVENT:
								if (NASDAQ_RASH_Config.remainingBytes >= NASDAQ_RASH_SYSTEM_EVENT_LEN)
								{
									NASDAQ_RASH_Config.incomingSeqNum++;

									NASDAQ_RASH_Config.startIndex += NASDAQ_RASH_SYSTEM_EVENT_LEN;
									
									return NASDAQ_RASH_SYSTEM_EVENT;
								}
								else
								{
									if (Receive_NASDAQ_RASH_CompleteMessage(NASDAQ_RASH_SYSTEM_EVENT_LEN) == ERROR)
									{
										return ERROR;
									}
								}
								break;
																					
							default:									
								TraceLog(ERROR_LEVEL, "Unknown NASDAQ RASH message type\n");
								return -1;						
						}
					}
					break;

				//Heartbeat Request
				case NASDAQ_RASH_HEARTBEAT:
					if (NASDAQ_RASH_Config.remainingBytes >= NASDAQ_RASH_HEARTBEAT_LEN)
					{
						NASDAQ_RASH_Config.startIndex += NASDAQ_RASH_HEARTBEAT_LEN;
						return NASDAQ_RASH_HEARTBEAT;
					}
					else
					{
						if (Receive_NASDAQ_RASH_CompleteMessage(NASDAQ_RASH_HEARTBEAT_LEN) == ERROR)
						{
							return ERROR;
						}
					}
					
					break;
				
				//Debug Packet
				case '+':
					i = NASDAQ_RASH_Config.startIndex;
					while (NASDAQ_RASH_Config.buffer[i] != 10)
					{
						if (i >= (NASDAQ_RASH_Config.numOfByteReceived - 1))
						{
							if (Receive_NASDAQ_RASH_CompleteMessage(1) == ERROR)
							{
								return ERROR;
							}
						}
						else
						{
							i += 1;
						}
					}

					//Debug Packet
					NASDAQ_RASH_Config.startIndex = i + 1;
					return NASDAQ_RASH_DEBUG;

				//Login Accepted Packet
				case NASDAQ_RASH_LOGIN_ACCEPTED:
					if (NASDAQ_RASH_Config.remainingBytes >= NASDAQ_RASH_LOGIN_ACCEPTED_LEN)
					{
						NASDAQ_RASH_Config.startIndex += NASDAQ_RASH_LOGIN_ACCEPTED_LEN;
						return NASDAQ_RASH_LOGIN_ACCEPTED_RETURN;	
					}
					else
					{
						if (Receive_NASDAQ_RASH_CompleteMessage(NASDAQ_RASH_LOGIN_ACCEPTED_LEN) == ERROR)
						{
							return ERROR;
						}
					}
					
					break;	
					
				//Login Rejected Packet
				case NASDAQ_RASH_LOGIN_REJECTED:
					if (NASDAQ_RASH_Config.remainingBytes >= NASDAQ_RASH_LOGIN_REJECTED_LEN)
					{
						NASDAQ_RASH_Config.startIndex += NASDAQ_RASH_LOGIN_REJECTED_LEN;
						return NASDAQ_RASH_LOGIN_REJECTED_RETURN;	
					}
					else
					{
						if (Receive_NASDAQ_RASH_CompleteMessage(NASDAQ_RASH_LOGIN_ACCEPTED_LEN) == ERROR)
						{
							return ERROR;
						}
					}
					break;
				
				default:
					TraceLog(ERROR_LEVEL, "Unknown NASDAQ RASH messag type\n");
					return -1;
			}
		}
		else
		{	
			if (NASDAQ_RASH_Config.numOfByteReceived >= HALF_MAX_BUFFER_LEN)
			{
				NASDAQ_RASH_Config.isUnCompletedMsg = NO;
				
				NASDAQ_RASH_Config.numOfBytesToRecv = HALF_MAX_BUFFER_LEN;

				if (Receive_NASDAQ_RASH_Msg() == -1)
				{
					return -1;
				}
			}
			else
			{
				NASDAQ_RASH_Config.numOfBytesToRecv = HALF_MAX_BUFFER_LEN - NASDAQ_RASH_Config.numOfByteReceived;

				if (Receive_NASDAQ_RASH_Msg() == -1)
				{
					return -1;
				}
			}		
		}
	}
}

/****************************************************************************
- Function name:	Receive_NASDAQ_RASH_CompleteMessage
- Input:			N/A
- Output:		N/A
- Return:		Success or Failure
- Description:	+ The routine hides the following facts
				  - Try to receive a complete message
				+ There are no preconditions guaranteed to the routine
				+ The routine guarantees that the return value will 
						have a value of either
				  - Success
					or 
				  - Failure
- Usage:			N/A
****************************************************************************/
int Receive_NASDAQ_RASH_CompleteMessage(int msgLen)
{	
	if (NASDAQ_RASH_Config.numOfByteReceived >= HALF_MAX_BUFFER_LEN)
	{
		NASDAQ_RASH_Config.isUnCompletedMsg = YES;

		NASDAQ_RASH_Config.numOfBytesToRecv = msgLen - NASDAQ_RASH_Config.remainingBytes;

		if (Receive_NASDAQ_RASH_Msg() == ERROR)
		{
			return ERROR;
		}
	}
	else
	{
		NASDAQ_RASH_Config.numOfBytesToRecv = HALF_MAX_BUFFER_LEN - NASDAQ_RASH_Config.numOfByteReceived;

		if (Receive_NASDAQ_RASH_Msg() == ERROR)
		{
			return ERROR;
		}
	}
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	Send_NASDAQ_RASH_Msg
- Input:			+ message
				+ msgLen
- Output:		N/A
- Return:		Success or Failure
- Description:	+ The routine hides the following facts
				  - Send message to server
				+ There are preconditions guaranteed to the routine
				  - 	message and msgLen must be checked before
				+ The routine guarantees that the return value will 
						have a value of
				  - message length
- Usage:			N/A
****************************************************************************/
int Send_NASDAQ_RASH_Msg(const char *message, int msgLen)
{
	int retValue = send(NASDAQ_RASH_Config.socket, message, msgLen, 0);
	
	if (retValue != msgLen)
	{
		return ERROR;
	}
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	Add_NASDAQ_RASH_DataBlockToCollection
- Input:			+ dataBlock
- Output:		NASDAQ RASH collection will be updated with new data block
- Return:		N/A
- Description:	+ The routine hides the following facts
				  - Update block length for data block
				  - Update additional info
				  - Add data block to collection
				  - Update incoming Seq Number
				+ There are preconditions guaranteed to the routine
				  - 	dataBlock must be allocated and initialized before
				+ The routine guarantees that the return value will 
						have a value of either
				  - Success
					or 
				  - Failure
- Usage:			N/A
****************************************************************************/
void Add_NASDAQ_RASH_DataBlockToCollection(t_DataBlock *dataBlock, int isManualOrder)
{
	// Update Block Length
	dataBlock->blockLen = 4 + 4 + dataBlock->msgLen + 4 + MAX_ADD_INFO_LEN;
	
	// Update additional content 
	GetTimeFormatTime(dataBlock->addContent);
	
	dataBlock->addLen = MAX_ADD_INFO_LEN;
	
	if (isManualOrder == 1)
	{
		//We will use value at CROSS_ID_INDEX to mark this rawdata is of a manual order
		int manualOrderMarking = -9876;
		memcpy(&dataBlock->addContent[CROSS_ID_INDEX], &manualOrderMarking, 4);
	}

	// Add data block to collection
	AddDataBlockToCollection(dataBlock, &pmRawDataMgmt[NASDAQ_RASH_INDEX]);
	
	// Save data block to file
	SaveDataBlockToFile(&pmRawDataMgmt[NASDAQ_RASH_INDEX]);
}

/****************************************************************************
- Function name:	SaveNASDAQ_RASH_SeqNumToFile
- Input:			N/A
- Output:		N/A
- Return:		Success or Failure
- Description:	+ The routine hides the following facts
				+ There are no preconditions guaranteed to the routine
				+ The routine guarantees that the status value will 
					have a value of either
				  - Success
					or
				  - Failure
- Usage:			N/A
****************************************************************************/
int SaveNASDAQ_RASH_SeqNumToFile(void)
{
	return SaveSeqNumToFile("nasdaq_rash_seqs", NASDAQ_RASH_INDEX, &NASDAQ_RASH_Config);
}

/****************************************************************************
- Function name:	ProcessNewOrderOfRASH
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessNewOrderOfRASH(t_DataBlock *dataBlock)
{
	t_SendNewOrder order;

	// order.ECNId
	order.ECNId = TYPE_NASDAQ_RASH;
		
	// Order ID - Prepend Token with last two digits of account number
	char fieldToken[15];
	memcpy( fieldToken, &dataBlock->msgContent[4], 12 );
	fieldToken[12] = 0;

	// order.orderId
	order.orderId = atoi( fieldToken );
	
	if (order.orderId < 500000)
	{
		// Register account suffix
		accountMapping[order.orderId % MAX_ORDER_PLACEMENT] = dataBlock->addContent[ACCOUNT_SUFFIX_INDEX];
	
		//PM will not process rawdata of manual order
		return 0;
	}
	
	// "B", "S" or "SS"
	char action = dataBlock->msgContent[16]; 
	switch ( action )
	{
		case 'B':
			order.side = BUY_TO_CLOSE_TYPE;
			break;
		
		case 'S':
			order.side = SELL_TYPE;
			break;
		
		case 'T':
			order.side = SHORT_SELL_TYPE;
			break;
		
		default:
			order.side = -1;
			
			TraceLog(ERROR_LEVEL, "RASH Order: not support side = %c, %d\n", action, action );
			break;
	}
	
	// Shares
	char fieldShares[7];
	memcpy( fieldShares, &dataBlock->msgContent[17], 6 );
	fieldShares[6] = 0;

	//order.shares
	order.shares = atoi( fieldShares );

	// Stock symbol
	strncpy(order.symbol, (char*) &dataBlock->msgContent[23], SYMBOL_LEN);
	TrimRight(order.symbol);
	
	// Account
	order.accountSuffix = dataBlock->addContent[ACCOUNT_SUFFIX_INDEX];

	int symbolIndex = GetStockSymbolIndex(order.symbol);

	if (symbolIndex == -1)
	{
		// Try to add it to Symbol List
		if ((symbolIndex = UpdateStockSymbolIndex(order.symbol)) == ERROR)
		{
			TraceLog(ERROR_LEVEL, "Cannot find symbol = %.8s in SymbolList\n", order.symbol);
			
			// Ignore current add message
			return ERROR;
		}
	}

	order.symbolIndex = symbolIndex;
	
	// Price
	char fieldPrice[11];
	memcpy( fieldPrice, &dataBlock->msgContent[31], 10 );
	fieldPrice[10] = 0;

	//order.price
	order.price = atof( fieldPrice ) / 10000;
	
	//Time In Force
	char tif[6];
	memcpy(tif, &dataBlock->msgContent[41], 5);
	tif[5] = 0;
	
	int tifValue = atoi(tif);
	if(tifValue == 0)	
	{
		order.tif = IOC_TYPE;
	}
	else if(tifValue > 0)	
	{
		order.tif = DAY_TYPE;
	}
	else
	{
		TraceLog(ERROR_LEVEL, "RASH: TimeInForce = %s\n", tif );
		order.tif = -1;
	}

	order.type = BBO_ORDER;

	ProcessAddNewOrder(&order);

	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessAcceptedOrderOfRASH
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessAcceptedOrderOfRASH(t_DataBlock dataBlock, int isTrading)
{
	// Order ID - Prepend Token with last two digits of account number
	char fieldToken[15];
	memcpy( fieldToken, &dataBlock.msgContent[12], 12 );
	fieldToken[12] = 0;
	
	int orderID = atoi(fieldToken);
	
	// Order reference number
	char fieldOrdRef[10];
	memcpy( fieldOrdRef, &dataBlock.msgContent[59], 9 );
	fieldOrdRef[9] = 0;
	long ecnOrderId = atol(fieldOrdRef);

	if (orderID < 500000)
	{
		//PM will not process rawdata of manual order
		AutoCancelOrder.orderInfo[orderID % MAX_MANUAL_ORDER].marked = 1;
		return 0;
	}
	

	int numSeconds, numMicroseconds;
	
	numSeconds = GetIntNumber(dataBlock.addContent);
	numMicroseconds = GetIntNumber(&dataBlock.addContent[4]);

	ProcessOrderAccepted(orderID, ecnOrderId, numSeconds, numMicroseconds, isTrading);

	return 0;
}

/****************************************************************************
- Function name:	ProcessExecutedOrderOfRASH
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessExecutedOrderOfRASH(t_DataBlock dataBlock)
{
	// Order ID - Prepend Token with last two digits of account number
	char fieldToken[15];
	memcpy( fieldToken, &dataBlock.msgContent[12], 12 );
	fieldToken[12] = 0;
	
	int orderID = atoi(fieldToken);
	
	// Shares
	char fieldShares[7];
	memcpy( fieldShares, &dataBlock.msgContent[24], 6 );
	fieldShares[6] = 0;
	
	int shares = atoi(fieldShares);

	if (orderID < 500000)
	{
		//PM will not process rawdata of manual order
		AutoCancelOrder.orderInfo[orderID % MAX_MANUAL_ORDER].leftShares -= shares;
		return 0;
	}
	

	ProcessOrderFilled(orderID, shares);

	return 0;
}

/****************************************************************************
- Function name:	ProcessCanceledOrderOfRASH
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessCanceledOrderOfRASH(t_DataBlock dataBlock)
{
	// Order ID - Prepend Token with last two digits of account number
	char fieldToken[15];
	memcpy( fieldToken, &dataBlock.msgContent[12], 12 );
	fieldToken[12] = 0;
	
	int orderID = atoi(fieldToken);
	
	char tempBuffer[7];
	memcpy(&tempBuffer, &dataBlock.msgContent[24], 6);
	tempBuffer[6] = 0;
	int canceledShares = atoi(tempBuffer);

	if (orderID < 500000)
	{
		//PM will not process rawdata of manual order
		AutoCancelOrder.orderInfo[orderID % MAX_MANUAL_ORDER].leftShares -= canceledShares;
		return 0;
	}
	
	ProcessOrderCanceled(orderID, canceledShares);

	return 0;
}

/****************************************************************************
- Function name:	ProcessRejectedOrderOfRASH
- Input:			
- Output:			
- Return:			
- Description:		
- Usage:			
****************************************************************************/
int ProcessRejectedOrderOfRASH(t_DataBlock dataBlock)
{
	// Order ID - Prepend Token with last two digits of account number
	char fieldToken[15];
	memcpy( fieldToken, &dataBlock.msgContent[12], 12 );
	fieldToken[12] = 0;
	
	int orderID = atoi(fieldToken);

	if (orderID < 500000)
	{
		//PM will not process rawdata of manual order
		AutoCancelOrder.orderInfo[orderID % MAX_MANUAL_ORDER].leftShares = 0;
		return 0;
	}
	
	char reason = dataBlock.msgContent[24];
	
	if (reason == 'H') //Security is Halted
	{
		int indexOpenOrder = GetOrderIdIndex(orderID);

		if (indexOpenOrder != -1)
		{
			char symbol[SYMBOL_LEN];
			strncpy(symbol, asOpenOrder.orderSummary[indexOpenOrder].symbol, SYMBOL_LEN);
			
			int symbolIndex = GetTradeSymbolIndex(symbol);
			if (symbolIndex == -1)
			{
				ProcessOrderRejected(orderID);
				return SUCCESS;
			}
			
			unsigned int haltTime;
			memcpy (&haltTime, dataBlock.addContent, 4);	//First 4 bytes is timestamp in seconds from epoch, binary format
	
			tradeSymbolList.symbolInfo[symbolIndex].haltTime[BOOK_ARCA] = haltTime;
			tradeSymbolList.symbolInfo[symbolIndex].haltStatus[BOOK_ARCA] = SYMBOL_STATUS_HALT;
		
			tradeSymbolList.symbolInfo[symbolIndex].haltTime[BOOK_NASDAQ] = haltTime;
			tradeSymbolList.symbolInfo[symbolIndex].haltStatus[BOOK_NASDAQ] = SYMBOL_STATUS_HALT;
		
			tradeSymbolList.symbolInfo[symbolIndex].haltStatusSent = NO;
		
			SendAHaltedStockToAS(&asCollection.connection, symbolIndex, 1);
		}
	}
	else if (reason == 'X') // Invalid Price, we only halt symbol on specific ECN (NASDAQ)
	{
		int indexOpenOrder = GetOrderIdIndex(orderID);

		if (indexOpenOrder != -1)
		{
			char symbol[SYMBOL_LEN];
			strncpy(symbol, asOpenOrder.orderSummary[indexOpenOrder].symbol, SYMBOL_LEN);
			
			int symbolIndex = GetTradeSymbolIndex(symbol);
			if (symbolIndex == -1)
			{
				ProcessOrderRejected(orderID);
				return SUCCESS;
			}
			
			unsigned int haltTime;
			memcpy (&haltTime, dataBlock.addContent, 4);	//First 4 bytes is timestamp in seconds from epoch, binary format
	
			tradeSymbolList.symbolInfo[symbolIndex].haltTime[BOOK_ARCA] = haltTime;
			tradeSymbolList.symbolInfo[symbolIndex].haltStatus[BOOK_ARCA] = SYMBOL_STATUS_HALT;

			tradeSymbolList.symbolInfo[symbolIndex].haltStatusSent = NO;
		
			SendAHaltedStockToAS(&asCollection.connection, symbolIndex, 0);
		}
	}
	
	ProcessOrderRejected(orderID);

	return 0;
}
