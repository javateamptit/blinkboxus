/****************************************************************************
** Project name:	Equity Arbitrage Application
** File name: 	book_mgmt.c
** Description: 	This file contains function definitions that were declared
				in book_mgmt.h

** Author: 		Sang Nguyen-Minh
** First created on 26 September 2007
** Last updated on 27 September 2007
****************************************************************************/

/****************************************************************************
** Include files
****************************************************************************/
#define _ISOC9X_SOURCE
#include "book_mgmt.h"
#include "configuration.h"
#include "trading_mgmt.h"
#include "aggregation_server_proc.h"

#include "kernel_algorithm.h"
#include "arca_book_proc.h"
#include "nasdaq_book_proc.h"
#include "cqs_feed_proc.h"
#include "uqdf_feed_proc.h"
#include "cts_feed_proc.h"
#include "utdf_proc.h"

#include <errno.h>

/****************************************************************************
** Global variable definitions
****************************************************************************/
t_SkipList *stockBook[MAX_BOOK_CONNECTIONS][MAX_SIDE][MAX_STOCK_SYMBOL];
t_TopOrder bestQuote[MAX_SIDE][MAX_STOCK_SYMBOL];
t_TopOrder bestQuoteRoundLot[MAX_SIDE][MAX_STOCK_SYMBOL];

t_PartialHashingItem NASDAQ_PartialHashing[NASDAQ_MAX_DIVISOR][NASDAQ_MAX_SAME_REMAINDER];
t_NasdaqHashingBackupItem NASDAQ_HashingBackup[NASDAQ_MAX_DIVISOR][NASDAQ_MAX_SAME_REMAINDER];

t_PartialHashingItem ARCA_PartialHashing[ARCA_MAX_DIVISOR][ARCA_MAX_SAME_REMAINDER];
t_ArcaHashingBackupItem ARCA_HashingBackup[ARCA_MAX_DIVISOR][ARCA_MAX_SAME_REMAINDER];

pthread_mutex_t updateAskBidMutex[MAX_STOCK_SYMBOL];

pthread_mutex_t tradingMgmtMutex[MAX_STOCK_SYMBOL];

pthread_mutex_t deleteQuotesMgmtMutex[MAX_BOOK_CONNECTIONS][MAX_STOCK_SYMBOL][MAX_SIDE];

t_QuoteNeedDelete QuoteNeedDeleteCollection[MAX_BOOK_CONNECTIONS];

char strTemp[80];

t_QuoteToDelete quoteToDeleteCollection[MAX_ORDER_CONNECTIONS];
t_FlushBookRequest flushBookRequestCollection[MAX_BOOK_CONNECTIONS];

char manFlushBookFlag[MAX_BOOK_CONNECTIONS];
int manFlushBookStockSymbolIndex[MAX_BOOK_CONNECTIONS];

t_OrderDetail quoteIgnoreList[MAX_STOCK_SYMBOL][MAX_QUOTE_IGNORE];
t_OrderInfo LastNasdaqOrder[MAX_STOCK_SYMBOL];

extern t_DBLMgmt DBLMgmt;
/****************************************************************************
- Function name:	InitBookMgmtDataStructure
- Input:			
- Output:		N/A
- Return:		Success or Failure
- Description:	+ The routine hides the following facts
				  - Initialize all stock books
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
void InitBookMgmtDataStructure(void)
{
	int ecnBookIndex;
	int sideIndex;
	int stockSymbolIndex;
	
	// Initialize Mutex Locks
	for (stockSymbolIndex = 0; stockSymbolIndex < MAX_STOCK_SYMBOL; stockSymbolIndex++)
	{
		pthread_mutex_init(&updateAskBidMutex[stockSymbolIndex], NULL);
		pthread_mutex_init(&tradingMgmtMutex[stockSymbolIndex], NULL);
	}
	
	// Initialize quote ignore list
	int quoteIndex;

	for (stockSymbolIndex = 0; stockSymbolIndex < MAX_STOCK_SYMBOL; stockSymbolIndex++)
	{
		for (quoteIndex = 0; quoteIndex < MAX_QUOTE_IGNORE; quoteIndex ++)
		{
			quoteIgnoreList[stockSymbolIndex][quoteIndex].shareVolume = 0;
			quoteIgnoreList[stockSymbolIndex][quoteIndex].sharePrice = 0.00;
			quoteIgnoreList[stockSymbolIndex][quoteIndex].refNum = -1;
		}
		
		LastNasdaqOrder[stockSymbolIndex].shares = 0;
		LastNasdaqOrder[stockSymbolIndex].price = 0.0;
		LastNasdaqOrder[stockSymbolIndex].orderID = -1;
		LastNasdaqOrder[stockSymbolIndex].side = -1;
	}
	
	// Initialize book status
	for (ecnBookIndex = 0; ecnBookIndex < MAX_BOOK_CONNECTIONS; ecnBookIndex++)
	{
		bookStatusMgmt[ecnBookIndex].isConnected = DISCONNECTED;
		bookStatusMgmt[ecnBookIndex].shouldConnect = NO;
		bookStatusMgmt[ecnBookIndex].isFlushBook = NO;
	}
	
	// Initialize stock books
	for (ecnBookIndex = 0; ecnBookIndex < MAX_BOOK_CONNECTIONS; ecnBookIndex++)
	{
		for (sideIndex = 0; sideIndex < MAX_SIDE; sideIndex++)
		{
			for (stockSymbolIndex = 0; stockSymbolIndex < MAX_STOCK_SYMBOL; stockSymbolIndex++)
			{
				Initialize_List(&stockBook[ecnBookIndex][sideIndex][stockSymbolIndex]);
			}
		}
	}
	
	// Initialize best quote
	for (sideIndex = 0; sideIndex < MAX_SIDE; sideIndex++)
	{
		for (stockSymbolIndex = 0; stockSymbolIndex < MAX_STOCK_SYMBOL; stockSymbolIndex++)
		{
			bestQuote[sideIndex][stockSymbolIndex].ecnIndicator = -1;
			bestQuote[sideIndex][stockSymbolIndex].info.shareVolume = 0;
			bestQuote[sideIndex][stockSymbolIndex].info.sharePrice = 0;
			bestQuote[sideIndex][stockSymbolIndex].info.refNum = 0;
		}
	}

	// Initialize best quote Not Odd Lot
	for (sideIndex = 0; sideIndex < MAX_SIDE; sideIndex++)
	{
		for (stockSymbolIndex = 0; stockSymbolIndex < MAX_STOCK_SYMBOL; stockSymbolIndex++)
		{
			bestQuoteRoundLot[sideIndex][stockSymbolIndex].ecnIndicator = -1;
			
			bestQuoteRoundLot[sideIndex][stockSymbolIndex].info.shareVolume = 0;
			bestQuoteRoundLot[sideIndex][stockSymbolIndex].info.sharePrice = 0;
			bestQuoteRoundLot[sideIndex][stockSymbolIndex].info.refNum = 0;
		}
	}
	
	// Initialize partial hash data structure
	int divisorIndex;
	int remainderIndex;
	
	// NASDAQ
	for (divisorIndex = 0; divisorIndex < NASDAQ_MAX_DIVISOR; divisorIndex++)
	{
		for (remainderIndex = 0; remainderIndex < NASDAQ_MAX_SAME_REMAINDER; remainderIndex++)
		{
			NASDAQ_PartialHashing[divisorIndex][remainderIndex].quotient = -1;
			NASDAQ_PartialHashing[divisorIndex][remainderIndex].side = 0;
			NASDAQ_PartialHashing[divisorIndex][remainderIndex].stockSymbolIndex = -1;
			NASDAQ_PartialHashing[divisorIndex][remainderIndex].node = NULL;
			
			NASDAQ_HashingBackup[divisorIndex][remainderIndex].quotient = -1;
			NASDAQ_HashingBackup[divisorIndex][remainderIndex].side = 0;
			NASDAQ_HashingBackup[divisorIndex][remainderIndex].stockSymbolIndex = -1;
		}
	}
			
	// ARCA
	for (divisorIndex = 0; divisorIndex < ARCA_MAX_DIVISOR; divisorIndex++)
	{
		for (remainderIndex = 0; remainderIndex < ARCA_MAX_SAME_REMAINDER; remainderIndex++)
		{
			ARCA_PartialHashing[divisorIndex][remainderIndex].quotient = -1;
			ARCA_PartialHashing[divisorIndex][remainderIndex].side = 0;
			ARCA_PartialHashing[divisorIndex][remainderIndex].stockSymbolIndex = -1;
			ARCA_PartialHashing[divisorIndex][remainderIndex].node = NULL;
			
			ARCA_HashingBackup[divisorIndex][remainderIndex].quotient = -1;
			ARCA_HashingBackup[divisorIndex][remainderIndex].tradeSession = 0;
		}
	}
	
	// Initialize QuoteToDelete collection and flush book request collection
	for (ecnBookIndex = 0; ecnBookIndex < MAX_BOOK_CONNECTIONS; ecnBookIndex++)
	{
		flushBookRequestCollection[ecnBookIndex].isFlushBook = NO;
		flushBookRequestCollection[ecnBookIndex].stockSymbolIndex = -1;
		flushBookRequestCollection[ecnBookIndex].side = -1;
	}
	
	TraceLog(DEBUG_LEVEL, "Initialize deleteQuotesMgmt mutexes ...\n");

	// Initialize deleteQuotesMgmt mutexes	
	for (ecnBookIndex = 0; ecnBookIndex < MAX_BOOK_CONNECTIONS; ecnBookIndex++)
	{
		for (stockSymbolIndex = 0; stockSymbolIndex < MAX_STOCK_SYMBOL; stockSymbolIndex++)
		{
			for (sideIndex = 0; sideIndex < MAX_SIDE; sideIndex++)
			{
				pthread_mutex_init(&deleteQuotesMgmtMutex[ecnBookIndex][stockSymbolIndex][sideIndex], NULL);
			}
		}
	}
	
	// Initialize manFlushBookFlag
	for (ecnBookIndex = 0; ecnBookIndex < MAX_BOOK_CONNECTIONS; ecnBookIndex++)
	{
		manFlushBookFlag[ecnBookIndex] = NO;
		manFlushBookStockSymbolIndex[ecnBookIndex] = -1;
	}
}

/****************************************************************************
- Function name:	Initialize_List
- Input:			+ inputList
- Output:		N/A
- Return:		N/A
- Description:	+ The routine hides the following facts
				  - Initialize a skip list
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
void Initialize_List(t_SkipList **inputList)
{
	int lvl; // LEVEL loop

	t_SkipList *list;

	list = (t_SkipList *) malloc(sizeof(t_SkipList));

	if (list == NULL)
	{
		TraceLog(ERROR_LEVEL, "Cannot initialize skiplist\n");
	}

	list->head = (t_OrderDetailNode *) malloc(sizeof(t_OrderDetailNode));
	
	if (list->head == NULL)
	{
		TraceLog(ERROR_LEVEL, "Cannot allocate memory for head node in skiplist\n");
	}

	list->head->info.sharePrice = 0;
	list->head->info.shareVolume = 0;
	list->head->info.refNum = 0;
	
	for (lvl = 0; lvl < MAX_LEVEL; lvl++)
	{
		list->head->forward[lvl] = NULL;
	}

	list->level = 0;
	list->countNode = 0;
	list->newNode = NULL;

	*inputList = list;
}

/****************************************************************************
- Function name:	Destroy_List
- Input:			N/A
- Output:		N/A
- Return:		N/A
- Description:	+ The routine hides the following facts
				  - Destroy a skip list data structures
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
void Destroy_List(t_SkipList *list)
{
	t_OrderDetailNode *next_ptr, *temp_ptr;
	
	if (list != NULL)
	{
		if (list->head != NULL)
		{			
			next_ptr = list->head->forward[0];
			
			while (next_ptr != NULL)
			{
				temp_ptr = next_ptr;
				next_ptr = temp_ptr->forward[0];
				free(temp_ptr);
				temp_ptr = NULL;
			}
			
			free(list->head);
			list->head = NULL;
			
			free(list);
			list = NULL;
		}
	}
}

/****************************************************************************
- Function name:	GenerateLevel
- Input:			N/A
- Output:		N/A
- Return:		N/A
- Description:	+ The routine hides the following facts
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
int GenerateLevel(void)
{
	static int __opMaxLevel = MAX_LEVEL - 1;
	
	int newLevel = 0;
	
	while ( ((rand() % 100) < 50) && (newLevel < __opMaxLevel) )
		newLevel++;

	return newLevel;
}

/****************************************************************************
- Function name:	MakeNode
- Input:			N/A
- Output:		N/A
- Return:		N/A
- Description:	+ The routine hides the following facts
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
t_OrderDetailNode *MakeNode(int NewLevel, t_OrderDetail *info)
{
	t_OrderDetailNode *ret_ptr;
	
	ret_ptr = (t_OrderDetailNode *) malloc(sizeof(t_OrderDetailNode));
	if (ret_ptr == NULL)
	{
		TraceLog(ERROR_LEVEL, "in MakeNode(), can not allocate memory!\n");
	}

	ret_ptr->info = *info;
	ret_ptr->level = NewLevel;
	
	return ret_ptr;
}

/****************************************************************************
- Function name:	GeneralInsert
- Input:			N/A
- Output:		N/A
- Return:		N/A
- Description:	+ The routine hides the following facts
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
t_OrderDetailNode *GeneralInsert(t_SkipList *list, t_OrderDetail *info, int side, int stockSymbolIndex)
{
	double searchKey = info->sharePrice;

	// *Update* is used to store pointers that point to new node
	t_OrderDetailNode *update[MAX_LEVEL];
	
	// Store position to insert new node
	t_OrderDetailNode *x;
	
	if (list == NULL)
	{
		TraceLog(ERROR_LEVEL, "This line %d causes Segmentation Fault: access member of NULL pointer!\n", __LINE__);
	}
	
	x = list->head;
		
	int i = 0;

	// BID_SIDE: 3->2->1...
	if (side == BID_SIDE)
	{
		// Check number of nodes in list
		if (list->countNode == 0)
		{
			update[0] = x;				
		}
		else
		{
			// Begin at top level -> bottom level
			for (i = list->level; i > -1; i--)
			{
				while (x->forward[i] != NULL)
				{
					if (x->forward[i]->info.sharePrice >= searchKey)
					{
						x = x->forward[i];
					}
					else
					{
						break;
					}
				}

				update[i] = x;
			}
		}
	}
	// ASK: 1->2->3...
	else
	{
		// Check number of nodes in list
		if (list->countNode == 0)
		{
			update[i] = x;				
		}
		else
		{
			// Begin at top level -> bottom level
			for (i = list->level; i > -1; i--)
			{
				while (x->forward[i] != NULL)
				{
					if (fle(x->forward[i]->info.sharePrice, searchKey))
					{
						x = x->forward[i];
					}
					else
					{
						break;
					}
				}

				update[i] = x;
			}
		}
	}
	
	// Generate New Level
	int newLevel = GenerateLevel();

	// Update level of the list, if newLevel is greater than list level
	if (newLevel > list->level)
	{
		for (i = list->level + 1; i <= newLevel; i++)
		{
			update[i] = list->head;
		}

		list->level = newLevel;
	}
	
	// Make a new node
	x = (t_OrderDetailNode *) malloc(sizeof(t_OrderDetailNode));
	
	/*
	If cannot malloc for new node
	We will return null
	*/
	if (x == NULL)
	{
		TraceLog(ERROR_LEVEL, "in GeneralInsert(). Cannot allocate memory\n");
		return NULL;//Cannot malloc
	}

	x->info = *info;
	x->level = newLevel;

	if (list->countNode == 0)
	{
		// Update links
		for (i = 0; i <= newLevel; i++)
		{
			list->head->forward[i] = x;
			x->forward[i] = NULL;
		}
	}
	else
	{
		// Update links
		for (i = 0; i <= newLevel; i++)
		{
			x->forward[i] = update[i]->forward[i];
			update[i]->forward[i] = x;
		}
	}
	
	list->countNode++;

	list->newNode = x;
	
	// Update MaxBid, MinAsk
	int isCanceled = ERROR;
	
	if (side == BID_SIDE)
	{
		if ((info->shareVolume > MAX_ODD_LOT_SHARE) && fge(info->sharePrice, bestQuoteRoundLot[side][stockSymbolIndex].info.sharePrice))
		{
			isCanceled = UpdateMaxBidRoundLot(stockSymbolIndex, 1, NO, -1);
		}
		
		if (fge(info->sharePrice, bestQuote[side][stockSymbolIndex].info.sharePrice))
		{
			if (isCanceled == SUCCESS)
				UpdateMaxBid(stockSymbolIndex, 0, -1);
			else
				UpdateMaxBid(stockSymbolIndex, 1, -1);
		}
	}
	else
	{
		if ((info->shareVolume > MAX_ODD_LOT_SHARE) && (fle(info->sharePrice, bestQuoteRoundLot[side][stockSymbolIndex].info.sharePrice) || fle(bestQuoteRoundLot[side][stockSymbolIndex].info.sharePrice, 0.0)))
		{
			isCanceled = UpdateMinAskRoundLot(stockSymbolIndex, 1, NO, -1);
		}
		
		if (fle(info->sharePrice, bestQuote[side][stockSymbolIndex].info.sharePrice) || fle(bestQuote[side][stockSymbolIndex].info.sharePrice, 0.0) )
		{
			if (isCanceled == SUCCESS)
				UpdateMinAsk(stockSymbolIndex, 0, -1);
			else
				UpdateMinAsk(stockSymbolIndex, 1, -1);
		}
	}
	
	//sprintf(strTemp, "%d: insert price = %lf, refNum = %d\n", side, info->sharePrice, (int)info->refNum);
	//print_List(list);
	
	return list->newNode;
}

/****************************************************************************
- Function name:	GeneralDelete
- Input:			N/A
- Output:		N/A
- Return:		N/A
- Description:	+ The routine hides the following facts
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
int GeneralDelete(t_SkipList *list, t_OrderDetailNode *delete_node, int side, int stockSymbolIndex)
{
	// For loop var
	int i = 0;
	
	// Get searchKey
	double searchKey = delete_node->info.sharePrice;
	//int refNum = delete_node->info.refNum;

	// *Update* is used to store pointers that point to delete_node
	t_OrderDetailNode *update[MAX_LEVEL];
	
	t_OrderDetailNode *x;

	x = list->head;
	
	// BID_SIDE: 3->2->1...
	if (side == BID_SIDE)
	{
		// Check countNode of the list
		if (list->countNode == 0)
		{
			// The list hasn't any node
			return ERROR;
		}

		// Begin at top level -> bottom level
		for (i = list->level; i >= 0; i--)
		{
			while (x->forward[i] != NULL)
			{
				if (fgt(x->forward[i]->info.sharePrice, searchKey))
				{
					x = x->forward[i];
				}
				else if (flt(fabs(x->forward[i]->info.sharePrice - searchKey), 0.0001))
				{
					if (i <= delete_node->level)
					{
						while(delete_node != x->forward[i])
						{
							x = x->forward[i];

							if (x == NULL)
							{
								return ERROR;
							}
						}
						
						break;
					}
					
					break;
				}
				else
				{
					break;
				}
			}
			
			update[i] = x;
		}	
	}
	// ASK: 1->2->3...
	else
	{
		// Check countNode of the list
		if (list->countNode == 0)
		{
			// The list hasn't any node
			return ERROR;
		}

		// Begin at top level -> bottom level
		for (i = list->level; i >= 0; i--)
		{
			while (x->forward[i] != NULL)
			{
				if ( x->forward[i]->info.sharePrice < searchKey)
				{
					x = x->forward[i];
				}
				else if (flt(fabs(x->forward[i]->info.sharePrice - searchKey), 0.0001))
				{
					// x = rememberNode;
					if (i <= delete_node->level)
					{
						while(delete_node != x->forward[i])
						{
							x = x->forward[i];
							
							if (x == NULL)
							{
								return ERROR;
							}
						}			

						break;
					}
					
					break;
				}
				else
				{
					break;
				}
			}

			update[i] = x;
		}	
	}
	
	// Update Links for nodes in update list
	for (i = 0; i <= list->level; i++)
	{
		if (update[i]->forward[i] != delete_node)
		{
			break;
		}

		update[i]->forward[i] = delete_node->forward[i];
	}
	
	// Free memory
	free(delete_node);
	delete_node = NULL;
	
	// Update level of list
	while ( (list->level > 0) && (list->head->forward[list->level] == NULL) )
	{
		list->level--;
	}
	
	// Update countNode
	
	list->countNode--;
	
	// Update MaxBid, MinAsk
	// Update MaxBid
	int isCanceled = ERROR;
	
	if (side == BID_SIDE)
	{
		if (flt(fabs(searchKey - bestQuoteRoundLot[side][stockSymbolIndex].info.sharePrice), 0.0001))
		{
			isCanceled = UpdateMaxBidRoundLot(stockSymbolIndex, 1, NO, -1);
		}
		
		if (flt(fabs(searchKey - bestQuote[side][stockSymbolIndex].info.sharePrice), 0.0001))
		{
			if (isCanceled == SUCCESS)
				UpdateMaxBid(stockSymbolIndex, 0, -1);
			else
				UpdateMaxBid(stockSymbolIndex, 1, -1);
		}
	}
	// Update MinAsk
	else
	{
		if (flt(fabs(searchKey - bestQuoteRoundLot[side][stockSymbolIndex].info.sharePrice), 0.0001))
		{
			isCanceled = UpdateMinAskRoundLot(stockSymbolIndex, 1, NO, -1);
		}
		
		if (flt(fabs(searchKey - bestQuote[side][stockSymbolIndex].info.sharePrice), 0.0001))
		{
			if (isCanceled == SUCCESS)
				UpdateMinAsk(stockSymbolIndex, 0, -1);
			else
				UpdateMinAsk(stockSymbolIndex, 1, -1);
		}
	}
	
	//sprintf(strTemp, "%d: delete price = %lf refNum = %d\n", side, searchKey, refNum);
	
	//print_List(list);
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	GeneralModify
- Input:			N/A
- Output:		N/A
- Return:		N/A
- Description:	+ The routine hides the following facts
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
int GeneralModify(t_OrderDetailNode *modify_node, int NewShareVolume, int side, int stockSymbolIndex)
{
	modify_node->info.shareVolume = NewShareVolume;
	
	int isCanceled = ERROR;
	
	if (side == BID_SIDE)
	{
		if (flt(fabs(modify_node->info.sharePrice - bestQuoteRoundLot[side][stockSymbolIndex].info.sharePrice), 0.001))
		{
			isCanceled = UpdateMaxBidRoundLot(stockSymbolIndex, 1, NO, -1);
		}
		
		if (flt(fabs(modify_node->info.sharePrice - bestQuote[side][stockSymbolIndex].info.sharePrice), 0.0001))
		{
			if (isCanceled == SUCCESS)
				UpdateMaxBid(stockSymbolIndex, 0, -1);
			else
				UpdateMaxBid(stockSymbolIndex, 1, -1);
		}
	}
	else
	{
		if (flt(fabs(modify_node->info.sharePrice - bestQuoteRoundLot[side][stockSymbolIndex].info.sharePrice), 0.0001))
		{
			isCanceled = UpdateMinAskRoundLot(stockSymbolIndex, 1, NO, -1);
		}
		
		if (flt(fabs(modify_node->info.sharePrice - bestQuote[side][stockSymbolIndex].info.sharePrice), 0.001))
		{
			if (isCanceled == SUCCESS)
				UpdateMinAsk(stockSymbolIndex, 0, -1);
			else
				UpdateMinAsk(stockSymbolIndex, 1, -1);
		}

	}
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	UpdateMaxBid
- Input:			N/A
- Output:		N/A
- Return:		N/A
- Description:	+ The routine hides the following facts
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
int UpdateMaxBid(int stockSymbolIndex, const int isPlaceOrder, int ecnHalted)
{
	t_TopOrder *currentTopOrder = &bestQuote[BID_SIDE][stockSymbolIndex];
	
	// Get mutex lock to synchronize update/access on current Best Quote
	pthread_mutex_lock(&updateAskBidMutex[stockSymbolIndex]);
	
	t_TopOrder topOrderArray[MAX_BOOK_CONNECTIONS];
	
	// Get Top Quotes
	GetTopQuotes(topOrderArray, BID_SIDE, stockSymbolIndex, ecnHalted);
	
	// Get best quote
	t_TopOrder *retValue = GetBestQuote(topOrderArray, BID_SIDE);
	
	if (retValue != NULL)
	{
		currentTopOrder->info = retValue->info;
		currentTopOrder->ecnIndicator = retValue->ecnIndicator;
		positionCollection[stockSymbolIndex].holdQuote[BID_SIDE].isHold = YES; //khanhnd : set flag to indicate that the inside changed
	}
	else
	{
		currentTopOrder->ecnIndicator = -1;
		currentTopOrder->info.sharePrice = 0;
		currentTopOrder->info.shareVolume = 0;
	}
	
	// Release mutex lock
	pthread_mutex_unlock(&updateAskBidMutex[stockSymbolIndex]);

	// Check BBO orders placed
	if (isPlaceOrder == 1)
	{
		if (retValue != NULL)
		{
			return CheckBBOOrder(stockSymbolIndex, BID_SIDE, retValue->ecnIndicator, retValue->info.refNum, retValue->info.shareVolume, retValue->info.sharePrice, ODD_LOT_TYPE);
		}
		else if(positionCollection[stockSymbolIndex].isTrading == YES)	// Best Bid is empty
		{
			ProcessDisengageSymbol(positionCollection[stockSymbolIndex].stuck.symbol, TRADER_REQUEST);
			SendPMDisengageAlertToAS(&positionCollection[stockSymbolIndex].stuck, DISENGAGE_REASON_BEST_BID_NOTHING);
			return SUCCESS;
		}
	}
	return ERROR;
}

/****************************************************************************
- Function name:	UpdateMinAsk
- Input:			N/A
- Output:		N/A
- Return:		N/A
- Description:	+ The routine hides the following facts
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
int UpdateMinAsk(int stockSymbolIndex, const int isPlaceOrder, int ecnHalted)
{
	t_TopOrder *currentTopOrder = &bestQuote[ASK_SIDE][stockSymbolIndex];
	
	// Get mutex lock to synchronize update/access on current Best Quote
	pthread_mutex_lock(&updateAskBidMutex[stockSymbolIndex]);
	
	t_TopOrder topOrderArray[MAX_BOOK_CONNECTIONS];
	
	// Get Top Quotes
	GetTopQuotes(topOrderArray, ASK_SIDE, stockSymbolIndex, ecnHalted);
	
	// Get Best Quote
	t_TopOrder *retValue = GetBestQuote(topOrderArray, ASK_SIDE);
	
	if (retValue != NULL)
	{
		currentTopOrder->info = retValue->info;
		currentTopOrder->ecnIndicator = retValue->ecnIndicator;
		positionCollection[stockSymbolIndex].holdQuote[ASK_SIDE].isHold = YES; //khanhnd : set flag to indicate that the inside changed
	}
	else
	{
		currentTopOrder->ecnIndicator = -1;
		currentTopOrder->info.sharePrice = 0;
		currentTopOrder->info.shareVolume = 0;
	}
	
	// Release mutex lock
	pthread_mutex_unlock(&updateAskBidMutex[stockSymbolIndex]);

	// Check BBO orders placed
	if (isPlaceOrder == 1)
	{
		if (retValue != NULL)
		{
			return CheckBBOOrder(stockSymbolIndex, ASK_SIDE, retValue->ecnIndicator, retValue->info.refNum, retValue->info.shareVolume, retValue->info.sharePrice, ODD_LOT_TYPE);
		}
		else if(positionCollection[stockSymbolIndex].isTrading == YES)	// Best ASK is empty
		{
			ProcessDisengageSymbol(positionCollection[stockSymbolIndex].stuck.symbol, TRADER_REQUEST);
			SendPMDisengageAlertToAS(&positionCollection[stockSymbolIndex].stuck, DISENGAGE_REASON_BEST_ASK_NOTHING);
			return SUCCESS;
		}
	}
	
	return ERROR;
}

/****************************************************************************
- Function name:	GetTopQuotes
- Input:			N/A
- Output:		N/A
- Return:		N/A
- Description:	+ The routine hides the following facts
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
void GetTopQuotes(t_TopOrder topOrderArray[MAX_BOOK_CONNECTIONS], int side, int stockSymbolIndex, int ecnHalted)
{
	int ecnIndex;
	t_SkipList *currentList;
	t_OrderDetailNode *currentNode;
	
	for (ecnIndex = 0; ecnIndex < MAX_BOOK_CONNECTIONS; ecnIndex++)
	{
		if(ecnIndex == ecnHalted)
		{
			topOrderArray[ecnIndex].ecnIndicator = -1;
			continue;
		}
		
		// Check to know the stock book of ecn book is allowed to cross trade or not
		if (bookStatusMgmt[ecnIndex].isConnected == CONNECTED && 
			bookStatusMgmt[ecnIndex].shouldConnect == YES)
		{
			currentList = stockBook[ecnIndex][side][stockSymbolIndex];
			
			if (currentList == NULL)
			{
				topOrderArray[ecnIndex].ecnIndicator = -1;
				continue;
			}
			
			currentNode = currentList->head;
			
			if (currentNode == NULL)
			{
				topOrderArray[ecnIndex].ecnIndicator = -1;
				continue;
			}
			
			if (currentNode->forward[0] != NULL)
			{
				topOrderArray[ecnIndex].info = currentNode->forward[0]->info;
				
				if (fle(topOrderArray[ecnIndex].info.sharePrice, 0.0))
				{
					topOrderArray[ecnIndex].ecnIndicator = -1;
				}
				else
				{
					topOrderArray[ecnIndex].ecnIndicator = ecnIndex;
				}
			}
			else
			{
				topOrderArray[ecnIndex].ecnIndicator = -1;
			}
		}
		else
		{
			// If ecnIndicator equals -1 that means the top order cannot be used
			topOrderArray[ecnIndex].ecnIndicator = -1;
		}
	}
}
	
/****************************************************************************
- Function name:	GetBestQuote
- Input:			N/A
- Output:		N/A
- Return:		N/A
- Description:	+ The routine hides the following facts
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
t_TopOrder *GetBestQuote(t_TopOrder topOrderArray[MAX_BOOK_CONNECTIONS], int side)
{
	int ecnIndex;
	
	// We assume that the first item in array is best
	t_TopOrder *bestQuote = &topOrderArray[0];
	
	if (side == BID_SIDE)
	{
		for (ecnIndex = 1; ecnIndex < MAX_BOOK_CONNECTIONS; ecnIndex++)
		{
			// Check Top Order to know that it is available to access
			if (topOrderArray[ecnIndex].ecnIndicator != -1)
			{
				if (bestQuote->ecnIndicator == -1)
				{
					bestQuote = &topOrderArray[ecnIndex];
					
					continue;
				}
				else
				{
					if (flt(bestQuote->info.sharePrice, topOrderArray[ecnIndex].info.sharePrice))
					{
						bestQuote = &topOrderArray[ecnIndex];
					}
				}
			}
		}
	}
	else
	{
		for (ecnIndex = 1; ecnIndex < MAX_BOOK_CONNECTIONS; ecnIndex++)
		{
			// Check Top Order to know that it is available to access
			if (topOrderArray[ecnIndex].ecnIndicator != -1)
			{
				if (bestQuote->ecnIndicator == -1)
				{
					bestQuote = &topOrderArray[ecnIndex];
					
					continue;
				}
				else
				{
					if (fgt(bestQuote->info.sharePrice, topOrderArray[ecnIndex].info.sharePrice))
					{
						bestQuote = &topOrderArray[ecnIndex];
					}
				}
			}
		}
	}
	
	// Check before return for calling the routine
	if (bestQuote->ecnIndicator != -1)
	{
		return bestQuote;
	}
	else
	{
		return NULL;
	}
}

/****************************************************************************
- Function name:	FlushECNStockBook
- Input:			N/A
- Output:		N/A
- Return:		N/A
- Description:	+ The routine hides the following facts
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
int FlushECNStockBook(int ecnBookID)
{
	int stockSymbolIndex;

	for (stockSymbolIndex = 0; stockSymbolIndex < MAX_STOCK_SYMBOL; stockSymbolIndex++)
	{
		pthread_mutex_lock(&deleteQuotesMgmtMutex[ecnBookID][stockSymbolIndex][ASK_SIDE]);

		pthread_mutex_lock(&updateAskBidMutex[stockSymbolIndex]);

		// Free SkipList
		Destroy_List(stockBook[ecnBookID][ASK_SIDE][stockSymbolIndex]);
		
		// Initialize SkipList
		Initialize_List(&stockBook[ecnBookID][ASK_SIDE][stockSymbolIndex]);
		
		pthread_mutex_unlock(&updateAskBidMutex[stockSymbolIndex]);

		pthread_mutex_unlock(&deleteQuotesMgmtMutex[ecnBookID][stockSymbolIndex][ASK_SIDE]);
		
		
		pthread_mutex_lock(&deleteQuotesMgmtMutex[ecnBookID][stockSymbolIndex][BID_SIDE]);

		pthread_mutex_lock(&updateAskBidMutex[stockSymbolIndex]);
		
		// Free SkipList
		Destroy_List(stockBook[ecnBookID][BID_SIDE][stockSymbolIndex]);
		
		// Initialize SkipList
		Initialize_List(&stockBook[ecnBookID][BID_SIDE][stockSymbolIndex]);
		
		pthread_mutex_unlock(&updateAskBidMutex[stockSymbolIndex]);

		pthread_mutex_unlock(&deleteQuotesMgmtMutex[ecnBookID][stockSymbolIndex][BID_SIDE]);
		
	
		// Update BestQuote list
		UpdateMaxBidRoundLot(stockSymbolIndex, 1, YES, -1);
		UpdateMinAskRoundLot(stockSymbolIndex, 1, YES, -1);
		UpdateMaxBid(stockSymbolIndex, 0, -1);
		UpdateMinAsk(stockSymbolIndex, 0, -1);
	}
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	FlushECNSymbolStockBook
- Input:			N/A
- Output:		N/A
- Return:		N/A
- Description:	+ The routine hides the following facts
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
int FlushECNSymbolStockBook(int ecnBookID)
{
	if (manFlushBookFlag[ecnBookID] == NO)
	{
		return SUCCESS;
	}
	
	int stockSymbolIndex = manFlushBookStockSymbolIndex[ecnBookID];
	
	if (stockSymbolIndex < 0 || stockSymbolIndex >= MAX_STOCK_SYMBOL)
	{
		// Reset flag
		manFlushBookFlag[ecnBookID] = NO;
		
		return SUCCESS;
	}
	
	// Flush Ask Book
	FlushASideOfBook(ecnBookID, stockSymbolIndex, ASK_SIDE);
	
	// Flush Bid Book
	FlushASideOfBook(ecnBookID, stockSymbolIndex, BID_SIDE);
	
	// Reset flag
	manFlushBookFlag[ecnBookID] = NO;
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	DeleteAllExpiredArcaOrders
- Input:			stock symbol index
					valid trade sessions
- Output:			N/A
- Return:			N/A
- Description:		Delete all invalid (expired) orders from ARCA Stock Book
- Usage:			Call this function when receive Trading Session Change msg
					from ARCA
****************************************************************************/
int DeleteAllExpiredArcaOrders(const int stockSymbolIndex, const unsigned char validSession, const int sideToDelete)
{
	t_SkipList *list = stockBook[BOOK_ARCA][sideToDelete][stockSymbolIndex];
	
	// Check countNode of the list
	if (list->countNode == 0)
	{
		// The list hasn't any node
		return SUCCESS;
	}

	// Store nodes need to be deleted
	int icount = 0;
	t_OrderDetailNode *deletedNodes[100000];
	
	t_OrderDetailNode *x = list->head;
	while (x->forward[0] != NULL)
	{
		if ((x->forward[0]->info.tradeSession & validSession) == 0) //Delete this expired order
		{
			x = x->forward[0];

			if (icount < 100000)
			{
				deletedNodes[icount++] = x;
			}
			else
			{
				//We should increase deletedNodes size if come here! Basically this should not happed!
				TraceLog(ERROR_LEVEL, "ARCA Book error: There are so many orders need to be deleted for symbol '%.8s'!\n", symbolMgmt.symbolList[stockSymbolIndex].symbol);
				return ERROR;
			}
		}
		else
		{
			x = x->forward[0]; 		//We go to next order and check it
		}
	}

	// Delete detected nodes from stock book
	int i;
	for (i = 0; i < icount; i++)
	{
		DeletePartialHashingItem(BOOK_ARCA, deletedNodes[i]->info.refNum);
		DeleteBackupPartialHashingItem(BOOK_ARCA, deletedNodes[i]->info.refNum);
		
		if (GeneralDelete(stockBook[BOOK_ARCA][sideToDelete][stockSymbolIndex], deletedNodes[i], sideToDelete, stockSymbolIndex) != SUCCESS)
		{
			TraceLog(ERROR_LEVEL, "ARCA Book error: Cannot delete node %d %s %lf\n", deletedNodes[i]->info.refNum, symbolMgmt.symbolList[stockSymbolIndex].symbol, deletedNodes[i]->info.sharePrice);
			//return SUCCESS;
		}
	}	
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	FlushASideOfBook
- Input:			N/A
- Output:		N/A
- Return:		N/A
- Description:	+ The routine hides the following facts
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
int FlushASideOfBook(int ecnBookID, int stockSymbolIndex, int side)
{			
	if (pthread_mutex_trylock(&deleteQuotesMgmtMutex[ecnBookID][stockSymbolIndex][side]) == 0)
	{
		// Successfully lock acquiring
		DeleteAllQuoteOnASide(ecnBookID, stockSymbolIndex, side);
		
		pthread_mutex_unlock(&deleteQuotesMgmtMutex[ecnBookID][stockSymbolIndex][side]);
	}
	else
	{
		// Acquire a lock by LOCK()
		pthread_mutex_lock(&deleteQuotesMgmtMutex[ecnBookID][stockSymbolIndex][side]);
		
		DeleteAllQuoteOnASide(ecnBookID, stockSymbolIndex, side);
		
		pthread_mutex_unlock(&deleteQuotesMgmtMutex[ecnBookID][stockSymbolIndex][side]);
	}
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	DeleteQuotes
- Input:			N/A
- Output:			N/A
- Return:			N/A
- Description:	+ The routine hides the following facts
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
int DeleteQuotes(int ecnBookID, t_QuoteToDelete *quoteToDelete)
{
	t_OrderDetailNode *node;

	node = stockBook[ecnBookID][quoteToDelete->side][quoteToDelete->stockSymbolIndex]->head->forward[0];

	if (node != NULL)
	{
		if (quoteToDelete->side == ASK_SIDE)
		{
			while (node != NULL)
			{
				if (fle(node->info.sharePrice, quoteToDelete->sharePrice))
				{
					/*Before deleting, add to backup list */
					if (ecnBookID == BOOK_ARCA)
					{
						InsertBackupPartialHashingItem_ARCA(node->info.refNum, node->info.tradeSession);
					}
					else if (ecnBookID == BOOK_NASDAQ)
					{
						InsertBackupPartialHashingItem_NASDAQ(node->info.refNum, quoteToDelete->stockSymbolIndex, quoteToDelete->side);
					}
					
					// Delete old node
					DeletePartialHashingItem(ecnBookID, node->info.refNum);

					GeneralDelete(stockBook[ecnBookID][quoteToDelete->side][quoteToDelete->stockSymbolIndex], 
						node, quoteToDelete->side, quoteToDelete->stockSymbolIndex);
					
					// Try with next node
					node = stockBook[ecnBookID][quoteToDelete->side][quoteToDelete->stockSymbolIndex]->head->forward[0];
				}
				else
				{
					break;
				}
			}
		}
		else
		{
			while (node != NULL)
			{
				if (fge(node->info.sharePrice, quoteToDelete->sharePrice))
				{
					/*Before deleting, add to backup list */
					if (ecnBookID == BOOK_ARCA)
					{
						InsertBackupPartialHashingItem_ARCA(node->info.refNum, node->info.tradeSession);
					}
					else if (ecnBookID == BOOK_NASDAQ)
					{
						InsertBackupPartialHashingItem_NASDAQ(node->info.refNum, quoteToDelete->stockSymbolIndex, quoteToDelete->side);
					}
					
					// Delete old node
					DeletePartialHashingItem(ecnBookID, node->info.refNum);

					GeneralDelete(stockBook[ecnBookID][quoteToDelete->side][quoteToDelete->stockSymbolIndex], 
						node, quoteToDelete->side, quoteToDelete->stockSymbolIndex);
					
					// Try with next node
					node = stockBook[ecnBookID][quoteToDelete->side][quoteToDelete->stockSymbolIndex]->head->forward[0];
				}
				else
				{
					break;
				}
			}
		}
	}
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	DeleteAllQuoteOnASide
- Input:			N/A
- Output:			N/A
- Return:			N/A
- Description:	+ The routine hides the following facts
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
int DeleteAllQuoteOnASide(int ecnBookID, int stockSymbolIndex, int side)
{
	t_OrderDetailNode *node;

	node = stockBook[ecnBookID][side][stockSymbolIndex]->head->forward[0];

	while (node != NULL)
	{
		// Delete old node
		DeletePartialHashingItem(ecnBookID, node->info.refNum);
		DeleteBackupPartialHashingItem(ecnBookID, node->info.refNum);
		
		GeneralDelete(stockBook[ecnBookID][side][stockSymbolIndex], 
			node, side, stockSymbolIndex);
		
		// Try with next node
		node = stockBook[ecnBookID][side][stockSymbolIndex]->head->forward[0];
	}
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	GetBestOrder
- Input:			N/A
- Output:		N/A
- Return:		N/A
- Description:	+ The routine hides the following facts
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
int GetBestOrder(t_TopOrder *bestOrder, int side, int stockSymbolIndex)
{
	t_TopOrder *currentTopOrder = &bestQuote[side][stockSymbolIndex];

	*bestOrder = *currentTopOrder;
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	InsertPartialHashingItem
- Input:			N/A
- Output:		N/A
- Return:		N/A
- Description:	+ The routine hides the following facts
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
int InsertPartialHashingItem(t_OrderDetailNode *node, int ecnBookID, long refNum, 
							int stockSymbolIndex, char side)
{
	int remainder;
	int quotient;
	int i = 0;
	
	switch (ecnBookID)
	{	
		/*
		Calculate and store partial hashing item into 
		ARCA Partial Hashing data structure
		*/
		case BOOK_ARCA:
			quotient = refNum / ARCA_MAX_DIVISOR;
			remainder = refNum % ARCA_MAX_DIVISOR;
			
			if (remainder < 0)
			{
				remainder = -remainder;
			}
			
			for (i = 0; i < ARCA_MAX_SAME_REMAINDER; i++)
			{
				// We find position to store new Info2Find
				if (ARCA_PartialHashing[remainder][i].stockSymbolIndex == -1)
				{
					
					ARCA_PartialHashing[remainder][i].quotient = quotient;
					ARCA_PartialHashing[remainder][i].stockSymbolIndex = stockSymbolIndex;
					ARCA_PartialHashing[remainder][i].side = side;
					ARCA_PartialHashing[remainder][i].node = node;
					
					// Successfully
					return SUCCESS;
				}
			}
			
			break;
		/*
		Calculate and store partial hashing item into 
		NASDAQ Partial Hashing data structure
		*/
		case BOOK_NASDAQ:
		
			quotient = refNum / NASDAQ_MAX_DIVISOR;
			remainder = refNum % NASDAQ_MAX_DIVISOR;
			
			if (remainder < 0)
			{
				remainder = -remainder;
			}
			
			for (i = 0; i < NASDAQ_MAX_SAME_REMAINDER; i++)
			{
				// We find position to store new Info2Find
				if (NASDAQ_PartialHashing[remainder][i].stockSymbolIndex == -1)
				{
					NASDAQ_PartialHashing[remainder][i].quotient = quotient;
					NASDAQ_PartialHashing[remainder][i].stockSymbolIndex = stockSymbolIndex;
					NASDAQ_PartialHashing[remainder][i].side = side;
					NASDAQ_PartialHashing[remainder][i].node = node;
					
					// Successfully
					return SUCCESS;
				}
			}
			
			break;
		default:
			return ERROR;
	}
	
	TraceLog(ERROR_LEVEL, "Partial Hashing data structure is full (ECN BOOK ID: %d)\n", ecnBookID);
	
	return ERROR;
}


/****************************************************************************
- Function name:		RegisterPartialHashingItem_ARCA
- Input:			N/A
- Output:			+ rowIndex
					+ columnIndex
- Return:			SUCCESS or ERROR
- Description:		Find position of item to insert into partial hashing list, after register this item in this list
- Usage:			N/A
****************************************************************************/
int RegisterPartialHashingItem_ARCA(long refNum, int *rowIndex, int *columnIndex)
{
	*rowIndex = refNum % ARCA_MAX_DIVISOR;
	
	if (*rowIndex < 0)
	{
		*rowIndex *= -1;
	}

	for (*columnIndex = 0; *columnIndex < ARCA_MAX_SAME_REMAINDER; (*columnIndex)++)
	{
		// We find position to store partial hashing item
		if (ARCA_PartialHashing[*rowIndex][*columnIndex].stockSymbolIndex == -1)
		{
			ARCA_PartialHashing[*rowIndex][*columnIndex].stockSymbolIndex = -2;
			
			return SUCCESS;
		}
	}
	
	TraceLog(ERROR_LEVEL, "ARCA Partial Hashing List is full (refNum = %ld)\n", refNum);
	
	return ERROR;
}

/****************************************************************************
- Function name:	InsertPartialHashingItem_ARCA
- Input:			N/A
- Output:			N/A
- Return:			N/A
- Description:		+ The routine hides the following facts
					+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
int InsertPartialHashingItem_ARCA(t_OrderDetailNode *node, long refNum, int stockSymbolIndex, char side, int rowIndex, int columnIndex)
{
	/*
	Calculate and store partial hashing item into 
	ARCA Partial Hashing data structure
	*/
	
	// Check item at position that registered
	if (ARCA_PartialHashing[rowIndex][columnIndex].stockSymbolIndex == -2)
	{
		ARCA_PartialHashing[rowIndex][columnIndex].stockSymbolIndex = stockSymbolIndex;
		ARCA_PartialHashing[rowIndex][columnIndex].quotient = refNum / ARCA_MAX_DIVISOR;					
		ARCA_PartialHashing[rowIndex][columnIndex].side = side;
		ARCA_PartialHashing[rowIndex][columnIndex].node = node;
		
		return SUCCESS;
	}
	else
	{
		// Find new positon of item to insert into partial hashing list
		int i = 0;
		for (i = 0; i < ARCA_MAX_SAME_REMAINDER; i++)
		{
			// We find position to store new Info2Find
			if (ARCA_PartialHashing[rowIndex][i].stockSymbolIndex == -1)
			{
				ARCA_PartialHashing[rowIndex][i].stockSymbolIndex = stockSymbolIndex;
				ARCA_PartialHashing[rowIndex][i].quotient = refNum / ARCA_MAX_DIVISOR;					
				ARCA_PartialHashing[rowIndex][i].side = side;
				ARCA_PartialHashing[rowIndex][i].node = node;
								
				return SUCCESS;
			}
		}
	}
	
	TraceLog(ERROR_LEVEL, "ARCA Partial Hashing List is full (refNum = %ld).\n", refNum);
	
	return ERROR;
}

/****************************************************************************
- Function name:	UpdatePartialHashingItem_ARCA
- Input:			N/A
- Output:			N/A
- Return:			N/A
- Description:		+ The routine hides the following facts
					+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
int UpdatePartialHashingItem_ARCA(t_OrderDetailNode *node, long refNum)
{
	int quotient = refNum / ARCA_MAX_DIVISOR;
	int remainder = refNum % ARCA_MAX_DIVISOR;
	
	int i = 0;
	for (i = 0; i < ARCA_MAX_SAME_REMAINDER; i++)
	{
		if (ARCA_PartialHashing[remainder][i].quotient == quotient)
		{
			ARCA_PartialHashing[remainder][i].node = node;
			
			return SUCCESS;
		}
	}
	
	return ERROR;
}

/****************************************************************************
- Function name:	FindPartialHashingItem
- Input:			N/A
- Output:		N/A
- Return:		t_OrderDetailNode or t_PartialHashingItem or 
				t_ARCA_PartialHashingItem or NULL
- Description:	+ The routine hides the following facts
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
void* FindPartialHashingItem(int ecnBookID, long refNum)
{
	int remainder;
	int quotient;
	int i = 0;
	
	switch (ecnBookID)		
	{
		case BOOK_ARCA:
			quotient = refNum / ARCA_MAX_DIVISOR;
			remainder = refNum % ARCA_MAX_DIVISOR;
			
			if (remainder < 0)
			{
				remainder = -remainder;
			}

			for (i = 0; i < ARCA_MAX_SAME_REMAINDER; i++)
			{
				// We find position to store partial hashing item
				if (ARCA_PartialHashing[remainder][i].quotient == quotient)
				{
					return &ARCA_PartialHashing[remainder][i];
				}
			}
			break;
		/*
		Calculate and find NASDAQ partial hashing item
		*/
		case BOOK_NASDAQ:
			quotient = refNum / NASDAQ_MAX_DIVISOR;
			remainder = refNum % NASDAQ_MAX_DIVISOR;
			
			if (remainder < 0)
			{
				remainder = -remainder;
			}

			for (i = 0; i < NASDAQ_MAX_SAME_REMAINDER; i++)
			{
				// We find position to store partial hashing item
				if (NASDAQ_PartialHashing[remainder][i].quotient == quotient)
				{
					return &NASDAQ_PartialHashing[remainder][i];
				}
			}
			
			break;
		default:
			return NULL;
	}
	
	return NULL;
}

/****************************************************************************
- Function name:	FindBackupPartialHashingItem
- Input:			
- Output:		
- Return:		
				
- Description:	
				
- Usage:		
****************************************************************************/
void* FindBackupPartialHashingItem(int ecnBookID, long refNum)
{
	register int remainder;
	register int quotient;
	register int i = 0;
	
	switch (ecnBookID)		
	{
		case BOOK_ARCA:
			quotient = refNum / ARCA_MAX_DIVISOR;
			remainder = refNum % ARCA_MAX_DIVISOR;

			for (i = 0; i < ARCA_MAX_SAME_REMAINDER; i++)
			{
				// We find position to store partial hashing item
				if (ARCA_HashingBackup[remainder][i].quotient == quotient)
				{
					return &ARCA_HashingBackup[remainder][i];
				}
			}
			break;

		case BOOK_NASDAQ:
			quotient = refNum / NASDAQ_MAX_DIVISOR;
			remainder = refNum % NASDAQ_MAX_DIVISOR;
			
			for (i = 0; i < NASDAQ_MAX_SAME_REMAINDER; i++)
			{
				// We find position to store partial hashing item
				if (NASDAQ_HashingBackup[remainder][i].quotient == quotient)
				{
					return &NASDAQ_HashingBackup[remainder][i];
				}
			}
			
			break;
		default:
			return NULL;
	}
	
	return NULL;
}

/****************************************************************************
- Function name:	InsertBackupPartialHashingItem_NASDAQ
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int InsertBackupPartialHashingItem_NASDAQ(long refNum, int stockSymbolIndex, int side)
{
	register int remainder;
	register int quotient;
	register int i = 0;

	quotient = refNum / NASDAQ_MAX_DIVISOR;
	remainder = refNum % NASDAQ_MAX_DIVISOR;
			
	for (i = 0; i < NASDAQ_MAX_SAME_REMAINDER; i++)
	{
		// We find position to store new Info2Find
		if (NASDAQ_HashingBackup[remainder][i].quotient == -1)
		{
			NASDAQ_HashingBackup[remainder][i].stockSymbolIndex = stockSymbolIndex;
			NASDAQ_HashingBackup[remainder][i].quotient = quotient;					
			NASDAQ_HashingBackup[remainder][i].side = side;
					
			return SUCCESS;
		}
	}

	TraceLog(ERROR_LEVEL, "Backup Partial Hashing data structure is full (ECN: NASDAQ, refNum = %d)\n", refNum);
	
	return ERROR;
}

/****************************************************************************
- Function name:	InsertBackupPartialHashingItem_ARCA
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int InsertBackupPartialHashingItem_ARCA(long refNum, unsigned char tradeSession)
{
	register int remainder;
	register int quotient;
	register int i = 0;

	quotient = refNum / ARCA_MAX_DIVISOR;
	remainder = refNum % ARCA_MAX_DIVISOR;

	for (i = 0; i < ARCA_MAX_SAME_REMAINDER; i++)
	{
		// We find position to store new Info2Find
		if (ARCA_HashingBackup[remainder][i].quotient == -1)
		{
			ARCA_HashingBackup[remainder][i].quotient = quotient;					
			ARCA_HashingBackup[remainder][i].tradeSession = tradeSession;
			return SUCCESS;
		}
	}

	TraceLog(ERROR_LEVEL, "Backup Partial Hashing data structure is full (ECN: ARCA, refNum = %d)\n", refNum);
	
	return ERROR;
}

/****************************************************************************
- Function name:	DeletePartialHashingItem
- Input:			N/A
- Output:		N/A
- Return:		N/A
- Description:	+ The routine hides the following facts
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
int DeletePartialHashingItem(int ecnBookID, long refNum)
{
	int remainder;
	int quotient;
	int i = 0;

	switch (ecnBookID)
	{
		/*
		Find and delete ARCA partial hashing item
		*/
		case BOOK_ARCA:
			quotient = refNum / ARCA_MAX_DIVISOR;
			remainder = refNum % ARCA_MAX_DIVISOR;
			
			if (remainder < 0)
			{
				remainder = -remainder;
			}
			
			for (i = 0; i < ARCA_MAX_SAME_REMAINDER; i++)
			{
				if (ARCA_PartialHashing[remainder][i].quotient == quotient)
				{
					ARCA_PartialHashing[remainder][i].quotient = -1;
					ARCA_PartialHashing[remainder][i].side = 0;
					ARCA_PartialHashing[remainder][i].stockSymbolIndex = -1;
					ARCA_PartialHashing[remainder][i].node = NULL;
					
					// Successfully
					return SUCCESS;
				}
			}
			
			break;
		/*
		Find and delete NASDAQ partial hashing item
		*/
		case BOOK_NASDAQ:
			quotient = refNum / NASDAQ_MAX_DIVISOR;
			remainder = refNum % NASDAQ_MAX_DIVISOR;
			
			if (remainder < 0)
			{
				remainder = -remainder;
			}
			
			for (i = 0; i < NASDAQ_MAX_SAME_REMAINDER; i++)
			{
				if (NASDAQ_PartialHashing[remainder][i].quotient == quotient)
				{
					NASDAQ_PartialHashing[remainder][i].quotient = -1;
					NASDAQ_PartialHashing[remainder][i].side = 0;
					NASDAQ_PartialHashing[remainder][i].stockSymbolIndex = -1;
					NASDAQ_PartialHashing[remainder][i].node = NULL;
					
					// Successfully
					return SUCCESS;
				}
			}
			
			break;
		default:
			return ERROR;
	}
	
	return ERROR;
}

/****************************************************************************
- Function name:	DeleteBackupPartialHashingItem
- Input:			N/A
- Output:		N/A
- Return:		N/A
- Description:	+ The routine hides the following facts
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
int DeleteBackupPartialHashingItem(int ecnBookID, long refNum)
{
	register int remainder;
	register int quotient;
	register int i = 0;

	switch (ecnBookID)
	{
		case BOOK_ARCA:
			quotient = refNum / ARCA_MAX_DIVISOR;
			remainder = refNum % ARCA_MAX_DIVISOR;
			
			for (i = 0; i < ARCA_MAX_SAME_REMAINDER; i++)
			{
				if (ARCA_HashingBackup[remainder][i].quotient == quotient)
				{
					ARCA_HashingBackup[remainder][i].quotient = -1;
					return SUCCESS;
				}
			}
			
			break;

		case BOOK_NASDAQ:
			quotient = refNum / NASDAQ_MAX_DIVISOR;
			remainder = refNum % NASDAQ_MAX_DIVISOR;

			for (i = 0; i < NASDAQ_MAX_SAME_REMAINDER; i++)
			{
				if (NASDAQ_HashingBackup[remainder][i].quotient == quotient)
				{
					NASDAQ_HashingBackup[remainder][i].quotient = -1;
					return SUCCESS;
				}
			}
			
			break;
		default:
			return ERROR;
	}
	
	return ERROR;
}

/****************************************************************************
- Function name:	UpdateMaxBidRoundLot
- Input:			N/A
- Output:		N/A
- Return:		N/A
- Description:	+ The routine hides the following facts
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
int UpdateMaxBidRoundLot(int stockSymbolIndex, const int isPlaceOrder, int needLock, int ecnHalted)
{
	t_TopOrder *currentTopOrder = &bestQuoteRoundLot[BID_SIDE][stockSymbolIndex];
	
	// Get mutex lock to synchronize update/access on current Best Quote
	pthread_mutex_lock(&updateAskBidMutex[stockSymbolIndex]);
	
	t_TopOrder topOrderArray[MAX_BOOK_CONNECTIONS];
	
	// Get Top Quotes
	GetTopQuotesRoundLot(topOrderArray, BID_SIDE, stockSymbolIndex, needLock, ecnHalted);
	
	// Get best quote
	t_TopOrder *retValue = GetBestQuoteRoundLot(topOrderArray, BID_SIDE);
	
	if (retValue != NULL)
	{
		currentTopOrder->info = retValue->info;
		currentTopOrder->ecnIndicator = retValue->ecnIndicator;
	}
	else
	{
		currentTopOrder->ecnIndicator = -1;
		currentTopOrder->info.sharePrice = 0;
		currentTopOrder->info.shareVolume = 0;
	}
	
	// Release mutex lock
	pthread_mutex_unlock(&updateAskBidMutex[stockSymbolIndex]);

	// Check BBO orders placed
	if (isPlaceOrder == 1)
	{
		if (retValue != NULL)
		{
			return CheckBBOOrder(stockSymbolIndex, BID_SIDE, retValue->ecnIndicator, retValue->info.refNum, retValue->info.shareVolume, retValue->info.sharePrice, ROUND_LOT_TYPE);
		}
		else if(positionCollection[stockSymbolIndex].isTrading == YES)	// Best BID is empty
		{
			ProcessDisengageSymbol(positionCollection[stockSymbolIndex].stuck.symbol, TRADER_REQUEST);
			SendPMDisengageAlertToAS(&positionCollection[stockSymbolIndex].stuck, DISENGAGE_REASON_BEST_BID_NOTHING);
			return SUCCESS;
		}
	}
	
	return ERROR;
}

/****************************************************************************
- Function name:	UpdateMinAskRoundLot
- Input:			N/A
- Output:		N/A
- Return:		N/A
- Description:	+ The routine hides the following facts
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
int UpdateMinAskRoundLot(int stockSymbolIndex, const int isPlaceOrder, int needLock, int ecnHalted)
{
	t_TopOrder *currentTopOrder = &bestQuoteRoundLot[ASK_SIDE][stockSymbolIndex];
	
	// Get mutex lock to synchronize update/access on current Best Quote
	pthread_mutex_lock(&updateAskBidMutex[stockSymbolIndex]);
	
	t_TopOrder topOrderArray[MAX_BOOK_CONNECTIONS];
	
	// Get Top Quotes
	GetTopQuotesRoundLot(topOrderArray, ASK_SIDE, stockSymbolIndex, needLock, ecnHalted);
	
	// Get Best Quote
	t_TopOrder *retValue = GetBestQuoteRoundLot(topOrderArray, ASK_SIDE);
	
	if (retValue != NULL)
	{
		currentTopOrder->info = retValue->info;
		currentTopOrder->ecnIndicator = retValue->ecnIndicator;
	}
	else
	{
		currentTopOrder->ecnIndicator = -1;
		currentTopOrder->info.sharePrice = 0;
		currentTopOrder->info.shareVolume = 0;
	}
	
	// Release mutex lock
	pthread_mutex_unlock(&updateAskBidMutex[stockSymbolIndex]);

	// Check BBO orders placed
	if (isPlaceOrder == 1)
	{
		if (retValue != NULL)
		{
			return CheckBBOOrder(stockSymbolIndex, ASK_SIDE, retValue->ecnIndicator, retValue->info.refNum, retValue->info.shareVolume, retValue->info.sharePrice, ROUND_LOT_TYPE);
		}
		else if(positionCollection[stockSymbolIndex].isTrading == YES)	// Best ASK is empty
		{
			ProcessDisengageSymbol(positionCollection[stockSymbolIndex].stuck.symbol, TRADER_REQUEST);
			SendPMDisengageAlertToAS(&positionCollection[stockSymbolIndex].stuck, DISENGAGE_REASON_BEST_ASK_NOTHING);
			return SUCCESS;
		}
	}
	
	return ERROR;
}

/****************************************************************************
- Function name:	GetTopQuotesRoundLot
- Input:			N/A
- Output:		N/A
- Return:		N/A
- Description:	+ The routine hides the following facts
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
void GetTopQuotesRoundLot(t_TopOrder topOrderArray[MAX_BOOK_CONNECTIONS], int side, int stockSymbolIndex, int needLock, int ecnHalted)
{
	int ecnIndex;
	t_SkipList *currentList;
	t_OrderDetailNode *currentNode;
	
	for (ecnIndex = 0; ecnIndex < MAX_BOOK_CONNECTIONS; ecnIndex++)
	{
		if(ecnIndex == ecnHalted)
		{
			topOrderArray[ecnIndex].ecnIndicator = -1;
			continue;
		}
		
		// Check to know the stock book of ecn book is allowed to cross trade or not
		if (bookStatusMgmt[ecnIndex].isConnected == CONNECTED && 
			bookStatusMgmt[ecnIndex].shouldConnect == YES)
		{
			if (needLock == YES)
			{
				pthread_mutex_lock(&deleteQuotesMgmtMutex[ecnIndex][stockSymbolIndex][side]);
			}
			
			currentList = stockBook[ecnIndex][side][stockSymbolIndex];
			
			if (currentList == NULL)
			{
				topOrderArray[ecnIndex].ecnIndicator = -1;
				if (needLock == YES)
				{
					pthread_mutex_unlock(&deleteQuotesMgmtMutex[ecnIndex][stockSymbolIndex][side]);
				}
				continue;
			}
			
			currentNode = currentList->head;
			
			if (currentNode == NULL)
			{
				topOrderArray[ecnIndex].ecnIndicator = -1;
				if (needLock == YES)
				{
					pthread_mutex_unlock(&deleteQuotesMgmtMutex[ecnIndex][stockSymbolIndex][side]);
				}
				continue;
			}
			
			topOrderArray[ecnIndex].ecnIndicator = -1;

			while (currentNode->forward[0] != NULL)
			{
				topOrderArray[ecnIndex].info = currentNode->forward[0]->info;
				
				if (fle(topOrderArray[ecnIndex].info.sharePrice, 0.0))
				{
					break;
				}
				else if (topOrderArray[ecnIndex].info.shareVolume > MAX_ODD_LOT_SHARE)
				{
					topOrderArray[ecnIndex].ecnIndicator = ecnIndex;
					
					break;
				}
				else
				{
					currentNode = currentNode->forward[0];
				}
			}
			
			if (needLock == YES)
			{
				pthread_mutex_unlock(&deleteQuotesMgmtMutex[ecnIndex][stockSymbolIndex][side]);
			}
		}
		else
		{
			// If ecnIndicator equals -1 that means the top order cannot be used
			topOrderArray[ecnIndex].ecnIndicator = -1;
		}
	}
}
	
/****************************************************************************
- Function name:	GetBestQuoteRoundLot
- Input:			N/A
- Output:		N/A
- Return:		N/A
- Description:	+ The routine hides the following facts
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
t_TopOrder *GetBestQuoteRoundLot(t_TopOrder topOrderArray[MAX_BOOK_CONNECTIONS], int side)
{
	int ecnIndex;
	
	// We assume that the first item in array is best
	t_TopOrder *bestQuote = &topOrderArray[0];
	
	if (side == BID_SIDE)
	{
		for (ecnIndex = 1; ecnIndex < MAX_BOOK_CONNECTIONS; ecnIndex++)
		{
			// Check Top Order to know that it is available to access
			if (topOrderArray[ecnIndex].ecnIndicator != -1)
			{
				if (bestQuote->ecnIndicator == -1)
				{
					bestQuote = &topOrderArray[ecnIndex];
					
					continue;
				}
				else
				{
					if (flt(bestQuote->info.sharePrice, topOrderArray[ecnIndex].info.sharePrice))
					{
						bestQuote = &topOrderArray[ecnIndex];
					}
				}
			}
		}
	}
	else
	{
		for (ecnIndex = 1; ecnIndex < MAX_BOOK_CONNECTIONS; ecnIndex++)
		{
			// Check Top Order to know that it is available to access
			if (topOrderArray[ecnIndex].ecnIndicator != -1)
			{
				if (bestQuote->ecnIndicator == -1)
				{
					bestQuote = &topOrderArray[ecnIndex];
					
					continue;
				}
				else
				{
					if (fgt(bestQuote->info.sharePrice, topOrderArray[ecnIndex].info.sharePrice))
					{
						bestQuote = &topOrderArray[ecnIndex];
					}
				}
			}
		}
	}
	
	// Check before return for calling the routine
	if (bestQuote->ecnIndicator != -1)
	{
		return bestQuote;
	}
	else
	{
		return NULL;
	}
}

/****************************************************************************
- Function name:	GetBestOrderRoundLot
- Input:			N/A
- Output:		N/A
- Return:		N/A
- Description:	+ The routine hides the following facts
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
int GetBestOrderRoundLot(t_TopOrder *bestOrder, int side, int stockSymbolIndex)
{
	t_TopOrder *currentTopOrder = &bestQuoteRoundLot[side][stockSymbolIndex];

	*bestOrder = *currentTopOrder;
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	ThreadReceiveBookData
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
void *ThreadReceiveBookData(void *args)
{
	dbl_device_t *dev = (dbl_device_t *)args;
	t_DBLInstance *instance = GetDBLInstance(*dev);
	if (instance == NULL)
	{
		TraceLog(ERROR_LEVEL, "ThreadReceiveBookData: Could not get DBL instance\n");
		return NULL;
	}
	
	instance->isTimedOut = 0;
	
	char threadName[64];
	sprintf(threadName, "[%s]", instance->macAddress);
	
	SetKernelAlgorithm(threadName);
	
	//DBL Receive info
	char buffer[MAX_MTU];
	struct dbl_recv_info rInfo;

	// *** Warning: If using poll() to implement socket timeout, do not use a large amount of timeout
	//				Must use *multiple* small amount of timeout. Because poll() will block until timeout even when DBL device is closed!
	//				So this will create a huge delay when we disconnect the book --> because it must wait for poll() to time out!

	struct pollfd pfd;
	pfd.events = POLLIN;
	pfd.fd = dbl_device_handle(*dev);

	t_DataInfo *dataInfo;
	int timeoutCounter = 0;
	int returnValue;
	int nextIndex;

	/*Note about heartbeat message on various Feeds:
		- ARCA XDP Integrated: Heartbeat is sent after 60 seconds if there is no data
		- Nasdaq Book: Heartbeat is sent after 1 second if there is no data
		- BATSZ Book: Heartbeat is sent after 1 scond if there is no data
		- CQS: Line-Integrity is sent after approximately 1 minute
		- UQDF: Line-Integrity is sent after approximately 1 minute
		- CTS: Line-Integrity is sent after approximately 1 minute
		- UTDF: Line-Integrity is sent after approximately 1 minute
		
		So, we must set timeout for DBL device around 60 seconds at least
	*/
		
	while (*dev)
	{
		// Begin receive data from multicast group
		returnValue = poll(&pfd, 1, 1000);	// Receive timeout = 1 second

		if (returnValue > 0)	//Have something to read
		{
			if (*dev == NULL)
			{
				return NULL;
			}

			dbl_recvfrom(*dev, DBL_RECV_NONBLOCK, buffer, MAX_MTU, &rInfo);

			if (rInfo.msg_len > 0)
			{
				dataInfo = (t_DataInfo *)rInfo.chan_context;
				
				nextIndex = dataInfo->currentReceivedIndex;
				if(dataInfo->buffer[nextIndex][MAX_MTU - 1] == STORED)
				{
					if (dataInfo->connectionFlag == 1)
					{
						TraceLog(ERROR_LEVEL, "%s: Buffer is full: receivedIndex = %d, readIndex = %d\n", dataInfo->bookName, nextIndex, dataInfo->currentReadIndex);
						dataInfo->DisconnectBook();
					}
					timeoutCounter = 0;
					continue;
				}

				memcpy(dataInfo->buffer[nextIndex], buffer, rInfo.msg_len);
				// dataInfo->timestamp[nextIndex] = rInfo.timestamp / 1000 - hbitime_get_offset_micros();

				dataInfo->buffer[nextIndex][MAX_MTU - 1] = STORED;

				if (dataInfo->currentReceivedIndex == MAX_BOOK_MSG_IN_BUFFER - 1)
				{
					dataInfo->currentReceivedIndex = 0;
				}
				else
				{
					dataInfo->currentReceivedIndex++;
				}

				if (sem_post(&dataInfo->sem) != SUCCESS)
				{
					TraceLog(ERROR_LEVEL, "%s: Call to sem_post() system call failed!\n", dataInfo->bookName);
					if (dataInfo->connectionFlag == 1)
					{
						dataInfo->DisconnectBook();
					}
				}
				
				timeoutCounter = 0;	
			}
			else
			{
				TraceLog(ERROR_LEVEL, "ThreadReceiveBookData: couldn't receive anything from multicast groups, poll() = %d, revents = %d\n", returnValue, pfd.revents);
				TerminateBookReceivingThread(dev);
				return NULL;
			}
		}
		else if (returnValue < 0)	//Poll problem!
		{
			PrintErrStr(ERROR_LEVEL, "ThreadReceiveBookData poll(): ", errno);
			TerminateBookReceivingThread(dev);
			return NULL;
		}
		else if (returnValue == 0)	//Timed out
		{
			timeoutCounter++;
			if (timeoutCounter > 61)
			{
				TraceLog(ERROR_LEVEL, "ThreadReceiveBookData: we couldn't receive anything from multicast groups (Receive Timeout)\n");
				TerminateBookReceivingThread(dev);
				return NULL;
			}

			continue;
		}
	}

	//TerminateBookReceivingThread(dev);
	return NULL;
}

void TerminateBookReceivingThread(dbl_device_t *dev)
{
	/* Disconnect all book threads which is on current dbl device
		Then, close the device
	*/

	pthread_mutex_lock(&DBLMgmt.mutexLock);
	
	if (dev == NULL)
	{
		pthread_mutex_unlock(&DBLMgmt.mutexLock);
		return;
	}
	
	if (*dev == NULL)
	{
		pthread_mutex_unlock(&DBLMgmt.mutexLock);
		return;
	}
	
	int i;
	for (i = 0; i < MAX_DBL_INSTANCE; i++)
	{
		if (DBLMgmt.dblInstance[i].dev == *dev)
		{
			DBLMgmt.dblInstance[i].isTimedOut = 1;
			break;
		}
	}
	
	//Nasdaq
	if (NDAQ_MoldUDP_Book.Data.dev == *dev)
	{
		DisconnectNASDAQBook();
	}
	
	//Arca
	if (ARCA_Book_Conf.dataDev == *dev)
	{
		DisconnectARCABook();
	}
	
	//cqs
	if (CQS_Feed_Conf.dev == *dev)
	{
		DisconnectCQS_Feed();
	}
	
	//uqdf
	if (UQDF_Conf.dev == *dev)
	{
		DisconnectUQDF_Multicast();
	}
	
	//cts
	if (CTS_Feed_Conf.dev == *dev)
	{
		DisconnectCTS_Feed();
	}
	
	//utdf
	if (UTDF_Conf.dev == *dev)
	{
		DisconnectUTDF_Multicast();
	}
	
	//Close DBL device
	for (i = 0; i < MAX_DBL_INSTANCE; i++)
	{
		if (DBLMgmt.dblInstance[i].dev == *dev)
		{
			DBLMgmt.dblInstance[i].instanceCounter = 0;
			dbl_close(DBLMgmt.dblInstance[i].dev);
			DBLMgmt.dblInstance[i].macAddress[0] = 0;
			DBLMgmt.dblInstance[i].dev = NULL;
			*dev = NULL;
			pthread_mutex_unlock(&DBLMgmt.mutexLock);
			return;
		}
	}

	pthread_mutex_unlock(&DBLMgmt.mutexLock);
	return;
}
