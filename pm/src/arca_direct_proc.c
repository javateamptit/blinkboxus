/****************************************************************************
** Project name:	Equity Arbitrage Application
** File name: 	arca_direct_proc.c
** Description: 	This file contains function definitions that were declared
				in arca_direct_proc.h

** Author: 		Sang Nguyen-Minh
** First created on 18 September 2007
** Last updated on 18 September 2007
****************************************************************************/

#define _ISOC9X_SOURCE
//#define _XOPEN_SOURCE 500

#include <unistd.h>
#include <sys/time.h>

#include "configuration.h"
#include "socket_util.h"
#include "raw_data_mgmt.h"
#include "arca_direct_proc.h"
#include "kernel_algorithm.h"

/****************************************************************************
					GLOBAL VARIABLES
****************************************************************************/

static double _DIRECT_PRICE_PORTION[10];

t_ARCA_DIRECT_Config ARCA_DIRECT_Config;
static pthread_mutex_t ADSockMutex = PTHREAD_MUTEX_INITIALIZER;

/****************************************************************************
** Function definitions
****************************************************************************/

/****************************************************************************
- Function name:	Init_ARCA_DIRECT_DataStructure
- Input:			N/A
- Output:		Update some global variables
- Return:		N/A
- Description:	+ The routine hides the following facts
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
int Init_ARCA_DIRECT_DataStructure(void)
{
	_DIRECT_PRICE_PORTION[0] = 1.0;
	_DIRECT_PRICE_PORTION[1] = 0.1;
	_DIRECT_PRICE_PORTION[2] = 0.01;
	_DIRECT_PRICE_PORTION[3] = 0.001;
	_DIRECT_PRICE_PORTION[4] = 0.0001;
	_DIRECT_PRICE_PORTION[5] = 0.00001;
	_DIRECT_PRICE_PORTION[6] = 0.000001;
	_DIRECT_PRICE_PORTION[7] = 0.0000001;
	_DIRECT_PRICE_PORTION[8] = 0.00000001;
	_DIRECT_PRICE_PORTION[9] = 0.000000001;
	
	// Load incoming and outgoing sequence number from file
	if (LoadSequenceNumber("arca_direct_seqs", ARCA_DIRECT_INDEX, &ARCA_DIRECT_Config) == ERROR)
	{	
		TraceLog(ERROR_LEVEL, "Cannot load arca_direct_seqs file\n");
		return ERROR;
	}

	if (ARCA_DIRECT_Config.socket < 1)
	{
		ARCA_DIRECT_Config.socket = -1;
	}
	else
	{
		close(ARCA_DIRECT_Config.socket);
		ARCA_DIRECT_Config.socket = -1;
	}
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	StartUp_ARCA_DIRECT_Module
- Input:			+ threadArgs
- Output:		N/A
- Return:		Success or Failure
- Description:	+ The routine hides the following facts
				+ There are no preconditions guaranteed to the routine
				+ The routine guarantees that the return value will 
						have a value of either
				  - Success
					or
				  - Failure
- Usage:			N/A
****************************************************************************/
void *StartUp_ARCA_DIRECT_Module(void *threadArgs)
{
	SetKernelAlgorithm ("[ARCA_DIRECT_RECEIVE_PROCESS_MSGS]");
	if (Init_ARCA_DIRECT_DataStructure() == ERROR)
	{	
		orderStatusMgmt[ARCA_DIRECT_INDEX].shouldConnect = NO;
		return NULL;
	}
	
	TraceLog(DEBUG_LEVEL, "Trying connect to ARCA DIRECT Server\n");
	// Connect to ARCA DIRECT Server
	ARCA_DIRECT_Config.socket = Connect2Server(ARCA_DIRECT_Config.ipAddress, ARCA_DIRECT_Config.port, "ARCA DIRECT");
	
	if (ARCA_DIRECT_Config.socket == ERROR)
	{
		TraceLog(ERROR_LEVEL, "Cannot connect to ARCA DIRECT Server\n");
		orderStatusMgmt[ARCA_DIRECT_INDEX].shouldConnect = NO;
		return NULL;
	}
	
	/*
	If success, build logon request message and send it to server
	+ Try to receive logon accepted message or logon rejected message
	+ With Logon Accepted Message: get Last Sequence Number from response message
	and update outgoing sequence number at local
	+ With Logon Rejected Message: print something and exit module
	*/
	if (SetSocketRecvTimeout(ARCA_DIRECT_Config.socket, ARCA_DIRECT_RECV_TIMEOUT) == ERROR)
	{
		close(ARCA_DIRECT_Config.socket);
		ARCA_DIRECT_Config.socket = -1;
		orderStatusMgmt[ARCA_DIRECT_INDEX].shouldConnect = NO;
		return NULL;
	}
	
	// Talk to ARCA DIRECT Server
	TalkTo_ARCA_DIRECT_Server();
		
	/*
	Stop execution of the module
	*/
	if (ARCA_DIRECT_Config.socket > 0)
	{
		close(ARCA_DIRECT_Config.socket);
		ARCA_DIRECT_Config.socket = -1;
	}
	
	orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected = DISCONNECTED;
	orderStatusMgmt[ARCA_DIRECT_INDEX].shouldConnect = NO;
	
	asCollection.orderConnection[ARCA_DIRECT_INDEX].status = DISCONNECTED;
	
	SendPMDisconnectedBookOrderAlertToAS(SERVICE_ARCA_DIRECT);
	CheckAndSendAlertDisableTrading(2);
	
	TraceLog(DEBUG_LEVEL, "Connection to ARCA DIRECT Server was disconnected\n");
	
	return NULL;
}

/****************************************************************************
- Function name:	Send_Test_Request_Thread
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
void *Send_Test_Request_Thread(void *arg)
{
	TraceLog(DEBUG_LEVEL, "ARCA DIRECT: Start thread to auto send Test Request message ...\n");
	unsigned int countTimeInSencond = 0;
	while(orderStatusMgmt[ARCA_DIRECT_INDEX].shouldConnect == YES)
	{
		//Send Test Request after 30 seconds
		if(countTimeInSencond % 30 == 0)
		{
			if (BuildAndSend_ARCA_DIRECT_TestRequestMsg() == ERROR)
			{
				TraceLog(ERROR_LEVEL, "ARCA DIRECT: Killed thread send Test Request due to error send Test Request\n");
				return NULL;
			}
		}

		countTimeInSencond++;
		sleep(1);
	}

	TraceLog(DEBUG_LEVEL, "ARCA DIRECT: Killed thread send Test Request due to disconnect from AS\n");

	return NULL;
}

/****************************************************************************
- Function name:	TalkTo_ARCA_DIRECT_Server
- Input:			N/A
- Output:		N/A
- Return:		N/A
- Description:	+ The routine hides the following facts
				  - Receive message from server and send response appropriately
				  - When the routine returns, meaning has some problems with 
				  socket
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
void TalkTo_ARCA_DIRECT_Server(void)
{
	t_DataBlock dataBlock;
	
	TraceLog(DEBUG_LEVEL, "Talking to ARCA DIRECT Server...\n");
	
	// Send Logon message to server
	if (BuildAndSend_ARCA_DIRECT_LogonRequestMsg() == ERROR)
	{
		TraceLog(ERROR_LEVEL, "Cannot send logon request message to ARCA DIRECT server\n");
		return;
	}
	
	while(orderStatusMgmt[ARCA_DIRECT_INDEX].shouldConnect == YES)
	{
		memset (&dataBlock, 0, sizeof(t_DataBlock));
		
		if(Receive_ARCA_DIRECT_Msg(&dataBlock) == ERROR)
		{
			return;
		}
		else
		{
			switch (dataBlock.msgContent[MSG_TYPE_INDEX])
			{
				case ORDER_ACK_TYPE:
					ARCA_DIRECT_Config.incomingSeqNum++;
					Add_ARCA_DIRECT_DataBlockToCollection(&dataBlock, GetClOrderId_ARCA_DIRECT(dataBlock.msgContent), -1);
					SaveARCA_DIRECT_SeqNumToFile();
					ProcessARCA_DIRECT_OrderAckMsg(&dataBlock, YES);
					break;
					
				case ORDER_FILLED_TYPE:
					ARCA_DIRECT_Config.incomingSeqNum++;
					Add_ARCA_DIRECT_DataBlockToCollection(&dataBlock, GetClOrderId_ARCA_DIRECT(dataBlock.msgContent), -1);
					SaveARCA_DIRECT_SeqNumToFile();
					ProcessARCA_DIRECT_OrderFilledMsg(&dataBlock);
					break;
					
				case CANCEL_ACK_TYPE:
					ARCA_DIRECT_Config.incomingSeqNum++;
					Add_ARCA_DIRECT_DataBlockToCollection(&dataBlock, GetClOrderId_ARCA_DIRECT(dataBlock.msgContent), -1);
					SaveARCA_DIRECT_SeqNumToFile();
					ProcessARCA_DIRECT_OrderCancelAckMsg(&dataBlock);
					break;
					
				case ORDER_REJECTED_TYPE:
					ARCA_DIRECT_Config.incomingSeqNum++;
					Add_ARCA_DIRECT_DataBlockToCollection(&dataBlock, GetClOrderId_ARCA_DIRECT(dataBlock.msgContent), -1);
					SaveARCA_DIRECT_SeqNumToFile();
					ProcessARCA_DIRECT_OrderRejectedMsg(&dataBlock);
					break;
				
				case ORDER_KILLED_TYPE:
					ARCA_DIRECT_Config.incomingSeqNum++;
					Add_ARCA_DIRECT_DataBlockToCollection(&dataBlock, GetClOrderId_ARCA_DIRECT(dataBlock.msgContent), -1);
					SaveARCA_DIRECT_SeqNumToFile();
					ProcessARCA_DIRECT_OrderKilledMsg(&dataBlock);
					break;			
				
				case BUST_OR_CORRECT_TYPE:
					ARCA_DIRECT_Config.incomingSeqNum++;
					Add_ARCA_DIRECT_DataBlockToCollection(&dataBlock, GetClOrderId_ARCA_DIRECT(dataBlock.msgContent), -1);
					SaveARCA_DIRECT_SeqNumToFile();
					ProcessARCA_DIRECT_BustOrCorrectMsg(&dataBlock);
					break;
			
				case TEST_REQUEST_TYPE:
					
					//TraceLog(DEBUG_LEVEL, "TEST_REQUEST_TYPE\n");
					
					// Send heartbeat to reply server test request
					if (BuildAndSend_ARCA_DIRECT_HeartbeatMsg() == ERROR)
					{
						return;
					}
					
					break;
					
				case HEARTBEAT_TYPE:
					
					//TraceLog(DEBUG_LEVEL, "HEARTBEAT_TYPE\n");
					
					// Send heartbeat to reply server heartbeat
					if (BuildAndSend_ARCA_DIRECT_HeartbeatMsg() == ERROR)
					{
						return;
					}

					break;
					
				case LOGON_TYPE:
					TraceLog(DEBUG_LEVEL, "Received logon accepted from ARCA DIRECT Server\n");
					
					ProcessARCA_DIRECT_LogonAcceptedMsg(&dataBlock);
					
					orderStatusMgmt[ARCA_DIRECT_INDEX].isOrderConnected = CONNECTED;
					asCollection.orderConnection[ARCA_DIRECT_INDEX].status = CONNECTED;
					
					TraceLog(DEBUG_LEVEL, "ARCA_DIRECT_Config.incomingSeqNum = %d\n", ARCA_DIRECT_Config.incomingSeqNum);
					TraceLog(DEBUG_LEVEL, "ARCA_DIRECT_Config.outgoingSeqNum = %d\n", ARCA_DIRECT_Config.outgoingSeqNum);
					
					pthread_t sendTestRequestThr;
					pthread_create(&sendTestRequestThr, NULL, (void*)&Send_Test_Request_Thread, NULL);
					
					break;
					
				case LOGON_REJECTED_TYPE:
					// We must stop here and try to connect to server again
					TraceLog(DEBUG_LEVEL, "Received logon rejected from ARCA DIRECT Server\n");
					
					TraceLog(DEBUG_LEVEL, "Reject Type %d\n", __bswap16(*(unsigned short *)&dataBlock.msgContent[16]));
					TraceLog(DEBUG_LEVEL, "Reject Text = %s\n", &dataBlock.msgContent[18]);
					
					ProcessARCA_DIRECT_LogonRejectedMsg(&dataBlock);
					
					return;
					
				default:
					TraceLog(WARN_LEVEL, "ARCA DIRECT - Unknown message type %d\n", dataBlock.msgContent[MSG_TYPE_INDEX]);
					break;
			}
		}
	}
	
	TraceLog(DEBUG_LEVEL, "Connection to ARCA DIRECT Server will be closed now...\n");
}

/****************************************************************************
- Function name:	BuildLogonRequestMessage
- Input:			N/A
- Output:		N/A
- Return:		Success or Failure
- Description:	+ The routine hides the following facts
				  - Build logon message with using some global variables
				  - Calculate message length and return through return value
				+ There are preconditions guaranteed to the routine
				  - 	message must be allocated before
				+ The routine guarantees that the return value will 
						have a value of either
				  - Success
					or 
				  - Failure
- Usage:			N/A
****************************************************************************/
int BuildAndSend_ARCA_DIRECT_LogonRequestMsg(void)
{
	/*
	- API version: ArcaDirect 4.1g
	- Date updated: 03-Jan-2013
	- Modified by: Long NKH
	- Logon Messeage (and Logon Accepted), it includes the following fields:
		+ Message Type (1 byte) = 'A'
		+ Variant (1 byte) = 1
		+ Length (2 bytes) = 48
		+ SeqNum (4 bytes) = null (should be ignored)
		+ Last Sequence Number (4 bytes) = Last sequence number processed
		+ UserName (5 bytes) = ArcaDirect Login ID
		+ Symbology (1 byte) = null (use for Options orders only.)
		+ Message Version Profile (28 bytes) = 'L',1,'D',1,'G',1,'E',1,'a',1,'2',1,'6',1,'4',1 (16 bytes)
		+ Cancel On Disconnect (1 byte binary) 0: Do not cancel when disconnect, 1: Auto Cancel when disconnect
		+ Message Terminator (1 byte) = '\n'
		=> Total byte = 48
	*/
	
	char message[LOGON_TYPE_MSG_LEN + 1] = "\0";
	
	t_IntConverter lastSeqNum;
	t_ShortConverter tmpLength;
	
	// Message type is 'A'
	message[0] = LOGON_TYPE;

	// Variant
	message[1] = 1;

	// Length
	tmpLength.value = LOGON_TYPE_MSG_LEN;
	message[2] = tmpLength.c[1];
	message[3] = tmpLength.c[0];
	
	// SeqNum (4 bytes)
	
	/*
	Last sequence number
	Message last sequence number should start at 0
	*/
	lastSeqNum.value = ARCA_DIRECT_Config.incomingSeqNum;
	message[8] = lastSeqNum.c[3];
	message[9] = lastSeqNum.c[2];
	message[10] = lastSeqNum.c[1];
	message[11] = lastSeqNum.c[0];

	// Company Group ID (User name)
	memcpy(&message[12], ARCA_DIRECT_Config.userName, 5);

	// Symbology (1 byte)
	
	// Message Version Profile
	strncpy(&message[18], VERSION_PROFILE, 28);
	
	// Cancel On Disconnect (1 bytes binary). Should be 0 or 1
	message[46] = ARCA_DIRECT_Config.autoCancelOnDisconnect;

	// Terminator
	message[47] = '\n';
	
	// Send logon message
	return Send_ARCA_DIRECT_Msg(message, LOGON_TYPE_MSG_LEN);
}

/****************************************************************************
- Function name:	BuildAndSend_ARCA_DIRECT_HeartbeatMsg
- Input:			N/A
- Output:		+ message
- Return:		message length
- Description:	+ The routine hides the following facts
				  - Build heartbeat message with using some global variables
				  - Calculate message length and return through return value
				+ There are preconditions guaranteed to the routine
				  - 	message must be allocated before
				+ The routine guarantees that the return value will 
						have a value of
				  - message length
- Usage:			N/A
****************************************************************************/
int BuildAndSend_ARCA_DIRECT_HeartbeatMsg(void)
{
	/*
	- API version: ArcaDirect 4.1g
	- Date updated: 03-Jan-2013
	- Modified by: Long NKH
	- Heartbeat Message, it includes the following fields:
		+ Message Type (1 byte) = '0'
		+ Variant (1 byte) = 1
		+ Length (2 bytes) = 12
		+ SeqNum (4 bytes) = null (should be ignored)
		+ Filler (3 byte) = null
		+ Message Terminator (1 byte) = '\n'
		=> Total byte = 12
	*/
	
	char message[HEARTBEAT_MSG_LEN] = "\0";
	t_ShortConverter tmpLength;
		
	// Message type is '0'
	message[0] = HEARTBEAT_TYPE;

	// Variant (1 byte) = 1
	message[1] = 1;

	// Length
	tmpLength.value = HEARTBEAT_MSG_LEN;
	message[2] = tmpLength.c[1];
	message[3] = tmpLength.c[0];

	// SeqNum (4 bytes)
	
	// Filler (3 byte)

	// Terminator
	message[11] = '\n';
	
	// Send heartbeat
	return Send_ARCA_DIRECT_Msg(message, HEARTBEAT_MSG_LEN);
}

/****************************************************************************
- Function name:	BuildAndSend_ARCA_DIRECT_TestRequestMsg
- Input:			N/A
- Output:		N/A
- Return:		message length
- Description:	+ The routine hides the following facts
				  - Build heartbeat message with using some global variables
				  - Calculate message length and return through return value
				+ There are preconditions guaranteed to the routine
				  - 	message must be allocated before
				+ The routine guarantees that the return value will 
						have a value of
				  - message length
- Usage:			N/A
****************************************************************************/
int BuildAndSend_ARCA_DIRECT_TestRequestMsg(void)
{	
	/*
	- API version: ArcaDirect 4.1g
	- Date updated: 03-Jan-2013
	- Modified by: Long NKH
	- Test Request Message, it includes the following fields:
		+ Message Type (1 byte) = '1'
		+ Variant (1 byte) = 1
		+ Length (2 bytes) = 12
		+ SeqNum (4 bytes) = null (should be ignored)
		+ Filler (3 byte) = null
		+ Message Terminator (1 byte) = '\n'
		=> Total byte = 12
	*/
	
	char message[TEST_REQUEST_MSG_LEN + 1] = "\0";
	t_ShortConverter tmpLength;

	// Message type is '1'
	message[0] = TEST_REQUEST_TYPE;

	// Variant (1 byte) = 1
	message[1] = 1;

	// Length
	tmpLength.value = TEST_REQUEST_MSG_LEN;
	message[2] = tmpLength.c[1];
	message[3] = tmpLength.c[0];

	// SeqNum (4 bytes)
	
	// Filler (3 byte)

	// Terminator
	message[11] = '\n';
	
	//Send test request
	return Send_ARCA_DIRECT_Msg(message, TEST_REQUEST_MSG_LEN);
}

/****************************************************************************
- Function name:	BuildAndSend_ARCA_DIRECT_OrderMsg_V1
- Input:			+ newOrder
- Output:		N/A
- Return:		Success or Failure
- Description:	
				
- Usage:	N/A
****************************************************************************/
int BuildAndSend_ARCA_DIRECT_OrderMsg_V1(t_ARCA_DIRECT_NewOrder *newOrder, t_SendNewOrder *originalOrder)
{
	/*
	- API version: ArcaDirect 4.1g
	- Date updated: 03-Jan-2013
	- Modified by: Long NKH
	- New Order Message � (Variant 1: formerly V2 API � small message), it includes the following fields:
		+ Message Type (1 byte) = 'D'
		+ Variant (1 byte) = 1
		+ Length (2 bytes) = 76
		+ Sequence Number (4 bytes) = Client-assigned sequence number
		+ Client Order Id (4 bytes) = A client-assigned ID for this order
		+ PCS Link ID (4 bytes) = null
		+ Order Quantity (4 bytes) = The number of shares
		+ Price (4 bytes) = The price as a long value
		+ ExDestination (2 bytes) = 102
		+ Price Scale (1 byte) = '2'
		+ Symbol (8 bytes) = Stock symbol
		+ Company Group ID (5 bytes) = HBIWB
		+ Deliver To Comp ID (5 bytes) = null
		+ SenderSubID (5 bytes) = null
		+ Execution Instructions (1 byte) = '1'
		+ Side (1 byte) = '1', '2', or '5'
		+ Order Type (1 byte) = '2'
		+ Time In Force (1 byte) = '0' or '3'
		+ Rule80A (1 byte) = 'P' --> change A to P (related to ticket 1112)
		+ Trading Session Id (4 bytes) = "123"
		+ Account (10 bytes) = null
		+ ISO (1 byte) = 'N'
		+ Extended Execution Instructions (1 byte) = null
		+ ExtendedPNP (1 byte) = null
		+ NoSelfTrade (1 byte) = 'O'
		+ ProactiveIfLocked (1 byte) = null
		+ Filler (1 byte) = null
		+ Message Terminator (1 byte) = '\n'
		=> Total byte = 76
	*/
	
	t_IntConverter tmpValue;
	t_ShortConverter tmpLength;

	char message[NEW_ORDER_MSG_LEN_V1 + 1] = "\0";

	// Message type is 'D'
	message[0] = NEW_ORDER_TYPE;

	// Variant (1 byte) = 1
	message[1] = 1;

	// Length
	tmpLength.value = NEW_ORDER_MSG_LEN_V1;
	message[2]  = tmpLength.c[1];
	message[3]  = tmpLength.c[0];

	// Sequence number
	tmpValue.value =  Get_ARCA_DIRECT_OutgoingSeqNum();
	message[4] = tmpValue.c[3];
	message[5] = tmpValue.c[2];
	message[6] = tmpValue.c[1];
	message[7] = tmpValue.c[0];

	// Client order Id
	tmpValue.value =  newOrder->clOrdID;
	message[8] = tmpValue.c[3];
	message[9] = tmpValue.c[2];
	message[10] = tmpValue.c[1];
	message[11] = tmpValue.c[0];
	
	// PCS Link ID
	message[12] = 0;
	message[13] = 0;
	message[14] = 0;
	message[15] = 0;

	// Order Quantity
	tmpValue.value =  newOrder->orderQty;
	message[16] = tmpValue.c[3];
	message[17] = tmpValue.c[2];
	message[18] = tmpValue.c[1];
	message[19] = tmpValue.c[0];

	// Price
	tmpValue.value = (int)(newOrder->price * 100.00 + 0.500001);
	message[20] = tmpValue.c[3];
	message[21] = tmpValue.c[2];
	message[22] = tmpValue.c[1];
	message[23] = tmpValue.c[0];
	
	// ExDestination (2 bytes) = 102
	tmpLength.value = EXEXCHANGE_DESTINATION;
	message[24]  = tmpLength.c[1];
	message[25]  = tmpLength.c[0];

	// Price scale
	message[26] = PRICE_SCALE;

	// Symbol
	strncpy(&message[27], newOrder->symbol, SYMBOL_LEN);
	
	// Company group ID
	// memcpy(&message[35], MPID_ARCA_DIRECT, 5);
	memcpy(&message[35], ARCA_DIRECT_Config.compGroupID, 5);
	
	// Deliver Comp ID
	message[40] = 0;
	message[41] = 0;
	message[42] = 0;
	message[43] = 0;
	message[44] = 0;
	
	// SenderSubID (5 bytes) = null
	message[45] = 0;
	message[46] = 0;
	message[47] = 0;
	message[48] = 0;
	message[49] = 0;

	// Execution Instructions
	message[50] = '1';

	// Side
	message[51] = newOrder->side;

	// Order Type
	message[52] = ORDER_TYPE_LIMIT;

	// Time In Force
	message[53] = newOrder->timeInForce;	//IOC

	// Rule80A: A, P, E
	message[54] = RULE80A;
	
	// Trading session Id: "1", "2", "3"
	memcpy(&message[55], TRADING_SESSION_ID, 4);
	
	// Account
	strncpy(&message[59], TradingAccount.ArcaDirectAccount[originalOrder->accountSuffix], 10);
	
	// ISO Flag
	message[69] = 'N';
	
	// Extended Execution instruction
	message[70] = 0;
	
	// ExtendedPNP (1 byte) = null
	message[71] = 0;
	
	// NoSelfTrade (1 byte) = 'O'
	message[72] = 'O';
	
	// ProactiveIfLocked (1 byte) = null
	message[73] = 0;
	
	// Filler (1 byte)
	message[74] = 0;
	
	//Terminator
	message[75] = '\n';
	
	if (Send_ARCA_DIRECT_Msg(message, NEW_ORDER_MSG_LEN_V1) == SUCCESS)
	{
		ProcessAddNewOrder(originalOrder);

		t_DataBlock dataBlock;
		memset(&dataBlock, 0, sizeof(t_DataBlock));

		dataBlock.msgLen = NEW_ORDER_MSG_LEN_V1;
		memcpy(dataBlock.msgContent, message, NEW_ORDER_MSG_LEN_V1);

		// Add Buy Launch
		dataBlock.addContent[BUY_LAUNCH_INDEX] = newOrder->buyLaunch;

		//Add Cross ID
		memcpy(&dataBlock.addContent[CROSS_ID_INDEX], &(CrossIDCollection[originalOrder->symbolIndex]), 4);

		Add_ARCA_DIRECT_DataBlockToCollection(&dataBlock, newOrder->clOrdID, originalOrder->accountSuffix);

		// Save sequence number to file
		SaveARCA_DIRECT_SeqNumToFile();

		return SUCCESS;
	}
	else
	{
		return ERROR;
	}
}

int BuildAndSend_ARCA_DIRECT_OrderMsg_V3(t_ARCA_DIRECT_NewOrder *newOrder, t_SendNewOrder *originalOrder)
{
	t_IntConverter tmpValue;
	t_ShortConverter tmpLength;

	char message[NEW_ORDER_MSG_LEN_V3 + 1] = "\0";

	// Message type is 'D'
	message[0] = NEW_ORDER_TYPE;

	// Variant (1 byte) = 1
	message[1] = 3;

	// Length
	tmpLength.value = NEW_ORDER_MSG_LEN_V3;
	message[2]  = tmpLength.c[1];
	message[3]  = tmpLength.c[0];

	// Sequence number
	tmpValue.value =  Get_ARCA_DIRECT_OutgoingSeqNum();
	message[4] = tmpValue.c[3];
	message[5] = tmpValue.c[2];
	message[6] = tmpValue.c[1];
	message[7] = tmpValue.c[0];

	// Client order Id
	tmpValue.value =  newOrder->clOrdID;
	message[8] = tmpValue.c[3];
	message[9] = tmpValue.c[2];
	message[10] = tmpValue.c[1];
	message[11] = tmpValue.c[0];
	
	// PCS Link ID
	message[12] = 0;
	message[13] = 0;
	message[14] = 0;
	message[15] = 0;

	// Order Quantity
	tmpValue.value =  newOrder->orderQty;
	message[16] = tmpValue.c[3];
	message[17] = tmpValue.c[2];
	message[18] = tmpValue.c[1];
	message[19] = tmpValue.c[0];
	
	// Strike Price (4 BYTES) 20 - 23
	// MinQty 24-27
	
	// Max Floor 28 - 31
	tmpValue.value = newOrder->maxFloor;
	message[28] = tmpValue.c[3];
	message[29] = tmpValue.c[2];
	message[30] = tmpValue.c[1];
	message[31] = tmpValue.c[0];
	
	//Display Range 32 35
	// DiscretionOffset 36 39 
	// Peg Difference 40 43
	// StopPx 44 47

	// Price
	tmpValue.value = (int)(newOrder->price * 100.00 + 0.500001);
	message[48] = tmpValue.c[3];
	message[49] = tmpValue.c[2];
	message[50] = tmpValue.c[1];
	message[51] = tmpValue.c[0];
	
	// ExDestination (2 bytes) = 102
	tmpLength.value = EXEXCHANGE_DESTINATION;
	message[52]  = tmpLength.c[1];
	message[53]  = tmpLength.c[0];

	// Price scale
	//message[26] = PRICE_SCALE;
	
	//Underlying Quantity 54 - 55
	// Put Cal 56
	// LocalOrAway 57
	
	//Price Scale
	message[58] = PRICE_SCALE;
	
	// Corporate Action 59
	// Open Close 60
	
	// Symbol
	strncpy(&message[61], newOrder->symbol, SYMBOL_LEN);
	
	// Strike Date 69-76
	
	// Company group ID
	memcpy(&message[77], ARCA_DIRECT_Config.compGroupID, 5);
	
	// Deliver Comp ID 82-86
	
	
	// SenderSubID 87-91 
	
	// Execution Instructions 92
	message[92] = '1';
	
	// Side
	message[93] = newOrder->side;

	// Order Type
	message[94] = ORDER_TYPE_LIMIT;

	// Time In Force
	message[95] = newOrder->timeInForce;	

	// Rule80A: A, P, E
	message[96] = RULE80A;
	
	// Customer Or Firm
	message[97] = '0';
	
	// Trading session Id: "1", "2", "3"
	memcpy(&message[98], TRADING_SESSION_ID, 4);
	
	// Account
	strncpy(&message[102], TradingAccount.ArcaDirectAccount[originalOrder->accountSuffix], 10);
	
	// Optional Data 112-127
	
	// Clearing Firm 128-132
	
	// Clearing Account 133-137
	
	// Expire Time Flag 138
	
	// ExpireTime 139-142
	
	// Effective Time 143 146
	
	// DiscretionInst 147
	// Proactive Discretion 148
	// ProactiveIfLocked 149
	
	// ExecBroker 150-154
	
	// ISO Flag
	message[155] = 'N';
	
	// Symbol Type 156
	
	// Extended Execution instruction
	message[157] = 0;
	
	// ExtendedPNP (1 byte) = null
	message[158] = 0;
	
	// NoSelfTrade (1 byte) = 'O'
	message[159] = 'O';
	
	// Filler (1 byte)
	message[160] = 0;
	
	//Terminator
	message[163] = '\n';
	
	if (Send_ARCA_DIRECT_Msg(message, NEW_ORDER_MSG_LEN_V3) == SUCCESS)
	{
		ProcessAddNewOrder(originalOrder);

		t_DataBlock dataBlock;
		memset(&dataBlock, 0, sizeof(t_DataBlock));

		dataBlock.msgLen = NEW_ORDER_MSG_LEN_V3;
		memcpy(dataBlock.msgContent, message, NEW_ORDER_MSG_LEN_V3);

		// Add Buy Launch
		dataBlock.addContent[BUY_LAUNCH_INDEX] = newOrder->buyLaunch;

		//Add Cross ID
		memcpy(&dataBlock.addContent[CROSS_ID_INDEX], &(CrossIDCollection[originalOrder->symbolIndex]), 4);

		Add_ARCA_DIRECT_DataBlockToCollection(&dataBlock, newOrder->clOrdID, originalOrder->accountSuffix);

		// Save sequence number to file
		SaveARCA_DIRECT_SeqNumToFile();

		return SUCCESS;
	}
	else
	{
		return ERROR;
	}
}

int BuildAndSend_ARCA_DIRECT_ManualCancelMsg(t_ManualCancelOrder *cancelOrder)
{
	char message[CANCEL_ORDER_MSG_LEN] = "\0";
	t_IntConverter tmpValue;
	t_LongConverter tmpLongValue;
	t_ShortConverter tmpLength;

	// Message type is 'F'
	message[0] = CANCEL_ORDER_TYPE;

	// Variant (1 byte) = 1
	message[1] = 1;

	// Length
	tmpLength.value = CANCEL_ORDER_MSG_LEN;
	memcpy(&message[2], tmpLength.c, 2);
	message[2] = tmpLength.c[1];
	message[3] = tmpLength.c[0];

	// Sequence number
	tmpValue.value =  Get_ARCA_DIRECT_OutgoingSeqNum();
	message[4] = tmpValue.c[3];
	message[5] = tmpValue.c[2];
	message[6] = tmpValue.c[1];
	message[7] = tmpValue.c[0];
	
  if(cancelOrder->ecnOrderID == 0) // Daedalus does not send ecnOrderID to Blinbox
  {
    cancelOrder->ecnOrderID = AutoCancelOrder.orderInfo[cancelOrder->origClOrdID % MAX_MANUAL_ORDER].ecnOrderID;
  }
	// ARCA Order Id
	tmpLongValue.value = cancelOrder->ecnOrderID;
	message[8] = tmpLongValue.c[7];
	message[9] = tmpLongValue.c[6];
	message[10] = tmpLongValue.c[5];
	message[11] = tmpLongValue.c[4];
	message[12] = tmpLongValue.c[3];
	message[13] = tmpLongValue.c[2];
	message[14] = tmpLongValue.c[1];
	message[15] = tmpLongValue.c[0];

	// Client order Id
	tmpValue.value = cancelOrder->origClOrdID;
	message[16] = tmpValue.c[3];
	message[17] = tmpValue.c[2];
	message[18] = tmpValue.c[1];
	message[19] = tmpValue.c[0];
	
	// Strike Price (4 bytes) = null
	
	// Under Qty (2 bytes) = null
	
	// ExDestination (2 bytes) = 102
	tmpLength.value = EXEXCHANGE_DESTINATION;
	message[26] = tmpLength.c[1];
	message[27] = tmpLength.c[0];
	
	// Corporate Action (1 byte) = null
	
	// Pull or Call (1 byte) = null
	
	// Bulk Cancel (1 byte) = null
	
	// Open or Close (1 byte) = null
	
	// Symbol
	strncpy(&message[32], cancelOrder->symbol, SYMBOL_LEN);
	
	// Strike Date (8 bytes) = null

	// Side
	message[48] = cancelOrder->side;
	
	// Deliver To Comp ID (5 bytes) = null
	
	// Account (10 bytes)
	cancelOrder->accountSuffix = accountMapping[cancelOrder->origClOrdID % MAX_ORDER_PLACEMENT];
	strncpy(&message[54], TradingAccount.ArcaDirectAccount[cancelOrder->accountSuffix], 10);
	
	// Padding (7 bytes)

	// Terminator
	message[71] = '\n';
	
	if (Send_ARCA_DIRECT_Msg(message, CANCEL_ORDER_MSG_LEN) == SUCCESS)
	{
		t_DataBlock dataBlock;
		dataBlock.msgLen = CANCEL_ORDER_MSG_LEN;
		
		// Set timestamp for current rawdata
		GetTimeFormatTime(dataBlock.addContent);
		
		memcpy(dataBlock.msgContent, message, CANCEL_ORDER_MSG_LEN);
		
		Add_ARCA_DIRECT_DataBlockToCollection(&dataBlock, cancelOrder->origClOrdID, cancelOrder->accountSuffix);

		return SUCCESS;
	}
	else
	{
		return ERROR;
	}
}

int BuildAndSend_ARCA_DIRECT_ManualOrder_V1(t_ManualOrder *tmpOrder)
{
	t_ARCA_DIRECT_NewOrder newOrder;
	newOrder.clOrdID = tmpOrder->orderId;
	
	newOrder.buyLaunch = 0;
	if (tmpOrder->side == BUY_TO_CLOSE_TYPE)
	{
		newOrder.side = '1';
		newOrder.buyLaunch = BUY_TO_CLOSE;
	}
	else if (tmpOrder->side == SELL_TYPE)
	{
		newOrder.side = '2';
	}
	else if (tmpOrder->side == SHORT_SELL_TYPE)
	{
		newOrder.side = '5';
	}
	else 
	{
		newOrder.side = '1';
		newOrder.buyLaunch = BUY_TO_OPEN;
	}
	
	// Share Volume
	newOrder.orderQty = tmpOrder->shares;
	
	// Stock Symbol
	newOrder.symbol = tmpOrder->symbol;
	
	// Price
	newOrder.price = tmpOrder->price;
	
	if (tmpOrder->tif == 0 || tmpOrder->tif == 1)
	{
		newOrder.timeInForce = '3';
	}
	else // IOC
	{
		newOrder.timeInForce = '0';
	}
	
	t_IntConverter tmpValue;
	t_ShortConverter tmpLength;

	char message[NEW_ORDER_MSG_LEN_V1 + 1] = "\0";

	// Message type is 'D'
	message[0] = NEW_ORDER_TYPE;

	// Variant (1 byte) = 1
	message[1] = 1;

	// Length
	tmpLength.value = NEW_ORDER_MSG_LEN_V1;
	message[2]  = tmpLength.c[1];
	message[3]  = tmpLength.c[0];

	// Sequence number
	tmpValue.value =  Get_ARCA_DIRECT_OutgoingSeqNum();
	message[4] = tmpValue.c[3];
	message[5] = tmpValue.c[2];
	message[6] = tmpValue.c[1];
	message[7] = tmpValue.c[0];

	// Client order Id
	tmpValue.value =  newOrder.clOrdID;
	message[8] = tmpValue.c[3];
	message[9] = tmpValue.c[2];
	message[10] = tmpValue.c[1];
	message[11] = tmpValue.c[0];
	
	// PCS Link ID
	message[12] = 0;
	message[13] = 0;
	message[14] = 0;
	message[15] = 0;

	// Order Quantity
	tmpValue.value =  newOrder.orderQty;
	message[16] = tmpValue.c[3];
	message[17] = tmpValue.c[2];
	message[18] = tmpValue.c[1];
	message[19] = tmpValue.c[0];

	// Price
	tmpValue.value = (int)(newOrder.price * 100.00 + 0.500001);
	message[20] = tmpValue.c[3];
	message[21] = tmpValue.c[2];
	message[22] = tmpValue.c[1];
	message[23] = tmpValue.c[0];
	
	// ExDestination (2 bytes) = 102
	tmpLength.value = EXEXCHANGE_DESTINATION;
	message[24]  = tmpLength.c[1];
	message[25]  = tmpLength.c[0];

	// Price scale
	message[26] = PRICE_SCALE;

	// Symbol
	strncpy(&message[27], newOrder.symbol, SYMBOL_LEN);
	
	// Company group ID
	// memcpy(&message[35], MPID_ARCA_DIRECT, 5);
	memcpy(&message[35], ARCA_DIRECT_Config.compGroupID, 5);
	
	// Deliver Comp ID
	message[40] = 0;
	message[41] = 0;
	message[42] = 0;
	message[43] = 0;
	message[44] = 0;
	
	// SenderSubID (5 bytes) = null
	message[45] = 0;
	message[46] = 0;
	message[47] = 0;
	message[48] = 0;
	message[49] = 0;

	// Execution Instructions
	message[50] = '1';

	// Side
	message[51] = newOrder.side;

	// Order Type
	message[52] = ORDER_TYPE_LIMIT;

	// Time In Force
	message[53] = newOrder.timeInForce;

	// Rule80A: A, P, E
	message[54] = RULE80A;
	
	// Trading session Id: "1", "2", "3"
	memcpy(&message[55], TRADING_SESSION_ID, 4);
	
	// Account
	strncpy(&message[59], TradingAccount.ArcaDirectAccount[tmpOrder->accountSuffix], 10);
	
	// ISO Flag
	message[69] = 'N';
	
	// Extended Execution instruction
	message[70] = 0;
	
	// ExtendedPNP (1 byte) = null
	message[71] = 0;
	
	// NoSelfTrade (1 byte) = 'O'
	message[72] = 'O';
	
	// ProactiveIfLocked (1 byte) = null
	message[73] = 0;
	
	// Filler (1 byte)
	message[74] = 0;
	
	//Terminator
	message[75] = '\n';
	
	if (Send_ARCA_DIRECT_Msg(message, NEW_ORDER_MSG_LEN_V1) == SUCCESS)
	{
		accountMapping[newOrder.clOrdID % MAX_ORDER_PLACEMENT] = tmpOrder->accountSuffix;
		
		t_DataBlock dataBlock;
		memset(&dataBlock, 0, sizeof(t_DataBlock));

		dataBlock.msgLen = NEW_ORDER_MSG_LEN_V1;

		memcpy(dataBlock.msgContent, message, NEW_ORDER_MSG_LEN_V1);

		// Add Buy Launch
		dataBlock.addContent[BUY_LAUNCH_INDEX] = newOrder.buyLaunch;

		Add_ARCA_DIRECT_DataBlockToCollection(&dataBlock, newOrder.clOrdID, tmpOrder->accountSuffix);

		// Save sequence number to file
		SaveARCA_DIRECT_SeqNumToFile();

		return SUCCESS;
	}
	else
	{
		return ERROR;
	}
}

int BuildAndSend_ARCA_DIRECT_ManualOrder_V3(t_ManualOrder *tmpOrder)
{
	t_ARCA_DIRECT_NewOrder newOrder;
	newOrder.clOrdID = tmpOrder->orderId;
	
	newOrder.buyLaunch = 0;
	if (tmpOrder->side == BUY_TO_CLOSE_TYPE)
	{
		newOrder.side = '1';
		newOrder.buyLaunch = BUY_TO_CLOSE;
	}
	else if (tmpOrder->side == SELL_TYPE)
	{
		newOrder.side = '2';
	}
	else if (tmpOrder->side == SHORT_SELL_TYPE)
	{
		newOrder.side = '5';
	}
	else 
	{
		newOrder.side = '1';
		newOrder.buyLaunch = BUY_TO_OPEN;
	}
	
	// Share Volume
	newOrder.orderQty = tmpOrder->shares;
	
	newOrder.maxFloor = tmpOrder->maxFloor;
	
	// Stock Symbol
	newOrder.symbol = tmpOrder->symbol;
	
	// Price
	newOrder.price = tmpOrder->price;
	
	if (tmpOrder->tif == 0 || tmpOrder->tif == 1)
	{
		newOrder.timeInForce = '3';
	}
	else // IOC
	{
		newOrder.timeInForce = '0';
	}
	
	t_IntConverter tmpValue;
	t_ShortConverter tmpLength;

	char message[NEW_ORDER_MSG_LEN_V3 + 1] = "\0";

	// Message type is 'D'
	message[0] = NEW_ORDER_TYPE;

	// Variant (1 byte) = 1
	message[1] = 3;

	// Length
	tmpLength.value = NEW_ORDER_MSG_LEN_V3;
	message[2]  = tmpLength.c[1];
	message[3]  = tmpLength.c[0];

	// Sequence number
	tmpValue.value =  Get_ARCA_DIRECT_OutgoingSeqNum();
	message[4] = tmpValue.c[3];
	message[5] = tmpValue.c[2];
	message[6] = tmpValue.c[1];
	message[7] = tmpValue.c[0];

	// Client order Id
	tmpValue.value =  newOrder.clOrdID;
	message[8] = tmpValue.c[3];
	message[9] = tmpValue.c[2];
	message[10] = tmpValue.c[1];
	message[11] = tmpValue.c[0];
	
	// PCS Link ID
	message[12] = 0;
	message[13] = 0;
	message[14] = 0;
	message[15] = 0;

	// Order Quantity
	tmpValue.value =  newOrder.orderQty;
	message[16] = tmpValue.c[3];
	message[17] = tmpValue.c[2];
	message[18] = tmpValue.c[1];
	message[19] = tmpValue.c[0];
	
	// Strike Price 20-23
	// MinQty 24-27
	
	// Max Floor 28 - 31
	tmpValue.value = newOrder.maxFloor;
	message[28] = tmpValue.c[3];
	message[29] = tmpValue.c[2];
	message[30] = tmpValue.c[1];
	message[31] = tmpValue.c[0];
	
	//Display Range 32 35
	// DiscretionOffset 36 39 
	// Peg Difference 40 43
	// StopPx 44 47

	// Price
	tmpValue.value = (int)(newOrder.price * 100.00 + 0.500001);
	message[48] = tmpValue.c[3];
	message[49] = tmpValue.c[2];
	message[50] = tmpValue.c[1];
	message[51] = tmpValue.c[0];
	
	// ExDestination (2 bytes) = 102
	tmpLength.value = EXEXCHANGE_DESTINATION;
	message[52]  = tmpLength.c[1];
	message[53]  = tmpLength.c[0];
	
	//Underlying Quantity 54 - 55
	// Put Cal 56
	// LocalOrAway 57
	
	//Price Scale
	message[58] = PRICE_SCALE;
	
	// Corporate Action 59
	// Open Close 60
	
	// Symbol
	strncpy(&message[61], newOrder.symbol, SYMBOL_LEN);
	
	// Strike Date 69-76
	
	// Company group ID
	memcpy(&message[77], ARCA_DIRECT_Config.compGroupID, 5);
	
	// Deliver Comp ID 82-86
	
	
	// SenderSubID 87-91 
	
	// Execution Instructions 92
	message[92] = '1';
	
	// Side
	message[93] = newOrder.side;

	// Order Type
	message[94] = ORDER_TYPE_LIMIT;

	// Time In Force
	message[95] = newOrder.timeInForce;	

	// Rule80A: A, P, E
	message[96] = RULE80A;
	
	// Customer Or Firm
	message[97] = '0';
	
	// Trading session Id: "1", "2", "3"
	memcpy(&message[98], TRADING_SESSION_ID, 4);
	
	// Account
	strncpy(&message[102], TradingAccount.ArcaDirectAccount[tmpOrder->accountSuffix], 10);
	
	// Optional Data 112-127
	
	// Clearing Firm 128-132
	
	// Clearing Account 133-137
	
	// Expire Time Flag 138
	
	// ExpireTime 139-142
	
	// Effective Time 143 146
	
	// DiscretionInst 147
	// Proactive Discretion 148
	// ProactiveIfLocked 149
	
	// ExecBroker 150-154
	
	// ISO Flag
	message[155] = 'N';
	
	// Symbol Type 156
	
	// Extended Execution instruction
	message[157] = 0;
	
	// ExtendedPNP (1 byte) = null
	message[158] = 0;
	
	// NoSelfTrade (1 byte) = 'O'
	message[159] = 'O';
	
	// Filler (1 byte)
	message[160] = 0;
	
	//Terminator
	message[163] = '\n';
	
	if (Send_ARCA_DIRECT_Msg(message, NEW_ORDER_MSG_LEN_V3) == SUCCESS)
	{
		accountMapping[newOrder.clOrdID % MAX_ORDER_PLACEMENT] = tmpOrder->accountSuffix;
		
		t_DataBlock dataBlock;
		memset(&dataBlock, 0, sizeof(t_DataBlock));

		dataBlock.msgLen = NEW_ORDER_MSG_LEN_V3;

		memcpy(dataBlock.msgContent, message, NEW_ORDER_MSG_LEN_V3);

		// Add Buy Launch
		dataBlock.addContent[BUY_LAUNCH_INDEX] = newOrder.buyLaunch;

		Add_ARCA_DIRECT_DataBlockToCollection(&dataBlock, newOrder.clOrdID, tmpOrder->accountSuffix);

		// Save sequence number to file
		SaveARCA_DIRECT_SeqNumToFile();

		return SUCCESS;
	}
	else
	{
		return ERROR;
	}
}

int SaveARCA_DIRECT_SeqNumToFile(void)
{
	return SaveSeqNumToFile("arca_direct_seqs", ARCA_DIRECT_INDEX, &ARCA_DIRECT_Config);
}

int BuildAndSend_ARCA_DIRECT_CancelMsg(const t_ARCA_DIRECT_CancelOrder *cancelOrder)
{
	/*
	- API version: ArcaDirect 4.0
	- Date updated: 07-Jul-2009
	- Modified by: Luan Vo-Kinh
	- Order Cancel Message Equities (Variant 1), it includes the following fields:
		+ Message Type (1 byte) = 'F'
		+ Variant (1 byte) = 1
		+ Length (2 bytes) = 72
		+ Sequence Number (4 bytes) = Client-assigned sequence number
		+ OrderID (8 bytes) = NYSE Arca assigned OrderID. 
		+ Client Order Id (4 bytes) = A client-assigned ID for this order
		+ Strike Price (4 bytes) = null
		+ Under Qty (2 bytes) = null
		+ ExDestination (2 bytes) = 102
		+ Corporate Action (1 byte) = null
		+ Pull or Call (1 byte) = null
		+ Bulk Cancel (1 byte) = null
		+ Open or Close (1 byte) = null
		+ Symbol (8 bytes) = Stock symbol
		+ Strike Date (8 bytes) = null
		+ Side (1 byte) = '1', '2', or '5'
		+ Deliver To Comp ID (5 bytes) = null
		+ Account (10 bytes) = null
		+ Filler (7 byte) = null
		+ Message Terminator (1 byte) = '\n'
		=> Total byte = 72
	*/
	
	char message[CANCEL_ORDER_MSG_LEN] = "\0";
	t_IntConverter tmpValue;
	t_LongConverter tmpLongValue;
	t_ShortConverter tmpLength;

	// Message type is 'F'
	message[0] = CANCEL_ORDER_TYPE;

	// Variant (1 byte) = 1
	message[1] = 1;

	// Length
	tmpLength.value = CANCEL_ORDER_MSG_LEN;
	memcpy(&message[2], tmpLength.c, 2);
	message[2] = tmpLength.c[1];
	message[3] = tmpLength.c[0];

	// Sequence number
	tmpValue.value =  Get_ARCA_DIRECT_OutgoingSeqNum();
	message[4] = tmpValue.c[3];
	message[5] = tmpValue.c[2];
	message[6] = tmpValue.c[1];
	message[7] = tmpValue.c[0];
	
	// ARCA Order Id
	tmpLongValue.value =  cancelOrder->ecnOrderID;
	message[8] = tmpLongValue.c[7];
	message[9] = tmpLongValue.c[6];
	message[10] = tmpLongValue.c[5];
	message[11] = tmpLongValue.c[4];
	message[12] = tmpLongValue.c[3];
	message[13] = tmpLongValue.c[2];
	message[14] = tmpLongValue.c[1];
	message[15] = tmpLongValue.c[0];

	// Client order Id
	tmpValue.value = cancelOrder->origClOrdID;
	message[16] = tmpValue.c[3];
	message[17] = tmpValue.c[2];
	message[18] = tmpValue.c[1];
	message[19] = tmpValue.c[0];
	
	// Strike Price (4 bytes) = null
	
	// Under Qty (2 bytes) = null
	
	// ExDestination (2 bytes) = 102
	tmpLength.value = EXEXCHANGE_DESTINATION;
	message[26] = tmpLength.c[1];
	message[27] = tmpLength.c[0];
	
	// Corporate Action (1 byte) = null
	
	// Pull or Call (1 byte) = null
	
	// Bulk Cancel (1 byte) = null
	
	// Open or Close (1 byte) = null
	
	// Symbol
	strncpy(&message[32], cancelOrder->symbol, SYMBOL_LEN);
	
	// Strike Date (8 bytes) = null

	// Side
	message[48] = cancelOrder->side;
	
	// Deliver To Comp ID (5 bytes) = null
	
	// Account (10 bytes)
	strncpy(&message[54], TradingAccount.ArcaDirectAccount[cancelOrder->accountSuffix], 10);
	
	// Padding (7 bytes)

	// Terminator
	message[71] = '\n';
	
	if (Send_ARCA_DIRECT_Msg(message, CANCEL_ORDER_MSG_LEN) == SUCCESS)
	{
		t_DataBlock dataBlock;
		dataBlock.msgLen = CANCEL_ORDER_MSG_LEN;
		
		// Set timestamp for current rawdata
		GetTimeFormatTime(dataBlock.addContent);
		
		memcpy(dataBlock.msgContent, message, CANCEL_ORDER_MSG_LEN);
		
		Add_ARCA_DIRECT_DataBlockToCollection(&dataBlock, cancelOrder->origClOrdID, cancelOrder->accountSuffix);

		return SUCCESS;
	}
	else
	{
		return ERROR;
	}
}

int ProcessARCA_DIRECT_NewOrder_V1(t_DataBlock *dataBlock)
{
	t_SendNewOrder order;

	// order.ECNId
	order.ECNId = TYPE_ARCA_DIRECT;

	// order.orderId
	order.orderId = __builtin_bswap32(*(unsigned int*) &(dataBlock->msgContent[8]));
	if (order.orderId < 500000) 
	{
		// Register account suffix
		accountMapping[order.orderId % MAX_ORDER_PLACEMENT] = dataBlock->addContent[ACCOUNT_SUFFIX_INDEX];
		
		return SUCCESS; //PM doesn't process rawdata of manual order
	}
	
	// "1", "2" or "5"
	switch (dataBlock->msgContent[51])
	{
		case '1':
			order.side = BUY_TO_CLOSE_TYPE;
			break;
		
		case '2':
			order.side = SELL_TYPE;
			break;
		
		case '5':
			order.side = SHORT_SELL_TYPE;
			break;
		
		default:
			order.side = -1;
			
			TraceLog(ERROR_LEVEL,  "ARCA DIRECT Order: not support side = %c\n", dataBlock->msgContent[51]);
			break;
	}
	
	// order.shares
	order.shares = __builtin_bswap32(*(unsigned int*) &(dataBlock->msgContent[16]));

	// order.symbol
	strncpy(order.symbol, (char*)&dataBlock->msgContent[27], SYMBOL_LEN);

	// Account	
	order.accountSuffix = dataBlock->addContent[ACCOUNT_SUFFIX_INDEX];
	
	int symbolIndex = GetStockSymbolIndex(order.symbol);

	if (symbolIndex == -1)
	{
		// Try to add it to Symbol List
		if ((symbolIndex = UpdateStockSymbolIndex(order.symbol)) == ERROR)
		{
			TraceLog(ERROR_LEVEL, "Cannot find symbol = %.8s in SymbolList\n", order.symbol);
			
			// Ignore current add message
			return ERROR;
		}
	}

	order.symbolIndex = symbolIndex;
		
	// order.price
	int tmpPrice = __builtin_bswap32(*(unsigned int*) &(dataBlock->msgContent[20]));
	int priceScale = dataBlock->msgContent[26];
	order.price = tmpPrice * _DIRECT_PRICE_PORTION[priceScale - 48];
	
	//Time In Force
	if(dataBlock->msgContent[53] == '3')	
	{
		order.tif = IOC_TYPE;
	}
	else if(dataBlock->msgContent[53] == '0')
	{
		order.tif = DAY_TYPE;
	}
	else
	{
		TraceLog(ERROR_LEVEL, "ARCA DIRECT: TimeInForce = %c \n", dataBlock->msgContent[53] );
		order.tif = -1;
	}

	order.type = BBO_ORDER;

	ProcessAddNewOrder(&order);

	return SUCCESS;
}

int ProcessARCA_DIRECT_NewOrder_V3(t_DataBlock *dataBlock)
{
	t_SendNewOrder order;

	// order.ECNId
	order.ECNId = TYPE_ARCA_DIRECT;

	// order.orderId
	order.orderId = __builtin_bswap32(*(unsigned int*) &(dataBlock->msgContent[8]));
	if (order.orderId < 500000) 
	{
		// Register account suffix
		accountMapping[order.orderId % MAX_ORDER_PLACEMENT] = dataBlock->addContent[ACCOUNT_SUFFIX_INDEX];
		
		return SUCCESS; //PM doesn't process rawdata of manual order
	}
	
	// "1", "2" or "5"
	switch (dataBlock->msgContent[93])
	{
		case '1':
			order.side = BUY_TO_CLOSE_TYPE;
			break;
		
		case '2':
			order.side = SELL_TYPE;
			break;
		
		case '5':
			order.side = SHORT_SELL_TYPE;
			break;
		
		default:
			order.side = -1;
			
			TraceLog(ERROR_LEVEL,  "ARCA DIRECT Order: not support side = %c\n", dataBlock->msgContent[93]);
			break;
	}
	
	// order.shares
	order.shares = __builtin_bswap32(*(unsigned int*) &(dataBlock->msgContent[16]));

	// order.symbol
	strncpy(order.symbol, (char*)&dataBlock->msgContent[61], SYMBOL_LEN);

	// Account	
	order.accountSuffix = dataBlock->addContent[ACCOUNT_SUFFIX_INDEX];
	
	int symbolIndex = GetStockSymbolIndex(order.symbol);

	if (symbolIndex == -1)
	{
		// Try to add it to Symbol List
		if ((symbolIndex = UpdateStockSymbolIndex(order.symbol)) == ERROR)
		{
			TraceLog(ERROR_LEVEL, "Cannot find symbol = %.8s in SymbolList\n", order.symbol);
			
			// Ignore current add message
			return ERROR;
		}
	}

	order.symbolIndex = symbolIndex;
		
	// order.price
	int tmpPrice = __builtin_bswap32(*(unsigned int*) &(dataBlock->msgContent[48]));
	int priceScale = dataBlock->msgContent[58];
	order.price = tmpPrice * _DIRECT_PRICE_PORTION[priceScale - 48];
	
	//Time In Force
	if(dataBlock->msgContent[95] == '3')	
	{
		order.tif = IOC_TYPE;
	}
	else if(dataBlock->msgContent[95] == '0')
	{
		order.tif = DAY_TYPE;
	}
	else
	{
		TraceLog(ERROR_LEVEL, "ARCA DIRECT: TimeInForce = %c \n", dataBlock->msgContent[95] );
		order.tif = -1;
	}

	order.type = BBO_ORDER;

	ProcessAddNewOrder(&order);

	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessARCA_DIRECT_OrderAckMsg
- Input:			+ dataBlock
- Output:		N/A
- Return:		Success or Failure
- Description:	+ The routine hides the following facts
				  - Update block length
				  - Update additional content
				  - Add data block to collection
				  - Update incoming sequence number
				  ARCA DIRECT configuration
				+ There are preconditions guaranteed to the routine
				  - 	dataBlock must be allocated and updated before
				+ The routine guarantees that the return value will 
						have a value of either
				  - Success
					or
				  - Failure
- Usage:			N/A
****************************************************************************/
int ProcessARCA_DIRECT_OrderAckMsg(t_DataBlock *dataBlock, int isTrading)
{
	/*
	- API version: ArcaDirect 4.1g
	- Date updated: 03-Jan-2013
	- Modified by: Long NKH
	- Order Ack Message (Variant 1), it includes the following fields:
		+ Message Type		1		Alpha/Numeric 	�a�
		+ Variant			1		Binary		Value = 1
		+ Length			2		Binary		Binary length of the message. Value = 48.
		+ Sequence Number	4		Binary		Arca-assigned sequence number
		+ Sending Time		8		Binary		UTC Time the message was sent in microseconds since Midnight
		+ Transaction Time	8		Binary		UTC Time the message was sent in microseconds since Midnight 
		+ Client Order Id	4		Binary		Client order ID
		+ Order ID			8		Binary		Exchange assigned Order ID
		+ Price				4		Binary		Price at which the order was acked
		+ Price Scale		1		Alpha/Numeric	"0" through "5"
		+ Liquidity			1		Alpha/Numeric	null, 1 = Candidate for Liquidity Indicator �S�, 2 = Blind, 3 = Not Blind
		+ Filler			5		Ignore
		+ Message Terminator 1		Alpha		ASCII new line character: �\n�
		==> Total byte		48
	*/
	
	// ECNOrderId
	long encOrderId = __builtin_bswap64(*(unsigned long*) &dataBlock->msgContent[28]);
	
	// Client OrderId
	int clOrderID = __builtin_bswap32(*(unsigned int*) &dataBlock->msgContent[24]);

	if (clOrderID < 500000) 
	{
		AutoCancelOrder.orderInfo[clOrderID % MAX_MANUAL_ORDER].marked = 1;
		AutoCancelOrder.orderInfo[clOrderID % MAX_MANUAL_ORDER].ecnOrderID = encOrderId;
		return SUCCESS; //PM doesn't process rawdata of manual order
	}

	int numSeconds, numMicroseconds;

	numSeconds = GetIntNumber(dataBlock->addContent);
	numMicroseconds = GetIntNumber(&dataBlock->addContent[4]);

	ProcessOrderAccepted(clOrderID, encOrderId, numSeconds, numMicroseconds, isTrading);
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessARCA_DIRECT_OrderFilledMsg
- Input:			+ dataBlock
- Output:		N/A
- Return:		Success or Failure
- Description:	+ The routine hides the following facts
				  - Check to process cross trade first
				  - Add data block to collection
				  ARCA DIRECT configuration
				+ There are preconditions guaranteed to the routine
				  - 	dataBlock must be allocated and updated before
				+ The routine guarantees that the return value will 
						have a value of either
				  - Success
					or
				  - Failure
- Usage:			N/A
****************************************************************************/
int ProcessARCA_DIRECT_OrderFilledMsg(t_DataBlock *dataBlock)
{
	/*
	- API version: ArcaDirect 4.1g
	- Date updated: 03-Jan-2013
	- Modified by: Long NKH
	- Order Fill Message (Variant 1), it includes the following fields:
		+ Message Type			1		Alpha Numeric 	�2�
		+ Variant				1		Binary		Value = 1
		+ Length				2		Binary		Binary length of the message. Value = 88.
		+ Sequence Number		4		Binary		Arca-assigned sequence number
		+ Sending Time			8		Binary		UTC Time the message was sent in microseconds since Midnight
		+ Transaction Time		8		Binary		UTC Time the message was sent in microseconds since Midnight 
		+ Client Order Id		4		Binary		Client order ID
		+ Order ID				8		Binary		Exchange assigned Order ID
		+ Execution ID			8		Binary		Exchange assigned Execution ID
		+ ArcaExID				20		Alpha/Numeric	Trade record
		+ Last Shares or Contracts	4		Binary		Number of equity shares or option contracts filled
		+ Last Price				4		Binary		Price at which the shares or contracts were filled
		+ Price Scale			1		Alpha Numeric 	�0� through �5�
		+ Liquidity Indicator	1		Alpha Numeric
		+ Side					1		Alpha Numeric
		+ LastMkt				2		Alpha
		+ Filler				10		Ignore
		+ Message Terminator	1		Alpha			ASCII new line character: �\n�
		==>Total byte		88
	*/
	
	int clOrderID = 0,
		shares = 0;
		
	clOrderID = __builtin_bswap32(*(unsigned int*) &(dataBlock->msgContent[24]));
	shares = __builtin_bswap32(*(unsigned int*) &(dataBlock->msgContent[64]));
	
	if (clOrderID < 500000) 
	{
		AutoCancelOrder.orderInfo[clOrderID % MAX_MANUAL_ORDER].leftShares -= shares;
		return SUCCESS; //PM doesn't process rawdata of manual order
	}
	
	

	ProcessOrderFilled(clOrderID, shares);

	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessARCA_DIRECT_BustOrCorrectMsg
- Input:			+ dataBlock
- Output:		N/A
- Return:		Success or Failure
- Description:	+ The routine hides the following facts
				  - Add data block to collection
				  ARCA DIRECT configuration
				+ There are preconditions guaranteed to the routine
				  - 	dataBlock must be allocated and updated before
				+ The routine guarantees that the return value will 
						have a value of either
				  - Success
					or
				  - Failure
- Usage:			N/A
****************************************************************************/
int ProcessARCA_DIRECT_BustOrCorrectMsg(t_DataBlock *dataBlock)
{
	/*
	- API version: ArcaDirect 4.1g
	- Date updated: 03-Jan-2013
	- Modified by: Long NKH
	- Bust or Correct Message (Variant 1) , it includes the following fields:
		+ Message Type		1		Alpha Numeric 	�C�
		+ Variant			1		Binary		Value = 1
		+ Length			2		Binary		Binary length of the message. Value = 48.
		+ Sequence Number	4		Binary		Exchange-assigned sequence number
		+ Sending Time		8		Binary		UTC Time the message was sent in microseconds since Midnight
		+ Transaction Time	8		Binary		UTC Time the message was sent in microseconds since Midnight 
		+ Client Order Id	4		Binary		Client order ID of the order that is being busted or corrected.
		+ Execution Id		8		Binary		Exchange assigned Execution ID from the order fill message (64 bit binary)
		+ Order Quantity	4		Binary		Shares or contracts executed
		+ Price				4		Binary		Corrected price in a correct message
		+ Price Scale		1		Alpha/Numeric	�0� through �5�
		+ Type				1		Alpha/Numeric	�1�=Bust  �2�=Correct 
		+ Filler			1		Ignore
		+ Message Terminator 1		Alpha		ASCII new line character: �\n�
		==>Total byte		48
	*/
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessARCA_DIRECT_OrderCancelAckMsg
- Input:			+ dataBlock
- Output:		N/A
- Return:		Success or Failure
- Description:	+ The routine hides the following facts
				  - Check to process cross trade first
				  - Add data block to collection
				  ARCA DIRECT configuration
				+ There are preconditions guaranteed to the routine
				  - 	dataBlock must be allocated and updated before
				+ The routine guarantees that the return value will 
						have a value of either
				  - Success
					or
				  - Failure
- Usage:			N/A
****************************************************************************/
int ProcessARCA_DIRECT_OrderCancelAckMsg(t_DataBlock *dataBlock)
{
	/*
	- API version: ArcaDirect 4.1g
	- Date updated: 03-Jan-2013
	- Modified by: Long NKH
	- Cancel Request Ack Message (Variant 1) , it includes the following fields:
		+ Message Type		1		Alpha Numeric 	'6'
		+ Variant			1		Binary		Value = 1
		+ Length			2		Binary		Binary length of the message. Value = 40.
		+ Sequence Number	4		Binary		Exchange-assigned sequence number
		+ Sending Time		8		Binary		UTC Time the message was sent in microseconds since Midnight
		+ Transaction Time	8		Binary		UTC Time the message was sent in microseconds since Midnight 
		+ Client Order Id	4		Binary		Client order ID of the order to be canceled.
		+ Order ID			8		Binary		Exchange assigned Order ID
		+ Filler			3		Ignore
		+ Message Terminator 1		Alpha		ASCII new line character: �\n�
		==> Total byte		40
	*/
	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessARCA_DIRECT_OrderRejectedMsg
- Input:			+ dataBlock
- Output:		N/A
- Return:		Success or Failure
- Description:	+ The routine hides the following facts
				  - Check to process cross trade first
				  - Add data block to collection
				  ARCA DIRECT configuration
				+ There are preconditions guaranteed to the routine
				  - 	dataBlock must be allocated and updated before
				+ The routine guarantees that the return value will 
						have a value of either
				  - Success
					or
				  - Failure
- Usage:			N/A
****************************************************************************/
int ProcessARCA_DIRECT_OrderRejectedMsg(t_DataBlock *dataBlock)
{
	/*
	- API version: ArcaDirect 4.1g
	- Date updated: 03-Jan-2013
	- Modified by: Long NKH
	- Order Cancel/Replace Reject Message (Variant 1), it includes the following fields:
		+ Message Type			1		Alpha Numeric 	�8�
		+ Variant				1		Binary		Value = 1
		+ Length				2		Binary		Binary length of the message. Value = 80.
		+ Sequence Number		4		Binary		Exchange-assigned sequence number
		+ Sending Time			8		Binary		UTC Time the message was sent in microseconds since Midnight
		+ Transaction Time		8		Binary		UTC Time the message was sent in microseconds since Midnight 
		+ Client Order Id			4	Binary		Client order id of the order, cancel, or cancel replace that was sent.
		+ Original Client Order ID	4	Binary		ID of original order
		+ Rejected Message Type		1	Numeric		�1�=order reject  �2�=cancel reject  �3�=cancel replace reject
		+ Text					40		Alpha		Reason for the rejection
		+ Reject Reason			1		Alpha		Fix Tag 102 or 103
		+ Filler				5		Ignore
		+ Message Terminator	1		Alpha		ASCII new line character: �\n�
		==> Total byte			80
	*/
	
	int clOrderID = 0; 
	clOrderID = __builtin_bswap32(*(unsigned int*) &(dataBlock->msgContent[24]));
	
	if (clOrderID < 500000)
	{
		AutoCancelOrder.orderInfo[clOrderID % MAX_MANUAL_ORDER].leftShares = 0;
		return SUCCESS; //PM doesn't process rawdata of manual order
	}
	
	if(dataBlock->msgContent[32] == '1') // order reject
	{
		if ((strncmp((char*)&dataBlock->msgContent[33], "Price too far outside", 21) == 0) ||
			(strncmp((char*)&dataBlock->msgContent[33], "Mkt/Symbol not open", 19) == 0) ||
			(strncmp((char*)&dataBlock->msgContent[33], "Symbol closed", 13) == 0))
		{
			int indexOpenOrder = GetOrderIdIndex(clOrderID);
			if (indexOpenOrder != -1)
			{
				char symbol[SYMBOL_LEN];
				strncpy(symbol, asOpenOrder.orderSummary[indexOpenOrder].symbol, SYMBOL_LEN);
				
				int symbolIndex = GetTradeSymbolIndex(symbol);
				if (symbolIndex == -1)
				{
					ProcessOrderRejected(clOrderID);
					return SUCCESS;
				}
				unsigned int haltTime;
				memcpy (&haltTime, dataBlock->addContent, 4);
				
				tradeSymbolList.symbolInfo[symbolIndex].haltTime[BOOK_ARCA] = haltTime;
				tradeSymbolList.symbolInfo[symbolIndex].haltStatus[BOOK_ARCA] = SYMBOL_STATUS_HALT;
				
				tradeSymbolList.symbolInfo[symbolIndex].haltStatusSent = NO;
				
				SendAHaltedStockToAS(&asCollection.connection, symbolIndex, 0);
			}
		}
		ProcessOrderRejected(clOrderID);
	}
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessARCA_DIRECT_OrderKilledMsg
- Input:			+ dataBlock
- Output:		N/A
- Return:		Success or Failure
- Description:	+ The routine hides the following facts
				  - Check to process cross trade first
				  - Add data block to collection
				  ARCA DIRECT configuration
				+ There are preconditions guaranteed to the routine
				  - 	dataBlock must be allocated and updated before
				+ The routine guarantees that the return value will 
						have a value of either
				  - Success
					or
				  - Failure
- Usage:			N/A
****************************************************************************/
int ProcessARCA_DIRECT_OrderKilledMsg(t_DataBlock *dataBlock)
{
	/*
	- API version: ArcaDirect 4.1g
	- Date updated: 03-Jan-2013
	- Modified by: Long NKH
	- Order Killed Message (Variant 1), it includes the following fields:
		+ Message Type		1		Alpha Numeric 	�4�
		+ Variant			1		Binary		Value = 1
		+ Length			2		Binary		Binary length of the message. Value = 40.
		+ Sequence Number	4		Binary		Exchange-assigned sequence number
		+ Sending Time		8		Binary		UTC Time the message was sent in microseconds since Midnight
		+ Transaction Time	8		Binary		UTC Time the message was sent in microseconds since Midnight 
		+ Client Order Id	4		Binary		Client order ID of the canceled order.
		+ Order ID			8		Binary		Exchange assigned Order ID
		+ Information Text	1		Binary		Indicates whether the kill was initiated by the user or by exchange rules.
		+ Filler			2		Ignore
		+ Message Terminator 1		Alpha		ASCII new line character: �\n�
		==>Total byte		40
	*/
	
	int clOrderID = 0;
	clOrderID = __builtin_bswap32(*(unsigned int*) &(dataBlock->msgContent[24]));
	
	if (clOrderID < 500000) 
	{
		AutoCancelOrder.orderInfo[clOrderID % MAX_MANUAL_ORDER].leftShares = 0;
		return SUCCESS; //PM doesn't process rawdata of manual order
	}
	
	ProcessOrderCanceled(clOrderID, -1);

	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessARCA_DIRECT_LogonAcceptedMsg
- Input:			+ dataBlock
- Output:		N/A
- Return:		Success or Failure
- Description:	+ The routine hides the following facts
				  - Parse message content
				  - Update Outgoing Sequence Number of 
				  ARCA DIRECT configuration
				+ There are preconditions guaranteed to the routine
				  - 	dataBlock must be allocated and updated before
				+ The routine guarantees that the return value will 
						have a value of either
				  - Success
					or
				  - Failure
- Usage:			N/A
****************************************************************************/
int ProcessARCA_DIRECT_LogonAcceptedMsg(t_DataBlock *dataBlock)
{
	TraceLog(DEBUG_LEVEL, "Logon to ARCA DIRECT was successful\n");
				
	/*
	Update Outgoing Sequence Number before send any messages to ARCA DIRECT Server
	*/
	ARCA_DIRECT_Config.outgoingSeqNum = __builtin_bswap32(*(unsigned int*)&dataBlock->msgContent[OUTGOING_SEQ_NUM_INDEX]);
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessARCA_DIRECT_LogonRejectedMsg
- Input:			+ dataBlock
- Output:		N/A
- Return:		Success or Failure
- Description:	+ The routine hides the following facts
				  - Parse message content
				  - Update Outgoing Sequence Number of 
				  ARCA DIRECT configuration
				+ There are preconditions guaranteed to the routine
				  - 	dataBlock must be allocated and updated before
				+ The routine guarantees that the return value will 
						have a value of either
				  - Success
					or
				  - Failure
- Usage:			N/A
****************************************************************************/
int ProcessARCA_DIRECT_LogonRejectedMsg(t_DataBlock *dataBlock)
{
	TraceLog(DEBUG_LEVEL, "ARCA DIRECT logon rejected\n");
	return SUCCESS;
}

/****************************************************************************
- Function name:	Send_ARCA_DIRECT_Msg
- Input:			+ message
				+ msgLen
- Output:		N/A
- Return:		Success or Failure
- Description:	+ The routine hides the following facts
				  - Send message to ARCA DIRECT
				+ There are preconditions guaranteed to the routine
				  - 	message and msgLen must be checked before
				+ The routine guarantees that the return value will 
						have a value of
				  - message length
- Usage:			N/A
****************************************************************************/
int Send_ARCA_DIRECT_Msg(const char *message, int msgLen)
{
	pthread_mutex_lock(&ADSockMutex);
		if (send(ARCA_DIRECT_Config.socket, message, msgLen, 0) == msgLen)
		{
			pthread_mutex_unlock(&ADSockMutex);
			return SUCCESS;
		}
	pthread_mutex_unlock(&ADSockMutex);
	
	TraceLog(ERROR_LEVEL, "Arca Direct: Send_ARCA_DIRECT_Msg() error\n");
	PrintErrStr(ERROR_LEVEL, "calling send(): ", errno);
	
	orderStatusMgmt[ARCA_DIRECT_INDEX].shouldConnect = NO;
	return ERROR;
}

/****************************************************************************
- Function name:	Receive_ARCA_DIRECT_Msg
- Input:
- Output:		
- Return:		Success or Failure
- Description:	
- Usage:		N/A
****************************************************************************/
int Receive_ARCA_DIRECT_Msg(t_DataBlock* dataBlock)
{	
	int numReceivedBytes = 0;
	int recvBytes = 0;

	// receive header bytes
	while(numReceivedBytes < ARCA_DIRECT_MSG_HEADER_LEN)
	{
		errno = 0;
		recvBytes = recv(ARCA_DIRECT_Config.socket, &dataBlock->msgContent[numReceivedBytes], ARCA_DIRECT_MSG_HEADER_LEN - numReceivedBytes, 0);

		if(recvBytes > 0)
		{
			numReceivedBytes += recvBytes;
		}
		else if ( (recvBytes == -1) && (errno == EAGAIN) ) //Timeout
		{
			TraceLog(ERROR_LEVEL, "ARCA DIRECT: Socket receive-timeout\n");
			return ERROR;
		}
		else if ( (recvBytes == 0) && (errno == 0) )	//Server Disconnected
		{
			TraceLog(ERROR_LEVEL, "ARCA DIRECT: Connection is terminated\n");
			return ERROR;
		}
		else
		{
			TraceLog(ERROR_LEVEL, "ARCA DIRECT: recv() returned %d, errno: %d\n", numReceivedBytes, errno);
			PrintErrStr(ERROR_LEVEL, "recv(): ", errno);
			return ERROR;
		}
	}

	//receive body message bytes
	t_ShortConverter msgLen;
	msgLen.c[0] = dataBlock->msgContent[ARCA_DIRECT_MSG_HEADER_LEN - 1];
	msgLen.c[1] = dataBlock->msgContent[ARCA_DIRECT_MSG_HEADER_LEN - 2];
	dataBlock->msgLen = (unsigned short)msgLen.value;

	while(numReceivedBytes < dataBlock->msgLen)
	{
		errno = 0;
		recvBytes = recv(ARCA_DIRECT_Config.socket, &dataBlock->msgContent[numReceivedBytes], dataBlock->msgLen - numReceivedBytes, 0);

		if(recvBytes > 0)
		{
			numReceivedBytes += recvBytes;
		}
		else if ( (recvBytes == -1) && (errno == EAGAIN) ) //Timeout
		{
			TraceLog(ERROR_LEVEL, "ARCA DIRECT: Socket receive-timeout.\n");
			return ERROR;
		}
		else if ( (recvBytes == 0) && (errno == 0) )	//Server Disconnected
		{
			TraceLog(ERROR_LEVEL, "ARCA DIRECT: Connection is terminated\n");
			return ERROR;
		}
		else
		{
			TraceLog(ERROR_LEVEL, "ARCA DIRECT: recv() returned %d, errno: %d\n", numReceivedBytes, errno);
			PrintErrStr(ERROR_LEVEL, "recv(): ", errno);
			
			return ERROR;
		}
	}
	
	return SUCCESS;
}

/****************************************************************************
- Function name:	GetClOrderId_ARCA_DIRECT
- Input:
- Output:		
- Return:		Success or Failure
- Description:	
- Usage:		N/A
****************************************************************************/
int GetClOrderId_ARCA_DIRECT(const unsigned char *msgContent)
{
	return __builtin_bswap32(*(unsigned int*) &msgContent[24]);
}

/****************************************************************************
- Function name:	Add_ARCA_DIRECT_DataBlockToCollection
- Input:			+ dataBlock
- Output:		ARCA DIRECT collection will be updated with new data block
- Return:		N/A
- Description:	
				
- Usage:		N/A
****************************************************************************/
void Add_ARCA_DIRECT_DataBlockToCollection(t_DataBlock *dataBlock, int clOrderId, int accountSuffix)
{
	// Update Block Length
	//dataBlock->blockLen = 4 + 4 + 4 + MAX_ADD_INFO_LEN + dataBlock->msgLen;
	dataBlock->blockLen = 12 + MAX_ADD_INFO_LEN + dataBlock->msgLen;

	// Set timestamp for current rawdata
	GetTimeFormatTime(dataBlock->addContent);

	dataBlock->addLen = MAX_ADD_INFO_LEN;

	if (clOrderId < 500000)
	{
		//We will use value at CROSS_ID_INDEX to mark this rawdata is of a manual order
		int manualOrderMarking = -9876;
		dataBlock->addContent[ACCOUNT_SUFFIX_INDEX] = accountMapping[clOrderId % MAX_ORDER_PLACEMENT];
		memcpy(&dataBlock->addContent[CROSS_ID_INDEX], &manualOrderMarking, 4);
	}
	else
	{
		if(accountSuffix != -1)
		{
			dataBlock->addContent[ACCOUNT_SUFFIX_INDEX] = accountSuffix;
		}
		else
		{
			int indexOpenOrder = GetOrderIdIndex(clOrderId);
			if (indexOpenOrder != -1) 
			{
				dataBlock->addContent[ACCOUNT_SUFFIX_INDEX] = asOpenOrder.orderSummary[indexOpenOrder].accountSuffix;
			}
		}
	}
	
	// Add data block to collection
	AddDataBlockToCollection(dataBlock, &pmRawDataMgmt[ARCA_DIRECT_INDEX]);
	
	// Save data block to file
	SaveDataBlockToFile(&pmRawDataMgmt[ARCA_DIRECT_INDEX]);
}

/****************************************************************************
- Function name:	Get_ARCA_DIRECT_OutgoingSeqNum
- Input:			N/A
- Output:		N/A
- Return:		Outgoing sequence number
- Description:	+ The routine hides the following facts
				  - Get outgoing sequence number
				  - Increase outgoing sequence number
				  - Use Mutex to synchronize manipulating on outgoing sequence number
				+ There are no preconditions guaranteed to the routine
				+ The routine guarantees that the return value will 
						have a value of
				  - Outgoing sequence number
- Usage:			N/A
****************************************************************************/
inline int Get_ARCA_DIRECT_OutgoingSeqNum(void)
{
	return __sync_fetch_and_add(&ARCA_DIRECT_Config.outgoingSeqNum, 1);
}
