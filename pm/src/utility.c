/*****************************************************************************
**	Project:		Equity Arbitrage Application
**	Filename:		utility.c
**	Description:	This file contains function definitions that were declared
					in utility.h	
**	Author:			Luan Vo-Kinh
**	First created:	17-Sep-2007
**	Last updated:	-----------
*****************************************************************************/

/****************************************************************************
** Include files and define several variables
****************************************************************************/
#define _XOPEN_SOURCE
#define _GNU_SOURCE
#include <stdarg.h>
#include <execinfo.h>
#include <sys/ioctl.h>
#include <net/if.h>

#include "utility.h"
#include "book_mgmt.h"
#include "configuration.h"
#include <arpa/inet.h>
#include <errno.h>
#include "environment_proc.h"
#include "hbitime.h"

int Secs_in_years = 0;
int Current_year = 0;

char globalDateStringOfData[MAX_LINE_LEN];
char globalDateStringOfDatabase[MAX_LINE_LEN];

extern t_DBLMgmt DBLMgmt;

/****************************************************************************
** Function definitions
****************************************************************************/

/****************************************************************************
- Function name:	GetFullConfigPath
- Input:			+ fileName
				+ pathType
- Output:		+ fullConfigPath
- Return:		NULL or pointer to fullConfigPath
- Description:	+ The routine hides the following facts
				  - Create full configuration path to file name path type based
				+ There are preconditions guaranteed to the routine
				  - fullConfigPath must be allocated before 	
				+ The routine guarantees that the status value will 
					have a value of either
				  - Pointer to full configuraton path
					or
				  - Failure
- Usage:			N/A
****************************************************************************/
char *GetFullConfigPath(const char *fileName, int pathType, char *fullConfigPath)
{
	if (fullConfigPath == NULL)
	{
		return NULL;
	}
	
	/* 
	Base on path type, fullConfigPath will be
	updated appropriately
	*/ 
	switch (pathType)
	{
		case CONF_BOOK:
			return Environment_get_book_conf_filename(fileName, fullConfigPath, MAX_PATH_LEN);
		case CONF_ORDER:
			return Environment_get_order_conf_filename(fileName, fullConfigPath, MAX_PATH_LEN);
		case CONF_SYMBOL:
			return Environment_get_symbol_conf_filename(fileName, fullConfigPath, MAX_PATH_LEN);
		case CONF_MISC:
			return Environment_get_misc_conf_filename(fileName, fullConfigPath, MAX_PATH_LEN);
		default:
			return NULL;
	}
	
	return fullConfigPath;
}

/****************************************************************************
- Function name:	GetFullRawDataPath
- Input:			+ fileName
- Output:		+ fullConfigPath
- Return:		NULL or pointer to fullConfigPath
- Description:	+ The routine hides the following facts
				  - Create full raw data path to file name
				+ There are preconditions guaranteed to the routine
				  - fullConfigPath must be allocated and enough to store data path 	
				+ The routine guarantees that the status value will 
					have a value of either
				  - Pointer to full raw data path
					or
				  - Failure
- Usage:			N/A
****************************************************************************/
char *GetFullRawDataPath(const char *fileName, char *fullDataPath)
{
	// Build full raw data path
	return Environment_get_data_filename(fileName, fullDataPath, MAX_PATH_LEN);
}

/****************************************************************************
- Function name:	CreateDataPath
- Input:			N/A
- Output:		N/A
- Return:		Success or Failure
- Description:
- Usage:			N/A
****************************************************************************/
int CreateDataPath(void)
{
	char fullRawDataPath[512] = "\0";
	// Build full raw data path
	Environment_get_root_data_dir(fullRawDataPath, MAX_PATH_LEN);

	// Command to create directories
	char command[MAX_PATH_LEN];
	sprintf(command, "mkdir -p %s", fullRawDataPath);

	// Create directory
	system(command);

	TraceLog(DEBUG_LEVEL, "Root Data path: %s\n", fullRawDataPath);

	return SUCCESS;
}

/****************************************************************************
- Function name:	Lrc_is_leap
- Input:			[To be done]
- Output:		[To be done]
- Return:		[To be done]
- Description:	+ The routine hides the following facts
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
inline int Lrc_is_leap(int year)
{
	year += 1900; /* Get year, as ordinary humans know it */

	/*
	 *	 The rules for leap years are not
	 *	 as simple as "every fourth year
	 *	 is leap year":
	 */

	if( (unsigned int)year % 100 == 0 ) {
		return (unsigned int)year % 400 == 0;
	}

	return (unsigned int)year % 4 == 0;
}

/****************************************************************************
- Function name:	GetShortNumber
- Input:			+ buffer
- Output:		N/A
- Return:		Integer number
- Description:	+ The routine hides the following facts
				  - Convert 2 bytes in buffer to short value
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
int GetShortNumber(unsigned char *buffer)
{
	t_ShortConverter shortConverter;
	memcpy(shortConverter.c, buffer, 2);
	
	return shortConverter.value;
}

int GetUnsignedShortNumber(const unsigned char *buffer)
{
	t_ShortConverter converter;
	
	converter.c[0] = buffer[0];
	converter.c[1] = buffer[1];
	
	return (unsigned short)converter.value;
}

/****************************************************************************
- Function name:	GetShortNumberBigEndian
- Input:			+ buffer
- Output:		N/A
- Return:		Integer number
- Description:	+ The routine hides the following facts
				  - Convert 2 bytes in buffer to short value
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
int GetShortNumberBigEndian(unsigned char *buffer)
{
	t_ShortConverter converter;
	
	converter.c[1] = buffer[0];
	converter.c[0] = buffer[1];
	
	return converter.value;
}

/****************************************************************************
- Function name:	GetIntNumber
- Input:			+ buffer
- Output:		N/A
- Return:		Integer number
- Description:	+ The routine hides the following facts
				  - Convert 4 bytes in buffer to int value
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
int GetIntNumber(unsigned char *buffer)
{
	t_IntConverter intConverter;
	memcpy(intConverter.c, buffer, 4);
	
	return intConverter.value;
}

int GetUnsignedIntNumber(const unsigned char *buffer)
{
	t_IntConverter converter;
	converter.c[0] = buffer[0];
	converter.c[1] = buffer[1];
	converter.c[2] = buffer[2];
	converter.c[3] = buffer[3];
	
	return (unsigned int)converter.value;
}


/****************************************************************************
- Function name:	GetIntNumberBigEndian
- Input:			+ buffer
- Output:		N/A
- Return:		Integer number
- Description:	+ The routine hides the following facts
				  - Convert 4 bytes in buffer to short value
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
int GetIntNumberBigEndian(unsigned char *buffer)
{
	t_IntConverter converter;
	converter.c[3] = buffer[0];
	converter.c[2] = buffer[1];
	converter.c[1] = buffer[2];
	converter.c[0] = buffer[3];
	
	return converter.value;
}

/****************************************************************************
- Function name:	GetDoubleNumber
- Input:			+ buffer
- Output:		N/A
- Return:		Integer number
- Description:	+ The routine hides the following facts
				  - Convert 8 bytes in buffer to double value
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
double GetDoubleNumber(unsigned char *buffer)
{
	t_DoubleConverter doubleConverter;
	memcpy(doubleConverter.c, buffer, 8);
	
	return doubleConverter.value;
}

/****************************************************************************
- Function name:	RemoveCharacters
- Input:			+ buffer
				+ bufferLen
- Output:		+ buffer
- Return:		Buffer with removed used characters
- Description:	+ The routine hides the following facts
				  - Check byte by byte with ASCII code 32 (SPACE) and remove
				  any characters less than or equal
				+ There are preconditions guaranteed to the routine
				  - buffer must be allocated and initiazed before
				  - buffeLen must be indicated exactly length of 
				  buffer
				+ The routine guarantees that the status value will 
					have value of
				  - buffer
- Usage:			N/A
****************************************************************************/
char *RemoveCharacters(char *buffer, int bufferLen)
{
	int i;
	
	for (i = bufferLen -1 ; i > -1; i--)
	{
		if (buffer[i] <= USED_CHAR_CODE)
		{	
			buffer[i] = 0;
		}
		else
		{
			break;
		}
	}
	
	return buffer;
}

/****************************************************************************
- Function name:	Lrc_itoa
- Input:			+ value
- Output:		+ strResult
- Return:		[To be done]
- Description:	+ The routine hides the following facts
				  - Convert value from integer data type to 'string'
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
int Lrc_itoa(int value, char *strResult)
{
	char temp[11];
	register int index = 0;
	register int i = 0;
	
	if (value > 0)
	{
		while (value != 0)
		{
			temp[index] = (48 + value % 10);
			value = value / 10;	
			index++;
		}

		for (i = index - 1; i > -1; i--)
		{
			*strResult++ = (char)temp[i];
		}

		*strResult = 0;
		
		return index;
	}
	else if (value < 0)
	{
		*strResult++ = '-';

		value = -1 * value;

		while (value != 0)
		{
			temp[index] = (char)(48 + value % 10);
			value = value / 10;	
			index++;
		}

		for (i = index - 1; i > -1; i--)
		{
			*strResult++ = (char)temp[i];
		}

		*strResult = 0;
		
		return (index + 1);
	}
	else
	{
		strResult[0] = 48;
		strResult[1] = 0;
		
		return 1;
	}
}

/****************************************************************************
- Function name:	Lrc_itoaf
- Input:			+ value
				+ pad
				+ len
- Output:		+ strResult
- Return:		[To be done]
- Description:	+ The routine hides the following facts
				  - Convert value from integer data type to 'string'
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
int Lrc_itoaf(int value, const char pad, const int len, char *strResult)
{
	register int i = 0;
	if (value == 0)
	{
		for (i = 0; i < len; i++)
		{
			strResult[i] = pad;
		}
		strResult[len] = 0;

		return len;
	}

	register int index = 0;

	while (value != 0)
	{
		strResult[len - index - 1] = 48 + value % 10;
		value = value / 10;	
		index++;
	}
	
	for (i = 0; i < len - index; i++)
	{
		strResult[i] = pad;
	}

	strResult[len] = 0;

	return len;
}

/****************************************************************************
- Function name:	GetTimeFormatTime
- Input:			+ formattedTime
- Output:		N/A
- Return:		Pointer to formattedTime buffer
- Description:	+ The routine hides the following facts
				  - Get current time
				  - Format it base on format: yyyy/mm/dd-hh:MM:ss.mss AM/PM
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
unsigned char *GetTimeFormatTime(unsigned char *formattedTime)
{
	int sec, usec;
	hbitime_micros_parts(&sec, &usec);
	
	memcpy(formattedTime, &sec, 4);
	memcpy(&formattedTime[4], &usec, 4);
	formattedTime[8] = 0;
	return formattedTime;
}

/****************************************************************************
- Function name:	GetLocalTimeString
- Input:			
- Output:		
- Return:		
- Description:
- Usage:		
****************************************************************************/
char *GetLocalTimeString(char *timeString)
{
	int seconds, micros;
	hbitime_micros_parts(&seconds, &micros);
	time_t now = seconds;
	struct tm *l_time = (struct tm *)localtime(&now);

	// Get time string
	strftime(timeString, 16, "%H:%M:%S", l_time);
	sprintf(timeString + strlen(timeString), ".%06d", micros);

	return timeString;
}

/****************************************************************************
- Function name:	Lrc_gmtime
- Input:			[To be done]
- Output:		[To be done]
- Return:		[To be done]
- Description:	+ The routine hides the following facts
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
inline struct tm *Lrc_gmtime (time_t *tp, int years, int secs_in_years, struct tm *tm2)
{
	static int __days_per_month[] = {31,28,31,30,31,30,31,31,30,31,30,31};
	//static struct tm tm2;
	time_t t;

	t = *tp;
	tm2->tm_sec  = 0;
	tm2->tm_min  = 0;
	tm2->tm_hour = 0;
	tm2->tm_mday = 1;
	tm2->tm_mon  = 0;
	tm2->tm_year = years;

	t -= secs_in_years;

	tm2->tm_yday = t / LRC_SECS_PER_DAY;				/* days since Jan 1 */

	if ( Lrc_is_leap(tm2->tm_year) )					/* leap year ? */
		__days_per_month[1]++;

	while ( t >= __days_per_month[tm2->tm_mon] * LRC_SECS_PER_DAY ) {
		t -= __days_per_month[tm2->tm_mon++] * LRC_SECS_PER_DAY;
	}
	
	(tm2->tm_mon)++;

	if ( Lrc_is_leap(tm2->tm_year) )					/* leap year ? */
		__days_per_month[1]--;

	tm2->tm_mday = t / LRC_SECS_PER_DAY + 1;
	t = t % LRC_SECS_PER_DAY;

	tm2->tm_hour = t / LRC_SECS_PER_HOUR;
	t = t % LRC_SECS_PER_HOUR;
	
	tm2->tm_min = t / LRC_SECS_PER_MINUTE;
	tm2->tm_sec = t % LRC_SECS_PER_MINUTE;

	return tm2;
}

/****************************************************************************
- Function name:	Get_secs_in_years
- Input:			[To be done]
- Output:		[To be done]
- Return:		[To be done]
- Description:	+ The routine hides the following facts
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
inline int Get_secs_in_years(time_t *tp, int *year, int *secs_in_years)
{
	static struct tm tm2;
	time_t t,secs_this_year;

	t = *tp;
	tm2.tm_year = 70;

	/*
	 *	This loop handles dates in 1970 and later
	 */
	while ( t >= ( secs_this_year =
				   Lrc_is_leap(tm2.tm_year) ?
				   LRC_SECS_PER_LEAP :
				   LRC_SECS_PER_YEAR ) ) {
		t -= secs_this_year;
		tm2.tm_year++;
	}

	/*
	 *	This loop handles dates before 1970
	 */
	while ( t < 0 )
		t += Lrc_is_leap(--tm2.tm_year) ? LRC_SECS_PER_LEAP : LRC_SECS_PER_YEAR;
	
	*secs_in_years = (*tp - t);
	*year = tm2.tm_year + 1900;

	return (*tp - t);
}

/****************************************************************************
- Function name:	TrimRight
- Input:			+ buffer
- Output:		
- Return:		
- Description:	+ 
- Usage:		
****************************************************************************/
int TrimRight( char * buffer )
{
	int i;
	for( i = 0; i < strlen( buffer ); i ++ )
	{
		if( buffer[i] == ' ' )
		{
			buffer[i] = 0;

			break;
		}	
	}
	
	return 0;
}

/****************************************************************************
- Function name:	Lrc_Split
- Input:			
- Output:		
- Return:		
- Description:
- Usage:		
****************************************************************************/
char Lrc_Split(const char *str, char c, void *parameter)
{
	t_Parameters *workingList = (t_Parameters *)parameter;

	int i = 0;
	
	// Initialize ...
	workingList->countParameters = 0;

	while ((*str != 0) && (*str != 10) && (*str != 13))
	{
		if (*str != c)
		{
			workingList->parameterList[workingList->countParameters][i] = *str;

			i ++;
		}
		else
		{
			if (i > 0)
			{
				workingList->parameterList[workingList->countParameters][i] = 0;

				workingList->countParameters ++;				
			}

			i = 0;
		}

		str ++;
	}

	if (i > 0)
	{
		workingList->parameterList[workingList->countParameters][i] = 0;

		workingList->countParameters ++;
	}

	return workingList->countParameters;
}

/****************************************************************************
- Function name:	GetDateString
- Input:			
- Output:		
- Return:		
- Description:
- Usage:		
****************************************************************************/
char *GetDateString(int numberOfSeconds, char *dateString)
{
	struct tm *l_time;
	time_t numberOfSecondsTmp = numberOfSeconds;
	
	// Convert to local time
	l_time = (struct tm *)localtime(&numberOfSecondsTmp);
	
	// Get date string 
	strftime(dateString, 16, "%Y/%m/%d", l_time);

	return dateString;
}

/****************************************************************************
- Function name:	GetNumberOfSecondsFromTimestamp
- Input:			
- Output:		
- Return:		
- Description:
- Usage:		
****************************************************************************/
int GetNumberOfSecondsFromTimestamp(const char *timestamp)
{
	// Get number of seconds from timestamp (HH:MM:SS.ssssss)
	return (((timestamp[0] - 48) * 10 + (timestamp[1] - 48)) * 3600 + 
			((timestamp[3] - 48) * 10 + (timestamp[4] - 48)) * 60 + 
			(timestamp[6] - 48) * 10 + (timestamp[7] - 48));
}

/****************************************************************************
- Function name:	GetNumberOfSecondsFrom1970
- Input:			
- Output:		
- Return:		
- Description:
- Usage:		
****************************************************************************/
int GetNumberOfSecondsFrom1970(const char *format, const char *dateString)
{
	struct tm tmpDate;
	time_t numSeconds;
	char *valRet;

	// Get seconds since 1970
	valRet = strptime(dateString, format, &tmpDate);

	if (valRet != 0)
	{
		tmpDate.tm_hour = 0;
		tmpDate.tm_min = 0;
		tmpDate.tm_sec = 0;
		
		numSeconds = mktime(&tmpDate);
		
		return numSeconds;
	}
	else
	{
		return ERROR;
	}
}

/****************************************************************************
- Function name:	Lrc_atoi
- Input:			+ value
- Output:		+ strResult
- Return:		[To be done]
- Description:	+ The routine hides the following facts
				  - Convert value from string data type to integer
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
inline int Lrc_atoi(const char * buffer, int len)
{
	register int retval = 0;

	while (*buffer == 32)
	{
		buffer++;
		len--;
	}
	
	for (; len > 0; buffer++, len--)
	{
		retval = retval * 10 + ((*buffer) - 48);
	}
	return retval;
}

/****************************************************************************
- Function name:	GetLongNumber
- Input:			+ buffer
- Output:		N/A
- Return:		Long number
- Description:	+ The routine hides the following facts
				  - Convert 8 bytes in buffer to long value
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
long GetLongNumber(const char *buffer)
{
	t_LongConverter converter;
	
	memcpy(converter.c, buffer, 8);
	
	return converter.value;
}

/****************************************************************************
- Function name:	GetLongNumberBigEndian
- Input:			+ buffer
- Output:		N/A
- Return:		Long number
- Description:	+ The routine hides the following facts
				  - Convert 8 bytes in buffer to long value
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
long GetLongNumberBigEndian(const unsigned char *buffer)
{
	t_LongConverter converter;
	
	converter.c[7] = buffer[0];
	converter.c[6] = buffer[1];
	converter.c[5] = buffer[2];
	converter.c[4] = buffer[3];
	converter.c[3] = buffer[4];
	converter.c[2] = buffer[5];
	converter.c[1] = buffer[6];
	converter.c[0] = buffer[7];
	
	return converter.value;
}

/****************************************************************************
- Function name:	GetUnsignedShortNumberBigEndian
- Input:			+ buffer
- Output:		N/A
- Return:		Integer number
- Description:	+ The routine hides the following facts
				  - Convert 2 bytes in buffer to unsiged short value
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
int GetUnsignedShortNumberBigEndian(const unsigned char *buffer)
{
	t_ShortConverter converter;
	
	converter.c[1] = buffer[0];
	converter.c[0] = buffer[1];
	
	return (unsigned short)converter.value;
}

/****************************************************************************
- Function name:	GetUnsignedIntNumberBigEndian
- Input:			+ buffer
- Output:		N/A
- Return:		Integer number
- Description:	+ The routine hides the following facts
				  - Convert 4 bytes in buffer to unsiged int value
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
int GetUnsignedIntNumberBigEndian(const unsigned char *buffer)
{
	t_IntConverter converter;
	converter.c[3] = buffer[0];
	converter.c[2] = buffer[1];
	converter.c[1] = buffer[2];
	converter.c[0] = buffer[3];
	
	return (unsigned int)converter.value;
}

/****************************************************************************
- Function name:	GetLocalTimeInTradingTime
- Input:			
- Output:		
- Return:		
- Description:
- Usage:		
****************************************************************************/
int GetLocalTimeInTradingTime(void)
{
	// Get current time
	time_t now = hbitime_seconds();

	struct tm l_time;
	
	// Convert to local time
	localtime_r(&now, &l_time);

	int currentTime = l_time.tm_hour * 3600 + l_time.tm_min * 60 + l_time.tm_sec;
	
	// Check to see if currentTime is incorrect, get localtime again
	// 06:00:00 -> 6*3600 + 0*60 + 0 = 21600
	if (currentTime < 21600)
	{
		localtime_r(&now, &l_time);
		
		currentTime = l_time.tm_hour * 3600 + l_time.tm_min * 60 + l_time.tm_sec;
	}
	
	return currentTime;
}

/*************************************************
- Function Name: 	GetMacAddress
- Input:			
- Output:			
- Return:        	ERROR or SUCCESS
- Description:   
- Usage:	 
*************************************************/
int GetMacAddress(char *ifName, char *mac)
{
	struct ifreq s;
	int fd = socket(PF_INET, SOCK_DGRAM, IPPROTO_IP);
	char tmp[6];
	strcpy(s.ifr_name, ifName);
	if (0 == ioctl(fd, SIOCGIFHWADDR, &s))
	{
		memcpy(tmp, s.ifr_addr.sa_data, 6);
		sprintf(mac, "%02X:%02X:%02X:%02X:%02X:%02X", (unsigned char) tmp[0], (unsigned char)tmp[1], (unsigned char)tmp[2], (unsigned char)tmp[3], (unsigned char)tmp[4], (unsigned char)tmp[5]);
		close(fd);
		return SUCCESS;
	}
	
	close(fd);
	return ERROR;
}

/*************************************************
- Function Name: 	GetIPAddress
- Input:			
- Output:			
- Return:        	
- Description:   
- Usage:	 
*************************************************/
int GetIPAddress(char *if_name, char *ip)
{
	struct ifreq ifr;
	size_t if_name_len = strlen(if_name);
	
	if (if_name_len < sizeof(ifr.ifr_name))
	{
		memcpy(ifr.ifr_name, if_name, if_name_len);
		ifr.ifr_name[if_name_len] = 0;
	}
	else
	{
		TraceLog(ERROR_LEVEL, "Interface name is too long\n");
		ip[0] = 0;
		return ERROR;
	}
	
	int fd = socket(AF_INET, SOCK_DGRAM, 0);
	if (fd == -1)
	{
		PrintErrStr(ERROR_LEVEL, "GetIPAddress, socket(): ", errno);
		ip[0] = 0;
		close(fd);
		return ERROR;
	}
	
	ifr.ifr_addr.sa_family = AF_INET;	/* I want to get an IPv4 IP address */
	
	if (ioctl(fd, SIOCGIFADDR, &ifr) == -1)
	{
		PrintErrStr(ERROR_LEVEL, "GetIPAddress, ioctl(): ", errno);
		ip[0] = 0;
		close(fd);
		return ERROR;
	}
	close(fd);
	
	struct sockaddr_in* ipaddr = (struct sockaddr_in*)&ifr.ifr_addr;
	strcpy(ip, inet_ntoa(ipaddr->sin_addr));
	
	return SUCCESS;
}

/*************************************************
- Function Name: 	CreateDBLDevice
- Input:			NIC Name to use
- Output:			DBL Device
- Return:        	ERROR or SUCCESS
- Description:
- Usage:
*************************************************/
int CreateDBLDevice(char *NIC, dbl_device_t *dev)
{
	int ret = dbl_open_if(NIC, DBL_OPEN_THREADSAFE, dev);
	if (ret != 0)
	{
		TraceLog(ERROR_LEVEL, "dbl_open_if() failed (%d)\n", ret);
		return ERROR;
	}

	return SUCCESS;
}

/*************************************************
- Function Name: 	CreateDBLDevice
- Input:			Interface name to use
- Output:			DBL Device
- Return:        	ERROR or SUCCESS
- Description:   
- Usage:	 
*************************************************/
int CreateDBLDeviceV2(char *NIC, dbl_device_t *dev)
{
	//--------------------------------------------------------------------------
	// dblOpenFlags value:
	//	DBL_OPEN_THREADSAFE			0x1
	//	DBL_OPEN_DISABLED			0x2
	//	DBL_OPEN_HW_TIMESTAMPING	0x4
	// If do not use flag, set dblOpenFlags to 0 when calling this function
	// For using multiple dblOpenFlags: use XOR operator when calling this function
	//	ex: dblOpenFlags = DBL_OPEN_THREADSAFE | DBL_OPEN_HW_TIMESTAMPING
	//--------------------------------------------------------------------------
	pthread_mutex_lock(&DBLMgmt.mutexLock);
	
	// --------------------------------------------------------
	// Get MacAddress of Interface,
	// if mac address is opened, we will reuse the dbl device
	// otherwiase, create new device
	// --------------------------------------------------------
	char macAddr[128];
	int ret = GetMacAddress(NIC, macAddr);
	if (ret == ERROR)
	{
		TraceLog(ERROR_LEVEL, "Could not get MAC Address for interface named '%s'\n", NIC);
		pthread_mutex_unlock(&DBLMgmt.mutexLock);
		return ERROR;
	}
	
	//Search if mac address is opened for dbl
	int i, freeSlot = -1;
	for (i = 0; i < MAX_DBL_INSTANCE; i++)
	{
		if (strcmp(macAddr, DBLMgmt.dblInstance[i].macAddress) == 0)
		{
			break;	//found
		}
		else if ((freeSlot == -1) && (DBLMgmt.dblInstance[i].macAddress[0] == 0))
		{
			freeSlot = i;
		}
	}
	
	if (i != MAX_DBL_INSTANCE)
	{
		//found, mac address was opened before, we will reuse
		*dev = DBLMgmt.dblInstance[i].dev;
		DBLMgmt.dblInstance[i].instanceCounter++;
		pthread_mutex_unlock(&DBLMgmt.mutexLock);
		return SUCCESS;
	}
	
	//not found, mac address have not opened, we will create a new dbl device
	if (freeSlot == -1)
	{
		TraceLog(ERROR_LEVEL, "PM has reached maximum number of dbl devices (allowed only %d opened devices)\n", MAX_DBL_INSTANCE);
		pthread_mutex_unlock(&DBLMgmt.mutexLock);
		return ERROR;
	}
	
	//On PM, we do not get timestamp, so only use Thread Safe
	ret = dbl_open_if(NIC, DBL_OPEN_THREADSAFE, &DBLMgmt.dblInstance[freeSlot].dev);
	if (ret != 0)
	{
		TraceLog(ERROR_LEVEL, "dbl_open_if() failed (%d)\n", ret);
		pthread_mutex_unlock(&DBLMgmt.mutexLock);
		return ERROR;
	}
	
	*dev = DBLMgmt.dblInstance[freeSlot].dev;
	
	strcpy(DBLMgmt.dblInstance[freeSlot].macAddress, macAddr);
	DBLMgmt.dblInstance[freeSlot].instanceCounter++;
	
	pthread_t receiveThreadID;
	pthread_create (&receiveThreadID, NULL, (void *) &ThreadReceiveBookData, (void *) &DBLMgmt.dblInstance[freeSlot].dev);
	pthread_mutex_unlock(&DBLMgmt.mutexLock);
	
	return SUCCESS;
}

/*************************************************
- Function Name: 	JoinMulticastGroupUsingExistingDevice
- Input:			existing device, multicast ip, multicast port
- Output:			new channel for multicast group
- Return:        	ERROR or SUCCESS
- Description:
- Usage:
*************************************************/
int JoinMulticastGroupUsingExistingDevice(dbl_device_t dev, char *ip, int port, dbl_channel_t *channel)
{
	//bind
	int ret = dbl_bind(dev, 0, port, NULL, channel);
	if (ret != 0)
	{
		TraceLog(ERROR_LEVEL, "dbl_bind() failed (%d)\n", ret);
		return ERROR;
	}

	//join multicast group
	struct in_addr addr;
	inet_aton(ip, &addr);
	ret = dbl_mcast_join(*channel, &addr, NULL);
	if (ret != 0)
	{
		TraceLog(ERROR_LEVEL, "dbl_mcast_join() failed (%d)\n", ret);
		dbl_unbind (*channel);
		*channel = NULL;
		return ERROR;
	}

	return SUCCESS;
}

/*************************************************
- Function Name: 	JoinMulticastGroupUsingExistingDevice
- Input:			existing device, multicast ip, multicast port
- Output:			new channel for multicast group
- Return:        	ERROR or SUCCESS
- Description:   
- Usage:	 
*************************************************/
int JoinMulticastGroupUsingExistingDeviceV2(dbl_device_t dev, char *ip, int port, dbl_channel_t *channel, char *interfaceName, void *context)
{
	char ip_addr[32];
	if (GetIPAddress(interfaceName, ip_addr) == ERROR)
	{
		TraceLog(ERROR_LEVEL, "Could not get IP Address for interface named '%s'\n", interfaceName);
		return ERROR;
	}
	
	pthread_mutex_lock(&DBLMgmt.mutexLock);
	struct in_addr nicAddr;
	inet_aton(ip_addr, &nicAddr);
	
	int flags = DBL_BIND_REUSEADDR;		//DBL_BIND_REUSEADDR, DBL_BIND_DUP_TO_KERNEL, DBL_BIND_NO_UNICAST, DBL_BIND_BROADCAST
	int ret = dbl_bind_addr(dev, &nicAddr, flags, port, context, channel);
	if (ret != 0)
	{
		TraceLog(ERROR_LEVEL, "dbl_bind_addr() failed (%d)\n", ret);
		PrintErrStr(ERROR_LEVEL, "dbl_bind_addr(): ", errno);
		pthread_mutex_unlock(&DBLMgmt.mutexLock);
		return ERROR;
	}
	
	struct in_addr addr;
	inet_aton(ip, &addr);
	ret = dbl_mcast_join(*channel, &addr, NULL);
	if (ret != 0)
	{
		TraceLog(ERROR_LEVEL, "dbl_mcast_join() failed (%d)\n", ret);
		PrintErrStr(ERROR_LEVEL, "dbl_mcast_join(): ", errno);
		dbl_unbind (*channel);
		*channel = NULL;
		pthread_mutex_unlock(&DBLMgmt.mutexLock);
		return ERROR;
	}
	
	pthread_mutex_unlock(&DBLMgmt.mutexLock);
	return SUCCESS;
}

/*************************************************
- Function Name: 	LeaveDBLMulticastGroup
- Input:			ip address of multicast channel to leave
- Output:			N/A
- Return:        	ERROR or SUCCESS
- Description:
- Usage:
*************************************************/
int LeaveDBLMulticastGroup(dbl_channel_t ch, const char *ip)
{
	if (ch == NULL) return SUCCESS;

	struct in_addr addr;
	inet_aton(ip, &addr);
	int ret = dbl_mcast_leave(ch, &addr);
	if (ret != 0)
	{
		TraceLog(ERROR_LEVEL, "dbl_mcast_leave() failed (%d)\n", ret);
		return ret;
	}

	return SUCCESS;
}

/****************************************************************************
- Function name:	IsFileTimestampToday
- Input:			full file path
- Output:			
- Return:			-1: could not get current time
					 0: not same date
					 1: same date
					 2: file does not exist
- Description:		Check file modification time to see if it's of the same 
					date of current date or not
- Usage:		
****************************************************************************/
int IsFileTimestampToday(char *fullPath)
{
	//Check if file already existed
	if (access(fullPath, F_OK) != 0)
	{
		return 2;
	}
	
	struct stat fileInfo;
	stat(fullPath, &fileInfo);
	
	// Check modification time fileInfo.st_mtime vs current date
	// Convert to local time
	struct tm fileTime;
	time_t seconds = fileInfo.st_mtime - (time_t)hbitime_get_offset_seconds();
	localtime_r(&seconds, &fileTime);
	
	//Get current date
	time_t currentTime = hbitime_seconds();
	
	struct tm local;
	// Convert to local time
	localtime_r(&currentTime, &local);
	
	if ((local.tm_year != fileTime.tm_year) ||
		(local.tm_mday != fileTime.tm_mday) ||
		(local.tm_mon != fileTime.tm_mon))
	{
		return 0;
	}
	
	return 1;
}

/****************************************************************************
- Function name:	LrcPrintfLogFormat
- Input:
- Output:			N/A
- Return:			N/A
- Description:
- Usage:			N/A
****************************************************************************/
void LrcPrintfLogFormat(int level, char *fmt, ...)
{
	char formatedStr[5120] = "\0";
	switch (level)
	{
		case DEBUG_LEVEL:
			strcpy(formatedStr, DEBUG_STR);
			break;
		case NOTE_LEVEL:
			strcpy(formatedStr, NOTE_STR);
			break;
		case WARN_LEVEL:
			strcpy(formatedStr, WARN_STR);
			break;
		case ERROR_LEVEL:
			strcpy(formatedStr, ERROR_STR);
			break;
		case FATAL_LEVEL:
			strcpy(formatedStr, FATAL_STR);
			break;
		case USAGE_LEVEL:
			strcpy(formatedStr, USAGE_STR);
			break;
		case STATS_LEVEL:
			strcpy(formatedStr, STATS_STR);
			break;
		default:
			formatedStr[0] = 0;
			break;
	}

	strcat(formatedStr, fmt);
	
	va_list ap;
	va_start (ap, fmt);
	
	// vprintf (fmt, ap);
	vprintf (formatedStr, ap);

	va_end (ap);

	fflush(stdout);
}

/****************************************************************************
- Function name:	PrintErrStr
- Input:
- Output:
- Return:
- Description:		Print on stdout the following:
					level description, then the input string _str, then the
					string description of errno _errno, and a new line character
					After that, flush stdout
- Usage:			N/A
****************************************************************************/
void PrintErrStr(int _level, const char *_str, int _errno)
{
	char errStr[128];
	errStr[0] = 0;

#if ((_POSIX_C_SOURCE >= 200112L || _XOPEN_SOURCE >= 600) && !defined (_GNU_SOURCE))
	strerror_r(_errno, errStr, 128);
	switch (_level)
	{
		case DEBUG_LEVEL:
			printf("%s%s%s\n", DEBUG_STR, _str, errStr);
			break;
		case NOTE_LEVEL:
			printf("%s%s%s\n", NOTE_STR, _str, errStr);
			break;
		case WARN_LEVEL:
			printf("%s%s%s\n", WARN_STR, _str, errStr);
			break;
		case ERROR_LEVEL:
			printf("%s%s%s\n", ERROR_STR, _str, errStr);
			break;
		case FATAL_LEVEL:
			printf("%s%s%s\n", FATAL_STR, _str, errStr);
			break;
		case USAGE_LEVEL:
			printf("%s%s%s\n", USAGE_STR, _str, errStr);
			break;
		case STATS_LEVEL:
			printf("%s%s%s\n", STATS_STR, _str, errStr);
			break;
		default:
			printf("%s%s\n", _str, errStr);
			break;
	}
#else
	char *_errString;
	_errString = strerror_r(_errno, errStr, 128);
	switch (_level)
	{
		case DEBUG_LEVEL:
			printf("%s%s%s\n", DEBUG_STR, _str, _errString);
			break;
		case NOTE_LEVEL:
			printf("%s%s%s\n", NOTE_STR, _str, _errString);
			break;
		case WARN_LEVEL:
			printf("%s%s%s\n", WARN_STR, _str, _errString);
			break;
		case ERROR_LEVEL:
			printf("%s%s%s\n", ERROR_STR, _str, _errString);
			break;
		case FATAL_LEVEL:
			printf("%s%s%s\n", FATAL_STR, _str, _errString);
			break;
		case USAGE_LEVEL:
			printf("%s%s%s\n", USAGE_STR, _str, _errString);
			break;
		case STATS_LEVEL:
			printf("%s%s%s\n", STATS_STR, _str, _errString);
			break;
		default:
			printf("%s%s\n", _str, _errString);
			break;
	}
#endif
	fflush(stdout);
}

inline unsigned short __bswap16(unsigned short x)
{
	asm("xchgb %b0, %h0" : "=Q" (x) : "0" (x));
	return x;
}
