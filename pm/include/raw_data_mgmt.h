/*****************************************************************************
**	Project:		Equity Arbitrage Application
**	Filename:		raw_data_mgmt.h
**	Description:	This file contains some declaration for raw_data_mgmt.c	
**	Author:			Luan Vo-Kinh
**	First created:	13-Sep-2007
**	Last updated:	-----------
*****************************************************************************/

#ifndef __RAW_DATA_MGMT_H__
#define __RAW_DATA_MGMT_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include "global_definition.h"
#include "utility.h"

/****************************************************************************
** Data structures definitions
****************************************************************************/

/****************************************************************************
** Global variables definition
****************************************************************************/
extern t_RawDataMgmt pmRawDataMgmt[MAX_ORDER_CONNECTIONS];
extern int CrossIDCollection[MAX_STOCK_SYMBOL];
/****************************************************************************
** Function declarations
****************************************************************************/
int AddDataBlockToCollection(t_DataBlock *dataBlock, t_RawDataMgmt *orderRawDataMgmt);
int BuildPMRawDataFromFile( t_ASMessage *message, t_RawDataMgmt *rawDataMgmt );
int InitializeRawDataManegementStructures(void);
int LoadRawDataEntries(void);
int LoadRawDataEntriesFromFile(const char *fileName, void *orderRawDataMgmt);
int ProcessRawDataEntries(void);
int ProcessRawDataEntriesFromCollecion(t_RawDataMgmt *orderRawDataMgmt);
int ProcessRawDataEntriesFromFile(t_RawDataMgmt *orderRawDataMgmt);
int SaveDataBlockToFile(t_RawDataMgmt *orderRawDataMgmt);

/***************************************************************************/

#endif
