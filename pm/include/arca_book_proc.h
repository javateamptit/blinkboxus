/****************************************************************************
** Project name:	Equity Arbitrage Application
** File name: 		arca_book_proc.h
** Description: 	This file contains some declarations for arca_book_proc.c

** Author: 		Danh Thai - Hoang
** First created on 29 September 2007
** Last updated on 01 October 2007
****************************************************************************/
#ifndef _ARCA_BOOK_PROC_H_
#define _ARCA_BOOK_PROC_H_

#include "configuration.h"
//#include "arca_fast_multicast_decode.h"

//Product ID used for sending message to TCP (heartbeat response, retrans request, symbol mapping request)
#define ARCA_PRODUCT_ID 157

//ARCA XDP Packet ID, one packet may contain 0 or more messages
#define ARCA_HEARTBEAT_PACKET 1
#define ARCA_SEQ_RESET_PACKET 12
#define ARCA_ORIGINAL_PACKET 11
#define ARCA_MESSAGE_UNAVAILABLE_PACKET 21
#define ARCA_END_OF_REFRESH_PACKET 20

//Arca Integrated XDP Control Msgs
#define ARCA_VENDOR_MAPPING_MSG 4
#define ARCA_SYMBOL_CLEAR_MSG 32
#define ARCA_TRADING_SESSION_CHANGE_MSG 33
#define ARCA_SECURITY_STATUS_MSG 34
#define ARCA_MESSAGE_UNAVAILABLE_MSG 31

//This msg may exists in any type of packet???
#define ARCA_SOURCE_TIME_REF_MSG 2
#define ARCA_SYMBOL_INDEX_MAPPING_MSG 3

//Arca Integrated XDP Book Msgs
#define ARCA_ADD_MSG_BODY 100
#define ARCA_MOD_MSG_BODY 101
#define ARCA_DEL_MSG_BODY 102
#define ARCA_EXEC_MSG_BODY 103
#define ARCA_IMB_MSG_BODY 105

//ARCA Integrated XDP Trade Msgs (ignored all)
#define ARCA_TRADE_MSG 220
#define ARCA_TRADE_CANCELED_MSG 221
#define ARCA_TRADE_CORRECTION_MSG 222

//ARCA Integrated XDP other msg not related to Book data (ignored all)
#define ARCA_STOCK_SUMMARY_MSG 223
#define ARCA_PBBO_MSG 104
#define ARCA_ADD_REFRESH_MSG 106

//Msgs sent to TCP Retrans Request
#define ARCA_HEARTBEAT_RESPONSE_MSG 12
#define ARCA_RETRANS_REQUEST_MSG 10

//Msgs received from TCP Retrans Request
#define ARCA_REQUEST_RESPONSE_MSG 11

#define AB_IGNORE -2
#define MAX_ARCA_STOCK_SYMBOL 80000


/* ARCA XDP Integrated Message Structure Definition*/
typedef struct gsxBook_Header
{
	unsigned short msgSize;
	unsigned short msgType;
}gsxBook_Header;

typedef struct gsxBook_Add_Message_Body
{
	unsigned short msgSize;
	unsigned short msgType;
	unsigned int symbolIndex;
	//unsigned int sourceSeqNum;
	//unsigned int sourceTime;
	unsigned int orderID;
	unsigned int volume;
	unsigned int priceNumerator;
	unsigned char priceScaleCode;
	unsigned char side;
	//unsigned char GTCIndicator;
	unsigned char tradeSession;
	unsigned char _padding;
}gsxBook_Add_Message_Body;

typedef struct gsxBook_Modify_Message_Body
{
	unsigned short msgSize;
	unsigned short msgType;
	unsigned int symbolIndex;
	//unsigned int sourceSeqNum;
	//unsigned int sourceTime;
	unsigned int orderID;
	unsigned int volume;
	unsigned int priceNumerator;
	unsigned char priceScaleCode;
	unsigned char side;
	unsigned char _padding[2];
	//unsigned char GTCIndicator;
	//unsigned char reasonCode;
}gsxBook_Modify_Message_Body;

typedef struct gsxBook_Delete_Message_Body
{
	unsigned short msgSize;
	unsigned short msgType;
	unsigned int symbolIndex;
	//unsigned int sourceSeqNum;
	//unsigned int sourceTime;
	unsigned int orderID;
	unsigned char side;
	unsigned char _padding[3];
	//unsigned char GTCIndicator;
	//unsigned char reasonCode;
}gsxBook_Delete_Message_Body;

typedef struct gsxBook_Execute_Message_Body
{
	unsigned short msgSize;
	unsigned short msgType;
	unsigned int symbolIndex;
	//unsigned int sourceSeqNum;
	//unsigned int sourceTime;
	unsigned int orderID;
	unsigned int volume;
	//unsigned int priceNumerator;
	//unsigned char priceScaleCode;
	//unsigned char GTCIndicator;
	unsigned char reasonCode;
	//unsigned int tradeId;
}gsxBook_Execute_Message_Body;

typedef struct gsxBook_Symbol_Clear_Message_Body
{
	unsigned short msgSize;
	unsigned short msgType;
	unsigned int symbolIndex;
}gsxBook_Symbol_Clear_Message_Body;

typedef struct gsxBook_Trading_Session_Change_Message_Body
{
	unsigned short msgSize;
	unsigned short msgType;
	unsigned int symbolIndex;
	unsigned char tradeSession;
	unsigned char _padding[3];
}gsxBook_Trading_Session_Change_Message_Body;

typedef struct gsxBook_Symbol_Mapping_Message_Body
{
	unsigned short msgSize;
	unsigned short msgType;
	unsigned int symbolIndex;
	unsigned char symbol[12];
	unsigned char priceScaleCode;
	unsigned char _padding[3];
}gsxBook_Symbol_Mapping_Message_Body;

union book_message_union_binary
{
	struct gsxBook_Header mHeader;
	struct gsxBook_Add_Message_Body mAdd;
	struct gsxBook_Modify_Message_Body mModify;
	struct gsxBook_Delete_Message_Body mDelete;
	struct gsxBook_Execute_Message_Body mExecute;
	struct gsxBook_Symbol_Clear_Message_Body mSymbolClear;
	struct gsxBook_Symbol_Mapping_Message_Body mSymbolMapping;
	struct gsxBook_Trading_Session_Change_Message_Body mTradingSessionChange;
}book_message_union_binary;

/* End of ARCA XDP Integrated Message Structure Definition*/

typedef struct t_Symbol_Index_Mapping
{
	char symbol[SYMBOL_LEN]; //Currently 8 bytes
	
	unsigned char priceScaleCode;
	char _padding1;
	char _padding2;
	char _padding3;
	
	int stockSymbolIndex;
}t_Symbol_Index_Mapping;

extern t_ARCA_Book_Conf ARCA_Book_Conf;
extern t_Symbol_Index_Mapping symbolIndexMappingTable[MAX_ARCA_STOCK_SYMBOL];
extern int StockSymbolIndex_to_ArcaSymbolIndex_Mapping[MAX_STOCK_SYMBOL];
extern int NoArcaGapRequest;
/*****************************************************************************************************/

void *ARCA_Book_Thread(void *agrs);
int BuildAndSendHeartbeatResponse();
int CheckSymbolIndex(int symbolIndex, int *stockSymbolIndex);
int DecodeAndUpdateSymbolDataStructure(unsigned char *buffer);
int ReleaseARCABookResouce(void);
int DisconnectARCABook(void);
int InitARCA_BOOK_DataStructure(void);
int32_t ParseBookMsg(union book_message_union_binary *dstMsg, unsigned char *tempBuffer);

//int ProcessAllIgnoredMessages(char *inputBuffer, t_ARCA_Book_GroupInfo *workingGroup);
int ProcessBookMessage(unsigned char *inputBuffer, t_ARCA_Book_GroupInfo *workingGroup);
int ProcessDecodedAddMessage(t_ARCA_Book_GroupInfo *workingGroup, struct gsxBook_Add_Message_Body *msg, char isCheckCross);
int ProcessDecodedDeleteMessage(t_ARCA_Book_GroupInfo *workingGroup, struct gsxBook_Delete_Message_Body *msg);
int ProcessDecodedModifyMessage(t_ARCA_Book_GroupInfo *workingGroup, struct gsxBook_Modify_Message_Body *msg);
int ProcessDecodedExecuteMessage(t_ARCA_Book_GroupInfo *workingGroup, struct gsxBook_Execute_Message_Body *msg);
int ProcessDecodedSymbolClearMessage(t_ARCA_Book_GroupInfo *workingGroup, struct gsxBook_Symbol_Clear_Message_Body *msg);
int ProcessTradingSessionChangeMessage(t_ARCA_Book_GroupInfo *workingGroup, struct gsxBook_Trading_Session_Change_Message_Body *msg);
int ProcessSymbolIndexMappingMessage(t_ARCA_Book_GroupInfo *workingGroup, struct gsxBook_Symbol_Mapping_Message_Body *msg);

int ProcessResetSeqNumMessage(unsigned char *buffer, t_ARCA_Book_GroupInfo *workingGroup);
int ProcessRetransmissionMessage(char *retransmissionMsg, t_ARCA_Book_GroupInfo *workingGroup);
int RecoveryProcessing(int beginSeqNum, int endSeqNum, t_ARCA_Book_GroupInfo *workingGroup);
int SendRetransmissionRequest(int beginSeqNum, int endSeqNum, t_ARCA_Book_GroupInfo *workingGroup);

void *TalkToTCPServer(void *args);
void *ThreadProcessData(void *args);
void *ThreadReceiveData(void *args);
int LoadAllSymbolMappings(char *destFile);
#endif
