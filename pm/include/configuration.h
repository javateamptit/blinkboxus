/*****************************************************************************
**	Project:		Equity Arbitrage Application
**	Filename:		configuration.h
**	Description:	This file contains some declaration for configuration.c	
**	Author:			Luan Vo-Kinh
**	First created:	13-Sep-2007
**	Last updated:	-----------
*****************************************************************************/

#ifndef __CONFIGURATION_H__
#define __CONFIGURATION_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include <stdio.h>
#include <string.h>

#include <semaphore.h>

#include "global_definition.h"
#include "utility.h"
//#include "arca_fast_multicast_decode.h"

#define FILE_NAME_OF_AS_CONF "as_config"
#define FILE_NAME_OF_PM_GLOBAL_CONF "global_config"
#define FILE_NAME_OF_IGNORE_STOCK_LIST "ignore_stocklist"

//For Exchange Configuration
#define FILE_NAME_OF_EXCHANGES_CONF "exchange_list"

#define MAX_ARCA_MSG_IN_BUFFER 1000

#define MAX_ARCA_RETRAN_MSG_IN_BUFFER 1000

#define MAX_ARCA_GROUPS 4

#define READ 0
#define STORED 1

#define NDAQ_ADD_MSG 'A'
#define NDAQ_ADD_MPID_MSG 'F'
#define NDAQ_EXECUTED_MSG 'E'
#define NDAQ_EXECUTED_PRICE_MSG 'C'
#define NDAQ_CANCEL_MSG 'X'
#define NDAQ_DELETE_MSG 'D'
#define NDAQ_REPLACE_MSG 'U'
#define NDAQ_TRADE_MSG 'P'
#define NDAQ_BROKEN_TRADE_MSG 'B'
#define NDAQ_SYSTEM_EVENT_MSG 'S'

// For CTS
#define MAX_MULTICAST_LINES 26
#define MAX_CTS_HEADER_LEN 24
#define MAX_CTS_MSG_LEN 90

// For UTDF
#define MAX_UTDF_CHANNELS 6
#define MAX_UTDF_HEADER_LEN 24
#define MAX_UTDF_MSG_LEN 128

// For UQDF
#define MAX_UQDF_CHANNELS 6
#define MAX_UQDF_HEADER_LEN 24
#define MAX_UQDF_MSG_LEN 128

// For CQS
#define MAX_CQS_MULTICAST_LINES 24
#define MAX_CQS_HEADER_LEN 24
#define MAX_CQS_MSG_LEN 512

// For ...
#define SOH_CHAR 1
#define ETX_CHAR 3
#define US_CHAR 31

#define USED 10
#define NOT_USE 0

#define MAX_DBL_INSTANCE 5
#define MAX_MTU 1500
#define MAX_BOOK_MSG_IN_BUFFER  40000
/****************************************************************************
** Data structures definitions
****************************************************************************/
typedef struct t_DBLInstance
{
	dbl_device_t dev;
	char macAddress[MAX_LINE_LEN];
	int instanceCounter;
	int isTimedOut;
}t_DBLInstance;

typedef struct t_DBLMgmt
{
	t_DBLInstance dblInstance[MAX_DBL_INSTANCE];
	
	pthread_mutex_t mutexLock;
}t_DBLMgmt;

typedef struct t_DataInfo
{
	char buffer[MAX_BOOK_MSG_IN_BUFFER][MAX_MTU];
	long timestamp[MAX_BOOK_MSG_IN_BUFFER];	//timestamp (in micro second) when NIC has received packet
	int currentReceivedIndex;
	int currentReadIndex;
	char bookName[31];
	char connectionFlag;
	int (*DisconnectBook)(void);
	sem_t sem;
} t_DataInfo;

typedef struct t_MonitorArcaBook
{
	int lastReceivedIndex;
	int lastReadIndex;
	int recvCounter;	//timeout counter of last received data index
	int procCounter;	//timeout counter of last read data index
}t_MonitorArcaBook;

typedef struct t_MonitorNasdaqBook
{
	int lastReceivedIndex;
	int lastReadIndex;
	int recvCounter;	//timeout counter of last received data index
	int procCounter;	//timeout counter of last read data index
}t_MonitorNasdaqBook;

typedef struct t_MonitorCQS
{
	int lastReceivedIndex;
	int lastReadIndex;
	int recvCounter;	//timeout counter of last received data index
	int procCounter;	//timeout counter of last read data index
}t_MonitorCQS;

typedef struct t_MonitorUQDF
{
	int lastReceivedIndex;
	int lastReadIndex;
	int recvCounter;	//timeout counter of last received data index
	int procCounter;	//timeout counter of last read data index
}t_MonitorUQDF;

typedef struct t_MonitorUTDF
{
	int lastReceivedIndex;
	int lastReadIndex;
	int recvCounter;	//timeout counter of last received data index
	int procCounter;	//timeout counter of last read data index
}t_MonitorUTDF;

typedef struct t_MonitorCTS
{
	int lastReceivedIndex;
	int lastReadIndex;
	int recvCounter;	//timeout counter of last received data index
	int procCounter;	//timeout counter of last read data index
}t_MonitorCTS;

typedef struct t_DBLConnectionInfo
{
    char ip[MAX_LINE_LEN];
	char NICName[MAX_LINE_LEN];
    int port;

	dbl_channel_t channel;
	dbl_device_t dev;
	dbl_send_t sendHandle;
} t_DBLConnectionInfo;

typedef struct t_NDAQ_MoldUDP_Book
{
	t_DBLConnectionInfo Data;
	t_DBLConnectionInfo Recovery;

	t_DataInfo dataInfo;

	int lastSequenceNumber;
	int currentDetectedIndex;
    char userName[MAX_LINE_LEN];
	char password[MAX_LINE_LEN];
	
} t_NDAQ_MoldUDP_Book;


//Arca XDP Book config
typedef struct t_ARCA_Book_GroupInfo
{	
	// Primary data feed multicast group
	t_DataInfo dataInfo;
	
	pthread_t primaryProcessThreadID;

	// For book mgmt synchronization
	int groupIndex;
	int isGotResponeFromTCP;

	dbl_device_t 	retranDev;

	char dataIP[MAX_LINE_LEN];
	char retranIP[MAX_LINE_LEN];
	char refreshIP[MAX_LINE_LEN];

	int  dataPort;
	int  retranPort;
	int  refreshPort;
	
	// For Primary data feed processing
	int lastSequenceNumber;

	dbl_channel_t 	dataChannel;
	dbl_channel_t 	retranChannel;
	dbl_channel_t	refreshChannel;
} t_ARCA_Book_GroupInfo;

typedef struct t_ARCA_Book_Conf
{
	char NICName[MAX_LINE_LEN];						// NIC Name used for Data Multicast Groups + Retransmission Multicast Groups

	char RetransmissionRequestIP[MAX_LINE_LEN];		// Retransmission Request IP
	char RetransmissionRequestSource[MAX_LINE_LEN];	// Retransmission Request username
	
	int RetransmissionRequestPort;					// Retransmission Request Port
	int retranRequestSocket;						// (BSD TCP Socket)
	
	char currentFeedName;							// ARCA feed config is being used 

	t_ARCA_Book_GroupInfo group[MAX_ARCA_GROUPS];
	pthread_mutex_t retranMutex;
	pthread_t retranRequestID;

	dbl_device_t 	dataDev;

}t_ARCA_Book_Conf;

/*
The structure is used to manage 
status/behavior of a connection to ECN Order
*/
typedef struct t_OrderStatus
{
	/*
	The field is used to store status of a connection that was made to 
	an ECN Order successfully (CONNECTED);;; or disconnect to (DISCONNECTED);;;
	(Default value = DISCONNECTED);;;
	*/
	char isOrderConnected; 
	
	/*
	The field is used to store request from AS: YES or NO
	YES: Trade Server should makes a connection to an ECN Order Server
	NO: Trade Server should disconnect connection to an ECN Order Server
	(Default value = YES);;;
	*/
	char shouldConnect;
} t_OrderStatus;

/*
The structure is used to manage 
status/behavior of a connection to ECN Book
*/
typedef struct t_BookStatus
{
	/*
	The field is used to store status of a connection that was made to 
	an ECN Book successfully (CONNECTED) or disconnect to (DISCONNECTED)
	(Default value = DISCONNECTED)
	*/
	char isConnected; 
	
	/*
	The field is used to store request from AS: YES or NO
	YES: Trade Server should makes a connection to an ECN Book Server
	NO: Trade Server should disconnect connection to an ECN Book Server
	(Default value = YES)
	*/
	char shouldConnect;
	
	/*
	If isFlushBook == YES then we should stop all related threads, close socket,
	and flush stock book of all stock symbols. After that, it must be turned OFF (value = NO)
	*/
	char isFlushBook;
	
} t_BookStatus;

typedef struct t_CTS_Msg
{	
	int msgSeqNum;
	char msgHeader[MAX_CTS_HEADER_LEN + 1];
	char msgBody[MAX_CTS_MSG_LEN];
	char msgCategory;
	char msgType;
} t_CTS_Msg;

typedef struct t_CTS_Feed_Group
{
	// Primary data feed multicast group (Group A)
	char dataIP[MAX_LINE_LEN];
	int  dataPort;
	dbl_channel_t 	dataChannel;

	t_DataInfo dataInfo;

	// Thread variables
	pthread_t primaryProcessThreadID;

	int primaryLastSequenceNumber;
	int index;

} t_CTS_Feed_Group;

typedef struct t_CTS_Feed_Conf
{
	// Primary data feed multicast group (Group A)
	t_CTS_Feed_Group group[MAX_MULTICAST_LINES];

	//Shared
	char NICName[MAX_LINE_LEN];
	dbl_device_t dev;

} t_CTS_Feed_Conf;

typedef struct t_CQS_Msg
{	
	int msgSeqNum;
	char msgHeader[MAX_CQS_HEADER_LEN + 1];
	char msgBody[MAX_CQS_MSG_LEN];
	char msgCategory;
	char msgType;
} t_CQS_Msg;

typedef struct t_CQS_Feed_Group
{
	// Primary data feed multicast group (Group A)

	char dataIP[MAX_LINE_LEN];
	int  dataPort;
	dbl_channel_t 	dataChannel;

	t_DataInfo dataInfo;

	// Thread variables
	pthread_t primaryProcessThreadID;

	int primaryLastSequenceNumber;
	int index;
} t_CQS_Feed_Group;

typedef struct t_CQS_Feed_Conf
{	//Please be careful with memory alignment! it will slow down speed of application alot!

	//specific
	t_CQS_Feed_Group group[MAX_CQS_MULTICAST_LINES];

	//Shared
	char NICName[MAX_LINE_LEN];
	dbl_device_t dev;
}t_CQS_Feed_Conf;


//For UQDF Message
typedef struct t_UQDF_Msg
{
	int msgSeqNum;
	char msgHeader[MAX_UQDF_HEADER_LEN + 1];
	char msgBody[MAX_UQDF_MSG_LEN];
	char msgCategory;
	char msgType;
} t_UQDF_Msg;

//For UQDF Configuration
typedef struct t_UQDF_Group
{
	// Primary data feed multicast group (Group A)
	char dataIP[MAX_LINE_LEN];
	int  dataPort;
	dbl_channel_t 	dataChannel;

	t_DataInfo dataInfo;
	char primaryFileName[MAX_LINE_LEN];
	FILE *primaryFileDesc;

	// Thread variables
	pthread_t primaryProcessThreadID;

	int primaryLastSequenceNumber;
	int index;
} t_UQDF_Group;

typedef struct t_UQDF_Conf
{
	// Primary data feed multicast group (Group A)
	t_UQDF_Group group[MAX_UQDF_CHANNELS];

	//Shared
	char NICName[MAX_LINE_LEN];
	dbl_device_t dev;

} t_UQDF_Conf;

typedef struct t_UTDF_Msg
{	
	int msgSeqNum;
	char msgHeader[MAX_UTDF_HEADER_LEN + 1];
	char msgBody[MAX_UTDF_MSG_LEN];
	char msgCategory;
	char msgType;
} t_UTDF_Msg;

typedef struct t_UTDF_Group
{
	// Primary data feed multicast group (Group A)
	char dataIP[MAX_LINE_LEN];
	int  dataPort;
	dbl_channel_t 	dataChannel;

	t_DataInfo dataInfo;
	
	// Thread variables
	pthread_t primaryProcessThreadID;
	
	int primaryLastSequenceNumber;
	int index;
} t_UTDF_Group;

typedef struct t_UTDF_Conf
{
	// Primary data feed multicast group (Group A)
	t_UTDF_Group group[MAX_UTDF_CHANNELS];

	//Shared
	char NICName[MAX_LINE_LEN];
	dbl_device_t dev;
} t_UTDF_Conf;

typedef struct t_TradingAccount
{
	char ArcaDirectAccount[MAX_ACCOUNT][32];
	char RASHAccount[MAX_ACCOUNT][32];
} t_TradingAccount;

/****************************************************************************
** Global variables definition
****************************************************************************/
extern char fileNamePrefix[MAX_ORDER_CONNECTIONS][MAX_LINE_LEN];

extern t_ASCollection asCollection;

extern t_IgnoreStockList IgnoreStockList;

extern char symbolIgnoreStatus[MAX_STOCK_SYMBOL];

extern t_OrderStatus orderStatusMgmt[MAX_ORDER_CONNECTIONS];

extern int clientOrderID;

extern pthread_mutex_t nextOrderID_Mutex;

extern t_BookStatus bookStatusMgmt[MAX_BOOK_CONNECTIONS];

extern t_BookStatus CTS_StatusMgmt;
extern t_CTS_Feed_Conf CTS_Feed_Conf;

extern t_BookStatus CQS_StatusMgmt;
extern t_CQS_Feed_Conf CQS_Feed_Conf;

//For UQDF
extern t_BookStatus UQDF_StatusMgmt;
extern t_UQDF_Conf UQDF_Conf;

extern t_BookStatus UTDF_StatusMgmt;
extern t_UTDF_Conf UTDF_Conf;

extern int maxMilisecondToSleep;
extern int maxShareNeedSleep;

extern t_TradingAccount TradingAccount;
/****************************************************************************
** Function declarations
****************************************************************************/
int InitializeAggregationServerInfo(void);
int InitializeFileNamePrefix(void);
int InitializeIgnoreStockList();
int IsSymbolIgnored(char *symbol);
int LoadARCA_Book_Config(const char *fileName, t_ARCA_Book_GroupInfo group[MAX_ARCA_GROUPS]);
int LoadARCA_DIRECTConf(char *fileName, void *ECNOrderConf);
int LoadAggregationServerConf(void);
int LoadAllBooksConfig(void);
int LoadCQS_Config(const char *fileName);
int LoadCTS_Config(const char *fileName);
int LoadClientOrderID(const char *fileName);
int LoadIgnoreStockList(void);
int LoadIncomingAndOutgoingSeqNum(const char *fullPath, int type, void *configInfo);
int LoadIncomingSeqNum(const char *fullPath, int type, void *configInfo);
int LoadNASDAQ_MoldUDP_Config(const char *fileName);
int LoadNASDAQ_RASHConf(char *fileName, void *ECNOrderConf);
int LoadPMGlobalConf(void);
int LoadPMOrdersConf(void);
int LoadSequenceNumber(const char *fileName, int type, void *configInfo);
int LoadUTDF_Config(const char *fileName);
int ProcessSavePMOrderConf(int ECNIndex);
int SaveARCA_DIRECTConf(char *fileName, void *ECNOrderConf);
int SaveASConf();
int SaveClientOrderID(const char *fileName, int clientOrderID);
int SaveIgnoreStockList(void);
int SaveIncomingAndOutgoingSeqNumToFile(const char *fileName, int type, void *configInfo);
int SaveIncomingSeqNumToFile(const char *fileName, int type, void *configInfo);
int SaveNASDAQ_RASHConf(char *fileName, void *ECNOrderConf);
int SavePMGlobalConf(void);
int SaveSeqNumToFile(const char *fileName, int type, void *configInfo);
int UpdateStatusSymbolIgnored(char symbol[SYMBOL_LEN], char status);
int UpdateStatusSymbolIgnoredList(void);
int LoadTradingAccountFromFile(const char *fileName, void *_tradingAccount);

// For UQDF
int LoadUQDF_Config(const char *fileName);

// For Exchange Configuration
int LoadExchangesConf(void);
int SaveExchangesConf(void);

t_DBLInstance* GetDBLInstance(dbl_device_t dev);
int IsDBLTimedOut(dbl_device_t dev);
/***************************************************************************/

#endif
