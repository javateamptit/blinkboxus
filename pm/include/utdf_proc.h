/****************************************************************************
** Project name:	Equity Arbitrage Application
** File name: 		utdf_proc.h
** Description: 	This file contains some declarations for utdf_proc.c
** Author: 			Luan Vo-Kinh
** First created on May 02, 2008
** Last updated on May 02, 2008
****************************************************************************/

#ifndef _UTDF_PROC_H_
#define _UTDF_PROC_H_

#include "configuration.h"
#include "order_mgmt_proc.h"

/*****************************************************************************************************/
int ReleaseUTDF_MulticastResource(void);
int DisconnectUTDF_Multicast(void);
int InitializeUTDF_DenominatorValue(void);
int ProcessUTDF_LongTradeMsg(t_UTDF_Msg *msg);
int ProcessUTDF_MsgFromPrimaryData(void *args, char buffer[MAX_MTU]);
int ProcessUTDF_ShortTradeMsg(t_UTDF_Msg *msg);
int ProcessUTDF_TradeCorrectionMsg(t_UTDF_Msg *msg);
void *Start_UTDF_Thread(void *agrs);
void *ThreadProcessUTDF_PrimaryData(void *args);
void *ThreadReceiveUTDF_PrimaryData(void *args);
void InitUTDF_DataStructure(void);
#endif
