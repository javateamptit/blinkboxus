/****************************************************************************
** Project name:	Equity Arbitrage Application
** File name: 		cqs_feed_proc.h
** Description: 	This file contains some declarations for cqs_feed_proc.c
** Author: 			Luan Vo-Kinh
** First created on Oct 03, 2008
** Last updated on Oct 03, 2008
****************************************************************************/

#ifndef _CQS_FEED_PROC_H_
#define _CQS_FEED_PROC_H_

#include "configuration.h"
#include "order_mgmt_proc.h"
#include "bbo_mgmt.h"

/*****************************************************************************************************/
int ReleaseCQS_FeedResource(void);

int DisconnectCQS_Feed(void);

int InitializeCQS_DenominatorValue(void);

int ProcessCQS_LongQuoteMsg(t_CQS_Msg *msg);

int ProcessCQS_MsgFromPrimaryData(void *args, char buffer[MAX_MTU]);

int ProcessCQS_ShortQuoteMsg(t_CQS_Msg *msg);

void *CQS_Feed_Thread(void *agrs);

void *ThreadProcessCQS_PrimaryData(void *args);

void *ThreadReceiveCQS_PrimaryData(void *args);

void InitCQS_Feed_DataStructure(void);

void InitCQS_Quote_DataStructure(void);

void ProcessCQS_LongNationalBBOAppendage(char *msg, t_NationalAppendage *nationalAppendage);

void ProcessCQS_ShortNationalBBOAppendage(char *msg, t_NationalAppendage *nationalAppendage);


#endif
