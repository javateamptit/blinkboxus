/****************************************************************************
** Project name:	Equity Arbitrage Application
** File name: 		nasdaq_book_proc.h
** Description: 	This file contains some declarations for nasdaq_book_proc.c

** Author: 		Danh Thai - Hoang
** First created on 29 September 2007
** Last updated on 01 October 2007
****************************************************************************/

#ifndef _NASDAQ_BOOK_PROC_H_
#define _NASDAQ_BOOK_PROC_H_

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include "configuration.h"
#include "book_mgmt.h"
#include <semaphore.h>

/****************************************************************************
** Data structure definitions
****************************************************************************/


/****************************************************************************
** Global variable declarations
****************************************************************************/
extern t_NDAQ_MoldUDP_Book NDAQ_MoldUDP_Book;
extern sem_t nasdaqBookSem;
extern int NoNasdaqGapRequest;
/****************************************************************************
** Function declarations
****************************************************************************/
int InitRecoveryDBLConnection(void);
int BuildAndSendReRequestPacket(long firstMsgSeq, int msgCount, char *session);
int ReleaseNASDAQBookResource(void);
int DisconnectNASDAQBook(void);
int ProcessAddOrderMessage(unsigned char *buffer);
int ProcessCancelOrderMessage(unsigned char *buffer);
int ProcessDeleteOrderMessage(unsigned char *buffer);
int ProcessExecutedOrderMessage(unsigned char *buffer);
int ProcessMissedPacket(long firstMsgSeq, int msgCount, char *session);
int ProcessPacketWithoutMissing(unsigned char *buffer, int currentReadIndex);
int ProcessRecoveryPacket(unsigned char *buffer);
int ProcessReplaceOrderMessage(unsigned char *buffer);
void *NDAQ_Book_Thread(void *args);
void *ThreadProcessNDAQData(void *args);
void InitNASDAQ_BOOK_DataStructure(void);


#endif
