/****************************************************************************
** Project name:	Equity Arbitrage Application
** File name:		uqdf_feed_proc.h
** Description:		This file contains some declarations for uqdf_proc.c
** Author:			ThoHN
** First created on August 27, 2010
** Last updated on August 27, 2010
****************************************************************************/

#ifndef _UQDF_FEED_PROC_H_
#define _UQDF_FEED_PROC_H_

#include "configuration.h"
#include "order_mgmt_proc.h"
#include "bbo_mgmt.h"

/*****************************************************************************************************/
int ReleaseUQDF_MulticastResource(void);
int DisconnectUQDF_Multicast(void);
int InitializeUQDF_DenominatorValue(void);
void InitUQDF_DataStructure(void);
void *UQDF_Feed_Thread(void *args);
void *ThreadReceiveUQDF_PrimaryData(void *args);
void *ThreadProcessUQDF_PrimaryData(void *args);
int ProcessUQDF_MsgFromPrimaryData(void *args, char buffer[MAX_MTU]);
int ProcessUQDF_LongQuote(t_UQDF_Msg *msg);
int ProcessUQDF_ShortQuote(t_UQDF_Msg *msg);
void ProcessUQDF_ShortNationalBBOAppendage(char *msg, t_NationalAppendage *nationalAppendage);
void ProcessUQDF_LongNationalBBOAppendage(char *msg, t_NationalAppendage *nationalAppendage);
int ProcessUQDF_BBOShort(t_UQDF_Msg *msg);
int ProcessUQDF_BBOLong(t_UQDF_Msg *msg);
void InitUQDF_Quote_DataStructure(void);
void InitUQDF_Feed_DataStructure(void);

#endif
