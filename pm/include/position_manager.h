/*****************************************************************************
**	Project:		Equity Arbitrage Application
**	Filename:		position_manager.h
**	Description:	This file contains some declaration for position_manager.c	
**	Author:			Luan Vo-Kinh
**	First created:	13-Sep-2007
**	Last updated:	-----------
*****************************************************************************/

#ifndef __POSITION_MANAGER_H__
#define __POSITION_MANAGER_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include <stdio.h>
#include <signal.h>
#include <getopt.h>

/****************************************************************************
** Data structures definitions
****************************************************************************/
typedef struct t_AutoStart
{
	int isProcess;
	int value;
} t_AutoStart;

/****************************************************************************
** Global variables definition
****************************************************************************/

/****************************************************************************
** Function declarations
****************************************************************************/
int GetOptionForAllECNBooks(const char *);
int GetOptionForAllECNOrders(const char *);
int GetOptionForAllFeeds(const char *);
int GetOptionForECNBook(int , const char *);
int GetOptionForECNOrder(int , const char *);
int GetOptionForFeed(int , const char *);
int InitializeDataConfs(void);
int InitializeDataStructures(void);
int LoadDataConfig(void);
int LoadDataStructures(void);
int Print_Help(const char *program_name);
int ProcessUserInput(int argc, char *argv[]);
int StartUpMainThreads(void);
int main (int argc, char *argv[]);
void *MonitorThread(void* args);
void *ProcessAutoStartThreads(void *arg);
void InitMonitorArcaBook(void);
void InitMonitorNasdaqBook(void);
void InitMonitorCTS(void);
void InitMonitorUTDF(void);
void InitMonitorCQS(void);
void InitMonitorUQDF(void);
void InstallCoreDump(void);
void PrintCmdHelp(void);
int SaveLoginInfo(void);
void Process_INTERRUPT_Signal(int sig_num);
int RegisterSignalHandlers(void);
void Process_SEGV_Signal(int signalno, siginfo_t *siginfo, void *context);

/***************************************************************************/

#endif
