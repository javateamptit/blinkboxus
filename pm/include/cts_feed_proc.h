/****************************************************************************
** Project name:	Equity Arbitrage Application
** File name: 		cts_feed_proc.h
** Description: 	This file contains some declarations for cts_feed_proc.c
** Author: 			Luan Vo-Kinh
** First created on May 02, 2008
** Last updated on May 02, 2008
****************************************************************************/

#ifndef _CTS_FEED_PROC_H_
#define _CTS_FEED_PROC_H_

#include "configuration.h"
#include "order_mgmt_proc.h"

/*****************************************************************************************************/
int ReleaseCTS_FeedResource(void);
int DisconnectCTS_Feed(void);
int InitializeDenominatorValue(void);
int ProcessCTS_LongTradeMsg(t_CTS_Msg *msg);
int ProcessCTS_MsgFromPrimaryData(void *args, char buffer[MAX_MTU]);
int ProcessCTS_ShortTradeMsg(t_CTS_Msg *msg);
void *CTS_Feed_Thread(void *agrs);
void *ThreadProcessCTS_PrimaryData(void *args);
void *ThreadReceiveCTS_PrimaryData(void *args);
void InitCTS_Feed_DataStructure(void);
#endif
