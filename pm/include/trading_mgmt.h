/****************************************************************************
** Project name:	Equity Arbitrage Application
** File name: 		trading_mgmt.h
** Description: 	This file contains some declarations for trading_mgmt.c

** Author: 		Sang Nguyen-Minh
** First created on 01 October 2007
** Last updated on 01 October 2007
****************************************************************************/

#ifndef __TRADING_MGMT_H__
#define __TRADING_MGMT_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include "global_definition.h"
#include "stock_symbol.h"
#include "book_mgmt.h"
#include "order_mgmt_proc.h"

// Detect quote delay in microseconds
//#define DETECT_QUOTE_DELAY 50000

// Monitoring liquidate orders placed 
#define NUM_MICROSECOND_TO_CANCEL 1000
#define MONITOR_ORDER_DELAY 100
#define MONITOR_BBO_ORDER_SLEEPING_DELAY 150000//150ms

#define CROSS_ID_INDEX 30

#define PRE_MARKET_END_HOUR 8
#define PRE_MARKET_END_MIN 30
#define INTRADAY_END_HOUR 15

/****************************************************************************
** Data structure definitions
****************************************************************************/
typedef struct t_QuoteHolding
{
	char isHold;
	char ecnBookID;
	int shares;
	long refNum;
	double price;
} t_QuoteHolding;

typedef struct t_PositionInfo
{
	int isTrading;
	int typePlacedOrder;
	int willReplace;
	int isCalculatingInformation;
	int consecutiveRejectNumber;
	int ECNIndex;
	int replaceAfterSleeping;
	int releaseStuckSleeping; // release stuck sleeping
	int startThread;
	t_Position stuck;
	
	t_TopOrder bestQuote[MAX_SIDE];
	t_QuoteHolding holdQuote[MAX_SIDE];
} t_PositionInfo;

typedef struct t_TradingStatus
{
	int *status[MAX_ORDER_CONNECTIONS];
} t_TradingStatus;

typedef struct t_Order
{
	int ecnToPlaceID;
	int crossID;
	int orderID;
	int stockSymbolIndex;
	int shareVolume;
	double sharePrice;
	int side;
	char *symbol;
	int timeInForce;
	int ecnAskBidLaunch;
} t_Order;

/*These structures are used for order 
status management at Entry and Exit
*/
typedef struct t_Ack
{
	int ecnID;
	int stockSymbolIndex;
	int clientOrderID;
	int acceptedShare;
	double acceptedPrice;
} t_Ack;

typedef struct t_Rejected
{
	int ecnID;
	int stockSymbolIndex;
	int clientOrderID;
} t_Rejected;

typedef struct t_Canceled
{
	int ecnID;
	int stockSymbolIndex;
	int clientOrderID;
	int canceledShare;
} t_Canceled;

typedef struct t_PartiallyFilled
{
	int ecnID;
	int stockSymbolIndex;
	int clientOrderID;
	int filledShare;
	double filledPrice; 
} t_PartiallyFilled;

/****************************************************************************
** Global variable declarations
****************************************************************************/
extern t_PositionInfo positionCollection[MAX_STOCK_SYMBOL];

extern pthread_mutex_t clientOrderID_Mutex;
extern t_TradingStatus tradingStatus;

extern int currentCrossID;
extern pthread_mutex_t currentCrossIDMutex;
extern pthread_mutex_t currentOrderIDMutex;
extern int currentOrderID;

/****************************************************************************
** Function declarations
****************************************************************************/
int CheckAndPlaceOrder(t_Position *stuck, int crossId, int needSleep, int replaceAfterSleeping, int indexOpenOrder);
int CheckAndPlaceOrderForFastMarket(t_Position *stuck, int needSleep, int replaceAfterSleeping, int indexOpenOrder, int forceEngage);
int CheckBBOOrder(int stockSymbolIndex, int side, int ecnBookID, long refNum, int shares, double price, int isType);
int CheckQuoteForBBOOrder(t_Position *stuck);
int AddQuoteIgnore(int symbolIndex, int orderID, int shares, double price);
int DeleteQuoteIgnore(int symbolIndex, int orderID);
int FindQuoteIgnore(int symbolIndex, int orderID);
int InitializePositionCllection(void);
int IsSleeping(int symbolIndex);
int SendBBOOrder(t_SendNewOrder *newOrder, t_Position *stuck, t_TopOrder bestQuote[MAX_SIDE], int isNeedCheckExecutionRate, int executionRate, int useFastMarketParameters, int needSleep, int replaceAfterSleeping, int indexOpenOrder, int forceEngage);
int SendLiquidateOrder(t_SendNewOrder *newOrder, t_Position *stuck, int isNeedCheckExecutionRate, int executionRate, int useFastMarketParameters, int needSleep, int replaceAfterSleeping, int indexOpenOrder, int forceEngage);
long CheckSelfCrossTrade(t_OrderDetail *newOrder, int newOrderSide, int stockSymbolIndex, int ecnLaunchID, t_TopOrder *_bestQuote);
void *Monitoring_Liquidate_Order_Thread(void *agrs);
void CheckAndPlaceOrderWhenSymbolResume (char *symbol);
void *Monitoring_BBO_Order_Sleeping_Thread(void *agrs);
int InitializeSymbolTradingMutex();
int CheckOurBBOOrderWithCurrentBestquote(double orderPrice, int symbolIndex, int side, int orderShares);
int GetNewOrder(t_SendNewOrder *newOrder, t_Position *stuck);
int ReplaceOrderAfterSleeping(t_SendNewOrder *newOrder, t_Position *stuck);
void *Monitoring_Orders_GoAway_Or_Spread_Too_Wide_Thread(void *agrs);
void *Monitoring_Sleep_And_Replace_Order_Thread(void *agrs);
int CheckHaltedECNForSymbol(int tradeSymbolIndex, int ecnIndex);
#endif

