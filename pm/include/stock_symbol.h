/****************************************************************************
** Project name:	Equity Arbitrage Application
** File name: 		stock_symbol.h
** Description: 	This file contains some declarations for stock_symbol.c

** Author: 			Sang Nguyen-Minh
** First created on 18 September 2007
** Last updated on 18 September 2007
****************************************************************************/

#ifndef __STOCK_SYMBOL_H__
#define __STOCK_SYMBOL_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include "global_definition.h"
#include "utility.h"

#define MAX_STOCK_SYMBOL 15000
#define MAX_CHARSET_MEMBER_NO 31
#define INDEX_OF_A_CHAR 60

/****************************************************************************
** Data structure definitions
****************************************************************************/

// The data structure is used to store string form and type of stock symbol
typedef struct t_Symbol
{
    char symbol[SYMBOL_LEN];
} t_Symbol;

typedef struct t_SymbolMgmt
{
	//----------------------------------------------------------------------------
	// This symbol management structure can store symbols up to 8 chars length!
	//----------------------------------------------------------------------------
	
	//This contains the list of symbols
	t_Symbol symbolList[MAX_STOCK_SYMBOL];
		
	//This array stores index of symbols from 1 to 5 characters.
	short symbolIndexArray[MAX_CHARSET_MEMBER_NO]
								[MAX_CHARSET_MEMBER_NO]
								[MAX_CHARSET_MEMBER_NO]
								[MAX_CHARSET_MEMBER_NO]
								[MAX_CHARSET_MEMBER_NO];
	
	//This array stores index of symbols from 6 to 7 characters.
	short longSymbolIndexArray[MAX_STOCK_SYMBOL][MAX_CHARSET_MEMBER_NO][MAX_CHARSET_MEMBER_NO];
	
	//This array stores index of symbols with exactly 8 characters.
	short extraLongSymbolIndexArray[MAX_STOCK_SYMBOL][MAX_CHARSET_MEMBER_NO];
	
	//This is the index of last symbol in the list (number of symbols in the list)
	short nextSymbolIndex;
} t_SymbolMgmt;

/****************************************************************************
** Global variable declarations
****************************************************************************/
extern t_SymbolMgmt symbolMgmt;
extern pthread_mutex_t updateStockSymbolMutex;

/****************************************************************************
** Function declarations
****************************************************************************/

// Stock symbol management
int GetStockSymbolIndex(const char *stockSymbol);
int InitStockSymbolDataStructure(void);
int UpdateStockSymbolIndex(const char *stockSymbol);
int ConvertCMSSymbolToComstock(const char *cms, char *symbol);
int ConvertCQSSymbolToComstock(char *cqs, char *comstock);
#endif
