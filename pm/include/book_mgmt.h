/****************************************************************************
** Project name:	Equity Arbitrage Application
** File name: 		book_mgmt.h
** Description: 	This file contains some declarations for book_mgmt.c

** Author: 		Sang Nguyen-Minh
** First created on 26 September 2007
** Last updated on 26 September 2007
****************************************************************************/

#ifndef __BOOK_MGMT_H__
#define __BOOK_MGMT_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include <math.h>
#include "global_definition.h"
#include "stock_symbol.h"
#include "utility.h"

#define MAX_ODD_LOT_SHARE 99
#define ROUND_LOT_TYPE 0
#define ODD_LOT_TYPE 1

#define MAX_LEVEL 12


#define NASDAQ_MAX_DIVISOR 18000000
#define NASDAQ_MAX_SAME_REMAINDER 6

#define ARCA_MAX_DIVISOR 45000000
#define ARCA_MAX_SAME_REMAINDER 6

#define MAX_QUOTE_IGNORE 10

#define NASDAQ_MAX_REF_NUM_DIVISOR	(NASDAQ_MAX_DIVISOR * 2147483647L)
#define ARCA_MAX_REF_NUM_DIVISOR	(ARCA_MAX_DIVISOR * 2147483647L)

/****************************************************************************
** Data structure definitions
****************************************************************************/
typedef struct t_OrderInfo
{
	int orderID;
	int side;
	int shares;
	double price;
} t_OrderInfo;

typedef struct t_OrderDetail
{
	int shareVolume;
	unsigned char tradeSession;
	char _padding[3];
	double sharePrice;
	long refNum;
} t_OrderDetail;

typedef struct t_OrderDetailNode
{
	t_OrderDetail info;
	int level;
	struct t_OrderDetailNode *forward[MAX_LEVEL];
} t_OrderDetailNode;

typedef struct t_SkipList
{
	t_OrderDetailNode *head;
	
	// Temp variable is used to return new node
	t_OrderDetailNode *newNode;

	// 0, 1, 2, 3...
	int level;

	// Number of nodes in list
	int countNode;
} t_SkipList;

typedef struct t_TopOrder
{
	t_OrderDetail info;
	char ecnIndicator;
} t_TopOrder;

typedef struct t_PartialHashingItem
{
	int quotient;
	char side;
	short stockSymbolIndex;
	t_OrderDetailNode *node;
} t_PartialHashingItem;

typedef struct t_ArcaHashingBackupItem
{
	int quotient;
	char tradeSession;
} t_ArcaHashingBackupItem;

typedef struct t_NasdaqHashingBackupItem
{
	int quotient;
	short stockSymbolIndex;
	char side;	// BID_SIDE or ASK_SIDE, NOT 'B'/'S'
} t_NasdaqHashingBackupItem;

typedef struct t_QuoteNeedDelete
{
	int ecnID;
	int side;
	int stockSymbolIndex;
	double priceToDelete;
} t_QuoteNeedDelete;

typedef struct t_QuoteToDelete
{
	int stockSymbolIndex;
	int side;
	double sharePrice;
} t_QuoteToDelete;

typedef struct t_FlushBookRequest
{
	int isFlushBook;
	int side;
	int stockSymbolIndex;
} t_FlushBookRequest;

/****************************************************************************
** Global variable declarations
****************************************************************************/

extern t_QuoteToDelete quoteToDeleteCollection[MAX_ORDER_CONNECTIONS];
extern t_FlushBookRequest flushBookRequestCollection[MAX_BOOK_CONNECTIONS];

extern t_SkipList *stockBook[MAX_BOOK_CONNECTIONS][MAX_SIDE][MAX_STOCK_SYMBOL];
extern t_TopOrder bestQuote[MAX_SIDE][MAX_STOCK_SYMBOL];
extern t_TopOrder bestQuoteRoundLot[MAX_SIDE][MAX_STOCK_SYMBOL];

extern t_PartialHashingItem NASDAQ_PartialHashing[NASDAQ_MAX_DIVISOR][NASDAQ_MAX_SAME_REMAINDER];
extern t_PartialHashingItem ARCA_PartialHashing[ARCA_MAX_DIVISOR][ARCA_MAX_SAME_REMAINDER];
extern t_NasdaqHashingBackupItem NASDAQ_HashingBackup[NASDAQ_MAX_DIVISOR][NASDAQ_MAX_SAME_REMAINDER];
extern t_ArcaHashingBackupItem ARCA_HashingBackup[ARCA_MAX_DIVISOR][ARCA_MAX_SAME_REMAINDER];

extern pthread_mutex_t updateAskBidMutex[MAX_STOCK_SYMBOL];
extern pthread_mutex_t lockStockBookMutex[3];
extern pthread_mutex_t tradingMgmtMutex[MAX_STOCK_SYMBOL];
extern pthread_mutex_t deleteQuotesMgmtMutex[MAX_BOOK_CONNECTIONS][MAX_STOCK_SYMBOL][MAX_SIDE];
extern char manFlushBookFlag[MAX_BOOK_CONNECTIONS];
extern int manFlushBookStockSymbolIndex[MAX_BOOK_CONNECTIONS];

extern t_OrderDetail quoteIgnoreList[MAX_STOCK_SYMBOL][MAX_QUOTE_IGNORE];
/****************************************************************************
** Function declarations
****************************************************************************/
int DeleteAllQuoteOnASide(int ecnBookID, int stockSymbolIndex, int side);
int DeletePartialHashingItem(int ecnBookID, long refNum);
int DeleteQuotes(int ecnBookID, t_QuoteToDelete *quoteToDelete);
int FlushASideOfBook(int ecnBookID, int stockSymbolIndex, int side);
int FlushECNStockBook(int ecnBookID);
int FlushECNSymbolStockBook(int ecnBookID);
int GeneralDelete(t_SkipList *list, t_OrderDetailNode *delete_node, int side, int stockSymbolIndex);
int GeneralModify(t_OrderDetailNode *modify_node, int NewShareVolume, int side, int stockSymbolIndex);
int GenerateLevel(void);
int GetBestOrder(t_TopOrder *bestOrder, int side, int stockSymbolIndex);
int GetBestOrderRoundLot(t_TopOrder *bestOrder, int side, int stockSymbolIndex);
int InsertPartialHashingItem(t_OrderDetailNode *node, int ecnBookID, long refNum, int stockSymbolIndex, char side);
int UpdateMaxBid(int stockSymbolIndex, const int isPlaceOrder, int ecnHalted);
int UpdateMaxBidRoundLot(int stockSymbolIndex, const int isPlaceOrder, int needLock, int ecnHalted);
int UpdateMinAsk(int stockSymbolIndex, const int isPlaceOrder, int ecnHalted);
int UpdateMinAskRoundLot(int stockSymbolIndex, const int isPlaceOrder, int needLock, int ecnHalted);
t_OrderDetailNode *GeneralInsert(t_SkipList *list, t_OrderDetail *info, int side, int stockSymbolIndex);
t_OrderDetailNode *MakeNode(int NewLevel, t_OrderDetail *info);
t_TopOrder *GetBestQuote(t_TopOrder topOrderArray[MAX_BOOK_CONNECTIONS], int side);
t_TopOrder *GetBestQuoteRoundLot(t_TopOrder topOrderArray[MAX_BOOK_CONNECTIONS], int side);
void Destroy_List(t_SkipList *list);
void GetTopQuotes(t_TopOrder topOrderArray[MAX_BOOK_CONNECTIONS], int side, int stockSymbolIndex, int ecnHalted);
void GetTopQuotesRoundLot(t_TopOrder topOrderArray[MAX_BOOK_CONNECTIONS], int side, int stockSymbolIndex, int needLock, int ecnHalted);
void InitBookMgmtDataStructure(void);
void Initialize_List(t_SkipList **inputList);
void* FindPartialHashingItem(int ecnBookID, long refNum);
int DeleteAllExpiredArcaOrders(const int stockSymbolIndex, const unsigned char validSession, const int sideToDelete);
int RegisterPartialHashingItem_ARCA(long refNum, int *rowIndex, int *columnIndex);
int InsertPartialHashingItem_ARCA(t_OrderDetailNode *node, long refNum, int stockSymbolIndex, char side, int rowIndex, int columnIndex);
int UpdatePartialHashingItem_ARCA(t_OrderDetailNode *node, long refNum);
void* FindBackupPartialHashingItem(int ecnBookID, long refNum);
int DeleteBackupPartialHashingItem(int ecnBookID, long refNum);
int InsertBackupPartialHashingItem_ARCA(long refNum, unsigned char tradeSession);
int InsertBackupPartialHashingItem_NASDAQ(long refNum, int stockSymbolIndex, int side);
void *ThreadReceiveBookData(void *args);
void TerminateBookReceivingThread(dbl_device_t *dev);
#endif
