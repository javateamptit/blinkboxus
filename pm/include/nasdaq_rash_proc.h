/****************************************************************************
** Project name:	Equity Arbitrage Application
** File name: 		nasdaq_rash_proc.h
** Description: 	This file contains some declarations for nasdaq_rash_proc.c

** Author: 		Sang Nguyen-Minh
** First created on 26 September 2007
** Last updated on 26 September 2007
****************************************************************************/

#ifndef __NASDAQ_RASH_PROC_H__
#define __NASDAQ_RASH_PROC_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include "global_definition.h"
#include "aggregation_server_proc.h"
#include "utility.h"
#include "trading_mgmt.h"

#define NASDAQ_RASH_NEW_ORDER_MSG_LEN 141
#define CANCEL_MSG_LEN 23

// NASDAQ RASH Sequence Messages
#define SEQUENCE_MSG 'S'

#define NASDAQ_RASH_ORDER_ACCEPTED 'A'
#define NASDAQ_RASH_ORDER_EXECUTED 'E'
#define NASDAQ_RASH_ORDER_CANCELED 'C'
#define NASDAQ_RASH_ORDER_REJECTED 'J'
#define NASDAQ_RASH_BROKEN_TRADE 'B'
#define NASDAQ_RASH_SYSTEM_EVENT 'S'

// NASDAQ RASH Unsequence Messages
#define NASDAQ_RASH_HEARTBEAT 'H'
#define NASDAQ_RASH_DEBUG '+'
#define NASDAQ_RASH_LOGIN_ACCEPTED 'A'
#define NASDAQ_RASH_LOGIN_REJECTED 'J'

#define NASDAQ_RASH_LOGIN_ACCEPTED_RETURN '0'
#define NASDAQ_RASH_LOGIN_REJECTED_RETURN '1'

// NASDAQ RASH Sequence Message lengths
#define NASDAQ_RASH_ORDER_ACCEPTED_LEN 158
#define NASDAQ_RASH_ORDER_EXECUTED_LEN 51
#define NASDAQ_RASH_ORDER_CANCELED_LEN 32
#define NASDAQ_RASH_ORDER_REJECTED_LEN 26
#define NASDAQ_RASH_BROKEN_TRADE_LEN 35
#define NASDAQ_RASH_SYSTEM_EVENT_LEN 12

// NASDAQ RASH Unsequence Message lengths
#define NASDAQ_RASH_HEARTBEAT_LEN 2
#define NASDAQ_RASH_LOGIN_ACCEPTED_LEN 22
#define NASDAQ_RASH_LOGIN_REJECTED_LEN 3

#define HALF_MAX_BUFFER_LEN 512
/****************************************************************************
** Data structure definitions
****************************************************************************/
typedef struct t_NASDAQ_RASH_Config
{
	int incomingSeqNum;
	int outgoingSeqNum;
	int enableTrading;
	int isConnected;

	pthread_mutex_t outgoingSeqNum_Mutex;
	
	int socket;
	int port;
	char ipAddress[80];
	
	char userName[80];
	char password[80];
	
	char buffer[2 * HALF_MAX_BUFFER_LEN];
	
	int remainingBytes;
	int numOfByteReceived;
	int numOfBytesPerRecv;
	int numOfBytesToRecv;
	int startIndex;
	
	char isUnCompletedMsg;
	
} t_NASDAQ_RASH_Config;	

typedef struct t_NASDAQ_RASH_NewOrder
{
	char *orderToken;
	char side;
	int share;
	char *stock;
	double price;
	double pegDef;
	int timeInForce;
	char *firm;
	char display;
	char buyLaunch;
	int maxFloor;
	
	char tagForSS;
	char ecnCrossTrade;
} t_NASDAQ_RASH_NewOrder;

typedef struct t_NASDAQ_RASH_CancelOrder
{
	char *orderToken;
	char symbol[SYMBOL_LEN];
	int share;
	int accountSuffix;
} t_NASDAQ_RASH_CancelOrder;

/****************************************************************************
** Global variable declarations
****************************************************************************/

extern t_NASDAQ_RASH_Config NASDAQ_RASH_Config;

/****************************************************************************
** Function declarations
****************************************************************************/
int BuildAndSend_NASDAQ_RASH_CancelMsg(t_NASDAQ_RASH_CancelOrder *cancelOrder);
int BuildAndSend_NASDAQ_RASH_Heartbeat(void);
int BuildAndSend_NASDAQ_RASH_LogonRequest(void);
int BuildAndSend_NASDAQ_RASH_OrderMsg(t_NASDAQ_RASH_NewOrder *newOrder, t_SendNewOrder *originalOrder);
int Get_NASDAQ_RASH_CompletedMessage(void);
int ProcessAcceptedOrderOfRASH(t_DataBlock dataBlock, int isTrading);
int ProcessCanceledOrderOfRASH(t_DataBlock dataBlock);
int ProcessExecutedOrderOfRASH(t_DataBlock dataBlock);
int ProcessNewOrderOfRASH(t_DataBlock *dataBlock);
int ProcessRejectedOrderOfRASH(t_DataBlock dataBlock);
int Receive_NASDAQ_RASH_CompleteMessage(int msgLen);
int Receive_NASDAQ_RASH_Msg();
int SaveNASDAQ_RASH_SeqNumToFile(void);
int Send_NASDAQ_RASH_Msg(const char *message, int msgLen);
void *StartUp_NASDAQ_RASH_Module(void *threadArgs);
void Add_NASDAQ_RASH_DataBlockToCollection(t_DataBlock *dataBlock, int isManualOrder);
void Initialize_NASDAQ_RASH(void);
void TalkTo_NASDAQ_RASH_Server(void);
int BuildAndSendRASHManualOrder (t_ManualOrder *tmpOrder);
int BuildAndSend_NASDAQ_RASH_ManualCancelMsg(t_ManualCancelOrder *cancelOrder);
#endif
