/*****************************************************************************
**	Project:		Equity Arbitrage Application
**	Filename:		global_definition.h
**	Description:	This file contains some global definitions
**	Author:			Luan Vo-Kinh
**	First created:	15-Sep-2007
**	Last updated:	-----------
*****************************************************************************/
#ifndef __GLOBAL_DEFINITION_H__
#define __GLOBAL_DEFINITION_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

//#define _XOPEN_SOURCE

#include <pthread.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#include <sys/stat.h>

#define VERSION "20140605"

// Return values
#define ERROR -1
#define SUCCESS 0

#define MIN_PORT 1024
#define MAX_PORT 65535

// Path types
#define CONF_BOOK 0
#define CONF_ORDER 1
#define CONF_SYMBOL 2
#define CONF_MISC 3


// AS send delay in microseconds
#define AS_SEND_DELAY 500000

// Maximum message length for AS
#define AS_MAX_MSG_LEN 4000000

// Message header length
#define MSG_HEADER_LEN 6

// Message body length index
#define MSG_BODY_LEN_INDEX 2

// Maximum length of path
#define MAX_PATH_LEN 256

// Maximum bytes of a line
#define MAX_LINE_LEN 80

#define MAX_COMP_GROUP_ID_LEN 6

// maximum bytes of timestamp
#define MAX_TIMESTAMP 28

#define TIMESTAMP_ADJUST_HOUR 1

// Define some special characters
#define USED_CHAR_CODE 32

// Null pointer
#define NULL_POINTER 0

// Maximum entries for raw data of each ECN Order
#define MAX_RAW_DATA_ENTRIES 40000

// AS centralized order types
#define TYPE_ARCA_DIRECT	100
#define TYPE_NASDAQ_OUCH	101
#define TYPE_NASDAQ_RASH	102
#define TYPE_NYSE_CCG		103
#define TYPE_BATSZ_BOE		104
#define TYPE_EDGX			105

// Maximum orders placement
#define MAX_ORDER_PLACEMENT 200000

// Maximum connections for ECN Order
#define MAX_ORDER_CONNECTIONS 2
#define ARCA_DIRECT_INDEX 0
#define NASDAQ_RASH_INDEX 1

// Maximum connections for ECN Book
#define MAX_BOOK_CONNECTIONS 2

// Maximum of supported ECN Books
#define BOOK_ARCA 0
#define BOOK_NASDAQ 1
#define BOOK_NYSE 2
#define BOOK_BATSZ 3
#define BOOK_EDGX 4

#define SYMBOL_STATUS_HALT 1
#define SYMBOL_STATUS_RESUME 0

#define MAX_SIDE 2
#define BID_SIDE 1
#define ASK_SIDE 0

// Manual Order and Automatic Order
#define MANUAL_ORDER -1
#define AUTOMATIC_ORDER 0

#define MAX_MANUAL_ORDER 300

// Maximum length of rawdata (message) from ECN Orders
#define MAX_ORDER_MSG_LEN 512 - 52

// Maximum length of additional information
#define MAX_ADD_INFO_LEN 40

#define BUY_LAUNCH_INDEX 28
#define CROSS_ID_INDEX 30
#define ACCOUNT_SUFFIX_INDEX 35

#define MAX_ACCOUNT 4
#define FIRST_ACCOUNT 0
#define SECOND_ACCOUNT 1
#define THIRD_ACCOUNT 2
#define FOURTH_ACCOUNT 3

#define BUY_TO_CLOSE 0
#define BUY_TO_OPEN 1

// Legnth of symbol
#define SYMBOL_LEN 8

// Maximum ignore symbols
#define MAX_IGNORE_SYMBOL 10000

#define MAX_STOCK_SYMBOL 15000

#define MAX_PARAMETER 100
#define MAX_LEN_PARAMETER 80

#define YES 1
#define NO 0

#define DISCONNECTED 0 
#define CONNECTED 1

#define MAX_RECV_TIMEOUT 1
#define MAX_RECV_ARCA_BOOK_TIMEOUT 32

#define DISABLED 0 
#define ENABLED 1

#define DEFAULT -1
#define ENABLE 1
#define DISABLE 0

#define UPDATED 1
#define NOT_YET 0

// For MPID
#define MPID_NASDAQ_RASH "HBIF"

// Disengage reason code:
#define DISENGAGE_REASON_CONSECUTIVE_REJECTS 1
#define DISENGAGE_REASON_BEST_ASK_NOTHING 2
#define DISENGAGE_REASON_BEST_BID_NOTHING 3
#define DISENGAGE_REASON_BID_GT_ASK 4
#define DISENGAGE_REASON_INSIDE_SPREAD_GT_MAX_SPREAD_EXEC_LOW 5
#define DISENGAGE_REASON_INSIDE_SPREAD_GT_MAX_PROFIT_SPREAD 6
#define DISENGAGE_REASON_INSIDE_SPREAD_GT_MAX_BIG_LOSER_SPREAD_EXEC_LOW 7
#define DISENGAGE_REASON_INSIDE_SPREAD_GT_MAX_SPREAD_FM 8
#define DISENGAGE_REASON_INSIDE_SPREAD_GT_MAX_BIG_LOSER_SPREAD_FM 9
#define DISENGAGE_REASON_BAD_PRICE_EXIT_SHORT_LIQUIDATE 10
#define DISENGAGE_REASON_BAD_PRICE_EXIT_LONG_LIQUIDATE 11
#define DISENGAGE_REASON_BAD_PRICE_EXIT_SHORT_BBO 12
#define DISENGAGE_REASON_BAD_PRICE_EXIT_LONG_BBO 13
#define DISENGAGE_REASON_CAN_NOT_FIND_SYMBOL 14
#define DISENGAGE_REASON_SYMBOL_IGNORED 15
#define DISENGAGE_REASON_SYMBOL_HALTED 16
#define DISENGAGE_REASON_CANT_CANCEL_EXISTING_ORDER 17
#define DISENGAGE_REASON_ZERO_SHARES 18
#define DISENGAGE_REASON_DISABLED_TRADING 19
#define DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN 20
#define DISENGAGE_REASON_REJECTED_BY_PM 21
#define DISENGAGE_REASON_SYMBOL_FULL_FILLED               22
#define DISENGAGE_REASON_TRADER_REQUESTED_IGNORED         23
#define DISENGAGE_REASON_TRADER_REQUESTED_DISENGAGE       24

// Note: size of reason(1 unsigned byte) => must define stuck status value in range (0 - 255)
#define STUCK_STATUS_REASON_DISABLED_TRADING 100
#define STUCK_STATUS_REASON_CAN_NOT_FIND_SYMBOL 101
#define STUCK_STATUS_REASON_SYMBOL_IGNORED 102
#define STUCK_STATUS_REASON_NONE 110

//Service Type
#define SERVICE_ARCA_BOOK 		10000
#define SERVICE_NASDAQ_BOOK 	10001

#define SERVICE_CQS_FEED		11000
#define SERVICE_UQDF_FEED		11001
#define SERVICE_CTS_FEED		11002
#define SERVICE_UTDF_FEED		11003

#define SERVICE_ARCA_DIRECT  	20000
#define SERVICE_NASDAQ_RASH 	20001

// Disable trading reason
#define DISABLE_TRADING_BY_ALL_BOOK_DISCONNECTED -2
#define DISABLE_TRADING_BY_ALL_ORDER_DISCONNECTED -1

/****************************************************************************
** Data structure definitions
****************************************************************************/

// This structure is to contain connection status
typedef struct t_ConnectionStatus
{
	char status;
	int socket;
	pthread_mutex_t socketMutex;
} t_ConnectionStatus;

// This structure is used to contain connection information
typedef struct t_ConnectionInfo
{
	char ip[MAX_LINE_LEN];
	int port;
	int socket;
} t_ConnectionInfo;

// This structure is used to contain ARCA Book configuration
typedef struct t_ARCABookConf
{
	char sourceID[MAX_LINE_LEN];
} t_ARCABookConf;

// This structure is used to contain NASDAQ Book configuration
typedef struct t_NASDAQBookConf
{
	t_ConnectionInfo UDP;
	t_ConnectionInfo TCP;
} t_NASDAQBookConf;

// This structure is used to contain ECN Books configuration
typedef struct t_ECNBooksConf
{
	t_ARCABookConf ARCABookConf;
	t_NASDAQBookConf NASDAQBookConf;
} t_ECNBooksConf;

// This structure is used to contain configuration of AS
typedef struct t_CurrentStatus
{	
	char tradingStatus;
} t_CurrentStatus;

// This structure is used to contain configuration of AS
typedef struct t_ASConf
{	
	int port;
} t_ASConf;

// This structure is used to management global configuration of Position Manager
typedef struct t_PMGlobalConf
{
	// Auto spread
	double autoSpread;
	
	// Maximum spread
	double maxSpread;

	// Maximum loser spread
	double maxLoserSpread;

	// Profit spread
	double profitSpread;

	// Maximum profit spread
	double maxProfitSpread;

	// Big loser amount
	double bigLoserAmount;

	// Big loser spread
	double bigLoserSpread;

	// Maximum big loser spread
	double maxBigLoserSpread;
	
	double fm_maxSpread;
	double fm_maxLoserSpread;
	double fm_maxBigLoserSpread;
	int highExecutionRate;
	int movingAveragePeriod;
} t_PMGlobalConf;

// This structure is to contain Aggregation Server information
typedef struct t_ASCollection
{
	// Aggregation Server configuration
	t_ASConf config;

	// Aggregation Server connection status
	t_ConnectionStatus connection;

	// ECN Orders connection status
	t_ConnectionStatus orderConnection[MAX_ORDER_CONNECTIONS];

	// ECN Books connection status
	t_ConnectionStatus bookConnection[MAX_BOOK_CONNECTIONS];

	// ECN global configuration
	t_PMGlobalConf globalConf;

	// Current status
	t_CurrentStatus currentStatus;
} t_ASCollection;

typedef struct t_Position
{
	char symbol[SYMBOL_LEN];
	int symbolIndex;
	char accountSuffix;
	char side;		
	int shares;
	double price;
	double referencePrice;
} t_Position;

//For verify position info
typedef struct t_VerifyPositionInfo
{
	char symbol[SYMBOL_LEN];
	int filledShares;
	char side;
} t_VerifyPositionInfo;

// This structure is used to contain a message to AS
typedef struct t_ASMessage
{
	int msgLen;
	unsigned char msgContent[AS_MAX_MSG_LEN];
} t_ASMessage;

// The structure is used to store sent/received messages to/from ECN Order servers
typedef struct t_DataBlock
{
	// Length of block data excluded block length
	int blockLen;
	
	// Length of message content
	int msgLen;
	
	// Content of message will be stored here
	unsigned char msgContent[MAX_ORDER_MSG_LEN];
	
	/*
	Length of additional information 
	such as timestamp, Entry/Exit indicator...
	*/
	int addLen;
	
	// Content of additional information
	unsigned char addContent[MAX_ADD_INFO_LEN];
} t_DataBlock;

// This structure is used to management raw data
typedef struct t_RawDataMgmt
{
	// File pointer contains file descriptor to raw data file
	FILE *fileDesc;

	// ECN Order index
	int orderIndex;

	// TS Id: AS = -1
	int tsId;

	// Last update index
	int lastUpdatedIndex;

	// Count raw data block
	int countRawDataBlock;

	// Total sent raw data block
	int totalSentRawData;

	// Check raw data loaded
	int isProcessed;

	// File name is used stored raw data
	char fileName[MAX_LINE_LEN];

	// Mutex lock
	pthread_mutex_t updateMutex;

	// All rawdata blocks will be stored at the collection
	t_DataBlock dataBlockCollection[MAX_RAW_DATA_ENTRIES];
} t_RawDataMgmt;

// This structure is used to contain ignore symbol list
typedef struct t_IgnoreStockList
{
	int countStock;
	char stockList[MAX_IGNORE_SYMBOL][SYMBOL_LEN];
} t_IgnoreStockList;

//	This structure is used to contain
typedef struct t_Parameters
{
	char parameterList[MAX_PARAMETER][MAX_LEN_PARAMETER];
	int countParameters;
} t_Parameters;

// This structure is used to converter between Double and UNSIGNED CHAR
typedef union t_DoubleConverter
{
	double value;
	unsigned char c[8];
} t_DoubleConverter;

// This structure is used to converter between INT and UNSIGNED CHAR
typedef union t_IntConverter
{
	int value;
	unsigned char c[4];
} t_IntConverter;

// This structure is used to converter between SHORT and UNSIGNED CHAR
typedef union t_ShortConverter
{
	unsigned short value;
	unsigned char c[2];
} t_ShortConverter;

typedef union t_LongConverter
{
	long value;
	unsigned char c[8];
} t_LongConverter;

// This structure is used to contain manual order information
typedef struct t_ManualOrder
{
	char symbol[SYMBOL_LEN];
	double price;
	double pegDef;
	
	char routeDest[4];
	int side;
	int shares;
	int maxFloor;
	int ECNId;
	int tif;
	int orderId;
	int ttIndex;
	int isForced;
	int sendId;
	int accountSuffix;
} t_ManualOrder;
/****************************************************************************
** Global variable definition
****************************************************************************/

/****************************************************************************
** Function declarations
****************************************************************************/

#endif
