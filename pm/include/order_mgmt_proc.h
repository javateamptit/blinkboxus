/*****************************************************************************
**	Project:		Equity Arbitrage Application
**	Filename:		order_mgmt_proc.h
**	Description:	This file contains some declaration for order_mgmt_proc.c	
**	Author:			Luan Vo-Kinh
**	First created:	13-Sep-2007
**	Last updated:	-----------
*****************************************************************************/

#ifndef __ORDER_MGMT_PROC_H__
#define __ORDER_MGMT_PROC_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include <stdlib.h>

#include "global_definition.h"
#include "utility.h"
#include "stock_symbol.h"

// Maximum order id range
#define MAX_ORDER_ID_RANGE 400000

// Maximum open order send
#define MAX_OPEN_ORDER_SEND 2000

// For time in force
#define DAY_TYPE 0
#define IOC_TYPE 1

// For side
#define BUY_TO_CLOSE_TYPE 0
#define SELL_TYPE 1
#define SHORT_SELL_TYPE 2

#define MAX_TYPE_CHAR 32

#define MAX_SYMBOL_TRADE 20000
#define MAX_LAST_TRADE 100

#define LIQUIDATE_ORDER 0
#define BBO_ORDER 1

#define MAX_LIQUIDATE_ITEM 10000
#define MAX_BBO_ORDER_SLEEPING_ITEM 500

#define TRADER_REQUEST 0
#define PM_REQUEST 1

#define MAX_SECONDS_FOR_TRADED_INFO 30

#define CTS_DATA 0
#define UTDF_DATA 1

/****************************************************************************
** Data structures definitions
****************************************************************************/

// This structure is used to contain index of status list of order placement
typedef struct t_OrderStatusIndex
{	
	int Sent;
	int Live;
	int Rejected;
	int Closed;
	int BrokenTrade;
	int CanceledByECN;
	int CXLSent;
	int CancelACK;
	int CancelRejected;
	int CancelPending;
	int CanceledByUser;
	int Sleeping;
} t_OrderStatusIndex;

// This structure is used to contain open order information
typedef struct t_OpenOrderInfo
{
	double price;
	double avgPrice;
	int symbolIndex;
	int side;
	int shares;
	int leftShares;	
	int canceledShares;
	int ECNId;
	int clOrdId;
	int status;
	int tif;
	int type;
	int requester;
	int numSeconds;
	int numMicroseconds;
	char symbol[SYMBOL_LEN];
	long ecnOrderId;
	char accountSuffix;
	char cancelRequest;  // 1: sent OK, 0: not OK
} t_OpenOrderInfo;

// This structure is used to management open order
typedef struct t_ASOpenOrderMgmt
{
	int countOpenOrder;
	t_OpenOrderInfo orderSummary[MAX_ORDER_PLACEMENT];
} t_ASOpenOrderMgmt;

// This structure is used to contain new order information
typedef struct t_SendNewOrder
{
	double price;
	double pegDef;
	int symbolIndex;
	int side;
	int shares;
	int maxFloor;
	int ECNId;
	int tif;
	int orderId;
	int type;
	char symbol[SYMBOL_LEN];
	int accountSuffix;
} t_SendNewOrder;

typedef struct t_TradeInfo
{	
	char dataFeed;
	char participantId;	
	int timestamp;
	int shares;
	double price;
} t_TradeInfo;

typedef struct t_TradeList
{
	double highPrice;
	double lowPrice;
	unsigned int haltTime[MAX_BOOK_CONNECTIONS];
	int countTrade;
	char symbol[SYMBOL_LEN];
	char haltStatus[MAX_BOOK_CONNECTIONS];
	char haltStatusSent;
	char isAutoResume;
	t_TradeInfo tradeList[MAX_LAST_TRADE];
} t_TradeList;

typedef struct t_TradeSymbol
{
	int countSymbol;
	t_TradeList symbolInfo[MAX_SYMBOL_TRADE];
} t_TradeSymbol;

typedef struct t_LiquidateOrderCollection
{
	int count;
	int collection[MAX_LIQUIDATE_ITEM];
} t_LiquidateOrderCollection;

typedef struct t_BBOOrderSleepingCollection
{
	char threadIsCreated;
	int count;
	int collection[MAX_BBO_ORDER_SLEEPING_ITEM];
} t_BBOOrderSleepingCollection;

typedef struct t_ManualCancelOrder
{
	//RASH
	char orderToken[16];
	
	//ARCA DIRECT
	int orderQty;
	int accountSuffix;
	int origClOrdID;
	long ecnOrderID;
	char symbol[SYMBOL_LEN];
	char newClOrdID[20];
	char side;
} t_ManualCancelOrder;

typedef struct t_CancelOrderInfo
{
	int clOrderId;
	long ecnOrderID;
	char symbol[SYMBOL_LEN];
	int ecnType;
	int side;
	int ttl; // in second
	int marked;
	int leftShares;
}t_CancelOrderInfo;

typedef struct t_AutoCancelOrder
{
	int threadStarted;
	t_CancelOrderInfo orderInfo[MAX_MANUAL_ORDER];
}t_AutoCancelOrder;

/****************************************************************************
** Global variables definition
****************************************************************************/
extern t_LiquidateOrderCollection LiquidateOrderCollection;

extern t_BBOOrderSleepingCollection BBOOrderSleepingCollection;

extern t_OrderStatusIndex orderStatusIndex;

extern t_ASOpenOrderMgmt asOpenOrder;

extern t_TradeSymbol tradeSymbolList;

extern short symboIndexlList[MAX_TYPE_CHAR][MAX_TYPE_CHAR][MAX_TYPE_CHAR][MAX_TYPE_CHAR][MAX_TYPE_CHAR];
extern short longSymbolIndexList[MAX_SYMBOL_TRADE][MAX_TYPE_CHAR][MAX_TYPE_CHAR];
extern short extraLongSymbolIndexList[MAX_SYMBOL_TRADE][MAX_TYPE_CHAR];
extern int orderIdIndexList[MAX_ORDER_ID_RANGE];

extern char accountMapping[MAX_ORDER_PLACEMENT];
extern t_AutoCancelOrder AutoCancelOrder;
/****************************************************************************
** Function declarations
****************************************************************************/
int AddTradeForTradeSymbolList(char *symbol, t_TradeInfo *trade);
int GetExecutionRatePerSecond(int symbolIndex);
int GetOrderIdIndex(int clOrdId);
int GetTradeSymbolIndex(const char *stockSymbol);
int InitializeASOpenOrderStructures(void);
int InitializeOrderStatusIndex(void);
int InitializeSymbolIndexList(void);
int InitializeTradeSymbolStructure(void);
int InitilizeOrderIdIndexList(void);
int ProcessAddNewOrder(t_SendNewOrder *order);
int ProcessCanceleRejected(int orderId);
int ProcessDisengageSymbol(char *symbol, int requester);
int ProcessOrderAccepted(int orderId, long ecnOrderId, int numSeconds, int numMicroseconds, int isTrading);
int ProcessOrderCanceled(int orderId, int canceledShares); // ARCA-DIRECT: full canceled, NASDAQ-RASH: partital cannceled
int ProcessOrderFilled(int orderId, int shares);
int ProcessOrderRejected(int orderId);
int ProcessPMConnectECNOrder(int ECNIndex);
int ProcessPMDisconnectECNOrder(int ECNIndex);
int ProcessSendCancelRequestToARCA_DIRECT(int indexOpenOrder, long encOrderId);
int ProcessSendCancelRequestToECNOrder(int clOrdId, long ecnOrderId);
int ProcessSendCancelRequestToNASDAQ_RASH(int indexOpenOrder);
int SendNewOrderToARCA_DIRECTOrder(t_SendNewOrder *order);
int SendNewOrderToNASDAQ_RASHOrder(t_SendNewOrder *order);
int ShowTradedInfo(char *symbol);
int AddToLiquidateCollection (const int);
int AddToBBOOrderSleepingCollection(const int indexOpenOrder);
int SendManualOrder(t_ManualOrder *order);
int CheckManualOrderPriceWithLastTrade(t_ManualOrder *newOrder);
int GetCurrentTradingSession(void);
int ProcessSendManualCancelOrder(t_ManualCancelOrder *cancelOrder, int ECNId);
int ProcessSendManualCancelRequestToARCA_DIRECT(t_ManualCancelOrder *cancelOrder);
int ProcessSendManualCancelRequestToNASDAQ_RASH(t_ManualCancelOrder *cancelOrder);

void* StartSendCancelOrderThread(void *arg);
int SendManualCancelOrder(int index, int force);
/***************************************************************************/

#endif
