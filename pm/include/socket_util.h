/*****************************************************************************
**	Project:		Equity Arbitrage Application
**	Filename:		socket_util.h
**	Description:	This file contains some declaration for socket_util.c	
**	Author:			Luan Vo-Kinh
**	First created:	13-Sep-2007
**	Last updated:	-----------
*****************************************************************************/

#ifndef __SOCKET_UTIL_H__
#define __SOCKET_UTIL_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <pthread.h>
#include <netinet/in.h>
#include <sys/types.h> 
#include <sys/socket.h> 
#include <arpa/inet.h> 
#include <netdb.h> 
#include <signal.h>
#include <poll.h>
#include <errno.h>

#include "global_definition.h"
#include "utility.h"

/****************************************************************************
** Data structures definitions
****************************************************************************/

/****************************************************************************
** Global variables definition
****************************************************************************/

/****************************************************************************
** Function declarations
****************************************************************************/
int CheckErrno(int errno_args);
int CheckValidPort(int port);
int CloseConnection(void *pConnection);
int Connect2Server(char *ip, int port, const char* serverName);
int IsSocketAlive(int socket);
int JoinMulticastGroup(char *ipAddress, int port, int currentSocket);
int Listen2Client(int port, int maxClient);
int SetSocketRecvTimeout(int socket, int seconds);

/***************************************************************************/

#endif
