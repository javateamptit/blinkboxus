/*****************************************************************************
**	Project:		Equity Arbitrage Application
**	Filename:		internal_proc.h
**	Description:	This file contains some declaration for internal_proc.c	
**	Author:			Luan Vo-Kinh
**	First created:	14-Sep-2007
**	Last updated:	-----------
*****************************************************************************/

#ifndef __INTERNAL_PROC_H__
#define __INTERNAL_PROC_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include <stdlib.h>

#include "global_definition.h"
#include "utility.h"

/****************************************************************************
** Data structures definitions
****************************************************************************/
// Process raw data delay in microseconds
#define RAW_DATA_DELAY 100000

/****************************************************************************
** Global variables definition
****************************************************************************/

/****************************************************************************
** Function declarations
****************************************************************************/


/***************************************************************************/

#endif
