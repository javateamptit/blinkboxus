/*****************************************************************************
**	Project:		Equity Arbitrage Application
**	Filename:		utility.h
**	Description:	This file contains function definitions that were declared
					in utility.c	
**	Author:			Luan Vo-Kinh
**	First created:	17-Sep-2007
**	Last updated:	-----------
*****************************************************************************/

#ifndef __UTILITY_H__
#define __UTILITY_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include <time.h>
	   
#include "global_definition.h"
#include "netinet/in.h"
#include "stdint.h"
#include "dbl.h"
#include <poll.h>
#include <fcntl.h>
#include <math.h>

// To turn on print out console, we must uncoment the line below
#define TraceLog(LVL, FMT, PARMS...)                              \
  do {                                                 \
		LrcPrintfLogFormat(LVL, FMT, ## PARMS); }  \
  while (0)
  
  
#define NO_LOG_LEVEL	-1
#define DEBUG_LEVEL		1
#define NOTE_LEVEL		2
#define WARN_LEVEL		3
#define ERROR_LEVEL		4
#define FATAL_LEVEL		5
#define USAGE_LEVEL		6
#define STATS_LEVEL		7

#define DEBUG_STR		"debug: "
#define NOTE_STR		"note: "
#define WARN_STR		"warn: "
#define ERROR_STR		"error: "
#define FATAL_STR		"fatal: "
#define USAGE_STR		"usage: "
#define STATS_STR		"stats: "

#define LRC_SECS_PER_MINUTE ((time_t)60)
#define LRC_SECS_PER_HOUR	((time_t)(60 * LRC_SECS_PER_MINUTE))
#define LRC_SECS_PER_DAY	((time_t)(24 * LRC_SECS_PER_HOUR))
#define LRC_SECS_PER_YEAR	((time_t)(365 * LRC_SECS_PER_DAY))
#define LRC_SECS_PER_LEAP	((time_t)(LRC_SECS_PER_YEAR + LRC_SECS_PER_DAY))

/****************************************************************************
** Data structure definitions
****************************************************************************/

/****************************************************************************
** Global variable definition
****************************************************************************/
extern int Secs_in_years;
extern int Current_year;

extern char globalDateStringOfData[MAX_LINE_LEN];
extern char globalDateStringOfDatabase[MAX_LINE_LEN];

/****************************************************************************
** Function declarations
****************************************************************************/
int JoinMulticastGroup(char *ipAddress, int port, int currentSocket);
int GetMacAddress(char *ifName, char *mac);
int GetIPAddress(char *if_name, char *ip);
int CreateDBLDevice(char *NIC, dbl_device_t *dev);
int CreateDBLDeviceV2(char *NIC, dbl_device_t *dev);
int JoinMulticastGroupUsingExistingDevice(dbl_device_t dev, char *ip, int port, dbl_channel_t *channel);
int JoinMulticastGroupUsingExistingDeviceV2(dbl_device_t dev, char *ip, int port, dbl_channel_t *channel, char *interfaceName, void *context);
int LeaveDBLMulticastGroup(dbl_channel_t ch, const char *ip);
char *GetDateString(int numberOfSeconds, char *dateString);
char *GetFullConfigPath(const char *fileName, int pathType, char *fullConfigPath);
char *GetFullRawDataPath(const char *fileName, char *fullDataPath);
char *GetLocalTimeString(char *timeString);
char *RemoveCharacters(char *buffer, int bufferLen);
char Lrc_Split(const char *str, char c, void *parameter);
double GetDoubleNumber(unsigned char *buffer);
int Get_secs_in_years(time_t *tp, int *year, int *secs_in_years);
int Lrc_atoi(const char * buffer, int len);
int Lrc_is_leap(int year);
struct tm *Lrc_gmtime (time_t *tp, int years, int secs_in_years, struct tm *tm2);
int CreateDataPath(void);
int GetIntNumber(unsigned char *buffer);
int GetIntNumberBigEndian(unsigned char *buffer);
int GetNumberOfSecondsFrom1970(const char *format, const char *dateString);
int GetNumberOfSecondsFromTimestamp(const char *timestamp);
int GetShortNumber(unsigned char *buffer);
int GetShortNumberBigEndian(unsigned char *buffer);
int GetUnsignedIntNumberBigEndian(const unsigned char *buffer);
int GetUnsignedShortNumberBigEndian(const unsigned char *buffer);
int Lrc_itoa(int value, char *strResult);
int Lrc_itoaf(int value, const char pad, const int len, char *strResult);
int TrimRight( char * buffer );
long GetLongNumber(const char *buffer);
long GetLongNumberBigEndian(const unsigned char *buffer);
unsigned char *GetTimeFormatTime(unsigned char *formattedTime);
int GetLocalTimeInTradingTime(void);

int GetUnsignedShortNumber(const unsigned char *buffer);
int GetUnsignedIntNumber(const unsigned char *buffer);
int IsFileTimestampToday(char *fullPath);

void LrcPrintfLogFormat(int level, char *fmt, ...);
void PrintErrStr(int _level, const char *_str, int _errno);

unsigned short __bswap16(unsigned short x);

static const double EPSILON = 1.0e-06;

static inline int feq(double a, double b) { return fabs(a-b) < EPSILON; }

static inline int fgt(double a, double b) { return a - b >  EPSILON; }
static inline int fge(double a, double b) { return a - b > -EPSILON; }

static inline int flt(double a, double b) { return a - b < -EPSILON; }
static inline int fle(double a, double b) { return a - b <  EPSILON; }

#endif
