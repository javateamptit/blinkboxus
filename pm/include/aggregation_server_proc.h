/*****************************************************************************
**	Project:		Equity Arbitrage Application
**	Filename:		aggregation_server_proc.h
**	Description:	This file contains some declaration for aggregation_server_proc.c	
**	Author:			Luan Vo-Kinh
**	First created:	13-Sep-2007
**	Last updated:	-----------
*****************************************************************************/

#ifndef __AGGREGATION_SERVER_PROC_H__
#define __AGGREGATION_SERVER_PROC_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include <stdlib.h>
#include <string.h>

#include "global_definition.h"
#include "utility.h"

#define UPDATE_SOURCE_ID 0
#define UPDATE_RETRANSMISSION_REQUEST 1

// For reload configuration type
#define GLOBAL_CONFIGURATION 1
#define ALGORITHM_CONFIGURATION	2
#define MDC_CONFIGURATION 3
#define OEC_CONFIGURATION 4
#define MIN_SPREAD_CONFIGURATION 5
#define SHORTABILITY_SCHEDULE 6

/****************************************************************************
** Data structures definitions
****************************************************************************/

/****************************************************************************
** Global variables definition
****************************************************************************/

/****************************************************************************
** Function declarations
****************************************************************************/
void *ProcessAggregationServer(void *pthread_arg);
void *SendMsgsToAS(void *pConnection);
int SendPMAllOrderConfToAS(void *pConnection);
int SendPMOrderConfToAS(void *pConnection, int ECNIndex);
int SendPMGlobalConfToAS(void *pConnection);
int SendIgnoreStockListToAS(void *pConnection);
int SendPMCurrentStatusToAS(void *pConnection);
void *ReceiveMsgsASTool(void *pConnection);
int ReceiveASMessage(int sock, void *message);
int ProcessASMessageForPMConnectECNBookFromAS(void *message);
int ProcessASMessageForPMDisconnectECNBookFromAS(void *message);
int ProcessASMessageForPMConnectECNOrderFromAS(void *message);
int ProcessASMessageForPMDisconnectECNOrderFromAS(void *message);
int ProcessASMessageForSetASConf(void *message);
int ProcessASMessageForSetPMOrder(void *message);
int ProcessASMessageForReloadConfig(void *message);
int ProcessASMessageForSetPMGlobalConf(void *message);
int ProcessASMessageForSetIgnoreStock(void *message);
int BuildASMessageForPMOrderConf(void *message, int ECNIndex);
int BuildASMessageForPMGlobalConf(void *message);
int BuildASMessageForIgnoreStockList(void *message);
int BuildASMessageForPMCurrentStatus(void *message);
int SendASMessage(void *pConnection, void *message);
int SendPMAllBookConfToAS(void *pConnection);
int SendPMBookConfToAS(void *pConnection, int ECNIndex);
int BuildASMessageForPMBookConf(void *message, int ECNIndex);
int ProcessASMessageForEnablePMFromAS(void *message);
int ProcessASMessageForDisablePMFromAS(void *message);
int ProcessASMessageForRawDataStatus(void *message);
int SendAllRawDataToAS(void *pConnection);
int SendRawDataToAS(void *pConnection, int ECNIndex);
int BuildASMessageForPMRawData(void *message, int ECNIndex);
int ProcessASMessageForGetStockStatus(void *message);
int SendPMSymbolStatusToAS(int ttIndex, char *stockSymbol, char *listMdc);
int BuildASMessageForPMSymbolStatus(void *message, int ttIndex, char *stockSymbol, char *listMdc);
int ProcessASMessageForStuckInfo(void *message);
int ProcessASMessageForCancelRequest(void *message);
int ProcessASMessageForManualOrder(void *message);
int ProcessASMessageForDisengageSymbol(void *message);
int ProcessASMessageForSetPMBook(void *message);
int ProcessSet_ARCA_Book_Configuration(unsigned char *message);
int ProcessSet_BATS_PITCH_Configuration(unsigned char *message);
int ProcessSet_NASDAQ_Book_Configuration(unsigned char *message);
//int SendCTSConfToAS(void *pConnection);
//int BuildASMessageForCTSConf(void *message);

int ProcessASMessageForPMConnectCTSFromAS(void *message);
int ProcessASMessageForPMDisconnectCTSFromAS(void *message);
int ProcessASMessageForPMConnectCQSFromAS(void *message);
int ProcessASMessageForPMDisconnectCQSFromAS(void *message);
int ProcessASMessageForHaltStock(void *message);
int SendAHaltedStockToAS(t_ConnectionStatus *pConnection, const int symbolIndex, const int willApplyToAllVenues);
int BuildASMessageForAHaltedStock(void *message, const int symbolIndex, const int willApplyToAllVenues);
int ProcessASMessageForGetTradedList(void *message);
int SendTradedListToAS(int ttIndex, char *stockSymbol);
int SendHaltStockListToAS(void *pConnection);
int BuildASMessageForTradedList(void *message, int ttIndex, char *stockSymbol);
int ProcessASMessageForPMConnectUTDFFromAS(void *message);
int ProcessASMessageForPMDisconnectUTDFFromAS(void *message);
int SendPMDisengageAlertToAS(t_Position *stuck, int reason);
int BuildASMessageForPMDisengageAlert(void *message, t_Position *stuck, int reason);
int BuildASMessageForBookIdleWarning(void *message, int ecnIndex, int groupIndex);
int SendPMBookIdleWarningToAS(int ecnIndex, int goupIndex);
int BuildASMessageForDataUnhandledWarning(void *message, int ecnIndex, int groupIndex);
int SendPMDataUnhandledOnGroup(int serviceIndex, int groupIndex);

//For UQDF
int ProcessASMessageForPMConnectUQDF_FromAS(void *message);
int ProcessASMessageForPMDisconnectUQDF_FromAS(void *message);

//For Exchange Configuration
int SendExchangeConfToAS(void *pConnection);
int BuildASMessageForExchangeConf(void *message);
int ProcessSetExchangeConfFromAS(void *message);

//For BBO Quotes
int ProcessGetBBOQuoteListFromAS(void *message);
int SendBBOQuotesToAS(int ttIndex, char *stockSymbol);
int BuildASMessageForBBOQuotesList(void *message, int ttIndex, char *stockSymbol);

//For Flush BBO Quotes for a exchange
int ProcessFlushBBOQuotesFromAS(void *message);

//For flush PM s symbol
int ProcessFlushBookOfASymbol(void *message);
int ProcessFlushBookOfAllSymbols(void *message);

int SendASMessageToRequestSendStuckInfo(t_Position *stuck);
int BuildASMessageForRequestSendStuckInfo(void *message, t_Position *stuck);

int SendManualOrderResponseToAS(t_ManualOrder *newOrder, int reason);
int BuildASMessageForManualOrderResponse(void *message, t_ManualOrder *newOrder, int reason);

int SendNASDAQ_RASH_SeqToAS();
int BuildASMessageForRASHSeqNumber(void *message);
int ProcessASMessageForRASHSeqNumber(void *message);
int ProcessASMessageForManualCancelOrder(void *message);
int ProcessSetArcaFeedMessage(void *message);

int SendPMDisconnectedBookOrderAlertToAS(int typeServer);
int BuildASMessageForPMDisconnetedBookOrderAlert(void *message, int serviceType);
int SendPMDisabledTradingAlertToAS(short reason);
int BuildASMessageForPMDiabledTradingAlert(void *message, short reason);
int CheckOpenPosition(t_Position *stuckInfo);
int SendPMStuckStatusAlertToAS(t_Position *stuck, int status, int reason);
int BuildASMessageForPMStuckStatusAlert(void *message, t_Position *stuck, int status, int reason);
int CheckAndSendAlertDisableTrading(int disconnectType);

int SendEndOfRawDataToAS(void *pConnection);
int BuildASMessageForPMEndOfRawData(void *message);
int ProcessASMessageForHaltStockList(void *message);

int SendPMSleepingSymbolsToAS();
int BuildASMessageForPMSleepingSymbols();

int ProcessASMessageForPriceUpdateRequest(void *message);
int SendPriceUpdateResponseToAS(int maxSymbol, char symbolList[][SYMBOL_LEN]);
int BuildASMessageForPriceUpdate(void *message, int maxSymbol, char symbolList[][SYMBOL_LEN]);
/***************************************************************************/

#endif
