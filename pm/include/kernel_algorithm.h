#define PM_MAX_THREAD 100
#include "global_definition.h"
#include "configuration.h"

typedef struct t_ThreadAlgorithm
{
	char threadDescription[MAX_LINE_LEN];	//Meaningful, human understandable
	int cpuPosition;						// 0 <= cpuPosition < maxAvailableCore
	int schedulerType;						//OTHER, FIFO
	int priority;							//priority for FIFO or nice value for OTHER
}t_ThreadAlgorithm;

typedef struct t_KernelConfig
{
	int maxSystemCPU;
	
	int numberOfThread;	
	t_ThreadAlgorithm threadAlgorithm[PM_MAX_THREAD];
} t_KernelConfig;

extern t_KernelConfig KernelConfig;

void InitKernelAlgorithmConfig(void);
int LoadKernelAlgorithmConfigFromFile(const char *fileName);
int SetKernelAlgorithm(const char *threadDescription);
