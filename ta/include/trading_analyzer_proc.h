
#ifndef _TRADING_ANALYZER_H_
#define _TRADING_ANALYZER_H_

#define _XOPEN_SOURCE

#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <getopt.h>
#include <stdarg.h>
#include <unistd.h>

#include "constant.h"
#include "database_util.h"
#include "configuration.h"
#include "trading_analyzer.h"

/*************
DATA STRUCTURE
**************/
typedef struct PlacedOrder_t
{
	int tradeServer;				//'1' or '2', default = ' '
	char strDate[12];				//format is 'YYYY/MM/DD'
	int clOrdId;
	char strTime[16];				//format is 'hh:mm:ss.xxx'
	int AskBid;						//ASK: 1, Bid: 0
	int entryExit;					//Entry: 1, Exit: 0
	int side;					//'B' or 'S' or 'SS', default = ' '
	char padding1;
	char stockSymbol[12];
	int shares;
	double price;
	int timeInForce;				//'D': DAY, 'G': GTX, 'I': IOC, default = ' '
	int  ECN;					//ARCA, OUCH, RASH, NYSE, BATZ, EDGX
	int leftShares;
	int filledShares;
	int  ecnLaunch;				// launched ECN
	double ackPrice;
	int tradeType;					// 1:simultaneous, !=1 :normal
	char status;					//'F': filled, 'C': canceled, 'R': rejected, default = ' '
	int stockType;					//'E': ETF, 'L': Listed, 'O': OTC
	char padding2[2];
	char routingInst[4];
	int crossId;
  int isoOrder;
} PlacedOrder_t;

// Store the profit/loss of old step
typedef struct ProfitLoss_t
{
	int totalVolume;
	double profitLoss;
	double totalMarket;
	double netProfitLoss;
	double expectedProfit;
} ProfitLoss_t;

// Store trading information of each symbol
typedef struct TradingInfo_t
{
	int totalVolume;
	int leftShares;
	double profitLoss;
	double totalMarket;
	double avgPrice;
	char symbol[MAX_SYMBOL_LEN];
} TradingInfo_t;

// Store trading information of all symbols
typedef struct TradinInfoList_t
{
	TradingInfo_t TradinInfoList[MAX_STOCK_SYMBOL];
	int countSymbol;
} TradinInfoList_t;

/***************
EXTEND DECLARATION
*****************/
// Status of TA, it will calculate on old data(offline) or new data (online)
extern int TaStatus;

// Current date
extern int CurrentDateNumber;

// Old Date
extern int OldDateNumber;

// Last date when data was updated
extern char LastUpdatedDate[MAX_LINE_LENGTH];

// Last date when data was processed
extern char LastProcessedDate[MAX_LINE_LENGTH];

// Last processed index
extern int LastProcessedIndex;

// Total number of senconds from 01/01/1970 to current date
extern int NumOfSecsFrom1970;

// Time Stamp Collection
extern int TimeStampCollection[MAX_TIME_OF_PATTERN];

// Profit/Loss
extern ProfitLoss_t prevProfitLoss;

extern TradinInfoList_t SymbolTradinInfoList;

/********************
FUNCTIONS PROTOTYPES
*********************/

void swap(int *a, int *b);

int  kth_smallest(int  a[], int n, int k);

// Get current date
int GetCurrentDate(int isUpdate);

int ProcessOrderAtCurrentPosition(int currentIndex, const char *currentDate);

int ProcessOrderDetails(int currentIndex, const char *currentDate);

int LoadDataFromEaaDbAtTime(int timeFrom, int timeTo, const char *currentDate);

int InsertDataIntoTwt2Db(int currentIndex, const char *currentDate);

int InsertEntryExitDataIntoTwt2Db(int currentIndex, const char *currentDate, int isOldData);

int CalculateEntryExitStatus();

int InitTimeStampCollection();

int CalculateStockTypesStatus();

int CalculateExitOrderSharesTrading();

int InsertStockTypesDataIntoTwt2Db(int currentIndex, const char *currentDate, int isOldData);

int InsertProfitLossDataIntoTwt2Db(int currentIndex, const char *currentDate);

int InsertOrderTypesDataIntoTwt2Db(int currentIndex, const char *currentDaten, int isOldData);

int CalculateOrderTypesStatus(int currentIndex, const char *currentDate);

int InsertAskBidLaunchIntoTwt2Db(int currentIndex, const char *currentDate, int isOldData);

int CalculateAskBidLaunch(int currentIndex, const char *currentDate);

int GetSymbolIndex(char stockSymbol[8]);

int InitializeSymbolIndexList(void);

int InsertOfflineProfitLossDataIntoTwt2Db(int currentIndex, const char *currentDate, int isOldData);

int CreateDataForBeginOfDay(const char *currentDate);

int InsertOfflineDataIntoTwt2Db(int currentIndex, const char *currentDate, int isOldData);

int ProcessOfflineOrderAtCurrentPosition(int currentIndex, const char *currentDate, int isOldData);

double CalculateFee(double price, int size, char side, char *liquidityField, char *ecn);

int CalculateMedianTime_5(int currentIndex, const char *currentDate);

int CalculateMedianTime_15(int currentIndex, const char *currentDate);

int CalculateMedianTime_30(int currentIndex, const char *currentDate);

int CalculateMedianTime_60(int currentIndex, const char *currentDate);

int InsertAckFillIntoTwt2Db(int currentIndex, const char *currentDate, int isOldData);

int InsertAckFillStep5IntoTwt2Db(int currentIndex, const char *currentDate, int isOldData);

int InsertAckFillStep15IntoTwt2Db(int currentIndex, const char *currentDate, int isOldData);

int InsertAckFillStep30IntoTwt2Db(int currentIndex, const char *currentDate, int isOldData);

int InsertAckFillStep60IntoTwt2Db(int currentIndex, const char *currentDate, int isOldData);

int InsertExitOrderSharesTradingDataIntoTwt2Db(int currentIndex, const char *currentDate, int isOldData);

#endif
