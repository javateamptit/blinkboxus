
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>

#include "constant.h"
#include "trading_analyzer_proc.h"

// Main Function
int main(int argc, char **argv);

// Register signal
int RegisterSignalHandlers(void);

// Handle Segmentation Fault
void ProcessSegmentationFault(int sig_num);

// Handle Interupt
void ProcessInterrupt(int sig_num);

// Start Trading Analyzer
int StartTradingAnalyzer(void);

// Process data on old date
int ProcessDataOnOldDate(int beginDateInSeconds, int endDateInSeconds);

void LrcPrintfLogFormat(int level, char *fmt, ...);

int Print_Help(const char *program_name);

int SaveLoginInfo();

int ProcessUserInput(int argc, char *argv[]);

int GetConfigurationPath();

int CreateDataPath(void);

extern char PathOfCurrentStatus[MAX_PATH_LEN];
extern char PathOfEaaDatabase[MAX_PATH_LEN];
extern char PathOfTwt2Database[MAX_PATH_LEN];
extern char PathOfFeeConfig[MAX_PATH_LEN];
