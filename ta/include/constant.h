/*****************************************************************************
**	Project:		EAA
**	Filename:		constant.h
**	Description:	This file included constant declaration
**	Author:			Danh Thai Hoang
**	First created:	08-07-2009
**	Last updated:	-----------
*****************************************************************************/
#ifndef _CONST_
#define _CONST_

#define VERSION "20140520"

#define ERROR -1
#define SUCCESS 0

#define MAX_LINE_LENGTH 80
#define MIN_VALID_LENGTH 11
#define MIN_VALID_DATE_LENGTH 9
#define MAX_PATH_LEN 256

#define ONLINE 1
#define OFFLINE 0

#define SHORT_SELL "SS"
#define BUY "B"
#define SELL "S"

#define SS 2
#define S 1
#define B 0

#define TRUE 1
#define FALSE 0

#define MAX_ORDER 150000
#define TOTAL_SECS_PER_HOUR 3600
#define TOTAL_SECS_PER_MIN 60
#define NUM_SECONDS_PER_PRIMETIVE_STEP 300

#define UPDATED 1
#define NOT_UPDATED 0

#define MAX_QUERRY_LENGTH 4096	//4 * 1024
#define NUM_OF_TS	9
#define NUM_OF_ECN 10
#define NUM_OF_SIDE 3
#define NUM_OF_ECN_LAUNCH 10
#define NUM_OF_ENTRY_EXIT_STATUS 6
#define NUM_OF_STOCK_TYPES_STATUS 4
#define NUM_OF_TRADING_MODE 2
#define NUM_OF_STOCK_TYPES 3
#define NUM_OF_TRADE_TYPES 3
#define NUM_OF_TIF 3
#define ASK_BID 2
#define NUM_OF_ASK_BID_STATUS 3
#define NUM_OF_TIME_TYPE 2
#define TOTAL_SECONDS_ORDER_COUNT 2
#define MAX_COUNT 512
#define ENTRY_EXIT 2
#define NUM_OF_SHARE_TYPE 2

#define ENTRY 1
#define EXIT 0
#define ENTRY_SUCC 1
#define ENTRY_MISS 0
#define ENTRY_REPRICE 2
#define EXIT_SUCC 4
#define EXIT_MISS 3
#define EXIT_REPRICE 5
#define SIM 1
#define NOR 0
#define ORDER_SIZE 0
#define SHARES_FILLED 1
#define NUM_OF_ISO_ORDER 2
#define ISO 0
#define NON_ISO 1

#define REPRICE 2
#define SUCC 1
#define MISS 0

#define ASK 1
#define BID 0

#define ETF 0
#define LISTED 1
#define OTC 2
//#define ALL 3

#define DAY 0
#define GTC 1
#define IOC 2

#define NUM_OF_ORDER_TYPE 3
#define NUM_OF_ORDER_STATUS 3

#define MAX_SIDE 3

#define FILLED 0
#define NOT_FILLED 1

#define ACK_TIME 0
#define FILL_TIME 1

#define TOTAL_TIME 0
#define ORDER_COUNT 1

#define MAX_STOCK_SYMBOL 15000
#define MAX_CHARSET_MEMBER_NO 32
#define MAX_SYMBOL_LEN 8
#define INDEX_OF_A_CHAR 59

// Define some special characters
#define USED_CHAR_CODE 32

#define MAX_CONNECTION_STRING_LEN 256
#define MAX_TIME_OF_PATTERN (24 * 12) // 24 * 12

#define SECOND_IN_ONE_DAY 24 * 60 * 60

// Log level format
// To turn on print out console, we must uncoment the line below
#define TraceLog(LVL, FMT, PARMS...)                              \
  do {                                                 \
		LrcPrintfLogFormat(LVL, FMT, ## PARMS); }  \
  while (0)

#define NO_LOG_LEVEL	-1
#define DEBUG_LEVEL		1
#define NOTE_LEVEL		2
#define WARN_LEVEL		3
#define ERROR_LEVEL		4
#define FATAL_LEVEL		5
#define USAGE_LEVEL		6
#define STATS_LEVEL		7

#define DEBUG_STR		"debug: "
#define NOTE_STR		"note: "
#define WARN_STR		"warn: "
#define ERROR_STR		"error: "
#define FATAL_STR		"fatal: "
#define USAGE_STR		"usage: "
#define STATS_STR		"stats: "

#endif
