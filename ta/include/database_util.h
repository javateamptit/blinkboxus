/*****************************************************************************
**	Project:		EAA
**	Filename:		database_util.h
**	Description:	This file contains database ultility declarations
					It inherits some functions in the file database_util of Le Quoc Hoang (old TA)
**	Author:			Danh Thai Hoang
**	First created:	08-07-2009
**	Last updated:	-----------
*****************************************************************************/
#ifndef _DB_UTIL_H_
#define _DB_UTIL_H_

#include <stdio.h>
#include <string.h>

#include "constant.h"
#include "configuration.h"
#include "trading_analyzer.h"
#include "libpq-fe.h"

/********************
FUNCTIONS PROTOTYPES
*********************/

// Create a connection to Postgres Database
PGconn *CreateConnection(DatabaseConf_t *dbConf);

//Destroy a connection
void DestroyConnection(PGconn *connection);

// Execute querry
PGresult *ExecuteQuery(PGconn *connection, const char *queryString);

// Get the number of row
int GetNumberOfRow(PGresult *queryResult);

// Get the number of column
int GetNumberOfColum(PGresult *queryResult);

// Check if a field is null
int	IsFieldNull(PGresult *queryResult, int rowIndex, int colIndex);

// Get value of a filed
char* GetValueField(PGresult *queryResult, int rowIndex, int colIndex);

// Clear Querry Result
void ClearResult(PGresult *queryResult);

// Check & Create new database for new month if needed
int CheckAndCreateNewDatabase();

#endif





