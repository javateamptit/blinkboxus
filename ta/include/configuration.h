/*****************************************************************************
**	Project:		EAA
**	Filename:		configuration.h
**	Description:	This file included all declrations for configuration

**	Author:			Danh Thai Hoang
**	First created:	08-07-2009
**	Last updated:	-----------
*****************************************************************************/
#ifndef _CONF_H_
#define _CONF_H_

#include <time.h>
#include <stdio.h>
#include <string.h>

#include "constant.h"


/*******************
DATA STRUCTURE
*******************/
typedef struct DatabaseConfiguration_t
{
	// Database name
	char dbname[512];

	// Database prefix
	char dbprefix[512];

	// User name
	char username[32];

	// password
	char passwd[32];

	// Host name
	char hostname[128];

	// Postgres path
	char postgresPath[128];

} DatabaseConf_t ;

typedef struct t_FeeConf
{
	double secFee;
	double tafFee;
	double commission;
	double addVenueFeeWithPriceLager1;
	double addVenueFeeWithPriceSmaler1;
	double removeVenueFeeWithPriceLager1;
	double removeVenueFeeWithPriceSmaler1;
	double auctionVenueFee;
	double arcaRouteVenueFee;
	double nasdaqRouteVenueFee;
	double batsRouteVenueFee;
	double edgxeRouteVenueFee;
} t_FeeConf;

extern t_FeeConf FeeConf;
extern DatabaseConf_t EaaDatabaseConf;
extern DatabaseConf_t Twt2DatabaseConf;

// Trading time in seconds
extern int BeginTradingTimeInSeconds;
extern int EndTradingTimeInSeconds;
extern int MaxIndexPerDay;

/*******************
FUNCTION PROTOTYPES
********************/

// Load Database Configuration
int LoadDatabaseConfiguration(char *fileName, DatabaseConf_t *dbConf);

int LoadTradingTimeConfiguration();

// Load the last processed position
int LoadProcessedPosition(const char *fileName, int *index, char *dateTime);

// Save the last processed position
int SaveProcessedPosition(const char *fileName, int index, const char *dateTime);

// Save db configuration
int SaveDatabaseConfiguration(const char *fileName);

// Convert date in string form into numerical form
int CovertStrDateToNDate(const char *strDate);

// Get seconds
int GetNumberOfSecondsFrom1970(const char *strDate);

// Init Datastructure
int InitDataStructure();

// Load data configuration
int LoadDataConfiguration();

// To Date Format
int ConvertToDateFormat(char dateFormat[11], const char *strDate);

// To Time Format
int ConvertToTimeFormat(int totalSeconds, char timeFormat[9], int isOldData);

// Load fee configuration
int LoadFeeConf(char *fileName, t_FeeConf *feeConf);

int GetNumberOfSecondFromTimeString(const char *timeString);

int GetTimeInSecondsFromFile(char fullPath[MAX_PATH_LEN], int *numSeconds, char wantedString[MAX_LINE_LENGTH]);

char *RemoveCharacters(char *buffer, int bufferLen);

#endif
