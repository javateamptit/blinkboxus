/*****************************************************************************
 **	Project:		EAA
 **	Filename:		trading_analyzer_proc.c
 **	Description:	This file contains code to process data for trading analyzer
 **	Author:			Danh Thai Hoang
 **	First created:	08-07-2009
 **	Last updated:	-----------
 *****************************************************************************/
#include "hbitime.h"
#include "trading_analyzer_proc.h"

#define MIN(a,b) ((a)>(b)?(b):(a))
#define ELEM_SWAP(a,b) { register int t=(a);(a)=(b);(b)=t; }
#define BATZ_SMART_ROUTE "RL2"

/******************
GLOBAL VARIABLE
******************/

int NumAppear = 0;
// Status of TA, it will calculate on old data(offline) or new data (online)
int TaStatus = OFFLINE;

// Current date
int CurrentDateNumber = -1;

// Old Date
int OldDateNumber = -1;

// Last date when data was updated
char LastUpdatedDate[MAX_LINE_LENGTH] = "\0";

// Last date when data was processed
char LastProcessedDate[MAX_LINE_LENGTH] = "\0";

// Last processed index
int LastProcessedIndex = 0;

// Total number of senconds from 01/01/1970 to current date
int NumOfSecsFrom1970 = 0;

// Time Stamp Collection
int TimeStampCollection[MAX_TIME_OF_PATTERN];

// Number of record
int NumOfRecords;

int side_entryexit[MAX_ORDER];
//side : S, SS, B

// Entry/exit Information
// 1: TS1, TS2, TS3, TS4, TS5, TS6, TS7, TS8
// 2: ARCA, RASH, OUCH, BATZ, NYSE, EDGX, last index is used for BATZ smart route
// 3: S, B, SS
// 4: ENTRY_SUCC, ENTRY_MISS, ENRTY_REPRICE, EXIT_SUCC, EXIT_MISS, EXIT REPRICE
int EntryExit[NUM_OF_TS][NUM_OF_ECN + 1][NUM_OF_SIDE][NUM_OF_ENTRY_EXIT_STATUS];

//Exit Order Trading Information
// 1: TS1, TS2, TS3, TS4, TS5, TS6, TS7, TS8
// 2: ARCA, RASH, OUCH, BATZ, NYSE, EDGX, last index is used for BATZ smart route
// 3: ORDER_SIZE, SHARES_FILLED
int ExitOrderShares[NUM_OF_TS][NUM_OF_ECN + 1][ENTRY_EXIT][NUM_OF_ECN_LAUNCH][NUM_OF_ISO_ORDER][NUM_OF_SHARE_TYPE];

// Stock Type Information
// 1: TS1, TS2, TS3, TS4, TS5, TS6, TS7, TS8
// 2: ARCA, RASH, OUCH, BATZ, NYSE, EDGX
// 3: S, B, SS
// 4: ETF, LISTED, OTC
// 5: IOC, DAY, GTC
// 6: 'E','L','O','A'
int StockTypes[NUM_OF_TS][NUM_OF_ECN][NUM_OF_SIDE][NUM_OF_STOCK_TYPES][NUM_OF_TIF][NUM_OF_STOCK_TYPES_STATUS];

// BS Indicator
int OrderTypes[NUM_OF_TS][NUM_OF_ECN + 1][ENTRY_EXIT][NUM_OF_ORDER_TYPE][NUM_OF_ORDER_STATUS];

// ASK BID Lauch
int AskBidLaunch[NUM_OF_TS][NUM_OF_ECN][NUM_OF_SIDE][NUM_OF_ECN_LAUNCH][ASK_BID][NUM_OF_ASK_BID_STATUS];
int Exit_AskBidLaunch[NUM_OF_TS][NUM_OF_ECN][NUM_OF_SIDE][NUM_OF_ECN_LAUNCH][ASK_BID][NUM_OF_ASK_BID_STATUS];

// Average time
// TYPE:ACK, FILL
int MedianTime[NUM_OF_TS][NUM_OF_ECN][NUM_OF_TIME_TYPE][TOTAL_SECONDS_ORDER_COUNT];
int MedianArray[NUM_OF_TS][NUM_OF_ECN][NUM_OF_TIME_TYPE][1250];

int MedianTime_15[NUM_OF_TS][NUM_OF_ECN][NUM_OF_TIME_TYPE][TOTAL_SECONDS_ORDER_COUNT];
int MedianArray_15[NUM_OF_TS][NUM_OF_ECN][NUM_OF_TIME_TYPE][2500];

int MedianTime_30[NUM_OF_TS][NUM_OF_ECN][NUM_OF_TIME_TYPE][TOTAL_SECONDS_ORDER_COUNT];
int MedianArray_30[NUM_OF_TS][NUM_OF_ECN][NUM_OF_TIME_TYPE][5000];

int MedianTime_60[NUM_OF_TS][NUM_OF_ECN][NUM_OF_TIME_TYPE][TOTAL_SECONDS_ORDER_COUNT];
int MedianArray_60[NUM_OF_TS][NUM_OF_ECN][NUM_OF_TIME_TYPE][10000];

// Size
char Side[MAX_SIDE][MAX_SIDE];

// ECN
char *Ecn[NUM_OF_ECN] = {"ARCA", "OUCH", "NYSE", "RASH", "BATZ", "EDGX", "EDGA", "OUBX","BATY", "PSX"};

// ECN Launch
char *EcnLaunch[NUM_OF_ECN_LAUNCH] = {"ARCA", "NDAQ", "NYSE", "BATZ", "EDGX", "EDGA", "NDBX", "BATY", "PSX","AMEX"};

// Stock Type
char StockType[NUM_OF_STOCK_TYPES] = {'E','L','O'};
	//E: ETF
	//L: Listed
	//O: OTC

// Time In Force
char TimeInForce[NUM_OF_TIF] = { 'D', 'G', 'I' };
//D: DAY
//G: GTC
//I: IOC

// Save information of orders
PlacedOrder_t Orders[MAX_ORDER];

// Repriced Orders
//int numOfRepricedOrders = 0;

// Reprice Orders at exit
//int numOfRepricedOrdersAtExit = 0;

// Profit/Loss
ProfitLoss_t prevProfitLoss;

// Trading info for all symbols
TradinInfoList_t SymbolTradinInfoList;

// Symbol management
short symbolIndexList[MAX_CHARSET_MEMBER_NO]
						[MAX_CHARSET_MEMBER_NO]
							[MAX_CHARSET_MEMBER_NO]
								[MAX_CHARSET_MEMBER_NO]
									[MAX_CHARSET_MEMBER_NO];
short longSymbolIndexArray[MAX_STOCK_SYMBOL]
							[MAX_CHARSET_MEMBER_NO]
								[MAX_CHARSET_MEMBER_NO];
short extraLongSymbolIndexArray[MAX_STOCK_SYMBOL][MAX_CHARSET_MEMBER_NO];

/***********************
 FUNCTIONS IMPLEMENTATION
 ************************/

/***************************************************************************
 Function name:	median
 - Input:			N/A
 - Output:			N/A
 - Return:
 - Description:
 - Usage:

 ***************************************************************************/
int median(int arr[], int n, int medianIndex)
{
	int low, high;
	int median;
	int middle, ll, hh;

    low = 0 ;
	high = n-1 ; 
	median = medianIndex;

    for (;;) {
		if (high <= low) /* One element only */
			return arr[median];

        if (high == low + 1) {  /* Two elements only */
			if (arr[low] > arr[high])
				ELEM_SWAP(arr[low], arr[high]);
			return arr[median];
		}

		/* Find median of low, middle and high items; swap into position low */
		middle = (low + high) / 2;
    if (arr[middle] > arr[high])    ELEM_SWAP(arr[middle], arr[high]) ;
    if (arr[low] > arr[high])       ELEM_SWAP(arr[low], arr[high]) ;
    if (arr[middle] > arr[low])     ELEM_SWAP(arr[middle], arr[low]) ;

		/* Swap low item (now in position middle) into position (low+1) */
		ELEM_SWAP(arr[middle], arr[low+1]);

		/* Nibble from each end towards middle, swapping items when stuck */
		ll = low + 1;
		hh = high;
    for (;;) {
        do ll++; while (arr[low] > arr[ll]) ;
        do hh--; while (arr[hh]  > arr[low]) ;

			if (hh < ll)
				break;

			ELEM_SWAP(arr[ll], arr[hh]);
		}

		/* Swap middle item (in position low) back into correct position */
		ELEM_SWAP(arr[low], arr[hh]);

		/* Re-set active partition */
		if (hh <= median)
			low = ll;
		if (hh >= median)
			high = hh - 1;
	}

	return SUCCESS;
}

/****************************************************************************
 - Function name:	GetCurrentTime
 - Input:			N/A
 - Output:			N/A
 - Return:			current date in numerical form
 - Description:
 - Usage:			Get the current date of timer on trading host
 ****************************************************************************/
int GetCurrentDate(int isUpdate)
{
	// Time declaration
	struct tm *dateTime;

	// Get current date
	time_t now = (time_t)hbitime_seconds();
	dateTime = (struct tm *) localtime(&now);

	//TraceLog("******Get current date: Date Time H: %d, M: %d, S: %d\n", dateTime->tm_hour, dateTime->tm_min, dateTime->tm_sec);

	//	Current date with the numerical form
	CurrentDateNumber = dateTime->tm_year * 10000 + dateTime->tm_mon * 100 + dateTime->tm_mday;

	//	NumberOfSeconds at current time
	int currentTimeInSeconds = dateTime->tm_hour * TOTAL_SECS_PER_HOUR + dateTime->tm_min * TOTAL_SECS_PER_MIN + dateTime->tm_sec;

	// Update lastDateUpdated
	strftime(LastUpdatedDate, MAX_LINE_LENGTH, "%Y/%m/%d", dateTime);

	//	We only trade from Monday to Friday
	if ((dateTime->tm_wday > 0) && (dateTime->tm_wday < 6))
	{
		if (isUpdate == UPDATED)
		{
			if (OldDateNumber < CurrentDateNumber)
			{
				// Update ...
				OldDateNumber = CurrentDateNumber;
				LastProcessedIndex = 0;
			}

			strftime(LastProcessedDate, MAX_LINE_LENGTH, "%Y/%m/%d", dateTime);

			//	Number of seconds since 1970
			dateTime->tm_hour = 0;
			dateTime->tm_min = 0;
			dateTime->tm_sec = 0;

			NumOfSecsFrom1970 = mktime(dateTime);
		}
	}
	else
	{
		if (TaStatus == 1)
		{
			TraceLog(DEBUG_LEVEL, "This is day off.\n");

			if (dateTime->tm_wday == 6)
			{
				TraceLog(DEBUG_LEVEL, "Because it is Saturday\n\n");
			}
			else
			{
				TraceLog(DEBUG_LEVEL, "Because it is Sunday\n\n");
			}
		}
	}

	if (isUpdate == UPDATED)
	{
		if (OldDateNumber < CurrentDateNumber)
		{
			// Update ...
			OldDateNumber = CurrentDateNumber;
		}
	}

	//TraceLog("NumOfSecsFrom1970= %d \n", NumOfSecsFrom1970);

	return currentTimeInSeconds;
}
/****************************************************************************
 - Function name:	InitTimeStampCollection
 - Input:			N/A
 - Output:			N/A
 - Return:
 - Description:
 - Usage:			Init Time Stamp Collection
 ****************************************************************************/
int InitTimeStampCollection()
{
	int index;

	for (index = 0; index < MAX_TIME_OF_PATTERN; index++)
	{
		TimeStampCollection[index] = BeginTradingTimeInSeconds + index * NUM_SECONDS_PER_PRIMETIVE_STEP;
	}

	return SUCCESS;
}
/****************************************************************************
 - Function name:	ProcessOrderAtCurrentPosition
 - Input:			Current index
 Current Date
 - Output:			N/A
 - Return:			void
 - Description:
 - Usage:			Process orders at the current position
 ****************************************************************************/
int ProcessOrderAtCurrentPosition(int currentIndex, const char *currentDate)
{
	// Memset orders before update
	memset(Orders, 0, sizeof(PlacedOrder_t));

	// Process Order Details
	ProcessOrderDetails(currentIndex, currentDate);

	// Insert data to TWT2 Database here
	InsertDataIntoTwt2Db(currentIndex, currentDate);

	return SUCCESS;
}
/****************************************************************************
 - Function name:	ProcessOfflineOrderAtCurrentPosition
 - Input:			Current index
 Current Date
 - Output:			N/A
 - Return:			void
 - Description:
 - Usage:			Process orders at the current position
 ****************************************************************************/
int ProcessOfflineOrderAtCurrentPosition(int currentIndex, const char *currentDate, int isOldData)
{
	// Insert data to TWT2 Database here
	InsertOfflineDataIntoTwt2Db(currentIndex, currentDate, isOldData);
	return SUCCESS;
}

/****************************************************************************
 - Function name:	ProcessOrderDetails
 - Input:			Current index
 Current Date
 - Output:			N/A
 - Return:			void
 - Description:
 - Usage:			Process orders details at the current position
 ****************************************************************************/
int ProcessOrderDetails(int currentIndex, const char *currentDate)
{
	// Time
	int timeFrom;
	int timeTo;

	if (currentIndex > 0)
	{
		// Get time from timestamp collection
		timeFrom = TimeStampCollection[currentIndex - 1];
		timeTo = TimeStampCollection[currentIndex];
	}
	else
	{
		timeFrom = TimeStampCollection[currentIndex];
		timeTo = TimeStampCollection[currentIndex];
	}

	// Load Data from EAA database
	LoadDataFromEaaDbAtTime(timeFrom, timeTo, currentDate);

	return SUCCESS;
}

/****************************************************************************
 - Function name:	LoadDataFromEaaDbAtTime
 - Input:			timeFrom: start time range
 timeTo: end time range
 currentDate: the current date
 - Output:			N/A
 - Return:			void
 - Description:
 - Usage:			Load data from EAA Database (from t1 to t2)
 ****************************************************************************/
int LoadDataFromEaaDbAtTime(int timeFrom, int timeTo, const char *currentDate)
{
	// Create the conenction
	PGconn *connection;

	// Connect to EAA
	connection = CreateConnection(&EaaDatabaseConf);
	if (connection == NULL )
	{
		return ERROR;
	}

	// Querry Result
	PGresult *querryResult;
	PGresult *querryResultToGetExitLaunch;

	// Build querry string
	char querryCmd[MAX_QUERRY_LENGTH];

	// Temp variable
	char tmp[6];

	// Reset Querry String
	memset(querryCmd, 0, MAX_QUERRY_LENGTH);

	if (timeFrom < timeTo)
	{
		// Build Querry String
		sprintf(querryCmd,"SELECT date, ts_id as ts, oid, trim(ecn), time_stamp, trim(ask_bid), trim(entry_exit), trim(side), trim(symbol), upper(trim(stock_type)), trim(time_in_force), shares, left_shares, avg_price as price, trim(status), trim(ecn_launch), trade_type, routing_inst, cross_id, is_iso FROM new_orders WHERE ts_id <> -1 AND ((Cast(substr(time_stamp::text,1,2) as int) * 60 * 60) + (Cast(substr(time_stamp::text,4,2) as int) * 60) + Cast(substr(time_stamp::text,7,2) as int) >= %d) and ((Cast(substr(time_stamp::text,1,2) as int) * 60 * 60) + (Cast(substr(time_stamp::text,4,2) as int) * 60) + Cast(substr(time_stamp::text,7,2) as int) < %d) and date = '%s'", timeFrom, timeTo, currentDate);
	}
	else
	{
		// Build Querry String
		sprintf(querryCmd,"SELECT date, ts_id as ts, oid, trim(ecn), time_stamp, trim(ask_bid), trim(entry_exit), trim(side), trim(symbol), upper(trim(stock_type)), trim(time_in_force), shares, left_shares, avg_price as price, trim(status), trim(ecn_launch), trade_type, routing_inst, cross_id, trim(is_iso) FROM new_orders WHERE ts_id <> -1 AND ((Cast(substr(time_stamp::text,1,2) as int) * 60 * 60) + (Cast(substr(time_stamp::text,4,2) as int) * 60) + Cast(substr(time_stamp::text,7,2) as int) < %d) and date = '%s'", timeFrom, currentDate);
	}

	// Execute the querry
	querryResult = ExecuteQuery(connection, querryCmd);
	if (querryResult == NULL )
	{
		return ERROR;
	}

	// Get records
	NumOfRecords = GetNumberOfRow(querryResult);

	// record index
	int recordIndex;

	if (NumOfRecords > 0)
	{
		// Have some records
		for (recordIndex = 0; recordIndex < NumOfRecords; recordIndex++)
		{
			// Date
			strncpy(Orders[recordIndex].strDate, GetValueField(querryResult, recordIndex, 0), 12);

			// Trade Server ID
			Orders[recordIndex].tradeServer = atoi(GetValueField(querryResult, recordIndex, 1));
			
			// Cross ID
			Orders[recordIndex].crossId = atoi(GetValueField(querryResult, recordIndex, 18));
      //
      Orders[recordIndex].isoOrder = 0; 
      if( GetValueField(querryResult, recordIndex, 19)[0] == 'Y')
      {
        Orders[recordIndex].isoOrder = 1; 
      }

			// Client Order ID
			Orders[recordIndex].clOrdId = atoi(GetValueField(querryResult, recordIndex, 2));

			// ECN ID
			//Orders[recordIndex].ECN = GetValueField(querryResult,recordIndex,3)[0];
      char ecn[4];
      int ecnIndex = 0;
      Orders[recordIndex].ECN = ecnIndex;
      strcpy(ecn, GetValueField(querryResult,recordIndex,3));
      for (ecnIndex = 0; ecnIndex < NUM_OF_ECN; ecnIndex++)
      {
        if (strncmp(ecn, Ecn[ecnIndex], strlen(Ecn[ecnIndex])) == 0)
        {
          Orders[recordIndex].ECN = ecnIndex;
          break;
        }
      }
			// Date Time
			strncpy(Orders[recordIndex].strTime, GetValueField(querryResult, recordIndex, 4), 16);

			// Temp
			strcpy(tmp, GetValueField(querryResult, recordIndex, 6));

			if (tmp[1] == 'n')
			{
				// Entry/Exit Indicator
				Orders[recordIndex].entryExit = 1;

				// ASK/BID
				Orders[recordIndex].AskBid = ((GetValueField(querryResult, recordIndex, 5))[0] == 'A');

				// ECN Launch
				strcpy(ecn, GetValueField(querryResult, recordIndex, 15));
			}
			else if (tmp[1] == 'x')
			{
				Orders[recordIndex].entryExit = 0;
				
				if (Orders[recordIndex].tradeServer >= 0)
				{
					// Reset Querry String
					memset(querryCmd, 0, MAX_QUERRY_LENGTH);
					
					sprintf(querryCmd,"SELECT trim(ecn_launch), ask_bid FROM new_orders WHERE date = '%s' and entry_exit = 'Entry' and ts_id = %d and cross_id = %d LIMIT 1", currentDate, Orders[recordIndex].tradeServer, Orders[recordIndex].crossId);

					// Execute the querry
					querryResultToGetExitLaunch = ExecuteQuery(connection, querryCmd);
					if (querryResultToGetExitLaunch == NULL)
					{
						TraceLog(ERROR_LEVEL, "Cannot get ecnLaunch of Exit, ts_id = %d, crossId = %d\n", Orders[recordIndex].tradeServer, Orders[recordIndex].crossId);
						continue;
					}
					
					int numberOfRecord = GetNumberOfRow(querryResultToGetExitLaunch);
					if(numberOfRecord > 0)
					{
						strcpy(ecn, GetValueField(querryResultToGetExitLaunch, 0, 0));
					
						Orders[recordIndex].AskBid = ((GetValueField(querryResultToGetExitLaunch, 0, 1))[0] == 'A') ? 0 : 1;
					}
					else
					{
						TraceLog(ERROR_LEVEL, "Cannot get ecnLaunch of Exit, ts_id = %d, crossId = %d\n", Orders[recordIndex].tradeServer, Orders[recordIndex].crossId);
						continue;
					}
					
				}
			}
      Orders[recordIndex].ecnLaunch = 0;
      for (ecnIndex = 0; ecnIndex < NUM_OF_ECN_LAUNCH; ecnIndex++)
      {
        if (strncmp(ecn, EcnLaunch[ecnIndex], strlen(EcnLaunch[ecnIndex])) == 0)
        {
          Orders[recordIndex].ecnLaunch = ecnIndex;
          break;
        }
      }
      char side[3];
			// Buy, Sell Indicator
			strncpy(side, GetValueField(querryResult, recordIndex, 7), 2);
      Orders[recordIndex].side = 0;
			if((strcmp(side,"BO") == 0) || strcmp(side,"BC") == 0 )
			{
				Orders[recordIndex].side = B; //B
			}
			else if (strcmp(side, "SS") == 0)
			{
				Orders[recordIndex].side = SS; //SS
			}
			else if (strcmp(side, "S") == 0)
			{
				Orders[recordIndex].side = S; //S
			}
			else
			{
				TraceLog(ERROR_LEVEL, "Side %s is invalid\n", side);
				return 0;
			}

			// Stock Symbol
			strncpy(Orders[recordIndex].stockSymbol, GetValueField(querryResult, recordIndex, 8), 12);

			// Stock Type
			char stockType = GetValueField(querryResult, recordIndex, 9)[0];
      int stockTypesIndex = 0;
      Orders[recordIndex].stockType = NUM_OF_STOCK_TYPES;
      for(stockTypesIndex = 0; stockTypesIndex < NUM_OF_STOCK_TYPES; stockTypesIndex++)
			{
        if(stockType == StockType[stockTypesIndex])
        {
          Orders[recordIndex].stockType = stockTypesIndex;
          break;
        }
      }
			// Time In Force
      char timeInForce;
      int tifIndex;
			timeInForce = GetValueField(querryResult, recordIndex, 10)[0];
      Orders[recordIndex].timeInForce = NUM_OF_TIF;
      for(tifIndex = 0; tifIndex < NUM_OF_TIF; tifIndex++)
			{
				if(timeInForce == TimeInForce[tifIndex])
        {
          Orders[recordIndex].timeInForce = tifIndex;
          break;
        }
      }
			// Total shares
			Orders[recordIndex].shares = atoi(GetValueField(querryResult, recordIndex, 11));

			// Left shares
			Orders[recordIndex].leftShares = atoi(GetValueField(querryResult, recordIndex, 12));

			// Filled shares
			Orders[recordIndex].filledShares = Orders[recordIndex].shares - Orders[recordIndex].leftShares;

			// Price
			Orders[recordIndex].price = atof(GetValueField(querryResult, recordIndex, 13));

			// Status
			Orders[recordIndex].status = GetValueField(querryResult, recordIndex, 14)[0];

			// ECN Launch
			// strcpy(Orders[recordIndex].ecnLaunch, GetValueField(querryResult, recordIndex, 15));

			// ACK price
			// Orders[recordIndex].ackPrice = atof(GetValueField(querryResult, recordIndex, 16));

			// Trade Types
			Orders[recordIndex].tradeType = atoi(GetValueField(querryResult, recordIndex, 16));
			
			// Routing instruction
			strncpy(Orders[recordIndex].routingInst, GetValueField(querryResult, recordIndex, 17), 3);

			/*
			 printf("\n=============================================\n");
			 printf("Trade Server ID: %d\n", Orders[recordIndex].tradeServer);
			 printf("Order ID: %d\n", Orders[recordIndex].clOrdId);
			 printf("ECN: %c \n", Orders[recordIndex].ECN);
			 printf("stockType: %c \n", Orders[recordIndex].stockType);
			 printf("entryExit: %d \n", Orders[recordIndex].entryExit);
			 printf("AskBid: %c\n", Orders[recordIndex].AskBid);
			 printf("side: %s\n", Orders[recordIndex].side);
			 printf("timeInForce: %c\n", Orders[recordIndex].timeInForce);
			 printf("leftShares: %d\n", Orders[recordIndex].leftShares);
			 printf("filledShares: %d\n", Orders[recordIndex].filledShares);
			 printf("price: %lf\n", Orders[recordIndex].price);
			 printf("ackPrice: %lf\n", Orders[recordIndex].ackPrice);
			 printf("share: %d\n", Orders[recordIndex].shares);

			 printf("\n=============================================\n");
			 */
		}
	}
	
	DestroyConnection(connection);

	return SUCCESS;
}

/****************************************************************************
 - Function name:	InsertDataIntoTwt2Db
 - Input:			N/A
 - Output:			N/A
 - Return:			int
 - Description:
 - Usage:			Insert data into some tables on TWT2 Database
 ****************************************************************************/
int InsertDataIntoTwt2Db(int currentIndex, const char *currentDate)
{
	// Insert Entry/Exit Data Into TWT2 DB
	InsertEntryExitDataIntoTwt2Db(currentIndex, currentDate, 0);
	
	//Insert ExitOrder Shares data for each venue into on TWT2 Database
	InsertExitOrderSharesTradingDataIntoTwt2Db(currentIndex, currentDate, 0);
	
	// Insert Stock Types into DB
	// InsertStockTypesDataIntoTwt2Db(currentIndex, currentDate);

	// Insert Profit/Loss Into Database
	InsertProfitLossDataIntoTwt2Db(currentIndex, currentDate);

	// Insert Order Types Into TWT2 DB
	InsertOrderTypesDataIntoTwt2Db(currentIndex, currentDate, 0);

	// Insert Ask/Bid Launch
	InsertAskBidLaunchIntoTwt2Db(currentIndex, currentDate, 0);

	// Insert ASK and FILL into DB
	InsertAckFillIntoTwt2Db(currentIndex, currentDate, 0);

	return SUCCESS;

}
/****************************************************************************
 - Function name:	InsertDataIntoTwt2Db
 - Input:			N/A
 - Output:			N/A
 - Return:			int
 - Description:
 - Usage:			Insert data into some tables on TWT2 Database
 ****************************************************************************/
int InsertOfflineDataIntoTwt2Db(int currentIndex, const char *currentDate, int isOldData)
{
	// Insert Profit/Loss Into Database
	InsertOfflineProfitLossDataIntoTwt2Db(currentIndex, currentDate, isOldData);

	return SUCCESS;

}

/****************************************************************************
 - Function name:	InsertExitOrderSharesTradingDataIntoTwt2Db
 - Input:			+ current index
 - Output:			+ current date
 - Return:			int
 - Description:
 - Usage:			Insert ExitOrder Shares data for each venue into TWT2 Database
 ****************************************************************************/
 int InsertExitOrderSharesTradingDataIntoTwt2Db(int currentIndex, const char *currentDate, int isOldData)
 {
	// Querry String
	char querryCmd[MAX_QUERRY_LENGTH];

	// Reset querry string
	memset(querryCmd, 0, MAX_QUERRY_LENGTH);

	// Reset Entry/Exit Data
	memset(&ExitOrderShares, 0, sizeof(ExitOrderShares));

	if (currentIndex >= 0)
	{
		CalculateExitOrderSharesTrading();
	}

	// Create the connection to TWT2 Database
	PGconn *connection = CreateConnection(&Twt2DatabaseConf);
	if (connection == NULL )
	{
		return ERROR;
	}
	//char date[MIN_VALID_LENGTH];
	//char time[MIN_VALID_DATE_LENGTH];
	char date[11];
	char time[9];

	// Reset buffers
	memset(date, 0, MIN_VALID_LENGTH);
	memset(time, 0, MIN_VALID_DATE_LENGTH);

	// Convert
	ConvertToDateFormat(date, currentDate);
	ConvertToTimeFormat(TimeStampCollection[currentIndex] + NumOfSecsFrom1970, time, isOldData);
	int ecnIndex, tsIndex, ecnLaunchIndex, entryExitIndex, isoOrderIndex;
	for(tsIndex = 0; tsIndex < NUM_OF_TS; tsIndex++)
	{
		// include BATZ smart route
		for (ecnIndex = 0; ecnIndex < NUM_OF_ECN + 1; ecnIndex++)
		{
      for(entryExitIndex = 0; entryExitIndex < ENTRY_EXIT; entryExitIndex++)
      {
        for(ecnLaunchIndex = 0; ecnLaunchIndex < NUM_OF_ECN_LAUNCH; ecnLaunchIndex++)
        {
          for(isoOrderIndex = 0; isoOrderIndex < NUM_OF_ISO_ORDER; isoOrderIndex++)
          {          
            sprintf(querryCmd, "INSERT INTO summary_exit_order_shares (date,time_stamp,total_second,ts_id,ecn_id,entry_exit,ecn_launch,is_iso,shares_filled,order_size) VALUES ('%s','%s',%d,%d,%d,%d,%d,%d,%d,%d)",
              date,
              time, TimeStampCollection[currentIndex] + NumOfSecsFrom1970,
                 tsIndex,ecnIndex,entryExitIndex,ecnLaunchIndex,isoOrderIndex,ExitOrderShares[tsIndex][ecnIndex][entryExitIndex][ecnLaunchIndex][isoOrderIndex][SHARES_FILLED],
                 ExitOrderShares[tsIndex][ecnIndex][entryExitIndex][ecnLaunchIndex][isoOrderIndex][ORDER_SIZE]);
            PGresult *querryResult = ExecuteQuery(connection, querryCmd);
            if (querryResult == NULL )
            {
              TraceLog(ERROR_LEVEL, "We can not insert data to database!\n");
              return ERROR;
            }

            // Clear querry
            ClearResult(querryResult);

            // Reset Querry String
            memset(querryCmd, 0, MAX_QUERRY_LENGTH);
          }
        }
      }
		}
	}
	// Destroy the connection
	DestroyConnection(connection);
	return SUCCESS;
 }
 /****************************************************************************
 - Function name:	CalculateExitOrderSharesTrading
 - Input:			N/A
 - Output:			N/A
 - Return:			int
 - Description:
 - Usage:			Update Exit order shares trading to Data Structure
*****************************************************************************/
int CalculateExitOrderSharesTrading()
{
	int recordIndex, ecnExitIndex;
  PlacedOrder_t *order;
  
	for (recordIndex = 0; recordIndex < NumOfRecords; recordIndex++)
	{
		order = &Orders[recordIndex];
       
    ecnExitIndex = order->ECN;    
    // Calculate Exit_Success for BATZ Smart Route
    if ((ecnExitIndex == 4) && (strncmp(order->routingInst, BATZ_SMART_ROUTE, 3) == 0))
    {
      ecnExitIndex = NUM_OF_ECN;
    }
    
    ExitOrderShares[order->tradeServer][ecnExitIndex][order->entryExit][order->ecnLaunch][order->isoOrder][SHARES_FILLED] += order->filledShares;        
    ExitOrderShares[order->tradeServer][ecnExitIndex][order->entryExit][order->ecnLaunch][order->isoOrder][ORDER_SIZE] += order->shares;
	}
	return SUCCESS;
 }

/****************************************************************************
 - Function name:	InsertEntryExitDataIntoTwt2Db
 - Input:			+ current index
 - Output:			+ current date
 - Return:			int
 - Description:
 - Usage:			Insert entry/exit data into on TWT2 Database
 ****************************************************************************/
int InsertEntryExitDataIntoTwt2Db(int currentIndex, const char *currentDate, int isOldData)
{
	// Querry String
	char querryCmd[MAX_QUERRY_LENGTH];

	// Reset querry string
	memset(querryCmd, 0, MAX_QUERRY_LENGTH);

	// Reset Entry/Exit Data
	memset(&EntryExit, 0, sizeof(EntryExit));

	if (currentIndex >= 0)
	{
		CalculateEntryExitStatus();
	}

	// Create the connection to TWT2 Database
	PGconn *connection = CreateConnection(&Twt2DatabaseConf);
	if (connection == NULL )
	{
		return ERROR;
	}

	// Indexes
	int tsIndex, ecnIndex, sideIndex;

	//char date[MIN_VALID_LENGTH];
	//char time[MIN_VALID_DATE_LENGTH];
	char date[11];
	char time[9];

	// Reset buffers
	memset(date, 0, MIN_VALID_LENGTH);
	memset(time, 0, MIN_VALID_DATE_LENGTH);

	// Convert
	ConvertToDateFormat(date, currentDate);
	ConvertToTimeFormat(TimeStampCollection[currentIndex] + NumOfSecsFrom1970, time, isOldData);
	
	for (tsIndex = 0; tsIndex < NUM_OF_TS; tsIndex++)
	{
		// include BATZ smart route
		for (ecnIndex = 0; ecnIndex < NUM_OF_ECN + 1; ecnIndex++)
		{
			for (sideIndex = 0; sideIndex < NUM_OF_SIDE; sideIndex++)
			{
				/************
				 print to debug
				 *************/

				// Build the query string
				sprintf(querryCmd, "INSERT INTO summary_entry_exits (date,time_stamp,total_second,ts_id,ecn_id,entry_filled,entry_total,exit_filled,exit_total,side) VALUES ('%s','%s',%d,%d,%d,%d,%d,%d,%d,%d)",
					date,
					time, TimeStampCollection[currentIndex] + NumOfSecsFrom1970,
						tsIndex, ecnIndex,
						EntryExit[tsIndex][ecnIndex][sideIndex][ENTRY_SUCC],
					EntryExit[tsIndex][ecnIndex][sideIndex][ENTRY_MISS] + EntryExit[tsIndex][ecnIndex][sideIndex][ENTRY_SUCC],
						EntryExit[tsIndex][ecnIndex][sideIndex][EXIT_SUCC],
					EntryExit[tsIndex][ecnIndex][sideIndex][EXIT_MISS] + EntryExit[tsIndex][ecnIndex][sideIndex][EXIT_SUCC],sideIndex);

				PGresult *querryResult = ExecuteQuery(connection, querryCmd);

				if (querryResult == NULL )
				{
					TraceLog(ERROR_LEVEL, "We can not insert data to database!\n");
					return ERROR;
				}

				// Clear querry
				ClearResult(querryResult);

				// Reset Querry String
				memset(querryCmd, 0, MAX_QUERRY_LENGTH);
			}
		}
	}

	// Destroy the connection
	DestroyConnection(connection);

	return SUCCESS;
}
/****************************************************************************
 - Function name:	CalculateEntryExitStatus
 - Input:			N/A
 - Output:			N/A
 - Return:			int
 - Description:
 - Usage:			Update Entry Exit Status to Data Structure
*****************************************************************************/
int CalculateEntryExitStatus()
{
	// Indexes
	int  ecnIndex, statusIndex, recordIndex;
  PlacedOrder_t *order;
  
	// Visit all records
	for (recordIndex = 0; recordIndex < NumOfRecords; recordIndex++)
	{
    order = &Orders[recordIndex];
    ecnIndex = order->ECN;
    
    // Calculate Exit_Success for BATZ Smart Route
    if ((ecnIndex == 4) && (strncmp(order->routingInst, BATZ_SMART_ROUTE, 3) == 0))
    {
      ecnIndex = NUM_OF_ECN;
    }
    
    statusIndex = ENTRY_MISS;
    if (order->entryExit == ENTRY)
		{
      statusIndex = ENTRY_MISS;
      if (order->filledShares > 0)
			{
				statusIndex = ENTRY_SUCC;
			}
    }
    else
    {
      statusIndex = EXIT_MISS;
      if (order->filledShares > 0)
			{
				statusIndex = EXIT_SUCC;
			}
    }
    EntryExit[order->tradeServer][ecnIndex][order->side][statusIndex] += 1;
	}
	return SUCCESS;
}
/****************************************************************************
 - Function name:	InsertStockTypesDataIntoTwt2Db
 - Input:			+ current index
 - Output:			+ current date
 - Return:			int
 - Description:
 - Usage:			Insert stock types data into on TWT2 Database
 ****************************************************************************/
int InsertStockTypesDataIntoTwt2Db(int currentIndex, const char *currentDate, int isOldData)
{
	// Querry String
	char querryCmd[MAX_QUERRY_LENGTH];

	// Reset querry string
	memset(querryCmd, 0, MAX_QUERRY_LENGTH);

	// Reset Entry/Exit Data
	memset(&StockTypes, 0, sizeof(StockTypes));

	if (currentIndex >= 0)
	{
		CalculateStockTypesStatus();
	}

	// Create the connection to TWT2 Database
	PGconn *connection = CreateConnection(&Twt2DatabaseConf);
	if (connection == NULL )
	{
		return ERROR;
	}
	char date[MIN_VALID_LENGTH];
	char time[MIN_VALID_DATE_LENGTH];

	// Reset buffers
	memset(date, 0, MIN_VALID_LENGTH);
	memset(time, 0, MIN_VALID_DATE_LENGTH);

	// Convert
	ConvertToDateFormat(date, currentDate);
	ConvertToTimeFormat(TimeStampCollection[currentIndex] + NumOfSecsFrom1970, time, isOldData);
	// Indexes
	int tsIndex, ecnIndex, stockTypesIndex, tifIndex, sideIndex;

	for (tsIndex = 0; tsIndex < NUM_OF_TS; tsIndex++)
	{
		for (ecnIndex = 0; ecnIndex < NUM_OF_ECN; ecnIndex++)
		{
			for (sideIndex = 0; sideIndex < NUM_OF_SIDE; sideIndex++)
			{
				for(stockTypesIndex = 0; stockTypesIndex < NUM_OF_STOCK_TYPES; stockTypesIndex++)
				{
					char *stockType = NULL;
					if (stockTypesIndex == ETF)
					{
						stockType = "ETF";
					}
					else if (stockTypesIndex == LISTED)
					{
						stockType = "LISTED";
					}
					else
					{
						stockType = "OTC";
					}

					for (tifIndex = 0; tifIndex < NUM_OF_TIF; tifIndex++)
					{
						char *tif = NULL;
						if (tifIndex == IOC)
						{
							tif = "IOC";
						}
						else if (tifIndex == GTC)
						{
							tif = "GTC";
						}
						else if (tifIndex == DAY)
						{
							tif = "DAY";
						}
						sprintf(querryCmd, "INSERT INTO summary_stock_types (date,time_stamp,total_second,ts_id,ecn_id,stock_type,tif,filled,total,side) VALUES ('%s','%s',%d,%d,%d,'%s','%s',%d,%d,%d)",
							date,
							time, TimeStampCollection[currentIndex] + NumOfSecsFrom1970,
							tsIndex, ecnIndex,
								stockType, tif,
								StockTypes[tsIndex][ecnIndex][sideIndex][stockTypesIndex][tifIndex][SUCC],
								StockTypes[tsIndex][ecnIndex][sideIndex][stockTypesIndex][tifIndex][SUCC] +
								StockTypes[tsIndex][ecnIndex][sideIndex][stockTypesIndex][tifIndex][MISS],sideIndex);

						PGresult *querryResult = ExecuteQuery(connection, querryCmd);

						if (querryResult == NULL )
						{
							TraceLog(ERROR_LEVEL, "We can not insert summary_stock_types data to database!\n");
							return ERROR;
						}

						// Clear querry
						ClearResult(querryResult);

						// Reset Querry String
						memset(querryCmd, 0, MAX_QUERRY_LENGTH);
					}
				}
			}
		}
	}

	// Destroy the connection
	DestroyConnection(connection);

	return SUCCESS;

}
/****************************************************************************
 - Function name:	CalculateStockTypesStatus
 - Input:			N/A
 - Output:			N/A
 - Return:			int
 - Description:
 - Usage:			Update some statistics about stock types
 + ETF
 + Listed
 + OTC
 + ...
 ****************************************************************************/
int CalculateStockTypesStatus()
{
	// Record index
	int recordIndex, statusIndex,ecnIndex;
  PlacedOrder_t *order;

	for (recordIndex = 0; recordIndex < NumOfRecords; recordIndex++)
	{
    order = &Orders[recordIndex];
    ecnIndex = order->ECN;
    
    // Calculate Exit_Success for BATZ Smart Route
    if ((ecnIndex == 4) && (strncmp(order->routingInst, BATZ_SMART_ROUTE, 3) == 0))
    {
      ecnIndex = NUM_OF_ECN;
    }
    
    statusIndex = MISS;
    if (order->filledShares > 0)
    {
      statusIndex = SUCC;
    }
    
    StockTypes[order->tradeServer][ecnIndex][order->side][order->stockType][order->timeInForce][statusIndex] += 1;    
  }
	return SUCCESS;
}
/****************************************************************************
 - Function name:	InsertProfitLossDataIntoTwt2Db
 - Input:			N/A
 - Output:			N/A
 - Return:			int
 - Description:
 - Usage:			Insert Profit/Loss data into some tables on TWT2 Database
 ****************************************************************************/
int InsertProfitLossDataIntoTwt2Db(int currentIndex, const char *currentDate)
{
	// Querry String
	char querryCmd[MAX_QUERRY_LENGTH];

	// Reset querry string
	memset(querryCmd, ' ', MAX_QUERRY_LENGTH);

	// Gross Profit/Loss
	double grossProfitLoss = 0.0;
	double profitLossPerSt = 0.0;

	// Expected profit
	double expectedProfit = 0.0;
	double expectedProfitPerSt = 0.0;

	// Net profit/Loss
	double totalFee = 0.0;
	double netProfitLoss = 0.0;
	double netProfitLossPerSt = 0.0;

	// total volume
	int totalVolume = 0;
	int totalVolumePerSt = 0;

	// Market value
	double marketValue;
	double marketValuePerSt;

	// Number of records
	int numRecords;

	// Connection
	PGconn *eaaConnection;

	// Create the connection to EAA DB
	eaaConnection = CreateConnection(&EaaDatabaseConf);
	if (eaaConnection == NULL )
	{
		return ERROR;
	}

	// Query Result
	PGresult *qResult;

	/*************
	 GET Gross PL
	 GET Total volume
	 **************/

	// Build the query string
	sprintf(querryCmd,
			"Select sum(total_quantity) as shares, sum(matched_profit_loss) as matchedPL, sum(total_fee) as totalFee from risk_managements Where date = '%s'",
			LastProcessedDate);

	// Execute the query
	qResult = ExecuteQuery(eaaConnection, querryCmd);

	if (qResult == NULL )
	{
		return ERROR;
	}

	// get the number of records
	numRecords = GetNumberOfRow(qResult);

	if (numRecords > 0)
	{
		//	Total Shares
		totalVolume = atoi(GetValueField(qResult, 0, 0));

		// Calculate the total volume in current step
		totalVolumePerSt = totalVolume - prevProfitLoss.totalVolume;

		// Hold the previous value of total volume
		prevProfitLoss.totalVolume = totalVolume;

		//	Total Matched PL
		grossProfitLoss = atof(GetValueField(qResult, 0, 1));

		// Calculate profit/loss in the current step
		profitLossPerSt = grossProfitLoss - prevProfitLoss.profitLoss;

		// Calculate net profit/Loss in the current step
		totalFee = atof(GetValueField(qResult, 0, 2));
		netProfitLoss = grossProfitLoss - totalFee;
		netProfitLossPerSt = netProfitLoss - prevProfitLoss.netProfitLoss;

		// Hold the previous value of profit/loss
		prevProfitLoss.profitLoss = grossProfitLoss;
		prevProfitLoss.netProfitLoss = netProfitLoss;
	}
	else
	{
		TraceLog(DEBUG_LEVEL, "We can not process the Profit/Loss\n");
		exit(0);
	}

	// Clear the result
	ClearResult(qResult);

	/*********************
	 GET Total Market Value
	 *********************/
	// Reset the query command
	memset(querryCmd, ' ', MAX_QUERRY_LENGTH);

	// Build the query string
	sprintf(querryCmd,"Select sum(total_market_value) as market from risk_managements Where date = '%s' and net_quantity <> 0", LastProcessedDate);	

	//TraceLog(DEBUG_LEVEL, "querryCmd = %s\n", querryCmd);

	// Execute the query
	qResult = ExecuteQuery(eaaConnection, querryCmd);
	if (qResult == NULL )
	{
		return ERROR;
	}

	// get the number of records
	numRecords = GetNumberOfRow(qResult);

	if (numRecords > 0)
	{
		// Have some records

		// Market value
		marketValue = atof(GetValueField(qResult, 0, 0));

		// Calculate market value in the current step
		marketValuePerSt = marketValue - prevProfitLoss.totalMarket;

		//Save total market for next calculation
		prevProfitLoss.totalMarket = marketValue;
	}
	else
	{
		TraceLog(DEBUG_LEVEL, "We can not process the PL\n");
		exit(0);
	}

	// Clear the result
	ClearResult(qResult);
	
	/*********************
	 GET Expected Profit
	 *********************/
	// Reset the query command
	memset(querryCmd, ' ', MAX_QUERRY_LENGTH);

	// Build the query string
	sprintf(querryCmd, "SELECT SUM(expected_pl) FROM trade_results WHERE date = '%s'", LastProcessedDate);	

	//TraceLog(DEBUG_LEVEL, "querryCmd = %s\n", querryCmd);

	// Execute the query
	qResult = ExecuteQuery(eaaConnection, querryCmd);
	if (qResult == NULL )
	{
		return ERROR;
	}

	// get the number of records
	numRecords = GetNumberOfRow(qResult);

	if (numRecords > 0)
	{
		// Expected profit
		expectedProfit = atof(GetValueField(qResult, 0, 0));

		// Calculate expected profit in the current step
		expectedProfitPerSt = expectedProfit - prevProfitLoss.expectedProfit;

		//Save expected profit for next calculation
		prevProfitLoss.expectedProfit = expectedProfit;
	}
	else
	{
		TraceLog(DEBUG_LEVEL, "We can not process the PL\n");
		exit(0);
	}

	// Clear the result
	ClearResult(qResult);

	// Destroy EAA DB connection
	DestroyConnection(eaaConnection);

	/************
	 INSERT Data into TWT2 DB
	 **************/
	// Reset the query command
	memset(querryCmd, ' ', MAX_QUERRY_LENGTH);
	
	PGconn *twt2Connection;

	//Create the connection to TWT2 DB
	twt2Connection = CreateConnection(&Twt2DatabaseConf);
	if (twt2Connection == NULL )
	{
		return ERROR;
	}

	// Process date time
	char date[MIN_VALID_LENGTH];
	char time[MIN_VALID_DATE_LENGTH];

	// Reset buffers
	memset(date, 0, MIN_VALID_LENGTH);
	memset(time, 0, MIN_VALID_DATE_LENGTH);

	// Convert
	ConvertToDateFormat(date, currentDate);
	ConvertToTimeFormat(TimeStampCollection[currentIndex] + NumOfSecsFrom1970, time, 0);

	// Build the query string
	sprintf(querryCmd, "INSERT INTO summary_profit_losses(date,time_stamp,total_second,gross_profit_loss,volume,market_value,net_profit_loss,expected_profit) VALUES('%s','%s',%d,%lf,%d,%lf,%lf,%lf)",
			date, time, TimeStampCollection[currentIndex] + NumOfSecsFrom1970,
			profitLossPerSt, totalVolumePerSt, marketValuePerSt, netProfitLossPerSt, expectedProfitPerSt);

	// Execute the query
	qResult = ExecuteQuery(twt2Connection, querryCmd);

	// Check if the query is not NULL
	if (qResult == NULL )
	{
		TraceLog(ERROR_LEVEL, "We can not insert profit/loss to database!\n");
		return ERROR;
	}

	// Clear querry
	ClearResult(qResult);

	// Destroy TWT2 DB connection
	DestroyConnection(twt2Connection);

	return SUCCESS;
}

/****************************************************************************
 - Function name:	InsertOfflineProfitLossDataIntoTwt2Db
 - Input:			N/A
 - Output:			N/A
 - Return:			int
 - Description:
 - Usage:			Insert Profit/Loss data into some tables on TWT2 Database
 ****************************************************************************/
int InsertOfflineProfitLossDataIntoTwt2Db(int currentIndex, const char *currentDate, int isOldData)
{
	// Create data for begin of day
	CreateDataForBeginOfDay(currentDate);

	// Querry String
	char querryCmd[MAX_QUERRY_LENGTH];

	// Reset querry string
	memset(querryCmd, ' ', MAX_QUERRY_LENGTH);

	// Connection
	PGconn *eaaConnection;

	// Create the connection to EAA DB
	eaaConnection = CreateConnection(&EaaDatabaseConf);
	if (eaaConnection == NULL )
	{
		return ERROR;
	}

	// TWT2 connection
	PGconn *twt2Connection;

	//Create the connection to TWT2 DB
	twt2Connection = CreateConnection(&Twt2DatabaseConf);
	if (twt2Connection == NULL )
	{
		return ERROR;
	}

	// Query Result
	PGresult *qResult;
	PGresult *iqResult;

	// Number of records
	int numRecords;

	// Indexes of records
	int recordIndex;

	int timeBegin;	
	
	char querryCmdString[MAX_QUERRY_LENGTH];
	char dateString[MIN_VALID_LENGTH];
	char timeString[MIN_VALID_DATE_LENGTH];

	// Reset buffers
	memset(dateString, 0, MIN_VALID_LENGTH);
	memset(timeString, 0, MIN_VALID_DATE_LENGTH);
	memset(querryCmdString, 0, MAX_QUERRY_LENGTH);
	
	if(currentIndex == 0)
	{
		ConvertToTimeFormat(TimeStampCollection[currentIndex] + NumOfSecsFrom1970, timeString, isOldData);
		timeBegin = TimeStampCollection[currentIndex];

	}
	else
	{
		ConvertToTimeFormat(TimeStampCollection[currentIndex + 1] + NumOfSecsFrom1970, timeString, isOldData);
		LastProcessedIndex = LastProcessedIndex + 1;
		timeBegin = TimeStampCollection[currentIndex];
	}
	// Convert
	ConvertToDateFormat(dateString, currentDate);
	
	// Build the query string to delete old data was inserted into summary_profit_losses table at previous time
	sprintf(querryCmdString,
			"DELETE FROM summary_profit_losses WHERE date = '%s' and time_stamp >= '%s'",
			dateString, timeString);
	iqResult = ExecuteQuery(twt2Connection, querryCmdString);
	
	if (iqResult == NULL )
	{
		TraceLog(ERROR_LEVEL, "We can not delete data in summary_profit_losses table!\n");
		return ERROR;
	}
	ClearResult(iqResult);
	
	memset(querryCmdString, 0, MAX_QUERRY_LENGTH);
	
	// Build the query string to delete old data was inserted into summary_entry_exits table at previous time
	sprintf(querryCmdString,
			"DELETE FROM summary_entry_exits WHERE date = '%s' and time_stamp >= '%s'",
			dateString, timeString);

	iqResult = ExecuteQuery(twt2Connection, querryCmdString);
	if (iqResult == NULL )
	{
		TraceLog(ERROR_LEVEL, "We can not delete data in summary_entry_exits table!\n");
		return ERROR;
	}

	ClearResult(iqResult);
	
	memset(querryCmdString, 0, MAX_QUERRY_LENGTH);
	
	// Build the query string to delete old data was inserted into summary_exit_order_shares table at previous time
	sprintf(querryCmdString,
			"DELETE FROM summary_exit_order_shares WHERE date = '%s' and time_stamp >= '%s'",
			dateString, timeString);

	iqResult = ExecuteQuery(twt2Connection, querryCmdString);
	if (iqResult == NULL )
	{
		TraceLog(ERROR_LEVEL, "We can not delete data in summary_exit_order_shares table!\n");
		return ERROR;
	}
	ClearResult(iqResult);
	
	memset(querryCmdString, 0, MAX_QUERRY_LENGTH);
	
	//  Build the query string to delete old data was inserted into summary_order_types table at previous time
	sprintf(querryCmdString,
			"DELETE FROM summary_order_types WHERE date = '%s' and time_stamp >= '%s'",
			dateString, timeString);
	iqResult = ExecuteQuery(twt2Connection, querryCmdString);

	if (iqResult == NULL )
	{
		TraceLog(ERROR_LEVEL, "We can not delete data in summary_order_types table!\n");
		return ERROR;
	}
	ClearResult(iqResult);
	
	memset(querryCmdString, 0, MAX_QUERRY_LENGTH);
	
	//  Build the query string to delete old data was inserted into summary_launch_entry_ecns table at previous time
	sprintf(querryCmdString,
			"DELETE FROM summary_launch_entry_ecns WHERE date = '%s' and time_stamp >= '%s'",
			dateString, timeString);
	iqResult = ExecuteQuery(twt2Connection, querryCmdString);

	if (iqResult == NULL )
	{
		TraceLog(ERROR_LEVEL, "We can not delete data in summary_launch_entry_ecns table!\n");
		return ERROR;
	}
	ClearResult(iqResult);
	
	memset(querryCmdString, 0, MAX_QUERRY_LENGTH);
	
	//  Build the query string to delete old data was inserted into summary_ack_fills_60 table at previous time
	sprintf(querryCmdString,
			"DELETE FROM summary_ack_fills_60 WHERE date = '%s' and time_stamp >= '%s'",
			dateString, timeString);
	iqResult = ExecuteQuery(twt2Connection, querryCmdString);

	if (iqResult == NULL )
	{
		TraceLog(ERROR_LEVEL, "We can not delete data in summary_ack_fills_60 table!\n");
		return ERROR;
	}
	ClearResult(iqResult);
	
	memset(querryCmdString, 0, MAX_QUERRY_LENGTH);
	
	//  Build the query string to delete old data was inserted into summary_ack_fills_30 table at previous time
	sprintf(querryCmdString,
			"DELETE FROM summary_ack_fills_30 WHERE date = '%s' and time_stamp >= '%s'",
			dateString, timeString);
	iqResult = ExecuteQuery(twt2Connection, querryCmdString);

	if (iqResult == NULL )
	{
		TraceLog(ERROR_LEVEL, "We can not delete data in summary_ack_fills_30 table!\n");
		return ERROR;
	}
	ClearResult(iqResult);
	
	memset(querryCmdString, 0, MAX_QUERRY_LENGTH);
	
	//  Build the query string to delete old data was inserted into summary_ack_fills_15 table at previous time
	sprintf(querryCmdString,
			"DELETE FROM summary_ack_fills_15 WHERE date = '%s' and time_stamp >= '%s'",
			dateString, timeString);
	iqResult = ExecuteQuery(twt2Connection, querryCmdString);

	if (iqResult == NULL )
	{
		TraceLog(ERROR_LEVEL, "We can not delete data in summary_ack_fills_15 table!\n");
		return ERROR;
	}
	ClearResult(iqResult);
	
	memset(querryCmdString, 0, MAX_QUERRY_LENGTH);
	
	//  Build the query string to delete old data was inserted into summary_ack_fills table at previous time
	sprintf(querryCmdString,
			"DELETE FROM summary_ack_fills WHERE date = '%s' and time_stamp >= '%s'",
			dateString, timeString);
	iqResult = ExecuteQuery(twt2Connection, querryCmdString);

	if (iqResult == NULL )
	{
		TraceLog(ERROR_LEVEL, "We can not delete data in summary_ack_fills table!\n");
		return ERROR;
	}
	ClearResult(iqResult);

	/*************
	 GET Gross PL
	 GET Total volume
	 **************/

	// Build the query string
	sprintf(querryCmd,
			"SELECT * FROM "
					"((SELECT trim(new_orders.symbol), trim(new_orders.side), fills.shares, fills.price, brokens.new_price, "
					"(Cast(substr(fills.time_stamp::text,1,2) as int) * 60 * 60) + (Cast(substr(fills.time_stamp::text,4,2) as int) * 60) "
					"+ Cast(substr(fills.time_stamp::text,7,2) as int) as time_stamp, new_orders.ts_id, new_orders.ecn, fills.liquidity as liquidity "
					"FROM fills "
					"JOIN new_orders ON fills.oid = new_orders.oid "
					"LEFT JOIN brokens ON fills.oid = brokens.oid AND fills.exec_id = brokens.exec_id "
					"WHERE (brokens.oid IS NULL OR (brokens.oid IS NOT NULL AND brokens.new_shares > 0)) "
					"AND ((Cast(substr(fills.time_stamp::text,1,2) as int) * 60 * 60) "
					"+ (Cast(substr(fills.time_stamp::text,4,2) as int) * 60) + Cast(substr(fills.time_stamp::text,7,2) as int)) >= %d "
					"AND date = '%s' ORDER BY fills.time_stamp)"

					"UNION ALL "

					"(SELECT trim(new_orders.symbol), trim(new_orders.side), cancel_pendings.shares, cancel_pendings.price, brokens.new_price,"
					"(Cast(substr(cancel_pendings.time_stamp::text,1,2) as int) * 60 * 60) "
					"+ (Cast(substr(cancel_pendings.time_stamp::text,4,2) as int) * 60) "
					"+ Cast(substr(cancel_pendings.time_stamp::text,7,2) as int) as time_stamp, new_orders.ts_id, new_orders.ecn, '' as liquidity "
					"FROM cancel_pendings "
					"JOIN new_orders ON cancel_pendings.oid = new_orders.oid "
					"LEFT JOIN brokens ON cancel_pendings.oid = brokens.oid AND cancel_pendings.exec_id = brokens.exec_id "
					"WHERE (brokens.oid IS NULL OR (brokens.oid IS NOT NULL AND brokens.new_shares > 0)) "
					"AND ((Cast(substr(cancel_pendings.time_stamp::text,1,2) as int) * 60 * 60) "
					"+ (Cast(substr(cancel_pendings.time_stamp::text,4,2) as int) * 60) "
					"+ Cast(substr(cancel_pendings.time_stamp::text,7,2) as int)) >= %d "
					"AND date = '%s' ORDER BY cancel_pendings.time_stamp)) AS temp "
					"ORDER BY temp.time_stamp ", timeBegin, LastProcessedDate,
			timeBegin, LastProcessedDate);

	//printf("querryCmd:\n%s\n",querryCmd);
	TraceLog(DEBUG_LEVEL, "Processing data on date: %s\n", LastProcessedDate);

	// Execute the query
	qResult = ExecuteQuery(eaaConnection, querryCmd);
	if (qResult == NULL )
	{
		return ERROR;
	}

	// get the number of records
	numRecords = GetNumberOfRow(qResult);

	int maxIndex;
	int CurrentProcessedIndex;
	double totalFee = 0.0;

	if (numRecords > 0)
	{
		//Get maximum index
		maxIndex = (atoi(GetValueField(qResult, numRecords - 1, 5)) - BeginTradingTimeInSeconds) / NUM_SECONDS_PER_PRIMETIVE_STEP + 1;

		// Init Record Index
		recordIndex = 0;
		int tCurrentIndex = (atoi(GetValueField(qResult, recordIndex, 5))- BeginTradingTimeInSeconds) / NUM_SECONDS_PER_PRIMETIVE_STEP;
		// Have some records
		for (CurrentProcessedIndex = LastProcessedIndex; CurrentProcessedIndex <= maxIndex; CurrentProcessedIndex++)
		{
			
			while (tCurrentIndex < CurrentProcessedIndex)
			{
				double price;

				// Calculate
				// current price
				if (atof(GetValueField(qResult, recordIndex, 4)) != 0)
				{
					price = atof(GetValueField(qResult, recordIndex, 4));
				}
				else
				{
					price = atof(GetValueField(qResult, recordIndex, 3));
				}

				// Get share
				int shares = atoi(GetValueField(qResult, recordIndex, 2));

				// Get symbol index
				int symbolIndex = GetSymbolIndex(GetValueField(qResult, recordIndex, 0));

				char ecn[5];
				memcpy(ecn, (char *) GetValueField(qResult, recordIndex, 7), 4);
				ecn[4] = 0;

				char liquidity[3];
				strncpy(liquidity, GetValueField(qResult, recordIndex, 8), 2);
				liquidity[2] = 0;

				if (symbolIndex == -1)
				{
					return ERROR;
				}

				char side = ((char *) GetValueField(qResult, recordIndex, 1))[0];
				if (strlen(liquidity) != 0)
				{
					totalFee += CalculateFee(price, shares, side, liquidity, ecn);
				}
				// CHECK LONG/SHORT
				if(SymbolTradinInfoList.TradinInfoList[symbolIndex].leftShares > 0)
				{
					// LONG
					if(strncmp(GetValueField(qResult, recordIndex, 1), "B", 1) == 0)
					{
						// BUY
						SymbolTradinInfoList.TradinInfoList[symbolIndex].avgPrice = (SymbolTradinInfoList.TradinInfoList[symbolIndex].avgPrice * SymbolTradinInfoList.TradinInfoList[symbolIndex].leftShares + price * shares) /
																		(shares + SymbolTradinInfoList.TradinInfoList[symbolIndex].leftShares);
						SymbolTradinInfoList.TradinInfoList[symbolIndex].leftShares +=	shares;
					}
					else
					{
						// SELL
						SymbolTradinInfoList.TradinInfoList[symbolIndex].profitLoss += (price - SymbolTradinInfoList.TradinInfoList[symbolIndex].avgPrice)  * MIN(SymbolTradinInfoList.TradinInfoList[symbolIndex].leftShares, shares);
						SymbolTradinInfoList.TradinInfoList[symbolIndex].leftShares -=	shares;

						if (SymbolTradinInfoList.TradinInfoList[symbolIndex].leftShares < 0)
						{
							SymbolTradinInfoList.TradinInfoList[symbolIndex].avgPrice = price;
						}
						else if (SymbolTradinInfoList.TradinInfoList[symbolIndex].leftShares == 0)
						{
							SymbolTradinInfoList.TradinInfoList[symbolIndex].avgPrice = 0;
						}
					}
				}

				else if (SymbolTradinInfoList.TradinInfoList[symbolIndex].leftShares < 0)
				{
					// SHORT
					if (strncmp(GetValueField(qResult, recordIndex, 1), "B", 1) == 0)
					{
						SymbolTradinInfoList.TradinInfoList[symbolIndex].profitLoss += (SymbolTradinInfoList.TradinInfoList[symbolIndex].avgPrice - price)  * MIN(abs(SymbolTradinInfoList.TradinInfoList[symbolIndex].leftShares), shares);
						SymbolTradinInfoList.TradinInfoList[symbolIndex].leftShares += shares;

						if (SymbolTradinInfoList.TradinInfoList[symbolIndex].leftShares > 0)
						{
							SymbolTradinInfoList.TradinInfoList[symbolIndex].avgPrice = price;
						}
						else if (SymbolTradinInfoList.TradinInfoList[symbolIndex].leftShares == 0)
						{
							SymbolTradinInfoList.TradinInfoList[symbolIndex].avgPrice = 0;
						}
					}
					else
					{
						// SELL
						SymbolTradinInfoList.TradinInfoList[symbolIndex].avgPrice = (SymbolTradinInfoList.TradinInfoList[symbolIndex].avgPrice * abs(SymbolTradinInfoList.TradinInfoList[symbolIndex].leftShares) + price * shares) /
																		(shares + abs(SymbolTradinInfoList.TradinInfoList[symbolIndex].leftShares));
						SymbolTradinInfoList.TradinInfoList[symbolIndex].leftShares -=	shares;
					}
				}
				else
				{
					SymbolTradinInfoList.TradinInfoList[symbolIndex].avgPrice = price;

					if (strncmp(GetValueField(qResult, recordIndex, 1), "B", 1) == 0)
					{
						SymbolTradinInfoList.TradinInfoList[symbolIndex].leftShares = shares;
					}
					else
					{
						SymbolTradinInfoList.TradinInfoList[symbolIndex].leftShares = -shares;

					}
				}

				SymbolTradinInfoList.TradinInfoList[symbolIndex].totalMarket = SymbolTradinInfoList.TradinInfoList[symbolIndex].avgPrice * abs(SymbolTradinInfoList.TradinInfoList[symbolIndex].leftShares);
				SymbolTradinInfoList.TradinInfoList[symbolIndex].totalVolume += shares;

				// Increase recode index
				recordIndex++;

				// int idx;
				if (recordIndex == numRecords)
				{
					/*
					 for(idx = 0; idx < SymbolTradinInfoList.countSymbol; idx++)
					 {
					 printf("%.8s,%lf\n", SymbolTradinInfoList.TradinInfoList[idx].symbol, SymbolTradinInfoList.TradinInfoList[idx].profitLoss);
					 }
					 */
					break;
				}
				tCurrentIndex = (atoi(GetValueField(qResult, recordIndex, 5)) - BeginTradingTimeInSeconds)	/ NUM_SECONDS_PER_PRIMETIVE_STEP;
			}

			// Insert data
			// Calculate
			int i;

			// Gross Profit/Loss
			double grossProfitLoss = 0.0;
			double profitLossPerSt = 0.0;

			// Net profit/loss
			double netProfitLoss = 0.0;
			double netProfitLossPerSt = 0.0;

			// total volume
			int totalVolume = 0;
			int totalVolumePerSt = 0;

			// Market value
			double marketValue = 0.0;
			double marketValuePerSt = 0.0;

			for (i = 0; i < SymbolTradinInfoList.countSymbol; i++)
			{
				grossProfitLoss += SymbolTradinInfoList.TradinInfoList[i].profitLoss;
				totalVolume += SymbolTradinInfoList.TradinInfoList[i].totalVolume;
				marketValue +=SymbolTradinInfoList.TradinInfoList[i].totalMarket;
			}

			netProfitLoss = grossProfitLoss - totalFee;

			// Calculate the total volume in current step
			totalVolumePerSt = totalVolume - prevProfitLoss.totalVolume;
			prevProfitLoss.totalVolume = totalVolume;

			// Calculate profit/loss in the current step
			profitLossPerSt = grossProfitLoss - prevProfitLoss.profitLoss;
			prevProfitLoss.profitLoss = grossProfitLoss;

			netProfitLossPerSt = netProfitLoss - prevProfitLoss.netProfitLoss;
			prevProfitLoss.netProfitLoss = netProfitLoss;

			// Calculate market value in the current step
			marketValuePerSt = marketValue - prevProfitLoss.totalMarket;
			prevProfitLoss.totalMarket = marketValue;

			/************
			 INSERT Data into TWT2 DB
			 **************/

			// Process date time
			char date[MIN_VALID_LENGTH];
			char time[MIN_VALID_DATE_LENGTH];

			// Reset buffers
			memset(date, 0, MIN_VALID_LENGTH);
			memset(time, 0, MIN_VALID_DATE_LENGTH);
			memset(querryCmd, 0, MAX_QUERRY_LENGTH);
			// Convert
			ConvertToDateFormat(date, currentDate);
			ConvertToTimeFormat(TimeStampCollection[CurrentProcessedIndex] + NumOfSecsFrom1970, time, isOldData);

			// Build the query string
			sprintf(querryCmd,
					"INSERT INTO summary_profit_losses(date,time_stamp,total_second,gross_profit_loss,volume,market_value,net_profit_loss) VALUES('%s','%s',%d,%lf,%d,%lf,%lf)",
					date, time,	TimeStampCollection[CurrentProcessedIndex] + NumOfSecsFrom1970, profitLossPerSt,
					totalVolumePerSt, marketValuePerSt, netProfitLossPerSt);

			// Execute the query
			iqResult = ExecuteQuery(twt2Connection, querryCmd);

			// Check if the query is not NULL
			if (iqResult == NULL )
			{
				TraceLog(ERROR_LEVEL, "We can not insert profit/loss to database!\n");
				return ERROR;
			}
			
			// Clear querry
			ClearResult(iqResult);

			// Memset orders before update
			memset(Orders, 0, sizeof(PlacedOrder_t));
			ProcessOrderDetails(CurrentProcessedIndex, currentDate);

			// Insert Entry/Exit Data Into TWT2 DB
			InsertEntryExitDataIntoTwt2Db(CurrentProcessedIndex, currentDate, isOldData);

			//Insert ExitOrder Shares data for each venue into on TWT2 Database
			InsertExitOrderSharesTradingDataIntoTwt2Db(CurrentProcessedIndex, currentDate, isOldData);
			
			// Insert Stock Types into DB
			// InsertStockTypesDataIntoTwt2Db(CurrentProcessedIndex, currentDate);

			// Insert Order Types Into TWT2 DB
			InsertOrderTypesDataIntoTwt2Db(CurrentProcessedIndex, currentDate, isOldData);

			// Insert Ask/Bid Launch
			InsertAskBidLaunchIntoTwt2Db(CurrentProcessedIndex, currentDate, isOldData);

			// Insert ASK and FILL into DB
			InsertAckFillIntoTwt2Db(CurrentProcessedIndex, currentDate, isOldData);

			// Save LastProcessedIndex to file
			SaveProcessedPosition(PathOfCurrentStatus, CurrentProcessedIndex, currentDate);
		}
	}
	else
	{
		TraceLog(ERROR_LEVEL, "No record\n");
		return ERROR;
	}

	// Clear the result
	ClearResult(qResult);

	// Reset the query command
	memset(querryCmd, ' ', MAX_QUERRY_LENGTH);
	
	/*************
	 GET Expected Profit
	 **************/
	// Build the query string
	sprintf(querryCmd,
			"SELECT timestamp, expected_pl "
			"FROM trade_results "
			"WHERE date='%s' "
			"ORDER BY timestamp",
			LastProcessedDate);

	//printf("querryCmd: %s\n",querryCmd);

	// Execute the query
	qResult = ExecuteQuery(eaaConnection, querryCmd);
	if (qResult == NULL )
	{
		return ERROR;
	}

	// get the number of records
	numRecords = GetNumberOfRow(qResult);
	int timestamp, i, iStepFive;
	double profit;
	
	int startOfDay = NumOfSecsFrom1970 + BeginTradingTimeInSeconds;
	struct tm * dateTime;
	time_t t = startOfDay;
	
	// Get local time
	dateTime = (struct tm *) localtime(&t);

	//check daylight saving time
	if(dateTime->tm_isdst == 1)
	{
		startOfDay -= 3600;
	}
	
	// Expected profit
	double expectedProfit[MAX_TIME_OF_PATTERN];
	memset(expectedProfit, 0, sizeof(expectedProfit));
	
	if (numRecords > 0)
	{		
		for (i = 0; i < numRecords; i++)
		{
			timestamp = atoi(GetValueField(qResult, i, 0));
			profit = atof(GetValueField(qResult, i, 1));
			iStepFive = (timestamp - startOfDay) / NUM_SECONDS_PER_PRIMETIVE_STEP;
			
			if (iStepFive < 0) 
			{
				expectedProfit[0] += profit;
			}
			else
			{
				expectedProfit[iStepFive] += profit;
			}
			
			// printf("%d, timestamp %d, step %d, profit %lf\n", i, timestamp, iStepFive, profit);
		}
		
		for (i = 0; i <= maxIndex; i++)
		{
			memset(querryCmd, 0, MAX_QUERRY_LENGTH);
			
			// Build the query string
			sprintf(querryCmd,
					"UPDATE summary_profit_losses SET expected_profit = %lf WHERE date='%s' AND total_second = %d",
					expectedProfit[i], LastProcessedDate, TimeStampCollection[i] + NumOfSecsFrom1970);

			//printf("querryCmd: %s\n",querryCmd);
			
			// Execute the query
			iqResult = ExecuteQuery(twt2Connection, querryCmd);

			// Check if the query is not NULL
			if (iqResult == NULL )
			{
				TraceLog(ERROR_LEVEL, "We can not insert profit/loss to database!\n");
				return ERROR;
			}
			
			// Clear querry
			ClearResult(iqResult);
		}
	}
	else
	{
		TraceLog(ERROR_LEVEL, "No record\n");
		return ERROR;
	}

	// Increase LastProcessedIndex
	LastProcessedIndex = maxIndex;
	
	// Save LastProcessedIndex to file
	SaveProcessedPosition(PathOfCurrentStatus, LastProcessedIndex, currentDate);
	
	// Destroy TWT2 BD connection
	DestroyConnection(twt2Connection);

	// Destroy EAA DB connection
	DestroyConnection(eaaConnection);

	return SUCCESS;
}
/****************************************************************************
 - Function name:	InsertOrderTypesDataIntoTwt2Db
 - Input:			N/A
 - Output:			N/A
 - Return:			int
 - Description:
 - Usage:			Insert order types(B, S, SS) data into some
 tables on TWT2 Database
 ****************************************************************************/
int InsertOrderTypesDataIntoTwt2Db(int currentIndex, const char *currentDate, int isOldData)
{
	// Querry String
	char querryCmd[MAX_QUERRY_LENGTH];

	// Reset querry string
	memset(querryCmd, 0, MAX_QUERRY_LENGTH);

	// Reset Entry/Exit Data
	memset(&OrderTypes, 0, sizeof(OrderTypes));

	if (currentIndex >= 0)
	{
		CalculateOrderTypesStatus(currentIndex, currentDate);
	}

	// Create the connection to TWT2 Database
	PGconn *connection = CreateConnection(&Twt2DatabaseConf);
	if (connection == NULL )
	{
		return ERROR;
	}
	char date[MIN_VALID_LENGTH];
	char time[MIN_VALID_DATE_LENGTH];

	// Reset buffers
	memset(date, 0, MIN_VALID_LENGTH);
	memset(time, 0, MIN_VALID_DATE_LENGTH);

	// Convert
	ConvertToDateFormat(date, currentDate);
	ConvertToTimeFormat(TimeStampCollection[currentIndex] + NumOfSecsFrom1970, time, isOldData);
	// Indexes
	int tsIndex, ecnIndex, isEntry_Exit;

	for (tsIndex = 0; tsIndex < NUM_OF_TS; tsIndex++)
	{
		for (ecnIndex = 0; ecnIndex < NUM_OF_ECN + 1; ecnIndex++)
		{
			for (isEntry_Exit = 0; isEntry_Exit < ENTRY_EXIT; isEntry_Exit++)
			{
				sprintf(querryCmd, "INSERT INTO summary_order_types(date,time_stamp,total_second,ts_id,ecn_id, buy_filled,buy_total,sell_filled,sell_total,shortsell_filled,shortsell_total,isentry_exit) VALUES('%s','%s',%d,%d,%d,%d,%d,%d,%d,%d,%d,%d)",
						date, time, TimeStampCollection[currentIndex] + NumOfSecsFrom1970,
						tsIndex, ecnIndex,
						OrderTypes[tsIndex][ecnIndex][isEntry_Exit][B][FILLED],
						OrderTypes[tsIndex][ecnIndex][isEntry_Exit][B][FILLED] + OrderTypes[tsIndex][ecnIndex][isEntry_Exit][B][NOT_FILLED],
						OrderTypes[tsIndex][ecnIndex][isEntry_Exit][S][FILLED],
						OrderTypes[tsIndex][ecnIndex][isEntry_Exit][S][FILLED] + OrderTypes[tsIndex][ecnIndex][isEntry_Exit][S][NOT_FILLED],
						OrderTypes[tsIndex][ecnIndex][isEntry_Exit][SS][FILLED],
						OrderTypes[tsIndex][ecnIndex][isEntry_Exit][SS][FILLED] + OrderTypes[tsIndex][ecnIndex][isEntry_Exit][SS][NOT_FILLED],
						isEntry_Exit);

				PGresult *querryResult = ExecuteQuery(connection, querryCmd);

				if (querryResult == NULL )
				{
					TraceLog(ERROR_LEVEL, "We can not insert data to database!\n");
					return ERROR;
				}

				// Clear querry
				ClearResult(querryResult);

				// Reset Querry String
				memset(querryCmd, 0, MAX_QUERRY_LENGTH);

				/******************
				 CALL POSTGRES FUNCTION
				 ********************/
				/*
				 // Build the query string
				 sprintf(querryCmd, "SELECT process_insert_order_types('%s','%s',%d,%d)",
				 date, time, tsIndex, ecnIndex);

				 PGresult *fQuerryResult = ExecuteQuery(connection, querryCmd);

				 if(fQuerryResult == NULL)
				 {
				 TraceLog("We can not insert data to database!\n");
				 return ERROR;
				 }

				 // Clear querry
				 ClearResult(fQuerryResult);

				 // Reset Querry String
				 memset(querryCmd, 0, MAX_QUERRY_LENGTH);
				 */
			}
		}
	}

	// Destroy connection to TWT2 DB
	DestroyConnection(connection);

	return SUCCESS;
}
/****************************************************************************
 - Function name:	CalculateOrderTypesStatus
 - Input:			N/A
 - Output:			N/A
 - Return:			int
 - Description:
 - Usage:			Calculate some information tha related to Order Types
 + Buy B
 + Sell S
 + Short Sell SS
 ****************************************************************************/
int CalculateOrderTypesStatus(int currentIndex, const char *currentDate)
{
	// Indexes
	int ecnIndex, statusIndex;
	int recordIndex;
  PlacedOrder_t *order;

	for (recordIndex = 0; recordIndex < NumOfRecords; recordIndex++)
	{
    order = &Orders[recordIndex];
    ecnIndex = order->ECN;
    
    // Calculate Exit_Success for BATZ Smart Route
    if ((ecnIndex == 4) && order->entryExit == EXIT && (strncmp(order->routingInst, BATZ_SMART_ROUTE, 3) == 0))
    {
      ecnIndex = NUM_OF_ECN;
    }
    
   statusIndex = NOT_FILLED;
    if (order->filledShares > 0)
    {
      statusIndex = FILLED;
    }
    
    OrderTypes[order->tradeServer ][ecnIndex][order->entryExit][order->side][statusIndex] += 1;
  }
  
	return SUCCESS;
}
/****************************************************************************
 - Function name:	InsertAskBidLaunchIntoTwt2Db
 - Input:			N/A
 - Output:			N/A
 - Return:			int
 - Description:
 - Usage:			Insert ASK/BID Launch data into on TWT2 Database
 ****************************************************************************/
int InsertAskBidLaunchIntoTwt2Db(int currentIndex, const char *currentDate, int isOldData)
{
	// Querry String
	char querryCmd[MAX_QUERRY_LENGTH];

	// Reset querry string
	memset(querryCmd, 0, MAX_QUERRY_LENGTH);

	// Reset Entry/Exit Data
	memset(&AskBidLaunch, 0, sizeof(AskBidLaunch));
	memset(&Exit_AskBidLaunch, 0, sizeof(Exit_AskBidLaunch));

	if (currentIndex >= 0)
	{
		CalculateAskBidLaunch(currentIndex, currentDate);
	}

	// DB connection
	PGconn *connection;

	// Create the connection
	connection = CreateConnection(&Twt2DatabaseConf);
	if (connection == NULL )
	{
		return ERROR;
	}
	char date[MIN_VALID_LENGTH];
	char time[MIN_VALID_DATE_LENGTH];

	// Reset buffers
	memset(date, 0, MIN_VALID_LENGTH);
	memset(time, 0, MIN_VALID_DATE_LENGTH);

	// Convert
	ConvertToDateFormat(date, currentDate);
	ConvertToTimeFormat(TimeStampCollection[currentIndex] + NumOfSecsFrom1970, time, isOldData);
	// Indexes
	int tsIndex, ecnIndex, ecnLaunchIndex, sideIndex;

	for (tsIndex = 0; tsIndex < NUM_OF_TS; tsIndex++)
	{
		for (ecnIndex = 0; ecnIndex < NUM_OF_ECN; ecnIndex++)
		{
			for (sideIndex = 0; sideIndex < NUM_OF_SIDE; sideIndex++)
			{
				for(ecnLaunchIndex = 0; ecnLaunchIndex < NUM_OF_ECN_LAUNCH; ecnLaunchIndex++)
				{
					sprintf(querryCmd, "INSERT INTO summary_launch_entry_ecns(date,time_stamp,total_second,ts_id,ecn_launch_id,ecn_entry_id,filled,total,ask_filled,ask_total,bid_filled,bid_total,side) VALUES('%s','%s',%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d)",
						date, time, TimeStampCollection[currentIndex] + NumOfSecsFrom1970,
						tsIndex, ecnLaunchIndex, ecnIndex,
						AskBidLaunch[tsIndex][ecnIndex][sideIndex][ecnLaunchIndex][ASK][SUCC] + AskBidLaunch[tsIndex][ecnIndex][sideIndex][ecnLaunchIndex][BID][SUCC],
						AskBidLaunch[tsIndex][ecnIndex][sideIndex][ecnLaunchIndex][ASK][SUCC] + AskBidLaunch[tsIndex][ecnIndex][sideIndex][ecnLaunchIndex][BID][SUCC] +
						AskBidLaunch[tsIndex][ecnIndex][sideIndex][ecnLaunchIndex][ASK][MISS] + AskBidLaunch[tsIndex][ecnIndex][sideIndex][ecnLaunchIndex][BID][MISS],
							AskBidLaunch[tsIndex][ecnIndex][sideIndex][ecnLaunchIndex][ASK][SUCC],
						AskBidLaunch[tsIndex][ecnIndex][sideIndex][ecnLaunchIndex][ASK][MISS] +
						AskBidLaunch[tsIndex][ecnIndex][sideIndex][ecnLaunchIndex][ASK][SUCC],
							AskBidLaunch[tsIndex][ecnIndex][sideIndex][ecnLaunchIndex][BID][SUCC],
						AskBidLaunch[tsIndex][ecnIndex][sideIndex][ecnLaunchIndex][BID][SUCC] +
						AskBidLaunch[tsIndex][ecnIndex][sideIndex][ecnLaunchIndex][BID][MISS],sideIndex);

					PGresult *querryResult = ExecuteQuery(connection, querryCmd);

					if (querryResult == NULL )
					{
						TraceLog(ERROR_LEVEL, "We can not insert ASK/BIG launch data to database!\n");
						return ERROR;
					}

					// Clear querry
					ClearResult(querryResult);

					// Reset Querry String
					memset(querryCmd, 0, MAX_QUERRY_LENGTH);

					sprintf(querryCmd, "INSERT INTO summary_launch_exit_ecns(date,time_stamp,total_second,ts_id,ecn_launch_id,ecn_exit_id,filled,total,ask_filled,ask_total,bid_filled,bid_total,side) VALUES('%s','%s',%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d)",
											date, time, TimeStampCollection[currentIndex] + NumOfSecsFrom1970,
											tsIndex, ecnLaunchIndex, ecnIndex,
											Exit_AskBidLaunch[tsIndex][ecnIndex][sideIndex][ecnLaunchIndex][ASK][SUCC] + Exit_AskBidLaunch[tsIndex][ecnIndex][sideIndex][ecnLaunchIndex][BID][SUCC],
											Exit_AskBidLaunch[tsIndex][ecnIndex][sideIndex][ecnLaunchIndex][ASK][SUCC] + Exit_AskBidLaunch[tsIndex][ecnIndex][sideIndex][ecnLaunchIndex][BID][SUCC] +
											Exit_AskBidLaunch[tsIndex][ecnIndex][sideIndex][ecnLaunchIndex][ASK][MISS] + Exit_AskBidLaunch[tsIndex][ecnIndex][sideIndex][ecnLaunchIndex][BID][MISS],
												Exit_AskBidLaunch[tsIndex][ecnIndex][sideIndex][ecnLaunchIndex][ASK][SUCC],
											Exit_AskBidLaunch[tsIndex][ecnIndex][sideIndex][ecnLaunchIndex][ASK][MISS] +
											Exit_AskBidLaunch[tsIndex][ecnIndex][sideIndex][ecnLaunchIndex][ASK][SUCC],
												Exit_AskBidLaunch[tsIndex][ecnIndex][sideIndex][ecnLaunchIndex][BID][SUCC],
											Exit_AskBidLaunch[tsIndex][ecnIndex][sideIndex][ecnLaunchIndex][BID][SUCC] +
											Exit_AskBidLaunch[tsIndex][ecnIndex][sideIndex][ecnLaunchIndex][BID][MISS], sideIndex);

					querryResult = ExecuteQuery(connection, querryCmd);

					if (querryResult == NULL )
					{
						TraceLog(ERROR_LEVEL, "We can not insert ASK/BID launch data to database!\n");
						return ERROR;
					}

					// Clear querry
					ClearResult(querryResult);

					// Reset Querry String
					memset(querryCmd, 0, MAX_QUERRY_LENGTH);
				}
			}
		}
	}

	// Destroy the connetion
	DestroyConnection(connection);

	return SUCCESS;
}

/****************************************************************************
 - Function name:	CalculateAskBidLaunch
 - Input:			N/A
 - Output:			N/A
 - Return:			int
 - Description:
 - Usage:
 ****************************************************************************/
int CalculateAskBidLaunch(int currentIndex, const char *currentDate)
{
	int ecnIndex, abStatusIndex, recordIndex;
  PlacedOrder_t *order;
  
	//	Loop each Orders
	for (recordIndex = 0; recordIndex < NumOfRecords; recordIndex++)
	{
    order = &Orders[recordIndex];
    ecnIndex = order->ECN;
    
    // Calculate Exit_Success for BATZ Smart Route
    if ((ecnIndex == 4) && (strncmp(order->routingInst, BATZ_SMART_ROUTE, 3) == 0))
    {
      ecnIndex = NUM_OF_ECN;
    }
    
    abStatusIndex = MISS;
    if (order->filledShares > 0)
    {
      abStatusIndex = SUCC;
    }
    
    if (order->entryExit == ENTRY)
		{
      AskBidLaunch[order->tradeServer][ecnIndex][order->side][order->ecnLaunch][order->AskBid][abStatusIndex] += 1;
    }
    else
    {
      Exit_AskBidLaunch[order->tradeServer][ecnIndex][order->side][order->ecnLaunch][order->AskBid][abStatusIndex] += 1;
    }
  }
	return SUCCESS;
}

/****************************************************************************
 - Function name:	InsertAckFillIntoTwt2Db
 - Input:			N/A
 - Output:			N/A
 - Return:			int
 - Description:
 - Usage:			Insert ACK and FILL time data into TWT2 Database
 ****************************************************************************/
int InsertAckFillIntoTwt2Db(int currentIndex, const char *currentDate, int isOldData)
{
	// Reset Entry/Exit Data
  memset(&MedianTime, 0, sizeof(MedianTime));
  memset(&MedianArray, 0, sizeof(MedianArray));

  memset(&MedianTime_15, 0, sizeof(MedianTime_15));
  memset(&MedianArray_15, 0, sizeof(MedianArray_15));

  memset(&MedianTime_30, 0, sizeof(MedianTime_30));
  memset(&MedianArray_30, 0, sizeof(MedianArray_30));

  memset(&MedianTime_60, 0, sizeof(MedianTime_60));
  memset(&MedianArray_60, 0, sizeof(MedianArray_60));

  if (currentIndex >= 0)
  {
    // Calculate Ack fill rate by 5 minute steps
    CalculateMedianTime_5(currentIndex, currentDate);
    InsertAckFillStep5IntoTwt2Db(currentIndex, currentDate, isOldData);
    // Calculate Ack fill rate by 15 minute steps
    if ((currentIndex % 3) == 0)
    {
      CalculateMedianTime_15(currentIndex, currentDate);
      InsertAckFillStep15IntoTwt2Db(currentIndex, currentDate, isOldData);
    }
    // Calculate Ack fill rate by 30 minute steps
    if ((currentIndex % 6) == 0)
    {
      CalculateMedianTime_30(currentIndex, currentDate);
      InsertAckFillStep30IntoTwt2Db(currentIndex, currentDate, isOldData);
    }
    // Calculate Ack fill rate by 60 minute steps
    if ((currentIndex % 12) == 0)
    {
      CalculateMedianTime_60(currentIndex, currentDate);
      InsertAckFillStep60IntoTwt2Db(currentIndex, currentDate, isOldData);
    }
  }
  return SUCCESS;
}

/****************************************************************************
 - Function name:	InsertAckFillStep5IntoTwt2Db
 - Input:			N/A
 - Output:			N/A
 - Return:			int
 - Description:
 - Usage:			Insert ACK and FILL time data into TWT2 Database
 ****************************************************************************/
int InsertAckFillStep5IntoTwt2Db(int currentIndex, const char *currentDate, int isOldData)
{
	char querryCmd[MAX_QUERRY_LENGTH];

	// Reset querry string
	memset(querryCmd, 0, MAX_QUERRY_LENGTH);

	// DB connection
	PGconn *connection;

	// Create the connection
	connection = CreateConnection(&Twt2DatabaseConf);
	if (connection == NULL )
	{
		return ERROR;
	}
	char date[MIN_VALID_LENGTH];
	char time[MIN_VALID_DATE_LENGTH];

	// Reset buffers
	memset(date, 0, MIN_VALID_LENGTH);
	memset(time, 0, MIN_VALID_DATE_LENGTH);

	// Convert
	ConvertToDateFormat(date, currentDate);
	ConvertToTimeFormat(TimeStampCollection[currentIndex] + NumOfSecsFrom1970, time, isOldData);
	// Indexes
	int tsIndex, ecnIndex;

	for (tsIndex = 0; tsIndex < NUM_OF_TS; tsIndex++)
	{
		for (ecnIndex = 0; ecnIndex < NUM_OF_ECN; ecnIndex++)
		{
			// Build the query string
			sprintf(querryCmd, "INSERT INTO summary_ack_fills(date,time_stamp,total_second,ts_id,ecn_id, total_time_ack,ack_count,total_time_fill,fill_count) VALUES('%s','%s',%d,%d,%d,%d,%d,%d,%d)",
					date, time, TimeStampCollection[currentIndex] + NumOfSecsFrom1970,
					tsIndex, ecnIndex,
					MedianTime[tsIndex][ecnIndex][ACK_TIME][TOTAL_TIME],
					(int) MedianTime[tsIndex][ecnIndex][ACK_TIME][ORDER_COUNT],
					MedianTime[tsIndex][ecnIndex][FILL_TIME][TOTAL_TIME],
					(int) MedianTime[tsIndex][ecnIndex][FILL_TIME][ORDER_COUNT]);

			PGresult *querryResult = ExecuteQuery(connection, querryCmd);

			if (querryResult == NULL )
			{
				TraceLog(ERROR_LEVEL, "We can not insert ACK/Fill data to database (Step: 5 minutes)!\n");
				return ERROR;
			}

			// Clear querry
			ClearResult(querryResult);

			// Reset Querry String
			memset(querryCmd, 0, MAX_QUERRY_LENGTH);

			/******************
			 CALL POSTGRES FUNCTION
			 ********************/

			/*
			 // Build the query string
			 sprintf(querryCmd, "SELECT process_insert_ack_fills('%s','%s',%d,%d)",
			 date, time, tsIndex, ecnIndex);

			 PGresult *fQuerryResult = ExecuteQuery(connection, querryCmd);

			 if(fQuerryResult == NULL)
			 {
			 TraceLog("We can not insert data to database!\n");
			 return ERROR;
			 }

			 // Clear querry
			 ClearResult(fQuerryResult);

			 // Reset Querry String
			 memset(querryCmd, 0, MAX_QUERRY_LENGTH);
			 */
		}
	}

	// Destroy thre connection
	DestroyConnection(connection);

	return SUCCESS;

}

/****************************************************************************
 - Function name:	InsertAckFillStep15IntoTwt2Db
 - Input:			N/A
 - Output:			N/A
 - Return:			int
 - Description:
 - Usage:			Insert ACK and FILL time data into TWT2 Database
 ****************************************************************************/
int InsertAckFillStep15IntoTwt2Db(int currentIndex, const char *currentDate, int isOldData)
{
	char querryCmd[MAX_QUERRY_LENGTH];

	// Reset querry string
	memset(querryCmd, 0, MAX_QUERRY_LENGTH);

	// DB connection
	PGconn *connection;

	// Create the connection
	connection = CreateConnection(&Twt2DatabaseConf);
	if (connection == NULL )
	{
		return ERROR;
	}
	char date[MIN_VALID_LENGTH];
	char time[MIN_VALID_DATE_LENGTH];

	// Reset buffers
	memset(date, 0, MIN_VALID_LENGTH);
	memset(time, 0, MIN_VALID_DATE_LENGTH);

	// Convert
	ConvertToDateFormat(date, currentDate);
	ConvertToTimeFormat(TimeStampCollection[currentIndex] + NumOfSecsFrom1970, time, isOldData);
	// Indexes
	int tsIndex, ecnIndex;

	for (tsIndex = 0; tsIndex < NUM_OF_TS; tsIndex++)
	{
		for (ecnIndex = 0; ecnIndex < NUM_OF_ECN; ecnIndex++)
		{
			// Build the query string
			sprintf(querryCmd, "INSERT INTO summary_ack_fills_15(date,time_stamp,total_second,ts_id,ecn_id, total_time_ack,ack_count,total_time_fill,fill_count) VALUES('%s','%s',%d,%d,%d,%d,%d,%d,%d)",
					date, time, TimeStampCollection[currentIndex] + NumOfSecsFrom1970,
					tsIndex, ecnIndex,
					MedianTime_15[tsIndex][ecnIndex][ACK_TIME][TOTAL_TIME],
					(int) MedianTime_15[tsIndex][ecnIndex][ACK_TIME][ORDER_COUNT],
					MedianTime_15[tsIndex][ecnIndex][FILL_TIME][TOTAL_TIME],
					(int) MedianTime_15[tsIndex][ecnIndex][FILL_TIME][ORDER_COUNT]);

			PGresult *querryResult = ExecuteQuery(connection, querryCmd);

			if (querryResult == NULL )
			{
				TraceLog(ERROR_LEVEL, "We can not insert ACK/Fill data to database (Step: 15 minutes)!\n");
				return ERROR;
			}

			// Clear querry
			ClearResult(querryResult);

			// Reset Querry String
			memset(querryCmd, 0, MAX_QUERRY_LENGTH);

			/******************
			 CALL POSTGRES FUNCTION
			 ********************/

			/*
			 // Build the query string
			 sprintf(querryCmd, "SELECT process_insert_ack_fills('%s','%s',%d,%d)",
			 date, time, tsIndex, ecnIndex);

			 PGresult *fQuerryResult = ExecuteQuery(connection, querryCmd);

			 if(fQuerryResult == NULL)
			 {
			 TraceLog("We can not insert data to database!\n");
			 return ERROR;
			 }

			 // Clear querry
			 ClearResult(fQuerryResult);

			 // Reset Querry String
			 memset(querryCmd, 0, MAX_QUERRY_LENGTH);
			 */
		}
	}

	// Destroy thre connection
	DestroyConnection(connection);

	return SUCCESS;

}

/****************************************************************************
 - Function name:	InsertAckFillStep30IntoTwt2Db
 - Input:			N/A
 - Output:			N/A
 - Return:			int
 - Description:
 - Usage:			Insert ACK and FILL time data into TWT2 Database
 ****************************************************************************/
int InsertAckFillStep30IntoTwt2Db(int currentIndex, const char *currentDate, int isOldData)
{
	char querryCmd[MAX_QUERRY_LENGTH];

	// Reset querry string
	memset(querryCmd, 0, MAX_QUERRY_LENGTH);

	// DB connection
	PGconn *connection;

	// Create the connection
	connection = CreateConnection(&Twt2DatabaseConf);
	if (connection == NULL )
	{
		return ERROR;
	}
	char date[MIN_VALID_LENGTH];
	char time[MIN_VALID_DATE_LENGTH];

	// Reset buffers
	memset(date, 0, MIN_VALID_LENGTH);
	memset(time, 0, MIN_VALID_DATE_LENGTH);

	// Convert
	ConvertToDateFormat(date, currentDate);
	ConvertToTimeFormat(TimeStampCollection[currentIndex] + NumOfSecsFrom1970, time, isOldData);
	// Indexes
	int tsIndex, ecnIndex;

	for (tsIndex = 0; tsIndex < NUM_OF_TS; tsIndex++)
	{
		for (ecnIndex = 0; ecnIndex < NUM_OF_ECN; ecnIndex++)
		{
			// Build the query string
			sprintf(querryCmd, "INSERT INTO summary_ack_fills_30(date,time_stamp,total_second,ts_id,ecn_id, total_time_ack,ack_count,total_time_fill,fill_count) VALUES('%s','%s',%d,%d,%d,%d,%d,%d,%d)",
					date, time, TimeStampCollection[currentIndex] + NumOfSecsFrom1970,
					tsIndex, ecnIndex,
					MedianTime_30[tsIndex][ecnIndex][ACK_TIME][TOTAL_TIME],
					(int) MedianTime_30[tsIndex][ecnIndex][ACK_TIME][ORDER_COUNT],
					MedianTime_30[tsIndex][ecnIndex][FILL_TIME][TOTAL_TIME],
					(int) MedianTime_30[tsIndex][ecnIndex][FILL_TIME][ORDER_COUNT]);

			PGresult *querryResult = ExecuteQuery(connection, querryCmd);

			if (querryResult == NULL )
			{
				TraceLog(ERROR_LEVEL, "We can not insert ACK/Fill data to database (Step: 30 minutes)!\n");
				return ERROR;
			}

			// Clear querry
			ClearResult(querryResult);

			// Reset Querry String
			memset(querryCmd, 0, MAX_QUERRY_LENGTH);

			/******************
			 CALL POSTGRES FUNCTION
			 ********************/

			/*
			 // Build the query string
			 sprintf(querryCmd, "SELECT process_insert_ack_fills('%s','%s',%d,%d)",
			 date, time, tsIndex, ecnIndex);

			 PGresult *fQuerryResult = ExecuteQuery(connection, querryCmd);

			 if(fQuerryResult == NULL)
			 {
			 TraceLog("We can not insert data to database!\n");
			 return ERROR;
			 }

			 // Clear querry
			 ClearResult(fQuerryResult);

			 // Reset Querry String
			 memset(querryCmd, 0, MAX_QUERRY_LENGTH);
			 */
		}
	}

	// Destroy thre connection
	DestroyConnection(connection);

	return SUCCESS;

}

/****************************************************************************
 - Function name:	InsertAckFillStep60IntoTwt2Db
 - Input:			N/A
 - Output:			N/A
 - Return:			int
 - Description:
 - Usage:			Insert ACK and FILL time data into TWT2 Database
 ****************************************************************************/
int InsertAckFillStep60IntoTwt2Db(int currentIndex, const char *currentDate, int isOldData)
{
	char querryCmd[MAX_QUERRY_LENGTH];

	// Reset querry string
	memset(querryCmd, 0, MAX_QUERRY_LENGTH);

	// DB connection
	PGconn *connection;

	// Create the connection
	connection = CreateConnection(&Twt2DatabaseConf);
	if (connection == NULL )
	{
		return ERROR;
	}
	char date[MIN_VALID_LENGTH];
	char time[MIN_VALID_DATE_LENGTH];

	// Reset buffers
	memset(date, 0, MIN_VALID_LENGTH);
	memset(time, 0, MIN_VALID_DATE_LENGTH);

	// Convert
	ConvertToDateFormat(date, currentDate);
	ConvertToTimeFormat(TimeStampCollection[currentIndex] + NumOfSecsFrom1970, time, isOldData);
	// Indexes
	int tsIndex, ecnIndex;

	for (tsIndex = 0; tsIndex < NUM_OF_TS; tsIndex++)
	{
		for (ecnIndex = 0; ecnIndex < NUM_OF_ECN; ecnIndex++)
		{
			// Build the query string
			sprintf(querryCmd, "INSERT INTO summary_ack_fills_60(date,time_stamp,total_second,ts_id,ecn_id, total_time_ack,ack_count,total_time_fill,fill_count) VALUES('%s','%s',%d,%d,%d,%d,%d,%d,%d)",
					date, time, TimeStampCollection[currentIndex] + NumOfSecsFrom1970,
					tsIndex, ecnIndex,
					MedianTime_60[tsIndex][ecnIndex][ACK_TIME][TOTAL_TIME],
					(int) MedianTime_60[tsIndex][ecnIndex][ACK_TIME][ORDER_COUNT],
					MedianTime_60[tsIndex][ecnIndex][FILL_TIME][TOTAL_TIME],
					(int) MedianTime_60[tsIndex][ecnIndex][FILL_TIME][ORDER_COUNT]);

			PGresult *querryResult = ExecuteQuery(connection, querryCmd);

			if (querryResult == NULL )
			{
				TraceLog(ERROR_LEVEL, "We can not insert data to database (Step: 60 minutes)!\n");
				return ERROR;
			}

			// Clear querry
			ClearResult(querryResult);

			// Reset Querry String
			memset(querryCmd, 0, MAX_QUERRY_LENGTH);

			/******************
			 CALL POSTGRES FUNCTION
			 ********************/

			/*
			 // Build the query string
			 sprintf(querryCmd, "SELECT process_insert_ack_fills('%s','%s',%d,%d)",
			 date, time, tsIndex, ecnIndex);

			 PGresult *fQuerryResult = ExecuteQuery(connection, querryCmd);

			 if(fQuerryResult == NULL)
			 {
			 TraceLog("We can not insert data to database!\n");
			 return ERROR;
			 }

			 // Clear querry
			 ClearResult(fQuerryResult);

			 // Reset Querry String
			 memset(querryCmd, 0, MAX_QUERRY_LENGTH);
			 */
		}
	}

	// Destroy thre connection
	DestroyConnection(connection);

	return SUCCESS;
}

/****************************************************************************
 - Function name:	CalculateMedianTime
 - Input:			N/A
 - Output:			N/A
 - Return:			int
 - Description:
 - Usage:			Calculate average time of ACK/FILL
 ****************************************************************************/
int CalculateMedianTime_5(int currentIndex, const char *currentDate)
{
	// Time range
	int timeBegin, timeEnd;
	if (currentIndex > 0)
	{
		timeBegin = TimeStampCollection[currentIndex - 1];
		timeEnd = TimeStampCollection[currentIndex];
	}
	else
	{
		timeBegin = TimeStampCollection[currentIndex];
		timeEnd = TimeStampCollection[currentIndex];
	}

	// 	DB Connection
	PGconn * connection;

	//	Create DB Connection
	connection = CreateConnection(&EaaDatabaseConf);
	if (connection == NULL )
	{
		return ERROR;
	}

	// Query Result
	PGresult * qResult;

	// Query string
	char queryCmd[MAX_QUERRY_LENGTH];

	// Reset query string
	memset(queryCmd, ' ', MAX_QUERRY_LENGTH);

	// Build the query string
	if (timeBegin < timeEnd)
	{
		sprintf(queryCmd,"select trim(ecn), ts_id, time_stamp, to_number(to_char(time_ack, 'SS:US'), '0000000000') as time_ack, to_number(to_char(time_fill, 'SS:US'), '0000000000') as time_fill from view_avg_times Where (Cast(substr(time_stamp::text,1,2) as int) * 60 * 60) + (Cast(substr(time_stamp::text,4,2) as int) * 60) + Cast(substr(time_stamp::text,7,2) as int) >= %d and (Cast(substr(time_stamp::text,1,2) as int) * 60 * 60) + (Cast(substr(time_stamp::text,4,2) as int) * 60) + Cast(substr(time_stamp::text,7,2) as int) < %d and date = '%s' ", timeBegin, timeEnd, LastProcessedDate);
	}
	else
	{
		sprintf(queryCmd,"select trim(ecn), ts_id, time_stamp, to_number(to_char(time_ack, 'SS:US'), '0000000000') as time_ack, to_number(to_char(time_fill, 'SS:US'), '0000000000') as time_fill from view_avg_times Where (Cast(substr(time_stamp::text,1,2) as int) * 60 * 60) + (Cast(substr(time_stamp::text,4,2) as int) * 60) + Cast(substr(time_stamp::text,7,2) as int) < %d and date = '%s' ", timeBegin, LastProcessedDate);
	}

	// Row index
	int rowIndex, tsId, ecnId;
	//Element_Array
	int oCount;
	//ts index,ecn index
	int tsIndex, ecnIndex;
	// ECN name
	char ecnOrder[4];

	// ACK and FILL time
	int ackTime = 0;
	int fillTime = 0;

	// Execute the query
	qResult = ExecuteQuery(connection, queryCmd);
	if (qResult == NULL )
	{
		return ERROR;
	}

	// Get the number of rows
	int numOfRows = GetNumberOfRow(qResult);

	if (numOfRows > 0) //	Have records
	{
		for (rowIndex = 0; rowIndex < numOfRows; rowIndex++)
		{
			// ECN
			strcpy(ecnOrder, GetValueField(qResult, rowIndex, 0));

			// Trade Server ID
			tsId = atoi(GetValueField(qResult, rowIndex, 1));

			//	ACK Time
			ackTime = atoi(GetValueField(qResult, rowIndex, 3));

			//	FILL Time
			fillTime = atoi(GetValueField(qResult, rowIndex, 4));

			for (ecnId = 0; ecnId < NUM_OF_ECN; ecnId++)
			{
				if (strcmp(Ecn[ecnId], ecnOrder) == 0)
				{
					oCount = MedianTime[tsId][ecnId][ACK_TIME][ORDER_COUNT];
					MedianArray[tsId][ecnId][ACK_TIME][oCount] = ackTime;
					MedianTime[tsId][ecnId][ACK_TIME][ORDER_COUNT]++;

					MedianArray[tsId][ecnId][FILL_TIME][oCount] = fillTime;
					MedianTime[tsId][ecnId][FILL_TIME][ORDER_COUNT]++;
				}
			}
		}

		// Clear querry
		ClearResult(qResult);

		int fillMedian, ackMedian;
		int medianIndex, temp1, temp2;

		for (tsIndex = 0; tsIndex < NUM_OF_TS; tsIndex++)
		{
			for (ecnIndex = 0; ecnIndex < NUM_OF_ECN; ecnIndex++)
			{
				oCount = MedianTime[tsIndex][ecnIndex][ACK_TIME][ORDER_COUNT];
				medianIndex = (oCount - 1) / 2;
				if ((oCount % 2) != 0)
				{
					ackMedian = median(MedianArray[tsIndex][ecnIndex][ACK_TIME], oCount,medianIndex);
					fillMedian = median(MedianArray[tsIndex][ecnIndex][FILL_TIME], oCount,medianIndex);
				}
				else
				{
					temp1 = median(MedianArray[tsIndex][ecnIndex][ACK_TIME], oCount,medianIndex);
					temp2 = median(MedianArray[tsIndex][ecnIndex][ACK_TIME], oCount,medianIndex+1);
					ackMedian = (temp1 + temp2) / 2;

					temp1 = median(MedianArray[tsIndex][ecnIndex][FILL_TIME], oCount,medianIndex);
					temp2 = median(MedianArray[tsIndex][ecnIndex][FILL_TIME], oCount,medianIndex+1);
					fillMedian = (temp1 + temp2) / 2;
				}

				MedianTime[tsIndex][ecnIndex][ACK_TIME][TOTAL_TIME] = ackMedian;
				//MedianTime[tsIndex][ecnIndex][ACK_TIME][ORDER_COUNT] = 1;

				MedianTime[tsIndex][ecnIndex][FILL_TIME][TOTAL_TIME] = fillMedian;
				//MedianTime[tsIndex][ecnIndex][FILL_TIME][ORDER_COUNT] = 1;
			}
		}
	}
	else
	{
		//TraceLog("No record found.\n");
		//exit(1);
	}

	DestroyConnection(connection);

	return numOfRows;
}

/****************************************************************************
 - Function name:	CalculateMedianTime_15
 - Input:			N/A
 - Output:			N/A
 - Return:			int
 - Description:
 - Usage:			Calculate average time of ACK/FILL
 ****************************************************************************/
int CalculateMedianTime_15(int currentIndex, const char *currentDate)
{
	// Time range
	int timeBegin, timeEnd;
	if (currentIndex >= 3)
	{
		timeBegin = TimeStampCollection[currentIndex - 3];
		timeEnd = TimeStampCollection[currentIndex];
	}
	else
	{
		timeBegin = TimeStampCollection[currentIndex];
		timeEnd = TimeStampCollection[currentIndex];
	}

	// 	DB Connection
	PGconn * connection;

	//	Create DB Connection
	connection = CreateConnection(&EaaDatabaseConf);
	if (connection == NULL )
	{
		return ERROR;
	}

	// Query Result
	PGresult * qResult;

	// Query string
	char queryCmd[MAX_QUERRY_LENGTH];

	// Reset query string
	memset(queryCmd, ' ', MAX_QUERRY_LENGTH);

	// Build the query string
	if (timeBegin < timeEnd)
	{
		sprintf(queryCmd,"select trim(ecn), ts_id, time_stamp, to_number(to_char(time_ack, 'SS:US'), '0000000000') as time_ack, to_number(to_char(time_fill, 'SS:US'), '0000000000') as time_fill from view_avg_times Where (Cast(substr(time_stamp::text,1,2) as int) * 60 * 60) + (Cast(substr(time_stamp::text,4,2) as int) * 60) + Cast(substr(time_stamp::text,7,2) as int) >= %d and (Cast(substr(time_stamp::text,1,2) as int) * 60 * 60) + (Cast(substr(time_stamp::text,4,2) as int) * 60) + Cast(substr(time_stamp::text,7,2) as int) < %d and date = '%s' ", timeBegin, timeEnd, LastProcessedDate);
	}
	else
	{
		sprintf(queryCmd,"select trim(ecn), ts_id, time_stamp, to_number(to_char(time_ack, 'SS:US'), '0000000000') as time_ack, to_number(to_char(time_fill, 'SS:US'), '0000000000') as time_fill from view_avg_times Where (Cast(substr(time_stamp::text,1,2) as int) * 60 * 60) + (Cast(substr(time_stamp::text,4,2) as int) * 60) + Cast(substr(time_stamp::text,7,2) as int) < %d and date = '%s' ", timeBegin, LastProcessedDate);
	}

	// Row index
	int rowIndex, tsId, ecnId;
	//Element_Array
	int oCount;
	//ts index,ecn index
	int tsIndex, ecnIndex;
	// ECN name
	char ecnOrder[6];

	// ACK and FILL time
	int ackTime = 0;
	int fillTime = 0;

	// Execute the query
	qResult = ExecuteQuery(connection, queryCmd);
	if (qResult == NULL )
	{
		return ERROR;
	}

	// Get the number of rows
	int numOfRows = GetNumberOfRow(qResult);

	if (numOfRows > 0) //	Have records
	{
		for (rowIndex = 0; rowIndex < numOfRows; rowIndex++)
		{
			// ECN
			strcpy(ecnOrder, GetValueField(qResult, rowIndex, 0));

			// Trade Server ID
			tsId = atoi(GetValueField(qResult, rowIndex, 1));

			//	ACK Time
			ackTime = atoi(GetValueField(qResult, rowIndex, 3));

			//	FILL Time
			fillTime = atoi(GetValueField(qResult, rowIndex, 4));

			for (ecnId = 0; ecnId < NUM_OF_ECN; ecnId++)
			{
				if (strcmp(Ecn[ecnId], ecnOrder) == 0)
				{
					oCount = MedianTime_15[tsId][ecnId][ACK_TIME][ORDER_COUNT];
					MedianArray_15[tsId][ecnId][ACK_TIME][oCount] = ackTime;
					MedianTime_15[tsId][ecnId][ACK_TIME][ORDER_COUNT]++;

					MedianArray_15[tsId][ecnId][FILL_TIME][oCount] = fillTime;
					MedianTime_15[tsId][ecnId][FILL_TIME][ORDER_COUNT]++;
				}
			}
		}

		// Clear querry
		ClearResult(qResult);

		int fillMedian, ackMedian;
		int medianIndex, temp1, temp2;

		for (tsIndex = 0; tsIndex < NUM_OF_TS; tsIndex++)
		{
			for (ecnIndex = 0; ecnIndex < NUM_OF_ECN; ecnIndex++)
			{
				oCount = MedianTime_15[tsIndex][ecnIndex][ACK_TIME][ORDER_COUNT];
				medianIndex = (oCount - 1) / 2;
				if ((oCount % 2) != 0)
				{
					ackMedian = median(MedianArray_15[tsIndex][ecnIndex][ACK_TIME], oCount,medianIndex);
					fillMedian = median(MedianArray_15[tsIndex][ecnIndex][FILL_TIME], oCount,medianIndex);
				}
				else
				{
					temp1 = median(MedianArray_15[tsIndex][ecnIndex][ACK_TIME], oCount,medianIndex);
					temp2 = median(MedianArray_15[tsIndex][ecnIndex][ACK_TIME], oCount,medianIndex+1);
					ackMedian = (temp1 + temp2) / 2;

					temp1 = median(MedianArray_15[tsIndex][ecnIndex][FILL_TIME], oCount,medianIndex);
					temp2 = median(MedianArray_15[tsIndex][ecnIndex][FILL_TIME], oCount,medianIndex+1);
					fillMedian = (temp1 + temp2) / 2;
				}

				MedianTime_15[tsIndex][ecnIndex][ACK_TIME][TOTAL_TIME] = ackMedian;
				//MedianTime[tsIndex][ecnIndex][ACK_TIME][ORDER_COUNT] = 1;

				MedianTime_15[tsIndex][ecnIndex][FILL_TIME][TOTAL_TIME] = fillMedian;
				//MedianTime[tsIndex][ecnIndex][FILL_TIME][ORDER_COUNT] = 1;
			}
		}
	}
	else
	{
		//printf("No record found.\n");
		//exit(1);
	}

	DestroyConnection(connection);

	return numOfRows;
}

/****************************************************************************
 - Function name:	CalculateMedianTime_30
 - Input:			N/A
 - Output:			N/A
 - Return:			int
 - Description:
 - Usage:			Calculate average time of ACK/FILL
 ****************************************************************************/
int CalculateMedianTime_30(int currentIndex, const char *currentDate)
{
	// Time range
	int timeBegin, timeEnd;
	if (currentIndex >= 6)
	{
		timeBegin = TimeStampCollection[currentIndex - 6];
		timeEnd = TimeStampCollection[currentIndex];
	}
	else
	{
		timeBegin = TimeStampCollection[currentIndex];
		timeEnd = TimeStampCollection[currentIndex];
	}

	// 	DB Connection
	PGconn * connection;

	//	Create DB Connection
	connection = CreateConnection(&EaaDatabaseConf);
	if (connection == NULL )
	{
		return ERROR;
	}

	// Query Result
	PGresult * qResult;

	// Query string
	char queryCmd[MAX_QUERRY_LENGTH];

	// Reset query string
	memset(queryCmd, ' ', MAX_QUERRY_LENGTH);

	// Build the query string
	if (timeBegin < timeEnd)
	{
		sprintf(queryCmd,"select trim(ecn), ts_id, time_stamp, to_number(to_char(time_ack, 'SS:US'), '0000000000') as time_ack, to_number(to_char(time_fill, 'SS:US'), '0000000000') as time_fill from view_avg_times Where (Cast(substr(time_stamp::text,1,2) as int) * 60 * 60) + (Cast(substr(time_stamp::text,4,2) as int) * 60) + Cast(substr(time_stamp::text,7,2) as int) >= %d and (Cast(substr(time_stamp::text,1,2) as int) * 60 * 60) + (Cast(substr(time_stamp::text,4,2) as int) * 60) + Cast(substr(time_stamp::text,7,2) as int) < %d and date = '%s' ", timeBegin, timeEnd, LastProcessedDate);
	}
	else
	{
		sprintf(queryCmd,"select trim(ecn), ts_id, time_stamp, to_number(to_char(time_ack, 'SS:US'), '0000000000') as time_ack, to_number(to_char(time_fill, 'SS:US'), '0000000000') as time_fill from view_avg_times Where (Cast(substr(time_stamp::text,1,2) as int) * 60 * 60) + (Cast(substr(time_stamp::text,4,2) as int) * 60) + Cast(substr(time_stamp::text,7,2) as int) < %d and date = '%s' ", timeBegin, LastProcessedDate);
	}

	// Row index
	int rowIndex, tsId, ecnId;
	//Element_Array
	int oCount;
	//ts index,ecn index
	int tsIndex, ecnIndex;
	// ECN name
	char ecnOrder[6];

	// ACK and FILL time
	int ackTime = 0;
	int fillTime = 0;

	// Execute the query
	qResult = ExecuteQuery(connection, queryCmd);
	if (qResult == NULL )
	{
		return ERROR;
	}

	// Get the number of rows
	int numOfRows = GetNumberOfRow(qResult);

	if (numOfRows > 0) //	Have records
	{
		for (rowIndex = 0; rowIndex < numOfRows; rowIndex++)
		{
			// ECN
			strcpy(ecnOrder, GetValueField(qResult, rowIndex, 0));

			// Trade Server ID
			tsId = atoi(GetValueField(qResult, rowIndex, 1));

			//	ACK Time
			ackTime = atoi(GetValueField(qResult, rowIndex, 3));

			//	FILL Time
			fillTime = atoi(GetValueField(qResult, rowIndex, 4));

			for (ecnId = 0; ecnId < NUM_OF_ECN; ecnId++)
			{
				if (strcmp(Ecn[ecnId], ecnOrder) == 0)
				{
					oCount = MedianTime_30[tsId][ecnId][ACK_TIME][ORDER_COUNT];
					MedianArray_30[tsId][ecnId][ACK_TIME][oCount] = ackTime;
					MedianTime_30[tsId][ecnId][ACK_TIME][ORDER_COUNT]++;

					MedianArray_30[tsId][ecnId][FILL_TIME][oCount] = fillTime;
					MedianTime_30[tsId][ecnId][FILL_TIME][ORDER_COUNT]++;
				}
			}
		}

		// Clear querry
		ClearResult(qResult);

		int fillMedian, ackMedian;
		int medianIndex, temp1, temp2;

		for (tsIndex = 0; tsIndex < NUM_OF_TS; tsIndex++)
		{
			for (ecnIndex = 0; ecnIndex < NUM_OF_ECN; ecnIndex++)
			{
				oCount = MedianTime_30[tsIndex][ecnIndex][ACK_TIME][ORDER_COUNT];
				medianIndex = (oCount - 1) / 2;
				if ((oCount % 2) != 0)
				{
					ackMedian = median(MedianArray_30[tsIndex][ecnIndex][ACK_TIME], oCount,medianIndex);
					fillMedian = median(MedianArray_30[tsIndex][ecnIndex][FILL_TIME], oCount,medianIndex);
				}
				else
				{
					temp1 = median(MedianArray_30[tsIndex][ecnIndex][ACK_TIME], oCount,medianIndex);
					temp2 = median(MedianArray_30[tsIndex][ecnIndex][ACK_TIME], oCount,medianIndex+1);
					ackMedian = (temp1 + temp2) / 2;

					temp1 = median(MedianArray_30[tsIndex][ecnIndex][FILL_TIME], oCount,medianIndex);
					temp2 = median(MedianArray_30[tsIndex][ecnIndex][FILL_TIME], oCount,medianIndex+1);
					fillMedian = (temp1 + temp2) / 2;
				}

				MedianTime_30[tsIndex][ecnIndex][ACK_TIME][TOTAL_TIME] = ackMedian;
				//MedianTime[tsIndex][ecnIndex][ACK_TIME][ORDER_COUNT] = 1;

				MedianTime_30[tsIndex][ecnIndex][FILL_TIME][TOTAL_TIME] = fillMedian;
				//MedianTime[tsIndex][ecnIndex][FILL_TIME][ORDER_COUNT] = 1;
			}
		}
	}
	else
	{
		//printf("No record found.\n");
		//exit(1);
	}

	DestroyConnection(connection);

	return numOfRows;
}

/****************************************************************************
 - Function name:	CalculateMedianTime_60
 - Input:			N/A
 - Output:			N/A
 - Return:			int
 - Description:
 - Usage:			Calculate average time of ACK/FILL
 ****************************************************************************/
int CalculateMedianTime_60(int currentIndex, const char *currentDate)
{
	// Time range
	int timeBegin, timeEnd;
	if (currentIndex >= 12)
	{
		timeBegin = TimeStampCollection[currentIndex - 12];
		timeEnd = TimeStampCollection[currentIndex];
	}
	else
	{
		timeBegin = TimeStampCollection[currentIndex];
		timeEnd = TimeStampCollection[currentIndex];
	}

	// 	DB Connection
	PGconn * connection;

	//	Create DB Connection
	connection = CreateConnection(&EaaDatabaseConf);
	if (connection == NULL )
	{
		return ERROR;
	}

	// Query Result
	PGresult * qResult;

	// Query string
	char queryCmd[MAX_QUERRY_LENGTH];

	// Reset query string
	memset(queryCmd, ' ', MAX_QUERRY_LENGTH);

	// Build the query string
	if (timeBegin < timeEnd)
	{
		sprintf(queryCmd,"select trim(ecn), ts_id, time_stamp, to_number(to_char(time_ack, 'SS:US'), '0000000000') as time_ack, to_number(to_char(time_fill, 'SS:US'), '0000000000') as time_fill from view_avg_times Where (Cast(substr(time_stamp::text,1,2) as int) * 60 * 60) + (Cast(substr(time_stamp::text,4,2) as int) * 60) + Cast(substr(time_stamp::text,7,2) as int) >= %d and (Cast(substr(time_stamp::text,1,2) as int) * 60 * 60) + (Cast(substr(time_stamp::text,4,2) as int) * 60) + Cast(substr(time_stamp::text,7,2) as int) < %d and date = '%s' ", timeBegin, timeEnd, LastProcessedDate);
	}
	else
	{
		sprintf(queryCmd,"select trim(ecn), ts_id, time_stamp, to_number(to_char(time_ack, 'SS:US'), '0000000000') as time_ack, to_number(to_char(time_fill, 'SS:US'), '0000000000') as time_fill from view_avg_times Where (Cast(substr(time_stamp::text,1,2) as int) * 60 * 60) + (Cast(substr(time_stamp::text,4,2) as int) * 60) + Cast(substr(time_stamp::text,7,2) as int) < %d and date = '%s' ", timeBegin, LastProcessedDate);
	}

	// Row index
	int rowIndex, tsId, ecnId;
	//Element_Array
	int oCount;
	//ts index,ecn index
	int tsIndex, ecnIndex;
	// ECN name
	char ecnOrder[6];

	// ACK and FILL time
	int ackTime = 0;
	int fillTime = 0;

	// Execute the query
	qResult = ExecuteQuery(connection, queryCmd);
	if (qResult == NULL )
	{
		return ERROR;
	}

	// Get the number of rows
	int numOfRows = GetNumberOfRow(qResult);

	if (numOfRows > 0) //	Have records
	{
		for (rowIndex = 0; rowIndex < numOfRows; rowIndex++)
		{
			// ECN
			strcpy(ecnOrder, GetValueField(qResult, rowIndex, 0));

			// Trade Server ID
			tsId = atoi(GetValueField(qResult, rowIndex, 1));

			//	ACK Time
			ackTime = atoi(GetValueField(qResult, rowIndex, 3));

			//	FILL Time
			fillTime = atoi(GetValueField(qResult, rowIndex, 4));

			for (ecnId = 0; ecnId < NUM_OF_ECN; ecnId++)
			{
				if (strcmp(Ecn[ecnId], ecnOrder) == 0)
				{
					oCount = MedianTime_60[tsId][ecnId][ACK_TIME][ORDER_COUNT];
					MedianArray_60[tsId][ecnId][ACK_TIME][oCount] = ackTime;
					MedianTime_60[tsId][ecnId][ACK_TIME][ORDER_COUNT]++;

					MedianArray_60[tsId][ecnId][FILL_TIME][oCount] = fillTime;
					MedianTime_60[tsId][ecnId][FILL_TIME][ORDER_COUNT]++;
				}
			}
		}

		// Clear querry
		ClearResult(qResult);

		int fillMedian, ackMedian;
		int medianIndex, temp1, temp2;

		for (tsIndex = 0; tsIndex < NUM_OF_TS; tsIndex++)
		{
			for (ecnIndex = 0; ecnIndex < NUM_OF_ECN; ecnIndex++)
			{
				oCount = MedianTime_60[tsIndex][ecnIndex][ACK_TIME][ORDER_COUNT];
				medianIndex = (oCount - 1) / 2;
				if ((oCount % 2) != 0)
				{
					ackMedian = median(MedianArray_60[tsIndex][ecnIndex][ACK_TIME], oCount,medianIndex);
					fillMedian = median(MedianArray_60[tsIndex][ecnIndex][FILL_TIME], oCount,medianIndex);
				}
				else
				{
					temp1 = median(MedianArray_60[tsIndex][ecnIndex][ACK_TIME], oCount,medianIndex);
					temp2 = median(MedianArray_60[tsIndex][ecnIndex][ACK_TIME], oCount,medianIndex+1);
					ackMedian = (temp1 + temp2) / 2;

					temp1 = median(MedianArray_60[tsIndex][ecnIndex][FILL_TIME], oCount,medianIndex);
					temp2 = median(MedianArray_60[tsIndex][ecnIndex][FILL_TIME], oCount,medianIndex+1);
					fillMedian = (temp1 + temp2) / 2;
				}

				MedianTime_60[tsIndex][ecnIndex][ACK_TIME][TOTAL_TIME] = ackMedian;
				//MedianTime[tsIndex][ecnIndex][ACK_TIME][ORDER_COUNT] = 1;

				MedianTime_60[tsIndex][ecnIndex][FILL_TIME][TOTAL_TIME] = fillMedian;
				//MedianTime[tsIndex][ecnIndex][FILL_TIME][ORDER_COUNT] = 1;
			}
		}
	}
	else
	{
		//printf("No record found.\n");
		//exit(1);
	}

	DestroyConnection(connection);

	return numOfRows;
}

/****************************************************************************
 - Function name:	InitializeSymbolIndexList
 - Input:
 - Output:
 - Return:
 - Description:
 - Usage:
 ****************************************************************************/
int InitializeSymbolIndexList(void)
{
	int i, j, k, m, n, longIndex;

	for (i = 0; i < MAX_CHARSET_MEMBER_NO; i++)
	{
		for (j = 0; j < MAX_CHARSET_MEMBER_NO; j++)
		{
			for (k = 0; k < MAX_CHARSET_MEMBER_NO; k++)
			{
				for (m = 0; m < MAX_CHARSET_MEMBER_NO; m++)
				{
					for (n = 0; n < MAX_CHARSET_MEMBER_NO; n++)
					{
						//Index of each stock symbol now will be -1, meaning
						//'not a valid index yet'
						symbolIndexList[i][j][k][m][n] = -1;
					}
				}
			}
		}
	}

	// added codes to handle 8 chars-symbol
	// 20100416
	for (longIndex = 0; longIndex < MAX_STOCK_SYMBOL; longIndex++)
	{
		for (i = 0; i < MAX_CHARSET_MEMBER_NO; i++)
		{
			for (j = 0; j < MAX_CHARSET_MEMBER_NO; j++)
			{
				longSymbolIndexArray[longIndex][i][j] = -1;
			}
		}
	}

	for (longIndex = 0; longIndex < MAX_STOCK_SYMBOL; longIndex++)
	{
		for (i = 0; i < MAX_CHARSET_MEMBER_NO; i++)
		{
			extraLongSymbolIndexArray[longIndex][i] = -1;
		}
	}
	return 0;
}

/****************************************************************************
 - Function name:	GetSymbolIndex
 - Input:
 - Output:
 - Return:
 - Description:
 - Usage:
 ****************************************************************************/
int GetSymbolIndex(char stockSymbol[8])
{
	if (SymbolTradinInfoList.countSymbol == MAX_STOCK_SYMBOL)
	{
		TraceLog(ERROR_LEVEL, "We can not insert symbol %s to symbol list. It is full\n", stockSymbol);
		return ERROR;
	}

	int a, b, c, d, e, f, g, h;
	int symbolIndex, longIndex, newIndex;

	//Character 1: in range A-Z, or $
	//65 is the ASCII code of 'A'
	//90 is the ASCII code of 'Z'
	//36 is the ASCII code of '$'

	if ((stockSymbol[0] > 64) && (stockSymbol[0] < 91))
	{
		a = stockSymbol[0] - INDEX_OF_A_CHAR;	//A-Z
	}
	else if (stockSymbol[0] == 36)	// $
	{
		a = 1;
	}
	else
	{
		//Invalid stock symbol
		return -1;
	}

	//Character 2: 0, A-Z
	//65 is the ASCII code of 'A'
	//90 is the ASCII code of 'Z'

	//The order of 'if' expressions are important for faster speed
	//This is experience, right?

	if ((stockSymbol[1] > 64) && (stockSymbol[1] < 91))
	{
		b = stockSymbol[1] - INDEX_OF_A_CHAR;	//A-Z
	}
	//A dollar sign and a dot never appear in the second position!!!
	else if ((stockSymbol[1] == 0) || (stockSymbol[1] == 32))//0: NULL; 32:SPACE
	{
		//Stock symbol's length is 1, no need to do more
		//This also guarantee fast speed
		//We use the variable a in the array index instead of
		//stockSymbol[0] - INDEX_OF_A_CHAR so as not to calculate anything

		//Check new stock symbol
		if (symbolIndexList[a][0][0][0][0] == -1)
		{

			symbolIndexList[a][0][0][0][0] = SymbolTradinInfoList.countSymbol++;

			symbolIndex = symbolIndexList[a][0][0][0][0];
			SymbolTradinInfoList.TradinInfoList[symbolIndex].totalVolume = 0;
			SymbolTradinInfoList.TradinInfoList[symbolIndex].profitLoss = 0.0;
			SymbolTradinInfoList.TradinInfoList[symbolIndex].totalMarket = 0.0;
			memcpy(SymbolTradinInfoList.TradinInfoList[symbolIndex].symbol, stockSymbol, 1);
			SymbolTradinInfoList.TradinInfoList[symbolIndex].avgPrice = 0.00;
			SymbolTradinInfoList.TradinInfoList[symbolIndex].leftShares = 0;

			//printf("stockSymbol = %.8s -> symbolIndex = %d\n", stockSymbol, symbolIndex);
		}

		return symbolIndexList[a][0][0][0][0];
	}
	//hyphen is expected to come if all other checks fail
	else if (stockSymbol[1] == '$')
	{
		b = 1;
	}
	else if (stockSymbol[1] == '+')
	{
		b = 2;
	}
	else if (stockSymbol[1] == '-')
	{
		b = 3;
	}
	else if (stockSymbol[1] == '.')
	{
		b = 4;
	}
	else if (stockSymbol[1] == '_')
	{
		b = 5;
	}
	else
	{
		return -1;
	}

	//Character 3: 0, ., A-Z
	//65 is the ASCII code of 'A'
	//90 is the ASCII code of 'Z'

	//The order of 'if' expressions are important for faster speed

	if ((stockSymbol[2] > 64) && (stockSymbol[2] < 91))
	{
		c = stockSymbol[2] - INDEX_OF_A_CHAR;	//A-Z
	}

	//Not in range A-Z. We have 244 zeros, and 119 dot
	//Maybe we need to count how many $ and dot at the third position
	//for even better 'if' order

	//244 zeros check first
	else if ((stockSymbol[2] == 0) || (stockSymbol[2] == 32))//0: NULL; 32: SPACE
	{
		//Stock symbol's length is 2, no need to do more
		//This also guarantee fast speed
		//We use the variable a, b in the array index instead of
		//stockSymbol[0] - INDEX_OF_A_CHAR, stockSymbol[1] - INDEX_OF_A_CHAR so as not to calculate anything

		//Check new stock symbol
		if (symbolIndexList[a][b][0][0][0] == -1)
		{
			symbolIndexList[a][b][0][0][0] = SymbolTradinInfoList.countSymbol++;

			symbolIndex = symbolIndexList[a][b][0][0][0];
			SymbolTradinInfoList.TradinInfoList[symbolIndex].totalVolume = 0;
			SymbolTradinInfoList.TradinInfoList[symbolIndex].profitLoss = 0.0;
			SymbolTradinInfoList.TradinInfoList[symbolIndex].totalMarket = 0.0;
			memcpy(SymbolTradinInfoList.TradinInfoList[symbolIndex].symbol, stockSymbol, 2);
			SymbolTradinInfoList.TradinInfoList[symbolIndex].avgPrice = 0.0;
			SymbolTradinInfoList.TradinInfoList[symbolIndex].leftShares = 0;

			//printf("stockSymbol = %.8s -> symbolIndex = %d\n", stockSymbol, symbolIndex);
		}

		return symbolIndexList[a][b][0][0][0];
	}
	else if (stockSymbol[2] == '$')
	{
		c = 1;
	}
	//dot is expected to come if all other checks fail
	else if (stockSymbol[2] == '+')
	{
		c = 2;
	}
	else if (stockSymbol[2] == '-')
	{
		c = 3;
	}
	else if (stockSymbol[2] == '.')
	{
		c = 4;
	}
	else if (stockSymbol[2] == '_')
	{
		c = 5;
	}
	//Nothing matched. This is an invalid stock symbol
	else
	{
		return -1;
	}

	//Character 4: 0, ., A-Z
	//65 is the ASCII code of 'A'
	//91 is the ASCII code of 'Z'

	if ((stockSymbol[3] > 64) && (stockSymbol[3] < 91))
	{
		d = stockSymbol[3] - INDEX_OF_A_CHAR;	//A-Z
	}

	//Not in range A-Z. We have 3802 zeros, and 119 dot
	//Maybe we need to count how many $ and dot at the fourth position
	//for even better 'if' order

	//Zero is checked first
	else if ((stockSymbol[3] == 0) || (stockSymbol[3] == 32))//0: NULL; 32:SPACE
	{
		//Check new stock symbol
		if (symbolIndexList[a][b][c][0][0] == -1)
		{

			symbolIndexList[a][b][c][0][0] = SymbolTradinInfoList.countSymbol++;

			symbolIndex = symbolIndexList[a][b][c][0][0];
			SymbolTradinInfoList.TradinInfoList[symbolIndex].totalVolume = 0;
			SymbolTradinInfoList.TradinInfoList[symbolIndex].profitLoss = 0.0;
			SymbolTradinInfoList.TradinInfoList[symbolIndex].totalMarket = 0.0;
			memcpy(SymbolTradinInfoList.TradinInfoList[symbolIndex].symbol, stockSymbol, 3);
			SymbolTradinInfoList.TradinInfoList[symbolIndex].avgPrice = 0.00;
			SymbolTradinInfoList.TradinInfoList[symbolIndex].leftShares = 0;

			//printf("stockSymbol = %.8s -> symbolIndex = %d\n", stockSymbol, symbolIndex);
		}

		return symbolIndexList[a][b][c][0][0];
	}
	//Dot is expected
	else if (stockSymbol[3] == '$')
	{
		d = 1;
	}
	else if (stockSymbol[3] == '+')
	{
		d = 2;
	}
	else if (stockSymbol[3] == '-')
	{
		d = 3;
	}
	else if (stockSymbol[3] == '.')
	{
		d = 4;
	}
	else if (stockSymbol[3] == '_')
	{
		d = 5;
	}
	//Nothing matched. This is an invalid stock symbol
	else
	{
		return -1;
	}

	//Character 5: 0, ., A-Z
	if ((stockSymbol[4] > 64) && (stockSymbol[4] < 91))
	{
		e = stockSymbol[4] - INDEX_OF_A_CHAR;	//A-Z
	}
	//Not in range A-Z. We have 3248 zeros, 1 dot (for the stock symbol ZZZZ.T only)
	//Zero is checked first

	else if ((stockSymbol[4] == 0) || (stockSymbol[4] == 32))//NULL Because: 4: 3248, 32: SPACE
	{
		//Check new stock symbol
		if (symbolIndexList[a][b][c][d][0] == -1)
		{

			symbolIndexList[a][b][c][d][0] = SymbolTradinInfoList.countSymbol++;

			symbolIndex = symbolIndexList[a][b][c][d][0];
			SymbolTradinInfoList.TradinInfoList[symbolIndex].totalVolume = 0;
			SymbolTradinInfoList.TradinInfoList[symbolIndex].profitLoss = 0.0;
			SymbolTradinInfoList.TradinInfoList[symbolIndex].totalMarket = 0.0;
			memcpy(SymbolTradinInfoList.TradinInfoList[symbolIndex].symbol, stockSymbol, 4);
			SymbolTradinInfoList.TradinInfoList[symbolIndex].avgPrice = 0.00;
			SymbolTradinInfoList.TradinInfoList[symbolIndex].leftShares = 0;

			//printf("stockSymbol = %.8s -> symbolIndex = %d\n", stockSymbol, symbolIndex);
		}

		return symbolIndexList[a][b][c][d][0];
	}
	else if (stockSymbol[4] == '$')
	{
		e = 1;
	}
	else if (stockSymbol[4] == '+')
	{
		e = 2;
	}
	else if (stockSymbol[4] == '-')
	{
		e = 3;
	}
	else if (stockSymbol[4] == '.')
	{
		e = 4;
	}
	else if (stockSymbol[4] == '_')
	{
		e = 5;
	}
	//Nothing matched. This is an invalid stock symbol
	else
	{
		return -1;
	}

	//Character 6: 0, ., A-Z
	if ((stockSymbol[5] > 64) && (stockSymbol[5] < 91))
	{
		f = stockSymbol[5] - INDEX_OF_A_CHAR;	//A-Z
	}
	else if ((stockSymbol[5] == 0) || (stockSymbol[5] == 32))//NULL Because: 4: 3248, 32: SPACE
	{
		if (symbolIndexList[a][b][c][d][e] == -1)
		{

			symbolIndexList[a][b][c][d][e] = SymbolTradinInfoList.countSymbol++;

			symbolIndex = symbolIndexList[a][b][c][d][e];
			SymbolTradinInfoList.TradinInfoList[symbolIndex].totalVolume = 0;
			SymbolTradinInfoList.TradinInfoList[symbolIndex].profitLoss = 0.0;
			SymbolTradinInfoList.TradinInfoList[symbolIndex].totalMarket = 0.0;
			memcpy(SymbolTradinInfoList.TradinInfoList[symbolIndex].symbol, stockSymbol, 5);
			SymbolTradinInfoList.TradinInfoList[symbolIndex].avgPrice = 0.00;
			SymbolTradinInfoList.TradinInfoList[symbolIndex].leftShares = 0;

			//printf("stockSymbol = %.8s -> symbolIndex = %d\n", stockSymbol, symbolIndex);
		}

		return symbolIndexList[a][b][c][d][e];
	}
	else if (stockSymbol[5] == '$')
	{
		f = 1;
	}
	else if (stockSymbol[5] == '+')
	{
		f = 2;
	}
	else if (stockSymbol[5] == '-')
	{
		f = 3;
	}
	else if (stockSymbol[5] == '.')
	{
		f = 4;
	}
	else if (stockSymbol[5] == '_')
	{
		f = 5;
	}
	//Nothing matched. This is an invalid stock symbol
	else
	{
		return -1;
	}

	if ((stockSymbol[6] > 64) && (stockSymbol[6] < 91))
	{
		g = stockSymbol[6] - INDEX_OF_A_CHAR;	//A-Z
	}
	else if ((stockSymbol[6] == 0) || (stockSymbol[6] == 32))//NULL Because: 4: 3248, 32: SPACE
	{
		if (symbolIndexList[a][b][c][d][e] == -1)
		{
			// Update character 5
			symbolIndexList[a][b][c][d][e] = SymbolTradinInfoList.countSymbol++;

			longIndex = symbolIndexList[a][b][c][d][e];

			// Update Trading Info
			SymbolTradinInfoList.TradinInfoList[longIndex].totalVolume = 0;
			SymbolTradinInfoList.TradinInfoList[longIndex].profitLoss = 0.0;
			SymbolTradinInfoList.TradinInfoList[longIndex].totalMarket = 0.0;
			memcpy(SymbolTradinInfoList.TradinInfoList[longIndex].symbol, stockSymbol, 5);
			SymbolTradinInfoList.TradinInfoList[longIndex].avgPrice = 0.00;
			SymbolTradinInfoList.TradinInfoList[longIndex].leftShares = 0;

			// Update character 6
			longSymbolIndexArray[longIndex][f][0] = SymbolTradinInfoList.countSymbol++;

			newIndex = longSymbolIndexArray[longIndex][f][0];
			// Update Trading Info
			SymbolTradinInfoList.TradinInfoList[newIndex].totalVolume = 0;
			SymbolTradinInfoList.TradinInfoList[newIndex].profitLoss = 0.0;
			SymbolTradinInfoList.TradinInfoList[newIndex].totalMarket = 0.0;
			memcpy(SymbolTradinInfoList.TradinInfoList[newIndex].symbol, stockSymbol, 6);
			SymbolTradinInfoList.TradinInfoList[newIndex].avgPrice = 0.00;
			SymbolTradinInfoList.TradinInfoList[newIndex].leftShares = 0;

			return newIndex;
		}
		else
		{
			longIndex = symbolIndexList[a][b][c][d][e];
			if (longSymbolIndexArray[longIndex][f][0] == -1)
			{
				// Update character 6
				longSymbolIndexArray[longIndex][f][0] = SymbolTradinInfoList.countSymbol++;

				newIndex = longSymbolIndexArray[longIndex][f][0];
				SymbolTradinInfoList.TradinInfoList[newIndex].totalVolume = 0;
				SymbolTradinInfoList.TradinInfoList[newIndex].profitLoss = 0.0;
				SymbolTradinInfoList.TradinInfoList[newIndex].totalMarket = 0.0;
				memcpy(SymbolTradinInfoList.TradinInfoList[newIndex].symbol, stockSymbol, 6);
				SymbolTradinInfoList.TradinInfoList[newIndex].avgPrice = 0.00;
				SymbolTradinInfoList.TradinInfoList[newIndex].leftShares = 0;

				return newIndex;
			}
			else
			{
				return newIndex = longSymbolIndexArray[longIndex][f][0];
			}
		}
	}
	else if (stockSymbol[6] == '$')
	{
		g = 1;
	}
	else if (stockSymbol[6] == '+')
	{
		g = 2;
	}
	else if (stockSymbol[6] == '-')
	{
		g = 3;
	}
	else if (stockSymbol[6] == '.')
	{
		g = 4;
	}
	else if (stockSymbol[6] == '_')
	{
		g = 5;
	}
	//Nothing matched. This is an invalid stock symbol
	else
	{
		return -1;
	}

	// Update code to handle 8 characters
	// 20100416
	// 7 characters
	if ((stockSymbol[7] > 64) && (stockSymbol[7] < 91))
	{
		h = stockSymbol[7] - INDEX_OF_A_CHAR;	//A-Z
	}
	else if ((stockSymbol[7] == 0) || (stockSymbol[7] == 32))//NULL Because: 4: 3248, 32: SPACE
	{
		if (symbolIndexList[a][b][c][d][e] == -1)
		{
			// Update character 5
			symbolIndexList[a][b][c][d][e] = SymbolTradinInfoList.countSymbol++;
			longIndex = symbolIndexList[a][b][c][d][e];

			// Update Trading Info
			SymbolTradinInfoList.TradinInfoList[longIndex].totalVolume = 0;
			SymbolTradinInfoList.TradinInfoList[longIndex].profitLoss = 0.0;
			SymbolTradinInfoList.TradinInfoList[longIndex].totalMarket = 0.0;
			memcpy(SymbolTradinInfoList.TradinInfoList[longIndex].symbol, stockSymbol, 5);
			SymbolTradinInfoList.TradinInfoList[longIndex].avgPrice = 0.00;
			SymbolTradinInfoList.TradinInfoList[longIndex].leftShares = 0;

			// Update character 7
			longSymbolIndexArray[longIndex][f][g] = SymbolTradinInfoList.countSymbol++;

			newIndex = longSymbolIndexArray[longIndex][f][g];
			// Update Trading Info
			SymbolTradinInfoList.TradinInfoList[newIndex].totalVolume = 0;
			SymbolTradinInfoList.TradinInfoList[newIndex].profitLoss = 0.0;
			SymbolTradinInfoList.TradinInfoList[newIndex].totalMarket = 0.0;
			memcpy(SymbolTradinInfoList.TradinInfoList[newIndex].symbol, stockSymbol, 7);
			SymbolTradinInfoList.TradinInfoList[newIndex].avgPrice = 0.00;
			SymbolTradinInfoList.TradinInfoList[newIndex].leftShares = 0;

			return newIndex;
		}
		else
		{
			longIndex = symbolIndexList[a][b][c][d][e];
			if (longSymbolIndexArray[longIndex][f][g] == -1)
			{
				// Update character 7
				longSymbolIndexArray[longIndex][f][g] = SymbolTradinInfoList.countSymbol++;

				newIndex = longSymbolIndexArray[longIndex][f][g];
				SymbolTradinInfoList.TradinInfoList[newIndex].totalVolume = 0;
				SymbolTradinInfoList.TradinInfoList[newIndex].profitLoss = 0.0;
				SymbolTradinInfoList.TradinInfoList[newIndex].totalMarket = 0.0;
				memcpy(SymbolTradinInfoList.TradinInfoList[newIndex].symbol, stockSymbol, 7);
				SymbolTradinInfoList.TradinInfoList[newIndex].avgPrice = 0.00;
				SymbolTradinInfoList.TradinInfoList[newIndex].leftShares = 0;

				return newIndex;
			}
			else
			{
				return newIndex = longSymbolIndexArray[longIndex][f][g];
			}

		}
	}
	else if (stockSymbol[7] == '$')
	{
		h = 1;
	}
	else if (stockSymbol[7] == '+')
	{
		h = 2;
	}
	else if (stockSymbol[7] == '-')
	{
		h = 3;
	}
	else if (stockSymbol[7] == '.')
	{
		h = 4;
	}
	else if (stockSymbol[7] == '_')
	{
		h = 5;
	}
	//Nothing matched. This is an invalid stock symbol
	else
	{
		return -1;
	}

	// 8 characters
	// Not have character 5
	if (symbolIndexList[a][b][c][d][e] == -1)
	{
		// Update character 5
		symbolIndexList[a][b][c][d][e] = SymbolTradinInfoList.countSymbol++;
		longIndex = symbolIndexList[a][b][c][d][e];

		// Update Trading Info
		SymbolTradinInfoList.TradinInfoList[longIndex].totalVolume = 0;
		SymbolTradinInfoList.TradinInfoList[longIndex].profitLoss = 0.0;
		SymbolTradinInfoList.TradinInfoList[longIndex].totalMarket = 0.0;
			memcpy(SymbolTradinInfoList.TradinInfoList[longIndex].symbol, stockSymbol, 5);
		SymbolTradinInfoList.TradinInfoList[longIndex].avgPrice = 0.00;
		SymbolTradinInfoList.TradinInfoList[longIndex].leftShares = 0;

		// Update character 7
			longSymbolIndexArray[longIndex][f][g] = SymbolTradinInfoList.countSymbol++;
		newIndex = longSymbolIndexArray[longIndex][f][g];

		// Update Trading Info
		SymbolTradinInfoList.TradinInfoList[newIndex].totalVolume = 0;
		SymbolTradinInfoList.TradinInfoList[newIndex].profitLoss = 0.0;
		SymbolTradinInfoList.TradinInfoList[newIndex].totalMarket = 0.0;
			memcpy(SymbolTradinInfoList.TradinInfoList[newIndex].symbol, stockSymbol, 7);
		SymbolTradinInfoList.TradinInfoList[newIndex].avgPrice = 0.00;
		SymbolTradinInfoList.TradinInfoList[newIndex].leftShares = 0;

		// Update character 8
			extraLongSymbolIndexArray[newIndex][h] = SymbolTradinInfoList.countSymbol++;
		newIndex = extraLongSymbolIndexArray[newIndex][h];

		// Update Trading Info
		SymbolTradinInfoList.TradinInfoList[newIndex].totalVolume = 0;
		SymbolTradinInfoList.TradinInfoList[newIndex].profitLoss = 0.0;
		SymbolTradinInfoList.TradinInfoList[newIndex].totalMarket = 0.0;
			memcpy(SymbolTradinInfoList.TradinInfoList[newIndex].symbol, stockSymbol, 8);
		SymbolTradinInfoList.TradinInfoList[newIndex].avgPrice = 0.00;
		SymbolTradinInfoList.TradinInfoList[newIndex].leftShares = 0;

		return newIndex;
	}
	else
	{
		longIndex = symbolIndexList[a][b][c][d][e];

		// Not have character 7
		if (longSymbolIndexArray[longIndex][f][g] == -1)
		{
			// Update character 7
			longSymbolIndexArray[longIndex][f][g] = SymbolTradinInfoList.countSymbol++;
			newIndex = longSymbolIndexArray[longIndex][f][g];

			// Update Trading Info
			SymbolTradinInfoList.TradinInfoList[newIndex].totalVolume = 0;
			SymbolTradinInfoList.TradinInfoList[newIndex].profitLoss = 0.0;
			SymbolTradinInfoList.TradinInfoList[newIndex].totalMarket = 0.0;
			memcpy(SymbolTradinInfoList.TradinInfoList[newIndex].symbol, stockSymbol, 7);
			SymbolTradinInfoList.TradinInfoList[newIndex].avgPrice = 0.00;
			SymbolTradinInfoList.TradinInfoList[newIndex].leftShares = 0;

			// Update character 8
			extraLongSymbolIndexArray[newIndex][h] = SymbolTradinInfoList.countSymbol++;
			newIndex = extraLongSymbolIndexArray[newIndex][h];

			// Update Trading Info
			SymbolTradinInfoList.TradinInfoList[newIndex].totalVolume = 0;
			SymbolTradinInfoList.TradinInfoList[newIndex].profitLoss = 0.0;
			SymbolTradinInfoList.TradinInfoList[newIndex].totalMarket = 0.0;
			memcpy(SymbolTradinInfoList.TradinInfoList[newIndex].symbol, stockSymbol, 8);
			SymbolTradinInfoList.TradinInfoList[newIndex].avgPrice = 0.00;
			SymbolTradinInfoList.TradinInfoList[newIndex].leftShares = 0;

		}
		else
		{
			newIndex = longSymbolIndexArray[longIndex][f][g];
		}

		if (extraLongSymbolIndexArray[newIndex][h] == -1)
		{
			newIndex = SymbolTradinInfoList.countSymbol++;
			newIndex = extraLongSymbolIndexArray[newIndex][h];

			// Update Trading Info
			SymbolTradinInfoList.TradinInfoList[newIndex].totalVolume = 0;
			SymbolTradinInfoList.TradinInfoList[newIndex].profitLoss = 0.0;
			SymbolTradinInfoList.TradinInfoList[newIndex].totalMarket = 0.0;
			memcpy(SymbolTradinInfoList.TradinInfoList[newIndex].symbol, stockSymbol, 8);
			SymbolTradinInfoList.TradinInfoList[newIndex].avgPrice = 0.00;
			SymbolTradinInfoList.TradinInfoList[newIndex].leftShares = 0;

			return newIndex;
		}
		else
		{
			return newIndex = extraLongSymbolIndexArray[newIndex][h];
		}
	}

}

/****************************************************************************
 - Function name:	GetStockSymbolIndex
 - Input:
 - Output:
 - Return:
 - Description:
 - Usage:
 ****************************************************************************/
int CreateDataForBeginOfDay(const char *currentDate)
{
	// 	DB Connection
	PGconn * connection;

	//	Create DB Connection
	connection = CreateConnection(&EaaDatabaseConf);
	if (connection == NULL )
	{
		return ERROR;
	}

	// Query Result
	PGresult * qResult;

	// Query string
	char queryCmd[MAX_QUERRY_LENGTH];

	// Reset query string
	memset(queryCmd, ' ', MAX_QUERRY_LENGTH);

	// Number of records
	// int numRecords;

	// Indexes of records
	int recordIndex;

	// Build query string
	sprintf(queryCmd, "SELECT * FROM manual_open_positions where symbol <> 'T3ST' and date = '%s'", currentDate);

	// Execute the query
	qResult = ExecuteQuery(connection, queryCmd);
	if (qResult == NULL )
	{
		return ERROR;
	}

	// Get the number of rows
	int numOfRows = GetNumberOfRow(qResult);

	if (numOfRows > 0)
	{
		for (recordIndex = 0; recordIndex < numOfRows; recordIndex++)
		{
			// Get Symbol Index
			int symbolIndex = GetSymbolIndex(GetValueField(qResult, recordIndex, 2));

			if (symbolIndex == -1)
			{
				return ERROR;
			}

			if (strncmp(GetValueField(qResult, recordIndex, 1), "B", 1) == 0)
			{
				// BUY
				SymbolTradinInfoList.TradinInfoList[symbolIndex].leftShares = atoi(GetValueField(qResult, recordIndex, 3));
			}
			else
			{
				SymbolTradinInfoList.TradinInfoList[symbolIndex].leftShares = -1 * atoi(GetValueField(qResult, recordIndex, 3));
			}

			SymbolTradinInfoList.TradinInfoList[symbolIndex].avgPrice = atof(GetValueField(qResult, recordIndex, 4));
			SymbolTradinInfoList.TradinInfoList[symbolIndex].profitLoss = 0.0;
			SymbolTradinInfoList.TradinInfoList[symbolIndex].totalVolume = atoi(GetValueField(qResult, recordIndex, 3));
			SymbolTradinInfoList.TradinInfoList[symbolIndex].totalMarket = atoi(GetValueField(qResult, recordIndex, 3)) * atof(GetValueField(qResult, recordIndex, 4));
		}
	}
	else
	{
		TraceLog(DEBUG_LEVEL, "No record is processed\n");
	}

	return SUCCESS;
}

/********************************************************************************
 - Function name:	CalculateFee
 - Input:
 - Output:
 - Return:
 - Description:
 - Usage:
 **********************************************************************************/
double CalculateFee(double price, int size, char side, char *liquidityField, char *ecn)
{
	double totalFees = 0.0;
	double totalSecFees = 0.0, totalTafFee = 0.0, totalWedbushCommission = 0.0,
			totalVenueFee = 0.0;
	
	char liquidity = liquidityField[0];
	if (side == 'S')
	{
		totalSecFees = (size * price * FeeConf.secFee);
	}

	//Taf fees
	totalTafFee = (size * FeeConf.tafFee);

	//Commission
	totalWedbushCommission = (size * FeeConf.commission);

	if (strncmp(ecn, "ARCA", 4) == 0)
	{
		/*
		 Executions on NYSE Arca
		 A  Added liquidity.
		 B  Added liquidity while order was not displayed.
		 D  Added liquidity on a sub-dollar execution.
		 M  Added liquidity on an MPL order.
		 S  Added liquidity, set new Arca BBO. Effective May 28th, 2012
		 R  Reduced liquidity.
		 E  Reduced liquidity on a sub-dollar execution.
		 L  Reduced liquidity on an MPL order.
		 O  Neutral.
		 G  Opening or halt auction of market or auction-only order.
		 Z  Closing auction of market or auction-only order.
		 Routed Orders
		 X  Routed to another venue.
		 F  Routed to NYSE/AMEX and added liquidity.*
		 N  Routed to NYSE/AMEX and reduced liquidity.
		 H  Routed sub-dollar execution.
		 C  Routed to NYSE/AMEX and participated in the open or halt auction.*
		 U  Routed NYSE MOC/LOC order.
		 Y  Routed AMEX MOC/LOC order.
		 W Routed to NYSE/AMEX and re-routed to an external market.
		 */
		if (liquidity == 'A' || liquidity == 'B' || liquidity == 'D'|| liquidity == 'M' || liquidity == 'S') // Add
		{
			totalVenueFee =
					(price >= 1.00) ?
							(size * FeeConf.addVenueFeeWithPriceLager1) :
							(size * FeeConf.addVenueFeeWithPriceSmaler1);
		}
		else if (liquidity == 'R' || liquidity == 'E' || liquidity == 'L') // Remove
		{
			totalVenueFee =
					(price >= 1.00) ?
							(size * FeeConf.removeVenueFeeWithPriceLager1) :
							(size * price * FeeConf.removeVenueFeeWithPriceSmaler1);
		}
		else if (liquidity == 'G' || liquidity == 'Z') //auction
		{
			totalVenueFee = (size * FeeConf.auctionVenueFee);
		}
		else if (liquidity == 'X' || liquidity == 'F' || liquidity == 'N'
				|| liquidity == 'H' || liquidity == 'C' || liquidity == 'U'
				|| liquidity == 'Y' || liquidity == 'W') //Route
		{
			totalVenueFee = (size * FeeConf.arcaRouteVenueFee);
		}
		else
		{
			TraceLog(WARN_LEVEL, "(CalculateFillFee): Invalid liquidity (%c) detected on ARCA\n", liquidity);
		}
	}
	else if (strncmp(ecn, "OUCH", 4) == 0 || strncmp(ecn, "OUBX", 4) == 0 ||  strncmp(ecn, "PSX", 3) == 0)
	{
		/*
		 Liquidity Flags
		 Flag   Value
		 A 	Added
		 R 	Removed
		 O  Opening Cross (billable)
		 M  Opening Cross (non-billable)
		 C  Closing Cross (billable)
		 L  Closing Cross (non-billable)
		 H  Halt/IPO Cross (billable)
		 K  Halt/IPO Cross (billable)
		 m  Removed liquidity at a midpoint
		 k  Added liquidity via a midpoint order
		 */
		if (liquidity == 'A' || liquidity == 'k' || liquidity == 'J' || 
					liquidity == '7' || liquidity == '8' || liquidity == 'e' || liquidity == 'f') //Add
		{
			totalVenueFee =
					(price >= 1.00) ?
							(size * FeeConf.addVenueFeeWithPriceLager1) :
							(size * FeeConf.addVenueFeeWithPriceSmaler1);
		}
		else if (liquidity == 'R' || liquidity == 'm' || liquidity == '6' || liquidity == 'd') //Remove
		{
			totalVenueFee =
					(price >= 1.00) ?
							(size * FeeConf.removeVenueFeeWithPriceLager1) :
							(size * price
									* FeeConf.removeVenueFeeWithPriceSmaler1);
		}
		else if (liquidity == 'O' || liquidity == 'M' || liquidity == 'L'
				|| liquidity == 'H' || liquidity == 'K' || liquidity == 'C') //auction
		{
			totalVenueFee = (size * FeeConf.auctionVenueFee);
		}
		else
		{
			TraceLog(WARN_LEVEL, "(CalculateFillFee): Invalid liquidity (%c) detected on OUCH\n", liquidity);
		}
	}
	else if (strncmp(ecn, "RASH", 4) == 0)
	{
		/*
		 Flag Value
		 A Added
		 R Removed
		 J Non-displayed and added liquidity
		 V Displayed added liquidity with original order size of greater than or equal to 2,000 shares
		 X Routed
		 D DOT
		 F Added or Opening Trade (on NYSE)
		 G Odd Lot or On-Close order (on NYSE)
		 O Open Cross (billable)
		 M Open Cross (non-billable)
		 C Closing Cross (billable)
		 L Closing Cross (non-billable)
		 H Halt/IPO Cross (billable)
		 K Halt/IPO Cross (billable)
		 I Intraday/Post-Market Cross
		 Y Re-Routed by NYSE
		 S Odd Lot Execution (on NYSE)
		 U Added Liquidity (on NYSE)
		 B Routed to BX
		 E NYSE Other
		 P Routed to PSX
		 T Opening Trade (on ARCA)
		 Z On-Close order (on ARCA)
		 Q Routed to Nasdaq
		 m Removed liquidity at a midpoint
		 k Added liquidity via a midpoint order
		 9 Added (displayed) using Minimum Life Order Type
		 */
		if (liquidity == 'A' || liquidity == 'J' || liquidity == 'V'
				|| liquidity == 'D' || liquidity == 'F' || liquidity == 'U'
				|| liquidity == 'k' || liquidity == '9' || liquidity == '7' || 
					liquidity == '8' || liquidity == 'e' || liquidity == 'f') //Add
		{
			totalVenueFee =
					(price >= 1.00) ?
							(size * FeeConf.addVenueFeeWithPriceLager1) :
							(size * FeeConf.addVenueFeeWithPriceSmaler1);
		}
		else if (liquidity == 'R' || liquidity == 'm' || liquidity == 'd' || liquidity == '6') //Remove
		{
			totalVenueFee =
					(price >= 1.00) ?
							(size * FeeConf.removeVenueFeeWithPriceLager1) :
							(size * price * FeeConf.removeVenueFeeWithPriceSmaler1);
		}
		else if (liquidity == 'G' || liquidity == 'O' || liquidity == 'M'
				|| liquidity == 'C' || liquidity == 'L' || liquidity == 'H'
				|| liquidity == 'K' || liquidity == 'I' || liquidity == 'T'
				|| liquidity == 'Z') //Auction
		{
			totalVenueFee = (size * FeeConf.auctionVenueFee);
		}
		else if (liquidity == 'X' || liquidity == 'Y' || liquidity == 'B'
				|| liquidity == 'P' || liquidity == 'Q') //Route
		{
			totalVenueFee = (size * FeeConf.nasdaqRouteVenueFee);
		}
		else
		{
			TraceLog(WARN_LEVEL, "(CalculateFillFee): Invalid liquidity (%c) detected on RASH\n", liquidity);
		}
	}
	else if (strncmp(ecn, "NYSE", 4) == 0)
	{
		if (liquidity == '2' || liquidity == '8') //provider
		{
			totalVenueFee =
					(price >= 1.00) ?
							(size * FeeConf.addVenueFeeWithPriceLager1) :
							(size * FeeConf.addVenueFeeWithPriceSmaler1);
		}
		else if (liquidity == '1' || liquidity == '9' || liquidity == '3' || liquidity == ' ') 	//taker and blended
		{
			totalVenueFee =
					(price >= 1.00) ?
							(size * FeeConf.removeVenueFeeWithPriceLager1) :
							(size * price
									* FeeConf.removeVenueFeeWithPriceSmaler1);
		}
		else if (liquidity == '4' || liquidity == '5' || liquidity == '6' || liquidity == '7') // Auction
		{
			totalVenueFee = (size * FeeConf.auctionVenueFee);
		}
		else
		{
			TraceLog(WARN_LEVEL, "(CalculateFillFee): Invalid liquidity (%c) detected on NYSE\n", liquidity);
		}
	}
	else if (strncmp(ecn, "BATZ", 4) == 0 || strncmp(ecn, "BATY", 4) == 0)
	{
		if (liquidity == 'A') //Add
		{
			totalVenueFee =
					(price >= 1.00) ?
							(size * FeeConf.addVenueFeeWithPriceLager1) :
							(size * FeeConf.addVenueFeeWithPriceSmaler1);
		}
		else if (liquidity == 'R') 	//Remove
		{
			totalVenueFee =
					(price >= 1.00) ?
							(size * FeeConf.removeVenueFeeWithPriceLager1) :
							(size * price
									* FeeConf.removeVenueFeeWithPriceSmaler1);
		}
		else if (liquidity == 'C') // Auction
		{
			totalVenueFee = (size * FeeConf.auctionVenueFee);
		}
		else if (liquidity == 'X') // Route
		{
			totalVenueFee = (size * FeeConf.batsRouteVenueFee);
		}
		else
		{
			TraceLog(WARN_LEVEL, "Invalid liquidity (%c) detected on BATZ\n", liquidity);
		}
	}
	else if (strncmp(ecn, "EDGX", 4) == 0 || strncmp(ecn, "EDGA", 4) == 0)
	{
		/* Based on Edge_EDGX_Spec_V1_29 */
		if ((strcmp(liquidityField, "EA") == 0) || (strcmp(liquidityField, "HA") == 0) || 
			(strcmp(liquidityField, "AA") == 0) || (strcmp(liquidityField, "CL") == 0) || 
			(strcmp(liquidityField, "DM") == 0) || (strcmp(liquidityField, "MM") == 0) || 
			(strcmp(liquidityField, "PA") == 0) || (strcmp(liquidityField, "RB") == 0) || 
			(strcmp(liquidityField, "RC") == 0) || (strcmp(liquidityField, "RS") == 0) || 
			(strcmp(liquidityField, "RW") == 0) || (strcmp(liquidityField, "RY") == 0) || 
			(strcmp(liquidityField, "RZ") == 0) || 
			liquidity == 'A' || liquidity == 'B' || liquidity == 'F' || 
			liquidity == 'M' || liquidity == 'P' || liquidity == 'V' || 
			liquidity == '3' || liquidity == '9' || liquidity == '4' || 
			liquidity == '8' || liquidity == '1' || liquidity == 'E' || 
			liquidity == 'H' || liquidity == 'Y') //Add
		{
			totalVenueFee = (price >= 1.00)?(size * FeeConf.addVenueFeeWithPriceLager1):(size * FeeConf.addVenueFeeWithPriceSmaler1);
		}
		else if ((strcmp(liquidityField, "ER") == 0) || (strcmp(liquidityField, "BB") == 0) || 
				(strcmp(liquidityField, "DT") == 0) || (strcmp(liquidityField, "MT") == 0) || 
				(strcmp(liquidityField, "PI") == 0) || (strcmp(liquidityField, "PT") == 0) || 
				liquidity == 'C' || liquidity == 'D' || liquidity == 'G' || liquidity == 'J' || 
				liquidity == 'L' || liquidity == 'N' || liquidity == 'U' || liquidity == 'W' || 
				liquidity == '2' || liquidity == '6') //Remove
		{
			totalVenueFee = (price >= 1.00)?(size * FeeConf.removeVenueFeeWithPriceLager1):(size * price * FeeConf.removeVenueFeeWithPriceSmaler1);
		}
		else if ((strcmp(liquidityField, "OO") == 0) || liquidity == 'O' || liquidity == '5') //Auction
		{
			totalVenueFee = (size * FeeConf.auctionVenueFee);
		}
		else if ((strcmp(liquidityField, "BY") == 0) || (strcmp(liquidityField, "PX") == 0) || 
				(strcmp(liquidityField, "RP") == 0) || (strcmp(liquidityField, "RQ") == 0) || 
				(strcmp(liquidityField, "RR") == 0) || (strcmp(liquidityField, "SW") == 0) || 
				liquidity == 'I' || liquidity == 'K' || liquidity == 'Q' || liquidity == 'R' || 
				liquidity == 'T' || liquidity == 'X' || liquidity == 'Z' || liquidity == '7' || 
				liquidity == 'S') //Route
		{
			totalVenueFee = (size * FeeConf.edgxeRouteVenueFee);
		}
		else
		{
			TraceLog(WARN_LEVEL, "(CalculateFillFee): Invalid liquidity (%c) detected on EDGX\n", liquidity);
		}
	}

	totalFees = totalSecFees + totalTafFee + totalWedbushCommission	+ totalVenueFee;

	return totalFees;
}
