/*****************************************************************************
**	Project:		EAA
**	Filename:		database_util.c
**	Description:	This file contains code to access Postgre Database
					It inherits some functions in the file database_util of Le Quoc Hoang (old TA)
**	Author:			Danh Thai Hoang
**	First created:	08-07-2009
**	Last updated:	-----------
*****************************************************************************/

/*************
INCLUDED FILES
**************/

#include <stdlib.h>
#include "database_util.h"
#include "hbitime.h"

/*****************************************************************************
- Function name:	CreateConnection
- Input:			-  A pointer to the PGconn Object
					-  A string contains connection infomation
					for ex: "dbname=eaa user=postgres password=1qazXSW@ host=localhost"
- Output:			N/A
- Return:			true if successfully,
					false otherwise
- Description:

- Usage:			Call this function when you have the correct parameters
					This function will not check for the validity of the parameters
******************************************************************************/
PGconn *CreateConnection(DatabaseConf_t *dbConf)
{
	// Connection string
	char connectionString[MAX_CONNECTION_STRING_LEN];
	memset(connectionString, ' ', MAX_CONNECTION_STRING_LEN);

	// Index
	int index = 0;

	// Build the connection string
	// Db name
	sprintf(&connectionString[index], "dbname=%s", dbConf->dbname);
	index += strlen(dbConf->dbname) + 7;

	// User name
	sprintf(&connectionString[index], " user=%s", dbConf->username);
	index += strlen(dbConf->username) + 6;

	// Password
	sprintf(&connectionString[index], " password=%s ", dbConf->passwd);
	index += strlen(dbConf->passwd) + 10;

	// Hostname
	sprintf(&connectionString[index], " host=%s ", dbConf->hostname);
	index += strlen(dbConf->dbname) + 6;

	// TraceLog(DEBUG_LEVEL, "Connection string: %s\n", connectionString);

	// Create the connection to Postgres database
	PGconn *connection = PQconnectdb(connectionString);

	// Check if the connection is successful
	if(connection == NULL)
	{
		TraceLog(ERROR_LEVEL, "Unable to allocate connection. Connection String: %s\n", connectionString);
		return NULL;
	}

	if(PQstatus(connection) != CONNECTION_OK)
	{
		TraceLog(ERROR_LEVEL, "Connection Failed!\n");
		TraceLog(ERROR_LEVEL, "Connection Eror Message: %s\n", PQerrorMessage(connection));
		TraceLog(ERROR_LEVEL, "Connection String: %s\n", connectionString);
		return NULL;
	}

	// Connected successfully
	return connection;
}

/*****************************************************************************
- Function name:	DestroyConnection
- Input:			- A pointer to the PGconn Object
- Output:			N/A
- Return:			N/A
- Description:		This function will close connection which was opened with
					CreateConnection function
- Usage:			Call this function when you have the correct parameters
					This function will not check for the validity of the parameters
****************************************************************************/
void DestroyConnection(PGconn *connection)
{
	PQfinish(connection);
}

/*******************************************************************************
- Function name:	ExecuteQuery
- Input:			- A pointer to PGconn Object
					- A query string to be executed
- Output:			N/A
- Return:			A pointer to PGresult Object if successfully,
					NULL otherwise
- Description:

- Usage:			Call this function when you have the correct parameters
					This function will not check for the validity of the parameters
********************************************************************************/
PGresult *ExecuteQuery(PGconn *connection, const char *queryString)
{
	// Querry string declarition
	PGresult *queryResult;

	// Execute Querry
	queryResult = PQexec(connection, queryString);

	// Check if execution querry is sucessful
	if(queryResult == NULL)
	{
		TraceLog(ERROR_LEVEL, "Query Error: %s\n", PQerrorMessage(connection));
		TraceLog(ERROR_LEVEL, "Query String: %s\n", queryString);
		return NULL;
	}

	if((PQresultStatus(queryResult) != PGRES_COMMAND_OK) &&
		(PQresultStatus(queryResult) != PGRES_TUPLES_OK))
	{
		TraceLog(ERROR_LEVEL, "Query Result Status: %s\n", PQresStatus(PQresultStatus(queryResult)));
		TraceLog(ERROR_LEVEL, "Query Result Error Message: %s\n", PQresultErrorMessage(queryResult));
		TraceLog(ERROR_LEVEL, "Query String: %s\n", queryString);
	}

	return queryResult;
}

/***********************************************************************
- Function name:	GetNumberOfRow
- Input:			- A pointer to the PGresult Object
- Output:			N/A
- Return:			An interger for number of rows
- Description:		This function returns the number of rows
- Usage:			Call this function when you have the correct parameters
					This function will not check for the validity of the parameters
************************************************************************/
int GetNumberOfRow(PGresult *queryResult)
{
	return PQntuples(queryResult);
}

/***********************************************************************
- Function name:	GetNumberOfColum
- Input:			- A pointer to the PGresult Object
- Output:			N/A
- Return:			An interger for number of colums
- Description:		This function returns the number of colums

- Usage:			Call this function when you have the correct parameters
					This function will not check for the validity of the parameters
**************************************************************************/
int GetNumberOfColum(PGresult *queryResult)
{
	return PQnfields(queryResult);
}

/***************************************************************************
- Function name:	IsFieldNull
- Input:		- A pointer to the PGresult Object
			- A row index
			- A colum index
- Output:		N/A
- Return:		true if successfully,
			false otherwise
- Description:	This function will check whether a field is NULL or not

- Usage:		Call this function when you have the correct parameters
			This function will not check for the validity of the parameters
***************************************************************************/
int	IsFieldNull(PGresult *queryResult, int rowIndex, int colIndex)
{
	if(PQgetisnull(queryResult, rowIndex, colIndex ))
	{
		return SUCCESS;
	}
	return ERROR;
}

/***************************************************************************
- Function name:	GetValueField
- Input:		- A pointer to the PGresult Object
				- A row index
				- A colum index
- Output:		N/A
- Return:		A value formated as a string
- Description:
- Usage:		Call this function when you have the correct parameters
				This function will not check for the validity of the parameters
****************************************************************************/
char* GetValueField(PGresult *queryResult, int rowIndex, int colIndex)
{
	return PQgetvalue(queryResult, rowIndex, colIndex);
}

/********************************************************************************
- Function name:	ClearResult
- Input:			- A pointer to PGresult Object
- Output:			N/A
- Return:			N/A
- Description:
- Usage:			Call this function when you have the correct parameters
					This function will not check for the validity of the parameters
**********************************************************************************/
void ClearResult(PGresult *queryResult)
{
	PQclear(queryResult);
}

/********************************************************************************
- Function name:	CheckAndCreateNewDatabase
- Input:			Twt2 database configuration
- Output:			N/A
- Return:			N/A
- Description:
- Usage:			Call this function when you have the correct parameters
					This function will not check for the validity of the parameters
**********************************************************************************/
int CheckAndCreateNewDatabase()
{
	// Create connection to postgres
	PGconn *defaultConn;

	// Get current time
	// Get current time
	time_t now = (time_t)hbitime_seconds();

	struct tm *currentlTime = (struct tm *)localtime(&now);

	if (currentlTime == NULL)
	{
		return ERROR;
	}

	// Database configuration
	DatabaseConf_t dbConf;

	//some temp variables
	// Database name
	char dbname[512];

	sprintf(dbname, "%s_%04d%02d", Twt2DatabaseConf.dbprefix, currentlTime->tm_year + 1900, currentlTime->tm_mon + 1);

	// Database Name
	strncpy(dbConf.dbname, dbname, strlen(dbname));

	// Username
	strcpy(dbConf.username, Twt2DatabaseConf.username);

	// Password
	strcpy(dbConf.passwd, Twt2DatabaseConf.passwd);

	// Hostname
	strcpy(dbConf.hostname, Twt2DatabaseConf.hostname);

	// Try connecting to DB
	defaultConn = CreateConnection(&dbConf);

	if (defaultConn != NULL)
	{
		//database has already existed
		DestroyConnection(defaultConn);
		return SUCCESS;
	}

	// This database is not exist, we will create new database from last month database
	char newDbName[512];

	sprintf(newDbName, "%s_%04d%02d", Twt2DatabaseConf.dbprefix, currentlTime->tm_year + 1900, currentlTime->tm_mon + 1);

	char oldDbName[512];
	int oldMon = currentlTime->tm_mon - 1;
	int oldYear = currentlTime->tm_year;
	if (oldMon < 0)
	{
		oldMon += 12;
		oldYear -= 1;
	}

	sprintf(oldDbName, "%s_%04d%02d", Twt2DatabaseConf.dbprefix, oldYear + 1900, oldMon + 1);

	TraceLog(DEBUG_LEVEL, "%s database is not exist, we will create new database from %s database\n", newDbName, oldDbName);

	/*
	# Create backup folder
	mkdir -p ./backup_db/20081001
	cd ./backup_db/20081001
	*/
	// Get path of backup directory
	char pathBackup[512];
	sprintf(pathBackup, "./backup_db_twt2/%04d%02d%02d", currentlTime->tm_year + 1900, currentlTime->tm_mon + 1, currentlTime->tm_mday);

	// Command to create directory
	char command[512];
	sprintf(command, "mkdir -p %s", pathBackup);

	// Create directory
	system(command);

	// Create file: create_new_database_YYYYMM.sh
	char fileNameToCreateDb[512];
	FILE *fpDb;

	sprintf(fileNameToCreateDb, "%s/create_new_database_twt2_%04d%02d.sh", pathBackup, currentlTime->tm_year + 1900, currentlTime->tm_mon + 1);

	TraceLog(DEBUG_LEVEL, "fileNameToCreateDb = %s\n", fileNameToCreateDb);

	fpDb = fopen(fileNameToCreateDb, "w+");

	if (fpDb == NULL)
	{
		TraceLog(ERROR_LEVEL, "Can't open file %s\n", fileNameToCreateDb);

		return ERROR;
	}

	/*
	#!/bin/bash
	*/
	fprintf(fpDb, "#!/bin/bash\n\n");

	/*
	# Create backup folder
	mkdir -p ./backup_db/20091201
	cd ./backup_db/20091201
	*/
	fprintf(fpDb, "# Create backup folder\n");
	fprintf(fpDb, "mkdir -p %s\n", pathBackup);
	fprintf(fpDb, "cd %s\n\n", pathBackup);

	/*
	# For backup dbscripts
	echo "For backup dbscripts for db twt2 "
	/usr/local/pgsql/bin/pg_dump -U postgres -h 10.8.28.205 -F p -s -d eaa10_twt2_200911 -f dbscript_create_eaa10_twt2_200811.sql

	*/
	fprintf(fpDb, "# For backup dbscripts\n");
	fprintf(fpDb, "echo \"For backup dbscripts for db twt2\"\n");

	fprintf(fpDb, "%s/pg_dump -U postgres -h %s -F p -s %s -f dbscript_create_%s.sql\n\n", Twt2DatabaseConf.postgresPath, Twt2DatabaseConf.hostname, oldDbName, oldDbName);

	/*
	# For create new database  twt2
	echo "For create new database : eaa10_twt2_200912"
	/usr/local/pgsql/bin/createdb -U postgres -h 10.8.28.205 eaa10_twt2_200912 -E 'SQL_ASCII'
	*/
	fprintf(fpDb, "# For create new database twt2\n");
	fprintf(fpDb, "echo \"For create new database: %s\"\n", newDbName);

	fprintf(fpDb, "%s/createdb -U postgres -h %s %s -E 'SQL_ASCII'\n\n", Twt2DatabaseConf.postgresPath, Twt2DatabaseConf.hostname, newDbName);

	/*
	# For add dbscripts
	echo "For add dbscripts "
	/usr/local/pgsql/bin/psql -U postgres -h 10.8.28.205 -d eaa10_200810 -f dbscript_create_eaa10_twt2_200912.sql >/dev/null
	*/
	fprintf(fpDb, "# For backup dbscripts\n");
	fprintf(fpDb, "echo \"For backup dbscripts\"\n");

	fprintf(fpDb, "%s/psql -U postgres -h %s %s -f dbscript_create_%s.sql >/dev/null\n", Twt2DatabaseConf.postgresPath, Twt2DatabaseConf.hostname, newDbName, oldDbName);

	fflush(fpDb);
	fclose(fpDb);

	// Execute shell script
	sprintf(command, "chmod 777 %s", fileNameToCreateDb);
	system(command);

	TraceLog(DEBUG_LEVEL, "sh %s >/dev/null\n", fileNameToCreateDb);

	system(fileNameToCreateDb);

	TraceLog(DEBUG_LEVEL, "Create completed new database: %s\n", newDbName);

	return SUCCESS;
}
