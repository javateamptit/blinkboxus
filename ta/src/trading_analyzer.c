/*****************************************************************************
**	Project:		EAA
**	Filename:		trading_analyzer.c
**	Description:	This file contains code to process data for trading analyzer
**	Author:			Danh Thai Hoang
**	First created:	08-07-2009
**	Last updated:	-----------
*****************************************************************************/
#include <time.h>
#include "configuration.h"
#include "trading_analyzer.h"
#include <unistd.h>
#include <fcntl.h>
#include "environment_proc.h"
#include "hbitime.h"

char PathOfCurrentStatus[MAX_PATH_LEN];
char PathOfEaaDatabase[MAX_PATH_LEN];
char PathOfTwt2Database[MAX_PATH_LEN];
char PathOfFeeConfig[MAX_PATH_LEN];

int main(int argc, char**argv)
{
	CreateDataPath();
	
	//Load version information
	SaveLoginInfo();
	
	if (ProcessUserInput(argc, argv) == -1)
	{
		return -1;
	}
	
	if (GetConfigurationPath() == ERROR)
	{
		TraceLog(ERROR_LEVEL, "Could not get all configuration path\n");
		return ERROR;
	}
	
	if (LoadTradingTimeConfiguration() == ERROR)
	{
		TraceLog(ERROR_LEVEL, "Could not load trading time configuration\n");
		return ERROR;
	}

	// Load database configuration
	if (LoadDatabaseConfiguration(PathOfEaaDatabase, &EaaDatabaseConf) == ERROR)
	{
		return ERROR;
	}
	
	if (LoadDatabaseConfiguration(PathOfTwt2Database, &Twt2DatabaseConf) == ERROR)
	{
		return ERROR;
	}

	// Load fee configuration
	if (LoadFeeConf(PathOfFeeConfig, &FeeConf) == ERROR)
	{
		return ERROR;
	}

	// Register handlers
	RegisterSignalHandlers();

	//Init Data Structure
	InitDataStructure();

	// Load Data configuration
	LoadDataConfiguration();

	// Check and Create DB
	CheckAndCreateNewDatabase();

	// Start processing
	StartTradingAnalyzer();

	return 0;
}

/***********************
FUNCTIONS IMPLEMENTATION
************************/
/****************************************************************************
- Function name:	RegisterSignalHandlers
- Input:			N/A
- Output:			N/A
- Return:
- Description:
- Usage:			Catch some system signal
****************************************************************************/
int RegisterSignalHandlers(void)
{
	TraceLog(DEBUG_LEVEL, "Register signal handlers\n");

	// SEGMENTATION FAULT
	signal(SIGSEGV, ProcessSegmentationFault);

	// INTERUPT
	signal(SIGINT, ProcessInterrupt);

	return SUCCESS;
}
/****************************************************************************
- Function name:	ProcessSegmentationFault
- Input:			N/A
- Output:			N/A
- Return:
- Description:
- Usage:			Catch some  segmantation Fault signal
****************************************************************************/
void ProcessSegmentationFault(int sig_num)
{
	TraceLog(DEBUG_LEVEL, "Processing segmentation Fault!!\n");

	// Catch signal
	signal(SIGSEGV, ProcessSegmentationFault);

	// Exit
	exit(0);
}

/****************************************************************************
- Function name:	ProcessInterrupt
- Input:			N/A
- Output:			N/A
- Return:
- Description:
- Usage:			Catch some  Interupt signal
****************************************************************************/
void ProcessInterrupt(int sig_num)
{
	TraceLog(DEBUG_LEVEL, "Processing Interupt Signal!!\n");

	// Catch signal
	signal(SIGINT, ProcessInterrupt);

	// Exit
	exit(0);
}

/****************************************************************************
- Function name:	StartTradingAnalyzer()
- Input:			N/A
- Output:			N/A
- Return:
- Description:
- Usage:			Process data on current date
****************************************************************************/
int StartTradingAnalyzer(void)
{
	// Some local variables
	// Current time
	int currentTime;

	// Current Index
	int currentIndex;

	currentTime = GetCurrentDate(NOT_UPDATED);

	// Check date time
	if(OldDateNumber < CurrentDateNumber)
	{
		int BeginDateInSeconds = GetNumberOfSecondsFrom1970(LastProcessedDate);
		int EndDateInSeconds = GetNumberOfSecondsFrom1970(LastUpdatedDate);

		TraceLog(DEBUG_LEVEL, "Begin process data from %s to %s...\n", LastProcessedDate, LastUpdatedDate);

		// Process Old Data (old date)
		ProcessDataOnOldDate(BeginDateInSeconds, EndDateInSeconds);

		TraceLog(DEBUG_LEVEL, "Processed data on old date from %d to %d\n", BeginDateInSeconds, EndDateInSeconds);

		// Update Old Date
		OldDateNumber = CurrentDateNumber;

		// Last Processed Index
		LastProcessedIndex = 0;
	}

	if (OldDateNumber == CurrentDateNumber)
	{
		currentTime = GetCurrentDate(UPDATED);

		// Get Index
		currentIndex = (currentTime - BeginTradingTimeInSeconds) / NUM_SECONDS_PER_PRIMETIVE_STEP;

		// Check the valid current index
		if(currentIndex > MaxIndexPerDay)
		{
			currentIndex = MaxIndexPerDay;
		}

		if (LastProcessedIndex < currentIndex)
		{
			//Do something to process data for the current day as offline mode
			if (LastProcessedIndex < currentIndex)
			{
				TraceLog(DEBUG_LEVEL, "Begin process offline data for Current Day\n");

				// Start calculations here
				ProcessOfflineOrderAtCurrentPosition(LastProcessedIndex, LastProcessedDate, 0);

				// Update Index
				//LastProcessedIndex += 1;

				// Check if date is the end of day
				if(LastProcessedIndex == MaxIndexPerDay)
				{
					TraceLog(DEBUG_LEVEL, "End of day..\n");
				}
			}

			SaveProcessedPosition(PathOfCurrentStatus, LastProcessedIndex, LastProcessedDate);

			time_t temp = GetNumberOfSecondsFrom1970(LastProcessedDate);
			struct tm * tv = localtime(&temp);

			//printf("tv->tm_year = %d\n", tv->tm_year);
			//printf("tv->tm_mon = %d\n", tv->tm_mon);

			// Update database name
			sprintf(EaaDatabaseConf.dbname, "%s_%04d%02d", EaaDatabaseConf.dbprefix, tv->tm_year + 1900, tv->tm_mon + 1);
			sprintf(Twt2DatabaseConf.dbname, "%s_%04d%02d", Twt2DatabaseConf.dbprefix, tv->tm_year + 1900, tv->tm_mon + 1);

			// Save db Configuration
			SaveDatabaseConfiguration(PathOfEaaDatabase);
			SaveDatabaseConfiguration(PathOfTwt2Database);
		}
	}
	TaStatus = ONLINE;

	while(TRUE)
	{
		// Get Current Time
		currentTime = GetCurrentDate(UPDATED);

		// Get Index
		currentIndex = (currentTime - BeginTradingTimeInSeconds) / NUM_SECONDS_PER_PRIMETIVE_STEP;

		if(currentIndex > MaxIndexPerDay)
		{
			currentIndex = MaxIndexPerDay;
		}
		//TraceLog(DEBUG_LEVEL, "Process data of current date: CurrentIndex = %d, LastIndexProce1ssed = %d, date = %s\n", currentIndex, LastProcessedIndex, LastUpdatedDate);

		//TraceLog(DEBUG_LEVEL, "TA: currentIndex=%d LastProcessedIndex=%d\n", currentIndex, LastProcessedIndex);

		while(LastProcessedIndex <= currentIndex)
		{
			if(currentIndex == 0)
			{
				// Reset profit/loss structure
				prevProfitLoss.totalVolume = 0;
				prevProfitLoss.profitLoss = 0.0;
				prevProfitLoss.totalMarket = 0.0;
				prevProfitLoss.netProfitLoss = 0.0;
				prevProfitLoss.expectedProfit = 0.0;
			}

			TraceLog(DEBUG_LEVEL, "Process online data: LastProcessedIndex = %d\n", LastProcessedIndex);
			// Start calculations here
			ProcessOrderAtCurrentPosition(LastProcessedIndex, LastProcessedDate);

			// Update Index
			LastProcessedIndex += 1;

			// Check if date is the end of day
			if(LastProcessedIndex == MaxIndexPerDay)
			{
				TraceLog(DEBUG_LEVEL, "End of day..\n");
			}

			//printf("LastProcessedIndex (ONLINE) = %d \n", LastProcessedIndex );

			// Save the current position
			SaveProcessedPosition(PathOfCurrentStatus, LastProcessedIndex, LastProcessedDate);
		}

		SaveProcessedPosition(PathOfCurrentStatus, LastProcessedIndex, LastProcessedDate);

		// Handle timne from 1970
		time_t temp = GetNumberOfSecondsFrom1970(LastProcessedDate);
		struct tm * tv = localtime(&temp);

		// Update database name
		sprintf(EaaDatabaseConf.dbname, "%s_%04d%02d", EaaDatabaseConf.dbprefix, tv->tm_year + 1900, tv->tm_mon + 1);
		sprintf(Twt2DatabaseConf.dbname, "%s_%04d%02d", Twt2DatabaseConf.dbprefix, tv->tm_year + 1900, tv->tm_mon + 1);

		// Save db Configuration
		SaveDatabaseConfiguration(PathOfEaaDatabase);
		SaveDatabaseConfiguration(PathOfTwt2Database);

		//TraceLog(DEBUG_LEVEL, "Processing data online: LastProcessedIndex=%d LastProcessedDate=%s\n", LastProcessedIndex, LastProcessedDate);
		sleep(30);
	}

	return SUCCESS;
}

/****************************************************************************
- Function name:	ProcessDataOnOldDate
- Input:			+ Begin date in seconds
					+ End date in seconds
- Output:			N/A
- Return:
- Description:
- Usage:			Process data on old date
****************************************************************************/
int ProcessDataOnOldDate(int beginDateInSeconds, int endDateInSeconds)
{
	struct tm *dateTime;
	struct tm *tv;
	time_t temp;

	TraceLog(DEBUG_LEVEL, "Processing data of old date: begin time %d, end time %d\n", beginDateInSeconds, endDateInSeconds);
	
	// current date in seconds
	time_t currentDateInSeconds = beginDateInSeconds;

	if(LastProcessedIndex >= MaxIndexPerDay)
	{
		// Inrease seconds of one day
		//currentDateInSeconds += 24 * 60 * 60;
		currentDateInSeconds += SECOND_IN_ONE_DAY;

		// Reset Index
		LastProcessedIndex = 0;

		// Reset profit/loss structure
		prevProfitLoss.totalVolume = 0;
		prevProfitLoss.profitLoss = 0.0;
		prevProfitLoss.totalMarket = 0.0;
		prevProfitLoss.netProfitLoss = 0.0;
		prevProfitLoss.expectedProfit = 0.0;

		memset(&SymbolTradinInfoList, 0, sizeof(SymbolTradinInfoList));

		// Get local time
		dateTime = localtime(&currentDateInSeconds);

		// Format time
		strftime(LastProcessedDate, MAX_LINE_LENGTH, "%Y/%m/%d", dateTime);

		// Save the current Position
		SaveProcessedPosition(PathOfCurrentStatus, LastProcessedIndex, LastProcessedDate);
	}

	while(currentDateInSeconds < endDateInSeconds)
	{
		//PathDirectoryOfTradingData to be a global variable
		dateTime = localtime(&currentDateInSeconds);

		//	Number of seconds since 1970
		NumOfSecsFrom1970 = currentDateInSeconds;
		TraceLog(DEBUG_LEVEL, "NumOfSecsFrom1970 = %d\n", NumOfSecsFrom1970);

		// Format time
		strftime(LastProcessedDate, MAX_LINE_LENGTH, "%Y/%m/%d", dateTime);

		//	Format is YYYYMMDD
		OldDateNumber = dateTime->tm_year * 10000 + dateTime->tm_mon * 100 + dateTime->tm_mday;

		// Handle timne from 1970
		temp = currentDateInSeconds + 3600;
		tv = localtime(&temp);

		// Update database name
		sprintf(EaaDatabaseConf.dbname, "%s_%04d%02d", EaaDatabaseConf.dbprefix, tv->tm_year + 1900, tv->tm_mon + 1);
		sprintf(Twt2DatabaseConf.dbname, "%s_%04d%02d", Twt2DatabaseConf.dbprefix, tv->tm_year + 1900, tv->tm_mon + 1);

		// Save db Configuration
		SaveDatabaseConfiguration(PathOfEaaDatabase);
		SaveDatabaseConfiguration(PathOfTwt2Database);

		//	We only trade from Monday to Friday
		if ((dateTime->tm_wday > 0) && (dateTime->tm_wday < 6))
		{
			// Process data at index
			ProcessOfflineOrderAtCurrentPosition(LastProcessedIndex, LastProcessedDate, 1);

			//	Update position processed
			SaveProcessedPosition(PathOfCurrentStatus, LastProcessedIndex, LastProcessedDate);

			//Update Data for End Day
			TraceLog(DEBUG_LEVEL, "Process End Day data ...\n");

			// Process data on the end of day

			TraceLog(DEBUG_LEVEL, "End Of Day...\n");

			// Reset the current index
			LastProcessedIndex = 0;

			// Reset profit/loss structure
			prevProfitLoss.totalVolume = 0;
			prevProfitLoss.profitLoss = 0.0;
			prevProfitLoss.totalMarket = 0.0;
			prevProfitLoss.netProfitLoss = 0.0;
			prevProfitLoss.expectedProfit = 0.0;

			// Init trading information structure
			int i;
			for(i = 0;i < SymbolTradinInfoList.countSymbol; i++)
			{
				SymbolTradinInfoList.TradinInfoList[i].totalVolume = 0;
				SymbolTradinInfoList.TradinInfoList[i].profitLoss = 0.0;
				SymbolTradinInfoList.TradinInfoList[i].totalMarket = 0.0;
				SymbolTradinInfoList.TradinInfoList[i].avgPrice = 0.00;
				SymbolTradinInfoList.TradinInfoList[i].leftShares = 0;
			}
		}
		else
		{
			TraceLog(DEBUG_LEVEL, "This is day off.\n");

			if (dateTime->tm_wday == 6)
			{
				TraceLog(DEBUG_LEVEL, "Because it is Saturday\n\n");
			}
			else
			{
				TraceLog(DEBUG_LEVEL, "Because it is Sunday\n\n");
			}

			LastProcessedIndex = 0;

			// Reset profit/loss structure
			prevProfitLoss.totalVolume = 0;
			prevProfitLoss.profitLoss = 0.0;
			prevProfitLoss.totalMarket = 0.0;
			prevProfitLoss.netProfitLoss = 0.0;
			prevProfitLoss.expectedProfit = 0.0;

			//memset(&SymbolTradinInfoList, 0, sizeof(SymbolTradinInfoList));
			int i;
			for(i = 0;i < SymbolTradinInfoList.countSymbol;i++)
			{
				SymbolTradinInfoList.TradinInfoList[i].totalVolume = 0;
				SymbolTradinInfoList.TradinInfoList[i].profitLoss = 0.0;
				SymbolTradinInfoList.TradinInfoList[i].totalMarket = 0.0;
				SymbolTradinInfoList.TradinInfoList[i].avgPrice = 0.00;
				SymbolTradinInfoList.TradinInfoList[i].leftShares = 0;
			}

			//	Update position processed
			SaveProcessedPosition(PathOfCurrentStatus, LastProcessedIndex, LastProcessedDate);
		}

		//	Increasing 1 day
		//currentDateInSeconds += 24 * 60 * 60;
		currentDateInSeconds += SECOND_IN_ONE_DAY;

		time_t currentTimeInSeconds = currentDateInSeconds + BeginTradingTimeInSeconds + 3600;
		struct tm *dbTime = localtime(&currentTimeInSeconds);

		// Update the database connnection string string
		memset(EaaDatabaseConf.dbname, 0, 32);
		memset(Twt2DatabaseConf.dbname, 0, 32);

		// Update database name
		sprintf(EaaDatabaseConf.dbname, "%s_%04d%02d", EaaDatabaseConf.dbprefix, dbTime->tm_year + 1900, dbTime->tm_mon + 1);
		sprintf(Twt2DatabaseConf.dbname, "%s_%04d%02d", Twt2DatabaseConf.dbprefix, dbTime->tm_year + 1900, dbTime->tm_mon + 1);

		// Save db Configuration
		SaveDatabaseConfiguration(PathOfEaaDatabase);
		SaveDatabaseConfiguration(PathOfTwt2Database);
	}

	//Local Time
	dateTime = localtime(&currentDateInSeconds);

	//	Number of seconds since 1970
	NumOfSecsFrom1970 = currentDateInSeconds;
	//printf("NumOfSecsFrom1970 = %d\n", NumOfSecsFrom1970);

	// Format Date Time
	strftime(LastProcessedDate, MAX_LINE_LENGTH, "%Y/%m/%d", dateTime);

	// Reset index
	LastProcessedIndex = 0;

	// Reset profit/loss structure
	prevProfitLoss.totalVolume = 0;
	prevProfitLoss.profitLoss = 0.0;
	prevProfitLoss.totalMarket = 0.0;
	prevProfitLoss.netProfitLoss = 0.0;
	prevProfitLoss.expectedProfit = 0.0;

	//memset(&SymbolTradinInfoList, 0, sizeof(SymbolTradinInfoList));

	TraceLog(DEBUG_LEVEL, "LastProcessedDate = %s\n", LastProcessedDate);
	/*
	printf("prevProfitLoss.totalVolume = %d\n", prevProfitLoss.totalVolume);
	printf("prevProfitLoss.profitLoss = %lf\n", prevProfitLoss.profitLoss);
	printf("prevProfitLoss.totalMarket = %lf\n", prevProfitLoss.totalMarket);
	*/

	int i;
	for(i = 0; i < SymbolTradinInfoList.countSymbol; i++)
	{
		SymbolTradinInfoList.TradinInfoList[i].totalVolume = 0;
		SymbolTradinInfoList.TradinInfoList[i].profitLoss = 0.0;
		SymbolTradinInfoList.TradinInfoList[i].totalMarket = 0.0;
		SymbolTradinInfoList.TradinInfoList[i].avgPrice = 0.00;
		SymbolTradinInfoList.TradinInfoList[i].leftShares = 0;
	}

	//	Format is YYYYMMDD
	OldDateNumber = dateTime->tm_year * 10000 + dateTime->tm_mon * 100 + dateTime->tm_mday;

	return SUCCESS;
}


/****************************************************************************
- Function name:	ProcessUserInput
- Input:
- Output:			N/A
- Return:
- Description:
- Usage:			Call in main function to parse user input arguments
****************************************************************************/
int ProcessUserInput(int argc, char *argv[])
{
	const char *optString = "hv";
	const char* program_name;

	program_name = argv[0];

	static struct option long_opt[] = {
					{"help", 0, NULL,'h'},
					{"version", 0 , NULL , 'v'},
					{NULL, 0, NULL, 0}
				};

	int valOpt, option_index = -1;


	while (1)
	{
		valOpt = getopt_long(argc, argv, optString, long_opt, &option_index);

		if (valOpt == -1)
		{
			break;
		}

		switch (valOpt)
		{
			case 'h':
				Print_Help(program_name);
				return -1;	//This will terminate the programe.
				break;

			case 'v':
				TraceLog(NO_LOG_LEVEL, "************************************************************\n");
				TraceLog(NO_LOG_LEVEL, "Version of TA2: '%s' \n", VERSION);
				TraceLog(NO_LOG_LEVEL, "************************************************************\n");

				return -1;
			default:
				TraceLog(ERROR_LEVEL, "Invalid commands. \"-h\" or \"--help\" for list commands\n\n");
				return -1;
		}
	}

	return 0;
}

/****************************************************************************
- Function name:	Print_Help
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int Print_Help(const char *program_name)
{
	TraceLog(NO_LOG_LEVEL, "**********************************************************************\n");
	TraceLog(NO_LOG_LEVEL, "* -h --help for list commands\n");
	TraceLog(NO_LOG_LEVEL, "* -v --version for show TA2's version \n\n");
	TraceLog(NO_LOG_LEVEL, "* Example: To show TA's version\n\n");
	TraceLog(NO_LOG_LEVEL, "*\t$%s --version\n", program_name);
	TraceLog(NO_LOG_LEVEL, "**********************************************************************\n\n");

	return 0;
}

/****************************************************************************
- Function name:	SaveLoginInfo
- Input:			+
					+
- Output:			N/A
- Return:
- Description:
- Usage:
****************************************************************************/
int SaveLoginInfo()
{
	//Initialize version
	#ifdef VERSION
		TraceLog(DEBUG_LEVEL, "Current version: %s\n", VERSION);
	#else
		TraceLog(DEBUG_LEVEL, "Current version: UNKNOWN\n");
	#endif

	int hour, min, sec, date, year, month;
	struct tm *timeNow;
	time_t now = (time_t)hbitime_seconds();
	timeNow= (struct tm *) localtime(&now);

	char fullPath[MAX_PATH_LEN] = "\0";
	Environment_get_data_filename("version_log.txt", fullPath, MAX_PATH_LEN);
	int file_desc;

	struct flock region_to_lock;
	int res;
	char byte_write[80];
	memset(byte_write, 0, 80);

	//Get time when LCT started
	hour 	= timeNow->tm_hour;
	min 	= timeNow->tm_min;
	sec 	= timeNow->tm_sec;
	date 	=  timeNow->tm_mday;
	month 	= 1 + timeNow->tm_mon;
	year 	= 1900 + timeNow->tm_year;

	//Initialize locking file
	region_to_lock.l_type = F_WRLCK; //Exclusive lock
	region_to_lock.l_whence = SEEK_SET; //Log from the beginning of the file
	file_desc = open(fullPath, O_RDWR | O_CREAT | O_APPEND, 0666);
	if (!file_desc)
	{
		TraceLog(ERROR_LEVEL, "Cannot open file '%s' to read or we don't have permission to create\n", fullPath);
		return -1;
	}
	res = fcntl(file_desc, F_SETLKW, &region_to_lock);
	if (res == -1)
	{
		fprintf(stderr, "Failed to lock region \n");
	}
	sprintf(byte_write,"%d-%.2d-%.2d %.2d:%.2d:%.2d - TA2 \t VERSION: %s \n", year, month, date, hour, min, sec, VERSION);
	write(file_desc, byte_write, strlen(byte_write));
	close(file_desc);

	return 0;
}

/****************************************************************************
- Function name:	GetConfigurationPath
- Input:
- Output:			Global path of configuration files for TA
- Return:			SUCCESS or ERROR
- Description:
- Usage:			Call this function only once when TA started
****************************************************************************/
int GetConfigurationPath()
{
	if (Environment_get_ta2_conf_filename("current_status", PathOfCurrentStatus, MAX_PATH_LEN) == 0)
	{
		TraceLog(ERROR_LEVEL, "Could not get path of current_status\n");
		return ERROR;
	}
	
	if (Environment_get_ta2_conf_filename("EaaDb", PathOfEaaDatabase, MAX_PATH_LEN) == 0)
	{
		TraceLog(ERROR_LEVEL, "Could not get path of EaaDb\n");
		return ERROR;
	}
	
	if (Environment_get_ta2_conf_filename("Twt2Db", PathOfTwt2Database, MAX_PATH_LEN) == 0)
	{
		TraceLog(ERROR_LEVEL, "Could not get path of Twt2Db\n");
		return ERROR;
	}
	
	if (Environment_get_ta2_conf_filename("fee_config", PathOfFeeConfig, MAX_PATH_LEN) == 0)
	{
		TraceLog(ERROR_LEVEL, "Could not get path of fee_config\n");
		return ERROR;
	}
	
	TraceLog(DEBUG_LEVEL, "current_status path: '%s'\n", PathOfCurrentStatus);
	TraceLog(DEBUG_LEVEL, "EaaDb path:          '%s'\n", PathOfEaaDatabase);
	TraceLog(DEBUG_LEVEL, "Twt2Db path:         '%s'\n", PathOfTwt2Database);
	TraceLog(DEBUG_LEVEL, "fee_config path:     '%s'\n", PathOfFeeConfig);

	return SUCCESS;
}

/****************************************************************************
- Function name:	LrcPrintfLogFormat
- Input:
- Output:			N/A
- Return:			N/A
- Description:
- Usage:			N/A
****************************************************************************/
void LrcPrintfLogFormat(int level, char *fmt, ...)
{
	char formatedStr[5120] = "\0";
	switch (level)
	{
		case DEBUG_LEVEL:
			strcpy(formatedStr, DEBUG_STR);
			break;
		case NOTE_LEVEL:
			strcpy(formatedStr, NOTE_STR);
			break;
		case WARN_LEVEL:
			strcpy(formatedStr, WARN_STR);
			break;
		case ERROR_LEVEL:
			strcpy(formatedStr, ERROR_STR);
			break;
		case FATAL_LEVEL:
			strcpy(formatedStr, FATAL_STR);
			break;
		case USAGE_LEVEL:
			strcpy(formatedStr, USAGE_STR);
			break;
		case STATS_LEVEL:
			strcpy(formatedStr, STATS_STR);
			break;
		default:
			formatedStr[0] = 0;
			break;
	}
	strcat(formatedStr, fmt);
	
	va_list ap;
	va_start (ap, fmt);
	
	vprintf (formatedStr, ap);

	va_end (ap);

	fflush(stdout);
}

/****************************************************************************
- Function name:	CreateDataPath
- Input:			N/A
- Output:		N/A
- Return:		Success or Failure
- Description:
- Usage:			N/A
****************************************************************************/
int CreateDataPath(void)
{
	char fullRawDataPath[512] = "\0";
	// Build full raw data path
	Environment_get_root_data_dir(fullRawDataPath, MAX_PATH_LEN);

	// Command to create directories
	char command[MAX_PATH_LEN];
	sprintf(command, "mkdir -p %s", fullRawDataPath);

	// Create directory
	system(command);

	TraceLog(DEBUG_LEVEL, "Root Data path: %s\n", fullRawDataPath);

	return SUCCESS;
}
