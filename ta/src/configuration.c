/*****************************************************************************
**	Project:		EAA
**	Filename:		configuration.c
**	Description:	This file included some functions for process TA configuration
**	Author:			Danh Thai Hoang
**	First created:	08-07-2009
**	Last updated:	-----------
*****************************************************************************/
#define _XOPEN_SOURCE
#include <time.h>
#include "configuration.h"
#include "trading_analyzer.h"
#include "environment_proc.h"
#include "parameters_proc.h"

/*******************
GLOBAL VARIABLE
*****************/
// EAA Database configuration
DatabaseConf_t EaaDatabaseConf;

// TWT2 Database configuration
DatabaseConf_t Twt2DatabaseConf;// Old Date

// Fee configuration
t_FeeConf FeeConf;

extern int OldDateNumber;

// Last date when data was updated
extern char LastUpdatedDate[MAX_LINE_LENGTH];

// Last date when data was processed
extern char LastProcessedDate[MAX_LINE_LENGTH];

// Last processed index
extern int LastProcessedIndex;

extern int EntryExit[NUM_OF_TS][NUM_OF_ECN + 1][NUM_OF_SIDE][NUM_OF_ENTRY_EXIT_STATUS];

extern int ExitOrderShares[NUM_OF_TS][NUM_OF_ECN + 1][ENTRY_EXIT][NUM_OF_ECN_LAUNCH][NUM_OF_ISO_ORDER][NUM_OF_SHARE_TYPE];

extern int StockTypes[NUM_OF_TS][NUM_OF_ECN][NUM_OF_STOCK_TYPES][NUM_OF_TIF][NUM_OF_STOCK_TYPES_STATUS];

extern int OrderTypes[NUM_OF_TS][NUM_OF_ECN + 1][ENTRY_EXIT][NUM_OF_ORDER_TYPE][NUM_OF_ORDER_STATUS];// define ENTRY_EXIT = 2

extern int AskBidLaunch[NUM_OF_TS][NUM_OF_ECN][NUM_OF_ECN_LAUNCH][ASK_BID][NUM_OF_ASK_BID_STATUS];

extern int Exit_AskBidLaunch[NUM_OF_TS][NUM_OF_ECN][NUM_OF_ECN_LAUNCH][ASK_BID][NUM_OF_ASK_BID_STATUS];

// Trading time in seconds
int BeginTradingTimeInSeconds;
int EndTradingTimeInSeconds;
int MaxIndexPerDay;

/**********************
FUNCTIONS IMPLEMENTATION
***********************/

/*****************************************************************************
- Function name:	LoadDatabaseConficuration
- Input:			- File name
					- Database configuration
- Output:			- Database configuration
- Return:			- void
- Description:
- Usage				- This function will load from file information of database including
						+ database name
						+ username
						+ passwd
						+ hostname
******************************************************************************/
int LoadDatabaseConfiguration(char *fileName, DatabaseConf_t *dbConf)
{
	if(fileName == NULL)
	{
		TraceLog(ERROR_LEVEL, "This file name %s is null\n", fileName);
		return ERROR;
	}
	
	TraceLog(DEBUG_LEVEL, "Loading %s\n", fileName);

	// Open this file to read data
	FILE *fp = fopen(fileName, "r");

	// Line number
	int lineNo = 0;

	// Buffer for each line in data file
	char lineBuf[MAX_LINE_LENGTH];

	//Check if openning file is successful
	if(fp == NULL)
	{
		TraceLog(ERROR_LEVEL, "We can not open database config %s\n", fileName);
		return ERROR;
	}

	// Memset buffer
	memset(lineBuf, 0, MAX_LINE_LENGTH);

	// File Open sucessfully
	// Read data
	while(fgets(lineBuf, MAX_LINE_LENGTH, fp) != NULL)
	{
		if(!strchr(lineBuf, '#'))
		{
			RemoveCharacters(lineBuf, MAX_LINE_LENGTH);
			
			// Increase line number
			lineNo++;

			// Data
			if(lineNo == 1)
			{
				// Database Name
				strncpy(dbConf->dbname, lineBuf, strlen(lineBuf));
			}
			else if(lineNo == 2)
			{
				// database prefix
				strncpy(dbConf->dbprefix, lineBuf, strlen(lineBuf));
			}
			else if(lineNo == 3)
			{
				// Username
				strncpy(dbConf->username, lineBuf, strlen(lineBuf));
			}
			else if(lineNo == 4)
			{
				strncpy(dbConf->passwd, lineBuf, strlen(lineBuf));
			}
			else if(lineNo == 5)
			{
				strncpy(dbConf->hostname, lineBuf, strlen(lineBuf));
			}
			else if(lineNo == 6)
			{
				strncpy(dbConf->postgresPath, lineBuf, strlen(lineBuf));
			}
			else
			{
				TraceLog(ERROR_LEVEL, "This configuration file included invalid data lines\n");
				return ERROR;
			}

			// Reset buffer
			memset(lineBuf, 0, MAX_LINE_LENGTH);
		}
	}

	TraceLog(DEBUG_LEVEL, "DB: '%s'\n", dbConf->dbname);
	TraceLog(DEBUG_LEVEL, "DB Prefix: '%s'\n", dbConf->dbprefix);
	TraceLog(DEBUG_LEVEL, "Username: '%s'\n", dbConf->username);
	TraceLog(DEBUG_LEVEL, "Password: '%s'\n", dbConf->passwd);
	TraceLog(DEBUG_LEVEL, "Hostname: '%s'\n", dbConf->hostname);
	TraceLog(DEBUG_LEVEL, "Posgres Path: '%s'\n", dbConf->postgresPath);
	return SUCCESS;
}

/****************************************************************************
- Function name:	LoadTradingTimeConfiguration
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int LoadTradingTimeConfiguration()
{
	TraceLog(DEBUG_LEVEL, "Loading trading time configuration\n");
	
	char tradingTimePath[MAX_PATH_LEN];
	if (Environment_get_ta2_conf_filename("trading_time", tradingTimePath, MAX_PATH_LEN) == 0)
	{
		TraceLog(ERROR_LEVEL, "Could not get trading_time path\n");
		return ERROR;
	}
	
	//--------------------------------
	// Get the values from files
	//--------------------------------
	int retVal = 0;
	retVal += GetTimeInSecondsFromFile(tradingTimePath, &BeginTradingTimeInSeconds, "start_of_day");
	retVal += GetTimeInSecondsFromFile(tradingTimePath, &EndTradingTimeInSeconds, "end_of_day");
	
	if (retVal != 0)
	{
		return ERROR;
	}
	
	// Calculate Max Index Per Day
	MaxIndexPerDay = ((EndTradingTimeInSeconds - BeginTradingTimeInSeconds)/ (NUM_SECONDS_PER_PRIMETIVE_STEP)) + 1;
	
	TraceLog(DEBUG_LEVEL, "Start of day (in seconds): %d\n", BeginTradingTimeInSeconds);
	TraceLog(DEBUG_LEVEL, "End of day (in seconds): %d\n", EndTradingTimeInSeconds);
	TraceLog(DEBUG_LEVEL, "Max index per day: %d\n", MaxIndexPerDay);

	return SUCCESS;
}

/****************************************************************************
- Function name:	GetNumberOfSecondFromTimeString
- Input:			
- Output:		
- Return:			The number of second from time format hh:mm:ss
- Description:
- Usage:		
****************************************************************************/
int GetNumberOfSecondFromTimeString(const char *timeString)
{
	// time string format: hh:mm:ss
	struct tm tm;
	memset(&tm, 0, sizeof(struct tm));
	char *retVal = strptime(timeString, "%H:%M:%S", &tm);
	if (retVal == NULL)
	{
		return -1;
	}
	
	int numSeconds = tm.tm_sec + tm.tm_min * 60 + tm.tm_hour * 3600;
	return numSeconds;
}

/****************************************************************************
- Function name:	GetTimeInSecondsFromFile
- Input:			
- Output:		
- Return:		
- Description:
- Usage:		
****************************************************************************/
int GetTimeInSecondsFromFile(char fullPath[MAX_PATH_LEN], int *numSeconds, char wantedString[MAX_LINE_LENGTH])
{
	char const* timeString = Parameters_get(fullPath, wantedString, 0);
	if (timeString != 0)
	{
		*numSeconds = GetNumberOfSecondFromTimeString(timeString);
		if (*numSeconds == -1)
		{
			TraceLog(ERROR_LEVEL, "Invalid time of '%s' from file '%s'\n", wantedString, fullPath);
			return -1;
		}
	}
	else
	{
		TraceLog(ERROR_LEVEL, "Could not get '%s' from file '%s'\n", wantedString, fullPath);
		return -1;
	}
	
	return 0;
}

/*****************************************************************************
- Function name:	LoadProcessedPosition
- Input:			- File name
					- Index
					- Date Time
- Output:			- Index
					- Date Time
- Return:			- void
- Description:
- Usage				- This function will load from file information about the processed data
						+ Step
						+ Date time
******************************************************************************/
int LoadProcessedPosition(const char *fileName, int *index, char *dateTime)
{
	if(fileName == NULL)
	{
		TraceLog(ERROR_LEVEL, "The file name %s is null\n", fileName);
		return ERROR;
	}
	
	TraceLog(DEBUG_LEVEL, "Loading %s\n", fileName);

	// Open file
	FILE *currenStatusFile = fopen(fileName, "r");

	// poiters point to the last step and the last date (processed position)
	char pLastIndex[MAX_LINE_LENGTH];
	char pLastDate[MAX_LINE_LENGTH];

	// Reset buffer
	memset(pLastIndex, 0, MAX_LINE_LENGTH);
	memset(pLastDate, 0, MAX_LINE_LENGTH);

	// Line number
	int lineNo = 0;

	// Data Buffer
	char dataBuffer[MAX_LINE_LENGTH];

	// Check if this file open sucessfully
	if(currenStatusFile == NULL)
	{
		TraceLog(ERROR_LEVEL, "We can not open file %s\n", fileName);
		return ERROR;
	}

	while(fgets(dataBuffer, MAX_LINE_LENGTH, currenStatusFile) != NULL)
	{
		if(!strchr(dataBuffer, '#'))
		{
			RemoveCharacters(dataBuffer, MAX_LINE_LENGTH);
			
			// Increase line number
			lineNo++;

			// Data
			if(lineNo == 1)
			{
				// Database Name
				strncpy(pLastIndex, dataBuffer, strlen(dataBuffer));
			}
			else if(lineNo == 2)
			{
				// Username
				strncpy(pLastDate, dataBuffer, strlen(dataBuffer));
			}
			else
			{
				TraceLog(ERROR_LEVEL, "This configuration file included invalid data lines\n");
				return ERROR;
			}

			// Reset buffer
			memset(dataBuffer, 0, MAX_LINE_LENGTH);
		}
	}

	// Update Index
	*index = atoi(pLastIndex);
	TraceLog(DEBUG_LEVEL, "Previous Processed Position: index = %d\n", *index);

	// Update time
	memcpy(dateTime, pLastDate, strlen(pLastDate));

	TraceLog(DEBUG_LEVEL, "date = %s\n", pLastDate);

	// Close file
	fclose(currenStatusFile);

	return *index;
}

/*****************************************************************************
- Function name:	SaveProcessedPosition
- Input:			- File name
					- Index
					- Date Time
- Output:			- Index
					- Date Time
- Return:			- void
- Description:
- Usage				- This function will save information about the processed data to file
						+ Step
						+ Date time
******************************************************************************/
int SaveProcessedPosition(const char *fileName, int index, const char *dateTime)
{
	if(fileName == NULL)
	{
		TraceLog(ERROR_LEVEL, "The file name %s is null\n", fileName);
		return ERROR;
	}

	// Open file to write
	FILE *currenStatusFile = fopen(fileName, "w");

	// Check if openning is successful
	if(currenStatusFile == NULL)
	{
		TraceLog(ERROR_LEVEL, "We can not open the file %s to write\n", fileName);
		return ERROR;
	}

	// Steps
	fprintf(currenStatusFile, "#Steps\n");
	fprintf(currenStatusFile, "%d\n", index);

	// Date
	fprintf(currenStatusFile, "#Date\n");
	fprintf(currenStatusFile, "%s\n", dateTime);

	// Flush
	fflush(currenStatusFile);

	// Close file
	fclose(currenStatusFile);

	return SUCCESS;
}
/*****************************************************************************
- Function name:	SaveDatabaseConfiguration
- Input:
- Output:			- Index
					- Date Time
- Return:			- void
- Description:
- Usage				- This function will save information about the processed data to file
						+ Step
						+ Date time
******************************************************************************/
int SaveDatabaseConfiguration(const char *fileName)
{
	if(fileName == NULL)
	{
		TraceLog(ERROR_LEVEL, "The file name %s is null\n", fileName);
		return ERROR;
	}

	// Open file to write
	FILE *dbConf = fopen(fileName, "w");

	// Check if openning is successful
	if(dbConf == NULL)
	{
		TraceLog(ERROR_LEVEL, "We can not open the file %s to write\n", fileName);
		return ERROR;
	}
	if(strcmp(fileName, PathOfEaaDatabase) == 0)
	{
		fprintf(dbConf, "#db name\n");
		fprintf(dbConf, "%s\n", EaaDatabaseConf.dbname);

		fprintf(dbConf, "#database prefix\n");
		fprintf(dbConf, "%s\n", EaaDatabaseConf.dbprefix);

		fprintf(dbConf, "#username\n");
		fprintf(dbConf, "%s\n", EaaDatabaseConf.username);

		fprintf(dbConf, "#passwd\n");
		fprintf(dbConf, "%s\n", EaaDatabaseConf.passwd);

		fprintf(dbConf, "#hostname\n");
		fprintf(dbConf, "%s\n", EaaDatabaseConf.hostname);

		fprintf(dbConf, "#Postgres Path\n");
		fprintf(dbConf, "%s\n", EaaDatabaseConf.postgresPath);
	}
	else if(strcmp(fileName, PathOfTwt2Database) == 0)
	{
		fprintf(dbConf, "#db name\n");
		fprintf(dbConf, "%s\n", Twt2DatabaseConf.dbname);

		fprintf(dbConf, "#database prefix\n");
		fprintf(dbConf, "%s\n", Twt2DatabaseConf.dbprefix);

		fprintf(dbConf, "#username\n");
		fprintf(dbConf, "%s\n", Twt2DatabaseConf.username);

		fprintf(dbConf, "#passwd\n");
		fprintf(dbConf, "%s\n", Twt2DatabaseConf.passwd);

		fprintf(dbConf, "#hostname\n");
		fprintf(dbConf, "%s\n", Twt2DatabaseConf.hostname);

		fprintf(dbConf, "#Postgres Path\n");
		fprintf(dbConf, "%s\n", Twt2DatabaseConf.postgresPath);
	}

	// Flush
	fflush(dbConf);

	// Close file
	fclose(dbConf);

	return SUCCESS;
}

/*****************************************************************************
- Function name:	CovertStrDateToNDate
- Input:			- strDate: Date in string form
- Return:			- int: Date in int form
- Description:
- Usage				- This function will convert the date in string form to numerical form
******************************************************************************/
int CovertStrDateToNDate(const char *strDate)
{
	struct tm dateTime;
	//TraceLog("strDate = %s\n", strDate);

	// Get date time as struct tm
	strptime(strDate, "%Y/%m/%d", &dateTime);

	// Convert into numerical date
	int nDate = dateTime.tm_year * 10000 + dateTime.tm_mon * 100 + dateTime.tm_mday;

	return nDate;
}
/*****************************************************************************
- Function name:	GetNumberOfSecondsFrom1970
- Input:			- strDate: Date in string form
- Return:			- int: Date in int form
- Description:
- Usage				- This function will get the total number of second from 01/01/1970 to current date
*******************************************************************************/
int GetNumberOfSecondsFrom1970(const char *strDate)
{
	struct tm dateTime;
	memset(&dateTime, 0, sizeof(dateTime));
	
	strptime(strDate, "%Y/%m/%d", &dateTime);

	int numSeconds = mktime(&dateTime);
	return numSeconds;
}

/****************************************************************************
- Function name:	RemoveCharacters
- Input:			+ buffer
				+ bufferLen
- Output:		+ buffer
- Return:		Buffer with removed used characters
- Description:	+ The routine hides the following facts
				  - Check byte by byte with ASCII code 32 (SPACE) and remove
				  any characters less than or equal
				+ There are preconditions guaranteed to the routine
				  - buffer must be allocated and initiazed before
				  - buffeLen must be indicated exactly length of 
				  buffer
				+ The routine guarantees that the status value will 
					have value of
				  - buffer
- Usage:			N/A
****************************************************************************/
char *RemoveCharacters(char *buffer, int bufferLen)
{
	int i;
	int len = strlen(buffer);
	
	for (i = len -1 ; i > -1; i--)
	{
		if (buffer[i] <= USED_CHAR_CODE)
		{	
			buffer[i] = 0;
		}
		else
		{
			break;
		}
	}
	
	return buffer;
}

/****************************************************************************
- Function name:	InitDataStruture
- Input:			N/A
- Output:			N/A
- Return:
- Description:
- Usage:			Init some data structures
****************************************************************************/
int InitDataStructure()
{
	// Init timestamp
	InitTimeStampCollection();

	// Init symol list
	InitializeSymbolIndexList();

	// Init Entry/Exit
	int tsIndex, ecnIndex, statusIndex, stockTypesIndex, tifIndex, orderTypesIndex, orderstIndex, sideIndex, shareTypesIndex;
	int askBidIndex, ecnLaunchIndex, entryExitIndex, isoOrderIndex;
	
	for(tsIndex = 0; tsIndex < NUM_OF_TS; tsIndex++)
	{
		for(ecnIndex = 0; ecnIndex < NUM_OF_ECN; ecnIndex++)
		{
			for (sideIndex = 0; sideIndex < NUM_OF_SIDE; sideIndex++)
			{
				for(statusIndex = 0; statusIndex < NUM_OF_ENTRY_EXIT_STATUS; statusIndex++)
				{
					EntryExit[tsIndex][ecnIndex][sideIndex][statusIndex] = 0;
				}
			}
		}
	}
	for(tsIndex = 0; tsIndex < NUM_OF_TS; tsIndex++)
	{
		for(ecnIndex = 0; ecnIndex < NUM_OF_ECN +1; ecnIndex++)
		{
      for(entryExitIndex = 0; entryExitIndex < ENTRY_EXIT; entryExitIndex++)
      {
        for(ecnLaunchIndex = 0; ecnLaunchIndex < NUM_OF_ECN_LAUNCH; ecnLaunchIndex++)
        {
          for(isoOrderIndex = 0; isoOrderIndex < NUM_OF_ISO_ORDER; isoOrderIndex++)
          {
            for(shareTypesIndex = 0; shareTypesIndex < NUM_OF_SHARE_TYPE; shareTypesIndex++)
            {
              ExitOrderShares[tsIndex][ecnIndex][entryExitIndex][ecnLaunchIndex][isoOrderIndex][shareTypesIndex] = 0;
            }
          }
        }
      }
		}
	}
	

	// Init Stock Type
	for(tsIndex = 0; tsIndex < NUM_OF_TS; tsIndex++)
	{
		for(ecnIndex = 0; ecnIndex < NUM_OF_ECN; ecnIndex++)
		{
			for(stockTypesIndex = 0; stockTypesIndex< NUM_OF_STOCK_TYPES; stockTypesIndex++)
			{
				for(tifIndex = 0; tifIndex < NUM_OF_TIF; tifIndex++)
				{
					for(statusIndex = 0; statusIndex < NUM_OF_STOCK_TYPES_STATUS; statusIndex++)
					{
						StockTypes[tsIndex][ecnIndex][stockTypesIndex][tifIndex][statusIndex] = 0;
					}
				}
			}
		}
	}

	// Order Types
	int isEntry_Exit;
	for(tsIndex = 0; tsIndex < NUM_OF_TS; tsIndex++)
	{
		for(ecnIndex = 0; ecnIndex < NUM_OF_ECN; ecnIndex++)
		{
			for (isEntry_Exit = 0; isEntry_Exit < ENTRY_EXIT; isEntry_Exit++)
			{
				for(orderTypesIndex = 0; orderTypesIndex < NUM_OF_ORDER_TYPE; orderTypesIndex++)
				{
					for(orderstIndex = 0; orderstIndex < NUM_OF_ORDER_STATUS; orderstIndex++)
					{
						OrderTypes[tsIndex][ecnIndex][isEntry_Exit][orderTypesIndex][orderstIndex] = 0;
					}
				}
			}
		}
	}

	// Ask bid launch
	// Init Stock Type
	for(tsIndex = 0; tsIndex < NUM_OF_TS; tsIndex++)
	{
		for(ecnIndex = 0; ecnIndex < NUM_OF_ECN; ecnIndex++)
		{
			for(ecnLaunchIndex = 0; ecnLaunchIndex < NUM_OF_ECN_LAUNCH; ecnLaunchIndex++)
			{
				for(askBidIndex = 0; askBidIndex < ASK_BID; askBidIndex++)
				{
					for(statusIndex = 0; statusIndex < NUM_OF_ASK_BID_STATUS; statusIndex++)
					{
						AskBidLaunch[tsIndex][ecnIndex][ecnLaunchIndex][askBidIndex][statusIndex] = 0;
						Exit_AskBidLaunch[tsIndex][ecnIndex][ecnLaunchIndex][askBidIndex][statusIndex] = 0;
					}
				}
			}
		}
	}

	// Init profit/loss
	prevProfitLoss.totalVolume = 0;
	prevProfitLoss.profitLoss = 0.0;
	prevProfitLoss.totalMarket = 0.0;
	prevProfitLoss.netProfitLoss = 0.0;
	prevProfitLoss.expectedProfit = 0.0;

	memset(&SymbolTradinInfoList, 0, sizeof(SymbolTradinInfoList));

	return SUCCESS;
}

/****************************************************************************
- Function name:	LoadDataConfiguration
- Input:			N/A
- Output:			N/A
- Return:
- Description:
- Usage:			Load data configuration included
					+ Load the current position: date, step
					+ Update some global varibales
****************************************************************************/
int LoadDataConfiguration()
{
	// Load configuration and update global variables
	if (LoadProcessedPosition(PathOfCurrentStatus, &LastProcessedIndex, LastProcessedDate) == ERROR)
	{
		return ERROR;
	}

	// Update Old Date Number
	OldDateNumber = CovertStrDateToNDate(LastProcessedDate);

	return SUCCESS;
}
/****************************************************************************
- Function name:	ConvertToDateFormat
- Input:			N/A
- Output:			N/A
- Return:
- Description:
- Usage:
****************************************************************************/
int ConvertToDateFormat(char dateFormat[11], const char *strDate)
{
	// Year-month-day
	char year[5];
	char month[3];
	char day[3];

	// Reset buffer
	memset(year, 0, 5);
	memset(month, 0, 3);
	memset(day, 0, 3);

	// Convert
	// Year
	strncpy(year, strDate, 4);

	// Month
	month[0] = strDate[5];
	month[1] = strDate[6];

	// Day
	day[0] = strDate[8];
	day[1] = strDate[9];

	// Update
	strncpy(dateFormat, year, 4);
	dateFormat[4] = '-';
	dateFormat[5] = month[0];
	dateFormat[6] = month[1];
	dateFormat[7] = '-';
	dateFormat[8] = day[0];
	dateFormat[9] = day[1];

	return SUCCESS;
}

/****************************************************************************
- Function name:	ConvertToTimeFormat
- Input:			N/A
- Output:			N/A
- Return:
- Description:
- Usage:
****************************************************************************/
int ConvertToTimeFormat(int totalSeconds, char timeFormat[9], int isOldData)
{
	struct tm *tv;
	time_t t = totalSeconds;

	// Get local time
	tv = (struct tm *)localtime(&t);

	//check daylight saving time
	if(tv->tm_isdst == 1 && isOldData == 1)
	{
		tv->tm_hour -= 1;
	}
	// Update time format string
	sprintf(timeFormat, "%02d:%02d:%02d", tv->tm_hour, tv->tm_min, tv->tm_sec);

	return SUCCESS;
}

/****************************************************************************
- Function name:	LoadFeeConf
- Input:			N/A
- Output:			N/A
- Return:
- Description:
- Usage:
****************************************************************************/
int LoadFeeConf(char *fileName, t_FeeConf *feeConf)
{
	if(fileName == NULL)
	{
		TraceLog(ERROR_LEVEL, "This file name %s is null\n", fileName);
		return ERROR;
	}
	
	TraceLog(DEBUG_LEVEL, "Loading %s\n", fileName);

	// Open this file to read data
	FILE *fp = fopen(fileName, "r");

	//Check if openning file is successful
	if(fp == NULL)
	{
		TraceLog(ERROR_LEVEL, "We can not open fee config %s\n", fileName);
		return ERROR;
	}
	else
	{
		//Buffer to store each line in the configuration file
		char line[MAX_LINE_LENGTH];

		//Number of lines read from the configuration file
		int  count = 0;

		//Clear the buffer pointed to by line
		memset(line, 0, MAX_LINE_LENGTH);

		//Get each line from the configuration file
		while (fgets(line, MAX_LINE_LENGTH, fp) != NULL)
		{
			if ((line[0] != '#') && (line[0] > 32))
			{
				count ++;

				if (count == 1)
				{
					FeeConf.secFee = atof(line);
				}
				else if (count == 2)
				{
					FeeConf.tafFee = atof(line);
				}
				else if (count == 3)
				{
					FeeConf.commission = atof(line);
				}
				else if (count == 4)
				{
					FeeConf.addVenueFeeWithPriceLager1 = atof(line);
				}
				else if (count == 5)
				{
					FeeConf.addVenueFeeWithPriceSmaler1 = atof(line);
				}
				else if (count == 6)
				{
					FeeConf.removeVenueFeeWithPriceLager1 = atof(line);
				}
				else if (count == 7)
				{
					FeeConf.removeVenueFeeWithPriceSmaler1 = atof(line);
				}
				else if (count == 8)
				{
					FeeConf.auctionVenueFee = atof(line);
				}
				else if (count == 9)
				{
					FeeConf.arcaRouteVenueFee = atof(line);
				}
				else if (count == 10)
				{
					FeeConf.nasdaqRouteVenueFee = atof(line);
				}
				else if (count == 11)
				{
					FeeConf.batsRouteVenueFee = atof(line);
				}
				else if (count == 12)
				{
					FeeConf.edgxeRouteVenueFee = atof(line);
				}
			}

			//Clear the buffer pointed to by line
			memset(line, 0, MAX_LINE_LENGTH);
		}

		if (count >= 12)
		{
			//Indicate a successful operation
			TraceLog(DEBUG_LEVEL, "Sec Fee = %.10lf\n", FeeConf.secFee);
			TraceLog(DEBUG_LEVEL, "Taf Fee = %lf\n", FeeConf.tafFee);
			TraceLog(DEBUG_LEVEL, "Wedbush commission = %lf\n", FeeConf.commission);
			TraceLog(DEBUG_LEVEL, "Add venue fee with price lager than $1 = %lf\n", FeeConf.addVenueFeeWithPriceLager1);
			TraceLog(DEBUG_LEVEL, "Add venue fee with price smaller than $1 = %lf\n", FeeConf.addVenueFeeWithPriceSmaler1);
			TraceLog(DEBUG_LEVEL, "Remove venue fee with price lager than $1 = %lf\n", FeeConf.removeVenueFeeWithPriceLager1);
			TraceLog(DEBUG_LEVEL, "Remove venue fee with price smaller than $1= %lf\n", FeeConf.removeVenueFeeWithPriceSmaler1);
			TraceLog(DEBUG_LEVEL, "Auction venue fee  = %lf\n", FeeConf.auctionVenueFee);
			TraceLog(DEBUG_LEVEL, "ARCA route venue fee = %lf\n", FeeConf.arcaRouteVenueFee);
			TraceLog(DEBUG_LEVEL, "NASDAQ route venue fee = %lf\n", FeeConf.nasdaqRouteVenueFee);
			TraceLog(DEBUG_LEVEL, "BATZ route venue fee = %lf\n", FeeConf.batsRouteVenueFee);
			TraceLog(DEBUG_LEVEL, "EDGX route venue fee = %lf\n", FeeConf.edgxeRouteVenueFee);
			
			//Close the file
			fclose(fp);
			return SUCCESS;
		}
		else
		{
			//Configuration file is invalid!
			TraceLog(ERROR_LEVEL, "Invalid configuration file '%s'\n", fp);
			
			//Close the file
			fclose(fp);
			return ERROR;
		}
	}
}
