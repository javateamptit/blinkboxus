#include "soupbintcp.h"
#include "network.h"

char* SOUPBINTCP_GetContent_UnsequencedPacket (t_DataBlock* dataBlock, int* len)
{
  int msgLen = ushortAt((unsigned char*)dataBlock->msgContent, 0) + SOUPBINTCP_HEARDER_PACKET_LEN;  
  if(msgLen != dataBlock->msgLen)
  {
    TraceLog(ERROR_LEVEL, "SOUPBINTCP_GetContent_UnsequencedPacket: length of message incorrect\n");
    return NULL;
  }
  
  char msgType = dataBlock->msgContent[SOUPBINTCP_HEARDER_PACKET_LEN];
  if(msgType != SOUPBINTCP_UNSEQUENCED_DATA_PACKET_TYPE)
  {
    TraceLog(ERROR_LEVEL, "SOUPBINTCP_GetContent_UnsequencedPacket: type of message incorrect\n");
    return NULL;
  }
  
  *len = msgLen - SOUPBINTCP_PAYLOAD_OFFSET;
  
  return &dataBlock->msgContent[SOUPBINTCP_PAYLOAD_OFFSET];
}

int SOUPBINTCP_BuildAndSend_ServerHeartbeatPacket (t_Connection* connection)
{
  char message[SOUPBINTCP_HEARTBEAT_PACKET_LEN];
  t_ShortConverter shortUnion;
  
  // Length
  shortUnion.value = SOUPBINTCP_HEARTBEAT_PACKET_LEN - SOUPBINTCP_HEARDER_PACKET_LEN;
  message[0] = shortUnion.c[1];
  message[1] = shortUnion.c[0];
  // Message type is 'H'
  message[2] = SOUPBINTCP_SERVER_HEARTBEAT_PACKET_TYPE;
  
  return SendMsg(connection->socket, &connection->lock, message, SOUPBINTCP_HEARTBEAT_PACKET_LEN);
}

int SOUPBINTCP_BuildAndSend_LoginAcceptedPacket (t_Connection* connection, const char* sessionId, const long seqNum)
{
  char message[SOUPBINTCP_LOGIN_ACCEPTED_PACKET_LEN];
  t_ShortConverter shortUnion;
  
  // Length
  shortUnion.value = SOUPBINTCP_LOGIN_ACCEPTED_PACKET_LEN - SOUPBINTCP_HEARDER_PACKET_LEN;
  message[0] = shortUnion.c[1];
  message[1] = shortUnion.c[0];
  // Message type is 'A'
  message[2] = SOUPBINTCP_LOGIN_ACCEPTED_PACKET_TYPE;
  // The session ID
  __StrWithSpaceLeftPad(sessionId, 10, &message[3]);
  // The sequence number
  __lToStrWithLeftPad(seqNum, ' ', 20, &message[13]);
  
  return SendMsg(connection->socket, &connection->lock, message, SOUPBINTCP_LOGIN_ACCEPTED_PACKET_LEN);
}

int SOUPBINTCP_BuildAndSend_LoginRejectedPacket (t_Connection* connection, const char reason)
{
  char message[SOUPBINTCP_LOGIN_REJECTED_PACKET_LEN];
  t_ShortConverter shortUnion;
  
  // Length
  shortUnion.value = SOUPBINTCP_LOGIN_REJECTED_PACKET_LEN - SOUPBINTCP_HEARDER_PACKET_LEN;
  message[0] = shortUnion.c[1];
  message[1] = shortUnion.c[0];
  // Message type is 'J'
  message[2] = SOUPBINTCP_LOGIN_REJECTED_PACKET_TYPE;
  // Reject Reason Code
  message[3] = reason;
  
  return SendMsg(connection->socket, &connection->lock, message, SOUPBINTCP_LOGIN_REJECTED_PACKET_LEN);
}

int SOUPBINTCP_BuildAndSend_EndSessionPacket (t_Connection* connection)
{
  char message[SOUPBINTCP_END_SESSION_PACKET_LEN];
  t_ShortConverter shortUnion;
  
  // Length
  shortUnion.value = SOUPBINTCP_END_SESSION_PACKET_LEN - SOUPBINTCP_HEARDER_PACKET_LEN;
  message[0] = shortUnion.c[1];
  message[1] = shortUnion.c[0];
  // Message type is 'Z'
  message[2] = SOUPBINTCP_END_SESSION_PACKET_TYPE;
  
  return SendMsg(connection->socket, &connection->lock, message, SOUPBINTCP_END_SESSION_PACKET_LEN);
}

int SOUPBINTCP_BuildAndSend_SequencedDataPacket (t_Connection* connection, const char* msg, const int msgLen)
{
  char message[msgLen + SOUPBINTCP_PAYLOAD_OFFSET];
  t_ShortConverter shortUnion;
  
  // Length
  shortUnion.value = msgLen + 1;
  message[0] = shortUnion.c[1];
  message[1] = shortUnion.c[0];
  // Message type is 'S'
  message[2] = SOUPBINTCP_SEQUENCED_DATA_PACKET_TYPE;
  // message content
  memcpy(&message[SOUPBINTCP_PAYLOAD_OFFSET], msg, msgLen);
  
  return SendMsg(connection->socket, &connection->lock, message, msgLen + SOUPBINTCP_PAYLOAD_OFFSET);
}

int SOUPBINTCP_BuildAndSend_ClientHeartbeatPacket (t_Connection* connection)
{
  char message[SOUPBINTCP_HEARTBEAT_PACKET_LEN];
  t_ShortConverter shortUnion;
  
  // Length
  shortUnion.value = SOUPBINTCP_HEARTBEAT_PACKET_LEN - SOUPBINTCP_HEARDER_PACKET_LEN;
  message[0] = shortUnion.c[1];
  message[1] = shortUnion.c[0];
  // Message type is 'R'
  message[2] = SOUPBINTCP_CLIENT_HEARTBEAT_PACKET_TYPE;
  
  return SendMsg(connection->socket, &connection->lock, message, SOUPBINTCP_HEARTBEAT_PACKET_LEN);
}

int SOUPBINTCP_BuildAndSend_LoginRequestPacket (t_Connection* connection, const char* userName, const char* password,
    const char* sessionId, const long seqNum)
{
  char message[SOUPBINTCP_LOGIN_REQUEST_PACKET_LEN];
  t_ShortConverter shortUnion;
  
  // Length
  shortUnion.value = SOUPBINTCP_LOGIN_REQUEST_PACKET_LEN - SOUPBINTCP_HEARDER_PACKET_LEN;
  message[0] = shortUnion.c[1];
  message[1] = shortUnion.c[0];
  // Message type is 'L'
  message[2] = SOUPBINTCP_LOGIN_REQUEST_PACKET_TYPE;
  // Username
  __StrWithSpaceRightPad(userName, 6, &message[3]);
  // Password
  __StrWithSpaceRightPad(password, 10, &message[9]);
  // Requested Session
  __StrWithSpaceLeftPad(sessionId, 10, &message[19]);
  // Sequence
  __lToStrWithLeftPad(seqNum, ' ', 20, &message[29]);
    
  return SendMsg(connection->socket, &connection->lock, message, SOUPBINTCP_LOGIN_REQUEST_PACKET_LEN);
}

int SOUPBINTCP_BuildAndSend_UnsequencedDataPacket (t_Connection* connection, const char* msg, const int msgLen)
{
  char message[msgLen + SOUPBINTCP_PAYLOAD_OFFSET];
  t_ShortConverter shortUnion;
  
  // Length
  shortUnion.value = msgLen + 1;
  message[0] = shortUnion.c[1];
  message[1] = shortUnion.c[0];
  // Message type is 'U'
  message[2] = SOUPBINTCP_UNSEQUENCED_DATA_PACKET_TYPE;
  // message content
  memcpy(&message[SOUPBINTCP_PAYLOAD_OFFSET], msg, msgLen);
  
  return SendMsg(connection->socket, &connection->lock, message, msgLen + SOUPBINTCP_PAYLOAD_OFFSET);
}

int SOUPBINTCP_BuildAndSend_LogoutRequestPacket (t_Connection* connection)
{
  char message[SOUPBINTCP_LOGOUT_REQUEST_PACKET_LEN];
  t_ShortConverter shortUnion;
  
  // Length
  shortUnion.value = SOUPBINTCP_LOGOUT_REQUEST_PACKET_LEN - SOUPBINTCP_HEARDER_PACKET_LEN;
  message[0] = shortUnion.c[1];
  message[1] = shortUnion.c[0];
  // Message type is 'O'
  message[2] = SOUPBINTCP_LOGOUT_REQUEST_PACKET_TYPE;
  
  return SendMsg(connection->socket, &connection->lock, message, SOUPBINTCP_LOGOUT_REQUEST_PACKET_LEN);
}

int SOUPBINTCP_Handle_LogonPacket(t_Connection* connection, t_DataBlock *dataBlock)
{
  Simulator_Initialize_User(connection);
  
  char sessionId[11] = "\0", seqNum[21] = "\0";
  
  memcpy(connection->userInfo->userName, &dataBlock->msgContent[3], 6);
  
  memcpy(connection->userInfo->password, &dataBlock->msgContent[9], 10);
  
  // TODO: check userName and password
  
  memcpy(sessionId, &dataBlock->msgContent[19], 10);
  
  memcpy(seqNum, &dataBlock->msgContent[29], 20);
  connection->userInfo->incomingSeq = atol(seqNum);
  
  // TODO: check session id and sequence number
  // if (connection->userInfo->incomingSeq != 0)
    // return BuildAndSendLoginRejected (connection, SOUPBINTCP_LOGIN_REJECTED_NOT_SESSION);
    
  return SOUPBINTCP_BuildAndSend_LoginAcceptedPacket(connection, sessionId, __sync_add_and_fetch(&connection->userInfo->outgoingSeq, 1));
}

int SOUPBINTCP_Handle_LogoutPacket(t_Connection* connection, t_DataBlock *dataBlock)
{
  connection->isAlive = 0;
  
  return SUCCESS;
}

void* SOUPBINTCP_HeartbeatThread(void* arg)
{
  t_Connection *connection = (t_Connection *)arg;
  
  while(connection->isAlive)
  {
    SOUPBINTCP_BuildAndSend_ServerHeartbeatPacket(connection);
    sleep(1); // The server heartbeat interval is 1 second
  }
  
  TraceLog(DEBUG_LEVEL, "Close HeartbeatThread\n");
  return NULL;
}

int SOUPBINTCP_StringFormatPrintable(char* outStr, const unsigned char* buffer, const int buffLen)
{
  // if (sizeof(buffer) < buffLen)
    // return -1;
  
  int iIdx, oIdx;
  
  for (iIdx = 0, oIdx = 0; iIdx < buffLen; iIdx++)
  {
    if (buffer[iIdx] == 0x01)
      oIdx += sprintf(&outStr[oIdx], "<SOH>");
    else if (buffer[iIdx] < 32 || buffer[iIdx] > 126)
      oIdx += sprintf(&outStr[oIdx], "%02X.", buffer[iIdx]);
    else 
      outStr[oIdx++] = buffer[iIdx];
  }
  
  outStr[oIdx] = 0;
  return oIdx;
}

