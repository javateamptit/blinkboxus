#include "chx.h"
#include "fix_protocol.h"
#include "network.h"


int CHX_GetNewOrderEntry(t_OrderEntry* orderEntry, const char* msg, const int msgLen)
{
  char strValue[64];
  double price;
  
  // Client Order Id
  FIX_GetTagValue (msg, 11, orderEntry->clOrdID);
  // Handle Instructions => NOT USE
  FIX_GetTagValue (msg, 21, strValue);
  // Symbol
  FIX_GetTagValue (msg, 55, orderEntry->symbol);
  // Side
  FIX_GetTagValue (msg, 54, strValue);
  orderEntry->side = strValue[0];
  // Order quantity
  FIX_GetTagValue (msg, 38, strValue);
  orderEntry->quantity = atoi(strValue);
  // Order Type
  FIX_GetTagValue (msg, 40, strValue);
  orderEntry->orderType = strValue[0];
  // Price
  FIX_GetTagValue (msg, 44, strValue);
  price = atof(strValue);
  orderEntry->price = (int) (price * 10000.0);
  // Order Capacity
  FIX_GetTagValue (msg, 47, strValue);
  orderEntry->capacity = strValue[0];
  // Time in force
  FIX_GetTagValue (msg, 59, strValue);
  orderEntry->tif = (int) strValue[0];
  // Transact Time => NOT USE
  FIX_GetTagValue (msg, 60, strValue);
  // Client Id
  FIX_GetTagValue (msg, 109, orderEntry->clientId);
  
  return SUCCESS;
}

int CHX_GetCancelOrderEntry(t_OrderEntry* orderEntry, const char* msg, const int msgLen)
{
  char strValue[64];
  
  FIX_GetTagValue (msg, 41, orderEntry->clOrdID); //OrigClOrdId
  
  FIX_GetTagValue (msg, 11, strValue);
  
  FIX_GetTagValue (msg, 55, orderEntry->symbol);
  
  FIX_GetTagValue (msg, 54, strValue);
  orderEntry->side = strValue[0];
  
  FIX_GetTagValue (msg, 38, strValue);
  orderEntry->quantity = atoi(strValue);
  
  FIX_GetTagValue (msg, 60, strValue); //TransactTime
  
  return SUCCESS;
}

static int CHX_BuildAndSend_HeartBeat(t_Connection* connection)
{
  char msg[CHX_RESERVE_ENTIRE_MSG_LEN];
  int headerLen, bodyLen;
  char* message = &msg[FIX_RESERVE_HEADER_LEN];
  
  // body
  bodyLen = FIX_BuildCommonTagsField(message, FIX_MSG_TYPE_HEARTBEAT, CHX_COMPID, connection->userInfo->userName, (int*) &connection->userInfo->outgoingSeq);
  // header
  message = FIX_BuildMsgWithHeaderField(msg, CHX_BEGIN_STRING, bodyLen, &headerLen);
  // trailer
  message = FIX_BuildMsgWithTrailerField(message, bodyLen, headerLen);
  
  return SendMsg(connection->socket, &connection->lock, message, headerLen + bodyLen + FIX_MSG_CHECKSUM_LEN);
}

static int CHX_BuildAndSend_LogonResponse(t_Connection* connection)
{
  char msg[CHX_RESERVE_ENTIRE_MSG_LEN];
  int headerLen, bodyLen;
  char* message = &msg[FIX_RESERVE_HEADER_LEN];
  
  // body
  bodyLen = FIX_BuildCommonTagsField(message, FIX_MSG_TYPE_LOGON, CHX_COMPID, connection->userInfo->userName, (int*) &connection->userInfo->outgoingSeq);
  bodyLen += FIX_AddTagValueInt(&message[bodyLen], FIX_TAG_ENCRYPT_METHOD, 0);
  bodyLen += FIX_AddTagValueInt(&message[bodyLen], FIX_TAG_HEARBEAT_INT, connection->heartbeatInterval);
  // header
  message = FIX_BuildMsgWithHeaderField(msg, CHX_BEGIN_STRING, bodyLen, &headerLen);
  // trailer
  message = FIX_BuildMsgWithTrailerField(message, bodyLen, headerLen);
  
  return SendMsg(connection->socket, &connection->lock, message, headerLen + bodyLen + FIX_MSG_CHECKSUM_LEN);
}

static int CHX_BuildAndSend_LogoutResponse(t_Connection* connection)
{
  char msg[CHX_RESERVE_ENTIRE_MSG_LEN];
  int headerLen, bodyLen;
  char* message = &msg[FIX_RESERVE_HEADER_LEN];
  
  // body
  bodyLen = FIX_BuildCommonTagsField(message, FIX_MSG_TYPE_LOGOUT, CHX_COMPID, connection->userInfo->userName, (int*) &connection->userInfo->outgoingSeq);
  // header
  message = FIX_BuildMsgWithHeaderField(msg, CHX_BEGIN_STRING, bodyLen, &headerLen);
  // trailer
  message = FIX_BuildMsgWithTrailerField(message, bodyLen, headerLen);
  
  return SendMsg(connection->socket, &connection->lock, message, headerLen + bodyLen + FIX_MSG_CHECKSUM_LEN);
}

static int CHX_BuildAndSend_ResendRequest(t_Connection* connection, const int beginSeq, const int endSeq)
{
  char msg[CHX_RESERVE_ENTIRE_MSG_LEN];
  int headerLen, bodyLen;
  char* message = &msg[FIX_RESERVE_HEADER_LEN];
  
  // body
  bodyLen = FIX_BuildCommonTagsField(message, FIX_MSG_TYPE_RESEND_REQUEST, CHX_COMPID, connection->userInfo->userName, (int*) &connection->userInfo->outgoingSeq);
  bodyLen += FIX_AddTagValueInt(&message[bodyLen], FIX_TAG_BEGIN_SEQNO, beginSeq);
  bodyLen += FIX_AddTagValueInt(&message[bodyLen], FIX_TAG_END_SEQNO, endSeq);
  // header
  message = FIX_BuildMsgWithHeaderField(msg, CHX_BEGIN_STRING, bodyLen, &headerLen);
  //trailer
  message = FIX_BuildMsgWithTrailerField(message, bodyLen, headerLen);
  
  return SendMsg(connection->socket, &connection->lock, message, headerLen + bodyLen + FIX_MSG_CHECKSUM_LEN);  
}

static int CHX_BuildAndSend_SequenceReset(t_Connection* connection, const int newSeq)
{
  char msg[CHX_RESERVE_ENTIRE_MSG_LEN];
  int headerLen, bodyLen;
  char* message = &msg[FIX_RESERVE_HEADER_LEN];
  
  // body
  bodyLen = FIX_BuildCommonTagsField(message, FIX_MSG_TYPE_SEQ_RESET, CHX_COMPID, connection->userInfo->userName, (int*) &connection->userInfo->outgoingSeq);
  bodyLen += FIX_AddTagValueInt(&message[bodyLen], FIX_TAG_NEW_SEQNO, newSeq);
  // header
  message = FIX_BuildMsgWithHeaderField(msg, CHX_BEGIN_STRING, bodyLen, &headerLen);
  // trailer
  message = FIX_BuildMsgWithTrailerField(message, bodyLen, headerLen);
  
  return SendMsg(connection->socket, &connection->lock, message, headerLen + bodyLen + FIX_MSG_CHECKSUM_LEN);  
}

// static int CHX_BuildAndSend_Reject(t_Connection* connection, const int errSeq)
// {
  // char msg[CHX_RESERVE_ENTIRE_MSG_LEN];
  // int headerLen, bodyLen;
  // char* message = &msg[FIX_RESERVE_HEADER_LEN];
  
  // // body
  // bodyLen = FIX_BuildCommonTagsField(message, FIX_MSG_TYPE_REJECT, CHX_COMPID, connection->userInfo->userName, (int*) &connection->userInfo->outgoingSeq);
  // bodyLen += FIX_AddTagValueInt(&message[bodyLen], FIX_TAG_REF_SEQNO, errSeq);
  // // header
  // message = FIX_BuildMsgWithHeaderField(msg, CHX_BEGIN_STRING, bodyLen, &headerLen);
  // // trailer
  // message = FIX_BuildMsgWithTrailerField(message, bodyLen, headerLen);
  
  // return SendMsg(connection->socket, &connection->lock, message, headerLen + bodyLen + FIX_MSG_CHECKSUM_LEN);
// }

static int CHX_BuildAndSend_OrderCancelReject(t_Connection* connection,
    const char* clOrdID, const char* origClOrdID, const char ordStatus,
    const char cxlRejResponseTo, const int cxlRejReason, const char* text)
{
  char orderId[16];
  char msg[CHX_RESERVE_ENTIRE_MSG_LEN];
  int headerLen, bodyLen;
  char* message = &msg[FIX_RESERVE_HEADER_LEN];
  
  sprintf(orderId, "%ld", connection->orderID);
  
  // body
  bodyLen = FIX_BuildCommonTagsField(message, FIX_MSG_TYPE_CANCEL_REJECT, CHX_COMPID, connection->userInfo->userName, (int*) &connection->userInfo->outgoingSeq);
  bodyLen += FIX_AddTagValueString(&message[bodyLen], 11, clOrdID);
  bodyLen += FIX_AddTagValueString(&message[bodyLen], 37, orderId);  
  bodyLen += FIX_AddTagValueChar(&message[bodyLen], 39, ordStatus);
  bodyLen += FIX_AddTagValueString(&message[bodyLen], 41, origClOrdID);
  if (cxlRejReason > -1)
    bodyLen += FIX_AddTagValueInt(&message[bodyLen], 102, cxlRejReason);
  if (cxlRejReason < 0 && text != NULL)
    bodyLen += FIX_AddTagValueString(&message[bodyLen], 58, text);
  bodyLen += FIX_AddTagValueChar(&message[bodyLen], 434, cxlRejResponseTo);
  // header
  message = FIX_BuildMsgWithHeaderField(msg, CHX_BEGIN_STRING, bodyLen, &headerLen);
  // trailer
  message = FIX_BuildMsgWithTrailerField(message, bodyLen, headerLen);
  
  return SendMsg(connection->socket, &connection->lock, message, headerLen + bodyLen + FIX_MSG_CHECKSUM_LEN);
}

static int CHX_BuildAndSend_ExecutionReport(t_Connection* connection, t_OrderEntry* orderEntry,
    const char ordStatus, const int cumQty, const int leavesQty, const double avgPx,
    const char execType, const int lastShares, const double lastPx,
    const char execTransType, const char* clOrdID, const int ordRejReason, const char* text)
{
  char orderId[16], execID[16];
  char msg[CHX_RESERVE_ENTIRE_MSG_LEN];
  int headerLen, bodyLen;
  char* message = &msg[FIX_RESERVE_HEADER_LEN];
  const double price = orderEntry->price / 10000.0;
  
  sprintf(orderId, "%ld", orderEntry->orderID);
  if (execTransType != FIX_EXEC_TRANS_TYPE_STATUS)
    sprintf(execID, "%ld", __sync_add_and_fetch(&connection->executionNumber, 1));
  else
    strcpy(execID, "0");  
  
  // body
  bodyLen = FIX_BuildCommonTagsField(message, FIX_MSG_TYPE_EXECUTION_REPORT, CHX_COMPID, connection->userInfo->userName, (int*) &connection->userInfo->outgoingSeq);
  bodyLen += FIX_AddTagValueString(&message[bodyLen], 37, orderId);
  bodyLen += FIX_AddTagValueString(&message[bodyLen], 17, execID);
  bodyLen += FIX_AddTagValueChar(&message[bodyLen], 20, execTransType);
  bodyLen += FIX_AddTagValueChar(&message[bodyLen], 39, ordStatus);
  bodyLen += FIX_AddTagValueString(&message[bodyLen], 55, orderEntry->symbol);
  bodyLen += FIX_AddTagValueChar(&message[bodyLen], 54, orderEntry->side);
  bodyLen += FIX_AddTagValueInt(&message[bodyLen], 38, orderEntry->quantity);
  bodyLen += FIX_AddTagValueInt(&message[bodyLen], 32, lastShares);
  bodyLen += FIX_AddTagValueDouble(&message[bodyLen], 31, lastPx);
  bodyLen += FIX_AddTagValueInt(&message[bodyLen], 14, cumQty);
  bodyLen += FIX_AddTagValueInt(&message[bodyLen], 151, leavesQty);
  bodyLen += FIX_AddTagValueDouble(&message[bodyLen], 6, avgPx);
  bodyLen += FIX_AddTagValueChar(&message[bodyLen], 150, execType);
  if (clOrdID != NULL)
    bodyLen += FIX_AddTagValueString(&message[bodyLen], 11, clOrdID);
  if (orderEntry->clOrdID[0] != 0) // orgClOrdID
    bodyLen += FIX_AddTagValueString(&message[bodyLen], 41, orderEntry->clOrdID);
  if (orderEntry->orderType != 0)
    bodyLen += FIX_AddTagValueChar(&message[bodyLen], 40, orderEntry->orderType);
  if (ordStatus == FIX_ORDER_STATUS_REPLACED && price > 0)
    bodyLen += FIX_AddTagValueDouble(&message[bodyLen], 44, price);
  if (ordStatus == FIX_ORDER_STATUS_REJECTED && ordRejReason > -1)
    bodyLen += FIX_AddTagValueInt(&message[bodyLen], 103, ordRejReason);
  if (text != NULL)
  {
    if (execTransType == FIX_EXEC_TRANS_TYPE_CORRECT)
      bodyLen += FIX_AddTagValueString(&message[bodyLen], 58, "NOT price");
    else if (ordStatus == FIX_ORDER_STATUS_REPLACED)
      bodyLen += FIX_AddTagValueString(&message[bodyLen], 58, "ORDER CHANGE");
    else if (ordStatus == FIX_ORDER_STATUS_STOPPED)
      bodyLen += FIX_AddTagValueString(&message[bodyLen], 58, "UR STPD price");
    else if (ordStatus != FIX_ORDER_STATUS_REJECTED || ordRejReason < 0)
      bodyLen += FIX_AddTagValueString(&message[bodyLen], 58, text);
  }    
  // header
  message = FIX_BuildMsgWithHeaderField(msg, CHX_BEGIN_STRING, bodyLen, &headerLen);
  // trailer
  message = FIX_BuildMsgWithTrailerField(message, bodyLen, headerLen);
  
  return SendMsg(connection->socket, &connection->lock, message, headerLen + bodyLen + FIX_MSG_CHECKSUM_LEN);
}

static int CHX_BuildAndSend_OrderACK(t_Connection* connection, t_OrderEntry* orderEntry)
{
  return CHX_BuildAndSend_ExecutionReport(connection, orderEntry,
          FIX_ORDER_STATUS_ACK, 0, 0, 0.0,
          FIX_EXEC_TYPE_ACK, 0, 0.0,
          FIX_EXEC_TRANS_TYPE_NEW, orderEntry->clOrdID, -1, NULL);
}

static int CHX_BuildAndSend_OrderRejected(t_Connection* connection, t_OrderEntry* orderEntry, 
      const int reason, const char* text)
{
  return CHX_BuildAndSend_ExecutionReport(connection, orderEntry,
          FIX_ORDER_STATUS_REJECTED, 0, 0, 0.0,
          FIX_EXEC_TYPE_REJECTED, 0, 0.0,
          FIX_EXEC_TRANS_TYPE_NEW, orderEntry->clOrdID, reason, text);
}

static int CHX_BuildAndSend_CancelACK(t_Connection* connection, t_OrderEntry* orderEntry)
{
  return CHX_BuildAndSend_ExecutionReport(connection, orderEntry,
          FIX_ORDER_STATUS_PENDING_CANCEL, 0, 0, 0.0,
          FIX_EXEC_TYPE_PENDING_CANCEL, 0, 0.0,
          FIX_EXEC_TRANS_TYPE_NEW, NULL, -1, NULL);
}

static int CHX_BuildAndSend_Cancelled(t_Connection* connection, t_QueueItem* item, const char* clOrdID)
{
  double avgPx = (item->totalPx / 10000.0) / item->cumShare;
  
  return CHX_BuildAndSend_ExecutionReport(connection, &item->orderEntry,
          FIX_ORDER_STATUS_CANCELLED, item->cumShare, 0, avgPx,
          FIX_EXEC_TYPE_CANCELLED, 0, 0.0,
          FIX_EXEC_TRANS_TYPE_NEW, clOrdID, -1, NULL);
}

static int CHX_BuildAndSend_Filled(t_Connection* connection, t_QueueItem* item, const int quantity, const long price)
{
  item->cumShare += quantity;
  item->totalPx += quantity * price;
  
  double curPx = price / 10000.0;
  double avgPx = (item->totalPx / 10000.0) / item->cumShare;
  const int isFilled = item->orderEntry.quantity == item->cumShare;
  
  return CHX_BuildAndSend_ExecutionReport(connection, &item->orderEntry,
      isFilled? FIX_ORDER_STATUS_FILLED : FIX_ORDER_STATUS_PARTIALLY, item->cumShare, item->orderEntry.quantity - item->cumShare, avgPx,
      isFilled? FIX_EXEC_TYPE_FILL : FIX_EXEC_TYPE_PARTIAL, quantity, curPx,
      FIX_EXEC_TRANS_TYPE_NEW, item->orderEntry.clOrdID, -1, NULL);
}

int CHX_CheckNewOrderEntry(t_Connection* connection, t_OrderEntry *orderEntry)
{
  if(orderEntry->tif != '0' && orderEntry->tif != '3')
  {
    TraceLog(DEBUG_LEVEL, "Reject by TIF\n");
    CHX_BuildAndSend_OrderRejected(connection, orderEntry, -1, "Reject by TIF");
    return ERROR;
  }
  else if(orderEntry->side != '1' && orderEntry->side != '2' && orderEntry->side != '5')
  {
    TraceLog(DEBUG_LEVEL, "Reject by Side\n");
    CHX_BuildAndSend_OrderRejected(connection, orderEntry, -1, "Reject by Side");
    return ERROR;
  }
  else if((orderEntry->quantity < 1) || (orderEntry->quantity > 1000000))
  {
    TraceLog(DEBUG_LEVEL, "Reject by Quantity\n");
    CHX_BuildAndSend_OrderRejected(connection, orderEntry, -1, "Reject by Quantity");
    return ERROR;
  }
  
  return SUCCESS;
}

int CHX_Callback_Filled (t_Connection* connection, t_QueueItem* item, const int quantity, const long price, const long matchNum)
{
  return CHX_BuildAndSend_Filled(connection, item, quantity, price);
}

int CHX_Callback_SystemCancel (t_Connection* connection, t_QueueItem* item)
{
  return CHX_BuildAndSend_Cancelled(connection, item, item->orderEntry.clOrdID);
}

int CHX_Callback_UserCancel (t_Connection* connection, t_QueueItem* item, const int quantity)
{
  return CHX_BuildAndSend_Cancelled(connection, item, NULL);
}

int CHX_Callback_CancelReject (t_Connection* connection, t_OrderEntry *orderEntry)
{
  return CHX_BuildAndSend_OrderCancelReject(connection, "", orderEntry->clOrdID, FIX_ORDER_STATUS_CANCELLED, '1', 1, "not found/opened");
}

static int CHX_Handle_MsgLogon(t_Connection* connection, t_DataBlock *dataBlock)
{
  Simulator_Initialize_User(connection);
  
  char strValue[16];
  
  FIX_GetTagValue (dataBlock->msgContent, FIX_TAG_SENDER_COMPID, connection->userInfo->userName);
  
  FIX_GetTagValue (dataBlock->msgContent, FIX_TAG_ENCRYPT_METHOD, strValue);
  if (atoi(strValue) != 0)
  {
    TraceLog(ERROR_LEVEL, "CHX does not support data encryption feature\n");
    return ERROR;
  }
    
  FIX_GetTagValue (dataBlock->msgContent, FIX_TAG_HEARBEAT_INT, strValue);
  connection->heartbeatInterval = atoi(strValue);
  
  return CHX_BuildAndSend_LogonResponse(connection);
}

static int CHX_Handle_MsgLogout(t_Connection* connection, t_DataBlock *dataBlock)
{
  connection->isAlive = 0;
  return CHX_BuildAndSend_LogoutResponse(connection);
}

static int CHX_Handle_MsgResendRequest(t_Connection* connection, t_DataBlock *dataBlock)
{
  char strValue[16];
  int beginSeq/* , endSeq */;
  
  FIX_GetTagValue (dataBlock->msgContent, FIX_TAG_BEGIN_SEQNO, strValue);
  beginSeq = atoi(strValue);
  
  // FIX_GetTagValue (dataBlock->msgContent, FIX_TAG_END_SEQNO, strValue);
  // endSeq = atoi(strValue);
  
  return CHX_BuildAndSend_SequenceReset(connection, beginSeq);
}

static int CHX_Handle_MsgSequenceReset(t_Connection* connection, t_DataBlock *dataBlock)
{
  char strValue[16];
  int newSeq;
  
  FIX_GetTagValue (dataBlock->msgContent, FIX_TAG_NEW_SEQNO, strValue);
  newSeq = atoi(strValue);
  
  connection->userInfo->incomingSeq = newSeq - 1;
  
  return SUCCESS;
}

static int CHX_Handle_MsgNewOrder(t_Connection* connection, t_DataBlock *dataBlock)
{
  t_OrderEntry orderEntry;
  memset(&orderEntry, 0, sizeof(t_OrderEntry));
  
  CHX_GetNewOrderEntry(&orderEntry, dataBlock->msgContent, dataBlock->msgLen);
  
  if(CHX_CheckNewOrderEntry(connection, &orderEntry) == SUCCESS)
  {
    // ACK
    CHX_BuildAndSend_OrderACK(connection, &orderEntry);
    
    // Token
    t_QueueItem item;
    memcpy(&item.orderEntry, &orderEntry, sizeof(t_OrderEntry));
    item.priceScale = '4';
    item.cumShare = 0;
    item.totalPx = 0;
    
    // Detect cross
    int hasCross = Simulator_Handle_CrossOrder(connection, &item, FIX_SIDE_BUY, &CHX_Callback_Filled);
    
    if (orderEntry.tif == FIX_TIMEINFORCE_IOC)
      if (hasCross)
      {
        if (item.orderEntry.quantity > item.cumShare)
          CHX_BuildAndSend_Cancelled(connection, &item, orderEntry.clOrdID);
      } else //IOC Random
        Simulator_Handle_RandomIOC(connection, &item, &CHX_Callback_Filled, &CHX_Callback_SystemCancel);
    else // DAY
      Simulator_Add_DAYOrder(&connection->orderQueue, &item);
  }
  
  return SUCCESS;
}

static int CHX_Handle_MsgCancelOrder(t_Connection* connection, t_DataBlock *dataBlock)
{
  t_OrderEntry orderEntry;
  memset(&orderEntry, 0, sizeof(t_OrderEntry));
  
  CHX_GetCancelOrderEntry(&orderEntry, dataBlock->msgContent, dataBlock->msgLen);
 
  // Cancel ACK
  CHX_BuildAndSend_CancelACK(connection, &orderEntry);
  
  return Simulator_Handle_CancelOrder(connection, &orderEntry, &CHX_Callback_UserCancel, &CHX_Callback_CancelReject);
}

static void* CHX_DAYThread(void* arg)
{
  t_Connection *connection = (t_Connection *)arg;
  
  while(connection->isAlive)
  {    
    Simulator_Handle_RandomDAY(connection, &CHX_Callback_Filled, &CHX_Callback_SystemCancel);
    
    sleep(15);
  }
  
  return NULL;
}

static void* CHX_HeartbeatThread(void* arg)
{
  t_Connection *connection = (t_Connection *)arg;
  
  while(connection->isAlive)
  {
    CHX_BuildAndSend_HeartBeat(connection);
    sleep(connection->heartbeatInterval);
  }
  
  TraceLog(DEBUG_LEVEL, "Close CHX_HeartbeatThread\n");
  return NULL;
} 

int HandleMsg_CHX(t_Connection* connection, t_DataBlock *dataBlock)
{
  char strValue[16];
  char msgType;
  
  FIX_GetTagValue (dataBlock->msgContent, FIX_TAG_BEGIN_STRING, strValue);
  if(strcmp(strValue, CHX_BEGIN_STRING) != 0)
  {
    TraceLog(ERROR_LEVEL, "Not CHX FIX 4.2 protocol (%s)\n", strValue);
    return ERROR;
  }
  
  FIX_GetTagValue (dataBlock->msgContent, FIX_TAG_TARGET_COMPID, strValue);
  if(strcmp(strValue, CHX_COMPID) != 0)
  {
    TraceLog(ERROR_LEVEL, "Not CHX target (%s)\n", strValue);
    return ERROR;
  }
  
  FIX_GetTagValue (dataBlock->msgContent, FIX_TAG_MSG_TYPE, strValue);
  msgType = strValue[0];
  
  if (connection->userInfo != NULL && msgType != FIX_MSG_TYPE_SEQ_RESET)
  {
    FIX_GetTagValue (dataBlock->msgContent, FIX_TAG_MSG_SEQNO, strValue);
    int incomingSeq = atoi(strValue);

    if (incomingSeq == connection->userInfo->incomingSeq + 1)
    {
      connection->userInfo->incomingSeq++;
    }
    else if (incomingSeq < connection->userInfo->incomingSeq + 1)
    {
      connection->isAlive = 0;
      return ERROR;
    }
    else
    {
      CHX_BuildAndSend_ResendRequest(connection, connection->userInfo->incomingSeq + 1, 0);
      return SUCCESS;
    }    
  }
  
  switch(msgType)
  {
    case FIX_MSG_TYPE_LOGON:
      TraceLog(DEBUG_LEVEL, "Received login message from connection\n");
      if(CHX_Handle_MsgLogon(connection, dataBlock) == SUCCESS)
      {
        pthread_t tDAY;
        pthread_create(&tDAY, NULL, CHX_DAYThread, (void*)connection);
        
        pthread_t tHeartbeat;
        pthread_create(&tHeartbeat, NULL, CHX_HeartbeatThread, (void*)connection);
      }
      break;
      
    case FIX_MSG_TYPE_NEW_ORDER:
      TraceLog(DEBUG_LEVEL, "Received new order message from connection\n");
      CHX_Handle_MsgNewOrder(connection, dataBlock);
      break;
      
    case FIX_MSG_TYPE_CANCEL_ORDER:
      TraceLog(DEBUG_LEVEL, "Received cancel order message from connection\n");
      CHX_Handle_MsgCancelOrder(connection, dataBlock);
      break;
    
    case FIX_MSG_TYPE_LOGOUT:
      TraceLog(DEBUG_LEVEL, "Received logout message from connection\n");
      CHX_Handle_MsgLogout(connection, dataBlock);
      break;
    
    case FIX_MSG_TYPE_RESEND_REQUEST:
      TraceLog(DEBUG_LEVEL, "Received resend request message from connection\n");
      CHX_Handle_MsgResendRequest(connection, dataBlock);
      break;
    
    case FIX_MSG_TYPE_SEQ_RESET:
      TraceLog(DEBUG_LEVEL, "Received sequence reset message from connection\n");
      CHX_Handle_MsgSequenceReset(connection, dataBlock);
      break;
      
    default:
      TraceLog(ERROR_LEVEL, "Unknown MsgType (%c)\n", msgType);
      break;
  }

  return SUCCESS;
}