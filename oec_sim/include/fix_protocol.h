/****************************************************************************
** Project name:    US Blink Simulator
** File name:       fix_protocol.h
** Description:     N/A
** Specification:   FINANCIAL INFORMATION EXCHANGE PROTOCOL (FIX)
**                  Version 4.2
**                  May 1, 2001
**
** Author:          Thien Nguyen-Tai
** First created:   April 24, 2014
** Last updated:    May 12, 2014
****************************************************************************/

#ifndef __FIX_PROTOCOL__
#define __FIX_PROTOCOL__

#include <time.h>

#define FIX_TAG_BEGIN_STRING        8
#define FIX_TAG_BODY_LENGTH         9
#define FIX_TAG_MSG_TYPE            35
#define FIX_TAG_SENDER_COMPID       49
#define FIX_TAG_TARGET_COMPID       56
#define FIX_TAG_MSG_SEQNO           34
#define FIX_TAG_SENDING_TIME        52
#define FIX_TAG_CHECKSUM            10
#define FIX_TAG_ENCRYPT_METHOD      98
#define FIX_TAG_HEARBEAT_INT        108
#define FIX_TAG_BEGIN_SEQNO         7
#define FIX_TAG_END_SEQNO           16
#define FIX_TAG_NEW_SEQNO           36
#define FIX_TAG_REF_SEQNO           45

#define FIX_RESERVE_HEADER_LEN      20
#define FIX_MSG_CHECKSUM_LEN        7

#define FIX_MSG_TYPE_LOGON             'A'
#define FIX_MSG_TYPE_NEW_ORDER         'D'
#define FIX_MSG_TYPE_CANCEL_ORDER      'F'
#define FIX_MSG_TYPE_LOGOUT            '5'
#define FIX_MSG_TYPE_RESEND_REQUEST    '2'
#define FIX_MSG_TYPE_SEQ_RESET         '4'
#define FIX_MSG_TYPE_HEARTBEAT         '0'
#define FIX_MSG_TYPE_REJECT            '3'
#define FIX_MSG_TYPE_CANCEL_REJECT     '9'
#define FIX_MSG_TYPE_EXECUTION_REPORT  '8'

#define FIX_TIMEINFORCE_DAY            '0'
#define FIX_TIMEINFORCE_IOC            '3'

#define FIX_SIDE_BUY                   '1'
#define FIX_SIDE_SELL                  '2'
#define FIX_SIDE_SELL_SHORT            '5'
#define FIX_SIDE_SELL_SHORT_EXEMPT     '6'

#define FIX_EXEC_TRANS_TYPE_NEW        '0'
#define FIX_EXEC_TRANS_TYPE_CANCEL     '1'
#define FIX_EXEC_TRANS_TYPE_CORRECT    '2'
#define FIX_EXEC_TRANS_TYPE_STATUS     '3'

#define FIX_EXEC_TYPE_ACK              '0'
#define FIX_EXEC_TYPE_PARTIAL          '1'
#define FIX_EXEC_TYPE_FILL             '2'
#define FIX_EXEC_TYPE_CANCELLED        '4'
#define FIX_EXEC_TYPE_REPLACE          '5'
#define FIX_EXEC_TYPE_PENDING_CANCEL   '6'
#define FIX_EXEC_TYPE_STOPPED          '7'
#define FIX_EXEC_TYPE_REJECTED         '8'
#define FIX_EXEC_TYPE_EXPIRED          'C'
#define FIX_EXEC_TYPE_PENDING_REPLACE  'E'

#define FIX_ORDER_STATUS_ACK              '0'
#define FIX_ORDER_STATUS_PARTIALLY        '1'
#define FIX_ORDER_STATUS_FILLED           '2'
#define FIX_ORDER_STATUS_CANCELLED        '4'
#define FIX_ORDER_STATUS_REPLACED         '5'
#define FIX_ORDER_STATUS_PENDING_CANCEL   '6'
#define FIX_ORDER_STATUS_STOPPED          '7'
#define FIX_ORDER_STATUS_REJECTED         '8'
#define FIX_ORDER_STATUS_EXPIRED          'C'
#define FIX_ORDER_STATUS_PENDING_REPLACE  'E'

#define FIX_HANDL_INST_AUTO_NO_BROKER  '1'
#define FIX_HANDL_INST_AUTO_BROKER_OK  '2'
#define FIX_HANDL_INST_MANUAL_ORDER    '3'

/****************************************************************************
- Function name:    FIX_GetTagValue
- Input:            'msg': message; 'tag': tag number
- Output:           'value': string of value of tag
- Return:           -1 if msg doesn't have tag, or the offset of next tag follow this tag in msg
- Description:      Get value string of the tag in msg
- Usage:            N/A
****************************************************************************/
int FIX_GetTagValue (const char* msg, const int tag, char* value);

/****************************************************************************
- Function name:    FIX_AddTagValueChar
- Input:            'tag': tag number; 'value': character value
- Output:           'msg': message of this tag in format "<tag>=<value><SOH>"
- Return:           Length of this tag
- Description:      Create a message of the tag
- Usage:            N/A
****************************************************************************/
int FIX_AddTagValueChar (char* msg, const int tag, const char value);

/****************************************************************************
- Function name:    FIX_AddTagValueString
- Input:            'tag': tag number; 'value': string value
- Output:           'msg': message of this tag in format "<tag>=<value><SOH>"
- Return:           Length of this tag
- Description:      Create a message of the tag
- Usage:            N/A
****************************************************************************/
int FIX_AddTagValueString (char* msg, const int tag, const char* value);

/****************************************************************************
- Function name:    FIX_AddTagValueInt
- Input:            'tag': tag number; 'value': interger value
- Output:           'msg': message of this tag in format "<tag>=<value><SOH>"
- Return:           Length of this tag
- Description:      Create a message of the tag
- Usage:            N/A
****************************************************************************/
int FIX_AddTagValueInt (char* msg, const int tag, const int value);

/****************************************************************************
- Function name:    FIX_AddTagValueDouble
- Input:            'tag': tag number; 'value': double value
- Output:           'msg': message of this tag in format "<tag>=<value><SOH>"
- Return:           Length of this tag
- Description:      Create a message of the tag
- Usage:            N/A
****************************************************************************/
int FIX_AddTagValueDouble (char* msg, const int tag, const double value);

/****************************************************************************
- Function name:    FIX_AddTagValueTime
- Input:            'tag': tag number; 'value': time_t value
- Output:           'msg': message of this tag in format "<tag>=<time format><SOH>"
- Return:           Length of this tag
- Description:      Create a message of the tag
- Usage:            N/A
****************************************************************************/
int FIX_AddTagValueTime (char* msg, const int tag, const time_t value);

/****************************************************************************
- Function name:    FIX_CalculateChecksum
- Input:            'buf': buffer; 'bufLen': buffer length
- Output:           N/A
- Return:           Checksum value
- Description:      Calculate Checksum value
- Usage:            N/A
****************************************************************************/
unsigned int FIX_CalculateChecksum(const char* buf, const long bufLen);

/****************************************************************************
- Function name:    FIX_BuildCommonTagsField
- Input:            'msgType': message type; 'senderComp': name of ETP Holder;
                    'targetComp': name of Exchange; 'seqNum': sequence number
- Output:           'msg': message of those tags in format "<tag>=<value><SOH><tag>=<value><SOH><tag>=<value><SOH>..."
                    'seqNum': sequence number increase by 1
- Return:           Length of this message
- Description:      Create a message of common tags in Standard Message Header 
- Usage:            N/A
****************************************************************************/
int FIX_BuildCommonTagsField(char* msg, const char msgType, const char* senderComp, const char* targetComp, int* seqNum);

/****************************************************************************
- Function name:    FIX_BuildMsgWithHeaderField
- Input:            'msg': message with body and reserve header; 'beginStr': begin string; 'bodyLen': body length
- Output:           'headerLen': header length of this message
- Return:           Final message
- Description:      Create entire message in FIX protocol with header
- Usage:            N/A
****************************************************************************/
char* FIX_BuildMsgWithHeaderField(char* msg, const char* beginStr, const int bodyLen, int* headerLen);

/****************************************************************************
- Function name:    FIX_BuildMsgWithTrailerField
- Input:            'msg': message with header and body; 'bodyLen': body length; 'headerLen': header length
- Output:           N/A
- Return:           Final message
- Description:      Create entire message in FIX protocol with trailer checksum
- Usage:            N/A
****************************************************************************/
char* FIX_BuildMsgWithTrailerField(char* msg, const int bodyLen, const int headerLen);

/****************************************************************************
- Function name:    FIX_StringReplaceAll
- Input:            'inStr': string need to replace; 'regular': regular string; 'replacement': replacement string
- Output:           'outStr': string replaced
- Return:           Total match
- Description:      Replaces all matches for regular within this inStr with the given replacement.
- Usage:            N/A
****************************************************************************/
int FIX_StringReplaceAll(char* outStr, const char* inStr, const char* regular, const char* replacement);

#endif //__FIX_PROTOCOL__
