#ifndef _SIM
#define _SIM

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <netinet/in.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>
#include "global.h"
#include "utility.h"

typedef struct MSGHANDLER
{
  int (*receiveMsg[MAX_VENUE_TYPE]) (t_Connection* connection, t_DataBlock *dataBlock);
  int (*handleMsg[MAX_VENUE_TYPE]) (t_Connection* connection, t_DataBlock *dataBlock);
} t_MsgHandler;

typedef struct SERVERHANDLER
{
  int index;
  int serverSock;
} t_ServerHandler;

typedef struct COMPDATA
{
  int             venueIndex;
  int             clientIndex;
  t_DataBlock     data;
} t_CompData;

#endif
