/****************************************************************************
** Project name:    US Blink Simulator
** File name:       ouch42.h
** Description:     N/A
** Specification:   OUCH42 
**                  Version 4.2
**                  May 01, 2013
**
** Author:          Thien Nguyen-Tai
** First created:   May 09, 2014
** Last updated:    May 15, 2014
****************************************************************************/

#ifndef __OUCH42__
#define __OUCH42__

#include "global.h"
#include "soupbintcp.h"
#include "ufo.h"
#include "ouch42.h"

#define LOGIN_REQUEST_PACKET_TYPE_OUCH42              'L'
#define CLIENT_HEARTBEAT_PACKET_TYPE_OUCH42           'R'
#define LOGOUT_REQUEST_PACKET_TYPE_OUCH42             'O'
#define UNSEQUENCED_DATA_PACKET_TYPE_OUCH42          'U'


// OUCH protocol
#define ENTER_ORDER_MSG_TYPE_OUCH42                   'O'
#define OUCH42_CANCEL_MSG_TYPE                        'X'

#define TIF_IOC_OUCH42                                0
#define TIF_DAY_OUCH42                                99999
#define SIDE_BUY_OUCH42                               'B'
#define SIDE_SELL_OUCH42                              'S'
#define SIDE_SELL_SHORT_OUCH42                        'T'
#define ORDER_STATE_LIVE_OUCH42                       'L'
#define ORDER_STATE_DEAD_OUCH42                       'D'

#define CANCEL_REASON_USER_OUCH42                     'U'
#define CANCEL_REASON_IOC_OUCH42                      'I'
#define CANCEL_REASON_SYSTEM_OUCH42                   'Z'

#define LIQUIDITY_FLAG_ADDED_OUCH42                   'A'
#define LIQUIDITY_FLAG_REMOVED_OUCH42                 'R'



#define OUCH_ENTER_ORDER_MSG_LEN                   49
#define OUCH_CANCEL_MSG_LEN                        19
#define OUCH_ACCEPTED_MSG_LEN                      66
#define OUCH_EXECUTED_MSG_LEN                      40
#define OUCH_REJECTED_MSG_LEN                      24
#define OUCH_CANCELLED_MSG_LEN                     28
#define OUCH_CANCEL_REJECT_MSG_LEN                 23

#define OUCH_ENTER_ORDER_MSG_TYPE                  'O'
#define OUCH_CANCEL_MSG_TYPE                       'X'
#define OUCH_ACCEPTED_MSG_TYPE                     'A'
#define OUCH_EXECUTED_MSG_TYPE                     'E'
#define OUCH_REJECTED_MSG_TYPE                     'J'
#define OUCH_CANCELLED_MSG_TYPE                    'C'
#define OUCH_CANCEL_REJECT_MSG_TYPE                'I'

#define OUCH_TIF_IOC                               0
#define OUCH_TIF_DAY                               0x1869F
#define OUCH_SIDE_BUY                              'B'
#define OUCH_SIDE_SELL                             'S'
#define OUCH_SIDE_SELL_SHORT                       'T'
#define OUCH_ORDER_STATE_LIVE                      'L'
#define OUCH_ORDER_STATE_DEAD                      'D'
#define OUCH_BBO_WEIGHT_INDICATOR_UNSPECIFIED      " "

#define OUCH_CANCEL_REASON_USER                    'U'
#define OUCH_CANCEL_REASON_IOC                     'I'
#define OUCH_CANCEL_REASON_SYSTEM                  'Z'

#define OUCH_LIQUIDITY_FLAG_ADDED                  'A'
#define OUCH_LIQUIDITY_FLAG_REMOVED                'R'

int HandleMsg_OUCH42(t_Connection* client, t_DataBlock *dataBlock);

#endif // __OUCH42__
