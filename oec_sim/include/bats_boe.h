#ifndef __BATS_BOE__
#define __BATS_BOE__

#include "global.h"

#define BATS_BOE_START_OF_MESSAGE     0xBA

#define BATS_BOE_SIDE_BUY             '1'
#define BATS_BOE_TIMEINFORCE_DAY      '0'
#define BATS_BOE_TIMEINFORCE_IOC      '3'

#define BATS_BOE_LOGON_RESPONSE_SHORT_LEN       165
#define BATS_BOE_LOGOUT_RESPONSE_SHORT_LEN      76
#define BATS_BOE_CANCEL_REJECT_SHORT_LEN        107

#define BATS_BOE_SERVER_HEARTBEAT_LEN           10
#define BATS_BOE_REPLAY_COMPLETE_LEN            10

#define BATS_BOE_ACK_MAX_LEN                    203
#define BATS_BOE_REJECTED_MAX_LEN               185
#define BATS_BOE_CANCELLED_MAX_LEN              195
#define BATS_BOE_CANCEL_REJECT_MAX_LEN          107
#define BATS_BOE_EXECUTION_MAX_LEN              162

#define BATS_BOE_LOGON_RESPONSE_TYPE            0x07
#define BATS_BOE_LOGOUT_RESPONSE_TYPE           0x08
#define BATS_BOE_SERVER_HEARTBEAT_TYPE          0x09
#define BATS_BOE_ACK_TYPE                       0x0A
#define BATS_BOE_REJECTED_TYPE                  0x0B
#define BATS_BOE_CANCELLED_TYPE                 0x0F
#define BATS_BOE_CANCEL_REJECT_TYPE             0x10
#define BATS_BOE_EXECUTION_TYPE                 0x11
#define BATS_BOE_REPLAY_COMPLETE_TYPE           0x13

int HandleMsg_BATS(t_Connection* client, t_DataBlock *dataBlock);

#endif // __BATS_BOE__
