/****************************************************************************
** Project name:    US Blink Simulator
** File name:       ufo.h
** Description:     N/A
** Specification:   UFO (UDP for Orders)
**                  Version 1.0 
**                  July 3, 2008  
**
** Author:          Thien Nguyen-Tai
** First created:   May 15, 2014
** Last updated:    May 15, 2014
****************************************************************************/

#ifndef __UFO__
#define __UFO__

#include "global.h"


#define UFO_MAX_NUMBER_MESSAGES                       1
#define UFO_HEADER_MESSAGE_BLOCK_LEN                  2

#define UFO_SEQUENCED_MESSAGE_BLOCK_OFFSET            7
#define UFO_HEADER_SEQUENCED_PACKET_LEN               UFO_SEQUENCED_MESSAGE_BLOCK_OFFSET
#define UFO_HEADER_UNSEQUENCED_MSG_LEN                1

#define UFO_LOGIN_ACCEPTED_PACKET_LEN                 15
#define UFO_LOGIN_REJECTED_PACKET_LEN                 2
#define UFO_SERVER_HEARTBEAT_PACKET_LEN               UFO_HEADER_SEQUENCED_PACKET_LEN

#define UFO_LOGIN_ACCEPTED_PACKET_TYPE                'A'
#define UFO_LOGIN_REJECTED_PACKET_TYPE                'J'
#define UFO_SEQUENCED_DATA_PACKET_TYPE                'S'
#define UFO_SERVER_HEARTBEAT_PACKET_TYPE              UFO_SEQUENCED_DATA_PACKET_TYPE

#define UFO_LOGIN_REQUEST_MSG_LEN                     27
#define UFO_LOGOUT_REQUEST_MSG_LEN                    1
#define UFO_CLIENT_HEARTBEAT_MSG_LEN                  1

#define UFO_LOGIN_REQUEST_MSG_TYPE                    'L'
#define UFO_LOGOUT_REQUEST_MSG_TYPE                   'O'
#define UFO_CLIENT_HEARTBEAT_MSG_TYPE                 'R'
#define UFO_UNSEQUENCED_MSG_TYPE                      'U'


#define UFO_LOGIN_REJECTED_NOT_AUTHORIZED             'A'
#define UFO_LOGIN_REJECTED_NOT_SESSION                'S'

 
/****************************************************************************
- Function name:    UFO_GetContent_UnsequencedMsg
- Input:            'dataBlock': the data
- Output:           content: data content; len: length of the content
- Return:           SUCCESS or ERROR
- Description:      Get data of application message
- Usage:            N/A
****************************************************************************/
char* UFO_GetContent_UnsequencedMsg (t_DataBlock* dataBlock, int* len);
 
/****************************************************************************
- Function name:    UFO_BuildAndSend_ServerHeartbeatPacket
- Input:            'connection': the connection
- Output:           N/A
- Return:           SUCCESS or ERROR
- Description:      Build and send Server Heartbeat Packet
- Usage:            N/A
****************************************************************************/
int UFO_BuildAndSend_ServerHeartbeatPacket (t_Connection* connection);

/****************************************************************************
- Function name:    UFO_BuildAndSend_LoginAcceptedPacket
- Input:            'connection': the connection; 'sessionId': The session is now logged into; 'seqNum': The sequence number
- Output:           N/A
- Return:           SUCCESS or ERROR
- Description:      Build and send Login Accepted Packet 
- Usage:            N/A
****************************************************************************/
int UFO_BuildAndSend_LoginAcceptedPacket (t_Connection* connection, const char* sessionId);

/****************************************************************************
- Function name:    UFO_BuildAndSend_LoginRejectedPacket
- Input:            'connection': the connection; 'reason': reject reason
- Output:           N/A
- Return:           SUCCESS or ERROR
- Description:      Build and send Login Rejected Packet
- Usage:            N/A
****************************************************************************/
int UFO_BuildAndSend_LoginRejectedPacket (t_Connection* connection, const char reason);

/****************************************************************************
- Function name:    UFO_BuildAndSend_SequencedDataPacket
- Input:            'connection': the connection; 'msg': content message; 'msgLen': message length
- Output:           N/A
- Return:           SUCCESS or ERROR
- Description:      Build and send Sequenced Data Packet
- Usage:            N/A
****************************************************************************/
int UFO_BuildAndSend_SequencedDataPacket (t_Connection* connection, const char* msg, const int msgLen);

/****************************************************************************
- Function name:    UFO_BuildAndSend_ClientHeartbeatMsg
- Input:            'connection': the connection
- Output:           N/A
- Return:           SUCCESS or ERROR
- Description:      Build and send Client Heartbeat Packet
- Usage:            N/A
****************************************************************************/
int UFO_BuildAndSend_ClientHeartbeatMsg (t_Connection* connection);

/****************************************************************************
- Function name:    UFO_BuildAndSend_LoginRequestMsg
- Input:            'connection': the connection
- Output:           N/A
- Return:           SUCCESS or ERROR
- Description:      Build and send Login Request Packet  
- Usage:            N/A
****************************************************************************/
int UFO_BuildAndSend_LoginRequestMsg (t_Connection* connection, const char* userName, const char* password, const char* sessionId);

/****************************************************************************
- Function name:    UFO_BuildAndSend_UnsequencedMsg
- Input:            'connection': the connection
- Output:           N/A
- Return:           SUCCESS or ERROR
- Description:      Build and send Unsequenced Data Packet
- Usage:            N/A
****************************************************************************/
int UFO_BuildAndSend_UnsequencedMsg (t_Connection* connection, const char* msg, const int msgLen);

/****************************************************************************
- Function name:    UFO_BuildAndSend_LogoutRequestMsg
- Input:            'connection': the connection
- Output:           N/A
- Return:           SUCCESS or ERROR
- Description:      Build and send Logout Request Packet
- Usage:            N/A
****************************************************************************/
int UFO_BuildAndSend_LogoutRequestMsg (t_Connection* connection);

/****************************************************************************
- Function name:    UFO_Handle_LogonPacket
- Input:            'connection': the connection; 'dataBlock': data
- Output:           N/A
- Return:           SUCCESS or ERROR
- Description:      N/A
- Usage:            N/A
****************************************************************************/
int UFO_Handle_LogonPacket(t_Connection* connection, t_DataBlock *dataBlock);

/****************************************************************************
- Function name:    UFO_Handle_LogoutPacket
- Input:            'connection': the connection; 'dataBlock': data
- Output:           N/A
- Return:           SUCCESS or ERROR
- Description:      N/A
- Usage:            N/A
****************************************************************************/
int UFO_Handle_LogoutPacket(t_Connection* connection, t_DataBlock *dataBlock);

/****************************************************************************
- Function name:    UFO_HeartbeatThread
- Input:            'arg': the connection
- Output:           N/A
- Return:           SUCCESS or ERROR
- Description:      N/A
- Usage:            N/A
****************************************************************************/
void* UFO_HeartbeatThread(void* arg);

#endif //__UFO__
