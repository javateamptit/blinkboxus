/*****************************************************************************
**  Project:    Equity Arbitrage Application
**  Filename:   order_mgmt_proc.h
**  Description:  This file contains some declaration for order_mgmt_proc.c 
**  Author:     Luan Vo-Kinh
**  First created:  13-Sep-2007
**  Last updated: -----------
*****************************************************************************/

#ifndef __ORDER_MGMT_PROC_H__
#define __ORDER_MGMT_PROC_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include <stdlib.h>

#include "global_definition.h"
#include "utility.h"
#include "order_file_creation.h"

// Maximum seconds to increase position
#define MAX_SECONDS_INCREASE_POSITION 3

// Maximum bytes of order message
#define MAX_ORDER_MSG 400

// Maximum order id range
#define MAX_ORDER_ID_RANGE 400000

// Maximum open order send
#define MAX_OPEN_ORDER_SEND 2000

// For time in force
#define DAY_TYPE 0
#define IOC_TYPE 1

// For broken trade
#define BROKEN_TRADE_REMOVED 2
#define BROKEN_TRADE_PRICE_CHANGED 1
#define BROKEN_TRADE_NONE 0

// For updated and sent status
#define OPEN_ORDER_UPDATED 1
#define OPEN_ORDER_SENT 0

// For side
#define BUY_TO_CLOSE_TYPE 0
#define SELL_TYPE 1
#define SHORT_SELL_TYPE 2
#define BUY_TO_OPEN_TYPE 3

#define BUY_SIDE 1
#define SELL_SIDE -1
#define MAX_SYMBOL_TRADE 15000
#define MAX_TYPE_CHAR 31
#define INDEX_OF_A_CHAR 60

#define HALT_REASON_ARCAREJECT 1
#define HALT_REASON_NASDAQREJECT 2
#define HALT_REASON_CTS 3
#define HALT_REASON_UTDF 4
#define HALT_REASON_USER 5
#define HALT_REASON_BATSZREJECT 6
#define HALT_REASON_NYSEREJECT 7
#define HALT_REASON_EDGXREJECT 8
#define HALT_REASON_NULL 0

// Stock symbol types
#define SYMBOL_TYPE_LISTED 0
#define SYMBOL_TYPE_ETF 1
#define SYMBOL_TYPE_OTC 2

#define AS_MANUAL_ORDER 0
#define PM_MANUAL_ORDER 1
/****************************************************************************
** Data structures definitions
****************************************************************************/
// This structure is used to contain status list of order placement
typedef struct t_OrderPlacementStatus
{
  char Sent[16];
  char Live[16];
  char Rejected[16];
  char Closed[16];
  char BrokenTrade[16];
  char CanceledByECN[16];
  char CXLSent[16];
  char CancelACK[16];
  char CancelRejected[16];
  char CancelPending[16]; 
  char CanceledByUser[16];
} t_OrderPlacementStatus;

// This structure is used to contain index of status list of order placement
typedef struct t_OrderStatusIndex
{ 
  int Sent;
  int Live;
  int Rejected;
  int Closed;
  int BrokenTrade;
  int CanceledByECN;
  int CXLSent;
  int CancelACK;
  int CancelRejected;
  int CancelPending;
  int CanceledByUser;
} t_OrderStatusIndex;

// This structure is used to contain all fields of order message
typedef struct tFieldIndex
{
  int BBOWeightIndicator;
  int ISO_Flag;
  int ArcaVariant;
  int ExDestination;
  int NoSelfTrade;
  int OrderState;
  int CrossType;
  int MinQuantity;
  int Eligibility;
  int Capacity;
  int SenderSubID; 
  int ExecBroker;
  int RandomReserve;
  int Rule80AIndicator; 
  int MsgType; 
  int Length;
  int InSeqNum;
  int OutSeqNum;
  int ClOrdID;
  int PCSLinkID; 
  int OrdQty;
  int PriceField; 
  int PriceScale;
  int Symbol; 
  int CompGroupID; 
  int DeliverToCompID; 
  int ExecInst;
  int Side;
  int OrdType; 
  int Time_In_Force; 
  int Rule80A;
  int TradingSessionID;
  int Account;
  int ArcaOrderID;
  int SendingTime;
  int TransactionTime;
  int ExecID;
  int ArcaExID;
  int LiquidityIndicator;
  int SendDate;
  int OriginalClientOrdID;
  int RejectText;
  int Type; 
  int DiscretionPegDifference;
  int DiscretionPegDifferenceSign; 
  int DiscretionPegType;
  int DiscretionPrice; 
  int PegDifference; 
  int PegDifferenceSign; 
  int PegType;
  int MaxFloor;
  int MinQty;
  int TimeStamp;
  int MessageType;
  int Token;
  int Action;
  int Shares;
  int Stock;
  int Price;
  int TimeInForce;
  int Firm ;
  int Display;
  int OrderRefNo;
  int Reason;
  int LiquidityFlag;
  int MatchNo;
  int ExtExeInstruction;
  int ExtendedPNP;
  int ProactiveIfLocked;

  //NYSE CCG
  int DBExecID;
  int DOTReserve;
  int BillingIndicator;
  int LastMarket;
  int ContraBroker;
  int ContraTrader;
  int ExecAwayMktID;
  int BillingRate;
  int InfoCode;
  int LeavesShares;
  int OrgShares;
  int ExecRefID;

  //BATSZ
  int MatchUnit;
  int LastShares;
  int BaseLiquidityIndicator;
  int SubLiquidityIndicator;
  int AccessFee;
  int ClrAccount;
  int ClrFirm;
  int RoutInst;
  int LastPx;
  int OrgTime;
  int SymbolSfx;
  int MaxRemovePct;
  int PreventMemberMatch;
  int LocateReqd;
  int AttributeQuote;
  int DisplayPrice;
  int WorkingPrice;
  
  // EDGX
  int SpecialOrderType;
  int DiscretionOffset;
  int SymbolSuffix;
  int RouteOutEligibility;
  int RoutingDeliveryMethod;
  int ExtendedHrsEligible;  
  int ExpireTime;
    
} tFieldIndex;

// This structure is used to contain open order information
// this struct is 8-byte aligned
// Becareful while updating this struct definition!
// because it's used in memcpy() to build msg to TT
typedef struct t_OpenOrderInfo    // variable on TT
{
  double price;         //OrderHistory.Price
  double avgPrice;        //OrderHistory.avgPrice
  char symbol[SYMBOL_LEN];    //OrderHistory.Symbol
  int side;           //OrderHistory.Action
  int shares;           //OrderHistory.Quantity
  int leftShares;         //OrderHistory.LVS
  int ECNId;            //OrderHistory.ECNOrder
  int clOrdId;          //OrderHistory.SequenceID
  int status;           //OrderHistory.Status
  int tif;            //OrderHistory.TimeInforce
  int tsId;           //OrderHistory.WhichTS
  char timestamp[MAX_TIMESTAMP];  //OrderHistory.TimeStamp
  int tradeType;          //OrderHistory.TradeType
  char account;
  char _padding[3];
  int crossId;
} t_OpenOrderInfo;

// This structure is used to contain order detail
typedef struct t_OrderDetail  //8byte aligned
{
  int length;
  int isBroken;
  long execId;
  double newPrice;
  char timestamp[MAX_TIMESTAMP];
  char _padding[4];
  char msgContent[MAX_ORDER_MSG];
} t_OrderDetail;

// This structure is used to contain information of open order list 
typedef struct t_OpenOrderCollection  //8byte aligned
{
  t_OpenOrderInfo orderSummary;
  int lifeTimeInSecond;
  int isNewOrder;
  int isBroken; 
  int countOrderDetail;
  int canceledShares;
  char isUpdated[MAX_TRADER_TOOL_CONNECTIONS];
  char countSent[MAX_TRADER_TOOL_CONNECTIONS];
  int indexOrderDetailList[MAX_ORDER_DETAIL];
} t_OpenOrderCollection;

// This structure is used to management open order
typedef struct t_ASOpenOrderMgmt
{
  t_OpenOrderCollection orderCollection[MAX_ORDER_PLACEMENT];
  int countOpenOrder;
  int _padding;
} t_ASOpenOrderMgmt;

// This structure is used to management order detail
typedef struct t_ASOrderDetailMgmt
{
  t_OrderDetail orderDetailList[MAX_RAW_DATA_TOTAL_ENTRIES];
  int countOrderDetail;
  int _padding;
} t_ASOrderDetailMgmt;

// This structure is used to contain new order information
typedef struct t_SendNewOrder //8byte aligned
{
  char symbol[SYMBOL_LEN];
  int side;
  int shares;
  double price;
  double pegDef;
  int maxFloor;
  int ECNId;
  int tif;
  int orderId;
  int tradingAccount;
} t_SendNewOrder;

typedef struct t_PositionBk
{
  int leftShares;
  int totalShares;
  double avgPrice;
  //char prevPositionState; //1: Normal, 6: Stuck
  int colorFlag;
  char __padding[7];
} t_PositionBk;

typedef struct t_RiskManagementInfo //8byte aligned
{
  int leftShares;
  int totalShares;
  int timestamp;
  int colorFlag;
  char positionKey[64]; //generated by combination of: symbol_account_Blink
  double avgPrice;
  double matchedPL;
  double totalMarket;
  double totalFee;
  t_PositionBk oldPositionSent[MAX_TRADER_TOOL_CONNECTIONS];
} t_RiskManagementInfo;

typedef struct t_SymbolInfo
{
  char symbol[SYMBOL_LEN];
  char isVolatile;
  double lastTradedPrice;
  double askPrice;
  double bidPrice;
} t_SymbolInfo;

typedef struct t_RiskManagementCollection
{
  int countSymbol;
  t_RiskManagementInfo collection[MAX_ACCOUNT][MAX_SYMBOL_TRADE];
  t_SymbolInfo symbolList[MAX_SYMBOL_TRADE];
} t_RiskManagementCollection;

typedef struct t_ProfitLossPerSymbol
{
  double tsLoss[MAX_TRADE_SERVER_CONNECTIONS];
  double asLoss;
  int listID; // 0: do not add to any list, 1: volatile list, 2: disabled list
} t_ProfitLossPerSymbol;

typedef struct t_LongShortInfo  //8 byte aligned
{
  char symbol[SYMBOL_LEN];
  double price;
  int shares;
  char side;
  char account;
} t_LongShortInfo;

typedef struct t_LongShortCollection
{
  t_LongShortInfo collection[MAX_SYMBOL_TRADE];
  int countSymbol;
  int _padding;
} t_LongShortCollection;

typedef struct t_BrokenTradeInfo  //8byte aligned
{
  long execId;
  double newPrice;
  int isUpdate;
  int oid;
  int brokenShares;
  int clOrdId;
  int tsId;
  int newShares;
  char symbol[SYMBOL_LEN];
  int account;
  int fillId;
} t_BrokenTradeInfo;

typedef struct t_BrokenTradeCollection
{
  t_BrokenTradeInfo collection[MAX_BROKEN_TRADE];
  int countBrokenTrade;
  int _padding;
} t_BrokenTradeCollection;

typedef struct t_FastMarketManagement //4byte aligned
{
  int thresHold;
  int currentOrdersPerSec;
  char mode;    // FAST_MARKET_MODE or NORMAL_MARKET_MODE
  char isCheck; // Initialize=-1, if 1: we will NOT do the check for switching, if 2: we will do!
  char _padding[2];
}t_FastMarketManagement;

//For verify position
#define TRADING_NONE 0
#define TRADING_MANUAL 1
#define TRADING_AUTOMATIC 2
#define TRADING_BOTH_MANUAL_AUTOMATIC 3

typedef struct t_VerifyPositionInfo   //4byte aligned
{
  int shares;
  int alertMonitoringTimeInSeconds;  // used for alert
  char alreadyAlerted;
  char side;
  char isTrading;
  char isPMOwner;
  char isSleeping;
  int lockCounter;
  int pmHandleInSeconds;
} t_VerifyPositionInfo;

typedef struct t_ManualOrderInfo  //4byte aligned
{
  char symbol[SYMBOL_LEN];
  int filledShares;
  int leftShares;
  char side;
  char username[31];  //from database (user who places manual order)
} t_ManualOrderInfo;

typedef struct t_PMOrderInfo  //4byte aligned
{
  char symbol[SYMBOL_LEN];
  int filledShares;
  int leftShares;
  char side;
  char _padding[3];
} t_PMOrderInfo;

typedef struct t_ARCADIRECTOrderInfo
{
  char symbol[SYMBOL_LEN];
  int shares;
  char side;
} t_ARCADIRECTOrderInfo;

//This struct stores infomation need to build cancel order message
//and is used for both RASH and ARCA DIRECT
typedef struct t_ManualCancelOrder
{
  //For RASH
  char orderToken[16];
  
  //For ARCA DIRECT
  int orderQty;
  char ecnOrderID[24];
  char symbol[SYMBOL_LEN];
  char origClOrdID[20];
  char newClOrdID[20];
  char side;
  char _padding[3];
} t_ManualCancelOrder;

#define MAX_VOLATILE_TOGGLE 100
typedef struct t_VolatileMgmt
{
  long sectionFileTimeStamp;
  long listFileTimeStamp;

  pthread_mutex_t mutexLock;
  
  int currentAllVolatileValue;  //0:off, 1:on
  int allVolatileOnTime[MAX_VOLATILE_TOGGLE];
  int allVolatileOffTime[MAX_VOLATILE_TOGGLE];
  int numAllVolatileToggle;
} t_VolatileMgmt;

typedef struct t_HaltStatusMgmt
{
  char symbol[MAX_STOCK_SYMBOL][SYMBOL_LEN];
  char haltStatus[MAX_STOCK_SYMBOL][TS_MAX_BOOK_CONNECTIONS];
  unsigned int haltTime[MAX_STOCK_SYMBOL][TS_MAX_BOOK_CONNECTIONS];
  char isAutoResume[MAX_STOCK_SYMBOL];  // Possible values: 0, 1, 2
  pthread_mutex_t mutexLock;
} t_HaltStatusMgmt;

#define REJECT_CODE_NUM               500
#define REJECT_TEXT_LEN               128
#define REJECT_CODE_LEN               8

typedef struct t_DisableRejectMgmt
{
  int countReason;
  int disableAlert[REJECT_CODE_NUM];
  char reasonText[REJECT_CODE_NUM][REJECT_TEXT_LEN];
  char reasonCode[REJECT_CODE_NUM][REJECT_CODE_LEN];
}t_DisableRejectMgmt;

typedef struct t_CancelOrderInfo
{
  int clOrderId;
  char ecnOrderID[21];
  int ttl; // in second
  int marked;
  int leftShares;
}t_CancelOrderInfo;

typedef struct t_AutoCancelOrder
{
  int threadStarted;
  t_CancelOrderInfo orderInfo[MAX_MANUAL_ORDER];
}t_AutoCancelOrder;

extern t_DisableRejectMgmt DisableRejectMgmt[MAX_ORDER_TYPE];

/****************************************************************************
** Global variables definition
****************************************************************************/
extern t_OrderPlacementStatus orderStatus;

extern t_OrderStatusIndex orderStatusIndex;

extern tFieldIndex fieldIndexList;

extern t_ASOpenOrderMgmt asOpenOrder;

extern t_ASOrderDetailMgmt asOrderDetail;

extern t_RiskManagementCollection RMList;

extern t_ProfitLossPerSymbol ProfitLossPerSymbol[MAX_SYMBOL_TRADE];

extern t_LongShortCollection longShortList;

extern short symboIndexlList[MAX_TYPE_CHAR][MAX_TYPE_CHAR][MAX_TYPE_CHAR][MAX_TYPE_CHAR][MAX_TYPE_CHAR][2];
extern short longSymbolIndexArray[MAX_STOCK_SYMBOL][MAX_TYPE_CHAR][MAX_TYPE_CHAR][2];
extern short extraLongSymbolIndexArray[MAX_STOCK_SYMBOL][MAX_TYPE_CHAR][2];

extern int orderIdIndexList[MAX_TRADE_SERVER_CONNECTIONS + 1][MAX_ORDER_ID_RANGE];

extern t_BrokenTradeCollection brokenTradeList;

extern t_FastMarketManagement FastMarketMgmt;

//For verify position
extern t_VerifyPositionInfo verifyPositionInfo[MAX_ACCOUNT][MAX_STOCK_SYMBOL];
extern t_ManualOrderInfo manualOrderInfo[MAX_MANUAL_ORDER];
extern t_ARCADIRECTOrderInfo ManualArcaDirectOrderInfo[MAX_MANUAL_ORDER];

extern char Manual_Order_Account_Mapping[MAX_ORDER_PLACEMENT];
extern t_PMOrderInfo pmOrderInfo[MAX_ORDER_PLACEMENT];
extern t_ARCADIRECTOrderInfo PMArcaDirectOrderInfo[MAX_ORDER_PLACEMENT];

extern t_HaltStatusMgmt HaltStatusMgmt;
extern int AutoDisableTrading[TS_MAX_ORDER_CONNECTIONS];

extern unsigned long ExchangeOrderIdMapping[MAX_TRADE_SERVER_CONNECTIONS + 1][MAX_ORDER_PLACEMENT];
extern t_AutoCancelOrder AutoCancelOrder;
/****************************************************************************
** Function declarations
****************************************************************************/
void InitExchangeOrderIdMapping(void);

void InitializeManualOrderAccountMapping(void);

int InitializeASOpenOrderStructures(void);

int InitializeOrderStatus(void);

int InitializeOrderStatusIndex(void);

int InitializeFieldIndexList(void);

int InitializeRiskManagementStructure(void);

int InitializeProfitLossPerSymbolStructure(void);

int InitializeLongShortStructure(void);

int InitializeVerifyPositionInfoStructure(void);

int InitializeManualOrderInfoStructure(void);

int InitializeSymbolIndexList(void);

int InitilizeOrderIdIndexList(void);

int InitializeBrokenTrade(void);

int InitializeVolatileSymbolList(void);

int GetStockSymbolIndex(const char *stockSymbol);

int GetOrderIdIndex(int clOrdId, int tsId);

int CheckOrderIdIndex(int clOrdId, int tsId);

int ProcessAddASOpenOrder(void *_openOrder, void *_orderDetail);

int ProcessUpdateASOpenOrderForOrderACK(void *_openOrder, void *_orderDetail);

int ProcessUpdateASOpenOrderForStatus(int tsId, int clOrdId, int orderStatus, void *_orderDetail);

int ProcessUpdateASOpenOrderForExecuted(int tsId, int clOrdId, int orderStatus, int shares, double price, void *_orderDetail, int account, char *liquidity, double *totalFee);

int ProcessUpdateASOpenOrderForNonExecuteOrderDetail(int tsId, int clOrdId, void *_orderDetail);

int ProcessASConnectToOrderECN(int ECNIndex);

int ProcessASDisconnectToOrderECN(int ECNIndex);

int ProcessASConnectToAllOrderECN(void);

int ProcessASDisconnectToAllOrderECN(void);

int SendNewOrderToNASDAQ_RASHOrder(void *order);

int SendNewOrderToARCA_DIRECTOrder(void *order);

int ProcessSendCancelRequestToECNOrder(int clOrdId, char *ECNOrderId);

int ProcessSendCancelRequestToNASDAQ_RASH(int indexOpenOrder);

int ProcessSendCancelRequestToARCA_DIRECT(int indexOpenOrder, char *ECNOrderId);

char *GetOrderStatus(int statusIndex);

int GetOrderStatusIndex(const char *status);

int UpdateRiskManagementCollection(char *symbol, int shareVolume, double sharePrice, int side, int timestamp, int account, double totalFee);

void CalculationPositionBuyingPower();

int GetSide(int side);

int UpdateNumberOfOrderPlaced(int tsId);

int UpdateTotalSharesFilled(int tsId, int shares);

int ResetBrokenTrade(void);
int RemoveBrokenTrade(void);
int UpdateBrokenTrade(t_BrokenTradeInfo brokenTrade);
int UpdateOpenOrders(void);

int ProcessChangePrice(t_OrderDetail *orderDetail, int ECNId);

char* GetDateTimeFromEpochNsec(long nsec, char *dateTime);
void ConvertDateTimeToEpoch(char *dateTime, char *res);
int CheckBookOrderSettingOfBATS(int numSec, int tsMdcIndex);
int CheckBookOrderSettingOfEDGE(int numSec, int tsMdcIndex);
int CheckBookOrderSettingOfNYSE(int numSec, int tsMdcIndex);
int CheckBookOrderSettingOfBX(int numSec, int tsMdcIndex);
int CheckToDisableTrading(int numSec);
void InitializeFastMarketMgmtStructure(void);
void ResetFastMarketMgmtValues(void);
void UpdateMarketMode(void);
void UpdateVerifyPositionInfo(void);
int UpdateVerifyPositionInfoAfterTradeBreak(int account, int symbolIndex);
int UpdateVerifyPositionInfoForStuckFromTSOutputLog(char symbol[SYMBOL_LEN], int stuckShares, int _stuckSide, int account, int lockType);
int UpdateVerifyPositionInfoFromFillOrder(char symbol[SYMBOL_LEN], int filledShares, char side, int account, int source);

int UpdateVerifyPositionInfoFromRASHAcceptedManualOrder(t_DataBlock *dataBlock, int sentBy);
int UpdateVerifyPositionInfoFromRASHExecutedManualOrder(t_DataBlock *dataBlock, int sentBy);
int UpdateVerifyPositionInfoFromRASHCanceledManualOrder(t_DataBlock *dataBlock, int sentBy);
int UpdateVerifyPositionInfoFromRASHRejectedManualOrder(t_DataBlock *dataBlock, int sentBy);

int UpdateVerifyPositionInfoFromDIRECTAcceptedManualOrder(t_DataBlock *dataBlock, int sentBy);
int UpdateVerifyPositionInfoFromDIRECTExecutedManualOrder(t_DataBlock *dataBlock, int sentBy);
int UpdateVerifyPositionInfoFromDIRECTCanceledManualOrder(t_DataBlock *dataBlock, int sentBy);
int UpdateVerifyPositionInfoFromDIRECTRejectedManualOrder(t_DataBlock *dataBlock, int sentBy);

int IsAbleToPlaceManualOrderOnAS(int ECNId);
int IsAbleToPlaceManualOrderOnPM(int ECNId);
int IsAbleToPlaceOrderOnASOrPM(int ECNId);
int IsCTSOrUTDFConnectedOnPM();
int ProcessSendManualCancelRequestToPM(int clOrdId, char *ECNOrderId);

int InitializePMOrderInfoStructure(void);

int UpdateVerifyPositionInfoFromRASHAcceptedFromPM(t_DataBlock *dataBlock);
int UpdateVerifyPositionInfoFromRASHExecutedFromPM(t_DataBlock *dataBlock);
int UpdateVerifyPositionInfoFromRASHCanceledFromPM(t_DataBlock *dataBlock);
int UpdateVerifyPositionInfoFromRASHRejectedFromPM(t_DataBlock *dataBlock);

int UpdateVerifyPositionInfoFromDIRECTAcceptedFromPM(t_DataBlock *dataBlock);
int UpdateVerifyPositionInfoFromDIRECTExecutedFromPM(t_DataBlock *dataBlock);
int UpdateVerifyPositionInfoFromDIRECTCanceledFromPM(t_DataBlock *dataBlock);
int UpdateVerifyPositionInfoFromDIRECTRejectedFromPM(t_DataBlock *dataBlock);

void ConvertArcaSymbolToComstock(const char *root, const char *suffix, char symbol[SYMBOL_LEN]);

double CalculateFillFee(int size, double price, int side, char *liquidity, int ecnId);
int GetCurrentAllVolatileToggleValue();
int GetRejectCodeFilePath(int ECNType, char *fullPath);
int SaveRejectCodeToFileOfAOrder(int ECNType);
int LoadRejectCodeFromFileOfAOrder(int ECNType);
int LoadRejectCodeFromFileOfAllOrders(void);

int GetECNLaunchInfo(int launchType, char *ecnLaunch, char *askBid);
int GetAccountName(int venueType, int accountIndex, char *account);
int UpdateColorForOpenPositions();
void* StartSendCancelOrderThread(void *arg);
void ReserveString(char *string);
char *ConvertDecimalToBase36ASCII(long number, char *result);
char *GetOECName(const int oecType);
/***************************************************************************/

#endif
