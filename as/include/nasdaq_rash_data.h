/*****************************************************************************
**  Project:    Equity Arbitrage Application
**  Filename:   nasdaq_rash_data.h
**  Description:  This file contains some declaration for nasdaq_rash_data.c  
**  Author:     Luan Vo-Kinh
**  First created:  13-Sep-2007
**  Last updated: -----------
*****************************************************************************/

#ifndef __NASDAQ_RASH_DATA_H__
#define __NASDAQ_RASH_DATA_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include <stdlib.h>
#include <math.h>

#include "global_definition.h"
#include "utility.h"
#include "position_manager_proc.h"

/****************************************************************************
** Data structures definitions
****************************************************************************/
// The structure is used contain several fields of New Order
typedef struct t_RASHNewOrder
{
  double price;
  double pegDifference;
  double discretionPrice;
  double discretionPegDifference;
  
  int orderID;
  int seqNum;
  int tsId;
  int crossId;
  int tradeType;
  int shares;
  int leftShares;
  int minQty;
  int tradingAccount;
  int randomReserve;
  int visibleShares;
  
  short bboId;
  unsigned short entryExitRefNum;
  
  char date[12];
  char timestamp[16];
  char msgType[16];
  char side[3];
  char entryExit[6];
  char askBid[4];
  char ecnLaunch[6];
  char ecn[6];
  char symbol[SYMBOL_LEN];
  char timeInForce[4];
  char firm[6];
  char display;
  char eligibility;
  char maxFloor[8];
  char pegType;
  char pegDifferenceSign;
  char discretionPegType;
  char discretionPegDifferenceSign;
  char rule80A;
  char execBroker[6];
  char senderSubID[34];
  char status[16];
  
  char msgContent[MAX_ORDER_MSG];
} t_RASHNewOrder;

// The structure is used contain several fields of Accepted Order
typedef struct t_RASHAcceptedOrder
{
  double price;

  int orderID;
  int tsId;
  int seqNum;
  int shares;
  int rashExOrderId;
  int tradingAccount;

  char date[12];
  char timestamp[16];
  char processedTimestamp[16];
  char transTime[10];
  char msgType[16];
  char ecn[6];
  char side[3];
  char symbol[SYMBOL_LEN];
  char timeInForce[4];
  char firm[6];
  char display;
  char status[16];

  char msgContent[MAX_ORDER_MSG];
} t_RASHAcceptedOrder;

// The structure is used contain several fields of Executed Order
typedef struct t_RASHExecutedOrder
{
  double price;
  double avgPrice;
  double fillFee;

  int orderID;
  int seqNum;
  int shares;
  int leftShares;
  int executionId;

  unsigned short entryExitRefNum;

  char date[12];
  char timestamp[16];
  char processedTimestamp[16];
  char transTime[10];
  char msgType[16];
  char liquidityIndicator;
  char status[16];

  char msgContent[MAX_ORDER_MSG];
} t_RASHExecutedOrder;

// The structure is used contain several fields of Canceled Order
typedef struct t_RASHCanceledOrder
{
  int orderID;
  int seqNum;
  int shares;
  unsigned short entryExitRefNum;
  
  char date[12];
  char timestamp[16];
  char processedTimestamp[16];
  char transTime[10];
  char msgType[16];
  char reason;
  char status[16];
  
  char msgContent[MAX_ORDER_MSG];
} t_RASHCanceledOrder;

// The structure is used contain several fields of Cancel Request
typedef struct t_RASHCancelRequest
{
  int orderID;
  int seqNum;
  int shares;
  
  char date[12];
  char timestamp[16];
  char msgType[16];
  char status[16];
  
  char msgContent[MAX_ORDER_MSG];
} t_RASHCancelRequest;

// The structure is used contain several fields of Rejected Order
typedef struct t_RASHRejectedOrder
{
  int orderID;
  int seqNum;
  
  unsigned short entryExitRefNum;
  
  char date[12];
  char timestamp[16];
  char processedTimestamp[16];
  char transTime[10];
  char msgType[16];
  char reason;
  char status[16];
  
  char msgContent[MAX_ORDER_MSG];
} t_RASHRejectedOrder;

// The structure is used contain several fields of Broken Order
typedef struct t_RASHBrokenOrder
{
  int orderID;
  int seqNum;
  int executionId;

  char date[12];
  char timestamp[16];
  char transTime[10];
  char msgType[16];
  char reason;
  char status[16];

  char msgContent[MAX_ORDER_MSG];
} t_RASHBrokenOrder;


/****************************************************************************
** Global variables definition
****************************************************************************/
extern char Manual_Order_Account_Mapping[MAX_ORDER_PLACEMENT];

/****************************************************************************
** Function declarations
****************************************************************************/

void * Process_RASHOrder( void *pRASH_Conf );
int Talk2RASHOrder( int socket );
int ParseRawDataForRASHOrder( t_DataBlock dataBlock, int type );

int ProcessNewOrderOfRASHOrder( t_DataBlock dataBlock, int type, char *timeStamp );

int ProcessAcceptedOrderOfRASHOrder( t_DataBlock dataBlock, int type, char *timeStamp );

int ProcessExecutedOrderOfRASHOrder( t_DataBlock dataBlock, int type, char *timeStamp );

int ProcessCanceledOrderOfRASHOrder( t_DataBlock dataBlock, int type, char *timeStamp );

int ProcessCancelRequestOfRASHOrder( t_DataBlock dataBlock, int type, char *timeStamp );

int ProcessAcceptedCancelOfRASHOrder( t_DataBlock dataBlock, int type );

int ProcessRejectedOrderOfRASHOrder( t_DataBlock dataBlock, int type, char *timeStamp );

int ProcessBrokenOrderOfRASHOrder( t_DataBlock dataBlock, int type );

/***************************************************************************/

#endif
