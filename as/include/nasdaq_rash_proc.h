/****************************************************************************
** Project name:  Equity Arbitrage Application
** File name:     nasdaq_rash_proc.h
** Description:   This file contains some declarations for nasdaq_rash_proc.c

** Author:    Sang Nguyen-Minh
** First created on 26 September 2007
** Last updated on 26 September 2007
****************************************************************************/

#ifndef __NASDAQ_RASH_PROC_H__
#define __NASDAQ_RASH_PROC_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include "global_definition.h"
#include "position_manager_proc.h"
#include "utility.h"
#include "nasdaq_rash_data.h"

#define NASDAQ_RASH_NEW_ORDER_MSG_LEN 141
#define CANCEL_MSG_LEN 23

// NASDAQ RASH Sequence Messages
#define SEQUENCE_MSG 'S'

#define NASDAQ_RASH_ORDER_ACCEPTED 'A'
#define NASDAQ_RASH_ORDER_EXECUTED 'E'
#define NASDAQ_RASH_ORDER_CANCELED 'C'
#define NASDAQ_RASH_ORDER_REJECTED 'J'
#define NASDAQ_RASH_BROKEN_TRADE 'B'
#define NASDAQ_RASH_SYSTEM_EVENT 'S'

// NASDAQ RASH Unsequence Messages
#define NASDAQ_RASH_HEARTBEAT 'H'
#define NASDAQ_RASH_DEBUG '+'
#define NASDAQ_RASH_LOGIN_ACCEPTED 'A'
#define NASDAQ_RASH_LOGIN_REJECTED 'J'

#define NASDAQ_RASH_LOGIN_ACCEPTED_RETURN '0'
#define NASDAQ_RASH_LOGIN_REJECTED_RETURN '1'

// NASDAQ RASH Sequence Message lengths
#define NASDAQ_RASH_ORDER_ACCEPTED_LEN 158
#define NASDAQ_RASH_ORDER_EXECUTED_LEN 51
#define NASDAQ_RASH_ORDER_CANCELED_LEN 32
#define NASDAQ_RASH_ORDER_REJECTED_LEN 26
#define NASDAQ_RASH_BROKEN_TRADE_LEN 35
#define NASDAQ_RASH_SYSTEM_EVENT_LEN 12

// NASDAQ RASH Unsequence Message lengths
#define NASDAQ_RASH_HEARTBEAT_LEN 2
#define NASDAQ_RASH_LOGIN_ACCEPTED_LEN 22
#define NASDAQ_RASH_LOGIN_REJECTED_LEN 3

#define HALF_MAX_BUFFER_LEN 512
/****************************************************************************
** Data structure definitions
****************************************************************************/
typedef struct t_NASDAQ_RASH_Config
{
  int incomingSeqNum;
  int outgoingSeqNum;
  
  int enableTrading;
  int isConnected;

  int socket;
  int port;
  
  char ipAddress[80];
  char userName[80];
  char password[80];
  char buffer[2 * HALF_MAX_BUFFER_LEN];
  
  int remainingBytes;
  int numOfByteReceived;
  
  int numOfBytesPerRecv;
  int numOfBytesToRecv;
  
  pthread_mutex_t outgoingSeqNum_Mutex;
  
  int startIndex;
  char isUnCompletedMsg;
  char _padding[3];
} t_NASDAQ_RASH_Config; 

typedef struct t_NASDAQ_RASH_NewOrder
{
  char *orderToken;
  char *stock;
  char *firm;
  double price;
  double pegDef;
  
  int share;
  int maxFloor;
  
  int timeInForce;
  char side;
  char display;
  char buyLaunch;
  char tagForSS;
  
  int tradingAccount;
  
  char ecnCrossTrade;
  char _padding[3];
} t_NASDAQ_RASH_NewOrder;

typedef struct t_NASDAQ_RASH_CancelOrder
{
  char *orderToken;
  int share;
  int tradingAccount;
} t_NASDAQ_RASH_CancelOrder;

/****************************************************************************
** Global variable declarations
****************************************************************************/

extern t_NASDAQ_RASH_Config NASDAQ_RASH_Config;

/****************************************************************************
** Function declarations
****************************************************************************/
void Initialize_NASDAQ_RASH(void);

void *StartUp_NASDAQ_RASH_Module(void *threadArgs);
void TalkTo_NASDAQ_RASH_Server(void);
void Add_NASDAQ_RASH_DataBlockToCollection(t_DataBlock *dataBlock, int clOrdId);

void Initialize_NASDAQ_RASH(void);
int Send_NASDAQ_RASH_Msg(const char *message, int msgLen);
int Receive_NASDAQ_RASH_Msg(void);
int Get_NASDAQ_RASH_CompletedMessage(void);
int Receive_NASDAQ_RASH_CompleteMessage(int msgLen);

// Session messages
int BuildAndSend_NASDAQ_RASH_LogonRequest(void);
int BuildAndSend_NASDAQ_RASH_Heartbeat(void);

// Application messages
int BuildAndSend_NASDAQ_RASH_OrderMsg(t_NASDAQ_RASH_NewOrder *newOrder);
int BuildAndSend_NASDAQ_RASH_CancelMsg(t_NASDAQ_RASH_CancelOrder *cancelOrder);

int SaveNASDAQ_RASH_SeqNumToFile(void);

#endif
