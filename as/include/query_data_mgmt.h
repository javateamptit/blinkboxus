/*****************************************************************************
**  Project:    Equity Arbitrage Application
**  Filename:   query_data_mgmt.h
**  Description:  This file contains some declaration for query_data_mgmt.c 
**  Author:     Tho Huynh-Ngoc
**  First created:  18-May-2011
**  Last updated: -----------
*****************************************************************************/

#ifndef __QUERY_DATA_MGMT_H__
#define __QUERY_DATA_MGMT_H__


/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include "global_definition.h"
#include "utility.h"
#include "order_mgmt_proc.h"
#include "arca_direct_data.h"
#include "nasdaq_ouch_data.h"
#include "nasdaq_rash_data.h"
#include "nyse_ccg_data.h"
#include "bats_boe_data.h"
#include "edge_order_data.h"
#include "output_log_mgmt.h"

#define FILE_NAME_OF_QUERY_PROCESSED "count_query_processed.txt"
#define FILE_NAME_OF_QUERIES_COLLECTION "queries_collection.txt"

/****************************************************************************
** Data structures definitions
****************************************************************************/
// The structure is used to store queries to insert/update database 
typedef struct t_New_Order_DB
{
  double price;
  double avgPrice;
  int shares;
  int leftShares; 
  int seqNum; 
  int orderID;
  int tsId;
  int crossId;
  int tradeType;
  int tradingAccount;
  int visibleShares;
  
  unsigned short entryExitRefNum;
  short bboId;
  
  char date[11]; 
  char msgContent[MAX_ORDER_MSG];
  char timestamp[TIME_LENGTH + 1];
  char ecn[6];
  char symbol[SYMBOL_LEN];
  char timeInForce[4]; 
  char status[16];
  char side[3]; 
  char entryExit[6];
  char ecnLaunch[6];
  char askBid[4];
  char isoFlag;
  char routingInst[4];
} t_New_Order_DB;

typedef struct t_Order_ACK_DB
{
  double ackPrice;

  int orderID;
  int seqNum;
  int ackShares;
  
  char date[11];    //YYYY/mm/DD
  char status[16];
  char timestamp[TIME_LENGTH + 1];
  char processedTimestamp[TIME_LENGTH + 1];
  char msgContent[MAX_ORDER_MSG];
  char exOrderId[24];
} t_Order_ACK_DB;

typedef struct t_Cancel_Request_DB
{
  int orderID;
  int seqNum;
  
  char date[11];    //YYYY/mm/DD
  char timestamp[TIME_LENGTH + 1];
  char msgContent[MAX_ORDER_MSG];
  char status[16];
  char exOrderId[20];
} t_Cancel_Request_DB;

typedef struct t_Order_Rejected_DB
{
  int orderID;
  int seqNum;

  unsigned short entryExitRefNum;
  
  char date[11];    //YYYY/mm/DD
  char timestamp[TIME_LENGTH + 1];
  char processedTimestamp[TIME_LENGTH + 1];
  char msgContent[MAX_ORDER_MSG];
  char status[16];
  char exOrderId[20];  
  char reasonText[128];
  char reasonCode[12];
} t_Order_Rejected_DB;

typedef struct t_Order_Canceled_DB
{
  int orderID;
  int seqNum;
  unsigned short entryExitRefNum;
  
  char date[11];    //YYYY/mm/DD
  char timestamp[TIME_LENGTH + 1];
  char processedTimestamp[TIME_LENGTH + 1];
  char msgContent[MAX_ORDER_MSG];
  char status[16];  
  char exOrderId[20];
} t_Order_Canceled_DB;

typedef struct t_Cancel_Rejected_DB
{
  int orderID;
  int seqNum;

  unsigned short entryExitRefNum;
  
  char date[11];    //YYYY/mm/DD
  char timestamp[TIME_LENGTH + 1];
  char msgContent[MAX_ORDER_MSG];
  char status[16];  
  char reasonText[128];
  char reasonCode[12];  
  char exOrderId[20];
} t_Cancel_Rejected_DB;

typedef struct t_Cancel_ACK_DB
{
  int orderID;
  int seqNum;
  
  char date[11];    //YYYY/mm/DD
  char timestamp[TIME_LENGTH + 1];
  char msgContent[MAX_ORDER_MSG];
  char status[16];
  char exOrderId[20];
} t_Cancel_ACK_DB;

typedef struct t_Cancel_Pending_DB
{
  double price;
  int orderID;
  int seqNum;
  int shares;
  
  char date[11];    //YYYY/mm/DD
  char timestamp[TIME_LENGTH + 1];
  char msgContent[MAX_ORDER_MSG];
  char exOrderId[20];
  char status[16];
} t_Cancel_Pending_DB;

typedef struct t_Broken_Trade_DB
{
  double price;
  
  int orderID;
  int seqNum;
  int shares;
  
  char date[11];    //YYYY/mm/DD
  char timestamp[TIME_LENGTH + 1];
  char msgContent[MAX_ORDER_MSG];
  char status[16];
  char execId[20];
} t_Broken_Trade_DB;

typedef struct t_Order_Executed_DB
{
  double lastPrice;
  double avgPrice;
  double fillFee;

  int seqNum;
  int orderID;
  int lastShares;
  int leftShares;
  unsigned short entryExitRefNum;
  
  char date[11];    //YYYY/mm/DD
  char timestamp[TIME_LENGTH + 1];
  char processedTimestamp[TIME_LENGTH + 1];
  char msgContent[MAX_ORDER_MSG];
  
  char exOrderId[20];
  char execId[32];
  char status[16];  
  char liquidity[3];  
} t_Order_Executed_DB;

typedef struct t_Short_Sell_Failed
{
  int tsCrossId;
  int tsIndex;
  int tradeType;
  char symbol[9];
  char date[11];
  char content[MAX_OUTPUT_LOG_TEXT_LEN];
} t_Short_Sell_Failed;

typedef struct t_Output_Log_Trade
{
  int tradeId;
  int tsId;
  int crossId;
  char date[11];
} t_Output_Log_Trade;

typedef struct t_Output_Log_Detected_Trade
{
  int asCrossId;
  char symbol[9];
  int tsCrossId;
  int tsIndex;
  int tradeIndex;
  int tradeType;
  int timestamp;
  char date[11];
  char content[MAX_OUTPUT_LOG_TEXT_LEN];
} t_Output_Log_Detected_Trade;

typedef struct t_Output_Log_Detected
{
  int asCrossId;
  int tsCrossId;
  int tsIndex;
  int tradeIndex;
  int tradeType;
  int timestamp;
  char symbol[9];
  char date[11];
  char content[MAX_OUTPUT_LOG_TEXT_LEN];
} t_Output_Log_Detected;

typedef struct t_Output_Log_Not_Yet_Detect_Trade
{
  int tsCrossId;
  int tsIndex;
  int tradeIndex;
  int timestamp;
  char date[11];
  char content[MAX_OUTPUT_LOG_TEXT_LEN];
} t_Output_Log_Not_Yet_Detect_Trade;

typedef struct t_Output_Log_Not_Yet_Detect
{
  int tsCrossId;
  int tsIndex;
  int timestamp;
  char date[11];
  char content[MAX_OUTPUT_LOG_TEXT_LEN];
} t_Output_Log_Not_Yet_Detect;

typedef struct t_Output_Log_Ask_Bid
{
  double price;
  int tsCrossId;
  int tsIndex;
  int shares;
  int timestamp;
  int askBid;
  int ecnLaunch;
  char date[11];
} t_Output_Log_Ask_Bid;

typedef struct t_Output_Log
{
  int asCrossId;
  int tsCrossId;
  int tsIndex;
  int tradeType;
  int ecnLaunch;
  char symbol[9];
  char date[11];
} t_Output_Log;

typedef struct t_Real_Value
{
  double profitLoss;
  double totalFee;
  int crossId;
  int tsId;
  int leftShares;
  char date[11];
} t_Real_Value;

typedef union t_QueryDataBlockDetail
{
  t_New_Order_DB                       newOrder;
  t_Order_ACK_DB                       orderAccepted;
  t_Order_Rejected_DB                  orderRejected;
  t_Cancel_Request_DB                  cancelRequest;
  t_Order_Canceled_DB                  orderCanceled;
  t_Order_Executed_DB                  orderExecuted;
  t_Cancel_Rejected_DB                 cancelRejected;
  t_Cancel_ACK_DB                      cancelACK;
  t_Cancel_Pending_DB                  cancelPending;
  t_Broken_Trade_DB                    brokenTrade;

  t_dbBBOInfo                          bboInfo;
  t_Short_Sell_Failed                  shortSellFailed;
  t_TradeResultInfo                    tradeResult;
  t_Output_Log_Trade                   outputLogTrade;
  t_Output_Log_Detected_Trade          outputLogDetectedTrade;
  t_Output_Log_Detected                outputLogDetected;
  t_Output_Log_Not_Yet_Detect_Trade    outputLogNotYetDetectTrade;
  t_Output_Log_Not_Yet_Detect          outputLogNotYetDetect;
  t_Output_Log_Ask_Bid                 outputLogAskBid;
  t_Output_Log                         outputLog;
  t_Real_Value                         realValue;
} t_QueryDataBlockDetail;

typedef struct t_QueryDataBlock
{
  t_QueryDataBlockDetail detail;
  int queryType;
} t_QueryDataBlock;

// This structure is used to management query data
typedef struct t_QueryDataMgmt
{
  // File pointer contains file descriptor to query data file
  FILE *fileQueriesCollectionDesc;  //8 bytes

  // File pointer contains file descriptor to query database file processed
  FILE *fileCountQueryProcessedDesc;
  
  // All query data blocks will be stored at the collection
  t_QueryDataBlock queryDataBlockCollection[MAX_QUERY_DATA_ENTRIES];  //aligned by 8bytes
  
  // File name is used stored query data
  char fileNameQueriesCollection[MAX_LINE_LEN]; //Total 80 bytes
  
  // Mutex lock
  pthread_mutex_t updateMutex;          //40 bytes
  
  // Last update index
  int lastUpdatedIndex;

  // Count query data block
  int countQueryDataBlock;

  // Check query data processed
  int countProccessed;

  // Position query database blocks has been processed
  int positionProcessed;
} t_QueryDataMgmt;
/****************************************************************************
** Global variables definition
****************************************************************************/
extern t_QueryDataMgmt queryDataMgmt;

/****************************************************************************
** Function declarations
****************************************************************************/
int InitializeQueryDataManegementStructures(void);
int SavePositionQueryProcessedToFile(void);
int AddQueryDataBlockToCollection(const void *queryDataBlock, void *queryDataMgmt);
int SaveQueryDataToFile(void *queryDataMgmt);
int GetNumberOfQueryEntriesFromFile(const char *fileName, void *queryDataMgmt);
int LoadPositionQueryProcessedFromFile(const char *fileName);
int ProcessHistoryOfQueryData( void );
int ProcessQueryDataBlockToDatabase(t_QueryDataBlock *queryDataBlock);
int ProcessQueryDatabaseFromFile( int *countQueryProcessed, void *queryDataMgmt );
int ProcessQueryDatabaseFromCollection(int *countQueryProcessed, void *queryDataMgmt);

int ProcessInsertNewOrder4ARCA_DIRECTQueryToCollection(t_ARCA_DIRECTNewOrder *newOrder);
int ProcessInsertOrderACK4ARCA_DIRECTQueryToCollection(t_ARCA_DIRECTOrderACK *orderACK);
int ProcessInsertExecutedOrder4ARCA_DIRECTQueryToCollection(t_ARCA_DIRECTOrderFill *orderFill);
int ProcessInsertCanceledOrder4ARCA_DIRECTQueryToCollection(t_ARCA_DIRECTOrderCancel *orderCancel);
int ProcessInsertCancelRequest4ARCA_DIRECTQueryToCollection(t_ARCA_DIRECTCancelRequest *cancelRequest);
int ProcessInsertCancelACK4ARCA_DIRECTQueryToCollection(t_ARCA_DIRECTCancelACK *cancelACK);
int ProcessInsertBustOrCorrect4ARCA_DIRECTQueryToCollection(t_ARCA_DIRECTBustOrCorrect *brokenTrade);
int ProcessInsertReject4ARCA_DIRECTQueryToCollection(t_ARCA_DIRECTReject *reject);

int ProcessInsertNewOrder4OUCHQueryToCollection(t_OUCHNewOrder *newOrder);
int ProcessInsertAcceptedOrder4OUCHQueryToCollection(t_OUCHAcceptedOrder *acceptedOrder);
int ProcessInsertExecutedOrder4OUCHQueryToCollection(t_OUCHExecutedOrder *executedOrder);
int ProcessInsertCanceledOrder4OUCHQueryToCollection(t_OUCHCanceledOrder *canceledOrder);
int ProcessInsertCancelRequest4OUCHQueryToCollection(t_OUCHCancelRequest *cancelRequest);
int ProcessInsertRejectedOrder4OUCHQueryToCollection(t_OUCHRejectedOrder *rejectedOrder);
int ProcessInsertBrokenTrade4OUCHQueryToCollection(t_OUCHBrokenOrder *brokenTrade);
int ProcessInsertCancelPending4OUCHQueryToCollection(t_OUCHCancelPending *pendingCancel);
int ProcessInsertCancelReject4OUCHQueryToCollection(t_OUCHCancelReject *rejectedCancel);

int ProcessInsertNewOrder4RASHQueryToCollection(t_RASHNewOrder *newOrder);
int ProcessInsertAcceptedOrder4RASHQueryToCollection(t_RASHAcceptedOrder *acceptedOrder);
int ProcessInsertExecutedOrder4RASHQueryToCollection(t_RASHExecutedOrder *executedOrder);
int ProcessInsertCanceledOrder4RASHQueryToCollection(t_RASHCanceledOrder *canceledOrder);
int ProcessInsertCancelRequest4RASHQueryToCollection(t_RASHCancelRequest *cancelRequest);
int ProcessInsertRejectedOrder4RASHQueryToCollection(t_RASHRejectedOrder *rejectedOrder);
int ProcessInsertBrokenTrade4RASHQueryToCollection(t_RASHBrokenOrder *brokenTrade);

int ProcessInsertNewOrder4NYSE_CCGQueryToCollection(t_NYSE_CCGNewOrder *newOrder);
int ProcessInsertACKOrder4NYSE_CCGQueryToCollection(t_NYSE_CCGACKOrder *orderACK);
int ProcessInsertCanceledOrder4NYSE_CCGQueryToCollection(t_NYSE_CCGCanceledOrder *canceledOrder);
int ProcessInsertRejectOrder4NYSE_CCGQueryToCollection(t_NYSE_CCGRejectedOrder *reject);
int ProcessInsertFillOrder4NYSE_CCGQueryToCollection(t_NYSE_CCGFillOrder *fillOrder);
int ProcessInsertCancelRequest4NYSE_CCGQueryToCollection(t_NYSE_CCGCancelRequest *cancelRequest);
int ProcessInsertCancelACK4NYSE_CCGQueryToCollection(t_NYSE_CCGCancelACK *cancelACK);
int ProcessInsertBustOrCorrect4NYSE_CCGQueryToCollection(t_NYSE_CCGBustOrCorrect *brokenTrade);

int ProcessInsertNewOrder4BATS_BOEQueryToCollection(t_BATS_BOENewOrder *newOrder);
int ProcessInsertAcceptedOrder4BATS_BOEQueryToCollection(t_BATS_BOEAcceptedOrder *acceptedOrder);
int ProcessInsertFillOrder4BATS_BOEQueryToCollection(t_BATS_BOEExecutedOrder *fillOrder);
int ProcessInsertCanceledOrder4BATS_BOEQueryToCollection(t_BATS_BOECanceledOrder *canceledOrder);
int ProcessInsertCancelRequest4BATS_BOEQueryToCollection(t_BATS_BOECancelRequest *cancelRequest);
int ProcessInsertRejectOrder4BATS_BOEQueryToCollection(t_BATS_BOERejectedOrder *rejectedOrder);
int ProcessInsertRejectCancel4BATS_BOEQueryToCollection(t_BATS_BOERejectedCancel *rejectedCancel);
int ProcessInsertBustOrCorrect4BATS_BOEQueryToCollection(t_BATS_BOEBrokenOrder *brokenTrade);

int ProcessInsertNewOrder4EDGEQueryToCollection(t_EDGENewOrder *newOrder);
int ProcessInsertAcceptedOrder4EDGEQueryToCollection(t_EDGEAcceptedOrder *acceptedOrder);
int ProcessInsertExecutedOrder4EDGEQueryToCollection(t_EDGEExecutedOrder *executedOrder);
int ProcessInsertCanceledOrder4EDGEQueryToCollection(t_EDGECanceledOrder *canceledOrder);
int ProcessInsertRejectedOrder4EDGEQueryToCollection(t_EDGERejectedOrder *rejectedOrder);
int ProcessInsertBrokenTrade4EDGEQueryToCollection(t_EDGEBrokenOrder *brokenTrade);

int ProcessInsertBBOInfoToCollection(t_dbBBOInfo *dbBBOInfo);
int ProcessInsertTradeOutputLogToCollection(int asCrossId, const char *symbol, int tsCrossId, int tsIndex, const char *dateString, int tradeType);
int ProcessInsertAskBidToCollection(int tsCrossId, int tsIndex, int shares, double price, int askBid, int ecnLaunch, const char *dateString);
int ProcessInsertTradeOutputLogNotYetDetectToCollection(int tsCrossId, int tsIndex, const char *dateString, const char *content);
int ProcessInsertTradeOutputLogNotYetDetect_TradeToCollection(int tsCrossId, int tsIndex, const char *dateString, const char *content, int tradeIndex);
int ProcessInsertTradeOutputLogDetectedToCollection(int asCrossId, const char *symbol, int tsCrossId, int tsIndex, const char *dateString, const char *content, int tradeType);
int ProcessInsertTradeOutputLogDetected_TradeToCollection(int asCrossId, const char *symbol, int tsCrossId, int tsIndex, const char *dateString, const char *content, int tradeIndex, int tradeType);
int ProcessInsertTradeResultToCollection(t_TradeResultInfo tradeResult);
int ProcessInsertTradeOutputLog_TradeToCollection(int tradeId, int tsCrossId, int tsIndex, const char *dateString);
int ProcessInsertShortSellFailedToCollection(const char *symbol, int tsCrossId, int tsIndex, const char *dateString, const char *content, int tradeType);
int ProcessUpdateRealValueToCollection(int tsId, int crossId, char *dateString, int leftShares, double realValue, double totalFee);

/***************************************************************************/

#endif
