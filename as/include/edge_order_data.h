/*****************************************************************************
**  Project:    Equity Arbitrage Application
**  Filename:   edge_order_data.h
**  Description:  This file contains some declaration for edge_order_data.c 
**  Author:     Dung Tran-Dinh
**  First created:  -----------
**  Last updated: -----------
*****************************************************************************/

#ifndef __EDGA_ORDER_DATA_H__
#define __EDGA_ORDER_DATA_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include <stdlib.h>
#include <math.h>

#include "global_definition.h"
#include "utility.h"

/****************************************************************************
** Data structures definitions
****************************************************************************/
// The structure is used contain several fields of New Order
typedef struct t_EDGENewOrder
{
  double price;
  
  int leftShares;
  int minQty;
  int maxFloor;
  int tradingAccount;
  int orderID;
  int seqNum;
  int tsId;
  int crossId;
  int tradeType;
  int shares;
  int visibleShares;

  short bboId;
  unsigned short entryExitRefNum;

  char date[12];
  char timestamp[16];
  char msgType[16];
  char side[3];
  char entryExit[6];
  char askBid[4];
  char ecnLaunch[6];
  char ecn[6];
  char symbol[SYMBOL_LEN];
  char timeInForce[4];
  char firm[6];
  char display;
  char eligibility;
  char capacity;
  char routingInfo[5];
  char status[16];

  char msgContent[MAX_ORDER_MSG];
} t_EDGENewOrder;

// The structure is used contain several fields of Accepted Order
typedef struct t_EDGEAcceptedOrder
{
  double price;

  unsigned long transTime;
  long exOrderId;

  int tradingAccount;
  int orderID;
  int tsId;
  int seqNum;
  int shares;

  char date[12];
  char timestamp[16];
  char processedTimestamp[16];
  char msgType[16];
  char ecn[6];
  char side[3];
  char symbol[SYMBOL_LEN];
  char timeInForce[4];
  char capacity;
  char display;
  char status[16];
  
  char msgContent[MAX_ORDER_MSG];
} t_EDGEAcceptedOrder;

// The structure is used contain several fields of Executed Order
typedef struct t_EDGEExecutedOrder
{
  double price;
  double avgPrice;
  double fillFee;

  unsigned long transTime;
  long executionId;

  int orderID;
  int seqNum;
  int shares;
  int leftShares;

  unsigned short entryExitRefNum;
  
  char date[12];
  char timestamp[16];
  char processedTimestamp[16];
  char msgType[16];
  char liquidityIndicator[6];
  char status[16];

  char msgContent[MAX_ORDER_MSG];
} t_EDGEExecutedOrder;

// The structure is used contain several fields of Canceled Order
typedef struct t_EDGECanceledOrder
{
  long transTime;

  int orderID;
  int seqNum;
  int shares;

  unsigned short entryExitRefNum;

  char date[12];
  char timestamp[16];
  char processedTimestamp[16];
  char msgType[16];
  char reason;
  char status[16];

  char msgContent[MAX_ORDER_MSG];
} t_EDGECanceledOrder;

// The structure is used contain several fields of Rejected Order
typedef struct t_EDGERejectedOrder
{
  unsigned long transTime;

  int orderID;
  int seqNum;

  unsigned short entryExitRefNum;

  char date[12];
  char timestamp[16];
  char processedTimestamp[16];
  char msgType[16];
  char reason;
  char status[16];
  
  char msgContent[MAX_ORDER_MSG];
} t_EDGERejectedOrder;
// The structure is used contain several fields of Broken Order
typedef struct t_EDGEBrokenOrder
{
  unsigned long transTime;
  long executionId;

  int orderID;
  int seqNum;

  char date[12];
  char timestamp[16];
  char msgType[16];
  char reason;
  char status[16];

  char msgContent[MAX_ORDER_MSG];
} t_EDGEBrokenOrder;

/****************************************************************************
** Global variables definition
****************************************************************************/

/****************************************************************************
** Function declarations
****************************************************************************/

int ParseRawDataForEDGEOrder(t_DataBlock dataBlock, int type, int oecType);

int ProcessNewOrderOfEDGEOrder(t_DataBlock dataBlock, int type, char *timeStamp, int oecType);

int ProcessNewExtOrderOfEDGEOrder(t_DataBlock dataBlock, int type, char *timeStamp, int oecType);

int ProcessAcceptedOrderOfEDGEOrder(t_DataBlock dataBlock, int type, char *timeStamp, int oecType);

int ProcessAcceptedExtOrderOfEDGEOrder(t_DataBlock dataBlock, int type, char *timeStamp, int oecType);

int ProcessExecutedOrderOfEDGEOrder(t_DataBlock dataBlock, int type, char *timeStamp, int oecType);

int ProcessCanceledOrderOfEDGEOrder(t_DataBlock dataBlock, int type, char *timeStamp, int oecType);

int ProcessAcceptedCancelOfEDGEOrder(t_DataBlock dataBlock, int type, int oecType);

int ProcessRejectedOrderOfEDGEOrder(t_DataBlock dataBlock, int type, char *timeStamp, int oecType);

int ProcessBrokenOrderOfEDGEOrder(t_DataBlock dataBlock, int type, int oecType);

int ProcessPriceCorrectionOfEDGEOrder(t_DataBlock dataBlock, int type, int oecType);

/***************************************************************************/

#endif
