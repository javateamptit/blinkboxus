#ifndef __ORDER_FILE_CREATION_H__
#define __ORDER_FILE_CREATION_H__

#include "global_definition.h"

#define VENUE_ARCA        TYPE_ARCA_DIRECT - TYPE_BASE
#define VENUE_OUCH        TYPE_NASDAQ_OUCH - TYPE_BASE
#define VENUE_RASH        TYPE_NASDAQ_RASH - TYPE_BASE
#define VENUE_CCG         TYPE_NYSE_CCG - TYPE_BASE
#define VENUE_BZX         TYPE_BZX_BOE - TYPE_BASE
#define VENUE_EDGX        TYPE_EDGX_DIRECT - TYPE_BASE
#define VENUE_EDGA        TYPE_EDGA_DIRECT - TYPE_BASE
#define VENUE_OUBX        TYPE_NASDAQ_OUBX - TYPE_BASE
#define VENUE_BYX         TYPE_BYX_BOE - TYPE_BASE
#define VENUE_PSX         TYPE_PSX - TYPE_BASE

#define BBO_RECORD_TYPE_MAIN_HEADER   1
#define BBO_RECORD_TYPE_QUOTES_OPEN   2
#define BBO_RECORD_TYPE_QUOTES        3
#define BBO_RECORD_TYPE_QUOTES_CLOSE  4
#define BBO_RECORD_TYPE_ORDER_CLOSE   5
#define BBO_RECORD_TYPE_ORDER         6
#define BBO_RECORD_TYPE_ORDER_OPEN    7
#define BBO_RECORD_TYPE_RECORD_CLOSE  8

#define MAX_ISO_OPEN_CROSS        10000
#define MAX_OPEN_POSITIONS          500
#define TOTAL_FEES              0.00305

#define MAX_LINE_LEN_OF_OJ        10240

//For Log Measurement
#define ENTRY_ORDER           1
#define EXIT_ORDER            2
#define ENTRY_ACK             3
#define EXIT_ACK              4
#define ENTRY_FILL            5
#define EXIT_FILL             6
#define ENTRY_CANCEL          7
#define EXIT_CANCEL           8
#define ENTRY_ACK_INTERNAL    9
#define EXIT_ACK_INTERNAL    10
#define ENTRY_FILL_INTERNAL  11
#define EXIT_FILL_INTERNAL   12
#define REENTRY_ORDER        13

#define OJ_BB_REASON_ENTRY    0
#define OJ_BB_REASON_EXIT     1
#define OJ_BB_REASON_PM       2
#define OJ_BB_REASON_AS       3
#define OJ_BB_REASON_UNKNOWN  4
/****************************************************************************
** Data structure definitions
****************************************************************************/

//Struct store information for ISO file online creation
typedef struct t_ISOOrderInfo
{
  char strTime[19];
  char strVenueID[3];
  char strBuysell[2];
  char strPrice[10];
  char strShares[8];
  char strSequenceID[8];
  char strCapacity[2];
  char strAccount[6];
  char strRoutingInfo[6];
} t_ISOOrderInfo;

typedef struct t_ISOOpenCrossInfo
{
  //For checking if we receive enough info to create a ISO Flight record
  int crossID;
  short bboID;
  
  char totalOrder;
  char isComplete;
  char waitingCount;
  
  //Information needs to create ISO Record
  char bbo[512];
  char symbol[SYMBOL_LEN];
  
  t_ISOOrderInfo orders[20];
  
}t_ISOOpenCrossInfo;

typedef struct t_ISOOpenCrossMgmt
{
  t_ISOOpenCrossInfo crosses[MAX_ISO_OPEN_CROSS];
  int bound;
} t_ISOOpenCrossMgmt;

typedef struct t_OpenPosition
{
  char symbol[SYMBOL_LEN];
  double price;
  int shares;
  int tradingAccount;
  int changedShares;
  char side;
  char date[11];
  char timeStamp[16];
  char isManual;
} t_OpenPosition;

typedef struct t_OpenPositionList
{
  t_OpenPosition positions[MAX_OPEN_POSITIONS];
  int count;
} t_OpenPositionList;

typedef struct t_OJBrokenInfo
{
  long execId;
  int clOrdId;
} t_OJBrokenInfo;

typedef struct t_OJBrokenCollection
{
  t_OJBrokenInfo collection[MAX_BROKEN_TRADE];
  int countBrokenMsg;
} t_OJBrokenCollection;


typedef struct t_LinkedListExecData
{
  int orderId;
  long execId;
} t_LinkedListExecData;

typedef struct t_LinkedlistExecution
{
  t_LinkedListExecData data;
  
  struct t_LinkedlistExecution *next;
} t_LinkedlistExecution;

typedef struct t_LinkedListOrderIdData
{
  int orderId;
} t_LinkedListOrderIdData;

typedef struct t_LinkedlistOrderId
{
  t_LinkedListOrderIdData data;
  
  struct t_LinkedlistOrderId *next;
} t_LinkedlistOrderId;


extern t_ISOOpenCrossMgmt ISOOpenCrossMgmt[MAX_TRADE_SERVER_CONNECTIONS];
extern t_OJBrokenCollection OJBrokenMsgList;
extern int hasChangedExposure;
extern t_LinkedlistExecution *__exec;
extern t_LinkedlistOrderId *__new;
extern t_LinkedlistOrderId *__ack;
extern t_LinkedlistOrderId *__cancel;
extern t_LinkedlistOrderId *__reject;

/****************************************************************************
** Function prototypes
****************************************************************************/
void InitVenueMappings();
int InitOrderFilesCreation();
int InitISOFilesCreation();
int LoadBreakMsgInOJ(void);
int AddOJNewOrderRecord(char *time, char *symbol, int venueID, char buysell, double price, int size, int orderID, char *tif, char visibility, int isISO, char *account, char *routingInfo, char capacity, int tsCrossId, unsigned char bbReason);
int AddOJOrderAcceptedRecord(char *time, char *symbol, char *exOrderId, int orderID, char *ecnOrderID, int venueID, char *account, char capacity);
int AddOJOrderCancelRecord(char *time, int orderID, char *symbol, int venueID, int size);
int AddOJOrderCancelResponseRecord(char *time, char *symbol, int orderID, char *ecnOrderID, char *reason, int venueID, int sizeCanceled, int orderComplete, char *account);
int AddOJOrderFillRecord(char *time, char *symbol, char *exOrderId, int venueID, char buysell, double price, int size, int orderID, char *liquidity, char *ecnOrderID, char *execID, char *reasonCode, int orderComplete, char *account, char execVenue);
int AddOJOrderTradeBreakRecord(char *time, char *symbol, int venueID, char buysell, double price, int size, int orderID, char *liquidity, char *ecnOrderID, char *execID, char *reasonCode, int orderComplete, char *account);
int AddOJOrderRejectRecord(char *time, int orderID, char *ecnOrderID, char *reason, char *symbol, int venueID, char *reasonCode, char *account);
int CreateOJFilesWithDateRange(int beginDateInSecs, int endDateInSecs);
int CreateISOFlightFilesWithDateRange(int beginDateInSecs, int endDateInSecs);
int ReadAndInsertOJRecordFromRawdataFile(char *fName, int orderIndex, int serverType);
int ReadAndInsertOJRecordHungOrderFile();
void AddRecordToISOFile(int type, char *text1, char *text2, char *text3, char *text4, char *text5, char *text6, char *text7, char *text8, char *text9, char *text10);
int AddISOOrderToCollection(int crossID, short bboID, char *orgSymbol, t_ISOOrderInfo *order);
int AddISOFlightRecordForCrossIndex (int tsIndex, int crossIndex);
int CreateOJRecordForOpenPosition(void);
int AddOJOpenPositionRecord(t_OpenPosition *position);
void InsertISOToggleToISOFlightFile(int tsId, int status, char *user);
void GetEpochTimeFromDateString(char *strDate, char *res);
int GetOECInstance(char *record, int orderId, int venueID);

// For time series format EZG
void *ExportTimeSeriesFormat(void *pArgs);
int InitTimeSeriesFormatEZG();
int AddTimeSeriesNetProfitRecord(char *time, double netProfitLoss);
int AddTimeSeriesTotalVolumetRecord(char *time, int totalVolume);
int AddTimeSeriesTotalMarketRecords();
int AddTimeSeriesTotalMarketRecord(char *time, double totalMarket, int accountIndex);
//For log measurement
int AddTimeSeriesDelayTimeRecord(char *time, int delayTime, int type, char *ecn, char *server, char *eventTime, char *routingInst);
int AddExecDataToList(t_LinkedlistExecution **list, const int orderId, const long execId);
int AddOrderIdToList(t_LinkedlistOrderId **list, const int orderId);
void DestroyExecLinkedlist(t_LinkedlistExecution **list);
void DestroyOrderLinkedlist(t_LinkedlistOrderId **list);
unsigned char GetBBReason(t_DataBlock *dataBlock, int orderId);
#endif
