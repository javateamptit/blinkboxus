/*****************************************************************************
**  Project:    Equity Arbitrage Application
**  Filename:   raw_data_mgmt.h
**  Description:  This file contains some declaration for raw_data_mgmt.c 
**  Author:     Luan Vo-Kinh
**  First created:  13-Sep-2007
**  Last updated: -----------
*****************************************************************************/

#ifndef __RAW_DATA_MGMT_H__
#define __RAW_DATA_MGMT_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include "global_definition.h"
#include "utility.h"

#define FILE_NAME_OF_RAW_DATA_PROCESSED "count_rawdata_processed.txt"

/*
  Wait List, which store rawdata blocks which comes to AS in invalid sequence.
  For example, ACK comes before NEW order. So ACK will be put into Wait List.
  Then, when NEW order comes, AS processes it, then checks Wait List to process ACK
*/

//File name for the Wait-List
#define RAW_DATA_WAIT_LIST_FILE "rawdata_wait_list.dat"

//Storage size
#define MAX_WAIT_LIST_ORDERS 30
#define MAX_WAIT_LIST_RAWDATA_BLOCKS_PER_ORDER 20

//data block type
enum __Waiting__List__Msg__Type
{
  //Special type, indicates that the raw data block has been processed
  WAIT_LIST_TYPE_ITEM_PROCESSED = 0,
  
  WAIT_LIST_TYPE_ARCA_DIRECT_ORDER_ACK,
  WAIT_LIST_TYPE_ARCA_DIRECT_ORDER_FILLED,
  WAIT_LIST_TYPE_ARCA_DIRECT_ORDER_CANCELED,
  WAIT_LIST_TYPE_ARCA_DIRECT_CANCEL_REQUEST,
  WAIT_LIST_TYPE_ARCA_DIRECT_CANCEL_ACK,
  WAIT_LIST_TYPE_ARCA_DIRECT_ORDER_REJECTED,
  WAIT_LIST_TYPE_ARCA_DIRECT_BUST_OR_CORRECT,
  
  WAIT_LIST_TYPE_OUCH_ORDER_ACK,
  WAIT_LIST_TYPE_OUCH_ORDER_FILLED,
  WAIT_LIST_TYPE_OUCH_ORDER_CANCELED,
  WAIT_LIST_TYPE_OUCH_CANCEL_REQUEST,
  WAIT_LIST_TYPE_OUCH_ORDER_REJECTED,
  WAIT_LIST_TYPE_OUCH_ORDER_BROKEN,
  WAIT_LIST_TYPE_OUCH_PRICE_CORRECTION,
  WAIT_LIST_TYPE_OUCH_CANCEL_PENDING,
  WAIT_LIST_TYPE_OUCH_CANCEL_REJECTED,
  
  WAIT_LIST_TYPE_RASH_ORDER_ACK,
  WAIT_LIST_TYPE_RASH_ORDER_FILLED,
  WAIT_LIST_TYPE_RASH_ORDER_CANCELED,
  WAIT_LIST_TYPE_RASH_CANCEL_REQUEST,
  WAIT_LIST_TYPE_RASH_ORDER_REJECTED,
  WAIT_LIST_TYPE_RASH_ORDER_BROKEN,
  
  WAIT_LIST_TYPE_CCG_ORDER_ACK,
  WAIT_LIST_TYPE_CCG_ORDER_FILLED,
  WAIT_LIST_TYPE_CCG_ORDER_CANCELED,
  WAIT_LIST_TYPE_CCG_REJECTED,
  WAIT_LIST_TYPE_CCG_CANCEL_REQUEST,
  WAIT_LIST_TYPE_CCG_CANCEL_ACK,
  WAIT_LIST_TYPE_CCG_BUST_ORDER_CORRECT,
  
  WAIT_LIST_TYPE_EDGE_ORDER_ACK,
  WAIT_LIST_TYPE_EDGE_ORDER_FILLED,
  WAIT_LIST_TYPE_EDGE_ORDER_CANCELED,
  WAIT_LIST_TYPE_EDGE_CANCEL_REQUEST,
  WAIT_LIST_TYPE_EDGE_ORDER_REJECTED,
  WAIT_LIST_TYPE_EDGE_ORDER_BROKEN,
  
  WAIT_LIST_TYPE_BATS_ORDER_ACK,
  WAIT_LIST_TYPE_BATS_ORDER_FILLED,
  WAIT_LIST_TYPE_BATS_ORDER_CANCELED,
  WAIT_LIST_TYPE_BATS_CANCEL_REQUEST,
  WAIT_LIST_TYPE_BATS_ORDER_REJECTED,
  WAIT_LIST_TYPE_BATS_CANCEL_REJECTED,
  WAIT_LIST_TYPE_BATS_BUST_OR_CORRECT
};
/****************************************************************************
** Data structures definitions
****************************************************************************/
typedef struct t_WaitListDetails
{
  char oecType;
  char itemType;
  char serverID;
  char timeStamp[TIME_LENGTH + 1];  //currently TIME_LENGTH is 15
  t_DataBlock dataBlock;
} t_WaitListDetails;

typedef struct t_RawdataWaitList
{
  int clOrdID;
  t_WaitListDetails blocks[MAX_WAIT_LIST_RAWDATA_BLOCKS_PER_ORDER];
} t_RawdataWaitList;

#define FILE_NAME_OF_HUNG_ORDER_LIST "hung_order_list.dat"
#define MAX_LIST_HUNG_ORDERS 10000
#define ORDER_EDITOR_CANCELLED 1
#define ORDER_EDITOR_FILLED 2
typedef struct t_HungOrderDetails
{
  int clOrdId;
  int shares;
  double price;
  char timestamp[MAX_TIMESTAMP];
  char execId[20];
  char symbol[SYMBOL_LEN];  
  char liquidity[4];  
  char orderType;
  char venueType;
  char side;
  char account;
  char msgContent[128];
} t_HungOrderDetails;

typedef struct t_HungOrderList
{
  int count;
  t_HungOrderDetails buffer[MAX_LIST_HUNG_ORDERS];
} t_HungOrderList;

/****************************************************************************
** Global variables definition
****************************************************************************/
extern t_RawDataMgmt tsRawDataMgmt[MAX_TRADE_SERVER_CONNECTIONS][TS_MAX_ORDER_CONNECTIONS];

extern t_RawDataMgmt asRawDataMgmt[AS_MAX_ORDER_CONNECTIONS];

extern t_RawDataMgmt pmRawDataMgmt[PM_MAX_ORDER_CONNECTIONS];

extern t_RawDataProcessedMgmt RawDataProcessedMgmt;

extern t_HungOrderList HungOrderList;
/****************************************************************************
** Function declarations
****************************************************************************/
int InitializeRawDataManegementStructures(void);

int InitializePositionRawDataProcessedStructure(void);

int GetNumberOfRawDataEntries(void);

int GetNumberOfRawDataEntriesFromFile(const char *fileName, void *orderRawDataMgmt);

int LoadPositionRawDataProcessedFromFile(const char *fileName);
int SavePositionRawDataProcessedToFile(void);

int ProcessHistoryRawDataOfTradeServer(void);

int ProcessRawDataOfTradeServerFromFile( int *countRawDataProcessed, void *rawDataMgmt );

int ProcessHistoryRawDataOfAggregationServer(void);

int ProcessRawDataOfAggregationServerFromFile(int *countRawDataProcessed, void *rawDataMgmt);

int ProcessRawDataOfTradeServer(void);

int ProcessRawDataOfTradeServerFromCollection(int *countRawDataProcessed, void *rawDataMgmt);

int ProcessRawDataOfAggregationServer(void);

int ProcessRawDataOfAggregationServerFromCollection(int *countRawDataProcessed, void *rawDataMgmt);

int ProcessHistoryRawDataOfPM( void );

int ProcessRawDataOfPMFromFile( int *countRawDataProcessed, void *rawDataMgmt );

int ProcessRawDataOfPM(void);

int ProcessRawDataOfPMFromCollection(int *countRawDataProcessed, void *rawDataMgmt);

int InitializeRawdataWaitList();
int LoadAndProcessRawdataWaitList();
int CheckAndProcessRawdataInWaitListForOrderID(int orderID);
int SaveRawdataWaitListToFile();
int AddDataBlockToWaitList(int oecType, int itemType, int clOrdID, t_DataBlock dataBlock, int serverID, char *timeStamp);

int InitializeHungOrderList();
int LoadHungOrderListFromFile();
int AddHungOrderToList(t_HungOrderDetails *hungOrder);
int SaveHungOrderListToFile(t_HungOrderDetails *hungOrder);
int ProcessHistoryHungOrder();
/***************************************************************************/

#endif
