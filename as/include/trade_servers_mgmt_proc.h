/*****************************************************************************
**  Project:    Equity Arbitrage Application
**  Filename:   trade_servers_mgmt_proc.h
**  Description:  This file contains some declaration for trade_servers_mgmt_proc.c 
**  Author:     Luan Vo-Kinh
**  First created:  13-Sep-2007
**  Last updated: -----------
*****************************************************************************/

#ifndef __TRADE_SERVERS_MGMT_PROC_H__
#define __TRADE_SERVERS_MGMT_PROC_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include <stdlib.h>

#include "global_definition.h"
#include "utility.h"

/****************************************************************************
** Data structures definitions
****************************************************************************/

/****************************************************************************
** Global variables definition
****************************************************************************/

/****************************************************************************
** Function declarations
****************************************************************************/
int AddDataBlockToCollection(const void *dataBlock, void *orderRawDataMgmt);

int SaveDataBlockToFile(void *orderRawDataMgmt);

int MappingTradeOutputLog(unsigned char srcBuff[MAX_OUTPUT_LOG_BIN_LEN], char destBuff[MAX_OUTPUT_LOG_TEXT_LEN]);

int AddOutputLogToCollection(const char *buffer, void *logMgmt);

int SaveOutputLogToFile(void *logMgmt);

int Connect2TradeServer(int tsIndex);

int Connect2AllTradeServer(int ttIndex);

int Disconnect2TradeServer(int tsIndex);

int Disconnect2AllTradeServer(void);

/***************************************************************************/

#endif
