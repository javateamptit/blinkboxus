/*****************************************************************************
**  Project:    Equity Arbitrage Application
**  Filename:   aggregation_server.h
**  Description:  This file contains some declaration for aggregation_server.c  
**  Author:     Luan Vo-Kinh
**  First created:  13-Sep-2007
**  Last updated: -----------
*****************************************************************************/

#ifndef __AGGREGATION_SERVER_H__
#define __AGGREGATION_SERVER_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include <stdio.h>
#include <signal.h>
#include <getopt.h>

/****************************************************************************
** Data structures definitions
****************************************************************************/
typedef struct t_AutoStart
{
  int isProcess;
  int value;
} t_AutoStart;

/****************************************************************************
** Global variables definition
****************************************************************************/

/****************************************************************************
** Function declarations
****************************************************************************/

int main (int argc, char *argv[]);

int ProcessUserInput(int argc, char *argv[]);

int Print_Help(const char *program_name);

int GetTimestampFromUserInput(const char *arg);

int GetOptionForTradeServer(int tsId, const char *arg);
int GetOptionForAllTradeServer(const char *arg);

int GetOptionForPM(const char *arg);

int GetOptionForECNOrder(int ecnId, const char *arg);
int GetOptionForAllECNOrders(const char *arg);

int GetOptionForAllConnections(const char *arg);

int ProcessOldDataWithDateRange(void);

int RemoveAllDataAtDate(void);

int LoadDataConfig(void);

int LoadDataStructures(void);

int LoadASOpenOrderFromDatabase(void);

int InitializeDataConfs(void);

int SaveLoginInfo(void);

int InitializeDataStructures(void);

int RegisterSignalHandlers(void);

void Process_INTERRUPT_Signal(int signalno, siginfo_t *siginfo, void *context);
void Process_SEGV_Signal(int signalno, siginfo_t *siginfo, void *context);
void Process_USER_DEFINED_Signal_1(int signalno, siginfo_t *siginfo, void *context);

void Process_Kill_Signal(int sig_num);

void Process_SIGPIPE_Signal(int signalno, siginfo_t *siginfo, void *context);

void *ProcessAutoStartThreads(void *arg);

int StartUpMainThreads(void);

/***************************************************************************/

#endif
