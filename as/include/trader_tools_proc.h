/*****************************************************************************
**  Project:    Equity Arbitrage Application
**  Filename:   trader_tools_proc.h
**  Description:  This file contains some declaration for trader_tools_proc.c 
**  Author:     Luan Vo-Kinh
**  First created:  13-Sep-2007
**  Last updated: -----------
*****************************************************************************/

#ifndef __TRADER_TOOLS_PROC_H__
#define __TRADER_TOOLS_PROC_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include <stdlib.h>
#include <string.h>

#include "global_definition.h"
#include "order_mgmt_proc.h"
#include "utility.h"

#define MSG_TYPE_FOR_SEND_ALERTING_TO_TT 15200

#define PM_DOES_NOT_ACCEPT_POSITION_ALERT_MESSAGE_TYPE      0
#define PM_HASNT_EXIT_POSITION_ALERT_MESSAGE_TYPE           1
#define PM_RELEASE_POSITION_ALERT_MESSAGE_TYPE              2
#define NEARING_CONSECUTIVE_LOSS_LIMIT_ALERT_MESSAGE_TYPE   3
#define NEARING_STUCK_LIMIT_ALERT_MESSAGE_TYPE              4
#define TRADING_DISABLED_ALERT_MESSAGE_TYPE                 5
#define DISCONNECT_ALERT_MESSAGE_TYPE                       6
#define SINGLE_SERVER_TRADES_ALERT_MESSAGE_TYPE             7
#define SERVER_ROLE_CONFIGURATION_ERROR_ALERT_MESSAGE_TYPE  8
#define GTR_CHANGE_ALERT_MESSAGE_TYPE                       9
#define NO_TRADES_ALERT_MESSAGE_TYPE                        10
#define INCOMPLETE_TRADE_ALERT_MESSAGE_TYPE                 11
#define ORDER_REJECTED_ALERT_MESSAGE_TYPE                   12
#define NO_ACTIVE_TRADER_ALERT_MESSAGE_TYPE                 13
#define NEW_ACTIVE_TRADER_ALERT_MESSAGE_TYPE                14
#define POSITION_WITH_NO_ORDER_ALERT_MESSAGE_TYPE           15
#define PM_ORDER_RATE_HIGHER_THAN_CONFIGURATION_PER_SECOND_ALERT_MESSAGE_TYPE   16
#define BUYING_POWER_ALERT_MESSAGE_TYPE                     17
#define GROUP_OF_BOOK_DATA_UNHANDLED_ALERT_MESSAGE_TYPE   18
#define TT_OUT_OF_DATE_MESSAGE_TYPE             19
#define ADD_SYMBOL_TO_VOLATILE_OR_DISABLED_LIST       20
#define ETB_PROBLEM_ALERT_MESSAGE_TYPE            21
#define MSG_TYPE_FOR_SEND_REJECT_CODE_TO_TT                 15202

/****************************************************************************
** Data structures definitions
****************************************************************************/

/****************************************************************************
** Global variables definition
****************************************************************************/
extern int TS_TO_TT_ORDER_TYPE_MAPPING[];

/****************************************************************************
** Function declarations
****************************************************************************/
int ProcessResetTTParameters(int ttIndex);

void *SendMsgsToTraderTool(void *pConnection);

int SendTSConfToTT(void *pConnection);

int SendTSConfToAllTT(void);

int SendTTConfToTT(void *pConnection);

int SendTTConfToAllTT(void);

int SendASAllOrderConfToTT(void *pConnection);

int SendASOrderConfToTT(void *pConnection, int ECNIndex);

int SendASOrderConfToAllTT(int ECNIndex);

int SendASGlobalConfToTT(void *pConnection);

int SendASGlobalConfToAllTT(void);

int SendASOpenOrderToTT(void *pConnection);

int SendIgnoreStockListToAllTT(void);

int SendIgnoreStockRangesListToAllTT(void);

int SendIgnoreStockListToTT(void *pConnection);

int SendIgnoreStockRangesListToTT(void *pConnection);

int SendAllConfOfTSToTT(void *pConnection);

int SendTSBookConfToTT(void *pConnection, int tsIndex, int ECNIndex);

int SendTSBookConfToAllTT(int tsIndex, int ECNIndex);

int SendTSOrderConfToTT(void *pConnection, int tsIndex, int ECNIndex);

int SendTSOrderConfToAllTT(int tsIndex, int ECNIndex);

int SendTSGlobalConfToTT(void *pConnection, int tsIndex);

int SendTSGlobalConfToAllTT(int tsIndex);

int SendTSMinSpreadConfToTT(void *pConnection, int tsIndex);

int SendTSMinSpreadConfToAllTT(int tsIndex);

int SendTSCurrentStatusToTT(void *pConnection);

int SendASCurrentStatusToTT(void *pConnection);

int SendSymbolStatusToTT(int tsIndex, int ttIndex, void *message);

int SendOpenPositionToTT(void *pConnection);

int SendProfitLossSummaryToTT(void *pConnection);

void *ReceiveMsgsFromTraderTool(void *pConnection);

int ReceiveTTMessage(t_ConnectionStatus *connection, void *message);

int ProcessTTMessageForConnectToTS(void *message);

int ProcessTTMessageForDisconnectToTS(void *message);

int ProcessTTMessageForConnectToAllTS(void *message, int ttIndex);

int ProcessTTMessageForDisconnectToAllTS(void *message, int ttIndex);

int ProcessTTMessageForResetStuckToAllTS(void *message);

int ProcessTTMessageForResetLoserToAllTS(void *message);

//int ProcessTTMessageForResetBuyingPowerToAllTS(void *message);

int ProcessTTMessageForSendNewOrderFromTT(void *message, int ttIndex);

int ProcessTTMessageForSendCancelRequestFromTT(void *message);

int ProcessTTMessageForGetSymbolStatusFromTT(void *message, int ttIndex);

int ProcessTTMessageForFulshASymbolFromTT(void *message);

int ProcessTTMessageForFulshPMASymbolFromTT(void *message);

int ProcessTTMessageForFulshAllSymbolFromTT(void *message);

int ProcessTTMessageForFulshPMAllSymbolFromTT(void *message);

int ProcessTTMessageForEnableTradingAOrderATSFromTT(void *message, int ttIndex);

int ProcessTTMessageForDisableTradingAOrderATSFromTT(void *message, int ttIndex);

int ProcessTTMessageForEnableTradingAllOrderATSFromTT(void *message, int ttIndex);

int ProcessTTMessageForDisableTradingAllOrderATSFromTT(void *message, int ttIndex);

int ProcessTTMessageForEnableTradingAllOrderAllTSFromTT(void *message, int ttIndex);

int ProcessTTMessageForDisableTradingAllOrderAllTSFromTT(void *message, int ttIndex);

int ProcessTTMessageForResetTSCurrentStatusFromTT(void *message);

int ProcessTTMessageForResetASCurrentStatusFromTT(void *message);

int ProcessTTMessageForConnectBookFromTT(void *message, int ttIndex);

int ProcessTTMessageForDisconnectBookFromTT(void *message, int ttIndex);

int ProcessTTMessageForConnectAllBookFromTT(void *message, int ttIndex);

int ProcessTTMessageForDisconnectAllBookFromTT(void *message, int ttIndex);

int ProcessTTMessageForConnectOrderFromTT(void *message, int ttIndex);

int ProcessTTMessageForDisconnectOrderFromTT(void *message, int ttIndex);

int ProcessTTMessageForConnectAllOrderFromTT(void *message, int ttIndex);

int ProcessTTMessageForDisconnectAllOrderFromTT(void *message, int ttIndex);

int ProcessTTMessageForASConnectOrderFromTT(void *message, int ttIndex);

int ProcessTTMessageForASDisconnectOrderFromTT(void *message, int ttIndex);

int ProcessTTMessageForASConnectAllOrderFromTT(void *message, int ttIndex);

int ProcessTTMessageForASDisconnectAllOrderFromTT(void *message, int ttIndex);

int ProcessTTMessageForSetTSConf(void *message);

int ProcessTTMessageForSetTTConf(void *message);

int ProcessTTMessageForSetAlertConf(void *message);

int ProcessTTMessageForSetASOrder(void *message);

int ProcessTTMessageForSetASGlobalConf(void *message, int ttIndex);

int ProcessTTMessageForIgnoreStockList(void *message, int ttIndex);

int ProcessTTMessageForIgnoreStockRangesList(void *message);

int ProcessTTMessageForSetTSBookConf(void *message);

int ProcessTTMessageForSetTSOrderConf(void *message);

int ProcessTTMessageForSetTSGlobalConf(void *message, int ttIndex);

int ProcessTTMessageForSetTSMinSpreadConf(void *message, int ttIndex);

int ProcessTTMessageForLongShort(void *message);

int BuildTTMessageForTradeServersConf(void *message);

int BuildTTMessageForTTConf(void *message);

int BuildTTMessageForAlertConfig(void *message);

int BuildTTMessageForASOrderConf(void *message, int ECNIndex);

int BuildTTMessageForASGlobalConf(void *message);

int BuildTTMessageForIgnoreStockList(void *message);

int BuildTTMessageForIgnoreStockRangesList(void *message);

int BuildTTMessageForTSBookConf(void *message, int tsIndex, int ECNIndex);

int BuildTTMessageForTSOrderConf(void *message, int tsIndex, int ECNIndex);

int BuildTTMessageForTSGlobalConf(void *message, int tsIndex);

int BuildTTMessageForTSMinSpreadConf(void *message, int tsIndex);

int BuildTTMessageForTSCurrentStatus(void *message, int tsIndex);

int BuildTTMessageForASCurrentStatus(void *message);

int BuildTTMessageForSymbolStatus(void *message, int tsIndex, void *messageSrc);

int BuildTTMessageForOpenPosition(void *message, int ttIndex);

int BuildTTMessageForProfitLossSummary(void *message);

int SendTTMessage(void *pConnection, void *message);

int ProcessResetASCurrentStatus(int type, int account);

int ProcessResponseSendNewOrderFromTT(t_SendNewOrder *newOrder, int sendId, int reason, int ttIndex);

int BuildTTMessageForResponseSendNewOrder(void *message, t_SendNewOrder *newOrder, int sendId, int reason);

int SendPMConfToTT(void *pConnection);

int BuildTTMessageForPMConf(void *message);

int SendPMBookConfToAllTT(int ECNIndex);

int BuildTTMessageForPMBookConf(void *message, int ECNIndex);

int SendPMOrderConfToAllTT(int ECNIndex);

int BuildTTMessageForPMOrderConf(void *message, int ECNIndex);

int SendPMGlobalConfToAllTT(void);

int BuildTTMessageForPMGlobalConf(void *message);

int SendPMIgnoredSymbolToAllTT(void);

int BuildTTMessageForPMIgnoredSymbol(void *message);

int ProcessTTMessageForConnectToPM(void *message);

int ProcessTTMessageForDisconnectToPM(void *message);

int SendPMCurrentStatusToTT(void *pConnection);

int BuildTTMessageForPMCurrentStatus(void *message);

int SendAllConfOfPMToTT(void *pConnection);

int SendPMBookConfToTT(void *pConnection, int ECNIndex);

int SendPMOrderConfToTT(void *pConnection, int ECNIndex);

int SendPMGlobalConfToTT(void *pConnection);

int SendPMIgnoredSymbolToToTT(void *pConnection);

int ProcessTTMessageForSetPMConf(void *message);

int SendPMConfToAllTT(void);

int ProcessTTMessageForSetPMGlobalConf(void *message);

int ProcessTTMessageForSetPMOrderConf(void *message);

int ProcessTTMessageForSetPMIgnoreSymbol(void *message, int ttIndex);

int ProcessTTMessageForEnablePMFromTT(void *message, int ttIndex);

int ProcessTTMessageForDisablePMFromTT(void *message, int ttIndex);

int ProcessTTMessageForConnectAPMOrderFromTT(void *message, int ttIndex);

int ProcessTTMessageForDisconnectAPMOrderFromTT(void *message, int ttIndex);

int ProcessTTMessageForConnectAllPMOrderFromTT(void *message, int ttIndex);

int ProcessTTMessageForDisconnectAllPMOrderFromTT(void *message, int ttIndex);

int ProcessTTMessageForConnectAPMBookFromTT(void *message, int ttIndex);

int ProcessTTMessageForDisconnectAPMBookFromTT(void *message, int ttIndex);

int ProcessTTMessageForConnectAllPMBookFromTT(void *message, int ttIndex);

int ProcessTTMessageForDisconnectAllPMBookFromTT(void *message, int ttIndex);

int ProcessTTMessageForGetPMSymbolStatusFromTT(void *message, int ttIndex);

int SendPMSymbolStatusToTT(int ttIndex, void *message);

int BuildTTMessageForPMSymbolStatus(void *message, void *messageSrc);

int ProcessTTMessageForSendDisengageSymbolFromTT(void *message, int ttIndex);

int ProcessTTMessageForSendEngageStuckFromTT(void *message, int ttIndex);

int ProcessTTMessageForSetPMBookConf(void *message);

int ProcessTTMessageForConnectCTSFromTT(void *message, int ttIndex);

int ProcessTTMessageForConnectCQSFromTT(void *message, int ttIndex);

int ProcessTTMessageForDisconnectCTSFromTT(void *message, int ttIndex);

int ProcessTTMessageForDisconnectCQSFromTT(void *message, int ttIndex);

int ProcessTTMessageForRerequestEtbList();

int SendHaltStockListToAllTT();
int BuildTTMessageForHaltedSymbolList(void *message);

int SendActiveHaltedStockListToAllTT(int updatedList[MAX_STOCK_SYMBOL], int updatedCount);
int BuildTTMessageForActiveHaltedSymbolList(void *message, int updatedList[MAX_STOCK_SYMBOL], int updatedCount);

int ProcessTTMessageForSetHaltStockFromTT(void *message, int ttIndex);
int ProcessTTMessageForActivateHaltedStockFromTT(void *message, int ttIndex);

int ProcessTTMessageForGetTradedListOfASymbolFromTT(void *message, int ttIndex);

int SendTradedListToTT(int ttIndex, void *message);
int BuildTTMessageForTradedList(void *message, void *msg);

int ProcessTTMessageForConnectUTDFFromTT(void *message, int ttIndex);
int ProcessTTMessageForDisconnectUTDFFromTT(void *message, int ttIndex);

int SendPMDisengageAlertToAllTT(void *message);
int BuildTTMessageForPMDisengageAlert(void *message, void *messageSrc);

int SendTSSettingToTT(void *pConnection);
int BuildTTMessageForTSSetting(void *message, int tsIndex);
int ProcessTTMessageForSetTSSettingFromTT(void *message);
int SendTSSettingToAllTT(int tsIndex);

int SendASSettingToTT(void *pConnection);
int BuildTTMessageForASSetting(void *message);
int ProcessTTMessageForSetASSettingFromTT(void *message);
int SendASSettingToAllTT(void);

int SendPMSettingToTT(void *pConnection);
int BuildTTMessageForPMSetting(void *message);
int ProcessTTMessageForSetPMSettingFromTT(void *message);
int SendPMSettingToAllTT(void);
int BuildTTMessageForTSPMBookIdleWarning(void *message, int tsPMIndex, int ECNIndex, int groupID);
int SendTSPMBookIdleWarningToAllTT(int tsPmIndex, int ecnIndex, int groupIndex);

int ProcessMessageForConnectToCQSnUQDF_FromTT(t_TTMessage *message, int ttIndex);
int ProcessMessageForDisconnectToCQSnUQDF_FromTT(t_TTMessage *message, int ttIndex);
int ProcessMessageForGetBBOQuotes(t_TTMessage *message, int ttIndex);
int ProcessMessageForSetExchangeConf(t_TTMessage *message);
int ProcessMessageForFlushBBOQuotes(t_TTMessage *message);
int SendExchangesConfToTT(void *pConnection, int tsIndex);
int SendExchangesConfToAllTT(int tsIndex);
int SendExchangesConfOfAllTSToTT(void *pConnection);
int SendBBOQuotesToTT(int ttIndex, int tsIndex, void *message);
int ProcessMessageForSetISO_TradingFromTT(t_TTMessage *message, int ttIndex);

int SendBookISO_TradingToAllTT(int tsIndex);
int SendBookISO_TradingOfAllTSToTT(void *pConnection);
int SendBookISO_TradingToTT(void *pConnection, int tsIndex);
int ProcessMessageForSetBookISO_Trading(t_TTMessage *message);
int SendTSCurrentStatusToAllTT(int tsIndex);
int SendASCurrentStatusToAllTT();
int SendPMCurrentStatusToAllTT();

//For UQDF
int ProcessTTMessageForConnectUQDF_FromTT(void *message, int ttIndex);
int ProcessTTMessageForDisconnectUQDF_FromTT(void *message, int ttIndex);

//For PM Exchange Configuration
int SendPMExchangesConfToTT(void *pConnection);
int SendPMExchangesConfToAllTT();
int ProcessMessageForSetPMExchangeConf(t_TTMessage *ttMessage);

//For PM BBO Viewer
int ProcessMessageForGetPMBBOQuotes(t_TTMessage *message, int ttIndex);
int ProcessMessageForFlushPMBBOQuotes(t_TTMessage *message);
int SendPMBBOQuotesToTT(int ttIndex, void *message);
int BuildTTMessageForMarketModeUpdate(void *message, const char mode);
int SendMarketModeUpdateToAllTT(const char mode);

//For version control
int ProcessMessageForLogTTVersionControl(t_TTMessage *message, int ttIndex);

//For Verify Position
int SendMessageForNotifyNoMatchingPositionToTT(int ttIndex, const char *symbol, int shares, int side, int isSent);
int BuildTTMessageForNotifyNoMatchingPosition(void *message, const char *symbol, int shares, int side, int isSent);

//For Login Info
int ProcessMessageForAuthenticateLoginInfo(t_TTMessage *message, int ttIndex);
int SendMessageForPermitTraderLoginToTT(int ttIndex, char trader[MAX_LINE_LEN], int state);
int BuildTTMessageForPermitTraderLogin(void *message, char trader[MAX_LINE_LEN], int state);
int BuildTTMessageForTraderStatusInfo(void *message, int type);
int SendTraderStatusInfoToAllTT(int type);
int ProcessMessageForSwitchTraderRole(t_TTMessage *message, int ttIndex);

// For get the number of symbols with quotes 
int ProcessTTMessageForGetNumberOfSymbolsWithQuotesFromTT(void *message, int ttIndex);
int SendNumberOfSymbolsWithQuotesToTT(int ttIndex, int tsIndex, int numberOfSymbolsWithQuotes[TS_MAX_BOOK_CONNECTIONS]);
int BuildTTMessageForNumberOfSymbolsWithQuotes(void *message, int tsIndex, int numberOfSymbolsWithQuotes[TS_MAX_BOOK_CONNECTIONS]);

int ProcessTTMessageForSetTSARCABookFeed(void *message);

int SendNotifyMessageToAllTT(char *msg, int level);
int SendNotifyMessageToTT(int ttIndex, char *msg, int level);
int BuildTTMessageForNotifyMessage(void *message, char *msg, int level);

int ProcessTTMessageForSetPMARCABookFeed(void *message);

int SendVolatileSymbolListToAllTT();
int BuildTTMessageForVolatileSymbolList(void *message);
int ProcessTTMessageForVolatileSymbolUpdate(void *message);
int SendVolatileSymbolListToTT(void *pConnection);

int SendVolatileSectionsToAllTT();
int BuildTTMessageForVolatileSections(void *message);
int ProcessTTMessageForVolatileSectionUpdate(void *message, t_ConnectionStatus *connection);
int SendVolatileSectionsToTT(t_ConnectionStatus *connection);

int SendAllVolatileToggleScheduleToAllTT();
int SendAllVolatileToggleScheduleToTT(t_ConnectionStatus *connection);
int ProcessTTMessageForAllVolatileToggle(void *message);
int BuildTTMessageForAllVolatileToggle(void *message);

int SendAlertConfToTT(void *pConnection);
int ProcessTTMessageForSetAlertConf(void *message);
int BuildTTMessageForAlertConfig(void *message);
int SendAlertConfigToAllTT(void);

// Alerting
int BuildNSendAlertToAllTT(char *alert);
int BuildNSendBuyingPowerDropsBelowThresholdAlertToAllTT(char *server, double amount);
int BuildNSendConsecutiveLossNearingLimitAlertToAllTT();
int BuildNSendGroupfOfBookDataUnhandledAlertToAllTT(char *reason);
int BuildNSendDisconnectAlertToAllTT(char *service);
int BuildNSendGTRAlertToAllTT(double amount);
int BuildNSendInCompleteTradeAlertToAllTT(char *orderIdList);
int BuildNSendNewActiveTraderAlertToAllTT(char * trader);
int BuildNSendNoActiveTraderAlertToAllTT();
int BuildNSendNoTradesDuringConfiguredTimePeriodAlertToAllTT(int serverID, int munites, char *startTimeStamp, char *endTimeStamp);
int BuildNSendOrderRejectedAlertToAllTT(int oid);
int BuildNSendPMDoesNotAcceptPositionAlertToAllTT(char *symbol, double price, int share, char side, int account);
int BuildNSendPMHasntExitPositionAlertToAllTT(char *symbol, double price, int share, char side, int account);
int BuildNSendPMReleasePositionAlertToAllTT(char *symbol, double price, int share, char side, int account);
int BuildNSendServerRoleConfigurationMisConfiguredAlertToAllTT(char * servers);
int BuildNSendSingleServerTradesAlertToAllTT(char *server);
int BuildNSendStuckNearingLimitAlertToAllTT();
int BuildNSendTradingDisabledAlertToAllTT(char *reason);
int BuildNSendUnhandledOpenPositionWithoutAnOrderAlertToTT(char *symbol, int share[MAX_ACCOUNT], char side[MAX_ACCOUNT], int account[MAX_ACCOUNT], int count);
int BuildNSendDisabledSymbolAlertToAllTT(char *reason);
int BuildNSendPMOrderRateAlertToAllTT(int number);

int BuildNSendTTOutOfDateAlertToTT(char *reason, int ttIndex);

int SendMessageToAllTT(t_TTMessage *ttMessage);
int ProcessTTMessageForSetDisabledAlertRejectCodeFromTT(void *message);

int SendRejectCodeToATT(t_ConnectionStatus *connection);
int BuildMsgRejectCodeToTT(int ECNType, t_TTMessage *ttMessage);
int BuildNSendRejectCodeToATT(int ECNType, t_ConnectionStatus *connection);

int SendRejectCodeToAllTT(void);
int BuildNSendRejectCodeToAllTT(int ECNType);

int SendHaltStockListToTT(void *pConnection);
int SendAHaltStockToAllTT(const int stockSymbolIndex);
int BuildTTMessageForHaltSymbol(void *message, const int stockSymbolIndex);

int BuildTTMessageForActiveVolatileSymbolList(void *message, int currentVolatileSession);
int BuildTTMessageForAllVolatileToggleStatus(void *message);
int SendAllVolatileToggleStatusToTT(t_ConnectionStatus *connection);
int SendActiveVolatileSymbolsToTT(t_ConnectionStatus *connection, int currentVolatileSession);

int SendSleepingSymbolListtoTT(t_ConnectionStatus *connection);
int BuildTTMessageForSleepingSymbolList(void *message);
int UpdateTimeInForce(t_SendNewOrder *newOrder);
/***************************************************************************/

#endif
