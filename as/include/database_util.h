/*****************************************************************************
**  Project:    Equity arbitrage application
**  Filename:   database_util.h
**  Description:    This file contains some declaration for database_util.c 
**  Author:   Hoang Le-Quoc
**  First created:  14-Sep-2007
**  Last updated: -----------
*****************************************************************************/

#ifndef __DATABASE_UTIL_H__
#define __DATABASE_UTIL_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include <stdio.h>

//  The library for using PostgreSQL API
#include <libpq-fe.h>

//  The library for using LDAP API
#include <ldap.h>

#include "global_definition.h"
#include "utility.h"
#include "output_log_mgmt.h"

#include "arca_direct_data.h"
#include "nasdaq_ouch_data.h"
#include "nasdaq_rash_data.h"
#include "nyse_ccg_data.h"
#include "bats_boe_data.h"
#include "edge_order_data.h"
#include "query_data_mgmt.h"

//#define CONNECTION_STRING "dbname=eaa10_200808 user=postgres password=1qazXSW@ host=localhost"

#define PGSQL_DIR "/usr/bin/"

/****************************************************************************
** Data structures definitions
****************************************************************************/

/****************************************************************************
** Global variables definition
****************************************************************************/

extern PGconn * connRawData;
extern PGconn * connSaveSettings;
extern PGconn * connQueryData;
extern PGconn * connTimeSeries;
extern PGconn * connPrevMonthDatabase;

pthread_mutex_t connSaveSettings_Mutex;

/****************************************************************************
** Function declarations
****************************************************************************/

void InitializeDatabaseMutex(void);

PGconn *CreateConnection(const char * connectionString, PGconn * connection);
void  DestroyConnection(PGconn * connection);
int CheckAndCreateNewDatabase(char *connStr);

PGresult * ExecuteQuery(PGconn * connection, const char * queryString);

int   GetNumberOfRow( PGresult * queryResult );
int   GetNumberOfColum( PGresult * queryResult );
int   IsFieldNull( PGresult * queryResult, int rowIndex, int colIndex );
char *  GetValueField( PGresult * queryResult, int rowIndex, int colIndex );
void  ClearResult ( PGresult * queryResult );

int   ProcessInsertNewOrder(t_New_Order_DB *newOrder);
int   ProcessInsertOrderACK(t_Order_ACK_DB *orderAccepted);
int   InsertOrderExecuted(t_Order_Executed_DB *orderExecuted);
int   InsertOrderCancelled(t_Order_Canceled_DB *oCancelled);
int   InsertCancelRequest(t_Cancel_Request_DB *cancelRequest);
int   InsertCancelACK(t_Cancel_ACK_DB *cancelACK);
int   InsertCancelPending(t_Cancel_Pending_DB *pendingCancel);
int   InsertlBustOrCorrect(t_Broken_Trade_DB *brokenTrade);
int   InsertOrderRejected(t_Order_Rejected_DB *rejectedOrder);
int   InsertCancelRejected(t_Cancel_Rejected_DB *rejectedCancel);

int   UpdateStatusOnly( int oid, const char * status );
int   UpdateNewOrderStatus( int oid, const char * status, int leftShares, double avgPrice );
int   UpdateNewOrderStatusForNASDAQ(int oid, const char * status, int shares );
int   GetCurrentOID( int orderID, const char * date);

int   GetTradeServersConfig( t_TradeServerInfo * tradeServersInfo );
int   UpdateTradeServerConfig( t_TradeServerConf * tradeServersConfig );
int UpdateTradeServerSetting(t_TSSetting *tsSetting, int tsId);

int GetOtherSetting(void);
int UpdateASSetting(void);
int UpdatePMSetting(void);

int   UpdateRASHPrice( int oid, double filledPrice );

int GetRawDataFromNewOrder(const char *date);
int GetRawDataFromNonExecuteTable(const char *date, const char *tableName, int editorType);
int GetRawDataFromExecuteTable(const char *date, const char *tableName, int editorType);
int UpdateDatabaseTable(const char *date, const char *tableName);

int GetOpenPositions(const char *date, int mustUpdate);

int GetBrokenTradeData(const char *date);
int GetFilledOrderForSymbolList(t_BrokenTradeCollection *newBrokenList, const char *dateString);
int GetFillInfoOfBroken(int fillIdList[MAX_BROKEN_TRADE], int count, const char *dateString);

int InsertTradeOutputLogShortSellFailed(t_Short_Sell_Failed *shortSellFailed);
int InsertTradeOutputLogNotYetDetect(t_Output_Log_Not_Yet_Detect *outputLogNotYetDetect);
int InsertTradeOutputLogDetected(t_Output_Log_Detected *outputLogDetected);
int UpdateTradeOutputLog(t_Output_Log *outputLog);
int InsertTradeOutputLogNotYetDetect_Trade(t_Output_Log_Not_Yet_Detect_Trade *outputLogNotYetDetectTrade);
int InsertTradeOutputLogDetected_Trade(t_Output_Log_Detected_Trade *outputLogDetectedTrade);
int UpdateTradeOutputLog_Trade(t_Output_Log_Trade *outputLogTrade);

int DeleteDatabaseAtDate(const char *dateString);

int CheckAndCreateLongShortAtDate(const char *dateString);
int LoadLongShortAtDate(const char *dateString);

int InsertTradeResultToDatabase(t_TradeResultInfo *tradeResult);
int UpdateRealValueToDatabase(t_Real_Value *realValue);
int LoadStuckFromDatabase(char *dateString);
int LoadTradeLossFromDatabase(char *dateString);

int LoadHungOrderStatusFromDatabase(void);
int UpdateHungOrderStatusToDatabase(void);

int LoadTradingStatusFromDatabase(int *isDisableTrading, int *isUpdate);
int LoadBrokenRawdataForISOOrderFromDatabase(const char *date);
int UpdateTradingStatusToDatabase(int tradingStatus, int isUpdate);
int LoadModifyLongShortStatusFromDatabase(int *rmUpdated);
int UpdateModifyLongShortStatusToDatabase(void);

int InsertAskBid(t_Output_Log_Ask_Bid *outputLogAskBid);
int InsertBBOQuotesIntoDatabase(t_dbBBOInfo *bboInfo);

int AuthenticateLoginInfo(char userName[MAX_LINE_LEN], char password[MAX_LINE_LEN]);
int LdapAuthentication(char userName[MAX_LINE_LEN], char password[MAX_LINE_LEN]);
int UpdateTTEventLogToDatabase(char userName[MAX_LINE_LEN], int activityType, const char *activityDetail, const char *dateString, const char *timeStamp);
int UpdateTraderStatusToDatabase(char userName[MAX_LINE_LEN], int status);
int InitializeTraderStatus(void);
int ProcessUpdateTTEventLogToDatabase(char userName[MAX_LINE_LEN], int activityType, const char *activityDetail);
int ProcessUpdateTraderStatusToDatabase(char userName[MAX_LINE_LEN], int status);
int GetTraderStatusInfo(void);
int GetStatusOfTrader(char userName[MAX_LINE_LEN]);
int GetTraderActiveInfo(t_TraderStatusInfo *traderActive);
int CountTraderHasStatus(int status);
int InsertNewTraderInfo(char userName[MAX_LINE_LEN]);
int IsTraderExisted(char userName[MAX_LINE_LEN]);
int LoadAndCalculateRealValue(char *symbol, int side, t_StuckInfo *stuckInfo, char *dateString);
int DisableRASHOnAS(void);
int EnableRASHOnPM(void);
int GetOpenPositionsFromDatabaseWithTime(void *_list);
int GetManualOrderInfoFromDatabase(char *strDate);
int LoadLogMeasurementFromDatabase(void);
int ProcessEntryNewOrderDelayTime(char *time);
int ProcessAckDelayTime(char *time);
int ProcessFillDelayTime(char *time);
int ProcessCancelDelayTime(char *time);
int ProcessExitNewOrderDelayTime(char *time);

int GetModuleVersion(int moduleIndex);
int IsSymbolTradedSinceHaltedDate(char *symbol, char *hatedDate);
char *GetVenueName(char *ecn);
char *GetServerName(int orderId);
/***************************************************************************/

#endif
