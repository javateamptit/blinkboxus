/*****************************************************************************
**  Project:    Equity Arbitrage Application
**  Filename:   nasdaq_ouch_data.h
**  Description:  This file contains some declaration for nasdaq_ouch_data.c  
**  Author:     Luan Vo-Kinh
**  First created:  13-Sep-2007
**  Last updated: -----------
*****************************************************************************/

#ifndef __NASDAQ_OUCH_DATA_H__
#define __NASDAQ_OUCH_DATA_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include <stdlib.h>
#include <math.h>

#include "global_definition.h"
#include "utility.h"
#include "position_manager_proc.h"

/****************************************************************************
** Data structures definitions
****************************************************************************/
// The structure is used contain several fields of New Order
typedef struct t_OUCHNewOrder
{
  double price;

  int orderID;
  int seqNum;
  int tsId;
  int crossId;
  int tradeType;
  int shares;
  int minQuantity;
  int leftShares;
  int tradingAccount;
  int visibleShares;

  short bboId;
  unsigned short entryExitRefNum;

  char date[12];
  char askBid[4];
  char timestamp[16];
  char msgType[16];
  char symbol[SYMBOL_LEN];
  char timeInForce[4];
  char firm[6];
  char entryExit[6];
  char ecnLaunch[6];
  char ecn[6];
  char side[3];
  char display;
  char status[16];
  char capacity;
  char eligibility;
  char crossType;

  char msgContent[MAX_ORDER_MSG];
} t_OUCHNewOrder;

// The structure is used contain several fields of Accepted Order
typedef struct t_OUCHAcceptedOrder
{
  double price;
  
  long ouchExOrderId;
  
  int minQuantity;
  int tradingAccount;
  int tsId;
  int orderID;
  int seqNum;
  int shares;
  
  char date[12];
  char ecn[6];
  char timestamp[16];
  char processedTimestamp[16];
  char transTime[20];
  char msgType[16];
  char side[3];
  char symbol[SYMBOL_LEN];
  char timeInForce[4];
  char firm[6];
  char display;
  char capacity;
  char eligibility;
  char crossType;
  char orderState;
  char status[16];
  
  char msgContent[MAX_ORDER_MSG];
} t_OUCHAcceptedOrder;

// The structure is used contain several fields of Executed Order
typedef struct t_OUCHExecutedOrder
{
  double price;
  double avgPrice;
  double fillFee;
  
  long executionId;
  
  int orderID;
  int seqNum;
  int shares;
  int leftShares;
  unsigned short entryExitRefNum;
  
  char date[12];
  char timestamp[16];
  char processedTimestamp[16];
  char transTime[20];
  char msgType[16];
  char status[16];
  char liquidityIndicator;
  
  char msgContent[MAX_ORDER_MSG];
} t_OUCHExecutedOrder;

// The structure is used contain several fields of Canceled Order
typedef struct t_OUCHCanceledOrder
{
  int orderID;
  int seqNum;
  int shares;
  unsigned short entryExitRefNum;
  
  char date[12];
  char timestamp[16];
  char processedTimestamp[16];
  char transTime[20];
  char msgType[16];
  char reason;
  char status[16];

  char msgContent[MAX_ORDER_MSG];
} t_OUCHCanceledOrder;

// The structure is used contain several fields of Cancel Request
typedef struct t_OUCHCancelRequest
{
  int orderID;
  int seqNum;
  int shares;
  
  char date[12];
  char timestamp[16];
  char msgType[16];
  char status[16];
  
  char msgContent[MAX_ORDER_MSG];
} t_OUCHCancelRequest;

// The structure is used contain several fields of Rejected Order
typedef struct t_OUCHRejectedOrder
{
  int orderID;
  int seqNum;  
  unsigned short entryExitRefNum;
  
  char date[12];
  char timestamp[16];
  char processedTimestamp[16];
  char transTime[20];
  char msgType[16];
  char status[16];
  char reason;

  char msgContent[MAX_ORDER_MSG];
} t_OUCHRejectedOrder;

// The structure is used contain several fields of Broken Order
typedef struct t_OUCHBrokenOrder
{
  long executionId;
  
  int orderID;
  int seqNum;
  
  char date[12];
  char timestamp[16];
  char transTime[20];
  char msgType[16];
  char status[16];
  char reason;
  
  char msgContent[MAX_ORDER_MSG];
} t_OUCHBrokenOrder;

// The structure is used contain several fields of Cancel Pending
typedef struct t_OUCHCancelPending
{
  int orderID;
  int seqNum;
  
  char date[12];
  char timestamp[16];
  char transTime[20];
  char msgType[16];
  char status[16];
  
  char msgContent[MAX_ORDER_MSG];
} t_OUCHCancelPending;


typedef struct t_OUCHCancelReject
{
  int orderID;
  int seqNum;

  char date[12];
  char timestamp[16];
  char transTime[20];
  char msgType[16];
  char status[16];

  char msgContent[MAX_ORDER_MSG];
} t_OUCHCancelReject;

/****************************************************************************
** Global variables definition
****************************************************************************/

/****************************************************************************
** Function declarations
****************************************************************************/

int ParseRawDataForOUCHOrder( t_DataBlock dataBlock, int type, int oecType );

int ProcessNewOrderOfOUCHOrder( t_DataBlock dataBlock, int type, char *timeStamp, int oecType );

int ProcessAcceptedOrderOfOUCHOrder( t_DataBlock dataBlock, int type, char *timeStamp, int oecType );

int ProcessExecutedOrderOfOUCHOrder( t_DataBlock dataBlock, int type, char *timeStamp, int oecType );

int ProcessCanceledOrderOfOUCHOrder( t_DataBlock dataBlock, int type, char *timeStamp, int oecType );

int ProcessCancelRequestOfOUCHOrder( t_DataBlock dataBlock, int type, char *timeStamp, int oecType );

int ProcessRejectedOrderOfOUCHOrder( t_DataBlock dataBlock, int type, char *timeStamp, int oecType );

int ProcessBrokenOrderOfOUCHOrder( t_DataBlock dataBlock, int type, int oecType );

int ProcessPriceCorrectionOfOUCHOrder( t_DataBlock dataBlock, int type, int oecType );

int ProcessCancelPendingOfOUCHOrder( t_DataBlock dataBlock, int type, int oecType );

int ProcessCancelRejectOfOUCHOrder( t_DataBlock dataBlock, int type, int oecType );

/***************************************************************************/

#endif
