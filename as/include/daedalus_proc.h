/*****************************************************************************
**  Project:    Equity Arbitrage Application
**  Filename:   daedalus_proc.h
**  Description:  This file contains some declaration for daedalus_proc.c 
**  Author:     Long Nguyen-Khoa-Hai
**  First created:  01-Oct-2013
**  Last updated: -----------
*****************************************************************************/

#ifndef __DAEDALUS_PROC_H__
#define __DAEDALUS_PROC_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include <stdlib.h>
#include <string.h>

#include "global_definition.h"
#include "order_mgmt_proc.h"
#include "utility.h"
#include "internal_proc.h"

#include "DaedalusMessage.pb-c.h"
#include "DaedalusRequest.pb-c.h"

/****************************************************************************
** Data structures definitions
****************************************************************************/

//CQS, UQDF, CTS, UTDF
enum e_AwayMarketBook
{
  CQS_BOOK = 0,
  UQDF_BOOK = 1,
  CTS_BOOK = 2,
  UTDF_BOOK = 3,
};

typedef struct t_GTRMgt
{
  double gross[MAX_GTR_SAMPLE_IN_SECONDS];
  int currentSecond;
}t_GTRMgt;

typedef struct t_NoTradesDurationMgt
{
  char startTimeStamp[8];
  char endTimeStamp[8];
  int  alertThresholdInMinute;
  int currentNoTradesDurationInSecond;
} t_NoTradesDurationMgt;

typedef struct t_HoldingEnableTradingInfo
{
  int ttIndex;
  int isEnabled;
} t_HoldingEnableTradingInfo;

typedef struct t_MonitorStatusChange
{
  int count;
  int updateServerStatus;
  int updateServerInfo;
  
  int tsServerStatus[MAX_TRADE_SERVER_CONNECTIONS];
  int tsMdcStatus[MAX_TRADE_SERVER_CONNECTIONS][TS_MAX_BOOK_CONNECTIONS];
  int tsOecStatus[MAX_TRADE_SERVER_CONNECTIONS][TS_MAX_ORDER_CONNECTIONS];
  int tsAwayMarketMdcStatus[MAX_TRADE_SERVER_CONNECTIONS][2]; // CQS, UQDF
  
  int pmServerStatus;
  int pmMdcStatus[PM_MAX_BOOK_CONNECTIONS];
  int pmOecStatus[PM_MAX_ORDER_CONNECTIONS];
  int pmAwayMarketMdcStatus[4]; // CQS, UQDF, CTS, UTDF
  
  int asServerStatus;
  int asOecStatus[AS_MAX_ORDER_CONNECTIONS];
}t_MonitorStatusChange;

/****************************************************************************
** Global variables definition
****************************************************************************/
extern t_TraderCollection traderCollection;

extern t_NoTradesDurationMgt NoTradesDurationMgt[MAX_TRADE_SERVER_CONNECTIONS];
extern int PMOrderPerSecond;
extern t_GTRMgt GTRMgt;
extern int AlertingProgress;

extern int DAEDALUS_OEC_INDEX_TO_TS_ORDER_MAPPING[];
extern int DAEDALUS_OEC_INDEX_TO_PM_ORDER_MAPPING[];
extern int DAEDALUS_MDC_INDEX_TO_TS_BOOK_MAPPING[];
extern int DAEDALUS_MDC_INDEX_TO_PM_BOOK_MAPPING[];
/****************************************************************************
** Function declarations
****************************************************************************/
void *ThreadSendMsgsToDaedalus(void *pConnection);
int ProcessResetDaedalusParameters(int ttIndex);

int SendHeartbeatToDaedalus(void *pConnection);
int SendServersCurrentStatusToDaedalus(t_MonitorStatusChange *monitor, void *pConnection);
int SendTSServerCurrentStatusToDaedalus(t_MonitorStatusChange *monitor, void *pConnection, int tsIndex);
int SendTSServersCurrentStatusToDaedalus(t_MonitorStatusChange *monitor, void *pConnection);
int SendPMServerCurrentStatusToDaedalus(t_MonitorStatusChange *monitor, void *pConnection);
int SendASServerCurrentStatusToDaedalus(t_MonitorStatusChange *monitor, void *pConnection);

int ResetTSStatus(int tsIndex);
int ResetPMStatus();

int CheckStatusChanges();
int MonitorStatusChange();
int InitMonitorServerStatusChange(t_MonitorStatusChange *monitor);
int MonitorServerStatusChange(t_MonitorStatusChange *monitor, int tsIndex, char *serverName, int accountIndex);

int BuildAndSendServerStatusToDaedalus(void *pConnection, int tsIndex, char *serverName);
int BuildAndSendServerInfoToDaedalus(void *pConnection, int tsIndex, char *serverName, int accountIndex);

int BuildAndSendMdcStatusToDaedalus(void *pConnection,
                char *serverName,
                char serverConnectionStatus,
                Com__Rgm__Daedalus__Proto__MDC mdcType,
                protobuf_c_boolean isConnected,
                protobuf_c_boolean queryable);              
                
int BuildAndSendOecStatusToDaedalus(void *pConnection,
                char *serverName,
                char serverConnectionStatus,
                Com__Rgm__Daedalus__Proto__OEC oecType,
                int orderStatus, //Internal State
                int tradingStatus, //External State
                char *orderIp,
                int orderPort,
                char* sessionId);
                
int BuildAndSendSymbolStatusToDaedalus(char serverName[MAX_LINE_LEN], int ttIndex, void *message);
                
int SendTSIgnoreSymbolListToDaedalus(void *pConnection);
int SendTSIgnoreSymbolListToAllDaedalus(void);
int BuildIgnoreSymbolListToDaedalus(void *message, Com__Rgm__Daedalus__Proto__BlinkBoxServerType serverType);

int SendIgnoreSymbolRangesListToAllDaedalus(void);
int SendIgnoreSymbolRangesListToDaedalus(void *pConnection);
int BuildIgnoreSymbolRangesListToDaedalus(void *message);
int SendPMIgnoredSymbolToAllDaedalus(void);
int SendPMIgnoredSymbolToToDaedalus(void *pConnection);

int SendHaltStockListToDaedalus(void *pConnection);
int SendHaltStockListToAllDaedalus();
int SendAHaltStockToAllDaedalus(const int stockSymbolIndex);
int BuildHaltedSymbolListToDaedalus(void *message);
int IsSymbolHaltedOnPMBooks(int symbolIndex);
int SendToActivateHaltStockListToAllDaedalus(int updatedList[MAX_STOCK_SYMBOL], int updatedCount);
int BuildToActivateHaltedSymbolListToDaedalus(void *message, int updatedList[MAX_STOCK_SYMBOL], int updatedCount);

int SendVolatileSymbolListToAllDaedalus();
int SendVolatileSymbolListToDaedalus(void *pConnection);
int BuildVolatileSymbolListToDaedalus(void *message);
int SendVolatileSectionsToAllDaedalus();
int SendVolatileSectionsToDaedalus(t_ConnectionStatus *connection);
int BuildVolatileSymbolListBySessionToDaedalus(void *message);
int SendAllVolatileToggleScheduleToAllDaedalus();
int SendAllVolatileToggleScheduleToDaedalus(t_ConnectionStatus *connection);
int BuildVolatileToggleListToDaedalus(void *message);

int SendCurrentVolatileSessionToAllDaedalus();
int SendCurrentVolatileSessionToDaedalus(void *pConnection);
int BuildCurrentVolatileSession(void *message);
// int BuildAndSendCurrentVolatileSessionToAllDaedalus();

int SendVolatileToggleToAllDaedalus();
int SendVolatileToggleToDaedalus(void *pConnection);
int BuildVolatileToggle(void *message);
// int BuildAndSendVolatileToggleToAllDaedalus();

int SendAlertConfToDaedalus(void *pConnection);
int SendAlertConfigToAllDaedalus(void);
int BuildAlertConfigToDaedalus(void *message);

int SendMessageForPermitTraderLoginToDaedalus(int ttIndex, char trader[MAX_LINE_LEN], int state);
int BuildLoginStatusToDaedalus(void *message, char trader[MAX_LINE_LEN], int state);
int SendTraderStatusInfoToAllDaedalus(int type);
int BuildOperatorInfoToDaedalus(void *message);

int SendExchangesConfToAllDaedalus(int tsIndex);
int SendExchangesConfOfAllTSToDaedalus(void *pConnection);
int SendPMExchangesConfToAllDaedalus();
int BuildAndSendTSExchangeConfigToDaedalus(void *pConnection, int tsIndex);
int BuildAndSendPMExchangeConfigToDaedalus(void *pConnection);

int BuildAndSendServerInfoToPlaceManualOrderToDaedalus(void *pConnection, char serverName[MAX_LINE_LEN]);
int BuildResponseSendNewOrderToDaedalus(void *message, t_SendNewOrder *newOrder, int sendId, int reason, char *reasonText);

int BuildAndSendASOpenOrderToDaedalus(void *pConnection);

int SendTSPMBookIdleWarningToAllDaedalus(int tsPmIndex, int ecnIndex, int groupIndex);
int BuildBookIdleWarningToDaedalus(void *message, int tsPMIndex, int ECNIndex, int groupID);

int SendASGlobalConfToDaedalus(void *pConnection);
int SendASGlobalConfToAllDaedalus(void);
int BuildASGlobalConfigToDaedalus(void *message);

int SendAllConfOfPMToDaedalus(void *pConnection);

int CheckNSendRequestPriceUpdate();
int SendPriceUpdateToAllDaedalus(char serverName[MAX_LINE_LEN], unsigned char *receivedMsg);
int BuildPriceUpdateToAllDaedalus(t_TTMessage *ttMessage, unsigned char *receivedMsg, int *msgIndex);

int SendFullOpenPositionListToDaedalus(void *pConnection);
int CheckAndSendOpenPositionsToDaedalus(void *pConnection);
int SendAnOpenPositionToDaedalus(void *pConnection, int accountIndex, int rmInfoIndex, int isAggregatedAll);
int BuildAndSendUpdatedPositionStateToDaedalus(void *pConnection, int account, int rmInfoIndex, Com__Rgm__Daedalus__Proto__PositionState internalState);

int BuildAndSendAlertToDaedalus(char *alertMsg, int ttIndex, Com__Rgm__Daedalus__Proto__NotificationLevelType level);
int BuildAndSendAlertToAllDaedalus(char *alertMsg, Com__Rgm__Daedalus__Proto__NotificationLevelType level);

int CheckNRaiseServerRoleConfigurationMisConfiguredAlertToAllTT();
int CheckNRaiseSingleServerTradesAlertToAllTT();
void CheckNSendGTRAlertToAllTT();
int CheckNSendOrderRejectedAlertToAllTT(int ECNType, const char* rejectCode, const char* rejectText, int oid);
int CheckAndSendVolatileSectionsToAllTT();
int CheckAndSendVolatileSymbolsToAllTT();
int BuildNSendBuyingPowerDropsBelowThresholdAlertToAllDaedalus(char *server, double amount);
int BuildNSendConsecutiveLossNearingLimitAlertToAllDaedalus();
int BuildNSendGroupfOfBookDataUnhandledAlertToAllDaedalus(char *reason);
int BuildNSendDisconnectAlertToAllDaedalus(char *service);
int BuildNSendGTRAlertToAllDaedalus(double amount);
int BuildNSendInCompleteTradeAlertToAllDaedalus(char *orderIdList);
int BuildNSendNewActiveTraderAlertToAllDaedalus(char * trader);
int BuildNSendNoActiveTraderAlertToAllDaedalus();
int BuildNSendNoTradesDuringConfiguredTimePeriodAlertToAllDaedalus(int serverID, int minutes, char *startTimeStamp, char *endTimeStamp);
int BuildNSendOrderRejectedAlertToAllDaedalus(int oid);
int BuildNSendPMOrderRateAlertToAllDaedalus(int number);
int BuildNSendPMDoesNotAcceptPositionAlertToAllDaedalus(char *symbol, double price, int share, char side, int account);
int BuildNSendPMHasntExitPositionAlertToAllDaedalus(char *symbol, double price, int share, char side, int account);
int BuildNSendPMReleasePositionAlertToAllDaedalus(char *symbol, double price, int share, char side, int account);
int BuildNSendServerRoleConfigurationMisConfiguredAlertToAllDaedalus(char * servers);
int BuildNSendSingleServerTradesAlertToAllDaedalus(char *server);
int BuildNSendStuckNearingLimitAlertToAllDaedalus();
int BuildNSendTradingDisabledAlertToAllDaedalus(char *reason);
int BuildNSendUnhandledOpenPositionWithoutAnOrderAlertToDaedalus(char *symbol, int share[MAX_ACCOUNT], char side[MAX_ACCOUNT], int account[MAX_ACCOUNT], int count);
int BuildNSendDisabledSymbolAlertToAllDaedalus(char *reason);

//------------------------------------------------------------------------------
void *ThreadReceiveMsgsFromDaedalus(void *pConnection);
int ReceiveDaedalusMessage(t_ConnectionStatus *connection, void *message);
int ProcessWebsocketHandshake(const void *message, int ttIndex);

int ProcessASConnectToServerFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex);

int ProcessConnectMdcFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex);
int ProcessConnectMdcOfTSFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex, int tsIndex);
int ProcessConnectMdcOfPMFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex);

int ProcessConnectOecFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex);
int ProcessConnectOecOfTSFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex, int tsIndex);
int ProcessConnectOecOfASFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex);
int ProcessConnectOecOfPMFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex);

int ProcessGetStockStatusFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex);
int ProcessGetStockStatusOfTSFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex, int tsIndex);
int ProcessGetStockStatusOfPMFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex);

int ProcessEnableISOTradingFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex);

int ProcessIgnoreSymbolFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex);
int ProcessIgnoreSymbolForTSFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex);
int ProcessIgnoreSymbolForPMFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex);

int ProcessIgnoreSymbolRangeFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg);

int ProcessSetHaltedSymbolFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex);

int ProcessSetMdcStateFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex);
int ProcessSetMdcStateForTSFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex, int tsIndex);
int ProcessSetMdcStateForPMFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex);

int ProcessClearBookFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex);
int ProcessClearBookForTSFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex, int tsIndex);
int ProcessClearBookForPMFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex);

int ProcessSetVolatileSymbolFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex);
int ProcessSetVolatileSymbolBySessionFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, t_ConnectionStatus *connection);
int ProcessSetVolatileSymbolByToggleFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex);

int ProcessSetAlertConfigFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex);

int ProcessChangeTraderStateFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex);

int ProcessSetExchangeConfigFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex);
int ProcessSetExchangeConfigForTSFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex, int tsIndex);
int ProcessSetExchangeConfigForPMFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex);

int ProcessSendManualOrderFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex);
int ProcessSendCancelOrderFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex);
int ProcessResponseSendNewOrderFromDaedalus(t_SendNewOrder *newOrder, int sendId, int reason, int ttIndex, char *reasonText);
int ProcessResponseSendNewOrderFromClientTool(t_SendNewOrder *newOrder, int sendId, int reason, int ttIndex, char *reasonText);
int ProcessSendCancelRequestToTS(int clOrdId, char *ECNOrderId, int tsId);

int ProcessEngagePositionFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex);
int ProcessDisengagePositionFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex);

int ProcessResetCurrentStatusFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex);

int ProcessCheckVersionFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex);
int ProcessEtbListRequestFromDaedalus();
int ProcessEmergencyStopRequestFromDaedalus();

//------------------------------------------------------------------------------

int SendDaedalusMessage(void *pConnection, void *message);
int SendMessageToAllDaedalus(t_TTMessage *ttMessage);

int SendPMOrderConfToAllDaedalus(int ECNIndex);
int SendTSOrderConfToAllDaedalus(int tsIndex, int ECNIndex);

int ProcessUpdatetASCurrentStatus(void);

int UpdateRejectMgmt(int ECNType, const char* rejectCode, const char* rejectText);

short GetTraderIndex(char *name);
int GetTraderName(short index, char *name);

/***************************************************************************/

#endif
