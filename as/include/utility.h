/*****************************************************************************
**  Project:    Equity Arbitrage Application
**  Filename:   utility.h
**  Description:  This file contains function definitions that were declared
          in utility.c  
**  Author:     Luan Vo-Kinh
**  First created:  17-Sep-2007
**  Last updated: -----------
*****************************************************************************/

#ifndef __UTILITY_H__
#define __UTILITY_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include "global_definition.h"

#define TraceLog(LVL, FMT, PARMS...)                              \
  do {                                                 \
    LrcPrintfLogFormat(LVL, FMT, ## PARMS); }  \
  while (0)


#define LRC_SECS_PER_MINUTE ((time_t)60)
#define LRC_SECS_PER_HOUR ((time_t)(60 * LRC_SECS_PER_MINUTE))
#define LRC_SECS_PER_DAY  ((time_t)(24 * LRC_SECS_PER_HOUR))
#define LRC_SECS_PER_YEAR ((time_t)(365 * LRC_SECS_PER_DAY))
#define LRC_SECS_PER_LEAP ((time_t)(LRC_SECS_PER_YEAR + LRC_SECS_PER_DAY))

#define Lrc_print_char(a) (a == 0 ? ' ': a)

#define NO_LOG_LEVEL  -1
#define DEBUG_LEVEL   1
#define NOTE_LEVEL    2
#define WARN_LEVEL    3
#define ERROR_LEVEL   4
#define FATAL_LEVEL   5
#define USAGE_LEVEL   6
#define STATS_LEVEL   7

#define DEBUG_STR   "debug: "
#define NOTE_STR    "note: "
#define WARN_STR    "warn: "
#define ERROR_STR   "error: "
#define FATAL_STR   "fatal: "
#define USAGE_STR   "usage: "
#define STATS_STR   "stats: "

/****************************************************************************
** Data structure definitions
****************************************************************************/

/****************************************************************************
** Global variable definition
****************************************************************************/
extern int Secs_in_years;
extern int Current_year;

extern char globalDateStringOfData[MAX_LINE_LEN];
extern char globalDateStringOfDatabase[MAX_LINE_LEN];
extern char Nyse_CCG_timestamp[8];

/****************************************************************************
** Function declarations
****************************************************************************/
char *GetFullConfigPath(const char *fileName, int pathType, char *fullConfigPath);

char *GetFullRawDataPath(const char *fileName, char *fullDataPath);

int CreateDataPathCurrentDate(void);

int CreateDataPathAtDate(int numSeconds);

char GetByteNumber(const unsigned char *buffer);

char GetByteNumberV1(const unsigned char *buffer, int *index);

int PutByteNumberV1(const unsigned char num, unsigned char *buffer, int *index);

char GetByteNumberBigEndianV1(const unsigned char *buffer, int *index);

int PutByteNumberBigEndianV1(const unsigned char num, unsigned char *buffer, int *index);

int GetShortNumberV1(const unsigned char *buffer, int *index);

int PutShortNumberV1(const short num, unsigned char *buffer, int *index);

int GetShortNumberBigEndianV1(const unsigned char *buffer, int *index);

int GetIntNumberV1(const unsigned char *buffer, int *index);

int GetIntNumberBigEndianV1(const unsigned char *buffer, int *index);

int PutIntNumberBigEndianV1(const int num, unsigned char *buffer, int *index);

long GetLongNumberV1(const char *buffer, int *index);

int PutLongNumberV1(const long num, unsigned char *buffer, int *index);

long GetLongNumberBigEndianV1(const unsigned char *buffer, int *index);

int PutLongNumberBigEndianV1(const long num, unsigned char *buffer, int *index);

int GetStringV1(char *dst, const unsigned char *src, int len, int *index);

int GetStringVariablelenV1(char *dst, const unsigned char *src, int *index);

int PutStringV1(const char *src, unsigned char *dst, int len, int *index);

int PutStringVariableLenV1(const char *src, unsigned char *dst, int *index);

int GetShortNumber(const unsigned char *buffer);

int GetShortNumberBigEndian(const unsigned char *buffer);

int GetIntNumber(const unsigned char *buffer);

int PutIntNumberV1(const int num, unsigned char *buffer, int *index);

int GetIntNumberBigEndian(const unsigned char *buffer);

long GetLongNumber(const char *buffer);

long GetLongNumberBigEndian(const char *buffer);

double GetDoubleNumber(const unsigned char *buffer);

double GetDoubleNumberV1(const unsigned char *buffer, int *index);

int PutDoubleNumberV1(const double num, unsigned char *buffer, int *index);

double GetDoubleNumberBigEndian(const unsigned char *buffer);

double GetDoubleNumberBigEndianV1(const unsigned char *buffer, int *index);

int PutDoubleNumberBigEndianV1(const double num, unsigned char *buffer, int *index);

char *RemoveCharacters(char *buffer, int bufferLen);

int Lrc_itoa(int value, char *strResult);
inline int Lrc_atoi(const char * buffer, int len);

int Lrc_itoaf(int value, const char pad, const int len, char *strResult);

void LrcPrintfLogFormat(int level, char *fmt, ...);
void PrintErrStr(int _level, const char *_str, int _errno);

void AddDataBlockTime(unsigned char *formattedTime);

char *GetLocalTimeString(char *timeString);

struct tm *Lrc_gmtime (time_t *tp, int years, int secs_in_years);

int Lrc_is_leap(int year);

int Get_secs_in_years(time_t *tp, int *year, int *secs_in_years);

void TrimRight( char * buffer, int len );

char Lrc_Split(const char *str, char c, void *parameter);

char *GetDateStringFormEpochSecs(int numberOfSeconds, char *dateString);

int GetNumberOfSecondsOfLocalTime(void);

int GetNumberOfSecondsFromTimestamp(const char *timestamp);

long GetNanoSecondsFromData(const char *timestamp);

int GetNumberOfSecondFromTimeString(const char *timeString);

int GetTimeInSecondsFromFile(char fullPath[MAX_PATH_LEN], int *numSeconds, char wantedString[MAX_LINE_LEN]);

int GetNumberOfSecondsFrom1970(const char *format, const char *dateString);

long GetNumberOfNanoSecondsSinceUnixEpoch();

char *ReplaceCharacters(char *buffer);

char *GetCurrentDateString(char *dateString);

char *GetTimeStampString(char *timeString);

int MappingStockSymbol(char *stockSymbol);

long ConvertToEpochTime(const char *timeStamp);
char *ConvertToEpochTimeString(const char *timeStamp, char *epochTimeString);

char *ConvertSpecialCharacter(char *text, char *result);

char *Convert_ClOrdID_To_NYSE_Format(int idOrder, char *strResult);

int IsFileTimestampToday(char *fullPath);

int GetCurrentVolatileSession();

void CheckToDeleteOldVolatileFiles();

int ExecuteCommand(const char *command, const int timeout);

void InitializeDecodeTable();
int DecodeBase64(const char encodedData[MAX_PATH_LEN], int inputLength, unsigned char decodedData[MAX_PATH_LEN]);

static const double EPSILON = 1.0e-06;
static inline int feq(double a, double b) {   return __builtin_fabs(a-b) < EPSILON; }
static inline int fgt(double a, double b) {   return a - b >  EPSILON; }
static inline int fge(double a, double b) {   return a - b > -EPSILON; }
static inline int flt(double a, double b) {   return a - b < -EPSILON; }
static inline int fle(double a, double b) {   return a - b <  EPSILON; }

#endif
