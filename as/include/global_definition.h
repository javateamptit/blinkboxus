/*****************************************************************************
**  Project:    Equity Arbitrage Application
**  Filename:   global_definition.h
**  Description:  This file contains some global definitions
**  Author:     Luan Vo-Kinh
**  First created:  15-Sep-2007
**  Last updated: -----------
*****************************************************************************/
#ifndef __GLOBAL_DEFINITION_H__
#define __GLOBAL_DEFINITION_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>

//#define _XOPEN_SOURCE

#include <pthread.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#include <sys/stat.h>

#define VERSION "20140605"

#define VOLATILE_SYMBOL_FILE_NAME           "volatile.txt"
#define VOLATILE_SECTION_FILE_NAME          "volatile_sections.txt"
#define ALL_VOLATILE_SCHEDULE_FILE_NAME     "all_volatile_toggle.txt"

// Volatile session
#define VOLATILE_SESSION_PRE                1
#define VOLATILE_SESSION_POST               2
#define VOLATILE_SESSION_INTRADAY           0

// For Server Name
#define AS_SERVER_NAME                      "carblink5"
#define PM_SERVER_NAME                      "carblink2"

// For MPID
#define MPID_ARCA_DIRECT                    "HBIFA"
#define MPID_NASDAQ_RASH                    "HBIF"

// Return values
#define ERROR_BUFFER_OVER   -2
#define ERROR               -1
#define SUCCESS              0

// Status valuse
#define CONNECT              1
#define DISCONNECT           0

#define MIN_PORT           1024
#define MAX_PORT          65535

// Path types
#define CONF_ORDER          0
#define CONF_SYMBOL         1
#define CONF_MISC           2


// TT send delay in microseconds
#define TT_SEND_DELAY 500000

// Trader tool recceive timeout
#define TRADER_TOOL_RECV_TIMEOUT 15

// Maximum message length for TS
#define TS_MAX_MSG_LEN 2000000

// Maximum message length for TT
#define TT_MAX_MSG_LEN 4000000

// Message header length
#define MSG_HEADER_LEN 6

// Daedalus message header length
#define DAEDALUS_MSG_HEADER_LEN 2 // 2 bytes of header

// Message body length index
#define MSG_BODY_LEN_INDEX 2

// Maximum length of path
#define MAX_PATH_LEN  256
#define MAX_LINE_LEN   80

#define MAX_OEC_SESSION_ID_LEN 8

// maximum bytes of timestamp
#define MAX_TIMESTAMP     28
#define TIMESTAMP_LENGTH  26
#define TIME_LENGTH       15

// Define some special characters
#define USED_CHAR_CODE 32

// Null pointer
#define NULL_POINTER 0

#define MAX_TRADE_SERVER_CONNECTIONS   9
#define MAX_TRADER_TOOL_CONNECTIONS   10

// Maximum entries for raw data of each ECN Order
#define MAX_RAW_DATA_ENTRIES      60000

// Maximum entries for query data of database
#define MAX_QUERY_DATA_ENTRIES    1500000

// Maximum orders placement
#define MAX_ORDER_PLACEMENT        250000

//Mapping between Client OrderID and Original Client OrderID
#define MAX_NYSE_ORDER_ID_MAPPING   10000

// Maximum raw data entries
#define MAX_RAW_DATA_TOTAL_ENTRIES 750000

// Maximum order detail
#define MAX_ORDER_DETAIL 250

// Maximum manual order
#define MAX_MANUAL_ORDER 300

// Maximum server allow to place manual order
#define MAX_SERVER_TO_PLACE_MANUAL_ORDER 2 // Rash and Arca Direct

// Maximum broken trade
#define MAX_BROKEN_TRADE 10000

// For broken trade
#define BROKEN_TRADE_DELETE -1
#define BROKEN_TRADE_NONE 0
#define BROKEN_TRADE_ADD 1

// AS centralized order types
#define MAX_ORDER_TYPE     10
#define TYPE_BASE         100
#define TYPE_ARCA_DIRECT  100
#define TYPE_NASDAQ_OUCH  101
#define TYPE_NASDAQ_RASH  102
#define TYPE_NYSE_CCG     103
#define TYPE_BZX_BOE      104
#define TYPE_EDGX_DIRECT  105
#define TYPE_EDGA_DIRECT  106
#define TYPE_NASDAQ_OUBX  107
#define TYPE_BYX_BOE      108
#define TYPE_PSX          109

// Maximum connections for ECN Order
#define TS_ARCA_DIRECT_INDEX          0
#define TS_NASDAQ_OUCH_INDEX          1
#define TS_NYSE_CCG_INDEX             2
#define TS_NASDAQ_RASH_INDEX          3
#define TS_BATSZ_BOE_INDEX            4
#define TS_EDGX_DIRECT_INDEX          5
#define TS_EDGA_DIRECT_INDEX          6
#define TS_NDAQ_OUBX_INDEX            7
#define TS_BYX_BOE_INDEX              8
#define TS_PSX_OUCH_INDEX             9
#define TS_MAX_ORDER_CONNECTIONS     10

#define TS_ARCA_BOOK_INDEX            0
#define TS_NASDAQ_BOOK_INDEX          1
#define TS_NYSE_BOOK_INDEX            2
#define TS_BATSZ_BOOK_INDEX           3
#define TS_EDGX_BOOK_INDEX            4
#define TS_EDGA_BOOK_INDEX            5
#define TS_NDBX_BOOK_INDEX            6
#define TS_BYX_BOOK_INDEX             7
#define TS_PSX_BOOK_INDEX             8
#define TS_AMEX_BOOK_INDEX            9
#define TS_MAX_BOOK_CONNECTIONS       10

#define ASK_SIDE                      0
#define BID_SIDE                      1

#define AS_ARCA_DIRECT_INDEX          0
#define AS_NASDAQ_RASH_INDEX          1
#define AS_MAX_ORDER_CONNECTIONS      2

// Maximum connections for ECN Book
#define PM_ARCA_BOOK_INDEX            0
#define PM_NASDAQ_BOOK_INDEX          1
#define PM_MAX_BOOK_CONNECTIONS       2

#define PM_ARCA_DIRECT_INDEX          0
#define PM_NASDAQ_RASH_INDEX          1
#define PM_MAX_ORDER_CONNECTIONS      2

// Manual Order and Automatic Order
#define MANUAL_ORDER                 -1
#define MANUAL_ORDER_MAKING       -9876
//#define AUTOMATIC_ORDER 0

/* t_Datablock constant */
#define DATABLOCK_MAX_MSG_LEN                512 - 48   // Maximum length of rawdata (message) from ECN Orders
#define DATABLOCK_ADDITIONAL_INFO_LEN        40             // Const length of additional information

#define DATABLOCK_OFFSET_FILTER_TIME         0 // 8 bytes: 0 -> 7
#define DATABLOCK_OFFSET_APP_TIME            8 // 8 bytes: 8 -> 15
#define DATABLOCK_OFFSET_VISIBLE_SIZE        16 // 4 bytes: 16 -> 19
#define DATABLOCK_OFFSET_LAUNCH              29 // 1 byte: 29
#define DATABLOCK_OFFSET_CROSS_ID            30 // 4 bytes: 30 -> 33
#define DATABLOCK_OFFSET_ACCOUNT_ID          35 // 1 byte: 35
#define DATABLOCK_OFFSET_BBO_ID              36 // 2 bytes: 36 -> 37
#define DATABLOCK_OFFSET_ENTRY_EXIT_REF      38 // 2 bytes: 38 -> 39

// Maximum bytes of output log
#define MAX_OUTPUT_LOG_BIN_LEN              64
#define MAX_OUTPUT_LOG_TEXT_LEN             96

#define BUY_TO_CLOSE  0
#define BUY_TO_OPEN   1

#define EXIT_VALUE    0
#define ARCA_ASK      1
#define ARCA_BID      2
#define NDAQ_ASK      3
#define NDAQ_BID      4
#define BATSZ_ASK     7
#define BATSZ_BID     8
#define NYSE_ASK      9
#define NYSE_BID      10
#define EDGX_ASK      11
#define EDGX_BID      12
#define EDGA_ASK      13
#define EDGA_BID      14
#define NDBX_ASK      15
#define NDBX_BID      16
#define BYX_ASK       17
#define BYX_BID       18
#define PSX_ASK       19
#define PSX_BID       20
#define AMEX_ASK      21
#define AMEX_BID      22

#define NORMALLY_TRADE          0
//#define SIMULTANEOUS_TRADE    1

// Legnth of symbol
#define SYMBOL_LEN              8
#define SYMBOL_LEN_PLUS_ONE     9

#define MAX_STOCK_SYMBOL         15000
#define SYMBOL_FOR_NEW_ORDER         0
#define SYMBOL_FOR_DETECT_CROSS      1

#define MAX_ARCA_MULTICAST_GROUP               4
#define MAX_NASDAQ_MULTICAST_GROUP             1
#define MAX_BATSZ_MULTICAST_GROUP             32
#define MAX_BYX_MULTICAST_GROUP               32
#define MAX_EDGX_MULTICAST_GROUP              10
#define MAX_CQS_MULTICAST_GROUP               24
#define MAX_UQDF_MULTICAST_GROUP               6
#define MAX_CTS_MULTICAST_GROUP               26
#define MAX_UTDF_MULTICAST_GROUP               6
#define MAX_NYSE_OPEN_ULTRA_MULTICAST_GROUP   20

// Reset current status index
#define NUM_STUCK_INDEX       0
#define BUYING_POWER_INDEX    1
#define NUM_LOSER_INDEX       2

#define MAX_PARAMETER       100
#define MAX_LEN_PARAMETER    80

#define YES             1
#define NO              0

#define DISABLE         0
#define ENABLE          1

#define DISCONNECTED    0 
#define CONNECTED       1

#define UPDATED         1
#define NOT_YET         0

#define TRADING_NORMAL  0
#define TRADING_HALTED  1

#define AS_MODULE       0
#define PM_MODULE       1
#define TT_MODULE       2

#define UPDATE_SOURCE_ID                0
#define UPDATE_RETRANSMISSION_REQUEST   1

//Trading time 7h -> 20h, step point = 90 minutes
// Points: 8h30, 10h00, 11h30, 13h00, 14h30, 16h00, 17h30, 19h00
#define RAW_DATA_TYPE       0
#define TRADE_OUTPUT_TYPE   1
#define ALL_DATA_TYPE       2
#define MAX_TYPE_DATA       3

#define STEP_POINT          90 * 60
#define MAX_POINT           (20 * 60 - 7 * 60) / STEP_POINT

#define MAX_RAW_DATA_TABLE     12
#define NEW_ORDERS_TABLE        0
#define ORDER_REJECTS_TABLE     1
#define ORDER_ACKS_TABLE        2
#define FILLS_TABLE             3
#define CANCELS_TABLE           4
#define CANCEL_REQUESTS_TABLE   5
#define CANCEL_ACKS_TABLE       6
#define CANCEL_REJECTS_TABLE    7
#define CANCEL_PENDING_TABLE    8
#define BROKEN_TRADES_TABLE     9

#define MAX_TRADE_OUTPUT_TABLE  4
#define RAW_CROSSES_TABLE       0
#define ASK_BIDS_TABLE          1
#define TRADE_RESULTS_TABLE     2

// CQS and UQDF
#define CQS_SOURCE              0
#define UQDF_SOURCE             1
#define MAX_BBO_SOURCE          2

#define MAX_EXCHANGE           16

#define PRE_MARKET_END_HOUR     9
#define PRE_MARKET_END_MIN     30
#define INTRADAY_END_HOUR      16

#define PRE_MARKET              0
#define INTRADAY                1
#define POST_MARKET             2
#define MAX_MARKET_TIME_TYPE    3

#define MAX_TIMELINE_INTRADAY_OF_MIN_SPREAD 10

//For Fast Market Mode management
#define NORMAL_MARKET_MODE      0
#define FAST_MARKET_MODE        1

//For Engaging
#define NO_MATCHING_POSITION            0
#define REQUESTED_SUCCESSFULLY          1
#define CAN_NOT_RESUME_HANDLING_STUCK   2
#define BEGIN_RESUME_HANDLING_STUCK     3

//Define activity type
#define ACTIVITY_TYPE_CONNECT_TO_AS                   1
#define ACTIVITY_TYPE_SWITCH_ROLE                     2
#define ACTIVITY_TYPE_DISCONNECT_FROM_AS              3
#define ACTIVITY_TYPE_MANUAL_ORDER                    4
#define ACTIVITY_TYPE_IGNORE_STOCK_LIST               5
#define ACTIVITY_TYPE_HALTED_STOCK_LIST               6
#define ACTIVITY_TYPE_PM_ENGAGE_DISENGAGE             7
#define ACTIVITY_TYPE_BUYING_POWER_SETTING            8
#define ACTIVITY_TYPE_ENABLE_DISENABLE_TRADING        9
#define ACTIVITY_TYPE_IGNORE_STOCK_LIST_PM            10
#define ACTIVITY_TYPE_CONNECT_DISCONNECT_BOOK         11
#define ACTIVITY_TYPE_CONNECT_DISCONNECT_ORDER        12
#define ACTIVITY_TYPE_CONNECT_DISCONNECT_BBO          13
#define ACTIVITY_TYPE_MAX_CONSECUTIVE_LOSS_SETTING    14
#define ACTIVITY_TYPE_MAX_STUCKS_SETTING              15
#define ACTIVITY_TYPE_MIN_SPREAD_TS_SETTING           16
#define ACTIVITY_TYPE_MAX_LOSER_TS_SETTING            17
#define ACTIVITY_TYPE_MAX_BUYING_POWER_SETTING        18
#define ACTIVITY_TYPE_MAX_SHARES_SETTING              19
#define ACTIVITY_TYPE_ENABLE_DISENABLE_ISO_TRADING    20
#define ACTIVITY_TYPE_MAX_CROSS_PER_MIN_SETTING       21
#define ACTIVITY_TYPE_AS_VOLATILE_THRESHOLD_SETTING   22
#define ACTIVITY_TYPE_TS_VOLATILE_THRESHOLD_SETTING   23
#define ACTIVITY_TYPE_AS_DISABLED_THRESHOLD_SETTING   24
#define ACTIVITY_TYPE_TS_DISABLED_THRESHOLD_SETTING   25

//For User Login Info
#define OFFLINE           0
#define ACTIVE            1
#define ACTIVE_ASSISTANT  2
#define MONITORING        3

#define USER_PASSWORD_INCORRECT -2
#define USER_LOGGED             -3
#define MAX_TRADER                50
#define MAX_TRADER_LOG_LEN      512

//For Authentication Method
#define LDAP_AUTHENTICATION_METHOD        0
#define NON_LDAP_AUTHENTICATION           1
#define UNDEFINE_AUTHENTICATION_METHOD   -1

//For client tool type
#define CLIENT_TOOL_DAEDALUS        0
#define CLIENT_TOOL_TRADER_TOOL     1
#define CLIENT_TOOL_UNDEFINED      -1

//For query database management
#define INSERT_NEW_ORDER        1
#define INSERT_ORDER_ACCEPTED   2
#define INSERT_ORDER_EXECUTED   3
#define INSERT_ORDER_CANCELED   4
#define INSERT_CANCEL_REQUEST   5
#define INSERT_ORDER_REJECTED   6
#define INSERT_BUST_OR_CORRECT  7
#define INSERT_CANCEL_PENDING   8
#define INSERT_CANCEL_ACK       9
#define INSERT_CANCEL_REJECTED  10

#define INSERT_BBO_QUOTE_INFO                     11
#define INSERT_SHORT_SELL_FAILED                  12
#define INSERT_OUTPUT_LOG_TRADE                   13
#define INSERT_TRADE_RESULT                       14
#define INSERT_OUTPUT_LOG_DETECTED_TRADE          15
#define INSERT_OUTPUT_LOG_DETECTED                16
#define INSERT_OUTPUT_LOG_NOT_YET_DETECT_TRADE    17
#define INSERT_OUTPUT_LOG_NOT_YET_DETECT          18
#define INSERT_OUTPUT_LOG_ASK_BID                 19
#define INSERT_OUTPUT_LOG                         20
#define UPDATE_REAL_VALUE                         21

//Trading account related
#define FIRST_ACCOUNT   0
#define SECOND_ACCOUNT  1
#define THIRD_ACCOUNT   2
#define FOURTH_ACCOUNT  3

#define FIRST_ACCOUNT_ASCII   '1'
#define SECOND_ACCOUNT_ASCII  '2'
#define THIRD_ACCOUNT_ASCII   '3'
#define FOURTH_ACCOUNT_ASCII  '4'
#define MAX_ACCOUNT             4

//Stuck locking (must be same as TS!!!)
#define NO_LOCK       0
#define SHORT_LOCK    1
#define LONG_LOCK    -1

// Raw data files extension
#define RAW_DATA_FILE_EXTENSION_DIRECT      "arcalog"
#define RAW_DATA_FILE_EXTENSION_FIX         "fixlog"
#define RAW_DATA_FILE_EXTENSION_OUCH        "ouchlog"
#define RAW_DATA_FILE_EXTENSION_RASH        "rashlog"
#define RAW_DATA_FILE_EXTENSION_NYSE        "nyselog"
#define RAW_DATA_FILE_EXTENSION_BATSZ       "batszlog"
#define RAW_DATA_FILE_EXTENSION_EDGX        "edgxlog"
#define RAW_DATA_FILE_EXTENSION_EDGA        "edgalog"
#define RAW_DATA_FILE_EXTENSION_OUBX        "bxlog"
#define RAW_DATA_FILE_EXTENSION_BYX         "byxlog"
#define RAW_DATA_FILE_EXTENSION_PSX         "psxlog"
#define RAW_DATA_FILE_PREFIX_FIX            "oec_arcafix"
#define RAW_DATA_FILE_PREFIX_DIRECT         "oec_arcadirect"
#define RAW_DATA_FILE_PREFIX_RASH           "oec_rash"
#define RAW_DATA_FILE_PREFIX_OUCH           "oec_ouch"
#define RAW_DATA_FILE_PREFIX_NYSE           "oec_nyse"
#define RAW_DATA_FILE_PREFIX_BATSZ          "oec_batsz"
#define RAW_DATA_FILE_PREFIX_BYX            "oec_byx"
#define RAW_DATA_FILE_PREFIX_EDGX           "oec_edgx"
#define RAW_DATA_FILE_PREFIX_EDGA           "oec_edga"
#define RAW_DATA_FILE_PREFIX_OUBX           "oec_bx"
#define RAW_DATA_FILE_PREFIX_PSX            "oec_psx"

#define FILE_LOSS_BY_SYMBOL                 "loss_by_symbol"

#define ARCA_SERVER_ROLE        0
#define NASDAQ_SERVER_ROLE      1
#define BATS_SERVER_ROLE        2
#define EDGX_SERVER_ROLE        3
#define STANDALONE_SERVER_ROLE  4
#define MAX_SERVER_ROLE         5

//Trading disabled reason code
#define TS_TRADING_DISABLED_BY_EMERGENCY_STOP                        -13
#define TS_TRADING_DISABLED_BY_NO_ACTIVE_OR_ACTIVE_ASSIST_TRADER     -12
#define TS_TRADING_DISABLED_BY_SCHEDULE                              -11
#define TS_TRADING_DISABLED_BY_MAX_CROSS_LIMIT                       -10 // Max Cross Per Minute Exceed
#define TS_TRADING_DISABLED_BY_WEDBUSH_REQUEST                        -9 // WedBush Risk Manager
#define TS_TRADING_DISABLED_BY_NO_TT_CONNECTION                       -8
#define TS_TRADING_DISABLED_BY_TWT_REQUEST                            -7
#define TS_TRADING_DISABLED_BY_BOOK_DISCONNECT                        -6
#define TS_TRADING_DISABLED_BY_ORDER_DISCONNECT                       -5
#define TS_TRADING_DISABLED_BY_EXCEED_MAX_STUCK_LIMIT                 -4
#define TS_TRADING_DISABLED_BY_EXCEED_MAX_LOSS_LIMIT                  -3
#define TS_TRADING_DISABLED_BY_EXCEED_MAX_STUCK_LIMIT_BY_AS_REQUEST   -2
#define TS_TRADING_DISABLED_BY_EXCEED_MAX_LOSS_LIMIT_BY_AS_REQUEST    -1

#define PM_TRADING_DISABLED_BY_EMERGENCY_STOP -6
#define PM_TRADING_DISABLED_BY_NO_ACTIVE_OR_ACTIVE_ASSIST_TRADER -5
#define PM_TRADING_DISABLED_BY_WEDBUSH_REQUEST        -4  // WedBush Risk Manager
#define PM_TRADING_DISABLED_BY_NO_TT_CONNECTION       -3
#define PM_TRADING_DISABLED_BY_BOOK_DISCONNECT        -2
#define PM_TRADING_DISABLED_BY_ORDER_DISCONNECT       -1

// Timer send delay in microseconds
#define TIMER_DELAY                       1000000
#define MAX_GTR_SAMPLE_IN_SECONDS             600   // 10 minutes
#define MAX_NO_TRADE_ALERT_MILESTONE           12
#define NUMBER_OF_SECONDS_PER_DAY           86400

// Hung order
#define ORDER_EDITOR_NONE       0
#define ORDER_EDITOR_ADD        1
#define ORDER_EDITOR_PROCESS    2

// Shortability Schedule
#define MAX_TIMELINE_OF_SHORTABILITY_SCHEDULE 16

// List identifier
#define NO_LIST             0
#define VOLATILE_LIST_ID    1
#define DISABLED_LIST_ID    2

// Account login
#define MAX_ACCOUNT_LOGIN_LEN 15

#define ADD_SYMBOL          0
#define REMOVE_SYMBOL       1

// Global Position Key
#define GLOBAL_POSITION_KEY "Global"

// Position State (for Daedalus, defined values by on enum PositionState)
#define POSITION_STATE_NORMAL   1
#define POSITION_STATE_HALT     4
#define POSITION_STATE_STUCK    6

/****************************************************************************
** Data structure definitions
****************************************************************************/
// This structure is used to contain configuration of Trader Info
// Be careful while updating this struct definition, it's used in memcpy()
typedef struct t_TraderStatusInfo
{
  char userName[MAX_LINE_LEN];
  int status;
  int id;
} t_TraderStatusInfo;

typedef struct t_TraderCollection
{
  int numberTrader;
  t_TraderStatusInfo traderStatusInfo[MAX_TRADER];
} t_TraderCollection;

// This structure is used to contain configuration of Trade Server
// Be careful while updating this struct definition, it's used in memcpy()
typedef struct t_TradeServerConf
{
  int tsId;
  char ip[MAX_LINE_LEN];
  int port;
  char description[MAX_LINE_LEN]; //server name
} t_TradeServerConf;

// This structure is to contain connection status
typedef struct t_ConnectionStatus
{
  int index;
  int socket;
  int statusUpdate;
  int currentState;
  char userName[MAX_LINE_LEN];
  char password[MAX_LINE_LEN];
  pthread_mutex_t socketMutex;
  char status;
  char authenticationMethod; //LDAP method: 0; Non-LDAP: 1
  char clientType;
  char preStatus;
} t_ConnectionStatus;

// This structure is used to contain connection information
// Be careful while updating this struct definition, it's used in memcpy()
typedef struct t_ConnectionInfo
{
  char ip[MAX_LINE_LEN];
  int port;
} t_ConnectionInfo;

// This structure is used to contain connection information
typedef struct t_ARCAConnectionInfo
{
  int index;
  char ip[MAX_LINE_LEN];
  int port;
} t_ARCAConnectionInfo;
// This structure is used to contain ARCA Book configuration
typedef struct t_ARCABookConf
{
  char sourceID[MAX_LINE_LEN];

  t_ARCAConnectionInfo retranConn[MAX_ARCA_MULTICAST_GROUP];
  char currentFeedName;
} t_ARCABookConf;

// This structure is used to contain NASDAQ Book configuration
typedef struct t_NASDAQBookConf
{
  t_ConnectionInfo UDP;
  t_ConnectionInfo TCP;
  int maxMsgLoss;
} t_NASDAQBookConf;

// This structure is used to contain NDBX Book configuration
typedef struct t_NDAQBXBookConf
{
  t_ConnectionInfo UDP;
} t_NDAQBXBookConf;

// This structure is used to contain BATS Book configuration
typedef struct t_BATSZBookConf
{
  t_ConnectionInfo primaryConn;
  t_ConnectionInfo retranConn;
} t_BATSZBookConf;

typedef struct t_BATSZBookGRPConf
{
  char sessionSubId[MAX_LINE_LEN];
  char userName[MAX_LINE_LEN];
  char password[MAX_LINE_LEN];
  t_ConnectionInfo retranRequestConn;
}t_BATSZBookGRPConf;

// This structure is used to contain EdgeX Book configuration
typedef struct t_EDGXBookConf
{
  t_ConnectionInfo primaryConn;
  t_ConnectionInfo retranConn;
} t_EDGXBookConf;

typedef struct t_EDGXBookGRPConf
{
  char userName[MAX_LINE_LEN];
  char password[MAX_LINE_LEN];
  char ip[MAX_LINE_LEN];
  int port;
}t_EDGXBookGRPConf;

// This structure is used to contain EdgeA Book configuration
typedef struct t_EDGABookConf
{
  t_ConnectionInfo primaryConn;
  t_ConnectionInfo retranConn;
} t_EDGABookConf;

typedef struct t_EDGABookGRPConf
{
  char userName[MAX_LINE_LEN];
  char password[MAX_LINE_LEN];
  char ip[MAX_LINE_LEN];
  int port;
}t_EDGABookGRPConf;

// This structure is used to contain connection information
typedef struct t_NYSEOpenUltraInfo
{
  int retranPort;
  int feedPort;
  char retranIP[MAX_LINE_LEN];
  char feedIP[MAX_LINE_LEN];
  char recoveryIP[MAX_LINE_LEN];
  char refreshIP[MAX_LINE_LEN];
  int recoveryPort;
  int refreshPort;
  int index;
  char _padding[4];
}t_NYSEOpenUltraInfo;

typedef struct t_NYSEBookConf
{
  char sourceID[MAX_LINE_LEN];
  t_NYSEOpenUltraInfo nyseBook[MAX_NYSE_OPEN_ULTRA_MULTICAST_GROUP];
} t_NYSEBookConf;

// This structure is used to contain ECN Books configuration
typedef struct t_ECNBooksConf
{
  t_ARCABookConf ARCABookConf;
  t_NASDAQBookConf NASDAQBookConf;
  t_NDAQBXBookConf NDAQBXBookConf;
  t_NDAQBXBookConf PSXBookConf;
  t_BATSZBookConf BATSZBookConf[MAX_BATSZ_MULTICAST_GROUP];
  t_BATSZBookConf BYXBookConf[MAX_BYX_MULTICAST_GROUP];
  t_BATSZBookGRPConf BATSZBookGRPConf;
  t_BATSZBookGRPConf BYXBookGRPConf;
  t_NYSEBookConf NYSEBookConf;
  t_EDGXBookConf EDGXBookConf[MAX_EDGX_MULTICAST_GROUP];
  t_EDGXBookGRPConf EDGXBookGRPConf;
  t_EDGABookConf EDGABookConf[MAX_EDGX_MULTICAST_GROUP];
  t_EDGABookGRPConf EDGABookGRPConf;
} t_ECNBooksConf;

// This structure is used to contain ECN Order configuration
// Be careful while updating this struct defition
// It's used in memcpy()
typedef struct t_ECNOrderConf
{
  char ip[MAX_LINE_LEN];
  int port;
  char username[MAX_LINE_LEN];
  char password[MAX_LINE_LEN];
} t_ECNOrderConf;

typedef struct t_SessionIdMgmt
{
  char tsSessionId[MAX_TRADE_SERVER_CONNECTIONS][TS_MAX_ORDER_CONNECTIONS][MAX_OEC_SESSION_ID_LEN];
  char pmSessionId[PM_MAX_ORDER_CONNECTIONS][MAX_OEC_SESSION_ID_LEN];
  char asSessionId[AS_MAX_ORDER_CONNECTIONS][MAX_OEC_SESSION_ID_LEN];
} t_SessionIdMgmt;

// This is used to contain global configuration
// Be careful while updating this structure definition
// It's used in memcpy()
typedef struct t_GlobalConf
{
  double maxBuyingPower;  
  int maxShares;  
  int maxConsecutiveLoser;
  int maxStuckPosition;
  int serverRole;
  double minSpread;
  double maxSpread;
  double secFee;
  double commission;
  double loserRate;
  double exRatio;
  
  // Nasdaq Auction period
  int nasdaqAuctionBeginTime;
  int nasdaqAuctionEndTime;
  
  short beginRegularSession;
  short endRegularSession;
} t_GlobalConf;

typedef struct t_SplitSymbolConf
{
  int rangeID;
  char startOfRange;
  char endOfRange;
} t_SplitSymbolConf;

typedef struct t_BackUpGlobalConf
{
  double maxBuyingPower;
  double minSpread;
  int maxShares;
  int maxConsecutiveLoser;
  int maxStuckPosition;
  int ISO_Trading;
} t_BackUpGlobalConf;

typedef struct t_Min_Spread_Switch
{
  int timeInSeconds;
  double minSpread;
} t_Min_Spread_Switch;

typedef struct t_Min_Spread_Intraday
{
  int countTimes;
  t_Min_Spread_Switch minSpreadList[MAX_TIMELINE_INTRADAY_OF_MIN_SPREAD];
} t_Min_Spread_Intraday;

typedef struct t_Min_Spread_Conf
{
  double preMarket;
  double postMarket;
  t_Min_Spread_Intraday intraday;
} t_Min_Spread_Conf;

// This structure is used to contain current status
// Be careful while updating this struct defition, it's used in memcpy()
typedef struct t_CurrentStatus
{
  double buyingPower;
  int numStuck;
  int numLoser;
  
  char tsMdcStatus[TS_MAX_BOOK_CONNECTIONS];
  char tsOecStatus[TS_MAX_ORDER_CONNECTIONS];
  char tsTradingStatus[TS_MAX_ORDER_CONNECTIONS];

  char statusCQS;
  char statusUQDF;
  char statusISOTrading;
  
  // TS does not send following fields
  char serverStatus;
  char isoTrading;
} t_CurrentStatus;

// This structure is used to contain TS setting
//Be careful while updating this struct defition, it's used in memcpy()
typedef struct t_TSSetting
{
  char bookEnable[TS_MAX_BOOK_CONNECTIONS];
  char orderEnable[TS_MAX_ORDER_CONNECTIONS];
  char bboEnable[MAX_BBO_SOURCE];
} t_TSSetting;

//Be careful while updating this struct defition, it's used in memcpy()
typedef struct t_OrderPlaceSummary
{
  int numberOfSent;
  int totalSharesFilled;
} t_OrderPlaceSummary;

// This structure is used to contain Trade Server information
typedef struct t_TradeServerInfo
{
  // Maximum connections
  int maxConnections;
  
  // Role of Books in the ISO algorithm
  char bookISO_Trading[MAX_TRADE_SERVER_CONNECTIONS][TS_MAX_BOOK_CONNECTIONS];
  
  // Keep status of Pre/Post ISO trading for each TS Book
  char bookPrePostISO_Trading[MAX_TRADE_SERVER_CONNECTIONS][TS_MAX_BOOK_CONNECTIONS];
  
  // Trade Server configuration
  t_TradeServerConf config[MAX_TRADE_SERVER_CONNECTIONS];
  
  // Trade Server setting
  t_TSSetting tsSetting[MAX_TRADE_SERVER_CONNECTIONS];
  
  // Trade Server connection status
  t_ConnectionStatus connection[MAX_TRADE_SERVER_CONNECTIONS];

  // ECN Books configuration
  t_ECNBooksConf ECNBooksConf[MAX_TRADE_SERVER_CONNECTIONS];

  // ECN Orders configuration
  t_ECNOrderConf ECNOrdersConf[MAX_TRADE_SERVER_CONNECTIONS][TS_MAX_ORDER_CONNECTIONS];

  // Global settings configuration
  t_GlobalConf globalConf[MAX_TRADE_SERVER_CONNECTIONS];
  
  t_Min_Spread_Conf minSpreadConf[MAX_TRADE_SERVER_CONNECTIONS];

  // Trade Server current status
  t_CurrentStatus currentStatus[MAX_TRADE_SERVER_CONNECTIONS];
  t_CurrentStatus preStatus[MAX_TRADE_SERVER_CONNECTIONS];

  // Order place summary
  t_OrderPlaceSummary orderPlaceSummary[MAX_TRADE_SERVER_CONNECTIONS];

  // Connect all
  int isConnectAll[MAX_TRADE_SERVER_CONNECTIONS];
  int ttIndexPerformedConnectAll[MAX_TRADE_SERVER_CONNECTIONS];
  
  // Split Symbol Config
  t_SplitSymbolConf splitConf[MAX_TRADE_SERVER_CONNECTIONS];
} t_TradeServerInfo;

typedef struct t_OutputLogProcessingStatus
{
  int numList[6]; //0 to 5  for 3 secs
  int iIndex;   //range: 0 to 5
  char cStatus; //0 for normal processing, 1 for partial processing
  char _padding[3];
} t_OutputLogProcessingStatus;

// This structure is used to contain PM current status
typedef struct t_PMCurrentStatus
{
  char tradingStatus;
  char pmMdcStatus[PM_MAX_BOOK_CONNECTIONS];
  char pmOecStatus[PM_MAX_ORDER_CONNECTIONS];

  char statusCTS_Feed;
  char statusCQS_Feed;
  char statusUQDF_Feed;
  char statusUTDF;
  
  char serverStatus;
} t_PMCurrentStatus;

// This structure is used to management global configuration of Position Manager
// Be careful while updating this struct defition, it's used in memcpy()
typedef struct t_PMGlobalConf
{
  // Auto spread
  double autoSpread;
  
  // Maximum spread
  double maxSpread;

  // Maximum loser spread
  double maxLoserSpread;

  // Profit spread
  double profitSpread;

  // Maximum profit spread
  double maxProfitSpread;

  // Big loser amount
  double bigLoserAmount;

  // Big loser spread
  double bigLoserSpread;

  // Maximum big loser spread
  double maxBigLoserSpread;
  
  // fast market max spread
  double fm_maxSpread;
  
  // fast market loser spread
  double fm_maxLoserSpread;
  
  // Fast Market max big loser spread
  double fm_maxBigLoserSpread;
  
  //High Execution Rate
  int highExecutionRate;
  
  //Moving Average Period
  int movingAveragePeriod;
  
} t_PMGlobalConf;

// This structure is used to contain PM information
typedef struct t_PMInfo
{
  // PM configuration
  t_ConnectionInfo config;

  // PM connection status
  t_ConnectionStatus connection;

  // ECN Books configuration
  t_ECNBooksConf ECNBooksConf;

  // ECN Orders configuration
  t_ECNOrderConf ECNOrdersConf[PM_MAX_ORDER_CONNECTIONS];

  // Global settings configuration
  t_PMGlobalConf globalConf;

  // Trade Server current status
  t_PMCurrentStatus currentStatus;

  t_PMCurrentStatus preStatus;
} t_PMInfo;

// This structure is used to contain configuration of Trader Tool
// Be careful while updating this struct definition
// It's used in memcpy()
typedef struct t_TraderToolConf
{ 
  int port;
  int maxConnections;
} t_TraderToolConf;

// This structure is used to management global configuration of Aggregation Server
typedef struct t_ASGlobalConf
{
  // Maximum stuck positions
  int maxStuck;
  
  // Maximum consecutive losers
  int maxLoser;

  // Max crosses per minute per TS
  int maxCross;

  // Padding 4 bytes
  int __padding;

  // AS Volatile threshold. Symbol is volatile if it loses >= this
  double asVolatileThreshold;

  // AS Disabled threshold. Symbol is disabled if it loses >= this
  double asDisabledThreshold;
    
    // TS Volatile threshold. Symbol is volatile if it loses >= this
  double tsVolatileThreshold;

  // TS Disabled threshold. Symbol is disabled if it loses >= this
  double tsDisabledThreshold;

  // Current buying power
  double buyingPower[MAX_ACCOUNT];
} t_ASGlobalConf;

// This structure is used to management current status of Aggregation Server
// Be careful while updating this struct definition
// It's used in memcpy()
typedef struct t_ASCurrentStatus
{ 
  // Current buying power
  double buyingPower[MAX_ACCOUNT];

  // Current stuck positions
  int numStuck;
  
  // Current consecutive losers
  int numLoser;

  int numberOfSent;
  int totalSharesFilled;
  
  char serverStatus;
} t_ASCurrentStatus;

// This structure is to contain Trader Tool information
typedef struct t_TraderToolInfo
{
  // Trader Tool configuration
  t_TraderToolConf config;

  // Trader Tool connection status
  t_ConnectionStatus connection[MAX_TRADER_TOOL_CONNECTIONS];

  // ECN Orders connection status
  t_ConnectionStatus orderConnection[AS_MAX_ORDER_CONNECTIONS];

  // ECN global configuration
  t_ASGlobalConf globalConf;

  // Trade Server current status
  t_ASCurrentStatus currentStatus;
  t_ASCurrentStatus preStatus;
} t_TraderToolInfo;

// This structure is used to contain a message from Trade Server
typedef struct t_TSMessage
{
  int msgLen;
  unsigned char msgContent[TS_MAX_MSG_LEN];
} t_TSMessage;

// This structure is used to contain a message to Trader Tool
typedef struct t_TTMessage
{
  int msgLen;
  unsigned char msgContent[TT_MAX_MSG_LEN];
} t_TTMessage;

// The structure is used to store sent/received messages to/from ECN Order servers
typedef struct t_DataBlock
{
  // Length of block data excluded block length
  int blockLen;
  
  // Length of message content
  int msgLen;
  
  // Content of message will be stored here
  unsigned char msgContent[DATABLOCK_MAX_MSG_LEN];
  
  // Content of additional information
  unsigned char addContent[DATABLOCK_ADDITIONAL_INFO_LEN];
} t_DataBlock;

// This structure is used to management raw data
typedef struct t_RawDataMgmt
{
  // File pointer contains file descriptor to raw data file
  FILE *fileDesc;

  // ECN Order index
  int orderIndex;

  // TS Id: AS = -1, TS = [0..15]
  int tsId;

  // Last update index
  int lastUpdatedIndex;

  // Count raw data block
  int countRawDataBlock;

  // File name is used stored raw data
  char fileName[MAX_LINE_LEN];
  
  // Check raw data loaded
  int isProcessed;
  int _padding;

  // Mutex lock
  pthread_mutex_t updateMutex;

  // All rawdata blocks will be stored at the collection
  t_DataBlock dataBlockCollection[MAX_RAW_DATA_ENTRIES];
} t_RawDataMgmt;

// This structure is used to management position raw data processed
typedef struct t_RawDataProcessedMgmt
{
  // File pointer contains file descriptor to raw data file
  FILE *fileDesc;

  // Position raw data blocks of Trade Server is processed
  int tsDataBlock[MAX_TRADE_SERVER_CONNECTIONS][TS_MAX_ORDER_CONNECTIONS];

  // Position raw data blocks of Aggregation Server has been processed
  int asDataBlock[AS_MAX_ORDER_CONNECTIONS];

  // Position raw data blocks of PM has been processed
  int pmDataBlock[PM_MAX_ORDER_CONNECTIONS];
} t_RawDataProcessedMgmt;

// This structure is used to contain ignore symbol list
typedef struct t_IgnoreStockList
{
  int countStock;
  char stockList[MAX_STOCK_SYMBOL][SYMBOL_LEN];
} t_IgnoreStockList;

// This structure is used to contain ignore symbol ranges list
typedef struct t_IgnoreStockRangesList
{
  int countStock;
  char stockList[MAX_STOCK_SYMBOL][SYMBOL_LEN];
} t_IgnoreStockRangesList;
//  This structure is used to contain
typedef struct t_Parameters
{
  char parameterList[MAX_PARAMETER][MAX_LEN_PARAMETER];
  int countParameters;
} t_Parameters;

// This structure is used to converter between Double and UNSIGNED CHAR
typedef union t_DoubleConverter
{
  double value;
  unsigned char c[8];
} t_DoubleConverter;

// This structure is used to converter between LONG and UNSIGNED CHAR
typedef union t_LongConverter
{
  long value;
  unsigned char c[8];
} t_LongConverter;

// This structure is used to converter between INT and UNSIGNED CHAR
typedef union t_IntConverter
{
  int value;
  unsigned char c[4];
} t_IntConverter;

// This structure is used to converter between SHORT and UNSIGNED CHAR
typedef union t_ShortConverter
{
  unsigned short value;
  unsigned char c[2];
} t_ShortConverter;

typedef struct t_TradeResultInfo
{
  int expectedShares;
  int boughtShares;
  double expectedPL;
  double totalBought;
  double spread;
  int soldShares;
  int leftStuckShares;
  double totalSold;
  double totalStuck;
  int as_cid;
  int ts_cid;
  int ts_id;
  int trade_id;
  
  char dateString[MAX_TIMESTAMP];
  int timestamp;
  
  char symbol[SYMBOL_LEN];
} t_TradeResultInfo;

typedef struct t_RawDataPoint
{
  int timestamp;
  int isUpdate;
  
  // Id of tables contain raw dta
  int idTables[MAX_RAW_DATA_TABLE];
  
  // Position raw data blocks of Trade Server has been processed
  int tsDataBlock[MAX_TRADE_SERVER_CONNECTIONS][TS_MAX_ORDER_CONNECTIONS];

  // Position raw data blocks of Aggregation Server has been processed
  int asDataBlock[AS_MAX_ORDER_CONNECTIONS];

  // Position raw data blocks of PM has been processed
  int pmDataBlock[PM_MAX_ORDER_CONNECTIONS];
} t_RawDataPoint;

typedef struct t_RawDataPointMgmt
{
  int currentPoint;
  t_RawDataPoint pointList[MAX_POINT];
  
  char fileName[MAX_LINE_LEN];
} t_RawDataPointMgmt;

// This structure is used to contain AS setting
// Be careful while updating this struct defition
// It's used in memcpy()
typedef struct t_ASSetting
{
  char orderEnable[AS_MAX_ORDER_CONNECTIONS];
  char description[MAX_LINE_LEN];
} t_ASSetting;

// This structure is used to contain PM setting
// Be careful while updating this struct defition
// It's used in memcpy()
typedef struct t_PMSetting
{
  char description[MAX_LINE_LEN];
  char bookEnable[PM_MAX_BOOK_CONNECTIONS];
  char orderEnable[PM_MAX_ORDER_CONNECTIONS];
  char CTS_Enable;
  char UTDF_Enable;
  char CQS_Enable;
  char UQDF_Enable;
  char isConnectAll;  //If this flag is YES, all connections in PM will be connected
} t_PMSetting;

typedef struct t_ExchangeStatus
{
  char visible;
  char enable;
} t_ExchangeStatus;

typedef struct t_ExchangeInfo
{
  char index;
  char exchangeId;
  t_ExchangeStatus status[MAX_BBO_SOURCE];
} t_ExchangeInfo;

//For PM Exchange Configuration
// Be careful while updating this struct defition
// It's used in memcpy()
typedef struct t_PMExchangeInfo
{
  char exchangeId;
  char ECNToPlace[MAX_LINE_LEN];
  t_ExchangeStatus status[MAX_BBO_SOURCE];
} t_PMExchangeInfo;

//For calculate the net profit/loss 
typedef struct t_FeeConf
{
  double secFee;
  double tafFee;
  double commission;
  double addVenueFeeWithPriceLager1;
  double addVenueFeeWithPriceSmaler1;
  double removeVenueFeeWithPriceLager1;
  double removeVenueFeeWithPriceSmaler1;
  double auctionVenueFee;
  double arcaRouteVenueFee;
  double nasdaqRouteVenueFee;
  double bzxRouteVenueFee;
  double edgxRouteVenueFee;
} t_FeeConf;

typedef struct t_NoTradeAlertItem
{
  int startInSecond;
  int durationInMinute;
} t_NoTradeAlertItem;

typedef struct t_NoTradeAlertMgmt
{
  int count;
  t_NoTradeAlertItem itemList[MAX_NO_TRADE_ALERT_MILESTONE];
} t_NoTradeAlertMgmt;

//Alerting config
typedef struct t_AlertConf
{
  int pmExitedPositionThreshhold;
  int consecutiveLossThreshhold; // counted from consecutive Loss Limit
  int stucksThreshhold;          // counted from stuck Limit
  int singleServerTradesThreshhold;
  double gtrAmountThreshhold;
  int gtrSecondThreshhold;
  int incompleteTradeThreshhold;
  int repetitionTimeThreshhold;
  int positionWithoutOrderThreshhold;
  int pmOrderRateThreshhold;
  double buyingPowerAmountThreshhold;
  int buyingPowerSecondsThreshhold;
  t_NoTradeAlertMgmt noTradeAlertMgmt;
} t_AlertConf;

typedef struct t_VersionInfo
{
  char version[16];
  char date[16];
  char description[MAX_LINE_LEN];
} t_VersionInfo;

typedef struct t_ModuleVersionMgmt
{
  t_VersionInfo ttVersion;
} t_ModuleVersionMgmt;

typedef struct t_ShortabilitySchedule
{
  int count;
  int timeInSeconds[MAX_TIMELINE_OF_SHORTABILITY_SCHEDULE];
} t_ShortabilitySchedule;

typedef struct t_SSLMgmt
{
  //ssEnable: 0 - can not short sell
  //      1 - be able to short sell
  char ssEnable[MAX_STOCK_SYMBOL]; //Stock Symbol Index is used the same as t_SymbolDetectCross
                  //so that, symbol can get from t_SymbolDetectCross
  int total;
} t_SSLMgmt;

typedef struct t_LdapInfo
{
  char url[MAX_LINE_LEN];
  int port;
  char userOU[MAX_LINE_LEN];
} t_LdapInfo;

/****************************************************************************
** Global variable definition
****************************************************************************/

extern double positionBuyingPower[MAX_ACCOUNT];
extern double tsBuyingPower[MAX_ACCOUNT];
extern t_OutputLogProcessingStatus OutputLogProcessingStatus[MAX_TRADE_SERVER_CONNECTIONS];

/****************************************************************************
** Function declarations
****************************************************************************/

#endif
