/*****************************************************************************
**  Project:    Equity Arbitrage Application
**  Filename:   arca_direct_data.h
**  Description:  This file contains some declaration for arca_direct_data.c
**  Author:     Luan Vo-Kinh
**  First created:  13-Sep-2007
**  Last updated: -----------
*****************************************************************************/

#ifndef __ARCA_DIRECT_DATA_H__
#define __ARCA_DIRECT_DATA_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include "global_definition.h"
#include "utility.h"
#include "position_manager_proc.h"

#include <stdlib.h>
#include <math.h>

/****************************************************************************
** Data structures definitions
****************************************************************************/

typedef struct t_ARCA_DIRECTNewOrder
{
  double price;

  int length;
  int seqNum;
  int orderID;
  int PCSLinkID;
  int shares;
  int maxFloor;
  int tsId;
  int crossId;
  int tradeType;
  int tradingAccount;
  int visibleShares;

  short bboId;
  unsigned short entryExitRefNum;

  char date[12];
  char timestamp[16];
  char msgType[16];
  char symbol[SYMBOL_LEN_PLUS_ONE];
  char compGroupID[6];
  char deliverCompID[6];
  char execIns;
  char side[3];
  char entryExit[6];
  char askBid[4];
  char ecnLaunch[6];
  char ecn[6];
  char ordType;
  char timeInForce[4];
  char rule80A;
  char tradingSessionId[6];
  char account[11];
  char isoFlag;
  char ExtExeInstruction;
  char ExtendedPNP;
  char ProactiveIfLocked;
  char status[16];

  char msgContent[MAX_ORDER_MSG];
} t_ARCA_DIRECTNewOrder;

typedef struct t_ARCA_DIRECTOrderACK
{
  double price;

  long sendingTime;
  long transTime;
  long arcaExOrderId;

  int length;
  int seqNum;
  int orderID;

  char date[12];
  char timestamp[16];
  char processedTimestamp[16];
  char msgType[16];
  char status[16];
  char liquidity;

  char msgContent[MAX_ORDER_MSG];
} t_ARCA_DIRECTOrderACK;

typedef struct t_ARCA_DIRECTOrderFill
{
  double price;
  double avgPrice;
  double fillFee;

  long sendingTime;
  long transTime;
  long arcaExOrderId;
  long executionId;

  int length;
  int seqNum;
  int orderID;
  int shares;
  int leftShares;

  unsigned short entryExitRefNum;

  char date[12];
  char timestamp[16];
  char processedTimestamp[16];
  char msgType[16];
  char ArcaExID[21];
  char liquidityIndicator;
  char side[3];
  char status[16];
  char LastMkt[3];

  char msgContent[MAX_ORDER_MSG];
} t_ARCA_DIRECTOrderFill;

typedef struct t_ARCA_DIRECTOrderCancel
{
  long sendingTime;
  long transTime;
  long arcaExOrderId;

  int orderID;
  int length;
  int seqNum;

  unsigned short entryExitRefNum;

  char date[12];
  char timestamp[16];
  char processedTimestamp[16];
  char msgType[16];
  char reason;
  char status[16];

  char msgContent[MAX_ORDER_MSG];
} t_ARCA_DIRECTOrderCancel;

typedef struct t_ARCA_DIRECTCancelRequest
{
  long arcaExOrderId;

  int length;
  int seqNum;
  int orderID;

  char date[12];
  char timestamp[16];
  char msgType[16];
  char symbol[SYMBOL_LEN_PLUS_ONE];
  char side[3];
  char deliverCompID[6];
  char status[16];

  char msgContent[MAX_ORDER_MSG];
} t_ARCA_DIRECTCancelRequest;

typedef struct t_ARCA_DIRECTCancelACK
{
  long sendingTime;
  long transTime;
  long arcaExOrderId;

  int length;
  int seqNum;
  int orderID;

  char date[12];
  char timestamp[16];
  char msgType[16];
  char status[16];

  char msgContent[MAX_ORDER_MSG];
} t_ARCA_DIRECTCancelACK;

typedef struct t_ARCA_DIRECTReject
{
  long sendingTime;
  long transTime;

  int length;
  int seqNum;
  int orderID;
  int orgOrderID;

  unsigned short entryExitRefNum;

  char date[12];
  char timestamp[16];
  char processedTimestamp[16];
  char msgType[16];
  char type;
  char reasonCode;
  char text[42];
  char status[16];

  char msgContent[MAX_ORDER_MSG];
} t_ARCA_DIRECTReject;

typedef struct t_ARCA_DIRECTBustOrCorrect
{
  double price;

  long sendingTime;
  long transTime;
  long executionId;

  int length;
  int seqNum;
  int orderID;
  int shares;

  char date[12];
  char timestamp[16];
  char msgType[16];
  char type;
  char status[16];

  char msgContent[MAX_ORDER_MSG];
} t_ARCA_DIRECTBustOrCorrect;

/****************************************************************************
** Global variables definition
****************************************************************************/

/****************************************************************************
** Function declarations
****************************************************************************/
int ParseRawDataForARCA_DIRECTOrder( t_DataBlock dataBlock, int type );

int ProcessNewOrderOfARCA_DIRECTOrder( t_DataBlock dataBlock, int type, char *timeStamp );

int ProcessNewOrderOfARCA_DIRECTOrderV3( t_DataBlock dataBlock, int type, char *timeStamp );

int ProcessOrderACKOfARCA_DIRECTOrder( t_DataBlock dataBlock, int type, char *timeStamp );

int ProcessOrderFillOfARCA_DIRECTOrder( t_DataBlock dataBlock, int type, char*timeStamp );

int ProcessOrderCancelOfARCA_DIRECTOrder( t_DataBlock dataBlock, int type, char *timeStamp );

int ProcessCancelRequestOfARCA_DIRECTOrder( t_DataBlock dataBlock, int type, char *timeStamp );

int ProcessCancelACKOfARCA_DIRECTOrder( t_DataBlock dataBlock, int type );

int ProcessRejectOfARCA_DIRECTOrder( t_DataBlock dataBlock, int type, char *timeStamp );

int ProcessBustOrCorrectOfARCA_DIRECTOrder( t_DataBlock dataBlock, int type );

/***************************************************************************/

#endif
