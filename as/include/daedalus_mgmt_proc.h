/*****************************************************************************
**  Project:    Equity Arbitrage Application
**  Filename:   daedalus_mgmt_proc.h
**  Description:  This file contains some declaration for daedalus_mgmt_proc.c  
**  Author:     Long Nguyen-Khoa-Hai
**  First created:  01-Oct-2013
**  Last updated: -----------
*****************************************************************************/

#ifndef __DAEDALUS_MGMT_PROC_H__
#define __DAEDALUS_MGMT_PROC_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include <stdlib.h>

#include "global_definition.h"
#include "utility.h"

/****************************************************************************
** Data structures definitions
****************************************************************************/

/****************************************************************************
** Global variables definition
****************************************************************************/

/****************************************************************************
** Function declarations
****************************************************************************/

void *ProcessDaedalus(void *pTrader_Tools_Conf);
void *SendCurrentStatusToDaedalus(void *pthread_arg);
int ProcessLoginSuccess(int ttIndex);

/***************************************************************************/

#endif
