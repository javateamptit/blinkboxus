/*---------------------------FILE SUMMARY--------------------------------------
|
| Project:    Blinkbox
| Filename:   nyse_ccg_data.h
| Description:  N/A
| Author:     Dung Tran-Dinh
| First created:  Aug 1, 2012
| Last updated: N/A
-----------------------------------------------------------------------------*/
#ifndef NYSE_CCG_DATA_H_
#define NYSE_CCG_DATA_H_

/*-----------------------------------------------------------------------------
|
|       INCLUDE FILES AND DEFINE SEVERAL CONSTANTS
|
-----------------------------------------------------------------------------*/
#include "global_definition.h"
#include "utility.h"
#include "position_manager_proc.h"

#include <stdlib.h>
#include <math.h>

// NYSE_CCG new order
typedef struct t_NYSE_CCGNewOrder
{
  double price;
  
  int length;
  int seqNum;
  int shares;
  int maxFloor;
  int clOrderID;
  int tsId;
  int crossId;
  int tradeType;
  int tradingAccount;
  int visibleShares;
  
  short bboId;
  unsigned short entryExitRefNum;
  
  char date[12];
  char timestamp[16];
  char msgType[16];
  char symbol[SYMBOL_LEN];
  char priceScale;
  char side[3];
  char askBid[4];
  char entryExit[6];
  char ecnLaunch[6];
  char timeInForce[4];
  char ecn[6];
  char compGroupID[6];
  char orderType;
  char execInst;
  char rule80A;
  char tradingSessionId[6];
  char account[11];
  char isoFlag;
  char dotReserve;
  char senderSubID[6];
  char firm[6];
  char status[16];
  
  char msgContent[MAX_ORDER_MSG];

}t_NYSE_CCGNewOrder;


typedef struct t_NYSE_CCGACKOrder
{
  double price;
  long meOrderID;

  int length;
  int seqNum;
  int transTime;
  int clOrderID;

  char date[12];
  char timestamp[16];
  char processedTimestamp[16];
  char msgType[16];
  char timeInForce[4];
  char deliverToCompID[6];
  char targetSubID[6];
  char account[11];
  char status[16];

  char msgContent[MAX_ORDER_MSG];

}t_NYSE_CCGACKOrder;

// NYSE_CCG fill order
typedef struct t_NYSE_CCGFillOrder
{
  double avgPrice;
  double lastPrice;
  double fillFee;
  
  long meOrderID;
  long execID;
  
  int length;
  int clOrderID;
  int seqNum;
  int transTime;
  int leavesShares;
  int lastShares;
  
  unsigned short entryExitRefNum;
  
  char date[12];
  char timestamp[16];
  char processedTimestamp[16];
  char msgType[16];
  char symbol[SYMBOL_LEN];
  char side[3];
  char priceScale;
  char billingIndicator;
  char lastMarket;
  char deliverToCompID[6];
  char targetSubID[6];
  char execBroker[6];
  char contraBroker[6];
  char contraTrader[6];
  char execAwayMktID[7];
  char billingRate[7];
  char Account[11];
  char dbExecID[11];
  char status[16];

  char msgContent[MAX_ORDER_MSG];

}t_NYSE_CCGFillOrder;

// NYSE_CCG cancel order
typedef struct t_NYSE_CCGCanceledOrder
{
  long meOrderID;
  
  int length;
  int seqNum;
  int transTime;
  int origClOrderID;
  
  unsigned short entryExitRefNum;
  
  char date[12];
  char timestamp[16];
  char processedTimestamp[16];
  char msgType[16];
  char deliverToCompID[6];
  char TargetSubID[6];
  char account[11];
  char infoCode;
  char status[16];

  char msgContent[MAX_ORDER_MSG];

}t_NYSE_CCGCanceledOrder;

// NYSE_CCG ACK order
typedef struct t_NYSE_CCGRejectedOrder
{
  long meOrderID;
  
  int length;
  int reason;
  int seqNum;
  int transTime;
  int clOrderID;
  int origClOrderID;
  
  unsigned short entryExitRefNum;
  
  char date[12];
  char timestamp[16];
  char processedTimestamp[16];
  char msgType[16];
  char deliverToCompID[6];
  char targetSubID[6];
  char type;
  char account[11];
  char text[41];
  char status[16];

  char msgContent[MAX_ORDER_MSG];

}t_NYSE_CCGRejectedOrder;

typedef struct t_NYSE_CCGCancelRequest
{
  long meOrderID;

  int length;
  int seqNum;
  int clOrderID;
  int origClOrderID;

  char date[12];
  char timestamp[16];
  char msgType[16];
  char symbol[SYMBOL_LEN];
  char side[3];
  char status[16];

  char msgContent[MAX_ORDER_MSG];
}t_NYSE_CCGCancelRequest;

typedef struct t_NYSE_CCGCancelACK
{
  long meOrderID;

  int length;
  int seqNum;
  int transTime;
  int clOrderID;

  char date[12];
  char timestamp[16];
  char msgType[16];
  char deliverToCompID[6];
  char status[16];
  
  char msgContent[MAX_ORDER_MSG];
} t_NYSE_CCGCancelACK;

typedef struct t_NYSE_CCGBustOrCorrect
{
  double price;

  long meOrderID;
  long execId;

  int clOrderID;
  int shares;
  int length;
  int seqNum;
  int transTime;

  char date[12];
  char timestamp[16];
  char msgType[16];
  char priceScale;
  char type;
  char deliverToCompID[6];
  char contraTrader[6];
  char status[16];
  char msgContent[MAX_ORDER_MSG];
} t_NYSE_CCGBustOrCorrect;

/******************************************************************
            FUNCTION DEFINITIONS
******************************************************************/

int ParseRawDataForNYSE_CCGOrder( t_DataBlock dataBlock, int type );

int ProcessNewOrderOfNYSE_CCGOrder( t_DataBlock dataBlock, int type, char *timeStamp );

int ProcessOrderACKOfNYSE_CCGOrder( t_DataBlock dataBlock, int type, char *timeStamp );

int ProcessOrderFillOfNYSE_CCGOrder( t_DataBlock dataBlock, int type, char*timeStamp );

int ProcessOrderCanceledOfNYSE_CCGOrder( t_DataBlock dataBlock, int type, char *timeStamp );

int ProcessRejectOfNYSE_CCGOrder( t_DataBlock dataBlock, int type, char *timeStamp );

int ProcessCancelRequestOfNYSE_CCGOrder( t_DataBlock dataBlock, int type, char *timeStamp );

int ProcessCancelACKOfNYSE_CCGOrder( t_DataBlock dataBlock, int type, char *timeStamp );

int ProcessBustOrCorrectOrderOfNYSE_CCGOrder( t_DataBlock dataBlock, int type, char *timeStamp );

int Convert_NYSECCG_Format_To_ClOrdID(unsigned char *valueFiled);

void Initialize_NYSE_Order_ID_Mapping(void);

/*****************************************************************************/
#endif /* NYSE_CCG_DATA_H_ */
