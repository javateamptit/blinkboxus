/*****************************************************************************
**  Project:    Equity Arbitrage Application
**  Filename:   trade_servers_proc.h
**  Description:  This file contains some declaration for trade_servers_proc.c  
**  Author:     Luan Vo-Kinh
**  First created:  13-Sep-2007
**  Last updated: -----------
*****************************************************************************/

#ifndef __TRADE_SERVERS_PROC_H__
#define __TRADE_SERVERS_PROC_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include <stdlib.h>

#include "DaedalusMessage.pb-c.h"
#include "DaedalusRequest.pb-c.h"
#include "global_definition.h"
#include "utility.h"

#define TS_SERVICE_ARCA_BOOK    10000
#define TS_SERVICE_NASDAQ_BOOK  10001
#define TS_SERVICE_NYSE_BOOK    10002
#define TS_SERVICE_BATSZ_BOOK   10003
#define TS_SERVICE_EDGX_BOOK    10004
#define TS_SERVICE_EDGA_BOOK    10005
#define TS_SERVICE_NDBX_BOOK    10006
#define TS_SERVICE_BYX_BOOK     10007
#define TS_SERVICE_PSX_BOOK     10008
#define TS_SERVICE_AMEX_BOOK    10009

#define TS_SERVICE_CQS_FEED     11000
#define TS_SERVICE_UQDF_FEED    11001

#define TS_SERVICE_ALL_OECs          -1
#define TS_SERVICE_ARCA_DIRECT    20000
#define TS_SERVICE_NASDAQ_OUCH    20001
#define TS_SERVICE_NYSE_CCG       20002
#define TS_SERVICE_NASDAQ_RASH    20003
#define TS_SERVICE_BATSZ_BOE      20004
#define TS_SERVICE_EDGX_DIRECT    20005
#define TS_SERVICE_EDGA_DIRECT    20006
#define TS_SERVICE_NDAQ_OUBX      20007
#define TS_SERVICE_BYX_BOE        20008
#define TS_SERVICE_PSX            20009

/****************************************************************************
** Data structures definitions
****************************************************************************/

/****************************************************************************
** Global variables definition
****************************************************************************/
extern t_BackUpGlobalConf BackupTSGlobalConf[MAX_TRADE_SERVER_CONNECTIONS];
extern int IsMaxBPUpdated;

/****************************************************************************
** Function declarations
****************************************************************************/

void *ProcessTradeServer(void *pthread_arg);

int Talk2TradeServer(int tsIndex);

int ReceiveTSMessage(int tsIndex, void *message);

int ProcessTSMessageForRawData(int tsIndex, void *message);

int ProcessTSMessageForOutputLog(int tsIndex, void *message);

int ProcessTSMessageForCurrentStatus(int tsIndex, void *message);

int ProcessTSMessageForBookConf(int tsIndex, void *message);

int ProcessTSMessageForOrderConf(int tsIndex, void *message);

int ProcessTSMessageForGlobalConf(int tsIndex, void *message);

int ProcessTSMessageForMinSpreadConf(int tsIndex, void *message);

int ProcessTSMessageForSymbolStatus(int tsIndex, void *message);

int BuildTSMessageForRawDataStatus(int tsIndex, void *message);

int BuildTSMessageForTradeOutputStatus(int tsIndex, void *message);

int BuildTSMessageForResetTSCurrentStatus(void *message, int type);

int SendResetTSCurrentStatusToAllTS(int type);

int BuildTSMessageForUpdateTSCurrentStatus(void *message, double buyingPower, int isMaxBPFlag);

int BuildTSMessageForEnableTrading(void *message, int ECNIndex);

int BuildTSMessageForDisableTrading(void *message, int ECNIndex, short reasonID);

int SendHeartBeatToTS(int tsIndex);

int BuildTSMessageForConnectBook(void *message, int ECNIndex);

int BuildTSMessageForDisconnectBook(void *message, int ECNIndex);

int BuildTSMessageForConnectOrder(void *message, int ECNIndex);

int BuildTSMessageForDisconnectOrder(void *message, int ECNIndex);

int BuildTSMessageForSetTSBookConf(void *message, int tsIndex, int ECNIndex);

int BuildTSMessageForSetTSOrderConf(void *message, int tsIndex, int ECNIndex);

int BuildTSMessageForSetTSGlobalConf(void *message, int tsIndex);

int BuildTSMessageForSetTSMinSpreadConf(void *message, int tsIndex);

int BuildTSMessageForGetSymbolStatus(void *message, int ttIndex, unsigned char symbol[SYMBOL_LEN], char *listMdc);

int BuildTSMessageForFlushASymbol(void *message, int ECNIndex, unsigned char symbol[SYMBOL_LEN]);

int BuildTSMessageForFlushAllSymbol(void *message, int ECNIndex);

int BuildTSMessageForIgnoreStockList(void *message);

int BuildTSMessageForSetIgnoreStockList(void *message, int type, const char *symbol);

int BuildTSMessageForSetIgnoreStockRangesList(void *message, int type, const char *symbol);

int SendTSMessage(int tsIndex, void *message);

int SendRawDataStatusToTS(int tsIndex);

int SendOutputLogStatusToTS(int tsIndex);

int SendResetTSCurrentStatusToTS(int tsIndex, int type);

int SendUpdateBuyingPowerToAllTS(int account);

int SendUpdateTSCurrentStatusToTS(int tsIndex, double buyingPower, int isMaxBPFlag);

int SendConnectBookToTS(int tsIndex, int ECNIndex);

int SendDisconnectBookToTS(int tsIndex, int ECNIndex);

int SendConnectOrderToTS(int tsIndex, int ECNIndex);

int SendDisconnectOrderToTS(int tsIndex, int ECNIndex);

int SetTSBookConfToTS(int tsIndex, int ECNIndex);

int SetTSOrderConfToTS(int tsIndex, int ECNIndex);

int SetTSGlobalConfToTS(int tsIndex);

int SetTSMinSpreadConfToTS(int tsIndex);

int SendGetSymbolStatusToTS(int tsIndex, int ttIndex, unsigned char symbol[SYMBOL_LEN], char *listMdc);

int SendFlushASymbolToTS(int tsIndex, int ECNIndex, unsigned char symbol[SYMBOL_LEN]);

int SendFlushAllSymbolToTS(int tsIndex, int ECNIndex);

int SendEnableTradingAllOrderToAllTS(void);

int SendDisableTradingAllOrderToAllTS(short reasonID);

int SendEnableTradingAllOrderToTS(int tsIndex);

int SendDisableTradingAllOrderToTS(int tsIndex, short reasonID);

int SendEnableTradingAOrderToTS(int tsIndex, int ECNIndex);

int SendDisableTradingAOrderToTS(int tsIndex, int ECNIndex, short reasonID);

int SendIgnoreStockListToAllTS(int type, const char *symbol);

int SendIgnoreStockRangesListToAllTS(int type, const char *symbol);

int SendIgnoreStockListToTS(int tsIndex);

int SendIgnoreStockRangesListToTS(int tsIndex);

int ProcessTSConnectAllConnection(int tsIndex, int ttIndex);

int ProcessTSConnectAllECNBook(int tsIndex);

int ProcessTSConnectAllECNOrder(int tsIndex);

int ProcessTSDisconnectAllBookAndOrder(void);

int ProcessTSDisconnectAllECNBook(int tsIndex);

int ProcessTSDisconnectAllECNOrder(int tsIndex);

int SetTSBookARCAConfToTS(int tsIndex, int type, int groupIndex);

int BuildTSMessageForSetTSBookARCAConf(void *message, int tsIndex, int type, int groupIndex);

int SetTSBookNYSEOpenUltraConfToTS(int tsIndex, int type, int groupIndex);

int BuildTSMessageForSetTSBookNYSEOpenUltraConf(void *message, int tsIndex, int type, int groupIndex);

int SendCancelRequestToTS(int tsIndex, int clOrdId, long ECNOrderId, char *symbol, char side);
int BuildTSMessageForCancelARCA_DIRECT(void *message, int clOrdId, long ECNOrderId, char *symbol, char side);

int SendHaltStockListToAllTS(int updatedList[MAX_STOCK_SYMBOL], int updatedCount);
int SendHaltStockListToTS(const int tsIndex, int updatedList[MAX_STOCK_SYMBOL], int updatedCount);
int BuildTSMessageForHaltedSymbolList(void *message, int updatedList[MAX_STOCK_SYMBOL], int updatedCount);

int ProcessTSMessageForBookIdleWarning(int tsIndex, void *message);

int SendConnectToCQSnUQDFToTS(int tsIndex, int sourceId);
int SendDisconnectToCQSnUQDFToTS(int tsIndex, int sourceId);
int ProcessTSMessageForBBOQuotes(int tsIndex, void *message);
int SendGetBBOQuotesToTS(int tsIndex, char *symbol, int ttIndex);
int SendFlushBBOQuotesToTS(int tsIndex, int exchangeId);
int SendSetExchangesConfToTS(int tsIndex, int buffLen, const unsigned char *buffer);
int ProcessTSMessageForExchangeConf(int tsIndex, void *message);
int SendSetISO_TradingToTS(int tsIndex, int status);

int ProcessTSMessageForBookISO_Trading(int tsIndex, void *message);
int SendSetBookISO_TradingToTS(int tsIndex);
int ProcessTSMessageForBBOInfo(int tsIndex, void *message);
int BuildTSMessageForBBOInfoStatus(int tsIndex, void *message);
int SendBBOInfoStatusToTS(int tsIndex);

// For get the number of symbols with quotes 
int SendGetNumberOfSymbolsWithQuotesToTS(int tsIndex, int ttIndex);
int BuildTSMessageForGetNumberOfSymbolsWithQuotes(void *message, int ttIndex);
int ProcessTSMessageForNumberOfSymbolsWithQuotes(int tsIndex, void *message);

int BuildTSMessageForSetTSBookARCAFeed(void *message, int tsIndex, char feedName);
int SetTSBookARCAFeedToTS(int tsIndex, char feedName);
int BuildTSMessageForSetStuckUnlockForASymbol(void *message, const char *symbol, const int lockCount);
int BuildTSMessageForSetLockCounterForASymbol(void *message, const char *symbol, const int lockCount);
int BuildTSMessageForCurrentSymbolsStuckStatus(void *message, const int tsIndex, int *count);
int SendStuckSymbolsToTS(int tsIndex);
int SendStuckUnlockForASymbolToTradeServers(const char *symbol, int account, const int lockCount);
int SendSymbolLockCounterForASymbolToTradeServers(const char *symbol, int account, const int lockCount);

int ProcessTSMessageForDisconnectAlert(int tsIndex, void *message);
int ProcessTSMessageForDisabledTradingAlert(int tsIndex, void *message);
int ProcessTSMessageForGroupOfBookDataUnhandledAlert(int tsIndex, void *message);
int ProcessTSMessageForHaltStockList(int tsIndex, void *message);
int ProcessTSMessageForMarketStaleAlert(int tsIndex, void *message);

int SendActiveVolatileSymbolsToTS(int tsIndex);
int SendActiveVolatileSymbolsToAllTS();
int BuildTSMessageForActiveVolatileSymbolList(void *message);

int SendAllVolatileToggleStatusToTS(int tsIndex);
int SendAllVolatileToggleStatusToAllTS();
int BuildTSMessageForAllVolatileToggleStatus(void *message);

int BuildTSMessageForHaltSymbol(void *message, const int stockSymbolIndex);
int SendHaltSymbolToTS(const int tsIndex, const int stockSymbolIndex);
int SendAHaltStockToAllTS(const int stockSymbolIndex);
int SendTradingAccountToTS(int tsIndex);
int BuildTSMessageForTradingAccount(void *message, const int tsIndex);

int SendEtbSymbolListToTS(int tsIndex); //Send ETB List for TS which has just connected to AS
int SendEtbSymbolListToAllTS(char prevStatus[MAX_STOCK_SYMBOL]); //Send for all connected TS
int BuildTSMessageForEtbSymbolList(void *message, char prevStatus[MAX_STOCK_SYMBOL]); //Use for build message
int ProcessTSMessageForSplitSymbolConf(int tsIndex, void *message);
int SendRequestPriceUpdateToTS(int tsIndex, const int totalSymbol, char symbolList[][SYMBOL_LEN]);
int BuildTSMessageForRequestPriceUpdate(void *message, int tsIndex, const int totalSymbol, char symbolList[][SYMBOL_LEN]);
int ProcessTSMessageForPriceUpdate(int tsIndex, t_TSMessage *tsMessage);
int DisableTradingAllVenues(int tsIndex);

int SendHungOrderResetToAllTS();
int SendHungOrderResetToTS(int tsIndex);
int BuildTSMessageForHungOrderReset(void *message, int clOrdId, char *symbol);
/***************************************************************************/

#endif
