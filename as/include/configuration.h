/*****************************************************************************
**  Project:    Equity Arbitrage Application
**  Filename:   configuration.h
**  Description:  This file contains some declaration for configuration.c 
**  Author:     Luan Vo-Kinh
**  First created:  13-Sep-2007
**  Last updated: -----------
*****************************************************************************/

#ifndef __CONFIGURATION_H__
#define __CONFIGURATION_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include <stdio.h>
#include <string.h>

#include "global_definition.h"
#include "utility.h"

#define FILE_NAME_OF_TRADER_TOOL_CONF             "tt_config"
#define FILE_NAME_OF_PM_CONF                      "pm_config"
#define FILE_NAME_OF_AS_GLOBAL_CONF               "global_config"
#define FILE_NAME_OF_IGNORE_STOCK_LIST            "ignore_stocklist"
#define FILE_NAME_OF_IGNORE_STOCK_RANGES_LIST     "ignore_stock_ranges_list"
#define FILE_NAME_OF_HALTED_STOCK_LIST            "halted_stocklist"
#define FILE_NAME_OF_SHORTABILITY_SCHEDULE        "shortability_schedule"
#define FILE_NAME_OF_ETB_SYMBOL                   "etb_symbol"
#define FILE_NAME_OF_LDAP_CONFIG                  "ldap_config"
#define FILE_NAME_OF_ALERT_CONF                   "alert_config"
#define FILE_NAME_OF_ECN_SETTING_SCHEDULE_CONF    "ecn_setting_schedule"

#define FILE_NAME_OF_ARCA_DIRECT_REJECT_CODE      "arca_direct_reject_code.csv"
#define FILE_NAME_OF_ARCA_FIX_REJECT_CODE         "arca_fix_reject_code.csv"
#define FILE_NAME_OF_NASDAQ_OUCH_REJECT_CODE      "nasdaq_ouch_reject_code.csv"
#define FILE_NAME_OF_NASDAQ_RASH_REJECT_CODE      "nasdaq_rash_reject_code.csv"
#define FILE_NAME_OF_NYSE_CCG_REJECT_CODE         "nyse_ccg_reject_code.csv"
#define FILE_NAME_OF_BATSZ_BOE_REJECT_CODE        "batsz_boe_reject_code.csv"
#define FILE_NAME_OF_BYX_BOE_REJECT_CODE          "byx_boe_reject_code.csv"
#define FILE_NAME_OF_EDGX_DIRECT_REJECT_CODE      "edgx_reject_code.csv"
#define FILE_NAME_OF_EDGA_DIRECT_REJECT_CODE      "edga_reject_code.csv"
#define FILE_NAME_OF_OUBX_DIRECT_REJECT_CODE      "oubx_reject_code.csv"
#define FILE_NAME_OF_PSX_REJECT_CODE              "psx_reject_code.csv"

extern int currentMarketTimeType;//-->PRE-MARKET, INTRADAY, POST-MARKET
extern char RashRouteID[3][6];

/****************************************************************************
** Data structures definitions
****************************************************************************/
/*
The structure is used to manage 
status/behavior of a connection to ECN Order
*/
typedef struct t_OrderStatus
{
  /*
  The field is used to store status of a connection that was made to 
  an ECN Order successfully (CONNECTED) or disconnect to (DISCONNECTED)
  (Default value = DISCONNECTED)
  */
  char isOrderConnected; 
  char preIsOrderConnected; 
  
  /*
  The field is used to store request from AS: YES or NO
  YES: Trade Server should makes a connection to an ECN Order Server
  NO: Trade Server should disconnect connection to an ECN Order Server
  (Default value = YES)
  */
  char shouldConnect;
} t_OrderStatus;

// Trading account
typedef struct t_TradingAccount
{
  // char ARCAFIX[MAX_ACCOUNT][32];
  char RASH[MAX_ACCOUNT][32];
  char OUCH[MAX_ACCOUNT][32];
  char DIRECT[MAX_ACCOUNT][32];
  char RASHToken[MAX_ACCOUNT][8];
  char OUCHToken[MAX_ACCOUNT][8];
  char OUBX[MAX_ACCOUNT][32];
  char OUBXToken[MAX_ACCOUNT][8];
  char CCG[MAX_ACCOUNT][32];
  char BATSZBOE[MAX_ACCOUNT][32];
  char BYXBOE[MAX_ACCOUNT][32];
  char EDGX[MAX_ACCOUNT][32];
  char EDGXToken[MAX_ACCOUNT][8];
  char EDGA[MAX_ACCOUNT][32];
  char EDGAToken[MAX_ACCOUNT][8];
  char PSX[MAX_ACCOUNT][32];
  char PSXToken[MAX_ACCOUNT][8];

  char ARCADIRECTLoginAS[MAX_ACCOUNT_LOGIN_LEN + 1];
  char ARCADIRECTLoginPM[MAX_ACCOUNT_LOGIN_LEN + 1];
  char RASHLoginAS[MAX_ACCOUNT_LOGIN_LEN + 1];
  char RASHLoginPM[MAX_ACCOUNT_LOGIN_LEN + 1];
  char RASHLoginTS[MAX_TRADE_SERVER_CONNECTIONS][MAX_ACCOUNT_LOGIN_LEN + 1];
  char OUCHLoginTS[MAX_TRADE_SERVER_CONNECTIONS][MAX_ACCOUNT_LOGIN_LEN + 1];
  char OUBXLoginTS[MAX_TRADE_SERVER_CONNECTIONS][MAX_ACCOUNT_LOGIN_LEN + 1];
  char DIRECTLoginTS[MAX_TRADE_SERVER_CONNECTIONS][MAX_ACCOUNT_LOGIN_LEN + 1];
  char CCGLoginTS[MAX_TRADE_SERVER_CONNECTIONS][MAX_ACCOUNT_LOGIN_LEN + 1];
  char BATSZBOELoginTS[MAX_TRADE_SERVER_CONNECTIONS][MAX_ACCOUNT_LOGIN_LEN + 1];
  char BYXBOELoginTS[MAX_TRADE_SERVER_CONNECTIONS][MAX_ACCOUNT_LOGIN_LEN + 1];
  char EDGXLoginTS[MAX_TRADE_SERVER_CONNECTIONS][MAX_ACCOUNT_LOGIN_LEN + 1];
  char EDGALoginTS[MAX_TRADE_SERVER_CONNECTIONS][MAX_ACCOUNT_LOGIN_LEN + 1];
  char PSXLoginTS[MAX_TRADE_SERVER_CONNECTIONS][MAX_ACCOUNT_LOGIN_LEN + 1];

  char accountForTS[MAX_TRADE_SERVER_CONNECTIONS];
} t_TradingAccount;

/****************************************************************************
** Global variables definition
****************************************************************************/
extern t_TradeServerInfo tradeServersInfo;
extern t_TraderToolInfo traderToolsInfo;
extern t_PMInfo pmInfo;

extern t_ASSetting asSetting;
extern t_PMSetting pmSetting;

extern t_IgnoreStockList IgnoreStockList;
extern t_IgnoreStockList pmIgnoreStockList;
extern t_IgnoreStockRangesList IgnoreStockRangesList;

extern t_OrderStatus orderStatusMgmt[AS_MAX_ORDER_CONNECTIONS];
extern int clientOrderID;
extern pthread_mutex_t nextOrderID_Mutex;

extern int NumberOutputLogToStop;
extern int NumberOutputLogToResume;

extern t_ExchangeInfo ExchangeStatusList[MAX_TRADE_SERVER_CONNECTIONS][MAX_EXCHANGE];
extern t_PMExchangeInfo PMExchangeStatusList[MAX_EXCHANGE];

extern t_TradingAccount TradingAccount;
extern t_FeeConf FeeConf;
extern t_AlertConf AlertConf;
extern int TradeTimeAlertConfig[NUMBER_OF_SECONDS_PER_DAY]; // 24h x 3600s
extern t_ModuleVersionMgmt moduleVersionMgmt;
extern t_ShortabilitySchedule ShortabilitySchedule;
extern t_SSLMgmt SSLMgmt;
extern t_LdapInfo LdapInfo;

extern int TimeToEnableBatszSetting;
extern int TimeToDisableBatszSetting;
extern int TimeToEnableBatszTrading;

extern int TimeToEnableBatsySetting;
extern int TimeToDisableBatsySetting;
extern int TimeToEnableBatsyTrading;

extern int TimeToEnableEdgxSetting;
extern int TimeToDisableEdgxSetting;
extern int TimeToEnableEdgxTrading;

extern int TimeToEnableEdgaSetting;
extern int TimeToDisableEdgaSetting;
extern int TimeToEnableEdgaTrading;

extern int TimeToDisableNyseSetting;
extern int TimeToDisableAmexSetting;

extern int TimeToEnableBxSetting;

extern int TimeToEnablePsxSetting;

extern int TimeToDisableTrading[TS_MAX_ORDER_CONNECTIONS];

extern t_SessionIdMgmt SessionIdMgmt;
/****************************************************************************
** Function declarations
****************************************************************************/
int InitializeTradeServersInfo(void);
int InitializePMInfo(void);
int InitializeIgnoreStockList();
int InitializeIgnoreStockRangesList();
int InitializeTraderToolsInfo(void);
int LoadTradeServersConf(void);
int FindTradeServerIndex(int tsId);
int FindTradeServerIndexFromDescription(char description[MAX_LINE_LEN]);
int SaveTradeServersConf(int tsIndex);
int LoadTraderToolsConf(void);
int SaveTraderToolsConf(void);
int LoadPMConf(void);
int SavePMConf(void);
int LoadASGlobalConf(void);
int SaveASGlobalConf(void);
int LoadASOrdersConf(void);
int LoadNASDAQ_RASHConf(char *fileName, void *ECNOrderConf);
int LoadARCA_DIRECTConf(char *fileName, void *ECNOrderConf);
int ProcessSaveASOrderConf(int ECNIndex);
int SaveNASDAQ_RASHConf(char *fileName, void *ECNOrderConf);
int SaveARCA_DIRECTConf(char *fileName, void *ECNOrderConf);
int LoadIgnoreStockList(void);
int LoadIgnoreStockRangesList(void);
int SaveIgnoreStockList(void);
int SaveIgnoreStockRangesList(void);
int LoadSequenceNumber(const char *fileName, int type, void *configInfo);
int LoadIncomingAndOutgoingSeqNum(const char *fullPath, int type, void *configInfo);
int LoadIncomingSeqNum(const char *fullPath, int type, void *configInfo);
int SaveSeqNumToFile(const char *fileName, int type, void *configInfo);
int SaveIncomingAndOutgoingSeqNumToFile(const char *fileName, int type, void *configInfo);
int SaveIncomingSeqNumToFile(const char *fileName, int type, void *configInfo);
int LoadClientOrderID(const char *fileName);
int SaveClientOrderID(const char *fileName, int clientOrderID);
int LoadLongShortFromDatabase(void);
int CheckAndProcessStopTradingByTWT(void);
int CheckAndModifyLongShortByTWT(void);
int CheckAndProcessHungOrders(void);
int CheckChangeOfBuyingPower(void);
int SaveTradeServerSetting(int tsIndex);
int SaveASSetting(void);
int SavePMSetting(void);
int LoadTradingAccountFromFile(const char *fileName, void *_tradingAccount);
int LoadFeeConf(void);
int LoadAlertConf(void);
int SaveAlertConf(void);
int UpdateTradeTimeAlertConfig(void);
int LoadVolatileSymbolsFromFileToList(char *list, int *count);
int SaveVolatileSymbolListToFile(char *list, int count);
int LoadVolatileSectionsFromFileToLists(char *preList, int *countPre, char *postList, int *countPost, char *intradayList, int *countIntraday);
int SaveVolatileSectionsToFile(char *preList, int countPre, char *postList, int countPost, char *intradayList, int countIntraday);
int LoadAllVolatileToggleSchedulesFromFile();
int SaveAllVolatileToggleSchedulesToFile();
int GetActiveVolatileSymbols(char *activeList);
int LoadHaltedStockListFromFile();
int SaveHaltedStockListToFile();
int CheckAndRemoveStaleHaltedSymbols();
int LoadShortabilitySchedule();
int LoadLdapConfiguration();
int LoadLossBySymbolFromfile();
int SaveLossBySymbol(char *symbol, int tsIndex, double currentLoss);
int LoadEcnSettingSchedule();
int InitializeSessionIdManagement();
/***************************************************************************/

#endif
