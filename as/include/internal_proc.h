/*****************************************************************************
**  Project:    Equity Arbitrage Application
**  Filename:   internal_proc.h
**  Description:  This file contains some declaration for internal_proc.c 
**  Author:     Luan Vo-Kinh
**  First created:  14-Sep-2007
**  Last updated: -----------
*****************************************************************************/

#ifndef __INTERNAL_PROC_H__
#define __INTERNAL_PROC_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include <stdlib.h>

#include "global_definition.h"
#include "utility.h"

#include "DaedalusMessage.pb-c.h"
#include "DaedalusRequest.pb-c.h"

/****************************************************************************
** Data structures definitions
****************************************************************************/
// Process raw data delay in microseconds
#define RAW_DATA_DELAY 100000

#define QUERY_PROCESS_DELAY 100000

// Process output log delay in microseconds
#define OUTPUT_LOG_DELAY 100000

// Use to check BATSZ ECN every second
#define ONE_SECOND 1000000
#define CHECK_BATSZ_ECN_SETTING 10 //(ONE_SECOND/RAW_DATA_DELAY)

/****************************************************************************
** Global variables definition
****************************************************************************/

/****************************************************************************
** Function declarations
****************************************************************************/

void *ProcessRawDataManagement(void *pArgs);

int ProcessHistoryRawData(void);

int ProcessCurrentRawData(void);

int ProcessBrokenTrade(void);

int CalculationRiskManagement(t_BrokenTradeCollection *newBrokenList);

void *ProcessOutputLogManagement(void *pArgs);

int ProcessHistoryOutputLog(void);

int ProcessCurrentOutputLog(void);

int ProcessHistoryBBOInfo(void);

void UpdateCurrentMarketTime(void);

void *ProcessQueryDatabaseManagement(void *pArgs);
int ProcessCurrentQueryDatabase(void);

void CheckNSendBuyingPowerAlertToAllTT();
void CheckNSendInCompleteTradeAlertToAllTT();
void CheckNSendNoTradeDuringConfiguredPeriodAlertToAllTT();
void CheckNSendPMHasntExitPositionAlertToAllTT();
void CheckNSendPMOrderRateAlertToAllTT();
void CheckNSendUnhandledOpenPositionWithoutAnOrderAlertToTT();
int InitilizeAlertDataStructure(void);
void *TimerThread(void *pArgs);
int CheckNRequestEtbList();
void *RequestEtbListThread(void* args);
int CheckToStopTSTradingByMaxCrossLimit();
int CheckToAddSymbolToVolatileOrDisabledList(char *symbol, int symbolIndex, int tsIndex);
int SLLMappingStockSymbol(char *stockSymbol);
int CheckNSendEnableTradingForTSAndPM();
int GetOrderConnectionStatusOfTS(int tsIndex, int orderIndex);
int GetBookConnectionStatusOfTS(int tsIndex, int bookIndex);
int GetTradingStatusOfTSOrder(int tsIndex, int orderIndex);
char* GetMdcNameFromMdcType(Com__Rgm__Daedalus__Proto__MDC mdcType);
char* GetOecNameFromOecType(Com__Rgm__Daedalus__Proto__OEC oecType);
char* GetExchangeNameFromCode(char exchangeCode);
/***************************************************************************/

#endif
