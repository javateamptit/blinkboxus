/*****************************************************************************
**  Project:    Equity Arbitrage Application
**  Filename:   output_log_mgmt.h
**  Description:  This file contains some declaration for output_log_mgmt.c 
**  Author:     Luan Vo-Kinh
**  First created:  13-Sep-2007
**  Last updated: -----------
*****************************************************************************/

#ifndef __OUTPUT_LOG_MGMT_H__
#define __OUTPUT_LOG_MGMT_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "global_definition.h"
#include "utility.h"

#define FILE_NAME_OF_OUTPUT_LOG_PROCESSED "count_outputlog_processed.txt"
#define FILE_NAME_OF_BBO_INFO_PROCESSED "count_bboinfo_processed.txt"
#define FILE_NAME_OF_CROSS_ID "current_cross_id"

// Maximum trade output entries
#define MAX_OUTPUT_LOG_ENTRIES 1505000

// Maximum output log send
#define MAX_OUTPUT_LOG_SEND 500


#define MAX_DETAIL_PER_CROSS 10
#define MAX_DETAIL_PER_TRADE 250

// Maximum cross id 
#define MAX_CROSS_ID 900000

// Maximum cross entries for each trade server
#define MAX_CROSS_ENTRIES 400000

// Maximum total cross-grouped entries
#define MAX_TOTAL_CROSS_ENTRIES 800000

// Maximum total trade entries
#define MAX_TOTAL_TRADE_ENTRIES 400000

#define CROSS_INDEX 0
#define TRADE_INDEX 1
#define SKIP_STATUS 2

#define SINGLE_LEVEL_VALUE 0
#define MULTI_LEVEL_VALUE 1

#define MAX_SIDE 2
#define ASK_INDEX 0
#define BID_INDEX 1

#define MAX_STUCK_PER_SYMBOL 50

#define MAX_BBO_ENTRY 200000
#define MAX_EXCHANGE 16

/****************************************************************************
** Data structures definitions
****************************************************************************/
// This structure is used to management output log
typedef struct t_OutputLogMgmt  //8byte aligned
{
  // File pointer contains file descriptor to raw data file
  FILE *fileDesc;

  // tsIndex
  int tsIndex;

  // Last update index
  int lastUpdatedIndex;

  // Count raw data block
  int countOutputLog;

  // Check raw data loaded
  int isProcessed;

  // File name is used stored raw data
  char fileName[MAX_LINE_LEN];

  // Mutex lock
  pthread_mutex_t updateMutex;

  // All trade output log will be stored at the collection
  char tradeLogCollection[MAX_OUTPUT_LOG_ENTRIES][MAX_OUTPUT_LOG_TEXT_LEN];
} t_OutputLogMgmt;

// This structure is used to management position trade output log processed
typedef struct t_OutputLogProcessedMgmt   //8byte aligned
{
  // File pointer contains file descriptor to raw data file
  FILE *fileDesc;

  // Position output log is processed
  int tsPosition[MAX_TRADE_SERVER_CONNECTIONS];
} t_OutputLogProcessedMgmt;

typedef struct t_TradeOutputPoint   //4byte aligned
{
  int timestamp;
  int isUpdate;
  
  // Id of tables contain trade output
  int idTables[MAX_TRADE_OUTPUT_TABLE];
  
  // Position output log is processed
  int tsPosition[MAX_TRADE_SERVER_CONNECTIONS];
} t_TradeOutputPoint;

typedef struct t_TradeOutputPointMgmt //4-byte aligned
{
  int currentPoint;
  t_TradeOutputPoint pointList[MAX_POINT];
  
  char fileName[MAX_LINE_LEN];
} t_TradeOutputPointMgmt;

// This structure is used to contain ASK/BID information
typedef struct t_AskBidInfo     //8-byte aligned
{
  double price;
  int shares;
  int ECNId;
  int indexLogMsg;
  int _padding;
} t_AskBidInfo;

// This structure is used to contain Trade Detail information
typedef struct t_TradeDetailInfo    //8-byte aligned
{
  double price;
  int shares;
  int tradeType;
  int ECNId;
  int indexLogMsg;
} t_TradeDetailInfo;

typedef struct t_CrossSummaryInfo   //4byte aligned
{
  char isUpdate[MAX_TRADER_TOOL_CONNECTIONS];
  char symbol[SYMBOL_LEN];
  char countSent[MAX_TRADER_TOOL_CONNECTIONS];
  int symbolIndex;
  int crossId;
  int timestamp;
  int ecnLaunch;
  int askBidLaunch;
  int ecnExist;
  int tradeIndex;
  int tsIndex;
  int indexCrossList[MAX_TRADE_SERVER_CONNECTIONS];
} t_CrossSummaryInfo;

typedef struct t_CrossSummaryCollection
{
  int countCross;
  int lastMonitorIndex;
  t_CrossSummaryInfo crossList[MAX_TOTAL_CROSS_ENTRIES];
} t_CrossSummaryCollection;

typedef struct t_BestAskBid
{
  int bookIndex;
  int shares;
  double price;
} t_BestAskBid;

typedef struct t_TSCrossSummary   //8 bytes aligned
{
  int crossId;
  int tsIndex;
  int asCrossIndex;
  int tradeIndex;
  int timestamp;
  int isMultiLevel;
  int symbolIndex;
  int serverRole;
  int ecnLaunch;
  int askBidLaunch;
  int ecnExist;
  int askShares;
  int bidShares;
  int tradeType;
  double worstAskPrice;
  double worstBidPrice;
  t_BestAskBid askBidList[MAX_SIDE][TS_MAX_BOOK_CONNECTIONS];
} t_TSCrossSummary;

typedef struct t_TSCrossInfo  //8-byte aligned
{
  t_TSCrossSummary summary;
  
  int countLogMsg;
  int _padding;
  int indexLogMsgList[MAX_DETAIL_PER_CROSS];
} t_TSCrossInfo;

typedef struct t_TSCrossCollection  //8-byte aligned
{
  int countCross;
  int prevCountCross; // this number is updated every minute
  t_TSCrossInfo crossList[MAX_CROSS_ENTRIES];
} t_TSCrossCollection;

typedef struct t_TradeSummary //8byte aligned
{
  int tradeId;
  int tsIndex;
  int crossId;
  int asCrossId;
  int boughtShares;
  int soldShares;
  int leftSharesStuck;
  int _padding;
  double totalBought;
  double totalSold;
  double totalStuck;
} t_TradeSummary;

typedef struct t_TradeInfo    //8 bytes aligned
{
  int indexLogMsgList[MAX_DETAIL_PER_TRADE];
  int countLogMsg;
  int _padding;
  t_TradeSummary summary;
} t_TradeInfo;

typedef struct t_TradeCollection //8 bytes aligned
{
  t_TradeInfo tradeList[MAX_TOTAL_TRADE_ENTRIES];
  int countTrade;
  int _padding;
} t_TradeCollection;

typedef struct t_SymbolDetectCross
{
  int countSymbol;
  char symbolList[MAX_STOCK_SYMBOL][SYMBOL_LEN];
} t_SymbolDetectCross;

typedef struct t_StuckInfo
{
  int tsIndex;
  int crossId;
  int timestamp;
  int stuckShares;
  double stuckPrice;
  double realValue;
  double totalFee;
} t_StuckInfo;

typedef struct t_StuckCollection
{
  int countUpdated;
  int countProcessed;
  int isCompleted;
  int _padding;
  t_StuckInfo stuckList[MAX_STUCK_PER_SYMBOL];
} t_StuckCollection;

typedef struct t_FillInfo
{
  int shares;
  int _padding;
  double price;
} t_FillInfo;

typedef struct t_SymbolStuckMgmt
{
  pthread_mutex_t updateRealValueMutex;
  t_StuckCollection stuckMgmt[MAX_SIDE];
  t_FillInfo fillMgmt[MAX_SIDE];
} t_SymbolStuckMgmt;

// Be careful while updating this struct defition
// It's used in memcpy()
/*typedef struct t_BBOQuote
{
  char  participantId;  
  int   shares;
  double  price;
} t_BBOQuote;
*/

typedef struct t_BBOSnapShot
{
  int   bidSize;
  int   offerSize;
  double  bidPrice; 
  double  offerPrice;
  char  participantId;
  char  quoteCondition;
  char  isHalted;
} t_BBOSnapShot;

// Be careful while updating this struct defition
// It's used in memcpy()
typedef struct t_BBOQuoteEntry
{
  int crossID;
  unsigned short bboID; //0: Entry, 1 --> n: exit
  char _padding[2];
  t_BBOSnapShot BBOSnapShot[MAX_EXCHANGE];
} t_BBOQuoteEntry;

typedef struct t_BBOInfoMgmt
{
  t_BBOQuoteEntry BBOInfoCollection[MAX_BBO_ENTRY];
  FILE *fileDesc;
  int receivedIndex;
  int processedIndex;
  int tsIndex;
  int _padding;
} t_BBOInfoMgmt;

typedef struct t_dbBBOInfo
{
  int ts_id;
  int cross_id;
  short bbo_id;
  
  //  There are two bytes padding here
  
  char contents[512];
} t_dbBBOInfo;

/****************************************************************************
** Global variables definition
****************************************************************************/
extern t_BBOInfoMgmt BBOInfoMgmt[MAX_TRADE_SERVER_CONNECTIONS];

extern t_OutputLogMgmt tradeOutputLogMgmt[MAX_TRADE_SERVER_CONNECTIONS];

extern t_OutputLogProcessedMgmt positionOutputLogProcessed;

extern t_TradeOutputPointMgmt tradeOutputPoints;

extern t_CrossSummaryCollection asCrossCollection;

extern t_TSCrossCollection tsCrossCollection[MAX_TRADE_SERVER_CONNECTIONS];

extern t_TradeCollection tradeCollection;

extern t_SymbolDetectCross symbolDetectCrossList;

extern t_SymbolStuckMgmt symbolStuckMgmt[MAX_STOCK_SYMBOL];

extern pthread_mutex_t updateOutputLogMutex;

extern pthread_mutex_t calculationRealValueMutex;

/****************************************************************************
** Function declarations
****************************************************************************/
int InitializeOutputLogManegementStructures(void);

int InitialzeTradeOutputLogStructures(void);

int InitializeTSCrossIndex(void);

int InitializePositionOutputLogProcessedStructure(void);

int InitializeSymbolStuckStructure(void);

int InitializeSymbolDetectCrossList(void);

int InitializeTradeOutputCheckPoint(void);

int UpdateStockSymbolDetectCrossIndex(const char *stockSymbol);

int GetTSCrossIndex(int crossId, int tsIndex, int logType);

int GetTradeIndex(int crossId, int tsIndex);

int LoadCurrentCrossID(const char *fileName);

int SaveCurrentCrossID(int crossID, int tradeId);

int LoadPositionOutputLogProcessedFromFile(const char *fileName);
int SavePositionOutputLogProcessedToFile(void);

int LoadTradeOutputCheckPointFromFile(void);
int SaveTradeOutputCheckPointToFile(void);

int FindTradeOutputPointToRecovery(int timestamp);

int LoadTradeOutputLog(void);

int LoadTradeOutputLogFromFile(const char *fileName, void *logMgmt, int startIndex);

int ProcessHistoryTradeOutputLog(void);

int ProcessTradeOutputLog(void);

int ProcessTradeOutputLogFromCollection(int *countLogProcessed, void *outputLogMgmt);

int ProcessGroupTradeOutputLog(int tsIndex, int index);

double CalculateExpectedProfit(int tsIndex, int crossIndex, int expectedShares);
int CalculateAndInsertTradeResults(int tsIndex, int crossIndex, int tradeIndex, char *dateString, char *symbol, int timestamp);

int InsertAndCalculateRealValue(char *symbol, int side, t_StuckInfo *stuckInfo, char *dateString);

int CalculateRealValue(char *symbol, int side, int shares, double price, int account, double fillFee);

int ProcessBBOInfoFromCollection(void *bboQuotesMgmt);

int SavePositionBBOInfoProcessedToFile(void);
int LoadPositionBBOInfoProcessedFromFile(const char *fileName);
int ProcessHistoryBBOInfoFromFile(void);
int InitializeBBOManagementStructure(void);
int ProcessBBOInfo(void);
/***************************************************************************/

#endif
