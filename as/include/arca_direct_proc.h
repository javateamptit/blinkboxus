/****************************************************************************
** Project name:  Equity Arbitrage Application
** File name:     arca_direct_proc.h
** Description:   This file contains some declarations for arca_direct_proc.c

** Author:    Sang Nguyen-Minh
** First created on 20 September 2007
** Last updated on 20 September 2007
****************************************************************************/

#ifndef __ARCA_DIRECT_PROC_H__
#define __ARCA_DIRECT_PROC_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include "global_definition.h"
#include "utility.h"
#include <sys/time.h>
#include "socket_util.h"

#define ARCA_DIRECT_MSG_HEADER_LEN 4

// Receive timeout for ARCA DIRECT is in second(s)
#define ARCA_DIRECT_RECV_TIMEOUT 65

/*
Some message length and specific field index
that will be used usually
*/
#define NEW_ORDER_MSG_LEN_V1 76
#define NEW_ORDER_MSG_LEN_V3 164
#define CANCEL_ORDER_MSG_LEN 72

#define MSG_TYPE_INDEX 0
#define OUTGOING_SEQ_NUM_INDEX 8

// Message types are used by Client
#define NEW_ORDER_TYPE 'D'
#define CANCEL_ORDER_TYPE 'F'

// Message types are used by Server
#define LOGON_REJECTED_TYPE 'L'
#define ORDER_ACK_TYPE 'a'
#define ORDER_FILLED_TYPE '2'
#define CANCEL_ACK_TYPE '6'
#define ORDER_REJECTED_TYPE '8'
#define ORDER_KILLED_TYPE '4'
#define BUST_OR_CORRECT_TYPE 'C'

// Message types are used by both Client and Server
#define LOGON_TYPE 'A'
#define TEST_REQUEST_TYPE '1'
#define HEARTBEAT_TYPE '0'

// Message length are used by client after TS connected to ARCA DIRECT
#define HEARTBEAT_MSG_LEN 12
#define TEST_REQUEST_MSG_LEN 12
#define ORDER_ACK_MSG_LEN 48
#define ORDER_FILLED_MSG_LEN 88
#define CANCEL_ACK_MSG_LEN 40
#define ORDER_KILLED_MSG_LEN 40
#define ORDER_REJECTED_MSG_LEN 80
#define BUST_OR_CORRECT_MSG_LEN 48
#define LOGON_REJECTED_MSG_LEN 60
#define LOGON_TYPE_MSG_LEN 48

/*
Constant values are used to 
build messages to sent to Server
*/
#define ORDER_TYPE_MARKET '1'
#define ORDER_TYPE_LIMIT '2'

#define PRICE_SCALE '2'
// #define ISO_FLAG 'N'

// #define ACCOUNT "6930"
#define TRADING_SESSION_ID "123\0"
#define RULE80A 'P' //Change A to P, related to ticket 1112  

#define VERSION_PROFILE "L\001D\001G\001E\001a\0012\0016\0014\001\0"
#define EXEXCHANGE_DESTINATION 102

/****************************************************************************
** Data structure definitions
****************************************************************************/
typedef struct t_ARCA_DIRECT_Config
{
  int incomingSeqNum;
  int outgoingSeqNum;
  int enableTrading;
  int isConnected;
  int autoCancelOnDisconnect;
  
  int socket;
  int port;
  char ipAddress[MAX_LINE_LEN];
  
  char userName[MAX_LINE_LEN];
  char password[MAX_LINE_LEN];
  char compGroupID[6]; //Max length of Group ID: 5
} t_ARCA_DIRECT_Config;

typedef struct t_ARCA_DIRECT_NewOrder
{
  int clOrdID;
  char *symbol;
  char side;
  int orderQty;
  double price;
  char timeInForce;
  int maxFloor;
  char buyLaunch;
  int tradingAccount;
} t_ARCA_DIRECT_NewOrder;


typedef struct t_ARCA_DIRECT_CancelOrder
{
  int msgSeqNum;
  int origClOrdID;
  char symbol[SYMBOL_LEN];
  char side;
  int orderQty;
  long ecnOrderID;
  int accountSuffix;
} t_ARCA_DIRECT_CancelOrder;

/****************************************************************************
** Global variable declarations
****************************************************************************/
extern t_ARCA_DIRECT_Config ARCA_DIRECT_Config;

/****************************************************************************
** Function declarations
****************************************************************************/
int Init_ARCA_DIRECT_DataStructure();
void *StartUp_ARCA_DIRECT_Module(void *threadArgs);
void TalkTo_ARCA_DIRECT_Server(void);
void Add_ARCA_DIRECT_DataBlockToCollection(t_DataBlock *dataBlock);
int GetClOrderId_ARCA_DIRECT(const unsigned char *msgContent);
int Get_ARCA_DIRECT_OutgoingSeqNum(void);

// Session messages
int BuildAndSend_ARCA_DIRECT_LogonRequestMsg(void);
int BuildAndSend_ARCA_DIRECT_HeartbeatMsg(void);
int BuildAndSend_ARCA_DIRECT_TestRequestMsg(void);

// Application messages
int BuildAndSend_ARCA_DIRECT_OrderMsg_V1(t_ARCA_DIRECT_NewOrder *newOrder);
int BuildAndSend_ARCA_DIRECT_OrderMsg_V3(t_ARCA_DIRECT_NewOrder *newOrder);
int BuildAndSend_ARCA_DIRECT_CancelMsg(const t_ARCA_DIRECT_CancelOrder *cancelOrder);

int ProcessARCA_DIRECT_NewOrder(t_DataBlock *dataBlock);
int ProcessARCA_DIRECT_OrderAckMsg(t_DataBlock *dataBlock, int isTrading);
int ProcessARCA_DIRECT_OrderFilledMsg(t_DataBlock *dataBlock);
int ProcessARCA_DIRECT_OrderCancelAckMsg(t_DataBlock *dataBlock);
int ProcessARCA_DIRECT_OrderRejectedMsg(t_DataBlock *dataBlock);
int ProcessARCA_DIRECT_OrderKilledMsg(t_DataBlock *dataBlock);

int ProcessARCA_DIRECT_BustOrCorrectMsg(t_DataBlock *dataBlock);
int ProcessARCA_DIRECT_TestRequestMsg(t_DataBlock *dataBlock);
int ProcessARCA_DIRECT_HeartbeatMsg(t_DataBlock *dataBlock);

int ProcessARCA_DIRECT_LogonAcceptedMsg(t_DataBlock *dataBlock);
int ProcessARCA_DIRECT_LogonRejectedMsg(t_DataBlock *dataBlock);

int Send_ARCA_DIRECT_Msg(const char *message, int msgLen);
int Process_ARCA_DIRECT_Msg(t_DataBlock *dataBlock);
int Receive_ARCA_DIRECT_Msg(t_DataBlock* dataBlock);
int SaveARCA_DIRECT_SeqNumToFile(void);

void *Send_Test_Request_Thread(void *arg);

#endif
