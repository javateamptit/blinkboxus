/*****************************************************************************
**  Project:    Equity Arbitrage Application
**  Filename:   position_manager_proc.h
**  Description:  This file contains some declaration for position_manager_proc.c 
**  Author:     Luan Vo-Kinh
**  First created:  13-Sep-2007
**  Last updated: -----------
*****************************************************************************/

#ifndef __POSITION_MANAGER_PROC_H__
#define __POSITION_MANAGER_PROC_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include <stdlib.h>

#include "DaedalusMessage.pb-c.h"
#include "DaedalusRequest.pb-c.h"
#include "global_definition.h"
#include "order_mgmt_proc.h"
#include "utility.h"

//Response reason from PM for manual order
#define MANUAL_ORDER_RESPONSE_SENT 1
#define MANUAL_ORDER_RESPONSE_FAILED_TO_SEND 2
#define MANUAL_ORDER_RESPONSE_FAILED_TO_CHECK 3

#define DISENGAGE_REASON_CONSECUTIVE_REJECTS                              1
#define DISENGAGE_REASON_BEST_ASK_NOTHING                                 2
#define DISENGAGE_REASON_BEST_BID_NOTHING                                 3
#define DISENGAGE_REASON_BID_GT_ASK                                       4
#define DISENGAGE_REASON_INSIDE_SPREAD_GT_MAX_SPREAD_EXEC_LOW             5
#define DISENGAGE_REASON_INSIDE_SPREAD_GT_MAX_PROFIT_SPREAD               6
#define DISENGAGE_REASON_INSIDE_SPREAD_GT_MAX_BIG_LOSER_SPREAD_EXEC_LOW   7
#define DISENGAGE_REASON_INSIDE_SPREAD_GT_MAX_SPREAD_FM                   8
#define DISENGAGE_REASON_INSIDE_SPREAD_GT_MAX_BIG_LOSER_SPREAD_FM         9
#define DISENGAGE_REASON_BAD_PRICE_EXIT_SHORT_LIQUIDATE                   10
#define DISENGAGE_REASON_BAD_PRICE_EXIT_LONG_LIQUIDATE                    11
#define DISENGAGE_REASON_BAD_PRICE_EXIT_SHORT_BBO                         12
#define DISENGAGE_REASON_BAD_PRICE_EXIT_LONG_BBO                          13
#define DISENGAGE_REASON_CAN_NOT_FIND_SYMBOL                              14
#define DISENGAGE_REASON_SYMBOL_IGNORED                                   15
#define DISENGAGE_REASON_SYMBOL_HALTED                                    16
#define DISENGAGE_REASON_CANT_CANCEL_EXISTING_ORDER                       17
#define DISENGAGE_REASON_ZERO_SHARES                                      18
#define DISENGAGE_REASON_DISABLED_TRADING                                 19
#define DISENGAGE_REASON_UNABLE_TO_PLACE_ORDER_ON_DISCONNECTED_ECN        20
#define DISENGAGE_REASON_REJECTED_BY_PM                                 21
#define DISENGAGE_REASON_SYMBOL_FULL_FILLED                               22
#define DISENGAGE_REASON_TRADER_REQUESTED_IGNORED                         23
#define DISENGAGE_REASON_TRADER_REQUESTED_DISENGAGE                       24

#define PM_SERVICE_ARCA_BOOK    10000
#define PM_SERVICE_NASDAQ_BOOK    10001

#define PM_SERVICE_CQS_FEED     11000
#define PM_SERVICE_UQDF_FEED    11001
#define PM_SERVICE_CTS_FEED     11002
#define PM_SERVICE_UTDF_FEED    11003

#define PM_SERVICE_ARCA_DIRECT    20000
#define PM_SERVICE_NASDAQ_RASH    20001

#define STUCK_STATUS_REASON_DISABLED_TRADING      100
#define STUCK_STATUS_REASON_CAN_NOT_FIND_SYMBOL   101
#define STUCK_STATUS_REASON_SYMBOL_IGNORED        102
#define STUCK_STATUS_REASON_SYMBOL_HALTED         103
#define STUCK_STATUS_REASON_CAN_NOT_UPDATE_SYMBOL   104
#define STUCK_STATUS_REASON_NONE                  110

#define MSG_TYPE_FOR_RECEIVE_DISCONNECT_ALERT_FROM_PM       41530
#define MSG_TYPE_FOR_RECEIVE_DISABLED_TRADING_ALERT_FROM_PM     41531
#define MSG_TYPE_FOR_RECEIVE_GROUP_OF_BOOK_DATA_UNHANDLED_ALERT_FROM_PM 41532

#define MSG_TYPE_FOR_RECEIVE_POSITION_STATUS_RESPONSE_ALERT_FROM_PM 43510

#define MAX_DATA_BLOCK_BUFFER 50000
/****************************************************************************
** Data structures definitions
****************************************************************************/

typedef struct t_TradeList
{
  char symbol[SYMBOL_LEN];
  char status[TS_MAX_BOOK_CONNECTIONS];
} t_TradeList;

typedef struct t_TradeSymbol
{
  int countSymbol;
  t_TradeList symbolInfo[MAX_SYMBOL_TRADE];
} t_TradeSymbol;

typedef struct t_DataBlockBufferItem
{
  void *dataBlock;
  int ECNId;
} t_DataBlockBufferItem;

typedef struct t_DataBlockBufferMgt
{
  t_DataBlockBufferItem queue[MAX_DATA_BLOCK_BUFFER];
  int count;
} t_DataBlockBufferMgt;

/****************************************************************************
** Global variables definition
****************************************************************************/
extern t_DataBlockBufferMgt PMDataBlockBuffer;

/****************************************************************************
** Function declarations
****************************************************************************/

int Connect2PM(void);
int Disconnect2PM(void);
void *ProcessPM(void *pthread_arg);
int Talk2PM(void);
int ReceivePMMessage(void *message);
int ProcessPMMessageForRawData(void *message);
int ProcessPMMessageForCurrentStatus(void *message);
int ProcessPMMessageForBookConf(void *message);
int ProcessPMMessageForOrderConf(void *message);
int ProcessPMMessageForGlobalConf(void *message);
int ProcessPMMessageForIgnoreSymbol(void *message);
int BuildPMMessageForRawDataStatus(void *message);
int BuildPMMessageForEnableTrading(void *message);
int BuildPMMessageForDisableTrading(void *message, short reasonID);
int BuildPMMessageForHeartbeart(void *message);
int BuildPMMessageForConnectBook(void *message, int ECNIndex);
int BuildPMMessageForDisconnectBook(void *message, int ECNIndex);
int BuildPMMessageForConnectOrder(void *message, int ECNIndex);
int BuildPMMessageForDisconnectOrder(void *message, int ECNIndex);
int BuildPMMessageForSetPMBookConf(void *message, int ECNIndex);
int BuildPMMessageForSetPMOrderConf(void *message, int ECNIndex);
int BuildPMMessageForSetPMGlobalConf(void *message);
int BuildPMMessageForSetIgnoreStockList(void *message, int type, const char *symbol);
int SendPMMessage(void *message);
int SendRawDataStatusToPM(void);
int SendHeartBeatToPM(void);
int SendConnectBookToPM(int ECNIndex);
int SendDisconnectBookToPM(int ECNIndex);
int SendConnectOrderToPM(int ECNIndex);
int SendDisconnectOrderToPM(int ECNIndex);
int SetPMBookConfToPM(int ECNIndex);
int SetPMOrderConfToPM(int ECNIndex);
int SetPMGlobalConfToPM(void);
int SendEnableTradingPMToPM(void);
int SendDisableTradingPMToPM(short reasonID);
int SetPMIgnoreStockListToPM(int type, const char *symbol);
int ProcessPMConnectAllConnection(void);
int ProcessPMConnectAllECNBook(void);
int ProcessPMConnectAllECNOrder(void);
int ProcessPMDisconnectAllConnection(void);
int ProcessPMDisconnectAllECNBook(void);
int ProcessPMDisconnectAllECNOrder(void);
int SetPMConfToPM(void);
int BuildPMMessageForSetPMConf(void *message);
int SendGetSymbolStatusToPM(int ttIndex, unsigned char symbol[SYMBOL_LEN], char *listMdc);
int BuildPMMessageForGetSymbolStatus(void *message, int ttIndex, unsigned char symbol[SYMBOL_LEN], char *listMdc);
int ProcessPMMessageForSymbolStatus(void *message);
int BuildPMMessageForSendStuckInfo(void *message, const char *symbol, int numOfShares, double stuckPrice, int side, int crossID, int account);
int SendCancelRequestToPM(int clOrdId, char *ECNOrderId);
int BuildPMMessageForCancelRequest(void *message, int clOrdId, char *ECNOrderId);
int SendDisengageSymbolToPM(char *symbol, int account);
int BuildPMMessageForDisengageSymbol(void *message, char *symbol, int account);
int SetPMBookARCAConfToPM(int type, int groupIndex);
int BuildPMMessageForSetPMBookARCAConf(void *message, int type, int groupIndex);
int SendConnectCTS_FeedToPM(void);
int SendConnectCQS_FeedToPM(void);
int BuildPMMessageForConnectCTS_Feed(void *message);
int BuildPMMessageForConnectCQS_Feed(void *message);
int SendDisconnectCTS_FeedToPM(void);
int SendDisconnectCQS_FeedToPM(void);
int BuildPMMessageForDisconnectCTS_Feed(void *message);
int BuildPMMessageForDisconnectCQS_Feed(void *message);
int ProcessPMMessageForHaltStockList(void *message);
int ProcessPMMessageForHaltedSymbol(void *message);
int SendGetTradedListToPM(int ttIndex, char *symbol);
int BuildPMMessageForGetTradedList(void *message, int ttIndex, char *symbol);
int ProcessPMMessageForTradedList(void *message);
int SendConnectUTDFToPM(void);
int BuildPMMessageForConnectUTDF(void *message);
int SendDisconnectUTDFToPM(void);
int BuildPMMessageForDisconnectUTDF(void *message);
int ProcessPMMessageForBookIdleWarning(void *message);
int ProcessConnectToPMAndAllPMConnection(void);
int ProcessDisconnectToPMAndAllPMConnection(void);
int ProcessPMMessageForSleepingSymbolList(void *message);

//For UQDF
int SendConnectUQDF_FeedToPM(void);
int BuildPMMessageForConnectUQDF_Feed(void *message);
int SendDisconnectUQDF_FeedToPM(void);
int BuildPMMessageForDisconnectUQDF_Feed(void *message);

//For PM Exchange Configuration
int ProcessPMMessageForExchangeConf(void *message);
int SendSetPMExchangesConfToPM(int buffLen, const unsigned char *buffer);
int BuildPMExchangeMessageForSetPMExchangeConf(void *message);
int SetPMExchangeConfToPM(void);

//For PM BBO Viewer
int ProcessPMMessageForBBOQuotes(void *message);
int SendGetPMBBOQuotesToPM(char *symbol, int ttIndex);
int SendFlushPMBBOQuotesToPM(int exchangeId);

//For Flush PM book
int SendFlushASymbolToPM(int ECNIndex, unsigned char symbol[SYMBOL_LEN]);
int BuildPMMessageForFlushASymbol(void *message, int ECNIndex, unsigned char symbol[SYMBOL_LEN]);

//For flush PM all symbol 
int SendFlushAllSymbolToPM(int ECNIndex);
int BuildPMMessageForFlushAllSymbol(void *message, int ECNIndex);

int ProcessPMMessageForGetStuckInfoBeforeResumeHandlingStuck(void *message);
int ProcessPMMessageForManualOrderResponse(void *message);
int ProcessPMMessageForDisengageAlert(void *message);

int SendManualOrderToPM(t_SendNewOrder *newOrder, int sendID, int isForced, int ttIndex);
int BuildPMMessageForManualOrder(void *message, t_SendNewOrder *newOrder, int sendID, int isForced, int ttIndex);

int ProcessPMMessageForRashSeqNumber(void *message);
int SendRASHSeqNumberToPM(void);
int BuildPMMessageForRASHSeqNumber(void *message, int seqNumber);
int SendManualCancelRequestToPM(t_ManualCancelOrder *cancelOrder, int ECNId);
int BuildPMMessageForSetPMBookARCAFeed(void *message, char feedName);
int SetBookARCAFeedToPM(char feedName);
int EnqueueStuckOfSameSymbolToPM(char *symbol, int symbolIndex, int account);

int ProcessPMMessageForPositionStatusResponseAlert(void *message);
int ProcessPMMessageForDisabledTradingAlert(void *message);
int ProcessPMMessageForDisconnectAlert(void *message);
int ProcessPMMessageForGroupOfBookDataUnhandledAlert(void *message);

int CompareTimestampOfTwoDatablocks(t_DataBlockBufferItem *item1, t_DataBlockBufferItem *item2);
int SortDataBlockBufferInAscend(t_DataBlockBufferMgt *PMDataBlockBuffer, t_DataBlockBufferItem *ouput[MAX_DATA_BLOCK_BUFFER]);
int ProcessPMMessageForEndOfRawData(void);

int SendHaltStockListToPM(int updatedList[MAX_STOCK_SYMBOL], int updatedCount);
int BuildPMMessageForHaltedStockList(void *message, int updatedList[MAX_STOCK_SYMBOL], int updatedCount);
int SendAHaltStockToPM(const int stockSymbolIndex);
int BuildPMMessageForHaltSymbol(void *message, const int stockSymbolIndex);

int SendRequestPriceUpdateToPM(const int totalSymbol, char symbolList[][SYMBOL_LEN]);
int BuildPMMessageForRequestPriceUpdate(void *message, const int totalSymbol, char symbolList[][SYMBOL_LEN]);
int ProcessPMMessageForPriceUpdate(t_TSMessage *message);
/***************************************************************************/

#endif
