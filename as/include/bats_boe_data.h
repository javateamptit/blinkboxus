/*****************************************************************************
**  Project:    Equity Arbitrage Application
**  Filename:   bats_boe_data.h
**  Description:  This file contains some declaration for bats_boe_data.c
**  Author:     Dung Tran-Dinh
**  First created:  13-Sep-2007
**  Last updated: -----------
*****************************************************************************/

#ifndef __BATS_BOE_DATA_H__
#define __BATS_BOE_DATA_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include <stdlib.h>
#include <math.h>

#include "global_definition.h"
#include "utility.h"

// Data type and length of option fields...
#define ACCOUNT_LEN 16          // Text     - 16 bytes
#define ATTRIBUTE_QUOTE_LEN 1     // Alphanumeric - 1 byte
#define BASE_LIQUIDITY_INDICATOR_LEN 1  // Alphanumeric - 1 byte
#define CANCEL_ORIG_ON_REJECT_LEN 1   // Alpha    - 1 byte
#define CAPACITY_LEN 1          // Alpha    - 1 byte
#define CLEARING_ACC_LEN 4        // Text     - 4 bytes
#define CLEARING_FIRM_LEN 4       // Text     - 4 bytes
#define DISCRETION_AMOUNT_LEN 2     // Binary   - 2 bytes
#define DISPLAY_INDICATOR_LEN 1     // Alphanumeric - 1 byte
#define DISPLAY_PRICE_LEN 8       // Binary price - 8 bytes
#define EXEC_INST_LEN 1         // Text     - 1 byte
#define EXPIRE_TIME_LEN 8       // DateTime   - 8 bytes
#define LASTPX_LEN 8          // Binary price - 8 bytes
#define LASTSHARE_LEN 4         // Binary   - 4 bytes
#define LEAVES_QTY_LEN 4        // Binary   - 4 bytes
#define LOCATE_REQD_LEN 1       // Alpha    - 1 byte
#define MAX_FLOOR_LEN 4         // Binary   - 4 bytes
#define MAX_REMOVE_PCT_LEN 1      // Binary   - 1 byte
#define MIN_QTY_LEN 4         // Binary   - 4 bytes
#define ORDER_QTY_LEN 4         // Binary   - 4 bytes
#define ORDER_TYPE_LEN 1        // Alphanumeric - 1 byte
#define ORIG_CLORDERID_LEN 20     // Text     - 20 bytes
#define PEG_DIFF_LEN 8          // Signed binary price - 8 bytes
#define PREVENT_MEMBER_MATCH_LEN 3    // Alpha    - 3 bytes
#define PRICE_LEN 8           // Binary price - 8 bytes
#define ROUT_INST_LEN 4         // Text     - 4 bytes
#define SECONDARY_ORDERID_LEN 8     // Binary   - 8 bytes
#define SIDE_LEN 1            // Binary   - 1 byte
#define SYMBOL_SFX_LEN 8          // Alphanumeric - 8 bytes
#define TIME_IN_FORCE_LEN 1       // Alphanumeric - 1 byte
#define WORKING_PRICE_LEN 8       // Binary price - 8 bytes

// Starting offset of option field in orders
#define STARTING_OFFSET_OF_NEW_ORDER 41
#define STARTING_OFFSET_OF_ACK_ORDER 54
#define STARTING_OFFSET_OF_REJECTED_ORDER 107
#define STARTING_OFFSET_OF_CANCEL_REQUEST 32
#define STARTING_OFFSET_OF_CANCELED_ORDER 47
#define STARTING_OFFSET_OF_REJECTED_CANCEL 107
#define STARTING_OFFSET_OF_FILL_ORDER 84
#define STARTING_OFFSET_OF_BROKEN_TRADE 100

/****************************************************************************
** Data structures definitions
****************************************************************************/
// The structure is used contain several fields of New Order
typedef struct t_BATS_BOENewOrder
{
  double price;
  double pegDifference;
  
  int seqNum;
  int orderID;
  int tsId;
  int crossId;
  int tradeType;
  int shares;
  int leftShares;
  int tradingAccount;
  int maxFloor;
  int visibleShares;

  unsigned short entryExitRefNum;
  short bboId;
  
  char date[12];
  char timestamp[16];
  char symbol[SYMBOL_LEN];
  char msgType[16];
  char entryExit[6];
  char askBid[4];
  char ecnLaunch[6];
  char routingInst[4];
  char ecn[6];
  char timeInForce[4];
  char account[6];
  char side[3];
  char clrFirm[6];
  char status[16];
  char execInst;
  char rule80A;
  char orderType;
  char isoFlag;

  char msgContent[MAX_ORDER_MSG];
} t_BATS_BOENewOrder;

// The structure is used contain several fields of Accepted Order
typedef struct t_BATS_BOEAcceptedOrder
{
  double price;
  
  long batsExOrderId;
  
  int clOrderID;
  int orgClOrderID;
  int seqNum;
  int shares;
  
  char date[12];
  char timestamp[16];
  char processedTimestamp[16];
  char msgType[16];
  char transTime[20];
  char symbol[SYMBOL_LEN];
  char clrFirm[6];
  char clrAccount[6];
  char timeInForce[4];
  char status[16];
  char side[3];
  
  char msgContent[MAX_ORDER_MSG];
} t_BATS_BOEAcceptedOrder;

// The structure is used contain several fields of Executed Order
typedef struct t_BATS_BOEExecutedOrder
{
  double lastPx;
  double price;
  double avgPrice;
  double fillFee;
  double accessFee;
  
  long execID;
  long batsExOrderId;
  
  int lastShares;
  int clOrderID;
  int seqNum;
  int shares;
  int leavesShares;
  
  unsigned short entryExitRefNum;
  
  char date[12];
  char timestamp[16];
  char processedTimestamp[16];
  char transTime[20];
  char msgType[16];
  char symbol[SYMBOL_LEN];
  char clrFirm[6];
  char clrAccount[6];
  char baseLiquidityIndicator;
  char subLiquidityIndicator;
  char controBroker[5];
  char side[3];
  char status[16];
  char msgContent[MAX_ORDER_MSG];
} t_BATS_BOEExecutedOrder;

// The structure is used contain several fields of Canceled Order
typedef struct t_BATS_BOECanceledOrder
{
  long batsExOrderId;
  
  int seqNum;
  int clOrderId;
  int orgClOrderId;
  
  unsigned short entryExitRefNum;
  
  char date[12];
  char transTime[20];
  char timestamp[16];
  char processedTimestamp[16];
  char msgType[16];
  char clrAccount[6];
  char account[6];
  char status[16];
  
  char reason;
  char clrFirm[6];
  
  char msgContent[MAX_ORDER_MSG];
} t_BATS_BOECanceledOrder;

// The structure is used contain several fields of Cancel Request
typedef struct t_BATS_BOECancelRequest
{
  long batsExOrderId;
  
  int orgClOrderID;
  int seqNum;
  
  char date[12];
  char timestamp[16];
  char msgType[16];
  char clrFirm[6];
  char status[16];
  
  char msgContent[MAX_ORDER_MSG];
} t_BATS_BOECancelRequest;

// The structure is used contain several fields of Rejected Order
typedef struct t_BATS_BOERejectedOrder
{
  long batsExOrderId;
  
  int clOrderID;
  int seqNum;
  
  unsigned short entryExitRefNum;
  
  char date[12];
  char timestamp[16];
  char processedTimestamp[16];
  char transTime[20];
  char msgType[16];
  char symbol[SYMBOL_LEN];
  char account[6];
  char clrFirm[6];
  char reason;
  char clrAccount[6];
  char text[61];
  char status[16];
  
  char msgContent[MAX_ORDER_MSG];
} t_BATS_BOERejectedOrder;

// The structure is used contain several fields of Rejected Cancel
typedef struct t_BATS_BOERejectedCancel
{
  long batsExOrderId;
  
  int orderID;
  int seqNum;
  
  char date[12];
  char timestamp[16];
  char transTime[20];
  char msgType[16];
  char status[16];
  char reason;
  char text[61];
  
  char msgContent[MAX_ORDER_MSG];
} t_BATS_BOERejectedCancel;

// The structure is used contain several fields of Broken Order
typedef struct t_BATS_BOEBrokenOrder
{
  double lastPx;
  double correctedPrice;
  
  long batszOrderID;
  long executionId;
  
  int orderID;
  int seqNum;
  int lastshares;
  
  char date[12];
  char timestamp[16];
  char transTime[20];
  char msgType[16];
  char orgTime[20];
  char clrFirm[6];
  char clrAccount[6];
  char symbol[SYMBOL_LEN];
  char side;
  char baseLiquidity;
  char status[16];
  
  char msgContent[MAX_ORDER_MSG];
} t_BATS_BOEBrokenOrder;

typedef struct t_BitStatus
{
  _Bool bit1: 1;
  _Bool bit2: 1;
  _Bool bit3: 1;
  _Bool bit4: 1;
  _Bool bit5: 1;
  _Bool bit6: 1;
  _Bool bit7: 1;
  _Bool bit8: 1;
}t_BitStatus;

typedef union t_BitField
{
  struct t_BitStatus bit;
  unsigned char value;
}t_BitField;

/****************************************************************************
** Global variables definition
****************************************************************************/
/****************************************************************************
** Function declarations
****************************************************************************/
int ParseRawDataForBATS_BOEOrder(t_DataBlock dataBlock, int type, int oecType);

int ProcessNewOrderOfBATS_BOEOrder(t_DataBlock dataBlock, int type, char *timeStamp, int oecType);

int ProcessAcceptedOrderOfBATS_BOEOrder(t_DataBlock dataBlock, int type, char *timeStamp, int oecType);

int ProcessExecutedOrderOfBATS_BOEOrder(t_DataBlock dataBlock, int type, char *timeStamp, int oecType);

int ProcessCanceledOrderOfBATS_BOEOrder(t_DataBlock dataBlock, int type, char *timeStamp, int oecType);

int ProcessCancelRequestOfBATS_BOEOrder(t_DataBlock dataBlock, int type, char *timeStamp, int oecType);

int ProcessRejectedCancelOfBATS_BOEOrder(t_DataBlock dataBlock, int type, char *timeStamp, int oecType);

int ProcessAcceptedCancelOfBATS_BOEOrder(t_DataBlock dataBlock, int type, char *timeStamp, int oecType);

int ProcessRejectedOrderOfBATS_BOEOrder(t_DataBlock dataBlock, int type, char *timeStamp, int oecType);

int ProcessBrokenOrderOfBATS_BOEOrder(t_DataBlock dataBlock, int type, char *timeStamp, int oecType);

int ProcessOrderRestatedOfBATS_BOEOrder(t_DataBlock dataBlock, int type, char *timeStamp, int oecType);


/***************************************************************************/

#endif
