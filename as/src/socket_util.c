/*****************************************************************************
**  Project:    Equity Arbitrage Application
**  Filename:   socket_util.c
**  Description:  This file contains function definitions that were declared
          in socket_util.h  
**  Author:     Luan Vo-Kinh
**  First created:  13-Sep-2007
**  Last updated: -----------
*****************************************************************************/

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include <poll.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

#include "socket_util.h"

/****************************************************************************
** Global variables definition
****************************************************************************/

/****************************************************************************
** Function declarations
****************************************************************************/

/****************************************************************************
- Function name:  Connect2Server
- Input:      - ip
          - port
- Output:     -1 if failed, socket number otherwise
- Return:     N/A
- Description:    
- Usage:      Use this function to create to a server
****************************************************************************/
int Connect2Server(char *ip, int port, const char* serverName)
{
  int sock, rc, valopt; 
  struct sockaddr_in localAddr, servAddr; 
  struct hostent *h;
  long arg;
  fd_set myset;
  socklen_t lon;
  
  h = gethostbyname(ip);
  
  if (h == NULL) 
  { 
    TraceLog(ERROR_LEVEL, "%s: Unknown host '%s'\n", serverName, ip);
    return -1; 
  } 
  
  servAddr.sin_family = h->h_addrtype; 
  memcpy((char *) &servAddr.sin_addr.s_addr, h->h_addr_list[0], h->h_length); 
  servAddr.sin_port = htons(port); 
  
  //Create socket
  sock = socket (AF_INET, SOCK_STREAM, 0); 
  
  if (sock < 0) 
  { 
    TraceLog(ERROR_LEVEL, "%s: Cannot open socket\n", serverName); 
    PrintErrStr(ERROR_LEVEL, "Calling socket(): ", errno);
    return -1; 
  }

  // Set non-blocking 
  arg = fcntl(sock, F_GETFL, NULL); 
  arg |= O_NONBLOCK; 
  fcntl(sock, F_SETFL, arg);
  
  //Bind to any port number
  localAddr.sin_family = AF_INET; 
  localAddr.sin_addr.s_addr = htonl(INADDR_ANY); 
  localAddr.sin_port = htons(0); 

  struct timeval tv;
  tv.tv_sec = 120000;
  tv.tv_usec = 0;
  
  int test = setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv, sizeof(tv));
  
  if (test < 0)
  {
    TraceLog(ERROR_LEVEL, "Can not set socket receive timeout\n");
    PrintErrStr(ERROR_LEVEL, "Calling setsockopt(): ", errno);

    exit(1);
  }
  
  rc = bind (sock, (struct sockaddr *) &localAddr, sizeof(localAddr)); 
  
  if (rc < 0) 
  {
    TraceLog(ERROR_LEVEL, "%s: Cannot bind to TCP port %u\n", serverName, port);
    close (sock);
    return -1;
  } 
  
  //Connect to server
  rc = connect(sock, (struct sockaddr *) &servAddr, sizeof(servAddr)); 
  
  if (rc < 0) 
  { 
    sleep(1);

    if (errno == EINPROGRESS)
    { 
      tv.tv_sec = 15; 
      tv.tv_usec = 0;
      FD_ZERO(&myset); 
      FD_SET(sock, &myset); 
      
      if (select(sock + 1, NULL, &myset, NULL, &tv) > 0)
      {
         lon = sizeof(int);
         
         getsockopt(sock, SOL_SOCKET, SO_ERROR, (void*)(&valopt), &lon); 
         
         if (valopt)
         {
          TraceLog(ERROR_LEVEL, "%s: No route to host\n", serverName);

          return -1; 
         } 
      } 
      else
      {
         TraceLog(ERROR_LEVEL, "%s: Connection timeout\n", serverName);

         return -1; 
      } 
    }
     else
    {
      TraceLog(ERROR_LEVEL, "%s: No route to host\n", serverName);

      return -1;
    } 
  }
  
  // Set to blocking mode again... 
  arg = fcntl(sock, F_GETFL, NULL); 
  arg &= (~O_NONBLOCK);
  fcntl(sock, F_SETFL, arg);

  TraceLog(DEBUG_LEVEL, "%s: Connection established.\n", serverName);
  return sock;
}

/****************************************************************************
- Function name:  Listen2Client
- Input:      - port
          - maxClient
- Output:     N/A
- Return:     -1 if failed, socket number that the server is listening
- Description:    Listen to a client
- Usage:      Please pass the correct parameters.
****************************************************************************/
int Listen2Client(int port, int maxClient)
{
  // Check valid port
  if (CheckValidPort(port) == ERROR)
  {
    return ERROR;
  }
  
    int serverSocket;
  struct sockaddr_in servAddr;

  // Create socket
  serverSocket = socket(AF_INET, SOCK_STREAM, 0);
  
  if (serverSocket < 0) 
  {
    TraceLog(ERROR_LEVEL, "Cannot open socket\n");
    PrintErrStr(ERROR_LEVEL, "Calling socket(): ", errno);

    return ERROR;
  }

  // Set SO_REUSEADDR on a socket to true (1):
  int optval = 1;
  
  if (setsockopt(serverSocket, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof optval) != SUCCESS)
  {
    TraceLog(ERROR_LEVEL, "Cannot set SO_REUSEADDR for socket (%d)\n", port);

    return ERROR;
  }

  // Bind server port
  servAddr.sin_family = AF_INET;
  servAddr.sin_addr.s_addr = htonl(INADDR_ANY);
  servAddr.sin_port = htons(port);
  
  if (bind(serverSocket, (struct sockaddr *) &servAddr, sizeof(servAddr)) < 0)
  {
    TraceLog(ERROR_LEVEL, "Cannot bind to the provided port (%d)\n", port);
    PrintErrStr(ERROR_LEVEL, "Calling bind(): ", errno);

    close(serverSocket);

    return ERROR;
  }

  // We allow at most concurrent clients
  if (listen(serverSocket, maxClient) == -1)
  {
    return ERROR;
  }
  
  return serverSocket;
}

/****************************************************************************
- Function name:  CheckValidPort
- Input:      + port
- Output:   N/A
- Return:   Success or Failure
- Description:  + The routine hides the following facts
          - check port is valid or not
        + There are preconditions guaranteed to the routine
        + The routine guarantees that the status value will 
          have a value of either
          - Success
          or
          - Failure
- Usage:      N/A
****************************************************************************/
int CheckValidPort(int port)
{
  if (port < MIN_PORT || port > MAX_PORT)
  {
    return ERROR;
  }
  else
  {
    return SUCCESS;
  }
}

/****************************************************************************
- Function name:  SetSocketRecvTimeout
- Input:      + socket
        + seconds
- Output:   Internal socket data will be updated
- Return:   NULL or pointer to fullConfigPath
- Description:  + The routine hides the following facts
          - Set receive timeout for socket
        + There are no preconditions guaranteed to the routine
        + The routine guarantees that the status value will 
          have a value of either
          - Succes
          or
          - Failure
- Usage:      N/A
****************************************************************************/
int SetSocketRecvTimeout(int socket, int seconds)
{
  struct timeval tv;
  tv.tv_sec = seconds;
  tv.tv_usec = 0;
  
  socklen_t optlen = sizeof(tv);
  
  return setsockopt(socket, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv, optlen);
}

/****************************************************************************
- Function name:  IsSocketAlive
- Input:      - socket: the socket which you want to check
- Output:     N/A
- Return:     - 0 if the socket still connected, -1 otherwise
- Description:    
- Usage:      Call this function when you want to check a socket status 
          of socket (connected or not)
****************************************************************************/
int IsSocketAlive(int socket)
{
  if (socket == -1)
  {
    return -1;
  }
  
  int pollret;
  struct pollfd sdinfo;

  sdinfo.fd = socket;
  sdinfo.events = POLLHUP;

  //check the socket for POLLHUP event with 1 miliseconds of timeout
  pollret = poll(&sdinfo, 1, 1);
  
  if ((pollret > 0) && (sdinfo.revents == POLLHUP))
  {
    //indicate that the socket is not alive 
    //printf("Socket is not alive anymore \n"); 
    return -1;
  }

  //indicate that the socket is still alive 
  return 0;
}

/****************************************************************************
- Function name:  CloseConnection
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int CloseConnection(void *pConnection)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  connection->status = DISCONNECT;  
  
  if (connection->socket != -1)
  {
    shutdown(connection->socket, 2);
    close(connection->socket);
  }
  
  connection->socket = -1;
  connection->statusUpdate = -1;
  connection->currentState = OFFLINE;
  connection->authenticationMethod = UNDEFINE_AUTHENTICATION_METHOD;
  connection->clientType = CLIENT_TOOL_UNDEFINED;
  memset(connection->userName, 0, MAX_LINE_LEN);
  memset(connection->password, 0, MAX_LINE_LEN);
  
  return SUCCESS;
}

int SocketSend(int socket, const char *message, int msgLen)
{         
  ssize_t bytesSent = 0;
  while(bytesSent < msgLen) {
    bytesSent = send(socket, message, msgLen, MSG_NOSIGNAL);
    if(bytesSent < 0) {
      if(errno != EAGAIN) {
        TraceLog(ERROR_LEVEL, "error: error sending to socket: %d\n", socket);
        perror("error: could not socket send data");
        return ERROR;
      } else {
         TraceLog(DEBUG_LEVEL, "could not socket send data, trying again, sent bytes: %zu\n", bytesSent);
      }
    } else if(bytesSent < msgLen) {
      TraceLog(DEBUG_LEVEL, "could not socket send data, trying again, sent bytes: %zu\n", bytesSent);
    }
  }
  
  if(bytesSent != msgLen)
    return ERROR;
             
  return SUCCESS;
}

/***************************************************************************/
