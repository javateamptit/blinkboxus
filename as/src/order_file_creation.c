#define _GNU_SOURCE
#include <string.h>
#include <errno.h>
#include <math.h>
#include <stdlib.h>
#include <limits.h>
#include <pthread.h>

#include "order_file_creation.h"
#include "utility.h"
#include "global_definition.h"
#include "configuration.h"
#include "order_mgmt_proc.h"
#include "database_util.h"
#include "database_config.h"
#include "raw_data_mgmt.h"
#include "hbitime.h"

#define OJ_FILENAME "orders.jrnl"
#define ISO_FLIGHT_FILENALE "iso_snapshots.log"
#define TIME_SERIES_FILENAME "time_series.log"

#define __SERVER_AS 0
#define __SERVER_TS 1
#define __SERVER_PM 3

#define __MAX_VENUE_MAPPING 17
char MD_Mappings[__MAX_VENUE_MAPPING][32];  //[ID][venue name]
char OE_Mappings[MAX_ORDER_TYPE][32];  //[ID][venue name]
char INDEX_TO_VENUE_ID[MAX_EXCHANGE];
char PARTICIPANT_TO_INDEX[26];

pthread_mutex_t ISOFlight_Mutex;
PGconn *ISOFlightConn;

int hasChangedExposure = 0;
// ------------------------------------------
//        OJ FILE RELATED
// ------------------------------------------

// RGM Order Type
#define RGM_NEW_ORDER 1
#define RGM_ORDER_ACCEPTED 2
#define RGM_ORDER_FILL 3
#define RGM_ORDER_CANCEL 4
#define RGM_ORDER_CANCEL_RESPONSE 5
#define RGM_ORDER_REJECT 6

// Fixed value
#define OJ_SOURCE "eaa"

//------------------------------------------
int ProcessOJRecordForArcaDirect(t_DataBlock dataBlock, const int isFromWaitlist);
int ProcessOJRecordForOuch(t_DataBlock dataBlock, const int isFromWaitlist, const int venueType);
int ProcessOJRecordForRash(t_DataBlock dataBlock, const int isFromWaitlist);
int ProcessOJRecordForNyseCCG(t_DataBlock dataBlock, const int isFromWaitlist);
int ProcessOJRecordForBatsBOE(t_DataBlock dataBlock, const int isFromWaitlist, const int venueType);
int ProcessOJRecordForEdge(t_DataBlock dataBlock, const int isFromWaitlist, const int venueType);

void FindAndProcessRawdataInOJWaitList(int venue, int orderId);
int AddOJRawdataToWaitList(t_DataBlock *dataBlock);
//------------------------------------------
#define MAX_OJ_OPEN_ORDER 1000000
#define MAX_OJ_RAWDATA_WAITLIST 1000

//Struct store order information
typedef struct t_OJOpenOrder
{
  char symbol[SYMBOL_LEN];
  long batsExOrderID;
  int leftShares;
  int canceledShares;
  char buysell;
  char capacity;
} t_OJOpenOrder;


typedef struct t_OJRawdataWaitList
{
  t_DataBlock dataBlock[MAX_OJ_RAWDATA_WAITLIST];
  int dataBlockFlag[MAX_OJ_RAWDATA_WAITLIST];
  int uBound; //highest valid index in dataBlock[MAX_OJ_RAWDATA_WAITLIST]
} t_OJRawdataWaitList;


t_OJOpenOrder OJOpenOrder[MAX_OJ_OPEN_ORDER];
t_OJRawdataWaitList OJRawdataWaitList;
t_OJBrokenCollection OJBrokenMsgList;
t_ISOOpenCrossMgmt ISOOpenCrossMgmt[MAX_TRADE_SERVER_CONNECTIONS];

FILE *fpOJ, *fpISOFlight, *fpTimeSeries;
char OJFilePath[512];
char ISOFlightFilePath[512];
char TimeSeriesFilePath[512];

t_LinkedlistExecution *__exec = NULL;
t_LinkedlistOrderId *__new = NULL;
t_LinkedlistOrderId *__ack = NULL;
t_LinkedlistOrderId *__cancel = NULL;
t_LinkedlistOrderId *__reject = NULL;

void _________initialize_________(void){};

/****************************************************************************
- Function name:  InitVenueMappings
- Description:    
- Usage:      
****************************************************************************/
void InitVenueMappings()
{
  strcpy (MD_Mappings[0],  "isld");
  strcpy (MD_Mappings[1],  "arca");
  strcpy (MD_Mappings[2],  "bats");
  strcpy (MD_Mappings[3],  "edgx");
  strcpy (MD_Mappings[4],  "edga");
  strcpy (MD_Mappings[5],  "nyseultra");
  strcpy (MD_Mappings[8],  "finraadf_sip");
  strcpy (MD_Mappings[9],  "bx");
  strcpy (MD_Mappings[10], "nsx_sip");
  strcpy (MD_Mappings[12], "chx_sip");
  strcpy (MD_Mappings[13], "cbsx_sip");
  strcpy (MD_Mappings[14], "psx");
  strcpy (MD_Mappings[15], "amexultra");
  strcpy (MD_Mappings[16], "byx");

  strcpy (OE_Mappings[VENUE_ARCA],  "arca");
  strcpy (OE_Mappings[VENUE_OUCH],  "isld");
  strcpy (OE_Mappings[VENUE_RASH],  "rash");
  strcpy (OE_Mappings[VENUE_CCG],   "nyse");
  strcpy (OE_Mappings[VENUE_BZX],   "bats");
  strcpy (OE_Mappings[VENUE_EDGX],  "edgx");
  strcpy (OE_Mappings[VENUE_EDGA],  "edga");
  strcpy (OE_Mappings[VENUE_OUBX],  "bx");
  strcpy (OE_Mappings[VENUE_BYX],   "byx");
  strcpy (OE_Mappings[VENUE_PSX],   "psx");
}

/****************************************************************************
- Function name:  InitOrderFilesCreation
- Description:    
- Usage:      
****************************************************************************/
int InitOrderFilesCreation()
{
  InitVenueMappings();

  memset (OJOpenOrder, 0, sizeof(OJOpenOrder));
  memset (&OJBrokenMsgList, 0, sizeof(t_OJBrokenCollection));
  fpOJ = NULL;
  
  //------------------------------------------------------------------
  //        Open OJ file descriptor for writing
  //------------------------------------------------------------------
  if (GetFullRawDataPath(OJ_FILENAME, OJFilePath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Have some problem in get full raw data path operation (%s)\n", OJ_FILENAME);

    return ERROR;
  }
  
  if (IsFileTimestampToday(OJFilePath) == 0)
  {
    TraceLog(ERROR_LEVEL, "%s is old, please check the correctness of data files\n", OJFilePath);
    exit(0);
  }
  
  fpOJ = fopen(OJFilePath, "a+");
  if (fpOJ == NULL)
  {
    TraceLog(ERROR_LEVEL, "Could not open file %s for writing\n", OJFilePath);
    return ERROR;
  }
    
  return SUCCESS;
}

/****************************************************************************
- Function name:  InitISOFilesCreation
- Description:    
- Usage:      
****************************************************************************/
int InitISOFilesCreation()
{
  InitVenueMappings();

  int i, j;

  for (i = 0; i < MAX_EXCHANGE; i++)
  {
    INDEX_TO_VENUE_ID[i] = -1;
  }
  
  for (i = 0; i < MAX_TRADE_SERVER_CONNECTIONS; i++)
  {
    memset(&ISOOpenCrossMgmt[i], 0, sizeof(t_ISOOpenCrossMgmt));
    ISOOpenCrossMgmt[i].bound = 0;
    
    for (j = 0; j<MAX_ISO_OPEN_CROSS; j++)
    {
      ISOOpenCrossMgmt[i].crosses[j].crossID = -1;
      ISOOpenCrossMgmt[i].crosses[j].bboID = -1;
      ISOOpenCrossMgmt[i].crosses[j].isComplete = 1;
      ISOOpenCrossMgmt[i].crosses[j].waitingCount = 0;
    }
  }
  
  INDEX_TO_VENUE_ID[TS_ARCA_BOOK_INDEX] = 1;    // Arca
  INDEX_TO_VENUE_ID[TS_NASDAQ_BOOK_INDEX] = 0;  // Nasdaq
  INDEX_TO_VENUE_ID[TS_NYSE_BOOK_INDEX] = 5;    // NYSE
  INDEX_TO_VENUE_ID[TS_BATSZ_BOOK_INDEX] = 2;   // BATSZ
  INDEX_TO_VENUE_ID[TS_EDGX_BOOK_INDEX] = 3;    // EDGX
  INDEX_TO_VENUE_ID[TS_EDGA_BOOK_INDEX] = 4;  // EDGA
  INDEX_TO_VENUE_ID[TS_NDBX_BOOK_INDEX] = 9;  // Nasdaq BX
  INDEX_TO_VENUE_ID[7] = -1;    // National_Stock_Exchange (defunct)
  INDEX_TO_VENUE_ID[8] = 8;     // FINRA_ADF (LavaFlow)
  INDEX_TO_VENUE_ID[9] = -1;    // 
  INDEX_TO_VENUE_ID[10] = -1;   // International_Securities_Exchange (defunct)
  INDEX_TO_VENUE_ID[11] = 12;   // Chicago stock exchange
  INDEX_TO_VENUE_ID[12] = 15;   // AMEX
  INDEX_TO_VENUE_ID[13] = -1;   // Chicago_Board_Options_Exchange (defunct)
  INDEX_TO_VENUE_ID[14] = 14;   // PSX
  INDEX_TO_VENUE_ID[15] = 16;   // BATS_Y_Exchange
  
  PARTICIPANT_TO_INDEX['P' - 65] = TS_ARCA_BOOK_INDEX;    // Arca book
  PARTICIPANT_TO_INDEX['T' - 65] = TS_NASDAQ_BOOK_INDEX;    // Nasdaq book
  PARTICIPANT_TO_INDEX['Q' - 65] = TS_NASDAQ_BOOK_INDEX;    // Nasdaq book
  PARTICIPANT_TO_INDEX['N' - 65] = TS_NYSE_BOOK_INDEX;    // NYSE ULTRA
  PARTICIPANT_TO_INDEX['Z' - 65] = TS_BATSZ_BOOK_INDEX;   // BOOK_BATSZ;
  PARTICIPANT_TO_INDEX['K' - 65] = TS_EDGX_BOOK_INDEX;    // EDGX_Exchange
  PARTICIPANT_TO_INDEX['J' - 65] = TS_EDGA_BOOK_INDEX;    // EDGA_Exchange
  PARTICIPANT_TO_INDEX['B' - 65] = TS_NDBX_BOOK_INDEX;    // Nasdaq BX;
  PARTICIPANT_TO_INDEX['C' - 65] = 7;   // National_Stock_Exchange;
  PARTICIPANT_TO_INDEX['D' - 65] = 8;   // FINRA_ADF;
  PARTICIPANT_TO_INDEX['E' - 65] = 9;   // Market_Independent;
  PARTICIPANT_TO_INDEX['I' - 65] = 10;  // International_Securities_Exchange;
  PARTICIPANT_TO_INDEX['M' - 65] = 11;  // Chicago_Stock_Exchange;
  PARTICIPANT_TO_INDEX['A' - 65] = 12;  // NYSE_Amex;
  PARTICIPANT_TO_INDEX['W' - 65] = 13;  // Chicago_Board_Options_Exchange;
  PARTICIPANT_TO_INDEX['X' - 65] = 14;  // Philadelphia_Stock_Exchange;
  PARTICIPANT_TO_INDEX['Y' - 65] = 15;  // BATS_Y_Exchange
  
  fpISOFlight = NULL;
  
  //------------------------------------------------------------------
  //        Open ISO Flight file descriptor for writing
  //------------------------------------------------------------------
  if (GetFullRawDataPath(ISO_FLIGHT_FILENALE, ISOFlightFilePath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Have some problem in get full raw data path operation (%s)\n", ISO_FLIGHT_FILENALE);

    return ERROR;
  }
  
  if (IsFileTimestampToday(ISOFlightFilePath) == 0)
  {
    TraceLog(ERROR_LEVEL, "%s is old, please check the correctness of data files\n", ISOFlightFilePath);
    exit(0);
  }
  
  fpISOFlight = fopen (ISOFlightFilePath, "a+");
  if (fpISOFlight == NULL)
  {
    TraceLog(ERROR_LEVEL, "Could not open file %s for writing\n", ISOFlightFilePath);
    return ERROR;
  }
  
  pthread_mutex_init (&ISOFlight_Mutex, NULL);
  return SUCCESS;
}

/****************************************************************************
- Function name:  
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int InitTimeSeriesFormatEZG()
{
  //Open Time Series Format file descriptor for writing
  if (GetFullRawDataPath(TIME_SERIES_FILENAME, TimeSeriesFilePath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Have some problem in get full raw data path operation (%s)\n", TIME_SERIES_FILENAME);

    return ERROR;
  }
  
  if (IsFileTimestampToday(TimeSeriesFilePath) == 0)
  {
    TraceLog(ERROR_LEVEL, "%s is old, please check the correctness of data files\n", TimeSeriesFilePath);
    exit(0);
  }
  
  fpTimeSeries = fopen(TimeSeriesFilePath, "a+");
  if (fpTimeSeries == NULL)
  {
    TraceLog(ERROR_LEVEL, "Could not open file %s for writing\n", TimeSeriesFilePath);
    return ERROR;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadBreakMsgInOJ
- Input:      
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int LoadBreakMsgInOJ(void)
{
  if (fpOJ != NULL)
  {
    fclose(fpOJ);
    fpOJ = NULL;
  }
  
  fpOJ = fopen(OJFilePath, "r");
  if (fpOJ == NULL)
  {
    TraceLog(ERROR_LEVEL, "Could not open file %s for reading\n", OJFilePath);
    return ERROR;
  }
  
  char buffer[MAX_LINE_LEN_OF_OJ] = "\0";
  char *tempBuffer, *fieldValue;
  int orderId;
  long execId;
  
  // Read all messages and find Break message
  while (fgets(buffer, MAX_LINE_LEN_OF_OJ, fpOJ) != NULL)
  {
    // Check break message
    tempBuffer = strcasestr(buffer, "RGMBrokenTradeMsg");
    
    if (tempBuffer != NULL)
    {
      /*
        [type=busclient::RGMBrokenTradeMsg tm=1353423910.517828 source=eaa 
          symbol=TOL venue=rash buysell=B price=30.8000 size=-900. 
          rgmoid=EAA501127 liquidity=82 venue_oid='' 
          reason_code=1 order_complete=1 publisher=eaa-pm 
          properties=[version=3 strings=[Liquidity=R 
          VenueTradeId=38535168 account=9823 capacity=P 
          clearing_firm=Fortis creator=blinkbox execution_firm=RGMS 
          rgm_mpid=HBIF] codables=[BasisCurrency=@[[type=market_interface::MiBasisCurrency 
          currency=USD multiplier=1. usd_mult=1.]]]]]
      */
      
      // Get value field of rgmoid
      fieldValue = strcasestr(tempBuffer, "rgmoid=EAA");
      if (fieldValue != NULL)
      {
        orderId = atoi(&fieldValue[10]);
      }
      else
      {
        TraceLog(WARN_LEVEL, "Can't find rgmoid\n");
        break;
      }
      
      // Get value field of VenueTradeId
      fieldValue = strcasestr(tempBuffer, "VenueTradeId=");
      if (fieldValue != NULL)
      {
        execId = atol(&fieldValue[13]);
      }
      else
      {
        TraceLog(WARN_LEVEL, "Can't find VenueTradeId\n");
        break;
      }

      TraceLog(DEBUG_LEVEL, "OJ Break message: %d, %ld\n", orderId, execId);

      // Add break message in collection
      OJBrokenMsgList.collection[OJBrokenMsgList.countBrokenMsg].clOrdId = orderId;
      OJBrokenMsgList.collection[OJBrokenMsgList.countBrokenMsg].execId = execId;
      OJBrokenMsgList.countBrokenMsg++;
    }
    
    memset(buffer, 0, MAX_LINE_LEN_OF_OJ);
  }
  
  if (fpOJ != NULL)
  {
    fclose(fpOJ);
    fpOJ = NULL;
  }
  
  fpOJ = fopen(OJFilePath, "a+");
  if (fpOJ == NULL)
  {
    TraceLog(ERROR_LEVEL, "Could not open file %s for writing\n", OJFilePath);
    return ERROR;
  }
  
  return SUCCESS;
}

int ReadAndInsertOJRecordHungOrderFile()
{
  // Load hung order from file
  LoadHungOrderListFromFile();
  
  int i, lastMarket;
  t_HungOrderDetails *hungOrder;
  
  char timestamp[32], execIdField[20];
  char accountStr[32];
  
  for (i = 0; i < HungOrderList.count; i++)
  {
    hungOrder = &HungOrderList.buffer[i];
    
    GetEpochTimeFromDateString(hungOrder->timestamp, timestamp);
    GetAccountName(hungOrder->venueType, hungOrder->account, accountStr);
    
    if (hungOrder->orderType == ORDER_EDITOR_CANCELLED) // Order cancelled
    {
      AddOJOrderCancelResponseRecord(timestamp, hungOrder->symbol, hungOrder->clOrdId, "''", "1", hungOrder->venueType, hungOrder->shares, 1, accountStr);
    }
    else if (hungOrder->orderType == ORDER_EDITOR_FILLED) // Order filled
    {
      lastMarket = (hungOrder->venueType == TYPE_NYSE_CCG) ? hungOrder->msgContent[31] : 0;
      
      sprintf(execIdField, "%lu", (unsigned long)atol(hungOrder->execId));
      char *exOrderId = "0";
      
      AddOJOrderFillRecord(timestamp, hungOrder->symbol, exOrderId, hungOrder->venueType, hungOrder->side, hungOrder->price, hungOrder->shares,
                hungOrder->clOrdId, hungOrder->liquidity, "''", execIdField, "1", 1, accountStr, lastMarket);
    }
    else
    {
      TraceLog(ERROR_LEVEL, "Unknown order type in Hung Order (%d)\n", hungOrder->orderType);
    }
  }
  
  return SUCCESS;
}

void _________OJ_File_Creation_________(void){};
/****************************************************************************
- Function name:  
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int CreateOJRecordForOpenPosition(void)
{
  struct stat buf;
  errno = 0;
  if (stat(OJFilePath, &buf) == 0)  //success
  {
    int i;
    
    //Query for open positions
    t_OpenPositionList list;
    GetOpenPositionsFromDatabaseWithTime(&list);
      
    if (buf.st_size == 0) //File is empty
    {
      //Insert to file
      for (i = 0; i < list.count; i++)
      {
        if (list.positions[i].shares > 0)
        {
          AddOJOpenPositionRecord(&list.positions[i]);
        }
      }
      
      return SUCCESS;
    }
    else
    {
      //Insert to file
      for (i = 0; i < list.count; i++)
      {
        if (list.positions[i].isManual == 1 && list.positions[i].changedShares != 0)
        {
          if (list.positions[i].changedShares > 0)
          {
            list.positions[i].side = 'B';
          }
          else
          {
            list.positions[i].side = 'S';
          }
          
          list.positions[i].shares = abs(list.positions[i].changedShares);
    
          AddOJOpenPositionRecord(&list.positions[i]);
        }
      }
      
      return SUCCESS;
    }
  }
  else
  {
    TraceLog(ERROR_LEVEL, "Error in checking OJ file\n");
    PrintErrStr(ERROR_LEVEL, "Calling stat(): ", errno);
    return ERROR;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  AddOJOpenPositionRecord
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int AddOJOpenPositionRecord(t_OpenPosition *position)
{
  //Sample: //[type=busclient::RGMFillMsg tm=1338296358.420293 source=sod symbol=WTM venue='' buysell=B price=522.05 size=1. rgmoid=WTM:1338296358.506825 liquidity=0 venue_oid=sod refnum=0 reason_code=5 order_complete=1 publisher=sod_esp templ_id=0 templ_ver=0 properties=[version=3 strings=[account=9816 clearing_firm=Fortis esp_metatag=WTM strategy=tl_WTM] numbers=[BusWrapperTime=1338296358.700425] codables=[BasisCurrency=@[[type=market_interface::MiBasisCurrency currency=USD multiplier=1. usd_mult=1.]]]]]
  char record[2048];
  char tmp[32];
  char timeStamp[32];
  char account[12];
  int offset = 0;
  char symbol[SYMBOL_LEN];
  char source[32];
  
  if (position->isManual == 1)
  {
    strcpy(source, "doneaway");
  }
  else
  {
    strcpy(source, "sod");
  }
  
  //Type
  offset += sprintf(&record[offset], "[type=busclient::RGMFillMsg ");
  
  //Time stamp
  sprintf(tmp, "%s-%s", position->date, position->timeStamp);
  GetEpochTimeFromDateString(tmp, timeStamp);
  offset += sprintf(&record[offset], "tm=%s ", timeStamp);
  
  //source
  offset += sprintf(&record[offset], "source=%s ", source);
  
  //symbol
  int len = strnlen(position->symbol, SYMBOL_LEN);
  memset(symbol, 0, SYMBOL_LEN);
  memcpy(symbol, position->symbol, len);
  
  offset += sprintf(&record[offset], "symbol=%.8s ", symbol);
  
  //venue
  offset += sprintf(&record[offset], "venue='' ");
  
  //buysell
  offset += sprintf(&record[offset], "buysell=%c ", position->side);
  
  //price
  offset += sprintf(&record[offset], "price=%.4lf ", position->price);
  
  //size
  offset += sprintf(&record[offset], "size=%d. ", position->shares);
  
  GetAccountName(TYPE_ARCA_DIRECT, position->tradingAccount, account);

  //rgmoid
  offset += sprintf(&record[offset], "rgmoid=EAA:%.8s:%s%s ", symbol, timeStamp, account);
  
  //liquidity (0)
  offset += sprintf(&record[offset], "liquidity=0 ");
  
  //venue_oid
  offset += sprintf(&record[offset], "venue_oid=%s ", source);
  
  //refnum
  char refNum[] = "0";
  offset += sprintf(&record[offset], "refnum=%s ", refNum);
  
  //reason_code (always 5)
  offset += sprintf(&record[offset], "reason_code=5 ");
  
  //order_complete (always 1)
  offset += sprintf(&record[offset], "order_complete=1 ");
  
  //publisher
  if (position->isManual == 1)
  {
    offset += sprintf(&record[offset], "publisher=rgm_operations ");
  } 
  else
  {
    offset += sprintf(&record[offset], "publisher=sod_bb ");
  }
  
  //templ_id
  //templ_ver
  
  //properties=[version=3 strings=[account=9816 clearing_firm=Fortis esp_metatag=WTM strategy=tl_WTM] numbers=[BusWrapperTime=1338296358.700425] codables=[BasisCurrency=@[[type=market_interface::MiBasisCurrency currency=USD multiplier=1. usd_mult=1.]]]]]
  //properties
  
  offset += sprintf(&record[offset], "properties=[version=3 strings=[account=%s clearing_firm=Fortis creator=blinkbox execution_firm=RGMS rgm_mpid=HBIF] numbers=[] codables=[BasisCurrency=@[[type=market_interface::MiBasisCurrency currency=USD multiplier=1. usd_mult=1.]]]]", account);
  
  //End of record
  record[offset++] = ']';
  record[offset++] = '\n';
  record[offset] = 0;

  fprintf(fpOJ, "%s", record);
  fflush(fpOJ);
  return SUCCESS;
}

/****************************************************************************
- Function name:  
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int AddOJNewOrderRecord(char *time, char *symbol, int venueID, char buysell, double price, int size, int orderID, char *tif, char visibility, int isISO, char *account, char *routingInfo, char capacity, int tsCrossId, unsigned char bbReason)
{
  char record[2048];
  char seqID[12];
  int offset = 0;
  int isManualOrder = 0;
  
  sprintf(seqID, "%d", orderID);
        
  if (orderID < 500000) //as
  {
    isManualOrder = 1;
  }
  
  //Type
  offset += sprintf(&record[offset], "[type=busclient::RGMOrderMsg ");
  
  //Time stamp
  offset += sprintf(&record[offset], "tm=%s ", time);
  
  //source
  if (isManualOrder == 0)
  {
    offset += sprintf(&record[offset], "source=%s ", OJ_SOURCE);
  }
  else
  {
    offset += sprintf(&record[offset], "source=rgm_operations ");
  }
  
  //symbol
  offset += sprintf(&record[offset], "symbol=%.8s ", symbol);
  
  //venue
  offset += sprintf(&record[offset], "venue=%s ", OE_Mappings[venueID]);
  
  //buysell
  offset += sprintf(&record[offset], "buysell=%c ", buysell);
  
  //price
  offset += sprintf(&record[offset], "price=%.4lf ", price);
  
  //size
  offset += sprintf(&record[offset], "size=%d. ", size);
  
  //rgmoid
  offset += sprintf(&record[offset], "rgmoid=EAA%s ", seqID);
  
  //expiration
  offset += sprintf(&record[offset], "expiration=%s. ", tif);
  
  //visibility
  offset += sprintf(&record[offset], "visibility=%c ", visibility);
  
  //state
  offset += sprintf(&record[offset], "state=1 ");
  
  //oecinstance
  offset += GetOECInstance(&record[offset], orderID, venueID);
  
  //templ_id
  //templ_ver
  
  /*  properties  */
  offset += sprintf(&record[offset], "properties=[version=3 strings=[");
  
  if (isISO == 1)
  {
    offset += sprintf(&record[offset], "ExecInst=f ");
  }
  
  if (routingInfo != NULL)
  {
    offset += sprintf(&record[offset], "Routing=%s ", routingInfo);
  }
  
  if (isManualOrder == 1)
  {
    offset += sprintf(&record[offset], "OperatorUserName=%s ", manualOrderInfo[orderID % MAX_MANUAL_ORDER].username);
  }
  
  char serverName[16];
  memset(serverName, 0, 16);
  strcpy(serverName, GetServerName(orderID));
  
  char tsCrossIdStr[12] = "\0";
  if (orderID < 1000000)
  {
    strcpy(tsCrossIdStr, "''");
  }
  else
  {
    sprintf(tsCrossIdStr, "%d", tsCrossId);
  }

  offset += sprintf(&record[offset], "account=%s capacity=%c clearing_firm=Fortis creator=blinkbox execution_firm=RGMS rgm_mpid=HBIF BBHost=%s BBReason=%d TSCrossID=%s] numbers=[] codables=[BasisCurrency=@[[type=market_interface::MiBasisCurrency currency=USD multiplier=1. usd_mult=1.]]]]", account, capacity, serverName, bbReason, tsCrossIdStr);
  
  //End of record
  record[offset++] = ']';
  record[offset++] = '\n';
  record[offset] = 0;

  fprintf(fpOJ, "%s", record);
  fflush(fpOJ);
  return SUCCESS;
}

/****************************************************************************
- Function name:  
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int AddOJOrderAcceptedRecord(char *time, char *symbol, char *exOrderId, int orderID, char *ecnOrderID, int venueID, char *account, char capacity)
{
  char record[2048];
  int offset = 0;
  int isManualOrder = 0;
  
  if (orderID < 500000) //as
  {
    isManualOrder = 1;
  }
  
  //Type
  offset += sprintf(&record[offset], "[type=busclient::RGMOrderAcceptMsg ");
  
  //Time stamp
  offset += sprintf(&record[offset], "tm=%s ", time);
  
  //symbol
  offset += sprintf(&record[offset], "symbol=%.8s ", symbol);
  
  //source
  if (isManualOrder == 0)
  {
    offset += sprintf(&record[offset], "source=%s ", OJ_SOURCE);
  }
  else
  {
    offset += sprintf(&record[offset], "source=rgm_operations ");
  }
  
  //rgmoid
  offset += sprintf(&record[offset], "rgmoid=EAA%d ", orderID);
  
  //venue_oid
  offset += sprintf(&record[offset], "venue_oid=%s ", ecnOrderID);
  
  //venue
  offset += sprintf(&record[offset], "venue=%s ", OE_Mappings[venueID]);
  
  //publisher ???
  if (orderID < 500000) //as
  {
    offset += sprintf(&record[offset], "publisher=eaa-as ");
  }
  else if (orderID < 1000000) //pm
  {
    offset += sprintf(&record[offset], "publisher=eaa-pm ");
  }
  else if (orderID < 2000000) //TS1
  {
    offset += sprintf(&record[offset], "publisher=eaa-ts1 ");
  }
  else if (orderID < 3000000) //TS2
  {
    offset += sprintf(&record[offset], "publisher=eaa-ts2 ");
  }
  else if (orderID < 4000000) //TS3
  {
    offset += sprintf(&record[offset], "publisher=eaa-ts3 ");
  }
  else if (orderID < 5000000) //TS4
  {
    offset += sprintf(&record[offset], "publisher=eaa-ts4 ");
  }
  else if (orderID < 6000000) //TS5
  {
    offset += sprintf(&record[offset], "publisher=eaa-ts5 ");
  }
  else if (orderID < 7000000) //TS6
  {
    offset += sprintf(&record[offset], "publisher=eaa-ts6 ");
  }
  else if (orderID < 8000000) //TS7
  {
    offset += sprintf(&record[offset], "publisher=eaa-ts7 ");
  }
  else if (orderID < 9000000) //TS8
  {
    offset += sprintf(&record[offset], "publisher=eaa-ts8 ");
  }
  else if (orderID < 10000000) //TS9
  {
    offset += sprintf(&record[offset], "publisher=eaa-ts9 ");
  }
  else
  {
    //????????????
  }
  
  //templ_id
  //templ_ver

  //properties
  if (capacity == 0)  //No capacity, we dont add
  {
    offset += sprintf(&record[offset], "properties=[version=3 strings=[ExternalOID=%s account=%s clearing_firm=Fortis creator=blinkbox execution_firm=RGMS rgm_mpid=HBIF] numbers=[] codables=[BasisCurrency=@[[type=market_interface::MiBasisCurrency currency=USD multiplier=1. usd_mult=1.]]]]", exOrderId, account);
  }
  else
  {
    offset += sprintf(&record[offset], "properties=[version=3 strings=[ExternalOID=%s account=%s capacity=%c clearing_firm=Fortis creator=blinkbox execution_firm=RGMS rgm_mpid=HBIF] numbers=[] codables=[BasisCurrency=@[[type=market_interface::MiBasisCurrency currency=USD multiplier=1. usd_mult=1.]]]]", exOrderId, account, capacity);
  }
    
  //End of record
  record[offset++] = ']';
  record[offset++] = '\n';
  record[offset] = 0;
  
  fprintf(fpOJ, "%s", record);
  fflush(fpOJ);
  return SUCCESS;
}

/****************************************************************************
- Function name:  AddOJOrderTradeBreakRecord
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int AddOJOrderTradeBreakRecord(char *time, char *symbol, int venueID, char buysell, double price, int size, int orderID, char *liquidityField, char *ecnOrderID, char *execID, char *reasonCode, int orderComplete, char *account)
{
  char record[2048];
  int offset = 0;
  int liquidity = liquidityField[0];
  
  //Type
  offset += sprintf(&record[offset], "[type=busclient::RGMBrokenTradeMsg ");
  
  //Time stamp
  offset += sprintf(&record[offset], "tm=%s ", time);
  
  //source
  offset += sprintf(&record[offset], "source=%s ", OJ_SOURCE);
  
  //symbol
  offset += sprintf(&record[offset], "symbol=%.8s ", symbol);
  
  //venue
  offset += sprintf(&record[offset], "venue=%s ", OE_Mappings[venueID]);
  
  //buysell
  offset += sprintf(&record[offset], "buysell=%c ", buysell);
  
  //price
  offset += sprintf(&record[offset], "price=%.4lf ", price);
  
  //size
  offset += sprintf(&record[offset], "size=%d. ", abs(size));
  
  //rgmoid
  offset += sprintf(&record[offset], "rgmoid=EAA%d ", orderID);
  
  //liquidity (ASCII value of liquidity character)
  offset += sprintf(&record[offset], "liquidity=%d ", liquidity);
  
  //venue_oid
  offset += sprintf(&record[offset], "venue_oid=%s ", ecnOrderID);
  
  //refnum
  char refNum[] = "0";
  offset += sprintf(&record[offset], "refnum=%s ", refNum);
  
  //reason_code
  offset += sprintf(&record[offset], "reason_code=%s ", reasonCode);
  
  //order_complete
  offset += sprintf(&record[offset], "order_complete=%d ", orderComplete);
  
  //publisher ???
  if (orderID < 500000) //as
  {
    offset += sprintf(&record[offset], "publisher=eaa-as ");
  }
  else if (orderID < 1000000) //pm
  {
    offset += sprintf(&record[offset], "publisher=eaa-pm ");
  }
  else if (orderID < 2000000) //TS1
  {
    offset += sprintf(&record[offset], "publisher=eaa-ts1 ");
  }
  else if (orderID < 3000000) //TS2
  {
    offset += sprintf(&record[offset], "publisher=eaa-ts2 ");
  }
  else if (orderID < 4000000) //TS3
  {
    offset += sprintf(&record[offset], "publisher=eaa-ts3 ");
  }
  else if (orderID < 5000000) //TS4
  {
    offset += sprintf(&record[offset], "publisher=eaa-ts4 ");
  }
  else if (orderID < 6000000) //TS5
  {
    offset += sprintf(&record[offset], "publisher=eaa-ts5 ");
  }
  else if (orderID < 7000000) //TS6
  {
    offset += sprintf(&record[offset], "publisher=eaa-ts6 ");
  }
  else if (orderID < 8000000) //TS7
  {
    offset += sprintf(&record[offset], "publisher=eaa-ts7 ");
  }
  else if (orderID < 9000000) //TS8
  {
    offset += sprintf(&record[offset], "publisher=eaa-ts8 ");
  }
  else if (orderID < 10000000) //TS9
  {
    offset += sprintf(&record[offset], "publisher=eaa-ts9 ");
  }
  else
  {
    //????????????
  }
  
  //templ_id
  //templ_ver
  
  //properties
  if (liquidity == 32)    // liquidity = ' '
  {
    offset += sprintf(&record[offset], "properties=[version=3 strings=[Liquidity=%s VenueTradeId=%s account=%s capacity=P clearing_firm=Fortis creator=blinkbox execution_firm=RGMS rgm_mpid=HBIF] numbers=[] codables=[BasisCurrency=@[[type=market_interface::MiBasisCurrency currency=USD multiplier=1. usd_mult=1.]]]]", "%20", execID, account);
  }
  else
  {
    offset += sprintf(&record[offset], "properties=[version=3 strings=[Liquidity=%s VenueTradeId=%s account=%s capacity=P clearing_firm=Fortis creator=blinkbox execution_firm=RGMS rgm_mpid=HBIF] numbers=[] codables=[BasisCurrency=@[[type=market_interface::MiBasisCurrency currency=USD multiplier=1. usd_mult=1.]]]]", liquidityField, execID, account);
  }

  
  //End of record
  record[offset++] = ']';
  record[offset++] = '\n';
  record[offset] = 0;

  fprintf(fpOJ, "%s", record);
  fflush(fpOJ);
  
  // Add break message in collection
  OJBrokenMsgList.collection[OJBrokenMsgList.countBrokenMsg].clOrdId = orderID;
  OJBrokenMsgList.collection[OJBrokenMsgList.countBrokenMsg].execId = atol(execID);
  OJBrokenMsgList.countBrokenMsg++;
  
  return SUCCESS;
}
/****************************************************************************
- Function name:  
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int AddOJOrderFillRecord(char *time, char *symbol, char *exOrderId, int venueID, char buysell, double price, int size, int orderID, char *liquidityField, char *ecnOrderID, char *execID, char *reasonCode, int orderComplete, char *account, char execVenue)
{
  char record[2048];
  int offset = 0;
  int isManualOrder = 0;
  int liquidity = liquidityField[0];
  
  if (orderID < 500000) //as
  {
    isManualOrder = 1;
  }
  
  //Type
  offset += sprintf(&record[offset], "[type=busclient::RGMFillMsg ");
  
  //Time stamp
  offset += sprintf(&record[offset], "tm=%s ", time);
  
  //source
  if (isManualOrder == 0)
  {
    offset += sprintf(&record[offset], "source=%s ", OJ_SOURCE);
  }
  else
  {
    offset += sprintf(&record[offset], "source=rgm_operations ");
  }
  
  //symbol
  offset += sprintf(&record[offset], "symbol=%.8s ", symbol);
  
  //venue
  offset += sprintf(&record[offset], "venue=%s ", OE_Mappings[venueID]);
  
  //buysell
  offset += sprintf(&record[offset], "buysell=%c ", buysell);
  
  //price
  offset += sprintf(&record[offset], "price=%.4lf ", price);
  
  //size
  offset += sprintf(&record[offset], "size=%d. ", size);

  //rgmoid
  offset += sprintf(&record[offset], "rgmoid=EAA%d ", orderID);
  
  //liquidity (ASCII value of liquidity character)
  offset += sprintf(&record[offset], "liquidity=%d ", liquidity);
  
  //venue_oid
  offset += sprintf(&record[offset], "venue_oid=%s ", ecnOrderID);
  
  //refnum
  char refNum[] = "0";
  offset += sprintf(&record[offset], "refnum=%s ", refNum);
  
  //reason_code
  offset += sprintf(&record[offset], "reason_code=%s ", reasonCode);
  
  //order_complete
  offset += sprintf(&record[offset], "order_complete=%d ", orderComplete);
  
  //publisher ???
  if (orderID < 500000) //as
  {
    offset += sprintf(&record[offset], "publisher=eaa-as ");
  }
  else if (orderID < 1000000) //pm
  {
    offset += sprintf(&record[offset], "publisher=eaa-pm ");
  }
  else if (orderID < 2000000) //TS1
  {
    offset += sprintf(&record[offset], "publisher=eaa-ts1 ");
  }
  else if (orderID < 3000000) //TS2
  {
    offset += sprintf(&record[offset], "publisher=eaa-ts2 ");
  }
  else if (orderID < 4000000) //TS3
  {
    offset += sprintf(&record[offset], "publisher=eaa-ts3 ");
  }
  else if (orderID < 5000000) //TS4
  {
    offset += sprintf(&record[offset], "publisher=eaa-ts4 ");
  }
  else if (orderID < 6000000) //TS5
  {
    offset += sprintf(&record[offset], "publisher=eaa-ts5 ");
  }
  else if (orderID < 7000000) //TS6
  {
    offset += sprintf(&record[offset], "publisher=eaa-ts6 ");
  }
  else if (orderID < 8000000) //TS7
  {
    offset += sprintf(&record[offset], "publisher=eaa-ts7 ");
  }
  else if (orderID < 9000000) //TS8
  {
    offset += sprintf(&record[offset], "publisher=eaa-ts8 ");
  }
  else if (orderID < 10000000) //TS9
  {
    offset += sprintf(&record[offset], "publisher=eaa-ts9 ");
  }
  else
  {
    //????????????
  }
  
  /* 
    ... strings=[Currency=USD ExecVenue=A ExternalOID=...
  */
  if (execVenue != 0 && execVenue != ' ')
  {
    offset += sprintf(&record[offset], "properties=[version=3 strings=[ExternalOID=%s ExecVenue=%c ", exOrderId, execVenue);
  }
  else
  {
    offset += sprintf(&record[offset], "properties=[version=3 strings=[ExternalOID=%s ", exOrderId);
  }
  
  //templ_id
  //templ_ver
  
  //properties
  if (liquidity == 32)    // liquidity = ' '
  {
    offset += sprintf(&record[offset], "Liquidity=%s VenueTradeId=%s account=%s capacity=P clearing_firm=Fortis creator=blinkbox execution_firm=RGMS rgm_mpid=HBIF] numbers=[] codables=[BasisCurrency=@[[type=market_interface::MiBasisCurrency currency=USD multiplier=1. usd_mult=1.]]]]", "%20", execID, account);
  }
  else
  {
    offset += sprintf(&record[offset], "Liquidity=%s VenueTradeId=%s account=%s capacity=P clearing_firm=Fortis creator=blinkbox execution_firm=RGMS rgm_mpid=HBIF] numbers=[] codables=[BasisCurrency=@[[type=market_interface::MiBasisCurrency currency=USD multiplier=1. usd_mult=1.]]]]", liquidityField, execID, account);
  }
  
  //End of record
  record[offset++] = ']';
  record[offset++] = '\n';
  record[offset] = 0;

  fprintf(fpOJ, "%s", record);
  fflush(fpOJ);
  return SUCCESS;
}

/****************************************************************************
- Function name:  
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int AddOJOrderCancelRecord(char *time, int orderID, char *symbol, int venueID, int size)
{
  char record[2048];
  int offset = 0;
  char seqID[12];
  int isManualOrder = 0;
  
  sprintf(seqID, "%d", orderID);
  
  if (orderID < 500000) //as
  {
    isManualOrder = 1;
  }
  
  //Type
  offset += sprintf(&record[offset], "[type=busclient::RGMCancelMsg ");
  
  //Time stamp
  offset += sprintf(&record[offset], "tm=%s ", time);
  
  //source
  if (isManualOrder == 0)
  {
    offset += sprintf(&record[offset], "source=%s ", OJ_SOURCE);
  }
  else
  {
    offset += sprintf(&record[offset], "source=rgm_operations ");
  }
  
  //rgmoid
  offset += sprintf(&record[offset], "rgmoid=EAA%s ", seqID);
  
  //symbol
  offset += sprintf(&record[offset], "symbol=%.8s ", symbol);
  
  //venue
  offset += sprintf(&record[offset], "venue=%s ", OE_Mappings[venueID]);
  
  //size
  offset += sprintf(&record[offset], "size=%d. ", size);
  
  //oecinstance
  offset += GetOECInstance(&record[offset], orderID, venueID);
  
  //templ_id
  //templ_ver
  
  //properties
  offset += sprintf(&record[offset], "properties=[version=3 strings=[] numbers=[] codables=[]]");
  
  //End of record
  record[offset++] = ']';
  record[offset++] = '\n';
  record[offset] = 0;
  
  fprintf(fpOJ, "%s", record);
  fflush(fpOJ);
  return SUCCESS;
}

/****************************************************************************
- Function name:  
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int AddOJOrderCancelResponseRecord(char *time, char *symbol, int orderID, char *ecnOrderID, char *reason, int venueID, int sizeCanceled, int orderComplete, char *account)
{
  char record[2048];
  int offset = 0;
  char seqID[12];
  int isManualOrder = 0;
  
  sprintf(seqID, "%d", orderID);
  
  if (orderID < 500000) //as
  {
    isManualOrder = 1;
  }
  
  //Type
  offset += sprintf(&record[offset], "[type=busclient::RGMCancelResponseMsg ");
  
  //Time stamp
  offset += sprintf(&record[offset], "tm=%s ", time);
  
  //source
  if (isManualOrder == 0)
  {
    offset += sprintf(&record[offset], "source=%s ", OJ_SOURCE);
  }
  else
  {
    offset += sprintf(&record[offset], "source=rgm_operations ");
  }
  
  //symbol
  offset += sprintf(&record[offset], "symbol=%.8s ", symbol);
  
  //rgmoid
  offset += sprintf(&record[offset], "rgmoid=EAA%s ", seqID);
  
  //venue_oid
  offset += sprintf(&record[offset], "venue_oid=%s ", ecnOrderID);
  
  //reason ?? url encoded style ???
  offset += sprintf(&record[offset], "reason=%s ", reason);
  
  //venue
  offset += sprintf(&record[offset], "venue=%s ", OE_Mappings[venueID]);
  
  //size_canceled
  offset += sprintf(&record[offset], "size_canceled=%d. ", sizeCanceled);
  
  //order_complete
  offset += sprintf(&record[offset], "order_complete=%d ", orderComplete);
  
  //publisher ???
  if (orderID < 500000) //as
  {
    offset += sprintf(&record[offset], "publisher=eaa-as ");
  }
  else if (orderID < 1000000) //pm
  {
    offset += sprintf(&record[offset], "publisher=eaa-pm ");
  }
  else if (orderID < 2000000) //TS1
  {
    offset += sprintf(&record[offset], "publisher=eaa-ts1 ");
  }
  else if (orderID < 3000000) //TS2
  {
    offset += sprintf(&record[offset], "publisher=eaa-ts2 ");
  }
  else if (orderID < 4000000) //TS3
  {
    offset += sprintf(&record[offset], "publisher=eaa-ts3 ");
  }
  else if (orderID < 5000000) //TS4
  {
    offset += sprintf(&record[offset], "publisher=eaa-ts4 ");
  }
  else if (orderID < 6000000) //TS5
  {
    offset += sprintf(&record[offset], "publisher=eaa-ts5 ");
  }
  else if (orderID < 7000000) //TS6
  {
    offset += sprintf(&record[offset], "publisher=eaa-ts6 ");
  }
  else if (orderID < 8000000) //TS7
  {
    offset += sprintf(&record[offset], "publisher=eaa-ts7 ");
  }
  else if (orderID < 9000000) //TS8
  {
    offset += sprintf(&record[offset], "publisher=eaa-ts8 ");
  }
  else if (orderID < 10000000) //TS9
  {
    offset += sprintf(&record[offset], "publisher=eaa-ts9 ");
  }
  else
  {
    //????????????
  }
  
  //templ_id
  //templ_ver
  
  //properties
  offset += sprintf(&record[offset], "properties=[version=3 strings=[account=%s capacity=P clearing_firm=Fortis creator=blinkbox execution_firm=RGMS rgm_mpid=HBIF] numbers=[] codables=[BasisCurrency=@[[type=market_interface::MiBasisCurrency currency=USD multiplier=1. usd_mult=1.]]]]", account);
  
  //End of record
  record[offset++] = ']';
  record[offset++] = '\n';
  record[offset] = 0;
  
  fprintf(fpOJ, "%s", record);
  fflush(fpOJ);
  return SUCCESS;
}

/****************************************************************************
- Function name:  
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int AddOJOrderRejectRecord(char *time, int orderID, char *ecnOrderID, char *reason, char *symbol, int venueID, char *reasonCode, char *account)
{
  char record[2048];
  int offset = 0;
  char seqID[12];
  int isManualOrder = 0;
  
  sprintf(seqID, "%d", orderID);
  
  if (orderID < 500000) //as
  {
    isManualOrder = 1;
  }
  
  //Type
  offset += sprintf(&record[offset], "[type=busclient::RGMOrderRejectMsg ");
  
  //Time stamp
  offset += sprintf(&record[offset], "tm=%s ", time);
  
  //source
  if (isManualOrder == 0)
  {
    offset += sprintf(&record[offset], "source=%s ", OJ_SOURCE);
  }
  else
  {
    offset += sprintf(&record[offset], "source=rgm_operations ");
  }
  
  //rgmoid
  offset += sprintf(&record[offset], "rgmoid=EAA%s ", seqID);
  
  //venue_oid
  offset += sprintf(&record[offset], "venue_oid=%s ", ecnOrderID);
  
  //reason ?? url encoded style ???
  offset += sprintf(&record[offset], "reason=%s ", reason);
  
  //symbol
  offset += sprintf(&record[offset], "symbol=%.8s ", symbol);
  
  //venue
  offset += sprintf(&record[offset], "venue=%s ", OE_Mappings[venueID]);
  
  //publisher ???
  if (orderID < 500000) //as
  {
    offset += sprintf(&record[offset], "publisher=eaa-as ");
  }
  else if (orderID < 1000000) //pm
  {
    offset += sprintf(&record[offset], "publisher=eaa-pm ");
  }
  else if (orderID < 2000000) //TS1
  {
    offset += sprintf(&record[offset], "publisher=eaa-ts1 ");
  }
  else if (orderID < 3000000) //TS2
  {
    offset += sprintf(&record[offset], "publisher=eaa-ts2 ");
  }
  else if (orderID < 4000000) //TS3
  {
    offset += sprintf(&record[offset], "publisher=eaa-ts3 ");
  }
  else if (orderID < 5000000) //TS4
  {
    offset += sprintf(&record[offset], "publisher=eaa-ts4 ");
  }
  else if (orderID < 6000000) //TS5
  {
    offset += sprintf(&record[offset], "publisher=eaa-ts5 ");
  }
  else if (orderID < 7000000) //TS6
  {
    offset += sprintf(&record[offset], "publisher=eaa-ts6 ");
  }
  else if (orderID < 8000000) //TS7
  {
    offset += sprintf(&record[offset], "publisher=eaa-ts7 ");
  }
  else if (orderID < 9000000) //TS8
  {
    offset += sprintf(&record[offset], "publisher=eaa-ts8 ");
  }
  else if (orderID < 10000000) //TS9
  {
    offset += sprintf(&record[offset], "publisher=eaa-ts9 ");
  }
  else
  {
    //????????????
  }
  
  //reason_code
  offset += sprintf(&record[offset], "reason_code=%s ", reasonCode);
  
  //templ_id
  //templ_ver
  
  //properties
  offset += sprintf(&record[offset], "properties=[version=3 strings=[account=%s capacity=P clearing_firm=Fortis creator=blinkbox execution_firm=RGMS rgm_mpid=HBIF] numbers=[] codables=[BasisCurrency=@[[type=market_interface::MiBasisCurrency currency=USD multiplier=1. usd_mult=1.]]]]", account);
    
  //End of record
  record[offset++] = ']';
  record[offset++] = '\n';
  record[offset] = 0;
  
  fprintf(fpOJ, "%s", record);
  fflush(fpOJ);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int CreateOJFilesWithDateRange(int beginDateInSecs, int endDateInSecs)
{
  struct tm l_time;
  time_t numberOfSecondsTmp = beginDateInSecs;
  
  int i, j;
  char currentRawdataFile[512];
  
  while (numberOfSecondsTmp <= endDateInSecs)
  {
    TraceLog(DEBUG_LEVEL, "***************************************************\n");

    // Convert time_t to tm
    localtime_r(&numberOfSecondsTmp, &l_time);

    //  We only trade from Monday to Friday
    if ((l_time.tm_wday > 0) && (l_time.tm_wday < 6))
    {
      //-------------------------------------
      //  Initialize for current date
      //  This creates data path and OJ file for appending
      //-------------------------------------
      CreateDataPathAtDate(numberOfSecondsTmp);
      TraceLog(DEBUG_LEVEL, "globalDateStringOfDatabase: %s\n", globalDateStringOfDatabase);
      //Get manual orderID and username who placed manual order, for OJ record creation
      GetManualOrderInfoFromDatabase(globalDateStringOfDatabase);
      
      // Delete current order journal file if exist
      if (GetFullRawDataPath(OJ_FILENAME, OJFilePath) == NULL_POINTER)
      {
        TraceLog(ERROR_LEVEL, "Have some problem in get full raw data path operation (%s)\n", OJ_FILENAME);

        return ERROR;
      }
      
      //Check if file already existed
      if (access(OJFilePath, F_OK) == 0) //File exists, remove it
      {
        char sysCmd[512];
        sprintf(sysCmd, "rm %s", OJFilePath);
        system(sysCmd);
      }
      
      if (InitOrderFilesCreation() == ERROR)
      {
        TraceLog(ERROR_LEVEL, "Could not generate OJ file for this date");

        return ERROR;
      }
      
      //-------------------------------------------------------
      //  Generate files (file already opened for writing)
      //-------------------------------------------------------
      
      int isError = 0;

      //Add records from TSs rawdata files
      for (i = 0; i < MAX_TRADE_SERVER_CONNECTIONS; i++)
      {
        isError = 0;
        for (j = 0; j < TS_MAX_ORDER_CONNECTIONS; j++)
        {
          switch (j) // order index
          {
            case TS_ARCA_DIRECT_INDEX:
              sprintf(currentRawdataFile, "%s_%s_ts%d.%s", RAW_DATA_FILE_PREFIX_DIRECT, TradingAccount.DIRECTLoginTS[i], i + 1, RAW_DATA_FILE_EXTENSION_DIRECT);
              break;

            case TS_NASDAQ_OUCH_INDEX:
              sprintf(currentRawdataFile, "%s_%s_ts%d.%s", RAW_DATA_FILE_PREFIX_OUCH, TradingAccount.OUCHLoginTS[i], i + 1, RAW_DATA_FILE_EXTENSION_OUCH);
              break;

            case TS_NYSE_CCG_INDEX:
              sprintf(currentRawdataFile, "%s_%s_ts%d.%s", RAW_DATA_FILE_PREFIX_NYSE, TradingAccount.CCGLoginTS[i], i + 1, RAW_DATA_FILE_EXTENSION_NYSE);
              break;

            case TS_NASDAQ_RASH_INDEX:
              sprintf(currentRawdataFile, "%s_%s_ts%d.%s", RAW_DATA_FILE_PREFIX_RASH, TradingAccount.RASHLoginTS[i], i + 1, RAW_DATA_FILE_EXTENSION_RASH);
              break;

            case TS_BATSZ_BOE_INDEX:
              sprintf(currentRawdataFile, "%s_%s_ts%d.%s", RAW_DATA_FILE_PREFIX_BATSZ, TradingAccount.BATSZBOELoginTS[i], i + 1, RAW_DATA_FILE_EXTENSION_BATSZ);
              break;
              
            case TS_EDGX_DIRECT_INDEX:
              sprintf(currentRawdataFile, "%s_%s_ts%d.%s", RAW_DATA_FILE_PREFIX_EDGX, TradingAccount.EDGXLoginTS[i], i + 1, RAW_DATA_FILE_EXTENSION_EDGX);
              break;
              
            case TS_EDGA_DIRECT_INDEX:
              sprintf(currentRawdataFile, "%s_%s_ts%d.%s", RAW_DATA_FILE_PREFIX_EDGA, TradingAccount.EDGALoginTS[i], i + 1, RAW_DATA_FILE_EXTENSION_EDGA);
              break;
              
            case TS_NDAQ_OUBX_INDEX:
              sprintf(currentRawdataFile, "%s_%s_ts%d.%s", RAW_DATA_FILE_PREFIX_OUBX, TradingAccount.OUBXLoginTS[i], i + 1, RAW_DATA_FILE_EXTENSION_OUBX);
              break;
              
            case TS_BYX_BOE_INDEX:
              sprintf(currentRawdataFile, "%s_%s_ts%d.%s", RAW_DATA_FILE_PREFIX_BYX, TradingAccount.BYXBOELoginTS[i], i + 1, RAW_DATA_FILE_EXTENSION_BYX);
              break;
              
            case TS_PSX_OUCH_INDEX:
              sprintf(currentRawdataFile, "%s_%s_ts%d.%s", RAW_DATA_FILE_PREFIX_PSX, TradingAccount.PSXLoginTS[i], i + 1, RAW_DATA_FILE_EXTENSION_PSX);
              break;
              
            default:
              isError = 1;
              TraceLog(ERROR_LEVEL, "(CreateOJFilesWithDateRange): Invalid order index (%d)\n", j);
              break;
          }
          
          if (isError == 0)
          {
            ReadAndInsertOJRecordFromRawdataFile(currentRawdataFile, j, __SERVER_TS);
          }
        }
      }
      
      //Add records from AS rawdata files      
      for (i = 0; i < AS_MAX_ORDER_CONNECTIONS; i++)
      {
        if (i == AS_ARCA_DIRECT_INDEX)
        {
          sprintf(currentRawdataFile, "%s_%s_as.%s", RAW_DATA_FILE_PREFIX_DIRECT, TradingAccount.ARCADIRECTLoginAS, RAW_DATA_FILE_EXTENSION_DIRECT);
        }
        else  // RASH rawdata
        {
          sprintf(currentRawdataFile, "%s_%s_as.%s", RAW_DATA_FILE_PREFIX_RASH, TradingAccount.RASHLoginAS, RAW_DATA_FILE_EXTENSION_RASH);
        }
        ReadAndInsertOJRecordFromRawdataFile(currentRawdataFile, i, __SERVER_AS);
      }
      
      //Add records from PM rawdata files
      for (i = 0; i < PM_MAX_ORDER_CONNECTIONS; i++)
      {
        if (i == PM_ARCA_DIRECT_INDEX)
        {
          sprintf(currentRawdataFile, "%s_%s_pm.%s", RAW_DATA_FILE_PREFIX_DIRECT, TradingAccount.ARCADIRECTLoginPM, RAW_DATA_FILE_EXTENSION_DIRECT);
        }
        else  // RASH rawdata
        {
          sprintf(currentRawdataFile, "%s_%s_pm.%s", RAW_DATA_FILE_PREFIX_RASH, TradingAccount.RASHLoginPM, RAW_DATA_FILE_EXTENSION_RASH);
        }
        ReadAndInsertOJRecordFromRawdataFile(currentRawdataFile, i, __SERVER_PM);
      }

      // Add records from history hung file
      ReadAndInsertOJRecordHungOrderFile();

      //-------------------------------------
      //  Close file pointer for this date
      //-------------------------------------
      if (fpOJ != NULL)
      {
        fclose(fpOJ);
        fpOJ = NULL;
      }

    }
    else
    {
      if (l_time.tm_wday == 6)
      {
        TraceLog(DEBUG_LEVEL, "** This is day off (Saturday).\n");
      }
      else
      {
        TraceLog(DEBUG_LEVEL, "** This is day off (Sunday).\n");
      }

      TraceLog(DEBUG_LEVEL, "***************************************************\n");
    }

    //  Increasing 1 day
    numberOfSecondsTmp += NUMBER_OF_SECONDS_PER_DAY;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ReadAndInsertOJRecordFromRawdataFile
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ReadAndInsertOJRecordFromRawdataFile(char *fName, int orderIndex, int serverType)
{
  char fullPath[512];
  if (GetFullRawDataPath(fName, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Have some problem in get full raw data path operation (%s)\n", fName);
    return ERROR;
  }
  
  // Check if file already existed
  if (access(fullPath, F_OK) != 0)
  {
    //File does not exists, ignore
    TraceLog(WARN_LEVEL, "File %s does not exist\n", fullPath);
    return SUCCESS;
  }
  
  if (IsFileTimestampToday(fullPath) == 0)
  {
    TraceLog(ERROR_LEVEL, "%s is old, please check the correctness of data files\n", fullPath);
    exit(0);
  }

  TraceLog(DEBUG_LEVEL, "-> Processing '%s' orderIndex: %d, server: %d\n", fName, orderIndex, serverType);

  // Open for reading
  errno = 0;
  FILE *fp = fopen(fullPath, "r");
  if (fp == NULL)
  {
    TraceLog(ERROR_LEVEL, "Can not open %s\n", fullPath);
    PrintErrStr(ERROR_LEVEL, "Calling fopen(): ", errno);
    return ERROR;
  }
  
  // Initialize OJOpenOrder
  memset (OJOpenOrder, 0, sizeof(OJOpenOrder));
  memset (&OJRawdataWaitList, 0, sizeof(OJRawdataWaitList));
  
  DestroyOrderLinkedlist(&__new);
  DestroyOrderLinkedlist(&__ack);
  DestroyOrderLinkedlist(&__cancel);
  DestroyOrderLinkedlist(&__reject);
  DestroyExecLinkedlist(&__exec);

  // Read each rawdata block and process it
  t_DataBlock dataBlock;
  int ret;

  while (!feof(fp))
  {
    ret = fread(&dataBlock, sizeof( t_DataBlock ), 1, fp);
    if( ret != 1 )
    {
      return ERROR;
    }
    
    //Notice: PM Rawdata also contains manual orders, which are duplicated in AS rawdata
    //So we must ignore them
    if (serverType == __SERVER_PM)
    {
      int isManualOrderMarking = 0;
      memcpy(&isManualOrderMarking, &dataBlock.addContent[DATABLOCK_OFFSET_CROSS_ID], 4);
      if (isManualOrderMarking == MANUAL_ORDER_MAKING)  //This is manual order
      {
        continue;
      }
    }
    
    if (serverType == __SERVER_TS)
    {
      switch (orderIndex)
      {
        case TS_NYSE_CCG_INDEX:
          ProcessOJRecordForNyseCCG(dataBlock, NO);
          break;
          
        case TS_ARCA_DIRECT_INDEX:
          ProcessOJRecordForArcaDirect(dataBlock, NO);
          break;

        case TS_NASDAQ_OUCH_INDEX:
          ProcessOJRecordForOuch(dataBlock, NO, VENUE_OUCH);
          break;
          
        case TS_NDAQ_OUBX_INDEX:
          ProcessOJRecordForOuch(dataBlock, NO, VENUE_OUBX);
          break;
          
        case TS_PSX_OUCH_INDEX:
          ProcessOJRecordForOuch(dataBlock, NO, VENUE_PSX);
          break;

        case TS_NASDAQ_RASH_INDEX:
          ProcessOJRecordForRash(dataBlock, NO);
          break;

        case TS_BATSZ_BOE_INDEX:
          ProcessOJRecordForBatsBOE(dataBlock, NO, VENUE_BZX);
          break;
          
        case TS_BYX_BOE_INDEX:
          ProcessOJRecordForBatsBOE(dataBlock, NO, VENUE_BYX);
          break;
          
        case TS_EDGX_DIRECT_INDEX:
          ProcessOJRecordForEdge(dataBlock, NO, VENUE_EDGX);
          break;
          
        case TS_EDGA_DIRECT_INDEX:
          ProcessOJRecordForEdge(dataBlock, NO, VENUE_EDGA);
          break;
          
        default:
          TraceLog(ERROR_LEVEL, "Processing %s: Invalid order index (%d)\n", fName, orderIndex);
          break;
      }
    }
    else if (serverType == __SERVER_AS)
    {
      switch (orderIndex)
      {
        case AS_NASDAQ_RASH_INDEX:
          ProcessOJRecordForRash(dataBlock, NO);
          break;
        case AS_ARCA_DIRECT_INDEX:
          ProcessOJRecordForArcaDirect(dataBlock, NO);
          break;
        default:
          TraceLog(ERROR_LEVEL, "Processing %s: Invalid order index (%d)\n", fName, orderIndex);
          break;
      }
    }
    else if (serverType == __SERVER_PM)
    {
      switch (orderIndex)
      {
        case PM_NASDAQ_RASH_INDEX:
          ProcessOJRecordForRash(dataBlock, NO);
          break;
        case PM_ARCA_DIRECT_INDEX:
          ProcessOJRecordForArcaDirect(dataBlock, NO);
          break;
        default:
          TraceLog(ERROR_LEVEL, "Processing %s: Invalid order index (%d)\n", fName, orderIndex);
          break;
      }
    }
  }
  
  fclose(fp);
  return SUCCESS;
}

int AddOJRawdataToWaitList(t_DataBlock *dataBlock)
{
  int i;
  for (i = 0; i < MAX_OJ_RAWDATA_WAITLIST; i++)
  {
    if (OJRawdataWaitList.dataBlockFlag[i] == 0)  //free
    {
      OJRawdataWaitList.dataBlockFlag[i] = 1;
      memcpy(&OJRawdataWaitList.dataBlock[i], dataBlock, sizeof(t_DataBlock));
      if (i > OJRawdataWaitList.uBound)
        OJRawdataWaitList.uBound = i;
        
      return SUCCESS;
    }
  }

  TraceLog(FATAL_LEVEL, "OJRawdataWaitList is full, please increase MAX_OJ_RAWDATA_WAITLIST value, rebuild AS and try again.\n");
  exit(0);
  return ERROR;
}

void FindAndProcessRawdataInOJWaitList(int venue, int orderId)
{
  int i, ret;
  if (OJOpenOrder[orderId % MAX_OJ_OPEN_ORDER].symbol[0] != 0)
  {
    for (i = 0; i <= OJRawdataWaitList.uBound; i++)
    {
      if (OJRawdataWaitList.dataBlockFlag[i] == 1)  //available
      {
        switch (venue)
        {
          case VENUE_BZX:
          case VENUE_BYX:
            ret = ProcessOJRecordForBatsBOE(OJRawdataWaitList.dataBlock[i], YES, venue);
            break;
          case VENUE_CCG:
            ret = ProcessOJRecordForNyseCCG(OJRawdataWaitList.dataBlock[i], YES);
            break;
          case VENUE_ARCA:
            ret = ProcessOJRecordForArcaDirect(OJRawdataWaitList.dataBlock[i], YES);
            break;
          case VENUE_OUCH:
          case VENUE_PSX:
          case VENUE_OUBX:
            ret = ProcessOJRecordForOuch(OJRawdataWaitList.dataBlock[i], YES, venue);
            break;
          case VENUE_RASH:
            ret = ProcessOJRecordForRash(OJRawdataWaitList.dataBlock[i], YES);
            break;
          case VENUE_EDGX:
          case VENUE_EDGA:
            ret = ProcessOJRecordForEdge(OJRawdataWaitList.dataBlock[i], YES, venue);
            break;
        }
        
        if (ret == SUCCESS)
        {
          // We have used this datablock, so remove it so that it will not be duplicate-processed
          OJRawdataWaitList.dataBlockFlag[i] = 0;
        }
      }
    }
  }
}

/****************************************************************************
- Function name:  ProcessOJRecordForArcaDirect
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessOJRecordForArcaDirect(t_DataBlock dataBlock, const int isFromWaitlist)
{
  char timeStamp[32];
  char symbol[32];
  char buysell = '0';
  double price = 0.0;
  int shares = 0;
  int orderID = 0;
  char tif[6];
  char visibility;
  char ecnOrderID[32];
  char reason[1024];
  char liquidity[6];
  char account[32];
  char capacity;
  int variant;
  int isISO;
  int crossID;
  
  GetAccountName(TYPE_ARCA_DIRECT, dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID], account);
  
  t_IntConverter myInt;
  t_LongConverter myLong;
  
  long usec;
  
  switch (dataBlock.msgContent[0])
  {
    case 'D': //NewOrder
      variant = dataBlock.msgContent[1];
      
      // Shares
      myInt.c[3] = dataBlock.msgContent[16];
      myInt.c[2] = dataBlock.msgContent[17];
      myInt.c[1] = dataBlock.msgContent[18];
      myInt.c[0] = dataBlock.msgContent[19];
      shares = myInt.value;
  
      // Order ID
      myInt.c[3] = dataBlock.msgContent[8];
      myInt.c[2] = dataBlock.msgContent[9];
      myInt.c[1] = dataBlock.msgContent[10];
      myInt.c[0] = dataBlock.msgContent[11];
      orderID = myInt.value;
      
      if (AddOrderIdToList(&__new, orderID) == ERROR)
      {
        return SUCCESS;
      }
      
      if (variant == 1)
      {
        strncpy(symbol, (char *)&dataBlock.msgContent[27], SYMBOL_LEN);
        
        switch (dataBlock.msgContent[51])
        {
          case '1':
            buysell = 'B';
            break;
          case '2':
            buysell = 'S';
            break;
          case '5':
            buysell = 'T';
            break;
        }
    
        // Price and Price Scale
        myInt.c[3] = dataBlock.msgContent[20];
        myInt.c[2] = dataBlock.msgContent[21];
        myInt.c[1] = dataBlock.msgContent[22];
        myInt.c[0] = dataBlock.msgContent[23];
        price = myInt.value * 1.0 / pow(10, dataBlock.msgContent[26] - 48);
  
        switch (dataBlock.msgContent[53])
        {
          case '0': //DAY
            strcpy(tif, "99999");
            break;
          case '3': //IOC
            strcpy(tif, "0");
            break;
        }
      
        capacity = dataBlock.msgContent[54];
      
        isISO = (dataBlock.msgContent[69] == 'Y')?1:0; //ISO Flag
        
      }
      else // variant 3
      {
        strncpy(symbol, (char *)&dataBlock.msgContent[61], SYMBOL_LEN);
      
        switch (dataBlock.msgContent[93])
        {
          case '1':
            buysell = 'B';
            break;
          case '2':
            buysell = 'S';
            break;
          case '5':
            buysell = 'T';
            break;
        }
      
        // Price and Price Scale
        myInt.c[3] = dataBlock.msgContent[48];
        myInt.c[2] = dataBlock.msgContent[49];
        myInt.c[1] = dataBlock.msgContent[50];
        myInt.c[0] = dataBlock.msgContent[51];
        price = myInt.value * 1.0 / pow(10, dataBlock.msgContent[58] - 48);
        
        switch (dataBlock.msgContent[95])
        {
          case '0': //DAY
            strcpy(tif, "99999");
            break;
          case '3': //IOC
            strcpy(tif, "0");
            break;
        }
        
        capacity = dataBlock.msgContent[96];
      
        isISO = (dataBlock.msgContent[155] == 'Y')?1:0; //ISO Flag
      }
      
      //Store to memory for re-use
      memcpy(OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].symbol, symbol, SYMBOL_LEN);
      OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].leftShares = shares;
      OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].buysell = buysell;
      
      if (orderID < 500000) //Manual order
      {
        if (strcmp(account, TradingAccount.DIRECT[FIRST_ACCOUNT]) == 0)
        {
          Manual_Order_Account_Mapping[orderID] = FIRST_ACCOUNT;
        }
        else if (strcmp(account, TradingAccount.DIRECT[SECOND_ACCOUNT]) == 0)
        {
          Manual_Order_Account_Mapping[orderID] = SECOND_ACCOUNT;
        }
        else if (strcmp(account, TradingAccount.DIRECT[THIRD_ACCOUNT]) == 0)
        {
          Manual_Order_Account_Mapping[orderID] = THIRD_ACCOUNT;
        }
        else
        {
          Manual_Order_Account_Mapping[orderID] = FOURTH_ACCOUNT;
        }
      }
      
      visibility = 'Y';
      
      usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]) / 1000;
      sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
      
      crossID = *((int *) &dataBlock.addContent[DATABLOCK_OFFSET_CROSS_ID]);
      
      AddOJNewOrderRecord(timeStamp, symbol, VENUE_ARCA, buysell, price, shares, orderID, tif, visibility, isISO, account, NULL, capacity, crossID, GetBBReason(&dataBlock, orderID));
      FindAndProcessRawdataInOJWaitList(VENUE_ARCA, orderID);
      break;

    case 'a': // Order Ack
    {
      // Order ID
      myInt.c[3] = dataBlock.msgContent[24];
      myInt.c[2] = dataBlock.msgContent[25];
      myInt.c[1] = dataBlock.msgContent[26];
      myInt.c[0] = dataBlock.msgContent[27];
      orderID = myInt.value;
      
      char exOrderId[32];
      sprintf(exOrderId, "%d", orderID);
      
      memcpy(symbol, OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].symbol, SYMBOL_LEN);
      if (symbol[0] == 0)
      {
        if (isFromWaitlist == NO) AddOJRawdataToWaitList(&dataBlock);
        return ERROR;
      }

      if (AddOrderIdToList(&__ack, orderID) == ERROR)
      {
        return SUCCESS;
      }
      
      myLong.c[7] = dataBlock.msgContent[28];
      myLong.c[6] = dataBlock.msgContent[29];
      myLong.c[5] = dataBlock.msgContent[30];
      myLong.c[4] = dataBlock.msgContent[31];
      myLong.c[3] = dataBlock.msgContent[32];
      myLong.c[2] = dataBlock.msgContent[33];
      myLong.c[1] = dataBlock.msgContent[34];
      myLong.c[0] = dataBlock.msgContent[35];
      sprintf(ecnOrderID, "%lu",  myLong.value);
  
      capacity = 'P';
      usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]) / 1000;
      sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
      AddOJOrderAcceptedRecord(timeStamp, symbol, exOrderId, orderID, ecnOrderID, VENUE_ARCA, account, capacity);
    }
      break;
    case '2': // Order Fill
    {
      myInt.c[3] = dataBlock.msgContent[24];
      myInt.c[2] = dataBlock.msgContent[25];
      myInt.c[1] = dataBlock.msgContent[26];
      myInt.c[0] = dataBlock.msgContent[27];
      orderID = myInt.value;
      
      char exOrderId[32];
      sprintf(exOrderId, "%d", orderID);
      
      memcpy(symbol, OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].symbol, SYMBOL_LEN);
      if (symbol[0] == 0)
      {
        if (isFromWaitlist == NO) AddOJRawdataToWaitList(&dataBlock);
        return ERROR;
      }
      
      switch (dataBlock.msgContent[74])
      {
        case '1':
          buysell = 'B';
          break;
        case '2':
          buysell = 'S';
          break;
        case '5':
          buysell = 'T';
          break;
        default:
          break;
      }
      
      // Price
      myInt.c[3] = dataBlock.msgContent[68];
      myInt.c[2] = dataBlock.msgContent[69];
      myInt.c[1] = dataBlock.msgContent[70];
      myInt.c[0] = dataBlock.msgContent[71];
      price = myInt.value * 1.0 / pow( 10, dataBlock.msgContent[72] - 48 );
      
      // Shares
      myInt.c[3] = dataBlock.msgContent[64];
      myInt.c[2] = dataBlock.msgContent[65];
      myInt.c[1] = dataBlock.msgContent[66];
      myInt.c[0] = dataBlock.msgContent[67];
      shares = myInt.value;
      
      liquidity[0] = dataBlock.msgContent[73];
      liquidity[1] = 0;

      //Arca Order ID
      myLong.c[7] = dataBlock.msgContent[28];
      myLong.c[6] = dataBlock.msgContent[29];
      myLong.c[5] = dataBlock.msgContent[30];
      myLong.c[4] = dataBlock.msgContent[31];
      myLong.c[3] = dataBlock.msgContent[32];
      myLong.c[2] = dataBlock.msgContent[33];
      myLong.c[1] = dataBlock.msgContent[34];
      myLong.c[0] = dataBlock.msgContent[35];
      sprintf(ecnOrderID, "%lu",  myLong.value);
      
      //Arca Execution ID
      char execID[32];
      myLong.c[7] = dataBlock.msgContent[36];
      myLong.c[6] = dataBlock.msgContent[37];
      myLong.c[5] = dataBlock.msgContent[38];
      myLong.c[4] = dataBlock.msgContent[39];
      myLong.c[3] = dataBlock.msgContent[40];
      myLong.c[2] = dataBlock.msgContent[41];
      myLong.c[1] = dataBlock.msgContent[42];
      myLong.c[0] = dataBlock.msgContent[43];
      sprintf(execID, "%lu",  myLong.value);
      
      if (AddExecDataToList(&__exec, orderID, myLong.value) == ERROR)
      {
        return SUCCESS;
      }
      
      OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].leftShares -= shares;
      usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]) / 1000;
      sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
      
      AddOJOrderFillRecord(timeStamp, symbol, exOrderId, VENUE_ARCA, buysell, price, shares, orderID, liquidity,
                ecnOrderID, execID, "1", (OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].leftShares <= 0)?1:0, account, 0);
    }
    break;
    case '4': // Order killed
      // Order ID
      myInt.c[3] = dataBlock.msgContent[24];
      myInt.c[2] = dataBlock.msgContent[25];
      myInt.c[1] = dataBlock.msgContent[26];
      myInt.c[0] = dataBlock.msgContent[27];
      orderID = myInt.value;
    
      memcpy(symbol, OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].symbol, SYMBOL_LEN);
      if (symbol[0] == 0)
      {
        if (isFromWaitlist == NO) AddOJRawdataToWaitList(&dataBlock);
        return ERROR;
      }
      
      if (AddOrderIdToList(&__cancel, orderID) == ERROR)
      {
        return SUCCESS;
      }
      
      //Arca Order ID
      myLong.c[7] = dataBlock.msgContent[28];
      myLong.c[6] = dataBlock.msgContent[29];
      myLong.c[5] = dataBlock.msgContent[30];
      myLong.c[4] = dataBlock.msgContent[31];
      myLong.c[3] = dataBlock.msgContent[32];
      myLong.c[2] = dataBlock.msgContent[33];
      myLong.c[1] = dataBlock.msgContent[34];
      myLong.c[0] = dataBlock.msgContent[35];
      sprintf(ecnOrderID, "%lu",  myLong.value);
      
      int _reasonCode = dataBlock.msgContent[36];
      if (_reasonCode == 0)
      {
        strcpy(reason, "user-initiated%20kill");
      }
      else if(_reasonCode == 1)
      {
        strcpy(reason, "exchange-initiated%20for%20PNP%20crossed%20market");
      }
      else
      {
        sprintf(reason, "unknown%%20reason,%%20reason%%20code%%20%%3d%%20%d", _reasonCode);
      }
      usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]) / 1000;
      sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
      AddOJOrderCancelResponseRecord(timeStamp, symbol, orderID, ecnOrderID, reason, VENUE_ARCA, OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].leftShares, 1, account);
                    
      break;
    case 'F': // Cancel Request
      // Order ID
      myInt.c[3] = dataBlock.msgContent[16];
      myInt.c[2] = dataBlock.msgContent[17];
      myInt.c[1] = dataBlock.msgContent[18];
      myInt.c[0] = dataBlock.msgContent[19];
      orderID = myInt.value;
      
      memcpy(symbol, OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].symbol, SYMBOL_LEN);
      if (symbol[0] == 0)
      {
        if (isFromWaitlist == NO) AddOJRawdataToWaitList(&dataBlock);
        return ERROR;
      }
      usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]) / 1000;
      sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
      AddOJOrderCancelRecord(timeStamp, orderID, symbol, VENUE_ARCA, OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].leftShares);
      break;
    case '8': // Rejected
      // Order ID
      myInt.c[3] = dataBlock.msgContent[24];
      myInt.c[2] = dataBlock.msgContent[25];
      myInt.c[1] = dataBlock.msgContent[26];
      myInt.c[0] = dataBlock.msgContent[27];
      orderID = myInt.value;
      
      memcpy(symbol, OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].symbol, SYMBOL_LEN);
      if (symbol[0] == 0)
      {
        if (isFromWaitlist == NO) AddOJRawdataToWaitList(&dataBlock);
        return ERROR;
      }
      
      if (AddOrderIdToList(&__reject, orderID) == ERROR)
      {
        return SUCCESS;
      }
      
      if (dataBlock.msgContent[32] == '2') // Cancel rejected
      {
        return 0;
      }
      
      //When order is rejected (ARCA DIRECT), there is no ARCA Order ID, there is only Client Order ID
      strcpy(ecnOrderID, "''");
      
      //reason
      int i, offset = 0;
      char rejectText[41];
      memcpy(rejectText, &dataBlock.msgContent[33], 40);
      rejectText[40] = 0;
      
      int len = strlen(rejectText);
      
      for (i=0; i<len; i++)
      {
        if (rejectText[i] == ' ')
        {
          reason[offset++] = '%';
          reason[offset++] = '2';
          reason[offset++] = '0';
        }
        else
        {
          reason[offset++] = rejectText[i];
        }
      }
      reason[offset] = 0;

      char reasonCode[2];
      reasonCode[0] = dataBlock.msgContent[73];
      reasonCode[1] = 0;
      usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]) / 1000;
      sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
      AddOJOrderRejectRecord(timeStamp, orderID, ecnOrderID, reason, symbol, VENUE_ARCA, reasonCode, account);
      break;
    case '6': // Cancel ACK
      //ProcessCancelACKOfARCA_DIRECTOrder( dataBlock, type );
      break;
    case 'C': // Bust or Correct
      //ProcessBustOrCorrectOfARCA_DIRECTOrder( dataBlock, type );
      break;
    default:
      //TraceLog(DEBUG_LEVEL,  "ARCA DIRECT Order: Invalid order message, msgType = %c ASCII = %d\n", dataBlock.msgContent[0], dataBlock.msgContent[0] );
      break;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessOJRecordForOuch
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessOJRecordForOuch(t_DataBlock dataBlock, const int isFromWaitlist, const int venueType)
{
  char timeStamp[32];
  char symbol[32];
  char buysell = '0';
  double price = 0.0;
  int shares = 0;
  int orderID = 0;
  char tif[6];
  char visibility;
  char ecnOrderID[32];
  char reason[1024];
  char liquidity[6];
  char fieldToken[15];
  char account[32];
  unsigned long _ecnOrderId;
  int _serverId, tsId;
  
  int _oecType = venueType + TYPE_BASE;
  GetAccountName(_oecType, dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID], account);

  long usec;
  
  if( dataBlock.msgContent[0] == 'S' )
  {
    usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]) / 1000;
    sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
  
    switch ( dataBlock.msgContent[1] )
    {
      case 'A': // Accepted order
      {
        strncpy(symbol, (char *) &dataBlock.msgContent[29], SYMBOL_LEN);
        TrimRight(symbol, strnlen(symbol, SYMBOL_LEN));
        
        // Order ID
        memcpy(fieldToken, &dataBlock.msgContent[12], 12);
        fieldToken[12] = 0;
        orderID = atoi(fieldToken);
        
        char exOrderId[32];
        exOrderId[0] = dataBlock.msgContent[10];
        exOrderId[1] = dataBlock.msgContent[11];
        sprintf(&exOrderId[2], "%d", orderID);
        
        if (AddOrderIdToList(&__ack, orderID) == ERROR)
        {
          return SUCCESS;
        }
        
        //Order ref num
        _ecnOrderId = GetLongNumberBigEndian((char *)&dataBlock.msgContent[50]);
        
        tsId = (orderID / 1000000) - 1;
        _serverId = (tsId == -1) ? MAX_TRADE_SERVER_CONNECTIONS : tsId;
        ExchangeOrderIdMapping[_serverId][orderID % MAX_ORDER_PLACEMENT] = _ecnOrderId;
        
        char capacity = dataBlock.msgContent[58];       
        sprintf(ecnOrderID, "%lu", _ecnOrderId);
        
        AddOJOrderAcceptedRecord(timeStamp, symbol, exOrderId, orderID, ecnOrderID, venueType, account, capacity);
        
        char orderState = dataBlock.msgContent[65];
        if (orderState == 'D')
        {
          // This is special case for OUCH, that is when ACK comes but indicating order is dead, so we must add one more OJ record
          // to indicate order is canceled
          
          //shares to be canceled:
          int shares = GetIntNumberBigEndian(&dataBlock.msgContent[25]);
          AddOJOrderCancelResponseRecord(timeStamp, symbol, orderID, ecnOrderID, "Order%20Dead:%20order%20was%20accepted%20and%20atomically%20canceled.", venueType, shares, 1, account);
        }
      }  
        break;
      
      case 'C': // Canceled order
      {
        // OrderId
        memcpy(fieldToken, &dataBlock.msgContent[12], 12);
        fieldToken[12] = 0;
        orderID = atoi(fieldToken);

        memcpy(symbol, OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].symbol, SYMBOL_LEN);
        if (symbol[0] == 0)
        {
          if (isFromWaitlist == NO) AddOJRawdataToWaitList(&dataBlock);
          return ERROR;
        }

        if (AddOrderIdToList(&__cancel, orderID) == ERROR)
        {
          return SUCCESS;
        }
        
        tsId = (orderID / 1000000) - 1;
        _serverId = (tsId == -1) ? MAX_TRADE_SERVER_CONNECTIONS : tsId;
        if (ExchangeOrderIdMapping[_serverId][orderID % MAX_ORDER_PLACEMENT] != 0)
        {
          sprintf(ecnOrderID, "%lu", ExchangeOrderIdMapping[_serverId][orderID % MAX_ORDER_PLACEMENT]);
        }
        else
        {
          TraceLog(ERROR_LEVEL, "%s - %s Canceled: Could not get ExOrderId, orderID: %d\n", __func__, GetOECName(_oecType), orderID);
          strcpy(ecnOrderID, "''"); //There is no Order Reference Number in this message type
        }
        
        shares = GetIntNumberBigEndian(&dataBlock.msgContent[24]);
        
        //reason text
        switch (dataBlock.msgContent[28])
        {
          /*
          U User requested cancel. Sent in response to a Cancel Order Message or a Replace Order Message
          I Immediate or Cancel order. This order was originally sent with a timeout of zero and no further matches were available on the book so the remaining unexecuted shares were immediately canceled
          T Timeout. The Time In Force for this order has expired
          S Supervisory. This order was manually canceled or reduced by a NASDAQ supervisory terminal. This is usually in response to a participant request via telephone.
          D This order cannot be executed because of a regulatory restriction (e.g.: trade through restrictions).
          Q Self Match Prevention. The order was cancelled because it would have executed with an existing order entered by the same MPID.
          */
          case 'U':
            strcpy(reason, "User%20requested%20cancel.%20Sent%20in%20response%20to%20a%20Cancel%20Order%20Message%20or%20a%20Replace%20Order%20Message");
            break;
          case 'I':
            strcpy(reason, "Immediate%20or%20Cancel%20order.%20This%20order%20was%20originally%20sent%20with%20a%20timeout%20of%20zero%20and%20no%20further%20matches%20were%20available%20on%20the%20book%20so%20the%20remaining%20unexecuted%20shares%20were%20immediately%20canceled");
            break;
          case 'T':
            strcpy(reason, "Timeout.%20The%20Time%20In%20Force%20for%20this%20order%20has%20expired");
            break;
          case 'S':
            strcpy(reason, "Supervisory.%20This%20order%20was%20manually%20canceled%20or%20reduced%20by%20a%20NASDAQ%20supervisory%20terminal.%20This%20is%20usually%20in%20response%20to%20a%20participant%20request%20via%20telephone");
            break;
          case 'D':
            strcpy(reason, "This%20order%20cannot%20be%20executed%20because%20of%20a%20regulatory%20restriction%20(e.g.:%20trade%20through%20restrictions)");
            break;
          case 'Q':
            strcpy(reason, "Self%20Match%20Prevention.%20The%20order%20was%20cancelled%20because%20it%20would%20have%20executed%20with%20an%20existing%20order%20entered%20by%20the%20same%20MPID");
            break;
          default:
            sprintf(reason, "Unknown%%20reason,%%20reason%%20code%%20%%3d%%20%c", dataBlock.msgContent[28]);
            break;
        }

        AddOJOrderCancelResponseRecord(timeStamp, symbol, orderID, ecnOrderID, reason, venueType, shares, 1, account);
      }
        break;
        
      case 'E': // Executed order
        {
        // OrderId
        memcpy(fieldToken, &dataBlock.msgContent[12], 12);
        fieldToken[12] = 0;
        orderID = atoi(fieldToken);
        
        char exOrderId[32];
        exOrderId[0] = dataBlock.msgContent[10];
        exOrderId[1] = dataBlock.msgContent[11];
        sprintf(&exOrderId[2], "%d", orderID);
        
        memcpy(symbol, OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].symbol, SYMBOL_LEN);
        if (symbol[0] == 0)
        {
          if (isFromWaitlist == NO) AddOJRawdataToWaitList(&dataBlock);
          return ERROR;
        }
        
        long _execID = GetLongNumberBigEndian((char *)&dataBlock.msgContent[33]);
        if (AddExecDataToList(&__exec, orderID, _execID) == ERROR)
        {
          return SUCCESS;
        }

        price = ((double)GetIntNumberBigEndian(&dataBlock.msgContent[28])) / 10000;
        
        shares = GetIntNumberBigEndian(&dataBlock.msgContent[24]);
        
        OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].leftShares -= shares;
        
        liquidity[0] = dataBlock.msgContent[32];
        liquidity[1] = 0;
        
        tsId = (orderID / 1000000) - 1;
        _serverId = (tsId == -1) ? MAX_TRADE_SERVER_CONNECTIONS : tsId;
        if (ExchangeOrderIdMapping[_serverId][orderID % MAX_ORDER_PLACEMENT] != 0)
        {
          sprintf(ecnOrderID, "%lu", ExchangeOrderIdMapping[_serverId][orderID % MAX_ORDER_PLACEMENT]);
        }
        else
        {
          TraceLog(ERROR_LEVEL, "%s - %s Executed: Could not get ExOrderId, orderID: %d\n", __func__, GetOECName(_oecType), orderID);
          strcpy(ecnOrderID, "''"); //There is no Order Reference Number in this message type
        }
        
        char execID[32];
        sprintf(execID, "%lu", _execID);
        
        AddOJOrderFillRecord(timeStamp, symbol, exOrderId, venueType, OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].buysell, price, shares, orderID, liquidity, 
                  ecnOrderID, execID, "1", (OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].leftShares <= 0)?1:0, account, 0);
        }
        break;

      case 'J': // Rejected order
      {
        // OrderId
        memcpy( fieldToken, &dataBlock.msgContent[12], 12);
        fieldToken[12] = 0;
        orderID = atoi(fieldToken);
        
        memcpy(symbol, OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].symbol, SYMBOL_LEN);
        if (symbol[0] == 0)
        {
          if (isFromWaitlist == NO) AddOJRawdataToWaitList(&dataBlock);
          return ERROR;
        }
        
        if (AddOrderIdToList(&__reject, orderID) == ERROR)
        {
          return SUCCESS;
        }

        strcpy(ecnOrderID, "''"); //There is no Order Reference Number in this message type
        
        char reasonCode[2];
        reasonCode[0] = dataBlock.msgContent[24];
        reasonCode[1] = 0;
  
        switch (reasonCode[0])
        {
          case 'T':
            strcpy(reason, "Test%20Mode%20%20This%20OUCH%20Account%20is%20configured%20for%20test%20mode%20and%20is%20not%20able%20to%20accept%20orders%20in%20non-TEST%20securities");
            break;
          case 'H':
            strcpy(reason, "Halted%20%20There%20is%20currently%20a%20trading%20halt%20so%20no%20orders%20can%20be%20accepted%20in%20this%20stock%20at%20this%20time");
            break;
          case 'Z':
            strcpy(reason, "Shares%20exceeds%20configured%20safety%20threshold%20%20The%20number%20of%20shares%20entered%20must%20be%20less%20than%20the%20safety%20threshold%20configured%20for%20this%20Account.%20The%20safety%20threshold%20can%20be%20added/updated%20through%20NASDAQ%20Subscriber%20Services");
            break;
          case 'S':
            strcpy(reason, "Invalid%20stock%20%20The%20stock%20field%20must%20be%20a%20valid%20issue,%20tradable%20on%20NASDAQ");
            break;
          case 'D':
            strcpy(reason, "Invalid%20Display%20Type%20%20Sent%20when%20Display%20Type%20Entered%20cannot%20be%20accepted%20in%20current%20circumstances%20and%20cant%20be%20simply%20converted%20to%20a%20valid%20Display%20Type");
            break;
          case 'C':
            strcpy(reason, "NASDAQ%20is%20closed");
            break;
          case 'L':
            strcpy(reason, "Requested%20firm%20not%20authorized%20for%20requested%20clearing%20type%20on%20this%20account%20%20To%20authorize%20additional%20firms,%20use%20the%20NASDAQ%20Service%20Bureau%20Agreement");
            break;
          case 'M':
            strcpy(reason, "Outside%20of%20permitted%20times%20for%20requested%20clearing%20type");
            break;
          case 'R':
            strcpy(reason, "This%20order%20is%20not%20allowed%20in%20this%20type%20of%20cross%20(stock%20or%20time%20restrictions)");
            break;
          case 'X':
            strcpy(reason, "Invalid%20price");
            break;
          case 'N':
            strcpy(reason, "Invalid%20Minimum%20Quantity");
            break;
          default:
            strcpy(reason, "Unknown%20reason");
            break;
        }
        
        AddOJOrderRejectRecord(timeStamp, orderID, ecnOrderID, reason, symbol, venueType, reasonCode, account);
      }
        break;

      case 'B': // Broken trade
        //ProcessBrokenOrderOfOUCHOrder( dataBlock, type );
        break;
      
      case 'K': // Price correction
        //ProcessPriceCorrectionOfOUCHOrder( dataBlock, type );
        break;
      
      case 'P': // Cancel pending
        //ProcessCancelPendingOfOUCHOrder( dataBlock, type );
        break;
        
      case 'I': // Cancel reject
        //ProcessCancelRejectOfOUCHOrder( dataBlock, type );
        break;

      default:
        TraceLog(ERROR_LEVEL, "%s Order: Invalid order message, msgType = %c ASCII = %d\n", GetOECName(_oecType), dataBlock.msgContent[1], dataBlock.msgContent[1] );
        break;
    }
  }
  else if (dataBlock.msgContent[0] == 'U')
  {
    usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]) / 1000;
    sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
  
    if (dataBlock.msgContent[1] == 'O')  // New Order
    {
      strncpy(symbol, (char *) &dataBlock.msgContent[21], SYMBOL_LEN);
      TrimRight(symbol, strnlen(symbol, SYMBOL_LEN));
      
      buysell = '0';
      switch (dataBlock.msgContent[16])
      {
        case 'B':
          buysell = 'B';
          break;
        case 'S':
          buysell = 'S';
          break;
        case 'T':
          buysell = 'T';
          break;
      }
      
      shares = GetIntNumberBigEndian(&dataBlock.msgContent[17]);
      
      price = ((double)GetIntNumberBigEndian(&dataBlock.msgContent[29])) / 10000;
      
      // Order ID
      memcpy(fieldToken, &dataBlock.msgContent[4], 12);
      fieldToken[12] = 0;
      orderID = atoi(fieldToken);
      
      if (AddOrderIdToList(&__new, orderID) == ERROR)
      {
        return SUCCESS;
      }
        
      int tifValue = GetIntNumberBigEndian(&dataBlock.msgContent[33]);
      if (tifValue == 0)    //IOC
      {
        strcpy(tif, "0");
      }
      else if(tifValue > 0) //DAY
      {
        strcpy(tif, "99999");
      }
      
      //Store for re-use
      memcpy(OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].symbol, symbol, SYMBOL_LEN);
      OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].leftShares = shares;
      OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].buysell = buysell;
      
      int isISO = (dataBlock.msgContent[43] == 'Y')?1:0; //ISO Flag
      visibility = dataBlock.msgContent[41];
      char capacity = dataBlock.msgContent[42];
      
      int crossID = *((int *) &dataBlock.addContent[DATABLOCK_OFFSET_CROSS_ID]);
      
      AddOJNewOrderRecord(timeStamp, symbol, venueType, buysell, price, shares, orderID, tif, visibility, isISO, account, NULL, capacity, crossID, GetBBReason(&dataBlock, orderID));
      FindAndProcessRawdataInOJWaitList(venueType, orderID);
    }
    else if (dataBlock.msgContent[1] == 'X') // Cancel Request
    {
      // OrderId
      memcpy(fieldToken, &dataBlock.msgContent[4], 12);
      fieldToken[12] = 0;
      orderID = atoi(fieldToken);
      
      if (OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].symbol[0] == 0)
      {
        if (isFromWaitlist == NO) AddOJRawdataToWaitList(&dataBlock);
        return ERROR;
      }
      
      shares = GetIntNumberBigEndian(&dataBlock.msgContent[16]);
      
      AddOJOrderCancelRecord(timeStamp, orderID, OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].symbol, VENUE_OUCH, shares);
    }
    else
    {
      TraceLog(ERROR_LEVEL, "%s Order: Invalid order message, msgType = %c ASCII = %d\n", GetOECName(_oecType), dataBlock.msgContent[1], dataBlock.msgContent[1] );
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessOJRecordForRash
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessOJRecordForRash(t_DataBlock dataBlock, const int isFromWaitlist)
{
  char timeStamp[32];
  char symbol[32];
  char buysell = '0';
  double price = 0.0;
  int shares = 0;
  int orderID = 0;
  char tif[6];
  char visibility;
  char ecnOrderID[32];
  char reason[1024];
  char liquidity[6];
  char fieldToken[15];
  char account[32];
  int _serverId, tsId;
  
  long usec;
  
  GetAccountName(TYPE_NASDAQ_RASH, dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID], account);
  
  if( dataBlock.msgContent[0] == 'S' )
  {
    usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]) / 1000;
    sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
    
    switch ( dataBlock.msgContent[9] )
    {
      case 'A':   // Accepted order: 
      case 'R':   // R = Accepted Order Message with Cross Functionality
      {
        strncpy(symbol, (char *) &dataBlock.msgContent[31], SYMBOL_LEN);
        TrimRight(symbol, strnlen(symbol, SYMBOL_LEN) );
        
        // Order ID
        memcpy(fieldToken, &dataBlock.msgContent[12], 12);
        fieldToken[12] = 0;
        orderID = atoi(fieldToken);
        
        char exOrderId[32];
        exOrderId[0] = dataBlock.msgContent[10];
        exOrderId[1] = dataBlock.msgContent[11];
        sprintf(&exOrderId[2], "%d", orderID);
        
        if (AddOrderIdToList(&__ack, orderID) == ERROR)
        {
          return SUCCESS;
        }

        //Order reference number
        memcpy(fieldToken, &dataBlock.msgContent[59], 9);
        fieldToken[9] = 0;
        sprintf(ecnOrderID, "%d", atoi(fieldToken));
        
        tsId = (orderID / 1000000) - 1;
        _serverId = (tsId == -1) ? MAX_TRADE_SERVER_CONNECTIONS : tsId;
        ExchangeOrderIdMapping[_serverId][orderID % MAX_ORDER_PLACEMENT] = atoi(fieldToken);
        
        char capacity = dataBlock.msgContent[114];
        
        AddOJOrderAcceptedRecord(timeStamp, symbol, exOrderId, orderID, ecnOrderID, VENUE_RASH, account, capacity);
      }
        break;
      
      case 'C':   // Canceled order
        // OrderId
        memcpy(fieldToken, &dataBlock.msgContent[12], 12);
        fieldToken[12] = 0;
        orderID = atoi(fieldToken);
        
        if (OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].symbol[0] == 0)
        {
          if (isFromWaitlist == NO) AddOJRawdataToWaitList(&dataBlock);
          return ERROR;
        }
        
        // Shares
        memcpy(fieldToken, &dataBlock.msgContent[24], 6);
        fieldToken[6] = 0;
        shares = atoi(fieldToken);
        
        if (orderID < 1000000)
        {
          if (AddOrderIdToList(&__cancel, orderID) == ERROR)  //Duplicated
          {
            return 0;
          }
        }
        else
        {
          if (OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].canceledShares + shares > OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].leftShares)
          {
            return 0;
          }
          
          OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].canceledShares += shares;
        }
        
        tsId = (orderID / 1000000) - 1;
        _serverId = (tsId == -1) ? MAX_TRADE_SERVER_CONNECTIONS : tsId;
        if (ExchangeOrderIdMapping[_serverId][orderID % MAX_ORDER_PLACEMENT] != 0)
        {
          sprintf(ecnOrderID, "%lu", ExchangeOrderIdMapping[_serverId][orderID % MAX_ORDER_PLACEMENT]);
        }
        else
        {
          TraceLog(ERROR_LEVEL, "%s - Canceled: Could not get ExOrderId, orderID: %d\n", __func__, orderID);
          strcpy(ecnOrderID, "''");
        }
        
        switch (dataBlock.msgContent[30])
        {
          /*
          U User requested cancel. Sent in response to a Cancel Request Message.
          I Immediate or Cancel Order.
          T Timeout. The Time In Force for this order has expired
          S Supervisory. The order was manually canceled or reduced by an NASDAQ supervisory terminal.
          D This order cannot be executed because of a regulatory restriction (e.g.: trade through restrictions).
          Q Self Match Prevention. The order was cancelled because it would have executed with an existing order entered by the same MPID.
          */
          case 'U':
            strcpy(reason, "User%20requested%20cancel.%20Sent%20in%20response%20to%20a%20Cancel%20Request%20Message");
            break;
          case 'I':
            strcpy(reason, "Immediate%20or%20Cancel%20Order");
            break;
          case 'T':
            strcpy(reason, "Timeout.%20The%20Time%20In%20Force%20for%20this%20order%20has%20expired");
            break;
          case 'S':
            strcpy(reason, "Supervisory.%20The%20order%20was%20manually%20canceled%20or%20reduced%20by%20an%20NASDAQ%20supervisory%20terminal");
            break;
          case 'D':
            strcpy(reason, "This%20order%20cannot%20be%20executed%20because%20of%20a%20regulatory%20restriction%20(e.g.:%20trade%20through%20restrictions)");
            break;
          case 'Q':
            strcpy(reason, "Self%20Match%20Prevention.%20The%20order%20was%20cancelled%20because%20it%20would%20have%20executed%20with%20an%20existing%20order%20entered%20by%20the%20same%20MPID");
            break;
          default:
            sprintf(reason, "Unknown%%20reason,%%20reason%%20code%%20%%3d%%20%c", dataBlock.msgContent[30]);
            break;
        }
        
        AddOJOrderCancelResponseRecord(timeStamp, OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].symbol, 
                    orderID, ecnOrderID, reason, VENUE_RASH, shares, 1, account);
        break;
      
      case 'E':   // Executed order
      {
        // OrderId
        memcpy( fieldToken, &dataBlock.msgContent[12], 12);
        fieldToken[12] = 0;
        orderID = atoi( fieldToken );
        
        char exOrderId[32];
        exOrderId[0] = dataBlock.msgContent[10];
        exOrderId[1] = dataBlock.msgContent[11];
        sprintf(&exOrderId[2], "%d", orderID);
        
        if (OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].symbol[0] == 0)
        {
          if (isFromWaitlist == NO) AddOJRawdataToWaitList(&dataBlock);
          return ERROR;
        }
        
        //MatchNo
        memcpy(fieldToken, &dataBlock.msgContent[41], 9);
        fieldToken[9] = 0;
        long _execID = atoi(fieldToken);
        if (AddExecDataToList(&__exec, orderID, _execID) == ERROR)
        {
          return SUCCESS;
        }

        // Shares
        memcpy( fieldToken, &dataBlock.msgContent[24], 6 );
        fieldToken[6] = 0;
        shares = atoi( fieldToken );
        
        liquidity[0] = dataBlock.msgContent[40];
        liquidity[1] = 0;
        
        memcpy( fieldToken, &dataBlock.msgContent[30], 10 );
        fieldToken[10] = 0;
        price = atof( fieldToken ) / 10000;
        
        OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].leftShares -= shares;
        
        tsId = (orderID / 1000000) - 1;
        _serverId = (tsId == -1) ? MAX_TRADE_SERVER_CONNECTIONS : tsId;
        if (ExchangeOrderIdMapping[_serverId][orderID % MAX_ORDER_PLACEMENT] != 0)
        {
          sprintf(ecnOrderID, "%lu", ExchangeOrderIdMapping[_serverId][orderID % MAX_ORDER_PLACEMENT]);
        }
        else
        {
          TraceLog(ERROR_LEVEL, "%s - Canceled: Could not get ExOrderId, orderID: %d\n", __func__, orderID);
          strcpy(ecnOrderID, "''");
        }
        
        char execID[32];
        sprintf(execID, "%lu", _execID);
        
        AddOJOrderFillRecord(timeStamp, OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].symbol, exOrderId, VENUE_RASH, OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].buysell, price,
              shares, orderID, liquidity,
              ecnOrderID, execID, "1", (OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].leftShares <= 0)?1:0, account, 0);
      }
        break;

      case 'J': // Rejected order
        // OrderId
        memcpy( fieldToken, &dataBlock.msgContent[12], 12);
        fieldToken[12] = 0;
        orderID = atoi(fieldToken);
        
        memcpy(symbol, OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].symbol, SYMBOL_LEN);
        if (symbol[0] == 0)
        {
          if (isFromWaitlist == NO) AddOJRawdataToWaitList(&dataBlock);
          return ERROR;
        }

        if (AddOrderIdToList(&__reject, orderID) == ERROR)
        {
          return SUCCESS;
        }
        
        strcpy(ecnOrderID, "''"); //There is no Order Reference Number in this message type
        
        char reasonCode[2];
        reasonCode[0] = dataBlock.msgContent[24];
        reasonCode[1] = 0;
  
        switch (reasonCode[0])
        {
          case 'Y':
            strcpy(reason, "No%20Shares%20Found%20for%20Routing");
            break;
          case 'C':
            strcpy(reason, "NASDAQ%20is%20Closed");
            break;
          case 'I':
            strcpy(reason, "Invalid%20Order%20Side");
            break;
          case 'E':
            strcpy(reason, "Invalid%20Peg");
            break;
          case 'L':
            strcpy(reason, "Invalid%20Firm");
            break;
          case 'Z':
            strcpy(reason, "Quantity%20Exceeds%20Threshold");
            break;
          case 'O':
            strcpy(reason, "Other.%20A%20reason%20not%20contemplated%20in%20this%20version%20of%20RASH");
            break;
          case 'B':
            strcpy(reason, "Quote%20not%20available%20for%20pegged%20order");
            break;
          case 'P':
            strcpy(reason, "Pegging%20Not%20Allowed");
            break;
          case 'X':
            strcpy(reason, "Invalid%20Price");
            break;
          case 'G':
            strcpy(reason, "Destination%20Not%20Available");
            break;
          case 'J':
            strcpy(reason, "Processing%20Error");
            break;
          case 'N':
            strcpy(reason, "Invalid%20Routing%20Instructions");
            break;
          case 'D':
            strcpy(reason, "Invalid%20Display%20value");
            break;
          case 'M':
            strcpy(reason, "Outside%20of%20Permitted%20Times%20for%20Clearing%20Destination");
            break;
          case 'H':
            strcpy(reason, "Security%20is%20Halted");
            break;
          case 'S':
            strcpy(reason, "Invalid%20Symbol");
            break;
          case 'Q':
            strcpy(reason, "Invalid%20Order%20Quantity");
            break;
          case 'K':
            strcpy(reason, "Invalid%20Minimum%20Quantity");
            break;
          case 'W':
            strcpy(reason, "Invalid%20Destination");
            break;
          case 'A':
            strcpy(reason, "Advance%20Features%20Not%20Allowed");
            break;
          case 'U':
            strcpy(reason, "Possible%20Duplicate%20Order");
            break;
          case 'V':
            strcpy(reason, "Invalid%20Order%20Type");
            break;
          case 'T':
            strcpy(reason, "Test%20Mode");
            break;
          case 'R':
            strcpy(reason, "Routing%20Not%20Allowed");
            break;
          default:
            strcpy(reason, "Unknown%20reason");
            break;
        }
        
        AddOJOrderRejectRecord(timeStamp, orderID, ecnOrderID, reason, symbol, VENUE_RASH, reasonCode, account);
        break;

      case 'B': // Broken trade
        //ProcessBrokenOrderOfRASHOrder( dataBlock, type );
        break;

      default:
        TraceLog(ERROR_LEVEL, "RASH Order: Invalid order message, msgType = %c ASCII = %d\n", dataBlock.msgContent[9], dataBlock.msgContent[9] );
        break;
    }
  }
  else if( dataBlock.msgContent[0] == 'U' )
  {
    usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]) / 1000;
    sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
  
    if ((dataBlock.msgContent[1] == 'O') || (dataBlock.msgContent[1] == 'Q')) // New Order
    {
      strncpy( symbol, (char *) &dataBlock.msgContent[23], SYMBOL_LEN);
      TrimRight( symbol, strnlen(symbol, SYMBOL_LEN) );
            
      buysell = '0';
      switch (dataBlock.msgContent[16])
      {
        case 'B':
          buysell = 'B';
          break;
        case 'S':
          buysell = 'S';
          break;
        case 'T':
          buysell = 'T';
          break;
      }
      
      memcpy( fieldToken, &dataBlock.msgContent[17], 6 );
      fieldToken[6] = 0;
      shares = atoi(fieldToken);
      
      // Price
      memcpy( fieldToken, &dataBlock.msgContent[31], 10 );
      fieldToken[10] = 0;
      price = atof( fieldToken ) / 10000;
  
      // Order ID
      memcpy(fieldToken, &dataBlock.msgContent[4], 12);
      fieldToken[12] = 0;
      orderID = atoi(fieldToken);
      
      if (AddOrderIdToList(&__new, orderID) == ERROR)
      {
        return SUCCESS;
      }
      
      //TIF
      memcpy(fieldToken, &dataBlock.msgContent[41], 5);
      fieldToken[5] = 0;
  
      int tifValue = atoi(fieldToken);
      if (tifValue == 0)    //IOC
      {
        strcpy(tif, "0");
      }
      else if(tifValue > 0) //DAY
      {
        strcpy(tif, "99999");
      }
      
      //Store for re-use
      memcpy(OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].symbol, symbol, SYMBOL_LEN);
      OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].leftShares = shares;
      OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].buysell = buysell;
      OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].canceledShares = 0;
      
      //Seq ID from EAA
      memcpy(fieldToken, &dataBlock.msgContent[4], 12);
      fieldToken[12] = 0;
      int seqID = atoi(fieldToken);
      
      if (seqID < 500000) //Manual order
      {
        //Acount indicator
        memcpy(fieldToken, &dataBlock.msgContent[2], 2);
        fieldToken[2] = 0;
        if (strncmp(fieldToken, TradingAccount.RASHToken[FIRST_ACCOUNT], 2) == 0)
        {
          Manual_Order_Account_Mapping[seqID] = FIRST_ACCOUNT;
          strcpy(account, TradingAccount.RASH[FIRST_ACCOUNT]);
        }
        else if (strncmp(fieldToken, TradingAccount.RASHToken[SECOND_ACCOUNT], 2) == 0)
        {
          Manual_Order_Account_Mapping[seqID] = SECOND_ACCOUNT;
          strcpy(account, TradingAccount.RASH[SECOND_ACCOUNT]);
        }
        else if (strncmp(fieldToken, TradingAccount.RASHToken[THIRD_ACCOUNT], 2) == 0)
        {
          Manual_Order_Account_Mapping[seqID] = THIRD_ACCOUNT;
          strcpy(account, TradingAccount.RASH[THIRD_ACCOUNT]);
        }
        else
        {
          Manual_Order_Account_Mapping[seqID] = FOURTH_ACCOUNT;
          strcpy(account, TradingAccount.RASH[FOURTH_ACCOUNT]);
        }
      }
      else  //Order from TS, to take from addContent
      {
        if (dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID] == FIRST_ACCOUNT)
        {
          strcpy(account, TradingAccount.RASH[FIRST_ACCOUNT]);
        }
        else if (dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID] == SECOND_ACCOUNT)
        {
          strcpy(account, TradingAccount.RASH[SECOND_ACCOUNT]);
        }
        else if (dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID] == THIRD_ACCOUNT)
        {
          strcpy(account, TradingAccount.RASH[THIRD_ACCOUNT]);
        }
        else
        {
          strcpy(account, TradingAccount.RASH[FOURTH_ACCOUNT]);
        }
      }
      
      int isISO = 0;
      if (dataBlock.msgContent[1] == 'Q')
      {
        if (dataBlock.msgContent[140] == 'Y')
        {
          isISO = 1;
        }
      }

      visibility = dataBlock.msgContent[50]; //DISPLAY
      char routeDest[5];
      memcpy(routeDest, &dataBlock.msgContent[104], 4);
      routeDest[4] = 0;
      char capacity = dataBlock.msgContent[97];
      
      int crossID = *((int *) &dataBlock.addContent[DATABLOCK_OFFSET_CROSS_ID]);
      
      AddOJNewOrderRecord(timeStamp, symbol, VENUE_RASH, buysell, price, shares, orderID, tif, visibility, isISO, account, routeDest, capacity, crossID, GetBBReason(&dataBlock, orderID));
      FindAndProcessRawdataInOJWaitList(VENUE_RASH, orderID);
    }
    else if( dataBlock.msgContent[1] == 'X' ) // Cancel Request
    {
      // OrderId
      memcpy(fieldToken, &dataBlock.msgContent[4], 12);
      fieldToken[12] = 0;
      orderID = atoi(fieldToken);
      
      if (OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].symbol[0] == 0)
      {
        if (isFromWaitlist == NO) AddOJRawdataToWaitList(&dataBlock);
        return ERROR;
      }
      
      memcpy( fieldToken, &dataBlock.msgContent[16], 6 );
      fieldToken[6] = 0;
      shares = atoi( fieldToken );  
  
      AddOJOrderCancelRecord(timeStamp, orderID, OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].symbol, VENUE_RASH, shares);
    }
    else
      TraceLog(ERROR_LEVEL, "RASH Order: Invalid order message, msgType = %c ASCII = %d\n", dataBlock.msgContent[1], dataBlock.msgContent[1] );
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessOJRecordForNyseCCG
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessOJRecordForNyseCCG(t_DataBlock dataBlock, const int isFromWaitlist)
{
  char timeStamp[32];
  char dateTime[32];
  char symbol[32];
  char buysell = '0';
  double price = 0.0;
  int shares = 0;
  int orderID = 0;
  char tif[6];
  char visibility;
  char ecnOrderID[16];
  char reason[1024];
  char liquidity[6];
  char account[32];
  char capacity;
  char rejectType;
  int isISO;
  int crossID;
  
  GetAccountName(TYPE_NYSE_CCG, dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID], account);

  t_IntConverter myInt;

  long usec;

  t_ShortConverter myShort;

  myShort.c[1] = dataBlock.msgContent[0];
  myShort.c[0] = dataBlock.msgContent[1];
  int msgType = myShort.value;

  switch (msgType)
  {
    //-------------------------------------------
    //      New order
    //-------------------------------------------
    case 0x0041:
      // Shares
      myInt.c[3] = dataBlock.msgContent[8];
      myInt.c[2] = dataBlock.msgContent[9];
      myInt.c[1] = dataBlock.msgContent[10];
      myInt.c[0] = dataBlock.msgContent[11];
      shares = myInt.value;

      // Price and Price Scale
      myInt.c[3] = dataBlock.msgContent[16];
      myInt.c[2] = dataBlock.msgContent[17];
      myInt.c[1] = dataBlock.msgContent[18];
      myInt.c[0] = dataBlock.msgContent[19];
      price = myInt.value * 1.0 / pow(10, dataBlock.msgContent[20] - 48);

      // Symbol
      strncpy(symbol, (char *)&dataBlock.msgContent[84], SYMBOL_LEN);

      // Action
      switch (dataBlock.msgContent[33])
      {
        case '1':
          buysell = 'B';
          break;
        case '2':
          buysell = 'S';
          break;
        case '5':
          buysell = 'T';
          break;
      }

      // Order ID
      orderID = Convert_NYSECCG_Format_To_ClOrdID( &dataBlock.msgContent[64] );
      
      if (AddOrderIdToList(&__new, orderID) == ERROR)
      {
        return SUCCESS;
      }
        
      //Store to memory for re-use
      memcpy(OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].symbol, symbol, SYMBOL_LEN);
      OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].leftShares = shares;
      OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].buysell = buysell;

      // time in force
      switch (dataBlock.msgContent[35])
      {
        case '0': //DAY
          strcpy(tif, "99999");
          break;
        case '3': //IOC
          strcpy(tif, "0");
          break;
      }

      // Rule80A
      capacity = dataBlock.msgContent[36];

      // routing instruction
      isISO = (dataBlock.msgContent[37] == 'I')?1:0; //ISO Flag
      visibility = 'Y';
      
      usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]) / 1000;
      sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
      
      crossID = *((int *) &dataBlock.addContent[DATABLOCK_OFFSET_CROSS_ID]);
  
      AddOJNewOrderRecord(timeStamp, symbol, VENUE_CCG, buysell, price, shares, orderID, tif, visibility, isISO, account, NULL, capacity, crossID, GetBBReason(&dataBlock, orderID));
      FindAndProcessRawdataInOJWaitList(VENUE_CCG, orderID);
      break;

    //-------------------------------------------
    //      ACK order
    //-------------------------------------------
    case 0x0091:
    {
      // NYSE Order ID
      myInt.c[3] = dataBlock.msgContent[8];
      myInt.c[2] = dataBlock.msgContent[9];
      myInt.c[1] = dataBlock.msgContent[10];
      myInt.c[0] = dataBlock.msgContent[11];
      sprintf(ecnOrderID, "%d",  myInt.value);

      // Convert date format to CCG date format
      GetDateTimeFromEpochNsec(*((long *) &dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]), &dateTime[0]);
      
      // MM
      strncpy(Nyse_CCG_timestamp, &dateTime[5], 2);
      // DD
      strncpy(&Nyse_CCG_timestamp[2], &dateTime[8], 2);
      // YYYY
      strncpy(&Nyse_CCG_timestamp[4], dateTime, 4);

      // Order ID
      orderID = Convert_NYSECCG_Format_To_ClOrdID(&dataBlock.msgContent[36]);
      
      memcpy(symbol, OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].symbol, SYMBOL_LEN);
      if (symbol[0] == 0)
      {
        if (isFromWaitlist == NO) AddOJRawdataToWaitList(&dataBlock);
        return ERROR;
      }
      
      if (AddOrderIdToList(&__ack, orderID) == ERROR)
      {
        return SUCCESS;
      }

      capacity = 'P';
      usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]) / 1000;
      sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
      
      char exOrderId[32];
      Convert_ClOrdID_To_NYSE_Format(orderID, exOrderId);
      
      AddOJOrderAcceptedRecord(timeStamp, symbol, exOrderId, orderID, ecnOrderID, VENUE_CCG, account, capacity);
    }
      break;

    //-------------------------------------------
    //      Fill order
    //-------------------------------------------
    case 0x0081:
      // client orderID
      orderID = Convert_NYSECCG_Format_To_ClOrdID(&dataBlock.msgContent[99]);
      memcpy(symbol, OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].symbol, SYMBOL_LEN);
      if (symbol[0] == 0)
      {
        if (isFromWaitlist == NO) AddOJRawdataToWaitList(&dataBlock);
        return ERROR;
      }
      
      // NYSE Order ID
      myInt.c[3] = dataBlock.msgContent[8];
      myInt.c[2] = dataBlock.msgContent[9];
      myInt.c[1] = dataBlock.msgContent[10];
      myInt.c[0] = dataBlock.msgContent[11];
      sprintf(ecnOrderID, "%d",  myInt.value);

      // Shares
      myInt.c[3] = dataBlock.msgContent[20];
      myInt.c[2] = dataBlock.msgContent[21];
      myInt.c[1] = dataBlock.msgContent[22];
      myInt.c[0] = dataBlock.msgContent[23];
      shares = myInt.value;

      // Price and price scale
      myInt.c[3] = dataBlock.msgContent[24];
      myInt.c[2] = dataBlock.msgContent[25];
      myInt.c[1] = dataBlock.msgContent[26];
      myInt.c[0] = dataBlock.msgContent[27];
      price = myInt.value * 1.0 / pow( 10, dataBlock.msgContent[28] - 48 );

      // Action
      switch (dataBlock.msgContent[29])
      {
        case '1':
          buysell = 'B';
          break;
        case '2':
          buysell = 'S';
          break;
        case '5':
          buysell = 'T';
          break;
        default:
          break;
      }

      // Billing indicator
      if (dataBlock.msgContent[30] == 0)
      {
        liquidity[0] = ' ';
      }
      else
      {
        liquidity[0] = dataBlock.msgContent[30];
      }
      liquidity[1] = 0;
      
      char _execVenue;
      _execVenue = (dataBlock.msgContent[31] != 0) ? dataBlock.msgContent[31] : ' ';

      // Execution ID
      char execID[20];
      memcpy(execID, &dataBlock.msgContent[69], 10);
      execID[10] = 0;
      
      long _execID = atol(execID);
      if (AddExecDataToList(&__exec, orderID, _execID) == ERROR)
      {
        return SUCCESS;
      }

      OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].leftShares -= shares;
      sprintf(execID, "%lu", _execID);
      
      usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]) / 1000;
      sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
      
      char exOrderId[32];
      Convert_ClOrdID_To_NYSE_Format(orderID, exOrderId);
      
      AddOJOrderFillRecord(timeStamp, symbol, exOrderId, VENUE_CCG, buysell, price, shares, orderID, liquidity,
                ecnOrderID, execID, "1", (OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].leftShares <= 0)?1:0, account, _execVenue);
      break;

    //-------------------------------------------
    //      Canceled order
    //-------------------------------------------
    case 0x00D1:
      // Order ID
      orderID = Convert_NYSECCG_Format_To_ClOrdID(&dataBlock.msgContent[37]);
      
      memcpy(symbol, OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].symbol, SYMBOL_LEN);
      if (symbol[0] == 0)
      {
        if (isFromWaitlist == NO) AddOJRawdataToWaitList(&dataBlock);
        return ERROR;
      }
      
      if (AddOrderIdToList(&__cancel, orderID) == ERROR)
      {
        return SUCCESS;
      }

      // NYSE Order ID
      myInt.c[3] = dataBlock.msgContent[8];
      myInt.c[2] = dataBlock.msgContent[9];
      myInt.c[1] = dataBlock.msgContent[10];
      myInt.c[0] = dataBlock.msgContent[11];
      sprintf(ecnOrderID, "%d",  myInt.value);

      // Information code
      char _reasonCode = dataBlock.msgContent[16];
      if (_reasonCode == 0)
      {
        strcpy(reason, "user-initiated");
      }
      else if(_reasonCode == 1)
      {
        strcpy(reason, "exchange-initiated-Unsolicited%20UROUT");
      }
      else if(_reasonCode == 2)
      {
        strcpy(reason, "exchange-initiated-Cancel%20On%20Disconnect");
      }
      else if(_reasonCode == 3)
      {
        strcpy(reason, "exchange-initiated-Done%20for%20Day");
      }
      else
      {
        sprintf(reason, "unknown%%20reason,%%20reason%%20code%%20%%3d%%20%d", _reasonCode);
      }
      usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]) / 1000;
      sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
      AddOJOrderCancelResponseRecord(timeStamp, symbol, orderID, ecnOrderID, reason, VENUE_CCG, OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].leftShares, 1, account);
      break;

    //-------------------------------------------
    //    Rejected order/Rejected Cancel
    //-------------------------------------------
    case 0x00F1:
      //Reject type: '1' - Order rejected, '2' - Cancel Rejected
      rejectType = dataBlock.msgContent[18];
      if (rejectType == '2') 
      {
        break;
      }
      
      // Order ID
      orderID = Convert_NYSECCG_Format_To_ClOrdID(&dataBlock.msgContent[39]);

      memcpy(symbol, OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].symbol, SYMBOL_LEN);
      if (symbol[0] == 0)
      {
        if (isFromWaitlist == NO) AddOJRawdataToWaitList(&dataBlock);
        return ERROR;
      }

      if (AddOrderIdToList(&__reject, orderID) == ERROR)
      {
        return SUCCESS;
      }
      
      // NYSE Order ID
      myInt.c[3] = dataBlock.msgContent[8];
      myInt.c[2] = dataBlock.msgContent[9];
      myInt.c[1] = dataBlock.msgContent[10];
      myInt.c[0] = dataBlock.msgContent[11];
      sprintf(ecnOrderID, "%d",  myInt.value);

      //reason
      ConvertSpecialCharacter((char *)&dataBlock.msgContent[73], reason);

      char reasonCode[5];
      myShort.c[1] = dataBlock.msgContent[16];
      myShort.c[0] = dataBlock.msgContent[17];
      sprintf(reasonCode, "%d", myShort.value);
      usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]) / 1000;
      sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
      AddOJOrderRejectRecord(timeStamp, orderID, ecnOrderID, reason, symbol, VENUE_CCG, reasonCode, account);
      break;

    //-------------------------------------------
    //      Cancel request order
    //-------------------------------------------
    case 0x0061:
      // Order ID
      orderID = Convert_NYSECCG_Format_To_ClOrdID(&dataBlock.msgContent[73]);
      
      memcpy(symbol, OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].symbol, SYMBOL_LEN);
      if (symbol[0] == 0)
      {
        if (isFromWaitlist == NO) AddOJRawdataToWaitList(&dataBlock);
        return ERROR;
      }
      usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]) / 1000;
      sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
      AddOJOrderCancelRecord(timeStamp, orderID, symbol, VENUE_CCG, OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].leftShares);
      
      break;

    default:
      //TraceLog(DEBUG_LEVEL,  "ARCA DIRECT Order: Invalid order message, msgType = %c ASCII = %d\n", dataBlock.msgContent[0], dataBlock.msgContent[0] );
      break;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessOJRecordForBatsBOE
- Description:    
- Usage:      
****************************************************************************/
int ProcessOJRecordForBatsBOE(t_DataBlock dataBlock, const int isFromWaitlist, const int venueType)
{
  char timeStamp[32];
  char symbol[32];
  char buysell = '0';
  double price = 0.0;
  int shares = 0;
  int orderID = 0;
  char tif[6];
  char visibility;
  char ecnOrderID[32];
  char reason[1024];
  char liquidity[6];
  char account[32];
  char fieldClOrderID[21];
  char execInst;
  char routInst;
  int isISO;
  int offset;
  int crossID;
  union t_BitField bitField;

  int _oecType = venueType + TYPE_BASE;
  GetAccountName(_oecType, dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID], account);

  long usec;

  int msgType = dataBlock.msgContent[4];

  switch (msgType)
  {
    //-------------------------------------------
    //      New order
    //-------------------------------------------
    case 0x04:
    {
      // Order id
      strncpy(fieldClOrderID, (char *)&dataBlock.msgContent[10], 20);
      fieldClOrderID[20] = 0;
      orderID = atoi(fieldClOrderID);

      if (AddOrderIdToList(&__new, orderID) == ERROR)
      {
        return SUCCESS;
      }
        
      // Action
      switch (dataBlock.msgContent[30])
      {
        case '1':
          buysell = 'B';
          break;
        case '2':
          buysell = 'S';
          break;
        case '5':
          buysell = 'T';
          break;
      }
      OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].buysell = buysell;

      // Shares
      shares = GetIntNumber(&dataBlock.msgContent[31]);
      OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].leftShares = shares;

      /*----------------------------------------
       * Process optional fields
       * Optional fields start from offset 41...
       *--------------------------------------*/
      offset = STARTING_OFFSET_OF_NEW_ORDER;

      /*------------------------
       *  New order bitField 1
       *-----------------------*/
      bitField.value = dataBlock.msgContent[35];

      if (bitField.bit.bit1 == 1) offset += CLEARING_FIRM_LEN;

      if (bitField.bit.bit2 == 1) offset += CLEARING_ACC_LEN;

      // price
      if (bitField.bit.bit3 == 1)
      {
        price = GetLongNumber((char *)&dataBlock.msgContent[offset]) * 1.00 / 10000;
        offset += PRICE_LEN;
      }

      if (bitField.bit.bit4 == 1)
      {
        execInst = dataBlock.msgContent[offset];
        offset += EXEC_INST_LEN;
      }
      else
      {
        execInst = 0;
      }

      if (bitField.bit.bit5 == 1) offset += ORDER_TYPE_LEN;

      // time in force
      if (bitField.bit.bit6 == 1)
      {
        char _tif = dataBlock.msgContent[offset];
        switch(_tif)
        {
          case '0': //DAY
            strcpy(tif, "99999");
            break;
          case '3': //IOC
            strcpy(tif, "0");
            break;
          default:
            strcpy(tif, "%20");
            break;
        }
        offset += TIME_IN_FORCE_LEN;
      }

      if (bitField.bit.bit7 == 1) offset += MIN_QTY_LEN;

      if (bitField.bit.bit8 == 1) offset += MAX_FLOOR_LEN;

      /*------------------------
       *  New order bitField 2
       *-----------------------*/
      bitField.value = dataBlock.msgContent[36];

      // Stock symbol
      if (bitField.bit.bit1 == 1)
      {
        strncpy( OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].symbol, ( char * )&dataBlock.msgContent[offset], SYMBOL_LEN );
        offset += SYMBOL_LEN;
      }

      if (bitField.bit.bit2 == 1) offset += SYMBOL_SFX_LEN;

      // Rule80A
      if (bitField.bit.bit7 == 1)
      {
        OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].capacity = dataBlock.msgContent[offset];
        offset += CAPACITY_LEN;
      }

      // Routing instruction
      if (bitField.bit.bit8 == 1)
      {
        routInst = dataBlock.msgContent[offset];
        offset += ROUT_INST_LEN;
      }
      else
      {
        routInst = 0;
      }

      if (routInst == 'B' && execInst == 'f')
      {
        isISO = 1;
      }
      else isISO = 0;

      /*------------------------
       *  New order bitField 3
       *-----------------------*/
      bitField.value = dataBlock.msgContent[37];

      if (bitField.bit.bit1 == 1) offset += ACCOUNT_LEN;

      if (bitField.bit.bit2 == 1)
      {
        if (dataBlock.msgContent[offset] == ' '|| dataBlock.msgContent[offset] == 'I')
        {
          visibility = 'N';
        }
        else
        {
          visibility = 'Y';
        }
        
        offset += DISPLAY_INDICATOR_LEN;
      }
      else
      {
        visibility = 'N';
      }
      
      usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]) / 1000;
      sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
      
      crossID = *((int *) &dataBlock.addContent[DATABLOCK_OFFSET_CROSS_ID]);
      
      AddOJNewOrderRecord(timeStamp, OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].symbol, venueType, buysell, price, shares, orderID, tif, visibility, isISO, account, NULL, OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].capacity, crossID, GetBBReason(&dataBlock, orderID));
      FindAndProcessRawdataInOJWaitList(venueType, orderID);
    }
      break;

    //-------------------------------------------
    //      ACK order
    //-------------------------------------------
    case 0x0A:
    {
      // Order ID
      strncpy(fieldClOrderID, (char *)&dataBlock.msgContent[18], 20);
      fieldClOrderID[20] = 0;
      orderID = atoi(fieldClOrderID);
        
      // BZX BOE Order ID
      sprintf(ecnOrderID, "%lu", GetLongNumber((char *)&dataBlock.msgContent[38]));
      OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].batsExOrderID = GetLongNumber((char *)&dataBlock.msgContent[38]);

      //Now parse symbol and capacity
      union t_BitField bitField;
      int offset = STARTING_OFFSET_OF_ACK_ORDER;
      bitField.value = dataBlock.msgContent[46];
      if (bitField.bit.bit1 == 1) //Side
        offset += 1;
        
      if (bitField.bit.bit2 == 1) // peg difference
        offset += 8;
        
      if (bitField.bit.bit3 == 1) // price
        offset += 8;
        
      if (bitField.bit.bit4 == 1) // Exec instruction
        offset += 1;
          
      if (bitField.bit.bit5 == 1) // order type
        offset += 1;
          
      if (bitField.bit.bit6 == 1) // tif
        offset += 1;
          
      if (bitField.bit.bit7 == 1) // min qty
        offset += 4;
          
      if (bitField.bit.bit8 == 1) // Max remove pct
        offset += 1;
        
      char symbol[SYMBOL_LEN];
      bitField.value = dataBlock.msgContent[47];
      if (bitField.bit.bit1 == 1) //symbol
      {
        strncpy(symbol, (char *)&dataBlock.msgContent[offset], 8);
        offset += 8;
      }
      else
      {
        strncpy(symbol, OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].symbol, SYMBOL_LEN);
        if (symbol[0] == 0)
        {
          if (isFromWaitlist == NO) AddOJRawdataToWaitList(&dataBlock);
          return ERROR;
        }
      }
        
      if (bitField.bit.bit2 == 1) //Symbol Sfx
      {
        char suffix[8];
        strncpy(suffix, (char *)&dataBlock.msgContent[offset], 8);
          
        strcat(symbol, suffix);
        offset += 8;
      }
        
      if (bitField.bit.bit3 == 1) // price
        offset += 8;
        
      // bit 4,5,6 Preserved
        
      char capacity;
      if (bitField.bit.bit7 == 1) // capacity
      {
        capacity = dataBlock.msgContent[offset];
      }
      else
      {
        if (OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].symbol[0] == 0)
        {
          if (isFromWaitlist == NO) AddOJRawdataToWaitList(&dataBlock);
          return ERROR;
        }
        capacity = OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].capacity;
      }
        
      if (AddOrderIdToList(&__ack, orderID) == ERROR)
      {
        return SUCCESS;
      }

      usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]) / 1000;
      sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
      
      char exOrderId[32];
      sprintf(exOrderId, "%d", orderID);
      AddOJOrderAcceptedRecord(timeStamp, symbol, exOrderId, orderID, ecnOrderID, venueType, account, capacity);
    }
      break;

    //-------------------------------------------
    //      Fill order
    //-------------------------------------------
    case 0x11:
    {
      // client orderID
      strncpy(fieldClOrderID, (char *)&dataBlock.msgContent[18], 20);
      fieldClOrderID[20] = 0;
      orderID = atoi(fieldClOrderID);
      
      char exOrderId[32];
      sprintf(exOrderId, "%d", orderID);
      
      if (OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].symbol[0] == 0)
      {
        if (isFromWaitlist == NO) AddOJRawdataToWaitList(&dataBlock);
        return ERROR;
      }
      
      // BZX BOE Order ID
      sprintf(ecnOrderID, "%lu",  OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].batsExOrderID);

      // Execution ID
      char execID[32] = "";
      long executionID = GetLongNumber((char *)&dataBlock.msgContent[38]);
      sprintf(execID, "%lu", executionID);

      if (AddExecDataToList(&__exec, orderID, executionID) == ERROR)
      {
        return SUCCESS;
      }
        
      // Shares
      shares = GetIntNumber(&dataBlock.msgContent[46]);

      price = GetLongNumber((char *)&dataBlock.msgContent[50]) * 1.00 / 10000;

      // base liquidity indicator
      liquidity[0] = dataBlock.msgContent[62];
      liquidity[1] = 0;

      /*----------------------------------------
       * Process optional fields
       * Optional fields start from offset 84...
       *--------------------------------------*/
      offset = STARTING_OFFSET_OF_FILL_ORDER;

      /*------------------------
       *  Accepted order bitField 1
       *-----------------------*/
      bitField.value = dataBlock.msgContent[76];

      if (bitField.bit.bit1 == 1)
      {
        switch( dataBlock.msgContent[offset] )
        {
          case '1':
            buysell = 'B';
            break;
          case '2':
            buysell = 'S';
            break;
          case '5':
            buysell = 'T';
            break;
          default:
            TraceLog(WARN_LEVEL, "%s: Unexpected buysell = %c\n", GetOECName(_oecType), dataBlock.msgContent[84]);
        }
        offset += SIDE_LEN;
      }

      strncpy(symbol, OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].symbol, SYMBOL_LEN);

      OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].leftShares -= shares;
      usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]) / 1000;
      sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
      
      AddOJOrderFillRecord(timeStamp, symbol, exOrderId, venueType, buysell, price, shares, orderID, liquidity,
                ecnOrderID, execID, "1", (OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].leftShares <= 0)?1:0, account, 0);
    }
      break;

    //-------------------------------------------
    //      Canceled order
    //-------------------------------------------
    case 0x0F:
    {
      // client orderid;
      strncpy(fieldClOrderID, (char *)&dataBlock.msgContent[18], 20);
      fieldClOrderID[20] = 0;
      orderID = atoi(fieldClOrderID);

      if (OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].symbol[0] == 0)
      {
        if (isFromWaitlist == NO) AddOJRawdataToWaitList(&dataBlock);
        return ERROR;
      }
      
      if (AddOrderIdToList(&__cancel, orderID) == ERROR)
      {
        return SUCCESS;
      }

      sprintf(ecnOrderID, "%lu",  OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].batsExOrderID);

      // Cancel reason, type: Text, size: 1 byte
      char _reasonCode = dataBlock.msgContent[38];
      switch(_reasonCode)
      {
        case 'A':
          strcpy(reason, "Admin");
          break;
        case 'B':
          strcpy(reason, "Duplicate%20ClOrdID");
          break;
        case 'H':
          strcpy(reason, "Halted");
          break;
        case 'L':
          strcpy(reason, "Order%20would%20look%20or%20cross%20NBBO");
          break;
        case 'N':
          strcpy(reason, "Ran%20out%20of%20liquidity%20to%20execute%20against");
          break;
        case 'R':
          strcpy(reason, "Routing%20unavailable");
          break;
        case 'S':
          strcpy(reason, "Short%20sell%20price%20violation");
          break;
        case 'T':
          strcpy(reason, "Fill%20would%20trade-through%20NBBO");
          break;
        case 'U':
          strcpy(reason, "User%20requested");
          break;
        case 'V':
          strcpy(reason, "Would%20wash");
          break;
        case 'W':
          strcpy(reason, "Add%20liquidity%20only%20order%20would%20remove");
          break;
        case 'X':
          strcpy(reason, "Order%20expired");
          break;
        case 'Z':
          strcpy(reason, "Unforeseen%20reason");
          break;
        case 'u':
          strcpy(reason, "User%20requested%20(delayed%20due%20to%20order%20being%20route%20pending)");
          break;
        case 'x':
          strcpy(reason, "Crossed%20market");
          break;
        default:
          sprintf(reason, "uknown%%20reason,%%20reason%%20code%%20%%3d%%20%c", _reasonCode);
          break;
      }

      /*----------------------------------------
       * Process optional fields
       * Optional fields start from offset 47...
       *--------------------------------------*/
      offset = STARTING_OFFSET_OF_CANCELED_ORDER;

      /*------------------------
       *  Canceled order bitField 1
       *-----------------------*/
      bitField.value = dataBlock.msgContent[39];

      // Side
      if (bitField.bit.bit1 == 1) offset += SIDE_LEN;

      /*------------------------
       *  Canceled order bitField 2
       *-----------------------*/
      bitField.value = dataBlock.msgContent[40];

      // Symbol
      if (bitField.bit.bit1 == 1) offset += SYMBOL_LEN;

      /*------------------------
       *  Canceled order bitField 3
       *-----------------------*/
      bitField.value = dataBlock.msgContent[41];

      if (bitField.bit.bit1 == 1) offset += ACCOUNT_LEN;
      if (bitField.bit.bit2 == 1) offset += CLEARING_FIRM_LEN;
      if (bitField.bit.bit3 == 1) offset += CLEARING_ACC_LEN;
      if (bitField.bit.bit4 == 1) offset += DISPLAY_INDICATOR_LEN;
      if (bitField.bit.bit5 == 1) offset += MAX_FLOOR_LEN;
      if (bitField.bit.bit6 == 1) offset += DISCRETION_AMOUNT_LEN;
      if (bitField.bit.bit7 == 1) offset += ORDER_QTY_LEN;
      if (bitField.bit.bit8 == 1) offset += PREVENT_MEMBER_MATCH_LEN;

      /*------------------------
       *  Canceled order bitField 4, 6, 7: use in the future
       *-----------------------*/

      /*------------------------
       *  Canceled order bitField 5
       *-----------------------*/
      bitField.value = dataBlock.msgContent[43];

      // Original client order id
      char fieldOrgClOrderId[21];
      int orgClOrderId = 0;
      if (bitField.bit.bit1 == 1)
      {
        strncpy(fieldOrgClOrderId, (char *)&dataBlock.msgContent[offset], 20);
        fieldOrgClOrderId[20] = 0;
        orgClOrderId = atoi(fieldOrgClOrderId);
      }

      if (orgClOrderId > 0)
        orderID = orgClOrderId;

      // Order ID: boeOJOrder.orgClOrderID
      usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]) / 1000;
      sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
        
      AddOJOrderCancelResponseRecord(timeStamp, OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].symbol, orderID, ecnOrderID, reason, venueType, OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].leftShares, 1, account);
    }
      break;

    //-------------------------------------------
    //      Rejected order
    //-------------------------------------------
    case 0x0B:
    {
      // Order ID
      strncpy(fieldClOrderID, (char *)&dataBlock.msgContent[18], 20);
      fieldClOrderID[20] = 0;
      orderID = atoi(fieldClOrderID);

      if (OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].symbol[0] == 0)
      {
        if (isFromWaitlist == NO) AddOJRawdataToWaitList(&dataBlock);
        return ERROR;
      }

      if (AddOrderIdToList(&__reject, orderID) == ERROR)
      {
        return SUCCESS;
      }
      
      // BZX BOE Order ID
      strcpy(ecnOrderID, "''");

      char reasonCode[2];
      reasonCode[0] = dataBlock.msgContent[38];
      reasonCode[1] = 0;

      //reason
      char rejectText[61];
      memcpy(rejectText, &dataBlock.msgContent[39], 60);
      rejectText[60] = 0;

      ConvertSpecialCharacter(rejectText, reason);
      usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]) / 1000;
      sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
      AddOJOrderRejectRecord(timeStamp, orderID, ecnOrderID, reason, OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].symbol, venueType, reasonCode, account);
    }
      break;

    //-------------------------------------------
    //      Cancel request order
    //-------------------------------------------
    case 0x05:
    {
      // original client Order ID
      strncpy(fieldClOrderID, (char *)&dataBlock.msgContent[10], 20);
      fieldClOrderID[20] = 0;
      orderID = atoi(fieldClOrderID);

      if (OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].symbol[0] == 0)
      {
        if (isFromWaitlist == NO) AddOJRawdataToWaitList(&dataBlock);
        return ERROR;
      }
      
      usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]) / 1000;
      sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
      AddOJOrderCancelRecord(timeStamp, orderID, OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].symbol, venueType, OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].leftShares);
    }
      break;

    default:
      break;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessOJRecordForEdge
- Description:    
- Usage:      
****************************************************************************/
int ProcessOJRecordForEdge(t_DataBlock dataBlock, const int isFromWaitlist, const int venueType)
{
  char timeStamp[32];
  char symbol[32];
  char buysell = '0';
  double price = 0.0;
  int shares = 0;
  int orderID = 0;
  char tif[6];
  char visibility;
  char ecnOrderID[32];
  char reason[1024];
  char fieldToken[15];
  char account[32];
  unsigned long _ecnOrderId;
  int _serverId, tsId;
  
  int _oecType = venueType + TYPE_BASE;
  GetAccountName(_oecType, dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID], account);
  
  char msgType = dataBlock.msgContent[3];
  if( dataBlock.msgContent[2] == 'S' )
  {
    long usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]) / 1000;
    sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
    
    switch ( msgType )
    {
      case 'A':   // Accepted order: 
      case 'P':   // P: Extended format, A: Short format
      {
        // Order ID (including first two digits of account)
        memcpy(fieldToken, &dataBlock.msgContent[14], 12);
        fieldToken[12] = 0;
        orderID = atoi(fieldToken);
        
        char exOrderId[32];
        exOrderId[0] = dataBlock.msgContent[12];
        exOrderId[1] = dataBlock.msgContent[13];
        sprintf(&exOrderId[2], "%d", orderID);
  
        if (msgType == 'A') //Short format, do not have suffix
        {
          strncpy(symbol, (char *) &dataBlock.msgContent[31], 6);
          symbol[6] = 0;
          TrimRight(symbol, strlen(symbol));
        }
        else        //Extended format, must consider symbol suffix
        {
          /* We must take original symbol, so we must ensure that
            we have processed new order extended format rawdata*/
          
          if (OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].symbol[0] == 0)
          {
            if (isFromWaitlist == NO) AddOJRawdataToWaitList(&dataBlock);
            return ERROR;
          }
          
          memcpy(symbol, OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].symbol, SYMBOL_LEN);
        }
        
        if (AddOrderIdToList(&__ack, orderID) == ERROR)
        {
          return SUCCESS;
        }

        //Order reference number
        _ecnOrderId = GetLongNumberBigEndian( (char *)&dataBlock.msgContent[45] );
        
        tsId = (orderID / 1000000) - 1;
        _serverId = (tsId == -1) ? MAX_TRADE_SERVER_CONNECTIONS : tsId;
        ExchangeOrderIdMapping[_serverId][orderID % MAX_ORDER_PLACEMENT] = _ecnOrderId;
        
        char capacity = dataBlock.msgContent[53];
        sprintf(ecnOrderID, "%lu", _ecnOrderId);
        
        AddOJOrderAcceptedRecord(timeStamp, symbol, exOrderId, orderID, ecnOrderID, venueType, account, capacity);
      }
        break;
      
      case 'C':   // Canceled order
      {
        // OrderId
        memcpy(fieldToken, &dataBlock.msgContent[14], 12);
        fieldToken[12] = 0;
        orderID = atoi(fieldToken);
        
        if (OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].symbol[0] == 0)
        {
          if (isFromWaitlist == NO) AddOJRawdataToWaitList(&dataBlock);
          return ERROR;
        }
        
        if (AddOrderIdToList(&__cancel, orderID) == ERROR)
        {
          return SUCCESS;
        }
        
        // Shares
        shares = GetIntNumberBigEndian(&dataBlock.msgContent[26]);
        
        tsId = (orderID / 1000000) - 1;
        _serverId = (tsId == -1) ? MAX_TRADE_SERVER_CONNECTIONS : tsId;
        if (ExchangeOrderIdMapping[_serverId][orderID % MAX_ORDER_PLACEMENT] != 0)
        {
          sprintf(ecnOrderID, "%lu", ExchangeOrderIdMapping[_serverId][orderID % MAX_ORDER_PLACEMENT]);
        }
        else
        {
          TraceLog(ERROR_LEVEL, "%s - Canceled: Could not get ExOrderId, orderID: %d\n", __func__, orderID);
          strcpy(ecnOrderID, "''"); //There is no Order Reference Number in this message type
        }
        
        switch (dataBlock.msgContent[30])
        {
          case 'U':
            strcpy(reason, "User%20requested%20cancel.%20Sent%20in%20response%20to%20a%20Cancel%20Request%20Message");
            break;
          case 'I':
            strcpy(reason, "Immediate%20or%20Cancel%20Order");
            break;
          case 'T':
            strcpy(reason, "Timeout.%20The%20Time%20In%20Force%20for%20this%20order%20has%20expired");
            break;
          case 'S':
            strcpy(reason, "This%20order%20was%20manually%20canceled%20or%20reduced%20by%20Direct%20Edge.");
            break;
          case 'D':
            strcpy(reason, "This%20order%20cannot%20be%20executed%20because%20of%20a%20regulatory%20restriction%20(e.g.:%20trade%20through%20restrictions)");
            break;
          case 'A':
            strcpy(reason, "This%20order%20cannot%20be%20posted%20because%20it%20will%20result%20in%20a%20locked%20or%20crossed%20market.");
            break;
          case 'B':
            strcpy(reason, "Order%20was%20canceled%20due%20to%20Anti-Internalization%20settings.");
            break;
          default:
            sprintf(reason, "Unknown%%20reason,%%20reason%%20code%%20%%3d%%20%c", dataBlock.msgContent[30]);
            break;
        }
        
        AddOJOrderCancelResponseRecord(timeStamp, OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].symbol, 
                    orderID, ecnOrderID, reason, venueType, shares, 1, account);
      }
        break;
      
      case 'E':   // Executed order
      {
        // Shares
        shares = GetIntNumberBigEndian( &dataBlock.msgContent[26] );
        
        // OrderId
        memcpy( fieldToken, &dataBlock.msgContent[14], 12);
        fieldToken[12] = 0;
        orderID = atoi( fieldToken );
        
        char exOrderId[32];
        exOrderId[0] = dataBlock.msgContent[12];
        exOrderId[1] = dataBlock.msgContent[13];
        sprintf(&exOrderId[2], "%d", orderID);
        
        if (OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].symbol[0] == 0)
        {
          if (isFromWaitlist == NO) AddOJRawdataToWaitList(&dataBlock);
          return ERROR;
        }
        
        tsId = (orderID / 1000000) - 1;
        _serverId = (tsId == -1) ? MAX_TRADE_SERVER_CONNECTIONS : tsId;
        if (ExchangeOrderIdMapping[_serverId][orderID % MAX_ORDER_PLACEMENT] != 0)
        {
          sprintf(ecnOrderID, "%lu", ExchangeOrderIdMapping[_serverId][orderID % MAX_ORDER_PLACEMENT]);
        }
        else
        {
          TraceLog(ERROR_LEVEL, "%s - Executed: Could not get ExOrderId, orderID: %d\n", __func__, orderID);
          strcpy(ecnOrderID, "''"); //There is no Order Reference Number in this message type
        }
        
        //MatchNo
        char execID[32];
        long _execID = GetLongNumberBigEndian( (char *)&dataBlock.msgContent[39] );
        sprintf(execID, "%lu", _execID);
  
        if (AddExecDataToList(&__exec, orderID, _execID) == ERROR)
        {
          return SUCCESS;
        }

        char fieldLiquidity[6];
        memcpy(fieldLiquidity, &dataBlock.msgContent[34], 5);
        fieldLiquidity[5] = 0;
        TrimRight(fieldLiquidity, strnlen(fieldLiquidity, 5));
        
        price = GetIntNumberBigEndian( &dataBlock.msgContent[30] ) / 100000.00;
        
        OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].leftShares -= shares;
        
        AddOJOrderFillRecord(timeStamp, OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].symbol, exOrderId, venueType, OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].buysell, price,
              shares, orderID, fieldLiquidity,
              ecnOrderID, execID, "1", (OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].leftShares <= 0)?1:0, account, 0);
      }
        break;

      case 'J': // Rejected order
      {
        // OrderId
        memcpy( fieldToken, &dataBlock.msgContent[14], 12);
        fieldToken[12] = 0;
        orderID = atoi(fieldToken);
        
        if (OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].symbol[0] == 0)
        {
          if (isFromWaitlist == NO) AddOJRawdataToWaitList(&dataBlock);
          return ERROR;
        }
        
        if (AddOrderIdToList(&__reject, orderID) == ERROR)
        {
          return SUCCESS;
        }
        
        strcpy(ecnOrderID, "''"); //There is no Order Reference Number in this message type
        
        memcpy(symbol, OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].symbol, SYMBOL_LEN);
        
        char reasonCode[2];
        reasonCode[0] = dataBlock.msgContent[26];
        reasonCode[1] = 0;
  
        switch (reasonCode[0])
        {
          case 'A':
            strcpy(reason, "Order%20characteristics%20not%20supported%20in%20current%20trading%20session");
            break;
          case 'B':
            strcpy(reason, "Number%20of%20orders%20in%20the%20bulk%20order%20message%20exceeded%20threshold");
            break;
          case 'C':
            strcpy(reason, "Exchange%20closed");
            break;
          case 'D':
            strcpy(reason, "Invalid%20Display%20Type");
            break;
          case 'E':
            strcpy(reason, "Exchange%20option");
            break;
          case 'F':
            strcpy(reason, "Halted");
            break;
          case 'H':
            strcpy(reason, "Cannot%20execute%20in%20current%20trading%20state");
            break;
          case 'I':
            strcpy(reason, "Order%20Not%20Found");
            break;
          case 'L':
            strcpy(reason, "Firm%20not%20authorized%20for%20clearing%20(invalid%20firm)");
            break;
          case 'O':
            strcpy(reason, "Other");
            break;
          case 'P':
            strcpy(reason, "Order%20already%20in%20a%20pending%20cancel%20or%20replace%20state");
            break;
          case 'Q':
            strcpy(reason, "Invalid%20quantity");
            break;
          case 'R':
            strcpy(reason, "Risk%20Control%20Reject");
            break;
          case 'S':
            strcpy(reason, "Invalid%20stock");
            break;
          case 'T':
            strcpy(reason, "Test%20Mode");
            break;
          case 'U':
            strcpy(reason, "Order%20has%20an%20invalid%20or%20unsupported%20characteristic");
            break;
          case 'V':
            strcpy(reason, "Order%20is%20rejected%20because%20maximum%20order%20rate%20is%20exceeded.");
            break;
          case 'X':
            strcpy(reason, "Invalid%20price");
            break;
          default:
            strcpy(reason, "Unknown%20reason");
            break;
        }
        
        AddOJOrderRejectRecord(timeStamp, orderID, ecnOrderID, reason, symbol, venueType, reasonCode, account);
      }
        break;

      case 'B': // Broken trade
        break;
        
      case 'S': // System Event
        break;

      default:
        TraceLog(DEBUG_LEVEL, "%s Order: Invalid order message in sequenced packet, msgType = %c ASCII = %d\n", GetOECName(_oecType), msgType, msgType );
        break;
    }
  }
  else if( dataBlock.msgContent[2] == 'U' )
  {
    long usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]) / 1000;
    sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
    
    // New Order
    // O: New order (short format), N: New order (extended format)
    if ((msgType == 'N') || (msgType == 'O'))
    {
      if (msgType == 'O') //Short format, do not have suffix
      {
        strncpy( symbol, (char *) &dataBlock.msgContent[23], 6);
        symbol[6] = 0;
        TrimRight(symbol, strlen(symbol));
      }
      else  //Extended format, must consider symbol suffix
      {
        //Note: TS already saved original symbol for us, so just take it
        
        strncpy( symbol, (char *) &dataBlock.msgContent[67], SYMBOL_LEN);
      }
            
      buysell = '0';
      switch (dataBlock.msgContent[18])
      {
        case 'B':
          buysell = 'B';
          break;
        case 'S':
          buysell = 'S';
          break;
        case 'T':
          buysell = 'T';
          break;
      }
      
      shares = GetIntNumberBigEndian(&dataBlock.msgContent[19]);
      
      // Price
      price = GetIntNumberBigEndian(&dataBlock.msgContent[29]) / 100000.00;
  
      // Order ID
      memcpy(fieldToken, &dataBlock.msgContent[6], 12);
      fieldToken[12] = 0;
      orderID = atoi(fieldToken);
      
      if (AddOrderIdToList(&__new, orderID) == ERROR)
      {
        return SUCCESS;
      }
        
      //TIF
      int tifValue = dataBlock.msgContent[33];
      if (tifValue == 0)    //IOC
      {
        strcpy(tif, "0");
      }
      else if(tifValue > 0) //DAY
      {
        strcpy(tif, "99999");
      }
      
      //Store for re-use
      memcpy(OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].symbol, symbol, SYMBOL_LEN);
      OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].leftShares = shares;
      OJOpenOrder[orderID % MAX_OJ_OPEN_ORDER].buysell = buysell;
            
      //DISPLAY
      visibility = dataBlock.msgContent[34]; 
      
      char capacity = dataBlock.msgContent[37];
      
      char routeOutEligibility = dataBlock.msgContent[38];
      
      int isISO = 0;
      if (dataBlock.msgContent[39] == 'Y')
      {
        isISO = 1;
      }
      
      int crossID = *((int *) &dataBlock.addContent[DATABLOCK_OFFSET_CROSS_ID]);
      
      // Process with New Order extended format
      if (msgType == 'N')
      {
        char *routingInfo = NULL;
        if (routeOutEligibility != 'N' && routeOutEligibility != 'P')
        {
          short routeStrategy = GetShortNumberBigEndian(&dataBlock.msgContent[41]);
          char routeDest[5];
          switch (routeStrategy)
          {
            case 1:
              memcpy(routeDest, "ROUT", 4);
              break;
            case 2:
              memcpy(routeDest, "ROUD", 4);
              break;
            case 3:
              memcpy(routeDest, "ROUE", 4);
              break;
            case 4:
              memcpy(routeDest, "ROUX", 4);
              break;
            case 5:
              memcpy(routeDest, "ROUZ", 4);
              break;
            case 6:
              memcpy(routeDest, "ROUQ", 4);
              break;
            case 7:
              memcpy(routeDest, "RDOT", 4);
              break;
            case 8:
              memcpy(routeDest, "RDOX", 4);
              break;
            case 9:
              memcpy(routeDest, "ROPA", 4);
              break;
            case 10:
              memcpy(routeDest, "ROBA", 4);
              break;
            case 11:
              memcpy(routeDest, "ROBX", 4);
              break;
            case 12:
              memcpy(routeDest, "INET", 4);
              break;
            case 13:
              memcpy(routeDest, "IOCT", 4);
              break;
            case 14:
              memcpy(routeDest, "IOCX", 4);
              break;
            case 15:
              memcpy(routeDest, "ISAM", 4);
              break;
            case 16:
              memcpy(routeDest, "ISPA", 4);
              break;
            case 17:
              memcpy(routeDest, "ISBA", 4);
              break;
            case 18:
              memcpy(routeDest, "ISBX", 4);
              break;
            case 19:
              memcpy(routeDest, "ISCB", 4);
              break;
            case 20:
              memcpy(routeDest, "ISCX", 4);
              break;
            case 21:
              memcpy(routeDest, "ISCN", 4);
              break;
            case 22:
              memcpy(routeDest, "ISGA", 4);
              break;
            case 23:
              memcpy(routeDest, "ISGX", 4);
              break;
            case 24:
              memcpy(routeDest, "ISLF", 4);
              break;
            case 25:
              memcpy(routeDest, "ISNQ", 4);
              break;
            case 26:
              memcpy(routeDest, "ISNY", 4);
              break;
            case 27:
              memcpy(routeDest, "ISPX", 4);
              break;
            case 29:
              memcpy(routeDest, "ROUC", 4);
              break;
            case 30:
              memcpy(routeDest, "ROLF", 4);
              break;
            case 31:
              memcpy(routeDest, "ISBY", 4);
              break;
            case 32:
              memcpy(routeDest, "SWPA", 4);
              break;
            case 33:
              memcpy(routeDest, "SWPB", 4);
              break;
            case 34:
              memcpy(routeDest, "IOCM", 4);
              break;
            case 35:
              memcpy(routeDest, "ICMT", 4);
              break;
            case 36:
              memcpy(routeDest, "ROOC", 4);
              break;
            case 37:
              memcpy(routeDest, "ROBY", 4);
              break;
            case 38:
              memcpy(routeDest, "ROBB", 4);
              break;
            case 39:
              memcpy(routeDest, "ROCO", 4);
              break;
            case 40:
              memcpy(routeDest, "SWPC", 4);
              break;
            case 42:
              memcpy(routeDest, "RMPT", 4);
              break;
            default:
              TraceLog(ERROR_LEVEL, "%s New Order (Extended format): Invalid value %d in RouteStrategy field\n", GetOECName(_oecType), routeStrategy);
              break;
          }
          routeDest[4] = 0;
          
          routingInfo = routeDest;
        }
        
        AddOJNewOrderRecord(timeStamp, symbol, venueType, buysell, price, shares, orderID, tif, visibility, isISO, account, routingInfo, capacity, crossID, GetBBReason(&dataBlock, orderID));
        FindAndProcessRawdataInOJWaitList(venueType, orderID);
      }
      else if (msgType == 'O') // NewOrder (short form) haven't routingInfo
      {
        AddOJNewOrderRecord(timeStamp, symbol, venueType, buysell, price, shares, orderID, tif, visibility, isISO, account, NULL, capacity, crossID, GetBBReason(&dataBlock, orderID));
        FindAndProcessRawdataInOJWaitList(venueType, orderID);
      }
    }
    else
      TraceLog(DEBUG_LEVEL, "%: Invalid order message in unsequenced packet, msgType = %c ASCII = %d\n", GetOECName(_oecType), msgType, msgType );
  }
  
  return SUCCESS;
}

void _________ISO_File_Creation_________(void){};
/****************************************************************************
- Function name:  InsertISOToggleToISOFlightFile
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
void InsertISOToggleToISOFlightFile(int tsId, int status, char *user)
{
  double timeInFraction = hbitime_frac_seconds();
  char tm[32] = "\0";
  sprintf(tm, "%.06lf", timeInFraction);
  
  pthread_mutex_lock(&ISOFlight_Mutex);
  if (status == 0)  //Turn off
  {
    fprintf(fpISOFlight, "[type=busclient::BusCommandLog user=%s tm=%s command=parameter args=@[override use_iso 0 ALL] targets=@[[type=busclient::BusService id=[type=busclient::BusServiceIdentifier category=scoring service=score_cluster instance=score_cluster] host=ts%d commands=@[activate bench cleanup_lost complete deactivate deps exit group hosts organize parameter params remove select set_iterations set_state status timeline zero_rejected]]]]\n", user, tm, tsId);
  }
  else  //Turn on
  {
    fprintf(fpISOFlight, "[type=busclient::BusCommandLog user=%s tm=%s command=parameter args=@[revert use_iso ALL] targets=@[[type=busclient::BusService id=[type=busclient::BusServiceIdentifier category=scoring service=score_cluster instance=score_cluster] host=ts%d commands=@[activate bench cleanup_lost complete deactivate deps exit group hosts organize parameter params remove select set_iterations set_state status timeline zero_rejected]]]]\n", user, tm, tsId);
  }
  
  fflush(fpISOFlight);
  pthread_mutex_unlock(&ISOFlight_Mutex);
  
}

/****************************************************************************
- Function name:  
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
void AddRecordToISOFile(int type, char *text1, char *text2, char *text3, char *text4, char *text5, char *text6, char *text7, char *text8, char *text9, char *text10)
{
  t_BBOSnapShot BBOSnapShot[MAX_EXCHANGE];
  char oecinstance[32] = "\0";
  
  switch (type)
  {
    case BBO_RECORD_TYPE_MAIN_HEADER:
      fprintf(fpISOFlight, "[type=mi_iso::FlightRecord symbol=%.8s ", text1);
      break;
    case BBO_RECORD_TYPE_QUOTES_OPEN:
      fprintf(fpISOFlight, "quotes=@[");
      break;
    case BBO_RECORD_TYPE_QUOTES:
      memset (BBOSnapShot, 0, sizeof(t_BBOSnapShot) * MAX_EXCHANGE);
      
      //Get all ASK parts and insert into BBOSnapShot
      char *c = strstr(text1, "BID: ");
      char *b, *e;
      if (c)
      {
        *c = 0;   //terminate at exactly position "B" in "BID:"
        b = text1 + 5; //Start at the first ASK BBO
        
        //Get whole info for a market
        e = strchr (b, ';');
        while (e)
        {
          *e = 0;
          char marketChar = *b;
          int index = PARTICIPANT_TO_INDEX[marketChar - 65];
          //BBOSnapShot[index].participantId = marketChar;
          
          b += 2;
          BBOSnapShot[index].offerSize = atoi(b);
          
          b = strchr(b, ' ');
          b++;
          BBOSnapShot[index].offerPrice = atof(b);
          
          //Go to next market
          b = e + 2;
          if (*b == 0)
          {
            break; //End
          }
          else
          {
            e = strchr (b, ';');
          }
        }
        
        
        //Get all BID parts and insert into BBOSnapShot
        b = c + 5;  //point to the first market of BID Part
        if (*b != 0) //There is something
        {
          //Get whole info for a market
          e = strchr (b, ';');
          while (e)
          {
            *e = 0;
            char marketChar = *b;
            int index = PARTICIPANT_TO_INDEX[marketChar - 65];
            //BBOSnapShot[index].participantId = marketChar;
            
            b += 2;
            BBOSnapShot[index].bidSize = atoi(b);
            
            b = strchr(b, ' ');
            b++;
            BBOSnapShot[index].bidPrice = atof(b);
            
            //Go to next market
            b = e + 2;
            if (*b == 0)
            {
              break; //End
            }
            else
            {
              e = strchr (b, ';');
            }
          }
        }
        
        //Now add Quotes to record
        int j;
        for (j = 0; j < MAX_EXCHANGE; j++)
        {
          int venue_id = INDEX_TO_VENUE_ID[j];
          if (venue_id == -1) //We do not record this exchange
          {
            continue;
          }
          
          char strSize[32], strPrice[32];
          char strVenueID[4];
          sprintf(strVenueID, "%d", venue_id);
          
          //ASK part
          if (BBOSnapShot[j].offerSize <= 0 || BBOSnapShot[j].offerPrice < 0.0001)
          {
            strcpy(strSize, "NAN");
            strcpy(strPrice, "NAN");
            
            fprintf(fpISOFlight, "[type=mi_iso::Quote size=%s price=%s side=A venue_id=%s venue_name=%s]", 
                        strSize, strPrice, strVenueID, MD_Mappings[venue_id]);
          }
          else
          {
            sprintf(strSize, "%d", BBOSnapShot[j].offerSize);
            sprintf(strPrice, "%.2lf", BBOSnapShot[j].offerPrice);
            
            fprintf(fpISOFlight, "[type=mi_iso::Quote size=%s. price=%s side=A venue_id=%s venue_name=%s]", 
                        strSize, strPrice, strVenueID, MD_Mappings[venue_id]);
          }
          
          //BID part
          if (BBOSnapShot[j].bidSize <= 0 || BBOSnapShot[j].bidPrice < 0.0001)
          {
            strcpy(strSize, "NAN");
            strcpy(strPrice, "NAN");
            
            fprintf(fpISOFlight, "[type=mi_iso::Quote size=%s price=%s side=B venue_id=%s venue_name=%s]", 
              strSize, strPrice, strVenueID, MD_Mappings[venue_id]);
          }
          else
          {
            sprintf(strSize, "%d", BBOSnapShot[j].bidSize);
            sprintf(strPrice, "%.2lf", BBOSnapShot[j].bidPrice);
            
            fprintf(fpISOFlight, "[type=mi_iso::Quote size=%s. price=%s side=B venue_id=%s venue_name=%s]", 
              strSize, strPrice, strVenueID, MD_Mappings[venue_id]);
          }
        }
      }
      else
      {
        //Wrong BBO format!!!
      }
      break;
    case BBO_RECORD_TYPE_QUOTES_CLOSE:
    case BBO_RECORD_TYPE_ORDER_CLOSE:
      fprintf(fpISOFlight, "]");
      break;
    case BBO_RECORD_TYPE_ORDER:
      /*
        text1: time
        text2: symbol
        text3: venue
        text4: buysell
        text5: price
        text6: shares
        text7: sequence ID
        text8: Capacity
        text9: Account
        text10: Routing Info
      */
      
      //oecinstance
      GetOECInstance(oecinstance, atoi(text7), atoi(text3));

      fprintf(fpISOFlight, "[type=busclient::RGMOrderMsg tm=%s source=%s symbol=%.8s venue=%s buysell=%s price=%s size=%s. rgmoid=EAA%s expiration=0. visibility=Y state=1 %s", 
          text1, OJ_SOURCE, text2, OE_Mappings[atoi(text3)], text4, text5, text6, text7, oecinstance);
      
      //Properties
      if (text10 == NULL) //No routing info
      {
        fprintf(fpISOFlight, "properties=[version=3 strings=[ExecInst=f account=%s capacity=%s clearing_firm=Fortis creator=blinkbox execution_firm=RGMS rgm_mpid=HBIF] numbers=[] codables=[BasisCurrency=@[[type=market_interface::MiBasisCurrency currency=USD multiplier=1. usd_mult=1.]]]]]", text9, text8);
      }
      else
      {
        fprintf(fpISOFlight, "properties=[version=3 strings=[ExecInst=f Routing=%s account=%s capacity=%s clearing_firm=Fortis creator=blinkbox execution_firm=RGMS rgm_mpid=HBIF] numbers=[] codables=[BasisCurrency=@[[type=market_interface::MiBasisCurrency currency=USD multiplier=1. usd_mult=1.]]]]]", text10, text9, text8);
      }
      break;

    case BBO_RECORD_TYPE_ORDER_OPEN:
      fprintf(fpISOFlight, " tm=%s count=%s orders=@[", text1, text2);
      break;

    case BBO_RECORD_TYPE_RECORD_CLOSE:  //add close tag and flush file
      fprintf(fpISOFlight, "]\n");
      fflush(fpISOFlight);
      break;
  }
}

/****************************************************************************
- Function name:  
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int CreateISOFlightFilesWithDateRange(int beginDateInSecs, int endDateInSecs)
{
  char strQuery[720];
  memset(strQuery, 0, 720);

  int i;
  
  //Build query for current month
  strcat(strQuery, "SELECT DISTINCT new_orders.date, new_orders.time_stamp, new_orders.order_id, trim(new_orders.symbol), new_orders.side, trim(new_orders.ecn), ");
  strcat(strQuery, "new_orders.shares, new_orders.price, new_orders.time_in_force, new_orders.ts_id, new_orders.cross_id, bbo_infos.content, new_orders.ecn_suffix, msg_content, new_orders.account, new_orders.bbo_id ");
  strcat(strQuery, "FROM new_orders JOIN bbo_infos ON new_orders.date = bbo_infos.date AND new_orders.ts_id = bbo_infos.ts_id ");
  strcat(strQuery, "AND new_orders.cross_id = bbo_infos.cross_id AND new_orders.is_iso='Y' AND new_orders.bbo_id = bbo_infos.bbo_id ");
  strcat(strQuery, "ORDER BY date, ts_id, cross_id, bbo_id, order_id;");

  PGresult *queryResult = NULL;
  char *strField = NULL;
  
  int currentMonthFlag = 0, nRows = 0;
  struct tm l_time;
  time_t numberOfSecondsTmp = beginDateInSecs;
  
  int currentTSID;
  int currentCrossID;
  int currentBBOID;
  
  while (numberOfSecondsTmp <= endDateInSecs)
  {
    TraceLog(DEBUG_LEVEL, "***************************************************\n");

    //Get current date
    localtime_r(&numberOfSecondsTmp, &l_time);

    //  We only trade from Monday to Friday
    if ((l_time.tm_wday > 0) && (l_time.tm_wday < 6))
    {
      // Check current date to see if it's new month
      // If so, close current connection and open new connection for new month's database
      if (((l_time.tm_year % 100) * 100 + l_time.tm_mon) != currentMonthFlag) 
      {
        if (currentMonthFlag != 0)
        {
          //Close current connection
          if (queryResult != NULL)
          {
            ClearResult (queryResult);
            queryResult = NULL;
          }
          
          if (ISOFlightConn != NULL)
          {
            DestroyConnection(ISOFlightConn);
            ISOFlightConn = NULL;
          }
        }
        
        // Update flag to store current month
        currentMonthFlag = (l_time.tm_year % 100) * 100 + l_time.tm_mon;
        
        // Connect to database
        char connStr[512];
        if (get_monthly_db_connection_string("eaa", l_time.tm_year + 1900, l_time.tm_mon + 1, connStr, 512) == NULL_POINTER) {
          TraceLog(ERROR_LEVEL, "Cannot get database configuration\n" );
          return ERROR;
        }

        TraceLog(DEBUG_LEVEL, "New database connection string: %s\n", connStr);

        // Create connection to database for current month
        ISOFlightConn = CreateConnection(connStr, ISOFlightConn);
        
        if (ISOFlightConn == NULL)
        {
          TraceLog(ERROR_LEVEL, "Cannot open database connection for current month, ignore processing for this month.\n" );

          // Bypass days to go for new months
          while (1)
          {
            numberOfSecondsTmp += NUMBER_OF_SECONDS_PER_DAY;  //Increase day by 1

            // Start checking for new month
            localtime_r(&numberOfSecondsTmp, &l_time);
            
            if (((l_time.tm_year % 100) * 100 + l_time.tm_mon) != currentMonthFlag)
            {
              break;
            }
          }
          
          continue;
        }

        //Query for one month!
        queryResult = ExecuteQuery(ISOFlightConn, strQuery);
        if( queryResult == NULL )
        {
          TraceLog(DEBUG_LEVEL, "Query database for this month returns nothing, ignore processing for this month.\n");

          // Bypass days to go for new months
          while (1)
          {
            numberOfSecondsTmp += NUMBER_OF_SECONDS_PER_DAY;  //Increase day by 1
          
            // Start checking for new month
            localtime_r(&numberOfSecondsTmp, &l_time);
            
            if (((l_time.tm_year % 100) * 100 + l_time.tm_mon) != currentMonthFlag)
            {
              break;
            }
          }
          
          continue;
        }
        
        // Get total number of rows from query results
        nRows = GetNumberOfRow(queryResult); 
        
        if (nRows <= 0)
        {
          TraceLog(DEBUG_LEVEL, "There is no data for this month, ignore processing for this month.\n");

          // Bypass days to go for new months
          while (1)
          {
            numberOfSecondsTmp += NUMBER_OF_SECONDS_PER_DAY;  //Increase day by 1
          
            // Start checking for new month
            localtime_r(&numberOfSecondsTmp, &l_time);
            
            if (((l_time.tm_year % 100) * 100 + l_time.tm_mon) != currentMonthFlag)
            {
              break;
            }
          }
          continue;
        }
      }
      
      //-------------------------------------
      //  Initialize for current date
      //  This creates data path and ISO file for appending
      //-------------------------------------
      CreateDataPathAtDate(numberOfSecondsTmp);
      
      // Delete current ISO Snapshot file if exist
      if (GetFullRawDataPath(ISO_FLIGHT_FILENALE, ISOFlightFilePath) == NULL_POINTER)
      {
        TraceLog(ERROR_LEVEL, "Have some problem in get full raw data path operation (%s)\n", ISO_FLIGHT_FILENALE);

        return ERROR;
      }
      
      //Check if file already existed
      if (access(ISOFlightFilePath, F_OK) == 0) //File exists, remove it
      {
        char sysCmd[512];
        sprintf(sysCmd, "rm %s", ISOFlightFilePath);
        system(sysCmd);
      }
  
      //-------------------------------------------------------
      //  Generate files
      //-------------------------------------------------------
      if (InitISOFilesCreation() == ERROR)
      {
        TraceLog(ERROR_LEVEL, "Could not generate ISO file for this date");

        return ERROR;
      }
      
      //-------------------------------------------------------
      //  Process query results and update the file
      //  The query returns the following table:
      //  (0)date, (1)time_stamp, (2)order_id, (3)symbol, (4)side, (5)ecn, (6)shares, (7)price, (8)time_in_force,
      //  (9)ts_id, (10)cross_id, (11)BBO, (12)ECN Suffix, (13)msgContent, (14)account, (15)bbo_id
      //
      //  rows are ordered by date, ts_id, cross_id
      //
      //  Note when processing query result:
      //      - Rows are sorted by date, then ts_id, then cross_id
      //          + So, detect new cross by check the combination of (date, ts_id, cross_id)
      //          + So, detect new date by check "date" field
      //      - One row is for one ISO order:
      //      - BBO info is the same for all orders in one cross
      //      - BBO has the following format
      //        ASK: Z 100 120.00; P 1234 10.30; BID: Z 100 120.00; P 1234 10.30;
      //-------------------------------------------------------
      
      char currentDate[12];
      char isProcessed = 0;
      
      //Get current date as in database format
      strftime(currentDate, 12, "%Y-%m-%d", &l_time);
      
      for (i = 0; i < nRows; i++)
      {
        //Check row field for date and compare with current ISO Flight's date
        strField = GetValueField(queryResult, i, 0);
        if ((strField == NULL) || (strcmp(strField, currentDate) != 0))
        {
          if (isProcessed == 0)
          {
            continue; //This is not the date we want
          }
          else
          {
            //Just come to new date, so we finished all rows in current date
            break;
          }
        }
        
        isProcessed = 1;
        
        //---------------------------------------------------------------
        //  Start to generate record, we are at the first order of cross
        //---------------------------------------------------------------
        
        //Record header and quote header
        strField = GetValueField(queryResult, i, 3);  //symbol
        
        char symbol[SYMBOL_LEN];
        strncpy(symbol, strField, SYMBOL_LEN);
        
        AddRecordToISOFile(BBO_RECORD_TYPE_MAIN_HEADER, symbol, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
        
        //----------------
        //    Quotes
        //----------------
        //Open quotes
        AddRecordToISOFile(BBO_RECORD_TYPE_QUOTES_OPEN, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
        
        //Add quotes: BBO field: "ASK: Z 100 120.00; P 1234 10.30; BID: Z 100 120.00; P 1234 10.30;"
        char bbo[1024];
        strField = GetValueField(queryResult, i, 11);
        
        strcpy(bbo, strField);
        AddRecordToISOFile(BBO_RECORD_TYPE_QUOTES, bbo, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
        
        //Close quotes
        AddRecordToISOFile(BBO_RECORD_TYPE_QUOTES_CLOSE, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
                
        //----------------
        //    Orders
        //----------------

        int orderCount = 0;
        char strTime[32];
        
        currentTSID = atoi(GetValueField(queryResult, i, 9));
        currentCrossID = atoi(GetValueField(queryResult, i, 10));
        currentBBOID = atoi(GetValueField(queryResult, i, 15));

        strField = GetValueField(queryResult, i, 1);  //First order time stamp
        TrimRight(strField, strlen(strField));
        sprintf(strTime, "%s-%s", currentDate, strField); //e.g 2012-05-13:08:04:10.012345 (%Y-%m-%d:%H:%M:%S.)
        GetEpochTimeFromDateString(strTime, strTime);
        
        while (i + orderCount < nRows)
        {
          strField = GetValueField(queryResult, i + orderCount, 0); //date
          int _TSID = atoi(GetValueField(queryResult, i + orderCount, 9));
          int _CrossID = atoi(GetValueField(queryResult, i + orderCount, 10));
          int _BBOID = atoi(GetValueField(queryResult, i + orderCount, 15));
          
          if ((strcmp(strField, currentDate) == 0) &&
            (currentTSID == _TSID) &&
            (currentCrossID == _CrossID) &&
            (currentBBOID == _BBOID))
          {
            orderCount++;
          }
          else
          {
            break;
          }
        }
        
        //Order Header
        char strOrderCount[4];
        sprintf(strOrderCount, "%d", orderCount);
        AddRecordToISOFile(BBO_RECORD_TYPE_ORDER_OPEN, strTime, strOrderCount, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
        
        //Orders
        int j;
        for (j = 0; j < orderCount; j++)
        {
          //Time stamp
          strField = GetValueField(queryResult, i+j, 1);
          TrimRight(strField, strlen(strField));
          sprintf(strTime, "%s-%s", currentDate, strField); //e.g 2012-05-13:08:04:10.012345 (%Y-%m-%d:%H:%M:%S.)
          GetEpochTimeFromDateString(strTime, strTime);
          
          //venue + account
          int tradingAccountIndex = atoi(GetValueField(queryResult, i+j, 14));

          // From database, sequenceID is always equal to orderID
          int orderID;
          strField = GetValueField(queryResult, i+j, 2);  //order id
          orderID = atoi(strField);
          
          strField = GetValueField(queryResult, i+j, 5);  //ecn
                    
          char strAccount[32] = "";
          if (tradingAccountIndex < 0 || tradingAccountIndex > MAX_ACCOUNT)
          {
            TraceLog(ERROR_LEVEL, "(CreateISOFlightFilesWithDateRange): Invalid account index (%d)\n", tradingAccountIndex);
            continue;
          }

          //ecn_suffix:   ecn=OUCH or ecn=RASH will have no ecn_suffix
          //        ecn=ARCA and ecn_suffix = NULL ---> ARCA DIRECT
          //        ecn=ARCA and ecn_suffix = FIX ----> ARCA DIRECT
          //        ecn=NYSE and ecn_suffix = NULL ---> NYSE CCG
          //        ecn=BATZ and ecn_suffix = NULL ---> BATSZ BOE
          //        ecn=EDGX and ecn_suffix = NULL ---> EDGX

          char strVenueID[5];
          if (strncmp(strField, "ARCA", 4) == 0)
          {
            strField = GetValueField(queryResult, i+j, 12); //ecn_suffix
            
            if (strField == NULL)
            {
              sprintf(strVenueID, "%d", VENUE_ARCA);
              strcpy(strAccount, TradingAccount.DIRECT[tradingAccountIndex]);
            }
            else
            {
              sprintf(strVenueID, "%d", VENUE_ARCA);
              strcpy(strAccount, TradingAccount.DIRECT[tradingAccountIndex]);
            }
          }
          else if(strncmp(strField, "OUCH", 4) == 0)
          {
            sprintf(strVenueID, "%d", VENUE_OUCH);
            strcpy(strAccount, TradingAccount.OUCH[tradingAccountIndex]);
          }
          else if(strncmp(strField, "RASH", 4) == 0)
          {
            sprintf(strVenueID, "%d", VENUE_RASH);
            strcpy(strAccount, TradingAccount.RASH[tradingAccountIndex]);
          }
          else if(strncmp(strField, "BATZ", 4) == 0)
          {
            sprintf(strVenueID, "%d", VENUE_BZX);
            strcpy(strAccount, TradingAccount.BATSZBOE[tradingAccountIndex]);
          }
          else if(strncmp(strField, "NYSE", 4) == 0)
          {
            sprintf(strVenueID, "%d", VENUE_CCG);
            strcpy(strAccount, TradingAccount.CCG[tradingAccountIndex]);
          }
          else if(strncmp(strField, "EDGX", 4) == 0)
          {
            sprintf(strVenueID, "%d", VENUE_EDGX);
            strcpy(strAccount, TradingAccount.EDGX[tradingAccountIndex]);
          }
          else if(strncmp(strField, "BATY", 4) == 0)
          {
            sprintf(strVenueID, "%d", VENUE_BYX);
            strcpy(strAccount, TradingAccount.BYXBOE[tradingAccountIndex]);
          }
          else
          {
            TraceLog(ERROR_LEVEL, "Unknown ecn value (%s) from database\n", strField);
          }

          //BuySell
          char strBuySell[4];
          strField = GetValueField(queryResult, i+j, 4);  //side
          if (strncmp(strField, "SS", 2) == 0)
          {
            strcpy(strBuySell, "T");
          }
          else if (*strField == 'S')
          {
            strcpy(strBuySell, "S");
          }
          else
          {
            strcpy(strBuySell, "B");
          }
          
          //price
          char strPrice[16];
          strField = GetValueField(queryResult, i+j, 7);  //price
          sprintf(strPrice, "%.4lf", atof(strField));
          
          //size
          char strShares[16];
          strField = GetValueField(queryResult, i+j, 6);  //shares
          strcpy(strShares, strField);
          
          //seqID
          char strSeqID[16];
          sprintf(strSeqID, "%d", orderID);
          
          //Get capacity (Rule80A) and Routing Info (for RASH Only) from msgContent
          char needle[8], strRoutingInfo[10], strCapacity[4] = "";

          strField = GetValueField(queryResult, i+j, 13); //msgContent
          
          //Get routing info
          strRoutingInfo[0] = 0;
          
          sprintf(needle, "\001%d=", fieldIndexList.ExecBroker);
          char *b = strstr(strField, needle);
          if (b)
          {
            b += strlen(needle);
            char *e = strchr(b, 1);
            if (e)
            {
              *e = 0;
              strcpy(strRoutingInfo, b);
              *e = 1;
            }
          }
          
          //Get capacity
          int isFound = 0;
          sprintf(needle, "\001%d=", fieldIndexList.Rule80A);
          b = strstr(strField, needle);
          if (b)
          {
            b += strlen(needle);
            sprintf(strCapacity, "%c", *b);
            isFound = 1;
          }
          
          if (isFound == 0)
          {
            sprintf(needle, "\001%d=", fieldIndexList.Rule80AIndicator);
            b = strstr(strField, needle);
            if (b)
            {
              b += strlen(needle);
              sprintf(strCapacity, "%c", *b);
              isFound = 1;
            }
          }
          
          if (isFound == 0)
          {
            sprintf(needle, "\001%d=", fieldIndexList.Capacity);
            b = strstr(strField, needle);
            if (b)
            {
              b += strlen(needle);
              sprintf(strCapacity, "%c", *b);
            }
          }
          
          if (strRoutingInfo[0] == 0) //No Routing info
          {
            AddRecordToISOFile(BBO_RECORD_TYPE_ORDER, strTime, symbol, strVenueID, strBuySell, strPrice, strShares, strSeqID, strCapacity, strAccount, NULL);
          }
          else
          {
            AddRecordToISOFile(BBO_RECORD_TYPE_ORDER, strTime, symbol, strVenueID, strBuySell, strPrice, strShares, strSeqID, strCapacity, strAccount, strRoutingInfo);
          }
        }
        
        AddRecordToISOFile(BBO_RECORD_TYPE_ORDER_CLOSE, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
        AddRecordToISOFile(BBO_RECORD_TYPE_RECORD_CLOSE, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL); //write and flush to file
        
        //Skip processed cross
        i += (orderCount - 1);
      }

      //-------------------------------------------------------
      //  Close file pointer for this date
      //-------------------------------------------------------
      fclose(fpISOFlight);
      fpISOFlight = NULL;
    }
    else
    {
      if (l_time.tm_wday == 6)
      {
        TraceLog(DEBUG_LEVEL, "** This is day off (Saturday).\n");
      }
      else
      {
        TraceLog(DEBUG_LEVEL, "** This is day off (Sunday).\n");
      }

      TraceLog(DEBUG_LEVEL, "***************************************************\n");
    }

    //  Increasing 1 day
    numberOfSecondsTmp += NUMBER_OF_SECONDS_PER_DAY;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  AddISOOrderToCollection
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int AddISOOrderToCollection(int crossID, short bboID, char *symbol, t_ISOOrderInfo *order)
{
  //determine tsIndex
  int tsIndex;
  int seqID = atoi(order->strSequenceID);
  
  if (seqID < 2000000)  tsIndex = 0;
  else if (seqID < 3000000) tsIndex = 1;
  else if (seqID < 4000000) tsIndex = 2;
  else if (seqID < 5000000) tsIndex = 3;
  else if (seqID < 6000000) tsIndex = 4;
  else if (seqID < 7000000) tsIndex = 5;
  else if (seqID < 8000000) tsIndex = 6;
  else if (seqID < 9000000) tsIndex = 7;
  else tsIndex = 8;

  t_ISOOpenCrossMgmt *currentISOMgmt = &ISOOpenCrossMgmt[tsIndex];
  
  int i, isFound = 0, slot = -1;
  for (i = 0; i < currentISOMgmt->bound; i++)
  {
    if ((currentISOMgmt->crosses[i].crossID == crossID) && (bboID == currentISOMgmt->crosses[i].bboID) )
    {
      isFound = 1;
      slot = i;
      break;
    }
    else if (currentISOMgmt->crosses[i].crossID == -1)
    {
      if (slot == -1) slot = i;
    }

  }
  
  //add to <slot>
  if (isFound == 1)
  {
    //currentISOMgmt->crosses[slot].crossID = crossID;  <-- no need to set, already there!
    //currentISOMgmt->crosses[slot].bboID = bboID;  <-- no need to set, already there!
    strncpy(currentISOMgmt->crosses[slot].symbol, symbol, SYMBOL_LEN);
    memcpy(&currentISOMgmt->crosses[slot].orders[(int)currentISOMgmt->crosses[slot].totalOrder], order, sizeof(t_ISOOrderInfo));
    currentISOMgmt->crosses[slot].totalOrder++;
  }
  else
  {
    if (slot != -1) //Found slot, but don't increase <bound>
    {
      //Add to this slot
      currentISOMgmt->crosses[slot].crossID = crossID;
      currentISOMgmt->crosses[slot].bboID = bboID;
      strncpy(currentISOMgmt->crosses[slot].symbol, symbol, SYMBOL_LEN);
      memcpy(&currentISOMgmt->crosses[slot].orders[(int)currentISOMgmt->crosses[slot].totalOrder], order, sizeof(t_ISOOrderInfo));
      currentISOMgmt->crosses[slot].totalOrder++;
    }
    else  //There is no free slot, increase the bound and check if overflow occurs
    {
      slot = currentISOMgmt->bound;
      if (slot >= (MAX_ISO_OPEN_CROSS - 1))
      {
        TraceLog(ERROR_LEVEL, "ISO Collection for storing ISO Crosses is full!\n");
        return ERROR;
      }
      else
      {
        currentISOMgmt->bound++;
        currentISOMgmt->crosses[slot].crossID = crossID;
        currentISOMgmt->crosses[slot].bboID = bboID;
        strncpy(currentISOMgmt->crosses[slot].symbol, symbol, SYMBOL_LEN);
        memcpy(&currentISOMgmt->crosses[slot].orders[(int)currentISOMgmt->crosses[slot].totalOrder], order, sizeof(t_ISOOrderInfo));
        currentISOMgmt->crosses[slot].totalOrder++;
      }
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  AddISOFlightRecordForCrossIndex
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int AddISOFlightRecordForCrossIndex (int tsIndex, int crossIndex)
{
  t_ISOOpenCrossInfo *crossInfo = &(ISOOpenCrossMgmt[tsIndex].crosses[crossIndex]);
  
  //--------------------------------------
  //  Short orders by sequenceID
  //--------------------------------------
  int i, j, tmpIndex = 0, selectedIndex = 0;
  int countOrder = (int)crossInfo->totalOrder;
  t_ISOOrderInfo tmpOrders[countOrder];
  
  int tmpSeq, minSeq;
  for (i=0; i<countOrder; i++)
  {
    minSeq = 0;
    selectedIndex = -1;
    for (j=0; j<countOrder; j++)
    {
      tmpSeq = atoi(crossInfo->orders[j].strSequenceID);
      if ((tmpSeq > 0) && (minSeq == 0))
      {
        minSeq = tmpSeq;
        selectedIndex = j;
      }
      else if (( tmpSeq <= minSeq) && (tmpSeq > 0))
      {
        minSeq = tmpSeq;
        selectedIndex = j;
      }
    }
    
    if (selectedIndex != -1)
    {
      //Copy to new collection
      memcpy(&tmpOrders[tmpIndex], &crossInfo->orders[selectedIndex], sizeof(t_ISOOrderInfo));
      /*
      //Remove trailing zero from price and time
      int x, len;
      len = strlen(tmpOrders[tmpIndex].strPrice);
      for (x=len-1; x >=0; x--)
      {
        if (tmpOrders[tmpIndex].strPrice[x] == '0')
        {
          tmpOrders[tmpIndex].strPrice[x] = 0;
        }
        else if (tmpOrders[tmpIndex].strPrice[x] == '.')
        {
          tmpOrders[tmpIndex].strPrice[x] = 0;
          break;
        }
        else break;
      }
      
      len = strlen(tmpOrders[tmpIndex].strTime);
      for (x=len-1; x >=0; x--)
      {
        if (tmpOrders[tmpIndex].strTime[x] == '0')
        {
          tmpOrders[tmpIndex].strTime[x] = 0;
        }
        else if (tmpOrders[tmpIndex].strTime[x] == '.')
        {
          tmpOrders[tmpIndex].strTime[x] = 0;
          break;
        }
        else break;
      }
      */
      tmpIndex++;
      strcpy(crossInfo->orders[selectedIndex].strSequenceID, "0");  //Set it to Zero, so it wont be checked again
    }
  }
  
  pthread_mutex_lock(&ISOFlight_Mutex);
  AddRecordToISOFile(BBO_RECORD_TYPE_MAIN_HEADER, crossInfo->symbol, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
  AddRecordToISOFile(BBO_RECORD_TYPE_QUOTES_OPEN, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
  AddRecordToISOFile(BBO_RECORD_TYPE_QUOTES, crossInfo->bbo, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
  AddRecordToISOFile(BBO_RECORD_TYPE_QUOTES_CLOSE, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
  
  char strOrderCount[3];
  sprintf(strOrderCount, "%d", countOrder);
  
  AddRecordToISOFile(BBO_RECORD_TYPE_ORDER_OPEN, tmpOrders[0].strTime, strOrderCount, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
  
  for (i = 0; i < tmpIndex; i++)
  {
    if (tmpOrders[i].strRoutingInfo[0] == 0)  //No Routing info
    {
      AddRecordToISOFile(BBO_RECORD_TYPE_ORDER, tmpOrders[i].strTime,
                            crossInfo->symbol, 
                            tmpOrders[i].strVenueID, 
                            tmpOrders[i].strBuysell, 
                            tmpOrders[i].strPrice, 
                            tmpOrders[i].strShares, 
                            tmpOrders[i].strSequenceID, 
                            tmpOrders[i].strCapacity, 
                            tmpOrders[i].strAccount, 
                            NULL);
    }
    else
    {
      AddRecordToISOFile(BBO_RECORD_TYPE_ORDER, tmpOrders[i].strTime,
                            crossInfo->symbol, 
                            tmpOrders[i].strVenueID, 
                            tmpOrders[i].strBuysell, 
                            tmpOrders[i].strPrice, 
                            tmpOrders[i].strShares, 
                            tmpOrders[i].strSequenceID, 
                            tmpOrders[i].strCapacity, 
                            tmpOrders[i].strAccount, 
                            tmpOrders[i].strRoutingInfo);
                            
    }
  }
  
  AddRecordToISOFile(BBO_RECORD_TYPE_ORDER_CLOSE, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
  AddRecordToISOFile(BBO_RECORD_TYPE_RECORD_CLOSE, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL); //write and flush to file
  
  pthread_mutex_unlock(&ISOFlight_Mutex);

  //Clear all data for this cross, create space for another cross
  crossInfo->crossID = -1;
  crossInfo->totalOrder = 0;
  crossInfo->bboID = -1;
  crossInfo->waitingCount = 0;
  return SUCCESS;
}

void _________Time_Series_Creation_________(void){};
/****************************************************************************
- Function name:  ExportTimeSeriesFormat
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
void *ExportTimeSeriesFormat(void *pArgs)
{ 
  int rmInfoIndex, totalShares = 0;
  double totalMatchPL = 0.00, totalFee = 0.00, totalMarket = 0.00;
  int account, countSymbol;
  double timeInFraction = 0.00;
  
  while (1)
  {
    totalShares = 0;
    totalMatchPL = 0.00;
    totalFee = 0.00;
    totalMarket = 0.00;
    
    countSymbol = RMList.countSymbol;
    for (account = 0; account < MAX_ACCOUNT; account++)
    {   
      for (rmInfoIndex = 0; rmInfoIndex < countSymbol; rmInfoIndex++)
      {
        // Total shares
        totalShares += RMList.collection[account][rmInfoIndex].totalShares;

        // Total matched PL
        totalMatchPL += RMList.collection[account][rmInfoIndex].matchedPL;

        // Total market
        totalMarket += fabs(RMList.collection[account][rmInfoIndex].totalMarket);
        
        // Total fee
        totalFee += RMList.collection[account][rmInfoIndex].totalFee;
      }
    }
    
    //Get timestamp from epoch with format <second from epoch>.<microsecond> (eg. 1338452744.415999)
    timeInFraction = hbitime_frac_seconds();
    char tm[32] = "\0";
    sprintf(tm, "%.06lf", timeInFraction);
    
    // Net profit record
    AddTimeSeriesNetProfitRecord(tm, totalMatchPL - totalFee);
    
    // Total volume record
    AddTimeSeriesTotalVolumetRecord(tm, totalShares);
    
    // Entry new order delay time
    ProcessEntryNewOrderDelayTime(tm);
    
    // ACK delay time
    ProcessAckDelayTime(tm);
    
    // Fill delay time
    ProcessFillDelayTime(tm);
    
    // Cancel delay time
    ProcessCancelDelayTime(tm);
    
    // Exit new order delay
    ProcessExitNewOrderDelayTime(tm);
    
    // Export total net profit per minute
    sleep(60);
  }

  return NULL;
}

/****************************************************************************
- Function name:  AddTimeSeriesNetProfitRecord
- Input:    
- Output:   
- Return:   
- Description:    
- Usage:      N/A 
****************************************************************************/
int AddTimeSeriesNetProfitRecord(char *time, double netProfitLoss)
{
  char record[1024];
  int offset = 0;
  
  // Example format
  // 1333631065.434563 [type=timeseries::TimeSeriesValues numeric_values=@[[type=timeseries::TimeSeriesNumericValue tm=1333631065.434563 id=0 keywords=location:us%20pool:blink%20type:net_profit_total value=5000]] string_values=@[]]
  
  //Time stamp
  offset += sprintf(&record[offset], "%s ", time);
  
  //Type
  offset += sprintf(&record[offset], "[type=timeseries::TimeSeriesValues numeric_values=@[[type=timeseries::TimeSeriesNumericValue ");
  
  //Time stamp
  offset += sprintf(&record[offset], "tm=%s ", time);
  
  //id
  offset += sprintf(&record[offset], "id=0 ");
  
  //keywords
  char keywords[128];
  strcpy(keywords, "location:car%20pool:carblink%20type:net_profit_total");
  offset += sprintf(&record[offset], "keywords=%s ", keywords);
  
  //value
  offset += sprintf(&record[offset], "value=%.4lf]] ", netProfitLoss);
  
  //string
  offset += sprintf(&record[offset], "string_values=@[]");
  
  //End of record
  record[offset++] = ']';
  record[offset++] = '\n';
  record[offset] = 0;

  fprintf(fpTimeSeries, "%s", record);
  fflush(fpTimeSeries);
  return SUCCESS;
}

/****************************************************************************
- Function name:  AddTimeSeriesTotalVolumetRecord
- Input:    
- Output:   
- Return:   
- Description:    
- Usage:      N/A 
****************************************************************************/
int AddTimeSeriesTotalVolumetRecord(char *time, int totalVolume)
{
  char record[1024];
  int offset = 0;
  
  // Example format
  // 1333631065.434563 [type=timeseries::TimeSeriesValues numeric_values=@[[type=timeseries::TimeSeriesNumericValue tm=1333631065.434563 id=0 keywords=location:us%20pool:blink%20type:size_traded_total value=919574]] string_values=@[]]
  
  //Time stamp
  offset += sprintf(&record[offset], "%s ", time);
    
  //Type
  offset += sprintf(&record[offset], "[type=timeseries::TimeSeriesValues numeric_values=@[[type=timeseries::TimeSeriesNumericValue ");
  
  //Time stamp
  offset += sprintf(&record[offset], "tm=%s ", time);
  
  //id
  offset += sprintf(&record[offset], "id=0 ");
  
  //keywords
  char keywords[128];
  strcpy(keywords, "location:car%20pool:carblink%20type:size_traded_total");
  offset += sprintf(&record[offset], "keywords=%s ", keywords);
  
  //value
  offset += sprintf(&record[offset], "value=%d]] ", totalVolume);
  
  //string
  offset += sprintf(&record[offset], "string_values=@[]");
  
  //End of record
  record[offset++] = ']';
  record[offset++] = '\n';
  record[offset] = 0;

  fprintf(fpTimeSeries, "%s", record);
  fflush(fpTimeSeries);
  return SUCCESS;
}

/****************************************************************************
- Function name:  AddTimeSeriesTotalMarketRecords
- Input:
- Output:
- Return:
- Description:
- Usage:      N/A
****************************************************************************/
int AddTimeSeriesTotalMarketRecords()
{
  double totalMarketSpec[MAX_ACCOUNT], totalMarket = 0.00;
  int account, rmInfoIndex;
  int countSymbol = RMList.countSymbol;
  
  for (account = 0; account < MAX_ACCOUNT; account++)
  {
    totalMarketSpec[account] = 0.00;
    for (rmInfoIndex = 0; rmInfoIndex < countSymbol; rmInfoIndex++)
    {
      // Total market
      totalMarketSpec[account] += fabs(RMList.collection[account][rmInfoIndex].totalMarket);
      totalMarket += fabs(RMList.collection[account][rmInfoIndex].totalMarket);
    }
  }
    
  //Get timestamp from epoch with format <second from epoch>.<microsecond> (eg. 1338452744.415999)
  double timeInFraction = hbitime_frac_seconds();
  char tm[32] = "\0";
  sprintf(tm, "%.06lf", timeInFraction);
  
  // Total volume record
  for (account = 0; account < MAX_ACCOUNT; account++)
  {
    AddTimeSeriesTotalMarketRecord(tm, totalMarketSpec[account], account);
  }
  AddTimeSeriesTotalMarketRecord(tm, totalMarket, -1);
    
  return SUCCESS;
}

/****************************************************************************
- Function name:  AddTimeSeriesTotalMarketRecord
- Input:
- Output:
- Return:
- Description:
- Usage:      N/A
****************************************************************************/
int AddTimeSeriesTotalMarketRecord(char *time, double totalMarket, int accountIndex)
{
  char record[1024];
  int offset = 0;

  // Example format
  // 1359963241.177703 [type=timeseries::TimeSeriesNumericValue tm=1359963241.177703 id=0 keywords=type:invested_nominal_total%20pool:carblink%20location:car value=103232.]

  //Time stamp
  offset += sprintf(&record[offset], "%s ", time);

  //Type
  offset += sprintf(&record[offset], "[type=timeseries::TimeSeriesValues numeric_values=@[[type=timeseries::TimeSeriesNumericValue ");

  //Time stamp
  offset += sprintf(&record[offset], "tm=%s ", time);

  //id
  offset += sprintf(&record[offset], "id=0 ");

  //keywords
  char keywords[128];
  
  if (accountIndex > -1 && accountIndex < MAX_ACCOUNT)
  {
    char account[32];
    strcpy(account, TradingAccount.DIRECT[accountIndex]);
    
    strcpy(keywords, "location:car%20pool:carblink%20type:invested_nominal_total%20account:");
    offset += sprintf(&record[offset], "keywords=%s%s ", keywords, account);
  }
  else
  {
    strcpy(keywords, "location:car%20pool:carblink%20type:invested_nominal_total");
    offset += sprintf(&record[offset], "keywords=%s ", keywords);
  }

  //value
  offset += sprintf(&record[offset], "value=%.0lf.]] ", totalMarket);

  //string
  offset += sprintf(&record[offset], "string_values=@[]");

  //End of record
  record[offset++] = ']';
  record[offset++] = '\n';
  record[offset] = 0;

  fprintf(fpTimeSeries, "%s", record);
  fflush(fpTimeSeries);
  return SUCCESS;
}

/****************************************************************************
- Function name:  AddTimeSeriesDelayTimeRecord
- Input:    
- Output:   
- Return:   
- Description:    
- Usage:      N/A 
****************************************************************************/
int AddTimeSeriesDelayTimeRecord(char *time, int delayTime, int type, char *ecn, char *server, char *eventTime, char *routingInst)
{
  char record[1024];
  char venueName[8];
  char serverName[16];
  int offset = 0;
  
  // Example format
  // 1333631065.434563 [type=timeseries::TimeSeriesValues numeric_values=@[[type=timeseries::TimeSeriesNumericValue tm=1333631065.434563 id=0 keywords=location:us%20pool:blink%20type:internal_order_delay phase: entry value=18]] string_values=@[]]

  //Time stamp
  offset += sprintf(&record[offset], "%s ", time);
  
  //Type
  offset += sprintf(&record[offset], "[type=timeseries::TimeSeriesValues numeric_values=@[[type=timeseries::TimeSeriesNumericValue ");
    
  //Time stamp
  offset += sprintf(&record[offset], "tm=%s ", eventTime);
  
  //id
  offset += sprintf(&record[offset], "id=0 ");
  
  memset(venueName, 0, 8);
  if (ecn != NULL)
  {
    strcpy(venueName, ecn);
  }
  
  memset(serverName, 0, 16);
  if (server != NULL)
  {
    strcpy(serverName, server);
  }
  
  //keywords
  char keywords[128];
  switch (type)
  {
    case REENTRY_ORDER:
      sprintf(keywords, "location:car%%20pool:carblink%%20type:order_internal_delay%%20venue:%s%%20server:%s%%20phase:reentry", venueName, serverName);
      break;
    case ENTRY_ORDER:
      sprintf(keywords, "location:car%%20pool:carblink%%20type:order_internal_delay%%20venue:%s%%20server:%s%%20phase:entry", venueName, serverName);
      break;
    case EXIT_ORDER:
      sprintf(keywords, "location:car%%20pool:carblink%%20type:order_internal_delay%%20venue:%s%%20server:%s%%20phase:exit", venueName, serverName);
      break;
    case ENTRY_ACK:
      sprintf(keywords, "location:car%%20pool:carblink%%20type:order_accept_delay%%20venue:%s%%20server:%s%%20phase:entry", venueName, serverName);
      break;
    case EXIT_ACK:
      if (strncmp(ecn, "batsz", 5) == 0)
      {
        if (strncmp(routingInst, "RL2", 3) == 0)
          sprintf(keywords, "location:car%%20pool:carblink%%20type:order_accept_delay%%20venue:%s%%20server:%s%%20phase:exit%%20routable:1", venueName, serverName);
        else
          sprintf(keywords, "location:car%%20pool:carblink%%20type:order_accept_delay%%20venue:%s%%20server:%s%%20phase:exit%%20routable:0", venueName, serverName);
      }
      else
        sprintf(keywords, "location:car%%20pool:carblink%%20type:order_accept_delay%%20venue:%s%%20server:%s%%20phase:exit", venueName, serverName);
      
      break;
    case ENTRY_ACK_INTERNAL:
      sprintf(keywords, "location:car%%20pool:carblink%%20type:order_accept_internal_delay%%20venue:%s%%20server:%s%%20phase:entry", venueName, serverName);
      break;
    case EXIT_ACK_INTERNAL:
      if (strncmp(ecn, "batsz", 5) == 0)
      {
        if (strncmp(routingInst, "RL2", 3) == 0)
          sprintf(keywords, "location:car%%20pool:carblink%%20type:order_accept_internal_delay%%20venue:%s%%20server:%s%%20phase:exit%%20routable:1", venueName, serverName);
        else
          sprintf(keywords, "location:car%%20pool:carblink%%20type:order_accept_internal_delay%%20venue:%s%%20server:%s%%20phase:exit%%20routable:0", venueName, serverName);
      }
      else
        sprintf(keywords, "location:car%%20pool:carblink%%20type:order_accept_internal_delay%%20venue:%s%%20server:%s%%20phase:exit", venueName, serverName);
      
      break;
    case ENTRY_FILL:
      sprintf(keywords, "location:car%%20pool:carblink%%20type:order_fill_delay%%20venue:%s%%20server:%s%%20phase:entry", venueName, serverName);
      break;
    case EXIT_FILL:
      if (strncmp(ecn, "batsz", 5) == 0)
      {
        if (strncmp(routingInst, "RL2", 3) == 0) 
          sprintf(keywords, "location:car%%20pool:carblink%%20type:order_fill_delay%%20venue:%s%%20server:%s%%20phase:exit%%20routable:1", venueName, serverName);
        else
          sprintf(keywords, "location:car%%20pool:carblink%%20type:order_fill_delay%%20venue:%s%%20server:%s%%20phase:exit%%20routable:0", venueName, serverName);
      }
      else
        sprintf(keywords, "location:car%%20pool:carblink%%20type:order_fill_delay%%20venue:%s%%20server:%s%%20phase:exit", venueName, serverName);
      break;
    case ENTRY_FILL_INTERNAL:
      sprintf(keywords, "location:car%%20pool:carblink%%20type:order_fill_internal_delay%%20venue:%s%%20server:%s%%20phase:entry", venueName, serverName);
      break;
    case EXIT_FILL_INTERNAL:
      if (strncmp(ecn, "batsz", 5) == 0)
      {
        if (strncmp(routingInst, "RL2", 3) == 0) 
          sprintf(keywords, "location:car%%20pool:carblink%%20type:order_fill_internal_delay%%20venue:%s%%20server:%s%%20phase:exit%%20routable:1", venueName, serverName);
        else
          sprintf(keywords, "location:car%%20pool:carblink%%20type:order_fill_internal_delay%%20venue:%s%%20server:%s%%20phase:exit%%20routable:0", venueName, serverName);
      }
      else
        sprintf(keywords, "location:car%%20pool:carblink%%20type:order_fill_internal_delay%%20venue:%s%%20server:%s%%20phase:exit", venueName, serverName);
      break;
    case ENTRY_CANCEL:
      strcpy(keywords, "location:car%20pool:carblink%20type:order_cancel_delay%20phase:entry");
      break;
    case EXIT_CANCEL:
      strcpy(keywords, "location:car%20pool:carblink%20type:order_cancel_delay%20phase:exit");
      break;
    default:
      TraceLog(ERROR_LEVEL, "Invalid Delay Time Type Record\n");
      break;
  }
  
  offset += sprintf(&record[offset], "keywords=%s ", keywords);
  
  //delay
  offset += sprintf(&record[offset], "value=%d]] ", delayTime);
  
  //string
  offset += sprintf(&record[offset], "string_values=@[]");
  
  //End of record
  record[offset++] = ']';
  record[offset++] = '\n';
  record[offset] = 0;

  fprintf(fpTimeSeries, "%s", record);
  fflush(fpTimeSeries);
  return SUCCESS;
}


void _________Utilities_________(void){};

unsigned char GetBBReason(t_DataBlock *dataBlock, int orderId)
{
  if (orderId >= 1000000) //Trade Server order
  {
    if (dataBlock->addContent[DATABLOCK_OFFSET_LAUNCH] == EXIT_VALUE)
      return OJ_BB_REASON_EXIT;
    else
      return OJ_BB_REASON_ENTRY;
  }
  else    //AS or PM order
  {
    if (orderId < 500000)
      return OJ_BB_REASON_AS;
    else
      return OJ_BB_REASON_PM;
  }
}

/****************************************************************************
- Function name:  GetEpochTimeFromDateString
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
void GetEpochTimeFromDateString(char *strDate, char *res)
{
  struct tm localTime;
  memset(&localTime, 0, sizeof(localTime));
  
  strptime(strDate, "%Y-%m-%d-%H:%M:%S.", &localTime);
  localTime.tm_isdst = -1;  //Auto detect daylight saving time
  
  time_t epochTime = mktime(&localTime);
  int numSecond = epochTime;
  
  char numMicroSecond[12];
  strcpy(numMicroSecond, &strDate[20]);
  
  sprintf(res, "%d.%s", numSecond, numMicroSecond);
  
  return;
}

/****************************************************************************
- Function name:  GetOECInstance
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int GetOECInstance(char *record, int orderId, int venueID)
{
  int iLen = 0;
  //oecinstance
  char login[MAX_ACCOUNT_LOGIN_LEN + 1] = "\0";
  if (orderId < 500000) //as
  {
    switch (venueID)
    {
      case VENUE_ARCA:
        strncpy(login, TradingAccount.ARCADIRECTLoginAS, MAX_ACCOUNT_LOGIN_LEN);
        break;

      case VENUE_RASH:
        strncpy(login, TradingAccount.RASHLoginAS, MAX_ACCOUNT_LOGIN_LEN);
        break;
    }
    
    iLen = sprintf(record, "oecinstance=eaa-as/%s/%s ", OE_Mappings[venueID], login);
  }
  else if (orderId < 1000000) //pm
  {
    switch (venueID)
    {
      case VENUE_ARCA:
        strncpy(login, TradingAccount.ARCADIRECTLoginPM, MAX_ACCOUNT_LOGIN_LEN);
        break;

      case VENUE_RASH:
        strncpy(login, TradingAccount.RASHLoginPM, MAX_ACCOUNT_LOGIN_LEN);
        break;
    }
    
    iLen = sprintf(record, "oecinstance=eaa-pm/%s/%s ", OE_Mappings[venueID], login);
  }
  else if (orderId < (MAX_TRADE_SERVER_CONNECTIONS + 1) * 1000000) // For all TSs
  {
    int tsId = orderId / 1000000;
    
    switch (venueID)
    {
      case VENUE_ARCA:
        strncpy(login, TradingAccount.DIRECTLoginTS[tsId - 1], MAX_ACCOUNT_LOGIN_LEN);
        break;
        
      case VENUE_RASH:
        strncpy(login, TradingAccount.RASHLoginTS[tsId - 1], MAX_ACCOUNT_LOGIN_LEN);
        break;

      case VENUE_OUCH:
        strncpy(login, TradingAccount.OUCHLoginTS[tsId - 1], MAX_ACCOUNT_LOGIN_LEN);
        break;

      case VENUE_CCG:
        strncpy(login, TradingAccount.CCGLoginTS[tsId - 1], MAX_ACCOUNT_LOGIN_LEN);
        break;

      case VENUE_BZX:
        strncpy(login, TradingAccount.BATSZBOELoginTS[tsId - 1], MAX_ACCOUNT_LOGIN_LEN);
        break;
        
      case VENUE_EDGX:
        strncpy(login, TradingAccount.EDGXLoginTS[tsId - 1], MAX_ACCOUNT_LOGIN_LEN);
        break;
        
      case VENUE_EDGA:
        strncpy(login, TradingAccount.EDGALoginTS[tsId - 1], MAX_ACCOUNT_LOGIN_LEN);
        break;
        
      case VENUE_OUBX:
        strncpy(login, TradingAccount.OUBXLoginTS[tsId - 1], MAX_ACCOUNT_LOGIN_LEN);
        break;
        
      case VENUE_BYX:
        strncpy(login, TradingAccount.BYXBOELoginTS[tsId - 1], MAX_ACCOUNT_LOGIN_LEN);
        break;
        
      case VENUE_PSX:
        strncpy(login, TradingAccount.PSXLoginTS[tsId - 1], MAX_ACCOUNT_LOGIN_LEN);
        break;
    }
    
    iLen = sprintf(record, "oecinstance=eaa-ts%d/%s/%s ", tsId, OE_Mappings[venueID], login);
  }
  else
  {
    //????????????
  }
  
  return iLen;
}


int AddExecDataToList(t_LinkedlistExecution **list, const int orderId, const long execId)
{
  t_LinkedlistExecution *node;
  
  if (*list == NULL)
  {
    node = (t_LinkedlistExecution *) malloc(sizeof(t_LinkedlistExecution));
    node->data.orderId = orderId;
    node->data.execId = execId;
    node->next = NULL;
    
    *list = node;
    
    //printf("Added %d %ld\n", orderId, execId);
    return SUCCESS;
  }
  
  node = *list;
  t_LinkedlistExecution *lastNode;
  
  while (node != NULL)
  {
    if ((node->data.execId == execId) && (node->data.orderId == orderId))
    {
      //Same data already in the list, can not add
      //printf("Duplicated fill %d %ld\n", orderId, execId);  //Remove this
      return ERROR;
    }
    lastNode = node;
    node = node->next;
  }
  
  // Data is not in the list, add it
  t_LinkedlistExecution *newNode = (t_LinkedlistExecution *) malloc(sizeof(t_LinkedlistExecution));
  newNode->data.orderId = orderId;
  newNode->data.execId = execId;
  newNode->next = NULL;
  lastNode->next = newNode;
  //printf("Added %d %ld\n", orderId, execId);    //Remove this
  return SUCCESS;
}

int AddOrderIdToList(t_LinkedlistOrderId **list, const int orderId)
{
  t_LinkedlistOrderId *node;
  
  if (*list == NULL)
  {
    node = (t_LinkedlistOrderId *) malloc(sizeof(t_LinkedlistOrderId));
    node->data.orderId = orderId;
    node->next = NULL;
    
    *list = node;
    
    //printf("Added %d\n", orderId);
    return SUCCESS;
  }
  
  node = *list;
  t_LinkedlistOrderId *lastNode;
  
  while (node != NULL)
  {
    if ((node->data.orderId == orderId))
    {
      //Same data already in the list, can not add
      //printf("Duplicated order %d\n", orderId); //Remove this
      return ERROR;
    }
    lastNode = node;
    node = node->next;
  }
  
  // Data is not in the list, add it
  t_LinkedlistOrderId *newNode = (t_LinkedlistOrderId *) malloc(sizeof(t_LinkedlistOrderId));
  newNode->data.orderId = orderId;
  newNode->next = NULL;
  lastNode->next = newNode;
  //printf("Added %d \n", orderId);   //Remove this
  return SUCCESS;
}

void DestroyExecLinkedlist(t_LinkedlistExecution **list)
{
  t_LinkedlistExecution *tmp;
  t_LinkedlistExecution *deletedNode;
  
  tmp = *list;
  while (tmp != NULL)
  {
    deletedNode = tmp;
    //printf("Delete %d %ld\n", deletedNode->data.orderId, deletedNode->data.execId); //Remove this
    tmp = tmp->next;
    free(deletedNode);
  }
  
  *list = NULL;
}

void DestroyOrderLinkedlist(t_LinkedlistOrderId **list)
{
  t_LinkedlistOrderId *tmp;
  t_LinkedlistOrderId *deletedNode;
  
  tmp = *list;
  while (tmp != NULL)
  {
    deletedNode = tmp;
    //printf("Delete %d\n", deletedNode->data.orderId); //Remove this
    tmp = tmp->next;
    free(deletedNode);
  }
  
  *list = NULL;
}
