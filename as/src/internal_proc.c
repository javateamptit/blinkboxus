/*****************************************************************************
**  Project:    Equity Arbitrage Application
**  Filename:   internal_proc.c
**  Description:  This file contains function definitions that were declared
          in internal_proc.h  
**  Author:     Luan Vo-Kinh
**  First created:  14-Sep-2007
**  Last updated: -----------
*****************************************************************************/

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include "configuration.h"
#include "raw_data_mgmt.h"
#include "output_log_mgmt.h"
#include "order_mgmt_proc.h"
#include "arca_direct_data.h"
#include "nasdaq_ouch_data.h"
#include "nasdaq_rash_data.h"
#include "bats_boe_data.h"
#include "database_util.h"
#include "internal_proc.h"
#include "query_data_mgmt.h"
#include "daedalus_proc.h"
#include "trader_tools_proc.h"
#include "trade_servers_proc.h"
#include "environment_proc.h"
#include "hbitime.h"
#include "parameters_proc.h"

/****************************************************************************
** Global variables definition
****************************************************************************/
extern t_QueryDataMgmt queryDataMgmt;
extern t_VolatileMgmt volatileMgmt;
t_HoldingEnableTradingInfo RequestToEnableTradingTS[MAX_TRADE_SERVER_CONNECTIONS][TS_MAX_ORDER_CONNECTIONS];
t_HoldingEnableTradingInfo RequestToEnableTradingPM;
/****************************************************************************
** Function declarations
****************************************************************************/

/****************************************************************************
- Function name:  ProcessRawDataManagement
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
void *ProcessRawDataManagement(void *pArgs)
{ 
  // Process current raw data
  ProcessCurrentRawData();
  
  return NULL;
}

/****************************************************************************
- Function name:  ProcessQueryDatabaseManagement
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
void *ProcessQueryDatabaseManagement(void *pArgs)
{ 
  // Process current query database
  ProcessCurrentQueryDatabase();

  return NULL;
}
/****************************************************************************
- Function name:  ProcessHistoryRawData
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessHistoryRawData(void)
{ 
  // Process history raw data of trade server
  ProcessHistoryRawDataOfTradeServer();
  TraceLog(DEBUG_LEVEL, "Finished processing history raw data of Trade Server\n" );

  // Process history raw data of aggregation server
  ProcessHistoryRawDataOfAggregationServer();
  TraceLog(DEBUG_LEVEL, "Finished processing history raw data of Aggregation Server\n" );

  // Process history raw data of PM
  ProcessHistoryRawDataOfPM();
  TraceLog(DEBUG_LEVEL, "Finished processing history raw data of PM\n" );

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessCurrentRawData
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessCurrentRawData(void)
{
  static int count = 0;
  int currentVolatileSession = VOLATILE_SESSION_PRE;
  
  while (1)
  {
    // Check and process stop trading by TWT
    CheckAndProcessStopTradingByTWT();

    // Process broken trade
    if (GetBrokenTradeData(globalDateStringOfDatabase) == ERROR)
    {
      return ERROR;
    }
    
    // Check and process hung order
    CheckAndProcessHungOrders();
    
    // Check and modify Long/Short by TWT
    CheckAndModifyLongShortByTWT();

    // Process raw data of TradeServer
    if (ProcessRawDataOfTradeServer() == ERROR)
    {
      return ERROR;
    }

    if (ProcessBBOInfo() == ERROR)
    {
      return ERROR;
    }
    
    // Process raw data of Aggregation Server
    if (ProcessRawDataOfAggregationServer() == ERROR)
    {
      return ERROR;
    }

    // Process raw data of PM
    if (ProcessRawDataOfPM() == ERROR)
    {
      return ERROR;
    }

    // Check buying power is change
    CheckChangeOfBuyingPower();
    
    if (hasChangedExposure == 1)
    {
      hasChangedExposure = 0;
      
      // Total volume record
      AddTimeSeriesTotalMarketRecords();
    }
    
    // Check time to enable/disable the BATZ book and order ports
    if (count % CHECK_BATSZ_ECN_SETTING == 0)
    {
      // Get number of seconds for current time
      time_t now = (time_t)hbitime_seconds();
      struct tm l_time;
      localtime_r(&now, &l_time);
      int currentSeconds = (l_time.tm_hour * 60 + l_time.tm_min) * 60 + l_time.tm_sec;
      
      CheckBookOrderSettingOfBATS(currentSeconds, TS_BATSZ_BOOK_INDEX);
      CheckBookOrderSettingOfBATS(currentSeconds, TS_BYX_BOOK_INDEX);
      CheckBookOrderSettingOfEDGE(currentSeconds, TS_EDGX_BOOK_INDEX);
      CheckBookOrderSettingOfEDGE(currentSeconds, TS_EDGA_BOOK_INDEX);
      CheckBookOrderSettingOfNYSE(currentSeconds, TS_NYSE_BOOK_INDEX);
      CheckBookOrderSettingOfNYSE(currentSeconds, TS_AMEX_BOOK_INDEX);
      CheckBookOrderSettingOfBX(currentSeconds, TS_NDBX_BOOK_INDEX);
      CheckBookOrderSettingOfBX(currentSeconds, TS_PSX_BOOK_INDEX);
      CheckToDisableTrading(currentSeconds);
    }
    
    /*
    Check time for route destinations
     * Before 9:30AM --> PRE-MARKET
     * 9:30AM to 4:00PM --> INTRADAY
     * After 4:00PM --> POST-MARKET
    */
    UpdateCurrentMarketTime();
    
    //Check for volatile session changes
    int tmpCurrentVolatileSession = GetCurrentVolatileSession();
    if (currentVolatileSession != tmpCurrentVolatileSession)
    {
      //volatile session has changed to a new session, we must send new volatile list to all TSs
      currentVolatileSession = tmpCurrentVolatileSession;
      TraceLog(DEBUG_LEVEL, "Volatile session has changed, AS will send new volatile list to all Trade Servers\n");
      SendActiveVolatileSymbolsToAllTS();
      SendCurrentVolatileSessionToAllDaedalus();
    }
    
    if (++count % 30 == 0)    //Auto check for volatile sections file modification
    {
      count = 0;
      int changed = 0;
      changed = CheckAndSendVolatileSectionsToAllTT();
      changed += CheckAndSendVolatileSymbolsToAllTT();
      if (changed != 0)
      {
        SendActiveVolatileSymbolsToAllTS();
      }
    }
    
    //Check for All-Volatile Toggle changes
    int isAllVolatile = GetCurrentAllVolatileToggleValue();
    if (isAllVolatile != volatileMgmt.currentAllVolatileValue)
    {
      TraceLog(DEBUG_LEVEL, "Toggle All-Volatile %s on all Trade Servers\n", (isAllVolatile == 1)?"ON":"OFF");
      volatileMgmt.currentAllVolatileValue = isAllVolatile;
      SendAllVolatileToggleStatusToAllTS();
      SendVolatileToggleToAllDaedalus();
    }
    
    // Update color for open positions
    UpdateColorForOpenPositions();
    
    // Process raw data delay in microseconds
    usleep(RAW_DATA_DELAY);
  }

  return SUCCESS;
}


/****************************************************************************
- Function name:  ProcessCurrentQueryDatabase
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessCurrentQueryDatabase(void)
{
  int iTimeChecking = 0;
  int iLastProcessedNumber = 0;
  while (1)
  {
    iTimeChecking ++;
    
    if (queryDataMgmt.positionProcessed < queryDataMgmt.countQueryDataBlock)
    {
      if (ProcessQueryDatabaseFromCollection(&queryDataMgmt.positionProcessed, &queryDataMgmt) == ERROR_BUFFER_OVER)
      {
        return ERROR;
      }
    }
    
    if (iTimeChecking * QUERY_PROCESS_DELAY >= 5000000) //5 Seconds
    {
      iTimeChecking = 0;
      if (queryDataMgmt.positionProcessed == queryDataMgmt.countQueryDataBlock)
      {
        if (iLastProcessedNumber != queryDataMgmt.positionProcessed)
        {
          iLastProcessedNumber = queryDataMgmt.positionProcessed;
          TraceLog(DEBUG_LEVEL, "Database processing status (finished): %d/%d\n", queryDataMgmt.positionProcessed, queryDataMgmt.positionProcessed);
        }
      }
    }
    
    // Process raw data delay in microseconds
    usleep(QUERY_PROCESS_DELAY);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessBrokenTrade
- Input:      
- Output:   
- Return:   
- Description:
- Usage:    
****************************************************************************/
int ProcessBrokenTrade(void)
{
  int i, count = 0;
  int fillIdList[MAX_BROKEN_TRADE];
  
  t_BrokenTradeCollection newBrokenList;
  memset(&newBrokenList, 0, sizeof(newBrokenList));
  
  for (i = 0; i < brokenTradeList.countBrokenTrade; i++)
  {
    if ((brokenTradeList.collection[i].oid != -1) && 
      (brokenTradeList.collection[i].isUpdate != BROKEN_TRADE_NONE))
    {
      count = newBrokenList.countBrokenTrade++;
      memcpy(&newBrokenList.collection[count], &brokenTradeList.collection[i], sizeof(t_BrokenTradeInfo));
      fillIdList[count] = brokenTradeList.collection[i].fillId;
    }
  }

  // Update open orders
  UpdateOpenOrders();

  // Remove broken trade
  RemoveBrokenTrade();

  if (newBrokenList.countBrokenTrade > 0)
  {
    // Re-calculate risk management
    CalculationRiskManagement(&newBrokenList);
    
    // Add Break message in OJ
    GetFillInfoOfBroken(fillIdList, newBrokenList.countBrokenTrade, globalDateStringOfDatabase);
  }  

  return SUCCESS;
}

/****************************************************************************
- Function name:  CalculationRiskManagement
- Input:      
- Output:   
- Return:   
- Description:
- Usage:    
****************************************************************************/
int CalculationRiskManagement(t_BrokenTradeCollection *newBrokenList)
{
  int i, j, symbolIndex;
  int isFound, account;
  
  int positionCanUpdate[newBrokenList->countBrokenTrade], positionCount = 0;
  memset(positionCanUpdate, 0, sizeof(positionCanUpdate));

  for (i = 0; i < newBrokenList->countBrokenTrade; i++)
  {
    symbolIndex = GetStockSymbolIndex(newBrokenList->collection[i].symbol);

    if (symbolIndex == -1)
    {
      TraceLog(ERROR_LEVEL, "Invalid symbol of broken trade, symbol = %.8s\n", newBrokenList->collection[i].symbol);
      return ERROR;
    }
    
    account = newBrokenList->collection[i].account;

    isFound = 0;
    for (j = 0; j < positionCount; j++) 
    {
      if (positionCanUpdate[j] == (account * MAX_STOCK_SYMBOL + symbolIndex))
      {
        isFound = 1;
        break;
      }
    }
    
    if (isFound == 1)
    {
      // This position was initialized
      continue;
    }
    
    positionCanUpdate[positionCount] = account * MAX_STOCK_SYMBOL + symbolIndex;
    positionCount++;
    
    isFound = 0;
    for (j = 0; j < longShortList.countSymbol; j++) 
    {
      if (strncmp(longShortList.collection[j].symbol, newBrokenList->collection[i].symbol, SYMBOL_LEN) == 0 &&
        longShortList.collection[j].account == account) 
      {
        isFound = 1;
        break;
      }
    }

    // Update countSymbol of RMList
    if (isFound == 1) 
    {
      if (longShortList.collection[j].side == 'S') 
      {
        RMList.collection[account][symbolIndex].leftShares = -1 * longShortList.collection[j].shares;
      }
      else 
      {
        RMList.collection[account][symbolIndex].leftShares = longShortList.collection[j].shares;
      }
      
      RMList.collection[account][symbolIndex].totalShares = longShortList.collection[j].shares;
      RMList.collection[account][symbolIndex].avgPrice = longShortList.collection[j].price;
      RMList.collection[account][symbolIndex].matchedPL = 0.00;
      RMList.collection[account][symbolIndex].totalFee = 0.00;
      RMList.collection[account][symbolIndex].totalMarket = fabs(longShortList.collection[j].shares * longShortList.collection[j].price);
      RMList.symbolList[symbolIndex].lastTradedPrice = longShortList.collection[j].price;
    }
    else
    {
      RMList.collection[account][symbolIndex].leftShares = 0;
      RMList.collection[account][symbolIndex].totalShares = 0;
      RMList.collection[account][symbolIndex].avgPrice = 0.00;
      RMList.collection[account][symbolIndex].matchedPL = 0.00;
      RMList.collection[account][symbolIndex].totalMarket = 0.00;
      RMList.collection[account][symbolIndex].totalFee = 0.00;
      RMList.symbolList[symbolIndex].lastTradedPrice = 0.00;
      RMList.symbolList[symbolIndex].askPrice = 0.00;
      RMList.symbolList[symbolIndex].bidPrice = 0.00;
    }   
  }

  // Get filled order for symbol list
  GetFilledOrderForSymbolList(newBrokenList, globalDateStringOfDatabase);
  
  // Update verify position list
  for (j = 0; j < positionCount; j++)
  {
    symbolIndex = positionCanUpdate[j] % MAX_STOCK_SYMBOL;
    account = positionCanUpdate[j] / MAX_STOCK_SYMBOL;
    
    UpdateVerifyPositionInfoAfterTradeBreak(account, symbolIndex);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessOutputLogManagement
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
void *ProcessOutputLogManagement(void *pArgs)
{ 
  // Process current output log
  ProcessCurrentOutputLog();

  return NULL;
}

/****************************************************************************
- Function name:  ProcessHistoryOutputLog
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessHistoryOutputLog(void)
{
  // Process hhistory trade output log
  ProcessHistoryTradeOutputLog();
  TraceLog(DEBUG_LEVEL, "Finished processing history trade output log\n" );

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessHistoryBBOInfo
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessHistoryBBOInfo(void)
{
  // Process hhistory trade output log
  ProcessHistoryBBOInfoFromFile();
  TraceLog(DEBUG_LEVEL, "Finished processing history BBO info\n" );

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessCurrentOutputLog
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessCurrentOutputLog(void)
{
  while (1)
  {
    // Process trade output log
    if (ProcessTradeOutputLog() == ERROR)
    {
      return ERROR;
    }

    // Process output log delay in microseconds
    usleep(OUTPUT_LOG_DELAY);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  UpdateCurrentMarketTime
- Input:      N/A
- Output:
- Return:     N/A
- Description:
- Usage:      N/A
****************************************************************************/
void UpdateCurrentMarketTime(void)
{
  time_t now = (time_t)hbitime_seconds();
  struct tm *local = localtime(&now);

  // PRE-MARKET
  if ( (local->tm_hour < PRE_MARKET_END_HOUR) ||
    (local->tm_hour == PRE_MARKET_END_HOUR && local->tm_min < PRE_MARKET_END_MIN)
    )
  {
    if (currentMarketTimeType != PRE_MARKET)
    {
      currentMarketTimeType = PRE_MARKET;
    }
  }
  // INTRADAY
  else if (local->tm_hour < INTRADAY_END_HOUR)
  {
    if (currentMarketTimeType != INTRADAY)
    {
      currentMarketTimeType = INTRADAY;
    }
  }
  // POST-MARKET
  else
  {
    if (currentMarketTimeType != POST_MARKET)
    {
      currentMarketTimeType = POST_MARKET;
    }
  }
  
  /*static int createBBOFile = 0;
  if (local->tm_hour == 20 && createBBOFile == 0)
  {
    createBBOFile = 1;
    
    now = (time_t)hbitime_seconds();
    int numSeconds = now;

    CreateISOFlightFilesWithDateRange(numSeconds, numSeconds);
  }*/
}

/****************************************************************************
- Function name:  InitilizeAlertDataStructure
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int InitilizeAlertDataStructure(void)
{
  double totalMatchPL = 0.0;

  int rmInfoIndex, account;
  int countSymbol = RMList.countSymbol;
  for (account = 0; account < MAX_ACCOUNT; account++)
  {
    for (rmInfoIndex = 0; rmInfoIndex < countSymbol; rmInfoIndex++)
    {
      // Total matched PL
      totalMatchPL += RMList.collection[account][rmInfoIndex].matchedPL;
    }
  }

  GTRMgt.currentSecond = 0;
  int i;
  for (i = 0; i < AlertConf.gtrSecondThreshhold; i++)
  {
    GTRMgt.gross[i] = totalMatchPL;
  }

  AlertingProgress = ENABLE;
  asCrossCollection.lastMonitorIndex = asCrossCollection.countCross;
  
  PMOrderPerSecond = 0;

  int tsIndex;
  for (tsIndex = 0; tsIndex < MAX_TRADE_SERVER_CONNECTIONS; tsIndex++)
  {
    NoTradesDurationMgt[tsIndex].currentNoTradesDurationInSecond = -1;
    NoTradesDurationMgt[tsIndex].alertThresholdInMinute = -1;

    NoTradesDurationMgt[tsIndex].startTimeStamp[0] = 0;
    NoTradesDurationMgt[tsIndex].endTimeStamp[0] = 0;
  }

  UpdateTradeTimeAlertConfig();

  return SUCCESS;
}

/****************************************************************************
- Function name:  TimerThread
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
void *TimerThread(void *pArgs)
{
  // init alert data structure
  InitilizeAlertDataStructure();
  
  // For holding enable trading flag
  memset(RequestToEnableTradingTS, 0, sizeof(RequestToEnableTradingTS));
  memset(&RequestToEnableTradingPM, 0, sizeof(RequestToEnableTradingPM));
  
  int counter = 0;
  
  while(1)
  {
    counter++;
    
    // GTR Alert
    CheckNSendGTRAlertToAllTT();

    // Buying Power Alert
    CheckNSendBuyingPowerAlertToAllTT();

    // PM hasn't exit position alert
    CheckNSendPMHasntExitPositionAlertToAllTT();

    // Unhandled open position without an order alert
    CheckNSendUnhandledOpenPositionWithoutAnOrderAlertToTT();

    // Handle PM order rate alert
    CheckNSendPMOrderRateAlertToAllTT();

    // No trade during configured period
    CheckNSendNoTradeDuringConfiguredPeriodAlertToAllTT();

    // InCompleteTrade Alert
    CheckNSendInCompleteTradeAlertToAllTT();

    // Request and Process ETB List
    CheckNRequestEtbList();
    
    // Check and request disable trading on specific TS if exceed Max Crosses
    if (counter % 60 == 0)
    {
      CheckToStopTSTradingByMaxCrossLimit();
    }

    // Check and request disable trading on specific TS if exceed Max Crosses
    if (counter % 60 == 0)
    {
      CheckToStopTSTradingByMaxCrossLimit();
    }
    
    // Monitor and request enable trading for TS and PM
    CheckNSendEnableTradingForTSAndPM();
    
    // Request Price Update
    CheckNSendRequestPriceUpdate(counter);

    usleep(TIMER_DELAY); // 1 second

  }
  return NULL;
}

/****************************************************************************
- Function name:  RequestEtbListThread
- Input:    N/A
- Output:   N/A
- Return:   N/A
- Description:  Thread to request and update ETB Symbol List from SVN
- Usage:    N/A
****************************************************************************/
void *RequestEtbListThread(void* args)
{
  char fullPath[MAX_PATH_LEN];
  int retFlag = 0;
  int countSymbol = 0;
  
  if (GetFullRawDataPath(FILE_NAME_OF_ETB_SYMBOL, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Have some problem in get full path operation (%s)\n", FILE_NAME_OF_ETB_SYMBOL);
    return NULL;
  }
  
  char shortabilityConfigPath[MAX_PATH_LEN];
  Environment_get_misc_conf_filename("shortability", shortabilityConfigPath, MAX_PATH_LEN);
  char const* query_cmd = Parameters_get(shortabilityConfigPath, "etb_query_command", "false");

  TraceLog(DEBUG_LEVEL, "Requesting ETB symbol list...\n");

  //Running command to get all ETB symbols
  char cmdline[512];
  sprintf(cmdline, "%s > %s", query_cmd, fullPath);
  int count = 0;
  while(count < 3)
  {
    if (ExecuteCommand(cmdline, 10) == SUCCESS)
    {
      TraceLog(DEBUG_LEVEL, "Requested ETB symbol list successfully\n");
      retFlag = 0;
      break;
    }
    else
    {
      TraceLog(WARN_LEVEL, "Request ETB symbol list failed, attempt to request more time...\n");
      sleep(1);
      if (count == 3)
      {
        TraceLog(ERROR_LEVEL, "Request ETB symbol list failed\n");
        retFlag = -1;
        break;
      }
    }
    
    count++;
  }

  if (retFlag == 0) //Request successfully
  {
    //Read FILE_NAME_OF_ETB_SYMBOL
    FILE *fileDesc = fopen(fullPath, "r");

    if (fileDesc == NULL)
    {
      TraceLog(ERROR_LEVEL, "Cannot open file '%s' to read\n", fullPath);
      PrintErrStr(ERROR_LEVEL, "calling fopen(): ", errno);
      return NULL;
    }

    char tempSymbol[128];
    memset(tempSymbol, 0, 128);
    
    int stockSymbolIndex;
    
    //Save the previous status of ssEnable
    char prevStatus[MAX_STOCK_SYMBOL];
    memcpy(prevStatus, SSLMgmt.ssEnable, MAX_STOCK_SYMBOL);
    memset(SSLMgmt.ssEnable, 0, MAX_STOCK_SYMBOL);
    
    // Read a stock symbol per time and store in stock symbol list
    while(fgets(tempSymbol, 128, fileDesc) != NULL)
    {
      // Remove unused characters
      RemoveCharacters(tempSymbol, strlen(tempSymbol));
      
      // First char >= 'A' and <= 'Z'
      if ((tempSymbol[0] >= 'A') &&
        (tempSymbol[0] <= 'Z'))
      {
        // Mapping stock symbols
        SLLMappingStockSymbol(tempSymbol);
  
        stockSymbolIndex = UpdateStockSymbolDetectCrossIndex(tempSymbol);
  
        if (stockSymbolIndex == -1)
        {
          TraceLog(WARN_LEVEL, "Symbol is invalid '%s'\n", tempSymbol);
          memset(tempSymbol, 0, 128);
          continue;
        }
        else
        {
          // Enable short-sell for tempSymbol
          SSLMgmt.ssEnable[stockSymbolIndex] = 1;
          countSymbol++;
        }
      }
      else
      {
        TraceLog(WARN_LEVEL, "Invalid symbol '%s'\n", tempSymbol);
      }

      memset(tempSymbol, 0, 128);
    }
    
    fclose (fileDesc);
    SSLMgmt.total = countSymbol;

    if (countSymbol > 0)
    {
      TraceLog(DEBUG_LEVEL, "Successfully request ETB Symbol List, total symbols: %d\n", countSymbol);
      SendEtbSymbolListToAllTS(prevStatus);
    }
    else 
    {
      TraceLog(ERROR_LEVEL, "No symbols found in ETB list\n");
      retFlag = -2;
    }
  }
  
  char alert[128] = "\0";
  switch (retFlag)
  {
    case 0:   //Success request with number of symbols > 0
      sprintf(alert, "AS successfully requested ETB Symbol List, total symbols: %d", countSymbol);
      BuildAndSendAlertToAllDaedalus(alert, COM__RGM__DAEDALUS__PROTO__NOTIFICATION_LEVEL_TYPE__ShowLog);
      SendNotifyMessageToAllTT(alert, 0);
      break;
    case -1:  //There is error in requesting ETB list
      sprintf(alert, "ETB Request: There was error in requesting ETB list from AS. Please re-request");
      BuildAndSendAlertToAllDaedalus(alert, COM__RGM__DAEDALUS__PROTO__NOTIFICATION_LEVEL_TYPE__ShowLog);
      SendNotifyMessageToAllTT(alert, 0);
      break;
    case -2:  //Success request with number of symbols <= 0
      sprintf(alert, "ETB Request: AS requested ETB list but there was no symbol in the list. Please re-request");
      BuildAndSendAlertToAllDaedalus(alert, COM__RGM__DAEDALUS__PROTO__NOTIFICATION_LEVEL_TYPE__ShowLog);
      SendNotifyMessageToAllTT(alert, 0);
  }
  return NULL;
}

/****************************************************************************
- Function name:  CheckNRequestEtbList
- Input:
- Output:
- Return:
- Description:
- Usage:      N/A
****************************************************************************/
int isETBLoaded = 0;
int isRequestStartOfDay = 0; //Turn on this flag if ETB Thread is created because of Start Of Day
int CheckNRequestEtbList()
{
  //Get the current Time (HH:MM)
  time_t now = (time_t)hbitime_seconds();
  struct tm *timeNow = (struct tm *) localtime(&now);
  int timeInSeconds = timeNow->tm_hour * 3600 + timeNow->tm_min * 60 + timeNow->tm_sec;
  int flag = 0;
  int etbVariance, i;
  for (i = 0; i < ShortabilitySchedule.count; i++)
  {
    etbVariance = timeInSeconds - ShortabilitySchedule.timeInSeconds[i];

    if ((etbVariance >= 0) && (etbVariance <= 3)) //Tolerance is 3 seconds
    {
      flag = 1;
    }
  }

  if ((isRequestStartOfDay == 0) ||
    ((flag == 1) && (isETBLoaded == 0)))
  {
    pthread_t requestEtbListThread;
    pthread_create(&requestEtbListThread, NULL, (void *)RequestEtbListThread, NULL);
    
    isETBLoaded = 1;
    if (isRequestStartOfDay == 0)
    {
      //Request ETB list when AS starts and turn on this flag to mark it is done
      isRequestStartOfDay = 1;
    }
  }
  else if (flag == 0)
  {
    isETBLoaded = 0;
  }
  return SUCCESS;
}

/****************************************************************************
- Function name:  SLLMappingStockSymbol
- Input:
- Output:
- Return:
- Description:
- Usage:      N/A
****************************************************************************/
int SLLMappingStockSymbol(char *stockSymbol)
{
  int i;
  for (i = 0; i < strlen(stockSymbol); i++)
  {
    // Mapping stock symbol
    // The suffix ''' -> '-'
    // The suffix '/' -> '.'
    if (stockSymbol[i] == '\'')
    {
      stockSymbol[i] = '-';
    }
    else if (stockSymbol[i] == '/')
    {
      stockSymbol[i] = '.';
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  CheckToStopTSTradingByMaxCrossLimit
- Input:
- Output:
- Return:
- Description:
- Usage:      N/A
****************************************************************************/
int CheckToStopTSTradingByMaxCrossLimit()
{
  int tsIndex, delta;
  for (tsIndex = 0; tsIndex < tradeServersInfo.maxConnections; tsIndex++)
  {
    // Check to send disable trading all ECN order
    delta = tsCrossCollection[tsIndex].countCross - tsCrossCollection[tsIndex].prevCountCross;
    
    tsCrossCollection[tsIndex].prevCountCross = tsCrossCollection[tsIndex].countCross;

    if (delta >= traderToolsInfo.globalConf.maxCross)
    {
      // Check connection between AS and TS
      if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
      {
        // Reset trading status
        DisableTradingAllVenues(tsIndex);
        continue;
      }
    
      if (SendDisableTradingAllOrderToTS(tsIndex, TS_TRADING_DISABLED_BY_MAX_CROSS_LIMIT) == ERROR)
      {
        return ERROR;
      }
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  CheckToAddSymbolToVolatileOrDisabledList
- Input:
- Output:
- Return:
- Description:
- Usage:      N/A
****************************************************************************/
int CheckToAddSymbolToVolatileOrDisabledList(char *symbol, int symbolIndex, int tsIndex)
{
  // If a symbol loses >= volatile threshold --> add it to volatile list until it is manually removed
  // The same logic is applied for disabled threshold
  // Disabled threshold >= volatile threshold

  int isVolatile = NO, isDisabled = NO;
  if (tsIndex > -1 && tsIndex < MAX_TRADE_SERVER_CONNECTIONS)
  {
    if (fge(ProfitLossPerSymbol[symbolIndex].tsLoss[tsIndex], traderToolsInfo.globalConf.tsDisabledThreshold))
    {
      isDisabled = YES;
    }
    else if (fge(ProfitLossPerSymbol[symbolIndex].tsLoss[tsIndex], traderToolsInfo.globalConf.tsVolatileThreshold))
    {
      isVolatile = YES;
    }
  }
  
  if (isDisabled == YES ||
    fge(ProfitLossPerSymbol[symbolIndex].asLoss, traderToolsInfo.globalConf.asDisabledThreshold))
  {
    // Add symbol to disabled list
    // Check this symbol is in list or not
    if (ProfitLossPerSymbol[symbolIndex].listID == DISABLED_LIST_ID)
    {
      return SUCCESS;
    }
    
    // Add this symbol to Disabled Symbol List
    int i, countSymbol;
    char tmpSymbolList[MAX_STOCK_SYMBOL][SYMBOL_LEN];
    memset(tmpSymbolList, 0, sizeof(tmpSymbolList));
    
    countSymbol = 0;
    memcpy(tmpSymbolList[countSymbol], symbol, SYMBOL_LEN);
    countSymbol++;
    
    for (i = 0; i < IgnoreStockList.countStock; i++)
    {
      memcpy(tmpSymbolList[countSymbol], IgnoreStockList.stockList[i], SYMBOL_LEN);
      countSymbol++;
    }

    // Update ignored symbol list
    IgnoreStockList.countStock = 0;

    for (i = 0; i < countSymbol; i++)
    {
      memcpy(IgnoreStockList.stockList[IgnoreStockList.countStock], tmpSymbolList[i], SYMBOL_LEN);
      IgnoreStockList.countStock++;
    }

    // Save data to the file
    SaveIgnoreStockList();

    // Send ignored symbol list to all Trade Server
    SendIgnoreStockListToAllTS(ADD_SYMBOL, symbol);

    // Send ignored symbol list to all Trader Tool
    SendTSIgnoreSymbolListToAllDaedalus();
    SendIgnoreStockListToAllTT();
    
    // Send Alerting to TT
    char reason[160] = "\0";
    if (fge(ProfitLossPerSymbol[symbolIndex].asLoss, traderToolsInfo.globalConf.asDisabledThreshold))
    {
      sprintf(reason, "%.8s loses more than %.2lf on AS, added it to disabled list\n",
        symbol, traderToolsInfo.globalConf.asDisabledThreshold);
    }
    else
    {
      sprintf(reason, "%.8s loses more than %.2lf on TS, added it to disabled list\n",
        symbol, traderToolsInfo.globalConf.tsDisabledThreshold);
    }
    
    BuildNSendDisabledSymbolAlertToAllDaedalus(reason);
    BuildNSendDisabledSymbolAlertToAllTT(reason);
    ProfitLossPerSymbol[symbolIndex].listID = DISABLED_LIST_ID;
  }
  else if (isVolatile == YES ||
      fge(ProfitLossPerSymbol[symbolIndex].asLoss, traderToolsInfo.globalConf.asVolatileThreshold))
  {
    // Add symbol to volatile list
    // Check this symbol is in list or not
    if (ProfitLossPerSymbol[symbolIndex].listID >= VOLATILE_LIST_ID)
    {
      return SUCCESS;
    }
    
    char list[MAX_STOCK_SYMBOL * SYMBOL_LEN];
    int count = 0;
    int changed = 1;
    
    pthread_mutex_lock(&volatileMgmt.mutexLock);
    if (LoadVolatileSymbolsFromFileToList(list, &count) == ERROR)
    {
      pthread_mutex_unlock(&volatileMgmt.mutexLock);
      return SUCCESS;
    }
    
    int index;
    for (index = 0; index < count; index++)
    {
      if (strncmp(&list[index * SYMBOL_LEN], symbol, SYMBOL_LEN) == 0)
      {
        // Symbol is already in the list
        changed = 0;
        break;
      }
    }
    
    if (changed == 1)
    {
      strncpy(&list[count * SYMBOL_LEN], symbol, SYMBOL_LEN);
      count++;
      
      SaveVolatileSymbolListToFile(list, count);
    }
    pthread_mutex_unlock(&volatileMgmt.mutexLock);
    
    if (changed == 1)
    {
      SendVolatileSymbolListToAllDaedalus();
      SendVolatileSymbolListToAllTT();
      
      // Send Alert to TT
      char reason[160] = "\0";
      if (fge(ProfitLossPerSymbol[symbolIndex].asLoss, traderToolsInfo.globalConf.asVolatileThreshold))
      {
        sprintf(reason, "%.8s loses more than %.2lf on AS, added it to volatile list\n",
          symbol, traderToolsInfo.globalConf.asVolatileThreshold);
      }
      else
      {
        sprintf(reason, "%.8s loses more than %.2lf on TS, added it to volatile list\n",
          symbol, traderToolsInfo.globalConf.tsVolatileThreshold);
      }
      
      BuildNSendDisabledSymbolAlertToAllDaedalus(reason);
      BuildNSendDisabledSymbolAlertToAllTT(reason);
      ProfitLossPerSymbol[symbolIndex].listID = VOLATILE_LIST_ID;
    }
  }

  return SUCCESS;
}

int CheckNSendEnableTradingForTSAndPM()
{
  //Check to send enable trading for TS
  int tsIndex, orderIndex, ttIndex;
  for (tsIndex = 0; tsIndex < MAX_TRADE_SERVER_CONNECTIONS; tsIndex++)
  {
    for (orderIndex = 0; orderIndex < TS_MAX_ORDER_CONNECTIONS; orderIndex++)
    {
      if (RequestToEnableTradingTS[tsIndex][orderIndex].isEnabled == 1 &&
        GetOrderConnectionStatusOfTS(tsIndex, orderIndex) == CONNECTED)
      {
        TraceLog(DEBUG_LEVEL, "Request to enable trading ts%d orderIndex=%d\n", tsIndex, orderIndex);
        // Send enable trading
        if (SendEnableTradingAOrderToTS(tsIndex, orderIndex) == SUCCESS)
        {
          RequestToEnableTradingTS[tsIndex][orderIndex].isEnabled = 0;
          ttIndex = RequestToEnableTradingTS[tsIndex][orderIndex].ttIndex;
          
          //Update enable trading on a ECN odrder of TS event log to database
          char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
          sprintf(activityDetail, "%d,0,%d%02d", traderToolsInfo.connection[ttIndex].currentState, tsIndex, orderIndex + 1);
          ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_ENABLE_DISENABLE_TRADING, activityDetail);
        }
      }
    }
  }
  
  //Check to send enable trading for PM: condition: at least 1 order and 1 book is connected
  if ((pmInfo.currentStatus.pmOecStatus[PM_NASDAQ_RASH_INDEX] == CONNECTED ||
    pmInfo.currentStatus.pmOecStatus[PM_ARCA_DIRECT_INDEX] == CONNECTED)
    &&
    (pmInfo.currentStatus.pmMdcStatus[PM_ARCA_BOOK_INDEX] == CONNECTED ||
    pmInfo.currentStatus.pmMdcStatus[PM_NASDAQ_BOOK_INDEX] == CONNECTED)
    &&
    RequestToEnableTradingPM.isEnabled == 1)
  {
    TraceLog(DEBUG_LEVEL, "Request to enable trading PM\n");
    // Call function to enable PM to PM
    if (SendEnableTradingPMToPM() == SUCCESS)
    {
      RequestToEnableTradingPM.isEnabled = 0;
      ttIndex = RequestToEnableTradingPM.ttIndex;
      //Update enable trading event log to database
      char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
      sprintf(activityDetail, "%d,0,99", traderToolsInfo.connection[ttIndex].currentState);
      ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_ENABLE_DISENABLE_TRADING, activityDetail);
    }
  }
  return SUCCESS;
}

int GetOrderConnectionStatusOfTS(int tsIndex, int orderIndex)
{
  if(orderIndex < 0 || orderIndex >= TS_MAX_ORDER_CONNECTIONS)
  {
    return DISCONNECTED;
  }
  
  return tradeServersInfo.currentStatus[tsIndex].tsOecStatus[orderIndex];
}

int GetBookConnectionStatusOfTS(int tsIndex, int bookIndex)
{
  if(bookIndex < 0 || bookIndex >= TS_MAX_BOOK_CONNECTIONS)
  {
    return DISCONNECTED;
  }
  
  return tradeServersInfo.currentStatus[tsIndex].tsMdcStatus[bookIndex];
}

int GetTradingStatusOfTSOrder(int tsIndex, int orderIndex)
{
  if(orderIndex < 0 || orderIndex >= TS_MAX_ORDER_CONNECTIONS)
  {
    return DISCONNECTED;
  }
  
  return tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[orderIndex];
}

char* GetMdcNameFromMdcType(Com__Rgm__Daedalus__Proto__MDC mdcType)
{
  switch (mdcType)
  {
    case COM__RGM__DAEDALUS__PROTO__MDC__ARCA_MDC:
      return "ARCA";
    case COM__RGM__DAEDALUS__PROTO__MDC__NASDAQ_MDC:
      return "NASDAQ";
    case COM__RGM__DAEDALUS__PROTO__MDC__NYSE_MDC:
      return "NYSE";
    case COM__RGM__DAEDALUS__PROTO__MDC__BZX_MDC:
      return "BATSZ";
    case COM__RGM__DAEDALUS__PROTO__MDC__EDGX_MDC:
      return "EDGX";
    case COM__RGM__DAEDALUS__PROTO__MDC__EDGA_MDC:
      return "EDGA";
    case COM__RGM__DAEDALUS__PROTO__MDC__BX_MDC:
      return "NDBX";
    case COM__RGM__DAEDALUS__PROTO__MDC__BYX_MDC:
      return "BYX";
    case COM__RGM__DAEDALUS__PROTO__MDC__PSX_MDC:
      return "PSX";
    case COM__RGM__DAEDALUS__PROTO__MDC__CQS_MDC:
      return "CQS";
    case COM__RGM__DAEDALUS__PROTO__MDC__CTS_MDC:
      return "CTS";
    case COM__RGM__DAEDALUS__PROTO__MDC__UQDF_MDC:
      return "UQDF";
    case COM__RGM__DAEDALUS__PROTO__MDC__UTDF_MDC:
      return "UTDF";
    case COM__RGM__DAEDALUS__PROTO__MDC__ALL_MDC:
      return "All MDC";
    default:
      return "UNSUPPORTED MDC";
  }
  return "UNSUPPORTED MDC";
}

char* GetOecNameFromOecType(Com__Rgm__Daedalus__Proto__OEC oecType)
{
  switch (oecType)
  {
    case COM__RGM__DAEDALUS__PROTO__OEC__ARCA_DIRECT_OEC:
      return "ARCA DIRECT";
    case COM__RGM__DAEDALUS__PROTO__OEC__ARCA_FIX_OEC:
      return "ARCA FIX";
    case COM__RGM__DAEDALUS__PROTO__OEC__RASH_OEC:
      return "NASDAQ RASH";
    case COM__RGM__DAEDALUS__PROTO__OEC__NASDAQ_OEC:
      return "NASDAQ OUCH";
    case COM__RGM__DAEDALUS__PROTO__OEC__NYSE_OEC:
      return "NYSE CCG";
    case COM__RGM__DAEDALUS__PROTO__OEC__BZX_OEC:
      return "BATSZ BOE";
    case COM__RGM__DAEDALUS__PROTO__OEC__EDGX_OEC:
      return "EDGX DIRECT";
    case COM__RGM__DAEDALUS__PROTO__OEC__EDGA_OEC:
      return "EDGA DIRECT";
    case COM__RGM__DAEDALUS__PROTO__OEC__BX_OEC:
      return "NASDAQ OUCH BX";
    case COM__RGM__DAEDALUS__PROTO__OEC__BYX_OEC:
      return "BYX BOE";
    case COM__RGM__DAEDALUS__PROTO__OEC__PSX_OEC:
      return "PSX";
    case COM__RGM__DAEDALUS__PROTO__OEC__ALL_OEC:
      return "All OEC";
    default:
      return "UNSUPPORTED OEC";
  }
  return "UNSUPPORTED OEC";
}

char* GetExchangeNameFromCode(char exchangeCode)
{
  switch (exchangeCode)
  {
    case 'P':
      return "NYSE Arca Exchange";
    case 'Q':
      return "Nasdaq Stock Exchange";
    case 'N':
      return "NYSE Euronex";
    case 'Z':
      return "BATS Exchange Inc";
    case 'K':
      return "EDGX Exchange, Inc";
    case 'J':
      return "EDGA Exchange, Inc";
    case 'B':
      return "Boston Stock Exchange";
    case 'C':
      return "National Stock Exchange";
    case 'D':
      return "FINRA ADF";
    case 'E':
      return "Market Independent (Generated by SIP)";
    case 'I':
      return "International Securities Exchange";
    case 'M':
      return "Chicago Stock Exchange";
    case 'A':
      return "NYSE Amex";
    case 'W':
      return "Chicago Board Options Exchange";
    case 'X':
      return "Philadelphia Stock Exchange";
    case 'Y':
      return "BATS Y-Exchange, Inc";
  }
  return "";
}

/***************************************************************************/
