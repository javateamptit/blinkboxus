/*****************************************************************************
**  Project:    Equity Arbitrage Application
**  Filename:   order_mgmt_proc.c
**  Description:  This file contains function definitions that were declared
          in order_mgmt_proc.h  
**  Author:     Luan Vo-Kinh
**  First created:  13-Sep-2007
**  Last updated: -----------
*****************************************************************************/

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#define _GNU_SOURCE
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>      
#include <math.h>

#include "configuration.h"
#include "nasdaq_rash_proc.h"
#include "arca_direct_proc.h"
#include "arca_direct_data.h"
#include "order_mgmt_proc.h"
#include "output_log_mgmt.h"
#include "daedalus_proc.h"
#include "trader_tools_proc.h"
#include "trade_servers_proc.h"
#include "position_manager_proc.h"
#include "nasdaq_rash_data.h"
#include "edge_order_data.h"
#include "hbitime.h"
#include "logging.h"

/****************************************************************************
** Global variables definition
****************************************************************************/
t_OrderPlacementStatus orderStatus;

t_OrderStatusIndex orderStatusIndex;

tFieldIndex fieldIndexList;

t_ASOpenOrderMgmt asOpenOrder;

t_ASOrderDetailMgmt asOrderDetail;

t_RiskManagementCollection RMList;

t_ProfitLossPerSymbol ProfitLossPerSymbol[MAX_SYMBOL_TRADE];

t_LongShortCollection longShortList;

t_VerifyPositionInfo verifyPositionInfo[MAX_ACCOUNT][MAX_STOCK_SYMBOL];

t_ManualOrderInfo manualOrderInfo[MAX_MANUAL_ORDER];
t_PMOrderInfo pmOrderInfo[MAX_ORDER_PLACEMENT];
t_ARCADIRECTOrderInfo arcaDirectOrderInfo[MAX_MANUAL_ORDER];

t_ARCADIRECTOrderInfo ManualArcaDirectOrderInfo[MAX_MANUAL_ORDER];
t_ARCADIRECTOrderInfo PMArcaDirectOrderInfo[MAX_ORDER_PLACEMENT];

short symboIndexlList[MAX_TYPE_CHAR][MAX_TYPE_CHAR][MAX_TYPE_CHAR][MAX_TYPE_CHAR][MAX_TYPE_CHAR][2];

// danhth added to handle the six-chars-symbol
short longSymbolIndexArray[MAX_STOCK_SYMBOL][MAX_TYPE_CHAR][MAX_TYPE_CHAR][2];
short extraLongSymbolIndexArray[MAX_STOCK_SYMBOL][MAX_TYPE_CHAR][2];

int orderIdIndexList[MAX_TRADE_SERVER_CONNECTIONS + 1][MAX_ORDER_ID_RANGE];

t_BrokenTradeCollection brokenTradeList;

t_FastMarketManagement FastMarketMgmt;

static int enableBATSZSetting = NO;
static int disableBATSZSetting = NO;
static int autoEnableBATSZTrading = NO;

static int enableBATSYSetting = NO;
static int disableBATSYSetting = NO;
static int autoEnableBATSYTrading = NO;

static int enableEDGXSetting = NO;
static int disableEDGXSetting = NO;
static int autoEnableEDGXTrading = NO;

static int enableEDGASetting = NO;
static int disableEDGASetting = NO;
static int autoEnableEDGATrading = NO;

static int enableNYSESetting = NO;
static int disableNYSESetting = NO;

static int enableAMEXSetting = NO;
static int disableAMEXSetting = NO;

static int enableBXSetting = NO;
static int enablePSXSetting = NO;

int AutoDisableTrading[TS_MAX_ORDER_CONNECTIONS];

char Manual_Order_Account_Mapping[MAX_ORDER_PLACEMENT];

t_VolatileMgmt volatileMgmt;

t_DisableRejectMgmt DisableRejectMgmt[MAX_ORDER_TYPE];
t_HaltStatusMgmt HaltStatusMgmt;

// Last index is PM & AS
unsigned long ExchangeOrderIdMapping[MAX_TRADE_SERVER_CONNECTIONS + 1][MAX_ORDER_PLACEMENT];

t_AutoCancelOrder AutoCancelOrder;

/****************************************************************************
** Function declarations
****************************************************************************/
void InitExchangeOrderIdMapping(void)
{
  int tsIndex, i;
  for (tsIndex = 0; tsIndex <= MAX_TRADE_SERVER_CONNECTIONS; tsIndex++)
  {
    for (i = 0; i < MAX_ORDER_PLACEMENT; i++)
    {
      ExchangeOrderIdMapping[tsIndex][i] = 0;
    }
  }
}
/****************************************************************************
- Function name:  InitializeManualOrderAccountMapping
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
void InitializeManualOrderAccountMapping(void)
{
  int i;
  for (i=0; i<MAX_ORDER_PLACEMENT; i++)
  {
    Manual_Order_Account_Mapping[i] = -1;
  }
}

/****************************************************************************
- Function name:  InitializeFastMarketMgmtStructure
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
void InitializeFastMarketMgmtStructure(void)
{
  FastMarketMgmt.thresHold = 999999;
  FastMarketMgmt.currentOrdersPerSec = 0;
  FastMarketMgmt.isCheck = -1;
  FastMarketMgmt.mode = NORMAL_MARKET_MODE;
}

/****************************************************************************
- Function name:  ResetFastMarketMgmtValues
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
void ResetFastMarketMgmtValues(void)
{
  // We reset some values of FastMarketMgmt
  // If we do not reset, MarketMode may currently be in FAST_MARKET_MODE state
  // because when AS load rawdata from file, it changes currentOrdersPerSec value
  // So once, AS loaded all rawdata, we should reset this to 0
  // Do not reset FastMarketMgmt.thresHold, since this is loaded from config file before!
  
  FastMarketMgmt.currentOrdersPerSec = 0;
  FastMarketMgmt.isCheck = -1;
  FastMarketMgmt.mode = NORMAL_MARKET_MODE;
}

/****************************************************************************
- Function name:  InitializeASOpenOrderStructures
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int InitializeASOpenOrderStructures(void)
{
  int i, j;

  asOpenOrder.countOpenOrder = 0;
  for (i = 0; i < MAX_ORDER_PLACEMENT; i++)
  {
    memset(&asOpenOrder.orderCollection[i].orderSummary, 0, sizeof(t_OpenOrderInfo));
    asOpenOrder.orderCollection[i].isNewOrder = -1;
    asOpenOrder.orderCollection[i].isBroken = BROKEN_TRADE_NONE;
    asOpenOrder.orderCollection[i].countOrderDetail = 0;
    asOpenOrder.orderCollection[i].lifeTimeInSecond = -1;
    asOpenOrder.orderCollection[i].canceledShares = 0;
    asOpenOrder.orderCollection[i].orderSummary.account = -1;

    memset(asOpenOrder.orderCollection[i].isUpdated, 0, MAX_TRADER_TOOL_CONNECTIONS);
    memset(asOpenOrder.orderCollection[i].countSent, 0, MAX_TRADER_TOOL_CONNECTIONS);
    
    for (j = 0; j < MAX_ORDER_DETAIL; j++)
    {
      asOpenOrder.orderCollection[i].indexOrderDetailList[j] = -1;
    }
  } 

  memset(&asOrderDetail, OPEN_ORDER_SENT, sizeof(t_ASOrderDetailMgmt));

  return 0;
}

/****************************************************************************
- Function name:  InitializeOrderStatus
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int InitializeOrderStatus(void)
{
  memset(&orderStatus, 0, sizeof(t_OrderStatus));

  strcpy(orderStatus.Sent, "Sent");
  strcpy(orderStatus.Live, "Live");
  strcpy(orderStatus.Rejected, "Rejected");
  strcpy(orderStatus.Closed, "Closed"); 
  strcpy(orderStatus.BrokenTrade, "BrokenTrade");
  strcpy(orderStatus.CanceledByECN, "CanceledByECN"); 
  strcpy(orderStatus.CXLSent, "CXLSent");
  strcpy(orderStatus.CancelACK, "CancelACK");
  strcpy(orderStatus.CancelRejected, "CancelRejected"); 
  strcpy(orderStatus.CancelPending, "CancelPending"); 
  strcpy(orderStatus.CanceledByUser, "CanceledByUser");

  return 0;
}

/****************************************************************************
- Function name:  InitializeOrderStatusIndex
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int InitializeOrderStatusIndex(void)
{
  //Be carefull when change these number, it is also used for mapping table of Order Status Type between AS and Daedalus
  orderStatusIndex.Sent = 0;
  orderStatusIndex.Live = 1;
  orderStatusIndex.Rejected = 2;
  orderStatusIndex.Closed = 3;
  orderStatusIndex.BrokenTrade = 4;
  orderStatusIndex.CanceledByECN = 5;
  orderStatusIndex.CXLSent = 6;
  orderStatusIndex.CancelACK = 7;
  orderStatusIndex.CancelRejected = 8;
  orderStatusIndex.CancelPending = 9;
  orderStatusIndex.CanceledByUser = 10;

  return 0;
}

/****************************************************************************
- Function name:  InitializeFieldIndexList
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int InitializeFieldIndexList(void)
{
  fieldIndexList.BBOWeightIndicator = -104;
  fieldIndexList.SpecialOrderType = -103;
  fieldIndexList.DiscretionOffset = -102;
  fieldIndexList.SymbolSuffix = -101;
  fieldIndexList.RouteOutEligibility = -100;
  fieldIndexList.RoutingDeliveryMethod = -99;
  fieldIndexList.ExtendedHrsEligible = -98;
  fieldIndexList.ExpireTime = -97;
  
  /*WARNING: DO NOT CHANGE THESE VALUES, THIS AFFECTS OTHER PLACES*/
  fieldIndexList.ProactiveIfLocked = -96;
  fieldIndexList.ExtendedPNP = -95;
  fieldIndexList.ExtExeInstruction = -94;
  fieldIndexList.ArcaExID = -93;
  fieldIndexList.SendingTime = -92;
  //BATSZ BOE
  fieldIndexList.OrgTime = -91;
  fieldIndexList.WorkingPrice = -90;
  fieldIndexList.DisplayPrice = -89;
  fieldIndexList.AttributeQuote = -88;
  fieldIndexList.LocateReqd = -87;
  fieldIndexList.PreventMemberMatch = -86;
  fieldIndexList.MaxRemovePct = -85;
  fieldIndexList.SymbolSfx = -84;
  fieldIndexList.LastPx = -83;
  fieldIndexList.MatchUnit = -82;
  fieldIndexList.LastShares = -81;
  fieldIndexList.SubLiquidityIndicator = -80;
  fieldIndexList.AccessFee = -79;
  fieldIndexList.ClrAccount = -78;
  fieldIndexList.ClrFirm = -77;
  fieldIndexList.RoutInst = -76;

  //NYSE CCG
  fieldIndexList.ExecRefID = -75;
  fieldIndexList.OrgShares = -74;
  fieldIndexList.LeavesShares = -73;
  fieldIndexList.DBExecID = -72;
  fieldIndexList.DOTReserve = -71;
  fieldIndexList.BillingIndicator = -70;
  fieldIndexList.LastMarket = -69;
  fieldIndexList.ContraBroker = -68;
  fieldIndexList.ContraTrader = -67;
  fieldIndexList.ExecAwayMktID = -66;
  fieldIndexList.BillingRate = -65;
  fieldIndexList.InfoCode = -64;

  // arca direct 4.0
  fieldIndexList.ISO_Flag = -63;
  fieldIndexList.ArcaVariant = -62;
  fieldIndexList.ExDestination = -61;
  fieldIndexList.NoSelfTrade = -60;
  
  // ouch 4.0
  fieldIndexList.OrderState = -59;
  fieldIndexList.CrossType = -58;
  fieldIndexList.MinQuantity = -57;
  
  // The existing fields in current system (before AD 4.0 and OUCH 4.0 update)
  fieldIndexList.Eligibility = -56;
  fieldIndexList.Capacity = -55;
  fieldIndexList.SenderSubID = -54;
  fieldIndexList.ExecBroker = -53;
  fieldIndexList.RandomReserve = -52;
  fieldIndexList.Rule80AIndicator = -51;
  fieldIndexList.MsgType = -50;
  fieldIndexList.Length = -49;
  fieldIndexList.InSeqNum = -48;
  fieldIndexList.OutSeqNum = -47;
  fieldIndexList.ClOrdID = -46;
  fieldIndexList.PCSLinkID = -45;
  fieldIndexList.OrdQty = -44;
  fieldIndexList.PriceField = -43;
  fieldIndexList.PriceScale = -42;
  fieldIndexList.Symbol = -41;
  fieldIndexList.CompGroupID  = -40;
  fieldIndexList.DeliverToCompID = -39;
  fieldIndexList.ExecInst = -38;
  fieldIndexList.Side = -37;
  fieldIndexList.OrdType = -36;
  fieldIndexList.Time_In_Force = -35;
  fieldIndexList.Rule80A = -34;
  fieldIndexList.TradingSessionID = -33;
  fieldIndexList.Account = -32;
  fieldIndexList.ArcaOrderID = -31;
  fieldIndexList.TransactionTime = -30;
  fieldIndexList.ExecID = -29;
  fieldIndexList.LiquidityIndicator = -28;
  fieldIndexList.SendDate = -27;
  fieldIndexList.OriginalClientOrdID = -26;
  fieldIndexList.RejectText = -25;
  fieldIndexList.Type = -24;  
  fieldIndexList.DiscretionPegDifference = -23;
  fieldIndexList.DiscretionPegDifferenceSign = -22;
  fieldIndexList.DiscretionPegType = -21;
  fieldIndexList.DiscretionPrice = -20;
  fieldIndexList.PegDifference = -19;
  fieldIndexList.PegDifferenceSign = -18;
  fieldIndexList.PegType = -17;
  fieldIndexList.MaxFloor = -16;
  fieldIndexList.MinQty = -15;
  fieldIndexList.TimeStamp = -14;
  fieldIndexList.MessageType = -13;
  fieldIndexList.Token = -12;
  fieldIndexList.Action = -11;
  fieldIndexList.Shares = -10;
  fieldIndexList.Stock = -9;
  fieldIndexList.Price = -8;
  fieldIndexList.TimeInForce = -7;
  fieldIndexList.Firm = -6;
  fieldIndexList.Display = -5;
  fieldIndexList.OrderRefNo = -4;
  fieldIndexList.Reason = -3;
  fieldIndexList.LiquidityFlag = -2;
  fieldIndexList.MatchNo = -1;

  return 0;
}

/****************************************************************************
- Function name:  InitializeRiskManagementStructure
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int InitializeRiskManagementStructure(void)
{
  int i, account;

  RMList.countSymbol = 0;
  for (account=0; account < MAX_ACCOUNT; account++)
  {
    for (i = 0; i < MAX_SYMBOL_TRADE; i++)
    {   
      memset(RMList.collection[account][i].oldPositionSent, 0, sizeof(t_PositionBk) * MAX_TRADER_TOOL_CONNECTIONS);

      RMList.collection[account][i].leftShares = 0;
      RMList.collection[account][i].totalShares = 0;
      RMList.collection[account][i].timestamp = 0;
      memset(RMList.collection[account][i].positionKey, 0, sizeof(RMList.collection[account][i].positionKey));
      // RMList.collection[account][i].prevPositionState = 0;
      RMList.collection[account][i].avgPrice = 0.00;
      RMList.collection[account][i].matchedPL = 0.00;
      RMList.collection[account][i].totalMarket = 0.00;
      RMList.collection[account][i].totalFee = 0.00;
    }
  }
  
  for (i = 0; i < MAX_SYMBOL_TRADE; i++)
  {
    memset(RMList.symbolList[i].symbol, 0, SYMBOL_LEN);
    RMList.symbolList[i].lastTradedPrice = 0.00;
    RMList.symbolList[i].askPrice = 0.00;
    RMList.symbolList[i].bidPrice = 0.00;
  }

  return 0;
}

/****************************************************************************
- Function name:  InitializeLongShortStructure
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int InitializeProfitLossPerSymbolStructure(void)
{
  memset(ProfitLossPerSymbol, 0, sizeof(ProfitLossPerSymbol));
  return 0;
}

/****************************************************************************
- Function name:  InitializeLongShortStructure
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int InitializeLongShortStructure(void)
{
  memset(&longShortList, 0, sizeof(longShortList));
  
  return 0;
}

/****************************************************************************
- Function name:  InitializeSymbolIndexList
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int InitializeSymbolIndexList(void)
{
  int i, j, k, n, m;

  for (i = 0; i < MAX_TYPE_CHAR; i++)
  {
    for (j = 0; j < MAX_TYPE_CHAR; j++)
    {
      for (k = 0; k < MAX_TYPE_CHAR; k++)
      {
        for (m = 0; m < MAX_TYPE_CHAR; m++)
        {
          for (n = 0; n < MAX_TYPE_CHAR; n++)
          {
            //Index of each stock symbol now will be -1, meaning 
            //'not a valid index yet'
            symboIndexlList[i][j][k][m][n][SYMBOL_FOR_NEW_ORDER] = -1;
            symboIndexlList[i][j][k][m][n][SYMBOL_FOR_DETECT_CROSS] = -1;
          }
        }
      }
    }
  }
  
  for (i = 0; i < MAX_STOCK_SYMBOL; i++)
  {
    for(j = 0; j < MAX_TYPE_CHAR; j++)
    {
      for (k = 0; k < MAX_TYPE_CHAR; k++)
      {
        longSymbolIndexArray[i][j][k][SYMBOL_FOR_NEW_ORDER] = -1;
        longSymbolIndexArray[i][j][k][SYMBOL_FOR_DETECT_CROSS] = -1;
      }
    }
  }
  
  for (i = 0; i < MAX_STOCK_SYMBOL; i++)
  {
    for(j = 0; j < MAX_TYPE_CHAR; j++)
    {
      extraLongSymbolIndexArray[i][j][SYMBOL_FOR_NEW_ORDER] = -1;
      extraLongSymbolIndexArray[i][j][SYMBOL_FOR_DETECT_CROSS] = -1;
    }
  }
  
  return 0;
}

/****************************************************************************
- Function name:  InitilizeOrderIdIndex
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int InitilizeOrderIdIndexList(void)
{
  int i, j;

  for (i = 0; i < MAX_TRADE_SERVER_CONNECTIONS + 1; i++)
  {
    for (j = 0; j < MAX_ORDER_ID_RANGE; j++)
    {
      orderIdIndexList[i][j] = -1;
    }   
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  InitializeBrokenTrade
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int InitializeBrokenTrade(void)
{
  int i;

  brokenTradeList.countBrokenTrade = 0;

  for (i = 0; i < MAX_BROKEN_TRADE; i++)
  {
    brokenTradeList.collection[i].isUpdate = BROKEN_TRADE_DELETE;
    brokenTradeList.collection[i].oid = -1;
    brokenTradeList.collection[i].execId = -1;
    brokenTradeList.collection[i].newShares = 0;
    brokenTradeList.collection[i].newPrice = 0.00;
    brokenTradeList.collection[i].brokenShares = 0;
    brokenTradeList.collection[i].clOrdId = -1;
    brokenTradeList.collection[i].tsId = -1;
    brokenTradeList.collection[i].account = -1;

    memset(brokenTradeList.collection[i].symbol, 0, SYMBOL_LEN);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  InitializeVerifyPositionInfoStructure
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int InitializeVerifyPositionInfoStructure(void)
{
  int symbolIndex;
  int account;
  for (account = 0; account < MAX_ACCOUNT; account++)
  {
    for (symbolIndex = 0; symbolIndex < MAX_STOCK_SYMBOL; symbolIndex++)
    {   
      verifyPositionInfo[account][symbolIndex].shares = 0;
      verifyPositionInfo[account][symbolIndex].side = -1;
      verifyPositionInfo[account][symbolIndex].isTrading = TRADING_NONE;
      verifyPositionInfo[account][symbolIndex].isPMOwner = NO;
      verifyPositionInfo[account][symbolIndex].isSleeping = NO;
      verifyPositionInfo[account][symbolIndex].lockCounter = NO_LOCK;
      verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = -1;
      verifyPositionInfo[account][symbolIndex].alreadyAlerted = YES;
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  InitializeManualOrderInfoStructure
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int InitializeManualOrderInfoStructure(void)
{
  int orderId;
  for (orderId = 0; orderId < MAX_MANUAL_ORDER; orderId++)
  { 
    memset(manualOrderInfo[orderId].symbol, 0, SYMBOL_LEN);
    manualOrderInfo[orderId].filledShares = 0;
    manualOrderInfo[orderId].leftShares = 0;
    manualOrderInfo[orderId].side = -1;
    memset(manualOrderInfo[orderId].username, 0, 31);
    
    memset(ManualArcaDirectOrderInfo[orderId].symbol, 0, SYMBOL_LEN);
    ManualArcaDirectOrderInfo[orderId].shares = 0;
    ManualArcaDirectOrderInfo[orderId].side = -1;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  InitializePMOrderInfoStructure
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int InitializePMOrderInfoStructure(void)
{
  int orderIndex;
  for (orderIndex = 0; orderIndex < MAX_ORDER_PLACEMENT; orderIndex++)
  { 
    memset(pmOrderInfo[orderIndex].symbol, 0, SYMBOL_LEN);
    pmOrderInfo[orderIndex].filledShares = 0;
    pmOrderInfo[orderIndex].leftShares = 0;
    pmOrderInfo[orderIndex].side = -1;
    
    memset(PMArcaDirectOrderInfo[orderIndex].symbol, 0, SYMBOL_LEN);
    PMArcaDirectOrderInfo[orderIndex].shares = 0;
    PMArcaDirectOrderInfo[orderIndex].side = -1;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  InitializeVolatileSymbolList
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int InitializeVolatileSymbolList(void)
{
  volatileMgmt.sectionFileTimeStamp = 0;
  volatileMgmt.listFileTimeStamp = 0;

  pthread_mutex_init (&volatileMgmt.mutexLock, NULL);
  
  volatileMgmt.currentAllVolatileValue = 0; //0:off, 1:on
  volatileMgmt.numAllVolatileToggle = 0;
  
  pthread_mutex_lock(&volatileMgmt.mutexLock);
    LoadAllVolatileToggleSchedulesFromFile();
  pthread_mutex_unlock(&volatileMgmt.mutexLock);
  return SUCCESS;
}

/****************************************************************************
- Function name:  GetStockSymbolIndex
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int GetStockSymbolIndex(const char *stockSymbol)
{
  //TraceLog(DEBUG_LEVEL, "****In GetStockSymbolIndex stockSymbol: %s\n",stockSymbol);
  if (RMList.countSymbol == MAX_STOCK_SYMBOL)
  {
    TraceLog(ERROR_LEVEL, "can not insert to symbol list. It is full\n");
    return ERROR;
  }
  
  int a, b, c, d, e, f, g, h;
  int symbolIndex, oldSymbolIndex;
  int accountIndex = 0;

  //Character 1: in range A-Z
  //65 is the ASCII code of 'A'
  //90 is the ASCII code of 'Z'
  //36 is the ASCII code of '$'

  if ((stockSymbol[0] > 64) && (stockSymbol[0] < 91))
  {
    a = stockSymbol[0] - INDEX_OF_A_CHAR;//A-Z  
  }
  else
  {
    //Invalid stock symbol
    return -1;
  }
  
  //Character 2: 0, A-Z
  //65 is the ASCII code of 'A'
  //90 is the ASCII code of 'Z'

  //The order of 'if' expressions are important for faster speed
  //This is experience, right?

  if ((stockSymbol[1] > 64) && (stockSymbol[1] < 91))
  {
    b = stockSymbol[1] - INDEX_OF_A_CHAR;//A-Z  
  }
  //A dollar sign and a dot never appear in the second position!!!
  else if ((stockSymbol[1] == 0) || (stockSymbol[1] == 32))//0: NULL; 32:SPACE
  {
    //Stock symbol's length is 1, no need to do more
    //This also guarantee fast speed
    //We use the variable a in the array index instead of 
    //stockSymbol[0] - INDEX_OF_A_CHAR so as not to calculate anything 

    //Check new stock symbol
    if (symboIndexlList[a][0][0][0][0][SYMBOL_FOR_NEW_ORDER] == -1)
    {
      symbolIndex = __sync_fetch_and_add(&RMList.countSymbol, 1);
    
      if (symbolIndex >= MAX_STOCK_SYMBOL)
      {
        TraceLog(ERROR_LEVEL, "Symbol list is full!\n");
        return -1;
      }
      
      symboIndexlList[a][0][0][0][0][SYMBOL_FOR_NEW_ORDER] = symbolIndex;
      
      memcpy(RMList.symbolList[symbolIndex].symbol, stockSymbol, 1);
      RMList.symbolList[symbolIndex].lastTradedPrice = 0.0;
      RMList.symbolList[symbolIndex].askPrice = 0.0;
      RMList.symbolList[symbolIndex].bidPrice = 0.0;
      
      for (accountIndex = 0; accountIndex < MAX_ACCOUNT; accountIndex++)
      {
        RMList.collection[accountIndex][symbolIndex].leftShares = 0;
        RMList.collection[accountIndex][symbolIndex].totalShares = 0;
        RMList.collection[accountIndex][symbolIndex].timestamp = 0;
        sprintf(RMList.collection[accountIndex][symbolIndex].positionKey, "%.8s_%.8s_Blink",
            RMList.symbolList[symbolIndex].symbol, TradingAccount.DIRECT[accountIndex]);
        // RMList.collection[accountIndex][symbolIndex].prevPositionState = 0;
        RMList.collection[accountIndex][symbolIndex].avgPrice = 0.00;
        RMList.collection[accountIndex][symbolIndex].matchedPL = 0.00;
        RMList.collection[accountIndex][symbolIndex].totalMarket = 0.00;
        RMList.collection[accountIndex][symbolIndex].totalFee = 0.00;
      }

      //TraceLog(DEBUG_LEVEL, "Added symbol: %s (Index: %d)\n", stockSymbol, symbolIndex);
    }

    return symboIndexlList[a][0][0][0][0][SYMBOL_FOR_NEW_ORDER];
  }
  //hyphen is expected to come if all other checks fail
  else if (stockSymbol[1] == '-')
  {
    b = 3;
  }
  else if (stockSymbol[1] == '.')
  {
    b = 4;
  }
  else if (stockSymbol[1] == '$')
  {
    b = 1;
  }
  else if (stockSymbol[1] == '+')
  {
    b = 2;
  }
  else
  {
    return -1;
  }

  //Character 3: 0, ., A-Z
  //65 is the ASCII code of 'A'
  //90 is the ASCII code of 'Z'

  //The order of 'if' expressions are important for faster speed

  if ((stockSymbol[2] > 64) && (stockSymbol[2] < 91))
  {
    c = stockSymbol[2] - INDEX_OF_A_CHAR;//A-Z  
  }

  //Not in range A-Z. We have 244 zeros, and 119 dot
  //Maybe we need to count how many $ and dot at the third position
  //for even better 'if' order

  //244 zeros check first
  else if ((stockSymbol[2] == 0) || (stockSymbol[2] == 32))//0: NULL; 32: SPACE
  { 
    //Stock symbol's length is 2, no need to do more
    //This also guarantee fast speed
    //We use the variable a, b in the array index instead of 
    //stockSymbol[0] - INDEX_OF_A_CHAR, stockSymbol[1] - INDEX_OF_A_CHAR so as not to calculate anything 

    //Check new stock symbol
    if (symboIndexlList[a][b][0][0][0][SYMBOL_FOR_NEW_ORDER] == -1)
    {
      symbolIndex = __sync_fetch_and_add(&RMList.countSymbol, 1);
    
      if (symbolIndex >= MAX_STOCK_SYMBOL)
      {
        TraceLog(ERROR_LEVEL, "Symbol list is full!\n");
        return -1;
      }
      
      symboIndexlList[a][b][0][0][0][SYMBOL_FOR_NEW_ORDER] = symbolIndex;
      
      memcpy(RMList.symbolList[symbolIndex].symbol, stockSymbol, 2);
      RMList.symbolList[symbolIndex].lastTradedPrice = 0.0;
      RMList.symbolList[symbolIndex].askPrice = 0.0;
      RMList.symbolList[symbolIndex].bidPrice = 0.0;
      
      for (accountIndex = 0; accountIndex < MAX_ACCOUNT; accountIndex++)
      {
        RMList.collection[accountIndex][symbolIndex].leftShares = 0;
        RMList.collection[accountIndex][symbolIndex].totalShares = 0;
        RMList.collection[accountIndex][symbolIndex].timestamp = 0;
        sprintf(RMList.collection[accountIndex][symbolIndex].positionKey, "%.8s_%.8s_Blink",
            RMList.symbolList[symbolIndex].symbol, TradingAccount.DIRECT[accountIndex]);
        // RMList.collection[accountIndex][symbolIndex].prevPositionState = 0;
        RMList.collection[accountIndex][symbolIndex].avgPrice = 0.00;
        RMList.collection[accountIndex][symbolIndex].matchedPL = 0.00;
        RMList.collection[accountIndex][symbolIndex].totalMarket = 0.00;
        RMList.collection[accountIndex][symbolIndex].totalFee = 0.00;
      }

      //TraceLog(DEBUG_LEVEL, "Added symbol: %s (Index: %d)\n", stockSymbol, symbolIndex);
    }

    return symboIndexlList[a][b][0][0][0][SYMBOL_FOR_NEW_ORDER];
  }
  else if (stockSymbol[2] == '-')
  {
    c = 3;
  }
  else if (stockSymbol[2] == '.')
  {
    c = 4;
  }
  else if (stockSymbol[2] == '$')
  {
    c = 1;
  }
  else if (stockSymbol[2] == '+')
  {
    c = 2;
  }
  else 
  {
    return -1;
  }

  //Character 4: 0, ., A-Z
  //65 is the ASCII code of 'A'
  //91 is the ASCII code of 'Z'

  if ((stockSymbol[3] > 64) && (stockSymbol[3] < 91))
  {
    d = stockSymbol[3] - INDEX_OF_A_CHAR;//A-Z  
  }

  //Not in range A-Z. We have 3802 zeros, and 119 dot
  //Maybe we need to count how many $ and dot at the fourth position
  //for even better 'if' order

  //Zero is checked first
  else if ((stockSymbol[3] == 0) || (stockSymbol[3] == 32))//0: NULL; 32:SPACE
  {
    //Check new stock symbol
    if (symboIndexlList[a][b][c][0][0][SYMBOL_FOR_NEW_ORDER] == -1)
    {
      symbolIndex = __sync_fetch_and_add(&RMList.countSymbol, 1);
    
      if (symbolIndex >= MAX_STOCK_SYMBOL)
      {
        TraceLog(ERROR_LEVEL, "Symbol list is full!\n");
        return -1;
      }
      
      symboIndexlList[a][b][c][0][0][SYMBOL_FOR_NEW_ORDER] = symbolIndex;
      
      memcpy(RMList.symbolList[symbolIndex].symbol, stockSymbol, 3);
      RMList.symbolList[symbolIndex].lastTradedPrice = 0.0;
      RMList.symbolList[symbolIndex].askPrice = 0.0;
      RMList.symbolList[symbolIndex].bidPrice = 0.0;
      
      for (accountIndex = 0; accountIndex < MAX_ACCOUNT; accountIndex++)
      {
        RMList.collection[accountIndex][symbolIndex].leftShares = 0;
        RMList.collection[accountIndex][symbolIndex].totalShares = 0;
        RMList.collection[accountIndex][symbolIndex].timestamp = 0;
        sprintf(RMList.collection[accountIndex][symbolIndex].positionKey, "%.8s_%.8s_Blink",
            RMList.symbolList[symbolIndex].symbol, TradingAccount.DIRECT[accountIndex]);
        // RMList.collection[accountIndex][symbolIndex].prevPositionState = 0;
        RMList.collection[accountIndex][symbolIndex].avgPrice = 0.00;
        RMList.collection[accountIndex][symbolIndex].matchedPL = 0.00;
        RMList.collection[accountIndex][symbolIndex].totalMarket = 0.00;
        RMList.collection[accountIndex][symbolIndex].totalFee = 0.00;
      }

      //TraceLog(DEBUG_LEVEL, "Added symbol: %s (Index: %d)\n", stockSymbol, symbolIndex);
    }

    return symboIndexlList[a][b][c][0][0][SYMBOL_FOR_NEW_ORDER];
  }
  else if (stockSymbol[3] == '-')
  {
    d = 3;
  }
  else if (stockSymbol[3] == '.')
  {
    d = 4;
  }
  else if (stockSymbol[3] == '$')
  {
    d = 1;
  }
  else if (stockSymbol[3] == '+')
  {
    d = 2;
  }
  else
  {
    return -1;
  }

  //Character 5: 0, ., A-Z
  if ((stockSymbol[4] > 64) && (stockSymbol[4] < 91))
  {
    e = stockSymbol[4] - INDEX_OF_A_CHAR;//A-Z  
  }
  //Not in range A-Z. We have 3248 zeros, 1 dot (for the stock symbol ZZZZ.T only)
  //Zero is checked first

  else if ((stockSymbol[4] == 0) || (stockSymbol[4] == 32))//NULL Because: 4: 3248, 32: SPACE
  {
    //Check new stock symbol
    if (symboIndexlList[a][b][c][d][0][SYMBOL_FOR_NEW_ORDER] == -1)
    {
      symbolIndex = __sync_fetch_and_add(&RMList.countSymbol, 1);
    
      if (symbolIndex >= MAX_STOCK_SYMBOL)
      {
        TraceLog(ERROR_LEVEL, "Symbol list is full!\n");
        return -1;
      }
      
      symboIndexlList[a][b][c][d][0][SYMBOL_FOR_NEW_ORDER] = symbolIndex;
      
      memcpy(RMList.symbolList[symbolIndex].symbol, stockSymbol, 4);
      RMList.symbolList[symbolIndex].lastTradedPrice = 0.0;
      RMList.symbolList[symbolIndex].askPrice = 0.0;
      RMList.symbolList[symbolIndex].bidPrice = 0.0;
      
      for (accountIndex = 0; accountIndex < MAX_ACCOUNT; accountIndex++)
      {
        RMList.collection[accountIndex][symbolIndex].leftShares = 0;
        RMList.collection[accountIndex][symbolIndex].totalShares = 0;
        RMList.collection[accountIndex][symbolIndex].timestamp = 0;
        sprintf(RMList.collection[accountIndex][symbolIndex].positionKey, "%.8s_%.8s_Blink",
            RMList.symbolList[symbolIndex].symbol, TradingAccount.DIRECT[accountIndex]);
        // RMList.collection[accountIndex][symbolIndex].prevPositionState = 0;
        RMList.collection[accountIndex][symbolIndex].avgPrice = 0.00;
        RMList.collection[accountIndex][symbolIndex].matchedPL = 0.00;
        RMList.collection[accountIndex][symbolIndex].totalMarket = 0.00;
        RMList.collection[accountIndex][symbolIndex].totalFee = 0.00;
      }

      //TraceLog(DEBUG_LEVEL, "Added symbol: %s (Index: %d)\n", stockSymbol, symbolIndex);
    }

    return symboIndexlList[a][b][c][d][0][SYMBOL_FOR_NEW_ORDER];
  }
  else if (stockSymbol[4] == '-')
  {
    e = 3;
  }
  else if (stockSymbol[4] == '.')
  {
    e = 4;
  }
  else if (stockSymbol[4] == '$')
  {
    e = 1;
  }
  else if (stockSymbol[4] == '+')
  {
    e = 2;
  }
  else
  {
    return -1;
  }
  
  //Character 6
  if ((stockSymbol[5] > 64) && (stockSymbol[5] < 91))
  {
    f = stockSymbol[5] - INDEX_OF_A_CHAR;//A-Z  
  }
  else if ((stockSymbol[5] == 0) || (stockSymbol[5] == 32))//NULL Because: 4: 3248, 32: SPACE
  {
    if (symboIndexlList[a][b][c][d][e][SYMBOL_FOR_NEW_ORDER] == -1)
    {
      symbolIndex = __sync_fetch_and_add(&RMList.countSymbol, 1);
    
      if (symbolIndex >= MAX_STOCK_SYMBOL)
      {
        TraceLog(ERROR_LEVEL, "Symbol list is full!\n");
        return -1;
      }
      
      symboIndexlList[a][b][c][d][e][SYMBOL_FOR_NEW_ORDER] = symbolIndex;
      
      memcpy(RMList.symbolList[symbolIndex].symbol, stockSymbol, 5);
      RMList.symbolList[symbolIndex].lastTradedPrice = 0.0;
      RMList.symbolList[symbolIndex].askPrice = 0.0;
      RMList.symbolList[symbolIndex].bidPrice = 0.0;
      
      for (accountIndex = 0; accountIndex < MAX_ACCOUNT; accountIndex++)
      {
        RMList.collection[accountIndex][symbolIndex].leftShares = 0;
        RMList.collection[accountIndex][symbolIndex].totalShares = 0;
        RMList.collection[accountIndex][symbolIndex].timestamp = 0;
        sprintf(RMList.collection[accountIndex][symbolIndex].positionKey, "%.8s_%.8s_Blink",
            RMList.symbolList[symbolIndex].symbol, TradingAccount.DIRECT[accountIndex]);
        // RMList.collection[accountIndex][symbolIndex].prevPositionState = 0;
        RMList.collection[accountIndex][symbolIndex].avgPrice = 0.00;
        RMList.collection[accountIndex][symbolIndex].matchedPL = 0.00;
        RMList.collection[accountIndex][symbolIndex].totalMarket = 0.00;
        RMList.collection[accountIndex][symbolIndex].totalFee = 0.00;
      }

      return symbolIndex;
    }
    else
    {
      //TraceLog(DEBUG_LEVEL, "stockSymbol = %s -> symbolIndex = %d\n", stockSymbol, symboIndexlList[a][b][c][d][e][SYMBOL_FOR_NEW_ORDER]);
      return symboIndexlList[a][b][c][d][e][SYMBOL_FOR_NEW_ORDER];
    }
  }
  else if (stockSymbol[5] == '-')
  {
    f = 3;
  }
  else if (stockSymbol[5] == '.')
  {
    f = 4;
  }
  else if (stockSymbol[5] == '$')
  {
    f = 1;
  }
  else if (stockSymbol[5] == '+')
  {
    f = 2;
  }
  else
  {
    return -1;
  }
  
  //Character 7: 0, ., A-Z
  if ((stockSymbol[6] > 64) && (stockSymbol[6] < 91))
  {
      //A-Z
    g = stockSymbol[6] - INDEX_OF_A_CHAR; 
  }
  else if ((stockSymbol[6] == 0) || (stockSymbol[6] == 32))
  {
    if (symboIndexlList[a][b][c][d][e][SYMBOL_FOR_NEW_ORDER] == -1)
    {
      symbolIndex = __sync_fetch_and_add(&RMList.countSymbol, 1);
    
      if (symbolIndex >= MAX_STOCK_SYMBOL)
      {
        TraceLog(ERROR_LEVEL, "Symbol list is full!\n");
        return -1;
      }
      
      symboIndexlList[a][b][c][d][e][SYMBOL_FOR_NEW_ORDER] = symbolIndex;
      
      memcpy(RMList.symbolList[symbolIndex].symbol, stockSymbol, 5);
      RMList.symbolList[symbolIndex].lastTradedPrice = 0.0;
      RMList.symbolList[symbolIndex].askPrice = 0.0;
      RMList.symbolList[symbolIndex].bidPrice = 0.0;
      
      for (accountIndex = 0; accountIndex < MAX_ACCOUNT; accountIndex++)
      {
        RMList.collection[accountIndex][symbolIndex].leftShares = 0;
        RMList.collection[accountIndex][symbolIndex].totalShares = 0;
        RMList.collection[accountIndex][symbolIndex].timestamp = 0;
        sprintf(RMList.collection[accountIndex][symbolIndex].positionKey, "%.8s_%.8s_Blink",
            RMList.symbolList[symbolIndex].symbol, TradingAccount.DIRECT[accountIndex]);
        // RMList.collection[accountIndex][symbolIndex].prevPositionState = 0;
        RMList.collection[accountIndex][symbolIndex].avgPrice = 0.00;
        RMList.collection[accountIndex][symbolIndex].matchedPL = 0.00;
        RMList.collection[accountIndex][symbolIndex].totalMarket = 0.00;
        RMList.collection[accountIndex][symbolIndex].totalFee = 0.00;
      }
      
      // Add to long list with 6 chars
      oldSymbolIndex = symbolIndex;
      symbolIndex = __sync_fetch_and_add(&RMList.countSymbol, 1);
    
      if (symbolIndex >= MAX_STOCK_SYMBOL)
      {
        TraceLog(ERROR_LEVEL, "Symbol list is full!\n");
        return -1;
      }
      
      longSymbolIndexArray[oldSymbolIndex][f][0][SYMBOL_FOR_NEW_ORDER] = symbolIndex;
      
      memcpy(RMList.symbolList[symbolIndex].symbol, stockSymbol, 6);
      RMList.symbolList[symbolIndex].lastTradedPrice = 0.0;
      RMList.symbolList[symbolIndex].askPrice = 0.0;
      RMList.symbolList[symbolIndex].bidPrice = 0.0;
      
      for (accountIndex = 0; accountIndex < MAX_ACCOUNT; accountIndex++)
      {
        RMList.collection[accountIndex][symbolIndex].leftShares = 0;
        RMList.collection[accountIndex][symbolIndex].totalShares = 0;
        RMList.collection[accountIndex][symbolIndex].timestamp = 0;
        sprintf(RMList.collection[accountIndex][symbolIndex].positionKey, "%.8s_%.8s_Blink",
            RMList.symbolList[symbolIndex].symbol, TradingAccount.DIRECT[accountIndex]);
        // RMList.collection[accountIndex][symbolIndex].prevPositionState = 0;
        RMList.collection[accountIndex][symbolIndex].avgPrice = 0.00;
        RMList.collection[accountIndex][symbolIndex].matchedPL = 0.00;
        RMList.collection[accountIndex][symbolIndex].totalMarket = 0.00;
        RMList.collection[accountIndex][symbolIndex].totalFee = 0.00;
      }

      //TraceLog(DEBUG_LEVEL, "stockSymbol = %s -> symbolIndex = %d\n", stockSymbol, symbolIndex);
      return symbolIndex;
    }
    else
    {
      oldSymbolIndex = symboIndexlList[a][b][c][d][e][SYMBOL_FOR_NEW_ORDER];
      
      if (longSymbolIndexArray[oldSymbolIndex][f][0][SYMBOL_FOR_NEW_ORDER] == -1)
      {
        symbolIndex = __sync_fetch_and_add(&RMList.countSymbol, 1);
      
        if (symbolIndex >= MAX_STOCK_SYMBOL)
        {
          TraceLog(ERROR_LEVEL, "Symbol list is full!\n");
          return -1;
        }
        
        longSymbolIndexArray[oldSymbolIndex][f][0][SYMBOL_FOR_NEW_ORDER] = symbolIndex;
        
        memcpy(RMList.symbolList[symbolIndex].symbol, stockSymbol, 6);
        RMList.symbolList[symbolIndex].lastTradedPrice = 0.0;
        RMList.symbolList[symbolIndex].askPrice = 0.0;
        RMList.symbolList[symbolIndex].bidPrice = 0.0;
        
        for (accountIndex = 0; accountIndex < MAX_ACCOUNT; accountIndex++)
        {
          RMList.collection[accountIndex][symbolIndex].leftShares = 0;
          RMList.collection[accountIndex][symbolIndex].totalShares = 0;
          RMList.collection[accountIndex][symbolIndex].timestamp = 0;
          sprintf(RMList.collection[accountIndex][symbolIndex].positionKey, "%.8s_%.8s_Blink",
              RMList.symbolList[symbolIndex].symbol, TradingAccount.DIRECT[accountIndex]);
          // RMList.collection[accountIndex][symbolIndex].prevPositionState = 0;
          RMList.collection[accountIndex][symbolIndex].avgPrice = 0.00;
          RMList.collection[accountIndex][symbolIndex].matchedPL = 0.00;
          RMList.collection[accountIndex][symbolIndex].totalMarket = 0.00;
          RMList.collection[accountIndex][symbolIndex].totalFee = 0.00;
        }

        return symbolIndex;
      }
      else
      {
        return longSymbolIndexArray[oldSymbolIndex][f][0][SYMBOL_FOR_NEW_ORDER];
      } 
    }
  }
  //Not in range A-Z. We have 3248 zeros, 1 dot (for the stock symbol ZZZZ.T only)
  //Zero is checked first
    //NULL Because: 4: 3248, 32: SPAC
  else if (stockSymbol[6] == '-')
  {
    g = 3;
  }
  else if (stockSymbol[6] == '.')
  {
    g = 4;
  }
  else if (stockSymbol[6] == '$')
  {
    g = 1;
  }
  else if (stockSymbol[6] == '+')
  {
    g = 2;
  }
  else
  {
    return -1;
  }
  
  //character 8 0, $, ., A-Z
  if ((stockSymbol[7] > 64) && (stockSymbol[7] < 91))// A-Z
  {
    h = stockSymbol[7] - INDEX_OF_A_CHAR;   
  }
  else if ((stockSymbol[7] == 32) || (stockSymbol[7] == 0))
  {
    if (symboIndexlList[a][b][c][d][e][SYMBOL_FOR_NEW_ORDER] == -1)
    {
      symbolIndex = __sync_fetch_and_add(&RMList.countSymbol, 1);
    
      if (symbolIndex >= MAX_STOCK_SYMBOL)
      {
        TraceLog(ERROR_LEVEL, "Symbol list is full!\n");
        return -1;
      }
      
      symboIndexlList[a][b][c][d][e][SYMBOL_FOR_NEW_ORDER] = symbolIndex;
      
      memcpy(RMList.symbolList[symbolIndex].symbol, stockSymbol, 5);
      RMList.symbolList[symbolIndex].lastTradedPrice = 0.0;
      RMList.symbolList[symbolIndex].askPrice = 0.0;
      RMList.symbolList[symbolIndex].bidPrice = 0.0;
      
      for (accountIndex = 0; accountIndex < MAX_ACCOUNT; accountIndex++)
      {
        RMList.collection[accountIndex][symbolIndex].leftShares = 0;
        RMList.collection[accountIndex][symbolIndex].totalShares = 0;
        RMList.collection[accountIndex][symbolIndex].timestamp = 0;
        sprintf(RMList.collection[accountIndex][symbolIndex].positionKey, "%.8s_%.8s_Blink",
            RMList.symbolList[symbolIndex].symbol, TradingAccount.DIRECT[accountIndex]);
        // RMList.collection[accountIndex][symbolIndex].prevPositionState = 0;
        RMList.collection[accountIndex][symbolIndex].avgPrice = 0.00;
        RMList.collection[accountIndex][symbolIndex].matchedPL = 0.00;
        RMList.collection[accountIndex][symbolIndex].totalMarket = 0.00;
        RMList.collection[accountIndex][symbolIndex].totalFee = 0.00;
      }

      // Add symbol to long list with 7 chars
      oldSymbolIndex = symbolIndex;
      symbolIndex = __sync_fetch_and_add(&RMList.countSymbol, 1);
    
      if (symbolIndex >= MAX_STOCK_SYMBOL)
      {
        TraceLog(ERROR_LEVEL, "Symbol list is full!\n");
        return -1;
      }
      
      longSymbolIndexArray[oldSymbolIndex][f][g][SYMBOL_FOR_NEW_ORDER] = symbolIndex;
      
      memcpy(RMList.symbolList[symbolIndex].symbol, stockSymbol, 7);
      RMList.symbolList[symbolIndex].lastTradedPrice = 0.0;
      RMList.symbolList[symbolIndex].askPrice = 0.0;
      RMList.symbolList[symbolIndex].bidPrice = 0.0;
      
      for (accountIndex = 0; accountIndex < MAX_ACCOUNT; accountIndex++)
      {
        RMList.collection[accountIndex][symbolIndex].leftShares = 0;
        RMList.collection[accountIndex][symbolIndex].totalShares = 0;
        RMList.collection[accountIndex][symbolIndex].timestamp = 0;
        sprintf(RMList.collection[accountIndex][symbolIndex].positionKey, "%.8s_%.8s_Blink",
            RMList.symbolList[symbolIndex].symbol, TradingAccount.DIRECT[accountIndex]);
        // RMList.collection[accountIndex][symbolIndex].prevPositionState = 0;
        RMList.collection[accountIndex][symbolIndex].avgPrice = 0.00;
        RMList.collection[accountIndex][symbolIndex].matchedPL = 0.00;
        RMList.collection[accountIndex][symbolIndex].totalMarket = 0.00;
        RMList.collection[accountIndex][symbolIndex].totalFee = 0.00;
      }

      //TraceLog(DEBUG_LEVEL, "stockSymbol = %s -> symbolIndex = %d\n", stockSymbol, symbolIndex);
      return symbolIndex;
    }
    else
    {
      oldSymbolIndex = symboIndexlList[a][b][c][d][e][SYMBOL_FOR_NEW_ORDER];
        
      if (longSymbolIndexArray[oldSymbolIndex][f][g][SYMBOL_FOR_NEW_ORDER] == -1)
      { 
        symbolIndex = __sync_fetch_and_add(&RMList.countSymbol, 1);
    
        if (symbolIndex >= MAX_STOCK_SYMBOL)
        {
          TraceLog(ERROR_LEVEL, "Symbol list is full!\n");
          return -1;
        }
        
        longSymbolIndexArray[oldSymbolIndex][f][g][SYMBOL_FOR_NEW_ORDER] = symbolIndex;
        
        memcpy(RMList.symbolList[symbolIndex].symbol, stockSymbol, 7);
        RMList.symbolList[symbolIndex].lastTradedPrice = 0.0;
        RMList.symbolList[symbolIndex].askPrice = 0.0;
        RMList.symbolList[symbolIndex].bidPrice = 0.0;
        
        for (accountIndex = 0; accountIndex < MAX_ACCOUNT; accountIndex++)
        { 
          RMList.collection[accountIndex][symbolIndex].leftShares = 0;
          RMList.collection[accountIndex][symbolIndex].totalShares = 0;
          RMList.collection[accountIndex][symbolIndex].timestamp = 0;
          sprintf(RMList.collection[accountIndex][symbolIndex].positionKey, "%.8s_%.8s_Blink",
              RMList.symbolList[symbolIndex].symbol, TradingAccount.DIRECT[accountIndex]);
          // RMList.collection[accountIndex][symbolIndex].prevPositionState = 0;
          RMList.collection[accountIndex][symbolIndex].avgPrice = 0.00;
          RMList.collection[accountIndex][symbolIndex].matchedPL = 0.00;
          RMList.collection[accountIndex][symbolIndex].totalMarket = 0.00;
          RMList.collection[accountIndex][symbolIndex].totalFee = 0.00;
        }
        
        return symbolIndex;
      }
      else
      {
        return longSymbolIndexArray[oldSymbolIndex][f][g][SYMBOL_FOR_NEW_ORDER];
      }
    }
  }
  else if (stockSymbol[7] == '-')
  {
    h = 3;
  }
  else if (stockSymbol[7] == '.')
  {
    h = 4;
  }
  else if (stockSymbol[7] == '$')
  {
    h = 1;
  }
  else if (stockSymbol[7] == '+')
  {
    h = 2;
  }
  else
  {
    return -1;
  }
  
  //This is where we update the symbols with 8 characters long
  if (symboIndexlList[a][b][c][d][e][SYMBOL_FOR_NEW_ORDER] == -1)
  {
    symbolIndex = __sync_fetch_and_add(&RMList.countSymbol, 1);
    
    if (symbolIndex >= MAX_STOCK_SYMBOL)
    {
      TraceLog(ERROR_LEVEL, "Symbol list is full!\n");
      return -1;
    }
    
    symboIndexlList[a][b][c][d][e][SYMBOL_FOR_NEW_ORDER] = symbolIndex;
    
    memcpy(RMList.symbolList[symbolIndex].symbol, stockSymbol, 5);
    RMList.symbolList[symbolIndex].lastTradedPrice = 0.0;
    RMList.symbolList[symbolIndex].askPrice = 0.0;
    RMList.symbolList[symbolIndex].bidPrice = 0.0;
    
    for (accountIndex = 0; accountIndex < MAX_ACCOUNT; accountIndex++)
    {
      RMList.collection[accountIndex][symbolIndex].leftShares = 0;
      RMList.collection[accountIndex][symbolIndex].totalShares = 0;
      RMList.collection[accountIndex][symbolIndex].timestamp = 0;
      sprintf(RMList.collection[accountIndex][symbolIndex].positionKey, "%.8s_%.8s_Blink",
          RMList.symbolList[symbolIndex].symbol, TradingAccount.DIRECT[accountIndex]);
      // RMList.collection[accountIndex][symbolIndex].prevPositionState = 0;
      RMList.collection[accountIndex][symbolIndex].avgPrice = 0.00;
      RMList.collection[accountIndex][symbolIndex].matchedPL = 0.00;
      RMList.collection[accountIndex][symbolIndex].totalMarket = 0.00;
      RMList.collection[accountIndex][symbolIndex].totalFee = 0.00;
    }

    // Update the RM symbol list, 7 chars
    oldSymbolIndex = symbolIndex;
    symbolIndex = __sync_fetch_and_add(&RMList.countSymbol, 1);
  
    if (symbolIndex >= MAX_STOCK_SYMBOL)
    {
      TraceLog(ERROR_LEVEL, "Symbol list is full!\n");
      return -1;
    }
    
    longSymbolIndexArray[oldSymbolIndex][f][g][SYMBOL_FOR_NEW_ORDER] = symbolIndex;
    
    memcpy(RMList.symbolList[symbolIndex].symbol, stockSymbol, 7);
    RMList.symbolList[symbolIndex].lastTradedPrice = 0.0;
    RMList.symbolList[symbolIndex].askPrice = 0.0;
    RMList.symbolList[symbolIndex].bidPrice = 0.0;
    
    for (accountIndex = 0; accountIndex < MAX_ACCOUNT; accountIndex++)
    {
      RMList.collection[accountIndex][symbolIndex].leftShares = 0;
      RMList.collection[accountIndex][symbolIndex].totalShares = 0;
      RMList.collection[accountIndex][symbolIndex].timestamp = 0;
      sprintf(RMList.collection[accountIndex][symbolIndex].positionKey, "%.8s_%.8s_Blink",
          RMList.symbolList[symbolIndex].symbol, TradingAccount.DIRECT[accountIndex]);
      // RMList.collection[accountIndex][symbolIndex].prevPositionState = 0;
      RMList.collection[accountIndex][symbolIndex].avgPrice = 0.00;
      RMList.collection[accountIndex][symbolIndex].matchedPL = 0.00;
      RMList.collection[accountIndex][symbolIndex].totalMarket = 0.00;
      RMList.collection[accountIndex][symbolIndex].totalFee = 0.00;
    }

    // Update the RM symbol list, 8 chars
    oldSymbolIndex = symbolIndex;
    symbolIndex = __sync_fetch_and_add(&RMList.countSymbol, 1);
  
    if (symbolIndex >= MAX_STOCK_SYMBOL)
    {
      TraceLog(ERROR_LEVEL, "Symbol list is full!\n");
      return -1;
    }
    
    extraLongSymbolIndexArray[oldSymbolIndex][h][SYMBOL_FOR_NEW_ORDER] = symbolIndex;
    
    memcpy(RMList.symbolList[symbolIndex].symbol, stockSymbol, 8);
    RMList.symbolList[symbolIndex].lastTradedPrice = 0.0;
    RMList.symbolList[symbolIndex].askPrice = 0.0;
    RMList.symbolList[symbolIndex].bidPrice = 0.0;
    
    for (accountIndex = 0; accountIndex < MAX_ACCOUNT; accountIndex++)
    {
      RMList.collection[accountIndex][symbolIndex].leftShares = 0;
      RMList.collection[accountIndex][symbolIndex].totalShares = 0;
      RMList.collection[accountIndex][symbolIndex].timestamp = 0;
      sprintf(RMList.collection[accountIndex][symbolIndex].positionKey, "%.8s_%.8s_Blink",
          RMList.symbolList[symbolIndex].symbol, TradingAccount.DIRECT[accountIndex]);
      // RMList.collection[accountIndex][symbolIndex].prevPositionState = 0;
      RMList.collection[accountIndex][symbolIndex].avgPrice = 0.00;
      RMList.collection[accountIndex][symbolIndex].matchedPL = 0.00;
      RMList.collection[accountIndex][symbolIndex].totalMarket = 0.00;
      RMList.collection[accountIndex][symbolIndex].totalFee = 0.00;
    }

    //TraceLog(DEBUG_LEVEL, "stockSymbol = %s -> symbolIndex = %d\n", stockSymbol, symbolIndex);
    return symbolIndex;
  }
  else
  {
    oldSymbolIndex = symboIndexlList[a][b][c][d][e][SYMBOL_FOR_NEW_ORDER];
    
    if (longSymbolIndexArray[oldSymbolIndex][f][g][SYMBOL_FOR_NEW_ORDER] == -1)
    {
      symbolIndex = __sync_fetch_and_add(&RMList.countSymbol, 1);
  
      if (symbolIndex >= MAX_STOCK_SYMBOL)
      {
        TraceLog(ERROR_LEVEL, "Symbol list is full!\n");
        return -1;
      }
      
      longSymbolIndexArray[oldSymbolIndex][f][g][SYMBOL_FOR_NEW_ORDER] = symbolIndex;
      
      memcpy(RMList.symbolList[symbolIndex].symbol, stockSymbol, 7);
      RMList.symbolList[symbolIndex].lastTradedPrice = 0.0;
      RMList.symbolList[symbolIndex].askPrice = 0.0;
      RMList.symbolList[symbolIndex].bidPrice = 0.0;
      
      for (accountIndex = 0; accountIndex < MAX_ACCOUNT; accountIndex++)
      {
        RMList.collection[accountIndex][symbolIndex].leftShares = 0;
        RMList.collection[accountIndex][symbolIndex].totalShares = 0;
        RMList.collection[accountIndex][symbolIndex].timestamp = 0;
        sprintf(RMList.collection[accountIndex][symbolIndex].positionKey, "%.8s_%.8s_Blink",
            RMList.symbolList[symbolIndex].symbol, TradingAccount.DIRECT[accountIndex]);
        // RMList.collection[accountIndex][symbolIndex].prevPositionState = 0;
        RMList.collection[accountIndex][symbolIndex].avgPrice = 0.00;
        RMList.collection[accountIndex][symbolIndex].matchedPL = 0.00;
        RMList.collection[accountIndex][symbolIndex].totalMarket = 0.00;
        RMList.collection[accountIndex][symbolIndex].totalFee = 0.00;
      }
    }
    
    oldSymbolIndex = longSymbolIndexArray[oldSymbolIndex][f][g][SYMBOL_FOR_NEW_ORDER];
    
    if (extraLongSymbolIndexArray[oldSymbolIndex][h][SYMBOL_FOR_NEW_ORDER] == -1)
    {
      symbolIndex = __sync_fetch_and_add(&RMList.countSymbol, 1);
    
      if (symbolIndex >= MAX_STOCK_SYMBOL)
      {
        TraceLog(ERROR_LEVEL, "Symbol list is full!\n");
        return -1;
      }
      
      extraLongSymbolIndexArray[oldSymbolIndex][h][SYMBOL_FOR_NEW_ORDER] = symbolIndex;
      
      memcpy(RMList.symbolList[symbolIndex].symbol, stockSymbol, 8);
      RMList.symbolList[symbolIndex].lastTradedPrice = 0.0;
      RMList.symbolList[symbolIndex].askPrice = 0.0;
      RMList.symbolList[symbolIndex].bidPrice = 0.0;
      
      for (accountIndex = 0; accountIndex < MAX_ACCOUNT; accountIndex++)
      {
        RMList.collection[accountIndex][symbolIndex].leftShares = 0;
        RMList.collection[accountIndex][symbolIndex].totalShares = 0;
        RMList.collection[accountIndex][symbolIndex].timestamp = 0;
        sprintf(RMList.collection[accountIndex][symbolIndex].positionKey, "%.8s_%.8s_Blink",
            RMList.symbolList[symbolIndex].symbol, TradingAccount.DIRECT[accountIndex]);
        // RMList.collection[accountIndex][symbolIndex].prevPositionState = 0;
        RMList.collection[accountIndex][symbolIndex].avgPrice = 0.00;
        RMList.collection[accountIndex][symbolIndex].matchedPL = 0.00;
        RMList.collection[accountIndex][symbolIndex].totalMarket = 0.00;
        RMList.collection[accountIndex][symbolIndex].totalFee = 0.00;
      }

      return symbolIndex;
    }
    else
    {
      return extraLongSymbolIndexArray[oldSymbolIndex][h][SYMBOL_FOR_NEW_ORDER];
    }
  }
}

/****************************************************************************
- Function name:  GetOrderIdIndex
- Input:      
- Output:     
- Return:     
- Description:    check if exist, if not, insert
- Usage:      
****************************************************************************/
int GetOrderIdIndex(int clOrdId, int tsId)
{
  int tsIndex;

  if (tsId == MANUAL_ORDER)
  {
    tsIndex = 0;
  }
  else
  {
    tsIndex = FindTradeServerIndex(tsId);

    if (tsIndex == -1)
    {
      return ERROR;
    }
    else
    {
      tsIndex += 1;
    }
  }
  
  if (orderIdIndexList[tsIndex][clOrdId % MAX_ORDER_ID_RANGE] == -1)
  {
    orderIdIndexList[tsIndex][clOrdId % MAX_ORDER_ID_RANGE] = asOpenOrder.countOpenOrder;
  }

  return orderIdIndexList[tsIndex][clOrdId % MAX_ORDER_ID_RANGE];
}

/****************************************************************************
- Function name:  CheckOrderIdIndex
- Input:      
- Output:     
- Return:     
- Description:    Only check if exist, if yes, return index,
          otherwise return error, do no insert
- Usage:      
****************************************************************************/
int CheckOrderIdIndex(int clOrdId, int tsId)
{
  int tsIndex;

  if (tsId == MANUAL_ORDER)
  {
    tsIndex = 0;
  }
  else
  {
    tsIndex = FindTradeServerIndex(tsId);

    if (tsIndex == -1)
    {
      return -1;
    }
    else
    {
      tsIndex += 1;
    }
  }
  
  if (orderIdIndexList[tsIndex][clOrdId % MAX_ORDER_ID_RANGE] == -1)
  {
    return -2;
  }

  return orderIdIndexList[tsIndex][clOrdId % MAX_ORDER_ID_RANGE];
}

/****************************************************************************
- Function name:  ProcessAddASOpenOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessAddASOpenOrder(void *_openOrder, void *_orderDetail)
{
  t_OpenOrderInfo *openOrder = (t_OpenOrderInfo *)_openOrder;
  t_OrderDetail *orderDetail = (t_OrderDetail *)_orderDetail;

  int totalCountOrderDetail = asOrderDetail.countOrderDetail;
  int countOrderDetail, isNew = 0;

  int indexOpenOrder = GetOrderIdIndex(openOrder->clOrdId, openOrder->tsId);

  if (indexOpenOrder == -1)
  {
    TraceLog(ERROR_LEVEL, "GetOrderIdIndex: tsId = %d, orderId = %d\n", openOrder->tsId, openOrder->clOrdId);

    return ERROR;
  }

  if ((asOpenOrder.orderCollection[indexOpenOrder].orderSummary.clOrdId > 0) && (openOrder->clOrdId != asOpenOrder.orderCollection[indexOpenOrder].orderSummary.clOrdId))
  {
    TraceLog(ERROR_LEVEL, "GetOrderIdIndex: duplicate hash table, max length = %d. \nNew item: orderId = %d, tsId = %d. \nOld item: orderId = %d, tsId = %d\n", MAX_ORDER_ID_RANGE, openOrder->clOrdId, openOrder->tsId, asOpenOrder.orderCollection[indexOpenOrder].orderSummary.clOrdId, asOpenOrder.orderCollection[indexOpenOrder].orderSummary.tsId);

    return ERROR;
  }

  if (indexOpenOrder == asOpenOrder.countOpenOrder)
  {
    if (asOpenOrder.countOpenOrder + 1 == MAX_ORDER_PLACEMENT)
    {
      TraceLog(ERROR_LEVEL, "Orders placement collection is full, MAX_ORDER_PLACEMENT = %d\n", MAX_ORDER_PLACEMENT);

      return ERROR;
    }

    // Increase count open order
    asOpenOrder.countOpenOrder ++;

    isNew = 1;
  }

  if (isNew == 1)
  {
    UpdateNumberOfOrderPlaced(openOrder->tsId);

    countOrderDetail = asOpenOrder.orderCollection[indexOpenOrder].countOrderDetail;
  
    // Order detail
    memcpy(&asOrderDetail.orderDetailList[totalCountOrderDetail], orderDetail, sizeof(t_OrderDetail));    
    
    // Order summary  
    memcpy(&asOpenOrder.orderCollection[indexOpenOrder].orderSummary, openOrder, sizeof(t_OpenOrderInfo));
    
    if (asOpenOrder.orderCollection[indexOpenOrder].isNewOrder == -2)
    {
      isNew = -2; //ACK comes first so do not insert database
    }
    
    asOpenOrder.orderCollection[indexOpenOrder].isNewOrder = 0;

    // Update index order detail
    asOpenOrder.orderCollection[indexOpenOrder].indexOrderDetailList[countOrderDetail] = totalCountOrderDetail;

    if (asOpenOrder.orderCollection[indexOpenOrder].countOrderDetail + 1 == MAX_ORDER_DETAIL)
    {
      TraceLog(ERROR_LEVEL, "Index of orders detail collection is full, clOrdId = %d, MAX_ORDER_DETAIL = %d\n", openOrder->clOrdId, MAX_ORDER_DETAIL);

      // isUpdated 
      memset(asOpenOrder.orderCollection[indexOpenOrder].isUpdated, OPEN_ORDER_UPDATED, MAX_TRADER_TOOL_CONNECTIONS);

      return ERROR;
    }
    
    // Increase count order detail of open order
    asOpenOrder.orderCollection[indexOpenOrder].countOrderDetail ++;

    if (asOrderDetail.countOrderDetail + 1 == MAX_RAW_DATA_TOTAL_ENTRIES)
    {
      TraceLog(ERROR_LEVEL, "Orders detail collection is full, MAX_RAW_DATA_TOTAL_ENTRIES = %d\n", MAX_RAW_DATA_TOTAL_ENTRIES);

      // isUpdated 
      memset(asOpenOrder.orderCollection[indexOpenOrder].isUpdated, OPEN_ORDER_UPDATED, MAX_TRADER_TOOL_CONNECTIONS);

      return ERROR;
    }

    // Increase count order detail of as
    asOrderDetail.countOrderDetail ++;
  }
  else
  {
    if (asOpenOrder.orderCollection[indexOpenOrder].isNewOrder == 0)
    {
      TraceLog(ERROR_LEVEL, "AddOpenOrders: Duplicate tsId = %d, orderId = %d\n", openOrder->tsId, openOrder->clOrdId);

      return ERROR;
    }

    /*
      This is the case New order comes after Ack/fill
      This should never happen since we implemented wait-list
      return SUCCESS;
    */
    TraceLog(ERROR_LEVEL, "(ProcessAddASOpenOrder): New order comes after Ack/Fill\n");
    
    countOrderDetail = asOpenOrder.orderCollection[indexOpenOrder].countOrderDetail;

    // Order detail
    memcpy(&asOrderDetail.orderDetailList[totalCountOrderDetail], orderDetail, sizeof(t_OrderDetail));

    // Update order summary
    memset(asOpenOrder.orderCollection[indexOpenOrder].orderSummary.symbol, 0, SYMBOL_LEN);
    strncpy(asOpenOrder.orderCollection[indexOpenOrder].orderSummary.symbol, openOrder->symbol, SYMBOL_LEN);
    asOpenOrder.orderCollection[indexOpenOrder].orderSummary.side = openOrder->side;
    asOpenOrder.orderCollection[indexOpenOrder].orderSummary.shares = openOrder->shares;
    asOpenOrder.orderCollection[indexOpenOrder].orderSummary.price = openOrder->price;
    asOpenOrder.orderCollection[indexOpenOrder].orderSummary.tif = openOrder->tif;
    asOpenOrder.orderCollection[indexOpenOrder].orderSummary.account = openOrder->account;

    if (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.leftShares == 0) //Fix bug when ACK comes before NEW Order
    {
      asOpenOrder.orderCollection[indexOpenOrder].orderSummary.leftShares = openOrder->shares;
    }
    
    if (asOpenOrder.orderCollection[indexOpenOrder].isNewOrder == -2)
    {
      isNew = -2; //ACK come first, so do not insert into database
    }
    
    asOpenOrder.orderCollection[indexOpenOrder].isNewOrder = 0;

    if ((asOpenOrder.orderCollection[indexOpenOrder].orderSummary.leftShares > 0) && 
      (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.leftShares <= asOpenOrder.orderCollection[indexOpenOrder].orderSummary.shares))
    {
      if ((asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status != orderStatusIndex.BrokenTrade) && 
        (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status != orderStatusIndex.CanceledByECN))
      {
        asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status = orderStatusIndex.Live;
      }
    }

    // Update index order detail
    asOpenOrder.orderCollection[indexOpenOrder].indexOrderDetailList[countOrderDetail] = totalCountOrderDetail;

    if (asOpenOrder.orderCollection[indexOpenOrder].countOrderDetail + 1 == MAX_ORDER_DETAIL)
    {
      TraceLog(ERROR_LEVEL, "Index of orders detail collection is full, clOrdId = %d, MAX_ORDER_DETAIL = %d\n", openOrder->clOrdId, MAX_ORDER_DETAIL);

      // isUpdated 
      memset(asOpenOrder.orderCollection[indexOpenOrder].isUpdated, OPEN_ORDER_UPDATED, MAX_TRADER_TOOL_CONNECTIONS);

      return ERROR;
    }
    
    // Increase count order detail of open order
    asOpenOrder.orderCollection[indexOpenOrder].countOrderDetail ++;

    if (asOrderDetail.countOrderDetail + 1 == MAX_RAW_DATA_TOTAL_ENTRIES)
    {
      TraceLog(ERROR_LEVEL, "Orders detail collection is full, MAX_RAW_DATA_TOTAL_ENTRIES = %d\n", MAX_RAW_DATA_TOTAL_ENTRIES);

      // isUpdated 
      memset(asOpenOrder.orderCollection[indexOpenOrder].isUpdated, OPEN_ORDER_UPDATED, MAX_TRADER_TOOL_CONNECTIONS);

      return ERROR;
    }

    // Increase count order detail of as
    asOrderDetail.countOrderDetail ++;
  }
  
  // Reset implemented trade
  asOpenOrder.orderCollection[indexOpenOrder].lifeTimeInSecond = -1;

  // isUpdated 
  memset(asOpenOrder.orderCollection[indexOpenOrder].isUpdated, OPEN_ORDER_UPDATED, MAX_TRADER_TOOL_CONNECTIONS);
  
  if (isNew == -2)
  {
    return -2;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessUpdateASOpenOrderForOrderACK
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessUpdateASOpenOrderForOrderACK(void *_openOrder, void *_orderDetail)
{
  t_OpenOrderInfo *openOrder = (t_OpenOrderInfo *)_openOrder;
  t_OrderDetail *orderDetail = (t_OrderDetail *)_orderDetail; 
  
  int indexOpenOrder = CheckOrderIdIndex(openOrder->clOrdId, openOrder->tsId);

  if (indexOpenOrder == -1)
  {
    TraceLog(ERROR_LEVEL, "CheckOrderIdIndex: tsId = %d, orderId = %d\n", openOrder->tsId, openOrder->clOrdId);

    return ERROR;
  }

  if (indexOpenOrder == -2)
  {
    // In this case, receive recovery message
    /*ACK comes before new orders, put in int wait-list*/
    return -2;
  }
    
  if ((asOpenOrder.orderCollection[indexOpenOrder].orderSummary.clOrdId > 0) && (openOrder->clOrdId != asOpenOrder.orderCollection[indexOpenOrder].orderSummary.clOrdId))
  {
    TraceLog(ERROR_LEVEL, "CheckOrderIdIndex: duplicate hash table, max length = %d. \nNew item: orderId = %d, tsId = %d. \nOld item: orderId = %d, tsId = %d\n", MAX_ORDER_ID_RANGE, openOrder->clOrdId, openOrder->tsId, asOpenOrder.orderCollection[indexOpenOrder].orderSummary.clOrdId, asOpenOrder.orderCollection[indexOpenOrder].orderSummary.tsId);

    return ERROR;
  }

  // Update ACK shares of OUCH and RASH
  if ((asOpenOrder.orderCollection[indexOpenOrder].orderSummary.ECNId == TYPE_NASDAQ_OUCH) ||
    (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.ECNId == TYPE_NASDAQ_RASH))
  {
    if (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.shares > openOrder->shares)
    {
      asOpenOrder.orderCollection[indexOpenOrder].orderSummary.shares = openOrder->shares;
    }

    if (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.leftShares > openOrder->leftShares)
    {
      asOpenOrder.orderCollection[indexOpenOrder].orderSummary.leftShares = openOrder->leftShares;
    }
  }

  // Begin update status ...  
  if (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status > orderStatusIndex.Sent)
  {
    if (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status != orderStatusIndex.CXLSent)
    {
      // Duplicate data, reset several parameters
      TraceLog(ERROR_LEVEL, "(Duplicate data): old (clOrdId: %d, status: %d, tsId: %d, order type: %d) , current (clOrdId: %d, status: ACK, tsId: %d, order type: %d)\n", 
              asOpenOrder.orderCollection[indexOpenOrder].orderSummary.clOrdId, 
              asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status,
              asOpenOrder.orderCollection[indexOpenOrder].orderSummary.tsId,
              asOpenOrder.orderCollection[indexOpenOrder].orderSummary.ECNId,
              openOrder->clOrdId, openOrder->tsId, openOrder->ECNId);

      asOpenOrder.orderCollection[indexOpenOrder].orderSummary.leftShares = asOpenOrder.orderCollection[indexOpenOrder].orderSummary.shares;
      asOpenOrder.orderCollection[indexOpenOrder].orderSummary.avgPrice = asOpenOrder.orderCollection[indexOpenOrder].orderSummary.price;

      asOpenOrder.orderCollection[indexOpenOrder].countOrderDetail = 1;
    }
    //else status == CXLSent : this is the case PM sent Cancel Request before the order ack (Cancel request for a *Sent* order)
  }

  int totalCountOrderDetail = asOrderDetail.countOrderDetail;
  int countOrderDetail = asOpenOrder.orderCollection[indexOpenOrder].countOrderDetail;
  
  // Order detail
  memcpy(&asOrderDetail.orderDetailList[totalCountOrderDetail], orderDetail, sizeof(t_OrderDetail));  
  
  // Order summary  
  asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status = orderStatusIndex.Live;

  // Update index order detail
  asOpenOrder.orderCollection[indexOpenOrder].indexOrderDetailList[countOrderDetail] = totalCountOrderDetail;

  if (asOpenOrder.orderCollection[indexOpenOrder].countOrderDetail + 1 == MAX_ORDER_DETAIL)
  {
    TraceLog(ERROR_LEVEL, "Index of orders detail collection is full, clOrdId = %d, MAX_ORDER_DETAIL = %d\n", openOrder->clOrdId, MAX_ORDER_DETAIL);

    // isUpdated 
    memset(asOpenOrder.orderCollection[indexOpenOrder].isUpdated, OPEN_ORDER_UPDATED, MAX_TRADER_TOOL_CONNECTIONS);

    return ERROR;
  }
  
  // Increase count order detail of open order
  asOpenOrder.orderCollection[indexOpenOrder].countOrderDetail ++;

  if (asOrderDetail.countOrderDetail + 1 == MAX_RAW_DATA_TOTAL_ENTRIES)
  {
    TraceLog(ERROR_LEVEL, "Orders detail collection is full, MAX_RAW_DATA_TOTAL_ENTRIES = %d\n", MAX_RAW_DATA_TOTAL_ENTRIES);

    // isUpdated 
    memset(asOpenOrder.orderCollection[indexOpenOrder].isUpdated, OPEN_ORDER_UPDATED, MAX_TRADER_TOOL_CONNECTIONS);

    return ERROR;
  }

  // Increase count order detail of as
  asOrderDetail.countOrderDetail ++;

  // isUpdated 
  memset(asOpenOrder.orderCollection[indexOpenOrder].isUpdated, OPEN_ORDER_UPDATED, MAX_TRADER_TOOL_CONNECTIONS);

  return indexOpenOrder;
}

/****************************************************************************
- Function name:  ProcessUpdateASOpenOrderForStatus
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessUpdateASOpenOrderForStatus(int tsId, int clOrdId, int orderStatus, void *_orderDetail)
{ 
  t_OrderDetail *orderDetail = (t_OrderDetail *)_orderDetail; 
  
  int indexOpenOrder = CheckOrderIdIndex(clOrdId, tsId);

  if (indexOpenOrder == -1)
  {
    TraceLog(ERROR_LEVEL, "CheckOrderIdIndex: tsId = %d, orderId = %d\n", tsId, clOrdId);

    return ERROR;
  }

  if (indexOpenOrder == -2)
  {
    // Not found
    /*there is no new order in the memory, probably it will come later,
    so put this in wait-list*/
    
    return -2;
  }
  
  if ((asOpenOrder.orderCollection[indexOpenOrder].orderSummary.clOrdId > 0) && (clOrdId != asOpenOrder.orderCollection[indexOpenOrder].orderSummary.clOrdId))
  {
    TraceLog(ERROR_LEVEL, "CheckOrderIdIndex: duplicate hash table, max length = %d. \nNew item: orderId = %d, tsId = %d. \nOld item: orderId = %d, tsId = %d\n", MAX_ORDER_ID_RANGE, clOrdId, tsId, asOpenOrder.orderCollection[indexOpenOrder].orderSummary.clOrdId, asOpenOrder.orderCollection[indexOpenOrder].orderSummary.tsId);

    return ERROR;
  }
  
  if (orderStatus == orderStatusIndex.CXLSent)
  {
    // Reset implemented trade
    asOpenOrder.orderCollection[indexOpenOrder].lifeTimeInSecond = -1;
  }

  // Begin update status ...  
  int totalCountOrderDetail = asOrderDetail.countOrderDetail;
  int countOrderDetail = asOpenOrder.orderCollection[indexOpenOrder].countOrderDetail;

  if (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status != orderStatusIndex.BrokenTrade)
  {   
    if (orderStatus == orderStatusIndex.BrokenTrade)
    {
      if (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status == orderStatusIndex.BrokenTrade)
      {
        return indexOpenOrder;
      }

      // Order summary  
      asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status = orderStatusIndex.BrokenTrade;
    }
    else
    {
      if ((asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status != orderStatusIndex.Closed) &&
        (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status != orderStatusIndex.CanceledByUser))
      {
        if (orderStatus == orderStatusIndex.Rejected)
        {
          if (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status >= orderStatusIndex.CXLSent)
          {
            if (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status == orderStatusIndex.CancelRejected)
            {
              return indexOpenOrder;
            }
                    
            // Order summary  
            asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status = orderStatusIndex.CancelRejected;        
          }
          else
          {
            if (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status == orderStatusIndex.Rejected)
            {
              return indexOpenOrder;
            }

            // Order summary  
            asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status = orderStatusIndex.Rejected;
          }
        }
        else if (orderStatus == orderStatusIndex.CanceledByECN)
        {
          if (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status >= orderStatusIndex.CXLSent)
          {
            if (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status == orderStatusIndex.CanceledByUser)
            {
              return indexOpenOrder;
            }

            // Order summary  
            asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status = orderStatusIndex.CanceledByUser;
          }
          else
          {
            if (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status == orderStatusIndex.CanceledByECN)
            {
              return indexOpenOrder;
            }

            // Order summary  
            asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status = orderStatusIndex.CanceledByECN;
          }
        }
        else if (orderStatus == orderStatusIndex.CXLSent)
        {
          if (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status == orderStatusIndex.CanceledByECN)
          {
            // Order summary  
            asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status = orderStatusIndex.CanceledByUser;
          }
          else
          {
            if (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status == orderStatusIndex.CXLSent)
            {
              return indexOpenOrder;
            }

            // Order summary  
            asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status = orderStatusIndex.CXLSent;
          }          
        }
        else if (orderStatus == orderStatusIndex.CancelACK)
        {
          if (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status == orderStatusIndex.CancelACK)
          {
            return indexOpenOrder;
          }

          // Order summary  
          asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status = orderStatusIndex.CancelACK;
        }
        else if (orderStatus == orderStatusIndex.CancelPending)
        {
          if (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status == orderStatusIndex.CancelPending)
          {
            return indexOpenOrder;
          }

          // Order summary  
          asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status = orderStatusIndex.CancelPending;
        }
      }
    }   
  }
  
  // Order detail
  memcpy(&asOrderDetail.orderDetailList[totalCountOrderDetail], orderDetail, sizeof(t_OrderDetail));  
    
  // Update index order detail
  asOpenOrder.orderCollection[indexOpenOrder].indexOrderDetailList[countOrderDetail] = totalCountOrderDetail;

  if (asOpenOrder.orderCollection[indexOpenOrder].countOrderDetail + 1 == MAX_ORDER_DETAIL)
  {
    TraceLog(ERROR_LEVEL, "Index of orders detail collection is full, clOrdId = %d, MAX_ORDER_DETAIL = %d\n", clOrdId, MAX_ORDER_DETAIL);

    // isUpdated 
    memset(asOpenOrder.orderCollection[indexOpenOrder].isUpdated, OPEN_ORDER_UPDATED, MAX_TRADER_TOOL_CONNECTIONS);

    return ERROR;
  }
  
  // Increase count order detail of open order
  asOpenOrder.orderCollection[indexOpenOrder].countOrderDetail ++;

  if (asOrderDetail.countOrderDetail + 1 == MAX_RAW_DATA_TOTAL_ENTRIES)
  {
    TraceLog(ERROR_LEVEL, "Orders detail collection is full, MAX_RAW_DATA_TOTAL_ENTRIES = %d\n", MAX_RAW_DATA_TOTAL_ENTRIES);

    // isUpdated 
    memset(asOpenOrder.orderCollection[indexOpenOrder].isUpdated, OPEN_ORDER_UPDATED, MAX_TRADER_TOOL_CONNECTIONS);

    return ERROR;
  } 

  // Increase count order detail of as
  asOrderDetail.countOrderDetail ++;

  // isUpdated 
  memset(asOpenOrder.orderCollection[indexOpenOrder].isUpdated, OPEN_ORDER_UPDATED, MAX_TRADER_TOOL_CONNECTIONS);

  return indexOpenOrder;
}

/****************************************************************************
- Function name:  ProcessUpdateASOpenOrderForExecuted
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessUpdateASOpenOrderForExecuted(int tsId, int clOrdId, int orderStatus, int shares, double price, void *_orderDetail, int account, char *liquidity, double *totalFee)
{ 
  t_OrderDetail *orderDetail = (t_OrderDetail *)_orderDetail; 
  
  int indexOpenOrder = CheckOrderIdIndex(clOrdId, tsId);

  if (indexOpenOrder == -1)
  {
    TraceLog(ERROR_LEVEL, "CheckOrderIdIndex: tsId = %d, orderId = %d\n", tsId, clOrdId);
    return ERROR;
  }

  if (indexOpenOrder == -2)
  {
    /*Fill comes before new order, so put it in wait-list*/
    return -2;
  }
  
  if ((asOpenOrder.orderCollection[indexOpenOrder].orderSummary.clOrdId > 0) && (clOrdId != asOpenOrder.orderCollection[indexOpenOrder].orderSummary.clOrdId))
  {
    TraceLog(ERROR_LEVEL, "CheckOrderIdIndex: duplicate hash table, max length = %d. \nNew item: orderId = %d, tsId = %d. \nOld item: orderId = %d, tsId = %d\n", MAX_ORDER_ID_RANGE, clOrdId, tsId, asOpenOrder.orderCollection[indexOpenOrder].orderSummary.clOrdId, asOpenOrder.orderCollection[indexOpenOrder].orderSummary.tsId);

    return ERROR;
  }

  int numSec = GetNumberOfSecondsFromTimestamp(&orderDetail->timestamp[11]);
  
  int ecnId = asOpenOrder.orderCollection[indexOpenOrder].orderSummary.ECNId;
  int side = asOpenOrder.orderCollection[indexOpenOrder].orderSummary.side;
  
  *totalFee = CalculateFillFee(shares, price, GetSide(side), liquidity, ecnId);

  // Calculation real value of stuck
  if (tsId == MANUAL_ORDER)
  {
    CalculateRealValue(asOpenOrder.orderCollection[indexOpenOrder].orderSummary.symbol, 
              side, shares, price, account, *totalFee);
  }
  
  UpdateRiskManagementCollection(asOpenOrder.orderCollection[indexOpenOrder].orderSummary.symbol, 
                shares, price, side, 
                numSec, account, *totalFee);
  
  int totalCountOrderDetail = asOrderDetail.countOrderDetail;
  int countOrderDetail = asOpenOrder.orderCollection[indexOpenOrder].countOrderDetail;

  int currentStatus = asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status;
      
  if (orderStatus == orderStatusIndex.Closed)
  {
    if (asOpenOrder.orderCollection[indexOpenOrder].isNewOrder != 0)
    {
      if (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.account == -1)
      {
        asOpenOrder.orderCollection[indexOpenOrder].orderSummary.account = account;
      }
      
      if (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.shares == 0)
      {
        asOpenOrder.orderCollection[indexOpenOrder].orderSummary.shares = shares;
      }

      if (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.leftShares == 0)
      {
        asOpenOrder.orderCollection[indexOpenOrder].orderSummary.leftShares = shares;
      }
      
      if (feq(asOpenOrder.orderCollection[indexOpenOrder].orderSummary.price, 0.00))
      {
        asOpenOrder.orderCollection[indexOpenOrder].orderSummary.price = price;
        asOpenOrder.orderCollection[indexOpenOrder].orderSummary.avgPrice = price;
      }
    }

    if (shares > asOpenOrder.orderCollection[indexOpenOrder].orderSummary.leftShares)
    {
      TraceLog(ERROR_LEVEL, "filled shares (%d) > leftShares (%d), clOrdId = %d\n", shares, asOpenOrder.orderCollection[indexOpenOrder].orderSummary.leftShares, asOpenOrder.orderCollection[indexOpenOrder].orderSummary.clOrdId);

      return ERROR;
    }

    // avgPrice
    asOpenOrder.orderCollection[indexOpenOrder].orderSummary.avgPrice = ((asOpenOrder.orderCollection[indexOpenOrder].orderSummary.shares - 
                                      asOpenOrder.orderCollection[indexOpenOrder].orderSummary.leftShares) * 
                                      asOpenOrder.orderCollection[indexOpenOrder].orderSummary.avgPrice + 
                                      shares * price) / 
                                      ((asOpenOrder.orderCollection[indexOpenOrder].orderSummary.shares - 
                                      asOpenOrder.orderCollection[indexOpenOrder].orderSummary.leftShares) + 
                                      shares);
    // leftShares
    asOpenOrder.orderCollection[indexOpenOrder].orderSummary.leftShares -= shares;

    UpdateTotalSharesFilled(tsId, shares);

    if (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.leftShares == 0)
    { 
      // Order summary  
      asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status = orderStatusIndex.Closed;        
    }
  }

  if (currentStatus == orderStatusIndex.BrokenTrade)
  {
    asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status = orderStatusIndex.BrokenTrade;
  }
  
  // Order detail
  memcpy(&asOrderDetail.orderDetailList[totalCountOrderDetail], orderDetail, sizeof(t_OrderDetail));
    
  // Update index order detail
  asOpenOrder.orderCollection[indexOpenOrder].indexOrderDetailList[countOrderDetail] = totalCountOrderDetail;

  if (asOpenOrder.orderCollection[indexOpenOrder].countOrderDetail + 1 == MAX_ORDER_DETAIL)
  {
    TraceLog(ERROR_LEVEL, "Index of orders detail collection is full, clOrdId = %d, MAX_ORDER_DETAIL = %d\n", clOrdId, MAX_ORDER_DETAIL);

    // isUpdated 
    memset(asOpenOrder.orderCollection[indexOpenOrder].isUpdated, OPEN_ORDER_UPDATED, MAX_TRADER_TOOL_CONNECTIONS);

    return ERROR;
  }
  
  // Increase count order detail of open order
  asOpenOrder.orderCollection[indexOpenOrder].countOrderDetail ++;

  if (asOrderDetail.countOrderDetail + 1 == MAX_RAW_DATA_TOTAL_ENTRIES)
  {
    TraceLog(ERROR_LEVEL, "Orders detail collection is full, MAX_RAW_DATA_TOTAL_ENTRIES = %d\n", MAX_RAW_DATA_TOTAL_ENTRIES);

    // isUpdated 
    memset(asOpenOrder.orderCollection[indexOpenOrder].isUpdated, OPEN_ORDER_UPDATED, MAX_TRADER_TOOL_CONNECTIONS);

    return ERROR;
  } 

  // Increase count order detail of as
  asOrderDetail.countOrderDetail ++;
  
  // isUpdated 
  memset(asOpenOrder.orderCollection[indexOpenOrder].isUpdated, OPEN_ORDER_UPDATED, MAX_TRADER_TOOL_CONNECTIONS);

  return indexOpenOrder;
}

/****************************************************************************
- Function name:  ProcessUpdateASOpenOrderForNonExecuteOrderDetail
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessUpdateASOpenOrderForNonExecuteOrderDetail(int tsId, int clOrdId, void *_orderDetail)
{ 
  t_OrderDetail *orderDetail = (t_OrderDetail *)_orderDetail; 
  
  int indexOpenOrder = CheckOrderIdIndex(clOrdId, tsId);

  if (indexOpenOrder == -1)
  {
    TraceLog(ERROR_LEVEL, "CheckOrderIdIndex: tsId = %d, orderId = %d\n", tsId, clOrdId);

    return ERROR;
  }

  if (indexOpenOrder == -2)
  {
    // Not found
    TraceLog(ERROR_LEVEL, "can't find new order: tsId = %d, clOrdId = %d\n", tsId, clOrdId);

    return ERROR;
  }
  
  if ((asOpenOrder.orderCollection[indexOpenOrder].orderSummary.clOrdId > 0) && (clOrdId != asOpenOrder.orderCollection[indexOpenOrder].orderSummary.clOrdId))
  {
    TraceLog(ERROR_LEVEL, "CheckOrderIdIndex: duplicate hash table, max length = %d. \nNew item: orderId = %d, tsId = %d. \nOld item: orderId = %d, tsId = %d\n", MAX_ORDER_ID_RANGE, clOrdId, tsId, asOpenOrder.orderCollection[indexOpenOrder].orderSummary.clOrdId, asOpenOrder.orderCollection[indexOpenOrder].orderSummary.tsId);

    return ERROR;
  }

  int totalCountOrderDetail = asOrderDetail.countOrderDetail;
  int countOrderDetail = asOpenOrder.orderCollection[indexOpenOrder].countOrderDetail;

  // Order detail
  memcpy(&asOrderDetail.orderDetailList[totalCountOrderDetail], orderDetail, sizeof(t_OrderDetail));
  
  // isUpdated 
  memset(asOpenOrder.orderCollection[indexOpenOrder].isUpdated, OPEN_ORDER_UPDATED, MAX_TRADER_TOOL_CONNECTIONS);
    
  // Update index order detail
  asOpenOrder.orderCollection[indexOpenOrder].indexOrderDetailList[countOrderDetail] = totalCountOrderDetail;

  if (asOpenOrder.orderCollection[indexOpenOrder].countOrderDetail + 1 == MAX_ORDER_DETAIL)
  {
    TraceLog(ERROR_LEVEL, "Index of orders detail collection is full, clOrdId = %d, MAX_ORDER_DETAIL = %d\n", clOrdId, MAX_ORDER_DETAIL);

    // isUpdated 
    memset(asOpenOrder.orderCollection[indexOpenOrder].isUpdated, OPEN_ORDER_UPDATED, MAX_TRADER_TOOL_CONNECTIONS);

    return ERROR;
  }
  
  // Increase count order detail of open order
  asOpenOrder.orderCollection[indexOpenOrder].countOrderDetail ++;

  if (asOrderDetail.countOrderDetail + 1 == MAX_RAW_DATA_TOTAL_ENTRIES)
  {
    TraceLog(ERROR_LEVEL, "Orders detail collection is full, MAX_RAW_DATA_TOTAL_ENTRIES = %d\n", MAX_RAW_DATA_TOTAL_ENTRIES);

    // isUpdated 
    memset(asOpenOrder.orderCollection[indexOpenOrder].isUpdated, OPEN_ORDER_UPDATED, MAX_TRADER_TOOL_CONNECTIONS);

    return ERROR;
  } 

  // Increase count order detail of as
  asOrderDetail.countOrderDetail ++;

  return indexOpenOrder;
}

/****************************************************************************
- Function name:  ProcessASConnectOrderToECN
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessASConnectToOrderECN(int ECNIndex)
{
  // For test 
  //traderToolsInfo.orderConnection[ECNIndex].status = CONNECT;
  
  switch (ECNIndex)
  {
    case AS_NASDAQ_RASH_INDEX:
      if (orderStatusMgmt[AS_NASDAQ_RASH_INDEX].isOrderConnected == DISCONNECTED &&
        orderStatusMgmt[AS_NASDAQ_RASH_INDEX].shouldConnect != YES)
      {
        orderStatusMgmt[AS_NASDAQ_RASH_INDEX].shouldConnect = YES;
        
        pthread_t nasdaq_rash_Thread;
        pthread_create(&nasdaq_rash_Thread, NULL, (void *)&StartUp_NASDAQ_RASH_Module, NULL);
      }
      break;
      
    case AS_ARCA_DIRECT_INDEX:
      if (orderStatusMgmt[AS_ARCA_DIRECT_INDEX].isOrderConnected == DISCONNECTED &&
        orderStatusMgmt[AS_ARCA_DIRECT_INDEX].shouldConnect != YES)
      {
        orderStatusMgmt[AS_ARCA_DIRECT_INDEX].shouldConnect = YES;
        
        pthread_t arca_direct_Thread;
        pthread_create(&arca_direct_Thread, NULL, (void *)&StartUp_ARCA_DIRECT_Module, NULL);
      }
      break;

    default:
      TraceLog(ERROR_LEVEL, "received request to connect to ECN Order with unknown ID (%d)\n", ECNIndex);
      break;
  } 
  
  //TraceLog(DEBUG_LEVEL, "Connect to ECN order: ECNIndex = %d, status = %d\n", ECNIndex, traderToolsInfo.orderConnection[ECNIndex].status);  
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessASDisconnectToOrderECN
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessASDisconnectToOrderECN(int ECNIndex)
{
  // For test 
  //traderToolsInfo.orderConnection[ECNIndex].status = DISCONNECT;

  switch (ECNIndex)
  {
    case AS_NASDAQ_RASH_INDEX:
      if (orderStatusMgmt[AS_NASDAQ_RASH_INDEX].isOrderConnected == CONNECTED)
      {
        orderStatusMgmt[AS_NASDAQ_RASH_INDEX].shouldConnect = NO;
      }
      else
      {
        traderToolsInfo.orderConnection[AS_NASDAQ_RASH_INDEX].status = DISCONNECT;
      }
      break;
    case AS_ARCA_DIRECT_INDEX:
      orderStatusMgmt[AS_ARCA_DIRECT_INDEX].shouldConnect = NO; 
      
      shutdown(ARCA_DIRECT_Config.socket, SHUT_RDWR);
      close(ARCA_DIRECT_Config.socket);
      ARCA_DIRECT_Config.socket = -1;
      
      if (orderStatusMgmt[AS_ARCA_DIRECT_INDEX].isOrderConnected == DISCONNECTED)
      {       
        traderToolsInfo.orderConnection[AS_ARCA_DIRECT_INDEX].status = DISCONNECT;
      }     
      break;

    default:
      break;
  }
    
  //TraceLog(DEBUG_LEVEL, "Disconnect to ECN order: ECNIndex = %d, status = %d\n", ECNIndex, traderToolsInfo.orderConnection[ECNIndex].status);

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessASConnectToAllOrderECN
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessASConnectToAllOrderECN(void)
{
  int ECNIndex;

  for (ECNIndex = 0; ECNIndex < AS_MAX_ORDER_CONNECTIONS; ECNIndex ++)
  {
    if (asSetting.orderEnable[ECNIndex] == YES)
    {
      ProcessASConnectToOrderECN(ECNIndex);

      // Delay in micro second(s)
      usleep(10);
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessASDisconnectToAllOrderECN
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessASDisconnectToAllOrderECN(void)
{
  int ECNIndex;

  for (ECNIndex = 0; ECNIndex < AS_MAX_ORDER_CONNECTIONS; ECNIndex ++)
  {
    ProcessASDisconnectToOrderECN(ECNIndex);

    // Delay in micro second(s)
    usleep(10);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendNewOrderToNASDAQ_RASHOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendNewOrderToNASDAQ_RASHOrder(void *order)
{
  t_SendNewOrder *tmpOrder = (t_SendNewOrder *)order;
  t_NASDAQ_RASH_NewOrder newOrder;
  
  //orderToken
  char orderToken[16];
  /*
  code added for safety
  */
  memset(orderToken, 0, 16);
  sprintf(orderToken, "%d", tmpOrder->orderId);

  int len = strlen(orderToken);

  memset(&orderToken[len], ' ', 14 - len);
  orderToken[14] = 0;

  newOrder.orderToken = orderToken;

  //TraceLog(DEBUG_LEVEL, "orderToken = %s\n", newOrder.orderToken);
  
  //side
  if (tmpOrder->side == BUY_TO_CLOSE_TYPE)
  {
    newOrder.side = 'B';
    newOrder.buyLaunch = BUY_TO_CLOSE;
  }
  else if (tmpOrder->side == SELL_TYPE)
  {
    newOrder.side = 'S';
  }
  else if (tmpOrder->side == SHORT_SELL_TYPE)
  {
    newOrder.side = 'S';
  }
  else 
  {
    newOrder.side = 'B';
    newOrder.buyLaunch = BUY_TO_OPEN;
  }
  
  //share
  newOrder.share = tmpOrder->shares;
  
  //stock
  char symbol[SYMBOL_LEN];
  memset(symbol, ' ', SYMBOL_LEN);
  
  int lenSymbol = strnlen(tmpOrder->symbol, SYMBOL_LEN);

  memcpy(symbol, tmpOrder->symbol, lenSymbol);

  newOrder.stock = symbol;

  //TraceLog(DEBUG_LEVEL, "stock = %.8s\n", newOrder.stock);
  
  //price
  newOrder.price = tmpOrder->price;

  // pegDef
  newOrder.pegDef = tmpOrder->pegDef;
  
  //timeInForce
  if (tmpOrder->tif == 0 || tmpOrder->tif == 1)
  {
    newOrder.timeInForce = 0;
  }
  else
  {
    newOrder.timeInForce = 99999;
  }

  //firm
  newOrder.firm = MPID_NASDAQ_RASH;

  //display
  if (tmpOrder->maxFloor == 0)
  {
    newOrder.display = 'N';
  }
  else
  {
    newOrder.display = 'Y';
  }

  /*
  Max Floor
  ...
  */
  newOrder.maxFloor = tmpOrder->maxFloor;

  newOrder.tradingAccount = tmpOrder->tradingAccount;

  TraceLog(DEBUG_LEVEL, "Begin build and send NASDAQ RASH order...\n");
  
  int index = tmpOrder->orderId % MAX_MANUAL_ORDER;
  memset(&AutoCancelOrder.orderInfo[index], 0, sizeof(t_CancelOrderInfo));
  
  AutoCancelOrder.orderInfo[index].ttl = tmpOrder->tif;
  AutoCancelOrder.orderInfo[index].clOrderId = tmpOrder->orderId;

  int retval = BuildAndSend_NASDAQ_RASH_OrderMsg(&newOrder);
  
  if (retval == SUCCESS)
  {
    TraceLog(DEBUG_LEVEL, "Send Order to NASDAQ RASH Server successfully\n");
  }
  else
  {
    TraceLog(ERROR_LEVEL, "Failed to send Order to NASDAQ RASH Server\n");

    return ERROR;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendNewOrderToARCA_DIRECTOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendNewOrderToARCA_DIRECTOrder(void *order)
{

/*
  int maxFloor;
*/
  t_SendNewOrder *tmpOrder = (t_SendNewOrder *)order;
  t_ARCA_DIRECT_NewOrder newOrder;

  newOrder.clOrdID = tmpOrder->orderId;

  //TraceLog(DEBUG_LEVEL, "Client Order ID = %d\n", newOrder.clOrdID);
  
  /*
  Side values:
  Buy=�1�
  Sell=�2�
  Sell Short=�5� (client affirms ability to borrow)
  */
  if (tmpOrder->side == BUY_TO_CLOSE_TYPE)
  {
    newOrder.side = '1';
    newOrder.buyLaunch = BUY_TO_CLOSE;
  }
  else if (tmpOrder->side == SELL_TYPE)
  {
    newOrder.side = '2';
  }
  else if (tmpOrder->side == SHORT_SELL_TYPE)
  {
    newOrder.side = '5';
  }
  else 
  {
    newOrder.side = '1';
    newOrder.buyLaunch = BUY_TO_OPEN;
  }
  
  // Share Volume
  newOrder.orderQty = tmpOrder->shares;
  
  // Stock Symbol
  newOrder.symbol = tmpOrder->symbol;

  //TraceLog(DEBUG_LEVEL, "Stock Symbol = %.8s\n", newOrder.symbol);
  
  // Price
  newOrder.price = tmpOrder->price;
  
  /*
  Time In Force values
  �0� = DAY (expires at end of day)
  �1� = GTC (allowed, but treated same as Day)
  �3� = IOC (portion not filled immediately is cancelled. Market orders are implicitly IOC)
  �6� = GTD (expires at earlier of specified ExpireTime or end of day)
  */
  if (tmpOrder->tif == 0 || tmpOrder->tif == 1)
  {
    newOrder.timeInForce = '3';
  }
  else // IOC
  {
    newOrder.timeInForce = '0';
  }
  
  /*
  Max Floor
  ...
  */
  newOrder.maxFloor = tmpOrder->maxFloor;
  
  //Trading account
  newOrder.tradingAccount = tmpOrder->tradingAccount;
  
  /*
  Build and send ARCA DIRECT Order server
  */
  TraceLog(DEBUG_LEVEL, "Begin build and send ARCA DIRECT order...\n");
  
  int index = tmpOrder->orderId % MAX_MANUAL_ORDER;
  memset(&AutoCancelOrder.orderInfo[index], 0, sizeof(t_CancelOrderInfo));
  
  AutoCancelOrder.orderInfo[index].ttl = tmpOrder->tif;
  AutoCancelOrder.orderInfo[index].clOrderId = tmpOrder->orderId;
    
  if(tmpOrder->tif > 1 && tmpOrder->tif < 86400)
  {
    if(AutoCancelOrder.threadStarted == 0)
    {
      AutoCancelOrder.threadStarted = 1;
      pthread_t cancelOrderThread;
      pthread_create(&cancelOrderThread, NULL, (void *)&StartSendCancelOrderThread, NULL);
    }
  }

  // WARNING: Update here
  int retval = ERROR;
  if(newOrder.maxFloor >= 100 && (newOrder.maxFloor % 100 == 0))
  {
    retval = BuildAndSend_ARCA_DIRECT_OrderMsg_V3(&newOrder);
  }
  else
  {
    retval = BuildAndSend_ARCA_DIRECT_OrderMsg_V1(&newOrder);
  }
  
  if (retval == SUCCESS)
  {
    TraceLog(DEBUG_LEVEL, "Send Order to ARCA DIRECT Server successfully\n");
  }
  else
  {
    TraceLog(ERROR_LEVEL, "Failed to send Order to ARCA DIRECT Server\n");
    return ERROR;
  }
  
  return SUCCESS;
}

void* StartSendCancelOrderThread(void *arg)
{
  int i;
  TraceLog(DEBUG_LEVEL, "Start Thread to send cancel-order\n");
  
  while(1)
  {
    for(i = 0; i < MAX_MANUAL_ORDER; i++)
    {
      if(AutoCancelOrder.orderInfo[i].marked == 1 && AutoCancelOrder.orderInfo[i].leftShares > 0)
      {
        if(orderStatusMgmt[AS_ARCA_DIRECT_INDEX].isOrderConnected == DISCONNECTED)
        {
          continue;
        }
        
        AutoCancelOrder.orderInfo[i].ttl--;
        if(AutoCancelOrder.orderInfo[i].ttl == 0)
        {
          AutoCancelOrder.orderInfo[i].marked = 0;
          
          int clOrdId = AutoCancelOrder.orderInfo[i].clOrderId;
          char *ecnOrderID = AutoCancelOrder.orderInfo[i].ecnOrderID;
          
          ProcessSendCancelRequestToECNOrder(clOrdId, ecnOrderID);
        }
      }
    }
    
    sleep(1);
  }
  
  AutoCancelOrder.threadStarted = 0;
  
  TraceLog(DEBUG_LEVEL, "Return thread to send cancel-order.\n");
  
  return NULL;
}

/****************************************************************************
- Function name:  ProcessSendManualCancelRequestToPM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessSendManualCancelRequestToPM(int clOrdId, char *ECNOrderId)
{
  int indexOpenOrder = CheckOrderIdIndex(clOrdId, MANUAL_ORDER);

  if ((indexOpenOrder == -1) || (indexOpenOrder == -2))
  {
    TraceLog(ERROR_LEVEL, "Cannot find order information with clOrdId = %d and tsId = %d\n", clOrdId, MANUAL_ORDER);

    return ERROR;
  }

  //Add required info so PM will be able to create the cancel message
  t_ManualCancelOrder cancelOrder;
  memset(&cancelOrder, 0, sizeof(t_ManualCancelOrder));
  
  switch (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.ECNId)
  {
    case TYPE_NASDAQ_RASH:
      //For RASH cancel order msg, only "order token" is required
      sprintf(cancelOrder.orderToken, "%d", asOpenOrder.orderCollection[indexOpenOrder].orderSummary.clOrdId);
      TraceLog(DEBUG_LEVEL, "orderToken: '%s'\n", cancelOrder.orderToken);
      break;

    case TYPE_ARCA_DIRECT:
      //For ARCA DIRECT cancel order msg, many info is required:
      
      // Client order Id
      sprintf(cancelOrder.origClOrdID, "%d", asOpenOrder.orderCollection[indexOpenOrder].orderSummary.clOrdId);
      
      // ARCA DIRECT Order Id
      strcpy(cancelOrder.ecnOrderID, ECNOrderId);
      cancelOrder.ecnOrderID[20] = 0;
      
      //assign new value to OrderNextID
      pthread_mutex_lock(&nextOrderID_Mutex); 
        int nextOrderId = clientOrderID;
        clientOrderID += 1;
        SaveClientOrderID("next_order_id", clientOrderID);
      pthread_mutex_unlock(&nextOrderID_Mutex);
      sprintf(cancelOrder.newClOrdID , "%d", nextOrderId);
      
      // shares
      cancelOrder.orderQty = asOpenOrder.orderCollection[indexOpenOrder].orderSummary.shares;

      // Symbol
      strncpy(cancelOrder.symbol, asOpenOrder.orderCollection[indexOpenOrder].orderSummary.symbol, SYMBOL_LEN);

      // Side
      if (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.side == BUY_TO_CLOSE_TYPE)
      {
        cancelOrder.side = '1';
      }
      else if (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.side == SELL_TYPE)
      {
        cancelOrder.side = '2';
      }
      else if (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.side == SHORT_SELL_TYPE)
      {
        cancelOrder.side = '5';
      }
      else 
      {
        cancelOrder.side = '6';
      }
      break;
  }

  SendManualCancelRequestToPM(&cancelOrder, asOpenOrder.orderCollection[indexOpenOrder].orderSummary.ECNId);
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessSendCancelRequestToECNOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessSendCancelRequestToECNOrder(int clOrdId, char *ECNOrderId)
{
  int indexOpenOrder = CheckOrderIdIndex(clOrdId, MANUAL_ORDER);

  if ((indexOpenOrder == -1) || (indexOpenOrder == -2))
  {
    TraceLog(ERROR_LEVEL, "Cannot find order information with clOrdId = %d and tsId = %d\n", clOrdId, MANUAL_ORDER);

    return ERROR;
  }

  switch (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.ECNId)
  {
    case TYPE_NASDAQ_RASH:
      ProcessSendCancelRequestToNASDAQ_RASH(indexOpenOrder);
      break;

    case TYPE_ARCA_DIRECT:
      //TraceLog(DEBUG_LEVEL, "ECNOrderId = %s\n", ECNOrderId);
      ProcessSendCancelRequestToARCA_DIRECT(indexOpenOrder, ECNOrderId);
      break;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessSendCancelRequestToNASDAQ_RASH
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessSendCancelRequestToNASDAQ_RASH(int indexOpenOrder)
{
  t_NASDAQ_RASH_CancelOrder cancelOrder;

  //orderToken
  char orderToken[16] = "\0";
  sprintf(orderToken, "%d", asOpenOrder.orderCollection[indexOpenOrder].orderSummary.clOrdId);

  TraceLog(DEBUG_LEVEL, "orderToken = %s\n", orderToken);

  int len = strlen(orderToken);

  memset(&orderToken[len], ' ', 14 - len);
  orderToken[14] = 0;

  cancelOrder.orderToken = orderToken;

  //trading account
  if (Manual_Order_Account_Mapping[asOpenOrder.orderCollection[indexOpenOrder].orderSummary.clOrdId] == -1)
  {
    Manual_Order_Account_Mapping[asOpenOrder.orderCollection[indexOpenOrder].orderSummary.clOrdId] = asOpenOrder.orderCollection[indexOpenOrder].orderSummary.account;
  }
  
  cancelOrder.tradingAccount = Manual_Order_Account_Mapping[asOpenOrder.orderCollection[indexOpenOrder].orderSummary.clOrdId];
  
  //share
  cancelOrder.share = asOpenOrder.orderCollection[indexOpenOrder].orderSummary.shares;

  TraceLog(DEBUG_LEVEL, "share = %d\n", cancelOrder.share);

  // Call function to send cancel request
  TraceLog(DEBUG_LEVEL, "Begin build and send cancel request to NASDAQ RASH server  ...\n");

  int retval = BuildAndSend_NASDAQ_RASH_CancelMsg(&cancelOrder);

  if (retval == SUCCESS)
  {
    TraceLog(DEBUG_LEVEL, "Send cancel request to NASDAQ RASH Server successfully\n");

    return SUCCESS;
  }
  else
  {
    TraceLog(ERROR_LEVEL, "Failed to send cancel request to NASDAQ RASH Server\n");

    return ERROR;
  }

}

/****************************************************************************
- Function name:  ProcessSendCancelRequestToARCA_DIRECT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessSendCancelRequestToARCA_DIRECT(int indexOpenOrder, char *ECNOrderId)
{
  //TraceLog(DEBUG_LEVEL, "Inside function ProcessSendCancelRequestToARCA_DIRECT\n");
  
  t_ARCA_DIRECT_CancelOrder cancelOrder;

  // Client order Id
  cancelOrder.origClOrdID = asOpenOrder.orderCollection[indexOpenOrder].orderSummary.clOrdId;

  // ARCA DIRECT Order Id
  cancelOrder.ecnOrderID = atol(ECNOrderId);
  
  //trading account
  if (Manual_Order_Account_Mapping[asOpenOrder.orderCollection[indexOpenOrder].orderSummary.clOrdId] == -1)
  {
    Manual_Order_Account_Mapping[asOpenOrder.orderCollection[indexOpenOrder].orderSummary.clOrdId] = asOpenOrder.orderCollection[indexOpenOrder].orderSummary.account;
  }
  
  cancelOrder.accountSuffix = Manual_Order_Account_Mapping[asOpenOrder.orderCollection[indexOpenOrder].orderSummary.clOrdId];

  // shares
  cancelOrder.orderQty = asOpenOrder.orderCollection[indexOpenOrder].orderSummary.shares;

  // Symbol
  strncpy(cancelOrder.symbol, asOpenOrder.orderCollection[indexOpenOrder].orderSummary.symbol, SYMBOL_LEN);

  // Side
  if (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.side == BUY_TO_CLOSE_TYPE)
  {
    cancelOrder.side = '1';
  }
  else if (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.side == SELL_TYPE)
  {
    cancelOrder.side = '2';
  }
  else if (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.side == SHORT_SELL_TYPE)
  {
    cancelOrder.side = '5';
  }
  else 
  {
    cancelOrder.side = '6';
  }

  // Call function to send cancel request
  TraceLog(DEBUG_LEVEL, "Begin build and send cancel request to ARCA DIRECT server  ...\n");

  int retval = BuildAndSend_ARCA_DIRECT_CancelMsg(&cancelOrder);

  if (retval == SUCCESS)
  {
    TraceLog(DEBUG_LEVEL, "Send cancel request to ARCA DIRECT Server successfully\n");

    return SUCCESS;
  }
  else
  {
    TraceLog(ERROR_LEVEL, "Failed to send cancel request to ARCA DIRECT Server\n");

    return ERROR;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  GetOrderStatus
- Input:      + statusIndex
- Output:   
- Return:   
- Description:  + 
- Usage:    
****************************************************************************/
char *GetOrderStatus(int statusIndex)
{
  switch ( statusIndex )
  {
    case 0:
      return orderStatus.Sent;
      break;
    
    case 1: 
      return orderStatus.Live;
      break;
      
    case 2:
      return orderStatus.Rejected;
      break;
    
    case 3:
      return orderStatus.Closed;
      break;
    
    case 4:
      return orderStatus.BrokenTrade;
      break;
    
    case 5:
      return orderStatus.CanceledByECN;
      break;
      
    case 6:
      return orderStatus.CXLSent;
      break;
    
    case 7:
      return orderStatus.CancelACK;
      break;
      
    case 8:
      return orderStatus.CancelRejected;
      break;
      
    case 9:
      return orderStatus.CancelPending;
      break;
    
    case 10:
      return orderStatus.CanceledByUser;   
      break;
      
    default:
      return NULL;
  }
}

/****************************************************************************
- Function name:  GetOrderStatusIndex
- Input:      + statusIndex
- Output:   
- Return:   
- Description:  + 
- Usage:    
****************************************************************************/
int GetOrderStatusIndex(const char *status)
{
  if (strcmp(status, orderStatus.Closed) == 0)
  {
    return orderStatusIndex.Closed;
  }
  else if (strcmp(status, orderStatus.CanceledByECN) == 0)
  {
    return orderStatusIndex.CanceledByECN;
  }
  else if (strcmp(status, orderStatus.CanceledByUser) == 0)
  {
    return orderStatusIndex.CanceledByUser;
  }
  else if (strcmp(status, orderStatus.Live) == 0)
  {
    return orderStatusIndex.Live;
  }
  else if (strcmp(status, orderStatus.Rejected) == 0)
  {
    return orderStatusIndex.Rejected;
  }
  else if (strcmp(status, orderStatus.CancelRejected) == 0)
  {
    return orderStatusIndex.CancelRejected;
  }
  else if (strcmp(status, orderStatus.BrokenTrade) == 0)
  {
    return orderStatusIndex.BrokenTrade;
  }
  else if (strcmp(status, orderStatus.CancelPending) == 0)
  {
    return orderStatusIndex.CancelPending;
  }
  else if (strcmp(status, orderStatus.CXLSent) == 0)
  {
    return orderStatusIndex.CXLSent;
  }
  else if (strcmp(status, orderStatus.CancelACK) == 0)
  {
    return orderStatusIndex.CancelACK;
  }
  else if (strcmp(status, orderStatus.Sent) == 0)
  {
    return orderStatusIndex.Sent;
  }

  return ERROR;
}

/****************************************************************************
- Function name:  GetSide
- Input:      + side
- Output:   
- Return:   
- Description:
- Usage:    
****************************************************************************/
int GetSide(int side)
{
  switch (side)
  {
    case BUY_TO_CLOSE_TYPE:
      return BUY_SIDE;
    case SELL_TYPE:
      return SELL_SIDE;
    case SHORT_SELL_TYPE:
      return SELL_SIDE;
    case BUY_TO_OPEN_TYPE:
      return BUY_SIDE;
    default:
      return 0;
  }
}

/****************************************************************************
- Function name:  UpdateRiskManagementCollection
- Input:      + synbol
        + shareVolume
        + sharePrice
        + side
- Output:   
- Return:   
- Description:
- Usage:    
****************************************************************************/
int UpdateRiskManagementCollection(char *symbol, int shareVolume, double sharePrice, int side, int timestamp, int account, double totalFee)
{ 
  int stockSymbolIndex;
  t_RiskManagementInfo *rmInfo;
  int currentSide;
    
  // Get stock symbol index
  stockSymbolIndex = GetStockSymbolIndex(symbol); 
  
  if (stockSymbolIndex == -1)
  {
    //TraceLog(ERROR_LEVEL, "(UpdateRiskManagementCollection): Cannot find index for sumbol %s\n", symbol);
    return -1;
  }
  
  // Convert old side format to new format
  currentSide = GetSide(side);
  
  /*
  Get current risk management 
  information of current stock symbol
  */
  rmInfo = &RMList.collection[account][stockSymbolIndex];

  // Timestamp of last update
  rmInfo->timestamp = timestamp;
  
  // Calculate total Shares
  rmInfo->totalShares += shareVolume;
  
  // Calculate Left Shares
  int oldLeftShares = rmInfo->leftShares;
  
  rmInfo->leftShares += (currentSide * shareVolume);

  // Total fee
  rmInfo->totalFee += totalFee;
  
  RMList.symbolList[stockSymbolIndex].lastTradedPrice = sharePrice;
  
  // Calculate AVG price
  if (oldLeftShares == 0 || (currentSide * oldLeftShares > 0))
  {
    rmInfo->avgPrice = (currentSide * oldLeftShares * rmInfo->avgPrice + shareVolume * sharePrice) / 
            (currentSide * oldLeftShares + shareVolume);
  }
  else if (oldLeftShares != 0 && (currentSide * oldLeftShares < 0) )
  {
    // Calculate Match PL
    if (currentSide == SELL_SIDE)
    {
      rmInfo->matchedPL += (sharePrice - rmInfo->avgPrice) * (abs(oldLeftShares) > 
                shareVolume?shareVolume:abs(oldLeftShares));
    }
    else
    {
      rmInfo->matchedPL += (rmInfo->avgPrice - sharePrice) * (abs(oldLeftShares) > 
                shareVolume?shareVolume:abs(oldLeftShares));    
    }
    
    // Update AVG price
    if ((oldLeftShares * currentSide * (-1)) < shareVolume)
    {
      rmInfo->avgPrice = sharePrice;
    }
    else if ((oldLeftShares * currentSide * (-1)) > shareVolume)
    {
      // No change
    }
    else
    {
      rmInfo->avgPrice = 0.00;
    }
  }
  
  // Total Market
  rmInfo->totalMarket = fabs(rmInfo->avgPrice * rmInfo->leftShares);
    
  /*
  TraceLog(DEBUG_LEVEL, "After update\n");
  TraceLog(DEBUG_LEVEL, "------------------------------------------------\n");
  TraceLog(DEBUG_LEVEL, "leftShares = %d\n", rmInfo->leftShares);
  TraceLog(DEBUG_LEVEL, "totalShares = %d\n", rmInfo->totalShares);
  TraceLog(DEBUG_LEVEL, "symbol = %.8s\n", symbol);
  TraceLog(DEBUG_LEVEL, "avgPrice = %lf\n", rmInfo->avgPrice);
  TraceLog(DEBUG_LEVEL, "matchedPL = %lf\n", rmInfo->matchedPL);
  TraceLog(DEBUG_LEVEL, "totalMarket = %lf\n", rmInfo->totalMarket);
  TraceLog(DEBUG_LEVEL, "lastTradedPrice = %lf\n", rmInfo->lastTradedPrice);
  TraceLog(DEBUG_LEVEL, "------------------------------------------------\n\n");
  */
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  CalculationPositionBuyingPower
- Input:      
- Output:   
- Return:   
- Description:
- Usage:    
****************************************************************************/
void CalculationPositionBuyingPower()
{
  int rmInfoIndex, timestamp;

  // Get number of seconds
  timestamp = GetNumberOfSecondsOfLocalTime();

  int account;
  for (account = 0; account < MAX_ACCOUNT; account++)
  {
    positionBuyingPower[account] = 0.00;
    for (rmInfoIndex = 0; rmInfoIndex < RMList.countSymbol; rmInfoIndex++)
    {
      if ((RMList.collection[account][rmInfoIndex].leftShares != 0) && ((timestamp - RMList.collection[account][rmInfoIndex].timestamp) > MAX_SECONDS_INCREASE_POSITION))
      {
        positionBuyingPower[account] += fabs(RMList.collection[account][rmInfoIndex].totalMarket);        
      }
    }
  }
}

/****************************************************************************
- Function name:  UpdateNumberOfOrderPlaced
- Input:      
- Output:   
- Return:   
- Description:
- Usage:    
****************************************************************************/
int UpdateNumberOfOrderPlaced(int tsId)
{
  int tsIndex;

  if (tsId != MANUAL_ORDER)
  {
    tsIndex = FindTradeServerIndex(tsId);

    if (tsIndex != -1)
    {
      // Increasing number of order plased of TS
      tradeServersInfo.orderPlaceSummary[tsIndex].numberOfSent ++;
      
      // Update current number of Orders sent to detect FastMarketMode
      FastMarketMgmt.currentOrdersPerSec++;
    }   
  }

  // Increasing number of order plased of AS
  traderToolsInfo.currentStatus.numberOfSent ++;

  return SUCCESS;
}

/****************************************************************************
- Function name:  UpdateTotalSharesFilled
- Input:      
- Output:   
- Return:   
- Description:
- Usage:    
****************************************************************************/
int UpdateTotalSharesFilled(int tsId, int shares)
{
  int tsIndex;

  if (tsId != MANUAL_ORDER)
  {
    tsIndex = FindTradeServerIndex(tsId);

    if (tsIndex != -1)
    {
      // Increasing number of order plased of TS
      tradeServersInfo.orderPlaceSummary[tsIndex].totalSharesFilled += shares;
    }   
  }

  // Increasing number of order plased of AS
  traderToolsInfo.currentStatus.totalSharesFilled += shares;

  return SUCCESS;
}

/****************************************************************************
- Function name:  ResetBrokenTrade
- Input:      
- Output:   
- Return:   
- Description:
- Usage:    
****************************************************************************/
int ResetBrokenTrade(void)
{
  int i;

  for (i = 0; i < brokenTradeList.countBrokenTrade; i++)
  {
    brokenTradeList.collection[i].isUpdate = BROKEN_TRADE_DELETE;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  RemoveBrokenTrade
- Input:      
- Output:   
- Return:   
- Description:
- Usage:    
****************************************************************************/
int RemoveBrokenTrade(void)
{
  int i;

  for (i = 0; i < brokenTradeList.countBrokenTrade; i++)
  {
    if (brokenTradeList.collection[i].isUpdate == BROKEN_TRADE_DELETE)
    {
      if (brokenTradeList.collection[i].oid != -1)
      {     
        TraceLog(DEBUG_LEVEL, "Delete broken Trade: symbol = %.8s, oid = %d, execId = %ld, newShares = %d, newPrice = %lf, brokenShares = %d, account = %d\n", 
          brokenTradeList.collection[i].symbol, brokenTradeList.collection[i].oid, brokenTradeList.collection[i].execId, 
          brokenTradeList.collection[i].newShares, brokenTradeList.collection[i].newPrice, brokenTradeList.collection[i].brokenShares, brokenTradeList.collection[i].account);
      }

      brokenTradeList.collection[i].oid = -1;
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  CheckBrokenTrade
- Input:      
- Output:   
- Return:   
- Description:
- Usage:    
****************************************************************************/
int UpdateBrokenTrade(t_BrokenTradeInfo brokenTrade)
{
  int i, isNew = 1, indexBackup = -1;

  for (i = 0; i < brokenTradeList.countBrokenTrade; i++)
  {
    if ((brokenTradeList.collection[i].oid == brokenTrade.oid) && 
      (brokenTradeList.collection[i].execId == brokenTrade.execId))
    {
      isNew = 0;

      if ((brokenTradeList.collection[i].newShares == brokenTrade.newShares) && 
        feq(brokenTradeList.collection[i].newPrice, brokenTrade.newPrice))
      {
        brokenTradeList.collection[i].isUpdate = BROKEN_TRADE_NONE;

        return SUCCESS;
      }
      else
      {
        brokenTradeList.collection[i].newShares = brokenTrade.newShares;
        brokenTradeList.collection[i].newPrice = brokenTrade.newPrice;

        brokenTradeList.collection[i].isUpdate = BROKEN_TRADE_ADD;
      }

      break;
    }
    else if (brokenTradeList.collection[i].oid == -1)
    {
      indexBackup = i;
    }
  }

  if (isNew == 1)
  {
    TraceLog(DEBUG_LEVEL, "Add broken Trade: symbol = %.8s, oid = %d, execId = %ld, newShares = %d, newPrice = %lf, brokenShares = %d, account = %d\n",
        brokenTrade.symbol, brokenTrade.oid, brokenTrade.execId, brokenTrade.newShares, brokenTrade.newPrice, brokenTrade.brokenShares, brokenTrade.account);

    if (indexBackup != -1)
    {
      memcpy(&brokenTradeList.collection[indexBackup], &brokenTrade, sizeof(t_BrokenTradeInfo));
    }
    else
    {
      memcpy(&brokenTradeList.collection[brokenTradeList.countBrokenTrade], &brokenTrade, sizeof(t_BrokenTradeInfo));

      brokenTradeList.countBrokenTrade += 1;
    }   
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  UpdateOpenOrders
- Input:      
- Output:   
- Return:   
- Description:
- Usage:    
****************************************************************************/
int UpdateOpenOrders(void)
{
  int i, j, indexOpenOrder;

  // Process undo broken trade for open orders
  for (i = 0; i < brokenTradeList.countBrokenTrade; i++)
  {
    if ((brokenTradeList.collection[i].isUpdate == BROKEN_TRADE_DELETE) && 
      (brokenTradeList.collection[i].oid != -1))
    {
      indexOpenOrder = CheckOrderIdIndex(brokenTradeList.collection[i].clOrdId, brokenTradeList.collection[i].tsId);

      if ((indexOpenOrder == -1) || (indexOpenOrder == -2))
      {
        TraceLog(ERROR_LEVEL, "UpdateOpenOrders: tsId = %d, orderId = %d\n", brokenTradeList.collection[i].tsId, brokenTradeList.collection[i].clOrdId);

        return ERROR;
      }

      asOpenOrder.orderCollection[indexOpenOrder].isBroken = BROKEN_TRADE_NONE;
      asOpenOrder.orderCollection[indexOpenOrder].orderSummary.leftShares -= brokenTradeList.collection[i].brokenShares;

      for (j = 0; j < asOpenOrder.orderCollection[indexOpenOrder].countOrderDetail; j++) {
        if (asOrderDetail.orderDetailList[asOpenOrder.orderCollection[indexOpenOrder].indexOrderDetailList[j]].execId == brokenTradeList.collection[i].execId)
        {
          asOrderDetail.orderDetailList[asOpenOrder.orderCollection[indexOpenOrder].indexOrderDetailList[j]].isBroken = BROKEN_TRADE_NONE;

          break;
        }
      }

      // isUpdated 
      memset(asOpenOrder.orderCollection[indexOpenOrder].isUpdated, OPEN_ORDER_UPDATED, MAX_TRADER_TOOL_CONNECTIONS);
    }
  }

  // Process broken trade for open orders
  for (i = 0; i < brokenTradeList.countBrokenTrade; i++)
  {
    if (brokenTradeList.collection[i].isUpdate == BROKEN_TRADE_ADD)
    {
      indexOpenOrder = CheckOrderIdIndex(brokenTradeList.collection[i].clOrdId, brokenTradeList.collection[i].tsId);

      if ((indexOpenOrder == -1) || (indexOpenOrder == -2))
      {
        TraceLog(ERROR_LEVEL, "UpdateOpenOrders: tsId = %d, orderId = %d\n", brokenTradeList.collection[i].tsId, brokenTradeList.collection[i].clOrdId);

        return ERROR;
      }

      // Update ...
      if (brokenTradeList.collection[i].brokenShares > 0)
      {
        asOpenOrder.orderCollection[indexOpenOrder].isBroken = BROKEN_TRADE_REMOVED;
        asOpenOrder.orderCollection[indexOpenOrder].orderSummary.leftShares += brokenTradeList.collection[i].brokenShares;

        for (j = 0; j < asOpenOrder.orderCollection[indexOpenOrder].countOrderDetail; j++) {
          if (asOrderDetail.orderDetailList[asOpenOrder.orderCollection[indexOpenOrder].indexOrderDetailList[j]].execId == brokenTradeList.collection[i].execId)
          {
            asOrderDetail.orderDetailList[asOpenOrder.orderCollection[indexOpenOrder].indexOrderDetailList[j]].isBroken = BROKEN_TRADE_REMOVED;

            break;
          }
        }
      }
      else
      {       
        asOpenOrder.orderCollection[indexOpenOrder].isBroken = BROKEN_TRADE_PRICE_CHANGED;

        for (j = 0; j < asOpenOrder.orderCollection[indexOpenOrder].countOrderDetail; j++) {
          if (asOrderDetail.orderDetailList[asOpenOrder.orderCollection[indexOpenOrder].indexOrderDetailList[j]].execId == brokenTradeList.collection[i].execId)
          {
            asOrderDetail.orderDetailList[asOpenOrder.orderCollection[indexOpenOrder].indexOrderDetailList[j]].isBroken = BROKEN_TRADE_PRICE_CHANGED;
            asOrderDetail.orderDetailList[asOpenOrder.orderCollection[indexOpenOrder].indexOrderDetailList[j]].newPrice = brokenTradeList.collection[i].newPrice;

            break;
          }
        }
      }

      // isUpdated 
      memset(asOpenOrder.orderCollection[indexOpenOrder].isUpdated, OPEN_ORDER_UPDATED, MAX_TRADER_TOOL_CONNECTIONS);
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessChangePrice
- Input:      
- Output:   
- Return:   
- Description:
- Usage:    
****************************************************************************/
int ProcessChangePrice(t_OrderDetail *orderDetail, int ECNId)
{
  t_Parameters paraList;
  int i;

  // Parse ...
  paraList.countParameters = Lrc_Split(orderDetail->msgContent, 1, &paraList);

  switch (ECNId)
  {
    case TYPE_NYSE_CCG:
    case TYPE_ARCA_DIRECT:
      // Update fieldIndexList.PriceField(-43=) and fieldIndexList.PriceScale(-42=)
      for (i = 0; i < paraList.countParameters; i++)
      {
        if ((paraList.parameterList[i][0] == '-') &&
          (paraList.parameterList[i][1] == '4') &&
          (paraList.parameterList[i][2] == '3') &&
          (paraList.parameterList[i][3] == '='))
        {
          //fieldPrice * 1.0 / pow( 10, fieldPriceScale - 48 );
          sprintf(paraList.parameterList[i], "-43=%d", (int)(orderDetail->newPrice * pow(10, paraList.parameterList[i + 1][4] - 48)));

          break;
        }
      }
      break;
    case TYPE_NASDAQ_OUCH:
    case TYPE_NASDAQ_RASH:
    case TYPE_BZX_BOE:
    case TYPE_EDGX_DIRECT:
    case TYPE_EDGA_DIRECT:
    case TYPE_NASDAQ_OUBX:
    case TYPE_BYX_BOE:
    case TYPE_PSX:
      // Update fieldIndexList.Price(-8=)
      for (i = 0; i < paraList.countParameters; i++)
      {
        if ((paraList.parameterList[i][0] == '-') &&
          (paraList.parameterList[i][1] == '8') &&
          (paraList.parameterList[i][2] == '='))
        {
          sprintf(paraList.parameterList[i], "-8=%lf", orderDetail->newPrice);

          break;
        }
      }
      break;
    default:
      break;
  }

  orderDetail->length = 0;
  for (i = 0; i < paraList.countParameters; i++)
  {
    sprintf(&(orderDetail->msgContent[orderDetail->length]), "%s\001", paraList.parameterList[i]);
    orderDetail->length = strlen(orderDetail->msgContent);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  GetDateTimeFromEpochNsec
****************************************************************************/
char* GetDateTimeFromEpochNsec(long nsec, char *dateTime)
{
  // Get number of seconds
  int sPart = nsec/1000000000;
  int uPart = (nsec/1000)%1000000;

  struct tm l_time;
  time_t numberOfSecondsTmp = sPart;
  
  // Convert to local time
  localtime_r(&numberOfSecondsTmp, &l_time);
  
  // Get date string
  strftime(dateTime, MAX_TIMESTAMP - 1, "%Y/%m/%d-%H:%M:%S.", &l_time);
  sprintf(&dateTime[20], "%06d", uPart);
  dateTime[TIMESTAMP_LENGTH] = 0;

  return dateTime;
}

/****************************************************************************
- Function name:  ConvertDateTimeToEpoch
****************************************************************************/
void ConvertDateTimeToEpoch(char *dateTime, char *res)
{
  struct tm localTime;
  
  strptime(dateTime, "%Y/%m/%d-%H:%M:%S.", &localTime);
  localTime.tm_isdst = -1;  //Auto detect daylight saving time
  
  time_t epochTime = mktime(&localTime);
  int numSecond = epochTime;
  
  char numMicroSecond[7];
  memcpy(numMicroSecond, &dateTime[20], 6);
  numMicroSecond[6] = 0;
  
  sprintf(res, "%d.%s", numSecond, numMicroSecond);
  
  return;
}

/****************************************************************************
- Function name:  CheckBookOrderSettingOfBATS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int CheckBookOrderSettingOfBATS(int numSec, int tsMdcIndex)
{
  // Check time to enable/disable the BATSZ book and order ports
  // 6:45AM EST - Enable BATSZ book and order ports
  // 6:59AM EST - Enable trading automatically
  // 4:00PM EST - Disable BATSZ book and order ports
  
  int tsOecIndex;
  int timeToEnableSetting, timeToDisableSetting, timeToEnableTrading;
  int *enableSetting, *disableSetting, *autoEnableTrading;
  if (tsMdcIndex == TS_BATSZ_BOOK_INDEX)
  {
    tsOecIndex = TS_BATSZ_BOE_INDEX;
    timeToEnableSetting  = TimeToEnableBatszSetting;
    timeToDisableSetting = TimeToDisableBatszSetting;
    timeToEnableTrading  = TimeToEnableBatszTrading;
    
    autoEnableTrading = &autoEnableBATSZTrading;
    enableSetting     = &enableBATSZSetting;
    disableSetting    = &disableBATSZSetting;
  }
  else
  {
    tsOecIndex = TS_BYX_BOE_INDEX;
    timeToEnableSetting  = TimeToEnableBatsySetting;
    timeToDisableSetting = TimeToDisableBatsySetting;
    timeToEnableTrading  = TimeToEnableBatsyTrading;
    
    autoEnableTrading = &autoEnableBATSYTrading;
    enableSetting     = &enableBATSYSetting;
    disableSetting    = &disableBATSYSetting;
  }
  
  // Enable trading automatically
  if (numSec >= timeToEnableTrading && numSec < (timeToEnableTrading + 10))
  {
    if (*autoEnableTrading == NO)
    {
      TraceLog(DEBUG_LEVEL, "Enable trading for %s automatically\n", tsMdcIndex == TS_BATSZ_BOOK_INDEX ? "BATSZ":"BYX");
      int tsIndex;
      for (tsIndex = 0; tsIndex < MAX_TRADE_SERVER_CONNECTIONS; tsIndex++)
      {
        if ((tradeServersInfo.tsSetting[tsIndex].orderEnable[tsOecIndex] == YES) &&
          (tradeServersInfo.tsSetting[tsIndex].bookEnable[tsMdcIndex] == YES) &&
          (tradeServersInfo.currentStatus[tsIndex].tsOecStatus[tsOecIndex] == CONNECTED) &&
          (tradeServersInfo.currentStatus[tsIndex].tsMdcStatus[tsMdcIndex] == CONNECTED) &&
          (tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[tsOecIndex] == 0))
        {
          TraceLog(DEBUG_LEVEL, "Send request %s enable trading of %s BOE automatically\n", tradeServersInfo.config[tsIndex].description, tsMdcIndex == TS_BATSZ_BOOK_INDEX ? "BATSZ":"BYX");
          
          if (SendEnableTradingAOrderToTS(tsIndex, tsOecIndex) == ERROR)
          {
            return ERROR;
          }
        }
      }
      
      *autoEnableTrading = YES; //turn on this flag to enable trading only once
    }
  }
  
  if (*enableSetting == NO)
  {
    if ((numSec >= timeToEnableSetting) && (numSec < timeToDisableSetting))
    {
      TraceLog(DEBUG_LEVEL, "Begin enable %s MDC and OEC automatically\n", tsMdcIndex == TS_BATSZ_BOOK_INDEX ? "BATSZ":"BYX");

      // Process enable BATSZ book and order ports on TS
      int tsIndex;
      for (tsIndex = 0; tsIndex < MAX_TRADE_SERVER_CONNECTIONS; tsIndex++)
      {
        tradeServersInfo.tsSetting[tsIndex].bookEnable[tsMdcIndex] = YES;
        tradeServersInfo.tsSetting[tsIndex].orderEnable[tsOecIndex] = YES;
        
        // Save Trade Server setting
        SaveTradeServerSetting(tsIndex);

        // Send Trade Server setting to all Trader Tool connected
        SendTSSettingToAllTT(tsIndex); // ECN Setting is disabled for now
      }
      
      // BATSZ hasn't been integrated to PM
      
      // Process enable BATSZ book and order ports on PM      
      // Save PM setting
      // SavePMSetting();

      // Send PM setting to all Trader Tool connected
      // SendPMSettingToAllTT();
      
      *enableSetting = YES; //turn on this flag to send BATSZ ECN Setting only once to TT
      *disableSetting = NO;
    }
  }
  
  if (*disableSetting == NO)
  {
    if (numSec >= timeToDisableSetting)
    {
      TraceLog(DEBUG_LEVEL, "Begin disable %s MDC and OEC automatically\n", tsMdcIndex == TS_BATSZ_BOOK_INDEX ? "BATSZ":"BYX");

      // Process disable BATSZ book and order ports on TS
      int tsIndex;
      for (tsIndex = 0; tsIndex < MAX_TRADE_SERVER_CONNECTIONS; tsIndex++)
      {
        // Call function to disconnect to ECN Book from TS
        tradeServersInfo.tsSetting[tsIndex].bookEnable[tsMdcIndex] = NO;
        TraceLog(DEBUG_LEVEL, "Send request %s disconnect to %s MDC automatically\n", tradeServersInfo.config[tsIndex].description, tsMdcIndex == TS_BATSZ_BOOK_INDEX ? "BATSZ":"BYX");
        SendDisconnectBookToTS(tsIndex, tsMdcIndex);
        
        // Call function to disconnect to ECN Order from TS
        tradeServersInfo.tsSetting[tsIndex].orderEnable[tsOecIndex] = NO;
        TraceLog(DEBUG_LEVEL, "Send request %s disconnect to %s BOE automatically\n", tradeServersInfo.config[tsIndex].description, tsMdcIndex == TS_BATSZ_BOOK_INDEX ? "BATSZ":"BYX");
        SendDisconnectOrderToTS(tsIndex, tsOecIndex);

        // Save Trade Server setting
        SaveTradeServerSetting(tsIndex);

        // Send Trade Server setting to all Trader Tool connected
        SendTSSettingToAllTT(tsIndex); // ECN Setting is disabled for now
      }
      
      // BATSZ hasn't been integrated to PM
      
      // Save PM setting
      // SavePMSetting();

      // Send PM setting to all Trader Tool connected
      // SendPMSettingToAllTT();
      
      *disableSetting = YES;
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  CheckBookOrderSettingOfEDGE
****************************************************************************/
int CheckBookOrderSettingOfEDGE(int numSec, int tsMdcIndex)
{
  // Check time to enable/disable the EDGX book and order ports
  // 6:45AM EST - Enable EDGX book and order ports
  // 6:59AM EST - Enable trading automatically
  // 4:00PM EST - Disable EDGX book and order ports
  
  int tsOecIndex;
  int timeToEnableSetting, timeToDisableSetting, timeToEnableTrading;
  int *enableSetting, *disableSetting, *autoEnableTrading;
  if (tsMdcIndex == TS_EDGX_BOOK_INDEX)
  {
    tsOecIndex = TS_EDGX_DIRECT_INDEX;
    timeToEnableSetting  = TimeToEnableEdgxSetting;
    timeToDisableSetting = TimeToDisableEdgxSetting;
    timeToEnableTrading  = TimeToEnableEdgxTrading;
    
    autoEnableTrading = &autoEnableEDGXTrading;
    enableSetting     = &enableEDGXSetting;
    disableSetting    = &disableEDGXSetting;
  }
  else
  {
    tsOecIndex = TS_EDGA_DIRECT_INDEX;
    timeToEnableSetting  = TimeToEnableEdgaSetting;
    timeToDisableSetting = TimeToDisableEdgaSetting;
    timeToEnableTrading  = TimeToEnableEdgaTrading;
    
    autoEnableTrading = &autoEnableEDGATrading;
    enableSetting     = &enableEDGASetting;
    disableSetting    = &disableEDGASetting;
  }
  
  // Enable trading automatically
  if (numSec >= timeToEnableTrading && numSec < (timeToEnableTrading + 10))
  {
    if (*autoEnableTrading == NO)
    {
      TraceLog(DEBUG_LEVEL, "Enable trading for %s automatically\n", tsMdcIndex == TS_EDGX_BOOK_INDEX ? "EDGX":"EDGA");
      int tsIndex;
      for (tsIndex = 0; tsIndex < MAX_TRADE_SERVER_CONNECTIONS; tsIndex++)
      {
        if ((tradeServersInfo.tsSetting[tsIndex].orderEnable[tsOecIndex] == YES) &&
          (tradeServersInfo.tsSetting[tsIndex].bookEnable[tsMdcIndex] == YES) &&
          (tradeServersInfo.currentStatus[tsIndex].tsOecStatus[tsOecIndex] == CONNECTED) &&
          (tradeServersInfo.currentStatus[tsIndex].tsMdcStatus[tsMdcIndex] == CONNECTED) &&
          (tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[tsOecIndex] == 0))
        {
          TraceLog(DEBUG_LEVEL, "Send request %s enable trading of %s DIRECT automatically\n", tradeServersInfo.config[tsIndex].description,  tsMdcIndex == TS_EDGX_BOOK_INDEX ? "EDGX":"EDGA");
          
          if (SendEnableTradingAOrderToTS(tsIndex, tsOecIndex) == ERROR)
          {
            return ERROR;
          }
        }
      }
      
      *autoEnableTrading = YES; //turn on this flag to enable trading only once
    }
  }
  
  if (*enableSetting == NO)
  {
    if ((numSec >= timeToEnableSetting) && (numSec < timeToDisableSetting))
    {
      TraceLog(DEBUG_LEVEL, "Begin enable %s MDC and OEC automatically\n", tsMdcIndex == TS_EDGX_BOOK_INDEX ? "EDGX":"EDGA");

      // Process enable EDGE book and order ports on TS
      int tsIndex;
      for (tsIndex = 0; tsIndex < MAX_TRADE_SERVER_CONNECTIONS; tsIndex++)
      {
        tradeServersInfo.tsSetting[tsIndex].bookEnable[tsMdcIndex] = YES;
        tradeServersInfo.tsSetting[tsIndex].orderEnable[tsOecIndex] = YES;
        
        // Save Trade Server setting
        SaveTradeServerSetting(tsIndex);

        // Send Trade Server setting to all Trader Tool connected
        SendTSSettingToAllTT(tsIndex); // ECN Setting is disabled for now
      }
      
      // Process enable EDGE book and order ports on PM     
      // Save PM setting
      // SavePMSetting();

      // Send PM setting to all Trader Tool connected
      // SendPMSettingToAllTT();
      
      *enableSetting = YES;  //turn on this flag to send EDGE ECN Setting only once to TT
      *disableSetting = NO;
    }
  }
  
  if (*disableSetting == NO)
  {
    if (numSec >= timeToDisableSetting)
    {
      TraceLog(DEBUG_LEVEL, "Begin disable %s MDC and OEC automatically\n", tsMdcIndex == TS_EDGX_BOOK_INDEX ? "EDGX":"EDGA");

      // Process disable EDGE book and order ports on TS
      int tsIndex;
      for (tsIndex = 0; tsIndex < MAX_TRADE_SERVER_CONNECTIONS; tsIndex++)
      {
        // Call function to disconnect to ECN Book from TS
        tradeServersInfo.tsSetting[tsIndex].bookEnable[tsMdcIndex] = NO;
        TraceLog(DEBUG_LEVEL, "Send request %s disconnect to %s MDC automatically\n", tradeServersInfo.config[tsIndex].description, tsMdcIndex == TS_EDGX_BOOK_INDEX ? "EDGX":"EDGA");
        SendDisconnectBookToTS(tsIndex, tsMdcIndex);
        
        // Call function to disconnect to ECN Order from TS
        tradeServersInfo.tsSetting[tsIndex].orderEnable[tsOecIndex] = NO;
        TraceLog(DEBUG_LEVEL, "Send request %s disconnect to %s DIRECT automatically\n", tradeServersInfo.config[tsIndex].description, tsMdcIndex == TS_EDGX_BOOK_INDEX ? "EDGX":"EDGA");
        SendDisconnectOrderToTS(tsIndex, tsOecIndex);

        // Save Trade Server setting
        SaveTradeServerSetting(tsIndex);

        // Send Trade Server setting to all Trader Tool connected
        SendTSSettingToAllTT(tsIndex); // ECN Setting is disabled for now
      }
      
      // Save PM setting
      // SavePMSetting();

      // Send PM setting to all Trader Tool connected
      // SendPMSettingToAllTT();
      
      *disableSetting = YES;
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  CheckBookOrderSettingOfNYSE
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int CheckBookOrderSettingOfNYSE(int numSec, int tsMdcIndex)
{
  int timeToDisableSetting;
  int *enableSetting, *disableSetting;
  if (tsMdcIndex == TS_NYSE_BOOK_INDEX)
  {
    enableSetting = &enableNYSESetting;
    disableSetting = &disableNYSESetting;
    timeToDisableSetting = TimeToDisableNyseSetting;
  }
  else
  {
    enableSetting = &enableAMEXSetting;
    disableSetting = &disableAMEXSetting;
    timeToDisableSetting = TimeToDisableAmexSetting;
  }
  
  if (*enableSetting == NO)
  {
    if (numSec < timeToDisableSetting)
    {
      TraceLog(DEBUG_LEVEL, "Begin enable %s MDC and OEC automatically\n", tsMdcIndex == TS_NYSE_BOOK_INDEX ? "NYSE":"AMEX");

      int tsIndex;
      for (tsIndex = 0; tsIndex < MAX_TRADE_SERVER_CONNECTIONS; tsIndex++)
      {
        tradeServersInfo.tsSetting[tsIndex].bookEnable[tsMdcIndex] = YES;
        tradeServersInfo.tsSetting[tsIndex].orderEnable[TS_NYSE_CCG_INDEX] = YES;
        
        // Save Trade Server setting
        SaveTradeServerSetting(tsIndex);

        // Send Trade Server setting to all Trader Tool connected
        SendTSSettingToAllTT(tsIndex); // ECN Setting is disabled for now
      }
      
      *enableSetting = YES;
      *disableSetting = NO;
    }
  }
  
  if (*disableSetting == NO)
  {
    if (numSec >= timeToDisableSetting)
    {
      TraceLog(DEBUG_LEVEL, "Begin disable %s MDC and OEC automatically\n", tsMdcIndex == TS_NYSE_BOOK_INDEX ? "NYSE":"AMEX");

      // Process disable NYSE book and order ports on TS
      int tsIndex;
      for (tsIndex = 0; tsIndex < MAX_TRADE_SERVER_CONNECTIONS; tsIndex++)
      {
        // Call function to disconnect to ECN Book from TS
        tradeServersInfo.tsSetting[tsIndex].bookEnable[tsMdcIndex] = NO;
        
        TraceLog(DEBUG_LEVEL, "Send request %s disconnect %s MDC automatically\n", tradeServersInfo.config[tsIndex].description, tsMdcIndex == TS_NYSE_BOOK_INDEX ? "NYSE":"AMEX");
        SendDisconnectBookToTS(tsIndex, tsMdcIndex);
        
        // Call function to disconnect to ECN Order from TS
        tradeServersInfo.tsSetting[tsIndex].orderEnable[TS_NYSE_CCG_INDEX] = NO;
        
        TraceLog(DEBUG_LEVEL, "Send request %s disconnect NYSE CCG automatically\n", tradeServersInfo.config[tsIndex].description);
        SendDisconnectOrderToTS(tsIndex, TS_NYSE_CCG_INDEX);

        // Save Trade Server setting
        SaveTradeServerSetting(tsIndex);

        // Send Trade Server setting to all Trader Tool connected
        SendTSSettingToAllTT(tsIndex); // ECN Setting is disabled for now
      }
      
      *disableSetting = YES;
    }
  }
  return SUCCESS;
}

/****************************************************************************
- Function name:  CheckBookOrderSettingOfBX
****************************************************************************/
int CheckBookOrderSettingOfBX(int numSec, int tsMdcIndex)
{
  int *enableSetting;
  int timeToEnableSetting;
  int tsOecIndex;
  if (tsMdcIndex == TS_NDBX_BOOK_INDEX)
  {
    tsOecIndex = TS_NDAQ_OUBX_INDEX;
    enableSetting = &enableBXSetting;
    timeToEnableSetting = TimeToEnableBxSetting;
  }
  else
  {
    tsOecIndex = TS_PSX_OUCH_INDEX;
    enableSetting = &enablePSXSetting;
    timeToEnableSetting = TimeToEnablePsxSetting;
  }
  
  if (*enableSetting == NO)
  {
    if (numSec >= timeToEnableSetting)
    {
      TraceLog(DEBUG_LEVEL, "Begin enable %s MDC and OEC automatically\n", tsMdcIndex == TS_NDBX_BOOK_INDEX ? "BX":"PSX");

      // Process enable BX book and order ports on TS
      int tsIndex;
      for (tsIndex = 0; tsIndex < MAX_TRADE_SERVER_CONNECTIONS; tsIndex++)
      {
        tradeServersInfo.tsSetting[tsIndex].bookEnable[tsMdcIndex] = YES;
        tradeServersInfo.tsSetting[tsIndex].orderEnable[tsOecIndex] = YES;
        
        // Save Trade Server setting
        SaveTradeServerSetting(tsIndex);

        // Send Trade Server setting to all Trader Tool connected
        SendTSSettingToAllTT(tsIndex); // ECN Setting is disabled for now
      }

      *enableSetting = YES;  //turn on this flag to send BX ECN Setting only once to TT
    }
  }
  return SUCCESS;
}

/****************************************************************************
- Function name:  CheckToDisableTrading
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int CheckToDisableTrading(int numSec)
{
  int ecnOrderID;
  for (ecnOrderID = 0; ecnOrderID < TS_MAX_ORDER_CONNECTIONS; ecnOrderID++)
  {
    if (AutoDisableTrading[ecnOrderID] == 1)
    {
      if (numSec >= TimeToDisableTrading[ecnOrderID])
      {
        TraceLog(DEBUG_LEVEL, "Disable trading for order id %d automatically\n", ecnOrderID);

        int tsIndex;
        for (tsIndex = 0; tsIndex < MAX_TRADE_SERVER_CONNECTIONS; tsIndex++)
        {
          TraceLog(DEBUG_LEVEL, "Request %s disable trading for order id %d\n", tradeServersInfo.config[tsIndex].description, ecnOrderID);
          SendDisableTradingAOrderToTS(tsIndex, ecnOrderID, TS_TRADING_DISABLED_BY_SCHEDULE);
        }
        
        AutoDisableTrading[ecnOrderID] = 0;
      }
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  UpdateMarketMode
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
void UpdateMarketMode(void)
{
  // Note: This function is called once per 0.5 sec
  // So this will affect FastMarketMgmt.isCheck value
  
  FastMarketMgmt.isCheck++; //Each time this statement is called, 0.5 sec has just passed.
  
  if (FastMarketMgmt.isCheck == 2)  // Detect to see if 1 second has passed
  {
    if (FastMarketMgmt.currentOrdersPerSec > FastMarketMgmt.thresHold)
    {
      // We will switch to Fast Market Mode
      if (FastMarketMgmt.mode == NORMAL_MARKET_MODE)
      {
        FastMarketMgmt.mode = FAST_MARKET_MODE;
        SendMarketModeUpdateToAllTT((char)FAST_MARKET_MODE); //this message is not implemented in TT
      }
    }
    else
    {
      // We will switch back to Normal Mode
      if (FastMarketMgmt.mode == FAST_MARKET_MODE)
      {
        FastMarketMgmt.mode = NORMAL_MARKET_MODE;
        SendMarketModeUpdateToAllTT((char)NORMAL_MARKET_MODE); //this message is not implemented in TT
      }
    }
    
    // Reset parameters for the next 1 second checking!
    FastMarketMgmt.isCheck = 0;
    FastMarketMgmt.currentOrdersPerSec = 0;
    
  }
  
}

/****************************************************************************
- Function name:  UpdateVerifyPositionInfo
- Input:      
- Output:     
- Return:     
- Description:    - Called when AS starts
          - Called when TWT modifies LONG/SHORT
- Usage:      
****************************************************************************/
void UpdateVerifyPositionInfo(void)
{
  int symbolIndex;
  int countSymbol = RMList.countSymbol;
  
  int account;
  for (account = 0; account < MAX_ACCOUNT; account++)
  {
    for (symbolIndex = 0; symbolIndex < countSymbol; symbolIndex++)
    {
      // Symbol
      char *symbol = RMList.symbolList[symbolIndex].symbol;
      
      // Share
      if (RMList.collection[account][symbolIndex].leftShares > 0)
      {
        verifyPositionInfo[account][symbolIndex].shares = RMList.collection[account][symbolIndex].leftShares;
        verifyPositionInfo[account][symbolIndex].side = BID_SIDE;
        verifyPositionInfo[account][symbolIndex].isTrading = TRADING_NONE;
        verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
        
        if (verifyPositionInfo[account][symbolIndex].lockCounter != SHORT_LOCK)
        {
          verifyPositionInfo[account][symbolIndex].lockCounter = SHORT_LOCK;  //SHORT_LOCK with counter = 1
          SendSymbolLockCounterForASymbolToTradeServers(symbol, account, SHORT_LOCK);
        }
      }
      else if (RMList.collection[account][symbolIndex].leftShares < 0)
      {
        verifyPositionInfo[account][symbolIndex].shares = -RMList.collection[account][symbolIndex].leftShares;
        verifyPositionInfo[account][symbolIndex].side = ASK_SIDE;
        verifyPositionInfo[account][symbolIndex].isTrading = TRADING_NONE;
        verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
        
        if (verifyPositionInfo[account][symbolIndex].lockCounter != LONG_LOCK)
        {
          verifyPositionInfo[account][symbolIndex].lockCounter = LONG_LOCK; //LONG_LOCK with counter = 1
          SendSymbolLockCounterForASymbolToTradeServers(symbol, account, LONG_LOCK);
        }
      }
      else  //left shares = 0 so we will reset verify list
      {
        verifyPositionInfo[account][symbolIndex].shares = 0;
        verifyPositionInfo[account][symbolIndex].side = -1;
        verifyPositionInfo[account][symbolIndex].isTrading = TRADING_NONE;
        verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
        if (verifyPositionInfo[account][symbolIndex].lockCounter != NO_LOCK)
        {
          verifyPositionInfo[account][symbolIndex].lockCounter = NO_LOCK;
          SendSymbolLockCounterForASymbolToTradeServers(symbol, account, NO_LOCK);
        }
      }
      
      //TraceLog(DEBUG_LEVEL, "(%d) -- (%d) -- (%d)\n", verifyPositionInfo[symbolIndex].shares, verifyPositionInfo[symbolIndex].side, verifyPositionInfo[symbolIndex].isTrading);
    }
  }
  
  // Add total volume record in OJ
  hasChangedExposure = 1;
}

/****************************************************************************
- Function name:  UpdateVerifyPositionInfoAfterTradeBreak
- Input:      
- Output:      
- Return:      
- Description:    
- Usage:      
****************************************************************************/
int UpdateVerifyPositionInfoAfterTradeBreak(int account, int symbolIndex)
{
  // Symbol
  char *symbol = RMList.symbolList[symbolIndex].symbol;
  
  // Share
  if (RMList.collection[account][symbolIndex].leftShares > 0)
  {
    verifyPositionInfo[account][symbolIndex].shares = RMList.collection[account][symbolIndex].leftShares;
    verifyPositionInfo[account][symbolIndex].side = BID_SIDE;
    verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
    
    if (verifyPositionInfo[account][symbolIndex].lockCounter != SHORT_LOCK)
    {
      verifyPositionInfo[account][symbolIndex].lockCounter = SHORT_LOCK;  //SHORT_LOCK with counter = 1
      SendSymbolLockCounterForASymbolToTradeServers(symbol, account, SHORT_LOCK);
    }
  }
  else if (RMList.collection[account][symbolIndex].leftShares < 0)
  {
    verifyPositionInfo[account][symbolIndex].shares = -RMList.collection[account][symbolIndex].leftShares;
    verifyPositionInfo[account][symbolIndex].side = ASK_SIDE;
    verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
    
    if (verifyPositionInfo[account][symbolIndex].lockCounter != LONG_LOCK)
    {
      verifyPositionInfo[account][symbolIndex].lockCounter = LONG_LOCK;  //LONG_LOCK with counter = 1
      SendSymbolLockCounterForASymbolToTradeServers(symbol, account, LONG_LOCK);
    }
  }
  else  //left shares = 0 so we will reset verify list
  {
    verifyPositionInfo[account][symbolIndex].shares = 0;
    verifyPositionInfo[account][symbolIndex].side = -1;
    verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
    if (verifyPositionInfo[account][symbolIndex].lockCounter != NO_LOCK)
    {
      verifyPositionInfo[account][symbolIndex].lockCounter = NO_LOCK;
      SendSymbolLockCounterForASymbolToTradeServers(symbol, account, NO_LOCK);
    }
  }
  
  //TraceLog(DEBUG_LEVEL, "(%d) -- (%d) -- (%d)\n", verifyPositionInfo[symbolIndex].shares, verifyPositionInfo[symbolIndex].side, verifyPositionInfo[symbolIndex].isTrading);
  
  // Add total volume record in OJ
  hasChangedExposure = 1;
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  UpdateVerifyPositionInfoForStuckFromTSOutputLog
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int UpdateVerifyPositionInfoForStuckFromTSOutputLog(char symbol[SYMBOL_LEN], int stuckShares, int _stuckSide, int account, int lockType)
{
  //Get verify position info
  char stuckSide = _stuckSide;
  int symbolIndex = GetStockSymbolIndex(symbol);
  if (symbolIndex < 0) return SUCCESS;
  
  if (stuckSide == verifyPositionInfo[account][symbolIndex].side)
  {
    verifyPositionInfo[account][symbolIndex].shares += stuckShares;
    verifyPositionInfo[account][symbolIndex].lockCounter += lockType; //Increase lock counter
  }
  else
  {
    if (stuckShares > verifyPositionInfo[account][symbolIndex].shares)
    {
      int oldShares = verifyPositionInfo[account][symbolIndex].shares;
      verifyPositionInfo[account][symbolIndex].shares = stuckShares - oldShares;
      verifyPositionInfo[account][symbolIndex].side = stuckSide;
      
      if (verifyPositionInfo[account][symbolIndex].lockCounter != NO_LOCK)
      {
        //The side of final position has been changed now
        verifyPositionInfo[account][symbolIndex].lockCounter = lockType; //assign new lock counter
        SendSymbolLockCounterForASymbolToTradeServers(symbol, account, lockType);
      }
      else
      {
        verifyPositionInfo[account][symbolIndex].lockCounter = lockType; //assign new lock counter
      }
    }
    else if (stuckShares < verifyPositionInfo[account][symbolIndex].shares)
    {
      verifyPositionInfo[account][symbolIndex].shares -= stuckShares;
      if (lockType == SHORT_LOCK)
      {
        verifyPositionInfo[account][symbolIndex].lockCounter = LONG_LOCK; //assign new lock counter
        SendSymbolLockCounterForASymbolToTradeServers(symbol, account, LONG_LOCK);
      }
      else
      {
        verifyPositionInfo[account][symbolIndex].lockCounter = SHORT_LOCK; //assign new lock counter
        SendSymbolLockCounterForASymbolToTradeServers(symbol, account, SHORT_LOCK);
      }
    }
    else
    {
      verifyPositionInfo[account][symbolIndex].shares = 0;
      verifyPositionInfo[account][symbolIndex].side = -1;
      
      verifyPositionInfo[account][symbolIndex].lockCounter = NO_LOCK; //unlock
      SendSymbolLockCounterForASymbolToTradeServers(symbol, account, NO_LOCK);
    }
  }
  
  // Add total volume record in OJ
  hasChangedExposure = 1;
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  UpdateVerifyPositionInfoFromFillOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int UpdateVerifyPositionInfoFromFillOrder(char symbol[SYMBOL_LEN], int filledShares, char side, int account, int source)
{
  //Get stock symbol index
  int symbolIndex = GetStockSymbolIndex(symbol);
  if (symbolIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "UpdateVerifyPositionInfoFromFillOrder: Invalid symbol (%.8s)\n", symbol);
    return SUCCESS;
  }
  
  if (side == verifyPositionInfo[account][symbolIndex].side)
  {
    if (filledShares == verifyPositionInfo[account][symbolIndex].shares)
    {
      int lockCounter = verifyPositionInfo[account][symbolIndex].lockCounter;
      verifyPositionInfo[account][symbolIndex].lockCounter = 0;
      verifyPositionInfo[account][symbolIndex].shares = 0;
      verifyPositionInfo[account][symbolIndex].side = -1;
      verifyPositionInfo[account][symbolIndex].isTrading = TRADING_NONE;
      verifyPositionInfo[account][symbolIndex].isPMOwner = NO;
      verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
      SendStuckUnlockForASymbolToTradeServers(symbol, account, lockCounter);
      
      verifyPositionInfo[account][symbolIndex].alreadyAlerted = YES;
    }
    else if (filledShares < verifyPositionInfo[account][symbolIndex].shares)
    {
      verifyPositionInfo[account][symbolIndex].shares -= filledShares;
      verifyPositionInfo[account][symbolIndex].isTrading = source;
      verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
    }
    else
    {
      //The side of open position has changed
      int oldShares = verifyPositionInfo[account][symbolIndex].shares;
      verifyPositionInfo[account][symbolIndex].shares = filledShares - oldShares;
      verifyPositionInfo[account][symbolIndex].side = 1 - side;
      verifyPositionInfo[account][symbolIndex].isTrading = source;
      verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
      
      if (side == BID_SIDE) //Just sold somce shares
      {
        verifyPositionInfo[account][symbolIndex].lockCounter = LONG_LOCK; //assign new lock counter
        SendSymbolLockCounterForASymbolToTradeServers(symbol, account, LONG_LOCK);
      }
      else
      {
        verifyPositionInfo[account][symbolIndex].lockCounter = SHORT_LOCK; //assign new lock counter
        SendSymbolLockCounterForASymbolToTradeServers(symbol, account, SHORT_LOCK);
      }
    }
  }
  else
  { 
    if (verifyPositionInfo[account][symbolIndex].side == -1)
    {
      verifyPositionInfo[account][symbolIndex].side = 1 - side;
    }
    
    if (side == BID_SIDE)
    {
      verifyPositionInfo[account][symbolIndex].lockCounter = LONG_LOCK; //assign new lock counter
      SendSymbolLockCounterForASymbolToTradeServers(symbol, account, LONG_LOCK);
    }
    else
    {
      verifyPositionInfo[account][symbolIndex].lockCounter = SHORT_LOCK; //assign new lock counter
      SendSymbolLockCounterForASymbolToTradeServers(symbol, account, SHORT_LOCK);
    }
      
    //The side of stuck is changed, so we should add filledShares volum from PM to new stuck shares
    verifyPositionInfo[account][symbolIndex].shares += filledShares;
    verifyPositionInfo[account][symbolIndex].isTrading = source;
    verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
  }
  
  // Add total volume record in OJ
  hasChangedExposure = 1;
  
  return SUCCESS;
}


/****************************************************************************
- Function name:  UpdateVerifyPositionInfoFromRASHAcceptedManualOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int UpdateVerifyPositionInfoFromRASHAcceptedManualOrder(t_DataBlock *dataBlock, int sentBy)
{
  char fieldShares[7];
  char action;
  int orderId;
  int account;
  int symbolIndex;
  /*
    msgContent[10] -->orderId, length =14
    msgContent[24] -->side, length = 1
    msgContent[25] --> shares, length = 6
    msgContent[31] --> symbol, length = 8
  */
  
  // Order ID (strip first two number of account)
  char fieldToken[16];
  memcpy(fieldToken, &dataBlock->msgContent[12], 12);
  fieldToken[12] = 0;
  orderId = atoi( fieldToken ); 
  
  if (orderId >= MAX_MANUAL_ORDER || orderId < 0)
  {
    TraceLog(ERROR_LEVEL, "UpdateVerifyPositionInfoFromRASHAcceptedManualOrder(Accpted): Invalid orderId(%d)\n", orderId);
    return SUCCESS;
  }

  // Symbol
  strncpy(manualOrderInfo[orderId].symbol, (char *) &dataBlock->msgContent[31], SYMBOL_LEN);
  TrimRight( manualOrderInfo[orderId].symbol, strnlen(manualOrderInfo[orderId].symbol, SYMBOL_LEN) ); //Convert all ' ' to null chars
  
  symbolIndex = GetStockSymbolIndex(manualOrderInfo[orderId].symbol);
  if (symbolIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "UpdateVerifyPositionInfoFromRASHAcceptedManualOrder(Accepted): Invalid symbol (%.8s)\n", manualOrderInfo[orderId].symbol);
    return SUCCESS;
  }
  
  if (sentBy == AS_MANUAL_ORDER)
  {
    account = Manual_Order_Account_Mapping[orderId];
    dataBlock->addContent[DATABLOCK_OFFSET_ACCOUNT_ID] = account;
  }
  else if (sentBy == PM_MANUAL_ORDER)
  {
    account = dataBlock->addContent[DATABLOCK_OFFSET_ACCOUNT_ID];
  }
  else
  {
    TraceLog(ERROR_LEVEL, "UpdateVerifyPositionInfoFromRASHAcceptedManualOrder(Accpted): could not detect account\n");
    return SUCCESS;
  }
  
  action = dataBlock->msgContent[24];
  if(action == 'B')
  {
    //We sent new order to BUY stock that mean we have a SHORT on TT (side = 0)
    manualOrderInfo[orderId].side = 0;
  }
  else if(action == 'S' || action == 'T')
  {
    manualOrderInfo[orderId].side = 1;
  }

  // Shares
  memcpy(fieldShares, &dataBlock->msgContent[25], 6);
  fieldShares[6] = 0;
  manualOrderInfo[orderId].leftShares = atoi(fieldShares);
  
  if (sentBy == AS_MANUAL_ORDER)
  {
    AutoCancelOrder.orderInfo[orderId].marked = 1;
    memset(AutoCancelOrder.orderInfo[orderId].ecnOrderID, 0, 20);
    AutoCancelOrder.orderInfo[orderId].leftShares = manualOrderInfo[orderId].leftShares;
  }
  
  verifyPositionInfo[account][symbolIndex].isTrading = TRADING_MANUAL;
  verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  UpdateVerifyPositionInfoFromRASHAcceptedFromPM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int UpdateVerifyPositionInfoFromRASHAcceptedFromPM(t_DataBlock *dataBlock)
{
  int orderId;
  int symbolIndex;
  char fieldShares[7];
  char action;
  int account = dataBlock->addContent[DATABLOCK_OFFSET_ACCOUNT_ID];
  
  /*
    msgContent[10] -->orderId, length =14
    msgContent[24] -->side, length = 1
    msgContent[25] --> shares, length = 6
    msgContent[31] --> symbol, length = 8
  */
  
  // Order ID (strip first two number of account)
  char fieldToken[16];
  memcpy(fieldToken, &dataBlock->msgContent[12], 12);
  fieldToken[12] = 0;
  orderId = atoi( fieldToken ); 
  
  if ((orderId % MAX_ORDER_PLACEMENT) == 0 || orderId < 0)
  {
    TraceLog(ERROR_LEVEL, "%s: Invalid orderId(%d)\n", __func__, orderId);
    return SUCCESS;
  }

  // Symbol
  strncpy(pmOrderInfo[orderId % MAX_ORDER_PLACEMENT].symbol, (char *) &dataBlock->msgContent[31], SYMBOL_LEN);
  TrimRight( pmOrderInfo[orderId % MAX_ORDER_PLACEMENT].symbol, strnlen(pmOrderInfo[orderId % MAX_ORDER_PLACEMENT].symbol, SYMBOL_LEN) ); //Convert all ' ' to null chars

  symbolIndex = GetStockSymbolIndex(pmOrderInfo[orderId % MAX_ORDER_PLACEMENT].symbol);
  if (symbolIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "%s: Invalid symbol (%.8s)\n", __func__, pmOrderInfo[orderId % MAX_ORDER_PLACEMENT].symbol);
    return SUCCESS;
  }
  
  action = dataBlock->msgContent[24];
  if(action == 'B')
  {
    //We sent new order to BUY stock that mean we have a SHORT on TT (side = 0)
    pmOrderInfo[orderId % MAX_ORDER_PLACEMENT].side = 0;
  }
  else if(action == 'S' || action == 'T')
  {
    pmOrderInfo[orderId % MAX_ORDER_PLACEMENT].side = 1;
  }

  // Shares
  memcpy(fieldShares, &dataBlock->msgContent[25], 6);
  fieldShares[6] = 0;
  pmOrderInfo[orderId % MAX_ORDER_PLACEMENT].leftShares = atoi(fieldShares);
  
  verifyPositionInfo[account][symbolIndex].isSleeping = NO;
  verifyPositionInfo[account][symbolIndex].isTrading = TRADING_AUTOMATIC;
  verifyPositionInfo[account][symbolIndex].isPMOwner = YES;
  verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  UpdateVerifyPositionInfoFromRASHExecutedManualOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int UpdateVerifyPositionInfoFromRASHExecutedManualOrder(t_DataBlock *dataBlock, int sentBy)
{
  int orderId;
  char fieldShares[7];
  int symbolIndex;
  /*
    msgContent[10] --> orderId, length = 14
    msgContent[24] --> shares, length = 6
  */
  // OrderId (strip first two digits of account)
  char fieldToken[16];
  memcpy( fieldToken, &dataBlock->msgContent[12], 12);
  fieldToken[12] = 0;
  orderId = atoi( fieldToken );
  
  if (orderId >= MAX_MANUAL_ORDER || orderId < 0)
  {
    TraceLog(ERROR_LEVEL, "UpdateVerifyPositionInfoFromRASHExecutedManualOrder(Executed): Invalid orderId(%d)\n", orderId);
    return SUCCESS;
  }
  
  //account
  int account;
  if (sentBy == AS_MANUAL_ORDER)
  {
    account = Manual_Order_Account_Mapping[orderId];
    dataBlock->addContent[DATABLOCK_OFFSET_ACCOUNT_ID] = account;
  }
  else if (sentBy == PM_MANUAL_ORDER)
  {
    account = dataBlock->addContent[DATABLOCK_OFFSET_ACCOUNT_ID];
  }
  else
  {
    TraceLog(ERROR_LEVEL, "UpdateVerifyPositionInfoFromRASHExecutedManualOrder(Executed): could not detect account\n");
    return SUCCESS;
  }
  
  // Filled Shares
  memcpy( fieldShares, &dataBlock->msgContent[24], 6 );
  fieldShares[6] = 0;
  manualOrderInfo[orderId].filledShares = atoi( fieldShares );
  
  UpdateVerifyPositionInfoFromFillOrder(manualOrderInfo[orderId].symbol, manualOrderInfo[orderId].filledShares, manualOrderInfo[orderId].side, account, TRADING_MANUAL);
  
  //Update leftShares
  manualOrderInfo[orderId].leftShares -= manualOrderInfo[orderId].filledShares;
  
  if (sentBy == AS_MANUAL_ORDER)
  {
    AutoCancelOrder.orderInfo[orderId].leftShares = manualOrderInfo[orderId].leftShares;
  }

  if (manualOrderInfo[orderId].leftShares == 0)
  {
    symbolIndex = GetStockSymbolIndex(manualOrderInfo[orderId].symbol);
    if (symbolIndex == -1)
    {
      TraceLog(ERROR_LEVEL, "UpdateVerifyPositionInfoFromRASHExecutedManualOrder(Executed): Invalid symbol(%.8s)\n", manualOrderInfo[orderId].symbol);
      return SUCCESS;
    }
    
    verifyPositionInfo[account][symbolIndex].isTrading = TRADING_NONE;
    verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  UpdateVerifyPositionInfoFromRASHExecutedFromPM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int UpdateVerifyPositionInfoFromRASHExecutedFromPM(t_DataBlock *dataBlock)
{
  int orderId;
  int symbolIndex;
  char fieldShares[7];
  int account = dataBlock->addContent[DATABLOCK_OFFSET_ACCOUNT_ID];
  
  /*
    msgContent[10] --> orderId, length = 14
    msgContent[24] --> shares, length = 6
  */
  // OrderId (strip first two digits of account)
  char fieldToken[16];
  memcpy( fieldToken, &dataBlock->msgContent[12], 12);
  fieldToken[12] = 0;
  orderId = atoi( fieldToken );
  
  if ((orderId % MAX_ORDER_PLACEMENT) == 0 || orderId < 0)
  {
    TraceLog(ERROR_LEVEL, "UpdateVerifyPositionInfoFromRASHExecutedFromPM(Executed): Invalid orderId(%d)\n", orderId);
    return SUCCESS;
  }
  
  // Filled Shares
  memcpy( fieldShares, &dataBlock->msgContent[24], 6 );
  fieldShares[6] = 0;
  pmOrderInfo[orderId % MAX_ORDER_PLACEMENT].filledShares = atoi( fieldShares );
  
  UpdateVerifyPositionInfoFromFillOrder(pmOrderInfo[orderId % MAX_ORDER_PLACEMENT].symbol, pmOrderInfo[orderId % MAX_ORDER_PLACEMENT].filledShares, pmOrderInfo[orderId % MAX_ORDER_PLACEMENT].side, account, TRADING_AUTOMATIC);
  
  //Update leftShares
  pmOrderInfo[orderId % MAX_ORDER_PLACEMENT].leftShares -= pmOrderInfo[orderId % MAX_ORDER_PLACEMENT].filledShares;

  if (pmOrderInfo[orderId % MAX_ORDER_PLACEMENT].leftShares == 0)
  {
    symbolIndex = GetStockSymbolIndex(pmOrderInfo[orderId % MAX_ORDER_PLACEMENT].symbol);
    if (symbolIndex == -1)
    {
      TraceLog(ERROR_LEVEL, "UpdateVerifyPositionInfoFromRASHExecutedFromPM(Executed): Invalid symbol(%.8s, -1)\n", pmOrderInfo[orderId % MAX_ORDER_PLACEMENT].symbol);
      return SUCCESS;
    }
    
    verifyPositionInfo[account][symbolIndex].isTrading = TRADING_NONE;
    verifyPositionInfo[account][symbolIndex].isPMOwner = NO;
    verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();

    /*  Stuck of this symbol on this account has been resolved.
      Check if there is any other stuck of the same symbol from another account
      If, so engage it to PM */
    EnqueueStuckOfSameSymbolToPM(pmOrderInfo[orderId % MAX_ORDER_PLACEMENT].symbol, symbolIndex, account);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  UpdateVerifyPositionInfoFromRASHCanceledManualOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int UpdateVerifyPositionInfoFromRASHCanceledManualOrder(t_DataBlock *dataBlock, int sentBy)
{
  int symbolIndex;
  int orderId;
  int account;
  
  // OrderId (stripped first two digits of account)
  char fieldToken[16];
  memcpy( fieldToken, &dataBlock->msgContent[12], 12);
  fieldToken[12] = 0;
  orderId = atoi( fieldToken );
  
  if (orderId >= MAX_MANUAL_ORDER || orderId < 0)
  {
    TraceLog(ERROR_LEVEL, "UpdateVerifyPositionInfoFromRASHCanceledManualOrder(Canceled): Invalid orderId(%d)\n", orderId);
    return SUCCESS;
  }
  
  symbolIndex = GetStockSymbolIndex(manualOrderInfo[orderId].symbol);
  if (symbolIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "UpdateVerifyPositionInfoFromRASHCanceledManualOrder(Canceled): Invalid symbol(%.8s)\n", manualOrderInfo[orderId].symbol);
    return SUCCESS;
  }
  
  //account
  if (sentBy == AS_MANUAL_ORDER)
  {
    account = Manual_Order_Account_Mapping[orderId];
    dataBlock->addContent[DATABLOCK_OFFSET_ACCOUNT_ID] = account;
  }
  else if (sentBy == PM_MANUAL_ORDER)
  {
    account = dataBlock->addContent[DATABLOCK_OFFSET_ACCOUNT_ID];
  }
  else
  {
    TraceLog(ERROR_LEVEL, "UpdateVerifyPositionInfoFromRASHCanceledManualOrder(Canceled): could not detect account\n");
    return SUCCESS;
  }
  
  verifyPositionInfo[account][symbolIndex].isTrading = TRADING_NONE;
  verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  UpdateVerifyPositionInfoFromRASHCanceledFromPM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int UpdateVerifyPositionInfoFromRASHCanceledFromPM(t_DataBlock *dataBlock)
{
  int symbolIndex;
  int orderId;
  
  // OrderId (stripped first two digits of account)
  char fieldToken[16];
  memcpy( fieldToken, &dataBlock->msgContent[12], 12);
  fieldToken[12] = 0;
  orderId = atoi( fieldToken );
  
  if ((orderId % MAX_ORDER_PLACEMENT) == 0 || orderId < 0)
  {
    TraceLog(ERROR_LEVEL, "UpdateVerifyPositionInfoFromRASHCanceledFromPM(Canceled): Invalid orderId(%d)\n", orderId);
    return SUCCESS;
  }
  
  symbolIndex = GetStockSymbolIndex(pmOrderInfo[orderId % MAX_ORDER_PLACEMENT].symbol);
  if (symbolIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "UpdateVerifyPositionInfoFromRASHCanceledFromPM(Canceled): Invalid symbol(%.8s)\n", pmOrderInfo[orderId % MAX_ORDER_PLACEMENT].symbol);
    return SUCCESS;
  }
  
  int account = dataBlock->addContent[DATABLOCK_OFFSET_ACCOUNT_ID];
  
  verifyPositionInfo[account][symbolIndex].isTrading = TRADING_NONE;
  verifyPositionInfo[account][symbolIndex].isPMOwner = NO;
  verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  UpdateVerifyPositionInfoFromRASHRejectedManualOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int UpdateVerifyPositionInfoFromRASHRejectedManualOrder(t_DataBlock *dataBlock, int sentBy)
{
  int symbolIndex;
  int orderId;
  int account;
  
  // OrderId (stripped first two digits of account)
  char fieldToken[16];
  memcpy( fieldToken, &dataBlock->msgContent[12], 12 );
  fieldToken[12] = 0;
  orderId = atoi( fieldToken );
  
  if (orderId >= MAX_MANUAL_ORDER || orderId < 0)
  {
    TraceLog(ERROR_LEVEL, "UpdateVerifyPositionInfoFromRASHRejectedManualOrder(Rejected): Invalid orderId(%d)\n", orderId);
    return SUCCESS;
  }
  
  symbolIndex = GetStockSymbolIndex(manualOrderInfo[orderId].symbol);
  if (symbolIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "UpdateVerifyPositionInfoFromRASHRejectedManualOrder(Rejected): Invalid symbol(%.8s)\n", manualOrderInfo[orderId].symbol);
    return SUCCESS;
  }
  
  //account
  if (sentBy == AS_MANUAL_ORDER)
  {
    account = Manual_Order_Account_Mapping[orderId];
    dataBlock->addContent[DATABLOCK_OFFSET_ACCOUNT_ID] = account;
    AutoCancelOrder.orderInfo[orderId].leftShares = 0;
  }
  else if (sentBy == PM_MANUAL_ORDER)
  {
    account = dataBlock->addContent[DATABLOCK_OFFSET_ACCOUNT_ID];
  }
  else
  {
    TraceLog(ERROR_LEVEL, "UpdateVerifyPositionInfoFromRASHRejectedManualOrder(Rejected): could not detect account\n");
    return SUCCESS;
  }
  
  verifyPositionInfo[account][symbolIndex].isTrading = TRADING_NONE;
  verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  UpdateVerifyPositionInfoFromRASHRejectedFromPM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int UpdateVerifyPositionInfoFromRASHRejectedFromPM(t_DataBlock *dataBlock)
{
  int symbolIndex;
  int orderId;
  int account = dataBlock->addContent[DATABLOCK_OFFSET_ACCOUNT_ID];
  
  // OrderId (stripped first two digits of account)
  char fieldToken[16];
  memcpy( fieldToken, &dataBlock->msgContent[12], 12 );
  fieldToken[12] = 0;
  orderId = atoi( fieldToken );
  
  if ((orderId % MAX_ORDER_PLACEMENT) == 0 || orderId < 0)
  {
    TraceLog(ERROR_LEVEL, "UpdateVerifyPositionInfoFromRASHRejectedFromPM(Rejected): Invalid orderId(%d)\n", orderId);
    return SUCCESS;
  }
  
  symbolIndex = GetStockSymbolIndex(pmOrderInfo[orderId % MAX_ORDER_PLACEMENT].symbol);
  if (symbolIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "UpdateVerifyPositionInfoFromRASHRejectedFromPM(Rejected): Invalid symbol(%.8s)\n", pmOrderInfo[orderId % MAX_ORDER_PLACEMENT].symbol);
    return SUCCESS;
  }
  
  verifyPositionInfo[account][symbolIndex].isTrading = TRADING_NONE;
  verifyPositionInfo[account][symbolIndex].isPMOwner = NO;
  verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
  
  return SUCCESS;
}


/****************************************************************************
- Function name:  UpdateVerifyPositionInfoFromDIRECTAcceptedManualOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int UpdateVerifyPositionInfoFromDIRECTAcceptedManualOrder(t_DataBlock *dataBlock, int sentBy)
{
  char action;
  int orderId;
  int account;
  int symbolIndex;
  
  // Order ID
  orderId = GetIntNumberBigEndian(&dataBlock->msgContent[24]);
  
  if (orderId >= MAX_MANUAL_ORDER || orderId < 0)
  {
    TraceLog(ERROR_LEVEL, "UpdateVerifyPositionInfoFromDIRECTAcceptedManualOrder(Accpted): Invalid orderId(%d)\n", orderId);
    return SUCCESS;
  }

  // Symbol
  strncpy(manualOrderInfo[orderId].symbol, ManualArcaDirectOrderInfo[orderId].symbol, SYMBOL_LEN);
  TrimRight(manualOrderInfo[orderId].symbol, strnlen(manualOrderInfo[orderId].symbol, SYMBOL_LEN)); //Convert all ' ' to null chars
  
  symbolIndex = GetStockSymbolIndex(manualOrderInfo[orderId].symbol);
  if (symbolIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "UpdateVerifyPositionInfoFromDIRECTAcceptedManualOrder(Accepted): Invalid symbol (%.8s)\n", manualOrderInfo[orderId].symbol);
    return SUCCESS;
  }
  
  if (sentBy == AS_MANUAL_ORDER)
  {
    account = Manual_Order_Account_Mapping[orderId];
    dataBlock->addContent[DATABLOCK_OFFSET_ACCOUNT_ID] = account;
  }
  else if (sentBy == PM_MANUAL_ORDER)
  {
    account = dataBlock->addContent[DATABLOCK_OFFSET_ACCOUNT_ID];
  }
  else
  {
    TraceLog(ERROR_LEVEL, "UpdateVerifyPositionInfoFromDIRECTAcceptedManualOrder(Accepted): could not detect account\n");
    return SUCCESS;
  }
  
  action = ManualArcaDirectOrderInfo[orderId].side;
  if(action == '1')
  {
    //We sent new order to BUY stock that mean we have a SHORT on TT (side = 0)
    manualOrderInfo[orderId].side = 0;
  }
  else if(action == '2' || action == '5')
  {
    manualOrderInfo[orderId].side = 1;
  }

  // Shares
  manualOrderInfo[orderId].leftShares = ManualArcaDirectOrderInfo[orderId].shares;
  if (sentBy == AS_MANUAL_ORDER)
  {
    AutoCancelOrder.orderInfo[orderId].marked = 1;
    long ecnOrderID = GetLongNumberBigEndian((char *)&dataBlock->msgContent[28]);
    sprintf(AutoCancelOrder.orderInfo[orderId].ecnOrderID, "%lu", ecnOrderID);
    AutoCancelOrder.orderInfo[orderId].leftShares = manualOrderInfo[orderId].leftShares;
  }
  verifyPositionInfo[account][symbolIndex].isTrading = TRADING_MANUAL;
  verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  UpdateVerifyPositionInfoFromDIRECTAcceptedFromPM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int UpdateVerifyPositionInfoFromDIRECTAcceptedFromPM(t_DataBlock *dataBlock)
{
  int orderId;
  int symbolIndex;
  char action;
  int account = dataBlock->addContent[DATABLOCK_OFFSET_ACCOUNT_ID];
  
  // Order ID
  orderId = GetIntNumberBigEndian(&dataBlock->msgContent[24]);
  
  if ((orderId % MAX_ORDER_PLACEMENT) == 0 || orderId < 0)
  {
    TraceLog(ERROR_LEVEL, "%s: Invalid orderId(%d)\n", __func__, orderId);
    return SUCCESS;
  }

  // Symbol
  strncpy(pmOrderInfo[orderId % MAX_ORDER_PLACEMENT].symbol, PMArcaDirectOrderInfo[orderId % MAX_ORDER_PLACEMENT].symbol, SYMBOL_LEN);
  TrimRight( pmOrderInfo[orderId % MAX_ORDER_PLACEMENT].symbol, strnlen(pmOrderInfo[orderId % MAX_ORDER_PLACEMENT].symbol, SYMBOL_LEN) ); //Convert all ' ' to null chars

  symbolIndex = GetStockSymbolIndex(pmOrderInfo[orderId % MAX_ORDER_PLACEMENT].symbol);
  if (symbolIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "%s: Invalid symbol (%.8s)\n", __func__, pmOrderInfo[orderId % MAX_ORDER_PLACEMENT].symbol);
    return SUCCESS;
  }
  
  action = PMArcaDirectOrderInfo[orderId % MAX_ORDER_PLACEMENT].side;
  if(action == '1')
  {
    //We sent new order to BUY stock that mean we have a SHORT on TT (side = 0)
    pmOrderInfo[orderId % MAX_ORDER_PLACEMENT].side = 0;
  }
  else if(action == '2' || action == '5')
  {
    pmOrderInfo[orderId % MAX_ORDER_PLACEMENT].side = 1;
  }

  // Shares
  pmOrderInfo[orderId % MAX_ORDER_PLACEMENT].leftShares = PMArcaDirectOrderInfo[orderId % MAX_ORDER_PLACEMENT].shares;
  
  verifyPositionInfo[account][symbolIndex].isSleeping = NO;
  verifyPositionInfo[account][symbolIndex].isTrading = TRADING_AUTOMATIC;
  verifyPositionInfo[account][symbolIndex].isPMOwner = YES;
  verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  UpdateVerifyPositionInfoFromDIRECTExecutedManualOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int UpdateVerifyPositionInfoFromDIRECTExecutedManualOrder(t_DataBlock *dataBlock, int sentBy)
{
  int orderId;
  int symbolIndex;

  // OrderId
  orderId = GetIntNumberBigEndian(&dataBlock->msgContent[24]);
  
  if (orderId >= MAX_MANUAL_ORDER || orderId < 0)
  {
    TraceLog(ERROR_LEVEL, "UpdateVerifyPositionInfoFromDIRECTExecutedManualOrder(Executed): Invalid orderId(%d)\n", orderId);
    return SUCCESS;
  }
  
  //account
  int account;
  if (sentBy == AS_MANUAL_ORDER)
  {
    account = Manual_Order_Account_Mapping[orderId];
    dataBlock->addContent[DATABLOCK_OFFSET_ACCOUNT_ID] = account;
  }
  else if (sentBy == PM_MANUAL_ORDER)
  {
    account = dataBlock->addContent[DATABLOCK_OFFSET_ACCOUNT_ID];
  }
  else
  {
    TraceLog(ERROR_LEVEL, "UpdateVerifyPositionInfoFromDIRECTExecutedManualOrder(Executed): could not detect account\n");
    return SUCCESS;
  }
  
  // Filled Shares
  manualOrderInfo[orderId].filledShares = GetIntNumberBigEndian(&dataBlock->msgContent[64]);
  
  UpdateVerifyPositionInfoFromFillOrder(manualOrderInfo[orderId].symbol, manualOrderInfo[orderId].filledShares, manualOrderInfo[orderId].side, account, TRADING_MANUAL);
  
  //Update leftShares
  manualOrderInfo[orderId].leftShares -= manualOrderInfo[orderId].filledShares;
  
  if (sentBy == AS_MANUAL_ORDER)
  {
    AutoCancelOrder.orderInfo[orderId].leftShares = manualOrderInfo[orderId].leftShares;
  }

  if (manualOrderInfo[orderId].leftShares == 0)
  {
    symbolIndex = GetStockSymbolIndex(manualOrderInfo[orderId].symbol);
    if (symbolIndex == -1)
    {
      TraceLog(ERROR_LEVEL, "UpdateVerifyPositionInfoFromDIRECTExecutedManualOrder(Executed): Invalid symbol(%.8s)\n", manualOrderInfo[orderId].symbol);
      return SUCCESS;
    }
    
    verifyPositionInfo[account][symbolIndex].isTrading = TRADING_NONE;
    verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  UpdateVerifyPositionInfoFromDIRECTExecutedFromPM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int UpdateVerifyPositionInfoFromDIRECTExecutedFromPM(t_DataBlock *dataBlock)
{
  int orderId;
  int symbolIndex;
  int account = dataBlock->addContent[DATABLOCK_OFFSET_ACCOUNT_ID];

  // OrderId
  orderId = GetIntNumberBigEndian(&dataBlock->msgContent[24]);
  
  if ((orderId % MAX_ORDER_PLACEMENT) == 0 || orderId < 0)
  {
    TraceLog(ERROR_LEVEL, "UpdateVerifyPositionInfoFromDIRECTExecutedFromPM(Executed): Invalid orderId(%d)\n", orderId);
    return SUCCESS;
  }
  
  // Filled Shares
  pmOrderInfo[orderId % MAX_ORDER_PLACEMENT].filledShares = GetIntNumberBigEndian(&dataBlock->msgContent[64]);
  
  UpdateVerifyPositionInfoFromFillOrder(pmOrderInfo[orderId % MAX_ORDER_PLACEMENT].symbol, pmOrderInfo[orderId % MAX_ORDER_PLACEMENT].filledShares, pmOrderInfo[orderId % MAX_ORDER_PLACEMENT].side, account, TRADING_AUTOMATIC);
  
  //Update leftShares
  pmOrderInfo[orderId % MAX_ORDER_PLACEMENT].leftShares -= pmOrderInfo[orderId % MAX_ORDER_PLACEMENT].filledShares;

  if (pmOrderInfo[orderId % MAX_ORDER_PLACEMENT].leftShares == 0)
  {
    symbolIndex = GetStockSymbolIndex(pmOrderInfo[orderId % MAX_ORDER_PLACEMENT].symbol);
    if (symbolIndex == -1)
    {
      TraceLog(ERROR_LEVEL, "UpdateVerifyPositionInfoFromDIRECTExecutedFromPM(Executed): Invalid symbol(%.8s, -1)\n", pmOrderInfo[orderId % MAX_ORDER_PLACEMENT].symbol);
      return SUCCESS;
    }
    
    verifyPositionInfo[account][symbolIndex].isTrading = TRADING_NONE;
    verifyPositionInfo[account][symbolIndex].isPMOwner = NO;
    verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();

    /*  Stuck of this symbol on this account has been resolved.
      Check if there is any other stuck of the same symbol from another account
      If, so engage it to PM */
    EnqueueStuckOfSameSymbolToPM(pmOrderInfo[orderId % MAX_ORDER_PLACEMENT].symbol, symbolIndex, account);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  UpdateVerifyPositionInfoFromDIRECTCanceledManualOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int UpdateVerifyPositionInfoFromDIRECTCanceledManualOrder(t_DataBlock *dataBlock, int sentBy)
{
  int symbolIndex;
  int orderId;
  int account;
  
  // OrderId
  orderId = GetIntNumberBigEndian(&dataBlock->msgContent[24]);
  
  if (orderId >= MAX_MANUAL_ORDER || orderId < 0)
  {
    TraceLog(ERROR_LEVEL, "UpdateVerifyPositionInfoFromDIRECTCanceledManualOrder(Canceled): Invalid orderId(%d)\n", orderId);
    return SUCCESS;
  }
  
  symbolIndex = GetStockSymbolIndex(manualOrderInfo[orderId].symbol);
  if (symbolIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "UpdateVerifyPositionInfoFromDIRECTCanceledManualOrder(Canceled): Invalid symbol(%.8s)\n", manualOrderInfo[orderId].symbol);
    return SUCCESS;
  }
  
  //account
  if (sentBy == AS_MANUAL_ORDER)
  {
    account = Manual_Order_Account_Mapping[orderId];
    dataBlock->addContent[DATABLOCK_OFFSET_ACCOUNT_ID] = account;
    
    AutoCancelOrder.orderInfo[orderId].leftShares = 0;
  }
  else if (sentBy == PM_MANUAL_ORDER)
  {
    account = dataBlock->addContent[DATABLOCK_OFFSET_ACCOUNT_ID];
  }
  else
  {
    TraceLog(ERROR_LEVEL, "UpdateVerifyPositionInfoFromDIRECTCanceledManualOrder(Canceled): could not detect account\n");
    return SUCCESS;
  }
  
  verifyPositionInfo[account][symbolIndex].isTrading = TRADING_NONE;
  verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  UpdateVerifyPositionInfoFromDIRECTCanceledFromPM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int UpdateVerifyPositionInfoFromDIRECTCanceledFromPM(t_DataBlock *dataBlock)
{
  int symbolIndex;
  int orderId;
  
  // OrderId
  orderId = GetIntNumberBigEndian(&dataBlock->msgContent[24]);
  
  if ((orderId % MAX_ORDER_PLACEMENT) == 0 || orderId < 0)
  {
    TraceLog(ERROR_LEVEL, "UpdateVerifyPositionInfoFromDIRECTCanceledFromPM(Canceled): Invalid orderId(%d)\n", orderId);
    return SUCCESS;
  }
  
  symbolIndex = GetStockSymbolIndex(pmOrderInfo[orderId % MAX_ORDER_PLACEMENT].symbol);
  if (symbolIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "UpdateVerifyPositionInfoFromDIRECTCanceledFromPM(Canceled): Invalid symbol(%.8s)\n", pmOrderInfo[orderId % MAX_ORDER_PLACEMENT].symbol);
    return SUCCESS;
  }
  
  int account = dataBlock->addContent[DATABLOCK_OFFSET_ACCOUNT_ID];
  
  verifyPositionInfo[account][symbolIndex].isTrading = TRADING_NONE;
  verifyPositionInfo[account][symbolIndex].isPMOwner = NO;
  verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  UpdateVerifyPositionInfoFromDIRECTRejectedManualOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int UpdateVerifyPositionInfoFromDIRECTRejectedManualOrder(t_DataBlock *dataBlock, int sentBy)
{
  if(dataBlock->msgContent[32] != '1') // not order reject
  {
    return ERROR;
  }
  
  int symbolIndex;
  int orderId;
  int account;
  
  // OrderId
  orderId = GetIntNumberBigEndian(&dataBlock->msgContent[24]);
  
  if (orderId >= MAX_MANUAL_ORDER || orderId < 0)
  {
    TraceLog(ERROR_LEVEL, "UpdateVerifyPositionInfoFromDIRECTRejectedManualOrder(Rejected): Invalid orderId(%d)\n", orderId);
    return SUCCESS;
  }
  
  symbolIndex = GetStockSymbolIndex(manualOrderInfo[orderId].symbol);
  if (symbolIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "UpdateVerifyPositionInfoFromDIRECTRejectedManualOrder(Rejected): Invalid symbol(%.8s)\n", manualOrderInfo[orderId].symbol);
    return SUCCESS;
  }
  
  //account
  if (sentBy == AS_MANUAL_ORDER)
  {
    account = Manual_Order_Account_Mapping[orderId];
    dataBlock->addContent[DATABLOCK_OFFSET_ACCOUNT_ID] = account;
    AutoCancelOrder.orderInfo[orderId].leftShares = 0;
  }
  else if (sentBy == PM_MANUAL_ORDER)
  {
    account = dataBlock->addContent[DATABLOCK_OFFSET_ACCOUNT_ID];
  }
  else
  {
    TraceLog(ERROR_LEVEL, "UpdateVerifyPositionInfoFromDIRECTRejectedManualOrder(Rejected): could not detect account\n");
    return SUCCESS;
  }
  
  verifyPositionInfo[account][symbolIndex].isTrading = TRADING_NONE;
  verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  UpdateVerifyPositionInfoFromDIRECTRejectedFromPM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int UpdateVerifyPositionInfoFromDIRECTRejectedFromPM(t_DataBlock *dataBlock)
{
  if(dataBlock->msgContent[32] != '1') // not order reject
  {
    return ERROR;
  }
  
  int symbolIndex;
  int orderId;
  int account = dataBlock->addContent[DATABLOCK_OFFSET_ACCOUNT_ID];
  
  // OrderId
  orderId =  GetIntNumberBigEndian(&dataBlock->msgContent[24]);
  
  if ((orderId % MAX_ORDER_PLACEMENT) == 0 || orderId < 0)
  {
    TraceLog(ERROR_LEVEL, "UpdateVerifyPositionInfoFromDIRECTRejectedFromPM(Rejected): Invalid orderId(%d)\n", orderId);
    return SUCCESS;
  }
  
  symbolIndex = GetStockSymbolIndex(pmOrderInfo[orderId % MAX_ORDER_PLACEMENT].symbol);
  if (symbolIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "UpdateVerifyPositionInfoFromDIRECTRejectedFromPM(Rejected): Invalid symbol(%.8s)\n", pmOrderInfo[orderId % MAX_ORDER_PLACEMENT].symbol);
    return SUCCESS;
  }
  
  verifyPositionInfo[account][symbolIndex].isTrading = TRADING_NONE;
  verifyPositionInfo[account][symbolIndex].isPMOwner = NO;
  verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
  
  return SUCCESS;
}

void ____Helper_Functions____(){return;}

/****************************************************************************
- Function name:  IsAbleToPlaceManualOrderOnAS
- Input:      ECN Order ID
- Output:     0 or 1
- Return:     
- Description:    Check to see if it's able to place manual order on AS
- Usage:      Used when there is a request to send manual order from TT
****************************************************************************/
int IsAbleToPlaceManualOrderOnAS(int ECNId)
{
  if (ECNId == TYPE_ARCA_DIRECT)
  {
    if (orderStatusMgmt[AS_ARCA_DIRECT_INDEX].isOrderConnected == CONNECTED)
    {
      return 1;
    }
    else
    {
      return 0;
    }
  }
  else if (ECNId == TYPE_NASDAQ_RASH)
  {
    if (orderStatusMgmt[AS_NASDAQ_RASH_INDEX].isOrderConnected == CONNECTED)
    {
      return 1;
    }
    else
    {
      return 0;
    }
  }
  else
  {
    TraceLog(ERROR_LEVEL, "Invalid order type (%d)\n", ECNId);
  }
  
  return 0;
}

/****************************************************************************
- Function name:  IsAbleToPlaceManualOrderOnPM
- Input:      ECN Order ID
- Output:     0 or 1
- Return:     
- Description:    Check to see if it's able to place manual order on PM
- Usage:      Used when there is a request to send manual order from TT
****************************************************************************/
int IsAbleToPlaceManualOrderOnPM(int ECNId)
{
  if (pmInfo.connection.status == CONNECT)
  {
    //PM is connected
    if (ECNId == TYPE_ARCA_DIRECT)
    {
      if (pmInfo.currentStatus.pmOecStatus[PM_ARCA_DIRECT_INDEX] == CONNECTED)
      {
        return 1;
      }
      
      //Order is not connected --> cannot place order
      return 0;
    }
    else if (ECNId == TYPE_NASDAQ_RASH)
    {
      if (pmInfo.currentStatus.pmOecStatus[PM_NASDAQ_RASH_INDEX] == CONNECTED)
      {
        return 1;
      }
      
      //Order is not connected --> cannot place order
      return 0;
    }
    else
    {
      TraceLog(ERROR_LEVEL, "Invalid order type (%d)\n", ECNId);
    }
    
    //ECN is not ARCA DIRECT or RASH --> invalid ECN Order --> cannot place order
    return 0;
  }
  else
  {
    //PM is not connected --> cannot place order
    return 0;
  }
}

/****************************************************************************
- Function name:  IsCTSOrUTDFConnectedOnPM
- Input:      N/A
- Output:     1 or 0
- Return:     
- Description:    Check to see if CTS or UTDF is connected on PM
- Usage:      Used when there is a request to send manual order from TT
          So we can check if it's able to check the order price with
          last trade from CTS/UTDF
****************************************************************************/
int IsCTSOrUTDFConnectedOnPM()
{
  if (pmInfo.connection.status == CONNECT)
  {
    if ((pmInfo.currentStatus.statusUTDF == CONNECTED) ||
      (pmInfo.currentStatus.statusCTS_Feed == CONNECTED))
      {
        return 1;
      }
    
    //None of CTS or UTDF connected
    return 0;
  }
  else
  {
    //PM is not connected
    return 0;
  }
}

/****************************************************************************
- Function name:  IsAbleToPlaceOrderOnASOrPM
- Input:      ECN Order ID
- Output:     Number indicating result
- Return:     
- Description:    Check to see if it's able to place manual order
          on AS or PM, or both
- Usage:      Used when there is a request to send manual order from TT
****************************************************************************/
int IsAbleToPlaceOrderOnASOrPM(int ECNId)
{
  /* if returned value is
    0: Not able to place order on AS or PM
    1: Able to place order on AS only
    2: Able to place order on PM only
    3: Able to place order on AS and PM
  */
  
  int ret = 0;
  if (IsAbleToPlaceManualOrderOnPM(ECNId) == 1)
  {
    ret += 2;
  }
  
  if (IsAbleToPlaceManualOrderOnAS(ECNId) == 1)
  {
    ret += 1;
  }
  
  return ret;
}

/****************************************************************************
- Function name:  ConvertArcaSymbolToComstock
- Input:      root symbol
          suffix
- Output:     symbol in comstock format
- Return:     
- Description:    
- Usage:      
****************************************************************************/
void ConvertArcaSymbolToComstock(const char *root, const char *suffix, char symbol[SYMBOL_LEN])
{
  /*
    ---------------------------------------------------------------------------------------------------
    Following conversion is taken from "NYSE Arca FIX Specification
    For Products Available via the NYSE Arca FIX Gateway", version Version 11.9.3 (Date: June 28, 2012)
    ---------------------------------------------------------------------------------------------------
    Security Categorization | Comstock |  Symbol/Sfx (Tag 65)
    Preferred           -         PR
    Preferred Class �A�     -A        PRA
    Class �A�           .A        A
    Warrants          +         WS
    Warrants Class �A�      +A        WSA
    Preferred �A� when issued   -A#       PRAWI
    Units             .U        U
    When issued         #         WI
    Preferred when issued     -#        PRWI
    Class �A� When issued     .A#       AWI
    Test symbol         .TEST       TEST
  */
  
  //Convert suffix to part of comstock symbol
  char tail[32], tmpSymbol[32];
  int i, isUnit = 1;
  int offset = 0;
  int len = strlen(suffix);
  
  if (((suffix[0] == 'P') && (suffix[1] == 'R')) ||
    ((suffix[0] == 'W') && (suffix[1] == 'S')) ||
    ((suffix[0] == 'W') && (suffix[1] == 'I')) )  //Check to convert to "."
  {
    isUnit = 0;
  }
  
  if (isUnit == 1)
  {
    tail[0] = '.';
    offset = 1;
  }
  
  for (i = 0; i < len; i++)
  {
    switch (suffix[i])
    {
      case 'P':   // PR to -
        if (suffix[i+1] == 'R')
        {
          tail[offset++] = '-';
          i++;
        }
        else
          tail[offset++] = suffix[i];
        break;
      case 'W':   // "WS" to "+", "WI" to "#"
        if (suffix[i+1] == 'S')
        {
          tail[offset++] = '+';
          i++;
        }
        else if (suffix[i+1] == 'I')
        {
          tail[offset++] = '#';
          i++;
        }
        else
          tail[offset++] = suffix[i];
        break;
      default:
        tail[offset++] = suffix[i];
        break;
    }
  }
  
  tail[offset] = 0;
  sprintf(tmpSymbol, "%s%s", root, tail);
  strncpy(symbol, tmpSymbol, SYMBOL_LEN);
  
  return;
}

/****************************************************************************
- Function name:  CalculateFillFee
- Input:      
- Output:     
- Return:     
- Description:  
- Usage:      
****************************************************************************/
double CalculateFillFee(int size, double price, int side, char *liquidityField, int ecnId)
{ 
  double totalFees = 0.00, totalSecFees = 0.00;
  double totalTafFee = 0.00, totalClearingCommission = 0.00;
  double totalVenueFee = 0.00;

  // Used to calculate fee for ECNs but EDGX, EDGA
  char liquidity = liquidityField[0];
      
  //Sec fees
  if (side == SELL_SIDE)
  {
    totalSecFees += (size * price * FeeConf.secFee);
  }
  
  //Taf fees
  totalTafFee += (size * FeeConf.tafFee);
  
  //Commission
  totalClearingCommission += (size * FeeConf.commission);

  switch(ecnId)
  {
    case TYPE_ARCA_DIRECT:
    {
      /*
      Executions on NYSE Arca
      A  Added liquidity. 
      B  Added liquidity while order was not displayed. 
      D  Added liquidity on a sub-dollar execution. 
      M  Added liquidity on an MPL order. 
      S  Added liquidity, set new Arca BBO. Effective May 28th, 2012
      R  Reduced liquidity. 
      E  Reduced liquidity on a sub-dollar execution. 
      L  Reduced liquidity on an MPL order. 
      O  Neutral - Orders executed in an auction. 
      G  Opening or halt auction of market or auction-only order. 
      Z  Closing auction of market or auction-only order. 
      Routed Orders
      X  Routed to another venue. 
      F  Routed to NYSE/AMEX and added liquidity.* 
      N  Routed to NYSE/AMEX and reduced liquidity. 
      H  Routed sub-dollar execution. 
      C  Routed to NYSE/AMEX and participated in the open or halt auction.* 
      U  Routed NYSE MOC/LOC order. 
      Y  Routed AMEX MOC/LOC order. 
      W Routed to NYSE/AMEX and re-routed to an external market.
      */
      if (liquidity == 'A' || liquidity == 'B' || liquidity == 'D' || liquidity == 'M' || liquidity == 'S') // Add
      {
        totalVenueFee += fge(price, 1.00)?(size * FeeConf.addVenueFeeWithPriceLager1):(size * FeeConf.addVenueFeeWithPriceSmaler1);
      }
      else if (liquidity == 'R' || liquidity == 'E' || liquidity == 'L') // Remove
      {
        totalVenueFee += fge(price, 1.00)?(size * FeeConf.removeVenueFeeWithPriceLager1):(size * price * FeeConf.removeVenueFeeWithPriceSmaler1);
      }
      else if (liquidity == 'G' || liquidity == 'Z' || liquidity == 'O') //auction
      {
        totalVenueFee += (size * FeeConf.auctionVenueFee);
      }
      else if (liquidity == 'X' || liquidity == 'F' || liquidity == 'N' || liquidity == 'H' || liquidity == 'C' || liquidity == 'U' || liquidity == 'Y' || liquidity == 'W')//Route
      {
        totalVenueFee += (size * FeeConf.arcaRouteVenueFee);
      }
      else
      {
        TraceLog(ERROR_LEVEL, "(%s): Invalid liquidity (%c) detected on ecnId %d\n", __func__, liquidity, ecnId);
      }
    }
      break;
    case TYPE_NASDAQ_OUCH:
    case TYPE_NASDAQ_OUBX:
    case TYPE_PSX:
    {
      /*
      Liquidity Flags 
      Flag   Value 
      A   Added 
      R   Removed 
      O  Opening Cross (billable) 
      M  Opening Cross (non-billable) 
      C  Closing Cross (billable) 
      L  Closing Cross (non-billable) 
      H  Halt/IPO Cross (billable) 
      K  Halt/IPO Cross (billable) 
      m  Removed liquidity at a midpoint
      k  Added liquidity via a midpoint order
      J  Non-displayed adding liquidity
      6  Liquidity Removing Order in designated securities
      7  Displayed, liquidity-adding order improves the NBBO
      8  Displayed, liquidity-adding order sets the QBBO while joining the NBBO
      d  Retail designated execution that removed liquidity
      e  Retail designated execution that added displayed liquidity
      f  Retail designated execution that added non-displayed liquidity
      */
      if (liquidity == 'A' || liquidity == 'k' || liquidity == 'J' || 
        liquidity == '7' || liquidity == '8' || liquidity == 'e' || liquidity == 'f') //Add
      {
        totalVenueFee += fge(price, 1.00)?(size * FeeConf.addVenueFeeWithPriceLager1):(size * FeeConf.addVenueFeeWithPriceSmaler1);
      }
      else if (liquidity == 'R' || liquidity == 'm' || liquidity == '6' || liquidity == 'd')//Remove
      {
        totalVenueFee += fge(price, 1.00)?(size * FeeConf.removeVenueFeeWithPriceLager1):(size * price * FeeConf.removeVenueFeeWithPriceSmaler1);
      }
      else if (liquidity == 'O' || liquidity == 'M' || liquidity == 'L' || liquidity == 'H' || liquidity == 'K' || liquidity == 'C') //auction
      {
        totalVenueFee += (size * FeeConf.auctionVenueFee);
      }
      else 
      {
        TraceLog(ERROR_LEVEL, "(%s): Invalid liquidity (%c) detected on ecnId %d\n", __func__, liquidity, ecnId);
      }
    }
      break;
    case TYPE_NASDAQ_RASH:
    {
      /*
      Flag Value 
      A Added 
      R Removed 
      J Non-displayed and added liquidity 
      V Displayed added liquidity with original order size of greater than or equal to 2,000 shares 
      X Routed 
      D DOT 
      F Added or Opening Trade (on NYSE) 
      G Odd Lot or On-Close order (on NYSE) 
      O Open Cross (billable) 
      M Open Cross (non-billable) 
      C Closing Cross (billable) 
      L Closing Cross (non-billable) 
      H Halt/IPO Cross (billable) 
      K Halt/IPO Cross (billable) 
      I Intraday/Post-Market Cross 
      Y Re-Routed by NYSE 
      S Odd Lot Execution (on NYSE) 
      U Added Liquidity (on NYSE) 
      B Routed to BX 
      E NYSE Other 
      P Routed to PSX 
      T Opening Trade (on ARCA) 
      Z On-Close order (on ARCA) 
      Q Routed to Nasdaq 
      m Removed liquidity at a midpoint
      k Added liquidity via a midpoint order
      9 Added (displayed) using Minimum Life Order Type 
      6  Liquidity Removing Order in designated securities
      7  Displayed, liquidity-adding order improves the NBBO
      8  Displayed, liquidity-adding order sets the QBBO while joining the NBBO
      d  Retail designated execution that removed liquidity
      e  Retail designated execution that added displayed liquidity
      f  Retail designated execution that added non-displayed liquidity
      */
      if (liquidity == 'A' || liquidity == 'J' || liquidity == 'V' || 
        liquidity == 'D' || liquidity == 'F' || liquidity == 'U' || 
        liquidity == 'k' || liquidity == '9' || liquidity == '7' || 
        liquidity == '8' || liquidity == 'e' || liquidity == 'f') //Add
      {
        totalVenueFee += fge(price, 1.00)?(size * FeeConf.addVenueFeeWithPriceLager1):(size * FeeConf.addVenueFeeWithPriceSmaler1);
      }
      else if (liquidity == 'R' || liquidity == 'm' || liquidity == 'd' || liquidity == '6') //Remove
      {
        totalVenueFee += fge(price, 1.00)?(size * FeeConf.removeVenueFeeWithPriceLager1):(size * price * FeeConf.removeVenueFeeWithPriceSmaler1);
      }
      else if (liquidity == 'G' || liquidity == 'O' || liquidity == 'M' || liquidity == 'C' || liquidity == 'L' || liquidity == 'H' || liquidity == 'K' || liquidity == 'I' || liquidity == 'T' || liquidity == 'Z') //Auction
      {
        totalVenueFee += (size * FeeConf.auctionVenueFee);
      }
      else if (liquidity == 'X' || liquidity == 'Y' || liquidity == 'B' || liquidity == 'P' || liquidity == 'Q') //Route
      {
        totalVenueFee += (size * FeeConf.nasdaqRouteVenueFee);
      }
      else
      {
        TraceLog(ERROR_LEVEL, "(%s): Invalid liquidity (%c) detected on ecnId %d\n", __func__, liquidity, ecnId);
      }
    }
      break;
    case TYPE_NYSE_CCG:
    {
      if (liquidity == '2' || liquidity == '8') //provider
      {
        totalVenueFee += fge(price, 1.00)?(size * FeeConf.addVenueFeeWithPriceLager1):(size * FeeConf.addVenueFeeWithPriceSmaler1);
      }
      else if (liquidity == '1' || liquidity == '9' || liquidity == '3' || liquidity == ' ')  //taker and blended
      {
        totalVenueFee += fge(price, 1.00)?(size * FeeConf.removeVenueFeeWithPriceLager1):(size * price * FeeConf.removeVenueFeeWithPriceSmaler1);
      }
      else if (liquidity == '4' || liquidity == '5' || liquidity == '6' || liquidity == '7' ) // Auction
      {
        totalVenueFee += (size * FeeConf.auctionVenueFee);
      }
      else
      {
        TraceLog(ERROR_LEVEL, "(%s): Invalid liquidity (%c) detected on ecnId %d\n", __func__, liquidity, ecnId);
      }
    }
      break;
    case TYPE_BZX_BOE:
    case TYPE_BYX_BOE:
    {
      if (liquidity == 'A')       // Add
      {
        totalVenueFee += fge(price, 1.00)?(size * FeeConf.addVenueFeeWithPriceLager1):(size * FeeConf.addVenueFeeWithPriceSmaler1);
      }
      else if (liquidity == 'R')      // Remove
      {
        totalVenueFee += fge(price, 1.00)?(size * FeeConf.removeVenueFeeWithPriceLager1):(size * price * FeeConf.removeVenueFeeWithPriceSmaler1);
      }
      else if (liquidity == 'C')      // Auction
      {
        totalVenueFee += (size * FeeConf.auctionVenueFee);
      }
      else if (liquidity == 'X')      // Route
      {
        totalVenueFee += (size * FeeConf.bzxRouteVenueFee);
      }
      else
      {
        TraceLog(ERROR_LEVEL, "(%s): Invalid liquidity (%c) detected on ecnId %d\n", __func__, liquidity, ecnId);
      }
    }
      break;
    case TYPE_EDGX_DIRECT:
    {
      /* Based on Edge_EDGX_Spec_V1_29 */
      if ((strcmp(liquidityField, "EA") == 0) || (strcmp(liquidityField, "HA") == 0) || 
        (strcmp(liquidityField, "AA") == 0) || (strcmp(liquidityField, "CL") == 0) || 
        (strcmp(liquidityField, "DM") == 0) || (strcmp(liquidityField, "MM") == 0) || 
        (strcmp(liquidityField, "PA") == 0) || (strcmp(liquidityField, "RB") == 0) || 
        (strcmp(liquidityField, "RC") == 0) || (strcmp(liquidityField, "RS") == 0) || 
        (strcmp(liquidityField, "RW") == 0) || (strcmp(liquidityField, "RY") == 0) || 
        (strcmp(liquidityField, "RZ") == 0) || 
        liquidity == 'A' || liquidity == 'B' || liquidity == 'F' || 
        liquidity == 'M' || liquidity == 'P' || liquidity == 'V' || 
        liquidity == '3' || liquidity == '9' || liquidity == '4' || 
        liquidity == '8' || liquidity == '1' || liquidity == 'E' || 
        liquidity == 'H' || liquidity == 'Y') //Add
      {
        totalVenueFee += fge(price, 1.00)?(size * FeeConf.addVenueFeeWithPriceLager1):(size * FeeConf.addVenueFeeWithPriceSmaler1);
      }
      else if ((strcmp(liquidityField, "ER") == 0) || (strcmp(liquidityField, "BB") == 0) || 
          (strcmp(liquidityField, "DT") == 0) || (strcmp(liquidityField, "MT") == 0) || 
          (strcmp(liquidityField, "PI") == 0) || (strcmp(liquidityField, "PT") == 0) || 
          liquidity == 'C' || liquidity == 'D' || liquidity == 'G' || liquidity == 'J' || 
          liquidity == 'L' || liquidity == 'N' || liquidity == 'U' || liquidity == 'W' || 
          liquidity == '2' || liquidity == '6') //Remove
      {
        totalVenueFee += fge(price, 1.00)?(size * FeeConf.removeVenueFeeWithPriceLager1):(size * price * FeeConf.removeVenueFeeWithPriceSmaler1);
      }
      else if ((strcmp(liquidityField, "OO") == 0) || liquidity == 'O' || liquidity == '5') //Auction
      {
        totalVenueFee += (size * FeeConf.auctionVenueFee);
      }
      else if ((strcmp(liquidityField, "BY") == 0) || (strcmp(liquidityField, "PX") == 0) || 
          (strcmp(liquidityField, "RP") == 0) || (strcmp(liquidityField, "RQ") == 0) || 
          (strcmp(liquidityField, "RR") == 0) || (strcmp(liquidityField, "SW") == 0) || 
          liquidity == 'I' || liquidity == 'K' || liquidity == 'Q' || liquidity == 'R' || 
          liquidity == 'T' || liquidity == 'X' || liquidity == 'Z' || liquidity == '7' || 
          liquidity == 'S') //Route
      {
        totalVenueFee += (size * FeeConf.edgxRouteVenueFee);
      }
      else
      {
        TraceLog(DEBUG_LEVEL, "(%s): Invalid liquidity (%c) detected on ecnId %d\n", __func__, liquidity, ecnId);
      }
    }
      break;
      
    case TYPE_EDGA_DIRECT:
    {
      /* Based on Edge_EDGX_Spec_V1_29 */
      if ((strcmp(liquidityField, "EA") == 0) || (strcmp(liquidityField, "HA") == 0) || 
        (strcmp(liquidityField, "AA") == 0) || (strcmp(liquidityField, "CL") == 0) || 
        (strcmp(liquidityField, "DM") == 0) || (strcmp(liquidityField, "MM") == 0) || 
        (strcmp(liquidityField, "PA") == 0) || (strcmp(liquidityField, "RB") == 0) || 
        (strcmp(liquidityField, "RC") == 0) || (strcmp(liquidityField, "RS") == 0) || 
        (strcmp(liquidityField, "RW") == 0) || (strcmp(liquidityField, "RY") == 0) || 
        (strcmp(liquidityField, "RZ") == 0) || 
        liquidity == 'A' || liquidity == 'B' || liquidity == 'F' || 
        liquidity == 'M' || liquidity == 'P' || liquidity == 'V' || 
        liquidity == '3' || liquidity == '9' || liquidity == '4' || 
        liquidity == '8' || liquidity == '1' || liquidity == 'E' || 
        liquidity == 'H' || liquidity == 'Y') //Add
      {
        totalVenueFee += fge(price, 1.00)?(size * FeeConf.addVenueFeeWithPriceLager1):(size * FeeConf.addVenueFeeWithPriceSmaler1);
      }
      else if ((strcmp(liquidityField, "ER") == 0) || (strcmp(liquidityField, "BB") == 0) || 
          (strcmp(liquidityField, "DT") == 0) || (strcmp(liquidityField, "MT") == 0) || 
          (strcmp(liquidityField, "PI") == 0) || (strcmp(liquidityField, "PT") == 0) || 
          (strcmp(liquidityField, "HR") == 0) || (strcmp(liquidityField, "CR") == 0) ||
          (strcmp(liquidityField, "PR") == 0) ||
          liquidity == 'C' || liquidity == 'D' || liquidity == 'G' || liquidity == 'J' || 
          liquidity == 'L' || liquidity == 'N' || liquidity == 'U' || liquidity == 'W' || 
          liquidity == '2' || liquidity == '6') //Remove
      {
        totalVenueFee += fge(price, 1.00)?(size * FeeConf.removeVenueFeeWithPriceLager1):(size * price * FeeConf.removeVenueFeeWithPriceSmaler1);
      }
      else if ((strcmp(liquidityField, "OO") == 0) || liquidity == 'O' || liquidity == '5') //Auction
      {
        totalVenueFee += (size * FeeConf.auctionVenueFee);
      }
      else if ((strcmp(liquidityField, "BY") == 0) || (strcmp(liquidityField, "PX") == 0) || 
          (strcmp(liquidityField, "RP") == 0) || (strcmp(liquidityField, "RQ") == 0) || 
          (strcmp(liquidityField, "RR") == 0) || (strcmp(liquidityField, "SW") == 0) || 
          (strcmp(liquidityField, "RT") == 0) || (strcmp(liquidityField, "RX") == 0) || 
          (strcmp(liquidityField, "XR") == 0) || 
          liquidity == 'I' || liquidity == 'K' || liquidity == 'Q' || liquidity == 'R' || 
          liquidity == 'T' || liquidity == 'X' || liquidity == 'Z' || liquidity == '7' || 
          liquidity == 'S') //Route
      {
        totalVenueFee += (size * FeeConf.edgxRouteVenueFee);
      }
      else
      {
        TraceLog(DEBUG_LEVEL, "(%s): Invalid liquidity (%c) detected on ecnId %d\n", __func__, liquidity, ecnId);
      }
    }
      break;
    default:
      TraceLog(ERROR_LEVEL, "(%s): Invalid ECN: ecnId = %d\n", __func__, ecnId);
      break;
  }
    
  totalFees = totalSecFees + totalTafFee + totalClearingCommission + totalVenueFee;
  
  return totalFees;
}

/****************************************************************************
- Function name:  GetRejectCodeFilePath
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int GetRejectCodeFilePath(int ECNType, char *fullPath)
{
  char fileName[MAX_PATH_LEN];

  switch(ECNType)
  {
    case TYPE_ARCA_DIRECT:
      strcpy(fileName, FILE_NAME_OF_ARCA_DIRECT_REJECT_CODE);
      break;
    case TYPE_NASDAQ_OUCH:
      strcpy(fileName, FILE_NAME_OF_NASDAQ_OUCH_REJECT_CODE);
      break;
    case TYPE_NASDAQ_RASH:
      strcpy(fileName, FILE_NAME_OF_NASDAQ_RASH_REJECT_CODE);
      break;
    case TYPE_NYSE_CCG:
      strcpy(fileName, FILE_NAME_OF_NYSE_CCG_REJECT_CODE);
      break;
    case TYPE_BZX_BOE:
      strcpy(fileName, FILE_NAME_OF_BATSZ_BOE_REJECT_CODE);
      break;
    case TYPE_EDGX_DIRECT:
      strcpy(fileName, FILE_NAME_OF_EDGX_DIRECT_REJECT_CODE);
      break;
    case TYPE_EDGA_DIRECT:
      strcpy(fileName, FILE_NAME_OF_EDGA_DIRECT_REJECT_CODE);
      break;
    case TYPE_NASDAQ_OUBX:
      strcpy(fileName, FILE_NAME_OF_OUBX_DIRECT_REJECT_CODE);
      break;
    case TYPE_BYX_BOE:
      strcpy(fileName, FILE_NAME_OF_BYX_BOE_REJECT_CODE);
      break;
    case TYPE_PSX:
      strcpy(fileName, FILE_NAME_OF_PSX_REJECT_CODE);
      break;
    default:
      TraceLog(ERROR_LEVEL, "(%s) Invalid ECNType = %d\n", __func__, ECNType);
      return ERROR;
    break;
  }

  if (GetFullConfigPath(fileName, CONF_ORDER, fullPath) == NULL_POINTER)
  {
    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SaveRejectCodeToFileOfAOrder
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int SaveRejectCodeToFileOfAOrder(int ECNType)
{
  char fullPath[MAX_PATH_LEN];

  if(GetRejectCodeFilePath(ECNType, fullPath))
  {
    TraceLog(ERROR_LEVEL, "(SaveRejectCodeToFileOfAOrder) Have some problem in get %s full path\n", fullPath);
    return ERROR;
  }

  FILE *fileDesc = fopen(fullPath, "w+");
  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "(SaveRejectCodeToFileOfAOrder) Cannot open file '%s' to read or we don't have permission to create\n", fullPath);

    return ERROR;
  }

  int i;

  char buffer[REJECT_TEXT_LEN + 20];

  int ECNIndex = ECNType - TYPE_BASE;

  t_DisableRejectMgmt *reject = &DisableRejectMgmt[ECNIndex];

  for(i = 0; i < reject->countReason; i++)
  {
    sprintf(buffer, "%d,%s,%s\n", reject->disableAlert[i], reject->reasonCode[i], reject->reasonText[i]);
    fputs(buffer, fileDesc);
  }

  fclose(fileDesc);

  return ERROR;
}

/****************************************************************************
- Function name:  LoadRejectCodeFromFileOfAllOrders
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int LoadRejectCodeFromFileOfAllOrders(void)
{
  int i;
  for(i = 0; i < MAX_ORDER_TYPE; i++)
  {
    LoadRejectCodeFromFileOfAOrder(i + TYPE_BASE);
  }
  return SUCCESS;
}

/****************************************************************************
}
- Function name:  LoadRejectCodeFromFileOfAOrder
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int LoadRejectCodeFromFileOfAOrder(int ECNType)
{
  char fullPath[MAX_PATH_LEN];

  if(GetRejectCodeFilePath(ECNType, fullPath))
  {
    TraceLog(ERROR_LEVEL, "(LoadRejectCodeFromFileOfAOrder) Have some problem in get %s full path\n", fullPath);
    return ERROR;
  }

  FILE *fileDesc = fopen(fullPath, "r");
  if (fileDesc == NULL)
  {
    //TraceLog(ERROR_LEVEL, "(LoadRejectCodeFromFileOfAOrder) Cannot open file '%s' to read or we don't have permission to create\n", fullPath);
    return SUCCESS;
  }

  //Buffer to store each line in the configuration file
  char line[1024];

  //Clear the buffer pointed to by line
  memset(line, 0, 1024);

  int ECNIndex = ECNType - TYPE_BASE;

  t_DisableRejectMgmt *reject = &DisableRejectMgmt[ECNIndex];

  // Initialize data ...
  memset(reject, 0, sizeof(t_DisableRejectMgmt));

  int rejectIndex = 0;
  t_Parameters parameterList;

  //Get each line from the configuration file
  while (fgets(line, 1023, fileDesc) != NULL)
  {
    Lrc_Split(line, ',', &parameterList);

    if(parameterList.countParameters != 3)
    {
      TraceLog(ERROR_LEVEL, "(LoadRejectCodeFromFileOfAOrder) Invalid line = %s, file %s\n", line, fullPath);

      //Close the file
      fclose(fileDesc);

      return ERROR;
    }
    reject->disableAlert[rejectIndex] = atoi(parameterList.parameterList[0]);

    strcpy(reject->reasonCode[rejectIndex], parameterList.parameterList[1]);

    strcpy(reject->reasonText[rejectIndex], parameterList.parameterList[2]);

    rejectIndex++;

    //Clear the buffer pointed to by line
    memset(line, 0, MAX_PATH_LEN);
  }

  reject->countReason = rejectIndex;

  //Close the file
  fclose(fileDesc);

  return SUCCESS;

}

/****************************************************************************
- Function name:  GetCurrentAllVolatileToggleValue
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:    
****************************************************************************/
int GetCurrentAllVolatileToggleValue()
{
  time_t now = (time_t)hbitime_seconds();
  
  struct tm local;
  localtime_r(&now, &local);
  
  int timeInSecs = local.tm_hour * 3600 + local.tm_min * 60 + local.tm_sec;
  
  int i;
  for (i=0; i<volatileMgmt.numAllVolatileToggle; i++)
  {
    if ((timeInSecs >= volatileMgmt.allVolatileOnTime[i]) &&
      (timeInSecs < volatileMgmt.allVolatileOffTime[i]))
    {
        return 1;
    }
  }
  
  return 0;
}

/****************************************************************************
- Function name:  GetECNLaunchInfo
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:    
****************************************************************************/
int GetECNLaunchInfo(int launchType, char *ecnLaunch, char *askBid)
{
  // Get ECN launch and Ask/Bid launch
  switch (launchType)
  {
    case ARCA_ASK:
      strcpy(ecnLaunch, "ARCA");
      strcpy(askBid, "ASK");
      break;

    case ARCA_BID:
      strcpy(ecnLaunch, "ARCA");
      strcpy(askBid, "BID");
      break;

    case NDAQ_ASK:
      strcpy(ecnLaunch, "NDAQ");
      strcpy(askBid, "ASK");
      break;

    case NDAQ_BID:
      strcpy(ecnLaunch, "NDAQ");
      strcpy(askBid, "BID");
      break;

    case BATSZ_ASK:
      strcpy(ecnLaunch, "BATZ");
      strcpy(askBid, "ASK");
      break;

    case BATSZ_BID:
      strcpy(ecnLaunch, "BATZ");
      strcpy(askBid, "BID");
      break;

    case NYSE_ASK:
      strcpy(ecnLaunch, "NYSE");
      strcpy(askBid, "ASK");
      break;

    case NYSE_BID:
      strcpy(ecnLaunch, "NYSE");
      strcpy(askBid, "BID");
      break;
        
    case EDGX_ASK:
      strcpy(ecnLaunch, "EDGX");
      strcpy(askBid, "ASK");
      break;

    case EDGX_BID:
      strcpy(ecnLaunch, "EDGX");
      strcpy(askBid, "BID");
      break;
      
    case EDGA_ASK:
      strcpy(ecnLaunch, "EDGA");
      strcpy(askBid, "ASK");
      break;

    case EDGA_BID:
      strcpy(ecnLaunch, "EDGA");
      strcpy(askBid, "BID");
      break;
      
    case NDBX_ASK:
      strcpy(ecnLaunch, "NDBX");
      strcpy(askBid, "ASK");
      break;

    case NDBX_BID:
      strcpy(ecnLaunch, "NDBX");
      strcpy(askBid, "BID");
      break;
     
    case BYX_ASK:
      strcpy(ecnLaunch, "BATY");
      strcpy(askBid, "ASK");
      break;

    case BYX_BID:
      strcpy(ecnLaunch, "BATY");
      strcpy(askBid, "BID");
      break;
      
    case PSX_ASK:
      strcpy(ecnLaunch, "PSX");
      strcpy(askBid, "ASK");
      break;

    case PSX_BID:
      strcpy(ecnLaunch, "PSX");
      strcpy(askBid, "BID");
      break;
      
    case AMEX_ASK:
      strcpy(ecnLaunch, "AMEX");
      strcpy(askBid, "ASK");
      break;

    case AMEX_BID:
      strcpy(ecnLaunch, "AMEX");
      strcpy(askBid, "BID");
      break;

    default:
      TraceLog(ERROR_LEVEL, "(%s): INVALID ECN ASK/BID LAUCH (%d)\n", __func__, launchType);
      strcpy(ecnLaunch, " ");
      strcpy(askBid, " ");
      
      return ERROR;
  }
      
  return SUCCESS;
}

/****************************************************************************
- Function name:  GetAccountName
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int GetAccountName(int venueType, int accountIndex, char *account)
{
  if (accountIndex < 0 || accountIndex > MAX_ACCOUNT)
  {
    TraceLog(DEBUG_LEVEL, "(%s): Invalid account index (%d), ECNId: %d\n", __func__, accountIndex, venueType - TYPE_BASE);
    strcpy(account, "0");
    return ERROR;
  }
  
  switch(venueType)
  {
    case TYPE_ARCA_DIRECT:
      strcpy(account, TradingAccount.DIRECT[accountIndex]);
      break;
    case TYPE_NASDAQ_OUCH:
      strcpy(account, TradingAccount.OUCH[accountIndex]);
      break;
    case TYPE_NASDAQ_RASH:
      strcpy(account, TradingAccount.RASH[accountIndex]);
      break;
    case TYPE_NYSE_CCG:
      strcpy(account, TradingAccount.CCG[accountIndex]);
      break;
    case TYPE_BZX_BOE:
      strcpy(account, TradingAccount.BATSZBOE[accountIndex]);
      break;
    case TYPE_EDGX_DIRECT:
      strcpy(account, TradingAccount.EDGX[accountIndex]);
      break;
    case TYPE_EDGA_DIRECT:
      strcpy(account, TradingAccount.EDGA[accountIndex]);
      break;
    case TYPE_NASDAQ_OUBX:
      strcpy(account, TradingAccount.OUBX[accountIndex]);
      break;
    case TYPE_BYX_BOE:
      strcpy(account, TradingAccount.BYXBOE[accountIndex]);
      break;
    case TYPE_PSX:
      strcpy(account, TradingAccount.PSX[accountIndex]);
      break;
      
    default:
      TraceLog(DEBUG_LEVEL, "(%s): Invalid venue type (%d)\n", __func__, venueType);
      return ERROR;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  UpdateColorForOpenPositions
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int UpdateColorForOpenPositions()
{
  int i, account, symbolIndex;
  
  // Reset volatile flag in RMList  
  int countSymbol = RMList.countSymbol;
  for (symbolIndex = 0; symbolIndex < countSymbol; symbolIndex++)
  {
    RMList.symbolList[symbolIndex].isVolatile = volatileMgmt.currentAllVolatileValue;
  }
  
  if (volatileMgmt.currentAllVolatileValue == 0) // Does not set volatile toggle
  {
    char activeList[MAX_STOCK_SYMBOL * SYMBOL_LEN];
    int countActive = GetActiveVolatileSymbols(activeList);
    
    for (i = 0; i < countActive; i++)
    {
      symbolIndex = GetStockSymbolIndex(&activeList[i * SYMBOL_LEN]);
      
      if (symbolIndex != -1)
      {
        RMList.symbolList[symbolIndex].isVolatile = 1;
      }
    }
  }
  
  countSymbol = RMList.countSymbol;
  for (symbolIndex = 0; symbolIndex < countSymbol; symbolIndex++)
  {
    for (account = 0; account < MAX_ACCOUNT; account++)
    {
      if (RMList.symbolList[symbolIndex].isVolatile == 1)
      {
        if (verifyPositionInfo[account][symbolIndex].isTrading != TRADING_NONE)
        {
          if (verifyPositionInfo[account][symbolIndex].isPMOwner == YES)
          {
            //RMList.collection[account][symbolIndex].colorFlag = POSITION_VOLATILE_PM_HANDLE;
            RMList.collection[account][symbolIndex].colorFlag = COM__RGM__DAEDALUS__PROTO__POSITION_STATE__PMVolatile;
          }
          else
          {
            //RMList.collection[account][symbolIndex].colorFlag = POSITION_VOLATILE_TRADER_HANDLE;
            RMList.collection[account][symbolIndex].colorFlag = COM__RGM__DAEDALUS__PROTO__POSITION_STATE__ManualVolatile;
          }
        }
        else
        {
          int isHalted = HaltStatusMgmt.haltStatus[symbolIndex][TS_ARCA_BOOK_INDEX] + HaltStatusMgmt.haltStatus[symbolIndex][TS_NASDAQ_BOOK_INDEX];
          if (RMList.collection[account][symbolIndex].leftShares == 0)
          {
            RMList.collection[account][symbolIndex].colorFlag = COM__RGM__DAEDALUS__PROTO__POSITION_STATE__Normal;
          }
          else if (isHalted == 2 * TRADING_HALTED)
          {
            RMList.collection[account][symbolIndex].colorFlag = COM__RGM__DAEDALUS__PROTO__POSITION_STATE__Halt;
          }
          else
          {
            //RMList.collection[account][symbolIndex].colorFlag = POSITION_VOLATILE_NORMAL;
            RMList.collection[account][symbolIndex].colorFlag = COM__RGM__DAEDALUS__PROTO__POSITION_STATE__Volatile;
          }
        }
      }
      else
      {
        if (verifyPositionInfo[account][symbolIndex].isTrading != TRADING_NONE)
        {
          if (verifyPositionInfo[account][symbolIndex].isPMOwner == YES)
          {
            //RMList.collection[account][symbolIndex].colorFlag = POSITION_LIVE_PM_HANDLE;
            RMList.collection[account][symbolIndex].colorFlag = COM__RGM__DAEDALUS__PROTO__POSITION_STATE__PMStuck;
          }
          else
          {
            //RMList.collection[account][symbolIndex].colorFlag = POSITION_LIVE_TRADER_HANDLE;
            RMList.collection[account][symbolIndex].colorFlag = COM__RGM__DAEDALUS__PROTO__POSITION_STATE__ManualStuck;
          }
        }
        else
        {
          int isHalted = HaltStatusMgmt.haltStatus[symbolIndex][TS_ARCA_BOOK_INDEX] + HaltStatusMgmt.haltStatus[symbolIndex][TS_NASDAQ_BOOK_INDEX];
          if (RMList.collection[account][symbolIndex].leftShares == 0)
          {
            RMList.collection[account][symbolIndex].colorFlag = COM__RGM__DAEDALUS__PROTO__POSITION_STATE__Normal;
          }
          else if (isHalted == 2 * TRADING_HALTED)
          {
            //RMList.collection[account][symbolIndex].colorFlag = POSITION_HALTED;
            RMList.collection[account][symbolIndex].colorFlag = COM__RGM__DAEDALUS__PROTO__POSITION_STATE__Halt;
          }
          else
          {
            //RMList.collection[account][symbolIndex].colorFlag = POSITION_NORMAL;
            RMList.collection[account][symbolIndex].colorFlag = COM__RGM__DAEDALUS__PROTO__POSITION_STATE__Stuck;
          }
        }
      }
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ReserveString
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
void ReserveString(char *string)
{
   int length, c;
   char *begin, *end, temp;

   length = strlen(string);

   begin = string;
   end = string;

   end += (length - 1);

   for ( c = 0 ; c < length/2 ; c++ )
   {
      temp = *end;
      *end = *begin;
      *begin = temp;

      begin++;
      end--;
   }
}

/****************************************************************************
- Function name:  ConvertDecimalToBase36ASCII
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
char *ConvertDecimalToBase36ASCII(long number, char *result)
{
  char *base36Chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  int decimal = 0, i = 0, nbase = 36;
  while (number >= nbase)
  {
    decimal = number % nbase;
    result[i++] = base36Chars[decimal];
    number /= nbase;
  }
  result[i] = base36Chars[number];
  return result;
}

/****************************************************************************
- Function name:  GetOECName
- Description:
- Usage:
****************************************************************************/
char *GetOECName(const int oecType)
{
  // IMPORTANT: OEC name must match in database
  switch (oecType)
  {
    case TYPE_ARCA_DIRECT:
      return "ARCA";
    case TYPE_NASDAQ_OUCH:
      return "OUCH";
    case TYPE_NASDAQ_OUBX:
      return "OUBX";
    case TYPE_PSX:
      return "PSX";
    case TYPE_NASDAQ_RASH:
      return "RASH";
    case TYPE_NYSE_CCG:
      return "NYSE";
    case TYPE_BZX_BOE:
      return "BATZ";
    case TYPE_BYX_BOE:
      return "BATY";
    case TYPE_EDGX_DIRECT:
      return "EDGX";
    case TYPE_EDGA_DIRECT:
      return "EDGA";
    default:
      return "NAN";
  }
}
