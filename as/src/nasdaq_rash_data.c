/*****************************************************************************
**  Project:    Equity Arbitrage Application
**  Filename:   nasdaq_rash_data.c
**  Description:  This file contains function definitions that were declared
          in nasdaq_rash_data.h 
**  Author:     Luan Vo-Kinh
**  First created:  13-Sep-2007
**  Last updated: -----------
*****************************************************************************/

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#define _GNU_SOURCE

#include "configuration.h"
#include "order_mgmt_proc.h"
#include "arca_direct_data.h"
#include "nasdaq_ouch_data.h"
#include "nasdaq_rash_data.h"
#include "bats_boe_data.h"
#include "database_util.h"
#include "query_data_mgmt.h"
#include "raw_data_mgmt.h"
#include "daedalus_proc.h"
#include "trader_tools_proc.h"
#include "trade_servers_proc.h"
#include "hbitime.h"

/****************************************************************************
** Global variables definition
****************************************************************************/

/****************************************************************************
** Function declarations
****************************************************************************/

/****************************************************************************
- Function name:  ParseRawDataForRASHOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ParseRawDataForRASHOrder( t_DataBlock dataBlock, int type )
{
  //Get timestamp from epoch with format <second from epoch>.<microsecond> (eg. 124212553634.3235235)
  char timeStamp[32];
  long usec;
  
  if( dataBlock.msgContent[0] == 'S' )
  {
    usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]) / 1000;
    sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
        
    switch ( dataBlock.msgContent[9] )
    {
      // Accepted order: 
      // A = Accepted Order Message
      case 'A':
      // R = Accepted Order Message with Cross Functionality
      case 'R':
        //TraceLog(DEBUG_LEVEL, "RASH Order: Process accepted order\n");

        ProcessAcceptedOrderOfRASHOrder( dataBlock, type, timeStamp );
        break;
      
      // Canceled order
      case 'C':
        //TraceLog(DEBUG_LEVEL, "RASH Order: Process canceled order\n");
        
        ProcessCanceledOrderOfRASHOrder( dataBlock, type, timeStamp );
        break;
      
      // Executed order
      case 'E':
        //TraceLog(DEBUG_LEVEL, "RASH Order: Process executed order\n");
        
        ProcessExecutedOrderOfRASHOrder( dataBlock, type, timeStamp );
        break;

      // Rejected order
      case 'J':
        //TraceLog(DEBUG_LEVEL, "RASH Order: Process rejected order\n");
        
        ProcessRejectedOrderOfRASHOrder( dataBlock, type, timeStamp );
        break;

      // Broken trade
      case 'B':
        //TraceLog(DEBUG_LEVEL, "RASH Order: Process broken trade \n");
        
        ProcessBrokenOrderOfRASHOrder( dataBlock, type );
        break;

      default:
        TraceLog(ERROR_LEVEL, "RASH Order: Invalid order message, msgType = %c ASCII = %d\n", dataBlock.msgContent[9], dataBlock.msgContent[9] );
        break;
    }
  }
  else if( dataBlock.msgContent[0] == 'U' )
  {
    usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]) / 1000;
    sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
    
    // New Order
    if ((dataBlock.msgContent[1] == 'O') || (dataBlock.msgContent[1] == 'Q'))
    {
      //TraceLog(DEBUG_LEVEL, "RASH Order: Process new order\n");
      ProcessNewOrderOfRASHOrder( dataBlock, type, timeStamp );
    }
    // Cancel Request
    else if( dataBlock.msgContent[1] == 'X' )
    {
      //TraceLog(DEBUG_LEVEL, "RASH Order: Process cancel request\n");  
      ProcessCancelRequestOfRASHOrder( dataBlock, type, timeStamp );
    }
    else
      TraceLog(ERROR_LEVEL, "RASH Order: Invalid order message, msgType = %c ASCII = %d\n", dataBlock.msgContent[1], dataBlock.msgContent[1] );
  }
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessNewOrderOfRASHOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessNewOrderOfRASHOrder( t_DataBlock dataBlock, int type, char *timeStamp )
{
  t_RASHNewOrder newOrder;
  t_OpenOrderInfo openOrder;
  t_OrderDetail orderDetail;

  /*------------------------------------------------
  Order summary:
  - double price;
  - double avgPrice;
  - char symbol[SYMBOL_LEN];
  - int side;
  - int shares;
  - int leftShares; 
  - int ECNId;
  - int clOrdId;
  - int status;
  - int tif;
  - int tsId;
  - char timestamp[MAX_TIMESTAMP];
  ------------------------------------------------*/

  /*------------------------------------------------
  Order detail:
  - int length;
  - int execId;
  - int isBroken;
  - char timestamp[MAX_TIMESTAMP];
  - char msgContent[MAX_ORDER_MSG];
  ------------------------------------------------*/
  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;

  // YYYY/mm/DD
  char dateTime[32];
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]), &dateTime[0]);
  memcpy(newOrder.date, &dateTime[0], 10);
  newOrder.date[10] = 0;

  // HH:MM:SS.ms
  memcpy( newOrder.timestamp, &dateTime[11], TIME_LENGTH );
  newOrder.timestamp[TIME_LENGTH] = 0;

  // openOrder.timestamp
  memcpy( openOrder.timestamp, &dateTime[0], TIMESTAMP_LENGTH );
  openOrder.timestamp[TIMESTAMP_LENGTH] = 0;

  // orderDetail
  strcpy( orderDetail.timestamp, openOrder.timestamp );
  // "ARCA", "OUCH", "RASH" or "BATZ"
  strcpy( newOrder.ecn, "RASH" );

  // openOrder.ECNId
  openOrder.ECNId = TYPE_NASDAQ_RASH;
    
  // Status: Sent, Live, Rejected, Closed, BrokenTrade, CanceledByECN, 
  //      CXLSent, CancelACK, CancelRejected, CancelPending, CanceledByUser
  strcpy( newOrder.status, orderStatus.Sent );
  // openOrder.status
  openOrder.status = orderStatusIndex.Sent;
  
  // Messsage type
  strcpy( newOrder.msgType, "NewOrder" );

  // 
  char msgType = dataBlock.msgContent[1];

  // Order ID
  char fieldToken[16]; //Remove first two digits account
  memcpy( fieldToken, &dataBlock.msgContent[4], 12 );
  fieldToken[12] = 0;
  newOrder.orderID = atoi( fieldToken );

  // openOrder.clOrdId
  openOrder.clOrdId = newOrder.orderID;
  
  // "B", "S" or "SS"
  char action = dataBlock.msgContent[16]; 
  char buysell = '0';
  switch ( action )
  {
    case 'B':
      buysell = 'B';
      strcpy( newOrder.side, "BC" );
      openOrder.side = BUY_TO_CLOSE_TYPE;
      break;
    
    case 'S':
      buysell = 'S';
      strcpy( newOrder.side, "S" );     
      openOrder.side = SELL_TYPE;
      break;
    
    case 'T':
      buysell = 'T';
      strcpy( newOrder.side, "SS" );      
      openOrder.side = SHORT_SELL_TYPE;
      break;
    
    default:
      strcpy( newOrder.side, " " );
      openOrder.side = -1;

      TraceLog(ERROR_LEVEL, "RASH Order: not support side = %c, %d\n", action, action );
      break;
  }
  
  // Shares
  char fieldShares[7];
  memcpy( fieldShares, &dataBlock.msgContent[17], 6 );
  fieldShares[6] = 0;
  newOrder.shares = atoi( fieldShares );

  //openOrder.shares
  openOrder.shares = newOrder.shares;
  openOrder.leftShares = openOrder.shares;
  
  // Stock symbol
  strncpy(newOrder.symbol, (char *) &dataBlock.msgContent[23], SYMBOL_LEN);
  TrimRight( newOrder.symbol, strnlen(newOrder.symbol, SYMBOL_LEN) );
  
  // openOrder.symbol
  memcpy(openOrder.symbol, newOrder.symbol, SYMBOL_LEN);
  
  //openOrder.account = dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID];
  
  // Price
  char fieldPrice[11];
  memcpy( fieldPrice, &dataBlock.msgContent[31], 10 );
  fieldPrice[10] = 0;
  newOrder.price = atof( fieldPrice ) / 10000;

  //openOrder.price
  openOrder.price = newOrder.price;
  openOrder.avgPrice = openOrder.price;
  
  //Time In Force
  char tif[6];
  memcpy(tif, &dataBlock.msgContent[41], 5);
  tif[5] = 0;
  
  int tifValue = atoi(tif);
  char _tif[6];
  if(tifValue == 0) 
  {
    strcpy(_tif, "0");
    strcpy( newOrder.timeInForce, "IOC");
    openOrder.tif = IOC_TYPE;
  }
  else if(tifValue > 0) 
  {
    strcpy(_tif, "99999");
    strcpy( newOrder.timeInForce, "DAY");
    openOrder.tif = DAY_TYPE;
  }
  else
  {
    TraceLog(ERROR_LEVEL, "RASH Error: TimeInForce = %s\n", tif );
    openOrder.tif = -1;
  }
  
  // Firm
  memcpy(newOrder.firm, &dataBlock.msgContent[46], 4);
  newOrder.firm[4] = 0; 

  newOrder.maxFloor[6] = 0;
  
  // Display
  newOrder.display = dataBlock.msgContent[50];
  char _visibility = newOrder.display;
  
  // fieldIndexList.MinQty;
  char fieldMinQty[7];
  memcpy( fieldMinQty, &dataBlock.msgContent[51], 6 );
  fieldMinQty[6] = 0;
  newOrder.minQty = atoi( fieldMinQty );
  // %d=%d\001
  // fieldIndexList.MinQty, newOrder.minQty, 

  // Max Floor
  memcpy( newOrder.maxFloor, &dataBlock.msgContent[57], 6 );
  // %d=%d\001
  // fieldIndexList.MaxFloor,  atoi( newOrder.maxFloor ), 

  // fieldIndexList.PegType;
  newOrder.pegType = dataBlock.msgContent[63];
  // %d=%c\001
  // fieldIndexList.PegType, newOrder.pegType, 

  // fieldIndexList.PegDifferenceSign;
  newOrder.pegDifferenceSign = dataBlock.msgContent[64];
  // %d=%c\001
  // fieldIndexList.PegDifferenceSign, newOrder.pegDifferenceSign, 

  // fieldIndexList.PegDifference;
  char fieldPegDifference[11];
  memcpy( fieldPegDifference, &dataBlock.msgContent[65], 10 );
  fieldPegDifference[10] = 0;
  newOrder.pegDifference = atof( fieldPegDifference ) / 10000;
  // %d=%lf\001
  // fieldIndexList.PegDifference, newOrder.pegDifference, 

  // fieldIndexList.DiscretionPrice;
  char fieldDiscretionPrice[11];
  memcpy( fieldDiscretionPrice, &dataBlock.msgContent[75], 10 );
  fieldDiscretionPrice[10] = 0;
  newOrder.discretionPrice = atof( fieldDiscretionPrice ) / 10000;
  // %d=%lf\001
  // fieldIndexList.DiscretionPrice, newOrder.discretionPrice, 

  // fieldIndexList.DiscretionPegType;
  newOrder.discretionPegType = dataBlock.msgContent[85];
  // %d=%c\001
  // fieldIndexList.DiscretionPegType, newOrder.discretionPegType, 

  // fieldIndexList.DiscretionPegDifferenceSign;
  newOrder.discretionPegDifferenceSign = dataBlock.msgContent[86];
  // %d=%c\001
  // fieldIndexList.DiscretionPegDifferenceSign, newOrder.discretionPegDifferenceSign, 

  // fieldIndexList.DiscretionPegDifference;
  char fieldDiscretionPegDifference[11];
  memcpy( fieldDiscretionPegDifference, &dataBlock.msgContent[87], 10 );
  fieldDiscretionPegDifference[10] = 0;
  newOrder.discretionPegDifference = atof( fieldDiscretionPegDifference ) / 10000;
  // %d=%lf\001
  // fieldIndexList.DiscretionPegDifference, newOrder.discretionPegDifference, 

  // fieldIndexList.Rule80AIndicator;
  newOrder.rule80A = dataBlock.msgContent[97];
  // %d=%c\001
  // fieldIndexList.Rule80AIndicator, newOrder.rule80A, 

  //fieldIndexList.RandomReserve;
  char fieldRandomReserve[7];
  memcpy( fieldRandomReserve, &dataBlock.msgContent[98], 6 );
  fieldRandomReserve[6] = 0;
  newOrder.randomReserve = atoi( fieldRandomReserve );
  // %d=%d\001
  // fieldIndexList.RandomReserve, newOrder.randomReserve, 

  // fieldIndexList.ExecBroker;
  memcpy( newOrder.execBroker, &dataBlock.msgContent[104], 4 );
  newOrder.execBroker[4] = 0;
  // %d=%s\001
  // fieldIndexList.ExecBroker, newOrder.execBroker, 

  // fieldIndexList.SenderSubID;
  memcpy( newOrder.senderSubID, &dataBlock.msgContent[108], 32 );
  newOrder.senderSubID[32] = 0;
  
  // Cross type
  char crossType = 32;
  
  newOrder.eligibility = 'N';
  if (dataBlock.msgContent[1] == 'Q')
  {
    newOrder.eligibility = dataBlock.msgContent[140];
    crossType = dataBlock.msgContent[141];
  }
  
  int isISO = 0;
  short bboID = -1;
  unsigned char bbReason;
  if (newOrder.eligibility == 'Y')
  {
    isISO = 1;
    bboID = GetShortNumber(&dataBlock.addContent[DATABLOCK_OFFSET_BBO_ID]);
  }
  
  newOrder.bboId = bboID;
  
  // %d=%s\001
  // fieldIndexList.SenderSubID, newOrder.senderSubID, 
  /***********************************************************/
  
  // Sequence Number
  newOrder.seqNum = 0;
  
  //orderDetail.msgContent
  orderDetail.length = sprintf( orderDetail.msgContent, "%d=%c\001%d=%d\001%d=%c\001%d=%d\001%d=%.8s\001%d=%lf\001%d=%s\001%d=%s\001%d=%c\001%d=%d\001%d=%d\001%d=%c\001%d=%c\001%d=%lf\001%d=%lf\001%d=%c\001%d=%c\001%d=%lf\001%d=%c\001%d=%d\001%d=%s\001%d=%s\001%d=%c\001%d=%c\001", 
          fieldIndexList.MessageType, msgType, 
          fieldIndexList.Token, newOrder.orderID, 
          fieldIndexList.Action, action, 
          fieldIndexList.Shares, newOrder.shares, 
          fieldIndexList.Stock, newOrder.symbol, 
          fieldIndexList.Price, newOrder.price, 
          fieldIndexList.TimeInForce, tif,
          fieldIndexList.Firm, newOrder.firm, 
          fieldIndexList.Display, newOrder.display, 
          fieldIndexList.MinQty, newOrder.minQty, 
          fieldIndexList.MaxFloor, atoi( newOrder.maxFloor ), 
          fieldIndexList.PegType, newOrder.pegType, 
          fieldIndexList.PegDifferenceSign, newOrder.pegDifferenceSign, 
          fieldIndexList.PegDifference, newOrder.pegDifference, 
          fieldIndexList.DiscretionPrice, newOrder.discretionPrice, 
          fieldIndexList.DiscretionPegType, newOrder.discretionPegType, 
          fieldIndexList.DiscretionPegDifferenceSign, newOrder.discretionPegDifferenceSign, 
          fieldIndexList.DiscretionPegDifference, newOrder.discretionPegDifference, 
          fieldIndexList.Rule80AIndicator, newOrder.rule80A, 
          fieldIndexList.RandomReserve, newOrder.randomReserve, 
          fieldIndexList.ExecBroker, newOrder.execBroker, 
          fieldIndexList.SenderSubID, newOrder.senderSubID,
          fieldIndexList.Eligibility, newOrder.eligibility,
          fieldIndexList.CrossType, crossType);
  
  strcpy( newOrder.msgContent, orderDetail.msgContent );
  
  /*------------------------------------------------*/
  // Update database
  if( type == MANUAL_ORDER )
  {
    //openOrder.tsId
    openOrder.tsId = -1;

    //newOrder.tsId
    newOrder.tsId = -1;

    // Trade type
    newOrder.tradeType = NORMALLY_TRADE;
    openOrder.tradeType = NORMALLY_TRADE;

    strcpy(newOrder.entryExit, "Exit");     
    strcpy(newOrder.ecnLaunch, " ");
    strcpy(newOrder.askBid, " ");
    newOrder.visibleShares = 0;
    
    //Add CrossID if it belogs to PM
    if (newOrder.orderID >= 500000)
    {
      bbReason = OJ_BB_REASON_PM;
      newOrder.crossId = GetIntNumber(&dataBlock.addContent[DATABLOCK_OFFSET_CROSS_ID]);
      openOrder.crossId = newOrder.crossId;
      
      if (strlen(pmOrderInfo[newOrder.orderID % MAX_ORDER_PLACEMENT].symbol) == 0)
      {
        strncpy(pmOrderInfo[newOrder.orderID % MAX_ORDER_PLACEMENT].symbol, newOrder.symbol, SYMBOL_LEN);
      }
    }
    else
    {
      bbReason = OJ_BB_REASON_AS;
      newOrder.crossId = 0;
      
      if (strlen(manualOrderInfo[newOrder.orderID].symbol) == 0)
      {
        strncpy(manualOrderInfo[newOrder.orderID].symbol, newOrder.symbol, SYMBOL_LEN);
      }
    }
  }
  else
  {
    if (dataBlock.addContent[DATABLOCK_OFFSET_LAUNCH] == EXIT_VALUE)
    {
      // Get Entry/Exit
      bbReason = OJ_BB_REASON_EXIT;
      strcpy(newOrder.entryExit, "Exit");
      
      strcpy(newOrder.ecnLaunch, " ");
      strcpy(newOrder.askBid, " ");
      
      newOrder.entryExitRefNum = *(unsigned short *)&dataBlock.addContent[DATABLOCK_OFFSET_ENTRY_EXIT_REF];
    }
    else
    {
      // Get Entry/Exit
      bbReason = OJ_BB_REASON_ENTRY;
      strcpy(newOrder.entryExit, "Entry");

      // Get ECN launch and Ask/Bid launch
      GetECNLaunchInfo(dataBlock.addContent[DATABLOCK_OFFSET_LAUNCH], newOrder.ecnLaunch, newOrder.askBid);
    }

    // Visible shares
    newOrder.visibleShares = GetIntNumber(&dataBlock.addContent[DATABLOCK_OFFSET_VISIBLE_SIZE]);

    //Cross Id
    newOrder.crossId = GetIntNumber(&dataBlock.addContent[DATABLOCK_OFFSET_CROSS_ID]);
    openOrder.crossId = newOrder.crossId;

    // Trade type
    newOrder.tradeType = NORMALLY_TRADE;
    openOrder.tradeType = newOrder.tradeType;

    //openOrder.tsId
    openOrder.tsId = tradeServersInfo.config[type].tsId;

    //newOrder.tsId
    newOrder.tsId = type;
    
    // Reset no trade duration
    time_t t = (time_t)hbitime_seconds();
    struct tm tm = *localtime(&t);

    NoTradesDurationMgt[type].currentNoTradesDurationInSecond = 0;
    NoTradesDurationMgt[type].alertThresholdInMinute = TradeTimeAlertConfig[3600 * tm.tm_hour + 60 * tm.tm_min + tm.tm_sec];

    sprintf(NoTradesDurationMgt[type].startTimeStamp, "%d:%d", tm.tm_hour, tm.tm_min);
  }

  if (newOrder.orderID < 500000)  //Manual order
  {
    if (Manual_Order_Account_Mapping[newOrder.orderID] >= 0)
    {
      dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID] = Manual_Order_Account_Mapping[newOrder.orderID];
    }
    else
    {
      Manual_Order_Account_Mapping[newOrder.orderID] = dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID];
    }
  }
  
  newOrder.tradingAccount = dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID];
  openOrder.account = dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID];
  
  // Call function to add a open order
  ProcessAddASOpenOrder( &openOrder, &orderDetail );
  
  // Call function to update New Order of RASH Order in Manual Order
  ProcessInsertNewOrder4RASHQueryToCollection(&newOrder);

  //Add a new record to OJ file
  int isDuplicated = 0;
  int formatedOrderId;
  memcpy( fieldToken, &dataBlock.msgContent[4], 12);
  fieldToken[12] = 0;
  formatedOrderId = atoi( fieldToken );
  
  if (AddOrderIdToList(&__new, formatedOrderId) == ERROR)
  {
    isDuplicated = 1;
  }
  
  char account[32];
  GetAccountName(TYPE_NASDAQ_RASH, dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID], account);

  if (isDuplicated == 0)
  {
    AddOJNewOrderRecord(timeStamp, newOrder.symbol, VENUE_RASH, buysell, newOrder.price, newOrder.shares, newOrder.orderID, _tif, _visibility, isISO, account, newOrder.execBroker, newOrder.rule80A, newOrder.crossId, bbReason);
  }
  
  //Add ISO Order to ISO Collection for ISO Flight File creation
  if ((isISO == 1) && (isDuplicated == 0))
  {
    t_ISOOrderInfo isoOrderInfo;
    
    strcpy(isoOrderInfo.strTime, timeStamp);
    
    sprintf(isoOrderInfo.strVenueID, "%d", VENUE_RASH);
    
    isoOrderInfo.strBuysell[0] = buysell;
    isoOrderInfo.strBuysell[1] = 0;
    
    sprintf(isoOrderInfo.strPrice, "%.2lf", newOrder.price);
    
    sprintf(isoOrderInfo.strShares, "%d", newOrder.shares);
    
    sprintf(isoOrderInfo.strSequenceID, "%d", newOrder.orderID);
    
    isoOrderInfo.strCapacity[0] = newOrder.rule80A;
    isoOrderInfo.strCapacity[1] = 0;
    
    strcpy(isoOrderInfo.strAccount, account);
    
    strcpy(isoOrderInfo.strRoutingInfo, newOrder.execBroker);
    
    AddISOOrderToCollection(newOrder.crossId, bboID, newOrder.symbol, &isoOrderInfo);
  }
  
  CheckAndProcessRawdataInWaitListForOrderID(newOrder.orderID);
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessAcceptedOrderOfRASHOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessAcceptedOrderOfRASHOrder( t_DataBlock dataBlock, int type, char *timeStamp )
{
  t_RASHAcceptedOrder acceptedOrder;
  t_OpenOrderInfo openOrder;
  t_OrderDetail orderDetail;
  
  /*------------------------------------------------
  Order summary:
  - double price;
  - double avgPrice;
  - char symbol[SYMBOL_LEN];
  - int side;
  - int shares;
  - int leftShares; 
  - int ECNId;
  - int clOrdId;
  - int status;
  - int tif;
  - int tsId;
  - char timestamp[MAX_TIMESTAMP];
  ------------------------------------------------*/

  /*------------------------------------------------
  Order detail:
  - int length;
  - int execId;
  - int isBroken;
  - char timestamp[MAX_TIMESTAMP];
  - char msgContent[MAX_ORDER_MSG];
  ------------------------------------------------*/
  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;
  
  // YYYY/mm/DD
  char dateTime[32];
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]), &dateTime[0]);
  memcpy(acceptedOrder.processedTimestamp, &dateTime[11], TIME_LENGTH);
  acceptedOrder.processedTimestamp[TIME_LENGTH] = 0;
  
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]), &dateTime[0]);
  memcpy(acceptedOrder.date, &dateTime[0], 10);
  acceptedOrder.date[10] = 0;
  
  // HH:MM:SS.ms
  memcpy(acceptedOrder.timestamp, &dateTime[11], TIME_LENGTH);
  acceptedOrder.timestamp[TIME_LENGTH] = 0;

  //openOrder.timestamp
  memcpy(openOrder.timestamp, &dateTime[0], TIMESTAMP_LENGTH);
  openOrder.timestamp[TIMESTAMP_LENGTH] = 0;

  strcpy(orderDetail.timestamp, openOrder.timestamp);

  //openOrder.ECNId
  openOrder.ECNId = TYPE_NASDAQ_RASH;

  // "ARCA", "OUCH", "RASH" or "BATZ"
  strcpy( acceptedOrder.ecn, "RASH" );

  // tsId
  acceptedOrder.tsId = type;
  
  // Status: Sent, Live, Rejected, Closed, BrokenTrade, CanceledByECN, 
  //      CXLSent, CancelACK, CancelRejected, CancelPending, CanceledByUser
  strcpy( acceptedOrder.status, orderStatus.Live );
  
  //openOrder.status
  openOrder.status = orderStatusIndex.Live;
  // Messsage type
  strcpy(acceptedOrder.msgType, "AcceptedOrder");

  // Sending Time
  memcpy( acceptedOrder.transTime, &dataBlock.msgContent[1], 8 );
  acceptedOrder.transTime[8] = 0;

  //MsgType
  char msgType = dataBlock.msgContent[9];

  // Order ID (strip first two number of account)
  char fieldToken[16];
  memcpy(fieldToken, &dataBlock.msgContent[12], 12);
  fieldToken[12] = 0;
  acceptedOrder.orderID = atoi( fieldToken ); 
  openOrder.clOrdId = acceptedOrder.orderID;
  
  char exOrderId[32];
  exOrderId[0] = dataBlock.msgContent[10];
  exOrderId[1] = dataBlock.msgContent[11];
  sprintf(&exOrderId[2], "%d", openOrder.clOrdId);
  
  
  if (acceptedOrder.orderID < 500000) //AS order
  {
    if (Manual_Order_Account_Mapping[acceptedOrder.orderID] >= 0)
    {
      dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID] = Manual_Order_Account_Mapping[acceptedOrder.orderID];
    }
    else
    {
      Manual_Order_Account_Mapping[acceptedOrder.orderID] = dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID];
    }
  }
  
  // Action: "B", "S" or "SS"
  char action = dataBlock.msgContent[24]; 
  switch ( action )
  {
    case 'B':
      strcpy( acceptedOrder.side, "BC" );
      openOrder.side = BUY_TO_CLOSE_TYPE;
      break;
    
    case 'S':
      strcpy( acceptedOrder.side, "S" );      
      openOrder.side = SELL_TYPE;
      break;
    
    case 'T':
      strcpy( acceptedOrder.side, "SS" );     
      openOrder.side = SHORT_SELL_TYPE;
      break;
    
    default:
      strcpy( acceptedOrder.side, " " );
      openOrder.side = -1;

      TraceLog(ERROR_LEVEL, "RASH Order: not support side = %c, %d\n", action, action );
      break;
  }

  // Shares
  char fieldShares[7];
  memcpy( fieldShares, &dataBlock.msgContent[25], 6 );
  fieldShares[6] = 0;
  acceptedOrder.shares = atoi( fieldShares );

  // openOrder.shares
  openOrder.shares = acceptedOrder.shares;
  openOrder.leftShares = openOrder.shares;

  // Stock
  strncpy(acceptedOrder.symbol, (char *) &dataBlock.msgContent[31], SYMBOL_LEN);
  TrimRight( acceptedOrder.symbol, strnlen(acceptedOrder.symbol, SYMBOL_LEN) ); //Convert all ' ' to null chars
  
  openOrder.account = dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID];
  
  // openOrder.symbol
  memcpy( openOrder.symbol, acceptedOrder.symbol, SYMBOL_LEN );

  // Price
  char fieldPrice[11];
  memcpy( fieldPrice, &dataBlock.msgContent[39], 10 );
  fieldPrice[10] = 0;
  acceptedOrder.price = atof( fieldPrice ) / 10000;

  // openOrder.price
  openOrder.price = acceptedOrder.price;
  openOrder.avgPrice = openOrder.price;
  // Time In Force
  char tif[6];
  memcpy(tif, &dataBlock.msgContent[49], 5);
  tif[5] = 0;

  int tifValue = atoi(tif);
  if(tifValue == 0) 
  {
    strcpy( acceptedOrder.timeInForce, "IOC");
    openOrder.tif = IOC_TYPE;
  }
  else if(tifValue > 0) 
  {
    strcpy( acceptedOrder.timeInForce, "DAY");
    openOrder.tif = DAY_TYPE;
  }
  else
  {
    TraceLog(ERROR_LEVEL, "RASH Error: TimeInForce = %s\n", tif );
    strcpy( acceptedOrder.timeInForce, " ");
    openOrder.tif = -1;
  }

  // Firm
  memcpy( acceptedOrder.firm, &dataBlock.msgContent[54], 4 );
  acceptedOrder.firm[4] = 0;

  // Display
  acceptedOrder.display = dataBlock.msgContent[58];
  
  //Order reference number
  char fieldOrdRef[10];
  memcpy( fieldOrdRef, &dataBlock.msgContent[59], 9 );
  fieldOrdRef[9] = 0;
  acceptedOrder.rashExOrderId = atoi( fieldOrdRef );
  
  int _serverId = (type == MANUAL_ORDER) ? MAX_TRADE_SERVER_CONNECTIONS : type;
  ExchangeOrderIdMapping[_serverId][acceptedOrder.orderID % MAX_ORDER_PLACEMENT] = acceptedOrder.rashExOrderId;
  
  /*********************************************************/
  //  Add code at Dec 08, 2007
  //  By luanvk
  //FieldIndex.MinQty;            //66 + 6: Numeric
  char fieldMinQty[7];
  memcpy(fieldMinQty, &dataBlock.msgContent[68], 6);
  fieldMinQty[6] = 0;
  int MinQty = atoi(fieldMinQty);
  //  %d=%d\001
  //  FieldIndex.MinQty, MinQty, 

  //FieldIndex.MaxFloor;            //72 + 6: Numeric
  char fieldMaxFloor[7];
  memcpy(fieldMaxFloor, &dataBlock.msgContent[74], 6);
  fieldMaxFloor[6] = 0;
  int MaxFloor = atoi(fieldMaxFloor);
  //  %d=%d\001
  //  FieldIndex.MaxFloor, MaxFloor, 

  //FieldIndex.PegType;           //78 + 1: Alpha
  char PegType = dataBlock.msgContent[80];
  //  %d=%c\001
  //  FieldIndex.PegType, PegType, 

  //FieldIndex.PegDifferenceSign;       //79 + 1: Alpha
  char PegDifferenceSign = dataBlock.msgContent[81];
  //  %d=%c\001
  //  FieldIndex.PegDifferenceSign, PegDifferenceSign, 

  //FieldIndex.PegDifference;         //80 + 10: Price
  char fieldPegDifference[11];
  memcpy(fieldPegDifference, &dataBlock.msgContent[82], 10);
  fieldPegDifference[10] = 0;
  double PegDifference = atof(fieldPegDifference) / 10000;
  //  %d=%lf\001
  //  FieldIndex.PegDifference, PegDifference, 

  //FieldIndex.DiscretionPrice;       //90 + 10: Price
  char fieldDiscretionPrice[11];
  memcpy(fieldDiscretionPrice, &dataBlock.msgContent[92], 10);
  fieldDiscretionPrice[10] = 0;
  double DiscretionPrice = atof(fieldDiscretionPrice) / 10000;
  //  %d=%lf\001
  //  FieldIndex.DiscretionPrice, DiscretionPrice, 

  //FieldIndex.DiscretionPegType;       //100 + 1: Alpha
  char DiscretionPegType = dataBlock.msgContent[102];
  //  %d=%c\001
  //  FieldIndex.DiscretionPegType, DiscretionPegType, 

  //FieldIndex.DiscretionPegDifferenceSign; //101 + 1: Alpha
  char DiscretionPegDifferenceSign = dataBlock.msgContent[103];
  //  %d=%c\001
  //  FieldIndex.DiscretionPegDifferenceSign, DiscretionPegDifferenceSign, 

  //FieldIndex.DiscretionPegDifference;   //102 + 10: Price
  char fieldDiscretionPegDifference[11];
  memcpy(fieldDiscretionPegDifference, &dataBlock.msgContent[104], 10);
  fieldDiscretionPegDifference[10] = 0;
  double DiscretionPegDifference = atof(fieldDiscretionPegDifference) / 10000;
  //  %d=%lf\001
  //  FieldIndex.DiscretionPegDifference, DiscretionPegDifference, 

  //FieldIndex.Rule80AIndicator;        //112 + 1: Alpha
  char Rule80AIndicator = dataBlock.msgContent[114];
  //  %d=%c\001
  //  FieldIndex.Rule80AIndicator, Rule80AIndicator, 

  //FieldIndex.RandomReserve;         //113 + 6: Numeric
  char fieldRandomReserve[7];
  memcpy(fieldRandomReserve, &dataBlock.msgContent[115], 6);
  fieldRandomReserve[6] = 0;
  int RandomReserve = atoi(fieldRandomReserve);
  //  %d=%d\001
  //  FieldIndex.RandomReserve, RandomReserve, 

  //FieldIndex.ExecBroker;          //119 + 4: Alpha
  char ExecBroker[5];
  memcpy(ExecBroker, &dataBlock.msgContent[121], 4);
  ExecBroker[4] = 0;
  //  %d=%s\001
  //  FieldIndex.ExecBroker, ExecBroker, 

  //FieldIndex.SenderSubID;         //123 + 32: Alpha-Numeric
  char SenderSubID[33];
  memcpy(SenderSubID, &dataBlock.msgContent[125], 32);
  SenderSubID[32] = 0;
  //  %d=%s\001
  //  FieldIndex.SenderSubID, SenderSubID, 
  /***********************************************************/
  
  // Sequence number
  acceptedOrder.seqNum = 0;
  
  acceptedOrder.tradingAccount = dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID];

  //orderDetail.msgContent
  orderDetail.length = sprintf( orderDetail.msgContent, "%d=%c\001%d=%s\001%d=%d\001%d=%c\001%d=%d\001%d=%.8s\001%d=%lf\001%d=%s\001%d=%s\001%d=%c\001%d=%d\001%d=%d\001%d=%d\001%d=%c\001%d=%c\001%d=%lf\001%d=%lf\001%d=%c\001%d=%c\001%d=%lf\001%d=%c\001%d=%d\001%d=%s\001%d=%s\001", 
                  fieldIndexList.MessageType, msgType, 
                  fieldIndexList.TimeStamp, acceptedOrder.transTime, 
                  fieldIndexList.Token, acceptedOrder.orderID, 
                  fieldIndexList.Action, action, 
                  fieldIndexList.Shares, acceptedOrder.shares, 
                  fieldIndexList.Stock, acceptedOrder.symbol, 
                  fieldIndexList.Price, acceptedOrder.price, 
                  fieldIndexList.TimeInForce, tif,
                  fieldIndexList.Firm, acceptedOrder.firm, 
                  fieldIndexList.Display, acceptedOrder.display, 
                  fieldIndexList.OrderRefNo, acceptedOrder.rashExOrderId, 
                  fieldIndexList.MinQty, MinQty, 
                  fieldIndexList.MaxFloor, MaxFloor, 
                  fieldIndexList.PegType, PegType, 
                  fieldIndexList.PegDifferenceSign, PegDifferenceSign, 
                  fieldIndexList.PegDifference, PegDifference, 
                  fieldIndexList.DiscretionPrice, DiscretionPrice, 
                  fieldIndexList.DiscretionPegType, DiscretionPegType, 
                  fieldIndexList.DiscretionPegDifferenceSign, DiscretionPegDifferenceSign, 
                  fieldIndexList.DiscretionPegDifference, DiscretionPegDifference, 
                  fieldIndexList.Rule80AIndicator, Rule80AIndicator, 
                  fieldIndexList.RandomReserve, RandomReserve, 
                  fieldIndexList.ExecBroker, ExecBroker, 
                  fieldIndexList.SenderSubID, SenderSubID);
  
  strcpy( acceptedOrder.msgContent, orderDetail.msgContent );

  // Update database
  if( type == MANUAL_ORDER )
  {
    // openOrder.tsId
    openOrder.tsId = -1;
  }
  else
  {
    // openOrder.tsId
    openOrder.tsId = tradeServersInfo.config[type].tsId;
  }

  // Call function to update a open order
  int indexOpenOrder = ProcessUpdateASOpenOrderForOrderACK( &openOrder, &orderDetail );
  if (indexOpenOrder == -2)
  {
    AddDataBlockToWaitList(TYPE_NASDAQ_RASH, WAIT_LIST_TYPE_RASH_ORDER_ACK, acceptedOrder.orderID, dataBlock, type, timeStamp);
    return 0;
  }

  // Call function to update Order ACK of RASH
  ProcessInsertAcceptedOrder4RASHQueryToCollection(&acceptedOrder);

  int formatedOrderId;
  memcpy( fieldToken, &dataBlock.msgContent[12], 12);
  fieldToken[12] = 0;
  formatedOrderId = atoi( fieldToken );
  
  if (AddOrderIdToList(&__ack, formatedOrderId) == ERROR) //Duplicated
  {
    return 0;
  }
  
  char ecnOrderID[32];
  sprintf(ecnOrderID, "%d", acceptedOrder.rashExOrderId);
  
  char account[32];
  GetAccountName(TYPE_NASDAQ_RASH, dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID], account);

  AddOJOrderAcceptedRecord(timeStamp, acceptedOrder.symbol, exOrderId, acceptedOrder.orderID, ecnOrderID, VENUE_RASH, account, Rule80AIndicator);
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessExecutedOrderOfRASHOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessExecutedOrderOfRASHOrder( t_DataBlock dataBlock, int type, char *timeStamp )
{
  t_RASHExecutedOrder executedOrder;
  t_OrderDetail orderDetail;

  /*------------------------------------------------
  Order detail:
  - int length;
  - int execId;
  - int isBroken;
  - char timestamp[MAX_TIMESTAMP];
  - char msgContent[MAX_ORDER_MSG];
  ------------------------------------------------*/
  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;

  // YYYY/mm/DD
  char dateTime[32];
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]), &dateTime[0]);
  memcpy( executedOrder.processedTimestamp, &dateTime[11], TIME_LENGTH );
  executedOrder.processedTimestamp[TIME_LENGTH] = 0;
  
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]), &dateTime[0]);
  memcpy(executedOrder.date, &dateTime[0], 10);
  executedOrder.date[10] = 0;
  
  // HH:MM:SS.ms
  memcpy( executedOrder.timestamp, &dateTime[11], TIME_LENGTH );
  executedOrder.timestamp[TIME_LENGTH] = 0;

  // orderDetail.timestamp
  memcpy(orderDetail.timestamp, &dateTime[0], TIMESTAMP_LENGTH);
  orderDetail.timestamp[TIMESTAMP_LENGTH] = 0;

  // Sending Time
  memcpy( executedOrder.transTime, &dataBlock.msgContent[1], 8 );
  executedOrder.transTime[8] = 0;

  // Messsage type
  strcpy( executedOrder.msgType, "ExecutedOrder" );

  // MsgType
  char msgType = dataBlock.msgContent[9];

  // OrderId (stripped first two digits of account)
  char fieldToken[16];
  memcpy( fieldToken, &dataBlock.msgContent[12], 12);
  fieldToken[12] = 0;
  executedOrder.orderID = atoi( fieldToken );
  
  char exOrderId[32];
  exOrderId[0] = dataBlock.msgContent[10];
  exOrderId[1] = dataBlock.msgContent[11];
  sprintf(&exOrderId[2], "%d", executedOrder.orderID);
  
  if (executedOrder.orderID < 500000)
  {
    if (Manual_Order_Account_Mapping[executedOrder.orderID] >= 0)
    {
      dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID] = Manual_Order_Account_Mapping[executedOrder.orderID];
    }
    else
    {
      Manual_Order_Account_Mapping[executedOrder.orderID] = dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID];
    }
  }
  
  // Shares
  char fieldShares[7];
  memcpy( fieldShares, &dataBlock.msgContent[24], 6 );
  fieldShares[6] = 0;
  executedOrder.shares = atoi( fieldShares );

  // Price
  char fieldPrice[11];
  memcpy( fieldPrice, &dataBlock.msgContent[30], 10 );
  fieldPrice[10] = 0;
  executedOrder.price = atof( fieldPrice ) / 10000;

  //Liquidity flag
  executedOrder.liquidityIndicator = dataBlock.msgContent[40];

  //MatchNo
  char fieldMatchNo[10];
  memcpy(fieldMatchNo, &dataBlock.msgContent[41], 9);
  fieldMatchNo[9] = 0;
  executedOrder.executionId = atoi( fieldMatchNo );
  
  executedOrder.seqNum = 0;

  // orderDetail.msgContent
  orderDetail.execId = executedOrder.orderID;
  orderDetail.length = sprintf( orderDetail.msgContent, "%d=%c\001%d=%s\001%d=%d\001%d=%d\001%d=%lf\001%d=%c\001%d=%d\001", 
                  fieldIndexList.MessageType, msgType, 
                  fieldIndexList.TimeStamp, executedOrder.transTime, 
                  fieldIndexList.Token, executedOrder.orderID, 
                  fieldIndexList.Shares, executedOrder.shares, 
                  fieldIndexList.Price, executedOrder.price, 
                  fieldIndexList.LiquidityFlag, executedOrder.liquidityIndicator, 
                  fieldIndexList.MatchNo, executedOrder.executionId );
  
  strcpy( executedOrder.msgContent, orderDetail.msgContent );

  // Update database
  int tsId;

  if( type == MANUAL_ORDER )
  {
    // tsId
    tsId = MANUAL_ORDER;
  }
  else
  {
    // tsId
    tsId = tradeServersInfo.config[type].tsId;
  }

  char liquidityIndicator[6];
  liquidityIndicator[0] = executedOrder.liquidityIndicator;
  liquidityIndicator[1] = 0;
  
  // Call function to update a open order (non fill)
  int indexOpenOrder = ProcessUpdateASOpenOrderForExecuted(tsId, executedOrder.orderID, orderStatusIndex.Closed, executedOrder.shares, executedOrder.price, &orderDetail, dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID], liquidityIndicator, &executedOrder.fillFee);
  if (indexOpenOrder == -2)
  {
    AddDataBlockToWaitList(TYPE_NASDAQ_RASH, WAIT_LIST_TYPE_RASH_ORDER_FILLED, executedOrder.orderID, dataBlock, type, timeStamp);
    return 0;
  }
  
  if (indexOpenOrder != ERROR)
  {
    strcpy(executedOrder.status,GetOrderStatus(asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status));
    
    executedOrder.leftShares = asOpenOrder.orderCollection[indexOpenOrder].orderSummary.leftShares;   
    executedOrder.avgPrice = asOpenOrder.orderCollection[indexOpenOrder].orderSummary.avgPrice;
  }
  else
  {
  }
  
  executedOrder.entryExitRefNum = *(unsigned short *)&dataBlock.addContent[DATABLOCK_OFFSET_ENTRY_EXIT_REF];
  
  // Call function to update Executed Order of RASH
  ProcessInsertExecutedOrder4RASHQueryToCollection(&executedOrder);

  int formatedOrderId;
  memcpy( fieldToken, &dataBlock.msgContent[12], 12);
  fieldToken[12] = 0;
  formatedOrderId = atoi( fieldToken );
  
  if (AddExecDataToList(&__exec, formatedOrderId, executedOrder.executionId) == ERROR)  //Duplicated
  {
    return 0;
  }
  
  char execID[32];
  sprintf(execID, "%d", executedOrder.executionId);
  
  char ecnOrderID[32] = "''"; 
  int _serverId = (type == MANUAL_ORDER) ? MAX_TRADE_SERVER_CONNECTIONS : type;
  if (ExchangeOrderIdMapping[_serverId][executedOrder.orderID % MAX_ORDER_PLACEMENT] != 0)
  {
    sprintf(ecnOrderID, "%lu", ExchangeOrderIdMapping[_serverId][executedOrder.orderID % MAX_ORDER_PLACEMENT]);
  }
  else
  {
    TraceLog(ERROR_LEVEL, "%s: Could not get ExOrderId, orderID: %d\n", __func__, executedOrder.orderID);
  }
  
  char account[32];
  GetAccountName(TYPE_NASDAQ_RASH, dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID], account);

  char buysell = '0';
  if (indexOpenOrder != ERROR)
  {
    switch (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.side)
    {
      case BUY_TO_CLOSE_TYPE:
      case BUY_TO_OPEN_TYPE:
        buysell = 'B';
        break;
      case SELL_TYPE:
        buysell = 'S';
        break;
      case SHORT_SELL_TYPE:
        buysell = 'T';
        break;
    }
    
    AddOJOrderFillRecord(timeStamp, asOpenOrder.orderCollection[indexOpenOrder].orderSummary.symbol, exOrderId, VENUE_RASH, buysell, executedOrder.price,
              executedOrder.shares, executedOrder.orderID, liquidityIndicator,
              ecnOrderID, execID, "1", (executedOrder.leftShares == 0)?1:0, account, 0);
  }
  else
  {
    AddOJOrderFillRecord(timeStamp, "", exOrderId, VENUE_RASH, buysell, executedOrder.price,
              executedOrder.shares, executedOrder.orderID, liquidityIndicator,
              ecnOrderID, execID, "1", (executedOrder.leftShares == 0)?1:0, account, 0);
  }
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessCanceledOrderOfRASHOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessCanceledOrderOfRASHOrder( t_DataBlock dataBlock, int type, char *timeStamp )
{
  t_RASHCanceledOrder canceledOrder;
  t_OrderDetail orderDetail;

  /*------------------------------------------------
  Order detail:
  - int length;
  - int execId;
  - int isBroken;
  - char timestamp[MAX_TIMESTAMP];
  - char msgContent[MAX_ORDER_MSG];
  ------------------------------------------------*/
  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;
  
  // YYYY/mm/DD
  char dateTime[32];
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]), &dateTime[0]);
    memcpy( canceledOrder.processedTimestamp, &dateTime[11], TIME_LENGTH);
  canceledOrder.processedTimestamp[TIME_LENGTH] = 0;
    
    GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]), &dateTime[0]);
  memcpy( canceledOrder.date, &dateTime[0], 10);
  canceledOrder.date[10] = 0;
  
  // HH:MM:SS.ms
  memcpy( canceledOrder.timestamp, &dateTime[11], TIME_LENGTH);
  canceledOrder.timestamp[TIME_LENGTH] = 0;

  // orderDetail.timestamp
  memcpy(orderDetail.timestamp, &dateTime[0], TIMESTAMP_LENGTH);
  orderDetail.timestamp[TIMESTAMP_LENGTH] = 0;
  
  // Messsage type
  strcpy(canceledOrder.msgType, "CanceledOrder");

  // Sending Time
  memcpy( canceledOrder.transTime, &dataBlock.msgContent[1], 8 );
  canceledOrder.transTime[8] = 0;

  //MsgType
  char msgType = dataBlock.msgContent[9];

  // OrderId (stripped first two digits of account)
  char fieldToken[16];
  memcpy( fieldToken, &dataBlock.msgContent[12], 12);
  fieldToken[12] = 0;
  canceledOrder.orderID = atoi( fieldToken );
  
  if (canceledOrder.orderID < 500000)
  {
    if (Manual_Order_Account_Mapping[canceledOrder.orderID] >= 0)
    {
      dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID] = Manual_Order_Account_Mapping[canceledOrder.orderID];
    }
    else
    {
      Manual_Order_Account_Mapping[canceledOrder.orderID] = dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID];
    }
  }
  
  // Shares
  char fieldShares[7];
  memcpy( fieldShares, &dataBlock.msgContent[24], 6 );
  fieldShares[6] = 0;
  canceledOrder.shares = atoi( fieldShares ); 
  
  // Reason
  canceledOrder.reason = dataBlock.msgContent[30];
  
  canceledOrder.seqNum = 0;
  
  // orderDetail.msgContent
  orderDetail.length = sprintf(orderDetail.msgContent, "%d=%c\001%d=%s\001%d=%d\001%d=%d\001%d=%c\001", 
                  fieldIndexList.MessageType, msgType, 
                  fieldIndexList.TimeStamp, canceledOrder.transTime, 
                  fieldIndexList.Token, canceledOrder.orderID, 
                  fieldIndexList.Shares, canceledOrder.shares, 
                  fieldIndexList.LiquidityFlag, canceledOrder.reason);
  
  strcpy( canceledOrder.msgContent, orderDetail.msgContent );
  
  // Update database
  int tsId;

  if( type == MANUAL_ORDER )
  {
    // tsId
    tsId = MANUAL_ORDER;
  }
  else
  {
    // tsId
    tsId = tradeServersInfo.config[type].tsId;
  }

  // Call function to update a open order (non fill)
  int indexOpenOrder = ProcessUpdateASOpenOrderForStatus(tsId, canceledOrder.orderID, orderStatusIndex.CanceledByECN, &orderDetail );
  if (indexOpenOrder == -2)
  {
    AddDataBlockToWaitList(TYPE_NASDAQ_RASH, WAIT_LIST_TYPE_RASH_ORDER_CANCELED, canceledOrder.orderID, dataBlock, type, timeStamp);
    return 0;
  }
  
  if (indexOpenOrder != ERROR)
  {
    strcpy(canceledOrder.status, GetOrderStatus(asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status));
  }
  else
  {
    strcpy(canceledOrder.status, orderStatus.CanceledByECN);
  }
  
  canceledOrder.entryExitRefNum = *(unsigned short *)&dataBlock.addContent[DATABLOCK_OFFSET_ENTRY_EXIT_REF];
  
  // Call function to update Canceled Order of RASH
  ProcessInsertCanceledOrder4RASHQueryToCollection(&canceledOrder);

  //Add a record to OJ file
  //int AddOJOrderCancelResponseRecord(char *time, char *symbol, int orderID, char *ecnOrderID, char *reason, int venueID, int sizeCanceled, int orderComplete, char *account)
  
  int formatedOrderId;
  memcpy( fieldToken, &dataBlock.msgContent[12], 12);
  fieldToken[12] = 0;
  formatedOrderId = atoi( fieldToken );
  
  if (tsId != MANUAL_ORDER)
  {
    if (AddOrderIdToList(&__cancel, formatedOrderId) == ERROR)  //Duplicated
    {
      return 0;
    }
  }
  else
  {
    if (asOpenOrder.orderCollection[indexOpenOrder].canceledShares + canceledOrder.shares > asOpenOrder.orderCollection[indexOpenOrder].orderSummary.leftShares)
    {
      return 0;
    }
    
    asOpenOrder.orderCollection[indexOpenOrder].canceledShares += canceledOrder.shares;
  }
  
  char reason[1024];
  switch (canceledOrder.reason)
  {
    /*
    �U� User requested cancel. Sent in response to a Cancel Request Message.
    �I� Immediate or Cancel Order.
    �T� Timeout. The Time In Force for this order has expired
    �S� Supervisory. The order was manually canceled or reduced by an NASDAQ supervisory terminal.
    �D� This order cannot be executed because of a regulatory restriction (e.g.: trade through restrictions).
    �Q� Self Match Prevention. The order was cancelled because it would have executed with an existing order entered by the same MPID.
    */
    case 'U':
      strcpy(reason, "User%20requested%20cancel.%20Sent%20in%20response%20to%20a%20Cancel%20Request%20Message");
      break;
    case 'I':
      strcpy(reason, "Immediate%20or%20Cancel%20Order");
      break;
    case 'T':
      strcpy(reason, "Timeout.%20The%20Time%20In%20Force%20for%20this%20order%20has%20expired");
      break;
    case 'S':
      strcpy(reason, "Supervisory.%20The%20order%20was%20manually%20canceled%20or%20reduced%20by%20an%20NASDAQ%20supervisory%20terminal");
      break;
    case 'D':
      strcpy(reason, "This%20order%20cannot%20be%20executed%20because%20of%20a%20regulatory%20restriction%20(e.g.:%20trade%20through%20restrictions)");
      break;
    case 'Q':
      strcpy(reason, "Self%20Match%20Prevention.%20The%20order%20was%20cancelled%20because%20it%20would%20have%20executed%20with%20an%20existing%20order%20entered%20by%20the%20same%20MPID");
      break;
    default:
      sprintf(reason, "Unknown%%20reason,%%20reason%%20code%%20%%3d%%20%c", canceledOrder.reason);
      break;
  }
  
  char ecnOrderID[32] = "''";
  int _serverId = (type == MANUAL_ORDER) ? MAX_TRADE_SERVER_CONNECTIONS : type;
  if (ExchangeOrderIdMapping[_serverId][canceledOrder.orderID % MAX_ORDER_PLACEMENT] != 0)
  {
    sprintf(ecnOrderID, "%lu", ExchangeOrderIdMapping[_serverId][canceledOrder.orderID % MAX_ORDER_PLACEMENT]);
  }
  else
  {
    TraceLog(ERROR_LEVEL, "%s: Could not get ExOrderId, orderID: %d\n", __func__, canceledOrder.orderID);
  }
  
  char account[32];
  GetAccountName(TYPE_NASDAQ_RASH, dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID], account);

  if (indexOpenOrder != ERROR)
  {
    AddOJOrderCancelResponseRecord(timeStamp, asOpenOrder.orderCollection[indexOpenOrder].orderSummary.symbol, canceledOrder.orderID, ecnOrderID, reason, VENUE_RASH, canceledOrder.shares, 1, account);
  }
  else
  {
    AddOJOrderCancelResponseRecord(timeStamp, "", canceledOrder.orderID, ecnOrderID, reason, VENUE_RASH, canceledOrder.shares, 1, account);
  }
  return 0;
}

/****************************************************************************
- Function name:  ProcessCancelRequestOfRASHOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessCancelRequestOfRASHOrder( t_DataBlock dataBlock, int type, char *timeStamp)
{
  t_RASHCancelRequest cancelRequest;
  t_OrderDetail orderDetail;

  /*------------------------------------------------
  Order detail:
  - int length;
  - int execId;
  - int isBroken;
  - char timestamp[MAX_TIMESTAMP];
  - char msgContent[MAX_ORDER_MSG];
  ------------------------------------------------*/
  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;

  // YYYY/mm/DD
  char dateTime[32];
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]), &dateTime[0]);
  memcpy( cancelRequest.date, &dateTime[0], 10);
  cancelRequest.date[10] = 0;
  
  // HH:MM:SS.ms
  memcpy( cancelRequest.timestamp, &dateTime[11], TIME_LENGTH );
  cancelRequest.timestamp[TIME_LENGTH] = 0;

  // orderDetail.timestamp
  memcpy(orderDetail.timestamp, &dateTime[0], TIMESTAMP_LENGTH);
  orderDetail.timestamp[TIMESTAMP_LENGTH] = 0;
  
  // Messsage type
  strcpy( cancelRequest.msgType, "CancelRequest" );

  // MsgType
  char msgType = dataBlock.msgContent[1];

  // OrderId (stripped first two digits of account)
  char fieldToken[16];
  memcpy( fieldToken, &dataBlock.msgContent[4], 12 );
  fieldToken[12] = 0;
  cancelRequest.orderID = atoi( fieldToken );
  
  //Shares
  char fieldShares[7];
  memcpy( fieldShares, &dataBlock.msgContent[16], 6 );
  fieldShares[6] = 0;
  cancelRequest.shares = atoi( fieldShares ); 
  
  cancelRequest.seqNum = 0;
  
  // orderDetail.msgContent
  orderDetail.length = sprintf(orderDetail.msgContent, "%d=%c\001%d=%d\001%d=%d\001", 
                  fieldIndexList.MessageType, msgType, 
                  fieldIndexList.Token, cancelRequest.orderID, 
                  fieldIndexList.Shares, cancelRequest.shares); 
  
  strcpy( cancelRequest.msgContent, orderDetail.msgContent );

  // Update database
  int tsId;

  if( type == MANUAL_ORDER )
  {
    // tsId
    tsId = MANUAL_ORDER;
  }
  else
  {
    // tsId
    tsId = tradeServersInfo.config[type].tsId;
  }

  // Call function to update a open order (non fill)
  int indexOpenOrder = ProcessUpdateASOpenOrderForStatus(tsId, cancelRequest.orderID, orderStatusIndex.CXLSent, &orderDetail );
  if (indexOpenOrder == -2)
  {
    AddDataBlockToWaitList(TYPE_NASDAQ_RASH, WAIT_LIST_TYPE_RASH_CANCEL_REQUEST, cancelRequest.orderID, dataBlock, type, timeStamp);
    return 0;
  }
  
  if (indexOpenOrder != ERROR)
  {
    strcpy(cancelRequest.status, GetOrderStatus(asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status));
  }
  else
  {
    strcpy(cancelRequest.status, orderStatus.CXLSent);
  }
  
  // Call function to update cancel request of RASH
  ProcessInsertCancelRequest4RASHQueryToCollection(&cancelRequest);

  //Add a new record to OJ file
  //  int AddOJOrderCancelRecord(char *time, int orderID, char *symbol, int venueID, int size)

  if (indexOpenOrder != ERROR)
  {
    AddOJOrderCancelRecord(timeStamp, cancelRequest.orderID, asOpenOrder.orderCollection[indexOpenOrder].orderSummary.symbol, VENUE_RASH, cancelRequest.shares);
  }
  else
  {
    AddOJOrderCancelRecord(timeStamp, cancelRequest.orderID, "\0", VENUE_RASH, cancelRequest.shares);
  }
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessRejectedOrderOfRASHOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessRejectedOrderOfRASHOrder(t_DataBlock dataBlock, int type, char *timeStamp)
{
  t_RASHRejectedOrder rejectedOrder;
  t_OrderDetail orderDetail;

  /*------------------------------------------------
  Order detail:
  - int length;
  - int execId;
  - int isBroken;
  - char timestamp[MAX_TIMESTAMP];
  - char msgContent[MAX_ORDER_MSG];
  ------------------------------------------------*/
  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;

  // YYYY/mm/DD
  char dateTime[32];
    GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]), &dateTime[0]);
    memcpy( rejectedOrder.processedTimestamp, &dateTime[11], TIME_LENGTH );
  rejectedOrder.processedTimestamp[TIME_LENGTH] = 0;
    
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]), &dateTime[0]);
  memcpy( rejectedOrder.date, &dateTime[0], 10);
  rejectedOrder.date[10] = 0;
  
  // HH:MM:SS.ms
  memcpy( rejectedOrder.timestamp, &dateTime[11], TIME_LENGTH );
  rejectedOrder.timestamp[TIME_LENGTH] = 0;

  // orderDetail.timestamp
  memcpy(orderDetail.timestamp, &dateTime[0], TIMESTAMP_LENGTH);
  orderDetail.timestamp[TIMESTAMP_LENGTH] = 0;

  // Messsage type
  strcpy( rejectedOrder.msgType, "RejectedOrder" );

  // Sending Time
  memcpy( rejectedOrder.transTime, &dataBlock.msgContent[1], 8 );
  rejectedOrder.transTime[8] = 0;

  // Status: Sent, Live, Rejected, Closed, BrokenTrade, CanceledByECN, 
  // CXLSent, CancelACK, CancelRejected, CancelPending, CanceledByUser
  strcpy( rejectedOrder.status, orderStatus.Rejected );
  
  // MsgType
  char msgType = dataBlock.msgContent[9];

  // OrderId (stripped first two digits of account)
  char fieldToken[16];
  memcpy( fieldToken, &dataBlock.msgContent[12], 12 );
  fieldToken[12] = 0;
  rejectedOrder.orderID = atoi( fieldToken );
  
  if (rejectedOrder.orderID < 500000)
  {
    if (Manual_Order_Account_Mapping[rejectedOrder.orderID] >= 0)
    {
      dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID] = Manual_Order_Account_Mapping[rejectedOrder.orderID];
    }
    else
    {
      Manual_Order_Account_Mapping[rejectedOrder.orderID] = dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID];
    }
  }
  
  //Reason
  rejectedOrder.reason = dataBlock.msgContent[24];
  
  rejectedOrder.seqNum = 0;
  // orderDetail.msgContent
  orderDetail.length = sprintf( orderDetail.msgContent, "%d=%c\001%d=%s\001%d=%d\001%d=%c\001", 
                  fieldIndexList.MessageType, msgType, 
                  fieldIndexList.TimeStamp, rejectedOrder.transTime, 
                  fieldIndexList.Token, rejectedOrder.orderID, 
                  fieldIndexList.Reason, rejectedOrder.reason );
  
  strcpy( rejectedOrder.msgContent, orderDetail.msgContent );
  
  // Update database
  int tsId;

  if( type == MANUAL_ORDER )
  {
    // tsId
    tsId = MANUAL_ORDER;
  }
  else
  {
    // tsId
    tsId = tradeServersInfo.config[type].tsId;
  }

  // Call function to update a open order (non fill)
  int indexOpenOrder = ProcessUpdateASOpenOrderForStatus(tsId, rejectedOrder.orderID, orderStatusIndex.Rejected, &orderDetail );
  if (indexOpenOrder == -2)
  {
    AddDataBlockToWaitList(TYPE_NASDAQ_RASH, WAIT_LIST_TYPE_RASH_ORDER_REJECTED, rejectedOrder.orderID, dataBlock, type, timeStamp);
    return 0;
  }
  
  char reason[1024];
  char reasonCode[2];
  reasonCode[0] = rejectedOrder.reason;
  reasonCode[1] = 0;
  
  if (indexOpenOrder != ERROR)
  {
    //Check to halt symbol (just check on AS side. TSs and PM already checked on their sides)
    if (rejectedOrder.orderID < 500000)
    {
      if (rejectedOrder.reason == 'H')  // 'Halt' --> we will halt on all venues
      {
        unsigned int haltTime = atoi(timeStamp);
        int stockSymbolIndex = GetStockSymbolIndex(asOpenOrder.orderCollection[indexOpenOrder].orderSummary.symbol);
        
        pthread_mutex_lock(&HaltStatusMgmt.mutexLock);
        strncpy(HaltStatusMgmt.symbol[stockSymbolIndex], asOpenOrder.orderCollection[indexOpenOrder].orderSummary.symbol, SYMBOL_LEN);

        int i;
        for (i = 0; i < TS_MAX_BOOK_CONNECTIONS; i++)
        {
          if (haltTime > HaltStatusMgmt.haltTime[stockSymbolIndex][i])  //Only take the latest status
          {
            HaltStatusMgmt.haltTime[stockSymbolIndex][i] = haltTime;
            HaltStatusMgmt.haltStatus[stockSymbolIndex][i] = TRADING_HALTED;
          }
        }

        SaveHaltedStockListToFile();
        SendAHaltStockToAllTS(stockSymbolIndex);
        SendAHaltStockToPM(stockSymbolIndex);
        SendAHaltStockToAllDaedalus(stockSymbolIndex);
        SendAHaltStockToAllTT(stockSymbolIndex);
        pthread_mutex_unlock(&HaltStatusMgmt.mutexLock);
      }
      else if (rejectedOrder.reason == 'X') // 'Invalid Price'
      {
        unsigned int haltTime = atoi(timeStamp);
        int stockSymbolIndex = GetStockSymbolIndex(asOpenOrder.orderCollection[indexOpenOrder].orderSummary.symbol);
        
        pthread_mutex_lock(&HaltStatusMgmt.mutexLock);
        strncpy(HaltStatusMgmt.symbol[stockSymbolIndex], asOpenOrder.orderCollection[indexOpenOrder].orderSummary.symbol, SYMBOL_LEN);

        if (haltTime > HaltStatusMgmt.haltTime[stockSymbolIndex][TS_NASDAQ_BOOK_INDEX]) //Only take the latest status
        {
          HaltStatusMgmt.haltTime[stockSymbolIndex][TS_NASDAQ_BOOK_INDEX] = haltTime;
          HaltStatusMgmt.haltStatus[stockSymbolIndex][TS_NASDAQ_BOOK_INDEX] = TRADING_HALTED;
        }

        SaveHaltedStockListToFile();
        SendAHaltStockToAllTS(stockSymbolIndex);
        SendAHaltStockToPM(stockSymbolIndex);
        SendAHaltStockToAllDaedalus(stockSymbolIndex);
        SendAHaltStockToAllTT(stockSymbolIndex);
        pthread_mutex_unlock(&HaltStatusMgmt.mutexLock);
      }
    }
    
    strcpy(rejectedOrder.status, GetOrderStatus(asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status));
    
    if (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status != orderStatusIndex.CancelRejected)
    {
      // Raise order rejected alert
  
      switch (rejectedOrder.reason)
      {
        case 'Y':
          strcpy(reason, "No Shares Found for Routing");
          break;
        case 'C':
          strcpy(reason, "NASDAQ is Closed");
          break;
        case 'I':
          strcpy(reason, "Invalid Order Side");
          break;
        case 'E':
          strcpy(reason, "Invalid Peg");
          break;
        case 'L':
          strcpy(reason, "Invalid Firm");
          break;
        case 'Z':
          strcpy(reason, "Quantity Exceeds Threshold");
          break;
        case 'O':
          strcpy(reason, "Other. A reason not contemplated in this version of RASH");
          break;
        case 'B':
          strcpy(reason, "Quote not available for pegged order");
          break;
        case 'P':
          strcpy(reason, "Pegging Not Allowed");
          break;
        case 'X':
          strcpy(reason, "Invalid Price");
          break;
        case 'G':
          strcpy(reason, "Destination Not Available");
          break;
        case 'J':
          strcpy(reason, "Processing Error");
          break;
        case 'N':
          strcpy(reason, "Invalid Routing Instructions");
          break;
        case 'D':
          strcpy(reason, "Invalid Display value");
          break;
        case 'M':
          strcpy(reason, "Outside of Permitted Times for Clearing Destination");
          break;
        case 'H':
          strcpy(reason, "Security is Halted");
          break;
        case 'S':
          strcpy(reason, "Invalid Symbol");
          break;
        case 'Q':
          strcpy(reason, "Invalid Order Quantity");
          break;
        case 'K':
          strcpy(reason, "Invalid Minimum Quantity");
          break;
        case 'W':
          strcpy(reason, "Invalid Destination");
          break;
        case 'A':
          strcpy(reason, "Advance Features Not Allowed");
          break;
        case 'U':
          strcpy(reason, "Possible Duplicate Order");
          break;
        case 'V':
          strcpy(reason, "Invalid Order Type");
          break;
        case 'T':
          strcpy(reason, "Test Mode");
          break;
        case 'R':
          strcpy(reason, "Routing Not Allowed");
          break;
        default:
          strcpy(reason, "This is new/unknown reason code");
          break;
      }
      
      CheckNSendOrderRejectedAlertToAllTT(TYPE_NASDAQ_RASH, reasonCode, reason, rejectedOrder.orderID);
    }
  }
  else
  {
    strcpy(rejectedOrder.status, orderStatus.CancelRejected);
  }
      
  rejectedOrder.entryExitRefNum = *(unsigned short *)&dataBlock.addContent[DATABLOCK_OFFSET_ENTRY_EXIT_REF];
  
  // Call function to update Rejected Order of RASH
  ProcessInsertRejectedOrder4RASHQueryToCollection(&rejectedOrder);

  //Add a new record to OJ file
  //int AddOJOrderRejectRecord(char *time, int orderID, char *ecnOrderID, char *reason, char *symbol, int venueID, char *reasonCode, char *account)

  int formatedOrderId;
  memcpy( fieldToken, &dataBlock.msgContent[12], 12);
  fieldToken[12] = 0;
  formatedOrderId = atoi( fieldToken );
  
  if (AddOrderIdToList(&__reject, formatedOrderId) == ERROR)  //Duplicated
  {
    return 0;
  }
  
  char ecnOrderID[2];
  strcpy(ecnOrderID, "''"); //There is no Order Reference Number in this message type
    
  /*
    �Y� No Shares Found for Routing
    �C� NASDAQ is Closed
    �I� Invalid Order Side
    �E� Invalid Peg
    �L� Invalid Firm
    �Z� Quantity Exceeds Threshold
    �O� Other. A reason not contemplated in this version of RASH.
    �B� Quote not available for pegged order.
    �P� Pegging Not Allowed
    �X� Invalid Price
    �G� Destination Not Available
    �J� Processing Error
    �N� Invalid Routing Instructions
    �D� Invalid Display value
    �M� Outside of Permitted Times for Clearing Destination
    �H� Security is Halted
    �S� Invalid Symbol
    �Q� Invalid Order Quantity
    �K� Invalid Minimum Quantity
    �W� Invalid Destination
    �A� Advance Features Not Allowed
    �U� Possible Duplicate Order
    �V� Invalid Order Type
    �T� Test Mode
    �R� Routing Not Allowed
  */
  
  switch (rejectedOrder.reason)
  {
    case 'Y':
      strcpy(reason, "No%20Shares%20Found%20for%20Routing");
      break;
    case 'C':
      strcpy(reason, "NASDAQ%20is%20Closed");
      break;
    case 'I':
      strcpy(reason, "Invalid%20Order%20Side");
      break;
    case 'E':
      strcpy(reason, "Invalid%20Peg");
      break;
    case 'L':
      strcpy(reason, "Invalid%20Firm");
      break;
    case 'Z':
      strcpy(reason, "Quantity%20Exceeds%20Threshold");
      break;
    case 'O':
      strcpy(reason, "Other.%20A%20reason%20not%20contemplated%20in%20this%20version%20of%20RASH");
      break;
    case 'B':
      strcpy(reason, "Quote%20not%20available%20for%20pegged%20order");
      break;
    case 'P':
      strcpy(reason, "Pegging%20Not%20Allowed");
      break;
    case 'X':
      strcpy(reason, "Invalid%20Price");
      break;
    case 'G':
      strcpy(reason, "Destination%20Not%20Available");
      break;
    case 'J':
      strcpy(reason, "Processing%20Error");
      break;
    case 'N':
      strcpy(reason, "Invalid%20Routing%20Instructions");
      break;
    case 'D':
      strcpy(reason, "Invalid%20Display%20value");
      break;
    case 'M':
      strcpy(reason, "Outside%20of%20Permitted%20Times%20for%20Clearing%20Destination");
      break;
    case 'H':
      strcpy(reason, "Security%20is%20Halted");
      break;
    case 'S':
      strcpy(reason, "Invalid%20Symbol");
      break;
    case 'Q':
      strcpy(reason, "Invalid%20Order%20Quantity");
      break;
    case 'K':
      strcpy(reason, "Invalid%20Minimum%20Quantity");
      break;
    case 'W':
      strcpy(reason, "Invalid%20Destination");
      break;
    case 'A':
      strcpy(reason, "Advance%20Features%20Not%20Allowed");
      break;
    case 'U':
      strcpy(reason, "Possible%20Duplicate%20Order");
      break;
    case 'V':
      strcpy(reason, "Invalid%20Order%20Type");
      break;
    case 'T':
      strcpy(reason, "Test%20Mode");
      break;
    case 'R':
      strcpy(reason, "Routing%20Not%20Allowed");
      break;
    default:
      strcpy(reason, "Unknown%20reason");
      break;
      
  }
  
  char account[32];
  GetAccountName(TYPE_NASDAQ_RASH, dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID], account);

  if (indexOpenOrder != ERROR)
  {
    AddOJOrderRejectRecord(timeStamp, rejectedOrder.orderID, ecnOrderID, reason, asOpenOrder.orderCollection[indexOpenOrder].orderSummary.symbol, VENUE_RASH, reasonCode, account);
  }
  else
  {
    AddOJOrderRejectRecord(timeStamp, rejectedOrder.orderID, ecnOrderID, reason, "", VENUE_RASH, reasonCode, account);
  }
  return 0;
}

/****************************************************************************
- Function name:  ProcessBrokenOrderOfRASHOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessBrokenOrderOfRASHOrder(t_DataBlock dataBlock, int type)
{
  t_RASHBrokenOrder brokenTrade;
  t_OrderDetail orderDetail;

  /*------------------------------------------------
  Order detail:
  - int length;
  - int execId;
  - int isBroken;
  - char timestamp[MAX_TIMESTAMP];
  - char msgContent[MAX_ORDER_MSG];
  ------------------------------------------------*/
  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;

  // YYYY/mm/DD
  char dateTime[32];
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]), &dateTime[0]);
  memcpy( brokenTrade.date, &dateTime[0], 10);
  brokenTrade.date[10] = 0;
  
  // HH:MM:SS.ms
  memcpy( brokenTrade.timestamp, &dateTime[11], TIME_LENGTH );
  brokenTrade.timestamp[TIME_LENGTH] = 0;

  //orderDetail.timestamp
  memcpy( orderDetail.timestamp, &dateTime[0], TIMESTAMP_LENGTH );
  orderDetail.timestamp[TIMESTAMP_LENGTH] = 0;
  
  // Messsage type
  strcpy( brokenTrade.msgType, "BrokenOrder" );

  // Sending Time
  memcpy( brokenTrade.transTime, &dataBlock.msgContent[1], 8 );
  brokenTrade.transTime[8] = 0;
  
  // Status: Sent, Live, Rejected, Closed, BrokenTrade, CanceledByECN, 
  // CXLSent, CancelACK, CancelRejected, CancelPending, CanceledByUser
  strcpy( brokenTrade.status, orderStatus.BrokenTrade );
  
  // MsgType
  char msgType = dataBlock.msgContent[9];

  // Order Id
  char fieldToken[16];
  memcpy( fieldToken, &dataBlock.msgContent[12], 12);
  fieldToken[12] = 0;
  brokenTrade.orderID = atoi( fieldToken );

  if (brokenTrade.orderID < 500000)
  {
    if (Manual_Order_Account_Mapping[brokenTrade.orderID] >= 0)
    {
      dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID] = Manual_Order_Account_Mapping[brokenTrade.orderID];
    }
    else
    {
      Manual_Order_Account_Mapping[brokenTrade.orderID] = dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID];
    }
  }
  
  // MatchNo
  char fieldMatchNo[10];
  memcpy( fieldMatchNo, &dataBlock.msgContent[24], 9 );
  fieldMatchNo[9] = 0;
  brokenTrade.executionId = atoi( fieldMatchNo );

  //Reason
  brokenTrade.reason = dataBlock.msgContent[33];
  
  // orderDetail.msgContent
  orderDetail.length = sprintf( orderDetail.msgContent, "%d=%c\001%d=%s\001%d=%d\001%d=%d\001%d=%c\001", 
                  fieldIndexList.MessageType, msgType, 
                  fieldIndexList.TimeStamp, brokenTrade.transTime, 
                  fieldIndexList.Token, brokenTrade.orderID, 
                  fieldIndexList.MatchNo, brokenTrade.executionId, 
                  fieldIndexList.Reason, brokenTrade.reason );  
  
  strcpy( brokenTrade.msgContent, orderDetail.msgContent );
  
  // Update database
  int tsId;

  if( type == MANUAL_ORDER )
  {
    // tsId
    tsId = MANUAL_ORDER;
  }
  else
  {
    // tsId
    tsId = tradeServersInfo.config[type].tsId;
  }

  // Call function to update a open order (non fill)
  if (ProcessUpdateASOpenOrderForStatus(tsId, brokenTrade.orderID, orderStatusIndex.BrokenTrade, &orderDetail ) == -2)
  {
    AddDataBlockToWaitList(TYPE_NASDAQ_RASH, WAIT_LIST_TYPE_RASH_ORDER_BROKEN, brokenTrade.orderID, dataBlock, type, "");
    return 0;
  }
  
  // Call function to update Broken Trade of RASH
  ProcessInsertBrokenTrade4RASHQueryToCollection(&brokenTrade);
  
  //Process add broken trade message to as logs
  int indexOpenOrder = CheckOrderIdIndex(brokenTrade.orderID, tsId);
  if (indexOpenOrder >= 0)
  {
    // Add the broken trade message to AS error log
    TraceLog(DEBUG_LEVEL, "Broken Trade message received from RASH for symbol %.8s.\n", asOpenOrder.orderCollection[indexOpenOrder].orderSummary.symbol);
  }

  return 0;
}


/***************************************************************************/
