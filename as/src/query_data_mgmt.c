/*****************************************************************************
**  Project:    Equity Arbitrage Application
**  Filename:   query_data_mgmt.c
**  Description:  This file contains function definitions that were declared
          in query_data_mgmt.h  
**  Author:     Tho Huynh-Ngoc
**  First created:  18-May-2011
**  Last updated: -----------
*****************************************************************************/

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include "query_data_mgmt.h"
#include "configuration.h"
#include "order_mgmt_proc.h"
#include "arca_direct_data.h"
#include "nasdaq_ouch_data.h"
#include "nasdaq_rash_data.h"
#include "raw_data_mgmt.h"
#include "nyse_ccg_data.h"
#include "bats_boe_data.h"
#include "edge_order_data.h"
#include "database_util.h"

/****************************************************************************
** Global variables definition
****************************************************************************/

t_QueryDataMgmt queryDataMgmt;
/****************************************************************************
** Function declarations
****************************************************************************/

/****************************************************************************
- Function name:  InitializeQueryDataManegementStructures
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int InitializeQueryDataManegementStructures(void)
{
  queryDataMgmt.fileQueriesCollectionDesc = NULL;
  queryDataMgmt.lastUpdatedIndex = -1;
  queryDataMgmt.countQueryDataBlock = 0;
  queryDataMgmt.countProccessed = 0;
  pthread_mutex_init(&queryDataMgmt.updateMutex, NULL);
  memset(&queryDataMgmt.queryDataBlockCollection, 0, MAX_QUERY_DATA_ENTRIES * sizeof (t_QueryDataBlock));
  
  //For position query data strucure
  queryDataMgmt.fileCountQueryProcessedDesc = NULL;
  queryDataMgmt.positionProcessed = 0;
  
  return 0;
}

/****************************************************************************
- Function name:  SavePositionRawDataProcessedToFile
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SavePositionQueryProcessedToFile(void)
{ 
  if (queryDataMgmt.fileCountQueryProcessedDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "(%s) Cannot open file '%s'\n", __func__, FILE_NAME_OF_QUERY_PROCESSED);

    return ERROR;
  }

  rewind(queryDataMgmt.fileCountQueryProcessedDesc);

  if (fwrite(&queryDataMgmt.positionProcessed, sizeof(queryDataMgmt.positionProcessed), 1, queryDataMgmt.fileCountQueryProcessedDesc) != 1)
  {
    TraceLog(ERROR_LEVEL, "(%s) Cannot save count query data processed to file '%s'\n", __func__, FILE_NAME_OF_QUERY_PROCESSED);

    return ERROR;
  }
  
  // Flush all data from buffer to file
  fflush(queryDataMgmt.fileCountQueryProcessedDesc);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  AddQueryDataBlockToCollection
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int AddQueryDataBlockToCollection(const void *queryDataBlock, void *queryDataMgmt)
{
  t_QueryDataMgmt *workingQueryMgmt = (t_QueryDataMgmt *)queryDataMgmt;
  int numQueryBlocks = 0;
  
  numQueryBlocks = workingQueryMgmt->countQueryDataBlock - workingQueryMgmt->positionProcessed;
  
  if (numQueryBlocks >= MAX_QUERY_DATA_ENTRIES - 1)
  {
    TraceLog(ERROR_LEVEL, "Query Data block collection is full: countQueryDataBlock = %d, positionProcessed = %d\n", workingQueryMgmt->countQueryDataBlock, workingQueryMgmt->positionProcessed);
    exit(0);
  }
      
  // We will store new data block at last updated index + 1
  workingQueryMgmt->lastUpdatedIndex++;
  
  // Add new data block to collection
  workingQueryMgmt->queryDataBlockCollection[workingQueryMgmt->lastUpdatedIndex % MAX_QUERY_DATA_ENTRIES] = *((t_QueryDataBlock *)queryDataBlock);
  
  // Increase total of data blocks
  workingQueryMgmt->countQueryDataBlock++;
  
  return 0;
}

/****************************************************************************
- Function name:  SaveQueryDataToFile
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SaveQueryDataToFile(void *queryDataMgmt)
{
  t_QueryDataMgmt *workingQueryMgmt = (t_QueryDataMgmt *)queryDataMgmt;
  
  // Save new query data blocks at last updated index
  if (fwrite(&workingQueryMgmt->queryDataBlockCollection[workingQueryMgmt->lastUpdatedIndex % MAX_QUERY_DATA_ENTRIES], sizeof(t_QueryDataBlock), 1, workingQueryMgmt->fileQueriesCollectionDesc) != 1)
  {
    TraceLog(ERROR_LEVEL, "Cannot save query data to file\n");

    return ERROR;
  }
  
  // Flush all data from buffer to file
  fflush(workingQueryMgmt->fileQueriesCollectionDesc);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  GetNumberOfQueryEntriesFromFile
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int GetNumberOfQueryEntriesFromFile(const char *fileName, void *queryDataMgmt)
{
  t_QueryDataMgmt *workingQueryMgmt = (t_QueryDataMgmt *)queryDataMgmt;
  
  // Get full raw data path
  char fullPath[MAX_PATH_LEN] = "\0";
  
  if (GetFullRawDataPath(FILE_NAME_OF_QUERIES_COLLECTION, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Have some problem in get full query data path operation (%s)\n", FILE_NAME_OF_QUERIES_COLLECTION);

    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Load query data: %s\n", fullPath);
  if (IsFileTimestampToday(fullPath) == 0)
  {
    TraceLog(ERROR_LEVEL, "%s is old, please check the correctness of data files\n", fullPath);
    exit(0);
  }
  
  /* 
  Open file to read in BINARY MODE, 
  Open a file for reading and appending. The file is created if it does not exist.
  */
  FILE *fileDesc = fopen(fullPath, "ab+");
  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Cannot open file '%s' to read or we don't have permission to create\n", fullPath);

    return ERROR;
  }
  else
  {
    // Store pointer to file that will be used after
    workingQueryMgmt->fileQueriesCollectionDesc = fileDesc;
    
    struct stat fileStat;
    int retValue = stat(fullPath, &fileStat);
    
    if (retValue == SUCCESS)
    {
      // Get file size
      int fileSizeInBytes = fileStat.st_size;
      
      // Check file size
      if (fileSizeInBytes % sizeof(t_QueryDataBlock) != 0)
      {
        TraceLog(ERROR_LEVEL, "INVALID FILE SIZE: fileName = %s, fileSize = %d\n", fullPath, fileSizeInBytes);

        return ERROR;
      }
      
      workingQueryMgmt->countQueryDataBlock = fileSizeInBytes / sizeof(t_QueryDataBlock);

      TraceLog(DEBUG_LEVEL, "Count query entries: %d\n", workingQueryMgmt->countQueryDataBlock);
    }
    else
    {
      return ERROR;
    }
  } 
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadPositionQueryProcessedFromFile
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int LoadPositionQueryProcessedFromFile(const char *fileName)
{
  FILE *fileRead, *fileWrite; 

  // Get full raw data path
  char fullPath[MAX_PATH_LEN] = "\0";
  
  if (GetFullRawDataPath(fileName, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Have some problem in get full query path operation (%s)\n", fileName);

    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Load position query data processed: %s\n", fullPath);

  if (IsFileTimestampToday(fullPath) == 0)
  {
    TraceLog(ERROR_LEVEL, "%s is old, please check the correctness of data files\n", fullPath);
    exit(0);
  }
  
  struct stat fileStat;
  int retValue = stat(fullPath, &fileStat);
  int fileSizeInBytes = 0;

  if (retValue == SUCCESS)
  {
    // Get file size
    fileSizeInBytes = fileStat.st_size;

    // Check file size
    if (fileSizeInBytes == 0)
    {
      /* 
      Open file to read in BINARY MODE, 
      Create an empty file for both reading and writing. If a file with the same name already exists its content is erased and the file is treated as a new empty file.
      */
      fileWrite = fopen(fullPath, "wb+");
      if (fileWrite == NULL)
      {
        TraceLog(ERROR_LEVEL, "Cannot open file '%s' to read or we don't have permission to create\n", fullPath);

        return ERROR;
      }
      else
      {
        // Store pointer to file that will be used after
        queryDataMgmt.fileCountQueryProcessedDesc = fileWrite;
        
        return SUCCESS;
      }

      return SUCCESS;
    }
    else if (fileSizeInBytes > 0)
    {
      /*
      if (fileSizeInBytes != sizeof(RawDataProcessedMgmt.tsDataBlock) + sizeof(RawDataProcessedMgmt.asDataBlock))
      {
        TraceLog(ERROR_LEVEL, "INVALID FILE SIZE: %s\n", fullPath);
        
        return ERROR;
      }
      */      

      if (sizeof(queryDataMgmt.positionProcessed) % 4 != 0)
      {
        TraceLog(ERROR_LEVEL, "INVALID FILE SIZE: %s\n", fullPath);

        return ERROR;
      }     
    }
  }

  TraceLog(DEBUG_LEVEL, "Read index porcessed query data (%d bytes) ...\n", fileSizeInBytes);

  /* 
  Open file to read in BINARY MODE, 
  Open a file for update both reading and writing. The file must exist.
  */
  fileRead = fopen(fullPath, "rb+");
  if (fileRead != NULL)
  {
    if (fread(&queryDataMgmt.positionProcessed, sizeof(queryDataMgmt.positionProcessed), 1, fileRead) != 1)
    {
      TraceLog(ERROR_LEVEL, "Cannot load count block query data processed %s\n", fullPath);

      return ERROR;
    }

    TraceLog(DEBUG_LEVEL, "Load query data: %d\n", queryDataMgmt.positionProcessed);

    // Close the file
    fclose(fileRead);
  }
  
  /* 
  Open file to read in BINARY MODE, 
  Create an empty file for both reading and writing. If a file with the same name already exists its content is erased and the file is treated as a new empty file.
  */
  fileWrite = fopen(fullPath, "wb+");
  if (fileWrite == NULL)
  {
    TraceLog(ERROR_LEVEL, "Cannot open file '%s' to read or we don't have permission to create\n", fullPath);

    return ERROR;
  }
  else
  {
    // Store pointer to file that will be used after
    queryDataMgmt.fileCountQueryProcessedDesc = fileWrite;

    // Save position raw data processed
    SavePositionQueryProcessedToFile();
    
    return SUCCESS;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessHistoryOfQueryData
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessHistoryOfQueryData( void )
{
  TraceLog(DEBUG_LEVEL, "Process history of query data ...\n" );

  // Process query data from file
  if(queryDataMgmt.positionProcessed < queryDataMgmt.countQueryDataBlock)
  {
    ProcessQueryDatabaseFromFile(&queryDataMgmt.positionProcessed, &queryDataMgmt);
  }

  TraceLog(DEBUG_LEVEL, "Finished processing history of database query\n");

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessQueryDataBlockToDatabase
- Description:    
- Usage:      
****************************************************************************/
int ProcessQueryDataBlockToDatabase(t_QueryDataBlock *queryDataBlock)
{
  int queryType = queryDataBlock->queryType;
    
  switch (queryType)
  {
    case INSERT_NEW_ORDER:
      return ProcessInsertNewOrder(&queryDataBlock->detail.newOrder);
    case INSERT_ORDER_ACCEPTED: 
      return ProcessInsertOrderACK(&queryDataBlock->detail.orderAccepted);
    case INSERT_ORDER_EXECUTED:
      return InsertOrderExecuted(&queryDataBlock->detail.orderExecuted);
    case INSERT_ORDER_CANCELED:
      return InsertOrderCancelled(&queryDataBlock->detail.orderCanceled);
    case INSERT_CANCEL_REQUEST:
      return InsertCancelRequest(&queryDataBlock->detail.cancelRequest);
    case INSERT_ORDER_REJECTED:
      return InsertOrderRejected(&queryDataBlock->detail.orderRejected);
    case INSERT_BUST_OR_CORRECT:
      return InsertlBustOrCorrect(&queryDataBlock->detail.brokenTrade);
    case INSERT_CANCEL_PENDING:
      return InsertCancelPending(&queryDataBlock->detail.cancelPending);
    case INSERT_CANCEL_ACK:
      return InsertCancelACK(&queryDataBlock->detail.cancelACK);
    case INSERT_CANCEL_REJECTED:
      return InsertCancelRejected(&queryDataBlock->detail.cancelRejected);
    case INSERT_BBO_QUOTE_INFO:
      return InsertBBOQuotesIntoDatabase(&queryDataBlock->detail.bboInfo);
    case INSERT_SHORT_SELL_FAILED:
      return InsertTradeOutputLogShortSellFailed(&queryDataBlock->detail.shortSellFailed);
    case INSERT_OUTPUT_LOG_TRADE:
      return UpdateTradeOutputLog_Trade(&queryDataBlock->detail.outputLogTrade);
    case INSERT_TRADE_RESULT:
      return InsertTradeResultToDatabase(&queryDataBlock->detail.tradeResult);
    case INSERT_OUTPUT_LOG_DETECTED_TRADE:
      return InsertTradeOutputLogDetected_Trade(&queryDataBlock->detail.outputLogDetectedTrade);
    case INSERT_OUTPUT_LOG_DETECTED:
      return InsertTradeOutputLogDetected(&queryDataBlock->detail.outputLogDetected);
    case INSERT_OUTPUT_LOG_NOT_YET_DETECT_TRADE:
      return InsertTradeOutputLogNotYetDetect_Trade(&queryDataBlock->detail.outputLogNotYetDetectTrade);
    case INSERT_OUTPUT_LOG_NOT_YET_DETECT:
      return InsertTradeOutputLogNotYetDetect(&queryDataBlock->detail.outputLogNotYetDetect);
    case INSERT_OUTPUT_LOG_ASK_BID:
      return InsertAskBid(&queryDataBlock->detail.outputLogAskBid);
    case INSERT_OUTPUT_LOG:
      return UpdateTradeOutputLog(&queryDataBlock->detail.outputLog);
    case UPDATE_REAL_VALUE:
      return UpdateRealValueToDatabase(&queryDataBlock->detail.realValue);
    default:
      TraceLog(ERROR_LEVEL, "ProcessQueryDatabaseFromCollection: INVALID Query Type, queryType = %d\n", queryType);
      return ERROR;
  }
}

/****************************************************************************
- Function name:  ProcessQueryDatabaseFromFile
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessQueryDatabaseFromFile( int *countQueryProcessed, void *queryDataMgmt )
{
  //TraceLog(DEBUG_LEVEL, "\nProcess query data from file ...\n");
  t_QueryDataMgmt *workingQueryMgmt = (t_QueryDataMgmt *)queryDataMgmt;

  int i, retValue;
  t_QueryDataBlock queryDataBlock;

  // Number of data blocks need process
  int numQueryBlocks = workingQueryMgmt->countQueryDataBlock - (*countQueryProcessed);

  TraceLog(DEBUG_LEVEL, "Processing queries from file: %d/%d\n", *countQueryProcessed, workingQueryMgmt->countQueryDataBlock);

  // Set cursor position for file at begin of specific data block
  fseek(workingQueryMgmt->fileQueriesCollectionDesc, (*countQueryProcessed) * sizeof(t_QueryDataBlock), SEEK_SET);  

  int iShowBusyNotice = 0;
  for( i = 0; i <= numQueryBlocks; i++ )
  {
    iShowBusyNotice ++;
    if (iShowBusyNotice % 5000 == 0)
    {
      TraceLog(DEBUG_LEVEL, "Notice: Database is being updated (%d/%d), please be patient...\n", iShowBusyNotice, numQueryBlocks);
    }
    
    retValue = fread(&queryDataBlock, sizeof( t_QueryDataBlock ), 1, workingQueryMgmt->fileQueriesCollectionDesc);
      
    if( retValue != 1 )
    {
      return ERROR;
    }
    
    ProcessQueryDataBlockToDatabase(&queryDataBlock);
    
    // Increase number of data blocks process
    (*countQueryProcessed) ++;

    // Save position query data processed
    SavePositionQueryProcessedToFile();
    
  }

  return SUCCESS;
}
/****************************************************************************
- Function name:  ProcessQueryDatabaseFromCollection
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessQueryDatabaseFromCollection(int *countQueryProcessed, void *queryDataMgmt)
{
  //TraceLog(DEBUG_LEVEL, "\nProcess query data from collection ...\n");
  
  t_QueryDataMgmt *workingQueryMgmt = (t_QueryDataMgmt *)queryDataMgmt;
  t_QueryDataBlock *queryDataBlock;
  
  int numQueryBlocks, startIndex, i;  

  pthread_mutex_lock(&workingQueryMgmt->updateMutex);

  // Number of data blocks need process
  numQueryBlocks = workingQueryMgmt->countQueryDataBlock - (*countQueryProcessed);

  // Raw data index will start process
  startIndex = workingQueryMgmt->lastUpdatedIndex - numQueryBlocks + 1;

  pthread_mutex_unlock(&workingQueryMgmt->updateMutex);

  TraceLog(DEBUG_LEVEL, "Processing queries from collection: %d/%d\n", *countQueryProcessed, workingQueryMgmt->countQueryDataBlock);

  // Check collection is full
  if (numQueryBlocks > MAX_QUERY_DATA_ENTRIES)
  {
    TraceLog(ERROR_LEVEL, "Query block collection is full: countQueryDataBlock = %d, countQueryProcessed = %d\n", workingQueryMgmt->countQueryDataBlock, *countQueryProcessed);

    //  Add code here to process this problem

    return ERROR_BUFFER_OVER;
  }

  int iShowBusyNotice = 0;
  
  for (i = 0; i < numQueryBlocks; i++)
  {
    iShowBusyNotice ++;
    if (iShowBusyNotice % 5000 == 0)
    {
      TraceLog(DEBUG_LEVEL, "Notice: Database is being updated (%d/%d), please be patient...\n", iShowBusyNotice, numQueryBlocks);
    }
    
    queryDataBlock = &(workingQueryMgmt->queryDataBlockCollection[(startIndex + i) % MAX_QUERY_DATA_ENTRIES]);
    
    ProcessQueryDataBlockToDatabase(queryDataBlock);
    
    // Increase number of data blocks process
    (*countQueryProcessed) ++;

    // Save position query data processed
    SavePositionQueryProcessedToFile();
    
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessInsertNewOrder4ARCA_DIRECTQueryToCollection
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertNewOrder4ARCA_DIRECTQueryToCollection(t_ARCA_DIRECTNewOrder *newOrder)
{
  t_QueryDataBlock queryDataBlock;
  t_New_Order_DB *info = &queryDataBlock.detail.newOrder;
  
  memset(info, 0, sizeof(t_New_Order_DB));

  queryDataBlock.queryType = INSERT_NEW_ORDER;
  
  //time stamp
  memcpy(info->timestamp, newOrder->timestamp, TIME_LENGTH);
  
  //ecn
  strcpy(info->ecn, newOrder->ecn);
  
  //shares    
  info->shares = newOrder->shares;
  
  //price
  info->price = newOrder->price;

  //symbol
  strncpy(info->symbol, newOrder->symbol, SYMBOL_LEN);
  
  //time in force
  strcpy(info->timeInForce, newOrder->timeInForce);
  
  //left shares
  info->leftShares = newOrder->shares;

  //status
  strcpy(info->status, newOrder->status);
  
  //Sequence number
  info->seqNum = newOrder->seqNum;
  
  //OrderId
  info->orderID = newOrder->orderID;
  
  //side
  strcpy(info->side, newOrder->side);
  
  //avg_price
  info->avgPrice = newOrder->price;
  
  //msg_content
  strcpy(info->msgContent, newOrder->msgContent);
  
  //entry_exit
  strcpy(info->entryExit, newOrder->entryExit);
  
  //ecn_launch
  strcpy(info->ecnLaunch, newOrder->ecnLaunch);
  
  //ask bid
  strcpy(info->askBid, newOrder->askBid);
  
  //ts_id
  info->tsId = newOrder->tsId;
  
  //cross id
  info->crossId = newOrder->crossId;

  //date
  memcpy(info->date, newOrder->date, 10);
  
  //trade type
  info->tradeType = newOrder->tradeType;
  
  //is iso
  info->isoFlag = newOrder->isoFlag;
  
  //bboId
  info->bboId = newOrder->bboId;
  
  //trading account
  info->tradingAccount = newOrder->tradingAccount;
  
  //entry-exit ref num
  info->entryExitRefNum = newOrder->entryExitRefNum;
  
  // Visible shares
  info->visibleShares = newOrder->visibleShares;
  
  //routingInst
  strcpy(info->routingInst, "");
    
  pthread_mutex_lock(&queryDataMgmt.updateMutex);
    
  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);
  
  SaveQueryDataToFile(&queryDataMgmt);
  
  pthread_mutex_unlock(&queryDataMgmt.updateMutex);
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessInsertOrderACK4ARCA_DIRECTQueryToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertOrderACK4ARCA_DIRECTQueryToCollection(t_ARCA_DIRECTOrderACK *orderACK)
{
  t_QueryDataBlock queryDataBlock;
  t_Order_ACK_DB *info = &queryDataBlock.detail.orderAccepted;
  
  memset(info, 0, sizeof(t_Order_ACK_DB));
  
  //queryType
  queryDataBlock.queryType = INSERT_ORDER_ACCEPTED;
  
  //OrderId
  info->orderID = orderACK->orderID;
  
  //date
  memcpy(info->date, orderACK->date, 10);
  
  //time stamp
  memcpy(info->timestamp, orderACK->timestamp, TIME_LENGTH);
  
  memcpy(info->processedTimestamp, orderACK->processedTimestamp, TIME_LENGTH);
  
  //msg seq num
  info->seqNum = orderACK->seqNum;
  
  //exOrderId
  sprintf(info->exOrderId, "%lu", orderACK->arcaExOrderId);
  
  //msg_content
  strcpy(info->msgContent, orderACK->msgContent);
  
  //price
  info->ackPrice = orderACK->price;
  
  info->ackShares = 0;
  
  //status
  strcpy(info->status, orderACK->status);
  
  pthread_mutex_lock(&queryDataMgmt.updateMutex);
    
  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);
  
  SaveQueryDataToFile(&queryDataMgmt);
  
  pthread_mutex_unlock(&queryDataMgmt.updateMutex);
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessInsertExecutedOrder4ARCA_DIRECTQueryToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertExecutedOrder4ARCA_DIRECTQueryToCollection(t_ARCA_DIRECTOrderFill *orderFill)
{
  t_QueryDataBlock queryDataBlock;
  t_Order_Executed_DB *info = &queryDataBlock.detail.orderExecuted;
  
  memset(info, 0, sizeof(t_Order_Executed_DB));
  
  //queryType
  queryDataBlock.queryType = INSERT_ORDER_EXECUTED;
  
  //OrderId
  info->orderID = orderFill->orderID;
  
  //date
  memcpy(info->date, orderFill->date, 10);
  
  //shares
  info->lastShares = orderFill->shares;
  
  //price
  info->lastPrice = orderFill->price;
  
  //time stamp
  memcpy(info->timestamp, orderFill->timestamp, TIME_LENGTH);
  
  memcpy(info->processedTimestamp, orderFill->processedTimestamp, TIME_LENGTH);
  
  //msg seq num
  info->seqNum = orderFill->seqNum;
  
  //exOrderId
  sprintf(info->exOrderId, "%lu", orderFill->arcaExOrderId);
  
  //execId
  sprintf(info->execId, "%lu", orderFill->executionId);
  
  //msg_content
  strcpy(info->msgContent, orderFill->msgContent);
  
  //liquidityIndicator
  info->liquidity[0] = orderFill->liquidityIndicator;
  
  //status
  strcpy(info->status, orderFill->status);
  
  //leftShares
  info->leftShares = orderFill->leftShares;
  
  //avgPrice
  info->avgPrice = orderFill->avgPrice;
  
  // Fill fee
  info->fillFee = orderFill->fillFee;
  
  info->entryExitRefNum = orderFill->entryExitRefNum;
  
  pthread_mutex_lock(&queryDataMgmt.updateMutex);
    
  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);
  
  SaveQueryDataToFile(&queryDataMgmt);
  
  pthread_mutex_unlock(&queryDataMgmt.updateMutex);
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessInsertCanceledOrder4ARCA_DIRECTQueryToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertCanceledOrder4ARCA_DIRECTQueryToCollection(t_ARCA_DIRECTOrderCancel *orderCancel)
{
  t_QueryDataBlock queryDataBlock;
  t_Order_Canceled_DB *info = &queryDataBlock.detail.orderCanceled;
  
  memset(info, 0, sizeof(t_Order_Canceled_DB));
  
  //queryType
  queryDataBlock.queryType = INSERT_ORDER_CANCELED;
  
  //OrderId
  info->orderID = orderCancel->orderID;
  
  //date
  memcpy(info->date, orderCancel->date, 10);
  
  //time stamp
  memcpy(info->timestamp, orderCancel->timestamp, TIME_LENGTH);
    
  memcpy(info->processedTimestamp, orderCancel->processedTimestamp, TIME_LENGTH);
  
  //msg seq num
  info->seqNum = orderCancel->seqNum;
  
  //ExOrderId
  sprintf(info->exOrderId, "%lu", orderCancel->arcaExOrderId);

  //msg_content
  strcpy(info->msgContent, orderCancel->msgContent);
  
  //status
  strcpy(info->status, orderCancel->status);
  
  info->entryExitRefNum = orderCancel->entryExitRefNum;
  
  pthread_mutex_lock(&queryDataMgmt.updateMutex);
    
  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);
  
  SaveQueryDataToFile(&queryDataMgmt);
  
  pthread_mutex_unlock(&queryDataMgmt.updateMutex);
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessInsertCancelRequest4ARCA_DIRECTQueryToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertCancelRequest4ARCA_DIRECTQueryToCollection(t_ARCA_DIRECTCancelRequest *cancelRequest)
{
  t_QueryDataBlock queryDataBlock;
  t_Cancel_Request_DB *info = &queryDataBlock.detail.cancelRequest;
  
  memset(info, 0, sizeof(t_Cancel_Request_DB));
  
  //queryType
  queryDataBlock.queryType = INSERT_CANCEL_REQUEST;
  
  //OrderId
  info->orderID = cancelRequest->orderID;
  
  //date
  memcpy(info->date, cancelRequest->date, 10);
  
  //time stamp
  memcpy(info->timestamp, cancelRequest->timestamp, TIME_LENGTH);
  
  //msg seq num
  info->seqNum = cancelRequest->seqNum;
  
  //arcaExOrderId
  sprintf(info->exOrderId, "%lu", cancelRequest->arcaExOrderId);

  //msg_content
  strcpy(info->msgContent, cancelRequest->msgContent);
  
  //status
  strcpy(info->status, cancelRequest->status);
  
  pthread_mutex_lock(&queryDataMgmt.updateMutex);
    
  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);
  
  SaveQueryDataToFile(&queryDataMgmt);
  
  pthread_mutex_unlock(&queryDataMgmt.updateMutex);
  
  return 0;
}


/****************************************************************************
- Function name:  ProcessInsertCancelACK4ARCA_DIRECTQueryToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertCancelACK4ARCA_DIRECTQueryToCollection(t_ARCA_DIRECTCancelACK *cancelACK)
{
  t_QueryDataBlock queryDataBlock;
  t_Cancel_ACK_DB *info = &queryDataBlock.detail.cancelACK;
  
  memset(info, 0, sizeof(t_Cancel_ACK_DB));
  
  //queryType
  queryDataBlock.queryType = INSERT_CANCEL_ACK;
  
  //OrderId
  info->orderID = cancelACK->orderID;
  
  //date
  memcpy(info->date, cancelACK->date, 10);
  
  //time stamp
  memcpy(info->timestamp, cancelACK->timestamp, TIME_LENGTH);
  
  //msg seq num
  info->seqNum = cancelACK->seqNum;
  
  //arcaExOrderId
  sprintf(info->exOrderId, "%lu", cancelACK->arcaExOrderId);

  //msg_content
  strcpy(info->msgContent, cancelACK->msgContent);
  
  //status
  strcpy(info->status, cancelACK->status);
  
  pthread_mutex_lock(&queryDataMgmt.updateMutex);
    
  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);
  
  SaveQueryDataToFile(&queryDataMgmt);
  
  pthread_mutex_unlock(&queryDataMgmt.updateMutex);
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessInsertBustOrCorrect4ARCA_DIRECTQueryToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertBustOrCorrect4ARCA_DIRECTQueryToCollection(t_ARCA_DIRECTBustOrCorrect *brokenTrade)
{
  t_QueryDataBlock queryDataBlock;
  t_Broken_Trade_DB *info = &queryDataBlock.detail.brokenTrade;
  
  memset(info, 0, sizeof(t_Broken_Trade_DB));
  
  //queryType
  queryDataBlock.queryType = INSERT_BUST_OR_CORRECT;
  
  //OrderId
  info->orderID = brokenTrade->orderID;
  
  //date
  memcpy(info->date, brokenTrade->date, 10);
  
  //shares
  info->shares = brokenTrade->shares;
  
  //price
  info->price = brokenTrade->price;
  
  //time stamp
  memcpy(info->timestamp, brokenTrade->timestamp, TIME_LENGTH);
  
  //msg seq num
  info->seqNum = brokenTrade->seqNum;
  
  //executionId
  sprintf(info->execId, "%lu", brokenTrade->executionId);

  //msg_content
  strcpy(info->msgContent, brokenTrade->msgContent);
  
  //status
  strcpy(info->status, brokenTrade->status);
  
  pthread_mutex_lock(&queryDataMgmt.updateMutex);
    
  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);
  
  SaveQueryDataToFile(&queryDataMgmt);
  
  pthread_mutex_unlock(&queryDataMgmt.updateMutex);
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessInsertReject4ARCA_DIRECTQueryToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertReject4ARCA_DIRECTQueryToCollection(t_ARCA_DIRECTReject *reject)
{
  t_QueryDataBlock queryDataBlock;
  
  if (reject->type == '1') // Order rejected
  {
    t_Order_Rejected_DB *info = &queryDataBlock.detail.orderRejected;
    
    memset(info, 0, sizeof(t_Order_Rejected_DB));

    //queryType
    queryDataBlock.queryType = INSERT_ORDER_REJECTED;
    
    //OrderId
    info->orderID = reject->orderID;
    
    //date
    memcpy(info->date, reject->date, 10);
    
    //time stamp
    memcpy(info->timestamp, reject->timestamp, TIME_LENGTH);
    
    memcpy(info->processedTimestamp, reject->processedTimestamp, TIME_LENGTH);
    
    //msg seq num
    info->seqNum = reject->seqNum;
    
    //text
    ConvertSpecialCharacter(reject->text, info->reasonText);
    
    //msg_content
    strcpy(info->msgContent, reject->msgContent);
    
    //status
    strcpy(info->status, reject->status);
    
    info->entryExitRefNum = reject->entryExitRefNum;
  }
  else // Cancel rejected
  {
    t_Cancel_Rejected_DB *info = &queryDataBlock.detail.cancelRejected;
  
    memset(info, 0, sizeof(t_Cancel_Rejected_DB));
    
    //queryType
    queryDataBlock.queryType = INSERT_CANCEL_REJECTED;
    
    //OrderId
    info->orderID = reject->orderID;
    
    //date
    memcpy(info->date, reject->date, 10);
    
    //time stamp
    memcpy(info->timestamp, reject->timestamp, TIME_LENGTH);
    
    //msg seq num
    info->seqNum = reject->seqNum;
    
    //text
    ConvertSpecialCharacter(reject->text, info->reasonText);

    //msg_content
    strcpy(info->msgContent, reject->msgContent);

    //status
    strcpy(info->status, reject->status);
  }
  
  pthread_mutex_lock(&queryDataMgmt.updateMutex);
    
  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);
  
  SaveQueryDataToFile(&queryDataMgmt);
  
  pthread_mutex_unlock(&queryDataMgmt.updateMutex);
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessInsertNewOrder4OUCHQueryToCollection
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertNewOrder4OUCHQueryToCollection(t_OUCHNewOrder *newOrder)
{
  t_QueryDataBlock queryDataBlock;
  t_New_Order_DB *info = &queryDataBlock.detail.newOrder;
  
  memset(info, 0, sizeof(t_New_Order_DB));
  
  //queryType
  queryDataBlock.queryType = INSERT_NEW_ORDER;

  //time stamp
  memcpy(info->timestamp, newOrder->timestamp, TIME_LENGTH);
  
  //ecn
  strcpy(info->ecn, newOrder->ecn);
  
  //shares
  info->shares = newOrder->shares;
  
  //price
  info->price = newOrder->price;
  
  //symbol
  strncpy(info->symbol, newOrder->symbol, SYMBOL_LEN);
  
  //time in force
  strcpy(info->timeInForce, newOrder->timeInForce);
  
  //left shares
  info->leftShares = newOrder->shares;
  
  //status
  strcpy(info->status, newOrder->status);
  
  //msg seq num
  info->seqNum = newOrder->seqNum;
  
  //OrderId
  info->orderID = newOrder->orderID;
  
  //side
  strcpy(info->side, newOrder->side);
  
  //avg_price
  info->avgPrice = newOrder->price;
  
  //msg_content
  strcpy(info->msgContent, newOrder->msgContent);
  
  //entry_exit
  strcpy(info->entryExit, newOrder->entryExit);
  
  //ecn_launch
  strcpy(info->ecnLaunch, newOrder->ecnLaunch);
  
  //ask bid
  strcpy(info->askBid, newOrder->askBid);
  
  //ts_id
  info->tsId = newOrder->tsId;
  
  //cross id
  info->crossId = newOrder->crossId;
  
  //date
  memcpy(info->date, newOrder->date, 10);
  
  //trade type
  info->tradeType = newOrder->tradeType;
  
  //is iso
  info->isoFlag = newOrder->eligibility;
  
  //bboId
  info->bboId = newOrder->bboId;
  
  //account
  info->tradingAccount = newOrder->tradingAccount;
  
  info->entryExitRefNum = newOrder->entryExitRefNum;
  
  // Visible shares
  info->visibleShares = newOrder->visibleShares;
  
  //routingInst
  strcpy(info->routingInst, "");
  
  pthread_mutex_lock(&queryDataMgmt.updateMutex);
    
  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);
  
  SaveQueryDataToFile(&queryDataMgmt);
  
  pthread_mutex_unlock(&queryDataMgmt.updateMutex);
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessInsertAcceptedOrder4OUCHQueryToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertAcceptedOrder4OUCHQueryToCollection(t_OUCHAcceptedOrder *acceptedOrder)
{
  t_QueryDataBlock queryDataBlock;
  t_Order_ACK_DB *info = &queryDataBlock.detail.orderAccepted;
  
  memset(info, 0, sizeof(t_Order_ACK_DB));
  
  //queryType
  queryDataBlock.queryType = INSERT_ORDER_ACCEPTED;
  
  //OrderId
  info->orderID = acceptedOrder->orderID;
  
  //date
  memcpy(info->date, acceptedOrder->date, 10);
  
  //time stamp
  memcpy(info->timestamp, acceptedOrder->timestamp, TIME_LENGTH);
  
  memcpy(info->processedTimestamp, acceptedOrder->processedTimestamp, TIME_LENGTH);
  
  //msg seq num
  info->seqNum = acceptedOrder->seqNum;
  
  //exOrderId
  sprintf(info->exOrderId, "%lu", acceptedOrder->ouchExOrderId);
  
  //msg_content
  strcpy(info->msgContent, acceptedOrder->msgContent);
  
  //price
  info->ackPrice = acceptedOrder->price;
    
  //status
  strcpy(info->status, acceptedOrder->status);
  
  //shares
  info->ackShares = acceptedOrder->shares;
  
  pthread_mutex_lock(&queryDataMgmt.updateMutex);
    
  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);
  
  SaveQueryDataToFile(&queryDataMgmt);
  
  pthread_mutex_unlock(&queryDataMgmt.updateMutex);
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessInsertExecutedOrder4OUCHQueryToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertExecutedOrder4OUCHQueryToCollection(t_OUCHExecutedOrder *executedOrder)
{
  t_QueryDataBlock queryDataBlock;
  t_Order_Executed_DB *info = &queryDataBlock.detail.orderExecuted;
  
  memset(info, 0, sizeof(t_Order_Executed_DB));
  
  //queryType
  queryDataBlock.queryType = INSERT_ORDER_EXECUTED;
  
  //OrderId
  info->orderID = executedOrder->orderID;
  
  //date
  memcpy(info->date, executedOrder->date, 10);
  
  //shares
  info->lastShares = executedOrder->shares;
  
  //price
  info->lastPrice = executedOrder->price;
  
  //time stamp
  memcpy(info->timestamp, executedOrder->timestamp, TIME_LENGTH);
  
  memcpy(info->processedTimestamp, executedOrder->processedTimestamp, TIME_LENGTH);
  
  //msg seq num
  info->seqNum = executedOrder->seqNum;
  
  //executionId
  sprintf(info->execId, "%lu", executedOrder->executionId);
  
  //msg_content
  strcpy(info->msgContent, executedOrder->msgContent);
  
  //liquidityIndicator
  info->liquidity[0] = executedOrder->liquidityIndicator;
  
  //status
  strcpy(info->status, executedOrder->status);
  
  //leftShares
  info->leftShares = executedOrder->leftShares;
  
  //avgPrice
  info->avgPrice = executedOrder->avgPrice;
  
  //Fill fee
  info->fillFee = executedOrder->fillFee;
  
  info->entryExitRefNum = executedOrder->entryExitRefNum;

  pthread_mutex_lock(&queryDataMgmt.updateMutex);
  
  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);

  SaveQueryDataToFile(&queryDataMgmt);
  
  pthread_mutex_unlock(&queryDataMgmt.updateMutex);
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessInsertCanceledOrder4OUCHQueryToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertCanceledOrder4OUCHQueryToCollection(t_OUCHCanceledOrder *canceledOrder)
{
  t_QueryDataBlock queryDataBlock;
  t_Order_Canceled_DB *info = &queryDataBlock.detail.orderCanceled;
  
  memset(info, 0, sizeof(t_Order_Canceled_DB));
  
  //queryType
  queryDataBlock.queryType = INSERT_ORDER_CANCELED;
  
  //OrderId
  info->orderID = canceledOrder->orderID;
  
  //date
  memcpy(info->date, canceledOrder->date, 10);
  
  //time stamp
  memcpy(info->timestamp, canceledOrder->timestamp, TIME_LENGTH);
    
  memcpy(info->processedTimestamp, canceledOrder->processedTimestamp, TIME_LENGTH);
  
  //msg seq num
  info->seqNum = canceledOrder->seqNum;
  
  //msg_content
  strcpy(info->msgContent, canceledOrder->msgContent);

  //status
  strcpy(info->status, canceledOrder->status);
  
  info->entryExitRefNum = canceledOrder->entryExitRefNum;
  
  pthread_mutex_lock(&queryDataMgmt.updateMutex);
    
  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);
  
  SaveQueryDataToFile(&queryDataMgmt);
  
  pthread_mutex_unlock(&queryDataMgmt.updateMutex);
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessInsertCancelRequest4OUCHQueryToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertCancelRequest4OUCHQueryToCollection(t_OUCHCancelRequest *cancelRequest)
{
  t_QueryDataBlock queryDataBlock;
  t_Cancel_Request_DB *info = &queryDataBlock.detail.cancelRequest;
  
  memset(info, 0, sizeof(t_Cancel_Request_DB));
  
  //queryType
  queryDataBlock.queryType = INSERT_CANCEL_REQUEST;
  
  //OrderId
  info->orderID = cancelRequest->orderID;
  
  //date
  memcpy(info->date, cancelRequest->date, 10);
  
  //time stamp
  memcpy(info->timestamp, cancelRequest->timestamp, TIME_LENGTH);
  
  //msg seq num
  info->seqNum = cancelRequest->seqNum;
  
  //msg_content
  strcpy(info->msgContent, cancelRequest->msgContent);

  //status
  strcpy(info->status, cancelRequest->status);
  
  pthread_mutex_lock(&queryDataMgmt.updateMutex);
    
  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);
  
  SaveQueryDataToFile(&queryDataMgmt);
  
  pthread_mutex_unlock(&queryDataMgmt.updateMutex);
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessInsertRejectedOrder4OUCHQueryToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertRejectedOrder4OUCHQueryToCollection(t_OUCHRejectedOrder *rejectedOrder)
{
  t_QueryDataBlock queryDataBlock;
  t_Order_Rejected_DB *info = &queryDataBlock.detail.orderRejected;
  
  memset(info, 0, sizeof(t_Order_Rejected_DB));
  
  //queryType
  queryDataBlock.queryType = INSERT_ORDER_REJECTED;
  
  //OrderId
  info->orderID = rejectedOrder->orderID;
  
  //date
  memcpy(info->date, rejectedOrder->date, 10);
  
  //time stamp
  memcpy(info->timestamp, rejectedOrder->timestamp, TIME_LENGTH);
    
  memcpy(info->processedTimestamp, rejectedOrder->processedTimestamp, TIME_LENGTH);
  
  //msg seq num
  info->seqNum = rejectedOrder->seqNum;
  
  //reason
  sprintf(info->reasonCode, "%c", rejectedOrder->reason);
  
  //msg_content
  strcpy(info->msgContent, rejectedOrder->msgContent);

  //status
  strcpy(info->status, rejectedOrder->status);
  
  info->entryExitRefNum = rejectedOrder->entryExitRefNum;
  
  pthread_mutex_lock(&queryDataMgmt.updateMutex);
    
  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);
  
  SaveQueryDataToFile(&queryDataMgmt);
  
  pthread_mutex_unlock(&queryDataMgmt.updateMutex);
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessInsertBrokenTrade4OUCHQueryToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertBrokenTrade4OUCHQueryToCollection(t_OUCHBrokenOrder *brokenTrade)
{
  t_QueryDataBlock queryDataBlock;
  t_Broken_Trade_DB *info = &queryDataBlock.detail.brokenTrade;
  
  memset(info, 0, sizeof(t_Broken_Trade_DB));
  
  //queryType
  queryDataBlock.queryType = INSERT_BUST_OR_CORRECT;
  
  //OrderId
  info->orderID = brokenTrade->orderID;
  
  //date
  memcpy(info->date, brokenTrade->date, 10);
  
  //time stamp
  memcpy(info->timestamp, brokenTrade->timestamp, TIME_LENGTH);
  
  //msg seq num
  info->seqNum = brokenTrade->seqNum;
  
  //executionId
  sprintf(info->execId, "%lu", brokenTrade->executionId);
  
  //msg_content
  strcpy(info->msgContent, brokenTrade->msgContent);

  //status
  strcpy(info->status, brokenTrade->status);
  
  pthread_mutex_lock(&queryDataMgmt.updateMutex);
    
  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);
  
  SaveQueryDataToFile(&queryDataMgmt);
  
  pthread_mutex_unlock(&queryDataMgmt.updateMutex);
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessInsertCancelPending4OUCHQueryToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertCancelPending4OUCHQueryToCollection(t_OUCHCancelPending *pendingCancel)
{
  t_QueryDataBlock queryDataBlock;
  t_Cancel_Pending_DB *info = &queryDataBlock.detail.cancelPending;
  
  memset(info, 0, sizeof(t_Cancel_Pending_DB));
  
  //queryType
  queryDataBlock.queryType = INSERT_CANCEL_PENDING;
  
  //OrderId
  info->orderID = pendingCancel->orderID;
  
  //date
  memcpy(info->date, pendingCancel->date, 10);
  
  //time stamp
  memcpy(info->timestamp, pendingCancel->timestamp, TIME_LENGTH);
  
  //msg seq num
  info->seqNum = pendingCancel->seqNum;
  
  //msg_content
  strcpy(info->msgContent, pendingCancel->msgContent);

  //status
  strcpy(info->status, pendingCancel->status);
  
  pthread_mutex_lock(&queryDataMgmt.updateMutex);
    
  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);
  
  SaveQueryDataToFile(&queryDataMgmt);
  
  pthread_mutex_unlock(&queryDataMgmt.updateMutex);
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessInsertCancelReject4OUCHQueryToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertCancelReject4OUCHQueryToCollection(t_OUCHCancelReject *rejectedCancel)
{
  t_QueryDataBlock queryDataBlock;
  t_Cancel_Rejected_DB *info = &queryDataBlock.detail.cancelRejected;
  
  memset(info, 0, sizeof(t_Cancel_Rejected_DB));
  
  //queryType
  queryDataBlock.queryType = INSERT_CANCEL_REJECTED;
  
  //OrderId
  info->orderID = rejectedCancel->orderID;
  
  //date
  memcpy(info->date, rejectedCancel->date, 10);
  
  //time stamp
  memcpy(info->timestamp, rejectedCancel->timestamp, TIME_LENGTH);
  
  //msg seq num
  info->seqNum = rejectedCancel->seqNum;
  
  //msg_content
  strcpy(info->msgContent, rejectedCancel->msgContent);

  //status
  strcpy(info->status, rejectedCancel->status);
  
  pthread_mutex_lock(&queryDataMgmt.updateMutex);
    
  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);
  
  SaveQueryDataToFile(&queryDataMgmt);
  
  pthread_mutex_unlock(&queryDataMgmt.updateMutex);
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessInsertNewOrder4RASHQueryToCollection
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertNewOrder4RASHQueryToCollection(t_RASHNewOrder *newOrder)
{
  t_QueryDataBlock queryDataBlock;
  t_New_Order_DB *info = &queryDataBlock.detail.newOrder;
  
  memset(info, 0, sizeof(t_New_Order_DB));
  
  //queryType
  queryDataBlock.queryType = INSERT_NEW_ORDER;
  
  //time stamp
  memcpy(info->timestamp, newOrder->timestamp, TIME_LENGTH);
  
  //ecn
  strcpy(info->ecn, newOrder->ecn);
  
  //shares
  info->shares = newOrder->shares;
  
  //price
  info->price = newOrder->price;
  
  //symbol
  strncpy(info->symbol, newOrder->symbol, SYMBOL_LEN);
  
  //time in force
  strcpy(info->timeInForce, newOrder->timeInForce);
  
  //left shares
  info->leftShares = newOrder->shares;
  
  //status
  strcpy(info->status, newOrder->status);
  
  //msg seq num
  info->seqNum = newOrder->seqNum;
  
  //OrderId
  info->orderID = newOrder->orderID;
  
  //side
  strcpy(info->side, newOrder->side);
  
  //avg_price
  info->avgPrice = newOrder->price;
  
  //msg_content
  strcpy(info->msgContent, newOrder->msgContent);
  
  //entry_exit
  strcpy(info->entryExit, newOrder->entryExit);
  
  //ecn_launch
  strcpy(info->ecnLaunch, newOrder->ecnLaunch);
  
  //ask bid
  strcpy(info->askBid, newOrder->askBid);
  
  //ts_id
  info->tsId = newOrder->tsId;
  
  //cross id
  info->crossId = newOrder->crossId;
  
  //date
  memcpy(info->date, newOrder->date, 10);
  
  //trade type
  info->tradeType = newOrder->tradeType;
  
  //is iso
  info->isoFlag = newOrder->eligibility;
  
  //bboId
  info->bboId = newOrder->bboId;
  
  //account
  info->tradingAccount = newOrder->tradingAccount;
  
  info->entryExitRefNum = newOrder->entryExitRefNum;
  
  // Visible shares
  info->visibleShares = newOrder->visibleShares;
  
  //routingInst
  strcpy(info->routingInst, "");
  
  pthread_mutex_lock(&queryDataMgmt.updateMutex);
    
  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);
  
  SaveQueryDataToFile(&queryDataMgmt);
  
  pthread_mutex_unlock(&queryDataMgmt.updateMutex);
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessInsertAcceptedOrder4RASHQueryToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertAcceptedOrder4RASHQueryToCollection(t_RASHAcceptedOrder *acceptedOrder)
{
  t_QueryDataBlock queryDataBlock;
  t_Order_ACK_DB *info = &queryDataBlock.detail.orderAccepted;
  
  memset(info, 0, sizeof(t_Order_ACK_DB));
  
  //queryType
  queryDataBlock.queryType = INSERT_ORDER_ACCEPTED;
  
  //OrderId
  info->orderID = acceptedOrder->orderID;
  
  //date
  memcpy(info->date, acceptedOrder->date, 10);
  
  //time stamp
  memcpy(info->timestamp, acceptedOrder->timestamp, TIME_LENGTH);
  
  memcpy(info->processedTimestamp, acceptedOrder->processedTimestamp, TIME_LENGTH);
  
  //msg seq num
  info->seqNum = acceptedOrder->seqNum;
  
  //exOrderId
  sprintf(info->exOrderId, "%u", acceptedOrder->rashExOrderId);
  
  //msg_content
  strcpy(info->msgContent, acceptedOrder->msgContent);
  
  //price
  info->ackPrice = acceptedOrder->price;
  
  //status
  strcpy(info->status, acceptedOrder->status);
  
  //shares
  info->ackShares = acceptedOrder->shares;
  
  pthread_mutex_lock(&queryDataMgmt.updateMutex);
    
  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);
  
  SaveQueryDataToFile(&queryDataMgmt);
  
  pthread_mutex_unlock(&queryDataMgmt.updateMutex);
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessInsertExecutedOrder4RASHQueryToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertExecutedOrder4RASHQueryToCollection(t_RASHExecutedOrder *executedOrder)
{
  t_QueryDataBlock queryDataBlock;
  t_Order_Executed_DB *info = &queryDataBlock.detail.orderExecuted;
  
  memset(info, 0, sizeof(t_Order_Executed_DB));
  
  //queryType
  queryDataBlock.queryType = INSERT_ORDER_EXECUTED;
  
  //OrderId
  info->orderID = executedOrder->orderID;
  
  //date
  memcpy(info->date, executedOrder->date, 10);
  
  //shares
  info->lastShares = executedOrder->shares;
  
  //price
  info->lastPrice = executedOrder->price;
  
  //time stamp
  memcpy(info->timestamp, executedOrder->timestamp, TIME_LENGTH);
  
  memcpy(info->processedTimestamp, executedOrder->processedTimestamp, TIME_LENGTH);
  
  //msg seq num
  info->seqNum = executedOrder->seqNum;
  
  //executionId
  sprintf(info->execId, "%u", executedOrder->executionId);
  
  //msg_content
  strcpy(info->msgContent, executedOrder->msgContent);
  
  //liquidityIndicator
  info->liquidity[0] = executedOrder->liquidityIndicator;
  
  //status
  strcpy(info->status, executedOrder->status);
  
  //leftShares
  info->leftShares = executedOrder->leftShares;
  
  //avgPrice
  info->avgPrice = executedOrder->avgPrice;
  
  //Fill fee
  info->fillFee = executedOrder->fillFee;
  
  info->entryExitRefNum = executedOrder->entryExitRefNum;
  
  pthread_mutex_lock(&queryDataMgmt.updateMutex);
    
  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);
  
  SaveQueryDataToFile(&queryDataMgmt);
  
  pthread_mutex_unlock(&queryDataMgmt.updateMutex);
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessInsertCanceledOrder4RASHQueryToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertCanceledOrder4RASHQueryToCollection(t_RASHCanceledOrder *canceledOrder)
{
  t_QueryDataBlock queryDataBlock;
  t_Order_Canceled_DB *info = &queryDataBlock.detail.orderCanceled;
  
  memset(info, 0, sizeof(t_Order_Canceled_DB));
  
  //queryType
  queryDataBlock.queryType = INSERT_ORDER_CANCELED;
  
  //OrderId
  info->orderID = canceledOrder->orderID;
  
  //date
  memcpy(info->date, canceledOrder->date, 10);
  
  //time stamp
  memcpy(info->timestamp, canceledOrder->timestamp, TIME_LENGTH);
    
  memcpy(info->processedTimestamp, canceledOrder->processedTimestamp, TIME_LENGTH);
  
  //msg seq num
  info->seqNum = canceledOrder->seqNum;
  
  //msg_content
  strcpy(info->msgContent, canceledOrder->msgContent);

  //status
  strcpy(info->status, canceledOrder->status);
  
  info->entryExitRefNum = canceledOrder->entryExitRefNum;
  
  pthread_mutex_lock(&queryDataMgmt.updateMutex);
    
  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);
  
  SaveQueryDataToFile(&queryDataMgmt);
  
  pthread_mutex_unlock(&queryDataMgmt.updateMutex);
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessInsertCancelRequest4RASHQueryToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertCancelRequest4RASHQueryToCollection(t_RASHCancelRequest *cancelRequest)
{
  t_QueryDataBlock queryDataBlock;
  t_Cancel_Request_DB *info = &queryDataBlock.detail.cancelRequest;
  
  memset(info, 0, sizeof(t_Cancel_Request_DB));
  
  //queryType
  queryDataBlock.queryType = INSERT_CANCEL_REQUEST;
  
  //OrderId
  info->orderID = cancelRequest->orderID;
  
  //date
  memcpy(info->date, cancelRequest->date, 10);
  
  //time stamp
  memcpy(info->timestamp, cancelRequest->timestamp, TIME_LENGTH);
  
  //msg seq num
  info->seqNum = cancelRequest->seqNum;
  
  //msg_content
  strcpy(info->msgContent, cancelRequest->msgContent);

  //status
  strcpy(info->status, cancelRequest->status);
  
  pthread_mutex_lock(&queryDataMgmt.updateMutex);
    
  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);
  
  SaveQueryDataToFile(&queryDataMgmt);
  
  pthread_mutex_unlock(&queryDataMgmt.updateMutex);
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessInsertRejectedOrder4RASHQueryToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertRejectedOrder4RASHQueryToCollection(t_RASHRejectedOrder *rejectedOrder)
{
  t_QueryDataBlock queryDataBlock;
  t_Order_Rejected_DB *info = &queryDataBlock.detail.orderRejected;
  
  memset(info, 0, sizeof(t_Order_Rejected_DB));
  
  //queryType
  queryDataBlock.queryType = INSERT_ORDER_REJECTED;
  
  //OrderId
  info->orderID = rejectedOrder->orderID;
  
  //date
  memcpy(info->date, rejectedOrder->date, 10);
  
  //time stamp
  memcpy(info->timestamp, rejectedOrder->timestamp, TIME_LENGTH);
  
  memcpy(info->processedTimestamp, rejectedOrder->processedTimestamp, TIME_LENGTH);
    
  //msg seq num
  info->seqNum = rejectedOrder->seqNum;
  
  //reason
  sprintf(info->reasonCode, "%c", rejectedOrder->reason);
  
  //msg_content
  strcpy(info->msgContent, rejectedOrder->msgContent);

  //status
  strcpy(info->status, rejectedOrder->status);
  
  info->entryExitRefNum = rejectedOrder->entryExitRefNum;
  
  pthread_mutex_lock(&queryDataMgmt.updateMutex);
    
  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);
  
  SaveQueryDataToFile(&queryDataMgmt);
  
  pthread_mutex_unlock(&queryDataMgmt.updateMutex);
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessInsertBrokenTrade4RASHQueryToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertBrokenTrade4RASHQueryToCollection(t_RASHBrokenOrder *brokenTrade)
{
  t_QueryDataBlock queryDataBlock;
  t_Broken_Trade_DB *info = &queryDataBlock.detail.brokenTrade;
  
  memset(info, 0, sizeof(t_Broken_Trade_DB));
  
  //queryType
  queryDataBlock.queryType = INSERT_BUST_OR_CORRECT;
  
  //OrderId
  info->orderID = brokenTrade->orderID;
  
  //date
  memcpy(info->date, brokenTrade->date, 10);
  
  //time stamp
  memcpy(info->timestamp, brokenTrade->timestamp, TIME_LENGTH);
  
  //msg seq num
  info->seqNum = brokenTrade->seqNum;
  
  //executionId
  sprintf(info->execId, "%u", brokenTrade->executionId);
  
  //msg_content
  strcpy(info->msgContent, brokenTrade->msgContent);

  //status
  strcpy(info->status, brokenTrade->status);
  
  pthread_mutex_lock(&queryDataMgmt.updateMutex);
    
  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);
  
  SaveQueryDataToFile(&queryDataMgmt);
  
  pthread_mutex_unlock(&queryDataMgmt.updateMutex);
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessInsertNewOrder4NYSE_CCGQueryToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertNewOrder4NYSE_CCGQueryToCollection(t_NYSE_CCGNewOrder *newOrder)
{
  t_QueryDataBlock queryDataBlock;
  t_New_Order_DB *info = &queryDataBlock.detail.newOrder;
  
  memset(info, 0, sizeof(t_New_Order_DB));

  //queryType
  queryDataBlock.queryType = INSERT_NEW_ORDER;

  //time stamp
  memcpy(info->timestamp, newOrder->timestamp, TIME_LENGTH);

  //ecn
  strcpy(info->ecn, newOrder->ecn);

  //shares
  info->shares = newOrder->shares;

  //price
  info->price = newOrder->price;

  //symbol
  strncpy(info->symbol, newOrder->symbol, SYMBOL_LEN);

  //time in force
  strcpy(info->timeInForce, newOrder->timeInForce);

  //left shares
  info->leftShares = newOrder->shares;

  //status
  strcpy(info->status, newOrder->status);

  //msg seq num
  info->seqNum = newOrder->seqNum;

  //OrderId
  info->orderID = newOrder->clOrderID;

  //side
  strcpy(info->side, newOrder->side);

  //avg_price
  info->avgPrice = newOrder->price;

  //msg_content
  strcpy(info->msgContent, newOrder->msgContent);

  //entry_exit
  strcpy(info->entryExit, newOrder->entryExit);

  //ecn_launch
  strcpy(info->ecnLaunch, newOrder->ecnLaunch);

  //ask bid
  strcpy(info->askBid, newOrder->askBid);

  //ts_id
  info->tsId = newOrder->tsId;

  //cross id
  info->crossId = newOrder->crossId;

  //date
  memcpy(info->date, newOrder->date, 10);

  //trade type
  info->tradeType = newOrder->tradeType;

  //is iso
  info->isoFlag = newOrder->isoFlag == 'I' ? 'Y' : 'N';

  //bboId
  info->bboId = newOrder->bboId;

  //trading account
  info->tradingAccount = newOrder->tradingAccount;

  info->entryExitRefNum = newOrder->entryExitRefNum;
  
  // Visible shares
  info->visibleShares = newOrder->visibleShares;
  
  //routingInst
  strcpy(info->routingInst, "");
  
  pthread_mutex_lock(&queryDataMgmt.updateMutex);

  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);

  SaveQueryDataToFile(&queryDataMgmt);

  pthread_mutex_unlock(&queryDataMgmt.updateMutex);

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessInsertACKOrder4NYSE_CCGQueryToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertACKOrder4NYSE_CCGQueryToCollection(t_NYSE_CCGACKOrder *orderACK)
{
  t_QueryDataBlock queryDataBlock;
  t_Order_ACK_DB *info = &queryDataBlock.detail.orderAccepted;
  
  memset(info, 0, sizeof(t_Order_ACK_DB));

  //queryType
  queryDataBlock.queryType = INSERT_ORDER_ACCEPTED;

  //OrderId
  info->orderID = orderACK->clOrderID;

  //date
  memcpy(info->date, orderACK->date, 10);

  //time stamp
  memcpy(info->timestamp, orderACK->timestamp, TIME_LENGTH);
  
  memcpy(info->processedTimestamp, orderACK->processedTimestamp, TIME_LENGTH);

  //msg seq num
  info->seqNum = orderACK->seqNum;

  //nyse exec ID
  sprintf(info->exOrderId, "%lu", orderACK->meOrderID);

  //msg_content
  strcpy(info->msgContent, orderACK->msgContent);

  info->ackPrice = orderACK->price;
  
  info->ackShares = 0;

  //status
  strcpy(info->status, orderACK->status);

  pthread_mutex_lock(&queryDataMgmt.updateMutex);

  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);

  SaveQueryDataToFile(&queryDataMgmt);

  pthread_mutex_unlock(&queryDataMgmt.updateMutex);

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessInsertCanceledOrder4NYSE_CCGQueryToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertCanceledOrder4NYSE_CCGQueryToCollection(t_NYSE_CCGCanceledOrder *canceledOrder)
{
  t_QueryDataBlock queryDataBlock;
  t_Order_Canceled_DB *info = &queryDataBlock.detail.orderCanceled;
  
  memset(info, 0, sizeof(t_Order_Canceled_DB));

  //queryType
  queryDataBlock.queryType = INSERT_ORDER_CANCELED;

  //OrderId
  info->orderID = canceledOrder->origClOrderID;

  //date
  memcpy(info->date, canceledOrder->date, 10);

  //time stamp
  memcpy(info->timestamp, canceledOrder->timestamp, TIME_LENGTH);
    
  memcpy(info->processedTimestamp, canceledOrder->processedTimestamp, TIME_LENGTH);

  //msg seq num
  info->seqNum = canceledOrder->seqNum;

  // NYSE order ID
  sprintf(info->exOrderId, "%lu", canceledOrder->meOrderID);

  //msg_content
  strcpy(info->msgContent, canceledOrder->msgContent);

  //status
  strcpy(info->status, canceledOrder->status);

  info->entryExitRefNum = canceledOrder->entryExitRefNum;
  
  pthread_mutex_lock(&queryDataMgmt.updateMutex);

  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);

  SaveQueryDataToFile(&queryDataMgmt);

  pthread_mutex_unlock(&queryDataMgmt.updateMutex);

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessInsertRejectOrder4NYSE_CCGQueryToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertRejectOrder4NYSE_CCGQueryToCollection(t_NYSE_CCGRejectedOrder *reject)
{
  t_QueryDataBlock queryDataBlock;

  if (reject->type == '1') // Order rejected
  {
    t_Order_Rejected_DB *info = &queryDataBlock.detail.orderRejected;
    
    memset(info, 0, sizeof(t_Order_Rejected_DB));
    
    //queryType
    queryDataBlock.queryType = INSERT_ORDER_REJECTED;

    //OrderId
    info->orderID = reject->clOrderID;

    //date
    memcpy(info->date, reject->date, 10);

    //time stamp
    memcpy(info->timestamp, reject->timestamp, TIME_LENGTH);
      
    memcpy(info->processedTimestamp, reject->processedTimestamp, TIME_LENGTH);

    //msg seq num
    info->seqNum = reject->seqNum;

    //text
    ConvertSpecialCharacter(reject->text, info->reasonText);

    //msg_content
    strcpy(info->msgContent, reject->msgContent);

    //status
    strcpy(info->status, reject->status);

    info->entryExitRefNum = reject->entryExitRefNum;
  } 
  else // Cancel rejected
  {
    t_Cancel_Rejected_DB *info = &queryDataBlock.detail.cancelRejected;
  
    memset(info, 0, sizeof(t_Cancel_Rejected_DB));
    
    //queryType
    queryDataBlock.queryType = INSERT_CANCEL_REJECTED;
    
    //OrderId
    info->orderID = reject->clOrderID;
    
    //date
    memcpy(info->date, reject->date, 10);
    
    //time stamp
    memcpy(info->timestamp, reject->timestamp, TIME_LENGTH);
    
    //msg seq num
    info->seqNum = reject->seqNum;
    
    //text
    ConvertSpecialCharacter(reject->text, info->reasonText);

    //msg_content
    strcpy(info->msgContent, reject->msgContent);

    //status
    strcpy(info->status, reject->status);
  }
  
  pthread_mutex_lock(&queryDataMgmt.updateMutex);

  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);

  SaveQueryDataToFile(&queryDataMgmt);

  pthread_mutex_unlock(&queryDataMgmt.updateMutex);

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessInsertFillOrder4NYSE_CCGQueryToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertFillOrder4NYSE_CCGQueryToCollection(t_NYSE_CCGFillOrder *fillOrder)
{
  t_QueryDataBlock queryDataBlock;
  t_Order_Executed_DB *info = &queryDataBlock.detail.orderExecuted;
  
  memset(info, 0, sizeof(t_Order_Executed_DB));

  //queryType
  queryDataBlock.queryType = INSERT_ORDER_EXECUTED;

  //OrderId
  info->orderID = fillOrder->clOrderID;

  //date
  memcpy(info->date, fillOrder->date, 10);

  //shares
  info->lastShares = fillOrder->lastShares;

  //price
  info->lastPrice = fillOrder->lastPrice;

  //time stamp
  memcpy(info->timestamp, fillOrder->timestamp, TIME_LENGTH);
  
  memcpy(info->processedTimestamp, fillOrder->processedTimestamp, TIME_LENGTH);

  //msg seq num
  info->seqNum = fillOrder->seqNum;

  //ecnOrderID
  sprintf(info->exOrderId,"%lu", fillOrder->meOrderID);

  //executionId
  sprintf(info->execId, "%lu", fillOrder->execID);

  //msg_content
  strcpy(info->msgContent, fillOrder->msgContent);

  //liquidityIndicator
  info->liquidity[0] = fillOrder->billingIndicator;

  //status
  strcpy(info->status, fillOrder->status);

  //leftShares
  info->leftShares = fillOrder->leavesShares;

  //avgPrice
  info->avgPrice = fillOrder->avgPrice;

  //Fill fee
  info->fillFee = fillOrder->fillFee;
  
  info->entryExitRefNum = fillOrder->entryExitRefNum;

  pthread_mutex_lock(&queryDataMgmt.updateMutex);

  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);

  SaveQueryDataToFile(&queryDataMgmt);

  pthread_mutex_unlock(&queryDataMgmt.updateMutex);

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessInsertCancelRequest4NYSE_CCGQueryToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertCancelRequest4NYSE_CCGQueryToCollection(t_NYSE_CCGCancelRequest *cancelRequest)
{
  t_QueryDataBlock queryDataBlock;
  t_Cancel_Request_DB *info = &queryDataBlock.detail.cancelRequest;
  
  memset(info, 0, sizeof(t_Cancel_Request_DB));

  //queryType
  queryDataBlock.queryType = INSERT_CANCEL_REQUEST;

  //OrderId
  info->orderID = cancelRequest->origClOrderID;

  //date
  memcpy(info->date, cancelRequest->date, 10);

  //time stamp
  memcpy(info->timestamp, cancelRequest->timestamp, TIME_LENGTH);

  //msg seq num
  info->seqNum = cancelRequest->seqNum;

  // NYSE orderid
  sprintf(info->exOrderId, "%lu", cancelRequest->meOrderID);

  //msg_content
  strcpy(info->msgContent, cancelRequest->msgContent);

  //status
  strcpy(info->status, cancelRequest->status);

  pthread_mutex_lock(&queryDataMgmt.updateMutex);

  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);

  SaveQueryDataToFile(&queryDataMgmt);

  pthread_mutex_unlock(&queryDataMgmt.updateMutex);

  return 0;
}

/****************************************************************************
- Function name:  ProcessInsertCancelACK4NYSE_CCGQueryToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertCancelACK4NYSE_CCGQueryToCollection(t_NYSE_CCGCancelACK *cancelACK)
{
  t_QueryDataBlock queryDataBlock;
  t_Cancel_ACK_DB *info = &queryDataBlock.detail.cancelACK;
  
  memset(info, 0, sizeof(t_Cancel_ACK_DB));

  //queryType
  queryDataBlock.queryType = INSERT_CANCEL_ACK;

  //OrderId
  info->orderID = cancelACK->clOrderID;

  //date
  memcpy(info->date, cancelACK->date, 10);

  //time stamp
  memcpy(info->timestamp, cancelACK->timestamp, TIME_LENGTH);

  //msg seq num
  info->seqNum = cancelACK->seqNum;

  //NYSE orderid
  sprintf(info->exOrderId, "%lu", cancelACK->meOrderID);

  //msg_content
  strcpy(info->msgContent, cancelACK->msgContent);

  //status
  strcpy(info->status, cancelACK->status);

  pthread_mutex_lock(&queryDataMgmt.updateMutex);

  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);

  SaveQueryDataToFile(&queryDataMgmt);

  pthread_mutex_unlock(&queryDataMgmt.updateMutex);

  return 0;
}

/****************************************************************************
- Function name:  ProcessInsertBustOrCorrect4NYSE_CCGQueryToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertBustOrCorrect4NYSE_CCGQueryToCollection(t_NYSE_CCGBustOrCorrect *brokenTrade)
{
  t_QueryDataBlock queryDataBlock;
  t_Broken_Trade_DB *info = &queryDataBlock.detail.brokenTrade;
  
  memset(info, 0, sizeof(t_Broken_Trade_DB));

  //queryType
  queryDataBlock.queryType = INSERT_BUST_OR_CORRECT;

  //OrderId
  info->orderID = brokenTrade->clOrderID;

  //date
  memcpy(info->date, brokenTrade->date, 10);

  //shares
  info->shares = brokenTrade->shares;

  //price
  info->price = brokenTrade->price;

  //time stamp
  memcpy(info->timestamp, brokenTrade->timestamp, TIME_LENGTH);

  //msg seq num
  info->seqNum = brokenTrade->seqNum;

  //executionId
  sprintf(info->execId, "%lu", brokenTrade->execId);

  //msg_content
  strcpy(info->msgContent, brokenTrade->msgContent);

  //status
  strcpy(info->status, brokenTrade->status);

  pthread_mutex_lock(&queryDataMgmt.updateMutex);

  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);

  SaveQueryDataToFile(&queryDataMgmt);

  pthread_mutex_unlock(&queryDataMgmt.updateMutex);

  return 0;
}

/****************************************************************************
- Function name:  ProcessInsertNewOrder4BATS_BOEQueryToCollection
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertNewOrder4BATS_BOEQueryToCollection(t_BATS_BOENewOrder *newOrder)
{
  t_QueryDataBlock queryDataBlock;
  t_New_Order_DB *info = &queryDataBlock.detail.newOrder;
  
  memset(info, 0, sizeof(t_New_Order_DB));
  
  //queryType
  queryDataBlock.queryType = INSERT_NEW_ORDER;
  
  //time stamp
  memcpy(info->timestamp, newOrder->timestamp, TIME_LENGTH);
  
  //ecn
  strcpy(info->ecn, newOrder->ecn);
  
  //shares
  info->shares = newOrder->shares;
  
  //price
  info->price = newOrder->price;
    
  //symbol
  strncpy(info->symbol, newOrder->symbol, SYMBOL_LEN);
  
  //time in force
  strcpy(info->timeInForce, newOrder->timeInForce);
  
  //left shares
  info->leftShares = newOrder->shares;
  
  //status
  strcpy(info->status, newOrder->status);
  
  //msg seq num
  info->seqNum = newOrder->seqNum;
  
  //OrderId
  info->orderID = newOrder->orderID;
  
  //side
  strcpy(info->side, newOrder->side);
  
  //avg_price
  info->avgPrice = newOrder->price;
  
  //msg_content
  strcpy(info->msgContent, newOrder->msgContent);
  
  //entry_exit
  strcpy(info->entryExit, newOrder->entryExit);
  
  //ecn_launch
  strcpy(info->ecnLaunch, newOrder->ecnLaunch);
  
  //ask bid
  strcpy(info->askBid, newOrder->askBid);
  
  //routingInst
  strncpy(info->routingInst, newOrder->routingInst, 4);
  
  //ts_id
  info->tsId = newOrder->tsId;
  
  //cross id
  info->crossId = newOrder->crossId;
  
  //date
  memcpy(info->date, newOrder->date, 10);
  
  //trade type
  info->tradeType = newOrder->tradeType;
  
  //is iso
  info->isoFlag = newOrder->isoFlag;
  
  //bboId
  info->bboId = newOrder->bboId;

  //trading account
  info->tradingAccount = newOrder->tradingAccount;
  
  //entry-exit ref num
  info->entryExitRefNum = newOrder->entryExitRefNum;
  
  // Visible shares
  info->visibleShares = newOrder->visibleShares;
  
  //routingInst
  strncpy(info->routingInst, newOrder->routingInst, 4);

  pthread_mutex_lock(&queryDataMgmt.updateMutex);
    
  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);
  
  SaveQueryDataToFile(&queryDataMgmt);
  
  pthread_mutex_unlock(&queryDataMgmt.updateMutex);
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessInsertAcceptedOrder4BATS_BOEQueryToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertAcceptedOrder4BATS_BOEQueryToCollection(t_BATS_BOEAcceptedOrder *acceptedOrder)
{
  t_QueryDataBlock queryDataBlock;
  t_Order_ACK_DB *info = &queryDataBlock.detail.orderAccepted;
  
  memset(info, 0, sizeof(t_Order_ACK_DB));
  
  //queryType
  queryDataBlock.queryType = INSERT_ORDER_ACCEPTED;

  //OrderId
  info->orderID = acceptedOrder->clOrderID;
  
  //date
  memcpy(info->date, acceptedOrder->date, 10);
  
  //time stamp
  memcpy(info->timestamp, acceptedOrder->timestamp, TIME_LENGTH);
  
  memcpy(info->processedTimestamp, acceptedOrder->processedTimestamp, TIME_LENGTH);
  
  //msg seq num
  info->seqNum = acceptedOrder->seqNum;
  
  //exOrderId
  sprintf(info->exOrderId, "%lu", acceptedOrder->batsExOrderId);
  
  //msg_content
  strcpy(info->msgContent, acceptedOrder->msgContent);
  
  //ackPrice
  info->ackPrice = acceptedOrder->price;
    
  //status
  strcpy(info->status, acceptedOrder->status);
  
  //shares
  info->ackShares = acceptedOrder->shares;
  
  pthread_mutex_lock(&queryDataMgmt.updateMutex);
    
  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);
  
  SaveQueryDataToFile(&queryDataMgmt);
  
  pthread_mutex_unlock(&queryDataMgmt.updateMutex);
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessInsertFillOrder4BATS_BOEQueryToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertFillOrder4BATS_BOEQueryToCollection(t_BATS_BOEExecutedOrder *fillOrder)
{
  t_QueryDataBlock queryDataBlock;
  t_Order_Executed_DB *info = &queryDataBlock.detail.orderExecuted;
  
  memset(info, 0, sizeof(t_Order_Executed_DB));
  
  //queryType
  queryDataBlock.queryType = INSERT_ORDER_EXECUTED;

  //OrderId
  info->orderID = fillOrder->clOrderID;

  //date
  memcpy(info->date, fillOrder->date, 10);
  
  //shares
  info->lastShares = fillOrder->lastShares;

  //price
  info->lastPrice = fillOrder->lastPx;

  //time stamp
  memcpy(info->timestamp, fillOrder->timestamp, TIME_LENGTH);
  
  memcpy(info->processedTimestamp, fillOrder->processedTimestamp, TIME_LENGTH);
  
  //msg seq num
  info->seqNum = fillOrder->seqNum;

  //ECN Order ID
  sprintf(info->exOrderId, "%lu", fillOrder->batsExOrderId);

  //executionId
  sprintf(info->execId, "%lu", fillOrder->execID);

  //msg_content
  strcpy(info->msgContent, fillOrder->msgContent);

  //liquidityIndicator
  info->liquidity[0] = fillOrder->baseLiquidityIndicator;

  //status
  strcpy(info->status, fillOrder->status);

  //leftShares
  info->leftShares = fillOrder->leavesShares;

  //avgPrice
  info->avgPrice = fillOrder->avgPrice;
  
  //Fill fee
  info->fillFee = fillOrder->fillFee;

  info->entryExitRefNum = fillOrder->entryExitRefNum;

  pthread_mutex_lock(&queryDataMgmt.updateMutex);
    
  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);
  
  SaveQueryDataToFile(&queryDataMgmt);
  
  pthread_mutex_unlock(&queryDataMgmt.updateMutex);

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessInsertCanceledOrder4BATS_BOEQueryToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertCanceledOrder4BATS_BOEQueryToCollection(t_BATS_BOECanceledOrder *canceledOrder)
{
  t_QueryDataBlock queryDataBlock;
  t_Order_Canceled_DB *info = &queryDataBlock.detail.orderCanceled;
  
  memset(info, 0, sizeof(t_Order_Canceled_DB));
  
  //queryType
  queryDataBlock.queryType = INSERT_ORDER_CANCELED;
  
  //OrderId
  info->orderID = canceledOrder->clOrderId;
  
  //date
  memcpy(info->date, canceledOrder->date, 10);
  
  //time stamp
  memcpy(info->timestamp, canceledOrder->timestamp, TIME_LENGTH);
    
  memcpy(info->processedTimestamp, canceledOrder->processedTimestamp, TIME_LENGTH);
  
  //msg seq num
  info->seqNum = canceledOrder->seqNum;
  
  //External Order ID
  sprintf(info->exOrderId, "%lu", canceledOrder->batsExOrderId);

  //msg_content
  strcpy(info->msgContent, canceledOrder->msgContent);

  //status
  strcpy(info->status, canceledOrder->status);

  info->entryExitRefNum = canceledOrder->entryExitRefNum;
  
  pthread_mutex_lock(&queryDataMgmt.updateMutex);
    
  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);
  
  SaveQueryDataToFile(&queryDataMgmt);
  
  pthread_mutex_unlock(&queryDataMgmt.updateMutex);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessInsertCancelRequest4BATS_BOEQueryToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertCancelRequest4BATS_BOEQueryToCollection(t_BATS_BOECancelRequest *cancelRequest)
{
  t_QueryDataBlock queryDataBlock;
  t_Cancel_Request_DB *info = &queryDataBlock.detail.cancelRequest;
  
  memset(info, 0, sizeof(t_Cancel_Request_DB));
  
  //queryType
  queryDataBlock.queryType = INSERT_CANCEL_REQUEST;
  
  //OrderId
  info->orderID = cancelRequest->orgClOrderID;
  
  //date
  memcpy(info->date, cancelRequest->date, 10);
  
  //time stamp
  memcpy(info->timestamp, cancelRequest->timestamp, TIME_LENGTH);
  
  //msg seq num
  info->seqNum = cancelRequest->seqNum;
  
  //ECN assigned order ID
  sprintf(info->exOrderId, "%lu", cancelRequest->batsExOrderId);
  
  //msg_content
  strcpy(info->msgContent, cancelRequest->msgContent);

  //status
  strcpy(info->status, cancelRequest->status);
  
  pthread_mutex_lock(&queryDataMgmt.updateMutex);
    
  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);
  
  SaveQueryDataToFile(&queryDataMgmt);
  
  pthread_mutex_unlock(&queryDataMgmt.updateMutex);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessInsertRejectOrder4BATS_BOEQueryToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertRejectOrder4BATS_BOEQueryToCollection(t_BATS_BOERejectedOrder *rejectedOrder)
{
  t_QueryDataBlock queryDataBlock;
  t_Order_Rejected_DB *info = &queryDataBlock.detail.orderRejected;
  
  memset(info, 0, sizeof(t_Order_Rejected_DB));
  
  //queryType
  queryDataBlock.queryType = INSERT_ORDER_REJECTED;
  
  //OrderId
  info->orderID = rejectedOrder->clOrderID;
  
  //date
  memcpy(info->date, rejectedOrder->date, 10);
  
  //time stamp
  memcpy(info->timestamp, rejectedOrder->timestamp, TIME_LENGTH);
    
  memcpy(info->processedTimestamp, rejectedOrder->processedTimestamp, TIME_LENGTH);
  
  //msg seq num
  info->seqNum = rejectedOrder->seqNum;
  
  //reason
  sprintf(info->reasonCode, "%c", rejectedOrder->reason);
  
  //text
  ConvertSpecialCharacter(rejectedOrder->text, info->reasonText);

  //ECN assigned order ID
  sprintf(info->exOrderId, "%lu", rejectedOrder->batsExOrderId);

  //msg_content
  strcpy(info->msgContent, rejectedOrder->msgContent);

  //status
  strcpy(info->status, rejectedOrder->status);
  
  info->entryExitRefNum = rejectedOrder->entryExitRefNum;

  pthread_mutex_lock(&queryDataMgmt.updateMutex);
    
  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);
  
  SaveQueryDataToFile(&queryDataMgmt);
  
  pthread_mutex_unlock(&queryDataMgmt.updateMutex);
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessInsertRejectCancel4BATS_BOEQueryToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertRejectCancel4BATS_BOEQueryToCollection(t_BATS_BOERejectedCancel *rejectedCancel)
{
  t_QueryDataBlock queryDataBlock;
  t_Cancel_Rejected_DB *info = &queryDataBlock.detail.cancelRejected;
  
  memset(info, 0, sizeof(t_Cancel_Rejected_DB));
  
  //queryType
  queryDataBlock.queryType = INSERT_CANCEL_REJECTED;
  
  //OrderId
  info->orderID = rejectedCancel->orderID;
  
  //date
  memcpy(info->date, rejectedCancel->date, 10);
  
  //time stamp
  memcpy(info->timestamp, rejectedCancel->timestamp, TIME_LENGTH);
  
  //msg seq num
  info->seqNum = rejectedCancel->seqNum;
  
  //ECN assigned order id
  sprintf(info->exOrderId, "%lu", rejectedCancel->batsExOrderId);
  
  //reason
  sprintf(info->reasonCode, "%c", rejectedCancel->reason);
  
  //text
  ConvertSpecialCharacter(rejectedCancel->text, info->reasonText);

  //msg_content
  strcpy(info->msgContent, rejectedCancel->msgContent);

  //status
  strcpy(info->status, rejectedCancel->status);
  
  pthread_mutex_lock(&queryDataMgmt.updateMutex);
    
  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);
  
  SaveQueryDataToFile(&queryDataMgmt);
  
  pthread_mutex_unlock(&queryDataMgmt.updateMutex);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessInsertBustOrCorrect4BATS_BOEQueryToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertBustOrCorrect4BATS_BOEQueryToCollection(t_BATS_BOEBrokenOrder *brokenTrade)
{
  t_QueryDataBlock queryDataBlock;
  t_Broken_Trade_DB *info = &queryDataBlock.detail.brokenTrade;
  
  memset(info, 0, sizeof(t_Broken_Trade_DB));
  
  //queryType
  queryDataBlock.queryType = INSERT_BUST_OR_CORRECT;

  //OrderId
  info->orderID = brokenTrade->orderID;

  //date
  memcpy(info->date, brokenTrade->date, 10);

  //shares
  info->shares = brokenTrade->lastshares;

  //price
  info->price = brokenTrade->lastPx;

  //time stamp
  memcpy(info->timestamp, brokenTrade->timestamp, TIME_LENGTH);
  
  //msg seq num
  info->seqNum = brokenTrade->seqNum;

  //executionId
  sprintf(info->execId, "%lu", brokenTrade->executionId);

  //msg_content
  strcpy(info->msgContent, brokenTrade->msgContent);

  //status
  strcpy(info->status, brokenTrade->status);

  pthread_mutex_lock(&queryDataMgmt.updateMutex);
    
  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);
  
  SaveQueryDataToFile(&queryDataMgmt);
  
  pthread_mutex_unlock(&queryDataMgmt.updateMutex);

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessInsertNewOrder4EDGEQueryToCollection
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertNewOrder4EDGEQueryToCollection(t_EDGENewOrder *newOrder)
{
  t_QueryDataBlock queryDataBlock;
  t_New_Order_DB *info = &queryDataBlock.detail.newOrder;
  
  memset(info, 0, sizeof(t_New_Order_DB));
  
  //queryType
  queryDataBlock.queryType = INSERT_NEW_ORDER;
  
  //time stamp
  memcpy(info->timestamp, newOrder->timestamp, TIME_LENGTH);
  
  //ecn
  strcpy(info->ecn, newOrder->ecn);
  
  //shares
  info->shares = newOrder->shares;
  
  //price
  info->price = newOrder->price;
  
  //symbol
  strncpy(info->symbol, newOrder->symbol, SYMBOL_LEN);
  
  //time in force
  strcpy(info->timeInForce, newOrder->timeInForce);
  
  //left shares
  info->leftShares = newOrder->shares;
  
  //status
  strcpy(info->status, newOrder->status);
  
  //msg seq num
  info->seqNum = newOrder->seqNum;
  
  //OrderId
  info->orderID = newOrder->orderID;
  
  //side
  strcpy(info->side, newOrder->side);
  
  //avg_price
  info->avgPrice = newOrder->price;
  
  //msg_content
  strcpy(info->msgContent, newOrder->msgContent);
  
  //entry_exit
  strcpy(info->entryExit, newOrder->entryExit);
  
  //ecn_launch
  strcpy(info->ecnLaunch, newOrder->ecnLaunch);
  
  //ask bid
  strcpy(info->askBid, newOrder->askBid);
  
  //ts_id
  info->tsId = newOrder->tsId;
  
  //cross id
  info->crossId = newOrder->crossId;
  
  //date
  memcpy(info->date, newOrder->date, 10);
  
  //trade type
  info->tradeType = newOrder->tradeType;
  
  //is iso
  info->isoFlag = newOrder->eligibility;
  
  //bboId
  info->bboId = newOrder->bboId;
  
  //account
  info->tradingAccount = newOrder->tradingAccount;
  
  info->entryExitRefNum = newOrder->entryExitRefNum;
  
  // Visible shares
  info->visibleShares = newOrder->visibleShares;
  
  //routingInst
  strcpy(info->routingInst, "");
  
  pthread_mutex_lock(&queryDataMgmt.updateMutex);
    
  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);
  
  SaveQueryDataToFile(&queryDataMgmt);
  
  pthread_mutex_unlock(&queryDataMgmt.updateMutex);
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessInsertAcceptedOrder4EDGEQueryToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertAcceptedOrder4EDGEQueryToCollection(t_EDGEAcceptedOrder *acceptedOrder)
{
  t_QueryDataBlock queryDataBlock;
  t_Order_ACK_DB *info = &queryDataBlock.detail.orderAccepted;
  
  memset(info, 0, sizeof(t_Order_ACK_DB));
  
  //queryType
  queryDataBlock.queryType = INSERT_ORDER_ACCEPTED;
  
  //OrderId
  info->orderID = acceptedOrder->orderID;
  
  //date
  memcpy(info->date, acceptedOrder->date, 10);
  
  //time stamp
  memcpy(info->timestamp, acceptedOrder->timestamp, TIME_LENGTH);
  
  memcpy(info->processedTimestamp, acceptedOrder->processedTimestamp, TIME_LENGTH);
  
  //msg seq num
  info->seqNum = acceptedOrder->seqNum;
  
  //exOrderId
  sprintf(info->exOrderId, "%lu", acceptedOrder->exOrderId);
  
  //msg_content
  strcpy(info->msgContent, acceptedOrder->msgContent);
  
  //price
  info->ackPrice = acceptedOrder->price;
  
  //shares
  info->ackShares = acceptedOrder->shares;
  
  //status
  strcpy(info->status, acceptedOrder->status);
  
  pthread_mutex_lock(&queryDataMgmt.updateMutex);
    
  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);
  
  SaveQueryDataToFile(&queryDataMgmt);
  
  pthread_mutex_unlock(&queryDataMgmt.updateMutex);
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessInsertExecutedOrder4EDGEQueryToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertExecutedOrder4EDGEQueryToCollection(t_EDGEExecutedOrder *executedOrder)
{
  t_QueryDataBlock queryDataBlock;
  t_Order_Executed_DB *info = &queryDataBlock.detail.orderExecuted;
  
  memset(info, 0, sizeof(t_Order_Executed_DB));
  
  //queryType
  queryDataBlock.queryType = INSERT_ORDER_EXECUTED;
  
  //OrderId
  info->orderID = executedOrder->orderID;
  
  //date
  memcpy(info->date, executedOrder->date, 10);
  
  //shares
  info->lastShares = executedOrder->shares;
  
  //price
  info->lastPrice = executedOrder->price;
  
  //time stamp
  memcpy(info->timestamp, executedOrder->timestamp, TIME_LENGTH);
  
  memcpy(info->processedTimestamp, executedOrder->processedTimestamp, TIME_LENGTH);
  
  //msg seq num
  info->seqNum = executedOrder->seqNum;
  
  //executionId
  sprintf(info->execId, "%lu", executedOrder->executionId);
  
  //msg_content
  strcpy(info->msgContent, executedOrder->msgContent);
  
  //liquidityIndicator
  strncpy(info->liquidity, executedOrder->liquidityIndicator, 2);
  
  //status
  strcpy(info->status, executedOrder->status);
  
  //leftShares
  info->leftShares = executedOrder->leftShares;
  
  //avgPrice
  info->avgPrice = executedOrder->avgPrice;
  
  //Fill fee
  info->fillFee = executedOrder->fillFee;
  
  info->entryExitRefNum = executedOrder->entryExitRefNum;
  
  pthread_mutex_lock(&queryDataMgmt.updateMutex);
    
  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);
  
  SaveQueryDataToFile(&queryDataMgmt);
  
  pthread_mutex_unlock(&queryDataMgmt.updateMutex);
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessInsertCanceledOrder4EDGEQueryToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertCanceledOrder4EDGEQueryToCollection(t_EDGECanceledOrder *canceledOrder)
{
  t_QueryDataBlock queryDataBlock;
  t_Order_Canceled_DB *info = &queryDataBlock.detail.orderCanceled;
  
  memset(info, 0, sizeof(t_Order_Canceled_DB));
  
  //queryType
  queryDataBlock.queryType = INSERT_ORDER_CANCELED;
  
  //OrderId
  info->orderID = canceledOrder->orderID;
  
  //date
  memcpy(info->date, canceledOrder->date, 10);
  
  //time stamp
  memcpy(info->timestamp, canceledOrder->timestamp, TIME_LENGTH);
    
  memcpy(info->processedTimestamp, canceledOrder->processedTimestamp, TIME_LENGTH);
  
  //msg seq num
  info->seqNum = canceledOrder->seqNum;
  
  //msg_content
  strcpy(info->msgContent, canceledOrder->msgContent);

  //status
  strcpy(info->status, canceledOrder->status);
  
  info->entryExitRefNum = canceledOrder->entryExitRefNum;
  
  pthread_mutex_lock(&queryDataMgmt.updateMutex);
    
  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);
  
  SaveQueryDataToFile(&queryDataMgmt);
  
  pthread_mutex_unlock(&queryDataMgmt.updateMutex);
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessInsertRejectedOrder4EDGEQueryToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertRejectedOrder4EDGEQueryToCollection(t_EDGERejectedOrder *rejectedOrder)
{
  t_QueryDataBlock queryDataBlock;
  t_Order_Rejected_DB *info = &queryDataBlock.detail.orderRejected;
  
  memset(info, 0, sizeof(t_Order_Rejected_DB));
  
  //queryType
  queryDataBlock.queryType = INSERT_ORDER_REJECTED;
  
  //OrderId
  info->orderID = rejectedOrder->orderID;
  
  //date
  memcpy(info->date, rejectedOrder->date, 10);
  
  //time stamp
  memcpy(info->timestamp, rejectedOrder->timestamp, TIME_LENGTH);
    
  memcpy(info->processedTimestamp, rejectedOrder->processedTimestamp, TIME_LENGTH);
  
  //msg seq num
  info->seqNum = rejectedOrder->seqNum;
  
  //reason
  sprintf(info->reasonCode, "%c", rejectedOrder->reason);
  
  //msg_content
  strcpy(info->msgContent, rejectedOrder->msgContent);

  //status
  strcpy(info->status, rejectedOrder->status);
  
  info->entryExitRefNum = rejectedOrder->entryExitRefNum;
  
  pthread_mutex_lock(&queryDataMgmt.updateMutex);
    
  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);
  
  SaveQueryDataToFile(&queryDataMgmt);
  
  pthread_mutex_unlock(&queryDataMgmt.updateMutex);
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessInsertBrokenTrade4EDGEQueryToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertBrokenTrade4EDGEQueryToCollection(t_EDGEBrokenOrder *brokenTrade)
{
  t_QueryDataBlock queryDataBlock;
  t_Broken_Trade_DB *info = &queryDataBlock.detail.brokenTrade;
  
  memset(info, 0, sizeof(t_Broken_Trade_DB));
  
  //queryType
  queryDataBlock.queryType = INSERT_BUST_OR_CORRECT;
  
  //OrderId
  info->orderID = brokenTrade->orderID;
  
  //date
  memcpy(info->date, brokenTrade->date, 10);
  
  //time stamp
  memcpy(info->timestamp, brokenTrade->timestamp, TIME_LENGTH);
  
  //msg seq num
  info->seqNum = brokenTrade->seqNum;
  
  //executionId
  sprintf(info->execId, "%lu", brokenTrade->executionId);
  
  //msg_content
  strcpy(info->msgContent, brokenTrade->msgContent);

  //status
  strcpy(info->status, brokenTrade->status);
  
  pthread_mutex_lock(&queryDataMgmt.updateMutex);
    
  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);
  
  SaveQueryDataToFile(&queryDataMgmt);
  
  pthread_mutex_unlock(&queryDataMgmt.updateMutex);
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessInsertBBOInfoToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertBBOInfoToCollection(t_dbBBOInfo *dbBBOInfo)
{
  t_QueryDataBlock queryDataBlock;
  t_dbBBOInfo *info = &queryDataBlock.detail.bboInfo;
  
  memset(info, 0, sizeof(t_dbBBOInfo));  
  
  //queryType
  queryDataBlock.queryType = INSERT_BBO_QUOTE_INFO;
  
  //tsid
  info->ts_id = dbBBOInfo->ts_id;
  
  //CrossID
  info->cross_id = dbBBOInfo->cross_id;
  
  //bbo_id
  info->bbo_id = dbBBOInfo->bbo_id;
  
  //BBO contents
  strcpy (info->contents, dbBBOInfo->contents);
    
  pthread_mutex_lock(&queryDataMgmt.updateMutex);
    
  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);
  
  SaveQueryDataToFile(&queryDataMgmt);
  
  pthread_mutex_unlock(&queryDataMgmt.updateMutex);
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessInsertShortSellFailedToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertShortSellFailedToCollection(const char *symbol, int tsCrossId, int tsIndex, const char *dateString, const char *content, int tradeType)
{
  t_QueryDataBlock queryDataBlock;
  t_Short_Sell_Failed *info = &queryDataBlock.detail.shortSellFailed;
  
  memset(info, 0, sizeof(t_Short_Sell_Failed));
  
  //queryType
  queryDataBlock.queryType = INSERT_SHORT_SELL_FAILED;
  
  //crossid
  info->tsCrossId = tsCrossId;
  
  //tsindex
  info->tsIndex = tsIndex;
  
  //trade type
  info->tradeType = tradeType;
  
  //symbol
  strncpy(info->symbol, symbol, 8);
  
  //date
  strcpy(info->date, dateString);
  
  //contents
  strcpy(info->content, content);
  
  pthread_mutex_lock(&queryDataMgmt.updateMutex);
    
  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);
  
  SaveQueryDataToFile(&queryDataMgmt);
  
  pthread_mutex_unlock(&queryDataMgmt.updateMutex);
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessInsertTradeOutputLog_TradeToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertTradeOutputLog_TradeToCollection(int tradeId, int tsCrossId, int tsIndex, const char *dateString)
{
  t_QueryDataBlock queryDataBlock;
  t_Output_Log_Trade *info = &queryDataBlock.detail.outputLogTrade;
  
  memset(info, 0, sizeof(t_Output_Log_Trade));
  
  //queryType
  queryDataBlock.queryType = INSERT_OUTPUT_LOG_TRADE;
  
  //TradeID
  info->tradeId = tradeId;
  
  //crossid
  info->crossId = tsCrossId;
  
  //tsindex
  info->tsId = tsIndex;
  
  //date
  strcpy(info->date, dateString);
  
  pthread_mutex_lock(&queryDataMgmt.updateMutex);
    
  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);
  
  SaveQueryDataToFile(&queryDataMgmt);
  
  pthread_mutex_unlock(&queryDataMgmt.updateMutex);
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessInsertTradeResultToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertTradeResultToCollection(t_TradeResultInfo tradeResult)
{
  t_QueryDataBlock queryDataBlock;
  t_TradeResultInfo *info = &queryDataBlock.detail.tradeResult;
  
  memset(info, 0, sizeof(t_TradeResultInfo));
  
  //queryType
  queryDataBlock.queryType = INSERT_TRADE_RESULT;
  
  info->expectedShares = tradeResult.expectedShares;
  info->expectedPL = tradeResult.expectedPL;
  info->boughtShares = tradeResult.boughtShares;
  info->totalBought = tradeResult.totalBought;
  info->soldShares = tradeResult.soldShares;
  info->totalSold = tradeResult.totalSold;
  info->spread = tradeResult.spread;
  info->leftStuckShares = tradeResult.leftStuckShares;
  info->as_cid = tradeResult.as_cid;
  info->ts_cid = tradeResult.ts_cid;
  info->ts_id = tradeResult.ts_id;
  info->trade_id = tradeResult.trade_id;
  info->timestamp = tradeResult.timestamp;
  strncpy(info->symbol, tradeResult.symbol, SYMBOL_LEN);
  strcpy(info->dateString, tradeResult.dateString);
  
  pthread_mutex_lock(&queryDataMgmt.updateMutex);
    
  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);
  
  SaveQueryDataToFile(&queryDataMgmt);
  
  pthread_mutex_unlock(&queryDataMgmt.updateMutex);
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessInsertTradeOutputLogDetected_TradeToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertTradeOutputLogDetected_TradeToCollection(int asCrossId, const char *symbol, int tsCrossId, int tsIndex, const char *dateString, const char *content, int tradeIndex, int tradeType)
{
  t_QueryDataBlock queryDataBlock;
  t_Output_Log_Detected_Trade *info = &queryDataBlock.detail.outputLogDetectedTrade;
  
  memset(info, 0, sizeof(t_Output_Log_Detected_Trade));
  
  queryDataBlock.queryType = INSERT_OUTPUT_LOG_DETECTED_TRADE;
  
  info->asCrossId = asCrossId;
  strncpy(info->symbol, symbol, SYMBOL_LEN);
  info->tsCrossId = tsCrossId;
  info->tsIndex = tsIndex;
  strcpy(info->date, dateString);
  strcpy(info->content, content);
  info->tradeIndex = tradeIndex;
  info->tradeType = tradeType;
  
  pthread_mutex_lock(&queryDataMgmt.updateMutex);
    
  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);
  
  SaveQueryDataToFile(&queryDataMgmt);
  
  pthread_mutex_unlock(&queryDataMgmt.updateMutex);
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessInsertTradeOutputLogDetectedToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertTradeOutputLogDetectedToCollection(int asCrossId, const char *symbol, int tsCrossId, int tsIndex, const char *dateString, const char *content, int tradeType)
{
  t_QueryDataBlock queryDataBlock;
  t_Output_Log_Detected *info = &queryDataBlock.detail.outputLogDetected;
  
  memset(info, 0, sizeof(t_Output_Log_Detected));
  
  queryDataBlock.queryType = INSERT_OUTPUT_LOG_DETECTED;
  
  info->asCrossId = asCrossId;
  strncpy(info->symbol, symbol, SYMBOL_LEN);
  info->tsCrossId = tsCrossId;
  info->tsIndex = tsIndex;
  strcpy(info->date, dateString);
  strcpy(info->content, content);
  info->tradeType = tradeType;
  
  pthread_mutex_lock(&queryDataMgmt.updateMutex);
    
  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);
  
  SaveQueryDataToFile(&queryDataMgmt);
  
  pthread_mutex_unlock(&queryDataMgmt.updateMutex);
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessInsertTradeOutputLogNotYetDetect_TradeToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertTradeOutputLogNotYetDetect_TradeToCollection(int tsCrossId, int tsIndex, const char *dateString, const char *content, int tradeIndex)
{
  t_QueryDataBlock queryDataBlock;
  t_Output_Log_Not_Yet_Detect_Trade *info = &queryDataBlock.detail.outputLogNotYetDetectTrade;
  
  memset(info, 0, sizeof(t_Output_Log_Not_Yet_Detect_Trade));
  
  queryDataBlock.queryType = INSERT_OUTPUT_LOG_NOT_YET_DETECT_TRADE;
  
  info->tsCrossId = tsCrossId;
  info->tsIndex = tsIndex;
  strcpy(info->date, dateString);
  strcpy(info->content, content);
  info->tradeIndex = tradeIndex;
  
  pthread_mutex_lock(&queryDataMgmt.updateMutex);
    
  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);
  
  SaveQueryDataToFile(&queryDataMgmt);
  
  pthread_mutex_unlock(&queryDataMgmt.updateMutex);
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessInsertTradeOutputLogNotYetDetectToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertTradeOutputLogNotYetDetectToCollection(int tsCrossId, int tsIndex, const char *dateString, const char *content)
{
  t_QueryDataBlock queryDataBlock;
  t_Output_Log_Not_Yet_Detect *info = &queryDataBlock.detail.outputLogNotYetDetect;
  
  memset(info, 0, sizeof(t_Output_Log_Not_Yet_Detect));
  
  queryDataBlock.queryType = INSERT_OUTPUT_LOG_NOT_YET_DETECT;
  
  info->tsCrossId = tsCrossId;
  info->tsIndex = tsIndex;
  strcpy(info->date, dateString);
  strcpy(info->content, content);
  
  pthread_mutex_lock(&queryDataMgmt.updateMutex);
    
  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);
  
  SaveQueryDataToFile(&queryDataMgmt);
  
  pthread_mutex_unlock(&queryDataMgmt.updateMutex);
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessInsertAskBidToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertAskBidToCollection(int tsCrossId, int tsIndex, int shares, double price, int askBid, int ecnLaunch, const char *dateString)
{
  t_QueryDataBlock queryDataBlock;
  t_Output_Log_Ask_Bid *info = &queryDataBlock.detail.outputLogAskBid;
  
  memset(info, 0, sizeof(t_Output_Log_Ask_Bid));
  
  queryDataBlock.queryType = INSERT_OUTPUT_LOG_ASK_BID;
  
  info->tsCrossId = tsCrossId;
  info->tsIndex = tsIndex;
  info->shares = shares;
  info->price = price;
  info->askBid= askBid;
  info->ecnLaunch = ecnLaunch;
  strcpy(info->date, dateString);
  
  pthread_mutex_lock(&queryDataMgmt.updateMutex);
    
  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);
  
  SaveQueryDataToFile(&queryDataMgmt);
  
  pthread_mutex_unlock(&queryDataMgmt.updateMutex);
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessInsertTradeOutputLogToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessInsertTradeOutputLogToCollection(int asCrossId, const char *symbol, int tsCrossId, int tsIndex, const char *dateString, int tradeType)
{
  t_QueryDataBlock queryDataBlock;
  t_Output_Log *info = &queryDataBlock.detail.outputLog;
  
  memset(info, 0, sizeof(t_Output_Log));
  
  //queryType
  queryDataBlock.queryType = INSERT_OUTPUT_LOG;
  
  info->asCrossId = asCrossId;
  strncpy(info->symbol, symbol, SYMBOL_LEN);
  info->tsCrossId = tsCrossId;
  info->tsIndex = tsIndex;
  strcpy(info->date, dateString);
  info->tradeType = tradeType;
  
  pthread_mutex_lock(&queryDataMgmt.updateMutex);
    
  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);
  
  SaveQueryDataToFile(&queryDataMgmt);
  
  pthread_mutex_unlock(&queryDataMgmt.updateMutex);
  
  return 0;
}
/****************************************************************************
- Function name:  ProcessUpdateRealValueToCollection
- Input:      
- Output:   
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessUpdateRealValueToCollection(int tsId, int crossId, char *dateString, int leftShares, double profitLoss, double totalFee)
{
  t_QueryDataBlock queryDataBlock;
  t_Real_Value *info = &queryDataBlock.detail.realValue;
  
  memset(info, 0, sizeof(t_Real_Value));
  
  queryDataBlock.queryType = UPDATE_REAL_VALUE;
  
  info->tsId = tsId;
  info->crossId = crossId;
  strcpy(info->date, dateString);
  info->leftShares = leftShares;
  info->profitLoss = profitLoss;
  info->totalFee = totalFee;
  
  pthread_mutex_lock(&queryDataMgmt.updateMutex);
    
  AddQueryDataBlockToCollection(&queryDataBlock, &queryDataMgmt);
  
  SaveQueryDataToFile(&queryDataMgmt);
  
  pthread_mutex_unlock(&queryDataMgmt.updateMutex);
  
  return 0;
}

/******************************************************************************/
