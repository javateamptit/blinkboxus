/****************************************************************************
** Project name:  Equity Arbitrage Application
** File name:   arca_direct_proc.c
** Description:   This file contains function definitions that were declared
        in arca_direct_proc.h

** Author:    Sang Nguyen-Minh
** First created on 18 September 2007
** Last updated on 18 September 2007
****************************************************************************/

#define _ISOC9X_SOURCE
//#define _XOPEN_SOURCE 500

#include <unistd.h>
#include <time.h>
#include <sys/time.h>
#include <errno.h>

#include "configuration.h"
#include "raw_data_mgmt.h"
#include "trade_servers_mgmt_proc.h"
#include "arca_direct_proc.h"
#include "order_mgmt_proc.h"
#include "arca_direct_data.h"
#include "daedalus_proc.h"
#include "trader_tools_proc.h"


/****************************************************************************
          GLOBAL VARIABLES
****************************************************************************/
static double _DIRECT_PRICE_PORTION[10];

t_ARCA_DIRECT_Config ARCA_DIRECT_Config;
static pthread_mutex_t ADSockMutex = PTHREAD_MUTEX_INITIALIZER;

/****************************************************************************
** Function definitions
****************************************************************************/

/****************************************************************************
- Function name:  Init_ARCA_DIRECT_DataStructure
- Input:      N/A
- Output:   Update some global variables
- Return:   N/A
- Description:  + The routine hides the following facts
        + There are no preconditions guaranteed to the routine
- Usage:      N/A
****************************************************************************/
int Init_ARCA_DIRECT_DataStructure(void)
{
  _DIRECT_PRICE_PORTION[0] = 1.0;
  _DIRECT_PRICE_PORTION[1] = 0.1;
  _DIRECT_PRICE_PORTION[2] = 0.01;
  _DIRECT_PRICE_PORTION[3] = 0.001;
  _DIRECT_PRICE_PORTION[4] = 0.0001;
  _DIRECT_PRICE_PORTION[5] = 0.00001;
  _DIRECT_PRICE_PORTION[6] = 0.000001;
  _DIRECT_PRICE_PORTION[7] = 0.0000001;
  _DIRECT_PRICE_PORTION[8] = 0.00000001;
  _DIRECT_PRICE_PORTION[9] = 0.000000001;
  
  // Load incoming and outgoing sequence number from file
  if (LoadSequenceNumber("arca_direct_seqs", AS_ARCA_DIRECT_INDEX, &ARCA_DIRECT_Config) == ERROR)
  { 
    TraceLog(ERROR_LEVEL, "Cannot load arca_direct_seqs file\n");
    return ERROR;
  }

  if (ARCA_DIRECT_Config.socket < 1)
  {
    ARCA_DIRECT_Config.socket = -1;
  }
  else
  {
    close(ARCA_DIRECT_Config.socket);
    ARCA_DIRECT_Config.socket = -1;
  }
  
  ARCA_DIRECT_Config.enableTrading = -1;
  ARCA_DIRECT_Config.isConnected = DISCONNECTED;

  
  return SUCCESS;
}

/****************************************************************************
- Function name:  StartUp_ARCA_DIRECT_Module
- Input:      + threadArgs
- Output:   N/A
- Return:   Success or Failure
- Description:  + The routine hides the following facts
        + There are no preconditions guaranteed to the routine
        + The routine guarantees that the return value will 
            have a value of either
          - Success
          or
          - Failure
- Usage:      N/A
****************************************************************************/
void *StartUp_ARCA_DIRECT_Module(void *threadArgs)
{
  if (Init_ARCA_DIRECT_DataStructure() == ERROR)
  { 
    orderStatusMgmt[AS_ARCA_DIRECT_INDEX].shouldConnect = NO;
    return NULL;
  }
  
  TraceLog(DEBUG_LEVEL, "Trying connect to ARCA DIRECT Server\n");
  // Connect to ARCA DIRECT Server
  ARCA_DIRECT_Config.socket = Connect2Server(ARCA_DIRECT_Config.ipAddress, ARCA_DIRECT_Config.port, "ARCA DIRECT");
  
  if (ARCA_DIRECT_Config.socket == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot connect to ARCA DIRECT Server\n");
    orderStatusMgmt[AS_ARCA_DIRECT_INDEX].shouldConnect = NO;
    return NULL;
  }
  
  /*
  If success, build logon request message and send it to server
  + Try to receive logon accepted message or logon rejected message
  + With Logon Accepted Message: get Last Sequence Number from response message
  and update outgoing sequence number at local
  + With Logon Rejected Message: print something and exit module
  */
  if (SetSocketRecvTimeout(ARCA_DIRECT_Config.socket, ARCA_DIRECT_RECV_TIMEOUT) == ERROR)
  {
    close(ARCA_DIRECT_Config.socket);
    ARCA_DIRECT_Config.socket = -1;
    orderStatusMgmt[AS_ARCA_DIRECT_INDEX].shouldConnect = NO;
    return NULL;
  }
  
  // Talk to ARCA DIRECT Server
  TalkTo_ARCA_DIRECT_Server();
    
  /*
  Stop execution of the module
  */
  if (ARCA_DIRECT_Config.socket > 0)
  {
    close(ARCA_DIRECT_Config.socket);
    ARCA_DIRECT_Config.socket = -1;
  }
  
  orderStatusMgmt[AS_ARCA_DIRECT_INDEX].shouldConnect = NO;
  orderStatusMgmt[AS_ARCA_DIRECT_INDEX].isOrderConnected = DISCONNECTED;  
  traderToolsInfo.orderConnection[AS_ARCA_DIRECT_INDEX].status = DISCONNECT;

  TraceLog(DEBUG_LEVEL, "Connection to ARCA DIRECT Server was disconnected\n");
  
  BuildNSendDisconnectAlertToAllDaedalus("ARCA DIRECT from AS");
  BuildNSendDisconnectAlertToAllTT("ARCA DIRECT from AS");
  
  return NULL;
}

/****************************************************************************
- Function name:  Send_Test_Request_Thread
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
void *Send_Test_Request_Thread(void *arg)
{
  TraceLog(DEBUG_LEVEL, "ARCA DIRIECT: Start thread to auto send Test Request message ...\n");
  unsigned int countTimeInSencond = 0;
  while(orderStatusMgmt[AS_ARCA_DIRECT_INDEX].shouldConnect == YES)
  {
    //Send Test Request after 30 seconds
    if(countTimeInSencond % 30 == 0)
    {
      //if (BuildAndSend_NYSE_CCG_HeartbeatMsg() == ERROR)
      if (BuildAndSend_ARCA_DIRECT_TestRequestMsg() == ERROR)
      {
        TraceLog(ERROR_LEVEL, "NYSE CCG: Killed thread send Test Request due to error send Test Request\n");
        return NULL;
      }
    }

    countTimeInSencond++;
    sleep(1);
  }

  TraceLog(DEBUG_LEVEL, "ARCA DIRIECT: Killed thread send Test Request due to disconnect from AS\n");

  return NULL;
}

/****************************************************************************
- Function name:  TalkTo_ARCA_DIRECT_Server
- Input:      N/A
- Output:   N/A
- Return:   N/A
- Description:  + The routine hides the following facts
          - Receive message from server and send response appropriately
          - When the routine returns, meaning has some problems with 
          socket
        + There are no preconditions guaranteed to the routine
- Usage:      N/A
****************************************************************************/
void TalkTo_ARCA_DIRECT_Server(void)
{
  t_DataBlock dataBlock;
  
  TraceLog(DEBUG_LEVEL, "Talking to ARCA DIRECT Server...\n");
  
  // Send Logon message to server
  if (BuildAndSend_ARCA_DIRECT_LogonRequestMsg() == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send logon request message to ARCA DIRECT server\n");
    return;
  }
  
  while(orderStatusMgmt[AS_ARCA_DIRECT_INDEX].shouldConnect == YES)
  {
    memset (&dataBlock, 0, sizeof(t_DataBlock));
    
    if(Receive_ARCA_DIRECT_Msg(&dataBlock) == ERROR)
    {
      return;
    }
    else
    {
      switch (dataBlock.msgContent[MSG_TYPE_INDEX])
      {
        case ORDER_ACK_TYPE:
          ARCA_DIRECT_Config.incomingSeqNum++;
          SaveARCA_DIRECT_SeqNumToFile();
          UpdateVerifyPositionInfoFromDIRECTAcceptedManualOrder(&dataBlock, AS_MANUAL_ORDER);
          Add_ARCA_DIRECT_DataBlockToCollection(&dataBlock);
          break;
          
        case ORDER_FILLED_TYPE:
          ARCA_DIRECT_Config.incomingSeqNum++;
          SaveARCA_DIRECT_SeqNumToFile();
          UpdateVerifyPositionInfoFromDIRECTExecutedManualOrder(&dataBlock, AS_MANUAL_ORDER);
          Add_ARCA_DIRECT_DataBlockToCollection(&dataBlock);
          break;
          
        case CANCEL_ACK_TYPE:
          ARCA_DIRECT_Config.incomingSeqNum++;
          SaveARCA_DIRECT_SeqNumToFile();
          Add_ARCA_DIRECT_DataBlockToCollection(&dataBlock);
          break;
          
        case ORDER_REJECTED_TYPE:
          ARCA_DIRECT_Config.incomingSeqNum++;
          SaveARCA_DIRECT_SeqNumToFile();
          UpdateVerifyPositionInfoFromDIRECTRejectedManualOrder(&dataBlock, AS_MANUAL_ORDER);
          Add_ARCA_DIRECT_DataBlockToCollection(&dataBlock);
          break;
        
        case ORDER_KILLED_TYPE:
          ARCA_DIRECT_Config.incomingSeqNum++;
          SaveARCA_DIRECT_SeqNumToFile();
          UpdateVerifyPositionInfoFromDIRECTCanceledManualOrder(&dataBlock, AS_MANUAL_ORDER);
          Add_ARCA_DIRECT_DataBlockToCollection(&dataBlock);
          break;      
        
        case BUST_OR_CORRECT_TYPE:
          ARCA_DIRECT_Config.incomingSeqNum++;
          SaveARCA_DIRECT_SeqNumToFile();
          Add_ARCA_DIRECT_DataBlockToCollection(&dataBlock);
          break;
      
        case TEST_REQUEST_TYPE:
          
          //TraceLog(DEBUG_LEVEL, "TEST_REQUEST_TYPE\n");
          
          // Send heartbeat to reply server test request
          if (BuildAndSend_ARCA_DIRECT_HeartbeatMsg() == ERROR)
          {
            return;
          }
          
          break;
          
        case HEARTBEAT_TYPE:
          
          //TraceLog(DEBUG_LEVEL, "HEARTBEAT_TYPE\n");
          
          // Send heartbeat to reply server heartbeat
          if (BuildAndSend_ARCA_DIRECT_HeartbeatMsg() == ERROR)
          {
            return;
          }

          break;
          
        case LOGON_TYPE:
          TraceLog(DEBUG_LEVEL, "Received logon accepted from ARCA DIRECT Server\n");
          
          ProcessARCA_DIRECT_LogonAcceptedMsg(&dataBlock);
          
          traderToolsInfo.orderConnection[AS_ARCA_DIRECT_INDEX].status = CONNECT;
          orderStatusMgmt[AS_ARCA_DIRECT_INDEX].isOrderConnected = CONNECTED;
          
          TraceLog(DEBUG_LEVEL, "ARCA_DIRECT_Config.incomingSeqNum = %d\n", ARCA_DIRECT_Config.incomingSeqNum);
          TraceLog(DEBUG_LEVEL, "ARCA_DIRECT_Config.outgoingSeqNum = %d\n", ARCA_DIRECT_Config.outgoingSeqNum);
          
          pthread_t sendTestRequestThr;
          pthread_create(&sendTestRequestThr, NULL, (void*)&Send_Test_Request_Thread, NULL);
          
          break;
          
        case LOGON_REJECTED_TYPE:
          // We must stop here and try to connect to server again
          TraceLog(DEBUG_LEVEL, "Received logon rejected from ARCA DIRECT Server\n");
          
          TraceLog(DEBUG_LEVEL, "Reject Type %d\n", GetShortNumberBigEndian(&dataBlock.msgContent[16]));
          TraceLog(DEBUG_LEVEL, "Reject Text = %s\n", &dataBlock.msgContent[18]);
          
          ProcessARCA_DIRECT_LogonRejectedMsg(&dataBlock);
          
          return;
          
        default:
          TraceLog(WARN_LEVEL, "ARCA DIRECT - Unknown message type %d\n", dataBlock.msgContent[MSG_TYPE_INDEX]);
          break;
      }
    }
  }
  
  TraceLog(DEBUG_LEVEL, "Connection to ARCA DIRECT Server will be closed now...\n");
}

/****************************************************************************
- Function name:  BuildLogonRequestMessage
- Input:      N/A
- Output:   N/A
- Return:   Success or Failure
- Description:  + The routine hides the following facts
          - Build logon message with using some global variables
          - Calculate message length and return through return value
        + There are preconditions guaranteed to the routine
          -   message must be allocated before
        + The routine guarantees that the return value will 
            have a value of either
          - Success
          or 
          - Failure
- Usage:      N/A
****************************************************************************/
int BuildAndSend_ARCA_DIRECT_LogonRequestMsg(void)
{
  /*
  - API version: ArcaDirect 4.1g
  - Date updated: 03-Jan-2013
  - Modified by: Long NKH
  - Logon Messeage (and Logon Accepted), it includes the following fields:
    + Message Type (1 byte) = 'A'
    + Variant (1 byte) = 1
    + Length (2 bytes) = 48
    + SeqNum (4 bytes) = null (should be ignored)
    + Last Sequence Number (4 bytes) = Last sequence number processed
    + UserName (5 bytes) = ArcaDirect Login ID
    + Symbology (1 byte) = null (use for Options orders only.)
    + Message Version Profile (28 bytes) = 'L',1,'D',1,'G',1,'E',1,'a',1,'2',1,'6',1,'4',1 (16 bytes)
    + Cancel On Disconnect (1 byte binary) 0: Do not cancel when disconnect, 1: Auto Cancel when disconnect
    + Message Terminator (1 byte) = '\n'
    => Total byte = 48
  */
  
  char message[LOGON_TYPE_MSG_LEN + 1] = "\0";
  
  t_IntConverter lastSeqNum;
  t_ShortConverter tmpLength;
  
  // Message type is 'A'
  message[0] = LOGON_TYPE;

  // Variant
  message[1] = 1;

  // Length
  tmpLength.value = LOGON_TYPE_MSG_LEN;
  message[2] = tmpLength.c[1];
  message[3] = tmpLength.c[0];
  
  // SeqNum (4 bytes)
  
  /*
  Last sequence number
  Message last sequence number should start at 0
  */
  lastSeqNum.value = ARCA_DIRECT_Config.incomingSeqNum;
  message[8] = lastSeqNum.c[3];
  message[9] = lastSeqNum.c[2];
  message[10] = lastSeqNum.c[1];
  message[11] = lastSeqNum.c[0];

  // Company Group ID (User name)
  memcpy(&message[12], ARCA_DIRECT_Config.userName, 5);

  // Symbology (1 byte)
  
  // Message Version Profile
  strncpy(&message[18], VERSION_PROFILE, 28);
  
  // Cancel On Disconnect (1 bytes binary). Should be 0 or 1
  message[46] = ARCA_DIRECT_Config.autoCancelOnDisconnect;

  // Terminator
  message[47] = '\n';
  
  // Send logon message
  return Send_ARCA_DIRECT_Msg(message, LOGON_TYPE_MSG_LEN);
}

/****************************************************************************
- Function name:  BuildAndSend_ARCA_DIRECT_HeartbeatMsg
- Input:      N/A
- Output:   + message
- Return:   message length
- Description:  + The routine hides the following facts
          - Build heartbeat message with using some global variables
          - Calculate message length and return through return value
        + There are preconditions guaranteed to the routine
          -   message must be allocated before
        + The routine guarantees that the return value will 
            have a value of
          - message length
- Usage:      N/A
****************************************************************************/
int BuildAndSend_ARCA_DIRECT_HeartbeatMsg(void)
{
  /*
  - API version: ArcaDirect 4.1g
  - Date updated: 03-Jan-2013
  - Modified by: Long NKH
  - Heartbeat Message, it includes the following fields:
    + Message Type (1 byte) = '0'
    + Variant (1 byte) = 1
    + Length (2 bytes) = 12
    + SeqNum (4 bytes) = null (should be ignored)
    + Filler (3 byte) = null
    + Message Terminator (1 byte) = '\n'
    => Total byte = 12
  */
  
  char message[HEARTBEAT_MSG_LEN] = "\0";
  t_ShortConverter tmpLength;
    
  // Message type is '0'
  message[0] = HEARTBEAT_TYPE;

  // Variant (1 byte) = 1
  message[1] = 1;

  // Length
  tmpLength.value = HEARTBEAT_MSG_LEN;
  message[2] = tmpLength.c[1];
  message[3] = tmpLength.c[0];

  // SeqNum (4 bytes)
  
  // Filler (3 byte)

  // Terminator
  message[11] = '\n';
  
  // Send heartbeat
  return Send_ARCA_DIRECT_Msg(message, HEARTBEAT_MSG_LEN);
}

/****************************************************************************
- Function name:  BuildAndSend_ARCA_DIRECT_TestRequestMsg
- Input:      N/A
- Output:   N/A
- Return:   message length
- Description:  + The routine hides the following facts
          - Build heartbeat message with using some global variables
          - Calculate message length and return through return value
        + There are preconditions guaranteed to the routine
          -   message must be allocated before
        + The routine guarantees that the return value will 
            have a value of
          - message length
- Usage:      N/A
****************************************************************************/
int BuildAndSend_ARCA_DIRECT_TestRequestMsg(void)
{ 
  /*
  - API version: ArcaDirect 4.1g
  - Date updated: 03-Jan-2013
  - Modified by: Long NKH
  - Test Request Message, it includes the following fields:
    + Message Type (1 byte) = '1'
    + Variant (1 byte) = 1
    + Length (2 bytes) = 12
    + SeqNum (4 bytes) = null (should be ignored)
    + Filler (3 byte) = null
    + Message Terminator (1 byte) = '\n'
    => Total byte = 12
  */
  
  char message[TEST_REQUEST_MSG_LEN + 1] = "\0";
  t_ShortConverter tmpLength;

  // Message type is '1'
  message[0] = TEST_REQUEST_TYPE;

  // Variant (1 byte) = 1
  message[1] = 1;

  // Length
  tmpLength.value = TEST_REQUEST_MSG_LEN;
  message[2] = tmpLength.c[1];
  message[3] = tmpLength.c[0];

  // SeqNum (4 bytes)
  
  // Filler (3 byte)

  // Terminator
  message[11] = '\n';
  
  //Send test request
  return Send_ARCA_DIRECT_Msg(message, TEST_REQUEST_MSG_LEN);
}

/****************************************************************************
- Function name:  BuildAndSend_ARCA_DIRECT_OrderMsg_V1
- Input:      + newOrder
- Output:   N/A
- Return:   Success or Failure
- Description:  
        
- Usage:  N/A
****************************************************************************/
int BuildAndSend_ARCA_DIRECT_OrderMsg_V1(t_ARCA_DIRECT_NewOrder *newOrder)
{
  /*
  - API version: ArcaDirect 4.1g
  - Date updated: 03-Jan-2013
  - Modified by: Long NKH
  - New Order Message � (Variant 1: formerly V2 API � small message), it includes the following fields:
    + Message Type (1 byte) = 'D'
    + Variant (1 byte) = 1
    + Length (2 bytes) = 76
    + Sequence Number (4 bytes) = Client-assigned sequence number
    + Client Order Id (4 bytes) = A client-assigned ID for this order
    + PCS Link ID (4 bytes) = null
    + Order Quantity (4 bytes) = The number of shares
    + Price (4 bytes) = The price as a long value
    + ExDestination (2 bytes) = 102
    + Price Scale (1 byte) = '2'
    + Symbol (8 bytes) = Stock symbol
    + Company Group ID (5 bytes) = HBIFA
    + Deliver To Comp ID (5 bytes) = null
    + SenderSubID (5 bytes) = null
    + Execution Instructions (1 byte) = '1'
    + Side (1 byte) = '1', '2', or '5'
    + Order Type (1 byte) = '2'
    + Time In Force (1 byte) = '0' or '3'
    + Rule80A (1 byte) = 'P' --> change A to P (related to ticket 1112)
    + Trading Session Id (4 bytes) = "123"
    + Account (10 bytes) = null
    + ISO (1 byte) = 'N'
    + Extended Execution Instructions (1 byte) = null
    + ExtendedPNP (1 byte) = null
    + NoSelfTrade (1 byte) = 'O'
    + ProactiveIfLocked (1 byte) = null
    + Filler (1 byte) = null
    + Message Terminator (1 byte) = '\n'
    => Total byte = 76
  */
  
  t_IntConverter tmpValue;
  t_ShortConverter tmpLength;

  char message[NEW_ORDER_MSG_LEN_V1 + 1] = "\0";

  // Message type is 'D'
  message[0] = NEW_ORDER_TYPE;

  // Variant (1 byte) = 1
  message[1] = 1;

  // Length
  tmpLength.value = NEW_ORDER_MSG_LEN_V1;
  message[2]  = tmpLength.c[1];
  message[3]  = tmpLength.c[0];

  // Sequence number
  tmpValue.value =  Get_ARCA_DIRECT_OutgoingSeqNum();
  message[4] = tmpValue.c[3];
  message[5] = tmpValue.c[2];
  message[6] = tmpValue.c[1];
  message[7] = tmpValue.c[0];

  // Client order Id
  tmpValue.value =  newOrder->clOrdID;
  message[8] = tmpValue.c[3];
  message[9] = tmpValue.c[2];
  message[10] = tmpValue.c[1];
  message[11] = tmpValue.c[0];
  
  // PCS Link ID
  message[12] = 0;
  message[13] = 0;
  message[14] = 0;
  message[15] = 0;

  // Order Quantity
  tmpValue.value =  newOrder->orderQty;
  message[16] = tmpValue.c[3];
  message[17] = tmpValue.c[2];
  message[18] = tmpValue.c[1];
  message[19] = tmpValue.c[0];

  // Price
  tmpValue.value = (int)(newOrder->price * 100.00 + 0.500001);
  message[20] = tmpValue.c[3];
  message[21] = tmpValue.c[2];
  message[22] = tmpValue.c[1];
  message[23] = tmpValue.c[0];
  
  // ExDestination (2 bytes) = 102
  tmpLength.value = EXEXCHANGE_DESTINATION;
  message[24]  = tmpLength.c[1];
  message[25]  = tmpLength.c[0];

  // Price scale
  message[26] = PRICE_SCALE;

  // Symbol
  strncpy(&message[27], newOrder->symbol, SYMBOL_LEN);
  
  // Company group ID
  // memcpy(&message[35], MPID_ARCA_DIRECT, 5);
  memcpy(&message[35], ARCA_DIRECT_Config.compGroupID, 5);
  
  // Deliver Comp ID
  message[40] = 0;
  message[41] = 0;
  message[42] = 0;
  message[43] = 0;
  message[44] = 0;
  
  // SenderSubID (5 bytes) = null
  message[45] = 0;
  message[46] = 0;
  message[47] = 0;
  message[48] = 0;
  message[49] = 0;

  // Execution Instructions
  message[50] = '1';

  // Side
  message[51] = newOrder->side;

  // Order Type
  message[52] = ORDER_TYPE_LIMIT;

  // Time In Force
  message[53] = newOrder->timeInForce;  //IOC

  // Rule80A: A, P, E
  message[54] = RULE80A;
  
  // Trading session Id: "1", "2", "3"
  memcpy(&message[55], TRADING_SESSION_ID, 4);
  
  // Account
  strncpy(&message[59], TradingAccount.DIRECT[newOrder->tradingAccount], 10);
  
  // ISO Flag
  message[69] = 'N';
  
  // Extended Execution instruction
  message[70] = 0;
  
  // ExtendedPNP (1 byte) = null
  message[71] = 0;
  
  // NoSelfTrade (1 byte) = 'O'
  message[72] = 'O';
  
  // ProactiveIfLocked (1 byte) = null
  message[73] = 0;
  
  // Filler (1 byte)
  message[74] = 0;
  
  //Terminator
  message[75] = '\n';
  
  if (Send_ARCA_DIRECT_Msg(message, NEW_ORDER_MSG_LEN_V1) == SUCCESS)
  {
    ManualArcaDirectOrderInfo[newOrder->clOrdID].shares = newOrder->orderQty;
    ManualArcaDirectOrderInfo[newOrder->clOrdID].side = newOrder->side;
    strncpy(ManualArcaDirectOrderInfo[newOrder->clOrdID].symbol, newOrder->symbol, SYMBOL_LEN);
    strncpy(manualOrderInfo[newOrder->clOrdID].symbol, newOrder->symbol, SYMBOL_LEN);
    
    t_DataBlock dataBlock;
    dataBlock.msgLen = NEW_ORDER_MSG_LEN_V1;
    memcpy(dataBlock.msgContent, message, NEW_ORDER_MSG_LEN_V1);
    Manual_Order_Account_Mapping[newOrder->clOrdID] = newOrder->tradingAccount;
    dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID] = newOrder->tradingAccount;

    Add_ARCA_DIRECT_DataBlockToCollection(&dataBlock);
    
    SaveARCA_DIRECT_SeqNumToFile();
    
    return SUCCESS;
  }
  else
  {
    return ERROR;
  }
}

/****************************************************************************
- Function name:  BuildAndSend_ARCA_DIRECT_OrderMsg_V3
- Input:      + newOrder
- Output:   N/A
- Return:   Success or Failure
- Description:  
        
- Usage:  N/A
****************************************************************************/
int BuildAndSend_ARCA_DIRECT_OrderMsg_V3(t_ARCA_DIRECT_NewOrder *newOrder)
{
  t_IntConverter tmpValue;
  t_ShortConverter tmpLength;

  char message[NEW_ORDER_MSG_LEN_V3 + 1] = "\0";

  // Message type is 'D'
  message[0] = NEW_ORDER_TYPE;

  // Variant (1 byte) = 1
  message[1] = 3;

  // Length
  tmpLength.value = NEW_ORDER_MSG_LEN_V3;
  message[2]  = tmpLength.c[1];
  message[3]  = tmpLength.c[0];

  // Sequence number
  tmpValue.value =  Get_ARCA_DIRECT_OutgoingSeqNum();
  message[4] = tmpValue.c[3];
  message[5] = tmpValue.c[2];
  message[6] = tmpValue.c[1];
  message[7] = tmpValue.c[0];

  // Client order Id
  tmpValue.value =  newOrder->clOrdID;
  message[8] = tmpValue.c[3];
  message[9] = tmpValue.c[2];
  message[10] = tmpValue.c[1];
  message[11] = tmpValue.c[0];
  
  // PCS Link ID
  message[12] = 0;
  message[13] = 0;
  message[14] = 0;
  message[15] = 0;

  // Order Quantity
  tmpValue.value =  newOrder->orderQty;
  message[16] = tmpValue.c[3];
  message[17] = tmpValue.c[2];
  message[18] = tmpValue.c[1];
  message[19] = tmpValue.c[0];
  
  // Strike Price (4 BYTES) 20 - 23
  // MinQty 24-27
  
  // Max Floor 28 - 31
  tmpValue.value = newOrder->maxFloor;
  message[28] = tmpValue.c[3];
  message[29] = tmpValue.c[2];
  message[30] = tmpValue.c[1];
  message[31] = tmpValue.c[0];
  
  //Display Range 32 35
  // DiscretionOffset 36 39 
  // Peg Difference 40 43
  // StopPx 44 47

  // Price
  tmpValue.value = (int)(newOrder->price * 100.00 + 0.500001);
  message[48] = tmpValue.c[3];
  message[49] = tmpValue.c[2];
  message[50] = tmpValue.c[1];
  message[51] = tmpValue.c[0];
  
  // ExDestination (2 bytes) = 102
  tmpLength.value = EXEXCHANGE_DESTINATION;
  message[52]  = tmpLength.c[1];
  message[53]  = tmpLength.c[0];

  // Price scale
  //message[26] = PRICE_SCALE;
  
  //Underlying Quantity 54 - 55
  // Put Cal 56
  // LocalOrAway 57
  
  //Price Scale
  message[58] = PRICE_SCALE;
  
  // Corporate Action 59
  // Open Close 60
  
  // Symbol
  strncpy(&message[61], newOrder->symbol, SYMBOL_LEN);
  
  // Strike Date 69-76
  
  // Company group ID
  memcpy(&message[77], ARCA_DIRECT_Config.compGroupID, 5);
  
  // Deliver Comp ID 82-86
  
  
  // SenderSubID 87-91 
  
  // Execution Instructions 92
  message[92] = '1';

  // Side
  message[93] = newOrder->side;

  // Order Type
  message[94] = ORDER_TYPE_LIMIT;

  // Time In Force
  message[95] = newOrder->timeInForce;  

  // Rule80A: A, P, E
  message[96] = RULE80A;
  
  // Customer Or Firm
  message[97] = '0';
  
  // Trading session Id: "1", "2", "3"
  memcpy(&message[98], TRADING_SESSION_ID, 4);
  
  // Account
  strncpy(&message[102], TradingAccount.DIRECT[newOrder->tradingAccount], 10);
  
  // Optional Data 112-127
  
  // Clearing Firm 128-132
  
  // Clearing Account 133-137
  
  // Expire Time Flag 138
  
  // ExpireTime 139-142
  
  // Effective Time 143 146
  
  // DiscretionInst 147
  // Proactive Discretion 148
  // ProactiveIfLocked 149
  
  // ExecBroker 150-154
  
  // ISO Flag
  message[155] = 'N';
  
  // Symbol Type 156
  
  // Extended Execution instruction
  message[157] = 0;
  
  // ExtendedPNP (1 byte) = null
  message[158] = 0;
  
  // NoSelfTrade (1 byte) = 'O'
  message[159] = 'O';
  
  // Filler (1 byte)
  message[160] = 0;
  
  //Terminator
  message[163] = '\n';
  
  if (Send_ARCA_DIRECT_Msg(message, NEW_ORDER_MSG_LEN_V3) == SUCCESS)
  {
    ManualArcaDirectOrderInfo[newOrder->clOrdID].shares = newOrder->orderQty;
    ManualArcaDirectOrderInfo[newOrder->clOrdID].side = newOrder->side;
    strncpy(ManualArcaDirectOrderInfo[newOrder->clOrdID].symbol, newOrder->symbol, SYMBOL_LEN);
    strncpy(manualOrderInfo[newOrder->clOrdID].symbol, newOrder->symbol, SYMBOL_LEN);
    
    t_DataBlock dataBlock;
    dataBlock.msgLen = NEW_ORDER_MSG_LEN_V3;
    memcpy(dataBlock.msgContent, message, NEW_ORDER_MSG_LEN_V3);
    Manual_Order_Account_Mapping[newOrder->clOrdID] = newOrder->tradingAccount;
    dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID] = newOrder->tradingAccount;

    Add_ARCA_DIRECT_DataBlockToCollection(&dataBlock);
    
    SaveARCA_DIRECT_SeqNumToFile();
    
    return SUCCESS;
  }
  else
  {
    return ERROR;
  }
}

/****************************************************************************
- Function name:  BuildAndSend_ARCA_DIRECT_CancelMsg
- Input:      + newOrder
- Output:   N/A
- Return:   Success or Failure
- Description:  
        
- Usage:  N/A
****************************************************************************/
int BuildAndSend_ARCA_DIRECT_CancelMsg(const t_ARCA_DIRECT_CancelOrder *cancelOrder)
{
  /*
  - API version: ArcaDirect 4.0
  - Date updated: 07-Jul-2009
  - Modified by: Luan Vo-Kinh
  - Order Cancel Message Equities (Variant 1), it includes the following fields:
    + Message Type (1 byte) = 'F'
    + Variant (1 byte) = 1
    + Length (2 bytes) = 72
    + Sequence Number (4 bytes) = Client-assigned sequence number
    + OrderID (8 bytes) = NYSE Arca assigned OrderID. 
    + Client Order Id (4 bytes) = A client-assigned ID for this order
    + Strike Price (4 bytes) = null
    + Under Qty (2 bytes) = null
    + ExDestination (2 bytes) = 102
    + Corporate Action (1 byte) = null
    + Pull or Call (1 byte) = null
    + Bulk Cancel (1 byte) = null
    + Open or Close (1 byte) = null
    + Symbol (8 bytes) = Stock symbol
    + Strike Date (8 bytes) = null
    + Side (1 byte) = '1', '2', or '5'
    + Deliver To Comp ID (5 bytes) = null
    + Account (10 bytes) = null
    + Filler (7 byte) = null
    + Message Terminator (1 byte) = '\n'
    => Total byte = 72
  */
  
  char message[CANCEL_ORDER_MSG_LEN] = "\0";
  t_IntConverter tmpValue;
  t_LongConverter tmpLongValue;
  t_ShortConverter tmpLength;

  // Message type is 'F'
  message[0] = CANCEL_ORDER_TYPE;

  // Variant (1 byte) = 1
  message[1] = 1;

  // Length
  tmpLength.value = CANCEL_ORDER_MSG_LEN;
  memcpy(&message[2], tmpLength.c, 2);
  message[2] = tmpLength.c[1];
  message[3] = tmpLength.c[0];

  // Sequence number
  tmpValue.value =  Get_ARCA_DIRECT_OutgoingSeqNum();
  message[4] = tmpValue.c[3];
  message[5] = tmpValue.c[2];
  message[6] = tmpValue.c[1];
  message[7] = tmpValue.c[0];
  
  // ARCA Order Id
  tmpLongValue.value =  cancelOrder->ecnOrderID;
  message[8] = tmpLongValue.c[7];
  message[9] = tmpLongValue.c[6];
  message[10] = tmpLongValue.c[5];
  message[11] = tmpLongValue.c[4];
  message[12] = tmpLongValue.c[3];
  message[13] = tmpLongValue.c[2];
  message[14] = tmpLongValue.c[1];
  message[15] = tmpLongValue.c[0];

  // Client order Id
  tmpValue.value = cancelOrder->origClOrdID;
  message[16] = tmpValue.c[3];
  message[17] = tmpValue.c[2];
  message[18] = tmpValue.c[1];
  message[19] = tmpValue.c[0];
  
  // Strike Price (4 bytes) = null
  
  // Under Qty (2 bytes) = null
  
  // ExDestination (2 bytes) = 102
  tmpLength.value = EXEXCHANGE_DESTINATION;
  message[26] = tmpLength.c[1];
  message[27] = tmpLength.c[0];
  
  // Corporate Action (1 byte) = null
  
  // Pull or Call (1 byte) = null
  
  // Bulk Cancel (1 byte) = null
  
  // Open or Close (1 byte) = null
  
  // Symbol
  strncpy(&message[32], cancelOrder->symbol, SYMBOL_LEN);
  
  // Strike Date (8 bytes) = null

  // Side
  message[48] = cancelOrder->side;
  
  // Deliver To Comp ID (5 bytes) = null
  
  // Account (10 bytes)
  strncpy(&message[54], TradingAccount.DIRECT[cancelOrder->accountSuffix], 10);
  
  // Padding (7 bytes)

  // Terminator
  message[71] = '\n';
  
  if (Send_ARCA_DIRECT_Msg(message, CANCEL_ORDER_MSG_LEN) == SUCCESS)
  {
    t_DataBlock dataBlock;
    dataBlock.msgLen = CANCEL_ORDER_MSG_LEN;
    
    // Set timestamp for current rawdata
    memcpy(dataBlock.msgContent, message, CANCEL_ORDER_MSG_LEN);
    
    Add_ARCA_DIRECT_DataBlockToCollection(&dataBlock);

    return SUCCESS;
  }
  else
  {
    return ERROR;
  }
}

/****************************************************************************
- Function name:  ProcessARCA_DIRECT_LogonAcceptedMsg
- Input:      + dataBlock
- Output:   N/A
- Return:   Success or Failure
- Description:
        
- Usage:    N/A
****************************************************************************/
int ProcessARCA_DIRECT_LogonAcceptedMsg(t_DataBlock *dataBlock)
{
  TraceLog(DEBUG_LEVEL, "Logon to ARCA DIRECT was successful\n");
        
  /*
  Update Outgoing Sequence Number before send any messages to ARCA DIRECT Server
  */
  ARCA_DIRECT_Config.outgoingSeqNum = GetIntNumberBigEndian(&dataBlock->msgContent[OUTGOING_SEQ_NUM_INDEX]);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessARCA_DIRECT_LogonRejectedMsg
- Input:      + dataBlock
- Output:   N/A
- Return:   Success or Failure
- Description:  
        
- Usage:    N/A
****************************************************************************/
int ProcessARCA_DIRECT_LogonRejectedMsg(t_DataBlock *dataBlock)
{
  TraceLog(DEBUG_LEVEL, "ARCA DIRECT logon rejected\n");
  return SUCCESS;
}

/****************************************************************************
- Function name:  Send_ARCA_DIRECT_Msg
- Input:      + message
        + msgLen
- Output:   N/A
- Return:   Success or Failure
- Description:  
- Usage:    N/A
****************************************************************************/
int Send_ARCA_DIRECT_Msg(const char *message, int msgLen)
{
  pthread_mutex_lock(&ADSockMutex);
    if (send(ARCA_DIRECT_Config.socket, message, msgLen, 0) == msgLen)
    {
      pthread_mutex_unlock(&ADSockMutex);
      return SUCCESS;
    }
  pthread_mutex_unlock(&ADSockMutex);
  
  TraceLog(ERROR_LEVEL, "Arca Direct: Send_ARCA_DIRECT_Msg() error\n");
  PrintErrStr(ERROR_LEVEL, "calling send(): ", errno);
  
  orderStatusMgmt[AS_ARCA_DIRECT_INDEX].shouldConnect = NO;
  return ERROR;
}

/****************************************************************************
- Function name:  Add_ARCA_DIRECT_DataBlockToCollection
- Input:      + dataBlock
- Output:   ARCA DIRECT collection will be updated with new data block
- Return:   N/A
- Description:
         
- Usage:    N/A
****************************************************************************/

void Add_ARCA_DIRECT_DataBlockToCollection(t_DataBlock *dataBlock)
{
  // Update Block Length
  dataBlock->blockLen = 4 + 4 + dataBlock->msgLen + 4 + DATABLOCK_ADDITIONAL_INFO_LEN;
  
  // Update additional content 
  AddDataBlockTime(&dataBlock->addContent[DATABLOCK_OFFSET_APP_TIME]);
  memcpy(&dataBlock->addContent[DATABLOCK_OFFSET_FILTER_TIME], &dataBlock->addContent[DATABLOCK_OFFSET_APP_TIME], 8);
  
  // Add data block to collection
  AddDataBlockToCollection(dataBlock, &asRawDataMgmt[AS_ARCA_DIRECT_INDEX]);
  
  // Save data block to file
  SaveDataBlockToFile(&asRawDataMgmt[AS_ARCA_DIRECT_INDEX]);
}

/****************************************************************************
- Function name:  Get_ARCA_DIRECT_OutgoingSeqNum
- Input:      N/A
- Output:   N/A
- Return:   Outgoing sequence number
- Description:
          
- Usage:    N/A
****************************************************************************/
int Get_ARCA_DIRECT_OutgoingSeqNum(void)
{
  return __sync_fetch_and_add(&ARCA_DIRECT_Config.outgoingSeqNum, 1);
}

/****************************************************************************
- Function name:  Receive_ARCA_DIRECT_Msg
- Input:    numBytesToReceive
- Output:   ARCA_DIRECT_Buffer, ArcaDirectMsgReceivedBytes
- Return:   Success or Failure
- Description:  
- Usage:    N/A
****************************************************************************/
int Receive_ARCA_DIRECT_Msg(t_DataBlock* dataBlock)
{ 
  int numReceivedBytes = 0;
  int recvBytes = 0;

  // receive header bytes
  while(numReceivedBytes < ARCA_DIRECT_MSG_HEADER_LEN)
  {
    errno = 0;
    recvBytes = recv(ARCA_DIRECT_Config.socket, &dataBlock->msgContent[numReceivedBytes], ARCA_DIRECT_MSG_HEADER_LEN - numReceivedBytes, 0);

    if(recvBytes > 0)
    {
      numReceivedBytes += recvBytes;
    }
    else if ( (recvBytes == -1) && (errno == EAGAIN) ) //Timeout
    {
      TraceLog(ERROR_LEVEL, "ARCA DIRECT: Socket receive-timeout\n");
      return ERROR;
    }
    else if ( (recvBytes == 0) && (errno == 0) )  //Server Disconnected
    {
      TraceLog(ERROR_LEVEL, "ARCA DIRECT: Connection is terminated\n");
      return ERROR;
    }
    else
    {
      TraceLog(ERROR_LEVEL, "ARCA DIRECT: recv() returned %d, errno: %d\n", numReceivedBytes, errno);
      PrintErrStr(ERROR_LEVEL, "recv(): ", errno);
      return ERROR;
    }
  }

  //receive body message bytes
  t_ShortConverter msgLen;
  msgLen.c[0] = dataBlock->msgContent[ARCA_DIRECT_MSG_HEADER_LEN - 1];
  msgLen.c[1] = dataBlock->msgContent[ARCA_DIRECT_MSG_HEADER_LEN - 2];
  dataBlock->msgLen = (unsigned short)msgLen.value;

  while(numReceivedBytes < dataBlock->msgLen)
  {
    errno = 0;
    recvBytes = recv(ARCA_DIRECT_Config.socket, &dataBlock->msgContent[numReceivedBytes], dataBlock->msgLen - numReceivedBytes, 0);

    if(recvBytes > 0)
    {
      numReceivedBytes += recvBytes;
    }
    else if ( (recvBytes == -1) && (errno == EAGAIN) ) //Timeout
    {
      TraceLog(ERROR_LEVEL, "ARCA DIRECT: Socket receive-timeout.\n");
      return ERROR;
    }
    else if ( (recvBytes == 0) && (errno == 0) )  //Server Disconnected
    {
      TraceLog(ERROR_LEVEL, "ARCA DIRECT: Connection is terminated\n");
      return ERROR;
    }
    else
    {
      TraceLog(ERROR_LEVEL, "ARCA DIRECT: recv() returned %d, errno: %d\n", numReceivedBytes, errno);
      PrintErrStr(ERROR_LEVEL, "recv(): ", errno);
      
      return ERROR;
    }
  }
  
  return SUCCESS;
}

int SaveARCA_DIRECT_SeqNumToFile(void)
{
  return SaveSeqNumToFile("arca_direct_seqs", AS_ARCA_DIRECT_INDEX, &ARCA_DIRECT_Config);
}
