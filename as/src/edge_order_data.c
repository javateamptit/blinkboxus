/*****************************************************************************
**  Project:    Equity Arbitrage Application
**  Filename:   edge_order_data.c
**  Description:  This file contains function definitions that were declared
          in edge_order_data.h  
**  Author:     Dung Tran-Dinh
**  First created:  -----------
**  Last updated: -----------
*****************************************************************************/

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#define _GNU_SOURCE

#include "configuration.h"
#include "order_mgmt_proc.h"
#include "edge_order_data.h"
#include "database_util.h"
#include "query_data_mgmt.h"
#include "raw_data_mgmt.h"
#include "daedalus_proc.h"
#include "hbitime.h"

/****************************************************************************
** Global variables definition
****************************************************************************/

/****************************************************************************
** Function declarations
****************************************************************************/

/****************************************************************************
- Function name:  ParseRawDataForEDGEOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ParseRawDataForEDGEOrder(t_DataBlock dataBlock, int type, int oecType)
{
  //Get timestamp from epoch with format <second from epoch>.<microsecond> (eg. 124212553634.3235235)
  char timeStamp[32];
  long usec;
  
  if( dataBlock.msgContent[2] == 'S' )
  {
    usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]) / 1000;
    sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
    switch ( dataBlock.msgContent[3] )
    {
      // Accepted order: 
      case 'A':
        ProcessAcceptedOrderOfEDGEOrder(dataBlock, type, timeStamp, oecType);
        break;
      
      case 'P':
        ProcessAcceptedExtOrderOfEDGEOrder(dataBlock, type, timeStamp, oecType);
        break;
      
      // Canceled order
      case 'C':
        ProcessCanceledOrderOfEDGEOrder(dataBlock, type, timeStamp, oecType);
        break;
      
      // Executed order
      case 'E':
        ProcessExecutedOrderOfEDGEOrder(dataBlock, type, timeStamp, oecType);
        break;

      // Rejected Message + Extended Rejected Message
      case 'J':
      case 'L':
        ProcessRejectedOrderOfEDGEOrder(dataBlock, type, timeStamp, oecType);
        break;

      // Broken trade
      case 'B':
        ProcessBrokenOrderOfEDGEOrder(dataBlock, type, oecType);
        break;
      
      // Price Correction
      case 'K':
        ProcessPriceCorrectionOfEDGEOrder(dataBlock, type, oecType);
        break;
        
      // System Event
      case 'S':
        if (dataBlock.msgContent[12] == 'S')
        {
          TraceLog(DEBUG_LEVEL, "%s: Received 'Start of Day' message.\n", GetOECName(oecType));
        }
        else
        {
          TraceLog(DEBUG_LEVEL, "%s: Received 'End of Day' message.\n", GetOECName(oecType));
        }
        break;
      
      // Cancel Pending
      case 'I':
        TraceLog(DEBUG_LEVEL, "%s: Received 'Cancel Pending' message, token %14s\n", GetOECName(oecType), &dataBlock.msgContent[12]);
        break;

      default:
        TraceLog(ERROR_LEVEL, "%s: Invalid order message in sequenced packet, msgType = %c ASCII = %d\n", GetOECName(oecType), dataBlock.msgContent[3], dataBlock.msgContent[3]);
        break;
    }
  }
  else if(dataBlock.msgContent[2] == 'U')
  {
    // New Order
    // O: New order (short format), N: New order (extended format)
    // X: Cancel Order
    usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]) / 1000;
    sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
    switch ( dataBlock.msgContent[3] )
    {
      case 'O':
        ProcessNewOrderOfEDGEOrder(dataBlock, type, timeStamp, oecType);
        break;
      case 'N':
        ProcessNewExtOrderOfEDGEOrder(dataBlock, type, timeStamp, oecType);
        break;
      case 'X':
        TraceLog(DEBUG_LEVEL, "%s: Cancel request, token %14s\n", GetOECName(oecType), &dataBlock.msgContent[4]);
        break;
      default:
        TraceLog(ERROR_LEVEL, "%s: Invalid order message in unsequenced packet, msgType = %c ASCII = %d\n", GetOECName(oecType), dataBlock.msgContent[3], dataBlock.msgContent[3] );
        break;
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessNewOrderOfEDGEOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessNewOrderOfEDGEOrder(t_DataBlock dataBlock, int type, char *timeStamp, int oecType)
{
  t_EDGENewOrder newOrder;
  t_OpenOrderInfo openOrder;
  t_OrderDetail orderDetail;
  
  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;

  // YYYY/mm/DD
  char dateTime[32];
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]), &dateTime[0]);
  memcpy(newOrder.date, &dateTime[0], 10);
  newOrder.date[10] = 0;

  // HH:MM:SS.ms
  memcpy( newOrder.timestamp, &dateTime[11], TIME_LENGTH );
  newOrder.timestamp[TIME_LENGTH] = 0;

  // timestamp
  memcpy( openOrder.timestamp, &dateTime[0], TIMESTAMP_LENGTH );
  openOrder.timestamp[TIMESTAMP_LENGTH] = 0;

  // orderDetail
  strcpy( orderDetail.timestamp, openOrder.timestamp );

  strcpy( newOrder.ecn, GetOECName(oecType) );

  // ECNId
  openOrder.ECNId = oecType;
  
  // status
  strcpy( newOrder.status, orderStatus.Sent );
  openOrder.status = orderStatusIndex.Sent;
  
  strcpy( newOrder.msgType, "NewOrder" );

  // Messsage type
  char msgType = dataBlock.msgContent[3];

  // Order ID
  //Remove first two digits account to get orderID
  char fieldToken[16]; 
  memcpy( fieldToken, &dataBlock.msgContent[6], 12 );
  fieldToken[12] = 0;
  newOrder.orderID = atoi( fieldToken );

  // clOrdId
  openOrder.clOrdId = newOrder.orderID;
  
  // "B", "S" or "SS"
  char action = dataBlock.msgContent[18]; 
  char buysell = '0';
  switch ( action )
  {
    case 'B':
      buysell = 'B';
      strcpy( newOrder.side, "BC" );
      openOrder.side = BUY_TO_CLOSE_TYPE;
      break;
    
    case 'S':
      buysell = 'S';
      strcpy( newOrder.side, "S" );     
      openOrder.side = SELL_TYPE;
      break;
    
    case 'T':
      buysell = 'T';
      strcpy( newOrder.side, "SS" );      
      openOrder.side = SHORT_SELL_TYPE;
      break;
    
    default:
      strcpy( newOrder.side, " " );
      openOrder.side = -1;

      TraceLog(ERROR_LEVEL, "%s: Not support side = %d, %c\n", GetOECName(oecType), action, action );
      break;
  }
  
  // Shares
  newOrder.shares = GetIntNumberBigEndian( &dataBlock.msgContent[19] );

  //openOrder.shares
  openOrder.shares = newOrder.shares;
  openOrder.leftShares = openOrder.shares;
  
  // Stock symbol
  strncpy(newOrder.symbol, (char *) &dataBlock.msgContent[23], SYMBOL_LEN - 2);
  newOrder.symbol[6] = 0;
  TrimRight( newOrder.symbol, strnlen(newOrder.symbol, SYMBOL_LEN) );
  
  // symbol
  memcpy(openOrder.symbol, newOrder.symbol, SYMBOL_LEN);
  
  // Price
  newOrder.price = (double)GetIntNumberBigEndian( &dataBlock.msgContent[29] ) / 100000;

  //openOrder.price
  openOrder.price = newOrder.price;
  openOrder.avgPrice = openOrder.price;
  
  //Time In Force
  int tifValue = dataBlock.msgContent[33];
  char _tif[6];
  if(tifValue == 0) 
  {
    strcpy(_tif, "0");
    strcpy( newOrder.timeInForce, "IOC");
    openOrder.tif = IOC_TYPE;
  }
  else if(tifValue > 0) 
  {
    strcpy(_tif, "99999");
    strcpy( newOrder.timeInForce, "DAY");
    openOrder.tif = DAY_TYPE;
  }
  else
  {
    TraceLog(ERROR_LEVEL, "%s: Invalid TimeInForce = %d\n", GetOECName(oecType), tifValue );
    openOrder.tif = -1;
  }

  // Display
  newOrder.display = dataBlock.msgContent[34];
  char _visibility = newOrder.display;
  
  // Special order type
  char orderType = dataBlock.msgContent[35];
  
  // Extended hours eligible
  char extHrsEligible = dataBlock.msgContent[36];
  
  newOrder.capacity = dataBlock.msgContent[37];
  
  char routeOutEligibility = dataBlock.msgContent[38];
  
  // Inter-market sweep (ISO) eligibility
  newOrder.eligibility = dataBlock.msgContent[39];
  
  int isISO = 0;
  short bboID = -1;
  unsigned char bbReason;
  if (newOrder.eligibility == 'Y')
  {
    isISO = 1;
    bboID = GetShortNumber(&dataBlock.addContent[DATABLOCK_OFFSET_BBO_ID]);
  }
  
  newOrder.bboId = bboID;
  
  // Sequence Number
  newOrder.seqNum = 0;
  
  //orderDetail.msgContent
  orderDetail.length = sprintf( orderDetail.msgContent, "%d=%c\001%d=%d\001%d=%c\001%d=%d\001%d=%.8s\001%d=%lf\001%d=%d\001%d=%c\001%d=%c\001%d=%c\001%d=%c\001%d=%c\001%d=%c\001", 
          fieldIndexList.MessageType, msgType, 
          fieldIndexList.Token, newOrder.orderID, 
          fieldIndexList.Action, action, 
          fieldIndexList.Shares, newOrder.shares, 
          fieldIndexList.Stock, newOrder.symbol, 
          fieldIndexList.Price, newOrder.price, 
          fieldIndexList.TimeInForce, tifValue,
          fieldIndexList.Display, newOrder.display, 
          fieldIndexList.Capacity, newOrder.capacity, 
          fieldIndexList.Eligibility, newOrder.eligibility,
          fieldIndexList.SpecialOrderType, orderType,
          fieldIndexList.ExtendedHrsEligible, extHrsEligible,
          fieldIndexList.RouteOutEligibility, routeOutEligibility);
  
  strcpy( newOrder.msgContent, orderDetail.msgContent );
  
  /*------------------------------------------------*/
  // Update database
  if (dataBlock.addContent[DATABLOCK_OFFSET_LAUNCH] == EXIT_VALUE)
  {
    // Get Entry/Exit
    bbReason = OJ_BB_REASON_EXIT;
    strcpy(newOrder.entryExit, "Exit");
    
    strcpy(newOrder.ecnLaunch, " ");
    strcpy(newOrder.askBid, " ");
    
    newOrder.entryExitRefNum = *(unsigned short *)&dataBlock.addContent[DATABLOCK_OFFSET_ENTRY_EXIT_REF];
  }
  else
  {
    // Get Entry/Exit
    bbReason = OJ_BB_REASON_ENTRY;
    strcpy(newOrder.entryExit, "Entry");

    // Get ECN launch and Ask/Bid launch
    GetECNLaunchInfo(dataBlock.addContent[DATABLOCK_OFFSET_LAUNCH], newOrder.ecnLaunch, newOrder.askBid);
  }

  // Visible shares
  newOrder.visibleShares = GetIntNumber(&dataBlock.addContent[DATABLOCK_OFFSET_VISIBLE_SIZE]);

  //Cross Id
  newOrder.crossId = GetIntNumber(&dataBlock.addContent[DATABLOCK_OFFSET_CROSS_ID]);
  openOrder.crossId = newOrder.crossId;

  // Trade type
  newOrder.tradeType = NORMALLY_TRADE;
  openOrder.tradeType = newOrder.tradeType;

  //openOrder.tsId
  openOrder.tsId = tradeServersInfo.config[type].tsId;

  //newOrder.tsId
  newOrder.tsId = type;
  
  // Reset no trade duration
  time_t t = (time_t)hbitime_seconds();
  struct tm tm = *localtime(&t);

  NoTradesDurationMgt[type].currentNoTradesDurationInSecond = 0;
  NoTradesDurationMgt[type].alertThresholdInMinute = TradeTimeAlertConfig[3600 * tm.tm_hour + 60 * tm.tm_min + tm.tm_sec];

  sprintf(NoTradesDurationMgt[type].startTimeStamp, "%d:%d", tm.tm_hour, tm.tm_min);

  newOrder.tradingAccount = dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID];
  openOrder.account = dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID];
  
  // Call function to add a open order
  ProcessAddASOpenOrder( &openOrder, &orderDetail );
  
  // Call function to update New Order of EDGE in Manual Order
  ProcessInsertNewOrder4EDGEQueryToCollection(&newOrder);

  int isDuplicated = 0;
  int formatedOrderId;
  memcpy( fieldToken, &dataBlock.msgContent[6], 12);
  fieldToken[12] = 0;
  formatedOrderId = atoi( fieldToken );
  
  if (AddOrderIdToList(&__new, formatedOrderId) == ERROR)
  {
    isDuplicated = 1;
  }
  
  char account[32];
  GetAccountName(oecType, dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID], account);

  int _venueType = oecType - TYPE_BASE;
  if (isDuplicated == 0)
  {
    AddOJNewOrderRecord(timeStamp, newOrder.symbol, _venueType, buysell, newOrder.price, newOrder.shares, newOrder.orderID, _tif, _visibility, isISO, account, NULL, newOrder.capacity, newOrder.crossId, bbReason);
  }
  
  //Add ISO Order to ISO Collection for ISO Flight File creation
  if ((isISO == 1) && (isDuplicated == 0))
  {
    t_ISOOrderInfo isoOrderInfo;
    
    strcpy(isoOrderInfo.strTime, timeStamp);
    
    sprintf(isoOrderInfo.strVenueID, "%d", _venueType);
    
    isoOrderInfo.strBuysell[0] = buysell;
    isoOrderInfo.strBuysell[1] = 0;
    
    sprintf(isoOrderInfo.strPrice, "%.2lf", newOrder.price);
    
    sprintf(isoOrderInfo.strShares, "%d", newOrder.shares);
    
    sprintf(isoOrderInfo.strSequenceID, "%d", newOrder.orderID);
    
    isoOrderInfo.strCapacity[0] = newOrder.capacity;
    isoOrderInfo.strCapacity[1] = 0;
    
    strcpy(isoOrderInfo.strAccount, account);
    
    isoOrderInfo.strRoutingInfo[0] = 0;
    
    AddISOOrderToCollection(newOrder.crossId, bboID, newOrder.symbol, &isoOrderInfo);
  }
  
  CheckAndProcessRawdataInWaitListForOrderID(newOrder.orderID);
  
  return SUCCESS;
}


/****************************************************************************
- Function name:  ProcessNewExtOrderOfEDGEOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessNewExtOrderOfEDGEOrder(t_DataBlock dataBlock, int type, char *timeStamp, int oecType)
{
  t_EDGENewOrder newOrder;
  t_OpenOrderInfo openOrder;
  t_OrderDetail orderDetail;

  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;

  // YYYY/mm/DD
  char dateTime[32];
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]), &dateTime[0]);
  memcpy(newOrder.date, &dateTime[0], 10);
  newOrder.date[10] = 0;

  // HH:MM:SS.ms
  memcpy( newOrder.timestamp, &dateTime[11], TIME_LENGTH );
  newOrder.timestamp[TIME_LENGTH] = 0;

  // timestamp
  memcpy( openOrder.timestamp, &dateTime[0], TIMESTAMP_LENGTH );
  openOrder.timestamp[TIMESTAMP_LENGTH] = 0;

  // orderDetail
  strcpy( orderDetail.timestamp, openOrder.timestamp );
  
  strcpy( newOrder.ecn, GetOECName(oecType) );

  // ECNId
  openOrder.ECNId = oecType;
    
  // Status: Sent, Live, Rejected, Closed, BrokenTrade, CanceledByECN, 
  //      CXLSent, CancelACK, CancelRejected, CancelPending, CanceledByUser
  strcpy( newOrder.status, orderStatus.Sent );
  
  // status
  openOrder.status = orderStatusIndex.Sent;
  
  strcpy( newOrder.msgType, "NewOrder" );

  // Messsage type
  char msgType = dataBlock.msgContent[3];

  // Order ID
  //Remove first two digits account to get orderID
  char fieldToken[16]; 
  memcpy( fieldToken, &dataBlock.msgContent[6], 12 );
  fieldToken[12] = 0;
  newOrder.orderID = atoi( fieldToken );

  // clOrdId
  openOrder.clOrdId = newOrder.orderID;
  
  // "B", "S" or "SS"
  char action = dataBlock.msgContent[18]; 
  char buysell = '0';
  switch ( action )
  {
    case 'B':
      buysell = 'B';
      strcpy( newOrder.side, "BC" );
      openOrder.side = BUY_TO_CLOSE_TYPE;
      break;
    
    case 'S':
      buysell = 'S';
      strcpy( newOrder.side, "S" );     
      openOrder.side = SELL_TYPE;
      break;
    
    case 'T':
      buysell = 'T';
      strcpy( newOrder.side, "SS" );      
      openOrder.side = SHORT_SELL_TYPE;
      break;
    
    default:
      strcpy( newOrder.side, " " );
      openOrder.side = -1;

      TraceLog(ERROR_LEVEL, "%s: Not support side = %d, %c\n", GetOECName(oecType), action, action );
      break;
  }
  
  // Shares
  newOrder.shares = GetIntNumberBigEndian( &dataBlock.msgContent[19] );

  //openOrder.shares
  openOrder.shares = newOrder.shares;
  openOrder.leftShares = openOrder.shares;
  
  /* Special notice for DIRECT EDGE:
    Currently we don't know exactly the way to convert CMS root/suffix to original symbol (sent from Book)
    So TS will save the original symbol to data at special offet, so that AS will take this original symbol to update data structure
    Special offset: In dataBlock.msgContent, right after dataBlock.msgLen, size: SYMBOL_LEN
  */

  strncpy(newOrder.symbol, (char *) &dataBlock.msgContent[67], SYMBOL_LEN);
  
  // symbol
  memcpy(openOrder.symbol, newOrder.symbol, SYMBOL_LEN);
  
  // Price
  newOrder.price = (double)GetIntNumberBigEndian( &dataBlock.msgContent[29] ) / 100000;

  //openOrder.price
  openOrder.price = newOrder.price;
  openOrder.avgPrice = openOrder.price;
  
  //Time In Force
  int tifValue = dataBlock.msgContent[33];
  char _tif[6];
  if(tifValue == 0) 
  {
    strcpy(_tif, "0");
    strcpy( newOrder.timeInForce, "IOC");
    openOrder.tif = IOC_TYPE;
  }
  else if(tifValue > 0) 
  {
    strcpy(_tif, "99999");
    strcpy( newOrder.timeInForce, "DAY");
    openOrder.tif = DAY_TYPE;
  }
  else
  {
    TraceLog( ERROR_LEVEL, "%s: Invalid TimeInForce = %d\n", GetOECName(oecType), tifValue );
    openOrder.tif = -1;
  }

  // Display
  newOrder.display = dataBlock.msgContent[34];
  char _visibility = newOrder.display;
  
  // Special order type
  char orderType = dataBlock.msgContent[35];
  
  // Extended hours eligible
  char extHrsEligible = dataBlock.msgContent[36];
  
  newOrder.capacity = dataBlock.msgContent[37];
  
  char routeOutEligibility = dataBlock.msgContent[38];
  
  // Inter-market sweep (ISO) eligibility
  newOrder.eligibility = dataBlock.msgContent[39];
  
  char routingDeliveryMethod = dataBlock.msgContent[40];
  
  short routeStrategy = GetShortNumberBigEndian(&dataBlock.msgContent[41]);
  
  switch (routeStrategy)
  {
    case 1:
      memcpy(newOrder.routingInfo, "ROUT", 4);
      break;
    case 2:
      memcpy(newOrder.routingInfo, "ROUD", 4);
      break;
    case 3:
      memcpy(newOrder.routingInfo, "ROUE", 4);
      break;
    case 4:
      memcpy(newOrder.routingInfo, "ROUX", 4);
      break;
    case 5:
      memcpy(newOrder.routingInfo, "ROUZ", 4);
      break;
    case 6:
      memcpy(newOrder.routingInfo, "ROUQ", 4);
      break;
    case 7:
      memcpy(newOrder.routingInfo, "RDOT", 4);
      break;
    case 8:
      memcpy(newOrder.routingInfo, "RDOX", 4);
      break;
    case 9:
      memcpy(newOrder.routingInfo, "ROPA", 4);
      break;
    case 10:
      memcpy(newOrder.routingInfo, "ROBA", 4);
      break;
    case 11:
      memcpy(newOrder.routingInfo, "ROBX", 4);
      break;
    case 12:
      memcpy(newOrder.routingInfo, "INET", 4);
      break;
    case 13:
      memcpy(newOrder.routingInfo, "IOCT", 4);
      break;
    case 14:
      memcpy(newOrder.routingInfo, "IOCX", 4);
      break;
    case 15:
      memcpy(newOrder.routingInfo, "ISAM", 4);
      break;
    case 16:
      memcpy(newOrder.routingInfo, "ISPA", 4);
      break;
    case 17:
      memcpy(newOrder.routingInfo, "ISBA", 4);
      break;
    case 18:
      memcpy(newOrder.routingInfo, "ISBX", 4);
      break;
    case 19:
      memcpy(newOrder.routingInfo, "ISCB", 4);
      break;
    case 20:
      memcpy(newOrder.routingInfo, "ISCX", 4);
      break;
    case 21:
      memcpy(newOrder.routingInfo, "ISCN", 4);
      break;
    case 22:
      memcpy(newOrder.routingInfo, "ISGA", 4);
      break;
    case 23:
      memcpy(newOrder.routingInfo, "ISGX", 4);
      break;
    case 24:
      memcpy(newOrder.routingInfo, "ISLF", 4);
      break;
    case 25:
      memcpy(newOrder.routingInfo, "ISNQ", 4);
      break;
    case 26:
      memcpy(newOrder.routingInfo, "ISNY", 4);
      break;
    case 27:
      memcpy(newOrder.routingInfo, "ISPX", 4);
      break;
    case 29:
      memcpy(newOrder.routingInfo, "ROUC", 4);
      break;
    case 30:
      memcpy(newOrder.routingInfo, "ROLF", 4);
      break;
    case 31:
      memcpy(newOrder.routingInfo, "ISBY", 4);
      break;
    case 32:
      memcpy(newOrder.routingInfo, "SWPA", 4);
      break;
    case 33:
      memcpy(newOrder.routingInfo, "SWPB", 4);
      break;
    case 34:
      memcpy(newOrder.routingInfo, "IOCM", 4);
      break;
    case 35:
      memcpy(newOrder.routingInfo, "ICMT", 4);
      break;
    case 36:
      memcpy(newOrder.routingInfo, "ROOC", 4);
      break;
    case 37:
      memcpy(newOrder.routingInfo, "ROBY", 4);
      break;
    case 38:
      memcpy(newOrder.routingInfo, "ROBB", 4);
      break;
    case 39:
      memcpy(newOrder.routingInfo, "ROCO", 4);
      break;
    case 40:
      memcpy(newOrder.routingInfo, "SWPC", 4);
      break;
    case 42:
      memcpy(newOrder.routingInfo, "RMPT", 4);
      break;
    default:
      TraceLog(DEBUG_LEVEL, "%s: Invalid value %d in RouteStrategy field\n", GetOECName(oecType), routeStrategy);
      break;
  }
  newOrder.routingInfo[4] = 0;
  
  newOrder.minQty = GetIntNumberBigEndian( &dataBlock.msgContent[43] );

  // Max Floor
  newOrder.maxFloor = GetIntNumberBigEndian( &dataBlock.msgContent[47] );

  // fieldPegDifference
  int fieldPegDifference = dataBlock.msgContent[51];

  // fieldDiscretionaryOffset
  int fieldDiscretionaryOffset = dataBlock.msgContent[52];

  long expireTime = GetLongNumberBigEndian( (char *)&dataBlock.msgContent[53]);
  
  // root symbol
  char rootSymbol[7];
  strncpy(rootSymbol, (char *) &dataBlock.msgContent[23], 6);
  rootSymbol[6] = 0;
  TrimRight( rootSymbol, strlen(rootSymbol) );
  
  // symbol suffix
  char symbolSuffix[7];
  strncpy(symbolSuffix, (char *) &dataBlock.msgContent[61], 6);
  symbolSuffix[6] = 0;
  TrimRight( symbolSuffix, strlen(symbolSuffix) );

  int isISO = 0;
  short bboID = -1;
  unsigned char bbReason;
  if (newOrder.eligibility == 'Y')
  {
    isISO = 1;
    bboID = GetShortNumber(&dataBlock.addContent[DATABLOCK_OFFSET_BBO_ID]);
  }
  
  newOrder.bboId = bboID;
  
  /***********************************************************/
  
  // Sequence Number
  newOrder.seqNum = 0;
  
  //orderDetail.msgContent
  orderDetail.length = sprintf( orderDetail.msgContent, "%d=%c\001%d=%d\001%d=%c\001%d=%d\001%d=%.8s\001%d=%lf\001%d=%d\001%d=%c\001%d=%d\001%d=%d\001%d=%d\001%d=%d\001%d=%ld\001%d=%.6s\001%d=%c\001%d=%c\001%d=%s\001%d=%c\001%d=%c\001%d=%c\001%d=%c\001", 
          fieldIndexList.MessageType, msgType, 
          fieldIndexList.Token, newOrder.orderID, 
          fieldIndexList.Action, action, 
          fieldIndexList.Shares, newOrder.shares, 
          fieldIndexList.Stock, rootSymbol, 
          fieldIndexList.Price, newOrder.price, 
          fieldIndexList.TimeInForce, tifValue,
          fieldIndexList.Display, newOrder.display, 
          fieldIndexList.MinQty, newOrder.minQty, 
          fieldIndexList.MaxFloor, newOrder.maxFloor, 
          fieldIndexList.PegDifference, fieldPegDifference, 
          fieldIndexList.DiscretionOffset, fieldDiscretionaryOffset, 
          fieldIndexList.ExpireTime, expireTime, 
          fieldIndexList.SymbolSuffix, symbolSuffix, 
          fieldIndexList.Capacity, newOrder.capacity, 
          fieldIndexList.Eligibility, newOrder.eligibility,
          fieldIndexList.ExecBroker, newOrder.routingInfo,
          fieldIndexList.RouteOutEligibility, routeOutEligibility,
          fieldIndexList.RoutingDeliveryMethod, routingDeliveryMethod,
          fieldIndexList.SpecialOrderType, orderType,
          fieldIndexList.ExtendedHrsEligible, extHrsEligible);
  
  strcpy( newOrder.msgContent, orderDetail.msgContent );
  
  /*------------------------------------------------*/
  // Update database
  if (dataBlock.addContent[DATABLOCK_OFFSET_LAUNCH] == EXIT_VALUE)
  {
    // Get Entry/Exit
    bbReason = OJ_BB_REASON_EXIT;
    strcpy(newOrder.entryExit, "Exit");
    
    strcpy(newOrder.ecnLaunch, " ");
    strcpy(newOrder.askBid, " ");
    
    newOrder.entryExitRefNum = *(unsigned short *)&dataBlock.addContent[DATABLOCK_OFFSET_ENTRY_EXIT_REF];
  }
  else
  {
    // Get Entry/Exit
    bbReason = OJ_BB_REASON_ENTRY;
    strcpy(newOrder.entryExit, "Entry");

    // Get ECN launch and Ask/Bid launch
    GetECNLaunchInfo(dataBlock.addContent[DATABLOCK_OFFSET_LAUNCH], newOrder.ecnLaunch, newOrder.askBid);
  }

  // Visible shares
  newOrder.visibleShares = GetIntNumber(&dataBlock.addContent[DATABLOCK_OFFSET_VISIBLE_SIZE]);

  //Cross Id
  newOrder.crossId = GetIntNumber(&dataBlock.addContent[DATABLOCK_OFFSET_CROSS_ID]);
  openOrder.crossId = newOrder.crossId;

  // Trade type
  newOrder.tradeType = NORMALLY_TRADE;
  openOrder.tradeType = newOrder.tradeType;

  //openOrder.tsId
  openOrder.tsId = tradeServersInfo.config[type].tsId;

  //newOrder.tsId
  newOrder.tsId = type;
  
  // Reset no trade duration
  time_t t = (time_t)hbitime_seconds();
  struct tm tm = *localtime(&t);

  NoTradesDurationMgt[type].currentNoTradesDurationInSecond = 0;
  NoTradesDurationMgt[type].alertThresholdInMinute = TradeTimeAlertConfig[3600 * tm.tm_hour + 60 * tm.tm_min + tm.tm_sec];

  sprintf(NoTradesDurationMgt[type].startTimeStamp, "%d:%d", tm.tm_hour, tm.tm_min);

  newOrder.tradingAccount = dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID];
  openOrder.account = dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID];
  
  // Call function to add a open order  
  ProcessAddASOpenOrder( &openOrder, &orderDetail );
  
  // Call function to update New Order of EDGE in Manual Order
  ProcessInsertNewOrder4EDGEQueryToCollection(&newOrder);

  //Add a new record to OJ file
  int isDuplicated = 0;
  int formatedOrderId;
  memcpy( fieldToken, &dataBlock.msgContent[6], 12);
  fieldToken[12] = 0;
  formatedOrderId = atoi( fieldToken );
  
  if (AddOrderIdToList(&__new, formatedOrderId) == ERROR)
  {
    isDuplicated = 1;
  }
  
  char account[32];
  GetAccountName(oecType, dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID], account);

  int _venueType = oecType - TYPE_BASE;
  if (isDuplicated == 0)
  {
    char *routingInfo = NULL;
    if (routeOutEligibility != 'N' && routeOutEligibility != 'P') routingInfo = newOrder.routingInfo;
    AddOJNewOrderRecord(timeStamp, newOrder.symbol, _venueType, buysell, newOrder.price, newOrder.shares, newOrder.orderID, _tif, _visibility, isISO, account, routingInfo, newOrder.capacity, newOrder.crossId, bbReason);
  }
  
  //Add ISO Order to ISO Collection for ISO Flight File creation
  if ((isISO == 1) && (isDuplicated == 0))
  {
    t_ISOOrderInfo isoOrderInfo;
    
    strcpy(isoOrderInfo.strTime, timeStamp);
    
    sprintf(isoOrderInfo.strVenueID, "%d", _venueType);
    
    isoOrderInfo.strBuysell[0] = buysell;
    isoOrderInfo.strBuysell[1] = 0;
    
    sprintf(isoOrderInfo.strPrice, "%.2lf", newOrder.price);
    
    sprintf(isoOrderInfo.strShares, "%d", newOrder.shares);
    
    sprintf(isoOrderInfo.strSequenceID, "%d", newOrder.orderID);
    
    isoOrderInfo.strCapacity[0] = newOrder.capacity;
    isoOrderInfo.strCapacity[1] = 0;
    
    strcpy(isoOrderInfo.strAccount, account);
    
    strcpy(isoOrderInfo.strRoutingInfo, newOrder.routingInfo);
    
    AddISOOrderToCollection(newOrder.crossId, bboID, newOrder.symbol, &isoOrderInfo);
  }
  
  CheckAndProcessRawdataInWaitListForOrderID(newOrder.orderID);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessAcceptedOrderOfEDGEOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessAcceptedOrderOfEDGEOrder(t_DataBlock dataBlock, int type, char *timeStamp, int oecType)
{
  t_EDGEAcceptedOrder acceptedOrder;
  t_OpenOrderInfo openOrder;
  t_OrderDetail orderDetail;
  
  /*------------------------------------------------
  Order summary:
  - double price;
  - double avgPrice;
  - char symbol[SYMBOL_LEN];
  - int side;
  - int shares;
  - int leftShares; 
  - int ECNId;
  - int clOrdId;
  - int status;
  - int tif;
  - int tsId;
  - char timestamp[MAX_TIMESTAMP];
  ------------------------------------------------*/

  /*------------------------------------------------
  Order detail:
  - int length;
  - int execId;
  - int isBroken;
  - char timestamp[MAX_TIMESTAMP];
  - char msgContent[MAX_ORDER_MSG];
  ------------------------------------------------*/
  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;
  
  // YYYY/mm/DD
  char dateTime[32];
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]), &dateTime[0]);
  memcpy(acceptedOrder.processedTimestamp, &dateTime[11], TIME_LENGTH);
  acceptedOrder.processedTimestamp[TIME_LENGTH] = 0;
  
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]), &dateTime[0]);
  memcpy(acceptedOrder.date, &dateTime[0], 10);
  acceptedOrder.date[10] = 0;
  
  // HH:MM:SS.ms
  memcpy(acceptedOrder.timestamp, &dateTime[11], TIME_LENGTH);
  acceptedOrder.timestamp[TIME_LENGTH] = 0;

  //openOrder.timestamp
  memcpy(openOrder.timestamp, &dateTime[0], TIMESTAMP_LENGTH);
  openOrder.timestamp[TIMESTAMP_LENGTH] = 0;

  strcpy(orderDetail.timestamp, openOrder.timestamp);

  //openOrder.ECNId
  openOrder.ECNId = oecType;

  // tsId
  acceptedOrder.tsId = type;
  
  // Status: Sent, Live, Rejected, Closed, BrokenTrade, CanceledByECN, 
  //      CXLSent, CancelACK, CancelRejected, CancelPending, CanceledByUser
  strcpy( acceptedOrder.status, orderStatus.Live );
  
  //openOrder.status
  openOrder.status = orderStatusIndex.Live;
  // Messsage type
  strcpy(acceptedOrder.msgType, "AcceptedOrder");
  
  //MsgType
  char msgType = dataBlock.msgContent[3];

  // Ack timestamp
  acceptedOrder.transTime = GetLongNumberBigEndian( (char *)&dataBlock.msgContent[4] );

  // Order ID (strip first two number of account)
  char fieldToken[16];
  memcpy(fieldToken, &dataBlock.msgContent[14], 12);
  fieldToken[12] = 0;
  acceptedOrder.orderID = atoi( fieldToken ); 
  openOrder.clOrdId = acceptedOrder.orderID;
  
  char exOrderId[32];
  exOrderId[0] = dataBlock.msgContent[12];
  exOrderId[1] = dataBlock.msgContent[13];
  sprintf(&exOrderId[2], "%d", openOrder.clOrdId);
  
  // Action: "B", "S" or "SS"
  char action = dataBlock.msgContent[26]; 
  switch ( action )
  {
    case 'B':
      strcpy( acceptedOrder.side, "BC" );
      openOrder.side = BUY_TO_CLOSE_TYPE;
      break;
    
    case 'S':
      strcpy( acceptedOrder.side, "S" );      
      openOrder.side = SELL_TYPE;
      break;
    
    case 'T':
      strcpy( acceptedOrder.side, "SS" );     
      openOrder.side = SHORT_SELL_TYPE;
      break;
    
    default:
      strcpy( acceptedOrder.side, " " );
      openOrder.side = -1;

      TraceLog(ERROR_LEVEL, "%s: Not support side = %d, %c\n", GetOECName(oecType), action, action );
      break;
  }

  // Shares
  acceptedOrder.shares = GetIntNumberBigEndian( &dataBlock.msgContent[27] );

  // shares
  openOrder.shares = acceptedOrder.shares;
  openOrder.leftShares = openOrder.shares;

  // Stock
  strncpy(acceptedOrder.symbol, (char *) &dataBlock.msgContent[31], SYMBOL_LEN - 2);
  acceptedOrder.symbol[6] = 0;
  TrimRight( acceptedOrder.symbol, strnlen(acceptedOrder.symbol, SYMBOL_LEN) ); //Convert all ' ' to null chars
  
  openOrder.account = dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID];
  
  // symbol
  memcpy( openOrder.symbol, acceptedOrder.symbol, SYMBOL_LEN );

  // Price
  acceptedOrder.price = (double)GetIntNumberBigEndian( &dataBlock.msgContent[37] ) / 100000;

  // price
  openOrder.price = acceptedOrder.price;
  openOrder.avgPrice = openOrder.price;

  // Time In Force
  int tifValue = dataBlock.msgContent[41];
  if(tifValue == 0) 
  {
    strcpy( acceptedOrder.timeInForce, "IOC");
    openOrder.tif = IOC_TYPE;
  }
  else if(tifValue > 0) 
  {
    strcpy( acceptedOrder.timeInForce, "DAY");
    openOrder.tif = DAY_TYPE;
  }
  else
  {
    TraceLog(ERROR_LEVEL, "%s: Invalid TimeInForce = %d\n", GetOECName(oecType), tifValue );
    strcpy( acceptedOrder.timeInForce, " ");
    openOrder.tif = -1;
  }

  // Display
  acceptedOrder.display = dataBlock.msgContent[42];
  
  // Special order type
  char orderType = dataBlock.msgContent[43];
  
  // Extended hours eligible
  char extHrsEligible = dataBlock.msgContent[44];
  
  //Order reference number
  acceptedOrder.exOrderId = GetLongNumberBigEndian( (char *)&dataBlock.msgContent[45] );
  
  int _serverId = (type == MANUAL_ORDER) ? MAX_TRADE_SERVER_CONNECTIONS : type;
  ExchangeOrderIdMapping[_serverId][acceptedOrder.orderID % MAX_ORDER_PLACEMENT] = acceptedOrder.exOrderId;
  
  acceptedOrder.capacity = dataBlock.msgContent[53];
  
  char routeOutEligibility = dataBlock.msgContent[54];
  
  char isISO = dataBlock.msgContent[55];
  
  // Sequence number
  acceptedOrder.seqNum = 0;
  
  acceptedOrder.tradingAccount = dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID];

  //orderDetail.msgContent
  orderDetail.length = sprintf( orderDetail.msgContent, "%d=%c\001%d=%ld\001%d=%d\001%d=%c\001%d=%d\001%d=%.8s\001%d=%lf\001%d=%d\001%d=%c\001%d=%c\001%d=%c\001%d=%c\001%d=%c\001%d=%c\001", 
          fieldIndexList.MessageType, msgType, 
          fieldIndexList.TimeStamp, acceptedOrder.transTime,
          fieldIndexList.Token, acceptedOrder.orderID, 
          fieldIndexList.Action, action, 
          fieldIndexList.Shares, acceptedOrder.shares, 
          fieldIndexList.Stock, acceptedOrder.symbol, 
          fieldIndexList.Price, acceptedOrder.price, 
          fieldIndexList.TimeInForce, tifValue,
          fieldIndexList.Display, acceptedOrder.display, 
          fieldIndexList.Capacity, acceptedOrder.capacity, 
          fieldIndexList.Eligibility, isISO,
          fieldIndexList.SpecialOrderType, orderType,
          fieldIndexList.ExtendedHrsEligible, extHrsEligible,
          fieldIndexList.RouteOutEligibility, routeOutEligibility);
  
  strcpy( acceptedOrder.msgContent, orderDetail.msgContent );

  // Update database
  // tsId
  openOrder.tsId = tradeServersInfo.config[type].tsId;

  // Call function to update a open order
  int indexOpenOrder = ProcessUpdateASOpenOrderForOrderACK( &openOrder, &orderDetail );
  if (indexOpenOrder == -2)
  {
    AddDataBlockToWaitList(oecType, WAIT_LIST_TYPE_EDGE_ORDER_ACK, acceptedOrder.orderID, dataBlock, type, timeStamp);
    return SUCCESS;
  }

  // Call function to update Order ACK of EDGE
  ProcessInsertAcceptedOrder4EDGEQueryToCollection(&acceptedOrder);
  
  int formatedOrderId;
  memcpy( fieldToken, &dataBlock.msgContent[14], 12);
  fieldToken[12] = 0;
  formatedOrderId = atoi( fieldToken );
  
  if (AddOrderIdToList(&__ack, formatedOrderId) == ERROR) //Duplicated
  {
    return 0;
  }
  
  char ecnOrderID[32];
  sprintf(ecnOrderID, "%lu", acceptedOrder.exOrderId);
  
  char account[32];
  GetAccountName(oecType, dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID], account);

  int _venueType = oecType - TYPE_BASE;
  AddOJOrderAcceptedRecord(timeStamp, acceptedOrder.symbol, exOrderId, acceptedOrder.orderID, ecnOrderID, _venueType, account, acceptedOrder.capacity);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessAcceptedExtOrderOfEDGEOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessAcceptedExtOrderOfEDGEOrder(t_DataBlock dataBlock, int type, char *timeStamp, int oecType)
{
  /* Special note for DIRECT EDGE:
    Currently we do not know how to convert symbol from CMS symbology (EDGE format) to original symbol used by our system.
    The original symbol was added in the EDGE New Order Extended Format,
    in order to take this original symbol, we must ensure that we have processed rawdata of New Order Extended Format before process Accepted Order Extended Format
  */
  
  /*------------------------------------------------
  Order summary:
  - double price;
  - double avgPrice;
  - char symbol[SYMBOL_LEN];
  - int side;
  - int shares;
  - int leftShares; 
  - int ECNId;
  - int clOrdId;
  - int status;
  - int tif;
  - int tsId;
  - char timestamp[MAX_TIMESTAMP];
  ------------------------------------------------*/

  /*------------------------------------------------
  Order detail:
  - int length;
  - int execId;
  - int isBroken;
  - char timestamp[MAX_TIMESTAMP];
  - char msgContent[MAX_ORDER_MSG];
  ------------------------------------------------*/
  t_EDGEAcceptedOrder acceptedOrder;
  t_OpenOrderInfo openOrder;
  t_OrderDetail orderDetail;
  
  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;

  // tsId
  openOrder.tsId = tradeServersInfo.config[type].tsId;
  
  // Order ID (strip first two number of account)
  char fieldToken[16];
  memcpy(fieldToken, &dataBlock.msgContent[14], 12);
  fieldToken[12] = 0;
  acceptedOrder.orderID = atoi( fieldToken ); 
  openOrder.clOrdId = acceptedOrder.orderID;
  
  char exOrderId[32];
  exOrderId[0] = dataBlock.msgContent[12];
  exOrderId[1] = dataBlock.msgContent[13];
  sprintf(&exOrderId[2], "%d", openOrder.clOrdId);
  
  int indexOpenOrder = CheckOrderIdIndex(openOrder.clOrdId, openOrder.tsId);

  if ((indexOpenOrder == -2) || (indexOpenOrder == -1))
  {
    /*ACK comes before new orders, or some kind of error, put in int wait-list*/
    AddDataBlockToWaitList(oecType, WAIT_LIST_TYPE_EDGE_ORDER_ACK, acceptedOrder.orderID, dataBlock, type, timeStamp);
    return SUCCESS;
  }
  
  // YYYY/mm/DD
  char dateTime[32];
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]), &dateTime[0]);
  memcpy(acceptedOrder.processedTimestamp, &dateTime[11], TIME_LENGTH);
  acceptedOrder.processedTimestamp[TIME_LENGTH] = 0;
  
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]), &dateTime[0]);
  memcpy(acceptedOrder.date, &dateTime[0], 10);
  acceptedOrder.date[10] = 0;
  
  // HH:MM:SS.ms
  memcpy(acceptedOrder.timestamp, &dateTime[11], TIME_LENGTH);
  acceptedOrder.timestamp[TIME_LENGTH] = 0;

  //openOrder.timestamp
  memcpy(openOrder.timestamp, &dateTime[0], TIMESTAMP_LENGTH);
  openOrder.timestamp[TIMESTAMP_LENGTH] = 0;

  strcpy(orderDetail.timestamp, openOrder.timestamp);

  //openOrder.ECNId
  openOrder.ECNId = oecType;

  // tsId
  acceptedOrder.tsId = type;
  
  // Status: Sent, Live, Rejected, Closed, BrokenTrade, CanceledByECN, 
  //      CXLSent, CancelACK, CancelRejected, CancelPending, CanceledByUser
  strcpy( acceptedOrder.status, orderStatus.Live );
  
  //openOrder.status
  openOrder.status = orderStatusIndex.Live;
  // Messsage type
  strcpy(acceptedOrder.msgType, "AcceptedOrder");
  
  //MsgType
  char msgType = dataBlock.msgContent[3];

  // Ack timestamp
  acceptedOrder.transTime = GetLongNumberBigEndian( (char *)&dataBlock.msgContent[4] );
  
  // Action: "B", "S" or "SS"
  char action = dataBlock.msgContent[26]; 
  switch ( action )
  {
    case 'B':
      strcpy( acceptedOrder.side, "BC" );
      openOrder.side = BUY_TO_CLOSE_TYPE;
      break;
    
    case 'S':
      strcpy( acceptedOrder.side, "S" );      
      openOrder.side = SELL_TYPE;
      break;
    
    case 'T':
      strcpy( acceptedOrder.side, "SS" );     
      openOrder.side = SHORT_SELL_TYPE;
      break;
    
    default:
      strcpy( acceptedOrder.side, " " );
      openOrder.side = -1;

      TraceLog(ERROR_LEVEL, "%s: Not support side = %d, %c\n", GetOECName(oecType), action, action );
      break;
  }

  // Shares
  acceptedOrder.shares = GetIntNumberBigEndian( &dataBlock.msgContent[27] );

  // shares
  openOrder.shares = acceptedOrder.shares;
  openOrder.leftShares = openOrder.shares;

  // root symbol
  char rootSymbol[7];
  strncpy(rootSymbol, (char *) &dataBlock.msgContent[31], 6);
  rootSymbol[6] = 0;
  TrimRight( rootSymbol, strlen(rootSymbol) );  //Convert all ' ' to null chars
  
  // symbol suffix
  char symbolSuffix[7];
  strncpy(symbolSuffix, (char *)&dataBlock.msgContent[77], 6);
  symbolSuffix[6] = 0;
  TrimRight( symbolSuffix, strlen(symbolSuffix) );  //Convert all ' ' to null chars
  
  // original symbol
  strncpy(acceptedOrder.symbol, asOpenOrder.orderCollection[indexOpenOrder].orderSummary.symbol, SYMBOL_LEN);
  
  openOrder.account = dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID];

  // symbol
  memcpy( openOrder.symbol, acceptedOrder.symbol, SYMBOL_LEN );

  // Price
  acceptedOrder.price = (double)GetIntNumberBigEndian( &dataBlock.msgContent[37] ) / 100000;

  // price
  openOrder.price = acceptedOrder.price;
  openOrder.avgPrice = openOrder.price;

  // Time In Force
  int tifValue = dataBlock.msgContent[41];
  if(tifValue == 0) 
  {
    strcpy( acceptedOrder.timeInForce, "IOC");
    openOrder.tif = IOC_TYPE;
  }
  else if(tifValue > 0) 
  {
    strcpy( acceptedOrder.timeInForce, "DAY");
    openOrder.tif = DAY_TYPE;
  }
  else
  {
    TraceLog(ERROR_LEVEL, "%s: Invalid TimeInForce = %d\n", GetOECName(oecType), tifValue );
    strcpy( acceptedOrder.timeInForce, " ");
    openOrder.tif = -1;
  }

  // Display
  acceptedOrder.display = dataBlock.msgContent[42];
  
  // Special order type
  char orderType = dataBlock.msgContent[43];
  
  // Extended hours eligible
  char extHrsEligible = dataBlock.msgContent[44];
  
  //Order reference number
  acceptedOrder.exOrderId = GetLongNumberBigEndian( (char *)&dataBlock.msgContent[45] );
  
  int _serverId = (type == MANUAL_ORDER) ? MAX_TRADE_SERVER_CONNECTIONS : type;
  ExchangeOrderIdMapping[_serverId][acceptedOrder.orderID % MAX_ORDER_PLACEMENT] = acceptedOrder.exOrderId;
  
  acceptedOrder.capacity = dataBlock.msgContent[53];
  
  char routeOutEligibility = dataBlock.msgContent[54];
  
  char isISO = dataBlock.msgContent[55];
  
  char routingDeliveryMethod = dataBlock.msgContent[56];
  
  unsigned short routeStrategy = GetShortNumberBigEndian( &dataBlock.msgContent[57] );
  
  char routingInfo[5];
  switch (routeStrategy)
  {
    case 1:
      memcpy(routingInfo, "ROUT", 4);
      break;
    case 2:
      memcpy(routingInfo, "ROUD", 4);
      break;
    case 3:
      memcpy(routingInfo, "ROUE", 4);
      break;
    case 4:
      memcpy(routingInfo, "ROUX", 4);
      break;
    case 5:
      memcpy(routingInfo, "ROUZ", 4);
      break;
    case 6:
      memcpy(routingInfo, "ROUQ", 4);
      break;
    case 7:
      memcpy(routingInfo, "RDOT", 4);
      break;
    case 8:
      memcpy(routingInfo, "RDOX", 4);
      break;
    case 9:
      memcpy(routingInfo, "ROPA", 4);
      break;
    case 10:
      memcpy(routingInfo, "ROBA", 4);
      break;
    case 11:
      memcpy(routingInfo, "ROBX", 4);
      break;
    case 12:
      memcpy(routingInfo, "INET", 4);
      break;
    case 13:
      memcpy(routingInfo, "IOCT", 4);
      break;
    case 14:
      memcpy(routingInfo, "IOCX", 4);
      break;
    case 15:
      memcpy(routingInfo, "ISAM", 4);
      break;
    case 16:
      memcpy(routingInfo, "ISPA", 4);
      break;
    case 17:
      memcpy(routingInfo, "ISBA", 4);
      break;
    case 18:
      memcpy(routingInfo, "ISBX", 4);
      break;
    case 19:
      memcpy(routingInfo, "ISCB", 4);
      break;
    case 20:
      memcpy(routingInfo, "ISCX", 4);
      break;
    case 21:
      memcpy(routingInfo, "ISCN", 4);
      break;
    case 22:
      memcpy(routingInfo, "ISGA", 4);
      break;
    case 23:
      memcpy(routingInfo, "ISGX", 4);
      break;
    case 24:
      memcpy(routingInfo, "ISLF", 4);
      break;
    case 25:
      memcpy(routingInfo, "ISNQ", 4);
      break;
    case 26:
      memcpy(routingInfo, "ISNY", 4);
      break;
    case 27:
      memcpy(routingInfo, "ISPX", 4);
      break;
    case 28:
      memcpy(routingInfo, "ISPR", 4);
      break;
    case 29:
      memcpy(routingInfo, "ROUC", 4);
      break;
    case 30:
      memcpy(routingInfo, "ROLF", 4);
      break;
    case 31:
      memcpy(routingInfo, "ISBY", 4);
      break;
    case 32:
      memcpy(routingInfo, "SWPA", 4);
      break;
    case 33:
      memcpy(routingInfo, "SWPB", 4);
      break;
    case 34:
      memcpy(routingInfo, "IOCM", 4);
      break;
    case 35:
      memcpy(routingInfo, "ICMT", 4);
      break;
    case 36:
      memcpy(routingInfo, "ROOC", 4);
      break;
    case 37:
      memcpy(routingInfo, "ROBY", 4);
      break;
    case 38:
      memcpy(routingInfo, "ROBB", 4);
      break;
    case 39:
      memcpy(routingInfo, "ROCO", 4);
      break;
    case 40:
      memcpy(routingInfo, "SWPC", 4);
      break;
    case 42:
      memcpy(routingInfo, "RMPT", 4);
      break;
    default:
      TraceLog(ERROR_LEVEL, "%s: Invalid value %d in RouteStrategy field\n", GetOECName(oecType), routeStrategy);
      break;
  }
  routingInfo[4] = 0;
  
  int minQty = GetIntNumberBigEndian( &dataBlock.msgContent[59] );

  int maxFloor = GetIntNumberBigEndian( &dataBlock.msgContent[63] );
  
  int fieldPegDifference = dataBlock.msgContent[67];
  
  int fieldDiscretionaryOffset = dataBlock.msgContent[68];
  
  long expireTime = GetLongNumberBigEndian( (char *)&dataBlock.msgContent[69] );
  
  // Sequence number
  acceptedOrder.seqNum = 0;
  
  acceptedOrder.tradingAccount = dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID];

  //orderDetail.msgContent
  orderDetail.length = sprintf( orderDetail.msgContent, "%d=%c\001%d=%ld\001%d=%d\001%d=%c\001%d=%d\001%d=%.8s\001%d=%lf\001%d=%d\001%d=%c\001%d=%d\001%d=%d\001%d=%d\001%d=%d\001%d=%ld\001%d=%.8s\001%d=%c\001%d=%c\001%d=%s\001%d=%c\001%d=%c\001%d=%c\001%d=%c\001", 
          fieldIndexList.MessageType, msgType, 
          fieldIndexList.TimeStamp, acceptedOrder.transTime,
          fieldIndexList.Token, acceptedOrder.orderID, 
          fieldIndexList.Action, action, 
          fieldIndexList.Shares, acceptedOrder.shares, 
          fieldIndexList.Stock, rootSymbol, 
          fieldIndexList.Price, acceptedOrder.price, 
          fieldIndexList.TimeInForce, tifValue,
          fieldIndexList.Display, acceptedOrder.display, 
          fieldIndexList.MinQty, minQty, 
          fieldIndexList.MaxFloor, maxFloor, 
          fieldIndexList.PegDifference, fieldPegDifference, 
          fieldIndexList.DiscretionOffset, fieldDiscretionaryOffset, 
          fieldIndexList.ExpireTime, expireTime, 
          fieldIndexList.SymbolSuffix, symbolSuffix, 
          fieldIndexList.Capacity, acceptedOrder.capacity, 
          fieldIndexList.Eligibility, isISO,
          fieldIndexList.ExecBroker, routingInfo,
          fieldIndexList.RouteOutEligibility, routeOutEligibility,
          fieldIndexList.RoutingDeliveryMethod, routingDeliveryMethod,
          fieldIndexList.SpecialOrderType, orderType,
          fieldIndexList.ExtendedHrsEligible, extHrsEligible);
  
  strcpy( acceptedOrder.msgContent, orderDetail.msgContent );

  // Call function to update a open order
  ProcessUpdateASOpenOrderForOrderACK( &openOrder, &orderDetail );

  // Call function to update Order ACK of EDGE
  ProcessInsertAcceptedOrder4EDGEQueryToCollection(&acceptedOrder);
  
  int formatedOrderId;
  memcpy( fieldToken, &dataBlock.msgContent[14], 12);
  fieldToken[12] = 0;
  formatedOrderId = atoi( fieldToken );
  
  if (AddOrderIdToList(&__ack, formatedOrderId) == ERROR) //Duplicated
  {
    return 0;
  }
  
  char ecnOrderID[32];
  sprintf(ecnOrderID, "%lu", acceptedOrder.exOrderId);
  
  char account[32];
  GetAccountName(oecType, dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID], account);

  int _venueType = oecType - TYPE_BASE;
  AddOJOrderAcceptedRecord(timeStamp, acceptedOrder.symbol, exOrderId, acceptedOrder.orderID, ecnOrderID, _venueType, account, acceptedOrder.capacity);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessExecutedOrderOfEDGEOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessExecutedOrderOfEDGEOrder(t_DataBlock dataBlock, int type, char *timeStamp, int oecType)
{
  t_EDGEExecutedOrder executedOrder;
  t_OrderDetail orderDetail;

  /*------------------------------------------------
  Order detail:
  - int length;
  - int execId;
  - int isBroken;
  - char timestamp[MAX_TIMESTAMP];
  - char msgContent[MAX_ORDER_MSG];
  ------------------------------------------------*/
  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;

  // YYYY/mm/DD
  char dateTime[32];
  
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]), &dateTime[0]);
  memcpy(executedOrder.processedTimestamp, &dateTime[11], TIME_LENGTH );
  executedOrder.processedTimestamp[TIME_LENGTH] = 0;
  
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]), &dateTime[0]);
  memcpy(executedOrder.date, &dateTime[0], 10);
  executedOrder.date[10] = 0;
  
  // HH:MM:SS.ms
  memcpy( executedOrder.timestamp, &dateTime[11], TIME_LENGTH );
  executedOrder.timestamp[TIME_LENGTH] = 0;
  
  // orderDetail.timestamp
  memcpy(orderDetail.timestamp, &dateTime[0], TIMESTAMP_LENGTH);
  orderDetail.timestamp[TIMESTAMP_LENGTH] = 0;

  // Messsage type
  strcpy( executedOrder.msgType, "ExecutedOrder" );

  // MsgType
  char msgType = dataBlock.msgContent[3];
  
  // Sending time
  executedOrder.transTime = GetLongNumberBigEndian( (char *)&dataBlock.msgContent[4] );

  // OrderId (stripped first two digits of account)
  char fieldToken[16];
  memcpy( fieldToken, &dataBlock.msgContent[14], 12);
  fieldToken[12] = 0;
  executedOrder.orderID = atoi( fieldToken );
  
  char exOrderId[32];
  exOrderId[0] = dataBlock.msgContent[12];
  exOrderId[1] = dataBlock.msgContent[13];
  sprintf(&exOrderId[2], "%d", executedOrder.orderID);

  // Shares
  executedOrder.shares = GetIntNumberBigEndian( &dataBlock.msgContent[26] );

  // Price
  executedOrder.price = (double)GetIntNumberBigEndian( &dataBlock.msgContent[30] ) / 100000;

  //Liquidity flag
  strncpy(executedOrder.liquidityIndicator, (char *)&dataBlock.msgContent[34], 5);
  executedOrder.liquidityIndicator[5] = 0;
  TrimRight(executedOrder.liquidityIndicator, strlen(executedOrder.liquidityIndicator));

  //MatchNo
  executedOrder.executionId = GetLongNumberBigEndian( (char *)&dataBlock.msgContent[39] );
  
  executedOrder.seqNum = 0;

  // orderDetail.msgContent
  orderDetail.execId = executedOrder.orderID;
  orderDetail.length = sprintf( orderDetail.msgContent, "%d=%c\001%d=%ld\001%d=%d\001%d=%d\001%d=%lf\001%d=%s\001%d=%ld\001", 
                  fieldIndexList.MessageType, msgType, 
                  fieldIndexList.TimeStamp, executedOrder.transTime, 
                  fieldIndexList.Token, executedOrder.orderID, 
                  fieldIndexList.Shares, executedOrder.shares, 
                  fieldIndexList.Price, executedOrder.price, 
                  fieldIndexList.LiquidityFlag, executedOrder.liquidityIndicator, 
                  fieldIndexList.MatchNo, executedOrder.executionId );
  
  strcpy( executedOrder.msgContent, orderDetail.msgContent );

  // Update database
  int tsId;
  tsId = tradeServersInfo.config[type].tsId;

  // Call function to update a open order (non fill)
  int indexOpenOrder = ProcessUpdateASOpenOrderForExecuted(tsId, executedOrder.orderID, orderStatusIndex.Closed, executedOrder.shares, executedOrder.price, &orderDetail, dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID], executedOrder.liquidityIndicator, &executedOrder.fillFee);
  if (indexOpenOrder == -2)
  {
    AddDataBlockToWaitList(oecType, WAIT_LIST_TYPE_EDGE_ORDER_FILLED, executedOrder.orderID, dataBlock, type, timeStamp);
    return SUCCESS;
  }
  
  if (indexOpenOrder != ERROR)
  {
    strcpy(executedOrder.status, GetOrderStatus(asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status));
    
    executedOrder.leftShares = asOpenOrder.orderCollection[indexOpenOrder].orderSummary.leftShares;   
    executedOrder.avgPrice = asOpenOrder.orderCollection[indexOpenOrder].orderSummary.avgPrice;
  }
  else
  {
  }
  
  executedOrder.entryExitRefNum = *(unsigned short *)&dataBlock.addContent[DATABLOCK_OFFSET_ENTRY_EXIT_REF];
  
  // Call function to update Executed Order of EDGE
  ProcessInsertExecutedOrder4EDGEQueryToCollection(&executedOrder);

  int formatedOrderId;
  memcpy( fieldToken, &dataBlock.msgContent[14], 12);
  fieldToken[12] = 0;
  formatedOrderId = atoi( fieldToken );
  
  if (AddExecDataToList(&__exec, formatedOrderId, executedOrder.executionId) == ERROR)  //Duplicated
  {
    return SUCCESS;
  }
  
  char execID[32];
  sprintf(execID, "%lu", executedOrder.executionId);
  
  char ecnOrderID[32] = "''";
  int _serverId = (type == MANUAL_ORDER) ? MAX_TRADE_SERVER_CONNECTIONS : type;
  if (ExchangeOrderIdMapping[_serverId][executedOrder.orderID % MAX_ORDER_PLACEMENT] != 0)
  {
    sprintf(ecnOrderID, "%lu", ExchangeOrderIdMapping[_serverId][executedOrder.orderID % MAX_ORDER_PLACEMENT]);
  }
  else
  {
    TraceLog(ERROR_LEVEL, "%s: Could not get ExOrderId, orderID: %d\n", __func__, executedOrder.orderID);
  }
  
  char account[32];
  GetAccountName(oecType, dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID], account);

  int _venueType = oecType - TYPE_BASE;
  char buysell = '0';
  if (indexOpenOrder != ERROR)
  {
    switch (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.side)
    {
      case BUY_TO_CLOSE_TYPE:
      case BUY_TO_OPEN_TYPE:
        buysell = 'B';
        break;
      case SELL_TYPE:
        buysell = 'S';
        break;
      case SHORT_SELL_TYPE:
        buysell = 'T';
        break;
    }
    
    AddOJOrderFillRecord(timeStamp, asOpenOrder.orderCollection[indexOpenOrder].orderSummary.symbol, exOrderId, _venueType, buysell, executedOrder.price,
              executedOrder.shares, executedOrder.orderID, executedOrder.liquidityIndicator,
              ecnOrderID, execID, "1", (executedOrder.leftShares == 0)?1:0, account, 0);
  }
  else
  {
    AddOJOrderFillRecord(timeStamp, "", exOrderId, _venueType, buysell, executedOrder.price,
              executedOrder.shares, executedOrder.orderID, executedOrder.liquidityIndicator,
              ecnOrderID, execID, "1", (executedOrder.leftShares == 0)?1:0, account, 0);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessCanceledOrderOfEDGEOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessCanceledOrderOfEDGEOrder(t_DataBlock dataBlock, int type, char *timeStamp, int oecType)
{
  t_EDGECanceledOrder canceledOrder;
  t_OrderDetail orderDetail;

  /*------------------------------------------------
  Order detail:
  - int length;
  - int execId;
  - int isBroken;
  - char timestamp[MAX_TIMESTAMP];
  - char msgContent[MAX_ORDER_MSG];
  ------------------------------------------------*/
  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;
  
  // YYYY/mm/DD
  char dateTime[32];
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]), &dateTime[0]);
    memcpy( canceledOrder.processedTimestamp, &dateTime[11], TIME_LENGTH);
  canceledOrder.processedTimestamp[TIME_LENGTH] = 0;
  
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]), &dateTime[0]);
  memcpy( canceledOrder.date, &dateTime[0], 10);
  canceledOrder.date[10] = 0;
  
  // HH:MM:SS.ms
  memcpy( canceledOrder.timestamp, &dateTime[11], TIME_LENGTH);
  canceledOrder.timestamp[TIME_LENGTH] = 0;

  // orderDetail.timestamp
  memcpy(orderDetail.timestamp, &dateTime[0], TIMESTAMP_LENGTH);
  orderDetail.timestamp[TIMESTAMP_LENGTH] = 0;
  
  // Messsage type
  strcpy(canceledOrder.msgType, "CanceledOrder");

  // Sending Time
  canceledOrder.transTime = GetLongNumberBigEndian( (char *)&dataBlock.msgContent[4] );

  //MsgType
  char msgType = dataBlock.msgContent[3];

  // OrderId (stripped first two digits of account)
  char fieldToken[16];
  memcpy( fieldToken, &dataBlock.msgContent[14], 12);
  fieldToken[12] = 0;
  canceledOrder.orderID = atoi( fieldToken );
  
  // Decremented shares
  canceledOrder.shares = GetIntNumberBigEndian( &dataBlock.msgContent[26] );  
  
  // Reason
  canceledOrder.reason = dataBlock.msgContent[30];
  
  canceledOrder.seqNum = 0;
  
  // orderDetail.msgContent
  orderDetail.length = sprintf(orderDetail.msgContent, "%d=%c\001%d=%ld\001%d=%d\001%d=%d\001%d=%c\001", 
                  fieldIndexList.MessageType, msgType, 
                  fieldIndexList.TimeStamp, canceledOrder.transTime, 
                  fieldIndexList.Token, canceledOrder.orderID, 
                  fieldIndexList.Shares, canceledOrder.shares, 
                  fieldIndexList.Reason, canceledOrder.reason);
  
  strcpy( canceledOrder.msgContent, orderDetail.msgContent );
  
  // Update database
  int tsId;
  tsId = tradeServersInfo.config[type].tsId;

  // Call function to update a open order (non fill)
  int indexOpenOrder = ProcessUpdateASOpenOrderForStatus(tsId, canceledOrder.orderID, orderStatusIndex.CanceledByECN, &orderDetail );
  if (indexOpenOrder == -2)
  {
    AddDataBlockToWaitList(oecType, WAIT_LIST_TYPE_EDGE_ORDER_CANCELED, canceledOrder.orderID, dataBlock, type, timeStamp);
    return SUCCESS;
  }
  
  if (indexOpenOrder != ERROR)
  {
    strcpy(canceledOrder.status, GetOrderStatus(asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status));
  }
  else
  {
    strcpy(canceledOrder.status, orderStatus.CanceledByECN);
  }
  
  canceledOrder.entryExitRefNum = *(unsigned short *)&dataBlock.addContent[DATABLOCK_OFFSET_ENTRY_EXIT_REF];
  
  // Call function to update Canceled Order of EDGE
  ProcessInsertCanceledOrder4EDGEQueryToCollection(&canceledOrder);

  //Add a record to OJ file
  //int AddOJOrderCancelResponseRecord(char *time, char *symbol, int orderID, char *ecnOrderID, char *reason, int venueID, int sizeCanceled, int orderComplete, char *account)
  
  int formatedOrderId;
  memcpy( fieldToken, &dataBlock.msgContent[14], 12);
  fieldToken[12] = 0;
  formatedOrderId = atoi( fieldToken );
  
  if (AddOrderIdToList(&__cancel, formatedOrderId) == ERROR)  //Duplicated
  {
    return SUCCESS;
  }
  
  char reason[1024];
  switch (canceledOrder.reason)
  {
    case 'U':
      strcpy(reason, "User%20requested%20cancel.%20Sent%20in%20response%20to%20a%20Cancel%20Request%20Message");
      break;
    case 'I':
      strcpy(reason, "Immediate%20or%20Cancel%20Order");
      break;
    case 'T':
      strcpy(reason, "Timeout.%20The%20Time%20In%20Force%20for%20this%20order%20has%20expired");
      break;
    case 'S':
      strcpy(reason, "This%20order%20was%20manually%20canceled%20or%20reduced%20by%20Direct%20Edge.");
      break;
    case 'D':
      strcpy(reason, "This%20order%20cannot%20be%20executed%20because%20of%20a%20regulatory%20restriction%20(e.g.:%20trade%20through%20restrictions)");
      break;
    case 'A':
      strcpy(reason, "This%20order%20cannot%20be%20posted%20because%20it%20will%20result%20in%20a%20locked%20or%20crossed%20market.");
      break;
    case 'B':
      strcpy(reason, "Order%20was%20canceled%20due%20to%20Anti-Internalization%20settings.");
      break;
    default:
      sprintf(reason, "Unknown%%20reason,%%20reason%%20code%%20%%3d%%20%c", canceledOrder.reason);
      break;
  }
  
  char ecnOrderID[32] = "''";
  int _serverId = (type == MANUAL_ORDER) ? MAX_TRADE_SERVER_CONNECTIONS : type;
  if (ExchangeOrderIdMapping[_serverId][canceledOrder.orderID % MAX_ORDER_PLACEMENT] != 0)
  {
    sprintf(ecnOrderID, "%lu", ExchangeOrderIdMapping[_serverId][canceledOrder.orderID % MAX_ORDER_PLACEMENT]);
  }
  else
  {
    TraceLog(ERROR_LEVEL, "%s: Could not get ExOrderId, orderID: %d\n", __func__, canceledOrder.orderID);
  }
  
  char account[32];
  GetAccountName(oecType, dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID], account);

  int _venueType = oecType - TYPE_BASE;
  if (indexOpenOrder != ERROR)
  {
    AddOJOrderCancelResponseRecord(timeStamp, asOpenOrder.orderCollection[indexOpenOrder].orderSummary.symbol, canceledOrder.orderID, ecnOrderID, reason, _venueType, canceledOrder.shares, 1, account);
  }
  else
  {
    AddOJOrderCancelResponseRecord(timeStamp, "", canceledOrder.orderID, ecnOrderID, reason, _venueType, canceledOrder.shares, 1, account);
  }
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessRejectedOrderOfEDGEOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessRejectedOrderOfEDGEOrder(t_DataBlock dataBlock, int type, char *timeStamp, int oecType)
{
  t_EDGERejectedOrder rejectedOrder;
  t_OrderDetail orderDetail;

  /*------------------------------------------------
  Order detail:
  - int length;
  - int execId;
  - int isBroken;
  - char timestamp[MAX_TIMESTAMP];
  - char msgContent[MAX_ORDER_MSG];
  ------------------------------------------------*/
  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;

  // YYYY/mm/DD
  char dateTime[32];
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]), &dateTime[0]);
    memcpy( rejectedOrder.processedTimestamp, &dateTime[11], TIME_LENGTH );
  rejectedOrder.processedTimestamp[TIME_LENGTH] = 0;
  
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]), &dateTime[0]);
  memcpy( rejectedOrder.date, &dateTime[0], 10);
  rejectedOrder.date[10] = 0;
  
  // HH:MM:SS.ms
  memcpy( rejectedOrder.timestamp, &dateTime[11], TIME_LENGTH );
  rejectedOrder.timestamp[TIME_LENGTH] = 0;

  // orderDetail.timestamp
  memcpy(orderDetail.timestamp, &dateTime[0], TIMESTAMP_LENGTH);
  orderDetail.timestamp[TIMESTAMP_LENGTH] = 0;

  // Messsage type
  strcpy( rejectedOrder.msgType, "RejectedOrder" );

  // Sending Time
  rejectedOrder.transTime = GetLongNumberBigEndian( (char *)&dataBlock.msgContent[4] );

  // Status: Sent, Live, Rejected, Closed, BrokenTrade, CanceledByECN, 
  // CXLSent, CancelACK, CancelRejected, CancelPending, CanceledByUser
  strcpy( rejectedOrder.status, orderStatus.Rejected );
  
  // MsgType
  char msgType = dataBlock.msgContent[3];

  // OrderId (stripped first two digits of account)
  char fieldToken[16];
  memcpy( fieldToken, &dataBlock.msgContent[14], 12 );
  fieldToken[12] = 0;
  rejectedOrder.orderID = atoi( fieldToken );
  
  //Reason
  rejectedOrder.reason = dataBlock.msgContent[26];
  
  rejectedOrder.seqNum = 0;
  // orderDetail.msgContent
  orderDetail.length = sprintf( orderDetail.msgContent, "%d=%c\001%d=%ld\001%d=%d\001%d=%c\001", 
                  fieldIndexList.MessageType, msgType, 
                  fieldIndexList.TimeStamp, rejectedOrder.transTime, 
                  fieldIndexList.Token, rejectedOrder.orderID, 
                  fieldIndexList.Reason, rejectedOrder.reason );
  
  strcpy( rejectedOrder.msgContent, orderDetail.msgContent );
  
  // Update database
  int tsId;
  tsId = tradeServersInfo.config[type].tsId;

  // Call function to update a open order (non fill)
  int indexOpenOrder = ProcessUpdateASOpenOrderForStatus(tsId, rejectedOrder.orderID, orderStatusIndex.Rejected, &orderDetail );
  if (indexOpenOrder == -2)
  {
    AddDataBlockToWaitList(oecType, WAIT_LIST_TYPE_EDGE_ORDER_REJECTED, rejectedOrder.orderID, dataBlock, type, timeStamp);
    return SUCCESS;
  }

  char reason[1024];

  if (indexOpenOrder != ERROR)
  {
    strcpy(rejectedOrder.status, GetOrderStatus(asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status));
    
    if (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status != orderStatusIndex.CancelRejected)
    {
      switch (rejectedOrder.reason)
      {
        case 'A':
          strcpy(reason, "Order characteristics not supported in current trading session");
          break;
        case 'B':
          strcpy(reason, "Number of orders in the bulk order message exceeded threshold");
          break;
        case 'C':
          strcpy(reason, "Exchange closed");
          break;
        case 'D':
          strcpy(reason, "Invalid Display Type");
          break;
        case 'E':
          strcpy(reason, "Exchange option");
          break;
        case 'F':
          strcpy(reason, "Halted");
          break;
        case 'H':
          strcpy(reason, "Cannot execute in current trading state");
          break;
        case 'I':
          strcpy(reason, "Order Not Found");
          break;
        case 'L':
          strcpy(reason, "Firm not authorized for clearing (invalid firm)");
          break;
        case 'O':
          strcpy(reason, "Other");
          break;
        case 'P':
          strcpy(reason, "Order already in a pending cancel or replace state");
          break;
        case 'Q':
          strcpy(reason, "Invalid quantity");
          break;
        case 'R':
          strcpy(reason, "Risk Control Reject");
          break;
        case 'S':
          strcpy(reason, "Invalid stock");
          break;
        case 'T':
          strcpy(reason, "Test Mode");
          break;
        case 'U':
          strcpy(reason, "Order has an invalid or unsupported characteristic");
          break;
        case 'V':
          strcpy(reason, "Order is rejected because maximum order rate is exceeded.");
          break;
        case 'X':
          strcpy(reason, "Invalid price");
          break;
        default:
          strcpy(reason, "Unknown reason");
          break;
      }


      // Raise order rejected alert
      char _reasonCode[2];
      _reasonCode[0] = rejectedOrder.reason;
      _reasonCode[1] = 0;
      CheckNSendOrderRejectedAlertToAllTT(oecType, _reasonCode, reason, rejectedOrder.orderID);

    }
  }
  else
  {
    strcpy(rejectedOrder.status, orderStatus.CancelRejected);
  }
      
  rejectedOrder.entryExitRefNum = *(unsigned short *)&dataBlock.addContent[DATABLOCK_OFFSET_ENTRY_EXIT_REF];
  
  // Call function to update Rejected Order of EDGE
  ProcessInsertRejectedOrder4EDGEQueryToCollection(&rejectedOrder);

  //Add a new record to OJ file
  //int AddOJOrderRejectRecord(char *time, int orderID, char *ecnOrderID, char *reason, char *symbol, int venueID, char *reasonCode, char *account)

  int formatedOrderId;
  memcpy( fieldToken, &dataBlock.msgContent[14], 12 );
  fieldToken[12] = 0;
  formatedOrderId = atoi( fieldToken );
  
  if (AddOrderIdToList(&__reject, formatedOrderId) == ERROR)  //Duplicated
  {
    return SUCCESS;
  }
  
  char ecnOrderID[2];
  strcpy(ecnOrderID, "''"); //There is no Order Reference Number in this message type
  
  char reasonCode[2];
  reasonCode[0] = rejectedOrder.reason;
  reasonCode[1] = 0;

  switch (rejectedOrder.reason)
  {
    case 'A':
      strcpy(reason, "Order%20characteristics%20not%20supported%20in%20current%20trading%20session");
      break;
    case 'B':
      strcpy(reason, "Number%20of%20orders%20in%20the%20bulk%20order%20message%20exceeded%20threshold");
      break;
    case 'C':
      strcpy(reason, "Exchange%20closed");
      break;
    case 'D':
      strcpy(reason, "Invalid%20Display%20Type");
      break;
    case 'E':
      strcpy(reason, "Exchange%20option");
      break;
    case 'F':
      strcpy(reason, "Halted");
      break;
    case 'H':
      strcpy(reason, "Cannot%20execute%20in%20current%20trading%20state");
      break;
    case 'I':
      strcpy(reason, "Order%20Not%20Found");
      break;
    case 'L':
      strcpy(reason, "Firm%20not%20authorized%20for%20clearing%20(invalid%20firm)");
      break;
    case 'O':
      strcpy(reason, "Other");
      break;
    case 'P':
      strcpy(reason, "Order%20already%20in%20a%20pending%20cancel%20or%20replace%20state");
      break;
    case 'Q':
      strcpy(reason, "Invalid%20quantity");
      break;
    case 'R':
      strcpy(reason, "Risk%20Control%20Reject");
      break;
    case 'S':
      strcpy(reason, "Invalid%20stock");
      break;
    case 'T':
      strcpy(reason, "Test%20Mode");
      break;
    case 'U':
      strcpy(reason, "Order%20has%20an%20invalid%20or%20unsupported%20characteristic");
      break;
    case 'V':
      strcpy(reason, "Order%20is%20rejected%20because%20maximum%20order%20rate%20is%20exceeded.");
      break;
    case 'X':
      strcpy(reason, "Invalid%20price");
      break;
    default:
      strcpy(reason, "Unknown%20reason");
      break;
  }
  
  char account[32];
  GetAccountName(oecType, dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID], account);

  int _venueType = oecType - TYPE_BASE;
  if (indexOpenOrder != ERROR)
  {
    AddOJOrderRejectRecord(timeStamp, rejectedOrder.orderID, ecnOrderID, reason, asOpenOrder.orderCollection[indexOpenOrder].orderSummary.symbol, _venueType, reasonCode, account);
  }
  else
  {
    AddOJOrderRejectRecord(timeStamp, rejectedOrder.orderID, ecnOrderID, reason, "", _venueType, reasonCode, account);
  }
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessBrokenOrderOfEDGEOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessBrokenOrderOfEDGEOrder(t_DataBlock dataBlock, int type, int oecType)
{
  t_EDGEBrokenOrder brokenTrade;
  t_OrderDetail orderDetail;

  /*------------------------------------------------
  Order detail:
  - int length;
  - int execId;
  - int isBroken;
  - char timestamp[MAX_TIMESTAMP];
  - char msgContent[MAX_ORDER_MSG];
  ------------------------------------------------*/
  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;

  // YYYY/mm/DD
  char dateTime[32];
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]), &dateTime[0]);
  memcpy( brokenTrade.date, &dateTime[0], 10);
  brokenTrade.date[10] = 0;
  
  // HH:MM:SS.ms
  memcpy( brokenTrade.timestamp, &dateTime[11], TIME_LENGTH );
  brokenTrade.timestamp[TIME_LENGTH] = 0;

  //orderDetail.timestamp
  memcpy( orderDetail.timestamp, &dateTime[0], TIMESTAMP_LENGTH );
  orderDetail.timestamp[TIMESTAMP_LENGTH] = 0;
  
  // Messsage type
  strcpy( brokenTrade.msgType, "BrokenOrder" );

  // Sending Time
  brokenTrade.transTime = GetLongNumberBigEndian( (char *)&dataBlock.msgContent[4] );
  
  // Status: Sent, Live, Rejected, Closed, BrokenTrade, CanceledByECN, 
  // CXLSent, CancelACK, CancelRejected, CancelPending, CanceledByUser
  strcpy( brokenTrade.status, orderStatus.BrokenTrade );
  
  // MsgType
  char msgType = dataBlock.msgContent[3];

  // Order Id
  char fieldToken[16];
  memcpy( fieldToken, &dataBlock.msgContent[14], 12);
  fieldToken[12] = 0;
  brokenTrade.orderID = atoi( fieldToken );
  
  // MatchNo
  brokenTrade.executionId = GetLongNumberBigEndian( (char *)&dataBlock.msgContent[26] );

  //Reason
  brokenTrade.reason = dataBlock.msgContent[34];
  
  // orderDetail.msgContent
  orderDetail.length = sprintf( orderDetail.msgContent, "%d=%c\001%d=%ld\001%d=%d\001%d=%ld\001%d=%c\001", 
                  fieldIndexList.MessageType, msgType, 
                  fieldIndexList.TimeStamp, brokenTrade.transTime, 
                  fieldIndexList.Token, brokenTrade.orderID, 
                  fieldIndexList.MatchNo, brokenTrade.executionId, 
                  fieldIndexList.Reason, brokenTrade.reason );  
  
  strcpy( brokenTrade.msgContent, orderDetail.msgContent );
  
  // Update database
  int tsId;
  tsId = tradeServersInfo.config[type].tsId;

  // Call function to update a open order (non fill)
  if (ProcessUpdateASOpenOrderForStatus(tsId, brokenTrade.orderID, orderStatusIndex.BrokenTrade, &orderDetail ) == -2)
  {
    AddDataBlockToWaitList(oecType, WAIT_LIST_TYPE_EDGE_ORDER_BROKEN, brokenTrade.orderID, dataBlock, type, "");
    return SUCCESS;
  }
  
  // Call function to update Broken Trade of EDGE
  ProcessInsertBrokenTrade4EDGEQueryToCollection(&brokenTrade);
  
  //Process add broken trade message to as logs
  int indexOpenOrder = CheckOrderIdIndex(brokenTrade.orderID, tsId);
  if (indexOpenOrder >= 0)
  {
    // Add the broken trade message to AS error log
    TraceLog(DEBUG_LEVEL, "%s: Broken Trade message received for symbol %.8s.\n", GetOECName(oecType), asOpenOrder.orderCollection[indexOpenOrder].orderSummary.symbol);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessPriceCorrectionOfEDGEOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessPriceCorrectionOfEDGEOrder(t_DataBlock dataBlock, int type, int oecType)
{
  // Order Id
  char fieldToken[16];
  memcpy( fieldToken, &dataBlock.msgContent[14], 12);
  fieldToken[12] = 0;
  int orderID = atoi( fieldToken );
  
  // MatchNo
  long executionId = GetLongNumberBigEndian( (char *)&dataBlock.msgContent[26] );
  
  // New Execution Price
  double newPrice = (double)GetIntNumberBigEndian( &dataBlock.msgContent[34] ) / 100000;
  
  // Reason
  char reason = dataBlock.msgContent[38];

  TraceLog(DEBUG_LEVEL, "%s: Received 'Price Correction' message, orderID: %d, executionId: %ld, newPrice: %lf, reason: %c\n", GetOECName(oecType), orderID, executionId, newPrice, reason);

  return SUCCESS;
}

/***************************************************************************/
