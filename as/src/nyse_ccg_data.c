/*---------------------------FILE SUMMARY--------------------------------------
|
| Project:    Blinkbox
| Filename:   nyse_ccg_data.c
| Description:  N/A
| Author:     Dung Tran-Dinh
| First created:  Aug 1, 2012
| Last updated: N/A
-----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
|
|       INCLUDE FILES AND DEFINE SEVERAL CONSTANTS
|
-----------------------------------------------------------------------------*/
#include "configuration.h"
#include "order_mgmt_proc.h"
#include "nyse_ccg_data.h"
#include "database_util.h"
#include "query_data_mgmt.h"
#include "raw_data_mgmt.h"
#include "daedalus_proc.h"
#include "hbitime.h"

#include <limits.h>

/*-----------------------------------------------------------------------------
|
|           GLOBAL VARIABLE DEFINITIONS
|
-----------------------------------------------------------------------------*/
int NYSE_CCG_OrderID_Mapping[MAX_TRADE_SERVER_CONNECTIONS][MAX_NYSE_ORDER_ID_MAPPING];
char Nyse_CCG_timestamp[8];

/*-----------------------------------------------------------------------------
|
|           DATA STRUCTURE DEFINITIONS
|
-----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
|
|           FUNCTION DEFINITIONS
|
-----------------------------------------------------------------------------*/
/****************************************************************************
- Function name:  Initialize_NYSE_CCG_CancelRequestMapping
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
void Initialize_NYSE_Order_ID_Mapping(void)
{
  int i, j;

  for (i = 0; i < MAX_TRADE_SERVER_CONNECTIONS; i++)
    for (j = 0; j < MAX_NYSE_ORDER_ID_MAPPING; j++)
    {
      NYSE_CCG_OrderID_Mapping[i][j] = -1;
    }
}

/****************************************************************************
- Function name:  ParseRawDataForNYSE_CCGOrder
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ParseRawDataForNYSE_CCGOrder( t_DataBlock dataBlock, int type )
{
  //Get timestamp from epoch with format <second from epoch>.<microsecond> (eg. 124212553634.3235235)
  char timeStamp[32];
  long usec;

  t_ShortConverter myShort;

  myShort.c[1] = dataBlock.msgContent[0];
  myShort.c[0] = dataBlock.msgContent[1];
  int msgType = myShort.value;
  switch ( msgType )
  {
    // New order - 0x0041
    case 0x0041:
      usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]) / 1000;
      sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
      ProcessNewOrderOfNYSE_CCGOrder(dataBlock, type, timeStamp);
      break;

    // ACK order - 0x0091
    case 0x0091:
      usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]) / 1000;
      sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
      ProcessOrderACKOfNYSE_CCGOrder(dataBlock, type, timeStamp);
      break;

    // Fill order - 0x0081
    case 0x0081:
      usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]) / 1000;
      sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
      ProcessOrderFillOfNYSE_CCGOrder(dataBlock, type, timeStamp);
      break;

    // Canceled order - 0x00D1
    case 0x00D1:
      usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]) / 1000;
      sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
      ProcessOrderCanceledOfNYSE_CCGOrder(dataBlock, type, timeStamp);
      break;

    // Reject order - 0x00F1
    case 0x00F1:
      usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]) / 1000;
      sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
      ProcessRejectOfNYSE_CCGOrder(dataBlock, type, timeStamp);
      break;

    // Cancel request order - 0x0061
    case 0x0061:
      usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]) / 1000;
      sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
      ProcessCancelRequestOfNYSE_CCGOrder(dataBlock, type, timeStamp);
      break;

    // Cancel ACK order - 0x00A1
    case 0x00A1:
      usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]) / 1000;
      sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
      ProcessCancelACKOfNYSE_CCGOrder(dataBlock, type, timeStamp);
      break;

    // Bust or correct order - 0x0101
    case 0x0101:
      usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]) / 1000;
      sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
      ProcessBustOrCorrectOrderOfNYSE_CCGOrder(dataBlock, type, timeStamp);
      break;

    default:
      TraceLog(ERROR_LEVEL, "NYSE CCG Order: Invalid order message type: %X\n", msgType);
      break;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessNewOrderOfNYSE_CCGOrder
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessNewOrderOfNYSE_CCGOrder( t_DataBlock dataBlock, int type, char *timeStamp )
{
  /*
   * Fields of NYSE_CCG neworder
   *
   * 1. Message type        Binary    2 bytes
   * 2. Message length      Binary    2 bytes
   * 3. Sequence number     Binary    4 bytes
   * 4. Order quantity (shares) Binary    4 bytes
   * 5. Max floor quantity    Binary    4 bytes
   * 6. Price           Binary    4 bytes
   * 7. Price scale       Alpha/Num 1 bytes
   * 8. Symbol          Alpha   11 bytes
   * 9. Execution instruction   Alpha/Num 1 byte
   *    (E = DNI (Do not increase))
   *    (F = DNR (Do not reduce))
   *
   * 10.Side            Alpha/Num 1 byte
   *    (1 = Buy, 2 = Sell, 5 = Short sell)
   *
   * 11.Order type        Alpha/Num 1 byte
   * 12.Time in force       Alpha/Num 1 byte
   *    (0 = Day, 3 = IOC)
   *
   * 13.Rule80A         Alpha/Num 1 byte
   * 14.Routing instruction   Alpha/Num 1 byte
   *    (NX = 7, DNS = D, SOC = S, ISO = I, CO = C)
   *
   * 15.DOTReserve        Alpha   1 byte
   *    (Y or N)
   *
   * 16.OnBehalfOfCompID      Alpha   5 bytes
   * 17.Sender sub ID       Alpha/Num 5 bytes
   * 18.Clearing firm       Alpha   5 bytes
   * 19.Account         Alpha/Num 10 bytes
   * 20.Client orderID      Alpha/Num 17 bytes
   *    (Notes: The value of this field must be UPPERCASE)
   *
   */
  t_NYSE_CCGNewOrder newOrder;
  t_OpenOrderInfo openOrder;
  t_OrderDetail orderDetail;

  t_IntConverter myInt;
  t_ShortConverter myShort;

  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;

  // YYYY/mm/DD
  char dateTime[32];
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]), &dateTime[0]);
  strncpy(newOrder.date, &dateTime[0], 10);
  newOrder.date[10] = 0;

  // HH:MM:SS.mss
  strncpy(newOrder.timestamp, &dateTime[11], TIME_LENGTH);
  newOrder.timestamp[TIME_LENGTH] = 0;

  // openOrder.timestamp
  strncpy( openOrder.timestamp, &dateTime[0], TIMESTAMP_LENGTH );
  openOrder.timestamp[TIMESTAMP_LENGTH] = 0;

  // orderDetail
  strcpy( orderDetail.timestamp, openOrder.timestamp );

  // ECN order
  strcpy(newOrder.ecn, "NYSE");

  // openOrder.ECNId
  openOrder.ECNId = TYPE_NYSE_CCG;

  // Status
  strcpy( newOrder.status, orderStatus.Sent );

  // openOrder.status
  openOrder.status = orderStatusIndex.Sent;

  // Messsage type
  strcpy( newOrder.msgType, "NewOrder" );

  // msgType
  myShort.c[1] = dataBlock.msgContent[0];
  myShort.c[0] = dataBlock.msgContent[1];
  int msgType = myShort.value;

  // Length
  myShort.c[1] = dataBlock.msgContent[2];
  myShort.c[0] = dataBlock.msgContent[3];
  newOrder.length = myShort.value;

  // Sequence number
  myInt.c[3] = dataBlock.msgContent[4];
  myInt.c[2] = dataBlock.msgContent[5];
  myInt.c[1] = dataBlock.msgContent[6];
  myInt.c[0] = dataBlock.msgContent[7];
  newOrder.seqNum = myInt.value;

  // Shares
  myInt.c[3] = dataBlock.msgContent[8];
  myInt.c[2] = dataBlock.msgContent[9];
  myInt.c[1] = dataBlock.msgContent[10];
  myInt.c[0] = dataBlock.msgContent[11];
  newOrder.shares = myInt.value;

  // openOrder.shares
  openOrder.shares = newOrder.shares;
  openOrder.leftShares = openOrder.shares;

  // Max floor
  myInt.c[3] = dataBlock.msgContent[12];
  myInt.c[2] = dataBlock.msgContent[13];
  myInt.c[1] = dataBlock.msgContent[14];
  myInt.c[0] = dataBlock.msgContent[15];
  newOrder.maxFloor = myInt.value;

  // Price and Price Scale
  myInt.c[3] = dataBlock.msgContent[16];
  myInt.c[2] = dataBlock.msgContent[17];
  myInt.c[1] = dataBlock.msgContent[18];
  myInt.c[0] = dataBlock.msgContent[19];
  char fieldPriceScale = dataBlock.msgContent[20];
  int  fieldPrice = myInt.value;
  newOrder.price = fieldPrice * 1.0 / pow( 10, fieldPriceScale - 48 );

  // openOrder.price
  openOrder.price = newOrder.price;
  openOrder.avgPrice = openOrder.price;

  // Stock symbol
  strncpy(newOrder.symbol, (char *)&dataBlock.msgContent[84], SYMBOL_LEN);

  // openOrder.symbol
  strncpy( openOrder.symbol, (char *)newOrder.symbol, SYMBOL_LEN);

  // Execution instruction
  char fieldExecInst = dataBlock.msgContent[32];
  if (fieldExecInst == 0)
  {
    newOrder.execInst = ' ';
  }
  else
    newOrder.execInst = fieldExecInst;

  // "B", "S" or "SS"
  char action = dataBlock.msgContent[33];
  char buysell = '0';
  switch ( action )
  {
    case '1':
      buysell = 'B';
      strcpy( newOrder.side, "BC" );
      openOrder.side = BUY_TO_CLOSE_TYPE;
      break;

    case '2':
      buysell = 'S';
      strcpy( newOrder.side, "S" );
      openOrder.side = SELL_TYPE;
      break;

    case '5':
      buysell = 'T';
      strcpy( newOrder.side, "SS" );
      openOrder.side = SHORT_SELL_TYPE;
      break;

    default:
      strcpy( newOrder.side, " " );
      openOrder.side = -1;

      TraceLog(ERROR_LEVEL, "NYSE_CCG New Order: not support side = %c, %d\n", action, action );
      break;
  }

  // Order type
  newOrder.orderType = dataBlock.msgContent[34];

  // Time in force
  char tif = dataBlock.msgContent[35];
  char _tif[6];
  switch (tif)
  {
    case '0':
      strcpy( newOrder.timeInForce, "DAY" );
      openOrder.tif = DAY_TYPE;
      strcpy(_tif, "99999");
      break;

    case '3':
      strcpy( newOrder.timeInForce, "IOC" );
      strcpy(_tif, "0");
      openOrder.tif = IOC_TYPE;
      break;

    default:
      strcpy( newOrder.timeInForce, " " );
      openOrder.tif = -1;
      TraceLog(ERROR_LEVEL, "NYSE_CCG New Order: not support tif = %c, %d\n", tif, tif );
      break;
  }

  // Rule 80A
  newOrder.rule80A = dataBlock.msgContent[36];

  // Iso Flag (routing instruction)
  char fieldRoutIns = dataBlock.msgContent[37];
  if (fieldRoutIns == 0)
  {
    newOrder.isoFlag = ' ';
  }
  else
    newOrder.isoFlag = fieldRoutIns;

  int isISO = 0;
  short bboID = -1;
  unsigned char bbReason;
  if (newOrder.isoFlag == 'I')
  {
    isISO = 1;
    bboID = GetShortNumber(&dataBlock.addContent[DATABLOCK_OFFSET_BBO_ID]);
  }

  newOrder.bboId = bboID;

  // DOTReserve
  char fieldDOTReserve = dataBlock.msgContent[38];
  if (fieldDOTReserve == 0)
  {
    newOrder.dotReserve = ' ';
  }
  else
    newOrder.dotReserve = fieldDOTReserve;

  // Company group Id
  if (dataBlock.msgContent[39] == 0)
  {
    strcpy(newOrder.compGroupID, " ");
  }
  else
  {
    strncpy(newOrder.compGroupID, (char *)&dataBlock.msgContent[39], 5);
    newOrder.compGroupID[5] = 0;
  }

  // Sender subID
  if (dataBlock.msgContent[44] == 0)
  {
    strcpy(newOrder.senderSubID, " ");
  }
  else
  {
    strncpy(newOrder.senderSubID, (char *)&dataBlock.msgContent[44], 5);
    newOrder.senderSubID[5] = 0;
  }

  // Firm
  strncpy(newOrder.firm, (char *)&dataBlock.msgContent[49], 5);
  newOrder.firm[5] = 0;

  // Account
  strncpy( newOrder.account, (char *)&dataBlock.msgContent[54], 10);
  newOrder.account[10] = 0;

  newOrder.tradingAccount = dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID];

  // Client orderID
  char fieldClOrderID[18];
  strncpy( fieldClOrderID, (char *)&dataBlock.msgContent[64], 17);
  fieldClOrderID[17] = 0;
  newOrder.clOrderID = Convert_NYSECCG_Format_To_ClOrdID((unsigned char *)&fieldClOrderID);

  // openOrder.clOrdId
  openOrder.clOrdId = newOrder.clOrderID;

  //orderDetail.msgContent
  orderDetail.length = sprintf( orderDetail.msgContent,
      "%d=%X\001%d=%d\001%d=%d\001%d=%d\001%d=%d\001%d=%d\001%d=%c\001%d=%s\001%d=%c\001%d=%c\001"
      "%d=%c\001%d=%c\001%d=%c\001%d=%c\001%d=%c\001%d=%s\001%d=%s\001%d=%s\001%d=%s\001%d=%s\001",
      fieldIndexList.MsgType, msgType,
      fieldIndexList.Length, newOrder.length,
      fieldIndexList.InSeqNum, newOrder.seqNum,
      fieldIndexList.Shares, newOrder.shares,
      fieldIndexList.MaxFloor, newOrder.maxFloor,
      fieldIndexList.Price, fieldPrice,
      fieldIndexList.PriceScale, fieldPriceScale,
      fieldIndexList.Symbol, newOrder.symbol,
      fieldIndexList.ExecInst, newOrder.execInst,
      fieldIndexList.Side, action,
      fieldIndexList.Type, newOrder.orderType,
      fieldIndexList.TimeInForce, tif,
      fieldIndexList.Rule80A, newOrder.rule80A,
      fieldIndexList.ISO_Flag, newOrder.isoFlag,
      fieldIndexList.DOTReserve, newOrder.dotReserve,
      fieldIndexList.CompGroupID, newOrder.compGroupID,
      fieldIndexList.SenderSubID, newOrder.senderSubID,
      fieldIndexList.Firm, newOrder.firm,
      fieldIndexList.Account, newOrder.account,
      fieldIndexList.ClOrdID, fieldClOrderID);

  strcpy( newOrder.msgContent, orderDetail.msgContent );

  // Update database
  if ( type == MANUAL_ORDER )
  {
    //openOrder.tsId
    openOrder.tsId = MANUAL_ORDER;

    //newOrder.tsId
    newOrder.tsId = MANUAL_ORDER;

    // Trade type
    newOrder.tradeType = NORMALLY_TRADE;
    openOrder.tradeType = NORMALLY_TRADE;

    strcpy(newOrder.entryExit, "Exit");
    strcpy(newOrder.ecnLaunch, " ");
    strcpy(newOrder.askBid, " ");

    //Add CrossID if it belogs to PM
    if (newOrder.clOrderID >= 500000)
    {
      bbReason = OJ_BB_REASON_PM;
      newOrder.crossId = GetIntNumber(&dataBlock.addContent[DATABLOCK_OFFSET_CROSS_ID]);
      openOrder.crossId = newOrder.crossId;
    }
    else
    {
      bbReason = OJ_BB_REASON_AS;
      newOrder.crossId = 0;
    }
  }
  else
  {
    if (dataBlock.addContent[DATABLOCK_OFFSET_LAUNCH] == EXIT_VALUE)
    {
      // Get Entry/Exit
      bbReason = OJ_BB_REASON_EXIT;
      strcpy(newOrder.entryExit, "Exit");

      strcpy(newOrder.ecnLaunch, " ");
      strcpy(newOrder.askBid, " ");
      
      newOrder.entryExitRefNum = *(unsigned short *)&dataBlock.addContent[DATABLOCK_OFFSET_ENTRY_EXIT_REF];
    }
    else
    {
      // Get Entry/Exit
      bbReason = OJ_BB_REASON_ENTRY;
      strcpy(newOrder.entryExit, "Entry");

      // Get ECN launch and Ask/Bid launch
      GetECNLaunchInfo(dataBlock.addContent[DATABLOCK_OFFSET_LAUNCH], newOrder.ecnLaunch, newOrder.askBid);
    }

    // Visible shares
    newOrder.visibleShares = GetIntNumber(&dataBlock.addContent[DATABLOCK_OFFSET_VISIBLE_SIZE]);

    //Cross Id
    newOrder.crossId = GetIntNumber(&dataBlock.addContent[DATABLOCK_OFFSET_CROSS_ID]);
    openOrder.crossId = newOrder.crossId;

    // Trade type
    newOrder.tradeType = NORMALLY_TRADE;
    openOrder.tradeType = newOrder.tradeType;

    //openOrder.tsId
    openOrder.tsId = tradeServersInfo.config[type].tsId;

    //newOrder.tsId
    newOrder.tsId = type;
    
    // Reset no trade duration
    time_t t = (time_t)hbitime_seconds();
    struct tm tm = *localtime(&t);

    NoTradesDurationMgt[type].currentNoTradesDurationInSecond = 0;
    NoTradesDurationMgt[type].alertThresholdInMinute = TradeTimeAlertConfig[3600 * tm.tm_hour + 60 * tm.tm_min + tm.tm_sec];

    sprintf(NoTradesDurationMgt[type].startTimeStamp, "%d:%d", tm.tm_hour, tm.tm_min);
  }

  openOrder.account = dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID];

  // Call function to add a open order
  ProcessAddASOpenOrder( &openOrder, &orderDetail );

  // Call function to update New Order of ARCA DIRECT Order in Automatic Order
  ProcessInsertNewOrder4NYSE_CCGQueryToCollection(&newOrder);

  //Add a new record to OJ file
  int isDuplicated = 0;
  if (AddOrderIdToList(&__new, newOrder.clOrderID) == ERROR)
  {
    isDuplicated = 1;
  }
  
  // value of field RoutingInstruction = I(ISO)
  char _visibility = 'Y';

  char account[32];
  GetAccountName(TYPE_NYSE_CCG, dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID], account);

  if (isDuplicated == 0)
  {
    AddOJNewOrderRecord(timeStamp, newOrder.symbol, VENUE_CCG, buysell, newOrder.price, newOrder.shares, newOrder.clOrderID, _tif, _visibility, isISO, account, NULL, newOrder.rule80A, newOrder.crossId, bbReason);
  }
  
  //Add ISO Order to ISO Collection for ISO Flight File creation
  if ((isISO == 1) && (isDuplicated == 0))
  {
    t_ISOOrderInfo isoOrderInfo;
    strcpy(isoOrderInfo.strTime, timeStamp);

    sprintf(isoOrderInfo.strVenueID, "%d", VENUE_CCG);

    isoOrderInfo.strBuysell[0] = buysell;
    isoOrderInfo.strBuysell[1] = 0;

    sprintf(isoOrderInfo.strPrice, "%.2lf", newOrder.price);

    sprintf(isoOrderInfo.strShares, "%d", newOrder.shares);

    sprintf(isoOrderInfo.strSequenceID, "%d", newOrder.clOrderID);

    isoOrderInfo.strCapacity[0] = newOrder.rule80A;
    isoOrderInfo.strCapacity[1] = 0;

    strcpy(isoOrderInfo.strAccount, account);

    isoOrderInfo.strRoutingInfo[0] = 0;

    AddISOOrderToCollection(newOrder.crossId, bboID, newOrder.symbol, &isoOrderInfo);
  }
  
  CheckAndProcessRawdataInWaitListForOrderID(newOrder.clOrderID);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessOrderACKOfNYSE_CCGOrder
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessOrderACKOfNYSE_CCGOrder( t_DataBlock dataBlock, int type, char *timeStamp )
{
  /*
   * Fields of NYSE_CCG ACK order
   *
   * 1. Message type        Binary    2 bytes
   * 2. Message length      Binary    2 bytes
   * 3. Sequence number     Binary    4 bytes
   * 4. OrderID         Binary    4 bytes
   * 5. Transaction time      Binary    4 bytes
   * 6. Deliver to compID     Alpha   5 bytes
   * 7. Target subID        Alpha   5 bytes
   * 8. Account         Alpha/Num 10 bytes
   * 9. Client orderID      Alphan/Num  17 bytes
   *
   */

  t_NYSE_CCGACKOrder orderACK;
  t_OpenOrderInfo openOrder;
  t_OrderDetail orderDetail;

  t_IntConverter myInt;
  t_ShortConverter myShort;

  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;

  // YYYY/mm/DD
  char dateTime[32];
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]), &dateTime[0]);
  strncpy( orderACK.processedTimestamp, &dateTime[11], TIME_LENGTH );
  orderACK.processedTimestamp[TIME_LENGTH] = 0;
  
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]), &dateTime[0]);
  strncpy( orderACK.date, &dateTime[0], 10 );
  orderACK.date[10] = 0;

  // Convert date format to CCG date format
  // MM
  strncpy(Nyse_CCG_timestamp, &dateTime[5], 2);
  // DD
  strncpy(&Nyse_CCG_timestamp[2], &dateTime[8], 2);
  // YYYY
  strncpy(&Nyse_CCG_timestamp[4], &dateTime[0], 4);

  // HH:MM:SS.ms
  strncpy( orderACK.timestamp, &dateTime[11], TIME_LENGTH );
  orderACK.timestamp[TIME_LENGTH] = 0;

  // openOrder.timestamp
  strncpy(openOrder.timestamp, &dateTime[0], TIMESTAMP_LENGTH);
  openOrder.timestamp[TIMESTAMP_LENGTH] = 0;

  strcpy(orderDetail.timestamp, openOrder.timestamp);

  //openOrder.ECNId
  openOrder.ECNId = TYPE_NYSE_CCG;

  // status
  strcpy( orderACK.status, orderStatus.Live );

  // openOrder.status
  openOrder.status = orderStatusIndex.Live;

  // Messsage type
  strcpy( orderACK.msgType, "OrderACK" );

  // Stock symbol
  memset( openOrder.symbol, 0, SYMBOL_LEN);

  // Side
  openOrder.side = -1;

  // Shares and leftShares
  openOrder.shares = 0;
  openOrder.leftShares = 0;

  // Price and avgPrice
  openOrder.price = 0.00;
  openOrder.avgPrice = 0.00;
  orderACK.price = 0.00;

  // msgType
  myShort.c[1] = dataBlock.msgContent[0];
  myShort.c[0] = dataBlock.msgContent[1];
  int msgType = myShort.value;

  // Length
  myShort.c[1] = dataBlock.msgContent[2];
  myShort.c[0] = dataBlock.msgContent[3];
  orderACK.length = myShort.value;

  // Sequence number
  myInt.c[3] = dataBlock.msgContent[4];
  myInt.c[2] = dataBlock.msgContent[5];
  myInt.c[1] = dataBlock.msgContent[6];
  myInt.c[0] = dataBlock.msgContent[7];
  orderACK.seqNum = myInt.value;

  // MEOrderID
  myInt.c[3] = dataBlock.msgContent[8];
  myInt.c[2] = dataBlock.msgContent[9];
  myInt.c[1] = dataBlock.msgContent[10];
  myInt.c[0] = dataBlock.msgContent[11];
  orderACK.meOrderID = myInt.value;

  // Transaction time
  if (dataBlock.msgContent[12] == 0)
  {
    orderACK.transTime = 0;
  }
  else
  {
    myInt.c[3] = dataBlock.msgContent[12];
    myInt.c[2] = dataBlock.msgContent[13];
    myInt.c[1] = dataBlock.msgContent[14];
    myInt.c[0] = dataBlock.msgContent[15];
    orderACK.transTime = myInt.value;
  }

  // Deliver to compID
  if (dataBlock.msgContent[16] == 0)
  {
    strcpy(orderACK.deliverToCompID, " ");
  }
  else
  {
    strncpy(orderACK.deliverToCompID, (char *)&dataBlock.msgContent[16], 5);
    orderACK.deliverToCompID[5] = 0;
  }

  // Target subID
  if (dataBlock.msgContent[21] == 0)
  {
    strcpy(orderACK.targetSubID, " ");
  }
  else
  {
    strncpy(orderACK.targetSubID, (char *)&dataBlock.msgContent[21], 5);
    orderACK.targetSubID[5] = 0;
  }

  // Account
  if (dataBlock.msgContent[26] == 0)
  {
    strcpy(orderACK.account, " ");
  }
  else
  {
    strncpy(orderACK.account, (char *)&dataBlock.msgContent[26], 10);
    orderACK.account[10] = 0;
  }

  // client orderID
  char fieldClOrderID[18];
  strncpy(fieldClOrderID, (char *)&dataBlock.msgContent[36], 17);
  fieldClOrderID[17] = 0;
  orderACK.clOrderID = Convert_NYSECCG_Format_To_ClOrdID((unsigned char *)&fieldClOrderID);

  // openOrder.clOrdId
  openOrder.clOrdId = orderACK.clOrderID;

  // Time in force
  openOrder.tif = -1;

  // orderDetail.msgContent
  orderDetail.length = sprintf( orderDetail.msgContent,
      "%d=%X\001%d=%d\001%d=%d\001%d=%ld\001%d=%d\001%d=%s\001%d=%s\001%d=%s\001%d=%s\001",
          fieldIndexList.MsgType, msgType,
          fieldIndexList.Length, orderACK.length,
          fieldIndexList.InSeqNum, orderACK.seqNum,
          fieldIndexList.ArcaOrderID, orderACK.meOrderID,
          fieldIndexList.TransactionTime, orderACK.transTime,
          fieldIndexList.DeliverToCompID, orderACK.deliverToCompID,
          fieldIndexList.Firm, orderACK.targetSubID,
          fieldIndexList.Account, orderACK.account,
//          fieldIndexList.ClOrdID, orderACK.clOrderID,
          fieldIndexList.ClOrdID, fieldClOrderID);

  strcpy(orderACK.msgContent, orderDetail.msgContent);

  // Update database
  if( type == MANUAL_ORDER )
  {
    // openOrder.tsId
    openOrder.tsId = MANUAL_ORDER;
  }
  else
  {
    // openOrder.tsId
    openOrder.tsId = tradeServersInfo.config[type].tsId;
  }

  // Call function to update a open order
  openOrder.account = dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID];
  int indexOpenOrder = ProcessUpdateASOpenOrderForOrderACK( &openOrder, &orderDetail );

  if (indexOpenOrder == -2)
  {
    AddDataBlockToWaitList(TYPE_NYSE_CCG, WAIT_LIST_TYPE_CCG_ORDER_ACK, orderACK.clOrderID, dataBlock, type, timeStamp);
    return 0;
  }
  
  // Call function to update Order ACK of NYSE_CCG Order in Automatic Order
  ProcessInsertACKOrder4NYSE_CCGQueryToCollection(&orderACK);

  if (AddOrderIdToList(&__ack, openOrder.clOrdId) == ERROR) //Duplicated
  {
    return 0;
  }
  
  char ecnOrderID[32];
  sprintf(ecnOrderID, "%lu", orderACK.meOrderID);

  char account[32];
  GetAccountName(TYPE_NYSE_CCG, dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID], account);

  // Rule80A
  char capacity = 'P';

  char exOrderId[32];
  Convert_ClOrdID_To_NYSE_Format(openOrder.clOrdId, exOrderId);
  
  if (indexOpenOrder != ERROR)
  {
    AddOJOrderAcceptedRecord(timeStamp, asOpenOrder.orderCollection[indexOpenOrder].orderSummary.symbol, exOrderId, openOrder.clOrdId, ecnOrderID, VENUE_CCG, account, capacity);
  }
  else
  {
    AddOJOrderAcceptedRecord(timeStamp, "", exOrderId, openOrder.clOrdId, ecnOrderID, VENUE_CCG, account, capacity);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessOrderFillOfNYSE_CCGOrder
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessOrderFillOfNYSE_CCGOrder( t_DataBlock dataBlock, int type, char*timeStamp )
{
  /*
   * Fields of NYSE_CCG fill order
   *
   * 1. Message type        Binary    2 bytes
   * 2. Message length      Binary    2 bytes
   * 3. Sequence number     Binary    4 bytes
   * 4. OrderID         Binary    4 bytes
   * 5. Transaction time      Binary    4 bytes
   * 6. Leaves shares       Binary    4 bytes
   * 7. Last shares       Binary    4 bytes
   * 8. Last price        Binary    4 bytes
   * 9. Price scale       Alpha/Num 1 byte
   * 10.Side            Alpha/Num 1 byte
   *    (1 = Buy, 2 = Sell, 5 = Short sell)
   *
   * 11.Billing indicator     Alpha/Num 1 byte
   * 12.Last market       Alpha   1 byte
   * 13.Deliver to compID     Alpha   5 bytes
   * 14.Target subID        Alpha   5 bytes
   * 15.Exec broker       Alpha/Num 5 bytes
   * 16.Contra broker       Alpha/Num 5 bytes
   * 17.Contra Trader       Alpha/Num 5 bytes
   * 18.ExecAwayMktID       Alpha   6 bytes
   * 19.Billing rate        Alpha/Num 6 bytes
   * 20.ExchangeID        Alpha/Num 10 bytes
   * 21.Account         Alpha/Num 10 bytes
   * 22.DBExecID          Alpha/Num 10 bytes
   * 23.Client orderID      Alpha/Num 17 bytes
   *    (Notes: The value of this field must be UPPERCASE)
   *
   */
  t_NYSE_CCGFillOrder orderFill;
  t_OrderDetail orderDetail;

  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;

  t_IntConverter myInt;
  t_ShortConverter myShort;

  // YYYY/mm/DD
  char dateTime[32];
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]), &dateTime[0]);
  strncpy( orderFill.processedTimestamp, &dateTime[11], TIME_LENGTH );
  orderFill.processedTimestamp[TIME_LENGTH] = 0;
  
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]), &dateTime[0]);
  strncpy( orderFill.date, &dateTime[0], 10 );
  orderFill.date[10] = 0;

  // HH:MM:SS.ms
  strncpy( orderFill.timestamp, &dateTime[11], TIME_LENGTH );
  orderFill.timestamp[TIME_LENGTH] = 0;

  // orderDetail.timestamp
  strncpy(orderDetail.timestamp, &dateTime[0], TIMESTAMP_LENGTH);
  orderDetail.timestamp[TIMESTAMP_LENGTH] = 0;

  // Messsage type
  strcpy( orderFill.msgType, "OrderFill" );

  // msgType
  myShort.c[1] = dataBlock.msgContent[0];
  myShort.c[0] = dataBlock.msgContent[1];
  int msgType = myShort.value;
  // %d=%d\001

  // Length
  myShort.c[1] = dataBlock.msgContent[2];
  myShort.c[0] = dataBlock.msgContent[3];
  orderFill.length = myShort.value;
  // %d=%d\001

  // Sequence number
  myInt.c[3] = dataBlock.msgContent[4];
  myInt.c[2] = dataBlock.msgContent[5];
  myInt.c[1] = dataBlock.msgContent[6];
  myInt.c[0] = dataBlock.msgContent[7];
  orderFill.seqNum = myInt.value;
  // %d=%d\001

  // MEOrderID
  myInt.c[3] = dataBlock.msgContent[8];
  myInt.c[2] = dataBlock.msgContent[9];
  myInt.c[1] = dataBlock.msgContent[10];
  myInt.c[0] = dataBlock.msgContent[11];
  orderFill.meOrderID = myInt.value;
  // %d=%d\001

  // Transaction time
  myInt.c[3] = dataBlock.msgContent[12];
  myInt.c[2] = dataBlock.msgContent[13];
  myInt.c[1] = dataBlock.msgContent[14];
  myInt.c[0] = dataBlock.msgContent[15];
  orderFill.transTime = myInt.value;
  // %d=%d\001

  // Leaves Shares
  myInt.c[3] = dataBlock.msgContent[16];
  myInt.c[2] = dataBlock.msgContent[17];
  myInt.c[1] = dataBlock.msgContent[18];
  myInt.c[0] = dataBlock.msgContent[19];
  orderFill.leavesShares = myInt.value;
  // %d=%d\001

  // Shares
  myInt.c[3] = dataBlock.msgContent[20];
  myInt.c[2] = dataBlock.msgContent[21];
  myInt.c[1] = dataBlock.msgContent[22];
  myInt.c[0] = dataBlock.msgContent[23];
  orderFill.lastShares = myInt.value;
  // %d=%d\001

  // Price and priceScale
  myInt.c[3] = dataBlock.msgContent[24];
  myInt.c[2] = dataBlock.msgContent[25];
  myInt.c[1] = dataBlock.msgContent[26];
  myInt.c[0] = dataBlock.msgContent[27];
  char fieldPriceScale = dataBlock.msgContent[28];
  int  fieldPrice = myInt.value;
  orderFill.lastPrice = fieldPrice * 1.0 / pow( 10, fieldPriceScale - 48 );
  // %d=%d\001

  // "B", "S" or "SS"
  char action = dataBlock.msgContent[29];
  char buysell = '0';
  switch ( action )
  {
    case '1':
      buysell = 'B';
      strcpy( orderFill.side, "B" );
      break;

    case '2':
      buysell = 'S';
      strcpy( orderFill.side, "S" );
      break;

    case '5':
      buysell = 'T';
      strcpy( orderFill.side, "SS" );
      break;

    default:
      strcpy( orderFill.side, " " );

      TraceLog(ERROR_LEVEL, "NYSE_CCG Fill Order: not support side = %c, %d\n", action, action );
      break;
  }

  // %d=%c\001

  // Billing indicator
  if (dataBlock.msgContent[30] == 0)
  {
    orderFill.billingIndicator = ' ';
  }
  else
  {
    orderFill.billingIndicator = dataBlock.msgContent[30];
  }
  // %d=%c\001

  // Last market
  if (dataBlock.msgContent[31] == 0)
  {
    orderFill.lastMarket = ' ';
  }
  else
  {
    orderFill.lastMarket = dataBlock.msgContent[31];
  }
  // %d=%c\001

  // Deliver to compID
  if (dataBlock.msgContent[32] == 0)
  {
    strcpy(orderFill.deliverToCompID, " ");
  }
  else
  {
    strncpy(orderFill.deliverToCompID, (char *)&dataBlock.msgContent[32], 5);
    orderFill.deliverToCompID[5] = 0;
  }
  // %d=%s\001

  // Target subID
  if (dataBlock.msgContent[37] == 0)
  {
    strcpy(orderFill.targetSubID, " ");
  }
  else
  {
    strncpy(orderFill.targetSubID, (char *)&dataBlock.msgContent[37], 5);
    orderFill.targetSubID[5] = 0;
  }
  // %d=%s\001

  // Execution broker
  if (dataBlock.msgContent[42] == 0)
  {
    strcpy(orderFill.execBroker, " ");
  }
  else
  {
    strncpy(orderFill.execBroker, (char *)&dataBlock.msgContent[42], 5);
    orderFill.execBroker[5] = 0;
  }
  // %d=%s\001

  // Contra broker
  if (dataBlock.msgContent[47] == 0)
  {
    strcpy(orderFill.contraBroker, " ");
  }
  else
  {
    strncpy(orderFill.contraBroker, (char *)&dataBlock.msgContent[47], 5);
    orderFill.contraBroker[5] = 0;
  }
  // %d=%s\001

  // Contra trader
  if (dataBlock.msgContent[52] == 0)
  {
    strcpy(orderFill.contraTrader, " ");
  }
  else
  {
    strncpy(orderFill.contraTrader, (char *)&dataBlock.msgContent[52], 5);
    orderFill.contraTrader[5] = 0;
  }
  // %d=%s\001

  // Exec away market ID
  if (dataBlock.msgContent[57] == 0)
  {
    strcpy(orderFill.execAwayMktID, " ");
  }
  else
  {
    strncpy(orderFill.execAwayMktID, (char *)&dataBlock.msgContent[57], 6);
    orderFill.execAwayMktID[6] = 0;
  }
  // %d=%s\001

  // Billing rate
  if (dataBlock.msgContent[63] == 0)
  {
    strcpy(orderFill.billingRate, " ");
  }
  else
  {
    strncpy(orderFill.billingRate, (char *)&dataBlock.msgContent[63], 6);
    orderFill.billingRate[6] = 0;
  }
  // %d=%s\001

  // Exchange ID
  char fieldExecID[11];
  strncpy(fieldExecID, (char *)&dataBlock.msgContent[69], 10);
  fieldExecID[10] = 0;
  orderFill.execID = atol(fieldExecID);
  // %d=%d\001

  orderDetail.execId = orderFill.execID;

  // Account
  strncpy(orderFill.Account, (char *)&dataBlock.msgContent[79], 10);
  orderFill.Account[10] = 0;
  // %d=%s\001

  // DBExecID
  strncpy(orderFill.dbExecID, (char *)&dataBlock.msgContent[89], 10);
  orderFill.dbExecID[10] = 0;
  // %d=%s\001

  // Client order ID
  char fieldClOrderID[18];
  strncpy( fieldClOrderID, (char *)&dataBlock.msgContent[99], 17);
  fieldClOrderID[17] = 0;
  orderFill.clOrderID = Convert_NYSECCG_Format_To_ClOrdID((unsigned char *)&fieldClOrderID);
  
  char exOrderId[32];
  Convert_ClOrdID_To_NYSE_Format(orderFill.clOrderID, exOrderId);

  // orderDetail.msgContent
  orderDetail.length = sprintf( orderDetail.msgContent,
      "%d=%X\001%d=%d\001%d=%d\001%d=%ld\001%d=%d\001%d=%d\001%d=%d\001%d=%d\001%d=%c\001%d=%c\001%d=%c\001%d=%c\001%d=%s\001%d=%s\001%d=%s\001"
      "%d=%s\001%d=%s\001%d=%s\001%d=%s\001%d=%ld\001%d=%s\001%d=%s\001%d=%s\001",
          fieldIndexList.MsgType, msgType,
          fieldIndexList.Length, orderFill.length,
          fieldIndexList.InSeqNum, orderFill.seqNum,
          fieldIndexList.ArcaOrderID, orderFill.meOrderID,
          fieldIndexList.TransactionTime, orderFill.transTime,
          fieldIndexList.OrdQty, orderFill.leavesShares,
          fieldIndexList.Shares, orderFill.lastShares,
          fieldIndexList.PriceField, fieldPrice,
          fieldIndexList.PriceScale, fieldPriceScale,
          fieldIndexList.Side, action,
          fieldIndexList.BillingIndicator, orderFill.billingIndicator,
          fieldIndexList.LastMarket, orderFill.lastMarket,
          fieldIndexList.DeliverToCompID, orderFill.deliverToCompID,
          fieldIndexList.Firm, orderFill.targetSubID,
          fieldIndexList.ExecBroker, orderFill.execBroker,
          fieldIndexList.ContraBroker, orderFill.contraBroker,
          fieldIndexList.ContraTrader, orderFill.contraTrader,
          fieldIndexList.ExecAwayMktID, orderFill.execAwayMktID,
          fieldIndexList.BillingRate, orderFill.billingRate,
          fieldIndexList.ExecID, orderFill.execID,
          fieldIndexList.Account, orderFill.Account,
          fieldIndexList.DBExecID, orderFill.dbExecID,
          fieldIndexList.ClOrdID, fieldClOrderID);

  strcpy( orderFill.msgContent, orderDetail.msgContent );

  // Update database
  int tsId;

  if( type == MANUAL_ORDER )
  {
    // tsId
    tsId = MANUAL_ORDER;
  }
  else
  {
    // tsId
    tsId = tradeServersInfo.config[type].tsId;
  }

  char liquidityIndicator[6];
  liquidityIndicator[0] = orderFill.billingIndicator;
  liquidityIndicator[1] = 0;

  // Call function to update a open order (non fill)
  int indexOpenOrder = ProcessUpdateASOpenOrderForExecuted(tsId, orderFill.clOrderID, orderStatusIndex.Closed, orderFill.lastShares, orderFill.lastPrice, &orderDetail, dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID], liquidityIndicator, &orderFill.fillFee);
  if (indexOpenOrder == -2)
  {
    AddDataBlockToWaitList(TYPE_NYSE_CCG, WAIT_LIST_TYPE_CCG_ORDER_FILLED, orderFill.clOrderID, dataBlock, type, timeStamp);
    return 0;
  }
  
  if (indexOpenOrder != ERROR)
  {
    strcpy( orderFill.status, GetOrderStatus(asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status));

    orderFill.leavesShares = asOpenOrder.orderCollection[indexOpenOrder].orderSummary.leftShares;
    orderFill.avgPrice = asOpenOrder.orderCollection[indexOpenOrder].orderSummary.avgPrice;
  }

  orderFill.entryExitRefNum = *(unsigned short *)&dataBlock.addContent[DATABLOCK_OFFSET_ENTRY_EXIT_REF];
  
  // Call function to update Order Fill of ARCA DIRECT
  ProcessInsertFillOrder4NYSE_CCGQueryToCollection(&orderFill);

  if (AddExecDataToList(&__exec, orderFill.clOrderID, orderFill.execID) == ERROR) //Duplicated
  {
    return 0;
  }
  
  char ecnOrderID[32], execID[32];
  sprintf(ecnOrderID, "%lu", orderFill.meOrderID);
  sprintf(execID, "%lu", orderFill.execID);

  char account[32];
  GetAccountName(TYPE_NYSE_CCG, dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID], account);

  if (indexOpenOrder != ERROR)
  {
    AddOJOrderFillRecord(timeStamp, asOpenOrder.orderCollection[indexOpenOrder].orderSummary.symbol, exOrderId, VENUE_CCG, buysell, orderFill.lastPrice,
              orderFill.lastShares, orderFill.clOrderID, liquidityIndicator,
              ecnOrderID, execID, "1", (orderFill.leavesShares == 0)?1:0, account, orderFill.lastMarket);
  }
  else
  {
    AddOJOrderFillRecord(timeStamp, "", exOrderId, VENUE_CCG, buysell, orderFill.lastPrice,
              orderFill.lastShares, orderFill.clOrderID, liquidityIndicator,
              ecnOrderID, execID, "1", (orderFill.leavesShares == 0)?1:0, account, orderFill.lastMarket);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessOrderCanceledOfNYSE_CCGOrder
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessOrderCanceledOfNYSE_CCGOrder( t_DataBlock dataBlock, int type, char *timeStamp )
{
  /*
   * Fields of NYSE_CCG canceled order
   *
   * 1. Message type        Binary    2 bytes
   * 2. Message length      Binary    2 bytes
   * 3. Sequence number     Binary    4 bytes
   * 4. OrderID         Binary    4 bytes
   * 5. Transaction time      Binary    4 bytes
   * 6. Information code      Binary    1 byte
   * 7. Deliver to compID     Alpha   5 bytes
   * 8. Target subID        Alpha   5 bytes
   * 9. Account         Alpha/Num 10 bytes
   * 10.Original client orderID Alpha/Num 17 bytes
   *
   */
  t_NYSE_CCGCanceledOrder orderCancel;
  t_OrderDetail orderDetail;

  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;

  t_IntConverter myInt;
  t_ShortConverter myShort;

  // YYYY/mm/DD
  char dateTime[32];
    GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]), &dateTime[0]);
    strncpy( orderCancel.processedTimestamp, &dateTime[11], TIME_LENGTH );
  orderCancel.processedTimestamp[TIME_LENGTH] = 0;
    
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]), &dateTime[0]);
  strncpy( orderCancel.date, &dateTime[0], 10 );
  orderCancel.date[10] = 0;

  // HH:MM:SS.ms
  strncpy( orderCancel.timestamp, &dateTime[11], TIME_LENGTH );
  orderCancel.timestamp[TIME_LENGTH] = 0;

  // orderDetail.timestamp
  strncpy(orderDetail.timestamp, &dateTime[0], TIMESTAMP_LENGTH);
  orderDetail.timestamp[TIMESTAMP_LENGTH] = 0;

  // Messsage type
  strcpy( orderCancel.msgType, "OrderCancel" );

  // msgType
  myShort.c[1] = dataBlock.msgContent[0];
  myShort.c[0] = dataBlock.msgContent[1];
  int msgType = myShort.value;

  // Length
  myShort.c[1] = dataBlock.msgContent[2];
  myShort.c[0] = dataBlock.msgContent[3];
  orderCancel.length = myShort.value;

  // Sequence number
  myInt.c[3] = dataBlock.msgContent[4];
  myInt.c[2] = dataBlock.msgContent[5];
  myInt.c[1] = dataBlock.msgContent[6];
  myInt.c[0] = dataBlock.msgContent[7];
  orderCancel.seqNum = myInt.value;

  // OrderID
  myInt.c[3] = dataBlock.msgContent[8];
  myInt.c[2] = dataBlock.msgContent[9];
  myInt.c[1] = dataBlock.msgContent[10];
  myInt.c[0] = dataBlock.msgContent[11];
  orderCancel.meOrderID = myInt.value;

  // Transaction time
  if (dataBlock.msgContent[12] == 0)
  {
    orderCancel.transTime = 0;
  }
  else
  {
    myInt.c[3] = dataBlock.msgContent[12];
    myInt.c[2] = dataBlock.msgContent[13];
    myInt.c[1] = dataBlock.msgContent[14];
    myInt.c[0] = dataBlock.msgContent[15];
    orderCancel.transTime = myInt.value;
  }

  // Information code
  orderCancel.infoCode = dataBlock.msgContent[16];

  // Deliver to compID
  if (dataBlock.msgContent[17] == 0)
  {
    strcpy(orderCancel.deliverToCompID, " ");
  }
  else
  {
    strncpy(orderCancel.deliverToCompID, (char *)&dataBlock.msgContent[17], 5);
    orderCancel.deliverToCompID[5] = 0;
  }

  // Target subID
  if (dataBlock.msgContent[22] == 0)
  {
    strcpy(orderCancel.TargetSubID, " ");
  }
  else
  {
    strncpy(orderCancel.TargetSubID, (char *)&dataBlock.msgContent[22], 5);
    orderCancel.TargetSubID[5] = 0;
  }

  // Account
  if (dataBlock.msgContent[27] == 0)
  {
    strcpy(orderCancel.account, " ");
  }
  else
  {
    strncpy(orderCancel.account, (char *)&dataBlock.msgContent[27], 10);
    orderCancel.account[10] = 0;
  }

  // Original client orderID
  char fieldOrgOrderID[18];
  strncpy(fieldOrgOrderID, (char *)&dataBlock.msgContent[37], 17);
  fieldOrgOrderID[17] = 0;
  orderCancel.origClOrderID = Convert_NYSECCG_Format_To_ClOrdID((unsigned char *)&fieldOrgOrderID);

  // orderDetail.msgContent
  orderDetail.length = sprintf( orderDetail.msgContent,
      "%d=%X\001%d=%d\001%d=%d\001%d=%ld\001%d=%d\001%d=%d\001%d=%s\001%d=%s\001%d=%s\001%d=%s\001",
      fieldIndexList.MsgType, msgType,
      fieldIndexList.Length, orderCancel.length,
      fieldIndexList.InSeqNum, orderCancel.seqNum,
      fieldIndexList.ArcaOrderID, orderCancel.meOrderID,
      fieldIndexList.TransactionTime, orderCancel.transTime,
      fieldIndexList.InfoCode, orderCancel.infoCode,
      fieldIndexList.DeliverToCompID, orderCancel.deliverToCompID,
      fieldIndexList.Firm, orderCancel.TargetSubID,
      fieldIndexList.Account, orderCancel.account,
//      fieldIndexList.OriginalClientOrdID, orderCancel.origClOrderID,
      fieldIndexList.OriginalClientOrdID, fieldOrgOrderID);

  strcpy( orderCancel.msgContent, orderDetail.msgContent );

  // Update database
  int tsId;

  if( type == MANUAL_ORDER )
  {
    // tsId
    tsId = MANUAL_ORDER;
  }
  else
  {
    // tsId
    tsId = tradeServersInfo.config[type].tsId;
  }

  // Call function to update a open order (non fill)
  int indexOpenOrder = ProcessUpdateASOpenOrderForStatus(tsId, orderCancel.origClOrderID, orderStatusIndex.CanceledByECN, &orderDetail);
  if (indexOpenOrder == -2)
  {
    AddDataBlockToWaitList(TYPE_NYSE_CCG, WAIT_LIST_TYPE_CCG_ORDER_CANCELED, orderCancel.origClOrderID, dataBlock, type, timeStamp);
    return 0;
  }
  
  if (indexOpenOrder != ERROR)
  {
    strcpy(orderCancel.status, GetOrderStatus(asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status));
  }
  else
  {
    strcpy(orderCancel.status, orderStatus.CanceledByECN);
  }

  orderCancel.entryExitRefNum = *(unsigned short *)&dataBlock.addContent[DATABLOCK_OFFSET_ENTRY_EXIT_REF];
  
  // Call function to update Order Cancel of NYSE_CCG
  ProcessInsertCanceledOrder4NYSE_CCGQueryToCollection(&orderCancel);

  //Add a record to OJ file
  //int AddOJOrderCancelResponseRecord(char *time, char *symbol, int orderID, char *ecnOrderID, char *reason, int venueID, int sizeCanceled, int orderComplete, char *account)
  
  if (AddOrderIdToList(&__cancel, orderCancel.origClOrderID) == ERROR)  //Duplicated
  {
    return 0;
  }
  
  char ecnOrderID[32];
  sprintf(ecnOrderID, "%lu", orderCancel.meOrderID);

  char reason[128];
  if (orderCancel.infoCode == 0)
  {
    strcpy(reason, "user-initiated");
  }
  else if(orderCancel.infoCode == 1)
  {
    strcpy(reason, "exchange-initiated-Unsolicited%20UROUT");
  }
  else if(orderCancel.infoCode == 2)
  {
    strcpy(reason, "exchange-initiated-Cancel%20On%20Disconnect");
  }
  else if(orderCancel.infoCode == 3)
  {
    strcpy(reason, "exchange-initiated-Done%20for%20Day");
  }
  else
  {
    sprintf(reason, "unknown%%20reason,%%20reason%%20code%%20%%3d%%20%d", orderCancel.infoCode);
  }

  char account[32];
  GetAccountName(TYPE_NYSE_CCG, dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID], account);

  if (indexOpenOrder != ERROR)
  {
    AddOJOrderCancelResponseRecord(timeStamp, asOpenOrder.orderCollection[indexOpenOrder].orderSummary.symbol, orderCancel.origClOrderID, ecnOrderID, reason, VENUE_CCG, asOpenOrder.orderCollection[indexOpenOrder].orderSummary.leftShares, 1, account);
  }
  else
  {
    AddOJOrderCancelResponseRecord(timeStamp, "", orderCancel.origClOrderID, ecnOrderID, reason, VENUE_CCG, 0, 1, account);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessRejectOfNYSE_CCGOrder
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessRejectOfNYSE_CCGOrder( t_DataBlock dataBlock, int type, char *timeStamp )
{
  /*
   * Fields of NYSE_CCG reject order
   *
   * 1. Message type        Binary    2 bytes
   * 2. Message length      Binary    2 bytes
   * 3. Sequence number     Binary    4 bytes
   * 4. OrderID         Binary    4 bytes
   * 5. Transaction time      Binary    4 bytes
   * 6. Reject reason       Binary    2 bytes
   * 7. Reject message type   Num     1 bytes
   * 8. Deliver to compID     Alpha   5 bytes
   * 3. Target subID        Alpha   5 bytes
   * 10.Account         Alpha/Num 10 bytes
   * 11.Client orderID      Alpha/Num 17 bytes
   * 12.Original client orderID Alpha/Num 17 bytes
   * 13.Text            Alpha   40 bytes
   *
   */
  t_NYSE_CCGRejectedOrder reject;
  t_OrderDetail orderDetail;

  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;

  t_IntConverter myInt;
  t_ShortConverter myShort;

  // YYYY/mm/DD
  char dateTime[32];
    GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]), &dateTime[0]);
    strncpy( reject.processedTimestamp, &dateTime[11], TIME_LENGTH );
  reject.processedTimestamp[TIME_LENGTH] = 0;
    
    GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]), &dateTime[0]);
  strncpy( reject.date, &dateTime[0], 10 );
  reject.date[10] = 0;

  // HH:MM:SS.ms
  strncpy( reject.timestamp, &dateTime[11], TIME_LENGTH );
  reject.timestamp[TIME_LENGTH] = 0;

  // orderDetail.timestamp
  strncpy(orderDetail.timestamp, &dateTime[0], TIMESTAMP_LENGTH);
  orderDetail.timestamp[TIMESTAMP_LENGTH] = 0;

  // msgType
  myShort.c[1] = dataBlock.msgContent[0];
  myShort.c[0] = dataBlock.msgContent[1];
  int msgType = myShort.value;

  // Length
  myShort.c[1] = dataBlock.msgContent[2];
  myShort.c[0] = dataBlock.msgContent[3];
  reject.length = myShort.value;

  // Sequence number
  myInt.c[3] = dataBlock.msgContent[4];
  myInt.c[2] = dataBlock.msgContent[5];
  myInt.c[1] = dataBlock.msgContent[6];
  myInt.c[0] = dataBlock.msgContent[7];
  reject.seqNum = myInt.value;

  // OrderID
  myInt.c[3] = dataBlock.msgContent[8];
  myInt.c[2] = dataBlock.msgContent[9];
  myInt.c[1] = dataBlock.msgContent[10];
  myInt.c[0] = dataBlock.msgContent[11];
  reject.meOrderID = myInt.value;

  // Transaction time
  if (dataBlock.msgContent[12] == 0)
  {
    reject.transTime = 0;
  }
  else
  {
    myInt.c[3] = dataBlock.msgContent[12];
    myInt.c[2] = dataBlock.msgContent[13];
    myInt.c[1] = dataBlock.msgContent[14];
    myInt.c[0] = dataBlock.msgContent[15];
    reject.transTime = myInt.value;
  }

  // Reject reason
  myShort.c[1] = dataBlock.msgContent[16];
  myShort.c[0] = dataBlock.msgContent[17];
  reject.reason = myShort.value;

  // Reject type
  reject.type = dataBlock.msgContent[18];

  // Deliver to compID
  if (dataBlock.msgContent[19] == 0)
  {
    strcpy(reject.deliverToCompID, " ");
  }
  else
  {
    strncpy(reject.deliverToCompID, (char *)&dataBlock.msgContent[19], 5);
    reject.deliverToCompID[5] = 0;
  }

  // Target subID
  if (dataBlock.msgContent[24] == 0)
  {
    strcpy(reject.targetSubID, " ");
  }
  else
  {
    strncpy(reject.targetSubID, (char *)&dataBlock.msgContent[24], 5);
    reject.targetSubID[5] = 0;
  }

  // Account
  strncpy(reject.account, (char *)&dataBlock.msgContent[29], 10);
  reject.account[10] = 0;

  // client orderID
  char fieldClOrderID[18];
  strncpy(fieldClOrderID, (char *)&dataBlock.msgContent[39], 17);
  fieldClOrderID[17] = 0;
  reject.clOrderID = Convert_NYSECCG_Format_To_ClOrdID((unsigned char *)&fieldClOrderID);

  // Original client orderID
  char fieldOrgOrderID[18];
  strncpy(fieldOrgOrderID, (char *)&dataBlock.msgContent[56], 17);
  fieldOrgOrderID[17] = 0;
  reject.origClOrderID = Convert_NYSECCG_Format_To_ClOrdID((unsigned char *)&fieldOrgOrderID);

  // Update database
  int tsId;

  if( type == MANUAL_ORDER )
  {
    tsId = MANUAL_ORDER;
  }
  else
  {
    tsId = tradeServersInfo.config[type].tsId;
  }

  // Reason
  strncpy(reject.text, (char *)&dataBlock.msgContent[73], 40);
  reject.text[40] = 0;

  // Reject type
  if( reject.type == '1')
  {
    strcpy( reject.msgType, "RejectedOrder" );
    
    // Raise order rejected alert
    char _reasonCode[24];
    sprintf(_reasonCode, "%d", reject.reason);
    
    CheckNSendOrderRejectedAlertToAllTT(TYPE_NYSE_CCG, _reasonCode, reject.text, reject.clOrderID);
  }
  else if( reject.type == '2' )
  {
    strcpy( reject.msgType, "RejectedCancel" );
    reject.clOrderID = reject.origClOrderID;
  }

  // orderDetail.msgContent
  orderDetail.length = sprintf( orderDetail.msgContent,
      "%d=%X\001%d=%d\001%d=%d\001%d=%ld\001%d=%d\001%d=%d\001%d=%c\001%d=%s\001%d=%s\001%d=%s\001%d=%s\001%d=%s\001%d=%s\001",
      fieldIndexList.MsgType, msgType,
      fieldIndexList.Length, reject.length,
      fieldIndexList.InSeqNum, reject.seqNum,
      fieldIndexList.ArcaOrderID, reject.meOrderID,
      fieldIndexList.TransactionTime, reject.transTime,
      fieldIndexList.Reason, reject.reason,
      fieldIndexList.Type, reject.type,
      fieldIndexList.DeliverToCompID, reject.deliverToCompID,
      fieldIndexList.Firm, reject.targetSubID,
      fieldIndexList.Account, reject.account,
      fieldIndexList.ClOrdID, fieldClOrderID,
      fieldIndexList.OriginalClientOrdID, fieldOrgOrderID,
      fieldIndexList.RejectText, reject.text);

  strcpy( reject.msgContent, orderDetail.msgContent );

  // Call function to update a open order (non fill)
  int indexOpenOrder = ProcessUpdateASOpenOrderForStatus(tsId, reject.clOrderID, orderStatusIndex.Rejected, &orderDetail );
  if (indexOpenOrder == -2)
  {
    AddDataBlockToWaitList(TYPE_NYSE_CCG, WAIT_LIST_TYPE_CCG_REJECTED, reject.clOrderID, dataBlock, type, timeStamp);
    return 0;
  }
  
  if (indexOpenOrder != ERROR)
  {
     strcpy(reject.status, GetOrderStatus(asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status));

    // char *isFound = NULL;

    // isFound = strstr(reject.text, "Mkt/Symbol not open");
    // if (isFound != NULL) //symbol is halted
    // {
    //  BuildAndSendPMMessageToHaltSymbol(asOpenOrder.orderCollection[indexOpenOrder].orderSummary.symbol, HALT_REASON_NYSEREJECT);
    // }
    // else
    // {
    //  isFound = strstr(reject.text, "Price too far outside");
    //  if (isFound != NULL) //symbol is halted
    //  {
    //    BuildAndSendPMMessageToHaltSymbolForAECN(asOpenOrder.orderCollection[indexOpenOrder].orderSummary.symbol, HALT_REASON_NYSEREJECT, TS_NYSE_BOOK_INDEX);
    //  }
    // }
  }
  else
  {
    strcpy(reject.status, orderStatus.CancelRejected);
  }

  reject.entryExitRefNum = *(unsigned short *)&dataBlock.addContent[DATABLOCK_OFFSET_ENTRY_EXIT_REF];
  
  // Call function to update order reject or cancel reject of NYSE CCG
  ProcessInsertRejectOrder4NYSE_CCGQueryToCollection(&reject);

  //Add a new record to OJ file
  //int AddOJOrderRejectRecord(char *time, int orderID, char *ecnOrderID, char *reason, char *symbol, int venueID, char *reasonCode, char *account)

  if (AddOrderIdToList(&__reject, reject.clOrderID) == ERROR) //Duplicated
  {
    return 0;
  }
  
  if (reject.type == '2')
  {
    return 0;
  }
  
  char ecnOrderID[32];
  sprintf(ecnOrderID, "%lu", reject.meOrderID);

  char reasonCode[5];
  sprintf(reasonCode, "%d", reject.reason);

  // Convert special character to HTML format char reason[120];
  char reason[120];
  ConvertSpecialCharacter(reject.text, reason);

  char account[32];
  GetAccountName(TYPE_NYSE_CCG, dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID], account);

  if (indexOpenOrder != ERROR)
  {
    AddOJOrderRejectRecord(timeStamp, reject.clOrderID, ecnOrderID, reason, asOpenOrder.orderCollection[indexOpenOrder].orderSummary.symbol, VENUE_CCG, reasonCode, account);
  }
  else
  {
    AddOJOrderRejectRecord(timeStamp, reject.clOrderID, ecnOrderID, reason, "", VENUE_CCG, reasonCode, account);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessCancelRequestOfNYSE_CCGOrder
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessCancelRequestOfNYSE_CCGOrder( t_DataBlock dataBlock, int type, char *timeStamp )
{
  t_NYSE_CCGCancelRequest cancelRequest;
  t_OrderDetail orderDetail;
  /*------------------------------------------------
  Order detail:
  - int length;
  - int execId;
  - int isBroken;
  - char timestamp[MAX_TIMESTAMP];
  - char msgContent[MAX_ORDER_MSG];
  ------------------------------------------------*/
  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;

  t_IntConverter myInt;
  t_ShortConverter myShort;

  // YYYY/mm/DD
  char dateTime[32];
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]), &dateTime[0]);
  strncpy( cancelRequest.date, &dateTime[0], 10 );
  cancelRequest.date[10] = 0;

  // HH:MM:SS.ms
  strncpy( cancelRequest.timestamp, &dateTime[11], TIME_LENGTH );
  cancelRequest.timestamp[TIME_LENGTH] = 0;

  // orderDetail.timestamp
  strncpy(orderDetail.timestamp, &dateTime[0], TIMESTAMP_LENGTH);
  orderDetail.timestamp[TIMESTAMP_LENGTH] = 0;

  // Messsage type
  strcpy( cancelRequest.msgType, "CancelRequest" );

  // msgType
  myShort.c[1] = dataBlock.msgContent[0];
  myShort.c[0] = dataBlock.msgContent[1];
  int msgType = myShort.value;
  // %d=%d\001

  // Length
  myShort.c[1] = dataBlock.msgContent[2];
  myShort.c[0] = dataBlock.msgContent[3];
  cancelRequest.length = myShort.value;
  // %d=%d\001

  // Sequence number
  myInt.c[3] = dataBlock.msgContent[4];
  myInt.c[2] = dataBlock.msgContent[5];
  myInt.c[1] = dataBlock.msgContent[6];
  myInt.c[0] = dataBlock.msgContent[7];
  cancelRequest.seqNum = myInt.value;
  // %d=%d\001

  // MEOrderID
  myInt.c[3] = dataBlock.msgContent[8];
  myInt.c[2] = dataBlock.msgContent[9];
  myInt.c[1] = dataBlock.msgContent[10];
  myInt.c[0] = dataBlock.msgContent[11];
  cancelRequest.meOrderID = myInt.value;
  // %d=%d\001

  // original shares
  myInt.c[3] = dataBlock.msgContent[12];
  myInt.c[2] = dataBlock.msgContent[13];
  myInt.c[1] = dataBlock.msgContent[14];
  myInt.c[0] = dataBlock.msgContent[15];
  int orgShares = myInt.value;

  // leaves shares
  myInt.c[3] = dataBlock.msgContent[16];
  myInt.c[2] = dataBlock.msgContent[17];
  myInt.c[1] = dataBlock.msgContent[18];
  myInt.c[0] = dataBlock.msgContent[19];
  int leavesShares = myInt.value;

  // cancel shares
  myInt.c[3] = dataBlock.msgContent[20];
  myInt.c[2] = dataBlock.msgContent[21];
  myInt.c[1] = dataBlock.msgContent[22];
  myInt.c[0] = dataBlock.msgContent[23];
  int cancelShares = myInt.value;

  // Stock symbol
  strncpy(cancelRequest.symbol, (char *)&dataBlock.msgContent[92], SYMBOL_LEN);

  // Side "B", "S" or "SS"
  char action = dataBlock.msgContent[35];

  // OnBehalfOfCompID
  char fieldOnBeHalfOfCompID[6];
  if (dataBlock.msgContent[36] == 0)
  {
    strcpy(fieldOnBeHalfOfCompID, " ");
  }
  else
  {
    strncpy(fieldOnBeHalfOfCompID, (char *)&dataBlock.msgContent[36], 5);
    fieldOnBeHalfOfCompID[5] = 0;
  }

  // sender subID
  char fieldSenderSubID[6];
  if (dataBlock.msgContent[41] == 0)
  {
    strcpy(fieldSenderSubID, " ");
  }
  else
  {
    strncpy(fieldSenderSubID, (char *)&dataBlock.msgContent[41], 5);
    fieldSenderSubID[5] = 0;
  }

  // account
  char fieldAccount[11];
  if (dataBlock.msgContent[46] == 0)
  {
    strcpy(fieldAccount, " ");
  }
  else
  {
    strncpy(fieldAccount, (char *)&dataBlock.msgContent[46], 10);
    fieldAccount[10] = 0;
  }


  // client orderID
  char fieldClOrderID[18];
  strncpy(fieldClOrderID, (char *)&dataBlock.msgContent[56], 17);
  fieldClOrderID[17] = 0;
  cancelRequest.clOrderID = Convert_NYSECCG_Format_To_ClOrdID((unsigned char *)&fieldClOrderID);

  // Original client orderID
  char fieldOrgOrderID[18];
  strncpy(fieldOrgOrderID, (char *)&dataBlock.msgContent[73], 17);
  fieldOrgOrderID[17] = 0;
  cancelRequest.origClOrderID = Convert_NYSECCG_Format_To_ClOrdID((unsigned char *)&fieldOrgOrderID);

  int tsId;

  if( type == MANUAL_ORDER )
  {
    tsId = MANUAL_ORDER;
  }
  else
  {
    tsId = tradeServersInfo.config[type].tsId;
    NYSE_CCG_OrderID_Mapping[type][cancelRequest.clOrderID % MAX_NYSE_ORDER_ID_MAPPING] = cancelRequest.origClOrderID;
  }

  // orderDetail.msgContent
  orderDetail.length = sprintf( orderDetail.msgContent,
      "%d=%X\001%d=%d\001%d=%d\001%d=%ld\001%d=%d\001%d=%d\001%d=%d\001%d=%s\001"
      "%d=%c\001%d=%s\001%d=%s\001%d=%s\001%d=%s\001%d=%s\001",
      fieldIndexList.MsgType, msgType,
      fieldIndexList.Length, cancelRequest.length,
      fieldIndexList.InSeqNum, cancelRequest.seqNum,
      fieldIndexList.ArcaOrderID, cancelRequest.meOrderID,
      fieldIndexList.OrgShares, orgShares,
      fieldIndexList.LeavesShares, leavesShares,
      fieldIndexList.Shares, cancelShares,
      fieldIndexList.Symbol, cancelRequest.symbol,
      fieldIndexList.Side, action,
      fieldIndexList.Firm, fieldOnBeHalfOfCompID,
      fieldIndexList.SenderSubID, fieldSenderSubID,
      fieldIndexList.Account, fieldAccount,
      fieldIndexList.ClOrdID, fieldClOrderID,
      fieldIndexList.OriginalClientOrdID, fieldOrgOrderID);

  strcpy( cancelRequest.msgContent, orderDetail.msgContent );

  // Call function to update a open order (non fill)
  //int indexOpenOrder = ProcessUpdateASOpenOrderForStatus(tsId, cancelRequest.clOrderID, orderStatusIndex.CXLSent, &orderDetail );
  int indexOpenOrder = ProcessUpdateASOpenOrderForStatus(tsId, cancelRequest.origClOrderID, orderStatusIndex.CXLSent, &orderDetail );

  if (indexOpenOrder == -2)
  {
    AddDataBlockToWaitList(TYPE_NYSE_CCG, WAIT_LIST_TYPE_CCG_CANCEL_REQUEST, cancelRequest.origClOrderID, dataBlock, type, timeStamp);
    return 0;
  }
  
  if (indexOpenOrder != ERROR)
  {
    strcpy(cancelRequest.status, GetOrderStatus(asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status));
  }
  else
  {
    strcpy(cancelRequest.status, orderStatus.CXLSent);
  }

//  Call function to update cancel request of NYSE CCG
  ProcessInsertCancelRequest4NYSE_CCGQueryToCollection(&cancelRequest);

//  Add a new record to OJ file
//  int AddOJOrderCancelRecord(char *time, int orderID, char *symbol, int venueID, int size)

  if (indexOpenOrder != ERROR)
  {
    AddOJOrderCancelRecord(timeStamp, cancelRequest.origClOrderID, cancelRequest.symbol, VENUE_CCG, asOpenOrder.orderCollection[indexOpenOrder].orderSummary.leftShares);
  }
  else
  {
    AddOJOrderCancelRecord(timeStamp, cancelRequest.origClOrderID, cancelRequest.symbol, VENUE_CCG, 0);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessCancelACKOfNYSE_CCGOrder
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessCancelACKOfNYSE_CCGOrder( t_DataBlock dataBlock, int type, char *timeStamp )
{
  t_NYSE_CCGCancelACK cancelACK;
  t_OrderDetail orderDetail;
  /*------------------------------------------------
  Order detail:
  - int length;
  - int execId;
  - int isBroken;
  - char timestamp[MAX_TIMESTAMP];
  - char msgContent[MAX_ORDER_MSG];
  ------------------------------------------------*/
  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;
  
  // status
  strcpy(cancelACK.status, orderStatus.CancelACK);

  t_IntConverter myInt;
  t_ShortConverter myShort;

  // YYYY/mm/DD
  char dateTime[32];
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]), &dateTime[0]);
  strncpy( cancelACK.date, &dateTime[0], 10);
  cancelACK.date[10] = 0;

  // HH:MM:SS.ms
  strncpy( cancelACK.timestamp, &dateTime[11], TIME_LENGTH );
  cancelACK.timestamp[TIME_LENGTH] = 0;

  // orderDetail.timestamp
  strncpy(orderDetail.timestamp, &dateTime[0], TIMESTAMP_LENGTH);
  orderDetail.timestamp[TIMESTAMP_LENGTH] = 0;

  // Messsage type
  strcpy( cancelACK.msgType, "CancelACK" );

  // msgType
  myShort.c[1] = dataBlock.msgContent[0];
  myShort.c[0] = dataBlock.msgContent[1];
  int msgType = myShort.value;

  // Length
  myShort.c[1] = dataBlock.msgContent[2];
  myShort.c[0] = dataBlock.msgContent[3];
  cancelACK.length = myShort.value;
  // %d=%d\001

  // Sequence number
  myInt.c[3] = dataBlock.msgContent[4];
  myInt.c[2] = dataBlock.msgContent[5];
  myInt.c[1] = dataBlock.msgContent[6];
  myInt.c[0] = dataBlock.msgContent[7];
  cancelACK.seqNum = myInt.value;
  // %d=%d\001

  // MEOrderID
  myInt.c[3] = dataBlock.msgContent[8];
  myInt.c[2] = dataBlock.msgContent[9];
  myInt.c[1] = dataBlock.msgContent[10];
  myInt.c[0] = dataBlock.msgContent[11];
  cancelACK.meOrderID = myInt.value;
  // %d=%d\001

  // Transaction time
  if (dataBlock.msgContent[12] == 0)
  {
    cancelACK.transTime = 0;
  }
  else
  {
    myInt.c[3] = dataBlock.msgContent[12];
    myInt.c[2] = dataBlock.msgContent[13];
    myInt.c[1] = dataBlock.msgContent[14];
    myInt.c[0] = dataBlock.msgContent[15];
    cancelACK.transTime = myInt.value;
  }
  // %d=%d\001

  // Deliver to compID
  if (dataBlock.msgContent[16] == 0)
  {
    strcpy(cancelACK.deliverToCompID, " ");
  }
  else
  {
    strncpy(cancelACK.deliverToCompID, (char *)&dataBlock.msgContent[16], 5);
    cancelACK.deliverToCompID[5] = 0;
  }

  // Target subID
  char fieldTargetSubID[6];
  if (dataBlock.msgContent[21] == 0)
  {
    strcpy(fieldTargetSubID, " ");
  }
  else
  {
    strncpy(fieldTargetSubID, (char *)&dataBlock.msgContent[21], 5);
    fieldTargetSubID[5] = 0;
  }

  // Account
  char fieldAccount[11];
  if (dataBlock.msgContent[26] == 0)
  {
    strcpy(fieldAccount, " ");
  }
  else
  {
    strncpy(fieldAccount, (char *)&dataBlock.msgContent[26], 10);
    fieldAccount[10] = 0;
  }

  // client orderID
  char fieldClOrderID[18];
  strncpy(fieldClOrderID, (char *)&dataBlock.msgContent[36], 17);
  fieldClOrderID[17] = 0;
  cancelACK.clOrderID = Convert_NYSECCG_Format_To_ClOrdID((unsigned char *)&fieldClOrderID);

  int tsId;

  if( type == MANUAL_ORDER )
  {
    tsId = MANUAL_ORDER;
  }
  else
  {
    tsId = tradeServersInfo.config[type].tsId;
    cancelACK.clOrderID = NYSE_CCG_OrderID_Mapping[type][cancelACK.clOrderID % MAX_NYSE_ORDER_ID_MAPPING];
  }


  // orderDetail.msgContent
  orderDetail.length = sprintf( orderDetail.msgContent,
      "%d=%X\001%d=%d\001%d=%d\001%d=%ld\001%d=%d\001%d=%s\001%d=%s\001%d=%s\001%d=%s\001",
      fieldIndexList.MsgType, msgType,
      fieldIndexList.Length, cancelACK.length,
      fieldIndexList.InSeqNum, cancelACK.seqNum,
      fieldIndexList.ArcaOrderID, cancelACK.meOrderID,
      fieldIndexList.TransactionTime, cancelACK.transTime,
      fieldIndexList.DeliverToCompID, cancelACK.deliverToCompID,
      fieldIndexList.Firm, fieldTargetSubID,
      fieldIndexList.Account, fieldAccount,
      fieldIndexList.ClOrdID, fieldClOrderID);

  strcpy(cancelACK.msgContent, orderDetail.msgContent);

  // Call function to update a open order (non fill)
  if (ProcessUpdateASOpenOrderForStatus(tsId, cancelACK.clOrderID, orderStatusIndex.CancelACK, &orderDetail ) == -2)
  {
    AddDataBlockToWaitList(TYPE_NYSE_CCG, WAIT_LIST_TYPE_CCG_CANCEL_ACK, cancelACK.clOrderID, dataBlock, type, timeStamp);
    return SUCCESS;
  }

  // Call function to update Cancel ACK of ARCA DIRECT
  ProcessInsertCancelACK4NYSE_CCGQueryToCollection(&cancelACK);

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessBustOrCorrectOrderOfNYSE_CCGOrder
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessBustOrCorrectOrderOfNYSE_CCGOrder( t_DataBlock dataBlock, int type, char *timeStamp )
{
  t_NYSE_CCGBustOrCorrect brokenTrade;
  t_OrderDetail orderDetail;
  /*------------------------------------------------
  Order detail:
  - int length;
  - int execId;
  - int isBroken;
  - char timestamp[MAX_TIMESTAMP];
  - char msgContent[MAX_ORDER_MSG];
  ------------------------------------------------*/
  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;

  t_IntConverter myInt;
  t_ShortConverter myShort;

  // YYYY/mm/DD
  char dateTime[32];
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]), &dateTime[0]);
  strncpy( brokenTrade.date, &dateTime[0], 10 );
  brokenTrade.date[10] = 0;

  // HH:MM:SS.ms
  strncpy( brokenTrade.timestamp, &dateTime[11], TIME_LENGTH );
  brokenTrade.timestamp[TIME_LENGTH] = 0;

  // orderDetail.timestamp
  strncpy(orderDetail.timestamp, &dateTime[0], TIMESTAMP_LENGTH);
  orderDetail.timestamp[TIMESTAMP_LENGTH] = 0;

  // Messsage type
  strcpy( brokenTrade.msgType, "BrokenTrade" );

  // msgType
  myShort.c[1] = dataBlock.msgContent[0];
  myShort.c[0] = dataBlock.msgContent[1];
  int msgType = myShort.value;

  // Length
  myShort.c[1] = dataBlock.msgContent[2];
  myShort.c[0] = dataBlock.msgContent[3];
  brokenTrade.length = myShort.value;
  // %d=%d\001

  // Sequence number
  myInt.c[3] = dataBlock.msgContent[4];
  myInt.c[2] = dataBlock.msgContent[5];
  myInt.c[1] = dataBlock.msgContent[6];
  myInt.c[0] = dataBlock.msgContent[7];
  brokenTrade.seqNum = myInt.value;
  // %d=%d\001

  // MEOrderID
  myInt.c[3] = dataBlock.msgContent[8];
  myInt.c[2] = dataBlock.msgContent[9];
  myInt.c[1] = dataBlock.msgContent[10];
  myInt.c[0] = dataBlock.msgContent[11];
  brokenTrade.meOrderID = myInt.value;
  // %d=%d\001

  // Transaction time
  if (dataBlock.msgContent[12] == 0)
  {
    brokenTrade.transTime = 0;
  }
  else
  {
    myInt.c[3] = dataBlock.msgContent[12];
    myInt.c[2] = dataBlock.msgContent[13];
    myInt.c[1] = dataBlock.msgContent[14];
    myInt.c[0] = dataBlock.msgContent[15];
    brokenTrade.transTime = myInt.value;
  }

  // Report quantity: shares executed
  myInt.c[3] = dataBlock.msgContent[16];
  myInt.c[2] = dataBlock.msgContent[17];
  myInt.c[1] = dataBlock.msgContent[18];
  myInt.c[0] = dataBlock.msgContent[19];
  brokenTrade.shares = myInt.value;

  // Price and price scale
  myInt.c[3] = dataBlock.msgContent[20];
  myInt.c[2] = dataBlock.msgContent[21];
  myInt.c[1] = dataBlock.msgContent[22];
  myInt.c[0] = dataBlock.msgContent[23];
  char fieldPriceScale = dataBlock.msgContent[24];
  int fieldPrice = myInt.value;
  brokenTrade.price = fieldPrice * 1.0 / pow( 10, fieldPriceScale - 48);

  // Type
  brokenTrade.type = dataBlock.msgContent[25];

  // Deliver to compID
  if (dataBlock.msgContent[26] == 0)
  {
    strcpy(brokenTrade.deliverToCompID, " ");
  }
  else
  {
    strncpy(brokenTrade.deliverToCompID, (char *)&dataBlock.msgContent[26], 5);
    brokenTrade.deliverToCompID[5] = 0;
  }

  // Target subID
  char fieldTargetSubID[6];
  if (dataBlock.msgContent[31] == 0)
  {
    strcpy(fieldTargetSubID, " ");
  }
  else
  {
    strncpy(fieldTargetSubID, (char *)&dataBlock.msgContent[31], 5);
    fieldTargetSubID[5] = 0;
  }

  // Contra broker
  char fieldContraBroker[6];
  if (dataBlock.msgContent[36] == 0)
  {
    strcpy(fieldContraBroker, " ");
  }
  else
  {
    strncpy(fieldContraBroker, (char *)&dataBlock.msgContent[36], 5);
    fieldContraBroker[5] = 0;
  }

  // Contra trader
  strncpy(brokenTrade.contraTrader, (char *)&dataBlock.msgContent[41], 5);
  brokenTrade.contraTrader[5] = 0;

  // Exchange ID
  char fieldExecID[11];
  strncpy(fieldExecID, (char *)&dataBlock.msgContent[46], 10);
  fieldExecID[10] = 0;
  brokenTrade.execId = atol(fieldExecID);

  // Exchange RefID
  char fieldExecRefID[11];
  strncpy(fieldExecRefID, (char *)&dataBlock.msgContent[56], 10);
  fieldExecRefID[10] = 0;
  long execRefID = atol(fieldExecRefID);

  // Account
  char fieldAccount[11];
  if (dataBlock.msgContent[66] == 0)
  {
    strcpy(fieldAccount, " ");
  }
  else
  {
    strncpy(fieldAccount, (char *)&dataBlock.msgContent[66], 10);
    fieldAccount[10] = 0;
  }

  // client orderID
  char fieldClOrderID[18];
  strncpy(fieldClOrderID, (char *)&dataBlock.msgContent[76], 17);
  fieldClOrderID[17] = 0;
  brokenTrade.clOrderID = Convert_NYSECCG_Format_To_ClOrdID((unsigned char *)&fieldClOrderID);

  // orderDetail.msgContent
  orderDetail.length = sprintf( orderDetail.msgContent,
      "%d=%X\001%d=%d\001%d=%d\001%d=%ld\001%d=%d\001%d=%d\001%d=%c\001%d=%c\001%d=%s\001"
      "%d=%s\001%d=%s\001%d=%s\001%d=%ld\001%d=%ld\001%d=%s\001%d=%s\001",
          fieldIndexList.MsgType, msgType,
          fieldIndexList.Length, brokenTrade.length,
          fieldIndexList.InSeqNum, brokenTrade.seqNum,
          fieldIndexList.ArcaOrderID, brokenTrade.meOrderID,
          fieldIndexList.TransactionTime, brokenTrade.transTime,
          fieldIndexList.PriceField, fieldPrice,
          fieldIndexList.PriceScale, fieldPriceScale,
          fieldIndexList.Type, brokenTrade.type,
          fieldIndexList.DeliverToCompID, brokenTrade.deliverToCompID,
          fieldIndexList.Firm, fieldTargetSubID,
          fieldIndexList.ContraBroker, fieldContraBroker,
          fieldIndexList.ContraTrader, brokenTrade.contraTrader,
          fieldIndexList.ExecID, brokenTrade.execId,
          fieldIndexList.ExecRefID, execRefID,
          fieldIndexList.Account, fieldAccount,
//          fieldIndexList.ClOrdID, brokenTrade.clOrderID,
          fieldIndexList.ClOrdID, fieldClOrderID);

  strcpy( brokenTrade.msgContent, orderDetail.msgContent );

  // Update database
  int tsId;

  if( type == MANUAL_ORDER )
  {
    tsId = MANUAL_ORDER;
  }
  else
  {
    tsId = tradeServersInfo.config[type].tsId;
  }

  // Call function to update a open order (non fill)
  if (ProcessUpdateASOpenOrderForStatus(tsId, brokenTrade.clOrderID, orderStatusIndex.BrokenTrade, &orderDetail) == -2)
  {
    AddDataBlockToWaitList(TYPE_NYSE_CCG, WAIT_LIST_TYPE_CCG_BUST_ORDER_CORRECT, brokenTrade.clOrderID, dataBlock, type, timeStamp);
    return 0;
  }

  // Call function to update Order Fill of ARCA DIRECT
  ProcessInsertBustOrCorrect4NYSE_CCGQueryToCollection(&brokenTrade);

  //Process add broken trade message to as logs
  int indexOpenOrder = CheckOrderIdIndex(brokenTrade.clOrderID, tsId);
  if (indexOpenOrder >= 0)
  {
    TraceLog(DEBUG_LEVEL, "Broken Trade message received from NYSE CCG for symbol %.8s.\n", asOpenOrder.orderCollection[indexOpenOrder].orderSummary.symbol);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  Convert_NYSECCG_Format_To_ClOrdID
- Input:
- Output:
- Return:
- Description:
- Usage:      N/A
****************************************************************************/
int Convert_NYSECCG_Format_To_ClOrdID(unsigned char *valueFiled)
{
  int branhCode = 0, seqNumber = 0;
  //We known 8 characters in advance

  // branch code
  branhCode = (valueFiled[0] - 65)* 100;
  branhCode += (valueFiled[1] - 65)* 10;
  branhCode += valueFiled[2] - 65;

  // skip space: valueFiled[3]

  // sequence number
  seqNumber += (valueFiled[4] - 48)* 1000;
  seqNumber += (valueFiled[5] - 48)* 100;
  seqNumber += (valueFiled[6] - 48)* 10;
  seqNumber += (valueFiled[7] - 48);

  seqNumber -= 1;

  return (branhCode * 9999 + seqNumber);
}

/*****************************************************************************/
