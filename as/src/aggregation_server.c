/*****************************************************************************
**  Project:    Equity Arbitrage Application
**  Filename:   aggregation_server.c
**  Description:  This file contains function definitions that were declared
          in aggregation_server.h 
**  Author:     Luan Vo-Kinh
**  First created:  13-Sep-2007
**  Last updated: -----------
*****************************************************************************/

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include <unistd.h>
#include <fcntl.h>

#include "aggregation_server.h"
#include "global_definition.h"
#include "socket_util.h"
#include "utility.h"
#include "daedalus_proc.h"
#include "daedalus_mgmt_proc.h"
#include "trade_servers_proc.h"
#include "trade_servers_mgmt_proc.h"
#include "position_manager_proc.h"
#include "internal_proc.h"
#include "raw_data_mgmt.h"
#include "output_log_mgmt.h"
#include "order_mgmt_proc.h"
#include "arca_direct_data.h"
#include "nasdaq_ouch_data.h"
#include "nasdaq_rash_data.h"
#include "nasdaq_rash_proc.h"
#include "edge_order_data.h"
#include "arca_direct_proc.h"
#include "database_util.h"
#include "configuration.h"
#include "query_data_mgmt.h"
#include "environment_proc.h"
#include "parameters_proc.h"
#include "database_config.h"
#include "hbitime.h"
#include "logging.h"

#define VALUE_OF_OPTION_FOR_HELP 'h'

#define VALUE_OF_OPTION_FOR_TS1 1
#define VALUE_OF_OPTION_FOR_TS2 2
#define VALUE_OF_OPTION_FOR_TS3 3
#define VALUE_OF_OPTION_FOR_TS4 4
#define VALUE_OF_OPTION_FOR_TS5 5
#define VALUE_OF_OPTION_FOR_TS6 6
#define VALUE_OF_OPTION_FOR_TS7 7
#define VALUE_OF_OPTION_FOR_TS8 8
#define VALUE_OF_OPTION_FOR_TS9 9
#define VALUE_OF_OPTION_FOR_TS10 10
#define VALUE_OF_OPTION_FOR_TS11 11
#define VALUE_OF_OPTION_FOR_TS12 12
#define VALUE_OF_OPTION_FOR_TS13 13
#define VALUE_OF_OPTION_FOR_TS14 14
#define VALUE_OF_OPTION_FOR_TS15 15
#define VALUE_OF_OPTION_FOR_TS16 16
#define VALUE_OF_OPTION_FOR_ALL_TS 17

#define VALUE_OF_OPTION_FOR_NDAQ_RASH 'r'
#define VALUE_OF_OPTION_FOR_ARCA_DIRECT 'f'
#define VALUE_OF_OPTION_FOR_ALL_ORDER 18

#define VALUE_OF_OPTION_FOR_ALL 19
#define VALUE_OF_OPTION_FOR_PM 20

#define VALUE_OF_OPTION_FOR_PROCESS_DATA 21
#define VALUE_OF_OPTION_FOR_CREATE_OJ_FILE 22
#define VALUE_OF_OPTION_FOR_CREATE_ISO_FLIGHT_FILE 23
#define VALUE_OF_OPTION_FOR_VERSION_INFO 'v'

/****************************************************************************
** Global variables definition
****************************************************************************/
int isProcessData = 0;
int isCreateOJFile = 0;
int isCreateISOFlightFile = 0;
int isAutoStart = 0;
int isEnableStart = 0;

int monYearToNum = 0;


char dateString[MAX_TIMESTAMP];

time_t NumberOfSecondsBeginDate = 0, NumberOfSecondsEndDate = 0, NumberOfSecondsOldDate = 0;

t_AutoStart numSecondsAutoStartTS[MAX_TRADE_SERVER_CONNECTIONS];
t_AutoStart numSecondsAutoStartECNOrders[AS_MAX_ORDER_CONNECTIONS];
t_AutoStart numSecondsAutoStartPM;

extern t_QueryDataMgmt queryDataMgmt;
/****************************************************************************
** Function declarations
****************************************************************************/

/****************************************************************************
- Function name:  main
- Input:      + argc
          + argv
- Output:     N/A
- Return:     Success or Failure
- Description:    
- Usage:      The routine will be called by runtime environment
****************************************************************************/
int main (int argc, char *argv[])
{
  DEVLOGF("Running with extra debugging output"); 
  
  // Create data path current date
  CreateDataPathCurrentDate();

  //Load version info
  SaveLoginInfo();

  // Block the SIGCHLD
  sigset_t mask;
  sigset_t orig_mask;
  sigemptyset(&mask);
  sigaddset(&mask, SIGCHLD);
 
  if (sigprocmask(SIG_BLOCK, &mask, &orig_mask) < 0)
  {
    PrintErrStr(ERROR_LEVEL, "Can not block SIGCHLD, sigprocmask(): ", errno);
    return ERROR;
  }
  
  // Register signal handlers
  RegisterSignalHandlers();
  
  // Build connection string and create new database
  char connStr[512];
  
  if (CheckAndCreateNewDatabase(connStr) == ERROR) 
  {
    TraceLog(FATAL_LEVEL, "Can't create new database\n");

    return ERROR;
  }

  // Create connection to process raw data
  connRawData = CreateConnection(connStr, connRawData);
  
  if (connRawData == NULL)
  {
    TraceLog(FATAL_LEVEL, "Cannot open database connection\n" );

    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Create completed raw data connection\n");

  // Create connection to process query data
  connQueryData = CreateConnection(connStr, connQueryData);
  
  if (connQueryData == NULL)
  {
    TraceLog(FATAL_LEVEL, "Cannot open database connection\n" );

    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Create completed query data connection\n");

  // Create connection to save settings
  connSaveSettings = CreateConnection(connStr, connSaveSettings);
  if (connSaveSettings == NULL)
  {
    TraceLog(FATAL_LEVEL, "Cannot open database connection\n" );

    return ERROR;
  }
  TraceLog(DEBUG_LEVEL, "Create completed connection for saving settings\n");

  // Create connection for time series
  connTimeSeries = CreateConnection(connStr, connTimeSeries);
  if (connTimeSeries == NULL)
  {
    TraceLog(FATAL_LEVEL, "Cannot open database connection\n" );

    return ERROR;
  }
  TraceLog(DEBUG_LEVEL, "Create completed connection for time series\n");

  // Initialize data configs
  InitializeDataConfs();

  // Load data configuation
  if (LoadDataConfig() == ERROR)
  {
    return ERROR;
  }

  // Check user input
  if (ProcessUserInput(argc, argv) == ERROR)
  {
    return ERROR;
  }

  // Check process old data
  if (isProcessData == 1)
  {
    // Only process old data
    ProcessOldDataWithDateRange();

    return SUCCESS;
  }

  if (isCreateOJFile == 1)
  {
    // Only process old data
    CreateOJFilesWithDateRange(NumberOfSecondsBeginDate, NumberOfSecondsEndDate);

    return SUCCESS;
  }
  
  if (isCreateISOFlightFile == 1)
  {
    // Only process old data
    CreateISOFlightFilesWithDateRange(NumberOfSecondsBeginDate, NumberOfSecondsEndDate);

    return SUCCESS;
  }
  
  if (isAutoStart == 1)
  {
    // Create thread to automatic connect to the servers
    pthread_t threadAutoStart_id;
    pthread_create (&threadAutoStart_id, NULL, (void*) &ProcessAutoStartThreads, NULL);
  }

  // Start up main thread
  StartUpMainThreads();

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessUserInput
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessUserInput(int argc, char *argv[])
{
  const char *optString = "ha:vo:r:b:";
  const char* program_name;
  
  program_name = argv[0];
  
  static struct option long_opt[] = {
          {"help", 0, NULL, VALUE_OF_OPTION_FOR_HELP}, 
          {"ts1", 1, NULL, VALUE_OF_OPTION_FOR_TS1}, 
          {"ts2", 1, NULL, VALUE_OF_OPTION_FOR_TS2}, 
          {"ts3", 1, NULL, VALUE_OF_OPTION_FOR_TS3}, 
          {"ts4", 1, NULL, VALUE_OF_OPTION_FOR_TS4}, 
          {"ts5", 1, NULL, VALUE_OF_OPTION_FOR_TS5}, 
          {"ts6", 1, NULL, VALUE_OF_OPTION_FOR_TS6}, 
          {"ts7", 1, NULL, VALUE_OF_OPTION_FOR_TS7}, 
          {"ts8", 1, NULL, VALUE_OF_OPTION_FOR_TS8}, 
          {"ts9", 1, NULL, VALUE_OF_OPTION_FOR_TS9}, 
          {"ts10", 1, NULL, VALUE_OF_OPTION_FOR_TS10}, 
          {"ts11", 1, NULL, VALUE_OF_OPTION_FOR_TS11}, 
          {"ts12", 1, NULL, VALUE_OF_OPTION_FOR_TS12}, 
          {"ts13", 1, NULL, VALUE_OF_OPTION_FOR_TS13}, 
          {"ts14", 1, NULL, VALUE_OF_OPTION_FOR_TS14}, 
          {"ts15", 1, NULL, VALUE_OF_OPTION_FOR_TS15}, 
          {"ts16", 1, NULL, VALUE_OF_OPTION_FOR_TS16}, 
          {"all-ts", 1, NULL, VALUE_OF_OPTION_FOR_ALL_TS}, 
          {"pm", 1, NULL, VALUE_OF_OPTION_FOR_PM}, 
          {"ndaq-rash", 1, 0, VALUE_OF_OPTION_FOR_NDAQ_RASH}, 
          {"arca-direct", 1, 0, VALUE_OF_OPTION_FOR_ARCA_DIRECT}, 
          {"all-order", 1, NULL, VALUE_OF_OPTION_FOR_ALL_ORDER}, 
          {"all", 1, NULL, VALUE_OF_OPTION_FOR_ALL}, 
          {"old-data", 1, NULL, VALUE_OF_OPTION_FOR_PROCESS_DATA}, 
          {"oj-generate", 1, NULL, VALUE_OF_OPTION_FOR_CREATE_OJ_FILE},
          {"iso-flight-generate", 1, NULL, VALUE_OF_OPTION_FOR_CREATE_ISO_FLIGHT_FILE},
          {"version", 0 , NULL, VALUE_OF_OPTION_FOR_VERSION_INFO},
          {NULL, 0, NULL, 0}
        };

  int valOpt, option_index = -1;
  int i;

  // Initilize numSecondsAutoStartTS structure
  for (i = 0; i < MAX_TRADE_SERVER_CONNECTIONS; i++)
  {
    numSecondsAutoStartTS[i].isProcess = -1;
    numSecondsAutoStartTS[i].value = -1;
  }

  // Initilize numSecondsAutoStartECNOrders structure
  for (i = 0; i < AS_MAX_ORDER_CONNECTIONS; i++)
  {
    numSecondsAutoStartECNOrders[i].isProcess = -1;
    numSecondsAutoStartECNOrders[i].value = -1;
  }
  
  // Initilize numSecondsAutoStartPM
  numSecondsAutoStartPM.isProcess = -1;
  numSecondsAutoStartPM.value = -1;

  while (1)
  {
    valOpt = getopt_long(argc, argv, optString, long_opt, &option_index);

    if (valOpt == -1)
    {
      break;
    }

    switch (valOpt)
    {
      case VALUE_OF_OPTION_FOR_HELP:
        Print_Help(program_name);
        return ERROR;
        
      case VALUE_OF_OPTION_FOR_PROCESS_DATA:
        if (GetTimestampFromUserInput(optarg) == ERROR)
        {
          return ERROR;
        }
        else 
        {
          isProcessData = 1;

          return SUCCESS;
        }
        break;
      case VALUE_OF_OPTION_FOR_CREATE_OJ_FILE:
        if (GetTimestampFromUserInput(optarg) == ERROR)
        {
          return ERROR;
        }
        else 
        {
          isCreateOJFile = 1;

          return SUCCESS;
        }
        break;
      case VALUE_OF_OPTION_FOR_CREATE_ISO_FLIGHT_FILE:
        if (GetTimestampFromUserInput(optarg) == ERROR)
        {
          return ERROR;
        }
        else 
        {
          isCreateISOFlightFile = 1;

          return SUCCESS;
        }
        break;
      case VALUE_OF_OPTION_FOR_TS1:
        //TraceLog(DEBUG_LEVEL, "Done. Not support command for Trade Server 1 ...\n");
        if (GetOptionForTradeServer(1, optarg) == ERROR)
        {
          return ERROR;
        }
        else
        {
          isAutoStart = 1;
        }
        break;
        
      case VALUE_OF_OPTION_FOR_TS2:
        //TraceLog(DEBUG_LEVEL, "Done. Not support command for Trade Server 2 ...\n");
        if (GetOptionForTradeServer(2, optarg) == ERROR)
        {
          return ERROR;
        }
        else
        {
          isAutoStart = 1;
        }
        break;
        
      case VALUE_OF_OPTION_FOR_TS3:
        //TraceLog(DEBUG_LEVEL, "Done. Not support command for Trade Server 3 ...\n");
        if (GetOptionForTradeServer(3, optarg) == ERROR)
        {
          return ERROR;
        }
        else
        {
          isAutoStart = 1;
        }
        break;
        
      case VALUE_OF_OPTION_FOR_TS4:
        //TraceLog(DEBUG_LEVEL, "Done. Not support command for Trade Server 4 ...\n");
        if (GetOptionForTradeServer(4, optarg) == ERROR)
        {
          return ERROR;
        }
        else
        {
          isAutoStart = 1;
        }
        break;
        
      case VALUE_OF_OPTION_FOR_TS5:
        //TraceLog(DEBUG_LEVEL, "Done. Not support command for Trade Server 5 ...\n");
        if (GetOptionForTradeServer(5, optarg) == ERROR)
        {
          return ERROR;
        }
        else
        {
          isAutoStart = 1;
        }
        break;
        
      case VALUE_OF_OPTION_FOR_TS6:
        //TraceLog(DEBUG_LEVEL, "Done. Not support command for Trade Server 6 ...\n");
        if (GetOptionForTradeServer(6, optarg) == ERROR)
        {
          return ERROR;
        }
        else
        {
          isAutoStart = 1;
        }
        break;
        
      case VALUE_OF_OPTION_FOR_TS7:
        //TraceLog(DEBUG_LEVEL, "Done. Not support command for Trade Server 7 ...\n");
        if (GetOptionForTradeServer(7, optarg) == ERROR)
        {
          return ERROR;
        }
        else
        {
          isAutoStart = 1;
        }
        break;
        
      case VALUE_OF_OPTION_FOR_TS8:
        //TraceLog(DEBUG_LEVEL, "Done. Not support command for Trade Server 8 ...\n");
        if (GetOptionForTradeServer(8, optarg) == ERROR)
        {
          return ERROR;
        }
        else
        {
          isAutoStart = 1;
        }
        break;
        
      case VALUE_OF_OPTION_FOR_TS9:
        //TraceLog(DEBUG_LEVEL, "Done. Not support command for Trade Server 9 ...\n");
        if (GetOptionForTradeServer(9, optarg) == ERROR)
        {
          return ERROR;
        }
        else
        {
          isAutoStart = 1;
        }
        break;
        
      case VALUE_OF_OPTION_FOR_TS10:
        //TraceLog(DEBUG_LEVEL, "Done. Not support command for Trade Server 10 ...\n");
        if (GetOptionForTradeServer(10, optarg) == ERROR)
        {
          return ERROR;
        }
        else
        {
          isAutoStart = 1;
        }       
        break;
        
      case VALUE_OF_OPTION_FOR_TS11:
        //TraceLog(DEBUG_LEVEL, "Done. Not support command for Trade Server 11 ...\n");
        if (GetOptionForTradeServer(11, optarg) == ERROR)
        {
          return ERROR;
        }
        else
        {
          isAutoStart = 1;
        }
        break;
        
      case VALUE_OF_OPTION_FOR_TS12:
        //TraceLog(DEBUG_LEVEL, "Done. Not support command for Trade Server 12 ...\n");
        if (GetOptionForTradeServer(12, optarg) == ERROR)
        {
          return ERROR;
        }
        else
        {
          isAutoStart = 1;
        }
        break;
        
      case VALUE_OF_OPTION_FOR_TS13:
        //TraceLog(DEBUG_LEVEL, "Done. Not support command for Trade Server 13 ...\n");
        if (GetOptionForTradeServer(13, optarg) == ERROR)
        {
          return ERROR;
        }
        else
        {
          isAutoStart = 1;
        }
        break;
        
      case VALUE_OF_OPTION_FOR_TS14:
        //TraceLog(DEBUG_LEVEL, "Done. Not support command for Trade Server 14 ...\n");
        if (GetOptionForTradeServer(14, optarg) == ERROR)
        {
          return ERROR;
        }
        else
        {
          isAutoStart = 1;
        }
        break;
        
      case VALUE_OF_OPTION_FOR_TS15:
        //TraceLog(DEBUG_LEVEL, "Done. Not support command for Trade Server 15 ...\n");
        if (GetOptionForTradeServer(15, optarg) == ERROR)
        {
          return ERROR;
        }
        else
        {
          isAutoStart = 1;
        }
        break;
        
      case VALUE_OF_OPTION_FOR_TS16:
        //TraceLog(DEBUG_LEVEL, "Done. Not support command for Trade Server 16 ...\n");
        if (GetOptionForTradeServer(16, optarg) == ERROR)
        {
          return ERROR;
        }
        else
        {
          isAutoStart = 1;
        }
        break;
        
      case VALUE_OF_OPTION_FOR_ALL_TS:
        //TraceLog(DEBUG_LEVEL, "Done. Not support command for all Trade Servers ...\n");
        if (GetOptionForAllTradeServer(optarg) == ERROR)
        {
          return ERROR;
        }
        else
        {
          isAutoStart = 1;
        }
        break;
        
      case VALUE_OF_OPTION_FOR_PM:
        //TraceLog(DEBUG_LEVEL, "Done. Not support command for PM ...\n");
        if (GetOptionForPM(optarg) == ERROR)
        {
          return ERROR;
        }
        else
        {
          isAutoStart = 1;
        }
        break;
        
        
      case VALUE_OF_OPTION_FOR_NDAQ_RASH:
        //TraceLog(DEBUG_LEVEL, "Done. Not support command for NASDAQ RASH Order ...\n");
        if (GetOptionForECNOrder(AS_NASDAQ_RASH_INDEX, optarg) == ERROR)
        {
          return ERROR;
        }
        else
        {
          isAutoStart = 1;
        }
        break;
        
      case VALUE_OF_OPTION_FOR_ARCA_DIRECT:
        if (GetOptionForECNOrder(AS_ARCA_DIRECT_INDEX, optarg) == ERROR)
        {
          return ERROR;
        }
        else
        {
          isAutoStart = 1;
        }
        break;
        
      case VALUE_OF_OPTION_FOR_ALL_ORDER:
        //TraceLog(DEBUG_LEVEL, "Done. Not support command for all ECN Orders ...\n");
        if (GetOptionForAllECNOrders(optarg) == ERROR)
        {
          return ERROR;
        }
        else
        {
          isAutoStart = 1;
        }
        break;
        
      case VALUE_OF_OPTION_FOR_ALL:
        //TraceLog(DEBUG_LEVEL, "Done. Not support command for all Trade Servers and all ECN Orders ...\n");
        if (GetOptionForAllConnections(optarg) == ERROR)
        {
          return ERROR;
        }
        else
        {
          isAutoStart = 1;
        }
        break;
      case VALUE_OF_OPTION_FOR_VERSION_INFO:

        TraceLog(DEBUG_LEVEL, "************************************************************\n");
        TraceLog(DEBUG_LEVEL, "Version of AS: '%s' \n", VERSION);
        TraceLog(DEBUG_LEVEL, "************************************************************\n");

        return ERROR;
      default:
        TraceLog(ERROR_LEVEL, "Invalid commands. \"-h\" or \"--help\" for list command\n\n");
        return ERROR;
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  Print_Help
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int Print_Help(const char *program_name)
{
  TraceLog(USAGE_LEVEL, "******************************************************************************\n");
  TraceLog(USAGE_LEVEL, "* -h --help for list commands\n");
  TraceLog(USAGE_LEVEL, "* --old-data [b=YYYY/MM/DD@e=YYYY/MM/DD]|[d=YYYY/MM/DD]: process old data for date range from 'b' to 'e', or for specific date 'd'\n");
  TraceLog(USAGE_LEVEL, "* --oj-generate [b=YYYY/MM/DD@e=YYYY/MM/DD]|[d=YYYY/MM/DD]: generate Order Journal files for date range from 'b' to 'e', or for specific date 'd'\n");
  TraceLog(USAGE_LEVEL, "* --iso-flight-generate  [b=YYYY/MM/DD@e=YYYY/MM/DD]|[d=YYYY/MM/DD]: generate ISO Flight files for date range from 'b' to 'e', or for specific date 'd'\n");
  TraceLog(USAGE_LEVEL, "* --ts<x> <hh:mm>: time to connect to Trade Server x\n");
  TraceLog(USAGE_LEVEL, "* --pm <hh:mm>: time to connect to Position Manager\n");
  TraceLog(USAGE_LEVEL, "* --all-ts <hh:mm>: time to connect to all Trade Servers\n");
  TraceLog(USAGE_LEVEL, "* -r --ndaq-rash <hh:mm>: time to connect to NASDAQ RASH Order\n");
  TraceLog(USAGE_LEVEL, "* -f --arca-direct <hh:mm>: time to connect to ARCA DIRECT Order\n");
  TraceLog(USAGE_LEVEL, "* -v --version for show AS's version \n\n");
  TraceLog(USAGE_LEVEL, "* --all-order <hh:mm>: time to connect to all ECN Orders\n");
  TraceLog(USAGE_LEVEL, "* --all <hh:mm>: time to connect to all Trade Servers and all ECN Orders\n\n");
  
  TraceLog(USAGE_LEVEL, "* Example:\n");
  TraceLog(USAGE_LEVEL, "* 1. Only to process old data from 2007/10/01 to 2007/11/07:\n  $%s --old-data b=2007/10/01@e=2007/11/07\n", program_name);
  TraceLog(USAGE_LEVEL, "* 2. Only to process old data at 2007/11/07:\n  $%s --old-data d=2007/11/07\n", program_name);
  TraceLog(USAGE_LEVEL, "* 3. To set time to connect to all ECN Orders at 6:30 AM, and all Trade Servers at 6:45 AM:\n  $%s --all-order 06:30 --all-ts 06:45\n", program_name);
  TraceLog(USAGE_LEVEL, "* 4. To generate Order Journal files for date range from 2012/05/01 to 2012/05/31:\n  $%s --oj-generate b=2012/05/01@e=2012/05/31\n", program_name);
  TraceLog(USAGE_LEVEL, "* 5. To generate Order Journal file for one day 2012/05/15:\n  $%s --oj-generate d=2012/05/15\n", program_name);
  TraceLog(USAGE_LEVEL, "******************************************************************************\n\n");

  return SUCCESS;
}

/****************************************************************************
- Function name:  GetTimestampFromUserInput
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int GetTimestampFromUserInput(const char *arg)
{
  int valRet;
  t_Parameters filedList;

  if ((arg[0] == 'd') && (arg[1] == '='))
  {
    //  Get seconds since 1970
    valRet = GetNumberOfSecondsFrom1970("%Y/%m/%d", &arg[2]);

    if (valRet != ERROR)
    {     
      NumberOfSecondsOldDate = valRet;
      TraceLog(DEBUG_LEVEL, "NumberOfSecondsOldDate = %d\n", (int)NumberOfSecondsOldDate);

      NumberOfSecondsBeginDate = NumberOfSecondsOldDate;
      NumberOfSecondsEndDate = NumberOfSecondsOldDate;

      return SUCCESS;
    }
    else
    {
      TraceLog(ERROR_LEVEL, "Invalid old date: %s\n", arg);
      TraceLog(ERROR_LEVEL, "-h --help for list commands\n");

      return ERROR;
    }
  }
  else if ((arg[0] == 'b') && (arg[1] == '='))
  {
    filedList.countParameters = Lrc_Split(arg, '@', &filedList);

    if (filedList.countParameters != 2)
    {
      TraceLog(ERROR_LEVEL, "Invalid begin date or end date: %s\n", arg);
      TraceLog(ERROR_LEVEL, "-h --help for list commands\n");

      return ERROR;
    }

    //  Get seconds since 1970
    valRet = GetNumberOfSecondsFrom1970("%Y/%m/%d", &filedList.parameterList[0][2]);

    if (valRet != ERROR)
    {
      NumberOfSecondsBeginDate = valRet;
      TraceLog(DEBUG_LEVEL, "NumberOfSecondsBeginDate = %d\n", (int)NumberOfSecondsBeginDate);
    }
    else
    {
      TraceLog(ERROR_LEVEL, "Invalid begin date or end date: %s\n", arg);
      TraceLog(ERROR_LEVEL, "-h --help for list commands\n");

      return ERROR;
    }

    if ((filedList.parameterList[1][0] == 'e') && (filedList.parameterList[1][1] == '='))
    {
      //  Get seconds since 1970
      valRet = GetNumberOfSecondsFrom1970("%Y/%m/%d", &filedList.parameterList[1][2]);

      if (valRet != 0)
      {
        NumberOfSecondsEndDate = valRet;
        TraceLog(DEBUG_LEVEL, "NumberOfSecondsEndDate = %d\n", (int)NumberOfSecondsEndDate);
      }
      else
      {
        TraceLog(ERROR_LEVEL, "Invalid begin date or end date: %s\n", arg);
        TraceLog(ERROR_LEVEL, "-h --help for list commands\n");

        return ERROR;
      }
    }
    else
    {
      TraceLog(ERROR_LEVEL, "Invalid begin date or end date: %s\n", arg);
      TraceLog(ERROR_LEVEL, "-h --help for list commands\n");

      return ERROR;
    }

    if (NumberOfSecondsBeginDate > NumberOfSecondsEndDate)
    {
      TraceLog(ERROR_LEVEL, "Begin date (%d) > end date (%d)\n", (int)NumberOfSecondsBeginDate, (int)NumberOfSecondsEndDate);
      TraceLog(ERROR_LEVEL, "-h --help for list commands\n");

      return ERROR;
    }
  }
  else
  {
    TraceLog(ERROR_LEVEL, "Invalid begin date or end date: %s\n", arg);
    TraceLog(ERROR_LEVEL, "-h --help for list commands\n");

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  GetOptionForTradeServer
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int GetOptionForTradeServer(int tsId, const char *arg)
{
  t_Parameters paraList;
  int tmpHour, tmpMin;

  int tsIndex = FindTradeServerIndex(tsId);

  if (tsIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "Cannot automatic connect to TS%d because configuration of TS%d is not exist\n", tsId, tsId);

    return ERROR;
  }

  // Parse ...
  paraList.countParameters = Lrc_Split(arg, ':', &paraList);
  if (paraList.countParameters != 2)
  {
    TraceLog(ERROR_LEVEL, "Invalid option to AS automatic connect to TS%d, optarg = %s\n", tsId, arg);
    TraceLog(ERROR_LEVEL, "-h --help for list commands\n");

    return ERROR;
  }

  tmpHour = atoi(paraList.parameterList[0]);
  if ((tmpHour < 0) || (tmpHour > 23))
  {
    TraceLog(ERROR_LEVEL, "Invalid option to AS automatic connect to TS%d, hour(s) = %s\n", tsId, paraList.parameterList[0]);
    TraceLog(ERROR_LEVEL, "-h --help for list commands\n");

    return ERROR;
  }
  else if (tmpHour == 0)
  {
    if ((paraList.parameterList[0][0] < 48) || (paraList.parameterList[0][1] > 57))
    {
      TraceLog(ERROR_LEVEL, "Invalid option to AS automatic connect to TS%d, hour(s) = %s\n", tsId, paraList.parameterList[0]);
      TraceLog(ERROR_LEVEL, "-h --help for list commands\n");

      return ERROR;
    }
  }
  
  tmpMin = atoi(paraList.parameterList[1]);
  if ((tmpMin < 0) || (tmpMin > 59))
  {
    TraceLog(ERROR_LEVEL, "Invalid option to AS automatic connect to TS%d, minute(s) = %s\n", tsId, paraList.parameterList[1]);
    TraceLog(ERROR_LEVEL, "-h --help for list commands\n");

    return ERROR;
  }
  else if (tmpMin == 0)
  {
    if ((paraList.parameterList[1][0] < 48) || (paraList.parameterList[1][1] > 57))
    {
      TraceLog(ERROR_LEVEL, "Invalid option to AS automatic connect to TS%d, minute(s) = %s\n", tsId, paraList.parameterList[1]);
      TraceLog(ERROR_LEVEL, "-h --help for list commands\n");

      return ERROR;
    }
  }

  TraceLog(DEBUG_LEVEL, "Done. AS will automatic connect to TS%d at %02d:%02d\n", tsId, tmpHour, tmpMin);

  numSecondsAutoStartTS[tsIndex].value = tmpHour * 60 + tmpMin;

  return SUCCESS;
}

/****************************************************************************
- Function name:  GetOptionForAllTradeServer
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int GetOptionForAllTradeServer(const char *arg)
{
  t_Parameters paraList;
  int tmpHour, tmpMin;

  // Parse ...
  paraList.countParameters = Lrc_Split(arg, ':', &paraList);
  if (paraList.countParameters != 2)
  {
    TraceLog(ERROR_LEVEL, "Invalid option to AS automatic connect to all TSs, optarg = %s\n", arg);
    TraceLog(ERROR_LEVEL, "-h --help for list commands\n");

    return ERROR;
  }

  tmpHour = atoi(paraList.parameterList[0]);
  if ((tmpHour < 0) || (tmpHour > 23))
  {
    TraceLog(ERROR_LEVEL, "Invalid option to AS automatic connect to all TSs, hour(s) = %s\n", paraList.parameterList[0]);
    TraceLog(ERROR_LEVEL, "-h --help for list commands\n");

    return ERROR;
  }
  else if (tmpHour == 0)
  {
    if ((paraList.parameterList[0][0] < 48) || (paraList.parameterList[0][1] > 57))
    {
      TraceLog(ERROR_LEVEL, "Invalid option to AS automatic connect to all TSs, hour(s) = %s\n", paraList.parameterList[0]);
      TraceLog(ERROR_LEVEL, "-h --help for list commands\n");

      return ERROR;
    }
  }
  
  tmpMin = atoi(paraList.parameterList[1]);
  if ((tmpMin < 0) || (tmpMin > 59))
  {
    TraceLog(ERROR_LEVEL, "Invalid option to AS automatic connect to all TSs, minute(s) = %s\n", paraList.parameterList[1]);
    TraceLog(ERROR_LEVEL, "-h --help for list commands\n");

    return ERROR;
  }
  else if (tmpMin == 0)
  {
    if ((paraList.parameterList[1][0] < 48) || (paraList.parameterList[1][1] > 57))
    {
      TraceLog(ERROR_LEVEL, "Invalid option to AS automatic connect to all TSs, minute(s) = %s\n", paraList.parameterList[1]);
      TraceLog(ERROR_LEVEL, "-h --help for list commands\n");

      return ERROR;
    }
  }

  TraceLog(DEBUG_LEVEL, "Done. AS will automatic connect to all TSs at %02d:%02d\n", tmpHour, tmpMin);

  int i;

  for (i = 0; i < MAX_TRADE_SERVER_CONNECTIONS; i++)
  {
    numSecondsAutoStartTS[i].value = tmpHour * 60 + tmpMin;
  } 

  return SUCCESS;
}

/****************************************************************************
- Function name:  GetOptionForPM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int GetOptionForPM(const char *arg)
{
  t_Parameters paraList;
  int tmpHour, tmpMin;
  
  // Parse ...
  paraList.countParameters = Lrc_Split(arg, ':', &paraList);
  if (paraList.countParameters != 2)
  {
    TraceLog(ERROR_LEVEL, "Invalid option to AS automatic connect to PM, optarg = %s\n", arg);
    TraceLog(ERROR_LEVEL, "-h --help for list command\n");

    return ERROR;
  }

  tmpHour = atoi(paraList.parameterList[0]);
  if ((tmpHour < 0) || (tmpHour > 23))
  {
    TraceLog(ERROR_LEVEL, "Invalid option to AS automatic connect to PM, hour(s) = %s\n", paraList.parameterList[0]);
    TraceLog(ERROR_LEVEL, "-h --help for list command\n");

    return ERROR;
  }
  else if (tmpHour == 0)
  {
    if ((paraList.parameterList[0][0] < 48) || (paraList.parameterList[0][1] > 57))
    {
      TraceLog(ERROR_LEVEL, "Invalid option to AS automatic connect to PM, hour(s) = %s\n", paraList.parameterList[0]);
      TraceLog(ERROR_LEVEL, "-h --help for list command\n");

      return ERROR;
    }
  }
  
  tmpMin = atoi(paraList.parameterList[1]);
  if ((tmpMin < 0) || (tmpMin > 59))
  {
    TraceLog(ERROR_LEVEL, "Invalid option to AS automatic connect to PM, minute(s) = %s\n", paraList.parameterList[1]);
    TraceLog(ERROR_LEVEL, "-h --help for list command\n");

    return ERROR;
  }
  else if (tmpMin == 0)
  {
    if ((paraList.parameterList[1][0] < 48) || (paraList.parameterList[1][1] > 57))
    {
      TraceLog(ERROR_LEVEL, "Invalid option to AS automatic connect to PM, minute(s) = %s\n", paraList.parameterList[1]);
      TraceLog(ERROR_LEVEL, "-h --help for list command\n");

      return ERROR;
    }
  }

  TraceLog(DEBUG_LEVEL, "Done. AS will automatic connect to PM at %02d:%02d\n", tmpHour, tmpMin);

  numSecondsAutoStartPM.value = tmpHour * 60 + tmpMin;

  return SUCCESS;
}

/****************************************************************************
- Function name:  GetOptionForECNOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int GetOptionForECNOrder(int ecnId, const char *arg)
{
  t_Parameters paraList;
  int tmpHour, tmpMin;

  char ecnName[20];

  switch (ecnId)
  {
    case AS_NASDAQ_RASH_INDEX:
      strcpy(ecnName, "NASDAQ RASH");
      break;
    case AS_ARCA_DIRECT_INDEX:
      strcpy(ecnName, "ARCA DIRECT");
      break;
  }

  // Parse ...
  paraList.countParameters = Lrc_Split(arg, ':', &paraList);
  if (paraList.countParameters != 2)
  {
    TraceLog(ERROR_LEVEL, "Invalid option to AS automatic connect to %s Order, optarg = %s\n", ecnName, arg);
    TraceLog(ERROR_LEVEL, "-h --help for list commands\n");

    return ERROR;
  }

  tmpHour = atoi(paraList.parameterList[0]);
  if ((tmpHour < 0) || (tmpHour > 23))
  {
    TraceLog(ERROR_LEVEL, "Invalid option to AS automatic connect to %s Order, hour(s) = %s\n", ecnName, paraList.parameterList[0]);
    TraceLog(ERROR_LEVEL, "-h --help for list commands\n");

    return ERROR;
  }
  else if (tmpHour == 0)
  {
    if ((paraList.parameterList[0][0] < 48) || (paraList.parameterList[0][1] > 57))
    {
      TraceLog(ERROR_LEVEL, "Invalid option to AS automatic connect to %s Order, hour(s) = %s\n", ecnName, paraList.parameterList[0]);
      TraceLog(ERROR_LEVEL, "-h --help for list commands\n");

      return ERROR;
    }
  }
  
  tmpMin = atoi(paraList.parameterList[1]);
  if ((tmpMin < 0) || (tmpMin > 59))
  {
    TraceLog(ERROR_LEVEL, "Invalid option to AS automatic connect to %s Order, minute(s) = %s\n", ecnName, paraList.parameterList[1]);
    TraceLog(ERROR_LEVEL, "-h --help for list commands\n");

    return ERROR;
  }
  else if (tmpMin == 0)
  {
    if ((paraList.parameterList[1][0] < 48) || (paraList.parameterList[1][1] > 57))
    {
      TraceLog(ERROR_LEVEL, "Invalid option to AS automatic connect to %s Order, minute(s) = %s\n", ecnName, paraList.parameterList[1]);
      TraceLog(ERROR_LEVEL, "-h --help for list commands\n");

      return ERROR;
    }
  }

  TraceLog(DEBUG_LEVEL, "Done. AS will automatic connect to %s Order at %02d:%02d\n", ecnName, tmpHour, tmpMin);

  numSecondsAutoStartECNOrders[ecnId].value = tmpHour * 60 + tmpMin;

  return SUCCESS;
}

/****************************************************************************
- Function name:  GetOptionForAllECNOrders
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int GetOptionForAllECNOrders(const char *arg)
{
  t_Parameters paraList;
  int tmpHour, tmpMin;

  // Parse ...
  paraList.countParameters = Lrc_Split(arg, ':', &paraList);
  if (paraList.countParameters != 2)
  {
    TraceLog(ERROR_LEVEL, "Invalid option to AS automatic connect to all ECN Orders, optarg = %s\n", arg);
    TraceLog(ERROR_LEVEL, "-h --help for list commands\n");

    return ERROR;
  }

  tmpHour = atoi(paraList.parameterList[0]);
  if ((tmpHour < 0) || (tmpHour > 23))
  {
    TraceLog(ERROR_LEVEL, "Invalid option to AS automatic connect to all ECN Orders, hour(s) = %s\n", paraList.parameterList[0]);
    TraceLog(ERROR_LEVEL, "-h --help for list commands\n");

    return ERROR;
  }
  else if (tmpHour == 0)
  {
    if ((paraList.parameterList[0][0] < 48) || (paraList.parameterList[0][1] > 57))
    {
      TraceLog(ERROR_LEVEL, "Invalid option to AS automatic connect to all ECN Orders, hour(s) = %s\n", paraList.parameterList[0]);
      TraceLog(ERROR_LEVEL, "-h --help for list commands\n");

      return ERROR;
    }
  }
  
  tmpMin = atoi(paraList.parameterList[1]);
  if ((tmpMin < 0) || (tmpMin > 59))
  {
    TraceLog(ERROR_LEVEL, "Invalid option to AS automatic connect to all ECN Orders, minute(s) = %s\n", paraList.parameterList[1]);
    TraceLog(ERROR_LEVEL, "-h --help for list commands\n");

    return ERROR;
  }
  else if (tmpMin == 0)
  {
    if ((paraList.parameterList[1][0] < 48) || (paraList.parameterList[1][1] > 57))
    {
      TraceLog(ERROR_LEVEL, "Invalid option to AS automatic connect to all ECN Orders, minute(s) = %s\n", paraList.parameterList[1]);
      TraceLog(ERROR_LEVEL, "-h --help for list commands\n");

      return ERROR;
    }
  }

  TraceLog(DEBUG_LEVEL, "Done. AS will automatic connect to all ECN Orders at %02d:%02d\n", tmpHour, tmpMin);

  int i;

  for (i = 0; i < AS_MAX_ORDER_CONNECTIONS; i++)
  {
    numSecondsAutoStartECNOrders[i].value = tmpHour * 60 + tmpMin;
  } 

  return SUCCESS;
}

/****************************************************************************
- Function name:  GetOptionForAllConnections
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int GetOptionForAllConnections(const char *arg)
{
  t_Parameters paraList;
  int tmpHour, tmpMin;

  // Parse ...
  paraList.countParameters = Lrc_Split(arg, ':', &paraList);
  if (paraList.countParameters != 2)
  {
    TraceLog(ERROR_LEVEL, "Invalid option to AS automatic connect to all TSs and all ECN Orders, optarg = %s\n", arg);
    TraceLog(ERROR_LEVEL, "-h --help for list commands\n");

    return ERROR;
  }

  tmpHour = atoi(paraList.parameterList[0]);
  if ((tmpHour < 0) || (tmpHour > 23))
  {
    TraceLog(ERROR_LEVEL, "Invalid option to AS automatic connect to all TSs and all ECN Orders, hour(s) = %s\n", paraList.parameterList[0]);
    TraceLog(ERROR_LEVEL, "-h --help for list commands\n");

    return ERROR;
  }
  else if (tmpHour == 0)
  {
    if ((paraList.parameterList[0][0] < 48) || (paraList.parameterList[0][1] > 57))
    {
      TraceLog(ERROR_LEVEL, "Invalid option to AS automatic connect to all TSs and all ECN Orders, hour(s) = %s\n", paraList.parameterList[0]);
      TraceLog(ERROR_LEVEL, "-h --help for list commands\n");

      return ERROR;
    }
  }
  
  tmpMin = atoi(paraList.parameterList[1]);
  if ((tmpMin < 0) || (tmpMin > 59))
  {
    TraceLog(ERROR_LEVEL, "Invalid option to AS automatic connect to all TSs and all ECN Orders, minute(s) = %s\n", paraList.parameterList[1]);
    TraceLog(ERROR_LEVEL, "-h --help for list commands\n");

    return ERROR;
  }
  else if (tmpMin == 0)
  {
    if ((paraList.parameterList[1][0] < 48) || (paraList.parameterList[1][1] > 57))
    {
      TraceLog(ERROR_LEVEL, "Invalid option to AS automatic connect to all TSs and all ECN Orders, minute(s) = %s\n", paraList.parameterList[1]);
      TraceLog(ERROR_LEVEL, "-h --help for list commands\n");

      return ERROR;
    }
  }

  TraceLog(DEBUG_LEVEL, "Done. AS will automatic connect to all TSs and all ECN Orders at %02d:%02d\n", tmpHour, tmpMin);

  int i;

  for (i = 0; i < MAX_TRADE_SERVER_CONNECTIONS; i++)
  {
    numSecondsAutoStartTS[i].value = tmpHour * 60 + tmpMin;
  }

  for (i = 0; i < AS_MAX_ORDER_CONNECTIONS; i++)
  {
    numSecondsAutoStartECNOrders[i].value = tmpHour * 60 + tmpMin;
  }
  
  numSecondsAutoStartPM.value = tmpHour * 60 + tmpMin;

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessOldDataWithDateRange
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessOldDataWithDateRange(void)
{
  struct tm *l_time;
  int numSecondsBefore, numSecondsAfter;
  time_t numberOfSecondsTmp = NumberOfSecondsBeginDate;
  char connStr[512];

  while (numberOfSecondsTmp <= NumberOfSecondsEndDate)
  {
    TraceLog(DEBUG_LEVEL, "***************************************************\n");

    // Convert time_t to tm
    l_time = (struct tm *)localtime(&numberOfSecondsTmp);

    //  We only trade from Monday to Friday
    if ((l_time->tm_wday > 0) && (l_time->tm_wday < 6))
    {
      if (((l_time->tm_year % 100) * 100 + l_time->tm_mon) != monYearToNum) 
      {
        if (monYearToNum != 0)
        {
          DestroyConnection(connRawData);
          DestroyConnection(connSaveSettings);
          DestroyConnection(connQueryData);
        }
        
        // Update monYearToNum
        monYearToNum = (l_time->tm_year % 100) * 100 + l_time->tm_mon;
        
        // Connect to database
        if (get_monthly_db_connection_string("eaa", l_time->tm_year + 1900, l_time->tm_mon + 1, connStr, 512) == NULL_POINTER) {
          TraceLog(ERROR_LEVEL, "Cannot get database configuration\n" );
          return ERROR;
        }

        TraceLog(DEBUG_LEVEL, "connectionString = %s\n", connStr);

        // Create connection to process raw data
        connRawData = CreateConnection(connStr, connRawData);
        
        if (connRawData == NULL)
        {
          TraceLog(ERROR_LEVEL, "Cannot open database connection\n" );

          return ERROR;
        }

        TraceLog(DEBUG_LEVEL, "Create completed raw data connection\n");

        // Create connection to process query data
        connQueryData = CreateConnection(connStr, connQueryData);
        
        if (connQueryData == NULL)
        {
          TraceLog(ERROR_LEVEL, "Cannot open database connection\n" );

          return ERROR;
        }

        TraceLog(DEBUG_LEVEL, "Create completed query data connection\n");
      }
      
      numSecondsBefore = hbitime_seconds();

      // Create data path at date
      CreateDataPathAtDate(numberOfSecondsTmp);

      // Initialize data structures
      if (InitializeDataStructures() == ERROR)
      {
        return ERROR;
      }

      // Remove all data at this date
      if (RemoveAllDataAtDate() == ERROR)
      {
        return ERROR;
      }

      // Load data into data structures
      if (LoadDataStructures() == ERROR)
      {
        return ERROR;
      }

      //Get manual orderID and username who placed manual order, for OJ record creation
      GetManualOrderInfoFromDatabase(globalDateStringOfDatabase);
  
      // Process history output log
      ProcessHistoryOutputLog();

      // Process history raw data
      ProcessHistoryRawData();
  
      // Process history hung order
      ProcessHistoryHungOrder();
      
      // Process history bbo info
      ProcessHistoryBBOInfo();
      
      // Process history of query data
      ProcessHistoryOfQueryData();

      numSecondsAfter = hbitime_seconds();

      TraceLog(DEBUG_LEVEL, "Number of seconds to process: %d\n", numSecondsAfter - numSecondsBefore);
    }
    else
    {
      TraceLog(DEBUG_LEVEL, "** This is day off. \n");

      if (l_time->tm_wday == 6)
      {
        TraceLog(DEBUG_LEVEL, "Because it is Saturday\n");
      }
      else
      {
        TraceLog(DEBUG_LEVEL, "Because it is Sunday\n");
      }

      TraceLog(DEBUG_LEVEL, "***************************************************\n");
    }

    //  Increasing 1 day
    numberOfSecondsTmp += NUMBER_OF_SECONDS_PER_DAY;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  RemoveAllDataAtDate
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int RemoveAllDataAtDate(void)
{
  char fullPath[MAX_PATH_LEN] = "\0";
  char command[MAX_PATH_LEN] = "\0";

  // Remove raw data processed
  if (GetFullRawDataPath(FILE_NAME_OF_RAW_DATA_PROCESSED, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Have some problem in get full raw data path operation (%s)\n", FILE_NAME_OF_RAW_DATA_PROCESSED);

    return ERROR;
  } 

  sprintf(command, "rm -f %s", fullPath);

  //TraceLog(DEBUG_LEVEL, "command = %s\n", command);

  // Call system()
  system(command);

  // Remove output log processed
  if (GetFullRawDataPath(FILE_NAME_OF_OUTPUT_LOG_PROCESSED, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Have some problem in get full raw data path operation (%s)\n", FILE_NAME_OF_OUTPUT_LOG_PROCESSED);

    return ERROR;
  }

  sprintf(command, "rm -f %s", fullPath);

  //TraceLog(DEBUG_LEVEL, "command = %s\n", command);

  // Call system()
  system(command);
  
  // Remove BBO info processed
  if (GetFullRawDataPath(FILE_NAME_OF_BBO_INFO_PROCESSED, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Have some problem in get full raw data path operation (%s)\n", FILE_NAME_OF_OUTPUT_LOG_PROCESSED);

    return ERROR;
  }

  sprintf(command, "rm -f %s", fullPath);

  //TraceLog(DEBUG_LEVEL, "command = %s\n", command);

  // Call system()
  system(command);

  // Reset cross id
  if (GetFullConfigPath(FILE_NAME_OF_CROSS_ID, CONF_MISC, fullPath) == NULL_POINTER)
  {
    return ERROR;
  }

  sprintf(command, "rm -f %s", fullPath);

  //TraceLog(DEBUG_LEVEL, "command = %s\n", command);

  // Call system()
  system(command);
  
  // Remove query index processed file
  if (GetFullRawDataPath(FILE_NAME_OF_QUERY_PROCESSED, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Have some problem in get full query processed position path operation (%s)\n", FILE_NAME_OF_QUERY_PROCESSED);

    return ERROR;
  }

  sprintf(command, "rm -f %s", fullPath);
  
  // Call system()
  system(command);

  // Remove query collection data file
  if (GetFullRawDataPath(FILE_NAME_OF_QUERIES_COLLECTION, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Have some problem in get full query collection data path operation (%s)\n", FILE_NAME_OF_QUERIES_COLLECTION);

    return ERROR;
  }

  sprintf(command, "rm -f %s", fullPath);
  
  // Call system()
  system(command);
  
  // Remove data in database
  DeleteDatabaseAtDate(globalDateStringOfDatabase);

  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadDataConfig
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int LoadDataConfig(void)
{
  // Load TT module version
  if (GetModuleVersion(TT_MODULE) == ERROR)
  {
    return ERROR;
  }
  
  // Load Trader Tools configuration
  if (LoadTraderToolsConf() == ERROR)
  {
    return ERROR;
  }

  // Load PM configuration
  if (LoadPMConf() == ERROR)
  {
    return ERROR;
  }

  // Load Trade Servers configuration
  if (LoadTradeServersConf() == ERROR)
  {
    return ERROR;
  }

  // Load ignore stock list
  if (LoadIgnoreStockList() == ERROR)
  {
    return ERROR;
  }
  
  //Load ignore stock ranges list
  if( LoadIgnoreStockRangesList() == ERROR)
  {
    return ERROR;
  }
  
  // Load Aggregation Server global configuration
  if (LoadASGlobalConf() == ERROR)
  {
    return ERROR;
  } 

  // Load AS Orders configuration
  if (LoadASOrdersConf() == ERROR)
  {
    return ERROR;
  }

  // We should not keep volatile list overnight
  CheckToDeleteOldVolatileFiles();
  
  // Init & load trading account
  memset(&TradingAccount, 0, sizeof(TradingAccount));
  if (LoadTradingAccountFromFile("trading_account", &TradingAccount) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot load Trading Account configuration!\n");
    return ERROR;
  }
  
  //Load fee config
  if (LoadFeeConf() == ERROR)
  {
    return ERROR;
  }
  
  //Load alert config
  if (LoadAlertConf() == ERROR)
  {
    return ERROR;
  }

  //Load reject code for all orders
  if (LoadRejectCodeFromFileOfAllOrders() == ERROR)
  {
    return ERROR;
  }
  
  if (LoadShortabilitySchedule() == ERROR)
  {
    return ERROR;
  }

  if (LoadLdapConfiguration() == ERROR)
  {
    return ERROR;
  }
  
  if (LoadEcnSettingSchedule() == ERROR)
  {
    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadDataStructures
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int LoadDataStructures(void)
{
  // Load position raw data processed from file
  if (LoadPositionRawDataProcessedFromFile(FILE_NAME_OF_RAW_DATA_PROCESSED) == ERROR)
  {
    return ERROR;
  }

  // Load position output log processed from file
  if (LoadPositionOutputLogProcessedFromFile(FILE_NAME_OF_OUTPUT_LOG_PROCESSED) == ERROR)
  {
    return ERROR;
  }
  
  // Load position bbo info processed from file
  if (LoadPositionBBOInfoProcessedFromFile(FILE_NAME_OF_BBO_INFO_PROCESSED) == ERROR)
  {
    return ERROR;
  }
  
  // Load position query from file
  if (LoadPositionQueryProcessedFromFile(FILE_NAME_OF_QUERY_PROCESSED) == ERROR)
  {
    return ERROR;
  }
  
  // Get number of query data entries from file
  if (GetNumberOfQueryEntriesFromFile(FILE_NAME_OF_QUERIES_COLLECTION, &queryDataMgmt) == ERROR)
  {
    return ERROR;
  }
  
  // Load rawdata wait-list from file
  LoadAndProcessRawdataWaitList();
  
  // Load hung order from file
  LoadHungOrderListFromFile();
  
  // Process data history ...
  ProcessHistoryOfQueryData();
  
  // Get number of raw data entries
  if (GetNumberOfRawDataEntries() == ERROR)
  {
    return ERROR;
  }

  // Load Long Short of last trade date
  if (LoadLongShortFromDatabase() == ERROR)
  {
    return ERROR;
  }

  // Get number of output log entries
  if (LoadTradeOutputLog() == ERROR)
  {
    return ERROR;
  }

  // Get open order from database
  if (LoadASOpenOrderFromDatabase() == ERROR)
  {
    return ERROR;
  }

  // Load stuck from database that wasn't exit
  if (LoadStuckFromDatabase(globalDateStringOfDatabase) == ERROR)
  {
    return ERROR;
  }
  
  // Load trade loss from database
  if (LoadTradeLossFromDatabase(globalDateStringOfDatabase) == ERROR)
  {
    return ERROR;
  }

  // Load log measurement info from database
  if (LoadLogMeasurementFromDatabase() == ERROR)
  {
    TraceLog(ERROR_LEVEL, "(Time series: Can not load measurement data from database)\n");
    return ERROR;
  }
  
  // Load halted stock list
  if (LoadHaltedStockListFromFile() == ERROR)
  {
    return ERROR;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadASOpenOrderFromDatabase
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int LoadASOpenOrderFromDatabase(void)
{
  char currentDateString[MAX_TIMESTAMP] = "\0";

  // Get date string
  strcpy(currentDateString, globalDateStringOfDatabase);

  UpdateHungOrderStatusToDatabase();
  
  // Get new order
  if (GetRawDataFromNewOrder(currentDateString) == ERROR)
  {
    return ERROR;
  }

  // Get order ack
  if (GetRawDataFromNonExecuteTable(currentDateString, "order_acks", ORDER_EDITOR_NONE) == ERROR)
  {
    return ERROR;
  }

  // Get order reject
  if (GetRawDataFromNonExecuteTable(currentDateString, "order_rejects", ORDER_EDITOR_NONE) == ERROR)
  {
    return ERROR;
  } 

  // Get fill
  if (GetRawDataFromExecuteTable(currentDateString, "fills", ORDER_EDITOR_NONE) == ERROR)
  {
    return ERROR;
  } 

  // Get cancel request
  if (GetRawDataFromNonExecuteTable(currentDateString, "cancel_requests", ORDER_EDITOR_NONE) == ERROR)
  {
    return ERROR;
  }

  // Get cancel ack
  if (GetRawDataFromNonExecuteTable(currentDateString, "cancel_acks", ORDER_EDITOR_NONE) == ERROR)
  {
    return ERROR;
  }

  // Get cancel reject
  if (GetRawDataFromNonExecuteTable(currentDateString, "cancel_rejects", ORDER_EDITOR_NONE) == ERROR)
  {
    return ERROR;
  }

  // Get cancel pending
  if (GetRawDataFromExecuteTable(currentDateString, "cancel_pendings", ORDER_EDITOR_NONE) == ERROR)
  {
    return ERROR;
  }

  // Get cancel
  if (GetRawDataFromNonExecuteTable(currentDateString, "cancels", ORDER_EDITOR_NONE) == ERROR)
  {
    return ERROR;
  }

  // Get broken trade message
  if (GetRawDataFromNonExecuteTable(currentDateString, "broken_trades", ORDER_EDITOR_NONE) == ERROR)
  {
    return ERROR;
  }

  // Get broken trade
  if (GetBrokenTradeData(currentDateString) == ERROR)
  {
    return ERROR;
  }

  // Get open positions
  if (GetOpenPositions(currentDateString, 1) == ERROR)
  {
    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  InitializeDataConfs
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int InitializeDataConfs(void)
{
  // Initialize Trade Servers information
  InitializeTradeServersInfo();

  // Initialize PM information
  InitializePMInfo();

  // Initialize Trader Tools information
  InitializeTraderToolsInfo();
  
  InitializeSessionIdManagement();

  // Initialize ignore stock list
  InitializeIgnoreStockList();
  
  //Initialize ignore stock ranges list
  InitializeIgnoreStockRangesList();
  
  //Initialize manual order mapping
  InitializeManualOrderAccountMapping();
  
  // Initialize order status
  InitializeOrderStatus();

  // Initialize order status index
  InitializeOrderStatusIndex();

  // Initialize order status index
  InitializeFieldIndexList();

  // Initialize database mutex
  InitializeDatabaseMutex();
  
  // Initialize FastMarketManagement structure
  InitializeFastMarketMgmtStructure();
  
  InitializeTraderStatus();
  
  InitializeDecodeTable();
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  InitializeDataStructures
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int InitializeDataStructures(void)
{
  // Initialize symbol index list
  InitializeSymbolIndexList();

  // Initilize order id index
  InitilizeOrderIdIndexList();

  // Initialize Volatile Symbol List
  InitializeVolatileSymbolList();

  // Initialize risk management structure
  InitializeRiskManagementStructure();

  // Initialize profit loss structure
  InitializeProfitLossPerSymbolStructure();

  // Initialize long short structure
  InitializeLongShortStructure();
  
  // Initialize verify position info 
  InitializeVerifyPositionInfoStructure();
  
  InitExchangeOrderIdMapping();
  
  // Initialize manual order info
  InitializeManualOrderInfoStructure();
  
  // Initialize manual order info
  InitializePMOrderInfoStructure();

  // Initialize raw data management structures
  InitializeRawDataManegementStructures();

  // Initialize Rawdata wait-list structure
  InitializeRawdataWaitList();
  
  // Initialize hung order structure
  InitializeHungOrderList();
  
  // Initialize query data management structures
  InitializeQueryDataManegementStructures();

  // Initialize output log manegement structures
  InitializeOutputLogManegementStructures();
  
  //Initialize BBO management structures
  InitializeBBOManagementStructure();

  // Initialize AS open order structures
  InitializeASOpenOrderStructures();
  
  // Initialize NYSE CCG cancel request mapping
  Initialize_NYSE_Order_ID_Mapping();

  // Initialize broken trade
  InitializeBrokenTrade();
  
  // Initialize check points
  InitializeTradeOutputCheckPoint();

  if (InitOrderFilesCreation() == ERROR)
  {
    return ERROR;
  }
  
  if (InitISOFilesCreation() == ERROR)
  {
    return ERROR;
  }
  
  // Initialize time series format EZG
  if (InitTimeSeriesFormatEZG() == ERROR)
  {
    return ERROR;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  RegisterSignalHandlers
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int RegisterSignalHandlers(void)
{
  TraceLog(DEBUG_LEVEL, "Register signal handlers\n");

  // Register SIGPIPE signal
  // signal(SIGPIPE, Process_SIGPIPE_Signal);
  struct sigaction actPipe;
  memset (&actPipe, 0, sizeof(actPipe));
  actPipe.sa_sigaction = &Process_SIGPIPE_Signal;
  sigfillset(&actPipe.sa_mask);
  actPipe.sa_flags = SA_SIGINFO;
  if (sigaction(SIGPIPE, &actPipe, NULL) < 0)
  {
    PrintErrStr(ERROR_LEVEL, "Can not register signal handler for SIGPIPE: ", errno);
  }
  
  // Register SIGINT signal
  // signal(SIGINT, Process_INTERRUPT_Signal);
  struct sigaction actInt;
  memset (&actInt, 0, sizeof(actInt));
  actInt.sa_sigaction = &Process_INTERRUPT_Signal;
  sigfillset(&actInt.sa_mask);
  actInt.sa_flags = SA_SIGINFO;
  if (sigaction(SIGINT, &actInt, NULL) < 0)
  {
    PrintErrStr(ERROR_LEVEL, "Can not register signal handler for SIGINT: ", errno);
  }
  
  // Register SIGSEGV signal
  // signal(SIGSEGV, Process_SEGV_Signal);
  struct sigaction actSegv;
  memset (&actSegv, 0, sizeof(actSegv));
  actSegv.sa_sigaction = &Process_SEGV_Signal;
  sigfillset(&actSegv.sa_mask);
  actSegv.sa_flags = SA_SIGINFO;
  if (sigaction(SIGSEGV, &actSegv, NULL) < 0)
  {
    PrintErrStr(ERROR_LEVEL, "Can not register signal handler for SIGSEGV: ", errno);
  }
  
  //sigaction does not apply for SIGKILL and SIGSTOP
  // Register SIGKILL signal
  signal(SIGKILL, Process_Kill_Signal);
  
  // Register SIGUSR1 signal
  // signal(SIGUSR1, Process_USER_DEFINED_Signal_1);
  struct sigaction actUsr1;
  memset (&actUsr1, 0, sizeof(actUsr1));
  actUsr1.sa_sigaction = &Process_USER_DEFINED_Signal_1;
  sigfillset(&actUsr1.sa_mask);
  actUsr1.sa_flags = SA_SIGINFO;
  if (sigaction(SIGUSR1, &actUsr1, NULL) < 0)
  {
    PrintErrStr(ERROR_LEVEL, "Can not register signal handler for SIGUSR1: ", errno);
  }
  
  return 0;
}

/****************************************************************************
- Function name:  Process_SISPIPE_Signal
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
void Process_SIGPIPE_Signal(int signalno, siginfo_t *siginfo, void *context)
{
  TraceLog(DEBUG_LEVEL, "SIGPIPE signal is catched and ignored.\n");

  // Save Sequence Number
  SaveARCA_DIRECT_SeqNumToFile();
  SaveNASDAQ_RASH_SeqNumToFile();

  // signal(SIGPIPE, Process_SIGPIPE_Signal);
}

/****************************************************************************
- Function name:  Process_INTERRUPT_Signal
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
void Process_INTERRUPT_Signal(int signalno, siginfo_t *siginfo, void *context)
{
  TraceLog(DEBUG_LEVEL, "Processing Interrupt Signal \n");

  // signal(SIGINT, Process_INTERRUPT_Signal);
  
  // Save Sequence Number
  SaveARCA_DIRECT_SeqNumToFile();
  SaveNASDAQ_RASH_SeqNumToFile();
  
  // Destroy database connection
  DestroyConnection(connRawData);
  DestroyConnection(connSaveSettings);
  DestroyConnection(connQueryData);
  DestroyConnection(connTimeSeries);
  if (connPrevMonthDatabase != NULL)
  {
    DestroyConnection(connPrevMonthDatabase);
  }
  exit(0);
}

/****************************************************************************
- Function name:  Process_SEGV_Signal
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
void Process_SEGV_Signal(int signalno, siginfo_t *siginfo, void *context)
{
  TraceLog(DEBUG_LEVEL, "Processing Segmentation Fault Signal\n");

  // Flush all error
  fflush(stderr);

  // Save position raw data processed to file
  SavePositionRawDataProcessedToFile();

  // Save position output log processed to file
  SavePositionOutputLogProcessedToFile();
  
  //signal(SIGSEGV, Process_SEGV_Signal);
  
  // Save Sequence Number
  SaveARCA_DIRECT_SeqNumToFile();
  SaveNASDAQ_RASH_SeqNumToFile();
  
  // Destroy database connection
  DestroyConnection(connRawData);
  DestroyConnection(connSaveSettings);
  DestroyConnection(connQueryData);
  DestroyConnection(connTimeSeries);
  if (connPrevMonthDatabase != NULL)
  {
    DestroyConnection(connPrevMonthDatabase);
  }

  exit(0);
}

/****************************************************************************
- Function name:  Process_Kill_Signal
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
void Process_Kill_Signal(int sig_num)
{
  TraceLog(DEBUG_LEVEL, "Processing SegFault Signal by SIGKILL\n");

  // Flush all error
  fflush(stderr);

  // Save position raw data processed to file
  SavePositionRawDataProcessedToFile();

  // Save position output log processed to file
  SavePositionOutputLogProcessedToFile();
  
  // Save Sequence Number
  SaveARCA_DIRECT_SeqNumToFile();
  SaveNASDAQ_RASH_SeqNumToFile();
  
  // Destroy database connection
  DestroyConnection(connRawData);
  DestroyConnection(connSaveSettings);
  DestroyConnection(connQueryData);
  DestroyConnection(connTimeSeries);
  if (connPrevMonthDatabase != NULL)
  {
    DestroyConnection(connPrevMonthDatabase);
  }

  exit(0);
}

/****************************************************************************
- Function name:  Process_USER_DEFINED_Signal_1
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
void Process_USER_DEFINED_Signal_1(int signalno, siginfo_t *siginfo, void *context)
{
  TraceLog(DEBUG_LEVEL, "Received User-define Signal 1\n");
  TraceLog(DEBUG_LEVEL, "Processing disable trading all TS and PM...\n");
  
  // Call function to disable trading on all TS
  if (SendDisableTradingAllOrderToAllTS(TS_TRADING_DISABLED_BY_WEDBUSH_REQUEST) != SUCCESS)
  {
    TraceLog(WARN_LEVEL, "Request to disable trading on TS from WedBush unsuccessfully\n");
  }

  // Call function to disable trading on PM
  if (SendDisableTradingPMToPM(PM_TRADING_DISABLED_BY_WEDBUSH_REQUEST) != SUCCESS)
  {
    TraceLog(WARN_LEVEL, "Request to disable trading on PM from WedBush unsuccessfully\n");
  }
  
  TraceLog(DEBUG_LEVEL, "Successfully disable trading for TS and PM from WedBush\n");
  
  // Register SIGUSR1 is handled by Process_USER_DEFINED_Signal_1 again
  // signal(SIGUSR1, Process_USER_DEFINED_Signal_1);
}

/****************************************************************************
- Function name:  ProcessAutoStartThreads
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
void *ProcessAutoStartThreads(void *arg)
{
  time_t now;
  struct tm *timeNow;
  int numSeconds, i;

  while (1)
  {
    if (isEnableStart == 0)
    {
      continue;
    }
    
    //Get local Time    
    now = (time_t)hbitime_seconds();
    
    timeNow = (struct tm *)localtime(&now);
    
    numSeconds = timeNow->tm_hour * 60 + timeNow->tm_min;

    // Check and start threads ...
    for (i = 0; i < MAX_TRADE_SERVER_CONNECTIONS; i++)
    {
      if ((numSecondsAutoStartTS[i].value != -1) && (numSecondsAutoStartTS[i].isProcess == -1))
      {
        if (numSeconds >= numSecondsAutoStartTS[i].value)
        {
          TraceLog(DEBUG_LEVEL, "AUTO: AS begin connect to %s ...\n", tradeServersInfo.config[i].description);

          numSecondsAutoStartTS[i].isProcess = 0;

          // Call function to connect to TS
          Connect2TradeServer(i);
        }
      }
    }

    for (i = 0; i < AS_MAX_ORDER_CONNECTIONS; i++)
    {
      if ((numSecondsAutoStartECNOrders[i].value != -1) && (numSecondsAutoStartECNOrders[i].isProcess == -1))
      {
        if (numSeconds >= numSecondsAutoStartECNOrders[i].value)
        {
          TraceLog(DEBUG_LEVEL, "AUTO: AS begin connect to ECN Order, ECNId = %d ...\n", i);

          numSecondsAutoStartECNOrders[i].isProcess = 0;

          // Call function to AS connect to ECN Order
          ProcessASConnectToOrderECN(i);
        }
      }
    }
    
    if ((numSecondsAutoStartPM.value != -1) && (numSecondsAutoStartPM.isProcess == -1))
    {
      if (numSeconds >= numSecondsAutoStartPM.value)
      {
        TraceLog(DEBUG_LEVEL, "AUTO: AS begin connect to PM ...\n");

        numSecondsAutoStartPM.isProcess = 0;

        // Call function to connect to TS
        Connect2PM();
      }
    }

    // Sleep in second(s)
    sleep(30);
  }

  return NULL;
}

/****************************************************************************
- Function name:  StartUpMainThreads
- Input:      + argc
          + argv
- Output:     N/A
- Return:     Success or Failure
- Description:    
- Usage:      The routine will be called by runtime environment
****************************************************************************/
int StartUpMainThreads(void)
{ 
  // Initialize data structures
  InitializeDataStructures();
  
  //Get Current Date
  GetCurrentDateString(dateString);
  
  // Load Break message in OJ
  if (LoadBreakMsgInOJ() == ERROR)
  {
    return ERROR;
  }

  // Load data into data structures
  if (LoadDataStructures() == ERROR)
  {
    return ERROR;
  }
  
  // Load loss by symbol from file
  if (LoadLossBySymbolFromfile() == ERROR)
  {
    return ERROR;
  }

  //Get manual orderID and username who placed manual order, for OJ record creation
  GetManualOrderInfoFromDatabase(globalDateStringOfDatabase);
  
  //Load broken ISO orders, since when AS is crashed, and restarted,
  //there might be some ISO orders were already in database but not in memory,
  //so ISO Snapshot file might be missing some ISO orders, we need to load these missing orders into memory
  if (LoadBrokenRawdataForISOOrderFromDatabase(globalDateStringOfDatabase) == ERROR)
  {
    return ERROR;
  }
  
  //Create OJ record for open positions
  CreateOJRecordForOpenPosition();
  
  // Update modify long/short status
  UpdateModifyLongShortStatusToDatabase();
  
  // Process history output log
  ProcessHistoryOutputLog();
  
  // Process history raw data
  ProcessHistoryRawData();  
  
  // Process history bbo info
  ProcessHistoryBBOInfo();
  
  // Reset FastMarketMgmt values, since it's affected by History rawdata!
  ResetFastMarketMgmtValues();
  
  UpdateVerifyPositionInfo();
  
  ProcessQueryDatabaseFromCollection(&queryDataMgmt.positionProcessed, &queryDataMgmt);
  
  // ---------------------------
  //    Create threads ...
  // ---------------------------
  
  // Create thread to process output log
  pthread_t threadOutputLogManagement_id;
  pthread_create (&threadOutputLogManagement_id, 
          NULL, 
          (void*) &ProcessOutputLogManagement, 
          NULL);
  
  // Create thread to process raw data
  pthread_t threadRawDataManagement_id;
  pthread_create (&threadRawDataManagement_id, 
          NULL, 
          (void*) &ProcessRawDataManagement, 
          NULL);

  // Create thread to talk to Daedalus
  pthread_t threadDaedalusManagement_id;
  pthread_create (&threadDaedalusManagement_id, 
          NULL, 
          (void*) &ProcessDaedalus, 
          NULL);
          
  pthread_t threadCurrentStatusToDaedalus_id;
  pthread_create (&threadCurrentStatusToDaedalus_id, 
          NULL, 
          (void*) &SendCurrentStatusToDaedalus, 
          NULL);
  
  // Create thread to process query database
  pthread_t threadQueryDatabaseManagement_id;
  pthread_create (&threadQueryDatabaseManagement_id, 
          NULL, 
          (void*) &ProcessQueryDatabaseManagement, 
          NULL);
          
  // Create thread to export Time Series Format - EZG
  pthread_t threadExportTimeSeriesFormat_id;
  pthread_create (&threadExportTimeSeriesFormat_id, 
          NULL, 
          (void*) &ExportTimeSeriesFormat, 
          NULL);

  // Create timer thread
  pthread_t threadTimer_id;
  pthread_create (&threadTimer_id,
          NULL,
          (void*) &TimerThread,
          NULL);
  //  Sleep in 5 seconds
  sleep(5);
  
  // Update isEnableStart
  isEnableStart = 1;
  
  // Wait for threads completion  
  pthread_join (threadOutputLogManagement_id, NULL);
  pthread_join (threadRawDataManagement_id, NULL);
  pthread_join (threadDaedalusManagement_id, NULL);
  pthread_join (threadQueryDatabaseManagement_id, NULL);
  pthread_join (threadExportTimeSeriesFormat_id, NULL);
  pthread_join (threadTimer_id, NULL);

  return 0;
}

/***************************************************************************/

/****************************************************************************
- Function name:  SaveLoginInfo
- Input:      + 
          + 
- Output:     N/A
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SaveLoginInfo()
{
  //Initialize version
  #ifdef VERSION
    TraceLog(DEBUG_LEVEL, "Current version: %s\n", VERSION);
  #else
    TraceLog(DEBUG_LEVEL, "Current version: UNKNOWN\n");
  #endif
    
  int hour, min, sec, date, year, month;
  time_t now = (time_t)hbitime_seconds();
  struct tm *timeNow = (struct tm *) localtime(&now); 
  
  char fullPath[MAX_PATH_LEN] = "\0";
  Environment_get_data_filename("version_log.txt", fullPath, MAX_PATH_LEN);
  int file_desc;
  
  struct flock region_to_lock;
  int res;
  char byte_write[80];
  memset(byte_write, 0, 80);
  
  //Get time when AS started
  hour  = timeNow->tm_hour;
  min   = timeNow->tm_min;
  sec   = timeNow->tm_sec;
  date  = timeNow->tm_mday;
  month   = 1 + timeNow->tm_mon;
  year  = 1900 + timeNow->tm_year;

  //Initialize locking file
  region_to_lock.l_type = F_WRLCK; //Exclusive lock
  region_to_lock.l_whence = SEEK_SET;//Lock from the beginning of the file
  file_desc = open(fullPath, O_RDWR | O_CREAT | O_APPEND, 0666);
  if (!file_desc)
  {
    TraceLog(ERROR_LEVEL, "Cannot open file '%s' to read or we don't have permission to create\n", fullPath);
    return -1;
  }
  res = fcntl(file_desc, F_SETLKW, &region_to_lock);
  if (res == -1) 
  {
    fprintf(stderr, "Failed to lock region \n");
  }
  sprintf(byte_write,"%d-%.2d-%.2d %.2d:%.2d:%.2d - AS \t VERSION: %s \n", year, month, date, hour, min, sec, VERSION);
  write(file_desc, byte_write, strlen(byte_write));
  close(file_desc);
  
  return 0;
}
