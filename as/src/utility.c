/*****************************************************************************
**  Project:    Equity Arbitrage Application
**  Filename:   utility.c
**  Description:  This file contains function definitions that were declared
          in utility.h  
**  Author:     Luan Vo-Kinh
**  First created:  17-Sep-2007
**  Last updated: -----------
*****************************************************************************/

/****************************************************************************
** Include files and define several variables
****************************************************************************/
#define _GNU_SOURCE

#include <string.h>
#include <sys/wait.h>
#include <stdarg.h>

#include "environment_proc.h"
#include "hbitime.h"
#include "parameters_proc.h"
#include "utility.h"

int Secs_in_years = 0;
int Current_year = 0;

char globalDateStringOfData[MAX_LINE_LEN];
char globalDateStringOfDatabase[MAX_LINE_LEN];
/****************************************************************************
** Function definitions
****************************************************************************/

/****************************************************************************
- Function name:  GetFullConfigPath
- Input:      + fileName
        + pathType
- Output:   + fullConfigPath
- Return:   NULL or pointer to fullConfigPath
- Description:  + The routine hides the following facts
          - Create full configuration path to file name path type based
        + There are preconditions guaranteed to the routine
          - fullConfigPath must be allocated before   
        + The routine guarantees that the status value will 
          have a value of either
          - Pointer to full configuraton path
          or
          - Failure
- Usage:      N/A
****************************************************************************/
char *GetFullConfigPath(const char *fileName, int pathType, char *fullConfigPath)
{
  if (fullConfigPath == NULL)
  {
    return NULL;
  }
  
  /* 
  Base on path type, fullConfigPath will be
  updated appropriately
  */ 
  switch (pathType)
  {
    case CONF_ORDER:
      return Environment_get_order_conf_filename(fileName, fullConfigPath, MAX_PATH_LEN);
    case CONF_SYMBOL:
      return Environment_get_symbol_conf_filename(fileName, fullConfigPath, MAX_PATH_LEN);
    case CONF_MISC:
      return Environment_get_misc_conf_filename(fileName, fullConfigPath, MAX_PATH_LEN);
    default:
      return NULL;
  }

  //TraceLog(DEBUG_LEVEL, "full name = %s\n", fullConfigPath);
  
  return fullConfigPath;
}

/****************************************************************************
- Function name:  GetFullRawDataPath
- Input:      + fileName
- Output:   + fullConfigPath
- Return:   NULL or pointer to fullConfigPath
- Description:  + The routine hides the following facts
          - Create full raw data path to file name
        + There are preconditions guaranteed to the routine
          - fullConfigPath must be allocated and enough to store data path  
        + The routine guarantees that the status value will 
          have a value of either
          - Pointer to full raw data path
          or
          - Failure
- Usage:      N/A
****************************************************************************/
char *GetFullRawDataPath(const char *fileName, char *fullDataPath)
{
  // Build full raw data path
  return Environment_get_data_filename(fileName, fullDataPath, MAX_PATH_LEN);
}

/****************************************************************************
- Function name:  CreateDataPathCurrentDate
- Input:      N/A
- Output:   N/A
- Return:   Success or Failure
- Description:  + The routine hides the following facts
          - Create full configuration path to file name path type based
        + There are preconditions guaranteed to the routine
          - fullConfigPath must be allocated and enough to store data path  
        + The routine guarantees that the status value will 
          have a value of either
          - Pointer to full raw data path
          or
          - Failure
- Usage:      N/A
****************************************************************************/
int CreateDataPathCurrentDate(void)
{
  char fullDataPath[MAX_PATH_LEN];
  
  // Get current time
  time_t currentTime = (time_t)hbitime_seconds();
  
  struct tm *currentLocalTime = (struct tm *)localtime(&currentTime);
  
  if (currentLocalTime == NULL)
  {
    return ERROR;
  }

  // Get globalDateStringOfData
  sprintf(globalDateStringOfData, "%d/%02d/%02d", currentLocalTime->tm_year + 1900, currentLocalTime->tm_mon + 1, currentLocalTime->tm_mday);
  
  // Update globalDateStringOfDatabase
  strcpy(globalDateStringOfDatabase, globalDateStringOfData);

  globalDateStringOfDatabase[4] = '-';
  globalDateStringOfDatabase[7] = '-';  

  // Build full data path
  Environment_get_root_data_dir(fullDataPath, MAX_PATH_LEN);
  
  // Command to create directories
  char command[MAX_PATH_LEN];
  sprintf(command, "mkdir -p %s", fullDataPath);
  
  // Create directory
  system(command);

  TraceLog(DEBUG_LEVEL, "fullDataPath = %s\n", fullDataPath);

  return SUCCESS;
}

/****************************************************************************
- Function name:  CreateDataPathAtDate
- Input:      N/A
- Output:   N/A
- Return:   Success or Failure
- Description:  + The routine hides the following facts
          - Create full configuration path to file name path type based
        + There are preconditions guaranteed to the routine
          - fullConfigPath must be allocated and enough to store data path  
        + The routine guarantees that the status value will 
          have a value of either
          - Pointer to full raw data path
          or
          - Failure
- Usage:      N/A
****************************************************************************/
int CreateDataPathAtDate(int numSeconds)
{
  char fullDataPath[MAX_PATH_LEN];  
  
  //GetDateString
  GetDateStringFormEpochSecs(numSeconds, globalDateStringOfData);

  // Update globalDateStringOfDatabase
  strcpy(globalDateStringOfDatabase, globalDateStringOfData);

  globalDateStringOfDatabase[4] = '-';
  globalDateStringOfDatabase[7] = '-';
  
  // Build full data path
  Environment_get_root_data_dir(fullDataPath, MAX_PATH_LEN);
  
  // Command to create directories
  char command[MAX_PATH_LEN];
  sprintf(command, "mkdir -p %s", fullDataPath);
  
  // Create directory
  system(command);

  TraceLog(DEBUG_LEVEL, "fullDataPath = %s\n", fullDataPath);

  return SUCCESS;
}

/****************************************************************************
- Function name:  Lrc_is_leap
- Input:      [To be done]
- Output:   [To be done]
- Return:   [To be done]
- Description:  + The routine hides the following facts
        + There are no preconditions guaranteed to the routine
- Usage:      N/A
****************************************************************************/
inline int Lrc_is_leap(int year)
{
  year += 1900; /* Get year, as ordinary humans know it */

  /*
   *   The rules for leap years are not
   *   as simple as "every fourth year
   *   is leap year":
   */

  if( (unsigned int)year % 100 == 0 ) {
    return (unsigned int)year % 400 == 0;
  }

  return (unsigned int)year % 4 == 0;
}

/****************************************************************************
- Function name:  GetByteNumber
- Input:      + buffer
- Output:   N/A
- Return:   Integer number
- Description:  + The routine hides the following facts
          - Convert 2 bytes in buffer to short value
        + There are no preconditions guaranteed to the routine
- Usage:      N/A
****************************************************************************/
inline char GetByteNumber(const unsigned char *buffer)
{
  return buffer[0];
}
/****************************************************************************
- Function name:  GetByteNumberV1
- Input:      + buffer
- Output:   N/A
- Return:   Integer number
- Description:  + The routine hides the following facts
          - Convert 2 bytes in buffer to short value
        + There are no preconditions guaranteed to the routine
- Usage:      N/A
****************************************************************************/
inline char GetByteNumberV1(const unsigned char *buffer, int *index)
{
  *index += 1;

  return buffer[0];
}
/****************************************************************************
- Function name:  PutByteNumberV1
- Input:      + buffer
- Output:   N/A
- Return:   Integer number
- Description:  + The routine hides the following facts
          - Convert 2 bytes in buffer to short value
        + There are no preconditions guaranteed to the routine
- Usage:      N/A
****************************************************************************/
inline int PutByteNumberV1(const unsigned char num, unsigned char *buffer, int *index)
{
  *index += 1;

  buffer[0] = num;
  return 1;
}

/****************************************************************************
- Function name:  GetByteNumberBigEndianV1
- Input:      + buffer
- Output:   N/A
- Return:   Integer number
- Description:  + The routine hides the following facts
          - Convert 2 bytes in buffer to short value
        + There are no preconditions guaranteed to the routine
- Usage:      N/A
****************************************************************************/
inline char GetByteNumberBigEndianV1(const unsigned char *buffer, int *index)
{
  *index += 1;

  return buffer[0];
}
/****************************************************************************
- Function name:  PutByteNumberBigEndianV1
- Input:      + buffer
- Output:   N/A
- Return:   Integer number
- Description:  + The routine hides the following facts
          - Convert 2 bytes in buffer to short value
        + There are no preconditions guaranteed to the routine
- Usage:      N/A
****************************************************************************/
inline int PutByteNumberBigEndianV1(const unsigned char num, unsigned char *buffer, int *index)
{
  *index += 1;

  buffer[0] = num;
  return 1;
}

/****************************************************************************
- Function name:  GetShortNumber
- Input:      + buffer
- Output:   N/A
- Return:   Integer number
- Description:  + The routine hides the following facts
          - Convert 2 bytes in buffer to short value
        + There are no preconditions guaranteed to the routine
- Usage:      N/A
****************************************************************************/
inline int GetShortNumber(const unsigned char *buffer)
{
  t_ShortConverter shortConverter;
  memcpy(shortConverter.c, buffer, 2);

  return shortConverter.value;
}
/****************************************************************************
- Function name:  GetShortNumberV1
- Input:      + buffer
- Output:   N/A
- Return:   Integer number
- Description:  + The routine hides the following facts
          - Convert 2 bytes in buffer to short value
        + There are no preconditions guaranteed to the routine
- Usage:      N/A
****************************************************************************/
inline int GetShortNumberV1(const unsigned char *buffer, int *index)
{
  t_ShortConverter shortConverter;
  memcpy(shortConverter.c, buffer, 2);

  *index += 2;

  return shortConverter.value;
}

/****************************************************************************
- Function name:  PutShortNumberV1
- Input:      + buffer
- Output:   N/A
- Return:   Integer number
- Description:  + The routine hides the following facts
          - Convert 2 bytes in buffer to short value
        + There are no preconditions guaranteed to the routine
- Usage:      N/A
****************************************************************************/
inline int PutShortNumberV1(const short num, unsigned char *buffer, int *index)
{
  memcpy(buffer, &num, 2);

  *index += 2;

  return 2;
}

/****************************************************************************
- Function name:  GetShortNumberBigEndian
- Input:      + buffer
- Output:   N/A
- Return:   Integer number
- Description:  + The routine hides the following facts
          - Convert 2 bytes in buffer to short value
        + There are no preconditions guaranteed to the routine
- Usage:      N/A
****************************************************************************/
inline int GetShortNumberBigEndian(const unsigned char *buffer)
{
  t_ShortConverter converter;

  converter.c[1] = buffer[0];
  converter.c[0] = buffer[1];

  return converter.value;
}

/****************************************************************************
- Function name:  GetShortNumberBigEndianV1
- Input:      + buffer
- Output:   N/A
- Return:   Integer number
- Description:  + The routine hides the following facts
          - Convert 2 bytes in buffer to short value
        + There are no preconditions guaranteed to the routine
- Usage:      N/A
****************************************************************************/
inline int GetShortNumberBigEndianV1(const unsigned char *buffer, int *index)
{
  t_ShortConverter converter;

  converter.c[1] = buffer[0];
  converter.c[0] = buffer[1];

  *index += 2;

  return converter.value;
}

/****************************************************************************
- Function name:  PutShortNumberBigEndianV1
- Input:      + buffer
- Output:   N/A
- Return:   Integer number
- Description:  + The routine hides the following facts
          - Convert 2 bytes in buffer to short value
        + There are no preconditions guaranteed to the routine
- Usage:      N/A
****************************************************************************/
inline int PutShortNumberBigEndianV1(const short num, unsigned char *buffer, int *index)
{
  t_ShortConverter converter;

  converter.value = num;

  buffer[0] = converter.c[1];
  buffer[1] = converter.c[0];

  *index += 2;

  return 2;
}

/****************************************************************************
- Function name:  GetIntNumber
- Input:      + buffer
- Output:   N/A
- Return:   Integer number
- Description:  + The routine hides the following facts
          - Convert 4 bytes in buffer to int value
        + There are no preconditions guaranteed to the routine
- Usage:      N/A
****************************************************************************/
inline int GetIntNumber(const unsigned char *buffer)
{
  t_IntConverter intConverter;
  memcpy(intConverter.c, buffer, 4);

  return intConverter.value;
}

/****************************************************************************
- Function name:  GetIntNumberV1
- Input:      + buffer
- Output:   N/A
- Return:   Integer number
- Description:  + The routine hides the following facts
          - Convert 4 bytes in buffer to int value
        + There are no preconditions guaranteed to the routine
- Usage:      N/A
****************************************************************************/
inline int GetIntNumberV1(const unsigned char *buffer, int *index)
{
  t_IntConverter intConverter;
  memcpy(intConverter.c, buffer, 4);

  *index += 4;

  return intConverter.value;
}

/****************************************************************************
- Function name:  PutIntNumberV1
- Input:      + buffer
- Output:   N/A
- Return:   Integer number
- Description:  + The routine hides the following facts
          - Convert 4 bytes in buffer to int value
        + There are no preconditions guaranteed to the routine
- Usage:      N/A
****************************************************************************/
inline int PutIntNumberV1(const int num, unsigned char *buffer, int *index)
{
  memcpy(buffer, &num, 4);

  *index += 4;

  return 4;
}

/****************************************************************************
- Function name:  GetIntNumberBigEndian
- Input:      + buffer
- Output:   N/A
- Return:   Integer number
- Description:  + The routine hides the following facts
          - Convert 4 bytes in buffer to short value
        + There are no preconditions guaranteed to the routine
- Usage:      N/A
****************************************************************************/
inline int GetIntNumberBigEndian(const unsigned char *buffer)
{
  t_IntConverter converter;
  converter.c[3] = buffer[0];
  converter.c[2] = buffer[1];
  converter.c[1] = buffer[2];
  converter.c[0] = buffer[3];

  return converter.value;
}
/****************************************************************************
- Function name:  GetIntNumberBigEndianV1
- Input:      + buffer
- Output:   N/A
- Return:   Integer number
- Description:  + The routine hides the following facts
          - Convert 4 bytes in buffer to short value
        + There are no preconditions guaranteed to the routine
- Usage:      N/A
****************************************************************************/
inline int GetIntNumberBigEndianV1(const unsigned char *buffer, int *index)
{
  t_IntConverter converter;
  converter.c[3] = buffer[0];
  converter.c[2] = buffer[1];
  converter.c[1] = buffer[2];
  converter.c[0] = buffer[3];

  *index += 4;

  return converter.value;
}

/****************************************************************************
- Function name:  PutIntNumberBigEndianV1
- Input:      + buffer
- Output:   N/A
- Return:   Integer number
- Description:  + The routine hides the following facts
          - Convert 4 bytes in buffer to short value
        + There are no preconditions guaranteed to the routine
- Usage:      N/A
****************************************************************************/
inline int PutIntNumberBigEndianV1(const int num, unsigned char *buffer, int *index)
{
  t_IntConverter converter;

  converter.value = num;

  buffer[0] = converter.c[3];
  buffer[1] = converter.c[2];
  buffer[2] = converter.c[1];
  buffer[3] = converter.c[0];

  *index += 4;

  return 4;
}
/****************************************************************************
- Function name:  GetLongNumber
- Input:      + buffer
- Output:   N/A
- Return:   Long number
- Description:  + The routine hides the following facts
          - Convert 8 bytes in buffer to long value
        + There are no preconditions guaranteed to the routine
- Usage:      N/A
****************************************************************************/
inline long GetLongNumber(const char *buffer)
{
  t_LongConverter converter;

  memcpy(converter.c, buffer, 8);

  return converter.value;
}
/****************************************************************************
- Function name:  GetLongNumberV1
- Input:      + buffer
- Output:   N/A
- Return:   Long number
- Description:  + The routine hides the following facts
          - Convert 8 bytes in buffer to long value
        + There are no preconditions guaranteed to the routine
- Usage:      N/A
****************************************************************************/
inline long GetLongNumberV1(const char *buffer, int *index)
{
  t_LongConverter converter;

  memcpy(converter.c, buffer, 8);

  *index += 8;

  return converter.value;
}

/****************************************************************************
- Function name:  PutLongNumberV1
- Input:      + buffer
- Output:   N/A
- Return:   Long number
- Description:  + The routine hides the following facts
          - Convert 8 bytes in buffer to long value
        + There are no preconditions guaranteed to the routine
- Usage:      N/A
****************************************************************************/
inline int PutLongNumberV1(const long num, unsigned char *buffer, int *index)
{
  memcpy(buffer, &num, 8);

  *index += 8;

  return 8;
}

/****************************************************************************
- Function name:  GetLongNumberBigEndian
- Input:      + buffer
- Output:   N/A
- Return:   Long number
- Description:  + The routine hides the following facts
          - Convert 8 bytes in buffer to long value
        + There are no preconditions guaranteed to the routine
- Usage:      N/A
****************************************************************************/
inline long GetLongNumberBigEndian(const char *buffer)
{
  t_LongConverter converter;

  converter.c[0] = buffer[7];
  converter.c[1] = buffer[6];
  converter.c[2] = buffer[5];
  converter.c[3] = buffer[4];
  converter.c[4] = buffer[3];
  converter.c[5] = buffer[2];
  converter.c[6] = buffer[1];
  converter.c[7] = buffer[0];

  return converter.value;
}
/****************************************************************************
- Function name:  GetLongNumberBigEndianV1
- Input:      + buffer
- Output:   N/A
- Return:   Long number
- Description:  + The routine hides the following facts
          - Convert 8 bytes in buffer to long value
        + There are no preconditions guaranteed to the routine
- Usage:      N/A
****************************************************************************/
inline long GetLongNumberBigEndianV1(const unsigned char *buffer, int *index)
{
  t_LongConverter converter;

  converter.c[0] = buffer[7];
  converter.c[1] = buffer[6];
  converter.c[2] = buffer[5];
  converter.c[3] = buffer[4];
  converter.c[4] = buffer[3];
  converter.c[5] = buffer[2];
  converter.c[6] = buffer[1];
  converter.c[7] = buffer[0];

  *index += 8;

  return converter.value;
}
/****************************************************************************
- Function name:  PutLongNumberBigEndianV1
- Input:      + buffer
- Output:   N/A
- Return:   Long number
- Description:  + The routine hides the following facts
          - Convert 8 bytes in buffer to long value
        + There are no preconditions guaranteed to the routine
- Usage:      N/A
****************************************************************************/
inline int PutLongNumberBigEndianV1(const long num, unsigned char *buffer, int *index)
{
  t_LongConverter converter;

  converter.value = num;

  buffer[7] = converter.c[0];
  buffer[6] = converter.c[1];
  buffer[5] = converter.c[2];
  buffer[4] = converter.c[3];
  buffer[3] = converter.c[4];
  buffer[2] = converter.c[5];
  buffer[1] = converter.c[6];
  buffer[0] = converter.c[7];

  *index += 8;

  return 8;
}

/****************************************************************************
- Function name:  GetStringV1
- Input:      + buffer
- Output:   N/A
- Return:   Long number
- Description:
- Usage:      N/A
****************************************************************************/
inline int GetStringV1(char *dst, const unsigned char *src, int len, int *index)
{
  memcpy(dst, src, len);

  *index += len;
  return len;
}

/****************************************************************************
- Function name:  GetStringVariablelenV1
- Input:      + buffer
- Output:   N/A
- Return:   Long number
- Description:
- Usage:      N/A
****************************************************************************/
inline int GetStringVariablelenV1(char *dst, const unsigned char *src, int *index)
{
  int len;
  memcpy(&len, src, 4);

  memcpy(dst, src + 4, len);

  *index += len + 4;
  return len + 4;
}

/****************************************************************************
- Function name:  PutStringV1
- Input:      + buffer
- Output:   N/A
- Return:   Long number
- Description:
- Usage:      N/A
****************************************************************************/
inline int PutStringV1(const char *src, unsigned char *dst, int len, int *index)
{
  memcpy(dst, src, len);

  *index += len;
  return len;
}

/****************************************************************************
- Function name:  PutStringVariableLenV1
- Input:      + buffer
- Output:   N/A
- Return:   Long number
- Description:
- Usage:      N/A
****************************************************************************/
inline int PutStringVariableLenV1(const char *src, unsigned char *dst, int *index)
{
  int len = strlen(src);
  memcpy(dst, &len, 4);

  memcpy(dst + 4, src, len);

  *index += len + 4;
  return len + 4;
}

/****************************************************************************
- Function name:  GetDoubleNumber
- Input:      + buffer
- Output:   N/A
- Return:   Integer number
- Description:  + The routine hides the following facts
          - Convert 8 bytes in buffer to double value
        + There are no preconditions guaranteed to the routine
- Usage:      N/A
****************************************************************************/
inline double GetDoubleNumber(const unsigned char *buffer)
{
  t_DoubleConverter doubleConverter;
  memcpy(doubleConverter.c, buffer, 8);

  return doubleConverter.value;
}
/****************************************************************************
- Function name:  GetDoubleNumberV1
- Input:      + buffer
- Output:   N/A
- Return:   Integer number
- Description:  + The routine hides the following facts
          - Convert 8 bytes in buffer to double value
        + There are no preconditions guaranteed to the routine
- Usage:      N/A
****************************************************************************/
inline double GetDoubleNumberV1(const unsigned char *buffer, int *index)
{
  t_DoubleConverter doubleConverter;
  memcpy(doubleConverter.c, buffer, 8);

  *index += 8;

  return doubleConverter.value;
}
/****************************************************************************
- Function name:  PutDoubleNumberV1
- Input:      + buffer
- Output:   N/A
- Return:   Integer number
- Description:  + The routine hides the following facts
          - Convert 8 bytes in buffer to double value
        + There are no preconditions guaranteed to the routine
- Usage:      N/A
****************************************************************************/
inline int PutDoubleNumberV1(const double num, unsigned char *buffer, int *index)
{
  memcpy(buffer, &num, 8);

  *index += 8;

  return 8;
}
/****************************************************************************
- Function name:  GetDoubleNumber
- Input:      + buffer
- Output:   N/A
- Return:   Integer number
- Description:  + The routine hides the following facts
          - Convert 8 bytes in buffer to double value
        + There are no preconditions guaranteed to the routine
- Usage:      N/A
****************************************************************************/
inline double GetDoubleNumberBigEndian(const unsigned char *buffer)
{
  t_DoubleConverter doubleConverter;

  doubleConverter.c[0] = buffer[7];
  doubleConverter.c[1] = buffer[6];
  doubleConverter.c[2] = buffer[5];
  doubleConverter.c[3] = buffer[4];
  doubleConverter.c[4] = buffer[3];
  doubleConverter.c[5] = buffer[2];
  doubleConverter.c[6] = buffer[1];
  doubleConverter.c[7] = buffer[0];

  return doubleConverter.value;
}
/****************************************************************************
- Function name:  GetDoubleNumberBigEndianV1
- Input:      + buffer
- Output:   N/A
- Return:   Integer number
- Description:  + The routine hides the following facts
          - Convert 8 bytes in buffer to double value
        + There are no preconditions guaranteed to the routine
- Usage:      N/A
****************************************************************************/
inline double GetDoubleNumberBigEndianV1(const unsigned char *buffer, int *index)
{
  t_DoubleConverter doubleConverter;

  doubleConverter.c[0] = buffer[7];
  doubleConverter.c[1] = buffer[6];
  doubleConverter.c[2] = buffer[5];
  doubleConverter.c[3] = buffer[4];
  doubleConverter.c[4] = buffer[3];
  doubleConverter.c[5] = buffer[2];
  doubleConverter.c[6] = buffer[1];
  doubleConverter.c[7] = buffer[0];

  *index += 8;

  return doubleConverter.value;
}
/****************************************************************************
- Function name:  PutDoubleNumberBigEndianV1
- Input:      + buffer
- Output:   N/A
- Return:   Integer number
- Description:  + The routine hides the following facts
          - Convert 8 bytes in buffer to double value
        + There are no preconditions guaranteed to the routine
- Usage:      N/A
****************************************************************************/
inline int PutDoubleNumberBigEndianV1(const double num,  unsigned char *buffer, int *index)
{
  t_DoubleConverter doubleConverter;

  doubleConverter.value = num;

  buffer[7] = doubleConverter.c[0];
  buffer[6] = doubleConverter.c[1];
  buffer[5] = doubleConverter.c[2];
  buffer[4] = doubleConverter.c[3];
  buffer[3] = doubleConverter.c[4];
  buffer[2] = doubleConverter.c[5];
  buffer[1] = doubleConverter.c[6];
  buffer[0] = doubleConverter.c[7];

  *index += 8;

  return 8;
}
/****************************************************************************
- Function name:  RemoveCharacters
- Input:      + buffer
        + bufferLen
- Output:   + buffer
- Return:   Buffer with removed used characters
- Description:  + The routine hides the following facts
          - Check byte by byte with ASCII code 32 (SPACE) and remove
          any characters less than or equal
        + There are preconditions guaranteed to the routine
          - buffer must be allocated and initiazed before
          - buffeLen must be indicated exactly length of 
          buffer
        + The routine guarantees that the status value will 
          have value of
          - buffer
- Usage:      N/A
****************************************************************************/
char *RemoveCharacters(char *buffer, int bufferLen)
{
  int i;
  int len = strlen(buffer);
  
  for (i = len -1 ; i > -1; i--)
  {
    if (buffer[i] <= USED_CHAR_CODE)
    { 
      buffer[i] = 0;
    }
    else
    {
      break;
    }
  }
  
  return buffer;
}

/****************************************************************************
- Function name:  Lrc_itoa
- Input:      + value
- Output:   + strResult
- Return:   [To be done]
- Description:  + The routine hides the following facts
          - Convert value from integer data type to 'string'
        + There are no preconditions guaranteed to the routine
- Usage:      N/A
****************************************************************************/
int Lrc_itoa(int value, char *strResult)
{
  char temp[11];
  register int index = 0;
  register int i = 0;
  
  if (value > 0)
  {
    while (value != 0)
    {
      temp[index] = (48 + value % 10);
      value = value / 10; 
      index++;
    }

    for (i = index - 1; i > -1; i--)
    {
      *strResult++ = (char)temp[i];
    }

    *strResult = 0;
    
    return index;
  }
  else if (value < 0)
  {
    *strResult++ = '-';

    value = -1 * value;

    while (value != 0)
    {
      temp[index] = (char)(48 + value % 10);
      value = value / 10; 
      index++;
    }

    for (i = index - 1; i > -1; i--)
    {
      *strResult++ = (char)temp[i];
    }

    *strResult = 0;
    
    return (index + 1);
  }
  else
  {
    strResult[0] = 48;
    strResult[1] = 0;
    
    return 1;
  }
}

/****************************************************************************
- Function name:  Lrc_itoaf
- Input:      + value
        + pad
        + len
- Output:   + strResult
- Return:   [To be done]
- Description:  + The routine hides the following facts
          - Convert value from integer data type to 'string'
        + There are no preconditions guaranteed to the routine
- Usage:      N/A
****************************************************************************/
int Lrc_itoaf(int value, const char pad, const int len, char *strResult)
{
  register int i = 0;
  if (value == 0)
  {
    for (i = 0; i < len; i++)
    {
      strResult[i] = pad;
    }
    strResult[len] = 0;

    return len;
  }

  register int index = 0;

  while (value != 0)
  {
    strResult[len - index - 1] = 48 + value % 10;
    value = value / 10; 
    index++;
  }
  
  for (i = 0; i < len - index; i++)
  {
    strResult[i] = pad;
  }

  strResult[len] = 0;

  return len;
}

inline int Lrc_atoi(const char * buffer, int len)
{
  register int retval = 0;

  while (*buffer == 32)
  {
    buffer++;
    len--;
  }
  
  for (; len > 0; buffer++, len--)
  {
    retval = retval * 10 + ((*buffer) - 48);
  }
  return retval;
}

/****************************************************************************
- Function name:  AddDataBlockTime
****************************************************************************/
void AddDataBlockTime(unsigned char *formattedTime)
{
  *((long*)formattedTime) = hbitime_micros() * 1000;
  /*int sec, usec;
  hbitime_micros_parts(&sec, &usec);
  
  memcpy(formattedTime, &sec, 4);
  memcpy(&formattedTime[4], &usec, 4);
  formattedTime[8] = 0;*/
}

/****************************************************************************
- Function name:  GetLocalTimeString
- Input:      
- Output:   
- Return:   
- Description:
- Usage:    
****************************************************************************/
char *GetLocalTimeString(char *timeString)
{
  int seconds, micros;
  hbitime_micros_parts(&seconds, &micros);
  time_t now = seconds;
  struct tm *l_time = (struct tm *)localtime(&now);

  // Get time string
  strftime(timeString, 16, "%H:%M:%S", l_time);
  sprintf(timeString + strlen(timeString), ".%06d", micros);

  return timeString;
}

/****************************************************************************
- Function name:  GetTimeStampString
- Input:      
- Output:   
- Return:   
- Description:
- Usage:    
****************************************************************************/
char *GetTimeStampString(char *timeString)
{
  int seconds, micros;
  hbitime_micros_parts(&seconds, &micros);
  time_t now = seconds;
  struct tm *l_time = (struct tm *)localtime(&now);

  // Get time string
  strftime(timeString, 16, "%H:%M:%S", l_time);
  sprintf(timeString + strlen(timeString), ".%06d", micros);

  return timeString;
}

/****************************************************************************
- Function name:  Lrc_gmtime
- Input:      [To be done]
- Output:   [To be done]
- Return:   [To be done]
- Description:  + The routine hides the following facts
        + There are no preconditions guaranteed to the routine
- Usage:      N/A
****************************************************************************/
inline struct tm *Lrc_gmtime (time_t *tp, int years, int secs_in_years)
{
  static int __days_per_month[] = {31,28,31,30,31,30,31,31,30,31,30,31};
  static struct tm tm2;
  time_t t;

  t = *tp;
  tm2.tm_sec  = 0;
  tm2.tm_min  = 0;
  tm2.tm_hour = 0;
  tm2.tm_mday = 1;
  tm2.tm_mon  = 0;
  tm2.tm_year = years;

  t -= secs_in_years;

  tm2.tm_yday = t / LRC_SECS_PER_DAY;       /* days since Jan 1 */

  if ( Lrc_is_leap(tm2.tm_year) )         /* leap year ? */
    __days_per_month[1]++;

  while ( t >= __days_per_month[tm2.tm_mon] * LRC_SECS_PER_DAY ) {
    t -= __days_per_month[tm2.tm_mon++] * LRC_SECS_PER_DAY;
  }
  
  tm2.tm_mon++;

  if ( Lrc_is_leap(tm2.tm_year) )         /* leap year ? */
    __days_per_month[1]--;

  tm2.tm_mday = t / LRC_SECS_PER_DAY + 1;
  t = t % LRC_SECS_PER_DAY;

  tm2.tm_hour = t / LRC_SECS_PER_HOUR;
  t = t % LRC_SECS_PER_HOUR;
  
  tm2.tm_min = t / LRC_SECS_PER_MINUTE;
  tm2.tm_sec = t % LRC_SECS_PER_MINUTE;

  return( &tm2);
}

/****************************************************************************
- Function name:  Get_secs_in_years
- Input:      [To be done]
- Output:   [To be done]
- Return:   [To be done]
- Description:  + The routine hides the following facts
        + There are no preconditions guaranteed to the routine
- Usage:      N/A
****************************************************************************/
inline int Get_secs_in_years(time_t *tp, int *year, int *secs_in_years)
{
  static struct tm tm2;
  time_t t,secs_this_year;

  t = *tp;
  tm2.tm_year = 70;

  /*
   *  This loop handles dates in 1970 and later
   */
  while ( t >= ( secs_this_year =
           Lrc_is_leap(tm2.tm_year) ?
           LRC_SECS_PER_LEAP :
           LRC_SECS_PER_YEAR ) ) {
    t -= secs_this_year;
    tm2.tm_year++;
  }

  /*
   *  This loop handles dates before 1970
   */
  while ( t < 0 )
    t += Lrc_is_leap(--tm2.tm_year) ? LRC_SECS_PER_LEAP : LRC_SECS_PER_YEAR;
  
  *secs_in_years = (*tp - t);
  *year = tm2.tm_year + 1900;

  return (*tp - t);
}

/****************************************************************************
- Function name:  TrimRight
- Input:      + buffer
- Output:   
- Return:   
- Description:  + 
- Usage:    
****************************************************************************/
void TrimRight( char * buffer, int len )
{
  int i;
  for( i = 0; i < len; i ++ )
  {
    if( buffer[i] == ' ' )
    {
      buffer[i] = 0;
    } 
  }
  
  return;
}

/****************************************************************************
- Function name:  Lrc_Split
- Input:      
- Output:   
- Return:   
- Description:
- Usage:    
****************************************************************************/
char Lrc_Split(const char *str, char c, void *parameter)
{
  t_Parameters *workingList = (t_Parameters *)parameter;

  int i = 0;
  
  // Initialize ...
  workingList->countParameters = 0;

  while ((*str != 0) && (*str != 10) && (*str != 13))
  {
    if (*str != c)
    {
      workingList->parameterList[workingList->countParameters][i] = *str;

      i ++;
    }
    else
    {
      workingList->parameterList[workingList->countParameters][i] = 0;
      workingList->countParameters ++;        
      i = 0;
    }

    str ++;
  }

  workingList->parameterList[workingList->countParameters][i] = 0;
  workingList->countParameters ++;

  return workingList->countParameters;
}

/****************************************************************************
- Function name:  GetDateStringFormEpochSecs
- Input:      
- Output:   
- Return:   
- Description:
- Usage:    
****************************************************************************/
char *GetDateStringFormEpochSecs(int numberOfSeconds, char *dateString)
{
  struct tm *l_time;
  time_t numberOfSecondsTmp = numberOfSeconds;
  
  // Convert to local time
  l_time = (struct tm *)localtime(&numberOfSecondsTmp);
  
  // Get date string 
  strftime(dateString, 16, "%Y/%m/%d", l_time);
  
  return dateString;
}

/****************************************************************************
- Function name:  GetCurrentDateString
- Input:      
- Output:   
- Return:   
- Description:
- Usage:    
****************************************************************************/
char *GetCurrentDateString(char *dateString)
{
  time_t now = (time_t)hbitime_seconds();
  struct tm *l_time = (struct tm *)localtime(&now);
  
  // Get date string 
  strftime(dateString, MAX_TIMESTAMP, "%Y-%m-%d", l_time);
  
  return dateString;
}

/****************************************************************************
- Function name:  GetNumberOfSecondsOfLocalTime
- Input:      
- Output:   
- Return:   
- Description:
- Usage:    
****************************************************************************/
int GetNumberOfSecondsOfLocalTime(void)
{
  return hbitime_seconds();
}

/****************************************************************************
- Function name:  GetNumberOfSecondsFromTimestamp
- Input:      
- Output:   
- Return:   
- Description:
- Usage:    
****************************************************************************/
int GetNumberOfSecondsFromTimestamp(const char *timestamp)
{
  // Get number of seconds from timestamp (HH:MM:SS.ssssss)
  return (((timestamp[0] - 48) * 10 + (timestamp[1] - 48)) * 3600 + 
      ((timestamp[3] - 48) * 10 + (timestamp[4] - 48)) * 60 + 
      (timestamp[6] - 48) * 10 + (timestamp[7] - 48));
}

/****************************************************************************
- Function name:  GetNumberOfSecondFromTimeString
- Input:      
- Output:   
- Return:     The number of second from time format hh:mm:ss
- Description:
- Usage:    
****************************************************************************/
int GetNumberOfSecondFromTimeString(const char *timeString)
{
  // time string format: hh:mm:ss
  struct tm tm;
  memset(&tm, 0, sizeof(struct tm));
  char *retVal = strptime(timeString, "%H:%M:%S", &tm);
  if (retVal == NULL)
  {
    return -1;
  }
  
  int numSeconds = tm.tm_sec + tm.tm_min * 60 + tm.tm_hour * 3600;
  return numSeconds;
}

/****************************************************************************
- Function name:  GetTimeInSecondsFromFile
- Input:      
- Output:   
- Return:   
- Description:
- Usage:    
****************************************************************************/
int GetTimeInSecondsFromFile(char fullPath[MAX_PATH_LEN], int *numSeconds, char wantedString[MAX_LINE_LEN])
{
  char const* timeString = Parameters_get(fullPath, wantedString, 0);
  if (timeString != 0)
  {
    *numSeconds = GetNumberOfSecondFromTimeString(timeString);
    if (*numSeconds == -1)
    {
      TraceLog(ERROR_LEVEL, "Invalid time of '%s' from file '%s'\n", wantedString, fullPath);
      return -1;
    }
  }
  else
  {
    TraceLog(ERROR_LEVEL, "Could not get '%s' from file '%s'\n", wantedString, fullPath);
    return -1;
  }
  
  return 0;
}

/****************************************************************************
- Function name:  GetNanoSecondsFromData
- Input:      
- Output:   
- Return:   
- Description:
- Usage:    
****************************************************************************/
long GetNanoSecondsFromData(const char *timestamp)
{
  struct tm * timeinfo;
  time_t rawtime;
  time ( &rawtime );
  timeinfo = localtime ( &rawtime );
  
  timeinfo->tm_year = (Lrc_atoi(&timestamp[0], 2) + 2000) - 1900;
  timeinfo->tm_mon = Lrc_atoi(&timestamp[3], 2) - 1;
  timeinfo->tm_mday = Lrc_atoi(&timestamp[6], 2);
  timeinfo->tm_hour = Lrc_atoi(&timestamp[9], 2);
  timeinfo->tm_min = Lrc_atoi(&timestamp[12], 2);
  timeinfo->tm_sec = Lrc_atoi(&timestamp[15], 2);
  long nanoSecond = ((mktime( timeinfo ) * 1000000) + Lrc_atoi(&timestamp[18], 6)) * 1000;
  
  return nanoSecond;
}

/****************************************************************************
- Function name:  GetNumberOfSecondsFrom1970
- Input:      
- Output:   
- Return:   
- Description:
- Usage:    
****************************************************************************/
int GetNumberOfSecondsFrom1970(const char *format, const char *dateString)
{
  struct tm tmpDate;
  time_t numSeconds;
  char *valRet;

  //  Get seconds since 1970
  memset(&tmpDate, 0, sizeof(tmpDate));
  
  valRet = strptime(dateString, format, &tmpDate);

  if (valRet != 0)
  {
    tmpDate.tm_hour = 0;
    tmpDate.tm_min = 0;
    tmpDate.tm_sec = 0;
    
    numSeconds = mktime(&tmpDate);
    
    return numSeconds;
  }
  else
  {
    return ERROR;
  }
}

/****************************************************************************
- Function name:  GetNumberOfNanoSecondsSinceUnixEpoch
- Input:      
- Output:   
- Return:   
- Description:
- Usage:    
****************************************************************************/
long GetNumberOfNanoSecondsSinceUnixEpoch()
{
  return hbitime_micros() * 1000;
}

/****************************************************************************
- Function name:  ReplaceCharacters
- Input:      + buffer
        + bufferLen
- Output:   + buffer
- Return:   Buffer with replace used characters
- Description:  + The routine hides the following facts
          - Check byte by byte with ASCII code 32 (SPACE) and remove
          any characters less than or equal
        + There are preconditions guaranteed to the routine
          - buffer must be allocated and initiazed before
          - buffeLen must be indicated exactly length of 
          buffer
        + The routine guarantees that the status value will 
          have value of
          - buffer
- Usage:      N/A
****************************************************************************/
char *ReplaceCharacters(char *buffer)
{
  int i;
  for(i = 0; i < strlen(buffer); i++)
  {
    if(buffer[i] == '\'')
    {
      buffer[i] = '"';
    }
  }
  return buffer;
}

/****************************************************************************
- Function name:  MappingStockSymbol
- Input:    
- Output:   
- Return:   
- Description:    
- Usage:      N/A 
****************************************************************************/
int MappingStockSymbol(char *stockSymbol)
{
  int i;
  int symbolLen;
  symbolLen = strnlen(stockSymbol, SYMBOL_LEN);
  
  for (i = 0; i < symbolLen; i++)
  {
    // Mapping stock symbol
    // The suffix '$' -> '-'    
    if (stockSymbol[i] == '$')
    {
      stockSymbol[i] = '-';
      //countMappedSymbol += 1;
      
      return SUCCESS;
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ConvertToEpochTime
- Input:    
- Output:   
- Return:   
- Description:    
- Usage:      N/A 
****************************************************************************/
long ConvertToEpochTime(const char *timeStamp)
{
  // Timestamp format = %H:%M:%S.XXXXXX
  char dateTime[32] = "\0";
  memcpy(dateTime, globalDateStringOfData, 10);
  dateTime[10] = '-'; 
  memcpy(&dateTime[11], timeStamp, 8);
  
  int numMicroSecond = atof(&timeStamp[8]) * 1000000 + 0.1; //casting fix
  
  struct tm localTime;
  strptime(dateTime, "%Y/%m/%d-%H:%M:%S", &localTime);
  localTime.tm_isdst = -1;
  time_t epochTime = mktime(&localTime);
  long numSecond = epochTime;
  
  return (numSecond * 1000000 + numMicroSecond);
}

/****************************************************************************
- Function name:  ConvertToEpochTimeString
- Input:    
- Output:   
- Return:   
- Description:    
- Usage:      N/A 
****************************************************************************/
char *ConvertToEpochTimeString(const char *timeStamp, char *epochTimeString)
{
  // Timestamp format = %H:%M:%S.XXXXXX
  char dateTime[32] = "\0";
  memcpy(dateTime, globalDateStringOfData, 10);
  dateTime[10] = '-'; 
  memcpy(&dateTime[11], timeStamp, 8);
  
  int numMicroSecond = atof(&timeStamp[8]) * 1000000;
  
  struct tm localTime;
  strptime(dateTime, "%Y/%m/%d-%H:%M:%S", &localTime);
  localTime.tm_isdst = -1;
  time_t epochTime = mktime(&localTime);
  long numSecond = epochTime;
  
  sprintf(epochTimeString, "%ld.%06d", numSecond, numMicroSecond);
  return epochTimeString;
}

/****************************************************************************
- Function name:  ConvertSpecialCharacter
- Input:
- Output:
- Return:
- Description:
- Usage:      N/A
****************************************************************************/
char *ConvertSpecialCharacter(char *text, char *result)
{
  int i, offset = 0;
  int len = strlen(text);

  for (i = 0; i < len; i++)
  {
    switch(text[i])
    {
      case ' ':
        result[offset++] = '%';
        result[offset++] = '2';
        result[offset++] = '0';
        break;

      case '!':
        result[offset++] = '%';
        result[offset++] = '2';
        result[offset++] = '1';
        break;

      case '"':
        result[offset++] = '%';
        result[offset++] = '2';
        result[offset++] = '2';
        break;

      case '#':
        result[offset++] = '%';
        result[offset++] = '2';
        result[offset++] = '3';
        break;

      case '$':
        result[offset++] = '%';
        result[offset++] = '2';
        result[offset++] = '4';
        break;

      case '%':
        result[offset++] = '%';
        result[offset++] = '2';
        result[offset++] = '5';
        break;

      case '&':
        result[offset++] = '%';
        result[offset++] = '2';
        result[offset++] = '6';
        break;

      case 39: // character '
        result[offset++] = '%';
        result[offset++] = '2';
        result[offset++] = '7';
        break;

      case '(':
        result[offset++] = '%';
        result[offset++] = '2';
        result[offset++] = '8';
        break;

      case ')':
        result[offset++] = '%';
        result[offset++] = '2';
        result[offset++] = '9';
        break;

      case '*':
        result[offset++] = '%';
        result[offset++] = '2';
        result[offset++] = 'A';
        break;

      case '+':
        result[offset++] = '%';
        result[offset++] = '2';
        result[offset++] = 'B';
        break;

      case ',':
        result[offset++] = '%';
        result[offset++] = '2';
        result[offset++] = 'C';
        break;

      case '/':
        result[offset++] = '%';
        result[offset++] = '2';
        result[offset++] = 'F';
        break;

      case ':':
        result[offset++] = '%';
        result[offset++] = '3';
        result[offset++] = 'A';
        break;

      case ';':
        result[offset++] = '%';
        result[offset++] = '3';
        result[offset++] = 'B';
        break;

      case '=':
        result[offset++] = '%';
        result[offset++] = '3';
        result[offset++] = 'D';
        break;

      case '?':
        result[offset++] = '%';
        result[offset++] = '3';
        result[offset++] = 'F';
        break;

      case '@':
        result[offset++] = '%';
        result[offset++] = '4';
        result[offset++] = '0';
        break;

      case '[':
        result[offset++] = '%';
        result[offset++] = '5';
        result[offset++] = 'B';
        break;

      case ']':
        result[offset++] = '%';
        result[offset++] = '5';
        result[offset++] = 'D';
        break;

      case '-':
        result[offset++] = '%';
        result[offset++] = '2';
        result[offset++] = 'D';
        break;

      case '.':
        result[offset++] = '%';
        result[offset++] = '2';
        result[offset++] = 'E';
        break;

      case '<':
        result[offset++] = '%';
        result[offset++] = '3';
        result[offset++] = 'C';
        break;

      case '>':
        result[offset++] = '%';
        result[offset++] = '3';
        result[offset++] = 'E';
        break;

      case 92: // character '\'
        result[offset++] = '%';
        result[offset++] = '5';
        result[offset++] = 'C';
        break;

      case '^':
        result[offset++] = '%';
        result[offset++] = '5';
        result[offset++] = 'E';
        break;

      case '_':
        result[offset++] = '%';
        result[offset++] = '5';
        result[offset++] = 'F';
        break;

      case '`':
        result[offset++] = '%';
        result[offset++] = '6';
        result[offset++] = '0';
        break;

      case '{':
        result[offset++] = '%';
        result[offset++] = '7';
        result[offset++] = 'B';
        break;

      case '|':
        result[offset++] = '%';
        result[offset++] = '7';
        result[offset++] = 'C';
        break;

      case '}':
        result[offset++] = '%';
        result[offset++] = '7';
        result[offset++] = 'D';
        break;

      case '~':
        result[offset++] = '%';
        result[offset++] = '7';
        result[offset++] = 'E';
        break;

      default:
        result[offset++] = text[i];
        break;
    }
  }
  result[offset] = 0;

  return result;
}

/****************************************************************************
- Function name:  Convert_ClOrdID_To_NYSE_Format
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
char *Convert_ClOrdID_To_NYSE_Format(int idOrder, char *strResult)
{
  /*
   * A = 0, B = 1, C = 2, D = 3, E = 4, F = 5, G = 6, H = 7, I = 8, J = 9
   * To avoid all zeros in the sequence number, we must plus 1
   * For example: idOrder = 1234567 is converted to BCD 4691
  */
  int seqNumber = idOrder % 9999;
  int branchCode = (idOrder -  seqNumber) / 9999;
  seqNumber = seqNumber + 1; // Avoid all zeros in the sequence number

  // branch code
  strResult[2] = 65 + branchCode % 10; // ASCII 'A' = 65
  branchCode = branchCode/10;
  strResult[1] = 65 + branchCode % 10;
  branchCode = branchCode/10;
  strResult[0] = 65 + branchCode;

  // space
  strResult[3] =  '%';
  strResult[4] =  '2';
  strResult[5] =  '0';

  // sequence number
  strResult[9] = 48 + seqNumber % 10; // // ASCII '0' = 48
  seqNumber = seqNumber/10;
  strResult[8] = 48 + seqNumber % 10;
  seqNumber = seqNumber/10;
  strResult[7] = 48 + seqNumber % 10;
  seqNumber = seqNumber/10;
  strResult[6] = 48 + seqNumber;

  // add character '/'
  strResult[10] = '%';
  strResult[11] = '2';
  strResult[12] = 'F';

  // add Stamptime
  strncpy(&strResult[13], Nyse_CCG_timestamp, 8);
  strResult[21] = 0;

  return strResult;
}

/****************************************************************************
- Function name:  IsFileTimestampToday
- Input:      full file path
- Output:     
- Return:     -1: could not get current time
           0: not same date
           1: same date
           2: file does not exist
- Description:    Check file modification time to see if it's of the same 
          date of current date or not
- Usage:    
****************************************************************************/
int IsFileTimestampToday(char *fullPath)
{
  //Check if file already existed
  if (access(fullPath, F_OK) != 0)
  {
    return 2;
  }
  
  struct stat fileInfo;
  stat(fullPath, &fileInfo);
  
  // Check modification time fileInfo.st_mtime vs current date
  // Convert to local time
  struct tm fileTime;
  time_t seconds = fileInfo.st_mtime - (time_t)hbitime_get_offset_seconds();
  localtime_r(&seconds, &fileTime);
  
  //Get current date
  time_t currentTime = (time_t)hbitime_seconds();
  
  struct tm local;
  // Convert to local time
  localtime_r(&currentTime, &local);
  
  if ((local.tm_year != fileTime.tm_year) ||
    (local.tm_mday != fileTime.tm_mday) ||
    (local.tm_mon != fileTime.tm_mon))
  {
    return 0;
  }
  
  return 1;
}

/****************************************************************************
- Function name:  GetCurrentVolatileSession
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:    
****************************************************************************/
int GetCurrentVolatileSession()
{
  time_t now = (time_t)hbitime_seconds();
  struct tm local;
  localtime_r(&now, &local);
  
  int timeInSecs = local.tm_hour * 3600 + local.tm_min * 60 + local.tm_sec;
  
  // [PRE]:     [00:00:00 to 08:35:00]
  // [POST]:    [15:00:00 to 24:00:00]
  // [INTRADAY]:  [08:35:01 to 14:59:59]
  
  // 08:35:00 ---> 8*3600 + 35*60 = 30900
  // 15:00:00 ---> 54000
  // 17:29:59 ---> 62999
  
  if (timeInSecs <= 30900) 
    return VOLATILE_SESSION_PRE;                // [PRE]
  else if (timeInSecs >= 54000)
    return VOLATILE_SESSION_POST;               // [POST]
  else
    return VOLATILE_SESSION_INTRADAY;             // [INTRADAY]
}

/****************************************************************************
- Function name:  CheckToDeleteOldVolatileFiles
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:    
****************************************************************************/
void CheckToDeleteOldVolatileFiles()
{
  char fullPath[MAX_PATH_LEN];
  int ret;
  
  if (GetFullConfigPath(VOLATILE_SYMBOL_FILE_NAME, CONF_SYMBOL, fullPath) != NULL_POINTER)
  {
    ret = IsFileTimestampToday(fullPath);
    if (ret == 0)
    {
      TraceLog(NOTE_LEVEL, "Volatile symbol list file (%s) is old, AS will automatically delete it now\n", fullPath);
      remove(fullPath);
    }
  }
    
  /*if (GetFullRawDataPath(VOLATILE_SECTION_FILE_NAME, fullPath) != NULL_POINTER)
  {
    ret = IsFileTimestampToday(fullPath);
    if (ret == 0)
    {
      TraceLog(DEBUG_LEVEL, "Volatile section file (%s) is old, AS will automatically delete it now\n", fullPath);
      remove(fullPath);
    }
  }*/
  
  return;
}

/****************************************************************************
- Function name:  LrcPrintfLogFormat
- Input:
- Output:     N/A
- Return:     N/A
- Description:
- Usage:      N/A
****************************************************************************/
void LrcPrintfLogFormat(int level, char *fmt, ...)
{
  char formatedStr[51200] = "\0";
  switch (level)
  {
    case DEBUG_LEVEL:
      strcpy(formatedStr, DEBUG_STR);
      break;
    case ERROR_LEVEL:
      strcpy(formatedStr, ERROR_STR);
      break;
    case NOTE_LEVEL:
      strcpy(formatedStr, NOTE_STR);
      break;
    case WARN_LEVEL:
      strcpy(formatedStr, WARN_STR);
      break;
    case STATS_LEVEL:
      strcpy(formatedStr, STATS_STR);
      break;
    case FATAL_LEVEL:
      strcpy(formatedStr, FATAL_STR);
      break;
    case USAGE_LEVEL:
      strcpy(formatedStr, USAGE_STR);
      break;
    default:
      formatedStr[0] = 0;
      break;
  }
  strcat(formatedStr, fmt);

  va_list ap;
  va_start (ap, fmt);

  // vprintf (fmt, ap);
  vprintf (formatedStr, ap);

  va_end (ap);

  fflush(stdout);
}

/****************************************************************************
- Function name:  PrintErrStr
- Input:      
- Output:   
- Return:   
- Description:    Print on stdout the following:
          level description, then the input string _str, then the 
          string description of errno _errno, and a new line character
          After that, flush stdout
- Usage:      N/A
****************************************************************************/
void PrintErrStr(int _level, const char *_str, int _errno)
{
  char errStr[128];
  errStr[0] = 0;
  
#if ((_POSIX_C_SOURCE >= 200112L || _XOPEN_SOURCE >= 600) && !defined (_GNU_SOURCE))
  strerror_r(_errno, errStr, 128);
  switch (_level)
  {
    case DEBUG_LEVEL:
      printf("%s%s%s\n", DEBUG_STR, _str, errStr);
      break;
    case NOTE_LEVEL:
      printf("%s%s%s\n", NOTE_STR, _str, errStr);
      break;
    case WARN_LEVEL:
      printf("%s%s%s\n", WARN_STR, _str, errStr);
      break;
    case ERROR_LEVEL:
      printf("%s%s%s\n", ERROR_STR, _str, errStr);
      break;
    case FATAL_LEVEL:
      printf("%s%s%s\n", FATAL_STR, _str, errStr);
      break;
    case USAGE_LEVEL:
      printf("%s%s%s\n", USAGE_STR, _str, errStr);
      break;
    case STATS_LEVEL:
      printf("%s%s%s\n", STATS_STR, _str, errStr);
      break;
    default:
      printf("%s%s\n", _str, errStr);
      break;
  }
#else
  char *_errString;
  _errString = strerror_r(_errno, errStr, 128);
  switch (_level)
  {
    case DEBUG_LEVEL:
      printf("%s%s%s\n", DEBUG_STR, _str, _errString);
      break;
    case NOTE_LEVEL:
      printf("%s%s%s\n", NOTE_STR, _str, _errString);
      break;
    case WARN_LEVEL:
      printf("%s%s%s\n", WARN_STR, _str, _errString);
      break;
    case ERROR_LEVEL:
      printf("%s%s%s\n", ERROR_STR, _str, _errString);
      break;
    case FATAL_LEVEL:
      printf("%s%s%s\n", FATAL_STR, _str, _errString);
      break;
    case USAGE_LEVEL:
      printf("%s%s%s\n", USAGE_STR, _str, _errString);
      break;
    case STATS_LEVEL:
      printf("%s%s%s\n", STATS_STR, _str, _errString);
      break;
    default:
      printf("%s%s\n", _str, _errString);
      break;
  }
#endif
  fflush(stdout);
}

/****************************************************************************
- Function name:  ExecuteCommand
- Input:      command : a command is executed
          timeout : timeout in second which force to stop the command
- Output:     
- Return:     SUCCESS execute the command successfully
          ERROR execute the command unsuccessfully
- Description:    
- Usage:    
****************************************************************************/
int ExecuteCommand(const char *command, const int timeout)
{
  pid_t pid = fork();
  
  if (pid == 0)
  {
    // This is the child process.  Execute the shell command.
    TraceLog(DEBUG_LEVEL, "Child process %d calls command '%s'\n", getpid(), command);
    
    execl("/bin/sh", "/bin/sh", "-c", command, NULL);
    
    //execl only returns if an error has occurred
    PrintErrStr(ERROR_LEVEL, "Inside ExecuteCommand(), execl(): ", errno);
    return ERROR;
  }
  else if (pid < 0)
  {
    // The fork failed.  Report failure.
    PrintErrStr(ERROR_LEVEL, "Inside ExecuteCommand(), fork(): ", errno);
    return ERROR;
  }
  
  // This is the parent process.  Wait for the child to complete.
  TraceLog(DEBUG_LEVEL, "Waiting for the child process (pid=%d)...\n", (int)pid);
  int counter = 0;
  pid_t retPID = -1;
  int status = 0;
  while (counter < timeout)
  {
    sleep(1); //wait for a second before check status of child process again
    retPID = waitpid(-1, &status, WNOHANG); //return immediately if no child has exited
    TraceLog(DEBUG_LEVEL, "returned process id=%d status=%d\n", retPID, status);
    if (retPID == -1)
    {
      PrintErrStr(ERROR_LEVEL, "Inside ExecuteCommand(), waitpid(): ", errno);
      return ERROR;
    }
    else if (retPID == 0)
    {
      //The child hasn't changed state
      counter++;
      continue;
    }
    else
    {
      //The child has changed state
      if (WIFEXITED(status))
      {
        TraceLog(DEBUG_LEVEL, "Child process has exited normally\n");
        return SUCCESS;
      }
      else if (WIFSIGNALED(status))
      {
        int signum = WTERMSIG(status); //Get number of signal which is sent to child process
        TraceLog(DEBUG_LEVEL, "Child process has terminated by signum=%d\n", signum);
        return ERROR;
      }
    }
  }
  
  if (counter == timeout)
  {
    TraceLog(DEBUG_LEVEL, "Child process (pid=%d) takes %d seconds, send signal to kill it\n", pid, timeout);
    int value = kill(pid, SIGKILL);
    if (value == 0)
    {
      //Child Process is terminated
      return ERROR;
    }
    else
    {
      TraceLog(DEBUG_LEVEL, "Can not kill the child process (pid=%d)\n", (int)pid);
      PrintErrStr(ERROR_LEVEL, "Inside ExecuteCommand(), kill(): ", errno);
      return ERROR;
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  InitializeDecodeTable
- Input:      
- Output:     
- Return:     
- Description:    Initialize the decoding table for base 64
- Usage:    
****************************************************************************/
char DecodingTable[256];
void InitializeDecodeTable()
{
  const char encodingTable[] = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                  'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                  'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                  'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                  'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                  'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                  'w', 'x', 'y', 'z', '0', '1', '2', '3',
                  '4', '5', '6', '7', '8', '9', '+', '/'};

  memset(DecodingTable, 0, sizeof(DecodingTable));
  int i;
    for (i = 0; i < 64; i++)
  {
        DecodingTable[(unsigned char) encodingTable[i]] = i;
  }
}

/****************************************************************************
- Function name:  DecodeBase64
- Input:      
- Output:     
- Return:     -1  if input is NULL
          length of decoded data
- Description:    Decode input in base64
- Usage:    
****************************************************************************/
int DecodeBase64(const char encodedData[MAX_PATH_LEN], int inputLength, unsigned char decodedData[MAX_PATH_LEN])
{
  memset(decodedData, 0, MAX_PATH_LEN);
    if (inputLength % 4 != 0)
  {
    return -1;
  }

    int outputLength = inputLength / 4 * 3;
    if (encodedData[inputLength - 1] == '=') outputLength--;
    if (encodedData[inputLength - 2] == '=') outputLength--;

  int i, j;
  unsigned int sextet_a, sextet_b, sextet_c, sextet_d, triple;
    for (i = 0, j = 0; i < inputLength;)
  {
    sextet_a = encodedData[i] == '=' ? 0 & i++ : DecodingTable[(int)encodedData[i++]];
    sextet_b = encodedData[i] == '=' ? 0 & i++ : DecodingTable[(int)encodedData[i++]];
    sextet_c = encodedData[i] == '=' ? 0 & i++ : DecodingTable[(int)encodedData[i++]];
    sextet_d = encodedData[i] == '=' ? 0 & i++ : DecodingTable[(int)encodedData[i++]];

        triple = (sextet_a << 3 * 6)
        + (sextet_b << 2 * 6)
        + (sextet_c << 1 * 6)
        + (sextet_d << 0 * 6);

        if (j < outputLength) decodedData[j++] = (triple >> 2 * 8) & 0xFF;
        if (j < outputLength) decodedData[j++] = (triple >> 1 * 8) & 0xFF;
        if (j < outputLength) decodedData[j++] = (triple >> 0 * 8) & 0xFF;
    }

    return outputLength;
}

/****************************************************************************/
