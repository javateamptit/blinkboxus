/*****************************************************************************
**  Project:    Equity Arbitrage Application
**  Filename:   trader_tools_proc.c
**  Description:  This file contains function definitions that were declared
          in trader_tools_proc.h  
**  Author:     Luan Vo-Kinh
**  First created:  13-Sep-2007
**  Last updated: -----------
*****************************************************************************/

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <errno.h>

#include "socket_util.h"
#include "configuration.h"
#include "order_mgmt_proc.h"
#include "raw_data_mgmt.h"
#include "output_log_mgmt.h"
#include "nasdaq_rash_proc.h"

#include "arca_direct_proc.h"
#include "position_manager_proc.h"
#include "trade_servers_proc.h"
#include "trade_servers_mgmt_proc.h"
#include "trader_tools_proc.h"
#include "database_util.h"
#include "internal_proc.h"
#include "daedalus_proc.h"
#include "hbitime.h"
#include "logging.h"

extern int PMtoTTCancelLimit;
extern t_VolatileMgmt volatileMgmt;
extern t_TraderCollection traderCollection;

int TT_TO_AS_ORDER_TYPE_MAPPING[] = {-1, -1, -1, TYPE_NASDAQ_RASH, TYPE_ARCA_DIRECT, -1, -1, -1, -1, -1};
int TT_TO_AS_ORDER_INDEX_MAPPING[] = {-1, -1, -1, AS_NASDAQ_RASH_INDEX, AS_ARCA_DIRECT_INDEX, -1, -1, -1, -1, -1};
int TT_TO_TS_ORDER_INDEX_MAPPING[] = {TS_ARCA_DIRECT_INDEX, TS_NASDAQ_OUCH_INDEX, TS_NYSE_CCG_INDEX, TS_NASDAQ_RASH_INDEX, TS_BATSZ_BOE_INDEX, TS_EDGX_DIRECT_INDEX, TS_EDGA_DIRECT_INDEX, TS_NDAQ_OUBX_INDEX, TS_BYX_BOE_INDEX, TS_PSX_OUCH_INDEX};
int TT_TO_TS_BOOK_INDEX_MAPPING[] = {TS_ARCA_BOOK_INDEX, TS_NASDAQ_BOOK_INDEX, TS_NYSE_BOOK_INDEX, TS_BATSZ_BOOK_INDEX, TS_EDGX_BOOK_INDEX, TS_EDGA_BOOK_INDEX, TS_NDBX_BOOK_INDEX, TS_BYX_BOOK_INDEX, TS_PSX_BOOK_INDEX, TS_AMEX_BOOK_INDEX};
int TT_TO_PM_ORDER_INDEX_MAPPING[] = {-1, -1, -1, AS_NASDAQ_RASH_INDEX, AS_ARCA_DIRECT_INDEX, -1, -1, -1, -1, -1};
int TT_TO_PM_BOOK_INDEX_MAPPING[] = {PM_ARCA_BOOK_INDEX, PM_NASDAQ_BOOK_INDEX, -1, -1, -1, -1, -1, -1, -1};

int TS_TO_TT_ORDER_TYPE_MAPPING[] = {0, 1, 3, 2, 4, 5, 6, 7, 8, 9};
int PM_TO_TT_ORDER_TYPE_MAPPING[] = {4, -1, -1, 3, -1, -1, -1, -1, -1, -1};
int AS_TO_TT_ORDER_TYPE_MAPPING[] = {4, -1, 3, -1, -1, -1, -1, -1, -1, -1};
int AS_TO_TT_ORDER_INDEX_MAPPING[] = {4, 3, -1, -1, -1, -1, -1, -1, -1, -1};
int TS_TO_TT_BOOK_INDEX_MAPPING[] = {0, 1 , 2, 3, 4, 5, 6, 7, 8, 9};
int TS_TO_TT_ORDER_INDEX_MAPPING[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
int PM_TO_TT_BOOK_INDEX_MAPPING[] = {0, 1, -1, -1, -1, -1, -1, -1, -1};
int PM_TO_TT_ORDER_INDEX_MAPPING[] = {4, 3, -1, -1, -1, -1, -1, -1, -1, -1};

extern int TTIndexUpdatedBP;

int isValidTTVersion[MAX_TRADER_TOOL_CONNECTIONS];
/****************************************************************************
** Global variables definition
****************************************************************************/

// For Trade Server configuration
#define MSG_TYPE_FOR_SEND_TS_CONF_TO_TT 15001
#define MSG_TYPE_FOR_SET_TS_CONF_FROM_TT 2001

// For Trader Tool configuration
#define MSG_TYPE_FOR_SEND_TT_CONF_TO_TT 19001
#define MSG_TYPE_FOR_SET_TT_CONF_FROM_TT 4001

// For Aggregation Server global setting
#define MSG_TYPE_FOR_SEND_AS_GLOBAL_SETTING_TO_TT 19002
#define MSG_TYPE_FOR_SET_AS_GLOBAL_SETTING_FROM_TT 4002

// For ignore symbol list
#define MSG_TYPE_FOR_SEND_IGNORE_SYMBOL_LIST_TO_TT 19003
#define MSG_TYPE_FOR_SET_IGNORE_SYMBOL_LIST_FROM_TT 4003

//For ignore symbol ranges list
#define MSG_TYPE_FOR_SEND_IGNORE_SYMBOL_RANGES_LIST_TO_TT 19005
#define MSG_TYPE_FOR_SET_IGNORE_SYMBOL_RANGES_LIST_FROM_TT 4006

#define MSG_TYPE_FOR_SEND_VOLATILE_SYMBOL_LIST_TO_TT 19006
#define MSG_TYPE_FOR_SET_VOLATILE_SYMBOL_UPDATE_FROM_TT 4004

#define MSG_TYPE_FOR_SEND_VOLATILE_SECTIONS_TO_TT 19008
#define MSG_TYPE_FOR_SET_VOLATILE_SECTION_UPDATE_FROM_TT 4008

#define MSG_TYPE_FOR_SEND_ALL_VOLATILE_TOGGLE_TO_TT 19007
#define MST_TYPE_FOR_SET_ALL_VOLATILE_TOGGLE_FROM_TT 4005

#define MSG_TYPE_FOR_SEND_ACTIVE_VOLATILE_SESSION_TO_TT 19009
#define MSG_TYPE_FOR_SEND_ACTIVE_VOLATILE_TOGGLE_TO_TT 19010

#define MSG_TYPE_FOR_REREQUEST_ETB_LIST_FROM_TT 4007

// For Market Mode status: NORMAL MODE or FAST MODE
#define MSG_TYPE_FOR_SEND_MARKET_MODE_TO_TT 19004

// For AS connect/disconnect to an TS
#define MSG_TYPE_FOR_RECEIVE_CONNECT_TS_FROM_TT 1
#define MSG_TYPE_FOR_RECEIVE_DISCONNECT_TS_FROM_TT 2

// For AS connect/disconnect to all TS
#define MSG_TYPE_FOR_RECEIVE_CONNECT_ALL_TS_FROM_TT 3
#define MSG_TYPE_FOR_RECEIVE_DISCONNECT_ALL_TS_FROM_TT 4

// For AS connect/disconnect to all TS
#define MSG_TYPE_FOR_RECEIVE_RESET_STUCK_ALL_TS_FROM_TT 24
#define MSG_TYPE_FOR_RECEIVE_RESET_LOSER_ALL_TS_FROM_TT 25
#define MSG_TYPE_FOR_RECEIVE_RESET_BUYING_POWER_ALL_TS_FROM_TT 28

// For connect/disconnect ECN Book
#define MSG_TYPE_FOR_RECEIVE_CONNECT_BOOK_FROM_TT 5
#define MSG_TYPE_FOR_RECEIVE_DISCONNECT_BOOK_FROM_TT 8

// For connect/disconnect all ECN Book
#define MSG_TYPE_FOR_RECEIVE_CONNECT_ALL_BOOK_FROM_TT 6
#define MSG_TYPE_FOR_RECEIVE_DISCONNECT_ALL_BOOK_FROM_TT 7

// For connect/disconnect ECN Order
#define MSG_TYPE_FOR_RECEIVE_CONNECT_ORDER_FROM_TT 9
#define MSG_TYPE_FOR_RECEIVE_DISCONNECT_ORDER_FROM_TT 10

// For connect/disconnect all ECN Order
#define MSG_TYPE_FOR_RECEIVE_CONNECT_ALL_ORDER_FROM_TT 26
#define MSG_TYPE_FOR_RECEIVE_DISCONNECT_ALL_ORDER_FROM_TT 27

// For enable/disable trading on a ECN Order of a TS
#define MSG_TYPE_FOR_RECEIVE_ENABLE_TRADING_A_ORDER_A_TS_FROM_TT 11
#define MSG_TYPE_FOR_RECEIVE_DISABLE_TRADING_A_ORDER_A_TS_FROM_TT 14

// For enable/disable trading on all ECN Order of a TS
#define MSG_TYPE_FOR_RECEIVE_ENABLE_TRADING_ALL_ORDER_A_TS_FROM_TT 12
#define MSG_TYPE_FOR_RECEIVE_DISABLE_TRADING_ALL_ORDER_A_TS_FROM_TT 15

// For enable/disable trading on all ECN Order of all TS
#define MSG_TYPE_FOR_RECEIVE_ENABLE_TRADING_ALL_ORDER_ALL_TS_FROM_TT 13
#define MSG_TYPE_FOR_RECEIVE_DISABLE_TRADING_ALL_ORDER_ALL_TS_FROM_TT 16

// For ECN Books configuration at TS
#define MSG_TYPE_FOR_SEND_TS_BOOK_CONF_TO_TT 15003
#define MSG_TYPE_FOR_SET_TS_BOOK_CONF_FROM_TT 2003

// For ECN Orders configuration at TS
#define MSG_TYPE_FOR_SEND_TS_ORDER_CONF_TO_TT 15004
#define MSG_TYPE_FOR_SET_TS_ORDER_CONF_FROM_TT 2004

// For TS global configuration
#define MSG_TYPE_FOR_SEND_TS_GLOBAL_CONF_TO_TT 15005
#define MSG_TYPE_FOR_SET_TS_GLOBAL_CONF_FROM_TT 2005

// For TS minimum spread conf
#define MSG_TYPE_FOR_SEND_MIN_SPREAD_CONF_TO_TT 15022
#define MSG_TYPE_FOR_SET_MIN_SPREAD_CONF_FROM_TT 2022

// For TS current status
#define MSG_TYPE_FOR_SEND_TS_CURRENT_STATUS_TO_TT 15006
#define MSG_TYPE_FOR_RESET_TS_CURRENT_STATUS_FROM_TT 2012

// For TS setting
#define MSG_TYPE_FOR_SEND_TS_SETTING_TO_TT 15013
#define MSG_TYPE_FOR_SET_TS_SETTING_FROM_TT 2013

// For AS setting
#define MSG_TYPE_FOR_SEND_AS_SETTING_TO_TT 15014
#define MSG_TYPE_FOR_SET_AS_SETTING_FROM_TT 2014

#define MSG_TYPE_FOR_SET_ARCA_BOOK_FEED_FROM_TT 15018

#define MSG_TYPE_FOR_SEND_NOTIFY_MSG_TO_TT 15019

//For switching PM ARCA feed
#define MSG_TYPE_FOR_SET_PM_ARCA_BOOK_FEED_FROM_TT 15020

// For ECN Orders configuration at AS
#define MSG_TYPE_FOR_SEND_AS_ORDERS_CONF_TO_TT 15010
#define MSG_TYPE_FOR_SET_AS_ORDERS_CONF_FROM_TT 2010

// For stock symbol status
#define MSG_TYPE_FOR_RECEIVE_SYMBOL_STATUS_FROM_TT 1002
#define MSG_TYPE_FOR_SEND_SYMBOL_STATUS_TO_TT 15002

// For flush book
#define MSG_TYPE_FOR_RECEIVE_FLUSH_A_SYMBOL_FROM_TT 2006
#define MSG_TYPE_FOR_RECEIVE_FLUSH_ALL_SYMBOL_FROM_TT 2007

//For flush PM book
#define MSG_TYPE_FOR_RECEIVE_FLUSH_PM_A_SYMBOL_FROM_TT 2008
#define MSG_TYPE_FOR_RECEIVE_FLUSH_PM_ALL_SYMBOL_FROM_TT 2009

// For send new order and cancel request
#define MSG_TYPE_FOR_RECEIVE_NEW_ORDER_FROM_TT 3001
#define MSG_TYPE_FOR_RECEIVE_CANCEL_REQUEST_FROM_TT 3002
#define MSG_TYPE_FOR_RESPONSE_NEW_ORDER_FROM_TT 17001

// For AS connect/disconnect a ECN Order
#define MSG_TYPE_FOR_RECEIVE_AS_CONNECT_ORDER_FROM_TT 17
#define MSG_TYPE_FOR_RECEIVE_AS_DISCONNECT_ORDER_FROM_TT 19

// For AS connect/disconnect all ECN Order
#define MSG_TYPE_FOR_RECEIVE_AS_CONNECT_ALL_ORDER_FROM_TT 18
#define MSG_TYPE_FOR_RECEIVE_AS_DISCONNECT_ALL_ORDER_FROM_TT 20

// For AS current status
#define MSG_TYPE_FOR_SEND_AS_CURRENT_STATUS_TO_TT 15011
#define MSG_TYPE_FOR_RESET_AS_CURRENT_STATUS_FROM_TT 2011

// For open order
#define MSG_TYPE_FOR_SEND_OPEN_ORDER_TO_TT 16010

// For profit/loss summary
#define MSG_TYPE_FOR_SEND_PROFIT_LOSS_SUMMARY_TO_TT 16011

// For heartbeat message
#define MSG_TYPE_FOR_RECEIVE_HEARTBEAT_TO_TT 3004 

// For open position
#define MSG_TYPE_FOR_SEND_OPEN_POSITION_TO_TT 32003
#define MSG_TYPE_FOR_RECEIVE_OPEN_POSITION_FROM_TT 32004

/*
- Added date: 3/19/2008
- By luanvk
- Purpose: send/receive PM message
*/
#define MSG_TYPE_FOR_SEND_PM_CURRENT_STATUS_TO_TT 11000

#define MSG_TYPE_FOR_SEND_PM_CONF_TO_TT 11500
#define MSG_TYPE_FOR_SET_PM_CONF_FROM_TT 10500

#define MSG_TYPE_FOR_SEND_PM_GLOBAL_SETTING_TO_TT 11501
#define MSG_TYPE_FOR_SET_PM_GLOBAL_SETTING_FROM_TT 10501

#define MSG_TYPE_FOR_SEND_PM_BOOK_CONF_TO_TT 11502
#define MSG_TYPE_FOR_SET_PM_BOOK_CONF_FROM_TT 10502

#define MSG_TYPE_FOR_SEND_PM_ORDER_CONF_TO_TT 11503
#define MSG_TYPE_FOR_SET_PM_ORDER_CONF_FROM_TT 10503

#define MSG_TYPE_FOR_SEND_PM_CTS_SERVER_TO_TT 11504
#define MSG_TYPE_FOR_SET_PM_CTS_SERVER_FROM_TT 10504

#define MSG_TYPE_FOR_SEND_PM_IGNORE_SYMBOL_TO_TT 11505
#define MSG_TYPE_FOR_SET_PM_IGNORE_SYMBOL_FROM_TT 10505

#define MSG_TYPE_FOR_SEND_PM_SYMBOL_STATUS_TO_TT 11506
#define MSG_TYPE_FOR_GET_PM_SYMBOL_STATUS_FROM_TT 10506

#define MSG_TYPE_FOR_SEND_HALTED_SYMBOL_LIST_TO_TT 11507

#define MSG_TYPE_FOR_SEND_A_HALTED_SYMBOL_TO_TT 11508
#define MSG_TYPE_FOR_SET_A_HALT_SYMBOL_FROM_TT 10508
#define MSG_TYPE_FOR_ACTIVATE_HALTED_STOCKS_FROM_TT 10511
#define MSG_TYPE_FOR_SEND_ACTIVATE_ALL_HALTED_STOCKS_TO_TT 11521

#define MSG_TYPE_FOR_SEND_TRADED_LIST_TO_TT 11509
#define MSG_TYPE_FOR_GET_TRADED_LIST_FROM_TT 10509

// For PM setting
#define MSG_TYPE_FOR_SEND_PM_SETTING_TO_TT 11510
#define MSG_TYPE_FOR_SET_PM_SETTING_FROM_TT 10510

//For Book idle warning from TS and PM, use only one message format
#define MSG_TYPE_FOR_SEND_TS_PM_BOOK_IDLE_WARNING_TO_TT 11511

#define MSG_TYPE_FOR_GET_DISENGAGE_ALERT_FROM_TT 10510

#define MSG_TYPE_FOR_RECEIVE_CONNECT_PM_FROM_TT 10000
#define MSG_TYPE_FOR_RECEIVE_DISCONNECT_PM_FROM_TT 10001

#define MSG_TYPE_FOR_RECEIVE_ENABLE_PM_FROM_TT 10002
#define MSG_TYPE_FOR_RECEIVE_DISABLE_PM_FROM_TT 10003

#define MSG_TYPE_FOR_RECEIVE_CONNECT_A_PM_BOOK_FROM_TT 10004
#define MSG_TYPE_FOR_RECEIVE_DISCONNECT_A_PM_BOOK_FROM_TT 10005

#define MSG_TYPE_FOR_RECEIVE_CONNECT_ALL_PM_BOOK_FROM_TT 10006
#define MSG_TYPE_FOR_RECEIVE_DISCONNECT_ALL_PM_BOOK_FROM_TT 10007

#define MSG_TYPE_FOR_RECEIVE_CONNECT_A_PM_ORDER_FROM_TT 10008
#define MSG_TYPE_FOR_RECEIVE_DISCONNECT_A_PM_ORDER_FROM_TT 10009

#define MSG_TYPE_FOR_RECEIVE_CONNECT_ALL_PM_ORDER_FROM_TT 10010
#define MSG_TYPE_FOR_RECEIVE_DISCONNECT_ALL_PM_ORDER_FROM_TT 10011

#define MSG_TYPE_FOR_RECEIVE_CONNECT_CTS_FROM_TT 10012
#define MSG_TYPE_FOR_RECEIVE_DISCONNECT_CTS_FROM_TT 10013

#define MSG_TYPE_FOR_RECEIVE_CONNECT_CQS_FROM_TT 10018
#define MSG_TYPE_FOR_RECEIVE_DISCONNECT_CQS_FROM_TT 10019

#define MSG_TYPE_FOR_RECEIVE_CONNECT_UQDF_FROM_TT 10020
#define MSG_TYPE_FOR_RECEIVE_DISCONNECT_UQDF_FROM_TT 10021

#define MSG_TYPE_FOR_RECEIVE_DISENGAGE_SYMBOL_FROM_TT 10014

#define MSG_TYPE_FOR_RECEIVE_ENGAGE_STUCK_FROM_TT 10015

#define MSG_TYPE_FOR_RECEIVE_CONNECT_UTDF_FROM_TT 10016
#define MSG_TYPE_FOR_RECEIVE_DISCONNECT_UTDF_FROM_TT 10017

#define MSG_TYPE_FOR_RECEIVE_DISABLED_ALERT_REJECT_CODE_FROM_TT 10022


// Message type

// Connect/disconnect to CQS/UQDF
// ts_id: 4 bytes
// CQS/UQDF: 1 byte; 0 for CQS, and 1 for UQDF
#define MSG_TYPE_FOR_REQUEST_CONNECT_TO_BBO_FROM_TT   40
#define MSG_TYPE_FOR_REQUEST_DISCONNECT_TO_BBO_FROM_TT 41

// Enable/disable ISO algorithm
// ts_id: 4 bytes
// ISO_TRADING: 1 byte; ENABLE = 1, DISABLE = 0
#define MSG_TYPE_FOR_REQUEST_ISO_TRADING_FROM_TT    42

// Get/set exchange configuration
// ts_id: 4 bytes
// EXCHANGE_CONF
#define MSG_TYPE_FOR_SET_EXCHANGE_CONF_FROM_TT      2100
#define MSG_TYPE_FOR_SEND_EXCHANGE_CONF_TO_TT     15100

// Get BBO quotes
// ts_id: 4 bytes
// symbol: 8 bytes
#define MSG_TYPE_FOR_GET_BBO_QUOTE_LIST_FROM_TT     2101
#define MSG_TYPE_FOR_SEND_BBO_QUOTE_LIST_TO_TT      15101

// Flush BBO quote for a exchange
// ts_id: 4 bytes
// exchange id: 1 byte
#define MSG_TYPE_FOR_REQUEST_FLUSH_BBO_QUOTES_FROM_TT 2102

// Books for ISO Trading
#define MSG_TYPE_FOR_SET_BOOK_ISO_TRADING_FROM_TT   2103
#define MSG_TYPE_FOR_SEND_BOOK_ISO_TRADING_TO_TT    15103

// For PM Exchange Configuration
#define MSG_TYPE_FOR_SET_PM_EXCHANGE_CONF_FROM_TT     2104
#define MSG_TYPE_FOR_SEND_PM_EXCHANGE_CONF_TO_TT      15102

// For PM BBO Viewer
#define MSG_TYPE_FOR_GET_PM_BBO_QUOTE_LIST_FROM_TT      2105
#define MSG_TYPE_FOR_SEND_PM_BBO_QUOTE_LIST_TO_TT     2106

// For Flush PM BBO Quote for a exchange
#define MSG_TYPE_FOR_REQUEST_FLUSH_PM_BBO_QUOTES_FROM_TT  2107

// For Version Control
#define MSG_TYPE_FOR_LOG_TT_VERSION_CONTROL   2108

//For Notify No Matching Position
#define MSG_TYPE_FOR_SEND_MESSAGE_NOTIFY_NO_MATCHING_POSITION_TO_TT 16012

//For Login Info
#define MSG_TYPE_FOR_RECEIVE_LOGIN_INFO_FROM_TT 3005
#define MSG_TYPE_FOR_SEND_MESSAGE_PERMIT_TRADE_LOGIN_TO_TT 15015
#define MSG_TYPE_FOR_SEND_TRADER_STATUS_INFO_TO_TT 16013
#define MSG_TYPE_FOR_SWITCH_TRADER_ROLE_FROM_TT 2109

// For get number of symbols with quotes
#define MSG_TYPE_FOR_RECEIVE_GET_NUMBER_OF_SYMBOLS_WITH_QUOTES_FROM_TT 3006
#define MSG_TYPE_FOR_SEND_NUMBER_OF_SYMBOLS_WITH_QUOTES_TO_TT 15016

// For alert config
#define MSG_TYPE_FOR_SEND_ALERT_CONFIG_TO_TT 11519
#define MSG_TYPE_FOR_SET_ALERT_CONFIG_FROM_TT 2019

// For sleeping symbol list
#define MSG_TYPE_FOR_SEND_SLEEPING_SYMBOL_LIST_TO_TT 11520
/****************************************************************************
** Function declarations
****************************************************************************/
t_NoTradesDurationMgt NoTradesDurationMgt[MAX_TRADE_SERVER_CONNECTIONS];

int buyingPowerSecondsThreshhold[MAX_TRADE_SERVER_CONNECTIONS];
int PMOrderPerSecond;
t_GTRMgt GTRMgt;

/****************************************************************************
- Function name:  ProcessResetTTParameters
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessResetTTParameters(int ttIndex)
{
  int i, indexOpenOrder, rmInfoIndex;

  for (indexOpenOrder = 0; indexOpenOrder < asOpenOrder.countOpenOrder; indexOpenOrder++)
  {
    if ((asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status == orderStatusIndex.Rejected) ||
      (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status == orderStatusIndex.Closed) ||
      (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status == orderStatusIndex.CanceledByECN) ||
      (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status == orderStatusIndex.CancelRejected) ||
      (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status == orderStatusIndex.CanceledByUser))
    {
      asOpenOrder.orderCollection[indexOpenOrder].isUpdated[ttIndex] = OPEN_ORDER_SENT;
    }
    else
    {
      asOpenOrder.orderCollection[indexOpenOrder].isUpdated[ttIndex] = OPEN_ORDER_UPDATED;
    }
  }

  // Get RM Information from RMList
  for (rmInfoIndex = 0; rmInfoIndex < MAX_SYMBOL_TRADE; rmInfoIndex++)
  {
    for (i = 0; i < MAX_ACCOUNT; i++)
    {
      memset(&RMList.collection[i][rmInfoIndex].oldPositionSent[ttIndex], 0, sizeof(t_PositionBk));
    }
  }

  for (i = 0; i < asCrossCollection.countCross; i++)
  {
    asCrossCollection.crossList[i].isUpdate[ttIndex] = NOT_YET;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendMsgsToTraderTool
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
void *SendMsgsToTraderTool(void *pConnection)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;
  
  isValidTTVersion[connection->index] = 0;
  
  // TraceLog(DEBUG_LEVEL, "userName = '%s', password = '%s'\n", connection->userName, connection->password);
  // TraceLog(DEBUG_LEVEL, "*** ttIndex = %d\n", connection->index);
  
  // TraceLog(DEBUG_LEVEL, "Create completed SendMsgsToTraderTool thread: socket = %d\n", connection->socket);
  
  //Send message to permit trader to TT
  if (SendMessageForPermitTraderLoginToTT(connection->index, connection->userName, SUCCESS) == SUCCESS)
  { 
    //Update trader status to monitoring
    ProcessUpdateTTEventLogToDatabase(connection->userName, ACTIVITY_TYPE_CONNECT_TO_AS, "0");
    
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    
    //Get all trader status
    GetTraderStatusInfo();
    
    int traderIndex;
    int allCurrentStatus = OFFLINE;
    for (traderIndex = 0; traderIndex < traderCollection.numberTrader; traderIndex++)
    {
      if (traderCollection.traderStatusInfo[traderIndex].status != OFFLINE)
      {
        allCurrentStatus = traderCollection.traderStatusInfo[traderIndex].status;
      }
    }
    
    /*
    Checking: If no other clients connected, the trader switch default to active trader
    else the trader switch to monitoring
    */
    if (allCurrentStatus == OFFLINE)
    {
      //Update TT event log
      sprintf(activityDetail, "%d,%d", connection->currentState, ACTIVE);
      ProcessUpdateTTEventLogToDatabase(connection->userName, ACTIVITY_TYPE_SWITCH_ROLE, activityDetail);
      
      ProcessUpdateTraderStatusToDatabase(connection->userName, ACTIVE);
      connection->currentState = ACTIVE;
    }
    else if (connection->currentState == MONITORING)
    {
      //Update TT event log
      sprintf(activityDetail, "%d,%d", connection->currentState, MONITORING);
      ProcessUpdateTTEventLogToDatabase(connection->userName, ACTIVITY_TYPE_SWITCH_ROLE, activityDetail);
    
      ProcessUpdateTraderStatusToDatabase(connection->userName, MONITORING);
      connection->currentState = MONITORING;
    }
    else if (connection->currentState == ACTIVE_ASSISTANT)
    {
      //Update TT event log
      sprintf(activityDetail, "%d,%d", connection->currentState, ACTIVE_ASSISTANT);
      ProcessUpdateTTEventLogToDatabase(connection->userName, ACTIVITY_TYPE_SWITCH_ROLE, activityDetail);
    
      ProcessUpdateTraderStatusToDatabase(connection->userName, ACTIVE_ASSISTANT);
      connection->currentState = ACTIVE_ASSISTANT;
    }
    else if (connection->currentState == ACTIVE)
    {
      //Update TT event log
      sprintf(activityDetail, "%d,%d", connection->currentState, ACTIVE);
      ProcessUpdateTTEventLogToDatabase(connection->userName, ACTIVITY_TYPE_SWITCH_ROLE, activityDetail);
      
      ProcessUpdateTraderStatusToDatabase(connection->userName, ACTIVE);
      connection->currentState = ACTIVE;
    }
  }
  
  while (1)
  {
    if (isValidTTVersion[connection->index] == 1)
    {
      break;
    }
    else if (isValidTTVersion[connection->index] == -1)
    {
      return NULL;
    }
    
    sleep(1);
  }
  
  // Send Trade Servers configuration to Trader Tool
  if (SendTSConfToTT(connection) == ERROR)
  {
    if(connection->statusUpdate == SUCCESS)
    {
      //Update trader status to offline
      ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);
    }

    CloseConnection(connection);

    return NULL;
  }
  
  // Send Trade Servers setting to Trader Tool
  if (SendTSSettingToTT(connection) == ERROR)
  {
    if(connection->statusUpdate == SUCCESS)
    {
      //Update trader status to offline
      ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);
    }

    CloseConnection(connection);

    return NULL;
  }

  // Send AS setting to Trader Tool
  if (SendASSettingToTT(connection) == ERROR)
  {
    if(connection->statusUpdate == SUCCESS)
    {
      //Update trader status to offline
      ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);
    }

    CloseConnection(connection);

    return NULL;
  }
  
  // Send PM setting to Trader Tool
  if (SendPMSettingToTT(connection) == ERROR)
  {
    if(connection->statusUpdate == SUCCESS)
    {
      //Update trader status to offline
      ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);
    }

    CloseConnection(connection);

    return NULL;
  }

  // Send Position Manager configuration to Trader Tool
  if (SendPMConfToTT(connection) == ERROR)
  {
    if(connection->statusUpdate == SUCCESS)
    {
      //Update trader status to offline
      ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);
    }

    CloseConnection(connection);

    return NULL;
  }

  // Send Trader Tools configuration to Trader Tool
  if (SendTTConfToTT(connection) == ERROR)
  {
    if(connection->statusUpdate == SUCCESS)
    {
      //Update trader status to offline
      ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);
    }

    CloseConnection(connection);

    return NULL;
  }

  // Send Alert configuration to Trader Tool
  if (SendAlertConfToTT(connection) == ERROR)
  {
    CloseConnection(connection);

    return NULL;
  }

  // Send Aggregation Server Order configuration to Trader Tool
  if (SendASAllOrderConfToTT(connection) == ERROR)
  {
    if(connection->statusUpdate == SUCCESS)
    {
      //Update trader status to offline
      ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);
    }

    CloseConnection(connection);

    return NULL;
  }

  // Send Aggregation Server global configuration to Trader Tool
  if (SendASGlobalConfToTT(connection) == ERROR)
  {
    if(connection->statusUpdate == SUCCESS)
    {
      //Update trader status to offline
      ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);
    }

    CloseConnection(connection);

    return NULL;
  }

  // Send ignore stock list to Trader Tool
  if (SendIgnoreStockListToTT(connection) == ERROR)
  {
    if(connection->statusUpdate == SUCCESS)
    {
      //Update trader status to offline
      ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);
    }

    CloseConnection(connection);

    return NULL;
  }
  
  //Send ignore stock ranges list to Trader Tool
  if (SendIgnoreStockRangesListToTT(connection) == ERROR)
  {
    if(connection->statusUpdate == SUCCESS)
    {
      //Update trader status to offline
      ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);
    }

    CloseConnection(connection);

    return NULL;
  }

  // Send Book/Order/Global configuration if AS connected to TS
  if (SendAllConfOfTSToTT(connection) == ERROR)
  {
    if(connection->statusUpdate == SUCCESS)
    {
      //Update trader status to offline
      ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);
    }

    CloseConnection(connection);

    return NULL;
  }

  // Send Book/Order/Global/... configuration if AS connected to TS
  if (SendAllConfOfPMToTT(connection) == ERROR)
  {
    if(connection->statusUpdate == SUCCESS)
    {
      //Update trader status to offline
      ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);
    }

    CloseConnection(connection);

    return NULL;
  }
  
  // Send  exchanges conf Of all TS to TT
  if (SendExchangesConfOfAllTSToTT(connection) == ERROR)
  {
    if(connection->statusUpdate == SUCCESS)
    {
      //Update trader status to offline
      ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);
    }

    CloseConnection(connection);

    return NULL;
  }
  
  // Send role book in the ISO algorithm of all TS to TT
  if (SendBookISO_TradingOfAllTSToTT(connection) == ERROR)
  {
    if(connection->statusUpdate == SUCCESS)
    {
      //Update trader status to offline
      ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);
    }

    CloseConnection(connection);

    return NULL;
  }
  
  if (SendVolatileSectionsToTT(connection) == ERROR)
  {
    if(connection->statusUpdate == SUCCESS)
    {
      //Update trader status to offline
      ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);
    }

    CloseConnection(connection);

    return NULL;
  }
  
  if (SendVolatileSymbolListToTT(connection) == ERROR)
  {
    if(connection->statusUpdate == SUCCESS)
    {
      //Update trader status to offline
      ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);
    }

    CloseConnection(connection);

    return NULL;
  }

  if (SendAllVolatileToggleScheduleToTT(connection) == ERROR)
  {
    if(connection->statusUpdate == SUCCESS)
    {
      //Update trader status to offline
      ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);
    }

    CloseConnection(connection);

    return NULL;
  }
  
  if (SendRejectCodeToATT(connection) == ERROR)
  {
    if(connection->statusUpdate == SUCCESS)
    {
      //Update trader status to offline
      ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);
    }

    CloseConnection(connection);

    return NULL;
  }

  if (SendHaltStockListToTT(connection) == ERROR)
  {
    if(connection->statusUpdate == SUCCESS)
    {
      //Update trader status to offline
      ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);
    }

    CloseConnection(connection);

    return NULL;
  }
  
  while (1)
  {
    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1))
    {
      if(connection->statusUpdate == SUCCESS)
      {
        //Update trader status to offline
        ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);
      }

      return NULL;
    }

    // Check to update if there is a switch between FAST vs NORMAL Market Mode
    UpdateMarketMode();
    
    // Send AS current status to TT
    if (SendASCurrentStatusToTT(connection) == ERROR)
    {
      if(connection->statusUpdate == SUCCESS)
      {
        //Update trader status to offline
        ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);
      }

      CloseConnection(connection);

      return NULL;
    }

    // Send PM current status to TT
    if (SendPMCurrentStatusToTT(connection) == ERROR)
    {
      if(connection->statusUpdate == SUCCESS)
      {
        //Update trader status to offline
        ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);
      }

      CloseConnection(connection);

      return NULL;
    }

    // Send TS current status to Trader Tool
    if (SendTSCurrentStatusToTT(connection) == ERROR)
    {
      if(connection->statusUpdate == SUCCESS)
      {
        //Update trader status to offline
        ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);
      }

      CloseConnection(connection);

      return NULL;
    }   
    
    // Send open order to TT
    if (SendASOpenOrderToTT(connection) == ERROR)
    {
      if(connection->statusUpdate == SUCCESS)
      {
        //Update trader status to offline
        ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);
      }

      CloseConnection(connection);

      return NULL;
    }   
    
    // Send open position to TT
    if (SendOpenPositionToTT(connection) == ERROR)
    {
      if(connection->statusUpdate == SUCCESS)
      {
        //Update trader status to offline
        ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);
      }

      CloseConnection(connection);

      return NULL;
    }
    
    // Send profit loss summary to TT
    if (SendProfitLossSummaryToTT(connection) == ERROR)
    {
      if(connection->statusUpdate == SUCCESS)
      {
        //Update trader status to offline
        ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);
      }

      CloseConnection(connection);

      return NULL;
    }

    if (SendActiveVolatileSymbolsToTT(connection, GetCurrentVolatileSession()) == ERROR)
    {
      if(connection->statusUpdate == SUCCESS)
      {
        //Update trader status to offline
        ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);
      }

      CloseConnection(connection);

      return NULL;
    }
    
    if (SendAllVolatileToggleStatusToTT(connection) == ERROR)
    {
      if(connection->statusUpdate == SUCCESS)
      {
        //Update trader status to offline
        ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);
      }

      CloseConnection(connection);

      return NULL;
    }
    
    if (SendSleepingSymbolListtoTT(connection) == ERROR)
    {
      if(connection->statusUpdate == SUCCESS)
      {
        //Update trader status to offline
        ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);
      }

      CloseConnection(connection);

      return NULL;
    }

    // TT send delay in in microseconds
    usleep(TT_SEND_DELAY);
  }

  return NULL;
}

/****************************************************************************
- Function name:  SendTSConfToTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendTSConfToTT(void *pConnection)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Send TS conf to TT ...\n");

  t_TTMessage ttMessage;

  // Build Trade Servers configuration message to send Trader Tool
  BuildTTMessageForTradeServersConf(&ttMessage);

  // Send message to Trader Tool
  return SendTTMessage(connection, &ttMessage);
}

/****************************************************************************
- Function name:  SendTSConfToAllTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendTSConfToAllTT(void)
{
  int ttIndex;
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;

  // Build Trade Servers configuration message to send Trader Tool
  BuildTTMessageForTradeServersConf(&ttMessage);

  // Send message to all Trader Tool connected to AS
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];
    
    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_TRADER_TOOL)
    {
      continue;
    }

    // Send message to Trader Tool
    SendTTMessage(connection, &ttMessage);
  }

  return SUCCESS;
}
/****************************************************************************
- Function name:  SendAlertConfToTT
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int SendAlertConfToTT(void *pConnection)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Send Alert conf to TT ...\n");

  t_TTMessage ttMessage;

  // Build Trader Tool configuration message to send Trader Tool
  BuildTTMessageForAlertConfig(&ttMessage);

  // Send message to Trader Tool
  return SendTTMessage(connection, &ttMessage);
}

/****************************************************************************
- Function name:  SendTTConfToTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendTTConfToTT(void *pConnection)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Send TT conf to TT ...\n");

  t_TTMessage ttMessage;

  // Build Trader Tool configuration message to send Trader Tool
  BuildTTMessageForTTConf(&ttMessage);

  // Send message to Trader Tool
  return SendTTMessage(connection, &ttMessage);
}

/****************************************************************************
- Function name:  SendTTConfToAllTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendTTConfToAllTT(void)
{
  int ttIndex;
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;

  // Build Trader Tool configuration message to send Trader Tool
  BuildTTMessageForTTConf(&ttMessage);

  // Send message to all Trader Tool connected to AS
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];
    
    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_TRADER_TOOL)
    {
      continue;
    }

    // Send message to Trader Tool
    SendTTMessage(connection, &ttMessage);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendASAllOrderConfToTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendASAllOrderConfToTT(void *pConnection)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  int ECNIndex;

  for (ECNIndex = 0; ECNIndex < AS_MAX_ORDER_CONNECTIONS; ECNIndex++)
  {
    // Send AS Order configuration message to send Trader Tool
    if (SendASOrderConfToTT(connection, ECNIndex) == ERROR)
    {
      return ERROR;
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendASOrderConfToTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendASOrderConfToTT(void *pConnection, int ECNIndex)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Send AS order conf to TT, ECNIndex = %d ...\n", ECNIndex);

  t_TTMessage ttMessage;
  
  // Build AS Order configuration message to send Trader Tool
  BuildTTMessageForASOrderConf(&ttMessage, ECNIndex);

  // Send message to Trader Tool
  return SendTTMessage(connection, &ttMessage);
}

/****************************************************************************
- Function name:  SendASOrderConfToAllTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendASOrderConfToAllTT(int ECNIndex)
{
  int ttIndex;
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;

  // Build AS Order configuration message to send Trader Tool
  BuildTTMessageForASOrderConf(&ttMessage, ECNIndex);

  // Send message to all Trader Tool connected to AS
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];
    
    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_TRADER_TOOL)
    {
      continue;
    }

    // Send message to Trader Tool
    SendTTMessage(connection, &ttMessage);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendASGlobalConfToTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendASGlobalConfToTT(void *pConnection)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Send AS global conf to TT ...\n");

  t_TTMessage ttMessage;

  // Build Aggregation Sever global configuration message to send Trader Tool
  BuildTTMessageForASGlobalConf(&ttMessage);

  // Send message to Trader Tool
  return SendTTMessage(connection, &ttMessage);
}

/****************************************************************************
- Function name:  SendASGlobalConfToAllTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendASGlobalConfToAllTT(void)
{
  int ttIndex;
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;

  // Build Aggregation Sever global configuration message to send Trader Tool
  BuildTTMessageForASGlobalConf(&ttMessage);

  // Send message to all Trader Tool connected to AS
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];
    
    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_TRADER_TOOL)
    {
      continue;
    }

    // Send message to Trader Tool
    SendTTMessage(connection, &ttMessage);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendOpenOrderToTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendASOpenOrderToTT(void *pConnection)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  int ttIndex = connection->index;
  t_TTMessage ttMessage;

  int countOpenOrderSend = 0;
  int indexOpenOrder, indexOrderDetail, countOrderDetail, lenMsgBackup;

  t_OpenOrderInfo orderSummary;
  t_OrderDetail orderDetail;
  
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;
  
  // Initialize message
  ttMessage.msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_OPEN_ORDER_TO_TT;
  ttMessage.msgContent[0] = shortConvert.c[0];
  ttMessage.msgContent[1] = shortConvert.c[1];
  ttMessage.msgLen += 2;

  // Update index of body length
  ttMessage.msgLen += 4;

  // Message body = blockLength (4 bytes) + blockContent (blockLength bytes)

  // Update index of block length
  ttMessage.msgLen += 4;

  // blockContent = num open order + N * (open order)
  
  // num open order
  ttMessage.msgLen += 4;

  /*  Filter PM messages to anti-flood TT
    
    // The following will be sent to TT:
    orderStatusIndex.Sent = 0;
    orderStatusIndex.Live = 1;
    orderStatusIndex.Rejected = 2;
    orderStatusIndex.BrokenTrade = 4
    orderStatusIndex.CXLSent = 6;
    orderStatusIndex.CancelPending = 9;
    left shares < shares
        
    // The following will be sent limited, only newest will be sent:
    orderStatusIndex.CanceledByECN = 5;
    orderStatusIndex.CancelRejected = 8;
    orderStatusIndex.CanceledByUser = 10;
  */
  
  int numPMOrdersWillSend = 1;
  int tempStatus;
  for (indexOpenOrder = 0; indexOpenOrder < asOpenOrder.countOpenOrder; indexOpenOrder++)
  {
    if (asOpenOrder.orderCollection[indexOpenOrder].isUpdated[ttIndex] == OPEN_ORDER_UPDATED)
    {

      // The following commented code will filter to ignore most PM canceled messages
      if (PMtoTTCancelLimit != -1)
      {
        // Check if it is PM message
        if ( (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.clOrdId >= 500000) &&
           (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.tsId == MANUAL_ORDER) )
        {
          tempStatus = asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status;
          if (
            ((tempStatus == orderStatusIndex.CanceledByUser) || (tempStatus == orderStatusIndex.CanceledByECN)) &&
            (asOpenOrder.orderCollection[indexOpenOrder].countSent[ttIndex] == 0)
            )
          {
            // Send up to the limit!
            if (numPMOrdersWillSend <= PMtoTTCancelLimit)
            {
              // Send
              numPMOrdersWillSend++;
            }
            else
            {
              //Do not send
              asOpenOrder.orderCollection[indexOpenOrder].isUpdated[ttIndex] = OPEN_ORDER_SENT;
              continue;
            }
          }
        }
      }
      
      
      // Check for fast market mode
      // If in the Fast Market Mode, Order History will be sent limited, Only Live orders will be sent
      if (FastMarketMgmt.mode == FAST_MARKET_MODE)
      {
        tempStatus = asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status;
        if (
          (tempStatus == orderStatusIndex.CanceledByECN)    ||
          (tempStatus == orderStatusIndex.Closed)       ||
          (tempStatus == orderStatusIndex.Rejected)       ||
          (tempStatus == orderStatusIndex.CanceledByUser)   ||
          (tempStatus == orderStatusIndex.CancelRejected) ||
          (asOpenOrder.orderCollection[indexOpenOrder].isBroken != BROKEN_TRADE_NONE)
          )
        {
          // These are Closed orders:
          // We have to send to TT when this order is currently displayed on TT --> so it will be removed from TT!
          // We will not send to TT if this order has never been sent to TT before!
          // "countSent" helps us to know whether this has been sent to TT before
          if (asOpenOrder.orderCollection[indexOpenOrder].countSent[ttIndex] == 0)
          {
            // We won't send in this case
            asOpenOrder.orderCollection[indexOpenOrder].isUpdated[ttIndex] = OPEN_ORDER_SENT;
            continue;
          }
        }
      }
      
      if (countOpenOrderSend == MAX_OPEN_ORDER_SEND)
      {
        break;
      }
      
      //Here are messages from TS's, AS, and PM (filtered)
      
      asOpenOrder.orderCollection[indexOpenOrder].isUpdated[ttIndex] = OPEN_ORDER_SENT;
      asOpenOrder.orderCollection[indexOpenOrder].countSent[ttIndex]++;
      
      // Increase count open order will send
      countOpenOrderSend ++;
      
      // order summary
      memcpy((unsigned char *)&orderSummary, (unsigned char *)&asOpenOrder.orderCollection[indexOpenOrder].orderSummary, sizeof(t_OpenOrderInfo));
      
      //convert to TT compatible format
      if (orderSummary.clOrdId < 500000)  //AS order
      {
        orderSummary.ECNId = AS_TO_TT_ORDER_TYPE_MAPPING[orderSummary.ECNId - 100];
      }
      else if (orderSummary.clOrdId < 1000000)
      {
        orderSummary.ECNId = PM_TO_TT_ORDER_TYPE_MAPPING[orderSummary.ECNId - 100];
      }
      else
      {
        orderSummary.ECNId = TS_TO_TT_ORDER_TYPE_MAPPING[orderSummary.ECNId - 100];
      }
      
      if (asOpenOrder.orderCollection[indexOpenOrder].isBroken != BROKEN_TRADE_NONE)
      {
        orderSummary.status = orderStatusIndex.BrokenTrade; 
      }
      
      memcpy(&ttMessage.msgContent[ttMessage.msgLen], (unsigned char *)&orderSummary, sizeof(t_OpenOrderInfo));
      ttMessage.msgLen += sizeof(t_OpenOrderInfo);

      //TraceLog(DEBUG_LEVEL, "clOrdId = %d, tradeType = %d\n", asOpenOrder.orderCollection[indexOpenOrder].orderSummary.clOrdId, asOpenOrder.orderCollection[indexOpenOrder].orderSummary.tradeType);

      // number of orders detail
      lenMsgBackup = ttMessage.msgLen;
      ttMessage.msgLen += 4;      

      countOrderDetail = 0;
      for (indexOrderDetail = 0; indexOrderDetail < asOpenOrder.orderCollection[indexOpenOrder].countOrderDetail; indexOrderDetail++)
      {
        if (asOrderDetail.orderDetailList[asOpenOrder.orderCollection[indexOpenOrder].indexOrderDetailList[indexOrderDetail]].isBroken != BROKEN_TRADE_REMOVED)
        {
          countOrderDetail ++;
          
          if (asOrderDetail.orderDetailList[asOpenOrder.orderCollection[indexOpenOrder].indexOrderDetailList[indexOrderDetail]].isBroken == BROKEN_TRADE_PRICE_CHANGED)
          {
            memcpy(&orderDetail, &asOrderDetail.orderDetailList[asOpenOrder.orderCollection[indexOpenOrder].indexOrderDetailList[indexOrderDetail]], sizeof(t_OrderDetail));
            
            // Process change price
            ProcessChangePrice(&orderDetail, asOpenOrder.orderCollection[indexOpenOrder].orderSummary.ECNId);

            // length of order detail
            memcpy(&ttMessage.msgContent[ttMessage.msgLen], (unsigned char *)&orderDetail.length, 4);
            ttMessage.msgLen += 4;

            // timestamp
            memcpy(&ttMessage.msgContent[ttMessage.msgLen], (unsigned char *)&orderDetail.timestamp, MAX_TIMESTAMP);
            ttMessage.msgLen += MAX_TIMESTAMP;

            // order detail
            memcpy(&ttMessage.msgContent[ttMessage.msgLen], (unsigned char *)&orderDetail.msgContent, orderDetail.length);
            ttMessage.msgLen += orderDetail.length;
          }
          else
          {
            // length of order detail
            memcpy(&ttMessage.msgContent[ttMessage.msgLen], (unsigned char *)&asOrderDetail.orderDetailList[asOpenOrder.orderCollection[indexOpenOrder].indexOrderDetailList[indexOrderDetail]].length, 4);
            ttMessage.msgLen += 4;

            // timestamp
            memcpy(&ttMessage.msgContent[ttMessage.msgLen], (unsigned char *)&asOrderDetail.orderDetailList[asOpenOrder.orderCollection[indexOpenOrder].indexOrderDetailList[indexOrderDetail]].timestamp, MAX_TIMESTAMP);
            ttMessage.msgLen += MAX_TIMESTAMP;

            // order detail
            memcpy(&ttMessage.msgContent[ttMessage.msgLen], (unsigned char *)&asOrderDetail.orderDetailList[asOpenOrder.orderCollection[indexOpenOrder].indexOrderDetailList[indexOrderDetail]].msgContent, asOrderDetail.orderDetailList[asOpenOrder.orderCollection[indexOpenOrder].indexOrderDetailList[indexOrderDetail]].length);
            ttMessage.msgLen += asOrderDetail.orderDetailList[asOpenOrder.orderCollection[indexOpenOrder].indexOrderDetailList[indexOrderDetail]].length;
          }
        }
      }

      // Update number of orders detail
      memcpy(&ttMessage.msgContent[lenMsgBackup], (unsigned char *)&countOrderDetail, 4);
    }
  }
  
  if (countOpenOrderSend == 0)
  {
    return SUCCESS;
  }

  //TraceLog(DEBUG_LEVEL, "Send open orders: countOrderPlacedSend = %d, totalOrderPlaced = %d\n", countOpenOrderSend, asOpenOrder.countOpenOrder);

  // num open order field
  intConvert.value = countOpenOrderSend;
  ttMessage.msgContent[10] = intConvert.c[0];
  ttMessage.msgContent[11] = intConvert.c[1];
  ttMessage.msgContent[12] = intConvert.c[2];
  ttMessage.msgContent[13] = intConvert.c[3];

  // Block length field
  intConvert.value = ttMessage.msgLen - MSG_HEADER_LEN;
  ttMessage.msgContent[6] = intConvert.c[0];
  ttMessage.msgContent[7] = intConvert.c[1];
  ttMessage.msgContent[8] = intConvert.c[2];
  ttMessage.msgContent[9] = intConvert.c[3];
  
  // Body length field
  intConvert.value = ttMessage.msgLen - MSG_HEADER_LEN;
  ttMessage.msgContent[2] = intConvert.c[0];
  ttMessage.msgContent[3] = intConvert.c[1];
  ttMessage.msgContent[4] = intConvert.c[2];
  ttMessage.msgContent[5] = intConvert.c[3];

  // Send message to Trader Tool
  return SendTTMessage(connection, &ttMessage);
}

/****************************************************************************
- Function name:  SendIgnoreStockListToAllTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendIgnoreStockListToAllTT(void)
{
  int ttIndex;
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;

  // Build ignore stock list message to send Trader Tool
  BuildTTMessageForIgnoreStockList(&ttMessage);

  // Send message to all Trader Tool connected to AS
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];
    
    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_TRADER_TOOL)
    {
      continue;
    }

    // Send message to Trader Tool
    SendTTMessage(connection, &ttMessage);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendVolatileSectionsToAllTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendVolatileSectionsToAllTT()
{
  int ttIndex;
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;

  // Build ignore stock list message to send Trader Tool
  BuildTTMessageForVolatileSections(&ttMessage);

  // Send message to all Trader Tool connected to AS
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];
    
    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_TRADER_TOOL)
    {
      continue;
    }

    // Send message to Trader Tool
    SendTTMessage(connection, &ttMessage);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendAllVolatileToggleScheduleToAllTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendAllVolatileToggleScheduleToAllTT()
{
  int ttIndex;
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;

  // Build ignore stock list message to send Trader Tool
  BuildTTMessageForAllVolatileToggle(&ttMessage);

  // Send message to all Trader Tool connected to AS
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];
    
    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_TRADER_TOOL)
    {
      continue;
    }

    // Send message to Trader Tool
    SendTTMessage(connection, &ttMessage);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendVolatileSectionsToTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendVolatileSectionsToTT(t_ConnectionStatus *connection)
{
  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  t_TTMessage ttMessage;

  BuildTTMessageForVolatileSections(&ttMessage);
  
  // Send message to Trader Tool
  return SendTTMessage(connection, &ttMessage);
}

/****************************************************************************
- Function name:  SendAllVolatileToggleScheduleToTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendAllVolatileToggleScheduleToTT(t_ConnectionStatus *connection)
{
  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  t_TTMessage ttMessage;

  BuildTTMessageForAllVolatileToggle(&ttMessage);
  
  // Send message to Trader Tool
  return SendTTMessage(connection, &ttMessage);
}

/****************************************************************************
- Function name:  SendVolatileSymbolListToAllTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendVolatileSymbolListToAllTT()
{
  int ttIndex;
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;

  // Build ignore stock list message to send Trader Tool
  BuildTTMessageForVolatileSymbolList(&ttMessage);

  // Send message to all Trader Tool connected to AS
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];
    
    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_TRADER_TOOL)
    {
      continue;
    }

    // Send message to Trader Tool
    SendTTMessage(connection, &ttMessage);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendVolatileSymbolListToTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendVolatileSymbolListToTT(void *pConnection)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  t_TTMessage ttMessage;

  BuildTTMessageForVolatileSymbolList(&ttMessage);
  
  // Send message to Trader Tool
  return SendTTMessage(connection, &ttMessage);
}
/****************************************************************************
- Function name:  SendIgnoreStockRangesListToAllTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendIgnoreStockRangesListToAllTT(void)
{
  int ttIndex;
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;

  // Build ignore stock list message to send Trader Tool
  BuildTTMessageForIgnoreStockRangesList(&ttMessage);

  // Send message to all Trader Tool connected to AS
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];
    
    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_TRADER_TOOL)
    {
      continue;
    }

    // Send message to Trader Tool
    SendTTMessage(connection, &ttMessage);
  }
  
  return SUCCESS;
}


/****************************************************************************
- Function name:  SendIgnoreStockListToTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendIgnoreStockListToTT(void *pConnection)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Send ignored stock list to TT ...\n");

  t_TTMessage ttMessage;

  // Build ignore stock list message to send Trader Tool
  BuildTTMessageForIgnoreStockList(&ttMessage);

  // Send message to Trader Tool
  return SendTTMessage(connection, &ttMessage);
}

/****************************************************************************
- Function name:  SendIgnoreStockRangesListToTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendIgnoreStockRangesListToTT(void *pConnection)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Send ignored ranges list to TT ...\n");

  t_TTMessage ttMessage;

  // Build a range ignore stock list message to send Trader Tool
  BuildTTMessageForIgnoreStockRangesList(&ttMessage);

  // Send message to Trader Tool
  return SendTTMessage(connection, &ttMessage);
}
/****************************************************************************
- Function name:  SendAllConfOfTSToTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendAllConfOfTSToTT(void *pConnection)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  int tsIndex, ECNIndex;

  for (tsIndex = 0; tsIndex < tradeServersInfo.maxConnections; tsIndex++)
  {
    // Check connection between AS and TS
    if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
    {
      continue;
    }

    // Send Book configuration
    for (ECNIndex = 0; ECNIndex < TS_MAX_BOOK_CONNECTIONS; ECNIndex++)
    {
      if (SendTSBookConfToTT(connection, tsIndex, ECNIndex) == ERROR)
      {
        return ERROR;
      }
    }

    // Send Order configuration
    for (ECNIndex = 0; ECNIndex < TS_MAX_ORDER_CONNECTIONS; ECNIndex++)
    {
      if (SendTSOrderConfToTT(connection, tsIndex, ECNIndex) == ERROR)
      {
        return ERROR;
      }
    }
    
    // Send global configuration
    if (SendTSGlobalConfToTT(connection, tsIndex) == ERROR)
    {
      return ERROR;
    }
    
    // Send minimum spread configuration
    if (SendTSMinSpreadConfToTT(connection, tsIndex) == ERROR)
    {
      return ERROR;
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendTSBookConfToTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendTSBookConfToTT(void *pConnection, int tsIndex, int ECNIndex)
{ 
  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Send TS book conf to TT, tsIndex = %d, ECNIndex = %d ...\n", tsIndex, ECNIndex);

  t_TTMessage ttMessage;

  // Build TS Book configuration message to send Trader Tool
  BuildTTMessageForTSBookConf(&ttMessage, tsIndex, ECNIndex);

  // Send message to Trader Tool
  return SendTTMessage(connection, &ttMessage);
}

/****************************************************************************
- Function name:  SendTSBookConfToAllTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendTSBookConfToAllTT(int tsIndex, int ECNIndex)
{
  int ttIndex;
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;

  // Build TS Book configuration message to send Trader Tool
  BuildTTMessageForTSBookConf(&ttMessage, tsIndex, ECNIndex);

  // Send message to all Trader Tool connected to AS
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];
    
    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_TRADER_TOOL)
    {
      continue;
    }

    // Send message to Trader Tool
    SendTTMessage(connection, &ttMessage);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendTSOrderConfToTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendTSOrderConfToTT(void *pConnection, int tsIndex, int ECNIndex)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Send TS order conf to TT, tsIndex = %d, ECNIndex = %d ...\n", tsIndex, ECNIndex);

  t_TTMessage ttMessage;

  // Build TS Order configuration message to send Trader Tool
  BuildTTMessageForTSOrderConf(&ttMessage, tsIndex, ECNIndex);  

  // Send message to Trader Tool
  return SendTTMessage(connection, &ttMessage);
}

/****************************************************************************
- Function name:  SendTSOrderConfToAllTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendTSOrderConfToAllTT(int tsIndex, int ECNIndex)
{
  int ttIndex;
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;

  // Build TS Order configuration message to send Trader Tool
  BuildTTMessageForTSOrderConf(&ttMessage, tsIndex, ECNIndex);

  // Send message to all Trader Tool connected to AS
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];

    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_TRADER_TOOL)
    {
      continue;
    }   

    // Send message to Trader Tool
    SendTTMessage(connection, &ttMessage);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendTSGlobalConfToTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendTSGlobalConfToTT(void *pConnection, int tsIndex)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Send TS global conf to TT, tsIndex = %d ...\n", tsIndex);

  t_TTMessage ttMessage;

  // Build TS global configuration message to send Trader Tool
  BuildTTMessageForTSGlobalConf(&ttMessage, tsIndex);
  
  // Send message to Trader Tool
  return SendTTMessage(connection, &ttMessage);
}

/****************************************************************************
- Function name:  SendTSGlobalConfToAllTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendTSGlobalConfToAllTT(int tsIndex)
{
  int ttIndex;
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;

  // Build TS global configuration message to send Trader Tool
  BuildTTMessageForTSGlobalConf(&ttMessage, tsIndex);

  // Send message to all Trader Tool connected to AS
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];

    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_TRADER_TOOL)
    {
      continue;
    }

    // Send message to Trader Tool
    SendTTMessage(connection, &ttMessage);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendTSMinSpreadConfToTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendTSMinSpreadConfToTT(void *pConnection, int tsIndex)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Send TS minimum spread conf to TT, tsIndex = %d ...\n", tsIndex);

  t_TTMessage ttMessage;

  // Build TS minimum spread configuration message to send Trader Tool
  BuildTTMessageForTSMinSpreadConf(&ttMessage, tsIndex);
  
  // Send message to Trader Tool
  return SendTTMessage(connection, &ttMessage);
}

/****************************************************************************
- Function name:  SendTSMinSpreadConfToAllTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendTSMinSpreadConfToAllTT(int tsIndex)
{
  int ttIndex;
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;

  // Build TS minimum spread configuration message to send Trader Tool
  BuildTTMessageForTSMinSpreadConf(&ttMessage, tsIndex);

  // Send message to all Trader Tool connected to AS
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];

    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_TRADER_TOOL)
    {
      continue;
    }   

    // Send message to Trader Tool
    SendTTMessage(connection, &ttMessage);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendMarketModeUpdateToAllTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendMarketModeUpdateToAllTT(const char mode)
{
  int ttIndex;
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;

  // Build Trade Servers configuration message to send Trader Tool
  BuildTTMessageForMarketModeUpdate(&ttMessage, mode);

  // Send message to all Trader Tool connected to AS
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];
    
    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_TRADER_TOOL)
    {
      continue;
    }

    // Send message to Trader Tool
    SendTTMessage(connection, &ttMessage);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendTSCurrentStatusToTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendTSCurrentStatusToTT(void *pConnection)
{
  //TraceLog(DEBUG_LEVEL, "Send TS current status to TT ...\n");

  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  int tsIndex;
  t_TTMessage ttMessage;

  for (tsIndex = 0; tsIndex < tradeServersInfo.maxConnections; tsIndex++)
  {
    // Build TS current status message to send Trader Tool
    BuildTTMessageForTSCurrentStatus(&ttMessage, tsIndex);

    // Send message to Trader Tool
    if (SendTTMessage(connection, &ttMessage) == ERROR)
    {
      return ERROR;
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendASCurrentStatusToTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendASCurrentStatusToTT(void *pConnection)
{
  //TraceLog(DEBUG_LEVEL, "Send AS current status to TT ...\n");

  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  t_TTMessage ttMessage;
  
  // Build AS current status message to send Trader Tool 
  BuildTTMessageForASCurrentStatus(&ttMessage);

  // Send message to Trader Tool
  return SendTTMessage(connection, &ttMessage);
}

/****************************************************************************
- Function name:  SendSymbolStatusToTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendSymbolStatusToTT(int tsIndex, int ttIndex, void *message)
{
  t_TTMessage *tsMessage = (t_TTMessage *)message;
  t_ConnectionStatus *connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];

  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  t_TTMessage ttMessage;

  // Build stock symbol status message to send Trader Tool
  BuildTTMessageForSymbolStatus(&ttMessage, tsIndex, tsMessage);

  // Send message to Trader Tool
  return SendTTMessage(connection, &ttMessage);
}

/****************************************************************************
- Function name:  SendOpenPositionToTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendOpenPositionToTT(void *pConnection)
{
  //TraceLog(DEBUG_LEVEL, "Send open position to TT ...\n");

  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }
  
  int countRMInfo, ttIndex = connection->index;
  t_TTMessage ttMessage;

  // Build Open status message to send Trader Tool
  countRMInfo = BuildTTMessageForOpenPosition(&ttMessage, ttIndex);

  if (countRMInfo == 0)
  {
    return SUCCESS;
  }

  // Send message to Trader Tool
  return SendTTMessage(connection, &ttMessage);
}

/****************************************************************************
- Function name:  SendProfitLossSummaryToTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendProfitLossSummaryToTT(void *pConnection)
{
  //TraceLog(DEBUG_LEVEL, "Send profit/loss information to TT ...\n");

  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }
  
  t_TTMessage ttMessage;

  // Build profit loss message to send Trader Tool
  BuildTTMessageForProfitLossSummary(&ttMessage);

  // Send message to Trader Tool
  return SendTTMessage(connection, &ttMessage);
}

/****************************************************************************
- Function name:  ReceiveMsgsFromTraderTool
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
void *ReceiveMsgsFromTraderTool(void *pConnection)
{
  t_TTMessage ttMessage;

  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  //TraceLog(DEBUG_LEVEL, "Create completed ReceiveMsgsFromTraderTool thread: socket = %d\n", connection->socket);

  while (1) 
  {   
    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1))
    {
      break;
    }

    // Receive a message from Trader Tool
    if (ReceiveTTMessage(connection, &ttMessage) != ERROR)
    {
      // Message type
      int msgType = GetShortNumber(ttMessage.msgContent);

      //TraceLog(DEBUG_LEVEL, "ReceiveTTMessage: %d, %d\n", msgType, ttMessage.msgLen);

      switch (msgType)
      {
        // Receive heartbeat message
        case MSG_TYPE_FOR_RECEIVE_HEARTBEAT_TO_TT:
          break;

        // Receive request enable trading on a ECN order of a TS
        case MSG_TYPE_FOR_RECEIVE_ENABLE_TRADING_A_ORDER_A_TS_FROM_TT:
          ProcessTTMessageForEnableTradingAOrderATSFromTT(&ttMessage, connection->index);
          break;

        // Receive request enable trading on a ECN order of a TS
        case MSG_TYPE_FOR_RECEIVE_DISABLE_TRADING_A_ORDER_A_TS_FROM_TT:
          ProcessTTMessageForDisableTradingAOrderATSFromTT(&ttMessage, connection->index);
          break;

        // Receive request enable trading on all ECN order of a TS
        case MSG_TYPE_FOR_RECEIVE_ENABLE_TRADING_ALL_ORDER_A_TS_FROM_TT:
          ProcessTTMessageForEnableTradingAllOrderATSFromTT(&ttMessage, connection->index);
          break;

        // Receive request disable trading on all ECN order of a TS
        case MSG_TYPE_FOR_RECEIVE_DISABLE_TRADING_ALL_ORDER_A_TS_FROM_TT:
          ProcessTTMessageForDisableTradingAllOrderATSFromTT(&ttMessage, connection->index);
          break;

        // Receive request enable trading on all ECN order of all TS
        case MSG_TYPE_FOR_RECEIVE_ENABLE_TRADING_ALL_ORDER_ALL_TS_FROM_TT:
          ProcessTTMessageForEnableTradingAllOrderAllTSFromTT(&ttMessage, connection->index);
          break;

        // Receive request disable trading on all ECN order of all TS
        case MSG_TYPE_FOR_RECEIVE_DISABLE_TRADING_ALL_ORDER_ALL_TS_FROM_TT:
          ProcessTTMessageForDisableTradingAllOrderAllTSFromTT(&ttMessage, connection->index);
          break;

        // Receive send new order
        case MSG_TYPE_FOR_RECEIVE_NEW_ORDER_FROM_TT:
          ProcessTTMessageForSendNewOrderFromTT(&ttMessage, connection->index);
          break;

        // Receive send cancel request
        case MSG_TYPE_FOR_RECEIVE_CANCEL_REQUEST_FROM_TT:
          ProcessTTMessageForSendCancelRequestFromTT(&ttMessage);
          break;

        // Receive set AS current status
        case MSG_TYPE_FOR_RESET_AS_CURRENT_STATUS_FROM_TT:
          ProcessTTMessageForResetASCurrentStatusFromTT(&ttMessage);
          break;

        // Receive set TS current status
        case MSG_TYPE_FOR_RESET_TS_CURRENT_STATUS_FROM_TT:
          ProcessTTMessageForResetTSCurrentStatusFromTT(&ttMessage);
          break;

        // Receive request AS connect to ECN order
        case MSG_TYPE_FOR_RECEIVE_AS_CONNECT_ORDER_FROM_TT:
          ProcessTTMessageForASConnectOrderFromTT(&ttMessage, connection->index);
          break;

        // Receive request AS disconnect to ECN order
        case MSG_TYPE_FOR_RECEIVE_AS_DISCONNECT_ORDER_FROM_TT:
          ProcessTTMessageForASDisconnectOrderFromTT(&ttMessage, connection->index);
          break;

        // Receive request AS connect to all ECN order
        case MSG_TYPE_FOR_RECEIVE_AS_CONNECT_ALL_ORDER_FROM_TT:
          ProcessTTMessageForASConnectAllOrderFromTT(&ttMessage, connection->index);
          break;

        // Receive request AS disconnect to all ECN order
        case MSG_TYPE_FOR_RECEIVE_AS_DISCONNECT_ALL_ORDER_FROM_TT:
          ProcessTTMessageForASDisconnectAllOrderFromTT(&ttMessage, connection->index);
          break;

        // Receive request get symbol status
        case MSG_TYPE_FOR_RECEIVE_SYMBOL_STATUS_FROM_TT:
          ProcessTTMessageForGetSymbolStatusFromTT(&ttMessage, connection->index);
          break;

        // Receive Long/Short from TT
        case MSG_TYPE_FOR_RECEIVE_OPEN_POSITION_FROM_TT: 
          ProcessTTMessageForLongShort(&ttMessage);
          break;

        // Receive request TS flush a symbol
        case MSG_TYPE_FOR_RECEIVE_FLUSH_A_SYMBOL_FROM_TT:
          ProcessTTMessageForFulshASymbolFromTT(&ttMessage);
          break;
          
        // Receive request PM flush a symbol
        case MSG_TYPE_FOR_RECEIVE_FLUSH_PM_A_SYMBOL_FROM_TT:
          ProcessTTMessageForFulshPMASymbolFromTT(&ttMessage);
          break;
          
        // Receive request TS flush all symbol
        case MSG_TYPE_FOR_RECEIVE_FLUSH_ALL_SYMBOL_FROM_TT:
          ProcessTTMessageForFulshAllSymbolFromTT(&ttMessage);
          break;
          
        // Receive request PM flush all symbol
        case MSG_TYPE_FOR_RECEIVE_FLUSH_PM_ALL_SYMBOL_FROM_TT:
          ProcessTTMessageForFulshPMAllSymbolFromTT(&ttMessage);
          break;

        // Receive request TS connect to a ECN Book
        case MSG_TYPE_FOR_RECEIVE_CONNECT_BOOK_FROM_TT:
          ProcessTTMessageForConnectBookFromTT(&ttMessage, connection->index);
          break;

        // Receive request TS disconnect to a ECN Book
        case MSG_TYPE_FOR_RECEIVE_DISCONNECT_BOOK_FROM_TT:
          ProcessTTMessageForDisconnectBookFromTT(&ttMessage, connection->index);
          break;          

        // Receive request TS connect to all ECN Book
        case MSG_TYPE_FOR_RECEIVE_CONNECT_ALL_BOOK_FROM_TT:
          ProcessTTMessageForConnectAllBookFromTT(&ttMessage, connection->index);
          break;

        // Receive request TS disconnect to all ECN Book
        case MSG_TYPE_FOR_RECEIVE_DISCONNECT_ALL_BOOK_FROM_TT:
          ProcessTTMessageForDisconnectAllBookFromTT(&ttMessage, connection->index);
          break;

        // Receive request TS connect to a ECN Order
        case MSG_TYPE_FOR_RECEIVE_CONNECT_ORDER_FROM_TT:
          ProcessTTMessageForConnectOrderFromTT(&ttMessage, connection->index);
          break;

        // Receive request TS disconnect to a ECN Order
        case MSG_TYPE_FOR_RECEIVE_DISCONNECT_ORDER_FROM_TT:
          ProcessTTMessageForDisconnectOrderFromTT(&ttMessage, connection->index);
          break;

        // Receive request TS connect to all ECN Order
        case MSG_TYPE_FOR_RECEIVE_CONNECT_ALL_ORDER_FROM_TT:
          ProcessTTMessageForConnectAllOrderFromTT(&ttMessage, connection->index);
          break;

        // Receive request TS disconnect to all ECN Order
        case MSG_TYPE_FOR_RECEIVE_DISCONNECT_ALL_ORDER_FROM_TT:
          ProcessTTMessageForDisconnectAllOrderFromTT(&ttMessage, connection->index);
          break;

        // Receive request to connect to a TS
        case MSG_TYPE_FOR_RECEIVE_CONNECT_TS_FROM_TT:
          ProcessTTMessageForConnectToTS(&ttMessage);
          break;

        // Receive request to disconnect to a TS
        case MSG_TYPE_FOR_RECEIVE_DISCONNECT_TS_FROM_TT:
          ProcessTTMessageForDisconnectToTS(&ttMessage);
          break;

        // Receive request to connect to all TS
        case MSG_TYPE_FOR_RECEIVE_CONNECT_ALL_TS_FROM_TT:
          ProcessTTMessageForConnectToAllTS(&ttMessage, connection->index);
          break;

        // Receive request to disconnect to all TS
        case MSG_TYPE_FOR_RECEIVE_DISCONNECT_ALL_TS_FROM_TT:
          ProcessTTMessageForDisconnectToAllTS(&ttMessage, connection->index);
          break;

        // Receive request to connect to PM
        case MSG_TYPE_FOR_RECEIVE_CONNECT_PM_FROM_TT:
          ProcessTTMessageForConnectToPM(&ttMessage);
          break;

        // Receive request to disconnect to PM
        case MSG_TYPE_FOR_RECEIVE_DISCONNECT_PM_FROM_TT:
          ProcessTTMessageForDisconnectToPM(&ttMessage);
          break;

        // Receive request to reset stuck to all TS
        case MSG_TYPE_FOR_RECEIVE_RESET_STUCK_ALL_TS_FROM_TT:
          ProcessTTMessageForResetStuckToAllTS(&ttMessage);
          break;

        // Receive request to reset loser to all TS
        case MSG_TYPE_FOR_RECEIVE_RESET_LOSER_ALL_TS_FROM_TT:
          ProcessTTMessageForResetLoserToAllTS(&ttMessage);
          break;
        
        // Receive request to reset loser to all TS
        case MSG_TYPE_FOR_RECEIVE_RESET_BUYING_POWER_ALL_TS_FROM_TT:
          //ProcessTTMessageForResetBuyingPowerToAllTS(&ttMessage); //currently not used
          break;

        // Receive set Trade Servers configuration message
        case MSG_TYPE_FOR_SET_TS_CONF_FROM_TT:
          ProcessTTMessageForSetTSConf(&ttMessage);
          break;
        
        // Receive set TS setting
        case MSG_TYPE_FOR_SET_TS_SETTING_FROM_TT:
          ProcessTTMessageForSetTSSettingFromTT(&ttMessage);
          break;
        
        // Receive set AS setting
        case MSG_TYPE_FOR_SET_AS_SETTING_FROM_TT:
          ProcessTTMessageForSetASSettingFromTT(&ttMessage);
          break;
        
        // Receive set PM setting
        case MSG_TYPE_FOR_SET_PM_SETTING_FROM_TT:
          ProcessTTMessageForSetPMSettingFromTT(&ttMessage);
          break;

        // Receive set Trader Tools configuration message
        case MSG_TYPE_FOR_SET_TT_CONF_FROM_TT:
          ProcessTTMessageForSetTTConf(&ttMessage);
          break;

        // Receive set Alert configuration message
        case MSG_TYPE_FOR_SET_ALERT_CONFIG_FROM_TT:
          ProcessTTMessageForSetAlertConf(&ttMessage);
          break;

        // Receive set Aggregation Server Order configuration
        case MSG_TYPE_FOR_SET_AS_ORDERS_CONF_FROM_TT:
          ProcessTTMessageForSetASOrder(&ttMessage);
          break;

        // Receive set Aggregation Server global configuration message
        case MSG_TYPE_FOR_SET_AS_GLOBAL_SETTING_FROM_TT:
          ProcessTTMessageForSetASGlobalConf(&ttMessage, connection->index);
          break;

        // Receive ignore symbol list message
        case MSG_TYPE_FOR_SET_IGNORE_SYMBOL_LIST_FROM_TT:
          ProcessTTMessageForIgnoreStockList(&ttMessage, connection->index);
          break;

        case MSG_TYPE_FOR_SET_VOLATILE_SYMBOL_UPDATE_FROM_TT:
          ProcessTTMessageForVolatileSymbolUpdate(&ttMessage);
          break;

        case MSG_TYPE_FOR_SET_VOLATILE_SECTION_UPDATE_FROM_TT:
          ProcessTTMessageForVolatileSectionUpdate(&ttMessage, connection);
          break;

        case MST_TYPE_FOR_SET_ALL_VOLATILE_TOGGLE_FROM_TT:
          ProcessTTMessageForAllVolatileToggle(&ttMessage);
          break;

        // Receive ignore symbol ranges list message
        case MSG_TYPE_FOR_SET_IGNORE_SYMBOL_RANGES_LIST_FROM_TT:
          ProcessTTMessageForIgnoreStockRangesList(&ttMessage);
          break;
        // Receive set Trade Server order configuration
        case MSG_TYPE_FOR_SET_TS_BOOK_CONF_FROM_TT:
          ProcessTTMessageForSetTSBookConf(&ttMessage);
          break;

        case MSG_TYPE_FOR_SET_ARCA_BOOK_FEED_FROM_TT:
          ProcessTTMessageForSetTSARCABookFeed(&ttMessage);
          break;

        // Receive set Trade Server order configuration
        case MSG_TYPE_FOR_SET_TS_ORDER_CONF_FROM_TT:
          ProcessTTMessageForSetTSOrderConf(&ttMessage);
          break;

        // Receive set Trade Server global configuration
        case MSG_TYPE_FOR_SET_TS_GLOBAL_CONF_FROM_TT:
          ProcessTTMessageForSetTSGlobalConf(&ttMessage, connection->index);
          break;

        // Receive set Trade Server minimum spread configuration
        case MSG_TYPE_FOR_SET_MIN_SPREAD_CONF_FROM_TT:
          ProcessTTMessageForSetTSMinSpreadConf(&ttMessage, connection->index);
          break;

        // Receive set PM configuration
        case MSG_TYPE_FOR_SET_PM_CONF_FROM_TT:
          ProcessTTMessageForSetPMConf(&ttMessage);
          break;

        // Receive set PM gkobal configuration
        case MSG_TYPE_FOR_SET_PM_GLOBAL_SETTING_FROM_TT:
          ProcessTTMessageForSetPMGlobalConf(&ttMessage);
          break;

        // Receive request enable PM from TT
        case MSG_TYPE_FOR_RECEIVE_ENABLE_PM_FROM_TT:
          ProcessTTMessageForEnablePMFromTT(&ttMessage, connection->index);
          break;

        // Receive request disable PM from TT
        case MSG_TYPE_FOR_RECEIVE_DISABLE_PM_FROM_TT:
          ProcessTTMessageForDisablePMFromTT(&ttMessage, connection->index);
          break;

        // Receive request connect to a PM Book from TT
        case MSG_TYPE_FOR_RECEIVE_CONNECT_A_PM_BOOK_FROM_TT:
          ProcessTTMessageForConnectAPMBookFromTT(&ttMessage, connection->index);
          break;

        // Receive request disconnect to a PM Book from TT
        case MSG_TYPE_FOR_RECEIVE_DISCONNECT_A_PM_BOOK_FROM_TT:
          ProcessTTMessageForDisconnectAPMBookFromTT(&ttMessage, connection->index);
          break;

        // Receive request connect to all PM Book from TT
        case MSG_TYPE_FOR_RECEIVE_CONNECT_ALL_PM_BOOK_FROM_TT:
          ProcessTTMessageForConnectAllPMBookFromTT(&ttMessage, connection->index);
          break;

        // Receive request disconnect to all PM Book from TT
        case MSG_TYPE_FOR_RECEIVE_DISCONNECT_ALL_PM_BOOK_FROM_TT:
          ProcessTTMessageForDisconnectAllPMBookFromTT(&ttMessage, connection->index);
          break;

        // Receive request connect to a PM Order from TT
        case MSG_TYPE_FOR_RECEIVE_CONNECT_A_PM_ORDER_FROM_TT:
          ProcessTTMessageForConnectAPMOrderFromTT(&ttMessage, connection->index);
          break;

        // Receive request disconnect to a PM Order from TT
        case MSG_TYPE_FOR_RECEIVE_DISCONNECT_A_PM_ORDER_FROM_TT:
          ProcessTTMessageForDisconnectAPMOrderFromTT(&ttMessage, connection->index);
          break;

        // Receive request connect to all PM Order from TT
        case MSG_TYPE_FOR_RECEIVE_CONNECT_ALL_PM_ORDER_FROM_TT:
          ProcessTTMessageForConnectAllPMOrderFromTT(&ttMessage, connection->index);
          break;

        // Receive request disconnect to all PM Order from TT
        case MSG_TYPE_FOR_RECEIVE_DISCONNECT_ALL_PM_ORDER_FROM_TT:
          ProcessTTMessageForDisconnectAllPMOrderFromTT(&ttMessage, connection->index);
          break;

        // Receive request connect to CTS from TT
        case MSG_TYPE_FOR_RECEIVE_CONNECT_CTS_FROM_TT:
          ProcessTTMessageForConnectCTSFromTT(&ttMessage, connection->index);
          break;
        
        // Receive request disconnect to CTS from TT
        case MSG_TYPE_FOR_RECEIVE_DISCONNECT_CTS_FROM_TT:
          ProcessTTMessageForDisconnectCTSFromTT(&ttMessage, connection->index);
          break;
          
        // Receive request connect to CQS from TT
        case MSG_TYPE_FOR_RECEIVE_CONNECT_CQS_FROM_TT:
          ProcessTTMessageForConnectCQSFromTT(&ttMessage, connection->index);
          break;
        
        // Receive request disconnect to CQS from TT
        case MSG_TYPE_FOR_RECEIVE_DISCONNECT_CQS_FROM_TT:
          ProcessTTMessageForDisconnectCQSFromTT(&ttMessage, connection->index);
          break;

        //For UQDF
        // Receive request connect to UQDF from TT
        case MSG_TYPE_FOR_RECEIVE_CONNECT_UQDF_FROM_TT:
          ProcessTTMessageForConnectUQDF_FromTT(&ttMessage, connection->index);
          break;

        // Receive request disconnect to UQDF from TT
        case MSG_TYPE_FOR_RECEIVE_DISCONNECT_UQDF_FROM_TT:
          ProcessTTMessageForDisconnectUQDF_FromTT(&ttMessage, connection->index);
          break;

        // Receive request connect to UTDF from TT
        case MSG_TYPE_FOR_RECEIVE_CONNECT_UTDF_FROM_TT:
          ProcessTTMessageForConnectUTDFFromTT(&ttMessage, connection->index);
          break;

        // Receive request disconnect to UTDF from TT
        case MSG_TYPE_FOR_RECEIVE_DISCONNECT_UTDF_FROM_TT:
          ProcessTTMessageForDisconnectUTDFFromTT(&ttMessage, connection->index);
          break;

        // Receive set PM Book configuration
        case MSG_TYPE_FOR_SET_PM_BOOK_CONF_FROM_TT:
          ProcessTTMessageForSetPMBookConf(&ttMessage);
          break;

        // Receive set PM Order configuration
        case MSG_TYPE_FOR_SET_PM_ORDER_CONF_FROM_TT:
          ProcessTTMessageForSetPMOrderConf(&ttMessage);
          break;

        // Receive set PM CTS configuration
        case MSG_TYPE_FOR_SET_PM_CTS_SERVER_FROM_TT:
          //ProcessTTMessageForSetPMCTSConf(&ttMessage);
          break;

        // Receive dsiabled alert reject code
        case MSG_TYPE_FOR_RECEIVE_DISABLED_ALERT_REJECT_CODE_FROM_TT:
          ProcessTTMessageForSetDisabledAlertRejectCodeFromTT(&ttMessage);
          break;

        // Receive set PM CQS configuration
        //case MSG_TYPE_FOR_SET_PM_CQS_SERVER_FROM_TT:
          //ProcessTTMessageForSetPMCQSConf(&ttMessage);
          //break;
        //end

        // Receive set PM ignore symbol
        case MSG_TYPE_FOR_SET_PM_IGNORE_SYMBOL_FROM_TT:
          ProcessTTMessageForSetPMIgnoreSymbol(&ttMessage, connection->index);
          break;

        // Receive get PM symbol status
        case MSG_TYPE_FOR_GET_PM_SYMBOL_STATUS_FROM_TT:
          ProcessTTMessageForGetPMSymbolStatusFromTT(&ttMessage, connection->index);
          break;

        // Receive get PM halted stock list
        case MSG_TYPE_FOR_SET_A_HALT_SYMBOL_FROM_TT:
          ProcessTTMessageForSetHaltStockFromTT(&ttMessage, connection->index);
          break;
          
        // Receive active halted stocks
        case MSG_TYPE_FOR_ACTIVATE_HALTED_STOCKS_FROM_TT:
          ProcessTTMessageForActivateHaltedStockFromTT(&ttMessage, connection->index);
          break;

        // Receive get traded list of a symbol
        case MSG_TYPE_FOR_GET_TRADED_LIST_FROM_TT:
          ProcessTTMessageForGetTradedListOfASymbolFromTT(&ttMessage, connection->index);
          break;

        // Receive send disengage symbol to PM
        case MSG_TYPE_FOR_RECEIVE_DISENGAGE_SYMBOL_FROM_TT:
          ProcessTTMessageForSendDisengageSymbolFromTT(&ttMessage, connection->index);
          break;

        // Receive send engage stuck to PM
        case MSG_TYPE_FOR_RECEIVE_ENGAGE_STUCK_FROM_TT:
          ProcessTTMessageForSendEngageStuckFromTT(&ttMessage, connection->index);
          break;
          
        // Luanvk
        // CQS+UQDF on TS
        // Receive request connect to CQS/UQDF
        case MSG_TYPE_FOR_REQUEST_CONNECT_TO_BBO_FROM_TT:
          ProcessMessageForConnectToCQSnUQDF_FromTT(&ttMessage, connection->index);
          break;
        
        // Receive request disconnect to CQS/UQDF
        case MSG_TYPE_FOR_REQUEST_DISCONNECT_TO_BBO_FROM_TT:
          ProcessMessageForDisconnectToCQSnUQDF_FromTT(&ttMessage, connection->index);
          break;
          
        // Receive request enable/disable ISO trading
        case MSG_TYPE_FOR_REQUEST_ISO_TRADING_FROM_TT:
          ProcessMessageForSetISO_TradingFromTT(&ttMessage, connection->index);
          break;
        
        // Receive set exchange conf
        case MSG_TYPE_FOR_SET_EXCHANGE_CONF_FROM_TT:
          ProcessMessageForSetExchangeConf(&ttMessage);
          break;
          
        // Receive set PM Exchange configuration
        case MSG_TYPE_FOR_SET_PM_EXCHANGE_CONF_FROM_TT:
          ProcessMessageForSetPMExchangeConf(&ttMessage);
          break;
          
        // Receive get BBO quotes
        case MSG_TYPE_FOR_GET_BBO_QUOTE_LIST_FROM_TT:
          ProcessMessageForGetBBOQuotes(&ttMessage, connection->index);
          break;
          
        // Receive request to flush BBO quote for a exchange
        case MSG_TYPE_FOR_REQUEST_FLUSH_BBO_QUOTES_FROM_TT:
          ProcessMessageForFlushBBOQuotes(&ttMessage);
          break;
        
        // Receive set role book in the ISO algorithm
        case MSG_TYPE_FOR_SET_BOOK_ISO_TRADING_FROM_TT:
          ProcessMessageForSetBookISO_Trading(&ttMessage);
          break;

        // Receive get PM BBO quotes
        case MSG_TYPE_FOR_GET_PM_BBO_QUOTE_LIST_FROM_TT:
          ProcessMessageForGetPMBBOQuotes(&ttMessage, connection->index);
          break;
          
        // Receive request to flush PM BBO quote for a exchange
        case MSG_TYPE_FOR_REQUEST_FLUSH_PM_BBO_QUOTES_FROM_TT:
          ProcessMessageForFlushPMBBOQuotes(&ttMessage);
          break;
          
        //For get version control from TT
        case MSG_TYPE_FOR_LOG_TT_VERSION_CONTROL:
          ProcessMessageForLogTTVersionControl(&ttMessage, connection->index);
          break;
          
        //For Login Info from TT
        case MSG_TYPE_FOR_RECEIVE_LOGIN_INFO_FROM_TT:
          ProcessMessageForAuthenticateLoginInfo(&ttMessage, connection->index);
          break;
        
        //For Switch trader role
        case MSG_TYPE_FOR_SWITCH_TRADER_ROLE_FROM_TT:
          ProcessMessageForSwitchTraderRole(&ttMessage, connection->index);
          break;
        
        // Receive request to get number of symbols with quotes
        case MSG_TYPE_FOR_RECEIVE_GET_NUMBER_OF_SYMBOLS_WITH_QUOTES_FROM_TT:
          ProcessTTMessageForGetNumberOfSymbolsWithQuotesFromTT(&ttMessage, connection->index);
          break;
        //For switching PM ARCA feed
        case MSG_TYPE_FOR_SET_PM_ARCA_BOOK_FEED_FROM_TT:
          ProcessTTMessageForSetPMARCABookFeed(&ttMessage);
          break;
        case MSG_TYPE_FOR_REREQUEST_ETB_LIST_FROM_TT:
          ProcessTTMessageForRerequestEtbList();
          break;
        // Invalid message
        default:
          TraceLog(ERROR_LEVEL, "Aggregation Server does not support message from Trader Tool, msgType = %d\n", msgType);
          break;
      }
    }
    else
    {
      TraceLog(DEBUG_LEVEL, "%s's TT is disconnected\n", connection->userName);

      if(connection->statusUpdate == USER_LOGGED || connection->statusUpdate == USER_PASSWORD_INCORRECT)
      {
        break;
      }
      
      char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
      sprintf(activityDetail, "%d", connection->currentState);
      
      //Trader disconnect from AS
      ProcessUpdateTTEventLogToDatabase(connection->userName, ACTIVITY_TYPE_DISCONNECT_FROM_AS, activityDetail);
      
      //Update trader status to offline
      ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);
      
      //connection->currentState = OFFLINE;

      CloseConnection(connection);
      
      break;
    }
  }

  // Find index for 
  int i, countConnection = 0;

  // Check number of connections for Trader Tool
  for (i = 0; i < MAX_TRADER_TOOL_CONNECTIONS; i++)
  {
    if (traderToolsInfo.connection[i].status == CONNECT)
    {
      countConnection ++;
    }
  }

  // Check auto disable trading
  if (countConnection == 0)
  {
    TraceLog(DEBUG_LEVEL, "No TT connections to AS --> Processing send disable trading to all TS\n");

    // Call function to disable trading on all ECN order
    SendDisableTradingAllOrderToAllTS(TS_TRADING_DISABLED_BY_NO_TT_CONNECTION);

    // Call function to disable PM
    SendDisableTradingPMToPM(PM_TRADING_DISABLED_BY_NO_TT_CONNECTION);
  }

  return NULL;
}

/****************************************************************************
- Function name:  ReceiveTTMessage
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ReceiveTTMessage(t_ConnectionStatus *connection, void *message)
{
  int receivedBytes;

  t_TTMessage *ttMessage = (t_TTMessage *)message;
  
  // Timeout counter
  int timeoutCounter = 0;
  
  // Current messsae body length
  int currentMsgBodyLen = 0;
  
  // Initialize message
  ttMessage->msgLen = 0;
  
  /*
  Try to receive message header
  
  If cannot receive message header when reached timeout, return and report error
  
  otherwise calculate message body
  
  If message body has length 0, return Success
  
  Otherwise try to receive message body
  
  If cannot receive message body when reached timeout, return and report error
  
  Otherwise return Success
   */
  
  // Try to receive message header
  while (ttMessage->msgLen < MSG_HEADER_LEN)
  {
    receivedBytes = recv(connection->socket, &ttMessage->msgContent[ttMessage->msgLen], MSG_HEADER_LEN - ttMessage->msgLen, 0);

    if (receivedBytes > 0)
    {
      timeoutCounter = 0;
      
      ttMessage->msgLen += receivedBytes;
    }
    else
    {
      if ((receivedBytes == 0) && (errno == 0))
      {
        TraceLog(ERROR_LEVEL, "%s's TT: Socket is close\n", connection->userName);
        return ERROR;
      }
      else
      {       
        if (timeoutCounter < 3)
        {
          timeoutCounter++;
          TraceLog(DEBUG_LEVEL, "%s's TT: Waiting. Sleep num = %d\n", connection->userName, timeoutCounter);
        }
        else
        {
          TraceLog(ERROR_LEVEL, "%s's TT: TIME OUT!\n", connection->userName);
          return ERROR;
        }
      }
    }
  }
  
  // Get message body length
  currentMsgBodyLen = GetIntNumber(&ttMessage->msgContent[MSG_BODY_LEN_INDEX]);

  //TraceLog(DEBUG_LEVEL, "Body length = %d\n", currentMsgBodyLen);

  if (currentMsgBodyLen == 0)
  {
    // Body is NULL

    return SUCCESS;
  }
  
  // Try to receive message body
  while (ttMessage->msgLen < currentMsgBodyLen + MSG_HEADER_LEN)
  {
    receivedBytes = recv(connection->socket, &ttMessage->msgContent[ttMessage->msgLen], currentMsgBodyLen - (ttMessage->msgLen - MSG_HEADER_LEN), 0);
    
    if (receivedBytes > 0)
    {
      timeoutCounter = 0;
      ttMessage->msgLen += receivedBytes;
    }
    else
    {
      if ((receivedBytes == 0) && (errno == 0))
      {
        TraceLog(ERROR_LEVEL, "%s's TT: Socket is close\n", connection->userName);
        return ERROR;
      }
      else
      {       
        if (timeoutCounter < 3)
        {
          timeoutCounter++;
          TraceLog(DEBUG_LEVEL, "%s's TT: Waiting. Sleep num = %d\n", connection->userName, timeoutCounter);
        }
        else
        {
          TraceLog(ERROR_LEVEL, "%s's TT: TIME OUT!\n", connection->userName);
          return ERROR;
        }
      }
    }
  }

  // Succesfully received
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForConnectToTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForConnectToTS(void *message)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Trade Server Id
  int tsId = GetIntNumber(&ttMessage->msgContent[currentIndex]);

  // Find Trade Server index
  int tsIndex = FindTradeServerIndex(tsId);

  if (tsIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "Invalid Trade Server Id = %d\n", tsId);

    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Received message to request AS connect to %s\n", tradeServersInfo.config[tsIndex].description);

  // Call function to connect to TS
  Connect2TradeServer(tsIndex);

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForDisconnectToTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForDisconnectToTS(void *message)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Trade Server Id
  int tsId = GetIntNumber(&ttMessage->msgContent[currentIndex]);

  // Find Trade Server index
  int tsIndex = FindTradeServerIndex(tsId);

  if (tsIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "Invalid Trade Server Id = %d\n", tsId);

    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Received message to request AS disconnect to %s\n", tradeServersInfo.config[tsIndex].description);

  // Call function to connect to TS
  Disconnect2TradeServer(tsIndex);

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForConnectToAllTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForConnectToAllTS(void *message, int ttIndex)
{
  TraceLog(DEBUG_LEVEL, "Received message to request AS connect to all connection from TT\n");

  if (Connect2AllTradeServer(ttIndex) == SUCCESS && ProcessConnectToPMAndAllPMConnection() == SUCCESS && ProcessASConnectToAllOrderECN() == SUCCESS)
  {
    // Update TT event log for the event: TT request AS connect to all servers
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    sprintf(activityDetail, "%d,0,1", traderToolsInfo.connection[ttIndex].currentState);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_BOOK, activityDetail);
  } 
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForDisconnectToAllTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForDisconnectToAllTS(void *message, int ttIndex)
{
  TraceLog(DEBUG_LEVEL, "Received message to request AS disconnect to all connection from TT\n");

  if (Disconnect2AllTradeServer() == SUCCESS && ProcessDisconnectToPMAndAllPMConnection() == SUCCESS && ProcessASDisconnectToAllOrderECN() == SUCCESS)
  {
    // Update TT event log for the event: TT request AS disconnect to all servers
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    sprintf(activityDetail, "%d,1,1", traderToolsInfo.connection[ttIndex].currentState);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_BOOK, activityDetail);
  } 
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForResetStuckToAllTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForResetStuckToAllTS(void *message)
{
  TraceLog(DEBUG_LEVEL, "Received message to request AS send reset stuck position to all TS from TT\n");

  // Call function to send request to reset stuck to all TS
  SendResetTSCurrentStatusToAllTS(NUM_STUCK_INDEX);

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForResetLoserToAllTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForResetLoserToAllTS(void *message)
{
  TraceLog(DEBUG_LEVEL, "Received message to request AS send reset consecutive loser to all TS from TT\n");

  // Call function to send request to reset loser to all TS
  SendResetTSCurrentStatusToAllTS(NUM_LOSER_INDEX);

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForResetBuyingPowerToAllTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
/*int ProcessTTMessageForResetBuyingPowerToAllTS(void *message)
{
  TraceLog(DEBUG_LEVEL, "Received message to request AS send reset buying power to all TS from TT\n");

  // Call function to send request to reset loser to all TS
  SendResetTSCurrentStatusToAllTS(BUYING_POWER_INDEX);

  return SUCCESS;
}*/

/****************************************************************************
- Function name:  ProcessTTMessageForSendNewOrderFromTT
- Input:      
- Output:     
- Return:     
- Description:    When TT requests to send new order, AS check to see
          where it should place (AS or PM)?
- Usage:      
****************************************************************************/
int ProcessTTMessageForSendNewOrderFromTT(void *message, int ttIndex)
{
  TraceLog(DEBUG_LEVEL, "user name = %s\n", traderToolsInfo.connection[ttIndex].userName);

  t_TTMessage *ttMessage = (t_TTMessage *)message;

  t_SendNewOrder newOrder;
  int isError = 0;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Symbol
  strncpy(newOrder.symbol, (char *) &ttMessage->msgContent[currentIndex], SYMBOL_LEN);
  currentIndex += SYMBOL_LEN;

  TraceLog(DEBUG_LEVEL, "symbol = %.8s\n", newOrder.symbol);

  // side
  newOrder.side = GetIntNumber(&ttMessage->msgContent[currentIndex]);
  currentIndex += 4;

  TraceLog(DEBUG_LEVEL, "side = %d\n", newOrder.side);

  // shares
  newOrder.shares = GetIntNumber(&ttMessage->msgContent[currentIndex]);
  currentIndex += 4;

  TraceLog(DEBUG_LEVEL, "shares = %d\n", newOrder.shares);

  if (newOrder.shares <= 0)
  {
    isError = -1;
  }

  // maxFloor
  newOrder.maxFloor = GetIntNumber(&ttMessage->msgContent[currentIndex]);
  currentIndex += 4;

  TraceLog(DEBUG_LEVEL, "maxFloor = %d\n", newOrder.maxFloor);

  // price
  newOrder.price = GetDoubleNumber(&ttMessage->msgContent[currentIndex]);
  currentIndex += 8;

  TraceLog(DEBUG_LEVEL, "price = %lf\n", newOrder.price);

  if (newOrder.price < 0.00)
  {
    isError = -1;
  }

  // ECNId
  newOrder.ECNId = GetIntNumber(&ttMessage->msgContent[currentIndex]);
  currentIndex += 4;

  newOrder.ECNId = TT_TO_AS_ORDER_TYPE_MAPPING[newOrder.ECNId]; //convert index from TT to AS

  int venueId = -1;
  
  switch (newOrder.ECNId)
  {
    case TYPE_ARCA_DIRECT:
      venueId = TS_ARCA_BOOK_INDEX;
      
      if (feq(newOrder.price, 0.00))
      {
        isError = -1;
      }
      break;
      
    case TYPE_NASDAQ_RASH:
      venueId = TS_NASDAQ_BOOK_INDEX;
      break;
      
    default:
      TraceLog(ERROR_LEVEL, "Invalid ECN (%d) for manual order!\n", newOrder.ECNId);
      isError = -1;
      break;
  }

  TraceLog(DEBUG_LEVEL, "ECNId = %s\n", (newOrder.ECNId == TYPE_ARCA_DIRECT) ? "ARCA DIRECT" : "NASDAQ RASH");

  // tif
  newOrder.tif = GetIntNumber(&ttMessage->msgContent[currentIndex]);
  currentIndex += 4;

  TraceLog(DEBUG_LEVEL, "tif = %d\n", newOrder.tif);

  int sendId = GetIntNumber(&ttMessage->msgContent[currentIndex]);
  currentIndex += 4;

  TraceLog(DEBUG_LEVEL, "sendId = %d\n", sendId);

  // Peg difference
  newOrder.pegDef = GetDoubleNumber(&ttMessage->msgContent[currentIndex]);
  currentIndex += 8;
  
  if (newOrder.pegDef > 0.0001)
  {
    TraceLog(DEBUG_LEVEL, "Peg difference = %lf\n", newOrder.pegDef);
  }
  
  // Order ID
  newOrder.orderId = GetIntNumber(&ttMessage->msgContent[currentIndex]);
  currentIndex += 4;
  
  //account
  newOrder.tradingAccount = ttMessage->msgContent[currentIndex++];
  TraceLog(DEBUG_LEVEL, "account = %d\n", newOrder.tradingAccount);

  // Force flag
  int isForced = GetIntNumber(&ttMessage->msgContent[currentIndex]);
  TraceLog(DEBUG_LEVEL, "isForced = %d\n", isForced);

  int stockSymbolIndex = GetStockSymbolIndex(newOrder.symbol);
  if (stockSymbolIndex == -1)
  {
    isError = -1;
  }
  
  if (isError == -1)  //There is a problem with values of manual order!
  {
    ProcessResponseSendNewOrderFromTT(&newOrder, sendId, -1, ttIndex);
    return ERROR;
  }
  
  if (isForced == 1)
  {
    int lastAccountTradingStatus = verifyPositionInfo[newOrder.tradingAccount][stockSymbolIndex].isTrading;
    verifyPositionInfo[newOrder.tradingAccount][stockSymbolIndex].isTrading = TRADING_MANUAL;
    verifyPositionInfo[newOrder.tradingAccount][stockSymbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();

    int willAddEventLog = 0;
    if (newOrder.orderId == -1)
    {
      // Increase Order ID
      willAddEventLog = 1;
      pthread_mutex_lock(&nextOrderID_Mutex); 
        newOrder.orderId = clientOrderID;
        clientOrderID += 1;
        SaveClientOrderID("next_order_id", clientOrderID);
      pthread_mutex_unlock(&nextOrderID_Mutex);
    }
    
    strcpy(manualOrderInfo[newOrder.orderId % MAX_MANUAL_ORDER].username, traderToolsInfo.connection[ttIndex].userName);
    
    if (IsAbleToPlaceManualOrderOnPM(newOrder.ECNId) == 1)
    {
      /*  Will place order on PM
        We dont increase Order ID, since it was already increased */
      UpdateTimeInForce(&newOrder);
      if (SendManualOrderToPM(&newOrder, sendId, isForced, ttIndex) == ERROR) //Failed
      {
        verifyPositionInfo[newOrder.tradingAccount][stockSymbolIndex].isTrading = lastAccountTradingStatus;
        verifyPositionInfo[newOrder.tradingAccount][stockSymbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
        
        if (ProcessResponseSendNewOrderFromTT(&newOrder, sendId, -1, ttIndex) == ERROR) return SUCCESS;
      }
    }
    else if (IsAbleToPlaceManualOrderOnAS(newOrder.ECNId) == 1)
    {
      /*  Will place order on AS
        We dont increase Order ID, since it was already increased */

      if (newOrder.ECNId == TYPE_ARCA_DIRECT)
      {
        UpdateTimeInForce(&newOrder);
        if (SendNewOrderToARCA_DIRECTOrder(&newOrder) == ERROR) //Failed
        {
          verifyPositionInfo[newOrder.tradingAccount][stockSymbolIndex].isTrading = lastAccountTradingStatus;
          verifyPositionInfo[newOrder.tradingAccount][stockSymbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
          
          if (ProcessResponseSendNewOrderFromTT(&newOrder, sendId, -1, ttIndex) == ERROR) return SUCCESS;
        }
        else  //Sent successfully
        {
          if (ProcessResponseSendNewOrderFromTT(&newOrder, sendId, newOrder.orderId, ttIndex) == ERROR) return SUCCESS;
        }
      }
      else
      {
        UpdateTimeInForce(&newOrder);
        if (SendNewOrderToNASDAQ_RASHOrder(&newOrder) == ERROR)   //Failed
        {
          verifyPositionInfo[newOrder.tradingAccount][stockSymbolIndex].isTrading = lastAccountTradingStatus;
          verifyPositionInfo[newOrder.tradingAccount][stockSymbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
          
          if (ProcessResponseSendNewOrderFromTT(&newOrder, sendId, -1, ttIndex) == ERROR) return SUCCESS;
        }
        else  //Sent successfully
        {
          if (ProcessResponseSendNewOrderFromTT(&newOrder, sendId, newOrder.orderId, ttIndex) == ERROR) return SUCCESS;
        }
      }
    }
    else  //Cannot place order, it's hard to go here!
    {
      verifyPositionInfo[newOrder.tradingAccount][stockSymbolIndex].isTrading = lastAccountTradingStatus;
      verifyPositionInfo[newOrder.tradingAccount][stockSymbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
      
      ProcessResponseSendNewOrderFromTT(&newOrder, sendId, -1, ttIndex);
      return SUCCESS;
    }
    
    if (willAddEventLog == 1)
    {
      //Update manual order event log to database
      char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
      sprintf(activityDetail, "%d,%d", traderToolsInfo.connection[ttIndex].currentState, newOrder.orderId);
      ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_MANUAL_ORDER, activityDetail);
    }
  }
  else  //isForced == 0
  {
    // Check symbol halted
    if (HaltStatusMgmt.haltStatus[stockSymbolIndex][venueId] == TRADING_HALTED)
    {
      char msg[1024];
      sprintf(msg, "Could not send manual order (%.8s, side %d, shares %d, price %lf), because this symbol is halted", 
            newOrder.symbol, newOrder.side, newOrder.shares, newOrder.price);
      
      TraceLog(DEBUG_LEVEL, "%s\n", msg);
      
      SendNotifyMessageToTT(ttIndex, msg, 0);
      ProcessResponseSendNewOrderFromTT(&newOrder, sendId, -1, ttIndex);      
      return SUCCESS;
    }
    
    if (verifyPositionInfo[FIRST_ACCOUNT][stockSymbolIndex].isTrading != TRADING_NONE || 
      verifyPositionInfo[SECOND_ACCOUNT][stockSymbolIndex].isTrading != TRADING_NONE ||
      verifyPositionInfo[THIRD_ACCOUNT][stockSymbolIndex].isTrading != TRADING_NONE || 
      verifyPositionInfo[FOURTH_ACCOUNT][stockSymbolIndex].isTrading != TRADING_NONE)
    {
      //stuck is being traded, so do not send
      char msg[1024];
      sprintf(msg, "Could not send manual order (%.8s, side %d, shares %d, price %lf), because there is a stuck of same symbol trading on AS/PM, please wait for it to be finished.",
          newOrder.symbol, newOrder.side, newOrder.shares, newOrder.price);
      SendNotifyMessageToTT(ttIndex, msg, 0);
      ProcessResponseSendNewOrderFromTT(&newOrder, sendId, -1, ttIndex);
      
      return SUCCESS;
    }

    verifyPositionInfo[newOrder.tradingAccount][stockSymbolIndex].isTrading = TRADING_MANUAL;
    verifyPositionInfo[newOrder.tradingAccount][stockSymbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
    
    // Increase Order ID
    pthread_mutex_lock(&nextOrderID_Mutex); 
      newOrder.orderId = clientOrderID;
      clientOrderID += 1;
      SaveClientOrderID("next_order_id", clientOrderID);
    pthread_mutex_unlock(&nextOrderID_Mutex);
    
    strcpy(manualOrderInfo[clientOrderID % MAX_MANUAL_ORDER].username, traderToolsInfo.connection[ttIndex].userName);
    
    if (IsCTSOrUTDFConnectedOnPM() == 0)
    {
      /* If CTS or UTDF is not connected on PM, it will be NOT possible to:
        - Check new order's price with last trade price
        - Check for fast market mode
         So ask user if he wants to force the order sending */
      
      verifyPositionInfo[newOrder.tradingAccount][stockSymbolIndex].isTrading = TRADING_NONE;
      verifyPositionInfo[newOrder.tradingAccount][stockSymbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
      
      ProcessResponseSendNewOrderFromTT(&newOrder, sendId, -2, ttIndex);
    }
    else
    {
      /*  
        The order will be sent to PM to check for last price with CTS/UTDF or fast market mode
        If OK, it will be sent on PM
        If NOT, a feedback will be returned to AS to ask for TT if he wants to force?
      */
      UpdateTimeInForce(&newOrder);
      if (SendManualOrderToPM(&newOrder, sendId, isForced, ttIndex) == ERROR) //Failed
      {
        verifyPositionInfo[newOrder.tradingAccount][stockSymbolIndex].isTrading = TRADING_NONE;
        verifyPositionInfo[newOrder.tradingAccount][stockSymbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
        
        ProcessResponseSendNewOrderFromTT(&newOrder, sendId, -1, ttIndex);
      }
    }
    
    //Update manual order event log to database
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    sprintf(activityDetail, "%d,%d", traderToolsInfo.connection[ttIndex].currentState, newOrder.orderId);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_MANUAL_ORDER, activityDetail);

    return SUCCESS;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForSendCancelRequestFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForSendCancelRequestFromTT(void *message)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // clOrdId
  int clOrdId = GetIntNumber(&ttMessage->msgContent[currentIndex]);
  //TraceLog(DEBUG_LEVEL, "clOrdId = %d\n", clOrdId);

  // Update current index
  currentIndex += 4;

  // ECNOrderId
  char ECNOrderId[24];
  
  strncpy(ECNOrderId, (char *)&ttMessage->msgContent[currentIndex], 20);
  ECNOrderId[20] = 0;

  // Update current index
  currentIndex += 20;

  // tsId
  int tsId = GetIntNumber(&ttMessage->msgContent[currentIndex]);
  //TraceLog(DEBUG_LEVEL, "tsId = %d\n", tsId);

  // Update current index
  currentIndex += 4;

  TraceLog(DEBUG_LEVEL, "Received request to send cancel request to ECN order, tsId = %d, clOrdId = %d\n", tsId, clOrdId);

  if (tsId == MANUAL_ORDER)
  {
    if (clOrdId >= 500000)
    {
      // Call function to send cancel request to PM
      SendCancelRequestToPM(clOrdId, ECNOrderId);
    }
    else  //clOrdId < 500000 --> cancel a manual order
    {
      /*  Check where we should cancel the order (AS or PM)
        We will check for the connection status */
        
      int indexOpenOrder = CheckOrderIdIndex(clOrdId, MANUAL_ORDER);

      if ((indexOpenOrder == -1) || (indexOpenOrder == -2))
      {
        TraceLog(ERROR_LEVEL, "Cannot find order information with clOrdId = %d and tsId = %d\n", clOrdId, MANUAL_ORDER);

        return ERROR;
      }
      
      int ECNId = asOpenOrder.orderCollection[indexOpenOrder].orderSummary.ECNId;
      
      if (ECNId == TYPE_NASDAQ_RASH)
      {
        //AS and PM now use the same RASH session
        if ((pmInfo.connection.status == CONNECT) && (pmInfo.currentStatus.pmOecStatus[PM_NASDAQ_RASH_INDEX] == CONNECTED))
        {
          ProcessSendManualCancelRequestToPM(clOrdId, ECNOrderId);
        }
        else if (orderStatusMgmt[AS_NASDAQ_RASH_INDEX].isOrderConnected == CONNECTED)
        {
          //RASH is connected at AS so we will use AS
          ProcessSendCancelRequestToECNOrder(clOrdId, ECNOrderId);
        }
        else
        {
          //RASH is not connected at both AS and PM, so we cannot send
          return SUCCESS;
        }
      }
      else if (ECNId == TYPE_ARCA_DIRECT)
      {
        //AS and PM have different ARCA session
        if ((pmInfo.connection.status == CONNECT) && (pmInfo.currentStatus.pmOecStatus[PM_ARCA_DIRECT_INDEX] == CONNECTED))
        {
          ProcessSendManualCancelRequestToPM(clOrdId, ECNOrderId);
        }
        else if (orderStatusMgmt[AS_ARCA_DIRECT_INDEX].isOrderConnected == CONNECTED)
        {
          //ARCA DIRECT is connected at AS so we will use AS
          ProcessSendCancelRequestToECNOrder(clOrdId, ECNOrderId);
        }
        else
        {
          //ARCA DIRECT is not connected at both AS and PM, so we cannot send
          return SUCCESS;
        }
      }
      else
      {
        TraceLog(ERROR_LEVEL, "Invalid ECN to send cancel request (ECN: %d)\n", ECNId);
        return ERROR;
      }
    }
  }
  else
  {
    // Call function to send cancel request to TS
    ProcessSendCancelRequestToTS(clOrdId, ECNOrderId, tsId);
  } 

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForGetSymbolStatusFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForGetSymbolStatusFromTT(void *message, int ttIndex)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Trade Server Id
  int tsId = GetIntNumber(&ttMessage->msgContent[currentIndex]);
  currentIndex += 4;

  // Find Trade Server index
  int tsIndex = FindTradeServerIndex(tsId);
  if (tsIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "Invalid Trade Server Id = %d\n", tsId);
    return ERROR;
  }

  char symbol[SYMBOL_LEN] = "\0";
  memcpy(symbol, &ttMessage->msgContent[currentIndex], SYMBOL_LEN);
  TraceLog(DEBUG_LEVEL, "symbol = %.8s\n", symbol);

  // Call function to get stock symbol status to TS
  char listMdc[TS_MAX_BOOK_CONNECTIONS];
  memset(listMdc, 1, TS_MAX_BOOK_CONNECTIONS);
  SendGetSymbolStatusToTS(tsIndex, ttIndex, (unsigned char *)symbol, listMdc);

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForGetNumberOfSymbolsWithQuotesFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForGetNumberOfSymbolsWithQuotesFromTT(void *message, int ttIndex)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Trade Server Id
  int tsId = GetIntNumber(&ttMessage->msgContent[currentIndex]);
  currentIndex += 4;

  // Find Trade Server index
  int tsIndex = FindTradeServerIndex(tsId);

  if (tsIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "Invalid Trade Server Id = %d\n", tsId);

    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Received message to request get number of symbols with quotes of %s\n", tradeServersInfo.config[tsIndex].description);

  // Call function to get number of symbols with quotes to TS
  SendGetNumberOfSymbolsWithQuotesToTS(tsIndex, ttIndex);

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForFulshASymbolFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForFulshASymbolFromTT(void *message)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Trade Server Id
  int tsId = GetIntNumber(&ttMessage->msgContent[currentIndex]);

  // Find Trade Server index
  int tsIndex = FindTradeServerIndex(tsId);

  if (tsIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "Invalid Trade Server Id = %d\n", tsId);

    return ERROR;
  }

  // Update current index
  currentIndex += 4;

  // ECN Order Index (4 bytes)
  int ECNIndex = GetIntNumber(&ttMessage->msgContent[currentIndex]);
  ECNIndex = TT_TO_TS_BOOK_INDEX_MAPPING[ECNIndex];

  // Update current index
  currentIndex += 4;

  TraceLog(DEBUG_LEVEL, "Received message to request %s flush book for a symbol, ECNIndex = %d, symbol = %.8s\n", tradeServersInfo.config[tsIndex].description, ECNIndex, &ttMessage->msgContent[currentIndex]);

  // Call function to send fulsh a symbol to TS
  SendFlushASymbolToTS(tsIndex, ECNIndex, &ttMessage->msgContent[currentIndex]);

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForFulshPMASymbolFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForFulshPMASymbolFromTT(void *message)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  TraceLog(DEBUG_LEVEL, "Received message to request to flush book for a symbol to PM\n");

  // ECN Order Index (4 bytes)
  int ECNIndex = GetIntNumber(&ttMessage->msgContent[currentIndex]);
  currentIndex += 4;

  ECNIndex = TT_TO_PM_BOOK_INDEX_MAPPING[ECNIndex];
  TraceLog(DEBUG_LEVEL, "symbol = %.8s, ECNIndex = %d\n", &ttMessage->msgContent[currentIndex], ECNIndex);

  // Call function to send fulsh a symbol to TS
  SendFlushASymbolToPM(ECNIndex, &ttMessage->msgContent[currentIndex]);

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForFulshAllSymbolFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForFulshAllSymbolFromTT(void *message)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Trade Server Id
  int tsId = GetIntNumber(&ttMessage->msgContent[currentIndex]);

  // Find Trade Server index
  int tsIndex = FindTradeServerIndex(tsId);

  if (tsIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "Invalid Trade Server Id = %d\n", tsId);

    return ERROR;
  }

  // Update current index
  currentIndex += 4;

  // ECN Order Index (4 bytes)
  int ECNIndex = GetIntNumber(&ttMessage->msgContent[currentIndex]);

  ECNIndex = TT_TO_TS_BOOK_INDEX_MAPPING[ECNIndex];

  TraceLog(DEBUG_LEVEL, "Received message to request %s flush book for all symbol, ECNIndex = %d\n", tradeServersInfo.config[tsIndex].description, ECNIndex);

  // Call function to send fulsh all symbol to TS
  SendFlushAllSymbolToTS(tsIndex, ECNIndex);

  return SUCCESS;
}



/****************************************************************************
- Function name:  ProcessTTMessageForFulshPMAllSymbolFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForFulshPMAllSymbolFromTT(void *message)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // ECN Order Index (4 bytes)
  int ECNIndex = GetIntNumber(&ttMessage->msgContent[currentIndex]);
  ECNIndex = TT_TO_PM_BOOK_INDEX_MAPPING[ECNIndex];

  TraceLog(DEBUG_LEVEL, "Received message to request to flush book for all symbol to PM, ECNIndex = %d\n", ECNIndex);

  // Call function to send fulsh all symbol to PM
  SendFlushAllSymbolToPM(ECNIndex);

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForEnableTradingAOrderATSFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForEnableTradingAOrderATSFromTT(void *message, int ttIndex)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Trade Server Id
  int tsId = GetIntNumber(&ttMessage->msgContent[currentIndex]);

  // Find Trade Server index
  int tsIndex = FindTradeServerIndex(tsId);

  if (tsIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "Invalid Trade Server Id = %d\n", tsId);

    return ERROR;
  }

  // Update current index
  currentIndex += 4;

  // ECN Order Id (4 bytes)
  int ECNIndex = GetIntNumber(&ttMessage->msgContent[currentIndex]);
  ECNIndex = TT_TO_TS_ORDER_INDEX_MAPPING[ECNIndex];

  TraceLog(DEBUG_LEVEL, "[%s]Received message to request %s enable trading on a ECN order, ECNIndex = %d\n", traderToolsInfo.connection[ttIndex].userName, tradeServersInfo.config[tsIndex].description, ECNIndex);

  // Call function to enable trading on a ECN order
  if (SendEnableTradingAOrderToTS(tsIndex, ECNIndex) == SUCCESS)
  {
    //Update enable trading on a ECN odrder of TS event log to database
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    sprintf(activityDetail, "%d,0,%d%02d", traderToolsInfo.connection[ttIndex].currentState, tsId, ECNIndex + 1);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_ENABLE_DISENABLE_TRADING, activityDetail);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForDisableTradingAOrderATSFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForDisableTradingAOrderATSFromTT(void *message, int ttIndex)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Trade Server Id
  int tsId = GetIntNumber(&ttMessage->msgContent[currentIndex]);

  // Find Trade Server index
  int tsIndex = FindTradeServerIndex(tsId);

  if (tsIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "Invalid Trade Server Id = %d\n", tsId);

    return ERROR;
  }

  // Update current index
  currentIndex += 4;

  // ECN Order Id (4 bytes)
  int ECNIndex = GetIntNumber(&ttMessage->msgContent[currentIndex]);
  ECNIndex = TT_TO_TS_ORDER_INDEX_MAPPING[ECNIndex];

  TraceLog(DEBUG_LEVEL, "[%s]Received message to request %s disable trading on a ECN order, ECNIndex = %d\n", traderToolsInfo.connection[ttIndex].userName, tradeServersInfo.config[tsIndex].description, ECNIndex);

  // Call function to disable trading on a ECN order
  if (SendDisableTradingAOrderToTS(tsIndex, ECNIndex, ttIndex) == SUCCESS)
  {
    //Update disenable trading on a ECN odrder of TS event log to database
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    sprintf(activityDetail, "%d,1,%d%02d", traderToolsInfo.connection[ttIndex].currentState, tsId, ECNIndex + 1);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_ENABLE_DISENABLE_TRADING, activityDetail);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForEnableTradingAllOrderATSFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForEnableTradingAllOrderATSFromTT(void *message, int ttIndex)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Trade Server Id
  int tsId = GetIntNumber(&ttMessage->msgContent[currentIndex]);

  // Find Trade Server index
  int tsIndex = FindTradeServerIndex(tsId);

  if (tsIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "Invalid Trade Server Id = %d\n", tsId);

    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "[%s]Received message to request %s enable trading on all ECN order\n", traderToolsInfo.connection[ttIndex].userName, tradeServersInfo.config[tsIndex].description);

  // Call function to enable trading on all ECN order
  if (SendEnableTradingAllOrderToTS(tsIndex) == SUCCESS)
  {
    //Update enable trading on all ECN odrder of TS event log to database
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    sprintf(activityDetail, "%d,0,%d00", traderToolsInfo.connection[ttIndex].currentState, tsId);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_ENABLE_DISENABLE_TRADING, activityDetail);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForDisableTradingAllOrderATSFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForDisableTradingAllOrderATSFromTT(void *message, int ttIndex)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Trade Server Id
  int tsId = GetIntNumber(&ttMessage->msgContent[currentIndex]);

  // Find Trade Server index
  int tsIndex = FindTradeServerIndex(tsId);

  if (tsIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "Invalid Trade Server Id = %d\n", tsId);

    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "[%s]Received message to request %s disable trading on all ECN order\n", traderToolsInfo.connection[ttIndex].userName, tradeServersInfo.config[tsIndex].description);

  // Call function to disable trading on all ECN order
  if (SendDisableTradingAllOrderToTS(tsIndex, ttIndex) == SUCCESS)
  {
    //Update disenable trading on all ECN odrder of TS event log to database
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    sprintf(activityDetail, "%d,1,%d00", traderToolsInfo.connection[ttIndex].currentState, tsId);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_ENABLE_DISENABLE_TRADING, activityDetail);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForEnableTradingAllOrderAllTSFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForEnableTradingAllOrderAllTSFromTT(void *message, int ttIndex)
{
  TraceLog(DEBUG_LEVEL, "Received message to request enable trading on all ECN order of all TS\n");

  // Call function to enable trading on all ECN order
  if (SendEnableTradingAllOrderToAllTS() == SUCCESS)
  {
    //Update enable trading on all ECN
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    sprintf(activityDetail, "%d,0,0", traderToolsInfo.connection[ttIndex].currentState);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_ENABLE_DISENABLE_TRADING, activityDetail);
  }

  // Call function to enable trading to PM
  if (SendEnableTradingPMToPM() == SUCCESS)
  {
    //Update enable trading event log to database
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    sprintf(activityDetail, "%d,0,99", traderToolsInfo.connection[ttIndex].currentState);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_ENABLE_DISENABLE_TRADING, activityDetail);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForDisableTradingAllOrderAllTSFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForDisableTradingAllOrderAllTSFromTT(void *message, int ttIndex)
{
  TraceLog(DEBUG_LEVEL, "Received message to request disable trading on all ECN order of all TS\n");

  // Call function to disable trading on all ECN order
  if (SendDisableTradingAllOrderToAllTS(ttIndex) == SUCCESS)
  {
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    sprintf(activityDetail, "%d,1,0", traderToolsInfo.connection[ttIndex].currentState);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_ENABLE_DISENABLE_TRADING, activityDetail);
  }

  // Call function to disable PM to PM
  if (SendDisableTradingPMToPM(ttIndex) == SUCCESS)
  {
    //Update disenable trading event log to database
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    sprintf(activityDetail, "%d,1,99", traderToolsInfo.connection[ttIndex].currentState);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_ENABLE_DISENABLE_TRADING, activityDetail);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForResetTSCurrentStatusFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForResetTSCurrentStatusFromTT(void *message)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Trade Server Id
  int tsId = GetIntNumber(&ttMessage->msgContent[currentIndex]);

  // Find Trade Server index
  int tsIndex = FindTradeServerIndex(tsId);

  if (tsIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "Invalid Trade Server Id = %d\n", tsId);

    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Received message to request %s reset current status\n", tradeServersInfo.config[tsIndex].description);

  // Update current index
  currentIndex += 4;

  // type (4 bytes)
  int type = GetIntNumber(&ttMessage->msgContent[currentIndex]);

  if (type == NUM_STUCK_INDEX)
  {
    TraceLog(DEBUG_LEVEL, "--> Reset stuck position\n");
  }
  else if (type == BUYING_POWER_INDEX)
  {
    TraceLog(DEBUG_LEVEL, "--> Reset buying power\n");
  }
  else if (type == NUM_LOSER_INDEX)
  {
    TraceLog(DEBUG_LEVEL, "--> Reset consecutive loser\n");
  }
  else
  {
    TraceLog(ERROR_LEVEL, "--> INVALID REQUESTED: type = %d\n", type);

    return ERROR;
  }

  // Call function to reset TS current status
  SendResetTSCurrentStatusToTS(tsIndex, type);

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForResetASCurrentStatusFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForResetASCurrentStatusFromTT(void *message)
{
  TraceLog(DEBUG_LEVEL, "Received message to reset AS current status\n");

  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // type (4 bytes)
  int type = GetIntNumber(&ttMessage->msgContent[currentIndex]);
  currentIndex += 4;
  
  int account = GetIntNumber(&ttMessage->msgContent[currentIndex]);

  if (type == NUM_STUCK_INDEX)
  {
    TraceLog(DEBUG_LEVEL, "--> Reset stuck position (all accounts)\n");
  }
  else if (type == BUYING_POWER_INDEX)
  {
    TraceLog(DEBUG_LEVEL, "--> Reset buying power (account: %d)\n", account);
    if ((account < 1) || (account > MAX_ACCOUNT))
    {
      TraceLog(ERROR_LEVEL, "--> INVALID REQUESTED: account = %d\n", account);
      return ERROR;
    }
  }
  else if (type == NUM_LOSER_INDEX)
  {
    TraceLog(DEBUG_LEVEL, "--> Reset consecutive loser (all accounts)\n");
  }
  else
  {
    TraceLog(ERROR_LEVEL, "--> INVALID REQUESTED: type = %d\n", type);

    return ERROR;
  }
  
  // Call function to reset AS current status
  ProcessResetASCurrentStatus(type, account - 1);

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForConnectBookFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForConnectBookFromTT(void *message, int ttIndex)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Trade Server Id
  int tsId = GetIntNumber(&ttMessage->msgContent[currentIndex]);

  // Find Trade Server index
  int tsIndex = FindTradeServerIndex(tsId);

  if (tsIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "Invalid Trade Server Id = %d\n", tsId);

    return ERROR;
  }

  // Update current index
  currentIndex += 4;

  // ECN Order Index (4 bytes)
  int ECNIndex = GetIntNumber(&ttMessage->msgContent[currentIndex]);
  ECNIndex = TT_TO_TS_BOOK_INDEX_MAPPING[ECNIndex];

  TraceLog(DEBUG_LEVEL, "Received message to request %s connect to ECN Book, ECNIndex = %d\n", tradeServersInfo.config[tsIndex].description, ECNIndex);

  // Call function to connect to ECN Book from TS
  if (SendConnectBookToTS(tsIndex, ECNIndex) == SUCCESS)
  {
    //Update connect to ECN book of TS event log to database
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    sprintf(activityDetail, "%d,0,%d%02d", traderToolsInfo.connection[ttIndex].currentState, tsId, ECNIndex + 20 + 1);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_BOOK, activityDetail);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForDisconnectBookFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForDisconnectBookFromTT(void *message, int ttIndex)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Trade Server Id
  int tsId = GetIntNumber(&ttMessage->msgContent[currentIndex]);

  // Find Trade Server index
  int tsIndex = FindTradeServerIndex(tsId);

  if (tsIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "Invalid Trade Server Id = %d\n", tsId);

    return ERROR;
  }

  // Update current index
  currentIndex += 4;

  // ECN Order Index (4 bytes)
  int ECNIndex = GetIntNumber(&ttMessage->msgContent[currentIndex]);
  ECNIndex = TT_TO_TS_BOOK_INDEX_MAPPING[ECNIndex];

  TraceLog(DEBUG_LEVEL, "Received message to request %s disconnect to ECN Book, ECNIndex = %d\n", tradeServersInfo.config[tsIndex].description, ECNIndex);

  // Call function to disconnect to ECN Book from TS
  if (SendDisconnectBookToTS(tsIndex, ECNIndex) == SUCCESS)
  {
    //Update connect to ECN book of TS event log to database
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    sprintf(activityDetail, "%d,1,%d%02d", traderToolsInfo.connection[ttIndex].currentState, tsId, ECNIndex + 20 + 1);
    
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_BOOK, activityDetail);
  }
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForConnectAllBookFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForConnectAllBookFromTT(void *message, int ttIndex)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Trade Server Id
  int tsId = GetIntNumber(&ttMessage->msgContent[currentIndex]);

  // Find Trade Server index
  int tsIndex = FindTradeServerIndex(tsId);

  if (tsIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "Invalid Trade Server Id = %d\n", tsId);

    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Received message to request %s connect to all ECN Book\n", tradeServersInfo.config[tsIndex].description);

  // Call function to connect to ECN all Book from TS
  if (ProcessTSConnectAllECNBook(tsIndex) == SUCCESS)
  {
    //Update connect to ECN book of TS event log to database
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    sprintf(activityDetail, "%d,0,%d20", traderToolsInfo.connection[ttIndex].currentState, tsId);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_BOOK, activityDetail);
  }
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForDisconnectAllBookFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForDisconnectAllBookFromTT(void *message, int ttIndex)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Trade Server Id
  int tsId = GetIntNumber(&ttMessage->msgContent[currentIndex]);

  // Find Trade Server index
  int tsIndex = FindTradeServerIndex(tsId);

  if (tsIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "Invalid Trade Server Id = %d\n", tsId);

    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Received message to request %s disconnect to all ECN Book\n", tradeServersInfo.config[tsIndex].description);

  // Call function to disconnect to ECN Book from TS
  if (ProcessTSDisconnectAllECNBook(tsIndex) == SUCCESS)
  {
    //Update connect to ECN book of TS event log to database
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    sprintf(activityDetail, "%d,1,%d20", traderToolsInfo.connection[ttIndex].currentState, tsId);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_BOOK, activityDetail);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForConnectOrderFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForConnectOrderFromTT(void *message, int ttIndex)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Trade Server Id
  int tsId = GetIntNumber(&ttMessage->msgContent[currentIndex]);

  // Find Trade Server index
  int tsIndex = FindTradeServerIndex(tsId);

  if (tsIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "Invalid Trade Server Id = %d\n", tsId);

    return ERROR;
  }

  // Update current index
  currentIndex += 4;

  // ECN Order Index (4 bytes)
  int ECNIndex = GetIntNumber(&ttMessage->msgContent[currentIndex]);
  ECNIndex = TT_TO_TS_ORDER_INDEX_MAPPING[ECNIndex];

  TraceLog(DEBUG_LEVEL, "Received message to request %s connect to ECN Order, ECNIndex = %d\n", tradeServersInfo.config[tsIndex].description, ECNIndex);

  // Call function to connect to ECN Order from TS
  if (SendConnectOrderToTS(tsIndex, ECNIndex) == SUCCESS)
  {
    //Update enable trading on all ECN
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    sprintf(activityDetail, "%d,0,%d%02d", traderToolsInfo.connection[ttIndex].currentState, tsId, ECNIndex + 40 + 1);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_ORDER, activityDetail);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForDisconnectOrderFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForDisconnectOrderFromTT(void *message, int ttIndex)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Trade Server Id
  int tsId = GetIntNumber(&ttMessage->msgContent[currentIndex]);

  // Find Trade Server index
  int tsIndex = FindTradeServerIndex(tsId);

  if (tsIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "Invalid Trade Server Id = %d\n", tsId);

    return ERROR;
  }

  // Update current index
  currentIndex += 4;

  // ECN Order Index (4 bytes)
  int ECNIndex = GetIntNumber(&ttMessage->msgContent[currentIndex]);
  ECNIndex = TT_TO_TS_ORDER_INDEX_MAPPING[ECNIndex];

  TraceLog(DEBUG_LEVEL, "Received message to request %s disconnect to ECN Order, ECNIndex = %d\n", tradeServersInfo.config[tsIndex].description, ECNIndex);

  // Call function to disconnect to ECN Order from TS
  if (SendDisconnectOrderToTS(tsIndex, ECNIndex) == SUCCESS)
  {
    //Update enable trading on all ECN
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    sprintf(activityDetail, "%d,1,%d%02d", traderToolsInfo.connection[ttIndex].currentState, tsId, ECNIndex + 40 + 1);
    
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_ORDER, activityDetail);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForConnectAllOrderFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForConnectAllOrderFromTT(void *message, int ttIndex)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Trade Server Id
  int tsId = GetIntNumber(&ttMessage->msgContent[currentIndex]);

  // Find Trade Server index
  int tsIndex = FindTradeServerIndex(tsId);

  if (tsIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "Invalid Trade Server Id = %d\n", tsId);

    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Received message to request %s connect to all ECN Order\n", tradeServersInfo.config[tsIndex].description);

  // Call function to connect to ECN all Order from TS
  if (ProcessTSConnectAllECNOrder(tsIndex) == SUCCESS)
  {
    //connect to all orders on TS
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    sprintf(activityDetail, "%d,0,%d30", traderToolsInfo.connection[ttIndex].currentState, tsId);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_ORDER, activityDetail);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForDisconnectAllOrderFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForDisconnectAllOrderFromTT(void *message, int ttIndex)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Trade Server Id
  int tsId = GetIntNumber(&ttMessage->msgContent[currentIndex]);

  // Find Trade Server index
  int tsIndex = FindTradeServerIndex(tsId);

  if (tsIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "Invalid Trade Server Id = %d\n", tsId);

    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Received message to request %s disconnect to all ECN Order\n", tradeServersInfo.config[tsIndex].description);

  // Call function to disconnect to ECN Order from TS
  if (ProcessTSDisconnectAllECNOrder(tsIndex) == SUCCESS)
  {
    //disconnect to all orders on TS
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    sprintf(activityDetail, "%d,1,%d30", traderToolsInfo.connection[ttIndex].currentState, tsId);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_ORDER, activityDetail);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForASConnectOrderFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForASConnectOrderFromTT(void *message, int ttIndex)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // ECN Order Index (4 bytes)
  int ECNIndex = GetIntNumber(&ttMessage->msgContent[currentIndex]);
  ECNIndex = TT_TO_AS_ORDER_INDEX_MAPPING[ECNIndex];

  TraceLog(DEBUG_LEVEL, "Received request to AS connect to ECN order from TT, ECNIndex = %d\n", ECNIndex);

  // Call function to AS connect to ECN Order
  if (ProcessASConnectToOrderECN(ECNIndex) == SUCCESS)
  {
    //Update connect order on AS
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    sprintf(activityDetail, "%d,0,1%d", traderToolsInfo.connection[ttIndex].currentState, ECNIndex + 1);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_ORDER, activityDetail);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForASDisconnectOrderFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForASDisconnectOrderFromTT(void *message, int ttIndex)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // ECN Order Index (4 bytes)
  int ECNIndex = GetIntNumber(&ttMessage->msgContent[currentIndex]);
  ECNIndex = TT_TO_AS_ORDER_INDEX_MAPPING[ECNIndex];

  TraceLog(DEBUG_LEVEL, "Received request to AS disconnect to ECN order from TT, ECNIndex = %d\n", ECNIndex);

  // Call function to AS disconnect to ECN Order
  if (ProcessASDisconnectToOrderECN(ECNIndex) == SUCCESS)
  {
    //Update disconnect order on AS
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    sprintf(activityDetail, "%d,1,1%d", traderToolsInfo.connection[ttIndex].currentState, ECNIndex + 1);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_ORDER, activityDetail);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForASConnectAllOrderFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForASConnectAllOrderFromTT(void *message, int ttIndex)
{
  TraceLog(DEBUG_LEVEL, "Received request to AS connect to all ECN order from TT ...\n");

  // Call function to AS connect to all ECN Order
  if (ProcessASConnectToAllOrderECN() == SUCCESS)
  {
    //connect to all orders on AS
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    sprintf(activityDetail, "%d,0,10", traderToolsInfo.connection[ttIndex].currentState);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_ORDER, activityDetail);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForASDisconnectAllOrderFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForASDisconnectAllOrderFromTT(void *message, int ttIndex)
{
  TraceLog(DEBUG_LEVEL, "Received request to AS disconnect to all ECN order from TT ...\n");

  // Call function to AS disconnect to all ECN Order
  if (ProcessASDisconnectToAllOrderECN() == SUCCESS)
  {
    // disconnect to all orders on AS
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    sprintf(activityDetail, "%d,1,10", traderToolsInfo.connection[ttIndex].currentState);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_ORDER, activityDetail);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForSetTSConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForSetTSConf(void *message)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Trade Server Id
  int tsId = GetIntNumber(&ttMessage->msgContent[currentIndex]);

  // Find Trade Server index
  int tsIndex = FindTradeServerIndex(tsId);

  if (tsIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "Invalid Trade Server Id = %d\n", tsId);

    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Set configuration of %s\n", tradeServersInfo.config[tsIndex].description);

  // Block content = Trade Server configuration
  memcpy((unsigned char *)&tradeServersInfo.config[tsIndex], &ttMessage->msgContent[currentIndex], sizeof(t_TradeServerConf));

  // Add code here to update Trade Server configuration
  SaveTradeServersConf(tsIndex);

  // Send Trade Server configuration to all Trader Tool
  SendTSConfToAllTT();

  return SUCCESS;
}
/****************************************************************************
- Function name:  ProcessTTMessageForSetAlertConf
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessTTMessageForSetAlertConf(void *message)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  TraceLog(DEBUG_LEVEL, "Recieve Alert config from TT\n");

  // Update current index
  currentIndex += 4;

  memcpy(&AlertConf.pmExitedPositionThreshhold, &ttMessage->msgContent[currentIndex], 4);
  currentIndex += 4;

  memcpy(&AlertConf.consecutiveLossThreshhold, &ttMessage->msgContent[currentIndex], 4);
  currentIndex += 4;

  memcpy(&AlertConf.stucksThreshhold, &ttMessage->msgContent[currentIndex], 4);
  currentIndex += 4;

  memcpy(&AlertConf.singleServerTradesThreshhold, &ttMessage->msgContent[currentIndex], 4);
  currentIndex += 4;

  memcpy(&AlertConf.gtrAmountThreshhold, &ttMessage->msgContent[currentIndex], 8);
  currentIndex += 8;

  memcpy(&AlertConf.gtrSecondThreshhold, &ttMessage->msgContent[currentIndex], 4);
  currentIndex += 4;

  memcpy(&AlertConf.incompleteTradeThreshhold, &ttMessage->msgContent[currentIndex], 4);
  currentIndex += 4;

  memcpy(&AlertConf.repetitionTimeThreshhold, &ttMessage->msgContent[currentIndex], 4);
  currentIndex += 4;

  memcpy(&AlertConf.positionWithoutOrderThreshhold, &ttMessage->msgContent[currentIndex], 4);
  currentIndex += 4;

  memcpy(&AlertConf.pmOrderRateThreshhold, &ttMessage->msgContent[currentIndex], 4);
  currentIndex += 4;

  memcpy(&AlertConf.buyingPowerAmountThreshhold, &ttMessage->msgContent[currentIndex], 8);
  currentIndex += 8;

  memcpy(&AlertConf.buyingPowerSecondsThreshhold, &ttMessage->msgContent[currentIndex], 4);
  currentIndex += 4;

  int i = 0;

  memcpy(&AlertConf.noTradeAlertMgmt.count, &ttMessage->msgContent[currentIndex], 4);
  currentIndex += 4;

  if((AlertConf.noTradeAlertMgmt.count <= 0) ||
     (AlertConf.noTradeAlertMgmt.count > MAX_NO_TRADE_ALERT_MILESTONE))
  {
    TraceLog(ERROR_LEVEL, "Invalid alert config count = %d\n", AlertConf.noTradeAlertMgmt.count);
    return 0;
  }

  for(i = 0; i < AlertConf.noTradeAlertMgmt.count; i++)
  {
    memcpy(&AlertConf.noTradeAlertMgmt.itemList[i].startInSecond, &ttMessage->msgContent[currentIndex], 4);
    currentIndex += 4;

    if((i >= 1) &&
       (AlertConf.noTradeAlertMgmt.itemList[i].startInSecond <= AlertConf.noTradeAlertMgmt.itemList[i - 1].startInSecond))
    {
      TraceLog(ERROR_LEVEL, "No trade alert time must be ascendent\n");

      return ERROR;
    }

    memcpy(&AlertConf.noTradeAlertMgmt.itemList[i].durationInMinute, &ttMessage->msgContent[currentIndex], 4);
    currentIndex += 4;
  }

  // Update Trade Time Alert Config
  UpdateTradeTimeAlertConfig();

  // Save data to the file
  SaveAlertConf();

  // Send alert config to all TT
  SendAlertConfigToAllDaedalus();
  SendAlertConfigToAllTT();
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForSetTTConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForSetTTConf(void *message)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Block content = Trader Tools configuration
  memcpy((unsigned char *)&traderToolsInfo.config, &ttMessage->msgContent[currentIndex], sizeof(t_TraderToolConf));

  // Save & send Trader Tool configuration to all Trader Tool
  if (SaveTraderToolsConf() == SUCCESS)
  {
    SendTTConfToAllTT();
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForSetASOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForSetASOrder(void *message)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Block content = Max stuck positions + Max consecutive losers
  // ECN Order Index (4 bytes)
  int ECNIndex = GetIntNumber(&ttMessage->msgContent[currentIndex]);
  ECNIndex = TT_TO_AS_ORDER_INDEX_MAPPING[ECNIndex];

  TraceLog(DEBUG_LEVEL, "Set AS Order ECNIndex = %d\n", ECNIndex);

  // Update current index
  currentIndex += 4;

  // ECN Order conf
  switch (ECNIndex)
  {
    case AS_NASDAQ_RASH_INDEX:
      // ipAddress
      memcpy(NASDAQ_RASH_Config.ipAddress, &ttMessage->msgContent[currentIndex], MAX_LINE_LEN);
      currentIndex += MAX_LINE_LEN;

      // port
      NASDAQ_RASH_Config.port = GetIntNumber(&ttMessage->msgContent[currentIndex]);
      currentIndex += 4;

      // userName
      memcpy(NASDAQ_RASH_Config.userName, &ttMessage->msgContent[currentIndex], MAX_LINE_LEN);
      currentIndex += MAX_LINE_LEN;

      // password
      memcpy(NASDAQ_RASH_Config.password, &ttMessage->msgContent[currentIndex], MAX_LINE_LEN);
      currentIndex += MAX_LINE_LEN;
      break;
    
    case AS_ARCA_DIRECT_INDEX:
      // ipAddress
      memcpy(ARCA_DIRECT_Config.ipAddress, &ttMessage->msgContent[currentIndex], MAX_LINE_LEN);
      currentIndex += MAX_LINE_LEN;

      // port
      ARCA_DIRECT_Config.port = GetIntNumber(&ttMessage->msgContent[currentIndex]);
      currentIndex += 4;

      // userName
      memcpy(ARCA_DIRECT_Config.userName, &ttMessage->msgContent[currentIndex], MAX_LINE_LEN);
      currentIndex += MAX_LINE_LEN;

      // password
      memcpy(ARCA_DIRECT_Config.password, &ttMessage->msgContent[currentIndex], MAX_LINE_LEN);
      currentIndex += MAX_LINE_LEN;
      break;

    default:
      TraceLog(ERROR_LEVEL, "Invalid message format of set AS order conf, ECNIndex = %d\n", ECNIndex);
      break;
  }

  // Call function save AS Order to file
  ProcessSaveASOrderConf(ECNIndex);

  // Send AS order configuration to all Trader Tool
  SendASOrderConfToAllTT(ECNIndex);

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForSetASGlobalConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForSetASGlobalConf(void *message, int ttIndex)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;
  int account;
  double BackupBuyingPowerOnAccount[MAX_ACCOUNT];
  int maxCross = traderToolsInfo.globalConf.maxCross;
  double asVolatile = traderToolsInfo.globalConf.asVolatileThreshold;
  double tsVolatile = traderToolsInfo.globalConf.tsVolatileThreshold;
  double asDisabled = traderToolsInfo.globalConf.asDisabledThreshold;
  double tsDisabled = traderToolsInfo.globalConf.tsDisabledThreshold;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;
  
  for (account = 0; account < MAX_ACCOUNT; account++)
  {
    BackupBuyingPowerOnAccount[account] = traderToolsInfo.globalConf.buyingPower[account];
  }

  // Block content = Max stuck positions + Max consecutive losers
  // Max stuck positions
  memcpy((unsigned char *)&traderToolsInfo.globalConf.maxStuck, &ttMessage->msgContent[currentIndex], 4);
  currentIndex += 4;

  // Max consecutive losers
  memcpy((unsigned char *)&traderToolsInfo.globalConf.maxLoser, &ttMessage->msgContent[currentIndex], 4);
  currentIndex += 4;

  // Maximum crosses per minute per TS
  memcpy((unsigned char *)&traderToolsInfo.globalConf.maxCross, &ttMessage->msgContent[currentIndex], 4);
  currentIndex += 4;

  // AS Volatile threshold
  memcpy((unsigned char *)&traderToolsInfo.globalConf.asVolatileThreshold, &ttMessage->msgContent[currentIndex], 8);
  currentIndex += 8;

  // AS Disabled threshold
  memcpy((unsigned char *)&traderToolsInfo.globalConf.asDisabledThreshold, &ttMessage->msgContent[currentIndex], 8);
  currentIndex += 8;
    
    // TS Volatile threshold
  memcpy((unsigned char *)&traderToolsInfo.globalConf.tsVolatileThreshold, &ttMessage->msgContent[currentIndex], 8);
  currentIndex += 8;

  // TS Disabled threshold
  memcpy((unsigned char *)&traderToolsInfo.globalConf.tsDisabledThreshold, &ttMessage->msgContent[currentIndex], 8);
  currentIndex += 8;

  // Global buying power for account 1
  memcpy((unsigned char *)&traderToolsInfo.globalConf.buyingPower[FIRST_ACCOUNT], &ttMessage->msgContent[currentIndex], 8);
  currentIndex += 8;
  
  // Global buying power for account 2
  memcpy((unsigned char *)&traderToolsInfo.globalConf.buyingPower[SECOND_ACCOUNT], &ttMessage->msgContent[currentIndex], 8);
  currentIndex += 8;

  // Global buying power for account 3
  memcpy((unsigned char *)&traderToolsInfo.globalConf.buyingPower[THIRD_ACCOUNT], &ttMessage->msgContent[currentIndex], 8);
  currentIndex += 8;

  // Global buying power for account 4
  memcpy((unsigned char *)&traderToolsInfo.globalConf.buyingPower[FOURTH_ACCOUNT], &ttMessage->msgContent[currentIndex], 8);
  currentIndex += 8;

  // Fast Market ThresHold
  memcpy((unsigned char *)&FastMarketMgmt.thresHold, &ttMessage->msgContent[currentIndex], 4);
  
  // Save data to the file
  SaveASGlobalConf();

  // Send AS global configuration to all Trader Tool
  SendASGlobalConfToAllDaedalus();
  SendASGlobalConfToAllTT();

  //Update buying power setting event log to database
  char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
  
  int isUpdated = 0;
  for (account = 0; account < MAX_ACCOUNT; account++)
  {
    if (!feq(BackupBuyingPowerOnAccount[account], traderToolsInfo.globalConf.buyingPower[account]))
    {
      isUpdated = 1;
      
      //Update buying power setting event log to database
      sprintf(activityDetail, "%d,%d,%lf", traderToolsInfo.connection[ttIndex].currentState, account, traderToolsInfo.globalConf.buyingPower[account]);
      ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_BUYING_POWER_SETTING, activityDetail);
    }
  }
  
  if (isUpdated == 1)
  {
    IsMaxBPUpdated = YES;
    // Update buying power of TS
    TTIndexUpdatedBP = ttIndex;
    SendUpdateBuyingPowerToAllTS(MAX_ACCOUNT);
  }
  
  // Update MaxCross, TS-AS volatile/disabled threshold to database if requried
  int isThresholdUpdated = 0;
  if (maxCross != traderToolsInfo.globalConf.maxCross)
  {
    memset(activityDetail, 0, MAX_TRADER_LOG_LEN);
    sprintf(activityDetail, "%d,%d", traderToolsInfo.connection[ttIndex].currentState, traderToolsInfo.globalConf.maxCross);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_MAX_CROSS_PER_MIN_SETTING, activityDetail);
  }
  if (!feq(asVolatile, traderToolsInfo.globalConf.asVolatileThreshold))
  {
    isThresholdUpdated = 1;
    memset(activityDetail, 0, MAX_TRADER_LOG_LEN);
    sprintf(activityDetail, "%d,%.2lf", traderToolsInfo.connection[ttIndex].currentState, traderToolsInfo.globalConf.asVolatileThreshold);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_AS_VOLATILE_THRESHOLD_SETTING, activityDetail);
  }
  if (!feq(tsVolatile, traderToolsInfo.globalConf.tsVolatileThreshold))
  {
    isThresholdUpdated = 1;
    memset(activityDetail, 0, MAX_TRADER_LOG_LEN);
    sprintf(activityDetail, "%d,%.2lf", traderToolsInfo.connection[ttIndex].currentState, traderToolsInfo.globalConf.tsVolatileThreshold);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_TS_VOLATILE_THRESHOLD_SETTING, activityDetail);
  }
  if (!feq(asDisabled, traderToolsInfo.globalConf.asDisabledThreshold))
  {
    isThresholdUpdated = 1;
    memset(activityDetail, 0, MAX_TRADER_LOG_LEN);
    sprintf(activityDetail, "%d,%.2lf", traderToolsInfo.connection[ttIndex].currentState, traderToolsInfo.globalConf.asDisabledThreshold);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_AS_DISABLED_THRESHOLD_SETTING, activityDetail);
  }
  if (!feq(tsDisabled, traderToolsInfo.globalConf.tsDisabledThreshold))
  {
    isThresholdUpdated = 1;
    memset(activityDetail, 0, MAX_TRADER_LOG_LEN);
    sprintf(activityDetail, "%d,%.2lf", traderToolsInfo.connection[ttIndex].currentState, traderToolsInfo.globalConf.tsDisabledThreshold);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_TS_DISABLED_THRESHOLD_SETTING, activityDetail);
  }
  
  // Check to add symbol to disabled or volatile list
  if (isThresholdUpdated)
  {
    TraceLog(DEBUG_LEVEL, "ProcessTTMessageForSetASGlobalConf: Check to add symbol to volatile or disabled list...\n");
    int symbolIndex, tsIndex;
    for (symbolIndex = 0; symbolIndex < MAX_SYMBOL_TRADE; symbolIndex++)
    {
      for (tsIndex = 0; tsIndex < MAX_TRADE_SERVER_CONNECTIONS; tsIndex++)
      {
        if (fgt(ProfitLossPerSymbol[symbolIndex].asLoss, 0.00))
        {
          CheckToAddSymbolToVolatileOrDisabledList(RMList.symbolList[symbolIndex].symbol, symbolIndex, tsIndex);
        }
      }
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForVolatileSectionUpdate
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForVolatileSectionUpdate(void *message, t_ConnectionStatus *connection)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Block content:
  //  action (1 byte binary, 1: remove, 0: add, -1: reload)
  //  section (1 byte binary, 0: pre, 1:post, 2: intraday)
  //  symbol (8 byte symbol length)
  
  char action = ttMessage->msgContent[currentIndex++];
  if (action == -1) //reload
  {
    SendVolatileSectionsToTT(connection);
    return SUCCESS;
  }
  
  int section = ttMessage->msgContent[currentIndex++];
  char symbol[SYMBOL_LEN];
  strncpy(symbol, (char*) &ttMessage->msgContent[currentIndex++], SYMBOL_LEN);
  
  int symbolIndex;
  symbolIndex = GetStockSymbolIndex(symbol);
  if (symbolIndex < 0)
  {
    TraceLog(ERROR_LEVEL, "(TT requests to %s a volatile symbol): invalid symbol %.8s\n", (action == ADD_SYMBOL) ? "add" : "remove", symbol);
    return ERROR;
  }
  
  char preList[MAX_STOCK_SYMBOL * SYMBOL_LEN];
  char postList[MAX_STOCK_SYMBOL * SYMBOL_LEN];
  char intradayList[MAX_STOCK_SYMBOL * SYMBOL_LEN];
  int countPre = 0, countPost = 0, countIntraday = 0;
  int index, changed = 1;
  
  // Load file to buffer --> update buffer --> save file
  pthread_mutex_lock(&volatileMgmt.mutexLock);
    if (LoadVolatileSectionsFromFileToLists(preList, &countPre, postList, &countPost, intradayList, &countIntraday) == ERROR)
    {
      pthread_mutex_unlock(&volatileMgmt.mutexLock);
      return ERROR;
    }
    
    if (action == ADD_SYMBOL)
    {
      switch (section)
      {
        case 0:   // [PRE]
          for (index = 0; index < countPre; index++)
          {
            if (strncmp(&preList[index * SYMBOL_LEN], symbol, SYMBOL_LEN) == 0)
            {
              // Symbol is already in the list
              changed = 0;
              break;
            }
          }
          
          if (changed == 1)
          {
            strncpy(&preList[countPre * SYMBOL_LEN], symbol, SYMBOL_LEN);
            countPre++;
          }
          break;
        case 1:   // [POST]
          for (index = 0; index < countPost; index++)
          {
            if (strncmp(&postList[index * SYMBOL_LEN], symbol, SYMBOL_LEN) == 0)
            {
              // Symbol is already in the list
              changed = 0;
              break;
            }
          }
          
          if (changed == 1)
          {
            strncpy(&postList[countPost * SYMBOL_LEN], symbol, SYMBOL_LEN);
            countPost++;
          }
          break;
        case 2:   // [INTRADAY]
          for (index = 0; index < countIntraday; index++)
          {
            if (strncmp(&intradayList[index * SYMBOL_LEN], symbol, SYMBOL_LEN) == 0)
            {
              // Symbol is already in the list
              changed = 0;
              break;
            }
          }
          
          if (changed == 1)
          {
            strncpy(&intradayList[countIntraday * SYMBOL_LEN], symbol, SYMBOL_LEN);
            countIntraday++;
          }
          break;
      }
    }
    else if (action == REMOVE_SYMBOL)
    {
      changed = 0;
      switch (section)
      {
        case 0:   // [PRE]
          for (index = 0; index < countPre; index++)
          {
            if (strncmp(&preList[index * SYMBOL_LEN], symbol, SYMBOL_LEN) == 0)
            {
              //found symbol to remove
              preList[index * SYMBOL_LEN] = 0;  //set to 0, it will not be saved to file
              changed = 1;
            }
          }
          break;
        case 1:   // [POST]
          for (index = 0; index < countPost; index++)
          {
            if (strncmp(&postList[index * SYMBOL_LEN], symbol, SYMBOL_LEN) == 0)
            {
              //found symbol to remove
              postList[index * SYMBOL_LEN] = 0; //set to 0, it will not be saved to file
              changed = 1;
            }
          }
          break;
        case 2:   // [INTRADAY]
          for (index = 0; index < countIntraday; index++)
          {
            if (strncmp(&intradayList[index * SYMBOL_LEN], symbol, SYMBOL_LEN) == 0)
            {
              //found symbol to remove
              intradayList[index * SYMBOL_LEN] = 0; //set to 0, it will not be saved to file
              changed = 1;
            }
          }
          break;
      }
    }
    else
    {
      TraceLog(ERROR_LEVEL, "Invalid set volatile symbol from TT, symbol = %.8s, action = %d\n", symbol, action);
      pthread_mutex_unlock(&volatileMgmt.mutexLock);
      return ERROR;
    }
    
    if (changed == 1)
    {
      SaveVolatileSectionsToFile(preList, countPre, postList, countPost, intradayList, countIntraday);
    }
  pthread_mutex_unlock(&volatileMgmt.mutexLock);
  
  if (changed == 1)
  {
    SendVolatileSectionsToAllDaedalus();
    SendVolatileSectionsToAllTT();
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForAllVolatileToggle
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForAllVolatileToggle(void *message)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Block content:
  //  number of schedule (1 byte binary)
  //  On time:  4 bytes (hr*3600 + min*60 + sec)
  //  Off time:   4 bytes (hr*3600 + min*60 + sec)
    
  char count = ttMessage->msgContent[currentIndex++];
  if (count < 0)
  {
    TraceLog(ERROR_LEVEL, "Received 'All Volatile' toggle from TT with invalid number of schedules (%d)\n", count);
    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Received 'All Volatile' toggle schedule from TT, number of schedule: %d\n", count);
  int i;
  
  pthread_mutex_lock(&volatileMgmt.mutexLock);
    volatileMgmt.numAllVolatileToggle = count;
    for (i = 0; i < count; i++)
    {
      memcpy(&volatileMgmt.allVolatileOnTime[i], &ttMessage->msgContent[currentIndex], 4);
      currentIndex += 4;
      memcpy(&volatileMgmt.allVolatileOffTime[i], &ttMessage->msgContent[currentIndex], 4);
      currentIndex += 4;
    }
  
    SaveAllVolatileToggleSchedulesToFile();
  pthread_mutex_unlock(&volatileMgmt.mutexLock);
  
  SendAllVolatileToggleScheduleToAllDaedalus();
  SendAllVolatileToggleScheduleToAllTT();
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForVolatileSymbolUpdate
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForVolatileSymbolUpdate(void *message)
{
  char list[MAX_STOCK_SYMBOL * SYMBOL_LEN];
  int count = 0;
  int changed = 1;
  
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  pthread_mutex_lock(&volatileMgmt.mutexLock);
  if (LoadVolatileSymbolsFromFileToList(list, &count) == ERROR)
  {
    pthread_mutex_unlock(&volatileMgmt.mutexLock);
    return ERROR;
  }
  
  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Block content = action (1 byte binary 1: remove, 0:add) + symbol (8 byte symbol length)
  
  int action = ttMessage->msgContent[currentIndex++];
  
  char symbol[SYMBOL_LEN];
  strncpy(symbol, (char*) &ttMessage->msgContent[currentIndex++], SYMBOL_LEN);
  
  int symbolIndex;
  if (action == ADD_SYMBOL) 
  {
    symbolIndex = GetStockSymbolIndex(symbol);
    if (symbolIndex < 0)
    {
      TraceLog(ERROR_LEVEL, "(TT requests to add a new volatile symbol): invalid symbol '%.8s'\n", symbol);
      pthread_mutex_unlock(&volatileMgmt.mutexLock);
      return ERROR;
    }
    
    int index;
    for (index = 0; index < count; index++)
    {
      if (strncmp(&list[index * SYMBOL_LEN], symbol, SYMBOL_LEN) == 0)
      {
        // Symbol is already in the list
        changed = 0;
        break;
      }
    }
    
    if (changed == 1)
    {
      strncpy(&list[count * SYMBOL_LEN], symbol, SYMBOL_LEN);
      count++;
      
      SaveVolatileSymbolListToFile(list, count);
    }
  }
  else if (action == REMOVE_SYMBOL)
  {
    changed = 0;
    
    symbolIndex = GetStockSymbolIndex(symbol);
    if (symbolIndex < 0)
    {
      TraceLog(ERROR_LEVEL, "(TT requests to remove a volatile symbol): invalid symbol '%.8s'\n", symbol);
      pthread_mutex_unlock(&volatileMgmt.mutexLock);
      return ERROR;
    }
    
    int index;
    for (index = 0; index < count; index++)
    {
      if (strncmp(&list[index * SYMBOL_LEN], symbol, SYMBOL_LEN) == 0)
      {
        //found symbol to remove
        list[index * SYMBOL_LEN] = 0; //set to 0, it will not be saved to file
        changed = 1;
      }
    }
    
    if (changed == 1)
    {
      SaveVolatileSymbolListToFile(list, count);
    }

    // Reset flag profit loss
    if (ProfitLossPerSymbol[symbolIndex].listID == VOLATILE_LIST_ID)
    {
      int tsIndex;
      for (tsIndex = 0; tsIndex < MAX_TRADE_SERVER_CONNECTIONS; tsIndex++)
      {
        if (fgt(ProfitLossPerSymbol[symbolIndex].tsLoss[tsIndex], 0.00))
        {
          SaveLossBySymbol(symbol, tsIndex, ProfitLossPerSymbol[symbolIndex].tsLoss[tsIndex]);
        }
      }
      
      memset(&ProfitLossPerSymbol[symbolIndex], 0, sizeof(t_ProfitLossPerSymbol));
      TraceLog(DEBUG_LEVEL, "Remove %s from volaitle list, reset loss counter for it\n", symbol);
    }
  }
  else
  {
    TraceLog(ERROR_LEVEL, "Invalid set volatile symbol from TT, symbol = %.8s, action = %d\n", symbol, action);
    pthread_mutex_unlock(&volatileMgmt.mutexLock);
    return ERROR;
  } 

  pthread_mutex_unlock(&volatileMgmt.mutexLock);
  
  if (changed == 1)
  {
    SendVolatileSymbolListToAllDaedalus();
    SendVolatileSymbolListToAllTT();
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForIgnoreStockList
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForIgnoreStockList(void *message, int ttIndex)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Block content = type(4 bytes: Add = 0, Remove = 1) + a symbol (8 bytes)
  
  // Type
  int type = GetIntNumber(&ttMessage->msgContent[currentIndex]);

  // Update current index
  currentIndex += 4;

  // Symbol
  char symbol[SYMBOL_LEN];

  strncpy(symbol, (char *) &ttMessage->msgContent[currentIndex], SYMBOL_LEN);

  // Update current index
  currentIndex += SYMBOL_LEN;

  int i, countSymbol = 0;
  char tmpSymbolList[MAX_STOCK_SYMBOL][SYMBOL_LEN];
  char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
  
  if (type == ADD_SYMBOL)
  {
    countSymbol = 0;

    memcpy(tmpSymbolList[countSymbol], symbol, SYMBOL_LEN);
    countSymbol ++;
    
    for (i = 0; i < IgnoreStockList.countStock; i++)
    {
      memcpy(tmpSymbolList[countSymbol], IgnoreStockList.stockList[i], SYMBOL_LEN);
      countSymbol ++;
    }

    // Update ignored symbol list
    IgnoreStockList.countStock = 0;

    for (i = 0; i < countSymbol; i++)
    {
      memcpy(IgnoreStockList.stockList[IgnoreStockList.countStock], tmpSymbolList[i], SYMBOL_LEN);
      IgnoreStockList.countStock ++;
    }
  }
  else if (type == REMOVE_SYMBOL)
  {
    // Reset flag lisID in RMList
    int symbolIndex = GetStockSymbolIndex(symbol);
    if (symbolIndex == -1)
    {
      TraceLog(ERROR_LEVEL, "(ProcessTTMessageForIgnoreStockList): Could not get symbol index of symbol '%.8s'\n", symbol);
      return ERROR;
    }
    
    // Reset profit loss
    if (ProfitLossPerSymbol[symbolIndex].listID == DISABLED_LIST_ID)
    {
      int tsIndex;
      for (tsIndex = 0; tsIndex < MAX_TRADE_SERVER_CONNECTIONS; tsIndex++)
      {
        if (fgt(ProfitLossPerSymbol[symbolIndex].tsLoss[tsIndex], 0.00))
        {
          SaveLossBySymbol(symbol, tsIndex, ProfitLossPerSymbol[symbolIndex].tsLoss[tsIndex]);
        }
      }
      
      memset(&ProfitLossPerSymbol[symbolIndex], 0, sizeof(t_ProfitLossPerSymbol));
      TraceLog(DEBUG_LEVEL, "Remove %s from disabled list, reset loss counter for it\n", symbol);
    }

    countSymbol = 0;

    for (i = 0; i < IgnoreStockList.countStock; i++)
    {
      if (strncmp(IgnoreStockList.stockList[i], symbol, SYMBOL_LEN) != 0)
      {
        memcpy(tmpSymbolList[countSymbol], IgnoreStockList.stockList[i], SYMBOL_LEN);
        countSymbol ++;
      }
    }

    // Update ignored symbol list
    IgnoreStockList.countStock = 0;

    for (i = 0; i < countSymbol; i++)
    {
      memcpy(IgnoreStockList.stockList[IgnoreStockList.countStock], tmpSymbolList[i], SYMBOL_LEN);
      IgnoreStockList.countStock ++;
    }
  }
  else
  {
    TraceLog(ERROR_LEVEL, "Invalid set symbol ignore, symbol = %.8s, type = %d\n", symbol, type);

    return ERROR;
  } 

  // Save data to the file
  SaveIgnoreStockList();

  // Send ignored symbol list to all Trade Server
  if (SendIgnoreStockListToAllTS(type, symbol) == SUCCESS)
  {
    // Update TT event log to database
    sprintf(activityDetail, "%d,%d,%.8s", traderToolsInfo.connection[ttIndex].currentState, type, symbol);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_IGNORE_STOCK_LIST, activityDetail);
  }

  // Send ignored symbol list to all Trader Tool
  SendIgnoreStockListToAllTT();
  SendTSIgnoreSymbolListToAllDaedalus();

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForIgnoreStockRangesList
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForIgnoreStockRangesList(void *message)
{

  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Block content = type(4 bytes: Add = 0, Remove = 1) + a symbol (8 bytes)
  
  // Type
  int type = GetIntNumber(&ttMessage->msgContent[currentIndex]);

  // Update current index
  currentIndex += 4;

  // Symbol
  char symbol[SYMBOL_LEN];

  strncpy(symbol, (char *) &ttMessage->msgContent[currentIndex], SYMBOL_LEN);

  // Update current index
  currentIndex += SYMBOL_LEN;

  int i, countSymbol = 0;
  char tmpSymbolList[MAX_STOCK_SYMBOL][SYMBOL_LEN];
  

  if (type == ADD_SYMBOL)
  {
    countSymbol = 0;

    memcpy(tmpSymbolList[countSymbol], symbol, SYMBOL_LEN);
    countSymbol ++;
    for (i = 0; i < IgnoreStockRangesList.countStock; i++)
    {
      memcpy(tmpSymbolList[countSymbol], IgnoreStockRangesList.stockList[i], SYMBOL_LEN);
      countSymbol ++;
    }

    // Update ignored symbol list
    IgnoreStockRangesList.countStock = 0;

    for (i = 0; i < countSymbol; i++)
    {
      memcpy(IgnoreStockRangesList.stockList[IgnoreStockRangesList.countStock], tmpSymbolList[i], SYMBOL_LEN);
      IgnoreStockRangesList.countStock ++;
    }
  }
  else if (type == REMOVE_SYMBOL)
  {
    countSymbol = 0;

    for (i = 0; i < IgnoreStockRangesList.countStock; i++)
    {
      if (strncmp(IgnoreStockRangesList.stockList[i], symbol, SYMBOL_LEN) != 0)
      {
        memcpy(tmpSymbolList[countSymbol], IgnoreStockRangesList.stockList[i], SYMBOL_LEN);
        countSymbol ++;
      }
    }

    // Update ignored symbol list
    IgnoreStockRangesList.countStock = 0;

    for (i = 0; i < countSymbol; i++)
    {
      memcpy(IgnoreStockRangesList.stockList[IgnoreStockRangesList.countStock], tmpSymbolList[i], SYMBOL_LEN);
      IgnoreStockRangesList.countStock ++;
    }
  }
  else
  {
    TraceLog(ERROR_LEVEL, "Invalid set symbol ignore, symbol = %.8s, type = %d\n", symbol, type);

    return ERROR;
  } 

  // Save data to the file
  SaveIgnoreStockRangesList();

  // Send ignored symbol list to all Trade Server
    SendIgnoreStockRangesListToAllTS(type, symbol);
  
  // Send ignored symbol list to all Trader Tool
  SendIgnoreStockRangesListToAllTT();
  SendIgnoreSymbolRangesListToAllDaedalus();

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForSetTSBookConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForSetTSBookConf(void *message)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;
  
  // Update current index
  currentIndex += 4;

  // Block content = Trade Server Id (4 bytes) + ECN Book Index (4 bytes) + ECN Book conf
  // Trade Server Id (4 bytes)
  int tsId = GetIntNumber(&ttMessage->msgContent[currentIndex]);

  // Find Trade Server index
  int tsIndex = FindTradeServerIndex(tsId);

  if (tsIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "Invalid Trade Server Id = %d\n", tsId);

    return ERROR;
  }

  // Update current index
  currentIndex += 4;

  // ECN Book Index (4 bytes)
  int ECNIndex = GetIntNumber(&ttMessage->msgContent[currentIndex]);
  ECNIndex = TT_TO_TS_BOOK_INDEX_MAPPING[ECNIndex];
  
  // Update current index
  currentIndex += 4;

  TraceLog(DEBUG_LEVEL, "Set Book config of %s, ECNIndex = %d\n", tradeServersInfo.config[tsIndex].description, ECNIndex);

  int type;

  // ECN Book configuration
  switch (ECNIndex)
  {
    // ARCA Book configuration
    case TS_ARCA_BOOK_INDEX:
      type = GetIntNumber(&ttMessage->msgContent[currentIndex]);
      currentIndex += 4;

      if (type == UPDATE_SOURCE_ID)
      {
        memcpy(tradeServersInfo.ECNBooksConf[tsIndex].ARCABookConf.sourceID, &ttMessage->msgContent[currentIndex], MAX_LINE_LEN);

        TraceLog(DEBUG_LEVEL, "Set ARCABookConf: source ID = %s\n", tradeServersInfo.ECNBooksConf[tsIndex].ARCABookConf.sourceID);

        // Call function set ARCA Book configuration to TS
        SetTSBookARCAConfToTS(tsIndex, type, -1);
      }
      else if (type == UPDATE_RETRANSMISSION_REQUEST)
      {
        int groupIndex = GetIntNumber(&ttMessage->msgContent[currentIndex]);
        currentIndex += 4;

        // Retransmission Request TCP/IP
        memcpy(tradeServersInfo.ECNBooksConf[tsIndex].ARCABookConf.retranConn[groupIndex].ip, &ttMessage->msgContent[currentIndex], MAX_LINE_LEN);
        currentIndex += MAX_LINE_LEN;

        tradeServersInfo.ECNBooksConf[tsIndex].ARCABookConf.retranConn[groupIndex].port = GetIntNumber(&ttMessage->msgContent[currentIndex]);
        currentIndex += 4;

        TraceLog(DEBUG_LEVEL, "Set ARCABookConf: groupIndex = %d, ip = %s, port = %d\n", groupIndex, tradeServersInfo.ECNBooksConf[tsIndex].ARCABookConf.retranConn[groupIndex].ip, tradeServersInfo.ECNBooksConf[tsIndex].ARCABookConf.retranConn[groupIndex].port);

        // Call function set ARCA Book configuration to TS
        SetTSBookARCAConfToTS(tsIndex, type, groupIndex);
      }
      else
      {
        TraceLog(ERROR_LEVEL, "Set ARCA FAST Book config to TS, type = %d\n", type);

        return ERROR;
      }

      return SUCCESS;
      break;
    
    // NASDAQ Book configuration
    case TS_NASDAQ_BOOK_INDEX:
      memcpy(tradeServersInfo.ECNBooksConf[tsIndex].NASDAQBookConf.UDP.ip, &ttMessage->msgContent[currentIndex], MAX_LINE_LEN);
      currentIndex += MAX_LINE_LEN;

      memcpy(&tradeServersInfo.ECNBooksConf[tsIndex].NASDAQBookConf.UDP.port, &ttMessage->msgContent[currentIndex], 4);
      currentIndex += 4;

      memcpy(tradeServersInfo.ECNBooksConf[tsIndex].NASDAQBookConf.TCP.ip, &ttMessage->msgContent[currentIndex], MAX_LINE_LEN);
      currentIndex += MAX_LINE_LEN;

      memcpy(&tradeServersInfo.ECNBooksConf[tsIndex].NASDAQBookConf.TCP.port, &ttMessage->msgContent[currentIndex], 4);
      currentIndex += 4;
      
      memcpy(&tradeServersInfo.ECNBooksConf[tsIndex].NASDAQBookConf.maxMsgLoss, &ttMessage->msgContent[currentIndex], 4);
      currentIndex += 4;

      TraceLog(DEBUG_LEVEL, "maxMsgLoss = %d\n", tradeServersInfo.ECNBooksConf[tsIndex].NASDAQBookConf.maxMsgLoss);

      break;
      
    case TS_NDBX_BOOK_INDEX:
      memcpy(tradeServersInfo.ECNBooksConf[tsIndex].NDAQBXBookConf.UDP.ip, &ttMessage->msgContent[currentIndex], MAX_LINE_LEN);
      currentIndex += MAX_LINE_LEN;

      memcpy(&tradeServersInfo.ECNBooksConf[tsIndex].NDAQBXBookConf.UDP.port, &ttMessage->msgContent[currentIndex], 4);
      currentIndex += 4;
      break;
      
    // BATSZ Book configuration
    case TS_BATSZ_BOOK_INDEX:
      memcpy(tradeServersInfo.ECNBooksConf[tsIndex].BATSZBookGRPConf.sessionSubId, &ttMessage->msgContent[currentIndex], MAX_LINE_LEN);
      currentIndex += MAX_LINE_LEN;

      memcpy(tradeServersInfo.ECNBooksConf[tsIndex].BATSZBookGRPConf.userName, &ttMessage->msgContent[currentIndex], MAX_LINE_LEN);
      currentIndex += MAX_LINE_LEN;

      memcpy(tradeServersInfo.ECNBooksConf[tsIndex].BATSZBookGRPConf.password, &ttMessage->msgContent[currentIndex], MAX_LINE_LEN);
      currentIndex += MAX_LINE_LEN;

      memcpy(tradeServersInfo.ECNBooksConf[tsIndex].BATSZBookGRPConf.retranRequestConn.ip, &ttMessage->msgContent[currentIndex], MAX_LINE_LEN);
      currentIndex += MAX_LINE_LEN;

      memcpy(&tradeServersInfo.ECNBooksConf[tsIndex].BATSZBookGRPConf.retranRequestConn.port, &ttMessage->msgContent[currentIndex], 4);
      currentIndex += 4;
      break;

    // NYSE Open Book configuration
    case TS_NYSE_BOOK_INDEX:
      type = GetIntNumber(&ttMessage->msgContent[currentIndex]);
      currentIndex += 4;

      if (type == UPDATE_SOURCE_ID)
      {
        memcpy(tradeServersInfo.ECNBooksConf[tsIndex].NYSEBookConf.sourceID, &ttMessage->msgContent[currentIndex], MAX_LINE_LEN);

        TraceLog(DEBUG_LEVEL, "Set NYSEBookConf: source ID = %s\n", tradeServersInfo.ECNBooksConf[tsIndex].NYSEBookConf.sourceID);

        // Call function set NYSE Book configuration to TS
        SetTSBookNYSEOpenUltraConfToTS(tsIndex, type, -1);
      }
      else if (type == UPDATE_RETRANSMISSION_REQUEST)
      {
        int groupIndex = GetIntNumber(&ttMessage->msgContent[currentIndex]);
        currentIndex += 4;

        memcpy(tradeServersInfo.ECNBooksConf[tsIndex].NYSEBookConf.nyseBook[groupIndex].retranIP, &ttMessage->msgContent[currentIndex], MAX_LINE_LEN);
        currentIndex += MAX_LINE_LEN;

        memcpy(&tradeServersInfo.ECNBooksConf[tsIndex].NYSEBookConf.nyseBook[groupIndex].retranPort, &ttMessage->msgContent[currentIndex], 4);
        currentIndex += 4;

        // Call function set NYSE Open Ultra Book configuration to TS
        SetTSBookNYSEOpenUltraConfToTS(tsIndex, type, groupIndex);
      }
      else
      {
        TraceLog(ERROR_LEVEL, "Set NYSE Open Ultra Book config to TS, type = %d\n", type);

        return ERROR;
      }

      return SUCCESS;
      break;    
    
    // EdgeX Book configuration
    case TS_EDGX_BOOK_INDEX:
      memcpy(tradeServersInfo.ECNBooksConf[tsIndex].EDGXBookGRPConf.userName, &ttMessage->msgContent[currentIndex], MAX_LINE_LEN);
      currentIndex += MAX_LINE_LEN;

      memcpy(tradeServersInfo.ECNBooksConf[tsIndex].EDGXBookGRPConf.password, &ttMessage->msgContent[currentIndex], MAX_LINE_LEN);
      currentIndex += MAX_LINE_LEN;

      memcpy(tradeServersInfo.ECNBooksConf[tsIndex].EDGXBookGRPConf.ip, &ttMessage->msgContent[currentIndex], MAX_LINE_LEN);
      currentIndex += MAX_LINE_LEN;

      memcpy(&tradeServersInfo.ECNBooksConf[tsIndex].EDGXBookGRPConf.port, &ttMessage->msgContent[currentIndex], 4);
      currentIndex += 4;
      break;
      
    case TS_EDGA_BOOK_INDEX:
      memcpy(tradeServersInfo.ECNBooksConf[tsIndex].EDGABookGRPConf.userName, &ttMessage->msgContent[currentIndex], MAX_LINE_LEN);
      currentIndex += MAX_LINE_LEN;

      memcpy(tradeServersInfo.ECNBooksConf[tsIndex].EDGABookGRPConf.password, &ttMessage->msgContent[currentIndex], MAX_LINE_LEN);
      currentIndex += MAX_LINE_LEN;

      memcpy(tradeServersInfo.ECNBooksConf[tsIndex].EDGABookGRPConf.ip, &ttMessage->msgContent[currentIndex], MAX_LINE_LEN);
      currentIndex += MAX_LINE_LEN;

      memcpy(&tradeServersInfo.ECNBooksConf[tsIndex].EDGABookGRPConf.port, &ttMessage->msgContent[currentIndex], 4);
      currentIndex += 4;
      break;
      
    // BYX Book configuration
    case TS_BYX_BOOK_INDEX:
    case TS_PSX_BOOK_INDEX:
    case TS_AMEX_BOOK_INDEX:
      // This feature is no longer supported
      break;

    // Invalid message
    default:
      TraceLog(ERROR_LEVEL, "Invalid set Book configuration message from TT, ECNIndex = %d\n", ECNIndex);
      return ERROR;
      break;
  }

  // Call function set Order configuration to TS
  SetTSBookConfToTS(tsIndex, ECNIndex);

  return SUCCESS;
}


/****************************************************************************
- Function name:  ProcessTTMessageForSetTSARCABookFeed
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForSetTSARCABookFeed(void *message)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;
  
  // Update current index
  currentIndex += 4;

  // Block content = Trade Server Id (4 bytes) + Feedname (A or B, 1 byte char)
  
  int tsId = GetIntNumber(&ttMessage->msgContent[currentIndex]);
  currentIndex += 4;
  
  char newFeedName = ttMessage->msgContent[currentIndex];
  
  // Find Trade Server index
  int tsIndex = FindTradeServerIndex(tsId);

  if (tsIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "Invalid Trade Server Id = %d\n", tsId);

    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Received request to switch ARCA Book to feed %c on TS %d\n", newFeedName, tsId);

  SetTSBookARCAFeedToTS(tsIndex, newFeedName);

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForSetTSOrderConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForSetTSOrderConf(void *message)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Block content = Trade Server Id (4 bytes) + ECN Order Index (4 bytes) + ECN Order conf
  // Trade Server Id (4 bytes)
  int tsId = GetIntNumber(&ttMessage->msgContent[currentIndex]);

  // Find Trade Server index
  int tsIndex = FindTradeServerIndex(tsId);

  if (tsIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "Invalid Trade Server Id = %d\n", tsId);

    return ERROR;
  }

  // Update current index
  currentIndex += 4;

  // ECN Order Index (4 bytes)
  int ECNIndex = GetIntNumber(&ttMessage->msgContent[currentIndex]);
  ECNIndex = TT_TO_TS_ORDER_INDEX_MAPPING[ECNIndex];

  TraceLog(DEBUG_LEVEL, "Set Order config of %s, ECNIndex = %d\n", tradeServersInfo.config[tsIndex].description, ECNIndex);

  // Update current index
  currentIndex += 4;
  
  if(ECNIndex < 0 || ECNIndex >= TS_MAX_ORDER_CONNECTIONS)
  {
    TraceLog(ERROR_LEVEL, "Invalid set Order configuration message from TT, ECNIndex = %d\n", ECNIndex);
    return ERROR;
  }
  
  memcpy((unsigned char *)&tradeServersInfo.ECNOrdersConf[tsIndex][ECNIndex], &ttMessage->msgContent[currentIndex], sizeof(t_ECNOrderConf));

  // Call function set Order configuration to TS
  SetTSOrderConfToTS(tsIndex, ECNIndex);

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForSetTSGlobalConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForSetTSGlobalConf(void *message, int ttIndex)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Block content = Trade Server Id (4 bytes) + global conf
  // Trade Server Id (4 bytes)
  int tsId = GetIntNumber(&ttMessage->msgContent[currentIndex]);

  // Find Trade Server index
  int tsIndex = FindTradeServerIndex(tsId);

  if (tsIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "Invalid Trade Server Id = %d\n", tsId);

    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Received request %s set global conf from TT\n", tradeServersInfo.config[tsIndex].description);

  // Update current index
  currentIndex += 4;

  memcpy(&tradeServersInfo.globalConf[tsIndex].maxBuyingPower, &ttMessage->msgContent[currentIndex], 8);
  currentIndex += 8;
  
  memcpy(&tradeServersInfo.globalConf[tsIndex].maxShares, &ttMessage->msgContent[currentIndex], 4);
  currentIndex += 4;
  
  memcpy(&tradeServersInfo.globalConf[tsIndex].maxConsecutiveLoser, &ttMessage->msgContent[currentIndex], 4);
  currentIndex += 4;
  
  memcpy(&tradeServersInfo.globalConf[tsIndex].maxStuckPosition, &ttMessage->msgContent[currentIndex], 4);
  currentIndex += 4;
  
  memcpy(&tradeServersInfo.globalConf[tsIndex].serverRole, &ttMessage->msgContent[currentIndex], 4);
  currentIndex += 4;
  
  memcpy(&tradeServersInfo.globalConf[tsIndex].minSpread, &ttMessage->msgContent[currentIndex], 8);
  currentIndex += 8;
  
  memcpy(&tradeServersInfo.globalConf[tsIndex].maxSpread, &ttMessage->msgContent[currentIndex], 8);
  currentIndex += 8;
  
  memcpy(&tradeServersInfo.globalConf[tsIndex].secFee, &ttMessage->msgContent[currentIndex], 8);
  currentIndex += 8;
  
  memcpy(&tradeServersInfo.globalConf[tsIndex].commission, &ttMessage->msgContent[currentIndex], 8);
  currentIndex += 8;
  
  memcpy(&tradeServersInfo.globalConf[tsIndex].loserRate, &ttMessage->msgContent[currentIndex], 8);
  currentIndex += 8;

  memcpy(&tradeServersInfo.globalConf[tsIndex].exRatio, &ttMessage->msgContent[currentIndex], 8);
  currentIndex += 8;
  
  memcpy(&tradeServersInfo.globalConf[tsIndex].nasdaqAuctionBeginTime, &ttMessage->msgContent[currentIndex], 4);
  currentIndex += 4;
  
  memcpy(&tradeServersInfo.globalConf[tsIndex].nasdaqAuctionEndTime, &ttMessage->msgContent[currentIndex], 4);
  currentIndex += 4;
  
  memcpy(&tradeServersInfo.globalConf[tsIndex].beginRegularSession, &ttMessage->msgContent[currentIndex], 2);
  currentIndex += 2;
  
  memcpy(&tradeServersInfo.globalConf[tsIndex].endRegularSession, &ttMessage->msgContent[currentIndex], 2);
  currentIndex += 2;
  
  char activityDetail[MAX_TRADER_LOG_LEN] = "\0"; 
  
  //Update max shares
  if (BackupTSGlobalConf[tsIndex].maxShares != tradeServersInfo.globalConf[tsIndex].maxShares)
  {
    BackupTSGlobalConf[tsIndex].maxShares = tradeServersInfo.globalConf[tsIndex].maxShares;
    
    sprintf(activityDetail, "%d,%d,%d", traderToolsInfo.connection[ttIndex].currentState, tradeServersInfo.config[tsIndex].tsId, tradeServersInfo.globalConf[tsIndex].maxShares);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_MAX_SHARES_SETTING, activityDetail);
  }
  
  //Update max Consecutive Loss
  if (BackupTSGlobalConf[tsIndex].maxConsecutiveLoser != tradeServersInfo.globalConf[tsIndex].maxConsecutiveLoser)
  {
    BackupTSGlobalConf[tsIndex].maxConsecutiveLoser = tradeServersInfo.globalConf[tsIndex].maxConsecutiveLoser;
    
    memset(activityDetail, 0 , MAX_TRADER_LOG_LEN);
    sprintf(activityDetail, "%d,%d,%d", traderToolsInfo.connection[ttIndex].currentState, tradeServersInfo.config[tsIndex].tsId, tradeServersInfo.globalConf[tsIndex].maxConsecutiveLoser);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_MAX_CONSECUTIVE_LOSS_SETTING, activityDetail);
  }
  
  // Update max Stucks
  if (BackupTSGlobalConf[tsIndex].maxStuckPosition != tradeServersInfo.globalConf[tsIndex].maxStuckPosition)
  {
    BackupTSGlobalConf[tsIndex].maxStuckPosition = tradeServersInfo.globalConf[tsIndex].maxStuckPosition;
    
    memset(activityDetail, 0 , MAX_TRADER_LOG_LEN);
    sprintf(activityDetail, "%d,%d,%d", traderToolsInfo.connection[ttIndex].currentState, tradeServersInfo.config[tsIndex].tsId, tradeServersInfo.globalConf[tsIndex].maxStuckPosition);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_MAX_STUCKS_SETTING, activityDetail);
  }

    // Call function set global configuration to TS
  SetTSGlobalConfToTS(tsIndex);
  
  t_ConnectionStatus *connection;
  int i;
  for (i = 0; i < MAX_TRADER_TOOL_CONNECTIONS; i++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[i];
    if ( (connection->status == DISCONNECT) || (connection->socket == -1) || (connection->clientType != CLIENT_TOOL_DAEDALUS) )
    {
      continue;
    }
    
    BuildAndSendServerStatusToDaedalus(connection, tsIndex, tradeServersInfo.config[tsIndex].description);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForSetTSMinSpreadConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForSetTSMinSpreadConf(void *message, int ttIndex)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;
  char activityLog[1024];
  char temp[32] = "\0";
  memset(activityLog, 0, 1024);

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Block content = Trade Server Id (4 bytes) + Minimum spread conf
  // Trade Server Id (4 bytes)
  int tsId = GetIntNumber(&ttMessage->msgContent[currentIndex]);

  // Find Trade Server index
  int tsIndex = FindTradeServerIndex(tsId);

  if (tsIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "Invalid Trade Server Id = %d\n", tsId);

    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Received request %s set minimum spread conf from TT\n", tradeServersInfo.config[tsIndex].description);

  // Update current index
  currentIndex += 4;
  
  // Minimum spread configuration
  // Min spread in pre-market
  memcpy(&tradeServersInfo.minSpreadConf[tsIndex].preMarket, &ttMessage->msgContent[currentIndex], 8);
  currentIndex += 8;
  sprintf(activityLog, "Pre-Market: %.2lf;", tradeServersInfo.minSpreadConf[tsIndex].preMarket);
  
  // Min spread in post-market
  memcpy(&tradeServersInfo.minSpreadConf[tsIndex].postMarket, &ttMessage->msgContent[currentIndex], 8);
  currentIndex += 8;
  sprintf(temp, "Post-Market: %.2lf;", tradeServersInfo.minSpreadConf[tsIndex].postMarket);
  strcat(activityLog, temp);
  memset(temp, 0, 32);
  strcat(activityLog, "Intraday: ");
  
  // Num times of intraday
  memcpy(&tradeServersInfo.minSpreadConf[tsIndex].intraday.countTimes, &ttMessage->msgContent[currentIndex], 4);
  currentIndex += 4;

  int i;
  int hour, minute, second;
  for (i = 0; i < tradeServersInfo.minSpreadConf[tsIndex].intraday.countTimes; i++)
  {
    memcpy(&tradeServersInfo.minSpreadConf[tsIndex].intraday.minSpreadList[i].timeInSeconds, &ttMessage->msgContent[currentIndex], 4);
    currentIndex += 4;
    hour = tradeServersInfo.minSpreadConf[tsIndex].intraday.minSpreadList[i].timeInSeconds / 3600;
    minute = tradeServersInfo.minSpreadConf[tsIndex].intraday.minSpreadList[i].timeInSeconds / 60 - hour * 60;
    second = tradeServersInfo.minSpreadConf[tsIndex].intraday.minSpreadList[i].timeInSeconds - minute*60 - hour*3600;
        
    memcpy(&tradeServersInfo.minSpreadConf[tsIndex].intraday.minSpreadList[i].minSpread, &ttMessage->msgContent[currentIndex], 8);
    currentIndex += 8;
    
    sprintf(temp, "At %d:%d:%d-%.2lf; ", hour, minute, second, tradeServersInfo.minSpreadConf[tsIndex].intraday.minSpreadList[i].minSpread);
    strcat(activityLog, temp);
    
    memset(temp, 0, 32);
  }

  // Call function set minimum spread configuration to TS
  SetTSMinSpreadConfToTS(tsIndex);
  
  char activityDetail[1024] = "\0"; 
  sprintf(activityDetail, "%d,%d,%s", traderToolsInfo.connection[ttIndex].currentState, tradeServersInfo.config[tsIndex].tsId, activityLog);
  ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_MIN_SPREAD_TS_SETTING, activityDetail);

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForLongShort
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForLongShort(void *message)
{
  TraceLog(DEBUG_LEVEL, "************************************************\n");
  TraceLog(DEBUG_LEVEL, "Received \"finalize trading day\" message from TT ...\n");
  TraceLog(DEBUG_LEVEL, "NOTE: AS not support!\n");
  TraceLog(DEBUG_LEVEL, "************************************************\n");

  return SUCCESS;
}

/****************************************************************************
- Function name:  BuildTTMessageForTradeServersConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTTMessageForTradeServersConf(void *message)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TTMessage *ttMessage = (t_TTMessage *)message;
  
  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_TS_CONF_TO_TT;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1];
  ttMessage->msgLen += 2;

  // Update index of body length
  ttMessage->msgLen += 4;

  // Message body = n * (blockLength (4 bytes) + blockContent (blockLength bytes))
  int tsIndex;

  for (tsIndex = 0; tsIndex < tradeServersInfo.maxConnections; tsIndex++)
  {
    // Update index of block length
    ttMessage->msgLen += 4;

    // Block content = Trade Server configuration
    int size = sizeof(t_TradeServerConf);

    memcpy(&ttMessage->msgContent[ttMessage->msgLen], (unsigned char *)&tradeServersInfo.config[tsIndex], size);
    ttMessage->msgLen += size;

    // Block length field
    intConvert.value = size + 4;
    ttMessage->msgContent[ttMessage->msgLen - size - 4] = intConvert.c[0];
    ttMessage->msgContent[ttMessage->msgLen - size - 3] = intConvert.c[1];
    ttMessage->msgContent[ttMessage->msgLen - size - 2] = intConvert.c[2];
    ttMessage->msgContent[ttMessage->msgLen - size - 1] = intConvert.c[3];
  }
  
  // Body length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[2] = intConvert.c[0];
  ttMessage->msgContent[3] = intConvert.c[1];
  ttMessage->msgContent[4] = intConvert.c[2];
  ttMessage->msgContent[5] = intConvert.c[3];

  return ttMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildTTMessageForTTConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTTMessageForTTConf(void *message)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TTMessage *ttMessage = (t_TTMessage *)message;
  
  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_TT_CONF_TO_TT;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1];
  ttMessage->msgLen += 2;

  // Update index of body length
  ttMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  // Update index of block length
  ttMessage->msgLen += 4;

  // Block content = Aggregarion Server configuration
  int size = sizeof(t_TraderToolConf);

  memcpy(&ttMessage->msgContent[ttMessage->msgLen], (unsigned char *)&traderToolsInfo.config, size);
  ttMessage->msgLen += size;

  // Block length field
  intConvert.value = size + 4;
  ttMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  ttMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  ttMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  ttMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[2] = intConvert.c[0];
  ttMessage->msgContent[3] = intConvert.c[1];
  ttMessage->msgContent[4] = intConvert.c[2];
  ttMessage->msgContent[5] = intConvert.c[3];

  return ttMessage->msgLen;
}
/****************************************************************************
- Function name:  BuildTTMessageForAlertConfig
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildTTMessageForAlertConfig(void *message)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TTMessage *ttMessage = (t_TTMessage *)message;

  // Initialize message
  ttMessage->msgLen = 0;

  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_ALERT_CONFIG_TO_TT;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1];
  ttMessage->msgLen += 2;

  // Update index of body length
  ttMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  // Update index of block length
  ttMessage->msgLen += 4;

  memcpy(&ttMessage->msgContent[ttMessage->msgLen], &AlertConf.pmExitedPositionThreshhold, 4);
  ttMessage->msgLen += 4;

  memcpy(&ttMessage->msgContent[ttMessage->msgLen], &AlertConf.consecutiveLossThreshhold, 4);
  ttMessage->msgLen += 4;

  memcpy(&ttMessage->msgContent[ttMessage->msgLen], &AlertConf.stucksThreshhold, 4);
  ttMessage->msgLen += 4;

  memcpy(&ttMessage->msgContent[ttMessage->msgLen], &AlertConf.singleServerTradesThreshhold, 4);
  ttMessage->msgLen += 4;

  memcpy(&ttMessage->msgContent[ttMessage->msgLen], &AlertConf.gtrAmountThreshhold, 8);
  ttMessage->msgLen += 8;

  memcpy(&ttMessage->msgContent[ttMessage->msgLen], &AlertConf.gtrSecondThreshhold, 4);
  ttMessage->msgLen += 4;

  memcpy(&ttMessage->msgContent[ttMessage->msgLen], &AlertConf.incompleteTradeThreshhold, 4);
  ttMessage->msgLen += 4;

  memcpy(&ttMessage->msgContent[ttMessage->msgLen], &AlertConf.repetitionTimeThreshhold, 4);
  ttMessage->msgLen += 4;

  memcpy(&ttMessage->msgContent[ttMessage->msgLen], &AlertConf.positionWithoutOrderThreshhold, 4);
  ttMessage->msgLen += 4;

  memcpy(&ttMessage->msgContent[ttMessage->msgLen], &AlertConf.pmOrderRateThreshhold, 4);
  ttMessage->msgLen += 4;

  memcpy(&ttMessage->msgContent[ttMessage->msgLen], &AlertConf.buyingPowerAmountThreshhold, 8);
  ttMessage->msgLen += 8;

  memcpy(&ttMessage->msgContent[ttMessage->msgLen], &AlertConf.buyingPowerSecondsThreshhold, 4);
  ttMessage->msgLen += 4;

  int i = 0;

  memcpy(&ttMessage->msgContent[ttMessage->msgLen], &AlertConf.noTradeAlertMgmt.count, 4);
  ttMessage->msgLen += 4;

  for(i = 0; i < AlertConf.noTradeAlertMgmt.count; i++)
  {
    memcpy(&ttMessage->msgContent[ttMessage->msgLen], &AlertConf.noTradeAlertMgmt.itemList[i].startInSecond, 4);
    ttMessage->msgLen += 4;

    memcpy(&ttMessage->msgContent[ttMessage->msgLen], &AlertConf.noTradeAlertMgmt.itemList[i].durationInMinute, 4);
    ttMessage->msgLen += 4;
  }

  // Block length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  ttMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  ttMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  ttMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];

  // Body length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[2] = intConvert.c[0];
  ttMessage->msgContent[3] = intConvert.c[1];
  ttMessage->msgContent[4] = intConvert.c[2];
  ttMessage->msgContent[5] = intConvert.c[3];

  return ttMessage->msgLen;
}
/****************************************************************************
- Function name:  BuildTTMessageForASOrderConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTTMessageForASOrderConf(void *message, int ECNIndex)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TTMessage *ttMessage = (t_TTMessage *)message;
  
  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_AS_ORDERS_CONF_TO_TT;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1];
  ttMessage->msgLen += 2;

  // Update index of body length
  ttMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  // Update index of block length
  ttMessage->msgLen += 4;

  // Block content = ECN Order Index (4 bytes) + AS Order conf
  // ECN Order Index (4 bytes)
  
  intConvert.value = AS_TO_TT_ORDER_INDEX_MAPPING[ECNIndex];
  ttMessage->msgContent[ttMessage->msgLen] = intConvert.c[0];
  ttMessage->msgContent[ttMessage->msgLen + 1] = intConvert.c[1];
  ttMessage->msgContent[ttMessage->msgLen + 2] = intConvert.c[2];
  ttMessage->msgContent[ttMessage->msgLen + 3] = intConvert.c[3];

  // Update index of body length
  ttMessage->msgLen += 4;

  // AS Order configuration
  switch (ECNIndex)
  {
    case AS_NASDAQ_RASH_INDEX:
      // ipAddress
      memcpy(&ttMessage->msgContent[ttMessage->msgLen], NASDAQ_RASH_Config.ipAddress, MAX_LINE_LEN);
      ttMessage->msgLen += MAX_LINE_LEN;

      // port
      memcpy(&ttMessage->msgContent[ttMessage->msgLen], (unsigned char *)&NASDAQ_RASH_Config.port, 4);
      ttMessage->msgLen += 4;

      // userName
      memcpy(&ttMessage->msgContent[ttMessage->msgLen], NASDAQ_RASH_Config.userName, MAX_LINE_LEN);
      ttMessage->msgLen += MAX_LINE_LEN;

      // password
      memcpy(&ttMessage->msgContent[ttMessage->msgLen], NASDAQ_RASH_Config.password, MAX_LINE_LEN);
      ttMessage->msgLen += MAX_LINE_LEN;
      break;
    
    case AS_ARCA_DIRECT_INDEX:
      // ipAddress
      memcpy(&ttMessage->msgContent[ttMessage->msgLen], ARCA_DIRECT_Config.ipAddress, MAX_LINE_LEN);
      ttMessage->msgLen += MAX_LINE_LEN;

      // port
      memcpy(&ttMessage->msgContent[ttMessage->msgLen], (unsigned char *)&ARCA_DIRECT_Config.port, 4);
      ttMessage->msgLen += 4;

      // userName
      memcpy(&ttMessage->msgContent[ttMessage->msgLen], ARCA_DIRECT_Config.userName, MAX_LINE_LEN);
      ttMessage->msgLen += MAX_LINE_LEN;

      // password
      memcpy(&ttMessage->msgContent[ttMessage->msgLen], ARCA_DIRECT_Config.password, MAX_LINE_LEN);
      ttMessage->msgLen += MAX_LINE_LEN;
      break;
  }

  // Block length field
  intConvert.value = (3 * MAX_LINE_LEN + 4) + 8;
  ttMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  ttMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  ttMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  ttMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[2] = intConvert.c[0];
  ttMessage->msgContent[3] = intConvert.c[1];
  ttMessage->msgContent[4] = intConvert.c[2];
  ttMessage->msgContent[5] = intConvert.c[3];

  return ttMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildTTMessageForASGlobalConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTTMessageForASGlobalConf(void *message)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TTMessage *ttMessage = (t_TTMessage *)message;
  
  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_AS_GLOBAL_SETTING_TO_TT;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1];
  ttMessage->msgLen += 2;

  // Update index of body length
  ttMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  // Update index of block length
  ttMessage->msgLen += 4;

  // Block content = Max stuck positions + Max consecutive losers
  // Max stuck positions
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], (unsigned char *)&traderToolsInfo.globalConf.maxStuck, 4);
  ttMessage->msgLen += 4;

  // Max consecutive losers
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], (unsigned char *)&traderToolsInfo.globalConf.maxLoser, 4);
  ttMessage->msgLen += 4;

  // Maximum crosses per minute per TS
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], (unsigned char *)&traderToolsInfo.globalConf.maxCross, 4);
  ttMessage->msgLen += 4;

  // AS Volatile threshold
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], (unsigned char *)&traderToolsInfo.globalConf.asVolatileThreshold, 8);
  ttMessage->msgLen += 8;

  // AS Disabled threshold
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], (unsigned char *)&traderToolsInfo.globalConf.asDisabledThreshold, 8);
  ttMessage->msgLen += 8;
    
    // TS Volatile threshold
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], (unsigned char *)&traderToolsInfo.globalConf.tsVolatileThreshold, 8);
  ttMessage->msgLen += 8;

  // TS Disabled threshold
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], (unsigned char *)&traderToolsInfo.globalConf.tsDisabledThreshold, 8);
  ttMessage->msgLen += 8;

  // Global buying power for account 1
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], (unsigned char *)&traderToolsInfo.globalConf.buyingPower[FIRST_ACCOUNT], 8);
  ttMessage->msgLen += 8;
  
  // Global buying power for account 2
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], (unsigned char *)&traderToolsInfo.globalConf.buyingPower[SECOND_ACCOUNT], 8);
  ttMessage->msgLen += 8;

  // Global buying power for account 3
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], (unsigned char *)&traderToolsInfo.globalConf.buyingPower[THIRD_ACCOUNT], 8);
  ttMessage->msgLen += 8;

  // Global buying power for account 4
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], (unsigned char *)&traderToolsInfo.globalConf.buyingPower[FOURTH_ACCOUNT], 8);
  ttMessage->msgLen += 8;

  // FastMarket threshold
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], (unsigned char *)&FastMarketMgmt.thresHold, 4);
  ttMessage->msgLen += 4;

  // Block length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  ttMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  ttMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  ttMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[2] = intConvert.c[0];
  ttMessage->msgContent[3] = intConvert.c[1];
  ttMessage->msgContent[4] = intConvert.c[2];
  ttMessage->msgContent[5] = intConvert.c[3];

  return ttMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildTTMessageForVolatileSections
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTTMessageForVolatileSections(void *message)
{
  int countPre = 0, countPost = 0, countIntraday = 0;
  char preList[MAX_STOCK_SYMBOL * SYMBOL_LEN];
  char postList[MAX_STOCK_SYMBOL * SYMBOL_LEN];
  char intradayList[MAX_STOCK_SYMBOL * SYMBOL_LEN];
  
  pthread_mutex_lock(&volatileMgmt.mutexLock);
    LoadVolatileSectionsFromFileToLists(preList, &countPre, postList, &countPost, intradayList, &countIntraday);
  pthread_mutex_unlock(&volatileMgmt.mutexLock);
  
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TTMessage *ttMessage = (t_TTMessage *)message;
  
  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_VOLATILE_SECTIONS_TO_TT;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1];
  ttMessage->msgLen += 2;

  // Update index of body length
  ttMessage->msgLen += 4;

  ttMessage->msgLen += 4;

  // number of symbols in [PRE]
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], &countPre, 4);
  ttMessage->msgLen += 4;
  
  //Symbols in [PRE]
  if (countPre > 0)
  {
    memcpy(&ttMessage->msgContent[ttMessage->msgLen], preList, SYMBOL_LEN * countPre);
    ttMessage->msgLen += SYMBOL_LEN * countPre;
  }
  
  // number of symbols in [POST]
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], &countPost, 4);
  ttMessage->msgLen += 4;
  
  //Symbols in [POST]
  if (countPost > 0)
  {
    memcpy(&ttMessage->msgContent[ttMessage->msgLen], postList, SYMBOL_LEN * countPost);
    ttMessage->msgLen += SYMBOL_LEN * countPost;
  }
  
  // number of symbols in [INTRADAY]
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], &countIntraday, 4);
  ttMessage->msgLen += 4;
  
  //Symbols in [INTRADAY]
  if (countIntraday > 0)
  {
    memcpy(&ttMessage->msgContent[ttMessage->msgLen], intradayList, SYMBOL_LEN * countIntraday);
    ttMessage->msgLen += SYMBOL_LEN * countIntraday;
  }
  
  // Current sections
  int currentSession = GetCurrentVolatileSession();
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], &currentSession, 4);
  ttMessage->msgLen += 4;
  
  // Block length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  ttMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  ttMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  ttMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  ttMessage->msgContent[2] = intConvert.c[0];
  ttMessage->msgContent[3] = intConvert.c[1];
  ttMessage->msgContent[4] = intConvert.c[2];
  ttMessage->msgContent[5] = intConvert.c[3];

  return ttMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildTTMessageForAllVolatileToggle
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTTMessageForAllVolatileToggle(void *message)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TTMessage *ttMessage = (t_TTMessage *)message;
  
  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_ALL_VOLATILE_TOGGLE_TO_TT;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1];
  ttMessage->msgLen += 2;

  // Update index of body length
  ttMessage->msgLen += 4;

  ttMessage->msgLen += 4;

  // number of schedules
  ttMessage->msgContent[ttMessage->msgLen++] = volatileMgmt.numAllVolatileToggle;
  
  int i;
  for (i=0; i<volatileMgmt.numAllVolatileToggle; i++)
  {
    memcpy(&ttMessage->msgContent[ttMessage->msgLen], &volatileMgmt.allVolatileOnTime[i], 4);
    ttMessage->msgLen += 4;
    memcpy(&ttMessage->msgContent[ttMessage->msgLen], &volatileMgmt.allVolatileOffTime[i], 4);
    ttMessage->msgLen += 4;
  }
  
  // Block length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  ttMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  ttMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  ttMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  ttMessage->msgContent[2] = intConvert.c[0];
  ttMessage->msgContent[3] = intConvert.c[1];
  ttMessage->msgContent[4] = intConvert.c[2];
  ttMessage->msgContent[5] = intConvert.c[3];

  return ttMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildTTMessageForVolatileSymbolList
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTTMessageForVolatileSymbolList(void *message)
{
  char list[MAX_STOCK_SYMBOL * SYMBOL_LEN];
  int count = 0;
  
  pthread_mutex_lock(&volatileMgmt.mutexLock);
    LoadVolatileSymbolsFromFileToList (list, &count);
  pthread_mutex_unlock(&volatileMgmt.mutexLock);
  
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TTMessage *ttMessage = (t_TTMessage *)message;
  
  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_VOLATILE_SYMBOL_LIST_TO_TT;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1];
  ttMessage->msgLen += 2;

  // Update index of body length
  ttMessage->msgLen += 4;

  ttMessage->msgLen += 4;

  memcpy(&ttMessage->msgContent[ttMessage->msgLen], &count, 4);
  ttMessage->msgLen += 4;
  
  if (count > 0)
  {
    memcpy(&ttMessage->msgContent[ttMessage->msgLen], list, SYMBOL_LEN * count);
    ttMessage->msgLen += SYMBOL_LEN * count;
  }
    
  // Block length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  ttMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  ttMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  ttMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  ttMessage->msgContent[2] = intConvert.c[0];
  ttMessage->msgContent[3] = intConvert.c[1];
  ttMessage->msgContent[4] = intConvert.c[2];
  ttMessage->msgContent[5] = intConvert.c[3];

  return ttMessage->msgLen;
}
/****************************************************************************
- Function name:  BuildTTMessageForIgnoreStockList
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTTMessageForIgnoreStockList(void *message)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TTMessage *ttMessage = (t_TTMessage *)message;
  
  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_IGNORE_SYMBOL_LIST_TO_TT;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1];
  ttMessage->msgLen += 2;

  // Update index of body length
  ttMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  // Update index of block length
  ttMessage->msgLen += 4;

  // Block content = N * symbol
  int i, blockLen = 4;

  for (i = 0; i < IgnoreStockList.countStock; i++)
  {
    memcpy(&ttMessage->msgContent[ttMessage->msgLen], IgnoreStockList.stockList[i], SYMBOL_LEN);
    blockLen += SYMBOL_LEN;
    ttMessage->msgLen += SYMBOL_LEN;
  }

  // Block length field
  intConvert.value = blockLen;
  ttMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  ttMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  ttMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  ttMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[2] = intConvert.c[0];
  ttMessage->msgContent[3] = intConvert.c[1];
  ttMessage->msgContent[4] = intConvert.c[2];
  ttMessage->msgContent[5] = intConvert.c[3];

  return ttMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildTTMessageForIgnoreStockRangesList
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTTMessageForIgnoreStockRangesList(void *message)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TTMessage *ttMessage = (t_TTMessage *)message;
  
  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_IGNORE_SYMBOL_RANGES_LIST_TO_TT;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1];
  ttMessage->msgLen += 2;

  // Update index of body length
  ttMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  // Update index of block length
  ttMessage->msgLen += 4;

  // Block content = N * symbol
  int i, blockLen = 4;

  for (i = 0; i < IgnoreStockRangesList.countStock; i++)
  {
    memcpy(&ttMessage->msgContent[ttMessage->msgLen], IgnoreStockRangesList.stockList[i], SYMBOL_LEN);
    blockLen += SYMBOL_LEN;
    ttMessage->msgLen += SYMBOL_LEN;
  }

  // Block length field
  intConvert.value = blockLen;
  ttMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  ttMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  ttMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  ttMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[2] = intConvert.c[0];
  ttMessage->msgContent[3] = intConvert.c[1];
  ttMessage->msgContent[4] = intConvert.c[2];
  ttMessage->msgContent[5] = intConvert.c[3];

  return ttMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildTTMessageForTSBookConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTTMessageForTSBookConf(void *message, int tsIndex, int ECNIndex)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TTMessage *ttMessage = (t_TTMessage *)message;
  
  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_TS_BOOK_CONF_TO_TT;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1];
  ttMessage->msgLen += 2;

  // Update index of body length
  ttMessage->msgLen += 4;

  // Message body = n * (blockLength (4 bytes) + blockContent (blockLength bytes))  
  // Update index of block length
  ttMessage->msgLen += 4;

  // Block content = Trade Server Id (4 bytes) + ECN Order Index (4 bytes) + ECN Order conf
  // Trade Server Id (4 bytes)
  intConvert.value = tradeServersInfo.config[tsIndex].tsId;
  ttMessage->msgContent[ttMessage->msgLen] = intConvert.c[0];
  ttMessage->msgContent[ttMessage->msgLen + 1] = intConvert.c[1];
  ttMessage->msgContent[ttMessage->msgLen + 2] = intConvert.c[2];
  ttMessage->msgContent[ttMessage->msgLen + 3] = intConvert.c[3];

  // Update index of body length
  ttMessage->msgLen += 4;

  // ECN Book Index (4 bytes)
  intConvert.value = TS_TO_TT_BOOK_INDEX_MAPPING[ECNIndex];
  ttMessage->msgContent[ttMessage->msgLen] = intConvert.c[0];
  ttMessage->msgContent[ttMessage->msgLen + 1] = intConvert.c[1];
  ttMessage->msgContent[ttMessage->msgLen + 2] = intConvert.c[2];
  ttMessage->msgContent[ttMessage->msgLen + 3] = intConvert.c[3];

  // Update index of body length
  ttMessage->msgLen += 4;

  // ECN Book configuration
  int size = 0, i;

  switch (ECNIndex)
  {
    // ARCA Book configuration
    case TS_ARCA_BOOK_INDEX:
      memcpy(&ttMessage->msgContent[ttMessage->msgLen], tradeServersInfo.ECNBooksConf[tsIndex].ARCABookConf.sourceID, MAX_LINE_LEN);
      ttMessage->msgLen += MAX_LINE_LEN;

      int groupIndex;
      for (groupIndex = 0; groupIndex < MAX_ARCA_MULTICAST_GROUP; groupIndex ++)
      {
        // Retransmission Request TCP/IP
        // Index
        memcpy(&ttMessage->msgContent[ttMessage->msgLen], &tradeServersInfo.ECNBooksConf[tsIndex].ARCABookConf.retranConn[groupIndex].index, 4);
        ttMessage->msgLen += 4;

        // IP Address
        memcpy(&ttMessage->msgContent[ttMessage->msgLen], tradeServersInfo.ECNBooksConf[tsIndex].ARCABookConf.retranConn[groupIndex].ip, MAX_LINE_LEN);
        ttMessage->msgLen += MAX_LINE_LEN;
        
        // Port
        memcpy(&ttMessage->msgContent[ttMessage->msgLen], &tradeServersInfo.ECNBooksConf[tsIndex].ARCABookConf.retranConn[groupIndex].port, 4);
        ttMessage->msgLen += 4;
      }
      
      ttMessage->msgContent[ttMessage->msgLen] = tradeServersInfo.ECNBooksConf[tsIndex].ARCABookConf.currentFeedName;
      ttMessage->msgLen += 1;
      
      size = MAX_LINE_LEN + MAX_ARCA_MULTICAST_GROUP * (MAX_LINE_LEN + 8) + 1;

      break;
    
    // NASDAQ Book configuration
    case TS_NASDAQ_BOOK_INDEX:
      memcpy(&ttMessage->msgContent[ttMessage->msgLen], tradeServersInfo.ECNBooksConf[tsIndex].NASDAQBookConf.UDP.ip, MAX_LINE_LEN);
      ttMessage->msgLen += MAX_LINE_LEN;

      memcpy(&ttMessage->msgContent[ttMessage->msgLen], &tradeServersInfo.ECNBooksConf[tsIndex].NASDAQBookConf.UDP.port, 4);
      ttMessage->msgLen += 4;

      memcpy(&ttMessage->msgContent[ttMessage->msgLen], tradeServersInfo.ECNBooksConf[tsIndex].NASDAQBookConf.TCP.ip, MAX_LINE_LEN);
      ttMessage->msgLen += MAX_LINE_LEN;

      memcpy(&ttMessage->msgContent[ttMessage->msgLen], &tradeServersInfo.ECNBooksConf[tsIndex].NASDAQBookConf.TCP.port, 4);
      ttMessage->msgLen += 4;

      memcpy(&ttMessage->msgContent[ttMessage->msgLen], &tradeServersInfo.ECNBooksConf[tsIndex].NASDAQBookConf.maxMsgLoss, 4);
      ttMessage->msgLen += 4;

      size = 2 * MAX_LINE_LEN + 3 * 4;

      break;
      
    case TS_NDBX_BOOK_INDEX:
      memcpy(&ttMessage->msgContent[ttMessage->msgLen], tradeServersInfo.ECNBooksConf[tsIndex].NDAQBXBookConf.UDP.ip, MAX_LINE_LEN);
      ttMessage->msgLen += MAX_LINE_LEN;

      memcpy(&ttMessage->msgContent[ttMessage->msgLen], &tradeServersInfo.ECNBooksConf[tsIndex].NDAQBXBookConf.UDP.port, 4);
      ttMessage->msgLen += 4;
      break;
      
    // BATSZ Book configuration
    case TS_BATSZ_BOOK_INDEX:
      //Number of units
      ttMessage->msgContent[ttMessage->msgLen] = MAX_BATSZ_MULTICAST_GROUP;
      ttMessage->msgLen += 1;
      
      //SessionSubId
      memcpy(&ttMessage->msgContent[ttMessage->msgLen], tradeServersInfo.ECNBooksConf[tsIndex].BATSZBookGRPConf.sessionSubId, MAX_LINE_LEN);
      ttMessage->msgLen += MAX_LINE_LEN;
      
      // Username
      memcpy(&ttMessage->msgContent[ttMessage->msgLen], tradeServersInfo.ECNBooksConf[tsIndex].BATSZBookGRPConf.userName, MAX_LINE_LEN);
      ttMessage->msgLen += MAX_LINE_LEN;

      //password
      memcpy(&ttMessage->msgContent[ttMessage->msgLen], tradeServersInfo.ECNBooksConf[tsIndex].BATSZBookGRPConf.password, MAX_LINE_LEN);
      ttMessage->msgLen += MAX_LINE_LEN;
      
      //IP
      memcpy(&ttMessage->msgContent[ttMessage->msgLen], tradeServersInfo.ECNBooksConf[tsIndex].BATSZBookGRPConf.retranRequestConn.ip, MAX_LINE_LEN);
      ttMessage->msgLen += MAX_LINE_LEN;
    
      //Port
      memcpy(&ttMessage->msgContent[ttMessage->msgLen], &tradeServersInfo.ECNBooksConf[tsIndex].BATSZBookGRPConf.retranRequestConn.port, 4);
      ttMessage->msgLen += 4;

      for (i = 0; i < MAX_BATSZ_MULTICAST_GROUP; i++)
      {
        //Data feed IP & Port
        memcpy(&ttMessage->msgContent[ttMessage->msgLen], tradeServersInfo.ECNBooksConf[tsIndex].BATSZBookConf[i].primaryConn.ip, MAX_LINE_LEN);
        ttMessage->msgLen += MAX_LINE_LEN;
        
        memcpy(&ttMessage->msgContent[ttMessage->msgLen], &tradeServersInfo.ECNBooksConf[tsIndex].BATSZBookConf[i].primaryConn.port, 4);
        ttMessage->msgLen += 4;
        
        //Retrans feed IP & Port
        memcpy(&ttMessage->msgContent[ttMessage->msgLen], tradeServersInfo.ECNBooksConf[tsIndex].BATSZBookConf[i].retranConn.ip, MAX_LINE_LEN);
        ttMessage->msgLen += MAX_LINE_LEN;
        
        memcpy(&ttMessage->msgContent[ttMessage->msgLen], &tradeServersInfo.ECNBooksConf[tsIndex].BATSZBookConf[i].retranConn.port, 4);
        ttMessage->msgLen += 4;
      }

      size = 1 + 4 * MAX_LINE_LEN + 4 + MAX_BATSZ_MULTICAST_GROUP * ( 2 * MAX_LINE_LEN + 2 * 4);
      break;

    // NYSE OPEN ULTRA BOOK configuration
    case TS_NYSE_BOOK_INDEX:
      // SourceID
      memcpy(&ttMessage->msgContent[ttMessage->msgLen], tradeServersInfo.ECNBooksConf[tsIndex].NYSEBookConf.sourceID, MAX_LINE_LEN);
      ttMessage->msgLen += MAX_LINE_LEN;

      for (groupIndex = 0; groupIndex < MAX_NYSE_OPEN_ULTRA_MULTICAST_GROUP; groupIndex ++)
      {
        // Group index
        memcpy(&ttMessage->msgContent[ttMessage->msgLen], &tradeServersInfo.ECNBooksConf[tsIndex].NYSEBookConf.nyseBook[groupIndex].index, 4);
        ttMessage->msgLen += 4;

        // Retransmission IP
        memcpy(&ttMessage->msgContent[ttMessage->msgLen], tradeServersInfo.ECNBooksConf[tsIndex].NYSEBookConf.nyseBook[groupIndex].retranIP, MAX_LINE_LEN);
        ttMessage->msgLen += MAX_LINE_LEN;

        // Retransmission port
        memcpy(&ttMessage->msgContent[ttMessage->msgLen], &tradeServersInfo.ECNBooksConf[tsIndex].NYSEBookConf.nyseBook[groupIndex].retranPort, 4);
        ttMessage->msgLen += 4;
      }

      size = MAX_NYSE_OPEN_ULTRA_MULTICAST_GROUP * (MAX_LINE_LEN + (2 * 4)) + MAX_LINE_LEN;

      break;
    
    // EdgeX Book configuration
    case TS_EDGX_BOOK_INDEX:
      //Number of units
      ttMessage->msgContent[ttMessage->msgLen] = MAX_EDGX_MULTICAST_GROUP;
      ttMessage->msgLen += 1;
      
      // Username
      memcpy(&ttMessage->msgContent[ttMessage->msgLen], tradeServersInfo.ECNBooksConf[tsIndex].EDGXBookGRPConf.userName, MAX_LINE_LEN);
      ttMessage->msgLen += MAX_LINE_LEN;

      //password
      memcpy(&ttMessage->msgContent[ttMessage->msgLen], tradeServersInfo.ECNBooksConf[tsIndex].EDGXBookGRPConf.password, MAX_LINE_LEN);
      ttMessage->msgLen += MAX_LINE_LEN;
      
      //IP
      memcpy(&ttMessage->msgContent[ttMessage->msgLen], tradeServersInfo.ECNBooksConf[tsIndex].EDGXBookGRPConf.ip, MAX_LINE_LEN);
      ttMessage->msgLen += MAX_LINE_LEN;
    
      //Port
      memcpy(&ttMessage->msgContent[ttMessage->msgLen], &tradeServersInfo.ECNBooksConf[tsIndex].EDGXBookGRPConf.port, 4);
      ttMessage->msgLen += 4;

      for (i = 0; i < MAX_EDGX_MULTICAST_GROUP; i++)
      {
        //Data feed IP & Port
        memcpy(&ttMessage->msgContent[ttMessage->msgLen], tradeServersInfo.ECNBooksConf[tsIndex].EDGXBookConf[i].primaryConn.ip, MAX_LINE_LEN);
        ttMessage->msgLen += MAX_LINE_LEN;
        
        memcpy(&ttMessage->msgContent[ttMessage->msgLen], &tradeServersInfo.ECNBooksConf[tsIndex].EDGXBookConf[i].primaryConn.port, 4);
        ttMessage->msgLen += 4;
        
        //Retrans feed IP & Port
        memcpy(&ttMessage->msgContent[ttMessage->msgLen], tradeServersInfo.ECNBooksConf[tsIndex].EDGXBookConf[i].retranConn.ip, MAX_LINE_LEN);
        ttMessage->msgLen += MAX_LINE_LEN;
        
        memcpy(&ttMessage->msgContent[ttMessage->msgLen], &tradeServersInfo.ECNBooksConf[tsIndex].EDGXBookConf[i].retranConn.port, 4);
        ttMessage->msgLen += 4;
      }

      size = 1 + (3 * MAX_LINE_LEN + 4) + MAX_EDGX_MULTICAST_GROUP * ( 2 * MAX_LINE_LEN + 2 * 4);
      break;
      
    case TS_EDGA_BOOK_INDEX:
      //Number of units
      ttMessage->msgContent[ttMessage->msgLen] = MAX_EDGX_MULTICAST_GROUP;
      ttMessage->msgLen += 1;
      
      // Username
      memcpy(&ttMessage->msgContent[ttMessage->msgLen], tradeServersInfo.ECNBooksConf[tsIndex].EDGABookGRPConf.userName, MAX_LINE_LEN);
      ttMessage->msgLen += MAX_LINE_LEN;

      //password
      memcpy(&ttMessage->msgContent[ttMessage->msgLen], tradeServersInfo.ECNBooksConf[tsIndex].EDGABookGRPConf.password, MAX_LINE_LEN);
      ttMessage->msgLen += MAX_LINE_LEN;
      
      //IP
      memcpy(&ttMessage->msgContent[ttMessage->msgLen], tradeServersInfo.ECNBooksConf[tsIndex].EDGABookGRPConf.ip, MAX_LINE_LEN);
      ttMessage->msgLen += MAX_LINE_LEN;
    
      //Port
      memcpy(&ttMessage->msgContent[ttMessage->msgLen], &tradeServersInfo.ECNBooksConf[tsIndex].EDGABookGRPConf.port, 4);
      ttMessage->msgLen += 4;

      for (i = 0; i < MAX_EDGX_MULTICAST_GROUP; i++)
      {
        //Data feed IP & Port
        memcpy(&ttMessage->msgContent[ttMessage->msgLen], tradeServersInfo.ECNBooksConf[tsIndex].EDGABookConf[i].primaryConn.ip, MAX_LINE_LEN);
        ttMessage->msgLen += MAX_LINE_LEN;
        
        memcpy(&ttMessage->msgContent[ttMessage->msgLen], &tradeServersInfo.ECNBooksConf[tsIndex].EDGABookConf[i].primaryConn.port, 4);
        ttMessage->msgLen += 4;
        
        //Retrans feed IP & Port
        memcpy(&ttMessage->msgContent[ttMessage->msgLen], tradeServersInfo.ECNBooksConf[tsIndex].EDGABookConf[i].retranConn.ip, MAX_LINE_LEN);
        ttMessage->msgLen += MAX_LINE_LEN;
        
        memcpy(&ttMessage->msgContent[ttMessage->msgLen], &tradeServersInfo.ECNBooksConf[tsIndex].EDGABookConf[i].retranConn.port, 4);
        ttMessage->msgLen += 4;
      }

      size = 1 + (3 * MAX_LINE_LEN + 4) + MAX_EDGX_MULTICAST_GROUP * ( 2 * MAX_LINE_LEN + 2 * 4);
      break;
    
    // BYX Book configuration
    case TS_BYX_BOOK_INDEX:
      //Number of units
      ttMessage->msgContent[ttMessage->msgLen] = MAX_BYX_MULTICAST_GROUP;
      ttMessage->msgLen += 1;
      
      //SessionSubId
      memcpy(&ttMessage->msgContent[ttMessage->msgLen], tradeServersInfo.ECNBooksConf[tsIndex].BYXBookGRPConf.sessionSubId, MAX_LINE_LEN);
      ttMessage->msgLen += MAX_LINE_LEN;
      
      // Username
      memcpy(&ttMessage->msgContent[ttMessage->msgLen], tradeServersInfo.ECNBooksConf[tsIndex].BYXBookGRPConf.userName, MAX_LINE_LEN);
      ttMessage->msgLen += MAX_LINE_LEN;

      //password
      memcpy(&ttMessage->msgContent[ttMessage->msgLen], tradeServersInfo.ECNBooksConf[tsIndex].BYXBookGRPConf.password, MAX_LINE_LEN);
      ttMessage->msgLen += MAX_LINE_LEN;
      
      //IP
      memcpy(&ttMessage->msgContent[ttMessage->msgLen], tradeServersInfo.ECNBooksConf[tsIndex].BYXBookGRPConf.retranRequestConn.ip, MAX_LINE_LEN);
      ttMessage->msgLen += MAX_LINE_LEN;
    
      //Port
      memcpy(&ttMessage->msgContent[ttMessage->msgLen], &tradeServersInfo.ECNBooksConf[tsIndex].BYXBookGRPConf.retranRequestConn.port, 4);
      ttMessage->msgLen += 4;

      for (i = 0; i < MAX_BYX_MULTICAST_GROUP; i++)
      {
        //Data feed IP & Port
        memcpy(&ttMessage->msgContent[ttMessage->msgLen], tradeServersInfo.ECNBooksConf[tsIndex].BYXBookConf[i].primaryConn.ip, MAX_LINE_LEN);
        ttMessage->msgLen += MAX_LINE_LEN;
        
        memcpy(&ttMessage->msgContent[ttMessage->msgLen], &tradeServersInfo.ECNBooksConf[tsIndex].BYXBookConf[i].primaryConn.port, 4);
        ttMessage->msgLen += 4;
        
        //Retrans feed IP & Port
        memcpy(&ttMessage->msgContent[ttMessage->msgLen], tradeServersInfo.ECNBooksConf[tsIndex].BYXBookConf[i].retranConn.ip, MAX_LINE_LEN);
        ttMessage->msgLen += MAX_LINE_LEN;
        
        memcpy(&ttMessage->msgContent[ttMessage->msgLen], &tradeServersInfo.ECNBooksConf[tsIndex].BYXBookConf[i].retranConn.port, 4);
        ttMessage->msgLen += 4;
      }

      size = 1 + 4 * MAX_LINE_LEN + 4 + MAX_BYX_MULTICAST_GROUP * ( 2 * MAX_LINE_LEN + 2 * 4);
      break;
      
    case TS_PSX_BOOK_INDEX:
      memcpy(&ttMessage->msgContent[ttMessage->msgLen], tradeServersInfo.ECNBooksConf[tsIndex].PSXBookConf.UDP.ip, MAX_LINE_LEN);
      ttMessage->msgLen += MAX_LINE_LEN;

      memcpy(&ttMessage->msgContent[ttMessage->msgLen], &tradeServersInfo.ECNBooksConf[tsIndex].PSXBookConf.UDP.port, 4);
      ttMessage->msgLen += 4;
      break;
    case TS_AMEX_BOOK_INDEX:
      // No longer supported
      break;
  }

  // Block length field
  intConvert.value = size + 12;
  ttMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  ttMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  ttMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  ttMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field 
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[2] = intConvert.c[0];
  ttMessage->msgContent[3] = intConvert.c[1];
  ttMessage->msgContent[4] = intConvert.c[2];
  ttMessage->msgContent[5] = intConvert.c[3];

  return ttMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildTTMessageForTSOrderConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTTMessageForTSOrderConf(void *message, int tsIndex, int ECNIndex)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TTMessage *ttMessage = (t_TTMessage *)message;
  
  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_TS_ORDER_CONF_TO_TT;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1];
  ttMessage->msgLen += 2;

  // Update index of body length
  ttMessage->msgLen += 4;

  // Message body = n * (blockLength (4 bytes) + blockContent (blockLength bytes))  
  // Update index of block length
  ttMessage->msgLen += 4;

  // Block content = Trade Server Id (4 bytes) + ECN Order Index (4 bytes) + ECN Order conf
  // Trade Server Id (4 bytes)
  intConvert.value = tradeServersInfo.config[tsIndex].tsId;
  ttMessage->msgContent[ttMessage->msgLen] = intConvert.c[0];
  ttMessage->msgContent[ttMessage->msgLen + 1] = intConvert.c[1];
  ttMessage->msgContent[ttMessage->msgLen + 2] = intConvert.c[2];
  ttMessage->msgContent[ttMessage->msgLen + 3] = intConvert.c[3];

  // Update index of body length
  ttMessage->msgLen += 4;

  // ECN Order Index (4 bytes)
  intConvert.value = TS_TO_TT_ORDER_INDEX_MAPPING[ECNIndex];
  ttMessage->msgContent[ttMessage->msgLen] = intConvert.c[0];
  ttMessage->msgContent[ttMessage->msgLen + 1] = intConvert.c[1];
  ttMessage->msgContent[ttMessage->msgLen + 2] = intConvert.c[2];
  ttMessage->msgContent[ttMessage->msgLen + 3] = intConvert.c[3];

  // Update index of body length
  ttMessage->msgLen += 4;

  // ECN Order configuration
  int size = sizeof(t_ECNOrderConf);
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], (unsigned char *)&tradeServersInfo.ECNOrdersConf[tsIndex][ECNIndex], size);
  ttMessage->msgLen += size;

  // Block length field
  intConvert.value = size + 12;
  ttMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  ttMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  ttMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  ttMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[2] = intConvert.c[0];
  ttMessage->msgContent[3] = intConvert.c[1];
  ttMessage->msgContent[4] = intConvert.c[2];
  ttMessage->msgContent[5] = intConvert.c[3];

  return ttMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildTTMessageForTSGlobalConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTTMessageForTSGlobalConf(void *message, int tsIndex)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TTMessage *ttMessage = (t_TTMessage *)message;
  
  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_TS_GLOBAL_CONF_TO_TT;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1];
  ttMessage->msgLen += 2;

  // Update index of body length
  ttMessage->msgLen += 4;

  // Message body = n * (blockLength (4 bytes) + blockContent (blockLength bytes))  
  // Update index of block length
  ttMessage->msgLen += 4;

  // Block content = Trade Server Id (4 bytes) + global conf
  // Trade Server Id (4 bytes)
  intConvert.value = tradeServersInfo.config[tsIndex].tsId;
  ttMessage->msgContent[ttMessage->msgLen] = intConvert.c[0];
  ttMessage->msgContent[ttMessage->msgLen + 1] = intConvert.c[1];
  ttMessage->msgContent[ttMessage->msgLen + 2] = intConvert.c[2];
  ttMessage->msgContent[ttMessage->msgLen + 3] = intConvert.c[3];

  // Update index of body length
  ttMessage->msgLen += 4;

  // Global configuration
  int size = 0;
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], &tradeServersInfo.globalConf[tsIndex].maxBuyingPower, 8);
  ttMessage->msgLen += 8;
  size += 8;
  
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], &tradeServersInfo.globalConf[tsIndex].maxShares, 4);
  ttMessage->msgLen += 4;
  size += 4;
  
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], &tradeServersInfo.globalConf[tsIndex].maxConsecutiveLoser, 4);
  ttMessage->msgLen += 4;
  size += 4;
  
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], &tradeServersInfo.globalConf[tsIndex].maxStuckPosition, 4);
  ttMessage->msgLen += 4;
  size += 4;
  
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], &tradeServersInfo.globalConf[tsIndex].serverRole, 4);
  ttMessage->msgLen += 4;
  size += 4;
  
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], &tradeServersInfo.globalConf[tsIndex].minSpread, 8);
  ttMessage->msgLen += 8;
  size += 8;
  
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], &tradeServersInfo.globalConf[tsIndex].maxSpread, 8);
  ttMessage->msgLen += 8;
  size += 8;
  
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], &tradeServersInfo.globalConf[tsIndex].secFee, 8);
  ttMessage->msgLen += 8;
  size += 8;
  
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], &tradeServersInfo.globalConf[tsIndex].commission, 8);
  ttMessage->msgLen += 8;
  size += 8;
  
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], &tradeServersInfo.globalConf[tsIndex].loserRate, 8);
  ttMessage->msgLen += 8;
  size += 8;
  
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], &tradeServersInfo.globalConf[tsIndex].exRatio, 8);
  ttMessage->msgLen += 8;
  size += 8;
  
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], &tradeServersInfo.globalConf[tsIndex].nasdaqAuctionBeginTime, 4);
  ttMessage->msgLen += 4;
  size += 4;
  
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], &tradeServersInfo.globalConf[tsIndex].nasdaqAuctionEndTime, 4);
  ttMessage->msgLen += 4;
  size += 4;
  
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], &tradeServersInfo.globalConf[tsIndex].beginRegularSession, 4);
  ttMessage->msgLen += 4;
  size += 4;
  
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], &tradeServersInfo.globalConf[tsIndex].endRegularSession, 4);
  ttMessage->msgLen += 4;
  size += 4;
  
  // Block length field
  intConvert.value = size + 8;
  ttMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  ttMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  ttMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  ttMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[2] = intConvert.c[0];
  ttMessage->msgContent[3] = intConvert.c[1];
  ttMessage->msgContent[4] = intConvert.c[2];
  ttMessage->msgContent[5] = intConvert.c[3];

  return ttMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildTTMessageForTSMinSpreadConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTTMessageForTSMinSpreadConf(void *message, int tsIndex)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TTMessage *ttMessage = (t_TTMessage *)message;
  
  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_MIN_SPREAD_CONF_TO_TT;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1];
  ttMessage->msgLen += 2;

  // Update index of body length
  ttMessage->msgLen += 4;

  // Update index of block length
  ttMessage->msgLen += 4;

  // Block content = Trade Server Id (4 bytes) + Minimum spread conf
  // Trade Server Id (4 bytes)
  intConvert.value = tradeServersInfo.config[tsIndex].tsId;
  ttMessage->msgContent[ttMessage->msgLen] = intConvert.c[0];
  ttMessage->msgContent[ttMessage->msgLen + 1] = intConvert.c[1];
  ttMessage->msgContent[ttMessage->msgLen + 2] = intConvert.c[2];
  ttMessage->msgContent[ttMessage->msgLen + 3] = intConvert.c[3];

  // Update index of body length
  ttMessage->msgLen += 4;

  // Minimum spread configuration
  // Min spread in pre-market
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], &tradeServersInfo.minSpreadConf[tsIndex].preMarket, 8);
  ttMessage->msgLen += 8;
  
  // Min spread in post-market
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], &tradeServersInfo.minSpreadConf[tsIndex].postMarket, 8);
  ttMessage->msgLen += 8;
  
  // Num times of intraday
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], &tradeServersInfo.minSpreadConf[tsIndex].intraday.countTimes, 4);
  ttMessage->msgLen += 4;
  
  int i;
  for (i = 0; i < tradeServersInfo.minSpreadConf[tsIndex].intraday.countTimes; i++)
  {
    memcpy(&ttMessage->msgContent[ttMessage->msgLen], &tradeServersInfo.minSpreadConf[tsIndex].intraday.minSpreadList[i].timeInSeconds, 4);
    ttMessage->msgLen += 4;
    
    memcpy(&ttMessage->msgContent[ttMessage->msgLen], &tradeServersInfo.minSpreadConf[tsIndex].intraday.minSpreadList[i].minSpread, 8);
    ttMessage->msgLen += 8;
  }

  // Block length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  ttMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  ttMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  ttMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[2] = intConvert.c[0];
  ttMessage->msgContent[3] = intConvert.c[1];
  ttMessage->msgContent[4] = intConvert.c[2];
  ttMessage->msgContent[5] = intConvert.c[3];

  return ttMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildTTMessageForTSCurrentStatus
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTTMessageForTSCurrentStatus(void *message, int tsIndex)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TTMessage *ttMessage = (t_TTMessage *)message;
  
  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_TS_CURRENT_STATUS_TO_TT;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1];
  ttMessage->msgLen += 2;

  // Update index of body length
  ttMessage->msgLen += 4;

  // Message body = n * (blockLength (4 bytes) + blockContent (blockLength bytes))  
  // Update index of block length
  ttMessage->msgLen += 4;

  // Block content = Trade Server Id (4 bytes) + AS connected (1 byte) + current status
  // Trade Server Id (4 bytes)
  intConvert.value = tradeServersInfo.config[tsIndex].tsId;
  ttMessage->msgContent[ttMessage->msgLen] = intConvert.c[0];
  ttMessage->msgContent[ttMessage->msgLen + 1] = intConvert.c[1];
  ttMessage->msgContent[ttMessage->msgLen + 2] = intConvert.c[2];
  ttMessage->msgContent[ttMessage->msgLen + 3] = intConvert.c[3];

  // Update index of body length
  ttMessage->msgLen += 4;

  // AS connected
  ttMessage->msgContent[ttMessage->msgLen] = tradeServersInfo.connection[tsIndex].status;

  // Update index of body length
  ttMessage->msgLen += 1;

  // Number of order placed and total shares filled
  int size2 = sizeof(t_OrderPlaceSummary);
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], (unsigned char *)&tradeServersInfo.orderPlaceSummary[tsIndex], size2);
  ttMessage->msgLen += size2;
  
  // Current status
  int size = sizeof(t_CurrentStatus);

  memcpy(&ttMessage->msgContent[ttMessage->msgLen], (unsigned char *)&tradeServersInfo.currentStatus[tsIndex], size);

  ttMessage->msgLen += size;  

  // Block length field
  intConvert.value = size + size2 + 9;
  ttMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  ttMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  ttMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  ttMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[2] = intConvert.c[0];
  ttMessage->msgContent[3] = intConvert.c[1];
  ttMessage->msgContent[4] = intConvert.c[2];
  ttMessage->msgContent[5] = intConvert.c[3];

  return ttMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildTTMessageForASCurrentStatus
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTTMessageForASCurrentStatus(void *message)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TTMessage *ttMessage = (t_TTMessage *)message;
  
  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_AS_CURRENT_STATUS_TO_TT;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1];
  ttMessage->msgLen += 2;

  // Update index of body length
  ttMessage->msgLen += 4;

  // Message body = blockLength (4 bytes) + blockContent (blockLength bytes)

  // Update index of block length
  ttMessage->msgLen += 4;

  // Block content = AS current status + ECN order connection status
  int account;
  for (account = 0; account < MAX_ACCOUNT; account++)
  {
    memcpy(&ttMessage->msgContent[ttMessage->msgLen], &traderToolsInfo.currentStatus.buyingPower[account], 8);
    ttMessage->msgLen += 8;
  }
  
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], &traderToolsInfo.currentStatus.numStuck, 4);
  ttMessage->msgLen += 4;
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], &traderToolsInfo.currentStatus.numLoser, 4);
  ttMessage->msgLen += 4;
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], &traderToolsInfo.currentStatus.numberOfSent, 4);
  ttMessage->msgLen += 4;
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], &traderToolsInfo.currentStatus.totalSharesFilled, 4);
  ttMessage->msgLen += 4;
  
  // ECN order connection status
  ttMessage->msgContent[ttMessage->msgLen++] = 0;
  ttMessage->msgContent[ttMessage->msgLen++] = 0;
  ttMessage->msgContent[ttMessage->msgLen++] = 0;
  ttMessage->msgContent[ttMessage->msgLen++] = traderToolsInfo.orderConnection[AS_NASDAQ_RASH_INDEX].status;
  ttMessage->msgContent[ttMessage->msgLen++] = traderToolsInfo.orderConnection[AS_ARCA_DIRECT_INDEX].status;

  // Find index for 
  int countConnection = 0;
  int i;
  // Check number of connections for Trader Tool
  for (i = 0; i < MAX_TRADER_TOOL_CONNECTIONS; i++)
  {
    if (traderToolsInfo.connection[i].status == CONNECT)
    {
      countConnection ++;
    }
  }

  // Number of TT connections field
  intConvert.value = countConnection;
  ttMessage->msgContent[ttMessage->msgLen] = intConvert.c[0];
  ttMessage->msgContent[ttMessage->msgLen + 1] = intConvert.c[1];
  ttMessage->msgContent[ttMessage->msgLen + 2] = intConvert.c[2];
  ttMessage->msgContent[ttMessage->msgLen + 3] = intConvert.c[3];
  ttMessage->msgLen += 4;

  // Block length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  ttMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  ttMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  ttMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[2] = intConvert.c[0];
  ttMessage->msgContent[3] = intConvert.c[1];
  ttMessage->msgContent[4] = intConvert.c[2];
  ttMessage->msgContent[5] = intConvert.c[3];

  return ttMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildTTMessageForSymbolStatus
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTTMessageForSymbolStatus(void *message, int tsIndex, void *messageSrc)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *tsMessage = (t_TSMessage *)messageSrc;
  t_TTMessage *ttMessage = (t_TTMessage *)message;
  
  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_SYMBOL_STATUS_TO_TT;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1];
  ttMessage->msgLen += 2;

  // Update index of body length
  ttMessage->msgLen += 4;

  // Message body = 3 block (block sender + block ASK + block BID)
  
  // Block sender connect = TS Id (4 bytes) + symbol (8 bytes)
  // Update index of block length
  ttMessage->msgLen += 4;

  // Trade Server Id (4 bytes)
  intConvert.value = tradeServersInfo.config[tsIndex].tsId;
  ttMessage->msgContent[ttMessage->msgLen] = intConvert.c[0];
  ttMessage->msgContent[ttMessage->msgLen + 1] = intConvert.c[1];
  ttMessage->msgContent[ttMessage->msgLen + 2] = intConvert.c[2];
  ttMessage->msgContent[ttMessage->msgLen + 3] = intConvert.c[3];

  // Update index of body length
  ttMessage->msgLen += 4;

  // Symbol (8 bytes)
  strncpy((char *) &ttMessage->msgContent[ttMessage->msgLen], (char *) &tsMessage->msgContent[MSG_HEADER_LEN + 5], SYMBOL_LEN);
  ttMessage->msgLen += SYMBOL_LEN;

  // Block length field
  intConvert.value = SYMBOL_LEN + 8;
  ttMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  ttMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  ttMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  ttMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];

  // Block ASK connect = N * (...)
  int blockASKLen = GetIntNumber(&tsMessage->msgContent[MSG_HEADER_LEN + 5 + SYMBOL_LEN]);
  
  // Block length field
  intConvert.value = blockASKLen;
  ttMessage->msgContent[ttMessage->msgLen] = intConvert.c[0];
  ttMessage->msgContent[ttMessage->msgLen + 1] = intConvert.c[1];
  ttMessage->msgContent[ttMessage->msgLen + 2] = intConvert.c[2];
  ttMessage->msgContent[ttMessage->msgLen + 3] = intConvert.c[3];

  // Update index of block length
  ttMessage->msgLen += 4;

  // N * (...)
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], &tsMessage->msgContent[MSG_HEADER_LEN + 5 + SYMBOL_LEN + 4], blockASKLen - 4);
  ttMessage->msgLen += (blockASKLen - 4);

  // Block BID connect = N * (...)
  int blockBIDLen = GetIntNumber(&tsMessage->msgContent[MSG_HEADER_LEN + 5 + SYMBOL_LEN + blockASKLen]);
  
  // Block length field
  intConvert.value = blockBIDLen;
  ttMessage->msgContent[ttMessage->msgLen] = intConvert.c[0];
  ttMessage->msgContent[ttMessage->msgLen + 1] = intConvert.c[1];
  ttMessage->msgContent[ttMessage->msgLen + 2] = intConvert.c[2];
  ttMessage->msgContent[ttMessage->msgLen + 3] = intConvert.c[3];

  // Update index of block length
  ttMessage->msgLen += 4;

  // N * (...)
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], &tsMessage->msgContent[MSG_HEADER_LEN + 5 + SYMBOL_LEN + blockASKLen + 4], blockBIDLen - 4);
  ttMessage->msgLen += (blockBIDLen - 4);
  
  // Body length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[2] = intConvert.c[0];
  ttMessage->msgContent[3] = intConvert.c[1];
  ttMessage->msgContent[4] = intConvert.c[2];
  ttMessage->msgContent[5] = intConvert.c[3];

  return ttMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildTTMessageForOpenPosition
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTTMessageForOpenPosition(void *message, int ttIndex)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TTMessage *ttMessage = (t_TTMessage *)message;
  
  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_OPEN_POSITION_TO_TT;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1];
  ttMessage->msgLen += 2;

  // Update index of body length
  ttMessage->msgLen += 4;

  int rmInfoIndex;
  int countRMInfo = 0;
  
  // Update index of block length
  ttMessage->msgLen += 4;
  
  // Number of LONG/SHORT
  ttMessage->msgLen += 4;

  int countSymbol = RMList.countSymbol;
  int account;
  
  // Get RM Information from RMList
  for (account = 0; account < MAX_ACCOUNT; account++)
  {
    for (rmInfoIndex = 0; rmInfoIndex < countSymbol; rmInfoIndex++)
    {
      if ((RMList.collection[account][rmInfoIndex].leftShares != RMList.collection[account][rmInfoIndex].oldPositionSent[ttIndex].leftShares) ||
        (!feq(RMList.collection[account][rmInfoIndex].avgPrice, RMList.collection[account][rmInfoIndex].oldPositionSent[ttIndex].avgPrice)))
      {
        RMList.collection[account][rmInfoIndex].oldPositionSent[ttIndex].leftShares = RMList.collection[account][rmInfoIndex].leftShares;
        RMList.collection[account][rmInfoIndex].oldPositionSent[ttIndex].avgPrice = RMList.collection[account][rmInfoIndex].avgPrice;

        countRMInfo++;
        
        // Symbol
        strncpy((char *) &ttMessage->msgContent[ttMessage->msgLen], RMList.symbolList[rmInfoIndex].symbol, SYMBOL_LEN);
        ttMessage->msgLen += SYMBOL_LEN;
        
        // AVG price
        memcpy(&(ttMessage->msgContent[ttMessage->msgLen]), &RMList.collection[account][rmInfoIndex].oldPositionSent[ttIndex].avgPrice, 8);
        ttMessage->msgLen += 8;
        
        // Share
        memcpy(&(ttMessage->msgContent[ttMessage->msgLen]), &RMList.collection[account][rmInfoIndex].oldPositionSent[ttIndex].leftShares, 4);
        ttMessage->msgLen += 4;
        
        //account
        ttMessage->msgContent[ttMessage->msgLen++] = account;
      } 
    }
  }
  
  if (countRMInfo == 0)
  {
    return 0;
  }
  
  //TraceLog(DEBUG_LEVEL, "Send open positions: countItemUpdate = %d, bodyLength = %d\n", countRMInfo, ttMessage->msgLen - MSG_HEADER_LEN);
    
  // Update Number of LONG/SHORT field
  memcpy(&ttMessage->msgContent[10], &countRMInfo, 4);  
  
  // Update block length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  memcpy(&ttMessage->msgContent[6], &intConvert.value, 4);
  
  // Body length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[2] = intConvert.c[0];
  ttMessage->msgContent[3] = intConvert.c[1];
  ttMessage->msgContent[4] = intConvert.c[2];
  ttMessage->msgContent[5] = intConvert.c[3];
    
  return countRMInfo;
}

/****************************************************************************
- Function name:  BuildTTMessageForProfitLossSummary
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTTMessageForProfitLossSummary(void *message)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TTMessage *ttMessage = (t_TTMessage *)message;
  
  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_PROFIT_LOSS_SUMMARY_TO_TT;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1];
  ttMessage->msgLen += 2;

  // Update index of body length
  ttMessage->msgLen += 4;
  
  // Block length field
  intConvert.value = 24;
  ttMessage->msgContent[6] = intConvert.c[0];
  ttMessage->msgContent[7] = intConvert.c[1];
  ttMessage->msgContent[8] = intConvert.c[2];
  ttMessage->msgContent[9] = intConvert.c[3]; 
  
  // Update index of block length
  ttMessage->msgLen += 4;
  
  int rmInfoIndex, totalShares = 0;
  double totalMatchPL = 0.00, totalFee = 0.00, totalMarket = 0.00;
  int account, countSymbol = RMList.countSymbol;  
  
  for (account = 0; account < MAX_ACCOUNT; account++)
  {
    for (rmInfoIndex = 0; rmInfoIndex < countSymbol; rmInfoIndex++)
    {
      // Total shares
      totalShares += RMList.collection[account][rmInfoIndex].totalShares;

      // Total matched PL
      totalMatchPL += RMList.collection[account][rmInfoIndex].matchedPL;

      // Total market
      totalMarket += fabs(RMList.collection[account][rmInfoIndex].totalMarket);
      
      // Total fee
      totalFee += RMList.collection[account][rmInfoIndex].totalFee;
    }
  }
  
  // Total matched PL field
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], &totalMatchPL, 8);
  ttMessage->msgLen += 8;
  
  // Total NET PL
  double totalNetPL = totalMatchPL - totalFee;
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], &totalNetPL, 8);
  ttMessage->msgLen += 8;

  // Total market field
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], &totalMarket, 8);
  ttMessage->msgLen += 8;

  // Total shares
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], &totalShares, 4);
  ttMessage->msgLen += 4;
    
  // Body length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[2] = intConvert.c[0];
  ttMessage->msgContent[3] = intConvert.c[1];
  ttMessage->msgContent[4] = intConvert.c[2];
  ttMessage->msgContent[5] = intConvert.c[3];
    
  return ttMessage->msgLen;
}

/****************************************************************************
- Function name:  SendTTMessage
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendTTMessage(void *pConnection, void *message)
{
  int result;
  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection; 

  t_TTMessage *ttMessage = (t_TTMessage *)message;

  pthread_mutex_lock(&(connection->socketMutex));
  
  // Check connection between AS and TT
  if ((connection->status != DISCONNECT) && (connection->socket != -1))
  {
    // Send message to Trader Tool
    result = send(connection->socket, ttMessage->msgContent, ttMessage->msgLen, 0);
    
    if (result != ttMessage->msgLen)
    {
      TraceLog(DEBUG_LEVEL, "FAIL: SendTTMessage(), socket = %d, numBytesSent = %d, numBytesExpected = %d\n", connection->socket, result, ttMessage->msgLen);
      //PrintErrStr(ERROR_LEVEL, "Calling send(): ", errno);
      
      pthread_mutex_unlock(&(connection->socketMutex));
      
      return ERROR;
    }
  }
  else
  {
    pthread_mutex_unlock(&(connection->socketMutex));
    
    return ERROR;
  }

  pthread_mutex_unlock(&(connection->socketMutex));

  //Successfully sent
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessResetASCurrentStatus
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessResetASCurrentStatus(int type, int account)
{
  int tsIndex;

  if (type == NUM_STUCK_INDEX)
  {
    // Reset num stuck of AS
    traderToolsInfo.currentStatus.numStuck = 0;
  }
  else if (type == NUM_LOSER_INDEX)
  {
    // Reset num loser of AS
    traderToolsInfo.currentStatus.numLoser = 0;
  }
  else if (type == BUYING_POWER_INDEX)
  {
    // Function update buying power to all TSs
    return SendUpdateBuyingPowerToAllTS(account);
  }

  for (tsIndex = 0; tsIndex < MAX_TRADE_SERVER_CONNECTIONS; tsIndex++)
  {
    // Check connection between AS and TS
    if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
    {
      // Reset current status
      if (type == NUM_STUCK_INDEX)
      {
        // Reset num stuck of TS
        tradeServersInfo.currentStatus[tsIndex].numStuck = 0;
      }
      else if (type == NUM_LOSER_INDEX)
      {
        // Reset num loser of TS
        tradeServersInfo.currentStatus[tsIndex].numLoser = 0;
      }

      continue;
    }   
        
    // Call function to reset TS current status (only num of stucks and num of losers)
    SendResetTSCurrentStatusToTS(tsIndex, type);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessResponseSendNewOrderFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessResponseSendNewOrderFromTT(t_SendNewOrder *newOrder, int sendId, int reason, int ttIndex)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];

  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  t_TTMessage ttMessage;

  // Build response send new order message to send Trader Tool
  BuildTTMessageForResponseSendNewOrder(&ttMessage, newOrder, sendId, reason);
  
  // Send message to Trader Tool
  return SendTTMessage(connection, &ttMessage);
}

/****************************************************************************
- Function name:  BuildTTMessageForResponseSendNewOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTTMessageForResponseSendNewOrder(void *message, t_SendNewOrder *newOrder, int sendId, int reason)
{
  TraceLog(DEBUG_LEVEL, "Response to 'manual order request from TT': sendId: %d, orderId: %d, response: %d\n", sendId, newOrder->orderId, reason);

  t_ShortConverter shortConvert;
  t_IntConverter intConvert;
  t_DoubleConverter doubleConvert;

  t_TTMessage *ttMessage = (t_TTMessage *)message;
  
  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_RESPONSE_NEW_ORDER_FROM_TT;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1];
  ttMessage->msgLen += 2;

  // Update index of body length
  ttMessage->msgLen += 4;

  // Update index of block length
  ttMessage->msgLen += 4;

  /* Block content */
  // send Id (4 bytes)
  intConvert.value = sendId;
  ttMessage->msgContent[ttMessage->msgLen] = intConvert.c[0];
  ttMessage->msgContent[ttMessage->msgLen + 1] = intConvert.c[1];
  ttMessage->msgContent[ttMessage->msgLen + 2] = intConvert.c[2];
  ttMessage->msgContent[ttMessage->msgLen + 3] = intConvert.c[3];
  ttMessage->msgLen += 4;

  // Symbol (8 bytes)
  strncpy((char *)&ttMessage->msgContent[ttMessage->msgLen], newOrder->symbol, SYMBOL_LEN);
  ttMessage->msgLen += SYMBOL_LEN;
  
  //Side (4 bytes)
  intConvert.value = newOrder->side;
  ttMessage->msgContent[ttMessage->msgLen] = intConvert.c[0];
  ttMessage->msgContent[ttMessage->msgLen + 1] = intConvert.c[1];
  ttMessage->msgContent[ttMessage->msgLen + 2] = intConvert.c[2];
  ttMessage->msgContent[ttMessage->msgLen + 3] = intConvert.c[3];
  ttMessage->msgLen += 4;
  
  //Shares
  intConvert.value = newOrder->shares;
  ttMessage->msgContent[ttMessage->msgLen] = intConvert.c[0];
  ttMessage->msgContent[ttMessage->msgLen + 1] = intConvert.c[1];
  ttMessage->msgContent[ttMessage->msgLen + 2] = intConvert.c[2];
  ttMessage->msgContent[ttMessage->msgLen + 3] = intConvert.c[3];
  ttMessage->msgLen += 4;
  
  //Max Floor
  intConvert.value = newOrder->maxFloor;
  ttMessage->msgContent[ttMessage->msgLen] = intConvert.c[0];
  ttMessage->msgContent[ttMessage->msgLen + 1] = intConvert.c[1];
  ttMessage->msgContent[ttMessage->msgLen + 2] = intConvert.c[2];
  ttMessage->msgContent[ttMessage->msgLen + 3] = intConvert.c[3];
  ttMessage->msgLen += 4;
  
  //price
  doubleConvert.value = newOrder->price;
  ttMessage->msgContent[ttMessage->msgLen] = doubleConvert.c[0];
  ttMessage->msgContent[ttMessage->msgLen + 1] = doubleConvert.c[1];
  ttMessage->msgContent[ttMessage->msgLen + 2] = doubleConvert.c[2];
  ttMessage->msgContent[ttMessage->msgLen + 3] = doubleConvert.c[3];
  ttMessage->msgContent[ttMessage->msgLen + 4] = doubleConvert.c[4];
  ttMessage->msgContent[ttMessage->msgLen + 5] = doubleConvert.c[5];
  ttMessage->msgContent[ttMessage->msgLen + 6] = doubleConvert.c[6];
  ttMessage->msgContent[ttMessage->msgLen + 7] = doubleConvert.c[7];
  ttMessage->msgLen += 8;
  
  //ECN ID
  intConvert.value = AS_TO_TT_ORDER_TYPE_MAPPING[newOrder->ECNId - 100];
  ttMessage->msgContent[ttMessage->msgLen] = intConvert.c[0];
  ttMessage->msgContent[ttMessage->msgLen + 1] = intConvert.c[1];
  ttMessage->msgContent[ttMessage->msgLen + 2] = intConvert.c[2];
  ttMessage->msgContent[ttMessage->msgLen + 3] = intConvert.c[3];
  ttMessage->msgLen += 4;
  
  //TIF
  intConvert.value = newOrder->tif;
  ttMessage->msgContent[ttMessage->msgLen] = intConvert.c[0];
  ttMessage->msgContent[ttMessage->msgLen + 1] = intConvert.c[1];
  ttMessage->msgContent[ttMessage->msgLen + 2] = intConvert.c[2];
  ttMessage->msgContent[ttMessage->msgLen + 3] = intConvert.c[3];
  ttMessage->msgLen += 4;
  
  //Peg Difference
  doubleConvert.value = newOrder->pegDef;
  ttMessage->msgContent[ttMessage->msgLen] = doubleConvert.c[0];
  ttMessage->msgContent[ttMessage->msgLen + 1] = doubleConvert.c[1];
  ttMessage->msgContent[ttMessage->msgLen + 2] = doubleConvert.c[2];
  ttMessage->msgContent[ttMessage->msgLen + 3] = doubleConvert.c[3];
  ttMessage->msgContent[ttMessage->msgLen + 4] = doubleConvert.c[4];
  ttMessage->msgContent[ttMessage->msgLen + 5] = doubleConvert.c[5];
  ttMessage->msgContent[ttMessage->msgLen + 6] = doubleConvert.c[6];
  ttMessage->msgContent[ttMessage->msgLen + 7] = doubleConvert.c[7];
  ttMessage->msgLen += 8;
  
  //OrderID
  intConvert.value = newOrder->orderId;
  ttMessage->msgContent[ttMessage->msgLen] = intConvert.c[0];
  ttMessage->msgContent[ttMessage->msgLen + 1] = intConvert.c[1];
  ttMessage->msgContent[ttMessage->msgLen + 2] = intConvert.c[2];
  ttMessage->msgContent[ttMessage->msgLen + 3] = intConvert.c[3];
  ttMessage->msgLen += 4;
  
  //trading account
  ttMessage->msgContent[ttMessage->msgLen++] = newOrder->tradingAccount;
  
  /* 
    Reason, has one of following values:
    -1: could not send order
    -2: Failed to check with last price, ask user if he wants to force?
    >=0: Manual Order sent successfully
  */
  intConvert.value = reason;
  ttMessage->msgContent[ttMessage->msgLen] = intConvert.c[0];
  ttMessage->msgContent[ttMessage->msgLen + 1] = intConvert.c[1];
  ttMessage->msgContent[ttMessage->msgLen + 2] = intConvert.c[2];
  ttMessage->msgContent[ttMessage->msgLen + 3] = intConvert.c[3];
  ttMessage->msgLen += 4;
  
  // Block length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  ttMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  ttMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  ttMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field 
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[2] = intConvert.c[0];
  ttMessage->msgContent[3] = intConvert.c[1];
  ttMessage->msgContent[4] = intConvert.c[2];
  ttMessage->msgContent[5] = intConvert.c[3];

  return ttMessage->msgLen;
}

/****************************************************************************
- Function name:  SendPMConfToTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendPMConfToTT(void *pConnection)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Send PM conf to TT ...\n");

  t_TTMessage ttMessage;

  // Build PM configuration message to send Trader Tool
  BuildTTMessageForPMConf(&ttMessage);

  // Send message to Trader Tool
  return SendTTMessage(connection, &ttMessage);
}

/****************************************************************************
- Function name:  BuildTTMessageForPMConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTTMessageForPMConf(void *message)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TTMessage *ttMessage = (t_TTMessage *)message;
  
  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_PM_CONF_TO_TT;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1];
  ttMessage->msgLen += 2;

  // Update index of body length
  ttMessage->msgLen += 4;

  // Message body = n * (blockLength (4 bytes) + blockContent (blockLength bytes))
  
  // Update index of block length
  ttMessage->msgLen += 4;
  
  // Block content = PM configuration
  int size = sizeof(t_TradeServerConf);

  memcpy(&ttMessage->msgContent[ttMessage->msgLen], (unsigned char *)&pmInfo.config, size);
  ttMessage->msgLen += size;

  // Block length field
  intConvert.value = size + 4;
  ttMessage->msgContent[ttMessage->msgLen - size - 4] = intConvert.c[0];
  ttMessage->msgContent[ttMessage->msgLen - size - 3] = intConvert.c[1];
  ttMessage->msgContent[ttMessage->msgLen - size - 2] = intConvert.c[2];
  ttMessage->msgContent[ttMessage->msgLen - size - 1] = intConvert.c[3];
  
  // Body length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[2] = intConvert.c[0];
  ttMessage->msgContent[3] = intConvert.c[1];
  ttMessage->msgContent[4] = intConvert.c[2];
  ttMessage->msgContent[5] = intConvert.c[3];

  return ttMessage->msgLen;
}

/****************************************************************************
- Function name:  ProcessTTMessageForConnectToPM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForConnectToPM(void *message)
{
  TraceLog(DEBUG_LEVEL, "Received message to request AS connect to PM\n");

  // Call function to connect to TS
  Connect2PM();

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForDisconnectToPM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForDisconnectToPM(void *message)
{
  TraceLog(DEBUG_LEVEL, "Received message to request AS disconnect to PM\n");

  // Call function to connect to TS
  Disconnect2PM();

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendPMBookConfToAllTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendPMBookConfToAllTT(int ECNIndex)
{
  int ttIndex;
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;

  // Build PM Book configuration message to send Trader Tool
  BuildTTMessageForPMBookConf(&ttMessage, ECNIndex);

  // Send message to all Trader Tool connected to AS
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];
    
    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_TRADER_TOOL)
    {
      continue;
    }

    // Send message to Trader Tool
    SendTTMessage(connection, &ttMessage);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  BuildTTMessageForPMBookConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTTMessageForPMBookConf(void *message, int ECNIndex)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TTMessage *ttMessage = (t_TTMessage *)message;
  
  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_PM_BOOK_CONF_TO_TT;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1];
  ttMessage->msgLen += 2;

  // Update index of body length
  ttMessage->msgLen += 4;

  // Message body = n * (blockLength (4 bytes) + blockContent (blockLength bytes))  
  // Update index of block length
  ttMessage->msgLen += 4;

  // Block content = ECN Book Index (4 bytes) + ECN Book conf

  // ECN Book Index (4 bytes)
  intConvert.value = PM_TO_TT_BOOK_INDEX_MAPPING[ECNIndex];
  ttMessage->msgContent[ttMessage->msgLen] = intConvert.c[0];
  ttMessage->msgContent[ttMessage->msgLen + 1] = intConvert.c[1];
  ttMessage->msgContent[ttMessage->msgLen + 2] = intConvert.c[2];
  ttMessage->msgContent[ttMessage->msgLen + 3] = intConvert.c[3];

  // Update index of body length
  ttMessage->msgLen += 4;

  // ECN Book configuration
  int size = 0;

  switch (ECNIndex)
  {
    // ARCA Book configuration
    case PM_ARCA_BOOK_INDEX:
      // SourceID
      memcpy(&ttMessage->msgContent[ttMessage->msgLen], pmInfo.ECNBooksConf.ARCABookConf.sourceID, MAX_LINE_LEN);
      ttMessage->msgLen += MAX_LINE_LEN;

      int groupIndex;
      for (groupIndex = 0; groupIndex < MAX_ARCA_MULTICAST_GROUP; groupIndex ++)
      {
        // Retransmission Request TCP/IP
        // Index
        memcpy(&ttMessage->msgContent[ttMessage->msgLen], &pmInfo.ECNBooksConf.ARCABookConf.retranConn[groupIndex].index, 4);
        ttMessage->msgLen += 4;

        // IP Address
        memcpy(&ttMessage->msgContent[ttMessage->msgLen], pmInfo.ECNBooksConf.ARCABookConf.retranConn[groupIndex].ip, MAX_LINE_LEN);
        ttMessage->msgLen += MAX_LINE_LEN;
        
        // Port
        memcpy(&ttMessage->msgContent[ttMessage->msgLen], &pmInfo.ECNBooksConf.ARCABookConf.retranConn[groupIndex].port, 4);
        ttMessage->msgLen += 4;
      }
      // A or B feed
      ttMessage->msgContent[ttMessage->msgLen] = pmInfo.ECNBooksConf.ARCABookConf.currentFeedName;
      ttMessage->msgLen += 1;
      
      size = MAX_LINE_LEN + MAX_ARCA_MULTICAST_GROUP * (MAX_LINE_LEN + 8) + 1;
      break;
    
    // NASDAQ Book configuration
    case PM_NASDAQ_BOOK_INDEX:
      memcpy(&ttMessage->msgContent[ttMessage->msgLen], pmInfo.ECNBooksConf.NASDAQBookConf.UDP.ip, MAX_LINE_LEN);
      ttMessage->msgLen += MAX_LINE_LEN;

      memcpy(&ttMessage->msgContent[ttMessage->msgLen], &pmInfo.ECNBooksConf.NASDAQBookConf.UDP.port, 4);
      ttMessage->msgLen += 4;

      memcpy(&ttMessage->msgContent[ttMessage->msgLen], pmInfo.ECNBooksConf.NASDAQBookConf.TCP.ip, MAX_LINE_LEN);
      ttMessage->msgLen += MAX_LINE_LEN;

      memcpy(&ttMessage->msgContent[ttMessage->msgLen], &pmInfo.ECNBooksConf.NASDAQBookConf.TCP.port, 4);
      ttMessage->msgLen += 4;

      size = 2 * (MAX_LINE_LEN + 4);

      //TraceLog(DEBUG_LEVEL, "ip = %s, port = %d, ip = %s, port = %d\n", pmInfo.ECNBooksConf.NASDAQBookConf.UDP.ip, pmInfo.ECNBooksConf.NASDAQBookConf.UDP.port, pmInfo.ECNBooksConf.NASDAQBookConf.TCP.ip, pmInfo.ECNBooksConf.NASDAQBookConf.TCP.port);
      break;
  }

  // Block length field
  intConvert.value = size + 8;
  ttMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  ttMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  ttMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  ttMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field 
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[2] = intConvert.c[0];
  ttMessage->msgContent[3] = intConvert.c[1];
  ttMessage->msgContent[4] = intConvert.c[2];
  ttMessage->msgContent[5] = intConvert.c[3];

  return ttMessage->msgLen;
}

/****************************************************************************
- Function name:  SendPMOrderConfToAllTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendPMOrderConfToAllTT(int ECNIndex)
{
  int ttIndex;
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;

  // Build PM Order configuration message to send Trader Tool
  BuildTTMessageForPMOrderConf(&ttMessage, ECNIndex);

  // Send message to all Trader Tool connected to AS
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];

    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_TRADER_TOOL)
    {
      continue;
    }   

    // Send message to Trader Tool
    SendTTMessage(connection, &ttMessage);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  BuildTTMessageForPMOrderConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTTMessageForPMOrderConf(void *message, int ECNIndex)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TTMessage *ttMessage = (t_TTMessage *)message;
  
  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_PM_ORDER_CONF_TO_TT;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1];
  ttMessage->msgLen += 2;

  // Update index of body length
  ttMessage->msgLen += 4;

  // Message body = n * (blockLength (4 bytes) + blockContent (blockLength bytes))  
  // Update index of block length
  ttMessage->msgLen += 4;

  // Block content = ECN Order Index (4 bytes) + ECN Order conf

  // ECN Order Index (4 bytes)
  intConvert.value = PM_TO_TT_ORDER_INDEX_MAPPING[ECNIndex];
  ttMessage->msgContent[ttMessage->msgLen] = intConvert.c[0];
  ttMessage->msgContent[ttMessage->msgLen + 1] = intConvert.c[1];
  ttMessage->msgContent[ttMessage->msgLen + 2] = intConvert.c[2];
  ttMessage->msgContent[ttMessage->msgLen + 3] = intConvert.c[3];

  // Update index
  ttMessage->msgLen += 4;

  // ECN Order configuration
  int size = sizeof(t_ECNOrderConf);
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], (unsigned char *)&pmInfo.ECNOrdersConf[ECNIndex], size);
  ttMessage->msgLen += size;

  // Block length field
  intConvert.value = size + 8;
  ttMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  ttMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  ttMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  ttMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[2] = intConvert.c[0];
  ttMessage->msgContent[3] = intConvert.c[1];
  ttMessage->msgContent[4] = intConvert.c[2];
  ttMessage->msgContent[5] = intConvert.c[3];

  return ttMessage->msgLen;
}

/****************************************************************************
- Function name:  SendPMGlobalConfToAllTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendPMGlobalConfToAllTT(void)
{
  int ttIndex;
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;

  // Build PM global configuration message to send Trader Tool
  BuildTTMessageForPMGlobalConf(&ttMessage);

  // Send message to all Trader Tool connected to AS
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];

    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_TRADER_TOOL)
    {
      continue;
    }   

    // Send message to Trader Tool
    SendTTMessage(connection, &ttMessage);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  BuildTTMessageForPMGlobalConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTTMessageForPMGlobalConf(void *message)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TTMessage *ttMessage = (t_TTMessage *)message;
  
  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_PM_GLOBAL_SETTING_TO_TT;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1];
  ttMessage->msgLen += 2;

  // Update index of body length
  ttMessage->msgLen += 4;

  // Message body = n * (blockLength (4 bytes) + blockContent (blockLength bytes))  
  // Update index of block length
  ttMessage->msgLen += 4;

  // Block content = global conf

  // Global configuration
  int size = sizeof(t_PMGlobalConf);
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], (unsigned char *)&pmInfo.globalConf, size);
  ttMessage->msgLen += size;

  // Block length field
  intConvert.value = size + 4;
  ttMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  ttMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  ttMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  ttMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[2] = intConvert.c[0];
  ttMessage->msgContent[3] = intConvert.c[1];
  ttMessage->msgContent[4] = intConvert.c[2];
  ttMessage->msgContent[5] = intConvert.c[3];

  return ttMessage->msgLen;
}

/****************************************************************************
- Function name:  SendPMIgnoredSymbolToAllTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendPMIgnoredSymbolToAllTT(void)
{
  int ttIndex;
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;

  // Build PM ignored symbol message to send Trader Tool
  BuildTTMessageForPMIgnoredSymbol(&ttMessage);

  // Send message to all Trader Tool connected to AS
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];

    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_TRADER_TOOL)
    {
      continue;
    }   

    // Send message to Trader Tool
    SendTTMessage(connection, &ttMessage);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  BuildTTMessageForPMIgnoredSymbol
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTTMessageForPMIgnoredSymbol(void *message)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TTMessage *ttMessage = (t_TTMessage *)message;
  
  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_PM_IGNORE_SYMBOL_TO_TT;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1];
  ttMessage->msgLen += 2;

  // Update index of body length
  ttMessage->msgLen += 4;

  // Message body = n * (blockLength (4 bytes) + blockContent (blockLength bytes))  
  // Update index of block length
  ttMessage->msgLen += 4;

  // Block content = PM ignore symbol

  // 
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], (unsigned char *)&pmIgnoreStockList.stockList, pmIgnoreStockList.countStock * SYMBOL_LEN);
  ttMessage->msgLen += pmIgnoreStockList.countStock * SYMBOL_LEN;

  // Block length field
  intConvert.value = pmIgnoreStockList.countStock * SYMBOL_LEN + 4;
  ttMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  ttMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  ttMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  ttMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[2] = intConvert.c[0];
  ttMessage->msgContent[3] = intConvert.c[1];
  ttMessage->msgContent[4] = intConvert.c[2];
  ttMessage->msgContent[5] = intConvert.c[3];

  return ttMessage->msgLen;
}

/****************************************************************************
- Function name:  SendPMCurrentStatusToTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendPMCurrentStatusToTT(void *pConnection)
{
  //TraceLog(DEBUG_LEVEL, "Send PM current status to TT ...\n");

  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  t_TTMessage ttMessage;
  
  // Build PM current status message to send Trader Tool 
  BuildTTMessageForPMCurrentStatus(&ttMessage);

  // Send message to Trader Tool
  return SendTTMessage(connection, &ttMessage);
}

/****************************************************************************
- Function name:  BuildTTMessageForPMCurrentStatus
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTTMessageForPMCurrentStatus(void *message)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TTMessage *ttMessage = (t_TTMessage *)message;
  
  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_PM_CURRENT_STATUS_TO_TT;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1];
  ttMessage->msgLen += 2;

  // Update index of body length
  ttMessage->msgLen += 4;

  // Message body = blockLength (4 bytes) + blockContent (blockLength bytes)

  // Update index of block length
  ttMessage->msgLen += 4;

  // Block content = PM current status
  
  // PM trading status
  ttMessage->msgContent[ttMessage->msgLen] = pmInfo.connection.status;
  ttMessage->msgLen += 1;

  ttMessage->msgContent[ttMessage->msgLen] = pmInfo.currentStatus.tradingStatus;
  ttMessage->msgLen += 1;

  // PM Books status
  ttMessage->msgContent[ttMessage->msgLen] = pmInfo.currentStatus.pmMdcStatus[PM_ARCA_BOOK_INDEX];
  ttMessage->msgLen += 1;

  ttMessage->msgContent[ttMessage->msgLen] = pmInfo.currentStatus.pmMdcStatus[PM_NASDAQ_BOOK_INDEX];
  ttMessage->msgLen += 1;

  ttMessage->msgContent[ttMessage->msgLen] = 0; //BATSZ book status
  ttMessage->msgLen += 1;

  // PM Orders status
  ttMessage->msgContent[ttMessage->msgLen] = 0; //DIRECT Order status
  ttMessage->msgLen += 1;

  ttMessage->msgContent[ttMessage->msgLen] = 0; //Ouch status
  ttMessage->msgLen += 1;

  ttMessage->msgContent[ttMessage->msgLen] = 0; // batsz order status
  ttMessage->msgLen += 1;
  
  ttMessage->msgContent[ttMessage->msgLen] = pmInfo.currentStatus.pmOecStatus[PM_NASDAQ_RASH_INDEX];
  ttMessage->msgLen += 1;

  ttMessage->msgContent[ttMessage->msgLen] = pmInfo.currentStatus.pmOecStatus[PM_ARCA_DIRECT_INDEX];
  ttMessage->msgLen += 1;

  // CTS Feed status
  ttMessage->msgContent[ttMessage->msgLen] = pmInfo.currentStatus.statusCTS_Feed;
  ttMessage->msgLen += 1;
  
  //khanhnd
  //CQS Feed status
  ttMessage->msgContent[ttMessage->msgLen] = pmInfo.currentStatus.statusCQS_Feed;
  ttMessage->msgLen += 1;

  //For UQDF
  //UQDF Feed status
  ttMessage->msgContent[ttMessage->msgLen] = pmInfo.currentStatus.statusUQDF_Feed;
  ttMessage->msgLen += 1;

  // UTDF status
  ttMessage->msgContent[ttMessage->msgLen] = pmInfo.currentStatus.statusUTDF;
  ttMessage->msgLen += 1;

  // Block length field
  intConvert.value = 12 + 4;
  ttMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  ttMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  ttMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  ttMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[2] = intConvert.c[0];
  ttMessage->msgContent[3] = intConvert.c[1];
  ttMessage->msgContent[4] = intConvert.c[2];
  ttMessage->msgContent[5] = intConvert.c[3];

  return ttMessage->msgLen;
}

/****************************************************************************
- Function name:  SendNotifyMessageToAllTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendNotifyMessageToAllTT(char *msg, int level)
{
  int ttIndex;
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;

  BuildTTMessageForNotifyMessage(&ttMessage, msg, level);

  // Send message to all Trader Tool connected to AS
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];

    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_TRADER_TOOL)
    {
      continue;
    }   

    // Send message to Trader Tool
    SendTTMessage(connection, &ttMessage);
  }
  
  return SUCCESS;
}


/****************************************************************************
- Function name:  SendNotifyMessageToTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendNotifyMessageToTT(int ttIndex, char *msg, int level)
{
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;

  BuildTTMessageForNotifyMessage(&ttMessage, msg, level);

  // Send message
  connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];

  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return SUCCESS;
  }

  // Send message to Trader Tool
  return SendTTMessage(connection, &ttMessage);
}

/****************************************************************************
- Function name:  BuildTTMessageForNotifyMessage
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTTMessageForNotifyMessage(void *message, char *msg, int level)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TTMessage *ttMessage = (t_TTMessage *)message;
  
  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_NOTIFY_MSG_TO_TT;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1];
  ttMessage->msgLen += 2;

  // Update index of body length
  ttMessage->msgLen += 4;

  // Message body = blockLength (4 bytes) + blockContent (blockLength bytes)

  // Update index of block length
  ttMessage->msgLen += 4;

  // Block content = level 4 bytes + msg (1024 bytes)
  //level
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], &level, 4);
  ttMessage->msgLen += 4;
  
  //msg
  strncpy((char *)&ttMessage->msgContent[ttMessage->msgLen], msg, 1024);
  ttMessage->msgLen += 1024;
  
  // Block length field
  intConvert.value = 12 + 4;
  ttMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  ttMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  ttMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  ttMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[2] = intConvert.c[0];
  ttMessage->msgContent[3] = intConvert.c[1];
  ttMessage->msgContent[4] = intConvert.c[2];
  ttMessage->msgContent[5] = intConvert.c[3];

  return ttMessage->msgLen;
}

/****************************************************************************
- Function name:  SendAllConfOfPMToTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendAllConfOfPMToTT(void *pConnection)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  // Check connection between AS and PM
  if ((pmInfo.connection.status == DISCONNECT) || (pmInfo.connection.socket == -1))
  {
    return SUCCESS;
  }
  
  int ECNIndex;

  // Send Books configuration
  for (ECNIndex = 0; ECNIndex < PM_MAX_BOOK_CONNECTIONS; ECNIndex++)
  {
    if (SendPMBookConfToTT(connection, ECNIndex) == ERROR)
    {
      return ERROR;
    }
  }

  // Send Orders configuration
  for (ECNIndex = 0; ECNIndex < PM_MAX_ORDER_CONNECTIONS; ECNIndex++)
  {
    if (SendPMOrderConfToTT(connection, ECNIndex) == ERROR)
    {
      return ERROR;
    }
  }
  
  // Send PM global configuration
  if (SendPMGlobalConfToTT(connection) == ERROR)
  {
    return ERROR;
  }
  
  // Send PM Exchange configuration
  if (SendPMExchangesConfToTT(connection) == ERROR)
  {
    return ERROR;
  }
  
  // Send PM ignored symbol
  if (SendPMIgnoredSymbolToToTT(connection) == ERROR)
  {
    return ERROR;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendPMBookConfToTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendPMBookConfToTT(void *pConnection, int ECNIndex)
{
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Send PM Book conf to TT, ECNIndex = %d ...\n", ECNIndex);

  // Build PM Book configuration message to send Trader Tool
  BuildTTMessageForPMBookConf(&ttMessage, ECNIndex);  

  // Send message to Trader Tool
  return SendTTMessage(connection, &ttMessage);
}

/****************************************************************************
- Function name:  SendPMOrderConfToTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendPMOrderConfToTT(void *pConnection, int ECNIndex)
{
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Send PM Order conf to TT, ECNIndex = %d ...\n", ECNIndex);

  // Build PM Order configuration message to send Trader Tool
  BuildTTMessageForPMOrderConf(&ttMessage, ECNIndex); 

  // Send message to Trader Tool
  return SendTTMessage(connection, &ttMessage);
}

/****************************************************************************
- Function name:  SendPMGlobalConfToTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendPMGlobalConfToTT(void *pConnection)
{
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Send PM global conf to TT ...\n");

  // Build PM global configuration message to send Trader Tool
  BuildTTMessageForPMGlobalConf(&ttMessage);

  // Send message to Trader Tool
  return SendTTMessage(connection, &ttMessage);
}

/****************************************************************************
- Function name:  SendPMIgnoredSymbolToToTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendPMIgnoredSymbolToToTT(void *pConnection)
{
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Send PM ignored symbol to TT ...\n");

  // Build PM ignored symbol message to send Trader Tool
  BuildTTMessageForPMIgnoredSymbol(&ttMessage);

  // Send message to Trader Tool
  return SendTTMessage(connection, &ttMessage);
}

/****************************************************************************
- Function name:  ProcessTTMessageForSetPMConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForSetPMConf(void *message)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Block content = PM configuration
  memcpy((unsigned char *)&pmInfo.config, &ttMessage->msgContent[currentIndex], sizeof(t_ConnectionInfo));

  // Save & send PM configuration to all PM
  if (SavePMConf() == SUCCESS)
  {
    SetPMConfToPM();
    SendPMConfToAllTT();    
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendPMConfToAllTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendPMConfToAllTT(void)
{
  int ttIndex;
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;

  // Build PM configuration message to send Trader Tool
  BuildTTMessageForPMConf(&ttMessage);

  // Send message to all Trader Tool connected to AS
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];
    
    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_TRADER_TOOL)
    {
      continue;
    }

    // Send message to Trader Tool
    SendTTMessage(connection, &ttMessage);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForSetPMGlobalConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForSetPMGlobalConf(void *message)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Block content = PM global conf

  // Global configuration
  memcpy((unsigned char *)&pmInfo.globalConf, &ttMessage->msgContent[currentIndex], sizeof(t_PMGlobalConf));

  // Call function set global configuration to PM
  SetPMGlobalConfToPM();

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForSetPMOrderConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForSetPMOrderConf(void *message)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Block content = ECN Order Index (4 bytes) + ECN Order conf
  
  // ECN Order Index (4 bytes)
  int ECNIndex = GetIntNumber(&ttMessage->msgContent[currentIndex]);
  ECNIndex = TT_TO_PM_ORDER_INDEX_MAPPING[ECNIndex];

  TraceLog(DEBUG_LEVEL, "Set PM Order conf, ECNIndex = %d\n", ECNIndex);

  // Update current index
  currentIndex += 4;

  // Check ECN Order Id
  if ((ECNIndex == PM_NASDAQ_RASH_INDEX) || (ECNIndex == PM_ARCA_DIRECT_INDEX))
  { 
    // ECN Order configuration
    memcpy((unsigned char *)&pmInfo.ECNOrdersConf[ECNIndex], &ttMessage->msgContent[currentIndex], sizeof(t_ECNOrderConf));
  }
  else
  {
    TraceLog(ERROR_LEVEL, "Invalid set PM Order configuration message from TT, ECNIndex = %d\n", ECNIndex);

    return ERROR;
  } 

  // Call function set PM Order configuration to PM
  SetPMOrderConfToPM(ECNIndex);

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForSetPMIgnoreSymbol
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForSetPMIgnoreSymbol(void *message, int ttIndex)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Block content = type(4 bytes: Add = 0, Remove = 1) + a symbol (8 bytes)
  
  // Type
  int type = GetIntNumber(&ttMessage->msgContent[currentIndex]);

  // Update current index
  currentIndex += 4;

  // Symbol
  char symbol[SYMBOL_LEN];

  strncpy(symbol, (char *) &ttMessage->msgContent[currentIndex], SYMBOL_LEN);   
  
  // Send PM ignored symbol list to PM
  if (SetPMIgnoreStockListToPM(type, symbol) == SUCCESS)
  {
    // Update TT event log to database
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    sprintf(activityDetail, "%d,%d,%.8s", traderToolsInfo.connection[ttIndex].currentState, type, symbol);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_IGNORE_STOCK_LIST_PM, activityDetail);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForEnablePMFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForEnablePMFromTT(void *message, int ttIndex)
{
  TraceLog(DEBUG_LEVEL, "[%s]Received message to request enable PM from TT\n", traderToolsInfo.connection[ttIndex].userName);

  // Call function to enable PM to PM
  if (SendEnableTradingPMToPM() == SUCCESS)
  {
    //Update enable trading event log to database
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    sprintf(activityDetail, "%d,0,99", traderToolsInfo.connection[ttIndex].currentState);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_ENABLE_DISENABLE_TRADING, activityDetail);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForDisablePMFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForDisablePMFromTT(void *message, int ttIndex)
{
  TraceLog(DEBUG_LEVEL, "[%s]Received message to request disable PM from TT\n", traderToolsInfo.connection[ttIndex].userName);

  // Call function to disable PM to PM
  if (SendDisableTradingPMToPM(ttIndex) == SUCCESS)
  {
    //Update disenable trading event log to database
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    sprintf(activityDetail, "%d,1,99", traderToolsInfo.connection[ttIndex].currentState);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_ENABLE_DISENABLE_TRADING, activityDetail);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForConnectAPMOrderFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForConnectAPMOrderFromTT(void *message, int ttIndex)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // ECN Order Index (4 bytes)
  int ECNIndex = GetIntNumber(&ttMessage->msgContent[currentIndex]);
  ECNIndex = TT_TO_PM_ORDER_INDEX_MAPPING[ECNIndex];

  TraceLog(DEBUG_LEVEL, "Received message to request PM connect to a ECN Order, ECNIndex = %d\n", ECNIndex);

  // Call function to connect to request connect to a ECN Order to PM
  if (SendConnectOrderToPM(ECNIndex) == SUCCESS)
  {
    //Update TT event log: TT request connect to ECN Order on PM
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    sprintf(activityDetail, "%d,0,5%d", traderToolsInfo.connection[ttIndex].currentState, ECNIndex + 1);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_ORDER, activityDetail);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForDisconnectAPMOrderFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForDisconnectAPMOrderFromTT(void *message, int ttIndex)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // ECN Order Index (4 bytes)
  int ECNIndex = GetIntNumber(&ttMessage->msgContent[currentIndex]);
  ECNIndex = TT_TO_PM_ORDER_INDEX_MAPPING[ECNIndex];

  TraceLog(DEBUG_LEVEL, "Received message to request PM disconnect to a ECN Order, ECNIndex = %d\n", ECNIndex);

  // Call function to disconnect to a ECN Order to PM
  if (SendDisconnectOrderToPM(ECNIndex) == SUCCESS)
  {
    //Update TT event log: TT request disconnect to ECN Order on PM
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    sprintf(activityDetail, "%d,1,5%d", traderToolsInfo.connection[ttIndex].currentState, ECNIndex + 1);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_ORDER, activityDetail);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForConnectAllPMOrderFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForConnectAllPMOrderFromTT(void *message, int ttIndex)
{
  TraceLog(DEBUG_LEVEL, "Received message to request PM connect to all ECN Order\n");

  // Call function to request PM connect to all ECN Orders
  if (ProcessPMConnectAllECNOrder() == SUCCESS)
  {
    //Update TT event log: TT request connect to all ECN Orders on PM
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    sprintf(activityDetail, "%d,0,50", traderToolsInfo.connection[ttIndex].currentState);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_ORDER, activityDetail);
  } 

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForDisconnectAllPMOrderFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForDisconnectAllPMOrderFromTT(void *message, int ttIndex)
{
  TraceLog(DEBUG_LEVEL, "Received message to request PM disconnect to all ECN Order\n");

  // Call function to request PM disconnect to all ECN Orders
  if (ProcessPMDisconnectAllECNOrder() == SUCCESS)
  {
    //Update TT event log: TT request disconnect to all ECN Orders on PM
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    sprintf(activityDetail, "%d,1,50", traderToolsInfo.connection[ttIndex].currentState);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_ORDER, activityDetail);
  } 

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForConnectAPMBookFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForConnectAPMBookFromTT(void *message, int ttIndex)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // ECN Book Index (4 bytes)
  int ECNIndex = GetIntNumber(&ttMessage->msgContent[currentIndex]);
  ECNIndex = TT_TO_PM_BOOK_INDEX_MAPPING[ECNIndex];

  TraceLog(DEBUG_LEVEL, "Received message to request PM connect to a ECN Book, ECNIndex = %d\n", ECNIndex);

  // Call function to connect to request connect to a ECN Book to PM
  if (SendConnectBookToPM(ECNIndex) == SUCCESS)
  {
    //Update TT event log: TT request connect to a ECN book on PM
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    sprintf(activityDetail, "%d,0,75%d", traderToolsInfo.connection[ttIndex].currentState, ECNIndex + 1);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_BOOK, activityDetail);
  } 

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForDisconnectAPMBookFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForDisconnectAPMBookFromTT(void *message, int ttIndex)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // ECN Book Index (4 bytes)
  int ECNIndex = GetIntNumber(&ttMessage->msgContent[currentIndex]);
  ECNIndex = TT_TO_PM_BOOK_INDEX_MAPPING[ECNIndex];

  TraceLog(DEBUG_LEVEL, "Received message to request PM disconnect to a ECN Book, ECNIndex = %d\n", ECNIndex);

  // Call function to disconnect to a ECN Book to PM
  if (SendDisconnectBookToPM(ECNIndex) == SUCCESS)
  {
    //Update TT event log: TT request disconnect to a ECN book on PM
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    sprintf(activityDetail, "%d,1,75%d", traderToolsInfo.connection[ttIndex].currentState, ECNIndex + 1);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_BOOK, activityDetail);
  } 
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForConnectAllPMBookFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForConnectAllPMBookFromTT(void *message, int ttIndex)
{
  TraceLog(DEBUG_LEVEL, "Received message to request PM connect to all ECN Book\n");

  // Call function to request PM connect to all ECN Books
  if (ProcessPMConnectAllECNBook() == SUCCESS)
  {
    // Update TT event log for the event: TT request PM connect to all ECN Books
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    sprintf(activityDetail, "%d,0,750", traderToolsInfo.connection[ttIndex].currentState);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_BOOK, activityDetail);
  } 
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForDisconnectAllPMBookFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForDisconnectAllPMBookFromTT(void *message, int ttIndex)
{
  TraceLog(DEBUG_LEVEL, "Received message to request PM disconnect to all ECN Book\n");

  // Call function to request PM disconnect to all ECN Books
  if (ProcessPMDisconnectAllECNBook() == SUCCESS)
  {
    // Update TT event log for the event: TT request PM disconnect to all ECN Books
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    sprintf(activityDetail, "%d,1,750", traderToolsInfo.connection[ttIndex].currentState);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_BOOK, activityDetail);
  } 
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForGetPMSymbolStatusFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForGetPMSymbolStatusFromTT(void *message, int ttIndex)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  char symbol[SYMBOL_LEN] = "\0";
  memcpy(symbol, &ttMessage->msgContent[currentIndex], SYMBOL_LEN);
  TraceLog(DEBUG_LEVEL, "Received message to request get stock symbol status to PM, symbol = %.8s\n", symbol);

  // Call function to get stock symbol status to PM
  char listMdc[PM_MAX_BOOK_CONNECTIONS];
  memset(listMdc, 1, PM_MAX_BOOK_CONNECTIONS);
  
  SendGetSymbolStatusToPM(ttIndex, (unsigned char *)symbol, listMdc);

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForSetPMARCABookFeed
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForSetPMARCABookFeed(void *message)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;
  
  // Update current index
  currentIndex += 4;

  // Block content = Feedname (A or B, 1 byte char)
  
  char newFeedName = ttMessage->msgContent[currentIndex];

  TraceLog(DEBUG_LEVEL, "[PM]: Received request to switch ARCA Book to feed %c\n", newFeedName);

  SetBookARCAFeedToPM(newFeedName);

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForRerequestEtbList
****************************************************************************/
int ProcessTTMessageForRerequestEtbList()
{
  TraceLog(DEBUG_LEVEL, "Received request to re-request ETB list from TT\n");
  pthread_t requestEtbListThread;
  pthread_create(&requestEtbListThread, NULL, (void *)RequestEtbListThread, NULL);
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendPMSymbolStatusToTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendPMSymbolStatusToTT(int ttIndex, void *message)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];

  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  t_TTMessage ttMessage;
  
  // Build PM stock symbol status message to send Trader Tool
  t_TTMessage *pmMessage = (t_TTMessage *)message;
  BuildTTMessageForPMSymbolStatus(&ttMessage, pmMessage);
  
  // Send message to Trader Tool
  return SendTTMessage(connection, &ttMessage);
}

/****************************************************************************
- Function name:  BuildTTMessageForPMSymbolStatus
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTTMessageForPMSymbolStatus(void *message, void *messageSrc)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *pmMessage = (t_TSMessage *)messageSrc;
  t_TTMessage *ttMessage = (t_TTMessage *)message;
  
  // Default value at begin index of symbol
  int pmMsgIndex = 11; // change ttIndex from 4 bytes to 1 byte
  
  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_PM_SYMBOL_STATUS_TO_TT;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1];
  ttMessage->msgLen += 2;

  // Update index of body length
  ttMessage->msgLen += 4;

  // Message body = 3 block (block sender + block ASK + block BID)
  // Block sender connect = symbol (8 bytes)
  
  // Update index of block length of message
  ttMessage->msgLen += 4;
  
  // Symbol (8 bytes)
  strncpy((char *) &ttMessage->msgContent[ttMessage->msgLen], (char *) &pmMessage->msgContent[pmMsgIndex], SYMBOL_LEN);
  ttMessage->msgLen += SYMBOL_LEN;
  pmMsgIndex += SYMBOL_LEN;

  // Block length field
  intConvert.value = 4 + 8;
  ttMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  ttMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  ttMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  ttMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  /****************************************************************************************************
  Handle ASK Block Data
  ****************************************************************************************************/
  // ASK block len
  int blockASKLen = GetIntNumber(&pmMessage->msgContent[pmMsgIndex]);
  pmMsgIndex += 4;
    
  // Block length field
  intConvert.value = blockASKLen;
  ttMessage->msgContent[ttMessage->msgLen] = intConvert.c[0];
  ttMessage->msgContent[ttMessage->msgLen + 1] = intConvert.c[1];
  ttMessage->msgContent[ttMessage->msgLen + 2] = intConvert.c[2];
  ttMessage->msgContent[ttMessage->msgLen + 3] = intConvert.c[3];
  ttMessage->msgLen += 4;

  // ASK Block contents: (number of quote) * (ecn(1) + share(4) + price(8))
  int i;
  blockASKLen = (blockASKLen - 4) / 13; //number of quotes
  for (i = 0; i < blockASKLen; i++)
  {
    ttMessage->msgContent[ttMessage->msgLen] = PM_TO_TT_BOOK_INDEX_MAPPING[pmMessage->msgContent[pmMsgIndex]];
    ttMessage->msgLen++;
    pmMsgIndex++;
    
    memcpy(&ttMessage->msgContent[ttMessage->msgLen], &pmMessage->msgContent[pmMsgIndex], 4);
    ttMessage->msgLen += 4;
    pmMsgIndex += 4;
    
    memcpy(&ttMessage->msgContent[ttMessage->msgLen], &pmMessage->msgContent[pmMsgIndex], 8);
    ttMessage->msgLen += 8;
    pmMsgIndex += 8;
  }
  
  /****************************************************************************************************
  Handle BID Block Data
  ****************************************************************************************************/
  // BID Block len
  int blockBIDLen = GetIntNumber(&pmMessage->msgContent[pmMsgIndex]);
  pmMsgIndex += 4;
  
  // Block length field
  intConvert.value = blockBIDLen;
  ttMessage->msgContent[ttMessage->msgLen] = intConvert.c[0];
  ttMessage->msgContent[ttMessage->msgLen + 1] = intConvert.c[1];
  ttMessage->msgContent[ttMessage->msgLen + 2] = intConvert.c[2];
  ttMessage->msgContent[ttMessage->msgLen + 3] = intConvert.c[3];
  ttMessage->msgLen += 4;

  // BID Block contents: (number of quote) * (ecn(1) + share(4) + price(8))
  blockBIDLen = (blockBIDLen - 4) / 13; //number of quotes
  for (i = 0; i < blockBIDLen; i++)
  {
    ttMessage->msgContent[ttMessage->msgLen] = PM_TO_TT_BOOK_INDEX_MAPPING[pmMessage->msgContent[pmMsgIndex]];
    ttMessage->msgLen++;
    pmMsgIndex++;
    
    memcpy(&ttMessage->msgContent[ttMessage->msgLen], &pmMessage->msgContent[pmMsgIndex], 4);
    ttMessage->msgLen += 4;
    pmMsgIndex += 4;
    
    memcpy(&ttMessage->msgContent[ttMessage->msgLen], &pmMessage->msgContent[pmMsgIndex], 8);
    ttMessage->msgLen += 8;
    pmMsgIndex += 8;
  }
    
  // Body length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[2] = intConvert.c[0];
  ttMessage->msgContent[3] = intConvert.c[1];
  ttMessage->msgContent[4] = intConvert.c[2];
  ttMessage->msgContent[5] = intConvert.c[3];

  return ttMessage->msgLen;
}

/****************************************************************************
- Function name:  ProcessTTMessageForSendDisengageSymbolFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForSendDisengageSymbolFromTT(void *message, int ttIndex)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // symbol
  char symbol[SYMBOL_LEN];
  
  strncpy(symbol, (char *)&ttMessage->msgContent[currentIndex], SYMBOL_LEN);
  currentIndex += SYMBOL_LEN;
  
  int account = ttMessage->msgContent[currentIndex];

  TraceLog(DEBUG_LEVEL, "Received request PM to disengage symbol: %.8s, account: %d...\n", symbol, account);

  //Check for verify position info
  int symbolIndex = GetStockSymbolIndex(symbol);
  
  // Call function to send disengage symbol to PM
  if (SendDisengageSymbolToPM(symbol, account) == SUCCESS)
  {
    if (symbolIndex != -1)
    {
      //Remove this symbol from verify position list
      verifyPositionInfo[account][symbolIndex].isTrading = TRADING_NONE;
      verifyPositionInfo[account][symbolIndex].isPMOwner = NO;
      verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
      
      // Check if symbol is halted, if halted, then check to change the $isAutoResume
      pthread_mutex_lock(&HaltStatusMgmt.mutexLock);
      if ((HaltStatusMgmt.haltStatus[symbolIndex][TS_NASDAQ_BOOK_INDEX] +
        HaltStatusMgmt.haltStatus[symbolIndex][TS_ARCA_BOOK_INDEX]) != TRADING_NORMAL)  //Symbol is halted on ARCA or NASDAQ
      {
        if (HaltStatusMgmt.isAutoResume[symbolIndex] != 2)
        {
          // We need to send to PM and TT only, TS does not care about isAutoResume value
          memcpy(HaltStatusMgmt.symbol[symbolIndex], symbol, SYMBOL_LEN);
          HaltStatusMgmt.isAutoResume[symbolIndex] = 2;
          SaveHaltedStockListToFile();
          SendAHaltStockToPM(symbolIndex);
          SendAHaltStockToAllDaedalus(symbolIndex);
          SendAHaltStockToAllTT(symbolIndex);
        }
      }
      pthread_mutex_unlock(&HaltStatusMgmt.mutexLock);
    }
    
    //Update disengage event log to database
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    int action = 1;//Disengage symbol
    sprintf(activityDetail, "%d,%d,%.8s", traderToolsInfo.connection[ttIndex].currentState, action, symbol);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_PM_ENGAGE_DISENGAGE, activityDetail);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForSendEngageStuckFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForSendEngageStuckFromTT(void *message, int ttIndex)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Block content = SYMBOL(8 bytes) + NUMBER OF SHARES (4 bytes) + STUCK PRICE (8 bytes) + SIDE (4 byte)
  
  // SYMBOL(8 bytes)
  char symbol[SYMBOL_LEN];
  strncpy(symbol, (char *) &ttMessage->msgContent[currentIndex], SYMBOL_LEN);
  currentIndex += SYMBOL_LEN;

  // NUMBER OF SHARES (4 bytes)
  int shares = GetIntNumber(&ttMessage->msgContent[currentIndex]);
  currentIndex += 4;

  // STUCK PRICE (8 bytes)
  double price = GetDoubleNumber(&ttMessage->msgContent[currentIndex]);
  currentIndex += 8;

  // SIDE (4 bytes)
  int side = GetIntNumber(&ttMessage->msgContent[currentIndex]);
  currentIndex += 4;
  
  // account
  int account = ttMessage->msgContent[currentIndex++];

  // isFilter (4 bytes)
  int isFilter = GetIntNumber(&ttMessage->msgContent[currentIndex]);
  currentIndex += 4;

  TraceLog(DEBUG_LEVEL, "ENGAGING PM: Received stuck from TT, symbol: %.8s, side: %d, shares: %d, price: %lf, account: %d, use filter: %d\n", symbol, side, shares, price, account, isFilter);

  //TraceLog(DEBUG_LEVEL, "Begin send this stuck to PM ...\n");

  //Check for engaging stuck 
  int symbolIndex = GetStockSymbolIndex(symbol);
  if (symbolIndex != -1)
  {
    TraceLog(DEBUG_LEVEL, "Verify with Position List (account: %d, symbol: %.8s, shares: %d, side: %d, isTrading: %d)\n", account, symbol, verifyPositionInfo[account][symbolIndex].shares, verifyPositionInfo[account][symbolIndex].side, verifyPositionInfo[account][symbolIndex].isTrading);

    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    int action = 0;   //Engage symbol
    sprintf(activityDetail, "%d,%d,%.8s", traderToolsInfo.connection[ttIndex].currentState, action, symbol);
  
    int lastTradingStatus = verifyPositionInfo[account][symbolIndex].isTrading;
    
    if (isFilter == 0)  //isFilter = 0 --> Engaging without filter, isFilter = 1 --> Use filter
    {
      verifyPositionInfo[account][symbolIndex].isTrading = TRADING_AUTOMATIC;
      verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
      
      t_TSMessage pmMessage;
      BuildPMMessageForSendStuckInfo(&pmMessage, symbol, shares, price, side, 0, account);
      
      if (SendPMMessage(&pmMessage) == SUCCESS)
      {
        //Update to verify position list for this symbol
        verifyPositionInfo[account][symbolIndex].shares = shares;
        verifyPositionInfo[account][symbolIndex].side = side;
        
        //Send a message to notify that stuck is sent to PM
        SendMessageForNotifyNoMatchingPositionToTT(ttIndex, symbol, shares, side, REQUESTED_SUCCESSFULLY);

        TraceLog(DEBUG_LEVEL, "Requested successfully, account: %d, symbol: %.8s, shares: %d, side: %d, use filter: %d\n", account, symbol, shares, side, isFilter);

        if (side == BID_SIDE)
        {
          verifyPositionInfo[account][symbolIndex].lockCounter = SHORT_LOCK;
          SendSymbolLockCounterForASymbolToTradeServers(symbol, account, SHORT_LOCK);
        }
        else
        {
          verifyPositionInfo[account][symbolIndex].lockCounter = LONG_LOCK;
          SendSymbolLockCounterForASymbolToTradeServers(symbol, account, LONG_LOCK);
        }
        
        //Update engage event log to database
        ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_PM_ENGAGE_DISENGAGE, activityDetail);   
      }
      else
      {
        verifyPositionInfo[account][symbolIndex].isTrading = lastTradingStatus;
        verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
        
        //Send message no matching position to TT
        SendMessageForNotifyNoMatchingPositionToTT(ttIndex, symbol, shares, side, NO_MATCHING_POSITION);

        TraceLog(ERROR_LEVEL, "Cound not engage position information to PM\n");
      }
    }
    else
    {
      int willSend = 1;

      if (lastTradingStatus != TRADING_NONE)
      {
        willSend = 0;
      }

      verifyPositionInfo[account][symbolIndex].isTrading = TRADING_AUTOMATIC;
      verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
      
      if (willSend == 0)
      {
        verifyPositionInfo[account][symbolIndex].isTrading = lastTradingStatus;
        verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
        
        //stuck is being traded, so do not send to PM
        char msg[1024];
        if (lastTradingStatus == TRADING_AUTOMATIC &&
          verifyPositionInfo[account][symbolIndex].isSleeping == YES)
        {
          sprintf(msg, "Could not engage (%.8s, side %d, shares: %d, price %lf, account %d), because there is a stuck of same symbol trading on PM that the inside spread is too wide. Please wait for it to be finished.",
              symbol, side, shares, price, account);
        }
        else
        {
          sprintf(msg, "Could not engage (%.8s, side %d, shares: %d, price %lf, account %d), because there is a stuck of same symbol trading on AS/PM, please wait for it to be finished.",
              symbol, side, shares, price, account);
        }
        SendNotifyMessageToTT(ttIndex, msg, 0);

        TraceLog(ERROR_LEVEL, "%s\n", msg);
      }
      else
      {
        if ((side == verifyPositionInfo[account][symbolIndex].side) && (shares == verifyPositionInfo[account][symbolIndex].shares))
        {
          // Build and send message to PM
          t_TSMessage pmMessage;
          BuildPMMessageForSendStuckInfo(&pmMessage, symbol, shares, price, side, 0, account);
        
          if (SendPMMessage(&pmMessage) == SUCCESS)
          {
            verifyPositionInfo[account][symbolIndex].isSleeping = NO;
            
            //Send a message to notify that stuck is sent to PM
            SendMessageForNotifyNoMatchingPositionToTT(ttIndex, symbol, shares, side, REQUESTED_SUCCESSFULLY);
          
            TraceLog(DEBUG_LEVEL, "Requested successfully, account: %d, symbol: %.8s, shares: %d, side: %d, use filter: %d\n", account, symbol, shares, side, isFilter);
            
            //Update engage event log to database
            ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_PM_ENGAGE_DISENGAGE, activityDetail);
          }
          else
          {
            verifyPositionInfo[account][symbolIndex].isTrading = lastTradingStatus;
            verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
            
            SendMessageForNotifyNoMatchingPositionToTT(ttIndex, symbol, shares, side, NO_MATCHING_POSITION);
            TraceLog(ERROR_LEVEL, "Position information matched but could not engage to PM\n");
          }
        }
        else
        {
          verifyPositionInfo[account][symbolIndex].isTrading = lastTradingStatus;
          verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
          
          //Send message no matching position to TT
          SendMessageForNotifyNoMatchingPositionToTT(ttIndex, symbol, shares, side, NO_MATCHING_POSITION);
        
          TraceLog(ERROR_LEVEL, "No Matching Positions, account: %d, symbol: %.8s, shares: %d, side: %d, use filter: %d\n", account, symbol, shares, side, isFilter);
        }
      }
    }
  
    /* If symbol in currently halted, when user engage it, the $isAutoResume will be 1*/
    pthread_mutex_lock(&HaltStatusMgmt.mutexLock);
      if ((HaltStatusMgmt.haltStatus[symbolIndex][TS_NASDAQ_BOOK_INDEX] +
        HaltStatusMgmt.haltStatus[symbolIndex][TS_ARCA_BOOK_INDEX]) != TRADING_NORMAL)  //Symbol is halted on ARCA or NASDAQ
      {
        if (HaltStatusMgmt.isAutoResume[symbolIndex] != 1)
        {
          // We need to send to PM and TT only, TS does not care about isAutoResume value
          memcpy(HaltStatusMgmt.symbol[symbolIndex], symbol, SYMBOL_LEN);
          HaltStatusMgmt.isAutoResume[symbolIndex] = 1;
          SaveHaltedStockListToFile();
          SendAHaltStockToPM(symbolIndex);
          SendAHaltStockToAllDaedalus(symbolIndex);
          SendAHaltStockToAllTT(symbolIndex);
        }
      }
    pthread_mutex_unlock(&HaltStatusMgmt.mutexLock);
  }

  return SUCCESS;
}


/****************************************************************************
- Function name:  SendMessageForNotifyNoMatchingPositionToTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendMessageForNotifyNoMatchingPositionToTT(int ttIndex, const char *symbol, int shares, int side, int isSent)
{
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;

  // Build AS setting message to send Trader Tool
  BuildTTMessageForNotifyNoMatchingPosition(&ttMessage, symbol, shares, side, isSent);

  // Send message to all Trader Tool connected to AS
  
  connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];
  
  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return SUCCESS;
  }

  // Send message to Trader Tool
  return SendTTMessage(connection, &ttMessage);
}

/****************************************************************************
- Function name:  BuildTTMessageForNotifyNoMatchingPosition
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTTMessageForNotifyNoMatchingPosition(void *message, const char *symbol, int shares, int side, int isSent)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TTMessage *ttMessage = (t_TTMessage *)message;
  
  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_MESSAGE_NOTIFY_NO_MATCHING_POSITION_TO_TT;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1];
  ttMessage->msgLen += 2;

  // Update index of body length
  ttMessage->msgLen += 4;

  // Message body 
  // Update index of block length
  ttMessage->msgLen += 4;

  // Block content = symbol(8 bytes) + shares(4 bytes) + side(1 byte))  
  
  // symbol
  strncpy((char *) &ttMessage->msgContent[ttMessage->msgLen], symbol, SYMBOL_LEN);
  ttMessage->msgLen += SYMBOL_LEN;
  
  // shares
  intConvert.value = shares;
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], intConvert.c, 4);
  ttMessage->msgLen += 4;
  
  //side
  ttMessage->msgContent[ttMessage->msgLen] = side;
  ttMessage->msgLen++;
  
  //isSent
  ttMessage->msgContent[ttMessage->msgLen] = isSent;
  ttMessage->msgLen++;
  
  // Block length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  ttMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  ttMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  ttMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[2] = intConvert.c[0];
  ttMessage->msgContent[3] = intConvert.c[1];
  ttMessage->msgContent[4] = intConvert.c[2];
  ttMessage->msgContent[5] = intConvert.c[3];

  return ttMessage->msgLen;
}

/****************************************************************************
- Function name:  ProcessTTMessageForSetPMBookConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForSetPMBookConf(void *message)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Block content = ECN Book Index (4 bytes) + ECN Book conf
  // ECN Book Index (4 bytes)
  int ECNIndex = GetIntNumber(&ttMessage->msgContent[currentIndex]);
  ECNIndex = TT_TO_PM_BOOK_INDEX_MAPPING[ECNIndex];

  TraceLog(DEBUG_LEVEL, "Set PM Book conf, ECNIndex = %d\n", ECNIndex);

  // Update current index
  currentIndex += 4;

  int type;

  // ECN Book configuration
  switch (ECNIndex)
  {
    // ARCA Book configuration
    case PM_ARCA_BOOK_INDEX:
      type = GetIntNumber(&ttMessage->msgContent[currentIndex]);
      currentIndex += 4;

      if (type == UPDATE_SOURCE_ID)
      {
        memcpy(pmInfo.ECNBooksConf.ARCABookConf.sourceID, &ttMessage->msgContent[currentIndex], MAX_LINE_LEN);

        TraceLog(DEBUG_LEVEL, "Set ARCABookConf: source ID = %s\n", pmInfo.ECNBooksConf.ARCABookConf.sourceID);

        // Call function set ARCA Book configuration to PM
        SetPMBookARCAConfToPM(type, -1);
      }
      else if (type == UPDATE_RETRANSMISSION_REQUEST)
      {
        int groupIndex = GetIntNumber(&ttMessage->msgContent[currentIndex]);
        currentIndex += 4;

        // Retransmission Request TCP/IP
        memcpy(pmInfo.ECNBooksConf.ARCABookConf.retranConn[groupIndex].ip, &ttMessage->msgContent[currentIndex], MAX_LINE_LEN);
        currentIndex += MAX_LINE_LEN;

        pmInfo.ECNBooksConf.ARCABookConf.retranConn[groupIndex].port = GetIntNumber(&ttMessage->msgContent[currentIndex]);
        currentIndex += 4;

        TraceLog(DEBUG_LEVEL, "Set ARCABookConf: groupIndex = %d, ip = %s, port = %d\n", groupIndex, pmInfo.ECNBooksConf.ARCABookConf.retranConn[groupIndex].ip, pmInfo.ECNBooksConf.ARCABookConf.retranConn[groupIndex].port);

        // Call function set ARCA Book configuration to PM
        SetPMBookARCAConfToPM(type, groupIndex);
      }
      else
      {
        TraceLog(ERROR_LEVEL, "Set ARCA FAST Book config to TS, type = %d\n", type);

        return ERROR;
      }

      return SUCCESS;
      break;
    
    // NASDAQ Book configuration
    case PM_NASDAQ_BOOK_INDEX:
      memcpy(pmInfo.ECNBooksConf.NASDAQBookConf.UDP.ip, &ttMessage->msgContent[currentIndex], MAX_LINE_LEN);
      currentIndex += MAX_LINE_LEN;

      memcpy(&pmInfo.ECNBooksConf.NASDAQBookConf.UDP.port, &ttMessage->msgContent[currentIndex], 4);
      currentIndex += 4;

      memcpy(pmInfo.ECNBooksConf.NASDAQBookConf.TCP.ip, &ttMessage->msgContent[currentIndex], MAX_LINE_LEN);
      currentIndex += MAX_LINE_LEN;

      memcpy(&pmInfo.ECNBooksConf.NASDAQBookConf.TCP.port, &ttMessage->msgContent[currentIndex], 4);
      currentIndex += 4;
      break;
    
    default:
      TraceLog(ERROR_LEVEL, "Invalid set Book configuration message from TT, ECNIndex = %d\n", ECNIndex);
      return ERROR;
      break;
  }

  // Call function set Order configuration to PM
  SetPMBookConfToPM(ECNIndex);

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForConnectCTSFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForConnectCTSFromTT(void *message, int ttIndex)
{
  TraceLog(DEBUG_LEVEL, "Received message to request PM connect to CTS Feed\n");

  // Call function to connect to request connect to CTS Feed to PM
  if (SendConnectCTS_FeedToPM() == SUCCESS)
  {
    //Update TT event log: TT request connect to CTS on PM
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    sprintf(activityDetail, "%d,0,81", traderToolsInfo.connection[ttIndex].currentState);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_BBO, activityDetail);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForConnectCQSFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForConnectCQSFromTT(void *message, int ttIndex)
{
  TraceLog(DEBUG_LEVEL, "Received message to request PM connect to CQS Feed\n");

  // Call function to connect to request connect to CQS Feed to PM
  if (SendConnectCQS_FeedToPM() == SUCCESS)
  {
    //Update TT event log: TT request connect to CQS on PM
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    sprintf(activityDetail, "%d,0,83", traderToolsInfo.connection[ttIndex].currentState);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_BBO, activityDetail);
  }
  
  return SUCCESS;
} //end

/****************************************************************************
- Function name:  ProcessTTMessageForConnectUQDF_FromTT
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessTTMessageForConnectUQDF_FromTT(void *message, int ttIndex)
{
  TraceLog(DEBUG_LEVEL, "Received message to request PM connect to UQDF\n");

  // Call function to connect UQDF Feed to PM
  if (SendConnectUQDF_FeedToPM() == SUCCESS)
  {
    //Update TT event log: TT request connect to UQDF on PM
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    sprintf(activityDetail, "%d,0,84", traderToolsInfo.connection[ttIndex].currentState);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_BBO, activityDetail);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForDisconnectCTSFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForDisconnectCTSFromTT(void *message, int ttIndex)
{
  TraceLog(DEBUG_LEVEL, "Received message to request PM disconnect to CTS Feed\n");

  // Call function to disconnect to CTS Feed to PM
  if (SendDisconnectCTS_FeedToPM() == SUCCESS)
  {
    //Update TT event log: TT request disconnect to CTS on PM
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    sprintf(activityDetail, "%d,1,81", traderToolsInfo.connection[ttIndex].currentState);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_BBO, activityDetail);
  } 

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForDisconnectCQSFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForDisconnectCQSFromTT(void *message, int ttIndex)
{
  TraceLog(DEBUG_LEVEL, "Received message to request PM disconnect to CQS Feed\n");

  // Call function to disconnect to CQS Feed to PM
  if (SendDisconnectCQS_FeedToPM() == SUCCESS)
  {
    //Update TT event log: TT request disconnect to CQS on PM
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    sprintf(activityDetail, "%d,1,83", traderToolsInfo.connection[ttIndex].currentState);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_BBO, activityDetail);
  } 

  return SUCCESS;
}//end

/****************************************************************************
- Function name:  ProcessTTMessageForDisconnectUQDF_FromTT
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessTTMessageForDisconnectUQDF_FromTT(void *message, int ttIndex)
{
  TraceLog(DEBUG_LEVEL, "Received message to request PM disconnect to UQDF Feed\n");

  // Call function to disconnect to UQDF Feed to PM
  if (SendDisconnectUQDF_FeedToPM() == SUCCESS)
  {
    //Update TT event log: TT request disconnect to UQDF on PM
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    sprintf(activityDetail, "%d,1,84", traderToolsInfo.connection[ttIndex].currentState);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_BBO, activityDetail);
  } 

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForConnectUTDFFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForConnectUTDFFromTT(void *message, int ttIndex)
{
  TraceLog(DEBUG_LEVEL, "Received message to request PM connect to UTDF\n");

  // Call function to connect to request connect to UTDF to PM
  if (SendConnectUTDFToPM() == SUCCESS)
  {
    //Update TT event log: TT request connect to UTDF on PM
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    sprintf(activityDetail, "%d,0,82", traderToolsInfo.connection[ttIndex].currentState);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_BBO, activityDetail);
  } 
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForDisconnectUTDFFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForDisconnectUTDFFromTT(void *message, int ttIndex)
{
  TraceLog(DEBUG_LEVEL, "Received message to request PM disconnect to UTDF\n");

  // Call function to disconnect to UTDF to PM
  if (SendDisconnectUTDFToPM() == SUCCESS)
  {
    //Update TT event log: TT request disconnect to UTDF on PM
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    sprintf(activityDetail, "%d,1,82", traderToolsInfo.connection[ttIndex].currentState);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_BBO, activityDetail);
  } 

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendAlertConfigToAllTT
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int SendAlertConfigToAllTT(void)
{
  t_TTMessage ttMessage;

  BuildTTMessageForAlertConfig(&ttMessage);

  return SendMessageToAllTT(&ttMessage);
}

/****************************************************************************
- Function name:  SendMessageAllTT
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int SendMessageToAllTT(t_TTMessage *ttMessage)
{
  int ttIndex;

  t_ConnectionStatus *connection;

  // Send message to all Trader Tool connected to AS
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];

    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_TRADER_TOOL)
    {
      continue;
    }

    // Send message to Trader Tool
    SendTTMessage(connection, ttMessage);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  BuildNSendNewActiveTraderAlertToAllTT
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildNSendNewActiveTraderAlertToAllTT(char * trader)
{
  char alert[128] = "\0";
  sprintf(alert, "%d\t%s", NEW_ACTIVE_TRADER_ALERT_MESSAGE_TYPE, trader);

  return BuildNSendAlertToAllTT(alert);
}

/****************************************************************************
- Function name:  BuildNSendAlertToAllTT
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildNSendAlertToAllTT(char *alert)
{ 
  t_TTMessage ttMessage;

  int temp;
  // Initialize message
  ttMessage.msgLen = 0;

  // Message type
  temp = MSG_TYPE_FOR_SEND_ALERTING_TO_TT;
  memcpy(&ttMessage.msgContent[0], &temp, 2);
  ttMessage.msgLen += 2;

  // Update index of body length
  ttMessage.msgLen += 4;

  // Message body = n * (blockLength (4 bytes) + blockContent (blockLength bytes))
  // Update index of block length
  ttMessage.msgLen += 4;

  // Block content = global conf

  strcpy((char *)&ttMessage.msgContent[ttMessage.msgLen], alert);

  temp = strlen(alert);

  ttMessage.msgLen += temp;

  // Block length field
  temp = temp + 4;

  memcpy(&ttMessage.msgContent[MSG_HEADER_LEN], &temp, 4);

  // Body length field
  temp = ttMessage.msgLen - MSG_HEADER_LEN;

  memcpy(&ttMessage.msgContent[2], &temp, 4);

  if (SendMessageToAllTT(&ttMessage) == SUCCESS)
  {
    TraceLog(DEBUG_LEVEL, "Send alert to all TTs: %s\n", alert);
    return SUCCESS;
  }
  
  return ERROR;
}

/****************************************************************************
- Function name:  BuildNSendAlertToTT
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildNSendAlertToTT(char *alert, int ttIndex)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];
  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }
  
  t_TTMessage ttMessage;

  int temp;
  // Initialize message
  ttMessage.msgLen = 0;

  // Message type
  temp = MSG_TYPE_FOR_SEND_ALERTING_TO_TT;
  memcpy(&ttMessage.msgContent[0], &temp, 2);
  ttMessage.msgLen += 2;

  // Update index of body length
  ttMessage.msgLen += 4;

  // Message body = n * (blockLength (4 bytes) + blockContent (blockLength bytes))
  // Update index of block length
  ttMessage.msgLen += 4;

  // Block content = global conf

  strcpy((char *)&ttMessage.msgContent[ttMessage.msgLen], alert);

  temp = strlen(alert);

  ttMessage.msgLen += temp;

  // Block length field
  temp = temp + 4;

  memcpy(&ttMessage.msgContent[MSG_HEADER_LEN], &temp, 4);

  // Body length field
  temp = ttMessage.msgLen - MSG_HEADER_LEN;

  memcpy(&ttMessage.msgContent[2], &temp, 4);

  return SendTTMessage(connection, &ttMessage);
}

/****************************************************************************
- Function name:  BuildNSendBuyingPowerDropsBelowThresholdAlertToAllTT
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildNSendBuyingPowerDropsBelowThresholdAlertToAllTT(char *server, double amount)
{
  char alert[128];
  sprintf(alert, "%d\t%s\t%lf", BUYING_POWER_ALERT_MESSAGE_TYPE, server, amount);

  return BuildNSendAlertToAllTT(alert);
}
/****************************************************************************
- Function name:  BuildNSendNoActiveTraderAlertToAllTT
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildNSendNoActiveTraderAlertToAllTT()
{
  char alert[128];
  sprintf(alert, "%d", NO_ACTIVE_TRADER_ALERT_MESSAGE_TYPE);

  return BuildNSendAlertToAllTT(alert);
}

/****************************************************************************
- Function name:  BuildNSendOrderRejectedAlertToAllTT
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildNSendOrderRejectedAlertToAllTT(int oid)
{
  char alert[128];
  sprintf(alert, "%d\t%d", ORDER_REJECTED_ALERT_MESSAGE_TYPE, oid);

  return BuildNSendAlertToAllTT(alert);
}

/****************************************************************************
- Function name:  BuildNSendPMOrderRateAlertToAllTT
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildNSendPMOrderRateAlertToAllTT(int number)
{
  char alert[128];
  sprintf(alert, "%d\t%d", PM_ORDER_RATE_HIGHER_THAN_CONFIGURATION_PER_SECOND_ALERT_MESSAGE_TYPE, number);

  return BuildNSendAlertToAllTT(alert);
}

/****************************************************************************
- Function name:  BuildNSendInCompleteAlertToAllTT
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildNSendInCompleteTradeAlertToAllTT(char *orderIdList)
{
  char alert[10240];
  sprintf(alert, "%d\t%s", INCOMPLETE_TRADE_ALERT_MESSAGE_TYPE, orderIdList);

  return BuildNSendAlertToAllTT(alert);
}

/****************************************************************************
- Function name:  BuildNSendNoTradesDuringConfiguredTimePeriodAlertToAllTT
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildNSendNoTradesDuringConfiguredTimePeriodAlertToAllTT(int serverID, int munites, char *startTimeStamp, char *endTimeStamp)
{
  char alert[128];
  sprintf(alert, "%d\t%d\t%d\t%s\t%s", NO_TRADES_ALERT_MESSAGE_TYPE, serverID, munites, startTimeStamp, endTimeStamp);

  return BuildNSendAlertToAllTT(alert);
}

/****************************************************************************
- Function name:  BuildNSendGTRAlertToAllTT
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildNSendGTRAlertToAllTT(double amount)
{
  char alert[128];
  // Format: amount - start time - end time
  sprintf(alert, "%d\t%lf", GTR_CHANGE_ALERT_MESSAGE_TYPE, amount);

  return BuildNSendAlertToAllTT(alert);
}

/****************************************************************************
- Function name:  BuildNSendServerRoleConfigurationMisConfiguredAlertToAllTT
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildNSendServerRoleConfigurationMisConfiguredAlertToAllTT(char * servers)
{
  char alert[128] = "\0";
  sprintf(alert, "%d\t%s", SERVER_ROLE_CONFIGURATION_ERROR_ALERT_MESSAGE_TYPE, servers);

  return BuildNSendAlertToAllTT(alert);
}

/****************************************************************************
- Function name:  BuildNSendSingleServerTradesAlertToAllTT
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildNSendSingleServerTradesAlertToAllTT(char *server)
{
  char alert[128] = "\0";
  sprintf(alert, "%d\t%s", SINGLE_SERVER_TRADES_ALERT_MESSAGE_TYPE, server);

  return BuildNSendAlertToAllTT(alert);
}

/****************************************************************************
- Function name:  BuildNSendTradingDisabledAlertToAllTT
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildNSendTradingDisabledAlertToAllTT(char *reason)
{
  if (strlen(reason) == 0)
  {
    return SUCCESS;
  }
  
  char alert[128] = "\0";
  sprintf(alert, "%d\t%s", TRADING_DISABLED_ALERT_MESSAGE_TYPE, reason);

  return BuildNSendAlertToAllTT(alert);
}

/****************************************************************************
- Function name:  BuildNSendGroupfOfBookDataUnhandledAlertToAllTT
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildNSendGroupfOfBookDataUnhandledAlertToAllTT(char *reason)
{
  char alert[128];
  sprintf(alert, "%d\t%s", GROUP_OF_BOOK_DATA_UNHANDLED_ALERT_MESSAGE_TYPE, reason);

  return BuildNSendAlertToAllTT(alert);
}

/****************************************************************************
- Function name:  BuildNSendDisconnectAlertToAllTT
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildNSendDisconnectAlertToAllTT(char *service)
{
  if (strlen(service) == 0)
  {
    return SUCCESS;
  }
  
  char alert[128] = "\0";
  sprintf(alert, "%d\t%s", DISCONNECT_ALERT_MESSAGE_TYPE, service);

  return BuildNSendAlertToAllTT(alert);
}

/****************************************************************************
- Function name:  BuildNSendStuckNearingLimitAlertToAllTT
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildNSendStuckNearingLimitAlertToAllTT()
{
  char alert[128];
  sprintf(alert, "%d", NEARING_STUCK_LIMIT_ALERT_MESSAGE_TYPE);

  return BuildNSendAlertToAllTT(alert);
}

/****************************************************************************
- Function name:  BuildNSendConsecutiveLossNearingLimitAlertToAllTT
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildNSendConsecutiveLossNearingLimitAlertToAllTT()
{
  char alert[128];
  sprintf(alert, "%d", NEARING_CONSECUTIVE_LOSS_LIMIT_ALERT_MESSAGE_TYPE);

  return BuildNSendAlertToAllTT(alert);
}

/****************************************************************************
- Function name:  BuildNSendPMReleasePositionAlertToAllTT
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildNSendPMReleasePositionAlertToAllTT(char *symbol, double price, int share, char side, int account)
{
  char alert[128];
  sprintf(alert, "%d\t%s\t%lf\t%d\t%d\t%d", PM_RELEASE_POSITION_ALERT_MESSAGE_TYPE, symbol, price, share, side, account);

  return BuildNSendAlertToAllTT(alert);
}

/****************************************************************************
- Function name:  BuildNSendUnhandledOpenPositionWithoutAnOrderAlertToTT
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildNSendUnhandledOpenPositionWithoutAnOrderAlertToTT(char *symbol, int share[MAX_ACCOUNT], char side[MAX_ACCOUNT], int account[MAX_ACCOUNT], int count)
{
  char alert[256] = "\0", tmp[80] = "\0";
  int i;
  
  sprintf(alert, "%d", POSITION_WITH_NO_ORDER_ALERT_MESSAGE_TYPE);
  
  for (i = 0; i < count; i++)
  {
    sprintf(tmp, "\t%s\t%d\t%d\t%d", symbol, share[i], side[i], account[i]);
    
    strcat(alert, tmp);
  }

  return BuildNSendAlertToAllTT(alert);
}

/****************************************************************************
- Function name:  BuildNSendPMHasntExitPositionAlertToAllTT
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildNSendPMHasntExitPositionAlertToAllTT(char *symbol, double price, int share, char side, int account)
{
  char alert[128];
  sprintf(alert, "%d\t%s\t%lf\t%d\t%d\t%d", PM_HASNT_EXIT_POSITION_ALERT_MESSAGE_TYPE, symbol, price, share, side, account);

  return BuildNSendAlertToAllTT(alert);
}

/****************************************************************************
- Function name:  BuildNSendPMDoesNotAcceptPositionAlertToAllTT
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildNSendPMDoesNotAcceptPositionAlertToAllTT(char *symbol, double price, int share, char side, int account)
{
  char alert[128];
  sprintf(alert, "%d\t%s\t%lf\t%d\t%d\t%d", PM_DOES_NOT_ACCEPT_POSITION_ALERT_MESSAGE_TYPE, symbol, price, share, side, account);

  return BuildNSendAlertToAllTT(alert);
}

/****************************************************************************
- Function name:  BuildNSendDisabledSymbolAlertToAllTT
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildNSendDisabledSymbolAlertToAllTT(char *reason)
{
  if (strlen(reason) == 0)
  {
    return SUCCESS;
  }

  char alert[180] = "\0";
  sprintf(alert, "%d\t%s", ADD_SYMBOL_TO_VOLATILE_OR_DISABLED_LIST, reason);

  return BuildNSendAlertToAllTT(alert);
}

/****************************************************************************
- Function name:  SendHaltStockListToAllTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendHaltStockListToAllTT()
{
  int ttIndex;
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;

  // Build PM halted symbol message to send Trader Tool
  if (BuildTTMessageForHaltedSymbolList(&ttMessage) == 0) return SUCCESS; //There is nothing to send

  // Send message to all Trader Tool connected to AS
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];

    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_TRADER_TOOL)
    {
      continue;
    }   

    // Send message to Trader Tool
    SendTTMessage(connection, &ttMessage);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendActiveHaltedStockListToAllTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendActiveHaltedStockListToAllTT(int updatedList[MAX_STOCK_SYMBOL], int updatedCount)
{
  int ttIndex;
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;

  // Build PM halted symbol message to send Trader Tool
  if (BuildTTMessageForActiveHaltedSymbolList(&ttMessage, updatedList, updatedCount) == 0) return SUCCESS;  //There is nothing to send

  // Send message to all Trader Tool connected to AS
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];

    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_TRADER_TOOL)
    {
      continue;
    }   

    // Send message to Trader Tool
    SendTTMessage(connection, &ttMessage);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendHaltStockListToTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendHaltStockListToTT(void *pConnection)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Send Halt stock list to TT ...\n");

  t_TTMessage ttMessage;

  // Build PM halted symbol message to send Trader Tool
  int count = BuildTTMessageForHaltedSymbolList(&ttMessage);
  if (count == 0) return SUCCESS; //There is nothing to send

  // Send message to Trader Tool
  return SendTTMessage(connection, &ttMessage);
}

/****************************************************************************
- Function name:  SendAHaltStockToAllTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendAHaltStockToAllTT(const int stockSymbolIndex)
{
  int ttIndex;
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;

  // Build PM halted symbol message to send Trader Tool
  if (BuildTTMessageForHaltSymbol(&ttMessage, stockSymbolIndex) == 0) return SUCCESS; //There is nothing to send

  // Send message to all Trader Tool connected to AS
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];

    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_TRADER_TOOL)
    {
      continue;
    }   

    // Send message to Trader Tool
    SendTTMessage(connection, &ttMessage);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  BuildTTMessageForHaltedSymbolList
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTTMessageForHaltedSymbolList(void *message)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TTMessage *ttMessage = (t_TTMessage *)message;

  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_HALTED_SYMBOL_LIST_TO_TT;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1];
  ttMessage->msgLen += 2;

  // Update index of body length
  ttMessage->msgLen += 4;

  // Update index of block length
  ttMessage->msgLen += 4;

  // Halted stock list
  int i, j, count = 0;
  int willSend;
  for (i = 0; i < RMList.countSymbol; i++)
  {
    willSend = 0;
    for (j = 0; j < TS_MAX_BOOK_CONNECTIONS; j++)
    {
      willSend += HaltStatusMgmt.haltStatus[i][j];
    }
    
    if (willSend)
    {
      count++;
      memcpy(&ttMessage->msgContent[ttMessage->msgLen], HaltStatusMgmt.symbol[i], SYMBOL_LEN);
      ttMessage->msgLen += SYMBOL_LEN;
      
      for (j = 0; j < TS_MAX_BOOK_CONNECTIONS; j++)
      {
        ttMessage->msgContent[ttMessage->msgLen] = HaltStatusMgmt.haltStatus[i][j];
        ttMessage->msgLen++;
      }

      ttMessage->msgContent[ttMessage->msgLen++] = HaltStatusMgmt.isAutoResume[i];
    }
  }
  
  // Block length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  ttMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  ttMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  ttMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[2] = intConvert.c[0];
  ttMessage->msgContent[3] = intConvert.c[1];
  ttMessage->msgContent[4] = intConvert.c[2];
  ttMessage->msgContent[5] = intConvert.c[3];

  return count;
}

/****************************************************************************
- Function name:  BuildTTMessageForActiveHaltedSymbolList
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTTMessageForActiveHaltedSymbolList(void *message, int updatedList[MAX_STOCK_SYMBOL], int updatedCount)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TTMessage *ttMessage = (t_TTMessage *)message;

  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_ACTIVATE_ALL_HALTED_STOCKS_TO_TT;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1];
  ttMessage->msgLen += 2;

  // Update index of body length
  ttMessage->msgLen += 4;

  // Update index of block length
  ttMessage->msgLen += 4;
  
  ttMessage->msgLen += 4;

  // Halted stock list
  int i, j, count = 0;
  int willSend;
  
  int stockSymbolIndex;
  for (i = 0; i < updatedCount; i++)
  {
    stockSymbolIndex = updatedList[i];
    willSend = 0;
    for (j = 0; j < TS_MAX_BOOK_CONNECTIONS; j++)
    {
      willSend += HaltStatusMgmt.haltStatus[stockSymbolIndex][j];
    }
    
    if (willSend == 0)
    {
      count++;
      memcpy(&ttMessage->msgContent[ttMessage->msgLen], HaltStatusMgmt.symbol[stockSymbolIndex], SYMBOL_LEN);
      ttMessage->msgLen += SYMBOL_LEN;
    }
  }
    
  memcpy(&ttMessage->msgContent[10], &count, 4);
  TraceLog(DEBUG_LEVEL, "Send activate all halted stocks to TT, number of symbol: %d\n", count);
  
  // Block length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  ttMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  ttMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  ttMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[2] = intConvert.c[0];
  ttMessage->msgContent[3] = intConvert.c[1];
  ttMessage->msgContent[4] = intConvert.c[2];
  ttMessage->msgContent[5] = intConvert.c[3];

  return count;
}

/****************************************************************************
- Function name:  BuildTTMessageForHaltSymbol
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTTMessageForHaltSymbol(void *message, const int stockSymbolIndex)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TTMessage *ttMessage = (t_TTMessage *)message;

  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_A_HALTED_SYMBOL_TO_TT;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1];
  ttMessage->msgLen += 2;

  // Update index of body length
  ttMessage->msgLen += 4;

  // Update index of block length
  ttMessage->msgLen += 4;

  int i;
  int willSend = 0;
  
  for (i = 0; i < TS_MAX_BOOK_CONNECTIONS; i++)
  {
    willSend += HaltStatusMgmt.haltTime[stockSymbolIndex][i];
  }
    
  if (willSend)
  {
    ttMessage->msgContent[ttMessage->msgLen++] = HaltStatusMgmt.isAutoResume[stockSymbolIndex];
    
    memcpy(&ttMessage->msgContent[ttMessage->msgLen], HaltStatusMgmt.symbol[stockSymbolIndex], SYMBOL_LEN);
    ttMessage->msgLen += SYMBOL_LEN;
      
    for (i = 0; i < TS_MAX_BOOK_CONNECTIONS; i++)
    {
      ttMessage->msgContent[ttMessage->msgLen] = HaltStatusMgmt.haltStatus[stockSymbolIndex][i];
      ttMessage->msgLen++;
    }
  }
  else
  {
    return 0;
  }
  
  // Block length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  ttMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  ttMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  ttMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[2] = intConvert.c[0];
  ttMessage->msgContent[3] = intConvert.c[1];
  ttMessage->msgContent[4] = intConvert.c[2];
  ttMessage->msgContent[5] = intConvert.c[3];

  return 1;
}

/****************************************************************************
- Function name:  ProcessTTMessageForSetHaltStockFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForSetHaltStockFromTT(void *message, int ttIndex)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Block content = type(4 bytes: Add = 0, Remove = 1) + a symbol (8 bytes)
  
  // Type
  int type = GetIntNumber(&ttMessage->msgContent[currentIndex]);

  // Update current index
  currentIndex += 4;

  // Symbol
  char symbol[SYMBOL_LEN];

  strncpy(symbol, (char *) &ttMessage->msgContent[currentIndex], SYMBOL_LEN);

  int stockSymbolIndex = GetStockSymbolIndex(symbol);
  if (stockSymbolIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "(ProcessTTMessageForSetHaltStockFromTT): Could not add symbol '%.8s' to stock list\n", symbol);
    return ERROR;
  }
  
  int i;
  unsigned int haltTime = hbitime_seconds();
  
  pthread_mutex_lock(&HaltStatusMgmt.mutexLock);
  if (type == 0)  // Halt, we will halt on all ECN
  {
    memcpy(HaltStatusMgmt.symbol[stockSymbolIndex], symbol, SYMBOL_LEN);
    for (i = 0; i < TS_MAX_BOOK_CONNECTIONS; i++)
    {
      HaltStatusMgmt.haltTime[stockSymbolIndex][i] = haltTime;
      HaltStatusMgmt.haltStatus[stockSymbolIndex][i] = TRADING_HALTED;
    }
    
    HaltStatusMgmt.isAutoResume[stockSymbolIndex] = 0;
  }
  else if (type == 1) //Remove
  {
    strncpy(HaltStatusMgmt.symbol[stockSymbolIndex], symbol, SYMBOL_LEN);
    for (i = 0; i < TS_MAX_BOOK_CONNECTIONS; i++)
    {
      HaltStatusMgmt.haltTime[stockSymbolIndex][i] = haltTime;
      HaltStatusMgmt.haltStatus[stockSymbolIndex][i] = TRADING_NORMAL;
    }
    HaltStatusMgmt.isAutoResume[stockSymbolIndex] = 1;
  }
  else
  {
    TraceLog(ERROR_LEVEL, "(ProcessTTMessageForSetHaltStockFromTT): Unknown action (%d) from TT for symbol '%.8s'\n", type, symbol);
    pthread_mutex_unlock(&HaltStatusMgmt.mutexLock);
    return ERROR;
  }
  
  SaveHaltedStockListToFile();
  SendAHaltStockToAllTS(stockSymbolIndex);
  SendAHaltStockToPM(stockSymbolIndex);
  SendAHaltStockToAllDaedalus(stockSymbolIndex);
  SendAHaltStockToAllTT(stockSymbolIndex);
  
  pthread_mutex_unlock(&HaltStatusMgmt.mutexLock);
  
  //Update TT event log to database
  char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
  sprintf(activityDetail, "%d,%d,%.8s", traderToolsInfo.connection[ttIndex].currentState, type, symbol);
  ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_HALTED_STOCK_LIST, activityDetail);

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForActivateHaltedStockFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForActivateHaltedStockFromTT(void *message, int ttIndex)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  int count = GetIntNumber(&ttMessage->msgContent[currentIndex]);
  TraceLog(DEBUG_LEVEL, "Receive activate all halted stocks from TT, number of symbols: %d\n", count);
  
  // Update current index
  currentIndex += 4;

  // Symbol
  char symbol[SYMBOL_LEN];

  pthread_mutex_lock(&HaltStatusMgmt.mutexLock);
  int i = 0;
  int stockSymbolIndex;
  int ecnIndex;
  unsigned int haltTime;
  int updatedList[MAX_STOCK_SYMBOL];
  int updatedCount = 0;
  
  while (i < count)
  {
    strncpy(symbol, (char *) &ttMessage->msgContent[currentIndex], SYMBOL_LEN);
    currentIndex += SYMBOL_LEN;
    
    stockSymbolIndex = GetStockSymbolIndex(symbol);
    if (stockSymbolIndex == -1)
    {
      TraceLog(ERROR_LEVEL, "(ProcessTTMessageForActivateHaltedStockFromTT): Could not get symbol index of symbol '%.8s'\n", symbol);
      pthread_mutex_unlock(&HaltStatusMgmt.mutexLock);
      return ERROR;
    }
    
    haltTime = hbitime_seconds();
    
    strncpy(HaltStatusMgmt.symbol[stockSymbolIndex], symbol, SYMBOL_LEN);
    for (ecnIndex = 0; ecnIndex < TS_MAX_BOOK_CONNECTIONS; ecnIndex++)
    {
      HaltStatusMgmt.haltTime[stockSymbolIndex][ecnIndex] = haltTime;
      HaltStatusMgmt.haltStatus[stockSymbolIndex][ecnIndex] = TRADING_NORMAL;
    }
    HaltStatusMgmt.isAutoResume[stockSymbolIndex] = 1;
    
    updatedList[updatedCount] = stockSymbolIndex;
    updatedCount++;
    i++;
  }
  
  SaveHaltedStockListToFile();
  SendHaltStockListToAllTS(updatedList, updatedCount);
  SendHaltStockListToPM(updatedList, updatedCount);
  SendActiveHaltedStockListToAllTT(updatedList, updatedCount);
  SendToActivateHaltStockListToAllDaedalus(updatedList, updatedCount);
  
  pthread_mutex_unlock(&HaltStatusMgmt.mutexLock);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTTMessageForGetTradedListOfASymbolFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForGetTradedListOfASymbolFromTT(void *message, int ttIndex)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Symbol
  char symbol[SYMBOL_LEN];

  strncpy(symbol, (char *) &ttMessage->msgContent[currentIndex], SYMBOL_LEN);

  TraceLog(DEBUG_LEVEL, "Received message to request get traded list to PM, symbol = %.8s\n", symbol);

  // Call function to get traded list to PM
  SendGetTradedListToPM(ttIndex, symbol);

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendTradedListToTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendTradedListToTT(int ttIndex, void *message)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];

  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  t_TTMessage ttMessage;
  
  // Build traded list message to send Trader Tool
  BuildTTMessageForTradedList(&ttMessage, message);
  
  // Send message to Trader Tool
  return SendTTMessage(connection, &ttMessage);
}

/****************************************************************************
- Function name:  BuildTTMessageForTradedList
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTTMessageForTradedList(void *message, void *msg)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *pmMessage = (t_TSMessage *)msg;
  t_TTMessage *ttMessage = (t_TTMessage *)message;
  
  // Default value at begin index of symbol
  int blockLen = GetIntNumber(&pmMessage->msgContent[6]);

  TraceLog(DEBUG_LEVEL, "BuildTTMessageForTradedList, blockLen = %d\n", blockLen - 4);

  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_TRADED_LIST_TO_TT;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1];
  ttMessage->msgLen += 2;

  // Body length field
  intConvert.value = blockLen - 4;
  ttMessage->msgContent[2] = intConvert.c[0];
  ttMessage->msgContent[3] = intConvert.c[1];
  ttMessage->msgContent[4] = intConvert.c[2];
  ttMessage->msgContent[5] = intConvert.c[3];
  ttMessage->msgLen += 4;

  // Message body = block len + block content (symbol + N * (dataFeed + participantId + timestamp + shares + price))
  
  // Block length field
  intConvert.value = blockLen - 4;
  ttMessage->msgContent[6] = intConvert.c[0];
  ttMessage->msgContent[7] = intConvert.c[1];
  ttMessage->msgContent[8] = intConvert.c[2];
  ttMessage->msgContent[9] = intConvert.c[3];
  ttMessage->msgLen += 4;
  
  // Symbol (8 bytes) + N * (dataFeed + participantId + timestamp + shares + price)
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], &pmMessage->msgContent[14], blockLen - 8);
  ttMessage->msgLen += (blockLen - 8);
  
  return ttMessage->msgLen;
}

/****************************************************************************
- Function name:  SendPMDisengageAlertToAllTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendPMDisengageAlertToAllTT(void *message)
{
  int ttIndex;
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;
  t_TSMessage *pmMessage = (t_TSMessage *)message;

  // Build PM disenage alert message to send Trader Tool
  BuildTTMessageForPMDisengageAlert(&ttMessage, pmMessage);

  // Send message to all Trader Tool connected to AS
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];

    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_TRADER_TOOL)
    {
      continue;
    }   

    // Send message to Trader Tool
    SendTTMessage(connection, &ttMessage);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  BuildTTMessageForPMDisengageAlert
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTTMessageForPMDisengageAlert(void *message, void *messageSrc)
{
  t_ShortConverter shortConvert;

  t_TSMessage *pmMessage = (t_TSMessage *)messageSrc;
  t_TTMessage *ttMessage = (t_TTMessage *)message;
    
  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_GET_DISENGAGE_ALERT_FROM_TT;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1]; 
  
  memcpy(&ttMessage->msgContent[2], &pmMessage->msgContent[2], pmMessage->msgLen - 2);
  ttMessage->msgLen = pmMessage->msgLen;
  
  char symbol[SYMBOL_LEN];
  double price;
  int shares, side;
  
  int account = pmMessage->msgContent[31];
  
  // SYMBOL(8 bytes)
  strncpy(symbol, (char *) &pmMessage->msgContent[10], SYMBOL_LEN);

  // STUCK PRICE (8 bytes)
  price = GetDoubleNumber(&pmMessage->msgContent[18]);

  // SHARES (4 bytes)
  shares = GetIntNumber(&pmMessage->msgContent[26]);
  
  // SIDE
  side = pmMessage->msgContent[30];

  TraceLog(DEBUG_LEVEL, "Send PM disengage alert: account: %d symbol: %.8s, side: %d, shares: %d, price: %lf\n", account, symbol, side, shares, price);

  return ttMessage->msgLen;
}

/****************************************************************************
- Function name:  SendTSSettingToTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendTSSettingToTT(void *pConnection)
{
  //TraceLog(DEBUG_LEVEL, "Send TS setting to TT ...\n");

  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  int tsIndex;
  t_TTMessage ttMessage;

  for (tsIndex = 0; tsIndex < tradeServersInfo.maxConnections; tsIndex++)
  {
    // Build TS setting message to send Trader Tool
    BuildTTMessageForTSSetting(&ttMessage, tsIndex);

    // Send message to Trader Tool
    if (SendTTMessage(connection, &ttMessage) == ERROR)
    {
      return ERROR;
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  BuildTTMessageForTSSetting
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTTMessageForTSSetting(void *message, int tsIndex)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TTMessage *ttMessage = (t_TTMessage *)message;
  
  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_TS_SETTING_TO_TT;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1];
  ttMessage->msgLen += 2;

  // Update index of body length
  ttMessage->msgLen += 4;

  // Message body = n * (blockLength (4 bytes) + blockContent (blockLength bytes))  
  // Update index of block length
  ttMessage->msgLen += 4;

  // Block content = Trade Server Id (4 bytes)  + TS setting
  // Trade Server Id (4 bytes)
  intConvert.value = tradeServersInfo.config[tsIndex].tsId;
  ttMessage->msgContent[ttMessage->msgLen] = intConvert.c[0];
  ttMessage->msgContent[ttMessage->msgLen + 1] = intConvert.c[1];
  ttMessage->msgContent[ttMessage->msgLen + 2] = intConvert.c[2];
  ttMessage->msgContent[ttMessage->msgLen + 3] = intConvert.c[3];
  ttMessage->msgLen += 4;
  
  // TS setting
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], (unsigned char *)&tradeServersInfo.tsSetting[tsIndex], sizeof(t_TSSetting));

  ttMessage->msgLen += sizeof(t_TSSetting); 

  // Block length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  ttMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  ttMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  ttMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[2] = intConvert.c[0];
  ttMessage->msgContent[3] = intConvert.c[1];
  ttMessage->msgContent[4] = intConvert.c[2];
  ttMessage->msgContent[5] = intConvert.c[3];

  return ttMessage->msgLen;
}

/****************************************************************************
- Function name:  ProcessTTMessageForSetTSSettingFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForSetTSSettingFromTT(void *message)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Trade Server Id
  int tsId = GetIntNumber(&ttMessage->msgContent[currentIndex]);
  currentIndex += 4;

  // Find Trade Server index
  int tsIndex = FindTradeServerIndex(tsId);

  if (tsIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "Invalid Trade Server Id = %d\n", tsId);

    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Set exchange setting for %s\n", tradeServersInfo.config[tsIndex].description);

  // Block content = Trade Server setting
  memcpy((unsigned char *)&tradeServersInfo.tsSetting[tsIndex], &ttMessage->msgContent[currentIndex], sizeof(t_TSSetting));
  
  int i;  
  for (i = 0; i < TS_MAX_BOOK_CONNECTIONS; i++)
  {
    if( (tradeServersInfo.tsSetting[tsIndex].bookEnable[i] == NO) &&
        (tradeServersInfo.currentStatus[tsIndex].tsMdcStatus[i] == CONNECTED)
       )
    {
      TraceLog(DEBUG_LEVEL, "Send request %s disconnect to ECN Book, ecnIndex = %d\n", tradeServersInfo.config[tsIndex].description, i);
      SendDisconnectBookToTS(tsIndex, i);
    }
  }
  
  for (i = 0; i < TS_MAX_ORDER_CONNECTIONS; i++)
  {
    if( (tradeServersInfo.tsSetting[tsIndex].orderEnable[i] == NO) &&
        (tradeServersInfo.currentStatus[tsIndex].tsOecStatus[i] == CONNECTED)
       )
    {
      TraceLog(DEBUG_LEVEL, "Send request %s disconnect to ECN Order, ecnIndex = %d\n", tradeServersInfo.config[tsIndex].description, i);
      SendDisconnectOrderToTS(tsIndex, i);
    }
  }
  
  if ((tradeServersInfo.tsSetting[tsIndex].bboEnable[CQS_SOURCE] == NO) && 
    (tradeServersInfo.currentStatus[tsIndex].statusCQS == CONNECTED))
  {
    TraceLog(DEBUG_LEVEL, "Send request %s disconnect to CQS\n", tradeServersInfo.config[tsIndex].description);
    
    // Call function to disconnect to CQS from TS
    SendDisconnectToCQSnUQDFToTS(tsIndex, CQS_SOURCE);
  }
  
  if ((tradeServersInfo.tsSetting[tsIndex].bboEnable[UQDF_SOURCE] == NO) &&
    (tradeServersInfo.currentStatus[tsIndex].statusUQDF == CONNECTED))
  {
    TraceLog(DEBUG_LEVEL, "Send request %s disconnect to UQDF\n", tradeServersInfo.config[tsIndex].description);
    
    // Call function to disconnect to UQDF from TS
    SendDisconnectToCQSnUQDFToTS(tsIndex, UQDF_SOURCE);
  }

  // Add code here to update Trade Server setting
  SaveTradeServerSetting(tsIndex);

  // Send Trade Server setting to all Trader Tool
  SendTSSettingToAllTT(tsIndex);

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendTSSettingToAllTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendTSSettingToAllTT(int tsIndex)
{
  int ttIndex;
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;

  // Build Trade Servers setting message to send Trader Tool
  BuildTTMessageForTSSetting(&ttMessage, tsIndex);

  // Send message to all Trader Tool connected to AS
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];
    
    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_TRADER_TOOL)
    {
      continue;
    }

    // Send message to Trader Tool
    SendTTMessage(connection, &ttMessage);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendASSettingToTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendASSettingToTT(void *pConnection)
{
  //TraceLog(DEBUG_LEVEL, "Send AS setting to TT ...\n");

  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  t_TTMessage ttMessage;

  // Build AS setting message to send Trader Tool
  BuildTTMessageForASSetting(&ttMessage);

  // Send message to Trader Tool
  return SendTTMessage(connection, &ttMessage);
}

/****************************************************************************
- Function name:  BuildTTMessageForASSetting
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTTMessageForASSetting(void *message)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TTMessage *ttMessage = (t_TTMessage *)message;
  
  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_AS_SETTING_TO_TT;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1];
  ttMessage->msgLen += 2;

  // Update index of body length
  ttMessage->msgLen += 4;

  // Message body = n * (blockLength (4 bytes) + blockContent (blockLength bytes))  
  // Update index of block length
  ttMessage->msgLen += 4;

  // Block content = AS setting
  
  // AS setting
  //memcpy(&ttMessage->msgContent[ttMessage->msgLen], (unsigned char *)&asSetting, sizeof(t_ASSetting));
  //ttMessage->msgLen += sizeof(t_ASSetting); 
  ttMessage->msgContent[ttMessage->msgLen++] = 0; //for old TT compatible
  ttMessage->msgContent[ttMessage->msgLen++] = 0; //for old TT compatible
  ttMessage->msgContent[ttMessage->msgLen++] = 0; //for old TT compatible
  ttMessage->msgContent[ttMessage->msgLen++] = asSetting.orderEnable[AS_NASDAQ_RASH_INDEX];
  ttMessage->msgContent[ttMessage->msgLen++] = asSetting.orderEnable[AS_ARCA_DIRECT_INDEX];

  // Block length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  ttMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  ttMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  ttMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[2] = intConvert.c[0];
  ttMessage->msgContent[3] = intConvert.c[1];
  ttMessage->msgContent[4] = intConvert.c[2];
  ttMessage->msgContent[5] = intConvert.c[3];

  return ttMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildTTMessageForMarketModeUpdate
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTTMessageForMarketModeUpdate(void *message, const char mode)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TTMessage *ttMessage = (t_TTMessage *)message;
  
  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_MARKET_MODE_TO_TT;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1];
  ttMessage->msgLen += 2;

  // Update index of body length
  ttMessage->msgLen += 4;

  // Update index of block length
  ttMessage->msgLen += 4;

  // Block content: Current Market Mode, 1 byte binary
  ttMessage->msgContent[ttMessage->msgLen] = mode;
  ttMessage->msgLen += 1; 

  // Block length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  ttMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  ttMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  ttMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[2] = intConvert.c[0];
  ttMessage->msgContent[3] = intConvert.c[1];
  ttMessage->msgContent[4] = intConvert.c[2];
  ttMessage->msgContent[5] = intConvert.c[3];

  return ttMessage->msgLen;
}

/****************************************************************************
- Function name:  ProcessTTMessageForSetASSettingFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForSetASSettingFromTT(void *message)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  TraceLog(DEBUG_LEVEL, "Set AS connections\n");

  // Block content = AS setting
  asSetting.orderEnable[AS_NASDAQ_RASH_INDEX] = ttMessage->msgContent[currentIndex + 3];
  asSetting.orderEnable[AS_ARCA_DIRECT_INDEX] = ttMessage->msgContent[currentIndex + 4];
  
  SaveASSetting();
  
  // Send AS setting to all Trader Tool
  SendASSettingToAllTT();
  
  if (asSetting.orderEnable[AS_NASDAQ_RASH_INDEX] == YES)
  {
    //RASH on AS is enabled --> disable RASH on PM
    if (pmSetting.orderEnable[PM_NASDAQ_RASH_INDEX] == YES)
    {
      pmSetting.orderEnable[PM_NASDAQ_RASH_INDEX] = NO;
      SavePMSetting();
      SendPMSettingToAllTT();
    }
  }
  else
  {
    //RASH on AS is disabled --> Enable RASH on PM
    if (pmSetting.orderEnable[PM_NASDAQ_RASH_INDEX] == NO)
    {
      pmSetting.orderEnable[PM_NASDAQ_RASH_INDEX] = YES;
      SavePMSetting();
      SendPMSettingToAllTT();
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendASSettingToAllTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendASSettingToAllTT(void)
{
  int ttIndex;
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;

  // Build AS setting message to send Trader Tool
  BuildTTMessageForASSetting(&ttMessage);

  // Send message to all Trader Tool connected to AS
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];
    
    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_TRADER_TOOL)
    {
      continue;
    }

    // Send message to Trader Tool
    SendTTMessage(connection, &ttMessage);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendPMSettingToTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendPMSettingToTT(void *pConnection)
{
  TraceLog(DEBUG_LEVEL, "Send PM setting to TT ...\n");

  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  t_TTMessage ttMessage;

  // Build PM setting message to send Trader Tool
  BuildTTMessageForPMSetting(&ttMessage);

  // Send message to Trader Tool
  return SendTTMessage(connection, &ttMessage);
}

/****************************************************************************
- Function name:  BuildTTMessageForPMSetting
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTTMessageForPMSetting(void *message)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TTMessage *ttMessage = (t_TTMessage *)message;
  
  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_PM_SETTING_TO_TT;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1];
  ttMessage->msgLen += 2;

  // Update index of body length
  ttMessage->msgLen += 4;

  // Message body = n * (blockLength (4 bytes) + blockContent (blockLength bytes))  
  // Update index of block length
  ttMessage->msgLen += 4;

  // Block content = PM setting
  
  // PM setting, does not send the last value to TT: isConnectAll
  //memcpy(&ttMessage->msgContent[ttMessage->msgLen], (unsigned char *)&pmSetting, (sizeof(t_PMSetting) - 1) );
  //ttMessage->msgLen += (sizeof(t_PMSetting) - 1);

  // PM setting, does not send the last value to TT: isConnectAll
  /*char bookEnable[MAX_BOOK_CONNECTIONS_OF_PM];
  char orderEnable[MAX_ORDER_CONNECTIONS_OF_AS];
  char CTS_Enable;
  char UTDF_Enable;
  char CQS_Enable;
  char UQDF_Enable;*/
  //memcpy(&ttMessage->msgContent[ttMessage->msgLen], (unsigned char *)&pmSetting, (sizeof(t_PMSetting) - 1) );
  //ttMessage->msgLen += (sizeof(t_PMSetting) - 1);
  ttMessage->msgContent[ttMessage->msgLen++] = pmSetting.bookEnable[PM_ARCA_BOOK_INDEX];
  ttMessage->msgContent[ttMessage->msgLen++] = pmSetting.bookEnable[PM_NASDAQ_BOOK_INDEX];
  ttMessage->msgContent[ttMessage->msgLen++] = 0; //batsz
  ttMessage->msgContent[ttMessage->msgLen++] = 0; //order index 1
  ttMessage->msgContent[ttMessage->msgLen++] = 0; //order index 2
  ttMessage->msgContent[ttMessage->msgLen++] = 0; //order index 3
  ttMessage->msgContent[ttMessage->msgLen++] = pmSetting.orderEnable[PM_NASDAQ_RASH_INDEX];
  ttMessage->msgContent[ttMessage->msgLen++] = pmSetting.orderEnable[PM_ARCA_DIRECT_INDEX];
  ttMessage->msgContent[ttMessage->msgLen++] = pmSetting.CTS_Enable;
  ttMessage->msgContent[ttMessage->msgLen++] = pmSetting.UTDF_Enable;
  ttMessage->msgContent[ttMessage->msgLen++] = pmSetting.CQS_Enable;
  ttMessage->msgContent[ttMessage->msgLen++] = pmSetting.UQDF_Enable;
  
  // Block length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  ttMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  ttMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  ttMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[2] = intConvert.c[0];
  ttMessage->msgContent[3] = intConvert.c[1];
  ttMessage->msgContent[4] = intConvert.c[2];
  ttMessage->msgContent[5] = intConvert.c[3];

  return ttMessage->msgLen;
}

/****************************************************************************
- Function name:  ProcessTTMessageForSetPMSettingFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTTMessageForSetPMSettingFromTT(void *message)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  TraceLog(DEBUG_LEVEL, "Set PM connections\n");

  // Block content = PM setting. Does not update the last value: isConnectAll
  //memcpy((unsigned char *)&pmSetting, &ttMessage->msgContent[currentIndex], (sizeof(t_PMSetting) - 1) );
  pmSetting.bookEnable[PM_ARCA_BOOK_INDEX] = ttMessage->msgContent[currentIndex++];
  pmSetting.bookEnable[PM_NASDAQ_BOOK_INDEX] = ttMessage->msgContent[currentIndex++];
  currentIndex++; //batsz
  currentIndex++; //order index 1
  currentIndex++; //order index 2
  currentIndex++; //order index 3
  pmSetting.orderEnable[PM_NASDAQ_RASH_INDEX] = ttMessage->msgContent[currentIndex++];
  pmSetting.orderEnable[PM_ARCA_DIRECT_INDEX] = ttMessage->msgContent[currentIndex++];
  pmSetting.CTS_Enable = ttMessage->msgContent[currentIndex++];
  pmSetting.UTDF_Enable = ttMessage->msgContent[currentIndex++];
  pmSetting.CQS_Enable = ttMessage->msgContent[currentIndex++];
  pmSetting.UQDF_Enable = ttMessage->msgContent[currentIndex++];
  
  SavePMSetting();

  // Send PM setting to all Trader Tool
  SendPMSettingToAllTT();
  
  if (pmSetting.orderEnable[PM_NASDAQ_RASH_INDEX] == YES)
  {
    //RASH on PM is enabled --> disable RASH on AS
    if (asSetting.orderEnable[AS_NASDAQ_RASH_INDEX] == YES)
    {
      asSetting.orderEnable[AS_NASDAQ_RASH_INDEX] = NO;
      SaveASSetting();
      SendASSettingToAllTT();
    }
  }
  else
  {
    //RASH on PM is disabled --> Enable RASH on AS
    if (asSetting.orderEnable[AS_NASDAQ_RASH_INDEX] == NO)
    {
      asSetting.orderEnable[AS_NASDAQ_RASH_INDEX] = YES;
      SaveASSetting();
      SendASSettingToAllTT();
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendPMSettingToAllTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendPMSettingToAllTT(void)
{
  int ttIndex;
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;

  // Build PM setting message to send Trader Tool
  BuildTTMessageForPMSetting(&ttMessage);

  // Send message to all Trader Tool connected to AS
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];
    
    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_TRADER_TOOL)
    {
      continue;
    }

    // Send message to Trader Tool
    SendTTMessage(connection, &ttMessage);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  BuildTTMessageForTSPMBookIdleWarning
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTTMessageForTSPMBookIdleWarning(void *message, int tsPMIndex, int ECNIndex, int groupID)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TTMessage *ttMessage = (t_TTMessage *)message;
  
  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_TS_PM_BOOK_IDLE_WARNING_TO_TT;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1];
  ttMessage->msgLen += 2;

  // Update index of body length
  intConvert.value = 4 + 4 + 4 + 4;
  ttMessage->msgContent[2] = intConvert.c[0];
  ttMessage->msgContent[3] = intConvert.c[1];
  ttMessage->msgContent[4] = intConvert.c[2];
  ttMessage->msgContent[5] = intConvert.c[3];
  ttMessage->msgLen += 4;

  // Update index of block length
  intConvert.value = 4 + 4 + 4 + 4;
  ttMessage->msgContent[6] = intConvert.c[0];
  ttMessage->msgContent[7] = intConvert.c[1];
  ttMessage->msgContent[8] = intConvert.c[2];
  ttMessage->msgContent[9] = intConvert.c[3];
  ttMessage->msgLen += 4;

  // TS index or PM index (4 bytes)
  if (tsPMIndex == 99) //Warning from PM
  {
    intConvert.value = 99;
    ttMessage->msgContent[ttMessage->msgLen] = intConvert.c[0];
    ttMessage->msgContent[ttMessage->msgLen + 1] = intConvert.c[1];
    ttMessage->msgContent[ttMessage->msgLen + 2] = intConvert.c[2];
    ttMessage->msgContent[ttMessage->msgLen + 3] = intConvert.c[3];
  }
  else  //Warning from TS
  {
    intConvert.value = tradeServersInfo.config[tsPMIndex].tsId;
    ttMessage->msgContent[ttMessage->msgLen] = intConvert.c[0];
    ttMessage->msgContent[ttMessage->msgLen + 1] = intConvert.c[1];
    ttMessage->msgContent[ttMessage->msgLen + 2] = intConvert.c[2];
    ttMessage->msgContent[ttMessage->msgLen + 3] = intConvert.c[3];
  }
  ttMessage->msgLen += 4;

  // ECN Book Index (4 bytes)
  if (tsPMIndex == 99)
  {
    intConvert.value = PM_TO_TT_ORDER_INDEX_MAPPING[ECNIndex];
  }
  else
  {
    intConvert.value = TS_TO_TT_ORDER_INDEX_MAPPING[ECNIndex];
  }
  ttMessage->msgContent[ttMessage->msgLen] = intConvert.c[0];
  ttMessage->msgContent[ttMessage->msgLen + 1] = intConvert.c[1];
  ttMessage->msgContent[ttMessage->msgLen + 2] = intConvert.c[2];
  ttMessage->msgContent[ttMessage->msgLen + 3] = intConvert.c[3];
  ttMessage->msgLen += 4;

  //Group ID (4 bytes)
  intConvert.value = groupID;
  ttMessage->msgContent[ttMessage->msgLen] = intConvert.c[0];
  ttMessage->msgContent[ttMessage->msgLen + 1] = intConvert.c[1];
  ttMessage->msgContent[ttMessage->msgLen + 2] = intConvert.c[2];
  ttMessage->msgContent[ttMessage->msgLen + 3] = intConvert.c[3];
  ttMessage->msgLen += 4;

  return ttMessage->msgLen;
}

/****************************************************************************
- Function name:  SendTSPMBookIdleWarningToAllTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendTSPMBookIdleWarningToAllTT(int tsPmIndex, int ecnIndex, int groupIndex)
{
  int ttIndex;
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;

  // Build TS global configuration message to send Trader Tool
  BuildTTMessageForTSPMBookIdleWarning(&ttMessage, tsPmIndex, ecnIndex, groupIndex);

  // Send message to all Trader Tool connected to AS
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];

    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_TRADER_TOOL)
    {
      continue;
    }   

    // Send message to Trader Tool
    SendTTMessage(connection, &ttMessage);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessMessageForConnectToCQSnUQDF_FromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessMessageForConnectToCQSnUQDF_FromTT(t_TTMessage *message, int ttIndex)
{
  int index = MSG_HEADER_LEN + 4;
  
  // TS id
  int tsId = GetIntNumber(&message->msgContent[index]);
  index += 4;
  
  // Find Trade Server index
  int tsIndex = FindTradeServerIndex(tsId);

  if (tsIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "Invalid Trade Server Id = %d\n", tsId);

    return ERROR;
  }
  
  int sourceId = message->msgContent[index];
  
  if (sourceId == CQS_SOURCE)
  {
    TraceLog(DEBUG_LEVEL, "Received request %s connect to CQS from TT\n", tradeServersInfo.config[tsIndex].description);
  }
  else if (sourceId == UQDF_SOURCE)
  {
    TraceLog(DEBUG_LEVEL, "Received request %s connect to UQDF from TT\n", tradeServersInfo.config[tsIndex].description);
  }
  else
  {
    TraceLog(ERROR_LEVEL, "Invalid message %s connect to CQS/UQDF from TT, sourceId = %d\n", tradeServersInfo.config[tsIndex].description, sourceId);
  }
  
  // Send request to connect to CQS/UDQF
  if (SendConnectToCQSnUQDFToTS(tsIndex, sourceId) == SUCCESS)
  {
    //Update TT event log: TT request connect to CQS/UQDF on TS 
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    sprintf(activityDetail, "%d,0,%d6%d", traderToolsInfo.connection[ttIndex].currentState, tsId, sourceId + 1);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_BBO, activityDetail);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessMessageForDisconnectToCQSnUQDF_FromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessMessageForDisconnectToCQSnUQDF_FromTT(t_TTMessage *message, int ttIndex)
{
  int index = MSG_HEADER_LEN + 4;
  
  // TS id
  int tsId = GetIntNumber(&message->msgContent[index]);
  index += 4;
  
  // Find Trade Server index
  int tsIndex = FindTradeServerIndex(tsId);

  if (tsIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "Invalid Trade Server Id = %d\n", tsId);

    return ERROR;
  }
  
  int sourceId = message->msgContent[index];
  
  if (sourceId == CQS_SOURCE)
  {
    TraceLog(DEBUG_LEVEL, "Received request %s disconnect to CQS from TT\n", tradeServersInfo.config[tsIndex].description);
  }
  else if (sourceId == UQDF_SOURCE)
  {
    TraceLog(DEBUG_LEVEL, "Received request %s disconnect to UQDF from TT\n", tradeServersInfo.config[tsIndex].description);
  }
  else
  {
    TraceLog(ERROR_LEVEL, "Invalid message %s disconnect to CQS/UQDF from TT, sourceId = %d\n", tradeServersInfo.config[tsIndex].description, sourceId);
  }
  
  // Send request to disconnect to CQS/UDQF
  if (SendDisconnectToCQSnUQDFToTS(tsIndex, sourceId) == SUCCESS)
  {
    //Update TT event log: TT request disconnect to CQS/UQDF on TS 
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    sprintf(activityDetail, "%d,1,%d6%d", traderToolsInfo.connection[ttIndex].currentState, tsId, sourceId + 1);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_BBO, activityDetail);
  }
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessMessageForSetISO_TradingFromTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessMessageForSetISO_TradingFromTT(t_TTMessage *message, int ttIndex)
{
  int index = MSG_HEADER_LEN + 4;
  
  // TS id
  int tsId = GetIntNumber(&message->msgContent[index]);
  index += 4;
  
  // Find Trade Server index
  int tsIndex = FindTradeServerIndex(tsId);

  if (tsIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "Invalid Trade Server Id = %d\n", tsId);

    return ERROR;
  }
  
  // ISO status
  int status = message->msgContent[index];
  
  TraceLog(DEBUG_LEVEL, "Received request to enable/disable ISO algorithm of %s from TT, status = %d\n", tradeServersInfo.config[tsIndex].description, status);
  
  // Send request to enable/disable ISO algorithm 
  if (SendSetISO_TradingToTS(tsIndex, status) == SUCCESS)
  {
    BackupTSGlobalConf[tsIndex].ISO_Trading = status;
    // Update TT event log: Enable/disable ISO trading on TS
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    sprintf(activityDetail, "%d,%d,%d", traderToolsInfo.connection[ttIndex].currentState, status, tsId);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_ENABLE_DISENABLE_ISO_TRADING, activityDetail);
  }
  
  //Create a ISO Record
  InsertISOToggleToISOFlightFile(tsId, status, traderToolsInfo.connection[ttIndex].userName);
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessMessageForGetBBOQuotes
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessMessageForGetBBOQuotes(t_TTMessage *message, int ttIndex)
{
  int index = MSG_HEADER_LEN + 4;
  
  // TS id
  int tsId = GetIntNumber(&message->msgContent[index]);
  index += 4;
  
  // Find Trade Server index
  int tsIndex = FindTradeServerIndex(tsId);

  if (tsIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "Invalid Trade Server Id = %d\n", tsId);

    return ERROR;
  }
  
  char symbol[SYMBOL_LEN];
  strncpy(symbol, (char *) &message->msgContent[index], SYMBOL_LEN);
  
  TraceLog(DEBUG_LEVEL, "Receive request to get BBO quotes of %s from TT: symbol = %.8s\n", tradeServersInfo.config[tsIndex].description, symbol);
  
  // Send get BBO quotes from TS
  SendGetBBOQuotesToTS(tsIndex, symbol, ttIndex);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessMessageForSetExchangeConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessMessageForSetExchangeConf(t_TTMessage *message)
{
  int index = MSG_HEADER_LEN + 4;
  
  // TS id
  int tsId = GetIntNumber(&message->msgContent[index]);
  index += 4;
  
  // Find Trade Server index
  int tsIndex = FindTradeServerIndex(tsId);

  if (tsIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "Invalid Trade Server Id = %d\n", tsId);

    return ERROR;
  }
  
  TraceLog(DEBUG_LEVEL, "Receive message to request %s set exchange conf from TT\n", tradeServersInfo.config[tsIndex].description);
  
  SendSetExchangesConfToTS(tsIndex, message->msgLen - (MSG_HEADER_LEN + 8), &message->msgContent[index]);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessMessageForFlushQuotes
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessMessageForFlushBBOQuotes(t_TTMessage *message)
{
  int index = MSG_HEADER_LEN + 4;
  
  // TS id
  int tsId = GetIntNumber(&message->msgContent[index]);
  index += 4;
  
  // Find Trade Server index
  int tsIndex = FindTradeServerIndex(tsId);

  if (tsIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "Invalid Trade Server Id = %d\n", tsId);

    return ERROR;
  }
  
  // Exchange id
  int exchangeId = message->msgContent[index];
  
  TraceLog(DEBUG_LEVEL, "Receive message to request %s flush BBO quotes from TT, exchangeId = %d\n", tradeServersInfo.config[tsIndex].description, exchangeId);
  
  SendFlushBBOQuotesToTS(tsIndex, exchangeId);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendExchangesConfToTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendExchangesConfToTT(void *pConnection, int tsIndex)
{
  t_TTMessage message;
  t_ShortConverter tmpShort;
  t_IntConverter tmpInt;
  
  int currentIndex = 0; 
  
  // Message type
  tmpShort.value = MSG_TYPE_FOR_SEND_EXCHANGE_CONF_TO_TT; 
  message.msgContent[currentIndex] = tmpShort.c[0];
  message.msgContent[currentIndex + 1] = tmpShort.c[1];
  
  currentIndex += 2;
  
  // Message length
  currentIndex += 4;
  
  // Block length
  currentIndex += 4;
  
  // Block content = 16 * [index + ID + CQS_Status{visible + Status} + UQDF_Status{visible + Status}]
  
  // TS Id
  tmpInt.value = tradeServersInfo.config[tsIndex].tsId;
  message.msgContent[currentIndex] = tmpInt.c[0];
  message.msgContent[currentIndex + 1] = tmpInt.c[1];
  message.msgContent[currentIndex + 2] = tmpInt.c[2];
  message.msgContent[currentIndex + 3] = tmpInt.c[3];
  currentIndex += 4;
  
  int i;
  for (i = 0; i < MAX_EXCHANGE; i++)
  {
    // index
    message.msgContent[currentIndex] = i;
    currentIndex += 1;
    
    // exchangeId
    message.msgContent[currentIndex] = ExchangeStatusList[tsIndex][i].exchangeId;
    currentIndex += 1;
    
    // CQS_Status{visible + Status}
    message.msgContent[currentIndex] = ExchangeStatusList[tsIndex][i].status[CQS_SOURCE].visible;
    message.msgContent[currentIndex + 1] = ExchangeStatusList[tsIndex][i].status[CQS_SOURCE].enable;
    currentIndex += 2;
    
    // UQDF_Status{visible + Status}
    message.msgContent[currentIndex] = ExchangeStatusList[tsIndex][i].status[UQDF_SOURCE].visible;
    message.msgContent[currentIndex + 1] = ExchangeStatusList[tsIndex][i].status[UQDF_SOURCE].enable;
    currentIndex += 2;
  }
  
  // Block length
  tmpInt.value = currentIndex - 6;
  message.msgContent[6] = tmpInt.c[0];
  message.msgContent[7] = tmpInt.c[1];
  message.msgContent[8] = tmpInt.c[2];
  message.msgContent[9] = tmpInt.c[3];
  
  // Message length
  tmpInt.value = currentIndex - 6;
  message.msgContent[2] = tmpInt.c[0];
  message.msgContent[3] = tmpInt.c[1];
  message.msgContent[4] = tmpInt.c[2];
  message.msgContent[5] = tmpInt.c[3];
  
  // Message length
  message.msgLen = currentIndex;
  
  return SendTTMessage(pConnection, &message);
}

/****************************************************************************
- Function name:  SendExchangesConfToAllTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendExchangesConfToAllTT(int tsIndex)
{
  int ttIndex;
  t_ConnectionStatus *connection;

  // Send message to all TT connected
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];
    
    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_TRADER_TOOL)
    {
      continue;
    }

    // Send message to Trader Tool
    SendExchangesConfToTT(connection, tsIndex);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendExchangesConfOfAllTSToTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendExchangesConfOfAllTSToTT(void *pConnection)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  int tsIndex;

  for (tsIndex = 0; tsIndex < tradeServersInfo.maxConnections; tsIndex++)
  {
    // Check connection between AS and TS
    if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
    {
      continue;
    }   
    
    // Send exchange configuration
    if (SendExchangesConfToTT(connection, tsIndex) == ERROR)
    {
      return ERROR;
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendBBOQuotesToTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendBBOQuotesToTT(int ttIndex, int tsIndex, void *message)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];

  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *tsMessage = (t_TSMessage *)message;
  t_TTMessage ttMessage;
  
  // Initialize message
  ttMessage.msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_BBO_QUOTE_LIST_TO_TT;
  ttMessage.msgContent[0] = shortConvert.c[0];
  ttMessage.msgContent[1] = shortConvert.c[1];
  ttMessage.msgLen += 2;

  // Update index of body length
  ttMessage.msgLen += 4;

  // Message body = 3 block (block sender + block ASK + block BID)
  
  // Block sender connect = TS Id (4 bytes) + symbol (8 bytes)
  // Update index of block length
  intConvert.value = SYMBOL_LEN + 8;
  ttMessage.msgContent[ttMessage.msgLen] = intConvert.c[0];
  ttMessage.msgContent[ttMessage.msgLen + 1] = intConvert.c[1];
  ttMessage.msgContent[ttMessage.msgLen + 2] = intConvert.c[2];
  ttMessage.msgContent[ttMessage.msgLen + 3] = intConvert.c[3];
  ttMessage.msgLen += 4;

  // Trade Server Id (4 bytes)
  intConvert.value = tradeServersInfo.config[tsIndex].tsId;
  ttMessage.msgContent[ttMessage.msgLen] = intConvert.c[0];
  ttMessage.msgContent[ttMessage.msgLen + 1] = intConvert.c[1];
  ttMessage.msgContent[ttMessage.msgLen + 2] = intConvert.c[2];
  ttMessage.msgContent[ttMessage.msgLen + 3] = intConvert.c[3];
  ttMessage.msgLen += 4;

  // Symbol (8 bytes)
  memcpy(&ttMessage.msgContent[ttMessage.msgLen], &tsMessage->msgContent[MSG_HEADER_LEN + 5], SYMBOL_LEN);
  ttMessage.msgLen += SYMBOL_LEN;
  
  // block ASK + block BID
  memcpy(&ttMessage.msgContent[ttMessage.msgLen], &tsMessage->msgContent[MSG_HEADER_LEN + 5 + SYMBOL_LEN], tsMessage->msgLen - (MSG_HEADER_LEN + 5 + SYMBOL_LEN));
  ttMessage.msgLen += tsMessage->msgLen - (MSG_HEADER_LEN + 5 + SYMBOL_LEN);
  
  // Body length field
  intConvert.value = ttMessage.msgLen - MSG_HEADER_LEN;
  ttMessage.msgContent[2] = intConvert.c[0];
  ttMessage.msgContent[3] = intConvert.c[1];
  ttMessage.msgContent[4] = intConvert.c[2];
  ttMessage.msgContent[5] = intConvert.c[3];

  // Send message to Trader Tool
  return SendTTMessage(connection, &ttMessage);
}

/****************************************************************************
- Function name:  SendBookISO_TradingToAllTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendBookISO_TradingToAllTT(int tsIndex)
{
  int ttIndex;
  t_ConnectionStatus *connection;

  // Send message to all TT connected
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];
    
    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_TRADER_TOOL)
    {
      continue;
    }

    // Send message to Trader Tool
    SendBookISO_TradingToTT(connection, tsIndex);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendBookISO_TradingOfAllTSToTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendBookISO_TradingOfAllTSToTT(void *pConnection)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  int tsIndex;

  for (tsIndex = 0; tsIndex < tradeServersInfo.maxConnections; tsIndex++)
  {
    // Check connection between AS and TS
    if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
    {
      continue;
    }   
    
    // Send exchange configuration
    if (SendBookISO_TradingToTT(connection, tsIndex) == ERROR)
    {
      return ERROR;
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendBookISO_TradingToTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendBookISO_TradingToTT(void *pConnection, int tsIndex)
{
  t_TTMessage message;
  t_ShortConverter tmpShort;
  t_IntConverter tmpInt;
  
  int currentIndex = 0; 
  
  // Message type
  tmpShort.value = MSG_TYPE_FOR_SEND_BOOK_ISO_TRADING_TO_TT;  
  message.msgContent[currentIndex] = tmpShort.c[0];
  message.msgContent[currentIndex + 1] = tmpShort.c[1];
  
  currentIndex += 2;
  
  // Message length
  currentIndex += 4;
  
  // Block length
  currentIndex += 4;
  
  // Block content = Role book in the ISO trading
  
  // TS Id
  tmpInt.value = tradeServersInfo.config[tsIndex].tsId;
  message.msgContent[currentIndex] = tmpInt.c[0];
  message.msgContent[currentIndex + 1] = tmpInt.c[1];
  message.msgContent[currentIndex + 2] = tmpInt.c[2];
  message.msgContent[currentIndex + 3] = tmpInt.c[3];
  currentIndex += 4;
  
  //Book role for ISO
  memcpy(&message.msgContent[currentIndex], &tradeServersInfo.bookISO_Trading[tsIndex], TS_MAX_BOOK_CONNECTIONS);
  currentIndex += TS_MAX_BOOK_CONNECTIONS;
  
  //Pre-Post enable, disable
  memcpy(&message.msgContent[currentIndex], &tradeServersInfo.bookPrePostISO_Trading[tsIndex], TS_MAX_BOOK_CONNECTIONS);
  currentIndex += TS_MAX_BOOK_CONNECTIONS;
  
  // Block length
  tmpInt.value = currentIndex - 6;
  message.msgContent[6] = tmpInt.c[0];
  message.msgContent[7] = tmpInt.c[1];
  message.msgContent[8] = tmpInt.c[2];
  message.msgContent[9] = tmpInt.c[3];
  
  // Message length
  tmpInt.value = currentIndex - 6;
  message.msgContent[2] = tmpInt.c[0];
  message.msgContent[3] = tmpInt.c[1];
  message.msgContent[4] = tmpInt.c[2];
  message.msgContent[5] = tmpInt.c[3];
  
  // Message length
  message.msgLen = currentIndex;
  
  return SendTTMessage(pConnection, &message);
}

/****************************************************************************
- Function name:  ProcessMessageForSetBookISO_Trading
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessMessageForSetBookISO_Trading(t_TTMessage *message)
{
  int index = MSG_HEADER_LEN + 4;
  
  // TS id
  int tsId = GetIntNumber(&message->msgContent[index]);
  index += 4;
  
  // Find Trade Server index
  int tsIndex = FindTradeServerIndex(tsId);

  if (tsIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "Invalid Trade Server Id = %d\n", tsId);

    return ERROR;
  }
  
  //  Book role for ISO
  memcpy(&tradeServersInfo.bookISO_Trading[tsIndex], &message->msgContent[index], TS_MAX_BOOK_CONNECTIONS);
  index += TS_MAX_BOOK_CONNECTIONS;
  
  //  Pre/Post enable, disable
  memcpy(&tradeServersInfo.bookPrePostISO_Trading[tsIndex], &message->msgContent[index], TS_MAX_BOOK_CONNECTIONS);
  
  TraceLog(DEBUG_LEVEL, "Receive message to request %s set book role/pre-post settings for ISO algorithm from TT\n", tradeServersInfo.config[tsIndex].description);
  
  SendSetBookISO_TradingToTS(tsIndex);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendTSCurrentStatusToAllTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendTSCurrentStatusToAllTT(int tsIndex)
{
  int ttIndex;
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;

  BuildTTMessageForTSCurrentStatus(&ttMessage, tsIndex);

  // Send message to all Trader Tool connected to AS
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];

    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_TRADER_TOOL)
    {
      continue;
    }   

    // Send message to Trader Tool
    SendTTMessage(connection, &ttMessage);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendASCurrentStatusToAllTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendASCurrentStatusToAllTT()
{
  int ttIndex;
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;

  BuildTTMessageForASCurrentStatus(&ttMessage);

  // Send message to all Trader Tool connected to AS
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];

    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_TRADER_TOOL)
    {
      continue;
    }   

    // Send message to Trader Tool
    SendTTMessage(connection, &ttMessage);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendPMCurrentStatusToAllTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendPMCurrentStatusToAllTT()
{
  int ttIndex;
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;

  BuildTTMessageForPMCurrentStatus(&ttMessage);

  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];

    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_TRADER_TOOL)
    {
      continue;
    }   

    // Send message to Trader Tool
    SendTTMessage(connection, &ttMessage);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendPMExchangesConfToAllTT
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int SendPMExchangesConfToAllTT()
{
  int ttIndex;
  t_ConnectionStatus *connection;

  // Send message to all TT connected
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];

    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_TRADER_TOOL)
    {
      continue;
    }

    // Send message to Trader Tool
    SendPMExchangesConfToTT(connection);
  }

  return SUCCESS;
}


/****************************************************************************
- Function name:  SendPMExchangesConfToTT
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int SendPMExchangesConfToTT(void *pConnection)
{
  t_TTMessage message;
  t_ShortConverter tmpShort;
  t_IntConverter tmpInt;

  int currentIndex = 0;

  // Message type
  tmpShort.value = MSG_TYPE_FOR_SEND_PM_EXCHANGE_CONF_TO_TT;
  message.msgContent[currentIndex] = tmpShort.c[0];
  message.msgContent[currentIndex + 1] = tmpShort.c[1];
  currentIndex += 2;

  // Message length
  currentIndex += 4;

  // Block length
  currentIndex += 4;

  int size = 0;
  int i;
  for (i = 0; i < MAX_EXCHANGE; i++)
  {
    // exchangeId
    message.msgContent[currentIndex] = PMExchangeStatusList[i].exchangeId;
    currentIndex += 1;

    // ECNToPlace
    memcpy(&message.msgContent[currentIndex], &PMExchangeStatusList[i].ECNToPlace, MAX_LINE_LEN);
    currentIndex += MAX_LINE_LEN;

    // CQS_Status{visible + Status}
    message.msgContent[currentIndex] = PMExchangeStatusList[i].status[CQS_SOURCE].visible;
    message.msgContent[currentIndex + 1] = PMExchangeStatusList[i].status[CQS_SOURCE].enable;
    currentIndex += 2;

    // UQDF_Status{visible + Status}
    message.msgContent[currentIndex] = PMExchangeStatusList[i].status[UQDF_SOURCE].visible;
    message.msgContent[currentIndex + 1] = PMExchangeStatusList[i].status[UQDF_SOURCE].enable;
    currentIndex += 2;
  }

  size = MAX_EXCHANGE * (1 + MAX_LINE_LEN + 2 + 2);

  // Block length
  tmpInt.value = size + 4;
  message.msgContent[6] = tmpInt.c[0];
  message.msgContent[7] = tmpInt.c[1];
  message.msgContent[8] = tmpInt.c[2];
  message.msgContent[9] = tmpInt.c[3];

  // Message length
  tmpInt.value = currentIndex - 6;
  message.msgContent[2] = tmpInt.c[0];
  message.msgContent[3] = tmpInt.c[1];
  message.msgContent[4] = tmpInt.c[2];
  message.msgContent[5] = tmpInt.c[3];

  // Message length
  message.msgLen = currentIndex;

  return SendTTMessage(pConnection, &message);
}

/****************************************************************************
- Function name:  ProcessMessageForSetPMExchangeConf
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessMessageForSetPMExchangeConf(t_TTMessage *ttMessage)
{
  int currentIndex = MSG_HEADER_LEN;

  TraceLog(DEBUG_LEVEL, "Receive message to set PM exchange conf from TT\n");

  // Update current index
  currentIndex += 4;

  // Block content = PM Exchange configuration
  memcpy(PMExchangeStatusList, &ttMessage->msgContent[currentIndex], sizeof(t_PMExchangeInfo) * MAX_EXCHANGE);
  
  SetPMExchangeConfToPM();

  SendPMExchangesConfToAllDaedalus();
  SendPMExchangesConfToAllTT();

  return SUCCESS;
}


/****************************************************************************
- Function name:  ProcessMessageForGetPMBBOQuotes
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessMessageForGetPMBBOQuotes(t_TTMessage *message, int ttIndex)
{
  int currentIndex = MSG_HEADER_LEN + 4;

  char symbol[SYMBOL_LEN];
  strncpy(symbol, (char *) &message->msgContent[currentIndex], SYMBOL_LEN);

  TraceLog(DEBUG_LEVEL, "Receive request to get PM BBO quotes from TT: symbol = %.8s\n", symbol);

  // Send get PM BBO quotes to PM
  SendGetPMBBOQuotesToPM(symbol, ttIndex);

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendPMBBOQuotesToTT
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int SendPMBBOQuotesToTT(int ttIndex, void *message)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];

  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  t_ShortConverter shortNum;
  t_IntConverter intNum;

  t_TSMessage *pmMessage = (t_TSMessage *)message;
  t_TTMessage ttMessage;

  // Initialize message
  ttMessage.msgLen = 0;

  // Message type
  shortNum.value = MSG_TYPE_FOR_SEND_PM_BBO_QUOTE_LIST_TO_TT;
  ttMessage.msgContent[0] = shortNum.c[0];
  ttMessage.msgContent[1] = shortNum.c[1];
  ttMessage.msgLen += 2;

  // Update index of body length
  ttMessage.msgLen += 4;

  // PM message: MsgType(2) + BodyLen(4) + BlockLen(4) + ttIndex(1) + Symbol(8) + ASK Block + BID Block

  // TT message: MsgType(2) + BodyLen(4) + Block1Len(4) + Symbol(8) + ASK Block + BID Block
  // Message body = Block 1 + Block 2 + Block 3
  // Block 1  = Symbol
  // Block 2  = ASK Block
  // Block 3  = BID Block

  // Update index of block length
  intNum.value = SYMBOL_LEN + 4;
  ttMessage.msgContent[ttMessage.msgLen] = intNum.c[0];
  ttMessage.msgContent[ttMessage.msgLen + 1] = intNum.c[1];
  ttMessage.msgContent[ttMessage.msgLen + 2] = intNum.c[2];
  ttMessage.msgContent[ttMessage.msgLen + 3] = intNum.c[3];
  ttMessage.msgLen += 4;
  
  // Block 1 = Symbol (8 bytes)
  memcpy(&ttMessage.msgContent[ttMessage.msgLen], &pmMessage->msgContent[MSG_HEADER_LEN + 4 + 1], SYMBOL_LEN);
  ttMessage.msgLen += SYMBOL_LEN;

  // ASK Block + BID Block
  memcpy(&ttMessage.msgContent[ttMessage.msgLen], &pmMessage->msgContent[MSG_HEADER_LEN + 4 + 1 + SYMBOL_LEN], pmMessage->msgLen - (MSG_HEADER_LEN + 4 + 1 + SYMBOL_LEN));
  ttMessage.msgLen += pmMessage->msgLen - (MSG_HEADER_LEN + 4 + 1 + SYMBOL_LEN);

  // Body length field
  intNum.value = ttMessage.msgLen - MSG_HEADER_LEN;
  ttMessage.msgContent[2] = intNum.c[0];
  ttMessage.msgContent[3] = intNum.c[1];
  ttMessage.msgContent[4] = intNum.c[2];
  ttMessage.msgContent[5] = intNum.c[3];

  // Send message to Trader Tool
  return SendTTMessage(connection, &ttMessage);
}

/****************************************************************************
- Function name:  ProcessMessageForFlushPMBBOQuotes
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessMessageForFlushPMBBOQuotes(t_TTMessage *message)
{
  int currentIndex = MSG_HEADER_LEN + 4;

  // Exchange id
  int exchangeId = message->msgContent[currentIndex];

  TraceLog(DEBUG_LEVEL, "Received message to flush PM BBO quotes from TT: ExchangeID = %c\n", exchangeId);

  SendFlushPMBBOQuotesToPM(exchangeId);

  return SUCCESS;
}

/***************************************************************************/

/****************************************************************************
- Function name:  ProcessMessageForLogTTVersionControl
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/ 
int ProcessMessageForLogTTVersionControl(t_TTMessage *message, int ttIndex)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;
  
  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;
  
  char version[11];
  strncpy(version, (char *) &ttMessage->msgContent[currentIndex], 10);
  version[10] = 0;
  
  currentIndex += 10;

  TraceLog(DEBUG_LEVEL, "[%s]TT's version: [%.10s]\n", traderToolsInfo.connection[ttIndex].userName, version);

  char reason[128] = "\0";
  int cmpRet = strncmp(version, moduleVersionMgmt.ttVersion.version, 10);
  if (cmpRet != 0)
  {
    TraceLog(DEBUG_LEVEL, "================================\n");
    TraceLog(DEBUG_LEVEL, "TT lastest version info: \n");
    TraceLog(DEBUG_LEVEL, "Version: %.10s\n", moduleVersionMgmt.ttVersion.version);
    TraceLog(DEBUG_LEVEL, "Release date: %.10s\n", moduleVersionMgmt.ttVersion.date);
    TraceLog(DEBUG_LEVEL, "Description: %s\n", moduleVersionMgmt.ttVersion.description);
    TraceLog(DEBUG_LEVEL, "================================\n");

    sprintf(reason, "%d\t%s", cmpRet, moduleVersionMgmt.ttVersion.version);
    BuildNSendTTOutOfDateAlertToTT(reason, ttIndex);
    
    isValidTTVersion[ttIndex] = -1;
  }
  else
  {
    isValidTTVersion[ttIndex] = 1;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessMessageForAuthenticateLoginInfo
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessMessageForAuthenticateLoginInfo(t_TTMessage *message, int ttIndex)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;
  
  int currentIndex = MSG_HEADER_LEN + 4;
  
  // userName
  memcpy((unsigned char *)&traderToolsInfo.connection[ttIndex].userName, &ttMessage->msgContent[currentIndex], MAX_LINE_LEN);
  currentIndex += MAX_LINE_LEN;
  
  // password
  memcpy((unsigned char *)&traderToolsInfo.connection[ttIndex].password, &ttMessage->msgContent[currentIndex], MAX_LINE_LEN);
  currentIndex += MAX_LINE_LEN;
  
  // Role 
  int status = GetIntNumber(&ttMessage->msgContent[currentIndex]);
  currentIndex += 4;
  
  // Authentication Method: LDAP or Normal
  // LDAP method: 0
  // Non-LDAP:  1
  traderToolsInfo.connection[ttIndex].authenticationMethod = ttMessage->msgContent[currentIndex];
  currentIndex += 1;
  
  TraceLog(DEBUG_LEVEL, "authenticationMethod=%d\n", traderToolsInfo.connection[ttIndex].authenticationMethod);
  if (traderToolsInfo.connection[ttIndex].authenticationMethod == LDAP_AUTHENTICATION_METHOD)
  {
    //Password: 80 bytes but do not use the first 8 bytes, so the real password length is 72 bytes
    char encodedPassword[MAX_LINE_LEN] = "\0";
    strncpy(encodedPassword, &traderToolsInfo.connection[ttIndex].password[8], MAX_LINE_LEN - 8);
    int len = strlen(encodedPassword);
    unsigned char decodedPassword[MAX_PATH_LEN];
    int outputLength = DecodeBase64(encodedPassword, len, decodedPassword);
    if (outputLength == -1)
    {
      TraceLog(ERROR_LEVEL, "Can not decode password\n");
    }
    memcpy(traderToolsInfo.connection[ttIndex].password, decodedPassword, MAX_LINE_LEN);
  }

  //Update current state of trader
  traderToolsInfo.connection[ttIndex].currentState = status;
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendMessageForPermitTraderLoginToTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendMessageForPermitTraderLoginToTT(int ttIndex, char trader[MAX_LINE_LEN], int state)
{
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;

  // Build AS setting message to send Trader Tool
  BuildTTMessageForPermitTraderLogin(&ttMessage, trader, state);

  // Send message to all Trader Tool connected to AS
  
  connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];
  
  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  // Send message to Trader Tool
  return SendTTMessage(connection, &ttMessage);
}

/****************************************************************************
- Function name:  BuildTTMessageForPermitTraderLogin
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTTMessageForPermitTraderLogin(void *message, char trader[MAX_LINE_LEN], int state)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TTMessage *ttMessage = (t_TTMessage *)message;
  
  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_MESSAGE_PERMIT_TRADE_LOGIN_TO_TT;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1];
  ttMessage->msgLen += 2;

  // Update index of body length
  ttMessage->msgLen += 4;

  // Message body 
  // Update index of block length
  ttMessage->msgLen += 4;

  // Block content = trader(80 bytes) + state(4 bytes
  
  // trader
  strncpy((char *) &ttMessage->msgContent[ttMessage->msgLen], trader, MAX_LINE_LEN);
  ttMessage->msgLen += MAX_LINE_LEN;
  
  // state
  intConvert.value = state;
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], intConvert.c, 4);
  ttMessage->msgLen += 4;
  
  // Block length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  ttMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  ttMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  ttMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[2] = intConvert.c[0];
  ttMessage->msgContent[3] = intConvert.c[1];
  ttMessage->msgContent[4] = intConvert.c[2];
  ttMessage->msgContent[5] = intConvert.c[3];

  return ttMessage->msgLen;
}

/****************************************************************************
- Function name:  SendTraderStatusInfoToAllTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendTraderStatusInfoToAllTT(int type)
{
  int ttIndex;
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;

  // Build trader status info message to send Trader Tool
  BuildTTMessageForTraderStatusInfo(&ttMessage, type);

  // Send message to all Trader Tool connected to AS
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];
    
    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_TRADER_TOOL)
    {
      continue;
    }

    // Send message to Trader Tool
    SendTTMessage(connection, &ttMessage);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  BuildTTMessageForTraderStatusInfo
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTTMessageForTraderStatusInfo(void *message, int type)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TTMessage *ttMessage = (t_TTMessage *)message;
  
  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_TRADER_STATUS_INFO_TO_TT;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1];
  ttMessage->msgLen += 2;

  // Update index of body length
  ttMessage->msgLen += 4;

  // Message body = n * (blockLength (4 bytes) + blockContent (blockLength bytes))
  int traderIndex;

  for (traderIndex = 0; traderIndex < traderCollection.numberTrader; traderIndex++)
  {
    // Update index of block length
    ttMessage->msgLen += 4;

    // Block content = Trade Server configuration
    int size = MAX_LINE_LEN + 4;  //username + status

    memcpy(&ttMessage->msgContent[ttMessage->msgLen], (unsigned char *)traderCollection.traderStatusInfo[traderIndex].userName, MAX_LINE_LEN);
    ttMessage->msgLen += MAX_LINE_LEN;
    
    memcpy(&ttMessage->msgContent[ttMessage->msgLen], (unsigned char *)&traderCollection.traderStatusInfo[traderIndex].status, 4);
    ttMessage->msgLen += 4;

    // Block length field
    intConvert.value = size + 4;
    ttMessage->msgContent[ttMessage->msgLen - size - 4] = intConvert.c[0];
    ttMessage->msgContent[ttMessage->msgLen - size - 3] = intConvert.c[1];
    ttMessage->msgContent[ttMessage->msgLen - size - 2] = intConvert.c[2];
    ttMessage->msgContent[ttMessage->msgLen - size - 1] = intConvert.c[3];
  }
  
  //type for notify TT when active trader switch to monitoring or disconnect
  //if the manual a trader switch to active trader --> type = 0
  //if the active assist automatic switch to active trader --> type = 1
  //if all trader in monitoring --> type = 2
  intConvert.value = type;
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], intConvert.c, 4);
  ttMessage->msgLen += 4;
  
  // Body length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[2] = intConvert.c[0];
  ttMessage->msgContent[3] = intConvert.c[1];
  ttMessage->msgContent[4] = intConvert.c[2];
  ttMessage->msgContent[5] = intConvert.c[3];

  return ttMessage->msgLen;
}


/****************************************************************************
- Function name:  ProcessMessageForSwitchTraderRole
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessMessageForSwitchTraderRole(t_TTMessage *message, int ttIndex)
{
  char userName[MAX_LINE_LEN] = "\0";
  
  t_TTMessage *ttMessage = (t_TTMessage *)message;
  
  int currentIndex = MSG_HEADER_LEN + 4;
  
  // userName
  memcpy(userName, &ttMessage->msgContent[currentIndex], MAX_LINE_LEN);
  currentIndex += MAX_LINE_LEN;
  
  // status
  int status = GetIntNumber(&message->msgContent[currentIndex]);
  currentIndex += 4;

  TraceLog(DEBUG_LEVEL, "[%s]Change trader state from %d to %d\n", traderToolsInfo.connection[ttIndex].userName, traderToolsInfo.connection[ttIndex].currentState, status);

  char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
  sprintf(activityDetail, "%d,%d", traderToolsInfo.connection[ttIndex].currentState, status);
  ProcessUpdateTTEventLogToDatabase(userName, ACTIVITY_TYPE_SWITCH_ROLE, activityDetail);
  
  //Update trader status to monitoring
  ProcessUpdateTraderStatusToDatabase(userName, status);
  
  //Update current state of trader
  traderToolsInfo.connection[ttIndex].currentState = status;
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendNumberOfSymbolsWithQuotesToTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendNumberOfSymbolsWithQuotesToTT(int ttIndex, int tsIndex, int numberOfSymbolsWithQuotes[TS_MAX_BOOK_CONNECTIONS])
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];

  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  t_TTMessage ttMessage;

  // Build number of symbols with quotes message to send Trader Tool
  BuildTTMessageForNumberOfSymbolsWithQuotes(&ttMessage, tsIndex, numberOfSymbolsWithQuotes);

  // Send message to Trader Tool
  return SendTTMessage(connection, &ttMessage);
}

/****************************************************************************
- Function name:  BuildTTMessageForNumberOfSymbolsWithQutoes
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTTMessageForNumberOfSymbolsWithQuotes(void *message, int tsIndex, int numberOfSymbolsWithQuotes[TS_MAX_BOOK_CONNECTIONS])
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TTMessage *ttMessage = (t_TTMessage *)message;
  
  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_NUMBER_OF_SYMBOLS_WITH_QUOTES_TO_TT;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1];
  ttMessage->msgLen += 2;

  // Update index of body length
  ttMessage->msgLen += 4;

  // Update index of block length
  ttMessage->msgLen += 4;

  // Updated by: DUNGTD
  // Block content: TS id (4) + TS_MAX_BOOK_CONNECTIONS * 4
    
  // TS ID
  intConvert.value = tsIndex;
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], intConvert.c, 4);
  ttMessage->msgLen += 4;
  
  int ecnIndex;
  for (ecnIndex = 0; ecnIndex < TS_MAX_BOOK_CONNECTIONS; ecnIndex++)
  {
    // Number of Symbols with quotes
    intConvert.value = numberOfSymbolsWithQuotes[ecnIndex];
    memcpy(&ttMessage->msgContent[ttMessage->msgLen], intConvert.c, 4);
    ttMessage->msgLen += 4;
  }

  // Block length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  ttMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  ttMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  ttMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[2] = intConvert.c[0];
  ttMessage->msgContent[3] = intConvert.c[1];
  ttMessage->msgContent[4] = intConvert.c[2];
  ttMessage->msgContent[5] = intConvert.c[3];

  return ttMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildNSendTTOutOfDateAlertToTT
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildNSendTTOutOfDateAlertToTT(char *reason, int ttIndex)
{
  char alert[128];
  sprintf(alert, "%d\t%s", TT_OUT_OF_DATE_MESSAGE_TYPE, reason);
  
  return BuildNSendAlertToTT(alert, ttIndex);
}

/****************************************************************************
- Function name:  SendRejectCodeToATT
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int SendRejectCodeToATT(t_ConnectionStatus *connection)
{
  int i;
  for(i = 0; i < MAX_ORDER_TYPE; i++)
  {
    BuildNSendRejectCodeToATT(i + TYPE_BASE, connection);
  }
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendRejectCodeToAllTT
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int SendRejectCodeToAllTT(void)
{
  int i;
  for(i = 0; i < MAX_ORDER_TYPE; i++)
  {
    BuildNSendRejectCodeToAllTT(i + TYPE_BASE);
  }
  return SUCCESS;
}
/****************************************************************************
- Function name:  BuildMsgRejectCodeToTT
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildMsgRejectCodeToTT(int ECNType, t_TTMessage *ttMessage)
{
  // Initialize message
  ttMessage->msgLen = 0;

  // Message type
  PutShortNumberV1(MSG_TYPE_FOR_SEND_REJECT_CODE_TO_TT, &ttMessage->msgContent[ttMessage->msgLen], &ttMessage->msgLen);

  // Update index of body length
  ttMessage->msgLen += 4;

  // Message body = n * (blockLength (4 bytes) + blockContent (blockLength bytes))
  // Update index of block length
  ttMessage->msgLen += 4;

  // ECNType
  PutIntNumberV1(ECNType, &ttMessage->msgContent[ttMessage->msgLen], &ttMessage->msgLen);

  int ECNIndex = ECNType - TYPE_BASE;

  t_DisableRejectMgmt *reject = &DisableRejectMgmt[ECNIndex];

  // num of reject
  PutIntNumberV1(reject->countReason, &ttMessage->msgContent[ttMessage->msgLen], &ttMessage->msgLen);

  int i;

  for(i = 0; i < reject->countReason; i++)
  {
    // alert flag
    PutIntNumberV1(reject->disableAlert[i], &ttMessage->msgContent[ttMessage->msgLen], &ttMessage->msgLen);

    // reject code
    PutStringV1(reject->reasonCode[i], &ttMessage->msgContent[ttMessage->msgLen], REJECT_CODE_LEN, &ttMessage->msgLen);

    // reject reason
    PutStringVariableLenV1(reject->reasonText[i], &ttMessage->msgContent[ttMessage->msgLen], &ttMessage->msgLen);
  }

  if(ttMessage->msgLen > TT_MAX_MSG_LEN)
  {
    TraceLog(ERROR_LEVEL, "(BuildMsgRejectCodeToTT) msg Len (%d) is over of boundary\n", ttMessage->msgLen);
    return ERROR;
  }

  // Body length field
  int temp = ttMessage->msgLen - MSG_HEADER_LEN;

  memcpy(&ttMessage->msgContent[MSG_HEADER_LEN], &temp, 4);

  memcpy(&ttMessage->msgContent[2], &temp, 4);

  return SUCCESS;
}

/****************************************************************************
- Function name:  BuildNSendRejectCodeToATT
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildNSendRejectCodeToATT(int ECNType, t_ConnectionStatus *connection)
{
  t_TTMessage ttMessage;

  BuildMsgRejectCodeToTT(ECNType, &ttMessage);

  return SendTTMessage(connection, &ttMessage);
}

/****************************************************************************
- Function name:  BuildNSendRejectCodeToAllTT
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildNSendRejectCodeToAllTT(int ECNType)
{
  t_TTMessage ttMessage;

  // Initialize message
  ttMessage.msgLen = 0;

  // Message type
  PutShortNumberV1(MSG_TYPE_FOR_SEND_REJECT_CODE_TO_TT, &ttMessage.msgContent[ttMessage.msgLen], &ttMessage.msgLen);

  // Update index of body length
  ttMessage.msgLen += 4;

  // Message body = n * (blockLength (4 bytes) + blockContent (blockLength bytes))
  // Update index of block length
  ttMessage.msgLen += 4;

  // ECNType
  PutIntNumberV1(ECNType, &ttMessage.msgContent[ttMessage.msgLen], &ttMessage.msgLen);

  int ECNIndex = ECNType - TYPE_BASE;

  t_DisableRejectMgmt *reject = &DisableRejectMgmt[ECNIndex];

  // num of reject
  PutIntNumberV1(reject->countReason, &ttMessage.msgContent[ttMessage.msgLen], &ttMessage.msgLen);

  int i;

  for(i = 0; i < reject->countReason; i++)
  {
    // alert flag
    PutIntNumberV1(reject->disableAlert[i], &ttMessage.msgContent[ttMessage.msgLen], &ttMessage.msgLen);

    // reject code
    PutStringV1(reject->reasonCode[i], &ttMessage.msgContent[ttMessage.msgLen], REJECT_CODE_LEN, &ttMessage.msgLen);

    // reject reason
    PutStringVariableLenV1(reject->reasonText[i], &ttMessage.msgContent[ttMessage.msgLen], &ttMessage.msgLen);
  }

  if(ttMessage.msgLen > TT_MAX_MSG_LEN)
  {
    TraceLog(ERROR_LEVEL, "(BuildNSendRejectCodeToAllTT) msg Len (%d) is over of boundary\n", ttMessage.msgLen);
    return ERROR;
  }

  // Body length field
  int temp = ttMessage.msgLen - MSG_HEADER_LEN;

  memcpy(&ttMessage.msgContent[MSG_HEADER_LEN], &temp, 4);

  memcpy(&ttMessage.msgContent[2], &temp, 4);

  return SendMessageToAllTT(&ttMessage);

}

/****************************************************************************
- Function name:  ProcessTTMessageForSetDisabledAlertRejectCodeFromTT
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessTTMessageForSetDisabledAlertRejectCodeFromTT(void *message)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;

  int curIndex = 10;

  int ECNType = GetIntNumberV1(&ttMessage->msgContent[curIndex], &curIndex);

  int ECNIndex = ECNType - TYPE_BASE;

  t_DisableRejectMgmt *reject = &DisableRejectMgmt[ECNIndex];

  int i, j;

  for(i = 0; i < reject->countReason; i++)
  {
    reject->disableAlert[i] = NO;
  }

  int count = GetIntNumberV1(&ttMessage->msgContent[curIndex], &curIndex);
  char rejectCode[REJECT_CODE_LEN];
  int existed;
  for(i = 0; i < count; i++)
  {
    existed = NO;
    GetStringV1(rejectCode, &ttMessage->msgContent[curIndex], REJECT_CODE_LEN, &curIndex);

    for(j = 0; j < reject->countReason; j++)
    {
      if(strcmp(reject->reasonCode[j], rejectCode) == 0)
      {
        reject->disableAlert[j] = YES;

        existed = YES;
        break;
      }
    }
    if(existed == NO)
    {
      TraceLog(ERROR_LEVEL, "(ProcessTTMessageForSetDisabledAlertRejectCodeFromTT) Invalid reject code = %s\n", rejectCode);
      return ERROR;
    }
  }

  BuildNSendRejectCodeToAllTT(ECNType);

  return SaveRejectCodeToFileOfAOrder(ECNType);

}

/****************************************************************************
- Function name:  SendActiveVolatileSymbolsToTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendActiveVolatileSymbolsToTT(t_ConnectionStatus *connection, int currentVolatileSession)
{
  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  t_TTMessage ttMessage;

  BuildTTMessageForActiveVolatileSymbolList(&ttMessage, currentVolatileSession);
  
  // Send message to Trader Tool
  return SendTTMessage(connection, &ttMessage);
}

/****************************************************************************
- Function name:  BuildTTMessageForActiveVolatileSymbolList
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTTMessageForActiveVolatileSymbolList(void *message, int currentVolatileSession)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TTMessage *ttMessage = (t_TTMessage *)message;
  
  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_ACTIVE_VOLATILE_SESSION_TO_TT;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1];
  ttMessage->msgLen += 2;

  // Update index of body length
  ttMessage->msgLen += 4;

  ttMessage->msgLen += 4;

  memcpy(&ttMessage->msgContent[ttMessage->msgLen], &currentVolatileSession, 4);
  ttMessage->msgLen += 4;
  
  // Block length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  ttMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  ttMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  ttMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  ttMessage->msgContent[2] = intConvert.c[0];
  ttMessage->msgContent[3] = intConvert.c[1];
  ttMessage->msgContent[4] = intConvert.c[2];
  ttMessage->msgContent[5] = intConvert.c[3];

  return ttMessage->msgLen;
}

/****************************************************************************
- Function name:  SendAllVolatileToggleStatusToTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendAllVolatileToggleStatusToTT(t_ConnectionStatus *connection)
{
  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  t_TTMessage ttMessage;

  BuildTTMessageForAllVolatileToggleStatus(&ttMessage);
  
  // Send message to Trader Tool
  return SendTTMessage(connection, &ttMessage);
}

/****************************************************************************
- Function name:  BuildTSMessageForAllVolatileToggleStatus
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTTMessageForAllVolatileToggleStatus(void *message)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TTMessage *ttMessage = (t_TTMessage *)message;
  
  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_ACTIVE_VOLATILE_TOGGLE_TO_TT;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1];
  ttMessage->msgLen += 2;

  // Update index of body length
  ttMessage->msgLen += 4;

  /* Message body: 1 byte status (0: off, 1: on) */
  ttMessage->msgLen += 4;
  
  ttMessage->msgContent[ttMessage->msgLen] = volatileMgmt.currentAllVolatileValue;
  ttMessage->msgLen ++;
  
  // Block length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  ttMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  ttMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  ttMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  ttMessage->msgContent[2] = intConvert.c[0];
  ttMessage->msgContent[3] = intConvert.c[1];
  ttMessage->msgContent[4] = intConvert.c[2];
  ttMessage->msgContent[5] = intConvert.c[3];

  return ttMessage->msgLen;
}

/****************************************************************************
- Function name:  SendSleepingSymbolListtoTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendSleepingSymbolListtoTT(t_ConnectionStatus *connection)
{
  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  t_TTMessage ttMessage;

  BuildTTMessageForSleepingSymbolList(&ttMessage);
  
  // Send message to Trader Tool
  return SendTTMessage(connection, &ttMessage);
}

/****************************************************************************
- Function name:  BuildTTMessageForSleepingSymbolList
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTTMessageForSleepingSymbolList(void *message)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TTMessage *ttMessage = (t_TTMessage *)message;
  
  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_SLEEPING_SYMBOL_LIST_TO_TT;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1];
  ttMessage->msgLen += 2;

  // Index of body length
  ttMessage->msgLen += 4;

  // Index of block length
  ttMessage->msgLen += 4;
  
  // Index of number of symbols
  ttMessage->msgLen += 4;
  
  int account, symbolIndex;
  int numOfSymbols = 0;
  int countSymbol = RMList.countSymbol;
  for (account = 0; account < MAX_ACCOUNT; account++)
  {
    for (symbolIndex = 0; symbolIndex < countSymbol; symbolIndex++)
    {
      if (verifyPositionInfo[account][symbolIndex].isTrading == TRADING_AUTOMATIC &&
        verifyPositionInfo[account][symbolIndex].isSleeping == YES)
      {
        numOfSymbols++;
        
        // Symbol
        memcpy(&ttMessage->msgContent[ttMessage->msgLen], &RMList.symbolList[symbolIndex].symbol, SYMBOL_LEN);
        ttMessage->msgLen += SYMBOL_LEN;
        
        // Account
        ttMessage->msgContent[ttMessage->msgLen] = account;
        ttMessage->msgLen += 1;
        
        DEVLOGF("Sleeping symbol list: %d, symbol = %.8s, account = %d",
          numOfSymbols, RMList.symbolList[symbolIndex].symbol, account);
      }
    }
  }
  if (numOfSymbols > 0)
  {
    DEVLOGF("Sleeping symbol list: numOfSymbols = %d", numOfSymbols);
  }
  
  // Number of symbols
  memcpy(&ttMessage->msgContent[10], &numOfSymbols, 4);
  
  // Block length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  ttMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  ttMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  ttMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  ttMessage->msgContent[2] = intConvert.c[0];
  ttMessage->msgContent[3] = intConvert.c[1];
  ttMessage->msgContent[4] = intConvert.c[2];
  ttMessage->msgContent[5] = intConvert.c[3];

  return ttMessage->msgLen;
}

int UpdateTimeInForce(t_SendNewOrder *newOrder)
{
  if(newOrder->tif == IOC_TYPE)
  {
    newOrder->tif = 0;
  }
  else
  {
    newOrder->tif = 86400;
  }
  
  return SUCCESS;
}
