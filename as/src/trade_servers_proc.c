/*****************************************************************************
**  Project:    Equity Arbitrage Application
**  Filename:   trade_servers_proc.c
**  Description:  This file contains function definitions that were declared
          in trade_servers_proc.h 
**  Author:     Luan Vo-Kinh
**  First created:  13-Sep-2007
**  Last updated: -----------
*****************************************************************************/

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#define _GNU_SOURCE
#include <string.h>
#include <math.h>
#include <errno.h>

#include "configuration.h"
#include "socket_util.h"
#include "raw_data_mgmt.h"
#include "output_log_mgmt.h"
#include "position_manager_proc.h"
#include "trade_servers_mgmt_proc.h"
#include "daedalus_proc.h"
#include "trader_tools_proc.h"
#include "trade_servers_proc.h"
#include "database_util.h"
#include "hbitime.h"

int MaxStuckOfASAlert = NO;
int MaxLossOfASAlert = NO;
int TWTRequestAlert = NO;

int IsMaxBPUpdated = NO;
extern int TTIndexUpdatedBP;
/****************************************************************************
** Global variables definition
****************************************************************************/
// For connect/disconnect ECN Book
#define MSG_TYPE_FOR_SEND_CONNECT_BOOK_TO_TS 30001
#define MSG_TYPE_FOR_SEND_DISCONNECT_BOOK_TO_TS 30003

// For connect/disconnect ECN Book
#define MSG_TYPE_FOR_SEND_CONNECT_ORDER_TO_TS 30005
#define MSG_TYPE_FOR_SEND_DISCONNECT_ORDER_TO_TS 30007

// For enable/disable trading
#define MSG_TYPE_FOR_SEND_ENABLE_TRADING_TO_TS 30009
#define MSG_TYPE_FOR_SEND_DISABLE_TRADING_TO_TS 30011

// For ECN Book configuration
#define MSG_TYPE_FOR_RECEIVE_BOOK_CONF_FROM_TS 46002
#define MSG_TYPE_FOR_SET_BOOK_CONF_FROM_TS 32001

// For ECN Order configuration
#define MSG_TYPE_FOR_RECEIVE_ORDER_CONF_FROM_TS 46003
#define MSG_TYPE_FOR_SET_ORDER_CONF_FROM_TS 32002

// For TS global configuration
#define MSG_TYPE_FOR_RECEIVE_GLOBAL_CONF_FROM_TS 46004
#define MSG_TYPE_FOR_SET_GLOBAL_CONF_FROM_TS 32003

// For current status information
#define MSG_TYPE_FOR_RECEIVE_CURRENT_STATUS_FROM_TS 46005

// For stock symbol status
#define MSG_TYPE_FOR_SEND_GET_SYMBOL_STATUS_TO_TS 31001
#define MSG_TYPE_FOR_RECEIVE_SYMBOL_STATUS_FROM_TS 46001

// For number of symbols with quotes
#define MSG_TYPE_FOR_SEND_GET_NUMBER_OF_SYMBOLS_WITH_QUOTES_TO_TS 31002
#define MSG_TYPE_FOR_RECEIVE_NUMBER_OF_SYMBOLS_WITH_QUOTES_FROM_TS 46007

// For fulsh book
#define MSG_TYPE_FOR_SEND_FLUSH_A_SYMBOL_TO_TS 32004
#define MSG_TYPE_FOR_SEND_FLUSH_ALL_SYMBOL_TO_TS 32005

// For reset current status
#define MSG_TYPE_FOR_SEND_RESET_CURRENT_STATUS_TO_TS 32006
#define MSG_TYPE_FOR_SEND_UPDATE_CURRENT_STATUS_TO_TS 32007

// For send symbol ignored list
#define MSG_TYPE_FOR_SEND_IGNORE_SYMBOL_LIST_TO_TS 32008
#define MSG_TYPE_FOR_SET_IGNORE_SYMBOL_LIST_TO_TS 32009

// For send symbol ignored ranges list
#define MSG_TYPE_FOR_SEND_IGNORE_SYMBOL_RANGES_LIST_TO_TS 32012
#define MSG_TYPE_FOR_SET_IGNORE_SYMBOL_RANGES_LIST_TO_TS 32013

#define MSG_TYPE_FOR_SEND_VOLATILE_SYMBOL_LIST_TO_TS 32020
#define MSG_TYPE_FOR_SEND_ALL_VOLATILE_VALUE_TO_TS 32021

// For ETB symbol list
#define MSG_TYPE_FOR_SEND_ETB_SYMBOL_LIST_TO_TS 32022

// For trading account
#define MSG_TYPE_FOR_SEND_TRADING_ACCOUNT_TO_TS 32030

// For Hung Order
#define MSG_TYPE_FOR_SEND_RESET_HUNG_ORDER_TO_TS 32025

#define MSG_TYPE_FOR_SET_ARCA_BOOK_FEED_FROM_TS 32014
#define MSG_TYPE_FOR_SET_STUCK_STATUS_FOR_SYMBOL 32015
#define MSG_TYPE_FOR_SET_ALL_STUCK_SYMBOLS_TO_TS 32016
#define MSG_TYPE_FOR_SET_LOCK_COUNTER_FOR_SYMBOL 32018

// Price Update Request
#define MSG_TYPE_FOR_PRICE_UPDATE_REQUEST_TO_TS 32024
#define MSG_TYPE_FOR_RECEIVE_PRICE_UPDATE_FROM_TS 46020

// For trading halt
#define MSG_TYPE_FOR_SYMBOL_HALT_STATUS_FROM_TS 46019
#define MSG_TYPE_FOR_SEND_HALTED_SYMBOL_LIST_TO_TS 32010
#define MSG_TYPE_FOR_SEND_A_HALTED_SYMBOL_TO_TS 32011

// For raw data
#define MSG_TYPE_FOR_SEND_GET_RAW_DATA_TO_TS 31006
#define MSG_TYPE_FOR_RECEIVE_RAW_DATA_FROM_TS 46006

// For output log
#define MSG_TYPE_FOR_SEND_GET_OUTPUT_LOG_TO_TS 31007
#define MSG_TYPE_FOR_RECEIVE_OUTPUT_LOG_FROM_TS 46008
#define MSG_TYPE_FOR_SEND_GET_BBO_INFO_TO_AS 31009

#define MSG_TYPE_FOR_SEND_ARCA_DIRECT_CACNCEL_REQUEST_TO_TS 31008

#define MSG_TYPE_FOR_RECEIVE_BOOK_IDLE_WARNING_FROM_TS 46009

#define MSG_TYPE_FOR_RECEIVE_BBO_INFO_FROM_TS 46010
// For send heartbeat message
#define MSG_TYPE_FOR_SEND_HEARTBEAT_TO_TS 31020

// Connect/disconnect to CQS/UQDF
// CQS/UQDF: 1 byte; 0 for CQS, and 1 for UQDF
#define MSG_TYPE_FOR_REQUEST_CONNECT_TO_BBO_TO_TS     30100
#define MSG_TYPE_FOR_REQUEST_DISCONNECT_TO_BBO_TO_TS  30101

// Enable/disable ISO algorithm
#define MSG_TYPE_FOR_SET_ISO_TRADING_TO_TS      30102

// Get/set exchange configuration
// EXCHANGE_CONF
#define MSG_TYPE_FOR_SET_EXCHANGE_CONF_TO_TS      32100
#define MSG_TYPE_FOR_RECEIVE_EXCHANGE_CONF_FROM_TS    46100

// Get BBO quotes
// symbol: 8 bytes
#define MSG_TYPE_FOR_GET_BBO_QUOTE_LIST_TO_TS       31100
#define MSG_TYPE_FOR_RECEIVE_BBO_QUOTE_LIST_FROM_TS   46101

// Flush BBO quote for a exchange
// exchange id: 1 byte
#define MSG_TYPE_FOR_SEND_FLUSH_BBO_QUOTES_TO_TS    32101

// Books for ISO Trading
#define MSG_TYPE_FOR_SET_BOOK_ISO_TRADING_TO_TS     32102
#define MSG_TYPE_FOR_RECEIVE_BOOK_ISO_TRADING_FROM_TS 46102

// For TS minimum spread conf
#define MSG_TYPE_FOR_SET_MIN_SPREAD_CONF_TO_TS      32019
#define MSG_TYPE_FOR_RECEIVE_MIN_SPREAD_CONF_FROM_TS  46012

#define MSG_TYPE_FOR_RECEIVE_DISCONNECT_ALERT_FROM_TS       46015
#define MSG_TYPE_FOR_RECEIVE_DISABLED_TRADING_ALERT_FROM_TS   46016
#define MSG_TYPE_FOR_RECEIVE_MARKET_STALE_ALERT_FROM_TS     46017

#define MSG_TYPE_FOR_RECEIVE_GROUP_OF_BOOK_DATA_UNHANDLED_ALERT_FROM_TS 46018

// For TS Split Symbol configuration
#define MSG_TYPE_FOR_RECEIVE_SPLIT_SYMBOL_CONF_FROM_TS 46013

int countCurrentStatusReceived[MAX_TRADE_SERVER_CONNECTIONS];

double positionBuyingPower[MAX_ACCOUNT] = {0.00, 0.00, 0.00, 0.00};
double tsBuyingPower[MAX_ACCOUNT] = {0.00, 0.00, 0.00, 0.00};

t_BackUpGlobalConf BackupTSGlobalConf[MAX_TRADE_SERVER_CONNECTIONS];

// This flag turn on when global config of TS is received
// Purpose: to send updated connection status and server role at the same time to Daedalus

extern t_VolatileMgmt volatileMgmt;
/****************************************************************************
** Function declarations
****************************************************************************/

/****************************************************************************
- Function name:  ProcessTradeServer
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
void *ProcessTradeServer(void *pthread_arg)
{
  int sock = -1, tsIndex;

  tsIndex = *((int *)pthread_arg);
  
  //Connect to Trade server
  TraceLog(DEBUG_LEVEL, "Trying to connect to '%s'...\n", tradeServersInfo.config[tsIndex].description);
  
  //initialize Output log processing status
  memset(&OutputLogProcessingStatus[tsIndex], 0, sizeof(t_OutputLogProcessingStatus));
  
  sock = Connect2Server(tradeServersInfo.config[tsIndex].ip, tradeServersInfo.config[tsIndex].port, "Trade Server");  

  if ((sock > 0) && (sock < 65535))
  { 
    TraceLog(DEBUG_LEVEL, "Connected successfully to %s\n", tradeServersInfo.config[tsIndex].description);

    struct timeval tv;
    tv.tv_sec = 3;
    tv.tv_usec = 0;
    
    socklen_t optlen = sizeof(tv);
    
    //Set receive timeout
    if (setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv, optlen) == ERROR)
    {
      TraceLog(ERROR_LEVEL, "Cannot set receive timeout for %s socket\n", tradeServersInfo.config[tsIndex].description);
      shutdown(sock, 2);
      close(sock);
      tradeServersInfo.connection[tsIndex].socket = -1;
      return NULL;
    }
    
    //Set sendtimeout
    tv.tv_sec = 3;
    tv.tv_usec = 0;
    if (setsockopt(sock, SOL_SOCKET, SO_SNDTIMEO, &tv, optlen) == ERROR)
    {
      TraceLog(ERROR_LEVEL, "Cannot set send-timeout for %s socket\n", tradeServersInfo.config[tsIndex].description);
      shutdown(sock, 2);
      close(sock);
      tradeServersInfo.connection[tsIndex].socket = -1;
      return NULL;
    }
    
    if (getsockopt(sock, SOL_SOCKET, SO_SNDTIMEO, &tv, &optlen) == ERROR)
    {
      TraceLog(ERROR_LEVEL, "Cannot get send-timeout for %s socket (%d)\n", tradeServersInfo.config[tsIndex].description, sock);
    }
    else
    {
      TraceLog(DEBUG_LEVEL, "getsockopt with SO_SNDTIMEO for %s socket descriptor %d: %d(s) + %d(us)\n", tradeServersInfo.config[tsIndex].description, sock, (long)tv.tv_sec, (long)tv.tv_usec);
    }

    // Update Trade Servers information
    tradeServersInfo.connection[tsIndex].status = CONNECT;
    tradeServersInfo.connection[tsIndex].socket = sock;

    // Initialize buying power
    tsBuyingPower[(int)TradingAccount.accountForTS[tsIndex]] = 0.00;

    // Update buying power of TS
    IsMaxBPUpdated = YES;
    TTIndexUpdatedBP = -1;
    SendUpdateBuyingPowerToAllTS(TradingAccount.accountForTS[tsIndex]);

    //Aggregation server talk Trade Server
    Talk2TradeServer(tsIndex);

    TraceLog(DEBUG_LEVEL, "*****************************************\n");
    TraceLog(DEBUG_LEVEL, "* %s is disconnected\n", tradeServersInfo.config[tsIndex].description);
    TraceLog(DEBUG_LEVEL, "*****************************************\n\n");

    // Close the socket
    close(sock);

    // Update Trade Servers information
    tradeServersInfo.connection[tsIndex].status = DISCONNECT;
    tradeServersInfo.connection[tsIndex].socket = -1;
    tradeServersInfo.globalConf[tsIndex].serverRole = -1;
    
    ResetTSStatus(tsIndex);
    
    char service[128];
    sprintf(service, "%s from AS", tradeServersInfo.config[tsIndex].description);

    BuildNSendDisconnectAlertToAllDaedalus(service);
    BuildNSendDisconnectAlertToAllTT(service);

    // Update buying power of TS
    IsMaxBPUpdated = YES;
    TTIndexUpdatedBP = -1;
    SendUpdateBuyingPowerToAllTS(TradingAccount.accountForTS[tsIndex]);
  }
  else
  {
    TraceLog(ERROR_LEVEL, "* Cannot connect to %s\n", tradeServersInfo.config[tsIndex].description);

    // Update flag to connect all
    tradeServersInfo.isConnectAll[tsIndex] = NO;
  }
  
  return NULL;
}

/****************************************************************************
- Function name:  Talk2TradeServer
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int Talk2TradeServer(int tsIndex)
{
  t_TSMessage tsMessage;

  countCurrentStatusReceived[tsIndex] = 0;

  // Send raw data received status to Trade Server
  if (SendRawDataStatusToTS(tsIndex) == ERROR)
  {
    CloseConnection(&tradeServersInfo.connection[tsIndex]);

    return ERROR;
  }

  usleep(10);

  // Send output log received status to Trade Server
  if (SendOutputLogStatusToTS(tsIndex) == ERROR)
  {
    CloseConnection(&tradeServersInfo.connection[tsIndex]);

    return ERROR;
  }
  
  usleep(10);
  
  // Send trading account to TS
  if (SendTradingAccountToTS(tsIndex) == ERROR)
  {
    CloseConnection(&tradeServersInfo.connection[tsIndex]);

    return ERROR;
  }
  
  usleep(10);
  
  // Send hung order reset to Trade Server
  if (SendHungOrderResetToTS(tsIndex) == ERROR)
  {
    CloseConnection(&tradeServersInfo.connection[tsIndex]);
    return ERROR;
  }

  usleep(10);
  
  //Send BBO info received status to TS
  if (SendBBOInfoStatusToTS(tsIndex) == ERROR)
  {
    CloseConnection(&tradeServersInfo.connection[tsIndex]);

    return ERROR;
  }

  usleep(10);

  // Check connect all
  if (tradeServersInfo.isConnectAll[tsIndex] == YES)
  {
    // Process connect all ECN Books and ECN Orders
    ProcessTSConnectAllConnection(tsIndex, tradeServersInfo.ttIndexPerformedConnectAll[tsIndex]);

    tradeServersInfo.isConnectAll[tsIndex] = NO;
  }
  
  usleep(10);

  // Send ignore stocklist to ts
  if (SendIgnoreStockListToTS(tsIndex) == ERROR)
  {
    CloseConnection(&tradeServersInfo.connection[tsIndex]);

    return ERROR;
  }

  usleep(10);
  
  //Send ignore stock ranges list to ts
  if(SendIgnoreStockRangesListToTS(tsIndex) == ERROR)
  {
    CloseConnection(&tradeServersInfo.connection[tsIndex]);
  }
  
  usleep(10);
  
  //Send all symbols which having stucks to TS so TS will lock it
  if (SendStuckSymbolsToTS(tsIndex) == ERROR)
  {
    CloseConnection(&tradeServersInfo.connection[tsIndex]);
  }
  
  usleep(10);
  
  SendActiveVolatileSymbolsToTS(tsIndex);
  
  usleep(10);
  
  if (SendAllVolatileToggleStatusToTS(tsIndex) == ERROR)
  {
    CloseConnection(&tradeServersInfo.connection[tsIndex]);
  }
  
  usleep(10);
  
  SendEtbSymbolListToTS(tsIndex);
  
  usleep(10);
  
  // Send all halted symbols to this TS
  pthread_mutex_lock(&HaltStatusMgmt.mutexLock);
  int tmpList[MAX_STOCK_SYMBOL];
  if (SendHaltStockListToTS(tsIndex, tmpList, -1) == ERROR)
  {
    CloseConnection(&tradeServersInfo.connection[tsIndex]);
  }
  pthread_mutex_unlock(&HaltStatusMgmt.mutexLock);
  
  usleep(10);
  
  int i;  
  for (i = 0; i < TS_MAX_BOOK_CONNECTIONS; i++)
  {
    if( (tradeServersInfo.tsSetting[tsIndex].bookEnable[i] == NO) &&
        (tradeServersInfo.currentStatus[tsIndex].tsMdcStatus[i])
       )
    {
      TraceLog(DEBUG_LEVEL, "Send request %s disconnect to ECN Book, ecnIndex = %d\n", tradeServersInfo.config[tsIndex].description, i);
      SendDisconnectBookToTS(tsIndex, i);
    }
  }
  
  for (i = 0; i < TS_MAX_ORDER_CONNECTIONS; i++)
  {
    if( (tradeServersInfo.tsSetting[tsIndex].orderEnable[i] == NO) &&
        (tradeServersInfo.currentStatus[tsIndex].tsOecStatus[i])
       )
    {
      TraceLog(DEBUG_LEVEL, "Send request %s disconnect to ECN Order, ecnIndex = %d\n", tradeServersInfo.config[tsIndex].description, i);
      SendDisconnectOrderToTS(tsIndex, i);
    }
  }

  while (1) 
  {   
    // Check connection between AS and TS
    if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
    {
      return ERROR;
    }

    // Receive a message from Trade Server
    if (ReceiveTSMessage(tsIndex, &tsMessage) != ERROR)
    {     
      // Message type
      int msgType = GetShortNumber(tsMessage.msgContent);

      // TraceLog(DEBUG_LEVEL, "TS: msgType=%d msgLen=%d\n", msgType, tsMessage.msgLen);  
      
      switch (msgType)
      {
        //Receive Book idle warning from TS
        case MSG_TYPE_FOR_RECEIVE_BOOK_IDLE_WARNING_FROM_TS:
          ProcessTSMessageForBookIdleWarning(tsIndex, &tsMessage);
          break;
        
        // Receive trading status message
        case MSG_TYPE_FOR_RECEIVE_CURRENT_STATUS_FROM_TS:
          ProcessTSMessageForCurrentStatus(tsIndex, &tsMessage);
          
          if (SendHeartBeatToTS(tsIndex) == ERROR)
          {
            return ERROR;
          }

          break;
        
        // Receive raw data message
        case MSG_TYPE_FOR_RECEIVE_RAW_DATA_FROM_TS:         
          ProcessTSMessageForRawData(tsIndex, &tsMessage);
          break;
        
        //Receive BBO Info
        case MSG_TYPE_FOR_RECEIVE_BBO_INFO_FROM_TS:
          ProcessTSMessageForBBOInfo(tsIndex, &tsMessage);
          break;
        
        // Receive output log message
        case MSG_TYPE_FOR_RECEIVE_OUTPUT_LOG_FROM_TS:
          ProcessTSMessageForOutputLog(tsIndex, &tsMessage);
          break;
          
        // Receive price update
        case MSG_TYPE_FOR_RECEIVE_PRICE_UPDATE_FROM_TS:
          ProcessTSMessageForPriceUpdate(tsIndex, &tsMessage);
          break;
        
        // Receive stock symbol status
        case MSG_TYPE_FOR_RECEIVE_SYMBOL_STATUS_FROM_TS:
          ProcessTSMessageForSymbolStatus(tsIndex, &tsMessage);
          break;

        case MSG_TYPE_FOR_SYMBOL_HALT_STATUS_FROM_TS:
          ProcessTSMessageForHaltStockList(tsIndex, &tsMessage);
          break;
          
        // Receive ECN Book configuration
        case MSG_TYPE_FOR_RECEIVE_BOOK_CONF_FROM_TS:
          ProcessTSMessageForBookConf(tsIndex, &tsMessage);
          break;

        // Receive ECN Order configuration
        case MSG_TYPE_FOR_RECEIVE_ORDER_CONF_FROM_TS:
          ProcessTSMessageForOrderConf(tsIndex, &tsMessage);
          break;

        // Receive global configuration
        case MSG_TYPE_FOR_RECEIVE_GLOBAL_CONF_FROM_TS:
          ProcessTSMessageForGlobalConf(tsIndex, &tsMessage);
          break;

        // Receive global configuration
        case MSG_TYPE_FOR_RECEIVE_MIN_SPREAD_CONF_FROM_TS:
          ProcessTSMessageForMinSpreadConf(tsIndex, &tsMessage);
          break;
        
        // Receive  exchange conf from TS
        case MSG_TYPE_FOR_RECEIVE_EXCHANGE_CONF_FROM_TS:
          ProcessTSMessageForExchangeConf(tsIndex, &tsMessage);
          break;
        
        case MSG_TYPE_FOR_RECEIVE_BBO_QUOTE_LIST_FROM_TS:
          ProcessTSMessageForBBOQuotes(tsIndex, &tsMessage);
          break;
        
        case MSG_TYPE_FOR_RECEIVE_BOOK_ISO_TRADING_FROM_TS:
          ProcessTSMessageForBookISO_Trading(tsIndex, &tsMessage);
          break;
          
        // Receive number of symbols with quotes from TS
        case MSG_TYPE_FOR_RECEIVE_NUMBER_OF_SYMBOLS_WITH_QUOTES_FROM_TS:
          ProcessTSMessageForNumberOfSymbolsWithQuotes(tsIndex, &tsMessage);
          break;
          
        // Receive disconnect alert from TS
        case MSG_TYPE_FOR_RECEIVE_DISCONNECT_ALERT_FROM_TS:
          ProcessTSMessageForDisconnectAlert(tsIndex, &tsMessage);
          break;

        // Receive disabled trading alert from TS
        case MSG_TYPE_FOR_RECEIVE_DISABLED_TRADING_ALERT_FROM_TS:
          ProcessTSMessageForDisabledTradingAlert(tsIndex, &tsMessage);
          break;
          
        case MSG_TYPE_FOR_RECEIVE_MARKET_STALE_ALERT_FROM_TS:
          ProcessTSMessageForMarketStaleAlert(tsIndex, &tsMessage);
          break;
          
        // Receive disabled trading alert from TS
        case MSG_TYPE_FOR_RECEIVE_GROUP_OF_BOOK_DATA_UNHANDLED_ALERT_FROM_TS:
          ProcessTSMessageForGroupOfBookDataUnhandledAlert(tsIndex, &tsMessage);
          break;

        case MSG_TYPE_FOR_RECEIVE_SPLIT_SYMBOL_CONF_FROM_TS:
          ProcessTSMessageForSplitSymbolConf(tsIndex, &tsMessage);
          break;

        // Invalid message
        default:
          TraceLog(ERROR_LEVEL, "Aggregation Server does not support message from Trade Server, msgType = %d\n", msgType);
          break;
      }     
    }
    else
    {     
      return ERROR;
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ReceiveTSMessage
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ReceiveTSMessage(int tsIndex, void *message)
{
  int receivedBytes, sock;

  t_TSMessage *tsMessage = (t_TSMessage *)message;

  // Socket to send messages to Trade Server
  sock = tradeServersInfo.connection[tsIndex].socket;
  
  // Timeout counter
  int timeoutCounter = 0;
  
  // Current messsae body length
  int currentMsgBodyLen = 0;
  
  // Initialize message
  tsMessage->msgLen = 0;
  
  /*
  Try to receive message header
  
  If cannot receive message header when reached timeout, return and report error
  
  otherwise calculate message body
  
  If message body has length 0, return Success
  
  Otherwise try to receive message body
  
  If cannot receive message body when reached timeout, return and report error
  
  Otherwise return Success
   */
  
  // Try to receive message header
  while (tsMessage->msgLen < MSG_HEADER_LEN)
  {
    receivedBytes = recv(sock, &tsMessage->msgContent[tsMessage->msgLen], MSG_HEADER_LEN - tsMessage->msgLen, 0);
    
    if (receivedBytes > 0)
    {
      timeoutCounter = 0;
      tsMessage->msgLen += receivedBytes;
    }
    else
    {
      if ((receivedBytes == 0) && (errno == 0))
      {
        TraceLog(ERROR_LEVEL, "%s: Socket is close\n", tradeServersInfo.config[tsIndex].description);
        return ERROR;
      }
      else
      {       
        if (timeoutCounter < 3)
        {
          timeoutCounter += 1;
          TraceLog(DEBUG_LEVEL, "%s: Waiting. Sleep num = %d\n", tradeServersInfo.config[tsIndex].description, timeoutCounter);
        }
        else
        {
          TraceLog(ERROR_LEVEL, "%s: TIME OUT!\n", tradeServersInfo.config[tsIndex].description);
          return ERROR;
        }
      }
    }
  }
  
  // Get message body length
  currentMsgBodyLen = GetIntNumber(&tsMessage->msgContent[MSG_BODY_LEN_INDEX]);

  //TraceLog(DEBUG_LEVEL, "Body length = %d\n", currentMsgBodyLen);

  if (currentMsgBodyLen == 0)
  {
    // Body is NULL

    return SUCCESS;
  }
  
  // Try to receive message body
  while (tsMessage->msgLen < currentMsgBodyLen + MSG_HEADER_LEN)
  {
    receivedBytes = recv(sock, &tsMessage->msgContent[tsMessage->msgLen], currentMsgBodyLen - (tsMessage->msgLen - MSG_HEADER_LEN), 0);
    
    if (receivedBytes > 0)
    {
      timeoutCounter = 0;
      tsMessage->msgLen += receivedBytes;
    }
    else
    {
      if ((receivedBytes == 0) && (errno == 0))
      {
        TraceLog(ERROR_LEVEL, "%s: Socket is close\n", tradeServersInfo.config[tsIndex].description);
        return ERROR;
      }
      else
      {       
        if (timeoutCounter < 3)
        {
          timeoutCounter ++;
          TraceLog(DEBUG_LEVEL, "%s: Waiting. Sleep num = %d\n", tradeServersInfo.config[tsIndex].description, timeoutCounter);
        }
        else
        {
          TraceLog(ERROR_LEVEL, "%s: TIME OUT!\n", tradeServersInfo.config[tsIndex].description);
          return ERROR;
        }
      }
    }
  }

  // Succesfully received
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTSMessageForBBOInfo
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTSMessageForBBOInfo(int tsIndex, void *message)
{
  t_TSMessage *tsMessage = (t_TSMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Number of block
  int numOfEntries;
  memcpy(&numOfEntries, &tsMessage->msgContent[currentIndex], 4);

  // Update current index
  currentIndex += 4;
  int startOfData = currentIndex;
  int i;
  for (i = 0; i < numOfEntries; i++)
  {
    //Copy to collection
    memcpy (&BBOInfoMgmt[tsIndex].BBOInfoCollection[BBOInfoMgmt[tsIndex].receivedIndex % MAX_BBO_ENTRY], &tsMessage->msgContent[currentIndex], sizeof(t_BBOQuoteEntry));
    
    // Update current index
    currentIndex += sizeof(t_BBOQuoteEntry);
    
    BBOInfoMgmt[tsIndex].receivedIndex ++;
  }

  TraceLog(DEBUG_LEVEL, "Receive BBO Info from %s, numOfEntries = %d\n", tradeServersInfo.config[tsIndex].description, numOfEntries);

  // Save data block to file
  if (fwrite(&tsMessage->msgContent[startOfData], sizeof(t_BBOQuoteEntry)*numOfEntries, 1, BBOInfoMgmt[tsIndex].fileDesc) != 1)
  {
    TraceLog(ERROR_LEVEL, "Cannot save BBO Info to file.\n");
    return ERROR;
  }
  
  // Flush all data from buffer to file
  fflush(BBOInfoMgmt[tsIndex].fileDesc);
    
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTSMessageForRawData
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTSMessageForRawData(int tsIndex, void *message)
{
  t_TSMessage *tsMessage = (t_TSMessage *)message;
  t_DataBlock dataBlock;
  memset (&dataBlock, 0, sizeof (dataBlock));

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // ECN Order Id
  int orderIndex = tsMessage->msgContent[currentIndex];

  // Update current index
  currentIndex += 1;

  while (currentIndex < tsMessage->msgLen)
  {
    // Block length
    int blockLen = GetIntNumber(&tsMessage->msgContent[currentIndex]);    
    dataBlock.blockLen = blockLen;

    // Update current index
    currentIndex += 4;    

    // Message length
    int msgLen = GetIntNumber(&tsMessage->msgContent[currentIndex]);
    dataBlock.msgLen = msgLen;

    // Update current index
    currentIndex += 4;

    // Message body
    memcpy(dataBlock.msgContent, &tsMessage->msgContent[currentIndex], msgLen);

    // Update current index
    currentIndex += msgLen;

    // Length of additional information
    int addLen = GetIntNumber(&tsMessage->msgContent[currentIndex]);

    // Update current index
    currentIndex += 4;

    // Body of additional information
    memcpy(dataBlock.addContent, &tsMessage->msgContent[currentIndex], addLen);

    // Update current index
    currentIndex += addLen;

    // Add data block to collection
    AddDataBlockToCollection(&dataBlock, &tsRawDataMgmt[tsIndex][orderIndex]);

    // Save data block to file
    SaveDataBlockToFile(&tsRawDataMgmt[tsIndex][orderIndex]);
  }

  TraceLog(DEBUG_LEVEL, "Receive raw data from %s, ECN Id = %d, countRawData = %d\n", tradeServersInfo.config[tsIndex].description, orderIndex, tsRawDataMgmt[tsIndex][orderIndex].countRawDataBlock);

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTSMessageForOutputLog
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTSMessageForOutputLog(int tsIndex, void *message)
{
  t_TSMessage *tsMessage = (t_TSMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  int bodyLen = GetIntNumber(&tsMessage->msgContent[currentIndex]);

  //TraceLog(DEBUG_LEVEL, "Block length = %d\n", bodyLen);

  // Update current index
  currentIndex += 4;

  // Number of entries
  int numEntries = GetIntNumber(&tsMessage->msgContent[currentIndex]);

  //Monitor if there is so many output log from TS
  OutputLogProcessingStatus[tsIndex].numList[OutputLogProcessingStatus[tsIndex].iIndex++] = numEntries;
  if (OutputLogProcessingStatus[tsIndex].iIndex == 6)
  {
    OutputLogProcessingStatus[tsIndex].iIndex = 0;
  }
  
  int totalReceivedWithin3secs = OutputLogProcessingStatus[tsIndex].numList[0] + OutputLogProcessingStatus[tsIndex].numList[1] + OutputLogProcessingStatus[tsIndex].numList[2] + OutputLogProcessingStatus[tsIndex].numList[3] + OutputLogProcessingStatus[tsIndex].numList[4] + OutputLogProcessingStatus[tsIndex].numList[5];
  if (totalReceivedWithin3secs > NumberOutputLogToStop)
  {
    if (OutputLogProcessingStatus[tsIndex].cStatus == 0)
    {
      OutputLogProcessingStatus[tsIndex].cStatus = 1; //Disable processing output log
    }
  }
  else if (totalReceivedWithin3secs < NumberOutputLogToResume)
  {
    if (OutputLogProcessingStatus[tsIndex].cStatus == 1)
    {
      OutputLogProcessingStatus[tsIndex].cStatus = 0; //Enable processing output log
    }
  }
    
  //TraceLog(DEBUG_LEVEL, "numEntries = %d\n", numEntries);

  // Update current index
  currentIndex += 4;

  if (bodyLen != numEntries * MAX_OUTPUT_LOG_BIN_LEN + 8)
  {
    TraceLog(ERROR_LEVEL, "invalide format message: bodyLen = %d, numEntries = %d\n", bodyLen, numEntries);

    return ERROR;
  }

  int i;
  char tradeOutputBuff[MAX_OUTPUT_LOG_TEXT_LEN];

  for (i = 0; i < numEntries; i++)
  {
    //TraceLog(DEBUG_LEVEL, "%s", &tsMessage->msgContent[currentIndex]);
    if (MappingTradeOutputLog(&tsMessage->msgContent[currentIndex], tradeOutputBuff) != ERROR)
    {
      // Add output log to collection
      AddOutputLogToCollection(tradeOutputBuff, &tradeOutputLogMgmt[tsIndex]);

      // Save output log to file
      SaveOutputLogToFile(&tradeOutputLogMgmt[tsIndex]);
    }
    
    // Update current index
    currentIndex += MAX_OUTPUT_LOG_BIN_LEN;
  }

  TraceLog(DEBUG_LEVEL, "Receive trade output log from %s, countOutputLog = %d\n", tradeServersInfo.config[tsIndex].description, tradeOutputLogMgmt[tsIndex].countOutputLog);

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTSMessageForRawData
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTSMessageForCurrentStatus(int tsIndex, void *message)
{
  //TraceLog(DEBUG_LEVEL, "Receive current status from %s\n", tradeServersInfo.config[tsIndex].description);

  t_TSMessage *tsMessage = (t_TSMessage *)message;  

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Block content = Trade Server current status
  
  // Trade Server current status
  memcpy(&tradeServersInfo.currentStatus[tsIndex].buyingPower, &tsMessage->msgContent[currentIndex], 8);
  currentIndex += 8;
  
  memcpy(&tradeServersInfo.currentStatus[tsIndex].numStuck, &tsMessage->msgContent[currentIndex], 4);
  currentIndex += 4;
  
  memcpy(&tradeServersInfo.currentStatus[tsIndex].numLoser, &tsMessage->msgContent[currentIndex], 4);
  currentIndex += 4;
  
  memcpy(tradeServersInfo.currentStatus[tsIndex].tsMdcStatus, &tsMessage->msgContent[currentIndex], TS_MAX_BOOK_CONNECTIONS);
  currentIndex += TS_MAX_BOOK_CONNECTIONS;
  
  memcpy(tradeServersInfo.currentStatus[tsIndex].tsOecStatus, &tsMessage->msgContent[currentIndex], TS_MAX_ORDER_CONNECTIONS);
  currentIndex += TS_MAX_ORDER_CONNECTIONS;
  
  memcpy(tradeServersInfo.currentStatus[tsIndex].tsTradingStatus, &tsMessage->msgContent[currentIndex], TS_MAX_ORDER_CONNECTIONS);
  currentIndex += TS_MAX_ORDER_CONNECTIONS;
  
  tradeServersInfo.currentStatus[tsIndex].statusCQS = tsMessage->msgContent[currentIndex++];
  tradeServersInfo.currentStatus[tsIndex].statusUQDF = tsMessage->msgContent[currentIndex++];
  tradeServersInfo.currentStatus[tsIndex].statusISOTrading = tsMessage->msgContent[currentIndex++];

  countCurrentStatusReceived[tsIndex] += 1;
  if (countCurrentStatusReceived[tsIndex] % 20 == 0)
  {
    char formattedTime[80] = "\0";
  
    time_t now = (time_t)hbitime_seconds();
    struct tm *l_time = (struct tm *) localtime(&now);
      
    strftime(formattedTime, 30, "%H:%M:%S", l_time);
    
    //TraceLog(DEBUG_LEVEL, "%s: Receive current status from %s\n", formattedTime, tradeServersInfo.config[tsIndex].description);
  }
  
  int isoStatus = tradeServersInfo.currentStatus[tsIndex].statusISOTrading;
  
  if (BackupTSGlobalConf[tsIndex].ISO_Trading != isoStatus)
  {
    BackupTSGlobalConf[tsIndex].ISO_Trading = isoStatus;
    // Update TT event log: Enable/disable ISO trading on TS
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    sprintf(activityDetail, "%d,%d,%d", 1, isoStatus, -1);
    ProcessUpdateTTEventLogToDatabase(tradeServersInfo.config[tsIndex].description, ACTIVITY_TYPE_ENABLE_DISENABLE_ISO_TRADING, activityDetail);
  }

  // Call function to update AS current status
  ProcessUpdatetASCurrentStatus();

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTSMessageForBookConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTSMessageForBookConf(int tsIndex, void *message)
{
  int i, numOfUnits;
  TraceLog(DEBUG_LEVEL, "Receive Book configuration from %s\n", tradeServersInfo.config[tsIndex].description);

  t_TSMessage *tsMessage = (t_TSMessage *)message;  

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Block content = ECN Book Id (1 byte) + ECN Book configuration

  // ECN Book Id (1 byte)
  int ECNIndex = tsMessage->msgContent[currentIndex];

  TraceLog(DEBUG_LEVEL, "ECNIndex = %d\n", ECNIndex);

  // Update current index
  currentIndex += 1;

  // ECN Book configuration
  switch (ECNIndex)
  {
    // ARCA Book configuration
    case TS_ARCA_BOOK_INDEX:
      tradeServersInfo.ECNBooksConf[tsIndex].ARCABookConf.currentFeedName = tsMessage->msgContent[currentIndex];
      currentIndex++;
      
      memcpy(tradeServersInfo.ECNBooksConf[tsIndex].ARCABookConf.sourceID, &tsMessage->msgContent[currentIndex], MAX_LINE_LEN);
      currentIndex += MAX_LINE_LEN;

      //TraceLog(DEBUG_LEVEL, "ARCABookConf: source ID = %s\n", tradeServersInfo.ECNBooksConf[tsIndex].ARCABookConf.sourceID);

      int groupIndex;
      for (groupIndex = 0; groupIndex < MAX_ARCA_MULTICAST_GROUP; groupIndex ++)
      {
        tradeServersInfo.ECNBooksConf[tsIndex].ARCABookConf.retranConn[groupIndex].index = tsMessage->msgContent[currentIndex];
        currentIndex += 1;

        memcpy(tradeServersInfo.ECNBooksConf[tsIndex].ARCABookConf.retranConn[groupIndex].ip, &tsMessage->msgContent[currentIndex], MAX_LINE_LEN);
        currentIndex += MAX_LINE_LEN;

        tradeServersInfo.ECNBooksConf[tsIndex].ARCABookConf.retranConn[groupIndex].port = GetIntNumber(&tsMessage->msgContent[currentIndex]);
        currentIndex += 4;
      }

      break;
    
    // NASDAQ Book configuration
    case TS_NASDAQ_BOOK_INDEX:
      memcpy(tradeServersInfo.ECNBooksConf[tsIndex].NASDAQBookConf.UDP.ip, &tsMessage->msgContent[currentIndex], MAX_LINE_LEN);
      currentIndex += MAX_LINE_LEN;

      memcpy(&tradeServersInfo.ECNBooksConf[tsIndex].NASDAQBookConf.UDP.port, &tsMessage->msgContent[currentIndex], 4);
      currentIndex += 4;

      memcpy(tradeServersInfo.ECNBooksConf[tsIndex].NASDAQBookConf.TCP.ip, &tsMessage->msgContent[currentIndex], MAX_LINE_LEN);
      currentIndex += MAX_LINE_LEN;

      memcpy(&tradeServersInfo.ECNBooksConf[tsIndex].NASDAQBookConf.TCP.port, &tsMessage->msgContent[currentIndex], 4);
      currentIndex += 4;

      memcpy(&tradeServersInfo.ECNBooksConf[tsIndex].NASDAQBookConf.maxMsgLoss, &tsMessage->msgContent[currentIndex], 4);
      currentIndex += 4;

      TraceLog(DEBUG_LEVEL, "maxMsgLoss = %d\n", tradeServersInfo.ECNBooksConf[tsIndex].NASDAQBookConf.maxMsgLoss);

      break;
      
    case TS_NDBX_BOOK_INDEX:
      memcpy(tradeServersInfo.ECNBooksConf[tsIndex].NDAQBXBookConf.UDP.ip, &tsMessage->msgContent[currentIndex], MAX_LINE_LEN);
      currentIndex += MAX_LINE_LEN;

      memcpy(&tradeServersInfo.ECNBooksConf[tsIndex].NDAQBXBookConf.UDP.port, &tsMessage->msgContent[currentIndex], 4);
      currentIndex += 4;

      break;
      
    // BATSZ Book configuration
    case TS_BATSZ_BOOK_INDEX:
      //Number of units --> UserName --> Password --> GAP IP --> GAP Port --> n * 2(IP/port)
      
      numOfUnits = tsMessage->msgContent[currentIndex];
      currentIndex += 1;
      
      //SessionSubId
      memcpy(tradeServersInfo.ECNBooksConf[tsIndex].BATSZBookGRPConf.sessionSubId, &tsMessage->msgContent[currentIndex], MAX_LINE_LEN);
      currentIndex += MAX_LINE_LEN;
      
      //UserName
      memcpy(tradeServersInfo.ECNBooksConf[tsIndex].BATSZBookGRPConf.userName, &tsMessage->msgContent[currentIndex], MAX_LINE_LEN);
      currentIndex += MAX_LINE_LEN;
      
      //Password
      memcpy(tradeServersInfo.ECNBooksConf[tsIndex].BATSZBookGRPConf.password, &tsMessage->msgContent[currentIndex], MAX_LINE_LEN);
      currentIndex += MAX_LINE_LEN;
      
      //Gap IP
      memcpy(tradeServersInfo.ECNBooksConf[tsIndex].BATSZBookGRPConf.retranRequestConn.ip, &tsMessage->msgContent[currentIndex], MAX_LINE_LEN);
      currentIndex += MAX_LINE_LEN;

      //GAP Port
      tradeServersInfo.ECNBooksConf[tsIndex].BATSZBookGRPConf.retranRequestConn.port = GetIntNumber(&tsMessage->msgContent[currentIndex]);
      currentIndex += 4;
      
      for (i=0; i<numOfUnits; i++)
      {
        //Data feed IP /Port
        memcpy(tradeServersInfo.ECNBooksConf[tsIndex].BATSZBookConf[i].primaryConn.ip, &tsMessage->msgContent[currentIndex], MAX_LINE_LEN);
        currentIndex += MAX_LINE_LEN;
        tradeServersInfo.ECNBooksConf[tsIndex].BATSZBookConf[i].primaryConn.port = GetIntNumber(&tsMessage->msgContent[currentIndex]);
        currentIndex += 4;
        
        //Retrans feed IP/Port
        memcpy(tradeServersInfo.ECNBooksConf[tsIndex].BATSZBookConf[i].retranConn.ip, &tsMessage->msgContent[currentIndex], MAX_LINE_LEN);
        currentIndex += MAX_LINE_LEN;
        tradeServersInfo.ECNBooksConf[tsIndex].BATSZBookConf[i].retranConn.port = GetIntNumber(&tsMessage->msgContent[currentIndex]);
        currentIndex += 4;
      }

      break;

    // NYSE OPEN ULTRA Book configuration
    case TS_NYSE_BOOK_INDEX:
      memcpy(tradeServersInfo.ECNBooksConf[tsIndex].NYSEBookConf.sourceID, &tsMessage->msgContent[currentIndex], MAX_LINE_LEN);
      currentIndex += MAX_LINE_LEN;

      for (groupIndex = 0; groupIndex < MAX_NYSE_OPEN_ULTRA_MULTICAST_GROUP; groupIndex ++)
      {
        // Group index
        tradeServersInfo.ECNBooksConf[tsIndex].NYSEBookConf.nyseBook[groupIndex].index = tsMessage->msgContent[currentIndex];
        currentIndex += 1;

        // Retransmission IP
        memcpy(tradeServersInfo.ECNBooksConf[tsIndex].NYSEBookConf.nyseBook[groupIndex].retranIP, &tsMessage->msgContent[currentIndex], MAX_LINE_LEN);
        currentIndex += MAX_LINE_LEN;

        // Retransmission port
        memcpy(&tradeServersInfo.ECNBooksConf[tsIndex].NYSEBookConf.nyseBook[groupIndex].retranPort, &tsMessage->msgContent[currentIndex], 4);
        currentIndex += 4;
      }

      break;
    
    // EdgeX Book configuration
    case TS_EDGX_BOOK_INDEX:
      //Gap IP
      memcpy(tradeServersInfo.ECNBooksConf[tsIndex].EDGXBookGRPConf.ip, &tsMessage->msgContent[currentIndex], MAX_LINE_LEN);
      currentIndex += MAX_LINE_LEN;

      //GAP Port
      tradeServersInfo.ECNBooksConf[tsIndex].EDGXBookGRPConf.port = GetIntNumber(&tsMessage->msgContent[currentIndex]);
      currentIndex += 4;
      
      //UserName
      memcpy(tradeServersInfo.ECNBooksConf[tsIndex].EDGXBookGRPConf.userName, &tsMessage->msgContent[currentIndex], MAX_LINE_LEN);
      currentIndex += MAX_LINE_LEN;
      
      //Password
      memcpy(tradeServersInfo.ECNBooksConf[tsIndex].EDGXBookGRPConf.password, &tsMessage->msgContent[currentIndex], MAX_LINE_LEN);
      currentIndex += MAX_LINE_LEN;
      
      numOfUnits = tsMessage->msgContent[currentIndex];
      currentIndex += 1;
      
      for (i=0; i<numOfUnits; i++)
      {
        //Data feed IP /Port
        memcpy(tradeServersInfo.ECNBooksConf[tsIndex].EDGXBookConf[i].primaryConn.ip, &tsMessage->msgContent[currentIndex], MAX_LINE_LEN);
        currentIndex += MAX_LINE_LEN;
        tradeServersInfo.ECNBooksConf[tsIndex].EDGXBookConf[i].primaryConn.port = GetIntNumber(&tsMessage->msgContent[currentIndex]);
        currentIndex += 4;
        
        //Retrans feed IP/Port
        memcpy(tradeServersInfo.ECNBooksConf[tsIndex].EDGXBookConf[i].retranConn.ip, &tsMessage->msgContent[currentIndex], MAX_LINE_LEN);
        currentIndex += MAX_LINE_LEN;
        tradeServersInfo.ECNBooksConf[tsIndex].EDGXBookConf[i].retranConn.port = GetIntNumber(&tsMessage->msgContent[currentIndex]);
        currentIndex += 4;
      }

      break;
      
    case TS_EDGA_BOOK_INDEX:
      //Gap IP
      memcpy(tradeServersInfo.ECNBooksConf[tsIndex].EDGABookGRPConf.ip, &tsMessage->msgContent[currentIndex], MAX_LINE_LEN);
      currentIndex += MAX_LINE_LEN;

      //GAP Port
      tradeServersInfo.ECNBooksConf[tsIndex].EDGABookGRPConf.port = GetIntNumber(&tsMessage->msgContent[currentIndex]);
      currentIndex += 4;
      
      //UserName
      memcpy(tradeServersInfo.ECNBooksConf[tsIndex].EDGABookGRPConf.userName, &tsMessage->msgContent[currentIndex], MAX_LINE_LEN);
      currentIndex += MAX_LINE_LEN;
      
      //Password
      memcpy(tradeServersInfo.ECNBooksConf[tsIndex].EDGABookGRPConf.password, &tsMessage->msgContent[currentIndex], MAX_LINE_LEN);
      currentIndex += MAX_LINE_LEN;
      
      numOfUnits = tsMessage->msgContent[currentIndex];
      currentIndex += 1;
      
      for (i=0; i<numOfUnits; i++)
      {
        //Data feed IP /Port
        memcpy(tradeServersInfo.ECNBooksConf[tsIndex].EDGABookConf[i].primaryConn.ip, &tsMessage->msgContent[currentIndex], MAX_LINE_LEN);
        currentIndex += MAX_LINE_LEN;
        tradeServersInfo.ECNBooksConf[tsIndex].EDGABookConf[i].primaryConn.port = GetIntNumber(&tsMessage->msgContent[currentIndex]);
        currentIndex += 4;
        
        //Retrans feed IP/Port
        memcpy(tradeServersInfo.ECNBooksConf[tsIndex].EDGABookConf[i].retranConn.ip, &tsMessage->msgContent[currentIndex], MAX_LINE_LEN);
        currentIndex += MAX_LINE_LEN;
        tradeServersInfo.ECNBooksConf[tsIndex].EDGABookConf[i].retranConn.port = GetIntNumber(&tsMessage->msgContent[currentIndex]);
        currentIndex += 4;
      }
      break;
      
    // BYX Book configuration
    case TS_BYX_BOOK_INDEX:
      //Number of units --> UserName --> Password --> GAP IP --> GAP Port --> n * 2(IP/port)
      
      numOfUnits = tsMessage->msgContent[currentIndex];
      currentIndex += 1;
      
      //SessionSubId
      memcpy(tradeServersInfo.ECNBooksConf[tsIndex].BYXBookGRPConf.sessionSubId, &tsMessage->msgContent[currentIndex], MAX_LINE_LEN);
      currentIndex += MAX_LINE_LEN;
      
      //UserName
      memcpy(tradeServersInfo.ECNBooksConf[tsIndex].BYXBookGRPConf.userName, &tsMessage->msgContent[currentIndex], MAX_LINE_LEN);
      currentIndex += MAX_LINE_LEN;
      
      //Password
      memcpy(tradeServersInfo.ECNBooksConf[tsIndex].BYXBookGRPConf.password, &tsMessage->msgContent[currentIndex], MAX_LINE_LEN);
      currentIndex += MAX_LINE_LEN;
      
      //Gap IP
      memcpy(tradeServersInfo.ECNBooksConf[tsIndex].BYXBookGRPConf.retranRequestConn.ip, &tsMessage->msgContent[currentIndex], MAX_LINE_LEN);
      currentIndex += MAX_LINE_LEN;

      //GAP Port
      tradeServersInfo.ECNBooksConf[tsIndex].BYXBookGRPConf.retranRequestConn.port = GetIntNumber(&tsMessage->msgContent[currentIndex]);
      currentIndex += 4;
      
      for (i=0; i<numOfUnits; i++)
      {
        //Data feed IP /Port
        memcpy(tradeServersInfo.ECNBooksConf[tsIndex].BYXBookConf[i].primaryConn.ip, &tsMessage->msgContent[currentIndex], MAX_LINE_LEN);
        currentIndex += MAX_LINE_LEN;
        tradeServersInfo.ECNBooksConf[tsIndex].BYXBookConf[i].primaryConn.port = GetIntNumber(&tsMessage->msgContent[currentIndex]);
        currentIndex += 4;
        
        //Retrans feed IP/Port
        memcpy(tradeServersInfo.ECNBooksConf[tsIndex].BYXBookConf[i].retranConn.ip, &tsMessage->msgContent[currentIndex], MAX_LINE_LEN);
        currentIndex += MAX_LINE_LEN;
        tradeServersInfo.ECNBooksConf[tsIndex].BYXBookConf[i].retranConn.port = GetIntNumber(&tsMessage->msgContent[currentIndex]);
        currentIndex += 4;
      }

      break;
      
    case TS_PSX_BOOK_INDEX:
      memcpy(tradeServersInfo.ECNBooksConf[tsIndex].PSXBookConf.UDP.ip, &tsMessage->msgContent[currentIndex], MAX_LINE_LEN);
      currentIndex += MAX_LINE_LEN;

      memcpy(&tradeServersInfo.ECNBooksConf[tsIndex].PSXBookConf.UDP.port, &tsMessage->msgContent[currentIndex], 4);
      currentIndex += 4;

      break;
      
    case TS_AMEX_BOOK_INDEX:
      // No longer supported
      break;
    
    // Invalid message
    default:
      TraceLog(ERROR_LEVEL, "Invalid Book configuration message from TS, ECNIndex = %d\n", ECNIndex);
      return ERROR;
      break;
  }

  // Call function to send Book configuration to all Trader Tool
  SendTSBookConfToAllTT(tsIndex, ECNIndex);

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTSMessageForOrderConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTSMessageForOrderConf(int tsIndex, void *message)
{
  TraceLog(DEBUG_LEVEL, "Receive Order configuration from %s\n", tradeServersInfo.config[tsIndex].description);

  t_TSMessage *tsMessage = (t_TSMessage *)message;  

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Block content = ECN Order Id (1 byte) + ECN Order configuration

  // ECN Order Id (1 byte)
  int ECNIndex = tsMessage->msgContent[currentIndex];

  TraceLog(DEBUG_LEVEL, "ECNIndex = %d\n", ECNIndex);

  // Update current index
  currentIndex += 1;
  
  if(ECNIndex < 0 || ECNIndex >= TS_MAX_ORDER_CONNECTIONS)
  {
    TraceLog(ERROR_LEVEL, "Invalid Order configuration message from TS, ECNIndex = %d\n", ECNIndex);
    return ERROR;
  }
  
  memcpy((unsigned char *)&tradeServersInfo.ECNOrdersConf[tsIndex][ECNIndex], &tsMessage->msgContent[currentIndex], sizeof(t_ECNOrderConf));

  // Call function to send Order configuration to all Trader Tool
  SendTSOrderConfToAllTT(tsIndex, ECNIndex);
  
  // Call function to send Order configuration to all Daedalus
  SendTSOrderConfToAllDaedalus(tsIndex, ECNIndex);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTSMessageForGlobalConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTSMessageForGlobalConf(int tsIndex, void *message)
{
  TraceLog(DEBUG_LEVEL, "Receive global configuration from %s\n", tradeServersInfo.config[tsIndex].description);

  t_TSMessage *tsMessage = (t_TSMessage *)message;  

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Block content = TS global configuration

  // TS global configuration
  memcpy(&tradeServersInfo.globalConf[tsIndex].maxBuyingPower, &tsMessage->msgContent[currentIndex], 8);
  currentIndex += 8;
  
  memcpy(&tradeServersInfo.globalConf[tsIndex].maxShares, &tsMessage->msgContent[currentIndex], 4);
  currentIndex += 4;
  
  memcpy(&tradeServersInfo.globalConf[tsIndex].maxConsecutiveLoser, &tsMessage->msgContent[currentIndex], 4);
  currentIndex += 4;
  
  memcpy(&tradeServersInfo.globalConf[tsIndex].maxStuckPosition, &tsMessage->msgContent[currentIndex], 4);
  currentIndex += 4;
  
  memcpy(&tradeServersInfo.globalConf[tsIndex].serverRole, &tsMessage->msgContent[currentIndex], 4);
  currentIndex += 4;
  
  memcpy(&tradeServersInfo.globalConf[tsIndex].minSpread, &tsMessage->msgContent[currentIndex], 8);
  currentIndex += 8;
  
  memcpy(&tradeServersInfo.globalConf[tsIndex].maxSpread, &tsMessage->msgContent[currentIndex], 8);
  currentIndex += 8;
  
  memcpy(&tradeServersInfo.globalConf[tsIndex].secFee, &tsMessage->msgContent[currentIndex], 8);
  currentIndex += 8;
  
  memcpy(&tradeServersInfo.globalConf[tsIndex].commission, &tsMessage->msgContent[currentIndex], 8);
  currentIndex += 8;
  
  memcpy(&tradeServersInfo.globalConf[tsIndex].loserRate, &tsMessage->msgContent[currentIndex], 8);
  currentIndex += 8;
  
  memcpy(&tradeServersInfo.globalConf[tsIndex].exRatio, &tsMessage->msgContent[currentIndex], 8);
  currentIndex += 8;
  
  memcpy(&tradeServersInfo.globalConf[tsIndex].nasdaqAuctionBeginTime, &tsMessage->msgContent[currentIndex], 4);
  currentIndex += 4;
  
  memcpy(&tradeServersInfo.globalConf[tsIndex].nasdaqAuctionEndTime, &tsMessage->msgContent[currentIndex], 4);
  currentIndex += 4;
  
  memcpy(&tradeServersInfo.globalConf[tsIndex].beginRegularSession, &tsMessage->msgContent[currentIndex], 2);
  currentIndex += 2;
  
  memcpy(&tradeServersInfo.globalConf[tsIndex].endRegularSession, &tsMessage->msgContent[currentIndex], 2);
  currentIndex += 2;
  
  if (CheckNRaiseServerRoleConfigurationMisConfiguredAlertToAllTT() == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Split Symbol Config is conflict among %s and others TS\n", tradeServersInfo.config[tsIndex].description);
    
    // Call function to connect to TS
    Disconnect2TradeServer(tsIndex);
    
    return ERROR;
  }
  
  // Call function to send global configuration to all Trader Tool
  SendTSGlobalConfToAllTT(tsIndex);
  
  char activityDetail[MAX_TRADER_LOG_LEN] = "\0"; 
  
  //Update max shares
  if (BackupTSGlobalConf[tsIndex].maxShares != tradeServersInfo.globalConf[tsIndex].maxShares)
  {
    sprintf(activityDetail, "1,%d,%d", tradeServersInfo.config[tsIndex].tsId, tradeServersInfo.globalConf[tsIndex].maxShares);
    ProcessUpdateTTEventLogToDatabase(tradeServersInfo.config[tsIndex].description, ACTIVITY_TYPE_MAX_SHARES_SETTING, activityDetail);
  }
  
  //Update max Consecutive Loss
  if (BackupTSGlobalConf[tsIndex].maxConsecutiveLoser != tradeServersInfo.globalConf[tsIndex].maxConsecutiveLoser)
  {
    sprintf(activityDetail, "1,%d,%d", tradeServersInfo.config[tsIndex].tsId, tradeServersInfo.globalConf[tsIndex].maxConsecutiveLoser);
    ProcessUpdateTTEventLogToDatabase(tradeServersInfo.config[tsIndex].description, ACTIVITY_TYPE_MAX_CONSECUTIVE_LOSS_SETTING, activityDetail);
  }
  
  // Update max Stucks
  if (BackupTSGlobalConf[tsIndex].maxStuckPosition != tradeServersInfo.globalConf[tsIndex].maxStuckPosition)
  {
    sprintf(activityDetail, "1,%d,%d", tradeServersInfo.config[tsIndex].tsId, tradeServersInfo.globalConf[tsIndex].maxStuckPosition);
    ProcessUpdateTTEventLogToDatabase(tradeServersInfo.config[tsIndex].description, ACTIVITY_TYPE_MAX_STUCKS_SETTING, activityDetail);
  }
  
  // Update max buying power
  if (!feq(BackupTSGlobalConf[tsIndex].maxBuyingPower, tradeServersInfo.globalConf[tsIndex].maxBuyingPower))
  {
    BackupTSGlobalConf[tsIndex].maxBuyingPower = tradeServersInfo.globalConf[tsIndex].maxBuyingPower;
    
    if (TTIndexUpdatedBP == -1)
    {
      sprintf(activityDetail, "1,%d,%.0lf", tradeServersInfo.config[tsIndex].tsId, tradeServersInfo.globalConf[tsIndex].maxBuyingPower);
      ProcessUpdateTTEventLogToDatabase(tradeServersInfo.config[tsIndex].description, ACTIVITY_TYPE_MAX_BUYING_POWER_SETTING, activityDetail);
    }
    else
    {
      sprintf(activityDetail, "%d,%d,%.0lf", traderToolsInfo.connection[TTIndexUpdatedBP].currentState, tradeServersInfo.config[tsIndex].tsId, tradeServersInfo.globalConf[tsIndex].maxBuyingPower);
      ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[TTIndexUpdatedBP].userName, ACTIVITY_TYPE_MAX_BUYING_POWER_SETTING, activityDetail);
    }
  }
  
  // Update min spread
  if (!feq(BackupTSGlobalConf[tsIndex].minSpread, tradeServersInfo.globalConf[tsIndex].minSpread))
  {
    BackupTSGlobalConf[tsIndex].minSpread = tradeServersInfo.globalConf[tsIndex].minSpread;
    
    sprintf(activityDetail, "1,%d,%.2lf", tradeServersInfo.config[tsIndex].tsId, tradeServersInfo.globalConf[tsIndex].minSpread);
    ProcessUpdateTTEventLogToDatabase(tradeServersInfo.config[tsIndex].description, ACTIVITY_TYPE_MIN_SPREAD_TS_SETTING, activityDetail);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTSMessageForMinSpreadConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTSMessageForMinSpreadConf(int tsIndex, void *message)
{
  TraceLog(DEBUG_LEVEL, "Receive minimum spread configuration from %s\n", tradeServersInfo.config[tsIndex].description);

  t_TSMessage *tsMessage = (t_TSMessage *)message;  

  // Block content = TS minimum spread configuration

  int index = 10; //Index to get value from TS message
  
  // Min spread in pre-market
  memcpy(&tradeServersInfo.minSpreadConf[tsIndex].preMarket, &tsMessage->msgContent[index], 8);
  index += 8;
  
  // Min spread in post-market
  memcpy(&tradeServersInfo.minSpreadConf[tsIndex].postMarket, &tsMessage->msgContent[index], 8);
  index += 8;
  
  // Num times of intraday
  memcpy(&tradeServersInfo.minSpreadConf[tsIndex].intraday.countTimes, &tsMessage->msgContent[index], 4);
  index += 4;
  
  int i;
  for (i = 0; i < tradeServersInfo.minSpreadConf[tsIndex].intraday.countTimes; i++)
  {
    memcpy(&tradeServersInfo.minSpreadConf[tsIndex].intraday.minSpreadList[i].timeInSeconds, &tsMessage->msgContent[index], 4);
    index += 4;
    
    memcpy(&tradeServersInfo.minSpreadConf[tsIndex].intraday.minSpreadList[i].minSpread, &tsMessage->msgContent[index], 8);
    index += 8;
  }
  
  // Call function to send global configuration to all Trader Tool
  SendTSMinSpreadConfToAllTT(tsIndex);

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTSMessageForHaltStockList
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTSMessageForHaltStockList(int tsIndex, void *message)
{
  t_TSMessage *tsMessage = (t_TSMessage *)message;  

  int updatedList[MAX_STOCK_SYMBOL];
  int updatedCount = 0;
  
  int currentIndex = MSG_HEADER_LEN + 4;
  int numSymbol;
  memcpy(&numSymbol, &tsMessage->msgContent[currentIndex], 4);
  currentIndex += 4;
  
  int stockSymbolIndex;
  int i, flag;
  unsigned int haltTime;
  char symbol[SYMBOL_LEN];
  
  pthread_mutex_lock(&HaltStatusMgmt.mutexLock);
  
  while (numSymbol > 0)
  {
    numSymbol --;
    memcpy(symbol, &tsMessage->msgContent[currentIndex], SYMBOL_LEN);
    currentIndex += SYMBOL_LEN;
    
    stockSymbolIndex = GetStockSymbolIndex(symbol);
    if (stockSymbolIndex != -1)
    {
      flag = 0;
      strncpy(HaltStatusMgmt.symbol[stockSymbolIndex], symbol, SYMBOL_LEN);
      for (i = 0; i < TS_MAX_BOOK_CONNECTIONS; i++)
      {
        memcpy(&haltTime, &tsMessage->msgContent[currentIndex + 1], 4);
        
        if (haltTime >= HaltStatusMgmt.haltTime[stockSymbolIndex][i]) //Only take the latest status, also status from TS takes higher priority than from AS
        {
          HaltStatusMgmt.haltTime[stockSymbolIndex][i] = haltTime;
          HaltStatusMgmt.haltStatus[stockSymbolIndex][i] = tsMessage->msgContent[currentIndex];
          
          flag = 1;
        }
        
        currentIndex += 5;
      }
      
      if (flag == 1)
      {
        updatedList[updatedCount] = stockSymbolIndex;
        updatedCount++;
      }
    }
    else
    {
      currentIndex += (5 * TS_MAX_BOOK_CONNECTIONS);
    }
  }

  SaveHaltedStockListToFile();
  
  SendHaltStockListToAllTS(updatedList, updatedCount);
  SendHaltStockListToPM(updatedList, updatedCount);
  SendHaltStockListToAllDaedalus();
  SendHaltStockListToAllTT();

  pthread_mutex_unlock(&HaltStatusMgmt.mutexLock);
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTSMessageForSymbolStatus
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTSMessageForSymbolStatus(int tsIndex, void *message)
{
  //TraceLog(DEBUG_LEVEL, "Receive stock symbol status from %s\n", tradeServersInfo.config[tsIndex].description);

  t_TSMessage *tsMessage = (t_TSMessage *)message;  

  int currentIndex = MSG_HEADER_LEN;

  // Body = 3 block (block sender + block ASK + block BID)

  // Update current index
  currentIndex += 4;

  int ttId = tsMessage->msgContent[currentIndex];

  //TraceLog(DEBUG_LEVEL, "ttId = %d\n", ttId);

  // Update current index
  currentIndex += 1;

  //TraceLog(DEBUG_LEVEL, "symbol = %s\n", &tsMessage->msgContent[currentIndex]);

  // Update current index
  //currentIndex += SYMBOL_LEN;

  //int blockASKLen = GetIntNumber(&tsMessage->msgContent[currentIndex]);

  //TraceLog(DEBUG_LEVEL, "Length of block ASK = %d\n", blockASKLen);

  // Update current index
  //currentIndex += blockASKLen;

  //int blockBIDLen = GetIntNumber(&tsMessage->msgContent[currentIndex]);

  //TraceLog(DEBUG_LEVEL, "Length of block BID = %d\n", blockBIDLen);
  
  // Call function to send stock symbol status to Daedalus
  if (traderToolsInfo.connection[ttId].clientType == CLIENT_TOOL_DAEDALUS)
  {
    BuildAndSendSymbolStatusToDaedalus(tradeServersInfo.config[tsIndex].description, ttId, tsMessage);
  }
  else if (traderToolsInfo.connection[ttId].clientType == CLIENT_TOOL_TRADER_TOOL)
  {
    SendSymbolStatusToTT(tsIndex, ttId, tsMessage);
  }
  
  return SUCCESS;
}

int ProcessTSMessageForPriceUpdate(int tsIndex, t_TSMessage *tsMessage)
{
  SendPriceUpdateToAllDaedalus(tradeServersInfo.config[tsIndex].description, tsMessage->msgContent);

  return SUCCESS;
}

/****************************************************************************
- Function name:  BuildTSMessageForRawDataStatus
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTSMessageForRawDataStatus(int tsIndex, void *message)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *tsMessage = (t_TSMessage *)message;
  
  // Initialize message
  tsMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_GET_RAW_DATA_TO_TS;
  tsMessage->msgContent[0] = shortConvert.c[0];
  tsMessage->msgContent[1] = shortConvert.c[1];
  tsMessage->msgLen += 2;

  // Update index of body length
  tsMessage->msgLen += 4;

  // Message body = n * (blockLength (4 bytes) + blockContent (blockLength bytes))
  int ECNIndex;

  for (ECNIndex = 0; ECNIndex < TS_MAX_ORDER_CONNECTIONS; ECNIndex++)
  {
    // Update index of block length
    tsMessage->msgLen += 4;

    // Block content = ECN Order Id (1 byte) + Number of entries (4 bytes)
    // ECN Order Id
    tsMessage->msgContent[tsMessage->msgLen] = ECNIndex;
    tsMessage->msgLen += 1;

    // Number of entries
    intConvert.value = tsRawDataMgmt[tsIndex][ECNIndex].countRawDataBlock - 1;
    tsMessage->msgContent[tsMessage->msgLen] = intConvert.c[0];
    tsMessage->msgContent[tsMessage->msgLen + 1] = intConvert.c[1];
    tsMessage->msgContent[tsMessage->msgLen + 2] = intConvert.c[2];
    tsMessage->msgContent[tsMessage->msgLen + 3] = intConvert.c[3];
    tsMessage->msgLen += 4;

    // Block length field
    intConvert.value = 9;
    tsMessage->msgContent[tsMessage->msgLen - 9] = intConvert.c[0];
    tsMessage->msgContent[tsMessage->msgLen - 8] = intConvert.c[1];
    tsMessage->msgContent[tsMessage->msgLen - 7] = intConvert.c[2];
    tsMessage->msgContent[tsMessage->msgLen - 6] = intConvert.c[3];
  }

  // Body length field
  intConvert.value = tsMessage->msgLen - MSG_HEADER_LEN;
  tsMessage->msgContent[2] = intConvert.c[0];
  tsMessage->msgContent[3] = intConvert.c[1];
  tsMessage->msgContent[4] = intConvert.c[2];
  tsMessage->msgContent[5] = intConvert.c[3];

  return tsMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildTSMessageForTradeOutputStatus
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTSMessageForTradeOutputStatus(int tsIndex, void *message)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *tsMessage = (t_TSMessage *)message;
  
  // Initialize message
  tsMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_GET_OUTPUT_LOG_TO_TS;
  tsMessage->msgContent[0] = shortConvert.c[0];
  tsMessage->msgContent[1] = shortConvert.c[1];
  tsMessage->msgLen += 2;

  // Update index of body length
  tsMessage->msgLen += 4;

  // Message body = blockLength (4 bytes) + blockContent (blockLength bytes)
  
  // Update index of block length
  intConvert.value = 8;
  tsMessage->msgContent[6] = intConvert.c[0];
  tsMessage->msgContent[7] = intConvert.c[1];
  tsMessage->msgContent[8] = intConvert.c[2];
  tsMessage->msgContent[9] = intConvert.c[3];
  tsMessage->msgLen += 4;

  // Number of entries
  intConvert.value = tradeOutputLogMgmt[tsIndex].countOutputLog;
  tsMessage->msgContent[tsMessage->msgLen] = intConvert.c[0];
  tsMessage->msgContent[tsMessage->msgLen + 1] = intConvert.c[1];
  tsMessage->msgContent[tsMessage->msgLen + 2] = intConvert.c[2];
  tsMessage->msgContent[tsMessage->msgLen + 3] = intConvert.c[3];
  tsMessage->msgLen += 4;
  
  // Body length field
  intConvert.value = tsMessage->msgLen - MSG_HEADER_LEN;
  tsMessage->msgContent[2] = intConvert.c[0];
  tsMessage->msgContent[3] = intConvert.c[1];
  tsMessage->msgContent[4] = intConvert.c[2];
  tsMessage->msgContent[5] = intConvert.c[3];

  return tsMessage->msgLen;
}


/****************************************************************************
- Function name:  BuildTSMessageForBBOInfoStatus
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTSMessageForBBOInfoStatus(int tsIndex, void *message)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *tsMessage = (t_TSMessage *)message;
  
  // Initialize message
  tsMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_GET_BBO_INFO_TO_AS;
  tsMessage->msgContent[0] = shortConvert.c[0];
  tsMessage->msgContent[1] = shortConvert.c[1];
  tsMessage->msgLen += 2;

  // Update index of body length
  tsMessage->msgLen += 4;

  // Message body = blockLength (4 bytes) + blockContent (blockLength bytes)
  
  // Update index of block length
  intConvert.value = 8;
  tsMessage->msgContent[6] = intConvert.c[0];
  tsMessage->msgContent[7] = intConvert.c[1];
  tsMessage->msgContent[8] = intConvert.c[2];
  tsMessage->msgContent[9] = intConvert.c[3];
  tsMessage->msgLen += 4;

  // Number of entries
  intConvert.value = BBOInfoMgmt[tsIndex].receivedIndex;
  tsMessage->msgContent[tsMessage->msgLen] = intConvert.c[0];
  tsMessage->msgContent[tsMessage->msgLen + 1] = intConvert.c[1];
  tsMessage->msgContent[tsMessage->msgLen + 2] = intConvert.c[2];
  tsMessage->msgContent[tsMessage->msgLen + 3] = intConvert.c[3];
  tsMessage->msgLen += 4;
  
  // Body length field
  intConvert.value = tsMessage->msgLen - MSG_HEADER_LEN;
  tsMessage->msgContent[2] = intConvert.c[0];
  tsMessage->msgContent[3] = intConvert.c[1];
  tsMessage->msgContent[4] = intConvert.c[2];
  tsMessage->msgContent[5] = intConvert.c[3];

  return tsMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildTSMessageForResetTSCurrentStatus
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTSMessageForResetTSCurrentStatus(void *message, int type)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *tsMessage = (t_TSMessage *)message;
  
  // Initialize message
  tsMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_RESET_CURRENT_STATUS_TO_TS;
  tsMessage->msgContent[0] = shortConvert.c[0];
  tsMessage->msgContent[1] = shortConvert.c[1];
  tsMessage->msgLen += 2;

  // Update index of body length
  tsMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  
  // Update index of block length
  tsMessage->msgLen += 4;

  // Block content = type
  tsMessage->msgContent[tsMessage->msgLen] = type;

  // Update index of body length
  tsMessage->msgLen += 1;

  // Block length field
  intConvert.value = 5;
  tsMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  tsMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  tsMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  tsMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = tsMessage->msgLen - MSG_HEADER_LEN;
  tsMessage->msgContent[2] = intConvert.c[0];
  tsMessage->msgContent[3] = intConvert.c[1];
  tsMessage->msgContent[4] = intConvert.c[2];
  tsMessage->msgContent[5] = intConvert.c[3];

  return tsMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildTSMessageForUpdateTSCurrentStatus
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTSMessageForUpdateTSCurrentStatus(void *message, double buyingPower, int isMaxBPFlag)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *tsMessage = (t_TSMessage *)message;
  
  // Initialize message
  tsMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_UPDATE_CURRENT_STATUS_TO_TS;
  tsMessage->msgContent[0] = shortConvert.c[0];
  tsMessage->msgContent[1] = shortConvert.c[1];
  tsMessage->msgLen += 2;

  // Update index of body length
  tsMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  
  // Update index of block length
  tsMessage->msgLen += 4;

  // Block content = buying power
  memcpy(&tsMessage->msgContent[tsMessage->msgLen], &buyingPower, 8);
  
  // Update index of body length
  tsMessage->msgLen += 8;
  
  memcpy(&tsMessage->msgContent[tsMessage->msgLen], &isMaxBPFlag, 4);
  tsMessage->msgLen += 4;
  
  // Block length field
  intConvert.value = tsMessage->msgLen - MSG_HEADER_LEN;
  tsMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  tsMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  tsMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  tsMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = tsMessage->msgLen - MSG_HEADER_LEN;
  tsMessage->msgContent[2] = intConvert.c[0];
  tsMessage->msgContent[3] = intConvert.c[1];
  tsMessage->msgContent[4] = intConvert.c[2];
  tsMessage->msgContent[5] = intConvert.c[3];

  return tsMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildTSMessageForEnableTrading
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTSMessageForEnableTrading(void *message, int ECNIndex)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *tsMessage = (t_TSMessage *)message;
  
  // Initialize message
  tsMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_ENABLE_TRADING_TO_TS;
  tsMessage->msgContent[0] = shortConvert.c[0];
  tsMessage->msgContent[1] = shortConvert.c[1];
  tsMessage->msgLen += 2;

  // Update index of body length
  tsMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  
  // Update index of block length
  tsMessage->msgLen += 4;

  // Block content = ECN Book Id
  tsMessage->msgContent[tsMessage->msgLen] = ECNIndex;

  // Update index of body length
  tsMessage->msgLen += 1;

  // Block length field
  intConvert.value = 5;
  tsMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  tsMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  tsMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  tsMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = tsMessage->msgLen - MSG_HEADER_LEN;
  tsMessage->msgContent[2] = intConvert.c[0];
  tsMessage->msgContent[3] = intConvert.c[1];
  tsMessage->msgContent[4] = intConvert.c[2];
  tsMessage->msgContent[5] = intConvert.c[3];

  return tsMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildTSMessageForDisableTrading
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTSMessageForDisableTrading(void *message, int ECNIndex, short reasonID)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *tsMessage = (t_TSMessage *)message;
  
  // Initialize message
  tsMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_DISABLE_TRADING_TO_TS;
  tsMessage->msgContent[0] = shortConvert.c[0];
  tsMessage->msgContent[1] = shortConvert.c[1];
  tsMessage->msgLen += 2;

  // Update index of body length
  tsMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  
  // Update index of block length
  tsMessage->msgLen += 4;

  // ECN Book Id
  tsMessage->msgContent[tsMessage->msgLen] = ECNIndex;
  tsMessage->msgLen += 1;
  
  // Reason Id
  memcpy(&tsMessage->msgContent[tsMessage->msgLen], &reasonID, 2);
  tsMessage->msgLen += 2;

  // Block length field
  intConvert.value = 7;
  tsMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  tsMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  tsMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  tsMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = tsMessage->msgLen - MSG_HEADER_LEN;
  tsMessage->msgContent[2] = intConvert.c[0];
  tsMessage->msgContent[3] = intConvert.c[1];
  tsMessage->msgContent[4] = intConvert.c[2];
  tsMessage->msgContent[5] = intConvert.c[3];

  return tsMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildTSMessageForHeartbeart
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTSMessageForHeartbeart(void *message)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *tsMessage = (t_TSMessage *)message;
  
  // Initialize message
  tsMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_HEARTBEAT_TO_TS;
  tsMessage->msgContent[0] = shortConvert.c[0];
  tsMessage->msgContent[1] = shortConvert.c[1];
  tsMessage->msgLen += 2;

  // Update index of body length
  tsMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  
  // Update index of block length
  tsMessage->msgLen += 4;

  // Block content = NULL

  // Block length field
  intConvert.value = 4;
  tsMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  tsMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  tsMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  tsMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = tsMessage->msgLen - MSG_HEADER_LEN;
  tsMessage->msgContent[2] = intConvert.c[0];
  tsMessage->msgContent[3] = intConvert.c[1];
  tsMessage->msgContent[4] = intConvert.c[2];
  tsMessage->msgContent[5] = intConvert.c[3];

  return tsMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildTSMessageForConnectBook
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTSMessageForConnectBook(void *message, int ECNIndex)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *tsMessage = (t_TSMessage *)message;
  
  // Initialize message
  tsMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_CONNECT_BOOK_TO_TS;
  tsMessage->msgContent[0] = shortConvert.c[0];
  tsMessage->msgContent[1] = shortConvert.c[1];
  tsMessage->msgLen += 2;

  // Update index of body length
  tsMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  
  // Update index of block length
  tsMessage->msgLen += 4;

  // Block content = ECN Book Id
  tsMessage->msgContent[tsMessage->msgLen] = ECNIndex;

  // Update index of body length
  tsMessage->msgLen += 1;

  // Block length field
  intConvert.value = 5;
  tsMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  tsMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  tsMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  tsMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = tsMessage->msgLen - MSG_HEADER_LEN;
  tsMessage->msgContent[2] = intConvert.c[0];
  tsMessage->msgContent[3] = intConvert.c[1];
  tsMessage->msgContent[4] = intConvert.c[2];
  tsMessage->msgContent[5] = intConvert.c[3];

  return tsMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildTSMessageForDisconnectBook
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTSMessageForDisconnectBook(void *message, int ECNIndex)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *tsMessage = (t_TSMessage *)message;
  
  // Initialize message
  tsMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_DISCONNECT_BOOK_TO_TS;
  tsMessage->msgContent[0] = shortConvert.c[0];
  tsMessage->msgContent[1] = shortConvert.c[1];
  tsMessage->msgLen += 2;

  // Update index of body length
  tsMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  
  // Update index of block length
  tsMessage->msgLen += 4;

  // Block content = ECN Book Id
  tsMessage->msgContent[tsMessage->msgLen] = ECNIndex;

  // Update index of body length
  tsMessage->msgLen += 1;

  // Block length field
  intConvert.value = 5;
  tsMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  tsMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  tsMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  tsMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = tsMessage->msgLen - MSG_HEADER_LEN;
  tsMessage->msgContent[2] = intConvert.c[0];
  tsMessage->msgContent[3] = intConvert.c[1];
  tsMessage->msgContent[4] = intConvert.c[2];
  tsMessage->msgContent[5] = intConvert.c[3];

  return tsMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildTSMessageForConnectOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTSMessageForConnectOrder(void *message, int ECNIndex)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *tsMessage = (t_TSMessage *)message;
  
  // Initialize message
  tsMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_CONNECT_ORDER_TO_TS;
  tsMessage->msgContent[0] = shortConvert.c[0];
  tsMessage->msgContent[1] = shortConvert.c[1];
  tsMessage->msgLen += 2;

  // Update index of body length
  tsMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  
  // Update index of block length
  tsMessage->msgLen += 4;

  // Block content = ECN Book Id
  tsMessage->msgContent[tsMessage->msgLen] = ECNIndex;

  // Update index of body length
  tsMessage->msgLen += 1;

  // Block length field
  intConvert.value = 5;
  tsMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  tsMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  tsMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  tsMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = tsMessage->msgLen - MSG_HEADER_LEN;
  tsMessage->msgContent[2] = intConvert.c[0];
  tsMessage->msgContent[3] = intConvert.c[1];
  tsMessage->msgContent[4] = intConvert.c[2];
  tsMessage->msgContent[5] = intConvert.c[3];

  return tsMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildTSMessageForDisconnectOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTSMessageForDisconnectOrder(void *message, int ECNIndex)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *tsMessage = (t_TSMessage *)message;
  
  // Initialize message
  tsMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_DISCONNECT_ORDER_TO_TS;
  tsMessage->msgContent[0] = shortConvert.c[0];
  tsMessage->msgContent[1] = shortConvert.c[1];
  tsMessage->msgLen += 2;

  // Update index of body length
  tsMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  
  // Update index of block length
  tsMessage->msgLen += 4;

  // Block content = ECN Book Id
  tsMessage->msgContent[tsMessage->msgLen] = ECNIndex;

  // Update index of body length
  tsMessage->msgLen += 1;

  // Block length field
  intConvert.value = 5;
  tsMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  tsMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  tsMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  tsMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = tsMessage->msgLen - MSG_HEADER_LEN;
  tsMessage->msgContent[2] = intConvert.c[0];
  tsMessage->msgContent[3] = intConvert.c[1];
  tsMessage->msgContent[4] = intConvert.c[2];
  tsMessage->msgContent[5] = intConvert.c[3];

  return tsMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildTSMessageForSetTSBookConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTSMessageForSetTSBookConf(void *message, int tsIndex, int ECNIndex)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *tsMessage = (t_TSMessage *)message;
  
  // Initialize message
  tsMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SET_BOOK_CONF_FROM_TS;
  tsMessage->msgContent[0] = shortConvert.c[0];
  tsMessage->msgContent[1] = shortConvert.c[1];
  tsMessage->msgLen += 2;

  // Update index of body length
  tsMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  
  // Update index of block length
  tsMessage->msgLen += 4;

  // Block content = ECN Book Id (1 byte) + ECN Book configuration

  // ECN Book Id (1 byte)
  tsMessage->msgContent[tsMessage->msgLen] = ECNIndex;

  // Update index of body length
  tsMessage->msgLen += 1;

  // ECN Book configuration
  int size = 0;

  switch (ECNIndex)
  {
    // NASDAQ Book configuration
    case TS_NASDAQ_BOOK_INDEX:
    
      memcpy(&tsMessage->msgContent[tsMessage->msgLen], tradeServersInfo.ECNBooksConf[tsIndex].NASDAQBookConf.UDP.ip, MAX_LINE_LEN);
      tsMessage->msgLen += MAX_LINE_LEN;

      memcpy(&tsMessage->msgContent[tsMessage->msgLen], &tradeServersInfo.ECNBooksConf[tsIndex].NASDAQBookConf.UDP.port, 4);
      tsMessage->msgLen += 4;

      memcpy(&tsMessage->msgContent[tsMessage->msgLen], tradeServersInfo.ECNBooksConf[tsIndex].NASDAQBookConf.TCP.ip, MAX_LINE_LEN);
      tsMessage->msgLen += MAX_LINE_LEN;

      memcpy(&tsMessage->msgContent[tsMessage->msgLen], &tradeServersInfo.ECNBooksConf[tsIndex].NASDAQBookConf.TCP.port, 4);
      tsMessage->msgLen += 4;

      memcpy(&tsMessage->msgContent[tsMessage->msgLen], &tradeServersInfo.ECNBooksConf[tsIndex].NASDAQBookConf.maxMsgLoss, 4);
      tsMessage->msgLen += 4;

      size = 2 * MAX_LINE_LEN + 3 * 4;

      break;
      
    case TS_NDBX_BOOK_INDEX:
      memcpy(&tsMessage->msgContent[tsMessage->msgLen], tradeServersInfo.ECNBooksConf[tsIndex].NDAQBXBookConf.UDP.ip, MAX_LINE_LEN);
      tsMessage->msgLen += MAX_LINE_LEN;

      memcpy(&tsMessage->msgContent[tsMessage->msgLen], &tradeServersInfo.ECNBooksConf[tsIndex].NDAQBXBookConf.UDP.port, 4);
      tsMessage->msgLen += 4;
      break;
    
    // BATSZ Book configuration
    case TS_BATSZ_BOOK_INDEX:
      //SessionSubId
      memcpy(&tsMessage->msgContent[tsMessage->msgLen], tradeServersInfo.ECNBooksConf[tsIndex].BATSZBookGRPConf.sessionSubId, MAX_LINE_LEN);
      tsMessage->msgLen += MAX_LINE_LEN;
      
      //Username
      memcpy(&tsMessage->msgContent[tsMessage->msgLen], tradeServersInfo.ECNBooksConf[tsIndex].BATSZBookGRPConf.userName, MAX_LINE_LEN);
      tsMessage->msgLen += MAX_LINE_LEN;
      
      //Password
      memcpy(&tsMessage->msgContent[tsMessage->msgLen], tradeServersInfo.ECNBooksConf[tsIndex].BATSZBookGRPConf.password, MAX_LINE_LEN);
      tsMessage->msgLen += MAX_LINE_LEN;

      //IP
      memcpy(&tsMessage->msgContent[tsMessage->msgLen], tradeServersInfo.ECNBooksConf[tsIndex].BATSZBookGRPConf.retranRequestConn.ip, MAX_LINE_LEN);
      tsMessage->msgLen += MAX_LINE_LEN;

      //Port
      memcpy(&tsMessage->msgContent[tsMessage->msgLen], &tradeServersInfo.ECNBooksConf[tsIndex].BATSZBookGRPConf.retranRequestConn.port, 4);
      tsMessage->msgLen += 4;

      size = 4 * MAX_LINE_LEN + 4;
      break;

    // NYSE OPEN ULTRA Book configuration
    case TS_NYSE_BOOK_INDEX:
      // SourceID
      memcpy(&tsMessage->msgContent[tsMessage->msgLen], tradeServersInfo.ECNBooksConf[tsIndex].NYSEBookConf.sourceID, MAX_LINE_LEN);
      tsMessage->msgLen += MAX_LINE_LEN;

      int groupIndex;

      for (groupIndex = 0; groupIndex < MAX_NYSE_OPEN_ULTRA_MULTICAST_GROUP; groupIndex ++)
      {
        // Group index
        tsMessage->msgContent[tsMessage->msgLen] = tradeServersInfo.ECNBooksConf[tsIndex].NYSEBookConf.nyseBook[groupIndex].index;
        tsMessage->msgLen += 1;

        // Retransmission IP
        memcpy(&tsMessage->msgContent[tsMessage->msgLen], tradeServersInfo.ECNBooksConf[tsIndex].NYSEBookConf.nyseBook[groupIndex].retranIP, MAX_LINE_LEN);
        tsMessage->msgLen += MAX_LINE_LEN;

        // Retransmission port
        memcpy(&tsMessage->msgContent[tsMessage->msgLen], &tradeServersInfo.ECNBooksConf[tsIndex].NYSEBookConf.nyseBook[groupIndex].retranPort, 4);
        tsMessage->msgLen += 4;

      }

      size = 1 + MAX_NYSE_OPEN_ULTRA_MULTICAST_GROUP * (1 + 4 + MAX_LINE_LEN);
      break;
    
    // EdgeX Book configuration
    case TS_EDGX_BOOK_INDEX:
      //IP
      memcpy(&tsMessage->msgContent[tsMessage->msgLen], tradeServersInfo.ECNBooksConf[tsIndex].EDGXBookGRPConf.ip, MAX_LINE_LEN);
      tsMessage->msgLen += MAX_LINE_LEN;

      //Port
      memcpy(&tsMessage->msgContent[tsMessage->msgLen], &tradeServersInfo.ECNBooksConf[tsIndex].EDGXBookGRPConf.port, 4);
      tsMessage->msgLen += 4;
      
      //Username
      memcpy(&tsMessage->msgContent[tsMessage->msgLen], tradeServersInfo.ECNBooksConf[tsIndex].EDGXBookGRPConf.userName, MAX_LINE_LEN);
      tsMessage->msgLen += MAX_LINE_LEN;
      
      //Password
      memcpy(&tsMessage->msgContent[tsMessage->msgLen], tradeServersInfo.ECNBooksConf[tsIndex].EDGXBookGRPConf.password, MAX_LINE_LEN);
      tsMessage->msgLen += MAX_LINE_LEN;

      size = 3 * MAX_LINE_LEN + 4;
      break;
    
    case TS_EDGA_BOOK_INDEX:
      //IP
      memcpy(&tsMessage->msgContent[tsMessage->msgLen], tradeServersInfo.ECNBooksConf[tsIndex].EDGABookGRPConf.ip, MAX_LINE_LEN);
      tsMessage->msgLen += MAX_LINE_LEN;

      //Port
      memcpy(&tsMessage->msgContent[tsMessage->msgLen], &tradeServersInfo.ECNBooksConf[tsIndex].EDGABookGRPConf.port, 4);
      tsMessage->msgLen += 4;
      
      //Username
      memcpy(&tsMessage->msgContent[tsMessage->msgLen], tradeServersInfo.ECNBooksConf[tsIndex].EDGABookGRPConf.userName, MAX_LINE_LEN);
      tsMessage->msgLen += MAX_LINE_LEN;
      
      //Password
      memcpy(&tsMessage->msgContent[tsMessage->msgLen], tradeServersInfo.ECNBooksConf[tsIndex].EDGABookGRPConf.password, MAX_LINE_LEN);
      tsMessage->msgLen += MAX_LINE_LEN;

      size = 3 * MAX_LINE_LEN + 4;
      break;
    
    // BYX Book configuration
    case TS_BYX_BOOK_INDEX:
    case TS_PSX_BOOK_INDEX:
    case TS_AMEX_BOOK_INDEX:
      // This feature is longer supported
      break;
  }

  // Block length field
  intConvert.value = size + 5;
  tsMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  tsMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  tsMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  tsMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = tsMessage->msgLen - MSG_HEADER_LEN;
  tsMessage->msgContent[2] = intConvert.c[0];
  tsMessage->msgContent[3] = intConvert.c[1];
  tsMessage->msgContent[4] = intConvert.c[2];
  tsMessage->msgContent[5] = intConvert.c[3];

  return tsMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildTSMessageForSetTSOrderConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTSMessageForSetTSOrderConf(void *message, int tsIndex, int ECNIndex)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *tsMessage = (t_TSMessage *)message;
  
  // Initialize message
  tsMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SET_ORDER_CONF_FROM_TS;
  tsMessage->msgContent[0] = shortConvert.c[0];
  tsMessage->msgContent[1] = shortConvert.c[1];
  tsMessage->msgLen += 2;

  // Update index of body length
  tsMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  
  // Update index of block length
  tsMessage->msgLen += 4;

  // Block content = ECN Order Id (1 byte) + ECN Order configuration
  
  // ECN Order Id (1 byte)
  tsMessage->msgContent[tsMessage->msgLen] = ECNIndex;

  // Update index of body length
  tsMessage->msgLen += 1;

  // ECN Order configuration
  int size = sizeof(t_ECNOrderConf);
  memcpy(&tsMessage->msgContent[tsMessage->msgLen], (unsigned char *)&tradeServersInfo.ECNOrdersConf[tsIndex][ECNIndex], size);
  tsMessage->msgLen += size;

  // Block length field
  intConvert.value = size + 5;
  tsMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  tsMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  tsMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  tsMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = tsMessage->msgLen - MSG_HEADER_LEN;
  tsMessage->msgContent[2] = intConvert.c[0];
  tsMessage->msgContent[3] = intConvert.c[1];
  tsMessage->msgContent[4] = intConvert.c[2];
  tsMessage->msgContent[5] = intConvert.c[3];

  return tsMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildTSMessageForSetTSGlobalConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTSMessageForSetTSGlobalConf(void *message, int tsIndex)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *tsMessage = (t_TSMessage *)message;
  
  // Initialize message
  tsMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SET_GLOBAL_CONF_FROM_TS;
  tsMessage->msgContent[0] = shortConvert.c[0];
  tsMessage->msgContent[1] = shortConvert.c[1];
  tsMessage->msgLen += 2;

  // Update index of body length
  tsMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  
  // Update index of block length
  tsMessage->msgLen += 4;

  // Block content = TS global configuration

  // TS global configuration
  int size = 0;
  memcpy(&tsMessage->msgContent[tsMessage->msgLen], &tradeServersInfo.globalConf[tsIndex].maxBuyingPower, 8);
  tsMessage->msgLen += 8;
  size += 8;
  
  memcpy(&tsMessage->msgContent[tsMessage->msgLen], &tradeServersInfo.globalConf[tsIndex].maxShares, 4);
  tsMessage->msgLen += 4;
  size += 4;
  
  memcpy(&tsMessage->msgContent[tsMessage->msgLen], &tradeServersInfo.globalConf[tsIndex].maxConsecutiveLoser, 4);
  tsMessage->msgLen += 4;
  size += 4;
  
  memcpy(&tsMessage->msgContent[tsMessage->msgLen], &tradeServersInfo.globalConf[tsIndex].maxStuckPosition, 4);
  tsMessage->msgLen += 4;
  size += 4;
  
  memcpy(&tsMessage->msgContent[tsMessage->msgLen], &tradeServersInfo.globalConf[tsIndex].serverRole, 4);
  tsMessage->msgLen += 4;
  size += 4;
  
  memcpy(&tsMessage->msgContent[tsMessage->msgLen], &tradeServersInfo.globalConf[tsIndex].minSpread, 8);
  tsMessage->msgLen += 8;
  size += 8;
  
  memcpy(&tsMessage->msgContent[tsMessage->msgLen], &tradeServersInfo.globalConf[tsIndex].maxSpread, 8);
  tsMessage->msgLen += 8;
  size += 8;
  
  memcpy(&tsMessage->msgContent[tsMessage->msgLen], &tradeServersInfo.globalConf[tsIndex].secFee, 8);
  tsMessage->msgLen += 8;
  size += 8;
  
  memcpy(&tsMessage->msgContent[tsMessage->msgLen], &tradeServersInfo.globalConf[tsIndex].commission, 8);
  tsMessage->msgLen += 8;
  size += 8;
  
  memcpy(&tsMessage->msgContent[tsMessage->msgLen], &tradeServersInfo.globalConf[tsIndex].loserRate, 8);
  tsMessage->msgLen += 8;
  size += 8;
  
  memcpy(&tsMessage->msgContent[tsMessage->msgLen], &tradeServersInfo.globalConf[tsIndex].exRatio, 8);
  tsMessage->msgLen += 8;
  size += 8;
  
  memcpy(&tsMessage->msgContent[tsMessage->msgLen], &tradeServersInfo.globalConf[tsIndex].nasdaqAuctionBeginTime, 4);
  tsMessage->msgLen += 4;
  size += 4;
  
  memcpy(&tsMessage->msgContent[tsMessage->msgLen], &tradeServersInfo.globalConf[tsIndex].nasdaqAuctionEndTime, 4);
  tsMessage->msgLen += 4;
  size += 4;
  
  memcpy(&tsMessage->msgContent[tsMessage->msgLen], &tradeServersInfo.globalConf[tsIndex].beginRegularSession, 2);
  tsMessage->msgLen += 2;
  size += 2;
  
  memcpy(&tsMessage->msgContent[tsMessage->msgLen], &tradeServersInfo.globalConf[tsIndex].endRegularSession, 2);
  tsMessage->msgLen += 2;
  size += 2;

  // Block length field
  intConvert.value = size + 4;
  tsMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  tsMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  tsMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  tsMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = tsMessage->msgLen - MSG_HEADER_LEN;
  tsMessage->msgContent[2] = intConvert.c[0];
  tsMessage->msgContent[3] = intConvert.c[1];
  tsMessage->msgContent[4] = intConvert.c[2];
  tsMessage->msgContent[5] = intConvert.c[3];

  return tsMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildTSMessageForSetTSMinSpreadConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTSMessageForSetTSMinSpreadConf(void *message, int tsIndex)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *tsMessage = (t_TSMessage *)message;
  
  // Initialize message
  tsMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SET_MIN_SPREAD_CONF_TO_TS;
  tsMessage->msgContent[0] = shortConvert.c[0];
  tsMessage->msgContent[1] = shortConvert.c[1];
  tsMessage->msgLen += 2;

  // Update index of body length
  tsMessage->msgLen += 4;

  // Update index of block length
  tsMessage->msgLen += 4;

  // Block content = TS minimum spread configuration

  // TS minimum spread configuration
  // Min spread in pre-market
  memcpy(&tsMessage->msgContent[tsMessage->msgLen], &tradeServersInfo.minSpreadConf[tsIndex].preMarket, 8);
  tsMessage->msgLen += 8;
  
  // Min spread in post-market
  memcpy(&tsMessage->msgContent[tsMessage->msgLen], &tradeServersInfo.minSpreadConf[tsIndex].postMarket, 8);
  tsMessage->msgLen += 8;
  
  // Num times of intraday
  memcpy(&tsMessage->msgContent[tsMessage->msgLen], &tradeServersInfo.minSpreadConf[tsIndex].intraday.countTimes, 4);
  tsMessage->msgLen += 4;
  
  int i;
  for (i = 0; i < tradeServersInfo.minSpreadConf[tsIndex].intraday.countTimes; i++)
  {
    memcpy(&tsMessage->msgContent[tsMessage->msgLen], &tradeServersInfo.minSpreadConf[tsIndex].intraday.minSpreadList[i].timeInSeconds, 4);
    tsMessage->msgLen += 4;
    
    memcpy(&tsMessage->msgContent[tsMessage->msgLen], &tradeServersInfo.minSpreadConf[tsIndex].intraday.minSpreadList[i].minSpread, 8);
    tsMessage->msgLen += 8;
  }

  // Block length field
  intConvert.value = tsMessage->msgLen - MSG_HEADER_LEN;
  tsMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  tsMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  tsMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  tsMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = tsMessage->msgLen - MSG_HEADER_LEN;
  tsMessage->msgContent[2] = intConvert.c[0];
  tsMessage->msgContent[3] = intConvert.c[1];
  tsMessage->msgContent[4] = intConvert.c[2];
  tsMessage->msgContent[5] = intConvert.c[3];

  return tsMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildTSMessageForGetSymbolStatus
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTSMessageForGetSymbolStatus(void *message, int ttIndex, unsigned char symbol[SYMBOL_LEN], char *listMdc)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *tsMessage = (t_TSMessage *)message;
  
  // Initialize message
  tsMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_GET_SYMBOL_STATUS_TO_TS;
  tsMessage->msgContent[0] = shortConvert.c[0];
  tsMessage->msgContent[1] = shortConvert.c[1];
  tsMessage->msgLen += 2;

  // Update index of body length
  tsMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  
  // Update index of block length
  tsMessage->msgLen += 4;

  // Block content = TT Id (1 byte) + Symbol (8 bytes)

  // TT Id (1 byte)
  tsMessage->msgContent[tsMessage->msgLen] = ttIndex;

  // Update index of block length
  tsMessage->msgLen += 1;

  // Symbol (8 bytes)
  strncpy((char *) &tsMessage->msgContent[tsMessage->msgLen], (char *) symbol, SYMBOL_LEN);
  tsMessage->msgLen += SYMBOL_LEN;
  
  // MDC Type
  int i;
  for(i = 0; i < TS_MAX_BOOK_CONNECTIONS; i++)
  {
    tsMessage->msgContent[tsMessage->msgLen] = listMdc[i];
    tsMessage->msgLen += 1;
  }

  // Block length field
  intConvert.value = SYMBOL_LEN + 5;
  tsMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  tsMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  tsMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  tsMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = tsMessage->msgLen - MSG_HEADER_LEN;
  tsMessage->msgContent[2] = intConvert.c[0];
  tsMessage->msgContent[3] = intConvert.c[1];
  tsMessage->msgContent[4] = intConvert.c[2];
  tsMessage->msgContent[5] = intConvert.c[3];

  return tsMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildTSMessageForFlushASymbol
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTSMessageForFlushASymbol(void *message, int ECNIndex, unsigned char symbol[SYMBOL_LEN])
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *tsMessage = (t_TSMessage *)message;
  
  // Initialize message
  tsMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_FLUSH_A_SYMBOL_TO_TS;
  tsMessage->msgContent[0] = shortConvert.c[0];
  tsMessage->msgContent[1] = shortConvert.c[1];
  tsMessage->msgLen += 2;

  // Update index of body length
  tsMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  
  // Update index of block length
  tsMessage->msgLen += 4;

  // Block content = ECN Id (1 bytes) + Symbol (8 bytes)

  // ECN Id (1 byte)
  tsMessage->msgContent[tsMessage->msgLen] = ECNIndex;

  // Update index of block length
  tsMessage->msgLen += 1;

  // Symbol (8 bytes)
  strncpy((char *) &tsMessage->msgContent[tsMessage->msgLen], (char *) symbol, SYMBOL_LEN);
  tsMessage->msgLen += SYMBOL_LEN;

  // Block length field
  intConvert.value = SYMBOL_LEN + 5;
  tsMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  tsMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  tsMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  tsMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = tsMessage->msgLen - MSG_HEADER_LEN;
  tsMessage->msgContent[2] = intConvert.c[0];
  tsMessage->msgContent[3] = intConvert.c[1];
  tsMessage->msgContent[4] = intConvert.c[2];
  tsMessage->msgContent[5] = intConvert.c[3];

  return tsMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildTSMessageForFlushAllSymbol
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTSMessageForFlushAllSymbol(void *message, int ECNIndex)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *tsMessage = (t_TSMessage *)message;
  
  // Initialize message
  tsMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_FLUSH_ALL_SYMBOL_TO_TS;
  tsMessage->msgContent[0] = shortConvert.c[0];
  tsMessage->msgContent[1] = shortConvert.c[1];
  tsMessage->msgLen += 2;

  // Update index of body length
  tsMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  
  // Update index of block length
  tsMessage->msgLen += 4;

  // Block content = ECN Id (1 bytes)

  // ECN Id (1 byte)
  tsMessage->msgContent[tsMessage->msgLen] = ECNIndex;

  // Update index of block length
  tsMessage->msgLen += 1;

  // Block length field
  intConvert.value = 5;
  tsMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  tsMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  tsMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  tsMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = tsMessage->msgLen - MSG_HEADER_LEN;
  tsMessage->msgContent[2] = intConvert.c[0];
  tsMessage->msgContent[3] = intConvert.c[1];
  tsMessage->msgContent[4] = intConvert.c[2];
  tsMessage->msgContent[5] = intConvert.c[3];

  return tsMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildTSMessageForIgnoreStockList
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTSMessageForIgnoreStockList(void *message)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *ttMessage = (t_TSMessage *)message;
  
  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_IGNORE_SYMBOL_LIST_TO_TS;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1];
  ttMessage->msgLen += 2;

  // Update index of body length
  ttMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  // Update index of block length
  ttMessage->msgLen += 4;

  // Block content = N * symbol
  int i, blockLen = 4;

  for (i = 0; i < IgnoreStockList.countStock; i++)
  {
    memcpy(&ttMessage->msgContent[ttMessage->msgLen], IgnoreStockList.stockList[i], SYMBOL_LEN);
    blockLen += SYMBOL_LEN;
    ttMessage->msgLen += SYMBOL_LEN;
  }

  // Block length field
  intConvert.value = blockLen;
  ttMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  ttMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  ttMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  ttMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[2] = intConvert.c[0];
  ttMessage->msgContent[3] = intConvert.c[1];
  ttMessage->msgContent[4] = intConvert.c[2];
  ttMessage->msgContent[5] = intConvert.c[3];

  return ttMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildTSMessageForIgnoreStockRangesList
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTSMessageForIgnoreStockRangesList(void *message)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *ttMessage = (t_TSMessage *)message;
  
  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_IGNORE_SYMBOL_RANGES_LIST_TO_TS;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1];
  ttMessage->msgLen += 2;

  // Update index of body length
  ttMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  // Update index of block length
  ttMessage->msgLen += 4;

  // Block content = N * symbol
  int i, blockLen = 4;

  for (i = 0; i < IgnoreStockRangesList.countStock; i++)
  {
    memcpy(&ttMessage->msgContent[ttMessage->msgLen], IgnoreStockRangesList.stockList[i], SYMBOL_LEN);
    blockLen += SYMBOL_LEN;
    ttMessage->msgLen += SYMBOL_LEN;
  }

  // Block length field
  intConvert.value = blockLen;
  ttMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  ttMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  ttMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  ttMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[2] = intConvert.c[0];
  ttMessage->msgContent[3] = intConvert.c[1];
  ttMessage->msgContent[4] = intConvert.c[2];
  ttMessage->msgContent[5] = intConvert.c[3];

  return ttMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildTSMessageForSetIgnoreStockList
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTSMessageForSetIgnoreStockList(void *message, int type, const char *symbol)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *ttMessage = (t_TSMessage *)message;
  
  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SET_IGNORE_SYMBOL_LIST_TO_TS;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1];
  ttMessage->msgLen += 2;

  // Update index of body length
  ttMessage->msgLen += 4;
    
  // Update index of block length
  ttMessage->msgLen += 4;

  ttMessage->msgContent[ttMessage->msgLen] = type;

  // Update index
  ttMessage->msgLen += 1;

  // Symbol
  strncpy((char *) &ttMessage->msgContent[ttMessage->msgLen], symbol, SYMBOL_LEN);
  ttMessage->msgLen += SYMBOL_LEN;
  
  //symbol list ranges
  int i, blockLen = 13;
  for (i = 0; i < IgnoreStockRangesList.countStock; i++)
  {
    memcpy(&ttMessage->msgContent[ttMessage->msgLen], IgnoreStockRangesList.stockList[i], SYMBOL_LEN);
    blockLen += SYMBOL_LEN;
    ttMessage->msgLen += SYMBOL_LEN;
  }
  
  // Message body = (blockLength (4 bytes) + type (1 bytes) + symbol (8 bytes)) + symbol ranges list
  // Block length field
  intConvert.value = blockLen;
  ttMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  ttMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  ttMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  ttMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[2] = intConvert.c[0];
  ttMessage->msgContent[3] = intConvert.c[1];
  ttMessage->msgContent[4] = intConvert.c[2];
  ttMessage->msgContent[5] = intConvert.c[3];

  return ttMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildTSMessageForSetIgnoreStockRangesList
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTSMessageForSetIgnoreStockRangesList(void *message, int type, const char *symbol)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *ttMessage = (t_TSMessage *)message;
  
  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SET_IGNORE_SYMBOL_RANGES_LIST_TO_TS;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1];
  ttMessage->msgLen += 2;

  // Update index of body length
  ttMessage->msgLen += 4;

  // Update index of block length
  ttMessage->msgLen += 4;

  ttMessage->msgContent[ttMessage->msgLen] = type;

  // Update index
  ttMessage->msgLen += 1;

  // Symbol
  strncpy((char *) &ttMessage->msgContent[ttMessage->msgLen], symbol, SYMBOL_LEN);
  ttMessage->msgLen += SYMBOL_LEN;
  //Symbol list
  int i, blockLen = 13;
  for (i = 0; i < IgnoreStockRangesList.countStock; i++)
  {
    memcpy(&ttMessage->msgContent[ttMessage->msgLen], IgnoreStockRangesList.stockList[i], SYMBOL_LEN);
    blockLen += SYMBOL_LEN;
    ttMessage->msgLen += SYMBOL_LEN;
  }
  
  // Message body = (blockLength (4 bytes) + type (1 bytes) + symbol (8 bytes)) + symbol ranges list

  // Block length field
  intConvert.value = blockLen;
  ttMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  ttMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  ttMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  ttMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[2] = intConvert.c[0];
  ttMessage->msgContent[3] = intConvert.c[1];
  ttMessage->msgContent[4] = intConvert.c[2];
  ttMessage->msgContent[5] = intConvert.c[3];

  return ttMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildTSMessageForSetStuckUnlockForASymbol
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTSMessageForSetStuckUnlockForASymbol(void *message, const char *symbol, const int lockCount)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *tsMessage = (t_TSMessage *)message;
  
  // Initialize message
  tsMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SET_STUCK_STATUS_FOR_SYMBOL;
  tsMessage->msgContent[0] = shortConvert.c[0];
  tsMessage->msgContent[1] = shortConvert.c[1];
  tsMessage->msgLen += 2;

  // Update index of body length
  tsMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  
  // Update index of block length
  tsMessage->msgLen += 4;

  // Block content = original symbol (8 bytes)
  
  //symbol
  memcpy(&tsMessage->msgContent[tsMessage->msgLen], symbol, SYMBOL_LEN);
  tsMessage->msgLen += SYMBOL_LEN;
  
  memcpy(&tsMessage->msgContent[tsMessage->msgLen], &lockCount, 4);
  tsMessage->msgLen += 4;

  // Block length field
  intConvert.value = tsMessage->msgLen - MSG_HEADER_LEN;
  tsMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  tsMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  tsMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  tsMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = tsMessage->msgLen - MSG_HEADER_LEN;
  tsMessage->msgContent[2] = intConvert.c[0];
  tsMessage->msgContent[3] = intConvert.c[1];
  tsMessage->msgContent[4] = intConvert.c[2];
  tsMessage->msgContent[5] = intConvert.c[3];

  return tsMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildTSMessageForSetLockCounterForASymbol
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTSMessageForSetLockCounterForASymbol(void *message, const char *symbol, const int lockCount)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *tsMessage = (t_TSMessage *)message;
  
  // Initialize message
  tsMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SET_LOCK_COUNTER_FOR_SYMBOL;
  tsMessage->msgContent[0] = shortConvert.c[0];
  tsMessage->msgContent[1] = shortConvert.c[1];
  tsMessage->msgLen += 2;

  // Update index of body length
  tsMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  
  // Update index of block length
  tsMessage->msgLen += 4;

  // Block content = original symbol (8 bytes)
  
  //symbol
  memcpy(&tsMessage->msgContent[tsMessage->msgLen], symbol, SYMBOL_LEN);
  tsMessage->msgLen += SYMBOL_LEN;
  
  //lock counter
  memcpy(&tsMessage->msgContent[tsMessage->msgLen], &lockCount, 4);
  tsMessage->msgLen += 4;

  // Block length field
  intConvert.value = tsMessage->msgLen - MSG_HEADER_LEN;
  tsMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  tsMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  tsMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  tsMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = tsMessage->msgLen - MSG_HEADER_LEN;
  tsMessage->msgContent[2] = intConvert.c[0];
  tsMessage->msgContent[3] = intConvert.c[1];
  tsMessage->msgContent[4] = intConvert.c[2];
  tsMessage->msgContent[5] = intConvert.c[3];

  return tsMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildTSMessageForCurrentSymbolsStuckStatus
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTSMessageForCurrentSymbolsStuckStatus(void *message, const int tsIndex, int *count)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *tsMessage = (t_TSMessage *)message;
  
  // Initialize message
  tsMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SET_ALL_STUCK_SYMBOLS_TO_TS;
  tsMessage->msgContent[0] = shortConvert.c[0];
  tsMessage->msgContent[1] = shortConvert.c[1];
  tsMessage->msgLen += 2;

  // Update index of body length
  tsMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  
  // Update index of block length
  tsMessage->msgLen += 4;

  /* Block content = count (4bytes) + symbol list (count * (SYMBOL_LEN + 4 bytes lock-count)) */
  //Get the count and build the list

  int countSymbol = RMList.countSymbol;
  
  if (countSymbol < 1)
  {
    *count = 0;
    return SUCCESS;
  }
  
  int rmInfoIndex;
  int countSentSymbol = 0;
  int account = TradingAccount.accountForTS[tsIndex];

  char tmpList[(SYMBOL_LEN + 4) * 500];
  memset(tmpList, 0, (SYMBOL_LEN + 4) * 500);
  
  for (rmInfoIndex = 0; rmInfoIndex < countSymbol; rmInfoIndex++)
  {
    int symbolIndex = GetStockSymbolIndex(RMList.symbolList[rmInfoIndex].symbol);
    if (symbolIndex == -1) continue;
    
    if (verifyPositionInfo[account][symbolIndex].shares != 0) //stuck
    {
      //Copy symbol
      strncpy(&tmpList[countSentSymbol * (SYMBOL_LEN + 4)], RMList.symbolList[rmInfoIndex].symbol, SYMBOL_LEN);
      
      //Copy number of locks
      memcpy(&tmpList[(countSentSymbol + 1) * SYMBOL_LEN + (countSentSymbol * 4)], &verifyPositionInfo[account][symbolIndex].lockCounter, 4);
      
      countSentSymbol++;
    }
  }
  
  *count = countSentSymbol;
  
  //numer of symbol
  memcpy(&tsMessage->msgContent[tsMessage->msgLen], &countSentSymbol, 4);
  tsMessage->msgLen += 4;

  //Symbol list
  memcpy(&tsMessage->msgContent[tsMessage->msgLen], tmpList, countSentSymbol * (SYMBOL_LEN + 4));
  tsMessage->msgLen += (countSentSymbol * (SYMBOL_LEN + 4));
  
  // Block length field
  intConvert.value = tsMessage->msgLen - MSG_HEADER_LEN;
  tsMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  tsMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  tsMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  tsMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = tsMessage->msgLen - MSG_HEADER_LEN;
  tsMessage->msgContent[2] = intConvert.c[0];
  tsMessage->msgContent[3] = intConvert.c[1];
  tsMessage->msgContent[4] = intConvert.c[2];
  tsMessage->msgContent[5] = intConvert.c[3];

  return tsMessage->msgLen;
}

/****************************************************************************
- Function name:  SendTSMessage
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendTSMessage(int tsIndex, void *message)
{
  int result, sock;

  t_TSMessage *tsMessage = (t_TSMessage *)message;
  
  // Socket to send messages to Trade Server
  sock = tradeServersInfo.connection[tsIndex].socket;

  pthread_mutex_lock(&tradeServersInfo.connection[tsIndex].socketMutex);

  // Check connection between AS and TS
  if ((tradeServersInfo.connection[tsIndex].status != DISCONNECT) && (tradeServersInfo.connection[tsIndex].socket != -1))
  {
    // Send message to Trade Server
    result = send(sock, tsMessage->msgContent, tsMessage->msgLen, 0);   

    if (result != tsMessage->msgLen)
    {     
      TraceLog(ERROR_LEVEL, "FAIL: SendTSMessage(), socket = %d, numBytesSent = %d, numBytesExpected = %d\n", sock, result, tsMessage->msgLen);
      PrintErrStr(ERROR_LEVEL, "Calling send(): ", errno);
      
      // Close the socket
      close(sock);

      // Update Trade Servers information
      tradeServersInfo.connection[tsIndex].status = DISCONNECT;
      tradeServersInfo.connection[tsIndex].socket = -1;

      pthread_mutex_unlock(&tradeServersInfo.connection[tsIndex].socketMutex);

      return ERROR;
    } 
  }
  else
  {
    pthread_mutex_unlock(&tradeServersInfo.connection[tsIndex].socketMutex);

    return ERROR;
  }

  pthread_mutex_unlock(&tradeServersInfo.connection[tsIndex].socketMutex);
  
  //Successfully sent
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendRawDataStatusToTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendRawDataStatusToTS(int tsIndex)
{
  // Check connection between AS and TS
  if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
  {
    return ERROR;
  }

  t_TSMessage tsMessage;

  // Build raw data status message to send Trade Server
  BuildTSMessageForRawDataStatus(tsIndex, &tsMessage);

  // Send message to Trade Server
  if (SendTSMessage(tsIndex, &tsMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send raw data received status to %s\n", tradeServersInfo.config[tsIndex].description);

    return ERROR;
  }
    
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendOutputLogStatusToTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendOutputLogStatusToTS(int tsIndex)
{
  t_TSMessage tsMessage;

  // Check connection between AS and TS
  if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
  {
    return ERROR;
  }

  // Build trade output log status message to send Trade Server
  BuildTSMessageForTradeOutputStatus(tsIndex, &tsMessage);

  // Send message to Trade Server
  if (SendTSMessage(tsIndex, &tsMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send output log received status to %s\n", tradeServersInfo.config[tsIndex].description);

    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Send count entries received of Trade Output Log to %s, count = %d\n", tradeServersInfo.config[tsIndex].description, tradeOutputLogMgmt[tsIndex].countOutputLog);

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendBBOInfoStatusToTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendBBOInfoStatusToTS(int tsIndex)
{
  t_TSMessage tsMessage;

  // Check connection between AS and TS
  if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
  {
    return ERROR;
  }

  // Build trade output log status message to send Trade Server
  BuildTSMessageForBBOInfoStatus(tsIndex, &tsMessage);

  // Send message to Trade Server
  if (SendTSMessage(tsIndex, &tsMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send BBO info received status to %s\n", tradeServersInfo.config[tsIndex].description);

    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Send count entries received of BBO info to %s, count = %d\n", tradeServersInfo.config[tsIndex].description, BBOInfoMgmt[tsIndex].receivedIndex);

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendResetTSCurrentStatusToTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendResetTSCurrentStatusToTS(int tsIndex, int type)
{
  if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage tsMessage;

  // Build reset TS current status message to send Trade Server
  BuildTSMessageForResetTSCurrentStatus(&tsMessage, type);
  
  if (type == NUM_STUCK_INDEX)
  {
    TraceLog(DEBUG_LEVEL, "Build completed message to reset stuck position for %s\n", tradeServersInfo.config[tsIndex].description);
  }
  else if (type == BUYING_POWER_INDEX)
  {
    TraceLog(DEBUG_LEVEL, "Build completed message to reset buying power for %s\n", tradeServersInfo.config[tsIndex].description);
  }
  else if (type == NUM_LOSER_INDEX)
  {
    TraceLog(DEBUG_LEVEL, "Build completed message to reset consecutive loser for %s\n", tradeServersInfo.config[tsIndex].description);
  }

  // Send message to Trade Server
  if (SendTSMessage(tsIndex, &tsMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request to reset TS current status to %s\n", tradeServersInfo.config[tsIndex].description);

    CloseConnection(&tradeServersInfo.connection[tsIndex]);

    return ERROR;
  }
  else
  {
    if (type == NUM_STUCK_INDEX)
    {
      TraceLog(DEBUG_LEVEL, "Finished reset stuck position for %s\n", tradeServersInfo.config[tsIndex].description);
    }
    else if (type == BUYING_POWER_INDEX)
    {
      TraceLog(DEBUG_LEVEL, "Finished reset buying power for %s\n", tradeServersInfo.config[tsIndex].description);
    }
    else if (type == NUM_LOSER_INDEX)
    {
      TraceLog(DEBUG_LEVEL, "Finished reset consecutive loser for %s\n", tradeServersInfo.config[tsIndex].description);
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendResetTSCurrentStatusToAllTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendResetTSCurrentStatusToAllTS(int type)
{
  int tsIndex;

  TraceLog(DEBUG_LEVEL, "Send request to reset current status to all TS ...\n");

  for (tsIndex = 0; tsIndex < tradeServersInfo.maxConnections; tsIndex++)
  {
    // Check connection between AS and TS
    if (tradeServersInfo.connection[tsIndex].status == DISCONNECT)
    {
      // Reset current status
      if (type == NUM_STUCK_INDEX)
      {
        // Reset num stuck of TS
        tradeServersInfo.currentStatus[tsIndex].numStuck = 0;
      }
      else if (type == NUM_LOSER_INDEX)
      {
        // Reset num loser of TS
        tradeServersInfo.currentStatus[tsIndex].numLoser = 0;
      }
    }
    else
    {
      // Call function to send request to reset current status to TS
      if (SendResetTSCurrentStatusToTS(tsIndex, type) == ERROR)
      {
        return ERROR;
      }
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendUpdateBuyingPowerToAllTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendUpdateBuyingPowerToAllTS(int account)
{
  int tsIndex, accountIndex;
  int numTSOnAccount[MAX_ACCOUNT];
  double buyingPowerTmp[MAX_ACCOUNT];
  
  memset(numTSOnAccount, 0, sizeof(numTSOnAccount));
  memset(buyingPowerTmp, 0, sizeof(buyingPowerTmp));
  
  // Count number of connected TSs
  for (tsIndex = 0; tsIndex < tradeServersInfo.maxConnections; tsIndex++)
  {
    if (tradeServersInfo.connection[tsIndex].status != DISCONNECT)
    {
      numTSOnAccount[(int)TradingAccount.accountForTS[tsIndex]]++;
    }
  }
  
  if (IsMaxBPUpdated == YES)
  {
    for (accountIndex = 0; accountIndex < MAX_ACCOUNT; accountIndex++)
    {
      if (numTSOnAccount[accountIndex] > 0)
      {
        buyingPowerTmp[accountIndex] = traderToolsInfo.globalConf.buyingPower[accountIndex] / numTSOnAccount[accountIndex];
      }
    }
        
    for (tsIndex = 0; tsIndex < tradeServersInfo.maxConnections; tsIndex++)
    {
      if (tradeServersInfo.connection[tsIndex].status != DISCONNECT)
      {
        SendUpdateTSCurrentStatusToTS(tsIndex, buyingPowerTmp[(int)TradingAccount.accountForTS[tsIndex]], YES);       
      }
    }
    
    IsMaxBPUpdated = NO;    
    SendUpdateBuyingPowerToAllTS(account);
    
    return SUCCESS;
  }

  if (account == MAX_ACCOUNT) // Update for all accounts
  {
    for (accountIndex = 0; accountIndex < MAX_ACCOUNT; accountIndex++)
    {
      if (numTSOnAccount[accountIndex] == 0)
      {
        traderToolsInfo.currentStatus.buyingPower[accountIndex] = 0.0;
        //traderToolsInfo.currentStatus.buyingPower[accountIndex] = traderToolsInfo.globalConf.buyingPower[accountIndex] - positionBuyingPower[accountIndex];
      }
    }
  }
  else // Special account
  {
    if (numTSOnAccount[account] == 0)
    {
      traderToolsInfo.currentStatus.buyingPower[account] = 0.0;
      // traderToolsInfo.currentStatus.buyingPower[account] = traderToolsInfo.globalConf.buyingPower[account] - positionBuyingPower[account];
    }
  }
  
  // Calculation buying power for each (equally divided)
  int willSend[MAX_ACCOUNT] = {1, 1 , 1, 1};
  if (account == MAX_ACCOUNT) //Update for all accounts
  {
    for (accountIndex = 0; accountIndex < MAX_ACCOUNT; accountIndex++)
    {
      if (numTSOnAccount[accountIndex] > 0)
      {
        buyingPowerTmp[accountIndex] = (traderToolsInfo.globalConf.buyingPower[accountIndex] - positionBuyingPower[accountIndex]) / numTSOnAccount[accountIndex];

        if (fabs(buyingPowerTmp[accountIndex] - tsBuyingPower[accountIndex]) < 10.0)
        {
          willSend[accountIndex] = 0;
        }
        else
        {
          tsBuyingPower[accountIndex] = buyingPowerTmp[accountIndex];
        }
      }
    }
  }
  else // Special account
  {
    if (numTSOnAccount[account] > 0)
    {
      buyingPowerTmp[account] = (traderToolsInfo.globalConf.buyingPower[account] - positionBuyingPower[account]) / numTSOnAccount[account];

      if (fabs(buyingPowerTmp[account] - tsBuyingPower[account]) < 10.0)
      {
        willSend[account] = 0;
      }
      else
      {
        tsBuyingPower[account] = buyingPowerTmp[account];
      }
    }
  }

  for (tsIndex = 0; tsIndex < tradeServersInfo.maxConnections; tsIndex++)
  {
    // Check connection between AS and TS
    if (tradeServersInfo.connection[tsIndex].status != DISCONNECT)
    {     
      // Call function to update buying power to TS
      if (account == MAX_ACCOUNT) //Send to all TS
      {
        for (accountIndex = 0; accountIndex < MAX_ACCOUNT; accountIndex++)
        {
          if (willSend[accountIndex] == 1 && TradingAccount.accountForTS[tsIndex] == accountIndex)
          {
            SendUpdateTSCurrentStatusToTS(tsIndex, buyingPowerTmp[accountIndex], NO);
          }
        }
      }
      else // Special account
      {
        if (willSend[account] == 1 && TradingAccount.accountForTS[tsIndex] == account)
        {
          SendUpdateTSCurrentStatusToTS(tsIndex, buyingPowerTmp[account], NO);
        }
      }
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendUpdateTSCurrentStatusToTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendUpdateTSCurrentStatusToTS(int tsIndex, double buyingPower, int isMaxBPFlag)
{
  if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage tsMessage;

  // Build update TS current status message to send Trade Server
  BuildTSMessageForUpdateTSCurrentStatus(&tsMessage, buyingPower, isMaxBPFlag);
  
  TraceLog(DEBUG_LEVEL, "Send buying power update to %s: buyingPower = %lf, updateMaxBuyingPower = %d\n", tradeServersInfo.config[tsIndex].description, buyingPower, isMaxBPFlag);

  // Send message to Trade Server
  if (SendTSMessage(tsIndex, &tsMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request to reset TS current status to %s\n", tradeServersInfo.config[tsIndex].description);
    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendHeartBeatToTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendHeartBeatToTS(int tsIndex)
{
  if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage tsMessage;

  // Build heartbeat message to send Trade Server
  BuildTSMessageForHeartbeart(&tsMessage);

  // Send message to Trade Server
  if (SendTSMessage(tsIndex, &tsMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send heartbeat to %s\n", tradeServersInfo.config[tsIndex].description);

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendConnectBookToTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendConnectBookToTS(int tsIndex, int ECNIndex)
{
  if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage tsMessage;

  // Build connect ECN Book message to send Trade Server
  BuildTSMessageForConnectBook(&tsMessage, ECNIndex);

  // Send message to Trade Server
  if (SendTSMessage(tsIndex, &tsMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request TS connect to ECN Book to %s\n", tradeServersInfo.config[tsIndex].description);

    CloseConnection(&tradeServersInfo.connection[tsIndex]);

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendDisconnectBookToTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendDisconnectBookToTS(int tsIndex, int ECNIndex)
{
  if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage tsMessage;

  // Build disconnect ECN Book message to send Trade Server
  BuildTSMessageForDisconnectBook(&tsMessage, ECNIndex);

  // Send message to Trade Server
  if (SendTSMessage(tsIndex, &tsMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request TS disconnect to ECN Book to %s\n", tradeServersInfo.config[tsIndex].description);

    CloseConnection(&tradeServersInfo.connection[tsIndex]);

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendConnectOrderToTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendConnectOrderToTS(int tsIndex, int ECNIndex)
{
  if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage tsMessage;

  // Build connect ECN Order message to send Trade Server
  BuildTSMessageForConnectOrder(&tsMessage, ECNIndex);

  // Send message to Trade Server
  if (SendTSMessage(tsIndex, &tsMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request TS connect to ECN Order to %s\n", tradeServersInfo.config[tsIndex].description);
    
    CloseConnection(&tradeServersInfo.connection[tsIndex]);

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendDisconnectOrderToTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendDisconnectOrderToTS(int tsIndex, int ECNIndex)
{
  if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage tsMessage;

  // Build disconnect ECN Order message to send Trade Server
  BuildTSMessageForDisconnectOrder(&tsMessage, ECNIndex);

  // Send message to Trade Server
  if (SendTSMessage(tsIndex, &tsMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request TS disconnect to ECN Book to %s\n", tradeServersInfo.config[tsIndex].description);

    CloseConnection(&tradeServersInfo.connection[tsIndex]);

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SetTSBookConfToTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SetTSBookConfToTS(int tsIndex, int ECNIndex)
{
  if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage tsMessage;

  // Build set Book configuration message to send Trade Server
  BuildTSMessageForSetTSBookConf(&tsMessage, tsIndex, ECNIndex);

  // Send message to Trade Server
  if (SendTSMessage(tsIndex, &tsMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request to set Book configuration to %s\n", tradeServersInfo.config[tsIndex].description);

    CloseConnection(&tradeServersInfo.connection[tsIndex]);

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SetTSOrderConfToTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SetTSOrderConfToTS(int tsIndex, int ECNIndex)
{
  if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage tsMessage;

  // Build set Order configuration message to send Trade Server
  BuildTSMessageForSetTSOrderConf(&tsMessage, tsIndex, ECNIndex);

  // Send message to Trade Server
  if (SendTSMessage(tsIndex, &tsMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request to set Order configuration to %s\n", tradeServersInfo.config[tsIndex].description);

    CloseConnection(&tradeServersInfo.connection[tsIndex]);

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SetTSGlobalConfToTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SetTSGlobalConfToTS(int tsIndex)
{
  if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage tsMessage;

  // Build set global configuration message to send Trade Server
  BuildTSMessageForSetTSGlobalConf(&tsMessage, tsIndex);

  // Send message to Trade Server
  if (SendTSMessage(tsIndex, &tsMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request to set global configuration to %s\n", tradeServersInfo.config[tsIndex].description);
    
    CloseConnection(&tradeServersInfo.connection[tsIndex]);

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SetTSMinSpreadConfToTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SetTSMinSpreadConfToTS(int tsIndex)
{
  if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage tsMessage;

  // Build set minimum spread configuration message to send Trade Server
  BuildTSMessageForSetTSMinSpreadConf(&tsMessage, tsIndex);

  // Send message to Trade Server
  if (SendTSMessage(tsIndex, &tsMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request to set minimum spread configuration to %s\n", tradeServersInfo.config[tsIndex].description);
    
    CloseConnection(&tradeServersInfo.connection[tsIndex]);

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendGetSymbolStatusToTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendGetSymbolStatusToTS(int tsIndex, int ttIndex, unsigned char symbol[SYMBOL_LEN], char *listMdc)
{
  if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage tsMessage;

  // Build get stock symbol status message to send Trade Server
  BuildTSMessageForGetSymbolStatus(&tsMessage, ttIndex, symbol, listMdc);

  // Send message to Trade Server
  if (SendTSMessage(tsIndex, &tsMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request to get stock symbol status to %s\n", tradeServersInfo.config[tsIndex].description);
    
    CloseConnection(&tradeServersInfo.connection[tsIndex]);

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendGetNumberOfSymbolsWithQuotesToTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendGetNumberOfSymbolsWithQuotesToTS(int tsIndex, int ttIndex)
{
  if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage tsMessage;

  // Build get number of symbols  with quotes message to send Trade Server
  BuildTSMessageForGetNumberOfSymbolsWithQuotes(&tsMessage, ttIndex);

  // Send message to Trade Server
  if (SendTSMessage(tsIndex, &tsMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request to get number of symbols with quotes to %s\n", tradeServersInfo.config[tsIndex].description);
    
    CloseConnection(&tradeServersInfo.connection[tsIndex]);

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  BuildTSMessageForGetNumberOfSymbolsWithQuotes
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTSMessageForGetNumberOfSymbolsWithQuotes(void *message, int ttIndex)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *tsMessage = (t_TSMessage *)message;
  
  // Initialize message
  tsMessage->msgLen = 0;
  
  // Message type for send get number of symbols with quotes to TS
  shortConvert.value = MSG_TYPE_FOR_SEND_GET_NUMBER_OF_SYMBOLS_WITH_QUOTES_TO_TS;
  tsMessage->msgContent[0] = shortConvert.c[0];
  tsMessage->msgContent[1] = shortConvert.c[1];
  tsMessage->msgLen += 2;

  // Update index of body length
  tsMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  
  // Update index of block length
  tsMessage->msgLen += 4;

  // Block content = TT Id (1 byte)

  // TT Id (1 byte)
  tsMessage->msgContent[tsMessage->msgLen] = ttIndex;
  tsMessage->msgLen += 1;

  // Block length field
  intConvert.value = 4 + 1;
  tsMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  tsMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  tsMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  tsMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = tsMessage->msgLen - MSG_HEADER_LEN;
  tsMessage->msgContent[2] = intConvert.c[0];
  tsMessage->msgContent[3] = intConvert.c[1];
  tsMessage->msgContent[4] = intConvert.c[2];
  tsMessage->msgContent[5] = intConvert.c[3];

  return tsMessage->msgLen;
}

/****************************************************************************
- Function name:  SendFlushASymbolToTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendFlushASymbolToTS(int tsIndex, int ECNIndex, unsigned char symbol[SYMBOL_LEN])
{
  if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage tsMessage;

  // Build flush a symbol message to send Trade Server
  BuildTSMessageForFlushASymbol(&tsMessage, ECNIndex, symbol);

  // Send message to Trade Server
  if (SendTSMessage(tsIndex, &tsMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request flush a symbol to %s\n", tradeServersInfo.config[tsIndex].description);
    
    CloseConnection(&tradeServersInfo.connection[tsIndex]);

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendFlushAllSymbolToTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendFlushAllSymbolToTS(int tsIndex, int ECNIndex)
{
  if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage tsMessage;

  TraceLog(DEBUG_LEVEL, "Send request %s flush book for all symbol, ECNIndex = %d\n", tradeServersInfo.config[tsIndex].description, ECNIndex);

  // Build flush all symbol message to send Trade Server
  BuildTSMessageForFlushAllSymbol(&tsMessage, ECNIndex);

  // Send message to Trade Server
  if (SendTSMessage(tsIndex, &tsMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request flush all symbol to %s\n", tradeServersInfo.config[tsIndex].description);
    CloseConnection(&tradeServersInfo.connection[tsIndex]);

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendEnableTradingAllOrderToAllTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendEnableTradingAllOrderToAllTS(void)
{
  int tsIndex;

  for (tsIndex = 0; tsIndex < tradeServersInfo.maxConnections; tsIndex++)
  {
    // Check connection between AS and TS
    if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
    {
      continue;
    }   
    
    // Call function to send enable trading all ECN order
    if (SendEnableTradingAllOrderToTS(tsIndex) == ERROR)
    {
      return ERROR;
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendEnableTradingAllOrderToAllTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendDisableTradingAllOrderToAllTS(short reasonID)
{
  int tsIndex;

  MaxStuckOfASAlert = NO;
  MaxLossOfASAlert = NO;
  TWTRequestAlert = NO;
  
  for (tsIndex = 0; tsIndex < tradeServersInfo.maxConnections; tsIndex++)
  {
    // Check connection between AS and TS
    if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
    {
      // Reset trading status
      DisableTradingAllVenues(tsIndex);

      continue;
    }
    
    // Call function to send disable trading all ECN order
    if (SendDisableTradingAllOrderToTS(tsIndex, reasonID) == ERROR)
    {
      return ERROR;
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendEnableTradingAllOrderToTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendEnableTradingAllOrderToTS(int tsIndex)
{
  int ECNIndex;

  for (ECNIndex = 0; ECNIndex < TS_MAX_ORDER_CONNECTIONS; ECNIndex++)
  {
    // Call function to send enable trading a ECN order
    if (SendEnableTradingAOrderToTS(tsIndex, ECNIndex) == ERROR)
    {
      return ERROR;
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendDisableTradingAllOrderToTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendDisableTradingAllOrderToTS(int tsIndex, short reasonID)
{
  if (SendDisableTradingAOrderToTS(tsIndex, TS_SERVICE_ALL_OECs, reasonID) == ERROR)
  {
    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendEnableTradingAOrderToTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendEnableTradingAOrderToTS(int tsIndex, int ECNIndex)
{
  if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage tsMessage;

  // Build enable trading message to send Trade Server
  BuildTSMessageForEnableTrading(&tsMessage, ECNIndex);

  // Send message to Trade Server
  if (SendTSMessage(tsIndex, &tsMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request to enable trading to %s\n", tradeServersInfo.config[tsIndex].description);
    
    CloseConnection(&tradeServersInfo.connection[tsIndex]);

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendDisableTradingAOrderToTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendDisableTradingAOrderToTS(int tsIndex, int ECNIndex, short reasonID)
{
  if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage tsMessage;

  if(reasonID >= 0)
  {
    reasonID = GetTraderIndex(traderToolsInfo.connection[reasonID].userName);
  }

  // Build disable trading message to send Trade Server
  BuildTSMessageForDisableTrading(&tsMessage, ECNIndex, reasonID);

  // Send message to Trade Server
  if (SendTSMessage(tsIndex, &tsMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request to disable trading to %s\n", tradeServersInfo.config[tsIndex].description);
    
    CloseConnection(&tradeServersInfo.connection[tsIndex]);

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendIgnoreStockListToAllTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendIgnoreStockListToAllTS(int type, const char *symbol)
{
  int tsIndex;
  t_TSMessage tsMessage;

  TraceLog(DEBUG_LEVEL, "Send ignored stock = %.8s to all TS ...\n", symbol);

  // Build ignore stock list message to send Trader Tool
  BuildTSMessageForSetIgnoreStockList(&tsMessage, type, symbol);

  for (tsIndex = 0; tsIndex < tradeServersInfo.maxConnections; tsIndex++)
  {
    if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
    {
      continue;
    }

    // Send ignore stock list to Trade Server
    if (SendTSMessage(tsIndex, &tsMessage) == ERROR)
    {
      return ERROR;
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendIgnoreStockRangesListToAllTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendIgnoreStockRangesListToAllTS(int type, const char *symbol)
{
  int tsIndex;
  t_TSMessage tsMessage;

  TraceLog(DEBUG_LEVEL, "Send ignored stock ranges = %.8s to all TS ...\n", symbol);

  // Build ignore stock list message to send Trader Tool
  BuildTSMessageForSetIgnoreStockRangesList(&tsMessage, type, symbol);

  for (tsIndex = 0; tsIndex < tradeServersInfo.maxConnections; tsIndex++)
  {
    if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
    {
      continue;
    }

    // Send ignore stock list to Trade Server
    if (SendTSMessage(tsIndex, &tsMessage) == ERROR)
    {
      return ERROR;
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendActiveVolatileSymbolsToTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendActiveVolatileSymbolsToTS(int tsIndex)
{
  if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage tsMessage;

  BuildTSMessageForActiveVolatileSymbolList(&tsMessage);

  // Send message to Trade Server
  if (SendTSMessage(tsIndex, &tsMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send volatile symbol list to %s\n", tradeServersInfo.config[tsIndex].description);

    CloseConnection(&tradeServersInfo.connection[tsIndex]);

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendAllVolatileToggleStatusToTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendAllVolatileToggleStatusToTS(int tsIndex)
{
  if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage tsMessage;

  BuildTSMessageForAllVolatileToggleStatus(&tsMessage);

  // Send message to Trade Server
  if (SendTSMessage(tsIndex, &tsMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send All Volatile Toggle Status to %s\n", tradeServersInfo.config[tsIndex].description);

    CloseConnection(&tradeServersInfo.connection[tsIndex]);

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendActiveVolatileSymbolsToAllTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendActiveVolatileSymbolsToAllTS()
{
  int tsIndex;
  t_TSMessage tsMessage;

  BuildTSMessageForActiveVolatileSymbolList(&tsMessage);

  for (tsIndex = 0; tsIndex < tradeServersInfo.maxConnections; tsIndex++)
  {
    if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
    {
      continue;
    }

    if (SendTSMessage(tsIndex, &tsMessage) == ERROR)
    {
      return ERROR;
    }
    else
    {
      TraceLog(DEBUG_LEVEL, "Sent Volatile symbol list to TS %d\n", tsIndex + 1);
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendAllVolatileToggleStatusToAllTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendAllVolatileToggleStatusToAllTS()
{
  int tsIndex;
  t_TSMessage tsMessage;

  BuildTSMessageForAllVolatileToggleStatus(&tsMessage);

  for (tsIndex = 0; tsIndex < tradeServersInfo.maxConnections; tsIndex++)
  {
    if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
    {
      continue;
    }

    if (SendTSMessage(tsIndex, &tsMessage) == ERROR)
    {
      return ERROR;
    }
    else
    {
      TraceLog(DEBUG_LEVEL, "Info: Sent All Volatile Toggle Status to TS %d\n", tsIndex + 1);
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendSymbolLockCounterForASymbolToTradeServers
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendSymbolLockCounterForASymbolToTradeServers(const char *symbol, int account, const int lockCount)
{
  int tsIndex;
  t_TSMessage tsMessage;

  // Build message
  BuildTSMessageForSetLockCounterForASymbol(&tsMessage, symbol, lockCount);
  
  // Send to specific Trade Servers (Trade Servers with correct account)
  int sent = 0;
  for (tsIndex = 0; tsIndex < tradeServersInfo.maxConnections; tsIndex++)
  {
    if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
    {
      continue;
    }

    // Check Trade Server's trading account
    if (TradingAccount.accountForTS[tsIndex] == account)
    {
      if (SendTSMessage(tsIndex, &tsMessage) == ERROR)
      {
        return ERROR;
      }
      sent = 1;
    }
  }
  
  if (sent == 1)
    TraceLog(DEBUG_LEVEL, "Send symbol lock counter for symbol: %.8s (lockCounter: %d) to Trade Server\n", symbol, lockCount);

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendStuckUnlockForASymbolToTradeServers
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendStuckUnlockForASymbolToTradeServers(const char *symbol, int account, const int lockCount)
{
  int tsIndex;
  t_TSMessage tsMessage;

  // Build message
  BuildTSMessageForSetStuckUnlockForASymbol(&tsMessage, symbol, lockCount);
  
  // Send to specific Trade Servers (Trade Servers with correct account)
  int sent = 0;
  for (tsIndex = 0; tsIndex < tradeServersInfo.maxConnections; tsIndex++)
  {
    if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
    {
      continue;
    }

    // Check Trade Server's trading account
    if (TradingAccount.accountForTS[tsIndex] == account)
    {
      if (SendTSMessage(tsIndex, &tsMessage) == ERROR)
      {
        return ERROR;
      }
      sent = 1;
    }
  }
  
  if (sent == 1)
    TraceLog(DEBUG_LEVEL, "Send stuck status unlock for symbol: %.8s (lockCounter: %d) to Trade Server\n", symbol, lockCount);

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendIgnoreStockListToTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendIgnoreStockListToTS(int tsIndex)
{
  if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
  {
    return SUCCESS;
  }
  
  t_TSMessage tsMessage;

  TraceLog(DEBUG_LEVEL, "Send ignored stock list to TS ...\n");

  // Build ignore stock list message to send Trader Tool
  BuildTSMessageForIgnoreStockList(&tsMessage);

  // Send message to Trader Tool
  if (SendTSMessage(tsIndex, &tsMessage) == ERROR)
  {
    return ERROR;
  }
  
  return SUCCESS;
}


/****************************************************************************
- Function name:  SendIgnoreStockRangesListToTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendIgnoreStockRangesListToTS(int tsIndex)
{
  if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
  {
    return SUCCESS;
  }
  
  t_TSMessage tsMessage;

  TraceLog(DEBUG_LEVEL, "Send ignored stock ranges list to TS ...\n");

  // Build ignore stock list message to send Trader Tool
  BuildTSMessageForIgnoreStockRangesList(&tsMessage);

  // Send message to Trader Tool
  if (SendTSMessage(tsIndex, &tsMessage) == ERROR)
  {
    return ERROR;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendStuckSymbolsToTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendStuckSymbolsToTS(int tsIndex)
{
  t_TSMessage tsMessage;

  // Build message
  int count = 0;

  BuildTSMessageForCurrentSymbolsStuckStatus(&tsMessage, tsIndex, &count);

  if (count == 0) return SUCCESS;
  
  if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
  {
    return SUCCESS;
  }
  else
  {
    if (SendTSMessage(tsIndex, &tsMessage) == ERROR)
    {
      return ERROR;
    }

    TraceLog(DEBUG_LEVEL, "Send all symbols which having stuck to Trade Server %d\n", tsIndex+1);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTSConnectAllConnection
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTSConnectAllConnection(int tsIndex, int ttIndex)
{
  // TS connect to all ECN Book
  if (ProcessTSConnectAllECNBook(tsIndex) == ERROR)
  {
    return ERROR;
  }

  // TS connect to all ECN Order
  if (ProcessTSConnectAllECNOrder(tsIndex) == ERROR)
  {
    return ERROR;
  }

  if (SendSetISO_TradingToTS(tsIndex, ENABLE) == ERROR)
  {
    return ERROR;
  }
  
  BackupTSGlobalConf[tsIndex].ISO_Trading = ENABLE;
  
  if (ttIndex != -1)
  {
    // Update TT event log: Enable/disable ISO trading on TS
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    int tsId = tradeServersInfo.config[tsIndex].tsId;
    sprintf(activityDetail, "%d,%d,%d", traderToolsInfo.connection[ttIndex].currentState, ENABLE, tsId);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_ENABLE_DISENABLE_ISO_TRADING, activityDetail);
      
    //Create a ISO Record
    InsertISOToggleToISOFlightFile(tsId, ENABLE, traderToolsInfo.connection[ttIndex].userName);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTSConnectAllECNBook
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTSConnectAllECNBook(int tsIndex)
{
  int ECNIndex;

  for (ECNIndex = 0; ECNIndex < TS_MAX_BOOK_CONNECTIONS; ECNIndex++)
  {
    // Check book enable
    if (tradeServersInfo.tsSetting[tsIndex].bookEnable[ECNIndex] == YES)
    {
      // Call function to send connect to a ECN Book
      if (SendConnectBookToTS(tsIndex, ECNIndex) == ERROR)
      {
        return ERROR;
      }
    }
  }
  
  // Connect to CQS
  if (tradeServersInfo.tsSetting[tsIndex].bboEnable[CQS_SOURCE] == YES)
  {
    if (SendConnectToCQSnUQDFToTS(tsIndex, CQS_SOURCE) == ERROR)
    {
      return ERROR;
    }
  }
  
  // Connect to UQDF
  if (tradeServersInfo.tsSetting[tsIndex].bboEnable[UQDF_SOURCE] == YES)
  {
    if (SendConnectToCQSnUQDFToTS(tsIndex, UQDF_SOURCE) == ERROR)
    {
      return ERROR;
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTSConnectAllECNOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTSConnectAllECNOrder(int tsIndex)
{
  int ECNIndex;

  for (ECNIndex = 0; ECNIndex < TS_MAX_ORDER_CONNECTIONS; ECNIndex++)
  {
    // Check order enable
    if (tradeServersInfo.tsSetting[tsIndex].orderEnable[ECNIndex] == YES)
    {
      // Call function to send connect to a ECN Order
      if (SendConnectOrderToTS(tsIndex, ECNIndex) == ERROR)
      {
        return ERROR;
      }
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTSDisconnectAllBookAndOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTSDisconnectAllBookAndOrder(void)
{
  int tsIndex;

  for (tsIndex = 0; tsIndex < tradeServersInfo.maxConnections; tsIndex++)
  {
    // Check connection between AS and TS
    if (tradeServersInfo.connection[tsIndex].status != DISCONNECT)
    {
      // TS disconnect to all ECN Book
      if (ProcessTSDisconnectAllECNBook(tsIndex) == ERROR)
      {
        return ERROR;
      }

      // TS disconnect to all ECN Order
      if (ProcessTSDisconnectAllECNOrder(tsIndex) == ERROR)
      {
        return ERROR;
      }
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTSDisconnectAllECNBook
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTSDisconnectAllECNBook(int tsIndex)
{
  int ECNIndex;

  for (ECNIndex = 0; ECNIndex < TS_MAX_BOOK_CONNECTIONS; ECNIndex++)
  {
    // Call function to send disconnect to a ECN Book
    if (SendDisconnectBookToTS(tsIndex, ECNIndex) == ERROR)
    {
      return ERROR;
    }
  }
  
  // Disconnect to CQS
  if (SendDisconnectToCQSnUQDFToTS(tsIndex, CQS_SOURCE) == ERROR)
  {
    return ERROR;
  }
  
  // Disconnect to UQDF
  if (SendDisconnectToCQSnUQDFToTS(tsIndex, UQDF_SOURCE) == ERROR)
  {
    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTSDisconnectAllECNOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTSDisconnectAllECNOrder(int tsIndex)
{
  int ECNIndex;

  for (ECNIndex = 0; ECNIndex < TS_MAX_ORDER_CONNECTIONS; ECNIndex++)
  {
    // Call function to send disconnect to a ECN Order
    if (SendDisconnectOrderToTS(tsIndex, ECNIndex) == ERROR)
    {
      return ERROR;
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SetTSBookARCAFeedToTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SetTSBookARCAFeedToTS(int tsIndex, char feedName)
{
  if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage tsMessage;

  // Build set Book configuration message to send Trade Server
  BuildTSMessageForSetTSBookARCAFeed(&tsMessage, tsIndex, feedName);

  // Send message to Trade Server
  if (SendTSMessage(tsIndex, &tsMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request to set ARCA Book Feed to %s\n", tradeServersInfo.config[tsIndex].description);

    CloseConnection(&tradeServersInfo.connection[tsIndex]);

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SetTSBookARCAConfToTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SetTSBookARCAConfToTS(int tsIndex, int type, int groupIndex)
{
  if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage tsMessage;

  // Build set Book configuration message to send Trade Server
  BuildTSMessageForSetTSBookARCAConf(&tsMessage, tsIndex, type, groupIndex);

  // Send message to Trade Server
  if (SendTSMessage(tsIndex, &tsMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request to set Book configuration to %s\n", tradeServersInfo.config[tsIndex].description);

    CloseConnection(&tradeServersInfo.connection[tsIndex]);

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  BuildTSMessageForSetTSBookARCAFeed
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTSMessageForSetTSBookARCAFeed(void *message, int tsIndex, char feedName)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *tsMessage = (t_TSMessage *)message;
  
  // Initialize message
  tsMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SET_ARCA_BOOK_FEED_FROM_TS;
  tsMessage->msgContent[0] = shortConvert.c[0];
  tsMessage->msgContent[1] = shortConvert.c[1];
  tsMessage->msgLen += 2;

  // Update index of body length
  tsMessage->msgLen += 4;

  // Update index of block length
  tsMessage->msgLen += 4;

  // Block content = FeedName 1 byte char
  tsMessage->msgContent[tsMessage->msgLen] = feedName;
  tsMessage->msgLen += 1;

  // Block length field
  intConvert.value = tsMessage->msgLen - MSG_HEADER_LEN;
  tsMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  tsMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  tsMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  tsMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = tsMessage->msgLen - MSG_HEADER_LEN;
  tsMessage->msgContent[2] = intConvert.c[0];
  tsMessage->msgContent[3] = intConvert.c[1];
  tsMessage->msgContent[4] = intConvert.c[2];
  tsMessage->msgContent[5] = intConvert.c[3];

  return tsMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildTSMessageForSetTSBookARCAConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTSMessageForSetTSBookARCAConf(void *message, int tsIndex, int type, int groupIndex)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *tsMessage = (t_TSMessage *)message;
  
  // Initialize message
  tsMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SET_BOOK_CONF_FROM_TS;
  tsMessage->msgContent[0] = shortConvert.c[0];
  tsMessage->msgContent[1] = shortConvert.c[1];
  tsMessage->msgLen += 2;

  // Update index of body length
  tsMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  
  // Update index of block length
  tsMessage->msgLen += 4;

  // Block content = ECN Book Id (1 byte) + ECN Book configuration

  // ECN Book Id (1 byte)
  tsMessage->msgContent[tsMessage->msgLen] = TS_ARCA_BOOK_INDEX;

  // Update index of body length
  tsMessage->msgLen += 1;

  // ECN Book configuration
  int size;
  
  if (type == UPDATE_SOURCE_ID)
  {
    tsMessage->msgContent[tsMessage->msgLen] = type;
    tsMessage->msgLen += 1;

    memcpy(&tsMessage->msgContent[tsMessage->msgLen], tradeServersInfo.ECNBooksConf[tsIndex].ARCABookConf.sourceID, MAX_LINE_LEN);
    tsMessage->msgLen += MAX_LINE_LEN;

    size = MAX_LINE_LEN + 1;
  }
  else
  {
    tsMessage->msgContent[tsMessage->msgLen] = type;
    tsMessage->msgLen += 1;

    tsMessage->msgContent[tsMessage->msgLen] = groupIndex;
    tsMessage->msgLen += 1;

    memcpy(&tsMessage->msgContent[tsMessage->msgLen], tradeServersInfo.ECNBooksConf[tsIndex].ARCABookConf.retranConn[groupIndex].ip, MAX_LINE_LEN);
    tsMessage->msgLen += MAX_LINE_LEN;

    memcpy(&tsMessage->msgContent[tsMessage->msgLen], &tradeServersInfo.ECNBooksConf[tsIndex].ARCABookConf.retranConn[groupIndex].port, 4);
    tsMessage->msgLen += 4;

    size = MAX_LINE_LEN + 6;
  }

  // Block length field
  intConvert.value = size + 5;
  tsMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  tsMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  tsMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  tsMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = tsMessage->msgLen - MSG_HEADER_LEN;
  tsMessage->msgContent[2] = intConvert.c[0];
  tsMessage->msgContent[3] = intConvert.c[1];
  tsMessage->msgContent[4] = intConvert.c[2];
  tsMessage->msgContent[5] = intConvert.c[3];

  return tsMessage->msgLen;
}

/****************************************************************************
- Function name:  SetTSBookNYSEOpenUltraConfToTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SetTSBookNYSEOpenUltraConfToTS(int tsIndex, int type, int groupIndex)
{
  if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage tsMessage;

  // Build set Book configuration message to send Trade Server
  BuildTSMessageForSetTSBookNYSEOpenUltraConf(&tsMessage, tsIndex, type, groupIndex);

  // Send message to Trade Server
  if (SendTSMessage(tsIndex, &tsMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request to set Book configuration to %s\n", tradeServersInfo.config[tsIndex].description);

    CloseConnection(&tradeServersInfo.connection[tsIndex]);

    return ERROR;
  }
  return SUCCESS;
}

/****************************************************************************
- Function name:  BuildTSMessageForSetTSBookNYSEOpenUltraConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTSMessageForSetTSBookNYSEOpenUltraConf(void *message, int tsIndex, int type, int groupIndex)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *tsMessage = (t_TSMessage *)message;

  // Initialize message
  tsMessage->msgLen = 0;

  // Message type
  shortConvert.value = MSG_TYPE_FOR_SET_BOOK_CONF_FROM_TS;
  tsMessage->msgContent[0] = shortConvert.c[0];
  tsMessage->msgContent[1] = shortConvert.c[1];
  tsMessage->msgLen += 2;

  // Update index of body length
  tsMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))

  // Update index of block length
  tsMessage->msgLen += 4;

  // Block content = ECN Book Id (1 byte) + ECN Book configuration

  // ECN Book Id (1 byte)
  tsMessage->msgContent[tsMessage->msgLen] = TS_NYSE_BOOK_INDEX;

  // Update index of body length
  tsMessage->msgLen += 1;

  // ECN Book configuration
  int size;

  if (type == UPDATE_SOURCE_ID)
  {
    tsMessage->msgContent[tsMessage->msgLen] = type;
    tsMessage->msgLen += 1;

    memcpy(&tsMessage->msgContent[tsMessage->msgLen], tradeServersInfo.ECNBooksConf[tsIndex].NYSEBookConf.sourceID, MAX_LINE_LEN);
    tsMessage->msgLen += MAX_LINE_LEN;

    size = MAX_LINE_LEN + 1;
  }

  if (type == UPDATE_RETRANSMISSION_REQUEST)
  {
    tsMessage->msgContent[tsMessage->msgLen] = type;
    tsMessage->msgLen += 1;

    memcpy(&tsMessage->msgContent[tsMessage->msgLen], &tradeServersInfo.ECNBooksConf[tsIndex].NYSEBookConf.nyseBook[groupIndex].index, 4);
    tsMessage->msgLen +=1;

    memcpy(&tsMessage->msgContent[tsMessage->msgLen], tradeServersInfo.ECNBooksConf[tsIndex].NYSEBookConf.nyseBook[groupIndex].retranIP, MAX_LINE_LEN);
    tsMessage->msgLen += MAX_LINE_LEN;

    memcpy(&tsMessage->msgContent[tsMessage->msgLen], &tradeServersInfo.ECNBooksConf[tsIndex].NYSEBookConf.nyseBook[groupIndex].retranPort, 4);
    tsMessage->msgLen +=4;

    size = MAX_LINE_LEN + 2 + 4;
  }

  // Block length field
  intConvert.value = size + 5;
  tsMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  tsMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  tsMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  tsMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];

  // Body length field
  intConvert.value = tsMessage->msgLen - MSG_HEADER_LEN;
  tsMessage->msgContent[2] = intConvert.c[0];
  tsMessage->msgContent[3] = intConvert.c[1];
  tsMessage->msgContent[4] = intConvert.c[2];
  tsMessage->msgContent[5] = intConvert.c[3];

  return tsMessage->msgLen;
}

/****************************************************************************
- Function name:  SendCancelRequestToTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendCancelRequestToTS(int tsIndex, int clOrdId, long ECNOrderId, char *symbol, char side)
{
  if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage tsMessage;

  // Build Cancel Arca Direct message to send to Trade Server
  BuildTSMessageForCancelARCA_DIRECT(&tsMessage, clOrdId, ECNOrderId, symbol, side);

  // Send message to Trade Server
  if (SendTSMessage(tsIndex, &tsMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send ARCA DIRECT cancel request to %s\n", tradeServersInfo.config[tsIndex].description);

    CloseConnection(&tradeServersInfo.connection[tsIndex]);

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  BuildTSMessageForCancelARCA_DIRECT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTSMessageForCancelARCA_DIRECT(void *message, int clOrdId, long ECNOrderId, char *symbol, char side)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *tsMessage = (t_TSMessage *)message;
  
  // Initialize message
  tsMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_ARCA_DIRECT_CACNCEL_REQUEST_TO_TS;
  tsMessage->msgContent[0] = shortConvert.c[0];
  tsMessage->msgContent[1] = shortConvert.c[1];
  tsMessage->msgLen += 2;

  // Update index of body length
  tsMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  
  // Update index of block length
  tsMessage->msgLen += 4;

  // Block content = clOrdId + ECN Order Id + symbol + side

  // clOrdId
  intConvert.value = clOrdId;
  tsMessage->msgContent[tsMessage->msgLen] = intConvert.c[0];
  tsMessage->msgContent[tsMessage->msgLen + 1] = intConvert.c[1];
  tsMessage->msgContent[tsMessage->msgLen + 2] = intConvert.c[2];
  tsMessage->msgContent[tsMessage->msgLen + 3] = intConvert.c[3];

  // Update index
  tsMessage->msgLen += 4;

  // ECN Order Id
  memcpy(&tsMessage->msgContent[tsMessage->msgLen], &ECNOrderId, 8);
  tsMessage->msgLen += 8;

  // symbol
  strncpy((char *) &tsMessage->msgContent[tsMessage->msgLen], symbol, SYMBOL_LEN);

  // Update index
  tsMessage->msgLen += SYMBOL_LEN;

  // side
  tsMessage->msgContent[tsMessage->msgLen] = side;

  // Update index
  tsMessage->msgLen += 1;

  // Block length field
  intConvert.value = tsMessage->msgLen - MSG_HEADER_LEN;
  tsMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  tsMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  tsMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  tsMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = tsMessage->msgLen - MSG_HEADER_LEN;
  tsMessage->msgContent[2] = intConvert.c[0];
  tsMessage->msgContent[3] = intConvert.c[1];
  tsMessage->msgContent[4] = intConvert.c[2];
  tsMessage->msgContent[5] = intConvert.c[3];

  return tsMessage->msgLen;
}

/****************************************************************************
- Function name:  SendHaltStockListToAllTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendHaltStockListToAllTS(int updatedList[MAX_STOCK_SYMBOL], int updatedCount)
{
  int tsIndex;

  TraceLog(DEBUG_LEVEL, "Send halted stock list to all TS ...\n");

  for (tsIndex = 0; tsIndex < tradeServersInfo.maxConnections; tsIndex++)
  {
    // Check connection between AS and TS
    if (tradeServersInfo.connection[tsIndex].status != DISCONNECT)    
    {
      // Call function to send halted stock list to TS
      if (SendHaltStockListToTS(tsIndex, updatedList, updatedCount) == ERROR)
      {
        return ERROR;
      }
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendAHaltStockToAllTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendAHaltStockToAllTS(const int stockSymbolIndex)
{
  int tsIndex;

  TraceLog(DEBUG_LEVEL, "Send halted stock to all TS...\n");  

  for (tsIndex = 0; tsIndex < tradeServersInfo.maxConnections; tsIndex++)
  {
    // Check connection between AS and TS
    if (tradeServersInfo.connection[tsIndex].status != DISCONNECT)    
    {
      // Call function to send halted stock list to TS
      if (SendHaltSymbolToTS(tsIndex, stockSymbolIndex) == ERROR)
      {
        return ERROR;
      }
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendHaltStockListToTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendHaltStockListToTS(const int tsIndex, int updatedList[MAX_STOCK_SYMBOL], int updatedCount)
{
  if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage tsMessage;

  // Build halted stock list to send Trade Server
  int count = BuildTSMessageForHaltedSymbolList(&tsMessage, updatedList, updatedCount);
  if (count == 0) return SUCCESS; //There is nothing to send

  // Send message to Trade Server
  if (SendTSMessage(tsIndex, &tsMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send halted stock list to %s\n", tradeServersInfo.config[tsIndex].description);

    CloseConnection(&tradeServersInfo.connection[tsIndex]);

    return ERROR;
  }
  else
  {
    TraceLog(DEBUG_LEVEL, "Sent %d halt stock(s) to TS\n", count);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendHaltSymbolToTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendHaltSymbolToTS(const int tsIndex, const int stockSymbolIndex)
{
  if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage tsMessage;

  // Build halted stock list to send Trade Server
  if (BuildTSMessageForHaltSymbol(&tsMessage, stockSymbolIndex) == 0) return SUCCESS; //There is nothing to send

  // Send message to Trade Server
  if (SendTSMessage(tsIndex, &tsMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send a halted stock to %s\n", tradeServersInfo.config[tsIndex].description);

    CloseConnection(&tradeServersInfo.connection[tsIndex]);

    return ERROR;
  }
  else
  {
    TraceLog(DEBUG_LEVEL, "Sent a halt stock to TS (%d)\n", tsIndex);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  BuildTSMessageForHaltedSymbolList
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTSMessageForHaltedSymbolList(void *message, int updatedList[MAX_STOCK_SYMBOL], int updatedCount)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *tsMessage = (t_TSMessage *)message;
  
  // Initialize message
  tsMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_HALTED_SYMBOL_LIST_TO_TS;
  tsMessage->msgContent[0] = shortConvert.c[0];
  tsMessage->msgContent[1] = shortConvert.c[1];
  tsMessage->msgLen += 2;

  // Update index of body length
  tsMessage->msgLen += 4;
  
  // Update index of block length
  tsMessage->msgLen += 4;

  tsMessage->msgLen += 4; //By pass number of symbols
  
  int i, j, count = 0;
  int willSend;
  if (updatedCount == -1)
  {
    for (i = 0; i < RMList.countSymbol; i++)
    {
      willSend = 0;
      for (j = 0; j < TS_MAX_BOOK_CONNECTIONS; j++)
      {
        willSend += HaltStatusMgmt.haltTime[i][j];
      }
      
      if (willSend)
      {
        count++;
        memcpy(&tsMessage->msgContent[tsMessage->msgLen], HaltStatusMgmt.symbol[i], SYMBOL_LEN);
        tsMessage->msgLen += SYMBOL_LEN;
        
        for (j = 0; j < TS_MAX_BOOK_CONNECTIONS; j++)
        {
          tsMessage->msgContent[tsMessage->msgLen] = HaltStatusMgmt.haltStatus[i][j];
          tsMessage->msgLen++;
          
          memcpy(&tsMessage->msgContent[tsMessage->msgLen], &HaltStatusMgmt.haltTime[i][j], 4);
          tsMessage->msgLen += 4;
        }
      }
    }
  }
  else  //Only send from updated list
  {
    int stockSymbolIndex;
    for (i = 0; i < updatedCount; i++)
    {
      stockSymbolIndex = updatedList[i];
      willSend = 0;
      for (j = 0; j < TS_MAX_BOOK_CONNECTIONS; j++)
      {
        willSend += HaltStatusMgmt.haltTime[stockSymbolIndex][j];
      }
      
      if (willSend)
      {
        count++;
        memcpy(&tsMessage->msgContent[tsMessage->msgLen], HaltStatusMgmt.symbol[stockSymbolIndex], SYMBOL_LEN);
        tsMessage->msgLen += SYMBOL_LEN;
        
        for (j = 0; j < TS_MAX_BOOK_CONNECTIONS; j++)
        {
          tsMessage->msgContent[tsMessage->msgLen] = HaltStatusMgmt.haltStatus[stockSymbolIndex][j];
          tsMessage->msgLen++;
          
          memcpy(&tsMessage->msgContent[tsMessage->msgLen], &HaltStatusMgmt.haltTime[stockSymbolIndex][j], 4);
          tsMessage->msgLen += 4;
        }
      }
    }
  }
  
  //Update 'count' field
  memcpy(&tsMessage->msgContent[10], &count, 4);
  
  // Block length field
  intConvert.value = tsMessage->msgLen - MSG_HEADER_LEN;
  tsMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  tsMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  tsMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  tsMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = tsMessage->msgLen - MSG_HEADER_LEN;
  tsMessage->msgContent[2] = intConvert.c[0];
  tsMessage->msgContent[3] = intConvert.c[1];
  tsMessage->msgContent[4] = intConvert.c[2];
  tsMessage->msgContent[5] = intConvert.c[3];

  return count;
}

/****************************************************************************
- Function name:  BuildTSMessageForHaltSymbol
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTSMessageForHaltSymbol(void *message, const int stockSymbolIndex)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *tsMessage = (t_TSMessage *)message;
  
  // Initialize message
  tsMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_A_HALTED_SYMBOL_TO_TS;
  tsMessage->msgContent[0] = shortConvert.c[0];
  tsMessage->msgContent[1] = shortConvert.c[1];
  tsMessage->msgLen += 2;

  // Update index of body length
  tsMessage->msgLen += 4;
  
  // Update index of block length
  tsMessage->msgLen += 4;

  int i;
  int willSend = 0;
  
  for (i = 0; i < TS_MAX_BOOK_CONNECTIONS; i++)
  {
    willSend += HaltStatusMgmt.haltTime[stockSymbolIndex][i];
  }
  
  if (willSend)
  {
    memcpy(&tsMessage->msgContent[tsMessage->msgLen], HaltStatusMgmt.symbol[stockSymbolIndex], SYMBOL_LEN);
    tsMessage->msgLen += SYMBOL_LEN;
      
    for (i = 0; i < TS_MAX_BOOK_CONNECTIONS; i++)
    {
      tsMessage->msgContent[tsMessage->msgLen] = HaltStatusMgmt.haltStatus[stockSymbolIndex][i];
      tsMessage->msgLen++;
        
      memcpy(&tsMessage->msgContent[tsMessage->msgLen], &HaltStatusMgmt.haltTime[stockSymbolIndex][i], 4);
      tsMessage->msgLen += 4;
    }
  }
  else
  {
    return 0;
  }
  
  // Block length field
  intConvert.value = tsMessage->msgLen - MSG_HEADER_LEN;
  tsMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  tsMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  tsMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  tsMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = tsMessage->msgLen - MSG_HEADER_LEN;
  tsMessage->msgContent[2] = intConvert.c[0];
  tsMessage->msgContent[3] = intConvert.c[1];
  tsMessage->msgContent[4] = intConvert.c[2];
  tsMessage->msgContent[5] = intConvert.c[3];

  return 1;
}

/****************************************************************************
- Function name:  BuildTSMessageForActiveVolatileSymbolList
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTSMessageForActiveVolatileSymbolList(void *message)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *tsMessage = (t_TSMessage *)message;
  
  // Initialize message
  tsMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_VOLATILE_SYMBOL_LIST_TO_TS;
  tsMessage->msgContent[0] = shortConvert.c[0];
  tsMessage->msgContent[1] = shortConvert.c[1];
  tsMessage->msgLen += 2;

  // Update index of body length
  tsMessage->msgLen += 4;

  /* Message body = count volatile (4) + volatile list (8*count volatile)*/
  tsMessage->msgLen += 4;
  
  char activeList[MAX_STOCK_SYMBOL * SYMBOL_LEN];
  int countActive = GetActiveVolatileSymbols(activeList);
  
  memcpy(&tsMessage->msgContent[tsMessage->msgLen], &countActive, 4);
  tsMessage->msgLen += 4;
    
  if (countActive > 0)
  {
    memcpy(&tsMessage->msgContent[tsMessage->msgLen], activeList, countActive * SYMBOL_LEN);
    tsMessage->msgLen += (countActive * SYMBOL_LEN);
  }
  
  // Block length field
  intConvert.value = tsMessage->msgLen - MSG_HEADER_LEN;
  tsMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  tsMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  tsMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  tsMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  tsMessage->msgContent[2] = intConvert.c[0];
  tsMessage->msgContent[3] = intConvert.c[1];
  tsMessage->msgContent[4] = intConvert.c[2];
  tsMessage->msgContent[5] = intConvert.c[3];

  return tsMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildTSMessageForAllVolatileToggleStatus
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTSMessageForAllVolatileToggleStatus(void *message)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *tsMessage = (t_TSMessage *)message;
  
  // Initialize message
  tsMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_ALL_VOLATILE_VALUE_TO_TS;
  tsMessage->msgContent[0] = shortConvert.c[0];
  tsMessage->msgContent[1] = shortConvert.c[1];
  tsMessage->msgLen += 2;

  // Update index of body length
  tsMessage->msgLen += 4;

  /* Message body: 1 byte status (0: off, 1: on) */
  tsMessage->msgLen += 4;
  
  tsMessage->msgContent[tsMessage->msgLen] = volatileMgmt.currentAllVolatileValue;
  tsMessage->msgLen ++;
  
  // Block length field
  intConvert.value = tsMessage->msgLen - MSG_HEADER_LEN;
  tsMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  tsMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  tsMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  tsMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  tsMessage->msgContent[2] = intConvert.c[0];
  tsMessage->msgContent[3] = intConvert.c[1];
  tsMessage->msgContent[4] = intConvert.c[2];
  tsMessage->msgContent[5] = intConvert.c[3];

  return tsMessage->msgLen;
}

/****************************************************************************
- Function name:  SendEtbSymbolListToTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendEtbSymbolListToTS(const int tsIndex)
{
  if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage tsMessage;

  //clean the prevStatus to send the whole ETB Symbol List
  char prevStatus[MAX_STOCK_SYMBOL];
  memset(prevStatus, 0, sizeof(prevStatus));
  
  // Build ETB Symbol List to send TS
  if (BuildTSMessageForEtbSymbolList(&tsMessage, prevStatus) == 0)
  {
    TraceLog(DEBUG_LEVEL, "No symbol in ETB Symbol List\n");
    return SUCCESS;
  }

  // Send message to Trade Server
  if (SendTSMessage(tsIndex, &tsMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send ETB Symbol List to %s\n", tradeServersInfo.config[tsIndex].description);

    CloseConnection(&tradeServersInfo.connection[tsIndex]);

    return ERROR;
  }
  else
  {
    TraceLog(DEBUG_LEVEL, "Sent ETB Symbol List to TS (%d)\n", tsIndex);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendEtbSymbolListToAllTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendEtbSymbolListToAllTS(char prevStatus[MAX_STOCK_SYMBOL])
{
  int tsIndex;
  t_TSMessage tsMessage;

  if (BuildTSMessageForEtbSymbolList(&tsMessage, prevStatus) == 0)
  {
    TraceLog(DEBUG_LEVEL, "There is not any updated symbol in ETB Symbol List\n");
    return SUCCESS;
  }

  for (tsIndex = 0; tsIndex < tradeServersInfo.maxConnections; tsIndex++)
  {
    if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
    {
      continue;
    }

    if (SendTSMessage(tsIndex, &tsMessage) == ERROR)
    {
      return ERROR;
    }
    else
    {
      TraceLog(DEBUG_LEVEL, "Sent ETB Symbol List to TS %d\n", tsIndex + 1);
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  BuildTSMessageForEtbSymbolList
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTSMessageForEtbSymbolList(void *message, char prevStatus[MAX_STOCK_SYMBOL])
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *tsMessage = (t_TSMessage *)message;
  
  // Initialize message
  tsMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_ETB_SYMBOL_LIST_TO_TS;
  tsMessage->msgContent[0] = shortConvert.c[0];
  tsMessage->msgContent[1] = shortConvert.c[1];
  tsMessage->msgLen += 2;

  // Update index of body length
  tsMessage->msgLen += 4;

  /* Message body = delta(4) + total(4) + ETB list (9*count)*/
  tsMessage->msgLen += 4;
  
  int delta = 0;
  tsMessage->msgLen += 4;
  
  // Total symbols
  intConvert.value = SSLMgmt.total;
  memcpy(&tsMessage->msgContent[tsMessage->msgLen], intConvert.c, 4);
  tsMessage->msgLen += 4;
  
  int i;
  for (i = 0; i < symbolDetectCrossList.countSymbol; i++)
  {
    if (SSLMgmt.ssEnable[i] != prevStatus[i])
    {
      //This symbol is added or removed
      delta++;
      memcpy(&tsMessage->msgContent[tsMessage->msgLen], &symbolDetectCrossList.symbolList[i], SYMBOL_LEN);
      tsMessage->msgLen += SYMBOL_LEN;
      
      //Action: 0 - remove from ETB List (0 - can not short sell)
      //      1 - add to ETB List (1 - be able to short sell)
      tsMessage->msgContent[tsMessage->msgLen] = SSLMgmt.ssEnable[i];
      tsMessage->msgLen++;
    }
    else
    {
      //This symbol does not change state
    }
  }
  
  // Block length field
  intConvert.value = tsMessage->msgLen - MSG_HEADER_LEN;
  tsMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  tsMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  tsMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  tsMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  tsMessage->msgContent[2] = intConvert.c[0];
  tsMessage->msgContent[3] = intConvert.c[1];
  tsMessage->msgContent[4] = intConvert.c[2];
  tsMessage->msgContent[5] = intConvert.c[3];
  
  // Number of Symbol
  intConvert.value = delta;
  tsMessage->msgContent[10] = intConvert.c[0];
  tsMessage->msgContent[11] = intConvert.c[1];
  tsMessage->msgContent[12] = intConvert.c[2];
  tsMessage->msgContent[13] = intConvert.c[3];

  return tsMessage->msgLen;
}

/****************************************************************************
- Function name:  ProcessTSMessageForBookIdleWarning
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTSMessageForBookIdleWarning(int tsIndex, void *message)
{
  t_TSMessage *tsMessage = (t_TSMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  //ECN ID
  int ecnID = GetIntNumber(&tsMessage->msgContent[currentIndex]);
  currentIndex += 4;
  
  //GroupID
  int groupID = GetIntNumber(&tsMessage->msgContent[currentIndex]);
  currentIndex += 4;

  TraceLog(DEBUG_LEVEL, "Receive Book status idle from TS, Book(%d) Group(%d) TS-Index(%d)\n", ecnID, groupID, tsIndex);

  //Send the warning to TT
  SendTSPMBookIdleWarningToAllDaedalus(tsIndex, ecnID, groupID);
  SendTSPMBookIdleWarningToAllTT(tsIndex, ecnID, groupID);
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTSMessageForDisconnectAlert
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessTSMessageForDisconnectAlert(int tsIndex, void *message)
{
  t_TSMessage *tsMessage = (t_TSMessage *)message;

  short serviceID;

  memcpy(&serviceID, &tsMessage->msgContent[10], 2);

  char service[128];

  sprintf(service, "%s ", tradeServersInfo.config[tsIndex].description);
  
  switch(serviceID)
  {
    case TS_SERVICE_ARCA_BOOK:
      tradeServersInfo.currentStatus[tsIndex].tsMdcStatus[TS_ARCA_BOOK_INDEX] = DISCONNECT;
      strcat(service, "ARCA MDC");
      break;
    case TS_SERVICE_NASDAQ_BOOK:
      tradeServersInfo.currentStatus[tsIndex].tsMdcStatus[TS_NASDAQ_BOOK_INDEX] = DISCONNECT;
      strcat(service, "NASDAQ MDC");
      break;
    case TS_SERVICE_NYSE_BOOK:
      tradeServersInfo.currentStatus[tsIndex].tsMdcStatus[TS_NYSE_BOOK_INDEX] = DISCONNECT;
      strcat(service, "NYSE MDC");
      break;
    case TS_SERVICE_BATSZ_BOOK:
      tradeServersInfo.currentStatus[tsIndex].tsMdcStatus[TS_BATSZ_BOOK_INDEX] = DISCONNECT;
      strcat(service, "BZX MDC");
      break;
    case TS_SERVICE_EDGX_BOOK:
      tradeServersInfo.currentStatus[tsIndex].tsMdcStatus[TS_EDGX_BOOK_INDEX] = DISCONNECT;
      strcat(service, "EDGX MDC");
      break;
    case TS_SERVICE_EDGA_BOOK:
      tradeServersInfo.currentStatus[tsIndex].tsMdcStatus[TS_EDGA_BOOK_INDEX] = DISCONNECT;
      strcat(service, "EDGA MDC");
      break;
    case TS_SERVICE_NDBX_BOOK:
      tradeServersInfo.currentStatus[tsIndex].tsMdcStatus[TS_NDBX_BOOK_INDEX] = DISCONNECT;
      strcat(service, "NASDAQ BX MDC");
      break;
    case TS_SERVICE_BYX_BOOK:
      tradeServersInfo.currentStatus[tsIndex].tsMdcStatus[TS_BYX_BOOK_INDEX] = DISCONNECT;
      strcat(service, "BYX MDC");
      break;
    case TS_SERVICE_PSX_BOOK:
      tradeServersInfo.currentStatus[tsIndex].tsMdcStatus[TS_PSX_BOOK_INDEX] = DISCONNECT;
      strcat(service, "PSX MDC");
      break;
    case TS_SERVICE_AMEX_BOOK:
      tradeServersInfo.currentStatus[tsIndex].tsMdcStatus[TS_AMEX_BOOK_INDEX] = DISCONNECT;
      strcat(service, "AMEX MDC");
      break;
      
    case TS_SERVICE_CQS_FEED:
      tradeServersInfo.currentStatus[tsIndex].statusCQS = DISCONNECT;
      strcat(service, "CQS Feed");
      break;
    case TS_SERVICE_UQDF_FEED:
      tradeServersInfo.currentStatus[tsIndex].statusUQDF = DISCONNECT;
      strcat(service, "UQDF Feed");
      break;
      
    case TS_SERVICE_ARCA_DIRECT:
      tradeServersInfo.currentStatus[tsIndex].tsOecStatus[TS_ARCA_DIRECT_INDEX] = DISCONNECT;
      strcat(service, "ARCA DIRECT");
      break;
    case TS_SERVICE_NASDAQ_OUCH:
      tradeServersInfo.currentStatus[tsIndex].tsOecStatus[TS_NASDAQ_OUCH_INDEX] = DISCONNECT;
      strcat(service, "NASDAQ OUCH");
      break;
    case TS_SERVICE_NYSE_CCG:
      tradeServersInfo.currentStatus[tsIndex].tsOecStatus[TS_NYSE_CCG_INDEX] = DISCONNECT;
      strcat(service, "NYSE CCG");
      break;
    case TS_SERVICE_NASDAQ_RASH:
      tradeServersInfo.currentStatus[tsIndex].tsOecStatus[TS_NASDAQ_RASH_INDEX] = DISCONNECT;
      strcat(service, "NASDAQ RASH");
      break;
    case TS_SERVICE_BATSZ_BOE:
      tradeServersInfo.currentStatus[tsIndex].tsOecStatus[TS_BATSZ_BOE_INDEX] = DISCONNECT;
      strcat(service, "BZX BOE");
      break;
    case TS_SERVICE_EDGX_DIRECT:
      tradeServersInfo.currentStatus[tsIndex].tsOecStatus[TS_EDGX_DIRECT_INDEX] = DISCONNECT;
      strcat(service, "EDGX DIRECT");
      break;
    case TS_SERVICE_EDGA_DIRECT:
      tradeServersInfo.currentStatus[tsIndex].tsOecStatus[TS_EDGA_DIRECT_INDEX] = DISCONNECT;
      strcat(service, "EDGA DIRECT");
      break;
    case TS_SERVICE_NDAQ_OUBX:
      tradeServersInfo.currentStatus[tsIndex].tsOecStatus[TS_NDAQ_OUBX_INDEX] = DISCONNECT;
      strcat(service, "NDAQ OUCH BX");
      break;
    case TS_SERVICE_BYX_BOE:
      tradeServersInfo.currentStatus[tsIndex].tsOecStatus[TS_BYX_BOE_INDEX] = DISCONNECT;
      strcat(service, "BYX BOE");
      break;
    case TS_SERVICE_PSX:
      tradeServersInfo.currentStatus[tsIndex].tsOecStatus[TS_PSX_OUCH_INDEX] = DISCONNECT;
      strcat(service, "PSX");
      break;
    default:
      TraceLog(ERROR_LEVEL, "(TSDisconnectAlert) Invalid Service ID = %d\n", serviceID);
      break;
  }
  
  BuildNSendDisconnectAlertToAllDaedalus(service);
  BuildNSendDisconnectAlertToAllTT(service);
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTSMessageForDisabledTradingAlert
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessTSMessageForDisabledTradingAlert(int tsIndex, void *message)
{
  t_TSMessage *tsMessage = (t_TSMessage *)message;

  short reasonID, serviceID;

  memcpy(&serviceID, &tsMessage->msgContent[10], 2);
  memcpy(&reasonID, &tsMessage->msgContent[12], 2);

  char reason[128] = "\0";

  if (reasonID >= 0)
  {
    char name[80];    
    GetTraderName(reasonID, name);
    
    switch(serviceID)
    {
      case TS_SERVICE_ALL_OECs:
        DisableTradingAllVenues(tsIndex);
        
        sprintf(reason, "%s %s all OECs", name, tradeServersInfo.config[tsIndex].description);
        break;
      case TS_SERVICE_ARCA_DIRECT:
        tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_ARCA_DIRECT_INDEX] = DISABLE;
        sprintf(reason, "%s %s ARCA DIRECT", name, tradeServersInfo.config[tsIndex].description);
        break;
      case TS_SERVICE_NASDAQ_OUCH:
        tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_NASDAQ_OUCH_INDEX] = DISABLE;
        sprintf(reason, "%s %s NASDAQ OUCH", name, tradeServersInfo.config[tsIndex].description);
        break;
      case TS_SERVICE_NYSE_CCG:
        tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_NYSE_CCG_INDEX] = DISABLE;
        sprintf(reason, "%s %s NYSE CCG", name, tradeServersInfo.config[tsIndex].description);
        break;
      case TS_SERVICE_NASDAQ_RASH:
        tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_NASDAQ_RASH_INDEX] = DISABLE;
        sprintf(reason, "%s %s NASDAQ RASH", name, tradeServersInfo.config[tsIndex].description);
        break;
      case TS_SERVICE_BATSZ_BOE:
        tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_BATSZ_BOE_INDEX] = DISABLE;
        sprintf(reason, "%s %s BZX BOE", name, tradeServersInfo.config[tsIndex].description);
        break;
      case TS_SERVICE_EDGX_DIRECT:
        tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_EDGX_DIRECT_INDEX] = DISABLE;
        sprintf(reason, "%s %s EDGX DIRECT", name, tradeServersInfo.config[tsIndex].description);
        break;
      case TS_SERVICE_EDGA_DIRECT:
        tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_EDGA_DIRECT_INDEX] = DISABLE;
        sprintf(reason, "%s %s EDGA DIRECT", name, tradeServersInfo.config[tsIndex].description);
        break;
      case TS_SERVICE_NDAQ_OUBX:
        tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_NDAQ_OUBX_INDEX] = DISABLE;
        sprintf(reason, "%s %s NDAQ OUCH BX", name, tradeServersInfo.config[tsIndex].description);
        break;
      case TS_SERVICE_BYX_BOE:
        tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_BYX_BOE_INDEX] = DISABLE;
        sprintf(reason, "%s %s BYX BOE", name, tradeServersInfo.config[tsIndex].description);
        break;
      case TS_SERVICE_PSX:
        tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_PSX_OUCH_INDEX] = DISABLE;
        sprintf(reason, "%s %s PSX", name, tradeServersInfo.config[tsIndex].description);
        break;
      default:
        TraceLog(ERROR_LEVEL, "(TSDisabledTradingAlert) Invalid Service ID = %d\n", serviceID);
        return ERROR;
    }
  }
  else
  {
    switch(reasonID)
    {
    // no TT --> no need to send
    case TS_TRADING_DISABLED_BY_NO_TT_CONNECTION:
      switch(serviceID)
      {
        case TS_SERVICE_ALL_OECs:
          DisableTradingAllVenues(tsIndex);
          return ERROR;
        case TS_SERVICE_ARCA_DIRECT:
          tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_ARCA_DIRECT_INDEX] = DISABLE;
          return ERROR;
        case TS_SERVICE_NASDAQ_OUCH:
          tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_NASDAQ_OUCH_INDEX] = DISABLE;
          return ERROR;
        case TS_SERVICE_NYSE_CCG:
          tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_NYSE_CCG_INDEX] = DISABLE;
          return ERROR;
        case TS_SERVICE_NASDAQ_RASH:
          tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_NASDAQ_RASH_INDEX] = DISABLE;
          return ERROR;
        case TS_SERVICE_BATSZ_BOE:
          tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_BATSZ_BOE_INDEX] = DISABLE;
          return ERROR;
        case TS_SERVICE_BYX_BOE:
          tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_BYX_BOE_INDEX] = DISABLE;
          return ERROR;
        case TS_SERVICE_EDGX_DIRECT:
          tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_EDGX_DIRECT_INDEX] = DISABLE;
          return ERROR;
        case TS_SERVICE_EDGA_DIRECT:
          tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_EDGA_DIRECT_INDEX] = DISABLE;
          return ERROR;
        case TS_SERVICE_NDAQ_OUBX:
          tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_NDAQ_OUBX_INDEX] = DISABLE;
          return ERROR;
        case TS_SERVICE_PSX:
          tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_PSX_OUCH_INDEX] = DISABLE;
          return ERROR;
        default:
          TraceLog(ERROR_LEVEL, "(TS_TRADING_DISABLED_BY_NO_TT) Invalid Service ID = %d\n", serviceID);
          return ERROR;
      }
      break;
      
    case TS_TRADING_DISABLED_BY_NO_ACTIVE_OR_ACTIVE_ASSIST_TRADER:
      DisableTradingAllVenues(tsIndex);
      
      sprintf(reason, "No Active and Active Assist, disable trading on %s", tradeServersInfo.config[tsIndex].description);
      break;
	  
	case TS_TRADING_DISABLED_BY_EMERGENCY_STOP:
      DisableTradingAllVenues(tsIndex);
      
      sprintf(reason, "Emergency Stop, disable trading on %s", tradeServersInfo.config[tsIndex].description);
      break;
      
    case TS_TRADING_DISABLED_BY_WEDBUSH_REQUEST:
      DisableTradingAllVenues(tsIndex);
      
      sprintf(reason, "WedBush Risk Manager disables trading on %s", tradeServersInfo.config[tsIndex].description);
      break;
      
    case TS_TRADING_DISABLED_BY_MAX_CROSS_LIMIT:
      DisableTradingAllVenues(tsIndex);

      sprintf(reason, "Max crosses per minute is exceeded on %s", tradeServersInfo.config[tsIndex].description);
      break;

    case TS_TRADING_DISABLED_BY_TWT_REQUEST:
      DisableTradingAllVenues(tsIndex);
      
      if (TWTRequestAlert == NO)
      {
        TWTRequestAlert = YES;
        
        sprintf(reason, "TWT %s all OECs", tradeServersInfo.config[tsIndex].description);
        BuildNSendTradingDisabledAlertToAllDaedalus(reason);
        BuildNSendTradingDisabledAlertToAllTT(reason);
      }
      return SUCCESS;
      break;

    case TS_TRADING_DISABLED_BY_BOOK_DISCONNECT:
      switch(serviceID)
      {
      case TS_SERVICE_ARCA_BOOK:
        tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_ARCA_DIRECT_INDEX] = DISABLE;
        
        sprintf(reason, "ARCA MDC disconnected from %s", tradeServersInfo.config[tsIndex].description);
        break;
      case TS_SERVICE_NASDAQ_BOOK:
        tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_NASDAQ_OUCH_INDEX] = DISABLE;
        tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_NASDAQ_RASH_INDEX] = DISABLE;
        
        sprintf(reason, "NASDAQ MDC disconnected from %s", tradeServersInfo.config[tsIndex].description);
        break;
      case TS_SERVICE_NYSE_BOOK:
      case TS_SERVICE_AMEX_BOOK:
        tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_NYSE_CCG_INDEX] = DISABLE;
        
        sprintf(reason, "NYSE MDC disconnected from %s", tradeServersInfo.config[tsIndex].description);
        break;
      case TS_SERVICE_BATSZ_BOOK:
        tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_BATSZ_BOE_INDEX] = DISABLE;
        
        sprintf(reason, "BZX MDC disconnected from %s", tradeServersInfo.config[tsIndex].description);
        break;
      case TS_SERVICE_EDGX_BOOK:
        tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_EDGX_DIRECT_INDEX] = DISABLE;
        
        sprintf(reason, "EDGX MDC disconnected from %s", tradeServersInfo.config[tsIndex].description);
        break;
      case TS_SERVICE_EDGA_BOOK:
        tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_EDGA_DIRECT_INDEX] = DISABLE;
        
        sprintf(reason, "EDGA MDC disconnected from %s", tradeServersInfo.config[tsIndex].description);
        break;
      case TS_SERVICE_NDBX_BOOK:
        tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_NDAQ_OUBX_INDEX] = DISABLE;
        
        sprintf(reason, "NASDAQ BX MDC disconnected from %s", tradeServersInfo.config[tsIndex].description);
        break;
      case TS_SERVICE_BYX_BOOK:
        tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_BYX_BOE_INDEX] = DISABLE;
        
        sprintf(reason, "BYX MDC disconnected from %s", tradeServersInfo.config[tsIndex].description);
        break;
      case TS_SERVICE_PSX_BOOK:
        tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_PSX_OUCH_INDEX] = DISABLE;
        
        sprintf(reason, "PSX MDC disconnected from %s", tradeServersInfo.config[tsIndex].description);
        break;
      default:
        TraceLog(ERROR_LEVEL, "(TS_TRADING_DISABLED_BOOK) Invalid Service ID = %d\n", serviceID);
        return ERROR;
      }
      break;
      
    case TS_TRADING_DISABLED_BY_ORDER_DISCONNECT:
      switch(serviceID)
      {
      case TS_SERVICE_ARCA_DIRECT:
        tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_ARCA_DIRECT_INDEX] = DISABLE;
        
        sprintf(reason, "ARCA DIRECT disconnected from %s", tradeServersInfo.config[tsIndex].description);
        break;
      case TS_SERVICE_NASDAQ_OUCH:
        tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_NASDAQ_OUCH_INDEX] = DISABLE;
        
        sprintf(reason, "NASDAQ OUCH disconnected from %s", tradeServersInfo.config[tsIndex].description);
        break;
      case TS_SERVICE_NYSE_CCG:
        tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_NYSE_CCG_INDEX] = DISABLE;
        
        sprintf(reason, "NYSE CCG disconnected from %s", tradeServersInfo.config[tsIndex].description);
        break;
      case TS_SERVICE_NASDAQ_RASH:
        tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_NASDAQ_RASH_INDEX] = DISABLE;
        
        sprintf(reason, "NASDAQ RASH disconnected from %s", tradeServersInfo.config[tsIndex].description);
        break;
      case TS_SERVICE_BATSZ_BOE:
        tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_BATSZ_BOE_INDEX] = DISABLE;
        
        sprintf(reason, "BZX BOE disconnected from %s", tradeServersInfo.config[tsIndex].description);
        break;
      case TS_SERVICE_EDGX_DIRECT:
        tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_EDGX_DIRECT_INDEX] = DISABLE;
        
        sprintf(reason, "EDGX DIRECT disconnected from %s", tradeServersInfo.config[tsIndex].description);
        break;
      case TS_SERVICE_EDGA_DIRECT:
        tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_EDGA_DIRECT_INDEX] = DISABLE;
        
        sprintf(reason, "EDGA DIRECT disconnected from %s", tradeServersInfo.config[tsIndex].description);
        break;
      case TS_SERVICE_NDAQ_OUBX:
        tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_NDAQ_OUBX_INDEX] = DISABLE;
        
        sprintf(reason, "NASDAQ OUCH BX disconnected from %s", tradeServersInfo.config[tsIndex].description);
        break;
      case TS_SERVICE_BYX_BOE:
        tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_BYX_BOE_INDEX] = DISABLE;
        
        sprintf(reason, "BYX BOE disconnected from %s", tradeServersInfo.config[tsIndex].description);
        break;
      case TS_SERVICE_PSX:
        tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_PSX_OUCH_INDEX] = DISABLE;
        
        sprintf(reason, "PSX disconnected from %s", tradeServersInfo.config[tsIndex].description);
        break;
      default:
        TraceLog(ERROR_LEVEL, "(TS_TRADING_DISABLED_ORDER) Invalid Service ID = %d\n", serviceID);
        return ERROR;
      }
      break;

    case TS_TRADING_DISABLED_BY_EXCEED_MAX_STUCK_LIMIT:
      DisableTradingAllVenues(tsIndex);

      sprintf(reason, "Stuck limit from %s", tradeServersInfo.config[tsIndex].description);
      break;
    case TS_TRADING_DISABLED_BY_EXCEED_MAX_LOSS_LIMIT:
      DisableTradingAllVenues(tsIndex);
      sprintf(reason, "Consecutive loss limit from %s", tradeServersInfo.config[tsIndex].description);
      break;
    case TS_TRADING_DISABLED_BY_EXCEED_MAX_STUCK_LIMIT_BY_AS_REQUEST:
    
      DisableTradingAllVenues(tsIndex);
      
      if (MaxStuckOfASAlert == NO)
      {
        MaxStuckOfASAlert = YES;
        
        sprintf(reason, "Stuck limit from AS");
        BuildNSendTradingDisabledAlertToAllDaedalus(reason);
        BuildNSendTradingDisabledAlertToAllTT(reason);
      }
      return SUCCESS;
      break;
    case TS_TRADING_DISABLED_BY_EXCEED_MAX_LOSS_LIMIT_BY_AS_REQUEST:
      DisableTradingAllVenues(tsIndex);

      if (MaxLossOfASAlert == NO)
      {
        MaxLossOfASAlert = YES;
        
        sprintf(reason, "Consecutive loss limit from AS");
        BuildNSendTradingDisabledAlertToAllDaedalus(reason);
        BuildNSendTradingDisabledAlertToAllTT(reason);
      }
      return SUCCESS;
      break;
    case TS_TRADING_DISABLED_BY_SCHEDULE:
      switch(serviceID)
      {
        case TS_SERVICE_ALL_OECs:
          DisableTradingAllVenues(tsIndex);
          sprintf("auto-scheduled %s all OECs", tradeServersInfo.config[tsIndex].description);
          break;
        case TS_SERVICE_ARCA_DIRECT:
          tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_ARCA_DIRECT_INDEX] = DISABLE;
          sprintf(reason, "auto-scheduled %s ARCA DIRECT", tradeServersInfo.config[tsIndex].description);
          break;
        case TS_SERVICE_NASDAQ_OUCH:
          tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_NASDAQ_OUCH_INDEX] = DISABLE;
          sprintf(reason, "auto-scheduled %s NASDAQ OUCH", tradeServersInfo.config[tsIndex].description);
          break;
        case TS_SERVICE_NYSE_CCG:
          tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_NYSE_CCG_INDEX] = DISABLE;
          sprintf(reason, "auto-scheduled %s NYSE CCG", tradeServersInfo.config[tsIndex].description);
          break;
        case TS_SERVICE_NASDAQ_RASH:
          tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_NASDAQ_RASH_INDEX] = DISABLE;
          sprintf(reason, "auto-scheduled %s NASDAQ RASH", tradeServersInfo.config[tsIndex].description);
          break;
        case TS_SERVICE_BATSZ_BOE:
          tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_BATSZ_BOE_INDEX] = DISABLE;
          sprintf(reason, "auto-scheduled %s BZX BOE", tradeServersInfo.config[tsIndex].description);
          break;
        case TS_SERVICE_EDGX_DIRECT:
          tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_EDGX_DIRECT_INDEX] = DISABLE;
          sprintf(reason, "auto-scheduled %s EDGX", tradeServersInfo.config[tsIndex].description);
          break;
        case TS_SERVICE_EDGA_DIRECT:
          tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_EDGA_DIRECT_INDEX] = DISABLE;
          sprintf(reason, "auto-scheduled %s EDGA", tradeServersInfo.config[tsIndex].description);
          break;
        case TS_SERVICE_NDAQ_OUBX:
          tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_NDAQ_OUBX_INDEX] = DISABLE;
          sprintf(reason, "auto-scheduled %s OUBX", tradeServersInfo.config[tsIndex].description);
          break;
        case TS_SERVICE_BYX_BOE:
          tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_BYX_BOE_INDEX] = DISABLE;
          sprintf(reason, "auto-scheduled %s BYX BOE", tradeServersInfo.config[tsIndex].description);
          break;
        case TS_SERVICE_PSX:
          tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[TS_PSX_OUCH_INDEX] = DISABLE;
          sprintf(reason, "auto-scheduled %s PSX", tradeServersInfo.config[tsIndex].description);
          break;
        default:
          TraceLog(ERROR_LEVEL, "(TSDisabledTradingAlert) Invalid Service ID = %d\n", serviceID);
          return ERROR;
      }
      break;
    default:
      TraceLog(ERROR_LEVEL, "(TSDisabledTradingAlert) Invalid Reason ID = %d\n", reasonID);
      return ERROR;
    }
  }
  
  BuildNSendTradingDisabledAlertToAllDaedalus(reason);
  BuildNSendTradingDisabledAlertToAllTT(reason);
  return SUCCESS;
}

int ProcessTSMessageForMarketStaleAlert(int tsIndex, void *message)
{
  t_TSMessage *tsMessage = (t_TSMessage *)message;
  
  int index = 10;
  int ecnIndex = tsMessage->msgContent[index];
  index += 1;
  
  char symbol[SYMBOL_LEN] = "\0";
  memcpy(symbol, &tsMessage->msgContent[index], SYMBOL_LEN);
  index += SYMBOL_LEN;
  
  long delay;
  memcpy(&delay, &tsMessage->msgContent[index], 8);
  index += 8;
  
  int isStale = tsMessage->msgContent[index];
  
  TraceLog(DEBUG_LEVEL, "Received stale market alert: %d '%.8s' %ld %d\n",
    ecnIndex, symbol, delay, isStale);
    
  char ecnName[10] = "\0";
  switch (ecnIndex)
  {
    case TS_ARCA_BOOK_INDEX:
      strcpy(ecnName, "ARCA");
      break;
    case TS_NASDAQ_BOOK_INDEX:
      strcpy(ecnName, "NASDAQ");
      break;
    case TS_NYSE_BOOK_INDEX:
      strcpy(ecnName, "NYSE");
      break;
    case TS_BATSZ_BOOK_INDEX:
      strcpy(ecnName, "BATSZ");
      break;
    case TS_EDGX_BOOK_INDEX:
      strcpy(ecnName, "EDGX");
      break;
    case TS_EDGA_BOOK_INDEX:
      strcpy(ecnName, "EDGA");
      break;
    case TS_NDBX_BOOK_INDEX:
      strcpy(ecnName, "BX");
      break;
    case TS_BYX_BOOK_INDEX:
      strcpy(ecnName, "BYX");
      break;
    case TS_PSX_BOOK_INDEX:
      strcpy(ecnName, "PSX");
      break;
    case TS_AMEX_BOOK_INDEX:
      strcpy(ecnName, "AMEX");
      break;
  }
    
  char alert[160] = "\0";
  if (isStale)
  {
    // stale
    sprintf(alert, "Slow on %s %s %.8s: %ld nanoseconds",
      tradeServersInfo.config[tsIndex].description, ecnName, symbol, delay);
  }
  else
  {
    // normal
    sprintf(alert, "Fast on %s %s %.8s",
      tradeServersInfo.config[tsIndex].description, ecnName, symbol);
  }
  BuildAndSendAlertToAllDaedalus(alert, COM__RGM__DAEDALUS__PROTO__NOTIFICATION_LEVEL_TYPE__ShowLog);
  SendNotifyMessageToAllTT(alert, 0);
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendConnectToCQSnUQDFToTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendConnectToCQSnUQDFToTS(int tsIndex, int sourceId)
{
  if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
  {
    return SUCCESS;
  }

  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage tsMessage;
  
  // Initialize message
  tsMessage.msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_REQUEST_CONNECT_TO_BBO_TO_TS;
  tsMessage.msgContent[0] = shortConvert.c[0];
  tsMessage.msgContent[1] = shortConvert.c[1];
  tsMessage.msgLen += 2;

  // Update index of body length
  tsMessage.msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  
  // Update index of block length
  tsMessage.msgLen += 4;

  // Block content = ECN Book Id
  tsMessage.msgContent[tsMessage.msgLen] = sourceId;

  // Update index of body length
  tsMessage.msgLen += 1;

  // Block length field
  intConvert.value = 5;
  tsMessage.msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  tsMessage.msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  tsMessage.msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  tsMessage.msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = 5;
  tsMessage.msgContent[2] = intConvert.c[0];
  tsMessage.msgContent[3] = intConvert.c[1];
  tsMessage.msgContent[4] = intConvert.c[2];
  tsMessage.msgContent[5] = intConvert.c[3];

  // Send message to Trade Server
  if (SendTSMessage(tsIndex, &tsMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request TS connect to CQS/UQDF to %s\n", tradeServersInfo.config[tsIndex].description);

    CloseConnection(&tradeServersInfo.connection[tsIndex]);

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendDisconnectToCQSnUQDFToTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendDisconnectToCQSnUQDFToTS(int tsIndex, int sourceId)
{
  if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
  {
    return SUCCESS;
  }

  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage tsMessage;
  
  // Initialize message
  tsMessage.msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_REQUEST_DISCONNECT_TO_BBO_TO_TS;
  tsMessage.msgContent[0] = shortConvert.c[0];
  tsMessage.msgContent[1] = shortConvert.c[1];
  tsMessage.msgLen += 2;

  // Update index of body length
  tsMessage.msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  
  // Update index of block length
  tsMessage.msgLen += 4;

  // Block content = ECN Book Id
  tsMessage.msgContent[tsMessage.msgLen] = sourceId;

  // Update index of body length
  tsMessage.msgLen += 1;

  // Block length field
  intConvert.value = 5;
  tsMessage.msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  tsMessage.msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  tsMessage.msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  tsMessage.msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = 5;
  tsMessage.msgContent[2] = intConvert.c[0];
  tsMessage.msgContent[3] = intConvert.c[1];
  tsMessage.msgContent[4] = intConvert.c[2];
  tsMessage.msgContent[5] = intConvert.c[3];

  // Send message to Trade Server
  if (SendTSMessage(tsIndex, &tsMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request TS disconnect to CQS/UQDF to %s\n", tradeServersInfo.config[tsIndex].description);

    CloseConnection(&tradeServersInfo.connection[tsIndex]);

    return ERROR;
  }

  return SUCCESS;
}


/****************************************************************************
- Function name:  SendSetISO_TradingToTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendSetISO_TradingToTS(int tsIndex, int status)
{
  if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
  {
    return SUCCESS;
  }

  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage tsMessage;
  
  // Initialize message
  tsMessage.msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SET_ISO_TRADING_TO_TS;
  tsMessage.msgContent[0] = shortConvert.c[0];
  tsMessage.msgContent[1] = shortConvert.c[1];
  tsMessage.msgLen += 2;

  // Update index of body length
  tsMessage.msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  
  // Update index of block length
  tsMessage.msgLen += 4;

  // Block content = ECN Book Id
  tsMessage.msgContent[tsMessage.msgLen] = status;

  // Update index of body length
  tsMessage.msgLen += 1;

  // Block length field
  intConvert.value = 5;
  tsMessage.msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  tsMessage.msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  tsMessage.msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  tsMessage.msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = 5;
  tsMessage.msgContent[2] = intConvert.c[0];
  tsMessage.msgContent[3] = intConvert.c[1];
  tsMessage.msgContent[4] = intConvert.c[2];
  tsMessage.msgContent[5] = intConvert.c[3];

  // Send message to Trade Server
  if (SendTSMessage(tsIndex, &tsMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request TS disconnect to CQS/UQDF to %s\n", tradeServersInfo.config[tsIndex].description);

    CloseConnection(&tradeServersInfo.connection[tsIndex]);

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendGetBBOQuotesToTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendGetBBOQuotesToTS(int tsIndex, char *symbol, int ttIndex)
{
  if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
  {
    return SUCCESS;
  }

  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage tsMessage;
  
  // Initialize message
  tsMessage.msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_GET_BBO_QUOTE_LIST_TO_TS;
  tsMessage.msgContent[0] = shortConvert.c[0];
  tsMessage.msgContent[1] = shortConvert.c[1];
  tsMessage.msgLen += 2;

  // Update index of body length
  tsMessage.msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  
  // Update index of block length
  tsMessage.msgLen += 4;

  // Block content = TT_Index + Symbol
  // TT Id (1 byte)
  tsMessage.msgContent[tsMessage.msgLen] = ttIndex;
  tsMessage.msgLen += 1;
  
  strncpy((char *) &tsMessage.msgContent[tsMessage.msgLen], symbol, SYMBOL_LEN);

  // Update index of body length
  tsMessage.msgLen += SYMBOL_LEN;

  // Block length field
  intConvert.value = tsMessage.msgLen - MSG_HEADER_LEN;
  tsMessage.msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  tsMessage.msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  tsMessage.msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  tsMessage.msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = tsMessage.msgLen - MSG_HEADER_LEN;
  tsMessage.msgContent[2] = intConvert.c[0];
  tsMessage.msgContent[3] = intConvert.c[1];
  tsMessage.msgContent[4] = intConvert.c[2];
  tsMessage.msgContent[5] = intConvert.c[3];

  // Send message to Trade Server
  if (SendTSMessage(tsIndex, &tsMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request TS get BBO quotes to %s\n", tradeServersInfo.config[tsIndex].description);

    CloseConnection(&tradeServersInfo.connection[tsIndex]);

    return ERROR;
  }

  return SUCCESS;
}


/****************************************************************************
- Function name:  SendFlushBBOQuotesToTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendFlushBBOQuotesToTS(int tsIndex, int exchangeId)
{
  if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
  {
    return SUCCESS;
  }

  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage tsMessage;
  
  // Initialize message
  tsMessage.msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_FLUSH_BBO_QUOTES_TO_TS;
  tsMessage.msgContent[0] = shortConvert.c[0];
  tsMessage.msgContent[1] = shortConvert.c[1];
  tsMessage.msgLen += 2;

  // Update index of body length
  tsMessage.msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  
  // Update index of block length
  tsMessage.msgLen += 4;

  // Block content = ECN Book Id
  tsMessage.msgContent[tsMessage.msgLen] = exchangeId;

  // Update index of body length
  tsMessage.msgLen += 1;

  // Block length field
  intConvert.value = 5;
  tsMessage.msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  tsMessage.msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  tsMessage.msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  tsMessage.msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = 5;
  tsMessage.msgContent[2] = intConvert.c[0];
  tsMessage.msgContent[3] = intConvert.c[1];
  tsMessage.msgContent[4] = intConvert.c[2];
  tsMessage.msgContent[5] = intConvert.c[3];

  // Send message to Trade Server
  if (SendTSMessage(tsIndex, &tsMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request TS flush BBO quotes to %s\n", tradeServersInfo.config[tsIndex].description);

    CloseConnection(&tradeServersInfo.connection[tsIndex]);

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendSetExchangesConfToTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendSetExchangesConfToTS(int tsIndex, int buffLen, const unsigned char *buffer)
{
  if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
  {
    return SUCCESS;
  }
  
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage tsMessage;
  
  // Initialize message
  tsMessage.msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SET_EXCHANGE_CONF_TO_TS;
  tsMessage.msgContent[0] = shortConvert.c[0];
  tsMessage.msgContent[1] = shortConvert.c[1];
  tsMessage.msgLen += 2;

  // Update index of body length
  tsMessage.msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  // Update index of block length
  tsMessage.msgLen += 4;

  // Exchange conf
  memcpy(&tsMessage.msgContent[tsMessage.msgLen], buffer, buffLen);
  tsMessage.msgLen += buffLen;

  // Block length field
  intConvert.value = tsMessage.msgLen - MSG_HEADER_LEN;
  tsMessage.msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  tsMessage.msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  tsMessage.msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  tsMessage.msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = tsMessage.msgLen - MSG_HEADER_LEN;
  tsMessage.msgContent[2] = intConvert.c[0];
  tsMessage.msgContent[3] = intConvert.c[1];
  tsMessage.msgContent[4] = intConvert.c[2];
  tsMessage.msgContent[5] = intConvert.c[3];

  // Send message to Trader Tool
  if (SendTSMessage(tsIndex, &tsMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request TS set exchange conf to %s\n", tradeServersInfo.config[tsIndex].description);

    CloseConnection(&tradeServersInfo.connection[tsIndex]);
    
    return ERROR;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTSMessageForExchangeConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTSMessageForExchangeConf(int tsIndex, void *message)
{
  TraceLog(DEBUG_LEVEL, "Receive exchange configuration from %s\n", tradeServersInfo.config[tsIndex].description);

  t_TSMessage *tsMessage = (t_TSMessage *)message;  

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Block content = Exchange conf
  int i;
  for (i = 0; i < MAX_EXCHANGE; i++)
  {
    // index
    ExchangeStatusList[tsIndex][i].index = tsMessage->msgContent[currentIndex];
    currentIndex += 1;
    
    // exchangeId
    ExchangeStatusList[tsIndex][i].exchangeId = tsMessage->msgContent[currentIndex];
    currentIndex += 1;
    
    // CQS_Status{visible + Status}
    ExchangeStatusList[tsIndex][i].status[CQS_SOURCE].visible = tsMessage->msgContent[currentIndex];
    ExchangeStatusList[tsIndex][i].status[CQS_SOURCE].enable = tsMessage->msgContent[currentIndex + 1];
    currentIndex += 2;
    
    // UQDF_Status{visible + Status}
    ExchangeStatusList[tsIndex][i].status[UQDF_SOURCE].visible = tsMessage->msgContent[currentIndex];
    ExchangeStatusList[tsIndex][i].status[UQDF_SOURCE].enable = tsMessage->msgContent[currentIndex + 1];
    currentIndex += 2;
  }
  
  // Call function to send global configuration to all Trader Tool
  SendExchangesConfToAllDaedalus(tsIndex);
  SendExchangesConfToAllTT(tsIndex);

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTSMessageForBBOQuotes
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTSMessageForBBOQuotes(int tsIndex, void *message)
{
  TraceLog(DEBUG_LEVEL, "Receive BBO quotes from %s\n", tradeServersInfo.config[tsIndex].description);

  t_TSMessage *tsMessage = (t_TSMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  int ttIndex = tsMessage->msgContent[currentIndex];
  
  SendBBOQuotesToTT(ttIndex, tsIndex, tsMessage);

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTSMessageForBookISO_Trading
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTSMessageForBookISO_Trading(int tsIndex, void *message)
{
  TraceLog(DEBUG_LEVEL, "Receive Book role/Pre-Post settings for ISO algorithm from %s\n", tradeServersInfo.config[tsIndex].description);

  t_TSMessage *tsMessage = (t_TSMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;
  
  // Role book in the ISO algorithm
  memcpy(&tradeServersInfo.bookISO_Trading[tsIndex], &tsMessage->msgContent[currentIndex], TS_MAX_BOOK_CONNECTIONS);
  currentIndex += TS_MAX_BOOK_CONNECTIONS;
  
  //Pre-Post algorithm
  memcpy(&tradeServersInfo.bookPrePostISO_Trading[tsIndex], &tsMessage->msgContent[currentIndex], TS_MAX_BOOK_CONNECTIONS);
  
  // Call function to send role book in the ISO algorithm  to all Trader Tool
  SendBookISO_TradingToAllTT(tsIndex);

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendSetBookISO_TradingToTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendSetBookISO_TradingToTS(int tsIndex)
{
  if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
  {
    return SUCCESS;
  }
  
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage tsMessage;
  
  // Initialize message
  tsMessage.msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SET_BOOK_ISO_TRADING_TO_TS;
  tsMessage.msgContent[0] = shortConvert.c[0];
  tsMessage.msgContent[1] = shortConvert.c[1];
  tsMessage.msgLen += 2;

  // Update index of body length
  tsMessage.msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  // Update index of block length
  tsMessage.msgLen += 4;

  // Role book in the ISO algorithm
  memcpy(&tsMessage.msgContent[tsMessage.msgLen], &tradeServersInfo.bookISO_Trading[tsIndex], TS_MAX_BOOK_CONNECTIONS);
  tsMessage.msgLen += TS_MAX_BOOK_CONNECTIONS;

  // Pre-Post algorithm
  memcpy(&tsMessage.msgContent[tsMessage.msgLen], &tradeServersInfo.bookPrePostISO_Trading[tsIndex], TS_MAX_BOOK_CONNECTIONS);
  tsMessage.msgLen += TS_MAX_BOOK_CONNECTIONS;
  
  // Block length field
  intConvert.value = tsMessage.msgLen - MSG_HEADER_LEN;
  tsMessage.msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  tsMessage.msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  tsMessage.msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  tsMessage.msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = tsMessage.msgLen - MSG_HEADER_LEN;
  tsMessage.msgContent[2] = intConvert.c[0];
  tsMessage.msgContent[3] = intConvert.c[1];
  tsMessage.msgContent[4] = intConvert.c[2];
  tsMessage.msgContent[5] = intConvert.c[3];

  // Send message to Trader Tool
  if (SendTSMessage(tsIndex, &tsMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request TS set book role/pre-post settings for ISO algorithm to %s\n", tradeServersInfo.config[tsIndex].description);

    CloseConnection(&tradeServersInfo.connection[tsIndex]);
    
    return ERROR;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTSMessageForNumberOfSymbolsWithQuotes
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTSMessageForNumberOfSymbolsWithQuotes(int tsIndex, void *message)
{
  TraceLog(DEBUG_LEVEL, "Receive number of symbols with quotes from %s\n", tradeServersInfo.config[tsIndex].description);

  t_TSMessage *tsMessage = (t_TSMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // TT index
  int ttIndex = tsMessage->msgContent[currentIndex];
  currentIndex += 1;
  
  int numberOfSymbolsWithQuotes[TS_MAX_BOOK_CONNECTIONS];
  int ecnIndex;
  
  for (ecnIndex = 0; ecnIndex < TS_MAX_BOOK_CONNECTIONS; ecnIndex++)
  {
    // Number of symbols with quotes
    numberOfSymbolsWithQuotes[ecnIndex] = GetIntNumber(&tsMessage->msgContent[currentIndex]);
    currentIndex += 4;
  }

  // Call function to send the number of symbols with quotes to Trader Tool
  SendNumberOfSymbolsWithQuotesToTT(ttIndex, tsIndex, numberOfSymbolsWithQuotes);

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTSMessageForGroupOfBookDataUnhandledAlert
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessTSMessageForGroupOfBookDataUnhandledAlert(int tsIndex, void *message)
{
  t_TSMessage *tsMessage = (t_TSMessage *)message;

  int serviceID;

  memcpy(&serviceID, &tsMessage->msgContent[10], 4);

  char reason[128];

  switch(serviceID)
  {
    case TS_SERVICE_ARCA_BOOK:
      sprintf(reason, "%s ARCA MDC", tradeServersInfo.config[tsIndex].description);
      break;

    case TS_SERVICE_NASDAQ_BOOK:
      sprintf(reason, "%s NASDAQ MDC", tradeServersInfo.config[tsIndex].description);
      break;

    case TS_SERVICE_NYSE_BOOK:
      sprintf(reason, "%s NYSE MDC", tradeServersInfo.config[tsIndex].description);
      break;

    case TS_SERVICE_BATSZ_BOOK:
      sprintf(reason, "%s BZX MDC", tradeServersInfo.config[tsIndex].description);
      break;

    case TS_SERVICE_EDGX_BOOK:
      sprintf(reason, "%s EGDX MDC", tradeServersInfo.config[tsIndex].description);
      break;
      
    case TS_SERVICE_EDGA_BOOK:
      sprintf(reason, "%s EGDA MDC", tradeServersInfo.config[tsIndex].description);
      break;
      
    case TS_SERVICE_NDBX_BOOK:
      sprintf(reason, "%s BX MDC", tradeServersInfo.config[tsIndex].description);
      break;

    case TS_SERVICE_CQS_FEED:
      sprintf(reason, "%s CQS FEED", tradeServersInfo.config[tsIndex].description);
      break;

    case TS_SERVICE_UQDF_FEED:
      sprintf(reason, "%s UQDF FEED", tradeServersInfo.config[tsIndex].description);
      break;
      
    case TS_SERVICE_BYX_BOOK:
      sprintf(reason, "%s BYX MDC", tradeServersInfo.config[tsIndex].description);
      break;
      
    case TS_SERVICE_PSX_BOOK:
      sprintf(reason, "%s PSX MDC", tradeServersInfo.config[tsIndex].description);
      break;
      
    case TS_SERVICE_AMEX_BOOK:
      sprintf(reason, "%s AMEX MDC", tradeServersInfo.config[tsIndex].description);
      break;

    default:
      TraceLog(ERROR_LEVEL, "(%s) Invalid Service ID = %d\n", __func__, serviceID);
      return ERROR;
  }

  BuildNSendGroupfOfBookDataUnhandledAlertToAllDaedalus(reason);
  BuildNSendGroupfOfBookDataUnhandledAlertToAllTT(reason);
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendTradingAccountToTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendTradingAccountToTS(int tsIndex)
{
  if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage tsMessage;

  BuildTSMessageForTradingAccount(&tsMessage, tsIndex);

  // Send message to Trade Server
  if (SendTSMessage(tsIndex, &tsMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send trading account to %s\n", tradeServersInfo.config[tsIndex].description);

    CloseConnection(&tradeServersInfo.connection[tsIndex]);

    return ERROR;
  }
  else
  {
    TraceLog(DEBUG_LEVEL, "Sent successfully trading account to %s\n", tradeServersInfo.config[tsIndex].description);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  BuildTSMessageForTradingAccount
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildTSMessageForTradingAccount(void *message, const int tsIndex)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *tsMessage = (t_TSMessage *)message;
  
  // Initialize message
  tsMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_TRADING_ACCOUNT_TO_TS;
  tsMessage->msgContent[0] = shortConvert.c[0];
  tsMessage->msgContent[1] = shortConvert.c[1];
  tsMessage->msgLen += 2;

  // Update index of body length
  tsMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  
  // Update index of block length
  tsMessage->msgLen += 4;

  /* Block content = account index (1 byte) + account name (16 bytes) */
  
  // Account index
  tsMessage->msgContent[tsMessage->msgLen] = TradingAccount.accountForTS[tsIndex];
  tsMessage->msgLen += 1;
  
  // Account name
  memcpy(&tsMessage->msgContent[tsMessage->msgLen], TradingAccount.DIRECT[(int)TradingAccount.accountForTS[tsIndex]], 16);
  tsMessage->msgLen += 16;
  
  // Block length field
  intConvert.value = tsMessage->msgLen - MSG_HEADER_LEN;
  tsMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  tsMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  tsMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  tsMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = tsMessage->msgLen - MSG_HEADER_LEN;
  tsMessage->msgContent[2] = intConvert.c[0];
  tsMessage->msgContent[3] = intConvert.c[1];
  tsMessage->msgContent[4] = intConvert.c[2];
  tsMessage->msgContent[5] = intConvert.c[3];

  return tsMessage->msgLen;
}

/****************************************************************************
- Function name:  ProcessTSMessageForSplitSymbolConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTSMessageForSplitSymbolConf(int tsIndex, void *message)
{
  t_TSMessage *tsMessage = (t_TSMessage *)message;
  TraceLog(DEBUG_LEVEL, "Received Split Symbol Conf from %s\n", tradeServersInfo.config[tsIndex].description);
  
  int index = 10;

  memcpy(&tradeServersInfo.splitConf[tsIndex].rangeID, &tsMessage->msgContent[index], 4);
  index += 4;
  
  tradeServersInfo.splitConf[tsIndex].startOfRange = tsMessage->msgContent[index];
  index += 1;
  
  tradeServersInfo.splitConf[tsIndex].endOfRange = tsMessage->msgContent[index];
  index += 1;
  
  TraceLog(DEBUG_LEVEL, "rangeID=%d start='%c' end='%c'\n",
    tradeServersInfo.splitConf[tsIndex].rangeID,
    tradeServersInfo.splitConf[tsIndex].startOfRange,
    tradeServersInfo.splitConf[tsIndex].endOfRange);

  return SUCCESS;
}

int SendRequestPriceUpdateToTS(int tsIndex, const int totalSymbol, char symbolList[][SYMBOL_LEN])
{
  if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage tsMessage;

  if (BuildTSMessageForRequestPriceUpdate(&tsMessage, tsIndex, totalSymbol, symbolList) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Could not build Price Update Request message to %s\n", tradeServersInfo.config[tsIndex].description);
    return ERROR;
  }

  // Send message to Trade Server
  if (SendTSMessage(tsIndex, &tsMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send Price Update Request to %s\n", tradeServersInfo.config[tsIndex].description);

    CloseConnection(&tradeServersInfo.connection[tsIndex]);
    return ERROR;
  }

  return SUCCESS;
}

int BuildTSMessageForRequestPriceUpdate(void *message, int tsIndex, const int totalSymbol, char symbolList[][SYMBOL_LEN])
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *tsMessage = (t_TSMessage *)message;
  
  // Initialize message
  tsMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_PRICE_UPDATE_REQUEST_TO_TS;
  tsMessage->msgContent[0] = shortConvert.c[0];
  tsMessage->msgContent[1] = shortConvert.c[1];
  tsMessage->msgLen += 2;

  // Update index of body length
  tsMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  
  // Update index of block length
  tsMessage->msgLen += 4;

  // Block content = totalSymbol (4 bytes) + Symbol List (8 * totalSymbol)
  
  // Total Symbol
  memcpy(&tsMessage->msgContent[tsMessage->msgLen], &totalSymbol, 4);
  tsMessage->msgLen += 4;

  // Symbol List
  memcpy(&tsMessage->msgContent[tsMessage->msgLen], symbolList, totalSymbol * SYMBOL_LEN);
  tsMessage->msgLen += totalSymbol * SYMBOL_LEN;

  // Block length field
  intConvert.value = tsMessage->msgLen - MSG_HEADER_LEN;
  memcpy(&tsMessage->msgContent[MSG_HEADER_LEN], intConvert.c, 4);
  
  // Body length field
  intConvert.value = tsMessage->msgLen - MSG_HEADER_LEN;
  memcpy(&tsMessage->msgContent[2], intConvert.c, 4);

  return tsMessage->msgLen;
}

int DisableTradingAllVenues(int tsIndex)
{
  int i;
  for(i = 0; i < TS_MAX_ORDER_CONNECTIONS; i++)
  {
    tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[i] = DISABLE;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendHungOrderResetToAllTS
- Description:
- Usage:
****************************************************************************/
int SendHungOrderResetToAllTS()
{
  int tsIndex;

  for (tsIndex = 0; tsIndex < tradeServersInfo.maxConnections; tsIndex++)
  {
    // Check connection between AS and TS
    if ((tradeServersInfo.connection[tsIndex].status == DISCONNECTED) || (tradeServersInfo.connection[tsIndex].socket == -1))
    {
      continue;
    }

    // Call function to send hung order reset to TS
    if (SendHungOrderResetToTS(tsIndex) == ERROR)
    {
      return ERROR;
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendHungOrderResetToTS
- Description:
- Usage:
****************************************************************************/
int SendHungOrderResetToTS(int tsIndex)
{
  if ((tradeServersInfo.connection[tsIndex].status == DISCONNECTED) || (tradeServersInfo.connection[tsIndex].socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage tsMessage;
  int i, clOrdId;
  
  for (i = 0; i < HungOrderList.count; i++)
  {
    if (HungOrderList.buffer[i].orderType != -1)
    {
      clOrdId = HungOrderList.buffer[i].clOrdId;
      
      // TS1: tsIndex: 0 => clOrdId: 1,000,000 .. 1,999,999
      // TS2: tsIndex: 1 => clOrdId: 2,000,000 .. 2,999,999
      if (clOrdId < (tsIndex + 1) * 1000000 || 
        clOrdId >= (tsIndex + 2) * 1000000)
      {
        continue;
      }
      
      // Build hung order reset message to send Trade Server
      BuildTSMessageForHungOrderReset(&tsMessage, clOrdId, HungOrderList.buffer[i].symbol);

      // Send message to Trade Server
      if (SendTSMessage(tsIndex, &tsMessage) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "could not send hung order reset message to %s\n", tradeServersInfo.config[tsIndex].description);

        CloseConnection(&tradeServersInfo.connection[tsIndex]);
        return ERROR;
      }
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  BuildTSMessageForHungOrderReset
- Description:
- Usage:
****************************************************************************/
int BuildTSMessageForHungOrderReset(void *message, int clOrdId, char *symbol)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *tsMessage = (t_TSMessage *)message;

  // Initialize message
  tsMessage->msgLen = 0;

  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_RESET_HUNG_ORDER_TO_TS;
  tsMessage->msgContent[0] = shortConvert.c[0];
  tsMessage->msgContent[1] = shortConvert.c[1];
  tsMessage->msgLen += 2;

  // Update index of body length
  tsMessage->msgLen += 4;

  // Update index of block length
  tsMessage->msgLen += 4;

  // Client Order ID
  memcpy(&tsMessage->msgContent[tsMessage->msgLen], (unsigned char*)&clOrdId, 4);
  tsMessage->msgLen += 4;

  // Symbol
  memcpy(&tsMessage->msgContent[tsMessage->msgLen], (unsigned char*)symbol, SYMBOL_LEN);
  tsMessage->msgLen += SYMBOL_LEN;

  // Block length field
  intConvert.value = tsMessage->msgLen - MSG_HEADER_LEN;
  tsMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  tsMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  tsMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  tsMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];

  // Body length field
  intConvert.value = tsMessage->msgLen - MSG_HEADER_LEN;
  tsMessage->msgContent[2] = intConvert.c[0];
  tsMessage->msgContent[3] = intConvert.c[1];
  tsMessage->msgContent[4] = intConvert.c[2];
  tsMessage->msgContent[5] = intConvert.c[3];

  return tsMessage->msgLen;
}

/***************************************************************************/
