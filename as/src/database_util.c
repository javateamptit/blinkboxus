/*****************************************************************************
**  Project:    Equity arbitrage application
**  Filename:   database_util.c
**  Description:    This file contains code to access Postgre Database
**  Author:   Hoang Le-Quoc
**  First created:  14-Sep-2007
**  Last updated: -----------
*****************************************************************************/

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#define _GNU_SOURCE

#include "configuration.h"
#include "order_mgmt_proc.h"
#include "output_log_mgmt.h"
#include "arca_direct_data.h"
#include "nasdaq_ouch_data.h"
#include "nasdaq_rash_data.h"
#include "bats_boe_data.h"
#include "nyse_ccg_data.h"
#include "internal_proc.h"
#include "raw_data_mgmt.h"
#include "daedalus_proc.h"
#include "trade_servers_proc.h"
#include "trader_tools_proc.h"
#include "database_util.h"
#include "database_config.h"
#include "hbitime.h"
#include "logging.h"

// BUF_LENGTH
#define BUF_LENGTH 4 * 1024

PGconn *  connRawData;
PGconn *  connSaveSettings;
PGconn *  connQueryData;
PGconn *  connTimeSeries;
PGconn *  connPrevMonthDatabase;

pthread_mutex_t updateTradeOutputMutex[MAX_STOCK_SYMBOL];

char askbidStr[MAX_SIDE][6];
char ecnLaunchStr[TS_MAX_BOOK_CONNECTIONS][6];

extern char dateString[MAX_TIMESTAMP];
extern t_TraderCollection traderCollection;
extern t_QueryDataMgmt queryDataMgmt;

//For Log measurement
int CurEntryNewOrderID = 0;
int CurAckID = 0;
int CurFillID = 0;
int CurCancelID = 0;
int CurExitNewOrderID = 0;
/****************************************************************************
** Function implementation
****************************************************************************/

/****************************************************************************
- Function name:  InitializeDatabaseMutex
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
void InitializeDatabaseMutex(void)
{
  int symbolIndex;

  strcpy(askbidStr[ASK_SIDE], "ASK");
  strcpy(askbidStr[BID_SIDE], "BID");

  strcpy(ecnLaunchStr[TS_ARCA_BOOK_INDEX],   "ARCA");
  strcpy(ecnLaunchStr[TS_NASDAQ_BOOK_INDEX], "NDAQ");
  strcpy(ecnLaunchStr[TS_NYSE_BOOK_INDEX],   "NYSE");
  strcpy(ecnLaunchStr[TS_BATSZ_BOOK_INDEX],  "BATZ");
  strcpy(ecnLaunchStr[TS_BYX_BOOK_INDEX],    "BYX");
  strcpy(ecnLaunchStr[TS_EDGX_BOOK_INDEX],   "EDGX");
  strcpy(ecnLaunchStr[TS_EDGA_BOOK_INDEX],   "EDGA");
  strcpy(ecnLaunchStr[TS_NDBX_BOOK_INDEX],   "NDBX");
  strcpy(ecnLaunchStr[TS_BYX_BOOK_INDEX],    "BATY");
  strcpy(ecnLaunchStr[TS_PSX_BOOK_INDEX],    "PSX");
  strcpy(ecnLaunchStr[TS_AMEX_BOOK_INDEX],   "AMEX");
  
  for (symbolIndex = 0; symbolIndex < MAX_STOCK_SYMBOL; symbolIndex++)
  {
    pthread_mutex_init(&updateTradeOutputMutex[symbolIndex], NULL);
  } 
}

/****************************************************************************
- Function name:  CreateConnection
- Input:      -  A pointer to the PGconn Object
          -  A string contains connection infomation
- Output:     N/A
- Return:     true if successfully, false otherwise     
- Description:        
- Usage:      Call this function when you have the correct parameters 
          This function will not check for the validity of the parameters
****************************************************************************/
PGconn * CreateConnection(const char *connString, PGconn *conn)
{
  TraceLog(DEBUG_LEVEL, "Connecting to database: '%s'\n", connString);
  conn = PQconnectdb(connString);

  //  Make a connection to a PostgreSQL server
  if( conn == NULL )
  {
    TraceLog(ERROR_LEVEL, "Fatal - Unable to allocate connection\n" );

    return NULL;
  }
  
  if( PQstatus(conn) != CONNECTION_OK )
  {
    //  Find the reason for failure
    TraceLog(ERROR_LEVEL, "Create DB connection failed, connString = \"%s\", error = %s\n", connString, PQerrorMessage(conn));
    PQfinish(conn);

    return NULL;
  }
  else
  {
    TraceLog(DEBUG_LEVEL, "Database connected.\n");

    return conn;
  }
}

/****************************************************************************
- Function name:  DestroyConnection
- Input:      - A pointer to the PGconn Object
- Output:     N/A
- Return:     N/A     
- Description:    This function will close connection which was opened with
          CreateConnection function     
- Usage:      Call this function when you have the correct parameters 
          This function will not check for the validity of the parameters
****************************************************************************/
void  DestroyConnection( PGconn *conn )
{
  if (conn != NULL)
  {
    PQfinish(conn);
    conn = NULL;
  }
}

/****************************************************************************
- Function name:  CheckAndCreateNewDatabase
- Input:      
- Output:     N/A
- Return:     N/A     
- Description:        
- Usage:      
****************************************************************************/
int CheckAndCreateNewDatabase(char *connStr)
{
  // Create connection to postgres
  PGconn *defaultConn = NULL;
  
  // Get current time
  time_t now = (time_t)hbitime_seconds();
  
  struct tm *currentlTime = (struct tm *)localtime(&now);
  
  if (currentlTime == NULL)
  {
    return ERROR;
  }
  
  if (get_monthly_db_connection_string("eaa", currentlTime->tm_year + 1900, currentlTime->tm_mon + 1, connStr, 512) == NULL_POINTER) {
    TraceLog(ERROR_LEVEL, "Cannot get database configuration\n" );
    return ERROR;
  }
  
  defaultConn = CreateConnection(connStr, defaultConn);
  
  if (defaultConn == NULL) 
  {
    // This database is not exist, we will create new database from last month database
    char newDbName[512];
    
    if (get_monthly_db_name("eaa", currentlTime->tm_year + 1900, currentlTime->tm_mon + 1, newDbName, 512) == NULL_POINTER) {
      TraceLog(ERROR_LEVEL, "Cannot get database configuration\n" );
      return ERROR;
    }
    
    char oldDbName[512];
    int oldMon = currentlTime->tm_mon - 1;
    int oldYear = currentlTime->tm_year;
    if (oldMon < 0) 
    {
      oldMon += 12;
      oldYear -= 1;
    }
    
    if (get_monthly_db_name("eaa", oldYear + 1900, oldMon + 1, oldDbName, 512) == NULL_POINTER) {
      TraceLog(ERROR_LEVEL, "Cannot get database configuration\n" );
      return ERROR;
    }

    TraceLog(DEBUG_LEVEL, "%s database does not exist, we will create new database from %s database\n", newDbName, oldDbName);

    /*
    # Create backup folder 
    mkdir -p ./backup_db/20081001
    cd ./backup_db/20081001
    */
    // Get path of backup directory
    char pathBackup[512];
    sprintf(pathBackup, "./backup_db/%04d%02d%02d", currentlTime->tm_year + 1900, currentlTime->tm_mon + 1, currentlTime->tm_mday);
    
    // Command to create directory
    char command[512];
    sprintf(command, "mkdir -p %s", pathBackup);
    
    // Create directory
    system(command);
    
    // Create file: create_new_database_YYYYDD.sh
    char fileNameToCreateDb[512];
    FILE *fpDb;
    
    sprintf(fileNameToCreateDb, "%s/create_new_database_%04d%02d.sh", pathBackup, currentlTime->tm_year + 1900, currentlTime->tm_mon + 1);

    TraceLog(DEBUG_LEVEL, "fileNameToCreateDb = %s\n", fileNameToCreateDb);

    fpDb = fopen(fileNameToCreateDb, "w+");
    
    if (fpDb == NULL)
    {
      TraceLog(ERROR_LEVEL, "Can't file %s\n", fileNameToCreateDb);

      return ERROR;
    }
    
    char const* dbHostname = get_db_hostname("eaa");
    if (dbHostname == NULL)
    {
      TraceLog(ERROR_LEVEL, "Can't find eaa database hostname\n");
      return ERROR;
    }
    
    /*
    #!/bin/bash
    */
    fprintf(fpDb, "#!/bin/bash\n\n");
    
    /*
    # Create backup folder 
    mkdir -p ./backup_db/20081001
    cd ./backup_db/20081001
    */
    fprintf(fpDb, "# Create backup folder\n");
    fprintf(fpDb, "mkdir -p %s\n", pathBackup);
    fprintf(fpDb, "cd %s\n\n", pathBackup);
    
    /*
      # Backup old database fully
      echo "Backup old database fully"
      /usr/bin/pg_dump -U postgres -h 10.148.71.212 eaa10_200809 -f eaa10_200809.sql
    */
    fprintf(fpDb, "# Backup old database fully\n");
    fprintf(fpDb, "echo \"debug: Backup old database fully\"\n");
    fprintf(fpDb, "%spg_dump -U postgres -h %s %s -f %s.sql\n\n", PGSQL_DIR, dbHostname, oldDbName, oldDbName);

    /*
      # Prepare schema and data for new database
      echo "Prepare schema and data for new database"
      /usr/bin/pg_dump -U postgres -h 10.148.71.212 -F p -s eaa10_200809 -f dbscript_create_eaa10_200809.sql
      /usr/bin/pg_dump -U postgres -h 10.148.71.212 -F p -a -t trade_servers eaa10_200809 -f trade_servers_of_eaa10_200809.sql
      /usr/bin/pg_dump -U postgres -h 10.148.71.212 -F p -a -t as_settings eaa10_200809 -f as_settings_of_eaa10_200809.sql
      /usr/bin/pg_dump -U postgres -h 10.148.71.212 -F p -a -t trading_status eaa10_200809 -f trading_status_of_eaa10_200809.sql
      /usr/bin/pg_dump -U postgres -h 10.148.71.212 -F p -a -t risk_managements eaa10_200809 -f risk_managements_of_eaa10_200809.sql
      .....
    */
    fprintf(fpDb, "# Prepare schema and data for new database\n");
    fprintf(fpDb, "echo \"debug: Prepare schema and data for new database\"\n");
    fprintf(fpDb, "%spg_dump -U postgres -h %s -F p -s %s -f dbscript_create_%s.sql\n", PGSQL_DIR, dbHostname, oldDbName, oldDbName);
    fprintf(fpDb, "%spg_dump -U postgres -h %s -F p -a -t trade_servers %s -f trade_servers_of_%s.sql\n", PGSQL_DIR, dbHostname, oldDbName, oldDbName);
    fprintf(fpDb, "%spg_dump -U postgres -h %s -F p -a -t as_settings %s -f as_settings_of_%s.sql\n", PGSQL_DIR, dbHostname, oldDbName, oldDbName);
    fprintf(fpDb, "%spg_dump -U postgres -h %s -F p -a -t trading_status %s -f trading_status_of_%s.sql\n", PGSQL_DIR, dbHostname, oldDbName, oldDbName);
    fprintf(fpDb, "%spg_dump -U postgres -h %s -F p -a -t risk_managements %s -f risk_managements_of_%s.sql\n\n", PGSQL_DIR, dbHostname, oldDbName, oldDbName);
    fprintf(fpDb, "%spg_dump -U postgres -h %s -F p -a -t user_login_info %s -f user_login_info_of_%s.sql\n\n", PGSQL_DIR, dbHostname, oldDbName, oldDbName);
    fprintf(fpDb, "%spg_dump -U postgres -h %s -F p -a -t version_control %s -f version_control_of_%s.sql\n\n", PGSQL_DIR, dbHostname, oldDbName, oldDbName);
    fprintf(fpDb, "%spg_dump -U postgres -h %s -F p -a -t release_versions %s -f release_versions_of_%s.sql\n\n", PGSQL_DIR, dbHostname, oldDbName, oldDbName);
    
    /*
      # Create new database 
      echo "Create new database: eaa10_200810"
      /usr/bin/createdb -U postgres -h 10.148.71.212 eaa10_200810 -E 'SQL_ASCII'
    */
    fprintf(fpDb, "# Create new database\n");
    fprintf(fpDb, "echo \"debug: Create new database: %s\"\n", newDbName);
    fprintf(fpDb, "%screatedb -U postgres -h %s %s -E 'SQL_ASCII'\n\n", PGSQL_DIR, dbHostname, newDbName);

    /*
      # Update schema and add required data to new database
      echo "Update schema and add required data to new database"
      /usr/bin/psql -U postgres -h 10.148.71.212 eaa10_200810 -f dbscript_create_eaa10_200809.sql >/dev/null
      /usr/bin/psql -U postgres -h 10.148.71.212 eaa10_200810 -f trade_servers_of_eaa10_200809.sql >/dev/null
      /usr/bin/psql -U postgres -h 10.148.71.212 eaa10_200810 -f as_settings_of_eaa10_200809.sql >/dev/null
      /usr/bin/psql -U postgres -h 10.148.71.212 eaa10_200810 -f trading_status_of_eaa10_200809.sql >/dev/null
      /usr/bin/psql -U postgres -h 10.148.71.212 eaa10_200810 -f risk_managements_of_eaa10_200809.sql >/dev/null
      .....
    */
    fprintf(fpDb, "# Update schema and add required data to new database\n");
    fprintf(fpDb, "echo \"debug: Update schema and add required data to new database\"\n");
    fprintf(fpDb, "%spsql -U postgres -h %s %s -f dbscript_create_%s.sql >/dev/null\n", PGSQL_DIR, dbHostname, newDbName, oldDbName);
    fprintf(fpDb, "%spsql -U postgres -h %s %s -f trade_servers_of_%s.sql >/dev/null\n", PGSQL_DIR, dbHostname, newDbName, oldDbName);
    fprintf(fpDb, "%spsql -U postgres -h %s %s -f as_settings_of_%s.sql >/dev/null\n", PGSQL_DIR, dbHostname, newDbName, oldDbName);
    fprintf(fpDb, "%spsql -U postgres -h %s %s -f trading_status_of_%s.sql >/dev/null\n", PGSQL_DIR, dbHostname, newDbName, oldDbName);
    fprintf(fpDb, "%spsql -U postgres -h %s %s -f risk_managements_of_%s.sql >/dev/null\n\n", PGSQL_DIR, dbHostname, newDbName, oldDbName);
    fprintf(fpDb, "%spsql -U postgres -h %s %s -f user_login_info_of_%s.sql >/dev/null\n\n", PGSQL_DIR, dbHostname, newDbName, oldDbName);
    fprintf(fpDb, "%spsql -U postgres -h %s %s -f version_control_of_%s.sql >/dev/null\n\n", PGSQL_DIR, dbHostname, newDbName, oldDbName);
    fprintf(fpDb, "%spsql -U postgres -h %s %s -f release_versions_of_%s.sql >/dev/null\n\n", PGSQL_DIR, dbHostname, newDbName, oldDbName);

    /*
      # Remove data from risk_managements table 
      echo "Remove data from risk_managements table"
      /usr/bin/psql -U postgres -h 10.148.71.212 eaa10_200810 -f remove_old_data_of_risk_managements.sql >/dev/null
    */
    fprintf(fpDb, "# Remove data from risk_managements table\n");
    fprintf(fpDb, "echo \"debug: Remove data from risk_managements table\"\n");
    fprintf(fpDb, "%spsql -U postgres -h %s %s -f remove_old_data_of_risk_managements.sql >/dev/null\n\n", PGSQL_DIR, dbHostname, newDbName);

    fflush(fpDb);
    fclose(fpDb);
    
    // Create file: remove_old_data_of_risk_managements.sql
    char fileNameToRemoveData[512];
    FILE *fpRm;
    
    sprintf(fileNameToRemoveData, "%s/remove_old_data_of_risk_managements.sql", pathBackup);
    
    fpRm = fopen(fileNameToRemoveData, "w+");
    
    if (fpRm == NULL)
    {
      TraceLog(ERROR_LEVEL, "Can't file %s\n", fileNameToRemoveData);

      return ERROR;
    }
    
    /*
    -- Remove old data in risk_managements
    DELETE FROM risk_managements where (date is NULL) OR (date < (SELECT max(date) FROM risk_managements WHERE date < '2008-10-01'));
    */
    
    fprintf(fpRm, "-- Remove old data from risk_managements\n");
    fprintf(fpRm, "DELETE FROM risk_managements where (date is NULL) OR (date < (SELECT max(date) FROM risk_managements WHERE date < '%04d-%02d-01'));\n", currentlTime->tm_year + 1900, currentlTime->tm_mon + 1);
    
    fflush(fpRm);
    fclose(fpRm);
    
    // Execute shell script
    sprintf(command, "chmod 777 %s", fileNameToCreateDb);
    system(command);

    TraceLog(DEBUG_LEVEL, "sh %s >/dev/null\n", fileNameToCreateDb);

    system(fileNameToCreateDb);

    TraceLog(DEBUG_LEVEL, "Create completed new database: %s\n", newDbName);
  }
  else
  {
    TraceLog(DEBUG_LEVEL, "Connection string = \"%s\"\n", connStr);

    DestroyConnection(defaultConn);
  } 
  
  return SUCCESS;
}
/****************************************************************************
- Function name:  ExecuteQuery
- Input:      - A pointer to PGconn Object
          - A query string to be executed
- Output:     N/A
- Return:     A pointer to PGresult Object if successfully, NULL otherwise      
- Description:        
- Usage:      Call this function when you have the correct parameters 
          This function will not check for the validity of the parameters
****************************************************************************/
PGresult * ExecuteQuery( PGconn *conn, const char * queryString )
{
  PGresult * queryResult;

  queryResult = PQexec(conn, queryString);
  
  // Check queryResult
  if (queryResult == NULL)
  {
    TraceLog(FATAL_LEVEL, "DATABASE ERROR: %s. queryString = %s\n", PQerrorMessage(conn), queryString);

    TraceLog(FATAL_LEVEL, "problem in querying database. AS will be killed.\n");

    exit(0);
  }
  
  if(( PQresultStatus( queryResult ) != PGRES_COMMAND_OK ) && 
  ( PQresultStatus( queryResult ) != PGRES_TUPLES_OK ))
  {
    TraceLog(ERROR_LEVEL, "resultStatus = %s\n", PQresStatus( PQresultStatus(queryResult)));
    TraceLog(ERROR_LEVEL, "resultErrorMessage = %s\n", PQresultErrorMessage(queryResult));
    TraceLog(ERROR_LEVEL, "queryString = %s\n", queryString);
  }
  
  return queryResult;
}

/****************************************************************************
- Function name:  GetNumberOfRow
- Input:      - A pointer to the PGresult Object
- Output:     N/A
- Return:     An interger for number of rows      
- Description:    This function returns the number of rows      
- Usage:      Call this function when you have the correct parameters 
        This function will not check for the validity of the parameters
****************************************************************************/
int   GetNumberOfRow( PGresult * queryResult )
{
  return PQntuples( queryResult );
}

/****************************************************************************
- Function name:  GetNumberOfColum
- Input:      - A pointer to the PGresult Object
- Output:     N/A
- Return:     An interger for number of colums    
- Description:    This function returns the number of colums      
- Usage:      Call this function when you have the correct parameters 
          This function will not check for the validity of the parameters
****************************************************************************/
int   GetNumberOfColum( PGresult * queryResult )
{
  return PQnfields( queryResult );
}

/****************************************************************************
- Function name:  IsFieldNull
- Input:      - A pointer to the PGresult Object
          - A row index
          - A colum index
- Output:     N/A
- Return:     true if successfully, false otherwise       
- Description:    This function will check whether a field is NULL or not     
- Usage:      Call this function when you have the correct parameters 
          This function will not check for the validity of the parameters
****************************************************************************/
int IsFieldNull( PGresult * queryResult, int rowIndex, int colIndex )
{
  if( PQgetisnull( queryResult, rowIndex, colIndex ) )
  {
    return 1;
  }
  return 0;
}

/****************************************************************************
- Function name:  GetValueField
- Input:      - A pointer to the PGresult Object
          - A row index
          - A colum index
- Output:     N/A
- Return:     A value formated as a string
- Description:        
- Usage:      Call this function when you have the correct parameters 
          This function will not check for the validity of the parameters
****************************************************************************/
char *  GetValueField( PGresult * queryResult, int rowIndex, int colIndex )
{
  return PQgetvalue( queryResult, rowIndex, colIndex );
}

/****************************************************************************
- Function name:  ClearResult
- Input:      - A pointer to PGresult Object
- Output:     N/A
- Return:     N/A     
- Description:  
- Usage:      Call this function when you have the correct parameters 
          This function will not check for the validity of the parameters
****************************************************************************/
void  ClearResult ( PGresult * queryResult )
{
  PQclear( queryResult );
}

/****************************************************************************
- Function name:  ProcessInsertNewOrder
- Description:
- Usage:
****************************************************************************/
int   ProcessInsertNewOrder(t_New_Order_DB *newOrder)
{
  char  insertString[BUF_LENGTH];

  sprintf( insertString,
    "INSERT INTO new_orders ( time_stamp, ecn, shares, price, symbol, "
      "time_in_force, left_shares, status, msgseq_num, order_id, "
      "side, avg_price, msg_content, entry_exit, ecn_launch, "
      "ask_bid, ts_id, cross_id, date, trade_type, "
      "is_iso, account, bbo_id, entry_exit_ref, routing_inst, "
      "visible_shares) "
    "VALUES ( '%s', '%s', %d, %lf, '%.8s', "
      "'%s', %d, '%s', %d, %d, "
      "'%s', %lf, '%s', '%s', '%s', "
      "'%s', %d, %d, '%s', %d, "
      "'%c', %d, %d, %d, '%s', "
      "%d)",
    newOrder->timestamp, newOrder->ecn, newOrder->shares, newOrder->price, newOrder->symbol, 
    newOrder->timeInForce, newOrder->leftShares, newOrder->status, newOrder->seqNum, newOrder->orderID, 
    newOrder->side, newOrder->avgPrice, newOrder->msgContent, newOrder->entryExit, newOrder->ecnLaunch, 
    newOrder->askBid, newOrder->tsId, newOrder->crossId, newOrder->date, newOrder->tradeType, 
    newOrder->isoFlag, newOrder->tradingAccount, newOrder->bboId, newOrder->entryExitRefNum, newOrder->routingInst, 
    newOrder->visibleShares);

  PGresult * queryResult = ExecuteQuery(connQueryData, insertString );
  if( queryResult == NULL )
  {
    TraceLog(ERROR_LEVEL, "%s: Database query error ocurred!\n", __func__);
    return ERROR;
  }
  ClearResult ( queryResult );
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessInsertOrderAccepted
- Description:  
- Usage:      
****************************************************************************/
int   ProcessInsertOrderACK(t_Order_ACK_DB *orderAccepted)
{
  char  insertString[BUF_LENGTH];   
  
  // Call function from database
  sprintf( insertString, "SELECT insert_order_accepted(%d, '%s', '%s', '%s', '%d', '%s', '%s', %lf, %d, '%s')", 
        orderAccepted->orderID, orderAccepted->date, orderAccepted->timestamp, orderAccepted->processedTimestamp, orderAccepted->seqNum, 
        orderAccepted->exOrderId, orderAccepted->msgContent, orderAccepted->ackPrice, orderAccepted->ackShares, orderAccepted->status);
  
  PGresult * queryResult = ExecuteQuery(connQueryData, insertString );
  
  if( queryResult == NULL )
  {
    TraceLog(ERROR_LEVEL, "%s: Database query error ocurred!\n", __func__);
    return ERROR;
  }
  ClearResult ( queryResult );
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  InsertOrderExecuted
- Description:  
- Usage:      
****************************************************************************/
int   InsertOrderExecuted(t_Order_Executed_DB *oExecuted)
{
  char  insertString[BUF_LENGTH];   
  
  // Call function from database
  sprintf( insertString, "SELECT insert_order_executed(%d, '%s', %d, %lf, '%s', '%s', '%d', '%s', '%s', '%s', '%s', '%s', %d, %lf, %d, %lf)", 
      oExecuted->orderID, oExecuted->date, oExecuted->lastShares, oExecuted->lastPrice, oExecuted->timestamp, 
      oExecuted->processedTimestamp, oExecuted->seqNum, oExecuted->exOrderId, oExecuted->execId, oExecuted->msgContent, 
      oExecuted->liquidity, oExecuted->status, oExecuted->leftShares, oExecuted->avgPrice, oExecuted->entryExitRefNum, 
      oExecuted->fillFee);
  
  PGresult * queryResult = ExecuteQuery(connQueryData, insertString );
  
  if( queryResult == NULL )
  {
    TraceLog(ERROR_LEVEL, "%s: Database query error ocurred!\n", __func__);
    return ERROR;
  }
  ClearResult ( queryResult );
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  InsertOrderCancelled
- Description:  
- Usage:      
****************************************************************************/
int   InsertOrderCancelled(t_Order_Canceled_DB *oCancelled)
{
  char  insertString[BUF_LENGTH];   
  
  // Call function from database
  sprintf( insertString, "SELECT insert_order_canceled(%d, '%s', '%s', '%s', '%d', '%s', '%s', '%s', %d)", 
      oCancelled->orderID, oCancelled->date, oCancelled->timestamp, oCancelled->processedTimestamp, oCancelled->seqNum, 
      oCancelled->exOrderId, oCancelled->msgContent, oCancelled->status, oCancelled->entryExitRefNum);
  
  PGresult * queryResult = ExecuteQuery(connQueryData, insertString );
  
  if( queryResult == NULL )
  {
    TraceLog(ERROR_LEVEL, "%s: Database query error ocurred!\n", __func__);
    return ERROR;
  }
  ClearResult ( queryResult );
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  InsertCancelRequest
- Description:  
- Usage:      
****************************************************************************/
int   InsertCancelRequest(t_Cancel_Request_DB *cancelRequest)
{
  char  insertString[BUF_LENGTH];
  
  // Call function from database
  sprintf( insertString, "SELECT insert_cancel_request(%d, '%s', '%s', '%d', '%s', '%s', '%s')", 
      cancelRequest->orderID, cancelRequest->date, cancelRequest->timestamp, cancelRequest->seqNum, cancelRequest->exOrderId, 
      cancelRequest->msgContent, cancelRequest->status);
  
  PGresult * queryResult = ExecuteQuery(connQueryData, insertString );
  
  if( queryResult == NULL )
  {
    TraceLog(ERROR_LEVEL, "%s: Database query error ocurred!\n", __func__);
    return ERROR;
  }
  ClearResult ( queryResult );
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  InsertCancelACK
- Description:  
- Usage:      
****************************************************************************/
int   InsertCancelACK(t_Cancel_ACK_DB *cancelACK)
{
  char  insertString[BUF_LENGTH];
  
  // Call function from database
  sprintf( insertString, "SELECT insert_cancel_ack(%d, '%s', '%s', '%d', '%s', '%s', '%s')", 
      cancelACK->orderID, cancelACK->date, cancelACK->timestamp, cancelACK->seqNum, 
      cancelACK->exOrderId, cancelACK->msgContent, cancelACK->status);
  
  PGresult * queryResult = ExecuteQuery(connQueryData, insertString );
  
  if( queryResult == NULL )
  {
    TraceLog(ERROR_LEVEL, "%s: Database query error ocurred!\n", __func__);
    return ERROR;
  }
  ClearResult ( queryResult );
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  InsertCancelPending
- Description:  
- Usage:      
****************************************************************************/
int   InsertCancelPending(t_Cancel_Pending_DB *pendingCancel)
{
  char  insertString[BUF_LENGTH];
  
  // Call function from database
  sprintf( insertString, "SELECT insert_cancel_pending(%d, '%s', '%s', '%d', '%s', %d, %lf, '%s', '%s')", 
      pendingCancel->orderID, pendingCancel->date, pendingCancel->timestamp, pendingCancel->seqNum, pendingCancel->exOrderId, 
      pendingCancel->shares, pendingCancel->price, pendingCancel->msgContent, pendingCancel->status);
  
  PGresult * queryResult = ExecuteQuery(connQueryData, insertString );
  
  if( queryResult == NULL )
  {
    TraceLog(ERROR_LEVEL, "%s: Database query error ocurred!\n", __func__);
    return ERROR;
  }

  ClearResult ( queryResult );
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  InsertOrderRejected
- Description:
- Usage:
****************************************************************************/
int   InsertOrderRejected(t_Order_Rejected_DB *rejectedOrder)
{
  char  insertString[BUF_LENGTH];   
  
  // Call function from database
  sprintf( insertString, "SELECT insert_order_rejected(%d, '%s', '%s', '%s', '%d', '%s', '%s', '%s', '%s', %d)", 
      rejectedOrder->orderID, rejectedOrder->date, rejectedOrder->timestamp, rejectedOrder->processedTimestamp, rejectedOrder->seqNum, 
      rejectedOrder->reasonCode, rejectedOrder->reasonText, rejectedOrder->msgContent, rejectedOrder->status, rejectedOrder->entryExitRefNum);
  
  PGresult * queryResult = ExecuteQuery(connQueryData, insertString );
  
  if( queryResult == NULL )
  {
    TraceLog(ERROR_LEVEL, "%s: Database query error ocurred!\n", __func__);
    return ERROR;
  }

  ClearResult ( queryResult );
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  InsertCancelRejected
- Description:
- Usage:
****************************************************************************/
int   InsertCancelRejected(t_Cancel_Rejected_DB *rejectedCancel)
{
  char  insertString[BUF_LENGTH];   
  
  // Call function from database
  sprintf( insertString, "SELECT insert_cancel_rejected(%d, '%s', '%s', '%d', '%s', '%s', '%s', '%s', '%s')", 
      rejectedCancel->orderID, rejectedCancel->date, rejectedCancel->timestamp, rejectedCancel->seqNum, rejectedCancel->exOrderId, 
      rejectedCancel->reasonCode, rejectedCancel->reasonText, rejectedCancel->msgContent, rejectedCancel->status);

  PGresult * queryResult = ExecuteQuery(connQueryData, insertString );
  
  if( queryResult == NULL )
  {
    TraceLog(ERROR_LEVEL, "%s: Database query error ocurred!\n", __func__);
    return ERROR;
  }

  ClearResult ( queryResult );
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  InsertlBustOrCorrect
- Input:      
- Output:     
- Return:     
- Description:  
- Usage:      
****************************************************************************/
int   InsertlBustOrCorrect(t_Broken_Trade_DB *brokenTrade)
{
  char  insertString[BUF_LENGTH];
  
  // Call function from database
  sprintf( insertString, "SELECT insert_bust_or_correct(%d, '%s', %d, %lf, '%s', '%d', '%s', '%s', '%s')", 
      brokenTrade->orderID, brokenTrade->date, brokenTrade->shares, brokenTrade->price, brokenTrade->timestamp, 
      brokenTrade->seqNum, brokenTrade->execId, brokenTrade->msgContent, brokenTrade->status);
  
  PGresult * queryResult = ExecuteQuery(connQueryData, insertString );
  
  if( queryResult == NULL )
  {
    TraceLog(ERROR_LEVEL, "%s: Database query error ocurred!\n", __func__);
    return ERROR;
  }

  ClearResult ( queryResult );
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  UpdateStatusOnly
- Input:      
- Output:     
- Return:     
- Description:  
- Usage:      
****************************************************************************/
int UpdateStatusOnly( int oid, const char * status )
{
  char  updateString[BUF_LENGTH];

  sprintf( updateString, "UPDATE new_orders SET status = '%s' where oid = %d ", status, oid );
  
  PGresult * queryResult = ExecuteQuery(connQueryData, updateString );
  if( queryResult == NULL ){ return ERROR; }
  ClearResult ( queryResult );
  
  return SUCCESS; 
}

/****************************************************************************
- Function name:  UpdateNewOrderStatus
- Input:      
- Output:     
- Return:     
- Description:  
- Usage:      
****************************************************************************/
int UpdateNewOrderStatus(int oid, const char * status, int leftShares, double avgPrice )
{
  char  updateString[BUF_LENGTH];

  sprintf( updateString, "UPDATE new_orders SET status = '%s', left_shares = %d, avg_price = %lf where oid = %d ", status, leftShares, avgPrice, oid );
  
  PGresult * queryResult = ExecuteQuery(connQueryData, updateString );
  if( queryResult == NULL ){ return ERROR; }
  ClearResult ( queryResult );
  
  return SUCCESS; 
}

/****************************************************************************
- Function name:  UpdateNewOrderStatusForNASDAQ
- Input:      
- Output:     
- Return:     
- Description:  
- Usage:      
****************************************************************************/
int UpdateNewOrderStatusForNASDAQ(int oid, const char * status, int shares )
{
  char  updateString[BUF_LENGTH];

  sprintf( updateString, "UPDATE new_orders SET status = '%s', shares = %d where oid = %d ", status, shares, oid );
  
  PGresult * queryResult = ExecuteQuery(connQueryData, updateString );
  if( queryResult == NULL ){ return ERROR; }
  ClearResult ( queryResult );
  
  return SUCCESS; 
}

/****************************************************************************
- Function name:  GetCurrentOID
- Input:      
- Output:     
- Return:     
- Description:  
- Usage:      
****************************************************************************/
int GetCurrentOID( int orderID, const char * date)
{
  int   oid = 0;
  char  selectString[BUF_LENGTH];

  sprintf( selectString, 
  "SELECT oid FROM new_orders WHERE order_id = %d and date = '%s'", orderID, date );
  
  PGresult * queryResult = ExecuteQuery(connQueryData, selectString );
  
  if( queryResult == NULL ){ return ERROR; }
  
  if (GetNumberOfRow(queryResult) == 0)
  {
    TraceLog(ERROR_LEVEL, "Cannot find corresponding oid, following query returns 0 record: %s\n", selectString );

    return ERROR;
  }
  
  oid = atoi( GetValueField( queryResult, 0, 0 ));
  
  ClearResult ( queryResult );
  
  return oid;
}

/****************************************************************************
- Function name:  GetTradeServersConfig
- Input:      
- Output:     
- Return:     
- Description:  
- Usage:      
****************************************************************************/
int   GetTradeServersConfig( t_TradeServerInfo * tradeServersInfo )
{ 
  int i, numberTS = 0;
  PGresult * queryResult = ExecuteQuery(connRawData, "SELECT ts_id, description, ip, port, "
                            "book_arca, book_nasdaq, book_nyse_open, book_bats, book_edgx, "
                            "arca_direct, nasdaq_ouch, nyse_ccg, nasdaq_rash, bats_fix, edgx_direct, "
                            "cqs, uqdf, book_edga, edga_direct, book_nasdaq_bx, nasdaq_ouch_bx, book_byx, byx_boe, book_psx, psx_ouch, book_amex "
                            "FROM trade_servers ORDER BY ts_id" );
  if( queryResult == NULL )
  {
    return ERROR;
  }
  
  numberTS = GetNumberOfRow( queryResult ); 

  if (numberTS > MAX_TRADE_SERVER_CONNECTIONS)
  {
    TraceLog(ERROR_LEVEL, "Load trade server configure from database: numberOfTradeServer (= %d) > MaxTradeServer (= %d)\n", numberTS, MAX_TRADE_SERVER_CONNECTIONS);

    return ERROR;
  }
  else if (numberTS == 0)
  {
    tradeServersInfo->maxConnections = 0;

    TraceLog(ERROR_LEVEL, "Load trade server configure from database: no data\n");

    return ERROR;
  }
  
  for (i = 0; i < numberTS; i++)
  {
    tradeServersInfo->config[i].tsId = atoi( GetValueField( queryResult, i, 0 ));
    strcpy(tradeServersInfo->config[i].description, GetValueField( queryResult, i, 1 ));
    strcpy(tradeServersInfo->config[i].ip, GetValueField( queryResult, i, 2 ));
    tradeServersInfo->config[i].port = atoi( GetValueField( queryResult, i, 3 ));
    
    // TS setting
    tradeServersInfo->tsSetting[i].bookEnable[TS_ARCA_BOOK_INDEX] = atoi( GetValueField( queryResult, i, 4));
    tradeServersInfo->tsSetting[i].bookEnable[TS_NASDAQ_BOOK_INDEX] = atoi( GetValueField( queryResult, i, 5));
    tradeServersInfo->tsSetting[i].bookEnable[TS_NYSE_BOOK_INDEX] = atoi( GetValueField( queryResult, i, 6));
    tradeServersInfo->tsSetting[i].bookEnable[TS_BATSZ_BOOK_INDEX] = atoi( GetValueField( queryResult, i, 7));
    tradeServersInfo->tsSetting[i].bookEnable[TS_EDGX_BOOK_INDEX] = atoi( GetValueField( queryResult, i, 8));   

    tradeServersInfo->tsSetting[i].orderEnable[TS_ARCA_DIRECT_INDEX] = atoi( GetValueField( queryResult, i, 9));
    tradeServersInfo->tsSetting[i].orderEnable[TS_NASDAQ_OUCH_INDEX] = atoi( GetValueField( queryResult, i, 10));
    tradeServersInfo->tsSetting[i].orderEnable[TS_NYSE_CCG_INDEX] = atoi( GetValueField( queryResult, i, 11));
    tradeServersInfo->tsSetting[i].orderEnable[TS_NASDAQ_RASH_INDEX] = atoi( GetValueField( queryResult, i, 12));
    tradeServersInfo->tsSetting[i].orderEnable[TS_BATSZ_BOE_INDEX] = atoi( GetValueField( queryResult, i, 13));
    tradeServersInfo->tsSetting[i].orderEnable[TS_EDGX_DIRECT_INDEX] = atoi( GetValueField( queryResult, i, 14));

    tradeServersInfo->tsSetting[i].bboEnable[CQS_SOURCE] = atoi( GetValueField( queryResult, i, 15));
    tradeServersInfo->tsSetting[i].bboEnable[UQDF_SOURCE] = atoi( GetValueField( queryResult, i, 16));
    
    tradeServersInfo->tsSetting[i].bookEnable[TS_EDGA_BOOK_INDEX] = atoi( GetValueField( queryResult, i, 17));
    tradeServersInfo->tsSetting[i].orderEnable[TS_EDGA_DIRECT_INDEX] = atoi( GetValueField( queryResult, i, 18));
    tradeServersInfo->tsSetting[i].bookEnable[TS_NDBX_BOOK_INDEX] = atoi( GetValueField( queryResult, i, 19));
    tradeServersInfo->tsSetting[i].orderEnable[TS_NDAQ_OUBX_INDEX] = atoi( GetValueField( queryResult, i, 20));
    
    tradeServersInfo->tsSetting[i].bookEnable[TS_BYX_BOOK_INDEX] = atoi( GetValueField( queryResult, i, 21));
    tradeServersInfo->tsSetting[i].orderEnable[TS_BYX_BOE_INDEX] = atoi( GetValueField( queryResult, i, 22));
    
    tradeServersInfo->tsSetting[i].bookEnable[TS_PSX_BOOK_INDEX] = atoi( GetValueField( queryResult, i, 23));
    tradeServersInfo->tsSetting[i].orderEnable[TS_PSX_OUCH_INDEX] = atoi( GetValueField( queryResult, i, 24));
    tradeServersInfo->tsSetting[i].orderEnable[TS_AMEX_BOOK_INDEX] = atoi( GetValueField( queryResult, i, 25));
  }
  
  tradeServersInfo->maxConnections = numberTS;
  
  ClearResult ( queryResult );
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  UpdateTradeServerConfig
- Input:      
- Output:     
- Return:     
- Description:  
- Usage:      
****************************************************************************/
int   UpdateTradeServerConfig( t_TradeServerConf * tradeServersConfig )
{ 
  char  updateString[BUF_LENGTH];

  sprintf( updateString, "UPDATE trade_servers SET description = '%s', ip = '%s', port = %d where ts_id = %d ", tradeServersConfig->description, tradeServersConfig->ip, tradeServersConfig->port, tradeServersConfig->tsId );
  
  pthread_mutex_lock(&connSaveSettings_Mutex);
  PGresult * queryResult = ExecuteQuery(connSaveSettings, updateString );
  pthread_mutex_unlock(&connSaveSettings_Mutex);
  
  if( queryResult == NULL )
  {
    return ERROR;
  }
  
  ClearResult ( queryResult );
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  UpdateTradeServerSetting
- Input:      
- Output:     
- Return:     
- Description:  
- Usage:      
****************************************************************************/
int UpdateTradeServerSetting(t_TSSetting *tsSetting, int tsId)
{ 
  char  updateString[BUF_LENGTH];

  sprintf( updateString, 
      "UPDATE trade_servers SET book_arca = %d, book_nasdaq = %d, book_bats = %d, arca_direct = %d, nasdaq_ouch = %d, bats_fix = %d, nasdaq_rash = %d, cqs = %d, uqdf = %d, book_nyse_open = %d, nyse_ccg = %d, book_edgx = %d, edgx_direct = %d, book_edga = %d, edga_direct = %d, book_nasdaq_bx = %d, nasdaq_ouch_bx = %d, book_byx = %d, byx_boe = %d, book_psx = %d, psx_ouch = %d, book_amex = %d where ts_id = %d ",
      tsSetting->bookEnable[TS_ARCA_BOOK_INDEX],
      tsSetting->bookEnable[TS_NASDAQ_BOOK_INDEX],
      tsSetting->bookEnable[TS_BATSZ_BOOK_INDEX],
      tsSetting->orderEnable[TS_ARCA_DIRECT_INDEX],
      tsSetting->orderEnable[TS_NASDAQ_OUCH_INDEX],
      tsSetting->orderEnable[TS_BATSZ_BOE_INDEX],
      tsSetting->orderEnable[TS_NASDAQ_RASH_INDEX],
      tsSetting->bboEnable[CQS_SOURCE],
      tsSetting->bboEnable[UQDF_SOURCE],
      tsSetting->bookEnable[TS_NYSE_BOOK_INDEX],
      tsSetting->orderEnable[TS_NYSE_CCG_INDEX],
      tsSetting->bookEnable[TS_EDGX_BOOK_INDEX],
      tsSetting->orderEnable[TS_EDGX_DIRECT_INDEX],
      tsSetting->bookEnable[TS_EDGA_BOOK_INDEX],
      tsSetting->orderEnable[TS_EDGA_DIRECT_INDEX],
      tsSetting->bookEnable[TS_NDBX_BOOK_INDEX],
      tsSetting->orderEnable[TS_NDAQ_OUBX_INDEX],
      tsSetting->bookEnable[TS_BYX_BOOK_INDEX],
      tsSetting->orderEnable[TS_BYX_BOE_INDEX],
      tsSetting->bookEnable[TS_PSX_BOOK_INDEX],
      tsSetting->orderEnable[TS_PSX_OUCH_INDEX],
      tsSetting->bookEnable[TS_AMEX_BOOK_INDEX],
      tsId);
  
  pthread_mutex_lock(&connSaveSettings_Mutex);
  PGresult * queryResult = ExecuteQuery(connSaveSettings, updateString );
  pthread_mutex_unlock(&connSaveSettings_Mutex);
  
  if( queryResult == NULL )
  {
    return ERROR;
  }
  
  ClearResult ( queryResult );
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  GetOtherSetting
- Input:      
- Output:     
- Return:     
- Description:  
- Usage:      
****************************************************************************/
int GetOtherSetting(void)
{ 
  int i, numRow = 0, mid;
  
  // Take a look: we use "arca_fix" as an alias of "arca_direct" for AS and PM
  PGresult * queryResult = ExecuteQuery(connRawData, "SELECT mid, description, "
                            "book_arca, book_nasdaq, "
                            "nasdaq_rash, arca_fix, "
                            "cts, utdf, cqs, uqdf "
                            "FROM as_settings" );
  if( queryResult == NULL )
  {
    return ERROR;
  }
  
  numRow = GetNumberOfRow( queryResult ); 
  if (numRow == 0)
  {
    TraceLog(WARN_LEVEL, "table 'as_settings' contains no record\n");
  }
  
  for (i = 0; i < numRow; i++)
  {
    mid = atoi( GetValueField( queryResult, i, 0 ));
    
    if (mid == AS_MODULE)
    {
      strcpy(asSetting.description, GetValueField( queryResult, i, 1));
      
      asSetting.orderEnable[AS_NASDAQ_RASH_INDEX] = atoi( GetValueField( queryResult, i, 4));
      asSetting.orderEnable[AS_ARCA_DIRECT_INDEX] = atoi( GetValueField( queryResult, i, 5));
      
      /*printf("AS Setting: NASDAQ_RASH = %s, ARCA_FIX = %s\n",
          asSetting.orderEnable[AS_NASDAQ_RASH_INDEX] ? "ENABLE":"DISABLE",
          asSetting.orderEnable[AS_ARCA_DIRECT_INDEX] ? "ENABLE":"DISABLE");*/
    }
    else if (mid == PM_MODULE)
    {
      strcpy(pmSetting.description, GetValueField( queryResult, i, 1));
      
      pmSetting.bookEnable[PM_ARCA_BOOK_INDEX] = atoi( GetValueField( queryResult, i, 2));
      pmSetting.bookEnable[PM_NASDAQ_BOOK_INDEX] = atoi( GetValueField( queryResult, i, 3));
      
      pmSetting.orderEnable[PM_NASDAQ_RASH_INDEX] = atoi( GetValueField( queryResult, i, 4));
      pmSetting.orderEnable[PM_ARCA_DIRECT_INDEX] = atoi( GetValueField( queryResult, i, 5));
      
      pmSetting.CTS_Enable = atoi( GetValueField( queryResult, i, 6));
      pmSetting.UTDF_Enable = atoi( GetValueField( queryResult, i, 7));
      pmSetting.CQS_Enable = atoi( GetValueField( queryResult, i, 8));
      pmSetting.UQDF_Enable = atoi(GetValueField(queryResult,i, 9));
      
      /*printf("PM Setting: ARCA_BOOK = %s, NASDAQ_BOOK = %s, NASDAQ_RASH = %s, ARCA_FIX = %s, CTS = %s, UTDF = %s, CQS = %s, UQDF = %s\n",
          pmSetting.bookEnable[PM_ARCA_BOOK_INDEX] ? "ENABLE":"DISABLE",
          pmSetting.bookEnable[PM_NASDAQ_BOOK_INDEX] ? "ENABLE":"DISABLE",
          pmSetting.orderEnable[PM_NASDAQ_RASH_INDEX] ? "ENABLE":"DISABLE",
          pmSetting.orderEnable[PM_ARCA_DIRECT_INDEX] ? "ENABLE":"DISABLE",
          pmSetting.CTS_Enable ? "ENABLE":"DISABLE",
          pmSetting.UTDF_Enable ? "ENABLE":"DISABLE",
          pmSetting.CQS_Enable ? "ENABLE":"DISABLE",
          pmSetting.UQDF_Enable ? "ENABLE":"DISABLE");*/
    }
  }
    
  ClearResult ( queryResult );
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  UpdateASSetting
- Input:      
- Output:     
- Return:     
- Description:  
- Usage:      
****************************************************************************/
int UpdateASSetting(void)
{ 
  char  updateString[BUF_LENGTH];

  // Take a look: we use "arca_fix" as an alias of "arca_direct" for AS and PM
  sprintf( updateString, 
      "UPDATE as_settings SET nasdaq_rash = %d, arca_fix = %d where mid = %d", 
      asSetting.orderEnable[AS_NASDAQ_RASH_INDEX], asSetting.orderEnable[AS_ARCA_DIRECT_INDEX],
      AS_MODULE);
  
  pthread_mutex_lock(&connSaveSettings_Mutex);
  PGresult * queryResult = ExecuteQuery(connSaveSettings, updateString );
  pthread_mutex_unlock(&connSaveSettings_Mutex);
  
  if( queryResult == NULL )
  {
    return ERROR;
  }
  
  ClearResult ( queryResult );
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  UpdatePMSetting
- Input:      
- Output:     
- Return:     
- Description:  
- Usage:      
****************************************************************************/
int UpdatePMSetting(void)
{ 
  char  updateString[BUF_LENGTH];

  // Take a look: we use "arca_fix" as an alias of "arca_direct" for AS and PM
  sprintf( updateString, 
      "UPDATE as_settings SET book_arca = %d, book_nasdaq = %d, nasdaq_rash = %d, arca_fix = %d, cts = %d, utdf = %d, cqs = %d, uqdf = %d where mid = %d",
      pmSetting.bookEnable[PM_ARCA_BOOK_INDEX], pmSetting.bookEnable[PM_NASDAQ_BOOK_INDEX],
      pmSetting.orderEnable[PM_NASDAQ_RASH_INDEX], pmSetting.orderEnable[PM_ARCA_DIRECT_INDEX],
      pmSetting.CTS_Enable, pmSetting.UTDF_Enable, pmSetting.CQS_Enable, pmSetting.UQDF_Enable,
      PM_MODULE);
  
  pthread_mutex_lock(&connSaveSettings_Mutex);
  PGresult * queryResult = ExecuteQuery(connSaveSettings, updateString );
  pthread_mutex_unlock(&connSaveSettings_Mutex);
  
  if( queryResult == NULL )
  {
    return ERROR;
  }
  
  ClearResult ( queryResult );
  
  return SUCCESS;
}


/****************************************************************************
- Function name:  UpdateRASHPrice
- Input:      
- Output:     
- Return:     
- Description:  
- Usage:      
****************************************************************************/
int UpdateRASHPrice( int oid, double filledPrice )
{
  char  updateString[BUF_LENGTH];
  sprintf( updateString, "UPDATE new_orders SET price = %f where oid = %d and price = 0", filledPrice, oid );
  
  PGresult * queryResult = ExecuteQuery(connQueryData, updateString );
  if( queryResult == NULL )
  {
    return ERROR;
  }
  
  ClearResult ( queryResult );
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  GetRawDataFromNewOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int GetRawDataFromNewOrder(const char *date)
{ 
  int i, countRow;
  char queryString[BUF_LENGTH];

  sprintf(queryString, 
    "SELECT n.oid, n.order_id, trim(n.symbol::text), n.side, n.shares, n.left_shares, n.price, n.avg_price, "
    " n.ecn, trim(n.status::text), n.time_in_force, n.ts_id, n.date, n.time_stamp, n.msg_content, "
    " n.ecn_suffix, n.trade_type, n.account, n.cross_id, a.arcaex_order_id "
    "FROM new_orders n "
    "LEFT JOIN order_acks a ON a.oid = n.oid "
    "WHERE date='%s' order by order_id", 
    date);

  PGresult * queryResult = ExecuteQuery(connRawData, queryString );
  if( queryResult == NULL )
  {
    return ERROR;
  }

  countRow = GetNumberOfRow( queryResult );

  TraceLog(DEBUG_LEVEL, "new_orders table: countRow = %d\n", countRow);

  t_OpenOrderInfo openOrder;
  t_OrderDetail orderDetail;

  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;

  char side[4];
  char ecn[6];
  char ecn_suffix[6];
  char tif[4];
  int  numSec;
  char *tmpField;
  int _serverId;
  
  for (i = 0; i < countRow; i ++)
  {
    // Order summary
    openOrder.clOrdId = atoi(GetValueField(queryResult, i, 1));
    strncpy(openOrder.symbol, GetValueField(queryResult, i, 2), SYMBOL_LEN);

    int account = atoi(GetValueField(queryResult, i, 17));
    openOrder.account = account;
  
    strcpy(side, GetValueField(queryResult, i, 3));
    if (strcmp(side, "BC") == 0)
    {
      openOrder.side = BUY_TO_CLOSE_TYPE;
    }
    else if (strcmp(side, "BO") == 0)
    {
      openOrder.side = BUY_TO_OPEN_TYPE;
    }
    else if (strcmp(side, "SS") == 0)
    {
      openOrder.side = SHORT_SELL_TYPE;
    }
    else
    {
      openOrder.side = SELL_TYPE;
    }

    openOrder.shares = atoi(GetValueField(queryResult, i, 4));
    openOrder.leftShares = atoi(GetValueField(queryResult, i, 5));

    openOrder.price = atof(GetValueField(queryResult, i, 6));
    openOrder.avgPrice = atof(GetValueField(queryResult, i, 7));

    strcpy(ecn, GetValueField(queryResult, i, 8));
    strcpy(ecn_suffix, GetValueField(queryResult, i, 15));
    
    if (strcmp(ecn, "ARCA") == 0)
    {
      openOrder.ECNId = TYPE_ARCA_DIRECT;
      if (openOrder.clOrdId < 500000)
      {
        Manual_Order_Account_Mapping[openOrder.clOrdId] = account;
        
        if (strlen(manualOrderInfo[openOrder.clOrdId].symbol) == 0)
        {
          strncpy(manualOrderInfo[openOrder.clOrdId].symbol, openOrder.symbol, SYMBOL_LEN);
        }
      }     
    }
    else if (strcmp(ecn, "OUCH") == 0)
    {
      openOrder.ECNId = TYPE_NASDAQ_OUCH;
    }
    else if (strcmp(ecn, "RASH") == 0)
    {
      openOrder.ECNId = TYPE_NASDAQ_RASH;
      if (openOrder.clOrdId < 500000)
      {
        Manual_Order_Account_Mapping[openOrder.clOrdId] = account;
        
        if (strlen(manualOrderInfo[openOrder.clOrdId].symbol) == 0)
        {
          strncpy(manualOrderInfo[openOrder.clOrdId].symbol, openOrder.symbol, SYMBOL_LEN);
        }
      }
      else if (openOrder.tsId == -1)
      {
        if (strlen(pmOrderInfo[openOrder.clOrdId % MAX_ORDER_PLACEMENT].symbol) == 0)
        {
          strncpy(pmOrderInfo[openOrder.clOrdId % MAX_ORDER_PLACEMENT].symbol, openOrder.symbol, SYMBOL_LEN);
        }
      }
    }
    else if (strcmp(ecn, "NYSE") == 0)
    {
      openOrder.ECNId = TYPE_NYSE_CCG;
    }
    else if (strcmp(ecn, "BATZ") == 0)
    {
      openOrder.ECNId = TYPE_BZX_BOE;
    }
    else if (strcmp(ecn, "EDGX") == 0)
    {
      openOrder.ECNId = TYPE_EDGX_DIRECT;
    }
    else if (strcmp(ecn, "EDGA") == 0)
    {
      openOrder.ECNId = TYPE_EDGA_DIRECT;
    }
    else if (strcmp(ecn, "OUBX") == 0)
    {
      openOrder.ECNId = TYPE_NASDAQ_OUBX;
    }
    else if (strcmp(ecn, "BATY") == 0)
    {
      openOrder.ECNId = TYPE_BYX_BOE;
    }
    else
    {
      openOrder.ECNId = TYPE_PSX;
    }

    //TraceLog(DEBUG_LEVEL, "%s %s --> %d\n", ecn, ecn_suffix, openOrder.ECNId);

    openOrder.status = GetOrderStatusIndex(GetValueField(queryResult, i, 9));

    strcpy(tif, GetValueField(queryResult, i, 10));
    if (strncmp(tif, "IOC", 3) == 0)
    {
      openOrder.tif = IOC_TYPE;
    }
    else
    {
      openOrder.tif = DAY_TYPE;
    }

    //TraceLog(DEBUG_LEVEL, "clOrdId = %d, tif = %d\n" , openOrder.clOrdId, openOrder.tif);

    openOrder.tsId = atof(GetValueField(queryResult, i, 11));
    _serverId = (openOrder.tsId == -1) ? MAX_TRADE_SERVER_CONNECTIONS : openOrder.tsId;
    if (openOrder.tsId != -1)
    {
      openOrder.tsId = tradeServersInfo.config[openOrder.tsId].tsId;
    }

    AddOrderIdToList(&__new, openOrder.clOrdId);

    sprintf(openOrder.timestamp, "%s %s", GetValueField(queryResult, i, 12), GetValueField(queryResult, i, 13));

    openOrder.timestamp[4] = '/';
    openOrder.timestamp[7] = '/';

    numSec = GetNumberOfSecondsFromTimestamp(GetValueField(queryResult, i, 13));

    // tradeType
    tmpField = GetValueField(queryResult, i, 16);
    if (tmpField == NULL)
    {
      openOrder.tradeType = NORMALLY_TRADE;
    }
    else
    {
      openOrder.tradeType = atoi(GetValueField(queryResult, i, 16));
    }   

    // Order detail
    strcpy(orderDetail.timestamp, openOrder.timestamp);
    strcpy(orderDetail.msgContent, GetValueField(queryResult, i, 14));
    orderDetail.length = strlen(orderDetail.msgContent);
    openOrder.crossId = atoi(GetValueField(queryResult, i, 18));

    ExchangeOrderIdMapping[_serverId][openOrder.clOrdId % MAX_ORDER_PLACEMENT] = atol(GetValueField(queryResult, i, 19));
    
    // Call function to add a open order
    ProcessAddASOpenOrder( &openOrder, &orderDetail );

    if (openOrder.shares > openOrder.leftShares)
    {
      UpdateRiskManagementCollection(openOrder.symbol, 
                      openOrder.shares - openOrder.leftShares, 
                      openOrder.avgPrice, 
                      openOrder.side, 
                      numSec, account, 0.00);
    }
    
    CheckAndProcessRawdataInWaitListForOrderID(openOrder.clOrdId);
  }
  
  ClearResult ( queryResult );
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  GetRawDataFromNonExecuteTable
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int GetRawDataFromNonExecuteTable(const char *date, const char *tableName, int editorType)
{ 
  int i, countRow;
  char queryString[BUF_LENGTH];
  int hasEditable = 0;

  t_OrderDetail orderDetail;
  /*------------------------------------------------
  Order detail:
  - int length;
  - int execId;
  - int isBroken;
  - char timestamp[MAX_TIMESTAMP];
  - char msgContent[MAX_ORDER_MSG];
  ------------------------------------------------*/
  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;

  if (editorType == ORDER_EDITOR_ADD)
  {
    hasEditable = 1;
    sprintf(queryString, "SELECT tb.origclord_id, no.date, tb.time_stamp, tb.msg_content, no.ts_id, tb.manual_update, trim(no.symbol), no.ecn, no.ecn_suffix, no.left_shares, no.account, no.side FROM %s tb JOIN new_orders no ON tb.oid = no.oid WHERE date = '%s' AND tb.manual_update = %d order by origclord_id", tableName, date, editorType);
  }
  else if (strncmp(tableName, "cancels", 7) == 0)
  {
    hasEditable = 1;
    sprintf(queryString, "SELECT tb.origclord_id, no.date, tb.time_stamp, tb.msg_content, no.ts_id, tb.manual_update, trim(no.symbol), no.ecn, no.ecn_suffix, no.left_shares, no.account, no.side FROM %s tb JOIN new_orders no ON tb.oid = no.oid WHERE date = '%s' order by origclord_id", tableName, date);
  }
  else
  {
    sprintf(queryString, "SELECT tb.origclord_id, no.date, tb.time_stamp, tb.msg_content, no.ts_id FROM %s tb JOIN new_orders no ON tb.oid = no.oid WHERE date = '%s' order by origclord_id", tableName, date);
  }

  PGresult * queryResult = ExecuteQuery(connRawData, queryString );
  if( queryResult == NULL )
  {
    return ERROR;
  }
  
  countRow = GetNumberOfRow( queryResult );

  TraceLog(DEBUG_LEVEL, "%s table: countRow = %d\n", tableName, countRow);
  if (countRow == 0)
  {
    ClearResult ( queryResult );
    return SUCCESS;
  }

  int clOrdId, tsId, manualUpdate;
  int accountIndex, leftShares, venueType = -1, accountPrefix = 0;
  char timestamp[32], symbol[SYMBOL_LEN + 1] = "\0";
  char ecn[6] = "\0", ecn_suffix[4] = "\0";
  char account[32], buySell;
  t_HungOrderDetails hungOrder;

  for (i = 0; i < countRow; i ++)
  {
    clOrdId = atoi(GetValueField(queryResult, i, 0));
    accountPrefix = 0;

    // Order detail
    sprintf(orderDetail.timestamp, "%s-%s", GetValueField(queryResult, i, 1), GetValueField(queryResult, i, 2));
    orderDetail.timestamp[4] = '/';
    orderDetail.timestamp[7] = '/';
    strcpy(orderDetail.msgContent, GetValueField(queryResult, i, 3));
    orderDetail.length = strlen(orderDetail.msgContent);

    tsId = atoi(GetValueField(queryResult, i, 4));
    if (tsId != MANUAL_ORDER)
    {
      tsId = tradeServersInfo.config[tsId].tsId;
    }
    
    if (strncmp(tableName, "order_acks", 10) == 0)
    {
      AddOrderIdToList(&__ack, clOrdId);
    }
    else if (strncmp(tableName, "order_rejects", 13) == 0)
    {
      AddOrderIdToList(&__reject, clOrdId);
    }
    else if (strncmp(tableName, "cancels", 7) == 0)
    {
      AddOrderIdToList(&__cancel, clOrdId);
    }
    
    if (hasEditable == 1 || editorType == ORDER_EDITOR_ADD)
    {
      ConvertDateTimeToEpoch(&orderDetail.timestamp[0], timestamp);
      
      manualUpdate = atoi(GetValueField(queryResult, i, 5));
      if (manualUpdate == ORDER_EDITOR_ADD)
      {
        strncpy(symbol, GetValueField(queryResult, i, 6), SYMBOL_LEN);
        strcpy(ecn, GetValueField(queryResult, i, 7));
        strcpy(ecn_suffix, GetValueField(queryResult, i, 8));
        leftShares = atoi(GetValueField(queryResult, i, 9));
        accountIndex = atoi(GetValueField(queryResult, i, 10));
        buySell = GetValueField(queryResult, i, 11)[0];
        if (accountIndex < 0 || accountIndex > MAX_ACCOUNT)
        {
          TraceLog(ERROR_LEVEL, "(GetRawDataFromNonExecuteTable): Invalid account index (%d)\n", accountIndex);
          continue;
        }
        
        if (strncmp(ecn, "ARCA", 4) == 0)
        {
          venueType = VENUE_ARCA;
          strcpy(account, TradingAccount.DIRECT[accountIndex]);
        }
        else if (strncmp(ecn, "OUCH", 4) == 0)
        {
          venueType = VENUE_OUCH;
          strcpy(account, TradingAccount.OUCH[accountIndex]);
          accountPrefix = atoi(TradingAccount.OUCHToken[accountIndex]);
        }
        else if (strncmp(ecn, "RASH", 4) == 0)
        {
          venueType = VENUE_RASH;
          strcpy(account, TradingAccount.RASH[accountIndex]);
          accountPrefix = atoi(TradingAccount.RASHToken[accountIndex]);
        }
        else if (strncmp(ecn, "NYSE", 4) == 0)
        {
          venueType = VENUE_CCG;
          strcpy(account, TradingAccount.CCG[accountIndex]);
        }
        else if (strncmp(ecn, "BATZ", 4) == 0)
        {
          venueType = VENUE_BZX;
          strcpy(account, TradingAccount.BATSZBOE[accountIndex]);
        }
        else if (strncmp(ecn, "EDGX", 4) == 0)
        {
          venueType = VENUE_EDGX;
          strcpy(account, TradingAccount.EDGX[accountIndex]);
          accountPrefix = atoi(TradingAccount.EDGXToken[accountIndex]);
        }
        else if (strncmp(ecn, "EDGA", 4) == 0)
        {
          venueType = VENUE_EDGA;
          strcpy(account, TradingAccount.EDGA[accountIndex]);
          accountPrefix = atoi(TradingAccount.EDGAToken[accountIndex]);
        }
        else if (strncmp(ecn, "OUBX", 4) == 0)
        {
          venueType = VENUE_OUBX;
          strcpy(account, TradingAccount.OUBX[accountIndex]);
          accountPrefix = atoi(TradingAccount.OUBXToken[accountIndex]);
        }
        else if (strncmp(ecn, "BATY", 4) == 0)
        {
          venueType = VENUE_BYX;
          strcpy(account, TradingAccount.BYXBOE[accountIndex]);
        }
        else if (strncmp(ecn, "PSX", 3) == 0)
        {
          venueType = VENUE_PSX;
          strcpy(account, TradingAccount.PSX[accountIndex]);
          accountPrefix = atoi(TradingAccount.PSXToken[accountIndex]);
        }
        else
        {
          venueType = -1;
        }
        
        if (editorType == ORDER_EDITOR_ADD)
        {
          // Update Open Orders
          ProcessUpdateASOpenOrderForStatus(tsId, clOrdId, orderStatusIndex.CanceledByECN, &orderDetail);
        }
        else
        {
          // Call function to update a open order
          ProcessUpdateASOpenOrderForNonExecuteOrderDetail(tsId, clOrdId, &orderDetail );
        }
        
        AddOJOrderCancelResponseRecord(timestamp, symbol, accountPrefix * 1000000 + clOrdId, "''", "1", venueType, leftShares, 1, account);
        
        hungOrder.orderType = ORDER_EDITOR_CANCELLED;
        hungOrder.clOrdId = clOrdId;
        hungOrder.venueType = venueType;
        hungOrder.side = buySell;
        hungOrder.account = accountIndex;
        hungOrder.shares = leftShares;
        strncpy(hungOrder.timestamp, orderDetail.timestamp, MAX_TIMESTAMP);
        strncpy(hungOrder.symbol, symbol, SYMBOL_LEN);
        strncpy(hungOrder.msgContent, orderDetail.msgContent, 128);
        
        AddHungOrderToList(&hungOrder);
      }
      else
      {
        // Call function to update a open order
        ProcessUpdateASOpenOrderForNonExecuteOrderDetail(tsId, clOrdId, &orderDetail );
      }
    } 
    else 
    {
      // Call function to update a open order
      ProcessUpdateASOpenOrderForNonExecuteOrderDetail(tsId, clOrdId, &orderDetail );
    }
  }
  
  ClearResult ( queryResult );
  
  if (hasEditable == 1)
  {
    UpdateDatabaseTable(date, tableName);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  GetRawDataFromExecuteTable
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int GetRawDataFromExecuteTable(const char *date, const char *tableName, int editorType)
{ 
  int i, countRow;
  char queryString[BUF_LENGTH];
  int hasEditable = 0;
  int _getFilled = 0;

  t_OrderDetail orderDetail;
  /*------------------------------------------------
  Order detail:
  - int length;
  - int execId;
  - int isBroken;
  - char timestamp[MAX_TIMESTAMP];
  - char msgContent[MAX_ORDER_MSG];
  ------------------------------------------------*/
  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;

  if (editorType == ORDER_EDITOR_ADD)
  {
    hasEditable = 1;
    sprintf(queryString, "SELECT tb.origclord_id, tb.exec_id, tb.shares, tb.price, no.date, tb.time_stamp, tb.msg_content, no.ts_id, trim(tb.liquidity), tb.manual_update, trim(no.symbol), no.side, no.ecn, no.ecn_suffix, no.left_shares, no.account FROM %s tb JOIN new_orders no ON tb.oid = no.oid WHERE date = '%s' AND tb.manual_update = %d order by origclord_id", tableName, date, editorType);
    
    _getFilled = 1;
  }
  else if (strncmp(tableName, "fills", 5) == 0)
  {
    hasEditable = 1;
    sprintf(queryString, "SELECT tb.origclord_id, tb.exec_id, tb.shares, tb.price, no.date, tb.time_stamp, tb.msg_content, no.ts_id, trim(tb.liquidity), tb.manual_update, trim(no.symbol), no.side, no.ecn, no.ecn_suffix, no.left_shares, no.account FROM %s tb JOIN new_orders no ON tb.oid = no.oid WHERE date = '%s' order by origclord_id", tableName, date);
    
    _getFilled = 1;
  }
  else
  {
    sprintf(queryString, "SELECT tb.origclord_id, tb.exec_id, tb.shares, tb.price, no.date, tb.time_stamp, tb.msg_content, no.ts_id FROM %s tb JOIN new_orders no ON tb.oid = no.oid WHERE date = '%s' order by origclord_id", tableName, date);
  }

  PGresult * queryResult = ExecuteQuery(connRawData, queryString );
  if( queryResult == NULL )
  {
    return ERROR;
  }
  
  countRow = GetNumberOfRow( queryResult );

  TraceLog(DEBUG_LEVEL, "%s table: countRow = %d\n", tableName, countRow);
  if (countRow == 0)
  {
    ClearResult ( queryResult );
    return SUCCESS;
  }

  int clOrdId, tsId, manualUpdate;
  long execId;
  int accountIndex, leftShares, venueType, accountPrefix = 0;
  char timestamp[32], symbol[SYMBOL_LEN + 1] = "\0";
  char ecn[6] = "\0", ecn_suffix[4] = "\0", buySell, liquidity[6];
  int fillShares;
  double fillPrice, fillFee;
  char account[32], execIdField[20], execIdVal[32];
  char _execVenue;
  char *_ptr;
  t_HungOrderDetails hungOrder;

  for (i = 0; i < countRow; i ++)
  {
    accountPrefix = 0;
    clOrdId = atoi(GetValueField(queryResult, i, 0));
    execId = atol(GetValueField(queryResult, i, 1));
    
    if (strncmp(tableName, "fills", 5) == 0)
    {
      if (strstr(GetValueField(queryResult, i, 1), "99999-") == NULL)
      {
        AddExecDataToList(&__exec, clOrdId, execId);
      }
    }
    
    fillShares = atoi(GetValueField(queryResult, i, 2));
    fillPrice = atof(GetValueField(queryResult, i, 3));

    // Order detail
    orderDetail.execId = execId;
    sprintf(orderDetail.timestamp, "%s-%s", GetValueField(queryResult, i, 4), GetValueField(queryResult, i, 5));
    orderDetail.timestamp[4] = '/';
    orderDetail.timestamp[7] = '/';
    strcpy(orderDetail.msgContent, GetValueField(queryResult, i, 6));
    orderDetail.length = strlen(orderDetail.msgContent);

    tsId = atoi(GetValueField(queryResult, i, 7));
    if (tsId != MANUAL_ORDER)
    {
      tsId = tradeServersInfo.config[tsId].tsId;
    }
    
    if (hasEditable == 1 || editorType == ORDER_EDITOR_ADD)
    {
      strcpy(liquidity, GetValueField(queryResult, i, 8));
      
      ConvertDateTimeToEpoch(&orderDetail.timestamp[0], timestamp);
      
      manualUpdate = atoi(GetValueField(queryResult, i, 9));
      if (manualUpdate == ORDER_EDITOR_ADD)
      {       
        strncpy(symbol, GetValueField(queryResult, i, 10), SYMBOL_LEN);
        buySell = GetValueField(queryResult, i, 11)[0];
        strcpy(ecn, GetValueField(queryResult, i, 12));
        strcpy(ecn_suffix, GetValueField(queryResult, i, 13));
        leftShares = atoi(GetValueField(queryResult, i, 14));
        accountIndex = atoi(GetValueField(queryResult, i, 15));
        
        if (accountIndex < 0 || accountIndex > MAX_ACCOUNT)
        {
          TraceLog(ERROR_LEVEL, "(GetRawDataFromNonExecuteTable): Invalid account index (%d)\n", accountIndex);
          continue;
        }
        
        if (strncmp(ecn, "ARCA", 4) == 0)
        {
          venueType = VENUE_ARCA;
          strcpy(account, TradingAccount.DIRECT[accountIndex]);
        }
        else if (strncmp(ecn, "OUCH", 4) == 0)
        {
          venueType = VENUE_OUCH;
          strcpy(account, TradingAccount.OUCH[accountIndex]);
          accountPrefix = atoi(TradingAccount.OUCHToken[accountIndex]);
        }
        else if (strncmp(ecn, "RASH", 4) == 0)
        {
          venueType = VENUE_RASH;
          strcpy(account, TradingAccount.RASH[accountIndex]);
          accountPrefix = atoi(TradingAccount.RASHToken[accountIndex]);
        }
        else if (strncmp(ecn, "NYSE", 4) == 0)
        {
          venueType = VENUE_CCG;
          strcpy(account, TradingAccount.CCG[accountIndex]);
        }
        else if (strncmp(ecn, "BATZ", 4) == 0)
        {
          venueType = VENUE_BZX;
          strcpy(account, TradingAccount.BATSZBOE[accountIndex]);
        }
        else if (strncmp(ecn, "EDGX", 4) == 0)
        {
          venueType = VENUE_EDGX;
          strcpy(account, TradingAccount.EDGX[accountIndex]);
          accountPrefix = atoi(TradingAccount.EDGXToken[accountIndex]);
        }
        else if (strncmp(ecn, "EDGA", 4) == 0)
        {
          venueType = VENUE_EDGA;
          strcpy(account, TradingAccount.EDGA[accountIndex]);
          accountPrefix = atoi(TradingAccount.EDGAToken[accountIndex]);
        }
        else if (strncmp(ecn, "OUBX", 4) == 0)
        {
          venueType = VENUE_OUBX;
          strcpy(account, TradingAccount.OUBX[accountIndex]);
          accountPrefix = atoi(TradingAccount.OUBXToken[accountIndex]);
        }
        else if (strncmp(ecn, "BATY", 4) == 0)
        {
          venueType = VENUE_BYX;
          strcpy(account, TradingAccount.BYXBOE[accountIndex]);
        }
        else if (strncmp(ecn, "PSX", 3) == 0)
        {
          venueType = VENUE_PSX;
          strcpy(account, TradingAccount.PSX[accountIndex]);
          accountPrefix = atoi(TradingAccount.PSXToken[accountIndex]);
        }
        else
        {
          venueType = -1;
        }
        
        if (editorType == ORDER_EDITOR_ADD)
        {
          // Update RM & Open Orders
          ProcessUpdateASOpenOrderForExecuted(tsId, clOrdId, orderStatusIndex.Closed, fillShares, fillPrice, &orderDetail, accountIndex, liquidity, &fillFee);          
        }
        else
        {
          // Call function to update a open order
          ProcessUpdateASOpenOrderForNonExecuteOrderDetail(tsId, clOrdId, &orderDetail);
        }
        
        _execVenue = 0;
        
        if (_getFilled == 1 && venueType == VENUE_CCG)
        {
          // fieldIndexList.LastMarket = -69;
          _ptr = strstr(orderDetail.msgContent, "-69=");
          if (_ptr != NULL)
          {
            _ptr += strlen("-69=");
            _execVenue = _ptr[0];
          }
          
          _ptr = NULL; // reset
        }
        
        sprintf(execIdField, "%lu", execId);
        char *exOrderId = "0";
        AddOJOrderFillRecord(timestamp, symbol, exOrderId, venueType, buySell, fillPrice, fillShares,
            clOrdId, liquidity, "''", execIdField, "1", (leftShares == 0)?1:0, account, _execVenue);
              
        hungOrder.orderType = ORDER_EDITOR_FILLED;
        hungOrder.clOrdId = clOrdId;
        hungOrder.venueType = venueType;
        hungOrder.side = buySell;
        hungOrder.account = accountIndex;
        hungOrder.shares = fillShares;
        hungOrder.price = fillPrice;
        hungOrder.liquidity[0] = liquidity[0];
        strncpy(hungOrder.timestamp, orderDetail.timestamp, MAX_TIMESTAMP);
        strncpy(hungOrder.symbol, symbol, SYMBOL_LEN);
        strncpy(hungOrder.execId, execIdVal, 20);
        strncpy(hungOrder.msgContent, orderDetail.msgContent, 128);
        
        AddHungOrderToList(&hungOrder);
      }
      else
      {
        // Call function to update a open order
        ProcessUpdateASOpenOrderForNonExecuteOrderDetail(tsId, accountPrefix * 1000000 + clOrdId, &orderDetail );
      }
    }
    else
    {
      // Call function to update a open order
      ProcessUpdateASOpenOrderForNonExecuteOrderDetail(tsId, clOrdId, &orderDetail );
    }
  }
  
  ClearResult ( queryResult );
  
  if (hasEditable == 1)
  {
    UpdateDatabaseTable(date, tableName);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  UpdateDatabaseTable
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int UpdateDatabaseTable(const char *date, const char *tableName)
{ 
  char queryString[BUF_LENGTH];

  sprintf(queryString, "UPDATE %s SET manual_update = %d WHERE manual_update = %d AND oid IN (SELECT oid FROM new_orders WHERE date = '%s')", tableName, ORDER_EDITOR_PROCESS, ORDER_EDITOR_ADD, date);

  //printf("UpdateDatabaseTable: queryString = %s\n", queryString);
  
  PGresult * queryResult = ExecuteQuery(connRawData, queryString);
  if( queryResult == NULL )
  {
    return ERROR;
  }
  
  ClearResult ( queryResult );
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  GetOpenPositions
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int GetOpenPositions(const char *date, int mustUpdate)
{ 
  int i, countRow, account;
  char queryString[BUF_LENGTH];
  
  sprintf(queryString, "SELECT trim(symbol::text), net_quantity, average_price, matched_profit_loss, total_quantity, date, account, total_fee, manual_update, changed_shares, last_update FROM risk_managements WHERE date='%s';", date);

  PGresult * queryResult = ExecuteQuery(connRawData, queryString);
  if (queryResult == NULL)
  {
    return ERROR;
  }
  
  countRow = GetNumberOfRow(queryResult);

  char symbol[SYMBOL_LEN];
  int stockSymbolIndex, shares, totalShares, isUpdate;
  int isManual, changedShares;
  double price, matchedPL, totalFee, oldMatchedPL;
  t_RiskManagementInfo *rmInfo; 
  t_OpenPosition position;
  
  for (i = 0; i < countRow; i ++)
  {
    strncpy(symbol, GetValueField(queryResult, i, 0), SYMBOL_LEN);
    shares = atoi(GetValueField(queryResult, i, 1));
    price = atof(GetValueField(queryResult, i, 2));
    matchedPL = atof(GetValueField(queryResult, i, 3));
    totalShares = atoi(GetValueField(queryResult, i, 4));
    account = atoi(GetValueField(queryResult, i, 6));
    totalFee = atof(GetValueField(queryResult, i, 7));
    isManual = atoi(GetValueField(queryResult, i, 8));
    changedShares = atoi(GetValueField(queryResult, i, 9));

    // Get stock symbol index
    stockSymbolIndex = GetStockSymbolIndex(symbol); 
    
    if (stockSymbolIndex == -1)
    {
      return ERROR;
    }

    rmInfo = &RMList.collection[account][stockSymbolIndex];

    isUpdate = mustUpdate;

    if (rmInfo->leftShares != shares)
    {
      rmInfo->leftShares = shares;
      isUpdate = 1;
    }

    if (!feq(rmInfo->avgPrice, price))
    {
      rmInfo->avgPrice = price;
      RMList.symbolList[stockSymbolIndex].lastTradedPrice = price;
      isUpdate = 1;
    }

    if (isUpdate == 1)
    {
      oldMatchedPL = rmInfo->matchedPL;
      rmInfo->matchedPL = matchedPL;
      rmInfo->totalShares = totalShares;
      rmInfo->totalFee = totalFee;
      rmInfo->totalMarket = fabs(rmInfo->leftShares * rmInfo->avgPrice);

      //Update this posotion into verify position list
      UpdateVerifyPositionInfo();
      
      if (mustUpdate == 0 && isManual == 1 && changedShares != 0)
      {
        // Add position into OJ file
        strncpy(position.symbol, symbol, SYMBOL_LEN);
        position.shares = abs(changedShares);
        position.price = price;
        strcpy(position.date, date);
        strcpy(position.timeStamp, GetValueField(queryResult, i, 10));
        position.tradingAccount = account;
        position.isManual = isManual;
        if (changedShares > 0)
        {
          position.side = 'B';
        }
        else
        {
          position.side = 'S';
        }
        
        AddOJOpenPositionRecord(&position);
      }
      
      if (isManual == 1 && fgt(oldMatchedPL, rmInfo->matchedPL))
      {
        ProfitLossPerSymbol[stockSymbolIndex].asLoss += (oldMatchedPL - rmInfo->matchedPL);
        
        // DEVLOGF("Symbol %s: asLoss = %lf", symbol, ProfitLossPerSymbol[stockSymbolIndex].asLoss);
        
        // Check to add symbol to Volatile or Disabled Symbol List
        CheckToAddSymbolToVolatileOrDisabledList(symbol, stockSymbolIndex, -1);
      }
    }
  }

  ClearResult ( queryResult );
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  GetBrokenTradeData
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int GetBrokenTradeData(const char *date)
{
  int i, countRow;
  char queryString[BUF_LENGTH];

  // Reset broken trade list
  ResetBrokenTrade();

  sprintf(queryString, 
      "SELECT b.oid, b.exec_id, b.new_shares, b.new_price, b.broken_shares, n.order_id, n.ts_id, trim(n.symbol::text) AS symbol, n.account, b.fill_id "
      "FROM brokens b "
      "LEFT JOIN new_orders n ON b.oid = n.oid "
      "WHERE b.trade_date = '%s' ORDER BY symbol, b.oid", date);

  PGresult * queryResult = ExecuteQuery(connRawData, queryString );
  if( queryResult == NULL )
  {
    return ERROR;
  }
  
  countRow = GetNumberOfRow( queryResult );

  //TraceLog(DEBUG_LEVEL, "Broken trade: countRow = %d\n", countRow);

  t_BrokenTradeInfo brokenTrade;
  int tsId;

  for (i = 0; i < countRow; i ++)
  {
    brokenTrade.isUpdate = BROKEN_TRADE_ADD;
    brokenTrade.oid = atoi(GetValueField(queryResult, i, 0));
    brokenTrade.execId = atoi(GetValueField(queryResult, i, 1));
    brokenTrade.newShares = atoi(GetValueField(queryResult, i, 2));
    brokenTrade.newPrice = atof(GetValueField(queryResult, i, 3));
    brokenTrade.brokenShares = atoi(GetValueField(queryResult, i, 4));
    brokenTrade.clOrdId = atoi(GetValueField(queryResult, i, 5));
    
    tsId = atoi(GetValueField(queryResult, i, 6));
    if (tsId != MANUAL_ORDER)
    {
      tsId = tradeServersInfo.config[tsId].tsId;
    }
    brokenTrade.tsId = tsId;

    strncpy(brokenTrade.symbol, GetValueField(queryResult, i, 7), SYMBOL_LEN);
    RemoveCharacters(brokenTrade.symbol, SYMBOL_LEN);
    
    brokenTrade.account = atoi(GetValueField(queryResult, i, 8));
    brokenTrade.fillId = atoi(GetValueField(queryResult, i, 9));

    UpdateBrokenTrade(brokenTrade);
  }

  ClearResult ( queryResult );  

  // Process broken trade
  ProcessBrokenTrade();
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  GetFilledOrderForSymbolList
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int GetFilledOrderForSymbolList(t_BrokenTradeCollection *newBrokenList, const char *dateString)
{
  int i, ilen = 0, countRow, account = -1;
  char queryString[MAX_ACCOUNT * BUF_LENGTH], symbolBuffer[MAX_ACCOUNT][BUF_LENGTH];
  char symbol[SYMBOL_LEN_PLUS_ONE] = "\0";
  
  int numSymbolPerAccount[MAX_ACCOUNT];
  for (account = 0; account < MAX_ACCOUNT; account++)
  {
    memset(symbolBuffer[account], 0, BUF_LENGTH);
    numSymbolPerAccount[account] = 0;
    ilen = 0;
    
    for (i = 0; i < newBrokenList->countBrokenTrade; i++)
    {
      if (newBrokenList->collection[i].account == account)
      {
        strncpy(symbol, newBrokenList->collection[i].symbol, SYMBOL_LEN);
        numSymbolPerAccount[account]++;
        
        if (ilen > 0)
        {
          ilen += sprintf(&symbolBuffer[account][ilen], ", '%s'", symbol);
        }
        else
        {
          ilen += sprintf(symbolBuffer[account], "'%s'", symbol);  
        }
      }
    }
  }
  
  ilen = 0;
  for (account = 0; account < MAX_ACCOUNT; account++)
  {
    if (numSymbolPerAccount[account] > 0)
    {  
      if (ilen > 0)
      {
        strcpy(&queryString[ilen], "UNION ALL ");
        ilen += 10;
      }
        
      ilen += sprintf(&queryString[ilen], 
              "(SELECT f.origclord_id, f.exec_id, trim(n.symbol), trim(n.side), f.shares, f.price, b.new_price, f.time_stamp, n.ts_id, n.account, n.ecn, n.ecn_suffix, trim(f.liquidity) AS liquidity "
              "FROM fills f " 
              "JOIN new_orders n ON f.oid = n.oid "
              "LEFT JOIN brokens b ON f.oid = b.oid AND f.exec_id = b.exec_id "
              "WHERE (b.oid IS NULL OR b.new_shares > 0) AND date = '%s' AND trim(symbol) in (%s) AND account = %d "
              "ORDER BY symbol, f.time_stamp) ",
              dateString, symbolBuffer[account], account);
    }
  }
  
  if (ilen == 0)
  {
    return SUCCESS;
  }
  
  TraceLog(DEBUG_LEVEL, "%s: queryString = %s\n", __func__, queryString);

  PGresult * queryResult = ExecuteQuery(connRawData, queryString );
  if( queryResult == NULL )
  {
    return ERROR;
  }
  
  countRow = GetNumberOfRow( queryResult );

  if (countRow == 0)
  {
    ClearResult ( queryResult );
    return SUCCESS;
  }

  char side[4], ecn[SYMBOL_LEN], ecn_suffix[SYMBOL_LEN];
  int shares, sideType, numSec;
  double price, newPrice;
  double fillFee;
  int ecnId;
  char liquidity[6];

  for (i = 0; i < countRow; i ++)
  {
    strncpy(symbol, GetValueField(queryResult, i, 2), SYMBOL_LEN);

    strcpy(side, GetValueField(queryResult, i, 3));
    if (strcmp(side, "BC") == 0)
    {
      sideType = BUY_TO_CLOSE_TYPE;
    }
    else if (strcmp(side, "BO") == 0)
    {
      sideType = BUY_TO_OPEN_TYPE;
    }
    else if (strcmp(side, "SS") == 0)
    {
      sideType = SHORT_SELL_TYPE;
    }
    else
    {
      sideType = SELL_TYPE;
    }

    shares = atoi(GetValueField(queryResult, i, 4));
    price = atof(GetValueField(queryResult, i, 5));
    newPrice = atof(GetValueField(queryResult, i, 6));
    account = atof(GetValueField(queryResult, i, 9));
    
    if (newPrice > 0.0001)
    {
      price = newPrice;
    }
    
    strcpy(ecn, GetValueField(queryResult, i, 10));
    strcpy(ecn_suffix, GetValueField(queryResult, i, 11));
    strcpy(liquidity, GetValueField(queryResult, i, 12));
    
    if (strcmp(ecn, "ARCA") == 0)
    {
      ecnId = TYPE_ARCA_DIRECT;
    }
    else if (strcmp(ecn, "OUCH") == 0)
    {
      ecnId = TYPE_NASDAQ_OUCH;
    }
    else if (strcmp(ecn, "RASH") == 0)
    {
      ecnId = TYPE_NASDAQ_RASH;
    }
    else if (strcmp(ecn, "NYSE") == 0)
    {
      ecnId = TYPE_NYSE_CCG;
    }
    else if (strcmp(ecn, "BATZ") == 0)
    {
      ecnId = TYPE_BZX_BOE;
    }
    else if (strcmp(ecn, "EDGX") == 0)
    {
      ecnId = TYPE_EDGX_DIRECT;
    }
    else if (strcmp(ecn, "EDGA") == 0)
    {
      ecnId = TYPE_EDGA_DIRECT;
    }
    else if (strcmp(ecn, "OUBX") == 0)
    {
      ecnId = TYPE_NASDAQ_OUBX;
    }
    else if (strcmp(ecn, "BATY") == 0)
    {
      ecnId = TYPE_BYX_BOE;
    }
    else
    {
      ecnId = TYPE_PSX;
    }

    numSec = GetNumberOfSecondsFromTimestamp(GetValueField(queryResult, i, 7));
    fillFee = CalculateFillFee(shares, price, GetSide(sideType), liquidity, ecnId);
    
    // Update risk management
    UpdateRiskManagementCollection(symbol, shares, price, sideType, numSec, account, fillFee);
  }

  ClearResult ( queryResult );

  return SUCCESS;
}

/****************************************************************************
- Function name:  GetFillInfoOfBroken
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int GetFillInfoOfBroken(int fillIdList[MAX_BROKEN_TRADE], int count, const char *dateString)
{
  int i, ilen = 0, countRow;
  char queryString[BUF_LENGTH], fillIdBuffer[BUF_LENGTH];

  for (i = 0; i < count; i++)
  {
    if (i > 0)
    {
      sprintf(&fillIdBuffer[ilen], ", %d", fillIdList[i]);
      ilen = strlen(fillIdBuffer);
    }
    else
    {
      sprintf(fillIdBuffer, "%d", fillIdList[i]);
      ilen = strlen(fillIdBuffer);
    }
  }

  sprintf(queryString, "SELECT f.arcaex_order_id, f.exec_id, f.shares, f.price, trim(f.liquidity), "
                "n.order_id, n.ts_id, trim(n.symbol), trim(n.side), "
                "n.ecn, n.ecn_suffix, n.account, n.left_shares, "
                "-1 * b.broken_shares as broken_shares, b.new_price, b.time_stamp "
              "FROM fills f "
              "LEFT JOIN new_orders n ON f.oid = n.oid "
              "LEFT JOIN brokens b ON f.oid = b.oid AND f.exec_id = b.exec_id "
              "WHERE date = '%s' AND b.new_shares = 0 AND f.id in (%s) "
              "ORDER BY n.oid", dateString, fillIdBuffer);

  PGresult * queryResult = ExecuteQuery(connRawData, queryString );
  if( queryResult == NULL )
  {
    return ERROR;
  }
  
  countRow = GetNumberOfRow( queryResult );

  if (countRow == 0)
  {
    ClearResult ( queryResult );

    return SUCCESS;
  }
  
  char *str, strDateTime[32], timeStamp[24];
  char ecnOrderId[24], execId[24], strAccount[32];
  int accountIndex, venueID, orderId, shares, leftShares;  
  char buysell = '0', liquidity[6], symbol[SYMBOL_LEN];
  double price;
  long execIdValue;
  int j, isExisted;

  for (i = 0; i < countRow; i ++)
  {    
    // Order Id
    orderId = atoi(GetValueField(queryResult, i, 5));
    
    // Execution Id
    str = GetValueField(queryResult, i, 1);
    strcpy(execId, str);
    execIdValue = atol(str);
    
    // Check existing of this broken in OJ
    isExisted = 0;
    for (j = 0; j < OJBrokenMsgList.countBrokenMsg; j++)
    {
      if (OJBrokenMsgList.collection[j].clOrdId == orderId &&
        OJBrokenMsgList.collection[j].execId == execIdValue)
      {
        isExisted = 1;
        break;
      }
    }
    
    // Ignore this broken if it existed in OJ
    if (isExisted)
    {
      TraceLog(DEBUG_LEVEL, "Ignore broken: %d, %ld\n", orderId, execIdValue);
      continue;
    }
    
    // Time stamp
    str = GetValueField(queryResult, i, 15);
    sprintf(strDateTime, "%s-%s", dateString, str);  //e.g 2012-05-13:08:04:10.012345 (%Y-%m-%d:%H:%M:%S.)
    GetEpochTimeFromDateString(strDateTime, timeStamp);
    
    // Symbol
    str = GetValueField(queryResult, i, 7);
    strncpy(symbol, str, SYMBOL_LEN);
    
    // Account
    accountIndex = atoi(GetValueField(queryResult, i, 11));
    if (accountIndex < 0 || accountIndex > MAX_ACCOUNT)
    {
      TraceLog(ERROR_LEVEL, "%s: Invalid account index (%d)\n", __func__, accountIndex);
      continue;
    }

    // Venue ID
    str = GetValueField(queryResult, i, 9);
    if (strncmp(str, "OUCH", 4) == 0)
    {
      venueID = VENUE_OUCH;
      strcpy(strAccount, TradingAccount.OUCH[accountIndex]);
    }
    else if (strncmp(str, "RASH", 4) == 0)
    {
      venueID = VENUE_RASH;
      strcpy(strAccount, TradingAccount.RASH[accountIndex]);
    }
    else if (strncmp(str, "ARCA", 4) == 0)
    {
      venueID = VENUE_ARCA;     
      strcpy(strAccount, TradingAccount.DIRECT[accountIndex]);
    }
    else if (strncmp(str, "NYSE", 4) == 0)
    {
      venueID = VENUE_CCG;      
      strcpy(strAccount, TradingAccount.CCG[accountIndex]);
    }
    else if (strncmp(str, "BATZ", 4) == 0)
    {
      venueID = VENUE_BZX;     
      strcpy(strAccount, TradingAccount.BATSZBOE[accountIndex]);
    }
    else if (strncmp(str, "EDGX", 4) == 0)
    {
      venueID = VENUE_EDGX;
      strcpy(strAccount, TradingAccount.EDGX[accountIndex]);
    }
    else if (strncmp(str, "EDGA", 4) == 0)
    {
      venueID = VENUE_EDGA;
      strcpy(strAccount, TradingAccount.EDGA[accountIndex]);
    }
    else if (strncmp(str, "OUBX", 4) == 0)
    {
      venueID = VENUE_OUBX;
      strcpy(strAccount, TradingAccount.OUBX[accountIndex]);
    }
    else if (strncmp(str, "BATY", 4) == 0)
    {
      venueID = VENUE_BYX;     
      strcpy(strAccount, TradingAccount.BYXBOE[accountIndex]);
    }
    else if (strncmp(str, "PSX", 3) == 0)
    {
      venueID = VENUE_PSX;     
      strcpy(strAccount, TradingAccount.PSX[accountIndex]);
    }
    else
    {
      TraceLog(ERROR_LEVEL, "%s: unknown value of 'ecn' column from database: %s\n", __func__, str);
      continue;
    }
    
    //Side
    str = GetValueField(queryResult, i, 8);
    if (str[0] == 'B')
    {
      buysell = 'B';
    }
    else
    {
      if (strncmp(str, "SS", 2) == 0)
      {
        buysell = 'T';
      }
      else
      {
        buysell = 'S';
      }
    }
    
    // Broken price
    price = atof(GetValueField(queryResult, i, 14));
    
    // Broken shares
    shares = atoi(GetValueField(queryResult, i, 13));
    
    // Liquidity indicator
    strcpy(liquidity, GetValueField(queryResult, i, 4));
    
    // ECN Order Id
    str = GetValueField(queryResult, i, 0);
    if (str != NULL)
    {
      if (str[0] > 32)
      {
        strcpy(ecnOrderId, str);
      }
      else
      {
        strcpy(ecnOrderId, "''");
      }
    }
    else
    {
      strcpy(ecnOrderId, "''");
    }
    
    // Reason Code, Always is '1'
    
    // Order completed
    leftShares = atoi(GetValueField(queryResult, i, 12));
    
    // Add Break message in OJ
    AddOJOrderTradeBreakRecord(timeStamp, symbol, venueID, buysell, 
                  price, shares, orderId, liquidity,
                  ecnOrderId, execId, "1", (leftShares == 0)?1:0, strAccount);
  }

  ClearResult ( queryResult );

  return SUCCESS;
}

/****************************************************************************
- Function name:  InsertTradeOutputLogNotYetDetect
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int InsertTradeOutputLogNotYetDetect(t_Output_Log_Not_Yet_Detect *notYetDetect)
{ 
  char queryString[BUF_LENGTH];
  
  sprintf(queryString, 
      "INSERT INTO raw_crosses (ts_cid, ts_id, date, raw_content) VALUES (%d, %d, '%s', '%s')", 
      notYetDetect->tsCrossId, notYetDetect->tsIndex, notYetDetect->date, notYetDetect->content);

  PGresult * queryResult = ExecuteQuery(connQueryData, queryString );
  if( queryResult == NULL )
  {
    return ERROR;
  }
  
  ClearResult ( queryResult );
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  InsertTradeOutputLogNotYetDetect_Trade
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int InsertTradeOutputLogNotYetDetect_Trade(t_Output_Log_Not_Yet_Detect_Trade *detectTrade)
{ 
  char queryString[BUF_LENGTH];
  
  sprintf(queryString, 
      "INSERT INTO raw_crosses (ts_cid, ts_id, trade_id, date, raw_content) VALUES (%d, %d, %d, '%s', '%s')", 
      detectTrade->tsCrossId, detectTrade->tsIndex, detectTrade->tradeIndex, detectTrade->date, detectTrade->content);

  PGresult * queryResult = ExecuteQuery(connQueryData, queryString );
  if( queryResult == NULL )
  {
    return ERROR;
  }
  
  ClearResult ( queryResult );
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  InsertTradeOutputLogDetected
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int InsertTradeOutputLogDetected(t_Output_Log_Detected *detected)
{ 
  char queryString[BUF_LENGTH];
  
  sprintf(queryString, 
      "INSERT INTO raw_crosses (as_cid, symbol, ts_cid, ts_id, trade_type, date, raw_content) VALUES (%d, '%.8s', %d, %d, %d, '%s', '%s')", 
      detected->asCrossId, detected->symbol, detected->tsCrossId, detected->tsIndex, detected->tradeType, detected->date, detected->content);

  PGresult * queryResult = ExecuteQuery(connQueryData, queryString );
  if( queryResult == NULL )
  {
    return ERROR;
  }
  
  ClearResult ( queryResult );
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  InsertTradeOutputLogDetected_Trade
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int InsertTradeOutputLogDetected_Trade(t_Output_Log_Detected_Trade *detectedTrade)
{ 
  char queryString[BUF_LENGTH];
  
  sprintf(queryString, 
      "INSERT INTO raw_crosses (as_cid, symbol, ts_cid, ts_id, trade_id, trade_type, date, raw_content) VALUES (%d, '%.8s', %d, %d, %d, %d, '%s', '%s')", 
      detectedTrade->asCrossId, detectedTrade->symbol, detectedTrade->tsCrossId, detectedTrade->tsIndex, detectedTrade->tradeIndex, detectedTrade->tradeType, detectedTrade->date, detectedTrade->content);

  PGresult * queryResult = ExecuteQuery(connQueryData, queryString );
  if( queryResult == NULL )
  {
    return ERROR;
  }
  
  ClearResult ( queryResult );
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  InsertTradeOutputLogShortSellFailed
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int InsertTradeOutputLogShortSellFailed(t_Short_Sell_Failed *shortSellFailed)
{ 
  char queryString[BUF_LENGTH];
  
  sprintf(queryString, 
      "INSERT INTO raw_crosses (symbol, ts_cid, ts_id, trade_type, date, raw_content) VALUES ('%.8s', %d, %d, %d, '%s', '%s')", 
       shortSellFailed->symbol, shortSellFailed->tsCrossId, shortSellFailed->tsIndex, shortSellFailed->tradeType, shortSellFailed->date, shortSellFailed->content);

  PGresult * queryResult = ExecuteQuery(connQueryData, queryString );
  if( queryResult == NULL )
  {
    return ERROR;
  }
  
  ClearResult ( queryResult );
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  UpdateTradeOutputLog
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int UpdateTradeOutputLog(t_Output_Log *outputLog)
{ 
  char queryString[BUF_LENGTH];
  
  sprintf(queryString, 
      "UPDATE raw_crosses SET as_cid = %d, symbol = '%.8s', trade_type = %d WHERE (date = '%s') and (ts_cid = %d) and (ts_id = %d)", 
      outputLog->asCrossId, outputLog->symbol, outputLog->tradeType, outputLog->date, outputLog->tsCrossId, outputLog->tsIndex);

  PGresult * queryResult = ExecuteQuery(connQueryData, queryString );
  if( queryResult == NULL )
  {
    return ERROR;
  }
  
  ClearResult ( queryResult );
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  UpdateTradeOutputLog_Trade
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int UpdateTradeOutputLog_Trade(t_Output_Log_Trade *outputLogTrade)
{ 
  char queryString[BUF_LENGTH];
  
  sprintf(queryString, 
      "UPDATE raw_crosses SET trade_id = %d WHERE (date = '%s') and (ts_cid = %d) and (ts_id = %d)", 
      outputLogTrade->tradeId, outputLogTrade->date, outputLogTrade->crossId, outputLogTrade->tsId);

  PGresult * queryResult = ExecuteQuery(connQueryData, queryString );
  if( queryResult == NULL )
  {
    return ERROR;
  }
  
  ClearResult ( queryResult );
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  DeleteDatabaseAtDate
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int DeleteDatabaseAtDate(const char *dateString)
{
  int numOrder;
  char queryString[BUF_LENGTH];
  
  sprintf(queryString, "SELECT delete_all('%s')", dateString);

  PGresult * queryResult = ExecuteQuery(connRawData, queryString );
  if( queryResult == NULL )
  {
    return ERROR;
  }

  numOrder = atoi(GetValueField(queryResult, 0, 0));

  TraceLog(DEBUG_LEVEL, "numOrder = %d\n", numOrder);

  ClearResult ( queryResult );
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  CheckAndCreateLongShortAtDate
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int CheckAndCreateLongShortAtDate(const char *dateString)
{
  char queryString[BUF_LENGTH];
  
  sprintf(queryString, "select check_and_create_long_short('%s')", dateString);

  PGresult * queryResult = ExecuteQuery(connRawData, queryString);
  if( queryResult == NULL )
  {
    return ERROR;
  }

  int numLongShort = atoi(GetValueField(queryResult, 0, 0));

  ClearResult ( queryResult );

  if (numLongShort > 0)
  {
    // Load long short at date
    LoadLongShortAtDate(dateString);
  }
  else
  {
    longShortList.countSymbol = 0;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadLongShortAtDate
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int LoadLongShortAtDate(const char *dateString)
{
  char queryString[BUF_LENGTH];
  
  sprintf(queryString, "SELECT side, trim(symbol), shares, price, account FROM open_positions WHERE date = '%s' AND shares != 0", dateString);

  PGresult * queryResult = ExecuteQuery(connRawData, queryString);
  if( queryResult == NULL )
  {
    return ERROR;
  }
  
  int countRow = GetNumberOfRow(queryResult);

  TraceLog(DEBUG_LEVEL, "LongShort at %s: countRow = %d\n", dateString, countRow);

  int i, account;

  longShortList.countSymbol = 0;

  for (i = 0; i < countRow; i ++)
  {
    account = atoi(GetValueField(queryResult, i, 4));
    longShortList.collection[i].account = account;
    
    longShortList.collection[i].side = GetValueField(queryResult, i, 0)[0];
    strncpy(longShortList.collection[i].symbol, GetValueField(queryResult, i, 1), SYMBOL_LEN);    
    longShortList.collection[i].shares = atoi(GetValueField(queryResult, i, 2));
    longShortList.collection[i].price = atof(GetValueField(queryResult, i, 3));
    longShortList.countSymbol++;
  }

  ClearResult ( queryResult );
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadLongShortAtDate
- Input:      
- Output:     
- Return:     
- Description:    Load broken ISO orders, since when AS is crashed, and restarted,
          there might be some ISO orders were already in database but not
          in memory, so ISO Snapshot file might be missing some ISO orders,
          we need to load these missing orders into memory
- Usage:      When AS is started and historical dababase queries are run
****************************************************************************/
int LoadBrokenRawdataForISOOrderFromDatabase(const char *date)
{
  //-------------------------------------------
  //    Load from database
  //-------------------------------------------
  char queryString[1024];
  sprintf(queryString, "SELECT time_stamp, trim(symbol), trim(ecn), trim(ecn_suffix), trim(side), price, shares, order_id, bbo_id, cross_id, ts_id, account, msg_content FROM new_orders WHERE date='%s' AND is_iso = 'Y' AND (bbo_id, cross_id, ts_id) NOT IN (SELECT bbo_id, cross_id, ts_id FROM bbo_infos WHERE date='%s');", date, date);
  /*  0 timestamp
    1 symbol
    2 ecn
    3 ecn_suffix
    4 side
    5 price
    6 shares
    7 order id
    8 bbo id
    9 cross id
    10 ts id
    11 account
    12 msg content
  */
  
  PGresult *queryResult = ExecuteQuery(connRawData, queryString);
  if( queryResult == NULL )
  {
    return SUCCESS;
  }
  
  int countRow = GetNumberOfRow(queryResult);
  int i;
  int count = 0;
  char *str;
  char strTime[48];
  char needle[8];
  //-------------------------------------------
  //Parse msg_content to get additional values
  //-------------------------------------------
  /*typedef struct t_ISOOrderInfo
  {
    char strTime[19];
    char strVenueID[3];
    char strBuysell[2];
    char strPrice[10];
    char strShares[8];
    char strSequenceID[8];
    char strCapacity[2];
    char strAccount[6];
    char strRoutingInfo[6];
  } t_ISOOrderInfo;
  */
  
  t_ISOOrderInfo isoOrderInfo;
  for (i = 0; i < countRow; i ++)
  {
    //char strTime[19]; //from db: "03:29:30.736591"
    str = GetValueField(queryResult, i, 0);
    sprintf(strTime, "%s-%s", date, str); //e.g 2012-05-13:08:04:10.012345 (%Y-%m-%d:%H:%M:%S.)
    GetEpochTimeFromDateString(strTime, strTime);
    strcpy(isoOrderInfo.strTime, strTime);
    
    //strVenueID[3] && strAccount[6];
    int tradingAccountIndex = atoi(GetValueField(queryResult, i, 11));
    if (tradingAccountIndex < 0 || tradingAccountIndex > MAX_ACCOUNT)
    {
      TraceLog(ERROR_LEVEL, "(LoadBrokenRawdataForISOOrderFromDatabase): Invalid account index (%d)\n", tradingAccountIndex);
      continue;
    }

    str = GetValueField(queryResult, i, 2);
    if (strncmp(str, "OUCH", 4) == 0)
    {
      sprintf(isoOrderInfo.strVenueID, "%d", VENUE_OUCH);
      strcpy(isoOrderInfo.strAccount, TradingAccount.OUCH[tradingAccountIndex]);
    }
    else if (strncmp(str, "RASH", 4) == 0)
    {
      sprintf(isoOrderInfo.strVenueID, "%d", VENUE_RASH);
      strcpy(isoOrderInfo.strAccount, TradingAccount.RASH[tradingAccountIndex]);
    }
    else if (strncmp(str, "ARCA", 4) == 0)
    {
      sprintf(isoOrderInfo.strVenueID, "%d", VENUE_ARCA);
      strcpy(isoOrderInfo.strAccount, TradingAccount.DIRECT[tradingAccountIndex]);
    }
    else if (strncmp(str, "NYSE", 4) == 0)
    {
      sprintf(isoOrderInfo.strVenueID, "%d", VENUE_CCG);
      strcpy(isoOrderInfo.strAccount, TradingAccount.CCG[tradingAccountIndex]);
    }
    else if (strncmp(str, "BATZ", 4) == 0)
    {
      sprintf(isoOrderInfo.strVenueID, "%d", VENUE_BZX);
      strcpy(isoOrderInfo.strAccount, TradingAccount.BATSZBOE[tradingAccountIndex]);
    }
    else if (strncmp(str, "EDGX", 4) == 0)
    {
      sprintf(isoOrderInfo.strVenueID, "%d", VENUE_EDGX);
      strcpy(isoOrderInfo.strAccount, TradingAccount.EDGX[tradingAccountIndex]);
    }
    else if (strncmp(str, "EDGA", 4) == 0)
    {
      sprintf(isoOrderInfo.strVenueID, "%d", VENUE_EDGA);
      strcpy(isoOrderInfo.strAccount, TradingAccount.EDGA[tradingAccountIndex]);
    }
    else if (strncmp(str, "OUBX", 4) == 0)
    {
      sprintf(isoOrderInfo.strVenueID, "%d", VENUE_OUBX);
      strcpy(isoOrderInfo.strAccount, TradingAccount.OUBX[tradingAccountIndex]);
    }
    else if (strncmp(str, "BATY", 4) == 0)
    {
      sprintf(isoOrderInfo.strVenueID, "%d", VENUE_BYX);
      strcpy(isoOrderInfo.strAccount, TradingAccount.BYXBOE[tradingAccountIndex]);
    }
    else if (strncmp(str, "PSX", 3) == 0)
    {
      sprintf(isoOrderInfo.strVenueID, "%d", VENUE_PSX);
      strcpy(isoOrderInfo.strAccount, TradingAccount.PSX[tradingAccountIndex]);
    }
    else
    {
      TraceLog(ERROR_LEVEL, "LoadBrokenRawdataForISOOrderFromDatabase(): unknown value of 'ecn' column from database: %s\n", str);
      continue;
    }
    
    //strBuysell[2];
    str = GetValueField(queryResult, i, 4);
    if (str[0] == 'B')
    {
      isoOrderInfo.strBuysell[0] = 'B';
      isoOrderInfo.strBuysell[1] = 0;
    }
    else
    {
      if (strncmp(str, "SS", 2) == 0)
      {
        isoOrderInfo.strBuysell[0] = 'T';
        isoOrderInfo.strBuysell[1] = 0;
      }
      else
      {
        isoOrderInfo.strBuysell[0] = 'S';
        isoOrderInfo.strBuysell[1] = 0;
      }
    }
    
    //strPrice[10];
    str = GetValueField(queryResult, i, 5);
    strcpy(isoOrderInfo.strPrice, str);
    
    //strShares[8];
    str = GetValueField(queryResult, i, 6);
    strcpy(isoOrderInfo.strShares, str);
    
    //strSequenceID[8];
    str = GetValueField(queryResult, i, 7);
    strcpy(isoOrderInfo.strSequenceID, str);
    
    //Get capacity (Rule80A) and Routing Info (for RASH Only) from msgContent
    isoOrderInfo.strRoutingInfo[0] = 0;
    isoOrderInfo.strCapacity[0] = 0;

    str = GetValueField(queryResult, i, 12);  //msgContent
    
    //Get routing info
    sprintf(needle, "\001%d=", fieldIndexList.ExecBroker);
    char *b = strstr(str, needle);
    if (b)
    {
      b += strlen(needle);
      char *e = strchr(b, 1);
      if (e)
      {
        *e = 0;
        strcpy(isoOrderInfo.strRoutingInfo, b);
        *e = 1;
      }
    }
    
    //Get capacity
    int isFound = 0;
    sprintf(needle, "\001%d=", fieldIndexList.Rule80A);
    b = strstr(str, needle);
    if (b)
    {
      b += strlen(needle);
      isoOrderInfo.strCapacity[0] = *b;
      isoOrderInfo.strCapacity[1] = 0;
      isFound = 1;
    }
    
    if (isFound == 0)
    {
      sprintf(needle, "\001%d=", fieldIndexList.Rule80AIndicator);
      b = strstr(str, needle);
      if (b)
      {
        b += strlen(needle);
        isoOrderInfo.strCapacity[0] = *b;
        isoOrderInfo.strCapacity[1] = 0;
        isFound = 1;
      }
    }
    
    if (isFound == 0)
    {
      sprintf(needle, "\001%d=", fieldIndexList.Capacity);
      b = strstr(str, needle);
      if (b)
      {
        b += strlen(needle);
        isoOrderInfo.strCapacity[0] = *b;
        isoOrderInfo.strCapacity[1] = 0;
      }
    }
    
    int crossID = atoi(GetValueField(queryResult, i, 9));
    short bboID = atoi(GetValueField(queryResult, i, 8));
    char *symbol = GetValueField(queryResult, i, 1);
    
    //-------------------------------------------
    //    Add to memory
    //-------------------------------------------
    AddISOOrderToCollection(crossID, bboID, symbol, &isoOrderInfo);
    count++;
  }
  
  if ((count + countRow) != 0)
  {
    if (count != countRow)
    {
      TraceLog(WARN_LEVEL, "Loaded %d/%d missing rawdata items to update ISO snapshot file\n", count, countRow);
    }
    else
    {
      TraceLog(DEBUG_LEVEL, "Loaded %d/%d missing rawdata items to update ISO snapshot file\n", count, countRow);
    }
  }
  
  ClearResult ( queryResult );
  return SUCCESS;
}

/****************************************************************************
- Function name:  InsertTradeResultToDatabase
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int InsertTradeResultToDatabase (t_TradeResultInfo *tradeResult)
{
  char queryString[BUF_LENGTH];

  double profitLoss = 0.00;

  if ((tradeResult->totalSold > 0) && (tradeResult->totalBought > 0))
  {
    if (tradeResult->totalSold == tradeResult->totalBought)
    {
      profitLoss = tradeResult->totalSold - tradeResult->totalBought;
    }
    else
    {
      profitLoss = ((tradeResult->soldShares + tradeResult->boughtShares) / 2 - 
            abs(tradeResult->soldShares - tradeResult->boughtShares) / 2) * 
            (tradeResult->totalSold / tradeResult->soldShares - 
            tradeResult->totalBought / tradeResult->boughtShares);
    }
  }
  
  sprintf(queryString, 
            "INSERT INTO trade_results(expected_shares, expected_pl, bought_shares, total_bought, sold_shares, total_sold, left_stuck_shares, ts_profit_loss, as_cid, ts_cid, ts_id, trade_id, symbol, date, timestamp, spread) VALUES (%d, %lf, %d, %lf, %d, %lf, %d, %lf, %d, %d, %d, %d, '%.8s', '%s', %d, %d)",
            tradeResult->expectedShares, tradeResult->expectedPL,
            tradeResult->boughtShares, tradeResult->totalBought,
            tradeResult->soldShares, tradeResult->totalSold,
            tradeResult->leftStuckShares, profitLoss,
            tradeResult->as_cid, tradeResult->ts_cid, tradeResult->ts_id, tradeResult->trade_id,
            tradeResult->symbol, tradeResult->dateString, tradeResult->timestamp, (int)(tradeResult->spread * 100 + 0.005));

  PGresult * queryResult = ExecuteQuery(connQueryData, queryString);

  if( queryResult == NULL )
  {
    return ERROR;
  }
  
  ClearResult ( queryResult );

  return SUCCESS;
}

/****************************************************************************
- Function name:  UpdateRealValueToDatabase
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int UpdateRealValueToDatabase(t_Real_Value *realValue)
{
  char queryString[BUF_LENGTH];
  
  sprintf(queryString, 
      "UPDATE trade_results SET left_stuck_shares = %d, pm_profit_loss = %lf, pm_total_fee = %lf WHERE date = '%s' AND ts_id = %d AND ts_cid = %d", 
      realValue->leftShares, realValue->profitLoss, realValue->totalFee, realValue->date, realValue->tsId, realValue->crossId);

  PGresult * queryResult = ExecuteQuery(connQueryData, queryString);

  if( queryResult == NULL )
  {
    return ERROR;
  }
  
  ClearResult ( queryResult );

  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadStuckFromDatabase
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int LoadStuckFromDatabase(char *dateString)
{
  char queryString[BUF_LENGTH];
  
  sprintf(queryString, 
      "SELECT ts_id, ts_cid, trim(symbol::text), bought_shares, total_bought, sold_shares, total_sold, left_stuck_shares, pm_profit_loss, pm_total_fee FROM trade_results WHERE date = '%s' AND left_stuck_shares != 0 ORDER BY id", 
      dateString);

  //TraceLog(DEBUG_LEVEL, "queryString = %s\n", queryString);

  PGresult * queryResult = ExecuteQuery(connRawData, queryString );
  if( queryResult == NULL )
  {
    return ERROR;
  }

  int countRow = GetNumberOfRow(queryResult);

  TraceLog(DEBUG_LEVEL, "%s at %s: countRow = %d\n", __func__, dateString, countRow);

  t_StuckInfo stuckInfo;
  int i, side, soldShares, boughtShares;
  double totalSold, totalBought;
  char symbol[SYMBOL_LEN];

  for (i = 0; i < countRow; i ++)
  {
    stuckInfo.tsIndex = atoi(GetValueField(queryResult, i, 0));
    stuckInfo.crossId = atoi(GetValueField(queryResult, i, 1));
    
    strncpy(symbol, GetValueField(queryResult, i, 2), SYMBOL_LEN);

    stuckInfo.timestamp = -1;

    boughtShares = atoi(GetValueField(queryResult, i, 3));
    totalBought = atof(GetValueField(queryResult, i, 4));

    soldShares = atoi(GetValueField(queryResult, i, 5));
    totalSold = atof(GetValueField(queryResult, i, 6));

    stuckInfo.stuckShares = abs(atoi(GetValueField(queryResult, i, 7)));
    stuckInfo.realValue = atof(GetValueField(queryResult, i, 8));
    stuckInfo.totalFee = atof(GetValueField(queryResult, i, 9));

    // Side, NumOfShares, StuckPrice
    if (soldShares > boughtShares)
    {
      // Short
      side = ASK_SIDE;
      stuckInfo.stuckPrice = totalSold / soldShares;
    }
    else
    {
      // Long
      side = BID_SIDE;
      stuckInfo.stuckPrice = totalBought / boughtShares;
    }

    TraceLog(DEBUG_LEVEL, "tsId = %d, crossId = %d, symbol = %.8s, side = %d, stuckShares = %d, stuckPrice = %lf, realValue = %lf\n", stuckInfo.tsIndex, stuckInfo.crossId, symbol, side, stuckInfo.stuckShares, stuckInfo.stuckPrice, stuckInfo.realValue);

    LoadAndCalculateRealValue(symbol, side, &stuckInfo, dateString);
  }
  
  ClearResult ( queryResult );

  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadTradeLossFromDatabase
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int LoadTradeLossFromDatabase(char *dateString)
{
  char queryString[BUF_LENGTH];
  
  sprintf(queryString, 
      "SELECT symbol, ts_id, ts_profit_loss "
      "FROM trade_results "
      "WHERE date = '%s' AND ts_profit_loss < 0.00",
      dateString);

  TraceLog(DEBUG_LEVEL, "%s: queryString = '%s'\n", __func__, queryString);

  PGresult *queryResult = ExecuteQuery(connRawData, queryString);
  if (queryResult == NULL)
  {
    return ERROR;
  }

  int countRow = GetNumberOfRow(queryResult);

  TraceLog(DEBUG_LEVEL, "%s at %s: countRow = %d\n", __func__, dateString, countRow);

  char symbol[SYMBOL_LEN];
  int symbolIndex;
  int tsIndex;
  double lossValue;

  int i;
  for (i = 0; i < countRow; i ++)
  {
    strncpy(symbol, GetValueField(queryResult, i, 0), SYMBOL_LEN);
    tsIndex = atoi(GetValueField(queryResult, i, 1));
    lossValue = fabs(atof(GetValueField(queryResult, i, 2)));
    
    symbolIndex = GetStockSymbolIndex(symbol);
    
    if (symbolIndex != -1)
    {
      ProfitLossPerSymbol[symbolIndex].asLoss += lossValue;
      if (tsIndex > -1 && tsIndex < MAX_TRADE_SERVER_CONNECTIONS)
      {
        ProfitLossPerSymbol[symbolIndex].tsLoss[tsIndex] += lossValue;
      }
      else
      {
        TraceLog(DEBUG_LEVEL, "(%s) calculate loss (%lf, %.8s) for AS only\n", __func__, lossValue, symbol);
      }
      
      // DEVLOGF("Symbol %s: tsLoss[%d] = %lf, asLoss = %lf", symbol, tsIndex, ProfitLossPerSymbol[symbolIndex].tsLoss[tsIndex], ProfitLossPerSymbol[symbolIndex].asLoss);
    }
    else
    {
      TraceLog(ERROR_LEVEL, "%s: Can not get stock symbol index for '%.8s'\n", __func__, symbol);
    }
  }
  
  ClearResult (queryResult);

  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadTradingStatusFromDatabase
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int LoadTradingStatusFromDatabase(int *isDisableTrading, int *isUpdate)
{
  *isDisableTrading = 0;
  *isUpdate = 0;

  PGresult * queryResult = ExecuteQuery(connRawData, "SELECT status, updated FROM trading_status");
  if (queryResult == NULL)
  {
    return ERROR;
  }

  if (GetNumberOfRow(queryResult) == 0)
  {
    TraceLog(ERROR_LEVEL, "table 'trading_status' contains no record\n");
    ClearResult (queryResult);
    return ERROR;
  }
  
  *isDisableTrading = atoi(GetValueField(queryResult, 0, 0));
  *isUpdate = atoi(GetValueField(queryResult, 0, 1));
  
  ClearResult ( queryResult );

  return SUCCESS;
}

/****************************************************************************
- Function name:  UpdateTradingStatusToDatabase
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int UpdateTradingStatusToDatabase(int tradingStatus, int isUpdate)
{
  char queryString[BUF_LENGTH];

  sprintf(queryString, "UPDATE trading_status SET status = %d, updated = %d", tradingStatus, isUpdate);

  PGresult * queryResult = ExecuteQuery(connRawData, queryString);
  if( queryResult == NULL )
  {
    return ERROR;
  }
  
  ClearResult ( queryResult );

  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadModifyLongShortStatusFromDatabase
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int LoadModifyLongShortStatusFromDatabase(int *rmUpdated)
{
  *rmUpdated = 0;

  PGresult * queryResult = ExecuteQuery(connRawData, "SELECT rmUpdated FROM trading_status" );
  if( queryResult == NULL )
  {
    return ERROR;
  }

  if (GetNumberOfRow(queryResult) == 0)
  {
    TraceLog(ERROR_LEVEL, "table 'trading_status' contains no record\n");
    ClearResult (queryResult);
    return ERROR;
  }
  
  *rmUpdated = atoi(GetValueField(queryResult, 0, 0));
  
  ClearResult ( queryResult );

  return SUCCESS;
}

/****************************************************************************
- Function name:  UpdateModifyLongShortStatusToDatabase
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int UpdateModifyLongShortStatusToDatabase(void)
{
  char queryString[BUF_LENGTH];

  sprintf(queryString, "UPDATE risk_managements SET manual_update = 0 WHERE date = '%s'; UPDATE trading_status SET rmUpdated = 0;", globalDateStringOfDatabase);
  
  //TraceLog(DEBUG_LEVEL, "queryString = %s\n", queryString);

  PGresult * queryResult = ExecuteQuery(connRawData, queryString);
  if( queryResult == NULL )
  {
    return ERROR;
  }
  
  ClearResult ( queryResult );

  return SUCCESS;
}
/****************************************************************************
- Function name:  LoadHungOrderStatusFromDatabase
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int LoadHungOrderStatusFromDatabase(void)
{
  int isUpdate = 0;

  PGresult * queryResult = ExecuteQuery(connRawData, "SELECT hung_order FROM trading_status" );
  if( queryResult == NULL )
  {
    return ERROR;
  }

  if (GetNumberOfRow(queryResult) == 0)
  {
    TraceLog(ERROR_LEVEL, "table 'trading_status' contains no record\n");
    ClearResult (queryResult);
    return NOT_YET;
  }
  
  isUpdate = atoi(GetValueField(queryResult, 0, 0));
  
  ClearResult ( queryResult );

  return ((isUpdate > 0) ? UPDATED : NOT_YET);
}

/****************************************************************************
- Function name:  UpdateHungOrderStatusToDatabase
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int UpdateHungOrderStatusToDatabase(void)
{
  char queryString[BUF_LENGTH];

  sprintf(queryString, "UPDATE trading_status SET hung_order = 0;");

  PGresult * queryResult = ExecuteQuery(connRawData, queryString);
  if( queryResult == NULL )
  {
    return ERROR;
  }
  
  ClearResult ( queryResult );

  return SUCCESS;
}

/****************************************************************************
- Function name:  InsertAskBid
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int InsertAskBid(t_Output_Log_Ask_Bid *askBid)
{ 
  char queryString[BUF_LENGTH];

  //TraceLog(DEBUG_LEVEL, "%d -> %s\n", ecnLaunch, ecnLaunchStr[ecnLaunch]);

  sprintf(queryString, 
      "INSERT INTO ask_bids (ask_bid, ecn_launch, shares, price, ts_id, ts_cid, date) VALUES ('%s', '%s', %d, %lf, %d, %d, '%s')", 
      askbidStr[askBid->askBid], ecnLaunchStr[askBid->ecnLaunch], askBid->shares, askBid->price, askBid->tsIndex, askBid->tsCrossId, askBid->date);

  //TraceLog(DEBUG_LEVEL, "queryString = %s\n", queryString);

  PGresult * queryResult = ExecuteQuery(connQueryData, queryString );
  if( queryResult == NULL )
  {
    return ERROR;
  }
  
  ClearResult ( queryResult );
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  InsertBBOQuotesIntoDatabase
- Input:      
- Output:     
- Return:     
- Description:  
- Usage:      
****************************************************************************/
int   InsertBBOQuotesIntoDatabase(t_dbBBOInfo *bboInfo)
{
  char  insertString[BUF_LENGTH];
  
  sprintf( insertString, "INSERT INTO bbo_infos(ts_id, cross_id, date, content, bbo_id) VALUES (%d, %d, '%s', '%s', %d)", 
      bboInfo->ts_id, bboInfo->cross_id, globalDateStringOfDatabase, bboInfo->contents, bboInfo->bbo_id);
  
  PGresult * queryResult = ExecuteQuery(connQueryData, insertString );
  if( queryResult == NULL ){ return ERROR; }
  ClearResult ( queryResult );
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  InitializeTraderStatus
- Input:      
- Output:     
- Return:     
- Description:  
- Usage:      
****************************************************************************/
int InitializeTraderStatus(void)
{ 
  int i, numRow = 0;
  char userName[MAX_LINE_LEN] = "\0";
  
  pthread_mutex_lock(&connSaveSettings_Mutex);
  PGresult * queryResult = ExecuteQuery(connSaveSettings, "SELECT username, password, status FROM user_login_info");
  pthread_mutex_unlock(&connSaveSettings_Mutex);
  
  if( queryResult == NULL )
  {
    return ERROR;
  }
  
  numRow = GetNumberOfRow( queryResult ); 
  if (numRow == 0)
  {
    TraceLog(WARN_LEVEL, "table 'user_login_info' contains no record\n");
    ClearResult (queryResult);
    return ERROR;
  }
  
  if (numRow > 0)
  {
    for (i = 0; i < numRow; i++)
    {
      //Get trader on database
      strcpy(userName, GetValueField(queryResult, i, 0));
      TrimRight(userName, strnlen(userName, MAX_LINE_LEN));
      
      //Initialize status of trader to OFFLINE
      UpdateTraderStatusToDatabase(userName, OFFLINE);
    }
  }

  ClearResult ( queryResult );
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  LdapAuthentication
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int LdapAuthentication(char userName[MAX_LINE_LEN], char password[MAX_LINE_LEN])
{
  char baseDN[256];
  sprintf(baseDN, "uid=%s,%s", userName, LdapInfo.userOU);
  
  char ldapServer[256];
  sprintf(ldapServer, "%s:%d", LdapInfo.url, LdapInfo.port);
  TraceLog(DEBUG_LEVEL, "Connecting LDAP Server (%s) as '%s'\n", ldapServer, baseDN);
  
  // Open LDAP Connection
  LDAP *ld;
  int rc = ldap_initialize(&ld, ldapServer);
  if (rc != LDAP_SUCCESS)
  {
    TraceLog(ERROR_LEVEL, "Can not initialize LDAP: error=%d: %s\n", rc, ldap_err2string(rc));
    return ERROR;
  }
   
  // User authentication (bind)
  // rc = ldap_simple_bind_s(ld, baseDN, password); //this function is deprecated
  struct berval credentials;
  credentials.bv_val = password;
  credentials.bv_len = strlen(password);
  rc = ldap_sasl_bind_s(ld, baseDN, LDAP_SASL_SIMPLE, &credentials, NULL, NULL, NULL);
  
  int retValue;
  if (rc == LDAP_SUCCESS)
  {
    // Check if trader has logged or not?
    int flag = IsTraderExisted(userName);
    if (flag == NO)
    {
      //This trader does not exist, add him to database
      if (InsertNewTraderInfo(userName) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "Can not add new trader '%s' to database\n", userName);
        retValue = ERROR;
      }
      else
      {
        retValue = SUCCESS;
      }
    }
    else if (flag == YES)
    {
      int status = GetStatusOfTrader(userName);
      if (status == OFFLINE)
      {
        retValue = SUCCESS;
      }
      else if (status == ERROR)
      {
        TraceLog(ERROR_LEVEL, "Can not check status of '%s'\n", userName);
        retValue = ERROR;
      }
      else
      {
        retValue = USER_LOGGED;
      }
    }
    else if (flag == ERROR)
    {
      TraceLog(ERROR_LEVEL, "Can not query database for trader '%s'\n", userName);
      retValue = ERROR;
    }
  }
  else if (rc == LDAP_INVALID_CREDENTIALS)
  {
    // Invalid password
    TraceLog(DEBUG_LEVEL, "Invalid password of '%s'\n", userName);
    retValue = USER_PASSWORD_INCORRECT;
  }
  else
  {
    // An error occurs
    TraceLog(ERROR_LEVEL, "Can not bind to LDAP server: error=%d: %s\n", rc, ldap_err2string(rc));
    retValue = ERROR;
  }

  // rc = ldap_unbind(ld); //this function is deprecated
  rc = ldap_unbind_ext(ld, NULL, NULL);
  if (rc != LDAP_SUCCESS)
  {
    TraceLog(ERROR_LEVEL, "Can not unbind LDAP: error=%d: %s\n", rc, ldap_err2string(rc));
    retValue = ERROR;
  }
  
  return retValue;
}

/****************************************************************************
- Function name:  AuthenticateLoginInfo
- Input:      
- Output:     
- Return:     
- Description:  
- Usage:      
****************************************************************************/
int AuthenticateLoginInfo(char userName[MAX_LINE_LEN], char password[MAX_LINE_LEN])
{ 
  int status, numRow = 0;
  char selectString[BUF_LENGTH];

  sprintf(selectString, "SELECT status FROM user_login_info WHERE lower(username) = '%s' AND password = '%s'",  userName, password);
  
  pthread_mutex_lock(&connSaveSettings_Mutex);
  PGresult * queryResult = ExecuteQuery(connSaveSettings, selectString);
  pthread_mutex_unlock(&connSaveSettings_Mutex);
  
  if( queryResult == NULL )
  {
    return ERROR;
  }
  
  numRow = GetNumberOfRow( queryResult );
  
  if (numRow > 0)
  {
    //Get trader status on database
    status = atoi(GetValueField(queryResult, 0, 0));
    if(status != OFFLINE)
    {
      ClearResult ( queryResult );
      return USER_LOGGED;
    }
    else
    {
      ClearResult ( queryResult );
      return SUCCESS;
    }
  }
  else
  {
    ClearResult ( queryResult );
    return USER_PASSWORD_INCORRECT;
  }
  
  ClearResult ( queryResult );
  
  return ERROR;
}

/****************************************************************************
- Function name:  UpdateTTEventLogToDatabase
- Input:      
- Output:     
- Return:   
- Description:  
- Usage:      
****************************************************************************/
int UpdateTTEventLogToDatabase(char userName[MAX_LINE_LEN], int activityType, const char *activityDetail, const char *dateString, const char *timeStamp)
{ 
  char queryString[BUF_LENGTH];
  
  sprintf(queryString, "INSERT INTO tt_event_log(username, activity_type, log_detail, date, time_stamp) VALUES ('%s', %d, '%s', '%s', '%s')", userName, activityType, activityDetail, dateString, timeStamp);

  pthread_mutex_lock(&connSaveSettings_Mutex);
  PGresult * queryResult = ExecuteQuery(connSaveSettings, queryString );
  pthread_mutex_unlock(&connSaveSettings_Mutex);
  
  if( queryResult == NULL )
  {
    return ERROR;
  }
  
  ClearResult ( queryResult );
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  UpdateTraderStatusToDatabase
- Input:      
- Output:     
- Return:     
- Description:  
- Usage:      
****************************************************************************/
int UpdateTraderStatusToDatabase(char userName[MAX_LINE_LEN], int status)
{
  char queryString[BUF_LENGTH];
  
  sprintf(queryString, "SELECT update_status_for_user_login_info('%s', %d)", userName, status);

  pthread_mutex_lock(&connSaveSettings_Mutex);
  PGresult * queryResult = ExecuteQuery(connSaveSettings, queryString );
  pthread_mutex_unlock(&connSaveSettings_Mutex);
  
  if( queryResult == NULL )
  {
    return ERROR;
  }
  
  if (GetNumberOfRow( queryResult ) == 0)
  {
    TraceLog(ERROR_LEVEL, "The following query returns no record: %s\n", queryString);
    ClearResult ( queryResult );
    exit(0);
  }
  
  int type = atoi( GetValueField( queryResult, 0, 0 ));
  
  ClearResult ( queryResult );
  
  return type;
}

/****************************************************************************
- Function name:  ProcessUpdateTTEventLogToDatabase
- Input:      
- Output:     
- Return:   
- Description:  
- Usage:      
****************************************************************************/
int ProcessUpdateTTEventLogToDatabase(char userName[MAX_LINE_LEN], int activityType, const char *activityDetail)
{
  char timeStamp[MAX_TIMESTAMP] = "\0";
  GetTimeStampString(timeStamp);

  UpdateTTEventLogToDatabase(userName, activityType, activityDetail, dateString, timeStamp);

  return SUCCESS;
}

/****************************************************************************
- Function name:  IsTraderExisted
- Input:      
- Output:     
- Return:     YES   userName exists
          NO    userName does not exist
          ERROR an error occurs
- Description:    Check if trader is existed in database or not
- Usage:      
****************************************************************************/
int IsTraderExisted(char userName[MAX_LINE_LEN])
{
  char selectString[BUF_LENGTH];
  sprintf(selectString, "SELECT status FROM user_login_info WHERE lower(username) = '%s'",  userName);

  pthread_mutex_lock(&connSaveSettings_Mutex);
  PGresult * queryResult = ExecuteQuery(connSaveSettings, selectString);
  pthread_mutex_unlock(&connSaveSettings_Mutex);
  
  if( queryResult == NULL )
  {
    return ERROR;
  }
  
  int numberTrader = GetNumberOfRow( queryResult );
  
  ClearResult(queryResult);
  
  if (numberTrader == 0)
  {
    return NO;
  }
  
  return YES;
}

/****************************************************************************
- Function name:  InsertNewTraderInfo
- Input:      
- Output:     
- Return:     
- Description:    Insert new trader to user_login_info
- Usage:      
****************************************************************************/
int InsertNewTraderInfo(char userName[MAX_LINE_LEN])
{
  char queryString[BUF_LENGTH];

  sprintf(queryString, 
      "INSERT INTO user_login_info(username, password, status) VALUES ('%s', '', 0)", 
      userName);

  pthread_mutex_lock(&connSaveSettings_Mutex);
  PGresult * queryResult = ExecuteQuery(connSaveSettings, queryString);
  pthread_mutex_unlock(&connSaveSettings_Mutex);
  
  if( queryResult == NULL )
  {
    return ERROR;
  }
  
  ClearResult ( queryResult );
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  GetStatusOfTrader
- Input:      
- Output:     
- Return:     ERROR an error occurs
          status code of userName
- Description:  
- Usage:      
****************************************************************************/
int GetStatusOfTrader(char userName[MAX_LINE_LEN])
{
  char selectString[BUF_LENGTH];
  sprintf(selectString, "SELECT status FROM user_login_info WHERE lower(username) = '%s'",  userName);

  pthread_mutex_lock(&connSaveSettings_Mutex);
  PGresult * queryResult = ExecuteQuery(connSaveSettings, selectString);
  pthread_mutex_unlock(&connSaveSettings_Mutex);
  
  if( queryResult == NULL )
  {
    return ERROR;
  }
  
  int numberTrader = GetNumberOfRow( queryResult );
  
  if (numberTrader == 0)
  {
    return ERROR;
  }
  
  char *ret = NULL;
  int status;
  
  ret = GetValueField(queryResult, 0, 0);
  if (ret == NULL)
  {
    status = OFFLINE;
  }
  else
  {
    status = atoi(ret);
  }

  ClearResult(queryResult);
  
  return status;
}

/****************************************************************************
- Function name:  GetTraderStatusInfo
- Input:      
- Output:     
- Return:     
- Description:  
- Usage:      
****************************************************************************/
int GetTraderStatusInfo(void)
{ 
  int i, numberTrader = 0;
  
  pthread_mutex_lock(&connSaveSettings_Mutex);
  PGresult * queryResult = ExecuteQuery(connSaveSettings, "SELECT id, lower(username), status FROM user_login_info");
  pthread_mutex_unlock(&connSaveSettings_Mutex);
  
  if( queryResult == NULL )
  {
    return ERROR;
  }
  
  numberTrader = GetNumberOfRow( queryResult ); 
  traderCollection.numberTrader = numberTrader;
  
  if (numberTrader > MAX_TRADER)
  {
    TraceLog(ERROR_LEVEL, "Load trader info from database: numberOfTrader (= %d) > MaxTradeServer (= %d)\n", numberTrader, MAX_TRADER);

    return ERROR;
  }
  else if (numberTrader == 0)
  {
    TraceLog(ERROR_LEVEL, "Load trader info from database: no data\n");

    return ERROR;
  }
  
  char *ret = NULL;
  for (i = 0; i < numberTrader; i++)
  {
    traderCollection.traderStatusInfo[i].id = atoi(GetValueField(queryResult, i, 0));
    
    ret = GetValueField( queryResult, i, 1 ); //username
    if (ret == NULL)
    {
      TraceLog(ERROR_LEVEL, "Load trader info from database: 'username' column has NULL value!\n");
      ClearResult ( queryResult );
      return ERROR;
    }
    
    strcpy(traderCollection.traderStatusInfo[i].userName, ret);
    
    ret = GetValueField( queryResult, i, 2 ); //status
    if (ret == NULL)
      traderCollection.traderStatusInfo[i].status = OFFLINE;
    else
      traderCollection.traderStatusInfo[i].status = atoi(ret);
  }
  
  ClearResult ( queryResult );
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  GetTraderActiveInfo
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int GetTraderActiveInfo(t_TraderStatusInfo *traderActive)
{
  traderActive->id = -1;

  int i;
  for (i = 0; i < traderCollection.numberTrader; i++)
  {
    if (traderCollection.traderStatusInfo[i].status == ACTIVE)
    {
      memcpy(traderActive, &traderCollection.traderStatusInfo[i], sizeof(t_TraderStatusInfo));

      return traderActive->id;
    }
  }

  return -1;
}

/****************************************************************************
- Function name:  CountTraderHasStatus
****************************************************************************/
int CountTraderHasStatus(int status)
{
  int i, numTrader = 0;
  for (i = 0; i < traderCollection.numberTrader; i++)
  {
    if (traderCollection.traderStatusInfo[i].status == status) numTrader++;
  }

  return numTrader;
}

/****************************************************************************
- Function name:  ProcessUpdateTraderStatusToDatabase
- Input:      
- Output:     
- Return:   
- Description:  
- Usage:      
****************************************************************************/
int ProcessUpdateTraderStatusToDatabase(char userName[MAX_LINE_LEN], int status)
{
  t_TraderStatusInfo oldTraderActive;
  GetTraderActiveInfo(&oldTraderActive);

  //Update trader status to database
  int type = UpdateTraderStatusToDatabase(userName, status);
  
  //Get trader status from database
  GetTraderStatusInfo();
  
  t_TraderStatusInfo newTraderActive;
  GetTraderActiveInfo(&newTraderActive);

  if (newTraderActive.id == -1)
  {
    BuildNSendNoActiveTraderAlertToAllDaedalus();
    BuildNSendNoActiveTraderAlertToAllTT();
    
    int numTrader = CountTraderHasStatus(ACTIVE_ASSISTANT);
    TraceLog(DEBUG_LEVEL, "Total active assist traders: %d\n", numTrader);
    if (numTrader == 0)
    {
      // There is no Active and Active Assist traders, trading is disabled
      TraceLog(DEBUG_LEVEL, "No Active and Active Assist trader. Trading is disabling\n");

      SendDisableTradingAllOrderToAllTS(TS_TRADING_DISABLED_BY_NO_ACTIVE_OR_ACTIVE_ASSIST_TRADER);
      SendDisableTradingPMToPM(PM_TRADING_DISABLED_BY_NO_ACTIVE_OR_ACTIVE_ASSIST_TRADER);
    }
  }
  else
  {
    if (newTraderActive.id != oldTraderActive.id)
    {
      BuildNSendNewActiveTraderAlertToAllDaedalus(newTraderActive.userName);
      BuildNSendNewActiveTraderAlertToAllTT(newTraderActive.userName);
    }
  }
  
  SendTraderStatusInfoToAllDaedalus(type);
  SendTraderStatusInfoToAllTT(type);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  DisableRASHOnAS
- Input:      
- Output:     
- Return:     
- Description:  
- Usage:      
****************************************************************************/
int DisableRASHOnAS(void)
{
  char  updateString[BUF_LENGTH];

  asSetting.orderEnable[AS_NASDAQ_RASH_INDEX] = NO;
  
  sprintf( updateString, 
      "UPDATE as_settings SET nasdaq_rash = %d where mid = %d",       
      asSetting.orderEnable[AS_NASDAQ_RASH_INDEX], AS_MODULE);
  
  pthread_mutex_lock(&connSaveSettings_Mutex);
  PGresult * queryResult = ExecuteQuery(connSaveSettings, updateString );
  pthread_mutex_unlock(&connSaveSettings_Mutex);
  
  if( queryResult == NULL )
  {
    return ERROR;
  }
  
  ClearResult ( queryResult );
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  EnableRASHOnPM
- Input:      
- Output:     
- Return:     
- Description:  
- Usage:      
****************************************************************************/
int EnableRASHOnPM(void)
{ 
  char  updateString[BUF_LENGTH];

  pmSetting.orderEnable[PM_NASDAQ_RASH_INDEX] = YES;
  
  sprintf( updateString, 
      "UPDATE as_settings SET nasdaq_rash = %d where mid = %d",       
      pmSetting.orderEnable[PM_NASDAQ_RASH_INDEX], PM_MODULE);
  
  pthread_mutex_lock(&connSaveSettings_Mutex);
  PGresult * queryResult = ExecuteQuery(connSaveSettings, updateString );
  pthread_mutex_unlock(&connSaveSettings_Mutex);
  
  if( queryResult == NULL )
  {
    return ERROR;
  }
  
  ClearResult ( queryResult );
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadLogMeasurementFromDatabase
- Input:      
- Output:     
- Return:     
- Description:  
- Usage:      
****************************************************************************/
int LoadLogMeasurementFromDatabase(void)
{ 
  // Querry String
  char queryString[BUF_LENGTH];
  
  // Reset querry string
  memset(queryString, ' ', BUF_LENGTH);
  
  /*
    Get MAX entry new order ID
  */
  sprintf(queryString,"SELECT max(oid) FROM vw_entry_delays WHERE date = '%s'", globalDateStringOfDatabase);
  //sprintf(queryString,"SELECT max(id) FROM vw_entry_delays WHERE date = '2012-06-01'");
  
  PGresult * queryResult = ExecuteQuery(connTimeSeries, queryString);
  
  if( queryResult == NULL )
  {
    return ERROR;
  }
  
  // Get current new order id
  CurEntryNewOrderID = atoi(GetValueField(queryResult, 0, 0));
  
  // Clear querry buffer
  ClearResult(queryResult);
  
  /*
    Get MAX ACK ID
  */
  sprintf(queryString,"SELECT max(oid) FROM vw_ack_delays WHERE date = '%s'", globalDateStringOfDatabase);

  queryResult = ExecuteQuery(connTimeSeries, queryString);
  
  if( queryResult == NULL )
  {
    return ERROR;
  }
  
  //Get current ack id
  CurAckID = atoi(GetValueField(queryResult, 0, 0));
  
  // Clear querry buffer
  ClearResult(queryResult);
  
  /*
    Get MAX Fill ID
  */
  sprintf(queryString,"SELECT max(fill_id) FROM vw_fill_delays WHERE date = '%s'", globalDateStringOfDatabase);

  queryResult = ExecuteQuery(connTimeSeries, queryString);
  
  if( queryResult == NULL )
  {
    return ERROR;
  }
  
  // Get current fill ID
  CurFillID = atoi(GetValueField(queryResult, 0, 0));
  
  // Clear querry buffer
  ClearResult(queryResult);
  
  /*
    Get MAX Cancel ID
  */
  sprintf(queryString,"SELECT max(id) FROM vw_cancel_delays WHERE date = '%s'", globalDateStringOfDatabase);
  
  queryResult = ExecuteQuery(connTimeSeries, queryString);
  
  if( queryResult == NULL )
  {
    return ERROR;
  }
  
  //Get current cancel ID
  CurCancelID = atoi(GetValueField(queryResult, 0, 0));
  
  // Clear querry buffer
  ClearResult(queryResult);
  
  /*
    Get MAX Exit order ID
  */
  sprintf(queryString,"SELECT max(id) FROM vw_exit_delays WHERE date = '%s'", globalDateStringOfDatabase);
  
  queryResult = ExecuteQuery(connTimeSeries, queryString);
  
  if( queryResult == NULL )
  {
    return ERROR;
  }
  
  //Get current cancel ID
  CurExitNewOrderID = atoi(GetValueField(queryResult, 0, 0));
  
  // Clear the result
  ClearResult(queryResult);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessEntryNewOrderDelayTime
- Input:      
- Output:     
- Return:     
- Description:  
- Usage:      
****************************************************************************/
int ProcessEntryNewOrderDelayTime(char *time)
{ 
  // Querry String
  char queryString[BUF_LENGTH];
  
  // Reset querry string
  memset(queryString, ' ', BUF_LENGTH);
  
  // Number of records
  int numRecords;
  
  // Build the query string
  sprintf(queryString,"SELECT oid, time_stamp, quote_time_stamp, ecn, reason, order_id FROM vw_entry_delays WHERE date = '%s' AND oid > %d ORDER BY oid", globalDateStringOfDatabase, CurEntryNewOrderID);
  
  // Execute the query
  PGresult * queryResult = ExecuteQuery(connTimeSeries, queryString);
  
  if( queryResult == NULL )
  {
    return ERROR;
  }
  
  // get the number of records
  numRecords = GetNumberOfRow(queryResult);
  
  if(numRecords <= 0)
  {
    ClearResult(queryResult);
    return ERROR;
  }
  else
  { 
    //Get max ID of the new order
    CurEntryNewOrderID = atoi(GetValueField(queryResult, numRecords - 1, 0));
    
    char timeStamp[32];
    long newOrderTimeStamp, quoteTimeStamp;
    int entryDelay;
    char ecn[5] = "\0";
    int reason;
    int orderId;
    char eventTime[32] = "\0";
    
    int i;
    for (i = 0; i < numRecords; i++)
    { 
      // New order time stamp
      strcpy(timeStamp, GetValueField( queryResult, i, 1 ));
      newOrderTimeStamp = ConvertToEpochTime(timeStamp);
      sprintf(eventTime, "%ld.%06d", newOrderTimeStamp / 1000000, (int)(newOrderTimeStamp % 1000000));
      
      // New Quote time stamp
      quoteTimeStamp = atol(GetValueField(queryResult, i, 2));
    
      entryDelay = newOrderTimeStamp - quoteTimeStamp;
      
      strcpy(ecn, GetValueField( queryResult, i, 3 ));
      reason = atoi(GetValueField( queryResult, i, 4 ));
      
      orderId = atoi(GetValueField( queryResult, i, 5 ));

      if (entryDelay > 1000)
      {
        TraceLog(WARN_LEVEL, "High entry delay: orderId (%d), delay (%d us)\n", orderId, entryDelay);
      }
      else if (entryDelay < 0)
      {
        TraceLog(WARN_LEVEL, "Invalid entry delay: orderId (%d), delay (%d us), time: %s vs %s\n", orderId, entryDelay,
            timeStamp, GetValueField(queryResult, i, 2));
      }

      //Entry quote delay
      switch (reason) //These are from Trade Server Output Log
      {
        case 2: //Re-Entry
          AddTimeSeriesDelayTimeRecord(time, entryDelay, REENTRY_ORDER, GetVenueName(ecn), GetServerName(orderId), eventTime, NULL);
          break;
          
        default: //Entry
          AddTimeSeriesDelayTimeRecord(time, entryDelay, ENTRY_ORDER, GetVenueName(ecn), GetServerName(orderId), eventTime, NULL);
          break;
      }
    }
  }
  
  // Clear the result
  ClearResult(queryResult);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessAckDelayTime
- Input:      
- Output:     
- Return:     
- Description:  
- Usage:      
****************************************************************************/
int ProcessAckDelayTime(char *time)
{ 
  // Querry String
  char queryString[BUF_LENGTH];
  
  // Reset querry string
  memset(queryString, ' ', BUF_LENGTH);
  
  // Number of records
  int numRecords;
  
  // Build the query string
  sprintf(queryString,"SELECT oid, ack_delay, ack_internal_delay, entry_exit, ecn, order_id, time_stamp, routing_inst FROM vw_ack_delays WHERE date = '%s' AND oid > %d ORDER BY oid", globalDateStringOfDatabase, CurAckID);

  // Execute the query
  PGresult * queryResult = ExecuteQuery(connTimeSeries, queryString);
  
  if( queryResult == NULL )
  {
    return ERROR;
  }
  
  // get the number of records
  numRecords = GetNumberOfRow(queryResult);
  
  if(numRecords <= 0)
  {
    ClearResult(queryResult);
    return ERROR;
  }
  else
  { 
    //Get max ID of the new order
    CurAckID = atoi(GetValueField(queryResult, numRecords - 1, 0));
    
    int ackDelay;
    int ackInternalDelay;
    char entryExit[2];
    char ecn[5] = "\0";
    int orderId;
    char eventTime[32] = "\0";
    
    int i;
    for (i = 0; i < numRecords; i++)
    {   
      //ACK delay time
      ackDelay = atoi(GetValueField(queryResult, i, 1));
      ackInternalDelay = atoi(GetValueField(queryResult, i, 2));
      
      // Entry exit
      strncpy(entryExit, GetValueField(queryResult, i, 3), 2);
      
      strcpy(ecn, GetValueField(queryResult, i, 4));
      
      orderId = atoi(GetValueField(queryResult, i, 5));
      
      ConvertToEpochTimeString(GetValueField(queryResult, i, 6), eventTime);
      
      if (strncmp(entryExit, "En", 2) == 0)
      {
        //Entry ACK delay
        AddTimeSeriesDelayTimeRecord(time, ackDelay, ENTRY_ACK, GetVenueName(ecn), GetServerName(orderId), eventTime, GetValueField(queryResult, i, 7));
        AddTimeSeriesDelayTimeRecord(time, ackInternalDelay, ENTRY_ACK_INTERNAL, GetVenueName(ecn), GetServerName(orderId), eventTime, GetValueField(queryResult, i, 7));
      }
      else
      {
        //Exit ACK delay
        AddTimeSeriesDelayTimeRecord(time, ackDelay, EXIT_ACK, GetVenueName(ecn), GetServerName(orderId), eventTime, GetValueField(queryResult, i, 7));
        AddTimeSeriesDelayTimeRecord(time, ackInternalDelay, EXIT_ACK_INTERNAL, GetVenueName(ecn), GetServerName(orderId), eventTime, GetValueField(queryResult, i, 7));
      }
    }
  }
  
  // Clear the result
  ClearResult(queryResult);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessFillDelayTime
- Input:      
- Output:     
- Return:     
- Description:  
- Usage:      
****************************************************************************/
int ProcessFillDelayTime(char *time)
{ 
  // Querry String
  char queryString[BUF_LENGTH];
  
  // Reset querry string
  memset(queryString, ' ', BUF_LENGTH);
  
  // Number of records
  int numRecords;
  
  // Build the query string
  sprintf(queryString,"SELECT fill_id, fill_delay, fill_internal_delay, entry_exit, ecn, order_id, time_stamp, routing_inst FROM vw_fill_delays WHERE date = '%s' AND fill_id > %d ORDER BY fill_id", globalDateStringOfDatabase, CurFillID);
  
  // Execute the query
  PGresult * queryResult = ExecuteQuery(connTimeSeries, queryString);
  
  if( queryResult == NULL )
  {
    return ERROR;
  }
  
  // get the number of records
  numRecords = GetNumberOfRow(queryResult);
  
  if(numRecords <= 0)
  {
    ClearResult(queryResult);
    return ERROR;
  }
  else
  { 
    //Get max ID of the new order
    CurFillID = atoi(GetValueField(queryResult, numRecords - 1, 0));
    
    int fillDelay;
    int fillDelayInternal;
    char entryExit[2];
    char ecn[5] = "\0";
    int orderId;
    char eventTime[32] = "\0";
    
    int i;
    for (i = 0; i < numRecords; i++)
    {   
      //Fill delay time
      fillDelay = atoi(GetValueField(queryResult, i, 1));
      fillDelayInternal = atoi(GetValueField(queryResult, i, 2));
      
      // Entry exit
      strncpy(entryExit, GetValueField(queryResult, i, 3), 2);
      
      strcpy(ecn, GetValueField(queryResult, i, 4));
      
      orderId = atoi(GetValueField(queryResult, i, 5));
      ConvertToEpochTimeString(GetValueField(queryResult, i, 6), eventTime);
      
      if (strncmp(entryExit, "En", 2) == 0)
      {
        //Entry Fill delay
        AddTimeSeriesDelayTimeRecord(time, fillDelay, ENTRY_FILL, GetVenueName(ecn), GetServerName(orderId), eventTime, GetValueField(queryResult, i, 7));
        AddTimeSeriesDelayTimeRecord(time, fillDelayInternal, ENTRY_FILL_INTERNAL, GetVenueName(ecn), GetServerName(orderId), eventTime, GetValueField(queryResult, i, 7));
      }
      else
      {
        //Exit Fill delay
        AddTimeSeriesDelayTimeRecord(time, fillDelay, EXIT_FILL, GetVenueName(ecn), GetServerName(orderId), eventTime, GetValueField(queryResult, i, 7));
        AddTimeSeriesDelayTimeRecord(time, fillDelayInternal, EXIT_FILL_INTERNAL, GetVenueName(ecn), GetServerName(orderId), eventTime, GetValueField(queryResult, i, 7));
      }
    }
  }
  
  // Clear the result
  ClearResult(queryResult);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessCancelDelayTime
- Input:      
- Output:     
- Return:     
- Description:  
- Usage:      
****************************************************************************/
int ProcessCancelDelayTime(char *time)
{ 
  // Querry String
  char queryString[BUF_LENGTH];
  
  // Reset querry string
  memset(queryString, ' ', BUF_LENGTH);
  
  // Number of records
  int numRecords;
  
  // Build the query string
  sprintf(queryString,"SELECT id, cancel_delay, entry_exit, time_stamp FROM vw_cancel_delays WHERE date = '%s' AND id > %d ORDER BY id", globalDateStringOfDatabase, CurCancelID);

  // Execute the query
  PGresult * queryResult = ExecuteQuery(connTimeSeries, queryString);
  
  if( queryResult == NULL )
  {
    return ERROR;
  }
  
  // get the number of records
  numRecords = GetNumberOfRow(queryResult);
  
  if(numRecords <= 0)
  {
    ClearResult(queryResult);
    return ERROR;
  }
  else
  { 
    //Get max ID of the new order
    CurCancelID = atoi(GetValueField(queryResult, numRecords - 1, 0));
    
    int cancelDelay;
    char entryExit[2];
    char eventTime[32] = "\0";
    
    int i;
    for (i = 0; i < numRecords; i++)
    {   
      //Fill delay time
      cancelDelay = atoi(GetValueField(queryResult, i, 1));
      
      // Entry exit
      strncpy(entryExit, GetValueField( queryResult, i, 2 ), 2);
      
      ConvertToEpochTimeString(GetValueField( queryResult, i, 3 ), eventTime);
      
      if (strncmp(entryExit, "En", 2) == 0)
      {
        //Entry Fill delay
        AddTimeSeriesDelayTimeRecord(time, cancelDelay, ENTRY_CANCEL, NULL, NULL, eventTime, NULL);
      }
      else
      {
        //Exit Fill delay
        AddTimeSeriesDelayTimeRecord(time, cancelDelay, EXIT_CANCEL, NULL, NULL, eventTime, NULL);
      }
    }
  }
  
  // Clear the result
  ClearResult(queryResult);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessExitNewOrderDelayTime
- Input:      
- Output:     
- Return:     
- Description:  
- Usage:      
****************************************************************************/
int ProcessExitNewOrderDelayTime(char *time)
{ 
  // Querry String
  char queryString[BUF_LENGTH];
  
  // Reset querry string
  memset(queryString, ' ', BUF_LENGTH);
  
  // Number of records
  int numRecords;
  
  // Build the query string
  sprintf(queryString,"SELECT id, delay, ecn, order_id, time_stamp FROM vw_exit_delays WHERE date = '%s' AND id > %d ORDER BY id", globalDateStringOfDatabase, CurExitNewOrderID);

  // Execute the query
  PGresult * queryResult = ExecuteQuery(connTimeSeries, queryString);
  
  if( queryResult == NULL )
  {
    return ERROR;
  }
  
  // get the number of records
  numRecords = GetNumberOfRow(queryResult);
  
  if(numRecords <= 0)
  {
    ClearResult(queryResult);
    return ERROR;
  }
  else
  { 
    //Get max ID of the new order
    CurExitNewOrderID = atoi(GetValueField(queryResult, numRecords - 1, 0));
    
    int i, exitDelay;
    
    char ecn[5] = "\0";
    int orderId;
    char eventTime[32] = "\0";
    
    for (i = 0; i < numRecords; i++)
    { 
      // exit order delay time
      exitDelay = atol(GetValueField(queryResult, i, 1));
      
      strcpy(ecn, GetValueField( queryResult, i, 2 ));
      
      orderId = atoi(GetValueField( queryResult, i, 3 ));
      
      ConvertToEpochTimeString(GetValueField( queryResult, i, 4 ), eventTime);

      if (exitDelay > 1000)
      {
        TraceLog(WARN_LEVEL, "High exit delay: orderId (%d), delay (%d us)\n", orderId, exitDelay);
      }       
      else if (exitDelay < 0)
      {
        TraceLog(WARN_LEVEL, "Invalid exit delay: orderId (%d), delay (%d us), time: %s\n", orderId, exitDelay,
            GetValueField(queryResult, i, 4));
      }
      
      //Exit delay
      AddTimeSeriesDelayTimeRecord(time, exitDelay, EXIT_ORDER, GetVenueName(ecn), GetServerName(orderId), eventTime, NULL);
    }
  }
  
  // Clear the result
  ClearResult(queryResult);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  GetOpenPositionsFromDatabaseWithTime
- Input:      
- Output:     
- Return:     
- Description:  
- Usage:      
****************************************************************************/
int GetOpenPositionsFromDatabaseWithTime(void *_list)
{
  char strQuery[1024];
  memset(strQuery, 0, 1024);
  
  t_OpenPositionList *list = (t_OpenPositionList *) _list;
  
  list->count = 0;
  
  sprintf(strQuery, "SELECT TRIM(symbol), net_quantity, average_price, date, last_update, account, manual_update, changed_shares FROM risk_managements WHERE date='%s' AND (net_quantity <> 0 OR manual_update = 1) ORDER BY last_update;", globalDateStringOfDatabase);
  
  //SELECT TRIM(symbol), net_quantity, average_price, date, last_update FROM risk_managements where date='2012-06-22' AND net_quantity <> 0 ORDER BY last_update;
  
  PGresult * queryResult = ExecuteQuery(connRawData, strQuery);
  if( queryResult == NULL )
  {
    return ERROR;
  }
  
  list->count = GetNumberOfRow(queryResult);
  if (list->count > MAX_OPEN_POSITIONS)
  {
    TraceLog(ERROR_LEVEL, "There are so many open positions (%d > MAX(%d)), open positions will not be included on top of OJ file\n", list->count, MAX_OPEN_POSITIONS);
    list->count = 0;
    return ERROR;
  }
  
  int i;
  for (i = 0; i < list->count; i ++)
  {
    strncpy(list->positions[i].symbol, GetValueField(queryResult, i, 0), SYMBOL_LEN);
    list->positions[i].shares = atoi(GetValueField(queryResult, i, 1));
    list->positions[i].price = atof(GetValueField(queryResult, i, 2));
    strcpy(list->positions[i].date, GetValueField(queryResult, i, 3));
    strcpy(list->positions[i].timeStamp, GetValueField(queryResult, i, 4));
    list->positions[i].tradingAccount = atoi(GetValueField(queryResult, i, 5));
    list->positions[i].isManual = atoi(GetValueField(queryResult, i, 6));
    list->positions[i].changedShares = atoi(GetValueField(queryResult, i, 7));

    if (list->positions[i].shares > 0)
    {
      list->positions[i].side = 'B';
    }
    else
    {
      list->positions[i].side = 'S';
      list->positions[i].shares = -list->positions[i].shares;
    }
  }
  
  ClearResult (queryResult);
  return SUCCESS;
}

/****************************************************************************
- Function name:  GetManualOrderInfoFromDatabase
- Input:      
- Output:     
- Return:     
- Description:  
- Usage:      
****************************************************************************/
int GetManualOrderInfoFromDatabase(char *strDate)
{
  char strQuery[1024];
  memset(strQuery, 0, 1024);
  
  sprintf(strQuery, "SELECT trim(username), log_detail FROM tt_event_log WHERE activity_type=%d AND date='%s' ORDER BY time_stamp ASC", ACTIVITY_TYPE_MANUAL_ORDER, strDate);
  
  PGresult * queryResult = ExecuteQuery(connRawData, strQuery);
  if( queryResult == NULL )
  {
    return ERROR;
  }
  
  int n = GetNumberOfRow(queryResult);

  int i;
  char *str, *sep;
  char username[32];
  int orderID;
  
  for (i = 0; i < n; i ++)
  {
    //Get order id and username, then update to memory
    str = GetValueField(queryResult, i, 0); //username
    if (str == NULL)
    {
      TraceLog(ERROR_LEVEL, "Error while loading manual order information from database: 'username' value in table 'tt_event_log' is empty\n");
      continue;
    }
    strcpy(username, str);
    
    str = GetValueField(queryResult, i, 1); //log_detail
    if (str == NULL)
    {
      TraceLog(ERROR_LEVEL, "Error while loading manual order information from database: 'log_detail' value in table 'tt_event_log' is empty\n");
      continue;
    }
    sep = strchr(str, ',');
    if (sep == NULL)
    {
      TraceLog(ERROR_LEVEL, "Error while loading manual order information from database: 'log_detail' value in table 'tt_event_log' is invalid\n");
      continue;
    }
    sep++;
    orderID = atoi(sep);
    
    strcpy(manualOrderInfo[orderID % MAX_MANUAL_ORDER].username, username);
  }
  
  ClearResult (queryResult);
  return SUCCESS;
}

/****************************************************************************
- Function name:  GetModuleVersion
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int GetModuleVersion(int moduleIndex)
{
  char queryString[BUF_LENGTH];
  memset(queryString, 0, BUF_LENGTH);
  
  sprintf(queryString, "SELECT version, release_date, description FROM release_versions where module_id = %d order by release_date DESC LIMIT 1", moduleIndex);
  
  PGresult * queryResult = ExecuteQuery(connRawData, queryString);
  if (queryResult == NULL)
  {
    return ERROR;
  }

  if (moduleIndex == TT_MODULE)
  {
    
    if (GetNumberOfRow(queryResult) == 0)
    {
      TraceLog(ERROR_LEVEL, "table 'release_versions' contains no record\n");
      ClearResult (queryResult);
      return ERROR;
    }
    
    strcpy(moduleVersionMgmt.ttVersion.version, GetValueField(queryResult, 0, 0));
    strcpy(moduleVersionMgmt.ttVersion.date, GetValueField(queryResult, 0, 1));
    strcpy(moduleVersionMgmt.ttVersion.description, GetValueField(queryResult, 0, 2));
  }
  
  ClearResult(queryResult);

  return SUCCESS;
}

/****************************************************************************
- Function name:  IsSymbolTradedSinceHaltedDate
- Input:      
- Output:     
- Return:     
- Description:  
- Usage:      
****************************************************************************/
int IsSymbolTradedSinceHaltedDate(char *symbol, char *haltedDate)
{
  // Create query string
  char strQuery[1024];
  memset(strQuery, 0, 1024);
  sprintf(strQuery, "SELECT MAX(id) FROM new_orders WHERE symbol = '%.8s' AND date > '%s'", symbol, haltedDate);
  
  // Get current time
  time_t now = (time_t)hbitime_seconds();
  
  struct tm *currentlTime = (struct tm *)localtime(&now);
  
  if (currentlTime == NULL)
  {
    return ERROR;
  }
  
  //query database
  PGresult *queryResult = NULL;
  int totalResults = 0;
  int maxID;
  
  //query for current month
  pthread_mutex_lock(&connSaveSettings_Mutex);
  queryResult = ExecuteQuery(connSaveSettings, strQuery);
  pthread_mutex_unlock(&connSaveSettings_Mutex);

  if (queryResult == NULL)
  {
    return ERROR;
  }
  
  totalResults = GetNumberOfRow(queryResult);
  
  if (totalResults == 1)
  {
    maxID = atoi(GetValueField(queryResult, 0, 0));
    // TraceLog(DEBUG_LEVEL, "maxID=%d, strMaxID='%s'\n", maxID, GetValueField(queryResult, 0, 0));
    if (maxID > 0)
    {
      ClearResult(queryResult);
      return YES;
    }
  }
  
  if (currentlTime->tm_mday < 15)
  {
    if (connPrevMonthDatabase == NULL)
    {
      int year, month;
      char connStr[512];
      
      //query for previous month
      if (currentlTime->tm_mon > 0)
      {
        //1->11: February --> December
        month = currentlTime->tm_mon;
        year = currentlTime->tm_year + 1900;
      }
      else
      {
        //0: January
        month = 12;
        year = currentlTime->tm_year + 1900 - 1;
      }
      
      // Create connection for time series
      if (get_monthly_db_connection_string("eaa", year, month, connStr, 512) == NULL_POINTER) {
        TraceLog(ERROR_LEVEL, "Cannot get database configuration\n" );
        return ERROR;
      }
      
      connPrevMonthDatabase = CreateConnection(connStr, connPrevMonthDatabase);
      if (connPrevMonthDatabase == NULL)
      {
        TraceLog(ERROR_LEVEL, "Cannot open database connection for previous database\nConnection String: '%s'\n", connStr);
        return ERROR;
      }
    }
    
    queryResult = ExecuteQuery(connPrevMonthDatabase, strQuery);
    if (queryResult == NULL)
    {
      return ERROR;
    }
    
    totalResults = GetNumberOfRow(queryResult);
    
    if (totalResults == 1)
    {
      maxID = atoi(GetValueField(queryResult, 0, 0));
      //TraceLog(DEBUG_LEVEL, "maxID=%d strMaxID='%s'\n", maxID, GetValueField(queryResult, 0, 0));
      if (maxID > 0)
      {
        ClearResult(queryResult);
        return YES;
      }
    }
    else
    {
      ClearResult(queryResult);
      return ERROR;
    }
  }
  
  ClearResult(queryResult);
  return NO;
}

/****************************************************************************
- Function name:  GetVenueName
- Input:      
- Output:     
- Return:     
- Description:  
- Usage:      
****************************************************************************/
char *GetVenueName(char *ecn)
{
  if (strncmp(ecn, "ARCA", 4) == 0)
  {
    return "arca";
    //  return "afix";
  }
  else if (strncmp(ecn, "OUCH", 4) == 0)
  {
    return "ouch";
  }
  else if (strncmp(ecn, "RASH", 4) == 0)
  {
    return "rash";
  }
  else if (strncmp(ecn, "BATZ", 4) == 0)
  {
    return "batsz";
  }
  else if (strncmp(ecn, "NYSE", 4) == 0)
  {
    return "nyse";
  }
  else if (strncmp(ecn, "EDGX", 4) == 0)
  {
    return "edgx";
  }
  else if (strncmp(ecn, "EDGA", 4) == 0)
  {
    return "edga";
  }
  else if (strncmp(ecn, "OUBX", 4) == 0)
  {
    return "oubx";
  }
  else if (strncmp(ecn, "BATY", 4) == 0)
  {
    return "byx";
  }
  else if (strncmp(ecn, "PSX", 3) == 0)
  {
    return "psx";
  }
  else
  {
    TraceLog(ERROR_LEVEL, "TIMESERIES: Invalid value, ecn = %s\n", ecn);
    return NULL;
  }
}

/****************************************************************************
- Function name:  GetServerName
- Input:      
- Output:     
- Return:     
- Description:  
- Usage:      
****************************************************************************/
char *GetServerName(int orderId)
{
  if (orderId < 500000)
  {
    return "carblink5";
  }
  else if (orderId < 1000000)
  {
    return "carblink2";
  }
  else if (orderId < 2000000)
  {
    return tradeServersInfo.config[0].description;
  }
  else if (orderId < 3000000)
  {
    return tradeServersInfo.config[1].description;
  }
  else if (orderId < 4000000)
  {
    return tradeServersInfo.config[2].description;
  }
  else if (orderId < 5000000)
  {
    return tradeServersInfo.config[3].description;
  }
  else if (orderId < 6000000)
  {
    return tradeServersInfo.config[4].description;
  }
  else if (orderId < 7000000)
  {
    return tradeServersInfo.config[5].description;
  }
  else if (orderId < 8000000)
  {
    return tradeServersInfo.config[6].description;
  }
  else if (orderId < 9000000)
  {
    return tradeServersInfo.config[7].description;
  }
  else if (orderId < 10000000)
  {
    return tradeServersInfo.config[8].description;
  }
  else
  {
    TraceLog(ERROR_LEVEL, "GetServerName: Invalid orderId: %d\n", orderId);
    return NULL;
  }
}
