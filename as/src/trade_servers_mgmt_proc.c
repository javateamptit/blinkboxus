/*****************************************************************************
**  Project:    Equity Arbitrage Application
**  Filename:   trade_servers_mgmt_proc.c
**  Description:  This file contains function definitions that were declared
          in trade_servers_mgmt_proc.h  
**  Author:     Luan Vo-Kinh
**  First created:  13-Sep-2007
**  Last updated: -----------
*****************************************************************************/

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include <pthread.h>

#include "configuration.h"
#include "socket_util.h"
#include "raw_data_mgmt.h"
#include "output_log_mgmt.h"
#include "trade_servers_proc.h"
#include "trade_servers_mgmt_proc.h"

/****************************************************************************
** Global variables definition
****************************************************************************/

/****************************************************************************
** Function declarations
****************************************************************************/
/****************************************************************************
- Function name:  AddDataBlockToCollection
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int AddDataBlockToCollection(const void *dataBlock, void *orderRawDataMgmt)
{
  t_RawDataMgmt *workingOrderMgmt = (t_RawDataMgmt *)orderRawDataMgmt;
  int numDataBlocks = 0, countProccessed = 0;

  pthread_mutex_lock(&workingOrderMgmt->updateMutex);
  
  if (workingOrderMgmt->tsId != MANUAL_ORDER)
  {
    // TS raw data
    countProccessed = RawDataProcessedMgmt.tsDataBlock[workingOrderMgmt->tsId][workingOrderMgmt->orderIndex];
    numDataBlocks = workingOrderMgmt->countRawDataBlock - countProccessed;
  }
  else
  {
    // PM raw data
    countProccessed = RawDataProcessedMgmt.pmDataBlock[workingOrderMgmt->orderIndex];
    numDataBlocks = workingOrderMgmt->countRawDataBlock - countProccessed;
  }
  
  if (numDataBlocks >= MAX_RAW_DATA_ENTRIES - 1)
  {
    TraceLog(ERROR_LEVEL, "%s, data block collection is full: countRawDataBlock = %d, countRawDataProcessed = %d\n", tradeServersInfo.config[workingOrderMgmt->tsId].description, workingOrderMgmt->countRawDataBlock, countProccessed);
    
    pthread_mutex_unlock(&workingOrderMgmt->updateMutex);
    
    exit(0);
  }
      
  // We will store new data block at last updated index + 1
  workingOrderMgmt->lastUpdatedIndex++;
  
  // Add new data block to collection
  workingOrderMgmt->dataBlockCollection[workingOrderMgmt->lastUpdatedIndex % MAX_RAW_DATA_ENTRIES] = *((t_DataBlock *)dataBlock);
  
  // Increase total of data blocks
  workingOrderMgmt->countRawDataBlock++;

  pthread_mutex_unlock(&workingOrderMgmt->updateMutex);
  
  return 0;
}

/****************************************************************************
- Function name:  SaveDataBlockToFile
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SaveDataBlockToFile(void *orderRawDataMgmt)
{
  t_RawDataMgmt *workingOrderMgmt = (t_RawDataMgmt *)orderRawDataMgmt;

  // Save new data blocks at last updated index
  if (fwrite(&workingOrderMgmt->dataBlockCollection[workingOrderMgmt->lastUpdatedIndex % MAX_RAW_DATA_ENTRIES], sizeof(t_DataBlock), 1, workingOrderMgmt->fileDesc) != 1)
  {
    TraceLog(ERROR_LEVEL, "Cannot save block data to file\n");

    return ERROR;
  }
  
  // Flush all data from buffer to file
  fflush(workingOrderMgmt->fileDesc);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  MappingTradeOutputLog
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int MappingTradeOutputLog(unsigned char srcBuff[MAX_OUTPUT_LOG_BIN_LEN], char destBuff[MAX_OUTPUT_LOG_TEXT_LEN])
{
  // Get log type
  int logType = srcBuff[0];
  int crossId = GetIntNumber(&srcBuff[1]);
  long timestamp = *((long*)&srcBuff[5]); //nano seconds from Epoch
  int secPart = timestamp/1000000000;
  int nSecPart = timestamp%1000000000;
  
  if (crossId < 0)
  {
    TraceLog(ERROR_LEVEL, "MappingTradeOutputLog: Invalid trade output log, logType = %d, crossId = %d\n", logType, crossId);
  }

  // Convert binary to text
  sprintf(destBuff, "%d %d %d.%09d", logType, crossId, secPart, nSecPart);
  int len = strlen(destBuff);

  int intVar1, intVar2, intVar3, intVar4;
  double doubleVar1, doubleVar2;
  long longVar1;
  char symbol[SYMBOL_LEN];
  char priceFormat[20];

  switch (logType)
  {
    case 0:
      // Message body is null
      return len;
    case 1:
      // Message body is null
      return len;
    case 2:
      strncpy(symbol, (char *) &srcBuff[13], SYMBOL_LEN);
      intVar1 = srcBuff[21];  //server role
      intVar2 = srcBuff[22];  //ecnLaunchID
      intVar3 = srcBuff[23];  //newOrderSide
      intVar4 = srcBuff[24];  //ecnIndicator
      longVar1 = GetLongNumber((char *)&srcBuff[25]); // Timestamp

      sprintf(&destBuff[len], " %.8s %d %d %d %d 0 %ld %d", symbol, intVar1, intVar2, intVar3, intVar4, longVar1, srcBuff[33]);
      len = strlen(destBuff);
      return len;
    case 3:
      intVar1 = srcBuff[13];
      intVar2 = GetIntNumber(&srcBuff[14]);
      intVar3 = srcBuff[18];
      doubleVar1 = GetDoubleNumber(&srcBuff[19]);

      sprintf(&destBuff[len], " %d %d %d %.02lf", intVar1, intVar2, intVar3, doubleVar1);
      len = strlen(destBuff);
      return len;
    case 4:
      // Message body is null
      return len;
    case 5:
      intVar1 = GetIntNumber(&srcBuff[13]);
      strncpy(symbol, (char *) &srcBuff[17], SYMBOL_LEN);
      doubleVar1 = GetDoubleNumber(&srcBuff[25]);
      intVar2 = srcBuff[33];

      sprintf(&destBuff[len], " %d %.8s %.02lf %d", intVar1, symbol, doubleVar1, intVar2);
      len = strlen(destBuff);
      return len;
    case 6:
      return len;
    case 7:
      return len;
    case 8:
      return len;
    case 9:
      return len;
    case 10:
      intVar1 = srcBuff[13];
      intVar2 = GetIntNumber(&srcBuff[14]);
      strncpy(symbol, (char *) &srcBuff[18], SYMBOL_LEN);
      doubleVar1 = GetDoubleNumber(&srcBuff[26]);
      intVar3 = srcBuff[34];
      
      if (fge(doubleVar1, 1.00))
      {
        // To round to 2 digits
        if (intVar1 == 0 || intVar1 == 1) // BUY SIDE: "B", "Bt"
        {
          sprintf(priceFormat, "%.02lf", (int)(doubleVar1 * 100.00 + 0.500001) * 0.01);
        }
        else
        {
          sprintf(priceFormat, "%.02lf", (int)(doubleVar1 * 100.00 + 0.000001) * 0.01);
        }
      }
      else
      {
        // To round to 4 digits
        sprintf(priceFormat, "%.04lf", (int)(doubleVar1 * 10000.00 + 0.000001) * 0.0001);
      }

      sprintf(&destBuff[len], " %d %d %.8s %s %d", intVar1, intVar2, symbol, priceFormat, intVar3);
      len = strlen(destBuff);
      return len;
    case 11:
      return len;
    case 12:
      intVar1 = srcBuff[13];

      sprintf(&destBuff[len], " %d", intVar1);
      len = strlen(destBuff);
      return len;
    case 13:
      strncpy(symbol, (char *) &srcBuff[13], SYMBOL_LEN);     
      doubleVar1 = GetDoubleNumber(&srcBuff[21]);
      doubleVar2 = GetDoubleNumber(&srcBuff[29]);
      intVar1 = GetIntNumber(&srcBuff[37]);
      intVar2 = GetIntNumber(&srcBuff[41]);

      sprintf(&destBuff[len], " %.8s %.02lf %.02lf %d %d", symbol, doubleVar1, doubleVar2, intVar1, intVar2);
      len = strlen(destBuff);
      return len;
    case 14:
      intVar1 = srcBuff[13];      
      doubleVar1 = GetDoubleNumber(&srcBuff[14]);
      strncpy(symbol, (char *) &srcBuff[22], SYMBOL_LEN);

      sprintf(&destBuff[len], " %d %.02lf %.8s", intVar1, doubleVar1, symbol);
      len = strlen(destBuff);
      return len;
    case 15:
      intVar1 = GetIntNumber(&srcBuff[13]);
      intVar2 = srcBuff[17];
      intVar3 = GetIntNumber(&srcBuff[18]);
      strncpy(symbol, (char *) &srcBuff[22], SYMBOL_LEN); 
      intVar4 = srcBuff[30];
      doubleVar1 = GetDoubleNumber(&srcBuff[31]);

      sprintf(&destBuff[len], " %d %d %d %.8s %d %.02lf", intVar1, intVar2, intVar3, symbol, intVar4, doubleVar1);
      len = strlen(destBuff);
      return len;
    default:
      TraceLog(ERROR_LEVEL, "MappingTradeOutputLog: Invalid trade output log, logType = %d, crossId = %d\n", logType, crossId);
      return ERROR;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  AddOutputLogToCollection
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int AddOutputLogToCollection(const char *buffer, void *logMgmt)
{
  t_OutputLogMgmt *outputLogMgmt = (t_OutputLogMgmt *)logMgmt;

  pthread_mutex_lock(&outputLogMgmt->updateMutex);
      
  // We will store new data block at last updated index + 1
  outputLogMgmt->lastUpdatedIndex++;
  
  // Add new output log to collection
  strncpy(outputLogMgmt->tradeLogCollection[outputLogMgmt->lastUpdatedIndex % MAX_OUTPUT_LOG_ENTRIES], buffer, MAX_OUTPUT_LOG_TEXT_LEN);
  
  // Increase total of output log
  outputLogMgmt->countOutputLog++;

  pthread_mutex_unlock(&outputLogMgmt->updateMutex);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SaveOutputLogToFile
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SaveOutputLogToFile(void *logMgmt)
{
  t_OutputLogMgmt *outputLogMgmt = (t_OutputLogMgmt *)logMgmt;

  // Save new data blocks at last updated index
  fprintf(outputLogMgmt->fileDesc, "%s\n", outputLogMgmt->tradeLogCollection[outputLogMgmt->lastUpdatedIndex % MAX_OUTPUT_LOG_ENTRIES]);
  
  // Flush all data from buffer to file
  fflush(outputLogMgmt->fileDesc);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  Connect2TradeServer
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int Connect2TradeServer(int tsIndex)
{
  if (tradeServersInfo.connection[tsIndex].status == CONNECT)
  {
    TraceLog(WARN_LEVEL, "AS has been connect to %s\n", tradeServersInfo.config[tsIndex].description);

    return 0;
  }

  // Thread id
  pthread_t threadTradeServer_id;

  // Create thread
  pthread_create (&threadTradeServer_id, 
          NULL, 
          (void*) &ProcessTradeServer, 
          (void*) &tsIndex);

  // Waiting for create thread completed (delay in 1 seconds) 
  sleep(1);

  return 0;
}

/****************************************************************************
- Function name:  Connect2AllTradeServer
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int Connect2AllTradeServer(int ttIndex)
{
  int tsIndex;

  for (tsIndex = 0; tsIndex < tradeServersInfo.maxConnections; tsIndex++)
  {
    // Check connection between AS and TS
    if (tradeServersInfo.connection[tsIndex].status == DISCONNECT)
    {
      // Update flag to connect all
      tradeServersInfo.isConnectAll[tsIndex] = YES;
      tradeServersInfo.ttIndexPerformedConnectAll[tsIndex] = ttIndex;
      
      // Call function to connect to all TS
      Connect2TradeServer(tsIndex);     
    }
    else
    {
      // Process connect all ECN Books and ECN Orders
      ProcessTSConnectAllConnection(tsIndex, ttIndex);

      tradeServersInfo.isConnectAll[tsIndex] = NO;
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  DisconnectTradeServer
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int Disconnect2TradeServer(int tsIndex)
{ 
  // Disconnect to Trade Server
  CloseConnection(&tradeServersInfo.connection[tsIndex]);

  return SUCCESS;
}

/****************************************************************************
- Function name:  Disconnect2AllTradeServer
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int Disconnect2AllTradeServer(void)
{
  int tsIndex;

  // Process disconnect all ECN Books and ECN Orders
  ProcessTSDisconnectAllBookAndOrder();
  
  // Waiting for disconnect all Book and Order, delay in second(s)
  sleep(1);

  for (tsIndex = 0; tsIndex < tradeServersInfo.maxConnections; tsIndex++)
  {
    // Check connection between AS and TS
    if (tradeServersInfo.connection[tsIndex].status != DISCONNECT)
    {
      // Call function to disconnect to all TS
      Disconnect2TradeServer(tsIndex);
    }
  }

  return SUCCESS;
}

/***************************************************************************/
