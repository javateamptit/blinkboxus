/*****************************************************************************
**  Project:    Equity Arbitrage Application
**  Filename:   configuration.c
**  Description:  This file contains function definitions that were declared
          in configuration.h  
**  Author:     Luan Vo-Kinh
**  First created:  13-Sep-2007
**  Last updated: -----------
*****************************************************************************/

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include <pthread.h>
#include <errno.h>
#include "utility.h"
#include "raw_data_mgmt.h"
#include "output_log_mgmt.h"
#include "order_mgmt_proc.h"
#include "arca_direct_data.h"
#include "nasdaq_ouch_data.h"
#include "nasdaq_rash_data.h"
#include "bats_boe_data.h"
#include "nasdaq_rash_proc.h"
#include "arca_direct_proc.h"
#include "database_util.h"
#include "trade_servers_proc.h"
#include "daedalus_proc.h"
#include "configuration.h"
#include "hbitime.h"

/****************************************************************************
** Global variables definition
****************************************************************************/
// Trade Servers information
t_TradeServerInfo tradeServersInfo;

// Trader Tools information
t_TraderToolInfo traderToolsInfo;

// PM information
t_PMInfo pmInfo;

t_ASSetting asSetting;
t_PMSetting pmSetting;

t_SessionIdMgmt SessionIdMgmt;

// Ignore symbol list
t_IgnoreStockList IgnoreStockList;
t_IgnoreStockList pmIgnoreStockList;

// Ignore symbol ranges list
t_IgnoreStockRangesList IgnoreStockRangesList;
// t_IgnoreStockRangesList pmIgnoreStockRangesList;

t_OrderStatus orderStatusMgmt[AS_MAX_ORDER_CONNECTIONS];

int clientOrderID;

pthread_mutex_t nextOrderID_Mutex;

t_ExchangeInfo ExchangeStatusList[MAX_TRADE_SERVER_CONNECTIONS][MAX_EXCHANGE];

t_PMExchangeInfo PMExchangeStatusList[MAX_EXCHANGE];

int currentMarketTimeType; //-->PRE-MARKET, INTRADAY, POST-MARKET
char RashRouteID[3][6];

//The maximum number of cancel messages will be sent to TT
//This will be read from as global configuration file, default value is 2
int PMtoTTCancelLimit = 2;

t_TradingAccount TradingAccount;

t_FeeConf FeeConf;

t_AlertConf AlertConf;
int TradeTimeAlertConfig[NUMBER_OF_SECONDS_PER_DAY]; // 24h x 3600s

t_ModuleVersionMgmt moduleVersionMgmt;
extern t_VolatileMgmt volatileMgmt;

t_ShortabilitySchedule ShortabilitySchedule;
t_SSLMgmt SSLMgmt;

t_LdapInfo LdapInfo;

int TimeToEnableBatszSetting;
int TimeToDisableBatszSetting;
int TimeToEnableBatszTrading;

int TimeToEnableBatsySetting;
int TimeToDisableBatsySetting;
int TimeToEnableBatsyTrading;

int TimeToEnableEdgxSetting;
int TimeToDisableEdgxSetting;
int TimeToEnableEdgxTrading;

int TimeToEnableEdgaSetting;
int TimeToDisableEdgaSetting;
int TimeToEnableEdgaTrading;

int TimeToDisableNyseSetting;
int TimeToDisableAmexSetting;

int TimeToEnableBxSetting;

int TimeToEnablePsxSetting;

int TimeToDisableTrading[TS_MAX_ORDER_CONNECTIONS];
/****************************************************************************
** Function declarations
****************************************************************************/
/****************************************************************************
- Function name:  InitializeTradeServersInfo
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int InitializeTradeServersInfo(void)
{
  // Maximum connections
  tradeServersInfo.maxConnections = -1;

  int i, j;  

  for (i = 0; i < MAX_TRADE_SERVER_CONNECTIONS; i++)
  {
    // Trade Server configuration
    tradeServersInfo.config[i].tsId = -1;
    memset(tradeServersInfo.config[i].ip, 0, MAX_LINE_LEN);
    tradeServersInfo.config[i].port = -1;
    memset(tradeServersInfo.config[i].description, 0, MAX_LINE_LEN);
    
    // Trade Server setting
    memset(&tradeServersInfo.tsSetting[i], NO, sizeof(t_TSSetting));
    
    // Trade Server connection status
    tradeServersInfo.connection[i].status = DISCONNECT;
    tradeServersInfo.connection[i].socket = -1;
    pthread_mutex_init(&tradeServersInfo.connection[i].socketMutex, NULL);

    // ECN Books configuration
    memset(&tradeServersInfo.ECNBooksConf[i], 0, sizeof(t_ECNBooksConf));

    // ECN Orders configuration
    memset(&tradeServersInfo.ECNOrdersConf[i], 0, sizeof(t_ECNOrderConf) * TS_MAX_ORDER_CONNECTIONS);
    
    // ECN global configuration
    memset(&tradeServersInfo.globalConf[i], 0, sizeof(t_GlobalConf));
    tradeServersInfo.globalConf[i].serverRole = -1;
    
    // Split Symbol configuration
    memset(&tradeServersInfo.splitConf[i], 0, sizeof(t_SplitSymbolConf));

    // Trade Server current status
    //memset(&tradeServersInfo.currentStatus[i], 0, sizeof(t_CurrentStatus));

    tradeServersInfo.currentStatus[i].buyingPower = 0.00;
    tradeServersInfo.currentStatus[i].numStuck = 0;
    tradeServersInfo.currentStatus[i].numLoser = 0;

    for(j = 0; j < TS_MAX_BOOK_CONNECTIONS; j++)
    {
      tradeServersInfo.currentStatus[i].tsMdcStatus[j] = DISCONNECT;
      tradeServersInfo.preStatus[i].tsMdcStatus[j] = DISCONNECT;
    }
    
    for(j = 0; j < TS_MAX_ORDER_CONNECTIONS; j++)
    {
      tradeServersInfo.currentStatus[i].tsOecStatus[j] = DISCONNECT;
      tradeServersInfo.preStatus[i].tsOecStatus[j] = DISCONNECT;
      
      tradeServersInfo.currentStatus[i].tsTradingStatus[j] = DISABLE;
      tradeServersInfo.preStatus[i].tsTradingStatus[j] = DISABLE;
    }
    
    //previous currentStatus
    tradeServersInfo.preStatus[i].buyingPower = 0.00;
    tradeServersInfo.preStatus[i].numStuck = 0;
    tradeServersInfo.preStatus[i].numLoser = 0;

    // Order place summary
    memset(&tradeServersInfo.orderPlaceSummary[i], 0, sizeof(t_OrderPlaceSummary));   

    // Check connect all
    tradeServersInfo.isConnectAll[i] = NO;
    tradeServersInfo.ttIndexPerformedConnectAll[i] = -1;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  InitializePMInfo
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int InitializePMInfo(void)
{
  // PM configuration
  memset(pmInfo.config.ip, 0, MAX_LINE_LEN);
  pmInfo.config.port = -1;
  
  // PM connection status
  pmInfo.connection.status = DISCONNECT;
  pmInfo.connection.socket = -1;
  pthread_mutex_init(&pmInfo.connection.socketMutex, NULL);

  // ECN Books configuration
  memset(&pmInfo.ECNBooksConf, 0, sizeof(t_ECNBooksConf));

  // ECN Orders configuration
  memset(&pmInfo.ECNOrdersConf, 0, sizeof(t_ECNOrderConf) * PM_MAX_ORDER_CONNECTIONS);
  
  // ECN global configuration
  memset(&pmInfo.globalConf, 0, sizeof(t_PMGlobalConf));

  pmInfo.currentStatus.tradingStatus = DISCONNECT;

  int i;
  for(i = 0; i < PM_MAX_BOOK_CONNECTIONS; i++)
  {
    pmInfo.currentStatus.pmMdcStatus[i] = DISCONNECT;
  }
  
  for(i = 0; i < PM_MAX_ORDER_CONNECTIONS; i++)
  {
    pmInfo.currentStatus.pmOecStatus[i] = DISCONNECT;
  }
  
  // PM setting
  memset(&pmSetting, NO, sizeof(t_PMSetting));

  return SUCCESS;
}

/****************************************************************************
- Function name:  InitializeSessionIdManagement
****************************************************************************/
int InitializeSessionIdManagement()
{
  int oecIndex;
  for (oecIndex = 0; oecIndex < AS_MAX_ORDER_CONNECTIONS; oecIndex++)
  {
    sprintf(SessionIdMgmt.asSessionId[oecIndex], "AS%04d", oecIndex + 1);
  }
  
  for (oecIndex = 0; oecIndex < PM_MAX_ORDER_CONNECTIONS; oecIndex++)
  {
    sprintf(SessionIdMgmt.pmSessionId[oecIndex], "PM%04d", oecIndex + 1);
  }
  
  int tsIndex;
  for (tsIndex = 0; tsIndex < MAX_TRADE_SERVER_CONNECTIONS; tsIndex++)
  {
    for (oecIndex = 0; oecIndex < TS_MAX_ORDER_CONNECTIONS; oecIndex++)
    {
      sprintf(SessionIdMgmt.tsSessionId[tsIndex][oecIndex], "TS%02d%02d", tsIndex + 1, oecIndex + 1);
    }
  }
  return SUCCESS;
}

/****************************************************************************
- Function name:  InitializeIgnoreStockList
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int InitializeIgnoreStockList()
{
  IgnoreStockList.countStock = 0;
  memset(IgnoreStockList.stockList, 0, MAX_STOCK_SYMBOL * SYMBOL_LEN);

  pmIgnoreStockList.countStock = 0;
  memset(pmIgnoreStockList.stockList, 0, MAX_STOCK_SYMBOL * SYMBOL_LEN);

  return SUCCESS;
}

/****************************************************************************
- Function name:  InitializeIgnoreStockRangesList
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int InitializeIgnoreStockRangesList()
{
  IgnoreStockRangesList.countStock = 0;
  memset(IgnoreStockRangesList.stockList, 0, MAX_STOCK_SYMBOL * SYMBOL_LEN);

  return SUCCESS;
}
/****************************************************************************
- Function name:  InitializeTraderToolsInfo
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int InitializeTraderToolsInfo(void)
{
  traderToolsInfo.config.port = -1;
  traderToolsInfo.config.maxConnections = -1;

  int i;

  for (i = 0; i < MAX_TRADER_TOOL_CONNECTIONS; i++)
  {   
    traderToolsInfo.connection[i].index = i;
    traderToolsInfo.connection[i].status = DISCONNECT;
    traderToolsInfo.connection[i].socket = -1;
    traderToolsInfo.connection[i].statusUpdate = -1;
    traderToolsInfo.connection[i].currentState = OFFLINE;
    traderToolsInfo.connection[i].authenticationMethod = UNDEFINE_AUTHENTICATION_METHOD;
    traderToolsInfo.connection[i].clientType = CLIENT_TOOL_UNDEFINED;
    memset(&traderToolsInfo.connection[i].userName, 0, MAX_LINE_LEN);
    memset(&traderToolsInfo.connection[i].password, 0, MAX_LINE_LEN);
    pthread_mutex_init(&traderToolsInfo.connection[i].socketMutex, NULL);
  }

  // ECN Orders configuration
  // Init_ARCA_FIX_DataStructure();
  Initialize_NASDAQ_RASH();

  // ECN Orders connection status
  memset(&traderToolsInfo.orderConnection, 0, sizeof(t_ConnectionStatus) * AS_MAX_ORDER_CONNECTIONS); 

  // ECN global configuration
  memset(&traderToolsInfo.globalConf, 0, sizeof(t_ASGlobalConf));

  // Trade Server current status
  memset(&traderToolsInfo.currentStatus, 0, sizeof(t_ASCurrentStatus));

  pthread_mutex_init(&nextOrderID_Mutex, NULL);
  
  // AS setting
  memset(&asSetting, NO, sizeof(t_ASSetting));

  return 0;
}

/****************************************************************************
- Function name:  LoadTradeServersConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int LoadTradeServersConf(void)
{
  int result, tsIndex;

  // For testing
  result = GetTradeServersConfig( &tradeServersInfo );

  if (result == ERROR)
  {
    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Max TS connection = %d\n", tradeServersInfo.maxConnections);

  for (tsIndex = 0; tsIndex < tradeServersInfo.maxConnections; tsIndex ++)
  {
    TraceLog(DEBUG_LEVEL, "tsId = %d\n", tradeServersInfo.config[tsIndex].tsId);
    TraceLog(DEBUG_LEVEL, "ip = %s\n", tradeServersInfo.config[tsIndex].ip);
    TraceLog(DEBUG_LEVEL, "port = %d\n", tradeServersInfo.config[tsIndex].port);
    TraceLog(DEBUG_LEVEL, "description = %s\n", tradeServersInfo.config[tsIndex].description);
  }
  
  // Set rash port for manual orders should default to PM
  DisableRASHOnAS();
  EnableRASHOnPM();
  
  // Add code here to update AS and PM setting
  GetOtherSetting();
  
  //--------------------------------------------------------------------------
  //BLINK-2440: Schedule BATS enable/disable as ECN setting
  // + Disable BATSZ ECN Setting at start of day
  //BLINK-2599: Edgx mdc startup time
  // + Replicate BATS ECN setting for EDGX's
  //--------------------------------------------------------------------------
  TraceLog(DEBUG_LEVEL, "Auto disable ECN Settings of BATSZ and EDGX on all Trade Servers\n");
  for (tsIndex = 0; tsIndex < MAX_TRADE_SERVER_CONNECTIONS; tsIndex++)
  {
    tradeServersInfo.tsSetting[tsIndex].bookEnable[TS_BATSZ_BOOK_INDEX] = NO;
    tradeServersInfo.tsSetting[tsIndex].orderEnable[TS_BATSZ_BOE_INDEX] = NO;
    
    tradeServersInfo.tsSetting[tsIndex].bookEnable[TS_BYX_BOOK_INDEX] = NO;
    tradeServersInfo.tsSetting[tsIndex].orderEnable[TS_BYX_BOE_INDEX] = NO;
    
    tradeServersInfo.tsSetting[tsIndex].bookEnable[TS_EDGX_BOOK_INDEX] = NO;
    tradeServersInfo.tsSetting[tsIndex].orderEnable[TS_EDGX_DIRECT_INDEX] = NO;
    
    tradeServersInfo.tsSetting[tsIndex].bookEnable[TS_EDGA_BOOK_INDEX] = NO;
    tradeServersInfo.tsSetting[tsIndex].orderEnable[TS_EDGA_DIRECT_INDEX] = NO;
    
    tradeServersInfo.tsSetting[tsIndex].bookEnable[TS_NDBX_BOOK_INDEX] = NO;
    tradeServersInfo.tsSetting[tsIndex].orderEnable[TS_NDAQ_OUBX_INDEX] = NO;
    
    tradeServersInfo.tsSetting[tsIndex].bookEnable[TS_PSX_BOOK_INDEX] = NO;
    tradeServersInfo.tsSetting[tsIndex].orderEnable[TS_PSX_OUCH_INDEX] = NO;

    //Save Trade Server setting to ensure database is updated to
    //avoid ECN Setting of BATSZ and EDGX are enabled when they are loaded again
    SaveTradeServerSetting(tsIndex);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  FindTradeServerIndex
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int FindTradeServerIndex(int tsId)
{
  int i;

  for (i = 0; i < tradeServersInfo.maxConnections; i++)
  {
    if (tradeServersInfo.config[i].tsId == tsId)
    {
      return i;
    }
  }

  return -1;
}

/****************************************************************************
- Function name:  FindTradeServerIndexFromDescription
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int FindTradeServerIndexFromDescription(char description[MAX_LINE_LEN])
{
  int i;

  // Find Trader Server
  for (i = 0; i < tradeServersInfo.maxConnections; i++)
  {
    if (strcmp(tradeServersInfo.config[i].description, description) == 0)
    {
      return i;
    }
  }

  return -1;
}

/****************************************************************************
- Function name:  SaveTradeServersConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SaveTradeServersConf(int tsIndex)
{
  TraceLog(DEBUG_LEVEL, "tsId = %d\n", tradeServersInfo.config[tsIndex].tsId);
  TraceLog(DEBUG_LEVEL, "ip = %s\n", tradeServersInfo.config[tsIndex].ip);
  TraceLog(DEBUG_LEVEL, "port = %d\n", tradeServersInfo.config[tsIndex].port);
  TraceLog(DEBUG_LEVEL, "description = %s\n", tradeServersInfo.config[tsIndex].description);

  // Add code here to update Trade Server configuration
  UpdateTradeServerConfig((t_TradeServerConf *) &tradeServersInfo.config[tsIndex] );

  return 0;
}

/****************************************************************************
- Function name:  LoadTraderToolsConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int LoadTraderToolsConf()
{
  // Get full raw data path
  char fullPath[MAX_PATH_LEN] = "\0";
  
  if (GetFullConfigPath(FILE_NAME_OF_TRADER_TOOL_CONF, CONF_MISC, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Have some problem in get full raw data path operation (%s)\n", FILE_NAME_OF_TRADER_TOOL_CONF);

    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Load Trader Tool config: %s\n", fullPath);

  //Try to open Trader Tool configuration file
  //Please be aware of the requirements for the configuration file here
  FILE *fileDesc = fopen(fullPath, "r");
  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Cannot open file '%s' to read or we don't have permission to create\n", fullPath);

    return ERROR;
  }
  else
  {
    //Buffer to store each line in the configuration file
    char line[MAX_PATH_LEN];

    //Number of lines read from the configuration file
    int  count = 0;

    //Clear the buffer pointed to by line
    memset(line, 0, MAX_PATH_LEN);

    //Get each line from the configuration file
    while (fgets(line, MAX_PATH_LEN, fileDesc) != NULL) 
    {
      if ((line[0] != '#') && (line[0] > 32))
      {
        count ++;

        if (count == 1)
        {
          traderToolsInfo.config.port = atoi(line);
        }
        else if (count == 2)
        {
          traderToolsInfo.config.maxConnections = atoi(line);
        }
      }

      //Clear the buffer pointed to by line
      memset(line, 0, MAX_PATH_LEN);
    }

    //Close the file
    fclose(fileDesc);   

    if (count == 2)
    {
      //Indicate a successful operation
      TraceLog(DEBUG_LEVEL, "Listen Trader Tool on the port = %d\n", traderToolsInfo.config.port);
      TraceLog(DEBUG_LEVEL, "Max connections of Trader Tool = %d\n", traderToolsInfo.config.maxConnections);

      return SUCCESS;
    }
    else
    {
      //Configuration file is invalid!
      TraceLog(ERROR_LEVEL, "Invalid configuration file '%s'\n", fullPath);

      return ERROR;
    }   
  }
}

/****************************************************************************
- Function name:  SaveTraderToolsConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SaveTraderToolsConf()
{
  // Get full raw data path
  char fullPath[MAX_PATH_LEN] = "\0";
  
  if (GetFullConfigPath(FILE_NAME_OF_TRADER_TOOL_CONF, CONF_MISC, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Have some problem in get full raw data path operation (%s)\n", FILE_NAME_OF_TRADER_TOOL_CONF);

    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Set Trader Tool config: %s\n", fullPath);
  TraceLog(DEBUG_LEVEL, "Listen Trader Tool on the port = %d\n", traderToolsInfo.config.port);
  TraceLog(DEBUG_LEVEL, "Max connections of Trader Tool = %d\n", traderToolsInfo.config.maxConnections);

  //Try to open Trader Tool configuration file
  //Please be aware of the requirements for the configuration file here
  FILE *fileDesc = fopen(fullPath, "w+");
  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Cannot open file '%s' to read or we don't have permission to create\n", fullPath);

    return ERROR;
  }
  else
  {
    /*
    # Port: Which port will Aggragation Server listen on? This value
    # must be in range 1024 and 65535
    2340
    # Max connections: How many connections from Trader Tool can
    # Aggregation Server accept? This value must be in range 1 and 4
    2
    */
    fprintf(fileDesc, "# Port: Which port will Aggragation Server listen on? This value\n");
    fprintf(fileDesc, "# must be in range 1024 and 65535\n");
    fprintf(fileDesc, "%d\n", traderToolsInfo.config.port);
    fprintf(fileDesc, "# Max connections: How many connections from Trader Tool can\n");
    fprintf(fileDesc, "# Aggregation Server accept? This value must be in range 1 and 4\n");
    fprintf(fileDesc, "%d\n", traderToolsInfo.config.maxConnections);
    
    fflush(fileDesc);

    //Close the file
    fclose(fileDesc);

    TraceLog(DEBUG_LEVEL, "Configuration of Trader Tool is saved successfully!\n");

    return SUCCESS;
  }

  return ERROR;
}

/****************************************************************************
- Function name:  LoadPMConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int LoadPMConf(void)
{
  // Get full raw data path
  char fullPath[MAX_PATH_LEN] = "\0";
  
  if (GetFullConfigPath(FILE_NAME_OF_PM_CONF, CONF_MISC, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Have some problem in get full raw data path operation (%s)\n", FILE_NAME_OF_PM_CONF);

    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Load PM config: %s\n", fullPath);

  //Try to open PM configuration file
  //Please be aware of the requirements for the configuration file here
  FILE *fileDesc = fopen(fullPath, "r");
  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Cannot open file '%s' to read or we don't have permission to create\n", fullPath);

    return ERROR;
  }
  else
  {
    //Buffer to store each line in the configuration file
    char line[MAX_PATH_LEN];

    //Number of lines read from the configuration file
    int  count = 0;

    //Clear the buffer pointed to by line
    memset(line, 0, MAX_PATH_LEN);

    //Get each line from the configuration file
    while (fgets(line, MAX_PATH_LEN, fileDesc) != NULL) 
    {
      if ((line[0] != '#') && (line[0] > 32))
      {
        count ++;

        if (count == 1)
        {
          memcpy(pmInfo.config.ip, line, strlen(line) - 1);
        }
        else if (count == 2)
        {
          pmInfo.config.port = atoi(line);
        }
      }

      //Clear the buffer pointed to by line
      memset(line, 0, MAX_PATH_LEN);
    }

    //Close the file
    fclose(fileDesc);   

    if (count == 2)
    {
      //Indicate a successful operation
      TraceLog(DEBUG_LEVEL, "IP address = %s\n", pmInfo.config.ip);
      TraceLog(DEBUG_LEVEL, "Port = %d\n", pmInfo.config.port);

      return SUCCESS;
    }
    else
    {
      //Configuration file is invalid!
      TraceLog(ERROR_LEVEL, "Invalid configuration file '%s'\n", fullPath);

      return ERROR;
    }   
  }
}

/****************************************************************************
- Function name:  SavePMConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SavePMConf(void)
{
  // Get full raw data path
  char fullPath[MAX_PATH_LEN] = "\0";
  
  if (GetFullConfigPath(FILE_NAME_OF_PM_CONF, CONF_MISC, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Have some problem in get full raw data path operation (%s)\n", FILE_NAME_OF_PM_CONF);

    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Set PM config: %s\n", fullPath);
  TraceLog(DEBUG_LEVEL, "IP address = %s\n", pmInfo.config.ip);
  TraceLog(DEBUG_LEVEL, "Port = %d\n", pmInfo.config.port);

  //Try to open PM configuration file
  //Please be aware of the requirements for the configuration file here
  FILE *fileDesc = fopen(fullPath, "w+");
  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Cannot open file '%s' to read or we don't have permission to create\n", fullPath);

    return ERROR;
  }
  else
  {
    /*
    # IP address to connect to PM
    192.168.9.112
    # Port to connect to PM. This value must be in range 1024 and 65535
    30010
    */
    fprintf(fileDesc, "# IP address to connect to PM\n");
    fprintf(fileDesc, "%s\n", pmInfo.config.ip);
    fprintf(fileDesc, "# Port to connect to PM. This value must be in range 1024 and 65535\n");
    fprintf(fileDesc, "%d\n", pmInfo.config.port);
    
    fflush(fileDesc);

    //Close the file
    fclose(fileDesc);

    TraceLog(DEBUG_LEVEL, "Configuration of PM is saved successfully!\n");

    return SUCCESS;
  }

  return ERROR;
}

/****************************************************************************
- Function name:  LoadASGlobalConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int LoadASGlobalConf(void)
{
  // Get full raw data path
  char fullPath[MAX_PATH_LEN] = "\0";
  
  if (GetFullConfigPath(FILE_NAME_OF_AS_GLOBAL_CONF, CONF_MISC, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Have some problem in get full raw data path operation (%s)\n", FILE_NAME_OF_AS_GLOBAL_CONF);

    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Load AS global config: %s\n", fullPath);

  //Try to open global configuration file
  //Please be aware of the requirements for the configuration file here
  FILE *fileDesc = fopen(fullPath, "r");
  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Cannot open file '%s' to read or we don't have permission to create\n", fullPath);

    return ERROR;
  }
  else
  {
    //Buffer to store each line in the configuration file
    char line[MAX_PATH_LEN];

    //Number of lines read from the configuration file
    int  count = 0;

    //Clear the buffer pointed to by line
    memset(line, 0, MAX_PATH_LEN);

    //Get each line from the configuration file
    while (fgets(line, MAX_PATH_LEN, fileDesc) != NULL) 
    {
      if ((line[0] != '#') && (line[0] > 32))
      {
        count ++;

        if (count == 1)
        {
          traderToolsInfo.globalConf.maxStuck = atoi(line);
        }
        else if (count == 2)
        {
          traderToolsInfo.globalConf.maxLoser = atoi(line);
        }
        else if (count == 3)
        {
          traderToolsInfo.globalConf.maxCross = atoi(line);
        }
        else if (count == 4)
        {
          traderToolsInfo.globalConf.asVolatileThreshold = atof(line);
        }
        else if (count == 5)
        {
          traderToolsInfo.globalConf.asDisabledThreshold = atof(line);
        }
                else if (count == 6)
        {
          traderToolsInfo.globalConf.tsVolatileThreshold = atof(line);
        }
        else if (count == 7)
        {
          traderToolsInfo.globalConf.tsDisabledThreshold = atof(line);
        }
        else if (count == 8)
        {
          traderToolsInfo.globalConf.buyingPower[FIRST_ACCOUNT] = atof(line);
        }
        else if (count == 9)
        {
          traderToolsInfo.globalConf.buyingPower[SECOND_ACCOUNT] = atof(line);
        }
        else if (count == 10)
        {
          traderToolsInfo.globalConf.buyingPower[THIRD_ACCOUNT] = atof(line);
        }
        else if (count == 11)
        {
          traderToolsInfo.globalConf.buyingPower[FOURTH_ACCOUNT] = atof(line);
        }
        else if (count == 12)
        {
          NumberOutputLogToStop = atoi(line);
        }
        else if (count == 13)
        {
          NumberOutputLogToResume = atoi(line);
        }
        else if (count == 14)
        {
          PMtoTTCancelLimit = atoi(line);
        }
        else if (count == 15)
        {
          FastMarketMgmt.thresHold = atoi(line);
        }
      }

      //Clear the buffer pointed to by line
      memset(line, 0, MAX_PATH_LEN);
    }

    //Close the file
    fclose(fileDesc);   

    if (count >= 15)
    {
            // Check volatile and disabled threshold
            if (traderToolsInfo.globalConf.asDisabledThreshold < traderToolsInfo.globalConf.asVolatileThreshold)
            {
                TraceLog(ERROR_LEVEL, "AS disabled threshold (%lf) < AS volatile threshold (%lf). This is not allowed\n",
                    traderToolsInfo.globalConf.asDisabledThreshold, traderToolsInfo.globalConf.asVolatileThreshold);
                return ERROR;
            }
            if (traderToolsInfo.globalConf.tsDisabledThreshold < traderToolsInfo.globalConf.tsVolatileThreshold)
            {
                TraceLog(ERROR_LEVEL, "TS disabled threshold (%lf) < TS volatile threshold (%lf). This is not allowed\n",
                    traderToolsInfo.globalConf.tsDisabledThreshold, traderToolsInfo.globalConf.tsVolatileThreshold);
                return ERROR;
            }
            
      //Indicate a successful operation
      TraceLog(DEBUG_LEVEL, "Maximum stuck positions = %d\n", traderToolsInfo.globalConf.maxStuck);
      TraceLog(DEBUG_LEVEL, "Maximum consecutive losers = %d\n", traderToolsInfo.globalConf.maxLoser);
      TraceLog(DEBUG_LEVEL, "Maximum crosses per minute per TS = %d\n", traderToolsInfo.globalConf.maxCross);
      TraceLog(DEBUG_LEVEL, "AS Volatile threshold = %lf\n", traderToolsInfo.globalConf.asVolatileThreshold);
      TraceLog(DEBUG_LEVEL, "AS Disabled threshold = %lf\n", traderToolsInfo.globalConf.asDisabledThreshold);
            TraceLog(DEBUG_LEVEL, "TS Volatile threshold = %lf\n", traderToolsInfo.globalConf.tsVolatileThreshold);
      TraceLog(DEBUG_LEVEL, "TS Disabled threshold = %lf\n", traderToolsInfo.globalConf.tsDisabledThreshold);
      TraceLog(DEBUG_LEVEL, "Global buying power for account 1 = %lf\n", traderToolsInfo.globalConf.buyingPower[FIRST_ACCOUNT]);
      TraceLog(DEBUG_LEVEL, "Global buying power for account 2 = %lf\n", traderToolsInfo.globalConf.buyingPower[SECOND_ACCOUNT]);
      TraceLog(DEBUG_LEVEL, "Global buying power for account 3 = %lf\n", traderToolsInfo.globalConf.buyingPower[THIRD_ACCOUNT]);
      TraceLog(DEBUG_LEVEL, "Global buying power for account 4 = %lf\n", traderToolsInfo.globalConf.buyingPower[FOURTH_ACCOUNT]);

      return SUCCESS;
    }
    else
    {
      //Configuration file is invalid!
      TraceLog(ERROR_LEVEL, "Invalid configuration file '%s'\n", fullPath);

      return ERROR;
    }
  }
}

/****************************************************************************
- Function name:  SaveASGlobalConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SaveASGlobalConf(void)
{
  // Get full raw data path
  char fullPath[MAX_PATH_LEN] = "\0";
  
  if (GetFullConfigPath(FILE_NAME_OF_AS_GLOBAL_CONF, CONF_MISC, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Have some problem in get full raw data path operation (%s)\n", FILE_NAME_OF_AS_GLOBAL_CONF);

    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Set global config of Aggregation Server: %s\n", fullPath);
  TraceLog(DEBUG_LEVEL, "Maximum stuck positions = %d\n", traderToolsInfo.globalConf.maxStuck);
  TraceLog(DEBUG_LEVEL, "Maximum consecutive losers = %d\n", traderToolsInfo.globalConf.maxLoser);
  TraceLog(DEBUG_LEVEL, "Maximum crosses per minute per TS = %d\n", traderToolsInfo.globalConf.maxCross);
  TraceLog(DEBUG_LEVEL, "AS Volatile threshold = %lf\n", traderToolsInfo.globalConf.asVolatileThreshold);
  TraceLog(DEBUG_LEVEL, "AS Disabled threshold = %lf\n", traderToolsInfo.globalConf.asDisabledThreshold);
    TraceLog(DEBUG_LEVEL, "TS Volatile threshold = %lf\n", traderToolsInfo.globalConf.tsVolatileThreshold);
  TraceLog(DEBUG_LEVEL, "TS Disabled threshold = %lf\n", traderToolsInfo.globalConf.tsDisabledThreshold);
  TraceLog(DEBUG_LEVEL, "Global buying power for account 1 = %lf\n", traderToolsInfo.globalConf.buyingPower[FIRST_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Global buying power for account 2 = %lf\n", traderToolsInfo.globalConf.buyingPower[SECOND_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Global buying power for account 3 = %lf\n", traderToolsInfo.globalConf.buyingPower[THIRD_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Global buying power for account 4 = %lf\n", traderToolsInfo.globalConf.buyingPower[FOURTH_ACCOUNT]);

  //Try to open global configuration file
  //Please be aware of the requirements for the configuration file here
  FILE *fileDesc = fopen(fullPath, "w+");
  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Cannot open file '%s' to read or we don't have permission to create\n", fullPath);

    return ERROR;
  }
  else
  {
    /*
    # Maximum stuck positions: Default is 5
    6
    # Maximum consecutive losers: Default is 5
    6
    */
    fprintf(fileDesc, "# Maximum stuck positions: Default is 5\n");
    fprintf(fileDesc, "%d\n", traderToolsInfo.globalConf.maxStuck);
    fprintf(fileDesc, "# Maximum consecutive losers: Default is 5\n");
    fprintf(fileDesc, "%d\n", traderToolsInfo.globalConf.maxLoser);
    fprintf(fileDesc, "# Maximum crosses per minute per TS\n");
    fprintf(fileDesc, "%d\n", traderToolsInfo.globalConf.maxCross);
    fprintf(fileDesc, "# AS Volatile threshold\n");
    fprintf(fileDesc, "%lf\n", traderToolsInfo.globalConf.asVolatileThreshold);
    fprintf(fileDesc, "# AS Disabled threshold\n");
    fprintf(fileDesc, "%lf\n", traderToolsInfo.globalConf.asDisabledThreshold);
        fprintf(fileDesc, "# TS Volatile threshold\n");
    fprintf(fileDesc, "%lf\n", traderToolsInfo.globalConf.tsVolatileThreshold);
    fprintf(fileDesc, "# TS Disabled threshold\n");
    fprintf(fileDesc, "%lf\n", traderToolsInfo.globalConf.tsDisabledThreshold);
    fprintf(fileDesc, "# Global buying power for all account 1\n");
    fprintf(fileDesc, "%lf\n", traderToolsInfo.globalConf.buyingPower[FIRST_ACCOUNT]);
    fprintf(fileDesc, "# Global buying power for all account 2\n");
    fprintf(fileDesc, "%lf\n", traderToolsInfo.globalConf.buyingPower[SECOND_ACCOUNT]);
    fprintf(fileDesc, "# Global buying power for all account 3\n");
    fprintf(fileDesc, "%lf\n", traderToolsInfo.globalConf.buyingPower[THIRD_ACCOUNT]);
    fprintf(fileDesc, "# Global buying power for all account 4\n");
    fprintf(fileDesc, "%lf\n", traderToolsInfo.globalConf.buyingPower[FOURTH_ACCOUNT]);
    fprintf(fileDesc, "# Number of output log entries to stop\n");
    fprintf(fileDesc, "%d\n", NumberOutputLogToStop);
    fprintf(fileDesc, "# Number of output log entries to resume\n");
    fprintf(fileDesc, "%d\n", NumberOutputLogToResume);
    fprintf(fileDesc, "# Maximum number of cancel message will be sent to TT each time: Default is 2\n");
    fprintf(fileDesc, "# If value is -1 --> there will be no filter\n");
    fprintf(fileDesc, "%d\n", PMtoTTCancelLimit);
    fprintf(fileDesc, "# FastMarket threshold: number of trades/sec to swith to FastMarket\n");
    fprintf(fileDesc, "%d\n", FastMarketMgmt.thresHold);
    fflush(fileDesc);

    //Close the file
    fclose(fileDesc);

    TraceLog(DEBUG_LEVEL, "Global configuration of Aggregation Server is saved successfully!\n");

    return SUCCESS;
  }

  return ERROR;
}

/****************************************************************************
- Function name:  LoadASOrdersConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int LoadASOrdersConf(void)
{
  // Load NASDAQ RASH configuration
  if (LoadNASDAQ_RASHConf("nasdaq_rash_conf", &NASDAQ_RASH_Config) == ERROR)
  {
    return ERROR;
  }

  // Load ARCA DIRECT configuration
  if (LoadARCA_DIRECTConf("arca_direct_conf", &ARCA_DIRECT_Config) == ERROR)
  {
    return ERROR;
  }
  
  // Load next order id from file
  clientOrderID = LoadClientOrderID("next_order_id");

  TraceLog(DEBUG_LEVEL, "Client Order ID = %d\n", clientOrderID);

  if (clientOrderID == ERROR)
  {
    return ERROR;
  } 
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadNASDAQ_RASHConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int LoadNASDAQ_RASHConf(char *fileName, void *ECNOrderConf)
{
  t_NASDAQ_RASH_Config *orderConf = (t_NASDAQ_RASH_Config *)ECNOrderConf;

  // Get full raw data path
  char fullPath[MAX_PATH_LEN] = "\0";
  
  if (GetFullConfigPath(fileName, CONF_ORDER, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Have some problem in get full raw data path operation (%s)\n", fileName);

    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Load NASDAQ RASH config: %s\n", fullPath);

  //Try to open ignore stock file
  //Please be aware of the requirements for the configuration file here
  FILE *fileDesc = fopen(fullPath, "r+");
  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Cannot open file '%s' to read or we don't have permission to create\n", fullPath);

    return ERROR;
  }
  else
  {
    //Buffer to store each line in the configuration file
    char line[512];

    //Number of lines read from the configuration file
    int  count = 0;

    //Clear the buffer pointed to by line
    memset(line, 0, 512);
    
    // Init buffer for RashRouteID
    memset(RashRouteID, 0, sizeof(RashRouteID));  
    memset(RashRouteID[PRE_MARKET], 32, 4);   
    memset(RashRouteID[INTRADAY], 32, 4);
    memset(RashRouteID[POST_MARKET], 32, 4);
    
    //Get each line from the configuration file
    while (fgets(line, 512, fileDesc) != NULL) 
    {
      if ((line[0] != '#') && (line[0] > 32))
      {
        count++;
        if (count == 1)
        {
          RemoveCharacters(line, 512);

          memset(orderConf->ipAddress, 0, MAX_LINE_LEN);
          strncpy(orderConf->ipAddress, line, strlen(line));
        }
        else if (count == 2)
        {
          orderConf->port = atoi(line);
        }
        else if (count == 3)
        {
          RemoveCharacters(line, 512);

          memset(orderConf->userName, 0, MAX_LINE_LEN);
          strncpy(orderConf->userName, line, strlen(line));
        }
        else if (count == 4)
        {
          RemoveCharacters(line, 512);

          memset(orderConf->password, 0, MAX_LINE_LEN);
          strncpy(orderConf->password, line, strlen(line));
        }
         // Load configuration for route destinations three time types
        else if (count == 5)
        {
          RemoveCharacters(line, 512);
          if (strncmp(line, "null", 4) != 0)
          {
            strncpy(RashRouteID[PRE_MARKET], line, 4);
          }
        }
        else if (count == 6)
        {
          RemoveCharacters(line, 512);
          if (strncmp(line, "null", 4) != 0)
          {
            strncpy(RashRouteID[INTRADAY], line, 4);
          }
        }
        else if (count == 7)
        {
          RemoveCharacters(line, 512);
          if (strncmp(line, "null", 4) != 0)
          {
            strncpy(RashRouteID[POST_MARKET], line, 4);
          } 
        }
      }

      //Clear the buffer pointed to by line
      memset(line, 0, 512);
    }

    //Close the file
    fclose(fileDesc);
    
    if (count == 7)
    {
      //Indicate a successful operation
      TraceLog(DEBUG_LEVEL, "ipAddress = %s\n", orderConf->ipAddress);
      TraceLog(DEBUG_LEVEL, "port = %d\n", orderConf->port);
      TraceLog(DEBUG_LEVEL, "userName = %s\n", orderConf->userName);
      TraceLog(DEBUG_LEVEL, "password = %s\n", orderConf->password);
      TraceLog(DEBUG_LEVEL, "Route Dest/Exec Broker for Pre-market-time = %.4s\n", RashRouteID[PRE_MARKET]);
      TraceLog(DEBUG_LEVEL, "Route Dest/Exec Broker for Intraday-market-time = %.4s\n", RashRouteID[INTRADAY]);
      TraceLog(DEBUG_LEVEL, "Route Dest/Exec Broker for Post-market-time = %.4s\n", RashRouteID[POST_MARKET]);
      return SUCCESS;
    }
    else
    {
      //Configuration file is invalid!
      TraceLog(ERROR_LEVEL, "Invalid configuration file '%s'\n", fullPath);

      return ERROR;
    }
  }
}

/****************************************************************************
- Function name:  LoadARCA_DIRECTConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int LoadARCA_DIRECTConf(char *fileName, void *ECNOrderConf)
{
  t_ARCA_DIRECT_Config *orderConf = (t_ARCA_DIRECT_Config *)ECNOrderConf;

  // Get full raw data path
  char fullPath[MAX_PATH_LEN] = "\0";
  
  if (GetFullConfigPath(fileName, CONF_ORDER, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Have some problem in get full raw data path operation (%s)\n", fileName);

    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Load ARCA DIRECT config: %s\n", fullPath);

  //Try to open ignore stock file
  //Please be aware of the requirements for the configuration file here
  FILE *fileDesc = fopen(fullPath, "r+");
  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Cannot open file '%s' to read or we don't have permission to create\n", fullPath);

    return ERROR;
  }
  else
  {
    //Buffer to store each line in the configuration file
    char line[MAX_LINE_LEN];

    //Number of lines read from the configuration file
    int  count = 0;

    //Clear the buffer pointed to by line
    memset(line, 0, MAX_LINE_LEN);

    //Get each line from the configuration file
    while (fgets(line, MAX_LINE_LEN, fileDesc) != NULL) 
    {
      if ((line[0] != '#') && (line[0] > 32))
      {
        count ++;

        if (count == 1)
        {
          RemoveCharacters(line, MAX_LINE_LEN);

          memset(orderConf->ipAddress, 0, MAX_LINE_LEN);
          strncpy(orderConf->ipAddress, line, strlen(line));
        }
        else if (count == 2)
        {
          orderConf->port = atoi(line);
        }
        else if (count == 3)
        {
          RemoveCharacters(line, MAX_LINE_LEN);

          memset(orderConf->userName, 0, MAX_LINE_LEN);
          strncpy(orderConf->userName, line, strlen(line));
        }
        else if (count == 4)
        {
          RemoveCharacters(line, MAX_LINE_LEN);

          memset(orderConf->password, 0, MAX_LINE_LEN);
          strncpy(orderConf->password, line, strlen(line));
        }
        else if (count == 5)
        {
          RemoveCharacters(line, 6);
          memset(orderConf->compGroupID, 0, 6);
          strncpy(orderConf->compGroupID, line, strlen(line));
        }
        else if (count == 6)
        {
          orderConf->autoCancelOnDisconnect = atoi(line);
        }
      }

      //Clear the buffer pointed to by line
      memset(line, 0, MAX_LINE_LEN);
    }

    //Close the file
    fclose(fileDesc);

    if (count == 6)
    {
      //Indicate a successful operation
      TraceLog(DEBUG_LEVEL, "ipAddress = %s\n", orderConf->ipAddress);
      TraceLog(DEBUG_LEVEL, "port = %d\n", orderConf->port);
      TraceLog(DEBUG_LEVEL, "userName = %s\n", orderConf->userName);
      TraceLog(DEBUG_LEVEL, "password = %s\n", orderConf->password);
      TraceLog(DEBUG_LEVEL, "compGroupID = %s\n", orderConf->compGroupID);
      TraceLog(DEBUG_LEVEL, "auto-cancel-on-disconnect = %d\n", orderConf->autoCancelOnDisconnect);
      return SUCCESS;
    }
    else
    {
      //Configuration file is invalid!
      TraceLog(ERROR_LEVEL, "Invalid configuration file '%s'\n", fullPath);

      return ERROR;
    }
  }
}

/****************************************************************************
- Function name:  ProcessSaveASOrderConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessSaveASOrderConf(int ECNIndex)
{
  switch (ECNIndex)
  {
    case AS_NASDAQ_RASH_INDEX:
      if (SaveNASDAQ_RASHConf("nasdaq_rash_conf", &NASDAQ_RASH_Config) == ERROR)
      {
        return ERROR;
      }
      break;

    case AS_ARCA_DIRECT_INDEX:
      if (SaveARCA_DIRECTConf("arca_direct_conf", &ARCA_DIRECT_Config) == ERROR)
      {
        return ERROR;
      }
      break;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SaveNASDAQ_RASHConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SaveNASDAQ_RASHConf(char *fileName, void *ECNOrderConf)
{
  t_NASDAQ_RASH_Config *orderConf = (t_NASDAQ_RASH_Config *)ECNOrderConf;

  // Get full raw data path
  char fullPath[MAX_PATH_LEN] = "\0";
  
  if (GetFullConfigPath(fileName, CONF_ORDER, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Have some problem in get full raw data path operation (%s)\n", fileName);

    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Save NASDAQ RASH configuration: %s\n", fullPath);
  TraceLog(DEBUG_LEVEL, "ipAddress = %s\n", orderConf->ipAddress);
  TraceLog(DEBUG_LEVEL, "port = %d\n", orderConf->port);
  TraceLog(DEBUG_LEVEL, "userName = %s\n", orderConf->userName);
  TraceLog(DEBUG_LEVEL, "password = %s\n", orderConf->password);
  TraceLog(DEBUG_LEVEL, "Route Dest/Exec Broker for Pre-market-time = %.4s\n", RashRouteID[PRE_MARKET]);
  TraceLog(DEBUG_LEVEL, "Route Dest/Exec Broker for Intraday-market-time = %.4s\n", RashRouteID[INTRADAY]);
  TraceLog(DEBUG_LEVEL, "Route Dest/Exec Broker for Post-market-time = %.4s\n", RashRouteID[POST_MARKET]);

  //Try to open ignore stock file
  //Please be aware of the requirements for the configuration file here
  FILE *fileDesc = fopen(fullPath, "w+");
  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Cannot open file '%s' to read or we don't have permission to create\n", fullPath);

    return ERROR;
  }
  else
  {
    /*
    #IP Address of the NASDAQ RASH Server
    172.18.17.233
    #Port number of the NASDAQ RASH Server to connect to
    1111
    #userName to log in to NASDAQ RASH Server
    PFS01
    #Password to log in to NASDAQ RASH Server
    P123456789
    */
    fprintf(fileDesc, "# IP Address of the NASDAQ RASH Server\n");
    fprintf(fileDesc, "%s\n", orderConf->ipAddress);
    fprintf(fileDesc, "# Port number of the NASDAQ RASH Server to connect to\n");
    fprintf(fileDesc, "%d\n", orderConf->port);
    fprintf(fileDesc, "# userName to log in to NASDAQ RASH Server\n");
    fprintf(fileDesc, "%s\n", orderConf->userName);
    fprintf(fileDesc, "# Password to log in to NASDAQ RASH Server\n");
    fprintf(fileDesc, "%s\n", orderConf->password);
    
    fprintf(fileDesc, "# Add Configurations Route Dest/Exec Broker for RASH, if conf is null --> \"    \"\n");
    fprintf(fileDesc, "# Route Dest/Exec Broker for pre-market-time\n");
    if (strncmp(RashRouteID[PRE_MARKET], "    ", 4) == 0)
    {
      fprintf(fileDesc, "%.4s\n", "null");
    }
    else
    {
      fprintf(fileDesc, "%.4s\n", RashRouteID[PRE_MARKET]);
    }

    fprintf(fileDesc, "# Route Dest/Exec Broker for intraday-market-time\n");
    if (strncmp(RashRouteID[INTRADAY], "    ", 4) == 0)
    {
      fprintf(fileDesc, "%.4s\n", "null");
    }
    else
    {
      fprintf(fileDesc, "%.4s\n", RashRouteID[INTRADAY]);
    }

    fprintf(fileDesc, "# Route Dest/Exec Broker for post-market-time\n");
    if (strncmp(RashRouteID[POST_MARKET], "    ", 4) == 0)
    {
      fprintf(fileDesc, "%.4s\n", "null");
    }
    else
    {
      fprintf(fileDesc, "%.4s\n", RashRouteID[POST_MARKET]);
    }

    fflush(fileDesc);
    
    //Close the file
    fclose(fileDesc);

    TraceLog(DEBUG_LEVEL, "Order configuration of Aggregation Server is saved successfully!\n");

    return SUCCESS;
  }
}

/****************************************************************************
- Function name:  SaveARCA_DIRECTConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SaveARCA_DIRECTConf(char *fileName, void *ECNOrderConf)
{
  t_ARCA_DIRECT_Config *orderConf = (t_ARCA_DIRECT_Config *)ECNOrderConf;

  // Get full raw data path
  char fullPath[MAX_PATH_LEN] = "\0";
  
  if (GetFullConfigPath(fileName, CONF_ORDER, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Have some problem in get full raw data path operation (%s)\n", fileName);

    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Save ARCA DIRECT configuration: %s\n", fullPath);
  TraceLog(DEBUG_LEVEL, "ipAddress = %s\n", orderConf->ipAddress);
  TraceLog(DEBUG_LEVEL, "port = %d\n", orderConf->port);
  TraceLog(DEBUG_LEVEL, "userName = %s\n", orderConf->userName);
  TraceLog(DEBUG_LEVEL, "password = %s\n", orderConf->password);
  TraceLog(DEBUG_LEVEL, "auto-cancel-on-disconnect = %d\n", orderConf->autoCancelOnDisconnect);

  //Try to open ignore stock file
  //Please be aware of the requirements for the configuration file here
  FILE *fileDesc = fopen(fullPath, "w+");
  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Cannot open file '%s' to read or we don't have permission to create\n", fullPath);

    return ERROR;
  }
  else
  {
    /*
    #IP Address of the ARCA DIRECT Server
    172.18.17.233
    #Port number of the ARCA DIRECT Server to connect to
    1111
    #userName to log in to ARCA DIRECT Server
    PFS01
    #Password to log in to ARCA DIRECT Server
    P123456789
    */
    fprintf(fileDesc, "#IP Address\n");
    fprintf(fileDesc, "%s\n", orderConf->ipAddress);
    fprintf(fileDesc, "#Port\n");
    fprintf(fileDesc, "%d\n", orderConf->port);
    fprintf(fileDesc, "#UserName\n");
    fprintf(fileDesc, "%s\n", orderConf->userName);
    fprintf(fileDesc, "#Password\n");
    fprintf(fileDesc, "%s\n", orderConf->password);
    fprintf(fileDesc, "#Company Group ID\n");
    fprintf(fileDesc, "%s\n", orderConf->compGroupID);
    fprintf(fileDesc, "#Auto Cancel on Disconnect: 0 or 1\n");
    fprintf(fileDesc, "%d\n", orderConf->autoCancelOnDisconnect);
    
    fflush(fileDesc);
    
    //Close the file
    fclose(fileDesc);

    TraceLog(DEBUG_LEVEL, "Order configuration of Aggregation Server is saved successfully!\n");

    return SUCCESS;
  }
}

/****************************************************************************
- Function name:  LoadIgnoreStockList
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int LoadIgnoreStockList(void)
{
  // Get full raw data path
  char fullPath[MAX_PATH_LEN] = "\0";
  
  if (GetFullConfigPath(FILE_NAME_OF_IGNORE_STOCK_LIST, CONF_SYMBOL, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Have some problem in get full ignore stock list path operation (%s)\n", FILE_NAME_OF_IGNORE_STOCK_LIST);

    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Load ignored symbol list: %s\n", fullPath);

  //Try to open ignore stock file
  //Please be aware of the requirements for the configuration file here
  FILE *fileDesc = fopen(fullPath, "r+");
  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Cannot open file '%s' to read or we don't have permission to create\n", fullPath);

    return ERROR;
  }
  else
  {
    // Reset number of symbol
    IgnoreStockList.countStock = 0;

    //Get each line from the configuration file
    while (fread(&IgnoreStockList.stockList[IgnoreStockList.countStock], SYMBOL_LEN, 1, fileDesc) == 1) 
    {
      // Increase number of stock
      IgnoreStockList.countStock ++;
    }

    //Close the file
    fclose(fileDesc);

    TraceLog(DEBUG_LEVEL, "Count symbol = %d\n", IgnoreStockList.countStock);

    return IgnoreStockList.countStock;
  }
}


/****************************************************************************
- Function name:  LoadIgnoreStockRangesList
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int LoadIgnoreStockRangesList(void)
{
  // Get full raw data path
  char fullPath[MAX_PATH_LEN] = "\0";
  
  if (GetFullConfigPath(FILE_NAME_OF_IGNORE_STOCK_RANGES_LIST, CONF_SYMBOL, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Have some problem in get full ignore stock ranges list path operation (%s)\n", FILE_NAME_OF_IGNORE_STOCK_RANGES_LIST);

    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Load ignored symbol ranges list: %s\n", fullPath);

  //Try to open ignore stock ranges file
  //Please be aware of the requirements for the configuration file here
  FILE *fileDesc = fopen(fullPath, "r+");
  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Cannot open file '%s' to read or we don't have permission to create\n", fullPath);

    return ERROR;
  }
  else
  {
    // Reset number of symbol
    IgnoreStockRangesList.countStock = 0;

    //Get each line from the configuration file
    while (fread(&IgnoreStockRangesList.stockList[IgnoreStockRangesList.countStock], SYMBOL_LEN, 1, fileDesc) == 1) 
    {
      // Increase number of stock
      IgnoreStockRangesList.countStock ++;
    }

    //Close the file
    fclose(fileDesc);

    TraceLog(DEBUG_LEVEL, "Count symbol = %d\n", IgnoreStockRangesList.countStock);

    return IgnoreStockRangesList.countStock;
  }
}

/****************************************************************************
- Function name:  SaveHaltedStockListToFile
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SaveHaltedStockListToFile()
{
  // Get full raw data path
  char fullPath[MAX_PATH_LEN] = "\0";
  
  if (GetFullConfigPath(FILE_NAME_OF_HALTED_STOCK_LIST, CONF_SYMBOL, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Have some problem in get full halted stock list path operation (%s)\n", FILE_NAME_OF_HALTED_STOCK_LIST);

    return ERROR;
  }

  FILE *fileDesc = fopen(fullPath, "w+");
  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Cannot open file '%s' to read or we don't have permission to create\n", fullPath);
    return ERROR;
  }
  
  int sumStatus = 0;
  int i, j;
  
  fprintf(fileDesc, "#<symbol>,<status on venue 1>,<update time>, <status on venue 2>,<update time>, ...,<will auto resume>\n");
  
  for (i = 0; i < RMList.countSymbol; i++)
  {
    sumStatus = 0;
    for (j = 0; j < TS_MAX_BOOK_CONNECTIONS; j++)
    {
      sumStatus += HaltStatusMgmt.haltStatus[i][j];
    }
    
    if (sumStatus > 0)
    {
      fprintf(fileDesc, "%.8s", HaltStatusMgmt.symbol[i]);
      for (j = 0; j < TS_MAX_BOOK_CONNECTIONS; j++)
      {
        fprintf(fileDesc, ",%d,%u", HaltStatusMgmt.haltStatus[i][j], HaltStatusMgmt.haltTime[i][j]);
      }
      fprintf(fileDesc, ",%d\n", HaltStatusMgmt.isAutoResume[i]);
    }
  }
  
  fflush(fileDesc);

  //Close the file
  fclose(fileDesc); 

  return SUCCESS;
}

/****************************************************************************
- Function name:  SaveIgnoreStockList
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SaveIgnoreStockList(void)
{
  // Get full raw data path
  char fullPath[MAX_PATH_LEN] = "\0";
  
  if (GetFullConfigPath(FILE_NAME_OF_IGNORE_STOCK_LIST, CONF_SYMBOL, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Have some problem in get full ignore stock list path operation (%s)\n", FILE_NAME_OF_IGNORE_STOCK_LIST);

    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Save ignore stock list: %s\n", fullPath);

  //Try to open ignore stock file
  //Please be aware of the requirements for the configuration file here
  FILE *fileDesc = fopen(fullPath, "w+");
  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Cannot open file '%s' to read or we don't have permission to create\n", fullPath);

    return ERROR;
  }
  else
  {
    TraceLog(DEBUG_LEVEL, "Number of symbol = %d\n", IgnoreStockList.countStock);

    int i;
    
    for (i = 0; i < IgnoreStockList.countStock; i++)
    {
      if (fwrite(&IgnoreStockList.stockList[i], SYMBOL_LEN, 1, fileDesc) != 1)
      {
        TraceLog(ERROR_LEVEL, "Cannot save ignore symbol list to file\n");

        return ERROR;
      }
    }
    
    fflush(fileDesc);

    //Close the file
    fclose(fileDesc); 

    TraceLog(DEBUG_LEVEL, "Ignore symbol list is saved successfully!\n");

    return IgnoreStockList.countStock;
  }
}


/****************************************************************************
- Function name:  SaveIgnoreStockRangesList
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SaveIgnoreStockRangesList(void)
{
  // Get full raw data path
  char fullPath[MAX_PATH_LEN] = "\0";
  
  if (GetFullConfigPath(FILE_NAME_OF_IGNORE_STOCK_RANGES_LIST, CONF_SYMBOL, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Have some problem in get full ignore stock ranges list path operation (%s)\n", FILE_NAME_OF_IGNORE_STOCK_RANGES_LIST);

    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Save ignore stock ranges list: %s\n", fullPath);

  //Try to open ignore stock file
  //Please be aware of the requirements for the configuration file here
  FILE *fileDesc = fopen(fullPath, "w+");
  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Cannot open file '%s' to read or we don't have permission to create\n", fullPath);

    return ERROR;
  }
  else
  {
    TraceLog(DEBUG_LEVEL, "Number of symbol = %d\n", IgnoreStockRangesList.countStock);

    int i;
    
    for (i = 0; i < IgnoreStockRangesList.countStock; i++)
    {
      if (fwrite(&IgnoreStockRangesList.stockList[i], SYMBOL_LEN, 1, fileDesc) != 1)
      {
        TraceLog(ERROR_LEVEL, "Cannot save ignore symbol ranges list to file\n");

        return ERROR;
      }
    }
    
    fflush(fileDesc);

    //Close the file
    fclose(fileDesc); 

    TraceLog(DEBUG_LEVEL, "Ignore symbol ranges list is saved successfully!\n");

    return IgnoreStockRangesList.countStock;
  }
}
/****************************************************************************
- Function name:  LoadSequenceNumber
- Input:      + fileName
- Output:   + configInfo
- Return:   + Success or Failure
- Description:  + The routine hides the following facts
          - Check valid file name
          - Load and check valid configuration information
        + There are preconditions guaranteed to the routine
          - Data structure to contain configuration information
            must be allocated before it is passed to the routine
        + The routine guarantees that the return value will 
          have a value of either
          - Success
          or
          - Failure
- Usage:      N/A
****************************************************************************/
int LoadSequenceNumber(const char *fileName, int type, void *configInfo)
{
  char fullPath[MAX_PATH_LEN];
  
  if (GetFullConfigPath(fileName, CONF_ORDER, fullPath) == NULL_POINTER)
  {
    return ERROR;
  }
  
  switch (type)
  {
    case AS_NASDAQ_RASH_INDEX:
      return LoadIncomingSeqNum(fullPath, type, configInfo);
    case AS_ARCA_DIRECT_INDEX:
      return LoadIncomingAndOutgoingSeqNum(fullPath, type, configInfo);

    default:
      return ERROR;
  }
}

/****************************************************************************
- Function name:  LoadIncomingAndOutgoingSeqNum
- Input:      + fullPath
        + type
- Output:   + configInfo
- Return:   + Success or Failure
- Description:  + The routine hides the following facts
          - Check valid file name
          - Load and check valid configuration information
        + There are preconditions guaranteed to the routine
          - Data structure to contain configuration information
            must be allocated before it is passed to the routine
        + The routine guarantees that the return value will 
          have a value of either
          - Success
          or
          - Failure
- Usage:      N/A
****************************************************************************/
int LoadIncomingAndOutgoingSeqNum(const char *fullPath, int type, void *configInfo)
{
  FILE *fileDesc = fopen(fullPath, "r");
  char tempBuffer[MAX_LINE_LEN];
  int fieldIndex = 0;
  
  if (fileDesc == NULL)
  {
    return ERROR;
  }
  
  while (fgets(tempBuffer, MAX_LINE_LEN, fileDesc) != NULL)
  {
    RemoveCharacters(tempBuffer, MAX_LINE_LEN);

    // Skip comment lines
    if ((tempBuffer[0] == '#') || (tempBuffer[0] < USED_CHAR_CODE))
    {
      continue;
    }
    
    // Base on TYPE, we will cast to appropriate data type
    switch(type)  
    {
      case AS_ARCA_DIRECT_INDEX:
        if (fieldIndex == 0)
        {
          ((t_ARCA_DIRECT_Config *)configInfo)->incomingSeqNum = atoi(tempBuffer);
        }
        else if (fieldIndex == 1)
        {
          ((t_ARCA_DIRECT_Config *)configInfo)->outgoingSeqNum = atoi(tempBuffer);
        }
        else
        {
          TraceLog(ERROR_LEVEL, "Invalid configuration file '%s'\n", fullPath);

          // Close file and release resource
          fclose(fileDesc);
        }
                
        break;

      default:
        TraceLog(ERROR_LEVEL, "Unsupported order type '%d'\n", type);
        fclose(fileDesc);
        return ERROR;
    }
    
    fieldIndex++;
  }
  
  // Close file and release resource
  fclose(fileDesc);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadIncomingSeqNum
- Input:      + fullPath
        + type
- Output:   + configInfo
- Return:   + Success or Failure
- Description:  + The routine hides the following facts
          - Check valid file name
          - Load and check valid configuration information
        + There are preconditions guaranteed to the routine
          - Data structure to contain configuration information
            must be allocated before it is passed to the routine
        + The routine guarantees that the return value will 
          have a value of either
          - Success
          or
          - Failure
- Usage:      N/A
****************************************************************************/
int LoadIncomingSeqNum(const char *fullPath, int type, void *configInfo)
{
  FILE *fileDesc = fopen(fullPath, "r");
  char tempBuffer[MAX_LINE_LEN];
  int fieldIndex = 0;
  
  if (fileDesc == NULL)
  {
    return ERROR;
  }
  
  while (fgets(tempBuffer, MAX_LINE_LEN, fileDesc) != NULL)
  {
    RemoveCharacters(tempBuffer, MAX_LINE_LEN);

    // Skip comment lines
    if ((tempBuffer[0] == '#') || (tempBuffer[0] < USED_CHAR_CODE))
    {
      continue;
    }
    
    // Base on TYPE, we will cast to appropriate data type
    switch(type)  
    {
      case AS_NASDAQ_RASH_INDEX:
        if (fieldIndex == 0)
        {
          ((t_NASDAQ_RASH_Config *)configInfo)->incomingSeqNum = atoi(tempBuffer);
        }
        else
        {
          TraceLog(ERROR_LEVEL, "Invalid configuration file '%s'\n", fullPath);

          // Close file and release resource
          fclose(fileDesc);
        }
        break;
      default:
        TraceLog(ERROR_LEVEL, "Unsupported order type '%d'\n", type);
        fclose(fileDesc);
        return ERROR;
    }
    
    fieldIndex++;
  }
  
  // Close file and release resource
  fclose(fileDesc);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SaveSeqNumToFile
- Input:      + fileName
        + type
        + configInfo
- Output:   N/A
- Return:   Success or Failure
- Description:  + The routine hides the following facts

        + There are no preconditions guaranteed to the routine
        + The routine guarantees that the status value will 
          have a value of either
          - Success
          or
          - Failure
- Usage:      N/A
****************************************************************************/
int SaveSeqNumToFile(const char *fileName, int type, void *configInfo)
{
  char fullPath[MAX_PATH_LEN];
  
  if (GetFullConfigPath(fileName, CONF_ORDER, fullPath) == NULL_POINTER)
  {
    return ERROR;
  }
  
  switch (type)
  {
    case AS_NASDAQ_RASH_INDEX:
      return SaveIncomingSeqNumToFile(fullPath, type, configInfo);
    case AS_ARCA_DIRECT_INDEX:
      return SaveIncomingAndOutgoingSeqNumToFile(fullPath, type, configInfo);

    default:
      TraceLog(ERROR_LEVEL, "Unsupported order type '%d'\n", type);

      return ERROR;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SaveIncomingAndOutgoingSeqNumToFile
- Input:      + fileName
        + type
        + configInfo
- Output:   N/A
- Return:   Success or Failure
- Description:  + The routine hides the following facts

        + There are no preconditions guaranteed to the routine
        + The routine guarantees that the status value will 
          have a value of either
          - Success
          or
          - Failure
- Usage:      N/A
****************************************************************************/
int SaveIncomingAndOutgoingSeqNumToFile(const char *fileName, int type, void *configInfo)
{
  FILE *fileDesc = fopen(fileName, "w");
  
  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Cannot open file '%s' to write\n", fileName);
    return ERROR;
  }
  else
  {
    // Truncate file content to zero offset
    if (truncate(fileName, 0) == ERROR)
    {
      return ERROR;
    }
    
    switch (type)
    {
      case AS_ARCA_DIRECT_INDEX:
      
        fprintf(fileDesc, "#Incoming Sequence Number\n");
        fprintf(fileDesc, "%d\n", ((t_ARCA_DIRECT_Config *)configInfo)->incomingSeqNum);
        
        fprintf(fileDesc, "#Outgoing Sequence Number\n");
        fprintf(fileDesc, "%d\n", ((t_ARCA_DIRECT_Config *)configInfo)->outgoingSeqNum);
        
        fflush(fileDesc);
        fclose(fileDesc);
        break;
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SaveIncomingSeqNumToFile
- Input:      + fileName
        + type
        + configInfo
- Output:   N/A
- Return:   Success or Failure
- Description:  + The routine hides the following facts

        + There are no preconditions guaranteed to the routine
        + The routine guarantees that the status value will 
          have a value of either
          - Success
          or
          - Failure
- Usage:      N/A
****************************************************************************/
int SaveIncomingSeqNumToFile(const char *fileName, int type, void *configInfo)
{
  FILE *fileDesc = fopen(fileName, "w");
  
  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Cannot open file '%s' to write\n", fileName);
    return ERROR;
  }
  else
  {
    // Truncate file content to zero offset
    if (truncate(fileName, 0) == ERROR)
    {
      return ERROR;
    }
    
    switch (type)
    {
      case AS_NASDAQ_RASH_INDEX:
        fprintf(fileDesc, "#Incoming Sequence Number\n");
        fprintf(fileDesc, "%d\n", ((t_NASDAQ_RASH_Config *)configInfo)->incomingSeqNum);
        
        fflush(fileDesc);
        fclose(fileDesc);
        break;
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadClientOrderID
- Input:      + fileName
- Output:   N/A
- Return:   client order ID
- Description:  + The routine hides the following facts
          - Check valid file name
          - Load and check valid configuration information
        + There are no preconditions guaranteed to the routine
        + The routine guarantees that the return value will 
          have a value of either
          - Success
          or
          - Failure
- Usage:      N/A
****************************************************************************/
int LoadClientOrderID(const char *fileName)
{
  char fullPath[MAX_PATH_LEN];
  
  if (GetFullConfigPath(fileName, CONF_MISC, fullPath) == NULL_POINTER)
  {
    return ERROR;
  }
  
  FILE *fileDesc = fopen(fullPath, "r");
  char tempBuffer[MAX_LINE_LEN];
  int tempClientOrderID = -1;
  
  if (fileDesc == NULL)
  {
    return ERROR;
  }

  //Number of lines read from the configuration file
  int count = 0;

  //Clear the buffer pointed to by line
  memset(tempBuffer, 0, MAX_LINE_LEN);
  
  while (fgets(tempBuffer, MAX_LINE_LEN, fileDesc) != NULL)
  {
    // Skip comment lines
    if (tempBuffer[0] != '#')
    {
      count ++;

      if (count == 1)
      {
        tempClientOrderID = atoi(tempBuffer);
      }
    }

    //Clear the buffer pointed to by line
    memset(tempBuffer, 0, MAX_LINE_LEN);
  }
  
  // Close file and release resource
  fclose(fileDesc);

  if (count == 1)
  {   
    return tempClientOrderID;
  }
  else
  {
    //Configuration file is invalid!
    TraceLog(ERROR_LEVEL, "Invalid configuration file '%s'\n", fullPath);

    return ERROR;
  }
}

/****************************************************************************
- Function name:  SaveClientOrderID
- Input:      + fileName
- Output:   N/A
- Return:   client order ID
- Description:  + The routine hides the following facts
          - Check valid file name
          - Load and check valid configuration information
        + There are no preconditions guaranteed to the routine
        + The routine guarantees that the return value will 
          have a value of either
          - Success
          or
          - Failure
- Usage:      N/A
****************************************************************************/
int SaveClientOrderID(const char *fileName, int clientOrderID)
{
  char fullPath[MAX_PATH_LEN];
  
  if (GetFullConfigPath(fileName, CONF_MISC, fullPath) == NULL_POINTER)
  {
    return ERROR;
  }
  
  FILE *fileDesc = fopen(fullPath, "w");
  
  if (fileDesc == NULL)
  {
    return ERROR;
  }
  
  fprintf(fileDesc, "%d\n", clientOrderID);
  
  // Close file and release resource
  fclose(fileDesc);
  
  return clientOrderID;
}

/****************************************************************************
- Function name:  LoadLongShortFromDatabase
- Input:      
- Output:   
- Return:   
- Description:
- Usage:    
****************************************************************************/
int LoadLongShortFromDatabase(void)
{
  int i, side;

  // Check and create long short at date
  CheckAndCreateLongShortAtDate(globalDateStringOfDatabase);
  
  for (i = 0; i < longShortList.countSymbol; i++)
  {
    TraceLog(DEBUG_LEVEL, "%d: %c, %.8s, %d, %f, account %d\n", i + 1,
        longShortList.collection[i].side,
        longShortList.collection[i].symbol,
        longShortList.collection[i].shares,
        longShortList.collection[i].price,
        longShortList.collection[i].account);
  
    if (longShortList.collection[i].side == 'S')
    {
      side = SELL_TYPE;
    }
    else
    {
      side = BUY_TO_CLOSE;
    }

    UpdateRiskManagementCollection(longShortList.collection[i].symbol, 
                    longShortList.collection[i].shares, 
                    longShortList.collection[i].price, side, 0, 
                    longShortList.collection[i].account, 0.00);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  CheckAndProcessStopTradingByTWT
- Input:      
- Output:   
- Return:   
- Description:
- Usage:    
****************************************************************************/
int CheckAndProcessStopTradingByTWT(void)
{
  int isDisableTrading, isUpdate;

  // Load trading staus from database
  LoadTradingStatusFromDatabase(&isDisableTrading, &isUpdate);

  if (isUpdate == UPDATED)
  {
    if (isDisableTrading == DISCONNECTED)
    {
      TraceLog(DEBUG_LEVEL, "** AS was received request from TWT to stop trading\n");

      // Call function to disable trading on all ECN order
      SendDisableTradingAllOrderToAllTS(TS_TRADING_DISABLED_BY_TWT_REQUEST);
    }
  }

  int i, j, isStatus = 0;

  for (i = 0; i < MAX_TRADE_SERVER_CONNECTIONS; i++)
  {
    if (tradeServersInfo.connection[i].status == CONNECTED)
    {
      for(j = 0; j < TS_MAX_ORDER_CONNECTIONS; j++)
      {
        isStatus += tradeServersInfo.currentStatus[i].tsTradingStatus[j];
      }
    }
  }

  // Update trading staus to database
  if (isStatus > 0)
  {
    if (isDisableTrading != CONNECTED || isUpdate != NOT_YET)
    {
      UpdateTradingStatusToDatabase(CONNECTED, NOT_YET);
    }
  }
  else
  {
    if (isDisableTrading != DISCONNECTED || isUpdate != NOT_YET)
    {
      UpdateTradingStatusToDatabase(DISCONNECTED, NOT_YET);
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  CheckAndProcessHungOrders
- Input:      
- Output:   
- Return:   
- Description:
- Usage:    
****************************************************************************/
int CheckAndProcessHungOrders(void)
{
  int isUpdate = LoadHungOrderStatusFromDatabase();

  if (isUpdate == UPDATED)
  {
    TraceLog(DEBUG_LEVEL, "Processing hung orders from database.\n");

    // Get fill
    if (GetRawDataFromExecuteTable(globalDateStringOfDatabase, "fills", ORDER_EDITOR_ADD) == ERROR)
    {
      return ERROR;
    }
    
    // Get cancel
    if (GetRawDataFromNonExecuteTable(globalDateStringOfDatabase, "cancels", ORDER_EDITOR_ADD) == ERROR)
    {
      return ERROR;
    }
    
    // Update status of processing hung order
    UpdateHungOrderStatusToDatabase();
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  CheckAndModifyLongShortByTWT
- Input:      
- Output:   
- Return:   
- Description:
- Usage:    
****************************************************************************/
int CheckAndModifyLongShortByTWT(void)
{
  int isRMUpdate;

  // Load rmUpdate status from database
  LoadModifyLongShortStatusFromDatabase(&isRMUpdate);

  if (isRMUpdate == UPDATED)
  {
    TraceLog(DEBUG_LEVEL, "** AS was received request modify long/short from TWT\n\n");

    // Get open positions
    if (GetOpenPositions(globalDateStringOfDatabase, 0) == ERROR)
    {
      return ERROR;
    }
    
    // Update modify long/short status
    UpdateModifyLongShortStatusToDatabase();
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  CheckChangeOfBuyingPower
- Input:      
- Output:   
- Return:   
- Description:
- Usage:    
****************************************************************************/
int CheckChangeOfBuyingPower(void)
{
  // Calculation buying power
  CalculationPositionBuyingPower();

  // Send update buying power to all TS
  SendUpdateBuyingPowerToAllTS(MAX_ACCOUNT);  //MAX_ACCOUNT sends to all accounts

  return SUCCESS;
}

/****************************************************************************
- Function name:  CheckAndRemoveStaleHaltedSymbols
- Input:      
- Output:   
- Return:   
- Description:
- Usage:    
****************************************************************************/
int CheckAndRemoveStaleHaltedSymbols()
{
  /* Feature: all symbols which was halted and have no trade for 2 weeks should be deleted from halted list */
  
  int stockSymbolIndex, ecnIndex, ret;
  int saveFlag = 0;
  unsigned int haltedTime;
  time_t currentSeconds = (time_t)hbitime_seconds();
  
  char queryDate[12] = "\0";
  GetDateStringFormEpochSecs(currentSeconds - (60 * 60 * 24 * 14), queryDate);
    
  for (stockSymbolIndex = 0; stockSymbolIndex < RMList.countSymbol; stockSymbolIndex++)
  {
    //Get time when symbol is halted
    haltedTime = 0;
    for (ecnIndex = 0; ecnIndex < TS_MAX_BOOK_CONNECTIONS; ecnIndex++)
    {
      if (HaltStatusMgmt.haltStatus[stockSymbolIndex][ecnIndex] != 0)   //halted on $ecnIndex
      {
        if (haltedTime == 0)
        {
          haltedTime = HaltStatusMgmt.haltTime[stockSymbolIndex][ecnIndex];
        }
        else if (haltedTime < HaltStatusMgmt.haltTime[stockSymbolIndex][ecnIndex])
        {
          haltedTime = HaltStatusMgmt.haltTime[stockSymbolIndex][ecnIndex];
        }
      }
    }
    
    if (haltedTime < 1) continue;
    
    // Check if it was halted for more than 2 weeks
    if (currentSeconds - haltedTime < (60 * 60 * 24 * 14)) continue;
    
    // Now check from database to see if there is no trade within 2 weeks
    char haltDate[12] = "\0";
    GetDateStringFormEpochSecs(haltedTime, haltDate);
    
    ret = IsSymbolTradedSinceHaltedDate(HaltStatusMgmt.symbol[stockSymbolIndex], queryDate);
    if (ret == ERROR)
    {
      TraceLog(ERROR_LEVEL, "Can not query database for halted symbol '%.8s'", HaltStatusMgmt.symbol[stockSymbolIndex]);

      break;
    }
    else if (ret == YES)
    {
      //This symbol is still trading
    }
    else if (ret == NO)
    {
      //We will remove the symbol from halted list
      TraceLog(DEBUG_LEVEL, "Removed '%.8s' from halt stock list, since it was halted and not traded for 2 weeks\n", HaltStatusMgmt.symbol[stockSymbolIndex]);
      saveFlag = 1;
      HaltStatusMgmt.isAutoResume[stockSymbolIndex] = 1;
      
      for (ecnIndex = 0; ecnIndex < TS_MAX_BOOK_CONNECTIONS; ecnIndex++)
      {
        HaltStatusMgmt.haltTime[stockSymbolIndex][ecnIndex] = 0;
        HaltStatusMgmt.haltStatus[stockSymbolIndex][ecnIndex] = 0;
      }
    }
  }
  
  if (connPrevMonthDatabase != NULL)
  {
    DestroyConnection(connPrevMonthDatabase);
    connPrevMonthDatabase = NULL;
  }
  
  if (saveFlag == 1)
  {
    SaveHaltedStockListToFile();
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SaveTradeServerSetting
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SaveTradeServerSetting(int tsIndex)
{
  TraceLog(DEBUG_LEVEL, "Save TS ECN Settings, TS index %d:\n", tsIndex);
  TraceLog(DEBUG_LEVEL, "Books: ARCA(%d) NASDAQ(%d) NYSE(%d) BATSZ(%d) EDGX(%d) EDGA(%d) NASDAQ_BX(%d) BYX(%d) PSX(%d) AMEX(%d) CQS(%d) UQDF(%d)\n",
                         tradeServersInfo.tsSetting[tsIndex].bookEnable[TS_ARCA_BOOK_INDEX],
                         tradeServersInfo.tsSetting[tsIndex].bookEnable[TS_NASDAQ_BOOK_INDEX],
                         tradeServersInfo.tsSetting[tsIndex].bookEnable[TS_NYSE_BOOK_INDEX],
                         tradeServersInfo.tsSetting[tsIndex].bookEnable[TS_BATSZ_BOOK_INDEX],
                         tradeServersInfo.tsSetting[tsIndex].bookEnable[TS_EDGX_BOOK_INDEX],
                         tradeServersInfo.tsSetting[tsIndex].bookEnable[TS_EDGA_BOOK_INDEX],
                         tradeServersInfo.tsSetting[tsIndex].bookEnable[TS_NDBX_BOOK_INDEX],
                         tradeServersInfo.tsSetting[tsIndex].bookEnable[TS_BYX_BOOK_INDEX],
                         tradeServersInfo.tsSetting[tsIndex].bookEnable[TS_PSX_BOOK_INDEX],
                         tradeServersInfo.tsSetting[tsIndex].bookEnable[TS_AMEX_BOOK_INDEX],
                         tradeServersInfo.tsSetting[tsIndex].bboEnable[CQS_SOURCE], 
                         tradeServersInfo.tsSetting[tsIndex].bboEnable[UQDF_SOURCE]);

  TraceLog(DEBUG_LEVEL, "Orders: ARCA_DIRECT(%d) OUCH(%d) RASH(%d) BATSZ_BOE(%d) NYSE_CCG(%d) EDGX_DIRECT(%d) EDGA_DIRECT(%d) OUBX(%d) BYX_BOE(%d) PSX(%d)\n\n",
                         tradeServersInfo.tsSetting[tsIndex].orderEnable[TS_ARCA_DIRECT_INDEX],
                         tradeServersInfo.tsSetting[tsIndex].orderEnable[TS_NASDAQ_OUCH_INDEX],
                         tradeServersInfo.tsSetting[tsIndex].orderEnable[TS_NASDAQ_RASH_INDEX],
                         tradeServersInfo.tsSetting[tsIndex].orderEnable[TS_BATSZ_BOE_INDEX],
                         tradeServersInfo.tsSetting[tsIndex].orderEnable[TS_NYSE_CCG_INDEX],
                         tradeServersInfo.tsSetting[tsIndex].orderEnable[TS_EDGX_DIRECT_INDEX],
                         tradeServersInfo.tsSetting[tsIndex].orderEnable[TS_EDGA_DIRECT_INDEX],
                         tradeServersInfo.tsSetting[tsIndex].orderEnable[TS_NDAQ_OUBX_INDEX],
                         tradeServersInfo.tsSetting[tsIndex].orderEnable[TS_BYX_BOE_INDEX],
                         tradeServersInfo.tsSetting[tsIndex].orderEnable[TS_PSX_OUCH_INDEX]);


  // Add code here to update Trade Server setting
  UpdateTradeServerSetting(&tradeServersInfo.tsSetting[tsIndex], tradeServersInfo.config[tsIndex].tsId);

  return 0;
}

/****************************************************************************
- Function name:  SaveASSetting
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SaveASSetting(void)
{
  TraceLog(DEBUG_LEVEL, "Save AS ECN Settings:\n");
  TraceLog(DEBUG_LEVEL, "ECN Orders: RASH(%d) ARCA_FIX(%d)\n\n",
      asSetting.orderEnable[AS_NASDAQ_RASH_INDEX],
      asSetting.orderEnable[AS_ARCA_DIRECT_INDEX]);

  // Add code here to update AS setting
  UpdateASSetting();

  return 0;
}

/****************************************************************************
- Function name:  SavePMSetting
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SavePMSetting(void)
{
  TraceLog(DEBUG_LEVEL, "Save PM ECN Settings:\n");
  TraceLog(DEBUG_LEVEL, "ECN Books: ARCA(%d) NASDAQ(%d)\n",
      pmSetting.bookEnable[PM_ARCA_BOOK_INDEX],
      pmSetting.bookEnable[PM_NASDAQ_BOOK_INDEX]);

  TraceLog(DEBUG_LEVEL, "ECN Orders: ARCA_DIRECT(%d) RASH(%d)\n",
      pmSetting.orderEnable[PM_ARCA_DIRECT_INDEX],
      pmSetting.orderEnable[PM_NASDAQ_RASH_INDEX]);

  TraceLog(DEBUG_LEVEL, "Feeds: CTS(%d) UTDF(%d) CQS(%d) UQDF(%d)\n\n",
      pmSetting.CTS_Enable,
      pmSetting.UTDF_Enable,
      pmSetting.CQS_Enable,
      pmSetting.UQDF_Enable);

  // Add code here to update PM setting
  UpdatePMSetting();

  return 0;
}

/****************************************************************************
- Function name:  LoadHaltedStockListFromFile
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int LoadHaltedStockListFromFile()
{
  pthread_mutex_init(&HaltStatusMgmt.mutexLock, NULL);
  char fullPath[MAX_PATH_LEN];
  
  if (GetFullConfigPath(FILE_NAME_OF_HALTED_STOCK_LIST, CONF_SYMBOL, fullPath) == NULL_POINTER)
  {
    return ERROR;
  }

  FILE *fileDesc = fopen(fullPath, "r");
  
  if (fileDesc == NULL)
  {
    //Can not open file
    return SUCCESS;
  }
  
  char buff[1024] = "\0";

  // Read configuration info and check valid configuration format
  t_Parameters parameterList;
  int stockSymbolIndex;
  int i, count = 0;
  for (stockSymbolIndex = 0; stockSymbolIndex < MAX_STOCK_SYMBOL; stockSymbolIndex++)
  {
    memset (HaltStatusMgmt.symbol[stockSymbolIndex], 0, SYMBOL_LEN);
    for (i = 0; i < TS_MAX_BOOK_CONNECTIONS; i++)
    {
      HaltStatusMgmt.haltStatus[stockSymbolIndex][i] = TRADING_NORMAL;
      HaltStatusMgmt.haltTime[stockSymbolIndex][i] = 0;
      HaltStatusMgmt.isAutoResume[stockSymbolIndex] = 1;
    }
  }
  
  while (fgets(buff, 1024, fileDesc) != NULL)
  {
    // Skip comment lines
    if (buff[0] == '#')
    {
      continue;
    }
    
    // Remove used characters
    RemoveCharacters(buff, strlen(buff));
    
    // Skip empty line
    if (strlen(buff) == 0)
    {
      continue;
    }
    
    /* Line format should be:
      Symbol,status book1, time updated for book 1, status book 2, time updated for book 2 ...
    */
    Lrc_Split(buff, ',', &parameterList);
    if (parameterList.countParameters !=  (2 + 2 * TS_MAX_BOOK_CONNECTIONS))
    {
      TraceLog(ERROR_LEVEL, "File '%s' has invalid line format\n", FILE_NAME_OF_HALTED_STOCK_LIST);
      fclose(fileDesc);
      return ERROR;
    }
    
    stockSymbolIndex = GetStockSymbolIndex(parameterList.parameterList[0]);
    if (stockSymbolIndex != -1)
    {
      count++;
      strncpy(HaltStatusMgmt.symbol[stockSymbolIndex], parameterList.parameterList[0], SYMBOL_LEN);
      for (i = 0; i < TS_MAX_BOOK_CONNECTIONS; i++)
      {
        HaltStatusMgmt.haltStatus[stockSymbolIndex][i] = atoi(parameterList.parameterList[1 + i*2]);
        HaltStatusMgmt.haltTime[stockSymbolIndex][i] = atoi(parameterList.parameterList[2 + i*2]);
      }
      
      HaltStatusMgmt.isAutoResume[stockSymbolIndex] = atoi(parameterList.parameterList[TS_MAX_BOOK_CONNECTIONS*2 + 1]);
    }
  }
  
  fclose(fileDesc);
  
  TraceLog(DEBUG_LEVEL, "Loaded %d halted symbols from file\n", count);
  
  //Now we will check to remove stale symbols (which halted any not traded for 2 weeks) from list
  CheckAndRemoveStaleHaltedSymbols();
  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadTradingAccountFromFile
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int LoadTradingAccountFromFile(const char *fileName, void *_tradingAccount)
{
  t_TradingAccount *tradingAccount = (t_TradingAccount *)_tradingAccount;
  t_Parameters parameterList;
  
  /*
  Get full configuration path. If have any problem ERROR status will be returned
  for calling routine
  */
  
  char fullPath[MAX_PATH_LEN];
  
  if (GetFullConfigPath(fileName, CONF_MISC, fullPath) == NULL_POINTER)
  {
    return ERROR;
  }

  /*
  Try to open file with plain text mode to read. If we cannot open, report ERROR
  otherwise try to read configuration info
  */
  FILE *fileDesc = fopen(fullPath, "r");
  
  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "can not open this %s to read\n", fullPath);
    return ERROR;
  }
  
  /*
  Create and initialize temporary buffer to 
  contain a line of configuration information
  */
  char buff[1024] = "\0";
  int iSection = 0, iLine = 0;

  int tsIndex = 0, accountIndex = 0;
  
  // Read configuration info and check valid configuration format
  while (fgets(buff, 1024, fileDesc) != NULL)
  {
    iLine++;
    
    // Skip comment lines
    if (buff[0] == '#')
    {
      continue;
    }
    
    // Remove used characters
    RemoveCharacters(buff, strlen(buff));
    
    // Skip empty line
    if (strlen(buff) == 0)
    {
      continue;
    }
    
    if (iSection == 0)    //Which TS uses which account
    {
      if (tsIndex < MAX_TRADE_SERVER_CONNECTIONS)
      {
        if (buff[0] == FIRST_ACCOUNT_ASCII)
        {
          tradingAccount->accountForTS[tsIndex] = FIRST_ACCOUNT;
        }
        else if (buff[0] == SECOND_ACCOUNT_ASCII)
        {
          tradingAccount->accountForTS[tsIndex] = SECOND_ACCOUNT;
        }
        else if (buff[0] == THIRD_ACCOUNT_ASCII)
        {
          tradingAccount->accountForTS[tsIndex] = THIRD_ACCOUNT;
        }
        else
        {
          tradingAccount->accountForTS[tsIndex] = FOURTH_ACCOUNT;
        }

        tsIndex ++;
        
        if (tsIndex == MAX_TRADE_SERVER_CONNECTIONS)
        {
          tsIndex = 0;
          iSection++;
        }
        
        continue;
      }
    }
    else if (iSection == 1) // Accounts for ARCA DIRECT
    {
      if (accountIndex < MAX_ACCOUNT)
      {       
        strcpy(tradingAccount->DIRECT[accountIndex], buff);
      
        accountIndex ++;
        
        if (accountIndex == MAX_ACCOUNT)
        {
          accountIndex = 0;
          iSection++;
        }
        
        continue;
      }
    }
    else if (iSection == 2) // Accounts for NASDAQ RASH
    {
      if (accountIndex < MAX_ACCOUNT)
      {       
        strcpy(tradingAccount->RASH[accountIndex], buff);
        int offset = strlen(tradingAccount->RASH[accountIndex]);
        offset -= 2;
        tradingAccount->RASHToken[accountIndex][0] = tradingAccount->RASH[accountIndex][offset];
        tradingAccount->RASHToken[accountIndex][1] = tradingAccount->RASH[accountIndex][offset + 1];
        tradingAccount->RASHToken[accountIndex][2] = 0;
      
        accountIndex ++;
        
        if (accountIndex == MAX_ACCOUNT)
        {
          accountIndex = 0;
          iSection++;
        }
        
        continue;
      }
    }
    else if (iSection == 3) // Accounts for NASDAQ OUCH
    {
      Lrc_Split(buff, ';', &parameterList);
      // account format:
      // 1. OUCH account
      // 2. OUCH BX account
      if (parameterList.countParameters != 2)
      {
        TraceLog(ERROR_LEVEL, "File 'trading_account' has invalid line format\n");
        fclose(fileDesc);
        return ERROR;
      }
      if (accountIndex < MAX_ACCOUNT)
      {       
        strcpy(tradingAccount->OUCH[accountIndex], parameterList.parameterList[0]);
        int offset = strlen(tradingAccount->OUCH[accountIndex]);
        offset -= 2;
        tradingAccount->OUCHToken[accountIndex][0] = tradingAccount->OUCH[accountIndex][offset];
        tradingAccount->OUCHToken[accountIndex][1] = tradingAccount->OUCH[accountIndex][offset + 1];
        tradingAccount->OUCHToken[accountIndex][2] = 0;
        
        strcpy(tradingAccount->OUBX[accountIndex], parameterList.parameterList[1]);
        offset = strlen(tradingAccount->OUBX[accountIndex]);
        offset -= 2;
        tradingAccount->OUBXToken[accountIndex][0] = tradingAccount->OUBX[accountIndex][offset];
        tradingAccount->OUBXToken[accountIndex][1] = tradingAccount->OUBX[accountIndex][offset + 1];
        tradingAccount->OUBXToken[accountIndex][2] = 0;
      
        accountIndex ++;
        
        if (accountIndex == MAX_ACCOUNT)
        {
          accountIndex = 0;
          iSection++;
        }
        
        continue;
      }
    }
    else if (iSection == 4) // Accounts for NYSE CCG
    {
      if (accountIndex < MAX_ACCOUNT)
      {       
        strcpy(tradingAccount->CCG[accountIndex], buff);
      
        accountIndex ++;
        
        if (accountIndex == MAX_ACCOUNT)
        {
          accountIndex = 0;
          iSection++;
        }
        
        continue;
      }
    }
    else if (iSection == 5) // Accounts for BATS-Z BOE
    {
      if (accountIndex < MAX_ACCOUNT)
      {       
        strcpy(tradingAccount->BATSZBOE[accountIndex], buff);
      
        accountIndex ++;
        
        if (accountIndex == MAX_ACCOUNT)
        {
          accountIndex = 0;
          iSection++;
        }
        
        continue;
      }
    }
    else if (iSection == 6) // Accounts for EDGX/EDGA DIRECT
    {
      Lrc_Split(buff, ';', &parameterList);
      // account format:
      // 1. EDGX account
      // 2. EDGA account
      if (parameterList.countParameters != 2)
      {
        TraceLog(ERROR_LEVEL, "File 'trading_account' has invalid line format\n");
        fclose(fileDesc);
        return ERROR;
      }
      if (accountIndex < MAX_ACCOUNT)
      {       
        strcpy(tradingAccount->EDGX[accountIndex], parameterList.parameterList[0]);
        int offset = strlen(tradingAccount->EDGX[accountIndex]);
        offset -= 2;
        tradingAccount->EDGXToken[accountIndex][0] = tradingAccount->EDGX[accountIndex][offset];
        tradingAccount->EDGXToken[accountIndex][1] = tradingAccount->EDGX[accountIndex][offset + 1];
        tradingAccount->EDGXToken[accountIndex][2] = 0;
        
        strcpy(tradingAccount->EDGA[accountIndex], parameterList.parameterList[1]);
        offset = strlen(tradingAccount->EDGA[accountIndex]);
        offset -= 2;
        tradingAccount->EDGAToken[accountIndex][0] = tradingAccount->EDGA[accountIndex][offset];
        tradingAccount->EDGAToken[accountIndex][1] = tradingAccount->EDGA[accountIndex][offset + 1];
        tradingAccount->EDGAToken[accountIndex][2] = 0;
        
        accountIndex ++;
        
        if (accountIndex == MAX_ACCOUNT)
        {
          accountIndex = 0;
          iSection++;
        }
        
        continue;
      }
    }
    else if (iSection == 7) //ARCA DIRECT Login for Trade Servers
    {
      if (tsIndex < MAX_TRADE_SERVER_CONNECTIONS)
      {
        strncpy(tradingAccount->DIRECTLoginTS[tsIndex], buff, MAX_ACCOUNT_LOGIN_LEN);
        tsIndex ++;
        
        if (tsIndex == MAX_TRADE_SERVER_CONNECTIONS)
        {
          tsIndex = 0;
          iSection++;
        }
        continue;
      }
    }
    else if (iSection == 8) //OUCH Login for Trade Servers
    {
      Lrc_Split(buff, ';', &parameterList);
      // account format:
      // 1. OUCH account
      // 2. OUCH BX account
      if (parameterList.countParameters != 2)
      {
        TraceLog(ERROR_LEVEL, "File 'trading_account' has invalid line format\n");
        fclose(fileDesc);
        return ERROR;
      }
      
      if (tsIndex < MAX_TRADE_SERVER_CONNECTIONS)
      {
        strncpy(tradingAccount->OUCHLoginTS[tsIndex], parameterList.parameterList[0], MAX_ACCOUNT_LOGIN_LEN);
        strncpy(tradingAccount->OUBXLoginTS[tsIndex], parameterList.parameterList[1], MAX_ACCOUNT_LOGIN_LEN);
        tsIndex ++;
        
        if (tsIndex == MAX_TRADE_SERVER_CONNECTIONS)
        {
          tsIndex = 0;
          iSection++;
        }
        continue;
      }
    }
    else if (iSection == 9) //RASH Login for Trade Servers
    {
      if (tsIndex < MAX_TRADE_SERVER_CONNECTIONS)
      {
        strncpy(tradingAccount->RASHLoginTS[tsIndex], buff, MAX_ACCOUNT_LOGIN_LEN);
        tsIndex ++;
        
        if (tsIndex == MAX_TRADE_SERVER_CONNECTIONS)
        {
          tsIndex = 0;
          iSection++;
        }
        continue;
      }
    }
    else if (iSection == 10) // NYSE CCG Login for Trade Servers
    {
      if (tsIndex < MAX_TRADE_SERVER_CONNECTIONS)
      {
        strncpy(tradingAccount->CCGLoginTS[tsIndex], buff, MAX_ACCOUNT_LOGIN_LEN);
        tsIndex ++;

        if (tsIndex == MAX_TRADE_SERVER_CONNECTIONS)
        {
          tsIndex = 0;
          iSection++;
        }
        continue;
      }
    }
    else if (iSection == 11) // BATS-Z BOE Login for Trade Servers
    {
      if (tsIndex < MAX_TRADE_SERVER_CONNECTIONS)
      {
        strncpy(tradingAccount->BATSZBOELoginTS[tsIndex], buff, MAX_ACCOUNT_LOGIN_LEN);
        tsIndex ++;

        if (tsIndex == MAX_TRADE_SERVER_CONNECTIONS)
        {
          tsIndex = 0;
          iSection++;
        }
        continue;
      }
    }
    else if (iSection == 12) // EDGX DIRECT Login for Trade Servers
    {
      Lrc_Split(buff, ';', &parameterList);
      // account format:
      // 1. EDGA account
      // 2. EDGX account
      if (parameterList.countParameters != 2)
      {
        TraceLog(ERROR_LEVEL, "File 'trading_account' has invalid line format\n");
        fclose(fileDesc);
        return ERROR;
      }
      
      if (tsIndex < MAX_TRADE_SERVER_CONNECTIONS)
      {
        strncpy(tradingAccount->EDGXLoginTS[tsIndex], parameterList.parameterList[0], MAX_ACCOUNT_LOGIN_LEN);
        strncpy(tradingAccount->EDGALoginTS[tsIndex], parameterList.parameterList[1], MAX_ACCOUNT_LOGIN_LEN);
        tsIndex ++;

        if (tsIndex == MAX_TRADE_SERVER_CONNECTIONS)
        {
          tsIndex = 0;
          iSection++;
        }
        continue;
      }
    }
    else if (iSection == 13)  //RASH Login for AS
    {
      strncpy(tradingAccount->RASHLoginAS, buff, MAX_ACCOUNT_LOGIN_LEN);
    }
    else if (iSection == 14)  //RASH Login for PM
    {
      strncpy(tradingAccount->RASHLoginPM, buff, MAX_ACCOUNT_LOGIN_LEN);
    }
    else if (iSection == 15)  //ARCA DIRECT Login for AS
    {
      strncpy(tradingAccount->ARCADIRECTLoginAS, buff, MAX_ACCOUNT_LOGIN_LEN);
    }
    else if (iSection == 16)  //ARCA DIRECT Login for PM
    {
      strncpy(tradingAccount->ARCADIRECTLoginPM, buff, MAX_ACCOUNT_LOGIN_LEN);
    }
    else if (iSection == 17) // Accounts for BYX BOE
    {
      if (accountIndex < MAX_ACCOUNT)
      {       
        strcpy(tradingAccount->BYXBOE[accountIndex], buff);
      
        accountIndex ++;
        
        if (accountIndex == MAX_ACCOUNT)
        {
          accountIndex = 0;
          iSection++;
        }
        
        continue;
      }
    }
    else if (iSection == 18) // BYX BOE Login for Trade Servers
    {
      if (tsIndex < MAX_TRADE_SERVER_CONNECTIONS)
      {
        strncpy(tradingAccount->BYXBOELoginTS[tsIndex], buff, MAX_ACCOUNT_LOGIN_LEN);
        tsIndex ++;

        if (tsIndex == MAX_TRADE_SERVER_CONNECTIONS)
        {
          tsIndex = 0;
          iSection++;
        }
        continue;
      }
    }
    else if (iSection == 19) // Accounts for PSX
    {
      if (accountIndex < MAX_ACCOUNT)
      {       
        strcpy(tradingAccount->PSX[accountIndex], buff);
        int offset = strlen(tradingAccount->PSX[accountIndex]);
        offset -= 2;
        tradingAccount->PSXToken[accountIndex][0] = tradingAccount->PSX[accountIndex][offset];
        tradingAccount->PSXToken[accountIndex][1] = tradingAccount->PSX[accountIndex][offset + 1];
        tradingAccount->PSXToken[accountIndex][2] = 0;
      
        accountIndex ++;
        
        if (accountIndex == MAX_ACCOUNT)
        {
          accountIndex = 0;
          iSection++;
        }
        
        continue;
      }
    }
    else if (iSection == 20)
    {
      if (tsIndex < MAX_TRADE_SERVER_CONNECTIONS)
      {
        strncpy(tradingAccount->PSXLoginTS[tsIndex], buff, MAX_ACCOUNT_LOGIN_LEN);
        tsIndex ++;
        
        if (tsIndex == MAX_TRADE_SERVER_CONNECTIONS)
        {
          tsIndex = 0;
          iSection++;
        }
        continue;
      }
    }
    else
    {
      // Close file to release resources
      fclose(fileDesc);
      TraceLog(ERROR_LEVEL, "%s has invalid format! (Section: %d, line: %d, content: %s)\n", fullPath, iSection, iLine, buff);
      return ERROR;
    }
    
    iSection++;
  }

  if (iSection != 21)
  {
    fclose(fileDesc);
    TraceLog(ERROR_LEVEL, "%s has invalid format!\n", fullPath);
    return ERROR;
  }

  // Close file to release resources
  fclose(fileDesc);

  TraceLog(DEBUG_LEVEL, "First trading account for Arca DIRECT: %s\n", tradingAccount->DIRECT[FIRST_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Second trading account for Arca DIRECT: %s\n", tradingAccount->DIRECT[SECOND_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Third trading account for Arca DIRECT: %s\n", tradingAccount->DIRECT[THIRD_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Fourth trading account for Arca DIRECT: %s\n", tradingAccount->DIRECT[FOURTH_ACCOUNT]);

  TraceLog(DEBUG_LEVEL, "First trading account for RASH: %s\n", tradingAccount->RASH[FIRST_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Second trading account for RASH: %s\n", tradingAccount->RASH[SECOND_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Third trading account for RASH: %s\n", tradingAccount->RASH[THIRD_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Fourth trading account for RASH: %s\n", tradingAccount->RASH[FOURTH_ACCOUNT]);

  TraceLog(DEBUG_LEVEL, "First trading account for OUCH: %s\n", tradingAccount->OUCH[FIRST_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Second trading account for OUCH: %s\n", tradingAccount->OUCH[SECOND_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Third trading account for OUCH: %s\n", tradingAccount->OUCH[THIRD_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Fourth trading account for OUCH: %s\n", tradingAccount->OUCH[FOURTH_ACCOUNT]);

  TraceLog(DEBUG_LEVEL, "First tradding account for RASH Token: %s\n", tradingAccount->RASHToken[FIRST_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Second tradding account for RASH Token: %s\n", tradingAccount->RASHToken[SECOND_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Third trading account for RASH Token: %s\n", tradingAccount->RASHToken[THIRD_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Fourth trading account for RASH Token: %s\n", tradingAccount->RASHToken[FOURTH_ACCOUNT]);

  TraceLog(DEBUG_LEVEL, "First tradding account for OUCH Token: %s\n", tradingAccount->OUCHToken[FIRST_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Second tradding account for OUCH Token: %s\n", tradingAccount->OUCHToken[SECOND_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Third trading account for OUCH Token: %s\n", tradingAccount->OUCHToken[THIRD_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Fourth trading account for OUCH Token: %s\n", tradingAccount->OUCHToken[FOURTH_ACCOUNT]);

  TraceLog(DEBUG_LEVEL, "First tradding account for BX Token: %s\n", tradingAccount->OUBXToken[FIRST_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Second tradding account for BX Token: %s\n", tradingAccount->OUBXToken[SECOND_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Third trading account for BX Token: %s\n", tradingAccount->OUBXToken[THIRD_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Fourth trading account for BX Token: %s\n", tradingAccount->OUBXToken[FOURTH_ACCOUNT]);

  TraceLog(DEBUG_LEVEL, "First trading account for NYSE CCG: %s\n", tradingAccount->CCG[FIRST_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Second trading account for NYSE CCG: %s\n", tradingAccount->CCG[SECOND_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Third trading account for NYSE CCG: %s\n", tradingAccount->CCG[THIRD_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Fourth trading account for NYSE CCG: %s\n", tradingAccount->CCG[FOURTH_ACCOUNT]);

  TraceLog(DEBUG_LEVEL, "First trading account for BATSZ BOE: %s\n", tradingAccount->BATSZBOE[FIRST_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Second trading account for BATSZ BOE: %s\n", tradingAccount->BATSZBOE[SECOND_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Third trading account for BATSZ BOE: %s\n", tradingAccount->BATSZBOE[THIRD_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Fourth trading account for BATSZ BOE: %s\n", tradingAccount->BATSZBOE[FOURTH_ACCOUNT]);
  
  TraceLog(DEBUG_LEVEL, "First trading account for BYX BOE: %s\n", tradingAccount->BYXBOE[FIRST_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Second trading account for BYX BOE: %s\n", tradingAccount->BYXBOE[SECOND_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Third trading account for BYX BOE: %s\n", tradingAccount->BYXBOE[THIRD_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Fourth trading account for BYX BOE: %s\n", tradingAccount->BYXBOE[FOURTH_ACCOUNT]);

  TraceLog(DEBUG_LEVEL, "First trading account for EDGX: %s\n", tradingAccount->EDGX[FIRST_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Second trading account for EDGX: %s\n", tradingAccount->EDGX[SECOND_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Third trading account for EDGX: %s\n", tradingAccount->EDGX[THIRD_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Fourth trading account for EDGX: %s\n", tradingAccount->EDGX[FOURTH_ACCOUNT]);

  TraceLog(DEBUG_LEVEL, "First tradding account for EDGX Token: %s\n", tradingAccount->EDGXToken[FIRST_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Second tradding account for EDGX Token: %s\n", tradingAccount->EDGXToken[SECOND_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Third trading account for EDGX Token: %s\n", tradingAccount->EDGXToken[THIRD_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Fourth trading account for EDGX Token: %s\n", tradingAccount->EDGXToken[FOURTH_ACCOUNT]);

  TraceLog(DEBUG_LEVEL, "First trading account for OUCH BX: %s\n", tradingAccount->OUBX[FIRST_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Second trading account for OUCH BX: %s\n", tradingAccount->OUBX[SECOND_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Third trading account for OUCH BX: %s\n", tradingAccount->OUBX[THIRD_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Fourth trading account for OUCH BX: %s\n", tradingAccount->OUBX[FOURTH_ACCOUNT]);
  
  TraceLog(DEBUG_LEVEL, "First trading account for EDGA: %s\n", tradingAccount->EDGA[FIRST_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Second trading account for EDGA: %s\n", tradingAccount->EDGA[SECOND_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Third trading account for EDGA: %s\n", tradingAccount->EDGA[THIRD_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Fourth trading account for EDGA: %s\n", tradingAccount->EDGA[FOURTH_ACCOUNT]);

  TraceLog(DEBUG_LEVEL, "First tradding account for EDGA Token: %s\n", tradingAccount->EDGAToken[FIRST_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Second tradding account for EDGA Token: %s\n", tradingAccount->EDGAToken[SECOND_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Third trading account for EDGA Token: %s\n", tradingAccount->EDGAToken[THIRD_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Fourth trading account for EDGA Token: %s\n", tradingAccount->EDGAToken[FOURTH_ACCOUNT]);
  
  TraceLog(DEBUG_LEVEL, "First trading account for PSX: %s\n", tradingAccount->PSX[FIRST_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Second trading account for PSX: %s\n", tradingAccount->PSX[SECOND_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Third trading account for PSX: %s\n", tradingAccount->PSX[THIRD_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Fourth trading account for PSX: %s\n", tradingAccount->PSX[FOURTH_ACCOUNT]);
  
  TraceLog(DEBUG_LEVEL, "First tradding account for PSX Token: %s\n", tradingAccount->PSXToken[FIRST_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Second tradding account for PSX Token: %s\n", tradingAccount->PSXToken[SECOND_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Third trading account for PSX Token: %s\n", tradingAccount->PSXToken[THIRD_ACCOUNT]);
  TraceLog(DEBUG_LEVEL, "Fourth trading account for PSX Token: %s\n", tradingAccount->PSXToken[FOURTH_ACCOUNT]);

  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadFeeConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int LoadFeeConf(void)
{
  // Get full raw data path
  char fullPath[MAX_PATH_LEN] = "\0";
  
  if (GetFullConfigPath("fee_config", CONF_MISC, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Have some problem in get fee_config full path\n");

    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Load Fee config: %s\n", fullPath);

  //Try to open global configuration file
  //Please be aware of the requirements for the configuration file here
  FILE *fileDesc = fopen(fullPath, "r");
  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Cannot open file '%s' to read or we don't have permission to create\n", fullPath);

    return ERROR;
  }
  else
  {
    //Buffer to store each line in the configuration file
    char line[MAX_PATH_LEN];

    //Number of lines read from the configuration file
    int  count = 0;

    //Clear the buffer pointed to by line
    memset(line, 0, MAX_PATH_LEN);

    //Get each line from the configuration file
    while (fgets(line, MAX_PATH_LEN, fileDesc) != NULL) 
    {
      if ((line[0] != '#') && (line[0] > 32))
      {
        count ++;

        if (count == 1)
        {
          FeeConf.secFee = atof(line);
        }
        else if (count == 2)
        {
          FeeConf.tafFee = atof(line);
        }
        else if (count == 3)
        {
          FeeConf.commission = atof(line);
        }
        else if (count == 4)
        {
          FeeConf.addVenueFeeWithPriceLager1 = atof(line);
        }
        else if (count == 5)
        {
          FeeConf.addVenueFeeWithPriceSmaler1 = atof(line);
        }
        else if (count == 6)
        {
          FeeConf.removeVenueFeeWithPriceLager1 = atof(line);
        }
        else if (count == 7)
        {
          FeeConf.removeVenueFeeWithPriceSmaler1 = atof(line);
        }
        else if (count == 8)
        {
          FeeConf.auctionVenueFee = atof(line);
        }
        else if (count == 9)
        {
          FeeConf.arcaRouteVenueFee = atof(line);
        }
        else if (count == 10)
        {
          FeeConf.nasdaqRouteVenueFee = atof(line);
        }
        else if (count == 11)
        {
          FeeConf.bzxRouteVenueFee = atof(line);
        }
        else if (count == 12)
        {
          FeeConf.edgxRouteVenueFee = atof(line);
        }
      }

      //Clear the buffer pointed to by line
      memset(line, 0, MAX_PATH_LEN);
    }

    //Close the file
    fclose(fileDesc);   

    if (count >= 12)
    {
      //Indicate a successful operation
      TraceLog(DEBUG_LEVEL, "Sec Fee = %.10lf\n", FeeConf.secFee);
      TraceLog(DEBUG_LEVEL, "Taf Fee = %lf\n", FeeConf.tafFee);
      TraceLog(DEBUG_LEVEL, "Clearing commission = %lf\n", FeeConf.commission);
      TraceLog(DEBUG_LEVEL, "Add venue fee with price lager than $1 = %lf\n", FeeConf.addVenueFeeWithPriceLager1);
      TraceLog(DEBUG_LEVEL, "Add venue fee with price smaller than $1 = %lf\n", FeeConf.addVenueFeeWithPriceSmaler1);
      TraceLog(DEBUG_LEVEL, "Remove venue fee with price lager than $1 = %lf\n", FeeConf.removeVenueFeeWithPriceLager1);
      TraceLog(DEBUG_LEVEL, "Remove venue fee with price smaller than $1= %lf\n", FeeConf.removeVenueFeeWithPriceSmaler1);
      TraceLog(DEBUG_LEVEL, "Auction venue fee  = %lf\n", FeeConf.auctionVenueFee);
      TraceLog(DEBUG_LEVEL, "ARCA route venue fee = %lf\n", FeeConf.arcaRouteVenueFee);
      TraceLog(DEBUG_LEVEL, "NASDAQ route venue fee = %lf\n", FeeConf.nasdaqRouteVenueFee);
      TraceLog(DEBUG_LEVEL, "BZX route venue fee = %lf\n", FeeConf.bzxRouteVenueFee);
      TraceLog(DEBUG_LEVEL, "EDGX route venue fee = %lf\n", FeeConf.edgxRouteVenueFee);
      return SUCCESS;
    }
    else
    {
      //Configuration file is invalid!
      TraceLog(ERROR_LEVEL, "Invalid configuration file '%s'\n", fullPath);

      return ERROR;
    }
  }
}

/****************************************************************************
- Function name:  LoadAlertConf
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int LoadAlertConf(void)
{
  // Get full raw data path
  char fullPath[MAX_PATH_LEN] = "\0";

  if (GetFullConfigPath(FILE_NAME_OF_ALERT_CONF, CONF_MISC, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Have some problem in get alert_config full path\n");

    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Load alert config: %s\n", fullPath);

  //Try to open global configuration file
  //Please be aware of the requirements for the configuration file here
  FILE *fileDesc = fopen(fullPath, "r");
  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Cannot open file '%s' to read or we don't have permission to create\n", fullPath);

    return ERROR;
  }
  else
  {
    //Buffer to store each line in the configuration file
    char line[MAX_PATH_LEN];

    //Number of lines read from the configuration file
    int  count = 0;

    //Clear the buffer pointed to by line
    memset(line, 0, MAX_PATH_LEN);

    AlertConf.noTradeAlertMgmt.count = 0;

    int mode = 0;
    t_Parameters parameterList;
    //Get each line from the configuration file
    while (fgets(line, MAX_PATH_LEN, fileDesc) != NULL)
    {
      if (line[0] != '#')
      {
        count ++;

        if (count == 1)
        {
          AlertConf.pmExitedPositionThreshhold = atoi(line);
        }
        else if (count == 2)
        {
          AlertConf.consecutiveLossThreshhold = atoi(line);

          if (fge(AlertConf.consecutiveLossThreshhold, traderToolsInfo.globalConf.maxLoser))
          {
            //Configuration file is invalid!
            TraceLog(ERROR_LEVEL, "Consecutive loss of Alert config = %d is not less than global config = %d\n", AlertConf.consecutiveLossThreshhold, traderToolsInfo.globalConf.maxLoser);

            return ERROR;
          }
        }
        else if (count == 3)
        {
          AlertConf.stucksThreshhold = atoi(line);

          if (AlertConf.stucksThreshhold >= traderToolsInfo.globalConf.maxStuck)
          {
            //Configuration file is invalid!
            TraceLog(ERROR_LEVEL, "Alert config = %d is not less than global config = %d\n", AlertConf.stucksThreshhold, traderToolsInfo.globalConf.maxStuck);

            return ERROR;
          }

        }
        else if (count == 4)
        {
          AlertConf.singleServerTradesThreshhold = atoi(line);
        }
        else if (count == 5)
        {
          AlertConf.gtrAmountThreshhold = atof(line);
        }
        else if (count == 6)
        {
          AlertConf.gtrSecondThreshhold = atoi(line);
          if (AlertConf.gtrSecondThreshhold > MAX_GTR_SAMPLE_IN_SECONDS)
          {
            //Configuration file is invalid!
            TraceLog(ERROR_LEVEL, "Invalid duration of GTR change, should not be greater than %d second\n", MAX_GTR_SAMPLE_IN_SECONDS);

            return ERROR;

          }
        }
        else if (count == 7)
        {
          AlertConf.incompleteTradeThreshhold = atoi(line);
        }
        else if (count == 8)
        {
          AlertConf.repetitionTimeThreshhold = atoi(line);
        }
        else if (count == 9)
        {
          AlertConf.positionWithoutOrderThreshhold = atof(line);
        }
        else if (count == 10)
        {
          AlertConf.pmOrderRateThreshhold = atoi(line);
        }
        else if (count == 11)
        {
          AlertConf.buyingPowerAmountThreshhold = atof(line);
        }
        else if (count == 12)
        {
          AlertConf.buyingPowerSecondsThreshhold = atoi(line);
        }
        else
        {
          if(mode == 0)
          {
            mode = 1;
            Lrc_Split(line, ':', &parameterList);

            AlertConf.noTradeAlertMgmt.itemList[AlertConf.noTradeAlertMgmt.count].startInSecond = (atoi(parameterList.parameterList[0])*60 + atoi(parameterList.parameterList[1]))*60;

            if((AlertConf.noTradeAlertMgmt.count >= 1) &&
               (AlertConf.noTradeAlertMgmt.itemList[AlertConf.noTradeAlertMgmt.count].startInSecond <= AlertConf.noTradeAlertMgmt.itemList[AlertConf.noTradeAlertMgmt.count - 1].startInSecond))
            {
              TraceLog(ERROR_LEVEL, "No trade alert time must be ascendent\n");

              return ERROR;
            }
          }
          else
          {
            mode = 0;
            AlertConf.noTradeAlertMgmt.itemList[AlertConf.noTradeAlertMgmt.count++].durationInMinute = atoi(line);
          }
        }
      }

      //Clear the buffer pointed to by line
      memset(line, 0, MAX_PATH_LEN);
    }

    //Close the file
    fclose(fileDesc);

    if (count == (12 + 2*AlertConf.noTradeAlertMgmt.count) &&
       (AlertConf.noTradeAlertMgmt.count >= 1))
    {
      //Indicate a successful operation
      TraceLog(DEBUG_LEVEL, "pmExitedPositionThreshhold = %d\n", AlertConf.pmExitedPositionThreshhold);
      TraceLog(DEBUG_LEVEL, "consecutiveLossThreshhold = %d\n", AlertConf.consecutiveLossThreshhold);
      TraceLog(DEBUG_LEVEL, "stucksThreshhold = %d\n", AlertConf.stucksThreshhold);
      TraceLog(DEBUG_LEVEL, "singleServerTradesThreshhold = %d\n", AlertConf.singleServerTradesThreshhold);
      TraceLog(DEBUG_LEVEL, "GTRAmountThreshhold = %-12.2f\n", AlertConf.gtrAmountThreshhold);
      TraceLog(DEBUG_LEVEL, "GTRSecondThreshhold = %d\n", AlertConf.gtrSecondThreshhold);
      TraceLog(DEBUG_LEVEL, "incompleteTradeThreshhold = %d\n", AlertConf.incompleteTradeThreshhold);
      TraceLog(DEBUG_LEVEL, "repetitionTimeThreshhold = %d\n", AlertConf.repetitionTimeThreshhold);
      TraceLog(DEBUG_LEVEL, "positionWithoutOrderThreshhold = %d\n", AlertConf.positionWithoutOrderThreshhold);
      TraceLog(DEBUG_LEVEL, "pmOrderRateThreshhold = %d\n", AlertConf.pmOrderRateThreshhold);
      TraceLog(DEBUG_LEVEL, "buyingPowerAmountThreshhold = %-12.2f\n", AlertConf.buyingPowerAmountThreshhold);
      TraceLog(DEBUG_LEVEL, "buyingPowerSecondsThreshhold = %d\n", AlertConf.buyingPowerSecondsThreshhold);

      int i = 0;
      int hour, minute;
      for(i = 0; i < AlertConf.noTradeAlertMgmt.count; i++)
      {
        minute = AlertConf.noTradeAlertMgmt.itemList[i].startInSecond/60;
        hour = minute/60;

        minute %= 60;

        TraceLog(DEBUG_LEVEL, "No trade threshold: %02d:%02d duration = %d\n", hour, minute, AlertConf.noTradeAlertMgmt.itemList[i].durationInMinute);
      }
      return SUCCESS;
    }
    else
    {
      //Configuration file is invalid!
      TraceLog(ERROR_LEVEL, "Invalid configuration file '%s'\n", fullPath);

      return ERROR;
    }
  }
}

/****************************************************************************
- Function name:  UpdateTradeTimeAlertConfig
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int UpdateTradeTimeAlertConfig(void)
{
  int i, j, from, to;

  for(i = 0; i < AlertConf.noTradeAlertMgmt.itemList[0].startInSecond; i++)
  {
    TradeTimeAlertConfig[i] = AlertConf.noTradeAlertMgmt.itemList[0].durationInMinute;
  }

  for(i = 0; i < AlertConf.noTradeAlertMgmt.count; i++)
  {
    from = AlertConf.noTradeAlertMgmt.itemList[i].startInSecond;
    if(i == AlertConf.noTradeAlertMgmt.count - 1)
    {
      to = NUMBER_OF_SECONDS_PER_DAY;
    }
    else
    {
      to = AlertConf.noTradeAlertMgmt.itemList[i + 1].startInSecond;
    }

    for(j = from; j < to; j++)
    {
      TradeTimeAlertConfig[j] = AlertConf.noTradeAlertMgmt.itemList[i].durationInMinute;
    }
  }
  
  int tsIndex;
  for (tsIndex = 0; tsIndex < MAX_TRADE_SERVER_CONNECTIONS; tsIndex++)
  {
    NoTradesDurationMgt[tsIndex].alertThresholdInMinute = -1;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SaveAlertConf
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int SaveAlertConf(void)
{
  // Get full raw data path
  char fullPath[MAX_PATH_LEN] = "\0";

  if (GetFullConfigPath(FILE_NAME_OF_ALERT_CONF, CONF_MISC, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Have some problem in get %s full path\n", FILE_NAME_OF_ALERT_CONF);

    return ERROR;
  }
  TraceLog(DEBUG_LEVEL, "Set alert config of Aggregation Server: %s\n", fullPath);

  TraceLog(DEBUG_LEVEL, "pmExitedPositionThreshhold = %d\n", AlertConf.pmExitedPositionThreshhold);
  TraceLog(DEBUG_LEVEL, "consecutiveLossThreshhold = %d\n", AlertConf.consecutiveLossThreshhold);
  TraceLog(DEBUG_LEVEL, "stucksThreshhold = %d\n", AlertConf.stucksThreshhold);
  TraceLog(DEBUG_LEVEL, "singleServerTradesThreshhold = %d\n", AlertConf.singleServerTradesThreshhold);
  TraceLog(DEBUG_LEVEL, "GTRAmountThreshhold = %-12.2f\n", AlertConf.gtrAmountThreshhold);
  TraceLog(DEBUG_LEVEL, "GTRSecondThreshhold = %d\n", AlertConf.gtrSecondThreshhold);
  TraceLog(DEBUG_LEVEL, "incompleteTradeThreshhold = %d\n", AlertConf.incompleteTradeThreshhold);
  TraceLog(DEBUG_LEVEL, "repetitionTimeThreshhold = %d\n", AlertConf.repetitionTimeThreshhold);
  TraceLog(DEBUG_LEVEL, "positionWithoutOrderThreshhold = %d\n", AlertConf.positionWithoutOrderThreshhold);
  TraceLog(DEBUG_LEVEL, "pmOrderRateThreshhold = %d\n", AlertConf.pmOrderRateThreshhold);
  TraceLog(DEBUG_LEVEL, "buyingPowerAmountThreshhold = %-12.2f\n", AlertConf.buyingPowerAmountThreshhold);
  TraceLog(DEBUG_LEVEL, "buyingPowerSecondsThreshhold = %d\n", AlertConf.buyingPowerSecondsThreshhold);

  int i = 0;
  int hour, minute;
  for(i = 0; i < AlertConf.noTradeAlertMgmt.count; i++)
  {
    minute = AlertConf.noTradeAlertMgmt.itemList[i].startInSecond/60;
    hour = minute/60;

    minute %= 60;

    TraceLog(DEBUG_LEVEL, "No trade threshold: %02d:%02d duration = %d\n", hour, minute, AlertConf.noTradeAlertMgmt.itemList[i].durationInMinute);
  }


  //Try to open global configuration file
  //Please be aware of the requirements for the configuration file here
  FILE *fileDesc = fopen(fullPath, "w+");
  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Cannot open file '%s' to read or we don't have permission to create\n", fullPath);

    return ERROR;
  }
  else
  {
    fprintf(fileDesc, "#Number of seconds PM hasn�t exited position\n");
    fprintf(fileDesc, "%d\n", AlertConf.pmExitedPositionThreshhold);

    fprintf(fileDesc, "#Consecutive Loss - nearing configured limit\n");
    fprintf(fileDesc, "%d\n", AlertConf.consecutiveLossThreshhold);

    fprintf(fileDesc, "#Stucks - nearing configured limit\n");
    fprintf(fileDesc, "%d\n", AlertConf.stucksThreshhold);

    fprintf(fileDesc, "#Single Server Trades Consecutive on different symbol\n");
    fprintf(fileDesc, "%d\n", AlertConf.singleServerTradesThreshhold);

    fprintf(fileDesc, "#GTR change (Gross Trading Revenue)\n");
    fprintf(fileDesc, "#$\n");
    fprintf(fileDesc, "%lf\n", AlertConf.gtrAmountThreshhold);
    fprintf(fileDesc, "#per x seconds\n");
    fprintf(fileDesc, "%d\n", AlertConf.gtrSecondThreshhold);

    fprintf(fileDesc, "#incomplete trade in second\n");
    fprintf(fileDesc, "%d\n", AlertConf.incompleteTradeThreshhold);
    
    fprintf(fileDesc, "#repetition time in second\n");
    fprintf(fileDesc, "%d\n", AlertConf.repetitionTimeThreshhold);

    fprintf(fileDesc, "#Unhandled open position - has been without an order for X seconds\n");
    fprintf(fileDesc, "%d\n", AlertConf.positionWithoutOrderThreshhold);

    fprintf(fileDesc, "#PM Order Rate is higher than x orders per second\n");
    fprintf(fileDesc, "%d\n", AlertConf.pmOrderRateThreshhold);

    fprintf(fileDesc, "#Buying power drops below threshold for some time\n");
    fprintf(fileDesc, "#$\n");
    fprintf(fileDesc, "%lf\n", AlertConf.buyingPowerAmountThreshhold);

    fprintf(fileDesc, "#per x seconds\n");
    fprintf(fileDesc, "%d\n", AlertConf.buyingPowerSecondsThreshhold);

    fprintf(fileDesc, "#No trades during configured time (minutes) period\n");

    int i = 0;

    for(i = 0; i < AlertConf.noTradeAlertMgmt.count; i++)
    {
      minute = AlertConf.noTradeAlertMgmt.itemList[i].startInSecond/60;
      hour = minute/60;

      minute %= 60;

      fprintf(fileDesc, "#Period %d\n", i + 1);
      fprintf(fileDesc, "%02d:%02d\n", hour, minute);
      fprintf(fileDesc, "%d\n", AlertConf.noTradeAlertMgmt.itemList[i].durationInMinute);
    }

    fflush(fileDesc);

    //Close the file
    fclose(fileDesc);

    TraceLog(DEBUG_LEVEL, "Alert configuration is saved successfully!\n");

    return SUCCESS;
  }
}

/****************************************************************************
- Function name:  LoadVolatileSectionsFromFileToLists
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int LoadVolatileSectionsFromFileToLists(char *preList, int *countPre, char *postList, int *countPost, char *intradayList, int *countIntraday)
{
  char fullPath[MAX_PATH_LEN];
  if (GetFullRawDataPath(VOLATILE_SECTION_FILE_NAME, fullPath) == NULL_POINTER)
  {
    return ERROR;
  }
  
  FILE *fileDesc = fopen(fullPath, "r");
  
  if (fileDesc == NULL)
  {
    if (ENOENT == errno)  //File does not exist
    {
      *countPre = 0;
      *countPost = 0;
      *countIntraday = 0;
      
      //We will create the file
      fileDesc = fopen(fullPath, "w");
      if (fileDesc == NULL)
      {
        TraceLog(ERROR_LEVEL, "problem in creating volatile sections file\n");
        PrintErrStr(ERROR_LEVEL, "Calling fopen(): ", errno);
        return ERROR;
      }
      
      fclose(fileDesc);
      return SUCCESS;
    }
    else
    {
      TraceLog(ERROR_LEVEL, "problem in opening volatile sections file\n");
      PrintErrStr(ERROR_LEVEL, "Calling fopen(): ", errno);
      return ERROR;
    }
  }
  
  char line[80];
  int sectionIndex = -1;      /* Current reading-section in the file:
                    -1: Has not read any section, or reading invalid section
                     1: Reading PRE section
                     2: Reading POST section
                     3: Reading INTRADAY section
                  */
  
  // ---------------------------------------------------------------------------------------------
  //  Start reading sections in the file
  // ---------------------------------------------------------------------------------------------
  memset(line, 0, 80);
  while(fgets(line, 80, fileDesc) != NULL)
  {
    RemoveCharacters(line, strlen(line));           //Remove trailing un-used chars (1 to 32)
    if (strlen(line) == 0)                    //empty line
    {
      memset(line, 0, 80);
      continue;
    }
    
    if (line[0] == '#')                     //comment
    {
      memset(line, 0, 80);
      continue;
    }
    
    if (line[0] == '[')                     //might start of a section
    {
      if ((strlen(line) >= 5) && (strcmp(line, "[PRE]") == 0))
        sectionIndex = 1;
      else if ((strlen(line) >= 6) && (strcmp(line, "[POST]") == 0))
        sectionIndex = 2;
      else if ((strlen(line) >= 10) && (strcmp(line, "[INTRADAY]") == 0))
        sectionIndex = 3;
      else
        sectionIndex = -1;
      
      memset(line, 0, 80);
      continue;
    }
    
    if ((line[0] >= 'A') && (line[0] <= 'Z'))         //symbol
    {
      if (sectionIndex == -1)
      {
        memset(line, 0, 80);
        continue;
      }
      
      MappingStockSymbol(line);               // Mapping stock symbol: '$' -> '-'

      if (strlen(line) > SYMBOL_LEN)
      {
        memset(line, 0, 80);
        continue;
      }
      
      switch (sectionIndex)
      {
        case 1:
          strncpy(&preList[*countPre * SYMBOL_LEN], line, SYMBOL_LEN);
          *countPre = (*countPre + 1);
          break;
        case 2:
          strncpy(&postList[*countPost * SYMBOL_LEN], line, SYMBOL_LEN);
          *countPost = (*countPost + 1);
          break;
        case 3:
          strncpy(&intradayList[*countIntraday * SYMBOL_LEN], line, SYMBOL_LEN);
          *countIntraday = (*countIntraday + 1);
          break;
      }
    }
    else                            //invalid line
    {
      TraceLog(ERROR_LEVEL, "(Volatile sections file): Invalid symbol %s\n", line);
    }

    memset(line, 0, 80);
  }
  
  fclose (fileDesc);
  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadAllVolatileToggleSchedulesFromFile
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int LoadAllVolatileToggleSchedulesFromFile()
{
  volatileMgmt.numAllVolatileToggle = 0;
  
  char fullPath[MAX_PATH_LEN];
  if (GetFullConfigPath(ALL_VOLATILE_SCHEDULE_FILE_NAME, CONF_SYMBOL, fullPath) == NULL_POINTER)
  {
    return ERROR;
  }
  
  FILE *fileDesc = fopen(fullPath, "r");
  
  if (fileDesc == NULL)
  {
    if (ENOENT == errno)  //File does not exist
    {
      return SUCCESS;
    }
    else
    {
      TraceLog(ERROR_LEVEL, "problem in opening All-Volatile-Toggle Schedule file\n");
      PrintErrStr(ERROR_LEVEL, "Calling fopen(): ", errno);
      return ERROR;
    }
  }
  
  char line[80];
  memset(line, 0, 80);
  
  int onHr, onMin, onSec, offHr, offMin, offSec;
  while(fgets(line, 80, fileDesc) != NULL)
  {
    RemoveCharacters(line, strlen(line));           //Remove trailing un-used chars (1 to 32)
    if (strlen(line) == 0)                    //empty line
    {
      memset(line, 0, 80);
      continue;
    }
    
    if (line[0] == '#')                     //comment
    {
      memset(line, 0, 80);
      continue;
    }
    
    //line sample: 08:33:00-08:36:00
    if (strlen(line) == 17)
    {
      if ((line[2] == ':') &&
        (line[5] == ':') &&
        (line[8] == '-') &&
        (line[11] == ':') &&
        (line[14] == ':'))
      {
        sscanf(line, "%d:%d:%d-%d:%d:%d", &onHr, &onMin, &onSec, &offHr, &offMin, &offSec);
        volatileMgmt.allVolatileOnTime[volatileMgmt.numAllVolatileToggle] = onSec + onMin*60 + onHr * 3600;
        volatileMgmt.allVolatileOffTime[volatileMgmt.numAllVolatileToggle] = offSec + offMin*60 + offHr * 3600;
        volatileMgmt.numAllVolatileToggle++;
      }
      else
      {
        TraceLog(ERROR_LEVEL, "All-Volatile-Toggle Schedule file has invalid line format, line: %s\n", line);
        volatileMgmt.numAllVolatileToggle = 0;
        fclose (fileDesc);
        return ERROR;
      }
    }
    else
    {
      TraceLog(ERROR_LEVEL, "All-Volatile-Toggle Schedule file has invalid line format, line: %s\n", line);
      volatileMgmt.numAllVolatileToggle = 0;
      fclose (fileDesc);
      return ERROR;
    }
    
    memset(line, 0, 80);
  }
  
  fclose (fileDesc);
  return SUCCESS;
}

/****************************************************************************
- Function name:  SaveVolatileSectionsToFile
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int SaveVolatileSectionsToFile(char *preList, int countPre, char *postList, int countPost, char *intrdayList, int countIntraday)
{
  char fullPath[MAX_PATH_LEN];
  if (GetFullRawDataPath(VOLATILE_SECTION_FILE_NAME, fullPath) == NULL_POINTER)
  {
    return ERROR;
  }

  FILE *fp = fopen(fullPath, "w+");
  if (fp == NULL)
  {
    TraceLog(ERROR_LEVEL, "Could not open %s for writing\n", fullPath);
    return ERROR;
  }
  
  int index;
  
  fprintf(fp, "[PRE]\n");
  for (index = 0; index < countPre; index++)
  {
    if (preList[index * SYMBOL_LEN] > 32)   //Only save valid symbol
    {
      fprintf(fp, "%.8s\n", &preList[index * SYMBOL_LEN]);
    }
  }
  
  fprintf(fp, "[POST]\n");
  for (index = 0; index < countPost; index++)
  {
    if (postList[index * SYMBOL_LEN] > 32)    //Only save invalid symbol
    {
      fprintf(fp, "%.8s\n", &postList[index * SYMBOL_LEN]);
    }
  }
  
  fprintf(fp, "[INTRADAY]\n");
  for (index = 0; index < countIntraday; index++)
  {
    if (intrdayList[index * SYMBOL_LEN] > 32) //Only save invalid symbol
    {
      fprintf(fp, "%.8s\n", &intrdayList[index * SYMBOL_LEN]);
    }
  }
  
  fflush(fp);
  fclose(fp);

  return SUCCESS;
}

/****************************************************************************
- Function name:  SaveAllVolatileToggleSchedulesToFile
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int SaveAllVolatileToggleSchedulesToFile()
{
  char fullPath[MAX_PATH_LEN];
  if (GetFullConfigPath(ALL_VOLATILE_SCHEDULE_FILE_NAME, CONF_SYMBOL, fullPath) == NULL_POINTER)
  {
    return ERROR;
  }

  FILE *fp = fopen(fullPath, "w+");
  if (fp == NULL)
  {
    TraceLog(ERROR_LEVEL, "Could not open %s for writing\n", fullPath);
    return ERROR;
  }
  
  int i;
  
  for (i = 0; i < volatileMgmt.numAllVolatileToggle; i++)
  {
    fprintf(fp, "%02d:%02d:%02d-%02d:%02d:%02d\n", volatileMgmt.allVolatileOnTime[i]/3600,
                            (volatileMgmt.allVolatileOnTime[i]%3600)/60, 
                            volatileMgmt.allVolatileOnTime[i] % 60,
                              volatileMgmt.allVolatileOffTime[i]/3600, 
                              (volatileMgmt.allVolatileOffTime[i]%3600)/60, 
                              volatileMgmt.allVolatileOffTime[i] % 60);
  }
  
  fflush(fp);
  fclose(fp);

  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadVolatileSymbolsFromFileToList
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int LoadVolatileSymbolsFromFileToList(char *list, int *count)
{
  *count = 0;
  
  char fullPath[MAX_PATH_LEN];
  if (GetFullConfigPath(VOLATILE_SYMBOL_FILE_NAME, CONF_SYMBOL, fullPath) == NULL_POINTER)
  {
    return ERROR;
  }
  
  FILE *fileDesc = NULL;
  fileDesc = fopen(fullPath, "r");
  
  if (fileDesc == NULL)
  {
    if (ENOENT == errno)  //File does not exist
    {
      //We will create the file
      fileDesc = fopen(fullPath, "w");
      if (fileDesc == NULL)
      {
        TraceLog(ERROR_LEVEL, "problem in creating volatile symbol list file\n");
        PrintErrStr(ERROR_LEVEL, "Calling fopen(): ", errno);
        return ERROR;
      }
      
      fclose(fileDesc);
      return SUCCESS;
    }
    else
    {
      TraceLog(ERROR_LEVEL, "problem in opening volatile symbol list file\n");
      PrintErrStr(ERROR_LEVEL, "Calling fopen(): ", errno);
      return ERROR;
    }
  }
  
  int listOffset = 0;
  char symbol[80];
  memset(symbol, 0, 80);
  
  while(fgets(symbol, 80, fileDesc) != NULL)
  {
    RemoveCharacters(symbol, strlen(symbol));           //Remove trailing un-used chars (1 to 32)
    if (strlen(symbol) == 0)                    //empty line
    {
      memset(symbol, 0, 80);
      continue;
    }
    
    if (symbol[0] == '#')                     //comment
    {
      memset(symbol, 0, 80);
      continue;
    }
    
    // First char >= 'A' and <= 'Z'
    if ((symbol[0] >= 'A') && (symbol[0] <= 'Z'))
    {
      // Mapping stock symbol: '$' -> '-'
      MappingStockSymbol(symbol);

      if (strlen(symbol) > SYMBOL_LEN)
      {
        TraceLog(ERROR_LEVEL, "(Volatile symbol list file): Invalid symbol '%s'\n", symbol);
        memset(symbol, 0, 80);
        continue;
      }
      
      //Add to list
      strncpy(&list[listOffset], symbol, SYMBOL_LEN);
      listOffset += SYMBOL_LEN;
      *count = *count + 1;
    }
    else
    {
      TraceLog(ERROR_LEVEL, "(Volatile symbol list file): Invalid symbol '%s'\n", symbol);
    }

    memset(symbol, 0, 80);
  }
  
  fclose (fileDesc);

  return SUCCESS;
}

/****************************************************************************
- Function name:  SaveVolatileSymbolListToFile
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int SaveVolatileSymbolListToFile(char *list, int count)
{
  char fullPath[MAX_PATH_LEN];
  if (GetFullConfigPath(VOLATILE_SYMBOL_FILE_NAME, CONF_SYMBOL, fullPath) == NULL_POINTER)
  {
    return ERROR;
  }
  
  FILE *fp = fopen(fullPath, "w+");
  if (fp == NULL)
  {
    TraceLog(ERROR_LEVEL, "Could not open %s for writing\n", fullPath);
    return ERROR;
  }
  
  int index;
  for (index = 0; index < count; index++)
  {
    if (list[index * SYMBOL_LEN] > 32)    //Only save valid symbol
    {
      fprintf(fp, "%.8s\n", &list[index * SYMBOL_LEN]);
    }
  }
  
  fflush(fp);
  fclose(fp);

  return SUCCESS;
}

/****************************************************************************
- Function name:  GetActiveVolatileSymbols
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int GetActiveVolatileSymbols(char *activeList)
{
  int countPre = 0, countPost = 0, countIntraday = 0, countActive = 0;
  char preList[MAX_STOCK_SYMBOL * SYMBOL_LEN];
  char postList[MAX_STOCK_SYMBOL * SYMBOL_LEN];
  char intradayList[MAX_STOCK_SYMBOL * SYMBOL_LEN];
  
  pthread_mutex_lock(&volatileMgmt.mutexLock);
    LoadVolatileSectionsFromFileToLists(preList, &countPre, postList, &countPost, intradayList, &countIntraday);
    LoadVolatileSymbolsFromFileToList(activeList, &countActive);
  pthread_mutex_unlock(&volatileMgmt.mutexLock);
  
  int currentVolatileSession = GetCurrentVolatileSession();
  switch (currentVolatileSession)
  {
    case VOLATILE_SESSION_INTRADAY:
      memcpy(&activeList[countActive * SYMBOL_LEN], intradayList, countIntraday * SYMBOL_LEN);
      countActive += countIntraday;
      break;
    case VOLATILE_SESSION_PRE:
      memcpy(&activeList[countActive * SYMBOL_LEN], preList, countPre * SYMBOL_LEN);
      countActive += countPre;
      break;
    case VOLATILE_SESSION_POST:
      memcpy(&activeList[countActive * SYMBOL_LEN], postList, countPost * SYMBOL_LEN);
      countActive += countPost;
      break;
  }
  
  return countActive;
}

/****************************************************************************
- Function name:  LoadShortabilitySchedule
- Input:    
- Output:   
- Return:   
- Description:  
- Usage:    
****************************************************************************/
int LoadShortabilitySchedule()
{
  TraceLog(DEBUG_LEVEL, "Loading Shortability Schedule...\n");
  char fullPath[MAX_PATH_LEN];
  
  if (GetFullConfigPath(FILE_NAME_OF_SHORTABILITY_SCHEDULE, CONF_MISC, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Can not get the path of %s\n", FILE_NAME_OF_SHORTABILITY_SCHEDULE);
    return ERROR;
  }

  // Try to open configuration file
  FILE *fileDesc = fopen(fullPath, "r");

  // Check file pointer
  if(fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Can not open %s to read\n", fullPath);
    return ERROR;
  }
  
  //Init Shortability Schedule
  ShortabilitySchedule.count = 0;
  int i;
  for (i = 0; i < MAX_TIMELINE_OF_SHORTABILITY_SCHEDULE; i++)
  {
    ShortabilitySchedule.timeInSeconds[i] = -1;
  }
  
  //Init SSLMgmt
  memset(&SSLMgmt, 0, sizeof(SSLMgmt));
  
  char lineBuffer[MAX_LINE_LEN];
  memset(lineBuffer, 0, MAX_LINE_LEN);
  t_Parameters paraList;
  int scheduleCounter = 0;
  int hour, min, sec;
  
  while (fgets(lineBuffer, MAX_LINE_LEN, fileDesc) != NULL)
  {
    // Remove unused characters
    RemoveCharacters(lineBuffer, strlen(lineBuffer));

    // Remove blank lines
    if (strlen(lineBuffer) == 0)
    {
      continue;
    }
    
    if (lineBuffer[0] == '#')
    {
      continue;
    }
    
    paraList.countParameters = Lrc_Split(lineBuffer, ':', &paraList);
    
    scheduleCounter++;
    if (scheduleCounter > MAX_TIMELINE_OF_SHORTABILITY_SCHEDULE)
    {
      TraceLog(ERROR_LEVEL, "Too many items for ETB schedule, maximum item supported: %d, current reading line: '%s'\n", MAX_TIMELINE_OF_SHORTABILITY_SCHEDULE, lineBuffer);
      fclose(fileDesc);
      return ERROR;
    }
    
    //check valid time
    hour = atoi(paraList.parameterList[0]);
    min = atoi(paraList.parameterList[1]);
    sec = atoi(paraList.parameterList[2]);
    
    if (hour < 0 || hour > 23
      || min < 0 || min > 59
      || sec < 0 || sec > 59)
    {
      TraceLog(ERROR_LEVEL, "Shortability Schedule: Invalid schedule, line:'%s'\n", lineBuffer);
      fclose(fileDesc);
      return ERROR;
    }
    
    //time in second
    ShortabilitySchedule.timeInSeconds[scheduleCounter-1] = hour * 3600 + min * 60 + sec;
    ShortabilitySchedule.count = scheduleCounter;
    
    memset(lineBuffer, 0, MAX_LINE_LEN);
  }
  
  TraceLog(DEBUG_LEVEL, "Number of shortability schedules: %d\n", ShortabilitySchedule.count);
  
  fclose(fileDesc);
  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadLdapConfiguration
- Input:    
- Output:   
- Return:   
- Description:  
- Usage:    
****************************************************************************/
int LoadLdapConfiguration()
{
  TraceLog(DEBUG_LEVEL, "Loading LDAP configuration...\n");
  char fullPath[MAX_PATH_LEN];
  
  if (GetFullConfigPath(FILE_NAME_OF_LDAP_CONFIG, CONF_MISC, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Can not get the path of %s\n", FILE_NAME_OF_LDAP_CONFIG);
    return ERROR;
  }

  // Try to open configuration file
  FILE *fileDesc = fopen(fullPath, "r");

  // Check file pointer
  if(fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Can not open %s to read\n", fullPath);
    return ERROR;
  }
  
  //Initialize LDAP Info
  memset(&LdapInfo, 0, sizeof(LdapInfo));
  
  char lineBuffer[MAX_LINE_LEN];
  memset(lineBuffer, 0, MAX_LINE_LEN);
  int counter = 1;
  
  while (fgets(lineBuffer, MAX_LINE_LEN, fileDesc) != NULL)
  {
    // Remove unused characters
    RemoveCharacters(lineBuffer, strlen(lineBuffer));

    // Remove blank lines
    if (strlen(lineBuffer) == 0)
    {
      continue;
    }
    
    if (lineBuffer[0] == '#')
    {
      memset(lineBuffer, 0, MAX_LINE_LEN);
      continue;
    }
    
    if (counter == 1)
    {
      // LDAP URL
      strcpy(LdapInfo.url, lineBuffer);
    }
    else if (counter == 2)
    {
      // LDAP Port
      LdapInfo.port = atoi(lineBuffer);
    }
    else if (counter == 3)
    {
      // User OU
      strcpy(LdapInfo.userOU, lineBuffer);
    }
    else
    {
      TraceLog(DEBUG_LEVEL, "Invalid file %s. Reading line '%s'\n", FILE_NAME_OF_LDAP_CONFIG, lineBuffer);
      return ERROR;
    }
    
    counter++;
    
    memset(lineBuffer, 0, MAX_LINE_LEN);
  }
  
  TraceLog(DEBUG_LEVEL, "Load LDAP configuration successfully:\n");
  TraceLog(DEBUG_LEVEL, "LDAP Url: '%s'\n", LdapInfo.url);
  TraceLog(DEBUG_LEVEL, "LDAP Port: %d\n", LdapInfo.port);
  TraceLog(DEBUG_LEVEL, "LDAP User OU: '%s'\n", LdapInfo.userOU);
  
  fclose(fileDesc);
  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadLossBySymbolFromfile
- Input:    
- Output:   
- Return:   
- Description:  
- Usage:    
****************************************************************************/
int LoadLossBySymbolFromfile()
{
  TraceLog(DEBUG_LEVEL, "Loading %s\n", FILE_LOSS_BY_SYMBOL);
  
  char fullPath[MAX_PATH_LEN];
  if (GetFullRawDataPath(FILE_LOSS_BY_SYMBOL, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Could not get full path of log file '%s'\n", FILE_LOSS_BY_SYMBOL);
    return ERROR;
  }
  
  FILE *fileDesc = fopen(fullPath, "r");
  if (fileDesc == NULL)
  {
    TraceLog(DEBUG_LEVEL, "Could not open file %s for reading\n", fullPath);
    return SUCCESS;
  }
  
  //Buffer to store each line in the configuration file
  char line[MAX_PATH_LEN] = "\0";
  t_Parameters paraList;
  char symbol[SYMBOL_LEN];
  int symbolIndex, tsIndex;
  double lossValue;

  //Get each line from the configuration file
  while (fgets(line, MAX_PATH_LEN, fileDesc) != NULL) 
  {
    if ((line[0] != '#') && (line[0] > 32))
    {
      // symbol,tsId,loss
      memset(&paraList, 0, sizeof(paraList));
      paraList.countParameters = Lrc_Split(line, ',', &paraList);
      if (paraList.countParameters != 3)
      {
        TraceLog(DEBUG_LEVEL, "(%s) Invalid line content '%s'\n", __func__, line);
        continue;
      }

      strncpy(symbol, paraList.parameterList[0], SYMBOL_LEN);
      tsIndex = atoi(paraList.parameterList[1]);
      lossValue = atof(paraList.parameterList[2]);
      
      symbolIndex = GetStockSymbolIndex(symbol);
      
      if (symbolIndex != -1)
      {
        // this info is only save when trader remove symbol from volatile/disabled list
        ProfitLossPerSymbol[symbolIndex].asLoss -= lossValue;
        if (tsIndex > -1 && tsIndex < MAX_TRADE_SERVER_CONNECTIONS)
        {
        ProfitLossPerSymbol[symbolIndex].tsLoss[tsIndex] -= lossValue;
        }
        else
        {
          TraceLog(DEBUG_LEVEL, "(%s) calculate loss (%lf, %.8s) for AS only\n", __func__, lossValue, symbol);
        }
      }
      else
      {
        TraceLog(ERROR_LEVEL, "(%s) Can not get stock symbol index for '%.8s'\n", __func__, symbol);
      }
    }
    
    //Clear the buffer pointed to by line
    memset(line, 0, MAX_PATH_LEN);
  }
  
  // Close file and release resource
  fclose(fileDesc);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SaveLossBySymbol
- Input:    
- Output:   
- Return:   
- Description:  
- Usage:    
****************************************************************************/
int SaveLossBySymbol(char *symbol, int tsIndex, double currentLoss)
{
  char fullPath[MAX_PATH_LEN];
  if (GetFullRawDataPath(FILE_LOSS_BY_SYMBOL, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Could not get full path of log file '%s'\n", FILE_LOSS_BY_SYMBOL);
    return ERROR;
  }
  
  FILE *fileDesc = fopen(fullPath, "a+");
  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Could not open file %s for updating\n", fullPath);
    return ERROR;
  }
  
  // symbol,tsIndex,fabs(currentLoss)
  fprintf(fileDesc, "%.8s,%d,%lf\n", symbol, tsIndex, currentLoss);
  
  // Close file and release resource
  fclose(fileDesc);
  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadEcnSettingSchedule
- Input:    
- Output:   
- Return:   
- Description:  
- Usage:    
****************************************************************************/
int LoadEcnSettingSchedule()
{
  TraceLog(DEBUG_LEVEL, "Loading %s\n", FILE_NAME_OF_ECN_SETTING_SCHEDULE_CONF);
  
  char fullPath[MAX_PATH_LEN];
  if (GetFullConfigPath(FILE_NAME_OF_ECN_SETTING_SCHEDULE_CONF, CONF_MISC, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Could not get full path of %s\n", FILE_NAME_OF_ECN_SETTING_SCHEDULE_CONF);
    return ERROR;
  }
  
  //--------------------------------
  // Get the values from files
  //--------------------------------
  memset(AutoDisableTrading, 0, sizeof(AutoDisableTrading));
  
  int retVal = 0;
  retVal += GetTimeInSecondsFromFile(fullPath, &TimeToEnableBatszSetting, "enable_batsz_ecn_setting");
  retVal += GetTimeInSecondsFromFile(fullPath, &TimeToDisableBatszSetting, "disable_batsz_ecn_setting");
  retVal += GetTimeInSecondsFromFile(fullPath, &TimeToEnableBatszTrading, "enable_batsz_trading");
  retVal += GetTimeInSecondsFromFile(fullPath, &TimeToDisableTrading[TS_BATSZ_BOE_INDEX], "disable_batsz_trading");
  AutoDisableTrading[TS_BATSZ_BOE_INDEX] = 1;
  
  retVal += GetTimeInSecondsFromFile(fullPath, &TimeToEnableBatsySetting, "enable_batsy_ecn_setting");
  retVal += GetTimeInSecondsFromFile(fullPath, &TimeToDisableBatsySetting, "disable_batsy_ecn_setting");
  retVal += GetTimeInSecondsFromFile(fullPath, &TimeToEnableBatsyTrading, "enable_batsy_trading");
  retVal += GetTimeInSecondsFromFile(fullPath, &TimeToDisableTrading[TS_BYX_BOE_INDEX], "disable_batsy_trading");
  AutoDisableTrading[TS_BYX_BOE_INDEX] = 1;
  
  retVal += GetTimeInSecondsFromFile(fullPath, &TimeToEnableEdgxSetting, "enable_edgx_ecn_setting");
  retVal += GetTimeInSecondsFromFile(fullPath, &TimeToDisableEdgxSetting, "disable_edgx_ecn_setting");
  retVal += GetTimeInSecondsFromFile(fullPath, &TimeToEnableEdgxTrading, "enable_edgx_trading");
  retVal += GetTimeInSecondsFromFile(fullPath, &TimeToDisableTrading[TS_EDGX_DIRECT_INDEX], "disable_edgx_trading");
  AutoDisableTrading[TS_EDGX_DIRECT_INDEX] = 1;
  
  retVal += GetTimeInSecondsFromFile(fullPath, &TimeToEnableEdgaSetting, "enable_edga_ecn_setting");
  retVal += GetTimeInSecondsFromFile(fullPath, &TimeToDisableEdgaSetting, "disable_edga_ecn_setting");
  retVal += GetTimeInSecondsFromFile(fullPath, &TimeToEnableEdgaTrading, "enable_edga_trading");
  retVal += GetTimeInSecondsFromFile(fullPath, &TimeToDisableTrading[TS_EDGA_DIRECT_INDEX], "disable_edga_trading");
  AutoDisableTrading[TS_EDGA_DIRECT_INDEX] = 1;
  
  retVal += GetTimeInSecondsFromFile(fullPath, &TimeToDisableNyseSetting, "disable_nyse_ecn_setting");
  retVal += GetTimeInSecondsFromFile(fullPath, &TimeToDisableAmexSetting, "disable_amex_ecn_setting");
  retVal += GetTimeInSecondsFromFile(fullPath, &TimeToDisableTrading[TS_NYSE_CCG_INDEX], "disable_nyse_trading");
  AutoDisableTrading[TS_NYSE_CCG_INDEX] = 1;
  
  retVal += GetTimeInSecondsFromFile(fullPath, &TimeToDisableTrading[TS_ARCA_DIRECT_INDEX], "disable_arca_trading");
  AutoDisableTrading[TS_ARCA_DIRECT_INDEX] = 1;
  
  retVal += GetTimeInSecondsFromFile(fullPath, &TimeToDisableTrading[TS_NASDAQ_OUCH_INDEX], "disable_nasdaq_ouch_trading");
  AutoDisableTrading[TS_NASDAQ_OUCH_INDEX] = 1;
  retVal += GetTimeInSecondsFromFile(fullPath, &TimeToDisableTrading[TS_NASDAQ_RASH_INDEX], "disable_nasdaq_rash_trading");
  AutoDisableTrading[TS_NASDAQ_RASH_INDEX] = 1;
  
  retVal += GetTimeInSecondsFromFile(fullPath, &TimeToEnableBxSetting, "enable_bx_ecn_setting");
  retVal += GetTimeInSecondsFromFile(fullPath, &TimeToDisableTrading[TS_NDAQ_OUBX_INDEX], "disable_bx_trading");
  AutoDisableTrading[TS_NDAQ_OUBX_INDEX] = 1;
  
  retVal += GetTimeInSecondsFromFile(fullPath, &TimeToEnablePsxSetting, "enable_psx_ecn_setting");
  retVal += GetTimeInSecondsFromFile(fullPath, &TimeToDisableTrading[TS_PSX_OUCH_INDEX], "disable_psx_trading");
  AutoDisableTrading[TS_PSX_OUCH_INDEX] = 1;
  
  if (retVal != 0)
  {
    return ERROR;
  }
  
  TraceLog(DEBUG_LEVEL, "Time (in seconds) to enable BATSZ ECN Setting: %d\n", TimeToEnableBatszSetting);
  TraceLog(DEBUG_LEVEL, "Time (in seconds) to disable BATSZ ECN Setting: %d\n", TimeToDisableBatszSetting);
  TraceLog(DEBUG_LEVEL, "Time (in seconds) to enable BATSZ Trading: %d\n", TimeToEnableBatszTrading);
  TraceLog(DEBUG_LEVEL, "Time (in seconds) to disable BATSZ Trading: %d\n", TimeToDisableTrading[TS_BATSZ_BOE_INDEX]);
  
  TraceLog(DEBUG_LEVEL, "Time (in seconds) to enable BYX ECN Setting: %d\n", TimeToEnableBatsySetting);
  TraceLog(DEBUG_LEVEL, "Time (in seconds) to disable BYX ECN Setting: %d\n", TimeToDisableBatsySetting);
  TraceLog(DEBUG_LEVEL, "Time (in seconds) to enable BYX Trading: %d\n", TimeToEnableBatsyTrading);
  TraceLog(DEBUG_LEVEL, "Time (in seconds) to disable BYX Trading: %d\n", TimeToDisableTrading[TS_BYX_BOE_INDEX]);
  
  TraceLog(DEBUG_LEVEL, "Time (in seconds) to enable EDGX ECN Setting: %d\n", TimeToEnableEdgxSetting);
  TraceLog(DEBUG_LEVEL, "Time (in seconds) to disable EDGX ECN Setting: %d\n", TimeToDisableEdgxSetting);
  TraceLog(DEBUG_LEVEL, "Time (in seconds) to enable EDGX Trading: %d\n", TimeToEnableEdgxTrading);
  TraceLog(DEBUG_LEVEL, "Time (in seconds) to disable EDGX Trading: %d\n", TimeToDisableTrading[TS_EDGX_DIRECT_INDEX]);
  
  TraceLog(DEBUG_LEVEL, "Time (in seconds) to enable EDGA ECN Setting: %d\n", TimeToEnableEdgaSetting);
  TraceLog(DEBUG_LEVEL, "Time (in seconds) to disable EDGA ECN Setting: %d\n", TimeToDisableEdgaSetting);
  TraceLog(DEBUG_LEVEL, "Time (in seconds) to enable EDGA Trading: %d\n", TimeToEnableEdgaTrading);
  TraceLog(DEBUG_LEVEL, "Time (in seconds) to disable EDGA Trading: %d\n", TimeToDisableTrading[TS_EDGA_DIRECT_INDEX]);
  
  TraceLog(DEBUG_LEVEL, "Time (in seconds) to disable NYSE ECN Setting: %d\n", TimeToDisableNyseSetting);
  TraceLog(DEBUG_LEVEL, "Time (in seconds) to disable AMEX ECN Setting: %d\n", TimeToDisableAmexSetting);
  TraceLog(DEBUG_LEVEL, "Time (in seconds) to disable NYSE Trading: %d\n", TimeToDisableTrading[TS_NYSE_CCG_INDEX]);
  
  TraceLog(DEBUG_LEVEL, "Time (in seconds) to disable ARCA Trading: %d\n", TimeToDisableTrading[TS_ARCA_DIRECT_INDEX]);
  TraceLog(DEBUG_LEVEL, "Time (in seconds) to disable NASDAQ OUCH Trading: %d\n", TimeToDisableTrading[TS_NASDAQ_OUCH_INDEX]);
  TraceLog(DEBUG_LEVEL, "Time (in seconds) to disable NASDAQ RASH Trading: %d\n", TimeToDisableTrading[TS_NASDAQ_RASH_INDEX]);
  
  TraceLog(DEBUG_LEVEL, "Time (in seconds) to enable BX ECN Setting: %d\n", TimeToEnableBxSetting);
  TraceLog(DEBUG_LEVEL, "Time (in seconds) to disable BX Trading: %d\n", TimeToDisableTrading[TS_NDAQ_OUBX_INDEX]);
  
  TraceLog(DEBUG_LEVEL, "Time (in seconds) to enable PSX ECN Setting: %d\n", TimeToEnablePsxSetting);
  TraceLog(DEBUG_LEVEL, "Time (in seconds) to disable PSX Trading: %d\n", TimeToDisableTrading[TS_PSX_OUCH_INDEX]);
  
  return SUCCESS;
}

/****************************************************************************/
