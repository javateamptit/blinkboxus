/*****************************************************************************
**  Project:    Equity Arbitrage Application
**  Filename:   position_manager_proc.c
**  Description:  This file contains function definitions that were declared
          in position_manager_proc.h  
**  Author:     Luan Vo-Kinh
**  First created:  13-Sep-2007
**  Last updated: -----------
*****************************************************************************/

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include <errno.h>
#include "configuration.h"
#include "socket_util.h"
#include "raw_data_mgmt.h"
#include "output_log_mgmt.h"
#include "daedalus_proc.h"
#include "trader_tools_proc.h"
#include "trade_servers_mgmt_proc.h"
#include "trade_servers_proc.h"
#include "position_manager_proc.h"
#include "order_mgmt_proc.h"
#include "nasdaq_rash_proc.h"
#include "database_util.h"
#include "arca_direct_proc.h"
#include "logging.h"
/****************************************************************************
** Global variables definition
****************************************************************************/

/*
- Added date: 3/19/2008
- By luanvk
- Purpose: send/receive PM message
*/
#define MSG_TYPE_FOR_RECEIVE_PM_CURRENT_STATUS_FROM_PM 41000
#define MSG_TYPE_FOR_RECEIVE_PM_RAW_DATA_FROM_PM 41001
#define MSG_TYPE_FOR_RECEIVE_PM_END_OF_RAW_DATA_FROM_PM 41002
#define MSG_TYPE_FOR_RECEIVE_SLEEPING_SYMBOL_LIST_FROM_PM 41003

#define MSG_TYPE_FOR_SET_PM_CONF_TO_PM 40500

#define MSG_TYPE_FOR_RECEIVE_PM_GLOBAL_SETTING_FROM_PM 41501
#define MSG_TYPE_FOR_SET_PM_GLOBAL_SETTING_TO_PM 40501

#define MSG_TYPE_FOR_RECEIVE_PM_BOOK_CONF_FROM_PM 41502
#define MSG_TYPE_FOR_SET_PM_BOOK_CONF_TO_PM 40502

#define MSG_TYPE_FOR_RECEIVE_PM_ORDER_CONF_FROM_PM 41503
#define MSG_TYPE_FOR_SET_PM_ORDER_CONF_TO_PM 40503

#define MSG_TYPE_FOR_RECEIVE_PM_IGNORE_SYMBOL_FROM_PM 41505
#define MSG_TYPE_FOR_SET_PM_IGNORE_SYMBOL_TO_PM 40505

#define MSG_TYPE_FOR_RECEIVE_PM_SYMBOL_STATUS_FROM_PM 41506
#define MSG_TYPE_FOR_GET_PM_SYMBOL_STATUS_TO_PM 40506

#define MSG_TYPE_FOR_RECEIVE_HALTED_SYMBOL_LIST_FROM_PM 41507

#define MSG_TYPE_FOR_RECEIVE_A_HALTED_SYMBOL_FROM_PM 41508

#define MSG_TYPE_FOR_RECEIVE_TRADED_LIST_FROM_PM 41509
#define MSG_TYPE_FOR_GET_TRADED_LIST_TO_PM 40509

#define MSG_TYPE_FOR_RECEIVE_DISENGAGE_ALERT_FROM_PM 41510
#define MSG_TYPE_FOR_RECEIVE_BOOK_IDLE_WARNING_FROM_PM 41512

#define MSG_TYPE_FOR_REQUEST_ENABLE_PM_TO_PM 40000
#define MSG_TYPE_FOR_REQUEST_DISABLE_PM_TO_PM 40001

#define MSG_TYPE_FOR_REQUEST_CONNECT_A_PM_BOOK_TO_PM 40002
#define MSG_TYPE_FOR_REQUEST_DISCONNECT_A_PM_BOOK_TO_PM 40003

#define MSG_TYPE_FOR_REQUEST_CONNECT_ALL_PM_BOOK_TO_PM 40004
#define MSG_TYPE_FOR_REQUEST_DISCONNECT_ALL_PM_BOOK_TO_PM 40005

#define MSG_TYPE_FOR_REQUEST_CONNECT_A_PM_ORDER_TO_PM 40006
#define MSG_TYPE_FOR_REQUEST_DISCONNECT_A_PM_ORDER_TO_PM 40007

#define MSG_TYPE_FOR_REQUEST_CONNECT_ALL_PM_ORDER_TO_PM 40008
#define MSG_TYPE_FOR_REQUEST_DISCONNECT_ALL_PM_ORDER_TO_PM 40009

#define MSG_TYPE_FOR_REQUEST_CONNECT_CTS_TO_PM 40010
#define MSG_TYPE_FOR_REQUEST_DISCONNECT_CTS_TO_PM 40011

#define MSG_TYPE_FOR_REQUEST_CONNECT_CQS_TO_PM 40021
#define MSG_TYPE_FOR_REQUEST_DISCONNECT_CQS_TO_PM 40022

//For PM UQDF
#define MSG_TYPE_FOR_REQUEST_CONNECT_UQDF_TO_PM 40023
#define MSG_TYPE_FOR_REQUEST_DISCONNECT_UQDF_TO_PM 40024

#define MSG_TYPE_FOR_SEND_RAW_DATA_STATUS_TO_PM 40012
#define MSG_TYPE_FOR_SEND_OPEN_POSITION_TO_PM 40013
#define MSG_TYPE_FOR_SEND_STUCK_TO_PM 40014
#define MSG_TYPE_FOR_REQUEST_DISENGAGE_SYMBOL_TO_PM 40015
#define MSG_TYPE_FOR_SEND_CANCEL_REQUEST_TO_PM 40016
#define MSG_TYPE_FOR_SEND_MANUAL_ORDER_TO_PM 40029
#define MSG_TYPE_FOR_SEND_MANUAL_CANCEL_ORDER_TO_PM 40031

#define MSG_TYPE_FOR_REQUEST_CONNECT_UTDF_TO_PM 40017
#define MSG_TYPE_FOR_REQUEST_DISCONNECT_UTDF_TO_PM 40018

#define MSG_TYPE_FOR_SEND_HALT_LIST_TO_PM 40032
#define MSG_TYPE_FOR_SEND_A_HALT_STOCK_TO_PM 40019

// Price Update
#define MSG_TYPE_FOR_SEND_PRICE_UDPATE_REQUEST_TO_PM 40033
#define MSG_TYPE_FOR_RECEIVE_PRICE_UPDATE_FROM_PM 41533

#define MSG_TYPE_FOR_SEND_HEARTBEAT_TO_PM 40020

//For PM Exchange Configuration
#define MSG_TYPE_FOR_RECEIVE_PM_EXCHANGE_CONF_FROM_PM 40504
#define MSG_TYPE_FOR_SET_PM_EXCHANGE_CONF_TO_PM 41504

//For PM BBO Viewer
#define MSG_TYPE_FOR_RECEIVE_BBO_QUOTE_LIST_FROM_PM 40026
#define MSG_TYPE_FOR_GET_PM_BBO_QUOTE_LIST_TO_PM 40025

//For Flush PM BBO Quote for a exchange
#define MSG_TYPE_FOR_SEND_FLUSH_PM_BBO_QUOTES_TO_PM 40027

//For flush PM book
#define MSG_TYPE_FOR_SEND_FLUSH_A_SYMBOL_TO_PM 32012
#define MSG_TYPE_FOR_SEND_FLUSH_ALL_SYMBOL_TO_PM 32013

//For receive PM message get stuck info
#define MSG_TYPE_FOR_RECEIVE_GET_STUCK_INFO_FROM_PM 41514

//For receive manual order response
#define MSG_TYPE_FOR_RECEIVE_MANUAL_ORDER_RESPONSE_FROM_PM 41515

#define MSG_TYPE_FOR_RECEIVE_RASH_SEQ_NUMBER 40510
#define MSG_TYPE_FOR_SEND_RASH_SEQUENCE_NUMBER_TO_PM 40030

//For switching PM Arca feed
#define MSG_TYPE_FOR_SET_PM_ARCA_BOOK_FEED_TO_PM 32017

//The reason when PM disengage handling a position, this is defined from PM's side
#define DISENGAGE_REASON_REJECTED_BY_PM 21
/****************************************************************************
** Global variables definition
****************************************************************************/
int PM_TO_AS_ORDER_INDEX_MAPPING[] = {0, 1, 2 ,3 ,4};

t_DataBlockBufferMgt PMDataBlockBuffer;

/****************************************************************************
- Function name:  Connect2PM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int Connect2PM(void)
{
  if (pmInfo.connection.status == CONNECT)
  {
    TraceLog(WARN_LEVEL, "There was already a AS-PM connection.The new connection could not be established.\n");

    return 0;
  }

  // Thread id
  pthread_t threadPM_id;

  // Create thread
  pthread_create (&threadPM_id, 
          NULL, 
          (void*) &ProcessPM, 
          NULL);

  // Waiting for create thread completed (delay in 1 seconds) 
  sleep(1);

  return 0;
}

/****************************************************************************
- Function name:  DisconnectPM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int Disconnect2PM(void)
{ 
  // Disconnect to PM
  CloseConnection(&pmInfo.connection);

  return SUCCESS;
}


/****************************************************************************
** Function declarations
****************************************************************************/

/****************************************************************************
- Function name:  ProcessPM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
void *ProcessPM(void *pthread_arg)
{
  int sock = -1;
  
  //Connect to PM
  TraceLog(DEBUG_LEVEL, "Trying to connect to PM ...\n");

  sock = Connect2Server(pmInfo.config.ip, pmInfo.config.port, "Position Manager");  

  if ((sock > 0) && (sock < 65535))
  {
    TraceLog(DEBUG_LEVEL, "Connected successfully to the PM\n");

    struct timeval tv;
    tv.tv_sec = 3;
    tv.tv_usec = 0;
    
    socklen_t optlen = sizeof(tv);
    
    //Set receive time-out to 3 secs
    if (setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, &tv, optlen) == ERROR)
    {
      TraceLog(ERROR_LEVEL, "Cannot set receive-timeout for PM socket\n");
      shutdown(sock, 2);
      close(sock);
      pmInfo.connection.socket = -1;
      return NULL;
    }

    //Set send timeout to 3 secs
    tv.tv_sec = 3;
    tv.tv_usec = 0;
    if (setsockopt(sock, SOL_SOCKET, SO_SNDTIMEO, &tv, optlen) == ERROR)
    {
      TraceLog(ERROR_LEVEL, "Cannot set send-timeout for PM socket\n");
      shutdown(sock, 2);
      close(sock);
      pmInfo.connection.socket = -1;
      return NULL;
    }
    
    if (getsockopt(sock, SOL_SOCKET, SO_SNDTIMEO, &tv, &optlen) == ERROR)
    {
      TraceLog(ERROR_LEVEL, "Cannot get send-timeout for PM socket %d\n", sock);
    }
    else
    {
      TraceLog(DEBUG_LEVEL, "getsockopt with SO_SNDTIMEO for socket descriptor %d: %d(s) + %d(us)\n", sock, (long)tv.tv_sec, (long)tv.tv_usec);
    }
        
    // Update PMs information
    pmInfo.connection.status = CONNECT;
    pmInfo.connection.socket = sock;

    //Aggregation server talk PM
    Talk2PM();

    TraceLog(DEBUG_LEVEL, "*****************************************\n");
    TraceLog(DEBUG_LEVEL, "* PM is disconnected\n");
    TraceLog(DEBUG_LEVEL, "*****************************************\n\n");

    // Update PMs information
    pmInfo.connection.status = DISCONNECT;
    
    // Close the socket
    if (sock != -1)
    {
      shutdown(sock, 2);
      close(sock);
    }
    
    // Update PMs information
    pmInfo.connection.socket = -1;
    
    ResetPMStatus();

    BuildNSendDisconnectAlertToAllDaedalus("PM from AS");
    BuildNSendDisconnectAlertToAllTT("PM from AS");
    
    // Clear sleeping symbol list
    int account, symbolIndex;
    int countSymbol = RMList.countSymbol;
    for (account = 0; account < MAX_ACCOUNT; account++)
    {
      for (symbolIndex = 0; symbolIndex < countSymbol; symbolIndex++)
      {
        if (verifyPositionInfo[account][symbolIndex].isTrading == TRADING_AUTOMATIC)
        {
          verifyPositionInfo[account][symbolIndex].isSleeping = NO;
          verifyPositionInfo[account][symbolIndex].isTrading = TRADING_NONE;
          verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
        }
      }
    }
  }
  else
  {
    pmSetting.isConnectAll = NO;
    TraceLog(ERROR_LEVEL, "* Cannot connect to PM\n");
  }
  
  return NULL;
}

/****************************************************************************
- Function name:  Talk2PM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int Talk2PM(void)
{
  t_TSMessage pmMessage;

  // Send raw data received status to PM
  if (SendRawDataStatusToPM() == ERROR)
  {
    CloseConnection(&pmInfo.connection);
    return ERROR;
  }
  
  if (SendRASHSeqNumberToPM() == ERROR)
  {
    CloseConnection(&pmInfo.connection);
    return ERROR;
  }
  
  int tmpList[MAX_STOCK_SYMBOL];
  if (SendHaltStockListToPM(tmpList, -1) == ERROR)
  {
    CloseConnection(&pmInfo.connection);
    return ERROR;
  }
  
  //Check if TT request PM to connect to all connections
  if (pmSetting.isConnectAll == YES)
  {
    pmSetting.isConnectAll = NO;
    ProcessPMConnectAllConnection();
  }
  
  // Initialize buffer
  memset(&PMDataBlockBuffer, 0, sizeof(t_DataBlockBufferMgt));

  while (1) 
  {
    // Check connection between AS and PM
    if ((pmInfo.connection.status == DISCONNECT) || (pmInfo.connection.socket == -1))
    {
      return ERROR;
    }

    // Receive a message from PM
    if (ReceivePMMessage(&pmMessage) != ERROR)
    {     
      // Message type
      int msgType = GetShortNumber(pmMessage.msgContent);
      
      switch (msgType)
      {
        //Book idle warning message
        case MSG_TYPE_FOR_RECEIVE_BOOK_IDLE_WARNING_FROM_PM:
          ProcessPMMessageForBookIdleWarning(&pmMessage);
          break;
          
        // Receive trading status message
        case MSG_TYPE_FOR_RECEIVE_PM_CURRENT_STATUS_FROM_PM:
          ProcessPMMessageForCurrentStatus(&pmMessage);
          
          if (SendHeartBeatToPM() == ERROR)
          {
            return ERROR;
          }

          break;
        
        // Receive raw data message
        case MSG_TYPE_FOR_RECEIVE_PM_RAW_DATA_FROM_PM:          
          ProcessPMMessageForRawData(&pmMessage);
          break;

        // Receive end of raw data message
        case MSG_TYPE_FOR_RECEIVE_PM_END_OF_RAW_DATA_FROM_PM:
          ProcessPMMessageForEndOfRawData();
          break;

        // Receive end of raw data message
        case MSG_TYPE_FOR_RECEIVE_SLEEPING_SYMBOL_LIST_FROM_PM:
          ProcessPMMessageForSleepingSymbolList(&pmMessage);
          break;

        // Receive ECN Books configuration
        case MSG_TYPE_FOR_RECEIVE_PM_BOOK_CONF_FROM_PM:
          ProcessPMMessageForBookConf(&pmMessage);
          break;

        // Receive ECN Orders configuration
        case MSG_TYPE_FOR_RECEIVE_PM_ORDER_CONF_FROM_PM:
          ProcessPMMessageForOrderConf(&pmMessage);
          break;

        // Receive global configuration
        case MSG_TYPE_FOR_RECEIVE_PM_GLOBAL_SETTING_FROM_PM:
          ProcessPMMessageForGlobalConf(&pmMessage);
          break;

        // Receive PM Exchange configuration
        case MSG_TYPE_FOR_RECEIVE_PM_EXCHANGE_CONF_FROM_PM:
          ProcessPMMessageForExchangeConf(&pmMessage);
          break;

        // Receive ignore symbol
        case MSG_TYPE_FOR_RECEIVE_PM_IGNORE_SYMBOL_FROM_PM:
          ProcessPMMessageForIgnoreSymbol(&pmMessage);
          break;

        // Receive halted symbol list
        case MSG_TYPE_FOR_RECEIVE_HALTED_SYMBOL_LIST_FROM_PM:
          ProcessPMMessageForHaltStockList(&pmMessage);
          break;

        case MSG_TYPE_FOR_RECEIVE_A_HALTED_SYMBOL_FROM_PM:
          ProcessPMMessageForHaltedSymbol(&pmMessage);
          break;
          
        // Receive price update
        case MSG_TYPE_FOR_RECEIVE_PRICE_UPDATE_FROM_PM:
          ProcessPMMessageForPriceUpdate(&pmMessage);
          break;

        // Receive ignore symbol
        case MSG_TYPE_FOR_RECEIVE_PM_SYMBOL_STATUS_FROM_PM:
          ProcessPMMessageForSymbolStatus(&pmMessage);
          break;

        // Receive traded list of a symbol
        case MSG_TYPE_FOR_RECEIVE_TRADED_LIST_FROM_PM:
          ProcessPMMessageForTradedList(&pmMessage);
          break;
          
        // Receive PM disengage alert
        case MSG_TYPE_FOR_RECEIVE_DISENGAGE_ALERT_FROM_PM:
          ProcessPMMessageForDisengageAlert(&pmMessage);
          break;
        
        // Receive PM BBO Quotes
        case MSG_TYPE_FOR_RECEIVE_BBO_QUOTE_LIST_FROM_PM:
          ProcessPMMessageForBBOQuotes(&pmMessage);
          break;
          
        // Receive PM get stuck info for resume handling stuck
        case MSG_TYPE_FOR_RECEIVE_GET_STUCK_INFO_FROM_PM:
          ProcessPMMessageForGetStuckInfoBeforeResumeHandlingStuck(&pmMessage);
          break;
        
        // Receive Manual Order response from PM
        case MSG_TYPE_FOR_RECEIVE_MANUAL_ORDER_RESPONSE_FROM_PM:
          ProcessPMMessageForManualOrderResponse(&pmMessage);
          break;

        // Receive RASH sequence number from PM
        case MSG_TYPE_FOR_RECEIVE_RASH_SEQ_NUMBER:
          ProcessPMMessageForRashSeqNumber(&pmMessage);
          break;

        case MSG_TYPE_FOR_RECEIVE_DISCONNECT_ALERT_FROM_PM:
          ProcessPMMessageForDisconnectAlert(&pmMessage);
          break;

          // Receive disabled trading alert from PM
        case MSG_TYPE_FOR_RECEIVE_GROUP_OF_BOOK_DATA_UNHANDLED_ALERT_FROM_PM:
          ProcessPMMessageForGroupOfBookDataUnhandledAlert(&pmMessage);
          break;

        case MSG_TYPE_FOR_RECEIVE_DISABLED_TRADING_ALERT_FROM_PM:
          ProcessPMMessageForDisabledTradingAlert(&pmMessage);
          break;

        case MSG_TYPE_FOR_RECEIVE_POSITION_STATUS_RESPONSE_ALERT_FROM_PM:
          ProcessPMMessageForPositionStatusResponseAlert(&pmMessage);
          break;
          
        // Invalid message
        default:
          TraceLog(ERROR_LEVEL, "Aggregation Server does not support message from PM, msgType = %d\n", msgType);
          break;
      }     
    }
    else
    {     
      return ERROR;
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ReceivePMMessage
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ReceivePMMessage(void *message)
{
  int receivedBytes, sock;

  t_TSMessage *pmMessage = (t_TSMessage *)message;

  // Socket to send messages to PM
  sock = pmInfo.connection.socket;
  
  // Timeout counter
  int timeoutCounter = 0;
  
  // Current messsae body length
  int currentMsgBodyLen = 0;
  
  // Initialize message
  pmMessage->msgLen = 0;
  
  /*
  Try to receive message header
  
  If cannot receive message header when reached timeout, return and report error
  
  otherwise calculate message body
  
  If message body has length 0, return Success
  
  Otherwise try to receive message body
  
  If cannot receive message body when reached timeout, return and report error
  
  Otherwise return Success
   */
  
  // Try to receive message header
  while (pmMessage->msgLen < MSG_HEADER_LEN)
  {
    receivedBytes = recv(sock, &pmMessage->msgContent[pmMessage->msgLen], MSG_HEADER_LEN - pmMessage->msgLen, 0);
    
    if (receivedBytes > 0)
    {
      timeoutCounter = 0;
      pmMessage->msgLen += receivedBytes;
    }
    else
    {
      if ((receivedBytes == 0) && (errno == 0))
      {
        TraceLog(ERROR_LEVEL, "PM: Socket is close\n");
        return ERROR;
      }
      else
      {       
        if (timeoutCounter < 3)
        {
          timeoutCounter += 1;
          TraceLog(DEBUG_LEVEL, "PM: Waiting. Sleep num = %d\n", timeoutCounter);
        }
        else
        {
          TraceLog(ERROR_LEVEL, "PM: TIME OUT!\n");
          return ERROR;
        }
      }
    }
  }
  
  // Get message body length
  currentMsgBodyLen = GetIntNumber(&pmMessage->msgContent[MSG_BODY_LEN_INDEX]);

  //TraceLog(DEBUG_LEVEL, "Body length = %d\n", currentMsgBodyLen);

  if (currentMsgBodyLen == 0)
  {
    // Body is NULL

    return SUCCESS;
  }
  
  // Try to receive message body
  while (pmMessage->msgLen < currentMsgBodyLen + MSG_HEADER_LEN)
  {
    receivedBytes = recv(sock, &pmMessage->msgContent[pmMessage->msgLen], currentMsgBodyLen - (pmMessage->msgLen - MSG_HEADER_LEN), 0);
    
    if (receivedBytes > 0)
    {
      timeoutCounter = 0;
      pmMessage->msgLen += receivedBytes;
    }
    else
    {
      if ((receivedBytes == 0) && (errno == 0))
      {
        TraceLog(ERROR_LEVEL, "PM: Socket is close \n");
        return ERROR;
      }
      else
      {       
        if (timeoutCounter < 3)
        {
          timeoutCounter += 1;
          TraceLog(DEBUG_LEVEL, "PM: Waiting. Sleep num = %d \n", timeoutCounter);
        }
        else
        {
          TraceLog(ERROR_LEVEL, "PM: TIME OUT! \n");
          return ERROR;
        }
      }
    }
  }

  // Succesfully received
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessPMMessageForManualOrderResponse
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessPMMessageForManualOrderResponse(void *message)
{
  t_TSMessage *pmMessage = (t_TSMessage *)message;
  t_SendNewOrder newOrder;
  
  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  strncpy(newOrder.symbol, (char *)&pmMessage->msgContent[currentIndex], SYMBOL_LEN);
  currentIndex += SYMBOL_LEN;
  
  newOrder.side = GetIntNumber(&pmMessage->msgContent[currentIndex]);
  currentIndex += 4;
  
  newOrder.shares = GetIntNumber(&pmMessage->msgContent[currentIndex]);
  currentIndex += 4;
  
  newOrder.maxFloor = GetIntNumber(&pmMessage->msgContent[currentIndex]);
  currentIndex += 4;
  
  newOrder.price = GetDoubleNumber(&pmMessage->msgContent[currentIndex]);
  currentIndex += 8;
  
  newOrder.ECNId = GetIntNumber(&pmMessage->msgContent[currentIndex]);
  currentIndex += 4;
  
  newOrder.tif = GetIntNumber(&pmMessage->msgContent[currentIndex]);
  currentIndex += 4;
  
  int sendId = GetIntNumber(&pmMessage->msgContent[currentIndex]);
  currentIndex += 4;
  
  newOrder.pegDef = GetDoubleNumber(&pmMessage->msgContent[currentIndex]);
  currentIndex += 8;
  
  newOrder.orderId = GetIntNumber(&pmMessage->msgContent[currentIndex]);
  currentIndex += 4;
  
  newOrder.tradingAccount = pmMessage->msgContent[currentIndex++];
  
  //int isForced = GetIntNumber(&pmMessage->msgContent[currentIndex]); (currently available in msg but not used)
  currentIndex += 4;
  
  int ttIndex = GetIntNumber(&pmMessage->msgContent[currentIndex]);
  currentIndex += 4;
  
  int reason = GetIntNumber(&pmMessage->msgContent[currentIndex]);
  currentIndex += 4;

  TraceLog(DEBUG_LEVEL, "Received manual order response from PM, response: %d\n", reason);

  int stockSymbolIndex = GetStockSymbolIndex(newOrder.symbol);
  
  char reasonText[2] = "\0";
  if (reason == MANUAL_ORDER_RESPONSE_SENT)
  {
    ProcessResponseSendNewOrderFromClientTool(&newOrder, sendId, newOrder.orderId, ttIndex, reasonText);
  }
  else if (reason == MANUAL_ORDER_RESPONSE_FAILED_TO_SEND)
  {
    //Price checking was OK but PM could not place order, so AS will try
    if (IsAbleToPlaceManualOrderOnAS(newOrder.ECNId) == 1)
    {
      /*  Will place order on AS
        We dont increase Order ID, since it was already increased */

      if (newOrder.ECNId == TYPE_ARCA_DIRECT)
      {
        if (SendNewOrderToARCA_DIRECTOrder(&newOrder) == ERROR) //Failed
        {
          verifyPositionInfo[newOrder.tradingAccount][stockSymbolIndex].isTrading = TRADING_NONE;
          verifyPositionInfo[newOrder.tradingAccount][stockSymbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
          if (ProcessResponseSendNewOrderFromClientTool(&newOrder, sendId, -1, ttIndex, reasonText) == ERROR) return SUCCESS;
        }
        else  //Sent successfully
        {
          if (ProcessResponseSendNewOrderFromClientTool(&newOrder, sendId, newOrder.orderId, ttIndex, reasonText) == ERROR) return SUCCESS;
        }
      }
      else
      {
        if (SendNewOrderToNASDAQ_RASHOrder(&newOrder) == ERROR)   //Failed
        {
          verifyPositionInfo[newOrder.tradingAccount][stockSymbolIndex].isTrading = TRADING_NONE;
          verifyPositionInfo[newOrder.tradingAccount][stockSymbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
          if (ProcessResponseSendNewOrderFromClientTool(&newOrder, sendId, -1, ttIndex, reasonText) == ERROR) return SUCCESS;
        }
        else  //Sent successfully
        {
          if (ProcessResponseSendNewOrderFromClientTool(&newOrder, sendId, newOrder.orderId, ttIndex, reasonText) == ERROR) return SUCCESS;
        }
      }
    }
    else
    {
      verifyPositionInfo[newOrder.tradingAccount][stockSymbolIndex].isTrading = TRADING_NONE;
      verifyPositionInfo[newOrder.tradingAccount][stockSymbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
      ProcessResponseSendNewOrderFromClientTool(&newOrder, sendId, -1, ttIndex, reasonText);
    }
  }
  else if (reason == MANUAL_ORDER_RESPONSE_FAILED_TO_CHECK)
  {
    //Ask user if he wants to force the order sending
    verifyPositionInfo[newOrder.tradingAccount][stockSymbolIndex].isTrading = TRADING_NONE;
    verifyPositionInfo[newOrder.tradingAccount][stockSymbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
    ProcessResponseSendNewOrderFromClientTool(&newOrder, sendId, -2, ttIndex, reasonText);
  }
  else
  {
    verifyPositionInfo[newOrder.tradingAccount][stockSymbolIndex].isTrading = TRADING_NONE;
    verifyPositionInfo[newOrder.tradingAccount][stockSymbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
    ProcessResponseSendNewOrderFromClientTool(&newOrder, sendId, -1, ttIndex, reasonText);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessPMMessageForRashSeqNumber
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessPMMessageForRashSeqNumber(void *message)
{
  t_TSMessage *pmMessage = (t_TSMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  int seqNumber = GetIntNumber(&pmMessage->msgContent[currentIndex]);
  
  if (NASDAQ_RASH_Config.incomingSeqNum < seqNumber)
  {
    NASDAQ_RASH_Config.incomingSeqNum = seqNumber;
    SaveNASDAQ_RASH_SeqNumToFile();
  }
  else if (NASDAQ_RASH_Config.incomingSeqNum > seqNumber)
  {
    // Do not update in this case.
    // AS has newer seq number, so AS will tell PM for new sequence number
    SendRASHSeqNumberToPM();
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessPMMessageForRawData
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessPMMessageForRawData(void *message)
{
  t_TSMessage *pmMessage = (t_TSMessage *)message;
  t_DataBlock dataBlock;
  memset(&dataBlock, 0, sizeof(dataBlock));

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // ECN Order Id
  int pmOrderIndex = GetIntNumber(&pmMessage->msgContent[currentIndex]);
  int asOrderIndex = PM_TO_AS_ORDER_INDEX_MAPPING[pmOrderIndex];

  // Update current index
  currentIndex += 4;

  while (currentIndex < pmMessage->msgLen)
  {
    // Block length
    int blockLen = GetIntNumber(&pmMessage->msgContent[currentIndex]);    
    dataBlock.blockLen = blockLen;

    // Update current index
    currentIndex += 4;    

    // Message length
    int msgLen = GetIntNumber(&pmMessage->msgContent[currentIndex]);
    dataBlock.msgLen = msgLen;

    // Update current index
    currentIndex += 4;

    // Message body
    memcpy(dataBlock.msgContent, &pmMessage->msgContent[currentIndex], msgLen);

    // Update current index
    currentIndex += msgLen;

    // Length of additional information
    int addLen = GetIntNumber(&pmMessage->msgContent[currentIndex]);
    
    // Update current index
    currentIndex += 4;

    // Body of additional information
    memcpy(dataBlock.addContent, &pmMessage->msgContent[currentIndex], addLen);
    
    //Nomalize timestamp to unique format (nanoseconds since epoch)
    long nsec = (*((int*)&dataBlock.addContent[0])) * 1000000000L + (*((int*)&dataBlock.addContent[4]))*1000L;
    memcpy(&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME], &nsec, 8);
    memcpy(&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME], &nsec, 8);
    
    // Update current index
    currentIndex += addLen;

    /*  Add data block to collection
      Special note: we must check if this rawdata item is of manual order?
      if it is, we will copy to rawdataMgmt of AS*/
    
    int isManualOrderMarking = 0;
    memcpy(&isManualOrderMarking, &dataBlock.addContent[DATABLOCK_OFFSET_CROSS_ID], 4);
    if (isManualOrderMarking == MANUAL_ORDER_MAKING)
    {
      AddDataBlockToCollection(&dataBlock, &asRawDataMgmt[asOrderIndex]);
      SaveDataBlockToFile(&asRawDataMgmt[asOrderIndex]);
    }
    
    AddDataBlockToCollection(&dataBlock, &pmRawDataMgmt[pmOrderIndex]);
    SaveDataBlockToFile(&pmRawDataMgmt[pmOrderIndex]);
    
    PMDataBlockBuffer.queue[PMDataBlockBuffer.count].ECNId = pmOrderIndex;
    PMDataBlockBuffer.queue[PMDataBlockBuffer.count].dataBlock = &pmRawDataMgmt[pmOrderIndex].dataBlockCollection[pmRawDataMgmt[pmOrderIndex].lastUpdatedIndex % MAX_RAW_DATA_ENTRIES];
    PMDataBlockBuffer.count++;    
  }

  TraceLog(DEBUG_LEVEL, "Received raw data from PM: pmECNIndex = %d, countRawData = %d\n", pmOrderIndex, pmRawDataMgmt[pmOrderIndex].countRawDataBlock);

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessPMMessageForSleepingSymbolList
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessPMMessageForSleepingSymbolList(void *message)
{
  t_TSMessage *pmMessage = (t_TSMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;
  
  // Number of symbol
  int numOfSymbols = GetIntNumber(&pmMessage->msgContent[currentIndex]);
  currentIndex += 4;
  
  int i, j, accountIndex, symbolIndex;
  char symbol[SYMBOL_LEN];
  
  int sleepingList[1000], sleepingCount = 0;
  int countSymbol = RMList.countSymbol;
  for (accountIndex = 0; accountIndex < MAX_ACCOUNT; accountIndex++)
  {
    for (symbolIndex = 0; symbolIndex < countSymbol; symbolIndex++)
    {
      if (verifyPositionInfo[accountIndex][symbolIndex].isTrading == TRADING_AUTOMATIC &&
        verifyPositionInfo[accountIndex][symbolIndex].isSleeping == YES)
      { 
        sleepingList[sleepingCount] = accountIndex * 1000000 + symbolIndex;
        sleepingCount++;
      }
    }
  }
  
  if (numOfSymbols > 0)
  {
    DEVLOGF("SleepingSymbolList: numOfSymbols = %d", numOfSymbols);
    for (i = 0; i < numOfSymbols; i++)
    {
      // Symbol
      strncpy(symbol, (char *)&pmMessage->msgContent[currentIndex], SYMBOL_LEN);
      currentIndex += SYMBOL_LEN;
      
      // Account index
      accountIndex = pmMessage->msgContent[currentIndex];
      currentIndex += 1;
      
      DEVLOGF("%d, symbol = %.8s, account = %d", i, symbol, accountIndex);
      symbolIndex = GetStockSymbolIndex(symbol);
      verifyPositionInfo[accountIndex][symbolIndex].isSleeping = YES;
      verifyPositionInfo[accountIndex][symbolIndex].isTrading = TRADING_AUTOMATIC;
      verifyPositionInfo[accountIndex][symbolIndex].isPMOwner = YES;
      verifyPositionInfo[accountIndex][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
      
      for (j = 0; j < sleepingCount; j++)
      {
        if (sleepingList[j] == accountIndex * 1000000 + symbolIndex)
        {
          sleepingList[j] = -1;
        }
      }
    }
  }
  
  for (j = 0; j < sleepingCount; j++)
  {
    if (sleepingList[j] != -1)
    {
      accountIndex = sleepingList[j] / 1000000;
      symbolIndex = sleepingList[j] % 1000000;
      
      verifyPositionInfo[accountIndex][symbolIndex].isSleeping = NO;
      verifyPositionInfo[accountIndex][symbolIndex].isTrading = TRADING_NONE;
      verifyPositionInfo[accountIndex][symbolIndex].isPMOwner = NO;
      verifyPositionInfo[accountIndex][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessPMMessageForEndOfRawData
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessPMMessageForEndOfRawData(void)
{
  if (PMDataBlockBuffer.count == 0)
  {
    return SUCCESS;
  }

  t_DataBlockBufferItem *sortedDataBlock[MAX_DATA_BLOCK_BUFFER];
  SortDataBlockBufferInAscend(&PMDataBlockBuffer, sortedDataBlock);

  t_DataBlock *dataBlock;

  int pmOrderIndex, asOrderIndex;
  int isManualOrderMarking;
  int i;
  for (i = 0; i < PMDataBlockBuffer.count; i++)
  {
    pmOrderIndex = sortedDataBlock[i]->ECNId;
    dataBlock = (t_DataBlock *)sortedDataBlock[i]->dataBlock;
    
    memcpy(&isManualOrderMarking, &dataBlock->addContent[DATABLOCK_OFFSET_CROSS_ID], 4);
    
    if (isManualOrderMarking != MANUAL_ORDER_MAKING)
    {
      DEVLOGF("Raw data %d: time = %ld nsec, ECNId = %d", i, *((long*)&dataBlock->addContent[DATABLOCK_OFFSET_APP_TIME]), pmOrderIndex);
    }
    
    if (isManualOrderMarking == MANUAL_ORDER_MAKING)
    { 
      asOrderIndex = PM_TO_AS_ORDER_INDEX_MAPPING[pmOrderIndex];
  
      /* Update Verify Position Collection */
      if (asOrderIndex == AS_ARCA_DIRECT_INDEX)
      {
        switch (dataBlock->msgContent[0])
        {
          // New order
          case NEW_ORDER_TYPE:
          {
            int variant = dataBlock->msgContent[1];
            // Order ID
            int orderID = GetIntNumberBigEndian(&dataBlock->msgContent[8]);
            
            // Stock symbol
            char symbol[SYMBOL_LEN + 1] = "\0";
            if(variant == 1)
            {
              ManualArcaDirectOrderInfo[orderID].shares = GetIntNumberBigEndian(&dataBlock->msgContent[16]);
              strncpy(symbol, (char *) &dataBlock->msgContent[27], SYMBOL_LEN);
              ManualArcaDirectOrderInfo[orderID].side = dataBlock->msgContent[51];
            }
            else
            {
              ManualArcaDirectOrderInfo[orderID].shares = GetIntNumberBigEndian(&dataBlock->msgContent[16]);
              strncpy(symbol, (char *) &dataBlock->msgContent[61], SYMBOL_LEN);
              ManualArcaDirectOrderInfo[orderID].side = dataBlock->msgContent[93];              
            }
            
            strncpy(ManualArcaDirectOrderInfo[orderID].symbol, symbol, SYMBOL_LEN);           
            TrimRight(symbol, strlen(symbol));            
            strncpy(manualOrderInfo[orderID % MAX_MANUAL_ORDER].symbol, symbol, SYMBOL_LEN);
          }
            break;
            
          // Order Ack
          case ORDER_ACK_TYPE:
            UpdateVerifyPositionInfoFromDIRECTAcceptedManualOrder(dataBlock, PM_MANUAL_ORDER);
            break;

          // Order Fill
          case ORDER_FILLED_TYPE:
            UpdateVerifyPositionInfoFromDIRECTExecutedManualOrder(dataBlock, PM_MANUAL_ORDER);
            break;

          // Order killed
          case ORDER_KILLED_TYPE:
            UpdateVerifyPositionInfoFromDIRECTCanceledManualOrder(dataBlock, PM_MANUAL_ORDER);
            break;

          // Rejected
          case ORDER_REJECTED_TYPE:
            UpdateVerifyPositionInfoFromDIRECTRejectedManualOrder(dataBlock, PM_MANUAL_ORDER);
            break;
        }
      }
      else if (asOrderIndex == AS_NASDAQ_RASH_INDEX)
      {
        if (dataBlock->msgContent[0] == 'S' )
        {
          switch (dataBlock->msgContent[9] )
          {
            case 'A': // A = Accepted Order Message
              UpdateVerifyPositionInfoFromRASHAcceptedManualOrder(dataBlock, PM_MANUAL_ORDER);
              break;
            case 'C': // Canceled order
              UpdateVerifyPositionInfoFromRASHCanceledManualOrder(dataBlock, PM_MANUAL_ORDER);
              break;
            case 'E': // Executed order
              UpdateVerifyPositionInfoFromRASHExecutedManualOrder(dataBlock, PM_MANUAL_ORDER);
              break;
            case 'J': // Rejected order
              UpdateVerifyPositionInfoFromRASHRejectedManualOrder(dataBlock, PM_MANUAL_ORDER);
              break;
          }
        }
        else if ((dataBlock->msgContent[0] == 'U') &&
            ((dataBlock->msgContent[1] == 'O') || (dataBlock->msgContent[1] == 'Q'))) // New Order
        {
          // New Order message
          
          // Order ID
          char fieldToken[16]; //Remove first two digits account
          memcpy( fieldToken, &dataBlock->msgContent[4], 12 );
          fieldToken[12] = 0;
          int orderID = atoi(fieldToken);
          
          // Stock symbol
          char symbol[SYMBOL_LEN + 1] = "\0";
          strncpy(symbol, (char *) &dataBlock->msgContent[23], SYMBOL_LEN);
          TrimRight(symbol, strlen(symbol));
          
          strncpy(manualOrderInfo[orderID % MAX_MANUAL_ORDER].symbol, symbol, SYMBOL_LEN);
        }
      }
    }
    else
    {
      /* Update Verify Position Collection */
      if (pmOrderIndex == PM_ARCA_DIRECT_INDEX)
      {
        switch (dataBlock->msgContent[0])
        {
          case NEW_ORDER_TYPE:
          {
            // New Order          
            PMOrderPerSecond++;
            
            int variant = dataBlock->msgContent[1];
            
            // Order ID
            int orderID = GetIntNumberBigEndian(&dataBlock->msgContent[8]);
            
            // Stock symbol
            char symbol[SYMBOL_LEN + 1] = "\0";
            if(variant == 1)
            {
              PMArcaDirectOrderInfo[orderID % MAX_ORDER_PLACEMENT].shares = GetIntNumberBigEndian(&dataBlock->msgContent[16]);
              strncpy(symbol, (char *) &dataBlock->msgContent[27], SYMBOL_LEN);
              PMArcaDirectOrderInfo[orderID % MAX_ORDER_PLACEMENT].side = dataBlock->msgContent[51];
            }
            else
            {
              PMArcaDirectOrderInfo[orderID % MAX_ORDER_PLACEMENT].shares = GetIntNumberBigEndian(&dataBlock->msgContent[16]);
              strncpy(symbol, (char *) &dataBlock->msgContent[61], SYMBOL_LEN);
              PMArcaDirectOrderInfo[orderID % MAX_ORDER_PLACEMENT].side = dataBlock->msgContent[93];
            }
            
            strncpy(PMArcaDirectOrderInfo[orderID % MAX_ORDER_PLACEMENT].symbol, symbol, SYMBOL_LEN);
            TrimRight(symbol, strlen(symbol));            
            strncpy(pmOrderInfo[orderID % MAX_ORDER_PLACEMENT].symbol, symbol, SYMBOL_LEN);
          }
            break;
            
          // Order Ack
          case ORDER_ACK_TYPE:
            UpdateVerifyPositionInfoFromDIRECTAcceptedFromPM(dataBlock);
            break;

          // Order Fill
          case ORDER_FILLED_TYPE:
            UpdateVerifyPositionInfoFromDIRECTExecutedFromPM(dataBlock);
            break;

          // Order killed
          case ORDER_KILLED_TYPE:
            UpdateVerifyPositionInfoFromDIRECTCanceledFromPM(dataBlock);
            break;

          // Rejected
          case ORDER_REJECTED_TYPE:
            UpdateVerifyPositionInfoFromDIRECTRejectedFromPM(dataBlock);
            break;
        }
      }
      else if (pmOrderIndex == PM_NASDAQ_RASH_INDEX)
      {
        if (dataBlock->msgContent[0] == 'S' )
        {
          switch (dataBlock->msgContent[9] )
          {
            case 'A': // A = Accepted Order Message
              UpdateVerifyPositionInfoFromRASHAcceptedFromPM(dataBlock);
              break;
            case 'C': // Canceled order
              UpdateVerifyPositionInfoFromRASHCanceledFromPM(dataBlock);
              break;
            case 'E': // Executed order
              UpdateVerifyPositionInfoFromRASHExecutedFromPM(dataBlock);
              break;
            case 'J': // Rejected order
              UpdateVerifyPositionInfoFromRASHRejectedFromPM(dataBlock);
              break;
          }
        }
        else if ((dataBlock->msgContent[0] == 'U') &&
            ((dataBlock->msgContent[1] == 'O') || (dataBlock->msgContent[1] == 'Q'))) // New Order
        {
          // New Order          
          PMOrderPerSecond++;
          
          // Order ID
          char fieldToken[16]; //Remove first two digits account
          memcpy( fieldToken, &dataBlock->msgContent[4], 12 );
          fieldToken[12] = 0;
          int orderID = atoi(fieldToken);
          
          // Stock symbol
          char symbol[SYMBOL_LEN + 1] = "\0";
          strncpy(symbol, (char *) &dataBlock->msgContent[23], SYMBOL_LEN);
          TrimRight(symbol, strlen(symbol));
          
          strncpy(pmOrderInfo[orderID % MAX_ORDER_PLACEMENT].symbol, symbol, SYMBOL_LEN);
        }
      }
    }
  }

  TraceLog(DEBUG_LEVEL, "Received raw data from PM: countRawData = %d\n", PMDataBlockBuffer.count);

  // Initialize buffer
  memset(&PMDataBlockBuffer, 0, sizeof(t_DataBlockBufferMgt));

  return SUCCESS;
}

/****************************************************************************
- Function name:  CompareTimestampOfTwoDatablocks
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int CompareTimestampOfTwoDatablocks(t_DataBlockBufferItem *item1, t_DataBlockBufferItem *item2)
{
  t_DataBlock *dataBlock;
  long nsec1, nsec2;

  // First object
  dataBlock = (t_DataBlock *)item1->dataBlock;
  nsec1 = *((long*) &dataBlock->addContent[DATABLOCK_OFFSET_APP_TIME]);

  // Second object
  dataBlock = (t_DataBlock *)item2->dataBlock;
  nsec2 = *((long*) &dataBlock->addContent[DATABLOCK_OFFSET_APP_TIME]);

  if (nsec1 < nsec2)
  {
    return -1;
  }
  else if(nsec1 > nsec2)
  {
    return 1;
  }

  return 0;
}

/****************************************************************************
- Function name:  SortDataBlockBufferInAscend
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int SortDataBlockBufferInAscend(t_DataBlockBufferMgt *PMDataBlockBuffer, t_DataBlockBufferItem *ouput[MAX_DATA_BLOCK_BUFFER])
{
  int i;
  for (i = 0; i < PMDataBlockBuffer->count; i++)
  {
    ouput[i] = &PMDataBlockBuffer->queue[i];
  }

  int j;
  t_DataBlockBufferItem *temp;

  for (i = 0; i < PMDataBlockBuffer->count - 1; i++)
  {
    for (j = i + 1; j < PMDataBlockBuffer->count; j++)
    {
      if (CompareTimestampOfTwoDatablocks(ouput[i], ouput[j]) > 0)
      {
        temp = ouput[j];
        ouput[j] = ouput[i];
        ouput[i] = temp;
      }
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessPMMessageForCurrentStatus
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessPMMessageForCurrentStatus(void *message)
{
  t_TSMessage *pmMessage = (t_TSMessage *)message;  

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Block content = PM current status
  
  // PM trading status
  pmInfo.currentStatus.tradingStatus = pmMessage->msgContent[currentIndex];
  currentIndex += 1;

  // PM Books status
  pmInfo.currentStatus.pmMdcStatus[PM_ARCA_BOOK_INDEX] = pmMessage->msgContent[currentIndex];
  currentIndex += 1;

  pmInfo.currentStatus.pmMdcStatus[PM_NASDAQ_BOOK_INDEX] = pmMessage->msgContent[currentIndex];
  currentIndex += 1;
  
  pmInfo.currentStatus.pmOecStatus[PM_ARCA_DIRECT_INDEX] = pmMessage->msgContent[currentIndex];
  currentIndex += 1;
  
  int lastRASHStatus = pmInfo.currentStatus.pmOecStatus[PM_NASDAQ_RASH_INDEX];
  pmInfo.currentStatus.pmOecStatus[PM_NASDAQ_RASH_INDEX] = pmMessage->msgContent[currentIndex];
  currentIndex += 1;

  // CTS Feed status
  pmInfo.currentStatus.statusCTS_Feed = pmMessage->msgContent[currentIndex];
  currentIndex += 1;
  
  pmInfo.currentStatus.statusCQS_Feed = pmMessage->msgContent[currentIndex];
  currentIndex += 1;

  //UQDF status
  pmInfo.currentStatus.statusUQDF_Feed = pmMessage->msgContent[currentIndex];
  currentIndex += 1;

  // UTDF status
  pmInfo.currentStatus.statusUTDF = pmMessage->msgContent[currentIndex];
  currentIndex += 1;

  if ((lastRASHStatus == CONNECTED) && (pmInfo.currentStatus.pmOecStatus[PM_NASDAQ_RASH_INDEX] == DISCONNECTED))
  {
    //PM just loses its RASH connection, so automatically enable RASH on AS but not connect yet)
    asSetting.orderEnable[AS_NASDAQ_RASH_INDEX] = YES;
    pmSetting.orderEnable[PM_NASDAQ_RASH_INDEX] = NO;
    SaveASSetting();
    SavePMSetting();
    SendASSettingToAllTT(); // ECN Setting is disable for now
    SendPMSettingToAllTT(); // ECN Setting is disabled for now
  }
  
  if ((pmSetting.orderEnable[PM_NASDAQ_RASH_INDEX] == NO) && (pmInfo.currentStatus.pmOecStatus[PM_NASDAQ_RASH_INDEX] == CONNECTED))
  {
    asSetting.orderEnable[AS_NASDAQ_RASH_INDEX] = NO;
    pmSetting.orderEnable[PM_NASDAQ_RASH_INDEX] = YES;
    SaveASSetting();
    SavePMSetting();
    SendASSettingToAllTT(); // ECN Setting is disable for now
    SendPMSettingToAllTT(); // ECN Setting is disabled for now
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessPMMessageForBookConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessPMMessageForBookConf(void *message)
{
  t_TSMessage *pmMessage = (t_TSMessage *)message;  

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Block content = ECN Book Id (1 byte) + ECN Book configuration

  // ECN Book Id (4 byte)
  int pmECNIndex = GetIntNumber(&pmMessage->msgContent[currentIndex]);

  TraceLog(DEBUG_LEVEL, "Received Book configuration from PM, pmECNIndex = %d\n", pmECNIndex);

  // Update current index
  currentIndex += 4;

  // ECN Book configuration
  switch (pmECNIndex)
  {
    // ARCA Book configuration
    case PM_ARCA_BOOK_INDEX:
      pmInfo.ECNBooksConf.ARCABookConf.currentFeedName = pmMessage->msgContent[currentIndex];
      currentIndex++;
      
      memcpy(pmInfo.ECNBooksConf.ARCABookConf.sourceID, &pmMessage->msgContent[currentIndex], MAX_LINE_LEN);
      currentIndex += MAX_LINE_LEN;

      //TraceLog(DEBUG_LEVEL, "ARCABookConf: source ID = %s\n", pmInfo.ECNBooksConf.ARCABookConf.sourceID);

      int groupIndex;
      for (groupIndex = 0; groupIndex < MAX_ARCA_MULTICAST_GROUP; groupIndex ++)
      {
        pmInfo.ECNBooksConf.ARCABookConf.retranConn[groupIndex].index = GetIntNumber(&pmMessage->msgContent[currentIndex]);
        currentIndex += 4;

        memcpy(pmInfo.ECNBooksConf.ARCABookConf.retranConn[groupIndex].ip, &pmMessage->msgContent[currentIndex], MAX_LINE_LEN);
        currentIndex += MAX_LINE_LEN;

        pmInfo.ECNBooksConf.ARCABookConf.retranConn[groupIndex].port = GetIntNumber(&pmMessage->msgContent[currentIndex]);
        currentIndex += 4;
      }

      break;
    
    // NASDAQ Book configuration
    case PM_NASDAQ_BOOK_INDEX:
      memcpy(pmInfo.ECNBooksConf.NASDAQBookConf.UDP.ip, &pmMessage->msgContent[currentIndex], MAX_LINE_LEN);
      currentIndex += MAX_LINE_LEN;

      memcpy(&pmInfo.ECNBooksConf.NASDAQBookConf.UDP.port, &pmMessage->msgContent[currentIndex], 4);
      currentIndex += 4;

      memcpy(pmInfo.ECNBooksConf.NASDAQBookConf.TCP.ip, &pmMessage->msgContent[currentIndex], MAX_LINE_LEN);
      currentIndex += MAX_LINE_LEN;

      memcpy(&pmInfo.ECNBooksConf.NASDAQBookConf.TCP.port, &pmMessage->msgContent[currentIndex], 4);
      currentIndex += 4;

      //TraceLog(DEBUG_LEVEL, "ip = %s, port = %d, ip = %s, port = %d\n", pmInfo.ECNBooksConf.NASDAQBookConf.UDP.ip, pmInfo.ECNBooksConf.NASDAQBookConf.UDP.port, pmInfo.ECNBooksConf.NASDAQBookConf.TCP.ip, pmInfo.ECNBooksConf.NASDAQBookConf.TCP.port);
      break;

    default:
      TraceLog(ERROR_LEVEL, "Invalid Book configuration message from PM, pmECNIndex = %d\n", pmECNIndex);
      return ERROR;
      break;
  }

  // Call function to send Book configuration to all Trader Tool
  SendPMBookConfToAllTT(pmECNIndex);

  return SUCCESS;
}

/****************************************************************************
- Function name:  BuildPMMessageForSetPMBookARCAFeed
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildPMMessageForSetPMBookARCAFeed(void *message, char feedName)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *pmMessage = (t_TSMessage *)message;
  
  // Initialize message
  pmMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SET_PM_ARCA_BOOK_FEED_TO_PM;
  pmMessage->msgContent[0] = shortConvert.c[0];
  pmMessage->msgContent[1] = shortConvert.c[1];
  pmMessage->msgLen += 2;

  // Update index of body length
  pmMessage->msgLen += 4;

  // Update index of block length
  pmMessage->msgLen += 4;

  // Block content = FeedName 1 byte char
  pmMessage->msgContent[pmMessage->msgLen] = feedName;
  pmMessage->msgLen += 1;

  // Block length field
  intConvert.value = pmMessage->msgLen - MSG_HEADER_LEN;
  pmMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  pmMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  pmMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  pmMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = pmMessage->msgLen - MSG_HEADER_LEN;
  pmMessage->msgContent[2] = intConvert.c[0];
  pmMessage->msgContent[3] = intConvert.c[1];
  pmMessage->msgContent[4] = intConvert.c[2];
  pmMessage->msgContent[5] = intConvert.c[3];

  return pmMessage->msgLen;
}
/****************************************************************************
- Function name:  ProcessPMMessageForOrderConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessPMMessageForOrderConf(void *message)
{
  t_TSMessage *pmMessage = (t_TSMessage *)message;  

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Block content = ECN Order Id (1 byte) + ECN Order configuration

  // ECN Order Id (4 byte)
  int pmECNIndex = GetIntNumber(&pmMessage->msgContent[currentIndex]);

  TraceLog(DEBUG_LEVEL, "Received Order configuration from PM, pmECNIndex = %d\n", pmECNIndex);

  // Update current index
  currentIndex += 4;

  // Check ECN Order Id
  if ((pmECNIndex == PM_NASDAQ_RASH_INDEX) || (pmECNIndex == PM_ARCA_DIRECT_INDEX))
  { 
    // ECN Order configuration
    memcpy((unsigned char *)&pmInfo.ECNOrdersConf[pmECNIndex], &pmMessage->msgContent[currentIndex], sizeof(t_ECNOrderConf));   
  }
  else
  {
    TraceLog(ERROR_LEVEL, "Invalid Order configuration message from PM, pmECNIndex = %d\n", pmECNIndex);
    return ERROR;
  }

  // Call function to send Order configuration to all Trader Tool
  SendPMOrderConfToAllTT(pmECNIndex);
  
  // Send Order configuration to daedalus
  SendPMOrderConfToAllDaedalus(pmECNIndex);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessPMMessageForGlobalConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessPMMessageForGlobalConf(void *message)
{
  TraceLog(DEBUG_LEVEL, "Received global configuration from PM\n");

  t_TSMessage *pmMessage = (t_TSMessage *)message;  

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Block content = PM global configuration

  // PM global configuration
  memcpy((unsigned char *)&pmInfo.globalConf, &pmMessage->msgContent[currentIndex], sizeof(t_PMGlobalConf));  
  
  // Call function to send global configuration to all Trader Tool
  SendPMGlobalConfToAllTT();

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessPMMessageForIgnoreSymbol
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessPMMessageForIgnoreSymbol(void *message)
{
  TraceLog(DEBUG_LEVEL, "Received ignored symbol list from PM\n");

  t_TSMessage *pmMessage = (t_TSMessage *)message;  

  int currentIndex = MSG_HEADER_LEN;

  int blockLen = GetIntNumber(&pmMessage->msgContent[currentIndex]);

  // Update current index
  currentIndex += 4;

  // Block content = PM ignored symbol list

  if ((blockLen - 4) % SYMBOL_LEN != 0)
  {
    TraceLog(ERROR_LEVEL, "PM: Invalid ignore symbol list message");

    return ERROR;
  }

  pmIgnoreStockList.countStock = (blockLen - 4) / SYMBOL_LEN;

  // PM ignored symbol
  memcpy(&pmIgnoreStockList.stockList, &pmMessage->msgContent[currentIndex], blockLen - 4);
  
  // Call function to send PM ignored symbol list to all Trader Tool
  SendPMIgnoredSymbolToAllDaedalus();
  SendPMIgnoredSymbolToAllTT();

  return SUCCESS;
}

/****************************************************************************
- Function name:  BuildPMMessageForRawDataStatus
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildPMMessageForRawDataStatus(void *message)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *pmMessage = (t_TSMessage *)message;
  
  // Initialize message
  pmMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_RAW_DATA_STATUS_TO_PM;
  pmMessage->msgContent[0] = shortConvert.c[0];
  pmMessage->msgContent[1] = shortConvert.c[1];
  pmMessage->msgLen += 2;

  // Update index of body length
  pmMessage->msgLen += 4;

  // Message body = n * (blockLength (4 bytes) + blockContent (blockLength bytes))
  int pmECNIndex;

  for (pmECNIndex = 0; pmECNIndex < PM_MAX_ORDER_CONNECTIONS; pmECNIndex++)
  {
    // Block length field
    intConvert.value = 12;
    pmMessage->msgContent[pmMessage->msgLen] = intConvert.c[0];
    pmMessage->msgContent[pmMessage->msgLen + 1] = intConvert.c[1];
    pmMessage->msgContent[pmMessage->msgLen + 2] = intConvert.c[2];
    pmMessage->msgContent[pmMessage->msgLen + 3] = intConvert.c[3];
  
    // Update index of block length
    pmMessage->msgLen += 4;

    // Block content = ECN Order Id (1 byte) + Number of entries (4 bytes)
    // ECN Order Id
    intConvert.value = pmECNIndex;
    pmMessage->msgContent[pmMessage->msgLen] = intConvert.c[0];
    pmMessage->msgContent[pmMessage->msgLen + 1] = intConvert.c[1];
    pmMessage->msgContent[pmMessage->msgLen + 2] = intConvert.c[2];
    pmMessage->msgContent[pmMessage->msgLen + 3] = intConvert.c[3];
    pmMessage->msgLen += 4;

    // Number of entries
    intConvert.value = pmRawDataMgmt[pmECNIndex].countRawDataBlock;
    pmMessage->msgContent[pmMessage->msgLen] = intConvert.c[0];
    pmMessage->msgContent[pmMessage->msgLen + 1] = intConvert.c[1];
    pmMessage->msgContent[pmMessage->msgLen + 2] = intConvert.c[2];
    pmMessage->msgContent[pmMessage->msgLen + 3] = intConvert.c[3];
    pmMessage->msgLen += 4;
  }

  // Body length field
  intConvert.value = pmMessage->msgLen - MSG_HEADER_LEN;
  pmMessage->msgContent[2] = intConvert.c[0];
  pmMessage->msgContent[3] = intConvert.c[1];
  pmMessage->msgContent[4] = intConvert.c[2];
  pmMessage->msgContent[5] = intConvert.c[3];

  return pmMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildPMMessageForEnableTrading
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildPMMessageForEnableTrading(void *message)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *pmMessage = (t_TSMessage *)message;
  
  // Initialize message
  pmMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_REQUEST_ENABLE_PM_TO_PM;
  pmMessage->msgContent[0] = shortConvert.c[0];
  pmMessage->msgContent[1] = shortConvert.c[1];
  pmMessage->msgLen += 2;

  // Update index of body length
  pmMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  
  // Update index of block length
  pmMessage->msgLen += 4;

  // Block length field
  intConvert.value = 4;
  pmMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  pmMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  pmMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  pmMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = pmMessage->msgLen - MSG_HEADER_LEN;
  pmMessage->msgContent[2] = intConvert.c[0];
  pmMessage->msgContent[3] = intConvert.c[1];
  pmMessage->msgContent[4] = intConvert.c[2];
  pmMessage->msgContent[5] = intConvert.c[3];

  return pmMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildPMMessageForDisableTrading
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildPMMessageForDisableTrading(void *message, short reasonID)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *pmMessage = (t_TSMessage *)message;
  
  // Initialize message
  pmMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_REQUEST_DISABLE_PM_TO_PM;
  pmMessage->msgContent[0] = shortConvert.c[0];
  pmMessage->msgContent[1] = shortConvert.c[1];
  pmMessage->msgLen += 2;

  // Update index of body length
  pmMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  
  // Update index of block length
  pmMessage->msgLen += 4;

  if(reasonID >= 0)
  {
    reasonID = GetTraderIndex(traderToolsInfo.connection[reasonID].userName);
  }

  memcpy(&pmMessage->msgContent[pmMessage->msgLen], &reasonID, 2);
  pmMessage->msgLen += 2;

  // Block length field
  intConvert.value = 6;
  pmMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  pmMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  pmMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  pmMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = pmMessage->msgLen - MSG_HEADER_LEN;
  pmMessage->msgContent[2] = intConvert.c[0];
  pmMessage->msgContent[3] = intConvert.c[1];
  pmMessage->msgContent[4] = intConvert.c[2];
  pmMessage->msgContent[5] = intConvert.c[3];

  return pmMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildPMMessageForHeartbeart
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildPMMessageForHeartbeart(void *message)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *pmMessage = (t_TSMessage *)message;
  
  // Initialize message
  pmMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_HEARTBEAT_TO_PM;
  pmMessage->msgContent[0] = shortConvert.c[0];
  pmMessage->msgContent[1] = shortConvert.c[1];
  pmMessage->msgLen += 2;

  // Update index of body length
  pmMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  
  // Update index of block length
  pmMessage->msgLen += 4;

  // Block content = NULL

  // Block length field
  intConvert.value = 4;
  pmMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  pmMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  pmMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  pmMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = pmMessage->msgLen - MSG_HEADER_LEN;
  pmMessage->msgContent[2] = intConvert.c[0];
  pmMessage->msgContent[3] = intConvert.c[1];
  pmMessage->msgContent[4] = intConvert.c[2];
  pmMessage->msgContent[5] = intConvert.c[3];

  return pmMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildPMMessageForConnectBook
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildPMMessageForConnectBook(void *message, int pmECNIndex)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *pmMessage = (t_TSMessage *)message;
  
  // Initialize message
  pmMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_REQUEST_CONNECT_A_PM_BOOK_TO_PM;
  pmMessage->msgContent[0] = shortConvert.c[0];
  pmMessage->msgContent[1] = shortConvert.c[1];
  pmMessage->msgLen += 2;

  // Update index of body length
  pmMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  
  // Update index of block length
  pmMessage->msgLen += 4;

  // Block content = ECN Book Id
  intConvert.value = pmECNIndex;
  pmMessage->msgContent[pmMessage->msgLen] = intConvert.c[0];
  pmMessage->msgContent[pmMessage->msgLen + 1] = intConvert.c[1];
  pmMessage->msgContent[pmMessage->msgLen + 2] = intConvert.c[2];
  pmMessage->msgContent[pmMessage->msgLen + 3] = intConvert.c[3];

  // Update index of body length
  pmMessage->msgLen += 4;

  // Block length field
  intConvert.value = 8;
  pmMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  pmMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  pmMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  pmMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = pmMessage->msgLen - MSG_HEADER_LEN;
  pmMessage->msgContent[2] = intConvert.c[0];
  pmMessage->msgContent[3] = intConvert.c[1];
  pmMessage->msgContent[4] = intConvert.c[2];
  pmMessage->msgContent[5] = intConvert.c[3];

  return pmMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildPMMessageForDisconnectBook
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildPMMessageForDisconnectBook(void *message, int pmECNIndex)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *pmMessage = (t_TSMessage *)message;
  
  // Initialize message
  pmMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_REQUEST_DISCONNECT_A_PM_BOOK_TO_PM;
  pmMessage->msgContent[0] = shortConvert.c[0];
  pmMessage->msgContent[1] = shortConvert.c[1];
  pmMessage->msgLen += 2;

  // Update index of body length
  pmMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  
  // Update index of block length
  pmMessage->msgLen += 4;

  // Block content = ECN Book Id
  intConvert.value = pmECNIndex;
  pmMessage->msgContent[pmMessage->msgLen] = intConvert.c[0];
  pmMessage->msgContent[pmMessage->msgLen + 1] = intConvert.c[1];
  pmMessage->msgContent[pmMessage->msgLen + 2] = intConvert.c[2];
  pmMessage->msgContent[pmMessage->msgLen + 3] = intConvert.c[3];

  // Update index of body length
  pmMessage->msgLen += 4;

  // Block length field
  intConvert.value = 8;
  pmMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  pmMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  pmMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  pmMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = pmMessage->msgLen - MSG_HEADER_LEN;
  pmMessage->msgContent[2] = intConvert.c[0];
  pmMessage->msgContent[3] = intConvert.c[1];
  pmMessage->msgContent[4] = intConvert.c[2];
  pmMessage->msgContent[5] = intConvert.c[3];

  return pmMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildPMMessageForConnectOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildPMMessageForConnectOrder(void *message, int pmECNIndex)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *pmMessage = (t_TSMessage *)message;
  
  // Initialize message
  pmMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_REQUEST_CONNECT_A_PM_ORDER_TO_PM;
  pmMessage->msgContent[0] = shortConvert.c[0];
  pmMessage->msgContent[1] = shortConvert.c[1];
  pmMessage->msgLen += 2;

  // Update index of body length
  pmMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  
  // Update index of block length
  pmMessage->msgLen += 4;

  // Block content = ECN Book Id
  intConvert.value = pmECNIndex;
  pmMessage->msgContent[pmMessage->msgLen] = intConvert.c[0];
  pmMessage->msgContent[pmMessage->msgLen + 1] = intConvert.c[1];
  pmMessage->msgContent[pmMessage->msgLen + 2] = intConvert.c[2];
  pmMessage->msgContent[pmMessage->msgLen + 3] = intConvert.c[3];

  // Update index of body length
  pmMessage->msgLen += 4;

  // Block length field
  intConvert.value = 8;
  pmMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  pmMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  pmMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  pmMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = pmMessage->msgLen - MSG_HEADER_LEN;
  pmMessage->msgContent[2] = intConvert.c[0];
  pmMessage->msgContent[3] = intConvert.c[1];
  pmMessage->msgContent[4] = intConvert.c[2];
  pmMessage->msgContent[5] = intConvert.c[3];

  return pmMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildPMMessageForDisconnectOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildPMMessageForDisconnectOrder(void *message, int pmECNIndex)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *pmMessage = (t_TSMessage *)message;
  
  // Initialize message
  pmMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_REQUEST_DISCONNECT_A_PM_ORDER_TO_PM;
  pmMessage->msgContent[0] = shortConvert.c[0];
  pmMessage->msgContent[1] = shortConvert.c[1];
  pmMessage->msgLen += 2;

  // Update index of body length
  pmMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  
  // Update index of block length
  pmMessage->msgLen += 4;

  // Block content = ECN Book Id
  intConvert.value = pmECNIndex;
  pmMessage->msgContent[pmMessage->msgLen] = intConvert.c[0];
  pmMessage->msgContent[pmMessage->msgLen + 1] = intConvert.c[1];
  pmMessage->msgContent[pmMessage->msgLen + 2] = intConvert.c[2];
  pmMessage->msgContent[pmMessage->msgLen + 3] = intConvert.c[3];

  // Update index of body length
  pmMessage->msgLen += 4;

  // Block length field
  intConvert.value = 8;
  pmMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  pmMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  pmMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  pmMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = pmMessage->msgLen - MSG_HEADER_LEN;
  pmMessage->msgContent[2] = intConvert.c[0];
  pmMessage->msgContent[3] = intConvert.c[1];
  pmMessage->msgContent[4] = intConvert.c[2];
  pmMessage->msgContent[5] = intConvert.c[3];

  return pmMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildPMMessageForSetPMBookConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildPMMessageForSetPMBookConf(void *message, int pmECNIndex)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *pmMessage = (t_TSMessage *)message;
  
  // Initialize message
  pmMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SET_PM_BOOK_CONF_TO_PM;
  pmMessage->msgContent[0] = shortConvert.c[0];
  pmMessage->msgContent[1] = shortConvert.c[1];
  pmMessage->msgLen += 2;

  // Update index of body length
  pmMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  
  // Update index of block length
  pmMessage->msgLen += 4;

  // Block content = ECN Book Id (4 byte) + ECN Book configuration

  // ECN Book Id (4 byte)
  intConvert.value = pmECNIndex;
  pmMessage->msgContent[pmMessage->msgLen] = intConvert.c[0];
  pmMessage->msgContent[pmMessage->msgLen + 1] = intConvert.c[1];
  pmMessage->msgContent[pmMessage->msgLen + 2] = intConvert.c[2];
  pmMessage->msgContent[pmMessage->msgLen + 3] = intConvert.c[3];

  // Update index of body length
  pmMessage->msgLen += 4;

  // ECN Book configuration
  int size = 0;

  switch (pmECNIndex)
  {
    // NASDAQ Book configuration
    case PM_NASDAQ_BOOK_INDEX:
      memcpy(&pmMessage->msgContent[pmMessage->msgLen], pmInfo.ECNBooksConf.NASDAQBookConf.UDP.ip, MAX_LINE_LEN);
      pmMessage->msgLen += MAX_LINE_LEN;

      memcpy(&pmMessage->msgContent[pmMessage->msgLen], &pmInfo.ECNBooksConf.NASDAQBookConf.UDP.port, 4);
      pmMessage->msgLen += 4;

      memcpy(&pmMessage->msgContent[pmMessage->msgLen], pmInfo.ECNBooksConf.NASDAQBookConf.TCP.ip, MAX_LINE_LEN);
      pmMessage->msgLen += MAX_LINE_LEN;

      memcpy(&pmMessage->msgContent[pmMessage->msgLen], &pmInfo.ECNBooksConf.NASDAQBookConf.TCP.port, 4);
      pmMessage->msgLen += 4;

      size = 2 * (MAX_LINE_LEN + 4);
      break;
  }

  // Block length field
  intConvert.value = size + 8;
  pmMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  pmMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  pmMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  pmMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = pmMessage->msgLen - MSG_HEADER_LEN;
  pmMessage->msgContent[2] = intConvert.c[0];
  pmMessage->msgContent[3] = intConvert.c[1];
  pmMessage->msgContent[4] = intConvert.c[2];
  pmMessage->msgContent[5] = intConvert.c[3];

  return pmMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildPMMessageForSetPMOrderConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildPMMessageForSetPMOrderConf(void *message, int pmECNIndex)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *pmMessage = (t_TSMessage *)message;
  
  // Initialize message
  pmMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SET_PM_ORDER_CONF_TO_PM;
  pmMessage->msgContent[0] = shortConvert.c[0];
  pmMessage->msgContent[1] = shortConvert.c[1];
  pmMessage->msgLen += 2;

  // Update index of body length
  pmMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  
  // Update index of block length
  pmMessage->msgLen += 4;

  // Block content = ECN Order Id (4 byte) + ECN Order configuration
  
  // ECN Order Id (4 byte)
  intConvert.value = pmECNIndex;
  pmMessage->msgContent[pmMessage->msgLen] = intConvert.c[0];
  pmMessage->msgContent[pmMessage->msgLen + 1] = intConvert.c[1];
  pmMessage->msgContent[pmMessage->msgLen + 2] = intConvert.c[2];
  pmMessage->msgContent[pmMessage->msgLen + 3] = intConvert.c[3];

  // Update index of body length
  pmMessage->msgLen += 4;

  // ECN Order configuration
  int size = sizeof(t_ECNOrderConf);
  memcpy(&pmMessage->msgContent[pmMessage->msgLen], (unsigned char *)&pmInfo.ECNOrdersConf[pmECNIndex], size);
  pmMessage->msgLen += size;

  // Block length field
  intConvert.value = size + 5;
  pmMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  pmMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  pmMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  pmMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = pmMessage->msgLen - MSG_HEADER_LEN;
  pmMessage->msgContent[2] = intConvert.c[0];
  pmMessage->msgContent[3] = intConvert.c[1];
  pmMessage->msgContent[4] = intConvert.c[2];
  pmMessage->msgContent[5] = intConvert.c[3];

  return pmMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildPMMessageForSetPMGlobalConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildPMMessageForSetPMGlobalConf(void *message)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *pmMessage = (t_TSMessage *)message;
  
  // Initialize message
  pmMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SET_PM_GLOBAL_SETTING_TO_PM;
  pmMessage->msgContent[0] = shortConvert.c[0];
  pmMessage->msgContent[1] = shortConvert.c[1];
  pmMessage->msgLen += 2;

  // Update index of body length
  pmMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  
  // Update index of block length
  pmMessage->msgLen += 4;

  // Block content = PM global configuration

  // PM global configuration
  int size = sizeof(t_PMGlobalConf);
  memcpy(&pmMessage->msgContent[pmMessage->msgLen], (unsigned char *)&pmInfo.globalConf, size);
  pmMessage->msgLen += size;

  // Block length field
  intConvert.value = size + 4;
  pmMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  pmMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  pmMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  pmMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = pmMessage->msgLen - MSG_HEADER_LEN;
  pmMessage->msgContent[2] = intConvert.c[0];
  pmMessage->msgContent[3] = intConvert.c[1];
  pmMessage->msgContent[4] = intConvert.c[2];
  pmMessage->msgContent[5] = intConvert.c[3];

  return pmMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildPMMessageForSetIgnoreStockList
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildPMMessageForSetIgnoreStockList(void *message, int type, const char *symbol)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *ttMessage = (t_TSMessage *)message;
  
  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SET_PM_IGNORE_SYMBOL_TO_PM;
  ttMessage->msgContent[0] = shortConvert.c[0];
  ttMessage->msgContent[1] = shortConvert.c[1];
  ttMessage->msgLen += 2;

  // Update index of body length
  ttMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + type (1 bytes) + symbol (8 bytes))
  
  // Block length field
  intConvert.value = 16;
  ttMessage->msgContent[ttMessage->msgLen] = intConvert.c[0];
  ttMessage->msgContent[ttMessage->msgLen + 1] = intConvert.c[1];
  ttMessage->msgContent[ttMessage->msgLen + 2] = intConvert.c[2];
  ttMessage->msgContent[ttMessage->msgLen + 3] = intConvert.c[3];

  // Update index of block length
  ttMessage->msgLen += 4;

  intConvert.value = type;
  ttMessage->msgContent[ttMessage->msgLen] = intConvert.c[0];
  ttMessage->msgContent[ttMessage->msgLen + 1] = intConvert.c[1];
  ttMessage->msgContent[ttMessage->msgLen + 2] = intConvert.c[2];
  ttMessage->msgContent[ttMessage->msgLen + 3] = intConvert.c[3];

  // Update index
  ttMessage->msgLen += 4;

  // Symbol
  strncpy((char *) &ttMessage->msgContent[ttMessage->msgLen], symbol, SYMBOL_LEN);
  ttMessage->msgLen += SYMBOL_LEN;
  
  // Body length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  ttMessage->msgContent[2] = intConvert.c[0];
  ttMessage->msgContent[3] = intConvert.c[1];
  ttMessage->msgContent[4] = intConvert.c[2];
  ttMessage->msgContent[5] = intConvert.c[3];

  return ttMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildPMMessageForSendStuckInfo
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildPMMessageForSendStuckInfo(void *message, const char *symbol, int numOfShares, double stuckPrice, int side, int crossID, int account)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;
  t_DoubleConverter dblConvert;

  t_TSMessage *ttMessage = (t_TSMessage *)message;
  
  // Initialize message
  ttMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_STUCK_TO_PM;
  memcpy(ttMessage->msgContent, shortConvert.c, 2);
  ttMessage->msgLen += 2;

  // Update index of body length
  ttMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + Symbol (8 bytes) + crossID(4 bytes) + Number Of Shares (4 bytes) + Stuck Price (8 bytes) + reference Price (8 bytes) + Side (1 byte) + account (1 byte))  
  
  // Block length field
  intConvert.value = 38;
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], intConvert.c, 4);
  ttMessage->msgLen += 4;
  
  // symbol
  strncpy((char *) &ttMessage->msgContent[ttMessage->msgLen], symbol, SYMBOL_LEN);
  ttMessage->msgLen += SYMBOL_LEN;
    
  int stockSymbolIndex = GetStockSymbolIndex(symbol);
  if (stockSymbolIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "(SendStuckInfo): Invalid symbol %.8s\n", symbol);
  }
  
  // cross id
  intConvert.value = crossID;
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], intConvert.c, 4);
  ttMessage->msgLen += 4;
  
  // stuck shares
  intConvert.value = numOfShares;
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], intConvert.c, 4);
  ttMessage->msgLen += 4;
  
  // stuck price
  double price = stuckPrice;
  if (stockSymbolIndex != -1 && feq(stuckPrice, 0.00))
  {
    price = RMList.collection[account][stockSymbolIndex].avgPrice;
  } 
  dblConvert.value = price;
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], dblConvert.c, 8);
  ttMessage->msgLen += 8;
  
  //reference price (used for evaluate this stuck and handling-stuck for priority)
  double referencePrice = 0.00;
  if (stockSymbolIndex != -1)
  {
    referencePrice = RMList.collection[account][stockSymbolIndex].avgPrice;
  }
  
  if (feq(referencePrice, 0.00)) referencePrice = stuckPrice;
  
  dblConvert.value = referencePrice;
  memcpy(&ttMessage->msgContent[ttMessage->msgLen], dblConvert.c, 8);
  ttMessage->msgLen += 8;
    
  // side
  ttMessage->msgContent[ttMessage->msgLen] = side;
  ttMessage->msgLen++;
  
  // account
  ttMessage->msgContent[ttMessage->msgLen] = account;
  ttMessage->msgLen++;
  
  // Body length field
  intConvert.value = ttMessage->msgLen - MSG_HEADER_LEN;
  memcpy(&ttMessage->msgContent[2], intConvert.c, 4);

  return ttMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildPMMessageForManualOrder
- Input:      Manual Order's information and Force Flag
- Output:     the message
- Return:     N/A
- Description:    Build a message to PM to ask it to check/send new manual order
- Usage:      
****************************************************************************/
int BuildPMMessageForManualOrder(void *message, t_SendNewOrder *newOrder, int sendID, int isForced, int ttIndex)
{
  t_IntConverter intConvert;
  t_ShortConverter shortConvert;
  t_DoubleConverter doubleConvert;
  
  t_TSMessage *pmMessage = (t_TSMessage *)message;
  
  // Initialize message
  pmMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_MANUAL_ORDER_TO_PM;
  memcpy(pmMessage->msgContent, shortConvert.c, 2);
  pmMessage->msgLen += 2;

  // Update index of body length
  pmMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) 
  
  // Block length field
  // (Block len + symbol + side + shares + maxfloor + price + ECN ID + TIF + SendID + PegDifference + OrderID + account + Forced Flag + ttIndex + routeDest)
  intConvert.value = 4 + SYMBOL_LEN + 4 + 4 + 4 + 8 + 4 + 4 + 4 + 8 + 4 + 4 + 1 + 4 + 4;
  memcpy(&pmMessage->msgContent[pmMessage->msgLen], intConvert.c, 4);
  pmMessage->msgLen += 4;
  
  //SYMBOL
  strncpy((char*)&pmMessage->msgContent[pmMessage->msgLen], newOrder->symbol, SYMBOL_LEN);
  pmMessage->msgLen += SYMBOL_LEN;
  
  //Side
  intConvert.value = newOrder->side;
  memcpy(&pmMessage->msgContent[pmMessage->msgLen], intConvert.c, 4);
  pmMessage->msgLen += 4;
  
  //Shares
  intConvert.value = newOrder->shares;
  memcpy(&pmMessage->msgContent[pmMessage->msgLen], intConvert.c, 4);
  pmMessage->msgLen += 4;
  
  //maxFloor
  intConvert.value = newOrder->maxFloor;
  memcpy(&pmMessage->msgContent[pmMessage->msgLen], intConvert.c, 4);
  pmMessage->msgLen += 4;
  
  //price
  doubleConvert.value = newOrder->price;
  memcpy(&pmMessage->msgContent[pmMessage->msgLen], doubleConvert.c, 8);
  pmMessage->msgLen += 8;
  
  //ECN ID
  intConvert.value = newOrder->ECNId;
  memcpy(&pmMessage->msgContent[pmMessage->msgLen], intConvert.c, 4);
  pmMessage->msgLen += 4;
  
  //TIF
  intConvert.value = newOrder->tif;
  memcpy(&pmMessage->msgContent[pmMessage->msgLen], intConvert.c, 4);
  pmMessage->msgLen += 4;
  
  //Send ID from TT
  intConvert.value = sendID;
  memcpy(&pmMessage->msgContent[pmMessage->msgLen], intConvert.c, 4);
  pmMessage->msgLen += 4;
  
  // Peg difference
  doubleConvert.value = newOrder->pegDef;
  memcpy(&pmMessage->msgContent[pmMessage->msgLen], doubleConvert.c, 8);
  pmMessage->msgLen += 8;
  
  //Order ID
  intConvert.value = newOrder->orderId;
  memcpy(&pmMessage->msgContent[pmMessage->msgLen], intConvert.c, 4);
  pmMessage->msgLen += 4;
  
  //trading account;
  pmMessage->msgContent[pmMessage->msgLen++] = newOrder->tradingAccount;
  
  //Force Flag
  intConvert.value = isForced;
  memcpy(&pmMessage->msgContent[pmMessage->msgLen], intConvert.c, 4);
  pmMessage->msgLen += 4;
  
  //TT Index
  intConvert.value = ttIndex;
  memcpy(&pmMessage->msgContent[pmMessage->msgLen], intConvert.c, 4);
  pmMessage->msgLen += 4;
  
  //RouteDest (RASH order only)
  memcpy(&pmMessage->msgContent[pmMessage->msgLen], RashRouteID[currentMarketTimeType], 4);
  pmMessage->msgLen += 4;
  
  // Body length field
  intConvert.value = pmMessage->msgLen - MSG_HEADER_LEN;
  memcpy(&pmMessage->msgContent[2], intConvert.c, 4);

  return pmMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildPMMessageForManualCancelOrder
- Input:      Manual Cancel Order's information and ECNId
- Output:     the message
- Return:     N/A
- Description:    Build a message to PM to ask it for sending cancel order msg
- Usage:      
****************************************************************************/
int BuildPMMessageForManualCancelOrder(void *message, t_ManualCancelOrder *cancelOrder, int ECNId)
{
  t_IntConverter intConvert;
  t_ShortConverter shortConvert;
  
  t_TSMessage *pmMessage = (t_TSMessage *)message;
  
  // Initialize message
  pmMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_MANUAL_CANCEL_ORDER_TO_PM;
  memcpy(pmMessage->msgContent, shortConvert.c, 2);
  pmMessage->msgLen += 2;

  // Update index of body length
  pmMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) 
  
  // Block length field
  // (Block len + ECNId + Cancel's info)
  intConvert.value = 4 + 4 + 16 + 4 + 24 + SYMBOL_LEN + 20 + 20 + 1;
  memcpy(&pmMessage->msgContent[pmMessage->msgLen], intConvert.c, 4);
  pmMessage->msgLen += 4;
  
  //ECN ID (ARCA DIRECT or RASH)
  intConvert.value = ECNId;
  memcpy(&pmMessage->msgContent[pmMessage->msgLen], intConvert.c, 4);
  pmMessage->msgLen += 4;
  
  //Order Token (16bytes) (for RASH)
  memcpy(&pmMessage->msgContent[pmMessage->msgLen], cancelOrder->orderToken, 16);
  pmMessage->msgLen += 16;
  
  //Order Quantity 4 bytes integer
  intConvert.value = cancelOrder->orderQty;
  memcpy(&pmMessage->msgContent[pmMessage->msgLen], intConvert.c, 4);
  pmMessage->msgLen += 4;
  
  //ecnOrderID (24 bytes)
  memcpy(&pmMessage->msgContent[pmMessage->msgLen], cancelOrder->ecnOrderID, 24);
  pmMessage->msgLen += 24;
  
  //symbol (SYMBOL_LEN bytes)
  memcpy(&pmMessage->msgContent[pmMessage->msgLen], cancelOrder->symbol, SYMBOL_LEN);
  pmMessage->msgLen += SYMBOL_LEN;
  
  //origClOrdID (20 bytes)
  memcpy(&pmMessage->msgContent[pmMessage->msgLen], cancelOrder->origClOrdID, 20);
  pmMessage->msgLen += 20;
  
  //newClOrdID (20 bytes)
  memcpy(&pmMessage->msgContent[pmMessage->msgLen], cancelOrder->newClOrdID, 20);
  pmMessage->msgLen += 20;
  
  //side (1 byte)
  pmMessage->msgContent[pmMessage->msgLen] = cancelOrder->side;
  pmMessage->msgLen += 1;
  
  // Body length field
  intConvert.value = pmMessage->msgLen - MSG_HEADER_LEN;
  memcpy(&pmMessage->msgContent[2], intConvert.c, 4);

  return pmMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildPMMessageForRASHSeqNumber
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildPMMessageForRASHSeqNumber(void *message, int seqNumber)
{
  t_IntConverter intConvert;
  t_ShortConverter shortConvert;

  t_TSMessage *pmMessage = (t_TSMessage *)message;
  
  // Initialize message
  pmMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_RASH_SEQUENCE_NUMBER_TO_PM;
  memcpy(pmMessage->msgContent, shortConvert.c, 2);
  pmMessage->msgLen += 2;

  // Update index of body length
  pmMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + RASH Sequence number(4 bytes)
  
  // Block length field
  intConvert.value = 8;
  memcpy(&pmMessage->msgContent[pmMessage->msgLen], intConvert.c, 4);
  pmMessage->msgLen += 4;
  
  // RASH Seq Number
  intConvert.value = seqNumber;
  memcpy(&pmMessage->msgContent[pmMessage->msgLen], intConvert.c, 4);
  pmMessage->msgLen += 4;
  
  // Body length field
  intConvert.value = pmMessage->msgLen - MSG_HEADER_LEN;
  memcpy(&pmMessage->msgContent[2], intConvert.c, 4);

  return pmMessage->msgLen;
}

/****************************************************************************
- Function name:  SendPMMessage
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendPMMessage(void *message)
{
  int result, sock;

  t_TSMessage *pmMessage = (t_TSMessage *)message;
  
  // Socket to send messages to PM
  sock = pmInfo.connection.socket;

  pthread_mutex_lock(&pmInfo.connection.socketMutex);

  // Check connection between AS and PM
  if ((pmInfo.connection.status != DISCONNECT) && (pmInfo.connection.socket != -1))
  {
    // Send message to PM
    result = send(sock, pmMessage->msgContent, pmMessage->msgLen, 0);
    
    if (result != pmMessage->msgLen)
    {     
      TraceLog(ERROR_LEVEL, "FAIL: SendPMMessage(), socket = %d, numBytesSent = %d, numBytesExpected = %d\n", sock, result, pmMessage->msgLen);

      // Update Trade Servers information
      pmInfo.connection.status = DISCONNECT;
      
      // Close the socket
      if (pmInfo.connection.socket != -1)
      {
        shutdown(pmInfo.connection.socket, 2);
        close(pmInfo.connection.socket);
      }

      // Update Trade Servers information
      pmInfo.connection.socket = -1;
      
      pthread_mutex_unlock(&pmInfo.connection.socketMutex);
      
      return ERROR;
    }
    else
    {
      pthread_mutex_unlock(&pmInfo.connection.socketMutex);
      return SUCCESS;
    }
  }
  else
  {
    pthread_mutex_unlock(&pmInfo.connection.socketMutex);

    return ERROR;
  }

  //Successfully sent
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendRawDataStatusToPM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendRawDataStatusToPM(void)
{
  // Check connection between AS and PM
  if ((pmInfo.connection.status == DISCONNECT) || (pmInfo.connection.socket == -1))
  {
    return ERROR;
  }

  t_TSMessage pmMessage;

  // Build raw data status message to send PM
  BuildPMMessageForRawDataStatus(&pmMessage);

  // Send message to PM
  if (SendPMMessage(&pmMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send raw data received status to PM\n");
    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendHeartBeatToPM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendHeartBeatToPM(void)
{
  if ((pmInfo.connection.status == DISCONNECT) || (pmInfo.connection.socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage pmMessage;

  // Build heartbeat message to send PM
  BuildPMMessageForHeartbeart(&pmMessage);

  // Send message to PM
  if (SendPMMessage(&pmMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send heartbeat to PM\n");

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendRASHSeqNumberToPM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendRASHSeqNumberToPM(void)
{
  // Check connection between AS and PM
  if ((pmInfo.connection.status == DISCONNECT) || (pmInfo.connection.socket == -1))
  {
    return ERROR;
  }

  t_TSMessage pmMessage;

  //Build msg
  BuildPMMessageForRASHSeqNumber(&pmMessage, NASDAQ_RASH_Config.incomingSeqNum);

  // Send message to PM
  if (SendPMMessage(&pmMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send RASH's sequence number to PM\n");

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendConnectBookToPM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendConnectBookToPM(int pmECNIndex)
{
  if ((pmInfo.connection.status == DISCONNECT) || (pmInfo.connection.socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage pmMessage;

  // Build connect ECN Book message to send PM
  BuildPMMessageForConnectBook(&pmMessage, pmECNIndex);

  // Send message to PM
  if (SendPMMessage(&pmMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request PM connect to ECN Book to PM\n");

    CloseConnection(&pmInfo.connection);

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendDisconnectBookToPM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendDisconnectBookToPM(int pmECNIndex)
{
  if ((pmInfo.connection.status == DISCONNECT) || (pmInfo.connection.socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage pmMessage;

  // Build disconnect ECN Book message to send PM
  BuildPMMessageForDisconnectBook(&pmMessage, pmECNIndex);

  // Send message to PM
  if (SendPMMessage(&pmMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request PM disconnect to ECN Book to PM\n");

    CloseConnection(&pmInfo.connection);

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendConnectOrderToPM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendConnectOrderToPM(int pmECNIndex)
{
  if ((pmInfo.connection.status == DISCONNECT) || (pmInfo.connection.socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage pmMessage;

  // Build connect ECN Order message to send PM
  BuildPMMessageForConnectOrder(&pmMessage, pmECNIndex);

  // Send message to PM
  if (SendPMMessage(&pmMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request PM connect to ECN Order to PM\n");

    CloseConnection(&pmInfo.connection);

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendDisconnectOrderToPM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendDisconnectOrderToPM(int pmECNIndex)
{
  if ((pmInfo.connection.status == DISCONNECT) || (pmInfo.connection.socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage pmMessage;

  // Build disconnect ECN Order message to send PM
  BuildPMMessageForDisconnectOrder(&pmMessage, pmECNIndex);

  // Send message to PM
  if (SendPMMessage(&pmMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request PM disconnect to ECN Book to PM\n");

    CloseConnection(&pmInfo.connection);

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SetPMBookConfToPM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SetPMBookConfToPM(int pmECNIndex)
{
  if ((pmInfo.connection.status == DISCONNECT) || (pmInfo.connection.socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage pmMessage;

  // Build set Book configuration message to send PM
  BuildPMMessageForSetPMBookConf(&pmMessage, pmECNIndex);

  // Send message to PM
  if (SendPMMessage(&pmMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request to set Book configuration to PM\n");

    CloseConnection(&pmInfo.connection);

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SetPMOrderConfToPM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SetPMOrderConfToPM(int pmECNIndex)
{
  if ((pmInfo.connection.status == DISCONNECT) || (pmInfo.connection.socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage pmMessage;

  // Build set Order configuration message to send PM
  BuildPMMessageForSetPMOrderConf(&pmMessage, pmECNIndex);

  // Send message to PM
  if (SendPMMessage(&pmMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request to set Order configuration to PM\n");

    CloseConnection(&pmInfo.connection);

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendManualOrderToPM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendManualOrderToPM(t_SendNewOrder *newOrder, int sendID, int isForced, int ttIndex)
{
  if ((pmInfo.connection.status == DISCONNECT) || (pmInfo.connection.socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage pmMessage;

  // Build msg
  BuildPMMessageForManualOrder(&pmMessage, newOrder, sendID, isForced, ttIndex);

  // Send message to PM
  if (SendPMMessage(&pmMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send manual order to PM\n");

    CloseConnection(&pmInfo.connection);

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendManualCancelRequestToPM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendManualCancelRequestToPM(t_ManualCancelOrder *cancelOrder, int ECNId)
{
  if ((pmInfo.connection.status == DISCONNECT) || (pmInfo.connection.socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage pmMessage;

  // Build msg
  BuildPMMessageForManualCancelOrder(&pmMessage, cancelOrder, ECNId);

  // Send message to PM
  if (SendPMMessage(&pmMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send manual cancel order to PM\n");

    CloseConnection(&pmInfo.connection);

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SetBookARCAFeedToPM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SetBookARCAFeedToPM(char feedName)
{
  if ((pmInfo.connection.status == DISCONNECT) || (pmInfo.connection.socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage pmMessage;

  // Build set Book configuration message to send PM
  BuildPMMessageForSetPMBookARCAFeed(&pmMessage, feedName);

  // Send message to PM
  if (SendPMMessage(&pmMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request to set ARCA Book Feed to PM\n");

    CloseConnection(&pmInfo.connection);

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SetPMGlobalConfToPM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SetPMGlobalConfToPM(void)
{
  if ((pmInfo.connection.status == DISCONNECT) || (pmInfo.connection.socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage pmMessage;

  // Build set global configuration message to send PM
  BuildPMMessageForSetPMGlobalConf(&pmMessage);

  // Send message to PM
  if (SendPMMessage(&pmMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request to set global configuration to PM\n");

    CloseConnection(&pmInfo.connection);

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendEnableTradingPMToPM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendEnableTradingPMToPM(void)
{
  if ((pmInfo.connection.status == DISCONNECT) || (pmInfo.connection.socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage pmMessage;

  // Build enable trading message to send PM
  BuildPMMessageForEnableTrading(&pmMessage);

  // Send message to PM
  if (SendPMMessage(&pmMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request to enable trading to PM\n");

    CloseConnection(&pmInfo.connection);

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendDisableTradingPMToPM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendDisableTradingPMToPM(short reasonID)
{
  if ((pmInfo.connection.status == DISCONNECT) || (pmInfo.connection.socket == -1))
  {
    return SUCCESS;
  }

  if (pmInfo.currentStatus.tradingStatus == DISABLE)
  {
    return SUCCESS;
  }
  t_TSMessage pmMessage;

  // Build disable trading message to send PM
  BuildPMMessageForDisableTrading(&pmMessage, reasonID);
  
  // Send message to PM
  if (SendPMMessage(&pmMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request to disable trading to PM\n");

    CloseConnection(&pmInfo.connection);

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SetPMIgnoreStockListToPM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SetPMIgnoreStockListToPM(int type, const char *symbol)
{
  if ((pmInfo.connection.status == DISCONNECT) || (pmInfo.connection.socket == -1))
  {
    return SUCCESS;
  }
  
  t_TSMessage pmMessage;

  TraceLog(DEBUG_LEVEL, "Set PM ignored stock list to PM, type = %d, symbol = %.8s\n", type, symbol);

  // Build ignore stock list message to send Trader Tool
  BuildPMMessageForSetIgnoreStockList(&pmMessage, type, symbol);

  // Send message to Trader Tool
  if (SendPMMessage(&pmMessage) == ERROR)
  {
    return ERROR;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessPMConnectAllConnection
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessPMConnectAllConnection(void)
{
  // PM connect to all ECN Book
  if (ProcessPMConnectAllECNBook() == ERROR)
  {
    return ERROR;
  }

  // PM connect to all ECN Order
  if (ProcessPMConnectAllECNOrder() == ERROR)
  {
    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessPMConnectAllECNBook
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessPMConnectAllECNBook(void)
{
  int ECNIndex;

  for (ECNIndex = 0; ECNIndex < PM_MAX_BOOK_CONNECTIONS; ECNIndex++)
  {
    if (pmSetting.bookEnable[ECNIndex] == YES)
    {
      // Call function to send connect to a ECN Book
      if (SendConnectBookToPM(ECNIndex) == ERROR)
      {
        return ERROR;
      }
    }
  }

  //as mantis issue, require to connect to all feed also
  if (pmSetting.CTS_Enable == YES)
  {
    //Connect to CTS
    SendConnectCTS_FeedToPM();
  }
  
  if (pmSetting.UTDF_Enable == YES)
  {
    //Connect to UTDF
    SendConnectUTDFToPM();
  }
  
  if (pmSetting.CQS_Enable == YES)
  {
    //Connect to CQS
    SendConnectCQS_FeedToPM();
  }
  
  //For UQDF
  if (pmSetting.UQDF_Enable == YES)
  {
    //Connect to UQDF
    SendConnectUQDF_FeedToPM();
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessPMConnectAllECNOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessPMConnectAllECNOrder(void)
{
  int ECNIndex;

  for (ECNIndex = 0; ECNIndex < PM_MAX_ORDER_CONNECTIONS; ECNIndex++)
  {
    if (pmSetting.orderEnable[ECNIndex] == YES)
    {
      // Call function to send connect to a ECN Order
      if (SendConnectOrderToPM(ECNIndex) == ERROR)
      {
        return ERROR;
      }
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessPMDisconnectAllConnection
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessPMDisconnectAllConnection(void)
{
  // Check connection between AS and PM
  if (pmInfo.connection.status != DISCONNECT)
  {
    // PM disconnect to all ECN Book
    if (ProcessPMDisconnectAllECNBook() == ERROR)
    {
      return ERROR;
    }

    // PM disconnect to all ECN Order
    if (ProcessPMDisconnectAllECNOrder() == ERROR)
    {
      return ERROR;
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessPMDisconnectAllECNBook
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessPMDisconnectAllECNBook(void)
{
  int ECNIndex;

  for (ECNIndex = 0; ECNIndex < PM_MAX_BOOK_CONNECTIONS; ECNIndex++)
  {
    // Call function to send disconnect to a ECN Book
    if (SendDisconnectBookToPM(ECNIndex) == ERROR)
    {
      return ERROR;
    }
  }

  //as mantis issue, require to disconnect all feed from PM
  //Disconnect CTS
  SendDisconnectCTS_FeedToPM();
  
  //Disconnect UTDF
  SendDisconnectUTDFToPM();
  
  //Disconnect CQS
  SendDisconnectCQS_FeedToPM();

  //Disconnect UQDF
  SendDisconnectUQDF_FeedToPM();

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessPMDisconnectAllECNOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessPMDisconnectAllECNOrder(void)
{
  int ECNIndex;

  for (ECNIndex = 0; ECNIndex < PM_MAX_ORDER_CONNECTIONS; ECNIndex++)
  {
    // Call function to send disconnect to a ECN Order
    if (SendDisconnectOrderToPM(ECNIndex) == ERROR)
    {
      return ERROR;
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SetPMConfToPM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SetPMConfToPM(void)
{
  if ((pmInfo.connection.status == DISCONNECT) || (pmInfo.connection.socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage pmMessage;

  // Build set PM configuration message to send PM
  BuildPMMessageForSetPMConf(&pmMessage);

  TraceLog(DEBUG_LEVEL, "Set PM conf to PM ...\n");

  // Send message to PM
  if (SendPMMessage(&pmMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request to set PM configuration to PM\n");

    CloseConnection(&pmInfo.connection);

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  BuildPMMessageForSetPMConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildPMMessageForSetPMConf(void *message)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *pmMessage = (t_TSMessage *)message;
  
  // Initialize message
  pmMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SET_PM_CONF_TO_PM;
  pmMessage->msgContent[0] = shortConvert.c[0];
  pmMessage->msgContent[1] = shortConvert.c[1];
  pmMessage->msgLen += 2;

  // Update index of body length
  pmMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  
  // Update index of block length
  pmMessage->msgLen += 4;

  // Block content = PM port

  // PM port
  memcpy(&pmMessage->msgContent[pmMessage->msgLen], (unsigned char *)&pmInfo.config.port, 4);
  pmMessage->msgLen += 4;

  // Block length field
  intConvert.value = 8;
  pmMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  pmMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  pmMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  pmMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = pmMessage->msgLen - MSG_HEADER_LEN;
  pmMessage->msgContent[2] = intConvert.c[0];
  pmMessage->msgContent[3] = intConvert.c[1];
  pmMessage->msgContent[4] = intConvert.c[2];
  pmMessage->msgContent[5] = intConvert.c[3];

  return pmMessage->msgLen;
}

/****************************************************************************
- Function name:  SendGetSymbolStatusToPM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendGetSymbolStatusToPM(int ttIndex, unsigned char symbol[SYMBOL_LEN], char *listMdc)
{
  if ((pmInfo.connection.status == DISCONNECT) || (pmInfo.connection.socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage pmMessage;

  // Build get stock symbol status message to send PM
  BuildPMMessageForGetSymbolStatus(&pmMessage, ttIndex, symbol, listMdc);

  // Send message to PM
  if (SendPMMessage(&pmMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request to get stock symbol status to PM\n");

    CloseConnection(&pmInfo.connection);

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  BuildPMMessageForGetSymbolStatus
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildPMMessageForGetSymbolStatus(void *message, int ttIndex, unsigned char symbol[SYMBOL_LEN], char *listMdc)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *pmMessage = (t_TSMessage *)message;
  
  // Initialize message
  pmMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_GET_PM_SYMBOL_STATUS_TO_PM;
  pmMessage->msgContent[0] = shortConvert.c[0];
  pmMessage->msgContent[1] = shortConvert.c[1];
  pmMessage->msgLen += 2;

  // Update index of body length
  pmMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  
  // Update index of block length
  pmMessage->msgLen += 4;

  // Block content = TT Id (4 byte) + Symbol (8 bytes)

  // TT Id (4 byte)
  pmMessage->msgContent[pmMessage->msgLen] = ttIndex;

  // Update index of block length
  pmMessage->msgLen += 4;

  // Symbol (8 bytes)
  strncpy((char *) &pmMessage->msgContent[pmMessage->msgLen], (char *) symbol, SYMBOL_LEN);
  pmMessage->msgLen += SYMBOL_LEN;
  
  // MDC Type
  int i;
  for(i = 0; i < PM_MAX_BOOK_CONNECTIONS; i++)
  {
    pmMessage->msgContent[pmMessage->msgLen] = listMdc[i];
    pmMessage->msgLen += 1;
  }

  // Block length field
  intConvert.value = SYMBOL_LEN + 8;
  pmMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  pmMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  pmMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  pmMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = pmMessage->msgLen - MSG_HEADER_LEN;
  pmMessage->msgContent[2] = intConvert.c[0];
  pmMessage->msgContent[3] = intConvert.c[1];
  pmMessage->msgContent[4] = intConvert.c[2];
  pmMessage->msgContent[5] = intConvert.c[3];

  return pmMessage->msgLen;
}

/****************************************************************************
- Function name:  ProcessPMMessageForSymbolStatus
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessPMMessageForSymbolStatus(void *message)
{
  //TraceLog(DEBUG_LEVEL, "Received stock symbol status from PM\n");

  t_TSMessage *pmMessage = (t_TSMessage *)message;  

  int currentIndex = MSG_HEADER_LEN;

  // Body = 3 block (block sender + block ASK + block BID)

  // Update current index
  currentIndex += 4;

  int ttId = pmMessage->msgContent[currentIndex];

  //TraceLog(DEBUG_LEVEL, "ttId = %d\n", ttId);

  // Update current index
  currentIndex += 1; // change ttIndex from 4 bytes to 1 byte

  //TraceLog(DEBUG_LEVEL, "symbol = %.8s\n", &tsMessage->msgContent[currentIndex]);

  // Update current index
  //currentIndex += SYMBOL_LEN;

  //int blockASKLen = GetIntNumber(&tsMessage->msgContent[currentIndex]);

  //TraceLog(DEBUG_LEVEL, "Length of block ASK = %d\n", blockASKLen);

  // Update current index
  //currentIndex += blockASKLen;

  //int blockBIDLen = GetIntNumber(&tsMessage->msgContent[currentIndex]);

  //TraceLog(DEBUG_LEVEL, "Length of block BID = %d\n", blockBIDLen);
  
  // Call function to send stock symbol status to Daedalus
  if (traderToolsInfo.connection[ttId].clientType == CLIENT_TOOL_DAEDALUS)
  {
    BuildAndSendSymbolStatusToDaedalus(PM_SERVER_NAME, ttId, pmMessage);
  }
  else if (traderToolsInfo.connection[ttId].clientType == CLIENT_TOOL_TRADER_TOOL)
  {
    SendPMSymbolStatusToTT(ttId, pmMessage);
  }

  return SUCCESS;
}

int ProcessPMMessageForPriceUpdate(t_TSMessage *message)
{
  // int currentIndex = MSG_HEADER_LEN;
  
  // num symbols
  // block for each symbol: symbol (8) + ask share(4) + ask price(8) + bid share(4) + bid price(8)
  
  SendPriceUpdateToAllDaedalus(PM_SERVER_NAME, message->msgContent);

  return SUCCESS;
}


/****************************************************************************
- Function name:  SendCancelRequestToPM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendCancelRequestToPM(int clOrdId, char *ECNOrderId)
{
  // Check connection between AS and PM
  if ((pmInfo.connection.status == DISCONNECT) || (pmInfo.connection.socket == -1))
  {
    return ERROR;
  }

  t_TSMessage pmMessage;

  // Build raw data status message to send PM
  BuildPMMessageForCancelRequest(&pmMessage, clOrdId, ECNOrderId);

  // Send message to PM
  if (SendPMMessage(&pmMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send cancel request to PM\n");

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  BuildPMMessageForCancelRequest
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildPMMessageForCancelRequest(void *message, int clOrdId, char *ECNOrderId)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *pmMessage = (t_TSMessage *)message;
  
  // Initialize message
  pmMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_CANCEL_REQUEST_TO_PM;
  pmMessage->msgContent[0] = shortConvert.c[0];
  pmMessage->msgContent[1] = shortConvert.c[1];
  pmMessage->msgLen += 2;

  // Update index of body length
  pmMessage->msgLen += 4;

  // Message body = blockLength (4 bytes) + clOrdId (4 bytes) + ECNOrderId (20 bytes)
  
  // Block length field
  intConvert.value = 28;
  pmMessage->msgContent[pmMessage->msgLen] = intConvert.c[0];
  pmMessage->msgContent[pmMessage->msgLen + 1] = intConvert.c[1];
  pmMessage->msgContent[pmMessage->msgLen + 2] = intConvert.c[2];
  pmMessage->msgContent[pmMessage->msgLen + 3] = intConvert.c[3];

  // Update index of block length
  pmMessage->msgLen += 4;

  // Block content = clOrdId (4 bytes) + ECNOrderId (20 bytes) + ECN ID to send (4 bytes)
  
  // clOrdId (4 bytes)
  intConvert.value = clOrdId;
  pmMessage->msgContent[pmMessage->msgLen] = intConvert.c[0];
  pmMessage->msgContent[pmMessage->msgLen + 1] = intConvert.c[1];
  pmMessage->msgContent[pmMessage->msgLen + 2] = intConvert.c[2];
  pmMessage->msgContent[pmMessage->msgLen + 3] = intConvert.c[3];
  pmMessage->msgLen += 4;

  // ECN Order ID
  memcpy(&pmMessage->msgContent[pmMessage->msgLen], ECNOrderId, 20);
  pmMessage->msgLen += 20;
  
  // Body length field
  intConvert.value = pmMessage->msgLen - MSG_HEADER_LEN;
  pmMessage->msgContent[2] = intConvert.c[0];
  pmMessage->msgContent[3] = intConvert.c[1];
  pmMessage->msgContent[4] = intConvert.c[2];
  pmMessage->msgContent[5] = intConvert.c[3];

  return pmMessage->msgLen;
}

/****************************************************************************
- Function name:  SendDisengageSymbolToPM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendDisengageSymbolToPM(char *symbol, int account)
{
  // Check connection between AS and PM
  if ((pmInfo.connection.status == DISCONNECT) || (pmInfo.connection.socket == -1))
  {
    return ERROR;
  }

  t_TSMessage pmMessage;

  // Build disengage symbol message to send PM
  BuildPMMessageForDisengageSymbol(&pmMessage, symbol, account);

  // Send message to PM
  if (SendPMMessage(&pmMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send disengage symbol to PM\n");

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  BuildPMMessageForDisengageSymbol
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildPMMessageForDisengageSymbol(void *message, char *symbol, int account)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *pmMessage = (t_TSMessage *)message;
  
  // Initialize message
  pmMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_REQUEST_DISENGAGE_SYMBOL_TO_PM;
  pmMessage->msgContent[0] = shortConvert.c[0];
  pmMessage->msgContent[1] = shortConvert.c[1];
  pmMessage->msgLen += 2;

  // Update index of body length
  pmMessage->msgLen += 4;

  // Message body = blockLength (4 bytes) + symbol (8 bytes) + account (1 byte)
  
  // Block length field
  intConvert.value = 13;
  pmMessage->msgContent[pmMessage->msgLen] = intConvert.c[0];
  pmMessage->msgContent[pmMessage->msgLen + 1] = intConvert.c[1];
  pmMessage->msgContent[pmMessage->msgLen + 2] = intConvert.c[2];
  pmMessage->msgContent[pmMessage->msgLen + 3] = intConvert.c[3];

  // Update index of block length
  pmMessage->msgLen += 4;

  // Block content = symbol (8 bytes)

  // symbol
  strncpy((char *) &pmMessage->msgContent[pmMessage->msgLen], symbol, SYMBOL_LEN);
  pmMessage->msgLen += SYMBOL_LEN;

  //account
  pmMessage->msgContent[pmMessage->msgLen++] = account;
  
  // Body length field
  intConvert.value = pmMessage->msgLen - MSG_HEADER_LEN;
  pmMessage->msgContent[2] = intConvert.c[0];
  pmMessage->msgContent[3] = intConvert.c[1];
  pmMessage->msgContent[4] = intConvert.c[2];
  pmMessage->msgContent[5] = intConvert.c[3];

  return pmMessage->msgLen;
}

/****************************************************************************
- Function name:  SetPMBookARCAConfToPM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SetPMBookARCAConfToPM(int type, int groupIndex)
{
  if ((pmInfo.connection.status == DISCONNECT) || (pmInfo.connection.socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage pmMessage;

  // Build set Book configuration message to send PM
  BuildPMMessageForSetPMBookARCAConf(&pmMessage, type, groupIndex);

  // Send message to PM
  if (SendPMMessage(&pmMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request to set Book configuration to PM\n");

    CloseConnection(&pmInfo.connection);

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  BuildPMMessageForSetPMBookARCAConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildPMMessageForSetPMBookARCAConf(void *message, int type, int groupIndex)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *pmMessage = (t_TSMessage *)message;
  
  // Initialize message
  pmMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SET_PM_BOOK_CONF_TO_PM;
  pmMessage->msgContent[0] = shortConvert.c[0];
  pmMessage->msgContent[1] = shortConvert.c[1];
  pmMessage->msgLen += 2;

  // Update index of body length
  pmMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  
  // Update index of block length
  pmMessage->msgLen += 4;

  // Block content = ECN Book Id (4 byte) + ECN Book configuration

  // ECN Book Id (4 byte)
  intConvert.value = PM_ARCA_BOOK_INDEX;
  pmMessage->msgContent[pmMessage->msgLen] = intConvert.c[0];
  pmMessage->msgContent[pmMessage->msgLen + 1] = intConvert.c[1];
  pmMessage->msgContent[pmMessage->msgLen + 2] = intConvert.c[2];
  pmMessage->msgContent[pmMessage->msgLen + 3] = intConvert.c[3];

  // Update index of body length
  pmMessage->msgLen += 4;

  // ECN Book configuration
  if (type == UPDATE_SOURCE_ID)
  {
    intConvert.value = type;
    pmMessage->msgContent[pmMessage->msgLen] = intConvert.c[0];
    pmMessage->msgContent[pmMessage->msgLen + 1] = intConvert.c[1];
    pmMessage->msgContent[pmMessage->msgLen + 2] = intConvert.c[2];
    pmMessage->msgContent[pmMessage->msgLen + 3] = intConvert.c[3];

    pmMessage->msgLen += 4;

    memcpy(&pmMessage->msgContent[pmMessage->msgLen], pmInfo.ECNBooksConf.ARCABookConf.sourceID, MAX_LINE_LEN);
    pmMessage->msgLen += MAX_LINE_LEN;
  }
  else
  {
    intConvert.value = type;
    pmMessage->msgContent[pmMessage->msgLen] = intConvert.c[0];
    pmMessage->msgContent[pmMessage->msgLen + 1] = intConvert.c[1];
    pmMessage->msgContent[pmMessage->msgLen + 2] = intConvert.c[2];
    pmMessage->msgContent[pmMessage->msgLen + 3] = intConvert.c[3];

    pmMessage->msgLen += 4;

    intConvert.value = groupIndex;
    pmMessage->msgContent[pmMessage->msgLen] = intConvert.c[0];
    pmMessage->msgContent[pmMessage->msgLen + 1] = intConvert.c[1];
    pmMessage->msgContent[pmMessage->msgLen + 2] = intConvert.c[2];
    pmMessage->msgContent[pmMessage->msgLen + 3] = intConvert.c[3];

    pmMessage->msgLen += 4;

    memcpy(&pmMessage->msgContent[pmMessage->msgLen], pmInfo.ECNBooksConf.ARCABookConf.retranConn[groupIndex].ip, MAX_LINE_LEN);
    pmMessage->msgLen += MAX_LINE_LEN;

    memcpy(&pmMessage->msgContent[pmMessage->msgLen], &pmInfo.ECNBooksConf.ARCABookConf.retranConn[groupIndex].port, 4);
    pmMessage->msgLen += 4;
  }

  // Block length field
  intConvert.value = pmMessage->msgLen - MSG_HEADER_LEN;
  pmMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  pmMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  pmMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  pmMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = pmMessage->msgLen - MSG_HEADER_LEN;
  pmMessage->msgContent[2] = intConvert.c[0];
  pmMessage->msgContent[3] = intConvert.c[1];
  pmMessage->msgContent[4] = intConvert.c[2];
  pmMessage->msgContent[5] = intConvert.c[3];

  return pmMessage->msgLen;
}

/****************************************************************************
- Function name:  SendConnectCTS_FeedToPM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendConnectCTS_FeedToPM(void)
{
  if ((pmInfo.connection.status == DISCONNECT) || (pmInfo.connection.socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage pmMessage;

  // Build connect CTS Feed message to send PM
  BuildPMMessageForConnectCTS_Feed(&pmMessage);

  // Send message to PM
  if (SendPMMessage(&pmMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request PM connect to CTS Feed to PM\n");

    CloseConnection(&pmInfo.connection);

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendConnectCQS_FeedToPM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendConnectCQS_FeedToPM(void)
{
  if ((pmInfo.connection.status == DISCONNECT) || (pmInfo.connection.socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage pmMessage;

  // Build connect CTS Feed message to send PM
  BuildPMMessageForConnectCQS_Feed(&pmMessage);

  // Send message to PM
  if (SendPMMessage(&pmMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request PM connect to CQS Feed to PM\n");

    CloseConnection(&pmInfo.connection);

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  BuildPMMessageForConnectCQS_Feed
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildPMMessageForConnectCQS_Feed(void *message)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *pmMessage = (t_TSMessage *)message;
  
  // Initialize message
  pmMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_REQUEST_CONNECT_CQS_TO_PM;
  pmMessage->msgContent[0] = shortConvert.c[0];
  pmMessage->msgContent[1] = shortConvert.c[1];
  pmMessage->msgLen += 2;

  // Message body = NULL
  
  // Body length field
  intConvert.value = 0;
  pmMessage->msgContent[2] = intConvert.c[0];
  pmMessage->msgContent[3] = intConvert.c[1];
  pmMessage->msgContent[4] = intConvert.c[2];
  pmMessage->msgContent[5] = intConvert.c[3];
  pmMessage->msgLen += 4;

  return pmMessage->msgLen;
} //end

/****************************************************************************
- Function name:  SendConnectUQDF_FeedToPM
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int SendConnectUQDF_FeedToPM(void)
{
  if ((pmInfo.connection.status == DISCONNECT) || (pmInfo.connection.socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage pmMessage;

  // Build connect UQDF Feed message to send PM
  BuildPMMessageForConnectUQDF_Feed(&pmMessage);

  // Send message to PM
  if (SendPMMessage(&pmMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request PM connect to UQDF Feed to PM\n");

    CloseConnection(&pmInfo.connection);

    return ERROR;
  }

  return SUCCESS;
}


/****************************************************************************
- Function name:  BuildPMMessageForConnectUQDF_Feed
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildPMMessageForConnectUQDF_Feed(void *message)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *pmMessage = (t_TSMessage *)message;

  // Initialize message
  pmMessage->msgLen = 0;

  // Message type
  shortConvert.value = MSG_TYPE_FOR_REQUEST_CONNECT_UQDF_TO_PM;
  pmMessage->msgContent[0] = shortConvert.c[0];
  pmMessage->msgContent[1] = shortConvert.c[1];
  pmMessage->msgLen += 2;

  // Message body = NULL

  // Body length field
  intConvert.value = 0;
  pmMessage->msgContent[2] = intConvert.c[0];
  pmMessage->msgContent[3] = intConvert.c[1];
  pmMessage->msgContent[4] = intConvert.c[2];
  pmMessage->msgContent[5] = intConvert.c[3];
  pmMessage->msgLen += 4;

  return pmMessage->msgLen;
}

/****************************************************************************
- Function name:  BuildPMMessageForConnectCTS_Feed
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildPMMessageForConnectCTS_Feed(void *message)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *pmMessage = (t_TSMessage *)message;
  
  // Initialize message
  pmMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_REQUEST_CONNECT_CTS_TO_PM;
  pmMessage->msgContent[0] = shortConvert.c[0];
  pmMessage->msgContent[1] = shortConvert.c[1];
  pmMessage->msgLen += 2;

  // Message body = NULL
  
  // Body length field
  intConvert.value = 0;
  pmMessage->msgContent[2] = intConvert.c[0];
  pmMessage->msgContent[3] = intConvert.c[1];
  pmMessage->msgContent[4] = intConvert.c[2];
  pmMessage->msgContent[5] = intConvert.c[3];
  pmMessage->msgLen += 4;

  return pmMessage->msgLen;
}

/****************************************************************************
- Function name:  SendDisconnectCTS_FeedToPM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendDisconnectCTS_FeedToPM(void)
{
  if ((pmInfo.connection.status == DISCONNECT) || (pmInfo.connection.socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage pmMessage;

  // Build disconnect CTS Feed message to send PM
  BuildPMMessageForDisconnectCTS_Feed(&pmMessage);

  // Send message to PM
  if (SendPMMessage(&pmMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request PM disconnect to CTS Feed to PM\n");

    CloseConnection(&pmInfo.connection);

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  BuildPMMessageForDisconnectCTS_Feed
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildPMMessageForDisconnectCTS_Feed(void *message)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *pmMessage = (t_TSMessage *)message;
  
  // Initialize message
  pmMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_REQUEST_DISCONNECT_CTS_TO_PM;
  pmMessage->msgContent[0] = shortConvert.c[0];
  pmMessage->msgContent[1] = shortConvert.c[1];
  pmMessage->msgLen += 2;

  // Message body = NULL
  
  // Body length field
  intConvert.value = 0;
  pmMessage->msgContent[2] = intConvert.c[0];
  pmMessage->msgContent[3] = intConvert.c[1];
  pmMessage->msgContent[4] = intConvert.c[2];
  pmMessage->msgContent[5] = intConvert.c[3];
  pmMessage->msgLen += 4;

  return pmMessage->msgLen;
}

/****************************************************************************
- Function name:  SendDisconnectCQS_FeedToPM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendDisconnectCQS_FeedToPM(void)
{
  if ((pmInfo.connection.status == DISCONNECT) || (pmInfo.connection.socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage pmMessage;

  // Build disconnect CQS Feed message to send PM
  BuildPMMessageForDisconnectCQS_Feed(&pmMessage);

  // Send message to PM
  if (SendPMMessage(&pmMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request PM disconnect to CQS Feed to PM\n");

    CloseConnection(&pmInfo.connection);

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  BuildPMMessageForDisconnectCQS_Feed
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildPMMessageForDisconnectCQS_Feed(void *message)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *pmMessage = (t_TSMessage *)message;
  
  // Initialize message
  pmMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_REQUEST_DISCONNECT_CQS_TO_PM;
  pmMessage->msgContent[0] = shortConvert.c[0];
  pmMessage->msgContent[1] = shortConvert.c[1];
  pmMessage->msgLen += 2;

  // Message body = NULL
  
  // Body length field
  intConvert.value = 0;
  pmMessage->msgContent[2] = intConvert.c[0];
  pmMessage->msgContent[3] = intConvert.c[1];
  pmMessage->msgContent[4] = intConvert.c[2];
  pmMessage->msgContent[5] = intConvert.c[3];
  pmMessage->msgLen += 4;

  return pmMessage->msgLen;
}//end

/****************************************************************************
- Function name:  SendDisconnectUQDF_FeedToPM
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int SendDisconnectUQDF_FeedToPM(void)
{
  if ((pmInfo.connection.status == DISCONNECT) || (pmInfo.connection.socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage pmMessage;

  // Build disconnect UQDF Feed message to send PM
  BuildPMMessageForDisconnectUQDF_Feed(&pmMessage);

  // Send message to PM
  if (SendPMMessage(&pmMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request PM disconnect to UQDF Feed to PM\n");

    CloseConnection(&pmInfo.connection);

    return ERROR;
  }

  return SUCCESS;
}


/****************************************************************************
- Function name:  BuildPMMessageForDisconnectUQDF_Feed
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildPMMessageForDisconnectUQDF_Feed(void *message)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *pmMessage = (t_TSMessage *)message;

  // Initialize message
  pmMessage->msgLen = 0;

  // Message type
  shortConvert.value = MSG_TYPE_FOR_REQUEST_DISCONNECT_UQDF_TO_PM;
  pmMessage->msgContent[0] = shortConvert.c[0];
  pmMessage->msgContent[1] = shortConvert.c[1];
  pmMessage->msgLen += 2;
  // Message body = NULL

  // Body length field
  intConvert.value = 0;
  pmMessage->msgContent[2] = intConvert.c[0];
  pmMessage->msgContent[3] = intConvert.c[1];
  pmMessage->msgContent[4] = intConvert.c[2];
  pmMessage->msgContent[5] = intConvert.c[3];
  pmMessage->msgLen += 4;
  
  return pmMessage->msgLen;
}

/****************************************************************************
- Function name:  ProcessPMMessageForHaltStockList
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessPMMessageForHaltStockList(void *message)
{
  t_TSMessage *pmMessage = (t_TSMessage *)message;  

  int currentIndex = MSG_HEADER_LEN;
  int updatedList[MAX_STOCK_SYMBOL];
  int updatedCount = 0;
  int flag = 0;
  int blockLen = GetIntNumber(&pmMessage->msgContent[currentIndex]);

  // Update current index
  currentIndex += 4;

  int numSymbol;
  memcpy(&numSymbol, &pmMessage->msgContent[currentIndex], 4);
  currentIndex += 4;
  
  if ((blockLen - 4 - 4) / (SYMBOL_LEN + 5*PM_MAX_BOOK_CONNECTIONS) != numSymbol)
  {
    TraceLog(ERROR_LEVEL, "Received invalid halt symbol list message from PM");

    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Received halt symbol list from PM, countSymbol = %d\n", numSymbol);

  // start to update data structure and then send symbols' halt status to TSs, TTs
  int stockSymbolIndex;
  unsigned int haltTime;
    
  pthread_mutex_lock(&HaltStatusMgmt.mutexLock);
  
  while (numSymbol > 0)
  {
    numSymbol --;
    stockSymbolIndex = GetStockSymbolIndex((char *) &pmMessage->msgContent[currentIndex]);
    if (stockSymbolIndex != -1)
    {
      flag = 0;
      strncpy(HaltStatusMgmt.symbol[stockSymbolIndex], (char *) &pmMessage->msgContent[currentIndex], SYMBOL_LEN);
      currentIndex += SYMBOL_LEN;
      
      // ARCA Book
      memcpy(&haltTime, &pmMessage->msgContent[currentIndex + 1], 4);
      
      if (haltTime >= HaltStatusMgmt.haltTime[stockSymbolIndex][TS_ARCA_BOOK_INDEX])  //Only take the latest status, also status from PM takes higher priority than from AS
      {
        HaltStatusMgmt.haltTime[stockSymbolIndex][TS_ARCA_BOOK_INDEX] = haltTime;
        HaltStatusMgmt.haltStatus[stockSymbolIndex][TS_ARCA_BOOK_INDEX] = pmMessage->msgContent[currentIndex];
        flag = 1;
      }
      
      currentIndex += 5;
      
      // NASDAQ Book
      memcpy(&haltTime, &pmMessage->msgContent[currentIndex + 1], 4);
      
      if (haltTime >= HaltStatusMgmt.haltTime[stockSymbolIndex][TS_NASDAQ_BOOK_INDEX])  //Only take the latest status, also status from PM takes higher priority than from AS
      {
        HaltStatusMgmt.haltTime[stockSymbolIndex][TS_NASDAQ_BOOK_INDEX] = haltTime;
        HaltStatusMgmt.haltStatus[stockSymbolIndex][TS_NASDAQ_BOOK_INDEX] = pmMessage->msgContent[currentIndex];
        flag = 1;
      }
      
      if (flag == 1)
      {
        updatedList[updatedCount] = stockSymbolIndex;
        updatedCount++;
      }
      
      currentIndex += 5;
    }
    else
    { 
      currentIndex += (SYMBOL_LEN + 10);
    }
  }

  SaveHaltedStockListToFile();
  SendHaltStockListToAllTS(updatedList, updatedCount);
  SendHaltStockListToPM(updatedList, updatedCount);
  SendHaltStockListToAllDaedalus();
  SendHaltStockListToAllTT();

  pthread_mutex_unlock(&HaltStatusMgmt.mutexLock);
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessPMMessageForHaltedSymbol
- Input:      
- Output:     
- Return:     
- Description:    PM sends this message when it detects symbol halted from order rejected
- Usage:      
****************************************************************************/
int ProcessPMMessageForHaltedSymbol(void *message)
{
  t_TSMessage *pmMessage = (t_TSMessage *)message;  

  int currentIndex = MSG_HEADER_LEN;

  int blockLen = GetIntNumber(&pmMessage->msgContent[currentIndex]);

  // Update current index
  currentIndex += 4;
  
  if ((blockLen - 4 - 1) != (SYMBOL_LEN + 5*PM_MAX_BOOK_CONNECTIONS))
  {
    TraceLog(ERROR_LEVEL, "Received invalid halt symbol message from PM");

    return ERROR;
  }

  int willApplyToAllVenues = pmMessage->msgContent[currentIndex];
  currentIndex++;
  
  // start to update data structure and then send symbols' halt status to TSs, TTs
  int stockSymbolIndex;
  unsigned int haltTime;
    
  pthread_mutex_lock(&HaltStatusMgmt.mutexLock);
  
  stockSymbolIndex = GetStockSymbolIndex((char *) &pmMessage->msgContent[currentIndex]);
  if (stockSymbolIndex != -1)
  {
    strncpy(HaltStatusMgmt.symbol[stockSymbolIndex], (char *) &pmMessage->msgContent[currentIndex], SYMBOL_LEN);
    currentIndex += SYMBOL_LEN;
      
    // ARCA Book
    memcpy(&haltTime, &pmMessage->msgContent[currentIndex + 1], 4);
      
    if (haltTime >= HaltStatusMgmt.haltTime[stockSymbolIndex][TS_ARCA_BOOK_INDEX])  //Only take the latest status, also status from PM takes higher priority than from AS
    {
      HaltStatusMgmt.haltTime[stockSymbolIndex][TS_ARCA_BOOK_INDEX] = haltTime;
      HaltStatusMgmt.haltStatus[stockSymbolIndex][TS_ARCA_BOOK_INDEX] = pmMessage->msgContent[currentIndex];
    }
    currentIndex += 5;
      
    // NASDAQ Book
    memcpy(&haltTime, &pmMessage->msgContent[currentIndex + 1], 4);
      
    if (haltTime >= HaltStatusMgmt.haltTime[stockSymbolIndex][TS_NASDAQ_BOOK_INDEX])  //Only take the latest status, also status from PM takes higher priority than from AS
    {
      HaltStatusMgmt.haltTime[stockSymbolIndex][TS_NASDAQ_BOOK_INDEX] = haltTime;
      HaltStatusMgmt.haltStatus[stockSymbolIndex][TS_NASDAQ_BOOK_INDEX] = pmMessage->msgContent[currentIndex];
    }
    
    if (willApplyToAllVenues == 1)
    {
      /* Will halt on all other venues
        In this case the $haltTime in the message is identical for ARCA and NASDAQ
        Also, in this case, the halt status is 'Halt' */
      
      int i;
      for (i = 0; i < TS_MAX_BOOK_CONNECTIONS; i++)
      {
        if (haltTime > HaltStatusMgmt.haltTime[stockSymbolIndex][i])  //Only take the latest status
        {
          HaltStatusMgmt.haltTime[stockSymbolIndex][i] = haltTime;
          HaltStatusMgmt.haltStatus[stockSymbolIndex][i] = TRADING_HALTED;
        }
      }
    }
  }
  else
  {
    TraceLog(ERROR_LEVEL, "(ProcessPMMessageForHaltedSymbol): Could not add symbol '%.8s' to stock list\n", &pmMessage->msgContent[currentIndex]);
    pthread_mutex_unlock(&HaltStatusMgmt.mutexLock);
    return ERROR;
  }

  SaveHaltedStockListToFile();
  
  SendAHaltStockToAllTS(stockSymbolIndex);
  SendAHaltStockToPM(stockSymbolIndex);
  SendAHaltStockToAllDaedalus(stockSymbolIndex);
  SendAHaltStockToAllTT(stockSymbolIndex);

  pthread_mutex_unlock(&HaltStatusMgmt.mutexLock);
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendHaltStockListToPM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendHaltStockListToPM(int updatedList[MAX_STOCK_SYMBOL], int updatedCount)
{
  if ((pmInfo.connection.status == DISCONNECT) || (pmInfo.connection.socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage pmMessage;

  // Build get halted stock list message to send PM
  int count = BuildPMMessageForHaltedStockList(&pmMessage, updatedList, updatedCount);
  if (count == 0) return SUCCESS;

  // Send message to PM
  if (SendPMMessage(&pmMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send halt stock list to PM\n");

    CloseConnection(&pmInfo.connection);

    return ERROR;
  }
  else
  {
    TraceLog(DEBUG_LEVEL, "Sent %d halt stock(s) to PM\n", count);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendAHaltStockToPM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendAHaltStockToPM(const int stockSymbolIndex)
{
  if ((pmInfo.connection.status == DISCONNECT) || (pmInfo.connection.socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage pmMessage;

  // Build get halted stock list message to send PM
  if (BuildPMMessageForHaltSymbol(&pmMessage, stockSymbolIndex) == 0) return SUCCESS;   //There is nothing to send

  // Send message to PM
  if (SendPMMessage(&pmMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send a halt stock to PM\n");

    CloseConnection(&pmInfo.connection);

    return ERROR;
  }
  else
  {
    TraceLog(DEBUG_LEVEL, "Sent a halt stock to PM\n");
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  BuildPMMessageForHaltedStockList
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildPMMessageForHaltedStockList(void *message, int updatedList[MAX_STOCK_SYMBOL], int updatedCount)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *pmMessage = (t_TSMessage *)message;
  
  // Initialize message
  pmMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_HALT_LIST_TO_PM;
  pmMessage->msgContent[0] = shortConvert.c[0];
  pmMessage->msgContent[1] = shortConvert.c[1];
  pmMessage->msgLen += 2;

  // Update index of body length
  pmMessage->msgLen += 4;
  
  // Update index of block length
  pmMessage->msgLen += 4;

  pmMessage->msgLen += 4; //By pass number of symbols
  
  int i, count = 0;
  int willSend;
  
  if (updatedCount == -1)
  {
    for (i = 0; i < RMList.countSymbol; i++)
    {
      willSend = HaltStatusMgmt.haltTime[i][TS_ARCA_BOOK_INDEX] + HaltStatusMgmt.haltTime[i][TS_NASDAQ_BOOK_INDEX];
      
      if (willSend)
      {
        count++;
        memcpy(&pmMessage->msgContent[pmMessage->msgLen], HaltStatusMgmt.symbol[i], SYMBOL_LEN);
        pmMessage->msgLen += SYMBOL_LEN;
        
        pmMessage->msgContent[pmMessage->msgLen++] = HaltStatusMgmt.isAutoResume[i];
        
        pmMessage->msgContent[pmMessage->msgLen++] = HaltStatusMgmt.haltStatus[i][TS_ARCA_BOOK_INDEX];
          
        memcpy(&pmMessage->msgContent[pmMessage->msgLen], &HaltStatusMgmt.haltTime[i][TS_ARCA_BOOK_INDEX], 4);
        pmMessage->msgLen += 4;
        
        pmMessage->msgContent[pmMessage->msgLen++] = HaltStatusMgmt.haltStatus[i][TS_NASDAQ_BOOK_INDEX];
          
        memcpy(&pmMessage->msgContent[pmMessage->msgLen], &HaltStatusMgmt.haltTime[i][TS_NASDAQ_BOOK_INDEX], 4);
        pmMessage->msgLen += 4;
      }
    }
  }
  else  //Only send from updated list
  {
    int stockSymbolIndex;
    for (i = 0; i < updatedCount; i++)
    {
      stockSymbolIndex = updatedList[i];
      willSend = HaltStatusMgmt.haltTime[stockSymbolIndex][TS_ARCA_BOOK_INDEX] + HaltStatusMgmt.haltTime[stockSymbolIndex][TS_NASDAQ_BOOK_INDEX];
      
      if (willSend)
      {
        count++;
        memcpy(&pmMessage->msgContent[pmMessage->msgLen], HaltStatusMgmt.symbol[stockSymbolIndex], SYMBOL_LEN);
        pmMessage->msgLen += SYMBOL_LEN;
        
        pmMessage->msgContent[pmMessage->msgLen++] = HaltStatusMgmt.isAutoResume[stockSymbolIndex];
        
        pmMessage->msgContent[pmMessage->msgLen++] = HaltStatusMgmt.haltStatus[stockSymbolIndex][TS_ARCA_BOOK_INDEX];
          
        memcpy(&pmMessage->msgContent[pmMessage->msgLen], &HaltStatusMgmt.haltTime[stockSymbolIndex][TS_ARCA_BOOK_INDEX], 4);
        pmMessage->msgLen += 4;
        
        pmMessage->msgContent[pmMessage->msgLen++] = HaltStatusMgmt.haltStatus[stockSymbolIndex][TS_NASDAQ_BOOK_INDEX];
          
        memcpy(&pmMessage->msgContent[pmMessage->msgLen], &HaltStatusMgmt.haltTime[stockSymbolIndex][TS_NASDAQ_BOOK_INDEX], 4);
        pmMessage->msgLen += 4;
      }
    }
  }
  
  //Update 'count' field
  memcpy(&pmMessage->msgContent[10], &count, 4);
  
  // Block length field
  intConvert.value = pmMessage->msgLen - MSG_HEADER_LEN;
  pmMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  pmMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  pmMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  pmMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = pmMessage->msgLen - MSG_HEADER_LEN;
  pmMessage->msgContent[2] = intConvert.c[0];
  pmMessage->msgContent[3] = intConvert.c[1];
  pmMessage->msgContent[4] = intConvert.c[2];
  pmMessage->msgContent[5] = intConvert.c[3];

  return count;
}

/****************************************************************************
- Function name:  BuildPMMessageForHaltSymbol
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildPMMessageForHaltSymbol(void *message, const int stockSymbolIndex)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *pmMessage = (t_TSMessage *)message;
  
  // Initialize message
  pmMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_A_HALT_STOCK_TO_PM;
  pmMessage->msgContent[0] = shortConvert.c[0];
  pmMessage->msgContent[1] = shortConvert.c[1];
  pmMessage->msgLen += 2;

  // Update index of body length
  pmMessage->msgLen += 4;
  
  // Update index of block length
  pmMessage->msgLen += 4;

  int willSend = HaltStatusMgmt.haltTime[stockSymbolIndex][TS_ARCA_BOOK_INDEX] + HaltStatusMgmt.haltTime[stockSymbolIndex][TS_NASDAQ_BOOK_INDEX];
    
  if (willSend)
  {
    memcpy(&pmMessage->msgContent[pmMessage->msgLen], HaltStatusMgmt.symbol[stockSymbolIndex], SYMBOL_LEN);
    pmMessage->msgLen += SYMBOL_LEN;
    
    pmMessage->msgContent[pmMessage->msgLen++] = HaltStatusMgmt.isAutoResume[stockSymbolIndex];
    
    pmMessage->msgContent[pmMessage->msgLen++] = HaltStatusMgmt.haltStatus[stockSymbolIndex][TS_ARCA_BOOK_INDEX];
        
    memcpy(&pmMessage->msgContent[pmMessage->msgLen], &HaltStatusMgmt.haltTime[stockSymbolIndex][TS_ARCA_BOOK_INDEX], 4);
    pmMessage->msgLen += 4;
      
    pmMessage->msgContent[pmMessage->msgLen++] = HaltStatusMgmt.haltStatus[stockSymbolIndex][TS_NASDAQ_BOOK_INDEX];
        
    memcpy(&pmMessage->msgContent[pmMessage->msgLen], &HaltStatusMgmt.haltTime[stockSymbolIndex][TS_NASDAQ_BOOK_INDEX], 4);
    pmMessage->msgLen += 4;
  }
  else
  {
    return 0;
  }
  
  // Block length field
  intConvert.value = pmMessage->msgLen - MSG_HEADER_LEN;
  pmMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  pmMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  pmMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  pmMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = pmMessage->msgLen - MSG_HEADER_LEN;
  pmMessage->msgContent[2] = intConvert.c[0];
  pmMessage->msgContent[3] = intConvert.c[1];
  pmMessage->msgContent[4] = intConvert.c[2];
  pmMessage->msgContent[5] = intConvert.c[3];

  return 1;
}

/****************************************************************************
- Function name:  SendGetTradedListToPM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendGetTradedListToPM(int ttIndex, char *symbol)
{
  if ((pmInfo.connection.status == DISCONNECT) || (pmInfo.connection.socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage pmMessage;

  // Build get traded list message to send PM
  BuildPMMessageForGetTradedList(&pmMessage, ttIndex, symbol);

  // Send message to PM
  if (SendPMMessage(&pmMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request to get traded list to PM\n");

    CloseConnection(&pmInfo.connection);

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  BuildPMMessageForGetTradedList
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildPMMessageForGetTradedList(void *message, int ttIndex, char *symbol)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *pmMessage = (t_TSMessage *)message;
  
  // Initialize message
  pmMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_GET_TRADED_LIST_TO_PM;
  pmMessage->msgContent[0] = shortConvert.c[0];
  pmMessage->msgContent[1] = shortConvert.c[1];
  pmMessage->msgLen += 2;

  // Update index of body length
  pmMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  
  // Update index of block length
  pmMessage->msgLen += 4;

  // Block content = TT Id (4 byte) + Symbol (8 bytes)

  // TT Id (4 byte)
  intConvert.value = ttIndex;
  pmMessage->msgContent[pmMessage->msgLen] = intConvert.c[0];
  pmMessage->msgContent[pmMessage->msgLen + 1] = intConvert.c[1];
  pmMessage->msgContent[pmMessage->msgLen + 2] = intConvert.c[2];
  pmMessage->msgContent[pmMessage->msgLen + 3] = intConvert.c[3];
  pmMessage->msgLen += 4;

  // Symbol (8 bytes)
  strncpy((char *) &pmMessage->msgContent[pmMessage->msgLen], symbol, SYMBOL_LEN);
  pmMessage->msgLen += SYMBOL_LEN;

  // Block length field
  intConvert.value = SYMBOL_LEN + 4 + 4;
  pmMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  pmMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  pmMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  pmMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = pmMessage->msgLen - MSG_HEADER_LEN;
  pmMessage->msgContent[2] = intConvert.c[0];
  pmMessage->msgContent[3] = intConvert.c[1];
  pmMessage->msgContent[4] = intConvert.c[2];
  pmMessage->msgContent[5] = intConvert.c[3];

  return pmMessage->msgLen;
}

/****************************************************************************
- Function name:  ProcessPMMessageForDisengageAlert
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessPMMessageForDisengageAlert(void *message)
{
  t_TSMessage *pmMessage = (t_TSMessage *)message;  
  int currentIndex = 10;
  
  // Symbol
  char symbol[SYMBOL_LEN];
  strncpy(symbol, (char *) &pmMessage->msgContent[currentIndex], SYMBOL_LEN);
  currentIndex += 8;  

  // Price
  double price;
  memcpy(&price, &pmMessage->msgContent[currentIndex], 8);
  currentIndex += 8;

  // Share
  int share;
  memcpy(&share, &pmMessage->msgContent[currentIndex], 4);
  currentIndex += 4;

  // Side
  int side = pmMessage->msgContent[currentIndex];
  currentIndex += 1;

  // Account
  int account = pmMessage->msgContent[currentIndex];
  currentIndex += 1;
  
  if (account < 0 || account > MAX_ACCOUNT)
  {
    TraceLog(ERROR_LEVEL, "Received disengage alert from PM: Invalid account %d for symbol %.8s\n", account, symbol);   
    return ERROR;
  }

  // Reason code, explains why the stuck is disengaged
  int reason = pmMessage->msgContent[currentIndex];
  currentIndex += 1;

  int symbolIndex = GetStockSymbolIndex(symbol);
  if (symbolIndex != -1)
  {
    verifyPositionInfo[account][symbolIndex].isTrading = TRADING_NONE;
    verifyPositionInfo[account][symbolIndex].isSleeping = NO;
    verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
    verifyPositionInfo[account][symbolIndex].isPMOwner = NO;
  }
  else
  {
    TraceLog(ERROR_LEVEL, "Received disengage alert from PM: Invalid symbol %.8s\n", symbol);
    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "DisengageAlert: symbol = %.8s, share = %d, account = %d, reason = %d\n", symbol, share, account, reason);

  if (reason != DISENGAGE_REASON_TRADER_REQUESTED_DISENGAGE)
  {
    // Send release alert to TT
    if (price < 0.0001)
    {
      price = RMList.collection[account][symbolIndex].avgPrice;
    }

    BuildNSendPMReleasePositionAlertToAllDaedalus(symbol, price, share, side, account);
    BuildNSendPMReleasePositionAlertToAllTT(symbol, price, share, side, account);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessPMMessageForTradedList
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessPMMessageForTradedList(void *message)
{
  t_TSMessage *pmMessage = (t_TSMessage *)message;  

  int currentIndex = MSG_HEADER_LEN;

  // Body = block len + block body (TT sender + symbol + N * (participantId + timestamp + shares + price))

  // Block len
  int blockLen = GetIntNumber(&pmMessage->msgContent[currentIndex]);
  currentIndex += 4;

  if ((blockLen - 8 - SYMBOL_LEN) % (2 + 4 + 4 + 8) != 0)
  {
    TraceLog(ERROR_LEVEL, "Invalid traded list message from RM: blockLen = %d\n", blockLen);

    return ERROR;
  }

  // TT sender
  int ttId = GetIntNumber(&pmMessage->msgContent[currentIndex]);
  currentIndex += 4;

  TraceLog(DEBUG_LEVEL, "Received traded list message from PM: ttSender = %d, symbol = %.8s, blockLen = %d\n", traderToolsInfo.connection[ttId].socket, &pmMessage->msgContent[currentIndex], blockLen);

  // Call function to send traded list to Trader Tool
  SendTradedListToTT(ttId, pmMessage);

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendConnectUTDFToPM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendConnectUTDFToPM(void)
{
  if ((pmInfo.connection.status == DISCONNECT) || (pmInfo.connection.socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage pmMessage;

  // Build connect UTDF message to send PM
  BuildPMMessageForConnectUTDF(&pmMessage);

  // Send message to PM
  if (SendPMMessage(&pmMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request PM connect to UTDF to PM\n");

    CloseConnection(&pmInfo.connection);

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  BuildPMMessageForConnectUTDF
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildPMMessageForConnectUTDF(void *message)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *pmMessage = (t_TSMessage *)message;
  
  // Initialize message
  pmMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_REQUEST_CONNECT_UTDF_TO_PM;
  pmMessage->msgContent[0] = shortConvert.c[0];
  pmMessage->msgContent[1] = shortConvert.c[1];
  pmMessage->msgLen += 2;

  // Message body = NULL
  
  // Body length field
  intConvert.value = 0;
  pmMessage->msgContent[2] = intConvert.c[0];
  pmMessage->msgContent[3] = intConvert.c[1];
  pmMessage->msgContent[4] = intConvert.c[2];
  pmMessage->msgContent[5] = intConvert.c[3];
  pmMessage->msgLen += 4;

  return pmMessage->msgLen;
}

/****************************************************************************
- Function name:  SendDisconnectUTDFToPM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendDisconnectUTDFToPM(void)
{
  if ((pmInfo.connection.status == DISCONNECT) || (pmInfo.connection.socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage pmMessage;

  // Build disconnect UTDF message to send PM
  BuildPMMessageForDisconnectUTDF(&pmMessage);

  // Send message to PM
  if (SendPMMessage(&pmMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request PM disconnect to UTDF to PM\n");

    CloseConnection(&pmInfo.connection);

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  BuildPMMessageForDisconnectUTDF
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildPMMessageForDisconnectUTDF(void *message)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *pmMessage = (t_TSMessage *)message;
  
  // Initialize message
  pmMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_REQUEST_DISCONNECT_UTDF_TO_PM;
  pmMessage->msgContent[0] = shortConvert.c[0];
  pmMessage->msgContent[1] = shortConvert.c[1];
  pmMessage->msgLen += 2;

  // Message body = NULL
  
  // Body length field
  intConvert.value = 0;
  pmMessage->msgContent[2] = intConvert.c[0];
  pmMessage->msgContent[3] = intConvert.c[1];
  pmMessage->msgContent[4] = intConvert.c[2];
  pmMessage->msgContent[5] = intConvert.c[3];
  pmMessage->msgLen += 4;

  return pmMessage->msgLen;
}

/****************************************************************************
- Function name:  ProcessPMMessageForBookIdleWarning
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessPMMessageForBookIdleWarning(void *message)
{
  t_TSMessage *pmMessage = (t_TSMessage *)message;  

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // ECN Book Id (4 byte)
  int pmECNid = GetIntNumber(&pmMessage->msgContent[currentIndex]);
  currentIndex += 4;
  
  //GroupID
  int groupID = GetIntNumber(&pmMessage->msgContent[currentIndex]);
  currentIndex += 4;

  TraceLog(DEBUG_LEVEL, "Receive Book status idle from PM, Book(%d) Group(%d)\n", pmECNid, groupID);

  //Send the warning to TT
  SendTSPMBookIdleWarningToAllDaedalus(99, pmECNid, groupID);
  SendTSPMBookIdleWarningToAllTT(99, pmECNid, groupID);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessPMMessageForBookIdleWarning
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessConnectToPMAndAllPMConnection(void)
{
  if (pmInfo.connection.status == DISCONNECT)
  {
    pmSetting.isConnectAll = YES;
    Connect2PM();
  }
  else
  {
    pmSetting.isConnectAll = NO;
    ProcessPMConnectAllConnection();
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessDisconnectToPMAndAllPMConnection
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessDisconnectToPMAndAllPMConnection(void)
{
  //Disconnect all connections of PM
  ProcessPMDisconnectAllConnection();
  
  sleep(1);
  
  //Disconnect PM from AS
  Disconnect2PM();
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessPMMessageForExchangeConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessPMMessageForExchangeConf(void *message)
{
  TraceLog(DEBUG_LEVEL, "PM: Receive PM Exchange Configuration\n");

  t_TSMessage *pmMessage = (t_TSMessage *)message;  

  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;

  // Block content = PM Exchange Configuration

  int i;

  for (i = 0; i < MAX_EXCHANGE; i++)
  {
    //exchangeId
    PMExchangeStatusList[i].exchangeId = pmMessage->msgContent[currentIndex];
    currentIndex += 1;
    
    //ECNToPlace
    memcpy(PMExchangeStatusList[i].ECNToPlace, &pmMessage->msgContent[currentIndex], MAX_LINE_LEN);
    currentIndex += MAX_LINE_LEN;
    
    //CQS Status
    PMExchangeStatusList[i].status[CQS_SOURCE].visible = pmMessage->msgContent[currentIndex];
    PMExchangeStatusList[i].status[CQS_SOURCE].enable = pmMessage->msgContent[currentIndex + 1];
    currentIndex += 2;
    
    //UQDF Status
    PMExchangeStatusList[i].status[UQDF_SOURCE].visible = pmMessage->msgContent[currentIndex];
    PMExchangeStatusList[i].status[UQDF_SOURCE].enable = pmMessage->msgContent[currentIndex + 1];
    currentIndex += 2;
  }

  // Call function to send PM Exchange configuration to all Trader Tool
  SendPMExchangesConfToAllDaedalus();
  SendPMExchangesConfToAllTT();

  return SUCCESS;
}


/****************************************************************************
- Function name:  SetPMExchangeConfToPM
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int SetPMExchangeConfToPM(void)
{
  if ((pmInfo.connection.status == DISCONNECT) || (pmInfo.connection.socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage pmMessage;

  // Build set PM Exchange configuration message to send PM
  BuildPMExchangeMessageForSetPMExchangeConf(&pmMessage);

  TraceLog(DEBUG_LEVEL, "Send PM Exchange configuration to PM\n");

  // Send message to PM
  if (SendPMMessage(&pmMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request to set PM Exchange configuration to PM\n");

    CloseConnection(&pmInfo.connection);

    return ERROR;
  }

  return SUCCESS;
}


/****************************************************************************
- Function name:  BuildPMExchangeMessageForSetPMExchangeConf
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildPMExchangeMessageForSetPMExchangeConf(void *message)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *pmMessage = (t_TSMessage *)message;

  // Initialize message
  pmMessage->msgLen = 0;

  // Message type
  shortConvert.value = MSG_TYPE_FOR_SET_PM_EXCHANGE_CONF_TO_PM;
  pmMessage->msgContent[0] = shortConvert.c[0];
  pmMessage->msgContent[1] = shortConvert.c[1];
  pmMessage->msgLen += 2;

  // Update index of body length
  pmMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))

  // Update index of block length
  pmMessage->msgLen += 4;

  // Block content = PM Exchange configuration

  // PM Exchange configuration
  memcpy(&pmMessage->msgContent[pmMessage->msgLen], &PMExchangeStatusList, sizeof(t_PMExchangeInfo) * MAX_EXCHANGE);
  pmMessage->msgLen += sizeof(t_PMExchangeInfo) * MAX_EXCHANGE;

  // Block length field
  intConvert.value = pmMessage->msgLen - MSG_HEADER_LEN;
  pmMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  pmMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  pmMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  pmMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];

  // Body length field
  intConvert.value = pmMessage->msgLen - MSG_HEADER_LEN;
  pmMessage->msgContent[2] = intConvert.c[0];
  pmMessage->msgContent[3] = intConvert.c[1];
  pmMessage->msgContent[4] = intConvert.c[2];
  pmMessage->msgContent[5] = intConvert.c[3];

  return pmMessage->msgLen;
}

/****************************************************************************
- Function name:  ProcessPMMessageForBBOQuotes
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessPMMessageForBBOQuotes(void *message)
{
  t_TSMessage *pmMessage = (t_TSMessage *)message;

  int currentIndex = MSG_HEADER_LEN;

  // Body = 3 block (block sender + block ASK + block BID)

  // Update current index
  currentIndex += 4;

  //ttIndex (1 byte)
  int ttIndex = pmMessage->msgContent[currentIndex];

  // Call function to send BBO Quotes to Trader Tool
  SendPMBBOQuotesToTT(ttIndex, pmMessage);

  return SUCCESS;
}


/****************************************************************************
- Function name:  SendGetPMBBOQuotesToPM
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int SendGetPMBBOQuotesToPM(char *symbol, int ttIndex)
{
  if (pmInfo.connection.status == DISCONNECT || pmInfo.connection.socket == -1)
  {
    return SUCCESS;
  }

  t_ShortConverter shortNum;
  t_IntConverter intNum;

  t_TSMessage pmMessage;

  // Initialize message
  pmMessage.msgLen = 0;

  // Message type
  shortNum.value = MSG_TYPE_FOR_GET_PM_BBO_QUOTE_LIST_TO_PM;
  pmMessage.msgContent[0] = shortNum.c[0];
  pmMessage.msgContent[1] = shortNum.c[1];
  pmMessage.msgLen += 2;

  // Update index of body length
  pmMessage.msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))

  // Update index of block length
  pmMessage.msgLen += 4;

  // Block content = ttIndex + Symbol
  // ttIndex (1 byte)
  pmMessage.msgContent[pmMessage.msgLen] = ttIndex;
  pmMessage.msgLen += 1;

  // Symbol
  strncpy((char *)&pmMessage.msgContent[pmMessage.msgLen], symbol, SYMBOL_LEN);
  pmMessage.msgLen += SYMBOL_LEN;

  // Block length field
  intNum.value = pmMessage.msgLen - MSG_HEADER_LEN;
  pmMessage.msgContent[MSG_HEADER_LEN] = intNum.c[0];
  pmMessage.msgContent[MSG_HEADER_LEN + 1] = intNum.c[1];
  pmMessage.msgContent[MSG_HEADER_LEN + 2] = intNum.c[2];
  pmMessage.msgContent[MSG_HEADER_LEN + 3] = intNum.c[3];

  // Body length field
  intNum.value = pmMessage.msgLen - MSG_HEADER_LEN;
  pmMessage.msgContent[2] = intNum.c[0];
  pmMessage.msgContent[3] = intNum.c[1];
  pmMessage.msgContent[4] = intNum.c[2];
  pmMessage.msgContent[5] = intNum.c[3];

  // Send message to Trade Server
  if (SendPMMessage(&pmMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request PM get BBO quotes to PM\n");

    CloseConnection(&pmInfo.connection);

    return ERROR;
  }

  return SUCCESS;
}


/****************************************************************************
- Function name:  SendFlushPMBBOQuotesToPM
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int SendFlushPMBBOQuotesToPM(int exchangeId)
{
  if (pmInfo.connection.status == DISCONNECT || pmInfo.connection.socket == -1)
  {
    return SUCCESS;
  }

  t_ShortConverter shortNum;
  t_IntConverter intNum;

  t_TSMessage pmMessage;

  // Initialize message
  pmMessage.msgLen = 0;

  // Message type
  shortNum.value = MSG_TYPE_FOR_SEND_FLUSH_PM_BBO_QUOTES_TO_PM;
  pmMessage.msgContent[0] = shortNum.c[0];
  pmMessage.msgContent[1] = shortNum.c[1];
  pmMessage.msgLen += 2;

  // Update index of body length
  pmMessage.msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  
  // Block length field
  intNum.value = 5;
  pmMessage.msgContent[MSG_HEADER_LEN] = intNum.c[0];
  pmMessage.msgContent[MSG_HEADER_LEN + 1] = intNum.c[1];
  pmMessage.msgContent[MSG_HEADER_LEN + 2] = intNum.c[2];
  pmMessage.msgContent[MSG_HEADER_LEN + 3] = intNum.c[3];
  pmMessage.msgLen += 4;
  
  // Block content = ECN Book Id
  pmMessage.msgContent[pmMessage.msgLen] = exchangeId;
  pmMessage.msgLen += 1;

  // Body length field
  intNum.value = 5;
  pmMessage.msgContent[2] = intNum.c[0];
  pmMessage.msgContent[3] = intNum.c[1];
  pmMessage.msgContent[4] = intNum.c[2];
  pmMessage.msgContent[5] = intNum.c[3];

  // Send message to PM
  if (SendPMMessage(&pmMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request PM flush BBO quotes to PM\n");

    CloseConnection(&pmInfo.connection);

    return ERROR;
  }

  return SUCCESS;
}


/****************************************************************************
- Function name:  SendFlushASymbolToPM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendFlushASymbolToPM(int pmECNIndex, unsigned char symbol[SYMBOL_LEN])
{
  if ((pmInfo.connection.status == DISCONNECT) || (pmInfo.connection.socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage pmMessage;

  // Build flush a symbol message to send PM
  BuildPMMessageForFlushASymbol(&pmMessage, pmECNIndex, symbol);

  // Send message to Trade Server
  if (SendPMMessage(&pmMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request flush a symbol to PM\n");

    CloseConnection(&pmInfo.connection);

    return ERROR;
  }

  return SUCCESS;
}


/****************************************************************************
- Function name:  BuildPMMessageForFlushASymbol
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildPMMessageForFlushASymbol(void *message, int pmECNIndex, unsigned char symbol[SYMBOL_LEN])
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *pmMessage = (t_TSMessage *)message;
  
  // Initialize message
  pmMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_FLUSH_A_SYMBOL_TO_PM;
  pmMessage->msgContent[0] = shortConvert.c[0];
  pmMessage->msgContent[1] = shortConvert.c[1];
  pmMessage->msgLen += 2;

  // Update index of body length
  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  pmMessage->msgLen += 4;
  
  // Update index of block length
  // Block content = ECN Id (1 byte) + Symbol (8 bytes)
  pmMessage->msgLen += 4;
  
  // ECN Id (1 byte)
  pmMessage->msgContent[pmMessage->msgLen] = pmECNIndex;
  pmMessage->msgLen += 1;

  // Symbol (8 bytes)
  strncpy((char *) &pmMessage->msgContent[pmMessage->msgLen], (char *) symbol, SYMBOL_LEN);
  pmMessage->msgLen += SYMBOL_LEN;

  // Block length field
  intConvert.value = SYMBOL_LEN + 5;
  pmMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  pmMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  pmMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  pmMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = pmMessage->msgLen - MSG_HEADER_LEN;
  pmMessage->msgContent[2] = intConvert.c[0];
  pmMessage->msgContent[3] = intConvert.c[1];
  pmMessage->msgContent[4] = intConvert.c[2];
  pmMessage->msgContent[5] = intConvert.c[3];

  return pmMessage->msgLen;
}


/****************************************************************************
- Function name:  SendFlushAllSymbolToPM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendFlushAllSymbolToPM(int pmECNIndex)
{
  if ((pmInfo.connection.status == DISCONNECT) || (pmInfo.connection.socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage pmMessage;

  TraceLog(DEBUG_LEVEL, "Send request to flush book for all symbol, pmECNIndex = %d\n", pmECNIndex);

  // Build flush all symbol message to send PM
  BuildPMMessageForFlushAllSymbol(&pmMessage, pmECNIndex);

  // Send message to Trade Server
  if (SendPMMessage(&pmMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send request flush all symbol to PM\n");

    CloseConnection(&pmInfo.connection);

    return ERROR;
  }

  return SUCCESS;
}


/****************************************************************************
- Function name:  BuildPMMessageForFlushAllSymbol
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildPMMessageForFlushAllSymbol(void *message, int pmECNIndex)
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *pmMessage = (t_TSMessage *)message;
  
  // Initialize message
  pmMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_FLUSH_ALL_SYMBOL_TO_PM;
  pmMessage->msgContent[0] = shortConvert.c[0];
  pmMessage->msgContent[1] = shortConvert.c[1];
  pmMessage->msgLen += 2;

  // Update index of body length
  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  pmMessage->msgLen += 4;

  // Update index of block length
  // Block content = ECN Id (1 bytes)
  pmMessage->msgLen += 4;

  // ECN Id (1 byte)
  pmMessage->msgContent[pmMessage->msgLen] = pmECNIndex;
  pmMessage->msgLen += 1;

  // Block length field
  intConvert.value = 5;
  pmMessage->msgContent[MSG_HEADER_LEN] = intConvert.c[0];
  pmMessage->msgContent[MSG_HEADER_LEN + 1] = intConvert.c[1];
  pmMessage->msgContent[MSG_HEADER_LEN + 2] = intConvert.c[2];
  pmMessage->msgContent[MSG_HEADER_LEN + 3] = intConvert.c[3];
  
  // Body length field
  intConvert.value = pmMessage->msgLen - MSG_HEADER_LEN;
  pmMessage->msgContent[2] = intConvert.c[0];
  pmMessage->msgContent[3] = intConvert.c[1];
  pmMessage->msgContent[4] = intConvert.c[2];
  pmMessage->msgContent[5] = intConvert.c[3];

  return pmMessage->msgLen;
}

/****************************************************************************
- Function name:  ProcessPMMessageForPositionStatusResponseAlert
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessPMMessageForPositionStatusResponseAlert(void *message)
{
  t_TSMessage *pmMessage = (t_TSMessage *)message;

  int currentIndex = MSG_HEADER_LEN + 4;

  char symbol[SYMBOL_LEN];
  double price;
  int share, account;
  char side, status;

  // Symbol
  memcpy(symbol, &pmMessage->msgContent[currentIndex], SYMBOL_LEN);
  currentIndex += SYMBOL_LEN;

  // Price
  memcpy(&price, &pmMessage->msgContent[currentIndex], 8);
  currentIndex += 8;

  // Share
  memcpy(&share, &pmMessage->msgContent[currentIndex], 4);
  currentIndex += 4;

  // Side
  side = pmMessage->msgContent[currentIndex];
  currentIndex += 1;
  
  // Account
  account = pmMessage->msgContent[currentIndex];
  currentIndex += 1;

  // Reason status, explains why the stuck is acepted or not
  status = pmMessage->msgContent[currentIndex];
  currentIndex += 1;

  // Reason code, explains why the stuck is disengaged
  //reason = pmMessage->msgContent[currentIndex];

  if (status == 0) // does not accept
  {
    //Remove this symbol from verify position info list
    int symbolIndex = GetStockSymbolIndex(symbol);
    if (symbolIndex != -1)
    {
      verifyPositionInfo[account][symbolIndex].isTrading = TRADING_NONE;
      verifyPositionInfo[account][symbolIndex].isSleeping = NO;
      verifyPositionInfo[account][symbolIndex].isPMOwner = NO;
      verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
    }
    else
    {
      TraceLog(ERROR_LEVEL, "Received position response from PM: Invalid symbol %s\n", symbol);
    }

    BuildNSendPMDoesNotAcceptPositionAlertToAllDaedalus(symbol, price, share, side, account);
    BuildNSendPMDoesNotAcceptPositionAlertToAllTT(symbol, price, share, side, account);
  }
  else
  {
    int symbolIndex = GetStockSymbolIndex(symbol);
    if (symbolIndex != -1)
    {
      // The alert flag will be reset whenever a new stuck appears
      verifyPositionInfo[account][symbolIndex].alreadyAlerted = NO;
  
      if (verifyPositionInfo[account][symbolIndex].isPMOwner == NO)
      {
        verifyPositionInfo[account][symbolIndex].pmHandleInSeconds = GetNumberOfSecondsOfLocalTime();
        verifyPositionInfo[account][symbolIndex].isPMOwner = YES;
        verifyPositionInfo[account][symbolIndex].isSleeping = NO;
      }
    }
    else
    {
      TraceLog(ERROR_LEVEL, "Received position response from PM: Invalid symbol %s\n", symbol);
    }
    
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  EnqueueStuckOfSameSymbolToPM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int EnqueueStuckOfSameSymbolToPM(char *symbol, int symbolIndex, int account)
{
  /*  Stuck of this symbol on this account has been resolved.
  Check if there is any other stuck of the same symbol from another account
  If, so engage it to PM */
      
  if (verifyPositionInfo[account][symbolIndex].shares != 0)
  {
    return SUCCESS; //Stuck on this symbol of this account is still remaining
  }
  
  //switch to another account
  int anotherAccount;

  for (anotherAccount = FIRST_ACCOUNT; anotherAccount < MAX_ACCOUNT; anotherAccount++)
  {
    if (anotherAccount == account) continue;

    //Start checking another account, and double-check isTrading flag (at this time it might be changed)
    if ((verifyPositionInfo[anotherAccount][symbolIndex].shares > 0) && 
      (verifyPositionInfo[anotherAccount][symbolIndex].isTrading == TRADING_NONE) &&
      (verifyPositionInfo[account][symbolIndex].isTrading == TRADING_NONE))
    {
      verifyPositionInfo[anotherAccount][symbolIndex].isSleeping = NO;
      verifyPositionInfo[anotherAccount][symbolIndex].isTrading = TRADING_AUTOMATIC;
      verifyPositionInfo[anotherAccount][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
    
      // Build and send message to PM
      t_TSMessage pmMessageSend;
      BuildPMMessageForSendStuckInfo(&pmMessageSend, symbol, verifyPositionInfo[anotherAccount][symbolIndex].shares, 0.00, verifyPositionInfo[anotherAccount][symbolIndex].side, 0, anotherAccount);
      
      if (SendPMMessage(&pmMessageSend) == SUCCESS)
      {
        //Send a message to al TTs notify that stuck is sent to PM
        int ttIndex;
        for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
        {
          if (traderToolsInfo.connection[ttIndex].clientType == CLIENT_TOOL_DAEDALUS)
          {
            char notifMsg[1024] = "\0";
            sprintf(notifMsg, "Begin auto resume handling stuck, symbol=%s, shares=%d, side=%s",
                symbol, verifyPositionInfo[anotherAccount][symbolIndex].shares, verifyPositionInfo[anotherAccount][symbolIndex].side == ASK_SIDE ? "SHORT" : "LONG");
            BuildAndSendAlertToDaedalus(notifMsg, ttIndex, COM__RGM__DAEDALUS__PROTO__NOTIFICATION_LEVEL_TYPE__ShowLog);
          }
          else if (traderToolsInfo.connection[ttIndex].clientType == CLIENT_TOOL_TRADER_TOOL) 
          {
            SendMessageForNotifyNoMatchingPositionToTT(ttIndex, symbol, verifyPositionInfo[anotherAccount][symbolIndex].shares, verifyPositionInfo[anotherAccount][symbolIndex].side, BEGIN_RESUME_HANDLING_STUCK);
          }
        }

        TraceLog(DEBUG_LEVEL, "Begin auto resume handling stuck, (%.8s, %d, %d)\n", symbol, verifyPositionInfo[anotherAccount][symbolIndex].shares, verifyPositionInfo[anotherAccount][symbolIndex].side);
      }
      else
      {
        verifyPositionInfo[anotherAccount][symbolIndex].isTrading = TRADING_NONE;
        verifyPositionInfo[anotherAccount][symbolIndex].isPMOwner = NO;
        verifyPositionInfo[anotherAccount][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();

        TraceLog(ERROR_LEVEL, "Could not send stuck to PM (%.8s, %d, %d)\n", symbol, verifyPositionInfo[anotherAccount][symbolIndex].shares, verifyPositionInfo[anotherAccount][symbolIndex].side);
      }
      
      return SUCCESS;
    }
  }
  
  return SUCCESS;
  
}

/****************************************************************************
- Function name:  ProcessTSMessageForDisconnectAlert
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessPMMessageForDisconnectAlert(void *message)
{
  t_TSMessage *tsMessage = (t_TSMessage *)message;

  short serviceID;

  memcpy(&serviceID, &tsMessage->msgContent[10], 2);

  char service[128] = "\0";

  switch(serviceID)
  {
  // Books
  case PM_SERVICE_ARCA_BOOK:
    pmInfo.currentStatus.pmMdcStatus[PM_ARCA_BOOK_INDEX] = DISCONNECT;
    strcat(service, "PM ARCA Book");
    break;
  case PM_SERVICE_NASDAQ_BOOK:
    pmInfo.currentStatus.pmMdcStatus[PM_NASDAQ_BOOK_INDEX] = DISCONNECT;
    strcat(service, "PM NASDAQ Book");
    break;
  
  // Feeds
  case PM_SERVICE_CQS_FEED:
    pmInfo.currentStatus.statusCQS_Feed = DISCONNECT;
    strcat(service, "PM CQS Feed");
    break;
  case PM_SERVICE_UQDF_FEED:
    pmInfo.currentStatus.statusUQDF_Feed = DISCONNECT;
    strcat(service, "PM UQDF Feed");
    break;
  case PM_SERVICE_CTS_FEED:
    pmInfo.currentStatus.statusCTS_Feed = DISCONNECT;
    strcat(service, "PM CTS Feed");
    break;
  case PM_SERVICE_UTDF_FEED:
    pmInfo.currentStatus.statusUTDF = DISCONNECT;
    strcat(service, "PM UTDF Feed");
    break;

  // Orders
  case PM_SERVICE_ARCA_DIRECT:
    pmInfo.currentStatus.pmOecStatus[PM_ARCA_DIRECT_INDEX] = DISCONNECT;
    strcat(service, "PM ARCA DIRECT");
    break;
  case PM_SERVICE_NASDAQ_RASH:
    pmInfo.currentStatus.pmOecStatus[PM_NASDAQ_RASH_INDEX] = DISCONNECT;
    strcat(service, "PM NASDAQ RASH");
    break;
  default:
    TraceLog(ERROR_LEVEL, "(PMDisconnectAlert) Invalid Service ID = %d\n", serviceID);
    return ERROR;
  }

  BuildNSendDisconnectAlertToAllDaedalus(service);
  BuildNSendDisconnectAlertToAllTT(service);
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessPMMessageForDisabledTradingAlert
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessPMMessageForDisabledTradingAlert(void *message)
{
  t_TSMessage *tsMessage = (t_TSMessage *)message;

  short reasonID;

  memcpy(&reasonID, &tsMessage->msgContent[10], 2);

  char reason[128];

  reason[0] = 0;

  if (reasonID >= 0)
  {
    char name[80];
    GetTraderName(reasonID, name);
    
    sprintf(reason, "%s PM", name);
    pmInfo.currentStatus.tradingStatus = DISABLE;
  }
  else
  {
    switch(reasonID)
    {
    // no TT, no need to send
    case PM_TRADING_DISABLED_BY_NO_TT_CONNECTION:
      pmInfo.currentStatus.tradingStatus = DISABLE;
      return SUCCESS;  // don't process this case

    case PM_TRADING_DISABLED_BY_BOOK_DISCONNECT:
      pmInfo.currentStatus.tradingStatus = DISABLE;
      strcpy(reason, "All Books disconnected from PM");
      break;

    case PM_TRADING_DISABLED_BY_ORDER_DISCONNECT:
      pmInfo.currentStatus.tradingStatus = DISABLE;
      strcpy(reason, "All Orders disconnected from PM");
      break;
      
    case PM_TRADING_DISABLED_BY_WEDBUSH_REQUEST:
      pmInfo.currentStatus.tradingStatus = DISABLE;
      strcpy(reason, "WedBush Risk Manager disables trading on PM\n");
      break;
    
    case PM_TRADING_DISABLED_BY_NO_ACTIVE_OR_ACTIVE_ASSIST_TRADER:
      pmInfo.currentStatus.tradingStatus = DISABLE;
      strcpy(reason, "No Active and Active Assist, disable trading on PM\n");
      break;
	  
	case PM_TRADING_DISABLED_BY_EMERGENCY_STOP:
      pmInfo.currentStatus.tradingStatus = DISABLE;
      strcpy(reason, "Emergency Stop, disable trading on PM\n");
      break;

    default:
      TraceLog(ERROR_LEVEL, "Invalid Reason ID = %d\n", reasonID);
      return ERROR;
    } 
  }
  
  BuildNSendTradingDisabledAlertToAllDaedalus(reason);
  BuildNSendTradingDisabledAlertToAllTT(reason);
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessPMMessageForGetStuckInfoBeforeResumeHandlingStuck
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessPMMessageForGetStuckInfoBeforeResumeHandlingStuck(void *message)
{
  t_TSMessage *pmMessage = (t_TSMessage *)message;  
  int ttIndex;
  
  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;
  
  // symbol(8 bytes)
  char symbol[SYMBOL_LEN];
  strncpy(symbol, (char*) &pmMessage->msgContent[currentIndex], SYMBOL_LEN);
  currentIndex += SYMBOL_LEN;
  
  //shares (4 bytes)
  int shares = GetIntNumber(&pmMessage->msgContent[currentIndex]);
  currentIndex += 4;
  
  // side (1 byte)
  char side = pmMessage->msgContent[currentIndex];
  currentIndex++;
  
  // account
  int account = pmMessage->msgContent[currentIndex++];
  
  //Get stock symbol index
  int symbolIndex = GetStockSymbolIndex(symbol);
  if (symbolIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "Invalid symbol (%.8s, -1)\n", symbol);
    return SUCCESS;
  }
  
  if (verifyPositionInfo[account][symbolIndex].isTrading != TRADING_NONE)
  {
    //Send message no matching position to all TTs
    for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
    {
      if (traderToolsInfo.connection[ttIndex].clientType == CLIENT_TOOL_DAEDALUS)
      {
        char notifMsg[1024] = "\0";
        sprintf(notifMsg, "Cannot resume handling stuck, symbol=%s, shares=%d, side=%s",
            symbol, shares, side == ASK_SIDE ? "SHORT" : "LONG");
        BuildAndSendAlertToDaedalus(notifMsg, ttIndex, COM__RGM__DAEDALUS__PROTO__NOTIFICATION_LEVEL_TYPE__ShowLog);
      }
      else if (traderToolsInfo.connection[ttIndex].clientType == CLIENT_TOOL_TRADER_TOOL) 
      {
        SendMessageForNotifyNoMatchingPositionToTT(ttIndex, symbol, shares, side, CAN_NOT_RESUME_HANDLING_STUCK);
      }
    }

    TraceLog(ERROR_LEVEL, "Can not resume handling this stuck when stuck is trading (%.8s, %d, %d)\n", symbol, shares, side);

    return SUCCESS;
  }
  else
  {
    if (shares > 0)
    {
      if ((side == verifyPositionInfo[account][symbolIndex].side) && (shares == verifyPositionInfo[account][symbolIndex].shares))
      {
        verifyPositionInfo[account][symbolIndex].isSleeping = NO;
        verifyPositionInfo[account][symbolIndex].isTrading = TRADING_AUTOMATIC;
        verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
        
        // Build and send message to PM
        t_TSMessage pmMessageSend;
        BuildPMMessageForSendStuckInfo(&pmMessageSend, symbol, shares, 0.00, side, 0, account);
          
        if (SendPMMessage(&pmMessageSend) == SUCCESS)
        {
          //Send a message to al TTs notify that stuck is sent to PM
          for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
          {
            if (traderToolsInfo.connection[ttIndex].clientType == CLIENT_TOOL_DAEDALUS)
            {
              char notifMsg[1024] = "\0";
              sprintf(notifMsg, "Begin auto resume handling stuck, symbol=%s, shares=%d, side=%s",
                  symbol, shares, side == ASK_SIDE ? "SHORT" : "LONG");
              BuildAndSendAlertToDaedalus(notifMsg, ttIndex, COM__RGM__DAEDALUS__PROTO__NOTIFICATION_LEVEL_TYPE__ShowLog);
            }
            else if (traderToolsInfo.connection[ttIndex].clientType == CLIENT_TOOL_TRADER_TOOL)
            {
              SendMessageForNotifyNoMatchingPositionToTT(ttIndex, symbol, shares, side, BEGIN_RESUME_HANDLING_STUCK);
            }
          }

          TraceLog(DEBUG_LEVEL, "Begin auto resume handling stuck (%.8s, %d, %d)\n", symbol, shares, side);
        }
        else
        {
          verifyPositionInfo[account][symbolIndex].isTrading = TRADING_NONE;
          verifyPositionInfo[account][symbolIndex].isPMOwner = NO;
          verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();

          TraceLog(ERROR_LEVEL, "Could not send stuck info to PM (%.8s, %d, %d)\n", symbol, shares, side);
        }
      }
      else 
      {
        //Send message no matching position to all TTs
        for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
        {
          if (traderToolsInfo.connection[ttIndex].clientType == CLIENT_TOOL_DAEDALUS)
          {
            char notifMsg[1024] = "\0";
            sprintf(notifMsg, "Cannot resume handling stuck, symbol=%s, shares=%d, side=%s",
                symbol, shares, side == ASK_SIDE ? "SHORT" : "LONG");
            BuildAndSendAlertToDaedalus(notifMsg, ttIndex, COM__RGM__DAEDALUS__PROTO__NOTIFICATION_LEVEL_TYPE__ShowLog);
          }
          else if (traderToolsInfo.connection[ttIndex].clientType == CLIENT_TOOL_TRADER_TOOL)
          {
            SendMessageForNotifyNoMatchingPositionToTT(ttIndex, symbol, shares, side, CAN_NOT_RESUME_HANDLING_STUCK);
          }
        }

        TraceLog(ERROR_LEVEL, "Can not resume handling this stuck (%.8s, %d, %d). Stuck information mismatch between AS and PM.\n", symbol, shares, side);

        return SUCCESS;
      }
    }
    else
    {
      if (verifyPositionInfo[account][symbolIndex].shares > 0)
      {
        verifyPositionInfo[account][symbolIndex].isSleeping = NO;
        verifyPositionInfo[account][symbolIndex].isTrading = TRADING_AUTOMATIC;
        verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
        
        // Build and send message to PM
        t_TSMessage pmMessageSend;
        BuildPMMessageForSendStuckInfo(&pmMessageSend, symbol, verifyPositionInfo[account][symbolIndex].shares, 0.00, verifyPositionInfo[account][symbolIndex].side, 0, account);
          
        if (SendPMMessage(&pmMessageSend) == SUCCESS)
        {
          //Send a message to al TTs notify that stuck is sent to PM
          for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
          {
            if (traderToolsInfo.connection[ttIndex].clientType == CLIENT_TOOL_DAEDALUS)
            {
              char notifMsg[1024] = "\0";
              sprintf(notifMsg, "Begin auto resume handling stuck, symbol=%s, shares=%d, side=%s",
                  symbol, verifyPositionInfo[account][symbolIndex].shares, verifyPositionInfo[account][symbolIndex].side == ASK_SIDE ? "SHORT" : "LONG");
              BuildAndSendAlertToDaedalus(notifMsg, ttIndex, COM__RGM__DAEDALUS__PROTO__NOTIFICATION_LEVEL_TYPE__ShowLog);
            }
            else if (traderToolsInfo.connection[ttIndex].clientType == CLIENT_TOOL_TRADER_TOOL)
            {
              SendMessageForNotifyNoMatchingPositionToTT(ttIndex, symbol, verifyPositionInfo[account][symbolIndex].shares, verifyPositionInfo[account][symbolIndex].side, BEGIN_RESUME_HANDLING_STUCK);
            }
          }

          TraceLog(DEBUG_LEVEL, "Begin auto resume handling stuck, (%.8s, %d, %d)\n", symbol, verifyPositionInfo[account][symbolIndex].shares, verifyPositionInfo[account][symbolIndex].side);
        }
        else
        {
          verifyPositionInfo[account][symbolIndex].isTrading = TRADING_NONE;
          verifyPositionInfo[account][symbolIndex].isPMOwner = NO;
          verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();

          TraceLog(ERROR_LEVEL, "Could not send stuck to PM (%.8s, %d, %d)\n", symbol, verifyPositionInfo[account][symbolIndex].shares, verifyPositionInfo[account][symbolIndex].side);
        }
      }
      else
      {
        return SUCCESS;
      }
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessPMMessageForGroupOfBookDataUnhandledAlert
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessPMMessageForGroupOfBookDataUnhandledAlert(void *message)
{
  t_TSMessage *tsMessage = (t_TSMessage *)message;

  int groupID, serviceID;

  memcpy(&serviceID, &tsMessage->msgContent[10], 4);
  memcpy(&groupID, &tsMessage->msgContent[14], 4);

  char reason[128];

  switch(serviceID)
  {
  case PM_SERVICE_ARCA_BOOK:
    if((groupID < 0) || (groupID >= MAX_ARCA_MULTICAST_GROUP))
    {
      TraceLog(ERROR_LEVEL, "(ProcessPMMessageForGroupOfBookDataUnhandledAlert) Invalid unit ID = %d serviceID = %d\n", groupID, serviceID);
      return ERROR;
    }

    sprintf(reason, "PM ARCA MDC group %d", groupID);
    break;

  case PM_SERVICE_NASDAQ_BOOK:
    if((groupID < 0) || (groupID >= MAX_NASDAQ_MULTICAST_GROUP))
    {
      TraceLog(ERROR_LEVEL, "(ProcessPMMessageForGroupOfBookDataUnhandledAlert) Invalid unit ID = %d serviceID = %d\n", groupID, serviceID);
      return ERROR;
    }

    sprintf(reason, "PM NASDAQ MDC");
    break;

  case PM_SERVICE_CQS_FEED:
    if((groupID < 0) || (groupID >= MAX_CQS_MULTICAST_GROUP))
    {
      TraceLog(ERROR_LEVEL, "(ProcessPMMessageForGroupOfBookDataUnhandledAlert) Invalid unit ID = %d serviceID = %d\n", groupID, serviceID);
      return ERROR;
    }

    sprintf(reason, "PM CQS FEED group %d", groupID);
    break;

  case PM_SERVICE_UQDF_FEED:
    if((groupID < 0) || (groupID >= MAX_UQDF_MULTICAST_GROUP))
    {
      TraceLog(ERROR_LEVEL, "(ProcessPMMessageForGroupOfBookDataUnhandledAlert) Invalid unit ID = %d serviceID = %d\n", groupID, serviceID);
      return ERROR;
    }

    sprintf(reason, "PM UQDF FEED group %d", groupID);
    break;

  case PM_SERVICE_CTS_FEED:
    if((groupID < 0) || (groupID >= MAX_CTS_MULTICAST_GROUP))
    {
      TraceLog(ERROR_LEVEL, "(ProcessPMMessageForGroupOfBookDataUnhandledAlert) Invalid unit ID = %d serviceID = %d\n", groupID, serviceID);
      return ERROR;
    }

    sprintf(reason, "PM CTS FEED group %d", groupID);
    break;

  case PM_SERVICE_UTDF_FEED:
    if((groupID < 0) || (groupID >= MAX_UTDF_MULTICAST_GROUP))
    {
      TraceLog(ERROR_LEVEL, "(ProcessPMMessageForGroupOfBookDataUnhandledAlert) Invalid unit ID = %d serviceID = %d\n", groupID, serviceID);
      return ERROR;
    }

    sprintf(reason, "PM UTDF FEED group %d", groupID);
    break;


  default:
    TraceLog(ERROR_LEVEL, "(ProcessPMMessageForGroupOfBookDataUnhandledAlert) Invalid Service ID = %d\n", serviceID);
    return ERROR;
  }

  BuildNSendGroupfOfBookDataUnhandledAlertToAllDaedalus(reason);
  BuildNSendGroupfOfBookDataUnhandledAlertToAllTT(reason);
  return SUCCESS;
}

int SendRequestPriceUpdateToPM(const int totalSymbol, char symbolList[][SYMBOL_LEN])
{
  if ((pmInfo.connection.status == DISCONNECT) || (pmInfo.connection.socket == -1))
  {
    return SUCCESS;
  }

  t_TSMessage pmMessage;

  if (BuildPMMessageForRequestPriceUpdate(&pmMessage, totalSymbol, symbolList) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Could not Price Update Request message to PM\n");
    return ERROR;
  }

  // Send message to PM
  if (SendPMMessage(&pmMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot send Price Update Request\n");
    CloseConnection(&pmInfo.connection);
    return ERROR;
  }

  return SUCCESS;
}

int BuildPMMessageForRequestPriceUpdate(void *message, const int totalSymbol, char symbolList[][SYMBOL_LEN])
{
  t_ShortConverter shortConvert;
  t_IntConverter intConvert;

  t_TSMessage *pmMessage = (t_TSMessage *)message;
  
  // Initialize message
  pmMessage->msgLen = 0;
  
  // Message type
  shortConvert.value = MSG_TYPE_FOR_SEND_PRICE_UDPATE_REQUEST_TO_PM;
  pmMessage->msgContent[0] = shortConvert.c[0];
  pmMessage->msgContent[1] = shortConvert.c[1];
  pmMessage->msgLen += 2;

  // Update index of body length
  pmMessage->msgLen += 4;

  // Message body = (blockLength (4 bytes) + blockContent (blockLength bytes))
  
  // Update index of block length
  pmMessage->msgLen += 4;

  // Block content = totalSymbol (4 bytes) + Symbol List (8 * totalSymbol)
  
  // Total Symbol
  memcpy(&pmMessage->msgContent[pmMessage->msgLen], &totalSymbol, 4);
  pmMessage->msgLen += 4;
  
  // Symbol List
  memcpy(&pmMessage->msgContent[pmMessage->msgLen], symbolList, totalSymbol * SYMBOL_LEN);
  pmMessage->msgLen += totalSymbol * SYMBOL_LEN;

  // Block length field
  intConvert.value = 5;
  memcpy(&pmMessage->msgContent[MSG_HEADER_LEN], intConvert.c, 4);
  
  // Body length field
  intConvert.value = pmMessage->msgLen - MSG_HEADER_LEN;
  memcpy(&pmMessage->msgContent[2], intConvert.c, 4);

  return pmMessage->msgLen;
}

/***************************************************************************/
