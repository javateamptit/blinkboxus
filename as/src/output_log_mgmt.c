/*****************************************************************************
**  Project:    Equity Arbitrage Application
**  Filename:   output_log_mgmt.c
**  Description:  This file contains function definitions that were declared
          in output_log_mgmt.h  
**  Author:     Luan Vo-Kinh
**  First created:  13-Sep-2007
**  Last updated: ----------- 20090826: 
*****************************************************************************/

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include "configuration.h"
#include "trade_servers_mgmt_proc.h"
#include "order_mgmt_proc.h"
#include "arca_direct_data.h"
#include "nasdaq_ouch_data.h"
#include "nasdaq_rash_data.h"
#include "bats_boe_data.h"
#include "edge_order_data.h"
#include "database_util.h"
#include "output_log_mgmt.h"
#include "position_manager_proc.h"
#include "query_data_mgmt.h"
#include "daedalus_proc.h"
#include "internal_proc.h"
#include "trader_tools_proc.h"
#include "logging.h"
/****************************************************************************
** Global variables definition
****************************************************************************/
t_OutputLogMgmt tradeOutputLogMgmt[MAX_TRADE_SERVER_CONNECTIONS];

t_OutputLogProcessedMgmt positionOutputLogProcessed;

t_TradeOutputPointMgmt tradeOutputPoints;

t_CrossSummaryCollection asCrossCollection;

t_TSCrossCollection tsCrossCollection[MAX_TRADE_SERVER_CONNECTIONS];

t_TradeCollection tradeCollection;

t_SymbolStuckMgmt symbolStuckMgmt[MAX_STOCK_SYMBOL];

t_SymbolDetectCross symbolDetectCrossList;

int tradeOutputIndex[MAX_CROSS_ID][MAX_TRADE_SERVER_CONNECTIONS][3];

int currentCrossId = 0;
int currentTradeId = 0;
FILE *fileCrossId = NULL;
FILE *fileBBOProcessedIndex = NULL;
pthread_mutex_t updateOutputLogMutex;

pthread_mutex_t calculationRealValueMutex;

t_BBOInfoMgmt BBOInfoMgmt[MAX_TRADE_SERVER_CONNECTIONS];

t_OutputLogProcessingStatus OutputLogProcessingStatus[MAX_TRADE_SERVER_CONNECTIONS];
int NumberOutputLogToStop = 5000;
int NumberOutputLogToResume = 1000;

int countTradePerEachProcess = 0;
/****************************************************************************
** Function declarations
****************************************************************************/

/****************************************************************************
- Function name:  InitializeOutputLogManegementStructures
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int InitializeBBOManagementStructure(void)
{
  char fullPath[MAX_PATH_LEN] = "\0";
  char fileName[MAX_LINE_LEN];
  
  int tsIndex;
  for (tsIndex = 0; tsIndex < MAX_TRADE_SERVER_CONNECTIONS; tsIndex++)
  {
    BBOInfoMgmt[tsIndex].fileDesc = NULL;
    BBOInfoMgmt[tsIndex].tsIndex = tsIndex;
    BBOInfoMgmt[tsIndex].processedIndex = 0;
    BBOInfoMgmt[tsIndex].receivedIndex = 0;
    memset(&BBOInfoMgmt[tsIndex].BBOInfoCollection, 0, sizeof(BBOInfoMgmt[tsIndex].BBOInfoCollection));
    
    //Open file to write bbo info
    sprintf(fileName, "bbo_info_ts%d.txt", tsIndex + 1);
    
    if (GetFullRawDataPath(fileName, fullPath) == NULL_POINTER)
    {
      TraceLog(ERROR_LEVEL, "Have some problem in get full bbo log path operation (%s)\n", fileName);

      return ERROR;
    }
    
    if (IsFileTimestampToday(fullPath) == 0)
    {
      TraceLog(ERROR_LEVEL, "%s is old, please check the correctness of data files\n", fullPath);
      exit(0);
    }
    
    BBOInfoMgmt[tsIndex].fileDesc = fopen(fullPath, "a+");
    if (BBOInfoMgmt[tsIndex].fileDesc == NULL)
    {
      TraceLog(ERROR_LEVEL, "Have some problem in openning file: %s\n", fullPath);
      return ERROR;
    }
    
    //Go to the end of file
    fseek(BBOInfoMgmt[tsIndex].fileDesc, 0, SEEK_END);
    
    //Update received index
    BBOInfoMgmt[tsIndex].receivedIndex = (int) (ftell(BBOInfoMgmt[tsIndex].fileDesc) / sizeof(t_BBOQuoteEntry));
  }
  
  return SUCCESS;
  
}

/****************************************************************************
- Function name:  InitializeOutputLogManegementStructures
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int InitializeOutputLogManegementStructures(void)
{
  int tsIndex;

  pthread_mutex_init(&updateOutputLogMutex, NULL);

  pthread_mutex_init(&calculationRealValueMutex, NULL);

  // Output log 
  for (tsIndex = 0; tsIndex < MAX_TRADE_SERVER_CONNECTIONS; tsIndex++)
  {
    tradeOutputLogMgmt[tsIndex].fileDesc = NULL;
    tradeOutputLogMgmt[tsIndex].tsIndex = tsIndex;
    tradeOutputLogMgmt[tsIndex].lastUpdatedIndex = -1;
    tradeOutputLogMgmt[tsIndex].countOutputLog = 0;
    tradeOutputLogMgmt[tsIndex].isProcessed = 0;
    sprintf(tradeOutputLogMgmt[tsIndex].fileName, "trade_output_log_ts%d.txt", tsIndex + 1);
    
    pthread_mutex_init(&tradeOutputLogMgmt[tsIndex].updateMutex, NULL);

    memset(&tradeOutputLogMgmt[tsIndex].tradeLogCollection, 0, MAX_OUTPUT_LOG_ENTRIES * MAX_OUTPUT_LOG_TEXT_LEN);
  }
  
  //Initialize Output Log Processing Status
  memset(OutputLogProcessingStatus, 0, sizeof(OutputLogProcessingStatus));
  
  //TraceLog(DEBUG_LEVEL, "sizeof(tradeOutputLogMgmt) = %d\n", sizeof(tradeOutputLogMgmt));

  // Initialze trade output log structures
  InitialzeTradeOutputLogStructures();

  // Initialize TS cross index
  InitializeTSCrossIndex();

  // Initialize position output log processed structure
  InitializePositionOutputLogProcessedStructure();

  // Initialize symbol stuck structure
  InitializeSymbolStuckStructure();

  // Initialize symbol detect cross list
  InitializeSymbolDetectCrossList();
  
  return 0;
}

/****************************************************************************
- Function name:  InitialzeTradeOutputLogStructures
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int InitialzeTradeOutputLogStructures(void)
{
  int i, j, k;

  /*
  char isUpdate[MAX_TRADER_TOOL_CONNECTIONS];
  char symbol[SYMBOL_LEN];
  int symbolIndex;
  int crossId;
  int timestamp;
  int ecnLaunch;
  int askBidLaunch;
  int ecnExist;
  int tradeId;
  int tsIndex;
  int indexCrossList[MAX_TRADE_SERVER_CONNECTIONS]; 
  */
  asCrossCollection.countCross = 0;
  for (i = 0; i < MAX_TOTAL_CROSS_ENTRIES; i ++)
  {
    memset(asCrossCollection.crossList[i].isUpdate, NOT_YET, MAX_TRADER_TOOL_CONNECTIONS);
    memset(asCrossCollection.crossList[i].countSent, 0, MAX_TRADER_TOOL_CONNECTIONS);
    memset(asCrossCollection.crossList[i].symbol, 0, SYMBOL_LEN);
    asCrossCollection.crossList[i].symbolIndex = -1;
    asCrossCollection.crossList[i].crossId = -1;
    asCrossCollection.crossList[i].timestamp = -1;
    asCrossCollection.crossList[i].ecnLaunch = -1;
    asCrossCollection.crossList[i].askBidLaunch = -1;
    asCrossCollection.crossList[i].ecnExist = -1;
    asCrossCollection.crossList[i].tradeIndex = -1;
    asCrossCollection.crossList[i].tsIndex = -1;

    for (j = 0; j < MAX_TRADE_SERVER_CONNECTIONS; j ++)
    {
      asCrossCollection.crossList[i].indexCrossList[j] = -1;
    }
  }

  //TraceLog(DEBUG_LEVEL, "sizeof(asCrossCollection) = %d\n", sizeof(asCrossCollection));

  for (i = 0; i < MAX_TRADE_SERVER_CONNECTIONS; i ++)
  {   
    tsCrossCollection[i].countCross = 0;
    tsCrossCollection[i].prevCountCross = 0;

    for (j = 0; j < MAX_CROSS_ENTRIES; j ++)
    {
      tsCrossCollection[i].crossList[j].summary.crossId = -1;
      tsCrossCollection[i].crossList[j].summary.tsIndex = -1;
      tsCrossCollection[i].crossList[j].summary.asCrossIndex = -1;
      tsCrossCollection[i].crossList[j].summary.tradeIndex = -1;
      tsCrossCollection[i].crossList[j].summary.timestamp = -1;
      tsCrossCollection[i].crossList[j].summary.isMultiLevel = SINGLE_LEVEL_VALUE;
      tsCrossCollection[i].crossList[j].summary.symbolIndex = -1;
      tsCrossCollection[i].crossList[j].summary.serverRole = -1;
      tsCrossCollection[i].crossList[j].summary.ecnLaunch = -1;
      tsCrossCollection[i].crossList[j].summary.askBidLaunch = -1;
      tsCrossCollection[i].crossList[j].summary.ecnExist = -1;
      tsCrossCollection[i].crossList[j].summary.askShares = 0;
      tsCrossCollection[i].crossList[j].summary.bidShares = 0;
      tsCrossCollection[i].crossList[j].summary.worstAskPrice = 0.00;
      tsCrossCollection[i].crossList[j].summary.worstBidPrice = 0.00;
      memset(tsCrossCollection[i].crossList[j].summary.askBidList, 0, sizeof(tsCrossCollection[i].crossList[j].summary.askBidList));

      tsCrossCollection[i].crossList[j].countLogMsg = 0;
      for (k = 0; k < MAX_DETAIL_PER_CROSS; k++)
      {
        tsCrossCollection[i].crossList[j].indexLogMsgList[k] = -1;
      }     
    }
  }

  //TraceLog(DEBUG_LEVEL, "sizeof(tsCrossCollection) = %d\n", sizeof(tsCrossCollection));

  tradeCollection.countTrade = 0;
  for (i = 0; i < MAX_TOTAL_TRADE_ENTRIES; i ++)
  {
    tradeCollection.tradeList[i].summary.tradeId = -1;    
    tradeCollection.tradeList[i].summary.tsIndex = -1;
    tradeCollection.tradeList[i].summary.crossId = -1;
    tradeCollection.tradeList[i].summary.asCrossId = -1;
    
    tradeCollection.tradeList[i].summary.boughtShares = -1;
    tradeCollection.tradeList[i].summary.soldShares = -1;
    tradeCollection.tradeList[i].summary.leftSharesStuck = 0;
    tradeCollection.tradeList[i].summary.totalBought = 0.00;
    tradeCollection.tradeList[i].summary.totalSold = 0.00;
    tradeCollection.tradeList[i].summary.totalStuck = 0.00;

    tradeCollection.tradeList[i].countLogMsg = 0;
    for (j = 0; j < MAX_DETAIL_PER_TRADE; j ++)
    {
      tradeCollection.tradeList[i].indexLogMsgList[j] = -1;
    }
  }

  //TraceLog(DEBUG_LEVEL, "sizeof(tradeCollection) = %d\n", sizeof(tradeCollection));

  return 0;
}

/****************************************************************************
- Function name:  InitializeTSCrossIndex
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int InitializeTSCrossIndex(void)
{
  int i, tsIndex;
    
  for (i = 0; i < MAX_CROSS_ID; i++)
  {
    for (tsIndex = 0; tsIndex < MAX_TRADE_SERVER_CONNECTIONS; tsIndex++)
    {     
      tradeOutputIndex[i][tsIndex][CROSS_INDEX] = -1;
      tradeOutputIndex[i][tsIndex][TRADE_INDEX] = -1;
      tradeOutputIndex[i][tsIndex][SKIP_STATUS] = 0;
    }
  } 

  //TraceLog(DEBUG_LEVEL, "sizeof(tradeOutputIndex) = %d\n", sizeof(tradeOutputIndex));
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  InitializeSymbolStuckStructure
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int InitializeSymbolStuckStructure(void)
{
  int i, j, k;
    
  for (i = 0; i < MAX_STOCK_SYMBOL; i++)
  {
    pthread_mutex_init(&(symbolStuckMgmt[i].updateRealValueMutex), NULL);
    
    for (j = 0; j < MAX_SIDE; j++)
    {
      symbolStuckMgmt[i].stuckMgmt[j].countUpdated = 0;
      symbolStuckMgmt[i].stuckMgmt[j].countProcessed = 0;
      symbolStuckMgmt[i].stuckMgmt[j].isCompleted = YES;

      for (k = 0; k < MAX_STUCK_PER_SYMBOL; k++)
      {
        symbolStuckMgmt[i].stuckMgmt[j].stuckList[k].tsIndex = -1;
        symbolStuckMgmt[i].stuckMgmt[j].stuckList[k].crossId = -1;
        symbolStuckMgmt[i].stuckMgmt[j].stuckList[k].timestamp = -1;
        symbolStuckMgmt[i].stuckMgmt[j].stuckList[k].stuckShares = 0;
        symbolStuckMgmt[i].stuckMgmt[j].stuckList[k].stuckPrice = 0.00;
        symbolStuckMgmt[i].stuckMgmt[j].stuckList[k].realValue = 0.00;
      }

      symbolStuckMgmt[i].fillMgmt[j].shares = 0;
      symbolStuckMgmt[i].fillMgmt[j].price = 0.00;
    }
  }

  //TraceLog(DEBUG_LEVEL, "sizeof(symbolStuckMgmt) = %d\n", sizeof(symbolStuckMgmt));
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  InitializePositionOutputLogProcessedStructure
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int InitializePositionOutputLogProcessedStructure(void)
{
  positionOutputLogProcessed.fileDesc = NULL;
  memset(positionOutputLogProcessed.tsPosition, 0, MAX_TRADE_SERVER_CONNECTIONS * sizeof(int));

  return SUCCESS;
}

/****************************************************************************
- Function name:  InitializeTradeOutputCheckPoint
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int InitializeTradeOutputCheckPoint(void)
{
  int i, j;
  
  tradeOutputPoints.currentPoint = -1;
  
  for (i = 0; i < MAX_POINT; i++)
  {
    // Points: 8h30, 10h00, 11h30, 13h00, ...
    tradeOutputPoints.pointList[i].timestamp = (7 * 60 + STEP_POINT * (i + 1)) * 60;
    tradeOutputPoints.pointList[i].isUpdate = -1;
    
    for (j = 0; j < MAX_TRADE_OUTPUT_TABLE;j++)
    {
      tradeOutputPoints.pointList[i].idTables[j] = -1;
    }
  }
  
  sprintf(tradeOutputPoints.fileName, "checkpoint_tradeoutput.txt");
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  InitializeSymbolDetectCrossList
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int InitializeSymbolDetectCrossList(void)
{
  /*
  int countSymbol;
  char symbol[MAX_STOCK_SYMBOL][SYMBOL_LEN];
  */

  memset(&symbolDetectCrossList, 0, sizeof(symbolDetectCrossList));

  return SUCCESS;
}

/****************************************************************************
- Function name:  UpdateStockSymbolDetectCrossIndex
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int UpdateStockSymbolDetectCrossIndex(const char *stockSymbol)
{
  int a, b, c, d, e, f, g, h;

  //Character 1: in range A-Z
  //65 is the ASCII code of 'A'
  //90 is the ASCII code of 'Z'
  //36 is the ASCII code of '$'

  if ((stockSymbol[0] > 64) && (stockSymbol[0] < 91))
  {
    a = stockSymbol[0] - INDEX_OF_A_CHAR;//A-Z  
  }
  else
  {
    //Invalid stock symbol
    return -1;
  }
  
  //Character 2: 0, A-Z
  //65 is the ASCII code of 'A'
  //90 is the ASCII code of 'Z'

  //The order of 'if' expressions are important for faster speed
  //This is experience, right?

  if ((stockSymbol[1] > 64) && (stockSymbol[1] < 91))
  {
    b = stockSymbol[1] - INDEX_OF_A_CHAR;//A-Z  
  }
  //A dollar sign and a dot never appear in the second position!!!
  else if ((stockSymbol[1] == 0) || (stockSymbol[1] == 32))//0: NULL; 32:SPACE
  {
    //Stock symbol's length is 1, no need to do more
    //This also guarantee fast speed
    //We use the variable a in the array index instead of 
    //stockSymbol[0] - INDEX_OF_A_CHAR so as not to calculate anything 

    //Check new stock symbol
    if (symboIndexlList[a][0][0][0][0][SYMBOL_FOR_DETECT_CROSS] == -1)
    {
      memcpy(symbolDetectCrossList.symbolList[symbolDetectCrossList.countSymbol], stockSymbol, 1);
      symboIndexlList[a][0][0][0][0][SYMBOL_FOR_DETECT_CROSS] = symbolDetectCrossList.countSymbol++;
    }
    return symboIndexlList[a][0][0][0][0][SYMBOL_FOR_DETECT_CROSS]; 
  }
  else if (stockSymbol[1] == '-')
  {
    b = 3;
  }
  else if (stockSymbol[1] == '.')
  {
    b = 4;
  }
  else if (stockSymbol[1] == '$')
  {
    b = 1;
  }
  else if (stockSymbol[1] == '+')
  {
    b = 2;
  }
  else
  {
    return -1;
  }

  //Character 3: 0, ., A-Z
  //65 is the ASCII code of 'A'
  //90 is the ASCII code of 'Z'

  //The order of 'if' expressions are important for faster speed

  if ((stockSymbol[2] > 64) && (stockSymbol[2] < 91))
  {
    c = stockSymbol[2] - INDEX_OF_A_CHAR;//A-Z  
  }

  //Not in range A-Z. We have 244 zeros, and 119 dot
  //Maybe we need to count how many $ and dot at the third position
  //for even better 'if' order

  //244 zeros check first
  else if ((stockSymbol[2] == 0) || (stockSymbol[2] == 32))//0: NULL; 32: SPACE
  { 
    //Stock symbol's length is 2, no need to do more
    //This also guarantee fast speed
    //We use the variable a, b in the array index instead of 
    //stockSymbol[0] - INDEX_OF_A_CHAR, stockSymbol[1] - INDEX_OF_A_CHAR so as not to calculate anything 

    //Check new stock symbol
    if (symboIndexlList[a][b][0][0][0][SYMBOL_FOR_DETECT_CROSS] == -1)
    {
      memcpy(symbolDetectCrossList.symbolList[symbolDetectCrossList.countSymbol], stockSymbol, 2);
      symboIndexlList[a][b][0][0][0][SYMBOL_FOR_DETECT_CROSS] = symbolDetectCrossList.countSymbol++;
    }
    return symboIndexlList[a][b][0][0][0][SYMBOL_FOR_DETECT_CROSS];
  }
  else if (stockSymbol[2] == '-')
  {
    c = 3;
  }
  else if (stockSymbol[2] == '.')
  {
    c = 4;
  }
  else if (stockSymbol[2] == '$')
  {
    c = 1;
  }
  else if (stockSymbol[2] == '+')
  {
    c = 2;
  }
  else 
  {
    return -1;
  }

  //Character 4: 0, ., A-Z
  //65 is the ASCII code of 'A'
  //91 is the ASCII code of 'Z'

  if ((stockSymbol[3] > 64) && (stockSymbol[3] < 91))
  {
    d = stockSymbol[3] - INDEX_OF_A_CHAR;//A-Z  
  }

  //Not in range A-Z. We have 3802 zeros, and 119 dot
  //Maybe we need to count how many $ and dot at the fourth position
  //for even better 'if' order

  //Zero is checked first
  else if ((stockSymbol[3] == 0) || (stockSymbol[3] == 32))//0: NULL; 32:SPACE
  {
    //Check new stock symbol
    if (symboIndexlList[a][b][c][0][0][SYMBOL_FOR_DETECT_CROSS] == -1)
    {
      memcpy(symbolDetectCrossList.symbolList[symbolDetectCrossList.countSymbol], stockSymbol, 3);
      symboIndexlList[a][b][c][0][0][SYMBOL_FOR_DETECT_CROSS] = symbolDetectCrossList.countSymbol++;
    }
    return symboIndexlList[a][b][c][0][0][SYMBOL_FOR_DETECT_CROSS];
  }
  else if (stockSymbol[3] == '-')
  {
    d = 3;
  }
  else if (stockSymbol[3] == '.')
  {
    d = 4;
  }
  else if (stockSymbol[3] == '$')
  {
    d = 1;
  }
  else if (stockSymbol[3] == '+')
  {
    d = 2;
  }
  else
  {
    return -1;
  }

  //Character 5: 0, ., A-Z
  if ((stockSymbol[4] > 64) && (stockSymbol[4] < 91))
  {
    e = stockSymbol[4] - INDEX_OF_A_CHAR;//A-Z  
  }
  //Not in range A-Z. We have 3248 zeros, 1 dot (for the stock symbol ZZZZ.T only)
  //Zero is checked first

  else if ((stockSymbol[4] == 0) || (stockSymbol[4] == 32))//NULL Because: 4: 3248, 32: SPACE
  {
    //Check new stock symbol
    if (symboIndexlList[a][b][c][d][0][SYMBOL_FOR_DETECT_CROSS] == -1)
    {
      memcpy(symbolDetectCrossList.symbolList[symbolDetectCrossList.countSymbol], stockSymbol, 4);
      symboIndexlList[a][b][c][d][0][SYMBOL_FOR_DETECT_CROSS] = symbolDetectCrossList.countSymbol++;
    }
    return symboIndexlList[a][b][c][d][0][SYMBOL_FOR_DETECT_CROSS];
  }
  else if (stockSymbol[4] == '-')
  {
    e = 3;
  }
  else if (stockSymbol[4] == '.')
  {
    e = 4;
  }
  else if (stockSymbol[4] == '$')
  {
    e = 1;
  }
  else if (stockSymbol[4] == '+')
  {
    e = 2;
  }
  else
  {
    return -1;
  }
  
  //Character 6: 0, ., A-Z
  if ((stockSymbol[5] > 64) && (stockSymbol[5] < 91))
  {
    f = stockSymbol[5] - INDEX_OF_A_CHAR;//A-Z  
  }
  //Not in range A-Z. We have 3248 zeros, 1 dot (for the stock symbol ZZZZ.T only)
  //Zero is checked first

  else if ((stockSymbol[5] == 0) || (stockSymbol[5] == 32))//NULL Because: 4: 3248, 32: SPACE
  {
    //Check new stock symbol
    if (symboIndexlList[a][b][c][d][e][SYMBOL_FOR_DETECT_CROSS] == -1)
    {
      memcpy(symbolDetectCrossList.symbolList[symbolDetectCrossList.countSymbol], stockSymbol, 5);
      symboIndexlList[a][b][c][d][e][SYMBOL_FOR_DETECT_CROSS] = symbolDetectCrossList.countSymbol++;
    }
    return symboIndexlList[a][b][c][d][e][SYMBOL_FOR_DETECT_CROSS];
  }
  else if (stockSymbol[5] == '-')
  {
    f = 3;
  }
  else if (stockSymbol[5] == '.')
  {
    f = 4;
  }
  else if (stockSymbol[5] == '$')
  {
    f = 1;
  }
  else if (stockSymbol[5] == '+')
  {
    f = 2;
  }
  else
  {
    return -1;
  }
  
  //Character 7: 0, ., A-Z
  if ((stockSymbol[6] > 64) && (stockSymbol[6] < 91))
  {
    g = stockSymbol[6] - INDEX_OF_A_CHAR;//A-Z  
  }
  else if ((stockSymbol[6] == 0) || (stockSymbol[6] == 32))//NULL Because: 4: 3248, 32: SPACE
  {
    //This symbol has 6 characters
    //Now check for the first 5 chars
    int symbolIndex;
    if (symboIndexlList[a][b][c][d][e][SYMBOL_FOR_DETECT_CROSS] == -1)
    {
      memcpy(symbolDetectCrossList.symbolList[symbolDetectCrossList.countSymbol], stockSymbol, 5);
      symboIndexlList[a][b][c][d][e][SYMBOL_FOR_DETECT_CROSS] = symbolDetectCrossList.countSymbol++;
      
      // Get real index
      symbolIndex = symboIndexlList[a][b][c][d][e][SYMBOL_FOR_DETECT_CROSS];
      memcpy(symbolDetectCrossList.symbolList[symbolDetectCrossList.countSymbol], stockSymbol, 6);
      longSymbolIndexArray[symbolIndex][f][0][SYMBOL_FOR_DETECT_CROSS] = symbolDetectCrossList.countSymbol++;
      return longSymbolIndexArray[symbolIndex][f][0][SYMBOL_FOR_DETECT_CROSS];
    }
    else
    {
      symbolIndex = symboIndexlList[a][b][c][d][e][SYMBOL_FOR_DETECT_CROSS];
      
      if (longSymbolIndexArray[symbolIndex][f][0][SYMBOL_FOR_DETECT_CROSS] == -1)
      {
        memcpy(symbolDetectCrossList.symbolList[symbolDetectCrossList.countSymbol], stockSymbol, 6);
        longSymbolIndexArray[symbolIndex][f][0][SYMBOL_FOR_DETECT_CROSS] = symbolDetectCrossList.countSymbol++;
      }
      return longSymbolIndexArray[symbolIndex][f][0][SYMBOL_FOR_DETECT_CROSS];
    }
  }
  else if (stockSymbol[6] == '-')
  {
    g = 3;
  }
  else if (stockSymbol[6] == '.')
  {
    g = 4;
  }
  else if (stockSymbol[6] == '$')
  {
    g = 1;
  }
  else if (stockSymbol[6] == '+')
  {
    g = 2;
  }
  else
  {
    return -1;
  }
  
  //Character 8: 0, ., A-Z
  if ((stockSymbol[7] > 64) && (stockSymbol[7] < 91))
  {
    h = stockSymbol[7] - INDEX_OF_A_CHAR;//A-Z  
  }
  else if ((stockSymbol[7] == 0) || (stockSymbol[7] == 32))//NULL Because: 4: 3248, 32: SPACE
  {
    //This symbol has 7 characters
    //Now check for the first 5 chars
    int symbolIndex;
    if (symboIndexlList[a][b][c][d][e][SYMBOL_FOR_DETECT_CROSS] == -1)
    {
      memcpy(symbolDetectCrossList.symbolList[symbolDetectCrossList.countSymbol], stockSymbol, 5);
      symboIndexlList[a][b][c][d][e][SYMBOL_FOR_DETECT_CROSS] = symbolDetectCrossList.countSymbol++;
      
      // Get real index
      symbolIndex = symboIndexlList[a][b][c][d][e][SYMBOL_FOR_DETECT_CROSS];
      memcpy(symbolDetectCrossList.symbolList[symbolDetectCrossList.countSymbol], stockSymbol, 7);
      longSymbolIndexArray[symbolIndex][f][g][SYMBOL_FOR_DETECT_CROSS] = symbolDetectCrossList.countSymbol++;
      return longSymbolIndexArray[symbolIndex][f][g][SYMBOL_FOR_DETECT_CROSS];
    }
    else
    {
      symbolIndex = symboIndexlList[a][b][c][d][e][SYMBOL_FOR_DETECT_CROSS];
      
      if (longSymbolIndexArray[symbolIndex][f][g][SYMBOL_FOR_DETECT_CROSS] == -1)
      {
        memcpy(symbolDetectCrossList.symbolList[symbolDetectCrossList.countSymbol], stockSymbol, 7);
        longSymbolIndexArray[symbolIndex][f][g][SYMBOL_FOR_DETECT_CROSS] = symbolDetectCrossList.countSymbol++;
      }
      return longSymbolIndexArray[symbolIndex][f][g][SYMBOL_FOR_DETECT_CROSS];
    }
  }
  else if (stockSymbol[7] == '-')
  {
    h = 3;
  }
  else if (stockSymbol[7] == '.')
  {
    h = 4;
  }
  else if (stockSymbol[7] == '$')
  {
    h = 1;
  }
  else if (stockSymbol[7] == '+')
  {
    h = 2;
  }
  else
  {
    return -1;
  }
  
  //This symbol has 8 character!
  int tmpIndex;
  if (symboIndexlList[a][b][c][d][e][SYMBOL_FOR_DETECT_CROSS] == -1)
  {
    //Add 5-char symbol
    memcpy(symbolDetectCrossList.symbolList[symbolDetectCrossList.countSymbol], stockSymbol, 5);
    symboIndexlList[a][b][c][d][e][SYMBOL_FOR_DETECT_CROSS] = symbolDetectCrossList.countSymbol++;
      
    //Add 7-char symbol
    
    tmpIndex = symboIndexlList[a][b][c][d][e][SYMBOL_FOR_DETECT_CROSS];
    memcpy(symbolDetectCrossList.symbolList[symbolDetectCrossList.countSymbol], stockSymbol, 7);
    longSymbolIndexArray[tmpIndex][f][g][SYMBOL_FOR_DETECT_CROSS] = symbolDetectCrossList.countSymbol++;
    
    //Add 8-char symbol
    tmpIndex = longSymbolIndexArray[tmpIndex][f][g][SYMBOL_FOR_DETECT_CROSS];
    memcpy(symbolDetectCrossList.symbolList[symbolDetectCrossList.countSymbol], stockSymbol, 8);
    extraLongSymbolIndexArray[tmpIndex][h][SYMBOL_FOR_DETECT_CROSS] = symbolDetectCrossList.countSymbol++;
    return extraLongSymbolIndexArray[tmpIndex][h][SYMBOL_FOR_DETECT_CROSS];
  }
  else
  {
    tmpIndex = symboIndexlList[a][b][c][d][e][SYMBOL_FOR_DETECT_CROSS];
    
    if (longSymbolIndexArray[tmpIndex][f][g][SYMBOL_FOR_DETECT_CROSS] == -1)
    {
      //Add 7-char symbol
      memcpy(symbolDetectCrossList.symbolList[symbolDetectCrossList.countSymbol], stockSymbol, 7);
      longSymbolIndexArray[tmpIndex][f][g][SYMBOL_FOR_DETECT_CROSS] = symbolDetectCrossList.countSymbol++;  
    }
    tmpIndex = longSymbolIndexArray[tmpIndex][f][g][SYMBOL_FOR_DETECT_CROSS];
    
    if (extraLongSymbolIndexArray[tmpIndex][h][SYMBOL_FOR_DETECT_CROSS] == -1)
    {
      //Add 8-char symbol
      memcpy(symbolDetectCrossList.symbolList[symbolDetectCrossList.countSymbol], stockSymbol, 8);
      extraLongSymbolIndexArray[tmpIndex][h][SYMBOL_FOR_DETECT_CROSS] = symbolDetectCrossList.countSymbol++;
    }
    return extraLongSymbolIndexArray[tmpIndex][h][SYMBOL_FOR_DETECT_CROSS];
  }
}

/****************************************************************************
- Function name:  GetTSCrossIndex
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int GetTSCrossIndex(int crossId, int tsIndex, int logType)
{
  int index = crossId % MAX_CROSS_ID;
  int crossIndex;

  if (tradeOutputIndex[index][tsIndex][CROSS_INDEX] == -1)  //This is new Cross ID
  {
    tradeOutputIndex[index][tsIndex][CROSS_INDEX] = tsCrossCollection[tsIndex].countCross;
    
    if (OutputLogProcessingStatus[tsIndex].cStatus == 1)  //AS is being flooded by output log from a TS
    {
      if (logType < 10)
      {
        tradeOutputIndex[index][tsIndex][SKIP_STATUS] = 1;
        return ERROR; //This will skip and do not process this
      }
    }
    else
    {
      tradeOutputIndex[index][tsIndex][SKIP_STATUS] = 0;
    }
  }
  else
  {
    crossIndex = tradeOutputIndex[index][tsIndex][CROSS_INDEX];
    if (tsCrossCollection[tsIndex].crossList[crossIndex].summary.crossId != crossId)
    {
      //This slot was used by someone before, so overwritten it! --> Consider it as new one, reset things.....
      crossIndex = tsCrossCollection[tsIndex].countCross;
      tradeOutputIndex[index][tsIndex][CROSS_INDEX] = crossIndex;
      tsCrossCollection[tsIndex].crossList[crossIndex].summary.symbolIndex = -1;
      tsCrossCollection[tsIndex].crossList[crossIndex].summary.askShares = 0;
      tsCrossCollection[tsIndex].crossList[crossIndex].summary.bidShares = 0;
      tsCrossCollection[tsIndex].crossList[crossIndex].summary.worstAskPrice = 0.00;
      tsCrossCollection[tsIndex].crossList[crossIndex].summary.worstBidPrice = 0.00;
      memset(tsCrossCollection[tsIndex].crossList[crossIndex].summary.askBidList, 0, sizeof(tsCrossCollection[tsIndex].crossList[crossIndex].summary.askBidList));
      
      if (OutputLogProcessingStatus[tsIndex].cStatus == 1)  //AS is being flooded by output log from a TS
      {
        if (logType < 10)
        {
          tradeOutputIndex[index][tsIndex][SKIP_STATUS] = 1;
          return ERROR; //This will skip and do not process this
        }
      }
      else
      {
        tradeOutputIndex[index][tsIndex][SKIP_STATUS] = 0;
      }
    }
  }
  
  if (tradeOutputIndex[index][tsIndex][SKIP_STATUS] == 1)
  {
    return ERROR;  //This will skip and do not process this
  }
  
  return tradeOutputIndex[index][tsIndex][CROSS_INDEX];
}

/****************************************************************************
- Function name:  GetTradeIndex
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int GetTradeIndex(int crossId, int tsIndex)
{
  int index = crossId % MAX_CROSS_ID;

  if (tradeOutputIndex[index][tsIndex][TRADE_INDEX] == -1)
  {
    tradeOutputIndex[index][tsIndex][TRADE_INDEX] = tradeCollection.countTrade;
  }
  else
  {
    if (tradeCollection.tradeList[tradeOutputIndex[index][tsIndex][TRADE_INDEX]].summary.crossId != crossId)
    {
        //This trade index was used by someone, overwrite it! --> consider as new one
        tradeOutputIndex[index][tsIndex][TRADE_INDEX] = tradeCollection.countTrade;
    }
  }
  
  return tradeOutputIndex[index][tsIndex][TRADE_INDEX];
}

/****************************************************************************
- Function name:  LoadCurrentCrossID
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int LoadCurrentCrossID(const char *fileName)
{
  char fullPath[MAX_PATH_LEN];

  currentCrossId = 0;
  currentTradeId = 0;
  
  if (GetFullConfigPath(fileName, CONF_MISC, fullPath) == NULL_POINTER)
  {
    return ERROR;
  }
  
  FILE *fileDesc = fopen(fullPath, "r+");
  char tempBuffer[MAX_LINE_LEN];
  
  if (fileDesc != NULL)
  {
    //Number of lines read from the configuration file
    int count = 0;

    //Clear the buffer pointed to by line
    memset(tempBuffer, 0, MAX_LINE_LEN);
    
    while (fgets(tempBuffer, MAX_LINE_LEN, fileDesc) != NULL)
    {
      // Skip comment lines
      if ((tempBuffer[0] != '#') && (tempBuffer[0] > 32))
      {
        count ++;

        if (count == 1)
        {
          currentCrossId = atoi(tempBuffer);
        }
        else if (count == 2)
        {
          currentTradeId = atoi(tempBuffer);
        }
      }

      //Clear the buffer pointed to by line
      memset(tempBuffer, 0, MAX_LINE_LEN);
    }
    
    // Close file and release resource
    fclose(fileDesc);

    if (count != 2)
    {
      //Configuration file is invalid!
      TraceLog(ERROR_LEVEL, "Invalid configuration file '%s'\n", fullPath);

      return ERROR;
    }
  } 

  fileDesc = fopen(fullPath, "w+");
  
  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Cannot open file '%s' to read or we don't have permission to create\n", fullPath);

    return ERROR;
  }
  else
  {
    // Update fileCrossId
    fileCrossId = fileDesc;

    // Save current cross id
    SaveCurrentCrossID(currentCrossId, currentTradeId);
  }

  TraceLog(DEBUG_LEVEL, "currentCrossId = %d\n", currentCrossId);
  TraceLog(DEBUG_LEVEL, "currentTradeId = %d\n", currentTradeId);

  return SUCCESS;
}

/****************************************************************************
- Function name:  SaveCurrentCrossID
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SaveCurrentCrossID(int crossID, int tradeId)
{
  if (fileCrossId == NULL)
  {
    TraceLog(ERROR_LEVEL, "Inside SaveCurrentCrossID()\n");

    return ERROR;
  }

  rewind(fileCrossId);
  
  fprintf(fileCrossId, "%d\n", crossID);
  fprintf(fileCrossId, "%d\n", tradeId);

  fflush(fileCrossId);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadPositionOutputLogProcessedFromFile
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int LoadPositionOutputLogProcessedFromFile(const char *fileName)
{
  FILE *fileRead, *fileWrite;

  // Get full trade output path
  char fullPath[MAX_PATH_LEN] = "\0";
  
  if (GetFullRawDataPath(fileName, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Have some problem in get full raw data path operation (%s)\n", fileName);

    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Load position output log processed: %s\n", fullPath);
  if (IsFileTimestampToday(fullPath) == 0)
  {
    TraceLog(ERROR_LEVEL, "%s is old, please check the correctness of data files\n", fullPath);
    exit(0);
  }
  
  struct stat fileStat;
  int retValue = stat(fullPath, &fileStat);

  if (retValue == SUCCESS)
  {
    // Get file size
    int fileSizeInBytes = fileStat.st_size;

    // Check file size
    if (fileSizeInBytes == 0)
    {
      /* 
      Open file to read in BINARY MODE, 
      if it is not existed, we will create it
      */
      fileWrite = fopen(fullPath, "wb+");
      if (fileWrite == NULL)
      {
        TraceLog(ERROR_LEVEL, "Cannot open file '%s' to read or we don't have permission to create\n", fullPath);

        return ERROR;
      }
      else
      {
        // Store pointer to file that will be used after
        positionOutputLogProcessed.fileDesc = fileWrite;

        return SUCCESS;
      }

      return SUCCESS;
    }
    else if (fileSizeInBytes > 0)
    {
      if (fileSizeInBytes != MAX_TRADE_SERVER_CONNECTIONS * sizeof(int))
      {
        TraceLog(ERROR_LEVEL, "INVALID FILE SIZE: %d\n", fileSizeInBytes);

        return ERROR;
      }
    }
  }

  /* 
  Open file to read in BINARY MODE, 
  if it is not existed, we will create it
  */
  fileRead = fopen(fullPath, "r+");
  if (fileRead != NULL)
  {
    if (fread(positionOutputLogProcessed.tsPosition, MAX_TRADE_SERVER_CONNECTIONS * sizeof(int), 1, fileRead) != 1)
    {
      TraceLog(ERROR_LEVEL, "Cannot load count output log processed from file: %s\n", fullPath);

      return ERROR;
    }

    int i;

    for (i = 0; i < MAX_TRADE_SERVER_CONNECTIONS; i++)
    {
      TraceLog(DEBUG_LEVEL, "Trade Output Log items processed (ts%d): %d\n", i+1, positionOutputLogProcessed.tsPosition[i]);
    }

    // Close the file
    fclose(fileRead);
  } 

  /* 
  Open file to read in BINARY MODE, 
  if it is not existed, we will create it
  */
  fileWrite = fopen(fullPath, "wb+");
  if (fileWrite == NULL)
  {
    TraceLog(ERROR_LEVEL, "Cannot open file '%s' to read or we don't have permission to create\n", fullPath);

    return ERROR;
  }
  else
  {
    // Store pointer to file that will be used after
    positionOutputLogProcessed.fileDesc = fileWrite;

    // Save position output log processed to file
    SavePositionOutputLogProcessedToFile();

    return SUCCESS;
  }
  
  return SUCCESS;
}


/****************************************************************************
- Function name:  LoadPositionBBOInfoProcessedFromFile
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int LoadPositionBBOInfoProcessedFromFile(const char *fileName)
{
  FILE *fileRead, *fileWrite;

  // Get full trade output path
  char fullPath[MAX_PATH_LEN] = "\0";
  
  if (GetFullRawDataPath(fileName, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Have some problem in get full raw data path operation (%s)\n", fileName);

    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Load position bbo info processed: %s\n", fullPath);
  if (IsFileTimestampToday(fullPath) == 0)
  {
    TraceLog(ERROR_LEVEL, "%s is old, please check the correctness of data files\n", fullPath);
    exit(0);
  }
  
  struct stat fileStat;
  int retValue = stat(fullPath, &fileStat);

  //If file exist
  if (retValue == SUCCESS)
  {
    // Get file size
    int fileSizeInBytes = fileStat.st_size;

    // If filesize = 0, the initialize of processed index will be 0, we open if for ready to write
    if (fileSizeInBytes == 0)
    {
      /* 
      Open file to read in BINARY MODE, 
      if it is not existed, we will create it
      */
      fileWrite = fopen(fullPath, "wb+");
      if (fileWrite == NULL)
      {
        TraceLog(ERROR_LEVEL, "Cannot open file '%s' to read or we don't have permission to create\n", fullPath);

        return ERROR;
      }
      else
      {
        // Store pointer to file that will be used after
        fileBBOProcessedIndex = fileWrite;

        return SUCCESS;
      }

      return SUCCESS;
    }
    else if (fileSizeInBytes > 0) //If file size >0 we check if it's valid file or not
    {
      if (fileSizeInBytes != MAX_TRADE_SERVER_CONNECTIONS * sizeof(int))
      {
        TraceLog(ERROR_LEVEL, "INVALID FILE SIZE: %d\n", fileSizeInBytes);

        return ERROR;
      }
    }
  }

  /* 
  Open file to read in BINARY MODE, 
  if it is not existed, we will create it
  */
  
  //If we reach here, we have 2 cases:
  // 1:  File exists and valid --> processed index will be read from file --> open file for ready to write
  // 2: File does not exit --> open it for ready to write, processed index will be 0
  
  fileRead = fopen(fullPath, "r+");
  if (fileRead != NULL) //Case 1: File exists and valid, read the index from file and update index
  {
    int i;
    for (i = 0; i < MAX_TRADE_SERVER_CONNECTIONS; i++)
    {
      if (fread(&BBOInfoMgmt[i].processedIndex, sizeof(int), 1, fileRead) != 1)
      {
        TraceLog(ERROR_LEVEL, "Cannot load BBO processed index from file: %s\n", fullPath);
        return ERROR;
      }
    }
    
    for (i = 0; i < MAX_TRADE_SERVER_CONNECTIONS; i++)
    {
      TraceLog(DEBUG_LEVEL, "Loaded BBO Processed Index for ts %d: %d \n", i+1, BBOInfoMgmt[i].processedIndex);
    }

    // Close the file
    fclose(fileRead);
  }

  /* 
  Open file to read in BINARY MODE, 
  if it is not existed, we will create it
  */
  fileWrite = fopen(fullPath, "wb+");
  if (fileWrite == NULL)
  {
    TraceLog(ERROR_LEVEL, "Cannot open file '%s' to read or we don't have permission to create\n", fullPath);

    return ERROR;
  }
  else
  {
    // Store pointer to file that will be used after
    fileBBOProcessedIndex = fileWrite;

    // Save position output log processed to file
    SavePositionBBOInfoProcessedToFile();

    return SUCCESS;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SavePositionBBOInfoProcessedToFile
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SavePositionBBOInfoProcessedToFile(void)
{ 
  //TraceLog(DEBUG_LEVEL, "Save BBO Processed Index %p\n", fileBBOProcessedIndex);
  
  if (fileBBOProcessedIndex == NULL)
  {
    return ERROR;
  }

  rewind(fileBBOProcessedIndex);

  int i;
  for (i = 0; i < MAX_TRADE_SERVER_CONNECTIONS; i++)
  {
    //TraceLog(DEBUG_LEVEL, "Save BBO Processed Index for ts %d: %d\n", i, BBOInfoMgmt[i].processedIndex);
    
    if (fwrite(&BBOInfoMgmt[i].processedIndex, sizeof(int), 1, fileBBOProcessedIndex) != 1)
    {
      TraceLog(ERROR_LEVEL, "(%s) Cannot save bbo info processed index to file '%s'\n", __func__, FILE_NAME_OF_BBO_INFO_PROCESSED);

      return ERROR;
    }
  }
  
  // Flush all data from buffer to file
  fflush(fileBBOProcessedIndex);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SavePositionOutputLogProcessedToFile
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SavePositionOutputLogProcessedToFile(void)
{ 
  if (positionOutputLogProcessed.fileDesc == NULL)
  {
    TraceLog(DEBUG_LEVEL, "(%s) Cannot open file '%s'\n", __func__, FILE_NAME_OF_OUTPUT_LOG_PROCESSED);

    return ERROR;
  }

  rewind(positionOutputLogProcessed.fileDesc);

  if (fwrite(positionOutputLogProcessed.tsPosition, MAX_TRADE_SERVER_CONNECTIONS * sizeof(int), 1, positionOutputLogProcessed.fileDesc) != 1)
  {
    TraceLog(ERROR_LEVEL, "(%s) Cannot save count output log processed to file '%s'\n", __func__, FILE_NAME_OF_OUTPUT_LOG_PROCESSED);

    return ERROR;
  }

  // Flush all data from buffer to file
  fflush(positionOutputLogProcessed.fileDesc);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadTradeOutputCheckPointFromFile
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int LoadTradeOutputCheckPointFromFile(void)
{
  FILE *fileRead; 

  // Get full raw data path
  char fullPath[MAX_PATH_LEN] = "\0";
  
  if (GetFullRawDataPath(tradeOutputPoints.fileName, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Have some problem in get full raw data path operation (%s)\n", tradeOutputPoints.fileName);

    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Load trade output check point from file: %s\n", fullPath);

  struct stat fileStat;
  t_TradeOutputPoint tmpTradeOutputPoint;

  if (stat(fullPath, &fileStat) == SUCCESS)
  {
    // Check file size
    if (fileStat.st_size % (sizeof(tmpTradeOutputPoint.timestamp) + sizeof(tmpTradeOutputPoint.idTables) + sizeof(tmpTradeOutputPoint.tsPosition)) != 0)
    {
      TraceLog(ERROR_LEVEL, "INVALID FILE SIZE: %s\n", fullPath);

      return ERROR;
    }
  }

  int pointIndex = 0;
  int byteLoaded = 0;

  /* 
  Open file to read in BINARY MODE, if it is not existed, we will create it
  */
  fileRead = fopen(fullPath, "a+");
  if (fileRead != NULL)
  {
    // Seek to the beginning  of the file
    rewind(fileRead);
    
    while (byteLoaded < fileStat.st_size)
    {
      if (fread(&tradeOutputPoints.pointList[pointIndex].timestamp, sizeof(tmpTradeOutputPoint.timestamp), 1, fileRead) != 1)
      {
        TraceLog(ERROR_LEVEL, "Cannot load time stamp of check point from file: %s\n", fullPath);

        return ERROR;
      }
      
      if (fread(tradeOutputPoints.pointList[pointIndex].idTables, sizeof(tmpTradeOutputPoint.idTables), 1, fileRead) != 1)
      {
        TraceLog(ERROR_LEVEL, "Cannot load Ids of check point from file: %s\n", fullPath);

        return ERROR;
      }
      
      if (fread(tradeOutputPoints.pointList[pointIndex].tsPosition, sizeof(tmpTradeOutputPoint.tsPosition), 1, fileRead) != 1)
      {
        TraceLog(ERROR_LEVEL, "Cannot load TS indexs of check point from file: %s\n", fullPath);

        return ERROR;
      }
      
      tradeOutputPoints.pointList[pointIndex].isUpdate = 0;
      tradeOutputPoints.currentPoint = pointIndex;
      
      pointIndex += 1;
      byteLoaded += sizeof(tmpTradeOutputPoint.timestamp) + sizeof(tmpTradeOutputPoint.tsPosition);
    }

    // Close the file
    fclose(fileRead);
  }
  else
  {
    TraceLog(ERROR_LEVEL, "Cannot open file '%s' to read or we don't have permission to create\n", fullPath);

    return ERROR;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SaveTradeOutputCheckPointToFile
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SaveTradeOutputCheckPointToFile(void)
{ 
  FILE *fileWrite;  

  // Get full raw data path
  char fullPath[MAX_PATH_LEN] = "\0";
  
  if (GetFullRawDataPath(tradeOutputPoints.fileName, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Have some problem in get full raw data path operation (%s)\n", tradeOutputPoints.fileName);

    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Save trade output check point to file: %s\n", fullPath);

  t_TradeOutputPoint tmpTradeOutputPoint;
  int pointIndex;
  
  /* 
  Open file to read in BINARY MODE, if it is not existed, we will create it
  */
  fileWrite = fopen(fullPath, "w+");
  if (fileWrite != NULL)
  {
    for (pointIndex = 0; pointIndex < MAX_POINT; pointIndex++)
    {
      if (tradeOutputPoints.pointList[pointIndex].isUpdate == 0)
      {
        if (fwrite(&tradeOutputPoints.pointList[pointIndex].timestamp, sizeof(tmpTradeOutputPoint.timestamp), 1, fileWrite) != 1)
        {
          TraceLog(ERROR_LEVEL, "Cannot save time stamp of check point to file: %s\n", fullPath);

          return ERROR;
        }
        
        if (fwrite(tradeOutputPoints.pointList[pointIndex].idTables, sizeof(tmpTradeOutputPoint.idTables), 1, fileWrite) != 1)
        {
          TraceLog(ERROR_LEVEL, "Cannot save TS indexs of check point to file: %s\n", fullPath);

          return ERROR;
        }
        
        if (fwrite(tradeOutputPoints.pointList[pointIndex].tsPosition, sizeof(tmpTradeOutputPoint.tsPosition), 1, fileWrite) != 1)
        {
          TraceLog(ERROR_LEVEL, "Cannot save TS indexs of check point to file: %s\n", fullPath);

          return ERROR;
        }
      }
      else
      {
        break;
      }
    }
    
    // Flush all data from buffer to file
    fflush(fileWrite);
    
    // Close the file
    fclose(fileWrite);
  }
  else
  {
    TraceLog(ERROR_LEVEL, "Cannot open file '%s' to write or we don't have permission to create\n", fullPath);

    return ERROR;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  FindTradeOutputPointToRecovery
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
/*int FindTradeOutputPointToRecovery(int timestamp)
{
  int pointIndex, lastPoint = -1;
  
  for (pointIndex = 0; pointIndex < MAX_POINT; pointIndex++)
  {
    if ((tradeOutputPoints.pointList[pointIndex].isUpdate == 0) && 
      (tradeOutputPoints.pointList[pointIndex].timestamp >= timestamp))
    {
      lastPoint = pointIndex;
    }
    else
    {
      break;
    }
  }
  
  return lastPoint;
}*/

/****************************************************************************
- Function name:  LoadTradeOutputLog
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int LoadTradeOutputLog(void)
{
  int tsIndex;

  // Load current cross id
  if (LoadCurrentCrossID(FILE_NAME_OF_CROSS_ID) == ERROR)
  {
    return ERROR;
  } 

  // Output log
  for (tsIndex = 0; tsIndex < tradeServersInfo.maxConnections; tsIndex++)
  {
    if (LoadTradeOutputLogFromFile(tradeOutputLogMgmt[tsIndex].fileName, &tradeOutputLogMgmt[tsIndex], positionOutputLogProcessed.tsPosition[tsIndex]) == ERROR)
    {
      return ERROR;
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadTradeOutputLogFromFile
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int LoadTradeOutputLogFromFile(const char *fileName, void *logMgmt, int startIndex)
{
  t_OutputLogMgmt *outputLogMgmt = (t_OutputLogMgmt *)logMgmt;
  
  // Get full raw data path
  char fullPath[MAX_PATH_LEN] = "\0";
  
  if (GetFullRawDataPath(fileName, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Have some problem in get full output log path operation (%s)\n", fileName);

    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Load output log: %s\n", fullPath);

  if (IsFileTimestampToday(fullPath) == 0)
  {
    TraceLog(ERROR_LEVEL, "%s is old, please check the correctness of data files\n", fullPath);
    exit(0);
  }
  
  /* 
  Open file to read in BINARY MODE, 
  if it is not existed, we will create it
  */
  FILE *fileDesc = fopen(fullPath, "a+");
  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Cannot open file '%s' to read or we don't have permission to create\n", fullPath);

    return ERROR;
  }
  else
  {
    // Store pointer to file that will be used after
    outputLogMgmt->fileDesc = fileDesc;
    
    // Initialize ... 
    outputLogMgmt->countOutputLog = 0;
    outputLogMgmt->lastUpdatedIndex = -1;

    //Buffer to store each line in the configuration file
    char line[MAX_OUTPUT_LOG_TEXT_LEN];

    //Number of lines read from the configuration file
    int  count = 0;

    //Get each line from the configuration file
    while (fgets(line, MAX_OUTPUT_LOG_TEXT_LEN, fileDesc) != NULL) 
    {     
      if (strlen(line) > 1)
      {
        count ++;

        if (count >= startIndex)
        {
          // Add output log to collection
          AddOutputLogToCollection(line, outputLogMgmt);
        }
        else
        {
          outputLogMgmt->countOutputLog ++;
        }
      }           
    }

    TraceLog(DEBUG_LEVEL, "Count output log entries: %d\n", outputLogMgmt->countOutputLog);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessHistoryBBOInfoFromFile
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessHistoryBBOInfoFromFile(void)
{
  TraceLog(DEBUG_LEVEL, "Process history BBO Info ...\n");

  int numberOfBlocksToLoad, i, tsIndex;
  char fullPath[MAX_PATH_LEN] = "\0";
  char fileName[MAX_LINE_LEN];
  FILE *fileDesc = NULL;
  
  // Process trade output log from file
  for( tsIndex = 0; tsIndex < MAX_TRADE_SERVER_CONNECTIONS; tsIndex++ )
  {
    //Check if we need to load file to process
    if (BBOInfoMgmt[tsIndex].processedIndex < BBOInfoMgmt[tsIndex].receivedIndex)
    {
      TraceLog(DEBUG_LEVEL, "Processing BBO history for tsIndex: %d\n", tsIndex);

      //***************************************************
      //    Load required BBO Info from file into Memory
      //***************************************************
    
      //Get the file name
      sprintf(fileName, "bbo_info_ts%d.txt", tsIndex + 1);
  
      if (GetFullRawDataPath(fileName, fullPath) == NULL_POINTER)
      {
        TraceLog(ERROR_LEVEL, "Have some problem in get full bbo log path operation (%s)\n", fileName);
        return ERROR;
      }
  
      //Open file for reading
      if (IsFileTimestampToday(fullPath) == 0)
      {
        TraceLog(ERROR_LEVEL, "%s is old, please check the correctness of data files\n", fullPath);
        exit(0);
      }
      fileDesc = fopen(fullPath, "r");
      if (fileDesc == NULL)
      {
        TraceLog(ERROR_LEVEL, "Have some problem in openning file: %s\n", fullPath);
        return ERROR;
      }
      
      //Go to the required position of file
      if (fseek(fileDesc, BBOInfoMgmt[tsIndex].processedIndex * sizeof(t_BBOQuoteEntry), SEEK_SET) == -1)
      {
        TraceLog(ERROR_LEVEL, "Problem in accessing bbo info file %s\n", fullPath);
        return ERROR;
      }
      
      //Get the number of blocks to load, and then check it if it's ok to load
      numberOfBlocksToLoad = BBOInfoMgmt[tsIndex].receivedIndex - BBOInfoMgmt[tsIndex].processedIndex;
      
      if (numberOfBlocksToLoad >= MAX_BBO_ENTRY)
      {
        TraceLog(ERROR_LEVEL, "(Buffer overloaded!): There are so many block to load!\n");
        return ERROR;
      }
      
      //Read data into structure one block by one !!! This is important, since we may get over the buffer!
      for (i=0; i<numberOfBlocksToLoad; i++)
      {
        if (fread(&BBOInfoMgmt[tsIndex].BBOInfoCollection[(BBOInfoMgmt[tsIndex].processedIndex+i)%MAX_BBO_ENTRY], sizeof(t_BBOQuoteEntry), 1, fileDesc) != 1)
        {
          TraceLog(ERROR_LEVEL, "There is some problem while reading data from file %s\n", fullPath);
        }
      }
      
      //Close file when finished loading
      fclose(fileDesc);
      
      //Process Data from collection
      if (ProcessBBOInfoFromCollection(&BBOInfoMgmt[tsIndex]) == ERROR_BUFFER_OVER)
      {
        return ERROR;
      }
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessHistoryTradeOutputLog
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessHistoryTradeOutputLog(void)
{
  TraceLog(DEBUG_LEVEL, "Process history trade output log ...\n");

  int tsIndex;

  // Process trade output log from file
  for( tsIndex = 0; tsIndex < MAX_TRADE_SERVER_CONNECTIONS; tsIndex++ )
  {
    if (positionOutputLogProcessed.tsPosition[tsIndex] < tradeOutputLogMgmt[tsIndex].countOutputLog)
    {
      ProcessTradeOutputLogFromCollection(&positionOutputLogProcessed.tsPosition[tsIndex], &tradeOutputLogMgmt[tsIndex]);     
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTradeOutputLog
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTradeOutputLog(void)
{
  //TraceLog(DEBUG_LEVEL, "Process trade output log\n");

  int tsIndex;

  countTradePerEachProcess = 0;
  
  // Process trade output log from collection
  for( tsIndex = 0; tsIndex < MAX_TRADE_SERVER_CONNECTIONS; tsIndex++ )
  {
    if (positionOutputLogProcessed.tsPosition[tsIndex] < tradeOutputLogMgmt[tsIndex].countOutputLog)
    {
      if (ProcessTradeOutputLogFromCollection(&positionOutputLogProcessed.tsPosition[tsIndex], &tradeOutputLogMgmt[tsIndex]) == ERROR_BUFFER_OVER)
      {
        return ERROR;
      }
    }
  }
  
  if (countTradePerEachProcess > 0)
  {
    CheckNRaiseSingleServerTradesAlertToAllTT();
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTradeOutputLogFromCollection
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessTradeOutputLogFromCollection(int *countLogProcessed, void *outputLogMgmt)
{
  t_OutputLogMgmt *workingMgmt = (t_OutputLogMgmt *)outputLogMgmt;

  int i, logType, crossId, crossIndex, tradeIndex, symbolIndex;
  t_Parameters paraList;
  char dateString[MAX_TIMESTAMP];
  int shares, bookIndex, asCrossIndex;
  double price;

  int askBidIndex, tmpTradeId = 0;

  int tsIndex = workingMgmt->tsIndex;

  int numOutputLogs, startIndex;

  pthread_mutex_lock(&workingMgmt->updateMutex);

  // Number of trade output log need process
  numOutputLogs = workingMgmt->countOutputLog - (*countLogProcessed);

  // Ouput log index will start process
  startIndex = workingMgmt->lastUpdatedIndex - numOutputLogs + 1;

  pthread_mutex_unlock(&workingMgmt->updateMutex);

  TraceLog(DEBUG_LEVEL, "OutputLog (%s) processing: %d/%d\n", tradeServersInfo.config[tsIndex].description, *countLogProcessed, workingMgmt->countOutputLog);

  // Check collection is full
  if (numOutputLogs > MAX_OUTPUT_LOG_ENTRIES)
  {
    TraceLog(ERROR_LEVEL, "Trade output log collection is full: tsIndex = %d, countOutputLog = %d, countLogProcessed = %d\n", tsIndex, workingMgmt->countOutputLog, *countLogProcessed);

    // Add code here to process this problem
    return ERROR_BUFFER_OVER;
  }

  for (i = 0; i < numOutputLogs; i++)
  {
    // Parse ...
    paraList.countParameters = Lrc_Split(workingMgmt->tradeLogCollection[(startIndex + i) % MAX_OUTPUT_LOG_ENTRIES], 32, &paraList);

    if (paraList.countParameters < 3)
    {
      TraceLog(ERROR_LEVEL, "Invalid trade output log: %s\n", workingMgmt->tradeLogCollection[(startIndex + i) % MAX_OUTPUT_LOG_ENTRIES]);

      // Increase number of trade output log process
      (*countLogProcessed) ++;

      // Save position output log processed to file
      SavePositionOutputLogProcessedToFile();

      continue;
    }

    // Get log type
    logType = atoi(paraList.parameterList[0]);

    // Get cross is
    crossId = atoi(paraList.parameterList[1]);

    // Get date string
    GetDateStringFormEpochSecs(atoi(paraList.parameterList[2]), dateString);

    // ------------------------------------------------------
    if (logType == 5) // This logtype indicates "Symbol can not short sell"
    {
      // Insert trade output log into database (raw_crosses table to indicating short sell failed)
      // In table "RAW_CROSSES" if "Trade_Type" = -1 ---> symbol can not short sell
      
      //                      symbol        ts_cid  ts_id     date              contents                      trade_type
      
      ProcessInsertShortSellFailedToCollection (paraList.parameterList[4], crossId, tsIndex, dateString, workingMgmt->tradeLogCollection[(startIndex + i) % MAX_OUTPUT_LOG_ENTRIES], -1);
      
      // Get index of cross
      crossIndex = GetTSCrossIndex(crossId, tsIndex, logType);

      if (crossIndex == ERROR)
      {
        //return ERROR;

        // Increase number of trade output log process
        (*countLogProcessed) ++;

        // Save position output log processed to file
        SavePositionOutputLogProcessedToFile();

        continue;
      }
      
      if (crossIndex == tsCrossCollection[tsIndex].countCross)
      {
        if (tsCrossCollection[tsIndex].countCross + 1 == MAX_CROSS_ENTRIES)
        {
          TraceLog(ERROR_LEVEL, "%s cross collection is full, MAX_CROSS_ENTRIES = %d\n", tradeServersInfo.config[tsIndex].description, MAX_CROSS_ENTRIES);

          //return ERROR;
          // Increase number of trade output log process
          (*countLogProcessed) ++;

          // Save position output log processed to file
          SavePositionOutputLogProcessedToFile();

          continue;
        }

        tsCrossCollection[tsIndex].countCross ++;
      }
      
      // Update cross index
      // 1. tsCross.crossId
      // 2. tsCross.tsIndex
      tsCrossCollection[tsIndex].crossList[crossIndex].summary.crossId = crossId;
      tsCrossCollection[tsIndex].crossList[crossIndex].summary.tsIndex = tsIndex;
    
      tsCrossCollection[tsIndex].crossList[crossIndex].indexLogMsgList[tsCrossCollection[tsIndex].crossList[crossIndex].countLogMsg] = (startIndex + i) % MAX_OUTPUT_LOG_ENTRIES;
      tsCrossCollection[tsIndex].crossList[crossIndex].countLogMsg ++;
      
      // Increase number of trade output log process
      (*countLogProcessed) ++;

      // Save position output log processed to file
      SavePositionOutputLogProcessedToFile();
      
      continue;
    }
    // ------------------------------------------------------
    
    // Get index of cross
    crossIndex = GetTSCrossIndex(crossId, tsIndex, logType);

    if (crossIndex == ERROR)
    {
      //return ERROR;

      // Increase number of trade output log process
      (*countLogProcessed) ++;

      // Save position output log processed to file
      SavePositionOutputLogProcessedToFile();

      continue;
    }
    
    if (crossIndex == tsCrossCollection[tsIndex].countCross)
    {
      if (tsCrossCollection[tsIndex].countCross + 1 == MAX_CROSS_ENTRIES)
      {
        TraceLog(ERROR_LEVEL, "%s cross collection is full, MAX_CROSS_ENTRIES = %d\n", tradeServersInfo.config[tsIndex].description, MAX_CROSS_ENTRIES);

        //return ERROR;
        // Increase number of trade output log process
        (*countLogProcessed) ++;

        // Save position output log processed to file
        SavePositionOutputLogProcessedToFile();

        continue;
      }

      tsCrossCollection[tsIndex].countCross ++;
    }

    // Update cross index
    // 1. tsCross.crossId
    // 2. tsCross.tsIndex
    tsCrossCollection[tsIndex].crossList[crossIndex].summary.crossId = crossId;
    tsCrossCollection[tsIndex].crossList[crossIndex].summary.tsIndex = tsIndex;

    // Trade information
    if (logType >= 10)
    {
      // Get index of trade
      tradeIndex = GetTradeIndex(crossId, tsIndex);     

      if (tradeIndex == tradeCollection.countTrade)
      {
        currentTradeId ++;
        SaveCurrentCrossID(currentCrossId, currentTradeId);

        //TraceLog(DEBUG_LEVEL, "%d: %d, %d\n", startIndex + i, tradeIndex, currentTradeId);

        if (tradeCollection.countTrade + 1 == MAX_TOTAL_CROSS_ENTRIES)
        {
          TraceLog(ERROR_LEVEL, "Trades collection is full, MAX_TOTAL_CROSS_ENTRIES = %d\n", MAX_TOTAL_CROSS_ENTRIES);

          //return ERROR;
          // Increase number of trade output log process
          (*countLogProcessed) ++;

          // Save position output log processed to file
          SavePositionOutputLogProcessedToFile();

          continue;
        }

        tmpTradeId = currentTradeId;
        tradeCollection.countTrade ++;

        // Update trade output log to database
        ProcessInsertTradeOutputLog_TradeToCollection (tmpTradeId, crossId, tsIndex, dateString);
      }
      else
      {
        tmpTradeId = tradeCollection.tradeList[tradeIndex].summary.tradeId;
      }
      
      // 3. tsCross.tradeIndex
      tsCrossCollection[tsIndex].crossList[crossIndex].summary.tradeIndex = tradeIndex;

      // Update trade index
      // 1. trade.crossId
      // 2. trade.tsIndex
      // 3. trade.tradeId
      tradeCollection.tradeList[tradeIndex].summary.crossId = crossId;
      tradeCollection.tradeList[tradeIndex].summary.tsIndex = tsIndex;
      tradeCollection.tradeList[tradeIndex].summary.tradeId = tmpTradeId;

      /*
      int boughtShares;
      int soldShares;
      int leftSharesStuck;
      double totalBought;
      double totalSold;
      double totalStuck;
      */

      // Check symbol
      if ((logType == 10) && (tsCrossCollection[tsIndex].crossList[crossIndex].summary.symbolIndex == -1))
      {
        // Cross summary collection
        symbolIndex = UpdateStockSymbolDetectCrossIndex(paraList.parameterList[5]);

        if (symbolIndex == -1)
        {
          //return ERROR;
          // Increase number of trade output log process
          (*countLogProcessed) ++;

          // Save position output log processed to file
          SavePositionOutputLogProcessedToFile();

          //return ERROR;
          continue;
        }

        tsCrossCollection[tsIndex].crossList[crossIndex].summary.symbolIndex = symbolIndex;
      }
      else if (logType == 13)
      {
        countTradePerEachProcess++;
        
        // Trade completed
        tradeCollection.tradeList[tradeIndex].summary.totalBought = atof(paraList.parameterList[4]);
        tradeCollection.tradeList[tradeIndex].summary.totalSold = atof(paraList.parameterList[5]);

        tradeCollection.tradeList[tradeIndex].summary.boughtShares = atoi(paraList.parameterList[6]);
        tradeCollection.tradeList[tradeIndex].summary.soldShares = atoi(paraList.parameterList[7]);

        t_StuckInfo stuckInfo;
        int lockType = NO_LOCK, side;
        char symbol[SYMBOL_LEN];
        int numOfShares = 0;
        double stuckPrice = 0;

        /*
        TraceLog(DEBUG_LEVEL, "SANGNM (%d): tsIndex = %d, crossId = %d: boughtShares = %d, totalBought = %.02f, soldShares = %d, totalSold = %.02f\n\n", 
            logType, tsIndex, crossId, 
            tradeCollection.tradeList[tradeIndex].summary.boughtShares, 
            tradeCollection.tradeList[tradeIndex].summary.totalBought, 
            tradeCollection.tradeList[tradeIndex].summary.soldShares, 
            tradeCollection.tradeList[tradeIndex].summary.totalSold);
        */            
        if (tradeCollection.tradeList[tradeIndex].summary.soldShares != tradeCollection.tradeList[tradeIndex].summary.boughtShares)
        {
          // Stock Symbol
          // Number of Shares
          // Stuck Price
          // Side (Long/Short)
          
          // Symbol
          strncpy(symbol, paraList.parameterList[3], SYMBOL_LEN);
          
          // Side, NumOfShares, StuckPrice
          if (tradeCollection.tradeList[tradeIndex].summary.soldShares > tradeCollection.tradeList[tradeIndex].summary.boughtShares)
          {
            // Short
            lockType = LONG_LOCK;
            side = ASK_SIDE;
            numOfShares = tradeCollection.tradeList[tradeIndex].summary.soldShares - tradeCollection.tradeList[tradeIndex].summary.boughtShares;
            stuckPrice = tradeCollection.tradeList[tradeIndex].summary.totalSold / tradeCollection.tradeList[tradeIndex].summary.soldShares;            
          }
          else
          {
            // Long
            lockType = SHORT_LOCK;
            side = BID_SIDE;
            numOfShares = -(tradeCollection.tradeList[tradeIndex].summary.soldShares - tradeCollection.tradeList[tradeIndex].summary.boughtShares);
            stuckPrice = tradeCollection.tradeList[tradeIndex].summary.totalBought / tradeCollection.tradeList[tradeIndex].summary.boughtShares;
          }         

          // Add code to calculate real value of stuck

          stuckInfo.tsIndex = tsIndex;
          stuckInfo.crossId = tsCrossCollection[tsIndex].crossList[crossIndex].summary.crossId;
          stuckInfo.timestamp = atoi(paraList.parameterList[2]);
          stuckInfo.stuckShares = abs(numOfShares);
          stuckInfo.stuckPrice = stuckPrice;
          stuckInfo.realValue = 0.00;
          stuckInfo.totalFee = 0.0;

          double profitLoss = 0.0;
          if ((tradeCollection.tradeList[tradeIndex].summary.soldShares > 0) && (tradeCollection.tradeList[tradeIndex].summary.boughtShares > 0))
          {
            profitLoss = ((tradeCollection.tradeList[tradeIndex].summary.soldShares + tradeCollection.tradeList[tradeIndex].summary.boughtShares) / 2 - 
                      abs(tradeCollection.tradeList[tradeIndex].summary.soldShares - tradeCollection.tradeList[tradeIndex].summary.boughtShares) / 2) * 
                      (tradeCollection.tradeList[tradeIndex].summary.totalSold / tradeCollection.tradeList[tradeIndex].summary.soldShares - 
                      tradeCollection.tradeList[tradeIndex].summary.totalBought / tradeCollection.tradeList[tradeIndex].summary.boughtShares);
                      
            if (flt(profitLoss, 0.0))
            {
              int symbolIndex = GetStockSymbolIndex(symbol);
              double loss = fabs(profitLoss);
              
              if (stuckInfo.tsIndex > -1 && stuckInfo.tsIndex < MAX_TRADE_SERVER_CONNECTIONS)
              {
                ProfitLossPerSymbol[symbolIndex].tsLoss[stuckInfo.tsIndex] += loss;
              }
              else
              {
                TraceLog(DEBUG_LEVEL, "(CalculateRealValue) tsIndex=-1, calculate the loss (%lf, %.8s) for AS only\n", loss, symbol);
              }
              ProfitLossPerSymbol[symbolIndex].asLoss += loss;
              
              // DEVLOGF("Symbol %s: tsLoss[%d] = %lf, asLoss = %lf", symbol, stuckInfo.tsIndex, ProfitLossPerSymbol[symbolIndex].tsLoss[stuckInfo.tsIndex], ProfitLossPerSymbol[symbolIndex].asLoss);
                
              // Check to add symbol to Volatile or Disabled Symbol List
              CheckToAddSymbolToVolatileOrDisabledList(symbol, symbolIndex, stuckInfo.tsIndex);
            }
          }

          //TraceLog(DEBUG_LEVEL, "symbol = %.8s, stuckShares = %d, stuckPrice = %lf, profitLoss = %lf\n", paraList.parameterList[3], stuckInfo.stuckShares, stuckInfo.stuckPrice, profitLoss);
        }
        
        // Calculation trade results
        CalculateAndInsertTradeResults(tsIndex, crossIndex, tradeIndex, dateString, paraList.parameterList[3], atoi(paraList.parameterList[2]));

        if (lockType != NO_LOCK)
        {
          //TraceLog(DEBUG_LEVEL, "---> symbol %s\n", symbol);
          // Note: tsIndex = 0 --> TS1
          
          //Check if there is some stuck of same symbol is trading on AS/PM
          //If yes, we will not send stuck to PM

          int willSend = 1;
          int account = TradingAccount.accountForTS[tsIndex];

          int stockSymbolIndex = GetStockSymbolIndex(symbol);
          if (stockSymbolIndex == -1)
          {
            TraceLog(ERROR_LEVEL, "ProcessTradeOutputLogFromCollection: Invalid symbol %.8s\n", symbol);

            return ERROR;
          }

          int lastTradingStatus = verifyPositionInfo[account][stockSymbolIndex].isTrading;
          if (lastTradingStatus == TRADING_MANUAL)
          {
            willSend = 0; //stuck is being manual ordered, so do not send to PM
          }
    
          InsertAndCalculateRealValue(paraList.parameterList[3], side, &stuckInfo, dateString);

          if (willSend == 1)  //Send stuck to PM, PM will know how to aggregate/evaluate
          {
            verifyPositionInfo[account][stockSymbolIndex].isTrading = TRADING_AUTOMATIC;
            verifyPositionInfo[account][stockSymbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
            
            // Build and send stuck to PM
            t_TSMessage pmMessage;
          
            //Call function to update verify position list
            UpdateVerifyPositionInfoForStuckFromTSOutputLog(symbol, numOfShares, side, account, lockType);

            BuildPMMessageForSendStuckInfo(&pmMessage, symbol, numOfShares, stuckPrice, side, crossId, account);
          
            if (SendPMMessage(&pmMessage) == SUCCESS)
            {
              TraceLog(DEBUG_LEVEL, "Sent stuck info to PM: symbol = %.8s, side = %d, stuckShares = %d, stuckPrice = %lf, account = %d\n", symbol, side, numOfShares, stuckPrice, account);
            }
            else
            {
              verifyPositionInfo[account][stockSymbolIndex].isTrading = lastTradingStatus;
              verifyPositionInfo[account][stockSymbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
            }
          }
          else
          {
            //Call function to update verify position list
            UpdateVerifyPositionInfoForStuckFromTSOutputLog(symbol, numOfShares, side, account, lockType);
              
            //Alert TT for stuck not being handled, because there is another stuck with the same symbol is being handled
            //level 0: shows on log, 1: message box
            char msg[1024];
            sprintf(msg, "Received stuck from TS (%.8s, side %d, shares: %d, price %lf), however there is a stuck of same symbol/account trading manually, so this stuck is not handled for now, please engage manually later", symbol, side, numOfShares, stuckPrice);
            SendNotifyMessageToAllTT(msg, 0);
            BuildAndSendAlertToAllDaedalus(msg, COM__RGM__DAEDALUS__PROTO__NOTIFICATION_LEVEL_TYPE__ShowLog);
          }
        }
      }

      if (tradeCollection.tradeList[tradeIndex].countLogMsg < MAX_DETAIL_PER_TRADE - 2)
      {
        tradeCollection.tradeList[tradeIndex].indexLogMsgList[tradeCollection.tradeList[tradeIndex].countLogMsg] = (startIndex + i) % MAX_OUTPUT_LOG_ENTRIES;
        tradeCollection.tradeList[tradeIndex].countLogMsg ++;
      }
      else
      {
        if (logType == 13)
        {
          tradeCollection.tradeList[tradeIndex].indexLogMsgList[MAX_DETAIL_PER_TRADE - 2] = (startIndex + i) % MAX_OUTPUT_LOG_ENTRIES;
          tradeCollection.tradeList[tradeIndex].countLogMsg = MAX_DETAIL_PER_TRADE - 1;
        }
        else if (logType == 14)
        {
          tradeCollection.tradeList[tradeIndex].indexLogMsgList[MAX_DETAIL_PER_TRADE - 1] = (startIndex + i) % MAX_OUTPUT_LOG_ENTRIES;
          tradeCollection.tradeList[tradeIndex].countLogMsg = MAX_DETAIL_PER_TRADE;
        }
      }   

      asCrossIndex = tsCrossCollection[tsIndex].crossList[crossIndex].summary.asCrossIndex;
      if (asCrossIndex != -1)
      {
        // 4. trade.asCrossId
        tradeCollection.tradeList[tradeIndex].summary.asCrossId = asCrossCollection.crossList[asCrossIndex].crossId;

        asCrossCollection.crossList[asCrossIndex].tradeIndex = tradeIndex;
        asCrossCollection.crossList[asCrossIndex].tsIndex = tsIndex;

        ProcessInsertTradeOutputLogDetected_TradeToCollection (asCrossCollection.crossList[asCrossIndex].crossId, 
                      asCrossCollection.crossList[asCrossIndex].symbol, 
                      crossId, tsIndex, dateString, workingMgmt->tradeLogCollection[(startIndex + i) % MAX_OUTPUT_LOG_ENTRIES], tmpTradeId, 
                      tsCrossCollection[tsIndex].crossList[crossIndex].summary.tradeType);
      }
      else
      {
        // Insert trade output log into database
        ProcessInsertTradeOutputLogNotYetDetect_TradeToCollection(crossId, tsIndex, dateString, workingMgmt->tradeLogCollection[(startIndex + i) % MAX_OUTPUT_LOG_ENTRIES], tmpTradeId);
      }

      // Add code here to process update trade information
    }
    // Cross information
    else 
    {
      tsCrossCollection[tsIndex].crossList[crossIndex].indexLogMsgList[tsCrossCollection[tsIndex].crossList[crossIndex].countLogMsg] = (startIndex + i) % MAX_OUTPUT_LOG_ENTRIES;
      tsCrossCollection[tsIndex].crossList[crossIndex].countLogMsg ++;

      // �[LogType CrossID Timestamp]TRADE: Stock symbol SNY, NASDAQ Server, NDAQ Bid launch�
      if (logType == 2)
      {
        if (paraList.countParameters >= 9)
        {
          // Cross summary collection
          symbolIndex = UpdateStockSymbolDetectCrossIndex(paraList.parameterList[3]);

          if (symbolIndex == -1)
          {
            //return ERROR;
            // Increase number of trade output log process
            (*countLogProcessed) ++;

            // Save position output log processed to file
            SavePositionOutputLogProcessedToFile();

            //return ERROR;
            continue;
          }

          tsCrossCollection[tsIndex].crossList[crossIndex].summary.timestamp = atoi(paraList.parameterList[2]);
          tsCrossCollection[tsIndex].crossList[crossIndex].summary.symbolIndex = symbolIndex;
          tsCrossCollection[tsIndex].crossList[crossIndex].summary.serverRole = atoi(paraList.parameterList[4]);
          tsCrossCollection[tsIndex].crossList[crossIndex].summary.ecnLaunch = atoi(paraList.parameterList[5]);
          tsCrossCollection[tsIndex].crossList[crossIndex].summary.askBidLaunch = atoi(paraList.parameterList[6]);
          tsCrossCollection[tsIndex].crossList[crossIndex].summary.tradeType = NORMALLY_TRADE;
          tsCrossCollection[tsIndex].crossList[crossIndex].summary.ecnExist = atoi(paraList.parameterList[7]);
        
          // Add code here!
          ProcessGroupTradeOutputLog(tsIndex, crossIndex);
        }
      }
      else if (logType < 2)
      {
        tsCrossCollection[tsIndex].crossList[crossIndex].summary.isMultiLevel = MULTI_LEVEL_VALUE;
      }
      // ASK: 3000 ARCA at 42.68
      else if (logType == 3)
      {
        askBidIndex = atoi(paraList.parameterList[3]);
        shares = atoi(paraList.parameterList[4]);
        bookIndex = atoi(paraList.parameterList[5]);
        price = atof(paraList.parameterList[6]);
        
        if (askBidIndex == ASK_INDEX)
        {
          tsCrossCollection[tsIndex].crossList[crossIndex].summary.askBidList[ASK_INDEX][bookIndex].bookIndex = bookIndex;
          tsCrossCollection[tsIndex].crossList[crossIndex].summary.askBidList[ASK_INDEX][bookIndex].shares = shares;
          tsCrossCollection[tsIndex].crossList[crossIndex].summary.askBidList[ASK_INDEX][bookIndex].price = price;
          
          tsCrossCollection[tsIndex].crossList[crossIndex].summary.askShares += shares;
          
          if (feq(tsCrossCollection[tsIndex].crossList[crossIndex].summary.worstAskPrice, 0.0))
          {
            tsCrossCollection[tsIndex].crossList[crossIndex].summary.worstAskPrice = price;
          }
          
          // Change definition of cross: maximum spread -> minimum spread
          if (flt(tsCrossCollection[tsIndex].crossList[crossIndex].summary.worstAskPrice, price))
          {
            tsCrossCollection[tsIndex].crossList[crossIndex].summary.worstAskPrice = price;
          }
          
          ProcessInsertAskBidToCollection(tsCrossCollection[tsIndex].crossList[crossIndex].summary.crossId, tsIndex, shares, price, ASK_INDEX, atoi(paraList.parameterList[5]), dateString);
        }
        else if (askBidIndex == BID_INDEX)
        {
          tsCrossCollection[tsIndex].crossList[crossIndex].summary.askBidList[BID_INDEX][bookIndex].bookIndex = bookIndex;
          tsCrossCollection[tsIndex].crossList[crossIndex].summary.askBidList[BID_INDEX][bookIndex].shares = shares;
          tsCrossCollection[tsIndex].crossList[crossIndex].summary.askBidList[BID_INDEX][bookIndex].price = price;
          
          tsCrossCollection[tsIndex].crossList[crossIndex].summary.bidShares += shares;
          
          if (feq(tsCrossCollection[tsIndex].crossList[crossIndex].summary.worstBidPrice, 0.0))
          {
            tsCrossCollection[tsIndex].crossList[crossIndex].summary.worstBidPrice = price;
          }
          
          // Change definition of cross: maximum spread -> minimum spread
          if (fgt(tsCrossCollection[tsIndex].crossList[crossIndex].summary.worstBidPrice, price))
          {
            tsCrossCollection[tsIndex].crossList[crossIndex].summary.worstBidPrice = price;
          }

          ProcessInsertAskBidToCollection(tsCrossCollection[tsIndex].crossList[crossIndex].summary.crossId, tsIndex, shares, price, BID_INDEX, atoi(paraList.parameterList[5]), dateString);
        }
        else
        {
          TraceLog(ERROR_LEVEL, "Invalid ASK/BID = %d\n", askBidIndex);
        }
      }

      asCrossIndex = tsCrossCollection[tsIndex].crossList[crossIndex].summary.asCrossIndex;
      if (asCrossIndex != -1)
      {
        if (tsCrossCollection[tsIndex].crossList[crossIndex].summary.tradeIndex != -1)
        {
          // Insert trade output log into database
          ProcessInsertTradeOutputLogDetected_TradeToCollection(asCrossCollection.crossList[asCrossIndex].crossId, 
                        asCrossCollection.crossList[asCrossIndex].symbol, 
                        crossId, tsIndex, dateString, workingMgmt->tradeLogCollection[(startIndex + i) % MAX_OUTPUT_LOG_ENTRIES], 
                        tradeCollection.tradeList[tsCrossCollection[tsIndex].crossList[crossIndex].summary.tradeIndex].summary.tradeId, 
                        tsCrossCollection[tsIndex].crossList[crossIndex].summary.tradeType);
        }
        else
        {
          // Insert trade output log into database
          ProcessInsertTradeOutputLogDetectedToCollection(asCrossCollection.crossList[asCrossIndex].crossId, 
                        asCrossCollection.crossList[asCrossIndex].symbol, 
                        crossId, tsIndex, dateString, workingMgmt->tradeLogCollection[(startIndex + i) % MAX_OUTPUT_LOG_ENTRIES], 
                        tsCrossCollection[tsIndex].crossList[crossIndex].summary.tradeType);
        }
      }
      else
      {
        if (tsCrossCollection[tsIndex].crossList[crossIndex].summary.tradeIndex != -1)
        {
          // Insert trade output log into database
          ProcessInsertTradeOutputLogNotYetDetect_TradeToCollection(crossId, tsIndex, dateString, workingMgmt->tradeLogCollection[(startIndex + i) % MAX_OUTPUT_LOG_ENTRIES], tradeCollection.tradeList[tsCrossCollection[tsIndex].crossList[crossIndex].summary.tradeIndex].summary.tradeId);
        }
        else
        {
          // Insert trade output log into database
          ProcessInsertTradeOutputLogNotYetDetectToCollection(crossId, tsIndex, dateString, workingMgmt->tradeLogCollection[(startIndex + i) % MAX_OUTPUT_LOG_ENTRIES]);
        }
      }
    }

    asCrossIndex = tsCrossCollection[tsIndex].crossList[crossIndex].summary.asCrossIndex;
    if (asCrossIndex != -1)
    {
      // isUpdate ...
      memset(asCrossCollection.crossList[asCrossIndex].isUpdate, UPDATED, MAX_TRADER_TOOL_CONNECTIONS);
    }

    // Increase number of trade output log process
    (*countLogProcessed) ++;

    // Save position output log processed to file
    SavePositionOutputLogProcessedToFile();
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessGroupTradeOutputLog
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessGroupTradeOutputLog(int tsIndex, int index)
{
  int crossIndex;
  int i, isFound;
  char dateString[MAX_TIMESTAMP];

  // Get date string
  GetDateStringFormEpochSecs(tsCrossCollection[tsIndex].crossList[index].summary.timestamp, dateString);

  /*
  char isUpdate[MAX_TRADER_TOOL_CONNECTIONS];
  char symbol[SYMBOL_LEN];
  int symbolIndex;
  int crossId;
  int timestamp;
  int ecnLaunch;
  int askBidLaunch;
  int tradeId;
  int tsIndex;
  int indexCrossList[MAX_TRADE_SERVER_CONNECTIONS]; 
  */
  
  isFound = 0;

  for (i = asCrossCollection.countCross - 1; i >= 0; i --)
  {
    if (asCrossCollection.crossList[i].timestamp < (tsCrossCollection[tsIndex].crossList[index].summary.timestamp - 2))
    {
      break;
    }

    if (abs(asCrossCollection.crossList[i].timestamp - tsCrossCollection[tsIndex].crossList[index].summary.timestamp) < 2)
    {
      if ((asCrossCollection.crossList[i].symbolIndex == tsCrossCollection[tsIndex].crossList[index].summary.symbolIndex) && 
        (asCrossCollection.crossList[i].ecnLaunch == tsCrossCollection[tsIndex].crossList[index].summary.ecnLaunch) && 
        (asCrossCollection.crossList[i].askBidLaunch == tsCrossCollection[tsIndex].crossList[index].summary.askBidLaunch) && 
        (asCrossCollection.crossList[i].ecnExist == tsCrossCollection[tsIndex].crossList[index].summary.ecnExist) && 
        (asCrossCollection.crossList[i].indexCrossList[tsIndex] == -1))
      {
        isFound = 1;
        crossIndex = i;
      }
    }
  }
  
  if (isFound == 1)
  {     
    asCrossCollection.crossList[crossIndex].indexCrossList[tsIndex] = index;

    // 4. tsCross.asCrossIndex
    tsCrossCollection[tsIndex].crossList[index].summary.asCrossIndex = crossIndex;

    // Update trade output log to database
    ProcessInsertTradeOutputLogToCollection(asCrossCollection.crossList[crossIndex].crossId, 
                asCrossCollection.crossList[crossIndex].symbol, 
                tsCrossCollection[tsIndex].crossList[index].summary.crossId, 
                tsIndex, dateString, tsCrossCollection[tsIndex].crossList[index].summary.tradeType);

    // isUpdate ...
    memset(asCrossCollection.crossList[crossIndex].isUpdate, UPDATED, MAX_TRADER_TOOL_CONNECTIONS);
  }
  else
  {
    crossIndex = asCrossCollection.countCross;

    // Save current cross id
    currentCrossId ++;
    SaveCurrentCrossID(currentCrossId, currentTradeId);
    
    if (asCrossCollection.countCross + 1 == MAX_TOTAL_CROSS_ENTRIES)
    {
      TraceLog(ERROR_LEVEL, "Cross grouped collection is full, MAX_TOTAL_CROSS_ENTRIES = %d\n", MAX_TOTAL_CROSS_ENTRIES);

      return ERROR;
    }
    
    asCrossCollection.crossList[crossIndex].symbolIndex = tsCrossCollection[tsIndex].crossList[index].summary.symbolIndex;
    strncpy(asCrossCollection.crossList[crossIndex].symbol, symbolDetectCrossList.symbolList[tsCrossCollection[tsIndex].crossList[index].summary.symbolIndex], SYMBOL_LEN);
    asCrossCollection.crossList[crossIndex].crossId = currentCrossId;
    asCrossCollection.crossList[crossIndex].timestamp = tsCrossCollection[tsIndex].crossList[index].summary.timestamp;
    asCrossCollection.crossList[crossIndex].ecnLaunch = tsCrossCollection[tsIndex].crossList[index].summary.ecnLaunch;
    asCrossCollection.crossList[crossIndex].askBidLaunch = tsCrossCollection[tsIndex].crossList[index].summary.askBidLaunch;
    asCrossCollection.crossList[crossIndex].ecnExist = tsCrossCollection[tsIndex].crossList[index].summary.ecnExist;
    asCrossCollection.crossList[crossIndex].indexCrossList[tsIndex] = index;

    // 4. tsCross.asCrossIndex
    tsCrossCollection[tsIndex].crossList[index].summary.asCrossIndex = crossIndex;
    
    if (tsCrossCollection[tsIndex].crossList[index].summary.tradeIndex != -1) 
    {
      asCrossCollection.crossList[crossIndex].tradeIndex = tsCrossCollection[tsIndex].crossList[index].summary.tradeIndex;
      asCrossCollection.crossList[crossIndex].tsIndex = tsIndex;
    }

    asCrossCollection.countCross ++;

    // Update trade output log to database
    ProcessInsertTradeOutputLogToCollection(asCrossCollection.crossList[crossIndex].crossId, 
                asCrossCollection.crossList[crossIndex].symbol, 
                tsCrossCollection[tsIndex].crossList[index].summary.crossId, 
                tsIndex, dateString, tsCrossCollection[tsIndex].crossList[index].summary.tradeType);

    // isUpdate ...
    memset(asCrossCollection.crossList[crossIndex].isUpdate, UPDATED, MAX_TRADER_TOOL_CONNECTIONS);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  CalculateExpectedProfit
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
double CalculateExpectedProfit(int tsIndex, int crossIndex, int expectedShares)
{
  double totalAsk = 0.0, totalBid = 0.0, totalFees = 0.0;
  int i, j, remaingShares;
  t_BestAskBid askBidList[TS_MAX_BOOK_CONNECTIONS], tmpAskBid;
  
  remaingShares = expectedShares;
  memcpy(askBidList, tsCrossCollection[tsIndex].crossList[crossIndex].summary.askBidList[ASK_INDEX], sizeof(askBidList));
  
  // Sorting ascending for ASK list
  for (i = 0; i < TS_MAX_BOOK_CONNECTIONS - 1; i++)
  {
    for (j = i + 1; j < TS_MAX_BOOK_CONNECTIONS; j++)
    {
      if (flt(askBidList[j].price, askBidList[i].price))
      {
        memcpy(&tmpAskBid, &askBidList[i], sizeof(t_BestAskBid));
        memcpy(&askBidList[i], &askBidList[j], sizeof(t_BestAskBid));
        memcpy(&askBidList[j], &tmpAskBid, sizeof(t_BestAskBid));
      }
    }
  }
  
  for (i = 0; i < TS_MAX_BOOK_CONNECTIONS; i++)
  {
    // Total amount / fees to buy   
    if (askBidList[i].shares > 0)
    {
      if (remaingShares > askBidList[i].shares)
      {
        totalAsk += askBidList[i].shares * askBidList[i].price;
        
        // totalSecFees + totalTafFee + totalClearingCommission + totalVenueFee
        totalFees += (
                0.0 + 
                (askBidList[i].shares * FeeConf.tafFee) + 
                (askBidList[i].shares * FeeConf.commission) + 
                (fge(askBidList[i].price, 1.00) ? (askBidList[i].shares * FeeConf.removeVenueFeeWithPriceLager1) : 
                  (askBidList[i].shares * askBidList[i].price * FeeConf.removeVenueFeeWithPriceSmaler1)
                )
              );
        
        remaingShares -= askBidList[i].shares;
      }
      else
      {
        totalAsk += remaingShares * askBidList[i].price;
        
        // totalSecFees + totalTafFee + totalClearingCommission + totalVenueFee
        totalFees += (
                0.0 + 
                (remaingShares * FeeConf.tafFee) + 
                (remaingShares * FeeConf.commission) + 
                (fge(askBidList[i].price, 1.00) ? (remaingShares * FeeConf.removeVenueFeeWithPriceLager1) : 
                  (remaingShares * askBidList[i].price * FeeConf.removeVenueFeeWithPriceSmaler1)
                )
              );
              
        remaingShares = 0;
      }
      
      if (remaingShares == 0) 
      {
        break;
      }
    }
  }
  
  remaingShares = expectedShares;
  memcpy(askBidList, tsCrossCollection[tsIndex].crossList[crossIndex].summary.askBidList[BID_INDEX], sizeof(askBidList));
  
  // Sorting descending for BID list
  for (i = 0; i < TS_MAX_BOOK_CONNECTIONS - 1; i++)
  {
    for (j = i + 1; j < TS_MAX_BOOK_CONNECTIONS; j++)
    {
      if (fgt(askBidList[j].price, askBidList[i].price))
      {
        memcpy(&tmpAskBid, &askBidList[i], sizeof(t_BestAskBid));
        memcpy(&askBidList[i], &askBidList[j], sizeof(t_BestAskBid));
        memcpy(&askBidList[j], &tmpAskBid, sizeof(t_BestAskBid));
      }
    }
  }
  
  for (i = 0; i < TS_MAX_BOOK_CONNECTIONS; i++)
  {
    // Total amount / fees to sell 
    if (askBidList[i].shares > 0)
    {
      if (remaingShares > askBidList[i].shares)
      {
        totalBid += askBidList[i].shares * askBidList[i].price;
        
        // totalSecFees + totalTafFee + totalClearingCommission + totalVenueFee
        totalFees += (
                (askBidList[i].shares * askBidList[i].price * FeeConf.secFee) + 
                (askBidList[i].shares * FeeConf.tafFee) + 
                (askBidList[i].shares * FeeConf.commission) + 
                (fge(askBidList[i].price, 1.00) ? (askBidList[i].shares * FeeConf.removeVenueFeeWithPriceLager1) : 
                  (askBidList[i].shares * askBidList[i].price * FeeConf.removeVenueFeeWithPriceSmaler1)
                )
              );
        
        remaingShares -= askBidList[i].shares;
      }
      else
      {
        totalBid += remaingShares * askBidList[i].price;
        
        // totalSecFees + totalTafFee + totalClearingCommission + totalVenueFee
        totalFees += (
                (remaingShares * askBidList[i].price * FeeConf.secFee) + 
                (remaingShares * FeeConf.tafFee) + 
                (remaingShares * FeeConf.commission) + 
                (fge(askBidList[i].price, 1.00) ? (remaingShares * FeeConf.removeVenueFeeWithPriceLager1) : 
                  (remaingShares * askBidList[i].price * FeeConf.removeVenueFeeWithPriceSmaler1)
                )
              );
              
        remaingShares = 0;
      }
      
      if (remaingShares == 0) 
      {
        break;
      }
    }
  }
  
  DEVLOGF("Trade id %d: expectedPL %lf, totalFees %lf\n", asCrossCollection.crossList[tsCrossCollection[tsIndex].crossList[crossIndex].summary.asCrossIndex].crossId, totalBid - totalAsk, totalFees);
  
  return ((totalBid - totalAsk) - totalFees);
}

/****************************************************************************
- Function name:  CalculateAndInsertTradeResults
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int CalculateAndInsertTradeResults(int tsIndex, int crossIndex, int tradeIndex, char *dateString, char *symbol, int timestamp)
{
  t_TradeResultInfo tradeResult;

  int askShares = tsCrossCollection[tsIndex].crossList[crossIndex].summary.askShares;
  int bidShares = tsCrossCollection[tsIndex].crossList[crossIndex].summary.bidShares;
  
  // expectedShares
  tradeResult.expectedShares = (bidShares > askShares) ? askShares : bidShares;
  
  // expectedPL
  tradeResult.expectedPL = CalculateExpectedProfit(tsIndex, crossIndex, tradeResult.expectedShares);

  // boughtShares
  tradeResult.boughtShares = tradeCollection.tradeList[tradeIndex].summary.boughtShares;

  // totalBought
  tradeResult.totalBought = tradeCollection.tradeList[tradeIndex].summary.totalBought;

  // soldShares
  tradeResult.soldShares = tradeCollection.tradeList[tradeIndex].summary.soldShares;

  // totalSold
  tradeResult.totalSold = tradeCollection.tradeList[tradeIndex].summary.totalSold;

  // leftStuckShares
  tradeResult.leftStuckShares = abs(tradeCollection.tradeList[tradeIndex].summary.boughtShares - tradeCollection.tradeList[tradeIndex].summary.soldShares);

  // totalStuck
  tradeResult.totalStuck = 0.00;

  // as_cid
  tradeResult.as_cid = asCrossCollection.crossList[tsCrossCollection[tsIndex].crossList[crossIndex].summary.asCrossIndex].crossId;

  // ts_cid
  tradeResult.ts_cid = tsCrossCollection[tsIndex].crossList[crossIndex].summary.crossId;

  // ts_id
  tradeResult.ts_id = tsIndex;

  // trade_id
  tradeResult.trade_id = tradeCollection.tradeList[tradeIndex].summary.tradeId;

  // dateString
  strcpy(tradeResult.dateString, dateString);

  // symbol
  strncpy(tradeResult.symbol, symbol, SYMBOL_LEN);
  
  // Timestamp
  tradeResult.timestamp = timestamp;
  
  // Spread
  tradeResult.spread = tsCrossCollection[tsIndex].crossList[crossIndex].summary.worstBidPrice - tsCrossCollection[tsIndex].crossList[crossIndex].summary.worstAskPrice;

  // Insert trade result to database
  ProcessInsertTradeResultToCollection(tradeResult);
    
    // Save Loss/Profit to raise alert if needed
    if (tradeResult.leftStuckShares == 0)
    {
      int symbolIndex = GetStockSymbolIndex(symbol);
      if (fgt(tradeResult.totalBought, tradeResult.totalSold))
      {
        //loss
        double loss = tradeResult.totalBought - tradeResult.totalSold;
        ProfitLossPerSymbol[symbolIndex].tsLoss[tsIndex] += loss;
        ProfitLossPerSymbol[symbolIndex].asLoss += loss;
    
        // DEVLOGF("Symbol %s: tsLoss[%d] = %lf, asLoss = %lf", symbol, tsIndex, ProfitLossPerSymbol[symbolIndex].tsLoss[tsIndex], ProfitLossPerSymbol[symbolIndex].asLoss);

        // Check to add symbol to Volatile or Disabled Symbol List
        CheckToAddSymbolToVolatileOrDisabledList(symbol, symbolIndex, tsIndex);
      }
    }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  InsertAndCalculateRealValue
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int InsertAndCalculateRealValue(char *symbol, int side, t_StuckInfo *stuckInfo, char *dateString)
{
  // Get symbol index
  int symbolIndex = GetStockSymbolIndex(symbol);

  if (symbolIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "%s: Invalid symbol %.8s in the stuck with crossId = %d, tsIndex = %d\n", __func__, symbol, stuckInfo->crossId, stuckInfo->tsIndex);

    return ERROR;
  }

  t_StuckInfo *stuckItem;
  int stuckIndex;
  int stuckSharesBackup = stuckInfo->stuckShares;

  //TraceLog(DEBUG_LEVEL, "symbol = %.8s, symbolIndex = %d, side = %d, isCompleted = %d\n", symbol, symbolIndex, side, symbolStuckMgmt[symbolIndex].stuckMgmt[!side].isCompleted);

  pthread_mutex_lock(&(symbolStuckMgmt[symbolIndex].updateRealValueMutex));

  if (symbolStuckMgmt[symbolIndex].stuckMgmt[!side].isCompleted == NO)  
  {
    while ((symbolStuckMgmt[symbolIndex].stuckMgmt[!side].countProcessed < symbolStuckMgmt[symbolIndex].stuckMgmt[!side].countUpdated) &&
        (stuckInfo->stuckShares > 0))
    {
      stuckIndex = symbolStuckMgmt[symbolIndex].stuckMgmt[!side].countProcessed;
      stuckItem = &symbolStuckMgmt[symbolIndex].stuckMgmt[!side].stuckList[stuckIndex % MAX_STUCK_PER_SYMBOL];

      if (stuckItem->stuckShares >= stuckInfo->stuckShares)
      {
        if (side == ASK_SIDE)
        {
          stuckInfo->realValue += stuckInfo->stuckShares * (stuckInfo->stuckPrice - stuckItem->stuckPrice);
        }
        else
        {
          stuckInfo->realValue += stuckInfo->stuckShares * (stuckItem->stuckPrice - stuckInfo->stuckPrice);
        }

        stuckItem->stuckShares -= stuckInfo->stuckShares;
        stuckInfo->stuckShares = 0;

        // Update real value of current item to database
        ProcessUpdateRealValueToCollection(stuckItem->tsIndex, stuckItem->crossId, dateString, stuckItem->stuckShares, stuckItem->realValue, 0.0);

        break;
      }
      else
      {
        if (side == ASK_SIDE)
        {
          stuckItem->realValue += stuckItem->stuckShares * (stuckInfo->stuckPrice - stuckItem->stuckPrice);
        }
        else
        {
          stuckItem->realValue += stuckItem->stuckShares * (stuckItem->stuckPrice - stuckInfo->stuckPrice);
        }
        
        stuckInfo->stuckShares -= stuckItem->stuckShares;
        stuckItem->stuckShares = 0;

        symbolStuckMgmt[symbolIndex].stuckMgmt[!side].countProcessed += 1;

        // Update real value of current item to database
        ProcessUpdateRealValueToCollection(stuckItem->tsIndex, stuckItem->crossId, 
                    dateString, stuckItem->stuckShares, stuckItem->realValue, 0.0);
      }
    }   
  }
  
  if (stuckInfo->stuckShares != stuckSharesBackup)
  {
    // Update real value of new stuck to database
    ProcessUpdateRealValueToCollection(stuckInfo->tsIndex, stuckInfo->crossId, 
                  dateString, stuckInfo->stuckShares, stuckInfo->realValue, 0.0);
  }
  
  if (stuckInfo->stuckShares > 0)
  {
    if (symbolStuckMgmt[symbolIndex].stuckMgmt[side].countUpdated - symbolStuckMgmt[symbolIndex].stuckMgmt[side].countProcessed >= MAX_STUCK_PER_SYMBOL)
    {
      TraceLog(DEBUG_LEVEL, "Stuck buffer of symbol %.8s is full: countUpdated = %d, countProcessed = %d, maxStuckPerSymbol = %d\n", 
          symbol, symbolStuckMgmt[symbolIndex].stuckMgmt[side].countUpdated, 
          symbolStuckMgmt[symbolIndex].stuckMgmt[side].countProcessed, MAX_STUCK_PER_SYMBOL);
    }
    else
    {
      symbolStuckMgmt[symbolIndex].stuckMgmt[!side].isCompleted = YES;

      stuckIndex = symbolStuckMgmt[symbolIndex].stuckMgmt[side].countUpdated;
      memcpy(&symbolStuckMgmt[symbolIndex].stuckMgmt[side].stuckList[stuckIndex % MAX_STUCK_PER_SYMBOL], stuckInfo, sizeof(t_StuckInfo));
      symbolStuckMgmt[symbolIndex].stuckMgmt[side].countUpdated += 1;
      symbolStuckMgmt[symbolIndex].stuckMgmt[side].isCompleted = NO;
    }
  }

  pthread_mutex_unlock(&(symbolStuckMgmt[symbolIndex].updateRealValueMutex));

  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadAndCalculateRealValue
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int LoadAndCalculateRealValue(char *symbol, int side, t_StuckInfo *stuckInfo, char *dateString)
{
  // Get symbol index
  int symbolIndex = GetStockSymbolIndex(symbol);

  if (symbolIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "%s: Invalid symbol %.8s in the stuck with crossId = %d, tsIndex = %d\n", __func__, symbol, stuckInfo->crossId, stuckInfo->tsIndex);

    return ERROR;
  }

  t_StuckInfo *stuckItem;
  int stuckIndex;
  int stuckSharesBackup = stuckInfo->stuckShares;

  //TraceLog(DEBUG_LEVEL, "symbol = %.8s, symbolIndex = %d, side = %d, isCompleted = %d\n", symbol, symbolIndex, side, symbolStuckMgmt[symbolIndex].stuckMgmt[!side].isCompleted);

  pthread_mutex_lock(&(symbolStuckMgmt[symbolIndex].updateRealValueMutex));

  if (symbolStuckMgmt[symbolIndex].stuckMgmt[!side].isCompleted == NO)  
  {
    while ((symbolStuckMgmt[symbolIndex].stuckMgmt[!side].countProcessed < symbolStuckMgmt[symbolIndex].stuckMgmt[!side].countUpdated) &&
        (stuckInfo->stuckShares > 0))
    {
      stuckIndex = symbolStuckMgmt[symbolIndex].stuckMgmt[!side].countProcessed;
      stuckItem = &symbolStuckMgmt[symbolIndex].stuckMgmt[!side].stuckList[stuckIndex % MAX_STUCK_PER_SYMBOL];

      if (stuckItem->stuckShares >= stuckInfo->stuckShares)
      {
        if (side == ASK_SIDE)
        {
          stuckInfo->realValue += stuckInfo->stuckShares * (stuckInfo->stuckPrice - stuckItem->stuckPrice);
        }
        else
        {
          stuckInfo->realValue += stuckInfo->stuckShares * (stuckItem->stuckPrice - stuckInfo->stuckPrice);
        }

        stuckItem->stuckShares -= stuckInfo->stuckShares;
        stuckInfo->stuckShares = 0;

        // Update real value of current item to database
        ProcessUpdateRealValueToCollection(stuckItem->tsIndex, stuckItem->crossId, 
                    dateString, stuckItem->stuckShares, stuckItem->realValue, stuckItem->totalFee);

        break;
      }
      else
      {
        if (side == ASK_SIDE)
        {
          stuckItem->realValue += stuckItem->stuckShares * (stuckInfo->stuckPrice - stuckItem->stuckPrice);
        }
        else
        {
          stuckItem->realValue += stuckItem->stuckShares * (stuckItem->stuckPrice - stuckInfo->stuckPrice);
        }
        
        stuckInfo->stuckShares -= stuckItem->stuckShares;
        stuckItem->stuckShares = 0;

        symbolStuckMgmt[symbolIndex].stuckMgmt[!side].countProcessed += 1;

        // Update real value of current item to database
        ProcessUpdateRealValueToCollection(stuckItem->tsIndex, stuckItem->crossId, 
                    dateString, stuckItem->stuckShares, stuckItem->realValue, stuckItem->totalFee);
      }
    }   
  }
  
  if (stuckInfo->stuckShares != stuckSharesBackup)
  {
    // Update real value of new stuck to database
    ProcessUpdateRealValueToCollection(stuckInfo->tsIndex, stuckInfo->crossId, 
                  dateString, stuckInfo->stuckShares, stuckInfo->realValue, stuckInfo->totalFee);
  }
  
  if (stuckInfo->stuckShares > 0)
  {
    if (symbolStuckMgmt[symbolIndex].stuckMgmt[side].countUpdated - symbolStuckMgmt[symbolIndex].stuckMgmt[side].countProcessed >= MAX_STUCK_PER_SYMBOL)
    {
      TraceLog(ERROR_LEVEL, "Stuck buffer of symbol %.8s is full: countUpdated = %d, countProcessed = %d, maxStuckPerSymbol = %d\n", 
          symbol, symbolStuckMgmt[symbolIndex].stuckMgmt[side].countUpdated, 
          symbolStuckMgmt[symbolIndex].stuckMgmt[side].countProcessed, MAX_STUCK_PER_SYMBOL);
    }
    else
    {
      symbolStuckMgmt[symbolIndex].stuckMgmt[!side].isCompleted = YES;

      stuckIndex = symbolStuckMgmt[symbolIndex].stuckMgmt[side].countUpdated;
      memcpy(&symbolStuckMgmt[symbolIndex].stuckMgmt[side].stuckList[stuckIndex % MAX_STUCK_PER_SYMBOL], stuckInfo, sizeof(t_StuckInfo));
      symbolStuckMgmt[symbolIndex].stuckMgmt[side].countUpdated += 1;
      symbolStuckMgmt[symbolIndex].stuckMgmt[side].isCompleted = NO;
    }
  }

  pthread_mutex_unlock(&(symbolStuckMgmt[symbolIndex].updateRealValueMutex));

  return SUCCESS;
}

/****************************************************************************
- Function name:  CalculateRealValue
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int CalculateRealValue(char *symbol, int sideType, int shares, double price, int account, double fillFee)
{
  // Get symbol index
  int symbolIndex = GetStockSymbolIndex(symbol);

  if (symbolIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "%s: Invalid symbol %.8s\n", __func__, symbol);

    return ERROR;
  }

  int side = ASK_SIDE;

  if ((sideType == BUY_TO_CLOSE_TYPE) || (sideType == BUY_TO_OPEN_TYPE))
  {
    side = BID_SIDE;
  }

  t_StuckInfo *stuckItem = NULL;
  int stuckIndex, isTSCalculate = NO;
  double currentRealValue;

  pthread_mutex_lock(&(symbolStuckMgmt[symbolIndex].updateRealValueMutex));

  double loss = 0.0;
  
  if (symbolStuckMgmt[symbolIndex].stuckMgmt[!side].isCompleted == NO)  
  {
    while ((symbolStuckMgmt[symbolIndex].stuckMgmt[!side].countProcessed < symbolStuckMgmt[symbolIndex].stuckMgmt[!side].countUpdated) &&
        (shares > 0))
    {
      isTSCalculate = YES;
      loss = 0.0;
      
      stuckIndex = symbolStuckMgmt[symbolIndex].stuckMgmt[!side].countProcessed;
      stuckItem = &symbolStuckMgmt[symbolIndex].stuckMgmt[!side].stuckList[stuckIndex % MAX_STUCK_PER_SYMBOL];

      // DEVLOGF("Calculate TS + AS loss: symbol %s: %d vs %d, %lf vs %lf", symbol, 
          // stuckItem->stuckShares, shares,
          // stuckItem->stuckPrice, price);
      
      if (stuckItem->stuckShares >= shares)
      {
        currentRealValue = stuckItem->realValue;
        
        if (side == ASK_SIDE)
        {
          stuckItem->realValue += shares * (price - stuckItem->stuckPrice);
        }
        else
        {
          stuckItem->realValue += shares * (stuckItem->stuckPrice - price);
        }
        
        stuckItem->totalFee += fillFee;

        if (flt(currentRealValue, 0.00) && flt(stuckItem->realValue, 0.00))
        {
          loss = currentRealValue - stuckItem->realValue;
        }
        else if (flt(currentRealValue, 0.00)) // Loss -> profit
        {
          loss = currentRealValue;
        }
        else if (flt(stuckItem->realValue, 0.00)) // Profit -> loss
        {
          loss = fabs(stuckItem->realValue);
        }
        else //
        {
        }
        
        stuckItem->stuckShares -= shares;
        shares = 0;

        if (stuckItem->stuckShares == 0)
        {
          symbolStuckMgmt[symbolIndex].stuckMgmt[!side].countProcessed += 1;
        }

        // Update real value of current item to database
        ProcessUpdateRealValueToCollection(stuckItem->tsIndex, stuckItem->crossId, 
                    globalDateStringOfDatabase, stuckItem->stuckShares, stuckItem->realValue, stuckItem->totalFee);
      }
      else
      {
        currentRealValue = stuckItem->realValue;
        
        if (side == ASK_SIDE)
        {
          stuckItem->realValue += stuckItem->stuckShares * (price - stuckItem->stuckPrice);
        }
        else
        {
          stuckItem->realValue += stuckItem->stuckShares * (stuckItem->stuckPrice - price);
        }
        
        stuckItem->totalFee += fillFee * stuckItem->stuckShares / shares;
        fillFee = fillFee * (shares - stuckItem->stuckShares) / shares;
        
        shares -= stuckItem->stuckShares;
        stuckItem->stuckShares = 0;

        if (flt(currentRealValue, 0.00) && flt(stuckItem->realValue, 0.00))
        {
          loss = currentRealValue - stuckItem->realValue;
        }
        else if (flt(currentRealValue, 0.00)) // Loss -> profit
        {
          loss = currentRealValue;
        }
        else if (flt(stuckItem->realValue, 0.00)) // Profit -> loss
        {
          loss = fabs(stuckItem->realValue);
        }
        else //
        {
        }

        symbolStuckMgmt[symbolIndex].stuckMgmt[!side].countProcessed += 1;

        // Update real value of current item to database
        ProcessUpdateRealValueToCollection(stuckItem->tsIndex, stuckItem->crossId, 
                    globalDateStringOfDatabase, stuckItem->stuckShares, stuckItem->realValue, stuckItem->totalFee);
      }

      if (!feq(loss, 0.0))
      {
        if (stuckItem->tsIndex > -1 && stuckItem->tsIndex < MAX_TRADE_SERVER_CONNECTIONS)
        {
          ProfitLossPerSymbol[symbolIndex].tsLoss[stuckItem->tsIndex] += loss;
        }
        else
        {
          TraceLog(DEBUG_LEVEL, "(%s) tsIndex=-1, calculate the loss (%lf, %.8s) for AS only\n", __func__, loss, symbol);
        }
        ProfitLossPerSymbol[symbolIndex].asLoss += loss;
        
        // DEVLOGF("Symbol %s: tsLoss[%d] = %lf, asLoss = %lf", symbol, stuckItem->tsIndex, ProfitLossPerSymbol[symbolIndex].tsLoss[stuckItem->tsIndex], ProfitLossPerSymbol[symbolIndex].asLoss);
          
        // Check to add symbol to Volatile or Disabled Symbol List
        CheckToAddSymbolToVolatileOrDisabledList(symbol, symbolIndex, stuckItem->tsIndex);
      }
    }
  }

  if (isTSCalculate == NO)
  {
    // DEVLOGF("Calculate AS loss: symbol %s: %d vs %d, %lf vs %lf", symbol, 
        // RMList.collection[account][symbolIndex].leftShares, shares,
        // RMList.collection[account][symbolIndex].avgPrice, price);
      
    if (side == ASK_SIDE)
    {
      if (RMList.collection[account][symbolIndex].leftShares > 0 || 
        fgt(RMList.collection[account][symbolIndex].avgPrice, price))
      {
        int minShares = (shares > abs(RMList.collection[account][symbolIndex].leftShares)) ? abs(RMList.collection[account][symbolIndex].leftShares): shares;
        loss = minShares * fabs(RMList.collection[account][symbolIndex].avgPrice - price);
      }
    }
    else
    {
      if (RMList.collection[account][symbolIndex].leftShares < 0 ||
        flt(RMList.collection[account][symbolIndex].avgPrice, price))
      {
        int minShares = (shares > abs(RMList.collection[account][symbolIndex].leftShares)) ? abs(RMList.collection[account][symbolIndex].leftShares): shares;
        loss = minShares * fabs(RMList.collection[account][symbolIndex].avgPrice - price);
      }
    }
    
    if (fgt(loss, 0.0))
    {
      ProfitLossPerSymbol[symbolIndex].asLoss += loss;
      
      // DEVLOGF("Symbol %s: asLoss = %lf", symbol, ProfitLossPerSymbol[symbolIndex].asLoss);
      
      // Check to add symbol to Volatile or Disabled Symbol List
      CheckToAddSymbolToVolatileOrDisabledList(symbol, symbolIndex, -1);
    }
  }

  if (symbolStuckMgmt[symbolIndex].stuckMgmt[!side].countProcessed == symbolStuckMgmt[symbolIndex].stuckMgmt[!side].countUpdated)
  {
    symbolStuckMgmt[symbolIndex].stuckMgmt[!side].isCompleted = YES;
  }

  pthread_mutex_unlock(&(symbolStuckMgmt[symbolIndex].updateRealValueMutex));

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessBBOInfo
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessBBOInfo(void)
{
  int tsIndex;

  for( tsIndex = 0; tsIndex < MAX_TRADE_SERVER_CONNECTIONS; tsIndex++ )
  {
    if (BBOInfoMgmt[tsIndex].processedIndex < BBOInfoMgmt[tsIndex].receivedIndex)
    {
      if (ProcessBBOInfoFromCollection(&BBOInfoMgmt[tsIndex]) == ERROR_BUFFER_OVER)
      {
        return ERROR;
      }
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessBBOInfoFromCollection
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessBBOInfoFromCollection(void *bboQuotesMgmt)
{
  t_BBOInfoMgmt *workingMgmt = (t_BBOInfoMgmt *)bboQuotesMgmt;
  
  register int  i, j;
  char  tmp[32];
  int   isInsert = 0;
  memset(tmp, 0, 32);
  t_dbBBOInfo dbBBOInfo;
  
  int tsIndex = workingMgmt->tsIndex;
  int receivedIndex = workingMgmt->receivedIndex;
  int processedIndex = workingMgmt->processedIndex;
  int numBBOQuotes = receivedIndex - processedIndex;

  TraceLog(DEBUG_LEVEL, "BBO Info (%s) processing: %d/%d\n", tradeServersInfo.config[tsIndex].description, processedIndex, receivedIndex);

  // Check collection is full
  if (numBBOQuotes > MAX_BBO_ENTRY)
  {
    TraceLog(ERROR_LEVEL, "BBO Quotes collection is full: tsIndex = %d, receivedIndex = %d, processedIndex = %d\n", tsIndex, receivedIndex, processedIndex);

    // Add code here to process this problem
    return ERROR_BUFFER_OVER;
  }

  /*  Rescan the list to create ISO records for crosses that did not receive all orders and bboinfo from last receives
    This is the case when bboinfo came first but rawdata (orders) came later */
  t_ISOOpenCrossMgmt *workingList;
  for (i = 0; i < MAX_TRADE_SERVER_CONNECTIONS; i++)
  {
    workingList = &ISOOpenCrossMgmt[i];
    for (j = 0; j < workingList->bound; j++)
    {
      if (workingList->crosses[j].isComplete == 0)
      {
        if (workingList->crosses[j].totalOrder > 0)
        {
          //Got full information for creating ISO Flight record!
          AddISOFlightRecordForCrossIndex(i, j);
          workingList->crosses[j].isComplete = 1;
          workingList->crosses[j].waitingCount = 0;
            
          if ((j+1) == workingList->bound)
          {
            workingList->bound--;
          }
        }
        else
        {
          //Found cross but there is no order, this is the case that trade could not sell short
          workingList->crosses[j].waitingCount++;
          
          // Wait for BBO info within 5 seconds
          if (workingList->crosses[j].waitingCount > 50)
          {
            //Remove from collection
            workingList->crosses[j].totalOrder = 0;
            workingList->crosses[j].crossID = -1;
            workingList->crosses[j].bboID = -1;
            workingList->crosses[j].isComplete = 1;
            workingList->crosses[j].waitingCount = 0;
            if ((j+1) == workingList->bound)
            {
              workingList->bound--;
            }
          }
        }
      }
    }
  }
  
  workingList = &ISOOpenCrossMgmt[tsIndex];
  for (i = processedIndex; i < receivedIndex; i++)
  {
    //TraceLog(DEBUG_LEVEL, "processedIndex = %d\n", i);
    
    memset(dbBBOInfo.contents, 0, 512);
    isInsert = 0;

    //Contents should look like:
    //ASK: Z 10 12.22; O 1000 25.55; BID: Z 10 12.22; O 1000 25.55;
    
    //Add ASK part
    strcat(dbBBOInfo.contents, "ASK: ");
    for (j = 0; j < MAX_EXCHANGE; j++)
    {
      if ((workingMgmt->BBOInfoCollection[i % MAX_BBO_ENTRY].BBOSnapShot[j].participantId != 0) &&
      (workingMgmt->BBOInfoCollection[i % MAX_BBO_ENTRY].BBOSnapShot[j].offerSize > 0) &&
      (workingMgmt->BBOInfoCollection[i % MAX_BBO_ENTRY].BBOSnapShot[j].offerPrice > 0.0001))
      {
        sprintf(tmp, "%c %d %.02lf; ", workingMgmt->BBOInfoCollection[i % MAX_BBO_ENTRY].BBOSnapShot[j].participantId,
                    workingMgmt->BBOInfoCollection[i % MAX_BBO_ENTRY].BBOSnapShot[j].offerSize,
                    workingMgmt->BBOInfoCollection[i % MAX_BBO_ENTRY].BBOSnapShot[j].offerPrice);
        strcat(dbBBOInfo.contents, tmp);
        isInsert += 1;
      }
    }
    
    //Add BID part
    strcat(dbBBOInfo.contents, "BID: ");
    for (j = 0; j < MAX_EXCHANGE; j++)
    {
      if ((workingMgmt->BBOInfoCollection[i % MAX_BBO_ENTRY].BBOSnapShot[j].participantId != 0) &&
      (workingMgmt->BBOInfoCollection[i % MAX_BBO_ENTRY].BBOSnapShot[j].bidSize > 0) &&
      (workingMgmt->BBOInfoCollection[i % MAX_BBO_ENTRY].BBOSnapShot[j].bidPrice > 0.0001))
      {
        sprintf(tmp, "%c %d %.02lf; ", workingMgmt->BBOInfoCollection[i % MAX_BBO_ENTRY].BBOSnapShot[j].participantId,
                    workingMgmt->BBOInfoCollection[i % MAX_BBO_ENTRY].BBOSnapShot[j].bidSize,
                    workingMgmt->BBOInfoCollection[i % MAX_BBO_ENTRY].BBOSnapShot[j].bidPrice);
        strcat(dbBBOInfo.contents, tmp);
        isInsert += 1;
      }
    }
        
    if (isInsert > 0)
    {
      //TraceLog(DEBUG_LEVEL, "%d exchanges: %s\n", isInsert, dbBBOInfo.contents);
      
      // Insert data into database
      dbBBOInfo.cross_id = workingMgmt->BBOInfoCollection[i % MAX_BBO_ENTRY].crossID;
      dbBBOInfo.ts_id = tsIndex;
      dbBBOInfo.bbo_id = workingMgmt->BBOInfoCollection[i % MAX_BBO_ENTRY].bboID;
      
      //Add BBO to ISOOpenCrossMgmt, and find freeSlot if needed
      int isFound = NO;
      int freeSlot = -1;
      
      for (j = 0; j < workingList->bound; j++)
      {
        if ((workingList->crosses[j].crossID == dbBBOInfo.cross_id) && 
            (workingList->crosses[j].bboID == dbBBOInfo.bbo_id) &&
            (workingList->crosses[j].isComplete == 1))  //Found
        {
          if (workingList->crosses[j].totalOrder > 0)
          {
            strcpy(workingList->crosses[j].bbo, dbBBOInfo.contents);
          
            //Got full information for creating ISO Flight record!
            //workingList->crosses[j].isComplete = 1;
            workingList->crosses[j].waitingCount = 0;
            AddISOFlightRecordForCrossIndex(tsIndex, j);
          }
          else
          {
            //Found cross but there is no order, this is an error!
            TraceLog(ERROR_LEVEL, "ISO File update error: found a cross but there is no order information in collection!\n");
            workingList->crosses[j].totalOrder = 0;
            workingList->crosses[j].crossID = -1;
            //workingList->crosses[j].isComplete = 1;
            workingList->crosses[j].bboID = -1;
            workingList->crosses[j].waitingCount = 0;
          }
          
          if ((j+1) == workingList->bound)
          {
            workingList->bound--;
          }
          
          isFound = YES;
          break;
        }
        else if (workingList->crosses[j].crossID == -1) //first free slot
        {
          if (freeSlot == -1)
          {
            freeSlot = j;
          }
        }
      }
      
      if (isFound == NO)
      {
        //Could not find existing cross, this means bbo comes before orders (very possible!!!)
        //So we must store this cross to collection, orders will come later, and then we will create ISO record
        if (freeSlot != -1) //Found a free slot, but don't increase <bound>
        {
          //Add bbo to this slot
          strcpy(workingList->crosses[freeSlot].bbo, dbBBOInfo.contents);
          workingList->crosses[freeSlot].crossID = dbBBOInfo.cross_id;
          workingList->crosses[freeSlot].bboID = dbBBOInfo.bbo_id;
          workingList->crosses[freeSlot].waitingCount = 0;
          workingList->crosses[freeSlot].isComplete = 0;
        }
        else  //There is no free slot, increase the bound and check if overflow occurs
        {
          freeSlot = workingList->bound;
          if (freeSlot >= (MAX_ISO_OPEN_CROSS - 1))
          {
            TraceLog(ERROR_LEVEL, "ISO Collection for storing ISO Crosses is full!\n");
          }
          else
          {
            //Add bbo to this slot
            strcpy(workingList->crosses[freeSlot].bbo, dbBBOInfo.contents);
            workingList->crosses[freeSlot].crossID = dbBBOInfo.cross_id;
            workingList->crosses[freeSlot].bboID = dbBBOInfo.bbo_id;
            workingList->crosses[freeSlot].isComplete = 0;
            workingList->crosses[freeSlot].waitingCount = 0;
            
            workingList->bound++;
          }
        }
      }
      
      ProcessInsertBBOInfoToCollection(&dbBBOInfo);
    }
    else
    {
      //TraceLog(DEBUG_LEVEL, "Why content is null %d\n", workingMgmt->BBOInfoCollection[i % MAX_BBO_ENTRY].crossID);
    }
  }
  
  //Update processedIndex
  workingMgmt->processedIndex += numBBOQuotes;
  SavePositionBBOInfoProcessedToFile();
  
  return SUCCESS;
}

/***************************************************************************/
