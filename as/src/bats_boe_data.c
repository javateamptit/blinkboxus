/*****************************************************************************
**  Project:    Equity Arbitrage Application
**  Filename:   bats_boe_data.c
**  Description:  This file contains function definitions that were declared
          in bats_boe_data.h
**  Author:     Luan Vo-Kinh
**  First created:  13-Sep-2007
**  Last updated: -----------
*****************************************************************************/

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include <stdlib.h>

#include "configuration.h"
#include "order_mgmt_proc.h"
#include "bats_boe_data.h"
#include "database_util.h"
#include "query_data_mgmt.h"
#include "raw_data_mgmt.h"
#include "daedalus_proc.h"
#include "hbitime.h"

/*define message types*/
// Member to BATS
#define BATS_BOE_NEW_ORDER 0x04
#define BATS_BOE_ORDER_CANCEL_REQUEST 0x05

//BATS to Member
#define BATS_BOE_ORDER_ACK 0x0A
#define BATS_BOE_ORDER_REJECTED 0x0B
#define BATS_BOE_ORDER_CANCELLED 0x0F
#define BATS_BOE_ORDER_EXECUTION 0x11
#define BATS_BOE_CANCEL_REJECTED 0x10
#define BATS_BOE_TRADE_CANCEL_OR_CORRECT 0x12
#define BATS_BOE_RESTATED_MESSAGE_TYPE 0x0D

/****************************************************************************
** Global variables definition
****************************************************************************/

/****************************************************************************
** Function declarations
****************************************************************************/

/****************************************************************************
- Function name:  ParseRawDataForBATS_BOEOrder
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ParseRawDataForBATS_BOEOrder(t_DataBlock dataBlock, int type, int oecType)
{
  //Get timestamp from epoch with format <second from epoch>.<microsecond> (eg. 124212553634.3235235)
  char timeStamp[32];
  long usec;

  char msgType;
  msgType = dataBlock.msgContent[4];

  switch (msgType)
  {
    // New order
    case BATS_BOE_NEW_ORDER:
      usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]) / 1000;
      sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
      ProcessNewOrderOfBATS_BOEOrder(dataBlock, type, timeStamp, oecType);
      break;

    // Accepted order
    case BATS_BOE_ORDER_ACK:
      usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]) / 1000;
      sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
      ProcessAcceptedOrderOfBATS_BOEOrder(dataBlock, type, timeStamp, oecType);
      break;

    // Cancel request order
    case BATS_BOE_ORDER_CANCEL_REQUEST:
      usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]) / 1000;
      sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
      ProcessCancelRequestOfBATS_BOEOrder(dataBlock, type, timeStamp, oecType);
      break;

    // Execution order
    case BATS_BOE_ORDER_EXECUTION:
      usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]) / 1000;
      sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
      ProcessExecutedOrderOfBATS_BOEOrder(dataBlock, type, timeStamp, oecType);
      break;

    // Canceled order
    case BATS_BOE_ORDER_CANCELLED:
      usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]) / 1000;
      sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
      ProcessCanceledOrderOfBATS_BOEOrder(dataBlock, type, timeStamp, oecType);
      break;

    // Rejected order
    case BATS_BOE_ORDER_REJECTED:
      usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]) / 1000;
      sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
      ProcessRejectedOrderOfBATS_BOEOrder(dataBlock, type, timeStamp, oecType);
      break;

    // Cancel rejected order
    case BATS_BOE_CANCEL_REJECTED:
      usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]) / 1000;
      sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
      ProcessRejectedCancelOfBATS_BOEOrder(dataBlock, type, timeStamp, oecType);
      break;

    // Broken trade
    case BATS_BOE_TRADE_CANCEL_OR_CORRECT:
      usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]) / 1000;
      sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
      ProcessBrokenOrderOfBATS_BOEOrder(dataBlock, type, timeStamp, oecType);
      break;
    
    // Order restated
    case BATS_BOE_RESTATED_MESSAGE_TYPE:
      usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]) / 1000;
      sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
      ProcessOrderRestatedOfBATS_BOEOrder(dataBlock, type, timeStamp, oecType);
      break;

    default:
      TraceLog(ERROR_LEVEL, "%s rawdata: Invalid message type: %X\n", GetOECName(oecType), msgType);
      break;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessNewOrderOfBATS_BOEOrder
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessNewOrderOfBATS_BOEOrder(t_DataBlock dataBlock, int type, char *timeStamp, int oecType)
{
  t_BATS_BOENewOrder newOrder;
  t_OpenOrderInfo openOrder;
  t_OrderDetail orderDetail;

  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;

  // YYYY/mm/DD
  char dateTime[32];
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]), &dateTime[0]);
  memcpy(newOrder.date, &dateTime[0], 10);
  newOrder.date[10] = 0;

  // HH:MM:SS.mss AM/PM
  memcpy(newOrder.timestamp, &dateTime[11], TIME_LENGTH);
  newOrder.timestamp[TIME_LENGTH] = 0;

  // openOrder.timestamp
  memcpy(openOrder.timestamp, &dateTime[0], TIMESTAMP_LENGTH);
  openOrder.timestamp[TIMESTAMP_LENGTH] = 0;

  // orderDetail
  strcpy(orderDetail.timestamp, openOrder.timestamp);

  strcpy( newOrder.ecn, GetOECName(oecType) );

  // openOrder.ECNId
  openOrder.ECNId = oecType;

  // Status: Sent, Live, Rejected, Closed, BrokenTrade, CanceledByECN,
  //      CXLSent, CancelACK, CancelRejected, CancelPending, CanceledByUser
  strcpy( newOrder.status, orderStatus.Sent );

  // openOrder.status
  openOrder.status = orderStatusIndex.Sent;

  // Message type
  strcpy( newOrder.msgType, "NewOrder" );

  // msgLength
  int msgLength = GetShortNumber(&dataBlock.msgContent[2]);

  // msgType
  char msgType = BATS_BOE_NEW_ORDER;

  // matching unit
  int fieldMatchUnit = dataBlock.msgContent[5];

  // Sequence number
  newOrder.seqNum = GetIntNumber(&dataBlock.msgContent[6]);

  // Order id
  char fieldClOrderID[21];
  strncpy(fieldClOrderID, (char *)&dataBlock.msgContent[10], 20);
  fieldClOrderID[20] = 0;
  newOrder.orderID = atoi(fieldClOrderID);

  // openOrder.clOrdId
  openOrder.clOrdId = newOrder.orderID;

  // "B", "S" or "SS"
  char action = dataBlock.msgContent[30];
  char buysell = '0';
  switch ( action )
  {
    case '1':
      buysell = 'B';
      strcpy( newOrder.side, "BC" );
      openOrder.side = BUY_TO_CLOSE_TYPE;
      break;

    case '2':
      buysell = 'S';
      strcpy( newOrder.side, "S" );
      openOrder.side = SELL_TYPE;
      break;

    case '5':
      buysell = 'T';
      strcpy( newOrder.side, "SS" );
      openOrder.side = SHORT_SELL_TYPE;
      break;

    default:
      strcpy( newOrder.side, " " );
      openOrder.side = -1;

      TraceLog(ERROR_LEVEL, "%s: Not support side = %c, %d\n", GetOECName(oecType), action, action );
      break;
  }

  // Shares
  newOrder.shares = GetIntNumber(&dataBlock.msgContent[31]);

  // openOrder.shares
  openOrder.shares = newOrder.shares;
  openOrder.leftShares = openOrder.shares;

  /*----------------------------------------
   * Process optional fields
   * Optional fields start from offset 41...
   *--------------------------------------*/
  int offset = STARTING_OFFSET_OF_NEW_ORDER;
  union t_BitField bitField;

  /*------------------------
   *  New order bitField 1
   *-----------------------*/
  bitField.value = dataBlock.msgContent[35];

  // clearing firm
  if (bitField.bit.bit1 == 1)
  {
    strncpy(newOrder.clrFirm, (char *)&dataBlock.msgContent[offset], 4);
    newOrder.clrFirm[4] = 0;
    offset += CLEARING_FIRM_LEN;
  }
  else
  {
    strcpy(newOrder.clrFirm, " ");
  }

  // clearing account
  if (bitField.bit.bit2 == 1) offset += CLEARING_ACC_LEN;

  // price
  if (bitField.bit.bit3 == 1)
  {
    newOrder.price = GetLongNumber((char *)&dataBlock.msgContent[offset]) * 1.00 / 10000;
    offset += PRICE_LEN;
  }
  else
  {
    newOrder.price = 0.00;
  }

  // openOrder.price
  openOrder.price = newOrder.price;
  openOrder.avgPrice = openOrder.price;

  // Exec instruction
  if (bitField.bit.bit4 == 1)
  {
    newOrder.execInst = dataBlock.msgContent[offset];
    offset += EXEC_INST_LEN;
  }
  else
  {
    newOrder.execInst = ' ';
  }

  // order type
  if (bitField.bit.bit5 == 1)
  {
    newOrder.orderType = dataBlock.msgContent[offset];
    offset += ORDER_TYPE_LEN;
  }
  else
  {
    newOrder.orderType = ' ';
  }

  // time in force
  char tif = ' ';
  char _tif[6];
  if (bitField.bit.bit6 == 1)
  {
    tif = dataBlock.msgContent[offset];
    switch (tif)
    {
      case '0':
        strcpy( newOrder.timeInForce, "DAY" );
        openOrder.tif = DAY_TYPE;
        strcpy(_tif, "99999");
        break;

      case '3':
        strcpy( newOrder.timeInForce, "IOC" );
        strcpy(_tif, "0");
        openOrder.tif = IOC_TYPE;
        break;

      default:
        strcpy( newOrder.timeInForce, " " );
        openOrder.tif = -1;
        TraceLog(ERROR_LEVEL, "%s: Not support tif = %c, %d\n", GetOECName(oecType), tif, tif );
        break;
    }
    offset += TIME_IN_FORCE_LEN;
  }
  else
  {
    strcpy( newOrder.timeInForce, " " );
  }

  // min quantity
  int minQty = 0;
  if (bitField.bit.bit7 == 1)
  {
    minQty = GetIntNumber(&dataBlock.msgContent[offset]);
    offset += MIN_QTY_LEN;
  }

  // max floor
  if (bitField.bit.bit8 == 1)
  {
    newOrder.maxFloor = GetIntNumber(&dataBlock.msgContent[offset]);
    offset += MAX_FLOOR_LEN;
  }
  else
  {
    newOrder.maxFloor = 0;
  }

  /*------------------------
   *  New order bitField 2
   *-----------------------*/
  bitField.value = dataBlock.msgContent[36];

  // Stock symbol
  if (bitField.bit.bit1 == 1)
  {
    strncpy( newOrder.symbol, ( char * )&dataBlock.msgContent[offset], SYMBOL_LEN );
    offset += SYMBOL_LEN;
  }
  else
  {
    strcpy(newOrder.symbol, " ");
  }

  // openOrder.symbol
  strncpy( openOrder.symbol, (char *)newOrder.symbol, SYMBOL_LEN);

  // Symbol Sfx
  char symbolSfx[SYMBOL_SFX_LEN];
  if (bitField.bit.bit2 == 1)
  {
    strncpy( symbolSfx, (char *)&dataBlock.msgContent[offset], SYMBOL_SFX_LEN);
    offset += SYMBOL_SFX_LEN;
  }
  else
  {
    strcpy(symbolSfx, " ");
  }

  // Capacity (rule80A)
  if (bitField.bit.bit7 == 1)
  {
    newOrder.rule80A = dataBlock.msgContent[offset];
    offset += CAPACITY_LEN;
  }
  else
  {
    newOrder.rule80A = ' ';
  }

  // Routing instruction
  memset(newOrder.routingInst, 0, 4);
  if (bitField.bit.bit8 == 1)
  {
    strncpy(newOrder.routingInst, (char *)&dataBlock.msgContent[offset], 4);
    offset += ROUT_INST_LEN;
  }
  else
  {
    newOrder.routingInst[0] = ' ';
  }

  /*------------------------
   *  New order bitField 3
   *-----------------------*/
  bitField.value = dataBlock.msgContent[37];

  // account
  if (bitField.bit.bit1 == 1)
  {
    strncpy(newOrder.account, (char *)&dataBlock.msgContent[offset], 4);
    newOrder.account[4] = 0;
    offset += ACCOUNT_LEN;
  }
  else
  {
    strcpy(newOrder.account, " ");
  }

  // display indicator
  char fieldDisplayIndicator = ' ';
  char _visibility =  'Y';
  if (bitField.bit.bit2 == 1)
  {
    fieldDisplayIndicator = dataBlock.msgContent[offset];
    offset += DISPLAY_INDICATOR_LEN;
  }
  if ((fieldDisplayIndicator == ' ') || (fieldDisplayIndicator == 'I'))
  {
    _visibility = 'N';
  }

  // Max remove Pct
  int fieldMaxRemovePct = 0;
  if (bitField.bit.bit3 == 1)
  {
    fieldMaxRemovePct = dataBlock.msgContent[offset];
    offset += MAX_REMOVE_PCT_LEN;
  }

  // Discretion amount
  short fieldDisAmount = 0;
  if (bitField.bit.bit4 == 1)
  {
    fieldDisAmount = GetShortNumber(&dataBlock.msgContent[offset]);
    offset += DISCRETION_AMOUNT_LEN;
  }

  // Peg difference
  if (bitField.bit.bit5 == 1)
  {
    newOrder.pegDifference = GetLongNumber((char *)&dataBlock.msgContent[offset]) * 1.00 / 10000;
    offset += PEG_DIFF_LEN;
  }
  else
  {
    newOrder.pegDifference = 0.00;
  }

  // Prevent member match
  char fieldPreventMemberMatch[4];
  if (bitField.bit.bit6 == 1)
  {
    strncpy(fieldPreventMemberMatch, (char *)&dataBlock.msgContent[offset], 3);
    fieldPreventMemberMatch[3] = 0;
    offset += PREVENT_MEMBER_MATCH_LEN;
  }
  else
  {
    strcpy(fieldPreventMemberMatch, " ");
  }

  // Locate reqd
  char fieldLocateReqd = ' ';
  if (bitField.bit.bit7 == 1)
  {
    fieldLocateReqd = dataBlock.msgContent[offset];
    offset += LOCATE_REQD_LEN;
  }

  // Expire time
  // Note: required for TIF = 6 orders
  if (bitField.bit.bit8 == 1)
  {
    offset += EXPIRE_TIME_LEN;
  }

  /*------------------------
   *  New order bitField 4: use in the future
   *-----------------------*/

  /*------------------------
   *  New order bitField 5
   *-----------------------*/
  bitField.value = dataBlock.msgContent[39];

  // Attribute quote
  char fieldAttributeQuote = ' ';
  if (bitField.bit.bit2 == 1)
  {
    fieldAttributeQuote = dataBlock.msgContent[offset];
    offset += ATTRIBUTE_QUOTE_LEN;
  }

  int isISO = 0;
  short bboID = -1;
  unsigned char bbReason;
  newOrder.isoFlag = 'N';

  // Get the 1st character of value in RoutingInstruction field to know whether order is ISO or NON-ISO
  if (newOrder.routingInst[0] == 'B' && newOrder.execInst == 'f')
  {
    newOrder.isoFlag = 'Y';
    isISO = 1;
    bboID = GetShortNumber(&dataBlock.addContent[DATABLOCK_OFFSET_BBO_ID]);
  }

  newOrder.bboId = bboID;

  //orderDetail.msgContent
  orderDetail.length = sprintf( orderDetail.msgContent,
      "%d=%X\001"   // msgtype
      "%d=%d\001"   // length
      "%d=%d\001"   // matching unit
      "%d=%d\001"   // seqNum
      "%d=%d\001"   // orderid
      "%d=%c\001"   // action
      "%d=%d\001"   // shares
      "%d=%s\001"   // clearing firm
      "%d=%lf\001"  // price
      "%d=%c\001"   // exec inst
      "%d=%c\001"   // order type
      "%d=%c\001"   // tif
      "%d=%d\001"   // min quantity
      "%d=%d\001"   // max floor
      "%d=%.8s\001" // symbol
      "%d=%.8s\001" // symbolSfx
      "%d=%c\001"   // capacity
      "%d=%.4s\001" // routing instruction
      "%d=%s\001"   // account
      "%d=%c\001"   // display indicator
      "%d=%d\001"   // max remove pct
      "%d=%d\001"   // discretion amount
      "%d=%lf\001"  // peg diff
      "%d=%s\001"   // prevent member match
      "%d=%c\001"   // locate reqd
      "%d=%c\001"   // attribute quote
      ,fieldIndexList.MsgType, msgType,
      fieldIndexList.Length, msgLength,
      fieldIndexList.MatchUnit, fieldMatchUnit,
      fieldIndexList.InSeqNum, newOrder.seqNum,
      fieldIndexList.ClOrdID, newOrder.orderID,
      fieldIndexList.Side, action,
      fieldIndexList.Shares, newOrder.shares,
      fieldIndexList.Firm, newOrder.clrFirm,
      fieldIndexList.Price, newOrder.price,
      fieldIndexList.ExecInst, newOrder.execInst,
      fieldIndexList.Type, newOrder.orderType,
      fieldIndexList.TimeInForce, tif,
      fieldIndexList.MinQty, minQty,
      fieldIndexList.MaxFloor, newOrder.maxFloor,
      fieldIndexList.Symbol, newOrder.symbol,
      fieldIndexList.SymbolSfx, symbolSfx,
      fieldIndexList.Rule80A, newOrder.rule80A,
      fieldIndexList.RoutInst, newOrder.routingInst,
      fieldIndexList.Account, newOrder.account,
      fieldIndexList.Display, fieldDisplayIndicator,
      fieldIndexList.MaxRemovePct, fieldMaxRemovePct,
      fieldIndexList.DiscretionPrice, fieldDisAmount,
      fieldIndexList.PegDifference, newOrder.pegDifference,
      fieldIndexList.PreventMemberMatch, fieldPreventMemberMatch,
      fieldIndexList.LocateReqd, fieldLocateReqd,
      fieldIndexList.AttributeQuote, fieldAttributeQuote
      );

  strcpy( newOrder.msgContent, orderDetail.msgContent );

  // Update database
  if( type == MANUAL_ORDER )
  {
    // openOrder.tsId
    openOrder.tsId = -1;

    //newOrder.tsId
    newOrder.tsId = -1;

    // Trade type
    newOrder.tradeType = NORMALLY_TRADE;
    openOrder.tradeType = NORMALLY_TRADE;

    strcpy(newOrder.entryExit, "Exit");
    strcpy(newOrder.ecnLaunch, " ");
    strcpy(newOrder.askBid, " ");

    //Add CrossID if it belogs to PM
    if (newOrder.orderID >= 500000)
    {
      bbReason = OJ_BB_REASON_PM;
      newOrder.crossId = GetIntNumber(&dataBlock.addContent[DATABLOCK_OFFSET_CROSS_ID]);
      openOrder.crossId = newOrder.crossId;
    }
    else
    {
      bbReason = OJ_BB_REASON_AS;
      newOrder.crossId = 0;
    }
  }
  else
  {
    if (dataBlock.addContent[DATABLOCK_OFFSET_LAUNCH] == EXIT_VALUE)
    {
      // Get Entry/Exit
      bbReason = OJ_BB_REASON_EXIT;
      strcpy(newOrder.entryExit, "Exit");
      strcpy(newOrder.ecnLaunch, " ");
      strcpy(newOrder.askBid, " ");
      
      newOrder.entryExitRefNum = *(unsigned short *)&dataBlock.addContent[DATABLOCK_OFFSET_ENTRY_EXIT_REF];
    }
    else
    {
      // Get Entry/Exit
      bbReason = OJ_BB_REASON_ENTRY;
      strcpy(newOrder.entryExit, "Entry");

      // Get ECN launch and Ask/Bid launch
      GetECNLaunchInfo(dataBlock.addContent[DATABLOCK_OFFSET_LAUNCH], newOrder.ecnLaunch, newOrder.askBid);
    }

    // Visible shares
    newOrder.visibleShares = GetIntNumber(&dataBlock.addContent[DATABLOCK_OFFSET_VISIBLE_SIZE]);

    //Cross Id
    newOrder.crossId = GetIntNumber(&dataBlock.addContent[DATABLOCK_OFFSET_CROSS_ID]);
    openOrder.crossId = newOrder.crossId;

    // Trade type
    newOrder.tradeType = NORMALLY_TRADE;
    openOrder.tradeType = newOrder.tradeType;

    // openOrder.tsId
    openOrder.tsId = tradeServersInfo.config[type].tsId;

    //newOrder.tsId
    newOrder.tsId = type;
    
    // Reset no trade duration
    time_t t = (time_t)hbitime_seconds();
    struct tm tm = *localtime(&t);

    NoTradesDurationMgt[type].currentNoTradesDurationInSecond = 0;
    NoTradesDurationMgt[type].alertThresholdInMinute = TradeTimeAlertConfig[3600 * tm.tm_hour + 60 * tm.tm_min + tm.tm_sec];

    sprintf(NoTradesDurationMgt[type].startTimeStamp, "%d:%d", tm.tm_hour, tm.tm_min);
  }

  openOrder.account = dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID];

  // Call function to add a open order
  ProcessAddASOpenOrder( &openOrder, &orderDetail );

  newOrder.tradingAccount = dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID];

  // Call function to update New Order of BATS BOE Order in Manual Order
  ProcessInsertNewOrder4BATS_BOEQueryToCollection(&newOrder);
  
  int isDuplicated = 0;
  if (AddOrderIdToList(&__new, newOrder.orderID) == ERROR)
  {
    isDuplicated = 1;
  }

  char account[32];
  GetAccountName(oecType, dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID], account);

  int _venueType = oecType - TYPE_BASE;
  if (isDuplicated == 0)
  {
    AddOJNewOrderRecord(timeStamp, newOrder.symbol, _venueType, buysell, newOrder.price, newOrder.shares, newOrder.orderID, _tif, _visibility, isISO, account, NULL, newOrder.rule80A, newOrder.crossId, bbReason);
  }
  
  //Add ISO Order to ISO Collection for ISO Flight File creation
  if ((isISO == 1) && (isDuplicated == 0))
  {
    t_ISOOrderInfo isoOrderInfo;
    strcpy(isoOrderInfo.strTime, timeStamp);

    sprintf(isoOrderInfo.strVenueID, "%d", _venueType);

    isoOrderInfo.strBuysell[0] = buysell;
    isoOrderInfo.strBuysell[1] = 0;

    sprintf(isoOrderInfo.strPrice, "%.2lf", newOrder.price);

    sprintf(isoOrderInfo.strShares, "%d", newOrder.shares);

    sprintf(isoOrderInfo.strSequenceID, "%d", newOrder.orderID);

    isoOrderInfo.strCapacity[0] = newOrder.rule80A;
    isoOrderInfo.strCapacity[1] = 0;

    strcpy(isoOrderInfo.strAccount, account);

    isoOrderInfo.strRoutingInfo[0] = 0; //No Routing for BATS BOE

    AddISOOrderToCollection(newOrder.crossId, bboID, newOrder.symbol, &isoOrderInfo);
  }

  CheckAndProcessRawdataInWaitListForOrderID(newOrder.orderID);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessAcceptedOrderOfBATS_BOEOrder
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessAcceptedOrderOfBATS_BOEOrder(t_DataBlock dataBlock, int type, char *timeStamp, int oecType)
{
  t_BATS_BOEAcceptedOrder acceptedOrder;
  t_OpenOrderInfo openOrder;
  t_OrderDetail orderDetail;

  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;

  openOrder.price = 0.00;
  openOrder.avgPrice = 0.00;

  // YYYY/mm/DD
  char dateTime[32];
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]), &dateTime[0]);
  memcpy(acceptedOrder.processedTimestamp, &dateTime[11], TIME_LENGTH);
  acceptedOrder.processedTimestamp[TIME_LENGTH] = 0;
  
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]), &dateTime[0]);
  memcpy(acceptedOrder.date, &dateTime[0], 10);
  acceptedOrder.date[10] = 0;

  // HH:MM:SS.mss AM/PM
  memcpy(acceptedOrder.timestamp, &dateTime[11], TIME_LENGTH);
  acceptedOrder.timestamp[TIME_LENGTH] = 0;

  // openOrder.timestamp
  memcpy(openOrder.timestamp, &dateTime[0], TIMESTAMP_LENGTH);
  openOrder.timestamp[TIMESTAMP_LENGTH] = 0;

  // orderDetail
  strcpy(orderDetail.timestamp, openOrder.timestamp);

  // openOrder.ECNId
  openOrder.ECNId = oecType;

  // Status: Sent, Live, Rejected, Closed, BrokenTrade, CanceledByECN,
  //      CXLSent, CancelACK, CancelRejected, CancelPending, CanceledByUser
  strcpy( acceptedOrder.status, orderStatus.Live );

  // openOrder.status
  openOrder.status = orderStatusIndex.Live;

  // Messsage type
  strcpy(acceptedOrder.msgType, "AcceptedOrder");

  // Length
  int msgLength = GetShortNumber(&dataBlock.msgContent[2]);

  // msgType
  char msgType = BATS_BOE_ORDER_ACK;

  // matching unit
  int fieldMatchUnit = dataBlock.msgContent[5];

  // Sequence number
  acceptedOrder.seqNum = GetIntNumber(&dataBlock.msgContent[6]);

  // Transaction time
  unsigned long totalnanos = GetLongNumber((char *)&dataBlock.msgContent[10]);

  // Get the number of seconds from Jan 1st 1970
  int numSecs = totalnanos / 1000000000;

  numSecs %= NUMBER_OF_SECONDS_PER_DAY;

  // Get the number of nanoseconds
  int numNanos = totalnanos % 1000000000;

  // write to transtime
  sprintf(acceptedOrder.transTime, "%02d:%02d:%02d.%09d", numSecs / 3600, (numSecs % 3600) / 60, (numSecs % 3600) % 60, numNanos);

  // client Order id
  char fieldClOrderID[21];
  strncpy(fieldClOrderID, (char *)&dataBlock.msgContent[18], 20);
  fieldClOrderID[20] = 0;
  acceptedOrder.clOrderID = atoi(fieldClOrderID);

  char exOrderId[32];
  sprintf(exOrderId, "%d", acceptedOrder.clOrderID);
  
  // openOrder.clOrdId
  openOrder.clOrdId = acceptedOrder.clOrderID;

  // BATS orderid
  acceptedOrder.batsExOrderId = GetLongNumber((char *)&dataBlock.msgContent[38]);
  
  int _serverId = (type == MANUAL_ORDER) ? MAX_TRADE_SERVER_CONNECTIONS : type;
  ExchangeOrderIdMapping[_serverId][acceptedOrder.clOrderID % MAX_ORDER_PLACEMENT] = acceptedOrder.batsExOrderId;

  /*----------------------------------------
   * Process optional fields
   * Optional fields start from offset 54...
   *--------------------------------------*/

  int offset = STARTING_OFFSET_OF_ACK_ORDER;
  union t_BitField bitField;

  /*------------------------
   *  New order bitField 1
   *-----------------------*/
  bitField.value = dataBlock.msgContent[46];

  // Side
  char fieldSide = ' ';
  if (bitField.bit.bit1 == 1)
  {
    // "B", "S" or "SS"
    fieldSide = dataBlock.msgContent[offset];
    switch ( fieldSide )
    {
      case '1':
        strcpy( acceptedOrder.side, "BC" );
        openOrder.side = BUY_TO_CLOSE_TYPE;
        break;

      case '2':
        strcpy( acceptedOrder.side, "S" );
        openOrder.side = SELL_TYPE;
        break;

      case '5':
        strcpy( acceptedOrder.side, "SS" );
        openOrder.side = SHORT_SELL_TYPE;
        break;

      default:
        strcpy( acceptedOrder.side, " " );
        openOrder.side = -1;

        TraceLog(ERROR_LEVEL, "%s: not support side = %c, %d\n", GetOECName(oecType), fieldSide, fieldSide );
        break;
    }
    offset += SIDE_LEN;
  }

  // peg difference
  double fieldPegDiff = 0.00;
  if (bitField.bit.bit2 == 1)
  {
    fieldPegDiff = GetLongNumber((char *)&dataBlock.msgContent[offset]) * 1.00 / 10000;
    offset += PEG_DIFF_LEN;
  }

  // Price
  if (bitField.bit.bit3 == 1)
  {
    acceptedOrder.price = GetLongNumber((char *)&dataBlock.msgContent[offset]) * 1.00 / 10000;
    offset += PRICE_LEN;
  }
  else
  {
    acceptedOrder.price = 0.00;
  }

  // Exec instruction
  char fieldExecInst = ' ';
  if (bitField.bit.bit4 == 1)
  {
    fieldExecInst = dataBlock.msgContent[offset];
    offset += EXEC_INST_LEN;
  }

  // Order type
  char fieldOrdType = ' ';
  if (bitField.bit.bit5 == 1)
  {
    fieldOrdType = dataBlock.msgContent[offset];
    offset += ORDER_TYPE_LEN;
  }

  //Time In Force
  char fieldTIF = ' ';
  if (bitField.bit.bit6 == 1)
  {
    fieldTIF = dataBlock.msgContent[offset];
    if( fieldTIF == '3' )
    {
      openOrder.tif = IOC_TYPE;
    }
    else
    {
      openOrder.tif = DAY_TYPE;
    }
    offset += TIME_IN_FORCE_LEN;
  }

  // Min quantity
  int fieldMinQty = 0;
  if (bitField.bit.bit7 == 1)
  {
    fieldMinQty = GetIntNumber(&dataBlock.msgContent[offset]);
    offset += MIN_QTY_LEN;
  }

  // Max remove pct
  int fieldMaxRemovePct = 0;
  if (bitField.bit.bit8 == 1)
  {
    fieldMaxRemovePct = dataBlock.msgContent[offset];
    offset += MAX_REMOVE_PCT_LEN;
  }

  /*------------------------
   *  New order bitField 2
   *-----------------------*/
  bitField.value = dataBlock.msgContent[47];

  // Symbol
  if (bitField.bit.bit1 == 1)
  {
    strncpy(acceptedOrder.symbol, (char *)&dataBlock.msgContent[offset], SYMBOL_LEN);
    offset += SYMBOL_LEN;
  }
  else
  {
    strcpy(acceptedOrder.symbol, " ");
  }

  // Symbol Sfx
  char fieldSymbolSfx[SYMBOL_SFX_LEN];
  if (bitField.bit.bit2 == 1)
  {
    strncpy(fieldSymbolSfx, (char *)&dataBlock.msgContent[offset], SYMBOL_SFX_LEN);
    offset += SYMBOL_SFX_LEN;
    strcat(acceptedOrder.symbol, fieldSymbolSfx);
  }
  else
  {
    strcpy(fieldSymbolSfx, " ");
  }

  // openOrder.symbol
  memcpy( openOrder.symbol, acceptedOrder.symbol, SYMBOL_LEN );

  // Capacity
  char fieldCapacity = ' ';
  if (bitField.bit.bit7 == 1)
  {
    fieldCapacity = dataBlock.msgContent[offset];
    offset += CAPACITY_LEN;
  }

  /*------------------------
   *  New order bitField 3
   *-----------------------*/
  bitField.value = dataBlock.msgContent[48];

  // Account
  char fieldAccount[5];
  if (bitField.bit.bit1 == 1)
  {
    strncpy(fieldAccount, (char *)&dataBlock.msgContent[offset], 4);
    fieldAccount[4] = 0;
    offset += ACCOUNT_LEN;
  }
  else
  {
    strcpy(fieldAccount, " ");
  }

  // clearing firm
  if (bitField.bit.bit2 == 1)
  {
    strncpy(acceptedOrder.clrFirm, (char *)&dataBlock.msgContent[offset], 4);
    acceptedOrder.clrFirm[4] = 0;
    offset += CLEARING_FIRM_LEN;
  }
  else
  {
    strcpy(acceptedOrder.clrFirm, " ");
  }

  // clearing account
  if (bitField.bit.bit3 == 1) offset += CLEARING_ACC_LEN;

  // Display indicator
  char fieldDisplayIndicator = ' ';
  if (bitField.bit.bit4 == 1)
  {
    fieldDisplayIndicator = dataBlock.msgContent[offset];
    offset += DISPLAY_INDICATOR_LEN;
  }

  // Max floor
  int fieldMaxFloor = 0;
  if (bitField.bit.bit5 == 1)
  {
    fieldMaxFloor = GetIntNumber(&dataBlock.msgContent[offset]);
    offset += MAX_FLOOR_LEN;
  }

  // Discretion amount
  short fieldDisAmount = 0;
  if (bitField.bit.bit6 == 1)
  {
    fieldDisAmount = GetShortNumber(&dataBlock.msgContent[offset]);
    offset += DISCRETION_AMOUNT_LEN;
  }

  // Order quantity
  if (bitField.bit.bit7 == 1)
  {
    acceptedOrder.shares = GetIntNumber(&dataBlock.msgContent[offset]);
    offset += ORDER_QTY_LEN;
  }
  else
  {
    acceptedOrder.shares = 0;
  }

  // openOrder.shares
  openOrder.shares = acceptedOrder.shares;
  openOrder.leftShares = openOrder.shares;

  // Prevent member match
  char fieldPreventMemberMatch[4];
  if (bitField.bit.bit8 == 1)
  {
    strncpy(fieldPreventMemberMatch, (char *)&dataBlock.msgContent[offset], 3);
    fieldPreventMemberMatch[3] = 0;
    offset += PREVENT_MEMBER_MATCH_LEN;
  }
  else
  {
    strcpy(fieldPreventMemberMatch, " ");
  }

  /*------------------------
   *  New order bitField 4: use in the future
   *-----------------------*/

  /*------------------------
   *  New order bitField 5
   *-----------------------*/
  bitField.value = dataBlock.msgContent[50];

  // Original client order id
  char fieldOrgClOrderID[21];
  if (bitField.bit.bit1 == 1)
  {
    strncpy(fieldOrgClOrderID, (char *)&dataBlock.msgContent[offset], 20);
    fieldOrgClOrderID[20] = 0;
    acceptedOrder.orgClOrderID = atoi(fieldOrgClOrderID);
    offset += ORIG_CLORDERID_LEN;
  }
  else
  {
    strcpy(fieldOrgClOrderID, " ");
  }

  // leaves quantity
  int fieldLeavesQty = 0;
  if (bitField.bit.bit2 == 1)
  {
    fieldLeavesQty = GetIntNumber(&dataBlock.msgContent[offset]);
    offset += LEAVES_QTY_LEN;
  }
  openOrder.leftShares = fieldLeavesQty;

  // Last shares
  int fieldLastShares = 0;
  if (bitField.bit.bit3 == 1)
  {
    fieldLastShares = GetIntNumber(&dataBlock.msgContent[offset]);
    offset += LASTSHARE_LEN;
  }
  openOrder.shares = fieldLeavesQty;

  // Last Px
  double fieldLastPx = 0.00;
  if (bitField.bit.bit4 == 1)
  {
    fieldLastPx = GetLongNumber((char *)&dataBlock.msgContent[offset]) * 1.00 / 10000;
    offset += LASTPX_LEN;
  }

  // Display price
  double fieldDisplayPrice = 0.00;
  if (bitField.bit.bit5 == 1)
  {
    fieldDisplayPrice = GetLongNumber((char *)&dataBlock.msgContent[offset]) * 1.00 / 10000;
    offset += DISPLAY_PRICE_LEN;
  }

  // Working price
  double fieldWorkingPrice = 0.00;
  if (bitField.bit.bit6 == 1)
  {
    fieldWorkingPrice = GetLongNumber((char *)&dataBlock.msgContent[offset]) * 1.00 / 10000;
    offset += WORKING_PRICE_LEN;
  }

  // Base liquidity indicator
  char fieldBaseLiquidityIndicator = ' ';
  if (bitField.bit.bit7 == 1)
  {
    fieldBaseLiquidityIndicator = dataBlock.msgContent[offset];
    offset += BASE_LIQUIDITY_INDICATOR_LEN;
  }

  // Expire time
  // Note: use for TIF = 6 orders
  if (bitField.bit.bit8 == 1)
  {
    offset += EXPIRE_TIME_LEN;
  }

  //orderDetail.msgContent
  orderDetail.length = sprintf( orderDetail.msgContent,
      "%d=%X\001"   // msgtype
      "%d=%d\001"   // length
      "%d=%d\001"   // matching unit
      "%d=%d\001"   // seqNum
      "%d=%s\001"   // transaction time
      "%d=%d\001"   // clOrderid
      "%d=%ld\001"  // BATS order id
      "%d=%c\001"   // action
      "%d=%lf\001"  // peg diff
      "%d=%lf\001"  // price
      "%d=%c\001"   // exec inst
      "%d=%c\001"   // order type
      "%d=%c\001"   // tif
      "%d=%d\001"   // min quantity
      "%d=%d\001"   // max remove pct
      "%d=%.8s\001" // symbol
      "%d=%.8s\001" // symbolSfx
      "%d=%c\001"   // capacity
      "%d=%s\001"   // account
      "%d=%s\001"   // clearing firm
      "%d=%c\001"   // display indicator
      "%d=%d\001"   // max floor
      "%d=%d\001"   // discretion amount
      "%d=%d\001"   // shares
      "%d=%s\001"   // prevent member match
      "%d=%d\001"   // leaves quantity
      "%d=%d\001"   // last shares
      "%d=%lf\001"  // last px
      "%d=%lf\001"  // display price
      "%d=%lf\001"  // working price
      "%d=%c\001"   // base liquidity indicator
      ,fieldIndexList.MsgType, msgType,
      fieldIndexList.Length, msgLength,
      fieldIndexList.MatchUnit, fieldMatchUnit,
      fieldIndexList.InSeqNum, acceptedOrder.seqNum,
      fieldIndexList.TransactionTime, acceptedOrder.transTime,
      fieldIndexList.ClOrdID, acceptedOrder.clOrderID,
      fieldIndexList.ArcaOrderID, acceptedOrder.batsExOrderId ,
      fieldIndexList.Side, fieldSide,
      fieldIndexList.PegDifference, fieldPegDiff,
      fieldIndexList.Price, acceptedOrder.price,
      fieldIndexList.ExecInst, fieldExecInst,
      fieldIndexList.Type, fieldOrdType,
      fieldIndexList.TimeInForce, fieldTIF,
      fieldIndexList.MinQty, fieldMinQty,
      fieldIndexList.MaxRemovePct, fieldMaxRemovePct,
      fieldIndexList.Symbol, acceptedOrder.symbol,
      fieldIndexList.SymbolSfx, fieldSymbolSfx,
      fieldIndexList.Rule80A, fieldCapacity,
      fieldIndexList.Account, fieldAccount,
      fieldIndexList.Firm, acceptedOrder.clrFirm,
      fieldIndexList.Display, fieldDisplayIndicator,
      fieldIndexList.MaxFloor, fieldMaxFloor,
      fieldIndexList.DiscretionPrice, fieldDisAmount,
      fieldIndexList.Shares, acceptedOrder.shares,
      fieldIndexList.PreventMemberMatch, fieldPreventMemberMatch,
      fieldIndexList.LeavesShares, fieldLeavesQty,
      fieldIndexList.LastShares, fieldLastShares,
      fieldIndexList.LastPx, fieldLastPx,
      fieldIndexList.DisplayPrice, fieldDisplayPrice,
      fieldIndexList.WorkingPrice, fieldWorkingPrice,
      fieldIndexList.LiquidityIndicator, fieldBaseLiquidityIndicator
      );

  strcpy( acceptedOrder.msgContent, orderDetail.msgContent );

  // Update database
  if( type == MANUAL_ORDER )
  {
    // openOrder.tsId
    openOrder.tsId = MANUAL_ORDER;
  }
  else
  {
    // openOrder.tsId
    openOrder.tsId = tradeServersInfo.config[type].tsId;
  }

  // Call function to update a open order
  openOrder.account = dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID];
  int indexOpenOrder = ProcessUpdateASOpenOrderForOrderACK( &openOrder, &orderDetail );
  if (indexOpenOrder == -2)
  {
    AddDataBlockToWaitList(oecType, WAIT_LIST_TYPE_BATS_ORDER_ACK, acceptedOrder.clOrderID, dataBlock, type, timeStamp);
    return 0;
  }
  
  // Call function to update Order ACK of BATS BOE Order in Automatic Order
  ProcessInsertAcceptedOrder4BATS_BOEQueryToCollection(&acceptedOrder);

  if (AddOrderIdToList(&__ack, openOrder.clOrdId) == ERROR) //Duplicated
  {
    return SUCCESS;
  }
  
  char ecnOrderID[32];
  sprintf(ecnOrderID, "%lu", acceptedOrder.batsExOrderId);

  char account[32];
  GetAccountName(oecType, dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID], account);

  if (fieldCapacity == ' ')
  {
    fieldCapacity = 'P';
  }
  
  int _venueType = oecType - TYPE_BASE;
  if (indexOpenOrder != ERROR)
  {
    AddOJOrderAcceptedRecord(timeStamp, asOpenOrder.orderCollection[indexOpenOrder].orderSummary.symbol, exOrderId, openOrder.clOrdId, ecnOrderID, _venueType, account, fieldCapacity);
  }
  else
  {
    AddOJOrderAcceptedRecord(timeStamp, "", exOrderId, openOrder.clOrdId, ecnOrderID, _venueType, account, fieldCapacity);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessExecutedOrderOfBATS_BOEOrder
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessExecutedOrderOfBATS_BOEOrder(t_DataBlock dataBlock, int type, char *timeStamp, int oecType)
{
  t_BATS_BOEExecutedOrder executedOrder;
  t_OrderDetail orderDetail;

  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;

  // YYYY/mm/DD
  char dateTime[32];
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]), &dateTime[0]);
  memcpy( executedOrder.processedTimestamp, &dateTime[11], TIME_LENGTH );
  executedOrder.processedTimestamp[TIME_LENGTH] = 0;
  
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]), &dateTime[0]);
  memcpy( executedOrder.date, &dateTime[0], 10);
  executedOrder.date[10] = 0;

  // HH:MM:SS.mss AM/PM
  memcpy( executedOrder.timestamp, &dateTime[11], TIME_LENGTH );
  executedOrder.timestamp[TIME_LENGTH] = 0;

  // orderDetail.timestamp
  memcpy(orderDetail.timestamp, &dateTime[0], TIMESTAMP_LENGTH);
  orderDetail.timestamp[TIMESTAMP_LENGTH] = 0;

  // Messsage type
  strcpy( executedOrder.msgType, "ExecutedOrder" );

  // Length
  int fieldLength = GetShortNumber(&dataBlock.msgContent[2]);

  // msgType
  char msgType = BATS_BOE_ORDER_EXECUTION;

  // matching unit
  int fieldMatchUnit = dataBlock.msgContent[5];

  // Sequence number
  executedOrder.seqNum = GetIntNumber(&dataBlock.msgContent[6]);

  // Transaction time
  unsigned long totalnanos = GetLongNumber((char *)&dataBlock.msgContent[10]);

  // Get the number of seconds from Jan 1st 1970
  int numSecs = totalnanos / 1000000000;

  numSecs %= NUMBER_OF_SECONDS_PER_DAY;

  // Get the number of nanoseconds
  int numNanos = totalnanos % 1000000000;

  // write to transtime
  sprintf(executedOrder.transTime, "%02d:%02d:%02d.%09d", numSecs / 3600, (numSecs % 3600) / 60, (numSecs % 3600) % 60, numNanos);

  // client Order id
  char fieldClOrderID[21];
  strncpy(fieldClOrderID, (char *)&dataBlock.msgContent[18], 20);
  fieldClOrderID[20] = 0;
  executedOrder.clOrderID = atoi(fieldClOrderID);

  char exOrderId[32];
  sprintf(exOrderId, "%d", executedOrder.clOrderID);
  
  // Execution ID
  executedOrder.execID = GetLongNumber((char *)&dataBlock.msgContent[38]);

  executedOrder.lastShares = GetIntNumber(&dataBlock.msgContent[46]);

  executedOrder.lastPx = GetLongNumber((char *)&dataBlock.msgContent[50]) * 1.00 / 10000;

  executedOrder.leavesShares = GetIntNumber(&dataBlock.msgContent[58]);

  executedOrder.baseLiquidityIndicator = dataBlock.msgContent[62];

  executedOrder.subLiquidityIndicator = dataBlock.msgContent[63];

  executedOrder.accessFee = GetLongNumber((char *)&dataBlock.msgContent[64]) * 1.00 / 100000;

  strncpy(executedOrder.controBroker, (char *)&dataBlock.msgContent[72], 4);
  executedOrder.controBroker[4] = 0;

  /*----------------------------------------
   * Process optional fields
   * Optional fields start from offset 84...
   *--------------------------------------*/
  int offset = STARTING_OFFSET_OF_FILL_ORDER;
  union t_BitField bitField;

  /*------------------------
   *  Accepted order bitField 1
   *-----------------------*/
  bitField.value = dataBlock.msgContent[76];

  // Side
  char buysell = '0';
  char action = ' ';
  if (bitField.bit.bit1 == 1)
  {
    action = dataBlock.msgContent[offset];
    switch ( action )
    {
      case '1':
        buysell = 'B';
        strcpy( executedOrder.side, "B" );
        break;

      case '2':
        buysell = 'S';
        strcpy( executedOrder.side, "S" );
        break;

      case '5':
        buysell = 'T';
        strcpy( executedOrder.side, "SS" );
        break;

      default:
        strcpy( executedOrder.side, " " );
        TraceLog(ERROR_LEVEL, "%s: Not support side = %c, %d\n", GetOECName(oecType), action, action );
        break;
    }
    offset += SIDE_LEN;
  }

  // peg difference
  double fieldPegDiff = 0.00;
  if (bitField.bit.bit2 == 1)
  {
    fieldPegDiff = GetLongNumber((char *)&dataBlock.msgContent[offset]) * 1.00 / 10000;
    offset += PEG_DIFF_LEN;
  }

  // Price
  if (bitField.bit.bit3 == 1)
  {
    executedOrder.price = GetLongNumber((char *)&dataBlock.msgContent[offset]) * 1.00 / 10000;
    offset += PRICE_LEN;
  }
  else
  {
    executedOrder.price = 0.00;
  }

  // Exec instruction
  char fieldExecInst = ' ';
  if (bitField.bit.bit4 == 1)
  {
    fieldExecInst = dataBlock.msgContent[offset];
    offset += EXEC_INST_LEN;
  }

  // Order type
  char fieldOrdType = ' ';
  if (bitField.bit.bit5 == 1)
  {
    fieldOrdType = dataBlock.msgContent[offset];
    offset += ORDER_TYPE_LEN;
  }

  //Time In Force
  char fieldTIF = ' ';
  if (bitField.bit.bit6 == 1)
  {
    fieldTIF = dataBlock.msgContent[offset];
    offset += TIME_IN_FORCE_LEN;
  }

  // Min quantity
  int fieldMinQty = 0;
  if (bitField.bit.bit7 == 1)
  {
    fieldMinQty = GetIntNumber(&dataBlock.msgContent[offset]);
    offset += MIN_QTY_LEN;
  }

  // Max remove pct
  int fieldMaxRemovePct = 0;
  if (bitField.bit.bit8 == 1)
  {
    fieldMaxRemovePct = dataBlock.msgContent[offset];
    offset += MAX_REMOVE_PCT_LEN;
  }

  /*------------------------
   *  Execution order bitField 2
   *-----------------------*/
  bitField.value = dataBlock.msgContent[77];

  // Symbol
  if (bitField.bit.bit1 == 1)
  {
    strncpy(executedOrder.symbol, (char *)&dataBlock.msgContent[offset], SYMBOL_LEN);
    offset += SYMBOL_LEN;
  }
  else
  {
    strcpy(executedOrder.symbol, " ");
  }

  // Symbol Sfx
  char fieldSymbolSfx[SYMBOL_SFX_LEN];
  if (bitField.bit.bit2 == 1)
  {
    strncpy(fieldSymbolSfx, (char *)&dataBlock.msgContent[offset], SYMBOL_SFX_LEN);
    offset += SYMBOL_SFX_LEN;
    strcat(executedOrder.symbol, fieldSymbolSfx);
  }
  else
  {
    strcpy(fieldSymbolSfx, " ");
  }

  // Capacity
  char fieldCapacity = ' ';
  if (bitField.bit.bit7 == 1)
  {
    fieldCapacity = dataBlock.msgContent[offset];
    offset += CAPACITY_LEN;
  }

  /*------------------------
   *  Execution order bitField 3
   *-----------------------*/
  bitField.value = dataBlock.msgContent[78];

  // Account
  char fieldAccount[6];
  if (bitField.bit.bit1 == 1)
  {
    strncpy(fieldAccount, (char *)&dataBlock.msgContent[offset], 4);
    fieldAccount[4] = 0;
    offset += ACCOUNT_LEN;
  }
  else
  {
    strcpy(fieldAccount, " ");
  }

  // Clearing firm
  if (bitField.bit.bit2 == 1)
  {
    strncpy(executedOrder.clrFirm, (char *)&dataBlock.msgContent[offset], 4);
    executedOrder.clrFirm[4] = 0;
    offset += CLEARING_FIRM_LEN;
  }
  else
  {
    strcpy(executedOrder.clrFirm, " ");
  }

  // Clearing account
  if (bitField.bit.bit3 == 1) offset += CLEARING_ACC_LEN;

  // Display indicator
  char fieldDisplayIndicator = ' ';
  if (bitField.bit.bit4 == 1)
  {
    fieldDisplayIndicator = dataBlock.msgContent[offset];
    offset += DISPLAY_INDICATOR_LEN;
  }

  // Max floor
  int fieldMaxFloor = 0;
  if (bitField.bit.bit5 == 1)
  {
    fieldMaxFloor = GetIntNumber(&dataBlock.msgContent[offset]);
    offset += MAX_FLOOR_LEN;
  }

  // Discretion amount
  short fieldDisAmount = 0;
  if (bitField.bit.bit6 == 1)
  {
    fieldDisAmount = GetShortNumber(&dataBlock.msgContent[offset]);
    offset += DISCRETION_AMOUNT_LEN;
  }

  // Order quantity
  if (bitField.bit.bit7 == 1)
  {
    executedOrder.shares = GetIntNumber(&dataBlock.msgContent[offset]);
    offset += ORDER_QTY_LEN;
  }
  else
  {
    executedOrder.shares = 0;
  }

  // Prevent member match
  char fieldPreventMemberMatch[4];
  if (bitField.bit.bit8 == 1)
  {
    strncpy(fieldPreventMemberMatch, (char *)&dataBlock.msgContent[offset], 3);
    fieldPreventMemberMatch[3] = 0;
    offset += PREVENT_MEMBER_MATCH_LEN;
  }
  else
  {
    strcpy(fieldPreventMemberMatch, " ");
  }

  /*------------------------
   *  Execution order bitField 4, 5, 6, 7: use in the future
   *-----------------------*/

  //orderDetail.msgContent
  orderDetail.length = sprintf( orderDetail.msgContent,
      "%d=%X\001"   // msgtype
      "%d=%d\001"   // length
      "%d=%d\001"   // matching unit
      "%d=%d\001"   // seqNum
      "%d=%s\001"   // transaction time
      "%d=%d\001"   // clOrderid
      "%d=%ld\001"  // Execution id
      "%d=%d\001"   // last shares
      "%d=%lf\001"  // last px
      "%d=%c\001"   // base liquidity indicator
      "%d=%c\001"   // sub liquidity indicator
      "%d=%lf\001"  // access fee
      "%d=%s\001"   // contra broker
      "%d=%c\001"   // Action
      "%d=%lf\001"  // peg diff
      "%d=%lf\001"  // price
      "%d=%c\001"   // exec inst
      "%d=%c\001"   // order type
      "%d=%c\001"   // tif
      "%d=%d\001"   // min quantity
      "%d=%d\001"   // max remove pct
      "%d=%.8s\001"   // symbol
      "%d=%.8s\001"   // symbolSfx
      "%d=%c\001"   // capacity
      "%d=%s\001"   // account
      "%d=%s\001"   // clearing firm
      "%d=%c\001"   // display indicator
      "%d=%d\001"   // max floor
      "%d=%d\001"   // discretion amount
      "%d=%d\001"   // shares
      "%d=%s\001"   // prevent member match
      ,fieldIndexList.MsgType, msgType,
      fieldIndexList.Length, fieldLength,
      fieldIndexList.MatchUnit, fieldMatchUnit,
      fieldIndexList.InSeqNum, executedOrder.seqNum,
      fieldIndexList.TransactionTime, executedOrder.transTime,
      fieldIndexList.ClOrdID, executedOrder.clOrderID,
      fieldIndexList.ExecID, executedOrder.execID,
      fieldIndexList.LastShares, executedOrder.lastShares,
      fieldIndexList.LastPx, executedOrder.lastPx,
      fieldIndexList.LiquidityIndicator, executedOrder.baseLiquidityIndicator,
      fieldIndexList.SubLiquidityIndicator, executedOrder.subLiquidityIndicator,
      fieldIndexList.AccessFee, executedOrder.accessFee,
      fieldIndexList.ContraBroker, executedOrder.controBroker,
      fieldIndexList.Side, action,
      fieldIndexList.PegDifference, fieldPegDiff,
      fieldIndexList.Price, executedOrder.price,
      fieldIndexList.ExecInst, fieldExecInst,
      fieldIndexList.Type, fieldOrdType,
      fieldIndexList.TimeInForce, fieldTIF,
      fieldIndexList.MinQty, fieldMinQty,
      fieldIndexList.MaxRemovePct, fieldMaxRemovePct,
      fieldIndexList.Symbol, executedOrder.symbol,
      fieldIndexList.SymbolSfx, fieldSymbolSfx,
      fieldIndexList.Rule80A, fieldCapacity,
      fieldIndexList.Account, fieldAccount,
      fieldIndexList.Firm, executedOrder.clrFirm,
      fieldIndexList.Display, fieldDisplayIndicator,
      fieldIndexList.MaxFloor, fieldMaxFloor,
      fieldIndexList.DiscretionPrice, fieldDisAmount,
      fieldIndexList.Shares, executedOrder.shares,
      fieldIndexList.PreventMemberMatch, fieldPreventMemberMatch
      );

  strcpy( executedOrder.msgContent, orderDetail.msgContent );

  // Update database
  int tsId;

  if( type == MANUAL_ORDER )
  {
    // tsId
    tsId = MANUAL_ORDER;
  }
  else
  {
    // tsId
    tsId = tradeServersInfo.config[type].tsId;
  }

  executedOrder.entryExitRefNum = *(unsigned short *)&dataBlock.addContent[DATABLOCK_OFFSET_ENTRY_EXIT_REF];
  
  char liquidityIndicator[6];
  liquidityIndicator[0] = executedOrder.baseLiquidityIndicator;
  liquidityIndicator[1] = 0;
  
  // Call function to update a open order (non fill)
  int indexOpenOrder = ProcessUpdateASOpenOrderForExecuted(tsId, executedOrder.clOrderID, orderStatusIndex.Closed, executedOrder.lastShares, executedOrder.lastPx, &orderDetail, dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID], liquidityIndicator, &executedOrder.fillFee);

  if (indexOpenOrder == -2)
  {
    AddDataBlockToWaitList(oecType, WAIT_LIST_TYPE_BATS_ORDER_FILLED, executedOrder.clOrderID, dataBlock, type, timeStamp);
    return 0;
  }
  
  if (indexOpenOrder != ERROR)
  {
    strcpy( executedOrder.status, GetOrderStatus(asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status));

    executedOrder.leavesShares = asOpenOrder.orderCollection[indexOpenOrder].orderSummary.leftShares;
    executedOrder.avgPrice = asOpenOrder.orderCollection[indexOpenOrder].orderSummary.avgPrice;
  }

  // Call function to update Order Fill of BATS BOE
  ProcessInsertFillOrder4BATS_BOEQueryToCollection(&executedOrder);

  if (AddExecDataToList(&__exec, executedOrder.clOrderID, executedOrder.execID) == ERROR) //Duplicated
  {
    return SUCCESS;
  }
  
  char execID[32];
  sprintf(execID, "%lu", executedOrder.execID);
  
  char ecnOrderID[32] = "''";
  int _serverId = (type == MANUAL_ORDER) ? MAX_TRADE_SERVER_CONNECTIONS : type;
  if (ExchangeOrderIdMapping[_serverId][executedOrder.clOrderID % MAX_ORDER_PLACEMENT] != 0)
  {
    sprintf(ecnOrderID, "%lu", ExchangeOrderIdMapping[_serverId][executedOrder.clOrderID % MAX_ORDER_PLACEMENT]);
  }
  else
  {
    TraceLog(ERROR_LEVEL, "%s: Could not get ExOrderId, orderID: %d\n", __func__, executedOrder.clOrderID);
  }

  char account[32];
  GetAccountName(oecType, dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID], account);

  int _venueType = oecType - TYPE_BASE;
  if (indexOpenOrder != ERROR)
  {
    AddOJOrderFillRecord(timeStamp, asOpenOrder.orderCollection[indexOpenOrder].orderSummary.symbol, exOrderId, _venueType, buysell, executedOrder.lastPx,
        executedOrder.lastShares, executedOrder.clOrderID, liquidityIndicator,
              ecnOrderID, execID, "1", (executedOrder.leavesShares == 0)?1:0, account, 0);
  }
  else
  {
    AddOJOrderFillRecord(timeStamp, "", exOrderId, _venueType, buysell, executedOrder.lastPx,
        executedOrder.lastShares, executedOrder.clOrderID, liquidityIndicator,
              ecnOrderID, execID, "1", (executedOrder.leavesShares == 0)?1:0, account, 0);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessCanceledOrderOfBATS_BOEOrder
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessCanceledOrderOfBATS_BOEOrder(t_DataBlock dataBlock, int type, char *timeStamp, int oecType)
{
  t_BATS_BOECanceledOrder canceledOrder;
  t_OrderDetail orderDetail;

  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;

  // YYYY/mm/DD
  char dateTime[32];
    GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]), &dateTime[0]);
    memcpy( canceledOrder.processedTimestamp, &dateTime[11], TIME_LENGTH );
  canceledOrder.processedTimestamp[TIME_LENGTH] = 0;
    
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]), &dateTime[0]);
  memcpy( canceledOrder.date, &dateTime[0], 10);
  canceledOrder.date[10] = 0;

  // HH:MM:SS.mss AM/PM
  memcpy( canceledOrder.timestamp, &dateTime[11], TIME_LENGTH );
  canceledOrder.timestamp[TIME_LENGTH] = 0;

  // orderDetail.timestamp
  memcpy(orderDetail.timestamp, &dateTime[0], TIMESTAMP_LENGTH);
  orderDetail.timestamp[TIMESTAMP_LENGTH] = 0;

  // Messsage type
  strcpy(canceledOrder.msgType, "CanceledOrder");

  // length
  int fieldLength = GetShortNumber(&dataBlock.msgContent[2]);

  // msgType
  char msgType = BATS_BOE_ORDER_CANCELLED;

  // Matching unit
  int fieldMatchUnit = dataBlock.msgContent[5];

  // Sequence number;
  canceledOrder.seqNum = GetIntNumber(&dataBlock.msgContent[6]);

  // Transaction time
  unsigned long totalnanos = GetLongNumber((char *)&dataBlock.msgContent[10]);

  // Get the number of seconds from Jan 1st 1970
  int numSecs = totalnanos / 1000000000;

  numSecs %= NUMBER_OF_SECONDS_PER_DAY;

  // Get the number of nanoseconds
  int numNanos = totalnanos % 1000000000;

  // write to transtime
  sprintf(canceledOrder.transTime, "%02d:%02d:%02d.%09d", numSecs / 3600 , (numSecs % 3600) / 60, (numSecs % 3600) % 60, numNanos);

  // client orderid;
  char fieldOrderID[21];
  strncpy(fieldOrderID, (char *)&dataBlock.msgContent[18], 20);
  fieldOrderID[20] = 0;
  canceledOrder.clOrderId = atoi(fieldOrderID);

  // canceled order reason
  canceledOrder.reason = dataBlock.msgContent[38];

  /*----------------------------------------
   * Process optional fields
   * Optional fields start from offset 47...
   *--------------------------------------*/
  int offset = STARTING_OFFSET_OF_CANCELED_ORDER;
  union t_BitField bitField;

  /*------------------------
   *  Canceled order bitField 1
   *-----------------------*/
  bitField.value = dataBlock.msgContent[39];

  // Side
  if (bitField.bit.bit1 == 1) offset += SIDE_LEN;

  /*------------------------
   *  Canceled order bitField 2
   *-----------------------*/
  bitField.value = dataBlock.msgContent[40];

  // Symbol
  if (bitField.bit.bit1 == 1) offset += SYMBOL_LEN;

  /*------------------------
   *  Canceled order bitField 3
   *-----------------------*/
  bitField.value = dataBlock.msgContent[41];

  // Account
  if (bitField.bit.bit1 == 1)
  {
    strncpy(canceledOrder.account, (char *)&dataBlock.msgContent[offset], 4);
    canceledOrder.account[4] = 0;
    offset += ACCOUNT_LEN;
  }
  else
  {
    strcpy(canceledOrder.account, " ");
  }

  // Clearing firm
  if (bitField.bit.bit2 == 1)
  {
    strncpy(canceledOrder.clrFirm, (char *)&dataBlock.msgContent[offset], 4);
    canceledOrder.clrFirm[4] = 0;
    offset += CLEARING_FIRM_LEN;
  }
  else
  {
    strcpy(canceledOrder.clrFirm, " ");
  }

  // Clearing account
  if (bitField.bit.bit3 == 1) offset += CLEARING_ACC_LEN;

  // Display indicator
  if (bitField.bit.bit4 == 1) offset += DISPLAY_INDICATOR_LEN;

  // Max floor
  if (bitField.bit.bit5 == 1) offset += MAX_FLOOR_LEN;

  // Discretion amount
  if (bitField.bit.bit6 == 1) offset += DISCRETION_AMOUNT_LEN;

  // Order quantity
  int shares = 0;
  if (bitField.bit.bit7 == 1)
  {
    shares = GetIntNumber(&dataBlock.msgContent[offset]);
    offset += ORDER_QTY_LEN;
  }

  // Prevent member match
  if (bitField.bit.bit8 == 1) offset += PREVENT_MEMBER_MATCH_LEN;

  /*------------------------
   *  Canceled order bitField 4, 6, 7: use in the future
   *-----------------------*/

  /*------------------------
   *  Canceled order bitField 5
   *-----------------------*/
  bitField.value = dataBlock.msgContent[43];

  // Original client order id
  char fieldOrgClOrderId[21];
  canceledOrder.orgClOrderId = 0;
  if (bitField.bit.bit1 == 1)
  {
    strncpy(fieldOrgClOrderId, (char *)&dataBlock.msgContent[offset], 20);
    fieldOrgClOrderId[20] = 0;
    canceledOrder.orgClOrderId = atoi(fieldOrgClOrderId);
  }

  if (canceledOrder.orgClOrderId > 0)
  {
    //orderDetail.msgContent
    orderDetail.length = sprintf( orderDetail.msgContent,
        "%d=%X\001"   // msgtype
        "%d=%d\001"   // length
        "%d=%d\001"   // matching unit
        "%d=%d\001"   // seqNum
        "%d=%s\001"   // transaction time
        "%d=%d\001"   // clOrderid
        "%d=%c\001"   // reason
        "%d=%s\001"   // account
        "%d=%s\001"   // clearing firm
        "%d=%d\001"   // leaves shares
        "%d=%d\001"   // original client order id
        ,fieldIndexList.MsgType, msgType,
        fieldIndexList.Length, fieldLength,
        fieldIndexList.MatchUnit, fieldMatchUnit,
        fieldIndexList.InSeqNum, canceledOrder.seqNum,
        fieldIndexList.TransactionTime, canceledOrder.transTime,
        fieldIndexList.ClOrdID, canceledOrder.clOrderId,
        fieldIndexList.Reason, canceledOrder.reason,
        fieldIndexList.Account, canceledOrder.account,
        fieldIndexList.Firm, canceledOrder.clrFirm,
        fieldIndexList.LeavesShares, shares,
        fieldIndexList.OriginalClientOrdID, canceledOrder.orgClOrderId
        );
    canceledOrder.clOrderId = canceledOrder.orgClOrderId;
  }
  else
  {
    orderDetail.length = sprintf( orderDetail.msgContent,
        "%d=%X\001"   // msgtype
        "%d=%d\001"   // length
        "%d=%d\001"   // matching unit
        "%d=%d\001"   // seqNum
        "%d=%s\001"   // transaction time
        "%d=%d\001"   // clOrderid
        "%d=%c\001"   // reason
        "%d=%s\001"   // account
        "%d=%s\001"   // clearing firm
        "%d=%d\001"   // leaves shares
        ,fieldIndexList.MsgType, msgType,
        fieldIndexList.Length, fieldLength,
        fieldIndexList.MatchUnit, fieldMatchUnit,
        fieldIndexList.InSeqNum, canceledOrder.seqNum,
        fieldIndexList.TransactionTime, canceledOrder.transTime,
        fieldIndexList.ClOrdID, canceledOrder.clOrderId,
        fieldIndexList.Reason, canceledOrder.reason,
        fieldIndexList.Account, canceledOrder.account,
        fieldIndexList.Firm, canceledOrder.clrFirm,
        fieldIndexList.LeavesShares, shares
        );
  }

  strcpy( canceledOrder.msgContent, orderDetail.msgContent );

  // Update database
  int tsId;

  if( type == MANUAL_ORDER )
  {
    // tsId
    tsId = MANUAL_ORDER;
  }
  else
  {
    // tsId
    tsId = tradeServersInfo.config[type].tsId;
  }

  // Call function to update a open order (non fill)
  int indexOpenOrder = ProcessUpdateASOpenOrderForStatus(tsId, canceledOrder.clOrderId, orderStatusIndex.CanceledByECN, &orderDetail);

  if (indexOpenOrder == -2)
  {
    AddDataBlockToWaitList(oecType, WAIT_LIST_TYPE_BATS_ORDER_CANCELED, canceledOrder.clOrderId, dataBlock, type, timeStamp);
    return 0;
  }
  
  if (indexOpenOrder != ERROR)
  {
    strcpy(canceledOrder.status, GetOrderStatus(asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status));
  }
  else
  {
    strcpy(canceledOrder.status, orderStatus.CanceledByECN);
  }

  canceledOrder.entryExitRefNum = *(unsigned short *)&dataBlock.addContent[DATABLOCK_OFFSET_ENTRY_EXIT_REF];
  
  // Call function to update Order Cancel of BATZ_BOE
  ProcessInsertCanceledOrder4BATS_BOEQueryToCollection(&canceledOrder);

  if (AddOrderIdToList(&__cancel, canceledOrder.clOrderId) == ERROR)  //Duplicated
  {
    return SUCCESS;
  }
  
  char ecnOrderID[32] = "''";
  int _serverId = (type == MANUAL_ORDER) ? MAX_TRADE_SERVER_CONNECTIONS : type;
  if (ExchangeOrderIdMapping[_serverId][canceledOrder.clOrderId % MAX_ORDER_PLACEMENT] != 0)
  {
    sprintf(ecnOrderID, "%lu", ExchangeOrderIdMapping[_serverId][canceledOrder.clOrderId % MAX_ORDER_PLACEMENT]);
  }
  else
  {
    TraceLog(ERROR_LEVEL, "%s: Could not get ExOrderId, orderID: %d\n", __func__, canceledOrder.clOrderId);
  }

  char reason[128];
  switch(canceledOrder.reason)
  {
    case 'A':
      strcpy(reason, "Admin");
      break;
    case 'B':
      strcpy(reason, "Duplicate%20ClOrdID");
      break;
    case 'H':
      strcpy(reason, "Halted");
      break;
    case 'L':
      strcpy(reason, "Order%20would%20look%20or%20cross%20NBBO");
      break;
    case 'N':
      strcpy(reason, "Ran%20out%20of%20liquidity%20to%20execute%20against");
      break;
    case 'R':
      strcpy(reason, "Routing%20unavailable");
      break;
    case 'S':
      strcpy(reason, "Short%20sell%20price%20violation");
      break;
    case 'T':
      strcpy(reason, "Fill%20would%20trade-through%20NBBO");
      break;
    case 'U':
      strcpy(reason, "User%20requested");
      break;
    case 'V':
      strcpy(reason, "Would%20wash");
      break;
    case 'W':
      strcpy(reason, "Add%20liquidity%20only%20order%20would%20remove");
      break;
    case 'X':
      strcpy(reason, "Order%20expired");
      break;
    case 'Z':
      strcpy(reason, "Unforeseen%20reason");
      break;
    case 'u':
      strcpy(reason, "User%20requested%20(delayed%20due%20to%20order%20being%20route%20pending)");
      break;
    case 'x':
      strcpy(reason, "Crossed%20market");
      break;
    default:
      sprintf(reason, "uknown%%20reason,%%20reason%%20code%%20%%3d%%20%c", canceledOrder.reason);
      break;
  }

  char account[32];
  GetAccountName(oecType, dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID], account);

  int _venueType = oecType - TYPE_BASE;
  if (indexOpenOrder != ERROR)
  {
    AddOJOrderCancelResponseRecord(timeStamp, asOpenOrder.orderCollection[indexOpenOrder].orderSummary.symbol, canceledOrder.clOrderId, ecnOrderID, reason, _venueType, asOpenOrder.orderCollection[indexOpenOrder].orderSummary.leftShares, 1, account);
  }
  else
  {
    AddOJOrderCancelResponseRecord(timeStamp, "", canceledOrder.clOrderId, ecnOrderID, reason, _venueType, 0, 1, account);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessCancelRequestOfBATS_BOEOrder
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessCancelRequestOfBATS_BOEOrder(t_DataBlock dataBlock, int type, char *timeStamp, int oecType)
{
  t_BATS_BOECancelRequest cancelRequest;
  t_OrderDetail orderDetail;

  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;

  // YYYY/mm/DD
  char dateTime[32];
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]), &dateTime[0]);
  memcpy( cancelRequest.date, &dateTime[0], 10);
  cancelRequest.date[10] = 0;

  // HH:MM:SS.mss AM/PM
  memcpy( cancelRequest.timestamp, &dateTime[11], TIME_LENGTH );
  cancelRequest.timestamp[TIME_LENGTH] = 0;

  // orderDetail.timestamp
  memcpy(orderDetail.timestamp, &dateTime[0], TIMESTAMP_LENGTH);
  orderDetail.timestamp[TIMESTAMP_LENGTH] = 0;

  // Messsage type
  strcpy( cancelRequest.msgType, "CancelRequest" );

  // Length
  int fieldLength = GetShortNumber(&dataBlock.msgContent[2]);

  // msgType
  char msgType = dataBlock.msgContent[4];

  // matching unit
  int fieldMatchUnit = dataBlock.msgContent[5];

  // Sequence number
  cancelRequest.seqNum = GetIntNumber(&dataBlock.msgContent[6]);

  // original client Order id
  char fieldOrgClOrderID[21];
  strncpy(fieldOrgClOrderID, (char *)&dataBlock.msgContent[10], 20);
  fieldOrgClOrderID[20] = 0;
  cancelRequest.orgClOrderID = atoi(fieldOrgClOrderID);

  /*----------------------------------------
   * Process optional fields
   * Optional fields start from offset 32...
   *--------------------------------------*/
  int offset = STARTING_OFFSET_OF_CANCEL_REQUEST;
  union t_BitField bitField;

  /*----------------------------------------
   * Cancel request bitfield 1
   *--------------------------------------*/
  bitField.value = dataBlock.msgContent[30];

  // clearing firm
  if (bitField.bit.bit1 == 1)
  {
    strncpy(cancelRequest.clrFirm, (char *)&dataBlock.msgContent[offset], 4);
    cancelRequest.clrFirm[4] = 0;
    offset += CLEARING_FIRM_LEN;
  }
  else strcpy(cancelRequest.clrFirm, " ");

  //orderDetail.msgContent
  orderDetail.length = sprintf( orderDetail.msgContent,
      "%d=%X\001"   // msgtype
      "%d=%d\001"   // length
      "%d=%d\001"   // matching unit
      "%d=%d\001"   // seqNum
      "%d=%d\001"   // original client order id
      "%d=%s\001"   // clearing firm
      ,fieldIndexList.MsgType, msgType,
      fieldIndexList.Length, fieldLength,
      fieldIndexList.MatchUnit, fieldMatchUnit,
      fieldIndexList.InSeqNum, cancelRequest.seqNum,
      fieldIndexList.OriginalClientOrdID, cancelRequest.orgClOrderID,
      fieldIndexList.ClrFirm, cancelRequest.clrFirm
      );

  strcpy( cancelRequest.msgContent, orderDetail.msgContent );

  // Update database
  int tsId;

  if( type == MANUAL_ORDER )
  {
    // tsId
    tsId = MANUAL_ORDER;
  }
  else
  {
    // tsId
    tsId = tradeServersInfo.config[type].tsId;
  }

  // Call function to update a open order (non fill)
  //int indexOpenOrder = ProcessUpdateASOpenOrderForStatus(tsId, cancelRequest.clOrderID, orderStatusIndex.CXLSent, &orderDetail );
  int indexOpenOrder = ProcessUpdateASOpenOrderForStatus(tsId, cancelRequest.orgClOrderID, orderStatusIndex.CXLSent, &orderDetail );

  if (indexOpenOrder == -2)
  {
    AddDataBlockToWaitList(oecType, WAIT_LIST_TYPE_BATS_CANCEL_REQUEST, cancelRequest.orgClOrderID, dataBlock, type, timeStamp);
    return 0;
  }
  
  if (indexOpenOrder != ERROR)
  {
    strcpy(cancelRequest.status, GetOrderStatus(asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status));
  }
  else
  {
    strcpy(cancelRequest.status, orderStatus.CXLSent);
  }

//  Call function to update cancel request of BATS BOE
  ProcessInsertCancelRequest4BATS_BOEQueryToCollection(&cancelRequest);

  char symbol[SYMBOL_LEN];
  strcpy(symbol, "''");

  int _venueType = oecType - TYPE_BASE;
  if (indexOpenOrder != ERROR)
  {
    AddOJOrderCancelRecord(timeStamp, cancelRequest.orgClOrderID, symbol, _venueType, asOpenOrder.orderCollection[indexOpenOrder].orderSummary.leftShares);
  }
  else
  {
    AddOJOrderCancelRecord(timeStamp, cancelRequest.orgClOrderID, symbol, _venueType, 0);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessRejectedOrderOfBATS_BOEOrder
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessRejectedOrderOfBATS_BOEOrder(t_DataBlock dataBlock, int type, char *timeStamp, int oecType)
{
  t_BATS_BOERejectedOrder rejectedOrder;
  t_OrderDetail orderDetail;

  /*------------------------------------------------
  Order detail:
  - int length;
  - int execId;
  - int isBroken;
  - char timestamp[MAX_TIMESTAMP];
  - char msgContent[MAX_ORDER_MSG];
  ------------------------------------------------*/
  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;

  // YYYY/mm/DD
  char dateTime[32];
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]), &dateTime[0]);
    memcpy( rejectedOrder.processedTimestamp, &dateTime[11], TIME_LENGTH );
  rejectedOrder.processedTimestamp[TIME_LENGTH] = 0;
    
    GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]), &dateTime[0]);
  memcpy( rejectedOrder.date, &dateTime[0], 10);
  rejectedOrder.date[10] = 0;

  // HH:MM:SS.mss AM/PM
  memcpy( rejectedOrder.timestamp, &dateTime[11], TIME_LENGTH );
  rejectedOrder.timestamp[TIME_LENGTH] = 0;

  // orderDetail.timestamp
  memcpy(orderDetail.timestamp, &dateTime[0], TIMESTAMP_LENGTH);
  orderDetail.timestamp[TIMESTAMP_LENGTH] = 0;

  // Messsage type
  strcpy( rejectedOrder.msgType, "RejectedOrder" );

  // Length
  int fieldLength = GetShortNumber(&dataBlock.msgContent[2]);

  // msgType
  char msgType = dataBlock.msgContent[4];

  // matching unit
  int fieldMatchUnit = dataBlock.msgContent[5];

  // Sequence number
  rejectedOrder.seqNum = GetIntNumber(&dataBlock.msgContent[6]);

  // Transaction time
  unsigned long totalnanos = GetLongNumber((char *)&dataBlock.msgContent[10]);

  // Get the number of seconds from Jan 1st 1970
  int numSecs = totalnanos / 1000000000;

  numSecs %= NUMBER_OF_SECONDS_PER_DAY;

  // Get the number of nanoseconds
  int numNanos = totalnanos % 1000000000;

  // write to transtime
  sprintf(rejectedOrder.transTime, "%02d:%02d:%02d.%09d", numSecs / 3600, (numSecs % 3600) / 60, (numSecs % 3600) % 60, numNanos);

  // client Order id
  char fieldClOrderID[21];
  strncpy(fieldClOrderID, (char *)&dataBlock.msgContent[18], 20);
  fieldClOrderID[20] = 0;
  rejectedOrder.clOrderID = atoi(fieldClOrderID);

  // Order reject reason
  rejectedOrder.reason = dataBlock.msgContent[38];

  // Text
  strncpy(rejectedOrder.text, (char *)&dataBlock.msgContent[39], 60);
  rejectedOrder.text[60] = 0;

  // Raise order rejected alert
  char _reasonCode[2];
  _reasonCode[0] = rejectedOrder.reason;
  _reasonCode[1] = 0;
  CheckNSendOrderRejectedAlertToAllTT(oecType, _reasonCode, rejectedOrder.text, rejectedOrder.clOrderID);

  /*----------------------------------------
   * Process optional fields
   * Optional fields start from offset 107...
   *--------------------------------------*/
  int offset = STARTING_OFFSET_OF_REJECTED_ORDER;
  union t_BitField bitField;

  /*------------------------
   *  Rejected order bitField 1
   *-----------------------*/
  bitField.value = dataBlock.msgContent[99];

  // Side
  if (bitField.bit.bit1 == 1) offset += SIDE_LEN;

  // Peg diff
  if (bitField.bit.bit2 == 1) offset += PEG_DIFF_LEN;

  // Price
  if (bitField.bit.bit3 == 1) offset += PRICE_LEN;

  // Exec inst
  if (bitField.bit.bit4 == 1) offset += EXEC_INST_LEN;

  // Order type
  if (bitField.bit.bit5 == 1) offset += ORDER_TYPE_LEN;

  // time in force
  if (bitField.bit.bit6 == 1) offset += TIME_IN_FORCE_LEN;

  // Min quantity
  if (bitField.bit.bit7 == 1) offset += MIN_QTY_LEN;

  // Max remove pct
  if (bitField.bit.bit8 == 1) offset += MAX_REMOVE_PCT_LEN;

  /*------------------------
   *  Rejected order bitField 2
   *-----------------------*/
  bitField.value = dataBlock.msgContent[100];

  // Symbol
  if (bitField.bit.bit1 == 1)
  {
    strncpy(rejectedOrder.symbol, (char *)&dataBlock.msgContent[offset], SYMBOL_LEN);
    offset += SYMBOL_LEN;
  }
  else
  {
    strcpy(rejectedOrder.symbol, " ");
  }

  // SymbolSfx
  if (bitField.bit.bit2 == 1) offset += SYMBOL_SFX_LEN;

  // Capacity
  if (bitField.bit.bit7 == 1) offset += CAPACITY_LEN;

  /*------------------------
   *  Rejected order bitField 3
   *-----------------------*/
  bitField.value = dataBlock.msgContent[101];

  // Account
  if (bitField.bit.bit1 == 1)
  {
    strncpy(rejectedOrder.account, (char *)&dataBlock.msgContent[offset], 4);
    rejectedOrder.account[4] = 0;
    offset += ACCOUNT_LEN;
  }
  else
  {
    strcpy(rejectedOrder.account, " ");
  }

  // Clearing firm
  if (bitField.bit.bit2 == 1)
  {
    strncpy(rejectedOrder.clrFirm, (char *)&dataBlock.msgContent[offset], 4);
    rejectedOrder.clrFirm[4] = 0;
    offset += CLEARING_FIRM_LEN;
  }
  else
  {
    strcpy(rejectedOrder.clrFirm, " ");
  }

  // Clearing account
  if (bitField.bit.bit3 == 1) offset += CLEARING_ACC_LEN;

  // Display indicator
  if (bitField.bit.bit4 == 1) offset += DISPLAY_INDICATOR_LEN;

  // Max floor
  if (bitField.bit.bit5 == 1) offset += MAX_FLOOR_LEN;

  // Discretion amount
  if (bitField.bit.bit6 == 1) offset += DISCRETION_AMOUNT_LEN;

  // Order quantity
  int fieldShares = 0;
  if (bitField.bit.bit7 == 1)
  {
    fieldShares = GetIntNumber(&dataBlock.msgContent[offset]);
    offset += ORDER_QTY_LEN;
  }

  // Prevent member match

  /*------------------------
   *  Rejected order bitField 4, 5, 6, 7: use in the future
   *-----------------------*/

  //orderDetail.msgContent
  orderDetail.length = sprintf( orderDetail.msgContent,
      "%d=%X\001"   // msgtype
      "%d=%d\001"   // length
      "%d=%d\001"   // matching unit
      "%d=%d\001"   // seqNum
      "%d=%s\001"   // transaction time
      "%d=%d\001"   // clOrderid
      "%d=%c\001"   // reason
      "%d=%s\001"   // text
      "%d=%.8s\001"   // symbol
      "%d=%s\001"   // account
      "%d=%s\001"   // clearing firm
      "%d=%d\001"   // shares
      ,fieldIndexList.MsgType, msgType,
      fieldIndexList.Length, fieldLength,
      fieldIndexList.MatchUnit, fieldMatchUnit,
      fieldIndexList.InSeqNum, rejectedOrder.seqNum,
      fieldIndexList.TransactionTime, rejectedOrder.transTime,
      fieldIndexList.ClOrdID, rejectedOrder.clOrderID,
      fieldIndexList.Reason, rejectedOrder.reason,
      fieldIndexList.RejectText, rejectedOrder.text,
      fieldIndexList.Symbol, rejectedOrder.symbol,
      fieldIndexList.Account, rejectedOrder.account,
      fieldIndexList.ClrFirm, rejectedOrder.clrFirm,
      fieldIndexList.Shares, fieldShares
      );

  strcpy( rejectedOrder.msgContent, orderDetail.msgContent );

  // Update database
  int tsId;

  if( type == MANUAL_ORDER )
  {
    tsId = MANUAL_ORDER;
  }
  else
  {
    tsId = tradeServersInfo.config[type].tsId;
  }

  // Call function to update a open order (non fill)
  int indexOpenOrder = ProcessUpdateASOpenOrderForStatus(tsId, rejectedOrder.clOrderID, orderStatusIndex.Rejected, &orderDetail );

  if (indexOpenOrder == -2)
  {
    AddDataBlockToWaitList(oecType, WAIT_LIST_TYPE_BATS_ORDER_REJECTED, rejectedOrder.clOrderID, dataBlock, type, timeStamp);
    return 0;
  }
  
  if (indexOpenOrder != ERROR)
  {
    strcpy(rejectedOrder.status, GetOrderStatus(asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status));
  }
  else
  {
    strcpy(rejectedOrder.status, orderStatus.CancelRejected);
  }

  rejectedOrder.entryExitRefNum = *(unsigned short *)&dataBlock.addContent[DATABLOCK_OFFSET_ENTRY_EXIT_REF];
  
  // Call function to update order reject or cancel reject of BATS BOE
  ProcessInsertRejectOrder4BATS_BOEQueryToCollection(&rejectedOrder);

  if (AddOrderIdToList(&__reject, rejectedOrder.clOrderID) == ERROR)  //Duplicated
  {
    return 0;
  }
  
  char ecnOrderID[32];
  strcpy(ecnOrderID, "''");

  char reasonCode[5];
  sprintf(reasonCode, "%d", rejectedOrder.reason);

  // Convert special character to HTML format char reason[120];
  char reason[120];
  ConvertSpecialCharacter(rejectedOrder.text, reason);

  char account[32];
  GetAccountName(oecType, dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID], account);

  int _venueType = oecType - TYPE_BASE;
  if (indexOpenOrder != ERROR)
  {
    AddOJOrderRejectRecord(timeStamp, rejectedOrder.clOrderID, ecnOrderID, reason, asOpenOrder.orderCollection[indexOpenOrder].orderSummary.symbol, _venueType, reasonCode, account);
  }
  else
  {
    AddOJOrderRejectRecord(timeStamp, rejectedOrder.clOrderID, ecnOrderID, reason, "", _venueType, reasonCode, account);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessRejectedCancelOfBATS_BOEOrder
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessRejectedCancelOfBATS_BOEOrder(t_DataBlock dataBlock, int type, char *timeStamp, int oecType)
{
  t_BATS_BOERejectedCancel rejectedCancel;
  t_OrderDetail orderDetail;

  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;

  // YYYY/mm/DD
  char dateTime[32];
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]), &dateTime[0]);
  memcpy( rejectedCancel.date, &dateTime[0], 10);
  rejectedCancel.date[10] = 0;

  // HH:MM:SS.mss AM/PM
  memcpy( rejectedCancel.timestamp, &dateTime[11], TIME_LENGTH );
  rejectedCancel.timestamp[TIME_LENGTH] = 0;

  // orderDetail.timestamp
  memcpy(orderDetail.timestamp, &dateTime[0], TIMESTAMP_LENGTH);
  orderDetail.timestamp[TIMESTAMP_LENGTH] = 0;

  // Messsage type
  strcpy( rejectedCancel.msgType, "RejectedCancel" );

  // Length
  int fieldLength = GetShortNumber(&dataBlock.msgContent[2]);

  // msgType
  char msgType = dataBlock.msgContent[4];

  // matching unit
  int fieldMatchUnit = dataBlock.msgContent[5];

  // Sequence number
  rejectedCancel.seqNum = GetIntNumber(&dataBlock.msgContent[6]);

  // Transaction time
  unsigned long totalnanos = GetLongNumber((char *)&dataBlock.msgContent[10]);

  // Get the number of seconds from Jan 1st 1970
  int numSecs = totalnanos / 1000000000;

  numSecs %= NUMBER_OF_SECONDS_PER_DAY;

  // Get the number of nanoseconds
  int numNanos = totalnanos % 1000000000;

  // write to transtime
  sprintf(rejectedCancel.transTime, "%02d:%02d:%02d.%09d", numSecs / 3600, (numSecs % 3600) / 60, (numSecs % 3600) % 60, numNanos);

  // client Order id
  char fieldClOrderID[21];
  strncpy(fieldClOrderID, (char *)&dataBlock.msgContent[18], 20);
  fieldClOrderID[20] = 0;
  rejectedCancel.orderID = atoi(fieldClOrderID);

  // Cancel reject reason
  rejectedCancel.reason = dataBlock.msgContent[38];

  // Text
  strncpy(rejectedCancel.text, (char *)&dataBlock.msgContent[39], 60);
  rejectedCancel.text[60] = 0;

  /*----------------------------------------
   * Process optional fields
   * Optional fields start from offset 107...
   *--------------------------------------*/

  /*------------------------
   *  Rejected order bitFields: use in the future
   *-----------------------*/

  //orderDetail.msgContent
  orderDetail.length = sprintf( orderDetail.msgContent,
      "%d=%X\001"   // msgtype
      "%d=%d\001"   // length
      "%d=%d\001"   // matching unit
      "%d=%d\001"   // seqNum
      "%d=%s\001"   // transaction time
      "%d=%d\001"   // clOrderid
      "%d=%c\001"   // reason
      "%d=%s\001"   // text
      ,fieldIndexList.MsgType, msgType,
      fieldIndexList.Length, fieldLength,
      fieldIndexList.MatchUnit, fieldMatchUnit,
      fieldIndexList.InSeqNum, rejectedCancel.seqNum,
      fieldIndexList.TransactionTime, rejectedCancel.transTime,
      fieldIndexList.ClOrdID, rejectedCancel.orderID,
      fieldIndexList.Reason, rejectedCancel.reason,
      fieldIndexList.RejectText, rejectedCancel.text
      );

  strcpy( rejectedCancel.msgContent, orderDetail.msgContent );

  // Update database
  int tsId;

  if( type == MANUAL_ORDER )
  {
    // tsId
    tsId = MANUAL_ORDER;
  }
  else
  {
    // tsId
    tsId = tradeServersInfo.config[type].tsId;
  }

  // Call function to update a open order (non fill)
  int indexOpenOrder = ProcessUpdateASOpenOrderForStatus(tsId, rejectedCancel.orderID, orderStatusIndex.Rejected, &orderDetail );

  if (indexOpenOrder == -2)
  {
    AddDataBlockToWaitList(oecType, WAIT_LIST_TYPE_BATS_CANCEL_REJECTED, rejectedCancel.orderID, dataBlock, type, timeStamp);
    return 0;
  }
  
  if (indexOpenOrder != ERROR)
  {
    strcpy(rejectedCancel.status, GetOrderStatus(asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status));
  }
  else
  {
    strcpy(rejectedCancel.status, orderStatus.CancelRejected);
  }

  // Call function to update Rejected Order of BATS
  ProcessInsertRejectCancel4BATS_BOEQueryToCollection(&rejectedCancel);

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessBrokenTradeOfBATS_BOEOrder
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessBrokenOrderOfBATS_BOEOrder(t_DataBlock dataBlock, int type, char *timeStamp, int oecType)
{
  t_BATS_BOEBrokenOrder brokenOrder;
  t_OrderDetail orderDetail;

  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;

  // YYYY/mm/DD
  char dateTime[32];
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]), &dateTime[0]);
  memcpy( brokenOrder.date, &dateTime[0], 10);
  brokenOrder.date[10] = 0;

  // HH:MM:SS.mss AM/PM
  memcpy( brokenOrder.timestamp, &dateTime[11], TIME_LENGTH );
  brokenOrder.timestamp[TIME_LENGTH] = 0;

  // orderDetail.timestamp
  memcpy(orderDetail.timestamp, &dateTime[0], TIMESTAMP_LENGTH);
  orderDetail.timestamp[TIMESTAMP_LENGTH] = 0;

  // Status: Sent, Live, Rejected, Closed, BrokenTrade, CanceledByECN,
  // CXLSent, CancelACK, CancelRejected, CancelPending, CanceledByUser
  strcpy( brokenOrder.status, orderStatus.BrokenTrade );

  // Messsage type
  strcpy( brokenOrder.msgType, "BrokenTrade" );

  // Length
  int fieldLength = GetShortNumber(&dataBlock.msgContent[2]);

  // msgType
  char msgType = dataBlock.msgContent[4];

  // matching unit
  char fieldMatchUnit = dataBlock.msgContent[5];

  // Sequence number
  brokenOrder.seqNum = GetIntNumber(&dataBlock.msgContent[6]);

  // Transaction time
  unsigned long totalnanos = GetLongNumber((char *)&dataBlock.msgContent[10]);

  // Get the number of seconds from Jan 1st 1970
  int numSecs = totalnanos / 1000000000;

  numSecs %= NUMBER_OF_SECONDS_PER_DAY;

  // Get the number of nanoseconds
  int numNanos = totalnanos % 1000000000;

  // write to transtime
  sprintf(brokenOrder.transTime, "%02d:%02d:%02d.%09d", numSecs / 3600, (numSecs % 3600) / 60, (numSecs % 3600) % 60, numNanos);

  // client Order id
  char fieldClOrderID[21];
  strncpy(fieldClOrderID, (char *)&dataBlock.msgContent[18], 20);
  fieldClOrderID[20] = 0;
  brokenOrder.orderID = atoi(fieldClOrderID);

  // BATS BOE order id
  brokenOrder.batszOrderID = GetLongNumber((char *)&dataBlock.msgContent[38]);

  // Execution id
  char execID[32] = "";
  brokenOrder.executionId = GetLongNumber((char *)&dataBlock.msgContent[46]);
  ConvertDecimalToBase36ASCII(brokenOrder.executionId, execID);
  ReserveString(execID);

  // Side
  brokenOrder.side = dataBlock.msgContent[54];

  // Base liquidity indicator
  brokenOrder.baseLiquidity = dataBlock.msgContent[55];

  // clearing firm
  strncpy(brokenOrder.clrFirm, (char *)&dataBlock.msgContent[56], 4);
  brokenOrder.clrFirm[4] = 0;

  // Last shares
  brokenOrder.lastshares = GetIntNumber(&dataBlock.msgContent[64]);

  // Last Px
  brokenOrder.lastPx = GetLongNumber((char *)&dataBlock.msgContent[68]) * 1.00 / 10000;

  // Corrected price
  brokenOrder.correctedPrice = GetLongNumber((char *)&dataBlock.msgContent[76]) * 1.00 / 10000;

  // Original time
  totalnanos = GetLongNumber((char *)&dataBlock.msgContent[84]);
  numSecs = totalnanos / 1000000000;
  numSecs %= NUMBER_OF_SECONDS_PER_DAY;
  numNanos = totalnanos % 1000000000;
  sprintf(brokenOrder.orgTime, "%02d:%02d:%02d.%09d", numSecs / 3600, (numSecs % 3600) / 60, (numSecs % 3600) % 60, numNanos);

  /*----------------------------------------
   * Process optional fields
   * Optional fields start from offset 100...
   *--------------------------------------*/
  int offset = STARTING_OFFSET_OF_BROKEN_TRADE;
  union t_BitField bitField;

  /*------------------------
   *  Trade or cancel order bitFields: 1, 3, 4, 5, 6, 7: use in the future
   *-----------------------*/

  /*------------------------
   *  Trade or cancel order bitField: 2
   *-----------------------*/
  bitField.value = dataBlock.msgContent[93];

  // symbol
  if (bitField.bit.bit1 == 1)
  {
    strncpy(brokenOrder.symbol, (char *)&dataBlock.msgContent[offset], SYMBOL_LEN);
    offset += SYMBOL_LEN;
  }
  else strcpy(brokenOrder.symbol, " ");

  // symbol sfx
  char fieldSymbolSfx[SYMBOL_LEN];
  if (bitField.bit.bit2 == 1)
  {
    strncpy(fieldSymbolSfx, (char *)&dataBlock.msgContent[offset], SYMBOL_LEN);
    offset += SYMBOL_LEN;
    strcat(brokenOrder.symbol, fieldSymbolSfx);
  }
  else strcpy(fieldSymbolSfx, " ");

  //orderDetail.msgContent
  orderDetail.length = sprintf( orderDetail.msgContent,
      "%d=%X\001"   // msgtype
      "%d=%d\001"   // length
      "%d=%c\001"   // matching unit
      "%d=%d\001"   // seqNum
      "%d=%s\001"   // transaction time
      "%d=%d\001"   // clOrderid
      "%d=%ld\001"  // BATS order id
      "%d=%s\001"   // execution id
      "%d=%c\001"   // side
      "%d=%c\001"   // base liquidity
      "%d=%s\001"   // clearing firm
      "%d=%d\001"   // last shares
      "%d=%lf\001"  // last Px
      "%d=%lf\001"  // corrected price
      "%d=%s\001"   // original time
      "%d=%.8s\001"   // symbol
      ,fieldIndexList.MsgType, msgType,
      fieldIndexList.Length, fieldLength,
      fieldIndexList.MatchUnit, fieldMatchUnit,
      fieldIndexList.InSeqNum, brokenOrder.seqNum,
      fieldIndexList.TransactionTime, brokenOrder.transTime,
      fieldIndexList.ClOrdID, brokenOrder.orderID,
      fieldIndexList.ArcaOrderID, brokenOrder.batszOrderID,
      fieldIndexList.ExecID, execID,
      fieldIndexList.Side, brokenOrder.side,
      fieldIndexList.BaseLiquidityIndicator, brokenOrder.baseLiquidity,
      fieldIndexList.ClrFirm, brokenOrder.clrFirm,
      fieldIndexList.LastShares, brokenOrder.lastshares,
      fieldIndexList.LastPx, brokenOrder.lastPx,
      fieldIndexList.Price, brokenOrder.correctedPrice,
      fieldIndexList.OrgTime, brokenOrder.orgTime,
      fieldIndexList.Symbol, brokenOrder.symbol
      );

  strcpy( brokenOrder.msgContent, orderDetail.msgContent );

  // Update database
  int tsId;

  if( type == MANUAL_ORDER )
  {
    tsId = MANUAL_ORDER;
  }
  else
  {
    tsId = tradeServersInfo.config[type].tsId;
  }

  // Call function to update a open order (non fill)
  if (ProcessUpdateASOpenOrderForStatus(tsId, brokenOrder.orderID, orderStatusIndex.BrokenTrade, &orderDetail) == -2)
  {
    AddDataBlockToWaitList(oecType, WAIT_LIST_TYPE_BATS_BUST_OR_CORRECT, brokenOrder.orderID, dataBlock, type, "");
    return 0;
  }

  // Call function to update Order Fill of BATS BOE
  ProcessInsertBustOrCorrect4BATS_BOEQueryToCollection(&brokenOrder);

  //Process add broken trade message to as logs
  int indexOpenOrder = CheckOrderIdIndex(brokenOrder.orderID, tsId);
  if (indexOpenOrder >= 0)
  {
    TraceLog(DEBUG_LEVEL, "%s: Broken Trade message received for symbol %.8s.\n", GetOECName(oecType), asOpenOrder.orderCollection[indexOpenOrder].orderSummary.symbol);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessOrderRestatedOfBATS_BOEOrder
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessOrderRestatedOfBATS_BOEOrder(t_DataBlock dataBlock, int type, char *timeStamp, int oecType)
{
  char temp[21];
  memcpy(temp, &dataBlock.msgContent[18], 20);
  temp[20] = 0;
  int clOrdID = atoi(temp);

  unsigned long orderID;
  memcpy(&orderID, &dataBlock.msgContent[38], 8);

  char restateReason;
  restateReason = dataBlock.msgContent[46];
  
  char restateText[80];
  switch (restateReason)
  {
    case 'R':
      strcpy(restateText, "Reroute");
      break;
    case 'X':
      strcpy(restateText, "Locked in cross");
      break;
    case 'W':
      strcpy(restateText, "Wash");
      break;
    case 'L':
      strcpy(restateText, "Reload");
      break;
    case 'Q':
      strcpy(restateText, "Liquidity Updated");
      break;
    default:
      TraceLog(ERROR_LEVEL, "(BATS_BOE - OrderRestated): Invalid restated reason: %c\n", restateReason);
      return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "BATS-Z BOE - Order Restated: clOrdID = %d, orderID = %ld, restateReason = %s\n", clOrdID, orderID, restateText);

  return SUCCESS;
}

/***************************************************************************/
