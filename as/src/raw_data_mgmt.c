/*****************************************************************************
**  Project:    Equity Arbitrage Application
**  Filename:   raw_data_mgmt.c
**  Description:  This file contains function definitions that were declared
          in raw_data_mgmt.h  
**  Author:     Luan Vo-Kinh
**  First created:  13-Sep-2007
**  Last updated: -----------
*****************************************************************************/

/****************************************************************************
** Include files and define several constants
****************************************************************************/
//#include <stdlib.h>
//#include <sys/types.h>
//#include <sys/stat.h>
//#include <unistd.h>
#include <time.h>

#include "configuration.h"
#include "order_mgmt_proc.h"
#include "arca_direct_data.h"
#include "nasdaq_ouch_data.h"
#include "nasdaq_rash_data.h"
#include "bats_boe_data.h"
#include "raw_data_mgmt.h"
#include "edge_order_data.h"
#include "nyse_ccg_data.h"
#include "trade_servers_proc.h"
#include "query_data_mgmt.h"

unsigned char currentTimestamp[MAX_TIMESTAMP];

/****************************************************************************
** Global variables definition
****************************************************************************/
t_RawDataMgmt tsRawDataMgmt[MAX_TRADE_SERVER_CONNECTIONS][TS_MAX_ORDER_CONNECTIONS];

t_RawDataMgmt asRawDataMgmt[AS_MAX_ORDER_CONNECTIONS];

t_RawDataMgmt pmRawDataMgmt[PM_MAX_ORDER_CONNECTIONS];

t_RawDataProcessedMgmt RawDataProcessedMgmt;

t_HungOrderList HungOrderList;

FILE *WaitListFp;
t_RawdataWaitList RawdataWaitList[MAX_WAIT_LIST_ORDERS];
/****************************************************************************
** Function declarations
****************************************************************************/
/****************************************************************************
- Function name:  InitializeRawDataManegementStructures
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int InitializeRawDataManegementStructures(void)
{
  int tsIndex, orderIndex;

  // Raw data at Trade Servers
  for (tsIndex = 0; tsIndex < MAX_TRADE_SERVER_CONNECTIONS; tsIndex++)
  {
    for (orderIndex = 0; orderIndex < TS_MAX_ORDER_CONNECTIONS; orderIndex++)
    {
      tsRawDataMgmt[tsIndex][orderIndex].fileDesc = NULL;
      tsRawDataMgmt[tsIndex][orderIndex].orderIndex = orderIndex;
      tsRawDataMgmt[tsIndex][orderIndex].tsId = tsIndex;
      tsRawDataMgmt[tsIndex][orderIndex].lastUpdatedIndex = -1;
      tsRawDataMgmt[tsIndex][orderIndex].countRawDataBlock = 0;
      tsRawDataMgmt[tsIndex][orderIndex].isProcessed = 0;
      
      switch (orderIndex)
      {
        case TS_ARCA_DIRECT_INDEX:
          sprintf(tsRawDataMgmt[tsIndex][orderIndex].fileName, "%s_%s_ts%d.%s", RAW_DATA_FILE_PREFIX_DIRECT, TradingAccount.DIRECTLoginTS[tsIndex], tsIndex + 1, RAW_DATA_FILE_EXTENSION_DIRECT);
          break;

        case TS_NASDAQ_OUCH_INDEX:
          sprintf(tsRawDataMgmt[tsIndex][orderIndex].fileName, "%s_%s_ts%d.%s", RAW_DATA_FILE_PREFIX_OUCH, TradingAccount.OUCHLoginTS[tsIndex], tsIndex + 1, RAW_DATA_FILE_EXTENSION_OUCH);
          break;

        case TS_NYSE_CCG_INDEX:
          sprintf(tsRawDataMgmt[tsIndex][orderIndex].fileName, "%s_%s_ts%d.%s", RAW_DATA_FILE_PREFIX_NYSE, TradingAccount.CCGLoginTS[tsIndex], tsIndex + 1, RAW_DATA_FILE_EXTENSION_NYSE);
          break;

        case TS_NASDAQ_RASH_INDEX:
          sprintf(tsRawDataMgmt[tsIndex][orderIndex].fileName, "%s_%s_ts%d.%s", RAW_DATA_FILE_PREFIX_RASH, TradingAccount.RASHLoginTS[tsIndex], tsIndex + 1, RAW_DATA_FILE_EXTENSION_RASH);
          break;

        case TS_BATSZ_BOE_INDEX:
          sprintf(tsRawDataMgmt[tsIndex][orderIndex].fileName, "%s_%s_ts%d.%s", RAW_DATA_FILE_PREFIX_BATSZ, TradingAccount.BATSZBOELoginTS[tsIndex], tsIndex + 1, RAW_DATA_FILE_EXTENSION_BATSZ);
          break;
          
        case TS_EDGX_DIRECT_INDEX:
          sprintf(tsRawDataMgmt[tsIndex][orderIndex].fileName, "%s_%s_ts%d.%s", RAW_DATA_FILE_PREFIX_EDGX, TradingAccount.EDGXLoginTS[tsIndex], tsIndex + 1, RAW_DATA_FILE_EXTENSION_EDGX);
          break;
          
        case TS_EDGA_DIRECT_INDEX:
          sprintf(tsRawDataMgmt[tsIndex][orderIndex].fileName, "%s_%s_ts%d.%s", RAW_DATA_FILE_PREFIX_EDGA, TradingAccount.EDGALoginTS[tsIndex], tsIndex + 1, RAW_DATA_FILE_EXTENSION_EDGA);
          break;
          
        case TS_NDAQ_OUBX_INDEX:
          sprintf(tsRawDataMgmt[tsIndex][orderIndex].fileName, "%s_%s_ts%d.%s", RAW_DATA_FILE_PREFIX_OUBX, TradingAccount.OUBXLoginTS[tsIndex], tsIndex + 1, RAW_DATA_FILE_EXTENSION_OUBX);
          break;
          
        case TS_BYX_BOE_INDEX:
          sprintf(tsRawDataMgmt[tsIndex][orderIndex].fileName, "%s_%s_ts%d.%s", RAW_DATA_FILE_PREFIX_BYX, TradingAccount.BYXBOELoginTS[tsIndex], tsIndex + 1, RAW_DATA_FILE_EXTENSION_BYX);
          break;
          
        case TS_PSX_OUCH_INDEX:
          sprintf(tsRawDataMgmt[tsIndex][orderIndex].fileName, "%s_%s_ts%d.%s", RAW_DATA_FILE_PREFIX_PSX, TradingAccount.PSXLoginTS[tsIndex], tsIndex + 1, RAW_DATA_FILE_EXTENSION_PSX);
          break;
          
        default:
          TraceLog(ERROR_LEVEL, "(InitializeRawDataManegementStructures): Invalid order index (%d)\n", orderIndex);
          break;
      }

      pthread_mutex_init(&tsRawDataMgmt[tsIndex][orderIndex].updateMutex, NULL);

      memset(&tsRawDataMgmt[tsIndex][orderIndex].dataBlockCollection, 0, MAX_RAW_DATA_ENTRIES * sizeof(t_DataBlock));
    }
  }

  // Raw data at Aggregration Server
  for (orderIndex = 0; orderIndex < AS_MAX_ORDER_CONNECTIONS; orderIndex++)
  {
    asRawDataMgmt[orderIndex].fileDesc = NULL;
    asRawDataMgmt[orderIndex].orderIndex = orderIndex;
    asRawDataMgmt[orderIndex].tsId = MANUAL_ORDER;
    asRawDataMgmt[orderIndex].lastUpdatedIndex = -1;
    asRawDataMgmt[orderIndex].countRawDataBlock = 0;
    asRawDataMgmt[orderIndex].isProcessed = 0;
    
    if (orderIndex == AS_ARCA_DIRECT_INDEX)
    {
      sprintf(asRawDataMgmt[orderIndex].fileName, "%s_%s_as.%s", RAW_DATA_FILE_PREFIX_DIRECT, TradingAccount.ARCADIRECTLoginAS, RAW_DATA_FILE_EXTENSION_DIRECT);
    }
    else  // RASH rawdata
    {
      sprintf(asRawDataMgmt[orderIndex].fileName, "%s_%s_as.%s", RAW_DATA_FILE_PREFIX_RASH, TradingAccount.RASHLoginAS, RAW_DATA_FILE_EXTENSION_RASH);
    }
    pthread_mutex_init(&asRawDataMgmt[orderIndex].updateMutex, NULL);

    memset(&asRawDataMgmt[orderIndex].dataBlockCollection, 0, MAX_RAW_DATA_ENTRIES * sizeof(t_DataBlock));
  }

  // Raw data at Position Manager
  for (orderIndex = 0; orderIndex < PM_MAX_ORDER_CONNECTIONS; orderIndex++)
  {
    pmRawDataMgmt[orderIndex].fileDesc = NULL;
    pmRawDataMgmt[orderIndex].orderIndex = orderIndex;
    pmRawDataMgmt[orderIndex].tsId = MANUAL_ORDER;
    pmRawDataMgmt[orderIndex].lastUpdatedIndex = -1;
    pmRawDataMgmt[orderIndex].countRawDataBlock = 0;
    pmRawDataMgmt[orderIndex].isProcessed = 0;
    
    if (orderIndex == PM_ARCA_DIRECT_INDEX)
    {
      sprintf(pmRawDataMgmt[orderIndex].fileName, "%s_%s_pm.%s", RAW_DATA_FILE_PREFIX_DIRECT, TradingAccount.ARCADIRECTLoginPM, RAW_DATA_FILE_EXTENSION_DIRECT);
    }
    else  // RASH rawdata
    {
      sprintf(pmRawDataMgmt[orderIndex].fileName, "%s_%s_pm.%s", RAW_DATA_FILE_PREFIX_RASH, TradingAccount.RASHLoginPM, RAW_DATA_FILE_EXTENSION_RASH);
    }

    pthread_mutex_init(&pmRawDataMgmt[orderIndex].updateMutex, NULL);

    memset(&pmRawDataMgmt[orderIndex].dataBlockCollection, 0, MAX_RAW_DATA_ENTRIES * sizeof(t_DataBlock));
  }

  // Initialize position raw data processed structure
  InitializePositionRawDataProcessedStructure();

  return 0;
}

/****************************************************************************
- Function name:  InitializePositionRawDataProcessedStructure
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int InitializePositionRawDataProcessedStructure(void)
{
  RawDataProcessedMgmt.fileDesc = NULL;
  memset(RawDataProcessedMgmt.tsDataBlock, 0, sizeof(RawDataProcessedMgmt.tsDataBlock));
  memset(RawDataProcessedMgmt.asDataBlock, 0, sizeof(RawDataProcessedMgmt.asDataBlock));
  memset(RawDataProcessedMgmt.pmDataBlock, 0, sizeof(RawDataProcessedMgmt.pmDataBlock));

  return SUCCESS;
}

/****************************************************************************
- Function name:  GetNumberOfRawDataEntries
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int GetNumberOfRawDataEntries(void)
{
  int tsIndex, orderIndex;

  // Raw data at Trade Servers
  for (tsIndex = 0; tsIndex < tradeServersInfo.maxConnections; tsIndex++)
  {
    for (orderIndex = 0; orderIndex < TS_MAX_ORDER_CONNECTIONS; orderIndex++)
    {
      if (GetNumberOfRawDataEntriesFromFile(tsRawDataMgmt[tsIndex][orderIndex].fileName, &tsRawDataMgmt[tsIndex][orderIndex]) == ERROR)
      {
        return ERROR;
      }
    }
  }

  // Raw data at Aggregration Server
  for (orderIndex = 0; orderIndex < AS_MAX_ORDER_CONNECTIONS; orderIndex++)
  {
    if (GetNumberOfRawDataEntriesFromFile(asRawDataMgmt[orderIndex].fileName, &asRawDataMgmt[orderIndex]) == ERROR)
    {
      return ERROR;
    }
  }

  // Raw data at PM
  for (orderIndex = 0; orderIndex < PM_MAX_ORDER_CONNECTIONS; orderIndex++)
  {
    if (GetNumberOfRawDataEntriesFromFile(pmRawDataMgmt[orderIndex].fileName, &pmRawDataMgmt[orderIndex]) == ERROR)
    {
      return ERROR;
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  GetNumberOfRawDataEntriesFromFile
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int GetNumberOfRawDataEntriesFromFile(const char *fileName, void *orderRawDataMgmt)
{
  t_RawDataMgmt *workingOrderMgmt = (t_RawDataMgmt *)orderRawDataMgmt;
  
  // Get full raw data path
  char fullPath[MAX_PATH_LEN] = "\0";
  
  if (GetFullRawDataPath(fileName, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Have some problem in get full raw data path operation (%s)\n", fileName);

    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Load raw data: %s\n", fullPath);

  if (IsFileTimestampToday(fullPath) == 0)
  {
    TraceLog(ERROR_LEVEL, "%s is old, please check the correctness of data files\n", fullPath);
    exit(0);
  }
  
  /* 
  Open file to read in BINARY MODE, 
  if it is not existed, we will create it
  */
  FILE *fileDesc = fopen(fullPath, "ab+");
  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Cannot open file '%s' to read or we don't have permission to create\n", fullPath);

    return ERROR;
  }
  else
  {
    // Store pointer to file that will be used after
    workingOrderMgmt->fileDesc = fileDesc;
    
    struct stat fileStat;
    int retValue = stat(fullPath, &fileStat);
    
    if (retValue == SUCCESS)
    {
      // Get file size
      int fileSizeInBytes = fileStat.st_size;
      
      // Check file size
      if (fileSizeInBytes % sizeof(t_DataBlock) != 0)
      {
        TraceLog(ERROR_LEVEL, "INVALID FILE SIZE: fileName = %s, fileSize = %d\n", fullPath, fileSizeInBytes);

        return ERROR;
      }

      workingOrderMgmt->countRawDataBlock = fileSizeInBytes / sizeof(t_DataBlock);

      TraceLog(DEBUG_LEVEL, "Count raw data entries: %d\n", workingOrderMgmt->countRawDataBlock);
    }
    else
    {
      return ERROR;
    }
  } 
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadPositionRawDataProcessedFromFile
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int LoadPositionRawDataProcessedFromFile(const char *fileName)
{
  FILE *fileRead, *fileWrite; 

  // Get full raw data path
  char fullPath[MAX_PATH_LEN] = "\0";
  
  if (GetFullRawDataPath(fileName, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Have some problem in get full raw data path operation (%s)\n", fileName);

    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Load position raw data processed: %s\n", fullPath);

  if (IsFileTimestampToday(fullPath) == 0)
  {
    TraceLog(ERROR_LEVEL, "%s is old, please check the correctness of data files\n", fullPath);
    exit(0);
  }
  
  struct stat fileStat;
  int retValue = stat(fullPath, &fileStat);
  int fileSizeInBytes = 0;

  if (retValue == SUCCESS)
  {
    // Get file size
    fileSizeInBytes = fileStat.st_size;

    // Check file size
    if (fileSizeInBytes == 0)
    {
      /* 
      Open file to read in BINARY MODE, 
      if it is not existed, we will create it
      */
      fileWrite = fopen(fullPath, "w+");
      if (fileWrite == NULL)
      {
        TraceLog(ERROR_LEVEL, "Cannot open file '%s' to read or we don't have permission to create\n", fullPath);

        return ERROR;
      }
      else
      {
        // Store pointer to file that will be used after
        RawDataProcessedMgmt.fileDesc = fileWrite;
        
        return SUCCESS;
      }

      return SUCCESS;
    }
    else if (fileSizeInBytes > 0)
    {
      if ((sizeof(RawDataProcessedMgmt.tsDataBlock) + sizeof(RawDataProcessedMgmt.asDataBlock) + sizeof(RawDataProcessedMgmt.pmDataBlock) - fileSizeInBytes) % 4 != 0)
      {
        TraceLog(ERROR_LEVEL, "INVALID FILE SIZE: %s\n", fullPath);

        return ERROR;
      }     
    }
  }

  int i, j;

  TraceLog(DEBUG_LEVEL, "Read index porcessed raw data (%d bytes) ...\n", fileSizeInBytes);

  /* 
  Open file to read in BINARY MODE, 
  if it is not existed, we will create it
  */
  fileRead = fopen(fullPath, "r+");
  if (fileRead != NULL)
  {
    if (fread(RawDataProcessedMgmt.tsDataBlock, sizeof(RawDataProcessedMgmt.tsDataBlock), 1, fileRead) != 1)
    {
      TraceLog(ERROR_LEVEL, "Cannot load count block data processed of Trade Server: %s\n", fullPath);

      return ERROR;
    }

    i = 0;

    while (i < ((fileSizeInBytes - sizeof(RawDataProcessedMgmt.tsDataBlock)) / 4))
    {
      if (fread(&RawDataProcessedMgmt.asDataBlock[i], 4, 1, fileRead) != 1)
      {
        TraceLog(ERROR_LEVEL, "Cannot load count block data processed of Aggregation Server: %s\n", fullPath);

        return ERROR;
      }

      i++;

      if (i == AS_MAX_ORDER_CONNECTIONS)
      {
        break;
      }
    }

    i = 0;

    while (i < ((fileSizeInBytes - sizeof(RawDataProcessedMgmt.tsDataBlock) - sizeof(RawDataProcessedMgmt.asDataBlock)) / 4))
    {
      if (fread(&RawDataProcessedMgmt.pmDataBlock[i], 4, 1, fileRead) != 1)
      {
        TraceLog(ERROR_LEVEL, "Cannot load count block data processed of PM: %s\n", fullPath);

        return ERROR;
      }

      i++;

      if (i == PM_MAX_ORDER_CONNECTIONS)
      {
        break;
      }
    }

    /*
    if (fread(RawDataProcessedMgmt.asDataBlock, sizeof(RawDataProcessedMgmt.asDataBlock), 1, fileRead) != 1)
    {
      TraceLog(DEBUG_LEVEL, "Cannot load count block data processed of Aggregation Server: %s\n", fullPath);

      return ERROR;
    }
    */

    TraceLog(DEBUG_LEVEL, "AS raw data:\n");

    for (i = 0; i < AS_MAX_ORDER_CONNECTIONS; i++)
    {
      TraceLog(DEBUG_LEVEL, "\tECN: %d, processed index: %d\n", i, RawDataProcessedMgmt.asDataBlock[i]);
    }

    TraceLog(DEBUG_LEVEL, "PM raw data:\n");

    for (i = 0; i < PM_MAX_ORDER_CONNECTIONS; i++)
    {
      TraceLog(DEBUG_LEVEL, "\tECN: %d, processed index: %d\n", i, RawDataProcessedMgmt.pmDataBlock[i]);
    }

    for (j = 0; j < MAX_TRADE_SERVER_CONNECTIONS; j++)
    {
      TraceLog(DEBUG_LEVEL, "%s raw data:\n", tradeServersInfo.config[j].description);

      for (i = 0; i < TS_MAX_ORDER_CONNECTIONS; i++)
      {
        TraceLog(DEBUG_LEVEL, "\tECN: %d, processed index: %d\n", i, RawDataProcessedMgmt.tsDataBlock[j][i]);
      }
    }

    // Close the file
    fclose(fileRead);
  }
  
  /* 
  Open file to read in BINARY MODE, 
  if it is not existed, we will create it
  */
  fileWrite = fopen(fullPath, "w+");
  if (fileWrite == NULL)
  {
    TraceLog(ERROR_LEVEL, "Cannot open file '%s' to read or we don't have permission to create\n", fullPath);

    return ERROR;
  }
  else
  {
    // Store pointer to file that will be used after
    RawDataProcessedMgmt.fileDesc = fileWrite;

    // Save position raw data processed
    SavePositionRawDataProcessedToFile();
    
    return SUCCESS;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SavePositionRawDataProcessedToFile
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SavePositionRawDataProcessedToFile(void)
{ 
  if (RawDataProcessedMgmt.fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "(%s) Cannot open file: %s\n", __func__, FILE_NAME_OF_RAW_DATA_PROCESSED);

    return ERROR;
  }

  rewind(RawDataProcessedMgmt.fileDesc);

  if (fwrite(RawDataProcessedMgmt.tsDataBlock, sizeof(RawDataProcessedMgmt.tsDataBlock), 1, RawDataProcessedMgmt.fileDesc) != 1)
  {
    TraceLog(ERROR_LEVEL, "Cannot save count block data processed of Trade Server to file: %s\n", FILE_NAME_OF_RAW_DATA_PROCESSED);

    return ERROR;
  }

  if (fwrite(RawDataProcessedMgmt.asDataBlock, sizeof(RawDataProcessedMgmt.asDataBlock), 1, RawDataProcessedMgmt.fileDesc) != 1)
  {
    TraceLog(ERROR_LEVEL, "Cannot save count block data processed of Aggregation Server to file: %s\n", FILE_NAME_OF_RAW_DATA_PROCESSED);

    return ERROR;
  }

  if (fwrite(RawDataProcessedMgmt.pmDataBlock, sizeof(RawDataProcessedMgmt.pmDataBlock), 1, RawDataProcessedMgmt.fileDesc) != 1)
  {
    TraceLog(ERROR_LEVEL, "Cannot save count block data processed of PM to file: %s\n", FILE_NAME_OF_RAW_DATA_PROCESSED);

    return ERROR;
  }

  // Flush all data from buffer to file
  fflush(RawDataProcessedMgmt.fileDesc);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessHistoryRawDataOfTradeServer
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessHistoryRawDataOfTradeServer(void)
{
  TraceLog(DEBUG_LEVEL, "Process history raw data of Trade Server ...\n");

  int tsIndex, orderIndex;

  // Process raw data of Trade Server from file
  for( tsIndex = 0; tsIndex < tradeServersInfo.maxConnections; tsIndex++ )
  {
    for( orderIndex = 0; orderIndex < TS_MAX_ORDER_CONNECTIONS; orderIndex++ )
    {
      if( RawDataProcessedMgmt.tsDataBlock[tsIndex][orderIndex] < tsRawDataMgmt[tsIndex][orderIndex].countRawDataBlock )
      {
        ProcessRawDataOfTradeServerFromFile(&RawDataProcessedMgmt.tsDataBlock[tsIndex][orderIndex], &tsRawDataMgmt[tsIndex][orderIndex] );        
      }
      
      //TraceLog(DEBUG_LEVEL,  "\nFinished processing raw data of Trade Server %d, ECN %d ...\n", tsIndex+1, orderIndex+1 );
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessRawDataOfTradeServerFromFile
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessRawDataOfTradeServerFromFile( int *countRawDataProcessed, void *rawDataMgmt )
{
  //TraceLog(DEBUG_LEVEL, "\nProcess raw data of Trade Server from file ...\n");

  t_RawDataMgmt *workingOrderMgmt = ( t_RawDataMgmt * )rawDataMgmt;

  int i, retValue;
  t_DataBlock dataBlock;
  
  // Number of data blocks need process
  int numDataBlocks = workingOrderMgmt->countRawDataBlock - (*countRawDataProcessed);

  TraceLog(DEBUG_LEVEL, "Rawdata (%s, ECN: %d) processing: %d/%d\n", tradeServersInfo.config[workingOrderMgmt->tsId].description, workingOrderMgmt->orderIndex, *countRawDataProcessed, workingOrderMgmt->countRawDataBlock);

  // Set cursor position for file at begin of specific data block
  fseek( workingOrderMgmt->fileDesc, (*countRawDataProcessed) * sizeof(t_DataBlock), SEEK_SET); 

  for( i = 0; i <= numDataBlocks; i++ )
  {
    retValue = fread(&dataBlock, sizeof(t_DataBlock), 1, workingOrderMgmt->fileDesc);
    
    if(retValue != 1)
    {
      return ERROR;
    }

    switch ( workingOrderMgmt->orderIndex )
    {
      case TS_ARCA_DIRECT_INDEX:
        ParseRawDataForARCA_DIRECTOrder( dataBlock, workingOrderMgmt->tsId );
        break;
        
      case TS_NASDAQ_OUCH_INDEX:
        ParseRawDataForOUCHOrder(dataBlock, workingOrderMgmt->tsId, TYPE_NASDAQ_OUCH);
        break;
      
      case TS_NDAQ_OUBX_INDEX:
        ParseRawDataForOUCHOrder(dataBlock, workingOrderMgmt->tsId, TYPE_NASDAQ_OUBX);
        break;
        
      case TS_PSX_OUCH_INDEX:
        ParseRawDataForOUCHOrder(dataBlock, workingOrderMgmt->tsId, TYPE_PSX);
        break;
      
      case TS_NASDAQ_RASH_INDEX:
        ParseRawDataForRASHOrder( dataBlock, workingOrderMgmt->tsId );
        break;
        
      case TS_BATSZ_BOE_INDEX:
        ParseRawDataForBATS_BOEOrder(dataBlock, workingOrderMgmt->tsId, TYPE_BZX_BOE);
        break;
        
      case TS_BYX_BOE_INDEX:
        ParseRawDataForBATS_BOEOrder(dataBlock, workingOrderMgmt->tsId, TYPE_BYX_BOE);
        break;

      case TS_NYSE_CCG_INDEX:
        ParseRawDataForNYSE_CCGOrder( dataBlock, workingOrderMgmt->tsId );
        break;
        
      case TS_EDGX_DIRECT_INDEX:
        ParseRawDataForEDGEOrder(dataBlock, workingOrderMgmt->tsId, TYPE_EDGX_DIRECT);
        break;
        
      case TS_EDGA_DIRECT_INDEX:
        ParseRawDataForEDGEOrder(dataBlock, workingOrderMgmt->tsId, TYPE_EDGA_DIRECT);
        break;
        
      default:
        TraceLog(ERROR_LEVEL, "%s: INVALID ECN ORDER, orderIndex = %d\n", __func__, workingOrderMgmt->orderIndex );
        break;
    }
  
    // Increase number of data blocks process
    (*countRawDataProcessed) ++;

    // Save position raw data processed
    SavePositionRawDataProcessedToFile();
  } 

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessHistoryRawDataOfAggregationServer
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessHistoryRawDataOfAggregationServer( void )
{
  TraceLog(DEBUG_LEVEL, "Process history raw data of Aggregation Server ...\n" );

  int orderIndex;
  
  for( orderIndex = 0; orderIndex < AS_MAX_ORDER_CONNECTIONS; orderIndex++ )
  {
    // Process raw data of Aggregation Server from file
    if( RawDataProcessedMgmt.asDataBlock[orderIndex] < asRawDataMgmt[orderIndex].countRawDataBlock )
    {
      ProcessRawDataOfAggregationServerFromFile( &RawDataProcessedMgmt.asDataBlock[orderIndex], &asRawDataMgmt[orderIndex] );
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessRawDataOfAggregationServerFromFile
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessRawDataOfAggregationServerFromFile( int *countRawDataProcessed, void *rawDataMgmt )
{
  //TraceLog(DEBUG_LEVEL, "\nProcess raw data of Aggregation Server from file ...\n");

  t_RawDataMgmt *workingOrderMgmt = (t_RawDataMgmt *)rawDataMgmt;

  int i, retValue;
  t_DataBlock dataBlock;

  // Number of data blocks need process
  int numDataBlocks = workingOrderMgmt->countRawDataBlock - (*countRawDataProcessed);

  TraceLog(DEBUG_LEVEL, "Rawdata (AS, ECN: %d) processing: %d/%d\n", workingOrderMgmt->orderIndex, *countRawDataProcessed, workingOrderMgmt->countRawDataBlock);

  // Set cursor position for file at begin of specific data block
  fseek(workingOrderMgmt->fileDesc, (*countRawDataProcessed) * sizeof(t_DataBlock), SEEK_SET);  

  for( i = 0; i <= numDataBlocks; i++ )
  {
    retValue = fread(&dataBlock, sizeof( t_DataBlock ), 1, workingOrderMgmt->fileDesc);
      
    if( retValue != 1 )
    {
      return ERROR;
    }
    
    switch ( workingOrderMgmt->orderIndex )
    {
      case AS_NASDAQ_RASH_INDEX:
        ParseRawDataForRASHOrder( dataBlock, workingOrderMgmt->tsId );
        break;
        
      case AS_ARCA_DIRECT_INDEX:
        ParseRawDataForARCA_DIRECTOrder( dataBlock, workingOrderMgmt->tsId );
        break;
        
      default:
        TraceLog(ERROR_LEVEL, "ProcessRawDataOfAggregationServerFromFile: INVALID ECN ORDER, orderIndex = %d\n", workingOrderMgmt->orderIndex );
        break;
    }
        
    // Increase number of data blocks process
    (*countRawDataProcessed) ++;

    // Save position raw data processed
    SavePositionRawDataProcessedToFile();
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessRawDataOfTradeServer
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessRawDataOfTradeServer(void)
{
  //TraceLog(DEBUG_LEVEL, "Process raw data of Trade Server ...\n");

  int tsIndex, orderIndex;

  // Process raw data of Trade Server from file
  for( tsIndex = 0; tsIndex < tradeServersInfo.maxConnections; tsIndex++ )
  {
    for( orderIndex = 0; orderIndex < TS_MAX_ORDER_CONNECTIONS; orderIndex++ )
    {
      if( RawDataProcessedMgmt.tsDataBlock[tsIndex][orderIndex] < tsRawDataMgmt[tsIndex][orderIndex].countRawDataBlock )
      {
        if (ProcessRawDataOfTradeServerFromCollection( &RawDataProcessedMgmt.tsDataBlock[tsIndex][orderIndex], &tsRawDataMgmt[tsIndex][orderIndex]) == ERROR_BUFFER_OVER)
        {
          return ERROR;
        }
      }
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessRawDataOfTradeServerFromCollection
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessRawDataOfTradeServerFromCollection(int *countRawDataProcessed, void *rawDataMgmt)
{
  //TraceLog(DEBUG_LEVEL, "\nProcess raw data of Trade Server from collection ...\n");
  
  t_RawDataMgmt *workingOrderMgmt = (t_RawDataMgmt *)rawDataMgmt;
  
  int numDataBlocks, startIndex, i; 

  pthread_mutex_lock(&workingOrderMgmt->updateMutex);

  // Number of data blocks need process
  numDataBlocks = workingOrderMgmt->countRawDataBlock - (*countRawDataProcessed);

  // Raw data index will start process
  startIndex = workingOrderMgmt->lastUpdatedIndex - numDataBlocks + 1;

  pthread_mutex_unlock(&workingOrderMgmt->updateMutex);
  
  TraceLog(DEBUG_LEVEL, "Rawdata (%s, ECN: %d) processing: %d/%d\n", tradeServersInfo.config[workingOrderMgmt->tsId].description, workingOrderMgmt->orderIndex, *countRawDataProcessed, workingOrderMgmt->countRawDataBlock);
  
  // Check collection is full
  if (numDataBlocks > MAX_RAW_DATA_ENTRIES)
  {
    TraceLog(ERROR_LEVEL, "%s, data block collection is full: countRawDataBlock = %d, countRawDataProcessed = %d\n", tradeServersInfo.config[workingOrderMgmt->tsId].description, workingOrderMgmt->countRawDataBlock, *countRawDataProcessed);

    //  Add code here to process this problem

    return ERROR_BUFFER_OVER;
  }

  for (i = 0; i < numDataBlocks; i++)
  {
    switch (workingOrderMgmt->orderIndex)
    {
      case TS_ARCA_DIRECT_INDEX:
        ParseRawDataForARCA_DIRECTOrder(workingOrderMgmt->dataBlockCollection[(startIndex + i) % MAX_RAW_DATA_ENTRIES], workingOrderMgmt->tsId);
        break;
      
      case TS_NASDAQ_OUCH_INDEX:
        ParseRawDataForOUCHOrder(workingOrderMgmt->dataBlockCollection[(startIndex + i) % MAX_RAW_DATA_ENTRIES], workingOrderMgmt->tsId, TYPE_NASDAQ_OUCH);
        break;
      
      case TS_NDAQ_OUBX_INDEX:
        ParseRawDataForOUCHOrder(workingOrderMgmt->dataBlockCollection[(startIndex + i) % MAX_RAW_DATA_ENTRIES], workingOrderMgmt->tsId, TYPE_NASDAQ_OUBX);
        break;
        
      case TS_PSX_OUCH_INDEX:
        ParseRawDataForOUCHOrder(workingOrderMgmt->dataBlockCollection[(startIndex + i) % MAX_RAW_DATA_ENTRIES], workingOrderMgmt->tsId, TYPE_PSX);
        break;
      
      case TS_NASDAQ_RASH_INDEX:
        ParseRawDataForRASHOrder(workingOrderMgmt->dataBlockCollection[(startIndex + i) % MAX_RAW_DATA_ENTRIES], workingOrderMgmt->tsId);
        break;
        
      case TS_BATSZ_BOE_INDEX:
        ParseRawDataForBATS_BOEOrder(workingOrderMgmt->dataBlockCollection[(startIndex + i) % MAX_RAW_DATA_ENTRIES], workingOrderMgmt->tsId, TYPE_BZX_BOE);
        break;
        
      case TS_BYX_BOE_INDEX:
        ParseRawDataForBATS_BOEOrder(workingOrderMgmt->dataBlockCollection[(startIndex + i) % MAX_RAW_DATA_ENTRIES], workingOrderMgmt->tsId, TYPE_BYX_BOE);
        break;

      case TS_NYSE_CCG_INDEX:
        ParseRawDataForNYSE_CCGOrder(workingOrderMgmt->dataBlockCollection[(startIndex + i) % MAX_RAW_DATA_ENTRIES], workingOrderMgmt->tsId);
        break;
        
      case TS_EDGX_DIRECT_INDEX:
        ParseRawDataForEDGEOrder(workingOrderMgmt->dataBlockCollection[(startIndex + i) % MAX_RAW_DATA_ENTRIES], workingOrderMgmt->tsId, TYPE_EDGX_DIRECT);
        break;
        
      case TS_EDGA_DIRECT_INDEX:
        ParseRawDataForEDGEOrder(workingOrderMgmt->dataBlockCollection[(startIndex + i) % MAX_RAW_DATA_ENTRIES], workingOrderMgmt->tsId, TYPE_EDGA_DIRECT);
        break;
        
      default:
        TraceLog(ERROR_LEVEL, "%s: INVALID ECN ORDER, orderIndex = %d\n", __func__, workingOrderMgmt->orderIndex);
        break;
    }

    // Increase number of data blocks process
    (*countRawDataProcessed) ++;

    // Save position raw data processed
    SavePositionRawDataProcessedToFile();
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessRawDataOfAggregationServer
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessRawDataOfAggregationServer(void)
{
  //TraceLog(DEBUG_LEVEL, "Process raw data of Aggregation Server ...\n");

  int orderIndex;

  for (orderIndex = 0; orderIndex < AS_MAX_ORDER_CONNECTIONS; orderIndex++)
  {
    // Process raw data of Aggregation Server from file
    if (RawDataProcessedMgmt.asDataBlock[orderIndex] < asRawDataMgmt[orderIndex].countRawDataBlock)
    {
      if (ProcessRawDataOfAggregationServerFromCollection(&RawDataProcessedMgmt.asDataBlock[orderIndex], &asRawDataMgmt[orderIndex]) == ERROR_BUFFER_OVER)
      {
        return ERROR;
      }
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessRawDataOfAggregationServerFromCollection
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessRawDataOfAggregationServerFromCollection(int *countRawDataProcessed, void *rawDataMgmt)
{
  //TraceLog(DEBUG_LEVEL, "\nProcess raw data of Aggregation Server from collection ...\n");
  
  t_RawDataMgmt *workingOrderMgmt = (t_RawDataMgmt *)rawDataMgmt;
  
  int numDataBlocks, startIndex, i; 

  pthread_mutex_lock(&workingOrderMgmt->updateMutex);

  // Number of data blocks need process
  numDataBlocks = workingOrderMgmt->countRawDataBlock - (*countRawDataProcessed);

  // Raw data index will start process
  startIndex = workingOrderMgmt->lastUpdatedIndex - numDataBlocks + 1;

  pthread_mutex_unlock(&workingOrderMgmt->updateMutex);

  TraceLog(DEBUG_LEVEL, "Rawdata (AS, ECN: %d) processing: %d/%d\n", workingOrderMgmt->orderIndex, *countRawDataProcessed, workingOrderMgmt->countRawDataBlock);

  // Check collection is full
  if (numDataBlocks > MAX_RAW_DATA_ENTRIES)
  {
    TraceLog(ERROR_LEVEL, "Data block collection is full: countRawDataBlock = %d, countRawDataProcessed = %d\n", workingOrderMgmt->countRawDataBlock, *countRawDataProcessed);

    //  Add code here to process this problem

    return ERROR_BUFFER_OVER;
  }

  for (i = 0; i < numDataBlocks; i++)
  {
    switch (workingOrderMgmt->orderIndex)
    {
      case AS_NASDAQ_RASH_INDEX:
        ParseRawDataForRASHOrder(workingOrderMgmt->dataBlockCollection[(startIndex + i) % MAX_RAW_DATA_ENTRIES], workingOrderMgmt->tsId);
        break;
      
      case AS_ARCA_DIRECT_INDEX:
        ParseRawDataForARCA_DIRECTOrder(workingOrderMgmt->dataBlockCollection[(startIndex + i) % MAX_RAW_DATA_ENTRIES], workingOrderMgmt->tsId);
        break;

      default:
        TraceLog(ERROR_LEVEL, "%s: INVALID ECN ORDER, orderIndex = %d\n", __func__, workingOrderMgmt->orderIndex);
        break;
    }

    // Increase number of data blocks process
    (*countRawDataProcessed) ++;

    // Save position raw data processed
    SavePositionRawDataProcessedToFile();
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessHistoryRawDataOfPM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessHistoryRawDataOfPM( void )
{
  TraceLog(DEBUG_LEVEL, "Process history raw data of PM ...\n" );

  int orderIndex;

  for( orderIndex = 0; orderIndex < PM_MAX_ORDER_CONNECTIONS; orderIndex++ )
  {
    // Process raw data of PM from file
    if( RawDataProcessedMgmt.pmDataBlock[orderIndex] < pmRawDataMgmt[orderIndex].countRawDataBlock )
    {
      ProcessRawDataOfPMFromFile( &RawDataProcessedMgmt.pmDataBlock[orderIndex], &pmRawDataMgmt[orderIndex] );
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessRawDataOfPMFromFile
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessRawDataOfPMFromFile( int *countRawDataProcessed, void *rawDataMgmt )
{
  //TraceLog(DEBUG_LEVEL, "\nProcess raw data of PM from file ...\n");

  t_RawDataMgmt *workingOrderMgmt = (t_RawDataMgmt *)rawDataMgmt;

  int i, retValue;
  t_DataBlock dataBlock;

  // Number of data blocks need process
  int numDataBlocks = workingOrderMgmt->countRawDataBlock - (*countRawDataProcessed);

  TraceLog(DEBUG_LEVEL, "Rawdata (PM, ECN: %d) processing: %d/%d\n", workingOrderMgmt->orderIndex, *countRawDataProcessed, workingOrderMgmt->countRawDataBlock);

  // Set cursor position for file at begin of specific data block
  fseek(workingOrderMgmt->fileDesc, (*countRawDataProcessed) * sizeof(t_DataBlock), SEEK_SET);  

  for( i = 0; i <= numDataBlocks; i++ )
  {
    retValue = fread(&dataBlock, sizeof( t_DataBlock ), 1, workingOrderMgmt->fileDesc);
      
    if( retValue != 1 )
    {
      return ERROR;
    }

    int isManualOrderMarking = 0;
    memcpy(&isManualOrderMarking, &dataBlock.addContent[DATABLOCK_OFFSET_CROSS_ID], 4);
    if (isManualOrderMarking != MANUAL_ORDER_MAKING)
    {
      //This is not manual order
      //PM will process it

      switch ( workingOrderMgmt->orderIndex )
      {
        case PM_NASDAQ_RASH_INDEX:
          ParseRawDataForRASHOrder( dataBlock, workingOrderMgmt->tsId );
          break;
        
        case PM_ARCA_DIRECT_INDEX:
          ParseRawDataForARCA_DIRECTOrder( dataBlock, workingOrderMgmt->tsId );
          break;
          
        default:
          TraceLog(ERROR_LEVEL, "ProcessRawDataOfPMFromFile: INVALID ECN ORDER, orderIndex = %d\n", workingOrderMgmt->orderIndex );
          break;
      }
    }
    
    // Increase number of data blocks process
    (*countRawDataProcessed) ++;

    // Save position raw data processed
    SavePositionRawDataProcessedToFile();
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessRawDataOfPM
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessRawDataOfPM(void)
{
  //TraceLog(DEBUG_LEVEL, "Process raw data of PM ...\n");

  int orderIndex;

  for (orderIndex = 0; orderIndex < PM_MAX_ORDER_CONNECTIONS; orderIndex++)
  {
    // Process raw data of PM from file
    if (RawDataProcessedMgmt.pmDataBlock[orderIndex] < pmRawDataMgmt[orderIndex].countRawDataBlock)
    {
      if (ProcessRawDataOfPMFromCollection(&RawDataProcessedMgmt.pmDataBlock[orderIndex], &pmRawDataMgmt[orderIndex]) == ERROR_BUFFER_OVER)
      {
        return ERROR;
      }
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessRawDataOfPMFromCollection
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessRawDataOfPMFromCollection(int *countRawDataProcessed, void *rawDataMgmt)
{
  //TraceLog(DEBUG_LEVEL, "\nProcess raw data of PM from collection ...\n");
  
  t_RawDataMgmt *workingOrderMgmt = (t_RawDataMgmt *)rawDataMgmt;
  
  int numDataBlocks, startIndex, i; 
  t_DataBlock dataBlock;
  
  pthread_mutex_lock(&workingOrderMgmt->updateMutex);

  // Number of data blocks need process
  numDataBlocks = workingOrderMgmt->countRawDataBlock - (*countRawDataProcessed);

  // Raw data index will start process
  startIndex = workingOrderMgmt->lastUpdatedIndex - numDataBlocks + 1;

  pthread_mutex_unlock(&workingOrderMgmt->updateMutex);

  TraceLog(DEBUG_LEVEL, "Rawdata (PM, ECN: %d) processing: %d/%d\n", workingOrderMgmt->orderIndex, *countRawDataProcessed, workingOrderMgmt->countRawDataBlock);

  // Check collection is full
  if (numDataBlocks > MAX_RAW_DATA_ENTRIES)
  {
    TraceLog(ERROR_LEVEL, "Data block collection is full: countRawDataBlock = %d, countRawDataProcessed = %d\n", workingOrderMgmt->countRawDataBlock, *countRawDataProcessed);

    //  Add code here to process this problem

    return ERROR_BUFFER_OVER;
  }

  for (i = 0; i < numDataBlocks; i++)
  {
    dataBlock = workingOrderMgmt->dataBlockCollection[(startIndex + i) % MAX_RAW_DATA_ENTRIES];
    
    int isManualOrderMarking = 0;
    memcpy(&isManualOrderMarking, &dataBlock.addContent[DATABLOCK_OFFSET_CROSS_ID], 4);
    if (isManualOrderMarking != MANUAL_ORDER_MAKING)
    {
      //This is not manual order
      //PM will process it
      switch (workingOrderMgmt->orderIndex)
      {
        case PM_NASDAQ_RASH_INDEX:
          ParseRawDataForRASHOrder(dataBlock, workingOrderMgmt->tsId);
          break;
        
        case PM_ARCA_DIRECT_INDEX:
          ParseRawDataForARCA_DIRECTOrder(dataBlock, workingOrderMgmt->tsId);
          break;

        default:
          TraceLog(ERROR_LEVEL, "ProcessRawDataOfPMFromCollection: INVALID ECN ORDER, orderIndex = %d\n", workingOrderMgmt->orderIndex);
          break;
      }
    }
    
    // Increase number of data blocks process
    (*countRawDataProcessed) ++;

    // Save position raw data processed
    SavePositionRawDataProcessedToFile();
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  InitializeRawdataWaitList
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int InitializeRawdataWaitList()
{
  //TraceLog(DEBUG_LEVEL, "Size of Rawdata Wait List: %d\n", sizeof(RawdataWaitList));
  int i, j;
  memset (RawdataWaitList, 0, sizeof(RawdataWaitList));
  
  for (i = 0; i < MAX_WAIT_LIST_ORDERS; i++)
  {
    RawdataWaitList[i].clOrdID = -1;    //mark it free
    
    for (j = 0; j < MAX_WAIT_LIST_RAWDATA_BLOCKS_PER_ORDER; j++)
    {
      RawdataWaitList[i].blocks[j].itemType = WAIT_LIST_TYPE_ITEM_PROCESSED;
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadAndProcessRawdataWaitList
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int LoadAndProcessRawdataWaitList()
{
  char fullPath[MAX_PATH_LEN] = "\0";
  
  if (GetFullRawDataPath(RAW_DATA_WAIT_LIST_FILE, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Have some problem in get full raw data path operation (%s)\n", RAW_DATA_WAIT_LIST_FILE);

    return ERROR;
  }

  if (IsFileTimestampToday(fullPath) == 0)
  {
    TraceLog(ERROR_LEVEL, "%s is old, please check the correctness of data files\n", fullPath);
    exit(0);
  }
  
  WaitListFp = fopen(fullPath, "r");
  if (WaitListFp == NULL)
  {
    //File not exist. So create it for writing
    WaitListFp = fopen(fullPath, "w+");
    if (WaitListFp == NULL)
    {
      TraceLog(ERROR_LEVEL, "Cannot open file '%s' to read or we don't have permission to create\n", fullPath);

      return ERROR;
    }
    
    return SUCCESS;
  }

  //File exist, load it.
  int ret = fread(RawdataWaitList, sizeof(RawdataWaitList), 1, WaitListFp);
  if (ret != 1)
  {
    //Check the size, if zero, it's normal
    struct stat fileInfo;
    stat(fullPath, &fileInfo);
    if (fileInfo.st_size != 0)
    {
      TraceLog(ERROR_LEVEL, "Contents of '%s' is invalid, skipped\n", fullPath);
    }
  }
  else
  {
    TraceLog(DEBUG_LEVEL, "Loaded data from '%s' successfully\n", fullPath);
  }
  
  fclose(WaitListFp);
  
  //Now open file for writing
  WaitListFp = fopen(fullPath, "w+");
  if (WaitListFp == NULL)
  {
    TraceLog(ERROR_LEVEL, "Cannot open file '%s' to read or we don't have permission to create\n", fullPath);

    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  CheckAndProcessRawdataInWaitListForOrderID
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int CheckAndProcessRawdataInWaitListForOrderID(int orderID)
{
  int i, j;
  t_WaitListDetails *item;
  for (i = 0; i < MAX_WAIT_LIST_ORDERS; i++)
  {
    if (RawdataWaitList[i].clOrdID == orderID)  //There is data for this clOrdID
    {
      for (j = 0; j < MAX_WAIT_LIST_RAWDATA_BLOCKS_PER_ORDER; j++)
      {
        item = &RawdataWaitList[i].blocks[j];
        if (item->itemType != WAIT_LIST_TYPE_ITEM_PROCESSED)
        {
          switch (item->itemType)
          {
            case WAIT_LIST_TYPE_ARCA_DIRECT_ORDER_ACK:
              ProcessOrderACKOfARCA_DIRECTOrder(item->dataBlock, item->serverID, item->timeStamp);
              break;
            case WAIT_LIST_TYPE_ARCA_DIRECT_ORDER_FILLED:
              ProcessOrderFillOfARCA_DIRECTOrder(item->dataBlock, item->serverID, item->timeStamp);
              break;
            case WAIT_LIST_TYPE_ARCA_DIRECT_ORDER_CANCELED:
              ProcessOrderCancelOfARCA_DIRECTOrder(item->dataBlock, item->serverID, item->timeStamp);
              break;
            case WAIT_LIST_TYPE_ARCA_DIRECT_CANCEL_REQUEST:
              ProcessCancelRequestOfARCA_DIRECTOrder(item->dataBlock, item->serverID, item->timeStamp);
              break;
            case WAIT_LIST_TYPE_ARCA_DIRECT_CANCEL_ACK:
              ProcessCancelACKOfARCA_DIRECTOrder(item->dataBlock, item->serverID);
              break;
            case WAIT_LIST_TYPE_ARCA_DIRECT_ORDER_REJECTED:
              ProcessRejectOfARCA_DIRECTOrder(item->dataBlock, item->serverID, item->timeStamp);
              break;
            case WAIT_LIST_TYPE_ARCA_DIRECT_BUST_OR_CORRECT:
              ProcessBustOrCorrectOfARCA_DIRECTOrder(item->dataBlock, item->serverID);
              break;
            case WAIT_LIST_TYPE_OUCH_ORDER_ACK:
              ProcessAcceptedOrderOfOUCHOrder(item->dataBlock, item->serverID, item->timeStamp, item->oecType);
              break;
            case WAIT_LIST_TYPE_OUCH_ORDER_FILLED:
              ProcessExecutedOrderOfOUCHOrder(item->dataBlock, item->serverID, item->timeStamp, item->oecType);
              break;
            case WAIT_LIST_TYPE_OUCH_ORDER_CANCELED:
              ProcessCanceledOrderOfOUCHOrder(item->dataBlock, item->serverID, item->timeStamp, item->oecType);
              break;
            case WAIT_LIST_TYPE_OUCH_CANCEL_REQUEST:
              ProcessCancelRequestOfOUCHOrder(item->dataBlock, item->serverID, item->timeStamp, item->oecType);
              break;
            case WAIT_LIST_TYPE_OUCH_ORDER_REJECTED:
              ProcessRejectedOrderOfOUCHOrder(item->dataBlock, item->serverID, item->timeStamp, item->oecType);
              break;
            case WAIT_LIST_TYPE_OUCH_ORDER_BROKEN:
              ProcessBrokenOrderOfOUCHOrder(item->dataBlock, item->serverID, item->oecType);
              break;
            case WAIT_LIST_TYPE_OUCH_PRICE_CORRECTION:
              ProcessPriceCorrectionOfOUCHOrder(item->dataBlock, item->serverID, item->oecType);
              break;
            case WAIT_LIST_TYPE_OUCH_CANCEL_PENDING:
              ProcessCancelPendingOfOUCHOrder(item->dataBlock, item->serverID, item->oecType);
              break;
            case WAIT_LIST_TYPE_OUCH_CANCEL_REJECTED:
              ProcessCancelRejectOfOUCHOrder(item->dataBlock, item->serverID, item->oecType);
              break;
            case WAIT_LIST_TYPE_RASH_ORDER_ACK:
              ProcessAcceptedOrderOfRASHOrder(item->dataBlock, item->serverID, item->timeStamp);
              break;
            case WAIT_LIST_TYPE_RASH_ORDER_FILLED:
              ProcessExecutedOrderOfRASHOrder(item->dataBlock, item->serverID, item->timeStamp);
              break;
            case WAIT_LIST_TYPE_RASH_ORDER_CANCELED:
              ProcessCanceledOrderOfRASHOrder(item->dataBlock, item->serverID, item->timeStamp);
              break;
            case WAIT_LIST_TYPE_RASH_CANCEL_REQUEST:
              ProcessCancelRequestOfRASHOrder(item->dataBlock, item->serverID, item->timeStamp);
              break;
            case WAIT_LIST_TYPE_RASH_ORDER_REJECTED:
              ProcessRejectedOrderOfRASHOrder(item->dataBlock, item->serverID, item->timeStamp);
              break;
            case WAIT_LIST_TYPE_RASH_ORDER_BROKEN:
              ProcessBrokenOrderOfRASHOrder(item->dataBlock, item->serverID);
              break;
              
            case WAIT_LIST_TYPE_CCG_ORDER_ACK:
              ProcessOrderACKOfNYSE_CCGOrder(item->dataBlock, item->serverID, item->timeStamp);
              break;
            case WAIT_LIST_TYPE_CCG_ORDER_FILLED:
              ProcessOrderFillOfNYSE_CCGOrder(item->dataBlock, item->serverID, item->timeStamp);
              break;
            case WAIT_LIST_TYPE_CCG_ORDER_CANCELED:
              ProcessOrderCanceledOfNYSE_CCGOrder(item->dataBlock, item->serverID, item->timeStamp);
              break;
            case WAIT_LIST_TYPE_CCG_REJECTED:
              ProcessRejectOfNYSE_CCGOrder(item->dataBlock, item->serverID, item->timeStamp);
              break;
            case WAIT_LIST_TYPE_CCG_CANCEL_REQUEST:
              ProcessCancelRequestOfNYSE_CCGOrder(item->dataBlock, item->serverID, item->timeStamp);
              break;
            case WAIT_LIST_TYPE_CCG_CANCEL_ACK:
              ProcessCancelACKOfNYSE_CCGOrder(item->dataBlock, item->serverID, item->timeStamp);
              break;
            case WAIT_LIST_TYPE_CCG_BUST_ORDER_CORRECT:
              ProcessBustOrCorrectOrderOfNYSE_CCGOrder(item->dataBlock, item->serverID, item->timeStamp);
              break;
              
            case WAIT_LIST_TYPE_BATS_ORDER_ACK:
              ProcessAcceptedOrderOfBATS_BOEOrder(item->dataBlock, item->serverID, item->timeStamp, item->oecType);
              break;
            case WAIT_LIST_TYPE_BATS_ORDER_FILLED:
              ProcessExecutedOrderOfBATS_BOEOrder(item->dataBlock, item->serverID, item->timeStamp, item->oecType);
              break;
            case WAIT_LIST_TYPE_BATS_ORDER_CANCELED:
              ProcessCanceledOrderOfBATS_BOEOrder(item->dataBlock, item->serverID, item->timeStamp, item->oecType);
              break;
            case WAIT_LIST_TYPE_BATS_CANCEL_REQUEST:
              ProcessCancelRequestOfBATS_BOEOrder(item->dataBlock, item->serverID, item->timeStamp, item->oecType);
              break;
            case WAIT_LIST_TYPE_BATS_ORDER_REJECTED:
              ProcessRejectedOrderOfBATS_BOEOrder(item->dataBlock, item->serverID, item->timeStamp, item->oecType);
              break;
            case WAIT_LIST_TYPE_BATS_CANCEL_REJECTED:
              ProcessRejectedCancelOfBATS_BOEOrder(item->dataBlock, item->serverID, item->timeStamp, item->oecType);
              break;
            case WAIT_LIST_TYPE_BATS_BUST_OR_CORRECT:
              ProcessBrokenOrderOfBATS_BOEOrder(item->dataBlock, item->serverID, item->timeStamp, item->oecType);
              break;
              
            case WAIT_LIST_TYPE_EDGE_ORDER_ACK:
              ProcessAcceptedExtOrderOfEDGEOrder(item->dataBlock, item->serverID, item->timeStamp, item->oecType);
              break;
            case WAIT_LIST_TYPE_EDGE_ORDER_FILLED:
              ProcessExecutedOrderOfEDGEOrder(item->dataBlock, item->serverID, item->timeStamp, item->oecType);
              break;
            case WAIT_LIST_TYPE_EDGE_ORDER_CANCELED:
              ProcessCanceledOrderOfEDGEOrder(item->dataBlock, item->serverID, item->timeStamp, item->oecType);
              break;
            case WAIT_LIST_TYPE_EDGE_CANCEL_REQUEST:
              break;
            case WAIT_LIST_TYPE_EDGE_ORDER_REJECTED:
              ProcessRejectedOrderOfEDGEOrder(item->dataBlock, item->serverID, item->timeStamp, item->oecType);
              break;
            case WAIT_LIST_TYPE_EDGE_ORDER_BROKEN:
              ProcessBrokenOrderOfEDGEOrder(item->dataBlock, item->serverID, item->oecType);
              break;
              
            default:
              TraceLog(ERROR_LEVEL, "UNKNOWN record type in Rawdata Wait-List (%d)\n", item->itemType);
              break;
          }
          
          item->itemType = WAIT_LIST_TYPE_ITEM_PROCESSED; //Processed this item, clear it
          SaveRawdataWaitListToFile();
        }
      }
      
      RawdataWaitList[i].clOrdID = -1;  //Processed all item in this orderID, so clear it
      
      SaveRawdataWaitListToFile();
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SaveRawdataWaitListToFile
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SaveRawdataWaitListToFile()
{
  if (WaitListFp == NULL)
  {
    TraceLog(ERROR_LEVEL, "Rawdata Wait-List file is not opened for writing\n");
    return ERROR;
  }
  
  rewind(WaitListFp);
  if (fwrite(RawdataWaitList, sizeof(RawdataWaitList), 1, WaitListFp) != 1)
  {
    TraceLog(ERROR_LEVEL, "Could not write data to Rawdata Wait-List file\n");
    return ERROR;
  }
  
  fflush (WaitListFp);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  AddDataBlockToWaitList
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int AddDataBlockToWaitList(int oecType, int itemType, int clOrdID, t_DataBlock dataBlock, int serverID, char *timeStamp)
{
  int i, j;
  int freeSlot = -1;
  for (i = 0; i < MAX_WAIT_LIST_ORDERS; i++)
  {
    if (RawdataWaitList[i].clOrdID == clOrdID)
    {
      //Found an existing order, so add item to it
      for (j = 0; j < MAX_WAIT_LIST_RAWDATA_BLOCKS_PER_ORDER; j++)
      {
        if (RawdataWaitList[i].blocks[j].itemType == WAIT_LIST_TYPE_ITEM_PROCESSED)
        {
          //Found an emptry slot, add it
          RawdataWaitList[i].blocks[j].itemType = itemType;
          RawdataWaitList[i].blocks[j].oecType = oecType;
          RawdataWaitList[i].blocks[j].serverID = serverID;
          strncpy(RawdataWaitList[i].blocks[j].timeStamp, timeStamp, TIME_LENGTH);
          RawdataWaitList[i].blocks[j].timeStamp[TIME_LENGTH] = 0;
          memcpy(&RawdataWaitList[i].blocks[j].dataBlock, &dataBlock, sizeof(t_DataBlock));
          return SUCCESS;
        }
      }

      TraceLog(ERROR_LEVEL, "CRITICAL ERROR - Rawdata Wait-List is full!\n");
      return ERROR;
    }
    else if (RawdataWaitList[i].clOrdID == -1)  //free slot
    {
      if (freeSlot == -1)
      {
        freeSlot = i;
      }
    }
  }
  
  if (freeSlot == -1)
  {
    TraceLog(ERROR_LEVEL, "CRITICAL ERROR - Rawdata Wait-List is full\n");
    return ERROR;
  }
  
  //Found no existing orderID, and there is a free slot in Wait List, so add it
  RawdataWaitList[freeSlot].blocks[0].itemType = itemType;
  RawdataWaitList[freeSlot].blocks[0].oecType = oecType;
  RawdataWaitList[freeSlot].blocks[0].serverID = serverID;
  strncpy(RawdataWaitList[freeSlot].blocks[0].timeStamp, timeStamp, TIME_LENGTH);
  RawdataWaitList[freeSlot].blocks[0].timeStamp[TIME_LENGTH] = 0;
  memcpy(&RawdataWaitList[freeSlot].blocks[0].dataBlock, &dataBlock, sizeof(t_DataBlock));
  RawdataWaitList[freeSlot].clOrdID = clOrdID;
  return SUCCESS;
}

/****************************************************************************
- Function name:  InitializeHungOrderList
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int InitializeHungOrderList()
{
  int i;

  HungOrderList.count = 0;
  for (i = 0; i < MAX_LIST_HUNG_ORDERS; i++)
  {
    memset(&HungOrderList.buffer[i], 0, sizeof(t_HungOrderDetails));
    
    HungOrderList.buffer[i].orderType = -1;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadHungOrderListFromFile
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int LoadHungOrderListFromFile()
{
  char fullPath[MAX_PATH_LEN] = "\0";

  if (GetFullRawDataPath(FILE_NAME_OF_HUNG_ORDER_LIST, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Have some problem in get full raw data path operation (%s)\n", FILE_NAME_OF_HUNG_ORDER_LIST);
    return ERROR;
  }

  if (IsFileTimestampToday(fullPath) == 0)
  {
    TraceLog(ERROR_LEVEL, "%s is old, please check the correctness of data files\n", fullPath);
    exit(0);
  }
  
  struct stat fileStat;
  int retValue = stat(fullPath, &fileStat);

  if (retValue == SUCCESS)
  {
    if (fileStat.st_size % sizeof(t_HungOrderDetails) != 0)
    {
      TraceLog(ERROR_LEVEL, "Contents of '%s' is invalid, skipped\n", fullPath);
    }
  }

  FILE *fp = fopen(fullPath, "r");
  if (fp == NULL)
  {
    //File not exist. So create it for writing
    fp = fopen(fullPath, "w+");
    if (fp == NULL)
    {
      TraceLog(ERROR_LEVEL, "Cannot open file '%s' to read or we don't have permission to create\n", fullPath);

      return ERROR;
    }
    
    return SUCCESS;
  }
  
  while (!feof(fp))
  {
    retValue = fread(&HungOrderList.buffer[HungOrderList.count], sizeof(t_HungOrderDetails), 1, fp);
    if ( retValue != 1 )
    {
      return ERROR;
    }
    
    HungOrderList.count++;
  }
  
  TraceLog(DEBUG_LEVEL, "Loaded data from '%s' successfully\n", fullPath);

  fclose(fp);

  return SUCCESS;
}

/****************************************************************************
- Function name:  SaveHungOrderListToFile
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int SaveHungOrderListToFile(t_HungOrderDetails *hungOrder)
{
  char fullPath[MAX_PATH_LEN] = "\0";
  if (GetFullRawDataPath(FILE_NAME_OF_HUNG_ORDER_LIST, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Have some problem in get full raw data path operation (%s)\n", FILE_NAME_OF_HUNG_ORDER_LIST);
    return ERROR;
  }
  
  FILE *fp = fopen(fullPath, "a+");
  if (fp == NULL)
  {
    TraceLog(DEBUG_LEVEL, "Could not open file %s for reading\n", fullPath);
    return SUCCESS;
  }
  
  // Save new hung order to file
  if (fwrite(hungOrder, sizeof(t_HungOrderDetails), 1, fp) != 1)
  {
    TraceLog(ERROR_LEVEL, "could not save hung order to file\n");
    return ERROR;
  }
  
  fflush(fp);
  fclose(fp);

  return SUCCESS;
}

/****************************************************************************
- Function name:  AddHungOrderToList
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int AddHungOrderToList(t_HungOrderDetails *hungOrder)
{
  memcpy(&HungOrderList.buffer[HungOrderList.count], hungOrder, sizeof(t_HungOrderDetails));
  HungOrderList.count++;
  
  SaveHungOrderListToFile(hungOrder);
  SendHungOrderResetToAllTS();
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessHistoryHungOrder
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessHistoryHungOrder()
{
  int i, leavesQty;
  t_HungOrderDetails *hungOrder;
  t_OrderDetail orderDetail;
  
  char timestamp[32], execIdField[20];
  char accountStr[32];
  int tsId, serverID, ooIndex;
  double filledFee;
  t_Parameters execInfo;
  int lastMarket;
  
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;
  
  for (i = 0; i < HungOrderList.count; i++)
  {
    hungOrder = &HungOrderList.buffer[i];
    
    // Order detail
    strcpy(orderDetail.timestamp, hungOrder->timestamp);
    strcpy(orderDetail.msgContent, hungOrder->msgContent);
    orderDetail.length = strlen(orderDetail.msgContent);
    
    GetEpochTimeFromDateString(orderDetail.timestamp, timestamp);
    GetAccountName(hungOrder->venueType, hungOrder->account, accountStr);
    
    serverID = (hungOrder->clOrdId / 1000000) - 1;
    tsId = (serverID == MANUAL_ORDER) ? -1 : tradeServersInfo.config[serverID].tsId;
    
    if (hungOrder->orderType == ORDER_EDITOR_CANCELLED) // Order cancelled
    {
      ooIndex = ProcessUpdateASOpenOrderForStatus(tsId, hungOrder->clOrdId, orderStatusIndex.CanceledByECN, &orderDetail);
      
      switch(hungOrder->venueType)
      {
        case TYPE_ARCA_DIRECT:
        {
          t_ARCA_DIRECTOrderCancel cancelled;
      
          // YYYY/mm/DD
          memcpy(cancelled.date, orderDetail.timestamp, 10);
          cancelled.date[10] = 0;

          // HH:MM:SS.ms
          memcpy(cancelled.timestamp, &orderDetail.timestamp[11], TIME_LENGTH);
          cancelled.timestamp[TIME_LENGTH] = 0;
          
          memcpy(cancelled.processedTimestamp, &orderDetail.timestamp[11], TIME_LENGTH);
          cancelled.processedTimestamp[TIME_LENGTH] = 0;
          
          // Message detail
          strcpy(cancelled.msgContent, orderDetail.msgContent);
          
          // Client Order ID
          cancelled.orderID = hungOrder->clOrdId;
          
          if (ooIndex > -1)
          {
            strcpy(cancelled.status, GetOrderStatus(asOpenOrder.orderCollection[ooIndex].orderSummary.status));
          }
          else
          {
            strcpy(cancelled.status, orderStatus.CanceledByECN);
          }

          ProcessInsertCanceledOrder4ARCA_DIRECTQueryToCollection(&cancelled);
        }
          break;
        case TYPE_NASDAQ_OUCH:
        case TYPE_NASDAQ_OUBX:
        case TYPE_PSX:
        {
          t_OUCHCanceledOrder cancelled;
      
          // YYYY/mm/DD
          memcpy(cancelled.date, orderDetail.timestamp, 10);
          cancelled.date[10] = 0;

          // HH:MM:SS.ms
          memcpy(cancelled.timestamp, &orderDetail.timestamp[11], TIME_LENGTH);
          cancelled.timestamp[TIME_LENGTH] = 0;
          
          memcpy(cancelled.processedTimestamp, &orderDetail.timestamp[11], TIME_LENGTH);
          cancelled.processedTimestamp[TIME_LENGTH] = 0;
          
          // Message detail
          strcpy(cancelled.msgContent, orderDetail.msgContent);
          
          // Client Order ID
          cancelled.orderID = hungOrder->clOrdId;
          
          // Canceled Quantity
          cancelled.shares = hungOrder->shares;
          
          if (ooIndex > -1)
          {
            strcpy(cancelled.status, GetOrderStatus(asOpenOrder.orderCollection[ooIndex].orderSummary.status));
          }
          else
          {
            strcpy(cancelled.status, orderStatus.CanceledByECN);
          }

          ProcessInsertCanceledOrder4OUCHQueryToCollection(&cancelled);
        }
          break;
        case TYPE_NASDAQ_RASH:
        {
          t_RASHCanceledOrder cancelled;
      
          // YYYY/mm/DD
          memcpy(cancelled.date, orderDetail.timestamp, 10);
          cancelled.date[10] = 0;

          // HH:MM:SS.ms
          memcpy(cancelled.timestamp, &orderDetail.timestamp[11], TIME_LENGTH);
          cancelled.timestamp[TIME_LENGTH] = 0;
          
          memcpy(cancelled.processedTimestamp, &orderDetail.timestamp[11], TIME_LENGTH);
          cancelled.processedTimestamp[TIME_LENGTH] = 0;
          
          // Message detail
          strcpy(cancelled.msgContent, orderDetail.msgContent);
          
          // Client Order ID
          cancelled.orderID = hungOrder->clOrdId;
          
          // Canceled Quantity
          cancelled.shares = hungOrder->shares;
          
          if (ooIndex > -1)
          {
            strcpy(cancelled.status, GetOrderStatus(asOpenOrder.orderCollection[ooIndex].orderSummary.status));
          }
          else
          {
            strcpy(cancelled.status, orderStatus.CanceledByECN);
          }

          ProcessInsertCanceledOrder4RASHQueryToCollection(&cancelled);
        }
          break;
        case TYPE_NYSE_CCG:
        {
          t_NYSE_CCGCanceledOrder cancelled;
      
          // YYYY/mm/DD
          memcpy(cancelled.date, orderDetail.timestamp, 10);
          cancelled.date[10] = 0;

          // HH:MM:SS.ms
          memcpy(cancelled.timestamp, &orderDetail.timestamp[11], TIME_LENGTH);
          cancelled.timestamp[TIME_LENGTH] = 0;
          
          memcpy(cancelled.processedTimestamp, &orderDetail.timestamp[11], TIME_LENGTH);
          cancelled.processedTimestamp[TIME_LENGTH] = 0;
          
          // Message detail
          strcpy(cancelled.msgContent, orderDetail.msgContent);
          
          // Client Order ID
          cancelled.origClOrderID = hungOrder->clOrdId;
          
          lastMarket = hungOrder->msgContent[31];
          
          if (ooIndex > -1)
          {
            strcpy(cancelled.status, GetOrderStatus(asOpenOrder.orderCollection[ooIndex].orderSummary.status));
          }
          else
          {
            strcpy(cancelled.status, orderStatus.CanceledByECN);
          }

          ProcessInsertCanceledOrder4NYSE_CCGQueryToCollection(&cancelled);
        }
          break;
        case TYPE_BZX_BOE:
        case TYPE_BYX_BOE:
        {
          t_BATS_BOECanceledOrder cancelled;
      
          // YYYY/mm/DD
          memcpy(cancelled.date, orderDetail.timestamp, 10);
          cancelled.date[10] = 0;

          // HH:MM:SS.ms
          memcpy(cancelled.timestamp, &orderDetail.timestamp[11], TIME_LENGTH);
          cancelled.timestamp[TIME_LENGTH] = 0;
          
          memcpy(cancelled.processedTimestamp, &orderDetail.timestamp[11], TIME_LENGTH);
          cancelled.processedTimestamp[TIME_LENGTH] = 0;
          
          // Message detail
          strcpy(cancelled.msgContent, orderDetail.msgContent);
          
          // Client Order ID
          cancelled.clOrderId = hungOrder->clOrdId;
          
          if (ooIndex > -1)
          {
            strcpy(cancelled.status, GetOrderStatus(asOpenOrder.orderCollection[ooIndex].orderSummary.status));
          }
          else
          {
            strcpy(cancelled.status, orderStatus.CanceledByECN);
          }

          ProcessInsertCanceledOrder4BATS_BOEQueryToCollection(&cancelled);
        }
          break;
        case TYPE_EDGX_DIRECT:
        case TYPE_EDGA_DIRECT:
        {
          t_EDGECanceledOrder cancelled;
      
          // YYYY/mm/DD
          memcpy(cancelled.date, orderDetail.timestamp, 10);
          cancelled.date[10] = 0;

          // HH:MM:SS.ms
          memcpy(cancelled.timestamp, &orderDetail.timestamp[11], TIME_LENGTH);
          cancelled.timestamp[TIME_LENGTH] = 0;
          
          memcpy(cancelled.processedTimestamp, &orderDetail.timestamp[11], TIME_LENGTH);
          cancelled.processedTimestamp[TIME_LENGTH] = 0;
          
          // Message detail
          strcpy(cancelled.msgContent, orderDetail.msgContent);
          
          // Client Order ID
          cancelled.orderID = hungOrder->clOrdId;
          
          // Canceled Quantity
          cancelled.shares = hungOrder->shares;
          
          if (ooIndex > -1)
          {
            strcpy(cancelled.status, GetOrderStatus(asOpenOrder.orderCollection[ooIndex].orderSummary.status));
          }
          else
          {
            strcpy(cancelled.status, orderStatus.CanceledByECN);
          }

          ProcessInsertCanceledOrder4EDGEQueryToCollection(&cancelled);
        }
          break;
        default:
          break;
      }
      
      AddOJOrderCancelResponseRecord(timestamp, hungOrder->symbol, hungOrder->clOrdId, "''", "1", hungOrder->venueType, hungOrder->shares, 1, accountStr);
    }
    else if (hungOrder->orderType == ORDER_EDITOR_FILLED) // Order filled
    {
      lastMarket = 0;
      leavesQty = 0;
      orderDetail.execId = (unsigned long)atol(hungOrder->execId);
      execInfo.countParameters = Lrc_Split(hungOrder->execId, '_', &execInfo);
      
      ooIndex = ProcessUpdateASOpenOrderForExecuted(tsId, hungOrder->clOrdId, orderStatusIndex.Closed, hungOrder->shares, hungOrder->price, &orderDetail, hungOrder->account, hungOrder->liquidity, &filledFee);
      
      switch(hungOrder->venueType)
      {
        case TYPE_ARCA_DIRECT:
        {
          t_ARCA_DIRECTOrderFill executed;
          executed.fillFee = filledFee;
      
          // YYYY/mm/DD
          memcpy(executed.date, orderDetail.timestamp, 10);
          executed.date[10] = 0;

          // HH:MM:SS.ms
          memcpy(executed.timestamp, &orderDetail.timestamp[11], TIME_LENGTH);
          executed.timestamp[TIME_LENGTH] = 0;
          
          memcpy(executed.processedTimestamp, &orderDetail.timestamp[11], TIME_LENGTH );
          executed.processedTimestamp[TIME_LENGTH] = 0;
          
          // Message detail
          strcpy(executed.msgContent, orderDetail.msgContent);
          
          // Client Order ID
          executed.orderID = hungOrder->clOrdId;
          
          // Last Quantity
          executed.shares = hungOrder->shares;
          
          // Last Price
          executed.price = hungOrder->price;
          
          // Liquidity Flag
          executed.liquidityIndicator = hungOrder->liquidity[0];
          
          // Execution ID
          executed.executionId = atoi(execInfo.parameterList[0]) * 1000 + atoi(execInfo.parameterList[1]);
      
          if (ooIndex > -1)
          {
            strcpy(executed.status, GetOrderStatus(asOpenOrder.orderCollection[ooIndex].orderSummary.status));

            executed.leftShares = asOpenOrder.orderCollection[ooIndex].orderSummary.leftShares;
            executed.avgPrice = asOpenOrder.orderCollection[ooIndex].orderSummary.avgPrice;
            
            leavesQty = executed.leftShares;
          }
          else
          {
            strcpy(executed.status, orderStatus.Closed);
          }

          ProcessInsertExecutedOrder4ARCA_DIRECTQueryToCollection(&executed);
        }
          break;
        case TYPE_NASDAQ_OUCH:
        case TYPE_NASDAQ_OUBX:
        case TYPE_PSX:
        {
          t_OUCHExecutedOrder executed;
          executed.fillFee = filledFee;
      
          // YYYY/mm/DD
          memcpy(executed.date, orderDetail.timestamp, 10);
          executed.date[10] = 0;

          // HH:MM:SS.ms
          memcpy(executed.timestamp, &orderDetail.timestamp[11], TIME_LENGTH);
          executed.timestamp[TIME_LENGTH] = 0;
          
          memcpy(executed.processedTimestamp, &orderDetail.timestamp[11], TIME_LENGTH );
          executed.processedTimestamp[TIME_LENGTH] = 0;
          
          // Message detail
          strcpy(executed.msgContent, orderDetail.msgContent);
          
          // Client Order ID
          executed.orderID = hungOrder->clOrdId;
          
          // Last Quantity
          executed.shares = hungOrder->shares;
          
          // Last Price
          executed.price = hungOrder->price;
          
          // Liquidity Flag
          executed.liquidityIndicator = hungOrder->liquidity[0];
          
          // Execution ID
          executed.executionId = atoi(execInfo.parameterList[0]) * 1000 + atoi(execInfo.parameterList[1]);
      
          if (ooIndex > -1)
          {
            strcpy(executed.status, GetOrderStatus(asOpenOrder.orderCollection[ooIndex].orderSummary.status));

            executed.leftShares = asOpenOrder.orderCollection[ooIndex].orderSummary.leftShares;
            executed.avgPrice = asOpenOrder.orderCollection[ooIndex].orderSummary.avgPrice;
            
            leavesQty = executed.leftShares;
          }
          else
          {
            strcpy(executed.status, orderStatus.Closed);
          }

          ProcessInsertExecutedOrder4OUCHQueryToCollection(&executed);
        }
          break;
        case TYPE_NASDAQ_RASH:
        {
          t_RASHExecutedOrder executed;
          executed.fillFee = filledFee;
      
          // YYYY/mm/DD
          memcpy(executed.date, orderDetail.timestamp, 10);
          executed.date[10] = 0;

          // HH:MM:SS.ms
          memcpy(executed.timestamp, &orderDetail.timestamp[11], TIME_LENGTH);
          executed.timestamp[TIME_LENGTH] = 0;
          
          memcpy(executed.processedTimestamp, &orderDetail.timestamp[11], TIME_LENGTH );
          executed.processedTimestamp[TIME_LENGTH] = 0;
          
          // Message detail
          strcpy(executed.msgContent, orderDetail.msgContent);
          
          // Client Order ID
          executed.orderID = hungOrder->clOrdId;
          
          // Last Quantity
          executed.shares = hungOrder->shares;
          
          // Last Price
          executed.price = hungOrder->price;
          
          // Liquidity Flag
          executed.liquidityIndicator = hungOrder->liquidity[0];
          
          // Execution ID
          executed.executionId = atoi(execInfo.parameterList[0]) * 1000 + atoi(execInfo.parameterList[1]);
      
          if (ooIndex > -1)
          {
            strcpy(executed.status, GetOrderStatus(asOpenOrder.orderCollection[ooIndex].orderSummary.status));

            executed.leftShares = asOpenOrder.orderCollection[ooIndex].orderSummary.leftShares;
            executed.avgPrice = asOpenOrder.orderCollection[ooIndex].orderSummary.avgPrice;
            
            leavesQty = executed.leftShares;
          }
          else
          {
            strcpy(executed.status, orderStatus.Closed);
          }

          ProcessInsertExecutedOrder4RASHQueryToCollection(&executed);
        }
          break;
        case TYPE_NYSE_CCG:
        {
          t_NYSE_CCGFillOrder executed;
          executed.fillFee = filledFee;
      
          // YYYY/mm/DD
          memcpy(executed.date, orderDetail.timestamp, 10);
          executed.date[10] = 0;

          // HH:MM:SS.ms
          memcpy(executed.timestamp, &orderDetail.timestamp[11], TIME_LENGTH);
          executed.timestamp[TIME_LENGTH] = 0;
          
          memcpy(executed.processedTimestamp, &orderDetail.timestamp[11], TIME_LENGTH );
          executed.processedTimestamp[TIME_LENGTH] = 0;
          
          // Message detail
          strcpy(executed.msgContent, orderDetail.msgContent);
          
          // Client Order ID
          executed.clOrderID = hungOrder->clOrdId;
          
          // Last Quantity
          executed.lastShares = hungOrder->shares;
          
          // Last Price
          executed.lastPrice = hungOrder->price;
          
          // Liquidity Flag
          executed.billingIndicator = hungOrder->liquidity[0];
          
          // Execution ID
          executed.execID = atoi(execInfo.parameterList[0]) * 1000 + atoi(execInfo.parameterList[1]);
      
          if (ooIndex > -1)
          {
            strcpy(executed.status, GetOrderStatus(asOpenOrder.orderCollection[ooIndex].orderSummary.status));

            executed.leavesShares = asOpenOrder.orderCollection[ooIndex].orderSummary.leftShares;
            executed.avgPrice = asOpenOrder.orderCollection[ooIndex].orderSummary.avgPrice;
            
            leavesQty = executed.leavesShares;
          }
          else
          {
            strcpy(executed.status, orderStatus.Closed);
          }

          ProcessInsertFillOrder4NYSE_CCGQueryToCollection(&executed);
        }
          break;
        case TYPE_BZX_BOE:
        case TYPE_BYX_BOE:
        {
          t_BATS_BOEExecutedOrder executed;
          executed.fillFee = filledFee;
      
          // YYYY/mm/DD
          memcpy(executed.date, orderDetail.timestamp, 10);
          executed.date[10] = 0;

          // HH:MM:SS.ms
          memcpy(executed.timestamp, &orderDetail.timestamp[11], TIME_LENGTH);
          executed.timestamp[TIME_LENGTH] = 0;
          
          memcpy(executed.processedTimestamp, &orderDetail.timestamp[11], TIME_LENGTH );
          executed.processedTimestamp[TIME_LENGTH] = 0;
          
          // Message detail
          strcpy(executed.msgContent, orderDetail.msgContent);
          
          // Client Order ID
          executed.clOrderID = hungOrder->clOrdId;
          
          // Last Quantity
          executed.lastShares = hungOrder->shares;
          
          // Last Price
          executed.lastPx = hungOrder->price;
          
          // Liquidity Flag
          executed.baseLiquidityIndicator = hungOrder->liquidity[0];
          
          // Execution ID
          executed.execID = atoi(execInfo.parameterList[0]) * 1000 + atoi(execInfo.parameterList[1]);
      
          if (ooIndex > -1)
          {
            strcpy(executed.status, GetOrderStatus(asOpenOrder.orderCollection[ooIndex].orderSummary.status));

            executed.leavesShares = asOpenOrder.orderCollection[ooIndex].orderSummary.leftShares;
            executed.avgPrice = asOpenOrder.orderCollection[ooIndex].orderSummary.avgPrice;
            
            leavesQty = executed.leavesShares;
          }
          else
          {
            strcpy(executed.status, orderStatus.Closed);
          }

          ProcessInsertFillOrder4BATS_BOEQueryToCollection(&executed);
        }
          break;
        case TYPE_EDGX_DIRECT:
        case TYPE_EDGA_DIRECT:
        {
          t_EDGEExecutedOrder executed;
          executed.fillFee = filledFee;
      
          // YYYY/mm/DD
          memcpy(executed.date, orderDetail.timestamp, 10);
          executed.date[10] = 0;

          // HH:MM:SS.ms
          memcpy(executed.timestamp, &orderDetail.timestamp[11], TIME_LENGTH);
          executed.timestamp[TIME_LENGTH] = 0;
          
          memcpy(executed.processedTimestamp, &orderDetail.timestamp[11], TIME_LENGTH );
          executed.processedTimestamp[TIME_LENGTH] = 0;
          
          // Message detail
          strcpy(executed.msgContent, orderDetail.msgContent);
          
          // Client Order ID
          executed.orderID = hungOrder->clOrdId;
          
          // Last Quantity
          executed.shares = hungOrder->shares;
          
          // Last Price
          executed.price = hungOrder->price;
          
          // Liquidity Flag
          strcpy(executed.liquidityIndicator, hungOrder->liquidity);
          
          // Execution ID
          executed.executionId = atoi(execInfo.parameterList[0]) * 1000 + atoi(execInfo.parameterList[1]);
      
          if (ooIndex > -1)
          {
            strcpy(executed.status, GetOrderStatus(asOpenOrder.orderCollection[ooIndex].orderSummary.status));

            executed.leftShares = asOpenOrder.orderCollection[ooIndex].orderSummary.leftShares;
            executed.avgPrice = asOpenOrder.orderCollection[ooIndex].orderSummary.avgPrice;
            
            leavesQty = executed.leftShares;
          }
          else
          {
            strcpy(executed.status, orderStatus.Closed);
          }

          ProcessInsertExecutedOrder4EDGEQueryToCollection(&executed);
        }
          break;
        default:
          break;
      }
      
      sprintf(execIdField, "%lu", orderDetail.execId);
      
      char *exOrderId = "0";
      
      AddOJOrderFillRecord(timestamp, hungOrder->symbol, exOrderId, hungOrder->venueType, hungOrder->side, hungOrder->price, hungOrder->shares,
                hungOrder->clOrdId, hungOrder->liquidity, "''", execIdField, "1", (leavesQty == 0)?1:0, accountStr, lastMarket);
    }
    else
    {
      TraceLog(ERROR_LEVEL, "Unknown order type in Hung Order (%d)\n", hungOrder->orderType);
    }
  }
  
  return SUCCESS;
}
/***************************************************************************/
