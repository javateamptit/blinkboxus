/****************************************************************************
** Project name:  Equity Arbitrage Application
** File name:   nasdaq_rash_proc.c
** Description:   This file contains function definitions that were declared
        in nasdaq_rash_proc.h

** Author:    Sang Nguyen-Minh
** First created on 26 September 2007
** Last updated on 26 September 2007
****************************************************************************/

/****************************************************************************
** Include files and define several variables
****************************************************************************/
#define _GNU_SOURCE
#include "configuration.h"
#include "socket_util.h"
#include "raw_data_mgmt.h"
#include "trade_servers_mgmt_proc.h"
#include "nasdaq_rash_proc.h"
#include "order_mgmt_proc.h"
#include "daedalus_proc.h"
#include "trader_tools_proc.h"

#include <unistd.h>
#include <math.h>
#include <errno.h>

t_NASDAQ_RASH_Config NASDAQ_RASH_Config;
/****************************************************************************
** Function definitions
****************************************************************************/

/****************************************************************************
- Function name:  Initialize_NASDAQ_RASH
- Input:      N/A
- Output:   N/A
- Return:   N/A
- Description:  + The routine hides the following facts
          - Set default value for NASDAQ RASH Configuration
        + There are no preconditions guaranteed to the routine
- Usage:      N/A
****************************************************************************/
void Initialize_NASDAQ_RASH(void)
{
  /*
  int incomingSeqNum;
  int outgoingSeqNum;
  int enableTrading;
  int isConnected;

  pthread_mutex_t outgoingSeqNum_Mutex;
  
  int socket;
  int port;
  char ipAddress[80];
  
  char userName[80];
  char password[80];
  
  char buffer[2 * HALF_MAX_BUFFER_LEN];
  
  int remainingBytes;
  int numOfByteReceived;
  int numOfBytesPerRecv;
  int numOfBytesToRecv;
  int startIndex;
  
  char isUnCompletedMsg;
  */

  // Load incoming sequence number from file
  if (LoadSequenceNumber("nasdaq_rash_seqs", AS_NASDAQ_RASH_INDEX, &NASDAQ_RASH_Config) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot load nasdaq_rash_seqs file\n");

    return;
  }

  NASDAQ_RASH_Config.enableTrading = -1;
  NASDAQ_RASH_Config.isConnected = DISCONNECTED;

  pthread_mutex_init(&NASDAQ_RASH_Config.outgoingSeqNum_Mutex, NULL);

  NASDAQ_RASH_Config.socket = -1;
  NASDAQ_RASH_Config.port = -1;

  memset(NASDAQ_RASH_Config.ipAddress, 0, MAX_LINE_LEN);
  memset(NASDAQ_RASH_Config.userName, 0, MAX_LINE_LEN);
  memset(NASDAQ_RASH_Config.password, 0, MAX_LINE_LEN);


  memset(NASDAQ_RASH_Config.buffer, 0, 2 * HALF_MAX_BUFFER_LEN);
  NASDAQ_RASH_Config.numOfByteReceived = 0;
  NASDAQ_RASH_Config.isUnCompletedMsg = NO;
  NASDAQ_RASH_Config.startIndex = 0;
}

/****************************************************************************
- Function name:  StartUp_NASDAQ_RASH_Module
- Input:      + threadArgs
- Output:   N/A
- Return:   Success or Failure
- Description:  + The routine hides the following facts
        + There are no preconditions guaranteed to the routine
        + The routine guarantees that the return value will 
            have a value of either
          - Success
          or
          - Failure
- Usage:      N/A
****************************************************************************/
void *StartUp_NASDAQ_RASH_Module(void *threadArgs)
{
  // Initialize several parameters
  memset(NASDAQ_RASH_Config.buffer, 0, 2 * HALF_MAX_BUFFER_LEN);
  NASDAQ_RASH_Config.numOfByteReceived = 0;
  NASDAQ_RASH_Config.isUnCompletedMsg = NO;
  NASDAQ_RASH_Config.startIndex = 0;
  
  // Load incoming sequence number from file
  if (LoadSequenceNumber("nasdaq_rash_seqs", AS_NASDAQ_RASH_INDEX, &NASDAQ_RASH_Config) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Cannot load nasdaq_rash_seqs file\n");

    return NULL;
  }

  TraceLog(DEBUG_LEVEL, "Trying connect to NASDAQ RASH Server...\n");

  // Connect to server
  NASDAQ_RASH_Config.socket = Connect2Server(NASDAQ_RASH_Config.ipAddress, NASDAQ_RASH_Config.port, "NASDAQ RASH");
  
  if (NASDAQ_RASH_Config.socket == ERROR)
  {
    TraceLog(ERROR_LEVEL, "* Cannot connect to NASDAQ RASH Server\n");

    return NULL;
  }
  
  // Talk to server
  TalkTo_NASDAQ_RASH_Server();
  
  // Close connection and exit the module
  close(NASDAQ_RASH_Config.socket);
  
  orderStatusMgmt[AS_NASDAQ_RASH_INDEX].shouldConnect = NO;
  orderStatusMgmt[AS_NASDAQ_RASH_INDEX].isOrderConnected = DISCONNECTED;  
  traderToolsInfo.orderConnection[AS_NASDAQ_RASH_INDEX].status = DISCONNECT;

  TraceLog(DEBUG_LEVEL, "Connection to NASDAQ RASH Server was disconnected\n");

  BuildNSendDisconnectAlertToAllDaedalus("NASDAQ RASH from AS");
  BuildNSendDisconnectAlertToAllTT("NASDAQ RASH from AS");
  
  return NULL;
}

/****************************************************************************
- Function name:  TalkTo_NASDAQ_RASH_Server
- Input:      N/A
- Output:   N/A
- Return:   N/A
- Description:  + The routine hides the following facts
          - Receive message from server and send response appropriately
          - When the routine returns, meaning has some problems with 
          socket
        + There are no preconditions guaranteed to the routine
- Usage:      N/A
****************************************************************************/
void TalkTo_NASDAQ_RASH_Server(void)
{
  t_DataBlock dataBlock;
  int msgType;

  TraceLog(DEBUG_LEVEL, "Talking to NASDAQ RASH Server...\n");

  // Send Logon message to server
  if (BuildAndSend_NASDAQ_RASH_LogonRequest() == ERROR)
  {
    return;
  }
  
  while( ((msgType = Get_NASDAQ_RASH_CompletedMessage()) != ERROR) && (orderStatusMgmt[AS_NASDAQ_RASH_INDEX].shouldConnect == YES) )
  {
    /*
    Base on message type we will call appropriate functions
    */

    switch (msgType)
    {
      case NASDAQ_RASH_ORDER_ACCEPTED:
      {
        // Call function to check cross trade here
        
        // Update data block collection
        dataBlock.msgLen = NASDAQ_RASH_ORDER_ACCEPTED_LEN;
        
        memcpy(dataBlock.msgContent, 
            &NASDAQ_RASH_Config.buffer[NASDAQ_RASH_Config.startIndex - NASDAQ_RASH_ORDER_ACCEPTED_LEN], 
            dataBlock.msgLen);
        
        // Order Id
        char fieldToken[16];
        memcpy(fieldToken, &dataBlock.msgContent[12], 12);
        fieldToken[12] = 0;
        int clOrdId = atoi(fieldToken);
  
        //Call function to update verify position list
        if (clOrdId < 500000) // AS manual order
        {         
          UpdateVerifyPositionInfoFromRASHAcceptedManualOrder(&dataBlock, AS_MANUAL_ORDER);
        }
        else // PM order
        {
          UpdateVerifyPositionInfoFromRASHAcceptedFromPM(&dataBlock);
        }
        
        Add_NASDAQ_RASH_DataBlockToCollection(&dataBlock, clOrdId);
        
        // Save incoming sequence number to file
        SaveNASDAQ_RASH_SeqNumToFile();
        SendRASHSeqNumberToPM();
        
        break;
      }
      
      case NASDAQ_RASH_ORDER_EXECUTED:
      { 
        // Update data block collection
        dataBlock.msgLen = NASDAQ_RASH_ORDER_EXECUTED_LEN;
        
        memcpy(dataBlock.msgContent, 
            &NASDAQ_RASH_Config.buffer[NASDAQ_RASH_Config.startIndex - NASDAQ_RASH_ORDER_EXECUTED_LEN], 
            dataBlock.msgLen);
        
        // Order Id
        char fieldToken[16];
        memcpy(fieldToken, &dataBlock.msgContent[12], 12);
        fieldToken[12] = 0;
        int clOrdId = atoi(fieldToken);
            
        //Call function to update verify position list
        if (clOrdId < 500000) // AS manual order
        {
          UpdateVerifyPositionInfoFromRASHExecutedManualOrder(&dataBlock, AS_MANUAL_ORDER);
        }
        else // PM order
        {
          UpdateVerifyPositionInfoFromRASHExecutedFromPM(&dataBlock);
        }

        Add_NASDAQ_RASH_DataBlockToCollection(&dataBlock, clOrdId);
        
        // Save incoming sequence number to file
        SaveNASDAQ_RASH_SeqNumToFile();
        SendRASHSeqNumberToPM();
        
        break;
      }
      
      case NASDAQ_RASH_ORDER_CANCELED:
      {
        // Call function to check cross trade here
        
        // Update data block collection
        dataBlock.msgLen = NASDAQ_RASH_ORDER_CANCELED_LEN;
        
        memcpy(dataBlock.msgContent, 
            &NASDAQ_RASH_Config.buffer[NASDAQ_RASH_Config.startIndex - NASDAQ_RASH_ORDER_CANCELED_LEN], 
            dataBlock.msgLen);
        
        // Order Id
        char fieldToken[16];
        memcpy(fieldToken, &dataBlock.msgContent[12], 12);
        fieldToken[12] = 0;
        int clOrdId = atoi(fieldToken);
            
        //Call function to update verify position list
        if (clOrdId < 500000) // AS manual order
        {
          UpdateVerifyPositionInfoFromRASHCanceledManualOrder(&dataBlock, AS_MANUAL_ORDER);
        }
        else // PM order
        {
          UpdateVerifyPositionInfoFromRASHCanceledFromPM(&dataBlock);
        }
            
        Add_NASDAQ_RASH_DataBlockToCollection(&dataBlock, clOrdId);
        
        // Save incoming sequence number to file
        SaveNASDAQ_RASH_SeqNumToFile();
        SendRASHSeqNumberToPM();
        
        break;
      }
      
      case NASDAQ_RASH_ORDER_REJECTED:
      {
        // Call function to check cross trade here
        
        // Update data block collection
        dataBlock.msgLen = NASDAQ_RASH_ORDER_REJECTED_LEN;
        
        memcpy(dataBlock.msgContent, 
            &NASDAQ_RASH_Config.buffer[NASDAQ_RASH_Config.startIndex - NASDAQ_RASH_ORDER_REJECTED_LEN], 
            dataBlock.msgLen);
        
        // Order Id
        char fieldToken[16];
        memcpy(fieldToken, &dataBlock.msgContent[12], 12);
        fieldToken[12] = 0;
        int clOrdId = atoi(fieldToken);
        
        //Call function to update verify position list
        if (clOrdId < 500000) // AS manual order
        {
          UpdateVerifyPositionInfoFromRASHRejectedManualOrder(&dataBlock, AS_MANUAL_ORDER);
        }
        else // PM order
        {
          UpdateVerifyPositionInfoFromRASHRejectedFromPM(&dataBlock);
        }
            
        Add_NASDAQ_RASH_DataBlockToCollection(&dataBlock, clOrdId);
        
        // Save incoming sequence number to file
        SaveNASDAQ_RASH_SeqNumToFile();
        SendRASHSeqNumberToPM();
        
        break;
      }
      
      case NASDAQ_RASH_BROKEN_TRADE:
      {
        // Call function to check cross trade here
        
        // Update data block collection
        dataBlock.msgLen = NASDAQ_RASH_BROKEN_TRADE_LEN;
        
        memcpy(dataBlock.msgContent, 
            &NASDAQ_RASH_Config.buffer[NASDAQ_RASH_Config.startIndex - NASDAQ_RASH_BROKEN_TRADE_LEN], 
            dataBlock.msgLen);
        
        // Order Id
        char fieldToken[16];
        memcpy(fieldToken, &dataBlock.msgContent[12], 12);
        fieldToken[12] = 0;
        int clOrdId = atoi(fieldToken);
            
        Add_NASDAQ_RASH_DataBlockToCollection(&dataBlock, clOrdId);
        
        // Save incoming sequence number to file
        SaveNASDAQ_RASH_SeqNumToFile();
        SendRASHSeqNumberToPM();
        
        break;
      }
      
      case NASDAQ_RASH_SYSTEM_EVENT:
        break;
        
      case NASDAQ_RASH_HEARTBEAT:
        //TraceLog(DEBUG_LEVEL, "Received heartbeat message from NASDAQ RASH Server\n");
        if (BuildAndSend_NASDAQ_RASH_Heartbeat() == ERROR)
        {
          return;
        }
        break;
      
      case NASDAQ_RASH_DEBUG:
        break;
        
      case NASDAQ_RASH_LOGIN_ACCEPTED_RETURN:
        // Update flag to indicate that we connected to server successfully

        TraceLog(DEBUG_LEVEL, "Received logon accepted message from NASDAQ RASH Server\n");

        orderStatusMgmt[AS_NASDAQ_RASH_INDEX].isOrderConnected = CONNECTED;
        traderToolsInfo.orderConnection[AS_NASDAQ_RASH_INDEX].status = CONNECT;
        break;
      
      case NASDAQ_RASH_LOGIN_REJECTED_RETURN:
      
        // Update flag to indicate that we cannot connect to server
        TraceLog(ERROR_LEVEL, "Received logon rejected message from NASDAQ RASH Server\n");

        return;
      default:
        break;
    }
  }
}

/****************************************************************************
- Function name:  BuildAndSend_NASDAQ_RASH_LogonRequest
- Input:      N/A
- Output:   N/A
- Return:   Success or Failure
- Description:  + The routine hides the following facts
          - Build logon message with using some global variables
          - Calculate message length and return through return value
        + There are preconditions guaranteed to the routine
          -   message must be allocated before
        + The routine guarantees that the return value will 
            have a value of either
          - Success
          or 
          - Failure
- Usage:      N/A
****************************************************************************/
int BuildAndSend_NASDAQ_RASH_LogonRequest(void)
{
  char message[38];
      
  int currentIndex;
  
  // len is used to store the length of various strings
  int len = 0;
  
  /*
  Both fields username and password are insensitive, 
  and must be filled with spaces on the right
  */
  memset(message, ' ', 38);
  
  // First character is the message type. 'L' means login request
  message[0] = 'L';
  
  /*
  Next is the 6 bytes which store the username
  We assume the length of the username is OK
  */
  len = strlen(NASDAQ_RASH_Config.userName);

  for (currentIndex = 0; currentIndex < len; currentIndex++)  
  {
    if (currentIndex < 6)
    {
      message[1 + currentIndex] = NASDAQ_RASH_Config.userName[currentIndex];
    }
  }
  
  /*
  Next is the 10 bytes which store the password
  We assume the length of the password is OK
  */
  len = strlen(NASDAQ_RASH_Config.password);

  for (currentIndex = 0; currentIndex < len; currentIndex++)
  {
    if (currentIndex < 10)
    {
      message[7 + currentIndex] = NASDAQ_RASH_Config.password[currentIndex];
    }
  }

  // All zero will do me good
  memset(&message[27], ' ', 10);

  char temp[10];

  sprintf(temp, "%10d", NASDAQ_RASH_Config.incomingSeqNum);
  
  memcpy(&message[27], temp, 10); 

  // Terminating linefeed
  message[37] = 10;
  
  return Send_NASDAQ_RASH_Msg(message, 38);
}

/****************************************************************************
- Function name:  BuildAndSend_NASDAQ_RASH_OrderMsg
- Input:      + newOrder
- Output:   N/A
- Return:   Success or Failure
- Description:  + The routine hides the following facts
          - Build new order message with using some global variables
        + There are preconditions guaranteed to the routine
          -   message must be allocated before
        + The routine guarantees that the return value will 
            have a value of either
          - Success
          or 
          - Failure 
- Usage:      N/A
****************************************************************************/
int BuildAndSend_NASDAQ_RASH_OrderMsg(t_NASDAQ_RASH_NewOrder *newOrder)
{
  char message[NASDAQ_RASH_NEW_ORDER_MSG_LEN];
  
  memset(message, ' ', NASDAQ_RASH_NEW_ORDER_MSG_LEN);
  
  // Unsequence data packet
  message[0] = 'U';
  
  // Order message type
  message[1] = 'O';
  
  if (newOrder->tradingAccount < 0 || newOrder->tradingAccount > MAX_ACCOUNT)
  {
    TraceLog(ERROR_LEVEL, "(BuildAndSend_NASDAQ_RASH_CancelMsg): Invalid account (%d)\n", newOrder->tradingAccount);
    return ERROR;
  }
  
  // Order token (2 digit account + orderID --> 14 char)
  message[2] = TradingAccount.RASHToken[newOrder->tradingAccount][0];
  message[3] = TradingAccount.RASHToken[newOrder->tradingAccount][1];
  
  strncpy(&message[4], newOrder->orderToken, 12);
  Manual_Order_Account_Mapping[atoi(newOrder->orderToken)] = newOrder->tradingAccount;
  
  // Buy/Sell indicator 
  message[16] = newOrder->side;
  
  // Share    
  Lrc_itoaf(newOrder->share, '0', 6, &message[17]);
  
  // Stock
  strncpy(&message[23], newOrder->stock, strnlen(newOrder->stock, SYMBOL_LEN));
  
  // Price
  Lrc_itoaf(lrint(newOrder->price * 10000), '0', 10, &message[31]);

  // Time in Force
  Lrc_itoaf(newOrder->timeInForce, '0', 5, &message[41]);

  // Firm
  strncpy(&message[46], newOrder->firm, 4);

  // Display
  message[50] = newOrder->display;

  /***************************************************************
  - MinQty: Minimum Fill Amount Allowed. 
  If a non-zero value is put in this field, TIF must be IOC (or 0)
  ***************************************************************/
  Lrc_itoaf(0, '0', 6, &message[51]);

  /***************************************************************
  - Max Floor: Shares to Display. If zero this field will default 
  to the order qty. Use the display field to specify a hidden order.
  ***************************************************************/
  if ((newOrder->maxFloor > 0) && (newOrder->maxFloor < newOrder->share))
  {
    Lrc_itoaf(newOrder->maxFloor, '0', 6, &message[57]);

    TraceLog(DEBUG_LEVEL, "maxFloor = %d\n", newOrder->maxFloor);
  }
  else
  {
    Lrc_itoaf(0, '0', 6, &message[57]);
  }
  
  /***************************************************************
  - Peg Type: N � No Peg
        P � Market
        R � Primary
  ***************************************************************/
  message[63] = 'N';

  if (feq(newOrder->price, 0.00))
  {
    if (fgt(newOrder->pegDef, 0.00))
    {
      message[63] = 'R';

      TraceLog(DEBUG_LEVEL, "Peg Difference = %lf\n", newOrder->pegDef);
    }
    else
    {
      message[63] = 'P';
    }   
  }

  /***************************************************************
  - Peg Difference Sign:  +
              -
  If peg type is set to �N�, specify �+�
  ***************************************************************/
  message[64] = '+';

  if ((message[63] == 'R') && (newOrder->side != 'B'))
  {
    message[64] = '-';    
  }
  
  /***************************************************************
  - Peg Difference: Amount; 6.4 (implied decimal). If peg type is 
  set to �N�, specify 0
  ***************************************************************/
  Lrc_itoaf(0, '0', 10, &message[65]);

  if (message[63] == 'R')
  {
    Lrc_itoaf(lrint(newOrder->pegDef * 10000), '0', 10, &message[65]);
  }
  
  /***************************************************************
  - Discretion Price: Discretion Price for Discretionary Order. 
  If set to 0, then this order does not have discretion.
  ***************************************************************/
  Lrc_itoaf(0, '0', 10, &message[75]);
  
  /***************************************************************
  - Discretion Peg Type:  N � No Peg
              P � Market
              R � Primary
  ***************************************************************/
  message[85] = 'N';

  /***************************************************************
  - Discretion Peg Difference Sign: +
                    -
  If peg type is set to �N�, specify �+�
  ***************************************************************/
  message[86] = '+';
  
  /***************************************************************
  - Discretion Peg Difference: Amount; 6.4 (implied decimal). 
  If peg type is set to �N�, specify 0
  ***************************************************************/ 
  Lrc_itoaf(0, '0', 10, &message[87]);

  /***************************************************************
  - Capacity/Rule 80A Indicator: Capacity Code
  ***************************************************************/
  message[97] = 'P';

  /***************************************************************
  - Random Reserve: Shares to do random reserve with
  ***************************************************************/
  Lrc_itoaf(0, '0', 6, &message[98]);

  /***************************************************************
  - Route Dest / Exec Broker: Target ID (INET, DOTN, DOTA, DOTM, 
  DOTP, STGY and SCAN)
  ***************************************************************/
  memcpy(&message[104], RashRouteID[currentMarketTimeType], 4);

  /***************************************************************
  - Cust / Terminal ID / Sender SubID: Client Initiated; Pass-thru. 
  Must be left justified and may not start with a space.
  ***************************************************************/
  
  memset(&message[108], ' ', 32);
  
  // Terminating linefeed
  message[140] = 10;
  
  // Send Enter Order Message
  
  if (Send_NASDAQ_RASH_Msg(message, NASDAQ_RASH_NEW_ORDER_MSG_LEN) == SUCCESS)
  {
    t_DataBlock dataBlock;
    
    dataBlock.msgLen = NASDAQ_RASH_NEW_ORDER_MSG_LEN;
    
    memcpy(dataBlock.msgContent, message, NASDAQ_RASH_NEW_ORDER_MSG_LEN);

    dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID] = newOrder->tradingAccount;
    
    Add_NASDAQ_RASH_DataBlockToCollection(&dataBlock, atoi(newOrder->orderToken));
    
    strncpy(manualOrderInfo[atoi(newOrder->orderToken) % MAX_MANUAL_ORDER].symbol, newOrder->stock, SYMBOL_LEN);

    return SUCCESS;
  }
  else
  {
    return ERROR;
  }
}

/****************************************************************************
- Function name:  BuildAndSend_NASDAQ_RASH_CancelMsg
- Input:      + cancelOrder
- Output:   N/A
- Return:   Success or Failure
- Description:  + The routine hides the following facts
          - Build cancel order message with using some global variables
        + There are preconditions guaranteed to the routine
          -   message must be allocated before
        + The routine guarantees that the return value will 
            have a value of either
          - Success
          or 
          - Failure 
- Usage:      N/A
****************************************************************************/
int BuildAndSend_NASDAQ_RASH_CancelMsg(t_NASDAQ_RASH_CancelOrder *cancelOrder)
{
  int index;    
  
  char message[CANCEL_MSG_LEN];
  memset(message, ' ', CANCEL_MSG_LEN); 
  
  // Unsequence data packet
  message[0] = 'U';
  
  // Order message type
  message[1] = 'X';
  
  // Order token
  if (cancelOrder->tradingAccount < 0 || cancelOrder->tradingAccount > MAX_ACCOUNT)
  {
    TraceLog(ERROR_LEVEL, "(BuildAndSend_NASDAQ_RASH_CancelMsg): Invalid account (%d)\n", cancelOrder->tradingAccount);
    return ERROR;
  }
  
  // Order token (2 digit account + orderID --> 14 char)
  message[2] = TradingAccount.RASHToken[cancelOrder->tradingAccount][0];
  message[3] = TradingAccount.RASHToken[cancelOrder->tradingAccount][1];
  
  memcpy(&message[4], cancelOrder->orderToken, 12);
  
  for (index = 2; index < 16; index++)
  {
    if (message[index] == 0)
    {
      message[index] = ' ';   
    }   
  }
  
  // Share
  sprintf(&message[16], "%s", "000000");
    
  // Terminating linefeed
  message[22] = 10;
  
  // Send Cancel Order Request Message
  if (Send_NASDAQ_RASH_Msg(message, CANCEL_MSG_LEN) == SUCCESS)
  {
    t_DataBlock dataBlock;
    
    dataBlock.msgLen = CANCEL_MSG_LEN;
    dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID] = cancelOrder->tradingAccount;
    
    memcpy(dataBlock.msgContent, message, CANCEL_MSG_LEN);
    
    Add_NASDAQ_RASH_DataBlockToCollection(&dataBlock, atoi(cancelOrder->orderToken));

    return SUCCESS;
  }
  else
  {
    return ERROR;
  }
}

/****************************************************************************
- Function name:  BuildAndSend_NASDAQ_RASH_Heartbeat
- Input:      N/A
- Output:   N/A
- Return:   Success or Failure
- Description:  + The routine hides the following facts
          - Build heartbeat message and send it to server
        + There are no preconditions guaranteed to the routine
        + The routine guarantees that the return value will 
            have a value of either
          - Success
          or 
          - Failure 
- Usage:      N/A
****************************************************************************/
int BuildAndSend_NASDAQ_RASH_Heartbeat(void)
{
  char message[2];    
  message[0] = 'R';
  message[1] = 10;
  
  return Send_NASDAQ_RASH_Msg(message, 2);
}

/****************************************************************************
- Function name:  Receive_NASDAQ_RASH_Msg
- Input:      N/A
- Output:   N/A
- Return:   Success or Failure
- Description:  + The routine hides the following facts
          - Receive message from server
        + There are preconditions guaranteed to the routine
          -   dataBlock must be allocated and initialized before
        + The routine guarantees that the return value will 
            have a value of either
          - Success
          or 
          - Failure
- Usage:      N/A
****************************************************************************/
int Receive_NASDAQ_RASH_Msg()
{
  if (NASDAQ_RASH_Config.numOfByteReceived < HALF_MAX_BUFFER_LEN)
  {
    NASDAQ_RASH_Config.numOfBytesPerRecv = recv(NASDAQ_RASH_Config.socket, 
                    &NASDAQ_RASH_Config.buffer[NASDAQ_RASH_Config.numOfByteReceived], 
                    NASDAQ_RASH_Config.numOfBytesToRecv, 0);
    
    // We use errno to indicate the socket is live or die
    if ( (NASDAQ_RASH_Config.numOfBytesPerRecv > 0) || 
      (NASDAQ_RASH_Config.numOfBytesPerRecv == 0 && errno == EAGAIN) )
    {
      NASDAQ_RASH_Config.numOfByteReceived += NASDAQ_RASH_Config.numOfBytesPerRecv;

      return SUCCESS;
    }
    //If we received an error
    else
    {
      return ERROR;
    }
  }
  else
  { 
    if (NASDAQ_RASH_Config.isUnCompletedMsg == YES)
    {
      NASDAQ_RASH_Config.numOfBytesPerRecv = recv(NASDAQ_RASH_Config.socket, 
          &NASDAQ_RASH_Config.buffer[NASDAQ_RASH_Config.numOfByteReceived], 
          NASDAQ_RASH_Config.numOfBytesToRecv, 0);
      
      // We use errno to indicate the socket is live or die
      if ( (NASDAQ_RASH_Config.numOfBytesPerRecv > 0) || (NASDAQ_RASH_Config.numOfBytesPerRecv == 0 && errno == EAGAIN) )
      {
        NASDAQ_RASH_Config.numOfByteReceived += NASDAQ_RASH_Config.numOfBytesPerRecv;

        return SUCCESS;
      }
      //If we received an error
      else
      {
        return ERROR;
      }
    }
    else if (NASDAQ_RASH_Config.isUnCompletedMsg == NO)
    {
      NASDAQ_RASH_Config.numOfBytesPerRecv = recv(NASDAQ_RASH_Config.socket, 
          NASDAQ_RASH_Config.buffer, 
          NASDAQ_RASH_Config.numOfBytesToRecv, 0);
      
      // We use errno to indicate the socket is live or die
      if ( (NASDAQ_RASH_Config.numOfBytesPerRecv > 0) || 
        (NASDAQ_RASH_Config.numOfBytesPerRecv == 0 && errno == EAGAIN) )
      {
        NASDAQ_RASH_Config.numOfByteReceived = NASDAQ_RASH_Config.numOfBytesPerRecv;
        NASDAQ_RASH_Config.startIndex = 0;

        return SUCCESS;
      }
      // If we received an error
      else
      {
        return ERROR;
      }
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  Get_NASDAQ_RASH_CompletedMessage
- Input:      N/A
- Output:   N/A
- Return:   Success or Failure
- Description:  + The routine hides the following facts
          - Try to get a completed message to process
        + There are no preconditions guaranteed to the routine
        + The routine guarantees that the return value will 
            have a value of either
          - Success
          or 
          - Failure
- Usage:      N/A
****************************************************************************/
int Get_NASDAQ_RASH_CompletedMessage(void)
{
  int i;

  while (1)
  {   
    NASDAQ_RASH_Config.remainingBytes = NASDAQ_RASH_Config.numOfByteReceived - NASDAQ_RASH_Config.startIndex;

    if (NASDAQ_RASH_Config.remainingBytes > 0)
    {     
      //TraceLog(DEBUG_LEVEL, "remainingBytes = %d\n", NASDAQ_RASH_Config.remainingBytes);

      switch (NASDAQ_RASH_Config.buffer[NASDAQ_RASH_Config.startIndex])
      {
        case 'S':
          if (NASDAQ_RASH_Config.remainingBytes < 10)
          {
            if ((NASDAQ_RASH_Config.remainingBytes == 2) && 
            (NASDAQ_RASH_Config.buffer[NASDAQ_RASH_Config.startIndex + 1] == 10))
            {             
              //Sequenced Data Packet 
              NASDAQ_RASH_Config.startIndex = 2;
              return 8;
            }
            
            if (Receive_NASDAQ_RASH_CompleteMessage(12) == ERROR)
            {
              return ERROR;
            }
          }
          else
          {
            switch (NASDAQ_RASH_Config.buffer[NASDAQ_RASH_Config.startIndex + 9])
            {
              //Accepted Order Message
              case NASDAQ_RASH_ORDER_ACCEPTED:
                if (NASDAQ_RASH_Config.remainingBytes >= NASDAQ_RASH_ORDER_ACCEPTED_LEN)
                {
                  NASDAQ_RASH_Config.incomingSeqNum++;

                  NASDAQ_RASH_Config.startIndex += NASDAQ_RASH_ORDER_ACCEPTED_LEN;
                  
                  return NASDAQ_RASH_ORDER_ACCEPTED;
                }
                else
                {
                  if (Receive_NASDAQ_RASH_CompleteMessage(NASDAQ_RASH_ORDER_ACCEPTED_LEN) == ERROR)
                  {
                    return ERROR;
                  }
                }                             
                break;
              
              //Executed Order Message
              case NASDAQ_RASH_ORDER_EXECUTED:
                if (NASDAQ_RASH_Config.remainingBytes >= NASDAQ_RASH_ORDER_EXECUTED_LEN)
                {
                  NASDAQ_RASH_Config.incomingSeqNum++;

                  NASDAQ_RASH_Config.startIndex += NASDAQ_RASH_ORDER_EXECUTED_LEN;
                  
                  return NASDAQ_RASH_ORDER_EXECUTED;
                }
                else
                {
                  if (Receive_NASDAQ_RASH_CompleteMessage(NASDAQ_RASH_ORDER_EXECUTED_LEN) == ERROR)
                  {
                    return ERROR;
                  }
                }
                break;
              
              //Cancel Order Message
              case NASDAQ_RASH_ORDER_CANCELED:
                if (NASDAQ_RASH_Config.remainingBytes >= NASDAQ_RASH_ORDER_CANCELED_LEN)
                {
                  NASDAQ_RASH_Config.incomingSeqNum++;

                  NASDAQ_RASH_Config.startIndex += NASDAQ_RASH_ORDER_CANCELED_LEN;
                  
                  return NASDAQ_RASH_ORDER_CANCELED;
                }
                else
                {
                  if (Receive_NASDAQ_RASH_CompleteMessage(NASDAQ_RASH_ORDER_CANCELED_LEN) == ERROR)
                  {
                    return ERROR;
                  }
                }
                break;

              //Rejected Order Message
              case NASDAQ_RASH_ORDER_REJECTED:
                if (NASDAQ_RASH_Config.remainingBytes >= NASDAQ_RASH_ORDER_REJECTED_LEN)
                {
                  NASDAQ_RASH_Config.incomingSeqNum++;

                  NASDAQ_RASH_Config.startIndex += NASDAQ_RASH_ORDER_REJECTED_LEN;
                  
                  return NASDAQ_RASH_ORDER_REJECTED;
                }
                else
                {
                  if (Receive_NASDAQ_RASH_CompleteMessage(NASDAQ_RASH_ORDER_REJECTED_LEN) == ERROR)
                  {
                    return ERROR;
                  }
                }
                break;

              //Broken Trade Order Message
              case NASDAQ_RASH_BROKEN_TRADE:
                if (NASDAQ_RASH_Config.remainingBytes >= NASDAQ_RASH_BROKEN_TRADE_LEN)
                {
                  NASDAQ_RASH_Config.incomingSeqNum++;

                  NASDAQ_RASH_Config.startIndex += NASDAQ_RASH_BROKEN_TRADE_LEN;
                  
                  return NASDAQ_RASH_BROKEN_TRADE;
                }
                else
                {
                  if (Receive_NASDAQ_RASH_CompleteMessage(NASDAQ_RASH_BROKEN_TRADE_LEN) == ERROR)
                  {
                    return ERROR;
                  }
                }
                break;

              //System Event Message
              case NASDAQ_RASH_SYSTEM_EVENT:
                if (NASDAQ_RASH_Config.remainingBytes >= NASDAQ_RASH_SYSTEM_EVENT_LEN)
                {
                  NASDAQ_RASH_Config.incomingSeqNum++;

                  NASDAQ_RASH_Config.startIndex += NASDAQ_RASH_SYSTEM_EVENT_LEN;
                  
                  return NASDAQ_RASH_SYSTEM_EVENT;
                }
                else
                {
                  if (Receive_NASDAQ_RASH_CompleteMessage(NASDAQ_RASH_SYSTEM_EVENT_LEN) == ERROR)
                  {
                    return ERROR;
                  }
                }
                break;

              default:
                TraceLog(ERROR_LEVEL, "Unknown NASDAQ RASH message type\n");
                return -1;
            }
          }
          break;

        //Heartbeat Request
        case NASDAQ_RASH_HEARTBEAT:
          if (NASDAQ_RASH_Config.remainingBytes >= NASDAQ_RASH_HEARTBEAT_LEN)
          {
            NASDAQ_RASH_Config.startIndex += NASDAQ_RASH_HEARTBEAT_LEN;
            return NASDAQ_RASH_HEARTBEAT;
          }
          else
          {
            if (Receive_NASDAQ_RASH_CompleteMessage(NASDAQ_RASH_HEARTBEAT_LEN) == ERROR)
            {
              return ERROR;
            }
          }
          
          break;
        
        //Debug Packet
        case '+':
          i = NASDAQ_RASH_Config.startIndex;
          while (NASDAQ_RASH_Config.buffer[i] != 10)
          {
            if (i >= (NASDAQ_RASH_Config.numOfByteReceived - 1))
            {
              if (Receive_NASDAQ_RASH_CompleteMessage(1) == ERROR)
              {
                return ERROR;
              }
            }
            else
            {
              i += 1;
            }
          }

          //Debug Packet
          NASDAQ_RASH_Config.startIndex = i + 1;
          return NASDAQ_RASH_DEBUG;

        //Login Accepted Packet
        case NASDAQ_RASH_LOGIN_ACCEPTED:
          if (NASDAQ_RASH_Config.remainingBytes >= NASDAQ_RASH_LOGIN_ACCEPTED_LEN)
          {
            NASDAQ_RASH_Config.startIndex += NASDAQ_RASH_LOGIN_ACCEPTED_LEN;
            return NASDAQ_RASH_LOGIN_ACCEPTED_RETURN; 
          }
          else
          {
            if (Receive_NASDAQ_RASH_CompleteMessage(NASDAQ_RASH_LOGIN_ACCEPTED_LEN) == ERROR)
            {
              return ERROR;
            }
          }
          
          break;  
          
        //Login Rejected Packet
        case NASDAQ_RASH_LOGIN_REJECTED:
          if (NASDAQ_RASH_Config.remainingBytes >= NASDAQ_RASH_LOGIN_REJECTED_LEN)
          {
            NASDAQ_RASH_Config.startIndex += NASDAQ_RASH_LOGIN_REJECTED_LEN;
            return NASDAQ_RASH_LOGIN_REJECTED_RETURN; 
          }
          else
          {
            if (Receive_NASDAQ_RASH_CompleteMessage(NASDAQ_RASH_LOGIN_ACCEPTED_LEN) == ERROR)
            {
              return ERROR;
            }
          }
          break;
        
        default:
          TraceLog(ERROR_LEVEL, "Unknown NASDAQ RASH messag type\n");
          return -1;
      }
    }
    else
    { 
      if (NASDAQ_RASH_Config.numOfByteReceived >= HALF_MAX_BUFFER_LEN)
      {
        NASDAQ_RASH_Config.isUnCompletedMsg = NO;
        
        NASDAQ_RASH_Config.numOfBytesToRecv = HALF_MAX_BUFFER_LEN;

        if (Receive_NASDAQ_RASH_Msg() == -1)
        {
          return -1;
        }
      }
      else
      {
        NASDAQ_RASH_Config.numOfBytesToRecv = HALF_MAX_BUFFER_LEN - NASDAQ_RASH_Config.numOfByteReceived;

        if (Receive_NASDAQ_RASH_Msg() == -1)
        {
          return -1;
        }
      }   
    }
  }
}

/****************************************************************************
- Function name:  Receive_NASDAQ_RASH_CompleteMessage
- Input:      N/A
- Output:   N/A
- Return:   Success or Failure
- Description:  + The routine hides the following facts
          - Try to receive a complete message
        + There are no preconditions guaranteed to the routine
        + The routine guarantees that the return value will 
            have a value of either
          - Success
          or 
          - Failure
- Usage:      N/A
****************************************************************************/
int Receive_NASDAQ_RASH_CompleteMessage(int msgLen)
{ 
  if (NASDAQ_RASH_Config.numOfByteReceived >= HALF_MAX_BUFFER_LEN)
  {
    NASDAQ_RASH_Config.isUnCompletedMsg = YES;

    NASDAQ_RASH_Config.numOfBytesToRecv = msgLen - NASDAQ_RASH_Config.remainingBytes;

    if (Receive_NASDAQ_RASH_Msg() == ERROR)
    {
      return ERROR;
    }
  }
  else
  {
    NASDAQ_RASH_Config.numOfBytesToRecv = HALF_MAX_BUFFER_LEN - NASDAQ_RASH_Config.numOfByteReceived;

    if (Receive_NASDAQ_RASH_Msg() == ERROR)
    {
      return ERROR;
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  Send_NASDAQ_RASH_Msg
- Input:      + message
        + msgLen
- Output:   N/A
- Return:   Success or Failure
- Description:  + The routine hides the following facts
          - Send message to server
        + There are preconditions guaranteed to the routine
          -   message and msgLen must be checked before
        + The routine guarantees that the return value will 
            have a value of
          - message length
- Usage:      N/A
****************************************************************************/
int Send_NASDAQ_RASH_Msg(const char *message, int msgLen)
{
  //TraceLog(DEBUG_LEVEL, "Inside Send_NASDAQ_RASH_Msg function\n");
  //TraceLog(DEBUG_LEVEL, "message = %s\n", message); 
  //TraceLog(DEBUG_LEVEL, "msgLen = %d\n", msgLen);
  
  int retValue = send(NASDAQ_RASH_Config.socket, message, msgLen, 0);
  
  if (retValue != msgLen)
  {
    return ERROR;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  Add_NASDAQ_RASH_DataBlockToCollection
- Input:      + dataBlock
- Output:   NASDAQ RASH collection will be updated with new data block
- Return:   N/A
- Description:  + The routine hides the following facts
          - Update block length for data block
          - Update additional info
          - Add data block to collection
          - Update incoming Seq Number
        + There are preconditions guaranteed to the routine
          -   dataBlock must be allocated and initialized before
        + The routine guarantees that the return value will 
            have a value of either
          - Success
          or 
          - Failure
- Usage:      N/A
****************************************************************************/
void Add_NASDAQ_RASH_DataBlockToCollection(t_DataBlock *dataBlock, int clOrdId)
{
  // Update Block Length
  dataBlock->blockLen = 4 + 4 + dataBlock->msgLen + 4 + DATABLOCK_ADDITIONAL_INFO_LEN;
  
  // Update additional content 
  AddDataBlockTime(&dataBlock->addContent[DATABLOCK_OFFSET_APP_TIME]);
  memcpy(&dataBlock->addContent[DATABLOCK_OFFSET_FILTER_TIME], &dataBlock->addContent[DATABLOCK_OFFSET_APP_TIME], 8);

  if (clOrdId < 500000) // AS manual order
  {
    // Add data block to collection
    AddDataBlockToCollection(dataBlock, &asRawDataMgmt[AS_NASDAQ_RASH_INDEX]);
    
    // Save data block to file
    SaveDataBlockToFile(&asRawDataMgmt[AS_NASDAQ_RASH_INDEX]);
  }
  else // PM order
  {
    // Add data block to collection
    AddDataBlockToCollection(dataBlock, &pmRawDataMgmt[PM_NASDAQ_RASH_INDEX]);
    
    // Save data block to file
    SaveDataBlockToFile(&pmRawDataMgmt[PM_NASDAQ_RASH_INDEX]);
  }
}

/****************************************************************************
- Function name:  SaveNASDAQ_RASH_SeqNumToFile
- Input:      N/A
- Output:   N/A
- Return:   Success or Failure
- Description:  + The routine hides the following facts
        + There are no preconditions guaranteed to the routine
        + The routine guarantees that the status value will 
          have a value of either
          - Success
          or
          - Failure
- Usage:      N/A
****************************************************************************/
int SaveNASDAQ_RASH_SeqNumToFile(void)
{
  return SaveSeqNumToFile("nasdaq_rash_seqs", AS_NASDAQ_RASH_INDEX, &NASDAQ_RASH_Config);
}
