/*****************************************************************************
**  Project:    Equity Arbitrage Application
**  Filename:   daedalus_proc.c
**  Description:  This file contains function definitions that were declared
          in daedalus_proc.h  
**  Author:     Long Nguyen-Khoa-Hai
**  First created:  01-Oct-2013
**  Last updated: -----------
*****************************************************************************/

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <errno.h>

#include "socket_util.h"
#include "configuration.h"
#include "order_mgmt_proc.h"
#include "raw_data_mgmt.h"
#include "output_log_mgmt.h"
#include "nasdaq_rash_proc.h"
#include "arca_direct_proc.h"
#include "position_manager_proc.h"
#include "trade_servers_proc.h"
#include "trade_servers_mgmt_proc.h"
#include "daedalus_proc.h"
#include "database_util.h"
#include "internal_proc.h"
#include "trader_tools_proc.h"
#include "hbitime.h"

#define TEN_RAISE_TO_FOUR 10000
#define TEN_DOWN_TO_FOUR 0.0001

extern int PMtoTTCancelLimit;
extern t_VolatileMgmt volatileMgmt;
t_TraderCollection traderCollection;

// Mapping table to get book index from order index
int TS_ORDER_INDEX_TO_BOOK_MAPPING[]
  = { TS_ARCA_BOOK_INDEX,
    TS_NASDAQ_BOOK_INDEX,
    TS_NYSE_BOOK_INDEX,
    TS_NASDAQ_BOOK_INDEX,
    TS_BATSZ_BOOK_INDEX,
    TS_EDGX_BOOK_INDEX,
    TS_EDGA_BOOK_INDEX,
    TS_NDBX_BOOK_INDEX,
    TS_BYX_BOOK_INDEX,
    TS_PSX_BOOK_INDEX,
    -1};
    
// Mapping table to get Daedalus MDC Index from TS Book index
int TS_BOOK_INDEX_TO_DAEDALUS_MDC_MAPPING[]
  = { COM__RGM__DAEDALUS__PROTO__MDC__ARCA_MDC, //TS_ARCA_BOOK_INDEX
    COM__RGM__DAEDALUS__PROTO__MDC__NASDAQ_MDC, //TS_NASDAQ_BOOK_INDEX
    COM__RGM__DAEDALUS__PROTO__MDC__NYSE_MDC, //TS_NYSE_BOOK_INDEX
    COM__RGM__DAEDALUS__PROTO__MDC__BZX_MDC,  //TS_BATSZ_BOOK_INDEX
    COM__RGM__DAEDALUS__PROTO__MDC__EDGX_MDC, //TS_EDGX_BOOK_INDEX
    COM__RGM__DAEDALUS__PROTO__MDC__EDGA_MDC, //TS_EDGA_BOOK_INDEX
    COM__RGM__DAEDALUS__PROTO__MDC__BX_MDC,   //TS_NDBX_BOOK_INDEX
    COM__RGM__DAEDALUS__PROTO__MDC__BYX_MDC, // TS_BYX_BOOK_INDEX
    COM__RGM__DAEDALUS__PROTO__MDC__PSX_MDC, // TS_PSX_BOOK_INDEX
    COM__RGM__DAEDALUS__PROTO__MDC__AMEX_MDC, // TS_AMEX_BOOK_INDEX
    -1};

// Mapping table to get book index from Daedalus's MDC index
int DAEDALUS_MDC_INDEX_TO_TS_BOOK_MAPPING[]
  = { -1, //COM__RGM__DAEDALUS__PROTO__MDC__NO_MDC
    -1, //COM__RGM__DAEDALUS__PROTO__MDC__ALL_MDC
    TS_ARCA_BOOK_INDEX, //COM__RGM__DAEDALUS__PROTO__MDC__ARCA_MDC
    TS_BATSZ_BOOK_INDEX, //COM__RGM__DAEDALUS__PROTO__MDC__BZX_MDC
    TS_BYX_BOOK_INDEX, //COM__RGM__DAEDALUS__PROTO__MDC__BYX_MDC
    -1, //COM__RGM__DAEDALUS__PROTO__MDC__CQS_MDC
    -1, //COM__RGM__DAEDALUS__PROTO__MDC__CTS_MDC
    TS_EDGA_BOOK_INDEX, //COM__RGM__DAEDALUS__PROTO__MDC__EDGA_MDC
    TS_EDGX_BOOK_INDEX, //COM__RGM__DAEDALUS__PROTO__MDC__EDGX_MDC
    TS_NYSE_BOOK_INDEX, //COM__RGM__DAEDALUS__PROTO__MDC__NYSE_MDC
    TS_NASDAQ_BOOK_INDEX, //COM__RGM__DAEDALUS__PROTO__MDC__NASDAQ_MDC
    TS_NDBX_BOOK_INDEX, //COM__RGM__DAEDALUS__PROTO__MDC__BX_MDC
    TS_PSX_BOOK_INDEX, //COM__RGM__DAEDALUS__PROTO__MDC__PSX_MDC
    -1, //COM__RGM__DAEDALUS__PROTO__MDC__UTDF_MDC
    -1, //COM__RGM__DAEDALUS__PROTO__MDC__UQDF_MDC
    -1, // COM__RGM__DAEDALUS__PROTO__MDC__TSE_MDC
    -1, // COM__RGM__DAEDALUS__PROTO__MDC__CHX_MDC
    -1, // COM__RGM__DAEDALUS__PROTO__MDC__JNX_MDC
    TS_AMEX_BOOK_INDEX, // COM__RGM__DAEDALUS__PROTO__MDC__AMEX_MDC
    -1, // COM__RGM__DAEDALUS__PROTO__MDC__LAVA_MDC
    -1};

// Mapping table to get Daedalus MDC Index from PM Book index
int PM_BOOK_INDEX_TO_DAEDALUS_MDC_MAPPING[]
  = { COM__RGM__DAEDALUS__PROTO__MDC__ARCA_MDC, //PM_ARCA_BOOK_INDEX
    COM__RGM__DAEDALUS__PROTO__MDC__NASDAQ_MDC, //PM_NASDAQ_BOOK_INDEX
    -1};
    
// Mapping table to get book index from Daedalus's MDC index
int DAEDALUS_MDC_INDEX_TO_PM_BOOK_MAPPING[]
  = { -1, //COM__RGM__DAEDALUS__PROTO__MDC__NO_MDC
    -1, //COM__RGM__DAEDALUS__PROTO__MDC__ALL_MDC
    PM_ARCA_BOOK_INDEX, //COM__RGM__DAEDALUS__PROTO__MDC__ARCA_MDC
    -1, //COM__RGM__DAEDALUS__PROTO__MDC__BZX_MDC
    -1, //COM__RGM__DAEDALUS__PROTO__MDC__BYX_MDC
    -1, //COM__RGM__DAEDALUS__PROTO__MDC__CQS_MDC
    -1, //COM__RGM__DAEDALUS__PROTO__MDC__CTS_MDC
    -1, //COM__RGM__DAEDALUS__PROTO__MDC__EDGA_MDC
    -1, //COM__RGM__DAEDALUS__PROTO__MDC__EDGX_MDC
    -1, //COM__RGM__DAEDALUS__PROTO__MDC__NYSE_MDC
    PM_NASDAQ_BOOK_INDEX, //COM__RGM__DAEDALUS__PROTO__MDC__NASDAQ_MDC
    -1, //COM__RGM__DAEDALUS__PROTO__MDC__BX_MDC
    -1, //COM__RGM__DAEDALUS__PROTO__MDC__PSX_MDC
    -1, //COM__RGM__DAEDALUS__PROTO__MDC__UTDF_MDC
    -1, //COM__RGM__DAEDALUS__PROTO__MDC__UQDF_MDC
    -1};
    
// Mapping table to get order index from Daedalus's OEC index
int DAEDALUS_OEC_INDEX_TO_TS_ORDER_MAPPING[]
  = { -1, //COM__RGM__DAEDALUS__PROTO__OEC__NO_OEC
    -1, //COM__RGM__DAEDALUS__PROTO__OEC__ALL_OEC
    TS_ARCA_DIRECT_INDEX, //COM__RGM__DAEDALUS__PROTO__OEC__ARCA_DIRECT_OEC
    TS_BATSZ_BOE_INDEX, //COM__RGM__DAEDALUS__PROTO__OEC__BZX_OEC
    TS_BYX_BOE_INDEX, //COM__RGM__DAEDALUS__PROTO__OEC__BYX_OEC
    TS_EDGA_DIRECT_INDEX, //COM__RGM__DAEDALUS__PROTO__OEC__EDGA_OEC
    TS_EDGX_DIRECT_INDEX, //COM__RGM__DAEDALUS__PROTO__OEC__EDGX_OEC
    TS_NYSE_CCG_INDEX, //COM__RGM__DAEDALUS__PROTO__OEC__NYSE_OEC
    TS_NASDAQ_OUCH_INDEX, //COM__RGM__DAEDALUS__PROTO__OEC__NASDAQ_OEC
    TS_NDAQ_OUBX_INDEX, //COM__RGM__DAEDALUS__PROTO__OEC__BX_OEC
    TS_PSX_OUCH_INDEX, //COM__RGM__DAEDALUS__PROTO__OEC__PSX_OEC
    TS_NASDAQ_RASH_INDEX, //COM__RGM__DAEDALUS__PROTO__OEC__RASH_OEC
    -1, //COM__RGM__DAEDALUS__PROTO__OEC__ARCA_FIX_OEC
    -1};

// Mapping table to get order index from Daedalus's OEC index
int DAEDALUS_OEC_INDEX_TO_PM_ORDER_MAPPING[]
  = { -1, //COM__RGM__DAEDALUS__PROTO__OEC__NO_OEC
    -1, //COM__RGM__DAEDALUS__PROTO__OEC__ALL_OEC
    PM_ARCA_DIRECT_INDEX, //COM__RGM__DAEDALUS__PROTO__OEC__ARCA_DIRECT_OEC
    -1, //COM__RGM__DAEDALUS__PROTO__OEC__BZX_OEC
    -1, //COM__RGM__DAEDALUS__PROTO__OEC__BYX_OEC
    -1, //COM__RGM__DAEDALUS__PROTO__OEC__EDGA_OEC
    -1, //COM__RGM__DAEDALUS__PROTO__OEC__EDGX_OEC
    -1, //COM__RGM__DAEDALUS__PROTO__OEC__NYSE_OEC
    -1, //COM__RGM__DAEDALUS__PROTO__OEC__NASDAQ_OEC
    -1, //COM__RGM__DAEDALUS__PROTO__OEC__BX_OEC
    -1, //COM__RGM__DAEDALUS__PROTO__OEC__PSX_OEC
    PM_NASDAQ_RASH_INDEX, //COM__RGM__DAEDALUS__PROTO__OEC__RASH_OEC
    -1, //COM__RGM__DAEDALUS__PROTO__OEC__ARCA_FIX_OEC
    -1};
    
// Mapping table to get order index from Daedalus's OEC index
int DAEDALUS_OEC_INDEX_TO_AS_ORDER_MAPPING[]
  = { -1, //COM__RGM__DAEDALUS__PROTO__OEC__NO_OEC
    -1, //COM__RGM__DAEDALUS__PROTO__OEC__ALL_OEC
    AS_ARCA_DIRECT_INDEX, //COM__RGM__DAEDALUS__PROTO__OEC__ARCA_DIRECT_OEC
    -1, //COM__RGM__DAEDALUS__PROTO__OEC__BZX_OEC
    -1, //COM__RGM__DAEDALUS__PROTO__OEC__BYX_OEC
    -1, //COM__RGM__DAEDALUS__PROTO__OEC__EDGA_OEC
    -1, //COM__RGM__DAEDALUS__PROTO__OEC__EDGX_OEC
    -1, //COM__RGM__DAEDALUS__PROTO__OEC__NYSE_OEC
    -1, //COM__RGM__DAEDALUS__PROTO__OEC__NASDAQ_OEC
    -1, //COM__RGM__DAEDALUS__PROTO__OEC__BX_OEC
    -1, //COM__RGM__DAEDALUS__PROTO__OEC__PSX_OEC
    AS_NASDAQ_RASH_INDEX, //COM__RGM__DAEDALUS__PROTO__OEC__RASH_OEC
    -1, //COM__RGM__DAEDALUS__PROTO__OEC__ARCA_FIX_OEC
    -1};
                        
char* TS_SERVER_ROLE_NAME_BY_INDEX[]
   = {"ARCA", // ARCA_SERVER_ROLE 0
    "NDAQ", // NASDAQ_SERVER_ROLE 1
    "BATZ", //BATS_SERVER_ROLE 2
    "EDGX", //EDGX_SERVER_ROLE 3
    "STAND ALONE", // STANDALONE_SERVER_ROLE 4
    "UNKNOWN"};
    
/*char* MDC_NAME_BY_DAEDALUS_MDC_INDEX[]
  = { "",     //COM__RGM__DAEDALUS__PROTO__MDC__NO_MDC = 0,
    "ALL MDC",  //COM__RGM__DAEDALUS__PROTO__MDC__ALL_MDC = 1,
    "ARCA",   //COM__RGM__DAEDALUS__PROTO__MDC__ARCA_MDC = 2,
    "BATSZ",  //COM__RGM__DAEDALUS__PROTO__MDC__BZX_MDC = 3,
    "",     //COM__RGM__DAEDALUS__PROTO__MDC__BYX_MDC = 4,
    "CQS",    //COM__RGM__DAEDALUS__PROTO__MDC__CQS_MDC = 5,
    "CTS",    //COM__RGM__DAEDALUS__PROTO__MDC__CTS_MDC = 6,
    "",     //COM__RGM__DAEDALUS__PROTO__MDC__EDGA_MDC = 7,
    "EDGX",   //COM__RGM__DAEDALUS__PROTO__MDC__EDGX_MDC = 8,
    "NYSE",   //COM__RGM__DAEDALUS__PROTO__MDC__NYSE_MDC = 9,
    "NASDAQ", //COM__RGM__DAEDALUS__PROTO__MDC__NASDAQ_MDC = 10,
    "",     //COM__RGM__DAEDALUS__PROTO__MDC__BX_MDC = 11,
    "",     //COM__RGM__DAEDALUS__PROTO__MDC__PSX_MDC = 12,
    "UTDF",   //COM__RGM__DAEDALUS__PROTO__MDC__UTDF_MDC = 13,
    "UQDF"    //COM__RGM__DAEDALUS__PROTO__MDC__UQDF_MDC = 14
    };*/

// Mapping table to get OEC from AS centralized order types
int AS_CENTRALIZED_ORDER_TYPE_TO_OEC_MAPPING[]
  = { COM__RGM__DAEDALUS__PROTO__OEC__ARCA_DIRECT_OEC,  //TYPE_ARCA_DIRECT(100) - 100
    COM__RGM__DAEDALUS__PROTO__OEC__NASDAQ_OEC,     //TYPE_NASDAQ_OUCH(101) - 100
    COM__RGM__DAEDALUS__PROTO__OEC__RASH_OEC,     //TYPE_NASDAQ_RASH(102) - 100
    COM__RGM__DAEDALUS__PROTO__OEC__NYSE_OEC,     //TYPE_NYSE_CCG(103) - 100
    COM__RGM__DAEDALUS__PROTO__OEC__BZX_OEC,      //TYPE_BZX_BOE(104) - 100
    COM__RGM__DAEDALUS__PROTO__OEC__EDGX_OEC,     //TYPE_EDGX_DIRECT(105) - 100
    COM__RGM__DAEDALUS__PROTO__OEC__EDGA_OEC,     //TYPE_EDGA_DIRECT(106) - 100
    COM__RGM__DAEDALUS__PROTO__OEC__BX_OEC,       //TYPE_EDGA_DIRECT(107) - 100
    COM__RGM__DAEDALUS__PROTO__OEC__BYX_OEC,      //TYPE_BYX_BOE(108) - 100
    COM__RGM__DAEDALUS__PROTO__OEC__PSX_OEC,      //TYPE_PSX(109) - 100
    -1};
    
// Mapping table to get Daedalus Order status from AS
int AS_ORDER_STATUS_TO_DAEDALUS_ORDER_STATUS_MAPPING[]
  = { COM__RGM__DAEDALUS__PROTO__ORDER_STATUS__Sent,      //orderStatusIndex.Sent = 0
    COM__RGM__DAEDALUS__PROTO__ORDER_STATUS__Live,      //orderStatusIndex.Live = 1
    COM__RGM__DAEDALUS__PROTO__ORDER_STATUS__Rejected,    //orderStatusIndex.Rejected = 2
    COM__RGM__DAEDALUS__PROTO__ORDER_STATUS__Closed,    //orderStatusIndex.Closed = 3
    COM__RGM__DAEDALUS__PROTO__ORDER_STATUS__Broken,    //orderStatusIndex.BrokenTrade = 4
    COM__RGM__DAEDALUS__PROTO__ORDER_STATUS__CanceledByECN, //orderStatusIndex.CanceledByECN = 5
    COM__RGM__DAEDALUS__PROTO__ORDER_STATUS__CXLSent,   //orderStatusIndex.CXLSent = 6
    COM__RGM__DAEDALUS__PROTO__ORDER_STATUS__CancelACK,   //orderStatusIndex.CancelACK = 7
    COM__RGM__DAEDALUS__PROTO__ORDER_STATUS__CancelRejected,//orderStatusIndex.CancelRejected = 8
    COM__RGM__DAEDALUS__PROTO__ORDER_STATUS__CancelPending, //orderStatusIndex.CancelPending = 9
    COM__RGM__DAEDALUS__PROTO__ORDER_STATUS__CanceledByUser,//orderStatusIndex.CanceledByUser = 10
    -1};
    
// Mapping table to get Account String from Account ID
char* ACCOUNT_STRING_FROM_ACCOUNT_INDEX_MAPPING[]
  = { "1", //FIRST_ACCOUNT 0
    "2", //SECOND_ACCOUNT 0
    "3", //THIRD_ACCOUNT 0
    "4", //FOURTH_ACCOUNT 0
    ""};

int TTIndexUpdatedBP = -1;

// int isValidTTVersion[MAX_TRADER_TOOL_CONNECTIONS];

//For Daedalus requests to connect and enable trading in the same message
extern t_HoldingEnableTradingInfo RequestToEnableTradingTS[MAX_TRADE_SERVER_CONNECTIONS][TS_MAX_ORDER_CONNECTIONS];
extern t_HoldingEnableTradingInfo RequestToEnableTradingPM;

/****************************************************************************
** Function declarations
****************************************************************************/
t_NoTradesDurationMgt NoTradesDurationMgt[MAX_TRADE_SERVER_CONNECTIONS];

int buyingPowerSecondsThreshhold[MAX_TRADE_SERVER_CONNECTIONS];
int PMOrderPerSecond;
t_GTRMgt GTRMgt;
int AlertingProgress = DISABLE;

int NearningStuckLimitCount = -1;
int NearningLossLimitCount = -1;

int StuckLimitAlert = NO;
int LossLimitAlert = NO;

/****************************************************************************
- Function name:  ProcessResetDaedalusParameters
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessResetDaedalusParameters(int ttIndex)
{
  int i, indexOpenOrder, rmInfoIndex;

  for (indexOpenOrder = 0; indexOpenOrder < asOpenOrder.countOpenOrder; indexOpenOrder++)
  {
    if ((asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status == orderStatusIndex.Rejected) ||
      (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status == orderStatusIndex.Closed) ||
      (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status == orderStatusIndex.CanceledByECN) ||
      (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status == orderStatusIndex.CancelRejected) ||
      (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status == orderStatusIndex.CanceledByUser))
    {
      asOpenOrder.orderCollection[indexOpenOrder].isUpdated[ttIndex] = OPEN_ORDER_SENT;
    }
    else
    {
      asOpenOrder.orderCollection[indexOpenOrder].isUpdated[ttIndex] = OPEN_ORDER_UPDATED;
    }
  }

  // Get RM Information from RMList
  for (rmInfoIndex = 0; rmInfoIndex < MAX_SYMBOL_TRADE; rmInfoIndex++)
  {
    for (i = 0; i < MAX_ACCOUNT; i++)
    {
      memset(&RMList.collection[i][rmInfoIndex].oldPositionSent[ttIndex], 0, sizeof(t_PositionBk));
    }
  }

  for (i = 0; i < asCrossCollection.countCross; i++)
  {
    asCrossCollection.crossList[i].isUpdate[ttIndex] = NOT_YET;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ThreadReceiveMsgsFromDaedalus
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
void *ThreadReceiveMsgsFromDaedalus(void *pConnection)
{
  t_TTMessage ttMessage;
  memset(&ttMessage, 0, sizeof(ttMessage));

  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  // TraceLog(DEBUG_LEVEL, "Create completed ThreadReceiveMsgsFromDaedalus thread: socket = %d\n", connection->socket);
  
  Com__Rgm__Daedalus__Proto__Request *requestMsg;
  
  while (1) 
  {
    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1))
    {
      break;
    }

    // Receive a message from Daedalus
    int payLoadLength = ReceiveDaedalusMessage(connection, &ttMessage);
    if (payLoadLength != ERROR)
    {
      // Unpack the message using protobuf-c
      requestMsg = com__rgm__daedalus__proto__request__unpack(NULL, (ttMessage.msgLen - payLoadLength),  &ttMessage.msgContent[payLoadLength]);
      if (requestMsg == NULL)
      {
        TraceLog(ERROR_LEVEL, "Can not unpack Daedalus message\n");
        return NULL;
      }
      if (!requestMsg->has_type)
      {
        TraceLog(ERROR_LEVEL, "Daedalus request message does not contain message type\n");
        return NULL;
      }
      
      Com__Rgm__Daedalus__Proto__Request__RequestType msgType = requestMsg->type;
      // TraceLog(DEBUG_LEVEL, "[%s]Receive Daedalus Msg Type: %d\n", connection->userName, msgType);
      
      switch (msgType)
      {
        case COM__RGM__DAEDALUS__PROTO__REQUEST__REQUEST_TYPE__MDC_CONNECTION:
          TraceLog(DEBUG_LEVEL, "Receive request MDC connection\n");
          ProcessConnectMdcFromDaedalus(requestMsg, connection->index);
          break;
        case COM__RGM__DAEDALUS__PROTO__REQUEST__REQUEST_TYPE__SET_OEC_STATE:
          TraceLog(DEBUG_LEVEL, "Receive request OEC connection\n");
          ProcessConnectOecFromDaedalus(requestMsg, connection->index);
          break;
        case COM__RGM__DAEDALUS__PROTO__REQUEST__REQUEST_TYPE__QUERY_BOOK:
          TraceLog(DEBUG_LEVEL, "[%s]Receive request stock status\n", connection->userName);
          ProcessGetStockStatusFromDaedalus(requestMsg, connection->index);
          break;
        case COM__RGM__DAEDALUS__PROTO__REQUEST__REQUEST_TYPE__CLEAR_BOOK:
          TraceLog(DEBUG_LEVEL, "[%s]Receive request clear book\n", connection->userName);
          ProcessClearBookFromDaedalus(requestMsg, connection->index);
          break;
        case COM__RGM__DAEDALUS__PROTO__REQUEST__REQUEST_TYPE__AS_TS_CONNECTION:
          TraceLog(DEBUG_LEVEL, "Receive Request Server Connection\n");
          ProcessASConnectToServerFromDaedalus(requestMsg, connection->index);
          break;
        case COM__RGM__DAEDALUS__PROTO__REQUEST__REQUEST_TYPE__SET_ISO:
          TraceLog(DEBUG_LEVEL, "Receive Request ISO Trading\n");
          ProcessEnableISOTradingFromDaedalus(requestMsg, connection->index);
          break;
        case COM__RGM__DAEDALUS__PROTO__REQUEST__REQUEST_TYPE__SET_DISABLED_SYMBOL:
          TraceLog(DEBUG_LEVEL, "Receive Request Set Ignored Symbol\n");
          ProcessIgnoreSymbolFromDaedalus(requestMsg, connection->index);
          break;
        case COM__RGM__DAEDALUS__PROTO__REQUEST__REQUEST_TYPE__SET_DISABLED_SYMBOL_RANGE:
          TraceLog(DEBUG_LEVEL, "Receive Request Set Ignored Symbol Range\n");
          ProcessIgnoreSymbolRangeFromDaedalus(requestMsg);
          break;
        case COM__RGM__DAEDALUS__PROTO__REQUEST__REQUEST_TYPE__SET_HALTED_SYMBOL:
          TraceLog(DEBUG_LEVEL, "Receive Request Set Halted Symbol\n");
          ProcessSetHaltedSymbolFromDaedalus(requestMsg, connection->index);
          break;
        case COM__RGM__DAEDALUS__PROTO__REQUEST__REQUEST_TYPE__SET_MDC_STATE:
          TraceLog(DEBUG_LEVEL, "Receive request to Set MDC State\n");
          ProcessSetMdcStateFromDaedalus(requestMsg, connection->index);
          break;
        case COM__RGM__DAEDALUS__PROTO__REQUEST__REQUEST_TYPE__SET_VOLATILE_SYMBOL:
          TraceLog(DEBUG_LEVEL, "Receive request to Set Volatile Symbol\n");
          ProcessSetVolatileSymbolFromDaedalus(requestMsg, connection->index);
          break;
        case COM__RGM__DAEDALUS__PROTO__REQUEST__REQUEST_TYPE__SET_VOLATILE_SYMBOL_BY_SESSION:
          TraceLog(DEBUG_LEVEL, "Receive request to Set Volatile Symbol By Session\n");
          ProcessSetVolatileSymbolBySessionFromDaedalus(requestMsg, connection);
          break;
        case COM__RGM__DAEDALUS__PROTO__REQUEST__REQUEST_TYPE__SET_VOLATILE_SYMBOL_BY_TOGGLE:
          TraceLog(DEBUG_LEVEL, "Receive request to Set Volatile Symbol By Toggle\n");
          ProcessSetVolatileSymbolByToggleFromDaedalus(requestMsg, connection->index);
          break;
        case COM__RGM__DAEDALUS__PROTO__REQUEST__REQUEST_TYPE__SET_ALERT_CONFIG:
          TraceLog(DEBUG_LEVEL, "Receive request to Set Alert Config\n");
          ProcessSetAlertConfigFromDaedalus(requestMsg, connection->index);
          break;
        case COM__RGM__DAEDALUS__PROTO__REQUEST__REQUEST_TYPE__SET_OPERATOR_STATE:
          TraceLog(DEBUG_LEVEL, "Receive request to Set Trader State\n");
          ProcessChangeTraderStateFromDaedalus(requestMsg, connection->index);
          break;
        case COM__RGM__DAEDALUS__PROTO__REQUEST__REQUEST_TYPE__SET_EXCHANGE_CONFIG:
          TraceLog(DEBUG_LEVEL, "Receive request to Set Exchange Config\n");
          ProcessSetExchangeConfigFromDaedalus(requestMsg, connection->index);
          break;
        case COM__RGM__DAEDALUS__PROTO__REQUEST__REQUEST_TYPE__MANUAL_ORDER:
          TraceLog(DEBUG_LEVEL, "Receive request to send manual order\n");
          ProcessSendManualOrderFromDaedalus(requestMsg, connection->index);
          break;
        case COM__RGM__DAEDALUS__PROTO__REQUEST__REQUEST_TYPE__CANCEL_ORDER_REQUEST:
          TraceLog(DEBUG_LEVEL, "Receive request to send Cancel Order\n");
          ProcessSendCancelOrderFromDaedalus(requestMsg, connection->index);
          break;
        case COM__RGM__DAEDALUS__PROTO__REQUEST__REQUEST_TYPE__ENGAGE_POSITION:
          TraceLog(DEBUG_LEVEL, "Receive request to engage position\n");
          ProcessEngagePositionFromDaedalus(requestMsg, connection->index);
          break;
        case COM__RGM__DAEDALUS__PROTO__REQUEST__REQUEST_TYPE__DISENGAGE_POSITION:
          TraceLog(DEBUG_LEVEL, "Receive request to disengage position\n");
          ProcessDisengagePositionFromDaedalus(requestMsg, connection->index);
          break;
        case COM__RGM__DAEDALUS__PROTO__REQUEST__REQUEST_TYPE__BLINK_RESET_CURRENT_STATUS:
          TraceLog(DEBUG_LEVEL, "Receive request to reset current status\n");
          ProcessResetCurrentStatusFromDaedalus(requestMsg, connection->index);
          break;
        case COM__RGM__DAEDALUS__PROTO__REQUEST__REQUEST_TYPE__ETB_REFRESH:
          ProcessEtbListRequestFromDaedalus();
          break;
        case COM__RGM__DAEDALUS__PROTO__REQUEST__REQUEST_TYPE__EMERGENCY_STOP:
          ProcessEmergencyStopRequestFromDaedalus();
          break;
        case COM__RGM__DAEDALUS__PROTO__REQUEST__REQUEST_TYPE__HEARTBEAT:
          SendHeartbeatToDaedalus(connection);
          break;
        case COM__RGM__DAEDALUS__PROTO__REQUEST__REQUEST_TYPE__UNKNOWN_REQUEST:
          TraceLog(WARN_LEVEL, "Unknow message type from Daedalus: %d\n", msgType);
          break;
        default: // Invalid message
          TraceLog(ERROR_LEVEL, "[%s] Invalid message type from Daedalus: %d\n", connection->userName, msgType);
          break;
      }
      
      // Free the unpacked message
      com__rgm__daedalus__proto__request__free_unpacked(requestMsg, NULL);
    }
    else
    {
      TraceLog(DEBUG_LEVEL, "%s's Daedalus is disconnected\n", connection->userName);

      if (connection->statusUpdate == USER_LOGGED || connection->statusUpdate == USER_PASSWORD_INCORRECT)
      {
        break;
      }
      
      char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
      sprintf(activityDetail, "%d", connection->currentState);
      
      //Trader disconnect from AS
      ProcessUpdateTTEventLogToDatabase(connection->userName, ACTIVITY_TYPE_DISCONNECT_FROM_AS, activityDetail);
      
      //Update trader status to offline
      ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);
      
      //connection->currentState = OFFLINE;

      CloseConnection(connection);
      
      break;
    }
  }

  // Find index for 
  int i, countConnection = 0;

  // Check number of connections for Daedalus
  for (i = 0; i < MAX_TRADER_TOOL_CONNECTIONS; i++)
  {
    if (traderToolsInfo.connection[i].status == CONNECT)
    {
      countConnection ++;
    }
  }

  // Check auto disable trading
  if (countConnection == 0)
  {
    TraceLog(DEBUG_LEVEL, "No Daedalus connection to AS --> Processing send disable trading to all TS\n");

    // Call function to disable trading on all ECN order
    SendDisableTradingAllOrderToAllTS(TS_TRADING_DISABLED_BY_NO_TT_CONNECTION);

    // Call function to disable PM
    SendDisableTradingPMToPM(PM_TRADING_DISABLED_BY_NO_TT_CONNECTION);
  }

  return NULL;
}

/****************************************************************************
- Function name:  ReceiveDaedalusMessage
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ReceiveDaedalusMessage(t_ConnectionStatus *connection, void *message)
{

  // Get message body length
  
  // Get the msg body length from data framing
  // this technique to parse message length is not correct
  // becase the first bit of each byte is just marked as 1 (this means we have 1xxx xxxx)
  // it does not require exactly the same with 0x82
  // but for now, the current solution is acceptable
  // IMPORTANT: when implementing websocket, must take care of this
  
  /*
     ___________________________________________________________________________ 
    | header |      payload         | 4bytes   |  protobuf    | 
    |(2bytes)|   0bytes || 4bytes || 10 bytes |  NULL    |          |
    |________|__________________________________|____________|__________________|
  */
  
  int receivedBytes = 0,
    numReceivedBytes = 0;

  t_TTMessage *ttMessage = (t_TTMessage *)message;
  
  // Timeout counter
  int timeoutCounter = 0;
  
  // Initialize message
  ttMessage->msgLen = 0;
  
  // Try to receive message header
  while (numReceivedBytes < DAEDALUS_MSG_HEADER_LEN)
  {
    receivedBytes = recv(connection->socket, &ttMessage->msgContent[numReceivedBytes], DAEDALUS_MSG_HEADER_LEN - numReceivedBytes, 0);

    if (receivedBytes > 0)
    {
      timeoutCounter = 0;
      
      numReceivedBytes += receivedBytes;
    }
    else
    {
      if ((receivedBytes == 0) && (errno == 0))
      {
        TraceLog(ERROR_LEVEL, "%s's Daedalus: Socket is close\n", connection->userName);
        return ERROR;
      }
      else
      {       
        if (timeoutCounter < 5)
        {
          timeoutCounter++;
          TraceLog(DEBUG_LEVEL, "%s's Daedalus: Waiting. Sleep num = %d\n", connection->userName, timeoutCounter);
        }
        else
        {
          TraceLog(ERROR_LEVEL, "%s's Daedalus: TIME OUT!\n", connection->userName);
          return ERROR;
        }
      }
    }
  }
  
  unsigned char b = ttMessage->msgContent[0];
  if ((b&0x0f) != 0x02)
  {
    TraceLog(ERROR_LEVEL, "Bad websocket binary data header\n");
    return ERROR;
  }
  
  b = ttMessage->msgContent[1] & 0x7f;
  int payLoadLength = 0;
  if(b <= 125)
  {
    payLoadLength = 6;
  }
  else if(b == 126)
  {
    payLoadLength = 8;
  }
  else
  {
    payLoadLength = 14;
  }
  
  // Try to receive message payload
  while (numReceivedBytes < payLoadLength)
  {
    receivedBytes = recv(connection->socket, &ttMessage->msgContent[numReceivedBytes], payLoadLength - numReceivedBytes, 0);
    
    if (receivedBytes > 0)
    {
      timeoutCounter = 0;
      numReceivedBytes += receivedBytes;
    }
    else
    {
      if ((receivedBytes == 0) && (errno == 0))
      {
        TraceLog(ERROR_LEVEL, "%s's Daedalus: Socket is close\n", connection->userName);
        return ERROR;
      }
      else
      {       
        if (timeoutCounter < 5)
        {
          timeoutCounter++;
          TraceLog(DEBUG_LEVEL, "%s's Daedalus: Waiting. Sleep num = %d\n", connection->userName, timeoutCounter);
        }
        else
        {
          TraceLog(ERROR_LEVEL, "%s's Daedalus: TIME OUT!\n", connection->userName);
          return ERROR;
        }
      }
    }
  }
  
  if(payLoadLength == 6)
  {
    ttMessage->msgLen = ttMessage->msgContent[1] & 0x7f;
  }
  else
  {
    int i;
    int range = payLoadLength - 4;
    
    for (i = DAEDALUS_MSG_HEADER_LEN; i < range; i++)
    {
      ttMessage->msgLen <<= 8;
      ttMessage->msgLen |= ttMessage->msgContent[i];
    }
  }
  
  ttMessage->msgLen += payLoadLength;
  
  // Try to receive message body
  while (numReceivedBytes < ttMessage->msgLen)
  {
    receivedBytes = recv(connection->socket, &ttMessage->msgContent[numReceivedBytes], ttMessage->msgLen - numReceivedBytes, 0);
    
    if (receivedBytes > 0)
    {
      timeoutCounter = 0;
      numReceivedBytes += receivedBytes;
    }
    else
    {
      if ((receivedBytes == 0) && (errno == 0))
      {
        TraceLog(ERROR_LEVEL, "%s's Daedalus: Socket is close\n", connection->userName);
        return ERROR;
      }
      else
      {       
        if (timeoutCounter < 5)
        {
          timeoutCounter++;
          TraceLog(DEBUG_LEVEL, "%s's Daedalus: Waiting. Sleep num = %d\n", connection->userName, timeoutCounter);
        }
        else
        {
          TraceLog(ERROR_LEVEL, "%s's Daedalus: TIME OUT!\n", connection->userName);
          return ERROR;
        }
      }
    }
  }

  // Succesfully received
  return payLoadLength;
}

/****************************************************************************
- Function name:  ThreadSendMsgsToDaedalus
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
void *ThreadSendMsgsToDaedalus(void *pConnection)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;
  
  // isValidTTVersion[connection->index] = 0;
  
  // TraceLog(DEBUG_LEVEL, "userName = '%s', password = '%s'\n", connection->userName, connection->password);
  // TraceLog(DEBUG_LEVEL, "*** ttIndex = %d\n", connection->index);
  
  // TraceLog(DEBUG_LEVEL, "Create completed ThreadSendMsgsToDaedalus thread: socket = %d\n", connection->socket);
    
  //Send message to permit trader to TT
  if (SendMessageForPermitTraderLoginToDaedalus(connection->index, connection->userName, SUCCESS) == SUCCESS)
  {
    //Update trader status to monitoring
    ProcessUpdateTTEventLogToDatabase(connection->userName, ACTIVITY_TYPE_CONNECT_TO_AS, "0");
    
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    
    //Get all trader status
    GetTraderStatusInfo();
    
    int traderIndex;
    int allCurrentStatus = OFFLINE;
    for (traderIndex = 0; traderIndex < traderCollection.numberTrader; traderIndex++)
    {
      if (traderCollection.traderStatusInfo[traderIndex].status != OFFLINE)
      {
        allCurrentStatus = traderCollection.traderStatusInfo[traderIndex].status;
      }
    }
    
    // TraceLog(DEBUG_LEVEL, "--->login accepted: user= %s' connection->currentState=%d\n", connection->userName, connection->currentState);

    /*
    Checking: If no other clients connected, the trader switch default to active trader
    else the trader switch to monitoring
    */
    if (allCurrentStatus == OFFLINE)
    {
      //Update TT event log
      sprintf(activityDetail, "%d,%d", connection->currentState, ACTIVE);
      ProcessUpdateTTEventLogToDatabase(connection->userName, ACTIVITY_TYPE_SWITCH_ROLE, activityDetail);
      
      ProcessUpdateTraderStatusToDatabase(connection->userName, ACTIVE);
      connection->currentState = ACTIVE;
    }
    else if (connection->currentState == MONITORING)
    {
      //Update TT event log
      sprintf(activityDetail, "%d,%d", connection->currentState, MONITORING);
      ProcessUpdateTTEventLogToDatabase(connection->userName, ACTIVITY_TYPE_SWITCH_ROLE, activityDetail);
    
      ProcessUpdateTraderStatusToDatabase(connection->userName, MONITORING);
      connection->currentState = MONITORING;
    }
    else if (connection->currentState == ACTIVE_ASSISTANT)
    {
      //Update TT event log
      sprintf(activityDetail, "%d,%d", connection->currentState, ACTIVE_ASSISTANT);
      ProcessUpdateTTEventLogToDatabase(connection->userName, ACTIVITY_TYPE_SWITCH_ROLE, activityDetail);
    
      ProcessUpdateTraderStatusToDatabase(connection->userName, ACTIVE_ASSISTANT);
      connection->currentState = ACTIVE_ASSISTANT;
    }
    else if (connection->currentState == ACTIVE)
    {
      //Update TT event log
      sprintf(activityDetail, "%d,%d", connection->currentState, ACTIVE);
      ProcessUpdateTTEventLogToDatabase(connection->userName, ACTIVITY_TYPE_SWITCH_ROLE, activityDetail);
      
      ProcessUpdateTraderStatusToDatabase(connection->userName, ACTIVE);
      connection->currentState = ACTIVE;
    }
  }

  // Send Alert configuration to Daedalus
  if (SendAlertConfToDaedalus(connection) == ERROR)
  {
    if(connection->statusUpdate == SUCCESS)
    {
      //Update trader status to offline
      ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);
    }
    
    CloseConnection(connection);

    return NULL;
  }

  // Send Aggregation Server global configuration to Daedalus
  if (SendASGlobalConfToDaedalus(connection) == ERROR)
  {
    if(connection->statusUpdate == SUCCESS)
    {
      //Update trader status to offline
      ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);
    }

    CloseConnection(connection);

    return NULL;
  }
  
  // Send ignore stock list to Daedalus
  if (SendTSIgnoreSymbolListToDaedalus(connection) == ERROR)
  {
    if(connection->statusUpdate == SUCCESS)
    {
      //Update trader status to offline
      ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);
    }

    CloseConnection(connection);

    return NULL;
  }
  
  //Send ignore stock ranges list to Daedalus
  if (SendIgnoreSymbolRangesListToDaedalus(connection) == ERROR)
  {
    if(connection->statusUpdate == SUCCESS)
    {
      //Update trader status to offline
      ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);
    }

    CloseConnection(connection);

    return NULL;
  }

  // Send Book/Order/Global/... configuration if AS connected to AS
  if (SendAllConfOfPMToDaedalus(connection) == ERROR)
  {
    if(connection->statusUpdate == SUCCESS)
    {
      //Update trader status to offline
      ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);
    }

    CloseConnection(connection);

    return NULL;
  }
  
  // Send  exchanges conf Of all TS to TT
  if (SendExchangesConfOfAllTSToDaedalus(connection) == ERROR)
  {
    if(connection->statusUpdate == SUCCESS)
    {
      //Update trader status to offline
      ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);
    }

    CloseConnection(connection);

    return NULL;
  }
  
  if (SendVolatileSectionsToDaedalus(connection) == ERROR)
  {
    if(connection->statusUpdate == SUCCESS)
    {
      //Update trader status to offline
      ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);
    }

    CloseConnection(connection);

    return NULL;
  }
  
  if (SendVolatileSymbolListToDaedalus(connection) == ERROR)
  {
    if(connection->statusUpdate == SUCCESS)
    {
      //Update trader status to offline
      ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);
    }

    CloseConnection(connection);

    return NULL;
  }

  if (SendAllVolatileToggleScheduleToDaedalus(connection) == ERROR)
  {
    if(connection->statusUpdate == SUCCESS)
    {
      //Update trader status to offline
      ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);
    }

    CloseConnection(connection);

    return NULL;
  }
  
  // Toggle Current Volatile Session must be sent after Volatile Session List
  if (SendCurrentVolatileSessionToDaedalus(connection) == ERROR)
  {
    if(connection->statusUpdate == SUCCESS)
    {
      //Update trader status to offline
      ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);
    }

    CloseConnection(connection);

    return NULL;
  }
  
  // Toggle Volatile symbol by schedule  must be sent after Volatile Schedule List
  if (SendVolatileToggleToDaedalus(connection) == ERROR)
  {
    if(connection->statusUpdate == SUCCESS)
    {
      //Update trader status to offline
      ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);
    }

    CloseConnection(connection);

    return NULL;
  }

  if (SendHaltStockListToDaedalus(connection) == ERROR)
  {
    if(connection->statusUpdate == SUCCESS)
    {
      //Update trader status to offline
      ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);
    }

    CloseConnection(connection);

    return NULL;
  }
  
  t_MonitorStatusChange monitor;
  InitMonitorServerStatusChange(&monitor);
  if(SendServersCurrentStatusToDaedalus(&monitor, connection) != SUCCESS)
  {
    if(connection->statusUpdate == SUCCESS)
    {
      //Update trader status to offline
      ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);
    }

    CloseConnection(connection);

    return NULL;
  }
  
  if (SendFullOpenPositionListToDaedalus(connection) == ERROR)
  {
    if(connection->statusUpdate == SUCCESS)
    {
      //Update trader status to offline
      ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);
    }

    CloseConnection(connection);

    return NULL;
  }
  
  while (1)
  {
    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1))
    {
      if(connection->statusUpdate == SUCCESS)
      {
        //Update trader status to offline
        ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);
      }

      return NULL;
    }

    // Check to update if there is a switch between FAST vs NORMAL Market Mode
    UpdateMarketMode();
    
    /* if (SendHeartbeatToDaedalus(connection) == ERROR)
    {
      if(connection->statusUpdate == SUCCESS)
      {
        //Update trader status to offline
        ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);
      }

      CloseConnection(connection);

      return NULL;
    } */
    
    // Send open order to TT
    // if (SendASOpenOrderToTT(connection) == ERROR)
    if (BuildAndSendASOpenOrderToDaedalus(connection) == ERROR)
    {
      if(connection->statusUpdate == SUCCESS)
      {
        //Update trader status to offline
        ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);
      }

      CloseConnection(connection);

      return NULL;
    }
    
    // Send open position to TT
    if (CheckAndSendOpenPositionsToDaedalus(connection) == ERROR)
    {
      if(connection->statusUpdate == SUCCESS)
      {
        //Update trader status to offline
        ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);
      }

      CloseConnection(connection);

      return NULL;
    }

    // TT send delay in in microseconds
    usleep(TT_SEND_DELAY);
  }

  return NULL;
}

/****************************************************************************
- Function name:  CheckNSendInCompleteTradeAlertToAllTT
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
void CheckNSendInCompleteTradeAlertToAllTT()
{
  int i;
  int listOfHungOrder[1000];
  int countHungOrder = 0, isAlert = 0;

  for (i = 0; i < asOpenOrder.countOpenOrder; i++)
  { 
    if ((asOpenOrder.orderCollection[i].orderSummary.status == orderStatusIndex.Sent) ||
       (asOpenOrder.orderCollection[i].orderSummary.status == orderStatusIndex.CXLSent))
    {
      asOpenOrder.orderCollection[i].lifeTimeInSecond++;

      if (asOpenOrder.orderCollection[i].lifeTimeInSecond >= AlertConf.incompleteTradeThreshhold)
      {
        listOfHungOrder[countHungOrder] = i;
        countHungOrder++;
        
        // New incomplete trade
        if (asOpenOrder.orderCollection[i].lifeTimeInSecond == AlertConf.incompleteTradeThreshhold)
        {
          isAlert = 1;
        }
      }
      
      // Repetition time
      if ((AlertConf.repetitionTimeThreshhold > 0) &&
        (asOpenOrder.orderCollection[i].lifeTimeInSecond - AlertConf.incompleteTradeThreshhold >= AlertConf.repetitionTimeThreshhold))
      {
        isAlert = 1;
      }
    }
    else
    {
      asOpenOrder.orderCollection[i].lifeTimeInSecond = -1;
    }
  }
  
  if (isAlert == 1)
  {
    char orderIdList[10000] = "\0";
    char ordIdStr[12];
    
    for (i = 0; i < countHungOrder; i++)
    {     
      asOpenOrder.orderCollection[listOfHungOrder[i]].lifeTimeInSecond = AlertConf.incompleteTradeThreshhold;
      
      if (i > 0)
      {
        sprintf(ordIdStr, " %d", asOpenOrder.orderCollection[listOfHungOrder[i]].orderSummary.clOrdId);
      }
      else
      {
        sprintf(ordIdStr, "%d", asOpenOrder.orderCollection[listOfHungOrder[i]].orderSummary.clOrdId);
      }
      
      strcat(orderIdList, ordIdStr);
    }
    
    BuildNSendInCompleteTradeAlertToAllDaedalus(orderIdList);
    BuildNSendInCompleteTradeAlertToAllTT(orderIdList);
  }
}

/****************************************************************************
- Function name:  CheckNSendBuyingPowerAlertToAllTT
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
void CheckNSendBuyingPowerAlertToAllTT()
{
  int tsIndex;

  for (tsIndex = 0; tsIndex < MAX_TRADE_SERVER_CONNECTIONS; tsIndex++)
  {
    if ((tradeServersInfo.connection[tsIndex].status == CONNECT) &&
      (tradeServersInfo.connection[tsIndex].socket != -1))
    {
      buyingPowerSecondsThreshhold[tsIndex]++;
      if (buyingPowerSecondsThreshhold[tsIndex] == AlertConf.buyingPowerSecondsThreshhold)
      {
        buyingPowerSecondsThreshhold[tsIndex] = 0;

        if (flt(tradeServersInfo.currentStatus[tsIndex].buyingPower, AlertConf.buyingPowerAmountThreshhold))
        {
          BuildNSendBuyingPowerDropsBelowThresholdAlertToAllDaedalus(tradeServersInfo.config[tsIndex].description, tradeServersInfo.currentStatus[tsIndex].buyingPower);
          BuildNSendBuyingPowerDropsBelowThresholdAlertToAllTT(tradeServersInfo.config[tsIndex].description, tradeServersInfo.currentStatus[tsIndex].buyingPower);
        }
      }
    }
    else
    {
      buyingPowerSecondsThreshhold[tsIndex] = 0;
    }
  }
}

/****************************************************************************
- Function name:  CheckNSendNoTradeDuringConfiguredPeriodAlertToAllTT
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
void CheckNSendNoTradeDuringConfiguredPeriodAlertToAllTT()
{
  // Handle no trade during time configured alert

  time_t t = (time_t)hbitime_seconds();
  struct tm tm = *localtime(&t);

  int i, tsIndex;
  for (tsIndex = 0; tsIndex < MAX_TRADE_SERVER_CONNECTIONS; tsIndex++)
  {
    int enableTradingCount = 0;
    for(i = 0; i < TS_MAX_ORDER_CONNECTIONS; i++)
    {
      if (tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[i] == ENABLE)
      {
        enableTradingCount++;
      }
    }

    if (enableTradingCount >= 2)
    {
      if (NoTradesDurationMgt[tsIndex].alertThresholdInMinute == -1)
      {
        NoTradesDurationMgt[tsIndex].currentNoTradesDurationInSecond = 0;
        NoTradesDurationMgt[tsIndex].alertThresholdInMinute = TradeTimeAlertConfig[3600 * tm.tm_hour + 60 * tm.tm_min + tm.tm_sec];
      }

      NoTradesDurationMgt[tsIndex].currentNoTradesDurationInSecond++;

      if (NoTradesDurationMgt[tsIndex].currentNoTradesDurationInSecond >= NoTradesDurationMgt[tsIndex].alertThresholdInMinute*60)
      {
        sprintf(NoTradesDurationMgt[tsIndex].endTimeStamp, "%d:%d", tm.tm_hour, tm.tm_min);

        BuildNSendNoTradesDuringConfiguredTimePeriodAlertToAllDaedalus(tsIndex, NoTradesDurationMgt[tsIndex].alertThresholdInMinute,
            NoTradesDurationMgt[tsIndex].startTimeStamp, NoTradesDurationMgt[tsIndex].endTimeStamp);
        BuildNSendNoTradesDuringConfiguredTimePeriodAlertToAllTT(tsIndex, NoTradesDurationMgt[tsIndex].alertThresholdInMinute,
            NoTradesDurationMgt[tsIndex].startTimeStamp, NoTradesDurationMgt[tsIndex].endTimeStamp);

        NoTradesDurationMgt[tsIndex].currentNoTradesDurationInSecond = 0;
        NoTradesDurationMgt[tsIndex].alertThresholdInMinute = TradeTimeAlertConfig[3600 * tm.tm_hour + 60 * tm.tm_min + tm.tm_sec];

        strcpy(NoTradesDurationMgt[tsIndex].startTimeStamp, NoTradesDurationMgt[tsIndex].endTimeStamp);
      }

    }
  }
}

/****************************************************************************
- Function name:  CheckNSendPMOrderRateAlertToAllTT
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
void CheckNSendPMOrderRateAlertToAllTT()
{
  if ((AlertConf.pmOrderRateThreshhold > 0) &&
    (PMOrderPerSecond >= AlertConf.pmOrderRateThreshhold))
  {
    BuildNSendPMOrderRateAlertToAllDaedalus(PMOrderPerSecond);
    BuildNSendPMOrderRateAlertToAllTT(PMOrderPerSecond);
  }
  PMOrderPerSecond = 0;
}

/****************************************************************************
- Function name:  IsSymbolHaltedOnPMBooks
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int IsSymbolHaltedOnPMBooks(int symbolIndex)
{
  // empty means not a halted one
  if(HaltStatusMgmt.symbol[symbolIndex][0] == 0)
  {
    return NO;
  }

  if ((HaltStatusMgmt.haltStatus[symbolIndex][TS_ARCA_BOOK_INDEX] + 
     HaltStatusMgmt.haltStatus[symbolIndex][TS_NASDAQ_BOOK_INDEX]) == TRADING_NORMAL)
  {
    return NO;
  }
  
  return YES;
}

/****************************************************************************
- Function name:  CheckNSendUnhandledOpenPositionWithoutAnOrderAlertToTT
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
void CheckNSendUnhandledOpenPositionWithoutAnOrderAlertToTT()
{
  int symbolIndex, account;

  int numSecond = GetNumberOfSecondsOfLocalTime();

  int lastTradingInSeconds = 0, isTrading = 0;
  for (symbolIndex = 0; symbolIndex < RMList.countSymbol; symbolIndex++)
  {
    lastTradingInSeconds = 0;
    
    if (IsSymbolHaltedOnPMBooks(symbolIndex) == YES) // symbol halted, ignore
    {
      for (account = 0; account < MAX_ACCOUNT; account++)
      {
        verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = numSecond;
      }
      continue;
    }

    for (account = 0; account < MAX_ACCOUNT; account++)
    {
      if (verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds > lastTradingInSeconds)
      {
        lastTradingInSeconds = verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds;
      }
        
      if (verifyPositionInfo[account][symbolIndex].isTrading != TRADING_NONE)
      {
        isTrading = 1;
      }
    }
    
    if ((isTrading == 0) && (AlertConf.positionWithoutOrderThreshhold > 0) &&
      (numSecond - lastTradingInSeconds > AlertConf.positionWithoutOrderThreshhold))
    {
      int sharesList[MAX_ACCOUNT];
      char sideList[MAX_ACCOUNT];
      int accList[MAX_ACCOUNT];
      int count = 0;
      
      for (account = 0; account < MAX_ACCOUNT; account++)
      {
        if ((verifyPositionInfo[account][symbolIndex].side != -1) &&
          (verifyPositionInfo[account][symbolIndex].shares > 0))
        {
          sharesList[count] = verifyPositionInfo[account][symbolIndex].shares;
          sideList[count] = verifyPositionInfo[account][symbolIndex].side;
          accList[count] = account;
          
          count++;
          verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = numSecond;
        }
      }
      
      if (count > 0)
      {
        BuildNSendUnhandledOpenPositionWithoutAnOrderAlertToDaedalus(RMList.symbolList[symbolIndex].symbol, sharesList, sideList, accList, count);
        BuildNSendUnhandledOpenPositionWithoutAnOrderAlertToTT(RMList.symbolList[symbolIndex].symbol, sharesList, sideList, accList, count);
      }
    }
  }
}

/****************************************************************************
- Function name:  CheckNSendPMHasntExitPositionAlertToAllTT
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
void CheckNSendPMHasntExitPositionAlertToAllTT()
{
  int symbolIndex, account;

  int numSecond = GetNumberOfSecondsOfLocalTime();

  for (symbolIndex = 0; symbolIndex < MAX_STOCK_SYMBOL; symbolIndex++)
  {
    for (account = 0; account < MAX_ACCOUNT; account++)
    {
      if (verifyPositionInfo[account][symbolIndex].isPMOwner == YES)
      {
        if (verifyPositionInfo[account][symbolIndex].alreadyAlerted == NO)
        {
          if ((AlertConf.pmExitedPositionThreshhold > 0) &&
            (numSecond - verifyPositionInfo[account][symbolIndex].pmHandleInSeconds >= AlertConf.pmExitedPositionThreshhold))
          {
            BuildNSendPMHasntExitPositionAlertToAllDaedalus(RMList.symbolList[symbolIndex].symbol, 
                              RMList.collection[account][symbolIndex].avgPrice, 
                              verifyPositionInfo[account][symbolIndex].shares, 
                              verifyPositionInfo[account][symbolIndex].side,
                              account);
            BuildNSendPMHasntExitPositionAlertToAllTT(RMList.symbolList[symbolIndex].symbol, 
                              RMList.collection[account][symbolIndex].avgPrice, 
                              verifyPositionInfo[account][symbolIndex].shares, 
                              verifyPositionInfo[account][symbolIndex].side,
                              account);
            
            verifyPositionInfo[account][symbolIndex].pmHandleInSeconds = GetNumberOfSecondsOfLocalTime();
            verifyPositionInfo[account][symbolIndex].alreadyAlerted = YES;
          }
        }
      }
    }
  }
}

/****************************************************************************
- Function name:  CheckNSendGTRAlertToAllTT
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
void CheckNSendGTRAlertToAllTT()
{
  double totalMatchPL = 0.0;

  int rmInfoIndex, account;
  int countSymbol = RMList.countSymbol;
  for (account = 0; account < MAX_ACCOUNT; account++)
  {
    for (rmInfoIndex = 0; rmInfoIndex < countSymbol; rmInfoIndex++)
    {
      // Total matched PL
      totalMatchPL += RMList.collection[account][rmInfoIndex].matchedPL;
    }
  }

  GTRMgt.gross[GTRMgt.currentSecond] = totalMatchPL;

  int nextIndex = (GTRMgt.currentSecond + 1) % AlertConf.gtrSecondThreshhold;

  double gtrChanged = GTRMgt.gross[GTRMgt.currentSecond] - GTRMgt.gross[nextIndex];

  if ((AlertConf.gtrAmountThreshhold > 0) &&
    (fgt(fabs(gtrChanged), AlertConf.gtrAmountThreshhold)))
  {
    BuildNSendGTRAlertToAllDaedalus(gtrChanged);
    BuildNSendGTRAlertToAllTT(gtrChanged);

    int i;
    for ( i = 0; i < AlertConf.gtrSecondThreshhold; i++)
    {
      GTRMgt.gross[i] = totalMatchPL;
    }
  }

  GTRMgt.currentSecond = nextIndex;
}

/****************************************************************************
- Function name:  SendAlertConfToDaedalus
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int SendAlertConfToDaedalus(void *pConnection)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Send Alert conf to TT ...\n");

  t_TTMessage ttMessage;

  // Build Daedalus configuration message to send Daedalus
  BuildAlertConfigToDaedalus(&ttMessage);

  // Send message to Daedalus
  return SendDaedalusMessage(connection, &ttMessage);
}

/****************************************************************************
- Function name:  SendASGlobalConfToDaedalus
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendASGlobalConfToDaedalus(void *pConnection)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Send AS global conf to TT ...\n");

  t_TTMessage ttMessage;

  // Build Aggregation Sever global configuration message to send Daedalus
  // BuildTTMessageForASGlobalConf(&ttMessage);
  BuildASGlobalConfigToDaedalus(&ttMessage);

  // Send message to Daedalus
  return SendDaedalusMessage(connection, &ttMessage);
}

/****************************************************************************
- Function name:  SendASGlobalConfToAllDaedalus
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendASGlobalConfToAllDaedalus(void)
{
  int ttIndex;
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;

  // Build Aggregation Sever global configuration message to send Daedalus
  // BuildTTMessageForASGlobalConf(&ttMessage);
  BuildASGlobalConfigToDaedalus(&ttMessage);

  // Send message to all Daedalus connected to AS
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];
    
    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_DAEDALUS)
    {
      continue;
    }

    // Send message to Daedalus
    SendDaedalusMessage(connection, &ttMessage);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  CheckAndSendVolatileSectionsToAllTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int CheckAndSendVolatileSectionsToAllTT()
{
  char fullPath[MAX_PATH_LEN];
  int ret;
  
  if (GetFullRawDataPath(VOLATILE_SECTION_FILE_NAME, fullPath) == NULL_POINTER)
  {
    return 0;
  }
  
  ret = IsFileTimestampToday(fullPath);
  if ((ret == -1) || (ret == 2))
  {
    /*  - File does not exist (might be deleted), or
      - There is no permission to access the file */
    
    if (volatileMgmt.sectionFileTimeStamp != 0)
    {
      /*  If file existed before (but currently deleted/not accessible), 
        we should remove all volatile symbols from memory */
        
      volatileMgmt.sectionFileTimeStamp = 0;
    }
    else
    {
      return 0;
    }
  }
  else  // File existing, so check if file contents have been changed by checking the file timestamp
  {
    struct stat fStat;
    if (stat(fullPath, &fStat) == 0)
    {
      if (fStat.st_ctime != volatileMgmt.sectionFileTimeStamp)    //File has changed, we will reload
      {
        volatileMgmt.sectionFileTimeStamp = fStat.st_ctime;
      }
      else
      {
        return 0;
      }
    }
    else
    {
      //There is some problem calling stat
      TraceLog(DEBUG_LEVEL, "problem in opening volatile sections file\n");
      //PrintErrStr(ERROR_LEVEL, "Calling fopen(): ", errno);
      return 0;
    }
  }

  TraceLog(DEBUG_LEVEL, "Volatile sections file has changed, reloading...\n");
  SendVolatileSectionsToAllDaedalus();
  SendVolatileSectionsToAllTT();
  return 1;
}

/****************************************************************************
- Function name:  CheckAndSendVolatileSymbolsToAllTT
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int CheckAndSendVolatileSymbolsToAllTT()
{
  char fullPath[MAX_PATH_LEN];
  int ret;
  
  if (GetFullConfigPath(VOLATILE_SYMBOL_FILE_NAME, CONF_SYMBOL, fullPath) == NULL_POINTER)
  {
    return 0;
  }
  
  ret = IsFileTimestampToday(fullPath);
  if (ret == 0)
  {
    remove(fullPath); // File is old, remove it
    return 0;
  }
  else if (ret != 1)
  {
    /*  - File does not exist (might be deleted), or
      - There is no permission to access the file */
    
    if (volatileMgmt.listFileTimeStamp != 0)
    {
      /*  If file existed before (but currently deleted/not accessible), 
        we should remove all volatile symbols from memory */
        
      volatileMgmt.listFileTimeStamp = 0;
    }
    else
    {
      return 0;
    }
  }
  else  // File existing, so check if file contents have been changed by checking the file timestamp
  {
    struct stat fStat;
    if (stat(fullPath, &fStat) == 0)
    {
      if (fStat.st_ctime != volatileMgmt.listFileTimeStamp)   //File has changed, we will reload
      {
        volatileMgmt.listFileTimeStamp = fStat.st_ctime;
      }
      else
      {
        return 0;
      }
    }
    else
    {
      //There is some problem calling stat
      TraceLog(DEBUG_LEVEL, "problem in opening volatile list file\n");
      //PrintErrStr(ERROR_LEVEL, "Calling fopen(): ", errno);
      return 0;
    }
  }

  TraceLog(DEBUG_LEVEL, "Volatile symbol list file has changed, reloading...\n");
  SendVolatileSymbolListToAllDaedalus();
  SendVolatileSymbolListToAllTT();
  return 1;
}

/****************************************************************************
- Function name:  SendVolatileSectionsToAllDaedalus
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendVolatileSectionsToAllDaedalus()
{
  int ttIndex;
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;

  // Build ignore stock list message to send Daedalus
  BuildVolatileSymbolListBySessionToDaedalus(&ttMessage);

  // Send message to all Daedalus connected to AS
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];
    
    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_DAEDALUS)
    {
      continue;
    }

    // Send message to Daedalus
    SendDaedalusMessage(connection, &ttMessage);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendAllVolatileToggleScheduleToAllDaedalus
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendAllVolatileToggleScheduleToAllDaedalus()
{
  int ttIndex;
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;

  // Build ignore stock list message to send Daedalus
  BuildVolatileToggleListToDaedalus(&ttMessage);

  // Send message to all Daedalus connected to AS
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];
    
    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_DAEDALUS)
    {
      continue;
    }

    // Send message to Daedalus
    SendDaedalusMessage(connection, &ttMessage);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendVolatileSectionsToDaedalus
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendVolatileSectionsToDaedalus(t_ConnectionStatus *connection)
{
  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  t_TTMessage ttMessage;

  BuildVolatileSymbolListBySessionToDaedalus(&ttMessage);
  
  // Send message to Daedalus
  return SendDaedalusMessage(connection, &ttMessage);
}

/****************************************************************************
- Function name:  SendAllVolatileToggleScheduleToDaedalus
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendAllVolatileToggleScheduleToDaedalus(t_ConnectionStatus *connection)
{
  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  t_TTMessage ttMessage;

  BuildVolatileToggleListToDaedalus(&ttMessage);
  
  // Send message to Daedalus
  return SendDaedalusMessage(connection, &ttMessage);
}

/****************************************************************************
- Function name:  SendVolatileSymbolListToAllDaedalus
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendVolatileSymbolListToAllDaedalus()
{
  int ttIndex;
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;

  // Build ignore stock list message to send Daedalus
  BuildVolatileSymbolListToDaedalus(&ttMessage);

  // Send message to all Daedalus connected to AS
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];
    
    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_DAEDALUS)
    {
      continue;
    }

    // Send message to Daedalus
    SendDaedalusMessage(connection, &ttMessage);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendVolatileSymbolListToDaedalus
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendVolatileSymbolListToDaedalus(void *pConnection)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  t_TTMessage ttMessage;

  BuildVolatileSymbolListToDaedalus(&ttMessage);
  
  // Send message to Daedalus
  return SendDaedalusMessage(connection, &ttMessage);
}

int SendTSServerCurrentStatusToDaedalus(t_MonitorStatusChange *monitor, void *pConnection, int tsIndex)
{
  int ret = SUCCESS;
  
  // Send TS Server Connections Status
  if(monitor->tsServerStatus[tsIndex] == 1)
  {
    if(monitor->updateServerStatus == 1)
    {
      ret += BuildAndSendServerStatusToDaedalus(pConnection, tsIndex,
            tradeServersInfo.config[tsIndex].description);
    }
    
    if(monitor->updateServerInfo == 1)
    {
      ret += BuildAndSendServerInfoToDaedalus(pConnection, tsIndex,
            tradeServersInfo.config[tsIndex].description,
            (int)TradingAccount.accountForTS[tsIndex]);
    }
  }
  
  // Send TS MDCs status
  int i;
  for(i = 0; i < TS_MAX_BOOK_CONNECTIONS; i++)
  {
    if(monitor->tsMdcStatus[tsIndex][i] == 1)
    {
      ret += BuildAndSendMdcStatusToDaedalus(pConnection,
              tradeServersInfo.config[tsIndex].description,
              tradeServersInfo.connection[tsIndex].status,
              TS_BOOK_INDEX_TO_DAEDALUS_MDC_MAPPING[i],
              tradeServersInfo.currentStatus[tsIndex].tsMdcStatus[i],
              1);
    }
  }
  
  // Away market
  if(monitor->tsAwayMarketMdcStatus[tsIndex][CQS_BOOK] == 1)    
  {
    ret += BuildAndSendMdcStatusToDaedalus(pConnection,
            tradeServersInfo.config[tsIndex].description,
            tradeServersInfo.connection[tsIndex].status,
            COM__RGM__DAEDALUS__PROTO__MDC__CQS_MDC,
            tradeServersInfo.currentStatus[tsIndex].statusCQS,
            0);
  }
        
  if(monitor->tsAwayMarketMdcStatus[tsIndex][UQDF_BOOK] == 1)
  {
    ret += BuildAndSendMdcStatusToDaedalus(pConnection,
            tradeServersInfo.config[tsIndex].description,
            tradeServersInfo.connection[tsIndex].status,
            COM__RGM__DAEDALUS__PROTO__MDC__UQDF_MDC,
            tradeServersInfo.currentStatus[tsIndex].statusUQDF,
            0);
  }

  // Send TS OECs Status
  for(i = 0; i < TS_MAX_ORDER_CONNECTIONS; i++)
  {
    if(monitor->tsOecStatus[tsIndex][i] == 1)
    {
      ret += BuildAndSendOecStatusToDaedalus(pConnection,
              tradeServersInfo.config[tsIndex].description,
              tradeServersInfo.connection[tsIndex].status,
              AS_CENTRALIZED_ORDER_TYPE_TO_OEC_MAPPING[i],
              tradeServersInfo.currentStatus[tsIndex].tsOecStatus[i],
              tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[i],
              tradeServersInfo.ECNOrdersConf[tsIndex][i].ip,
              tradeServersInfo.ECNOrdersConf[tsIndex][i].port,
              SessionIdMgmt.tsSessionId[tsIndex][i]);
    }
  }
  
  return ret;
}

int SendTSServersCurrentStatusToDaedalus(t_MonitorStatusChange *monitor, void *pConnection)
{
  int tsIndex, ret = SUCCESS;
  for (tsIndex = 0; tsIndex < tradeServersInfo.maxConnections; tsIndex++)
  {
    ret += SendTSServerCurrentStatusToDaedalus(monitor, pConnection, tsIndex);
  }
  
  return ret;
}

int SendPMServerCurrentStatusToDaedalus(t_MonitorStatusChange *monitor, void *pConnection)
{
  int ret = SUCCESS;
  
  //Send PM Server Status
  if(monitor->pmServerStatus == 1)
  {
    ret += BuildAndSendServerStatusToDaedalus(pConnection, -1, PM_SERVER_NAME);
    ret += BuildAndSendServerInfoToDaedalus(pConnection, -1, PM_SERVER_NAME, -1);
  }
  
  // Send PM MDCs Status
  int i;
  for(i = 0; i < PM_MAX_BOOK_CONNECTIONS; i++)
  {
    if(monitor->pmMdcStatus[i] == 1)
    {
     ret += BuildAndSendMdcStatusToDaedalus(pConnection,
            PM_SERVER_NAME,
            pmInfo.connection.status,
            PM_BOOK_INDEX_TO_DAEDALUS_MDC_MAPPING[i],
            pmInfo.currentStatus.pmMdcStatus[i],
            1);
    }
  }
  
  // Away market
  if(monitor->pmAwayMarketMdcStatus[CQS_BOOK] == 1)
  {
    ret += BuildAndSendMdcStatusToDaedalus(pConnection,
          PM_SERVER_NAME,
          pmInfo.connection.status,
          COM__RGM__DAEDALUS__PROTO__MDC__CQS_MDC,
          pmInfo.currentStatus.statusCQS_Feed,
          0);
  }
  
  if(monitor->pmAwayMarketMdcStatus[UQDF_BOOK] == 1)
  {
    ret += BuildAndSendMdcStatusToDaedalus(pConnection,
          PM_SERVER_NAME,
          pmInfo.connection.status,
          COM__RGM__DAEDALUS__PROTO__MDC__UQDF_MDC,
          pmInfo.currentStatus.statusUQDF_Feed,
          0);
  }
  
  if(monitor->pmAwayMarketMdcStatus[CTS_BOOK] == 1)
  {
    ret += BuildAndSendMdcStatusToDaedalus(pConnection,
          PM_SERVER_NAME,
          pmInfo.connection.status,
          COM__RGM__DAEDALUS__PROTO__MDC__CTS_MDC,
          pmInfo.currentStatus.statusCTS_Feed,
          0);
  }
  
  if(monitor->pmAwayMarketMdcStatus[UTDF_BOOK] == 1)
  {
    ret += BuildAndSendMdcStatusToDaedalus(pConnection,
          PM_SERVER_NAME,
          pmInfo.connection.status,
          COM__RGM__DAEDALUS__PROTO__MDC__UTDF_MDC,
          pmInfo.currentStatus.statusUTDF,
          0);
  }
  
  // Send PM OECs Status
  for(i = 0; i < PM_MAX_ORDER_CONNECTIONS; i++)
  {
    if(monitor->pmOecStatus[i] == 1)
    {
      int oec  = (i == PM_ARCA_DIRECT_INDEX) ? COM__RGM__DAEDALUS__PROTO__OEC__ARCA_DIRECT_OEC : COM__RGM__DAEDALUS__PROTO__OEC__RASH_OEC;
      ret += BuildAndSendOecStatusToDaedalus(pConnection,
          PM_SERVER_NAME,
          pmInfo.connection.status,
          oec,
          pmInfo.currentStatus.pmOecStatus[i],
          pmInfo.currentStatus.tradingStatus, 
          pmInfo.ECNOrdersConf[i].ip,
          pmInfo.ECNOrdersConf[i].port,
          SessionIdMgmt.pmSessionId[i]);
    }
  }
  
  return ret;
}

int SendASServerCurrentStatusToDaedalus(t_MonitorStatusChange *monitor, void *pConnection)
{
  int ret = SUCCESS;
  
  //Send AS Server Status
  if(monitor->asServerStatus == 1 && monitor->updateServerStatus == 1)
  {
    ret += BuildAndSendServerStatusToDaedalus(pConnection, -1, AS_SERVER_NAME);
  }
  
  int accountIndex;
  for (accountIndex = 0; accountIndex < MAX_ACCOUNT; accountIndex++)
  {
    if(monitor->asServerStatus == 1 && monitor->updateServerInfo == 1)
    {
      ret += BuildAndSendServerInfoToDaedalus(pConnection, -1, AS_SERVER_NAME, accountIndex);
    }
  }
  
  // Send AS OEC Status
  if(monitor->asOecStatus[AS_ARCA_DIRECT_INDEX] == 1)
  {
    ret += BuildAndSendOecStatusToDaedalus(pConnection,
          AS_SERVER_NAME,
          CONNECT,
          COM__RGM__DAEDALUS__PROTO__OEC__ARCA_DIRECT_OEC,
          orderStatusMgmt[AS_ARCA_DIRECT_INDEX].isOrderConnected,
          orderStatusMgmt[AS_ARCA_DIRECT_INDEX].isOrderConnected == CONNECTED ? ENABLE:DISABLE,
          ARCA_DIRECT_Config.ipAddress,
          ARCA_DIRECT_Config.port,
          SessionIdMgmt.asSessionId[AS_ARCA_DIRECT_INDEX]);
  }  
  return ret;
}

int SendServersCurrentStatusToDaedalus(t_MonitorStatusChange *monitor, void *pConnection)
{
  int ret = SUCCESS;
  ret += SendTSServersCurrentStatusToDaedalus(monitor, pConnection);
  ret += SendPMServerCurrentStatusToDaedalus(monitor, pConnection);
  ret += SendASServerCurrentStatusToDaedalus(monitor, pConnection);
  
  return ret;
}

/****************************************************************************
- Function name:  SendDaedalusMessage
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendDaedalusMessage(void *pConnection, void *message)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection; 
  
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
    return ERROR;

  t_TTMessage *ttMessage = (t_TTMessage *)message;
  
  t_TTMessage ttMsgToSend;
  
  // take care of it when implementing websocket data framing
  
  int payloadIdx = 2;
  ttMsgToSend.msgContent[0] = 0x82;
  ttMsgToSend.msgContent[1] = 0x80;
     
  if (ttMessage->msgLen <= 125) 
  {
    ttMsgToSend.msgContent[1] |= ttMessage->msgLen;
  } 
  else if (ttMessage->msgLen < 0xffff)
  {
    ttMsgToSend.msgContent[1] |= 126;
    ttMsgToSend.msgContent[2] = (ttMessage->msgLen >> 8) & 0xff;
    ttMsgToSend.msgContent[3] = (ttMessage->msgLen) & 0xff;
    payloadIdx = 4;
  } 
  else 
  {
    ttMsgToSend.msgContent[1] |= 127;
    ttMsgToSend.msgContent[2] = 0;
    ttMsgToSend.msgContent[3] = 0;
    ttMsgToSend.msgContent[4] = 0;
    ttMsgToSend.msgContent[5] = 0;
    ttMsgToSend.msgContent[6] = (ttMessage->msgLen >> 24) & 0xff;
    ttMsgToSend.msgContent[7] = (ttMessage->msgLen >> 16) & 0xff;
    ttMsgToSend.msgContent[8] = (ttMessage->msgLen >> 8) & 0xff;
    ttMsgToSend.msgContent[9] = (ttMessage->msgLen) & 0xff;
    payloadIdx = 10;
  }
  
  memcpy(&ttMsgToSend.msgContent[payloadIdx], ttMessage->msgContent, ttMessage->msgLen);
  
  ttMsgToSend.msgLen = ttMessage->msgLen + payloadIdx;
  
  int result = SUCCESS;
  pthread_mutex_lock(&(connection->socketMutex));
  
    int bytesSent = send(connection->socket, ttMsgToSend.msgContent, ttMsgToSend.msgLen, 0);
    if (bytesSent != ttMsgToSend.msgLen)
    {
      TraceLog(ERROR_LEVEL, "send data failed: bytesSent(%d), msgLen(%d)\n", bytesSent, ttMsgToSend.msgLen);
      result = ERROR;
    }
  pthread_mutex_unlock(&(connection->socketMutex));
  
  if(result != SUCCESS)
  {
    TraceLog(ERROR_LEVEL, "Close '%s' connection\n", connection->userName);
    CloseConnection(connection);
  }
  
  return result;
}

/****************************************************************************
- Function name:  ProcessUpdatetASCurrentStatus
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessUpdatetASCurrentStatus(void)
{
  int tsIndex, account;
  int totalNumStuck = 0, totalNumLoser = 0;
  double totalBuyingPower[MAX_ACCOUNT];
  memset(totalBuyingPower, 0, sizeof(totalBuyingPower));
  
  for (tsIndex = 0; tsIndex < MAX_TRADE_SERVER_CONNECTIONS; tsIndex++)
  {
    if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
    {
      tradeServersInfo.currentStatus[tsIndex].buyingPower = 0.00;
      tradeServersInfo.currentStatus[tsIndex].numStuck = 0;
      tradeServersInfo.currentStatus[tsIndex].numLoser = 0;
    }

    totalBuyingPower[(int)TradingAccount.accountForTS[tsIndex]] += tradeServersInfo.currentStatus[tsIndex].buyingPower;   

    totalNumStuck += tradeServersInfo.currentStatus[tsIndex].numStuck;
    totalNumLoser += tradeServersInfo.currentStatus[tsIndex].numLoser;
  }

  for (account = 0; account < MAX_ACCOUNT; account++)
  {   
    traderToolsInfo.currentStatus.buyingPower[account] = totalBuyingPower[account];
  }

  traderToolsInfo.currentStatus.numStuck = totalNumStuck;
  traderToolsInfo.currentStatus.numLoser = totalNumLoser;

  // Check total num stuck and total num loser  
  if (fge(totalNumLoser, traderToolsInfo.globalConf.maxLoser))
  {
    if (LossLimitAlert == NO)
    {
      LossLimitAlert = YES;
      
      // Call function to send disable trading to all TS
      TraceLog(DEBUG_LEVEL, "totalNumLoser = %d\n", totalNumLoser);

      TraceLog(DEBUG_LEVEL, "Process send disable trading to all TS\n");

      // Call function to disable trading on all ECN order
      SendDisableTradingAllOrderToAllTS(TS_TRADING_DISABLED_BY_EXCEED_MAX_LOSS_LIMIT_BY_AS_REQUEST);
    }
  }
  else if (fge(totalNumLoser, traderToolsInfo.globalConf.maxLoser - AlertConf.consecutiveLossThreshhold))
  {
    if (NearningLossLimitCount != totalNumLoser)
    {
      NearningLossLimitCount = totalNumLoser;
      BuildNSendConsecutiveLossNearingLimitAlertToAllDaedalus();
      BuildNSendConsecutiveLossNearingLimitAlertToAllTT();
    }
  }
  else
  {
    LossLimitAlert = NO;
    NearningLossLimitCount = 0;
  }
  
  if (totalNumStuck >= traderToolsInfo.globalConf.maxStuck)
  {
    if (StuckLimitAlert == NO)
    {
      StuckLimitAlert = YES;
      
      // Call function to send disable trading to all TS
      TraceLog(DEBUG_LEVEL, "totalNumStuck = %d\n", totalNumStuck);

      TraceLog(DEBUG_LEVEL, "Process send disable trading to all TS\n");

      // Call function to disable trading on all ECN order
      SendDisableTradingAllOrderToAllTS(TS_TRADING_DISABLED_BY_EXCEED_MAX_STUCK_LIMIT_BY_AS_REQUEST);
    }
  }
  else if (totalNumStuck >= traderToolsInfo.globalConf.maxStuck - AlertConf.stucksThreshhold)
  {
    if (NearningStuckLimitCount != totalNumStuck)
    {
      NearningStuckLimitCount = totalNumStuck;
      BuildNSendStuckNearingLimitAlertToAllDaedalus();
      BuildNSendStuckNearingLimitAlertToAllTT();
    }
  }
  else
  {
    StuckLimitAlert = 0;
    NearningStuckLimitCount = 0;
  }
  
  return SUCCESS;
}

int ProcessResponseSendNewOrderFromClientTool(t_SendNewOrder *newOrder, int sendId, int reason, int ttIndex, char *reasonText)
{
  if (traderToolsInfo.connection[ttIndex].clientType == CLIENT_TOOL_DAEDALUS)
    return ProcessResponseSendNewOrderFromDaedalus(newOrder, sendId, reason, ttIndex, reasonText);
  else
    return ProcessResponseSendNewOrderFromTT(newOrder, sendId, reason, ttIndex);
}

/****************************************************************************
- Function name:  ProcessResponseSendNewOrderFromDaedalus
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessResponseSendNewOrderFromDaedalus(t_SendNewOrder *newOrder, int sendId, int reason, int ttIndex, char *reasonText)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];

  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  t_TTMessage ttMessage;

  // Build response send new order message to send Daedalus
  // BuildTTMessageForResponseSendNewOrder(&ttMessage, newOrder, sendId, reason);
  BuildResponseSendNewOrderToDaedalus(&ttMessage, newOrder, sendId, reason, reasonText);
  
  // Send message to Daedalus
  return SendDaedalusMessage(connection, &ttMessage);
}

/****************************************************************************
- Function name:  SendPMIgnoredSymbolToAllDaedalus
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendPMIgnoredSymbolToAllDaedalus(void)
{
  int ttIndex;
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;

  // Build PM ignored symbol message to send Daedalus
  // BuildTTMessageForPMIgnoredSymbol(&ttMessage);
  int msglen = BuildIgnoreSymbolListToDaedalus(&ttMessage, COM__RGM__DAEDALUS__PROTO__BLINK_BOX_SERVER_TYPE__PositionManager);
  if (msglen == 0)
  {
    return SUCCESS;
  }

  // Send message to all Daedalus connected to AS
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];

    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_DAEDALUS)
    {
      continue;
    }   

    // Send message to Daedalus
    SendDaedalusMessage(connection, &ttMessage);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendAllConfOfPMToDaedalus
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendAllConfOfPMToDaedalus(void *pConnection)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  // Check connection between AS and PM
  if ((pmInfo.connection.status == DISCONNECT) || (pmInfo.connection.socket == -1))
  {
    return SUCCESS;
  }
  
  // int ECNIndex;
  
  // Send PM Exchange configuration
  if (BuildAndSendPMExchangeConfigToDaedalus(connection) == ERROR)
  {
    return ERROR;
  }
  
  // Send PM ignored symbol
  if (SendPMIgnoredSymbolToToDaedalus(connection) == ERROR)
  {
    return ERROR;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendPMIgnoredSymbolToToDaedalus
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendPMIgnoredSymbolToToDaedalus(void *pConnection)
{
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Send PM ignored symbol to Daedalus...\n");

  // Build PM ignored symbol message to send Daedalus
  // BuildTTMessageForPMIgnoredSymbol(&ttMessage);
  int msglen = BuildIgnoreSymbolListToDaedalus(&ttMessage, COM__RGM__DAEDALUS__PROTO__BLINK_BOX_SERVER_TYPE__PositionManager);
  if (msglen == 0)
  {
    return SUCCESS;
  }

  // Send message to Daedalus
  return SendDaedalusMessage(connection, &ttMessage);
}

/****************************************************************************
- Function name:  CheckNRaiseServerRoleConfigurationMisConfiguredAlert
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int CheckNRaiseServerRoleConfigurationMisConfiguredAlertToAllTT()
{
  int tsIndex, roleIndex;
  
  int countRole[MAX_SERVER_ROLE];
  int countSymbolRange[MAX_SERVER_ROLE][256]; //A-Z
  
  memset(&countRole, 0, MAX_SERVER_ROLE * 4);
  memset(&countSymbolRange, 0, sizeof(countSymbolRange));

  char start, end;
  int index;
  // Count the number of TS have the same server role
  for (tsIndex = 0; tsIndex < MAX_TRADE_SERVER_CONNECTIONS; tsIndex++)
  {
    if (tradeServersInfo.connection[tsIndex].status == CONNECTED &&
      tradeServersInfo.globalConf[tsIndex].serverRole != -1)
    {
      // TraceLog(DEBUG_LEVEL, "tsIndex=%d serverRole=%d startOfRange='%c' endOfRange='%c'\n", tsIndex, tradeServersInfo.globalConf[tsIndex].serverRole,
        // tradeServersInfo.splitConf[tsIndex].startOfRange, tradeServersInfo.splitConf[tsIndex].endOfRange);
      
      if (tradeServersInfo.globalConf[tsIndex].serverRole == STANDALONE_SERVER_ROLE)
      {
        for (roleIndex = 0; roleIndex < MAX_SERVER_ROLE; roleIndex++)
        {
          countRole[roleIndex]++;

          start = tradeServersInfo.splitConf[tsIndex].startOfRange;
          end = tradeServersInfo.splitConf[tsIndex].endOfRange;
          for (index = start; index <= end; index++)
          {
            countSymbolRange[roleIndex][index]++;
          }
        }
      }
      else
      {
        countRole[tradeServersInfo.globalConf[tsIndex].serverRole]++;
        
        start = tradeServersInfo.splitConf[tsIndex].startOfRange;
        end = tradeServersInfo.splitConf[tsIndex].endOfRange;
        for (index = start; index <= end; index++)
        {
          countSymbolRange[tradeServersInfo.globalConf[tsIndex].serverRole][index]++;
        }
      }
    }
  }
  
  char serverList[256] = "\0";
  char alertStr[256] = "\0";
  int flag = 0;
  int alertType = 0; // 0: don't raise, 1: overlap symbol
  int retVal = SUCCESS;
  
  // Check to raise alert if many TS have same server role and same symbol range
  for (roleIndex = 0; roleIndex < MAX_SERVER_ROLE; roleIndex++)
  {
    if (countRole[roleIndex] > 1)
    {
      flag = 0;
      alertType = 0;
      memset(serverList, 0, 256);
      memset(alertStr, 0, 256);
      
      //Get server name
      for (tsIndex = 0; tsIndex < MAX_TRADE_SERVER_CONNECTIONS; tsIndex++)
      {
        if (tradeServersInfo.connection[tsIndex].status == CONNECTED &&
          (tradeServersInfo.globalConf[tsIndex].serverRole == STANDALONE_SERVER_ROLE ||
          tradeServersInfo.globalConf[tsIndex].serverRole == roleIndex))
        {
          if (flag == 0)
          {
            // the first TS
            strcat(serverList, tradeServersInfo.config[tsIndex].description);
            flag = 1;
          }
          else
          {
            strcat(serverList, " ");
            strcat(serverList, tradeServersInfo.config[tsIndex].description);
          }
          
          //Check Symbol Ranges are overlapped or not
          for (index = 'A'; index <= 'Z'; index++)
          {
            if (countSymbolRange[roleIndex][index] > 1)
            {
              alertType = 1;
              retVal = ERROR;
              break;
            }
          }
        }
      }
      
      if (alertType == 1)
      {
        sprintf(alertStr, "(Overlapped Split Symbol) %s", serverList);
        BuildNSendServerRoleConfigurationMisConfiguredAlertToAllDaedalus(alertStr);
        BuildNSendServerRoleConfigurationMisConfiguredAlertToAllTT(alertStr);
      }
    }
  }

  return retVal;
}

/****************************************************************************
- Function name:  SendAlertConfigToAllDaedalus
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int SendAlertConfigToAllDaedalus(void)
{
  t_TTMessage ttMessage;

  BuildAlertConfigToDaedalus(&ttMessage);

  return SendMessageToAllDaedalus(&ttMessage);
}

/****************************************************************************
- Function name:  SendMessageToAllDaedalus
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int SendMessageToAllDaedalus(t_TTMessage *ttMessage)
{
  int ttIndex;

  t_ConnectionStatus *connection;

  // Send message to all Daedalus connected to AS
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];

    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_DAEDALUS)
    {
      continue;
    }

    // Send message to Daedalus
    SendDaedalusMessage(connection, ttMessage);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  BuildNSendNewActiveTraderAlertToAllDaedalus
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildNSendNewActiveTraderAlertToAllDaedalus(char * trader)
{
  char alert[128] = "\0";
  sprintf(alert, "New Active Trader: %s", trader);
  
  // TraceLog(DEBUG_LEVEL, "--->alert='%s'\n", alert);

  return BuildAndSendAlertToAllDaedalus(alert, COM__RGM__DAEDALUS__PROTO__NOTIFICATION_LEVEL_TYPE__ShowLog);
}

/****************************************************************************
- Function name:  BuildNSendBuyingPowerDropsBelowThresholdAlertToAllDaedalus
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildNSendBuyingPowerDropsBelowThresholdAlertToAllDaedalus(char *server, double amount)
{
  char alert[128] = "\0";
  sprintf(alert, "Buying Power: %s $%lf", server, amount);
  
  // TraceLog(DEBUG_LEVEL, "--->alert='%s'\n", alert);

  return BuildAndSendAlertToAllDaedalus(alert, COM__RGM__DAEDALUS__PROTO__NOTIFICATION_LEVEL_TYPE__ShowLog);

}
/****************************************************************************
- Function name:  BuildNSendNoActiveTraderAlertToAllDaedalus
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildNSendNoActiveTraderAlertToAllDaedalus()
{
  char alert[128] = "No Active Trader";
  
  // TraceLog(DEBUG_LEVEL, "--->alert='%s'\n", alert);

  return BuildAndSendAlertToAllDaedalus(alert, COM__RGM__DAEDALUS__PROTO__NOTIFICATION_LEVEL_TYPE__ShowLog);
}

/****************************************************************************
- Function name:  UpdateRejectMgmt
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int UpdateRejectMgmt(int ECNType, const char *rejectCode, const char* rejectText)
{
  char formattedText[1024];
  int i = 0;
  
  //Convert ',' to '/'
  formattedText[0] = 0;
  while (rejectText[i] != 0)
  {
    if (rejectText[i] == ',')
      formattedText[i] = '/';
    else
      formattedText[i] = rejectText[i];
      
    i++;
  }
  formattedText[i] = 0;
  
  t_DisableRejectMgmt* reject = &DisableRejectMgmt[ECNType - TYPE_BASE];

  // ARCA DIRECT reject managed by reject text instead of reject code
  if(ECNType == TYPE_ARCA_DIRECT)
  {
    
    for(i = 0; i < reject->countReason; i++)
    {
      // already exist -> return
      if(strcmp(reject->reasonText[i], formattedText) == 0)
      {
        return i;
      }
    }
    
    // doesn't exist -> add
    int tmp = reject->countReason;
    strcpy(reject->reasonText[tmp], formattedText);

    // ARCA DIRECT reject code is re-created, rejectCode is unused
    sprintf(reject->reasonCode[tmp], "%d", tmp);

    reject->disableAlert[tmp] = NO;

    reject->countReason++;

    TraceLog(DEBUG_LEVEL, "(UpdateRejectMgmt) New reject added ECNType = %d, rejectCode = %d, rejectText = %s\n", ECNType, tmp, formattedText);

    SaveRejectCodeToFileOfAOrder(ECNType);

    BuildNSendRejectCodeToAllTT(ECNType);

    return tmp;
  }
  else
  {
    int i;
    for(i = 0; i < reject->countReason; i++)
    {
      if(strcmp(reject->reasonCode[i], rejectCode) == 0)
      {
        return i;
      }
    }

    // doesn't exist -> add
    int tmp = reject->countReason;
    strcpy(reject->reasonText[tmp], formattedText);

    strcpy(reject->reasonCode[tmp], rejectCode);

    reject->disableAlert[tmp] = NO;

    reject->countReason++;

    TraceLog(DEBUG_LEVEL, "(UpdateRejectMgmt) New reject added ECNType = %d, rejectCode = %s, rejectText = %s\n", ECNType, rejectCode, formattedText);

    SaveRejectCodeToFileOfAOrder(ECNType);

    BuildNSendRejectCodeToAllTT(ECNType);

    return tmp;
  }
}

/****************************************************************************
- Function name:  CheckNSendOrderRejectedAlertToAllTT
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int CheckNSendOrderRejectedAlertToAllTT(int ECNType, const char *rejectCode, const char* rejectText, int oid)
{
  t_DisableRejectMgmt* reject = &DisableRejectMgmt[ECNType - TYPE_BASE];

  int index = UpdateRejectMgmt(ECNType, rejectCode, rejectText);

  if(index == ERROR)
  {
    return ERROR;
  }

  if(reject->disableAlert[index] == NO)
  {
    BuildNSendOrderRejectedAlertToAllDaedalus(oid);
    BuildNSendOrderRejectedAlertToAllTT(oid);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  BuildNSendOrderRejectedAlertToAllDaedalus
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildNSendOrderRejectedAlertToAllDaedalus(int oid)
{
  char alert[128] = "\0";
  sprintf(alert, "Reject: OID (%d)", oid);
  
  // TraceLog(DEBUG_LEVEL, "--->alert='%s'\n", alert);

  return BuildAndSendAlertToAllDaedalus(alert, COM__RGM__DAEDALUS__PROTO__NOTIFICATION_LEVEL_TYPE__ShowLog);
}

/****************************************************************************
- Function name:  BuildNSendPMOrderRateAlertToAllDaedalus
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildNSendPMOrderRateAlertToAllDaedalus(int number)
{
  char alert[128] = "\0";
  sprintf(alert, "PM Order Rate higher than %d per second.", number);
  
  // TraceLog(DEBUG_LEVEL, "--->alert='%s'\n", alert);

  return BuildAndSendAlertToAllDaedalus(alert, COM__RGM__DAEDALUS__PROTO__NOTIFICATION_LEVEL_TYPE__ShowLog);
}

/****************************************************************************
- Function name:  BuildNSendInCompleteTradeAlertToAllDaedalus
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildNSendInCompleteTradeAlertToAllDaedalus(char *orderIdList)
{
  char alert[10240] = "\0";
  sprintf(alert, "Incomplete Trade: Order List: %s", orderIdList);
  
  // TraceLog(DEBUG_LEVEL, "--->alert='%s'\n", alert);

  return BuildAndSendAlertToAllDaedalus(alert, COM__RGM__DAEDALUS__PROTO__NOTIFICATION_LEVEL_TYPE__ShowLog);
}

/****************************************************************************
- Function name:  BuildNSendNoTradesDuringConfiguredTimePeriodAlertToAllDaedalus
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildNSendNoTradesDuringConfiguredTimePeriodAlertToAllDaedalus(int serverID, int minutes, char *startTimeStamp, char *endTimeStamp)
{
  char alert[128] = "\0";
  sprintf(alert, "No Trades: %s in %d minutes", tradeServersInfo.config[serverID].description, minutes);
  
  // TraceLog(DEBUG_LEVEL, "--->alert='%s'\n", alert);

  return BuildAndSendAlertToAllDaedalus(alert, COM__RGM__DAEDALUS__PROTO__NOTIFICATION_LEVEL_TYPE__ShowLog);
}

/****************************************************************************
- Function name:  BuildNSendGTRAlertToAllDaedalus
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildNSendGTRAlertToAllDaedalus(double amount)
{
  char alert[128] = "\0";
  // sprintf(alert, "%d\t%lf", GTR_CHANGE_ALERT_MASSAGE_TYPE, amount);
  
  if (fgt(amount, 0.00))
  {
    sprintf(alert, "GTR change: Gained $%lf", amount);
  }
  else
  {
    sprintf(alert, "GTR change: Lost $%lf", fabs(amount));
  }
  
  // TraceLog(DEBUG_LEVEL, "--->alert='%s'\n", alert);

  return BuildAndSendAlertToAllDaedalus(alert, COM__RGM__DAEDALUS__PROTO__NOTIFICATION_LEVEL_TYPE__ShowLog);
}

/****************************************************************************
- Function name:  BuildNSendServerRoleConfigurationMisConfiguredAlertToAllDaedalus
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildNSendServerRoleConfigurationMisConfiguredAlertToAllDaedalus(char * servers)
{
  char alert[512] = "\0";
  // sprintf(alert, "%d\t%s", SERVER_ROLE_CONFIGURATION_ERROR_ALERT_MASSAGE_TYPE, servers);
  sprintf(alert, "Server Role Configuration Error: %s", servers);
  
  // TraceLog(DEBUG_LEVEL, "--->alert='%s'\n", alert);

  return BuildAndSendAlertToAllDaedalus(alert, COM__RGM__DAEDALUS__PROTO__NOTIFICATION_LEVEL_TYPE__ShowLog);
}

/****************************************************************************
- Function name:  CheckNRaiseSingleServerTradesAlertToAllTT
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int CheckNRaiseSingleServerTradesAlertToAllTT()
{
  int i, countTS = 0;
  for (i = 0; i < MAX_TRADE_SERVER_CONNECTIONS; i++)
  {
    if (tradeServersInfo.connection[i].status == CONNECTED)
    {
      countTS++;
    }
  }
  
  if (countTS < 2)
  {
    return SUCCESS;
  }
  
  // Check to raise Single Server Trades alert
  if (AlertingProgress == ENABLE)
  {
    int i, count = 0;
    int tradeId = asCrossCollection.lastMonitorIndex;
    for (i = asCrossCollection.lastMonitorIndex; i < asCrossCollection.countCross; i++)
    {
      if (asCrossCollection.crossList[i].tradeIndex >= 0)
      { 
        if (asCrossCollection.crossList[i].tsIndex == asCrossCollection.crossList[tradeId].tsIndex)
        {
          count++;
        
          if ((AlertConf.singleServerTradesThreshhold > 0) &&
            (count >= AlertConf.singleServerTradesThreshhold))
          {
            // Raise alert
            BuildNSendSingleServerTradesAlertToAllDaedalus(tradeServersInfo.config[asCrossCollection.crossList[tradeId].tsIndex].description);
            BuildNSendSingleServerTradesAlertToAllTT(tradeServersInfo.config[asCrossCollection.crossList[tradeId].tsIndex].description);
            
            asCrossCollection.lastMonitorIndex = i;
            tradeId = asCrossCollection.lastMonitorIndex;         
            count = 0;
          }
        }
        else
        {             
          asCrossCollection.lastMonitorIndex = i;
          tradeId = asCrossCollection.lastMonitorIndex;         
          count = 0;
        }
      }
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  BuildNSendSingleServerTradesAlertToAllDaedalus
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildNSendSingleServerTradesAlertToAllDaedalus(char *server)
{
  char alert[128] = "\0";
  // sprintf(alert, "%d\t%s", SINGLE_SERVER_TRADES_ALERT_MASSAGE_TYPE, server);
  
  sprintf(alert, "Single Server Trades: %s", server);
  
  // TraceLog(DEBUG_LEVEL, "--->alert='%s'\n", alert);

  return BuildAndSendAlertToAllDaedalus(alert, COM__RGM__DAEDALUS__PROTO__NOTIFICATION_LEVEL_TYPE__ShowLog);
}

/****************************************************************************
- Function name:  BuildNSendTradingDisabledAlertToAllDaedalus
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildNSendTradingDisabledAlertToAllDaedalus(char *reason)
{
  if (strlen(reason) == 0)
  {
    return SUCCESS;
  }
  
  char alert[256] = "\0";
  sprintf(alert, "Trading Disabled: %s", reason);
  
  // TraceLog(DEBUG_LEVEL, "--->alert='%s'\n", alert);

  return BuildAndSendAlertToAllDaedalus(alert, COM__RGM__DAEDALUS__PROTO__NOTIFICATION_LEVEL_TYPE__ShowLog);
}

/****************************************************************************
- Function name:  BuildNSendGroupfOfBookDataUnhandledAlertToAllDaedalus
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildNSendGroupfOfBookDataUnhandledAlertToAllDaedalus(char *reason)
{
  char alert[256];
  sprintf(alert, "Data unhandled: %s", reason);
  
  // TraceLog(DEBUG_LEVEL, "--->alert='%s'\n", alert);

  return BuildAndSendAlertToAllDaedalus(alert, COM__RGM__DAEDALUS__PROTO__NOTIFICATION_LEVEL_TYPE__ShowLog);
}

/****************************************************************************
- Function name:  BuildNSendDisconnectAlertToAllDaedalus
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildNSendDisconnectAlertToAllDaedalus(char *service)
{
  if (strlen(service) == 0)
  {
    return SUCCESS;
  }
  
  char alert[256] = "\0";
  sprintf(alert, "Disconnect: %s", service);
  
  // TraceLog(DEBUG_LEVEL, "--->alert='%s'\n", alert);

  return BuildAndSendAlertToAllDaedalus(alert, COM__RGM__DAEDALUS__PROTO__NOTIFICATION_LEVEL_TYPE__ShowLog);
}

/****************************************************************************
- Function name:  BuildNSendStuckNearingLimitAlertToAllDaedalus
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildNSendStuckNearingLimitAlertToAllDaedalus()
{
  char alert[128] = "Nearing Stuck Limit";
  // sprintf(alert, "%d", NEARING_STUCK_LIMIT_ALERT_MASSAGE_TYPE);
  
  // TraceLog(DEBUG_LEVEL, "--->alert='%s'\n", alert);

  return BuildAndSendAlertToAllDaedalus(alert, COM__RGM__DAEDALUS__PROTO__NOTIFICATION_LEVEL_TYPE__ShowLog);
}

/****************************************************************************
- Function name:  BuildNSendConsecutiveLossNearingLimitAlertToAllDaedalus
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildNSendConsecutiveLossNearingLimitAlertToAllDaedalus()
{
  char alert[128] = "Nearing Consecutive Loss Limit";
  // sprintf(alert, "%d", NEARING_CONSECUTIVE_LOSS_LIMIT_ALERT_MASSAGE_TYPE);
  
  // TraceLog(DEBUG_LEVEL, "--->alert='%s'\n", alert);

  return BuildAndSendAlertToAllDaedalus(alert, COM__RGM__DAEDALUS__PROTO__NOTIFICATION_LEVEL_TYPE__ShowLog);
}

/****************************************************************************
- Function name:  BuildNSendPMReleasePositionAlertToAllDaedalus
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildNSendPMReleasePositionAlertToAllDaedalus(char *symbol, double price, int share, char side, int account)
{
  char alert[128] = "\0";
  // sprintf(alert, "%d\t%s\t%lf\t%d\t%d\t%d", PM_RELEASE_POSITION_ALERT_MASSAGE_TYPE, symbol, price, share, side, account);
  if (side == ASK_SIDE)
  {
    sprintf(alert, "PM Release: Short %d %s $%lf Account %s", abs(share), symbol, price, TradingAccount.DIRECT[account]);
  }
  else
  {
    sprintf(alert, "PM Release: Long %d %s $%lf Account %s", abs(share), symbol, price, TradingAccount.DIRECT[account]);
  }
  
  // TraceLog(DEBUG_LEVEL, "--->alert='%s'\n", alert);

  return BuildAndSendAlertToAllDaedalus(alert, COM__RGM__DAEDALUS__PROTO__NOTIFICATION_LEVEL_TYPE__ShowLog);
}

/****************************************************************************
- Function name:  BuildNSendUnhandledOpenPositionWithoutAnOrderAlertToDaedalus
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildNSendUnhandledOpenPositionWithoutAnOrderAlertToDaedalus(char *symbol, int share[MAX_ACCOUNT], char side[MAX_ACCOUNT], int account[MAX_ACCOUNT], int count)
{
  char alert[1024] = "\0", tmp[80] = "\0";
  int i;
  
  // sprintf(alert, "%d", POSITION_WITH_NO_ORDER_ALERT_MASSAGE_TYPE);
  sprintf(alert, "Position with no order:");
  
  for (i = 0; i < count; i++)
  {
    //sprintf(tmp, "\t%s\t%d\t%d\t%d", symbol, share[i], side[i], account[i]);
    
    //strcat(alert, tmp);
    
    if (side[i] == ASK_SIDE)
    {
      sprintf(tmp, " Short %d %s %s;", abs(share[i]), symbol, TradingAccount.DIRECT[account[i]]);
    }
    else
    {
      sprintf(tmp, " Long %d %s %s;", share[i], symbol, TradingAccount.DIRECT[account[i]]);
    }
    
    strcat(alert, tmp);
    memset(tmp, 0, sizeof(tmp));
  }
  
  // TraceLog(DEBUG_LEVEL, "--->alert='%s'\n", alert);

  return BuildAndSendAlertToAllDaedalus(alert, COM__RGM__DAEDALUS__PROTO__NOTIFICATION_LEVEL_TYPE__ShowLog);
}

/****************************************************************************
- Function name:  BuildNSendPMHasntExitPositionAlertToAllDaedalus
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildNSendPMHasntExitPositionAlertToAllDaedalus(char *symbol, double price, int share, char side, int account)
{
  char alert[128] = "\0";
  // sprintf(alert, "%d\t%s\t%lf\t%d\t%d\t%d", PM_HASNT_EXIT_POSITION_ALERT_MASSAGE_TYPE, symbol, price, share, side, account);
  if (side == ASK_SIDE)
  {
    sprintf(alert, "PM Stuck: Short %d %s %lf Account %s", abs(share), symbol, price, TradingAccount.DIRECT[account]);
  }
  else
  {
    sprintf(alert, "PM Stuck: Long %d %s %lf Account %s", share, symbol, price, TradingAccount.DIRECT[account]);
  }
  
  // TraceLog(DEBUG_LEVEL, "--->alert='%s'\n", alert);

  return BuildAndSendAlertToAllDaedalus(alert, COM__RGM__DAEDALUS__PROTO__NOTIFICATION_LEVEL_TYPE__ShowLog);
}

/****************************************************************************
- Function name:  BuildNSendPMDoesNotAcceptPositionAlertToAllDaedalus
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildNSendPMDoesNotAcceptPositionAlertToAllDaedalus(char *symbol, double price, int share, char side, int account)
{
  char alert[128] = "\0";
  // sprintf(alert, "%d\t%s\t%lf\t%d\t%d\t%d", PM_DOES_NOT_ACCEPT_POSITION_ALERT_MASSAGE_TYPE, symbol, price, share, side, account);
  
  if (side == ASK_SIDE)
  {
    sprintf(alert, "Manual Stuck: Short %d %s %lf Account %s", abs(share), symbol, price, TradingAccount.DIRECT[account]);
  }
  else
  {
    sprintf(alert, "Manual Stuck: Long %d %s %lf Account %s", abs(share), symbol, price, TradingAccount.DIRECT[account]);
  }
  
  // TraceLog(DEBUG_LEVEL, "--->alert='%s'\n", alert);

  return BuildAndSendAlertToAllDaedalus(alert, COM__RGM__DAEDALUS__PROTO__NOTIFICATION_LEVEL_TYPE__ShowLog);
}


/****************************************************************************
- Function name:  BuildNSendDisabledSymbolAlertToAllDaedalus
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildNSendDisabledSymbolAlertToAllDaedalus(char *reason)
{
  if (strlen(reason) == 0)
  {
    return SUCCESS;
  }

  char alert[256] = "\0";
  sprintf(alert, "Loss Limit: %s", reason);

  return BuildAndSendAlertToAllDaedalus(alert, COM__RGM__DAEDALUS__PROTO__NOTIFICATION_LEVEL_TYPE__ShowLog);
}

/****************************************************************************
- Function name:  SendHaltStockListToAllDaedalus
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendHaltStockListToAllDaedalus()
{
  int ttIndex;
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;

  // Build PM halted symbol message to send Daedalus
  BuildHaltedSymbolListToDaedalus(&ttMessage);

  // Send message to all Daedalus connected to AS
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];

    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_DAEDALUS)
    {
      continue;
    }   

    // Send message to Daedalus
    SendDaedalusMessage(connection, &ttMessage);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendHaltStockListToDaedalus
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendHaltStockListToDaedalus(void *pConnection)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Send Halt stock list to TT ...\n");

  t_TTMessage ttMessage;

  // Build PM halted symbol message to send Daedalus
  BuildHaltedSymbolListToDaedalus(&ttMessage);

  // Send message to Daedalus
  return SendDaedalusMessage(connection, &ttMessage);
}

/****************************************************************************
- Function name:  SendAHaltStockToAllDaedalus
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendAHaltStockToAllDaedalus(const int stockSymbolIndex)
{
  int ttIndex;
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;

  // Build PM halted symbol message to send Daedalus
  BuildHaltedSymbolListToDaedalus(&ttMessage);

  // Send message to all Daedalus connected to AS
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];

    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_DAEDALUS)
    {
      continue;
    }   

    // Send message to Daedalus
    SendDaedalusMessage(connection, &ttMessage);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessSendCancelRequestToTS
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessSendCancelRequestToTS(int clOrdId, char *ECNOrderId, int tsId)
{
  int indexOpenOrder = CheckOrderIdIndex(clOrdId, tsId);

  if ((indexOpenOrder == -1) || (indexOpenOrder == -2))
  {
    TraceLog(ERROR_LEVEL, "Cannot find order information with clOrdId = %d and tsId = %d\n", clOrdId, tsId);

    return ERROR;
  }

  // Find Trade Server index
  int tsIndex = FindTradeServerIndex(tsId);

  if (tsIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "Invalid Trade Server Id = %d\n", tsId);

    return ERROR;
  }

  char side = asOpenOrder.orderCollection[indexOpenOrder].orderSummary.side;

  if ((side == BUY_TO_CLOSE_TYPE) || (side == BUY_TO_OPEN_TYPE))
  {
    side = '1';
  }
  else
  {
    side = '2';
  }

  switch (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.ECNId)
  {
    case TYPE_ARCA_DIRECT:
      /*
      int clOrdID;
      int ARCAOrderID;
      char symbol[SYMBOL_LEN];
      char side;
      */
      SendCancelRequestToTS(tsIndex, clOrdId, atol(ECNOrderId), asOpenOrder.orderCollection[indexOpenOrder].orderSummary.symbol, side);
      break;

    case TYPE_NASDAQ_OUCH:
      TraceLog(ERROR_LEVEL, "AS does not support send NASDAQ OUCH cancel request to TS\n");
      break;

    case TYPE_NASDAQ_RASH:
      TraceLog(ERROR_LEVEL, "AS does not support send NASDAQ RASH cancel request to TS\n");
      break;

    case TYPE_BZX_BOE:
      TraceLog(ERROR_LEVEL, "AS does not support send BATSZ BOE cancel request to TS\n");
      break;

    case TYPE_EDGX_DIRECT:
      TraceLog(ERROR_LEVEL, "AS does not support send EDGX DIRECT cancel request to TS\n");
      break;

    case TYPE_NYSE_CCG:
      TraceLog(ERROR_LEVEL, "AS does not support send NYSE CCG cancel request to TS\n");
      break;
      
    case TYPE_EDGA_DIRECT:
      TraceLog(ERROR_LEVEL, "AS does not support send EDGA DIRECT cancel request to TS\n");
      break;
      
    case TYPE_NASDAQ_OUBX:
      TraceLog(ERROR_LEVEL, "AS does not support send OUCH BX cancel request to TS\n");
      break;
      
    case TYPE_BYX_BOE:
      TraceLog(ERROR_LEVEL, "AS does not support send BYX BOE cancel request to TS\n");
      break;
      
    case TYPE_PSX:
      TraceLog(ERROR_LEVEL, "AS does not support send PSX cancel request to TS\n");
      break;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendTSPMBookIdleWarningToAllDaedalus
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendTSPMBookIdleWarningToAllDaedalus(int tsPmIndex, int ecnIndex, int groupIndex)
{
  int ttIndex;
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;

  // Build TS global configuration message to send Daedalus
  // BuildTTMessageForTSPMBookIdleWarning(&ttMessage, tsPmIndex, ecnIndex, groupIndex);
  BuildBookIdleWarningToDaedalus(&ttMessage, tsPmIndex, ecnIndex, groupIndex);

  // Send message to all Daedalus connected to AS
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];

    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_DAEDALUS)
    {
      continue;
    }   

    // Send message to Daedalus
    SendDaedalusMessage(connection, &ttMessage);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendExchangesConfToAllDaedalus
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendExchangesConfToAllDaedalus(int tsIndex)
{
  int ttIndex;
  t_ConnectionStatus *connection;

  // Send message to all TT connected
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];
    
    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_DAEDALUS)
    {
      continue;
    }

    // Send message to Daedalus
    BuildAndSendTSExchangeConfigToDaedalus(connection, tsIndex);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendExchangesConfOfAllTSToDaedalus
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendExchangesConfOfAllTSToDaedalus(void *pConnection)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  int tsIndex;

  for (tsIndex = 0; tsIndex < MAX_TRADE_SERVER_CONNECTIONS; tsIndex++)
  {
    // Check connection between AS and TS
    if ((tradeServersInfo.connection[tsIndex].status == DISCONNECT) || (tradeServersInfo.connection[tsIndex].socket == -1))
    {
      continue;
    }   
    
    // Send exchange configuration
    if (BuildAndSendTSExchangeConfigToDaedalus(connection, tsIndex) == ERROR)
    {
      return ERROR;
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendPMExchangesConfToAllDaedalus
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int SendPMExchangesConfToAllDaedalus()
{
  int ttIndex;
  t_ConnectionStatus *connection;

  // Send message to all TT connected
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];

    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_DAEDALUS)
    {
      continue;
    }

    // Send message to Daedalus
    BuildAndSendPMExchangeConfigToDaedalus(connection);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SendMessageForPermitTraderLoginToDaedalus
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendMessageForPermitTraderLoginToDaedalus(int ttIndex, char trader[MAX_LINE_LEN], int state)
{
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;

  // Build AS setting message to send Daedalus
  BuildLoginStatusToDaedalus(&ttMessage, trader, state);

  // Send message to all Daedalus connected to AS
  
  connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];
  
  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  // Send message to Daedalus
  return SendDaedalusMessage(connection, &ttMessage);
}

/****************************************************************************
- Function name:  SendTraderStatusInfoToAllDaedalus
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SendTraderStatusInfoToAllDaedalus(int type)
{
  int ttIndex;
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;

  // Build trader status info message to send Daedalus
  BuildOperatorInfoToDaedalus(&ttMessage);

  // Send message to all Daedalus connected to AS
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];
    
    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_DAEDALUS)
    {
      continue;
    }

    // Send message to Daedalus
    SendDaedalusMessage(connection, &ttMessage);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  GetTraderIndex
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
short GetTraderIndex(char *name)
{
  short i = 0;
  for(i = 0; i < traderCollection.numberTrader; i++)
  {
    if (strcmp(traderCollection.traderStatusInfo[i].userName, name) == 0)
    {
      return i;
    }
  }
  TraceLog(DEBUG_LEVEL, "User %s is not matched, traderCollection.numberTrader = %d\n", name, traderCollection.numberTrader);
  return -1;
}

/****************************************************************************
- Function name:  GetTraderName
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int GetTraderName(short index, char *name)
{
  if ((index < traderCollection.numberTrader) && (index >= 0))
  {
    strcpy(name, traderCollection.traderStatusInfo[index].userName);
    return SUCCESS;
  }
  TraceLog(ERROR_LEVEL, "Trader index is not in range [0..%d]\n", traderCollection.numberTrader - 1);
  return ERROR;
}

#ifndef RELEASE_MODE
void ______PROCESS_RECEIVED_DAEDALUS_MESSAGE_______(){}
#endif

int ProcessWebsocketHandshake(const void *message, int ttIndex)
{
  t_TTMessage *handshakeMsg = (t_TTMessage *)message;
  
  if (handshakeMsg->msgLen < 1)
  {
    TraceLog(ERROR_LEVEL, "Handshake message length is invalid: %d\n", handshakeMsg->msgLen);
    return ERROR;
  }
  
  // TraceLog(DEBUG_LEVEL, "--->Handshake message:'%s'\n", handshakeMsg->msgContent);
  
  // Get Authorization token
  char *occurrence = strstr((char*)handshakeMsg->msgContent, "Authorization:");
  if (occurrence == NULL)
  {
    TraceLog(ERROR_LEVEL, "Can not find the 'Authorization:' in handshake message\n");
    return ERROR;
  }
  
  char token[MAX_LINE_LEN] = "\0";
  occurrence += strlen("Authorization:");
  
  int i;
  int len = strlen(occurrence);
  for (i = 0; i < len; i++)
  {
    if (*occurrence == ' ') //ignore blank
    {
      occurrence++;
      // TraceLog(DEBUG_LEVEL, "i=%d *occurrence='%c'\n", i, *occurrence);
    }
    else
    {
      break;
    }
  }
  
  strncpy(token, occurrence, MAX_LINE_LEN);
  RemoveCharacters(token, MAX_LINE_LEN);
  // TraceLog(DEBUG_LEVEL, "Authorization Token = '%s'\n", token);
  
  len = strlen(token);
  unsigned char decodedToken[MAX_PATH_LEN];
  DecodeBase64(token, len, decodedToken);
  // TraceLog(DEBUG_LEVEL, "Decoded Token: '%s'\n", decodedToken);
  
  t_Parameters params;
  params.countParameters = Lrc_Split((char *)&decodedToken[0], ':', &params);
  if (params.countParameters != 2)
  {
    TraceLog(ERROR_LEVEL, "Can not get username or password from Authorization\n");
    return ERROR;
  }

  // userName
  memcpy((unsigned char *)&traderToolsInfo.connection[ttIndex].userName, params.parameterList[0], MAX_LINE_LEN);
  
  // password
  memcpy((unsigned char *)&traderToolsInfo.connection[ttIndex].password, params.parameterList[1], MAX_LINE_LEN);
  
  // Determine TT or Daedalus
  // Find tradertool in handshake message
  occurrence = strstr((char*)handshakeMsg->msgContent, "tradertool");
  if (occurrence == NULL)
  {
    // Daedalus
    traderToolsInfo.connection[ttIndex].clientType = CLIENT_TOOL_DAEDALUS;

    // Authentication Method: LDAP or Normal
    traderToolsInfo.connection[ttIndex].authenticationMethod = LDAP_AUTHENTICATION_METHOD;

    // Role
    traderToolsInfo.connection[ttIndex].currentState = MONITORING;

    TraceLog(DEBUG_LEVEL, "%s is attempting to log in from Daedalus, authentication %d, role %d\n",
      traderToolsInfo.connection[ttIndex].userName,
      traderToolsInfo.connection[ttIndex].authenticationMethod,
      traderToolsInfo.connection[ttIndex].currentState);
  }
  else
  {
    // TT
    traderToolsInfo.connection[ttIndex].clientType = CLIENT_TOOL_TRADER_TOOL;

    occurrence += strlen("tradertool");
    
    // Authentication Method: LDAP or Normal
    // LDAP method: 0
    // Non-LDAP:  1
    traderToolsInfo.connection[ttIndex].authenticationMethod = *occurrence - '0';

    // Role
    occurrence++;
    traderToolsInfo.connection[ttIndex].currentState = *occurrence - '0';
    
    TraceLog(DEBUG_LEVEL, "%s is attempting to log in from TT, authentication %d, role %d\n",
      traderToolsInfo.connection[ttIndex].userName,
      traderToolsInfo.connection[ttIndex].authenticationMethod,
      traderToolsInfo.connection[ttIndex].currentState);
  }
  return SUCCESS;
}

int ProcessASConnectToServerFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex)
{
  if (!requestMsg->astsconnectionrequest)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Server Connection Request is NULL\n");
    return ERROR;
  }
  if (!requestMsg->astsconnectionrequest->has_connect)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Server Connection Request does not contain Request Type (Connect/Disconnect)\n");
    return ERROR;
  }
  if (!requestMsg->astsconnectionrequest->hostname)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Server Connection Request does not contain Hostname\n");
    return ERROR;
  }

  int tsIndex = FindTradeServerIndexFromDescription(requestMsg->astsconnectionrequest->hostname);
  if (tsIndex > -1)
  {
    // This request is for TS
    if (requestMsg->astsconnectionrequest->connect)
    {
      TraceLog(DEBUG_LEVEL, "Received message to request AS connect to %s (TS%d)\n", requestMsg->astsconnectionrequest->hostname, tsIndex);
      Connect2TradeServer(tsIndex);
    }
    else
    {
      TraceLog(DEBUG_LEVEL, "Received message to request AS disconnect from %s (TS%d)\n", requestMsg->astsconnectionrequest->hostname, tsIndex);
      Disconnect2TradeServer(tsIndex);
    }
  }
  else
  {
    if (strcmp(PM_SERVER_NAME, requestMsg->astsconnectionrequest->hostname) == 0)
    {
      // This request is for PM
      if (requestMsg->astsconnectionrequest->connect)
      {
        TraceLog(DEBUG_LEVEL, "Received message to request AS connect to %s (PM)\n", requestMsg->astsconnectionrequest->hostname);
        Connect2PM();
      }
      else
      {
        TraceLog(DEBUG_LEVEL, "Received message to request AS disconnect from %s (PM)\n", requestMsg->astsconnectionrequest->hostname);
        Disconnect2PM();
      }
    }
    else if (strcmp(AS_SERVER_NAME, requestMsg->astsconnectionrequest->hostname) == 0)
    {
      // This request is for AS
      if (requestMsg->astsconnectionrequest->connect)
      {
        TraceLog(DEBUG_LEVEL, "Daedalus has already connected to %s\n", requestMsg->astsconnectionrequest->hostname);
      }
      else
      {
        TraceLog(DEBUG_LEVEL, "Received message to disconnect from %s\n", requestMsg->astsconnectionrequest->hostname);
        
        t_ConnectionStatus *connection = &traderToolsInfo.connection[ttIndex];
        
        if (connection->statusUpdate == USER_LOGGED || connection->statusUpdate == USER_PASSWORD_INCORRECT)
        {
          return SUCCESS;
        }

        char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
        sprintf(activityDetail, "%d", connection->currentState);

        //Trader disconnect from AS
        ProcessUpdateTTEventLogToDatabase(connection->userName, ACTIVITY_TYPE_DISCONNECT_FROM_AS, activityDetail);

        //Update trader status to offline
        ProcessUpdateTraderStatusToDatabase(connection->userName, OFFLINE);

        CloseConnection(connection);
      }
    }
    else
    {
      // This is not TS nor PM
      TraceLog(ERROR_LEVEL, "Daedalus request message: Hostname is invalid '%s'\n", requestMsg->astsconnectionrequest->hostname);
      return ERROR;
    }
  }

  return SUCCESS;
}

int ProcessConnectMdcFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex)
{
  if (!requestMsg->setmdcconnectionstate)
  {
    TraceLog(ERROR_LEVEL, "Daedalus MDC Connection Request is NULL\n");
    return ERROR;
  }
  if (!requestMsg->setmdcconnectionstate->has_mdc)
  {
    TraceLog(ERROR_LEVEL, "Daedalus MDC Connection Request does not contain MDC\n");
    return ERROR;
  }
  if (!requestMsg->setmdcconnectionstate->hostname)
  {
    TraceLog(ERROR_LEVEL, "Daedalus MDC Connection Request does not contain hostname\n");
    return ERROR;
  }
  if (!requestMsg->setmdcconnectionstate->has_connected)
  {
    TraceLog(ERROR_LEVEL, "Daedalus MDC Connection Request does not contain Connect/Disconnect\n");
    return ERROR;
  }
  
  // TraceLog(DEBUG_LEVEL, "Daedalus MDC Connection Request: hostname='%s' mdcID=%d isConnect=%d\n",
      // requestMsg->setmdcconnectionstate->hostname,
      // requestMsg->setmdcconnectionstate->mdc,
      // requestMsg->setmdcconnectionstate->connected);
  
  int tsIndex = FindTradeServerIndexFromDescription(requestMsg->setmdcconnectionstate->hostname);
  if (tsIndex > -1)
  {
    // This request is for TS
    return ProcessConnectMdcOfTSFromDaedalus(requestMsg, ttIndex, tsIndex);
  }
  else
  {
    if (strcmp(PM_SERVER_NAME, requestMsg->setmdcconnectionstate->hostname) == 0)
    {
      // This request is for PM
      return ProcessConnectMdcOfPMFromDaedalus(requestMsg, ttIndex);
    }
    else
    {
      // This is not TS nor PM
      TraceLog(ERROR_LEVEL, "Daedalus MDC Connection request: Hostname is invalid '%s'\n", requestMsg->setmdcconnectionstate->hostname);
      return ERROR;
    }
  }
}

int ProcessConnectMdcOfTSFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex, int tsIndex)
{
  int isBook = YES; // YES: Book, NO: Feed
  int ecnIndex = DAEDALUS_MDC_INDEX_TO_TS_BOOK_MAPPING[requestMsg->setmdcconnectionstate->mdc];
  if (ecnIndex == -1)
  {
    if (requestMsg->setmdcconnectionstate->mdc == COM__RGM__DAEDALUS__PROTO__MDC__CQS_MDC)
    {
      ecnIndex = CQS_SOURCE;
      isBook = NO;
    }
    else if (requestMsg->setmdcconnectionstate->mdc == COM__RGM__DAEDALUS__PROTO__MDC__UQDF_MDC)
    {
      ecnIndex = UQDF_SOURCE;
      isBook = NO;
    }
    else
    {
      TraceLog(ERROR_LEVEL, "Daedalus MDC Connection request for invalid MDC type (%d)\n", requestMsg->setmdcconnectionstate->mdc);
      return ERROR;
    }
  }

  if (isBook == YES)
  {
    // Request for Book
    if (requestMsg->setmdcconnectionstate->connected)
    {
      if (tradeServersInfo.tsSetting[tsIndex].bookEnable[ecnIndex] == YES)
      {
        // Request to connect book
        TraceLog(DEBUG_LEVEL, "Daedalus requests TS%d(%s) connect to %s (ecnIndex=%d)\n",
          tsIndex, tradeServersInfo.config[tsIndex].description, GetMdcNameFromMdcType(requestMsg->setmdcconnectionstate->mdc), ecnIndex);
          
        // Call function to connect to ECN Book from TS
        if (SendConnectBookToTS(tsIndex, ecnIndex) == SUCCESS)
        {
          //Update connect to ECN book of TS event log to database
          char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
          sprintf(activityDetail, "%d,0,%d%02d", traderToolsInfo.connection[ttIndex].currentState, tradeServersInfo.config[tsIndex].tsId, ecnIndex + 20 + 1);
          ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_BOOK, activityDetail);
        }
      }
      else
      {
        TraceLog(DEBUG_LEVEL, "Could not connect to %s on TS%d(%s)\n",
          GetMdcNameFromMdcType(requestMsg->setmdcconnectionstate->mdc), tsIndex, tradeServersInfo.config[tsIndex].description);
      }
    }
    else
    {
      // Request to disconnect book
      TraceLog(DEBUG_LEVEL, "Daedalus requests TS%d(%s) disconnect from %s (ecnIndex=%d)\n",
        tsIndex, tradeServersInfo.config[tsIndex].description, GetMdcNameFromMdcType(requestMsg->setmdcconnectionstate->mdc), ecnIndex);

      // Call function to disconnect to ECN Book from TS
      if (SendDisconnectBookToTS(tsIndex, ecnIndex) == SUCCESS)
      {
        //Update connect to ECN book of TS event log to database
        char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
        sprintf(activityDetail, "%d,1,%d%02d", traderToolsInfo.connection[ttIndex].currentState, tradeServersInfo.config[tsIndex].tsId, ecnIndex + 20 + 1);
        ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_BOOK, activityDetail);
      }
    }
  }
  else
  {
    // Request for Feed
    if (requestMsg->setmdcconnectionstate->connected)
    {
      if (tradeServersInfo.tsSetting[tsIndex].bboEnable[ecnIndex] == YES)
      {
        // Request to connect feed
        TraceLog(DEBUG_LEVEL, "Daedalus requests TS%d(%s) connect to %s (feedIndex=%d)\n",
          tsIndex, tradeServersInfo.config[tsIndex].description, GetMdcNameFromMdcType(requestMsg->setmdcconnectionstate->mdc), ecnIndex);

        // Send request to connect to CQS/UDQF
        if (SendConnectToCQSnUQDFToTS(tsIndex, ecnIndex) == SUCCESS)
        {
          //Update TT event log: TT request connect to CQS/UQDF on TS
          char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
          sprintf(activityDetail, "%d,0,%d6%d", traderToolsInfo.connection[ttIndex].currentState, tradeServersInfo.config[tsIndex].tsId, ecnIndex + 1);
          ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_BBO, activityDetail);
        }
      }
      else
      {
        TraceLog(DEBUG_LEVEL, "Could not connect to %s on TS%d(%s)\n",
          GetMdcNameFromMdcType(requestMsg->setmdcconnectionstate->mdc), tsIndex, tradeServersInfo.config[tsIndex].description);
      }
    }
    else
    {
      // Request to disconnect feed
      TraceLog(DEBUG_LEVEL, "Daedalus requests TS%d(%s) disconnect from %s (feedIndex=%d)\n",
        tsIndex, tradeServersInfo.config[tsIndex].description, GetMdcNameFromMdcType(requestMsg->setmdcconnectionstate->mdc), ecnIndex);

      // Send request to disconnect to CQS/UDQF
      if (SendDisconnectToCQSnUQDFToTS(tsIndex, ecnIndex) == SUCCESS)
      {
        //Update TT event log: TT request disconnect to CQS/UQDF on TS
        char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
        sprintf(activityDetail, "%d,1,%d6%d", traderToolsInfo.connection[ttIndex].currentState, tradeServersInfo.config[tsIndex].tsId, ecnIndex + 1);
        ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_BBO, activityDetail);
      }
    }
  }

  return SUCCESS;
}

int ProcessConnectMdcOfPMFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex)
{
  int isBook = YES; // YES: Book, NO: Feed
  int ecnIndex = DAEDALUS_MDC_INDEX_TO_PM_BOOK_MAPPING[requestMsg->setmdcconnectionstate->mdc];
  // TraceLog(DEBUG_LEVEL, "--->ecnIndex=%d mdc=%d\n", ecnIndex, requestMsg->setmdcconnectionstate->mdc);
  if (ecnIndex == -1)
  {
    if (requestMsg->setmdcconnectionstate->mdc == COM__RGM__DAEDALUS__PROTO__MDC__CQS_MDC ||
      requestMsg->setmdcconnectionstate->mdc == COM__RGM__DAEDALUS__PROTO__MDC__UQDF_MDC ||
      requestMsg->setmdcconnectionstate->mdc == COM__RGM__DAEDALUS__PROTO__MDC__CTS_MDC ||
      requestMsg->setmdcconnectionstate->mdc == COM__RGM__DAEDALUS__PROTO__MDC__UTDF_MDC)
    {
      isBook = NO;
    }
    else
    {
      TraceLog(ERROR_LEVEL, "Daedalus MDC Connection Request: invalid MDC type (%d)\n", requestMsg->setmdcconnectionstate->mdc);
      return ERROR;
    }
  }

  if (isBook == YES)
  {
    // Request for Book
    if (requestMsg->setmdcconnectionstate->connected)
    {
      if (pmSetting.bookEnable[ecnIndex] == YES)
      {
        // Request to connect book
        TraceLog(DEBUG_LEVEL, "Daedalus requests PM(%s) connect to %s (ecnIndex=%d)\n",
          PM_SERVER_NAME, GetMdcNameFromMdcType(requestMsg->setmdcconnectionstate->mdc), ecnIndex);

        // Call function to connect to request connect to a ECN Book to PM
        if (SendConnectBookToPM(ecnIndex) == SUCCESS)
        {
          //Update TT event log: TT request connect to a ECN book on PM
          char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
          sprintf(activityDetail, "%d,0,75%d", traderToolsInfo.connection[ttIndex].currentState, ecnIndex + 1);
          ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_BOOK, activityDetail);
        }
      }
      else
      {
        TraceLog(DEBUG_LEVEL, "Could not connect to %s on PM\n",
          GetMdcNameFromMdcType(requestMsg->setmdcconnectionstate->mdc));
      }
    }
    else
    {
      // Request to disconnect book
      TraceLog(DEBUG_LEVEL, "Daedalus requests PM(%s) disconnect from %s (ecnIndex=%d)\n",
        PM_SERVER_NAME, GetMdcNameFromMdcType(requestMsg->setmdcconnectionstate->mdc), ecnIndex);

      // Call function to disconnect to a ECN Book to PM
      if (SendDisconnectBookToPM(ecnIndex) == SUCCESS)
      {
        //Update TT event log: TT request disconnect to a ECN book on PM
        char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
        sprintf(activityDetail, "%d,1,75%d", traderToolsInfo.connection[ttIndex].currentState, ecnIndex + 1);
        ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_BOOK, activityDetail);
      }
    }
  }
  else
  {
    // Request for Feed
    if (requestMsg->setmdcconnectionstate->connected)
    {
      // Request to connect feed
      switch (requestMsg->setmdcconnectionstate->mdc)
      {
        case COM__RGM__DAEDALUS__PROTO__MDC__CQS_MDC:
          if (pmSetting.CQS_Enable == YES)
          {
            TraceLog(DEBUG_LEVEL, "Daedalus requests PM(%s) connect to CQS\n", PM_SERVER_NAME);

            // Call function to connect to request connect to CQS Feed to PM
            if (SendConnectCQS_FeedToPM() == SUCCESS)
            {
              //Update TT event log: TT request connect to CQS on PM
              char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
              sprintf(activityDetail, "%d,0,83", traderToolsInfo.connection[ttIndex].currentState);
              ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_BBO, activityDetail);
            }
          }
          else
          {
            TraceLog(DEBUG_LEVEL, "Could not connect to CQS on PM\n");
          }
          break;
        case COM__RGM__DAEDALUS__PROTO__MDC__UQDF_MDC:
          if (pmSetting.UQDF_Enable == YES)
          {
            TraceLog(DEBUG_LEVEL, "Daedalus requests PM(%s) connect to UQDF\n", PM_SERVER_NAME);

            // Call function to connect UQDF Feed to PM
            if (SendConnectUQDF_FeedToPM() == SUCCESS)
            {
              //Update TT event log: TT request connect to UQDF on PM
              char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
              sprintf(activityDetail, "%d,0,84", traderToolsInfo.connection[ttIndex].currentState);
              ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_BBO, activityDetail);
            }
          }
          else
          {
            TraceLog(DEBUG_LEVEL, "Could not connect to UQDF on PM\n");
          }
          break;
        case COM__RGM__DAEDALUS__PROTO__MDC__CTS_MDC:
          if (pmSetting.CTS_Enable == YES)
          {
            TraceLog(DEBUG_LEVEL, "Daedalus requests PM(%s) connect to CTS\n", PM_SERVER_NAME);

            // Call function to connect to request connect to CTS Feed to PM
            if (SendConnectCTS_FeedToPM() == SUCCESS)
            {
              //Update TT event log: TT request connect to CTS on PM
              char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
              sprintf(activityDetail, "%d,0,81", traderToolsInfo.connection[ttIndex].currentState);
              ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_BBO, activityDetail);
            }
          }
          else
          {
            TraceLog(DEBUG_LEVEL, "Could not connect to CTS on PM\n");
          }
          break;
        case COM__RGM__DAEDALUS__PROTO__MDC__UTDF_MDC:
          if (pmSetting.UTDF_Enable == YES)
          {
            TraceLog(DEBUG_LEVEL, "Daedalus requests PM(%s) connect to UTDF\n", PM_SERVER_NAME);

            // Call function to connect to request connect to UTDF to PM
            if (SendConnectUTDFToPM() == SUCCESS)
            {
              //Update TT event log: TT request connect to UTDF on PM
              char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
              sprintf(activityDetail, "%d,0,82", traderToolsInfo.connection[ttIndex].currentState);
              ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_BBO, activityDetail);
            }
          }
          else
          {
            TraceLog(DEBUG_LEVEL, "Could not connect to UTDF on PM\n");
          }
          break;
        default:
          TraceLog(DEBUG_LEVEL, "Daedalus requests PM(%s) connect to invalid MDC %d\n", PM_SERVER_NAME, requestMsg->setmdcconnectionstate->mdc);
          return ERROR;
      }
    }
    else
    {
      // Request to disconnect feed
      switch (requestMsg->setmdcconnectionstate->mdc)
      {
        case COM__RGM__DAEDALUS__PROTO__MDC__CQS_MDC:
          TraceLog(DEBUG_LEVEL, "Daedalus requests PM(%s) disconnect from CQS\n", PM_SERVER_NAME);

          // Call function to disconnect to CQS Feed to PM
          if (SendDisconnectCQS_FeedToPM() == SUCCESS)
          {
            //Update TT event log: TT request disconnect to CQS on PM
            char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
            sprintf(activityDetail, "%d,1,83", traderToolsInfo.connection[ttIndex].currentState);
            ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_BBO, activityDetail);
          }
          break;
        case COM__RGM__DAEDALUS__PROTO__MDC__UQDF_MDC:
          TraceLog(DEBUG_LEVEL, "Daedalus requests PM(%s) disconnect from UQDF\n", PM_SERVER_NAME);

          // Call function to disconnect to UQDF Feed to PM
          if (SendDisconnectUQDF_FeedToPM() == SUCCESS)
          {
            //Update TT event log: TT request disconnect to UQDF on PM
            char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
            sprintf(activityDetail, "%d,1,84", traderToolsInfo.connection[ttIndex].currentState);
            ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_BBO, activityDetail);
          }
          break;
        case COM__RGM__DAEDALUS__PROTO__MDC__CTS_MDC:
          TraceLog(DEBUG_LEVEL, "Daedalus requests PM(%s) disconnect from CTS\n", PM_SERVER_NAME);

          // Call function to disconnect to CTS Feed to PM
          if (SendDisconnectCTS_FeedToPM() == SUCCESS)
          {
            //Update TT event log: TT request disconnect to CTS on PM
            char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
            sprintf(activityDetail, "%d,1,81", traderToolsInfo.connection[ttIndex].currentState);
            ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_BBO, activityDetail);
          }
          break;
        case COM__RGM__DAEDALUS__PROTO__MDC__UTDF_MDC:
          TraceLog(DEBUG_LEVEL, "Daedalus requests PM(%s) disconnect from UTDF\n", PM_SERVER_NAME);

          // Call function to disconnect to UTDF to PM
          if (SendDisconnectUTDFToPM() == SUCCESS)
          {
            //Update TT event log: TT request disconnect to UTDF on PM
            char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
            sprintf(activityDetail, "%d,1,82", traderToolsInfo.connection[ttIndex].currentState);
            ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_BBO, activityDetail);
          }
          break;
        default:
          TraceLog(DEBUG_LEVEL, "Daedalus requests PM(%s) disconnect from invalid MDC %d\n", PM_SERVER_NAME, requestMsg->setmdcconnectionstate->mdc);
          return ERROR;
      }
    }
  }

  return SUCCESS;
}

int ProcessConnectOecFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex)
{
  if (!requestMsg->setoecstate)
  {
    TraceLog(ERROR_LEVEL, "Daedalus OEC Connection Request is NULL\n");
    return ERROR;
  }
  if (!requestMsg->setoecstate->has_oec)
  {
    TraceLog(ERROR_LEVEL, "Daedalus OEC Connection Request does not contain specific OEC\n");
    return ERROR;
  }
  if (!requestMsg->setoecstate->has_externalstate)
  {
    TraceLog(ERROR_LEVEL, "Daedalus OEC Connection Request does not contain specific OEC request\n");
    return ERROR;
  }
  if (!requestMsg->setoecstate->hostname)
  {
    TraceLog(ERROR_LEVEL, "Daedalus OEC Connection Request does not contain hostname\n");
    return ERROR;
  }
  
  // TraceLog(DEBUG_LEVEL, "Daedalus OEC State: hostname='%s' oec=%d externalstate=%d\n",
      // requestMsg->setoecstate->hostname,
      // requestMsg->setoecstate->oec,
      // requestMsg->setoecstate->externalstate);
  
  int tsIndex = FindTradeServerIndexFromDescription(requestMsg->setoecstate->hostname);
  if (tsIndex > -1)
  {
    // This request is for TS
    return ProcessConnectOecOfTSFromDaedalus(requestMsg, ttIndex, tsIndex);
  }
  else
  {
    if (strcmp(PM_SERVER_NAME, requestMsg->setoecstate->hostname) == 0)
    {
      // This request is for PM
      return ProcessConnectOecOfPMFromDaedalus(requestMsg, ttIndex);
    }
    else if (strcmp(AS_SERVER_NAME, requestMsg->setoecstate->hostname) == 0)
    {
      // This request is for AS
      return ProcessConnectOecOfASFromDaedalus(requestMsg, ttIndex);
    }
    else
    {
      // This is not TS nor PM
      TraceLog(ERROR_LEVEL, "Daedalus OEC Connection Request: Hostname is invalid '%s'\n", requestMsg->setoecstate->hostname);
      return ERROR;
    }
  }
}

int ProcessConnectOecOfTSFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex, int tsIndex)
{
  int orderIndex = DAEDALUS_OEC_INDEX_TO_TS_ORDER_MAPPING[requestMsg->setoecstate->oec];
  if (orderIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "Daedalus OEC Connection Request: invalid TS OEC type (%d) of TS\n", requestMsg->setoecstate->oec);
    return ERROR;
  }

  switch(requestMsg->setoecstate->externalstate)
  {
    case COM__RGM__DAEDALUS__PROTO__OEC_EXTERNAL_STATE__OecActive:
      if (tradeServersInfo.tsSetting[tsIndex].orderEnable[orderIndex] == YES)
      {
        // Request to connect and enable trading for order
        TraceLog(DEBUG_LEVEL, "%s requests TS%d(%s) connect and enable trading for %s (orderIndex=%d)\n",
          traderToolsInfo.connection[ttIndex].userName, tsIndex, tradeServersInfo.config[tsIndex].description, GetOecNameFromOecType(requestMsg->setoecstate->oec), orderIndex);
          
        if (GetOrderConnectionStatusOfTS(tsIndex, orderIndex) == CONNECTED)
        {
          if (GetBookConnectionStatusOfTS(tsIndex, TS_ORDER_INDEX_TO_BOOK_MAPPING[orderIndex]) == CONNECTED)
          {
            // Enable trading immediately
            TraceLog(DEBUG_LEVEL, "Enable trading for tsIndex=%d '%s' orderIndex=%d immediately\n", tsIndex, GetOecNameFromOecType(requestMsg->setoecstate->oec), orderIndex);
            if (SendEnableTradingAOrderToTS(tsIndex, orderIndex) == SUCCESS)
            {
              //Update enable trading on a ECN odrder of TS event log to database
              char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
              sprintf(activityDetail, "%d,0,%d%02d", traderToolsInfo.connection[ttIndex].currentState, tradeServersInfo.config[tsIndex].tsId, orderIndex + 1);
              ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_ENABLE_DISENABLE_TRADING, activityDetail);
            }
          }
        }
        else
        {
          if (GetBookConnectionStatusOfTS(tsIndex, TS_ORDER_INDEX_TO_BOOK_MAPPING[orderIndex]) == CONNECTED)
          {
            //Holding enable trading and waiting for TS orderIndex is connected
            TraceLog(DEBUG_LEVEL, "Hold enable trading of tsIndex=%d '%s' orderIndex=%d\n", tsIndex, GetOecNameFromOecType(requestMsg->setoecstate->oec), orderIndex);
            RequestToEnableTradingTS[tsIndex][orderIndex].isEnabled = 1;
            RequestToEnableTradingTS[tsIndex][orderIndex].ttIndex = ttIndex;
          }
          
          if (SendConnectOrderToTS(tsIndex, orderIndex) == SUCCESS)
          {
            //Update enable trading on all ECN
            char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
            sprintf(activityDetail, "%d,0,%d%02d", traderToolsInfo.connection[ttIndex].currentState, tradeServersInfo.config[tsIndex].tsId, orderIndex + 40 + 1);
            
            ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_ORDER, activityDetail);
          }
        }
      }
      else
      {
        TraceLog(DEBUG_LEVEL, "Could not enable trading for %s on %s\n",
          GetOecNameFromOecType(requestMsg->setoecstate->oec),
          tradeServersInfo.config[tsIndex].description);
      }
      break;
    case COM__RGM__DAEDALUS__PROTO__OEC_EXTERNAL_STATE__OecInactive:
      // Request to disable trading of order but keep order connection alive
        
      if (GetTradingStatusOfTSOrder(tsIndex, orderIndex) != DISABLE)
      {
        TraceLog(DEBUG_LEVEL, "%s requests TS%d(%s) disable trading for %s (orderIndex=%d)\n",
          traderToolsInfo.connection[ttIndex].userName, tsIndex, tradeServersInfo.config[tsIndex].description, GetOecNameFromOecType(requestMsg->setoecstate->oec), orderIndex);
        
        // Call function to disable trading on a ECN order
        if (SendDisableTradingAOrderToTS(tsIndex, orderIndex, ttIndex) == SUCCESS)
        {
          //Update disenable trading on a ECN odrder of TS event log to database
          char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
          sprintf(activityDetail, "%d,1,%d%02d", traderToolsInfo.connection[ttIndex].currentState, tradeServersInfo.config[tsIndex].tsId, orderIndex + 1);
          ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_ENABLE_DISENABLE_TRADING, activityDetail);
        }
      }
      
      if (tradeServersInfo.tsSetting[tsIndex].orderEnable[orderIndex] == YES)
      {
        if (GetOrderConnectionStatusOfTS(tsIndex, orderIndex) != CONNECTED)
        {
          TraceLog(DEBUG_LEVEL, "%s requests TS%d(%s) connect to %s (orderIndex=%d)\n",
            traderToolsInfo.connection[ttIndex].userName, tsIndex, tradeServersInfo.config[tsIndex].description, GetOecNameFromOecType(requestMsg->setoecstate->oec), orderIndex);
          if (SendConnectOrderToTS(tsIndex, orderIndex) == SUCCESS)
          {
            //Update enable trading on all ECN
            char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
            sprintf(activityDetail, "%d,0,%d%02d", traderToolsInfo.connection[ttIndex].currentState, tradeServersInfo.config[tsIndex].tsId, orderIndex + 40 + 1);

            ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_ORDER, activityDetail);
          }
        }
      }
      else
      {
        TraceLog(DEBUG_LEVEL, "Could not connect to %s on %s\n",
          GetOecNameFromOecType(requestMsg->setoecstate->oec),
          tradeServersInfo.config[tsIndex].description);
      }
      break;
    case COM__RGM__DAEDALUS__PROTO__OEC_EXTERNAL_STATE__OecDown:
      // Request to disconnect order
      TraceLog(DEBUG_LEVEL, "%s requests TS%d(%s) disconnect from %s (orderIndex=%d)\n",
        traderToolsInfo.connection[ttIndex].userName,tsIndex, tradeServersInfo.config[tsIndex].description, GetOecNameFromOecType(requestMsg->setoecstate->oec), orderIndex);
      if (SendDisconnectOrderToTS(tsIndex, orderIndex) == SUCCESS)
      {
        //Update enable trading on all ECN
        char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
        sprintf(activityDetail, "%d,1,%d%02d", traderToolsInfo.connection[ttIndex].currentState, tradeServersInfo.config[tsIndex].tsId, orderIndex + 40 + 1);

        ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_ORDER, activityDetail);
      }
      break;
    default:
      TraceLog(ERROR_LEVEL, "Daedalus requests TS%d(%s) to perform invalid action (externalstate=%d)\n", tsIndex, tradeServersInfo.config[tsIndex].description, requestMsg->setoecstate->externalstate);
      return ERROR;
  }
  return SUCCESS;
}

int ProcessConnectOecOfASFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex)
{
  int orderIndex = DAEDALUS_OEC_INDEX_TO_AS_ORDER_MAPPING[requestMsg->setoecstate->oec];
  if (orderIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "Daedalus OEC Connection Request: invalid AS OEC type (%d) of AS\n", requestMsg->setoecstate->oec);
    return ERROR;
  }

  switch(requestMsg->setoecstate->externalstate)
  {
    case COM__RGM__DAEDALUS__PROTO__OEC_EXTERNAL_STATE__OecActive:
      if (asSetting.orderEnable[orderIndex] == YES)
      {
        // Request to connect and enable trading for order
        TraceLog(DEBUG_LEVEL, "%s requests AS to connect and enable trading for %s (orderIndex=%d)\n",
          traderToolsInfo.connection[ttIndex].userName, GetOecNameFromOecType(requestMsg->setoecstate->oec), orderIndex);
        
        // Order of AS is always enabled trading
        
        // Call function to AS connect to ECN Order
        if (ProcessASConnectToOrderECN(orderIndex) == SUCCESS)
        {
          //Update connect order on AS
          char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
          sprintf(activityDetail, "%d,0,1%d", traderToolsInfo.connection[ttIndex].currentState, orderIndex + 1);
          ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_ORDER, activityDetail);
        }
      }
      else
      {
        TraceLog(DEBUG_LEVEL, "Could not connect to %s on AS\n", GetOecNameFromOecType(requestMsg->setoecstate->oec));
      }
      break;
    case COM__RGM__DAEDALUS__PROTO__OEC_EXTERNAL_STATE__OecInactive:
      if (asSetting.orderEnable[orderIndex] == YES)
      {
        // Request to connect order
        TraceLog(DEBUG_LEVEL, "%s requests AS to connect to %s (orderIndex=%d)\n",
          traderToolsInfo.connection[ttIndex].userName, GetOecNameFromOecType(requestMsg->setoecstate->oec), orderIndex);
        // Call function to AS connect to ECN Order
        if (ProcessASConnectToOrderECN(orderIndex) == SUCCESS)
        {
          //Update connect order on AS
          char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
          sprintf(activityDetail, "%d,0,1%d", traderToolsInfo.connection[ttIndex].currentState, orderIndex + 1);
          ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_ORDER, activityDetail);
        }
      }
      else
      {
        TraceLog(DEBUG_LEVEL, "Could not connect to %s on AS\n", GetOecNameFromOecType(requestMsg->setoecstate->oec));
      }
      break;
    case COM__RGM__DAEDALUS__PROTO__OEC_EXTERNAL_STATE__OecDown:
      // Request to disconnect order
      TraceLog(DEBUG_LEVEL, "%s requests AS to disconnect from %s (orderIndex=%d)\n",
        traderToolsInfo.connection[ttIndex].userName, GetOecNameFromOecType(requestMsg->setoecstate->oec), orderIndex);
      // Call function to AS disconnect to ECN Order
      if (ProcessASDisconnectToOrderECN(orderIndex) == SUCCESS)
      {
        //Update disconnect order on AS
        char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
        sprintf(activityDetail, "%d,1,1%d", traderToolsInfo.connection[ttIndex].currentState, orderIndex + 1);
        ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_ORDER, activityDetail);
      }
      break;
    default:
      TraceLog(ERROR_LEVEL, "Daedalus requests AS to perform invalid action (externalstate=%d)\n", requestMsg->setoecstate->externalstate);
      return ERROR;
  }
  return SUCCESS;
}

int ProcessConnectOecOfPMFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex)
{
  int orderIndex = DAEDALUS_OEC_INDEX_TO_PM_ORDER_MAPPING[requestMsg->setoecstate->oec];
  if (orderIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "Daedalus OEC Connection Request: invalid PM OEC type (%d) of PM\n", requestMsg->setoecstate->oec);
    return ERROR;
  }

  switch(requestMsg->setoecstate->externalstate)
  {
    case COM__RGM__DAEDALUS__PROTO__OEC_EXTERNAL_STATE__OecActive:
      if (pmSetting.orderEnable[orderIndex] == YES)
      {
        // Request to connect and enable trading for order
        TraceLog(DEBUG_LEVEL, "%s requests PM to connect and enable trading for %s (orderIndex=%d)\n",
          traderToolsInfo.connection[ttIndex].userName, GetOecNameFromOecType(requestMsg->setoecstate->oec), orderIndex);
        
        //Holding enable trading and waiting for PM orderIndex is connected
        if (pmInfo.currentStatus.tradingStatus == DISABLE &&
          (pmInfo.currentStatus.pmMdcStatus[PM_ARCA_BOOK_INDEX] == CONNECTED ||
          pmInfo.currentStatus.pmMdcStatus[PM_NASDAQ_BOOK_INDEX] == CONNECTED))
        {
          RequestToEnableTradingPM.isEnabled = 1;
          RequestToEnableTradingPM.ttIndex = ttIndex;
        }
        
        // Call function to connect to request connect to a ECN Order to PM
        if (SendConnectOrderToPM(orderIndex) == SUCCESS)
        {
          //Update TT event log: TT request connect to ECN Order on PM
          char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
          sprintf(activityDetail, "%d,0,5%d", traderToolsInfo.connection[ttIndex].currentState, orderIndex + 1);
          ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_ORDER, activityDetail);
        }
      }
      else
      {
        TraceLog(DEBUG_LEVEL, "Could not enable trading for %s on PM\n", GetOecNameFromOecType(requestMsg->setoecstate->oec));
      }
      break;
    case COM__RGM__DAEDALUS__PROTO__OEC_EXTERNAL_STATE__OecInactive:
      // Request to connect order
      TraceLog(DEBUG_LEVEL, "%s requests PM to disable trading of %s (orderIndex=%d)\n",
          traderToolsInfo.connection[ttIndex].userName, GetOecNameFromOecType(requestMsg->setoecstate->oec), orderIndex);
        
      // Call function to disable PM to PM
      if (SendDisableTradingPMToPM(ttIndex) == SUCCESS)
      {
        //Update disenable trading event log to database
        char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
        sprintf(activityDetail, "%d,1,99", traderToolsInfo.connection[ttIndex].currentState);
        ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_ENABLE_DISENABLE_TRADING, activityDetail);
      }
      
      if (pmSetting.orderEnable[orderIndex] == YES)
      {
        // Call function to connect to request connect to a ECN Order to PM
        if (SendConnectOrderToPM(orderIndex) == SUCCESS)
        {
          //Update TT event log: TT request connect to ECN Order on PM
          char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
          sprintf(activityDetail, "%d,0,5%d", traderToolsInfo.connection[ttIndex].currentState, orderIndex + 1);
          ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_ORDER, activityDetail);
        }
      }
      else
      {
        TraceLog(DEBUG_LEVEL, "Could not connect to %s on PM\n", GetOecNameFromOecType(requestMsg->setoecstate->oec));
      }
      break;
    case COM__RGM__DAEDALUS__PROTO__OEC_EXTERNAL_STATE__OecDown:
      // Request to disconnect order
      TraceLog(DEBUG_LEVEL, "%s requests PM to disconnect from %s (orderIndex=%d)\n",
        traderToolsInfo.connection[ttIndex].userName, GetOecNameFromOecType(requestMsg->setoecstate->oec), orderIndex);
      // Call function to disconnect to a ECN Order to PM
      if (SendDisconnectOrderToPM(orderIndex) == SUCCESS)
      {
        //Update TT event log: TT request disconnect to ECN Order on PM
        char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
        sprintf(activityDetail, "%d,1,5%d", traderToolsInfo.connection[ttIndex].currentState, orderIndex + 1);
        ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_CONNECT_DISCONNECT_ORDER, activityDetail);
      }
      break;
    default:
      TraceLog(ERROR_LEVEL, "Daedalus requests PM to perform invalid action (externalstate=%d)\n", requestMsg->setoecstate->externalstate);
      return ERROR;
  }
  return SUCCESS;
}

int ProcessGetStockStatusFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex)
{
  if (!requestMsg->querybook)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Stock Status Request is NULL\n");
    return ERROR;
  }
  if (!requestMsg->querybook->symbol)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Stock Status Request does not contain symbol\n");
    return ERROR;
  }
  if (!requestMsg->querybook->server)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Stock Status Request does not contain server name\n");
    return ERROR;
  }

  // TraceLog(DEBUG_LEVEL, "Daedalus Stock Status Request: server='%s' symbol='%s' mdc=%d\n",
      // requestMsg->querybook->server,
      // requestMsg->querybook->symbol,
      // requestMsg->querybook->mdc);

  int tsIndex = FindTradeServerIndexFromDescription(requestMsg->querybook->server);
  if (tsIndex > -1)
  {
    // This request is for TS
    return ProcessGetStockStatusOfTSFromDaedalus(requestMsg, ttIndex, tsIndex);
  }
  else
  {
    if (strcmp(PM_SERVER_NAME, requestMsg->querybook->server) == 0)
    {
      // This request is for PM
      return ProcessGetStockStatusOfPMFromDaedalus(requestMsg, ttIndex);
    }
    else
    {
      // This is not TS nor PM
      TraceLog(ERROR_LEVEL, "Daedalus Stock Status Request: Server is invalid '%s'\n", requestMsg->querybook->server);
      return ERROR;
    }
  }
  return SUCCESS;
}

int ProcessGetStockStatusOfTSFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex, int tsIndex)
{
  char listMdc[TS_MAX_BOOK_CONNECTIONS];
  memset(listMdc, 0, TS_MAX_BOOK_CONNECTIONS);
  
  int i, index;
  for(i = 0 ; i < requestMsg->querybook->n_mdc; i++)
  {
    index = DAEDALUS_MDC_INDEX_TO_TS_BOOK_MAPPING[requestMsg->querybook->mdc[i]];
    if(index < 0 || index >= TS_MAX_BOOK_CONNECTIONS)
    {
      continue;
    }
    listMdc[index] = 1;
  }
  
  SendGetSymbolStatusToTS(tsIndex, ttIndex, (unsigned char *)requestMsg->querybook->symbol, listMdc);
  
  return SUCCESS;
}

int ProcessGetStockStatusOfPMFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex)
{
  char listMdc[PM_MAX_BOOK_CONNECTIONS];
  memset(listMdc, 0, PM_MAX_BOOK_CONNECTIONS);
  
  int i, index;
  for(i = 0 ; i < requestMsg->querybook->n_mdc; i++)
  { 
    index = DAEDALUS_MDC_INDEX_TO_PM_BOOK_MAPPING[requestMsg->querybook->mdc[i]];
    if(index < 0 || index >= PM_MAX_BOOK_CONNECTIONS)
    {
      continue;
    }
    listMdc[index] = 1;   
  }
  
  SendGetSymbolStatusToPM(ttIndex, (unsigned char *)requestMsg->querybook->symbol, listMdc);
  
  return SUCCESS;
}

int ProcessEnableISOTradingFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex)
{
  if (!requestMsg->setiso)
  {
    TraceLog(ERROR_LEVEL, "Daedalus ISO Trading Request is NULL\n");
    return ERROR;
  }
  if (!requestMsg->setiso->hostname)
  {
    TraceLog(ERROR_LEVEL, "Daedalus ISO Trading Request does not contain hostname\n");
    return ERROR;
  }
  if (!requestMsg->setiso->has_isoenabled)
  {
    TraceLog(ERROR_LEVEL, "Daedalus ISO Trading Request does not contain Enable or Disable\n");
    return ERROR;
  }

  // TraceLog(DEBUG_LEVEL, "Daedalus ISO Trading Request: hostname='%s' isoenabled=%d\n",
      // requestMsg->setiso->hostname,
      // requestMsg->setiso->isoenabled);

  int tsIndex = FindTradeServerIndexFromDescription(requestMsg->setiso->hostname);
  if (tsIndex > -1)
  {
    // This request is for TS
    if (requestMsg->setiso->isoenabled)
    {
      TraceLog(DEBUG_LEVEL, "Daedalus requests to enable ISO trading for %s\n", requestMsg->setiso->hostname);
    }
    else
    {
      TraceLog(DEBUG_LEVEL, "Daedalus requests to disable ISO trading for %s\n", requestMsg->setiso->hostname);
    }
    
    if (SendSetISO_TradingToTS(tsIndex, requestMsg->setiso->isoenabled) == SUCCESS)
    {
      BackupTSGlobalConf[tsIndex].ISO_Trading = requestMsg->setiso->isoenabled;
      // Update TT event log: Enable/disable ISO trading on TS
      char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
      sprintf(activityDetail, "%d,%d,%d", traderToolsInfo.connection[ttIndex].currentState, requestMsg->setiso->isoenabled, tradeServersInfo.config[tsIndex].tsId);
      ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_ENABLE_DISENABLE_ISO_TRADING, activityDetail);
    }
    
    //Create a ISO Record
    InsertISOToggleToISOFlightFile(tsIndex, requestMsg->setiso->isoenabled, traderToolsInfo.connection[ttIndex].userName);
  }
  else
  {
    // This is not TS
    TraceLog(ERROR_LEVEL, "Daedalus ISO Trading request: '%s' does not support ISO\n", requestMsg->setiso->hostname);
    return ERROR;
  }
  return SUCCESS;
}

int ProcessIgnoreSymbolFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex)
{
  if (!requestMsg->setdisabledsymbol)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Disabled Symbol Request is NULL\n");
    return ERROR;
  }
  if (!requestMsg->setdisabledsymbol->symbol)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Disabled Symbol Request does not contain symbol\n");
    return ERROR;
  }
  if (!requestMsg->setdisabledsymbol->has_actiontype)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Disabled Symbol Request does not contain Action Type\n");
    return ERROR;
  }
  if (!requestMsg->setdisabledsymbol->has_servertype)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Disabled Symbol Request does not contain Server (TS or PM)\n");
    return ERROR;
  }

  // TraceLog(DEBUG_LEVEL, "Daedalus Disabled Symbol Request: symbol='%s' actiontype=%d serverType=%d\n",
      // requestMsg->setdisabledsymbol->symbol,
      // requestMsg->setdisabledsymbol->actiontype,
      // requestMsg->setdisabledsymbol->servertype);
  
  if (requestMsg->setdisabledsymbol->servertype == COM__RGM__DAEDALUS__PROTO__BLINK_BOX_SERVER_TYPE__TradeServer)
  {
    ProcessIgnoreSymbolForTSFromDaedalus(requestMsg, ttIndex);
  }
  else if (requestMsg->setdisabledsymbol->servertype == COM__RGM__DAEDALUS__PROTO__BLINK_BOX_SERVER_TYPE__PositionManager)
  {
    ProcessIgnoreSymbolForPMFromDaedalus(requestMsg, ttIndex);
  }

  return SUCCESS; 
}

int ProcessIgnoreSymbolForTSFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex)
{
  int i, countSymbol;
  char tmpSymbolList[MAX_STOCK_SYMBOL][SYMBOL_LEN];
  int type;
  
  if (requestMsg->setdisabledsymbol->actiontype == COM__RGM__DAEDALUS__PROTO__ACTION_TYPE__Add)
  {
    TraceLog(DEBUG_LEVEL, "Daedalus Request to disable symbol '%s' for TS\n", requestMsg->setdisabledsymbol->symbol);
    type = ADD_SYMBOL;
    countSymbol = 0;

    memcpy(tmpSymbolList[countSymbol], requestMsg->setdisabledsymbol->symbol, SYMBOL_LEN);
    countSymbol++;
    
    for (i = 0; i < IgnoreStockList.countStock; i++)
    {
      memcpy(tmpSymbolList[countSymbol], IgnoreStockList.stockList[i], SYMBOL_LEN);
      countSymbol++;
    }

    // Update ignored symbol list
    IgnoreStockList.countStock = 0;

    for (i = 0; i < countSymbol; i++)
    {
      memcpy(IgnoreStockList.stockList[IgnoreStockList.countStock], tmpSymbolList[i], SYMBOL_LEN);
      IgnoreStockList.countStock++;
    }
  }
  else if (requestMsg->setdisabledsymbol->actiontype == COM__RGM__DAEDALUS__PROTO__ACTION_TYPE__Remove)
  {
    TraceLog(DEBUG_LEVEL, "Daedalus Request to enable symbol '%s' for TS\n", requestMsg->setdisabledsymbol->symbol);
    type = REMOVE_SYMBOL;
    
    // Reset flag lisID in RMList
    int symbolIndex = GetStockSymbolIndex(requestMsg->setdisabledsymbol->symbol);
    if (symbolIndex == -1)
    {
      TraceLog(ERROR_LEVEL, "(ProcessTTMessageForIgnoreStockList): Could not get symbol index of symbol '%.8s'\n", requestMsg->setdisabledsymbol->symbol);
      return ERROR;
    }
    
    // Reset profit loss
    if (ProfitLossPerSymbol[symbolIndex].listID == DISABLED_LIST_ID)
    {
      int tsIndex;
      for (tsIndex = 0; tsIndex < MAX_TRADE_SERVER_CONNECTIONS; tsIndex++)
      {
        if (fgt(ProfitLossPerSymbol[symbolIndex].tsLoss[tsIndex], 0.00))
        {
          SaveLossBySymbol(requestMsg->setdisabledsymbol->symbol, tsIndex, ProfitLossPerSymbol[symbolIndex].tsLoss[tsIndex]);
        }
      }
      
      memset(&ProfitLossPerSymbol[symbolIndex], 0, sizeof(t_ProfitLossPerSymbol));
      TraceLog(DEBUG_LEVEL, "Remove %s from disabled list, reset loss counter for it\n", requestMsg->setdisabledsymbol->symbol);
    }
    
    countSymbol = 0;

    for (i = 0; i < IgnoreStockList.countStock; i++)
    {
      if (strncmp(IgnoreStockList.stockList[i], requestMsg->setdisabledsymbol->symbol, SYMBOL_LEN) != 0)
      {
        memcpy(tmpSymbolList[countSymbol], IgnoreStockList.stockList[i], SYMBOL_LEN);
        countSymbol++;
      }
    }

    // Update ignored symbol list
    IgnoreStockList.countStock = 0;

    for (i = 0; i < countSymbol; i++)
    {
      memcpy(IgnoreStockList.stockList[IgnoreStockList.countStock], tmpSymbolList[i], SYMBOL_LEN);
      IgnoreStockList.countStock++;
    }
  }
  else
  {
    TraceLog(ERROR_LEVEL, "Daedalus Disabled Symbol Request for TS does not contain Action Type\n");
    return ERROR;
  }
  
  // By default, this message is applied for TS and PM
  // Save data to the file
  SaveIgnoreStockList();

  // Send ignored symbol list to all Trade Server
  if (SendIgnoreStockListToAllTS(type, requestMsg->setdisabledsymbol->symbol) == SUCCESS)
  {
    // Update TT event log to database
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    sprintf(activityDetail, "%d,%d,%.8s", traderToolsInfo.connection[ttIndex].currentState, type, requestMsg->setdisabledsymbol->symbol);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_IGNORE_STOCK_LIST, activityDetail);
  }

  // Send ignored symbol list to all Daedalus
  SendTSIgnoreSymbolListToAllDaedalus();
  SendIgnoreStockListToAllTT();
  
  return SUCCESS;
}

int ProcessIgnoreSymbolForPMFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex)
{
  int type;
  if (requestMsg->setdisabledsymbol->actiontype == COM__RGM__DAEDALUS__PROTO__ACTION_TYPE__Add)
  {
    TraceLog(DEBUG_LEVEL, "Daedalus Request to disable symbol '%s' for PM\n", requestMsg->setdisabledsymbol->symbol);
    type = ADD_SYMBOL;
  }
  else if (requestMsg->setdisabledsymbol->actiontype == COM__RGM__DAEDALUS__PROTO__ACTION_TYPE__Remove)
  {
    TraceLog(DEBUG_LEVEL, "Daedalus Request to enable symbol '%s' for PM\n", requestMsg->setdisabledsymbol->symbol);
    type = REMOVE_SYMBOL;
  }
  else
  {
    TraceLog(ERROR_LEVEL, "Daedalus Disabled Symbol Request for PM does not contain Action Type\n");
    return ERROR;
  }
  
  // Send PM ignored symbol list to PM
  if (SetPMIgnoreStockListToPM(type, requestMsg->setdisabledsymbol->symbol) == SUCCESS)
  {
    // Update TT event log to database
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    sprintf(activityDetail, "%d,%d,%.8s", traderToolsInfo.connection[ttIndex].currentState, type, requestMsg->setdisabledsymbol->symbol);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_IGNORE_STOCK_LIST_PM, activityDetail);
  }
  
  return SUCCESS;
}

int ProcessIgnoreSymbolRangeFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg)
{
  if (!requestMsg->setdisabledsymbolrange)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Disable Symbol Ranage Request message is NULL\n");
    return ERROR;
  }
  if (!requestMsg->setdisabledsymbolrange->start)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Disable Symbol Ranage Request does not contain start of range\n");
    return ERROR;
  }
  // Do not check requestMsg->setdisablesymbolrange->start because there are
  // 2 type of ranges: A or A-B, end may be exist or not
  
  if (!requestMsg->setdisabledsymbolrange->has_actiontype)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Disable Symbol Ranage Request does not contain Action Type\n");
    return ERROR;
  }

  // TraceLog(DEBUG_LEVEL, "Daedalus Disable Symbol Ranage Request: start='%c' end='%c' actiontype=%d\n",
      // requestMsg->setdisabledsymbolrange->start[0],
      // requestMsg->setdisabledsymbolrange->end ? requestMsg->setdisabledsymbolrange->end[0] : 0,
      // requestMsg->setdisabledsymbolrange->actiontype);
      
  int i, countSymbol;
  char tmpSymbolList[MAX_STOCK_SYMBOL][SYMBOL_LEN];
  char range[SYMBOL_LEN] = "\0"; // 2 types of ranges: A or A-B
  int type;
  
  range[0] = requestMsg->setdisabledsymbolrange->start[0];
  if (requestMsg->setdisabledsymbolrange->end)
  {
    if (range[0] < requestMsg->setdisabledsymbolrange->end[0])
    {
      range[1] = '-';
      range[2] = requestMsg->setdisabledsymbolrange->end[0];
    }
    else
    {
      TraceLog(ERROR_LEVEL, "Invalid symbol range from Daedalus: '%c-%c'\n", range[0], requestMsg->setdisabledsymbolrange->end[0]);
      
      // Send ignored symbol list to all Daedalus
      SendIgnoreSymbolRangesListToAllDaedalus();
      SendIgnoreStockRangesListToAllTT();
      return ERROR;
    }
  }
  
  if (requestMsg->setdisabledsymbolrange->actiontype == COM__RGM__DAEDALUS__PROTO__ACTION_TYPE__Add)
  {
    type = ADD_SYMBOL;
    countSymbol = 0;

    memcpy(tmpSymbolList[countSymbol], range, SYMBOL_LEN);
    countSymbol++;
    for (i = 0; i < IgnoreStockRangesList.countStock; i++)
    {
      memcpy(tmpSymbolList[countSymbol], IgnoreStockRangesList.stockList[i], SYMBOL_LEN);
      countSymbol++;
    }

    // Update ignored symbol list
    IgnoreStockRangesList.countStock = 0;

    for (i = 0; i < countSymbol; i++)
    {
      memcpy(IgnoreStockRangesList.stockList[IgnoreStockRangesList.countStock], tmpSymbolList[i], SYMBOL_LEN);
      IgnoreStockRangesList.countStock++;
    }
  }
  else if (requestMsg->setdisabledsymbolrange->actiontype == COM__RGM__DAEDALUS__PROTO__ACTION_TYPE__Remove)
  {
    type = REMOVE_SYMBOL;
    countSymbol = 0;

    for (i = 0; i < IgnoreStockRangesList.countStock; i++)
    {
      if (strncmp(IgnoreStockRangesList.stockList[i], range, SYMBOL_LEN) != 0)
      {
        memcpy(tmpSymbolList[countSymbol], IgnoreStockRangesList.stockList[i], SYMBOL_LEN);
        countSymbol++;
      }
    }

    // Update ignored symbol list
    IgnoreStockRangesList.countStock = 0;

    for (i = 0; i < countSymbol; i++)
    {
      memcpy(IgnoreStockRangesList.stockList[IgnoreStockRangesList.countStock], tmpSymbolList[i], SYMBOL_LEN);
      IgnoreStockRangesList.countStock++;
    }
  }
  else
  {
    TraceLog(ERROR_LEVEL, "Daedalus Disable Symbol Ranage Request does not contain Action Type\n");
    return ERROR;
  }
  // Save data to the file
  SaveIgnoreStockRangesList();

  // Send ignored symbol list to all Trade Server
    SendIgnoreStockRangesListToAllTS(type, range);
  
  // Send ignored symbol list to all Daedalus
  SendIgnoreSymbolRangesListToAllDaedalus();
  SendIgnoreStockRangesListToAllTT();
  
  //PM does not manage to ignore range symbol list
  
  return SUCCESS;
}

int ProcessSetHaltedSymbolFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex)
{
  if (!requestMsg->sethaltedsymbol)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Set Halted Symbol Request is NULL\n");
    return ERROR;
  }
  if (!requestMsg->sethaltedsymbol->symbol)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Set Halted Symbol Request does not contain symbol\n");
    return ERROR;
  }
  if (!requestMsg->sethaltedsymbol->has_actiontype)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Set Halted Symbol Request does not contain Action Type\n");
    return ERROR;
  }

  // TraceLog(DEBUG_LEVEL, "Daedalus Set Halted Symbol Request: symbol='%s' actiontype=%d\n",
      // requestMsg->sethaltedsymbol->symbol,
      // requestMsg->sethaltedsymbol->actiontype);
  
  int stockSymbolIndex = GetStockSymbolIndex(requestMsg->sethaltedsymbol->symbol);
  if (stockSymbolIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Set Halted Symbol Request: Could not add symbol '%.8s' to stock list\n", requestMsg->sethaltedsymbol->symbol);
    return ERROR;
  }
  
  int i;
  unsigned int haltTime = hbitime_seconds();
  
  pthread_mutex_lock(&HaltStatusMgmt.mutexLock);
  if (requestMsg->sethaltedsymbol->actiontype == COM__RGM__DAEDALUS__PROTO__ACTION_TYPE__Add)
  {
    strncpy(HaltStatusMgmt.symbol[stockSymbolIndex], requestMsg->sethaltedsymbol->symbol, SYMBOL_LEN);
    for (i = 0; i < TS_MAX_BOOK_CONNECTIONS; i++)
    {
      HaltStatusMgmt.haltTime[stockSymbolIndex][i] = haltTime;
      HaltStatusMgmt.haltStatus[stockSymbolIndex][i] = TRADING_HALTED;
    }
    HaltStatusMgmt.isAutoResume[stockSymbolIndex] = 0;
  }
  else if (requestMsg->sethaltedsymbol->actiontype == COM__RGM__DAEDALUS__PROTO__ACTION_TYPE__Remove)
  {
    strncpy(HaltStatusMgmt.symbol[stockSymbolIndex], requestMsg->sethaltedsymbol->symbol, SYMBOL_LEN);
    for (i = 0; i < TS_MAX_BOOK_CONNECTIONS; i++)
    {
      HaltStatusMgmt.haltTime[stockSymbolIndex][i] = haltTime;
      HaltStatusMgmt.haltStatus[stockSymbolIndex][i] = TRADING_NORMAL;
    }
    HaltStatusMgmt.isAutoResume[stockSymbolIndex] = 1;
  }
  else
  {
    TraceLog(ERROR_LEVEL, "Daedalus Set Halted Symbol Request: Unknown action (%d) for symbol '%.8s'\n",
        requestMsg->sethaltedsymbol->actiontype, requestMsg->sethaltedsymbol->symbol);
    pthread_mutex_unlock(&HaltStatusMgmt.mutexLock);
    return ERROR;
  }
  
  SaveHaltedStockListToFile();
  SendAHaltStockToAllTS(stockSymbolIndex);
  SendAHaltStockToPM(stockSymbolIndex);
  SendAHaltStockToAllDaedalus(stockSymbolIndex);
  SendAHaltStockToAllTT(stockSymbolIndex);
  
  pthread_mutex_unlock(&HaltStatusMgmt.mutexLock);
  
  //Update TT event log to database
  char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
  sprintf(activityDetail, "%d,%d,%.8s", traderToolsInfo.connection[ttIndex].currentState, requestMsg->sethaltedsymbol->actiontype, requestMsg->sethaltedsymbol->symbol);
  ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_HALTED_STOCK_LIST, activityDetail);

  return SUCCESS;
}

int ProcessSetMdcStateFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex)
{
  if (!requestMsg->setmdcstate)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Set MDC State Request is NULL\n");
    return ERROR;
  }
  if (!requestMsg->setmdcstate->hostname)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Set MDC State Request does not contain hostname\n");
    return ERROR;
  }
  if (!requestMsg->setmdcstate->has_mdc)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Set MDC State Request does not contain MDC Type\n");
    return ERROR;
  }
  if (!requestMsg->setmdcstate->has_valid)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Set MDC State Request does not contain MDC State\n");
    return ERROR;
  }

  // TraceLog(DEBUG_LEVEL, "Daedalus Set MDC State Request: hostname='%s' mdc=%d mdcstate=%d\n",
      // requestMsg->setmdcstate->hostname,
      // requestMsg->setmdcstate->mdc,
      // requestMsg->setmdcstate->mdcstate);
      
  int tsIndex = FindTradeServerIndexFromDescription(requestMsg->setmdcstate->hostname);
  if (tsIndex > -1)
  {
    // This request is for TS
    return ProcessSetMdcStateForTSFromDaedalus(requestMsg, ttIndex, tsIndex);
  }
  else
  {
    if (strcmp(PM_SERVER_NAME, requestMsg->setmdcstate->hostname) == 0)
    {
      // This request is for PM
      return ProcessSetMdcStateForPMFromDaedalus(requestMsg, ttIndex);
    }
    else
    {
      // This is not TS nor PM
      TraceLog(ERROR_LEVEL, "Daedalus Set MDC State Request: Hostname is invalid '%s'\n", requestMsg->setmdcstate->hostname);
      return ERROR;
    }
  }
  return SUCCESS;
}

int ProcessSetMdcStateForTSFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex, int tsIndex)
{
  int ecnIndex = DAEDALUS_MDC_INDEX_TO_TS_BOOK_MAPPING[requestMsg->setmdcstate->mdc];
  if (ecnIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Set MDC State Request: invalid TS MDC type (%d)\n", requestMsg->setmdcstate->mdc);
    return ERROR;
  }
  
  // Process action
  switch (requestMsg->setmdcstate->valid)
  {
    case 1:
      TraceLog(DEBUG_LEVEL, "Receive request %s to set MDC State is Valid. This feature is currently not supported!\n",
        tradeServersInfo.config[tsIndex].description);
      break;
    case 0:
      TraceLog(DEBUG_LEVEL, "Receive request %s to set MDC State is Invalid. This feature is currently not supported!\n",
        tradeServersInfo.config[tsIndex].description);
      break;
    default:
      TraceLog(ERROR_LEVEL, "Daedalus Set MDC State Request: invalid MDC State (%d)\n", requestMsg->setmdcstate->valid);
      break;
  }

  return SUCCESS;
}
int ProcessSetMdcStateForPMFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex)
{
  int ecnIndex = DAEDALUS_MDC_INDEX_TO_PM_BOOK_MAPPING[requestMsg->setmdcstate->mdc];
  if (ecnIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Set MDC State Request: invalid PM MDC type (%d)\n", requestMsg->setmdcstate->mdc);
    return ERROR;
  }
  
  // Process action
  switch (requestMsg->setmdcstate->valid)
  {
    case 1:
      TraceLog(DEBUG_LEVEL, "Receive request PM to set MDC State is Valid. This feature is currently not supported!\n");
      break;
    case 0:
      TraceLog(DEBUG_LEVEL, "Receive request PM to set MDC State is Invalid. This feature is currently not supported!\n");
      break;
    default:
      TraceLog(ERROR_LEVEL, "Daedalus Set MDC State Request: invalid MDC State (%d)\n", requestMsg->setmdcstate->valid);
      break;
  }
  return SUCCESS;
}

int ProcessClearBookFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex)
{
  if (!requestMsg->clearbook)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Clear Book Request is NULL\n");
    return ERROR;
  }
  if (!requestMsg->clearbook->hostname)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Clear Book Request does not contain hostname\n");
    return ERROR;
  }
  if (!requestMsg->clearbook->has_mdc)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Clear Book Request does not contain MDC Type\n");
    return ERROR;
  }

  // TraceLog(DEBUG_LEVEL, "Daedalus Clear Book Request: hostname='%s' mdc=%d\n",
      // requestMsg->clearbook->hostname,
      // requestMsg->clearbook->mdc);
  
  int tsIndex = FindTradeServerIndexFromDescription(requestMsg->clearbook->hostname);
  if (tsIndex > -1)
  {
    // This request is for TS
    return ProcessClearBookForTSFromDaedalus(requestMsg, ttIndex, tsIndex);
  }
  else
  {
    if (strcmp(PM_SERVER_NAME, requestMsg->clearbook->hostname) == 0)
    {
      // This request is for PM
      return ProcessClearBookForPMFromDaedalus(requestMsg, ttIndex);
    }
    else
    {
      // This is not TS nor PM
      TraceLog(ERROR_LEVEL, "Daedalus Clear Book Request: Hostname is invalid '%s'\n", requestMsg->clearbook->hostname);
      return ERROR;
    }
  }
}

int ProcessClearBookForTSFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex, int tsIndex)
{
  int ecnIndex = DAEDALUS_MDC_INDEX_TO_TS_BOOK_MAPPING[requestMsg->clearbook->mdc];
  if (ecnIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Clear Book Request: invalid TS MDC type (%d)\n", requestMsg->clearbook->mdc);
    return ERROR;
  }
  
  TraceLog(DEBUG_LEVEL, "Receive request TS(%s) to flush book of %s\n",
    tradeServersInfo.config[tsIndex].description, GetMdcNameFromMdcType(requestMsg->clearbook->mdc));
    
  // Call function to send fulsh all symbol to TS
  SendFlushAllSymbolToTS(tsIndex, ecnIndex);

  return SUCCESS;
}

int ProcessClearBookForPMFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex)
{
  int ecnIndex = DAEDALUS_MDC_INDEX_TO_PM_BOOK_MAPPING[requestMsg->clearbook->mdc];
  if (ecnIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Clear Book Request: invalid PM MDC type (%d)\n", requestMsg->clearbook->mdc);
    return ERROR;
  }
  
  TraceLog(DEBUG_LEVEL, "Receive request PM to flush book of %s\n",
    GetMdcNameFromMdcType(requestMsg->clearbook->mdc));
    
  // Call function to send fulsh all symbol to PM
  SendFlushAllSymbolToPM(ecnIndex);
  
  return SUCCESS;
}

int ProcessSetVolatileSymbolFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex)
{
  if (!requestMsg->setvolatilesymbol)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Set Volatile Symbol Request is NULL\n");
    return ERROR;
  }
  if (!requestMsg->setvolatilesymbol->has_actiontype)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Set Volatile Symbol Request does not contain Action Type\n");
    return ERROR;
  }
  if (!requestMsg->setvolatilesymbol->symbol)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Set Volatile Symbol Request does not contain symbol\n");
    return ERROR;
  }

  // TraceLog(DEBUG_LEVEL, "Daedalus Set Volatile Symbol Request: symbol='%s' actiontype=%d\n",
      // requestMsg->setvolatilesymbol->symbol,
      // requestMsg->setvolatilesymbol->actiontype);
  
  char list[MAX_STOCK_SYMBOL * SYMBOL_LEN];
  int count = 0;
  int changed = 1;
  
  pthread_mutex_lock(&volatileMgmt.mutexLock);
  if (LoadVolatileSymbolsFromFileToList(list, &count) == ERROR)
  {
    pthread_mutex_unlock(&volatileMgmt.mutexLock);
    return ERROR;
  }
  
  char symbol[SYMBOL_LEN];
  strncpy(symbol, requestMsg->setvolatilesymbol->symbol, SYMBOL_LEN);
  int symbolIndex;
  
  if (requestMsg->setvolatilesymbol->actiontype == COM__RGM__DAEDALUS__PROTO__ACTION_TYPE__Add)
  {
    symbolIndex = GetStockSymbolIndex(symbol);
    if (symbolIndex < 0)
    {
      TraceLog(ERROR_LEVEL, "(Daedalus requests to add a new volatile symbol): invalid symbol '%.8s'\n", symbol);
      pthread_mutex_unlock(&volatileMgmt.mutexLock);
      return ERROR;
    }
    
    int index;
    for (index = 0; index < count; index++)
    {
      if (strncmp(&list[index * SYMBOL_LEN], symbol, SYMBOL_LEN) == 0)
      {
        // Symbol is already in the list
        changed = 0;
        break;
      }
    }
    
    if (changed == 1)
    {
      strncpy(&list[count * SYMBOL_LEN], symbol, SYMBOL_LEN);
      count++;
      
      SaveVolatileSymbolListToFile(list, count);
    }
  }
  else if (requestMsg->setvolatilesymbol->actiontype == COM__RGM__DAEDALUS__PROTO__ACTION_TYPE__Remove)
  {
    changed = 0;
    
    symbolIndex = GetStockSymbolIndex(symbol);
    if (symbolIndex < 0)
    {
      TraceLog(ERROR_LEVEL, "(Daedalus requests to remove a volatile symbol): invalid symbol '%.8s'\n", symbol);
      pthread_mutex_unlock(&volatileMgmt.mutexLock);
      return ERROR;
    }
    
    int index;
    for (index = 0; index < count; index++)
    {
      if (strncmp(&list[index * SYMBOL_LEN], symbol, SYMBOL_LEN) == 0)
      {
        //found symbol to remove
        list[index * SYMBOL_LEN] = 0; //set to 0, it will not be saved to file
        changed = 1;
      }
    }
    
    if (changed == 1)
    {
      SaveVolatileSymbolListToFile(list, count);
    }
    
    // Reset flag profit loss
    if (ProfitLossPerSymbol[symbolIndex].listID == VOLATILE_LIST_ID)
    {
      int tsIndex;
      for (tsIndex = 0; tsIndex < MAX_TRADE_SERVER_CONNECTIONS; tsIndex++)
      {
        if (fgt(ProfitLossPerSymbol[symbolIndex].tsLoss[tsIndex], 0.00))
        {
          SaveLossBySymbol(symbol, tsIndex, ProfitLossPerSymbol[symbolIndex].tsLoss[tsIndex]);
        }
      }
      
      memset(&ProfitLossPerSymbol[symbolIndex], 0, sizeof(t_ProfitLossPerSymbol));
      TraceLog(DEBUG_LEVEL, "Remove %s from volaitle list, reset loss counter for it\n", symbol);
    }
  }
  else
  {
    TraceLog(ERROR_LEVEL, "Invalid set volatile symbol from Daedalus, symbol = %.8s, actiontype = %d\n", symbol, requestMsg->setvolatilesymbol->actiontype);
    pthread_mutex_unlock(&volatileMgmt.mutexLock);
    return ERROR;
  }
  
  pthread_mutex_unlock(&volatileMgmt.mutexLock);
  
  if (changed == 1)
  {
    SendVolatileSymbolListToAllDaedalus();
    SendVolatileSymbolListToAllTT();
  }
  
  return SUCCESS;
}

int ProcessSetVolatileSymbolBySessionFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, t_ConnectionStatus *connection)
{
  if (!requestMsg->setvolatilesymbolbysession)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Set Volatile Symbol By Session Request is NULL\n");
    return ERROR;
  }
  if (!requestMsg->setvolatilesymbolbysession->has_actiontype)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Set Volatile Symbol By Session Request does not contain Action Type\n");
    return ERROR;
  }
  if (!requestMsg->setvolatilesymbolbysession->symbol)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Set Volatile Symbol By Session Request does not contain symbol\n");
    return ERROR;
  }
  if (!requestMsg->setvolatilesymbolbysession->has_session)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Set Volatile Symbol By Session Request does not contain Session\n");
    return ERROR;
  }

  // TraceLog(DEBUG_LEVEL, "Daedalus Set Volatile Symbol By Session Request: symbol='%s' actiontype=%d\n",
      // requestMsg->setvolatilesymbolbysession->symbol,
      // requestMsg->setvolatilesymbolbysession->actiontype,
      // requestMsg->setvolatilesymbolbysession->session);
  
  int actionType = requestMsg->setvolatilesymbolbysession->actiontype;
  int session = requestMsg->setvolatilesymbolbysession->session;
  
  if (actionType == COM__RGM__DAEDALUS__PROTO__ACTION_TYPE__Replay)
  {
    SendVolatileSectionsToDaedalus(connection);
    return SUCCESS;
  }
  
  char symbol[SYMBOL_LEN];
  strncpy(symbol, requestMsg->setvolatilesymbolbysession->symbol, SYMBOL_LEN);
  int symbolIndex;
  symbolIndex = GetStockSymbolIndex(symbol);
  if (symbolIndex < 0)
  {
    TraceLog(ERROR_LEVEL, "(Daedalus requests to Add Or Remove a volatile symbol): invalid symbol %.8s\n", symbol);
    return ERROR;
  }
  
  char preList[MAX_STOCK_SYMBOL * SYMBOL_LEN];
  char postList[MAX_STOCK_SYMBOL * SYMBOL_LEN];
  char intradayList[MAX_STOCK_SYMBOL * SYMBOL_LEN];
  int countPre = 0, countPost = 0, countIntraday = 0;
  int index, changed = 1;
  
  // Load file to buffer --> update buffer --> save file
  pthread_mutex_lock(&volatileMgmt.mutexLock);
  if (LoadVolatileSectionsFromFileToLists(preList, &countPre, postList, &countPost, intradayList, &countIntraday) == ERROR)
  {
    pthread_mutex_unlock(&volatileMgmt.mutexLock);
    return ERROR;
  }
  
  if (actionType == COM__RGM__DAEDALUS__PROTO__ACTION_TYPE__Add)
  {
    switch (session)
    {
      case COM__RGM__DAEDALUS__PROTO__VOLATILE_SESSION__PRE:    // [PRE]
        for (index = 0; index < countPre; index++)
        {
          if (strncmp(&preList[index * SYMBOL_LEN], symbol, SYMBOL_LEN) == 0)
          {
            // Symbol is already in the list
            changed = 0;
            break;
          }
        }
        
        if (changed == 1)
        {
          strncpy(&preList[countPre * SYMBOL_LEN], symbol, SYMBOL_LEN);
          countPre++;
        }
        break;
      case COM__RGM__DAEDALUS__PROTO__VOLATILE_SESSION__POST:   // [POST]
        for (index = 0; index < countPost; index++)
        {
          if (strncmp(&postList[index * SYMBOL_LEN], symbol, SYMBOL_LEN) == 0)
          {
            // Symbol is already in the list
            changed = 0;
            break;
          }
        }
        
        if (changed == 1)
        {
          strncpy(&postList[countPost * SYMBOL_LEN], symbol, SYMBOL_LEN);
          countPost++;
        }
        break;
      case COM__RGM__DAEDALUS__PROTO__VOLATILE_SESSION__INTRADAY: // [INTRADAY]
        for (index = 0; index < countIntraday; index++)
        {
          if (strncmp(&intradayList[index * SYMBOL_LEN], symbol, SYMBOL_LEN) == 0)
          {
            // Symbol is already in the list
            changed = 0;
            break;
          }
        }
        
        if (changed == 1)
        {
          strncpy(&intradayList[countIntraday * SYMBOL_LEN], symbol, SYMBOL_LEN);
          countIntraday++;
        }
        break;
    }
  }
  else if (actionType == COM__RGM__DAEDALUS__PROTO__ACTION_TYPE__Remove)
  {
    changed = 0;
    switch (session)
    {
      case COM__RGM__DAEDALUS__PROTO__VOLATILE_SESSION__PRE:    // [PRE]
        for (index = 0; index < countPre; index++)
        {
          if (strncmp(&preList[index * SYMBOL_LEN], symbol, SYMBOL_LEN) == 0)
          {
            //found symbol to remove
            preList[index * SYMBOL_LEN] = 0;  //set to 0, it will not be saved to file
            changed = 1;
          }
        }
        break;
      case COM__RGM__DAEDALUS__PROTO__VOLATILE_SESSION__POST:   // [POST]
        for (index = 0; index < countPost; index++)
        {
          if (strncmp(&postList[index * SYMBOL_LEN], symbol, SYMBOL_LEN) == 0)
          {
            //found symbol to remove
            postList[index * SYMBOL_LEN] = 0; //set to 0, it will not be saved to file
            changed = 1;
          }
        }
        break;
      case COM__RGM__DAEDALUS__PROTO__VOLATILE_SESSION__INTRADAY: // [INTRADAY]
        for (index = 0; index < countIntraday; index++)
        {
          if (strncmp(&intradayList[index * SYMBOL_LEN], symbol, SYMBOL_LEN) == 0)
          {
            //found symbol to remove
            intradayList[index * SYMBOL_LEN] = 0; //set to 0, it will not be saved to file
            changed = 1;
          }
        }
        break;
    }
  }
  else
  {
    TraceLog(ERROR_LEVEL, "Invalid set volatile symbol from Daedalus, symbol='%.8s', actionType=%d\n", symbol, actionType);
    pthread_mutex_unlock(&volatileMgmt.mutexLock);
    return ERROR;
  }
  
  if (changed == 1)
  {
    SaveVolatileSectionsToFile(preList, countPre, postList, countPost, intradayList, countIntraday);
  }
  pthread_mutex_unlock(&volatileMgmt.mutexLock);
  
  if (changed == 1)
  {
    SendVolatileSectionsToAllDaedalus();
    SendVolatileSectionsToAllTT();
  }
  
  return SUCCESS;
}

int ProcessSetVolatileSymbolByToggleFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex)
{
  if (!requestMsg->setvolatilesymbolbytoggle)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Set Volatile Symbol By Toogle Request is NULL\n");
    return ERROR;
  }
  if (!requestMsg->setvolatilesymbolbytoggle->has_actiontype)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Set Volatile Symbol By Toogle Request does not contain Action Type\n");
    return ERROR;
  }
  if (!requestMsg->setvolatilesymbolbytoggle->has_ontime)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Set Volatile Symbol By Toogle Request does not contain On Time\n");
    return ERROR;
  }
  if (!requestMsg->setvolatilesymbolbytoggle->has_offtime)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Set Volatile Symbol By Toogle Request does not contain Off Time\n");
    return ERROR;
  }

  // TraceLog(DEBUG_LEVEL, "Daedalus Set Volatile Symbol By Toogle Request: actiontype=%d ontime=%ld offtime=%ld\n",
      // requestMsg->setvolatilesymbolbytoggle->actiontype,
      // requestMsg->setvolatilesymbolbytoggle->ontime,
      // requestMsg->setvolatilesymbolbytoggle->offtime);
  
  int actionType = requestMsg->setvolatilesymbolbytoggle->actiontype;
  
  //  On time:  4 bytes (hr*3600 + min*60 + sec)
  //  Off time:   4 bytes (hr*3600 + min*60 + sec)
  
  if (actionType == COM__RGM__DAEDALUS__PROTO__ACTION_TYPE__Add)
  {
    pthread_mutex_lock(&volatileMgmt.mutexLock);
    int numItem = volatileMgmt.numAllVolatileToggle;
    if (numItem < MAX_VOLATILE_TOGGLE)
    {
      if (numItem < 0)
      {
        numItem = 0;
      }
      
      //Update on and off time
      volatileMgmt.allVolatileOnTime[numItem] = requestMsg->setvolatilesymbolbytoggle->ontime;
      volatileMgmt.allVolatileOffTime[numItem] = requestMsg->setvolatilesymbolbytoggle->offtime;
      
      //Update number of items
      numItem++;
      volatileMgmt.numAllVolatileToggle = numItem;
    
      SaveAllVolatileToggleSchedulesToFile();
      
/*#ifndef RELEASE_MODE
      // TraceLog(DEBUG_LEVEL, "--->Add volatile schedule result:\n");
      int k;
      for (k = 0; k < volatileMgmt.numAllVolatileToggle; k++)
      {
        // TraceLog(DEBUG_LEVEL, "--->item %d: on=%d off=%d\n", k, volatileMgmt.allVolatileOnTime[k], volatileMgmt.allVolatileOffTime[k]);
      }
#endif*/
    }
    else
    {
      TraceLog(ERROR_LEVEL, "Set Volatile Schedule Request: max number of schedule is reach (%d)\n", MAX_VOLATILE_TOGGLE);
      pthread_mutex_unlock(&volatileMgmt.mutexLock);
      return ERROR;
    }
    pthread_mutex_unlock(&volatileMgmt.mutexLock);
  }
  else if (actionType == COM__RGM__DAEDALUS__PROTO__ACTION_TYPE__Remove)
  {
    pthread_mutex_lock(&volatileMgmt.mutexLock);
    int numItem = volatileMgmt.numAllVolatileToggle;
    
    int i, j;

    // TraceLog(DEBUG_LEVEL, "--->1. numItem=%d\n", numItem);
    // Find item and remove it
    for (i = 0; i < numItem; i++)
    {
      if (volatileMgmt.allVolatileOnTime[i] == requestMsg->setvolatilesymbolbytoggle->ontime &&
        volatileMgmt.allVolatileOffTime[i] == requestMsg->setvolatilesymbolbytoggle->offtime)
      {
        // TraceLog(DEBUG_LEVEL, "--->i=%d on=%d off=%d\n", i, volatileMgmt.allVolatileOnTime[i], volatileMgmt.allVolatileOffTime[i]);
        // shift item from right to left
        for (j = i + 1; j < numItem; j++)
        {
          volatileMgmt.allVolatileOnTime[i] = volatileMgmt.allVolatileOnTime[j];
          volatileMgmt.allVolatileOffTime[i] = volatileMgmt.allVolatileOffTime[j];
          // TraceLog(DEBUG_LEVEL, "--->shifting j-->i: j=%d on=%d off=%d\n", j, volatileMgmt.allVolatileOnTime[i], volatileMgmt.allVolatileOffTime[i]);
        }
        
        break;
      }
    }
    
    // TraceLog(DEBUG_LEVEL, "--->2. numItem=%d\n", numItem);
    numItem -= 1;
    if (numItem < 0)
    {
      numItem = 0;
    }
    volatileMgmt.numAllVolatileToggle = numItem;
    
/*#ifndef RELEASE_MODE
    // TraceLog(DEBUG_LEVEL, "--->Remove volatile schedule result:\n");
    int k;
    for (k = 0; k < volatileMgmt.numAllVolatileToggle; k++)
    {
      // TraceLog(DEBUG_LEVEL, "--->item %d: on=%d off=%d\n", k, volatileMgmt.allVolatileOnTime[k], volatileMgmt.allVolatileOffTime[k]);
    }
#endif*/
    
    SaveAllVolatileToggleSchedulesToFile();
    pthread_mutex_unlock(&volatileMgmt.mutexLock);
    //----------------------------------------------------------------------
    /*pthread_mutex_lock(&volatileMgmt.mutexLock);
    int numItem = volatileMgmt.numAllVolatileToggle;
    
    int onTime[MAX_VOLATILE_TOGGLE];
    int offTime[MAX_VOLATILE_TOGGLE];
    memcpy(onTime, volatileMgmt.allVolatileOnTime, sizeof(onTime));
    memcpy(offTime, volatileMgmt.allVolatileOffTime, sizeof(offTime));
    
    //Remove on temp buffer and copy if back to main buffer
    int i;
    for (i = 0; i < numItem; i++)
    {
      if (onTime[i] == requestMsg->setvolatilesymbolbytoggle->ontime &&
        offTime[i] == requestMsg->setvolatilesymbolbytoggle->offtime)
      {
        onTime[i] = -1; // mark for removal
        offTime[i] = 1;
      }
    }
    
    //Copy buffer back to main
    volatileMgmt.numAllVolatileToggle = 0;
    for (i = 0; i < numItem; i++)
    {
      if (onTime[i] > -1)
      {
        volatileMgmt.allVolatileOnTime[volatileMgmt.numAllVolatileToggle] = onTime[i];
        volatileMgmt.allVolatileOffTime[volatileMgmt.numAllVolatileToggle] = offTime[i];
        volatileMgmt.numAllVolatileToggle++;
      }
    }
    
    SaveAllVolatileToggleSchedulesToFile();
    pthread_mutex_unlock(&volatileMgmt.mutexLock);*/
  }
  else
  {
    TraceLog(ERROR_LEVEL, "Set Volatile Schedule Request: invalid action type (%d)\n", actionType);
    return ERROR;
  }
  
  SendAllVolatileToggleScheduleToAllDaedalus();
  SendAllVolatileToggleScheduleToAllTT();
  return SUCCESS;
}

int ProcessSetAlertConfigFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex)
{
  if (!requestMsg->setalertconfig)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Set Alert Config Request is NULL\n");
    return ERROR;
  }
  if (!requestMsg->setalertconfig->has_pmexitedpositionthreshold)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Set Alert Config Request does not contain PM Exited Possition Threshold\n");
    return ERROR;
  }
  if (!requestMsg->setalertconfig->has_consecutivelossthreshold)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Set Alert Config Request does not contain Consecutive Loss Threshold\n");
    return ERROR;
  }
  if (!requestMsg->setalertconfig->has_stucksthreshold)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Set Alert Config Request does not contain Stuck Threshold\n");
    return ERROR;
  }
  if (!requestMsg->setalertconfig->has_singleservertradesthreshold)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Set Alert Config Request does not contain Single Server Trade Threshold\n");
    return ERROR;
  }
  if (!requestMsg->setalertconfig->has_gtramountthreshold)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Set Alert Config Request does not contain GTR Amount Threshold\n");
    return ERROR;
  }
  if (!requestMsg->setalertconfig->has_gtrsecondthreshold)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Set Alert Config Request does not contain GTR Second Threshold\n");
    return ERROR;
  }
  if (!requestMsg->setalertconfig->has_incompletetradethreshold)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Set Alert Config Request does not contain Incompleted Trade Threshold\n");
    return ERROR;
  }
  if (!requestMsg->setalertconfig->has_incompletetraderepetitionthreshold)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Set Alert Config Request does not contain Repetition Incompleted Trade Threshold\n");
    return ERROR;
  }
  if (!requestMsg->setalertconfig->has_positionwithoutorderthreshold)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Set Alert Config Request does not contain Position Without Order Threshold\n");
    return ERROR;
  }
  if (!requestMsg->setalertconfig->has_pmorderratethreshold)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Set Alert Config Request does not contain PM Order Rate Threshold\n");
    return ERROR;
  }
  if (!requestMsg->setalertconfig->has_buyingpoweramountthreshold)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Set Alert Config Request does not contain Buying Power Amount Threshold\n");
    return ERROR;
  }
  if (!requestMsg->setalertconfig->has_buyingpowersecondsthreshold)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Set Alert Config Request does not contain Buying Power Seconds Threshold\n");
    return ERROR;
  }
  // if (!requestMsg->setalertconfig->n_notradetime)
  // {
    // TraceLog(ERROR_LEVEL, "Daedalus Set Alert Config Request does not contain No Trade Time Item\n");
    // return ERROR;
  // }
  
  t_AlertConf tempAlertConf;
  memcpy(&tempAlertConf, &AlertConf, sizeof(t_AlertConf));

  tempAlertConf.pmExitedPositionThreshhold     = requestMsg->setalertconfig->pmexitedpositionthreshold;
  tempAlertConf.consecutiveLossThreshhold      = requestMsg->setalertconfig->consecutivelossthreshold;
  tempAlertConf.stucksThreshhold               = requestMsg->setalertconfig->stucksthreshold;
  tempAlertConf.singleServerTradesThreshhold   = requestMsg->setalertconfig->singleservertradesthreshold;
  tempAlertConf.gtrAmountThreshhold            = requestMsg->setalertconfig->gtramountthreshold * TEN_DOWN_TO_FOUR;
  tempAlertConf.gtrSecondThreshhold            = requestMsg->setalertconfig->gtrsecondthreshold;
  tempAlertConf.incompleteTradeThreshhold      = requestMsg->setalertconfig->incompletetradethreshold;
  tempAlertConf.repetitionTimeThreshhold       = requestMsg->setalertconfig->incompletetraderepetitionthreshold;
  tempAlertConf.positionWithoutOrderThreshhold = requestMsg->setalertconfig->positionwithoutorderthreshold;
  tempAlertConf.pmOrderRateThreshhold          = requestMsg->setalertconfig->pmorderratethreshold;
  tempAlertConf.buyingPowerAmountThreshhold    = requestMsg->setalertconfig->buyingpoweramountthreshold * TEN_DOWN_TO_FOUR;
  tempAlertConf.buyingPowerSecondsThreshhold   = requestMsg->setalertconfig->buyingpowersecondsthreshold;
  tempAlertConf.noTradeAlertMgmt.count         = requestMsg->setalertconfig->n_notradetime;
  
  int ret = SUCCESS;
  if((tempAlertConf.noTradeAlertMgmt.count <= 0) ||
     (tempAlertConf.noTradeAlertMgmt.count > MAX_NO_TRADE_ALERT_MILESTONE))
  {
    TraceLog(ERROR_LEVEL, "Invalid alert config count = %d\n", tempAlertConf.noTradeAlertMgmt.count);
    ret = ERROR;
  }
  
  if (ret == SUCCESS)
  {
    int i;
    for(i = 0; i < tempAlertConf.noTradeAlertMgmt.count; i++)
    {
      if (!requestMsg->setalertconfig->notradetime[i])
      {
        TraceLog(ERROR_LEVEL, "Daedalus Set Alert Config Request: No Trade Time Item %d is NULL\n", i);
        ret = ERROR;
        break;
      }
      if (!requestMsg->setalertconfig->notradetime[i]->has_period)
      {
        TraceLog(ERROR_LEVEL, "Daedalus Set Alert Config Request: No Trade Time Item %d does not have startInSecond\n", i);
        ret = ERROR;
        break;
      }
      tempAlertConf.noTradeAlertMgmt.itemList[i].startInSecond = requestMsg->setalertconfig->notradetime[i]->period;
      
      if (!requestMsg->setalertconfig->notradetime[i]->has_value)
      {
        TraceLog(ERROR_LEVEL, "Daedalus Set Alert Config Request: No Trade Time Item %d does not have durationInMinute\n", i);
        ret = ERROR;
        break;
      }
      tempAlertConf.noTradeAlertMgmt.itemList[i].durationInMinute = requestMsg->setalertconfig->notradetime[i]->value;

      if((i >= 1) &&
         (tempAlertConf.noTradeAlertMgmt.itemList[i].startInSecond <= tempAlertConf.noTradeAlertMgmt.itemList[i - 1].startInSecond))
      {
        TraceLog(ERROR_LEVEL, "No trade alert time must be ascendant\n");
        ret = ERROR;
        break;
      }
    }
  }
  
  if (ret == SUCCESS)
  {
    memcpy(&AlertConf, &tempAlertConf, sizeof(t_AlertConf));
  }
  else
  {
    TraceLog(DEBUG_LEVEL, "Do not update Alert Config because it is invalid\n");
  }
  
  // Update Trade Time Alert Config
  UpdateTradeTimeAlertConfig();

  // Save data to the file
  SaveAlertConf();

  // Send alert config to all TT
  SendAlertConfigToAllDaedalus();
  SendAlertConfigToAllTT();
  return SUCCESS;
}

int ProcessChangeTraderStateFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex)
{
  if (!requestMsg->setoperatorstate)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Set Operator State Request is NULL\n");
    return ERROR;
  }
  if (!requestMsg->setoperatorstate->operatorid)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Set Operator State Request does not contain Trader ID\n");
    return ERROR;
  }
  if (!requestMsg->setoperatorstate->has_operatorstatus)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Set Operator State Request does not contain New State\n");
    return ERROR;
  }

  // TraceLog(DEBUG_LEVEL, "Daedalus Set Operator State Request: trader='%s' new_state=%d (act=1, as=2, mor=3)\n",
      // requestMsg->setoperatorstate->operatorid,
      // requestMsg->setoperatorstate->operatorstatus);
      
  char userName[MAX_LINE_LEN] = "\0";
  strncpy(userName, requestMsg->setoperatorstate->operatorid, MAX_LINE_LEN);
  
  int status;
  switch (requestMsg->setoperatorstate->operatorstatus)
  {
    case COM__RGM__DAEDALUS__PROTO__OPERATOR_TYPE__ActiveOperator:
      status = ACTIVE;
      break;
    case COM__RGM__DAEDALUS__PROTO__OPERATOR_TYPE__ActiveAssistOperator:
      status = ACTIVE_ASSISTANT;
      break;
    case COM__RGM__DAEDALUS__PROTO__OPERATOR_TYPE__MonitoringOperator:
      status = MONITORING;
      break;
    default:
      TraceLog(ERROR_LEVEL, "Daedalus Set Operator State Request: Invalid new trader state: %d\n", requestMsg->setoperatorstate->operatorstatus);
      return ERROR;
      break;
  }

  TraceLog(DEBUG_LEVEL, "[%s]Change trader state from %d to %d (Active:1, Active Assist:2, Mornitoring:3)\n",
    traderToolsInfo.connection[ttIndex].userName, traderToolsInfo.connection[ttIndex].currentState, status);

  char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
  sprintf(activityDetail, "%d,%d", traderToolsInfo.connection[ttIndex].currentState, status);
  ProcessUpdateTTEventLogToDatabase(userName, ACTIVITY_TYPE_SWITCH_ROLE, activityDetail);
  
  //Update current state of trader
  traderToolsInfo.connection[ttIndex].currentState = status;
  // TraceLog(DEBUG_LEVEL, "--->currentState of %s is %d\n",
    // traderToolsInfo.connection[ttIndex].userName, traderToolsInfo.connection[ttIndex].currentState);
  
  //Update trader status to monitoring
  ProcessUpdateTraderStatusToDatabase(userName, status);

  return SUCCESS;
}

int ProcessSetExchangeConfigFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex)
{
  if (!requestMsg->setexchangeconfig)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Set Exchange Config Request is NULL\n");
    return ERROR;
  }
  if (!requestMsg->setexchangeconfig->hostname)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Set Exchange Config Request does not contain hostname\n");
    return ERROR;
  }
  if (!requestMsg->setexchangeconfig->n_exchanges)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Set Exchange Config Request does have Exchange List\n");
    return ERROR;
  }

  // TraceLog(DEBUG_LEVEL, "Daedalus Set Exchange Config Request: hostname='%s' n_exchanges=%d\n",
      // requestMsg->setexchangeconfig->hostname,
      // requestMsg->setexchangeconfig->n_exchanges);
  
  int tsIndex = FindTradeServerIndexFromDescription(requestMsg->setexchangeconfig->hostname);
  if (tsIndex > -1)
  {
    // This request is for TS
    return ProcessSetExchangeConfigForTSFromDaedalus(requestMsg, ttIndex, tsIndex);
  }
  else
  {
    if (strcmp(PM_SERVER_NAME, requestMsg->setexchangeconfig->hostname) == 0)
    {
      // This request is for PM
      return ProcessSetExchangeConfigForPMFromDaedalus(requestMsg, ttIndex);
    }
    else
    {
      // This is not TS nor PM
      TraceLog(ERROR_LEVEL, "Daedalus Set Exchange Config Request: Hostname is invalid '%s'\n", requestMsg->setexchangeconfig->hostname);
      return ERROR;
    }
  }
  return SUCCESS;
}

int ProcessSetExchangeConfigForTSFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex, int tsIndex)
{
  int maxItem = requestMsg->setexchangeconfig->n_exchanges;
  int item, ret = SUCCESS;
  t_ExchangeInfo tmpExchangeInfo[MAX_EXCHANGE];
  memcpy(tmpExchangeInfo, &ExchangeStatusList[tsIndex], sizeof(tmpExchangeInfo));
  
  for (item = 0; item < maxItem; item++)
  {
    if (requestMsg->setexchangeconfig->exchanges[item] == NULL ||
      requestMsg->setexchangeconfig->exchanges[item]->exchangecode == NULL ||
      requestMsg->setexchangeconfig->exchanges[item]->has_cqsenabled == 0 ||
      requestMsg->setexchangeconfig->exchanges[item]->has_uqdfenabled == 0)
      // requestMsg->setexchangeconfig->exchanges[item]->has_oectoplaceorder == 0)
    {
      TraceLog(ERROR_LEVEL, "Daedalus Set TS Exchange Config Request: Invalid fields of Exchange Entry Item: %d\n", item);
      ret = ERROR;
      break;
    }

    char code = requestMsg->setexchangeconfig->exchanges[item]->exchangecode[0];
    int j;
    for (j = 0; j < MAX_EXCHANGE; j++)
    {
      if (code == tmpExchangeInfo[j].exchangeId)
      {
        // tmpExchangeInfo[j].status[CQS_SOURCE].visible  = requestMsg->setexchangeconfig->exchanges[item]->cqsvisible;
        tmpExchangeInfo[j].status[CQS_SOURCE].enable   = requestMsg->setexchangeconfig->exchanges[item]->cqsenabled;
        // tmpExchangeInfo[j].status[UQDF_SOURCE].visible = requestMsg->setexchangeconfig->exchanges[item]->uqdfvisible;
        tmpExchangeInfo[j].status[UQDF_SOURCE].enable  = requestMsg->setexchangeconfig->exchanges[item]->uqdfenabled;
        break;
      }
    }
  }
  
  if (ret == ERROR)
  {
    return ERROR;
  }
  
  // Build temp buffer msg to TS
  const int numFixedItems = 6;
  const int buffLen = numFixedItems * MAX_EXCHANGE;
  unsigned char tsBuffer[buffLen];
  int msgIndex = 0;
  for (item = 0; item < MAX_EXCHANGE; item++)
  {
    // Exchange Index
    tsBuffer[msgIndex] = tmpExchangeInfo[item].index;
    msgIndex++;
    
    // Exchange Code
    tsBuffer[msgIndex] = tmpExchangeInfo[item].exchangeId;
    msgIndex++;
    
    // CQS Visible
    tsBuffer[msgIndex] = tmpExchangeInfo[item].status[CQS_SOURCE].visible;
    msgIndex++;
    
    // CQS Enabled
    tsBuffer[msgIndex] = tmpExchangeInfo[item].status[CQS_SOURCE].enable;
    msgIndex++;
    
    // UQDF Visible
    tsBuffer[msgIndex] = tmpExchangeInfo[item].status[UQDF_SOURCE].visible;
    msgIndex++;
    
    // UQDF Enabled
    tsBuffer[msgIndex] = tmpExchangeInfo[item].status[UQDF_SOURCE].enable;
    msgIndex++;
  }
  
  SendSetExchangesConfToTS(tsIndex, buffLen, tsBuffer);
  
  return SUCCESS;
  
}

int ProcessSetExchangeConfigForPMFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex)
{
  int maxItem = requestMsg->setexchangeconfig->n_exchanges;
  int item, ret = SUCCESS;
  t_PMExchangeInfo tmpPmExchangeInfo[MAX_EXCHANGE];
  memcpy(tmpPmExchangeInfo, PMExchangeStatusList, sizeof(tmpPmExchangeInfo));
  
  for (item = 0; item < maxItem; item++)
  {
    if (requestMsg->setexchangeconfig->exchanges[item] == NULL ||
      requestMsg->setexchangeconfig->exchanges[item]->exchangecode == NULL ||
      requestMsg->setexchangeconfig->exchanges[item]->has_cqsenabled == 0 ||
      requestMsg->setexchangeconfig->exchanges[item]->has_uqdfenabled == 0 ||
      requestMsg->setexchangeconfig->exchanges[item]->has_oectoplaceorder == 0)
    {
      TraceLog(ERROR_LEVEL, "Daedalus Set PM Exchange Config Request: Invalid fields of Exchange Entry Item: %d\n", item);
      ret = ERROR;
      break;
    }

    char code = requestMsg->setexchangeconfig->exchanges[item]->exchangecode[0];
    int j;
    for (j = 0; j < MAX_EXCHANGE; j++)
    {
      if (code == tmpPmExchangeInfo[j].exchangeId)
      {
        // tmpPmExchangeInfo[j].status[CQS_SOURCE].visible  = requestMsg->setexchangeconfig->exchanges[item]->cqsvisible;
        tmpPmExchangeInfo[j].status[CQS_SOURCE].enable   = requestMsg->setexchangeconfig->exchanges[item]->cqsenabled;
        // tmpPmExchangeInfo[j].status[UQDF_SOURCE].visible = requestMsg->setexchangeconfig->exchanges[item]->uqdfvisible;
        tmpPmExchangeInfo[j].status[UQDF_SOURCE].enable  = requestMsg->setexchangeconfig->exchanges[item]->uqdfenabled;
        if (requestMsg->setexchangeconfig->exchanges[item]->oectoplaceorder == COM__RGM__DAEDALUS__PROTO__OEC__RASH_OEC)
        {
          strcpy(tmpPmExchangeInfo[j].ECNToPlace, "RASH");
        }
        else if (requestMsg->setexchangeconfig->exchanges[item]->oectoplaceorder == COM__RGM__DAEDALUS__PROTO__OEC__ARCA_DIRECT_OEC)
        {
          strcpy(tmpPmExchangeInfo[j].ECNToPlace, "ARCA_DIRECT");
        }
        break;
      }
    }
  }
  
  if (ret == ERROR)
  {
    return ERROR;
  }
  
  // Copy temp buffer back to main buffer
  memcpy(PMExchangeStatusList, tmpPmExchangeInfo, sizeof(tmpPmExchangeInfo));
  // TraceLog(DEBUG_LEVEL, "--->Sending PM exchange to PM and TT\n");
  
  SetPMExchangeConfToPM();

  SendPMExchangesConfToAllDaedalus();
  SendPMExchangesConfToAllTT();

  return SUCCESS;
}

int ProcessSendManualOrderFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex)
{
  // Check message validity
  if (!requestMsg->manualorder)
  {
    TraceLog(ERROR_LEVEL, "Manual Order Request message is NULL\n");
    return ERROR;
  }
  if (!requestMsg->manualorder->hostname)
  {
    TraceLog(ERROR_LEVEL, "Manual Order Request does not contain hostname\n");
    return ERROR;
  }
  if (!requestMsg->manualorder->has_oec)
  {
    TraceLog(ERROR_LEVEL, "Manual Order Request does not contain specific MDC\n");
    return ERROR;
  }
  if (!requestMsg->manualorder->has_side)
  {
    TraceLog(ERROR_LEVEL, "Manual Order Request does not contain Side\n");
    return ERROR;
  }
  if (!requestMsg->manualorder->has_size)
  {
    TraceLog(ERROR_LEVEL, "Manual Order Request does not contain Volume\n");
    return ERROR;
  }
  if (!requestMsg->manualorder->has_price)
  {
    TraceLog(ERROR_LEVEL, "Manual Order Request does not contain Price\n");
    return ERROR;
  }
  if (!requestMsg->manualorder->has_hidden)
  {
    TraceLog(ERROR_LEVEL, "Manual Order Request does not contain Hidden\n");
    return ERROR;
  }
  if (!requestMsg->manualorder->has_ttl)
  {
    TraceLog(ERROR_LEVEL, "Manual Order Request does not contain Time To Live\n");
    return ERROR;
  }
  if (!requestMsg->manualorder->symbol)
  {
    TraceLog(ERROR_LEVEL, "Manual Order Request does not contain Symbol\n");
    return ERROR;
  }
  if (!requestMsg->manualorder->account)
  {
    TraceLog(ERROR_LEVEL, "Manual Order Request does not contain Account\n");
    return ERROR;
  }
  if (!requestMsg->manualorder->has_sendid)
  {
    TraceLog(ERROR_LEVEL, "Manual Order Request does not contain SendID\n");
    return ERROR;
  }
  if (!requestMsg->manualorder->has_force)
  {
    TraceLog(ERROR_LEVEL, "Manual Order Request does not contain Force\n");
    return ERROR;
  }
  
  // TraceLog(DEBUG_LEVEL, "--->Manual Order Request: account  = '%s'\n", requestMsg->manualorder->account);
  // TraceLog(DEBUG_LEVEL, "--->Manual Order Request: side     = %d(-1:ASK, 1:BID)\n", requestMsg->manualorder->side);
  // TraceLog(DEBUG_LEVEL, "--->Manual Order Request: force    = %d\n", requestMsg->manualorder->force);
  // TraceLog(DEBUG_LEVEL, "--->Manual Order Request: orderId  = %s'\n", requestMsg->manualorder->orderid);
  // TraceLog(DEBUG_LEVEL, "--->Manual Order Request: maxFloor = %d\n", requestMsg->manualorder->maxfloor);
  // TraceLog(DEBUG_LEVEL, "--->Manual Order Request: hidden   = %d\n", requestMsg->manualorder->hidden);
  
  // Get required fields
  if (strcmp(requestMsg->manualorder->hostname, PM_SERVER_NAME) != 0)
  {
    TraceLog(ERROR_LEVEL, "Manual Order Request: Invalid Server '%s'\n", requestMsg->manualorder->hostname);
    return ERROR;
  }
  
  TraceLog(DEBUG_LEVEL, "user name = %s\n", traderToolsInfo.connection[ttIndex].userName);
  
  t_SendNewOrder newOrder;
  int isError = 0;
  
  // Symbol
  strncpy(newOrder.symbol, requestMsg->manualorder->symbol, SYMBOL_LEN);
  TraceLog(DEBUG_LEVEL, "symbol = %.8s\n", newOrder.symbol);
  
  // Side
  if (requestMsg->manualorder->side == COM__RGM__DAEDALUS__PROTO__SIDE__Ask)
  {
    newOrder.side = BID_SIDE; // PM will sell
  }
  else if (requestMsg->manualorder->side == COM__RGM__DAEDALUS__PROTO__SIDE__Bid)
  {
    newOrder.side = ASK_SIDE; // PM will buy
  }
  else
  {
    isError = -1;
  }
  TraceLog(DEBUG_LEVEL, "side = %d\n", newOrder.side);
  
  // Shares
  newOrder.shares = requestMsg->manualorder->size;
  TraceLog(DEBUG_LEVEL, "shares = %d\n", newOrder.shares);
  if (newOrder.shares <= 0)
  {
    isError = -1;
  }
  
  // MaxFloor
  if (requestMsg->manualorder->hidden == 1)
  {
    newOrder.maxFloor = 0;
  }
  else
  {
    if (requestMsg->manualorder->has_maxfloor)
    {
      newOrder.maxFloor = requestMsg->manualorder->maxfloor;
    }
    else
    {
      newOrder.maxFloor = newOrder.shares;
    }
  }
  TraceLog(DEBUG_LEVEL, "maxFloor = %d (hidden=%d)\n",
    newOrder.maxFloor, requestMsg->manualorder->hidden);
  
  // Price
  newOrder.price = requestMsg->manualorder->price * TEN_DOWN_TO_FOUR;
  TraceLog(DEBUG_LEVEL, "price = %lf\n", newOrder.price);
  if (newOrder.price < 0.00)
  {
    isError = -1;
  }

  int oec = requestMsg->manualorder->oec;
  int accountIndex;
  if (oec == COM__RGM__DAEDALUS__PROTO__OEC__RASH_OEC)
  {
    newOrder.ECNId = TYPE_NASDAQ_RASH;
    
    if (strlen(requestMsg->manualorder->account) > 0)
    {
      for (accountIndex = 0; accountIndex < MAX_ACCOUNT; accountIndex++)
      {
        if (strcmp(requestMsg->manualorder->account, TradingAccount.RASH[accountIndex]) == 0)
        {
          newOrder.tradingAccount = accountIndex;
          break; //ensure accountIndex < MAX_ACCOUNT
        }
      }
      if (accountIndex == MAX_ACCOUNT)
      {
        isError = -1;
        TraceLog(ERROR_LEVEL, "RASH Account is invalid: '%s'\n", requestMsg->manualorder->account);
      }
    }
    else
    {
      isError = -1;
      TraceLog(ERROR_LEVEL, "RASH Account is invalid '%s'\n", requestMsg->manualorder->account);
    }
  }
  else if (oec == COM__RGM__DAEDALUS__PROTO__OEC__ARCA_DIRECT_OEC)
  {
    newOrder.ECNId = TYPE_ARCA_DIRECT;
    
    if (strlen(requestMsg->manualorder->account) > 0)
    {
      for (accountIndex = 0; accountIndex < MAX_ACCOUNT; accountIndex++)
      {
        if (strcmp(requestMsg->manualorder->account, TradingAccount.DIRECT[accountIndex]) == 0)
        {
          newOrder.tradingAccount = accountIndex;
          break; //ensure accountIndex < MAX_ACCOUNT
        }
      }
      if (accountIndex == MAX_ACCOUNT)
      {
        isError = -1;
        TraceLog(ERROR_LEVEL, "ARCA DIRECT Account is invalid: '%s'\n", requestMsg->manualorder->account);
      }
    }
    else
    {
      isError = -1;
      TraceLog(ERROR_LEVEL, "ARCA DIRECT Account is invalid '%s'\n", requestMsg->manualorder->account);
    }
  }
  else
  {
    TraceLog(ERROR_LEVEL, "Invalid OEC (%d) for manual order!\n", oec);
    return ERROR;
  }
  
  TraceLog(DEBUG_LEVEL, "account = %d\n", newOrder.tradingAccount);
  TraceLog(DEBUG_LEVEL, "ECNId = %s\n", (newOrder.ECNId == TYPE_ARCA_DIRECT) ? "ARCA DIRECT" : "NASDAQ RASH");

  int venueId = -1;
  
  switch (newOrder.ECNId)
  {
    case TYPE_ARCA_DIRECT:
      venueId = TS_ARCA_BOOK_INDEX;
      
      if (feq(newOrder.price, 0.00))
      {
        isError = -1;
      }
      break;
      
    case TYPE_NASDAQ_RASH:
      venueId = TS_NASDAQ_BOOK_INDEX;
      break;
      
    default:
      TraceLog(ERROR_LEVEL, "Invalid ECN (%d) for manual order!\n", newOrder.ECNId);
      isError = -1;
      break;
  }
   
  /*
    GTC: 86400
    IOC: 0
    TTL: 0 < value < 86400
  */
  
  newOrder.tif = requestMsg->manualorder->ttl;
  
  TraceLog(DEBUG_LEVEL, "tif = %d\n", newOrder.tif);
  
  // SendID
  int sendId = requestMsg->manualorder->sendid;
  TraceLog(DEBUG_LEVEL, "sendId = %d\n", sendId);
  
  // Peg difference
  newOrder.pegDef = 0; // This is not used
  
  // Order ID
  newOrder.orderId = atoi(requestMsg->manualorder->orderid);
  
  // Force flag
  int isForced = requestMsg->manualorder->force;
  TraceLog(DEBUG_LEVEL, "isForced = %d\n", isForced);
  
  // Session ID: we only have 1 session per oec for now, but it will be improve
  // Daedalus will send 'any' or and empty string ('') which will indicate that the AS can pick any session id it likes
  if (requestMsg->manualorder->optsessionid)
  {
    char optSessionId[32] = "\0";
    strncpy(optSessionId, requestMsg->manualorder->optsessionid, 32);
    TraceLog(DEBUG_LEVEL, "optSessionId = '%s'\n", optSessionId);
  }
  else
  {
    TraceLog(DEBUG_LEVEL, "optSessionId is NULL\n");
  }
  
  int stockSymbolIndex = GetStockSymbolIndex(newOrder.symbol);
  if (stockSymbolIndex == -1)
  {
    isError = -1;
  }
  
  char reasonText[2] = "\0";
  if (isError == -1)  //There is a problem with values of manual order!
  {
    // TraceLog(DEBUG_LEVEL, "--->1\n");
    ProcessResponseSendNewOrderFromDaedalus(&newOrder, sendId, -1, ttIndex, reasonText);
    return ERROR;
  }
  
  if (isForced == 1)
  {
    int lastAccountTradingStatus = verifyPositionInfo[newOrder.tradingAccount][stockSymbolIndex].isTrading;
    verifyPositionInfo[newOrder.tradingAccount][stockSymbolIndex].isTrading = TRADING_MANUAL;
    verifyPositionInfo[newOrder.tradingAccount][stockSymbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();

    int willAddEventLog = 0;
    if (newOrder.orderId == -1)
    {
      // Increase Order ID
      willAddEventLog = 1;
      pthread_mutex_lock(&nextOrderID_Mutex); 
        newOrder.orderId = clientOrderID;
        clientOrderID += 1;
        SaveClientOrderID("next_order_id", clientOrderID);
      pthread_mutex_unlock(&nextOrderID_Mutex);
    }
    
    strcpy(manualOrderInfo[newOrder.orderId % MAX_MANUAL_ORDER].username, traderToolsInfo.connection[ttIndex].userName);
    
    if (IsAbleToPlaceManualOrderOnPM(newOrder.ECNId) == 1)
    {
      /*  Will place order on PM
        We dont increase Order ID, since it was already increased */
      
      if (SendManualOrderToPM(&newOrder, sendId, isForced, ttIndex) == ERROR) //Failed
      {
        verifyPositionInfo[newOrder.tradingAccount][stockSymbolIndex].isTrading = lastAccountTradingStatus;
        verifyPositionInfo[newOrder.tradingAccount][stockSymbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
    
        if (ProcessResponseSendNewOrderFromDaedalus(&newOrder, sendId, -1, ttIndex, reasonText) == ERROR) return SUCCESS;
      }
    }
    else if (IsAbleToPlaceManualOrderOnAS(newOrder.ECNId) == 1)
    {
      /*  Will place order on AS
        We dont increase Order ID, since it was already increased */

      if (newOrder.ECNId == TYPE_ARCA_DIRECT)
      {
        if (SendNewOrderToARCA_DIRECTOrder(&newOrder) == ERROR) //Failed
        {
          verifyPositionInfo[newOrder.tradingAccount][stockSymbolIndex].isTrading = lastAccountTradingStatus;
          verifyPositionInfo[newOrder.tradingAccount][stockSymbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
          
          if (ProcessResponseSendNewOrderFromDaedalus(&newOrder, sendId, -1, ttIndex, reasonText) == ERROR) return SUCCESS;
        }
        else  //Sent successfully
        {
          if (ProcessResponseSendNewOrderFromDaedalus(&newOrder, sendId, newOrder.orderId, ttIndex, reasonText) == ERROR) return SUCCESS;
        }
      }
      else
      {
        if (SendNewOrderToNASDAQ_RASHOrder(&newOrder) == ERROR)   //Failed
        {
          verifyPositionInfo[newOrder.tradingAccount][stockSymbolIndex].isTrading = lastAccountTradingStatus;
          verifyPositionInfo[newOrder.tradingAccount][stockSymbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
          
          if (ProcessResponseSendNewOrderFromDaedalus(&newOrder, sendId, -1, ttIndex, reasonText) == ERROR) return SUCCESS;
        }
        else  //Sent successfully
        {
          if (ProcessResponseSendNewOrderFromDaedalus(&newOrder, sendId, newOrder.orderId, ttIndex, reasonText) == ERROR) return SUCCESS;
        }
      }
    }
    else  //Cannot place order, it's hard to go here!
    {
      verifyPositionInfo[newOrder.tradingAccount][stockSymbolIndex].isTrading = lastAccountTradingStatus;
      verifyPositionInfo[newOrder.tradingAccount][stockSymbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
      
      ProcessResponseSendNewOrderFromDaedalus(&newOrder, sendId, -1, ttIndex, reasonText);
      return SUCCESS;
    }
    
    if (willAddEventLog == 1)
    {
      //Update manual order event log to database
      char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
      sprintf(activityDetail, "%d,%d", traderToolsInfo.connection[ttIndex].currentState, newOrder.orderId);
      ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_MANUAL_ORDER, activityDetail);
    }
  }
  else  //isForced == 0
  {
    // Check symbol halted
    if (HaltStatusMgmt.haltStatus[stockSymbolIndex][venueId] == TRADING_HALTED)
    {
      char msg[1024];
      sprintf(msg, "Could not send manual order (%.8s, side %d, shares %d, price %lf), because this symbol is halted", 
            newOrder.symbol, newOrder.side, newOrder.shares, newOrder.price);
      
      TraceLog(DEBUG_LEVEL, "%s\n", msg);
      
      // SendNotifyMessageToTT(ttIndex, msg, 0);
      ProcessResponseSendNewOrderFromDaedalus(&newOrder, sendId, -1, ttIndex, msg);
      return SUCCESS;
    }
    
    if (verifyPositionInfo[FIRST_ACCOUNT][stockSymbolIndex].isTrading != TRADING_NONE || 
      verifyPositionInfo[SECOND_ACCOUNT][stockSymbolIndex].isTrading != TRADING_NONE ||
      verifyPositionInfo[THIRD_ACCOUNT][stockSymbolIndex].isTrading != TRADING_NONE || 
      verifyPositionInfo[FOURTH_ACCOUNT][stockSymbolIndex].isTrading != TRADING_NONE)
    {
      //stuck is being traded, so do not send
      char msg[1024];
      sprintf(msg, "Could not send manual order (%.8s, side %d, shares: %d, price %lf), because there is a stuck of same symbol trading on AS/PM, please wait for it to be finished.",
          newOrder.symbol, newOrder.side, newOrder.shares, newOrder.price);
      ProcessResponseSendNewOrderFromDaedalus(&newOrder, sendId, -1, ttIndex, msg);
      
      return SUCCESS;
    }

    verifyPositionInfo[newOrder.tradingAccount][stockSymbolIndex].isTrading = TRADING_MANUAL;
    verifyPositionInfo[newOrder.tradingAccount][stockSymbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
    
    // Increase Order ID
    pthread_mutex_lock(&nextOrderID_Mutex); 
      newOrder.orderId = clientOrderID;
      clientOrderID += 1;
      SaveClientOrderID("next_order_id", clientOrderID);
    pthread_mutex_unlock(&nextOrderID_Mutex);
    
    strcpy(manualOrderInfo[clientOrderID % MAX_MANUAL_ORDER].username, traderToolsInfo.connection[ttIndex].userName);
    
    if (IsCTSOrUTDFConnectedOnPM() == 0)
    {
      /* If CTS or UTDF is not connected on PM, it will be NOT possible to:
        - Check new order's price with last trade price
        - Check for fast market mode
         So ask user if he wants to force the order sending */
      
      verifyPositionInfo[newOrder.tradingAccount][stockSymbolIndex].isTrading = TRADING_NONE;
      verifyPositionInfo[newOrder.tradingAccount][stockSymbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
      
      ProcessResponseSendNewOrderFromDaedalus(&newOrder, sendId, -2, ttIndex, reasonText);
    }
    else
    {
      /*  
        The order will be sent to PM to check for last price with CTS/UTDF or fast market mode
        If OK, it will be sent on PM
        If NOT, a feedback will be returned to AS to ask for TT if he wants to force?
      */
      if (SendManualOrderToPM(&newOrder, sendId, isForced, ttIndex) == ERROR) //Failed
      {
        verifyPositionInfo[newOrder.tradingAccount][stockSymbolIndex].isTrading = TRADING_NONE;
        verifyPositionInfo[newOrder.tradingAccount][stockSymbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
        
        ProcessResponseSendNewOrderFromDaedalus(&newOrder, sendId, -1, ttIndex, reasonText);
      }
    }
    
    //Update manual order event log to database
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    sprintf(activityDetail, "%d,%d", traderToolsInfo.connection[ttIndex].currentState, newOrder.orderId);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_MANUAL_ORDER, activityDetail);
  }
  
  return SUCCESS;
}

int ProcessSendCancelOrderFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex)
{
  if (!requestMsg->cancelorderrequest)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Cancel Order Request is NULL\n");
    return ERROR;
  }
  if (!requestMsg->cancelorderrequest->hostname)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Cancel Order Request does not contain hostname\n");
    return ERROR;
  }
  if (!requestMsg->cancelorderrequest->has_oec)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Cancel Order Request does have OEC ID\n");
    return ERROR;
  }

  // TraceLog(DEBUG_LEVEL, "Daedalus Cancel Order Request: hostname='%s' orderid=%d oec=%d\n",
      // requestMsg->cancelorderrequest->hostname,
      // requestMsg->cancelorderrequest->orderid,
      // requestMsg->cancelorderrequest->oec);
  
  // clOrdId
  int clOrdId = atoi(requestMsg->cancelorderrequest->orderid);

  // ECNOrderId
  char ECNOrderId[24] = "\0"; // "RASH" or "ARCA DIRECT"
  if (requestMsg->cancelorderrequest->oec == COM__RGM__DAEDALUS__PROTO__OEC__RASH_OEC)
  {
    strcpy(ECNOrderId, "RASH");
  }
  else if (requestMsg->cancelorderrequest->oec == COM__RGM__DAEDALUS__PROTO__OEC__ARCA_DIRECT_OEC)
  {
    strcpy(ECNOrderId, "ARCA DIRECT");
  }
  else
  {
    TraceLog(ERROR_LEVEL, "Daedalus Cancel Order Request: Invalid OEC ID(%d), allowed values: RASH(%d) or ARCA DIRECT(%d), clOrdId=%d\n",
        requestMsg->cancelorderrequest->oec,
        COM__RGM__DAEDALUS__PROTO__OEC__RASH_OEC,
        COM__RGM__DAEDALUS__PROTO__OEC__ARCA_DIRECT_OEC,
        clOrdId);
    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Received request to send cancel request to ECN order, clOrdId = %d, OEC = '%s'\n", clOrdId, ECNOrderId);
  if (clOrdId >= 1000000) // TS Order ID
  {
    // Call function to send cancel request to TS
    int tsIndex = clOrdId / 1000000 - 1;
    TraceLog(DEBUG_LEVEL, "Daedalus Order Request: clOrdId=%d ---> tsIndex=%d\n", clOrdId, tsIndex);
    ProcessSendCancelRequestToTS(clOrdId, ECNOrderId, tsIndex);
  }
  else if (clOrdId >= 500000) // PM Order ID
  {
    // Call function to send cancel request to PM
    SendCancelRequestToPM(clOrdId, ECNOrderId);
  }
  else //cancel a manual order
  {
    // Check where we should cancel the order (AS or PM)
    // We will check for the connection status
        
    int indexOpenOrder = CheckOrderIdIndex(clOrdId, MANUAL_ORDER);

    if ((indexOpenOrder == -1) || (indexOpenOrder == -2))
    {
      TraceLog(ERROR_LEVEL, "Cannot find order information with clOrdId = %d and tsId = %d\n", clOrdId, MANUAL_ORDER);
      return ERROR;
    }
    
    int ECNId = asOpenOrder.orderCollection[indexOpenOrder].orderSummary.ECNId;
    
    if (ECNId == TYPE_NASDAQ_RASH)
    {
      //AS and PM now use the same RASH session
      if ((pmInfo.connection.status == CONNECT) && (pmInfo.currentStatus.pmOecStatus[PM_NASDAQ_RASH_INDEX] == CONNECTED))
      {
        ProcessSendManualCancelRequestToPM(clOrdId, ECNOrderId);
      }
      else if (orderStatusMgmt[AS_NASDAQ_RASH_INDEX].isOrderConnected == CONNECTED)
      {
        //RASH is connected at AS so we will use AS
        ProcessSendCancelRequestToECNOrder(clOrdId, ECNOrderId);
      }
      else
      {
        //RASH is not connected at both AS and PM, so we Could not send
        return SUCCESS;
      }
    }
    else if (ECNId == TYPE_ARCA_DIRECT)
    {
      //AS and PM have different ARCA session
      if ((pmInfo.connection.status == CONNECT) && (pmInfo.currentStatus.pmOecStatus[PM_ARCA_DIRECT_INDEX] == CONNECTED))
      {
        ProcessSendManualCancelRequestToPM(clOrdId, ECNOrderId);
      }
      else if (orderStatusMgmt[AS_ARCA_DIRECT_INDEX].isOrderConnected == CONNECTED)
      {
        //ARCA DIRECT is connected at AS so we will use AS
        ProcessSendCancelRequestToECNOrder(clOrdId, ECNOrderId);
      }
      else
      {
        //ARCA DIRECT is not connected at both AS and PM, so we Could not send
        return SUCCESS;
      }
    }
    else
    {
      TraceLog(ERROR_LEVEL, "Invalid ECN to send cancel request (ECN: %d)\n", ECNId);
      return ERROR;
    }
  }

  return SUCCESS;
}

int ProcessEngagePositionFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex)
{
  if (!requestMsg->engageposition)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Engage Position Request is NULL\n");
    return ERROR;
  }
  if (!requestMsg->engageposition->symbol)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Engage Position Request does not contain symbol\n");
    return ERROR;
  }
  if (!requestMsg->engageposition->has_size)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Engage Position Request does have shares\n");
    return ERROR;
  }
  if (!requestMsg->engageposition->has_price)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Engage Position Request does have price\n");
    return ERROR;
  }
  if (!requestMsg->engageposition->has_side)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Engage Position Request does have position side\n");
    return ERROR;
  }
  if (!requestMsg->engageposition->account)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Engage Position Request does have account\n");
    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Daedalus Engage Position Request: symbol='%s' size=%d price=%ld side=%d(Ask:-1,Bid:1) account='%s' filter=%d\n",
      requestMsg->engageposition->symbol,
      requestMsg->engageposition->size,
      requestMsg->engageposition->price,
      requestMsg->engageposition->side,
      requestMsg->engageposition->account,
      requestMsg->engageposition->force);
      
  // Get symbol
  char symbol[SYMBOL_LEN];
  strncpy(symbol, requestMsg->engageposition->symbol, SYMBOL_LEN);
  
  // Get shares
  int shares = requestMsg->engageposition->size;
  
  // Get price
  double price = requestMsg->engageposition->price * TEN_DOWN_TO_FOUR;
  
  // #define BID_SIDE 1 <-- stuck long
  // #define ASK_SIDE 0 <-- stuck short
  // From DungTD: side: 0 is buy, 1 is sell, filter: 0 no, 1 yes
  
  // Get stuck type
  int side;
  if (requestMsg->engageposition->side == COM__RGM__DAEDALUS__PROTO__SIDE__Bid)
  {
    side = ASK_SIDE; // stuck short, PM will buy
  }
  else if (requestMsg->engageposition->side == COM__RGM__DAEDALUS__PROTO__SIDE__Ask)
  {
    side = BID_SIDE; // stuck long, PM will sell
  }
  
  // Get account
  int account = -1; //save account index which gets from daedalus
  int index;
  if (strlen(requestMsg->engageposition->account) > 0)
  {
    for (index = 0; index < MAX_ACCOUNT; index++)
    {
      if (strcmp(requestMsg->engageposition->account, TradingAccount.RASH[index]) == 0)
      {
        account = index;
        break; //ensure index < MAX_ACCOUNT
      }
    }
    if (index == MAX_ACCOUNT)
    {
      TraceLog(ERROR_LEVEL, "Engage Position Request: Account is invalid = '%s'\n", requestMsg->engageposition->account);
      return ERROR;
    }
  }
  else
  {
    TraceLog(ERROR_LEVEL, "Engage Position Request: Account is invalid '%s'\n", requestMsg->engageposition->account);
    return ERROR;
  }
  
  int isFilter = requestMsg->engageposition->force;
  
  TraceLog(DEBUG_LEVEL, "ENGAGING PM: Received stuck from Daedalus, symbol: %.8s, side: %d, shares: %d, price: %lf, account: %d, use filter: %d\n",
                symbol, side, shares, price, account, isFilter);
                
  //Check for engaging stuck 
  int symbolIndex = GetStockSymbolIndex(symbol);
  if (symbolIndex != -1)
  {
    TraceLog(DEBUG_LEVEL, "Verify with Position List (account: %d, symbol: %.8s, shares: %d, side: %d, isTrading: %d)\n", account, symbol, verifyPositionInfo[account][symbolIndex].shares, verifyPositionInfo[account][symbolIndex].side, verifyPositionInfo[account][symbolIndex].isTrading);

    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    int action = 0;   //Engage symbol
    sprintf(activityDetail, "%d,%d,%.8s", traderToolsInfo.connection[ttIndex].currentState, action, symbol);
  
    int lastTradingStatus = verifyPositionInfo[account][symbolIndex].isTrading;
    
    if (isFilter == 0)  //isFilter = 0 --> Engaging without filter, isFilter = 1 --> Use filter
    {
      verifyPositionInfo[account][symbolIndex].isTrading = TRADING_AUTOMATIC;
      verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
      
      t_TSMessage pmMessage;
      BuildPMMessageForSendStuckInfo(&pmMessage, symbol, shares, price, side, 0, account);
      
      if (SendPMMessage(&pmMessage) == SUCCESS)
      {
        //Update to verify position list for this symbol
        verifyPositionInfo[account][symbolIndex].shares = shares;
        verifyPositionInfo[account][symbolIndex].side = side;
        
        //Send a message to notify that stuck is sent to PM
        // SendMessageForNotifyNoMatchingPositionToTT(ttIndex, symbol, shares, side, REQUESTED_SUCCESSFULLY);
        char notifMsg[1024] = "\0";
        sprintf(notifMsg, "Requested successfully, symbol=%s, shares=%d, side=%s",
            symbol, shares, side == ASK_SIDE ? "SHORT" : "LONG");
        BuildAndSendAlertToDaedalus(notifMsg, ttIndex, COM__RGM__DAEDALUS__PROTO__NOTIFICATION_LEVEL_TYPE__ShowLog);

        TraceLog(DEBUG_LEVEL, "Requested successfully, account: %d, symbol: %.8s, shares: %d, side: %d, use filter: %d\n", account, symbol, shares, side, isFilter);

        if (side == BID_SIDE)
        {
          verifyPositionInfo[account][symbolIndex].lockCounter = SHORT_LOCK;
          SendSymbolLockCounterForASymbolToTradeServers(symbol, account, SHORT_LOCK);
        }
        else
        {
          verifyPositionInfo[account][symbolIndex].lockCounter = LONG_LOCK;
          SendSymbolLockCounterForASymbolToTradeServers(symbol, account, LONG_LOCK);
        }
        
        //Update engage event log to database
        ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_PM_ENGAGE_DISENGAGE, activityDetail);   
      }
      else
      {
        verifyPositionInfo[account][symbolIndex].isTrading = lastTradingStatus;
        verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
        
        //Send message no matching position to TT
        // SendMessageForNotifyNoMatchingPositionToTT(ttIndex, symbol, shares, side, NO_MATCHING_POSITION);
        char notifMsg[1024] = "\0";
        sprintf(notifMsg, "No Matching Positions, symbol=%s, shares=%d, side=%s",
            symbol, shares, side == ASK_SIDE ? "SHORT" : "LONG");
        BuildAndSendAlertToDaedalus(notifMsg, ttIndex, COM__RGM__DAEDALUS__PROTO__NOTIFICATION_LEVEL_TYPE__ShowLog);

        TraceLog(ERROR_LEVEL, "Cound not engage position information to PM\n");
      }
    }
    else
    {
      int willSend = 1;

      if (lastTradingStatus != TRADING_NONE)
      {
        willSend = 0;
      }

      verifyPositionInfo[account][symbolIndex].isTrading = TRADING_AUTOMATIC;
      verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
      
      if (willSend == 0)
      {
        verifyPositionInfo[account][symbolIndex].isTrading = lastTradingStatus;
        verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
        
        //stuck is being traded, so do not send to PM
        char msg[1024];
        if (lastTradingStatus == TRADING_AUTOMATIC &&
          verifyPositionInfo[account][symbolIndex].isSleeping == YES)
        {
          sprintf(msg, "Could not engage (%.8s, side %d, shares: %d, price %lf, account %d), because there is a stuck of same symbol trading on PM that the inside spread is too wide. Please wait for it to be finished.",
              symbol, side, shares, price, account);
        }
        else
        {
          sprintf(msg, "Could not engage (%.8s, side %d, shares: %d, price %lf, account %d), because there is a stuck of same symbol trading on AS/PM, please wait for it to be finished.",
              symbol, side, shares, price, account);
        }
        // SendNotifyMessageToTT(ttIndex, msg, 0);
        BuildAndSendAlertToDaedalus(msg, ttIndex, COM__RGM__DAEDALUS__PROTO__NOTIFICATION_LEVEL_TYPE__ShowLog);

        TraceLog(ERROR_LEVEL, "%s\n", msg);
      }
      else
      {
        if ((side == verifyPositionInfo[account][symbolIndex].side) && (shares == verifyPositionInfo[account][symbolIndex].shares))
        {
          // Build and send message to PM
          t_TSMessage pmMessage;
          BuildPMMessageForSendStuckInfo(&pmMessage, symbol, shares, price, side, 0, account);
        
          if (SendPMMessage(&pmMessage) == SUCCESS)
          {
            verifyPositionInfo[account][symbolIndex].isSleeping = NO;
            
            //Send a message to notify that stuck is sent to PM
            // SendMessageForNotifyNoMatchingPositionToTT(ttIndex, symbol, shares, side, REQUESTED_SUCCESSFULLY);
            char notifMsg[1024] = "\0";
            sprintf(notifMsg, "Requested successfully, symbol=%s, shares=%d, side=%s",
                symbol, shares, side == ASK_SIDE ? "SHORT" : "LONG");
            BuildAndSendAlertToDaedalus(notifMsg, ttIndex, COM__RGM__DAEDALUS__PROTO__NOTIFICATION_LEVEL_TYPE__ShowLog);
          
            TraceLog(DEBUG_LEVEL, "Requested successfully, account: %d, symbol: %.8s, shares: %d, side: %d, use filter: %d\n", account, symbol, shares, side, isFilter);
            
            //Update engage event log to database
            ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_PM_ENGAGE_DISENGAGE, activityDetail);
          }
          else
          {
            verifyPositionInfo[account][symbolIndex].isTrading = lastTradingStatus;
            verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
            
            // SendMessageForNotifyNoMatchingPositionToTT(ttIndex, symbol, shares, side, NO_MATCHING_POSITION);
            char notifMsg[1024] = "\0";
            sprintf(notifMsg, "No Matching Positions, symbol=%s, shares=%d, side=%s",
                symbol, shares, side == ASK_SIDE ? "SHORT" : "LONG");
            BuildAndSendAlertToDaedalus(notifMsg, ttIndex, COM__RGM__DAEDALUS__PROTO__NOTIFICATION_LEVEL_TYPE__ShowLog);
            
            TraceLog(ERROR_LEVEL, "Position information matched but could not engage to PM\n");
          }
        }
        else
        {
          verifyPositionInfo[account][symbolIndex].isTrading = lastTradingStatus;
          verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
          
          //Send message no matching position to TT
          // SendMessageForNotifyNoMatchingPositionToTT(ttIndex, symbol, shares, side, NO_MATCHING_POSITION);
          char notifMsg[1024] = "\0";
          sprintf(notifMsg, "No Matching Positions, symbol=%s, shares=%d, side=%s",
              symbol, shares, side == ASK_SIDE ? "SHORT" : "LONG");
          BuildAndSendAlertToDaedalus(notifMsg, ttIndex, COM__RGM__DAEDALUS__PROTO__NOTIFICATION_LEVEL_TYPE__ShowLog);
        
          TraceLog(ERROR_LEVEL, "No Matching Positions, account: %d, symbol: %.8s, shares: %d, side: %d, use filter: %d\n", account, symbol, shares, side, isFilter);
        }
      }
    }
  
    /* If symbol in currently halted, when user engage it, the $isAutoResume will be 1*/
    pthread_mutex_lock(&HaltStatusMgmt.mutexLock);
    if ((HaltStatusMgmt.haltStatus[symbolIndex][TS_NASDAQ_BOOK_INDEX] +
      HaltStatusMgmt.haltStatus[symbolIndex][TS_ARCA_BOOK_INDEX]) != TRADING_NORMAL)  //Symbol is halted on ARCA or NASDAQ
    {
      if (HaltStatusMgmt.isAutoResume[symbolIndex] != 1)
      {
        // We need to send to PM and TT only, TS does not care about isAutoResume value
        memcpy(HaltStatusMgmt.symbol[symbolIndex], symbol, SYMBOL_LEN);
        HaltStatusMgmt.isAutoResume[symbolIndex] = 1;
        SaveHaltedStockListToFile();
        SendAHaltStockToPM(symbolIndex);
        SendAHaltStockToAllDaedalus(symbolIndex);
        SendAHaltStockToAllTT(symbolIndex);
      }
    }
    pthread_mutex_unlock(&HaltStatusMgmt.mutexLock);
  }

  return SUCCESS;
}

int ProcessDisengagePositionFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex)
{
  if (!requestMsg->disengageposition)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Disengage Position Request is NULL\n");
    return ERROR;
  }
  if (!requestMsg->disengageposition->symbol)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Disengage Position Request does not contain symbol\n");
    return ERROR;
  }
  if (!requestMsg->disengageposition->account)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Disengage Position Request does have account\n");
    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Daedalus Disengage Position Request: symbol='%s' account='%s'\n",
      requestMsg->disengageposition->symbol,
      requestMsg->disengageposition->account);

  // Get symbol
  char symbol[SYMBOL_LEN];
  strncpy(symbol, requestMsg->disengageposition->symbol, SYMBOL_LEN);

  // Get account ID
  int account = -1; //save account index which gets from daedalus
  int index;
  if (strlen(requestMsg->disengageposition->account) > 0)
  {
    for (index = 0; index < MAX_ACCOUNT; index++)
    {
      if (strcmp(requestMsg->disengageposition->account, TradingAccount.RASH[index]) == 0)
      {
        account = index;
        break; //ensure index < MAX_ACCOUNT
      }
    }
    if (index == MAX_ACCOUNT)
    {
      TraceLog(ERROR_LEVEL, "Disengage Position Request: Account is invalid = '%s'\n", requestMsg->disengageposition->account);
      return ERROR;
    }
  }
  else
  {
    TraceLog(ERROR_LEVEL, "Disengage Position Request: Account is invalid '%s'\n", requestMsg->disengageposition->account);
    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Received request PM to disengage symbol: %.8s, account: %d...\n", symbol, account);

  //Check for verify position info
  int symbolIndex = GetStockSymbolIndex(symbol);
  
  // Call function to send disengage symbol to PM
  if (SendDisengageSymbolToPM(symbol, account) == SUCCESS)
  {
    if (symbolIndex != -1)
    {
      //Remove this symbol from verify position list
      verifyPositionInfo[account][symbolIndex].isTrading = TRADING_NONE;
      verifyPositionInfo[account][symbolIndex].isPMOwner = NO;
      verifyPositionInfo[account][symbolIndex].alertMonitoringTimeInSeconds = GetNumberOfSecondsOfLocalTime();
      
      // Check if symbol is halted, if halted, then check to change the $isAutoResume
      pthread_mutex_lock(&HaltStatusMgmt.mutexLock);
      if ((HaltStatusMgmt.haltStatus[symbolIndex][TS_NASDAQ_BOOK_INDEX] +
        HaltStatusMgmt.haltStatus[symbolIndex][TS_ARCA_BOOK_INDEX]) != TRADING_NORMAL)  //Symbol is halted on ARCA or NASDAQ
      {
        if (HaltStatusMgmt.isAutoResume[symbolIndex] != 2)
        {
          // We need to send to PM and TT only, TS does not care about isAutoResume value
          memcpy(HaltStatusMgmt.symbol[symbolIndex], symbol, SYMBOL_LEN);
          HaltStatusMgmt.isAutoResume[symbolIndex] = 2;
          SaveHaltedStockListToFile();
          SendAHaltStockToPM(symbolIndex);
          SendAHaltStockToAllDaedalus(symbolIndex);
          SendAHaltStockToAllTT(symbolIndex);
        }
      }
      pthread_mutex_unlock(&HaltStatusMgmt.mutexLock);
    }
    
    //Update disengage event log to database
    char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
    int action = 1;//Disengage symbol
    sprintf(activityDetail, "%d,%d,%.8s", traderToolsInfo.connection[ttIndex].currentState, action, symbol);
    ProcessUpdateTTEventLogToDatabase(traderToolsInfo.connection[ttIndex].userName, ACTIVITY_TYPE_PM_ENGAGE_DISENGAGE, activityDetail);
  }
  
  return SUCCESS;
}

int ProcessResetCurrentStatusFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex)
{
  if (!requestMsg->resetcurrentstatus)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Reset Current Status Request is NULL\n");
    return ERROR;
  }
  if (!requestMsg->resetcurrentstatus->hostname)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Reset Current Status Request does not contain hostname\n");
    return ERROR;
  }
  if (!requestMsg->resetcurrentstatus->has_type)
  {
    TraceLog(ERROR_LEVEL, "Daedalus Reset Current Status Request does have Type (Stuck/Loser of BP)\n");
    return ERROR;
  }

  // TraceLog(DEBUG_LEVEL, "Daedalus Reset Current Status Request: hostname='%s' type=%d\n",
      // requestMsg->resetcurrentstatus->hostname,
      // requestMsg->resetcurrentstatus->type);
      
  int tsIndex = FindTradeServerIndexFromDescription(requestMsg->resetcurrentstatus->hostname);
  if (tsIndex > -1)
  {
    // This request is for TS
    int type;
    if (requestMsg->resetcurrentstatus->type == COM__RGM__DAEDALUS__PROTO__CURRENT_STATUS_TYPE__NumStucks)
    {
      TraceLog(DEBUG_LEVEL, "Reset stuck position\n");
      type = NUM_STUCK_INDEX;
    }
    else if (requestMsg->resetcurrentstatus->type == COM__RGM__DAEDALUS__PROTO__CURRENT_STATUS_TYPE__BuyingPower)
    {
      TraceLog(DEBUG_LEVEL, "Reset buying power\n");
      type = BUYING_POWER_INDEX;
    }
    else if (requestMsg->resetcurrentstatus->type == COM__RGM__DAEDALUS__PROTO__CURRENT_STATUS_TYPE__NumConsecutiveLosers)
    {
      TraceLog(DEBUG_LEVEL, "Reset consecutive loser\n");
      type = NUM_LOSER_INDEX;
    }
    else
    {
      TraceLog(ERROR_LEVEL, "Daedalus Reset TS Current Status Request: Invalid Type %d, allowed values: %d(Stuck), %d(Loser), %d(BP)\n",
          requestMsg->resetcurrentstatus->type,
          COM__RGM__DAEDALUS__PROTO__CURRENT_STATUS_TYPE__NumStucks,
          COM__RGM__DAEDALUS__PROTO__CURRENT_STATUS_TYPE__NumConsecutiveLosers,
          COM__RGM__DAEDALUS__PROTO__CURRENT_STATUS_TYPE__BuyingPower);
      return ERROR;
    }
    
    // Call function to reset TS current status
    SendResetTSCurrentStatusToTS(tsIndex, type);
  }
  else
  {
    if (strcmp(requestMsg->resetcurrentstatus->hostname, AS_SERVER_NAME) == 0)
    {
      int type;
      if (requestMsg->resetcurrentstatus->type == COM__RGM__DAEDALUS__PROTO__CURRENT_STATUS_TYPE__NumStucks)
      {
        TraceLog(DEBUG_LEVEL, "Reset AS stucks (all accounts)\n");
        type = NUM_STUCK_INDEX;
        traderToolsInfo.currentStatus.numStuck = 0;
      }
      else if (requestMsg->resetcurrentstatus->type == COM__RGM__DAEDALUS__PROTO__CURRENT_STATUS_TYPE__BuyingPower)
      {
        if (!requestMsg->resetcurrentstatus->account)
        {
          TraceLog(ERROR_LEVEL, "Reset AS Current Status: Account is NULL\n");
          return ERROR;
        }
        
        int accountIndex;
        for (accountIndex = 0; accountIndex < MAX_ACCOUNT; accountIndex++)
        {
          if (strcmp(requestMsg->resetcurrentstatus->account, TradingAccount.DIRECT[accountIndex]) == 0)
          {
            // found the account index
            break;
          }
        }
        if (accountIndex == MAX_ACCOUNT)
        {
          // could not found account from account list
          TraceLog(ERROR_LEVEL, "Reset AS Current Status: invalid account = '%d' (account index = %d)\n", requestMsg->resetcurrentstatus->account, accountIndex);
          return ERROR;
        }
        
        TraceLog(DEBUG_LEVEL, "Reset AS buying power for account '%s', account index %d\n", requestMsg->resetcurrentstatus->account, accountIndex);
        type = BUYING_POWER_INDEX;
        SendUpdateBuyingPowerToAllTS(accountIndex);
        return SUCCESS;
      }
      else if (requestMsg->resetcurrentstatus->type == COM__RGM__DAEDALUS__PROTO__CURRENT_STATUS_TYPE__NumConsecutiveLosers)
      {
        TraceLog(DEBUG_LEVEL, "Reset AS consecutive loser (all accounts)\n");
        type = NUM_LOSER_INDEX;
        traderToolsInfo.currentStatus.numLoser = 0;
      }
      else
      {
        TraceLog(ERROR_LEVEL, "Daedalus Reset AS Current Status Request: Invalid Type %d, allowed values: %d(Stuck), %d(Loser), %d(BP)\n",
            requestMsg->resetcurrentstatus->type,
            COM__RGM__DAEDALUS__PROTO__CURRENT_STATUS_TYPE__NumStucks,
            COM__RGM__DAEDALUS__PROTO__CURRENT_STATUS_TYPE__NumConsecutiveLosers,
            COM__RGM__DAEDALUS__PROTO__CURRENT_STATUS_TYPE__BuyingPower);
        return ERROR;
      }

      int tsID;
      for (tsID = 0; tsID < MAX_TRADE_SERVER_CONNECTIONS; tsID++)
      {
        // Check connection between AS and TS
        if ((tradeServersInfo.connection[tsID].status == DISCONNECTED) || (tradeServersInfo.connection[tsID].socket == -1))
        {
          if (type == NUM_STUCK_INDEX)
          {
            tradeServersInfo.currentStatus[tsID].numStuck = 0;
          }
          else if (type == NUM_LOSER_INDEX)
          {
            tradeServersInfo.currentStatus[tsID].numLoser = 0;
          }
          
          continue;
        }

        // Call function to reset TS current status (only num of stucks and num of losers)
        SendResetTSCurrentStatusToTS(tsID, type);
      }
    }
    else
    {
      TraceLog(ERROR_LEVEL, "Daedalus Reset Current Status Request: Do not support to reset for server '%s'\n", requestMsg->resetcurrentstatus->hostname);
      return ERROR;
    }
  }
      
  return SUCCESS;
}

int ProcessCheckVersionFromDaedalus(const Com__Rgm__Daedalus__Proto__Request *requestMsg, int ttIndex)
{
  /*t_TTMessage *ttMessage = (t_TTMessage *)message;
  
  int currentIndex = MSG_HEADER_LEN;

  // Update current index
  currentIndex += 4;
  
  char version[11];
  strncpy(version, (char *) &ttMessage->msgContent[currentIndex], 10);
  version[10] = 0;
  
  currentIndex += 10;

  TraceLog(DEBUG_LEVEL, "[%s]TT's version: [%0.10s]\n", &traderToolsInfo.connection[ttIndex].userName, version);

  char reason[1024] = "\0";
  int cmpRet = strncmp(version, moduleVersionMgmt.ttVersion.version, 10);
  if (cmpRet != 0)
  {
    TraceLog(DEBUG_LEVEL, "================================\n");
    TraceLog(DEBUG_LEVEL, "TT lastest version info: \n");
    TraceLog(DEBUG_LEVEL, "Version: %.10s\n", moduleVersionMgmt.ttVersion.version);
    TraceLog(DEBUG_LEVEL, "Release date: %.10s\n", moduleVersionMgmt.ttVersion.date);
    TraceLog(DEBUG_LEVEL, "Description: %s\n", moduleVersionMgmt.ttVersion.description);
    TraceLog(DEBUG_LEVEL, "================================\n");

    // sprintf(reason, "%d\t%s", cmpRet, moduleVersionMgmt.ttVersion.version);
    if (cmpRet < 0)
    {
      sprintf(reason, "Daedalus is out of date (%.10s) , please get the latest version (%.10s)",
            version, moduleVersionMgmt.ttVersion.version);
    }
    else
    {
      sprintf(reason, "Daedalus version (%.10s) is not matched with Daedalus version (%.10s) on database",
            version, moduleVersionMgmt.ttVersion.version);
    }
    BuildNSendTTOutOfDateAlertToTT(reason, ttIndex);
    
    isValidTTVersion[ttIndex] = -1;
  }
  else
  {
    isValidTTVersion[ttIndex] = 1;
  }*/
//---------------------------------------------
  return SUCCESS;
}

#ifndef RELEASE_MODE
void ______BUILD_DAEDALUS_MESSAGE_TO_SEND______(){}
#endif

int BuildAndSendMdcStatusToDaedalus(void *pConnection,
                char *serverName,
                char serverConnectionStatus,
                Com__Rgm__Daedalus__Proto__MDC mdcType,
                protobuf_c_boolean isConnected,
                protobuf_c_boolean queryable)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  // Check connection between AS and Daedalus
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }
  
  t_TTMessage ttMessage;

  // t_TTMessage *ttMessage = (t_TTMessage *)message;
  Com__Rgm__Daedalus__Proto__Message daedalusMsg = COM__RGM__DAEDALUS__PROTO__MESSAGE__INIT;
  
  daedalusMsg.has_type = 1;
  daedalusMsg.type = COM__RGM__DAEDALUS__PROTO__MESSAGE__MESSAGE_TYPE__MDC_STATUS;
  
  Com__Rgm__Daedalus__Proto__MdcStatus mdcStatus = COM__RGM__DAEDALUS__PROTO__MDC_STATUS__INIT;
  daedalusMsg.mdcstatus = &mdcStatus;
  
  // hostname (server name)
  mdcStatus.hostname = serverName;
  
  // mdc (venue)
  mdcStatus.has_mdc = 1;
  mdcStatus.mdc = mdcType;
  
  // connected (Connection State: Connected/Disconnected)
  mdcStatus.has_connected = 1;
  
  // (Book State: Valid/Invalid)
  mdcStatus.has_valid = 1;
  
  if (serverConnectionStatus == DISCONNECT)
  {
    mdcStatus.connected = 0;
    mdcStatus.valid = 0;
  }
  else
  {
    mdcStatus.connected = isConnected;
    mdcStatus.valid = isConnected;
  }
  
  //queryable
  mdcStatus.has_queryable = 1;
  mdcStatus.queryable = queryable;
  
  ttMessage.msgLen = com__rgm__daedalus__proto__message__get_packed_size(&daedalusMsg);
  com__rgm__daedalus__proto__message__pack(&daedalusMsg, ttMessage.msgContent);
  
  if(SendDaedalusMessage(connection, &ttMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL,"%s: Could not send Mdc status of (%s) to Daedalus\n", __func__, serverName);
    return ERROR;
  }
  
  return SUCCESS;
}

int BuildAndSendOecStatusToDaedalus(void *pConnection,
                char *serverName,
                char serverConnectionStatus,
                Com__Rgm__Daedalus__Proto__OEC oecType,
                int orderStatus, //Internal State
                int tradingStatus, //External State
                char *orderIp,
                int orderPort,
                char* sessionId)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  // Check connection between AS and Daedalus
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }
  
  t_TTMessage ttMessage;

  Com__Rgm__Daedalus__Proto__Message daedalusMsg = COM__RGM__DAEDALUS__PROTO__MESSAGE__INIT;
  
  daedalusMsg.has_type = 1;
  daedalusMsg.type = COM__RGM__DAEDALUS__PROTO__MESSAGE__MESSAGE_TYPE__OEC_STATUS;
  
  Com__Rgm__Daedalus__Proto__OecStatus oecStatus = COM__RGM__DAEDALUS__PROTO__OEC_STATUS__INIT;
  daedalusMsg.oecstatus = &oecStatus;
  
  // hostname (server name)
  oecStatus.hostname = serverName;
  
  // oec type
  oecStatus.has_venue = 1;
  oecStatus.venue = oecType;
  
  // oecId
  oecStatus.oecid = sessionId;

  // state (order is connected or not) , externalState (trading status)
  oecStatus.has_state = 1;
  oecStatus.has_externalstate = 1;
  if (serverConnectionStatus == CONNECTED &&
    orderStatus == CONNECTED)
  {
    oecStatus.state = COM__RGM__DAEDALUS__PROTO__OEC_STATE__OecConnected;
    if (tradingStatus == ENABLE)
    {
      oecStatus.externalstate = COM__RGM__DAEDALUS__PROTO__OEC_EXTERNAL_STATE__OecActive;
    }
    else
    {
      oecStatus.externalstate = COM__RGM__DAEDALUS__PROTO__OEC_EXTERNAL_STATE__OecInactive;
      //there may be check with connected value
    }
  }
  else
  {
    oecStatus.state = COM__RGM__DAEDALUS__PROTO__OEC_STATE__OecDisconnected;
    oecStatus.externalstate = COM__RGM__DAEDALUS__PROTO__OEC_EXTERNAL_STATE__OecDown;
  }
  
  Com__Rgm__Daedalus__Proto__NetworkAddress networkAddr = COM__RGM__DAEDALUS__PROTO__NETWORK_ADDRESS__INIT;
  oecStatus.networkaddress = &networkAddr;
  networkAddr.host = orderIp;
  networkAddr.has_port = 1;
  networkAddr.port = orderPort;
  // networkAddr.interface = vlanName;
  
  // oecStatus.username = 
  // oecStatus.password = 
  
  
  oecStatus.has_allowmanualorders = 1;
  
  if( (strcmp(serverName, PM_SERVER_NAME) == 0)
    && (oecType == COM__RGM__DAEDALUS__PROTO__OEC__ARCA_DIRECT_OEC || oecType == COM__RGM__DAEDALUS__PROTO__OEC__RASH_OEC)
    )
  {
    oecStatus.allowmanualorders = 1;
  }
  else
  {
    oecStatus.allowmanualorders = 0;
  }
  
  //clearingAccount
  oecStatus.n_clearingaccount = MAX_ACCOUNT;
  char *account[MAX_ACCOUNT];
  int i;
  for(i = 0; i < MAX_ACCOUNT; i++)
  {
    account[i] = TradingAccount.DIRECT[i];
  }
  oecStatus.clearingaccount = account;
  
  ttMessage.msgLen = com__rgm__daedalus__proto__message__get_packed_size(&daedalusMsg);
  com__rgm__daedalus__proto__message__pack(&daedalusMsg, ttMessage.msgContent);
  
  if(SendDaedalusMessage(connection, &ttMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL,"%s: Could not send Oec status of (%s) to Daedalus\n", __func__, serverName);
    return ERROR;
  }
  
  return SUCCESS;
}

int BuildAndSendServerInfoToDaedalus(void *pConnection, int tsIndex, char *serverName, int accountIndex)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  // Check connection between AS and Daedalus
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }
  
  Com__Rgm__Daedalus__Proto__Message daedalusMsg = COM__RGM__DAEDALUS__PROTO__MESSAGE__INIT;
  daedalusMsg.has_type = 1;
  daedalusMsg.type = COM__RGM__DAEDALUS__PROTO__MESSAGE__MESSAGE_TYPE__BLINKBOX_SERVER_INFO;
  
  Com__Rgm__Daedalus__Proto__BlinkBoxServerInfo blinkBoxServerInfo = COM__RGM__DAEDALUS__PROTO__BLINK_BOX_SERVER_INFO__INIT;
  daedalusMsg.blinkboxserverinfo = &blinkBoxServerInfo;
  
  Com__Rgm__Daedalus__Proto__NetworkAddress networkAddress = COM__RGM__DAEDALUS__PROTO__NETWORK_ADDRESS__INIT;
  blinkBoxServerInfo.address = &networkAddress;
  
  if (tsIndex > -1)
  {
    blinkBoxServerInfo.hostname = serverName;
    
    networkAddress.host = tradeServersInfo.config[tsIndex].ip;
    
    networkAddress.has_port = 1;
    networkAddress.port = tradeServersInfo.config[tsIndex].port;
    
    blinkBoxServerInfo.description = "Trade Server";
    
    // account
    blinkBoxServerInfo.account = TradingAccount.DIRECT[accountIndex];

    // Num Stucks
    blinkBoxServerInfo.has_numstucks = 1;
    blinkBoxServerInfo.numstucks = tradeServersInfo.currentStatus[tsIndex].numStuck;
    
    // Num Losers
    blinkBoxServerInfo.has_numlosers = 1;
    blinkBoxServerInfo.numlosers = tradeServersInfo.currentStatus[tsIndex].numLoser;
    
    // Buying Power
    blinkBoxServerInfo.has_buyingpower = 1;
    blinkBoxServerInfo.buyingpower = tradeServersInfo.currentStatus[tsIndex].buyingPower * TEN_RAISE_TO_FOUR;
    
    // Server Type
    blinkBoxServerInfo.has_servertype = 1;
    blinkBoxServerInfo.servertype = COM__RGM__DAEDALUS__PROTO__BLINK_BOX_SERVER_TYPE__TradeServer;
  }
  else
  {
    if (strcmp(serverName, PM_SERVER_NAME) == 0)
    {
      blinkBoxServerInfo.hostname = PM_SERVER_NAME;
      
      networkAddress.host = pmInfo.config.ip;
      networkAddress.has_port = 1;
      networkAddress.port = pmInfo.config.port;
      
      blinkBoxServerInfo.description = pmSetting.description;
    
      // Num Stucks
      blinkBoxServerInfo.has_numstucks = 0;
      
      // Num Losers
      blinkBoxServerInfo.has_numlosers = 0;
      
      // Buying Power
      blinkBoxServerInfo.has_buyingpower = 0;
      
      // Server Type
      blinkBoxServerInfo.has_servertype = 1;
      blinkBoxServerInfo.servertype = COM__RGM__DAEDALUS__PROTO__BLINK_BOX_SERVER_TYPE__PositionManager;
    }
    else
    {
      blinkBoxServerInfo.hostname = AS_SERVER_NAME;
      
      networkAddress.host = " ";
      networkAddress.has_port = 0;
      
      blinkBoxServerInfo.description = asSetting.description;
      
      // account
      blinkBoxServerInfo.account = TradingAccount.DIRECT[accountIndex];
    
      // Num Stucks
      blinkBoxServerInfo.has_numstucks = 1;
      blinkBoxServerInfo.numstucks = traderToolsInfo.currentStatus.numStuck;  

      // Num Losers
      blinkBoxServerInfo.has_numlosers = 1;
      blinkBoxServerInfo.numlosers = traderToolsInfo.currentStatus.numLoser;

      // Buying Power
      blinkBoxServerInfo.has_buyingpower = 1;
      blinkBoxServerInfo.buyingpower = traderToolsInfo.currentStatus.buyingPower[accountIndex] * TEN_RAISE_TO_FOUR;
      
      // Server Type
      blinkBoxServerInfo.has_servertype = 1;
      blinkBoxServerInfo.servertype = COM__RGM__DAEDALUS__PROTO__BLINK_BOX_SERVER_TYPE__AggregationServer;
    }
  }
  
  t_TTMessage ttMessage;
  ttMessage.msgLen = com__rgm__daedalus__proto__message__get_packed_size(&daedalusMsg);
  com__rgm__daedalus__proto__message__pack(&daedalusMsg, ttMessage.msgContent);
  
  // Send message to Daedalus
  if(SendDaedalusMessage(connection, &ttMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL,"%s: Could not send server infor of (%s) to Daedalus\n", __func__, serverName);
    return ERROR;
  }
  
  return SUCCESS;
}

int BuildAndSendServerStatusToDaedalus(void *pConnection, int tsIndex, char *serverName)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  // Check connection between AS and Daedalus
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }
  
  int i;
  
  Com__Rgm__Daedalus__Proto__Message daedalusMsg = COM__RGM__DAEDALUS__PROTO__MESSAGE__INIT;
  
  daedalusMsg.has_type = 1;
  daedalusMsg.type = COM__RGM__DAEDALUS__PROTO__MESSAGE__MESSAGE_TYPE__SERVER_STATUS;
  
  Com__Rgm__Daedalus__Proto__ServerStatus serverStatus = COM__RGM__DAEDALUS__PROTO__SERVER_STATUS__INIT;
  daedalusMsg.serverstatus = &serverStatus;

  // hostname (Server name)
  serverStatus.hostname = serverName;
  
  // server ID
  serverStatus.has_serverid = 1;
  serverStatus.serverid = tsIndex;
  
  // server role, server status and connection status
  if (tsIndex > -1)
  {
    // This is TS
    
    // Server Type
    serverStatus.has_servertype = 1;
    serverStatus.servertype = COM__RGM__DAEDALUS__PROTO__BLINK_BOX_SERVER_TYPE__TradeServer;
      
    if (tradeServersInfo.connection[tsIndex].status == CONNECT)
    {
      // connection status
      serverStatus.has_connectionstatus = 1;
      serverStatus.connectionstatus = 1;

      // set server role
      
      if (tradeServersInfo.globalConf[tsIndex].serverRole > -1 &&
        tradeServersInfo.globalConf[tsIndex].serverRole < MAX_SERVER_ROLE)
      {
        serverStatus.serverrole = TS_SERVER_ROLE_NAME_BY_INDEX[tradeServersInfo.globalConf[tsIndex].serverRole];
      }
      else
      {
        char blankRole[4] = "\0";
        serverStatus.serverrole = blankRole;
      }
      
      // set server status
      serverStatus.has_serverstatus = 1;
      int connectCounter = 0;
      for(i = 0; i < TS_MAX_BOOK_CONNECTIONS; i++)
      {
        connectCounter += tradeServersInfo.currentStatus[tsIndex].tsMdcStatus[i];
      }
      
      for(i = 0; i < TS_MAX_ORDER_CONNECTIONS; i++)
      {
        connectCounter += tradeServersInfo.currentStatus[tsIndex].tsOecStatus[i];
      }
      
      connectCounter += tradeServersInfo.currentStatus[tsIndex].statusCQS +
                        tradeServersInfo.currentStatus[tsIndex].statusUQDF;

      if (connectCounter == TS_MAX_BOOK_CONNECTIONS + TS_MAX_ORDER_CONNECTIONS + MAX_BBO_SOURCE)
      {
        serverStatus.serverstatus = COM__RGM__DAEDALUS__PROTO__SERVER_CONNECTIONS_STATUS__CONNECTED;
      }
      else if (connectCounter == 0)
      {
        serverStatus.serverstatus = COM__RGM__DAEDALUS__PROTO__SERVER_CONNECTIONS_STATUS__DISCONNECTED;
      }
      else
      {
        serverStatus.serverstatus = COM__RGM__DAEDALUS__PROTO__SERVER_CONNECTIONS_STATUS__SOME_CONNECTED;
      }
      
      // iso trading
      serverStatus.has_isotrading = 1;
      // If CQS or UQDF is disconnected, ISO is disabled
      if (tradeServersInfo.currentStatus[tsIndex].statusCQS == CONNECTED ||
        tradeServersInfo.currentStatus[tsIndex].statusUQDF == CONNECTED)
      {
        // serverStatus.isotrading = tradeServersInfo.currentStatus[tsIndex].statusISOTrading;
        if (tradeServersInfo.currentStatus[tsIndex].statusISOTrading == ENABLE)
        {
          serverStatus.isotrading = COM__RGM__DAEDALUS__PROTO__ISO_TRADING_STATE__IsoActive;
        }
        else
        {
          serverStatus.isotrading = COM__RGM__DAEDALUS__PROTO__ISO_TRADING_STATE__IsoInactive;
        }
      }
      else
      {
        serverStatus.isotrading = COM__RGM__DAEDALUS__PROTO__ISO_TRADING_STATE__IsoDisabled;
      }
    }
    else
    {
      // connection status
      serverStatus.has_connectionstatus = 1;
      serverStatus.connectionstatus = 0;

      // set server role
      char blankRole[4] = "\0";
      serverStatus.serverrole = blankRole;

      // set server status
      serverStatus.has_serverstatus = 1;
      serverStatus.serverstatus = COM__RGM__DAEDALUS__PROTO__SERVER_CONNECTIONS_STATUS__DISCONNECTED;
      tradeServersInfo.currentStatus[tsIndex].serverStatus = COM__RGM__DAEDALUS__PROTO__SERVER_CONNECTIONS_STATUS__DISCONNECTED;
      tradeServersInfo.preStatus[tsIndex].serverStatus = COM__RGM__DAEDALUS__PROTO__SERVER_CONNECTIONS_STATUS__DISCONNECTED;
      
      // iso trading
      serverStatus.has_isotrading = 1;
      serverStatus.isotrading = COM__RGM__DAEDALUS__PROTO__ISO_TRADING_STATE__IsoDisabled;
      tradeServersInfo.currentStatus[tsIndex].isoTrading = COM__RGM__DAEDALUS__PROTO__ISO_TRADING_STATE__IsoDisabled;
      tradeServersInfo.preStatus[tsIndex].isoTrading = COM__RGM__DAEDALUS__PROTO__ISO_TRADING_STATE__IsoDisabled;
    }
  }
  else
  {
    if (strcmp(serverName, PM_SERVER_NAME) == 0)
    {
      // This is PM
      // Server Type
      serverStatus.has_servertype = 1;
      serverStatus.servertype = COM__RGM__DAEDALUS__PROTO__BLINK_BOX_SERVER_TYPE__PositionManager;
      
      // iso trading
      serverStatus.has_isotrading = 0;
      // serverStatus.isotrading = COM__RGM__DAEDALUS__PROTO__ISO_TRADING_STATE__IsoDisable;
      
      // set server role
      char pmRole[4] = "\0";
      serverStatus.serverrole = pmRole;
      
      if (pmInfo.connection.status == CONNECT)
      {
        // connection status
        serverStatus.has_connectionstatus = 1;
        serverStatus.connectionstatus = 1;
        
        // set server status
        serverStatus.has_serverstatus = 1;
        
        int connectedCounter = 0;
            
        for(i = 0; i < PM_MAX_BOOK_CONNECTIONS; i++)
        {
          connectedCounter += pmInfo.currentStatus.pmMdcStatus[i];
        }
        
        for(i = 0; i < PM_MAX_ORDER_CONNECTIONS; i++)
        {
          connectedCounter += pmInfo.currentStatus.pmOecStatus[i];
        }
        
        connectedCounter += pmInfo.currentStatus.statusCTS_Feed + pmInfo.currentStatus.statusCQS_Feed +
                            pmInfo.currentStatus.statusUQDF_Feed + pmInfo.currentStatus.statusUTDF;
                      
        if(connectedCounter == (PM_MAX_BOOK_CONNECTIONS + PM_MAX_ORDER_CONNECTIONS + 4) )
        {
          serverStatus.serverstatus = COM__RGM__DAEDALUS__PROTO__SERVER_CONNECTIONS_STATUS__CONNECTED;
        }
        else if (connectedCounter == 0)
        {
          serverStatus.serverstatus = COM__RGM__DAEDALUS__PROTO__SERVER_CONNECTIONS_STATUS__DISCONNECTED;
        }
        else
        {
          serverStatus.serverstatus = COM__RGM__DAEDALUS__PROTO__SERVER_CONNECTIONS_STATUS__SOME_CONNECTED;
        }
      }
      else
      {
        // connection status
        serverStatus.has_connectionstatus = 1;
        serverStatus.connectionstatus = 0;
        
        // set server status
        serverStatus.has_serverstatus = 1;
        serverStatus.serverstatus = COM__RGM__DAEDALUS__PROTO__SERVER_CONNECTIONS_STATUS__DISCONNECTED;
        pmInfo.currentStatus.serverStatus = COM__RGM__DAEDALUS__PROTO__SERVER_CONNECTIONS_STATUS__DISCONNECTED;
        pmInfo.preStatus.serverStatus = COM__RGM__DAEDALUS__PROTO__SERVER_CONNECTIONS_STATUS__DISCONNECTED; 
      }
    }
    else
    {
      // This is AS
      // Server Type
      serverStatus.has_servertype = 1;
      serverStatus.servertype = COM__RGM__DAEDALUS__PROTO__BLINK_BOX_SERVER_TYPE__AggregationServer;
      
      // iso trading
      serverStatus.has_isotrading = 0;
      
      // set server role
      char asRole[4] = "\0";
      serverStatus.serverrole = asRole;
      
      // connection status
      serverStatus.has_connectionstatus = 1;
      serverStatus.connectionstatus = 1;
      
      // set server status
      serverStatus.has_serverstatus = 1;

      if (orderStatusMgmt[AS_ARCA_DIRECT_INDEX].isOrderConnected == CONNECTED)
      {
        serverStatus.serverstatus = COM__RGM__DAEDALUS__PROTO__SERVER_CONNECTIONS_STATUS__CONNECTED;
      }
      else
      {
        serverStatus.serverstatus = COM__RGM__DAEDALUS__PROTO__SERVER_CONNECTIONS_STATUS__DISCONNECTED;
      } 
    }
  }
  
#ifndef RELEASE_MODE
/*if (serverStatus.has_isotrading == 0)
{
  // TraceLog(DEBUG_LEVEL, "--->'%s' serverstatus=%d connectionstatus=%d\n", serverStatus.hostname, serverStatus.serverstatus, serverStatus.connectionstatus);
}
else// if (strcmp(serverStatus.hostname, "snjblink1") == 0)
{
  // TraceLog(DEBUG_LEVEL, "--->'%s' serverstatus=%d connectionstatus=%d isotrading=%d\n", serverStatus.hostname, serverStatus.serverstatus, serverStatus.connectionstatus, serverStatus.isotrading);
}*/
#endif

  t_TTMessage ttMessage;
  ttMessage.msgLen = com__rgm__daedalus__proto__message__get_packed_size(&daedalusMsg);
  com__rgm__daedalus__proto__message__pack(&daedalusMsg, ttMessage.msgContent);
  
  // Send message to Daedalus
  if(SendDaedalusMessage(connection, &ttMessage) == ERROR)
  {
    TraceLog(ERROR_LEVEL,"%s: Could not send server status of (%s) to Daedalus\n", __func__, serverName);
    return ERROR;
  }
  
  return SUCCESS;
}

int BuildAndSendSymbolStatusToDaedalus(char serverName[MAX_LINE_LEN], int ttIndex, void *message)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];

  // Check connection between AS and Daedalus
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }
  
  Com__Rgm__Daedalus__Proto__Message daedalusMsg = COM__RGM__DAEDALUS__PROTO__MESSAGE__INIT;
  
  daedalusMsg.has_type = 1;
  daedalusMsg.type = COM__RGM__DAEDALUS__PROTO__MESSAGE__MESSAGE_TYPE__SYMBOL_BOOK;
  
  Com__Rgm__Daedalus__Proto__SymbolBook symbolBook = COM__RGM__DAEDALUS__PROTO__SYMBOL_BOOK__INIT;
  daedalusMsg.symbolbook = &symbolBook;

  t_TSMessage *tsMessage = (t_TSMessage *)message;
  int index = MSG_HEADER_LEN + 5;
  char symbol[SYMBOL_LEN] = "\0";
  strncpy(symbol, (char *)&tsMessage->msgContent[index], SYMBOL_LEN);
  index += SYMBOL_LEN;

  int askBlockLen = GetIntNumber(&tsMessage->msgContent[index]);
  int bidBlockLen = GetIntNumber(&tsMessage->msgContent[index + askBlockLen]);
  index += 4; // move the the first ask block
  
  askBlockLen -= 4; //ignore the 4 bytes of block length
  bidBlockLen -= 4; //ignore the 4 bytes of block length
  if ((askBlockLen % 13 != 0) ||
    (bidBlockLen % 13 != 0))
  {
    TraceLog(ERROR_LEVEL, "Error in parsing stock status from TS, askBlockLen=%d bidBlockLen=%d\n", askBlockLen, bidBlockLen);
    return ERROR;
  }

  const int numAskBlocks = askBlockLen / 13; // ecn(1), shares(4), price(8)
  const int numBidBlocks = bidBlockLen / 13;
  int i, ecnIndex;
  double price;
  Com__Rgm__Daedalus__Proto__SymbolBook__BookEntry *pBidBlocks[numBidBlocks];
  Com__Rgm__Daedalus__Proto__SymbolBook__BookEntry bids[numBidBlocks];
  Com__Rgm__Daedalus__Proto__SymbolBook__BookEntry *pAskBlocks[numAskBlocks];
  Com__Rgm__Daedalus__Proto__SymbolBook__BookEntry asks[numAskBlocks];

  for (i = 0; i < numAskBlocks; i++)
  {
    pAskBlocks[i] = &asks[i];
    com__rgm__daedalus__proto__symbol_book__book_entry__init(&asks[i]);

    ecnIndex = tsMessage->msgContent[index];
    index += 1;
    asks[i].has_venue = 1;
    asks[i].venue = TS_BOOK_INDEX_TO_DAEDALUS_MDC_MAPPING[ecnIndex];

    asks[i].has_size = 1;
    asks[i].size = GetIntNumber(&tsMessage->msgContent[index]);;
    index += 4;

    price = GetDoubleNumber(&tsMessage->msgContent[index]);
    index += 8;
    asks[i].has_price = 1;
    asks[i].price = price * TEN_RAISE_TO_FOUR; //convert price from double to long
  }

  index += 4; // move the the first bid block
  for (i = 0; i < numBidBlocks; i++)
  {
    pBidBlocks[i] = &bids[i];
    com__rgm__daedalus__proto__symbol_book__book_entry__init(&bids[i]);

    ecnIndex = tsMessage->msgContent[index];
    index += 1;
    bids[i].has_venue = 1;
    bids[i].venue = TS_BOOK_INDEX_TO_DAEDALUS_MDC_MAPPING[ecnIndex];

    bids[i].has_size = 1;
    bids[i].size = GetIntNumber(&tsMessage->msgContent[index]);
    index += 4;

    price = GetDoubleNumber(&tsMessage->msgContent[index]);
    index += 8;
    bids[i].has_price = 1;
    bids[i].price = price * TEN_RAISE_TO_FOUR; //convert price from double to long
  }

  // symbol
  symbolBook.symbol = symbol;

  // hostname
  symbolBook.hostname = serverName;

  // asks
  symbolBook.n_asks = numAskBlocks;
  symbolBook.asks = pAskBlocks;

  // bids
  symbolBook.n_bids = numBidBlocks;
  symbolBook.bids = pBidBlocks;

  t_TTMessage ttMessage;
  ttMessage.msgLen = com__rgm__daedalus__proto__message__get_packed_size(&daedalusMsg);
  com__rgm__daedalus__proto__message__pack(&daedalusMsg, ttMessage.msgContent);
  
  // Send message to Daedalus
  return SendDaedalusMessage(connection, &ttMessage);
}

int SendTSIgnoreSymbolListToAllDaedalus(void)
{
  int ttIndex;
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;

  // Build ignore stock list message to send Daedalus
  int msglen = BuildIgnoreSymbolListToDaedalus(&ttMessage, COM__RGM__DAEDALUS__PROTO__BLINK_BOX_SERVER_TYPE__TradeServer);
  if (msglen == 0)
  {
    return SUCCESS;
  }

  // Send message to all Daedalus connected to AS
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];
    
    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_DAEDALUS)
    {
      continue;
    }

    // Send message to Daedalus
    SendDaedalusMessage(connection, &ttMessage);
  }
  
  return SUCCESS;
}

int SendTSIgnoreSymbolListToDaedalus(void *pConnection)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Send ignored stock list to TT ...\n");

  t_TTMessage ttMessage;

  // Build ignore stock list message to send Daedalus
  int msglen = BuildIgnoreSymbolListToDaedalus(&ttMessage, COM__RGM__DAEDALUS__PROTO__BLINK_BOX_SERVER_TYPE__TradeServer);
  if (msglen == 0)
  {
    return SUCCESS;
  }

  // Send message to Daedalus
  return SendDaedalusMessage(connection, &ttMessage);
}

int BuildIgnoreSymbolListToDaedalus(void *message, Com__Rgm__Daedalus__Proto__BlinkBoxServerType serverType)
{
  // TraceLog(DEBUG_LEVEL, "--->build ignore symbol: serverType=%d (TS:1, PM:2)\n", serverType);
  
  t_IgnoreStockList *tmpIgnoreList;
  if (serverType == COM__RGM__DAEDALUS__PROTO__BLINK_BOX_SERVER_TYPE__TradeServer)
  {
    // point to TS ignore stock list
    tmpIgnoreList = &IgnoreStockList;
  }
  else // if (serverType == COM__RGM__DAEDALUS__PROTO__BLINK_BOX_SERVER_TYPE__PositionManager)
  {
    // point to PM ignore stock list
    tmpIgnoreList = &pmIgnoreStockList;
  }
  
  int const maxSymbol = tmpIgnoreList->countStock;
  
  // Build Ignored Stock List
  Com__Rgm__Daedalus__Proto__IgnoredSymbolList ignoredList = COM__RGM__DAEDALUS__PROTO__IGNORED_SYMBOL_LIST__INIT;
  ignoredList.has_servertype = 1;
  ignoredList.servertype = serverType;
  ignoredList.n_ignoredsymbols = maxSymbol;
  
  // Build Daedalus msg
  Com__Rgm__Daedalus__Proto__Message daedalusMsg = COM__RGM__DAEDALUS__PROTO__MESSAGE__INIT;
  daedalusMsg.has_type = 1;
  daedalusMsg.type = COM__RGM__DAEDALUS__PROTO__MESSAGE__MESSAGE_TYPE__IGNORED_SYMBOLS_LIST;
  daedalusMsg.ignoredsymbollist = &ignoredList;

  if (maxSymbol == 0)
  {
    TraceLog(DEBUG_LEVEL, "There is no ignored symbol to send daedalus\n");
    
    ignoredList.ignoredsymbols = NULL;
  
    t_TTMessage *ttMessage = (t_TTMessage *)message;
    ttMessage->msgLen = com__rgm__daedalus__proto__message__get_packed_size(&daedalusMsg);
    com__rgm__daedalus__proto__message__pack(&daedalusMsg, ttMessage->msgContent);
    return ttMessage->msgLen;
  }
  
  // Get the ignored symbol list
  char *pSymbolList[maxSymbol];
  char symbolList[maxSymbol][SYMBOL_LEN + 1]; // 1 byte padding for Daedalus get symbol correctly
  memset(symbolList, 0, sizeof(symbolList));
  
  int i;
  for (i = 0; i < maxSymbol; i++)
  {
    strncpy(&symbolList[i][0], &tmpIgnoreList->stockList[i][0], SYMBOL_LEN);
    pSymbolList[i] = &symbolList[i][0];
  }
  
  // Build Ignored Stock List
  ignoredList.ignoredsymbols = pSymbolList;
  
// #ifndef RELEASE_MODE
  // TraceLog(DEBUG_LEVEL, "--->n_ignoredsymbols=%d\n", daedalusMsg.ignoredsymbollist->n_ignoredsymbols);
  // for (i = 0; i < daedalusMsg.ignoredsymbollist->n_ignoredsymbols; i++)
  // {
    // TraceLog(DEBUG_LEVEL, "--->%d: [%s]\n", i, daedalusMsg.ignoredsymbollist->ignoredsymbols[i]);
  // }
// #endif

  t_TTMessage *ttMessage = (t_TTMessage *)message;
  ttMessage->msgLen = com__rgm__daedalus__proto__message__get_packed_size(&daedalusMsg);
  com__rgm__daedalus__proto__message__pack(&daedalusMsg, ttMessage->msgContent);
  return ttMessage->msgLen;
}

int SendIgnoreSymbolRangesListToAllDaedalus(void)
{
  int ttIndex;
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;

  // Build ignore stock list message to send Daedalus
  BuildIgnoreSymbolRangesListToDaedalus(&ttMessage);

  // Send message to all Daedalus connected to AS
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];
    
    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_DAEDALUS)
    {
      continue;
    }

    // Send message to Daedalus
    SendDaedalusMessage(connection, &ttMessage);
  }
  
  return SUCCESS;
}

int SendIgnoreSymbolRangesListToDaedalus(void *pConnection)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Send ignored ranges list to TT ...\n");

  t_TTMessage ttMessage;

  // Build a range ignore stock list message to send Daedalus
  BuildIgnoreSymbolRangesListToDaedalus(&ttMessage);

  // Send message to Daedalus
  return SendDaedalusMessage(connection, &ttMessage);
}

int BuildIgnoreSymbolRangesListToDaedalus(void *message)
{
  int const maxRange = IgnoreStockRangesList.countStock;
  if (maxRange == 0)
  {
    TraceLog(DEBUG_LEVEL, "There is no ignored symbol range to send daedalus\n");
    
    Com__Rgm__Daedalus__Proto__Message daedalusMsg = COM__RGM__DAEDALUS__PROTO__MESSAGE__INIT;
    daedalusMsg.has_type = 1;
    daedalusMsg.type = COM__RGM__DAEDALUS__PROTO__MESSAGE__MESSAGE_TYPE__IGNORED_SYMBOL_RANGES;
    
    daedalusMsg.n_ignoredsymbolranges = 0;
    
    t_TTMessage *ttMessage = (t_TTMessage *)message;
    ttMessage->msgLen = com__rgm__daedalus__proto__message__get_packed_size(&daedalusMsg);
    com__rgm__daedalus__proto__message__pack(&daedalusMsg, ttMessage->msgContent);
    return ttMessage->msgLen;
  }
  
  Com__Rgm__Daedalus__Proto__IgnoredSymbolRange *pSymbolRangeList[maxRange];
  Com__Rgm__Daedalus__Proto__IgnoredSymbolRange symbolRangeList[maxRange];
  int i;
  char start[maxRange][2];
  memset(start, 0, maxRange * 2);
  char end[maxRange][2];
  memset(end, 0, maxRange * 2);
  
  for (i = 0; i < maxRange; i++)
  {
    // symbolRangeList[i] = COM__RGM__DAEDALUS__PROTO__IGNORED_SYMBOL_RANGE__INIT;
    com__rgm__daedalus__proto__ignored_symbol_range__init(&symbolRangeList[i]);
    pSymbolRangeList[i] = &symbolRangeList[i];
    
    start[i][0] = IgnoreStockRangesList.stockList[i][0];
    end[i][0]   = IgnoreStockRangesList.stockList[i][2];
    
    symbolRangeList[i].start = start[i];
    if (end[0] > 0)
    {
      symbolRangeList[i].end = end[i];
    }
  }
  
  Com__Rgm__Daedalus__Proto__Message daedalusMsg = COM__RGM__DAEDALUS__PROTO__MESSAGE__INIT;
  daedalusMsg.has_type = 1;
  daedalusMsg.type = COM__RGM__DAEDALUS__PROTO__MESSAGE__MESSAGE_TYPE__IGNORED_SYMBOL_RANGES;
  daedalusMsg.n_ignoredsymbolranges = maxRange;
  daedalusMsg.ignoredsymbolranges = pSymbolRangeList;
  
/*#ifndef RELEASE_MODE
  TraceLog(DEBUG_LEVEL, "--->n_ignoredsymbolranges=%d\n", daedalusMsg.n_ignoredsymbolranges);
  for (i = 0; i < daedalusMsg.n_ignoredsymbolranges; i++)
  {
    TraceLog(DEBUG_LEVEL, "--->%d: [%s  %s]\n", i, daedalusMsg.ignoredsymbolranges[i]->start, daedalusMsg.ignoredsymbolranges[i]->end);
  }
#endif*/

  t_TTMessage *ttMessage = (t_TTMessage *)message;
  ttMessage->msgLen = com__rgm__daedalus__proto__message__get_packed_size(&daedalusMsg);
  com__rgm__daedalus__proto__message__pack(&daedalusMsg, ttMessage->msgContent);

  return ttMessage->msgLen;
}

int BuildHaltedSymbolListToDaedalus(void *message)
{
  //Determine Number Halted Symbol to send
  int const maxSymbol = RMList.countSymbol;
  int numSymbolToSend = 0;
  int willSend[maxSymbol];
  int i, j;
  for (i = 0; i < maxSymbol; i++)
  {
    willSend[i] = 0;
    for (j = 0; j < TS_MAX_BOOK_CONNECTIONS; j++)
    {
      willSend[i] += HaltStatusMgmt.haltStatus[i][j];
    }
    
    if (willSend[i])
    {
      numSymbolToSend++;
    }
  }
  
  // Process the case: Number Halted Symbol to send is 0
  int const maxHaltedSymbol = numSymbolToSend;
  if (maxHaltedSymbol == 0)
  {
    TraceLog(DEBUG_LEVEL, "There is no halted symbol to send Daedalus\n");
    
    Com__Rgm__Daedalus__Proto__Message daedalusMsg = COM__RGM__DAEDALUS__PROTO__MESSAGE__INIT;
    daedalusMsg.has_type = 1;
    daedalusMsg.type = COM__RGM__DAEDALUS__PROTO__MESSAGE__MESSAGE_TYPE__HALTED_SYMBOLS;
    daedalusMsg.n_haltedsymbols = maxHaltedSymbol;
    
    t_TTMessage *ttMessage = (t_TTMessage *)message;
    ttMessage->msgLen = com__rgm__daedalus__proto__message__get_packed_size(&daedalusMsg);
    com__rgm__daedalus__proto__message__pack(&daedalusMsg, ttMessage->msgContent);
    return ttMessage->msgLen;
  }
  
  // Building the Halted Symbol Message
  Com__Rgm__Daedalus__Proto__HaltedSymbol *pHaltedSymbolList[maxHaltedSymbol];
  Com__Rgm__Daedalus__Proto__HaltedSymbol haltedSymbolList[maxHaltedSymbol];
  numSymbolToSend = 0; //since this point, it is used as index of haltedSymbolList

  int numHaltedVenues, numTradingVenues;
  Com__Rgm__Daedalus__Proto__MDC haltedVenues[maxHaltedSymbol][TS_MAX_BOOK_CONNECTIONS];
  Com__Rgm__Daedalus__Proto__MDC tradingVenues[maxHaltedSymbol][TS_MAX_BOOK_CONNECTIONS];
  memset(haltedVenues, 0, sizeof(haltedVenues));
  memset(tradingVenues, 0, sizeof(tradingVenues));
  
  for (i = 0; i < maxSymbol; i++)
  {
    if (willSend[i])
    {
      //This symbol is halted in some venues
      com__rgm__daedalus__proto__halted_symbol__init(&haltedSymbolList[numSymbolToSend]);
      pHaltedSymbolList[numSymbolToSend] = &haltedSymbolList[numSymbolToSend];
      
      // get symbol
      haltedSymbolList[numSymbolToSend].symbol = HaltStatusMgmt.symbol[i];
      
      // get halted venues
      numHaltedVenues = 0;
      numTradingVenues = 0;
      for (j = 0; j < TS_MAX_BOOK_CONNECTIONS; j++)
      {
        if (HaltStatusMgmt.haltStatus[i][j] == TRADING_HALTED)
        {
          haltedVenues[numSymbolToSend][numHaltedVenues] = TS_BOOK_INDEX_TO_DAEDALUS_MDC_MAPPING[j];
          numHaltedVenues++;
        }
        else //if (HaltStatusMgmt.haltStatus[i][j] == TRADING_NORMAL)
        {
          tradingVenues[numSymbolToSend][numTradingVenues] = TS_BOOK_INDEX_TO_DAEDALUS_MDC_MAPPING[j];
          numTradingVenues++;
        }
      }

      haltedSymbolList[numSymbolToSend].n_haltedvenues = numHaltedVenues;
      haltedSymbolList[numSymbolToSend].haltedvenues = haltedVenues[numSymbolToSend];
      
      haltedSymbolList[numSymbolToSend].n_tradingvenues = numTradingVenues;
      haltedSymbolList[numSymbolToSend].tradingvenues = tradingVenues[numSymbolToSend];
      
      numSymbolToSend++;
    }
  }
  
  Com__Rgm__Daedalus__Proto__Message daedalusMsg = COM__RGM__DAEDALUS__PROTO__MESSAGE__INIT;
  daedalusMsg.has_type = 1;
  daedalusMsg.type = COM__RGM__DAEDALUS__PROTO__MESSAGE__MESSAGE_TYPE__HALTED_SYMBOLS;
  daedalusMsg.n_haltedsymbols = maxHaltedSymbol;
  daedalusMsg.haltedsymbols = pHaltedSymbolList;
  
// #ifndef RELEASE_MODE
  // TraceLog(DEBUG_LEVEL, "Number of halted symbols: %d\n", daedalusMsg.n_haltedsymbols);
  // for (i = 0; i < daedalusMsg.n_haltedsymbols; i++)
  // {
    // TraceLog(DEBUG_LEVEL, "--->%d: [%s] n_haltedvenues=%d n_tradingvenues=%d\n",
      // i, daedalusMsg.haltedsymbols[i]->symbol,
      // daedalusMsg.haltedsymbols[i]->n_haltedvenues,
      // daedalusMsg.haltedsymbols[i]->n_tradingvenues);
  // }
// #endif

  t_TTMessage *ttMessage = (t_TTMessage *)message;
  ttMessage->msgLen = com__rgm__daedalus__proto__message__get_packed_size(&daedalusMsg);
  com__rgm__daedalus__proto__message__pack(&daedalusMsg, ttMessage->msgContent);

  return ttMessage->msgLen;
}

int SendToActivateHaltStockListToAllDaedalus(int updatedList[MAX_STOCK_SYMBOL], int updatedCount)
{
  int ttIndex;
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;

  // Build TS halted symbol message to send Daedalus
  BuildToActivateHaltedSymbolListToDaedalus(&ttMessage, updatedList, updatedCount);

  // Send message to all Daedalus connected to AS
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];

    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_DAEDALUS)
    {
      continue;
    }   

    // Send message to Daedalus
    SendDaedalusMessage(connection, &ttMessage);
  }
  
  return SUCCESS;
}

int BuildToActivateHaltedSymbolListToDaedalus(void *message, int updatedList[MAX_STOCK_SYMBOL], int updatedCount)
{
  //Determine Number Halted Symbol to send
  int const maxSymbol = RMList.countSymbol;
  int numSymbolToSend = 0;
  int willSend[maxSymbol];
  memset(willSend, 0, sizeof(willSend));
  int i, j;
  int stockSymbolIndex;
  
  for (i = 0; i < updatedCount; i++)
  {
    stockSymbolIndex = updatedList[i];
    for (j = 0; j < TS_MAX_BOOK_CONNECTIONS; j++)
    {
      willSend[stockSymbolIndex] += HaltStatusMgmt.haltStatus[stockSymbolIndex][j];
    }
    
    // Check to activate the halted symbol
    if (willSend[stockSymbolIndex] == 0)
    {
      willSend[stockSymbolIndex] = 1;
      numSymbolToSend++;
    }
  }
  
  // Process the case: Number Halted Symbol to send is 0
  int const maxHaltedSymbol = numSymbolToSend;
  if (maxHaltedSymbol == 0)
  {
    TraceLog(DEBUG_LEVEL, "There is halted symbol change to active\n");
  
    Com__Rgm__Daedalus__Proto__Message daedalusMsg = COM__RGM__DAEDALUS__PROTO__MESSAGE__INIT;
    daedalusMsg.has_type = 1;
    daedalusMsg.type = COM__RGM__DAEDALUS__PROTO__MESSAGE__MESSAGE_TYPE__HALTED_SYMBOLS;
    daedalusMsg.n_haltedsymbols = maxHaltedSymbol;
    
    t_TTMessage *ttMessage = (t_TTMessage *)message;
    ttMessage->msgLen = com__rgm__daedalus__proto__message__get_packed_size(&daedalusMsg);
    com__rgm__daedalus__proto__message__pack(&daedalusMsg, ttMessage->msgContent);
    return ttMessage->msgLen;
  }
  
  // Building the Halted Symbol Message
  Com__Rgm__Daedalus__Proto__HaltedSymbol *pHaltedSymbolList[maxHaltedSymbol];
  Com__Rgm__Daedalus__Proto__HaltedSymbol haltedSymbolList[maxHaltedSymbol];
  numSymbolToSend = 0; //since this point, it is used as index of haltedSymbolList

  int numHaltedVenues, numTradingVenues;
  Com__Rgm__Daedalus__Proto__MDC haltedVenues[maxHaltedSymbol][TS_MAX_BOOK_CONNECTIONS];
  Com__Rgm__Daedalus__Proto__MDC tradingVenues[maxHaltedSymbol][TS_MAX_BOOK_CONNECTIONS];
  memset(haltedVenues, 0, sizeof(haltedVenues));
  memset(tradingVenues, 0, sizeof(tradingVenues));
  
  for (i = 0; i < maxSymbol; i++)
  {
    if (willSend[i])
    {
      //This symbol is halted in some venues
      com__rgm__daedalus__proto__halted_symbol__init(&haltedSymbolList[numSymbolToSend]);
      pHaltedSymbolList[numSymbolToSend] = &haltedSymbolList[numSymbolToSend];
      
      // get symbol
      haltedSymbolList[numSymbolToSend].symbol = HaltStatusMgmt.symbol[i];
      
      // get halted venues
      numHaltedVenues = 0;
      numTradingVenues = 0;
      for (j = 0; j < TS_MAX_BOOK_CONNECTIONS; j++)
      {
        if (HaltStatusMgmt.haltStatus[i][j] == TRADING_HALTED)
        {
          haltedVenues[numSymbolToSend][numHaltedVenues] = TS_BOOK_INDEX_TO_DAEDALUS_MDC_MAPPING[j];
          numHaltedVenues++;
        }
        else //if (HaltStatusMgmt.haltStatus[i][j] == TRADING_NORMAL)
        {
          tradingVenues[numSymbolToSend][numTradingVenues] = TS_BOOK_INDEX_TO_DAEDALUS_MDC_MAPPING[j];
          numTradingVenues++;
        }
      }

      haltedSymbolList[numSymbolToSend].n_haltedvenues = numHaltedVenues;
      haltedSymbolList[numSymbolToSend].haltedvenues = haltedVenues[numSymbolToSend];
      
      haltedSymbolList[numSymbolToSend].n_tradingvenues = numTradingVenues;
      haltedSymbolList[numSymbolToSend].tradingvenues = tradingVenues[numSymbolToSend];
      
      numSymbolToSend++;
    }
  }
  
  Com__Rgm__Daedalus__Proto__Message daedalusMsg = COM__RGM__DAEDALUS__PROTO__MESSAGE__INIT;
  daedalusMsg.has_type = 1;
  daedalusMsg.type = COM__RGM__DAEDALUS__PROTO__MESSAGE__MESSAGE_TYPE__HALTED_SYMBOLS;
  daedalusMsg.n_haltedsymbols = maxHaltedSymbol;
  daedalusMsg.haltedsymbols = pHaltedSymbolList;
  
  TraceLog(DEBUG_LEVEL, "Send activate all halted stocks to Daedalus, number of symbol: %d\n", numSymbolToSend);
  
// #ifndef RELEASE_MODE
  // TraceLog(DEBUG_LEVEL, "Number of halted symbols: %d\n", daedalusMsg.n_haltedsymbols);
  // for (i = 0; i < daedalusMsg.n_haltedsymbols; i++)
  // {
    // TraceLog(DEBUG_LEVEL, "--->%d: [%s] n_haltedvenues=%d n_tradingvenues=%d\n",
      // i, daedalusMsg.haltedsymbols[i]->symbol,
      // daedalusMsg.haltedsymbols[i]->n_haltedvenues,
      // daedalusMsg.haltedsymbols[i]->n_tradingvenues);
  // }
// #endif

  t_TTMessage *ttMessage = (t_TTMessage *)message;
  ttMessage->msgLen = com__rgm__daedalus__proto__message__get_packed_size(&daedalusMsg);
  com__rgm__daedalus__proto__message__pack(&daedalusMsg, ttMessage->msgContent);

  return ttMessage->msgLen;
}

int BuildVolatileSymbolListToDaedalus(void *message)
{
  // Get the volatile symbol list to send
  char list[MAX_STOCK_SYMBOL * SYMBOL_LEN];
  int count = 0;
  
  pthread_mutex_lock(&volatileMgmt.mutexLock);
    LoadVolatileSymbolsFromFileToList(list, &count);
  pthread_mutex_unlock(&volatileMgmt.mutexLock);
  
  const int maxSymbol = count;
  
  // Build Daedalus msg
  Com__Rgm__Daedalus__Proto__Message daedalusMsg = COM__RGM__DAEDALUS__PROTO__MESSAGE__INIT;
  daedalusMsg.has_type = 1;
  daedalusMsg.type = COM__RGM__DAEDALUS__PROTO__MESSAGE__MESSAGE_TYPE__VOLATILE_SYMBOLS;
  daedalusMsg.n_volatilesymbols = maxSymbol;
  
  TraceLog(DEBUG_LEVEL, "Number of volatile symbols: %d\n", maxSymbol);
  
  if (maxSymbol == 0)
  {
    t_TTMessage *ttMessage = (t_TTMessage *)message;
    ttMessage->msgLen = com__rgm__daedalus__proto__message__get_packed_size(&daedalusMsg);
    com__rgm__daedalus__proto__message__pack(&daedalusMsg, ttMessage->msgContent);
    return ttMessage->msgLen;
  }
  
  char *pSymbolList[maxSymbol];
  // char symbolList[maxSymbol][SYMBOL_LEN];
  // memcpy(symbolList, list, maxSymbol * SYMBOL_LEN);
  char symbolList[maxSymbol][SYMBOL_LEN + 1]; // 1 byte padding for Daedalus gets symbol easier
  memset(symbolList, 0, sizeof(symbolList));
  
  int i;
  for (i = 0; i < maxSymbol; i++)
  {
    strncpy(&symbolList[i][0], &list[i * SYMBOL_LEN], SYMBOL_LEN);
    pSymbolList[i] = &symbolList[i][0];
  }

  daedalusMsg.volatilesymbols = pSymbolList;
  
/*#ifndef RELEASE_MODE
  TraceLog(DEBUG_LEVEL, "--->n_volatilesymbols=%d\n", daedalusMsg.n_volatilesymbols);
  for (i = 0; i < daedalusMsg.n_volatilesymbols; i++)
  {
    TraceLog(DEBUG_LEVEL, "--->%d: volatilesymbols [%s]\n", i, daedalusMsg.volatilesymbols[i]);
  }
#endif*/

  t_TTMessage *ttMessage = (t_TTMessage *)message;
  ttMessage->msgLen = com__rgm__daedalus__proto__message__get_packed_size(&daedalusMsg);
  com__rgm__daedalus__proto__message__pack(&daedalusMsg, ttMessage->msgContent);
  
  return ttMessage->msgLen;
}

int BuildVolatileSymbolListBySessionToDaedalus(void *message)
{
  // Get the volatile symbol lists to send
  int countPre = 0, countPost = 0, countIntraday = 0;
  char preList[MAX_STOCK_SYMBOL * SYMBOL_LEN];
  char postList[MAX_STOCK_SYMBOL * SYMBOL_LEN];
  char intradayList[MAX_STOCK_SYMBOL * SYMBOL_LEN];
  
  pthread_mutex_lock(&volatileMgmt.mutexLock);
    LoadVolatileSectionsFromFileToLists(preList, &countPre, postList, &countPost, intradayList, &countIntraday);
  pthread_mutex_unlock(&volatileMgmt.mutexLock);
  
  const int totalSymbol = countPre + countPost + countIntraday;

  // Build Daedalus msg
  Com__Rgm__Daedalus__Proto__Message daedalusMsg = COM__RGM__DAEDALUS__PROTO__MESSAGE__INIT;
  daedalusMsg.has_type = 1;
  daedalusMsg.type = COM__RGM__DAEDALUS__PROTO__MESSAGE__MESSAGE_TYPE__VOLATILE_SYMBOLS_BY_SESSION;
  
  Com__Rgm__Daedalus__Proto__VolatileSymbolsBySession volatileSymbolBySession = COM__RGM__DAEDALUS__PROTO__VOLATILE_SYMBOLS_BY_SESSION__INIT;
  daedalusMsg.volatilesymbolsbysession = &volatileSymbolBySession;
  
  volatileSymbolBySession.n_prevolatilesymbols = countPre;
  volatileSymbolBySession.n_postvolatilesymbols = countPost;
  volatileSymbolBySession.n_unknownvolatilesymbols = countIntraday;
  
  TraceLog(DEBUG_LEVEL, "Number of volatile symbols by session: pre=%d, post=%d, intraday=%d\n", countPre, countPost, countIntraday);
  
  if (totalSymbol == 0)
  {
    t_TTMessage *ttMessage = (t_TTMessage *)message;
    ttMessage->msgLen = com__rgm__daedalus__proto__message__get_packed_size(&daedalusMsg);
    com__rgm__daedalus__proto__message__pack(&daedalusMsg, ttMessage->msgContent);
    return ttMessage->msgLen;
  }
  
  // volatileList will contains all symbol from pre, post and intraday
  // with the order: pre, post, intraday
  char volatileList[totalSymbol][SYMBOL_LEN + 1];
  memset(volatileList, 0, sizeof(volatileList));
  int volatileIndex = 0; // index of volatileList
  
  int i;
  char *pointerPreList[MAX_STOCK_SYMBOL];
  if (countPre > 0)
  {
    for (i = 0; i < countPre; i++)
    {
      strncpy(&volatileList[volatileIndex][0], &preList[i * SYMBOL_LEN], SYMBOL_LEN);
      
      pointerPreList[i] = &volatileList[volatileIndex][0];
      
      volatileIndex++;
    }

    volatileSymbolBySession.prevolatilesymbols = pointerPreList;
  }
  
  char *pointerPostList[MAX_STOCK_SYMBOL];
  if (countPost > 0)
  {
    for (i = 0; i < countPost; i++)
    {
      strncpy(&volatileList[volatileIndex][0], &postList[i * SYMBOL_LEN], SYMBOL_LEN);
      
      pointerPostList[i] = &volatileList[volatileIndex][0];
      
      volatileIndex++;
    }

    volatileSymbolBySession.postvolatilesymbols = pointerPostList;
  }
  
  char *pointerIntradayList[MAX_STOCK_SYMBOL];
  if (countIntraday > 0)
  {
    for (i = 0; i < countIntraday; i++)
    {
      strncpy(&volatileList[volatileIndex][0], &intradayList[i * SYMBOL_LEN], SYMBOL_LEN);
      
      pointerIntradayList[i] = &volatileList[volatileIndex][0];
      
      volatileIndex++;
    }

    volatileSymbolBySession.unknownvolatilesymbols = pointerIntradayList;
  }
/*#ifndef RELEASE_MODE
  TraceLog(DEBUG_LEVEL, "--->n_prevolatilesymbols=%d n_postvolatilesymbols=%d n_unknownvolatilesymbols=%d\n", volatileSymbolBySession.n_prevolatilesymbols, volatileSymbolBySession.n_postvolatilesymbols, volatileSymbolBySession.n_unknownvolatilesymbols);
  for (i = 0; i < volatileSymbolBySession.n_prevolatilesymbols; i++)
  {
    TraceLog(DEBUG_LEVEL, "--->%d: prevolatilesymbols [%s]\n", i, volatileSymbolBySession.prevolatilesymbols[i]);
  }
  for (i = 0; i < volatileSymbolBySession.n_postvolatilesymbols; i++)
  {
    TraceLog(DEBUG_LEVEL, "--->%d: postvolatilesymbols [%s]\n", i, volatileSymbolBySession.postvolatilesymbols[i]);
  }
  for (i = 0; i < volatileSymbolBySession.n_unknownvolatilesymbols; i++)
  {
    TraceLog(DEBUG_LEVEL, "--->%d: intradayvolatilesymbols [%s]\n", i, volatileSymbolBySession.unknownvolatilesymbols[i]);
  }
#endif*/

  t_TTMessage *ttMessage = (t_TTMessage *)message;
  ttMessage->msgLen = com__rgm__daedalus__proto__message__get_packed_size(&daedalusMsg);
  com__rgm__daedalus__proto__message__pack(&daedalusMsg, ttMessage->msgContent);
  return ttMessage->msgLen;
}

int BuildVolatileToggleListToDaedalus(void *message)
{
  // Init Daedalus msg and set msg type
  Com__Rgm__Daedalus__Proto__Message daedalusMsg = COM__RGM__DAEDALUS__PROTO__MESSAGE__INIT;
  daedalusMsg.has_type = 1;
  daedalusMsg.type = COM__RGM__DAEDALUS__PROTO__MESSAGE__MESSAGE_TYPE__VOLATILE_SYMBOLS_SCHEDULE;
  
  // Get number of schedule
  Com__Rgm__Daedalus__Proto__VolatileSymbolsSchedule schedule = COM__RGM__DAEDALUS__PROTO__VOLATILE_SYMBOLS_SCHEDULE__INIT;
  daedalusMsg.volatilesymbolsschedule = &schedule;
  schedule.n_periods = volatileMgmt.numAllVolatileToggle;
  
  TraceLog(DEBUG_LEVEL, "Number of volatile symbol schedule: %d\n", schedule.n_periods);
  
  // Send msg if period is 0
  if (schedule.n_periods == 0)
  {
    t_TTMessage *ttMessage = (t_TTMessage *)message;
    ttMessage->msgLen = com__rgm__daedalus__proto__message__get_packed_size(&daedalusMsg);
    com__rgm__daedalus__proto__message__pack(&daedalusMsg, ttMessage->msgContent);
    return ttMessage->msgLen;
  }
  
  // Get the schedule items
  const int maxPeriod = schedule.n_periods;
  Com__Rgm__Daedalus__Proto__TimeRange *pPeriods[maxPeriod];
  Com__Rgm__Daedalus__Proto__TimeRange periods[maxPeriod];
  
  int i;
  for (i = 0; i < maxPeriod; i++)
  {
    com__rgm__daedalus__proto__time_range__init(&periods[i]);
    pPeriods[i] = &periods[i];
    
    periods[i].has_begin = 1;
    periods[i].has_end = 1;
    periods[i].begin = volatileMgmt.allVolatileOnTime[i];
    periods[i].end = volatileMgmt.allVolatileOffTime[i];
  }
  
  schedule.periods = pPeriods;

/*#ifndef RELEASE_MODE
  TraceLog(DEBUG_LEVEL, "--->n_periods=%d\n", schedule.n_periods);
  for (i = 0; i < daedalusMsg.volatilesymbolsschedule->n_periods; i++)
  {
    TraceLog(DEBUG_LEVEL, "--->%d: begin=%ld end=%ld\n", i, schedule.periods[i]->begin, schedule.periods[i]->end);
  }
#endif*/

  t_TTMessage *ttMessage = (t_TTMessage *)message;
  ttMessage->msgLen = com__rgm__daedalus__proto__message__get_packed_size(&daedalusMsg);
  com__rgm__daedalus__proto__message__pack(&daedalusMsg, ttMessage->msgContent);
  return ttMessage->msgLen;
}

int SendCurrentVolatileSessionToAllDaedalus()
{
  int ttIndex;
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;
  
  TraceLog(DEBUG_LEVEL, "Sending current volatile session to all Daedalus...\n");

  BuildCurrentVolatileSession(&ttMessage);

  // Send message to all Daedalus connected to AS
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];
    
    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_DAEDALUS)
    {
      continue;
    }

    // Send message to Daedalus
    SendDaedalusMessage(connection, &ttMessage);
  }

  return SUCCESS;
}

int SendCurrentVolatileSessionToDaedalus(void *pConnection)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Sending current volatile session to Daedalus...\n");

  t_TTMessage ttMessage;

  BuildCurrentVolatileSession(&ttMessage);

  // Send message to Daedalus
  return SendDaedalusMessage(connection, &ttMessage);
}

int BuildCurrentVolatileSession(void *message)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;
  
  // Build Active Volatile Symbol By Session Msg
  Com__Rgm__Daedalus__Proto__ActiveVolatileSymbolsBySession currentSession = COM__RGM__DAEDALUS__PROTO__ACTIVE_VOLATILE_SYMBOLS_BY_SESSION__INIT;
  currentSession.has_session = 1;
  
  char sessionName[16] = "\0";
  int session = GetCurrentVolatileSession();
  switch (session)
  {
    case VOLATILE_SESSION_INTRADAY:
      currentSession.session = COM__RGM__DAEDALUS__PROTO__VOLATILE_SESSION__INTRADAY;
      strncpy(sessionName, "Intraday", 8);
      break;
    case VOLATILE_SESSION_PRE:
      currentSession.session = COM__RGM__DAEDALUS__PROTO__VOLATILE_SESSION__PRE;
      strncpy(sessionName, "Pre", 3);
      break;
    case VOLATILE_SESSION_POST:
      currentSession.session = COM__RGM__DAEDALUS__PROTO__VOLATILE_SESSION__POST;
      strncpy(sessionName, "Post", 4);
      break;
    default:
      TraceLog(WARN_LEVEL, "Current volatile session is invalid: %d\n", session);
      return ERROR;
  }
  
  // Build Daedalus msg
  Com__Rgm__Daedalus__Proto__Message daedalusMsg = COM__RGM__DAEDALUS__PROTO__MESSAGE__INIT;
  daedalusMsg.has_type = 1;
  daedalusMsg.type = COM__RGM__DAEDALUS__PROTO__MESSAGE__MESSAGE_TYPE__ACTIVE_VOLATILE_SYMBOLS_BY_SESSION;
  daedalusMsg.activevolatilesymbolsbysession = &currentSession;
  
  TraceLog(DEBUG_LEVEL, "Built current volatile session: '%s' (%d) msgType=%d\n",
    sessionName, daedalusMsg.activevolatilesymbolsbysession->session, daedalusMsg.type);
  
  ttMessage->msgLen = com__rgm__daedalus__proto__message__get_packed_size(&daedalusMsg);
  com__rgm__daedalus__proto__message__pack(&daedalusMsg, ttMessage->msgContent);
  
  return ttMessage->msgLen;
}

/*int BuildAndSendCurrentVolatileSessionToAllDaedalus()
{
  // Build Active Volatile Symbol By Session Msg
  Com__Rgm__Daedalus__Proto__ActiveVolatileSymbolsBySession currentSession = COM__RGM__DAEDALUS__PROTO__ACTIVE_VOLATILE_SYMBOLS_BY_SESSION__INIT;
  currentSession.has_session = 1;
  
  char sessionName[16] = "\0";
  int session = GetCurrentVolatileSession();
  switch (session)
  {
    case VOLATILE_SESSION_INTRADAY:
      currentSession.session = COM__RGM__DAEDALUS__PROTO__VOLATILE_SESSION__INTRADAY;
      strncpy(sessionName, "Intraday", 8);
      break;
    case VOLATILE_SESSION_PRE:
      currentSession.session = COM__RGM__DAEDALUS__PROTO__VOLATILE_SESSION__PRE;
      strncpy(sessionName, "Pre", 3);
      break;
    case VOLATILE_SESSION_POST:
      currentSession.session = COM__RGM__DAEDALUS__PROTO__VOLATILE_SESSION__POST;
      strncpy(sessionName, "Post", 4);
      break;
    default:
      TraceLog(WARN_LEVEL, "Current volatile session is invalid: %d\n", session);
      return ERROR;
  }
  
  // Build Daedalus msg
  Com__Rgm__Daedalus__Proto__Message daedalusMsg = COM__RGM__DAEDALUS__PROTO__MESSAGE__INIT;
  daedalusMsg.has_type = 1;
  daedalusMsg.type = COM__RGM__DAEDALUS__PROTO__MESSAGE__MESSAGE_TYPE__NOTIFICATION;
  daedalusMsg.activevolatilesymbolsbysession = &currentSession;
  
  TraceLog(DEBUG_LEVEL, "Sending current volatile session to all Daedalus: '%s' (%d)\n",
    sessionName, daedalusMsg.activevolatilesymbolsbysession->session);
  
  // Packing Daedalus msg
  t_TTMessage ttMessage;
  ttMessage.msgLen = com__rgm__daedalus__proto__message__get_packed_size(&daedalusMsg);
  com__rgm__daedalus__proto__message__pack(&daedalusMsg, ttMessage.msgContent);
  
  int retVal = SendMessageToAllDaedalus(&ttMessage);
  return retVal;
}*/

int SendVolatileToggleToAllDaedalus()
{
  int ttIndex;
  t_TTMessage ttMessage;
  t_ConnectionStatus *connection;
  
  TraceLog(DEBUG_LEVEL, "Sending volatile symbols by toggle to all Daedalus...\n");

  BuildVolatileToggle(&ttMessage);

  // Send message to all Daedalus connected to AS
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];
    
    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_DAEDALUS)
    {
      continue;
    }

    // Send message to Daedalus
    SendDaedalusMessage(connection, &ttMessage);
  }

  return SUCCESS;
}

int SendVolatileToggleToDaedalus(void *pConnection)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Sending volatile symbols by toggle to Daedalus...\n");

  t_TTMessage ttMessage;

  BuildVolatileToggle(&ttMessage);

  // Send message to Daedalus
  return SendDaedalusMessage(connection, &ttMessage);
}

int BuildVolatileToggle(void *message)
{
  t_TTMessage *ttMessage = (t_TTMessage *)message;
  
  // Build Active Volatile Symbols By Toggle Msg
  Com__Rgm__Daedalus__Proto__ActiveVolatileSymbolsByToggle toggle = COM__RGM__DAEDALUS__PROTO__ACTIVE_VOLATILE_SYMBOLS_BY_TOGGLE__INIT;
  toggle.has_isactive = 1;
  toggle.isactive = volatileMgmt.currentAllVolatileValue;
  
  // Build Daedalus msg
  Com__Rgm__Daedalus__Proto__Message daedalusMsg = COM__RGM__DAEDALUS__PROTO__MESSAGE__INIT;
  daedalusMsg.has_type = 1;
  daedalusMsg.type = COM__RGM__DAEDALUS__PROTO__MESSAGE__MESSAGE_TYPE__ACTIVE_VOLATILE_SYMBOLS_BY_TOGGLE;
  daedalusMsg.activevolatilesymbolsbytoggle = &toggle;
  
  TraceLog(DEBUG_LEVEL, "Built all volatile symbols by toggle: '%s' msgType=%d\n",
    daedalusMsg.activevolatilesymbolsbytoggle->isactive == 1 ? "ON":"OFF", daedalusMsg.type);
  
  ttMessage->msgLen = com__rgm__daedalus__proto__message__get_packed_size(&daedalusMsg);
  com__rgm__daedalus__proto__message__pack(&daedalusMsg, ttMessage->msgContent);
  
  return ttMessage->msgLen;
}

/*int BuildAndSendVolatileToggleToAllDaedalus()
{
  // Build Active Volatile Symbols By Toggle Msg
  Com__Rgm__Daedalus__Proto__ActiveVolatileSymbolsByToggle toggle = COM__RGM__DAEDALUS__PROTO__ACTIVE_VOLATILE_SYMBOLS_BY_TOGGLE__INIT;
  toggle.has_isactive = 1;
  toggle.isactive = volatileMgmt.currentAllVolatileValue;
  
  // Build Daedalus msg
  Com__Rgm__Daedalus__Proto__Message daedalusMsg = COM__RGM__DAEDALUS__PROTO__MESSAGE__INIT;
  daedalusMsg.has_type = 1;
  daedalusMsg.type = COM__RGM__DAEDALUS__PROTO__MESSAGE__MESSAGE_TYPE__NOTIFICATION;
  daedalusMsg.activevolatilesymbolsbytoggle = &toggle;
  
  TraceLog(DEBUG_LEVEL, "Sending all volatile symbols by toggle to all Daedalus: '%s'\n",
    daedalusMsg.activevolatilesymbolsbytoggle->isactive == 1 ? "ON":"OFF");
  
  // Packing Daedalus msg
  t_TTMessage ttMessage;
  ttMessage.msgLen = com__rgm__daedalus__proto__message__get_packed_size(&daedalusMsg);
  com__rgm__daedalus__proto__message__pack(&daedalusMsg, ttMessage.msgContent);
  
  int retVal = SendMessageToAllDaedalus(&ttMessage);
  return retVal;
}*/

int BuildAlertConfigToDaedalus(void *message)
{
  // Init Daedalus msg and set msg type
  Com__Rgm__Daedalus__Proto__Message daedalusMsg = COM__RGM__DAEDALUS__PROTO__MESSAGE__INIT;
  daedalusMsg.has_type = 1;
  daedalusMsg.type = COM__RGM__DAEDALUS__PROTO__MESSAGE__MESSAGE_TYPE__TS_ALERT_CONFIG;
  
  Com__Rgm__Daedalus__Proto__TSAlertConfig alertConf = COM__RGM__DAEDALUS__PROTO__TSALERT_CONFIG__INIT;
  daedalusMsg.tsalertconfig = &alertConf;
  
  alertConf.has_pmexitedpositionthreshold = 1;
  alertConf.pmexitedpositionthreshold = AlertConf.pmExitedPositionThreshhold;
  
  alertConf.has_consecutivelossthreshold = 1;
  alertConf.consecutivelossthreshold = AlertConf.consecutiveLossThreshhold;
  
  alertConf.has_stucksthreshold = 1;
  alertConf.stucksthreshold = AlertConf.stucksThreshhold;
  
  alertConf.has_singleservertradesthreshold = 1;
  alertConf.singleservertradesthreshold = AlertConf.singleServerTradesThreshhold;
  
  alertConf.has_gtramountthreshold = 1;
  alertConf.gtramountthreshold = AlertConf.gtrAmountThreshhold * TEN_RAISE_TO_FOUR;
  
  alertConf.has_gtrsecondthreshold = 1;
  alertConf.gtrsecondthreshold = AlertConf.gtrSecondThreshhold;
  
  alertConf.has_incompletetradethreshold = 1;
  alertConf.incompletetradethreshold = AlertConf.incompleteTradeThreshhold;
  
  alertConf.has_incompletetraderepetitionthreshold = 1;
  alertConf.incompletetraderepetitionthreshold = AlertConf.repetitionTimeThreshhold;
  
  alertConf.has_positionwithoutorderthreshold = 1;
  alertConf.positionwithoutorderthreshold = AlertConf.positionWithoutOrderThreshhold;
  
  alertConf.has_pmorderratethreshold = 1;
  alertConf.pmorderratethreshold = AlertConf.pmOrderRateThreshhold;
  
  alertConf.has_buyingpoweramountthreshold = 1;
  alertConf.buyingpoweramountthreshold = AlertConf.buyingPowerAmountThreshhold * TEN_RAISE_TO_FOUR;
  
  alertConf.has_buyingpowersecondsthreshold = 1;
  alertConf.buyingpowersecondsthreshold = AlertConf.buyingPowerSecondsThreshhold;
  
  // No Trade Time
  const int maxItem = AlertConf.noTradeAlertMgmt.count;
  alertConf.n_notradetime = maxItem;
  
  if (maxItem == 0)
  {
    alertConf.notradetime = NULL;
    
    t_TTMessage *ttMessage = (t_TTMessage *)message;
    ttMessage->msgLen = com__rgm__daedalus__proto__message__get_packed_size(&daedalusMsg);
    com__rgm__daedalus__proto__message__pack(&daedalusMsg, ttMessage->msgContent);
    return ttMessage->msgLen;
  }
  
  Com__Rgm__Daedalus__Proto__NoTradeTime *pNoTradeTime[maxItem];
  Com__Rgm__Daedalus__Proto__NoTradeTime noTradeTime[maxItem];
    
  int i;
  for(i = 0; i < maxItem; i++)
  {
    com__rgm__daedalus__proto__no_trade_time__init(&noTradeTime[i]);
    pNoTradeTime[i] = &noTradeTime[i];
    
    noTradeTime[i].has_period = 1;
    noTradeTime[i].period = AlertConf.noTradeAlertMgmt.itemList[i].startInSecond;
    
    noTradeTime[i].has_value = 1;
    noTradeTime[i].value = AlertConf.noTradeAlertMgmt.itemList[i].durationInMinute;
  }
  
  alertConf.notradetime = pNoTradeTime;
  
/*#ifndef RELEASE_MODE
  TraceLog(DEBUG_LEVEL, "--->pmexitedpositionthreshold          = %d\n", alertConf.pmexitedpositionthreshold);
  TraceLog(DEBUG_LEVEL, "--->consecutivelossthreshold           = %d\n", alertConf.consecutivelossthreshold);
  TraceLog(DEBUG_LEVEL, "--->stucksthreshold                    = %d\n", alertConf.stucksthreshold);
  TraceLog(DEBUG_LEVEL, "--->singleservertradesthreshold        = %d\n", alertConf.singleservertradesthreshold);
  TraceLog(DEBUG_LEVEL, "--->gtramountthreshold                 = %ld\n", alertConf.gtramountthreshold);
  TraceLog(DEBUG_LEVEL, "--->gtrsecondthreshold                 = %d\n", alertConf.gtrsecondthreshold);
  TraceLog(DEBUG_LEVEL, "--->incompletetradethreshold           = %d\n", alertConf.incompletetradethreshold);
  TraceLog(DEBUG_LEVEL, "--->incompletetraderepetitionthreshold = %d\n", alertConf.incompletetraderepetitionthreshold);
  TraceLog(DEBUG_LEVEL, "--->positionwithoutorderthreshold      = %d\n", alertConf.positionwithoutorderthreshold);
  TraceLog(DEBUG_LEVEL, "--->pmorderratethreshold               = %d\n", alertConf.pmorderratethreshold);
  TraceLog(DEBUG_LEVEL, "--->buyingpoweramountthreshold         = %ld\n", alertConf.buyingpoweramountthreshold);
  TraceLog(DEBUG_LEVEL, "--->buyingpowersecondsthreshold        = %d\n", alertConf.buyingpowersecondsthreshold);
  TraceLog(DEBUG_LEVEL, "--->n_notradetime=%d\n", alertConf.n_notradetime);
  
  // int i;
  for (i = 0; i < alertConf.n_notradetime; i++)
  {
    TraceLog(DEBUG_LEVEL, "--->%d: period=%d value=%d\n", i, alertConf.notradetime[i]->period, alertConf.notradetime[i]->value);
  }
#endif*/
  
  t_TTMessage *ttMessage = (t_TTMessage *)message;
  ttMessage->msgLen = com__rgm__daedalus__proto__message__get_packed_size(&daedalusMsg);
  com__rgm__daedalus__proto__message__pack(&daedalusMsg, ttMessage->msgContent);
  return ttMessage->msgLen;
}

int BuildLoginStatusToDaedalus(void *message, char trader[MAX_LINE_LEN], int state)
{
  // Init Daedalus msg and set msg type
  Com__Rgm__Daedalus__Proto__Message daedalusMsg = COM__RGM__DAEDALUS__PROTO__MESSAGE__INIT;
  daedalusMsg.has_type = 1;
  daedalusMsg.type = COM__RGM__DAEDALUS__PROTO__MESSAGE__MESSAGE_TYPE__LOGIN_STATUS;
  
  Com__Rgm__Daedalus__Proto__LoginStatus loginStatus = COM__RGM__DAEDALUS__PROTO__LOGIN_STATUS__INIT;
  daedalusMsg.loginstatus = &loginStatus;
  
  loginStatus.trader = trader;
  loginStatus.has_state = 1;
  if (state == SUCCESS)
  {
    loginStatus.state = COM__RGM__DAEDALUS__PROTO__LOGIN_STATE__LoginSuccess;
  }
  else if (state == USER_LOGGED)
  {
    loginStatus.state = COM__RGM__DAEDALUS__PROTO__LOGIN_STATE__UserLogged;
  }
  else if (state == USER_PASSWORD_INCORRECT)
  {
    loginStatus.state = COM__RGM__DAEDALUS__PROTO__LOGIN_STATE__UserPasswordIncorrect;
  }
  
  loginStatus.has_tradingsystem = 1;
  loginStatus.tradingsystem = COM__RGM__DAEDALUS__PROTO__TRADING_SYSTEM__BlinkBox;
  
  loginStatus.has_region = 1;
  loginStatus.region = COM__RGM__DAEDALUS__PROTO__REGION__US;
  
// #ifndef RELEASE_MODE
  // TraceLog(DEBUG_LEVEL, "--->login status: trader='%s' state=%d(success=1,logged=3,incorrectPass=2)\n",
    // daedalusMsg.loginstatus->trader, daedalusMsg.loginstatus->state);
// #endif

  t_TTMessage *ttMessage = (t_TTMessage *)message;
  ttMessage->msgLen = com__rgm__daedalus__proto__message__get_packed_size(&daedalusMsg);
  com__rgm__daedalus__proto__message__pack(&daedalusMsg, ttMessage->msgContent);
  return ttMessage->msgLen;
}

int BuildOperatorInfoToDaedalus(void *message)
{
  // Init Daedalus msg and set msg type
  Com__Rgm__Daedalus__Proto__Message daedalusMsg = COM__RGM__DAEDALUS__PROTO__MESSAGE__INIT;
  daedalusMsg.has_type = 1;
  daedalusMsg.type = COM__RGM__DAEDALUS__PROTO__MESSAGE__MESSAGE_TYPE__OPERATOR_INFO;
  
  const int maxTrader = traderCollection.numberTrader;
  daedalusMsg.n_operatorinfo = maxTrader;
  
  if (maxTrader == 0)
  {
    daedalusMsg.operatorinfo = NULL;
    
    t_TTMessage *ttMessage = (t_TTMessage *)message;
    ttMessage->msgLen = com__rgm__daedalus__proto__message__get_packed_size(&daedalusMsg);
    com__rgm__daedalus__proto__message__pack(&daedalusMsg, ttMessage->msgContent);
    return ttMessage->msgLen;
  }
  
  Com__Rgm__Daedalus__Proto__OperatorInfo *pOperators[maxTrader];
  Com__Rgm__Daedalus__Proto__OperatorInfo operators[maxTrader];
  
  int traderIndex;
  for (traderIndex = 0; traderIndex < maxTrader; traderIndex++)
  {
    com__rgm__daedalus__proto__operator_info__init(&operators[traderIndex]);
    pOperators[traderIndex] = &operators[traderIndex];
    
    operators[traderIndex].name = traderCollection.traderStatusInfo[traderIndex].userName;

    operators[traderIndex].has_isonline = 1;
    operators[traderIndex].isonline = traderCollection.traderStatusInfo[traderIndex].status;
    
    operators[traderIndex].has_type = 1;
    if (traderCollection.traderStatusInfo[traderIndex].status == ACTIVE)
    {
      operators[traderIndex].type = COM__RGM__DAEDALUS__PROTO__OPERATOR_TYPE__ActiveOperator;
    }
    else if (traderCollection.traderStatusInfo[traderIndex].status == ACTIVE_ASSISTANT)
    {
      operators[traderIndex].type = COM__RGM__DAEDALUS__PROTO__OPERATOR_TYPE__ActiveAssistOperator;
    }
    else // MONITORING
    {
      operators[traderIndex].type = COM__RGM__DAEDALUS__PROTO__OPERATOR_TYPE__MonitoringOperator;
    }
    
    operators[traderIndex].role = "Operator"; //look like this role is gotten from LDAP info when authenticating
  }
  
  daedalusMsg.operatorinfo = pOperators;
  
// #ifndef RELEASE_MODE
  // TraceLog(DEBUG_LEVEL, "--->n_operatorinfo=%d\n", daedalusMsg.n_operatorinfo);
  // int i;
  // for (i = 0; i < maxTrader; i++)
  // {
    // TraceLog(DEBUG_LEVEL, "--->%d: '%s' online=%d type='%d'(act=1,assist=2,monitor=3) role='%s'\n",
      // i, daedalusMsg.operatorinfo[i]->name, daedalusMsg.operatorinfo[i]->isonline,
      // daedalusMsg.operatorinfo[i]->type, daedalusMsg.operatorinfo[i]->role);
  // }
// #endif
  
  t_TTMessage *ttMessage = (t_TTMessage *)message;
  ttMessage->msgLen = com__rgm__daedalus__proto__message__get_packed_size(&daedalusMsg);
  com__rgm__daedalus__proto__message__pack(&daedalusMsg, ttMessage->msgContent);
  return ttMessage->msgLen;
}

int BuildAndSendTSExchangeConfigToDaedalus(void *pConnection, int tsIndex)
{
  // Init Daedalus msg and set msg type
  Com__Rgm__Daedalus__Proto__Message daedalusMsg = COM__RGM__DAEDALUS__PROTO__MESSAGE__INIT;
  daedalusMsg.has_type = 1;
  daedalusMsg.type = COM__RGM__DAEDALUS__PROTO__MESSAGE__MESSAGE_TYPE__EXCHANGE_CONFIG;
  
  // Create and point to exchange conf
  Com__Rgm__Daedalus__Proto__ExchangeConfig exchangeConf = COM__RGM__DAEDALUS__PROTO__EXCHANGE_CONFIG__INIT;
  exchangeConf.hostname = tradeServersInfo.config[tsIndex].description;
  daedalusMsg.exchangeconfig = &exchangeConf;
  
  // Get the info of exhange entry
  Com__Rgm__Daedalus__Proto__ExchangeConfig__ExchangeEntry *pExchange[MAX_EXCHANGE];
  Com__Rgm__Daedalus__Proto__ExchangeConfig__ExchangeEntry exchanges[MAX_EXCHANGE];
  
  // Init data for exchange entry info
  char exchangeCodeList[MAX_EXCHANGE][2];
  memset(exchangeCodeList, 0, sizeof(exchangeCodeList));
  char venueDesc[MAX_EXCHANGE][MAX_LINE_LEN];
  memset(venueDesc, 0, sizeof(venueDesc));
  
  int i;
  for (i = 0; i < MAX_EXCHANGE; i++)
  {
    com__rgm__daedalus__proto__exchange_config__exchange_entry__init(&exchanges[i]);
    pExchange[i] = &exchanges[i];
    
    exchangeCodeList[i][0] = ExchangeStatusList[tsIndex][i].exchangeId;
    exchanges[i].exchangecode = exchangeCodeList[i];
    
    strncpy(venueDesc[i], GetExchangeNameFromCode(exchangeCodeList[i][0]), MAX_LINE_LEN);
    exchanges[i].description = venueDesc[i];
    
    exchanges[i].has_cqsvisible = 1;
    exchanges[i].cqsvisible = ExchangeStatusList[tsIndex][i].status[CQS_SOURCE].visible;
    
    exchanges[i].has_cqsenabled = 1;
    exchanges[i].cqsenabled = ExchangeStatusList[tsIndex][i].status[CQS_SOURCE].enable;
    
    exchanges[i].has_uqdfvisible = 1;
    exchanges[i].uqdfvisible = ExchangeStatusList[tsIndex][i].status[UQDF_SOURCE].visible;
    
    exchanges[i].has_uqdfenabled = 1;
    exchanges[i].uqdfenabled = ExchangeStatusList[tsIndex][i].status[UQDF_SOURCE].enable;
    
    exchanges[i].n_oeclist = 0;
    exchanges[i].oeclist = NULL;
    exchanges[i].has_defaultoec = 0;
  }
  
  // Point exchange list to static memory
  exchangeConf.n_exchanges = MAX_EXCHANGE;
  exchangeConf.exchanges = pExchange;
  
#ifndef RELEASE_MODE
  // TraceLog(DEBUG_LEVEL, "Sending Self Help to Daedalus\n");
  // for (i = 0; i < daedalusMsg.exchangeconfig->n_exchanges; i++)
  // {
    // TraceLog(DEBUG_LEVEL, "--->'%s' '%s' '%s' cqs(vis:%d,ena:%d) uqdf(vis:%d,ena:%d)\n",
      // daedalusMsg.exchangeconfig->hostname,
      // daedalusMsg.exchangeconfig->exchanges[i]->exchangecode,
      // daedalusMsg.exchangeconfig->exchanges[i]->description,
      // daedalusMsg.exchangeconfig->exchanges[i]->cqsvisible,
      // daedalusMsg.exchangeconfig->exchanges[i]->cqsenabled,
      // daedalusMsg.exchangeconfig->exchanges[i]->uqdfvisible,
      // daedalusMsg.exchangeconfig->exchanges[i]->uqdfenabled);
  // }
#endif
  
  t_TTMessage ttMessage;
  ttMessage.msgLen = com__rgm__daedalus__proto__message__get_packed_size(&daedalusMsg);
  com__rgm__daedalus__proto__message__pack(&daedalusMsg, ttMessage.msgContent);
  return SendDaedalusMessage(pConnection, &ttMessage);
}

int BuildAndSendPMExchangeConfigToDaedalus(void *pConnection)
{
  // Init Daedalus msg and set msg type
  Com__Rgm__Daedalus__Proto__Message daedalusMsg = COM__RGM__DAEDALUS__PROTO__MESSAGE__INIT;
  daedalusMsg.has_type = 1;
  daedalusMsg.type = COM__RGM__DAEDALUS__PROTO__MESSAGE__MESSAGE_TYPE__EXCHANGE_CONFIG;
  
  // Create and point to exchange conf
  Com__Rgm__Daedalus__Proto__ExchangeConfig exchangeConf = COM__RGM__DAEDALUS__PROTO__EXCHANGE_CONFIG__INIT;
  exchangeConf.hostname = PM_SERVER_NAME;
  daedalusMsg.exchangeconfig = &exchangeConf;
  
  // Get the info of exhange entry
  Com__Rgm__Daedalus__Proto__ExchangeConfig__ExchangeEntry *pExchange[MAX_EXCHANGE];
  Com__Rgm__Daedalus__Proto__ExchangeConfig__ExchangeEntry exchanges[MAX_EXCHANGE];
  
  // Init data for exchange entry info
  char exchangeCodeList[MAX_EXCHANGE][2];
  memset(exchangeCodeList, 0, sizeof(exchangeCodeList));
  
  char venueDesc[MAX_EXCHANGE][MAX_LINE_LEN];
  memset(venueDesc, 0, sizeof(venueDesc));
  
  Com__Rgm__Daedalus__Proto__OEC oecList[PM_MAX_ORDER_CONNECTIONS]; //ARCA DIRECT and RASH
  oecList[0] = COM__RGM__DAEDALUS__PROTO__OEC__RASH_OEC;
  oecList[1] = COM__RGM__DAEDALUS__PROTO__OEC__ARCA_DIRECT_OEC;
  
  int i;
  for (i = 0; i < MAX_EXCHANGE; i++)
  {
    com__rgm__daedalus__proto__exchange_config__exchange_entry__init(&exchanges[i]);
    pExchange[i] = &exchanges[i];
    
    exchangeCodeList[i][0] = PMExchangeStatusList[i].exchangeId;
    exchanges[i].exchangecode = exchangeCodeList[i];
    
    strncpy(venueDesc[i], GetExchangeNameFromCode(exchangeCodeList[i][0]), MAX_LINE_LEN);
    exchanges[i].description = venueDesc[i];
    
    exchanges[i].has_cqsvisible = 1;
    exchanges[i].cqsvisible = PMExchangeStatusList[i].status[CQS_SOURCE].visible;
    
    exchanges[i].has_cqsenabled = 1;
    exchanges[i].cqsenabled = PMExchangeStatusList[i].status[CQS_SOURCE].enable;
    
    exchanges[i].has_uqdfvisible = 1;
    exchanges[i].uqdfvisible = PMExchangeStatusList[i].status[UQDF_SOURCE].visible;
    
    exchanges[i].has_uqdfenabled = 1;
    exchanges[i].uqdfenabled = PMExchangeStatusList[i].status[UQDF_SOURCE].enable;
    
    exchanges[i].n_oeclist = PM_MAX_ORDER_CONNECTIONS;
    exchanges[i].oeclist = oecList;
    
    exchanges[i].has_defaultoec = 1;
    if (strcmp("RASH", PMExchangeStatusList[i].ECNToPlace) == 0)
    {
      exchanges[i].defaultoec = COM__RGM__DAEDALUS__PROTO__OEC__RASH_OEC;
    }
    else if ( (strcmp("ARCA_DIRECT", PMExchangeStatusList[i].ECNToPlace) == 0)
            || (strcmp("ARCA_FIX", PMExchangeStatusList[i].ECNToPlace) == 0) )
    {
      exchanges[i].defaultoec = COM__RGM__DAEDALUS__PROTO__OEC__ARCA_DIRECT_OEC;
    }
  }
  
  // Point exchange list to static memory
  exchangeConf.n_exchanges = MAX_EXCHANGE;
  exchangeConf.exchanges = pExchange;
  
/*#ifndef RELEASE_MODE
  for (i = 0; i < daedalusMsg.exchangeconfig->n_exchanges; i++)
  {
    TraceLog(DEBUG_LEVEL, "--->'%s' '%s' '%s' cqs(vis:%d,ena:%d) uqdf(vis:%d,ena:%d) oecList=%d defaultoec=%d\n",
      daedalusMsg.exchangeconfig->hostname,
      daedalusMsg.exchangeconfig->exchanges[i]->exchangecode,
      daedalusMsg.exchangeconfig->exchanges[i]->description,
      daedalusMsg.exchangeconfig->exchanges[i]->cqsvisible,
      daedalusMsg.exchangeconfig->exchanges[i]->cqsenabled,
      daedalusMsg.exchangeconfig->exchanges[i]->uqdfvisible,
      daedalusMsg.exchangeconfig->exchanges[i]->uqdfenabled,
      daedalusMsg.exchangeconfig->exchanges[i]->n_oeclist,
      daedalusMsg.exchangeconfig->exchanges[i]->defaultoec
      );
  }
#endif*/
  
  t_TTMessage ttMessage;
  ttMessage.msgLen = com__rgm__daedalus__proto__message__get_packed_size(&daedalusMsg);
  com__rgm__daedalus__proto__message__pack(&daedalusMsg, ttMessage.msgContent);
  return SendDaedalusMessage(pConnection, &ttMessage);
}

int BuildResponseSendNewOrderToDaedalus(void *message, t_SendNewOrder *newOrder, int sendId, int reason, char *reasonText)
{
  TraceLog(DEBUG_LEVEL, "Response to 'manual order request from Daedalus': sendId: %d, orderId: %d, response: %d\n", sendId, newOrder->orderId, reason);
  
  // Build Manual Order Response
  Com__Rgm__Daedalus__Proto__ManualOrderResponse response = COM__RGM__DAEDALUS__PROTO__MANUAL_ORDER_RESPONSE__INIT;
  
  // SendID
  response.has_sendid = 1;
  response.sendid = sendId;
  
  // Symbol
  response.symbol = newOrder->symbol;
  
  // Side: invert the side from Manual Order Request from Daedalus
  response.has_side = 1;
  response.side = (newOrder->side == BID_SIDE) ? COM__RGM__DAEDALUS__PROTO__SIDE__Ask : COM__RGM__DAEDALUS__PROTO__SIDE__Bid;
  
  // Size (shares)
  response.has_size = 1;
  response.size = newOrder->shares;
  
  // Max Floor
  response.has_maxfloor = 1;
  response.maxfloor = newOrder->maxFloor;
  
  // Price
  response.has_price = 1;
  response.price = newOrder->price * TEN_RAISE_TO_FOUR;
  
  // ECN
  response.has_oec = 1;
  response.oec = (newOrder->ECNId == TYPE_NASDAQ_RASH) ? COM__RGM__DAEDALUS__PROTO__OEC__RASH_OEC : COM__RGM__DAEDALUS__PROTO__OEC__ARCA_DIRECT_OEC;
  
  // TIF
  response.has_timeinforce = 1;
  response.timeinforce = COM__RGM__DAEDALUS__PROTO__TIME_IN_FORCE__DAY;
  
  // PEG Difference
  response.has_pegdifference = 1;
  response.pegdifference = newOrder->pegDef;
  
  // OrderID
  char _orderId[20] = "\0";
  sprintf(_orderId, "%u", newOrder->orderId);
  response.orderid = _orderId;
  
  // Trading account, newOrder->tradingAccount: is account index
  // char account[2] = "\0";
  // account[0] = newOrder->tradingAccount + FIRST_ACCOUNT_ASCII;
  // response.account = account;
  response.account = TradingAccount.RASH[newOrder->tradingAccount];
  
  // Reason Code
  // -1: could not send order
  // -2: Failed to check with last price, ask user if he wants to force?
  // >=0: Manual Order sent successfully
  response.has_reason = 1;
  if (reason == -1)
  {
    response.reason = COM__RGM__DAEDALUS__PROTO__MANUAL_ORDER_RESPONSE_TYPE__CanNotSend;
  }
  else if (reason == -2)
  {
    response.reason = COM__RGM__DAEDALUS__PROTO__MANUAL_ORDER_RESPONSE_TYPE__NeedToConfirm;
  }
  else
  {
    response.reason = COM__RGM__DAEDALUS__PROTO__MANUAL_ORDER_RESPONSE_TYPE__SendSuccessfully;
  }
  response.reasontext = reasonText;
  
  // Hostname
  response.hostname = PM_SERVER_NAME;

  // Build Daedalus msg
  Com__Rgm__Daedalus__Proto__Message daedalusMsg = COM__RGM__DAEDALUS__PROTO__MESSAGE__INIT;
  daedalusMsg.has_type = 1;
  daedalusMsg.type = COM__RGM__DAEDALUS__PROTO__MESSAGE__MESSAGE_TYPE__MANUAL_ORDER_RESPONSE;
  daedalusMsg.manualorderresponse = &response;
  
/*#ifndef RELEASE_MODE
  TraceLog(DEBUG_LEVEL, "--->'%s' sendid=%d side=%d vol=%d floor=%d price=%ld oec=%d orderid=%d tif=%d pef=%lf account='%s' reason=%d, text='%s'\n",
                response.symbol, response.sendid, response.side, response.size, response.maxfloor,
                response.price, response.oec, response.orderid, response.timeinforce,
                response.pegdifference, response.account, response.reason, response.reasontext);
#endif*/

  // Packing Daedalus msg
  t_TTMessage *ttMessage = (t_TTMessage *)message;
  ttMessage->msgLen = com__rgm__daedalus__proto__message__get_packed_size(&daedalusMsg);
  com__rgm__daedalus__proto__message__pack(&daedalusMsg, ttMessage->msgContent);
  return ttMessage->msgLen;
}

int BuildAndSendASOpenOrderToDaedalus(void *pConnection)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  // Check connection between AS and Daedalus
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }

  int ttIndex = connection->index;
  
  // Build Order History
  Com__Rgm__Daedalus__Proto__OrderInfo orderInfoList[MAX_OPEN_ORDER_SEND];
  
  int countOpenOrderSend = 0;
  int indexOpenOrder;
  int symbolIndex;

  t_OpenOrderInfo orderSumaryList[MAX_OPEN_ORDER_SEND];
  
  /*  Filter PM messages to anti-flood TT
  
  // The following will be sent to TT:
  orderStatusIndex.Sent = 0;
  orderStatusIndex.Live = 1;
  orderStatusIndex.Rejected = 2;
  orderStatusIndex.BrokenTrade = 4
  orderStatusIndex.CXLSent = 6;
  orderStatusIndex.CancelPending = 9;
  left shares < shares
      
  // The following will be sent limited, only newest will be sent:
  orderStatusIndex.CanceledByECN = 5;
  orderStatusIndex.CancelRejected = 8;
  orderStatusIndex.CanceledByUser = 10;
  */

  char _orderID[MAX_OPEN_ORDER_SEND][20];
  memset(_orderID, 0, MAX_OPEN_ORDER_SEND * 20);
  int numPMOrdersWillSend = 1;
  int tempStatus;
  for (indexOpenOrder = 0; indexOpenOrder < asOpenOrder.countOpenOrder; indexOpenOrder++)
  {
    if (asOpenOrder.orderCollection[indexOpenOrder].isUpdated[ttIndex] == OPEN_ORDER_UPDATED)
    {
      // The following commented code will filter to ignore most PM canceled messages
      if (PMtoTTCancelLimit != -1)
      {
        // Check if it is PM message
        if ( (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.clOrdId >= 500000) &&
           (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.tsId == MANUAL_ORDER) )
        {
          tempStatus = asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status;
          if (
            ((tempStatus == orderStatusIndex.CanceledByUser) || (tempStatus == orderStatusIndex.CanceledByECN)) &&
            (asOpenOrder.orderCollection[indexOpenOrder].countSent[ttIndex] == 0)
            )
          {
            // Send up to the limit!
            if (numPMOrdersWillSend <= PMtoTTCancelLimit)
            {
              // Send
              numPMOrdersWillSend++;
            }
            else
            {
              //Do not send
              asOpenOrder.orderCollection[indexOpenOrder].isUpdated[ttIndex] = OPEN_ORDER_SENT;
              // TraceLog(DEBUG_LEVEL, "--->numPMOrdersWillSend=%d PMtoTTCancelLimit=%d\n", numPMOrdersWillSend, PMtoTTCancelLimit);
              continue;
            }
          }
        }
      }
      
      // Check for fast market mode
      // If in the Fast Market Mode, Order History will be sent limited, Only Live orders will be sent
      if (FastMarketMgmt.mode == FAST_MARKET_MODE)
      {
        tempStatus = asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status;
        if (
          (tempStatus == orderStatusIndex.CanceledByECN)    ||
          (tempStatus == orderStatusIndex.Closed)       ||
          (tempStatus == orderStatusIndex.Rejected)       ||
          (tempStatus == orderStatusIndex.CanceledByUser)   ||
          (tempStatus == orderStatusIndex.CancelRejected) ||
          (asOpenOrder.orderCollection[indexOpenOrder].isBroken != BROKEN_TRADE_NONE)
          )
        {
          // These are Closed orders:
          // We have to send to TT when this order is currently displayed on TT --> so it will be removed from TT!
          // We will not send to TT if this order has never been sent to TT before!
          // "countSent" helps us to know whether this has been sent to TT before
          if (asOpenOrder.orderCollection[indexOpenOrder].countSent[ttIndex] == 0)
          {
            // We won't send in this case
            asOpenOrder.orderCollection[indexOpenOrder].isUpdated[ttIndex] = OPEN_ORDER_SENT;
            // TraceLog(DEBUG_LEVEL, "--->countSent=0, continue\n");
            continue;
          }
        }
      }
      
      if (countOpenOrderSend == MAX_OPEN_ORDER_SEND)
      {
        // TraceLog(DEBUG_LEVEL, "--->countOpenOrderSend=%d == MAX_OPEN_ORDER_SEND\n");
        break;
      }

      //Here are messages from TS's, AS, and PM (filtered)
      asOpenOrder.orderCollection[indexOpenOrder].isUpdated[ttIndex] = OPEN_ORDER_SENT;
      asOpenOrder.orderCollection[indexOpenOrder].countSent[ttIndex]++;
      
      // order summary
      // memcpy((unsigned char *)&orderSummary, (unsigned char *)&asOpenOrder.orderCollection[indexOpenOrder].orderSummary, sizeof(t_OpenOrderInfo));
      memcpy((unsigned char *)&orderSumaryList[countOpenOrderSend], (unsigned char *)&asOpenOrder.orderCollection[indexOpenOrder].orderSummary, sizeof(t_OpenOrderInfo));
      
      if (asOpenOrder.orderCollection[indexOpenOrder].isBroken != BROKEN_TRADE_NONE)
      {
        orderSumaryList[countOpenOrderSend].status = orderStatusIndex.BrokenTrade;
      }
      
      // Init Daedalus Order Info
      com__rgm__daedalus__proto__order_info__init(&orderInfoList[countOpenOrderSend]);
      
      // price
      orderInfoList[countOpenOrderSend].has_price = 1;
      orderInfoList[countOpenOrderSend].price = orderSumaryList[countOpenOrderSend].price * TEN_RAISE_TO_FOUR;
      
      // avg price
      orderInfoList[countOpenOrderSend].has_averageprice = 1;
      orderInfoList[countOpenOrderSend].averageprice = orderSumaryList[countOpenOrderSend].avgPrice * TEN_RAISE_TO_FOUR;
      
      // symbol
      orderInfoList[countOpenOrderSend].symbol = orderSumaryList[countOpenOrderSend].symbol;
      
      // side
      orderInfoList[countOpenOrderSend].has_side = 1;
      if (orderSumaryList[countOpenOrderSend].side == SELL_TYPE ||
        orderSumaryList[countOpenOrderSend].side == SHORT_SELL_TYPE)
      {
        orderInfoList[countOpenOrderSend].side = COM__RGM__DAEDALUS__PROTO__SIDE__Ask;
      }
      else
      {
        orderInfoList[countOpenOrderSend].side = COM__RGM__DAEDALUS__PROTO__SIDE__Bid;
      }
      
      // shares
      orderInfoList[countOpenOrderSend].has_shares = 1;
      orderInfoList[countOpenOrderSend].shares = orderSumaryList[countOpenOrderSend].shares;
      
      // left shares
      orderInfoList[countOpenOrderSend].has_leftshares = 1;
      orderInfoList[countOpenOrderSend].leftshares = orderSumaryList[countOpenOrderSend].leftShares;
    
      // OEC
      orderInfoList[countOpenOrderSend].has_oec = 1;
      orderInfoList[countOpenOrderSend].oec = AS_CENTRALIZED_ORDER_TYPE_TO_OEC_MAPPING[orderSumaryList[countOpenOrderSend].ECNId - 100];
      
      // orderID
      sprintf(_orderID[countOpenOrderSend], "%u", orderSumaryList[countOpenOrderSend].clOrdId);
      orderInfoList[countOpenOrderSend].orderid = _orderID[countOpenOrderSend];
      
      // status
      orderInfoList[countOpenOrderSend].has_status = 1;
      if (orderSumaryList[countOpenOrderSend].status > -1 &&
        orderSumaryList[countOpenOrderSend].status <= orderStatusIndex.CanceledByUser)
      {
        orderInfoList[countOpenOrderSend].status = AS_ORDER_STATUS_TO_DAEDALUS_ORDER_STATUS_MAPPING[orderSumaryList[countOpenOrderSend].status];
      }
      else
      {
        orderInfoList[countOpenOrderSend].status = COM__RGM__DAEDALUS__PROTO__ORDER_STATUS__InvalidStatus;
      }
      
      // TIF
      orderInfoList[countOpenOrderSend].has_timeinforce = 1;
      orderInfoList[countOpenOrderSend].timeinforce = (orderSumaryList[countOpenOrderSend].tif == DAY_TYPE) ? COM__RGM__DAEDALUS__PROTO__TIME_IN_FORCE__DAY : COM__RGM__DAEDALUS__PROTO__TIME_IN_FORCE__IOC;
      
      // hostname
      orderInfoList[countOpenOrderSend].hostname = PM_SERVER_NAME;
      
      // timestamp is 1_06:48:45:123456, whereas 1 is tsID, _ is space
      // time is nanoseconds since 1970
      orderInfoList[countOpenOrderSend].has_time = 1;
      orderInfoList[countOpenOrderSend].time = GetNanoSecondsFromData(&orderSumaryList[countOpenOrderSend].timestamp[2]);
      // TraceLog(DEBUG_LEVEL, "-->timestamp '%s' nano=%ld\n", &orderSumaryList[countOpenOrderSend].timestamp[2], orderInfoList[countOpenOrderSend].time);
      
      // account
      // orderSumaryList[countOpenOrderSend].account is always from 0-->3
      orderInfoList[countOpenOrderSend].account = ACCOUNT_STRING_FROM_ACCOUNT_INDEX_MAPPING[(int)orderSumaryList[countOpenOrderSend].account];
      
      // order details
      // orderInfoList[countOpenOrderSend].details = ;
      
      // position key
      symbolIndex = GetStockSymbolIndex(orderSumaryList[countOpenOrderSend].symbol);
      orderInfoList[countOpenOrderSend].positionkey = RMList.collection[(int)orderSumaryList[countOpenOrderSend].account][symbolIndex].positionKey;
      
      //TraceLog(DEBUG_LEVEL, "clOrdId = %d, tradeType = %d\n", asOpenOrder.orderCollection[indexOpenOrder].orderSummary.clOrdId, asOpenOrder.orderCollection[indexOpenOrder].orderSummary.tradeType);

      // Increase count open order will send
      countOpenOrderSend++;
    }
  }
  
  // TraceLog(DEBUG_LEVEL, "--->countOpenOrderSend=%d\n", countOpenOrderSend);
  if (countOpenOrderSend == 0)
  {
    return SUCCESS;
  }
  
  const int maxItem = countOpenOrderSend;
  Com__Rgm__Daedalus__Proto__OrderInfo *pOrderInfoList[maxItem];
  for (indexOpenOrder = 0; indexOpenOrder < maxItem; indexOpenOrder++)
  {
    pOrderInfoList[indexOpenOrder] = &orderInfoList[indexOpenOrder];
  }
  
  // Build Daedalus msg
  Com__Rgm__Daedalus__Proto__Message daedalusMsg = COM__RGM__DAEDALUS__PROTO__MESSAGE__INIT;
  daedalusMsg.has_type = 1;
  daedalusMsg.type = COM__RGM__DAEDALUS__PROTO__MESSAGE__MESSAGE_TYPE__ORDER_INFO;
  daedalusMsg.orderinfo = pOrderInfoList;
  daedalusMsg.n_orderinfo = maxItem;
  
#ifndef RELEASE_MODE
  /*TraceLog(DEBUG_LEVEL, "--->Open Order Info: n_orderinfo=%d\n", daedalusMsg.n_orderinfo);
  int jj;
  for (jj = 0; jj < daedalusMsg.n_orderinfo; jj++)
  {
    TraceLog(DEBUG_LEVEL, "--->'%s' '%s' price=%ld avg=%ld side=%d vol=%d lvVol=%d oec=%d orderId=%d status=%d tif=%d time=%d account='%s'\n",
      daedalusMsg.orderinfo[jj]->hostname,
      daedalusMsg.orderinfo[jj]->symbol,
      daedalusMsg.orderinfo[jj]->price,
      daedalusMsg.orderinfo[jj]->averageprice,
      daedalusMsg.orderinfo[jj]->side,
      daedalusMsg.orderinfo[jj]->shares,
      daedalusMsg.orderinfo[jj]->leftshares,
      daedalusMsg.orderinfo[jj]->oec,
      daedalusMsg.orderinfo[jj]->orderid,
      daedalusMsg.orderinfo[jj]->status,
      daedalusMsg.orderinfo[jj]->timeinforce,
      daedalusMsg.orderinfo[jj]->time,
      daedalusMsg.orderinfo[jj]->account
      );
  }*/
#endif

  // Packing Daedalus msg
  t_TTMessage ttMessage;
  ttMessage.msgLen = com__rgm__daedalus__proto__message__get_packed_size(&daedalusMsg);
  com__rgm__daedalus__proto__message__pack(&daedalusMsg, ttMessage.msgContent);

  //TraceLog(DEBUG_LEVEL, "Send open orders: countOrderPlacedSend = %d, totalOrderPlaced = %d\n", countOpenOrderSend, asOpenOrder.countOpenOrder);

  // Send message to Daedalus
  return SendDaedalusMessage(connection, &ttMessage);
}

int BuildBookIdleWarningToDaedalus(void *message, int tsPMIndex, int ECNIndex, int groupID)
{
  // Build MDC Idle Warning
  Com__Rgm__Daedalus__Proto__MdcIdleWarning mdcIdle = COM__RGM__DAEDALUS__PROTO__MDC_IDLE_WARNING__INIT;
  
  // hostname and MDC
  if (tsPMIndex == 99) //Warning from PM
  {
    mdcIdle.hostname = PM_SERVER_NAME;
    
    mdcIdle.has_mdc = 1;
    mdcIdle.mdc = PM_BOOK_INDEX_TO_DAEDALUS_MDC_MAPPING[ECNIndex];
  }
  else  //Warning from TS
  {
    mdcIdle.hostname = tradeServersInfo.config[tsPMIndex].description;
    
    mdcIdle.has_mdc = 1;
    mdcIdle.mdc = TS_BOOK_INDEX_TO_DAEDALUS_MDC_MAPPING[ECNIndex];
  }
  
  // Group ID
  mdcIdle.has_groupid = 1;
  mdcIdle.groupid = groupID;

  // Build Daedalus msg
  Com__Rgm__Daedalus__Proto__Message daedalusMsg = COM__RGM__DAEDALUS__PROTO__MESSAGE__INIT;
  daedalusMsg.has_type = 1;
  daedalusMsg.type = COM__RGM__DAEDALUS__PROTO__MESSAGE__MESSAGE_TYPE__MDC_IDLE_WARNING;
  daedalusMsg.mdcidlewarning = &mdcIdle;
  
/*#ifndef RELEASE_MODE
  TraceLog(DEBUG_LEVEL, "--->book idle: '%s' mdc=%d group=%d\n", mdcIdle.hostname, mdcIdle.mdc, mdcIdle.groupid);
#endif*/

  // Packing Daedalus msg
  t_TTMessage *ttMessage = (t_TTMessage *)message;
  ttMessage->msgLen = com__rgm__daedalus__proto__message__get_packed_size(&daedalusMsg);
  com__rgm__daedalus__proto__message__pack(&daedalusMsg, ttMessage->msgContent);
  return ttMessage->msgLen;
}

int BuildASGlobalConfigToDaedalus(void *message)
{
  // Build AS Global Config
  Com__Rgm__Daedalus__Proto__ASGlobalConfig asConfig = COM__RGM__DAEDALUS__PROTO__ASGLOBAL_CONFIG__INIT;
  
  asConfig.has_maxstuckpositions = 1;
  asConfig.maxstuckpositions = traderToolsInfo.globalConf.maxStuck;
  
  asConfig.has_maxconsecutivelosers = 1;
  asConfig.maxconsecutivelosers = traderToolsInfo.globalConf.maxLoser;
  
  asConfig.has_buyingpower1 = 1;
  asConfig.buyingpower1 = traderToolsInfo.globalConf.buyingPower[FIRST_ACCOUNT] * TEN_RAISE_TO_FOUR;
  
  asConfig.has_buyingpower2 = 1;
  asConfig.buyingpower2 = traderToolsInfo.globalConf.buyingPower[SECOND_ACCOUNT] * TEN_RAISE_TO_FOUR;
  
  asConfig.has_buyingpower3 = 1;
  asConfig.buyingpower3 = traderToolsInfo.globalConf.buyingPower[THIRD_ACCOUNT] * TEN_RAISE_TO_FOUR;
  
  asConfig.has_buyingpower4 = 1;
  asConfig.buyingpower4 = traderToolsInfo.globalConf.buyingPower[FOURTH_ACCOUNT] * TEN_RAISE_TO_FOUR;
  
  asConfig.has_fastmarketthreshold = 1;
  asConfig.fastmarketthreshold = FastMarketMgmt.thresHold;
  
  // need to send volatile/disable threshold to Daedalus, but for now, we do not need to send
  // because Daedalus do not set them
  
  // Build Daedalus msg
  Com__Rgm__Daedalus__Proto__Message daedalusMsg = COM__RGM__DAEDALUS__PROTO__MESSAGE__INIT;
  daedalusMsg.has_type = 1;
  daedalusMsg.type = COM__RGM__DAEDALUS__PROTO__MESSAGE__MESSAGE_TYPE__AS_GLOBAL_CONFIG;
  daedalusMsg.asglobalconfig = &asConfig;
  
/*#ifndef RELEASE_MODE
  TraceLog(DEBUG_LEVEL, "--->AS Global Config: stuck=%d loser=%d\n",
        daedalusMsg.asglobalconfig->maxstuckpositions,
        daedalusMsg.asglobalconfig->maxconsecutivelosers);
#endif*/

  // Packing Daedalus msg
  t_TTMessage *ttMessage = (t_TTMessage *)message;
  ttMessage->msgLen = com__rgm__daedalus__proto__message__get_packed_size(&daedalusMsg);
  com__rgm__daedalus__proto__message__pack(&daedalusMsg, ttMessage->msgContent);
  return ttMessage->msgLen;
}

int SendFullOpenPositionListToDaedalus(void *pConnection)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }
  
  int ttIndex = connection->index;
  int rmInfoIndex;
  int countRMInfo = 0;
  int countSymbol = RMList.countSymbol;
  int account;
  
  int firstItemAccount = -1;
  int firstItemRmInfoIndex = -1;
  
  t_PositionBk *oldPosition = NULL;
  int theColorFlag;
  
  // Get RM Information from RMList
  for (account = 0; account < MAX_ACCOUNT; account++)
  {
    for (rmInfoIndex = 0; rmInfoIndex < countSymbol; rmInfoIndex++)
    {
      if (RMList.collection[account][rmInfoIndex].leftShares ||
        RMList.collection[account][rmInfoIndex].totalShares)
      {
        if (firstItemAccount == -1)
        {
          // Use first item for send the key Global
          firstItemAccount = account;
          firstItemRmInfoIndex = rmInfoIndex;
          continue;
        }
        
        // Backup info
        RMList.collection[account][rmInfoIndex].oldPositionSent[ttIndex].leftShares = RMList.collection[account][rmInfoIndex].leftShares;
        RMList.collection[account][rmInfoIndex].oldPositionSent[ttIndex].totalShares = RMList.collection[account][rmInfoIndex].totalShares;
        RMList.collection[account][rmInfoIndex].oldPositionSent[ttIndex].avgPrice = RMList.collection[account][rmInfoIndex].avgPrice;
        
        // Send this open position to Daedalus
        SendAnOpenPositionToDaedalus(pConnection, account, rmInfoIndex, NO);

        countRMInfo++;
      }
      
      // Send INITIAL this position state to Daedalus if needed
      if (RMList.collection[account][rmInfoIndex].totalShares != 0)
      {
        oldPosition = &RMList.collection[account][rmInfoIndex].oldPositionSent[ttIndex];
        theColorFlag = RMList.collection[account][rmInfoIndex].colorFlag;
        if(oldPosition->colorFlag != theColorFlag)
        {
          oldPosition->colorFlag = theColorFlag;
          BuildAndSendUpdatedPositionStateToDaedalus(pConnection, account, rmInfoIndex, theColorFlag);
        }
      }
    }
  }
  
  // Send the first Item
  if (firstItemAccount > -1)
  {
    // Backup info
    RMList.collection[firstItemAccount][firstItemRmInfoIndex].oldPositionSent[ttIndex].leftShares = RMList.collection[firstItemAccount][firstItemRmInfoIndex].leftShares;
    RMList.collection[firstItemAccount][firstItemRmInfoIndex].oldPositionSent[ttIndex].totalShares = RMList.collection[firstItemAccount][firstItemRmInfoIndex].totalShares;
    RMList.collection[firstItemAccount][firstItemRmInfoIndex].oldPositionSent[ttIndex].avgPrice = RMList.collection[firstItemAccount][firstItemRmInfoIndex].avgPrice;
    
    // Send this open position to Daedalus
    SendAnOpenPositionToDaedalus(pConnection, firstItemAccount, firstItemRmInfoIndex, YES);
    
    //---------------
    // Send INITIAL this position state to Daedalus if needed
    if (RMList.collection[firstItemAccount][firstItemRmInfoIndex].totalShares != 0)
    {
      oldPosition = &RMList.collection[firstItemAccount][firstItemRmInfoIndex].oldPositionSent[ttIndex];
      theColorFlag = RMList.collection[firstItemAccount][firstItemRmInfoIndex].colorFlag;
      if(oldPosition->colorFlag != theColorFlag)
      {
        oldPosition->colorFlag = theColorFlag;
        BuildAndSendUpdatedPositionStateToDaedalus(pConnection, firstItemAccount, firstItemRmInfoIndex, theColorFlag);
      }
    }
  }

  // TraceLog(DEBUG_LEVEL, "--->Sent %d open positions to Daedalus when connection has just established\n", countRMInfo);
  
  return SUCCESS;
}

int CheckAndSendOpenPositionsToDaedalus(void *pConnection)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;

  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }
  
  int ttIndex = connection->index;
  
  int rmInfoIndex;
  int countRMInfo = 0;
  int countSymbol = RMList.countSymbol;
  int account;
  
  int firstItemAccount = -1;
  int firstItemRmInfoIndex = -1;
  
  t_PositionBk *oldPosition = NULL;
  int theColorFlag;
  
  // Get RM Information from RMList
  for (account = 0; account < MAX_ACCOUNT; account++)
  {
    for (rmInfoIndex = 0; rmInfoIndex < countSymbol; rmInfoIndex++)
    {
      if (RMList.collection[account][rmInfoIndex].leftShares != RMList.collection[account][rmInfoIndex].oldPositionSent[ttIndex].leftShares ||
        RMList.collection[account][rmInfoIndex].totalShares != RMList.collection[account][rmInfoIndex].oldPositionSent[ttIndex].totalShares ||
        (!feq(RMList.collection[account][rmInfoIndex].avgPrice, RMList.collection[account][rmInfoIndex].oldPositionSent[ttIndex].avgPrice)))
      {
        if (firstItemAccount == -1)
        {
          // Use first item for send the key Global
          firstItemAccount = account;
          firstItemRmInfoIndex = rmInfoIndex;
          continue;
        }
        
        // Backup info
        RMList.collection[account][rmInfoIndex].oldPositionSent[ttIndex].leftShares = RMList.collection[account][rmInfoIndex].leftShares;
        RMList.collection[account][rmInfoIndex].oldPositionSent[ttIndex].totalShares = RMList.collection[account][rmInfoIndex].totalShares;
        RMList.collection[account][rmInfoIndex].oldPositionSent[ttIndex].avgPrice = RMList.collection[account][rmInfoIndex].avgPrice;
        
        // Send this open position to Daedalus
        SendAnOpenPositionToDaedalus(pConnection, account, rmInfoIndex, NO);

        countRMInfo++;
      }
      
      // Send UPDATED position state to Daedalus if needed
      if (RMList.collection[account][rmInfoIndex].totalShares != 0)
      {
        oldPosition = &RMList.collection[account][rmInfoIndex].oldPositionSent[ttIndex];
        theColorFlag = RMList.collection[account][rmInfoIndex].colorFlag;
        if(oldPosition->colorFlag != theColorFlag)
        {
          oldPosition->colorFlag = theColorFlag;
          BuildAndSendUpdatedPositionStateToDaedalus(pConnection, account, rmInfoIndex, theColorFlag);
        }
      }
    }
  }

  /*if (countRMInfo)
  {
    TraceLog(DEBUG_LEVEL, "--->Sent %d open positions to Daedalus\n", countRMInfo);
  }*/
  
  // Send the first Item
  if (firstItemAccount > -1)
  {
    // Backup info
    RMList.collection[firstItemAccount][firstItemRmInfoIndex].oldPositionSent[ttIndex].leftShares = RMList.collection[firstItemAccount][firstItemRmInfoIndex].leftShares;
    RMList.collection[firstItemAccount][firstItemRmInfoIndex].oldPositionSent[ttIndex].totalShares = RMList.collection[firstItemAccount][firstItemRmInfoIndex].totalShares;
    RMList.collection[firstItemAccount][firstItemRmInfoIndex].oldPositionSent[ttIndex].avgPrice = RMList.collection[firstItemAccount][firstItemRmInfoIndex].avgPrice;
    
    // Send this open position to Daedalus
    SendAnOpenPositionToDaedalus(pConnection, firstItemAccount, firstItemRmInfoIndex, YES);
    
    //-------------------
    // Send UPDATED position state to Daedalus if needed
    if (RMList.collection[firstItemAccount][firstItemRmInfoIndex].totalShares != 0)
    {
      oldPosition = &RMList.collection[firstItemAccount][firstItemRmInfoIndex].oldPositionSent[ttIndex];
      theColorFlag = RMList.collection[firstItemAccount][firstItemRmInfoIndex].colorFlag;
      if(oldPosition->colorFlag != theColorFlag)
      {
        oldPosition->colorFlag = theColorFlag;
        BuildAndSendUpdatedPositionStateToDaedalus(pConnection, firstItemAccount, firstItemRmInfoIndex, theColorFlag);
      }
    }
  }
  
  return SUCCESS;
}

// This function apply the rules below
int SendAnOpenPositionToDaedalus(void *pConnection, int account, int rmInfoIndex, int isAggregatedAll)
{
  //--------------------------------------------------------------------------
  // RULES TO SEND POSITION MESSAGES:
  // For Global Key: suppose that we need to send 3 messages for 3 symbols, the routines must be
  //  + msg 1 for symbol 1: SymbolPositionMsg for symbol 1
  //  + msg 2 for symbol 2: SymbolPositionMsg for symbol 2
  //  + msg 3 for symbol 3: SymbolPositionMsg for symbol 3 and PositionAggregateMsg aggregates info from msg 1, 2 and 3
  // For Blink Key: send SymbolPositionMsg and PositionAggregateMsg have similar values
  // SymbolPositionMsg are the same for Blink Key and Global Key
  //--------------------------------------------------------------------------  
  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;
  int ttIndex = connection->index;
  
  //---------------------------------------------------------------
  // Build Symbol Position: uses for both BlinkBox and Global Key
  //---------------------------------------------------------------
  long positiveValue = RMList.collection[account][rmInfoIndex].totalMarket * TEN_RAISE_TO_FOUR;
  long negativeValue = 0 - positiveValue;
  Com__Rgm__Daedalus__Proto__SymbolPositionMsg symbolPos = COM__RGM__DAEDALUS__PROTO__SYMBOL_POSITION_MSG__INIT;
  
  // Symbol
  symbolPos.symbol = RMList.symbolList[rmInfoIndex].symbol;
  
  // Fee: Note that this fee is calculated by AS to profit loss, therefore no need to send it to Daedalus
  symbolPos.has_fees = 1;
  symbolPos.fees = RMList.collection[account][rmInfoIndex].totalFee;
  
  // Stuck invested
  symbolPos.has_invested = 1;
  if (RMList.collection[account][rmInfoIndex].oldPositionSent[ttIndex].leftShares < 0)
  {
    // stuck short
    symbolPos.invested = negativeValue;
  }
  else
  {
    symbolPos.invested = positiveValue;
  }
  
  // Total value of all shares sold
  symbolPos.has_valuesold = 1;
  if (RMList.collection[account][rmInfoIndex].oldPositionSent[ttIndex].leftShares < 0)
  {
    symbolPos.valuesold = positiveValue;
  }
  else
  {
    symbolPos.valuesold = 0;
  }
  
  // Committed Long: What invested would become if all long orders filled.
  // Committed Short: What invested would become if all short orders filled.
  // Net committed: max(abs(committedLong), abs(committedShort))
  // Cash held by position, positive when short, negative when long
  symbolPos.has_committedlong = 1;
  symbolPos.has_committedshort = 1;
  symbolPos.has_netcommitted = 1;
  symbolPos.has_cash = 1;
  if (RMList.collection[account][rmInfoIndex].oldPositionSent[ttIndex].leftShares < 0)
  {
    // Stuck short
    symbolPos.committedlong = 0;
    symbolPos.committedshort = symbolPos.invested;
    symbolPos.netcommitted = labs(symbolPos.committedshort);
  }
  else
  {
    symbolPos.committedlong = symbolPos.invested;
    symbolPos.committedshort = 0;
    symbolPos.netcommitted = symbolPos.committedlong;
  }
  
  symbolPos.cash = RMList.collection[account][rmInfoIndex].matchedPL - (RMList.collection[account][rmInfoIndex].leftShares * RMList.collection[account][rmInfoIndex].avgPrice);

  // Last traded price
  symbolPos.has_lasttradeprice = 1;
  symbolPos.lasttradeprice = RMList.symbolList[rmInfoIndex].lastTradedPrice * TEN_RAISE_TO_FOUR;
  
  // Size owned: positive when long, negative when short
  symbolPos.has_sizeowned = 1;
  symbolPos.sizeowned = RMList.collection[account][rmInfoIndex].oldPositionSent[ttIndex].leftShares;
  
  // Total Traded Size
  symbolPos.has_sizetraded = 1;
  symbolPos.sizetraded = RMList.collection[account][rmInfoIndex].oldPositionSent[ttIndex].totalShares;
  
  // sizecommittedshort: size we would own if all long orders filled
  // sizeCommittedShort: size we would own if all short orders filled
  // should we compare with Open Orders List????
  symbolPos.has_sizecommittedshort = 1;
  symbolPos.has_sizecommittedlong = 1;
  
  if (RMList.collection[account][rmInfoIndex].leftShares < 0)
  {
    symbolPos.sizecommittedshort = -1 *  RMList.collection[account][rmInfoIndex].leftShares;
    symbolPos.sizecommittedlong = 0;
  }
  else if (RMList.collection[account][rmInfoIndex].leftShares > 0)
  {
    symbolPos.sizecommittedlong =  RMList.collection[account][rmInfoIndex].leftShares;
    symbolPos.sizecommittedshort = 0;
  }
  else
  {
    symbolPos.sizecommittedshort = 0;
    symbolPos.sizecommittedlong = 0;
  }

  // sizeSellableLong: max(sizeOwned - size of outstanding short orders, 0)
  // should we compare with Open Orders List????
  symbolPos.has_sizesellablelong = 1;
  symbolPos.sizesellablelong = 0; // Daedalus does not used this
  
  // absSizeMarkedSell: the nonnegative amount of outstanding size marked sell
  // should we compare with Open Orders List????
  symbolPos.has_abssizemarkedsell = 1;
  symbolPos.abssizemarkedsell = 0; // Daedalus does not used this
  
  // Tax
  symbolPos.has_tax = 1;
  symbolPos.tax = 0;
  
  //--------------------------------------------
  // Build Symbol Aggregated For Blink Key
  //--------------------------------------------
  Com__Rgm__Daedalus__Proto__PositionAggregateMsg blinkSymbolAgg = COM__RGM__DAEDALUS__PROTO__POSITION_AGGREGATE_MSG__INIT;
  
  // Total trades shares
  blinkSymbolAgg.has_sizetraded = 1;
  blinkSymbolAgg.sizetraded = RMList.collection[account][rmInfoIndex].oldPositionSent[ttIndex].totalShares;

  // investedShort: 0 when long, negative when short
  // investedLong: positive when long, 0 when short
  // invested: investedLong + abs(investedShort)
  blinkSymbolAgg.has_invested = 1;
  blinkSymbolAgg.has_investedshort = 1;
  blinkSymbolAgg.has_investedlong = 1;
  if (RMList.collection[account][rmInfoIndex].oldPositionSent[ttIndex].leftShares < 0)
  {
    // Stuck short
    blinkSymbolAgg.investedshort = negativeValue;
    blinkSymbolAgg.investedlong = 0;
    blinkSymbolAgg.invested = positiveValue;
  }
  else
  {
    // Stuck long
    blinkSymbolAgg.investedshort = 0;
    blinkSymbolAgg.investedlong = positiveValue;
    blinkSymbolAgg.invested = positiveValue;
  }

  // committedShort: What invested would become if all short orders filled
  // committedLong: What invested would become if all long orders filled
  // netCommitted: max(abs(committedLong), abs(committedShort))
  blinkSymbolAgg.has_committedshort = 1;
  blinkSymbolAgg.has_committedlong = 1;
  blinkSymbolAgg.has_netcommitted = 1;
  blinkSymbolAgg.committedshort = symbolPos.committedshort;
  blinkSymbolAgg.committedlong = symbolPos.committedlong;
  blinkSymbolAgg.netcommitted = symbolPos.netcommitted;

  // Net Profit
  blinkSymbolAgg.has_netprofit = 1;
  blinkSymbolAgg.netprofit = (RMList.collection[account][rmInfoIndex].matchedPL - RMList.collection[account][rmInfoIndex].totalFee) * TEN_RAISE_TO_FOUR;

  // Balance: this is defined to be 0.5 if invested is 0, otherwise investedLong / invested
  blinkSymbolAgg.has_balance = 1;
  if (blinkSymbolAgg.invested == 0)
  {
    blinkSymbolAgg.balance = 0.50;
  }
  else
  {
    blinkSymbolAgg.balance = blinkSymbolAgg.investedlong / blinkSymbolAgg.invested;
  }
  
  //--------------------------------------------
  // Build All Symbol Aggregated For Global Key
  //--------------------------------------------
  Com__Rgm__Daedalus__Proto__PositionAggregateMsg allSymbolAgg = COM__RGM__DAEDALUS__PROTO__POSITION_AGGREGATE_MSG__INIT;
  if (isAggregatedAll == YES)
  {
    // Calcualte the Profit and Loss
    int rmIndex, totalShares = 0;
    double totalMatchPL = 0.00, totalFee = 0.00, totalMarket = 0.00;
    int accountIndex, countSymbol = RMList.countSymbol; 
    int shortShares = 0, longShares = 0;
    double totalShortValue = 0.00, totalLongValue = 0.00;
    
    for (accountIndex = 0; accountIndex < MAX_ACCOUNT; accountIndex++)
    {
      for (rmIndex = 0; rmIndex < countSymbol; rmIndex++)
      {
        // Total shares
        totalShares += RMList.collection[accountIndex][rmIndex].totalShares;

        // Total matched PL
        totalMatchPL += RMList.collection[accountIndex][rmIndex].matchedPL;

        // Total market
        totalMarket += fabs(RMList.collection[accountIndex][rmIndex].totalMarket);
        
        // Total fee
        totalFee += RMList.collection[accountIndex][rmIndex].totalFee;
        
        // Short and long shares
        if (RMList.collection[accountIndex][rmIndex].leftShares > 0)
        {
          // stuck long
          longShares += RMList.collection[accountIndex][rmIndex].leftShares;
          totalLongValue += RMList.collection[accountIndex][rmIndex].totalMarket;
        }
        else
        {
          // stuck short
          shortShares += RMList.collection[accountIndex][rmIndex].leftShares;
          totalShortValue += RMList.collection[accountIndex][rmIndex].totalMarket; // totalMarket > 0
        }
      }
    }
    
    // Total NET PL
    double totalNetPL = totalMatchPL - totalFee;
    // TraceLog(DEBUG_LEVEL, "--->totalNetPL=%lf totalMatchPL=%lf totalFee=%lf\n", totalNetPL, totalMatchPL, totalFee);
    
    // Outstanding
    long outstandingLong = totalLongValue * TEN_RAISE_TO_FOUR;
    long outstandingShort = totalShortValue * TEN_RAISE_TO_FOUR;
    
    // Building the fields of allSymbolAgg
    
    // Total trades shares
    allSymbolAgg.has_sizetraded = 1;
    allSymbolAgg.sizetraded = totalShares;
    
    // TraceLog(DEBUG_LEVEL, "--->sizetraded=%d\n", totalShares);
    
    // 
    // investedShort: The sum of invested for all short positions in this aggregate position. Always negative or 0
    //          ---> negative when short
    // investedLong: The sum of invested for all long positions in this aggregate position. Always positive or 0.
    //          ---> positive when long
    // invested: investedLong + abs(investedShort)
    allSymbolAgg.has_invested = 1;
    allSymbolAgg.has_investedshort = 1;
    allSymbolAgg.has_investedlong = 1;
    allSymbolAgg.investedshort = (-outstandingShort); // incorrect but acceptable
    allSymbolAgg.investedlong = outstandingLong; // incorrect but acceptable
    allSymbolAgg.invested = totalMarket * TEN_RAISE_TO_FOUR; // totalMarket > 0
    
    // committedShort: What invested would become if all short orders filled
    // committedLong: What invested would become if all long orders filled
    // netCommitted: max(abs(committedLong), abs(committedShort))
    allSymbolAgg.has_committedshort = 1;
    allSymbolAgg.has_committedlong = 1;
    allSymbolAgg.has_netcommitted = 1;
    allSymbolAgg.committedshort = (-outstandingShort); // this is ouststand short
    allSymbolAgg.committedlong = outstandingLong; // this is ouststand long
    allSymbolAgg.netcommitted = (totalLongValue > totalShortValue ? outstandingLong : outstandingShort);

    // Net Profit
    allSymbolAgg.has_netprofit = 1;
    allSymbolAgg.netprofit = totalNetPL * TEN_RAISE_TO_FOUR;
    
    // Balance: this is defined to be 0.5 if invested is 0, otherwise investedLong / invested
    allSymbolAgg.has_balance = 1;
    if (allSymbolAgg.invested == 0)
    {
      allSymbolAgg.balance = 0.50;
    }
    else
    {
      allSymbolAgg.balance = totalLongValue / (totalLongValue + totalShortValue);
    }
  }
  
  //------------------------------------------
  // Send message for Blinkbox Key which contains:
  //  SymbolPositionMsg and PositionAggregateMsg per each symbol per account
  //------------------------------------------
  Com__Rgm__Daedalus__Proto__PositionMessage positionMsg = COM__RGM__DAEDALUS__PROTO__POSITION_MESSAGE__INIT;
  
  // Get time
  positionMsg.has_time = 1;
  positionMsg.time = hbitime_micros() * 1000;
  
  // Symbol Position
  positionMsg.position = &symbolPos;
  
  // Aggregated Position
  positionMsg.aggregate = &blinkSymbolAgg;
  
  // Position Key
  positionMsg.positionkey = RMList.collection[account][rmInfoIndex].positionKey;
  
  // Account
  positionMsg.account = TradingAccount.DIRECT[account];
  
  // Build Daedalus msg for Blink's Position Key
  Com__Rgm__Daedalus__Proto__Message daedalusMsg = COM__RGM__DAEDALUS__PROTO__MESSAGE__INIT;
  daedalusMsg.has_type = 1;
  daedalusMsg.type = COM__RGM__DAEDALUS__PROTO__MESSAGE__MESSAGE_TYPE__POSITION;
  daedalusMsg.position = &positionMsg;
  
  // Packing Daedalus msg
  t_TTMessage ttMessage;
  ttMessage.msgLen = com__rgm__daedalus__proto__message__get_packed_size(&daedalusMsg);
  com__rgm__daedalus__proto__message__pack(&daedalusMsg, ttMessage.msgContent);
  if (SendDaedalusMessage(pConnection, &ttMessage) == ERROR)
  {
    return ERROR;
  }
  
  //------------------------------------------
  // Send message for Global Key which contains:
  //  1. SymbolPositionMsg: is the same as Blink Key
  //  2. PositionAggregateMsg if flag is ON
  //------------------------------------------
  positionMsg.positionkey = GLOBAL_POSITION_KEY;
  // Aggregated Position
  if (isAggregatedAll == YES)
  {
    positionMsg.aggregate = &allSymbolAgg;
  }
  else
  {
    positionMsg.aggregate = NULL;
  }
  
  // Packing Daedalus msg
  memset(&ttMessage, 0, sizeof(ttMessage));
  ttMessage.msgLen = com__rgm__daedalus__proto__message__get_packed_size(&daedalusMsg);
  com__rgm__daedalus__proto__message__pack(&daedalusMsg, ttMessage.msgContent);
  
  return SendDaedalusMessage(connection, &ttMessage);
}

int BuildAndSendUpdatedPositionStateToDaedalus(void *pConnection, int account, int rmInfoIndex, Com__Rgm__Daedalus__Proto__PositionState internalState)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)pConnection;
  
  //-------------------------------------
  // Build Position State for a symbol
  //-------------------------------------
  Com__Rgm__Daedalus__Proto__PositionStates symbolPositionState = COM__RGM__DAEDALUS__PROTO__POSITION_STATES__INIT;
  Com__Rgm__Daedalus__Proto__PositionStates *pSymbolPositionState[1];
  pSymbolPositionState[0] = &symbolPositionState;
  
  // Internal State: the state the trading system has entered on its own
  // External State: the state enforced by the operator
  symbolPositionState.has_internalstate = 1;
  symbolPositionState.has_externalstate = 1;
  symbolPositionState.internalstate = internalState;
  symbolPositionState.externalstate = COM__RGM__DAEDALUS__PROTO__POSITION_STATE__Normal;
  
  // Trader tags
  Com__Rgm__Daedalus__Proto__TraderTags traderTags = COM__RGM__DAEDALUS__PROTO__TRADER_TAGS__INIT;
  traderTags.source = TradingAccount.DIRECT[account]; // source is used to update to label of Positions & Sources Window
  traderTags.strategy = "Arbitrage";
  traderTags.creator = "Blinkbox";
  traderTags.positionkey = RMList.collection[account][rmInfoIndex].positionKey;
  symbolPositionState.tags = &traderTags;
  
  //-------------------------------------
  // Build Postion State List msg
  //-------------------------------------
  Com__Rgm__Daedalus__Proto__PositionStateList posStateList = COM__RGM__DAEDALUS__PROTO__POSITION_STATE_LIST__INIT;
  posStateList.n_positionstates = 1;
  posStateList.positionstates = pSymbolPositionState;
  
  //--------------------------------------------
  // Build Daedalus msg for Blink's Position Key
  //--------------------------------------------
  Com__Rgm__Daedalus__Proto__Message daedalusMsg = COM__RGM__DAEDALUS__PROTO__MESSAGE__INIT;
  daedalusMsg.has_type = 1;
  daedalusMsg.type = COM__RGM__DAEDALUS__PROTO__MESSAGE__MESSAGE_TYPE__POSITION_STATE;
  daedalusMsg.positionstatelist = &posStateList;
  
// #ifndef RELEASE_MODE
  // TraceLog(DEBUG_LEVEL, "--->State msg: '%.8s' n_positionstates=%d state=%d(normal:1, stuck:6) key='%s' source='%s'\n",
    // RMList.symbolList[rmInfoIndex].symbol,
    // daedalusMsg.positionstatelist->n_positionstates,
    // daedalusMsg.positionstatelist->positionstates[0]->internalstate,
    // daedalusMsg.positionstatelist->positionstates[0]->tags->positionkey,
    // daedalusMsg.positionstatelist->positionstates[0]->tags->source);
// #endif
  
  // Packing Daedalus msg
  t_TTMessage ttMessage;
  ttMessage.msgLen = com__rgm__daedalus__proto__message__get_packed_size(&daedalusMsg);
  com__rgm__daedalus__proto__message__pack(&daedalusMsg, ttMessage.msgContent);
  if (SendDaedalusMessage(connection, &ttMessage) == ERROR)
  {
    return ERROR;
  }
  
  //--------------------------------------------
  // Build Daedalus msg for Global Position Key
  //--------------------------------------------
  /*traderTags.positionkey = GLOBAL_POSITION_KEY;
  
// #ifndef RELEASE_MODE
  // TraceLog(DEBUG_LEVEL, "--->State msg: '%s' n_positionstates=%d state=%d(normal:1, stuck:6) key='%s'\n",
    // RMList.symbolList[rmInfoIndex].symbol,
    // daedalusMsg.positionstatelist->n_positionstates,
    // daedalusMsg.positionstatelist->positionstates[0]->internalstate,
    // daedalusMsg.positionstatelist->positionstates[0]->tags->positionkey);
// #endif
  
  // Packing Daedalus msg
  memset(&ttMessage, 0, sizeof(ttMessage));
  ttMessage.msgLen = com__rgm__daedalus__proto__message__get_packed_size(&daedalusMsg);
  com__rgm__daedalus__proto__message__pack(&daedalusMsg, ttMessage.msgContent);
  return SendDaedalusMessage(pConnection, &ttMessage);*/

  return SUCCESS;
}

int CheckNSendRequestPriceUpdate()
{
  char symbolList[MAX_SYMBOL_TRADE][SYMBOL_LEN];
  memset(symbolList, 0, MAX_SYMBOL_TRADE * SYMBOL_LEN);
  int totalSymbol = 0;
  
  int rmInfoIndex = 0;
  for (rmInfoIndex = 0; rmInfoIndex < RMList.countSymbol; rmInfoIndex++)
  {
    strncpy(symbolList[totalSymbol], RMList.symbolList[rmInfoIndex].symbol, SYMBOL_LEN);
    totalSymbol++;
  }
  
  if (totalSymbol == 0) return SUCCESS;

  // Determine which TS should use
   
  int severRole = 0, tsIndex = 0;
  int indexList[MAX_SERVER_ROLE];
  memset(indexList, 0, MAX_SERVER_ROLE);
  
  for (tsIndex = 0; tsIndex < MAX_TRADE_SERVER_CONNECTIONS; tsIndex++)
  {
    if(tradeServersInfo.currentStatus[tsIndex].statusCQS == CONNECTED
        || tradeServersInfo.currentStatus[tsIndex].statusUQDF == CONNECTED)
    {
      if(tradeServersInfo.globalConf[tsIndex].serverRole == ARCA_SERVER_ROLE)
      {
        indexList[ARCA_SERVER_ROLE]++;
      }
      else if(tradeServersInfo.globalConf[tsIndex].serverRole == NASDAQ_SERVER_ROLE)
      {
        indexList[NASDAQ_SERVER_ROLE]++;
      }
      else if(tradeServersInfo.globalConf[tsIndex].serverRole == BATS_SERVER_ROLE)
      {
        indexList[BATS_SERVER_ROLE]++;
      }
      else if(tradeServersInfo.globalConf[tsIndex].serverRole == EDGX_SERVER_ROLE)
      {
        indexList[EDGX_SERVER_ROLE]++;
      }
      else if(tradeServersInfo.globalConf[tsIndex].serverRole == STANDALONE_SERVER_ROLE)
      {
        indexList[STANDALONE_SERVER_ROLE]++;
      }
      else
      {
      }
    }
  }
  
  int selectedServerRole = -1;
  if(indexList[EDGX_SERVER_ROLE] > 1)
  {
    selectedServerRole = EDGX_SERVER_ROLE;
  }
  else if(indexList[BATS_SERVER_ROLE] > 1)
  {
    selectedServerRole = BATS_SERVER_ROLE;
  }
  else
  {
    for(severRole = 0; severRole < MAX_SERVER_ROLE; severRole++)
    {
      if(indexList[severRole] > 1)
      {
        selectedServerRole = severRole;
        break;
      }
      else if(indexList[severRole] > 0)
      {
        selectedServerRole = severRole;
      }
    }
  }
  
  if(selectedServerRole == -1)
  {
    return SUCCESS;
  }
  
  for (tsIndex = 0; tsIndex < MAX_TRADE_SERVER_CONNECTIONS; tsIndex++)
  {
    if(tradeServersInfo.globalConf[tsIndex].serverRole == selectedServerRole)
    {
      SendRequestPriceUpdateToTS(tsIndex, totalSymbol, symbolList);
    }
  }
  
  return SUCCESS;
}

int SendPriceUpdateToAllDaedalus(char serverName[MAX_LINE_LEN], unsigned char *receivedMsg)
{
  // Get number of symbol to build such price update message
  int numSymbols = GetIntNumber(&receivedMsg[10]);
    
  if (numSymbols < 1)
  {
    return SUCCESS;
  }
  
  int ttIndex;
  
  int item = 0, msgIndex = 14;
  for (item = 0; item < numSymbols; item++)
  {
    t_TTMessage ttMessage;
    t_ConnectionStatus *connection;

    // Build Aggregation Sever global configuration message to send Daedalus
    if (BuildPriceUpdateToAllDaedalus(&ttMessage, receivedMsg, &msgIndex) == ERROR)
    {
      return ERROR;
    }

    // Send message to all Daedalus connected to AS
    for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
    {
      connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];

      // Check connection between AS and Daedalus
      if ((connection->status == DISCONNECTED) || (connection->socket == -1) ||
        connection->clientType != CLIENT_TOOL_DAEDALUS)
      {
        continue;
      }

      // Send message to Daedalus
      SendDaedalusMessage(connection, &ttMessage);
    }
  }
  
  return SUCCESS;
}

int BuildPriceUpdateToAllDaedalus(t_TTMessage *ttMessage, unsigned char *receivedMsg, int *msgIndex)
{
  char symbol[SYMBOL_LEN + 1];
  int askShares, bidShares, stockSymbolIndex;
  unsigned long askPrice, bidPrice;

  //-------------------------------
  // Parse info from TS/PM message
  //-------------------------------
  // Symbol
  memset(symbol, 0, SYMBOL_LEN + 1);
  memcpy(symbol, &receivedMsg[*msgIndex], SYMBOL_LEN);
  *msgIndex += SYMBOL_LEN;
  
  stockSymbolIndex = GetStockSymbolIndex(symbol);
  if (stockSymbolIndex == -1)
  {
    TraceLog(ERROR_LEVEL, "Price Update: Invalid stock symbol index %d, symbol='%s'\n", stockSymbolIndex, symbol);
    return ERROR;
  }
  
  // Ask info
  memcpy(&askShares, &receivedMsg[*msgIndex], 4);
  *msgIndex += 4;
  memcpy(&askPrice, &receivedMsg[*msgIndex], 8);
  *msgIndex += 8;
  
  // Bid info
  memcpy(&bidShares, &receivedMsg[*msgIndex], 4);
  *msgIndex += 4;
  memcpy(&bidPrice, &receivedMsg[*msgIndex], 8);
  *msgIndex += 8;
  
  //-------------------------
  // Build Daedalus message
  //-------------------------
  Com__Rgm__Daedalus__Proto__Message daedalusMsg = COM__RGM__DAEDALUS__PROTO__MESSAGE__INIT;
  daedalusMsg.has_type = 1;
  daedalusMsg.type = COM__RGM__DAEDALUS__PROTO__MESSAGE__MESSAGE_TYPE__PRICE_UPDATE;
  
  Com__Rgm__Daedalus__Proto__PriceUpdate priceUpdate = COM__RGM__DAEDALUS__PROTO__PRICE_UPDATE__INIT;
  daedalusMsg.priceupdate = &priceUpdate;
  
  priceUpdate.symbol = RMList.symbolList[stockSymbolIndex].symbol;
  
  priceUpdate.has_insidebid = 1;
  priceUpdate.insidebid = bidPrice;
  RMList.symbolList[stockSymbolIndex].bidPrice = bidPrice;
  
  priceUpdate.has_insidebidsize = 1;
  priceUpdate.insidebidsize = bidShares;
  
  priceUpdate.has_insideask = 1;
  priceUpdate.insideask = askPrice;
  RMList.symbolList[stockSymbolIndex].askPrice = askPrice;
  
  priceUpdate.has_insideasksize = 1;
  priceUpdate.insideasksize = askShares;
  
  priceUpdate.has_time = 1;
  priceUpdate.time = hbitime_micros() * 1000;
  
  ttMessage->msgLen = com__rgm__daedalus__proto__message__get_packed_size(&daedalusMsg);
  com__rgm__daedalus__proto__message__pack(&daedalusMsg, ttMessage->msgContent);
  
  return SUCCESS;
}

int BuildAndSendAlertToDaedalus(char *alertMsg, int ttIndex, Com__Rgm__Daedalus__Proto__NotificationLevelType level)
{
  t_ConnectionStatus *connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];
  // Check connection between AS and TT
  if ((connection->status == DISCONNECT) || (connection->socket == -1))
  {
    return ERROR;
  }
  
  // Build Notification Msg
  Com__Rgm__Daedalus__Proto__Notification notification = COM__RGM__DAEDALUS__PROTO__NOTIFICATION__INIT;
  notification.has_level = 1;
  notification.level = level;
  notification.message = alertMsg;
  
  // Build Daedalus msg
  Com__Rgm__Daedalus__Proto__Message daedalusMsg = COM__RGM__DAEDALUS__PROTO__MESSAGE__INIT;
  daedalusMsg.has_type = 1;
  daedalusMsg.type = COM__RGM__DAEDALUS__PROTO__MESSAGE__MESSAGE_TYPE__NOTIFICATION;
  daedalusMsg.notification = &notification;
  
// #ifndef RELEASE_MODE
  // TraceLog(DEBUG_LEVEL, "--->alert to Daedalus: level=%d msg='%s'\n", daedalusMsg.notification->level, daedalusMsg.notification->message);
// #endif
  
  // Packing Daedalus msg
  t_TTMessage ttMessage;
  ttMessage.msgLen = com__rgm__daedalus__proto__message__get_packed_size(&daedalusMsg);
  com__rgm__daedalus__proto__message__pack(&daedalusMsg, ttMessage.msgContent);
  
  if (SendDaedalusMessage(connection, &ttMessage) == SUCCESS)
  {
    TraceLog(DEBUG_LEVEL, "Send alert to Daedalus index (%d): '%s'\n", ttIndex, alertMsg);
    return SUCCESS;
  }
  else
  {
    //TraceLog(ERROR_LEVEL, "Could not send Alert to Daedalus, socket = %d\n", connection->socket);
    return ERROR;
  }
}

int BuildAndSendAlertToAllDaedalus(char *alertMsg, Com__Rgm__Daedalus__Proto__NotificationLevelType level)
{
  // Build Notification Msg
  Com__Rgm__Daedalus__Proto__Notification notification = COM__RGM__DAEDALUS__PROTO__NOTIFICATION__INIT;
  notification.has_level = 1;
  notification.level = level;
  notification.message = alertMsg;
  
  // Build Daedalus msg
  Com__Rgm__Daedalus__Proto__Message daedalusMsg = COM__RGM__DAEDALUS__PROTO__MESSAGE__INIT;
  daedalusMsg.has_type = 1;
  daedalusMsg.type = COM__RGM__DAEDALUS__PROTO__MESSAGE__MESSAGE_TYPE__NOTIFICATION;
  daedalusMsg.notification = &notification;
  
// #ifndef RELEASE_MODE
  // TraceLog(DEBUG_LEVEL, "--->alert to All Daedalus: level=%d msg='%s'\n", daedalusMsg.notification->level, daedalusMsg.notification->message);
// #endif
  
  // Packing Daedalus msg
  t_TTMessage ttMessage;
  ttMessage.msgLen = com__rgm__daedalus__proto__message__get_packed_size(&daedalusMsg);
  com__rgm__daedalus__proto__message__pack(&daedalusMsg, ttMessage.msgContent);
  
  if (SendMessageToAllDaedalus(&ttMessage) == SUCCESS)
  {
    TraceLog(DEBUG_LEVEL, "Send alert to all Daedalus: %s\n", alertMsg);
    return SUCCESS;
  }
  else
  {
    //TraceLog(ERROR_LEVEL, "Could not send alert to all Daedalus\n");
    return ERROR;
  }
}

int SendPMOrderConfToAllDaedalus(int ECNIndex)
{
  if(ECNIndex < 0 || ECNIndex >=PM_MAX_ORDER_CONNECTIONS)
  {
    TraceLog(ERROR_LEVEL, "%s: ECNIndex(%d)\n", __func__, ECNIndex);
  }
    
  int i, ttIndex, result;
  t_ConnectionStatus *connection;
  
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];
    
    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_DAEDALUS)
    {
      continue;
    }
    
    for(i = 0 ; i < PM_MAX_ORDER_CONNECTIONS; i++)
    {
      int oec  = (i == PM_ARCA_DIRECT_INDEX) ? COM__RGM__DAEDALUS__PROTO__OEC__ARCA_DIRECT_OEC : COM__RGM__DAEDALUS__PROTO__OEC__RASH_OEC;
      result += BuildAndSendOecStatusToDaedalus(connection,
                  PM_SERVER_NAME,
                  pmInfo.connection.status,
                  oec,
                  pmInfo.currentStatus.pmOecStatus[i],
                  pmInfo.currentStatus.tradingStatus,
                  pmInfo.ECNOrdersConf[i].ip,
                  pmInfo.ECNOrdersConf[i].port,
                  SessionIdMgmt.pmSessionId[i]);
    }
  }
  
  return result;
}

int SendTSOrderConfToAllDaedalus(int tsIndex, int ECNIndex)
{
  if(ECNIndex < 0 || ECNIndex >=TS_MAX_ORDER_CONNECTIONS)
  {
    TraceLog(ERROR_LEVEL, "%s: ECNIndex(%d)\n", __func__, ECNIndex);
  }
  
  int i, ttIndex, result;
  t_ConnectionStatus *connection;
  
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];
    
    // Check connection between AS and TT
    if ((connection->status == DISCONNECT) || (connection->socket == -1) ||
      connection->clientType != CLIENT_TOOL_DAEDALUS)
    {
      continue;
    }

    for(i = 0 ; i < TS_MAX_ORDER_CONNECTIONS; i++)
    {
      result += BuildAndSendOecStatusToDaedalus(connection,
                tradeServersInfo.config[tsIndex].description,
                tradeServersInfo.connection[tsIndex].status,
                AS_CENTRALIZED_ORDER_TYPE_TO_OEC_MAPPING[i],
                tradeServersInfo.currentStatus[tsIndex].tsOecStatus[i],
                tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[i],
                tradeServersInfo.ECNOrdersConf[tsIndex][i].ip,
                tradeServersInfo.ECNOrdersConf[tsIndex][i].port,
                SessionIdMgmt.tsSessionId[tsIndex][i]);
    }
  }
  
  return result;
}

int ProcessEtbListRequestFromDaedalus()
{
  TraceLog(DEBUG_LEVEL, "Received request to re-request ETB list from Daedalus\n");
  pthread_t requestEtbListThread;
  pthread_create(&requestEtbListThread, NULL, (void *)RequestEtbListThread, NULL);
  return SUCCESS;
}

int ProcessEmergencyStopRequestFromDaedalus()
{ 
  TraceLog(DEBUG_LEVEL, "Received Emergency Stop request from Daedalus\n");
  SendDisableTradingAllOrderToAllTS(TS_TRADING_DISABLED_BY_EMERGENCY_STOP);
  SendDisableTradingPMToPM(PM_TRADING_DISABLED_BY_EMERGENCY_STOP);
  
  //Cancel all open order by manual
  int i;
  for(i = 0; i < MAX_MANUAL_ORDER; i++)
  {
    if(AutoCancelOrder.orderInfo[i].marked == 1 && AutoCancelOrder.orderInfo[i].leftShares > 0)
    {
      AutoCancelOrder.orderInfo[i].marked = 0;
      int clOrdId = AutoCancelOrder.orderInfo[i].clOrderId;
      char *ecnOrderID = AutoCancelOrder.orderInfo[i].ecnOrderID;
      ProcessSendCancelRequestToECNOrder(clOrdId, ecnOrderID);
    }
  }
  
  return SUCCESS;
}

int SendHeartbeatToDaedalus(void *pConnection)
{
  Com__Rgm__Daedalus__Proto__Message daedalusMsg = COM__RGM__DAEDALUS__PROTO__MESSAGE__INIT;
  
  daedalusMsg.has_type = 1;
  daedalusMsg.type = COM__RGM__DAEDALUS__PROTO__MESSAGE__MESSAGE_TYPE__TIMESTAMPED_HEARTBEAT;
  
  Com__Rgm__Daedalus__Proto__TimestampedHeartbeat heartbeat = COM__RGM__DAEDALUS__PROTO__TIMESTAMPED_HEARTBEAT__INIT;
  daedalusMsg.timestampedheartbeat = &heartbeat;
  
  heartbeat.has_time = 1;
  heartbeat.time = 1;
  
  t_TTMessage ttMessage;
  ttMessage.msgLen = com__rgm__daedalus__proto__message__get_packed_size(&daedalusMsg);
  com__rgm__daedalus__proto__message__pack(&daedalusMsg, ttMessage.msgContent);
  
  return SendDaedalusMessage(pConnection, &ttMessage);
}

int InitMonitorServerStatusChange(t_MonitorStatusChange *monitor)
{
  memset(monitor, 0, sizeof(t_MonitorStatusChange));
  
  monitor->updateServerStatus = 1;
  monitor->updateServerInfo = 1;
  
  int i, j;
  
  // TS
  for(i = 0; i < MAX_TRADE_SERVER_CONNECTIONS; i++)
  {
    monitor->tsServerStatus[i] = 1;
    
    for(j = 0; j < TS_MAX_BOOK_CONNECTIONS; j++)
    {
      monitor->tsMdcStatus[i][j] = 1;
    }
    
    for(j = 0; j < 2; j++)
    {
      monitor->tsAwayMarketMdcStatus[i][j] = 1;
    }
    
    for(j = 0; j < TS_MAX_ORDER_CONNECTIONS; j++)
    {
      monitor->tsOecStatus[i][j] = 1;
    }
  }
  
  // PM
  monitor->pmServerStatus = 1;
  
  for(i = 0; i < PM_MAX_BOOK_CONNECTIONS; i++)
  {
    monitor->pmMdcStatus[i] = 1;
  }
  
  for(i = 0; i < 4; i++)
  {
    monitor->pmAwayMarketMdcStatus[i] = 1;
  }
  
  for(i = 0; i < PM_MAX_ORDER_CONNECTIONS; i++)
  {
    monitor->pmOecStatus[i] = 1;
  }
  
  // AS
  monitor->asServerStatus = 1;
  for(i = 0; i < AS_MAX_ORDER_CONNECTIONS; i++)
  {
    monitor->asOecStatus[i] = 1;
  }
  
  return SUCCESS;
}

int MonitorServerStatusChange(t_MonitorStatusChange *monitor, int tsIndex, char *serverName, int accountIndex)
{
  int i;
  
  if (tsIndex > -1)
  {
    if(tradeServersInfo.connection[tsIndex].status == CONNECT)
    {
      if(tradeServersInfo.globalConf[tsIndex].serverRole > -1) // AS received global config from TS to get server role
      {   
        // connection status
        if(tradeServersInfo.connection[tsIndex].preStatus != tradeServersInfo.connection[tsIndex].status)
        {
          monitor->count += 1;
          monitor->updateServerStatus = 1;
          monitor->tsServerStatus[tsIndex] = 1;
          tradeServersInfo.connection[tsIndex].preStatus = tradeServersInfo.connection[tsIndex].status;
        }
        
        // Num Stucks
        if(tradeServersInfo.preStatus[tsIndex].numStuck != tradeServersInfo.currentStatus[tsIndex].numStuck)
        {
          monitor->count += 1;
          monitor->updateServerInfo = 1;
          monitor->tsServerStatus[tsIndex] = 1;
          tradeServersInfo.preStatus[tsIndex].numStuck = tradeServersInfo.currentStatus[tsIndex].numStuck;
        }
        
        // Num Losers
        if(tradeServersInfo.preStatus[tsIndex].numLoser != tradeServersInfo.currentStatus[tsIndex].numLoser)
        {
          monitor->count += 1;
          monitor->updateServerInfo = 1;
          monitor->tsServerStatus[tsIndex] = 1;
          tradeServersInfo.preStatus[tsIndex].numLoser = tradeServersInfo.currentStatus[tsIndex].numLoser;
        }
        
        // Buying Power
        if(!feq(tradeServersInfo.preStatus[tsIndex].buyingPower, tradeServersInfo.currentStatus[tsIndex].buyingPower))
        {
          monitor->count += 1;
          monitor->updateServerInfo = 1;
          monitor->tsServerStatus[tsIndex] = 1;
          tradeServersInfo.preStatus[tsIndex].buyingPower = tradeServersInfo.currentStatus[tsIndex].buyingPower;
        } 
        
        
        // set server status
        int connectCounter = 0;
        for(i = 0; i < TS_MAX_BOOK_CONNECTIONS; i++)
        {
          connectCounter += tradeServersInfo.currentStatus[tsIndex].tsMdcStatus[i];
        }
        
        for(i = 0; i < TS_MAX_ORDER_CONNECTIONS; i++)
        {
          connectCounter += tradeServersInfo.currentStatus[tsIndex].tsOecStatus[i];
        }
        
        connectCounter += tradeServersInfo.currentStatus[tsIndex].statusCQS +
                          tradeServersInfo.currentStatus[tsIndex].statusUQDF;
                    
        if (connectCounter == TS_MAX_BOOK_CONNECTIONS + TS_MAX_ORDER_CONNECTIONS + MAX_BBO_SOURCE)
        {
          tradeServersInfo.currentStatus[tsIndex].serverStatus = COM__RGM__DAEDALUS__PROTO__SERVER_CONNECTIONS_STATUS__CONNECTED;
        }
        else if (connectCounter == 0)
        {
          tradeServersInfo.currentStatus[tsIndex].serverStatus = COM__RGM__DAEDALUS__PROTO__SERVER_CONNECTIONS_STATUS__DISCONNECTED;
        }
        else
        {
          tradeServersInfo.currentStatus[tsIndex].serverStatus = COM__RGM__DAEDALUS__PROTO__SERVER_CONNECTIONS_STATUS__SOME_CONNECTED;
        }
        
        if(tradeServersInfo.preStatus[tsIndex].serverStatus != tradeServersInfo.currentStatus[tsIndex].serverStatus)
        {
          monitor->count += 1;
          monitor->updateServerStatus = 1;
          monitor->tsServerStatus[tsIndex] = 1;
          tradeServersInfo.preStatus[tsIndex].serverStatus = tradeServersInfo.currentStatus[tsIndex].serverStatus;
        }
        
        // iso trading
        if (tradeServersInfo.currentStatus[tsIndex].statusCQS == CONNECTED ||
            tradeServersInfo.currentStatus[tsIndex].statusUQDF == CONNECTED)
        {
          // serverStatus.isotrading = tradeServersInfo.currentStatus[tsIndex].statusISOTrading;
          if (tradeServersInfo.currentStatus[tsIndex].statusISOTrading == ENABLE)
          {
            tradeServersInfo.currentStatus[tsIndex].isoTrading = COM__RGM__DAEDALUS__PROTO__ISO_TRADING_STATE__IsoActive;
          }
          else
          {
            tradeServersInfo.currentStatus[tsIndex].isoTrading  = COM__RGM__DAEDALUS__PROTO__ISO_TRADING_STATE__IsoInactive;
          }
        }
        else
        {
          tradeServersInfo.currentStatus[tsIndex].isoTrading  = COM__RGM__DAEDALUS__PROTO__ISO_TRADING_STATE__IsoDisabled;
        }
        
        if(tradeServersInfo.preStatus[tsIndex].isoTrading != tradeServersInfo.currentStatus[tsIndex].isoTrading)
        {
          monitor->count += 1;
          monitor->updateServerStatus = 1;
          monitor->tsServerStatus[tsIndex] = 1;
          tradeServersInfo.preStatus[tsIndex].isoTrading = tradeServersInfo.currentStatus[tsIndex].isoTrading;
        }
      }
    }
    else
    {
      if(tradeServersInfo.connection[tsIndex].preStatus != tradeServersInfo.connection[tsIndex].status)
      {
        monitor->count += 1;
        monitor->updateServerStatus = 1;
        monitor->tsServerStatus[tsIndex] = 1;
        tradeServersInfo.connection[tsIndex].preStatus = tradeServersInfo.connection[tsIndex].status;
      }
    }
  }
  else
  {
    if (strcmp(serverName, PM_SERVER_NAME) == 0)
    {
      // connection status
      if (pmInfo.connection.status == CONNECT)
      {
        if(pmInfo.connection.preStatus != pmInfo.connection.status)
        {
          monitor->count += 1;
          monitor->updateServerStatus = 1;
          monitor->pmServerStatus = 1;
          pmInfo.connection.preStatus = pmInfo.connection.status;
        }
        
        // set server status
        int bookConnectedCounter = 0,
            orderConnectedCounter = 0;
        for(i = 0; i < PM_MAX_BOOK_CONNECTIONS; i++)
        {
          bookConnectedCounter += pmInfo.currentStatus.pmMdcStatus[i];
        }
        
        for(i = 0; i < PM_MAX_ORDER_CONNECTIONS; i++)
        {
          orderConnectedCounter += pmInfo.currentStatus.pmOecStatus[i];
        }
        
        int feedConnectedCounter = pmInfo.currentStatus.statusCTS_Feed + pmInfo.currentStatus.statusCQS_Feed +
                                    pmInfo.currentStatus.statusUQDF_Feed + pmInfo.currentStatus.statusUTDF;
                                    
        if (bookConnectedCounter == PM_MAX_BOOK_CONNECTIONS &&
            orderConnectedCounter == PM_MAX_ORDER_CONNECTIONS && 
            feedConnectedCounter == 4)
        {
          pmInfo.currentStatus.serverStatus = COM__RGM__DAEDALUS__PROTO__SERVER_CONNECTIONS_STATUS__CONNECTED;
        }
        else if (bookConnectedCounter == 0 && orderConnectedCounter == 0)
        {
          pmInfo.currentStatus.serverStatus = COM__RGM__DAEDALUS__PROTO__SERVER_CONNECTIONS_STATUS__DISCONNECTED;
        }
        else
        {
          pmInfo.currentStatus.serverStatus = COM__RGM__DAEDALUS__PROTO__SERVER_CONNECTIONS_STATUS__SOME_CONNECTED;
        }
        
        if(pmInfo.preStatus.serverStatus != pmInfo.currentStatus.serverStatus)
        {
          monitor->count += 1;
          monitor->updateServerStatus = 1;
          monitor->pmServerStatus = 1;
          pmInfo.preStatus.serverStatus = pmInfo.currentStatus.serverStatus;
        }
      }
      else
      {
        if(pmInfo.connection.preStatus != pmInfo.connection.status)
        {
          monitor->count += 1;
          monitor->updateServerStatus = 1;
          monitor->pmServerStatus = 1;
          pmInfo.connection.preStatus = pmInfo.connection.status;
        }
      }
    }
    else
    {
      // Num Stucks
      if(traderToolsInfo.preStatus.numStuck != traderToolsInfo.currentStatus.numStuck)
      {
        monitor->count += 1;
        monitor->updateServerInfo = 1;
        monitor->asServerStatus = 1;
        traderToolsInfo.preStatus.numStuck = traderToolsInfo.currentStatus.numStuck;
      }
      
      // Num Losers
      if(traderToolsInfo.preStatus.numLoser != traderToolsInfo.currentStatus.numLoser)
      {
        monitor->count += 1;
        monitor->updateServerInfo = 1;
        monitor->asServerStatus = 1;
        traderToolsInfo.preStatus.numLoser = traderToolsInfo.currentStatus.numLoser;
      }
      
      // Buying Power
      if(!feq(traderToolsInfo.preStatus.buyingPower[accountIndex], traderToolsInfo.currentStatus.buyingPower[accountIndex]))
      {
        monitor->count += 1;
        monitor->updateServerInfo = 1;
        monitor->asServerStatus = 1;
        traderToolsInfo.preStatus.buyingPower[accountIndex] = traderToolsInfo.currentStatus.buyingPower[accountIndex];
      }
      
      
      // set server status
      int orderConnectedCounter = 0;
      int orderIndex;
      for (orderIndex = 0; orderIndex < AS_MAX_ORDER_CONNECTIONS; orderIndex++)
      {
        orderConnectedCounter += orderStatusMgmt[orderIndex].isOrderConnected;
      }
      
      if (orderConnectedCounter == AS_MAX_ORDER_CONNECTIONS)
      {
        traderToolsInfo.currentStatus.serverStatus = COM__RGM__DAEDALUS__PROTO__SERVER_CONNECTIONS_STATUS__CONNECTED;
      }
      else if (orderConnectedCounter == 0)
      {
        traderToolsInfo.currentStatus.serverStatus = COM__RGM__DAEDALUS__PROTO__SERVER_CONNECTIONS_STATUS__DISCONNECTED;
      }
      else
      {
        traderToolsInfo.currentStatus.serverStatus = COM__RGM__DAEDALUS__PROTO__SERVER_CONNECTIONS_STATUS__SOME_CONNECTED;
      }
      
      if(traderToolsInfo.preStatus.serverStatus != traderToolsInfo.currentStatus.serverStatus)
      {
        monitor->count += 1;
        monitor->updateServerStatus = 1;
        monitor->asServerStatus = 1;
        traderToolsInfo.preStatus.serverStatus = traderToolsInfo.currentStatus.serverStatus;
      }
    }
  }
  
  return monitor->count;
}

int MonitorStatusChange(t_MonitorStatusChange *monitor)
{
  int tsIndex, i;
  
  for (tsIndex = 0; tsIndex < tradeServersInfo.maxConnections; tsIndex++)
  {
    // Check if TS Server status changed
    monitor->count += MonitorServerStatusChange(monitor, tsIndex, tradeServersInfo.config[tsIndex].description, (int)TradingAccount.accountForTS[tsIndex]);
    
    // Check if TS Mdc status changed
    for(i = 0 ; i < TS_MAX_BOOK_CONNECTIONS; i++)
    {
      if(tradeServersInfo.preStatus[tsIndex].tsMdcStatus[i] != tradeServersInfo.currentStatus[tsIndex].tsMdcStatus[i])
      {
        monitor->count += 1;
        monitor->updateServerStatus = 1;
        monitor->tsMdcStatus[tsIndex][i] = 1;
        tradeServersInfo.preStatus[tsIndex].tsMdcStatus[i] = tradeServersInfo.currentStatus[tsIndex].tsMdcStatus[i];
      }
    }
    
    if(tradeServersInfo.preStatus[tsIndex].statusCQS != tradeServersInfo.currentStatus[tsIndex].statusCQS)
    {
      monitor->count += 1;
      monitor->updateServerStatus = 1;
      monitor->tsAwayMarketMdcStatus[tsIndex][CQS_BOOK] = 1;
      tradeServersInfo.preStatus[tsIndex].statusCQS = tradeServersInfo.currentStatus[tsIndex].statusCQS;
    }
    
    if(tradeServersInfo.preStatus[tsIndex].statusUQDF != tradeServersInfo.currentStatus[tsIndex].statusUQDF)
    {
      monitor->count += 1;
      monitor->updateServerStatus = 1;
      monitor->tsAwayMarketMdcStatus[tsIndex][UQDF_BOOK] = 1;
      tradeServersInfo.preStatus[tsIndex].statusUQDF = tradeServersInfo.currentStatus[tsIndex].statusUQDF;
    }
    
    // Check if TS OEC status changed
    for(i = 0 ; i < TS_MAX_ORDER_CONNECTIONS; i++)
    {
      if((tradeServersInfo.preStatus[tsIndex].tsOecStatus[i] != tradeServersInfo.currentStatus[tsIndex].tsOecStatus[i])
          || (tradeServersInfo.preStatus[tsIndex].tsTradingStatus[i] != tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[i])
         )
      {
        monitor->count += 1;
        monitor->updateServerStatus = 1;
        monitor->tsOecStatus[tsIndex][i] = 1;
        tradeServersInfo.preStatus[tsIndex].tsOecStatus[i] = tradeServersInfo.currentStatus[tsIndex].tsOecStatus[i];
        tradeServersInfo.preStatus[tsIndex].tsTradingStatus[i] = tradeServersInfo.currentStatus[tsIndex].tsTradingStatus[i];
      }
    }
  }
  
  // Check if PM Server status changed
  int serverID = -1;
  monitor->count += MonitorServerStatusChange(monitor, serverID, PM_SERVER_NAME, -1);
  
  // Check if AS Server status changed
  int accountIndex;
  for (accountIndex = 0; accountIndex < MAX_ACCOUNT; accountIndex++)
  {
    serverID--;
    monitor->count += MonitorServerStatusChange(monitor, serverID, AS_SERVER_NAME, accountIndex);
  }
  
  // Check if PM MDC status changed
  for(i = 0; i < PM_MAX_BOOK_CONNECTIONS; i++)
  {
    if(pmInfo.preStatus.pmMdcStatus[i] != pmInfo.currentStatus.pmMdcStatus[i])
    {
      monitor->count += 1;
      monitor->updateServerStatus = 1;
      monitor->pmMdcStatus[i] = 1;
      pmInfo.preStatus.pmMdcStatus[i] = pmInfo.currentStatus.pmMdcStatus[i];
    }
  }
  
  // Away market
  if(pmInfo.preStatus.statusCQS_Feed != pmInfo.currentStatus.statusCQS_Feed)
  {
    monitor->count += 1;
    monitor->updateServerStatus = 1;
    monitor->pmAwayMarketMdcStatus[CQS_BOOK] = 1;
    pmInfo.preStatus.statusCQS_Feed = pmInfo.currentStatus.statusCQS_Feed;
  }
  
  if(pmInfo.preStatus.statusUQDF_Feed != pmInfo.currentStatus.statusUQDF_Feed)
  {
    monitor->count += 1;
    monitor->updateServerStatus = 1;
    monitor->pmAwayMarketMdcStatus[UQDF_BOOK] = 1;
    pmInfo.preStatus.statusUQDF_Feed = pmInfo.currentStatus.statusUQDF_Feed;
  }
  
  if(pmInfo.preStatus.statusCTS_Feed != pmInfo.currentStatus.statusCTS_Feed)
  {
    monitor->count += 1;
    monitor->updateServerStatus = 1;
    monitor->pmAwayMarketMdcStatus[CTS_BOOK] = 1;
    pmInfo.preStatus.statusCTS_Feed = pmInfo.currentStatus.statusCTS_Feed;
  }
  
  if(pmInfo.preStatus.statusUTDF != pmInfo.currentStatus.statusUTDF)
  {
    monitor->count += 1;
    monitor->updateServerStatus = 1;
    monitor->pmAwayMarketMdcStatus[UTDF_BOOK] = 1;
    pmInfo.preStatus.statusUTDF = pmInfo.currentStatus.statusUTDF;
  }
  
  // Check if PM OEC status changed
  for(i = 0; i < PM_MAX_ORDER_CONNECTIONS; i++)
  {
    if(pmInfo.preStatus.pmOecStatus[i] != pmInfo.currentStatus.pmOecStatus[i])
    {
      monitor->count += 1;
      monitor->updateServerStatus = 1;
      monitor->pmOecStatus[i] = 1;
      pmInfo.preStatus.pmOecStatus[i] = pmInfo.currentStatus.pmOecStatus[i];
      pmInfo.preStatus.tradingStatus = pmInfo.currentStatus.tradingStatus;
    }
  }
  
  if(pmInfo.preStatus.tradingStatus != pmInfo.currentStatus.tradingStatus)
  {
    monitor->count += 1;
    monitor->updateServerStatus = 1;
    monitor->pmOecStatus[PM_ARCA_DIRECT_INDEX] = 1;
    monitor->pmOecStatus[PM_NASDAQ_RASH_INDEX] = 1;
    pmInfo.preStatus.tradingStatus = pmInfo.currentStatus.tradingStatus;
  }
  
  // Check if AS OEC status changed
  if(orderStatusMgmt[AS_ARCA_DIRECT_INDEX].isOrderConnected != orderStatusMgmt[AS_ARCA_DIRECT_INDEX].preIsOrderConnected)
  {
    monitor->count += 1;
    monitor->updateServerStatus = 1;
    monitor->asOecStatus[AS_ARCA_DIRECT_INDEX] = 1;
    orderStatusMgmt[AS_ARCA_DIRECT_INDEX].preIsOrderConnected = orderStatusMgmt[AS_ARCA_DIRECT_INDEX].isOrderConnected;
  }
  
  return monitor->count;
}

int CheckStatusChanges()
{
  t_MonitorStatusChange monitor;
  memset(&monitor, 0, sizeof(t_MonitorStatusChange));
  
  MonitorStatusChange(&monitor);
  
  if(monitor.count > 0)
  {
    t_ConnectionStatus *connection;
    int ttIndex;
    for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
    {
      connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];
      if ( (connection->status == DISCONNECT) || (connection->socket == -1) || (connection->clientType != CLIENT_TOOL_DAEDALUS) )
      {
        continue;
      }
      SendServersCurrentStatusToDaedalus(&monitor, connection);
    }
  }
  
  return SUCCESS;
}

int ResetTSStatus(int tsIndex)
{
  t_MonitorStatusChange monitor;
  memset(&monitor, 0, sizeof(t_MonitorStatusChange));
  
  monitor.tsServerStatus[tsIndex] = 1;
  int i;
  for(i = 0; i < TS_MAX_BOOK_CONNECTIONS; i++)
  {
    tradeServersInfo.currentStatus[tsIndex].tsMdcStatus[i] = DISCONNECT;
    monitor.tsMdcStatus[tsIndex][i] = 1;
  }
  
  for(i = 0; i < 2; i++)
  {
    monitor.tsAwayMarketMdcStatus[tsIndex][i] = 1;
  }
  
  for(i = 0; i < TS_MAX_ORDER_CONNECTIONS; i++)
  {
    tradeServersInfo.currentStatus[tsIndex].tsOecStatus[i] = DISCONNECT;
    monitor.tsOecStatus[tsIndex][i] = 1;
  }
  
  t_ConnectionStatus *connection;
  int ttIndex, ret = SUCCESS;
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];
    if ( (connection->status == DISCONNECT) || (connection->socket == -1) || (connection->clientType != CLIENT_TOOL_DAEDALUS) )
    {
      continue;
    }
    
    ret += SendTSServerCurrentStatusToDaedalus(&monitor, connection, tsIndex);
  }
  
  return ret;
}

int ResetPMStatus()
{
  pmInfo.currentStatus.statusCTS_Feed = DISCONNECT;
  pmInfo.currentStatus.statusCQS_Feed = DISCONNECT;
  pmInfo.currentStatus.statusUQDF_Feed = DISCONNECT;
  pmInfo.currentStatus.statusUTDF = DISCONNECT;
  
  t_MonitorStatusChange monitor;
  memset(&monitor, 0, sizeof(t_MonitorStatusChange));
  monitor.pmServerStatus = 1;
  
  int i;
  for(i = 0; i < PM_MAX_BOOK_CONNECTIONS; i++)
  {
    pmInfo.currentStatus.pmMdcStatus[i] = DISCONNECT;
    monitor.pmMdcStatus[i] = 1;
  }
  
  for(i = 0; i < 4; i++)
  {
    monitor.pmAwayMarketMdcStatus[i] = 1;
  }
  
  for(i = 0; i < PM_MAX_ORDER_CONNECTIONS; i++)
  {
    pmInfo.currentStatus.pmOecStatus[i] = DISCONNECT;
    monitor.pmOecStatus[i] = 1;
  }
  
  t_ConnectionStatus *connection;
  int ttIndex, ret = SUCCESS;
  for (ttIndex = 0; ttIndex < MAX_TRADER_TOOL_CONNECTIONS; ttIndex++)
  {
    connection = (t_ConnectionStatus *)&traderToolsInfo.connection[ttIndex];
    if ( (connection->status == DISCONNECT) || (connection->socket == -1) || (connection->clientType != CLIENT_TOOL_DAEDALUS) )
    {
      continue;
    }
    ret += SendPMServerCurrentStatusToDaedalus(&monitor, connection);
  }
  
  return ret;
}
