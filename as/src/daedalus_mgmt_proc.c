/*****************************************************************************
**  Project:    Equity Arbitrage Application
**  Filename:   daedalus_mgmt_proc.c
**  Description:  This file contains function definitions that were declared
          in daedalus_mgmt_proc.h 
**  Author:     Long Nguyen-Khoa-Hai
**  First created:  01-Oct-2013
**  Last updated: -----------
*****************************************************************************/

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include <errno.h>
#include <pthread.h>
#include <unistd.h>

#include "configuration.h"
#include "socket_util.h"
#include "database_util.h"
#include "daedalus_proc.h"
#include "trader_tools_proc.h"
#include "daedalus_mgmt_proc.h"

/****************************************************************************
** Global variables definition
****************************************************************************/

/****************************************************************************
** Function declarations
****************************************************************************/

static int HttpRequestComplete(const char* msg)
{
    // A valid HTTP request will be terminated with an empty line which will
    // appear in the message text as two consecutive CRLFs

    if (msg == NULL)
        return 0;

    const char *s = strstr(msg, "\r\n\r\n");
    if (s == NULL)
        return 0;

    return 1;
}

int ProcessLoginSuccess(int ttIndex)
{
  traderToolsInfo.connection[ttIndex].statusUpdate = SUCCESS;
            
  if (traderToolsInfo.connection[ttIndex].clientType == CLIENT_TOOL_DAEDALUS)
  {
    TraceLog(DEBUG_LEVEL, "Daedalus is connecting...\n");
    
    // Reset parameters to send
    ProcessResetDaedalusParameters(ttIndex);
    
    // Thread ids
    pthread_t threadReceiveMsgsFromDaedalus_id;
    pthread_t threadSendMsgsToDaedalus_id;
    
    // Create threads
    pthread_create (&threadReceiveMsgsFromDaedalus_id , 
            NULL, 
            (void *) &ThreadReceiveMsgsFromDaedalus, 
            (void *) &traderToolsInfo.connection[ttIndex]);
    sleep(2);

    pthread_create (&threadSendMsgsToDaedalus_id , 
            NULL, 
            (void *) &ThreadSendMsgsToDaedalus,
            (void *) &traderToolsInfo.connection[ttIndex]);
            
    // Waiting for create thread completed
    sleep(1);
  }
  else if (traderToolsInfo.connection[ttIndex].clientType == CLIENT_TOOL_TRADER_TOOL)
  {
    TraceLog(DEBUG_LEVEL, "TT is connecting...\n");
    
    // Reset parameters to send
    ProcessResetTTParameters(ttIndex);

    // Thread ids
    pthread_t threadReceiveMsgsFromTraderTool_id;
    pthread_t threadSendMsgsToTraderTool_id;
    
    // Create threads
    pthread_create (&threadReceiveMsgsFromTraderTool_id , 
            NULL, 
            (void *) &ReceiveMsgsFromTraderTool, 
            (void *) &traderToolsInfo.connection[ttIndex]);
    sleep(2);

    pthread_create (&threadSendMsgsToTraderTool_id , 
            NULL, 
            (void *) &SendMsgsToTraderTool, 
            (void *) &traderToolsInfo.connection[ttIndex]);

    // Waiting for create thread completed
    sleep(1);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessDaedalus
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
void *ProcessDaedalus(void *pthread_arg)
{
  while (1)
  {
    // Initialize socket to listen connection from Daedalus
    int serverSocket = -1;
    serverSocket = Listen2Client(traderToolsInfo.config.port, MAX_TRADER_TOOL_CONNECTIONS);
    
    if (serverSocket == ERROR)
    {
      //We cannot use the provided port
      //Listen2Client has given the reason, so we do nothing
      exit(0);
      return NULL;
    }
    
    // Waiting for connection from Daedalus
    while (1)
    {
      TraceLog(DEBUG_LEVEL, "***************************************************************\n");
      TraceLog(DEBUG_LEVEL, "Waiting for connection from client...\n");

      int clientSocket = -1;
      clientSocket = accept(serverSocket, NULL, NULL);
      
      if (clientSocket != ERROR)
      {
        TraceLog(DEBUG_LEVEL, "***New client was accepted at socket descriptor = %d ***\n", clientSocket);

        // Find index for
        int i, countConnection = 0, ttIndex = -1;

        // Check number of connections for Daedalus
        for (i = 0; i < MAX_TRADER_TOOL_CONNECTIONS; i++)
        {
          if (traderToolsInfo.connection[i].status == CONNECT)
          {
            countConnection ++;
          }
          else
          {
            ttIndex = i;
          }
        }

        if (countConnection == MAX_TRADER_TOOL_CONNECTIONS)
        {
          TraceLog(ERROR_LEVEL, "Aggregation server deny connect from client, cause number of client connections is limited, maxConnections = %d\n", MAX_TRADER_TOOL_CONNECTIONS);

          // Close the client socket
          close(clientSocket);
        }
        else if (countConnection >= traderToolsInfo.config.maxConnections)
        {
          TraceLog(ERROR_LEVEL, "Aggregation server deny connect from client, cause number of client connections is limited, countConnection = %d, maxConnections = %d\n", countConnection, traderToolsInfo.config.maxConnections);

          // Close the client socket
          close(clientSocket);
        }
        else
        {
          // Set timeout for client socket
          struct timeval tv;
          tv.tv_sec = 5;
          tv.tv_usec = 0;
          
          socklen_t optlen = sizeof(tv);
          
          if (setsockopt(clientSocket, SOL_SOCKET, SO_SNDTIMEO, &tv, optlen) == ERROR)
          {
            TraceLog(ERROR_LEVEL, "Cannot set send-timeout for client socket %d\n", clientSocket);
          }
          
          if (getsockopt(clientSocket, SOL_SOCKET, SO_SNDTIMEO, &tv, &optlen) == ERROR)
          {
            TraceLog(ERROR_LEVEL, "Cannot get send-timeout for client socket %d\n", clientSocket);
          }
          else
          {
            TraceLog(DEBUG_LEVEL, "getsockopt with SO_SNDTIMEO for socket descriptor %d: %d(s) + %d(us)\n", clientSocket, (long)tv.tv_sec, (long)tv.tv_usec);
          }
          
          //Set recv time out
          tv.tv_sec = 10;
          tv.tv_usec = 0;
          if (setsockopt(clientSocket, SOL_SOCKET, SO_RCVTIMEO, &tv, optlen) == ERROR)
          {
            TraceLog(ERROR_LEVEL, "Cannot set receive-timeout for client socket %d\n", clientSocket);
          }
          
          // Update Daedalus conf
          traderToolsInfo.connection[ttIndex].index = ttIndex;
          traderToolsInfo.connection[ttIndex].status = CONNECT;
          traderToolsInfo.connection[ttIndex].socket = clientSocket;

          t_ConnectionStatus *connection = &traderToolsInfo.connection[ttIndex];
          
          // Determine which client tool is connecting (TT or Daedalus)
          // then call the appropriate thread
          t_TTMessage ttMessage;
          memset(&ttMessage, 0, sizeof(ttMessage));
          
          // Waiting for handhake processing
          int isError = NO;
          int numReceivedBytes = 0;
          int recvBytes;
          while (!HttpRequestComplete((char *)ttMessage.msgContent))
          {
            errno = 0;
            recvBytes = recv(clientSocket, (char*)&ttMessage.msgContent[numReceivedBytes], TT_MAX_MSG_LEN - numReceivedBytes, 0);
            
            if(recvBytes > 0)
            {
              numReceivedBytes += recvBytes;
            }
            else if ( (recvBytes == -1) && (errno == EAGAIN) ) //Timeout
            {
              TraceLog(ERROR_LEVEL, "Client Connection: Socket receive-timeout\n");
              //Close connection
              CloseConnection(connection);
              isError = YES;
              break;
            }
            else if ( (recvBytes == 0) && (errno == 0) )  //Server Disconnected
            {
              TraceLog(ERROR_LEVEL, "Client Connection: Connection is terminated\n");
              //Close connection
              CloseConnection(connection);
              isError = YES;
              break;
            }
            else
            {
              TraceLog(ERROR_LEVEL, "Client Connection: recv() returned %d, errno: %d\n", numReceivedBytes, errno);
              PrintErrStr(ERROR_LEVEL, "recv(): ", errno);
              //Close connection
              CloseConnection(connection);
              isError = YES;
              break;
            }
          }
          
          if (isError == YES) continue;
          
          // Handshake processing
          ttMessage.msgLen = numReceivedBytes;
          if (ProcessWebsocketHandshake((void *)&ttMessage, traderToolsInfo.connection[ttIndex].index) == ERROR)
          {
            TraceLog(ERROR_LEVEL, "Can not process Handshake message successfully\n");
            //Close connection
            CloseConnection(connection);
            continue;
          }
          
          TraceLog(DEBUG_LEVEL, "Processed handshake successfully\n");
          
          // We must send handshake response here because SendDaedalusMessage is using data framing
          if (traderToolsInfo.connection[ttIndex].clientType == CLIENT_TOOL_DAEDALUS)
          {
            TraceLog(DEBUG_LEVEL, "Sending handshake response to Daedalus...\n");
            char *response = "HTTP/1.1 101 Switching Protocols\x0D\x0A\x0D\x0A";
            ttMessage.msgLen = strlen(response);
            strncpy((char *)&ttMessage.msgContent, response, ttMessage.msgLen);
            int result = SUCCESS;
            int bytesSent = send(clientSocket, ttMessage.msgContent, ttMessage.msgLen, 0);
            if (bytesSent != ttMessage.msgLen)
            {
              TraceLog(ERROR_LEVEL, "send data failed: bytesSent(%d), msgLen(%d)\n", bytesSent, ttMessage.msgLen);
              result = ERROR;
            }
    
            if(result != SUCCESS)
            {
              TraceLog(ERROR_LEVEL, "ERROR: handshake sent response\n");
              TraceLog(ERROR_LEVEL, "Close '%s' connection\n", connection->userName);
              //Close connection
              CloseConnection(connection);
              continue;
            }
          }
          
          usleep(500);

          // Check login authentication
          int loginResult;
          
          if (connection->authenticationMethod == LDAP_AUTHENTICATION_METHOD)
          {
            loginResult = LdapAuthentication(connection->userName, connection->password);
          }
          else
          {
            loginResult = AuthenticateLoginInfo(connection->userName, connection->password);
          }

          if (loginResult == SUCCESS)
          {
            ProcessLoginSuccess(ttIndex);
          }
          else if (loginResult == USER_PASSWORD_INCORRECT) 
          {
            connection->statusUpdate = USER_PASSWORD_INCORRECT;
            
            TraceLog(DEBUG_LEVEL, "Login fail, userName: %s, reason: USER_PASSWORD_INCORRECT\n", connection->userName);
            
            if (traderToolsInfo.connection[ttIndex].clientType == CLIENT_TOOL_DAEDALUS)
            {
              //Send message to permit trader to TT
              SendMessageForPermitTraderLoginToDaedalus(connection->index, connection->userName, USER_PASSWORD_INCORRECT);
            }
            else if (traderToolsInfo.connection[ttIndex].clientType == CLIENT_TOOL_TRADER_TOOL)
            {
              //Send message to permit trader to TT
              SendMessageForPermitTraderLoginToTT(connection->index, connection->userName, USER_PASSWORD_INCORRECT);
            }
              
            //Close connection
            CloseConnection(connection);
          }
          else if (loginResult == USER_LOGGED)
          {     
            int i;
            for (i = 0; i < MAX_TRADER_TOOL_CONNECTIONS; i++)
            {
              t_ConnectionStatus *theConnection = (t_ConnectionStatus *)&traderToolsInfo.connection[i];
              if ( (theConnection->status == DISCONNECT) || (theConnection->socket == -1) )
              {
                continue;
              }
              
              if(theConnection->statusUpdate == SUCCESS && (strcmp(theConnection->userName, connection->userName) == 0) )
              {
                char activityDetail[MAX_TRADER_LOG_LEN] = "\0";
                sprintf(activityDetail, "%d", theConnection->currentState);
                
                //Trader disconnect from AS
                ProcessUpdateTTEventLogToDatabase(theConnection->userName, ACTIVITY_TYPE_DISCONNECT_FROM_AS, activityDetail);
                
                //Update trader status to offline
                ProcessUpdateTraderStatusToDatabase(theConnection->userName, OFFLINE);
                
                TraceLog(DEBUG_LEVEL, "Disconnect userName(%s) logged\n", connection->userName);
                
                CloseConnection(theConnection);
                break;
              }
            }
            
            ProcessLoginSuccess(ttIndex);
          }
          else
          {
            //Close connection
            CloseConnection(connection);
          }
        }
      }
    }
    
    // Close the socket
    close(serverSocket);
  }
  
    return NULL;
}

void *SendCurrentStatusToDaedalus(void *pthread_arg)
{
  while (1)
  { 
    CheckStatusChanges();
    usleep(TT_SEND_DELAY);
  }
  
  return NULL;
}

/***************************************************************************/
