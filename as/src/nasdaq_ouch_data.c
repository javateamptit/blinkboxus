/*****************************************************************************
**  Project:    Equity Arbitrage Application
**  Filename:   nasdaq_ouch_data.c
**  Description:  This file contains function definitions that were declared
          in nasdaq_ouch_data.h 
**  Author:     Luan Vo-Kinh
**  First created:  13-Sep-2007
**  Last updated: -----------
*****************************************************************************/

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#define _GNU_SOURCE

#include "configuration.h"
#include "order_mgmt_proc.h"
#include "arca_direct_data.h"
#include "nasdaq_ouch_data.h"
#include "nasdaq_rash_data.h"
#include "bats_boe_data.h"
#include "database_util.h"
#include "query_data_mgmt.h"
#include "raw_data_mgmt.h"
#include "daedalus_proc.h"
#include "hbitime.h"

#include <limits.h>

/****************************************************************************
** Global variables definition
****************************************************************************/

/****************************************************************************
** Function declarations
****************************************************************************/

/****************************************************************************
- Function name:  ParseRawDataForOUCHOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ParseRawDataForOUCHOrder( t_DataBlock dataBlock, int type, const int oecType )
{
  //Get timestamp from epoch with format <second from epoch>.<microsecond> (eg. 124212553634.3235235)
  char timeStamp[32];
  long usec;
  
  if( dataBlock.msgContent[0] == 'S' )
  {
    usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]) / 1000;
    sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
    
    switch ( dataBlock.msgContent[1] )
    {
      // Accepted order
      case 'A':
        ProcessAcceptedOrderOfOUCHOrder( dataBlock, type, timeStamp, oecType );
        break;
      
      // Canceled order
      case 'C':
        ProcessCanceledOrderOfOUCHOrder( dataBlock, type, timeStamp, oecType );
        break;
      
      // Executed order
      case 'E':
        ProcessExecutedOrderOfOUCHOrder( dataBlock, type, timeStamp, oecType );
        break;

      // Rejected order
      case 'J':
        ProcessRejectedOrderOfOUCHOrder( dataBlock, type, timeStamp, oecType );
        break;

      // Broken trade
      case 'B':
        ProcessBrokenOrderOfOUCHOrder( dataBlock, type, oecType );
        break;
      
      // Price correction
      case 'K':
        ProcessPriceCorrectionOfOUCHOrder( dataBlock, type, oecType );
        break;
        
      // Cancel pending     
      case 'P':
        ProcessCancelPendingOfOUCHOrder( dataBlock, type, oecType );
        
        break;
        
      // Cancel reject      
      case 'I':
        ProcessCancelRejectOfOUCHOrder( dataBlock, type, oecType);
        break;

      default:
        TraceLog(ERROR_LEVEL, "%s Order: Invalid order message, msgType = %c ASCII = %d\n", GetOECName(oecType), dataBlock.msgContent[1], dataBlock.msgContent[1] );
        break;
    }
  }
  else if( dataBlock.msgContent[0] == 'U' )
  {
    usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]) / 1000;
    sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
    // New Order
    if( dataBlock.msgContent[1] == 'O' )
    {
      ProcessNewOrderOfOUCHOrder(dataBlock, type, timeStamp, oecType);
    }
    // Cancel Request
    else if( dataBlock.msgContent[1] == 'X' )
    {
      ProcessCancelRequestOfOUCHOrder(dataBlock, type, timeStamp, oecType);
    }
    else
    {
      TraceLog(ERROR_LEVEL, "%s: Invalid order message, msgType = %c ASCII = %d\n", GetOECName(oecType), dataBlock.msgContent[1], dataBlock.msgContent[1] );
    }
  }
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessNewOrderOfOUCHOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessNewOrderOfOUCHOrder( t_DataBlock dataBlock, int type, char *timeStamp, const int oecType )
{
  t_OUCHNewOrder newOrder;
  t_OpenOrderInfo openOrder;
  t_OrderDetail orderDetail;

  /*------------------------------------------------
  Order summary:
  - double price;
  - double avgPrice;
  - char symbol[SYMBOL_LEN];
  - int side;
  - int shares;
  - int leftShares; 
  - int ECNId;
  - int clOrdId;
  - int status;
  - int tif;
  - int tsId;
  - char timestamp[MAX_TIMESTAMP];
  ------------------------------------------------*/

  /*------------------------------------------------
  Order detail:
  - int length;
  - int execId;
  - int isBroken;
  - char timestamp[MAX_TIMESTAMP];
  - char msgContent[MAX_ORDER_MSG];
  ------------------------------------------------*/
  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;

  // YYYY/mm/DD
  char dateTime[32];
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]), &dateTime[0]);
  
  memcpy(newOrder.date, &dateTime[0], 10);
  newOrder.date[10] = 0;

  // HH:MM:SS.mss
  memcpy( newOrder.timestamp, &dateTime[11], TIME_LENGTH);
  newOrder.timestamp[TIME_LENGTH] = 0;

  // openOrder.timestamp
  memcpy( openOrder.timestamp, &dateTime[0], TIMESTAMP_LENGTH);
  openOrder.timestamp[TIMESTAMP_LENGTH] = 0;

  // orderDetail
  strcpy( orderDetail.timestamp, openOrder.timestamp );
  
  strcpy( newOrder.ecn, GetOECName(oecType) );

  // openOrder.ECNId
  openOrder.ECNId = oecType;

  // Status: Sent, Live, Rejected, Closed, BrokenTrade, CanceledByECN, 
  //      CXLSent, CancelACK, CancelRejected, CancelPending, CanceledByUser
  strcpy( newOrder.status, orderStatus.Sent );

  // openOrder.status
  openOrder.status = orderStatusIndex.Sent;
  
  // Message type
  strcpy( newOrder.msgType, "NewOrder" );
  
  char msgType = dataBlock.msgContent[1];
    
  // Order ID
  char fieldToken[16];
  memcpy( fieldToken, &dataBlock.msgContent[2], 14 );
  fieldToken[14] = 0;
  newOrder.orderID = atoi( &fieldToken[2] );
  
  // openOrder.clOrdId
  openOrder.clOrdId = newOrder.orderID;

  // "B", "S" or "SS"
  char action = dataBlock.msgContent[16]; 
  char buysell = '0';
  switch ( action )
  {
    case 'B':
      buysell = 'B';
      strcpy( newOrder.side, "BC" );
      openOrder.side = BUY_TO_CLOSE_TYPE;
      break;
    
    case 'S':
      buysell = 'S';
      strcpy( newOrder.side, "S" );     
      openOrder.side = SELL_TYPE;
      break;
    
    case 'T':
      buysell = 'T';
      strcpy( newOrder.side, "SS" );      
      openOrder.side = SHORT_SELL_TYPE;
      break;
    
    default:
      strcpy( newOrder.side, " " );
      openOrder.side = -1;

      TraceLog(ERROR_LEVEL, "%s Order: not support side = %c, %d\n", newOrder.ecn, action, action );
      break;
  }
  
  // Shares
  newOrder.shares = GetIntNumberBigEndian(&dataBlock.msgContent[17]);

  // openOrder.shares
  openOrder.shares = newOrder.shares;
  openOrder.leftShares = openOrder.shares;

  // Stock symbol
  strncpy( newOrder.symbol, (char *) &dataBlock.msgContent[21], SYMBOL_LEN);
  TrimRight( newOrder.symbol, strnlen(newOrder.symbol, SYMBOL_LEN) );

  // openOrder.symbol
  memcpy( openOrder.symbol, newOrder.symbol, SYMBOL_LEN );
  
  // Price
  newOrder.price = ((double)GetIntNumberBigEndian(&dataBlock.msgContent[29])) / 10000;

  // openOrder.price
  openOrder.price = newOrder.price;
  openOrder.avgPrice = openOrder.price;

  //Time In Force
  int tifValue = GetIntNumberBigEndian(&dataBlock.msgContent[33]);
  char _tif[6];
  
  if( tifValue == 0 ) 
  {
    strcpy(_tif, "0");
    strcpy( newOrder.timeInForce, "IOC");
    openOrder.tif = IOC_TYPE;
  }
  else if(tifValue > 0) 
  {
    strcpy(_tif, "99999");
    strcpy( newOrder.timeInForce, "DAY" );
    openOrder.tif = DAY_TYPE;
  }
  else
  {
    TraceLog(ERROR_LEVEL, "%s Error: TimeInForce = %d\n", newOrder.ecn, tifValue );

    openOrder.tif = -1;
  }
  
  // Firm
  memcpy( newOrder.firm, &dataBlock.msgContent[37], 4 );
  newOrder.firm[4] = 0;

  // Display
  newOrder.display = dataBlock.msgContent[41];
  char _visibility = newOrder.display;
  
  // Capacity
  newOrder.capacity = dataBlock.msgContent[42];
  
  // Intermarket sweep eligibility
  newOrder.eligibility = dataBlock.msgContent[43];
  
  // Min Quantity
  newOrder.minQuantity = GetIntNumberBigEndian(&dataBlock.msgContent[44]);
  
  // Cross Type
  newOrder.crossType = dataBlock.msgContent[48];

  // Sequence Number
  newOrder.seqNum = 0;
  
  int isISO = 0;
  short bboID = -1;
  unsigned char bbReason;
  if (newOrder.eligibility == 'Y')
  {
    isISO = 1;
    bboID = GetShortNumber(&dataBlock.addContent[DATABLOCK_OFFSET_BBO_ID]);
  }
  
  newOrder.bboId = bboID;
  
  //orderDetail.msgContent
  orderDetail.length = sprintf( orderDetail.msgContent, 
                  "%d=%c\001"   // fieldIndexList.MessageType
                  "%d=%s\001"   // fieldIndexList.Token
                  "%d=%c\001"   // fieldIndexList.Action
                  "%d=%d\001"   // fieldIndexList.Shares
                  "%d=%.8s\001" // fieldIndexList.Stock
                  "%d=%lf\001"  // fieldIndexList.Price
                  "%d=%d\001"   // fieldIndexList.TimeInForce
                  "%d=%s\001"   // fieldIndexList.Firm
                  "%d=%c\001"   // fieldIndexList.Display
                  "%d=%c\001"   // fieldIndexList.Capacity
                  "%d=%c\001"   // fieldIndexList.Eligibility
                  "%d=%d\001"   // fieldIndexList.MinQuantity
                  "%d=%c\001",  // fieldIndexList.CrossType
                  fieldIndexList.MessageType, msgType, 
                  fieldIndexList.Token, fieldToken, 
                  fieldIndexList.Action, action, 
                  fieldIndexList.Shares, newOrder.shares, 
                  fieldIndexList.Stock, newOrder.symbol, 
                  fieldIndexList.Price, newOrder.price, 
                  fieldIndexList.TimeInForce, tifValue,
                  fieldIndexList.Firm, newOrder.firm, 
                  fieldIndexList.Display, newOrder.display, 
                  fieldIndexList.Capacity, newOrder.capacity, 
                  fieldIndexList.Eligibility, newOrder.eligibility,
                  fieldIndexList.MinQuantity, newOrder.minQuantity,
                  fieldIndexList.CrossType, newOrder.crossType);
  
  strcpy( newOrder.msgContent, orderDetail.msgContent );
  
  // Update database
  if( type == MANUAL_ORDER )
  {
    // openOrder.tsId
    openOrder.tsId = -1;

    //newOrder.tsId
    newOrder.tsId = -1;

    // Trade type
    newOrder.tradeType = NORMALLY_TRADE;
    openOrder.tradeType = NORMALLY_TRADE;

    strcpy(newOrder.entryExit, "Exit");     
    strcpy(newOrder.ecnLaunch, " ");
    strcpy(newOrder.askBid, " ");
    
    //Add CrossID if it belogs to PM
    if (newOrder.orderID >= 500000)
    {
      bbReason = OJ_BB_REASON_PM;
      newOrder.crossId = GetIntNumber(&dataBlock.addContent[DATABLOCK_OFFSET_CROSS_ID]);
      openOrder.crossId = newOrder.crossId;
    }
    else
    {
      bbReason = OJ_BB_REASON_AS;
      newOrder.crossId = 0;
    }
  }
  else
  {
    if (dataBlock.addContent[DATABLOCK_OFFSET_LAUNCH] == EXIT_VALUE)
    {
      // Get Entry/Exit
      bbReason = OJ_BB_REASON_EXIT;
      strcpy(newOrder.entryExit, "Exit");
    
      strcpy(newOrder.ecnLaunch, " ");
      strcpy(newOrder.askBid, " ");
      
      newOrder.entryExitRefNum = *(unsigned short *)&dataBlock.addContent[DATABLOCK_OFFSET_ENTRY_EXIT_REF];
    }
    else
    {
      // Get Entry/Exit
      bbReason = OJ_BB_REASON_ENTRY;
      strcpy(newOrder.entryExit, "Entry");

      // Get ECN launch and Ask/Bid launch
      GetECNLaunchInfo(dataBlock.addContent[DATABLOCK_OFFSET_LAUNCH], newOrder.ecnLaunch, newOrder.askBid);
    }

    // Visible shares
    newOrder.visibleShares = GetIntNumber(&dataBlock.addContent[DATABLOCK_OFFSET_VISIBLE_SIZE]);
  
    //Cross Id
    newOrder.crossId = GetIntNumber(&dataBlock.addContent[DATABLOCK_OFFSET_CROSS_ID]);
    openOrder.crossId = newOrder.crossId;

    // Trade type
    newOrder.tradeType = NORMALLY_TRADE;
    openOrder.tradeType = newOrder.tradeType;

    // openOrder.tsId
    openOrder.tsId = tradeServersInfo.config[type].tsId;

    //newOrder.tsId
    newOrder.tsId = type;
    
    // Reset no trade duration
    time_t t = (time_t)hbitime_seconds();
    struct tm tm = *localtime(&t);

    NoTradesDurationMgt[type].currentNoTradesDurationInSecond = 0;
    NoTradesDurationMgt[type].alertThresholdInMinute = TradeTimeAlertConfig[3600 * tm.tm_hour + 60 * tm.tm_min + tm.tm_sec];

    sprintf(NoTradesDurationMgt[type].startTimeStamp, "%d:%d", tm.tm_hour, tm.tm_min);
  }

  newOrder.tradingAccount = dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID];
  openOrder.account = dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID];
  
  // Call function to add a open order
  ProcessAddASOpenOrder( &openOrder, &orderDetail );
  
  // Call function to update New Order of OUCH Order in Manual Order
  ProcessInsertNewOrder4OUCHQueryToCollection(&newOrder);
  
  //Add a new record to OJ file
  int isDuplicated = 0;
  
  if (AddOrderIdToList(&__new, newOrder.orderID) == ERROR)
  {
    isDuplicated = 1;
  }
  
  char account[32];
  GetAccountName(oecType, dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID], account);
  
  int _venueType = oecType - TYPE_BASE;
  if (isDuplicated == 0)
  {
    AddOJNewOrderRecord(timeStamp, newOrder.symbol, _venueType, buysell, newOrder.price, newOrder.shares, newOrder.orderID, _tif, _visibility, isISO, account, NULL, newOrder.capacity, newOrder.crossId, bbReason);
  }
  
  //Add ISO Order to ISO Collection for ISO Flight File creation
  if ((isISO == 1) && (isDuplicated == 0))
  {
    t_ISOOrderInfo isoOrderInfo;
    
    strcpy(isoOrderInfo.strTime, timeStamp);
    
    sprintf(isoOrderInfo.strVenueID, "%d", _venueType);
    
    isoOrderInfo.strBuysell[0] = buysell;
    isoOrderInfo.strBuysell[1] = 0;
    
    sprintf(isoOrderInfo.strPrice, "%.2lf", newOrder.price);
    
    sprintf(isoOrderInfo.strShares, "%d", newOrder.shares);
    
    sprintf(isoOrderInfo.strSequenceID, "%d", newOrder.orderID);
    
    isoOrderInfo.strCapacity[0] = newOrder.capacity;
    isoOrderInfo.strCapacity[1] = 0;
    
    strcpy(isoOrderInfo.strAccount, account);
    
    isoOrderInfo.strRoutingInfo[0] = 0; //No Routing for OUCH
    
    AddISOOrderToCollection(newOrder.crossId, bboID, newOrder.symbol, &isoOrderInfo);
  }
  
  CheckAndProcessRawdataInWaitListForOrderID(newOrder.orderID);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessAcceptedOrderOfOUCHOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessAcceptedOrderOfOUCHOrder( t_DataBlock dataBlock, int type, char *timeStamp, const int oecType )
{
  t_OUCHAcceptedOrder acceptedOrder;
  t_OpenOrderInfo openOrder;
  t_OrderDetail orderDetail;

  /*------------------------------------------------
  Order summary:
  - double price;
  - double avgPrice;
  - char symbol[SYMBOL_LEN];
  - int side;
  - int shares;
  - int leftShares; 
  - int ECNId;
  - int clOrdId;
  - int status;
  - int tif;
  - int tsId;
  - char timestamp[MAX_TIMESTAMP];
  ------------------------------------------------*/

  /*------------------------------------------------
  Order detail:
  - int length;
  - int execId;
  - int isBroken;
  - char timestamp[MAX_TIMESTAMP];
  - char msgContent[MAX_ORDER_MSG];
  ------------------------------------------------*/
  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;

  // YYYY/mm/DD
  char dateTime[32];
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]), &dateTime[0]);
  memcpy(acceptedOrder.processedTimestamp, &dateTime[11], TIME_LENGTH);
  acceptedOrder.processedTimestamp[TIME_LENGTH] = 0;
  
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]), &dateTime[0]);
  memcpy(acceptedOrder.date, &dateTime[0], 10);
  acceptedOrder.date[10] = 0;
  
  // HH:MM:SS.ms
  memcpy(acceptedOrder.timestamp, &dateTime[11], TIME_LENGTH);
  acceptedOrder.timestamp[TIME_LENGTH] = 0;

  // openOrder.timestamp
  memcpy(openOrder.timestamp, &dateTime[0], TIMESTAMP_LENGTH);
  openOrder.timestamp[TIMESTAMP_LENGTH] = 0;
  
  // orderDetail
  strcpy(orderDetail.timestamp, openOrder.timestamp);

  // openOrder.ECNId
  openOrder.ECNId = oecType;
  char _ecn[8] = "\0";
  strcpy(_ecn, GetOECName(oecType));
  
  // Status: Sent, Live, Rejected, Closed, BrokenTrade, CanceledByECN, 
  //      CXLSent, CancelACK, CancelRejected, CancelPending, CanceledByUser
  strcpy( acceptedOrder.status, orderStatus.Live );
  
  // openOrder.status
  openOrder.status = orderStatusIndex.Live;

  // Message type
  strcpy( acceptedOrder.msgType, "AcceptedOrder" );
  
  // MsgType
  char msgType = dataBlock.msgContent[1];
    
  // Sending Time
  // Get to total of nano seconds
  long totalnanos = GetLongNumberBigEndian((char*)&dataBlock.msgContent[2]);
  
  // Get the number of seconds
  int numSecs = totalnanos / 1000000000;
  
  // Get the number of nanoseconds
  int numNanos = totalnanos % 1000000000;
  
  // write to transtime
  sprintf(acceptedOrder.transTime, "%02d:%02d:%02d.%09d", numSecs / 3600, (numSecs % 3600) / 60, (numSecs % 3600) % 60, numNanos);
  
  // Order ID
  char fieldToken[15];
  memcpy( fieldToken, &dataBlock.msgContent[10], 14 );
  fieldToken[14] = 0;
  acceptedOrder.orderID = atoi( &fieldToken[2] );

  char exOrderId[32];
  exOrderId[0] = dataBlock.msgContent[10];
  exOrderId[1] = dataBlock.msgContent[11];
  sprintf(&exOrderId[2], "%d", acceptedOrder.orderID);
  
  // openOrder.clOrdId
  openOrder.clOrdId = acceptedOrder.orderID;
  
  // Action: "B", "S" or "SS"
  char action = dataBlock.msgContent[24];
  switch ( action )
  {
    case 'B':
      strcpy( acceptedOrder.side, "BC" );
      openOrder.side = BUY_TO_CLOSE_TYPE;
      break;
    
    case 'S':
      strcpy( acceptedOrder.side, "S" );      
      openOrder.side = SELL_TYPE;
      break;
    
    case 'T':
      strcpy( acceptedOrder.side, "SS" );     
      openOrder.side = SHORT_SELL_TYPE;
      break;
    
    default:
      strcpy( acceptedOrder.side, " " );
      openOrder.side = -1;

      TraceLog(ERROR_LEVEL, "%s Order: not support side = %c, %d\n", _ecn, action, action );
      break;
  }

  // Shares
  acceptedOrder.shares = GetIntNumberBigEndian(&dataBlock.msgContent[25]);

  // openOrder.shares
  openOrder.shares = acceptedOrder.shares;
  openOrder.leftShares = openOrder.shares;

  // Stock
  strncpy( acceptedOrder.symbol, (char *) &dataBlock.msgContent[29], SYMBOL_LEN);
  TrimRight( acceptedOrder.symbol, strnlen(acceptedOrder.symbol, SYMBOL_LEN) );

  // openOrder.symbol
  memcpy( openOrder.symbol, acceptedOrder.symbol, SYMBOL_LEN );

  // Price
  acceptedOrder.price =  ((double)GetIntNumberBigEndian(&dataBlock.msgContent[37])) / 10000;

  // openOrder.price
  openOrder.price = acceptedOrder.price;
  openOrder.avgPrice = openOrder.price;
  
  // Time In Force
  int tifValue = GetIntNumberBigEndian(&dataBlock.msgContent[41]);
  if( tifValue == 0 ) 
  {
    openOrder.tif = IOC_TYPE;
  }
  else if(tifValue > 0) 
  {
    openOrder.tif = DAY_TYPE;
  }
  else
  {
    TraceLog(ERROR_LEVEL, "%s Error: TimeInForce = %d\n", _ecn, tifValue );

    openOrder.tif = -1;
  }

  // Firm
  memcpy( acceptedOrder.firm, &dataBlock.msgContent[45], 4 );
  acceptedOrder.firm[4] = 0;

  // Display
  acceptedOrder.display = dataBlock.msgContent[49];

  // Order reference number
  acceptedOrder.ouchExOrderId = GetLongNumberBigEndian((char *)&dataBlock.msgContent[50]) % INT_MAX;
  
  int _serverId = (type == MANUAL_ORDER) ? MAX_TRADE_SERVER_CONNECTIONS : type;
  ExchangeOrderIdMapping[_serverId][acceptedOrder.orderID % MAX_ORDER_PLACEMENT] = acceptedOrder.ouchExOrderId;
  
  // Capacity
  acceptedOrder.capacity = dataBlock.msgContent[58];
  
  // Intermarket sweep eligibility
  acceptedOrder.eligibility = dataBlock.msgContent[59];
  
  // Min Quantity
  acceptedOrder.minQuantity = GetIntNumberBigEndian(&dataBlock.msgContent[60]);
  
  // Cross Type
  acceptedOrder.crossType = dataBlock.msgContent[64];
  
  // Order State
  acceptedOrder.orderState = dataBlock.msgContent[65];
  
  // Sequence number
  acceptedOrder.seqNum = 0;
  
  // BBO Weight indicator
  char fieldBBOWeightIndicator = dataBlock.msgContent[66];
  
  // orderDetail.msgContent
  orderDetail.length = sprintf( orderDetail.msgContent, 
                  "%d=%c\001"   // fieldIndexList.MessageType
                  "%d=%s\001"   // fieldIndexList.TimeStamp
                  "%d=%s\001"   // fieldIndexList.Token
                  "%d=%c\001"   // fieldIndexList.Action
                  "%d=%d\001"   // fieldIndexList.Shares
                  "%d=%.8s\001" // fieldIndexList.Stock
                  "%d=%lf\001"  // fieldIndexList.Price
                  "%d=%d\001"   // fieldIndexList.TimeInForce
                  "%d=%s\001"   // fieldIndexList.Firm
                  "%d=%c\001"   // fieldIndexList.Display
                  "%d=%lu\001"  // fieldIndexList.OrderRefNo
                  "%d=%c\001"   // fieldIndexList.Capacity
                  "%d=%c\001"   // fieldIndexList.Eligibility
                  "%d=%d\001"   // fieldIndexList.MinQuantity
                  "%d=%c\001"   // fieldIndexList.CrossType
                  "%d=%c\001"   // fieldIndexList.OrderState
                  "%d=%c\001",  // fieldIndexList.BBOWeightIndicator
                  fieldIndexList.MessageType, msgType, 
                  fieldIndexList.TimeStamp, acceptedOrder.transTime, 
                  fieldIndexList.Token, fieldToken, 
                  fieldIndexList.Action, action, 
                  fieldIndexList.Shares, acceptedOrder.shares, 
                  fieldIndexList.Stock, acceptedOrder.symbol, 
                  fieldIndexList.Price, acceptedOrder.price, 
                  fieldIndexList.TimeInForce, tifValue,
                  fieldIndexList.Firm, acceptedOrder.firm, 
                  fieldIndexList.Display, acceptedOrder.display, 
                  fieldIndexList.OrderRefNo, acceptedOrder.ouchExOrderId, 
                  fieldIndexList.Capacity, acceptedOrder.capacity, 
                  fieldIndexList.Eligibility, acceptedOrder.eligibility,
                  fieldIndexList.MinQuantity, acceptedOrder.minQuantity,
                  fieldIndexList.CrossType, acceptedOrder.crossType,
                  fieldIndexList.OrderState, acceptedOrder.orderState,
                  fieldIndexList.BBOWeightIndicator, fieldBBOWeightIndicator);
  
  strcpy( acceptedOrder.msgContent, orderDetail.msgContent );
  
  // Update database
  if( type == MANUAL_ORDER )
  {
    // openOrder.tsId
    openOrder.tsId = -1;
  }
  else
  {
    // openOrder.tsId
    openOrder.tsId = tradeServersInfo.config[type].tsId;
  }

  openOrder.account = dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID];
  
  // Call function to update a open order
  int indexOpenOrder = ProcessUpdateASOpenOrderForOrderACK( &openOrder, &orderDetail );
  
  if (indexOpenOrder == -2)
  {
    AddDataBlockToWaitList(oecType, WAIT_LIST_TYPE_OUCH_ORDER_ACK, acceptedOrder.orderID, dataBlock, type, timeStamp);
    return 0;
  }
  
  if (indexOpenOrder != ERROR) 
  {
    acceptedOrder.shares = asOpenOrder.orderCollection[indexOpenOrder].orderSummary.shares;
  } 
  
  // Check order state is DEAD
  if (acceptedOrder.orderState == 'D')
  {
    // order state = DEAD
    asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status = orderStatusIndex.CanceledByECN;
    
    strcpy(acceptedOrder.status, orderStatus.CanceledByECN);
  }
  
  // Call function to update Order ACK of OUCH
  ProcessInsertAcceptedOrder4OUCHQueryToCollection(&acceptedOrder);
  
  char ecnOrderID[32];
  sprintf(ecnOrderID, "%lu", acceptedOrder.ouchExOrderId);
  
  char account[32];
  GetAccountName(oecType, dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID], account);

  int _venueType = oecType - TYPE_BASE;
  if (AddOrderIdToList(&__ack, acceptedOrder.orderID) == SUCCESS)
  {
    AddOJOrderAcceptedRecord(timeStamp, acceptedOrder.symbol, exOrderId, acceptedOrder.orderID, ecnOrderID, _venueType, account, acceptedOrder.capacity);
  }
  
  if (acceptedOrder.orderState == 'D')
  {
    // This is special case for OUCH, that is when ACK comes but indicating order is dead, so we must add one more OJ record
    // to indicate order is canceled
    
    if (AddOrderIdToList(&__cancel, acceptedOrder.orderID) == SUCCESS)
    {
      AddOJOrderCancelResponseRecord(timeStamp, acceptedOrder.symbol, acceptedOrder.orderID, ecnOrderID, "Order%20Dead:%20order%20was%20accepted%20and%20atomically%20canceled.", _venueType, acceptedOrder.shares, 1, account);
    }
  }
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessExecutedOrderOfOUCHOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessExecutedOrderOfOUCHOrder( t_DataBlock dataBlock, int type, char *timeStamp, const int oecType )
{
  t_OUCHExecutedOrder executedOrder;
  t_OrderDetail orderDetail;

  /*------------------------------------------------
  Order detail:
  - int length;
  - int execId;
  - int isBroken;
  - char timestamp[MAX_TIMESTAMP];
  - char msgContent[MAX_ORDER_MSG];
  ------------------------------------------------*/
  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;

  // YYYY/mm/DD
  char dateTime[32];
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]), &dateTime[0]);
  memcpy( executedOrder.processedTimestamp, &dateTime[11], TIME_LENGTH);
  executedOrder.processedTimestamp[TIME_LENGTH] = 0;
  
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]), &dateTime[0]);
  memcpy(executedOrder.date, &dateTime[0], 10);
  executedOrder.date[10] = 0;
  
  // HH:MM:SS.ms
  memcpy( executedOrder.timestamp, &dateTime[11], TIME_LENGTH);
  executedOrder.timestamp[TIME_LENGTH] = 0;

  // orderDetail.timestamp
  memcpy( orderDetail.timestamp, &dateTime[0], TIMESTAMP_LENGTH);
  orderDetail.timestamp[TIMESTAMP_LENGTH] = 0;

  // MsgType
  char msgType = dataBlock.msgContent[1];
  
  // Sending time
  // Get to total of nano seconds
  long totalnanos = GetLongNumberBigEndian((char*)&dataBlock.msgContent[2]);
  
  // Get the number of seconds
  int numSecs = totalnanos / 1000000000;
  
  // Get the number of nanoseconds
  int numNanos = totalnanos % 1000000000;
  
  // write to transtime
  sprintf(executedOrder.transTime, "%02d:%02d:%02d.%09d", numSecs / 3600, (numSecs % 3600) / 60, (numSecs % 3600) % 60, numNanos);

  // Messsage type
  strcpy( executedOrder.msgType, "ExecutedOrder" ); 

  // OrderId
  char fieldToken[15];
  memcpy( fieldToken, &dataBlock.msgContent[10], 14);
  fieldToken[14] = 0;
  executedOrder.orderID = atoi( &fieldToken[2] );
  
  char exOrderId[32];
  exOrderId[0] = dataBlock.msgContent[10];
  exOrderId[1] = dataBlock.msgContent[11];
  sprintf(&exOrderId[2], "%d", executedOrder.orderID);

  // Shares
  executedOrder.shares = GetIntNumberBigEndian(&dataBlock.msgContent[24]);
  
  // Price
  executedOrder.price =  ((double)GetIntNumberBigEndian(&dataBlock.msgContent[28])) / 10000;
  
  // Liquidity flag
  executedOrder.liquidityIndicator = dataBlock.msgContent[32];

  // MatchNo
  executedOrder.executionId = GetLongNumberBigEndian((char *)&dataBlock.msgContent[33]);
  
  executedOrder.seqNum = 0;

  // orderDetail.msgContent
  orderDetail.execId = executedOrder.orderID;
  orderDetail.length = sprintf( orderDetail.msgContent, 
                  "%d=%c\001"   // fieldIndexList.MessageType
                  "%d=%s\001"   // fieldIndexList.TimeStamp
                  "%d=%s\001"   // fieldIndexList.Token
                  "%d=%d\001"   // fieldIndexList.Shares
                  "%d=%lf\001"  // fieldIndexList.Price
                  "%d=%c\001"   // fieldIndexList.LiquidityFlag
                  "%d=%ld\001",   // fieldIndexList.MatchNo
                  fieldIndexList.MessageType, msgType, 
                  fieldIndexList.TimeStamp, executedOrder.transTime, 
                  fieldIndexList.Token, fieldToken, 
                  fieldIndexList.Shares, executedOrder.shares, 
                  fieldIndexList.Price, executedOrder.price, 
                  fieldIndexList.LiquidityFlag, executedOrder.liquidityIndicator, 
                  fieldIndexList.MatchNo, executedOrder.executionId );
  
  strcpy( executedOrder.msgContent, orderDetail.msgContent );
  
  // Update database
  int tsId;

  if( type == MANUAL_ORDER )
  {
    // tsId
    tsId = MANUAL_ORDER;
  }
  else
  {
    // tsId
    tsId = tradeServersInfo.config[type].tsId;
  }
  
  char liquidityIndicator[2];
  liquidityIndicator[0] = executedOrder.liquidityIndicator;
  liquidityIndicator[1] = 0;

  // Call function to update a open order (non fill)
  int indexOpenOrder = ProcessUpdateASOpenOrderForExecuted(tsId, executedOrder.orderID, orderStatusIndex.Closed, executedOrder.shares, executedOrder.price, &orderDetail, dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID], liquidityIndicator, &executedOrder.fillFee);
  if (indexOpenOrder == -2)
  {
    AddDataBlockToWaitList(oecType, WAIT_LIST_TYPE_OUCH_ORDER_FILLED, executedOrder.orderID, dataBlock, type, timeStamp);
    return 0;
  }
  
  if (indexOpenOrder != ERROR)
  {
    strcpy(executedOrder.status, GetOrderStatus(asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status));
  
    executedOrder.leftShares = asOpenOrder.orderCollection[indexOpenOrder].orderSummary.leftShares;
    executedOrder.avgPrice = asOpenOrder.orderCollection[indexOpenOrder].orderSummary.avgPrice;
  }
  
  executedOrder.entryExitRefNum = *(unsigned short *)&dataBlock.addContent[DATABLOCK_OFFSET_ENTRY_EXIT_REF];
  
  // Call function to update Executed Order of OUCH
  ProcessInsertExecutedOrder4OUCHQueryToCollection(&executedOrder);
  
  //Duplicated
  if (AddExecDataToList(&__exec, executedOrder.orderID, executedOrder.executionId) == ERROR)  
  {
    return 0;
  }
  
  char execID[32];
  sprintf(execID, "%lu", executedOrder.executionId);
  
  char ecnOrderID[32] = "''";
  int _serverId = (type == MANUAL_ORDER) ? MAX_TRADE_SERVER_CONNECTIONS : type;
  if (ExchangeOrderIdMapping[_serverId][executedOrder.orderID % MAX_ORDER_PLACEMENT] != 0)
  {
    sprintf(ecnOrderID, "%lu", ExchangeOrderIdMapping[_serverId][executedOrder.orderID % MAX_ORDER_PLACEMENT]);
  }
  else
  {
    TraceLog(ERROR_LEVEL, "%s: Could not get ExOrderId, orderID: %d\n", __func__, executedOrder.orderID);
  }

  char account[32];
  GetAccountName(oecType, dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID], account);

  int _venueType = oecType - TYPE_BASE;
  char buysell = '0';
  if (indexOpenOrder != ERROR)
  {
    switch (asOpenOrder.orderCollection[indexOpenOrder].orderSummary.side)
    {
      case BUY_TO_CLOSE_TYPE:
      case BUY_TO_OPEN_TYPE:
        buysell = 'B';
        break;
      case SELL_TYPE:
        buysell = 'S';
        break;
      case SHORT_SELL_TYPE:
        buysell = 'T';
        break;
    }
    
    AddOJOrderFillRecord(timeStamp, asOpenOrder.orderCollection[indexOpenOrder].orderSummary.symbol, exOrderId, _venueType, buysell, executedOrder.price,
                         executedOrder.shares, executedOrder.orderID, liquidityIndicator,
                         ecnOrderID, execID, "1", (executedOrder.leftShares == 0)?1:0, account, 0);
  }
  else
  {
    AddOJOrderFillRecord(timeStamp, "", exOrderId, _venueType, buysell, executedOrder.price,
                         executedOrder.shares, executedOrder.orderID, liquidityIndicator,
                    ecnOrderID, execID, "1", (executedOrder.leftShares == 0)?1:0, account, 0);
  }
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessCanceledOrderOfOUCHOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessCanceledOrderOfOUCHOrder( t_DataBlock dataBlock, int type, char *timeStamp, const int oecType )
{
  t_OUCHCanceledOrder canceledOrder;
  t_OrderDetail orderDetail;

  /*------------------------------------------------
  Order detail:
  - int length;
  - int execId;
  - int isBroken;
  - char timestamp[MAX_TIMESTAMP];
  - char msgContent[MAX_ORDER_MSG];
  ------------------------------------------------*/
  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;

  // YYYY/mm/DD
  char dateTime[32];
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]), &dateTime[0]);
  memcpy(canceledOrder.processedTimestamp, &dateTime[11], TIME_LENGTH);
  canceledOrder.processedTimestamp[TIME_LENGTH] = 0;
    
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]), &dateTime[0]);
  memcpy(canceledOrder.date, &dateTime[0], 10);
  canceledOrder.date[10] = 0;
  
  // HH:MM:SS.ms
  memcpy(canceledOrder.timestamp, &dateTime[11], TIME_LENGTH);
  canceledOrder.timestamp[TIME_LENGTH] = 0;

  // orderDetail.timestamp
  memcpy(orderDetail.timestamp, &dateTime[0], TIMESTAMP_LENGTH);
  orderDetail.timestamp[TIMESTAMP_LENGTH] = 0;
  
  // Messsage type
  strcpy( canceledOrder.msgType, "CanceledOrder" );

  //MsgType
  char msgType = dataBlock.msgContent[1];
  
  // Sending time
  // Get to total of nano seconds
  long totalnanos = GetLongNumberBigEndian((char*)&dataBlock.msgContent[2]);
  
  // Get the number of seconds
  int numSecs = totalnanos / 1000000000;
  
  // Get the number of nanoseconds
  int numNanos = totalnanos % 1000000000;
  
  // write to transtime
  sprintf(canceledOrder.transTime, "%02d:%02d:%02d.%09d", numSecs / 3600, (numSecs % 3600) / 60, (numSecs % 3600) % 60, numNanos);

  // OrderId
  char fieldToken[15];
  memcpy( fieldToken, &dataBlock.msgContent[10], 14);
  fieldToken[14] = 0;
  canceledOrder.orderID = atoi( &fieldToken[2] );

  // Shares
  canceledOrder.shares = GetIntNumberBigEndian(&dataBlock.msgContent[24]);

  // Reason
  canceledOrder.reason = dataBlock.msgContent[28];
  
  canceledOrder.seqNum = 0;

  // orderDetail.msgContent
  orderDetail.length = sprintf(orderDetail.msgContent, 
                  "%d=%c\001"   // fieldIndexList.MessageType
                  "%d=%s\001"   // fieldIndexList.TimeStamp
                  "%d=%s\001"   // fieldIndexList.Token
                  "%d=%d\001"   // fieldIndexList.Shares
                  "%d=%c\001",  // fieldIndexList.Reason
                  fieldIndexList.MessageType, msgType, 
                  fieldIndexList.TimeStamp, canceledOrder.transTime, 
                  fieldIndexList.Token, fieldToken, 
                  fieldIndexList.Shares, canceledOrder.shares, 
                  fieldIndexList.Reason, canceledOrder.reason);
  
  strcpy( canceledOrder.msgContent, orderDetail.msgContent );
  
  // Update database
  int tsId;

  if( type == MANUAL_ORDER )
  {
    // tsId
    tsId = MANUAL_ORDER;
  }
  else
  {
    // tsId
    tsId = tradeServersInfo.config[type].tsId;
  }

  // Call function to update a open order (non fill)
  int indexOpenOrder = ProcessUpdateASOpenOrderForStatus(tsId, canceledOrder.orderID, orderStatusIndex.CanceledByECN, &orderDetail );
  if (indexOpenOrder == -2)
  {
    AddDataBlockToWaitList(oecType, WAIT_LIST_TYPE_OUCH_ORDER_CANCELED, canceledOrder.orderID, dataBlock, type, timeStamp);
    return 0;
  }
  
  if (indexOpenOrder != ERROR) 
  {
    strcpy(canceledOrder.status, GetOrderStatus(asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status));
  }
  else
  {
    strcpy(canceledOrder.status, orderStatus.CanceledByECN);
  }
  
  canceledOrder.entryExitRefNum = *(unsigned short *)&dataBlock.addContent[DATABLOCK_OFFSET_ENTRY_EXIT_REF];
  
  // Call function to update Canceled Order of OUCH
  ProcessInsertCanceledOrder4OUCHQueryToCollection(&canceledOrder);
  
  //Duplicated
  if (AddOrderIdToList(&__cancel, canceledOrder.orderID) == ERROR)
  {
    return 0;
  }
  
  char reason[1024];
  switch (canceledOrder.reason)
  {
    /*
    U User requested cancel. Sent in response to a Cancel Order Message or a Replace Order Message
    I Immediate or Cancel order. This order was originally sent with a timeout of zero and no further matches were available on the book so the remaining unexecuted shares were immediately canceled
    T Timeout. The Time In Force for this order has expired
    S Supervisory. This order was manually canceled or reduced by a NASDAQ supervisory terminal. This is usually in response to a participant request via telephone.
    D This order cannot be executed because of a regulatory restriction (e.g.: trade through restrictions).
    Q Self Match Prevention. The order was cancelled because it would have executed with an existing order entered by the same MPID.
    */
    case 'U':
      strcpy(reason, "User%20requested%20cancel.%20Sent%20in%20response%20to%20a%20Cancel%20Order%20Message%20or%20a%20Replace%20Order%20Message");
      break;
    case 'I':
      strcpy(reason, "Immediate%20or%20Cancel%20order.%20This%20order%20was%20originally%20sent%20with%20a%20timeout%20of%20zero%20and%20no%20further%20matches%20were%20available%20on%20the%20book%20so%20the%20remaining%20unexecuted%20shares%20were%20immediately%20canceled");
      break;
    case 'T':
      strcpy(reason, "Timeout.%20The%20Time%20In%20Force%20for%20this%20order%20has%20expired");
      break;
    case 'S':
      strcpy(reason, "Supervisory.%20This%20order%20was%20manually%20canceled%20or%20reduced%20by%20a%20NASDAQ%20supervisory%20terminal.%20This%20is%20usually%20in%20response%20to%20a%20participant%20request%20via%20telephone");
      break;
    case 'D':
      strcpy(reason, "This%20order%20cannot%20be%20executed%20because%20of%20a%20regulatory%20restriction%20(e.g.:%20trade%20through%20restrictions)");
      break;
    case 'Q':
      strcpy(reason, "Self%20Match%20Prevention.%20The%20order%20was%20cancelled%20because%20it%20would%20have%20executed%20with%20an%20existing%20order%20entered%20by%20the%20same%20MPID");
      break;
    case 'Z':
      strcpy(reason, "System%20cancel.%20This%20order%20was%20cancelled%20by%20the%20system");
      break;
    default:
      sprintf(reason, "Unknown%%20reason,%%20reason%%20code%%20%%3d%%20%c", canceledOrder.reason);
      break;
  }
  
  char ecnOrderID[32] = "''";
  int _serverId = (type == MANUAL_ORDER) ? MAX_TRADE_SERVER_CONNECTIONS : type;
  if (ExchangeOrderIdMapping[_serverId][canceledOrder.orderID % MAX_ORDER_PLACEMENT] != 0)
  {
    sprintf(ecnOrderID, "%lu", ExchangeOrderIdMapping[_serverId][canceledOrder.orderID % MAX_ORDER_PLACEMENT]);
  }
  else
  {
    TraceLog(ERROR_LEVEL, "%s: Could not get ExOrderId, orderID: %d\n", __func__, canceledOrder.orderID);
  }
  
  char account[32];
  GetAccountName(oecType, dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID], account);

  int _venueType = oecType - TYPE_BASE;
  if (indexOpenOrder != ERROR)
  {
    AddOJOrderCancelResponseRecord(timeStamp, asOpenOrder.orderCollection[indexOpenOrder].orderSummary.symbol, canceledOrder.orderID, ecnOrderID, reason, _venueType, canceledOrder.shares, 1, account);
  }
  else
  {
    AddOJOrderCancelResponseRecord(timeStamp, "", canceledOrder.orderID, ecnOrderID, reason, _venueType, canceledOrder.shares, 1, account);
  }
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessCancelRequestOfOUCHOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessCancelRequestOfOUCHOrder( t_DataBlock dataBlock, int type, char *timeStamp, int oecType)
{
  t_OUCHCancelRequest cancelRequest;
  t_OrderDetail orderDetail;

  /*------------------------------------------------
  Order detail:
  - int length;
  - int execId;
  - int isBroken;
  - char timestamp[MAX_TIMESTAMP];
  - char msgContent[MAX_ORDER_MSG];
  ------------------------------------------------*/
  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;

  // YYYY/mm/DD
  char dateTime[32];
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]), &dateTime[0]);
  memcpy(cancelRequest.date, &dateTime[0], 10);
  cancelRequest.date[10] = 0;
  
  // HH:MM:SS.ms
  memcpy( cancelRequest.timestamp, &dateTime[11], TIME_LENGTH);
  cancelRequest.timestamp[TIME_LENGTH] = 0;

  // orderDetail.timestamp
  memcpy(orderDetail.timestamp, &dateTime[0], TIMESTAMP_LENGTH);
  orderDetail.timestamp[TIMESTAMP_LENGTH] = 0;
  
  // Messsage type
  strcpy( cancelRequest.msgType, "CancelRequest" );

  // MsgType
  char msgType = dataBlock.msgContent[1];

  // OrderId
  char fieldToken[15];
  memcpy( fieldToken, &dataBlock.msgContent[10], 14);
  fieldToken[14] = 0;
  cancelRequest.orderID = atoi( &fieldToken[2] );
  
  // Shares
  cancelRequest.shares = GetIntNumberBigEndian(&dataBlock.msgContent[16]);

  cancelRequest.seqNum = 0;

  // orderDetail.msgContent
  orderDetail.length = sprintf(orderDetail.msgContent, "%d=%c\001%d=%s\001%d=%d\001", 
                  fieldIndexList.MessageType, msgType, 
                  fieldIndexList.Token, fieldToken, 
                  fieldIndexList.Shares, cancelRequest.shares); 
  
  strcpy( cancelRequest.msgContent, orderDetail.msgContent );
  
  // Update database
  int tsId;

  if( type == MANUAL_ORDER )
  {
    // tsId
    tsId = MANUAL_ORDER;
  }
  else
  {
    // tsId
    tsId = tradeServersInfo.config[type].tsId;
  }

  // Call function to update a open order (non fill)
  int indexOpenOrder = ProcessUpdateASOpenOrderForStatus(tsId, cancelRequest.orderID, orderStatusIndex.CXLSent, &orderDetail );
  
  if (indexOpenOrder == -2)
  {
    AddDataBlockToWaitList(oecType, WAIT_LIST_TYPE_OUCH_CANCEL_REQUEST, cancelRequest.orderID, dataBlock, type, timeStamp);
    return 0;
  }
  
  if (indexOpenOrder != ERROR) 
  {
    strcpy(cancelRequest.status, GetOrderStatus(asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status));
  }
  else
  {
    strcpy(cancelRequest.status, orderStatus.CXLSent);
  }
  
  // Call function to update cancel request of OUCH
  ProcessInsertCancelRequest4OUCHQueryToCollection(&cancelRequest);

  int _venueType = oecType - TYPE_BASE;
  if (indexOpenOrder != ERROR) 
  {
    AddOJOrderCancelRecord(timeStamp, cancelRequest.orderID, asOpenOrder.orderCollection[indexOpenOrder].orderSummary.symbol, _venueType, cancelRequest.shares);
  }
  else
  {
    AddOJOrderCancelRecord(timeStamp, cancelRequest.orderID, "\0", _venueType, cancelRequest.shares);
  }
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessRejectedOrderOfOUCHOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessRejectedOrderOfOUCHOrder( t_DataBlock dataBlock, int type, char *timeStamp, const int oecType )
{
  t_OUCHRejectedOrder rejectedOrder;
  t_OrderDetail orderDetail;

  /*------------------------------------------------
  Order detail:
  - int length;
  - int execId;
  - int isBroken;
  - char timestamp[MAX_TIMESTAMP];
  - char msgContent[MAX_ORDER_MSG];
  ------------------------------------------------*/
  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;

  // YYYY/mm/DD
  char dateTime[32];
    GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]), &dateTime[0]);
    memcpy( rejectedOrder.processedTimestamp, &dateTime[11], TIME_LENGTH);
  rejectedOrder.processedTimestamp[TIME_LENGTH] = 0;
    
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]), &dateTime[0]);
  memcpy(rejectedOrder.date, &dateTime[0], 10);
  rejectedOrder.date[10] = 0;
  
  // HH:MM:SS.ms
  memcpy( rejectedOrder.timestamp, &dateTime[11], TIME_LENGTH);
  rejectedOrder.timestamp[TIME_LENGTH] = 0;

  // orderDetail.timestamp
  memcpy(orderDetail.timestamp, &dateTime[0], TIMESTAMP_LENGTH);
  orderDetail.timestamp[TIMESTAMP_LENGTH] = 0;

  // Messsage type
  strcpy( rejectedOrder.msgType, "RejectedOrder" );

  // MsgType
  char msgType = dataBlock.msgContent[1];
  
  // Sending time
  // Get to total of nano seconds
  long totalnanos = GetLongNumberBigEndian((char*)&dataBlock.msgContent[2]);
  
  // Get the number of seconds
  int numSecs = totalnanos / 1000000000;
  
  // Get the number of nanoseconds
  int numNanos = totalnanos % 1000000000;
  
  // write to transtime
  sprintf(rejectedOrder.transTime, "%02d:%02d:%02d.%09d", numSecs / 3600, (numSecs % 3600) / 60, (numSecs % 3600) % 60, numNanos);

  // OrderId
  char fieldToken[15];
  memcpy( fieldToken, &dataBlock.msgContent[10], 14 );
  fieldToken[14] = 0;
  rejectedOrder.orderID = atoi( &fieldToken[2] );

  // Reason
  rejectedOrder.reason = dataBlock.msgContent[24];

  // Raise order rejected alert
  char reasonCode[2];
  char reason[1024];
  
  reasonCode[0] = rejectedOrder.reason;
  reasonCode[1] = 0;
  
  switch (rejectedOrder.reason)
  {
    case 'T':
      strcpy(reason, "Test Mode  This OUCH Account is configured for test mode and is not able to accept orders in non-TEST securities");
      break;
    case 'H':
      strcpy(reason, "Halted  There is currently a trading halt so no orders can be accepted in this stock at this time");
      break;
    case 'Z':
      strcpy(reason, "Shares exceeds configured safety threshold  The number of shares entered must be less than the safety threshold configured for this Account. The safety threshold can be added/updated through NASDAQ Subscriber Services");
      break;
    case 'S':
      strcpy(reason, "Invalid stock  The stock field must be a valid issue / tradable on NASDAQ");
      break;
    case 'D':
      strcpy(reason, "Invalid Display Type  Sent when Display Type Entered cannot be accepted in current circumstances and cant be simply converted to a valid Display Type");
      break;
    case 'C':
      strcpy(reason, "NASDAQ is closed");
      break;
    case 'L':
      strcpy(reason, "Requested firm not authorized for requested clearing type on this account  To authorize additional firms / use the NASDAQ Service Bureau Agreement");
      break;
    case 'M':
      strcpy(reason, "Outside of permitted times for requested clearing type");
      break;
    case 'R':
      strcpy(reason, "This order is not allowed in this type of cross (stock or time restrictions)");
      break;
    case 'X':
      strcpy(reason, "Invalid price");
      break;
    case 'N':
      strcpy(reason, "Invalid Minimum Quantity");
      break;
    default:
      strcpy(reason, "This is new/unknown reason code");
      break;
  }
  
  CheckNSendOrderRejectedAlertToAllTT(oecType, reasonCode, reason, rejectedOrder.orderID);

  rejectedOrder.seqNum = 0;
    
  // orderDetail.msgContent
  orderDetail.length = sprintf( orderDetail.msgContent, "%d=%c\001%d=%s\001%d=%s\001%d=%c\001", 
                  fieldIndexList.MessageType, msgType, 
                  fieldIndexList.TimeStamp, rejectedOrder.transTime, 
                  fieldIndexList.Token, fieldToken, 
                  fieldIndexList.Reason, rejectedOrder.reason );
  
  strcpy( rejectedOrder.msgContent, orderDetail.msgContent );
  
  // Update database
  int tsId;

  if( type == MANUAL_ORDER )
  {
    // tsId
    tsId = MANUAL_ORDER;
  }
  else
  {
    // tsId
    tsId = tradeServersInfo.config[type].tsId;
  }

  // Call function to update a open order (non fill)
  int indexOpenOrder = ProcessUpdateASOpenOrderForStatus(tsId, rejectedOrder.orderID, orderStatusIndex.Rejected, &orderDetail );
  if (indexOpenOrder == -2)
  {
    AddDataBlockToWaitList(oecType, WAIT_LIST_TYPE_OUCH_ORDER_REJECTED, rejectedOrder.orderID, dataBlock, type, timeStamp);
    return 0;
  }
  
  if (indexOpenOrder != ERROR) 
  {
    strcpy(rejectedOrder.status, GetOrderStatus(asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status));
  }
  else
  {
    strcpy(rejectedOrder.status, orderStatus.CancelRejected);
  }
  
  rejectedOrder.entryExitRefNum = *(unsigned short *)&dataBlock.addContent[DATABLOCK_OFFSET_ENTRY_EXIT_REF];
  
  // Call function to update Rejected Order of OUCH
  ProcessInsertRejectedOrder4OUCHQueryToCollection(&rejectedOrder);

  //Add a new record to OJ file
  //int AddOJOrderRejectRecord(char *time, int orderID, char *ecnOrderID, char *reason, char *symbol, int venueID, char *reasonCode, char *account)

  //Duplicated
  if (AddOrderIdToList(&__reject, rejectedOrder.orderID) == ERROR)
  {
    return 0;
  }
  
  char ecnOrderID[2];
  strcpy(ecnOrderID, "''"); //There is no Order Reference Number in this message type
    
  /*
    T Test Mode  This OUCH Account is configured for test mode and is not able to accept orders in non-TEST securities.
    H Halted  There is currently a trading halt so no orders can be accepted in this stock at this time.
    Z Shares exceeds configured safety threshold  The number of shares entered must be less than the safety threshold configured for this Account. The safety threshold can be added/updated through NASDAQ Subscriber Services.
    S Invalid stock  The stock field must be a valid issue, tradable on NASDAQ.
    D Invalid Display Type  Sent when Display Type Entered cannot be accepted in current circumstances and cant be simply converted to a valid Display Type. 
    C NASDAQ is closed.
    L Requested firm not authorized for requested clearing type on this account  To authorize additional firms, use the NASDAQ Service Bureau Agreement.
    M Outside of permitted times for requested clearing type
    R This order is not allowed in this type of cross (stock or time restrictions).
    X Invalid price
    N Invalid Minimum Quantity
  */
  
  switch (rejectedOrder.reason)
  {
    case 'T':
      strcpy(reason, "Test%20Mode%20%20This%20OUCH%20Account%20is%20configured%20for%20test%20mode%20and%20is%20not%20able%20to%20accept%20orders%20in%20non-TEST%20securities");
      break;
    case 'H':
      strcpy(reason, "Halted%20%20There%20is%20currently%20a%20trading%20halt%20so%20no%20orders%20can%20be%20accepted%20in%20this%20stock%20at%20this%20time");
      break;
    case 'Z':
      strcpy(reason, "Shares%20exceeds%20configured%20safety%20threshold%20%20The%20number%20of%20shares%20entered%20must%20be%20less%20than%20the%20safety%20threshold%20configured%20for%20this%20Account.%20The%20safety%20threshold%20can%20be%20added/updated%20through%20NASDAQ%20Subscriber%20Services");
      break;
    case 'S':
      strcpy(reason, "Invalid%20stock%20%20The%20stock%20field%20must%20be%20a%20valid%20issue,%20tradable%20on%20NASDAQ");
      break;
    case 'D':
      strcpy(reason, "Invalid%20Display%20Type%20%20Sent%20when%20Display%20Type%20Entered%20cannot%20be%20accepted%20in%20current%20circumstances%20and%20cant%20be%20simply%20converted%20to%20a%20valid%20Display%20Type");
      break;
    case 'C':
      strcpy(reason, "NASDAQ%20is%20closed");
      break;
    case 'L':
      strcpy(reason, "Requested%20firm%20not%20authorized%20for%20requested%20clearing%20type%20on%20this%20account%20%20To%20authorize%20additional%20firms,%20use%20the%20NASDAQ%20Service%20Bureau%20Agreement");
      break;
    case 'M':
      strcpy(reason, "Outside%20of%20permitted%20times%20for%20requested%20clearing%20type");
      break;
    case 'R':
      strcpy(reason, "This%20order%20is%20not%20allowed%20in%20this%20type%20of%20cross%20(stock%20or%20time%20restrictions)");
      break;
    case 'X':
      strcpy(reason, "Invalid%20price");
      break;
    case 'N':
      strcpy(reason, "Invalid%20Minimum%20Quantity");
      break;
    default:
      strcpy(reason, "Unknown%20reason");
      break;
  }
  
  char account[32];
  GetAccountName(oecType, dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID], account);

  int _venueType = oecType - TYPE_BASE;
  if (indexOpenOrder != ERROR)
  {
    AddOJOrderRejectRecord(timeStamp, rejectedOrder.orderID, ecnOrderID, reason, asOpenOrder.orderCollection[indexOpenOrder].orderSummary.symbol, _venueType, reasonCode, account);
  }
  else
  {
    AddOJOrderRejectRecord(timeStamp, rejectedOrder.orderID, ecnOrderID, reason, "", _venueType, reasonCode, account);
  }
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessBrokenOrderOfOUCHOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessBrokenOrderOfOUCHOrder( t_DataBlock dataBlock, int type, const int oecType )
{
  t_OUCHBrokenOrder brokenTrade;
  t_OrderDetail orderDetail;

  /*------------------------------------------------
  Order detail:
  - int length;
  - int execId;
  - int isBroken;
  - char timestamp[MAX_TIMESTAMP];
  - char msgContent[MAX_ORDER_MSG];
  ------------------------------------------------*/
  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;

  // YYYY/mm/DD
  char dateTime[32];
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]), &dateTime[0]);
  memcpy( brokenTrade.date, &dateTime[0], 10);
  brokenTrade.date[10] = 0;
  
  // HH:MM:SS.ms
  memcpy( brokenTrade.timestamp, &dateTime[11], TIME_LENGTH);
  brokenTrade.timestamp[TIME_LENGTH] = 0;

  // orderDetail.timestamp
  memcpy( orderDetail.timestamp, &dateTime[0], TIMESTAMP_LENGTH);
  orderDetail.timestamp[TIMESTAMP_LENGTH] = 0;

  // Messsage type
  strcpy( brokenTrade.msgType, "BrokenOrder" );

  // MsgType
  char msgType = dataBlock.msgContent[1];
  
  // Sending time
  // Get to total of nano seconds
  long totalnanos = GetLongNumberBigEndian((char*)&dataBlock.msgContent[2]);
  
  // Get the number of seconds
  int numSecs = totalnanos / 1000000000;
  
  // Get the number of nanoseconds
  int numNanos = totalnanos % 1000000000;
  
  // write to transtime
  sprintf(brokenTrade.transTime, "%02d:%02d:%02d.%09d", numSecs / 3600, (numSecs % 3600) / 60, (numSecs % 3600) % 60, numNanos);
  
  // Status: Sent, Live, Rejected, Closed, BrokenTrade, CanceledByECN, 
  // CXLSent, CancelACK, CancelRejected, CancelPending, CanceledByUser
  strcpy( brokenTrade.status, orderStatus.BrokenTrade );
  
  // Order Id
  char fieldToken[15];
  memcpy( fieldToken, &dataBlock.msgContent[10], 14);
  fieldToken[14] = 0;
  brokenTrade.orderID = atoi( &fieldToken[2] );

  // MatchNo
  brokenTrade.executionId = GetLongNumberBigEndian((char*)&dataBlock.msgContent[24]);

  // Reason
  brokenTrade.reason = dataBlock.msgContent[32];

  // orderDetail.msgContent
  orderDetail.length = sprintf( orderDetail.msgContent, "%d=%c\001%d=%s\001%d=%s\001%d=%ld\001%d=%c\001", 
                  fieldIndexList.MessageType, msgType, 
                  fieldIndexList.TimeStamp, brokenTrade.transTime, 
                  fieldIndexList.Token, fieldToken, 
                  fieldIndexList.MatchNo, brokenTrade.executionId, 
                  fieldIndexList.Reason, brokenTrade.reason );
  
  strcpy( brokenTrade.msgContent, orderDetail.msgContent );
  
  // Update database
  int tsId;

  if( type == MANUAL_ORDER )
  {
    // tsId
    tsId = MANUAL_ORDER;
  }
  else
  {
    // tsId
    tsId = tradeServersInfo.config[type].tsId;
  }
  
  // Call function to update a open order (non fill)
  if (ProcessUpdateASOpenOrderForStatus(tsId, brokenTrade.orderID, orderStatusIndex.BrokenTrade, &orderDetail) == -2)
  {
    AddDataBlockToWaitList(oecType, WAIT_LIST_TYPE_OUCH_ORDER_BROKEN, brokenTrade.orderID, dataBlock, type, "");
    return 0;
  }
  
  // Call function to update Broken Trade of OUCH
  ProcessInsertBrokenTrade4OUCHQueryToCollection(&brokenTrade);
  
  //Process add broken trade message to as logs
  int indexOpenOrder = CheckOrderIdIndex(brokenTrade.orderID, tsId);
  if (indexOpenOrder >= 0)
  {
    // Add the broken trade message to AS error log
    TraceLog(DEBUG_LEVEL, "Broken Trade message received from %s for symbol %.8s.\n", GetOECName(oecType), asOpenOrder.orderCollection[indexOpenOrder].orderSummary.symbol);
  }

  return 0;
}

/****************************************************************************
- Function name:  ProcessPriceCorrectionOfOUCHOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessPriceCorrectionOfOUCHOrder( t_DataBlock dataBlock, int type, const int oecType )
{
  t_OUCHBrokenOrder brokenTrade;
  t_OrderDetail orderDetail;

  /*------------------------------------------------
  Order detail:
  - int length;
  - int execId;
  - int isBroken;
  - char timestamp[MAX_TIMESTAMP];
  - char msgContent[MAX_ORDER_MSG];
  ------------------------------------------------*/
  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;

  // YYYY/mm/DD
  char dateTime[32];
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]), &dateTime[0]);
  memcpy( brokenTrade.date, &dateTime[0], 10);
  brokenTrade.date[10] = 0;
  
  // HH:MM:SS.ms
  memcpy( brokenTrade.timestamp, &dateTime[11], TIME_LENGTH);
  brokenTrade.timestamp[TIME_LENGTH] = 0;

  // orderDetail.timestamp
  memcpy( orderDetail.timestamp, &dateTime[0], TIMESTAMP_LENGTH);
  orderDetail.timestamp[TIMESTAMP_LENGTH] = 0;

  // Messsage type
  strcpy( brokenTrade.msgType, "BrokenOrder" );

  // MsgType
  char msgType = dataBlock.msgContent[1];
  
  // Sending time
  // Get to total of nano seconds
  long totalnanos = GetLongNumberBigEndian((char*)&dataBlock.msgContent[2]);
  
  // Get the number of seconds
  int numSecs = totalnanos / 1000000000;
  
  // Get the number of nanoseconds
  int numNanos = totalnanos % 1000000000;
  
  // write to transtime
  sprintf(brokenTrade.transTime, "%02d:%02d:%02d.%09d", numSecs / 3600, (numSecs % 3600) / 60, (numSecs % 3600) % 60, numNanos);
  
  // Status: Sent, Live, Rejected, Closed, BrokenTrade, CanceledByECN, 
  // CXLSent, CancelACK, CancelRejected, CancelPending, CanceledByUser
  strcpy( brokenTrade.status, orderStatus.BrokenTrade );  

  // Order Id
  char fieldToken[15];
  memcpy( fieldToken, &dataBlock.msgContent[10], 14);
  fieldToken[14] = 0;
  brokenTrade.orderID = atoi( &fieldToken[2] );

  // MatchNo
  brokenTrade.executionId = GetLongNumberBigEndian((char*)&dataBlock.msgContent[24]);
  
  // New Execution Price
  double newPrice = (double)((GetIntNumberBigEndian(&dataBlock.msgContent[32])) / 10000);

  // Reason
  brokenTrade.reason = dataBlock.msgContent[36];

  // orderDetail.msgContent
  orderDetail.length = sprintf( orderDetail.msgContent, "%d=%c\001%d=%s\001%d=%s\001%d=%ld\001%d=%lf\001%d=%c\001", 
                  fieldIndexList.MessageType, msgType, 
                  fieldIndexList.TimeStamp, brokenTrade.transTime, 
                  fieldIndexList.Token, fieldToken, 
                  fieldIndexList.MatchNo, brokenTrade.executionId, 
                  fieldIndexList.Price, newPrice, 
                  fieldIndexList.Reason, brokenTrade.reason );
  
  strcpy( brokenTrade.msgContent, orderDetail.msgContent );
  
  // Update database
  int tsId;

  if( type == MANUAL_ORDER )
  {
    // tsId
    tsId = MANUAL_ORDER;
  }
  else
  {
    // tsId
    tsId = tradeServersInfo.config[type].tsId;
  }
  
  // Call function to update a open order (non fill)
  if (ProcessUpdateASOpenOrderForStatus(tsId, brokenTrade.orderID, orderStatusIndex.BrokenTrade, &orderDetail) == -2)
  {
    AddDataBlockToWaitList(oecType, WAIT_LIST_TYPE_OUCH_PRICE_CORRECTION, brokenTrade.orderID, dataBlock, type, "");
    return 0;
  }
  
  // Call function to update Broken Trade of OUCH
  ProcessInsertBrokenTrade4OUCHQueryToCollection(&brokenTrade);

  return 0;
}

/****************************************************************************
- Function name:  ProcessCancelPendingOfOUCHOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessCancelPendingOfOUCHOrder( t_DataBlock dataBlock, int type, int oecType)
{
  t_OUCHCancelPending cancelPending;
  t_OrderDetail orderDetail;

  /*------------------------------------------------
  Order detail:
  - int length;
  - int execId;
  - int isBroken;
  - char timestamp[MAX_TIMESTAMP];
  - char msgContent[MAX_ORDER_MSG];
  ------------------------------------------------*/
  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;

  char dateTime[32];
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]), &dateTime[0]);
  memcpy(cancelPending.date, &dateTime[0], 10);
  cancelPending.date[10] = 0;
  
  // HH:MM:SS.ms
  memcpy( cancelPending.timestamp, &dateTime[11], TIME_LENGTH);
  cancelPending.timestamp[TIME_LENGTH] = 0;

  // orderDetail.timestamp
  memcpy(orderDetail.timestamp, &dateTime[0], TIMESTAMP_LENGTH);
  orderDetail.timestamp[TIMESTAMP_LENGTH] = 0;

  // Messsage type
  strcpy( cancelPending.msgType, "CancelPending" );
  
  // MsgType
  char msgType = dataBlock.msgContent[1];
  
  // Sending time
  // Get to total of nano seconds
  long totalnanos = GetLongNumberBigEndian((char*)&dataBlock.msgContent[2]);
  
  // Get the number of seconds
  int numSecs = totalnanos / 1000000000;
  
  // Get the number of nanoseconds
  int numNanos = totalnanos % 1000000000;
  
  // write to transtime
  sprintf(cancelPending.transTime, "%02d:%02d:%02d.%09d", numSecs / 3600, (numSecs % 3600) / 60, (numSecs % 3600) % 60, numNanos);

  // OrderId
  char fieldToken[15];
  memcpy( fieldToken, &dataBlock.msgContent[10], 14 );
  fieldToken[14] = 0;
  cancelPending.orderID = atoi( &fieldToken[2] );
  
  cancelPending.seqNum = 0;
    
  // orderDetail.msgContent
  orderDetail.length = sprintf( orderDetail.msgContent, "%d=%c\001%d=%s\001%d=%s\001", 
                  fieldIndexList.MessageType, msgType, 
                  fieldIndexList.TimeStamp, cancelPending.transTime, 
                  fieldIndexList.Token, fieldToken);
  
  strcpy( cancelPending.msgContent, orderDetail.msgContent );
  
  // Update database
  int tsId;

  if( type == MANUAL_ORDER )
  {
    // tsId
    tsId = MANUAL_ORDER;
  }
  else
  {
    // tsId
    tsId = tradeServersInfo.config[type].tsId;
  }

  // Call function to update a open order (non fill)
  int indexOpenOrder = ProcessUpdateASOpenOrderForStatus(tsId, cancelPending.orderID, orderStatusIndex.CancelPending, &orderDetail );
  if (indexOpenOrder == -2)
  {
    AddDataBlockToWaitList(oecType, WAIT_LIST_TYPE_OUCH_CANCEL_PENDING, cancelPending.orderID, dataBlock, type, "");
    return 0;
  }
  
  if (indexOpenOrder != ERROR) 
  {
    strcpy(cancelPending.status, GetOrderStatus(asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status));
  }
  else
  {
    strcpy(cancelPending.status, orderStatus.CancelPending);
  }
  
  // Call function to update Cancel Pending of OUCH
  ProcessInsertCancelPending4OUCHQueryToCollection(&cancelPending);

  return 0;
}

/****************************************************************************
- Function name:  ProcessCancelRejectOfOUCHOrder
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessCancelRejectOfOUCHOrder( t_DataBlock dataBlock, int type, int oecType)
{
  t_OUCHCancelReject cancelReject;
  t_OrderDetail orderDetail;

  /*------------------------------------------------
  Order detail:
  - int length;
  - int execId;
  - int isBroken;
  - char timestamp[MAX_TIMESTAMP];
  - char msgContent[MAX_ORDER_MSG];
  ------------------------------------------------*/
  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;

  char dateTime[32];
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]), &dateTime[0]);
  memcpy(cancelReject.date, &dateTime[0], 10);
  cancelReject.date[10] = 0;
  
  // HH:MM:SS.ms
  memcpy( cancelReject.timestamp, &dateTime[11], TIME_LENGTH);
  cancelReject.timestamp[TIME_LENGTH] = 0;

  // orderDetail.timestamp
  memcpy(orderDetail.timestamp, &dateTime[0], TIMESTAMP_LENGTH);
  orderDetail.timestamp[TIMESTAMP_LENGTH] = 0;

  // Messsage type
  strcpy( cancelReject.msgType, "CancelReject" );
  
  // MsgType
  char msgType = dataBlock.msgContent[1];

  // Sending time
  // Get to total of nano seconds
  long totalnanos = GetLongNumberBigEndian((char*)&dataBlock.msgContent[2]);
  
  // Get the number of seconds
  int numSecs = totalnanos / 1000000000;
  
  // Get the number of nanoseconds
  int numNanos = totalnanos % 1000000000;
  
  // write to transtime
  sprintf(cancelReject.transTime, "%02d:%02d:%02d.%09d", numSecs / 3600, (numSecs % 3600) / 60, (numSecs % 3600) % 60, numNanos);

  // OrderId
  char fieldToken[15];
  memcpy( fieldToken, &dataBlock.msgContent[10], 14 );
  fieldToken[14] = 0;
  cancelReject.orderID = atoi( &fieldToken[2] );
  
  cancelReject.seqNum = 0;
    
  // orderDetail.msgContent
  orderDetail.length = sprintf( orderDetail.msgContent, "%d=%c\001%d=%s\001%d=%s\001", 
                  fieldIndexList.MessageType, msgType, 
                  fieldIndexList.TimeStamp, cancelReject.transTime, 
                  fieldIndexList.Token, fieldToken);
  
  strcpy( cancelReject.msgContent, orderDetail.msgContent );
  
  // Update database
  int tsId;

  if( type == MANUAL_ORDER )
  {
    // tsId
    tsId = MANUAL_ORDER;
  }
  else
  {
    // tsId
    tsId = tradeServersInfo.config[type].tsId;
  }

  // Call function to update a open order (non fill)
  int indexOpenOrder = ProcessUpdateASOpenOrderForStatus(tsId, cancelReject.orderID, orderStatusIndex.CancelRejected, &orderDetail );
  if (indexOpenOrder == -2)
  {
    AddDataBlockToWaitList(oecType, WAIT_LIST_TYPE_OUCH_CANCEL_REJECTED, cancelReject.orderID, dataBlock, type, "");
    return 0;
  }
  
  if (indexOpenOrder != ERROR) 
  {
    strcpy(cancelReject.status, GetOrderStatus(asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status));
  }
  else
  {
    strcpy(cancelReject.status, orderStatus.CancelRejected);
  }
  
  // Call function to update Cancel Reject of OUCH
  ProcessInsertCancelReject4OUCHQueryToCollection(&cancelReject);

  return 0;
}

/***************************************************************************/
