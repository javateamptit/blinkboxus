/*****************************************************************************
**  Project:    Equity Arbitrage Application
**  Filename:   arca_direct_data.c
**  Description:  This file contains function definitions that were declared
          in arca_direct_data.h
**  Author:     Luan Vo-Kinh
**  First created:  13-Sep-2007
**  Last updated: -----------
*****************************************************************************/

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include "configuration.h"
#include "order_mgmt_proc.h"
#include "arca_direct_data.h"
#include "nasdaq_ouch_data.h"
#include "nasdaq_rash_data.h"
#include "bats_boe_data.h"
#include "database_util.h"
#include "query_data_mgmt.h"
#include "raw_data_mgmt.h"
#include "daedalus_proc.h"
#include "trader_tools_proc.h"
#include "trade_servers_proc.h"
#include "hbitime.h"

#include <limits.h>

/****************************************************************************
** Global variables definition
****************************************************************************/

/****************************************************************************
** Function declarations
****************************************************************************/
/****************************************************************************
- Function name:  ParseRawDataForARCA_DIRECTOrder
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ParseRawDataForARCA_DIRECTOrder( t_DataBlock dataBlock, int type )
{
  //Get timestamp from epoch with format <second from epoch>.<microsecond> (eg. 124212553634.3235235)
  char timeStamp[32];
  long usec;
  int variant;

  switch ( dataBlock.msgContent[0] )
  {
    // New Order
    case 'D':
      usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]) / 1000;
      sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
      variant = dataBlock.msgContent[1];
      //TraceLog(DEBUG_LEVEL, "ARCA DIRECT Order: Process new order\n");
      if (variant == 1)
      {
        ProcessNewOrderOfARCA_DIRECTOrder( dataBlock, type, timeStamp );
      }
      else
      {
        ProcessNewOrderOfARCA_DIRECTOrderV3( dataBlock, type, timeStamp);
      }
      
      break;

    // Order Ack
    case 'a':
      usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]) / 1000;
      sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
      //TraceLog(DEBUG_LEVEL, "ARCA DIRECT Order: Process order ack\n");
      ProcessOrderACKOfARCA_DIRECTOrder( dataBlock, type, timeStamp );
      break;

    // Order Fill
    case '2':
      usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]) / 1000;
      sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
      //TraceLog(DEBUG_LEVEL, "ARCA DIRECT Order: Process order fill\n");
      ProcessOrderFillOfARCA_DIRECTOrder( dataBlock, type, timeStamp );
      break;

    // Order killed
    case '4':
      usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]) / 1000;
      sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
      //TraceLog(DEBUG_LEVEL, "ARCA DIRECT Order: Process order killed\n");
      ProcessOrderCancelOfARCA_DIRECTOrder( dataBlock, type, timeStamp );
      break;

    // Cancel Request
    case 'F':
      usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]) / 1000;
      sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
      //TraceLog(DEBUG_LEVEL, "ARCA DIRECT Order: Process cancel request\n");
      ProcessCancelRequestOfARCA_DIRECTOrder( dataBlock, type, timeStamp );
      break;

    // Cancel ACK
    case '6':
      usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]) / 1000;
      sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
      //TraceLog(DEBUG_LEVEL, "ARCA DIRECT Order: Process cancel ack\n");
      ProcessCancelACKOfARCA_DIRECTOrder( dataBlock, type );
      break;

    // Rejected
    case '8':
      usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]) / 1000;
      sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
      //TraceLog(DEBUG_LEVEL, "ARCA DIRECT Order: Process rejected\n");
      ProcessRejectOfARCA_DIRECTOrder( dataBlock, type, timeStamp );
      break;

    // Bust or Correct
    case 'C':
      usec = *((long *) &dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]) / 1000;
      sprintf(timeStamp, "%ld.%06ld", usec/1000000, usec % 1000000);
      //TraceLog(DEBUG_LEVEL, "ARCA DIRECT Order: Process bust or correct\n");
      ProcessBustOrCorrectOfARCA_DIRECTOrder( dataBlock, type );
      break;

    default:
      TraceLog(ERROR_LEVEL, "ARCA DIRECT Order: Invalid order message, msgType = %c ASCII = %d\n", dataBlock.msgContent[0], dataBlock.msgContent[0] );
      break;
  }

  return 0;
}

/****************************************************************************
- Function name:  ProcessNewOrderOfARCA_DIRECTOrder
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessNewOrderOfARCA_DIRECTOrder( t_DataBlock dataBlock, int type, char *timeStamp )
{
  /*
  - API version: ArcaDirect 4.1
  - Date updated: 21-Mar-2013
  - Modified by: Danh Hoang-Tang-Van
  - New Order Message � (Variant 1: formerly V2 API � small message), it includes the following fields:
    + Message Type (1 byte) = 'D'
    + Variant (1 byte) = 1
    + Length (2 bytes) = 76
    + Sequence Number (4 bytes) = Client-assigned sequence number
    + Client Order Id (4 bytes) = A client-assigned ID for this order
    + PCS Link ID (4 bytes) = null
    + Order Quantity (4 bytes) = The number of shares
    + Price (4 bytes) = The price as a long value
    + ExDestination (2 bytes) = 102
    + Price Scale (1 byte) = '2'
    + Symbol (8 bytes) = Stock symbol
    + Company Group ID (5 bytes) = HBIFA
    + Deliver To Comp ID (5 bytes) = null
    + SenderSubID (5 bytes) = null
    + Execution Instructions (1 byte) = '1'
    + Side (1 byte) = '1', '2', or '5'
    + Order Type (1 byte) = '2'
    + Time In Force (1 byte) = '0' or '3'
    + Rule80A (1 byte) = 'P'
    + Trading Session Id (4 bytes) = "123"
    + Account (10 bytes) = null
    + ISO (1 byte) = 'N'
    + Extended Execution Instructions (1 byte) = null
    + ExtendedPNP (1 byte) = null
    + NoSelfTrade (1 byte) = 'O'
    + Filler (2 byte) = null
    + Message Terminator (1 byte) = '\n'
    => Total byte = 76
  */
  
  t_ARCA_DIRECTNewOrder newOrder;
  t_OpenOrderInfo openOrder;
  t_OrderDetail orderDetail;

  /*------------------------------------------------
  Order summary:
  - double price;
  - double avgPrice;
  - char symbol[SYMBOL_LEN];
  - int side;
  - int shares;
  - int leftShares;
  - int ECNId;
  - int clOrdId;
  - int status;
  - int tif;
  - int tsId;
  - char timestamp[MAX_TIMESTAMP];
  ------------------------------------------------*/

  /*------------------------------------------------
  Order detail:
  - int length;
  - int execId;
  - int isBroken;
  - char timestamp[MAX_TIMESTAMP];
  - char msgContent[MAX_ORDER_MSG];
  ------------------------------------------------*/
  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;

  // YYYY/mm/DD
  char dateTime[32];
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]), &dateTime[0]);
  memcpy(newOrder.date, &dateTime[0], 10);
  newOrder.date[10] = 0;

  // HH:MM:SS.mss
  memcpy(newOrder.timestamp, &dateTime[11], TIME_LENGTH);
  newOrder.timestamp[TIME_LENGTH] = 0;

  // openOrder.timestamp
  memcpy( openOrder.timestamp, &dateTime[0], TIMESTAMP_LENGTH );
  openOrder.timestamp[TIMESTAMP_LENGTH] = 0;

  // orderDetail
  strcpy( orderDetail.timestamp, openOrder.timestamp );

  // "ARCA", "OUCH", "RASH" or "BATZ"
  strcpy( newOrder.ecn, "ARCA");

  // openOrder.ECNId
  openOrder.ECNId = TYPE_ARCA_DIRECT;

  // Status: Sent, Live, Rejected, Closed, BrokenTrade, CanceledByECN,
  //      CXLSent, CancelACK, CancelRejected, CancelPending, CanceledByUser
  strcpy( newOrder.status, orderStatus.Sent );

  // openOrder.status
  openOrder.status = orderStatusIndex.Sent;

  // Messsage type
  strcpy( newOrder.msgType, "NewOrder" );

  int currentIndex = 0;
  // msgType
  char msgType = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Variant (1 byte)
  int variant = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);
  
  // Length
  newOrder.length = GetShortNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Sequence number
  newOrder.seqNum = GetIntNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Order ID
  newOrder.orderID = GetIntNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // openOrder.clOrdId
  openOrder.clOrdId = newOrder.orderID;

  // PCS link ID
  newOrder.PCSLinkID = GetIntNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Shares
  newOrder.shares = GetIntNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);
    
  // openOrder.shares
  openOrder.shares = newOrder.shares;
  openOrder.leftShares = openOrder.shares;

  int  fieldPrice = GetIntNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);
  
  // ExDestination (2 bytes)
  int exDestination = GetShortNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Price and Price Scale
  char fieldPriceScale = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  newOrder.price = fieldPrice * 1.0 / pow( 10, fieldPriceScale - 48 );
  
  // openOrder.price
  openOrder.price = newOrder.price;
  openOrder.avgPrice = openOrder.price;

  // Stock symbol
  GetStringV1(newOrder.symbol, &dataBlock.msgContent[currentIndex], SYMBOL_LEN, &currentIndex);

  // openOrder.symbol
  memcpy( openOrder.symbol, newOrder.symbol, SYMBOL_LEN);

  // Company group Id
  GetStringV1(newOrder.compGroupID, &dataBlock.msgContent[currentIndex], 5, &currentIndex);
  newOrder.compGroupID[5] = 0;

  // Deliver to Comp Id
  GetStringV1(newOrder.deliverCompID, &dataBlock.msgContent[currentIndex], 5, &currentIndex);
  newOrder.deliverCompID[5] = 0;
  
  // SenderSubID (5 bytes)
  char senderSubID[6];
  GetStringV1(senderSubID, &dataBlock.msgContent[currentIndex], 5, &currentIndex);
  senderSubID[5] = 0;
  
  // Execution instruction
  newOrder.execIns = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // "B", "S" or "SS"
  char action = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);
  char buysell = '0';
  switch ( action )
  {
    case '1':
      buysell = 'B';
      strcpy( newOrder.side, "BC" );
      openOrder.side = BUY_TO_CLOSE_TYPE;
      break;

    case '2':
      buysell = 'S';
      strcpy( newOrder.side, "S" );
      openOrder.side = SELL_TYPE;
      break;

    case '5':
      buysell = 'T';
      strcpy( newOrder.side, "SS" );
      openOrder.side = SHORT_SELL_TYPE;
      break;

    default:
      strcpy( newOrder.side, " " );
      openOrder.side = -1;

      TraceLog(ERROR_LEVEL, "ARCA_DIRECT New Order V1: not support side = %c, %d\n", action, action );
      break;
  }

  // Order type
  newOrder.ordType = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Time in force
  char tif = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);
  char _tif[6];
  switch (tif)
  {
    case '0':
      strcpy( newOrder.timeInForce, "DAY" );
      openOrder.tif = DAY_TYPE;
      strcpy(_tif, "99999");
      break;

    case '3':
      strcpy( newOrder.timeInForce, "IOC" );
      strcpy(_tif, "0");
      openOrder.tif = IOC_TYPE;
      break;

    default:
      strcpy( newOrder.timeInForce, " " );
      openOrder.tif = -1;
      TraceLog(ERROR_LEVEL, "ARCA_DIRECTOrder V1: not support tif = %c, %d\n", tif, tif );
      break;
  }

  // Rule 80A
  newOrder.rule80A = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Trading session id
  GetStringV1(newOrder.tradingSessionId, &dataBlock.msgContent[currentIndex], 4, &currentIndex);
  newOrder.tradingSessionId[4] = 0;
  
  // Account
  GetStringV1(newOrder.account, &dataBlock.msgContent[currentIndex], 10, &currentIndex);
  newOrder.account[10] = 0;
  
  newOrder.tradingAccount = dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID];

  // Iso Flag
  newOrder.isoFlag = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  int isISO = 0;
  short bboID = -1;
  unsigned char bbReason;
  if (newOrder.isoFlag == 'Y')
  {
    isISO = 1;
    bboID = GetShortNumber(&dataBlock.addContent[DATABLOCK_OFFSET_BBO_ID]);
  }

  newOrder.bboId = bboID;

  // Extended Execution instruction
  newOrder.ExtExeInstruction = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // ExtendedPNP (1 byte)
  newOrder.ExtendedPNP = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // NoSelfTrade (1 byte)
  char noSelfTrade = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);;

  newOrder.ProactiveIfLocked = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  //orderDetail.msgContent
  orderDetail.length = sprintf( orderDetail.msgContent,
        "%d=%c\001" // fieldIndexList.MsgType
        "%d=%d\001" // fieldIndexList.ArcaVariant
        "%d=%d\001" // fieldIndexList.Length
        "%d=%d\001" // fieldIndexList.OutSeqNum
        "%d=%d\001" // fieldIndexList.ClOrdID
        "%d=%d\001" // fieldIndexList.PCSLinkID
        "%d=%d\001" // fieldIndexList.OrdQty
        "%d=%d\001" // fieldIndexList.PriceField
        "%d=%c\001" // fieldIndexList.PriceScale
        "%d=%.8s\001"// fieldIndexList.Symbol
        "%d=%s\001" // fieldIndexList.CompGroupID
        "%d=%s\001" // fieldIndexList.DeliverToCompID
        "%d=%c\001" // fieldIndexList.ExecInst
        "%d=%c\001" // fieldIndexList.Side
        "%d=%c\001" // fieldIndexList.OrdType
        "%d=%c\001" // fieldIndexList.Time_In_Force
        "%d=%c\001" // fieldIndexList.Rule80A
        "%d=%s\001" // fieldIndexList.TradingSessionID
        "%d=%s\001" // fieldIndexList.Account
        "%d=%c\001" // fieldIndexList.ISO_Flag
        "%d=%d\001" // fieldIndexList.ExDestination
        "%d=%c\001" // fieldIndexList.ExtExeInstruction
        "%d=%c\001" // fieldIndexList.ExtendedPNP
        "%d=%c\001" // fieldIndexList.NoSelfTrade
        "%d=%c\001", // fieldIndexList.ProactiveIfLocked
        fieldIndexList.MsgType, msgType,
        fieldIndexList.ArcaVariant, variant,
        fieldIndexList.Length, newOrder.length,
        fieldIndexList.OutSeqNum, newOrder.seqNum,
        fieldIndexList.ClOrdID, newOrder.orderID,
        fieldIndexList.PCSLinkID, newOrder.PCSLinkID,
        fieldIndexList.OrdQty, newOrder.shares,
        fieldIndexList.PriceField, fieldPrice,
        fieldIndexList.PriceScale, fieldPriceScale,
        fieldIndexList.Symbol, newOrder.symbol,
        fieldIndexList.CompGroupID, newOrder.compGroupID,
        fieldIndexList.DeliverToCompID, newOrder.deliverCompID,
        fieldIndexList.ExecInst, newOrder.execIns,
        fieldIndexList.Side, action,
        fieldIndexList.OrdType, newOrder.ordType,
        fieldIndexList.Time_In_Force, tif,
        fieldIndexList.Rule80A, newOrder.rule80A,
        fieldIndexList.TradingSessionID, newOrder.tradingSessionId,
        fieldIndexList.Account, newOrder.account,         
        fieldIndexList.ISO_Flag, newOrder.isoFlag,
        fieldIndexList.ExDestination, exDestination,
        fieldIndexList.ExtExeInstruction, Lrc_print_char(newOrder.ExtExeInstruction),
        fieldIndexList.ExtendedPNP, Lrc_print_char(newOrder.ExtendedPNP),
        fieldIndexList.NoSelfTrade, noSelfTrade,
        fieldIndexList.ProactiveIfLocked, Lrc_print_char(newOrder.ProactiveIfLocked));
  
  strcpy( newOrder.msgContent, orderDetail.msgContent );

  // Update database
  if ( type == MANUAL_ORDER )
  {
    //openOrder.tsId
    openOrder.tsId = MANUAL_ORDER;

    //newOrder.tsId
    newOrder.tsId = MANUAL_ORDER;

    // Trade type
    newOrder.tradeType = NORMALLY_TRADE;
    openOrder.tradeType = NORMALLY_TRADE;

    strcpy(newOrder.entryExit, "Exit");
    strcpy(newOrder.ecnLaunch, " ");
    strcpy(newOrder.askBid, " ");
    newOrder.visibleShares = 0;

    //Add CrossID if it belogs to PM
    if (newOrder.orderID >= 500000)
    {
      bbReason = OJ_BB_REASON_PM;
      
      newOrder.crossId = GetIntNumber(&dataBlock.addContent[DATABLOCK_OFFSET_CROSS_ID]);
      openOrder.crossId = newOrder.crossId;
      
      if (strlen(pmOrderInfo[newOrder.orderID % MAX_ORDER_PLACEMENT].symbol) == 0)
      {
        strncpy(pmOrderInfo[newOrder.orderID % MAX_ORDER_PLACEMENT].symbol, newOrder.symbol, SYMBOL_LEN);
        
        PMArcaDirectOrderInfo[newOrder.orderID % MAX_ORDER_PLACEMENT].shares = newOrder.shares;
        PMArcaDirectOrderInfo[newOrder.orderID % MAX_ORDER_PLACEMENT].side = action;
      }
    }
    else
    {
      bbReason = OJ_BB_REASON_AS;
      
      newOrder.crossId = 0;
      
      if (strlen(manualOrderInfo[newOrder.orderID].symbol) == 0)
      {
        strncpy(manualOrderInfo[newOrder.orderID].symbol, newOrder.symbol, SYMBOL_LEN);
        
        ManualArcaDirectOrderInfo[newOrder.orderID].shares = newOrder.shares;
        ManualArcaDirectOrderInfo[newOrder.orderID].side = action;
      }
    }
  }
  else
  {
    if (dataBlock.addContent[DATABLOCK_OFFSET_LAUNCH] == EXIT_VALUE)
    {
      bbReason = OJ_BB_REASON_EXIT;
      
      // Get Entry/Exit
      strcpy(newOrder.entryExit, "Exit");

      strcpy(newOrder.ecnLaunch, " ");
      strcpy(newOrder.askBid, " ");

      newOrder.entryExitRefNum = *(unsigned short *)&dataBlock.addContent[DATABLOCK_OFFSET_ENTRY_EXIT_REF];
    }
    else
    {
      bbReason = OJ_BB_REASON_ENTRY;
      
      // Get Entry/Exit
      strcpy(newOrder.entryExit, "Entry");

      // Get ECN launch and Ask/Bid launch
      GetECNLaunchInfo(dataBlock.addContent[DATABLOCK_OFFSET_LAUNCH], newOrder.ecnLaunch, newOrder.askBid);
    }

    // Visible shares
    newOrder.visibleShares = GetIntNumber(&dataBlock.addContent[DATABLOCK_OFFSET_VISIBLE_SIZE]);

    //Cross Id
    newOrder.crossId = GetIntNumber(&dataBlock.addContent[DATABLOCK_OFFSET_CROSS_ID]);
    openOrder.crossId = newOrder.crossId;

    // Trade type
    newOrder.tradeType = NORMALLY_TRADE;
    openOrder.tradeType = newOrder.tradeType;

    //openOrder.tsId
    openOrder.tsId = tradeServersInfo.config[type].tsId;

    //newOrder.tsId
    newOrder.tsId = type;

    // Reset no trade duration
    time_t t = (time_t)hbitime_seconds();
    struct tm tm = *localtime(&t);

    NoTradesDurationMgt[type].currentNoTradesDurationInSecond = 0;
    NoTradesDurationMgt[type].alertThresholdInMinute = TradeTimeAlertConfig[3600 * tm.tm_hour + 60 * tm.tm_min + tm.tm_sec];

    sprintf(NoTradesDurationMgt[type].startTimeStamp, "%d:%d", tm.tm_hour, tm.tm_min);
  }

  openOrder.account = dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID];

  // Call function to add a open order
  ProcessAddASOpenOrder( &openOrder, &orderDetail );

  // Call function to update New Order of ARCA DIRECT Order in Automatic Order
  ProcessInsertNewOrder4ARCA_DIRECTQueryToCollection(&newOrder);

  //Add a new record to OJ file
  char _visibility = 'Y';

  char account[32];
  GetAccountName(TYPE_ARCA_DIRECT, dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID], account);

  int isDuplicated = 0;
  if (AddOrderIdToList(&__new, newOrder.orderID) == ERROR)
  {
    isDuplicated = 1;
  }
  
  if (isDuplicated == 0)
  {
    AddOJNewOrderRecord(timeStamp, newOrder.symbol, VENUE_ARCA, buysell, newOrder.price, newOrder.shares, newOrder.orderID, _tif, _visibility, isISO, account, NULL, newOrder.rule80A, newOrder.crossId, bbReason);
  }

  //Add ISO Order to ISO Collection for ISO Flight File creation
  if ((isISO == 1) && (isDuplicated == 0))
  {
    t_ISOOrderInfo isoOrderInfo;
    strcpy(isoOrderInfo.strTime, timeStamp);

    sprintf(isoOrderInfo.strVenueID, "%d", VENUE_ARCA);

    isoOrderInfo.strBuysell[0] = buysell;
    isoOrderInfo.strBuysell[1] = 0;

    sprintf(isoOrderInfo.strPrice, "%.2lf", newOrder.price);

    sprintf(isoOrderInfo.strShares, "%d", newOrder.shares);

    sprintf(isoOrderInfo.strSequenceID, "%d", newOrder.orderID);

    isoOrderInfo.strCapacity[0] = newOrder.rule80A;
    isoOrderInfo.strCapacity[1] = 0;

    strcpy(isoOrderInfo.strAccount, account);

    isoOrderInfo.strRoutingInfo[0] = 0; //No Routing for ARCA Diret

    AddISOOrderToCollection(newOrder.crossId, bboID, newOrder.symbol, &isoOrderInfo);
  }

  CheckAndProcessRawdataInWaitListForOrderID(newOrder.orderID);

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessNewOrderOfARCA_DIRECTOrderV3
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessNewOrderOfARCA_DIRECTOrderV3( t_DataBlock dataBlock, int type, char *timeStamp )
{
  t_ARCA_DIRECTNewOrder newOrder;
  t_OpenOrderInfo openOrder;
  t_OrderDetail orderDetail;

  /*------------------------------------------------
  Order summary:
  - double price;
  - double avgPrice;
  - char symbol[SYMBOL_LEN];
  - int side;
  - int shares;
  - int leftShares;
  - int ECNId;
  - int clOrdId;
  - int status;
  - int tif;
  - int tsId;
  - char timestamp[MAX_TIMESTAMP];
  ------------------------------------------------*/

  /*------------------------------------------------
  Order detail:
  - int length;
  - int execId;
  - int isBroken;
  - char timestamp[MAX_TIMESTAMP];
  - char msgContent[MAX_ORDER_MSG];
  ------------------------------------------------*/
  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;

  // YYYY/mm/DD
  char dateTime[32];
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]), &dateTime[0]);
  memcpy(newOrder.date, &dateTime[0], 10);
  newOrder.date[10] = 0;

  // HH:MM:SS.mss
  memcpy(newOrder.timestamp, &dateTime[11], TIME_LENGTH);
  newOrder.timestamp[TIME_LENGTH] = 0;

  // openOrder.timestamp
  memcpy( openOrder.timestamp, &dateTime[0], TIMESTAMP_LENGTH );
  openOrder.timestamp[TIMESTAMP_LENGTH] = 0;

  // orderDetail
  strcpy( orderDetail.timestamp, openOrder.timestamp );

  // "ARCA", "OUCH", "RASH" or "BATZ"
  strcpy( newOrder.ecn, "ARCA");

  // openOrder.ECNId
  openOrder.ECNId = TYPE_ARCA_DIRECT;

  // Status: Sent, Live, Rejected, Closed, BrokenTrade, CanceledByECN,
  //      CXLSent, CancelACK, CancelRejected, CancelPending, CanceledByUser
  strcpy( newOrder.status, orderStatus.Sent );

  // openOrder.status
  openOrder.status = orderStatusIndex.Sent;

  // Messsage type
  strcpy( newOrder.msgType, "NewOrder" );

  int currentIndex = 0;
  // msgType
  char msgType = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Variant (1 byte)
  int variant = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);
  
  // Length
  newOrder.length = GetShortNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Sequence number
  newOrder.seqNum = GetIntNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Order ID
  newOrder.orderID = GetIntNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // openOrder.clOrdId
  openOrder.clOrdId = newOrder.orderID;

  // PCS link ID
  newOrder.PCSLinkID = GetIntNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Shares
  newOrder.shares = GetIntNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);
    
  // Strike price
  currentIndex += 4;
  
  // Min Quantity
  currentIndex += 4;
  
  // Max floor (use when placing )
  newOrder.maxFloor = GetIntNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);
  
  // Display range
  currentIndex += 4;
  
  // DiscretionOffset
  currentIndex += 4;
  
  // Peg Difference
  currentIndex += 4;
  
  // StopPx
  currentIndex += 4;
  
  // openOrder.shares
  openOrder.shares = newOrder.shares;
  openOrder.leftShares = openOrder.shares;

  int  fieldPrice = GetIntNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // ExDestination (2 bytes)
  int exDestination = GetShortNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);
  
  // Underlying Quantity
  currentIndex += 2;
  
  // Put Call
  currentIndex += 1;
  
  // LocalOrAway
  currentIndex += 1;

  // Price and Price Scale
  char fieldPriceScale = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  newOrder.price = fieldPrice * 1.0 / pow( 10, fieldPriceScale - 48 );
  
  // Corporate Action
  currentIndex += 1;
  
  // Open Close
  currentIndex += 1;

  // openOrder.price
  openOrder.price = newOrder.price;
  openOrder.avgPrice = openOrder.price;

  // Stock symbol
  GetStringV1(newOrder.symbol, &dataBlock.msgContent[currentIndex], SYMBOL_LEN, &currentIndex);

  // openOrder.symbol
  memcpy( openOrder.symbol, newOrder.symbol, SYMBOL_LEN);
  
  // Strike Date
  currentIndex += 8;

  // Company group Id
  GetStringV1(newOrder.compGroupID, &dataBlock.msgContent[currentIndex], 5, &currentIndex);
  newOrder.compGroupID[5] = 0;

  // Deliver to Comp Id
  GetStringV1(newOrder.deliverCompID, &dataBlock.msgContent[currentIndex], 5, &currentIndex);
  newOrder.deliverCompID[5] = 0;
  
  // SenderSubID (5 bytes)
  char senderSubID[6];
  GetStringV1(senderSubID, &dataBlock.msgContent[currentIndex], 5, &currentIndex);
  senderSubID[5] = 0;
  
  // Execution instruction
  newOrder.execIns = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // "B", "S" or "SS"
  char action = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);
  char buysell = '0';
  switch ( action )
  {
    case '1':
      buysell = 'B';
      strcpy( newOrder.side, "BC" );
      openOrder.side = BUY_TO_CLOSE_TYPE;
      break;

    case '2':
      buysell = 'S';
      strcpy( newOrder.side, "S" );
      openOrder.side = SELL_TYPE;
      break;

    case '5':
      buysell = 'T';
      strcpy( newOrder.side, "SS" );
      openOrder.side = SHORT_SELL_TYPE;
      break;

    default:
      strcpy( newOrder.side, " " );
      openOrder.side = -1;

      TraceLog(ERROR_LEVEL, "ARCA_DIRECT New Order V3: not support side = %c, %d\n", action, action );
      break;
  }

  // Order type
  newOrder.ordType = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Time in force
  char tif = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);
  char _tif[6];
  switch (tif)
  {
    case '0':
      strcpy( newOrder.timeInForce, "DAY" );
      openOrder.tif = DAY_TYPE;
      strcpy(_tif, "99999");
      break;

    case '3':
      strcpy( newOrder.timeInForce, "IOC" );
      strcpy(_tif, "0");
      openOrder.tif = IOC_TYPE;
      break;

    default:
      strcpy( newOrder.timeInForce, " " );
      openOrder.tif = -1;
      TraceLog(ERROR_LEVEL, "ARCA_DIRECTOrder V3: not support tif = %c, %d\n", tif, tif );
      break;
  }

  // Rule 80A
  newOrder.rule80A = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);
  
  // Customer Or Firm
  currentIndex += 1;

  // Trading session id
  GetStringV1(newOrder.tradingSessionId, &dataBlock.msgContent[currentIndex], 4, &currentIndex);
  newOrder.tradingSessionId[4] = 0;
  
  // Account
  GetStringV1(newOrder.account, &dataBlock.msgContent[currentIndex], 10, &currentIndex);
  newOrder.account[10] = 0;
  
  newOrder.tradingAccount = dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID];
  
  // Optional Data
  currentIndex += 16;
  
  // Clearing Firm
  currentIndex += 5;
  
  // Clearing Account 
  currentIndex += 5;
  
  // Expire Time Flag
  currentIndex += 1;
  
  // ExpireTime
  currentIndex += 4;
  
  // Effective Time
  currentIndex += 4;
  
  // DiscretionInst
  currentIndex += 1;
  
  // Proactive Discretion Indicator
  currentIndex += 1;
  
  // ProactiveIfLocked
  newOrder.ProactiveIfLocked = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);
  
  // ExecBroker
  currentIndex += 5;

  // Iso Flag
  newOrder.isoFlag = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  int isISO = 0;
  short bboID = -1;
  unsigned char bbReason;
  if (newOrder.isoFlag == 'Y')
  {
    isISO = 1;
    bboID = GetShortNumber(&dataBlock.addContent[DATABLOCK_OFFSET_BBO_ID]);
  }

  newOrder.bboId = bboID;
  
  // Symbol Type 
  currentIndex += 1;

  // Extended Execution instruction
  newOrder.ExtExeInstruction = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // ExtendedPNP (1 byte)
  newOrder.ExtendedPNP = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // NoSelfTrade (1 byte)
  char noSelfTrade = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Filler
  currentIndex += 3;
  
  //orderDetail.msgContent
  orderDetail.length = sprintf( orderDetail.msgContent,
        "%d=%c\001" // fieldIndexList.MsgType
        "%d=%d\001" // fieldIndexList.ArcaVariant
        "%d=%d\001" // fieldIndexList.Length
        "%d=%d\001" // fieldIndexList.OutSeqNum
        "%d=%d\001" // fieldIndexList.ClOrdID
        "%d=%d\001" // fieldIndexList.PCSLinkID
        "%d=%d\001" // fieldIndexList.OrdQty
        "%d=%d\001" // fieldIndexList.MaxFloor
        "%d=%d\001" // fieldIndexList.PriceField
        "%d=%c\001" // fieldIndexList.PriceScale
        "%d=%.8s\001"// fieldIndexList.Symbol
        "%d=%s\001" // fieldIndexList.CompGroupID
        "%d=%s\001" // fieldIndexList.DeliverToCompID
        "%d=%c\001" // fieldIndexList.ExecInst
        "%d=%c\001" // fieldIndexList.Side
        "%d=%c\001" // fieldIndexList.OrdType
        "%d=%c\001" // fieldIndexList.Time_In_Force
        "%d=%c\001" // fieldIndexList.Rule80A
        "%d=%s\001" // fieldIndexList.TradingSessionID
        "%d=%s\001" // fieldIndexList.Account
        "%d=%c\001" // fieldIndexList.ISO_Flag
        "%d=%d\001" // fieldIndexList.ExDestination
        "%d=%c\001" // fieldIndexList.ExtExeInstruction
        "%d=%c\001" // fieldIndexList.ExtendedPNP
        "%d=%c\001" // fieldIndexList.NoSelfTrade
        "%d=%c\001", // fieldIndexList.ProactiveIfLocked
        fieldIndexList.MsgType, msgType,
        fieldIndexList.ArcaVariant, variant,
        fieldIndexList.Length, newOrder.length,
        fieldIndexList.OutSeqNum, newOrder.seqNum,
        fieldIndexList.ClOrdID, newOrder.orderID,
        fieldIndexList.PCSLinkID, newOrder.PCSLinkID,
        fieldIndexList.OrdQty, newOrder.shares,
        fieldIndexList.MaxFloor, newOrder.maxFloor,
        fieldIndexList.PriceField, fieldPrice,
        fieldIndexList.PriceScale, fieldPriceScale,
        fieldIndexList.Symbol, newOrder.symbol,
        fieldIndexList.CompGroupID, newOrder.compGroupID,
        fieldIndexList.DeliverToCompID, newOrder.deliverCompID,
        fieldIndexList.ExecInst, newOrder.execIns,
        fieldIndexList.Side, action,
        fieldIndexList.OrdType, newOrder.ordType,
        fieldIndexList.Time_In_Force, tif,
        fieldIndexList.Rule80A, newOrder.rule80A,
        fieldIndexList.TradingSessionID, newOrder.tradingSessionId,
        fieldIndexList.Account, newOrder.account,         
        fieldIndexList.ISO_Flag, newOrder.isoFlag,
        fieldIndexList.ExDestination, exDestination,
        fieldIndexList.ExtExeInstruction, Lrc_print_char(newOrder.ExtExeInstruction),
        fieldIndexList.ExtendedPNP, Lrc_print_char(newOrder.ExtendedPNP),
        fieldIndexList.NoSelfTrade, noSelfTrade,
        fieldIndexList.ProactiveIfLocked, Lrc_print_char(newOrder.ProactiveIfLocked));
  
  strcpy( newOrder.msgContent, orderDetail.msgContent );

  // Update database
  if ( type == MANUAL_ORDER )
  {
    //openOrder.tsId
    openOrder.tsId = MANUAL_ORDER;

    //newOrder.tsId
    newOrder.tsId = MANUAL_ORDER;

    // Trade type
    newOrder.tradeType = NORMALLY_TRADE;
    openOrder.tradeType = NORMALLY_TRADE;

    strcpy(newOrder.entryExit, "Exit");
    strcpy(newOrder.ecnLaunch, " ");
    strcpy(newOrder.askBid, " ");
    newOrder.visibleShares = 0;

    //Add CrossID if it belogs to PM
    if (newOrder.orderID >= 500000)
    {
      bbReason = OJ_BB_REASON_PM;
      newOrder.crossId = GetIntNumber(&dataBlock.addContent[DATABLOCK_OFFSET_CROSS_ID]);
      openOrder.crossId = newOrder.crossId;
      
      if (strlen(pmOrderInfo[newOrder.orderID % MAX_ORDER_PLACEMENT].symbol) == 0)
      {
        strncpy(pmOrderInfo[newOrder.orderID % MAX_ORDER_PLACEMENT].symbol, newOrder.symbol, SYMBOL_LEN);
        
        PMArcaDirectOrderInfo[newOrder.orderID % MAX_ORDER_PLACEMENT].shares = newOrder.shares;
        PMArcaDirectOrderInfo[newOrder.orderID % MAX_ORDER_PLACEMENT].side = action;
      }
    }
    else
    {
      bbReason = OJ_BB_REASON_AS;
      newOrder.crossId = 0;
      
      if (strlen(manualOrderInfo[newOrder.orderID].symbol) == 0)
      {
        strncpy(manualOrderInfo[newOrder.orderID].symbol, newOrder.symbol, SYMBOL_LEN);
        
        ManualArcaDirectOrderInfo[newOrder.orderID].shares = newOrder.shares;
        ManualArcaDirectOrderInfo[newOrder.orderID].side = action;
      }
    }
  }
  else
  {
    if (dataBlock.addContent[DATABLOCK_OFFSET_LAUNCH] == EXIT_VALUE)
    {
      bbReason = OJ_BB_REASON_EXIT;
      
      // Get Entry/Exit
      strcpy(newOrder.entryExit, "Exit");

      strcpy(newOrder.ecnLaunch, " ");
      strcpy(newOrder.askBid, " ");

      newOrder.entryExitRefNum = *(unsigned short *)&dataBlock.addContent[DATABLOCK_OFFSET_ENTRY_EXIT_REF];
    }
    else
    {
      bbReason = OJ_BB_REASON_ENTRY;
      
      // Get Entry/Exit
      strcpy(newOrder.entryExit, "Entry");

      // Get ECN launch and Ask/Bid launch
      GetECNLaunchInfo(dataBlock.addContent[DATABLOCK_OFFSET_LAUNCH], newOrder.ecnLaunch, newOrder.askBid);
    }

    // Visible shares
    newOrder.visibleShares = GetIntNumber(&dataBlock.addContent[DATABLOCK_OFFSET_VISIBLE_SIZE]);

    //Cross Id
    newOrder.crossId = GetIntNumber(&dataBlock.addContent[DATABLOCK_OFFSET_CROSS_ID]);
    openOrder.crossId = newOrder.crossId;

    // Trade type
    newOrder.tradeType = NORMALLY_TRADE;
    openOrder.tradeType = newOrder.tradeType;

    //openOrder.tsId
    openOrder.tsId = tradeServersInfo.config[type].tsId;

    //newOrder.tsId
    newOrder.tsId = type;

    // Reset no trade duration
    time_t t = (time_t)hbitime_seconds();
    struct tm tm = *localtime(&t);

    NoTradesDurationMgt[type].currentNoTradesDurationInSecond = 0;
    NoTradesDurationMgt[type].alertThresholdInMinute = TradeTimeAlertConfig[3600 * tm.tm_hour + 60 * tm.tm_min + tm.tm_sec];

    sprintf(NoTradesDurationMgt[type].startTimeStamp, "%d:%d", tm.tm_hour, tm.tm_min);
  }

  openOrder.account = dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID];

  // Call function to add a open order
  ProcessAddASOpenOrder( &openOrder, &orderDetail );

  // Call function to update New Order of ARCA DIRECT Order in Automatic Order
  ProcessInsertNewOrder4ARCA_DIRECTQueryToCollection(&newOrder);

  //Add a new record to OJ file
  char _visibility = 'Y';

  char account[32];
  GetAccountName(TYPE_ARCA_DIRECT, dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID], account);

  int isDuplicated = 0;
  if (AddOrderIdToList(&__new, newOrder.orderID) == ERROR)
  {
    isDuplicated = 1;
  }
  
  if (isDuplicated == 0)
  {
    AddOJNewOrderRecord(timeStamp, newOrder.symbol, VENUE_ARCA, buysell, newOrder.price, newOrder.shares, newOrder.orderID, _tif, _visibility, isISO, account, NULL, newOrder.rule80A, newOrder.crossId, bbReason);
  }

  //Add ISO Order to ISO Collection for ISO Flight File creation
  if ((isISO == 1) && (isDuplicated == 0))
  {
    t_ISOOrderInfo isoOrderInfo;
    strcpy(isoOrderInfo.strTime, timeStamp);

    sprintf(isoOrderInfo.strVenueID, "%d", VENUE_ARCA);

    isoOrderInfo.strBuysell[0] = buysell;
    isoOrderInfo.strBuysell[1] = 0;

    sprintf(isoOrderInfo.strPrice, "%.2lf", newOrder.price);

    sprintf(isoOrderInfo.strShares, "%d", newOrder.shares);

    sprintf(isoOrderInfo.strSequenceID, "%d", newOrder.orderID);

    isoOrderInfo.strCapacity[0] = newOrder.rule80A;
    isoOrderInfo.strCapacity[1] = 0;

    strcpy(isoOrderInfo.strAccount, account);

    isoOrderInfo.strRoutingInfo[0] = 0; //No Routing for ARCA Diret

    AddISOOrderToCollection(newOrder.crossId, bboID, newOrder.symbol, &isoOrderInfo);
  }

  CheckAndProcessRawdataInWaitListForOrderID(newOrder.orderID);

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessOrderACKOfARCA_DIRECTOrder
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessOrderACKOfARCA_DIRECTOrder( t_DataBlock dataBlock, int type, char *timeStamp )
{
  /*
  - API version: ArcaDirect 4.1
  - Date updated: 21-Mar-2013
  - Modified by: Danh Hoang-Tang-Van
  - Order Ack Message (Variant 1), it includes the following fields:
    + Message Type    1   Alpha/Numeric   �a�
    + Variant     1   Binary    Value = 0
    + Length      2   Binary    Binary length of the message. Value = 32.
    + Sequence Number 4   Binary    Arca-assigned sequence number
    + Sending Time      8   Binary    UTC Time the message was sent in microseconds since Midnight
    + Transaction Time  8   Binary    UTC Time the message was sent in microseconds since Midnight
    + Client Order Id   4   Binary    Client order ID
    + Order ID      8   Binary    Exchange assigned Order ID
    + Filler      7   Ignore
    + Message Terminator  1   Alpha   ASCII new line character: �\n�
    ==> Total byte    32
  */
  t_ARCA_DIRECTOrderACK orderACK;
  t_OpenOrderInfo openOrder;
  t_OrderDetail orderDetail;
  /*------------------------------------------------
  Order summary:
  - double price;
  - double avgPrice;
  - char symbol[SYMBOL_LEN];
  - int side;
  - int shares;
  - int leftShares;
  - int ECNId;
  - int clOrdId;
  - int status;
  - int tif;
  - int tsId;
  - char timestamp[MAX_TIMESTAMP];
  ------------------------------------------------*/

  /*------------------------------------------------
  Order detail:
  - int length;
  - int execId;
  - int isBroken;
  - char timestamp[MAX_TIMESTAMP];
  - char msgContent[MAX_ORDER_MSG];
  ------------------------------------------------*/
  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;

  // YYYY/mm/DD
  char dateTime[32];
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]), &dateTime[0]);
  memcpy( orderACK.processedTimestamp, &dateTime[11], TIME_LENGTH );
  orderACK.processedTimestamp[TIME_LENGTH] = 0;
  
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]), &dateTime[0]);
  memcpy( orderACK.date, &dateTime[0], 10 );
  orderACK.date[10] = 0;

  // HH:MM:SS.ms
  memcpy( orderACK.timestamp, &dateTime[11], TIME_LENGTH );
  orderACK.timestamp[TIME_LENGTH] = 0;

  // openOrder.timestamp
  memcpy(openOrder.timestamp, &dateTime[0], TIMESTAMP_LENGTH);
  openOrder.timestamp[TIMESTAMP_LENGTH] = 0;

  strcpy(orderDetail.timestamp, openOrder.timestamp);

  //openOrder.ECNId
  openOrder.ECNId = TYPE_ARCA_DIRECT;

  // Status: Sent, Live, Rejected, Closed, BrokenTrade, CanceledByECN,
  //      CXLSent, CancelACK, CancelRejected, CancelPending, CanceledByUser
  strcpy( orderACK.status, orderStatus.Live );

  // openOrder.status
  openOrder.status = orderStatusIndex.Live;

  // Messsage type
  strcpy( orderACK.msgType, "OrderACK" );

  // Stock symbol
  memset( openOrder.symbol, 0, SYMBOL_LEN);

  // Side
  openOrder.side = -1;

  // Shares and leftShares
  openOrder.shares = 0;
  openOrder.leftShares = 0;

  // Price and avgPrice
  openOrder.price = 0.00;
  openOrder.avgPrice = 0.00;

  orderACK.price = 0.00;

  // Time in force
  openOrder.tif = -1;

  int currentIndex = 0;

  // msgType
  char msgType = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Variant (1 byte)
  int variant = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Length
  orderACK.length = GetShortNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Sequence number
  orderACK.seqNum = GetIntNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  //Sending time
  orderACK.sendingTime = GetLongNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  //Transaction time
  orderACK.transTime = GetLongNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Client Order ID
  orderACK.orderID = GetIntNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);
  
  char exOrderId[32];
  sprintf(exOrderId, "%d", orderACK.orderID);
  
  // openOrder.clOrdId
  openOrder.clOrdId = orderACK.orderID;

  //Arca Order ID
  orderACK.arcaExOrderId = GetLongNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  int  fieldPrice = GetIntNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Price and Price Scale
  char fieldPriceScale = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  orderACK.price = fieldPrice * 1.0 / pow( 10, fieldPriceScale - 48 );

  // openOrder.price
  openOrder.price = orderACK.price;
  openOrder.avgPrice = openOrder.price;

  orderACK.liquidity = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // orderDetail.msgContent
  orderDetail.length = sprintf( orderDetail.msgContent,
          "%d=%c\001"  // fieldIndexList.MsgType
          "%d=%d\001"  // fieldIndexList.ArcaVariant
          "%d=%d\001"  // fieldIndexList.Length
          "%d=%d\001"  // fieldIndexList.InSeqNum
          "%d=%ld\001" // fieldIndexList.SendingTime
          "%d=%ld\001" // fieldIndexList.TransactionTime
          "%d=%d\001"  // fieldIndexList.ClOrdID
          "%d=%ld\001" // fieldIndexList.ArcaOrderID
          "%d=%d\001"  // fieldIndexList.PriceField
          "%d=%c\001"  // fieldIndexList.PriceScale
          "%d=%c\001", // fieldIndexList.LiquidityIndicator
          fieldIndexList.MsgType, msgType,
          fieldIndexList.ArcaVariant, variant,
          fieldIndexList.Length, orderACK.length,
          fieldIndexList.InSeqNum, orderACK.seqNum,
          fieldIndexList.SendingTime, orderACK.sendingTime,
          fieldIndexList.TransactionTime, orderACK.transTime,
          fieldIndexList.ClOrdID, orderACK.orderID,
          fieldIndexList.ArcaOrderID, orderACK.arcaExOrderId,
          fieldIndexList.PriceField, fieldPrice,
          fieldIndexList.PriceScale, fieldPriceScale,
          fieldIndexList.LiquidityIndicator, orderACK.liquidity
          );

  strcpy(orderACK.msgContent, orderDetail.msgContent);

  // Update database
  if( type == MANUAL_ORDER )
  {
    // openOrder.tsId
    openOrder.tsId = MANUAL_ORDER;
  }
  else
  {
    // openOrder.tsId
    openOrder.tsId = tradeServersInfo.config[type].tsId;
  }

  // Call function to update a open order
  openOrder.account = dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID];
  int indexOpenOrder = ProcessUpdateASOpenOrderForOrderACK( &openOrder, &orderDetail );

  if (indexOpenOrder == -2)
  {
    AddDataBlockToWaitList(TYPE_ARCA_DIRECT, WAIT_LIST_TYPE_ARCA_DIRECT_ORDER_ACK, orderACK.orderID, dataBlock, type, timeStamp);
    return 0;
  }

  // Call function to update Order ACK of ARCA DIRECT Order in Automatic Order
  ProcessInsertOrderACK4ARCA_DIRECTQueryToCollection(&orderACK);

  if (AddOrderIdToList(&__ack, openOrder.clOrdId) == ERROR) //Duplicated
  {
    return 0;
  }
  
  char ecnOrderID[32];
  sprintf(ecnOrderID, "%lu", orderACK.arcaExOrderId);

  char account[32];
  GetAccountName(TYPE_ARCA_DIRECT, dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID], account);

  char capacity = 'P';
  
  if (indexOpenOrder != ERROR)
  {
    AddOJOrderAcceptedRecord(timeStamp, asOpenOrder.orderCollection[indexOpenOrder].orderSummary.symbol, exOrderId, openOrder.clOrdId, ecnOrderID, VENUE_ARCA, account, capacity);
  }
  else
  {
    AddOJOrderAcceptedRecord(timeStamp, "", exOrderId, openOrder.clOrdId, ecnOrderID, VENUE_ARCA, account, capacity);
  }

  return 0;
}

/****************************************************************************
- Function name:  ProcessOrderFillOfARCA_DIRECTOrder
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessOrderFillOfARCA_DIRECTOrder( t_DataBlock dataBlock, int type, char *timeStamp )
{
  /*
  - API version: ArcaDirect 4.1
  - Date updated: 21-Mar-2013
  - Modified by: Danh Hoang-Tang-Van
  - Order Fill Message (Variant 1), it includes the following fields:
    + Message Type      1   Alpha Numeric   �2�
    + Variant       1   Binary    Value = 0
    + Length        2   Binary    Binary length of the message. Value = 48.
    + Sequence Number   4   Binary    Arca-assigned sequence number
    + Sending Time          8   Binary    UTC Time the message was sent in microseconds since Midnight
    + Transaction Time    8   Binary    UTC Time the message was sent in microseconds since Midnight
    + Client Order Id     4   Binary    Client order ID
    + Order ID        8   Binary    Exchange assigned Order ID
    + Execution ID      8   Binary    Exchange assigned Execution ID
    + Last Shares or Contracts  4   Binary    Number of equity shares or option contracts filled
    + Last Price        4   Binary    Price at which the shares or contracts were filled
    + Price Scale     1   Alpha Numeric   �0� through �4�
    + Liquidity Indicator   1   Alpha Numeric
    + Side        1   Alpha Numeric
    + Filler        4   Ignore
    + Message Terminator    1   Alpha     ASCII new line character: �\n�
    ==>Total byte   48
  */

  t_ARCA_DIRECTOrderFill orderFill;
  t_OrderDetail orderDetail;
  /*------------------------------------------------
  Order detail:
  - int length;
  - int execId;
  - int isBroken;
  - char timestamp[MAX_TIMESTAMP];
  - char msgContent[MAX_ORDER_MSG];
  ------------------------------------------------*/
  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;

  // YYYY/mm/DD
  char dateTime[32];
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]), &dateTime[0]);
  memcpy( orderFill.processedTimestamp, &dateTime[11], TIME_LENGTH );
  orderFill.processedTimestamp[TIME_LENGTH] = 0;
  
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]), &dateTime[0]);
  memcpy( orderFill.date, &dateTime[0], 10 );
  orderFill.date[10] = 0;

  // HH:MM:SS.ms
  memcpy( orderFill.timestamp, &dateTime[11], TIME_LENGTH );
  orderFill.timestamp[TIME_LENGTH] = 0;

  // orderDetail.timestamp
  memcpy(orderDetail.timestamp, &dateTime[0], TIMESTAMP_LENGTH);
  orderDetail.timestamp[TIMESTAMP_LENGTH] = 0;

  // Messsage type
  strcpy( orderFill.msgType, "OrderFill" );

  int currentIndex = 0;

  // msgType
  char msgType = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Variant (1 byte)
  int variant = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Length
  orderFill.length = GetShortNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Sequence number
  orderFill.seqNum = GetIntNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Sending time
  orderFill.sendingTime = GetLongNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  //Transaction time
  orderFill.transTime = GetLongNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Order ID
  orderFill.orderID = GetIntNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);
  char exOrderId[32];
  sprintf(exOrderId, "%d", orderFill.orderID);
  
  // Arca Order ID
  orderFill.arcaExOrderId = GetLongNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  //Arca Execution ID
  orderFill.executionId = GetLongNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Trade record
  GetStringV1(orderFill.ArcaExID, &dataBlock.msgContent[currentIndex], 20, &currentIndex);
  orderFill.ArcaExID[20] = 0;
  
  // Shares
  orderFill.shares = GetIntNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Price
  int  fieldPrice = GetIntNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  char fieldPriceScale = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  orderFill.price = fieldPrice * 1.0 / pow( 10, fieldPriceScale - 48 );

  // Liquidity
  orderFill.liquidityIndicator = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // "B", "S" or "SS"
  char action = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);
  char buysell = '0';
  switch ( action )
  {
    case '1':
      buysell = 'B';
      strcpy( orderFill.side, "B" );
      break;

    case '2':
      buysell = 'S';
      strcpy( orderFill.side, "S" );
      break;

    case '5':
      buysell = 'T';
      strcpy( orderFill.side, "SS" );
      break;

    default:
      strcpy( orderFill.side, " " );

      TraceLog(ERROR_LEVEL, "ARCA_DIRECT Fill Order: not support side = %c, %d\n", action, action );
      break;
  }

  GetStringV1(orderFill.LastMkt, &dataBlock.msgContent[currentIndex], 2, &currentIndex);
  orderFill.LastMkt[2] = 0;
  
  // orderDetail.msgContent
  orderDetail.execId = orderFill.executionId;

  orderDetail.length = sprintf( orderDetail.msgContent,
          "%d=%c\001" // fieldIndexList.MsgType
          "%d=%d\001" // fieldIndexList.ArcaVariant
          "%d=%d\001" // fieldIndexList.Length
          "%d=%d\001" // fieldIndexList.InSeqNum
          "%d=%ld\001"// fieldIndexList.SendingTime
          "%d=%ld\001"// fieldIndexList.TransactionTime
          "%d=%d\001" // fieldIndexList.ClOrdID
          "%d=%ld\001"// fieldIndexList.ArcaOrderID
          "%d=%ld\001"// fieldIndexList.ExecID
          "%d=%s\001" // fieldIndexList.ArcaExID
          "%d=%d\001" // fieldIndexList.OrdQty
          "%d=%d\001" // fieldIndexList.PriceField
          "%d=%c\001" // fieldIndexList.PriceScale
          "%d=%c\001" // fieldIndexList.LiquidityIndicator
          "%d=%c\001" // fieldIndexList.Side
          "%d=%s\001",// fieldIndexList.LastMarket
          fieldIndexList.MsgType, msgType,
          fieldIndexList.ArcaVariant, variant,
          fieldIndexList.Length, orderFill.length,
          fieldIndexList.InSeqNum, orderFill.seqNum,
          fieldIndexList.SendingTime, orderFill.sendingTime,
          fieldIndexList.TransactionTime, orderFill.transTime,
          fieldIndexList.ClOrdID, orderFill.orderID,
          fieldIndexList.ArcaOrderID, orderFill.arcaExOrderId,
          fieldIndexList.ExecID, orderFill.executionId,
          fieldIndexList.ArcaExID, orderFill.ArcaExID,
          fieldIndexList.OrdQty, orderFill.shares,
          fieldIndexList.PriceField, fieldPrice,
          fieldIndexList.PriceScale, fieldPriceScale,
          fieldIndexList.LiquidityIndicator, orderFill.liquidityIndicator,
          fieldIndexList.Side, action,
          fieldIndexList.LastMarket, orderFill.LastMkt);


  strcpy( orderFill.msgContent, orderDetail.msgContent );

  // Update database
  int tsId;

  if( type == MANUAL_ORDER )
  {
    // tsId
    tsId = MANUAL_ORDER;
  }
  else
  {
    // tsId
    tsId = tradeServersInfo.config[type].tsId;
  }

  orderFill.entryExitRefNum = *(unsigned short *)&dataBlock.addContent[DATABLOCK_OFFSET_ENTRY_EXIT_REF];
  
  char liquidityIndicator[6];
  liquidityIndicator[0] = orderFill.liquidityIndicator;
  liquidityIndicator[1] = 0;

  // Call function to update a open order (non fill)
  int indexOpenOrder = ProcessUpdateASOpenOrderForExecuted(tsId, orderFill.orderID, orderStatusIndex.Closed, orderFill.shares, orderFill.price, &orderDetail, dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID], liquidityIndicator, &orderFill.fillFee);

  if (indexOpenOrder == -2)
  {
    AddDataBlockToWaitList(TYPE_ARCA_DIRECT, WAIT_LIST_TYPE_ARCA_DIRECT_ORDER_FILLED, orderFill.orderID, dataBlock, type, timeStamp);
    return 0;
  }

  if (indexOpenOrder != ERROR)
  {
    strcpy( orderFill.status, GetOrderStatus(asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status));

    orderFill.leftShares = asOpenOrder.orderCollection[indexOpenOrder].orderSummary.leftShares;
    orderFill.avgPrice = asOpenOrder.orderCollection[indexOpenOrder].orderSummary.avgPrice;
  }

  // Call function to update Order Fill of ARCA DIRECT
  ProcessInsertExecutedOrder4ARCA_DIRECTQueryToCollection(&orderFill);

  if (AddExecDataToList(&__exec, orderFill.orderID, orderFill.executionId) == ERROR)  //Duplicated
  {
    return 0;
  }
  
  char ecnOrderID[32], execID[32];
  sprintf(ecnOrderID, "%lu", orderFill.arcaExOrderId);
  sprintf(execID, "%lu", orderFill.executionId);

  char account[32];
  GetAccountName(TYPE_ARCA_DIRECT, dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID], account);
  
  if (indexOpenOrder != ERROR)
  {
    AddOJOrderFillRecord(timeStamp, asOpenOrder.orderCollection[indexOpenOrder].orderSummary.symbol, exOrderId, VENUE_ARCA, buysell, orderFill.price,
              orderFill.shares, orderFill.orderID, liquidityIndicator,
              ecnOrderID, execID, "1", (orderFill.leftShares == 0)?1:0, account, 0);
  }
  else
  {
    AddOJOrderFillRecord(timeStamp, "", exOrderId, VENUE_ARCA, buysell, orderFill.price,
              orderFill.shares, orderFill.orderID, liquidityIndicator,
              ecnOrderID, execID, "1", (orderFill.leftShares == 0)?1:0, account, 0);
  }

  return 0;
}

/****************************************************************************
- Function name:  ProcessOrderCancelOfARCA_DIRECTOrder
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessOrderCancelOfARCA_DIRECTOrder( t_DataBlock dataBlock, int type, char *timeStamp )
{
  /*
  - API version: ArcaDirect 4.1
  - Date updated: 21-Mar-2013
  - Modified by: Danh Hoang-Tang-Van
  - Order Cancel Message Equities (Variant 1), it includes the following fields:
    + Message Type (1 byte) = '4'
    + Variant (1 byte) = 1
    + Length (2 bytes) = 72
    + Sequence Number (4 bytes) = Client-assigned sequence number
    + OrderID (8 bytes) = NYSE Arca assigned OrderID.
    + Client Order Id (4 bytes) = A client-assigned ID for this order
    + Strike Price (4 bytes) = null
    + Under Qty (2 bytes) = null
    + ExDestination (2 bytes) = 102
    + Corporate Action (1 byte) = null
    + Pull or Call (1 byte) = null
    + Bulk Cancel (1 byte) = null
    + Open or Close (1 byte) = null
    + Symbol (8 bytes) = Stock symbol
    + Strike Date (8 bytes) = null
    + Side (1 byte) = '1', '2', or '5'
    + Deliver To Comp ID (5 bytes) = null
    + Account (10 bytes) = null
    + Filler (7 byte) = null
    + Message Terminator (1 byte) = '\n'
    => Total byte = 72
  */

  t_ARCA_DIRECTOrderCancel orderCancelled;
  t_OrderDetail orderDetail;
  /*------------------------------------------------
  Order detail:
  - int length;
  - int execId;
  - int isBroken;
  - char timestamp[MAX_TIMESTAMP];
  - char msgContent[MAX_ORDER_MSG];
  ------------------------------------------------*/
  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;

  // YYYY/mm/DD
  char dateTime[32];
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]), &dateTime[0]);
    memcpy( orderCancelled.processedTimestamp, &dateTime[11], TIME_LENGTH );
  orderCancelled.processedTimestamp[TIME_LENGTH] = 0;
    
    GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]), &dateTime[0]);
  memcpy( orderCancelled.date, &dateTime[0], 10 );
  orderCancelled.date[10] = 0;

  // HH:MM:SS.ms
  memcpy( orderCancelled.timestamp, &dateTime[11], TIME_LENGTH );
  orderCancelled.timestamp[TIME_LENGTH] = 0;

  // orderDetail.timestamp
  memcpy(orderDetail.timestamp, &dateTime[0], TIMESTAMP_LENGTH);
  orderDetail.timestamp[TIMESTAMP_LENGTH] = 0;

  // Messsage type
  strcpy( orderCancelled.msgType, "OrderCancel" );

  int currentIndex = 0;

  // msgType
  char msgType = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Variant (1 byte)
  int variant = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Length
  orderCancelled.length = GetShortNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Sequence number
  orderCancelled.seqNum = GetIntNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // SendingTime
  orderCancelled.sendingTime = GetLongNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // TransactionTime
  orderCancelled.transTime = GetLongNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Order ID
  orderCancelled.orderID = GetIntNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  //Arca Order ID
  orderCancelled.arcaExOrderId = GetLongNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // orderDetail.msgContent
  orderDetail.length = sprintf( orderDetail.msgContent,
          "%d=%c\001" // fieldIndexList.MsgType
          "%d=%d\001" // fieldIndexList.ArcaVariant
          "%d=%d\001" // fieldIndexList.Length
          "%d=%d\001" // fieldIndexList.InSeqNum
          "%d=%ld\001"// fieldIndexList.SendingTime
          "%d=%ld\001" // fieldIndexList.TransactionTime
          "%d=%d\001" // fieldIndexList.ClOrdID
          "%d=%ld\001", // fieldIndexList.ArcaOrderID
          fieldIndexList.MsgType, msgType,
          fieldIndexList.ArcaVariant, variant,
          fieldIndexList.Length, orderCancelled.length,
          fieldIndexList.InSeqNum, orderCancelled.seqNum,
          fieldIndexList.SendingTime, orderCancelled.sendingTime,
          fieldIndexList.TransactionTime, orderCancelled.transTime,
          fieldIndexList.ClOrdID, orderCancelled.orderID,
          fieldIndexList.ArcaOrderID, orderCancelled.arcaExOrderId);

  strcpy( orderCancelled.msgContent, orderDetail.msgContent );

  // Update database
  int tsId;

  if( type == MANUAL_ORDER )
  {
    // tsId
    tsId = MANUAL_ORDER;
  }
  else
  {
    // tsId
    tsId = tradeServersInfo.config[type].tsId;
  }

  // Call function to update a open order (non fill)
  int indexOpenOrder = ProcessUpdateASOpenOrderForStatus(tsId, orderCancelled.orderID, orderStatusIndex.CanceledByECN, &orderDetail);

  if (indexOpenOrder == -2)
  {
    AddDataBlockToWaitList(TYPE_ARCA_DIRECT, WAIT_LIST_TYPE_ARCA_DIRECT_ORDER_CANCELED, orderCancelled.orderID, dataBlock, type, timeStamp);
    return 0;
  }

  if (indexOpenOrder != ERROR)
  {
    strcpy(orderCancelled.status, GetOrderStatus(asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status));
  }
  else
  {
    strcpy(orderCancelled.status, orderStatus.CanceledByECN);
  }

  orderCancelled.entryExitRefNum = *(unsigned short *)&dataBlock.addContent[DATABLOCK_OFFSET_ENTRY_EXIT_REF];

  // Call function to update Order Cancel of ARCA DIRECT
  ProcessInsertCanceledOrder4ARCA_DIRECTQueryToCollection(&orderCancelled);

  //Add a record to OJ file
  //int AddOJOrderCancelResponseRecord(char *time, char *symbol, int orderID, char *ecnOrderID, char *reason, int venueID, int sizeCanceled, int orderComplete, char *account)
  
  if (AddOrderIdToList(&__cancel, orderCancelled.orderID) == ERROR) //Duplicated
  {
    return 0;
  }
  
  char ecnOrderID[32];
  sprintf(ecnOrderID, "%lu", orderCancelled.arcaExOrderId);

  char reason[128];
  int reasonCode = dataBlock.msgContent[24];
  if (reasonCode == 0)
  {
    strcpy(reason, "user-initiated%20kill");
  }
  else if(reasonCode == 1)
  {
    strcpy(reason, "exchange-initiated%20for%20PNP%20crossed%20market");
  }
  else
  {
    sprintf(reason, "unknown%%20reason,%%20reason%%20code%%20%%3d%%20%d", reasonCode);
  }

  char account[32];
  GetAccountName(TYPE_ARCA_DIRECT, dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID], account);

  if (indexOpenOrder != ERROR)
  {
    AddOJOrderCancelResponseRecord(timeStamp, asOpenOrder.orderCollection[indexOpenOrder].orderSummary.symbol, orderCancelled.orderID, ecnOrderID, reason, VENUE_ARCA, asOpenOrder.orderCollection[indexOpenOrder].orderSummary.leftShares, 1, account);
  }
  else
  {
    AddOJOrderCancelResponseRecord(timeStamp, "", orderCancelled.orderID, ecnOrderID, reason, VENUE_ARCA, 0, 1, account);
  }

  return 0;
}

/****************************************************************************
- Function name:  ProcessCancelRequestOfARCA_DIRECTOrder
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessCancelRequestOfARCA_DIRECTOrder( t_DataBlock dataBlock, int type, char *timeStamp )
{
  /*
  - API version: ArcaDirect 4.1
  - Date updated: 21-Mar-2013
  - Modified by: Danh Hoang-Tang-Van
  - Order Cancel Message Equities (Variant 1), it includes the following fields:
    + Message Type (1 byte) = 'F'
    + Variant (1 byte) = 1
    + Length (2 bytes) = 72
    + Sequence Number (4 bytes) = Client-assigned sequence number
    + OrderID (8 bytes) = NYSE Arca assigned OrderID.
    + Client Order Id (4 bytes) = A client-assigned ID for this order
    + Strike Price (4 bytes) = null
    + Under Qty (2 bytes) = null
    + ExDestination (2 bytes) = 102
    + Corporate Action (1 byte) = null
    + Pull or Call (1 byte) = null
    + Bulk Cancel (1 byte) = null
    + Open or Close (1 byte) = null
    + Symbol (8 bytes) = Stock symbol
    + Strike Date (8 bytes) = null
    + Side (1 byte) = '1', '2', or '5'
    + Deliver To Comp ID (5 bytes) = null
    + Account (10 bytes) = null
    + Filler (7 byte) = null
    + Message Terminator (1 byte) = '\n'
    => Total byte = 72
  */

  t_ARCA_DIRECTCancelRequest cancelRequest;
  t_OrderDetail orderDetail;
  /*------------------------------------------------
  Order detail:
  - int length;
  - int execId;
  - int isBroken;
  - char timestamp[MAX_TIMESTAMP];
  - char msgContent[MAX_ORDER_MSG];
  ------------------------------------------------*/
  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;

  // YYYY/mm/DD
  char dateTime[32];
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]), &dateTime[0]);

  memcpy( cancelRequest.date, &dateTime[0], 10 );
  cancelRequest.date[10] = 0;

  // HH:MM:SS.ms
  memcpy( cancelRequest.timestamp, &dateTime[11], TIME_LENGTH );
  cancelRequest.timestamp[TIME_LENGTH] = 0;

  // orderDetail.timestamp
  memcpy(orderDetail.timestamp, &dateTime[0], TIMESTAMP_LENGTH);
  orderDetail.timestamp[TIMESTAMP_LENGTH] = 0;

  // Messsage type
  strcpy( cancelRequest.msgType, "CancelRequest" );

  int currentIndex = 0;

  // msgType
  char msgType = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Variant (1 byte)
  int variant = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Length
  cancelRequest.length = GetShortNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Sequence number
  cancelRequest.seqNum = GetIntNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  //Arca Order ID
  cancelRequest.arcaExOrderId = GetLongNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Order ID
  cancelRequest.orderID = GetIntNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Strike Price, 4 byte
  // Under Qty, 2 bytes
  // ExDestination, 2 bytes
  // Corporate Action, 1 byte
  // Put or Call, 1 byte
  // Bulk Cancel, 1 byte
  // Open or Close, 1 byte
  currentIndex += 12;

  //Symbol
  GetStringV1(cancelRequest.symbol, &dataBlock.msgContent[currentIndex], SYMBOL_LEN, &currentIndex);

  // Strike Date
  currentIndex += 8;

  //Side
  char action = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Deliver to Comp Id
  GetStringV1(cancelRequest.deliverCompID, &dataBlock.msgContent[currentIndex], 5, &currentIndex);
  cancelRequest.deliverCompID[5] = 0;
  
  // orderDetail.msgContent
  orderDetail.length = sprintf( orderDetail.msgContent,
          "%d=%c\001" // fieldIndexList.MsgType
          "%d=%d\001" // fieldIndexList.ArcaVariant
          "%d=%d\001" // fieldIndexList.Length
          "%d=%d\001" // fieldIndexList.OutSeqNum
          "%d=%d\001" // fieldIndexList.ClOrdID
          "%d=%ld\001"// fieldIndexList.ArcaOrderID
          "%d=%s\001" // fieldIndexList.DeliverToCompID
          "%d=%.8s\001"// fieldIndexList.Symbol
          "%d=%c\001",// fieldIndexList.Side
          fieldIndexList.MsgType, msgType,
          fieldIndexList.ArcaVariant, variant,
          fieldIndexList.Length, cancelRequest.length,
          fieldIndexList.OutSeqNum, cancelRequest.seqNum,
          fieldIndexList.ClOrdID, cancelRequest.orderID,
          fieldIndexList.ArcaOrderID, cancelRequest.arcaExOrderId,
          fieldIndexList.DeliverToCompID, cancelRequest.deliverCompID,
          fieldIndexList.Symbol, cancelRequest.symbol,
          fieldIndexList.Side, action);

  strcpy( cancelRequest.msgContent, orderDetail.msgContent );

  // Update database
  int tsId;

  if( type == MANUAL_ORDER )
  {
    // tsId
    tsId = MANUAL_ORDER;
  }
  else
  {
    // tsId
    tsId = tradeServersInfo.config[type].tsId;
  }

  // Call function to update a open order (non fill)
  int indexOpenOrder = ProcessUpdateASOpenOrderForStatus(tsId, cancelRequest.orderID, orderStatusIndex.CXLSent, &orderDetail );

  if (indexOpenOrder == -2)
  {
    AddDataBlockToWaitList(TYPE_ARCA_DIRECT, WAIT_LIST_TYPE_ARCA_DIRECT_CANCEL_REQUEST, cancelRequest.orderID, dataBlock, type, timeStamp);
    return 0;
  }

  if (indexOpenOrder != ERROR)
  {
    strcpy(cancelRequest.status, GetOrderStatus(asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status));
  }
  else
  {
    strcpy(cancelRequest.status, orderStatus.CXLSent);
  }

  // Call function to update cancel request of ARCA DIRECT
  ProcessInsertCancelRequest4ARCA_DIRECTQueryToCollection(&cancelRequest);

  //Add a new record to OJ file
  //  int AddOJOrderCancelRecord(char *time, int orderID, char *symbol, int venueID, int size)

  if (indexOpenOrder != ERROR)
  {
    AddOJOrderCancelRecord(timeStamp, cancelRequest.orderID, cancelRequest.symbol, VENUE_ARCA, asOpenOrder.orderCollection[indexOpenOrder].orderSummary.leftShares);
  }
  else
  {
    AddOJOrderCancelRecord(timeStamp, cancelRequest.orderID, cancelRequest.symbol, VENUE_ARCA, 0);
  }

  return 0;
}

/****************************************************************************
- Function name:  ProcessCancelACKOfARCA_DIRECTOrder
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessCancelACKOfARCA_DIRECTOrder( t_DataBlock dataBlock, int type )
{
  /*
  - API version: ArcaDirect 4.1
  - Date updated: 25-August-2009
  - Modified by: Danh Hoang-Tang-Van
  - Cancel Request Ack Message (Variant 1) , it includes the following fields:
    + Message Type    1   Alpha Numeric   �6�
    + Variant     1   Binary    Value = 1
    + Length      2   Binary    Binary length of the message. Value = 32.
    + Sequence Number 4   Binary    Exchange-assigned sequence number
    + Sending Time      8   Binary    UTC Time the message was sent in microseconds since Midnight
    + Transaction Time  8   Binary    UTC Time the message was sent in microseconds since Midnight
    + Client Order Id   4   Binary    Client order ID of the order to be canceled.
    + Order ID      8   Binary    Exchange assigned Order ID
    + Filler      7   Ignore
    + Message Terminator  1   Alpha     ASCII new line character: �\n�
    ==> Total byte    32
  */

  t_ARCA_DIRECTCancelACK cancelACK;
  t_OrderDetail orderDetail;
  /*------------------------------------------------
  Order detail:
  - int length;
  - int execId;
  - int isBroken;
  - char timestamp[MAX_TIMESTAMP];
  - char msgContent[MAX_ORDER_MSG];
  ------------------------------------------------*/
  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;

  // YYYY/mm/DD
  char dateTime[32];
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]), &dateTime[0]);
  memcpy( cancelACK.date, &dateTime[0], 10 );
  cancelACK.date[10] = 0;

  // HH:MM:SS.ms
  memcpy( cancelACK.timestamp, &dateTime[11], TIME_LENGTH );
  cancelACK.timestamp[TIME_LENGTH] = 0;

  // orderDetail.timestamp
  memcpy(orderDetail.timestamp, &dateTime[0], TIMESTAMP_LENGTH);
  orderDetail.timestamp[TIMESTAMP_LENGTH] = 0;

  // Status: Sent, Live, Rejected, Closed, BrokenTrade, CanceledByECN,
  // CXLSent, CancelACK, CancelRejected, CancelPending, CanceledByUser
  strcpy( cancelACK.status, orderStatus.CancelACK );

  // Messsage type
  strcpy(cancelACK.msgType, "CancelACK");

  int currentIndex = 0;

  // msgType
  char msgType = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Variant (1 byte)
  int variant = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Length
  cancelACK.length = GetShortNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Sequence number
  cancelACK.seqNum = GetIntNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Sending time
  cancelACK.sendingTime = GetLongNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  //Transaction time
  cancelACK.transTime = GetLongNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Order ID
  cancelACK.orderID = GetIntNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  //Arca Order ID
  cancelACK.arcaExOrderId = GetLongNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // orderDetail.msgContent
  orderDetail.length = sprintf( orderDetail.msgContent,
          "%d=%c\001" // fieldIndexList.MsgType
          "%d=%d\001" // fieldIndexList.ArcaVariant
          "%d=%d\001" // fieldIndexList.Length
          "%d=%d\001" // fieldIndexList.InSeqNum
          "%d=%ld\001" // fieldIndexList.SendingTime
          "%d=%ld\001" // fieldIndexList.TransactionTime
          "%d=%d\001" // fieldIndexList.ClOrdID
          "%d=%ld\001",// fieldIndexList.ArcaOrderID
          fieldIndexList.MsgType, msgType,
          fieldIndexList.ArcaVariant, variant,
          fieldIndexList.Length, cancelACK.length,
          fieldIndexList.InSeqNum, cancelACK.seqNum,
          fieldIndexList.SendingTime, cancelACK.sendingTime,
          fieldIndexList.TransactionTime, cancelACK.transTime,
          fieldIndexList.ClOrdID, cancelACK.orderID,
          fieldIndexList.ArcaOrderID, cancelACK.arcaExOrderId);

  strcpy(cancelACK.msgContent, orderDetail.msgContent);

  // Update database
  int tsId;

  if( type == MANUAL_ORDER )
  {
    // tsId
    tsId = MANUAL_ORDER;
  }
  else
  {
    // tsId
    tsId = tradeServersInfo.config[type].tsId;
  }

  // Call function to update a open order (non fill)
  if (ProcessUpdateASOpenOrderForStatus(tsId, cancelACK.orderID, orderStatusIndex.CancelACK, &orderDetail) == -2)
  {
    AddDataBlockToWaitList(TYPE_ARCA_DIRECT, WAIT_LIST_TYPE_ARCA_DIRECT_CANCEL_ACK, cancelACK.orderID, dataBlock, type, "");
    return 0;
  }

  // Call function to update Cancel ACK of ARCA DIRECT
  ProcessInsertCancelACK4ARCA_DIRECTQueryToCollection(&cancelACK);

  return 0;
}

/****************************************************************************
- Function name:  ProcessRejectOfARCA_DIRECTOrder
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessRejectOfARCA_DIRECTOrder( t_DataBlock dataBlock, int type, char *timeStamp )
{
  /*
  - API version: ArcaDirect 4.1
  - Date updated: 25-August-2009
  - Modified by: Danh Hoang-Tang-Van
  - Order Cancel/Replace Reject Message (Variant 1), it includes the following fields:
    + Message Type      1   Alpha Numeric   �8�
    + Variant       1   Binary    Value = 1
    + Length        2   Binary    Binary length of the message. Value = 64.
    + Sequence Number   4   Binary    Exchange-assigned sequence number
    + Sending Time          8   Binary    UTC Time the message was sent in microseconds since Midnight
    + Transaction Time    8   Binary    UTC Time the message was sent in microseconds since Midnight
    + Client Order Id     4   Binary    Client order id of the order, cancel, or cancel replace that was sent.
    + Original Client Order ID  4   Binary    ID of original order
    + Rejected Message Type   1   Numeric   �1�=order reject  �2�=cancel reject  �3�=cancel replace reject
    + Text        40    Alpha     Reason for the rejection
    + Reject Reason     1   Alpha     Fix Tag 102 or 103
    + Filler        1   Ignore
    + Message Terminator    1   Alpha     ASCII new line character: �\n�
    ==> Total byte      64
  */

  t_ARCA_DIRECTReject reject;
  t_OrderDetail orderDetail;

  /*------------------------------------------------
  Order detail:
  - int length;
  - int execId;
  - int isBroken;
  - char timestamp[MAX_TIMESTAMP];
  - char msgContent[MAX_ORDER_MSG];
  ------------------------------------------------*/
  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;

  // YYYY/mm/DD
  char dateTime[32];
    GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]), &dateTime[0]);
    memcpy( reject.processedTimestamp, &dateTime[11], TIME_LENGTH );
  reject.processedTimestamp[TIME_LENGTH] = 0;
    
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]), &dateTime[0]);
  memcpy( reject.date, &dateTime[0], 10 );
  reject.date[10] = 0;

  // HH:MM:SS.ms
  memcpy( reject.timestamp, &dateTime[11], TIME_LENGTH );
  reject.timestamp[TIME_LENGTH] = 0;

  // orderDetail.timestamp
  memcpy(orderDetail.timestamp, &dateTime[0], TIMESTAMP_LENGTH);
  orderDetail.timestamp[TIMESTAMP_LENGTH] = 0;

  int currentIndex = 0;

  // msgType
  char msgType = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Variant (1 byte)
  int variant = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Length
  reject.length = GetShortNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Sequence number
  reject.seqNum = GetIntNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Sending time
  reject.sendingTime = GetLongNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  //Transaction time
  reject.transTime = GetLongNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Order ID
  reject.orderID = GetIntNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Original Order ID
  reject.orgOrderID = GetIntNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Reject type
  reject.type = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  GetStringV1(reject.text, &dataBlock.msgContent[currentIndex], 40, &currentIndex);
  reject.text[40] = 0;
  
  // Messsage type
  if (reject.type == '1')
  {
    // Raise order rejected alert
    CheckNSendOrderRejectedAlertToAllTT(TYPE_ARCA_DIRECT, NULL, reject.text, reject.orderID); //ARCA DIRECT does not have reject code

    strcpy( reject.msgType, "RejectedOrder" );
  }
  else if( reject.type == '2' )
  {
    strcpy( reject.msgType, "RejectedCancel" );
  }
  else
  {
    strcpy( reject.msgType, "RejectedReplace" );
  }

  reject.reasonCode = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);
  // orderDetail.msgContent
  orderDetail.length = sprintf( orderDetail.msgContent,
          "%d=%c\001" // fieldIndexList.MsgType
          "%d=%d\001" // fieldIndexList.ArcaVariant
          "%d=%d\001" // fieldIndexList.Length
          "%d=%d\001" // fieldIndexList.InSeqNum
          "%d=%ld\001"// fieldIndexList.SendingTime
          "%d=%ld\001"// fieldIndexList.TransactionTime
          "%d=%d\001" // fieldIndexList.ClOrdID
          "%d=%d\001" // fieldIndexList.OriginalClientOrdID
          "%d=%s\001" // fieldIndexList.Type
          "%d=%s\001",// fieldIndexList.RejectText
          fieldIndexList.MsgType, msgType,
          fieldIndexList.ArcaVariant, variant,
          fieldIndexList.Length, reject.length,
          fieldIndexList.InSeqNum, reject.seqNum,
          fieldIndexList.SendingTime, reject.sendingTime,
          fieldIndexList.TransactionTime, reject.transTime,
          fieldIndexList.ClOrdID, reject.orderID,
          fieldIndexList.OriginalClientOrdID, reject.orgOrderID,
          fieldIndexList.Type, reject.msgType,
          fieldIndexList.RejectText, reject.text);

  strcpy( reject.msgContent, orderDetail.msgContent );

  // Update database
  int tsId;

  if( type == MANUAL_ORDER )
  {
    // tsId
    tsId = MANUAL_ORDER;
  }
  else
  {
    // tsId
    tsId = tradeServersInfo.config[type].tsId;
  }

  // Call function to update a open order (non fill)
  int indexOpenOrder = ProcessUpdateASOpenOrderForStatus(tsId, reject.orderID, orderStatusIndex.Rejected, &orderDetail );
  if (indexOpenOrder == -2)
  {
    AddDataBlockToWaitList(TYPE_ARCA_DIRECT, WAIT_LIST_TYPE_ARCA_DIRECT_ORDER_REJECTED, reject.orderID, dataBlock, type, timeStamp);
    return 0;
  }

  if (indexOpenOrder != ERROR)
  {
    if (reject.type == '1' && reject.orderID < 500000)
    {
      if ((strncmp((char *)&dataBlock.msgContent[33], "Price too far outside", 21) == 0) ||
        (strncmp((char *)&dataBlock.msgContent[33], "Mkt/Symbol not open", 19) == 0) ||
        (strncmp((char *)&dataBlock.msgContent[33], "Symbol closed", 13) == 0))
      {
        unsigned int haltTime = atoi(timeStamp);
        int stockSymbolIndex = GetStockSymbolIndex(asOpenOrder.orderCollection[indexOpenOrder].orderSummary.symbol);
        
        pthread_mutex_lock(&HaltStatusMgmt.mutexLock);
        strncpy(HaltStatusMgmt.symbol[stockSymbolIndex], asOpenOrder.orderCollection[indexOpenOrder].orderSummary.symbol, SYMBOL_LEN);
        
        HaltStatusMgmt.haltTime[stockSymbolIndex][TS_ARCA_BOOK_INDEX] = haltTime;
        HaltStatusMgmt.haltStatus[stockSymbolIndex][TS_ARCA_BOOK_INDEX] = TRADING_HALTED;

        SaveHaltedStockListToFile();
        SendAHaltStockToAllTS(stockSymbolIndex);
        SendAHaltStockToPM(stockSymbolIndex);
        SendAHaltStockToAllDaedalus(stockSymbolIndex);
        SendAHaltStockToAllTT(stockSymbolIndex);
        pthread_mutex_unlock(&HaltStatusMgmt.mutexLock);
      }
    }
      
    strcpy(reject.status, GetOrderStatus(asOpenOrder.orderCollection[indexOpenOrder].orderSummary.status));
  }
  else
  {
    strcpy(reject.status, orderStatus.CancelRejected);
  }

  reject.entryExitRefNum = *(unsigned short *)&dataBlock.addContent[DATABLOCK_OFFSET_ENTRY_EXIT_REF];

  // Call function to update order reject or cancel reject of ARCA DIRECT
  ProcessInsertReject4ARCA_DIRECTQueryToCollection(&reject);

  //Add a new record to OJ file
  //int AddOJOrderRejectRecord(char *time, int orderID, char *ecnOrderID, char *reason, char *symbol, int venueID, char *reasonCode, char *account)

  if (AddOrderIdToList(&__reject, reject.orderID) == ERROR) //Duplicated
  {
    return 0;
  }
  
  if (reject.type == '2') // Cancel rejected
  {
    return 0;
  }
  
  //When order is rejected (ARCA DIRECT), there is no ARCA Order ID, there is only Client Order ID
  char ecnOrderID[2];
  strcpy(ecnOrderID, "''"); //There is no Order Reference Number in this message type

  char reasonCode[2];
  reasonCode[0] = reject.reasonCode;
  reasonCode[1] = 0;

  //convert space to "%20"
  char reason[1024];
  int i, offset = 0;
  int len = strlen(reject.text);

  for (i=0; i<len; i++)
  {
    if (reject.text[i] == ' ')
    {
      reason[offset++] = '%';
      reason[offset++] = '2';
      reason[offset++] = '0';
    }
    else
    {
      reason[offset++] = reject.text[i];
    }
  }
  reason[offset] = 0;

  char account[32];
  GetAccountName(TYPE_ARCA_DIRECT, dataBlock.addContent[DATABLOCK_OFFSET_ACCOUNT_ID], account);
  
  if (indexOpenOrder != ERROR)
  {
    AddOJOrderRejectRecord(timeStamp, reject.orderID, ecnOrderID, reason, asOpenOrder.orderCollection[indexOpenOrder].orderSummary.symbol, VENUE_ARCA, reasonCode, account);
  }
  else
  {
    AddOJOrderRejectRecord(timeStamp, reject.orderID, ecnOrderID, reason, "", VENUE_ARCA, reasonCode, account);
  }
  return 0;
}

/****************************************************************************
- Function name:  ProcessBustOrCorrectOfARCA_DIRECTOrder
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessBustOrCorrectOfARCA_DIRECTOrder( t_DataBlock dataBlock, int type )
{
  /*
  - API version: ArcaDirect 4.1
  - Date updated: 25-August-2009
  - Modified by: Danh Hoang-Tang-Van
  - Bust or Correct Message (Variant 1) , it includes the following fields:
    + Message Type    1   Alpha Numeric   �C�
    + Variant     1   Binary    Value = 1
    + Length      2   Binary    Binary length of the message. Value = 40.
    + Sequence Number 4   Binary    Exchange-assigned sequence number
    + Sending Time      8   Binary    UTC Time the message was sent in microseconds since Midnight
    + Transaction Time  8   Binary    UTC Time the message was sent in microseconds since Midnight
    + Client Order Id   4   Binary    Client order ID of the order that is being busted or corrected.
    + Execution Id    8   Binary    Exchange assigned Execution ID from the order fill message (64 bit binary)
    + Order Quantity    4   Binary    Shares or contracts executed
    + Price     4   Binary    Corrected price in a correct message
    + Price Scale   1   Alpha/Numeric �0� through �4�
    + Type      1   Alpha/Numeric �1�=Bust  �2�=Correct
    + Filler      5   Ignore
    + Message Terminator  1   Alpha   ASCII new line character: �\n�
    ==>Total byte   40
  */

  t_ARCA_DIRECTBustOrCorrect brokenTrade;
  t_OrderDetail orderDetail;
  /*------------------------------------------------
  Order detail:
  - int length;
  - int execId;
  - int isBroken;
  - char timestamp[MAX_TIMESTAMP];
  - char msgContent[MAX_ORDER_MSG];
  ------------------------------------------------*/
  // Initialize ...
  orderDetail.length = 0;
  orderDetail.execId = -1;
  orderDetail.isBroken = BROKEN_TRADE_NONE;

  // YYYY/mm/DD
  char dateTime[32];
  GetDateTimeFromEpochNsec(*((long*)&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME]), &dateTime[0]);
  memcpy( brokenTrade.date, &dateTime[0], 10 );
  brokenTrade.date[10] = 0;

  // HH:MM:SS.ms
  memcpy( brokenTrade.timestamp, &dateTime[11], TIME_LENGTH );
  brokenTrade.timestamp[TIME_LENGTH] = 0;

  // orderDetail.timestamp
  memcpy( orderDetail.timestamp, &dateTime[0], TIMESTAMP_LENGTH );
  orderDetail.timestamp[TIMESTAMP_LENGTH] = 0;

  // Status: Sent, Live, Rejected, Closed, BrokenTrade, CanceledByECN,
  // CXLSent, CancelACK, CancelRejected, CancelPending, CanceledByUser
  strcpy( brokenTrade.status, orderStatus.BrokenTrade );

  // Messsage type
  strcpy( brokenTrade.msgType, "BrokenTrade" );

  int currentIndex = 0;

  // msgType
  char msgType = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Variant (1 byte)
  int variant = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Length
  brokenTrade.length = GetShortNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Sequence number
  brokenTrade.seqNum = GetIntNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Sending time
  brokenTrade.sendingTime = GetLongNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  //Transaction time
  brokenTrade.transTime = GetLongNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Order ID
  brokenTrade.orderID = GetIntNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Execution ID
  brokenTrade.executionId = GetLongNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Shares
  brokenTrade.shares = GetIntNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // Price
  int fieldPrice;
  fieldPrice = GetIntNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  char fieldPriceScale = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);
  brokenTrade.price = fieldPrice * 1.0 / pow( 10, fieldPriceScale - 48);

  // Type
  brokenTrade.type = GetByteNumberBigEndianV1(&dataBlock.msgContent[currentIndex], &currentIndex);

  // orderDetail.msgContent
  orderDetail.length = sprintf( orderDetail.msgContent,
          "%d=%c\001" // fieldIndexList.MsgType
          "%d=%d\001" // fieldIndexList.ArcaVariant
          "%d=%d\001" // fieldIndexList.Length
          "%d=%d\001" // fieldIndexList.InSeqNum
          "%d=%ld\001"// fieldIndexList.TransactionTime
          "%d=%d\001" // fieldIndexList.ClOrdID
          "%d=%ld\001"// fieldIndexList.ExecID
          "%d=%d\001" // fieldIndexList.OrdQty
          "%d=%d\001" // fieldIndexList.PriceField
          "%d=%c\001" // fieldIndexList.PriceScale
          "%d=%c\001",// fieldIndexList.Type
          fieldIndexList.MsgType, msgType,
          fieldIndexList.ArcaVariant, variant,
          fieldIndexList.Length, brokenTrade.length,
          fieldIndexList.InSeqNum, brokenTrade.seqNum,
          fieldIndexList.TransactionTime, brokenTrade.transTime,
          fieldIndexList.ClOrdID, brokenTrade.orderID,
          fieldIndexList.ExecID, brokenTrade.executionId,
          fieldIndexList.OrdQty, brokenTrade.shares,
          fieldIndexList.PriceField, fieldPrice,
          fieldIndexList.PriceScale, fieldPriceScale,
          fieldIndexList.Type, brokenTrade.type );

  strcpy( brokenTrade.msgContent, orderDetail.msgContent );

  // Update database
  int tsId;

  if( type == MANUAL_ORDER )
  {
    // tsId
    tsId = MANUAL_ORDER;
  }
  else
  {
    // tsId
    tsId = tradeServersInfo.config[type].tsId;
  }

  // Call function to update a open order (non fill)
  if (ProcessUpdateASOpenOrderForStatus(tsId, brokenTrade.orderID, orderStatusIndex.BrokenTrade, &orderDetail) == -2)
  {
    AddDataBlockToWaitList(TYPE_ARCA_DIRECT, WAIT_LIST_TYPE_ARCA_DIRECT_BUST_OR_CORRECT, brokenTrade.orderID, dataBlock, type, "");
    return 0;
  }

  // Call function to update Order Fill of ARCA DIRECT
  ProcessInsertBustOrCorrect4ARCA_DIRECTQueryToCollection(&brokenTrade);

  //Process add broken trade message to as logs
  int indexOpenOrder = CheckOrderIdIndex(brokenTrade.orderID, tsId);
  if (indexOpenOrder >= 0)
  {
    TraceLog(DEBUG_LEVEL, "Broken Trade message received from ARCA DIRECT for symbol %.8s.\n", asOpenOrder.orderCollection[indexOpenOrder].orderSummary.symbol);
  }

  return 0;
}


/***************************************************************************/
