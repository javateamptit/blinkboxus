This text provides information related to the database scripts "upgrade_20121122.sql" and "upgrade_twt2_database_20121122.sql" placed at the same directory.

When run script "upgrade_20121122" on target database (eaa10_201211), it will:
- update tables: add column time_stamp to brokens table, add column total_fee to risk_managements talbe
- update two functions: update_rm, process_fills_insert
- create one function: calculate_total_fee
- create one view: view_liquidity
- update three triggers on tables: str_update_rm_for_broken on brokens, str_update_rm on fills, str_update_rm2 on cancel_pendings

When run script "upgrade_twt2_database_20121122" on target database (eaa10_twt2_201211), it will:
- Add one table: summary_net_profit_losses

*The update is needed to run with new release.

How to run script:
1) To upgrade, please copy two files upgrade_20121120.sql and upgrade_twt2_database_20121122.sql to the EAA database host and run the command (please notice that we must run this command in the same folder with those files):
	psql -U postgres -h localhost -d eaa10_201211 -f upgrade_20121122.sql
	psql -U postgres -h localhost -d eaa10_twt2_201211 -f upgrade_twt2_database_20121122.sql