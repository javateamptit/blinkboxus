BEGIN;
-- Table: summary_net_profit_losses

-- DROP TABLE summary_net_profit_losses;

CREATE TABLE summary_net_profit_losses
(
  id serial NOT NULL,
  date date NOT NULL,
  time_stamp time without time zone NOT NULL,
  total_second integer,
  net_profit_loss double precision,
  volume integer,
  market_value double precision,
  CONSTRAINT summary_net_profit_losses_pkey PRIMARY KEY (id )
)
WITH (
  OIDS=FALSE
);
ALTER TABLE summary_net_profit_losses
  OWNER TO postgres;

-- Index: summary_net_profit_losses_all

-- DROP INDEX summary_net_profit_losses_all;

CREATE INDEX summary_net_profit_losses_all
  ON summary_net_profit_losses
  USING btree
  (date , time_stamp );

-- Index: summary_net_profit_losses_date

-- DROP INDEX summary_net_profit_losses_date;

CREATE INDEX summary_net_profit_losses_date
  ON summary_net_profit_losses
  USING btree
  (date );

-- Index: summary_net_profit_losses_time_stamp

-- DROP INDEX summary_net_profit_losses_time_stamp;

CREATE INDEX summary_net_profit_losses_time_stamp
  ON summary_net_profit_losses
  USING btree
  (time_stamp );

COMMIT;