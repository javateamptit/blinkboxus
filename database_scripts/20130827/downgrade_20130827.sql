BEGIN;

-- Downgrade
DELETE FROM release_versions WHERE module_id = 2 AND version = '20130827';

DROP FUNCTION IF EXISTS update_time_stamp_of_raw_cross();

COMMIT;