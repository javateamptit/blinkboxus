BEGIN;

-- Upgrade
INSERT INTO release_versions(module_id, version) VALUES (2, '20130827');

UPDATE pg_attribute SET atttypmod = 80+4
WHERE attrelid = 'raw_crosses'::regclass
AND attname = 'raw_content';

--
-- Name: vw_entry_delays; Type: VIEW; Schema: public; Owner: postgres
--
DROP VIEW IF EXISTS vw_entry_delays;
CREATE VIEW vw_entry_delays AS
    SELECT DISTINCT new_orders.oid, new_orders.order_id, new_orders.time_stamp, max(split_part((raw_crosses.raw_content)::text, ' '::text, 10)) AS quote_time_stamp, new_orders.date, new_orders.ecn, new_orders.ecn_suffix FROM (new_orders JOIN raw_crosses ON ((((raw_crosses.date = new_orders.date) AND (raw_crosses.ts_id = new_orders.ts_id)) AND (raw_crosses.ts_cid = new_orders.cross_id)))) WHERE (((((raw_crosses.trade_id > 0) AND (raw_crosses.raw_content ~~ '2%'::text)) AND (new_orders.order_id <> 0)) AND (new_orders.ts_id <> (-1))) AND (new_orders.entry_exit = 'Entry'::bpchar)) GROUP BY new_orders.oid, new_orders.order_id, new_orders.date, new_orders.time_stamp, new_orders.ecn, new_orders.ecn_suffix ORDER BY new_orders.oid, new_orders.time_stamp, max(split_part((raw_crosses.raw_content)::text, ' '::text, 10)), new_orders.date;


ALTER TABLE public.vw_entry_delays OWNER TO postgres;

--
-- Name: vw_exit_delays; Type: VIEW; Schema: public; Owner: postgres
--
DROP VIEW IF EXISTS vw_exit_delays;
CREATE VIEW vw_exit_delays AS
    SELECT exit.id, exit.date, cause.order_id, date_part('microseconds'::text, (exit.time_stamp - cause.time_stamp)) AS delay, cause.ecn, cause.ecn_suffix FROM (((SELECT new_orders.date, new_orders.order_id, new_orders.ts_id, new_orders.cross_id, order_rejects.entry_exit_ref, order_rejects.time_stamp, new_orders.ecn, new_orders.ecn_suffix FROM (new_orders JOIN order_rejects ON ((new_orders.oid = order_rejects.oid))) UNION SELECT new_orders.date, new_orders.order_id, new_orders.ts_id, new_orders.cross_id, cancels.entry_exit_ref, cancels.time_stamp, new_orders.ecn, new_orders.ecn_suffix FROM (new_orders JOIN cancels ON ((new_orders.oid = cancels.oid)))) UNION SELECT new_orders.date, new_orders.order_id, new_orders.ts_id, new_orders.cross_id, fills.entry_exit_ref, fills.time_stamp, new_orders.ecn, new_orders.ecn_suffix FROM (new_orders JOIN fills ON ((new_orders.oid = fills.oid)))) cause JOIN (SELECT new_orders.date, new_orders.ts_id, new_orders.cross_id, new_orders.entry_exit_ref, min(new_orders.time_stamp) AS time_stamp, min(new_orders.id) AS id FROM new_orders WHERE ((new_orders.ts_id <> (-1)) AND (new_orders.entry_exit = 'Exit'::bpchar)) GROUP BY new_orders.ts_id, new_orders.cross_id, new_orders.entry_exit_ref, new_orders.date) exit ON (((((cause.date = exit.date) AND (cause.ts_id = exit.ts_id)) AND (cause.cross_id = exit.cross_id)) AND (cause.entry_exit_ref = exit.entry_exit_ref)))) ORDER BY exit.id;


ALTER TABLE public.vw_exit_delays OWNER TO postgres;

-- Function: update_time_stamp_of_raw_cross()

CREATE OR REPLACE FUNCTION update_time_stamp_of_raw_cross()
  RETURNS integer AS
$$ 
DECLARE
	recordTmp record;
	retVal integer;
BEGIN  
	retVal := 0;
	
	FOR recordTmp IN (SELECT date, ts_id, ts_cid, max(split_part(raw_crosses.raw_content::text, ' '::text, 8)) AS quote_time_stamp
					FROM raw_crosses WHERE raw_crosses.raw_content ~~ '3%'::text 
					GROUP BY date, ts_id, ts_cid ORDER BY date, ts_id, ts_cid) LOOP
		retVal = retVal + 1;
		
		UPDATE raw_crosses
			SET raw_content = trim(raw_content::text) || ' ' || recordTmp.quote_time_stamp
		WHERE date=recordTmp.date AND ts_id=recordTmp.ts_id AND ts_cid=recordTmp.ts_cid AND raw_crosses.raw_content ~~ '2%'::text;
	END LOOP;

	RETURN retVal;
END
$$  LANGUAGE plpgsql;
  
ALTER FUNCTION update_time_stamp_of_raw_cross() OWNER TO postgres;

COMMIT;

SELECT update_time_stamp_of_raw_cross();

-- -------------------------------------------------------
-- END
-- -------------------------------------------------------


