BEGIN;

-- Constraint: cancels_pkey

ALTER TABLE cancels DROP CONSTRAINT cancels_pkey;

COMMIT;
