BEGIN;
-- Update order_acks and fills to add new timestamp and init data for new column
    ALTER TABLE order_acks ADD COLUMN processed_time_stamp time without time zone;
	UPDATE order_acks set processed_time_stamp=time_stamp;
	
	ALTER TABLE fills ADD COLUMN processed_time_stamp time without time zone;
	UPDATE fills set processed_time_stamp=time_stamp;

-- insert_fill_hung_order
CREATE OR REPLACE FUNCTION insert_fill_hung_order(oid_t integer, list_shares text, list_price text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE

	shares_array integer[] = string_to_array(list_shares, '_');
	price_array double precision[] = string_to_array(list_price, '_');
	tempOrder record;
	tempFill record;
	i integer;
	liquidity character;
	msg_content text;
	id_current integer;
	exce_id_tmp text;
	total_fill_shares integer;
BEGIN
 
	i = 1;
	total_fill_shares = 0;
	select * into tempOrder from new_orders where oid = oid_t;
	if tempOrder.ecn = 'ARCA' then
		liquidity = 'R';
	elsif tempOrder.ecn = 'OUCH' then
		liquidity = 'R';
	elsif tempOrder.ecn = 'RASH' then
		liquidity = 'R';
	elsif tempOrder.ecn = 'NYSE' then
		liquidity = '1';
	elsif tempOrder.ecn = 'BATZ' then
		liquidity = 'R';
	end if;
	select max(id) into id_current from fills; 
	IF id_current IS NULL THEN
		id_current = 1;
	END IF;
	exce_id_tmp = '';
	WHILE shares_array[i] IS NOT NULL LOOP
		msg_content = '';
		total_fill_shares = total_fill_shares + shares_array[i];
		if tempOrder.ecn = 'ARCA' AND tempOrder.ecn_suffix IS NULL then
			msg_content = msg_content || '-50=2';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-46=' || tempOrder.order_id;
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-44=' || shares_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-43=' || (price_array[i]*100);
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-42=' || '2';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-29=' || '99999';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-28=' || 'R';
		ELSIF tempOrder.ecn = 'ARCA' AND tempOrder.ecn_suffix = 'FIX' then
			msg_content = msg_content || '35=8';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '11=' || tempOrder.order_id;
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '38=' || shares_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '44=' || price_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '17=' || '99999';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '9730=' || 'R';
		ELSIF tempOrder.ecn = 'OUCH' THEN
			msg_content = msg_content || '-13=E';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-12=' || tempOrder.order_id;
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-10=' || shares_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-8=' || price_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-2=' || 'R';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-29=' || '99999';
			msg_content = msg_content || E'\001';
		ELSIF tempOrder.ecn = 'RASH' THEN
			msg_content = msg_content || '-13=E';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-12=' || tempOrder.order_id;
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-10=' || shares_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-8=' || price_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-2=' || 'R';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-29=' || '99999';
			msg_content = msg_content || E'\001';
		ELSIF tempOrder.ecn = 'NYSE' THEN
			msg_content = msg_content || '-50=81';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-10=' || shares_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-43=' || (price_array[i]*100);
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-42=' || '2';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-29=' || '99999';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-28=' || '1';
		ELSIF tempOrder.ecn = 'BATZ' THEN
			msg_content = msg_content || '-50=11';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-81=' || shares_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-83=' || price_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-28=' || 'R';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-29=' || '99999';
			msg_content = msg_content || E'\001';
			
		END IF;
		
		msg_content = msg_content || E'\001';
		id_current = id_current + 1;
		exce_id_tmp = '99999' || '_' || id_current;
		
		INSERT INTO fills(oid, shares, price, time_stamp, processed_time_stamp, msgseq_num, origclord_id, exec_id, msg_content, liquidity,  manual_update)
			VALUES(oid_t, shares_array[i], price_array[i], tempOrder.time_stamp, tempOrder.time_stamp, tempOrder.msgseq_num, tempOrder.order_id, exce_id_tmp, msg_content, liquidity, 1 );
		i = i + 1;
	END LOOP;
	IF total_fill_shares < tempOrder.shares THEN
		UPDATE new_orders SET status = 'Lived' where oid = oid_t;
		UPDATE new_orders SET left_shares = (tempOrder.shares - total_fill_shares) where oid = oid_t;
	ELSE
		UPDATE new_orders SET status = 'Closed' where oid = oid_t;
		UPDATE new_orders SET left_shares = 0 where oid = oid_t;
	END IF;
	UPDATE trading_status SET hung_order = 1;
	
	return 1;
END
$$;

-- insertexecutedorder4directedge
DROP FUNCTION IF EXISTS insertexecutedorder4directedge(integer, date, integer, double precision, time without time zone, text, text, text, text, text, integer, double precision, integer);

CREATE OR REPLACE FUNCTION insertexecutedorder4directedge(_orderid integer, _date date, _shares integer, _price double precision, _timestamp time without time zone, _processedTimestamp time without time zone, _seqnum text, _execid text, _msgcontents text, _liquidityindicator text, _status text, _leftshares integer, _avgprice double precision, _entryexitrefnum integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	orderID integer;
BEGIN
	
	SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
	
	IF orderID IS NULL THEN
		RETURN -1;
	END IF;
	
	INSERT INTO fills (oid, shares, price, time_stamp, processed_time_stamp, msgseq_num, origclord_id, exec_id, msg_content, liquidity, entry_exit_ref) VALUES (orderID, _shares, _price, _timeStamp, _processedTimestamp, _seqNum, _orderID, _execID, _msgContents, _liquidityIndicator, _entryExitRefNum);

	UPDATE new_orders SET status = _status, left_shares = _leftShares, avg_price = _avgPrice WHERE oid = orderID;
	UPDATE new_orders SET price = _price WHERE oid = orderID AND price = 0;
	
	RETURN 0;
END
$$;

-- insertexecutedorder4variant1
-- insertexecutedorder4variant1
DROP FUNCTION IF EXISTS insertexecutedorder4variant1(integer, date, integer, double precision, time without time zone, text, text, text, character, text, integer, double precision);
DROP FUNCTION IF EXISTS insertexecutedorder4variant1(integer, date, integer, double precision, time without time zone, text, text, text, character, text, integer, double precision, integer);

CREATE OR REPLACE FUNCTION insertexecutedorder4variant1(_orderid integer, _date date, _shares integer, _price double precision, _timestamp time without time zone, _processedTimestamp time without time zone, _seqnum text, _execid text, _msgcontents text, _liquidityindicator character, _status text, _leftshares integer, _avgprice double precision, _entryexitrefnum integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	orderID integer;
BEGIN
	
	SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
	
	IF orderID IS NULL THEN
		RETURN -1;
	END IF;
	
	INSERT INTO fills (oid, shares, price, time_stamp, processed_time_stamp, msgseq_num, origclord_id, exec_id, msg_content, liquidity, entry_exit_ref) VALUES (orderID, _shares, _price, _timeStamp, _processedTimestamp, _seqNum, _orderID, _execID, _msgContents, _liquidityIndicator, _entryExitRefNum);

	UPDATE new_orders SET status = _status, left_shares = _leftShares, avg_price = _avgPrice WHERE oid = orderID;
	
	RETURN 0;
END
$$;

-- insertexecutedorder4variant2
-- insertexecutedorder4variant2
DROP FUNCTION IF EXISTS insertexecutedorder4variant2(integer, date, integer, double precision, time without time zone, text, text, text, character, text, integer, double precision);
DROP FUNCTION IF EXISTS insertexecutedorder4variant2(integer, date, integer, double precision, time without time zone, text, text, text, character, text, integer, double precision, integer);

CREATE OR REPLACE FUNCTION insertexecutedorder4variant2(_orderid integer, _date date, _shares integer, _price double precision, _timestamp time without time zone, _processedTimestamp time without time zone, _seqnum text, _execid text, _msgcontents text, _liquidityindicator character, _status text, _leftshares integer, _avgprice double precision, _entryexitrefnum integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	orderID integer;
BEGIN
	
	SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
	
	IF orderID IS NULL THEN
		RETURN -1;
	END IF;
	
	INSERT INTO fills (oid, shares, price, time_stamp, processed_time_stamp, msgseq_num, origclord_id, exec_id, msg_content, liquidity, entry_exit_ref) VALUES (orderID, _shares, _price, _timeStamp, _processedTimestamp, _seqNum, _orderID, _execID, _msgContents, _liquidityIndicator, _entryExitRefNum);

	UPDATE new_orders SET status = _status, left_shares = _leftShares, avg_price = _avgPrice WHERE oid = orderID;
	UPDATE new_orders SET price = _price WHERE oid = orderID AND price = 0;
	
	RETURN 0;
END
$$;

-- insertexecutedorder4variant3
-- insertexecutedorder4variant3
DROP FUNCTION IF EXISTS insertexecutedorder4variant3(integer, date, integer, double precision, time without time zone, text, text, text, text, character, text, integer, double precision);
DROP FUNCTION IF EXISTS insertexecutedorder4variant3(integer, date, integer, double precision, time without time zone, text, text, text, text, character, text, integer, double precision, integer);
CREATE OR REPLACE FUNCTION insertexecutedorder4variant3(_orderid integer, _date date, _shares integer, _price double precision, _timestamp time without time zone, _processedTimestamp time without time zone, _seqnum text, _arcaexorderid text, _execid text, _msgcontents text, _liquidityindicator character, _status text, _leftshares integer, _avgprice double precision, _entryexitrefnum integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	orderID integer;
BEGIN
	
	SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
	
	IF orderID IS NULL THEN
		RETURN -1;
	END IF;
	
	INSERT INTO fills (oid, shares, price, time_stamp, processed_time_stamp, msgseq_num, origclord_id, arcaex_order_id, exec_id, msg_content, liquidity, entry_exit_ref) VALUES (orderID, _shares, _price, _timeStamp, _processedTimestamp, _seqNum, _orderID, _arcaExOrderID, _execID, _msgContents, _liquidityIndicator, _entryExitRefNum);

	UPDATE new_orders SET status = _status, left_shares = _leftShares, avg_price = _avgPrice WHERE oid = orderID;
	
	RETURN 0;
END
$$;

-- insertfillorder4nyse_ccg
-- insertfillorder4nyse_ccg
DROP FUNCTION IF EXISTS insertfillorder4nyse_ccg(integer, date, integer, double precision, time without time zone, text, text, text, text, character, text, integer, double precision);
DROP FUNCTION IF EXISTS insertfillorder4nyse_ccg(integer, date, integer, double precision, time without time zone, text, text, text, text, character, text, integer, double precision, integer);
CREATE OR REPLACE FUNCTION insertfillorder4nyse_ccg(_orderid integer, _date date, _shares integer, _price double precision, _timestamp time without time zone, _processedTimestamp time without time zone, _seqnum text, _meorderid text, _execid text, _msgcontents text, _billingindicator character, _status text, _leftshares integer, _avgprice double precision, _entryexitrefnum integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	orderID integer;
BEGIN
	
	SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
	
	IF orderID IS NULL THEN
		RETURN -1;
	END IF;
	
	INSERT INTO fills (oid, shares, price, time_stamp, processed_time_stamp, msgseq_num, origclord_id, arcaex_order_id, exec_id, msg_content, liquidity, entry_exit_ref) VALUES (orderID, _shares, _price, _timestamp, _processedTimestamp, _seqnum, _orderID, _meorderid, _execid, _msgcontents, _billingindicator, _entryExitRefNum);

	UPDATE new_orders SET status = _status, left_shares = _leftshares, avg_price = _avgprice WHERE oid = orderID;
	
	RETURN 0;
END$$;

-- insertorderack4nyse_ccg
DROP FUNCTION IF EXISTS insertorderack4nyse_ccg(integer, date, time without time zone, text, text, text, double precision, text);
CREATE OR REPLACE FUNCTION insertorderack4nyse_ccg(_orderid integer, _date date, _timestamp time without time zone, _processedTimestamp time without time zone, _seqnum text, _meorderid text, _msgcontents text, _price double precision, _status text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE 
	orderID integer;
BEGIN
	SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;

	IF orderID IS NULL THEN
		RETURN -1;
	END IF;

	INSERT INTO order_acks (oid, time_stamp, processed_time_stamp, msgseq_num, origclord_id, arcaex_order_id, msg_content, price) VALUES (orderID, _timeStamp, _processedTimestamp, _seqNum, _orderID, _meorderid, _msgcontents, _price);
  
	UPDATE new_orders SET status = _status WHERE oid = orderID;

	RETURN 0;
END$$;

-- insertorderack4arca_direct
DROP FUNCTION IF EXISTS insertorderack4arca_direct(integer, date, time without time zone, text, text, text, double precision, text);
CREATE FUNCTION insertorderack4arca_direct(_orderid integer, _date date, _timestamp time without time zone, _processedTimestamp time without time zone, _seqnum text, _arcaexorderid text, _msgcontents text, _price double precision, _status text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	orderID integer;
BEGIN
	
	SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
	
	IF orderID IS NULL THEN
		RETURN -1;
	END IF;
	
	INSERT INTO order_acks (oid, time_stamp, processed_time_stamp, msgseq_num, origclord_id, arcaex_order_id, msg_content, price) VALUES (orderID, _timeStamp, _processedTimestamp, _seqNum, _orderID, _arcaExOrderId, _msgContents, _price);
	  
	UPDATE new_orders SET status = _status WHERE oid = orderID;
	
	RETURN 0;
END
$$;

-- insertacceptedorder4nasdaq
DROP FUNCTION IF EXISTS insertacceptedorder4nasdaq(integer, date, time without time zone, text, text, text, double precision, text, integer);
CREATE OR REPLACE FUNCTION insertacceptedorder4nasdaq(_orderid integer, _date date, _timestamp time without time zone, _processedTimestamp time without time zone, _seqnum text, _arcaexorderid text, _msgcontents text, _price double precision, _status text, _shares integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	orderID integer;
BEGIN
	
	SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
	
	IF orderID IS NULL THEN
		RETURN -1;
	END IF;
	
	INSERT INTO order_acks (oid, time_stamp, processed_time_stamp, msgseq_num, origclord_id, arcaex_order_id, msg_content, price) VALUES (orderID, _timeStamp, _processedTimestamp, _seqNum, _orderID, _arcaExOrderId, _msgContents, _price);
	
	UPDATE new_orders SET status = _status, shares = _shares WHERE oid = orderID;
	
	RETURN 0;
END
$$;

-- insertacceptedorder4arca_bats
DROP FUNCTION IF EXISTS insertacceptedorder4arca_bats(integer, date, time without time zone, text, text, text, double precision, text, integer);
CREATE OR REPLACE FUNCTION insertacceptedorder4arca_bats(_orderid integer, _date date, _timestamp time without time zone, _processedTimestamp time without time zone, _seqnum text, _arcaexorderid text, _msgcontents text, _price double precision, _status text, _shares integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	orderID integer;
BEGIN
	
	SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
	
	IF orderID IS NULL THEN
		RETURN -1;
	END IF;
	
	INSERT INTO order_acks (oid, time_stamp, processed_time_stamp, msgseq_num, origclord_id, arcaex_order_id, msg_content, price) VALUES (orderID, _timeStamp, _processedTimestamp, _seqNum, _orderID, _arcaExOrderId, _msgContents, _price);
	
	UPDATE new_orders SET status = _status, left_shares = _shares, avg_price =  _price WHERE oid = orderID;
	
	RETURN 0;
END
$$;

DROP VIEW IF EXISTS vw_fill_delays;
DROP VIEW IF EXISTS vw_ack_delays;
DROP VIEW IF EXISTS vw_trade_view;
CREATE OR REPLACE VIEW vw_fill_delays AS
    SELECT f.fill_id, n.entry_exit, date_part('microseconds'::text, (f.time_stamp - a.time_stamp)) AS fill_delay, date_part('microseconds'::text, (f.processed_time_stamp - f.time_stamp)) AS fill_internal_delay, n.date, n.ecn, n.ecn_suffix, n.order_id, f.time_stamp, btrim((n.routing_inst)::text) AS routing_inst FROM ((new_orders n JOIN order_acks a ON ((a.oid = n.oid))) JOIN fills f ON ((f.oid = n.oid))) WHERE ((n.ts_id <> (-1)) AND (n.order_id <> 0)) ORDER BY f.fill_id, n.date;
ALTER TABLE public.vw_fill_delays OWNER TO postgres;
   
CREATE OR REPLACE VIEW vw_ack_delays AS
    SELECT n.oid, n.entry_exit, n.date, date_part('microseconds'::text, (a.time_stamp - n.time_stamp)) AS ack_delay, date_part('microseconds'::text, (a.processed_time_stamp - a.time_stamp)) AS ack_internal_delay, n.ecn, n.ecn_suffix, n.order_id, n.time_stamp, btrim((n.routing_inst)::text) AS routing_inst FROM (new_orders n JOIN order_acks a ON ((a.oid = n.oid))) WHERE ((n.ts_id <> (-1)) AND (n.order_id <> 0)) ORDER BY n.oid, n.date;
ALTER TABLE public.vw_ack_delays OWNER TO postgres;

CREATE OR REPLACE VIEW vw_trade_view AS
    (SELECT DISTINCT tr.date, tr.as_cid, tr.ts_cid, tr.ts_id, tr.symbol, tr.bought_shares, tr.sold_shares, tr.total_bought, tr.total_sold, tr.trade_id, 0 AS trade_type, ('0 '::text || bi.content) AS raw_content, bi.bbo_id, bi.id, tr."timestamp"::text FROM (trade_results tr LEFT JOIN bbo_infos bi ON ((((bi.date = tr.date) AND (bi.cross_id = tr.ts_cid)) AND (bi.ts_id = tr.ts_id)))) ORDER BY bi.id, tr.date, tr.as_cid, tr.ts_cid, tr.ts_id, tr.symbol, tr.bought_shares, tr.sold_shares, tr.total_bought, tr.total_sold, tr.trade_id, 0::integer, ('0 '::text || bi.content), bi.bbo_id, tr."timestamp"::text) UNION (SELECT DISTINCT rc.date, rc.as_cid, rc.ts_cid, rc.ts_id, rc.symbol, tr.bought_shares, tr.sold_shares, tr.total_bought, tr.total_sold, tr.trade_id, rc.trade_type, rc.raw_content, (-1) AS bbo_id, (rc.id + 100000000) AS id, split_part((rc.raw_content)::text, ' '::text, 3) AS "timestamp" FROM (raw_crosses rc LEFT JOIN trade_results tr ON (((((rc.date = tr.date) AND (rc.as_cid = tr.as_cid)) AND (rc.ts_cid = tr.ts_cid)) AND (rc.ts_id = tr.ts_id)))) ORDER BY (rc.id + 100000000), rc.date, rc.as_cid, rc.ts_cid, rc.ts_id, rc.symbol, tr.bought_shares, tr.sold_shares, tr.total_bought, tr.total_sold, tr.trade_id, rc.trade_type, rc.raw_content, (-1)::integer, timestamp) ORDER BY 2, 14;
ALTER TABLE public.vw_trade_view OWNER TO postgres;

-- Insert new TT version to database
INSERT INTO release_versions(module_id, version) VALUES (2, '20140305');

COMMIT;
