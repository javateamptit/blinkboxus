BEGIN;

DROP TABLE IF EXISTS summary_profit_losses_15;

DROP TABLE IF EXISTS summary_profit_losses_30;

DROP TABLE IF EXISTS summary_profit_losses_60;

ALTER TABLE summary_profit_losses ADD COLUMN net_profit_loss double precision DEFAULT 0.00;
ALTER TABLE summary_profit_losses ADD COLUMN expected_profit double precision DEFAULT 0.00;

CREATE FUNCTION update_net_profit_loss() RETURNS integer
    LANGUAGE plpgsql
    AS $$ 
DECLARE
	recordTmp record;
	retVal integer;
BEGIN  
	retVal := 0;
	
	FOR recordTmp IN (SELECT date, time_stamp, total_second, net_profit_loss FROM summary_net_profit_losses) LOOP
		retVal = retVal + 1;
		
		UPDATE summary_profit_losses
			SET net_profit_loss = recordTmp.net_profit_loss
		WHERE date=recordTmp.date AND time_stamp=recordTmp.time_stamp AND total_second=recordTmp.total_second;
	END LOOP;

	RETURN retVal;
END
$$;

ALTER FUNCTION public.update_net_profit_loss() OWNER TO postgres;

SELECT update_net_profit_loss();

DROP FUNCTION IF EXISTS update_net_profit_loss();

DROP TABLE IF EXISTS summary_net_profit_losses;

COMMIT;
