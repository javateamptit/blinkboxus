BEGIN;
-- Function: get_mutiple_exchanges_trade(date, date)

-- DROP FUNCTION get_mutiple_exchanges_trade(date, date);

CREATE OR REPLACE FUNCTION get_mutiple_exchanges_trade(date_from date, date_to date)
  RETURNS text AS
$BODY$ 
declare
ret text;
BEGIN
  ret ='select distinct (ts_id * 1000000 + ts_cid) from (
		select date, ask_bid, ts_cid, ts_id, count(ask_bid) from ask_bids where date between ''' || date_from || ''' and ''' || date_to ||'''
		GROUP BY date, ts_id, ts_cid, ask_bid
		HAVING count(ask_bid) > 1
		ORDER BY ts_cid
            ) as tmp3';

  return ret;
END
$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION get_mutiple_exchanges_trade(date, date)
  OWNER TO postgres;


-- Function: get_query_string_array_id_multi_orders(date, date)

-- DROP FUNCTION get_query_string_array_id_multi_orders(date, date);

CREATE OR REPLACE FUNCTION get_query_string_array_id_multi_orders(date_from date, date_to date)
  RETURNS text AS
$BODY$ 
declare
ret text;
BEGIN
 ret ='select distinct (ts_id * 1000000 + cross_id) from (
          SELECT tmp.date, tmp.cross_id, tmp.ts_id, tmp.symbol, tmp.entry_exit,count(tmp.entry_exit) as count
          FROM (SELECT date, cross_id, ts_id, symbol, ecn, entry_exit, count(entry_exit) AS count_entryexit
               FROM new_orders
               WHERE new_orders.ts_id <> -1 and date between ''' || date_from || ''' and ''' || date_to ||'''
               GROUP BY cross_id, date, ts_id, symbol, ecn, entry_exit
               ORDER BY cross_id
            ) as tmp
          GROUP BY tmp.date, tmp.cross_id, tmp.ts_id, tmp.symbol, tmp.entry_exit
          HAVING count(tmp.entry_exit) > 1
          ORDER BY tmp.cross_id
        ) as tmp2';

  return ret;
END
$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION get_query_string_array_id_multi_orders(date, date)
  OWNER TO postgres;
  
-- Function: cal_filter_trade_statistic_byecn2_twt2_notime(date, date, integer, double precision, text, integer, integer, text, integer, text, text)

-- DROP FUNCTION cal_filter_trade_statistic_byecn2_twt2_notime(date, date, integer, double precision, text, integer, integer, text, integer, text, text);

CREATE OR REPLACE FUNCTION cal_filter_trade_statistic_byecn2_twt2_notime(to_date date, from_date date, share_size integer, expected_profit double precision, symb text, bt_shares integer, sd_shares integer, ecn_filter text, ts_id integer, cecn text, is_iso text)
  RETURNS trade_result_summary2 AS
$BODY$ 
declare
  trade_result record;
  summary trade_result_summary2;
  shares_filled integer;
  shares_stuck integer;
  profit_loss double precision;
 --statistic missed trade 
  count_ouch_missed_trade integer;
  sum_ouch_expected_pl double precision;
  missed_trade_record_of_ouch record;
  str_sql text;
  str_sql_no text;
  str_sql_multi_orders text;
  str_sql_tmp text;
  tmp_str text;
  
begin	
  -- Initia
  summary.count_miss := 0;
  summary.count_repriced := 0;
  summary.count_stuck_loss := 0;
  summary.count_stuck_profit := 0;
  summary.count_loss := 0;
  summary.count_profit_under := 0;
  summary.count_profit_above := 0;
  summary.count_total := 0;
  summary.expected_miss := 0.00;
  summary.expected_repriced := 0.00;
  summary.expected_stuck_loss := 0.00;
  summary.expected_stuck_profit := 0.00;
  summary.expected_loss := 0.00;
  summary.expected_profit_under := 0.00;
  summary.expected_profit_above := 0.00;
  summary.expected_total := 0.00;
  summary.real_miss := 0.00;
  summary.real_stuck_loss := 0.00;
  summary.real_stuck_profit := 0.00;
  summary.real_loss = 0.00;
  summary.real_profit_under := 0.00;
  summary.real_profit_above := 0.00;
  summary.real_total := 0.00;


	--Calculate repriced
 IF bt_shares = 0 AND sd_shares = 0 THEN
    --Select data from view "vwouch_missed_trades" and calculate total missed trade 	
	str_sql:='SELECT count(*) as count_missed, sum(expected_pl)as sum_expected_pl  FROM vwtrade_results WHERE date >='''|| to_date || ''' and date <='''|| from_date ||''' and expected_shares >='|| share_size ||' and expected_pl >=' || expected_profit ;
	str_sql := str_sql || ' and bought_shares < 1 and sold_shares < 1 ';
		--filter by ts when we get data from view vwtrade_results
		IF ts_id > -1 THEN
			str_sql:=str_sql || ' and ts_id =' || ts_id;
		END IF;
	str_sql_no :='SELECT (ts_id * 1000000 + cross_id) as tmpcol FROM new_orders WHERE date >='''|| to_date || ''' and date <='''|| from_date ||'''';	
	str_sql_no :=str_sql_no || ' and ecn =''OUCH'' and price != ack_price ';
		--filter by ts, ecn_launch when we get data from table new_orders
		IF ts_id > -1 THEN
			str_sql_no:=str_sql_no || ' and ts_id =' || ts_id;
		END IF;
		IF cecn <> '0' THEN
			str_sql_no:=str_sql_no || ' and ecn_launch =''' || cecn ||'''';
		END IF;
	IF ecn_filter = 'arcaen' THEN
		str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Entry''' ;
	ELSIF ecn_filter = 'arcaex' THEN
		str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Exit''' ;
	ELSIF ecn_filter = 'ouchen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'ouchex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Exit''';
	ELSIF ecn_filter = 'rashen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'rashex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Exit''';		
	ELSIF ecn_filter = 'batsen' THEN
		str_sql_no:=str_sql_no || ' and ecn =' ||'''BATS''' || ' and entry_exit=''Entry''';	
	ELSIF ecn_filter = 'batsex' THEN
		str_sql_no:=str_sql_no || ' and ecn =' ||'''BATS''' || ' and entry_exit=''Exit''';
	ELSIF ecn_filter = 'oubxen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''OUBX'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'oubxex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''OUBX'' and entry_exit=''Exit''';
	ELSIF ecn_filter = 'rabxen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''RABX'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'rabxex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''RABX'' and entry_exit=''Exit''';
	ELSIF ecn_filter = 'nyseen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'nyseex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Exit''';
	END IF;
	
	--Filter by ISO Odrer
	IF is_iso = '1' AND from_date > '2010-01-01' THEN
		str_sql_no:=str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
	END IF;	
	tmp_str := str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
	
	IF is_iso = '2' AND from_date > '2010-01-01' THEN
		str_sql_no:=str_sql_no || ' and (is_iso <> ''Y'' and (ts_id * 1000000 + cross_id) not in (' || tmp_str || '))';
	END IF;
	
	IF symb <>'' THEN
		str_sql:=str_sql || ' and symbol='''|| symb ||'''';
		str_sql_no:=str_sql_no || ' and symbol='''|| symb ||'''';
	END IF;

		str_sql_no:=str_sql_no || ' GROUP BY (ts_id * 1000000 + cross_id)' ;

	IF bt_shares > 0 THEN
		str_sql:=str_sql || ' and bought_shares >=' || bt_shares;
	END IF;
	IF sd_shares > 0 THEN
		str_sql:=str_sql || ' and sold_shares >=' || sd_shares;
	END IF;

	str_sql:=str_sql || ' and (ts_id * 1000000 + ts_cid) in ('|| str_sql_no ||')';
	
	IF ecn_filter <> '0' THEN
		str_sql_multi_orders := get_query_string_array_id_multi_orders(to_date, from_date);
		str_sql_tmp := get_mutiple_exchanges_trade(to_date, from_date); 
		str_sql :=str_sql || ' and (ts_id * 1000000 + ts_cid) not in (' || str_sql_multi_orders || ')'|| ' and (ts_id * 1000000 + ts_cid) not in (' || str_sql_tmp || ')';
	END IF;
	
	for missed_trade_record_of_ouch in EXECUTE str_sql loop
		count_ouch_missed_trade := missed_trade_record_of_ouch.count_missed;
		sum_ouch_expected_pl := missed_trade_record_of_ouch.sum_expected_pl;
	end loop;

	   IF count_ouch_missed_trade > 0 THEN
		summary.count_repriced := count_ouch_missed_trade;
		summary.expected_repriced := sum_ouch_expected_pl;

		summary.count_miss := -count_ouch_missed_trade;
		summary.expected_miss :=  -sum_ouch_expected_pl;
	   END IF;

  END IF;

--filter by  ECN by Entry/Exit
  str_sql:='SELECT * FROM vwtrade_results WHERE date >='''|| to_date || ''' and date <='''|| from_date ||''' and expected_shares >='|| share_size ||' and expected_pl >=' || expected_profit ;
  str_sql_no :='SELECT (ts_id * 1000000 + cross_id) as tmpcol FROM new_orders WHERE date >='''|| to_date || ''' and date <='''|| from_date ||'''';  
	--filter by ts, ecn launch
	IF ts_id > -1 THEN
		str_sql_no:=str_sql_no || ' and ts_id =' || ts_id;
	END IF;
	IF cecn <> '0' THEN
		str_sql_no:=str_sql_no || ' and ecn_launch =''' || cecn ||'''';
	END IF;
	
	IF ecn_filter = 'arcaen' THEN
		str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Entry''' ;
	ELSIF ecn_filter = 'arcaex' THEN
		str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Exit''' ;
	ELSIF ecn_filter = 'ouchen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'ouchex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Exit''';
	ELSIF ecn_filter = 'rashen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'rashex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Exit''';		
	ELSIF ecn_filter = 'batsen' THEN
		str_sql_no:=str_sql_no || ' and ecn =' ||'''BATS''' || ' and entry_exit=''Entry''';	
	ELSIF ecn_filter = 'batsex' THEN
		str_sql_no:=str_sql_no || ' and ecn =' ||'''BATS''' || ' and entry_exit=''Exit''';
	ELSIF ecn_filter = 'oubxen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''OUBX'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'oubxex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''OUBX'' and entry_exit=''Exit''';
	ELSIF ecn_filter = 'rabxen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''RABX'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'rabxex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''RABX'' and entry_exit=''Exit''';
	ELSIF ecn_filter = 'nyseen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'nyseex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Exit''';
	END IF;

	--Filter by ISO Odrer
	IF is_iso = '1' AND from_date > '2010-01-01' THEN
		str_sql_no:=str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
	END IF;	
	tmp_str := str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
	
	IF is_iso = '2' AND from_date > '2010-01-01' THEN
		str_sql_no:=str_sql_no || ' and (is_iso <> ''Y'' and (ts_id * 1000000 + cross_id) not in (' || tmp_str || '))';
	END IF;
	
	str_sql_no:=str_sql_no || ' GROUP BY (ts_id * 1000000 + cross_id)' ;
	
	-- filter by Symbol, Bought Shares, Sold Shares
	IF symb <>'' THEN
		str_sql:=str_sql || ' and symbol='''|| symb ||'''';
	END IF;
	IF bt_shares > 0 THEN
		str_sql:=str_sql || ' and bought_shares >=' || bt_shares;
	END IF;
	IF sd_shares > 0 THEN
		str_sql:=str_sql || ' and sold_shares >=' || sd_shares;
	END IF;
	--filter by ts
	IF ts_id > -1 THEN 
		str_sql:=str_sql || ' and ts_id =' || ts_id;
	END IF;
	
	str_sql:=str_sql || ' and (ts_id * 1000000 + ts_cid) in ('|| str_sql_no ||')';
	
	IF ecn_filter <> '0' THEN
		str_sql_multi_orders := get_query_string_array_id_multi_orders(to_date, from_date);
		str_sql_tmp := get_mutiple_exchanges_trade(to_date, from_date); 
		str_sql :=str_sql || ' and (ts_id * 1000000 + ts_cid) not in (' || str_sql_multi_orders || ')'|| ' and (ts_id * 1000000 + ts_cid) not in (' || str_sql_tmp || ')';
	END IF;
	
--calculate for trade_statistics table	
FOR trade_result in EXECUTE str_sql loop
    shares_filled := trade_result.bought_shares;

    IF (shares_filled > trade_result.sold_shares) THEN
      shares_filled := trade_result.sold_shares;
    END IF;

    shares_stuck := trade_result.bought_shares - trade_result.sold_shares;    

    IF (shares_stuck = 0) THEN
      profit_loss := trade_result.total_sold - trade_result.total_bought;

      IF (shares_filled = 0) THEN
        summary.count_miss := summary.count_miss + 1;
        summary.expected_miss := summary.expected_miss + trade_result.expected_pl;
      ELSE
        IF (profit_loss < 0) THEN
          summary.count_loss := summary.count_loss + 1;
          summary.expected_loss := summary.expected_loss + trade_result.expected_pl;
          summary.real_loss := summary.real_loss + profit_loss;
        ELSE
          IF ( profit_loss::real < trade_result.expected_pl::real) THEN
            summary.count_profit_under := summary.count_profit_under + 1;
            summary.expected_profit_under := summary.expected_profit_under + trade_result.expected_pl;
            summary.real_profit_under := summary.real_profit_under + profit_loss;
          ELSE
            summary.count_profit_above := summary.count_profit_above + 1;
            summary.expected_profit_above := summary.expected_profit_above + trade_result.expected_pl;
            summary.real_profit_above := summary.real_profit_above + profit_loss;
          END IF;
        END IF;
      END IF;
    ELSE
      --Calculate for stuck
      IF trade_result.real_value < 0 THEN
	--stuck_loss
	   summary.count_stuck_loss := summary.count_stuck_loss + 1;
	   summary.expected_stuck_loss := summary.expected_stuck_loss + trade_result.expected_pl;
	   summary.real_stuck_loss := summary.real_stuck_loss + trade_result.real_value;
      ELSE
	--stuck_profit
	   summary.count_stuck_profit := summary.count_stuck_profit + 1;
	   summary.expected_stuck_profit := summary.expected_stuck_profit + trade_result.expected_pl;
	   summary.real_stuck_profit := summary.real_stuck_profit + trade_result.real_value;
      END IF;
    END IF;

    summary.count_total := summary.count_total +  1;
    summary.expected_total := summary.expected_total + trade_result.expected_pl  ;
  end loop;

  return summary;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION cal_filter_trade_statistic_byecn2_twt2_notime(date, date, integer, double precision, text, integer, integer, text, integer, text, text)
  OWNER TO postgres;

COMMIT;