BEGIN;

ALTER TABLE fills ADD COLUMN total_fee double precision DEFAULT 0.00;
ALTER TABLE new_orders ADD COLUMN total_fee double precision DEFAULT 0.00;

ALTER TABLE trade_results ADD COLUMN ts_total_fee double precision DEFAULT 0.00;
ALTER TABLE trade_results ADD COLUMN pm_total_fee double precision DEFAULT 0.00;
ALTER TABLE trade_results ADD COLUMN ts_profit_loss double precision DEFAULT 0.00;
ALTER TABLE trade_results ADD COLUMN pm_profit_loss double precision DEFAULT 0.00;

DROP FUNCTION IF EXISTS cal_stock_type(date, date, integer, text, integer, integer, text, text);
DROP FUNCTION IF EXISTS cal_trading_info(date, date, integer, integer, integer);

DROP FUNCTION IF EXISTS cal_trade_statistic(date, date);
DROP FUNCTION IF EXISTS cal_trade_statistic(date, date, integer, double precision);
DROP FUNCTION IF EXISTS cal_trade_statistic_test(date, date, integer, double precision);
DROP FUNCTION IF EXISTS cal_trade_statistic_filter_by_tradetype(date, date, integer, double precision, integer);
DROP FUNCTION IF EXISTS cal_trade_statistic_filter_by_tradetype2(date, date, integer, double precision, integer);
DROP FUNCTION IF EXISTS cal_trade_statistic_date_range(date, date, time without time zone, time without time zone, integer, double precision, text, integer, integer, text, integer, text, text, integer, text);

DROP FUNCTION IF EXISTS cal_filter_trade_statistic(date, date, integer, double precision, text, integer, integer);
DROP FUNCTION IF EXISTS cal_filter_trade_statistic_byecn(date, date, integer, double precision, text, integer, integer, text);
DROP FUNCTION IF EXISTS cal_filter_trade_statistic_byecn2(date, date, integer, double precision, text, integer, integer, text, integer, text);
DROP FUNCTION IF EXISTS cal_filter_trade_statistic_byecn2_twt2_notime(date, date, integer, double precision, text, integer, integer, text, integer, text, text, integer, text);

--
-- Name: insertexecutedorder4directedge(integer, date, integer, double precision, time without time zone, time without time zone, text, text, text, text, text, integer, double precision, integer, double precision); Type: FUNCTION; Schema: public; Owner: postgres
--

DROP FUNCTION IF EXISTS insertexecutedorder4directedge(integer, date, integer, double precision, time without time zone, time without time zone, text, text, text, text, text, integer, double precision, integer);

CREATE OR REPLACE FUNCTION insertexecutedorder4directedge(_orderid integer, _date date, _shares integer, _price double precision, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _execid text, _msgcontents text, _liquidityindicator text, _status text, _leftshares integer, _avgprice double precision, _entryexitrefnum integer, _totalFee double precision) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	orderID integer;
BEGIN
	
	SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
	
	IF orderID IS NULL THEN
		RETURN -1;
	END IF;
	
	INSERT INTO fills (oid, shares, price, time_stamp, processed_time_stamp, msgseq_num, origclord_id, exec_id, msg_content, liquidity, entry_exit_ref, total_fee) VALUES (orderID, _shares, _price, _timeStamp, _processedTimestamp, _seqNum, _orderID, _execID, _msgContents, _liquidityIndicator, _entryExitRefNum, _totalFee);

	UPDATE new_orders SET status = _status, left_shares = _leftShares, avg_price = _avgPrice WHERE oid = orderID;
	UPDATE new_orders SET price = _price WHERE oid = orderID AND price = 0;
	
	RETURN 0;
END
$$;


ALTER FUNCTION public.insertexecutedorder4directedge(_orderid integer, _date date, _shares integer, _price double precision, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _execid text, _msgcontents text, _liquidityindicator text, _status text, _leftshares integer, _avgprice double precision, _entryexitrefnum integer, _totalFee double precision) OWNER TO postgres;

--
-- Name: insertexecutedorder4variant1(integer, date, integer, double precision, time without time zone, time without time zone, text, text, text, character, text, integer, double precision, integer, double precision); Type: FUNCTION; Schema: public; Owner: postgres
--

DROP FUNCTION IF EXISTS insertexecutedorder4variant1(integer, date, integer, double precision, time without time zone, time without time zone, text, text, text, character, text, integer, double precision, integer);

CREATE OR REPLACE FUNCTION insertexecutedorder4variant1(_orderid integer, _date date, _shares integer, _price double precision, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _execid text, _msgcontents text, _liquidityindicator character, _status text, _leftshares integer, _avgprice double precision, _entryexitrefnum integer, _totalFee double precision) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	orderID integer;
BEGIN
	
	SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
	
	IF orderID IS NULL THEN
		RETURN -1;
	END IF;
	
	INSERT INTO fills (oid, shares, price, time_stamp, processed_time_stamp, msgseq_num, origclord_id, exec_id, msg_content, liquidity, entry_exit_ref, total_fee) VALUES (orderID, _shares, _price, _timeStamp, _processedTimestamp, _seqNum, _orderID, _execID, _msgContents, _liquidityIndicator, _entryExitRefNum, _totalFee);

	UPDATE new_orders SET status = _status, left_shares = _leftShares, avg_price = _avgPrice WHERE oid = orderID;
	
	RETURN 0;
END
$$;


ALTER FUNCTION public.insertexecutedorder4variant1(_orderid integer, _date date, _shares integer, _price double precision, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _execid text, _msgcontents text, _liquidityindicator character, _status text, _leftshares integer, _avgprice double precision, _entryexitrefnum integer, _totalFee double precision) OWNER TO postgres;

--
-- Name: insertexecutedorder4variant2(integer, date, integer, double precision, time without time zone, time without time zone, text, text, text, character, text, integer, double precision, integer, double precision); Type: FUNCTION; Schema: public; Owner: postgres
--

DROP FUNCTION IF EXISTS insertexecutedorder4variant2(integer, date, integer, double precision, time without time zone, time without time zone, text, text, text, character, text, integer, double precision, integer);

CREATE OR REPLACE FUNCTION insertexecutedorder4variant2(_orderid integer, _date date, _shares integer, _price double precision, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _execid text, _msgcontents text, _liquidityindicator character, _status text, _leftshares integer, _avgprice double precision, _entryexitrefnum integer, _totalFee double precision) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	orderID integer;
BEGIN
	
	SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
	
	IF orderID IS NULL THEN
		RETURN -1;
	END IF;
	
	INSERT INTO fills (oid, shares, price, time_stamp, processed_time_stamp, msgseq_num, origclord_id, exec_id, msg_content, liquidity, entry_exit_ref, total_fee) VALUES (orderID, _shares, _price, _timeStamp, _processedTimestamp, _seqNum, _orderID, _execID, _msgContents, _liquidityIndicator, _entryExitRefNum, _totalFee);

	UPDATE new_orders SET status = _status, left_shares = _leftShares, avg_price = _avgPrice WHERE oid = orderID;
	UPDATE new_orders SET price = _price WHERE oid = orderID AND price = 0;
	
	RETURN 0;
END
$$;


ALTER FUNCTION public.insertexecutedorder4variant2(_orderid integer, _date date, _shares integer, _price double precision, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _execid text, _msgcontents text, _liquidityindicator character, _status text, _leftshares integer, _avgprice double precision, _entryexitrefnum integer, _totalFee double precision) OWNER TO postgres;

--
-- Name: insertexecutedorder4variant3(integer, date, integer, double precision, time without time zone, time without time zone, text, text, text, text, character, text, integer, double precision, integer, double precision); Type: FUNCTION; Schema: public; Owner: postgres
--

DROP FUNCTION IF EXISTS insertexecutedorder4variant3(integer, date, integer, double precision, time without time zone, time without time zone, text, text, text, text, character, text, integer, double precision, integer);

CREATE OR REPLACE FUNCTION insertexecutedorder4variant3(_orderid integer, _date date, _shares integer, _price double precision, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _arcaexorderid text, _execid text, _msgcontents text, _liquidityindicator character, _status text, _leftshares integer, _avgprice double precision, _entryexitrefnum integer, _totalFee double precision) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	orderID integer;
BEGIN
	
	SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
	
	IF orderID IS NULL THEN
		RETURN -1;
	END IF;
	
	INSERT INTO fills (oid, shares, price, time_stamp, processed_time_stamp, msgseq_num, origclord_id, arcaex_order_id, exec_id, msg_content, liquidity, entry_exit_ref, total_fee) VALUES (orderID, _shares, _price, _timeStamp, _processedTimestamp, _seqNum, _orderID, _arcaExOrderID, _execID, _msgContents, _liquidityIndicator, _entryExitRefNum, _totalFee);

	UPDATE new_orders SET status = _status, left_shares = _leftShares, avg_price = _avgPrice WHERE oid = orderID;
	
	RETURN 0;
END
$$;


ALTER FUNCTION public.insertexecutedorder4variant3(_orderid integer, _date date, _shares integer, _price double precision, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _arcaexorderid text, _execid text, _msgcontents text, _liquidityindicator character, _status text, _leftshares integer, _avgprice double precision, _entryexitrefnum integer, _totalFee double precision) OWNER TO postgres;

--
-- Name: insertfillorder4nyse_ccg(integer, date, integer, double precision, time without time zone, time without time zone, text, text, text, text, character, text, integer, double precision, integer, double precision); Type: FUNCTION; Schema: public; Owner: postgres
--

DROP FUNCTION IF EXISTS insertfillorder4nyse_ccg(integer, date, integer, double precision, time without time zone, time without time zone, text, text, text, text, character, text, integer, double precision, integer);

CREATE OR REPLACE FUNCTION insertfillorder4nyse_ccg(_orderid integer, _date date, _shares integer, _price double precision, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _meorderid text, _execid text, _msgcontents text, _billingindicator character, _status text, _leftshares integer, _avgprice double precision, _entryexitrefnum integer, _totalFee double precision) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	orderID integer;
BEGIN
	
	SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
	
	IF orderID IS NULL THEN
		RETURN -1;
	END IF;
	
	INSERT INTO fills (oid, shares, price, time_stamp, processed_time_stamp, msgseq_num, origclord_id, arcaex_order_id, exec_id, msg_content, liquidity, entry_exit_ref, total_fee) VALUES (orderID, _shares, _price, _timestamp, _processedTimestamp, _seqnum, _orderID, _meorderid, _execid, _msgcontents, _billingindicator, _entryExitRefNum, _totalFee);

	UPDATE new_orders SET status = _status, left_shares = _leftshares, avg_price = _avgprice WHERE oid = orderID;
	
	RETURN 0;
END$$;


ALTER FUNCTION public.insertfillorder4nyse_ccg(_orderid integer, _date date, _shares integer, _price double precision, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _meorderid text, _execid text, _msgcontents text, _billingindicator character, _status text, _leftshares integer, _avgprice double precision, _entryexitrefnum integer, _totalFee double precision) OWNER TO postgres;

--
-- Name: insert_fill_hung_order(integer, text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE OR REPLACE FUNCTION insert_fill_hung_order(oid_t integer, list_shares text, list_price text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	shares_array integer[] = string_to_array(list_shares, '_');
	price_array double precision[] = string_to_array(list_price, '_');
	tempOrder record;
	tempFill record;
	i integer;
	liquidity character;
	msg_content text;
	id_current integer;
	exce_id_tmp text;
	total_fill_shares integer;
	totalFee double precision;
BEGIN
 
	i = 1;
	total_fill_shares = 0;
	select * into tempOrder from new_orders where oid = oid_t;
	if tempOrder.ecn = 'ARCA' then
		liquidity = 'R';
	elsif tempOrder.ecn = 'OUCH' then
		liquidity = 'R';
	elsif tempOrder.ecn = 'RASH' then
		liquidity = 'R';
	elsif tempOrder.ecn = 'NYSE' then
		liquidity = '1';
	elsif tempOrder.ecn = 'BATZ' then
		liquidity = 'R';
	end if;
	select max(id) into id_current from fills; 
	IF id_current IS NULL THEN
		id_current = 1;
	END IF;
	exce_id_tmp = '';
	WHILE shares_array[i] IS NOT NULL LOOP
		msg_content = '';
		total_fill_shares = total_fill_shares + shares_array[i];
		if tempOrder.ecn = 'ARCA' AND tempOrder.ecn_suffix IS NULL then
			msg_content = msg_content || '-50=2';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-46=' || tempOrder.order_id;
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-44=' || shares_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-43=' || (price_array[i]*100);
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-42=' || '2';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-29=' || '99999';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-28=' || 'R';
		ELSIF tempOrder.ecn = 'ARCA' AND tempOrder.ecn_suffix = 'FIX' then
			msg_content = msg_content || '35=8';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '11=' || tempOrder.order_id;
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '38=' || shares_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '44=' || price_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '17=' || '99999';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '9730=' || 'R';
		ELSIF tempOrder.ecn = 'OUCH' THEN
			msg_content = msg_content || '-13=E';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-12=' || tempOrder.order_id;
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-10=' || shares_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-8=' || price_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-2=' || 'R';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-29=' || '99999';
			msg_content = msg_content || E'\001';
		ELSIF tempOrder.ecn = 'RASH' THEN
			msg_content = msg_content || '-13=E';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-12=' || tempOrder.order_id;
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-10=' || shares_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-8=' || price_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-2=' || 'R';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-29=' || '99999';
			msg_content = msg_content || E'\001';
		ELSIF tempOrder.ecn = 'NYSE' THEN
			msg_content = msg_content || '-50=81';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-10=' || shares_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-43=' || (price_array[i]*100);
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-42=' || '2';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-29=' || '99999';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-28=' || '1';
		ELSIF tempOrder.ecn = 'BATZ' THEN
			msg_content = msg_content || '-50=11';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-81=' || shares_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-83=' || price_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-28=' || 'R';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-29=' || '99999';
			msg_content = msg_content || E'\001';
			
		END IF;
		
		msg_content = msg_content || E'\001';
		id_current = id_current + 1;
		exce_id_tmp = '99999' || '_' || id_current;
		
		-- Calculate fill fee
		SELECT calculate_total_fee(price_array[i], shares_array[i], substring(tempOrder.side from 1 for 1), liquidity, tempOrder.ecn) into totalFee;
		
		INSERT INTO fills(oid, shares, price, time_stamp, processed_time_stamp, msgseq_num, origclord_id, exec_id, msg_content, liquidity,  manual_update, total_fee)
			VALUES(oid_t, shares_array[i], price_array[i], tempOrder.time_stamp, tempOrder.time_stamp, tempOrder.msgseq_num, tempOrder.order_id, exce_id_tmp, msg_content, liquidity, 1, totalFee);
		
		i = i + 1;
	END LOOP;
	
	IF total_fill_shares < tempOrder.shares THEN
		UPDATE new_orders SET status = 'Lived' where oid = oid_t;
		UPDATE new_orders SET left_shares = (tempOrder.shares - total_fill_shares) where oid = oid_t;
	ELSE
		UPDATE new_orders SET status = 'Closed' where oid = oid_t;
		UPDATE new_orders SET left_shares = 0 where oid = oid_t;
	END IF;
	
	UPDATE trading_status SET hung_order = 1;
	
	return 1;
END
$$;

ALTER FUNCTION public.insert_fill_hung_order(oid_t integer, list_shares text, list_price text) OWNER TO postgres;

CREATE OR REPLACE FUNCTION process_fills_insert() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
	symbolVal	text;
	leave_Shares	integer;
	avgPrice	double precision;
	totalQty	integer;
	netPL		double precision;
	totalMktValue	double precision;
	rowcount	integer;
	dateVal		date;
	sideVal		text;
	accountVal 	integer;
	tsIdVal 	integer;
	crossIdVal 	integer;
	ecnVal		text;
	liquidity	character;
	totalFee	double precision;
BEGIN
	-- Get symbol name
	SELECT trim(symbol), date, substring(side from 1 for 1) as side, account, trim(ecn), ts_id, cross_id into symbolVal, dateVal, sideVal, accountVal, ecnVal, tsIdVal, crossIdVal
	FROM new_orders WHERE oid = NEW.oid;

	-- Check symbol existent?
	SELECT COUNT(*) into rowcount
	FROM risk_managements
	WHERE trim(symbol) = trim(symbolVal) and date = dateVal and account = accountVal;
	
	UPDATE new_orders 
		SET total_fee = total_fee + NEW.total_fee 
	WHERE oid = NEW.oid;
	
	IF (tsIdVal <> -1) THEN
		SELECT SUM(total_fee) INTO totalFee
		FROM new_orders
		WHERE date = dateVal AND ts_id = tsIdVal AND cross_id = crossIdVal;
		
		UPDATE trade_results
			SET ts_total_fee = totalFee
		WHERE date = dateVal AND ts_id = tsIdVal AND ts_cid = crossIdVal;
	END IF;
	
	-- If new symbol
	IF (rowcount = 0) THEN
		IF (sideVal = 'B') THEN
			leave_Shares := NEW.shares;
		ELSE
			leave_Shares := -NEW.shares;
		END IF;
		
		totalMktValue := abs(leave_Shares) * NEW.price;
		
		-- insert into RM
		totalFee := NEW.total_fee;
				
		INSERT INTO risk_managements(symbol, average_price, net_quantity, matched_profit_loss, total_market_value, total_quantity, date, last_update, account, total_fee) 
			VALUES (symbolVal , NEW.price, leave_Shares, 0.00, totalMktValue, NEW.shares, dateVal, NEW.time_stamp, accountVal, totalFee);
	ELSE -- Old symbol
		SELECT average_price, net_quantity, matched_profit_loss, total_market_value, total_quantity, total_fee INTO avgPrice, leave_Shares, netPL, totalMktValue, totalQty, totalFee
		FROM risk_managements
		WHERE trim(symbol) = symbolVal AND date = dateVal AND account = accountVal;
				
		IF (leave_Shares = 0) THEN
			avgPrice := NEW.price;
			IF (sideVal = 'B') THEN
				leave_Shares := NEW.shares;
			ELSE -- Action is Sell or Short Sell
				leave_Shares := -NEW.shares;
			END IF;
		ELSIF (leave_Shares > 0) THEN
			IF (sideVal = 'B') THEN
				avgPrice := (avgPrice * leave_Shares + NEW.price * NEW.shares) / (NEW.shares + leave_Shares);
				leave_Shares := leave_Shares + NEW.shares;
			ELSE
				netPL := netPL + ((NEW.price - avgPrice) * LEAST(NEW.shares, leave_Shares));				
				leave_Shares := leave_Shares - NEW.shares;
				IF (leave_Shares = 0) THEN
					avgPrice := 0.00;
				ELSIF (leave_Shares < 0) THEN
					avgPrice := NEW.price;
				END IF;
			END IF;
		ELSE 
			IF (sideVal = 'B') THEN
				netPL := netPL + ((avgPrice - NEW.price)*LEAST(NEW.shares, abs(leave_Shares)));				
				leave_Shares := leave_Shares + NEW.shares;
				IF (leave_Shares = 0) THEN
					avgPrice := 0.00;
				ELSIF (leave_Shares > 0) THEN
					avgPrice := NEW.price;
				END IF;
			ELSE
				avgPrice := (avgPrice*(-leave_Shares) + NEW.price * NEW.shares) / (NEW.shares + (-leave_Shares));
				leave_Shares := leave_Shares - NEW.shares;	
			END IF;
		END IF;
		
		totalMktValue := avgPrice * abs(leave_Shares);
		totalQty := totalQty + NEW.shares;		
		totalFee := totalFee + NEW.total_fee;

		-- update RM
		UPDATE risk_managements
		SET average_price = avgPrice,
			net_quantity = leave_Shares, 
			matched_profit_loss = netPL, 
			total_market_value = totalMktValue, 
			total_quantity = totalQty, 
			last_update = NEW.time_stamp,
			total_fee = totalFee
		WHERE symbol = symbolVal and date = dateVal and account = accountVal;
	END IF;
	
	RETURN NEW;
END $$;

ALTER FUNCTION public.process_fills_insert() OWNER TO postgres;


CREATE FUNCTION insert_total_fee_in_tables() RETURNS integer
    LANGUAGE plpgsql
    AS $$ 
DECLARE
	recordTmp record;
	retVal integer;
	tempTotalFee double precision;
BEGIN  
	retVal := 0;
	
	CREATE TABLE temp_fills AS
	SELECT n.oid, n.date, n.order_id, n.ts_id, n.cross_id, substring(n.side from 1 for 1) as side, trim(n.ecn) as ecn, f.fill_id, f.shares, f.price, f.liquidity
	FROM new_orders n 
	JOIN fills f ON n.oid=f.oid
	LEFT JOIN brokens b ON f.oid = b.oid AND f.exec_id = b.exec_id 
	WHERE (b.oid IS NULL) OR ((b.oid IS NOT NULL) AND (b.new_shares > 0));
		
	FOR recordTmp IN (SELECT * FROM temp_fills) LOOP
		retVal = retVal + 1;
		
		-- Calculate fill fee
		SELECT calculate_total_fee(recordTmp.price, recordTmp.shares, recordTmp.side, recordTmp.liquidity, recordTmp.ecn) into tempTotalFee;
	
		IF (tempTotalFee <> 0.0) THEN
			UPDATE fills 
				SET total_fee = tempTotalFee
			WHERE oid = recordTmp.oid AND fill_id = recordTmp.fill_id;
			
			UPDATE new_orders 
				SET total_fee = total_fee + tempTotalFee 
			WHERE oid = recordTmp.oid;
			
			IF (recordTmp.ts_id <> -1) THEN
				UPDATE trade_results
					SET ts_total_fee = ts_total_fee + tempTotalFee
				WHERE date = recordTmp.date AND ts_id = recordTmp.ts_id AND ts_cid = recordTmp.cross_id;
			END IF;
		END IF;
	END LOOP;

	RETURN retVal;
END
$$;

ALTER FUNCTION public.insert_total_fee_in_tables() OWNER TO postgres;


CREATE FUNCTION recalculate_profit_loss() RETURNS integer
    LANGUAGE plpgsql
    AS $$ 
DECLARE
	recordTmp record;
	retVal integer;
	tsProfitLoss double precision;
	pmProfitLoss double precision;
	tempTotalFee double precision;
BEGIN  
	retVal := 0;
	
	CREATE TABLE temp_trade_results AS
	SELECT date, ts_id, ts_cid, bought_shares, total_bought, sold_shares, total_sold, real_value
	FROM trade_results;
		
	FOR recordTmp IN (SELECT * FROM temp_trade_results) LOOP
		retVal = retVal + 1;
		tempTotalFee := 0.0;
		
		IF (recordTmp.bought_shares > 0 AND recordTmp.sold_shares > 0) THEN
			tsProfitLoss := (recordTmp.total_sold/recordTmp.sold_shares - 
							recordTmp.total_bought/recordTmp.bought_shares) * 
							LEAST(recordTmp.bought_shares, recordTmp.sold_shares);
			pmProfitLoss := recordTmp.real_value - tsProfitLoss;
		ELSE
			tsProfitLoss := 0.0;
			pmProfitLoss := recordTmp.real_value;
			
			IF (recordTmp.bought_shares > recordTmp.sold_shares) THEN
				-- Calculate fill fee
				SELECT calculate_total_fee(recordTmp.total_bought/recordTmp.bought_shares, 
										recordTmp.bought_shares - recordTmp.sold_shares, 
										'S', 'R', 'ARCA') into tempTotalFee;
			ELSIF (recordTmp.sold_shares > 0) THEN
				-- Calculate fill fee
				SELECT calculate_total_fee(recordTmp.total_sold/recordTmp.sold_shares, 
										recordTmp.sold_shares - recordTmp.bought_shares, 
										'B', 'R', 'ARCA') into tempTotalFee;
			END IF;
		END IF;
		
		UPDATE trade_results
			SET ts_profit_loss = tsProfitLoss, pm_profit_loss = pmProfitLoss, pm_total_fee = tempTotalFee
		WHERE date = recordTmp.date AND ts_id = recordTmp.ts_id AND ts_cid = recordTmp.ts_cid;
	END LOOP;

	RETURN retVal;
END
$$;

ALTER FUNCTION public.recalculate_profit_loss() OWNER TO postgres;

SELECT insert_total_fee_in_tables();

DROP TABLE temp_fills;
DROP FUNCTION insert_total_fee_in_tables();

SELECT recalculate_profit_loss();

DROP TABLE temp_trade_results;
DROP FUNCTION recalculate_profit_loss();

DROP VIEW vwtrade_results;
DROP VIEW vwtrade_results_twt2;

CREATE VIEW vwtrade_results_twt2 AS
	SELECT t.expected_shares, t.expected_pl, t.bought_shares, t.total_bought, t.sold_shares, t.total_sold, t.left_stuck_shares, t.as_cid, t.ts_cid, t.ts_id, t.trade_id, t.symbol, t.date, t.ts_profit_loss, n.ecn, n.ecn_launch, n.time_stamp, n.price, n.ack_price, n.status, n.entry_exit, n.left_shares, n.shares, n.is_iso, t.pm_profit_loss, t.spread, (t.ts_profit_loss + t.pm_profit_loss - t.ts_total_fee - t.pm_total_fee) as net_pl, (t.pm_profit_loss - t.pm_total_fee) as pm_net_pl
	FROM (trade_results t JOIN (SELECT n1.date, n1.ts_id, n1.cross_id, n1.ecn, n1.time_stamp, n1.price, n1.ack_price, n1.status, n1.entry_exit, n1.left_shares, n1.shares, n1.is_iso, n2.ecn_launch FROM (new_orders n1 LEFT JOIN (SELECT DISTINCT new_orders.date, new_orders.ts_id, new_orders.cross_id, new_orders.ecn_launch FROM new_orders WHERE ((new_orders.entry_exit = 'Entry'::bpchar) AND (new_orders.ts_id <> (-1)))) n2 ON ((((n1.date = n2.date) AND (n1.ts_id = n2.ts_id)) AND (n1.cross_id = n2.cross_id))))) n ON ((((t.date = n.date) AND (t.ts_id = n.ts_id)) AND (t.ts_cid = n.cross_id))));

ALTER TABLE public.vwtrade_results_twt2 OWNER TO postgres;

ALTER TABLE trade_results DROP COLUMN IF EXISTS real_value;

--
-- Name: cal_filter_trade_statistic_byecn2_twt2(date, date, time without time zone, time without time zone, integer, double precision, text, integer, integer, text, integer, text, text, integer, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE OR REPLACE FUNCTION cal_filter_trade_statistic_byecn2_twt2(to_date date, from_date date, time_from time without time zone, time_to time without time zone, share_size integer, expected_profit double precision, symb text, bt_shares integer, sd_shares integer, ecn_filter text, ts_id integer, cecn text, is_iso text, exclusive_val integer, cross_size text) RETURNS trade_result_summary_shares_traded
    LANGUAGE plpgsql
    AS $$
declare
  trade_result record;
  summary trade_result_summary_shares_traded;
  shares_filled integer;
  shares_stuck integer;
  profit_loss double precision;
 --statistic missed trade 
  count_ouch_missed_trade integer;
  sum_shares_traded_miss integer;
  sum_ouch_expected_pl double precision;
  missed_trade_record_of_ouch record;
  str_sql text;
  str_sql1 text;
  str_sql2 text;	
  str_sql_no text;
  str_sql_no_repriced text;
  str_sql_repriced text;
  str_sql_multi_orders text;
  str_sql_tmp text;
  tmp_str text;
  tmp_ASCid text;
  new_orders_table text;
  share_size_condition text;
  
begin	
  -- Initialize
  summary.count_miss := 0;
  summary.count_repriced := 0;
  summary.count_stuck_loss := 0;
  summary.count_stuck_profit := 0;
  summary.count_loss := 0;
  summary.count_profit_under := 0;
  summary.count_profit_above := 0;
  summary.count_total := 0;
  summary.shares_traded_miss := 0;
  summary.shares_traded_repriced := 0;
  summary.shares_traded_stuck_loss := 0;
  summary.shares_traded_stuck_profit := 0;
  summary.shares_traded_loss := 0;
  summary.shares_traded_profit_under := 0;
  summary.shares_traded_profit_above := 0;
  summary.shares_traded_total := 0;
  summary.pm_shares_traded_miss := 0;
  summary.pm_shares_traded_repriced := 0;
  summary.pm_shares_traded_stuck_loss := 0;
  summary.pm_shares_traded_stuck_profit := 0;
  summary.pm_shares_traded_loss := 0;
  summary.pm_shares_traded_profit_under := 0;
  summary.pm_shares_traded_profit_above := 0;
  summary.pm_shares_traded_total := 0;
  
  summary.expected_miss := 0.00;
  summary.expected_repriced := 0.00;
  summary.expected_stuck_loss := 0.00;
  summary.expected_stuck_profit := 0.00;
  summary.expected_loss := 0.00;
  summary.expected_profit_under := 0.00;
  summary.expected_profit_above := 0.00;
  summary.expected_total := 0.00;
  summary.real_miss := 0.00;
  summary.real_stuck_loss := 0.00;
  summary.real_stuck_profit := 0.00;
  summary.real_loss = 0.00;
  summary.real_profit_under := 0.00;
  summary.real_profit_above := 0.00;
  summary.real_total := 0.00;
  
  summary.pm_miss := 0.00;
  summary.pm_stuck_loss := 0.00;
  summary.pm_stuck_profit := 0.00;
  summary.pm_loss = 0.00;
  summary.pm_profit_under := 0.00;
  summary.pm_profit_above := 0.00;
  summary.pm_total := 0.00;
  
  share_size_condition := '';
  IF share_size <> -100 THEN 
	share_size_condition := 'expected_shares >='|| share_size ||'';
  ELSE -- Filter share size < 100 
	share_size_condition := 'expected_shares < 100';   
  END IF;

  new_orders_table := '(SELECT n1.date, n1.ts_id, n1.cross_id, n1.ecn, n1.time_stamp, n1.price, n1.ack_price, n1.status, n1.entry_exit, n1.left_shares, n1.shares, n1.is_iso, n2.ecn_launch ' ||
						'FROM new_orders n1 '
						'LEFT JOIN (SELECT DISTINCT date, ts_id, cross_id, ecn_launch '
						'			FROM new_orders '
						'			WHERE entry_exit = ''Entry'' AND ts_id <> -1) n2 '
						'ON n1.date = n2.date AND n1.ts_id = n2.ts_id AND n1.cross_id = n2.cross_id) AS tmp ';
  
  str_sql :='';
  str_sql_no :='SELECT DISTINCT (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + cross_id) as tmpcol FROM ' || new_orders_table || ' WHERE date >='''|| to_date || ''' and date <='''|| from_date ||'''';
  str_sql_no :=str_sql_no || ' and time_stamp between ''' || time_from || ''' and '''|| time_to || '''';
	--filter by ts, ecn launch
	IF ts_id > -1 THEN
		str_sql_no:=str_sql_no || ' and ts_id =' || ts_id;
	END IF;
	IF cecn <> '0' THEN
		str_sql_no:=str_sql_no || ' and ecn_launch =''' || cecn ||'''';
	END IF;
	
	IF ecn_filter = 'arcaen' THEN
		str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Entry''' ;
	ELSIF ecn_filter = 'arcaex' THEN
		str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Exit''' ;
	ELSIF ecn_filter = 'ouchen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'ouchex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Exit''';
	ELSIF ecn_filter = 'rashen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'rashex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Exit''';		
	ELSIF ecn_filter = 'batzen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Entry''';	
	ELSIF ecn_filter = 'batzex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Exit''';	
	ELSIF ecn_filter = 'oubxen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''OUBX'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'oubxex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''OUBX'' and entry_exit=''Exit''';
	ELSIF ecn_filter = 'rabxen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''RABX'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'rabxex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''RABX'' and entry_exit=''Exit''';
	ELSIF ecn_filter = 'nyseen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'nyseex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Exit''';
	ELSIF ecn_filter = 'edgxen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''EDGX'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'edgxex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''EDGX'' and entry_exit=''Exit''';
	END IF;

	--Filter by ISO Odrer
	IF is_iso = '1' THEN
		str_sql_no:=str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
	END IF;
	-- tmp_str := str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
	
	IF is_iso = '2' THEN
		str_sql_no:=str_sql_no || ' and (is_iso <> ''Y'')';
	END IF;
	
	
	-- filter by Symbol, Bought Shares, Sold Shares
	IF symb <>'' THEN
		str_sql:=' and symbol='''|| symb ||'''';
	END IF;
	IF bt_shares > 0 THEN
		str_sql:=str_sql || ' and bought_shares >=' || bt_shares;
	END IF;
	IF sd_shares > 0 THEN
		str_sql:=str_sql || ' and sold_shares >=' || sd_shares;
	END IF;
	--filter by ts
	IF ts_id > -1 THEN 
		str_sql:=str_sql || ' and ts_id =' || ts_id;
	ELSIF ts_id = -2 THEN
		str_sql := str_sql || ' and bought_shares <> sold_shares ';
	END IF;
	IF cross_size <> '' then
		str_sql := str_sql || ' and ' || cross_size;
	END IF;
	IF ecn_filter <> '0' AND exclusive_val = 1 THEN
		str_sql_multi_orders := get_query_string_array_id_multi_orders(to_date, from_date);
		str_sql := str_sql || ' and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) not in (' || str_sql_multi_orders || ')';
	END IF;
	
	--Calculate repriced
 IF bt_shares = 0 AND sd_shares = 0 THEN   
	str_sql_no_repriced:=str_sql_no || ' and ecn =''OUCH'' and price != ack_price ' ;
	str_sql_repriced:=str_sql || '  and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) in ('|| str_sql_no_repriced ||')';	
	str_sql1:='SELECT count(*) as count_missed, sum(expected_pl)as sum_expected_pl, sum(bought_shares) as sum_bought_shares, sum(sold_shares) as sum_sold_shares FROM trade_results WHERE bought_shares = 0 and sold_shares = 0 and date >='''|| to_date || ''' and date <='''|| from_date ||''' and '|| share_size_condition ||' and expected_pl >=' || expected_profit ;
   
   str_sql1:=str_sql1 ||str_sql_repriced;
    for missed_trade_record_of_ouch in EXECUTE str_sql1 loop
		count_ouch_missed_trade := missed_trade_record_of_ouch.count_missed;
		sum_ouch_expected_pl := missed_trade_record_of_ouch.sum_expected_pl;
		sum_shares_traded_miss := missed_trade_record_of_ouch.sum_bought_shares + missed_trade_record_of_ouch.sum_sold_shares;
    end loop;

   IF count_ouch_missed_trade > 0 THEN
	summary.count_repriced := count_ouch_missed_trade;
	summary.expected_repriced := sum_ouch_expected_pl;
	summary.count_miss := -count_ouch_missed_trade;
	summary.expected_miss :=  -sum_ouch_expected_pl;
	summary.shares_traded_miss := -sum_shares_traded_miss;
	summary.shares_traded_repriced := sum_shares_traded_miss;
   END IF;

 END IF;
 
 --calculate for trade_statistics table	
 str_sql:=str_sql || '  and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) in ('|| str_sql_no ||')';
 str_sql2:='SELECT * FROM trade_results WHERE date >='''|| to_date || ''' and date <='''|| from_date ||''' and '|| share_size_condition ||' and expected_pl >=' || expected_profit ;
 str_sql2:=str_sql2 ||str_sql;
FOR trade_result in EXECUTE str_sql2 loop
    shares_filled := trade_result.bought_shares;

    IF (shares_filled > trade_result.sold_shares) THEN
      shares_filled := trade_result.sold_shares;
    END IF;

    shares_stuck := trade_result.bought_shares - trade_result.sold_shares;    

    IF (shares_stuck = 0) THEN
      profit_loss := trade_result.total_sold - trade_result.total_bought;

      IF (shares_filled = 0) THEN
        summary.count_miss := summary.count_miss + 1;
		summary.shares_traded_miss := summary.shares_traded_miss + trade_result.bought_shares + trade_result.sold_shares;
        summary.expected_miss := summary.expected_miss + trade_result.expected_pl;
      ELSE
        IF (profit_loss < 0) THEN
          summary.count_loss := summary.count_loss + 1;
		  summary.shares_traded_loss := summary.shares_traded_loss + trade_result.bought_shares + trade_result.sold_shares;
          summary.expected_loss := summary.expected_loss + trade_result.expected_pl;
          summary.real_loss := summary.real_loss + profit_loss;
        ELSE
          IF ( profit_loss::real < trade_result.expected_pl::real) THEN
            summary.count_profit_under := summary.count_profit_under + 1;
			summary.shares_traded_profit_under := summary.shares_traded_profit_under + trade_result.bought_shares + trade_result.sold_shares;
            summary.expected_profit_under := summary.expected_profit_under + trade_result.expected_pl;
            summary.real_profit_under := summary.real_profit_under + profit_loss;
          ELSE
            summary.count_profit_above := summary.count_profit_above + 1;
			summary.shares_traded_profit_above := summary.shares_traded_profit_above + trade_result.bought_shares + trade_result.sold_shares;
            summary.expected_profit_above := summary.expected_profit_above + trade_result.expected_pl;
            summary.real_profit_above := summary.real_profit_above + profit_loss;
          END IF;
        END IF;
      END IF;
    ELSE
      --Calculate for stuck
      IF trade_result.pm_profit_loss < -0.0001 THEN
	--stuck_loss
	   summary.count_stuck_loss := summary.count_stuck_loss + 1;
	   summary.shares_traded_stuck_loss := summary.shares_traded_stuck_loss + trade_result.bought_shares + trade_result.sold_shares;
	   summary.expected_stuck_loss := summary.expected_stuck_loss + trade_result.expected_pl;
	   summary.real_stuck_loss := summary.real_stuck_loss + trade_result.ts_profit_loss;
	   summary.pm_stuck_loss := summary.pm_stuck_loss + trade_result.pm_profit_loss;
	   IF trade_result.bought_shares < trade_result.sold_shares THEN
		summary.pm_shares_traded_stuck_loss := summary.pm_shares_traded_stuck_loss - trade_result.bought_shares + trade_result.sold_shares;
	   ELSE
		summary.pm_shares_traded_stuck_loss := summary.pm_shares_traded_stuck_loss + trade_result.bought_shares - trade_result.sold_shares;
	   END IF;
      ELSE
	--stuck_profit
	   summary.count_stuck_profit := summary.count_stuck_profit + 1;
	   summary.shares_traded_stuck_profit := summary.shares_traded_stuck_profit + trade_result.bought_shares + trade_result.sold_shares;
	   summary.expected_stuck_profit := summary.expected_stuck_profit + trade_result.expected_pl;
	   summary.real_stuck_profit := summary.real_stuck_profit + trade_result.ts_profit_loss;
	   summary.pm_stuck_profit := summary.pm_stuck_profit + trade_result.pm_profit_loss;
	   IF trade_result.bought_shares < trade_result.sold_shares THEN
		summary.pm_shares_traded_stuck_profit := summary.pm_shares_traded_stuck_profit - trade_result.bought_shares + trade_result.sold_shares;
	   ELSE
		summary.pm_shares_traded_stuck_profit := summary.pm_shares_traded_stuck_profit + trade_result.bought_shares - trade_result.sold_shares;
	   END IF;
      END IF;
    END IF;

    summary.count_total := summary.count_total +  1;
	summary.shares_traded_total := summary.shares_traded_total + trade_result.bought_shares + trade_result.sold_shares;
    summary.expected_total := summary.expected_total + trade_result.expected_pl  ;
  end loop;
  
  summary.count_total := summary.count_total - summary.count_repriced;
  summary.shares_traded_total := summary.shares_traded_total - summary.shares_traded_repriced;
  summary.expected_total := summary.expected_total - summary.expected_repriced;
  
  return summary;
end
$$;


ALTER FUNCTION public.cal_filter_trade_statistic_byecn2_twt2(to_date date, from_date date, time_from time without time zone, time_to time without time zone, share_size integer, expected_profit double precision, symb text, bt_shares integer, sd_shares integer, ecn_filter text, ts_id integer, cecn text, is_iso text, exclusive_val integer, cross_size text) OWNER TO postgres;

--
-- Name: cal_filter_trade_statistic_byecn2_twt2_with_step(date, date, time without time zone, time without time zone, integer, double precision, text, integer, integer, text, integer, text, integer, text, integer, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE OR REPLACE FUNCTION cal_filter_trade_statistic_byecn2_twt2_with_step(to_date date, from_date date, time_from time without time zone, time_to time without time zone, share_size integer, expected_profit double precision, symb text, bt_shares integer, sd_shares integer, ecn_filter text, ts_id integer, cecn text, step integer, is_iso text, exclusive_val integer, cross_size text) RETURNS SETOF trade_result_summary2_struct
    LANGUAGE plpgsql
    AS $$ 
declare
	trade_result 				record;
	summary 				trade_result_summary2_struct;
	shares_filled 				integer;
	shares_stuck 				integer;
	profit_loss 				double precision;
	--statistic missed trade 
	count_ouch_missed_trade 		integer;
	sum_ouch_expected_pl 			double precision;
	missed_trade_record_of_ouch 		record;
	str_sql 				text;
	str_sql_no 				text;
	str_sql_tmp			   	text;
	str_sql_multi_orders text;
	tmp text;
	tmp_str text;
	min_time				"time";
	max_time				"time";
	temp_time 				"time";
	t_current				"time";
	new_orders_table text;
	share_size_condition text;
  
begin	
						
	min_time := time_from;
	max_time := time_to;
	
	share_size_condition := '';	
	IF share_size <> -100 THEN 
		share_size_condition := 'expected_shares >='|| share_size ||'';
	ELSE -- Filter share size < 100 
		share_size_condition := 'expected_shares < 100' ;
	END IF;
	
	WHILE min_time < max_time LOOP
		-- Initia
		
		summary.count_miss := 0;
		summary.count_repriced := 0;
		summary.count_stuck_loss := 0;
		summary.count_stuck_profit := 0;
		summary.count_loss := 0;
		summary.count_profit_under := 0;
		summary.count_profit_above := 0;		
		
		IF (step = 5) THEN
			t_current = min_time + interval '5 minutes';
		ELSIF (step = 15) THEN
			t_current = min_time + interval '15 minutes';
			
		ELSIF (step = 30) THEN
			t_current = min_time + interval '30 minutes';
		ELSIF (step = 60) THEN
			t_current = min_time + interval '60 minutes';
		END IF;
		--raise notice '1(%)', t_current;
		IF ((t_current > max_time) or (t_current = '00:00:00')) THEN
			t_current = max_time;
			--raise notice '2(%)', t_current;
		END IF; 
		temp_time = t_current - interval '1 second';
		
		--Calculate repriced
		IF bt_shares = 0 AND sd_shares = 0 THEN
		
			--Select data from view "vwouch_missed_trades" and calculate total missed trade 	
			str_sql:='SELECT count(*) as count_missed, sum(expected_pl)as sum_expected_pl  FROM trade_results WHERE date >='''|| to_date || ''' and date <='''|| from_date ||''' and '|| share_size_condition ||' and expected_pl >=' || expected_profit ;
			str_sql := str_sql || ' and bought_shares = 0 and sold_shares = 0 ';
			
			--filter by ts when we get data from view trade_results
			IF ts_id > -1 THEN
				str_sql:= str_sql || ' and ts_id =' || ts_id;
			ELSIF ts_id = -2 THEN
				str_sql := str_sql || ' and bought_shares <> sold_shares ';
			END IF;
			
			IF cross_size <> '' then
				str_sql := str_sql || ' and ' || cross_size;
			END IF;
			
			str_sql_no :='SELECT (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + cross_id) as tmpcol FROM new_orders WHERE date >='''|| to_date || ''' and date <='''|| from_date ||'''';
			str_sql_no :=str_sql_no || ' and time_stamp between ''' || min_time || ''' and '''|| temp_time || '''';
			str_sql_no :=str_sql_no || ' and ecn =''OUCH'' and price != ack_price ';
			
			--filter by ts, ecn_launch when we get data from table new_orders
			IF ts_id > -1 THEN
				str_sql_no:=str_sql_no || ' and ts_id =' || ts_id;
			END IF;
			IF cecn <> '0' THEN
				str_sql_no:=str_sql_no || ' and ecn_launch =''' || cecn ||'''';
			END IF;
			
			IF ecn_filter = 'arcaen' THEN
				str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Entry''' ;
			ELSIF ecn_filter = 'arcaex' THEN
				str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Exit''' ;
			ELSIF ecn_filter = 'ouchen' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Entry''';
			ELSIF ecn_filter = 'ouchex' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Exit''';
			ELSIF ecn_filter = 'rashen' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Entry''';
			ELSIF ecn_filter = 'rashex' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Exit''';		
			ELSIF ecn_filter = 'batzen' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Entry''';	
			ELSIF ecn_filter = 'batzex' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Exit''';	
			ELSIF ecn_filter = 'oubxen' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''OUBX'' and entry_exit=''Entry''';
			ELSIF ecn_filter = 'oubxex' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''OUBX'' and entry_exit=''Exit''';
			ELSIF ecn_filter = 'rabxen' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''RABX'' and entry_exit=''Entry''';
			ELSIF ecn_filter = 'rabxex' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''RABX'' and entry_exit=''Exit''';
			ELSIF ecn_filter = 'nyseen' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Entry''';
			ELSIF ecn_filter = 'nyseex' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Exit''';
			ELSIF ecn_filter = 'edgxen' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''EDGX'' and entry_exit=''Entry''';
			ELSIF ecn_filter = 'edgxex' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''EDGX'' and entry_exit=''Exit''';
			END IF;

			IF symb <>'' THEN
				str_sql:=str_sql || ' and symbol='''|| symb ||'''';
				str_sql_no:=str_sql_no || ' and symbol='''|| symb ||'''';
			END IF;
			--Filter by ISO Odrer
			IF is_iso = '1' AND from_date > '2010-01-01' THEN
				str_sql_no:=str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
			END IF;
			-- tmp_str := str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
	
			IF is_iso = '2' AND from_date > '2010-01-01' THEN
				str_sql_no:=str_sql_no || ' and (is_iso <> ''Y'')';
			END IF;
			
			str_sql_no:=str_sql_no || ' GROUP BY (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + cross_id)' ;

			IF bt_shares > 0 THEN
				str_sql:=str_sql || ' and bought_shares >=' || bt_shares;
			END IF;
			IF sd_shares > 0 THEN
				str_sql:=str_sql || ' and sold_shares >=' || sd_shares;
			END IF;

			str_sql:=str_sql || ' and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) in ('|| str_sql_no ||')';
			
			IF ecn_filter <> '0' AND exclusive_val = 1 THEN
				str_sql_multi_orders := get_query_string_array_id_multi_orders(to_date, from_date);
				str_sql_tmp := get_mutiple_exchanges_trade(to_date, from_date); 
				str_sql :=str_sql || ' and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) not in (' || str_sql_multi_orders || ')'|| ' and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) not in (' || str_sql_tmp || ')';
			END IF;
			
			for missed_trade_record_of_ouch in EXECUTE str_sql loop
				count_ouch_missed_trade := missed_trade_record_of_ouch.count_missed;
				sum_ouch_expected_pl := missed_trade_record_of_ouch.sum_expected_pl;
			end loop;

			IF count_ouch_missed_trade > 0 THEN
				summary.count_repriced := count_ouch_missed_trade;				
				summary.count_miss := -count_ouch_missed_trade;				
			END IF;

		END IF;

		--filter by  ECN by Entry/Exit
		new_orders_table := '(SELECT n1.date, n1.ts_id, n1.cross_id, n1.ecn, n1.time_stamp, n1.price, n1.ack_price, n1.status, n1.entry_exit, n1.left_shares, n1.shares, n1.is_iso, n2.ecn_launch ' ||
						'FROM new_orders n1 '
						'LEFT JOIN (SELECT DISTINCT date, ts_id, cross_id, ecn_launch '
						'			FROM new_orders '
						'			WHERE entry_exit = ''Entry'' AND ts_id <> -1 AND time_stamp between ''' || min_time || ''' AND '''|| temp_time || ''') n2 '
						'ON n1.date = n2.date AND n1.ts_id = n2.ts_id AND n1.cross_id = n2.cross_id) AS tmp ';
						
		str_sql:='SELECT * FROM trade_results WHERE date >='''|| to_date || ''' and date <='''|| from_date ||''' and '|| share_size_condition ||' and expected_pl >=' || expected_profit ;
		str_sql_no :='SELECT (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + cross_id) as tmpcol FROM ' || new_orders_table || ' WHERE date >='''|| to_date || ''' and date <='''|| from_date ||'''';
		str_sql_no :=str_sql_no || ' and time_stamp between ''' || min_time || ''' and '''|| temp_time || '''';
		
		--filter by ts, ecn launch
		IF ts_id > -1 THEN
			str_sql_no:=str_sql_no || ' and ts_id =' || ts_id;
		END IF;
		
		IF cecn <> '0' THEN
			str_sql_no:=str_sql_no || ' and ecn_launch =''' || cecn ||'''';
		END IF;
		
		IF ecn_filter = 'arcaen' THEN
			str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Entry''' ;
		ELSIF ecn_filter = 'arcaex' THEN
			str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Exit''' ;
		ELSIF ecn_filter = 'ouchen' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Entry''';
		ELSIF ecn_filter = 'ouchex' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Exit''';
		ELSIF ecn_filter = 'rashen' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Entry''';
		ELSIF ecn_filter = 'rashex' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Exit''';		
		ELSIF ecn_filter = 'batzen' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Entry''';	
		ELSIF ecn_filter = 'batzex' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Exit''';	
		ELSIF ecn_filter = 'nyseen' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Entry''';
		ELSIF ecn_filter = 'nyseex' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Exit''';
		ELSIF ecn_filter = 'edgxen' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''EDGX'' and entry_exit=''Entry''';
		ELSIF ecn_filter = 'edgxex' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''EDGX'' and entry_exit=''Exit''';
		END IF;
		
		--Filter by ISO Odrer
		IF is_iso = '1' AND from_date > '2010-01-01' THEN
			str_sql_no:=str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
		END IF;
		-- tmp_str := str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
	
		IF is_iso = '2' AND from_date > '2010-01-01' THEN
			str_sql_no:=str_sql_no || ' and (is_iso <> ''Y'')';
		END IF;
		
		str_sql_no:=str_sql_no || ' GROUP BY (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + cross_id)' ;

		-- filter by Symbol, Bought Shares, Sold Shares
		IF symb <>'' THEN
			str_sql:=str_sql || ' and symbol='''|| symb ||'''';
		END IF;
		IF bt_shares > 0 THEN
			str_sql:=str_sql || ' and bought_shares >=' || bt_shares;
		END IF;
		IF sd_shares > 0 THEN
			str_sql:=str_sql || ' and sold_shares >=' || sd_shares;
		END IF;
		--filter by ts
		IF ts_id > -1 THEN 
			str_sql := str_sql || ' and ts_id =' || ts_id;
		ELSIF ts_id = -2 THEN
			str_sql := str_sql || ' and bought_shares <> sold_shares ';
		END IF;
		
		IF cross_size <> '' then
			str_sql := str_sql || ' and ' || cross_size;
		END IF;
		
		str_sql:=str_sql || ' and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) in ('|| str_sql_no ||')';
		
		IF ecn_filter <> '0' AND exclusive_val = 1 THEN
			str_sql_multi_orders := get_query_string_array_id_multi_orders(to_date, from_date);
			str_sql_tmp := get_mutiple_exchanges_trade(to_date, from_date); 
			str_sql :=str_sql || ' and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) not in (' || str_sql_multi_orders || ')'|| ' and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) not in (' || str_sql_tmp || ')';
		END IF;
		
		--calculate for trade_statistics table	
		FOR trade_result in EXECUTE str_sql loop
			shares_filled := trade_result.bought_shares;
			--RAISE NOTICE 'Variable an_integer was changed.';			
			IF (shares_filled > trade_result.sold_shares) THEN
			  shares_filled := trade_result.sold_shares;
			END IF;

			shares_stuck := trade_result.bought_shares - trade_result.sold_shares;    

			IF (shares_stuck = 0) THEN
				profit_loss := trade_result.total_sold - trade_result.total_bought;

				IF (shares_filled = 0) THEN
				summary.count_miss := summary.count_miss + 1;				
				ELSE
					IF (profit_loss < 0) THEN
						summary.count_loss := summary.count_loss + 1;						
					ELSE
						IF ( profit_loss::real < trade_result.expected_pl::real) THEN
							summary.count_profit_under := summary.count_profit_under + 1;						
						ELSE
							summary.count_profit_above := summary.count_profit_above + 1;							
						END IF;
					END IF;
				END IF;
			ELSE
				--Calculate for stuck
				IF trade_result.pm_profit_loss < -0.0001 THEN
					--stuck_loss
					summary.count_stuck_loss := summary.count_stuck_loss + 1;					
				ELSE
					--stuck_profit
					summary.count_stuck_profit := summary.count_stuck_profit + 1;				
				END IF;
			END IF;

		END LOOP;

		
		summary.time_stamp = t_current;
		min_time := t_current;	
	
		RETURN NEXT summary;
		
	END LOOP;
	RETURN;
end
$$;


ALTER FUNCTION public.cal_filter_trade_statistic_byecn2_twt2_with_step(to_date date, from_date date, time_from time without time zone, time_to time without time zone, share_size integer, expected_profit double precision, symb text, bt_shares integer, sd_shares integer, ecn_filter text, ts_id integer, cecn text, step integer, is_iso text, exclusive_val integer, cross_size text) OWNER TO postgres;


COMMIT;