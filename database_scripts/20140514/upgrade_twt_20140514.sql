BEGIN;

-- Add three columns: for table "step 5 mins"
ALTER TABLE summary_exit_order_shares ADD COLUMN ecn_launch integer;
ALTER TABLE summary_exit_order_shares ADD COLUMN entry_exit integer;
ALTER TABLE summary_exit_order_shares ADD COLUMN is_iso integer;

-- Add three columns: for table "step 15 mins"
ALTER TABLE summary_exit_order_shares_15 ADD COLUMN ecn_launch integer;
ALTER TABLE summary_exit_order_shares_15 ADD COLUMN entry_exit integer;
ALTER TABLE summary_exit_order_shares_15 ADD COLUMN is_iso integer;

-- Add three columns: for table "step 30 mins"
ALTER TABLE summary_exit_order_shares_30 ADD COLUMN ecn_launch integer;
ALTER TABLE summary_exit_order_shares_30 ADD COLUMN entry_exit integer;
ALTER TABLE summary_exit_order_shares_30 ADD COLUMN is_iso integer;

-- Add three columns: for table "step 60 mins"
ALTER TABLE summary_exit_order_shares_60 ADD COLUMN ecn_launch integer;
ALTER TABLE summary_exit_order_shares_60 ADD COLUMN entry_exit integer;
ALTER TABLE summary_exit_order_shares_60 ADD COLUMN is_iso integer;


--
-- Name: summary_exit_order_shares_15_all_3; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_15_all_3 ON summary_exit_order_shares_15 USING btree (date, time_stamp, ts_id, ecn_id,entry_exit);

--
-- Name: summary_exit_order_shares_30_all_3; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_30_all_3 ON summary_exit_order_shares_30 USING btree (date, time_stamp, ts_id, ecn_id, entry_exit);

--
-- Name: summary_exit_order_shares_60_all_3; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_60_all_3 ON summary_exit_order_shares_60 USING btree (date, time_stamp, ts_id, ecn_id, entry_exit);

--
-- Name: summary_exit_order_shares_all_3; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_all_3 ON summary_exit_order_shares USING btree (date, time_stamp, ts_id, ecn_id, entry_exit);

--
-- Name: process_insert_exit_order_shares(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE OR REPLACE FUNCTION process_insert_exit_order_shares() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  p_shares_filled  integer;
  p_order_size    integer;

BEGIN  
  IF (NEW.total_second%900=0) THEN  
    SELECT sum(shares_filled), sum(order_size)    
      INTO p_shares_filled, p_order_size
    FROM summary_exit_order_shares
    WHERE "date" = NEW.date 
      AND ecn_id = NEW.ecn_id 
      AND ts_id = NEW.ts_id
      AND entry_exit = NEW.entry_exit
      AND ecn_launch = NEW.ecn_launch
      AND is_iso = NEW.is_iso
      AND total_second <= NEW.total_second
      AND total_second > NEW.total_second -900;      
    
    INSERT INTO summary_exit_order_shares_15("date", time_stamp, ts_id, ecn_id, total_second, entry_exit, ecn_launch, is_iso, shares_filled, order_size)
    VALUES (NEW.date, NEW.time_stamp, NEW.ts_id, NEW.ecn_id, NEW.total_second, NEW.entry_exit,NEW.ecn_launch,NEW.is_iso, p_shares_filled, p_order_size);          
  END IF;   

  RETURN NEW;
END
$$;


ALTER FUNCTION public.process_insert_exit_order_shares() OWNER TO postgres;

--
-- Name: process_insert_exit_order_shares_15(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE OR REPLACE FUNCTION process_insert_exit_order_shares_15() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  p_shares_filled  integer;
  p_order_size    integer;

BEGIN  
  IF (NEW.total_second%1800=0) THEN  
    SELECT sum(shares_filled), sum(order_size)    
      INTO p_shares_filled, p_order_size
    FROM summary_exit_order_shares_15
    WHERE "date" = NEW.date 
      AND ecn_id = NEW.ecn_id 
      AND ts_id = NEW.ts_id
      AND entry_exit = NEW.entry_exit
      AND ecn_launch = NEW.ecn_launch
      AND is_iso = NEW.is_iso
      AND total_second <= NEW.total_second
      AND total_second > NEW.total_second -1800;      
    
    INSERT INTO summary_exit_order_shares_30("date", time_stamp, ts_id, ecn_id, total_second, entry_exit, ecn_launch, is_iso, shares_filled, order_size)
    VALUES (NEW.date, NEW.time_stamp, NEW.ts_id, NEW.ecn_id, NEW.total_second, NEW.entry_exit,NEW.ecn_launch,NEW.is_iso, p_shares_filled, p_order_size);
  END IF;   

  RETURN NEW;
END
$$;


ALTER FUNCTION public.process_insert_exit_order_shares_15() OWNER TO postgres;

--
-- Name: process_insert_exit_order_shares_30(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE OR REPLACE FUNCTION process_insert_exit_order_shares_30() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  p_shares_filled  integer;
  p_order_size    integer;

BEGIN  
  IF (NEW.total_second%3600=0) THEN  
    SELECT sum(shares_filled), sum(order_size)    
      INTO p_shares_filled, p_order_size
    FROM summary_exit_order_shares_30
    WHERE "date" = NEW.date 
      AND ecn_id = NEW.ecn_id 
      AND ts_id = NEW.ts_id
      AND entry_exit = NEW.entry_exit
      AND ecn_launch = NEW.ecn_launch
      AND is_iso = NEW.is_iso
      AND total_second <= NEW.total_second
      AND total_second > NEW.total_second -3600;      
    
    INSERT INTO summary_exit_order_shares_60("date", time_stamp, ts_id, ecn_id, total_second, entry_exit, ecn_launch, is_iso, shares_filled, order_size)
    VALUES (NEW.date, NEW.time_stamp, NEW.ts_id, NEW.ecn_id, NEW.total_second, NEW.entry_exit,NEW.ecn_launch,NEW.is_iso, p_shares_filled, p_order_size);
  END IF;   

  RETURN NEW;
END
$$;


ALTER FUNCTION public.process_insert_exit_order_shares_30() OWNER TO postgres;

COMMIT;
