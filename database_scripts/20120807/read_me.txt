This text provides information related to the database script "upgrade_20120807.sql" placed at the same directory.
When run this scipt on target database, it will:
- create a new function "_check_new_quotes"
- create 5 new views vw_ack_delays, vw_cancel_delays, vw_entry_delays, vw_fill_delays, vw_exit_delays

*The update is needed to run with new release.

How to run script:
1) To upgrade, please copy the file upgrade_20120807.sql to the EAA database host and run the command (please notice that we must run this command in the same folder with the file upgrade_20120807.sql):
	psql -U postgres -h localhost -d eaa10_201208 -f upgrade_20120807.sql

2) If you have any problems after upgrading, please copy the downgrade_201208007.sql to the EAA database host and run the command:
	psql -U postgres -h localhost -d eaa10_201208 -f downgrade_201208007.sql