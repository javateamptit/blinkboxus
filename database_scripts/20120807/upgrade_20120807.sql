-- Function: _check_new_quotes(character, integer)
CREATE OR REPLACE FUNCTION _check_new_quotes(_neworder_ecnlaunch character, _quote_ecnlaunch integer)
  RETURNS integer AS
$BODY$
DECLARE
	
BEGIN
	IF (_neworder_ecnLaunch = 'ARCA') THEN
		IF (_quote_ecnLaunch = 0) THEN
			return 1;
		ELSIF (_quote_ecnLaunch = 1) THEN
			return 0;
		ELSE
			-- Update later for other markets
			return 0;
		END IF;
	ELSIF (_neworder_ecnLaunch = 'NASDAQ') THEN
		IF (_quote_ecnLaunch = 1) THEN
			return 1;
		ELSIF (_quote_ecnLaunch = 0) THEN
			return 0;
		ELSE
			-- Update later for other markets
			return 0;
		END IF;
	ELSE
		-- Update later for ohter markets
		return 0;
	END IF;
END
$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION _check_new_quotes(character, integer) OWNER TO postgres;


-- Create vw_ack_delays
CREATE OR REPLACE VIEW vw_ack_delays AS 
 SELECT new_orders.oid, new_orders.entry_exit, new_orders.date, substr((order_acks.time_stamp - new_orders.time_stamp)::text, 8, 9)::double precision * 1000000::double precision AS ack_delay
   FROM new_orders
   JOIN order_acks ON order_acks.oid = new_orders.oid
  WHERE new_orders.ts_id <> -1 AND new_orders.order_id <> 0
  ORDER BY new_orders.oid, new_orders.date, new_orders.ts_id, new_orders.cross_id, new_orders.order_id;

ALTER TABLE vw_ack_delays OWNER TO postgres;

-- View: vw_cancel_delays
CREATE OR REPLACE VIEW vw_cancel_delays AS 
 SELECT c.id, n.entry_exit, substr((c.time_stamp - a.time_stamp)::text, 8, 9)::double precision * 1000000::double precision AS cancel_delay, n.date
   FROM new_orders n
   JOIN order_acks a ON a.oid = n.oid
   JOIN cancels c ON c.oid = n.oid
  WHERE n.ts_id <> -1 AND n.order_id <> 0
  ORDER BY c.id, n.ts_id, n.cross_id, n.date, c.time_stamp;

ALTER TABLE vw_cancel_delays OWNER TO postgres;

-- View: vw_entry_delays
CREATE OR REPLACE VIEW vw_entry_delays AS 
 SELECT DISTINCT new_orders.oid, new_orders.time_stamp, split_part(raw_crosses.raw_content::text, ' '::text, 8) AS quote_time_stamp, raw_crosses.date
   FROM raw_crosses
   LEFT JOIN new_orders ON raw_crosses.date = new_orders.date AND raw_crosses.ts_id = new_orders.ts_id AND raw_crosses.ts_cid = new_orders.cross_id
  WHERE raw_crosses.trade_id > 0 AND raw_crosses.raw_content ~~ '3%'::text AND new_orders.order_id <> 0 AND new_orders.ts_id <> -1 AND _check_new_quotes(new_orders.ecn_launch, split_part(raw_crosses.raw_content::text, ' '::text, 6)::integer) = 1 AND new_orders.entry_exit = 'Entry'::bpchar
  ORDER BY new_orders.oid, raw_crosses.date, split_part(raw_crosses.raw_content::text, ' '::text, 8), new_orders.time_stamp;

ALTER TABLE vw_entry_delays OWNER TO postgres;

-- View: vw_fill_delays
CREATE OR REPLACE VIEW vw_fill_delays AS 
 SELECT n.ts_id, n.cross_id, n.order_id, f.fill_id, n.entry_exit, a.time_stamp AS ack_time_stamp, f.time_stamp AS fill_time_stamp, substr((f.time_stamp - a.time_stamp)::text, 8, 9)::double precision * 1000000::double precision AS fill_delay, n.date
   FROM new_orders n
   JOIN order_acks a ON a.oid = n.oid
   JOIN fills f ON f.oid = n.oid
  WHERE n.ts_id <> -1 AND n.order_id <> 0
  ORDER BY f.fill_id, n.ts_id, n.cross_id, n.date, f.time_stamp;

ALTER TABLE vw_fill_delays OWNER TO postgres;

-- View: vw_exit_delays
CREATE OR REPLACE VIEW vw_exit_delays AS 
 SELECT DISTINCT new_orders.oid, substr((new_orders.time_stamp - min(vw_fill_delays.fill_time_stamp))::text, 8, 9)::double precision * 1000000::double precision AS exit_delay, new_orders.date
   FROM vw_fill_delays
   LEFT JOIN new_orders ON vw_fill_delays.date = new_orders.date AND vw_fill_delays.ts_id = new_orders.ts_id AND vw_fill_delays.cross_id = new_orders.cross_id
  WHERE new_orders.order_id <> 0 AND new_orders.ts_id <> -1 AND vw_fill_delays.entry_exit = 'Entry'::bpchar AND new_orders.entry_exit = 'Exit'::bpchar
  GROUP BY new_orders.oid, new_orders.date, new_orders.cross_id, new_orders.order_id, new_orders.entry_exit, new_orders.side, new_orders.time_stamp, new_orders.ts_id
  ORDER BY new_orders.oid, new_orders.date, substr((new_orders.time_stamp - min(vw_fill_delays.fill_time_stamp))::text, 8, 9)::double precision * 1000000::double precision;

ALTER TABLE vw_exit_delays OWNER TO postgres;

