--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'SQL_ASCII';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: clear_database(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION clear_database() RETURNS integer
    LANGUAGE plpgsql
    AS $$
BEGIN
  DELETE FROM summary_ack_fills;
  DELETE FROM summary_ack_fills_15;
  DELETE FROM summary_ack_fills_30;
  DELETE FROM summary_ack_fills_60;

  DELETE FROM summary_entry_exits;
  DELETE FROM summary_entry_exits_15;
  DELETE FROM summary_entry_exits_30;
  DELETE FROM summary_entry_exits_60;

  DELETE FROM summary_launch_entry_ecns;
  DELETE FROM summary_launch_entry_ecns_15;
  DELETE FROM summary_launch_entry_ecns_30;
  DELETE FROM summary_launch_entry_ecns_60;
  
  DELETE FROM summary_launch_exit_ecns;
  DELETE FROM summary_launch_exit_ecns_15;
  DELETE FROM summary_launch_exit_ecns_30;
  DELETE FROM summary_launch_exit_ecns_60;
    
  DELETE FROM summary_order_types;
  DELETE FROM summary_order_types_15;
  DELETE FROM summary_order_types_30;
  DELETE FROM summary_order_types_60;

  DELETE FROM summary_profit_losses;
  
  RETURN 0;
END
$$;


ALTER FUNCTION public.clear_database() OWNER TO postgres;

--
-- Name: clear_database_with_date(date); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION clear_database_with_date(_pdate date) RETURNS integer
    LANGUAGE plpgsql
    AS $$
BEGIN
  DELETE FROM summary_ack_fills WHERE date = _pdate;
  DELETE FROM summary_ack_fills_15 WHERE date = _pdate;
  DELETE FROM summary_ack_fills_30 WHERE date = _pdate;
  DELETE FROM summary_ack_fills_60 WHERE date = _pdate;

  DELETE FROM summary_entry_exits WHERE date = _pdate;
  DELETE FROM summary_entry_exits_15 WHERE date = _pdate;
  DELETE FROM summary_entry_exits_30 WHERE date = _pdate;
  DELETE FROM summary_entry_exits_60 WHERE date = _pdate;

  DELETE FROM summary_launch_entry_ecns WHERE date = _pdate;
  DELETE FROM summary_launch_entry_ecns_15 WHERE date = _pdate;
  DELETE FROM summary_launch_entry_ecns_30 WHERE date = _pdate;
  DELETE FROM summary_launch_entry_ecns_60 WHERE date = _pdate;

  DELETE FROM summary_order_types WHERE date = _pdate;
  DELETE FROM summary_order_types_15 WHERE date = _pdate;
  DELETE FROM summary_order_types_30 WHERE date = _pdate;
  DELETE FROM summary_order_types_60 WHERE date = _pdate;

  DELETE FROM summary_profit_losses WHERE date = _pdate;
  
  RETURN 0;
END
$$;


ALTER FUNCTION public.clear_database_with_date(_pdate date) OWNER TO postgres;

--
-- Name: process_insert_ack_fills(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION process_insert_ack_fills() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  p_total_time_ack  integer;
  p_ack_count    integer;
  p_total_time_fill  integer;
  p_fill_count    integer;

BEGIN  
  IF (NEW.total_second%900=0) THEN  
    SELECT sum(total_time_ack), sum(ack_count), sum(total_time_fill), sum(fill_count)      
      INTO p_total_time_ack, p_ack_count, p_total_time_fill, p_fill_count
    FROM summary_ack_fills
    WHERE "date" = NEW.date 
      AND ecn_id = NEW.ecn_id 
      AND ts_id = NEW.ts_id
      AND total_second <= NEW.total_second
      AND total_second > NEW.total_second -900;      
    
    INSERT INTO summary_ack_fills_15("date", time_stamp, ts_id, ecn_id, total_second, total_time_ack, ack_count, total_time_fill, fill_count)
    VALUES (NEW.date, NEW.time_stamp, NEW.ts_id, NEW.ecn_id, NEW.total_second, p_total_time_ack, p_ack_count, p_total_time_fill, p_fill_count );          
  END IF;   

  RETURN NEW;
END
$$;


ALTER FUNCTION public.process_insert_ack_fills() OWNER TO postgres;

--
-- Name: process_insert_ack_fills_15(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION process_insert_ack_fills_15() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  p_total_time_ack  integer;
  p_ack_count    integer;
  p_total_time_fill  integer;
  p_fill_count    integer;

BEGIN  
  IF (NEW.total_second%1800=0) THEN  
    SELECT sum(total_time_ack), sum(ack_count), sum(total_time_fill), sum(fill_count)      
      INTO p_total_time_ack, p_ack_count, p_total_time_fill, p_fill_count
    FROM summary_ack_fills_15
    WHERE "date" = NEW.date 
      AND ecn_id = NEW.ecn_id 
      AND ts_id = NEW.ts_id
      AND total_second <= NEW.total_second
      AND total_second > NEW.total_second -1800;
    
    INSERT INTO summary_ack_fills_30("date", time_stamp, ts_id, ecn_id, total_second, total_time_ack, ack_count, total_time_fill, fill_count)
    VALUES (NEW.date, NEW.time_stamp, NEW.ts_id, NEW.ecn_id, NEW.total_second, p_total_time_ack, p_ack_count, p_total_time_fill, p_fill_count );          
  END IF;   

  RETURN NEW;
END
$$;


ALTER FUNCTION public.process_insert_ack_fills_15() OWNER TO postgres;

--
-- Name: process_insert_ack_fills_30(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION process_insert_ack_fills_30() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  p_total_time_ack  integer;
  p_ack_count    integer;
  p_total_time_fill  integer;
  p_fill_count    integer;

BEGIN  
  IF (NEW.total_second%3600=0) THEN  
    SELECT sum(total_time_ack), sum(ack_count), sum(total_time_fill), sum(fill_count)      
      INTO p_total_time_ack, p_ack_count, p_total_time_fill, p_fill_count
    FROM summary_ack_fills_30
    WHERE "date" = NEW.date 
      AND ecn_id = NEW.ecn_id 
      AND ts_id = NEW.ts_id
      AND total_second <= NEW.total_second
      AND total_second > NEW.total_second -3600;
    
    INSERT INTO summary_ack_fills_60("date", time_stamp, ts_id, ecn_id, total_second, total_time_ack, ack_count, total_time_fill, fill_count)
    VALUES (NEW.date, NEW.time_stamp, NEW.ts_id, NEW.ecn_id, NEW.total_second, p_total_time_ack, p_ack_count, p_total_time_fill, p_fill_count );          
  END IF;   

  RETURN NEW;
END
$$;


ALTER FUNCTION public.process_insert_ack_fills_30() OWNER TO postgres;

--
-- Name: process_insert_entry_exits(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION process_insert_entry_exits() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
total_entry_filled integer;
total_entry integer;
total_exit_filled integer;
total_exit integer;

BEGIN
IF (NEW.total_second%900=0) THEN
SELECT sum(entry_filled), sum(entry_total), sum(exit_filled), sum(exit_total)
INTO total_entry_filled, total_entry, total_exit_filled, total_exit
FROM summary_entry_exits
WHERE "date" = NEW.date
AND ts_id = NEW.ts_id
AND ecn_id = NEW.ecn_id
AND side = NEW.side
AND total_second <= NEW.total_second
AND total_second > NEW.total_second -900;

INSERT INTO summary_entry_exits_15("date", time_stamp, ts_id, ecn_id, total_second, entry_filled, entry_total, exit_filled, exit_total, side) 
VALUES (NEW.date, NEW.time_stamp, NEW.ts_id, NEW.ecn_id, NEW.total_second, total_entry_filled, total_entry, total_exit_filled, total_exit, NEW.side);
END IF;

RETURN NEW;
END
$$;


ALTER FUNCTION public.process_insert_entry_exits() OWNER TO postgres;

--
-- Name: process_insert_entry_exits_15(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION process_insert_entry_exits_15() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
total_entry_filled integer;
total_entry integer;
total_exit_filled integer;
total_exit integer;
BEGIN

IF (NEW.total_second%1800=0) THEN
SELECT sum(entry_filled), sum(entry_total), sum(exit_filled), sum(exit_total)
INTO total_entry_filled, total_entry, total_exit_filled, total_exit
FROM summary_entry_exits_15
WHERE "date" = NEW.date
AND ts_id = NEW.ts_id
AND ecn_id = NEW.ecn_id
AND side = NEW.side
AND total_second <= NEW.total_second
AND total_second > NEW.total_second -1800;

INSERT INTO summary_entry_exits_30("date", time_stamp, ts_id, ecn_id, total_second, entry_filled, entry_total, exit_filled, exit_total, side)
VALUES (NEW.date, NEW.time_stamp, NEW.ts_id, NEW.ecn_id, NEW.total_second, total_entry_filled, total_entry, total_exit_filled, total_exit, NEW.side);
END IF;

RETURN NEW;
END
$$;


ALTER FUNCTION public.process_insert_entry_exits_15() OWNER TO postgres;

--
-- Name: process_insert_entry_exits_30(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION process_insert_entry_exits_30() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
total_entry_filled integer;
total_entry integer;
total_exit_filled integer;
total_exit integer;
BEGIN

IF (NEW.total_second%3600=0) THEN
SELECT sum(entry_filled), sum(entry_total), sum(exit_filled), sum(exit_total)
INTO total_entry_filled, total_entry, total_exit_filled, total_exit
FROM summary_entry_exits_30
WHERE "date" = NEW.date
AND ts_id = NEW.ts_id
AND ecn_id = NEW.ecn_id
AND side = NEW.side
AND total_second <= NEW.total_second
AND total_second > NEW.total_second -3600;

INSERT INTO summary_entry_exits_60("date", time_stamp, ts_id, ecn_id, total_second, entry_filled, entry_total, exit_filled, exit_total, side)
VALUES (NEW.date, NEW.time_stamp, NEW.ts_id, NEW.ecn_id, NEW.total_second, total_entry_filled, total_entry, total_exit_filled, total_exit, NEW.side);
END IF;

RETURN NEW;
END
$$;


ALTER FUNCTION public.process_insert_entry_exits_30() OWNER TO postgres;

--
-- Name: process_insert_exit_order_shares(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION process_insert_exit_order_shares() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  p_shares_filled  integer;
  p_order_size    integer;

BEGIN  
  IF (NEW.total_second%900=0) THEN  
    SELECT sum(shares_filled), sum(order_size)    
      INTO p_shares_filled, p_order_size
    FROM summary_exit_order_shares
    WHERE "date" = NEW.date 
      AND ecn_id = NEW.ecn_id 
      AND ts_id = NEW.ts_id
      AND entry_exit = NEW.entry_exit
      AND ecn_launch = NEW.ecn_launch
      AND is_iso = NEW.is_iso
      AND total_second <= NEW.total_second
      AND total_second > NEW.total_second -900;      
    
    INSERT INTO summary_exit_order_shares_15("date", time_stamp, ts_id, ecn_id, total_second, entry_exit, ecn_launch, is_iso, shares_filled, order_size)
    VALUES (NEW.date, NEW.time_stamp, NEW.ts_id, NEW.ecn_id, NEW.total_second, NEW.entry_exit,NEW.ecn_launch,NEW.is_iso, p_shares_filled, p_order_size);          
  END IF;   

  RETURN NEW;
END
$$;


ALTER FUNCTION public.process_insert_exit_order_shares() OWNER TO postgres;

--
-- Name: process_insert_exit_order_shares_15(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION process_insert_exit_order_shares_15() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  p_shares_filled  integer;
  p_order_size    integer;

BEGIN  
  IF (NEW.total_second%1800=0) THEN  
    SELECT sum(shares_filled), sum(order_size)    
      INTO p_shares_filled, p_order_size
    FROM summary_exit_order_shares_15
    WHERE "date" = NEW.date 
      AND ecn_id = NEW.ecn_id 
      AND ts_id = NEW.ts_id
      AND entry_exit = NEW.entry_exit
      AND ecn_launch = NEW.ecn_launch
      AND is_iso = NEW.is_iso
      AND total_second <= NEW.total_second
      AND total_second > NEW.total_second -1800;      
    
    INSERT INTO summary_exit_order_shares_30("date", time_stamp, ts_id, ecn_id, total_second, entry_exit, ecn_launch, is_iso, shares_filled, order_size)
    VALUES (NEW.date, NEW.time_stamp, NEW.ts_id, NEW.ecn_id, NEW.total_second, NEW.entry_exit,NEW.ecn_launch,NEW.is_iso, p_shares_filled, p_order_size);
  END IF;   

  RETURN NEW;
END
$$;


ALTER FUNCTION public.process_insert_exit_order_shares_15() OWNER TO postgres;

--
-- Name: process_insert_exit_order_shares_30(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION process_insert_exit_order_shares_30() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  p_shares_filled  integer;
  p_order_size    integer;

BEGIN  
  IF (NEW.total_second%3600=0) THEN  
    SELECT sum(shares_filled), sum(order_size)    
      INTO p_shares_filled, p_order_size
    FROM summary_exit_order_shares_30
    WHERE "date" = NEW.date 
      AND ecn_id = NEW.ecn_id 
      AND ts_id = NEW.ts_id
      AND entry_exit = NEW.entry_exit
      AND ecn_launch = NEW.ecn_launch
      AND is_iso = NEW.is_iso
      AND total_second <= NEW.total_second
      AND total_second > NEW.total_second -3600;      
    
    INSERT INTO summary_exit_order_shares_60("date", time_stamp, ts_id, ecn_id, total_second, entry_exit, ecn_launch, is_iso, shares_filled, order_size)
    VALUES (NEW.date, NEW.time_stamp, NEW.ts_id, NEW.ecn_id, NEW.total_second, NEW.entry_exit,NEW.ecn_launch,NEW.is_iso, p_shares_filled, p_order_size);
  END IF;   

  RETURN NEW;
END
$$;


ALTER FUNCTION public.process_insert_exit_order_shares_30() OWNER TO postgres;

--
-- Name: process_insert_launch_entry_ecns(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION process_insert_launch_entry_ecns() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  i_filled    integer;
  i_total      integer;  
  i_ask_filled    integer;
  i_ask_total    integer;
  i_bid_filled    integer;
  i_bid_total    integer;

BEGIN  
  IF (NEW.total_second%900=0) THEN  
    SELECT  sum(filled), sum(total), sum(ask_filled), sum(ask_total), sum(bid_filled), sum(bid_total)
      INTO  i_filled, i_total, i_ask_filled, i_ask_total, i_bid_filled, i_bid_total
    FROM summary_launch_entry_ecns
    WHERE "date" = NEW.date
      AND ts_id = NEW.ts_id 
      AND ecn_launch_id = NEW.ecn_launch_id 
      AND ecn_entry_id = NEW.ecn_entry_id 
      AND side = NEW.side
      AND total_second <= NEW.total_second
      AND total_second > NEW.total_second -900;      

    INSERT INTO summary_launch_entry_ecns_15("date", time_stamp, ts_id, ecn_launch_id, ecn_entry_id, total_second, filled, total, ask_filled, ask_total, bid_filled, bid_total,side)
    VALUES (NEW.date, NEW.time_stamp, NEW.ts_id, NEW.ecn_launch_id, NEW.ecn_entry_id, NEW.total_second,  i_filled, i_total, i_ask_filled, i_ask_total, i_bid_filled, i_bid_total,NEW.side);            
  END IF;   

  RETURN NEW;
END
$$;


ALTER FUNCTION public.process_insert_launch_entry_ecns() OWNER TO postgres;

--
-- Name: process_insert_launch_entry_ecns_15(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION process_insert_launch_entry_ecns_15() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  i_filled    integer;
  i_total      integer;  
  i_ask_filled    integer;
  i_ask_total    integer;
  i_bid_filled    integer;
  i_bid_total    integer;

BEGIN  
  IF (NEW.total_second%1800=0) THEN  

    SELECT  sum(filled), sum(total), sum(ask_filled), sum(ask_total), sum(bid_filled), sum(bid_total)
      INTO  i_filled, i_total, i_ask_filled, i_ask_total, i_bid_filled, i_bid_total
    FROM summary_launch_entry_ecns_15
    WHERE "date" = NEW.date
      AND ts_id = NEW.ts_id 
      AND ecn_launch_id = NEW.ecn_launch_id 
      AND ecn_entry_id = NEW.ecn_entry_id 
      AND side = NEW.side
      AND total_second <= NEW.total_second
      AND total_second > NEW.total_second -1800;      

    INSERT INTO summary_launch_entry_ecns_30("date", time_stamp, ts_id, ecn_launch_id, ecn_entry_id, total_second, filled, total, ask_filled, ask_total, bid_filled, bid_total,side)
    VALUES (NEW.date, NEW.time_stamp, NEW.ts_id, NEW.ecn_launch_id, NEW.ecn_entry_id, NEW.total_second,  i_filled, i_total, i_ask_filled, i_ask_total, i_bid_filled, i_bid_total,NEW.side);            
  END IF;   

  RETURN NEW;
END
$$;


ALTER FUNCTION public.process_insert_launch_entry_ecns_15() OWNER TO postgres;

--
-- Name: process_insert_launch_entry_ecns_30(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION process_insert_launch_entry_ecns_30() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  i_filled    integer;
  i_total      integer;  
  i_ask_filled    integer;
  i_ask_total    integer;
  i_bid_filled    integer;
  i_bid_total    integer;

BEGIN  
  IF (NEW.total_second%3600=0) THEN  

    SELECT  sum(filled), sum(total), sum(ask_filled), sum(ask_total), sum(bid_filled), sum(bid_total)
      INTO  i_filled, i_total, i_ask_filled, i_ask_total, i_bid_filled, i_bid_total
    FROM summary_launch_entry_ecns_30
    WHERE "date" = NEW.date
      AND ts_id = NEW.ts_id 
      AND ecn_launch_id = NEW.ecn_launch_id 
      AND ecn_entry_id = NEW.ecn_entry_id 
      AND side = NEW.side
      AND total_second <= NEW.total_second
      AND total_second > NEW.total_second - 3600;      

    INSERT INTO summary_launch_entry_ecns_60("date", time_stamp, ts_id, ecn_launch_id, ecn_entry_id, total_second, filled, total, ask_filled, ask_total, bid_filled, bid_total,side)
    VALUES (NEW.date, NEW.time_stamp, NEW.ts_id, NEW.ecn_launch_id, NEW.ecn_entry_id, NEW.total_second,  i_filled, i_total, i_ask_filled, i_ask_total, i_bid_filled, i_bid_total,NEW.side);            
  END IF;   

  RETURN NEW;
END
$$;


ALTER FUNCTION public.process_insert_launch_entry_ecns_30() OWNER TO postgres;

--
-- Name: process_insert_launch_exit_ecns(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION process_insert_launch_exit_ecns() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  i_filled    integer;
  i_total      integer;  
  i_ask_filled    integer;
  i_ask_total    integer;
  i_bid_filled    integer;
  i_bid_total    integer;

BEGIN  
  IF (NEW.total_second%900=0) THEN  
    SELECT  sum(filled), sum(total), sum(ask_filled), sum(ask_total), sum(bid_filled), sum(bid_total)
      INTO  i_filled, i_total, i_ask_filled, i_ask_total, i_bid_filled, i_bid_total
    FROM summary_launch_exit_ecns
    WHERE "date" = NEW.date
      AND ts_id = NEW.ts_id 
      AND ecn_launch_id = NEW.ecn_launch_id 
      AND ecn_exit_id = NEW.ecn_exit_id 
      AND side = NEW.side
      AND total_second <= NEW.total_second
      AND total_second > NEW.total_second -900;      

    INSERT INTO summary_launch_exit_ecns_15("date", time_stamp, ts_id, ecn_launch_id, ecn_exit_id, total_second, filled, total, ask_filled, ask_total, bid_filled, bid_total,side)
    VALUES (NEW.date, NEW.time_stamp, NEW.ts_id, NEW.ecn_launch_id, NEW.ecn_exit_id, NEW.total_second,  i_filled, i_total, i_ask_filled, i_ask_total, i_bid_filled, i_bid_total,NEW.side);            
  END IF;   

  RETURN NEW;
END
$$;


ALTER FUNCTION public.process_insert_launch_exit_ecns() OWNER TO postgres;

--
-- Name: process_insert_launch_exit_ecns_15(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION process_insert_launch_exit_ecns_15() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  i_filled    integer;
  i_total      integer;  
  i_ask_filled    integer;
  i_ask_total    integer;
  i_bid_filled    integer;
  i_bid_total    integer;

BEGIN  
  IF (NEW.total_second%1800=0) THEN  

    SELECT  sum(filled), sum(total), sum(ask_filled), sum(ask_total), sum(bid_filled), sum(bid_total)
      INTO  i_filled, i_total, i_ask_filled, i_ask_total, i_bid_filled, i_bid_total
    FROM summary_launch_exit_ecns_15
    WHERE "date" = NEW.date
      AND ts_id = NEW.ts_id 
      AND ecn_launch_id = NEW.ecn_launch_id 
      AND ecn_exit_id = NEW.ecn_exit_id 
      AND side = NEW.side
      AND total_second <= NEW.total_second
      AND total_second > NEW.total_second -1800;      

    INSERT INTO summary_launch_exit_ecns_30("date", time_stamp, ts_id, ecn_launch_id, ecn_exit_id, total_second, filled, total, ask_filled, ask_total, bid_filled, bid_total,side)
    VALUES (NEW.date, NEW.time_stamp, NEW.ts_id, NEW.ecn_launch_id, NEW.ecn_exit_id, NEW.total_second,  i_filled, i_total, i_ask_filled, i_ask_total, i_bid_filled, i_bid_total,NEW.side);            
  END IF;   

  RETURN NEW;
END
$$;


ALTER FUNCTION public.process_insert_launch_exit_ecns_15() OWNER TO postgres;

--
-- Name: process_insert_launch_exit_ecns_30(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION process_insert_launch_exit_ecns_30() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  i_filled    integer;
  i_total      integer;  
  i_ask_filled    integer;
  i_ask_total    integer;
  i_bid_filled    integer;
  i_bid_total    integer;

BEGIN  
  IF (NEW.total_second%3600=0) THEN  

    SELECT  sum(filled), sum(total), sum(ask_filled), sum(ask_total), sum(bid_filled), sum(bid_total)
      INTO  i_filled, i_total, i_ask_filled, i_ask_total, i_bid_filled, i_bid_total
    FROM summary_launch_exit_ecns_30
    WHERE "date" = NEW.date
      AND ts_id = NEW.ts_id 
      AND ecn_launch_id = NEW.ecn_launch_id 
      AND ecn_exit_id = NEW.ecn_exit_id 
      AND side = NEW.side
      AND total_second <= NEW.total_second
      AND total_second > NEW.total_second - 3600;      

    INSERT INTO summary_launch_exit_ecns_60("date", time_stamp, ts_id, ecn_launch_id, ecn_exit_id, total_second, filled, total, ask_filled, ask_total, bid_filled, bid_total,side)
    VALUES (NEW.date, NEW.time_stamp, NEW.ts_id, NEW.ecn_launch_id, NEW.ecn_exit_id, NEW.total_second,  i_filled, i_total, i_ask_filled, i_ask_total, i_bid_filled, i_bid_total,NEW.side);            
  END IF;   

  RETURN NEW;
END
$$;


ALTER FUNCTION public.process_insert_launch_exit_ecns_30() OWNER TO postgres;

--
-- Name: process_insert_order_types(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION process_insert_order_types() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
total_buy_filled integer;
total_buy integer;
total_sell_filled integer;
total_sell integer;
total_shortsell_filled integer;
total_shortsell integer;
BEGIN
IF (NEW.total_second%900=0) THEN
SELECT sum(buy_filled), sum(buy_total), sum(sell_filled), sum(sell_total), sum(shortsell_filled), sum(shortsell_total)
INTO total_buy_filled, total_buy, total_sell_filled, total_sell, total_shortsell_filled, total_shortsell
FROM summary_order_types
WHERE "date" = NEW.date
AND ts_id = NEW.ts_id 
AND ecn_id= NEW.ecn_id
AND isentry_exit = NEW.isentry_exit
AND total_second <= NEW.total_second
AND total_second > NEW.total_second -900;
INSERT INTO summary_order_types_15("date", time_stamp, ts_id, ecn_id, total_second, 
buy_filled, buy_total, sell_filled, sell_total, shortsell_filled, shortsell_total, isentry_exit)
VALUES (NEW.date, NEW.time_stamp, NEW.ts_id, NEW.ecn_id, NEW.total_second, 
total_buy_filled,total_buy, total_sell_filled, total_sell, total_shortsell_filled, total_shortsell, NEW.isentry_exit);
END IF;
RETURN NEW;
END
$$;


ALTER FUNCTION public.process_insert_order_types() OWNER TO postgres;

--
-- Name: process_insert_order_types_15(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION process_insert_order_types_15() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
total_buy_filled integer;
total_buy integer;
total_sell_filled integer;
total_sell integer;
total_shortsell_filled integer;
total_shortsell integer;
BEGIN
IF (NEW.total_second%1800=0) THEN
SELECT sum(buy_filled), sum(buy_total), sum(sell_filled), sum(sell_total), sum(shortsell_filled), sum(shortsell_total)
INTO total_buy_filled, total_buy, total_sell_filled, total_sell, total_shortsell_filled, total_shortsell
FROM summary_order_types_15
WHERE "date" = NEW.date
AND ts_id = NEW.ts_id 
AND ecn_id=NEW.ecn_id
AND isentry_exit = NEW.isentry_exit
AND total_second <= NEW.total_second
AND total_second > NEW.total_second -1800;
INSERT INTO summary_order_types_30("date", time_stamp, ts_id, ecn_id, total_second, 
buy_filled, buy_total, sell_filled, sell_total, shortsell_filled, shortsell_total, isentry_exit)
VALUES (NEW.date, NEW.time_stamp, NEW.ts_id, NEW.ecn_id, NEW.total_second, 
total_buy_filled,total_buy, total_sell_filled, total_sell, total_shortsell_filled, total_shortsell, NEW.isentry_exit);
END IF; 
RETURN NEW;
END
$$;


ALTER FUNCTION public.process_insert_order_types_15() OWNER TO postgres;

--
-- Name: process_insert_order_types_30(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION process_insert_order_types_30() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
total_buy_filled integer;
total_buy integer;
total_sell_filled integer;
total_sell integer;
total_shortsell_filled integer;
total_shortsell integer;
BEGIN
IF (NEW.total_second%3600=0) THEN
SELECT sum(buy_filled), sum(buy_total), sum(sell_filled), sum(sell_total), sum(shortsell_filled), sum(shortsell_total)
INTO total_buy_filled, total_buy, total_sell_filled, total_sell, total_shortsell_filled, total_shortsell
FROM summary_order_types_30
WHERE "date" = NEW.date
AND ts_id = NEW.ts_id 
AND ecn_id=NEW.ecn_id
AND isentry_exit = NEW.isentry_exit
AND total_second <= NEW.total_second
AND total_second > NEW.total_second -3600;
INSERT INTO summary_order_types_60("date", time_stamp, ts_id, ecn_id, total_second, 
buy_filled, buy_total, sell_filled, sell_total, shortsell_filled, shortsell_total, isentry_exit)
VALUES (NEW.date, NEW.time_stamp, NEW.ts_id, NEW.ecn_id, NEW.total_second, 
total_buy_filled,total_buy, total_sell_filled, total_sell, total_shortsell_filled, total_shortsell, NEW.isentry_exit);
END IF;
RETURN NEW;
END
$$;


ALTER FUNCTION public.process_insert_order_types_30() OWNER TO postgres;

--
-- Name: summary_ack_fills_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE summary_ack_fills_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.summary_ack_fills_id_seq OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: summary_ack_fills; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE summary_ack_fills (
    id integer DEFAULT nextval('summary_ack_fills_id_seq'::regclass) NOT NULL,
    date date NOT NULL,
    time_stamp time without time zone NOT NULL,
    total_second integer,
    ts_id integer,
    ecn_id integer,
    total_time_ack double precision,
    ack_count integer,
    total_time_fill double precision,
    fill_count integer
);


ALTER TABLE public.summary_ack_fills OWNER TO postgres;

--
-- Name: summary_ack_fills_15_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE summary_ack_fills_15_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.summary_ack_fills_15_id_seq OWNER TO postgres;

--
-- Name: summary_ack_fills_15; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE summary_ack_fills_15 (
    id integer DEFAULT nextval('summary_ack_fills_15_id_seq'::regclass) NOT NULL,
    date date NOT NULL,
    time_stamp time without time zone NOT NULL,
    total_second integer,
    ts_id integer,
    ecn_id integer,
    total_time_ack double precision,
    ack_count integer,
    total_time_fill double precision,
    fill_count integer
);


ALTER TABLE public.summary_ack_fills_15 OWNER TO postgres;

--
-- Name: summary_ack_fills_30_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE summary_ack_fills_30_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.summary_ack_fills_30_id_seq OWNER TO postgres;

--
-- Name: summary_ack_fills_30; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE summary_ack_fills_30 (
    id integer DEFAULT nextval('summary_ack_fills_30_id_seq'::regclass) NOT NULL,
    date date NOT NULL,
    time_stamp time without time zone NOT NULL,
    total_second integer,
    ts_id integer,
    ecn_id integer,
    total_time_ack double precision,
    ack_count integer,
    total_time_fill double precision,
    fill_count integer
);


ALTER TABLE public.summary_ack_fills_30 OWNER TO postgres;

--
-- Name: summary_ack_fills_60_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE summary_ack_fills_60_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.summary_ack_fills_60_id_seq OWNER TO postgres;

--
-- Name: summary_ack_fills_60; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE summary_ack_fills_60 (
    id integer DEFAULT nextval('summary_ack_fills_60_id_seq'::regclass) NOT NULL,
    date date NOT NULL,
    time_stamp time without time zone NOT NULL,
    total_second integer,
    ts_id integer,
    ecn_id integer,
    total_time_ack double precision,
    ack_count integer,
    total_time_fill double precision,
    fill_count integer
);


ALTER TABLE public.summary_ack_fills_60 OWNER TO postgres;

--
-- Name: summary_entry_exits_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE summary_entry_exits_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.summary_entry_exits_id_seq OWNER TO postgres;

--
-- Name: summary_entry_exits; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE summary_entry_exits (
    id integer DEFAULT nextval('summary_entry_exits_id_seq'::regclass) NOT NULL,
    date date NOT NULL,
    time_stamp time without time zone NOT NULL,
    total_second integer,
    ts_id integer,
    ecn_id integer,
    entry_filled integer,
    entry_total integer,
    exit_filled integer,
    exit_total integer,
    side integer
);


ALTER TABLE public.summary_entry_exits OWNER TO postgres;

--
-- Name: summary_entry_exits_15_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE summary_entry_exits_15_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.summary_entry_exits_15_id_seq OWNER TO postgres;

--
-- Name: summary_entry_exits_15; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE summary_entry_exits_15 (
    id integer DEFAULT nextval('summary_entry_exits_15_id_seq'::regclass) NOT NULL,
    date date NOT NULL,
    time_stamp time without time zone NOT NULL,
    total_second integer,
    ts_id integer,
    ecn_id integer,
    entry_filled integer,
    entry_total integer,
    exit_filled integer,
    exit_total integer,
    side integer
);


ALTER TABLE public.summary_entry_exits_15 OWNER TO postgres;

--
-- Name: summary_entry_exits_30_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE summary_entry_exits_30_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.summary_entry_exits_30_id_seq OWNER TO postgres;

--
-- Name: summary_entry_exits_30; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE summary_entry_exits_30 (
    id integer DEFAULT nextval('summary_entry_exits_30_id_seq'::regclass) NOT NULL,
    date date NOT NULL,
    time_stamp time without time zone NOT NULL,
    total_second integer,
    ts_id integer,
    ecn_id integer,
    entry_filled integer,
    entry_total integer,
    exit_filled integer,
    exit_total integer,
    side integer
);


ALTER TABLE public.summary_entry_exits_30 OWNER TO postgres;

--
-- Name: summary_entry_exits_60_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE summary_entry_exits_60_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.summary_entry_exits_60_id_seq OWNER TO postgres;

--
-- Name: summary_entry_exits_60; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE summary_entry_exits_60 (
    id integer DEFAULT nextval('summary_entry_exits_60_id_seq'::regclass) NOT NULL,
    date date NOT NULL,
    time_stamp time without time zone NOT NULL,
    total_second integer,
    ts_id integer,
    ecn_id integer,
    entry_filled integer,
    entry_total integer,
    exit_filled integer,
    exit_total integer,
    side integer
);


ALTER TABLE public.summary_entry_exits_60 OWNER TO postgres;

--
-- Name: summary_exit_order_shares; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE summary_exit_order_shares (
    id integer NOT NULL,
    date date NOT NULL,
    time_stamp time without time zone NOT NULL,
    total_second integer,
    ecn_id integer,
    ts_id integer,
    shares_filled integer,
    order_size integer,
    ecn_launch integer,
    entry_exit integer,
    is_iso integer
);


ALTER TABLE public.summary_exit_order_shares OWNER TO postgres;

--
-- Name: summary_exit_order_shares_15; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE summary_exit_order_shares_15 (
    id integer NOT NULL,
    date date NOT NULL,
    time_stamp time without time zone NOT NULL,
    total_second integer,
    ecn_id integer,
    ts_id integer,
    shares_filled integer,
    order_size integer,
    ecn_launch integer,
    entry_exit integer,
    is_iso integer
);


ALTER TABLE public.summary_exit_order_shares_15 OWNER TO postgres;

--
-- Name: summary_exit_order_shares_15_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE summary_exit_order_shares_15_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.summary_exit_order_shares_15_id_seq OWNER TO postgres;

--
-- Name: summary_exit_order_shares_15_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE summary_exit_order_shares_15_id_seq OWNED BY summary_exit_order_shares_15.id;


--
-- Name: summary_exit_order_shares_30; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE summary_exit_order_shares_30 (
    id integer NOT NULL,
    date date NOT NULL,
    time_stamp time without time zone NOT NULL,
    total_second integer,
    ecn_id integer,
    ts_id integer,
    shares_filled integer,
    order_size integer,
    ecn_launch integer,
    entry_exit integer,
    is_iso integer
);


ALTER TABLE public.summary_exit_order_shares_30 OWNER TO postgres;

--
-- Name: summary_exit_order_shares_30_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE summary_exit_order_shares_30_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.summary_exit_order_shares_30_id_seq OWNER TO postgres;

--
-- Name: summary_exit_order_shares_30_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE summary_exit_order_shares_30_id_seq OWNED BY summary_exit_order_shares_30.id;


--
-- Name: summary_exit_order_shares_60; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE summary_exit_order_shares_60 (
    id integer NOT NULL,
    date date NOT NULL,
    time_stamp time without time zone NOT NULL,
    total_second integer,
    ecn_id integer,
    ts_id integer,
    shares_filled integer,
    order_size integer,
    ecn_launch integer,
    entry_exit integer,
    is_iso integer
);


ALTER TABLE public.summary_exit_order_shares_60 OWNER TO postgres;

--
-- Name: summary_exit_order_shares_60_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE summary_exit_order_shares_60_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.summary_exit_order_shares_60_id_seq OWNER TO postgres;

--
-- Name: summary_exit_order_shares_60_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE summary_exit_order_shares_60_id_seq OWNED BY summary_exit_order_shares_60.id;


--
-- Name: summary_exit_order_shares_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE summary_exit_order_shares_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.summary_exit_order_shares_id_seq OWNER TO postgres;

--
-- Name: summary_exit_order_shares_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE summary_exit_order_shares_id_seq OWNED BY summary_exit_order_shares.id;


--
-- Name: summary_launch_entry_ecns_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE summary_launch_entry_ecns_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.summary_launch_entry_ecns_id_seq OWNER TO postgres;

--
-- Name: summary_launch_entry_ecns; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE summary_launch_entry_ecns (
    id integer DEFAULT nextval('summary_launch_entry_ecns_id_seq'::regclass) NOT NULL,
    date date NOT NULL,
    time_stamp time without time zone NOT NULL,
    total_second integer,
    ts_id integer,
    ecn_launch_id integer,
    ecn_entry_id integer,
    filled integer,
    total integer,
    ask_filled integer,
    ask_total integer,
    bid_filled integer,
    bid_total integer,
    side integer
);


ALTER TABLE public.summary_launch_entry_ecns OWNER TO postgres;

--
-- Name: summary_launch_entry_ecns_15_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE summary_launch_entry_ecns_15_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.summary_launch_entry_ecns_15_id_seq OWNER TO postgres;

--
-- Name: summary_launch_entry_ecns_15; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE summary_launch_entry_ecns_15 (
    id integer DEFAULT nextval('summary_launch_entry_ecns_15_id_seq'::regclass) NOT NULL,
    date date NOT NULL,
    time_stamp time without time zone NOT NULL,
    total_second integer,
    ts_id integer,
    ecn_launch_id integer,
    ecn_entry_id integer,
    filled integer,
    total integer,
    ask_filled integer,
    ask_total integer,
    bid_filled integer,
    bid_total integer,
    side integer
);


ALTER TABLE public.summary_launch_entry_ecns_15 OWNER TO postgres;

--
-- Name: summary_launch_entry_ecns_30_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE summary_launch_entry_ecns_30_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.summary_launch_entry_ecns_30_id_seq OWNER TO postgres;

--
-- Name: summary_launch_entry_ecns_30; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE summary_launch_entry_ecns_30 (
    id integer DEFAULT nextval('summary_launch_entry_ecns_30_id_seq'::regclass) NOT NULL,
    date date NOT NULL,
    time_stamp time without time zone NOT NULL,
    total_second integer,
    ts_id integer,
    ecn_launch_id integer,
    ecn_entry_id integer,
    filled integer,
    total integer,
    ask_filled integer,
    ask_total integer,
    bid_filled integer,
    bid_total integer,
    side integer
);


ALTER TABLE public.summary_launch_entry_ecns_30 OWNER TO postgres;

--
-- Name: summary_launch_entry_ecns_60_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE summary_launch_entry_ecns_60_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.summary_launch_entry_ecns_60_id_seq OWNER TO postgres;

--
-- Name: summary_launch_entry_ecns_60; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE summary_launch_entry_ecns_60 (
    id integer DEFAULT nextval('summary_launch_entry_ecns_60_id_seq'::regclass) NOT NULL,
    date date NOT NULL,
    time_stamp time without time zone NOT NULL,
    total_second integer,
    ts_id integer,
    ecn_launch_id integer,
    ecn_entry_id integer,
    filled integer,
    total integer,
    ask_filled integer,
    ask_total integer,
    bid_filled integer,
    bid_total integer,
    side integer
);


ALTER TABLE public.summary_launch_entry_ecns_60 OWNER TO postgres;

--
-- Name: summary_launch_exit_ecns_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE summary_launch_exit_ecns_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.summary_launch_exit_ecns_id_seq OWNER TO postgres;

--
-- Name: summary_launch_exit_ecns; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE summary_launch_exit_ecns (
    id integer DEFAULT nextval('summary_launch_exit_ecns_id_seq'::regclass) NOT NULL,
    date date NOT NULL,
    time_stamp time without time zone NOT NULL,
    total_second integer,
    ts_id integer,
    ecn_launch_id integer,
    ecn_exit_id integer,
    filled integer,
    total integer,
    ask_filled integer,
    ask_total integer,
    bid_filled integer,
    bid_total integer,
    side integer
);


ALTER TABLE public.summary_launch_exit_ecns OWNER TO postgres;

--
-- Name: summary_launch_exit_ecns_15_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE summary_launch_exit_ecns_15_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.summary_launch_exit_ecns_15_id_seq OWNER TO postgres;

--
-- Name: summary_launch_exit_ecns_15; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE summary_launch_exit_ecns_15 (
    id integer DEFAULT nextval('summary_launch_exit_ecns_15_id_seq'::regclass) NOT NULL,
    date date NOT NULL,
    time_stamp time without time zone NOT NULL,
    total_second integer,
    ts_id integer,
    ecn_launch_id integer,
    ecn_exit_id integer,
    filled integer,
    total integer,
    ask_filled integer,
    ask_total integer,
    bid_filled integer,
    bid_total integer,
    side integer
);


ALTER TABLE public.summary_launch_exit_ecns_15 OWNER TO postgres;

--
-- Name: summary_launch_exit_ecns_30_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE summary_launch_exit_ecns_30_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.summary_launch_exit_ecns_30_id_seq OWNER TO postgres;

--
-- Name: summary_launch_exit_ecns_30; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE summary_launch_exit_ecns_30 (
    id integer DEFAULT nextval('summary_launch_exit_ecns_30_id_seq'::regclass) NOT NULL,
    date date NOT NULL,
    time_stamp time without time zone NOT NULL,
    total_second integer,
    ts_id integer,
    ecn_launch_id integer,
    ecn_exit_id integer,
    filled integer,
    total integer,
    ask_filled integer,
    ask_total integer,
    bid_filled integer,
    bid_total integer,
    side integer
);


ALTER TABLE public.summary_launch_exit_ecns_30 OWNER TO postgres;

--
-- Name: summary_launch_exit_ecns_60_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE summary_launch_exit_ecns_60_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.summary_launch_exit_ecns_60_id_seq OWNER TO postgres;

--
-- Name: summary_launch_exit_ecns_60; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE summary_launch_exit_ecns_60 (
    id integer DEFAULT nextval('summary_launch_exit_ecns_60_id_seq'::regclass) NOT NULL,
    date date NOT NULL,
    time_stamp time without time zone NOT NULL,
    total_second integer,
    ts_id integer,
    ecn_launch_id integer,
    ecn_exit_id integer,
    filled integer,
    total integer,
    ask_filled integer,
    ask_total integer,
    bid_filled integer,
    bid_total integer,
    side integer
);


ALTER TABLE public.summary_launch_exit_ecns_60 OWNER TO postgres;

--
-- Name: summary_order_types_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE summary_order_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.summary_order_types_id_seq OWNER TO postgres;

--
-- Name: summary_order_types; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE summary_order_types (
    id integer DEFAULT nextval('summary_order_types_id_seq'::regclass) NOT NULL,
    date date NOT NULL,
    time_stamp time without time zone NOT NULL,
    total_second integer,
    ts_id integer,
    ecn_id integer,
    buy_filled integer,
    buy_total integer,
    sell_filled integer,
    sell_total integer,
    shortsell_filled integer,
    shortsell_total integer,
    isentry_exit integer
);


ALTER TABLE public.summary_order_types OWNER TO postgres;

--
-- Name: summary_order_types_15_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE summary_order_types_15_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.summary_order_types_15_id_seq OWNER TO postgres;

--
-- Name: summary_order_types_15; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE summary_order_types_15 (
    id integer DEFAULT nextval('summary_order_types_15_id_seq'::regclass) NOT NULL,
    date date NOT NULL,
    time_stamp time without time zone NOT NULL,
    total_second integer,
    ts_id integer,
    ecn_id integer,
    buy_filled integer,
    buy_total integer,
    sell_filled integer,
    sell_total integer,
    shortsell_filled integer,
    shortsell_total integer,
    isentry_exit integer
);


ALTER TABLE public.summary_order_types_15 OWNER TO postgres;

--
-- Name: summary_order_types_30_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE summary_order_types_30_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.summary_order_types_30_id_seq OWNER TO postgres;

--
-- Name: summary_order_types_30; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE summary_order_types_30 (
    id integer DEFAULT nextval('summary_order_types_30_id_seq'::regclass) NOT NULL,
    date date NOT NULL,
    time_stamp time without time zone NOT NULL,
    total_second integer,
    ts_id integer,
    ecn_id integer,
    buy_filled integer,
    buy_total integer,
    sell_filled integer,
    sell_total integer,
    shortsell_filled integer,
    shortsell_total integer,
    isentry_exit integer
);


ALTER TABLE public.summary_order_types_30 OWNER TO postgres;

--
-- Name: summary_order_types_60_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE summary_order_types_60_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.summary_order_types_60_id_seq OWNER TO postgres;

--
-- Name: summary_order_types_60; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE summary_order_types_60 (
    id integer DEFAULT nextval('summary_order_types_60_id_seq'::regclass) NOT NULL,
    date date NOT NULL,
    time_stamp time without time zone NOT NULL,
    total_second integer,
    ts_id integer,
    ecn_id integer,
    buy_filled integer,
    buy_total integer,
    sell_filled integer,
    sell_total integer,
    shortsell_filled integer,
    shortsell_total integer,
    isentry_exit integer
);


ALTER TABLE public.summary_order_types_60 OWNER TO postgres;

--
-- Name: summary_profit_losses_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE summary_profit_losses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.summary_profit_losses_id_seq OWNER TO postgres;

--
-- Name: summary_profit_losses; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE summary_profit_losses (
    id integer DEFAULT nextval('summary_profit_losses_id_seq'::regclass) NOT NULL,
    date date NOT NULL,
    time_stamp time without time zone NOT NULL,
    total_second integer,
    gross_profit_loss double precision,
    volume integer,
    market_value double precision,
    net_profit_loss double precision DEFAULT 0.00,
    expected_profit double precision DEFAULT 0.00
);


ALTER TABLE public.summary_profit_losses OWNER TO postgres;

--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY summary_exit_order_shares ALTER COLUMN id SET DEFAULT nextval('summary_exit_order_shares_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY summary_exit_order_shares_15 ALTER COLUMN id SET DEFAULT nextval('summary_exit_order_shares_15_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY summary_exit_order_shares_30 ALTER COLUMN id SET DEFAULT nextval('summary_exit_order_shares_30_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY summary_exit_order_shares_60 ALTER COLUMN id SET DEFAULT nextval('summary_exit_order_shares_60_id_seq'::regclass);


--
-- Name: summary_ack_fills_15_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY summary_ack_fills_15
    ADD CONSTRAINT summary_ack_fills_15_pkey PRIMARY KEY (id);


--
-- Name: summary_ack_fills_30_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY summary_ack_fills_30
    ADD CONSTRAINT summary_ack_fills_30_pkey PRIMARY KEY (id);


--
-- Name: summary_ack_fills_60_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY summary_ack_fills_60
    ADD CONSTRAINT summary_ack_fills_60_pkey PRIMARY KEY (id);


--
-- Name: summary_ack_fills_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY summary_ack_fills
    ADD CONSTRAINT summary_ack_fills_pkey PRIMARY KEY (id);


--
-- Name: summary_entry_exits_15_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY summary_entry_exits_15
    ADD CONSTRAINT summary_entry_exits_15_pkey PRIMARY KEY (id);


--
-- Name: summary_entry_exits_30_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY summary_entry_exits_30
    ADD CONSTRAINT summary_entry_exits_30_pkey PRIMARY KEY (id);


--
-- Name: summary_entry_exits_60_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY summary_entry_exits_60
    ADD CONSTRAINT summary_entry_exits_60_pkey PRIMARY KEY (id);


--
-- Name: summary_entry_exits_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY summary_entry_exits
    ADD CONSTRAINT summary_entry_exits_pkey PRIMARY KEY (id);


--
-- Name: summary_exit_order_shares_15_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY summary_exit_order_shares_15
    ADD CONSTRAINT summary_exit_order_shares_15_pkey PRIMARY KEY (id);


--
-- Name: summary_exit_order_shares_30_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY summary_exit_order_shares_30
    ADD CONSTRAINT summary_exit_order_shares_30_pkey PRIMARY KEY (id);


--
-- Name: summary_exit_order_shares_60_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY summary_exit_order_shares_60
    ADD CONSTRAINT summary_exit_order_shares_60_pkey PRIMARY KEY (id);


--
-- Name: summary_exit_order_shares_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY summary_exit_order_shares
    ADD CONSTRAINT summary_exit_order_shares_pkey PRIMARY KEY (id);


--
-- Name: summary_launch_entry_ecns_15_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY summary_launch_entry_ecns_15
    ADD CONSTRAINT summary_launch_entry_ecns_15_pkey PRIMARY KEY (id);


--
-- Name: summary_launch_entry_ecns_30_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY summary_launch_entry_ecns_30
    ADD CONSTRAINT summary_launch_entry_ecns_30_pkey PRIMARY KEY (id);


--
-- Name: summary_launch_entry_ecns_60_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY summary_launch_entry_ecns_60
    ADD CONSTRAINT summary_launch_entry_ecns_60_pkey PRIMARY KEY (id);


--
-- Name: summary_launch_entry_ecns_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY summary_launch_entry_ecns
    ADD CONSTRAINT summary_launch_entry_ecns_pkey PRIMARY KEY (id);


--
-- Name: summary_launch_exit_ecns_15_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY summary_launch_exit_ecns_15
    ADD CONSTRAINT summary_launch_exit_ecns_15_pkey PRIMARY KEY (id);


--
-- Name: summary_launch_exit_ecns_30_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY summary_launch_exit_ecns_30
    ADD CONSTRAINT summary_launch_exit_ecns_30_pkey PRIMARY KEY (id);


--
-- Name: summary_launch_exit_ecns_60_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY summary_launch_exit_ecns_60
    ADD CONSTRAINT summary_launch_exit_ecns_60_pkey PRIMARY KEY (id);


--
-- Name: summary_launch_exit_ecns_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY summary_launch_exit_ecns
    ADD CONSTRAINT summary_launch_exit_ecns_pkey PRIMARY KEY (id);


--
-- Name: summary_order_types_15_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY summary_order_types_15
    ADD CONSTRAINT summary_order_types_15_pkey PRIMARY KEY (id);


--
-- Name: summary_order_types_30_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY summary_order_types_30
    ADD CONSTRAINT summary_order_types_30_pkey PRIMARY KEY (id);


--
-- Name: summary_order_types_60_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY summary_order_types_60
    ADD CONSTRAINT summary_order_types_60_pkey PRIMARY KEY (id);


--
-- Name: summary_order_types_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY summary_order_types
    ADD CONSTRAINT summary_order_types_pkey PRIMARY KEY (id);


--
-- Name: summary_profit_losses_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY summary_profit_losses
    ADD CONSTRAINT summary_profit_losses_pkey PRIMARY KEY (id);


--
-- Name: summary_ack_fills_15_all; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_ack_fills_15_all ON summary_ack_fills_15 USING btree (date, time_stamp, ts_id, ecn_id);


--
-- Name: summary_ack_fills_15_all_1; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_ack_fills_15_all_1 ON summary_ack_fills_15 USING btree (date, ts_id, ecn_id);


--
-- Name: summary_ack_fills_15_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_ack_fills_15_date ON summary_ack_fills_15 USING btree (date);


--
-- Name: summary_ack_fills_15_date_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_ack_fills_15_date_ecn_id ON summary_ack_fills_15 USING btree (date, ecn_id);


--
-- Name: summary_ack_fills_15_date_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_ack_fills_15_date_ts_id ON summary_ack_fills_15 USING btree (date, ts_id);


--
-- Name: summary_ack_fills_15_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_ack_fills_15_ecn_id ON summary_ack_fills_15 USING btree (ecn_id);


--
-- Name: summary_ack_fills_15_time_stamp; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_ack_fills_15_time_stamp ON summary_ack_fills_15 USING btree (time_stamp);


--
-- Name: summary_ack_fills_15_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_ack_fills_15_ts_id ON summary_ack_fills_15 USING btree (ts_id);


--
-- Name: summary_ack_fills_30_all; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_ack_fills_30_all ON summary_ack_fills_30 USING btree (date, time_stamp, ts_id, ecn_id);


--
-- Name: summary_ack_fills_30_all_1; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_ack_fills_30_all_1 ON summary_ack_fills_30 USING btree (date, ts_id, ecn_id);


--
-- Name: summary_ack_fills_30_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_ack_fills_30_date ON summary_ack_fills_30 USING btree (date);


--
-- Name: summary_ack_fills_30_date_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_ack_fills_30_date_ecn_id ON summary_ack_fills_30 USING btree (date, ecn_id);


--
-- Name: summary_ack_fills_30_date_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_ack_fills_30_date_ts_id ON summary_ack_fills_30 USING btree (date, ts_id);


--
-- Name: summary_ack_fills_30_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_ack_fills_30_ecn_id ON summary_ack_fills_30 USING btree (ecn_id);


--
-- Name: summary_ack_fills_30_time_stamp; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_ack_fills_30_time_stamp ON summary_ack_fills_30 USING btree (time_stamp);


--
-- Name: summary_ack_fills_30_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_ack_fills_30_ts_id ON summary_ack_fills_30 USING btree (ts_id);


--
-- Name: summary_ack_fills_60_all; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_ack_fills_60_all ON summary_ack_fills_60 USING btree (date, time_stamp, ts_id, ecn_id);


--
-- Name: summary_ack_fills_60_all_1; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_ack_fills_60_all_1 ON summary_ack_fills_60 USING btree (date, ts_id, ecn_id);


--
-- Name: summary_ack_fills_60_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_ack_fills_60_date ON summary_ack_fills_60 USING btree (date);


--
-- Name: summary_ack_fills_60_date_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_ack_fills_60_date_ecn_id ON summary_ack_fills_60 USING btree (date, ecn_id);


--
-- Name: summary_ack_fills_60_date_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_ack_fills_60_date_ts_id ON summary_ack_fills_60 USING btree (date, ts_id);


--
-- Name: summary_ack_fills_60_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_ack_fills_60_ecn_id ON summary_ack_fills_60 USING btree (ecn_id);


--
-- Name: summary_ack_fills_60_time_stamp; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_ack_fills_60_time_stamp ON summary_ack_fills_60 USING btree (time_stamp);


--
-- Name: summary_ack_fills_60_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_ack_fills_60_ts_id ON summary_ack_fills_60 USING btree (ts_id);


--
-- Name: summary_ack_fills_all; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_ack_fills_all ON summary_ack_fills USING btree (date, time_stamp, ts_id, ecn_id);


--
-- Name: summary_ack_fills_all_1; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_ack_fills_all_1 ON summary_ack_fills USING btree (date, ts_id, ecn_id);


--
-- Name: summary_ack_fills_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_ack_fills_date ON summary_ack_fills USING btree (date);


--
-- Name: summary_ack_fills_date_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_ack_fills_date_ecn_id ON summary_ack_fills USING btree (date, ecn_id);


--
-- Name: summary_ack_fills_date_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_ack_fills_date_ts_id ON summary_ack_fills USING btree (date, ts_id);


--
-- Name: summary_ack_fills_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_ack_fills_ecn_id ON summary_ack_fills USING btree (ecn_id);


--
-- Name: summary_ack_fills_time_stamp; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_ack_fills_time_stamp ON summary_ack_fills USING btree (time_stamp);


--
-- Name: summary_ack_fills_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_ack_fills_ts_id ON summary_ack_fills USING btree (ts_id);


--
-- Name: summary_entry_exits_15_all_0; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_entry_exits_15_all_0 ON summary_entry_exits_15 USING btree (date, ts_id, ecn_id);


--
-- Name: summary_entry_exits_15_all_1; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_entry_exits_15_all_1 ON summary_entry_exits_15 USING btree (date, ts_id, ecn_id, total_second);


--
-- Name: summary_entry_exits_15_all_2; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_entry_exits_15_all_2 ON summary_entry_exits_15 USING btree (date, time_stamp, ts_id, ecn_id);


--
-- Name: summary_entry_exits_15_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_entry_exits_15_date ON summary_entry_exits_15 USING btree (date);


--
-- Name: summary_entry_exits_15_date_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_entry_exits_15_date_ecn_id ON summary_entry_exits_15 USING btree (date, ecn_id);


--
-- Name: summary_entry_exits_15_date_total_second; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_entry_exits_15_date_total_second ON summary_entry_exits_15 USING btree (date, total_second);


--
-- Name: summary_entry_exits_15_date_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_entry_exits_15_date_ts_id ON summary_entry_exits_15 USING btree (date, ts_id);


--
-- Name: summary_entry_exits_15_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_entry_exits_15_ecn_id ON summary_entry_exits_15 USING btree (ecn_id);


--
-- Name: summary_entry_exits_15_time_stamp; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_entry_exits_15_time_stamp ON summary_entry_exits_15 USING btree (time_stamp);


--
-- Name: summary_entry_exits_15_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_entry_exits_15_ts_id ON summary_entry_exits_15 USING btree (ts_id);


--
-- Name: summary_entry_exits_30_all_0; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_entry_exits_30_all_0 ON summary_entry_exits_30 USING btree (date, ts_id, ecn_id);


--
-- Name: summary_entry_exits_30_all_1; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_entry_exits_30_all_1 ON summary_entry_exits_30 USING btree (date, ts_id, ecn_id, total_second);


--
-- Name: summary_entry_exits_30_all_2; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_entry_exits_30_all_2 ON summary_entry_exits_30 USING btree (date, time_stamp, ts_id, ecn_id);


--
-- Name: summary_entry_exits_30_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_entry_exits_30_date ON summary_entry_exits_30 USING btree (date);


--
-- Name: summary_entry_exits_30_date_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_entry_exits_30_date_ecn_id ON summary_entry_exits_30 USING btree (date, ecn_id);


--
-- Name: summary_entry_exits_30_date_total_second; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_entry_exits_30_date_total_second ON summary_entry_exits_30 USING btree (date, total_second);


--
-- Name: summary_entry_exits_30_date_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_entry_exits_30_date_ts_id ON summary_entry_exits_30 USING btree (date, ts_id);


--
-- Name: summary_entry_exits_30_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_entry_exits_30_ecn_id ON summary_entry_exits_30 USING btree (ecn_id);


--
-- Name: summary_entry_exits_30_time_stamp; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_entry_exits_30_time_stamp ON summary_entry_exits_30 USING btree (time_stamp);


--
-- Name: summary_entry_exits_30_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_entry_exits_30_ts_id ON summary_entry_exits_30 USING btree (ts_id);


--
-- Name: summary_entry_exits_60_all_0; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_entry_exits_60_all_0 ON summary_entry_exits_60 USING btree (date, ts_id, ecn_id);


--
-- Name: summary_entry_exits_60_all_1; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_entry_exits_60_all_1 ON summary_entry_exits_60 USING btree (date, ts_id, ecn_id, total_second);


--
-- Name: summary_entry_exits_60_all_2; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_entry_exits_60_all_2 ON summary_entry_exits_60 USING btree (date, time_stamp, ts_id, ecn_id);


--
-- Name: summary_entry_exits_60_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_entry_exits_60_date ON summary_entry_exits_60 USING btree (date);


--
-- Name: summary_entry_exits_60_date_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_entry_exits_60_date_ecn_id ON summary_entry_exits_60 USING btree (date, ecn_id);


--
-- Name: summary_entry_exits_60_date_total_second; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_entry_exits_60_date_total_second ON summary_entry_exits_60 USING btree (date, total_second);


--
-- Name: summary_entry_exits_60_date_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_entry_exits_60_date_ts_id ON summary_entry_exits_60 USING btree (date, ts_id);


--
-- Name: summary_entry_exits_60_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_entry_exits_60_ecn_id ON summary_entry_exits_60 USING btree (ecn_id);


--
-- Name: summary_entry_exits_60_time_stamp; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_entry_exits_60_time_stamp ON summary_entry_exits_60 USING btree (time_stamp);


--
-- Name: summary_entry_exits_60_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_entry_exits_60_ts_id ON summary_entry_exits_60 USING btree (ts_id);


--
-- Name: summary_entry_exits_all_0; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_entry_exits_all_0 ON summary_entry_exits USING btree (date, ts_id, ecn_id);


--
-- Name: summary_entry_exits_all_1; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_entry_exits_all_1 ON summary_entry_exits USING btree (date, ts_id, ecn_id, total_second);


--
-- Name: summary_entry_exits_all_2; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_entry_exits_all_2 ON summary_entry_exits USING btree (date, time_stamp, ts_id, ecn_id);


--
-- Name: summary_entry_exits_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_entry_exits_date ON summary_entry_exits USING btree (date);


--
-- Name: summary_entry_exits_date_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_entry_exits_date_ecn_id ON summary_entry_exits USING btree (date, ecn_id);


--
-- Name: summary_entry_exits_date_total_second; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_entry_exits_date_total_second ON summary_entry_exits USING btree (date, total_second);


--
-- Name: summary_entry_exits_date_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_entry_exits_date_ts_id ON summary_entry_exits USING btree (date, ts_id);


--
-- Name: summary_entry_exits_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_entry_exits_ecn_id ON summary_entry_exits USING btree (ecn_id);


--
-- Name: summary_entry_exits_time_stamp; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_entry_exits_time_stamp ON summary_entry_exits USING btree (time_stamp);


--
-- Name: summary_entry_exits_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_entry_exits_ts_id ON summary_entry_exits USING btree (ts_id);


--
-- Name: summary_exit_order_shares_15_all_0; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_15_all_0 ON summary_exit_order_shares_15 USING btree (date, ts_id, ecn_id);


--
-- Name: summary_exit_order_shares_15_all_1; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_15_all_1 ON summary_exit_order_shares_15 USING btree (date, ts_id, ecn_id, total_second);


--
-- Name: summary_exit_order_shares_15_all_2; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_15_all_2 ON summary_exit_order_shares_15 USING btree (date, time_stamp, ts_id, ecn_id);

--
-- Name: summary_exit_order_shares_15_all_3; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_15_all_3 ON summary_exit_order_shares_15 USING btree (date, time_stamp, ts_id, ecn_id, entry_exit);


--
-- Name: summary_exit_order_shares_15_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_15_date ON summary_exit_order_shares_15 USING btree (date);


--
-- Name: summary_exit_order_shares_15_date_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_15_date_ecn_id ON summary_exit_order_shares_15 USING btree (date, ecn_id);


--
-- Name: summary_exit_order_shares_15_date_total_second; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_15_date_total_second ON summary_exit_order_shares_15 USING btree (date, total_second);


--
-- Name: summary_exit_order_shares_15_date_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_15_date_ts_id ON summary_exit_order_shares_15 USING btree (date, ts_id);


--
-- Name: summary_exit_order_shares_15_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_15_ecn_id ON summary_exit_order_shares_15 USING btree (ecn_id);


--
-- Name: summary_exit_order_shares_15_time_stamp; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_15_time_stamp ON summary_exit_order_shares_15 USING btree (time_stamp);


--
-- Name: summary_exit_order_shares_15_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_15_ts_id ON summary_exit_order_shares_15 USING btree (ts_id);


--
-- Name: summary_exit_order_shares_30_all_0; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_30_all_0 ON summary_exit_order_shares_30 USING btree (date, ts_id, ecn_id);


--
-- Name: summary_exit_order_shares_30_all_1; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_30_all_1 ON summary_exit_order_shares_30 USING btree (date, ts_id, ecn_id, total_second);


--
-- Name: summary_exit_order_shares_30_all_2; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_30_all_2 ON summary_exit_order_shares_30 USING btree (date, time_stamp, ts_id, ecn_id);

--
-- Name: summary_exit_order_shares_30_all_3; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_30_all_3 ON summary_exit_order_shares_30 USING btree (date, time_stamp, ts_id, ecn_id, entry_exit);



--
-- Name: summary_exit_order_shares_30_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_30_date ON summary_exit_order_shares_30 USING btree (date);


--
-- Name: summary_exit_order_shares_30_date_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_30_date_ecn_id ON summary_exit_order_shares_30 USING btree (date, ecn_id);


--
-- Name: summary_exit_order_shares_30_date_total_second; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_30_date_total_second ON summary_exit_order_shares_30 USING btree (date, total_second);


--
-- Name: summary_exit_order_shares_30_date_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_30_date_ts_id ON summary_exit_order_shares_30 USING btree (date, ts_id);


--
-- Name: summary_exit_order_shares_30_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_30_ecn_id ON summary_exit_order_shares_30 USING btree (ecn_id);


--
-- Name: summary_exit_order_shares_30_time_stamp; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_30_time_stamp ON summary_exit_order_shares_30 USING btree (time_stamp);


--
-- Name: summary_exit_order_shares_30_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_30_ts_id ON summary_exit_order_shares_30 USING btree (ts_id);


--
-- Name: summary_exit_order_shares_60_all_0; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_60_all_0 ON summary_exit_order_shares_60 USING btree (date, ts_id, ecn_id);


--
-- Name: summary_exit_order_shares_60_all_1; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_60_all_1 ON summary_exit_order_shares_60 USING btree (date, ts_id, ecn_id, total_second);


--
-- Name: summary_exit_order_shares_60_all_2; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_60_all_2 ON summary_exit_order_shares_60 USING btree (date, time_stamp, ts_id, ecn_id);

--
-- Name: summary_exit_order_shares_60_all_3; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_60_all_3 ON summary_exit_order_shares_60 USING btree (date, time_stamp, ts_id, ecn_id, entry_exit);


--
-- Name: summary_exit_order_shares_60_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_60_date ON summary_exit_order_shares_60 USING btree (date);


--
-- Name: summary_exit_order_shares_60_date_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_60_date_ecn_id ON summary_exit_order_shares_60 USING btree (date, ecn_id);


--
-- Name: summary_exit_order_shares_60_date_total_second; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_60_date_total_second ON summary_exit_order_shares_60 USING btree (date, total_second);


--
-- Name: summary_exit_order_shares_60_date_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_60_date_ts_id ON summary_exit_order_shares_60 USING btree (date, ts_id);


--
-- Name: summary_exit_order_shares_60_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_60_ecn_id ON summary_exit_order_shares_60 USING btree (ecn_id);


--
-- Name: summary_exit_order_shares_60_time_stamp; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_60_time_stamp ON summary_exit_order_shares_60 USING btree (time_stamp);


--
-- Name: summary_exit_order_shares_60_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_60_ts_id ON summary_exit_order_shares_60 USING btree (ts_id);


--
-- Name: summary_exit_order_shares_all_0; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_all_0 ON summary_exit_order_shares USING btree (date, ts_id, ecn_id);


--
-- Name: summary_exit_order_shares_all_1; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_all_1 ON summary_exit_order_shares USING btree (date, ts_id, ecn_id, total_second);


--
-- Name: summary_exit_order_shares_all_2; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_all_2 ON summary_exit_order_shares USING btree (date, time_stamp, ts_id, ecn_id);

--
-- Name: summary_exit_order_shares_all_3; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_all_3 ON summary_exit_order_shares USING btree (date, time_stamp, ts_id, ecn_id, entry_exit);


--
-- Name: summary_exit_order_shares_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_date ON summary_exit_order_shares USING btree (date);


--
-- Name: summary_exit_order_shares_date_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_date_ecn_id ON summary_exit_order_shares USING btree (date, ecn_id);


--
-- Name: summary_exit_order_shares_date_total_second; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_date_total_second ON summary_exit_order_shares USING btree (date, total_second);


--
-- Name: summary_exit_order_shares_date_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_date_ts_id ON summary_exit_order_shares USING btree (date, ts_id);


--
-- Name: summary_exit_order_shares_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_ecn_id ON summary_exit_order_shares USING btree (ecn_id);


--
-- Name: summary_exit_order_shares_time_stamp; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_time_stamp ON summary_exit_order_shares USING btree (time_stamp);


--
-- Name: summary_exit_order_shares_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_ts_id ON summary_exit_order_shares USING btree (ts_id);


--
-- Name: summary_launch_entry_ecns_15_all; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_entry_ecns_15_all ON summary_launch_entry_ecns_15 USING btree (date, time_stamp, ts_id, ecn_launch_id, ecn_entry_id);


--
-- Name: summary_launch_entry_ecns_15_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_entry_ecns_15_date ON summary_launch_entry_ecns_15 USING btree (date);


--
-- Name: summary_launch_entry_ecns_15_date_ecn_entry_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_entry_ecns_15_date_ecn_entry_id ON summary_launch_entry_ecns_15 USING btree (date, ecn_entry_id);


--
-- Name: summary_launch_entry_ecns_15_date_ecn_launch_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_entry_ecns_15_date_ecn_launch_id ON summary_launch_entry_ecns_15 USING btree (date, ecn_launch_id);


--
-- Name: summary_launch_entry_ecns_15_date_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_entry_ecns_15_date_ts_id ON summary_launch_entry_ecns_15 USING btree (date, ts_id);


--
-- Name: summary_launch_entry_ecns_15_date_ts_id_ecn_entry_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_entry_ecns_15_date_ts_id_ecn_entry_id ON summary_launch_entry_ecns_15 USING btree (date, ts_id, ecn_entry_id);


--
-- Name: summary_launch_entry_ecns_15_date_ts_id_ecn_launch_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_entry_ecns_15_date_ts_id_ecn_launch_id ON summary_launch_entry_ecns_15 USING btree (date, ts_id, ecn_launch_id);


--
-- Name: summary_launch_entry_ecns_15_date_ts_id_ecn_launch_id_ecn_entry; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_entry_ecns_15_date_ts_id_ecn_launch_id_ecn_entry ON summary_launch_entry_ecns_15 USING btree (date, ts_id, ecn_launch_id, ecn_entry_id);


--
-- Name: summary_launch_entry_ecns_15_entry_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_entry_ecns_15_entry_ecn_id ON summary_launch_entry_ecns_15 USING btree (ecn_entry_id);


--
-- Name: summary_launch_entry_ecns_15_launch_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_entry_ecns_15_launch_ecn_id ON summary_launch_entry_ecns_15 USING btree (ecn_launch_id);


--
-- Name: summary_launch_entry_ecns_15_time_stamp; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_entry_ecns_15_time_stamp ON summary_launch_entry_ecns_15 USING btree (time_stamp);


--
-- Name: summary_launch_entry_ecns_15_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_entry_ecns_15_ts_id ON summary_launch_entry_ecns_15 USING btree (ts_id);


--
-- Name: summary_launch_entry_ecns_30_all; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_entry_ecns_30_all ON summary_launch_entry_ecns_30 USING btree (date, time_stamp, ts_id, ecn_launch_id, ecn_entry_id);


--
-- Name: summary_launch_entry_ecns_30_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_entry_ecns_30_date ON summary_launch_entry_ecns_30 USING btree (date);


--
-- Name: summary_launch_entry_ecns_30_date_ecn_entry_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_entry_ecns_30_date_ecn_entry_id ON summary_launch_entry_ecns_30 USING btree (date, ecn_entry_id);


--
-- Name: summary_launch_entry_ecns_30_date_ecn_launch_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_entry_ecns_30_date_ecn_launch_id ON summary_launch_entry_ecns_30 USING btree (date, ecn_launch_id);


--
-- Name: summary_launch_entry_ecns_30_date_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_entry_ecns_30_date_ts_id ON summary_launch_entry_ecns_30 USING btree (date, ts_id);


--
-- Name: summary_launch_entry_ecns_30_date_ts_id_ecn_entry_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_entry_ecns_30_date_ts_id_ecn_entry_id ON summary_launch_entry_ecns_30 USING btree (date, ts_id, ecn_entry_id);


--
-- Name: summary_launch_entry_ecns_30_date_ts_id_ecn_launch_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_entry_ecns_30_date_ts_id_ecn_launch_id ON summary_launch_entry_ecns_30 USING btree (date, ts_id, ecn_launch_id);


--
-- Name: summary_launch_entry_ecns_30_date_ts_id_ecn_launch_id_ecn_entry; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_entry_ecns_30_date_ts_id_ecn_launch_id_ecn_entry ON summary_launch_entry_ecns_30 USING btree (date, ts_id, ecn_launch_id, ecn_entry_id);


--
-- Name: summary_launch_entry_ecns_30_entry_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_entry_ecns_30_entry_ecn_id ON summary_launch_entry_ecns_30 USING btree (ecn_entry_id);


--
-- Name: summary_launch_entry_ecns_30_launch_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_entry_ecns_30_launch_ecn_id ON summary_launch_entry_ecns_30 USING btree (ecn_launch_id);


--
-- Name: summary_launch_entry_ecns_30_time_stamp; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_entry_ecns_30_time_stamp ON summary_launch_entry_ecns_30 USING btree (time_stamp);


--
-- Name: summary_launch_entry_ecns_30_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_entry_ecns_30_ts_id ON summary_launch_entry_ecns_30 USING btree (ts_id);


--
-- Name: summary_launch_entry_ecns_60_all; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_entry_ecns_60_all ON summary_launch_entry_ecns_60 USING btree (date, time_stamp, ts_id, ecn_launch_id, ecn_entry_id);


--
-- Name: summary_launch_entry_ecns_60_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_entry_ecns_60_date ON summary_launch_entry_ecns_60 USING btree (date);


--
-- Name: summary_launch_entry_ecns_60_date_ecn_entry_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_entry_ecns_60_date_ecn_entry_id ON summary_launch_entry_ecns_60 USING btree (date, ecn_entry_id);


--
-- Name: summary_launch_entry_ecns_60_date_ecn_launch_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_entry_ecns_60_date_ecn_launch_id ON summary_launch_entry_ecns_60 USING btree (date, ecn_launch_id);


--
-- Name: summary_launch_entry_ecns_60_date_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_entry_ecns_60_date_ts_id ON summary_launch_entry_ecns_60 USING btree (date, ts_id);


--
-- Name: summary_launch_entry_ecns_60_date_ts_id_ecn_entry_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_entry_ecns_60_date_ts_id_ecn_entry_id ON summary_launch_entry_ecns_60 USING btree (date, ts_id, ecn_entry_id);


--
-- Name: summary_launch_entry_ecns_60_date_ts_id_ecn_launch_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_entry_ecns_60_date_ts_id_ecn_launch_id ON summary_launch_entry_ecns_60 USING btree (date, ts_id, ecn_launch_id);


--
-- Name: summary_launch_entry_ecns_60_date_ts_id_ecn_launch_id_ecn_entry; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_entry_ecns_60_date_ts_id_ecn_launch_id_ecn_entry ON summary_launch_entry_ecns_60 USING btree (date, ts_id, ecn_launch_id, ecn_entry_id);


--
-- Name: summary_launch_entry_ecns_60_entry_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_entry_ecns_60_entry_ecn_id ON summary_launch_entry_ecns_60 USING btree (ecn_entry_id);


--
-- Name: summary_launch_entry_ecns_60_launch_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_entry_ecns_60_launch_ecn_id ON summary_launch_entry_ecns_60 USING btree (ecn_launch_id);


--
-- Name: summary_launch_entry_ecns_60_time_stamp; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_entry_ecns_60_time_stamp ON summary_launch_entry_ecns_60 USING btree (time_stamp);


--
-- Name: summary_launch_entry_ecns_60_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_entry_ecns_60_ts_id ON summary_launch_entry_ecns_60 USING btree (ts_id);


--
-- Name: summary_launch_entry_ecns_all; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_entry_ecns_all ON summary_launch_entry_ecns USING btree (date, time_stamp, ts_id, ecn_launch_id, ecn_entry_id);


--
-- Name: summary_launch_entry_ecns_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_entry_ecns_date ON summary_launch_entry_ecns USING btree (date);


--
-- Name: summary_launch_entry_ecns_date_ecn_entry_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_entry_ecns_date_ecn_entry_id ON summary_launch_entry_ecns USING btree (date, ecn_entry_id);


--
-- Name: summary_launch_entry_ecns_date_ecn_launch_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_entry_ecns_date_ecn_launch_id ON summary_launch_entry_ecns USING btree (date, ecn_launch_id);


--
-- Name: summary_launch_entry_ecns_date_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_entry_ecns_date_ts_id ON summary_launch_entry_ecns USING btree (date, ts_id);


--
-- Name: summary_launch_entry_ecns_date_ts_id_ecn_entry_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_entry_ecns_date_ts_id_ecn_entry_id ON summary_launch_entry_ecns USING btree (date, ts_id, ecn_entry_id);


--
-- Name: summary_launch_entry_ecns_date_ts_id_ecn_launch_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_entry_ecns_date_ts_id_ecn_launch_id ON summary_launch_entry_ecns USING btree (date, ts_id, ecn_launch_id);


--
-- Name: summary_launch_entry_ecns_date_ts_id_ecn_launch_id_ecn_entry_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_entry_ecns_date_ts_id_ecn_launch_id_ecn_entry_id ON summary_launch_entry_ecns USING btree (date, ts_id, ecn_launch_id, ecn_entry_id);


--
-- Name: summary_launch_entry_ecns_entry_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_entry_ecns_entry_ecn_id ON summary_launch_entry_ecns USING btree (ecn_entry_id);


--
-- Name: summary_launch_entry_ecns_launch_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_entry_ecns_launch_ecn_id ON summary_launch_entry_ecns USING btree (ecn_launch_id);


--
-- Name: summary_launch_entry_ecns_time_stamp; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_entry_ecns_time_stamp ON summary_launch_entry_ecns USING btree (time_stamp);


--
-- Name: summary_launch_entry_ecns_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_entry_ecns_ts_id ON summary_launch_entry_ecns USING btree (ts_id);


--
-- Name: summary_launch_exit_ecns_15_all; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_15_all ON summary_launch_exit_ecns_15 USING btree (date, time_stamp, ts_id, ecn_launch_id, ecn_exit_id);


--
-- Name: summary_launch_exit_ecns_15_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_15_date ON summary_launch_exit_ecns_15 USING btree (date);


--
-- Name: summary_launch_exit_ecns_15_date_ecn_exit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_15_date_ecn_exit_id ON summary_launch_exit_ecns_15 USING btree (date, ecn_exit_id);


--
-- Name: summary_launch_exit_ecns_15_date_ecn_launch_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_15_date_ecn_launch_id ON summary_launch_exit_ecns_15 USING btree (date, ecn_launch_id);


--
-- Name: summary_launch_exit_ecns_15_date_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_15_date_ts_id ON summary_launch_exit_ecns_15 USING btree (date, ts_id);


--
-- Name: summary_launch_exit_ecns_15_date_ts_id_ecn_exit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_15_date_ts_id_ecn_exit_id ON summary_launch_exit_ecns_15 USING btree (date, ts_id, ecn_exit_id);


--
-- Name: summary_launch_exit_ecns_15_date_ts_id_ecn_launch_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_15_date_ts_id_ecn_launch_id ON summary_launch_exit_ecns_15 USING btree (date, ts_id, ecn_launch_id);


--
-- Name: summary_launch_exit_ecns_15_date_ts_id_ecn_launch_id_ecn_exit; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_15_date_ts_id_ecn_launch_id_ecn_exit ON summary_launch_exit_ecns_15 USING btree (date, ts_id, ecn_launch_id, ecn_exit_id);


--
-- Name: summary_launch_exit_ecns_15_exit_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_15_exit_ecn_id ON summary_launch_exit_ecns_15 USING btree (ecn_exit_id);


--
-- Name: summary_launch_exit_ecns_15_launch_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_15_launch_ecn_id ON summary_launch_exit_ecns_15 USING btree (ecn_launch_id);


--
-- Name: summary_launch_exit_ecns_15_time_stamp; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_15_time_stamp ON summary_launch_exit_ecns_15 USING btree (time_stamp);


--
-- Name: summary_launch_exit_ecns_15_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_15_ts_id ON summary_launch_exit_ecns_15 USING btree (ts_id);


--
-- Name: summary_launch_exit_ecns_30_all; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_30_all ON summary_launch_exit_ecns_30 USING btree (date, time_stamp, ts_id, ecn_launch_id, ecn_exit_id);


--
-- Name: summary_launch_exit_ecns_30_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_30_date ON summary_launch_exit_ecns_30 USING btree (date);


--
-- Name: summary_launch_exit_ecns_30_date_ecn_exit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_30_date_ecn_exit_id ON summary_launch_exit_ecns_30 USING btree (date, ecn_exit_id);


--
-- Name: summary_launch_exit_ecns_30_date_ecn_launch_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_30_date_ecn_launch_id ON summary_launch_exit_ecns_30 USING btree (date, ecn_launch_id);


--
-- Name: summary_launch_exit_ecns_30_date_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_30_date_ts_id ON summary_launch_exit_ecns_30 USING btree (date, ts_id);


--
-- Name: summary_launch_exit_ecns_30_date_ts_id_ecn_exit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_30_date_ts_id_ecn_exit_id ON summary_launch_exit_ecns_30 USING btree (date, ts_id, ecn_exit_id);


--
-- Name: summary_launch_exit_ecns_30_date_ts_id_ecn_launch_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_30_date_ts_id_ecn_launch_id ON summary_launch_exit_ecns_30 USING btree (date, ts_id, ecn_launch_id);


--
-- Name: summary_launch_exit_ecns_30_date_ts_id_ecn_launch_id_ecn_exit; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_30_date_ts_id_ecn_launch_id_ecn_exit ON summary_launch_exit_ecns_30 USING btree (date, ts_id, ecn_launch_id, ecn_exit_id);


--
-- Name: summary_launch_exit_ecns_30_exit_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_30_exit_ecn_id ON summary_launch_exit_ecns_30 USING btree (ecn_exit_id);


--
-- Name: summary_launch_exit_ecns_30_launch_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_30_launch_ecn_id ON summary_launch_exit_ecns_30 USING btree (ecn_launch_id);


--
-- Name: summary_launch_exit_ecns_30_time_stamp; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_30_time_stamp ON summary_launch_exit_ecns_30 USING btree (time_stamp);


--
-- Name: summary_launch_exit_ecns_30_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_30_ts_id ON summary_launch_exit_ecns_30 USING btree (ts_id);


--
-- Name: summary_launch_exit_ecns_60_all; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_60_all ON summary_launch_exit_ecns_60 USING btree (date, time_stamp, ts_id, ecn_launch_id, ecn_exit_id);


--
-- Name: summary_launch_exit_ecns_60_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_60_date ON summary_launch_exit_ecns_60 USING btree (date);


--
-- Name: summary_launch_exit_ecns_60_date_ecn_exit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_60_date_ecn_exit_id ON summary_launch_exit_ecns_60 USING btree (date, ecn_exit_id);


--
-- Name: summary_launch_exit_ecns_60_date_ecn_launch_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_60_date_ecn_launch_id ON summary_launch_exit_ecns_60 USING btree (date, ecn_launch_id);


--
-- Name: summary_launch_exit_ecns_60_date_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_60_date_ts_id ON summary_launch_exit_ecns_60 USING btree (date, ts_id);


--
-- Name: summary_launch_exit_ecns_60_date_ts_id_ecn_exit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_60_date_ts_id_ecn_exit_id ON summary_launch_exit_ecns_60 USING btree (date, ts_id, ecn_exit_id);


--
-- Name: summary_launch_exit_ecns_60_date_ts_id_ecn_launch_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_60_date_ts_id_ecn_launch_id ON summary_launch_exit_ecns_60 USING btree (date, ts_id, ecn_launch_id);


--
-- Name: summary_launch_exit_ecns_60_date_ts_id_ecn_launch_id_ecn_exit; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_60_date_ts_id_ecn_launch_id_ecn_exit ON summary_launch_exit_ecns_60 USING btree (date, ts_id, ecn_launch_id, ecn_exit_id);


--
-- Name: summary_launch_exit_ecns_60_exit_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_60_exit_ecn_id ON summary_launch_exit_ecns_60 USING btree (ecn_exit_id);


--
-- Name: summary_launch_exit_ecns_60_launch_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_60_launch_ecn_id ON summary_launch_exit_ecns_60 USING btree (ecn_launch_id);


--
-- Name: summary_launch_exit_ecns_60_time_stamp; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_60_time_stamp ON summary_launch_exit_ecns_60 USING btree (time_stamp);


--
-- Name: summary_launch_exit_ecns_60_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_60_ts_id ON summary_launch_exit_ecns_60 USING btree (ts_id);


--
-- Name: summary_launch_exit_ecns_all; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_all ON summary_launch_exit_ecns USING btree (date, time_stamp, ts_id, ecn_launch_id, ecn_exit_id);


--
-- Name: summary_launch_exit_ecns_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_date ON summary_launch_exit_ecns USING btree (date);


--
-- Name: summary_launch_exit_ecns_date_ecn_exit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_date_ecn_exit_id ON summary_launch_exit_ecns USING btree (date, ecn_exit_id);


--
-- Name: summary_launch_exit_ecns_date_ecn_launch_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_date_ecn_launch_id ON summary_launch_exit_ecns USING btree (date, ecn_launch_id);


--
-- Name: summary_launch_exit_ecns_date_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_date_ts_id ON summary_launch_exit_ecns USING btree (date, ts_id);


--
-- Name: summary_launch_exit_ecns_date_ts_id_ecn_exit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_date_ts_id_ecn_exit_id ON summary_launch_exit_ecns USING btree (date, ts_id, ecn_exit_id);


--
-- Name: summary_launch_exit_ecns_date_ts_id_ecn_launch_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_date_ts_id_ecn_launch_id ON summary_launch_exit_ecns USING btree (date, ts_id, ecn_launch_id);


--
-- Name: summary_launch_exit_ecns_date_ts_id_ecn_launch_id_ecn_exit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_date_ts_id_ecn_launch_id_ecn_exit_id ON summary_launch_exit_ecns USING btree (date, ts_id, ecn_launch_id, ecn_exit_id);


--
-- Name: summary_launch_exit_ecns_exit_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_exit_ecn_id ON summary_launch_exit_ecns USING btree (ecn_exit_id);


--
-- Name: summary_launch_exit_ecns_launch_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_launch_ecn_id ON summary_launch_exit_ecns USING btree (ecn_launch_id);


--
-- Name: summary_launch_exit_ecns_time_stamp; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_time_stamp ON summary_launch_exit_ecns USING btree (time_stamp);


--
-- Name: summary_launch_exit_ecns_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_ts_id ON summary_launch_exit_ecns USING btree (ts_id);


--
-- Name: summary_order_types_15_all; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_order_types_15_all ON summary_order_types_15 USING btree (date, time_stamp, ts_id, ecn_id);


--
-- Name: summary_order_types_15_all_1; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_order_types_15_all_1 ON summary_order_types_15 USING btree (date, ts_id, ecn_id);


--
-- Name: summary_order_types_15_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_order_types_15_date ON summary_order_types_15 USING btree (date);


--
-- Name: summary_order_types_15_date_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_order_types_15_date_ecn_id ON summary_order_types_15 USING btree (date, ecn_id);


--
-- Name: summary_order_types_15_date_time_stamp_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_order_types_15_date_time_stamp_ecn_id ON summary_order_types_15 USING btree (date, time_stamp, ecn_id);


--
-- Name: summary_order_types_15_date_time_stamp_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_order_types_15_date_time_stamp_ts_id ON summary_order_types_15 USING btree (date, time_stamp, ts_id);


--
-- Name: summary_order_types_15_date_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_order_types_15_date_ts_id ON summary_order_types_15 USING btree (date, ts_id);


--
-- Name: summary_order_types_15_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_order_types_15_ecn_id ON summary_order_types_15 USING btree (ecn_id);


--
-- Name: summary_order_types_15_time_stamp; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_order_types_15_time_stamp ON summary_order_types_15 USING btree (time_stamp);


--
-- Name: summary_order_types_15_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_order_types_15_ts_id ON summary_order_types_15 USING btree (ts_id);


--
-- Name: summary_order_types_30_all; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_order_types_30_all ON summary_order_types_30 USING btree (date, time_stamp, ts_id, ecn_id);


--
-- Name: summary_order_types_30_all_1; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_order_types_30_all_1 ON summary_order_types_30 USING btree (date, ts_id, ecn_id);


--
-- Name: summary_order_types_30_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_order_types_30_date ON summary_order_types_30 USING btree (date);


--
-- Name: summary_order_types_30_date_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_order_types_30_date_ecn_id ON summary_order_types_30 USING btree (date, ecn_id);


--
-- Name: summary_order_types_30_date_time_stamp_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_order_types_30_date_time_stamp_ecn_id ON summary_order_types_30 USING btree (date, time_stamp, ecn_id);


--
-- Name: summary_order_types_30_date_time_stamp_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_order_types_30_date_time_stamp_ts_id ON summary_order_types_30 USING btree (date, time_stamp, ts_id);


--
-- Name: summary_order_types_30_date_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_order_types_30_date_ts_id ON summary_order_types_30 USING btree (date, ts_id);


--
-- Name: summary_order_types_30_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_order_types_30_ecn_id ON summary_order_types_30 USING btree (ecn_id);


--
-- Name: summary_order_types_30_time_stamp; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_order_types_30_time_stamp ON summary_order_types_30 USING btree (time_stamp);


--
-- Name: summary_order_types_30_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_order_types_30_ts_id ON summary_order_types_30 USING btree (ts_id);


--
-- Name: summary_order_types_60_all; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_order_types_60_all ON summary_order_types_60 USING btree (date, time_stamp, ts_id, ecn_id);


--
-- Name: summary_order_types_60_all_1; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_order_types_60_all_1 ON summary_order_types_60 USING btree (date, ts_id, ecn_id);


--
-- Name: summary_order_types_60_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_order_types_60_date ON summary_order_types_60 USING btree (date);


--
-- Name: summary_order_types_60_date_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_order_types_60_date_ecn_id ON summary_order_types_60 USING btree (date, ecn_id);


--
-- Name: summary_order_types_60_date_time_stamp_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_order_types_60_date_time_stamp_ecn_id ON summary_order_types_60 USING btree (date, time_stamp, ecn_id);


--
-- Name: summary_order_types_60_date_time_stamp_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_order_types_60_date_time_stamp_ts_id ON summary_order_types_60 USING btree (date, time_stamp, ts_id);


--
-- Name: summary_order_types_60_date_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_order_types_60_date_ts_id ON summary_order_types_60 USING btree (date, ts_id);


--
-- Name: summary_order_types_60_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_order_types_60_ecn_id ON summary_order_types_60 USING btree (ecn_id);


--
-- Name: summary_order_types_60_time_stamp; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_order_types_60_time_stamp ON summary_order_types_60 USING btree (time_stamp);


--
-- Name: summary_order_types_60_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_order_types_60_ts_id ON summary_order_types_60 USING btree (ts_id);


--
-- Name: summary_order_types_all; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_order_types_all ON summary_order_types USING btree (date, time_stamp, ts_id, ecn_id);


--
-- Name: summary_order_types_all_1; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_order_types_all_1 ON summary_order_types USING btree (date, ts_id, ecn_id);


--
-- Name: summary_order_types_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_order_types_date ON summary_order_types USING btree (date);


--
-- Name: summary_order_types_date_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_order_types_date_ecn_id ON summary_order_types USING btree (date, ecn_id);


--
-- Name: summary_order_types_date_time_stamp_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_order_types_date_time_stamp_ecn_id ON summary_order_types USING btree (date, time_stamp, ecn_id);


--
-- Name: summary_order_types_date_time_stamp_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_order_types_date_time_stamp_ts_id ON summary_order_types USING btree (date, time_stamp, ts_id);


--
-- Name: summary_order_types_date_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_order_types_date_ts_id ON summary_order_types USING btree (date, ts_id);


--
-- Name: summary_order_types_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_order_types_ecn_id ON summary_order_types USING btree (ecn_id);


--
-- Name: summary_order_types_time_stamp; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_order_types_time_stamp ON summary_order_types USING btree (time_stamp);


--
-- Name: summary_order_types_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_order_types_ts_id ON summary_order_types USING btree (ts_id);


--
-- Name: summary_profit_losses_all; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_profit_losses_all ON summary_profit_losses USING btree (date, time_stamp);


--
-- Name: summary_profit_losses_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_profit_losses_date ON summary_profit_losses USING btree (date);


--
-- Name: summary_profit_losses_time_stamp; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_profit_losses_time_stamp ON summary_profit_losses USING btree (time_stamp);


--
-- Name: process_insert_entry_exits; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER process_insert_entry_exits AFTER INSERT ON summary_entry_exits FOR EACH ROW EXECUTE PROCEDURE process_insert_entry_exits();


--
-- Name: process_insert_entry_exits_15; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER process_insert_entry_exits_15 AFTER INSERT ON summary_entry_exits_15 FOR EACH ROW EXECUTE PROCEDURE process_insert_entry_exits_15();


--
-- Name: process_insert_entry_exits_30; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER process_insert_entry_exits_30 AFTER INSERT ON summary_entry_exits_30 FOR EACH ROW EXECUTE PROCEDURE process_insert_entry_exits_30();


--
-- Name: process_insert_exit_order_shares; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER process_insert_exit_order_shares AFTER INSERT ON summary_exit_order_shares FOR EACH ROW EXECUTE PROCEDURE process_insert_exit_order_shares();


--
-- Name: process_insert_exit_order_shares_15; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER process_insert_exit_order_shares_15 AFTER INSERT ON summary_exit_order_shares_15 FOR EACH ROW EXECUTE PROCEDURE process_insert_exit_order_shares_15();


--
-- Name: process_insert_exit_order_shares_30; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER process_insert_exit_order_shares_30 AFTER INSERT ON summary_exit_order_shares_30 FOR EACH ROW EXECUTE PROCEDURE process_insert_exit_order_shares_30();


--
-- Name: process_insert_launch_entry_ecns; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER process_insert_launch_entry_ecns AFTER INSERT ON summary_launch_entry_ecns FOR EACH ROW EXECUTE PROCEDURE process_insert_launch_entry_ecns();


--
-- Name: process_insert_launch_entry_ecns_15; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER process_insert_launch_entry_ecns_15 AFTER INSERT ON summary_launch_entry_ecns_15 FOR EACH ROW EXECUTE PROCEDURE process_insert_launch_entry_ecns_15();


--
-- Name: process_insert_launch_entry_ecns_30; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER process_insert_launch_entry_ecns_30 AFTER INSERT ON summary_launch_entry_ecns_30 FOR EACH ROW EXECUTE PROCEDURE process_insert_launch_entry_ecns_30();


--
-- Name: process_insert_launch_exit_ecns; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER process_insert_launch_exit_ecns AFTER INSERT ON summary_launch_exit_ecns FOR EACH ROW EXECUTE PROCEDURE process_insert_launch_exit_ecns();


--
-- Name: process_insert_launch_exit_ecns_15; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER process_insert_launch_exit_ecns_15 AFTER INSERT ON summary_launch_exit_ecns_15 FOR EACH ROW EXECUTE PROCEDURE process_insert_launch_exit_ecns_15();


--
-- Name: process_insert_launch_exit_ecns_30; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER process_insert_launch_exit_ecns_30 AFTER INSERT ON summary_launch_exit_ecns_30 FOR EACH ROW EXECUTE PROCEDURE process_insert_launch_exit_ecns_30();


--
-- Name: process_insert_order_types; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER process_insert_order_types AFTER INSERT ON summary_order_types FOR EACH ROW EXECUTE PROCEDURE process_insert_order_types();


--
-- Name: process_insert_order_types_15; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER process_insert_order_types_15 AFTER INSERT ON summary_order_types_15 FOR EACH ROW EXECUTE PROCEDURE process_insert_order_types_15();


--
-- Name: process_insert_order_types_30; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER process_insert_order_types_30 AFTER INSERT ON summary_order_types_30 FOR EACH ROW EXECUTE PROCEDURE process_insert_order_types_30();


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

