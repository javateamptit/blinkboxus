BEGIN;

-- Upgrade
INSERT INTO release_versions(module_id, version) VALUES (2, '20131014');

-- Insert (TS9) into trade_servers table
INSERT INTO trade_servers(
            ts_id, description, ip, port, book_arca, book_nasdaq, book_bats, 
            arca_direct, nasdaq_ouch, bats_fix, nasdaq_rash, cqs, uqdf, nasdaq_bx, 
            ouch_bx, rash_bx, book_nyse_open, nyse_ccg, book_edgx, edgx_direct)
    VALUES (9, 'carblink9', '10.148.144.13', 30001, 1, 1, 1, 
            1, 1, 1, 1, 1, 1, 1, 
            1, 1, 1, 1, 1, 1);


-- Function: _insert_tt_activities()

-- DROP FUNCTION _insert_tt_activities();

CREATE OR REPLACE FUNCTION _insert_tt_activities()
  RETURNS trigger AS
$BODY$
    DECLARE
		userVal                 text;
		typeVal                 integer;
		dateVal                 date;
		timeVal                 time;
		currentState    text[];
		_currentState   integer;
		status                  text[];
		_status                 integer;
		action                  text[];
		_action                 integer;
		orderId                 text;
		actionType              integer;
		symbol                  text;
		buyingPower     double precision;
		onWhich                 text[];
		_onWhich                integer;
		descriptionVal  text;
		_tsId			integer;
		_tsDescription  text;
		_maxShares		integer;
		_maxConsectutiveLoss integer;
		_maxStuck		integer;
		_count			integer;
		_activityAction text;
		_minSpreadTS	text;
		_maxLoserTS		text;
		_maxBuyingPower text;
		_checkUserOrServer integer;
	
	BEGIN
		timeVal := NEW.time_stamp;
		userVal := initcap(trim(NEW.username));
		typeVal = NEW.activity_type;
		dateVal = (NEW.date);
		timeVal = (NEW.time_stamp);

		--Init status
		status[0] := 'Off-Line';
		status[1] := 'Active';
		status[2] := 'Active Assistant';
		status[3] := 'Monitoring';

		currentState[0] := '[Off-Line]';
		currentState[1] := '[Active]';
		currentState[2] := '[Active Assistant]';
		currentState[3] := '[Monitoring]';

		--Init action
		action[0] := 'Add';
		action[1] := 'Remove';

		--Init enable/disable trading
		onWhich[100] := 'all trading on TS1';
		onWhich[101] := 'ARCA trading on TS1';
		onWhich[102] := 'OUCH trading on TS1';
		onWhich[103] := 'NYSE trading on TS1';
		onWhich[104] := 'RASH trading on TS1';
		onWhich[105] := 'BATZ trading on TS1';
		onWhich[106] := 'EDGX trading on TS1';
		onWhich[107] := 'EDGA trading on TS1';

		onWhich[200] := 'all trading on TS2';
		onWhich[201] := 'ARCA trading on TS2';
		onWhich[202] := 'OUCH trading on TS2';
		onWhich[203] := 'NYSE trading on TS2';
		onWhich[204] := 'RASH trading on TS2';
		onWhich[205] := 'BATZ trading on TS2';
		onWhich[206] := 'EDGX trading on TS2';
		onWhich[207] := 'EDGA trading on TS2';

		onWhich[300] := 'all trading on TS3';
		onWhich[301] := 'ARCA trading on TS3';
		onWhich[302] := 'OUCH trading on TS3';
		onWhich[303] := 'NYSE trading on TS3';
		onWhich[304] := 'RASH trading on TS3';
		onWhich[305] := 'BATZ trading on TS3';
		onWhich[306] := 'EDGX trading on TS3';
		onWhich[307] := 'EDGA trading on TS3';

		onWhich[400] := 'all trading on TS4';
		onWhich[401] := 'ARCA trading on TS4';
		onWhich[402] := 'OUCH trading on TS4';
		onWhich[403] := 'NYSE trading on TS4';
		onWhich[404] := 'RASH trading on TS4';
		onWhich[405] := 'BATZ trading on TS4';
		onWhich[406] := 'EDGX trading on TS4';
		onWhich[407] := 'EDGA trading on TS4';

		onWhich[500] := 'all trading on TS5';
		onWhich[501] := 'ARCA trading on TS5';
		onWhich[502] := 'OUCH trading on TS5';
		onWhich[503] := 'NYSE trading on TS5';
		onWhich[504] := 'RASH trading on TS5';
		onWhich[505] := 'BATZ trading on TS5';
		onWhich[506] := 'EDGX trading on TS5';
		onWhich[507] := 'EDGA trading on TS5';

		onWhich[600] := 'all trading on TS6';
		onWhich[601] := 'ARCA trading on TS6';
		onWhich[602] := 'OUCH trading on TS6';
		onWhich[603] := 'NYSE trading on TS6';
		onWhich[604] := 'RASH trading on TS6';
		onWhich[605] := 'BATZ trading on TS6';
		onWhich[606] := 'EDGX trading on TS6';
		onWhich[607] := 'EDGA trading on TS6';
		
		onWhich[700] := 'all trading on TS7';
		onWhich[701] := 'ARCA trading on TS7';
		onWhich[702] := 'OUCH trading on TS7';
		onWhich[703] := 'NYSE trading on TS7';
		onWhich[704] := 'RASH trading on TS7';
		onWhich[705] := 'BATZ trading on TS7';
		onWhich[706] := 'EDGX trading on TS7';
		onWhich[707] := 'EDGA trading on TS7';
		
		onWhich[800] := 'all trading on TS8';
		onWhich[801] := 'ARCA trading on TS8';
		onWhich[802] := 'OUCH trading on TS8';
		onWhich[803] := 'NYSE trading on TS8';
		onWhich[804] := 'RASH trading on TS8';
		onWhich[805] := 'BATZ trading on TS8';
		onWhich[806] := 'EDGX trading on TS8';
		onWhich[807] := 'EDGA trading on TS8';

		onWhich[900] := 'all trading on TS9';
		onWhich[901] := 'ARCA trading on TS9';
		onWhich[902] := 'OUCH trading on TS9';
		onWhich[903] := 'NYSE trading on TS9';
		onWhich[904] := 'RASH trading on TS9';
		onWhich[905] := 'BATZ trading on TS9';
		onWhich[906] := 'EDGX trading on TS9';
		onWhich[907] := 'EDGA trading on TS9';

		onWhich[99] := 'PM trading';

		--Init connect/disconnect all books and orders, enable/disable trading all orders on all TS
		onWhich[0] := 'trading all orders on all TSs and PM';
		onWhich[1] := 'all books and orders on all TSs and PM';

		--Init connect/disconnect BBO
		onWhich[110] := 'BBO on TS1';
		onWhich[111] := 'CQS on TS1';
		onWhich[112] := 'UQDF on TS1';

		onWhich[210] := 'BBO on TS2';
		onWhich[211] := 'CQS on TS2';
		onWhich[212] := 'UQDF on TS2';

		onWhich[310] := 'BBO on TS3';
		onWhich[311] := 'CQS on TS3';
		onWhich[312] := 'UQDF on TS3';

		onWhich[410] := 'BBO on TS4';
		onWhich[411] := 'CQS on TS4';
		onWhich[412] := 'UQDF on TS4';

		onWhich[510] := 'BBO on TS5';
		onWhich[511] := 'CQS on TS5';
		onWhich[512] := 'UQDF on TS5';

		onWhich[610] := 'BBO on TS6';
		onWhich[611] := 'CQS on TS6';
		onWhich[612] := 'UQDF on TS6';
		
		onWhich[710] := 'BBO on TS7';
		onWhich[711] := 'CQS on TS7';
		onWhich[712] := 'UQDF on TS7';
		
		onWhich[810] := 'BBO on TS8';
		onWhich[811] := 'CQS on TS8';
		onWhich[812] := 'UQDF on TS8';
		
		onWhich[910] := 'BBO on TS9';
		onWhich[911] := 'CQS on TS9';
		onWhich[912] := 'UQDF on TS9';

		onWhich[80] := 'BBO on PM';
		onWhich[81] := 'CTS on PM';
		onWhich[82] := 'UTDF on PM';
		onWhich[83] := 'CQS on PM';
		onWhich[84] := 'UQDF on PM';

		--Init connect/disconnect book
		onWhich[120] := 'all books on TS1';
		onWhich[121] := 'ARCA book on TS1';
		onWhich[122] := 'NDAQ book on TS1';
		onWhich[123] := 'NYSE book on TS1';
		onWhich[124] := 'BATS book on TS1';
		onWhich[125] := 'EDGX book on TS1';
		onWhich[126] := 'EDGA book on TS1';

		onWhich[220] := 'all books on TS2';
		onWhich[221] := 'ARCA book on TS2';
		onWhich[222] := 'NDAQ book on TS2';
		onWhich[223] := 'NYSE book on TS2';
		onWhich[224] := 'BATS book on TS2';
		onWhich[225] := 'EDGX book on TS2';
		onWhich[226] := 'EDGA book on TS2';

		onWhich[320] := 'all books on TS3';
		onWhich[321] := 'ARCA book on TS3';
		onWhich[322] := 'NDAQ book on TS3';
		onWhich[323] := 'NYSE book on TS3';
		onWhich[324] := 'BATS book on TS3';
		onWhich[325] := 'EDGX book on TS3';
		onWhich[326] := 'EDGA book on TS3';

		onWhich[420] := 'all books on TS4';
		onWhich[421] := 'ARCA book on TS4';
		onWhich[422] := 'NDAQ book on TS4';
		onWhich[423] := 'NYSE book on TS4';
		onWhich[424] := 'BATS book on TS4';
		onWhich[425] := 'EDGX book on TS4';
		onWhich[426] := 'EDGA book on TS4';

		onWhich[520] := 'all books on TS5';
		onWhich[521] := 'ARCA book on TS5';
		onWhich[522] := 'NDAQ book on TS5';
		onWhich[523] := 'NYSE book on TS5';
		onWhich[524] := 'BATS book on TS5';
		onWhich[525] := 'EDGX book on TS5';
		onWhich[526] := 'EDGA book on TS5';

		onWhich[620] := 'all books on TS6';
		onWhich[621] := 'ARCA book on TS6';
		onWhich[622] := 'NDAQ book on TS6';
		onWhich[623] := 'NYSE book on TS6';
		onWhich[624] := 'BATS book on TS6';
		onWhich[625] := 'EDGX book on TS6';
		onWhich[626] := 'EDGA book on TS6';
		
		onWhich[720] := 'all books on TS7';
		onWhich[721] := 'ARCA book on TS7';
		onWhich[722] := 'NDAQ book on TS7';
		onWhich[723] := 'NYSE book on TS7';
		onWhich[724] := 'BATS book on TS7';
		onWhich[725] := 'EDGX book on TS7';
		onWhich[726] := 'EDGA book on TS7';
		
		onWhich[820] := 'all books on TS8';
		onWhich[821] := 'ARCA book on TS8';
		onWhich[822] := 'NDAQ book on TS8';
		onWhich[823] := 'NYSE book on TS8';
		onWhich[824] := 'BATS book on TS8';
		onWhich[825] := 'EDGX book on TS8';
		onWhich[826] := 'EDGA book on TS8';
		
		onWhich[920] := 'all books on TS9';
		onWhich[921] := 'ARCA book on TS9';
		onWhich[922] := 'NDAQ book on TS9';
		onWhich[923] := 'NYSE book on TS9';
		onWhich[924] := 'BATS book on TS9';
		onWhich[925] := 'EDGX book on TS9';
		onWhich[926] := 'EDGA book on TS9';

		onWhich[750] := 'all books on PM';
		onWhich[751] := 'ARCA book on PM';
		onWhich[752] := 'NDAQ book on PM';

		--Init connect/disconnect order
		onWhich[130] := 'all orders on TS1';
		onWhich[131] := 'ARCA DIRECT on TS1';
		onWhich[132] := 'NDAQ OUCH on TS1';
		onWhich[133] := 'NYSE CCG on TS1';
		onWhich[134] := 'NDAQ RASH on TS1';
		onWhich[135] := 'BATZ BOE on TS1';
		onWhich[136] := 'EDGX on TS1';
		onWhich[137] := 'EDGA on TS1';

		onWhich[230] := 'all orders on TS2';
		onWhich[231] := 'ARCA DIRECT on TS2';
		onWhich[232] := 'NDAQ OUCH on TS2';
		onWhich[233] := 'NYSE CCG on TS2';
		onWhich[234] := 'NDAQ RASH on TS2';
		onWhich[235] := 'BATZ BOE on TS2';
		onWhich[236] := 'EDGX on TS2';
		onWhich[237] := 'EDGA on TS2';

		onWhich[330] := 'all orders on TS3';
		onWhich[331] := 'ARCA DIRECT on TS3';
		onWhich[332] := 'NDAQ OUCH on TS3';
		onWhich[333] := 'NYSE CCG on TS3';
		onWhich[334] := 'NDAQ RASH on TS3';
		onWhich[335] := 'BATZ BOE on TS3';
		onWhich[336] := 'EDGX on TS3';
		onWhich[337] := 'EDGA on TS3';

		onWhich[430] := 'all orders on TS4';
		onWhich[431] := 'ARCA DIRECT on TS4';
		onWhich[432] := 'NDAQ OUCH on TS4';
		onWhich[433] := 'NYSE CCG on TS4';
		onWhich[434] := 'NDAQ RASH on TS4';
		onWhich[435] := 'BATZ BOE on TS4';
		onWhich[436] := 'EDGX on TS4';
		onWhich[437] := 'EDGA on TS4';

		onWhich[530] := 'all orders on TS5';
		onWhich[531] := 'ARCA DIRECT on TS5';
		onWhich[532] := 'NDAQ OUCH on TS5';
		onWhich[533] := 'NYSE CCG on TS5';
		onWhich[534] := 'NDAQ RASH on TS5';
		onWhich[535] := 'BATZ BOE on TS5';
		onWhich[536] := 'EDGX on TS5';
		onWhich[537] := 'EDGA on TS5';

		onWhich[630] := 'all orders on TS6';
		onWhich[631] := 'ARCA DIRECT on TS6';
		onWhich[632] := 'NDAQ OUCH on TS6';
		onWhich[633] := 'NYSE CCG on TS6';
		onWhich[634] := 'NDAQ RASH on TS6';
		onWhich[635] := 'BATZ BOE on TS6';
		onWhich[636] := 'EDGX on TS6';
		onWhich[637] := 'EDGA on TS6';
		
		onWhich[730] := 'all orders on TS7';
		onWhich[731] := 'ARCA DIRECT on TS7';
		onWhich[732] := 'NDAQ OUCH on TS7';
		onWhich[733] := 'NYSE CCG on TS7';
		onWhich[734] := 'NDAQ RASH on TS7';
		onWhich[735] := 'BATZ BOE on TS7';
		onWhich[736] := 'EDGX on TS7';
		onWhich[737] := 'EDGA on TS7';
		
		onWhich[830] := 'all orders on TS8';
		onWhich[831] := 'ARCA DIRECT on TS8';
		onWhich[832] := 'NDAQ OUCH on TS8';
		onWhich[833] := 'NYSE CCG on TS8';
		onWhich[834] := 'NDAQ RASH on TS8';
		onWhich[835] := 'BATZ BOE on TS8';
		onWhich[836] := 'EDGX on TS8';
		onWhich[837] := 'EDGA on TS8';
		
		onWhich[930] := 'all orders on TS9';
		onWhich[931] := 'ARCA DIRECT on TS9';
		onWhich[932] := 'NDAQ OUCH on TS9';
		onWhich[933] := 'NYSE CCG on TS9';
		onWhich[934] := 'NDAQ RASH on TS9';
		onWhich[935] := 'BATZ BOE on TS9';
		onWhich[936] := 'EDGX on TS9';
		onWhich[937] := 'EDGA on TS9';

		onWhich[10] := 'all orders on AS';
		onWhich[11] := 'ARCA FIX on AS';
		onWhich[12] := 'NDAQ RASH on AS';
		
		onWhich[50] := 'all orders on PM';
		onWhich[51] := 'ARCA FIX on PM';
		onWhich[52] := 'NDAQ RASH on PM';

		-- Connect to AS
		IF (typeVal = 1) THEN
			SELECT substring(NEW.log_detail from 1 for 1)::integer INTO _currentState FROM tt_event_log;
			descriptionVal := userVal || ' ' || currentState[_currentState] || ' Connect to AS ' || dateVal || ' ' || timeVal;

		-- Switch trader role
		ELSIF (typeVal = 2) THEN
			SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer INTO _currentState, _status FROM tt_event_log;
			descriptionVal := userVal || ' ' || currentState[_currentState] || ' Client became ' || status[_status] || ' Trader ' || dateVal || ' ' || timeVal;

		-- Disconnect from AS
		ELSIF (typeVal = 3) THEN
			SELECT substring(NEW.log_detail from 1 for 1)::integer INTO _currentState FROM tt_event_log;
			descriptionVal := userVal || ' ' || currentState[_currentState] || ' Disconnect from AS ' || dateVal || ' ' || timeVal;

		-- Sent Manual order
		ELSIF (typeVal = 4) THEN
			SELECT substring(NEW.log_detail from 1 for 1)::integer, trim(substring(NEW.log_detail from 3 for 3)) INTO _currentState, orderId FROM tt_event_log;
			descriptionVal := userVal || ' ' || currentState[_currentState] || ' Sent Manual Order, OrderID = ' || orderId || ' ' || dateVal || ' ' || timeVal;

		-- Ignored stock list for TS
		ELSIF (typeVal = 5) THEN
			SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, trim(substring(NEW.log_detail from 5 for 8)) INTO _currentState, _action, symbol FROM tt_event_log;
			descriptionVal := userVal || ' ' || currentState[_currentState] || ' ' || action[_action] || ' ignored stock list for TS, symbol = ' || symbol || ' ' || dateVal || ' ' || timeVal;

		-- Halted stock list
		ELSIF (typeVal = 6) THEN
			SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, trim(substring(NEW.log_detail from 5 for 8)) INTO _currentState, _action, symbol FROM tt_event_log;
			descriptionVal := userVal || ' ' || currentState[_currentState] || ' ' || action[_action] || ' halted stock list, symbol = ' || symbol || ' ' || dateVal || ' ' || timeVal;

		-- PM engage/disengage
		ELSIF (typeVal = 7) THEN
			SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, trim(substring(NEW.log_detail from 5 for 8)) INTO _currentState, actionType, symbol FROM tt_event_log;
			-- engage
			IF (actionType = 0) THEN
				descriptionVal := userVal || ' ' || currentState[_currentState] || ' PM engage, symbol = ' || symbol || ' ' || dateVal || ' ' || timeVal;
			-- disengage
			ELSIF (actionType = 1) THEN
				descriptionVal := userVal || ' ' || currentState[_currentState] || ' PM disengage, symbol = ' || symbol || ' ' || dateVal || ' ' || timeVal;
			ELSE
			END IF;

		-- set buying power
		-- use _count variable for accountIndex
		ELSIF (typeVal = 8) THEN
			SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1):: integer, substring(NEW.log_detail, 5)::double precision INTO _currentState, _count, buyingPower FROM tt_event_log;
			
			IF (_count = 0) THEN
				_tsDescription := ' on first account ';
			ELSIF (_count = 1) THEN
				_tsDescription := ' on second account ';
			ELSIF (_count = 2) THEN
				_tsDescription := ' on third account ';
			ELSE
				_tsDescription := ' on fourth account ';
			END IF;
				
			descriptionVal := userVal || ' ' || currentState[_currentState] || ' Set Buying Power = ' || buyingPower || _tsDescription || ' ' || dateVal || ' ' || timeVal;

		-- Enable/Disable trading
		ELSIF (typeVal = 9) THEN
			SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, substring(NEW.log_detail from 5 for 3) INTO _currentState, actionType, _onWhich FROM tt_event_log;
			-- Enable
			IF (actionType = 0) THEN
				descriptionVal := userVal || ' ' || currentState[_currentState] || ' Enable ' || onWhich[_onWhich] || ' ' || dateVal || ' ' || timeVal;
			-- Disable
			ELSIF (actionType = 1) THEN
				descriptionVal := userVal || ' ' || currentState[_currentState] || ' Disable ' || onWhich[_onWhich] || ' ' || dateVal || ' ' || timeVal;
			ELSE
			END IF;

		-- Ignore stock list for PM
		ELSIF (typeVal = 10) THEN
			SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, trim(substring(NEW.log_detail from 5 for 8)) INTO _currentState, _action, symbol FROM tt_event_log;
			descriptionVal := userVal || ' ' || currentState[_currentState] || ' ' || action[_action] || ' ignored stock list for PM, symbol = ' || symbol || ' ' || dateVal || ' ' || timeVal;

		-- Connect/disconnect book(s) on TS and PM
		ELSIF (typeVal = 11) THEN
			SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, substring(NEW.log_detail from 5 for 3) INTO _currentState, actionType, _onWhich FROM tt_event_log;
			-- Connect
			IF (actionType = 0) THEN
				descriptionVal := userVal || ' ' || currentState[_currentState] || ' Connect to ' || onWhich[_onWhich] || ' ' || dateVal || ' ' || timeVal;
			-- Disconnect
			ELSIF (actionType = 1) THEN
				descriptionVal := userVal || ' ' || currentState[_currentState] || ' Disconnect and disable trading ' || onWhich[_onWhich] || ' ' || dateVal || ' ' || timeVal;
			ELSE
			END IF;

		-- Connect/disconnect order(s) on TS, AS and PM
		ELSIF (typeVal = 12) THEN
			SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, substring(NEW.log_detail from 5 for 3) INTO _currentState, actionType, _onWhich FROM tt_event_log;
			-- Connect
			IF (actionType = 0) THEN
				 descriptionVal := userVal || ' ' || currentState[_currentState] || ' Connect to ' || onWhich[_onWhich] || ' ' || dateVal || ' ' || timeVal;
			-- Disconnect
			ELSIF (actionType = 1) THEN
				 descriptionVal := userVal || ' ' || currentState[_currentState] || ' Disconnect and disable trading ' || onWhich[_onWhich] || ' ' || dateVal || ' ' || timeVal;
			ELSE
			END IF;

		-- connect/disconnect BBO on TS and PM
		ELSIF (typeVal = 13) THEN
			SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, substring(NEW.log_detail from 5 for 3) INTO _currentState, actionType, _onWhich FROM tt_event_log;
			-- Connect
			IF (actionType = 0) THEN
				 descriptionVal := userVal || ' ' || currentState[_currentState] || ' Connect to ' || onWhich[_onWhich] || ' ' || dateVal || ' ' || timeVal;
			-- Disconnect
			ELSIF (actionType = 1) THEN
				 descriptionVal := userVal || ' ' || currentState[_currentState] || ' Disconnect ' || onWhich[_onWhich] || ' ' || dateVal || ' ' || timeVal;
			ELSE
			END IF;
			
		-- enable/disable ISO trading on TS
		ELSIF (typeVal = 20) THEN
			SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, substring(NEW.log_detail, 5) INTO _currentState, actionType, _tsId FROM tt_event_log;
			
			IF (_tsId = -1) THEN
				-- Enable
				IF (actionType = 1) THEN
					descriptionVal := userVal || ' ' || currentState[_currentState] || ' Enable ISO trading' || ' ' || dateVal || ' ' || timeVal;
				-- Disable
				ELSIF (actionType = 0) THEN
					descriptionVal := userVal || ' ' || currentState[_currentState] || ' Disable ISO trading' || ' ' || dateVal || ' ' || timeVal;
				ELSE
				END IF;
			ELSE
				-- Enable
				IF (actionType = 1) THEN
					descriptionVal := userVal || ' ' || currentState[_currentState] || ' Enable ISO trading on TS' || _tsId || ' ' || dateVal || ' ' || timeVal;
				-- Disable
				ELSIF (actionType = 0) THEN
					descriptionVal := userVal || ' ' || currentState[_currentState] || ' Disable ISO trading on TS' || _tsId || ' ' || dateVal || ' ' || timeVal;
				ELSE
				END IF;
			END IF;
		
		ELSE
		
		SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer INTO _currentState, _tsId FROM tt_event_log;
		
		IF (_tsId = 1) THEN _tsDescription := ' on TS1 ';
		ELSIF (_tsId = 2) THEN _tsDescription := ' on TS2 ';
		ELSIF (_tsId = 3) THEN _tsDescription := ' on TS3 ';
		ELSIF (_tsId = 4) THEN _tsDescription := ' on TS4 ';
		ELSIF (_tsId = 5) THEN _tsDescription := ' on TS5 ';
		ELSIF (_tsId = 6) THEN _tsDescription := ' on TS6 ';
		ELSIF (_tsId = 7) THEN _tsDescription := ' on TS7 ';
		ELSIF (_tsId = 8) THEN _tsDescription := ' on TS8 ';
		ELSIF (_tsId = 9) THEN _tsDescription := ' on TS9 ';
		ELSE
		END IF;
		
		SELECT count(*) INTO _checkUserOrServer FROM user_login_info WHERE username = NEW.username;
		
		-- Check activity type was inserted on SOD or NOT
		SELECT count(*) INTO _count FROM tt_event_log WHERE date = dateVal AND activity_type = typeVal AND username = NEW.username;
		
		-- ACTIVITY_TYPE_MAX_CONSECUTIVE_LOSS_SETTING 14
		IF (typeVal = 14) THEN
			IF (_checkUserOrServer = 0) THEN
				IF (_count = 0) THEN
					_activityAction := ' SOD: Set max consecutive loss = ';
				ELSE RETURN OLD;
				END IF;
			ELSE
				_activityAction := ' Set max consecutive loss = ';
			END IF;
			
			SELECT substring(NEW.log_detail, 5) INTO _maxConsectutiveLoss FROM tt_event_log ;
	
			descriptionVal := userVal || ' ' || currentState[_currentState] || _activityAction ||  _maxConsectutiveLoss || _tsDescription || dateVal || ' ' || timeVal;
		
		-- ACTIVITY_TYPE_MAX_STUCKS_SETTING 15
		ELSIF (typeVal = 15) THEN
			IF (_checkUserOrServer = 0) THEN
				IF (_count = 0) THEN
					_activityAction := ' SOD: Set max stucks = ';
				ELSE RETURN OLD;
				END IF;
			ELSE
				_activityAction := ' Set max stucks = ';
			END IF;

			SELECT substring(NEW.log_detail, 5) INTO _maxStuck FROM tt_event_log;
		
			descriptionVal := userVal || ' ' || currentState[_currentState] || _activityAction || _maxStuck || _tsDescription || dateVal || ' ' || timeVal;
		
		-- ACTIVITY_TYPE_CURRENCY_MIN_SPREAD_TS_SETTING 16
		ELSIF (typeVal = 16) THEN
			IF (_checkUserOrServer = 0) THEN
				IF (_count = 0) THEN
					_activityAction := ' SOD: Set min spread = ';
				ELSE
					_activityAction := ' Set min spread = ';
				END IF;
			ELSE
				_activityAction := ' Set min spread = ';
			END IF;
		
			SELECT substring(NEW.log_detail, 5) INTO _minSpreadTS FROM tt_event_log;
			
			descriptionVal := userVal || ' ' || currentState[_currentState] || _activityAction || _minSpreadTS || _tsDescription || dateVal || ' ' || timeVal;
	
		-- ACTIVITY_TYPE_CURRENCY_MAX_LOSER_TS_SETTING 17
		ELSIF (typeVal = 17) THEN
			IF (_checkUserOrServer = 0) THEN
				IF (_count = 0) THEN
					_activityAction := ' SOD: Set max loser = ';
				ELSE RETURN OLD;
				END IF;
			ELSE
				_activityAction := ' Set max loser = ';
			END IF;
		
			SELECT substring(NEW.log_detail, 5) INTO _maxLoserTS FROM tt_event_log;
		
			descriptionVal := userVal || ' ' || currentState[_currentState] || _activityAction || _maxLoserTS || _tsDescription || dateVal || ' ' || timeVal;
		
		-- ACTIVITY_TYPE_MAX_BUYING_POWER_SETTING 18
		ELSIF (typeVal = 18) THEN
			IF (_checkUserOrServer = 0) THEN
				IF (_count = 0) THEN
					_activityAction := ' SOD: Set max buying power = ';
				ELSE RETURN OLD;
				END IF;
			ELSE
				_activityAction := ' Set max buying power = ';
			END IF;
		
			SELECT substring(NEW.log_detail, 5) INTO _maxBuyingPower FROM tt_event_log;
		
			descriptionVal := userVal || ' ' || currentState[_currentState] || _activityAction || _maxBuyingPower || _tsDescription || dateVal || ' ' || timeVal;
		
		-- ACTIVITY_TYPE_MAX_SHARES_SETTING 19
		ELSIF (typeVal = 19) THEN
			IF (_checkUserOrServer = 0) THEN
				IF (_count = 0) THEN
					_activityAction := ' SOD: Set max shares = ';
				ELSE RETURN OLD;
				END IF;
			ELSE
				_activityAction := ' Set max shares = ';
			END IF;
		
			SELECT substring(NEW.log_detail, 5) INTO _maxShares FROM tt_event_log;
		
			descriptionVal := userVal || ' ' || currentState[_currentState] || _activityAction || _maxShares || _tsDescription || dateVal || ' ' || timeVal;
		
		ELSE
		END IF;

		END IF;
		NEW.description := descriptionVal;
		RETURN NEW;
	END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION _insert_tt_activities()
  OWNER TO postgres;


COMMIT;
