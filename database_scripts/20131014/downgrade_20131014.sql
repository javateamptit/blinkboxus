BEGIN;

-- Downgrade
DELETE FROM release_versions WHERE module_id = 2 AND version = '20131014';

DELETE FROM trade_servers WHERE ts_id = 9;

COMMIT;