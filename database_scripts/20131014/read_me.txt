* Download all scripts to upgrade:
- Please copy the folder "20131014" to the EAA database host
- Change into "20131014" directory (on the EAA database host)

* The update is needed to run with new release, all databases will upgrade (eaa10_201310 -> eaa10_201206):
1) To upgrade, please run the command:
	$chmod 744 upgrade_20131014.sh
	$./upgrade_20131014.sh
	
2) If we have "critical issues" of TT and need downgrade it, please run the command:
	$chmod 744 downgrade_20131014.sh
	$./downgrade_20131014.sh

