#!/bin/sh

# DB scripts
db_script=database_scripts/20130416/downgrade_20130416.sql

# eaa10_201304
db_name=eaa10_201304
echo "Upgrading $db_script on $db_name ..."
cmd=`psql -U postgres $db_name -f $db_script`
echo "Result: $cmd"
if [ -n "$cmd" ];
then
	if [[ "$cmd" == *ROLLBACK* ]]
	then
		echo "Fail! Skiped db script $db_script"
		exit;
	else
		echo "Successful upgrade $db_name"
	fi
else
	echo "Fail! Skiped db script $db_script"
	exit;
fi

# eaa10_201303
db_name=eaa10_201303
echo "Upgrading $db_script on $db_name ..."
cmd=`psql -U postgres $db_name -f $db_script`
echo "Result: $cmd"
if [ -n "$cmd" ];
then
	if [[ "$cmd" == *ROLLBACK* ]]
	then
		echo "Fail! Skiped db script $db_script"
		exit;
	else
		echo "Successful upgrade $db_name"
	fi
else
	echo "Fail! Skiped db script $db_script"
	exit;
fi

# eaa10_201302
db_name=eaa10_201302
echo "Upgrading $db_script on $db_name ..."
cmd=`psql -U postgres $db_name -f $db_script`
echo "Result: $cmd"
if [ -n "$cmd" ];
then
	if [[ "$cmd" == *ROLLBACK* ]]
	then
		echo "Fail! Skiped db script $db_script"
		exit;
	else
		echo "Successful upgrade $db_name"
	fi
else
	echo "Fail! Skiped db script $db_script"
	exit;
fi

# eaa10_201301
db_name=eaa10_201301
echo "Upgrading $db_script on $db_name ..."
cmd=`psql -U postgres $db_name -f $db_script`
echo "Result: $cmd"
if [ -n "$cmd" ];
then
	if [[ "$cmd" == *ROLLBACK* ]]
	then
		echo "Fail! Skiped db script $db_script"
		exit;
	else
		echo "Successful upgrade $db_name"
	fi
else
	echo "Fail! Skiped db script $db_script"
	exit;
fi

# eaa10_201212
db_name=eaa10_201212
echo "Upgrading $db_script on $db_name ..."
cmd=`psql -U postgres $db_name -f $db_script`
echo "Result: $cmd"
if [ -n "$cmd" ];
then
	if [[ "$cmd" == *ROLLBACK* ]]
	then
		echo "Fail! Skiped db script $db_script"
		exit;
	else
		echo "Successful upgrade $db_name"
	fi
else
	echo "Fail! Skiped db script $db_script"
	exit;
fi

# eaa10_201211
db_name=eaa10_201211
echo "Upgrading $db_script on $db_name ..."
cmd=`psql -U postgres $db_name -f $db_script`
echo "Result: $cmd"
if [ -n "$cmd" ];
then
	if [[ "$cmd" == *ROLLBACK* ]]
	then
		echo "Fail! Skiped db script $db_script"
		exit;
	else
		echo "Successful upgrade $db_name"
	fi
else
	echo "Fail! Skiped db script $db_script"
	exit;
fi

# eaa10_201210
db_name=eaa10_201210
echo "Upgrading $db_script on $db_name ..."
cmd=`psql -U postgres $db_name -f $db_script`
echo "Result: $cmd"
if [ -n "$cmd" ];
then
	if [[ "$cmd" == *ROLLBACK* ]]
	then
		echo "Fail! Skiped db script $db_script"
		exit;
	else
		echo "Successful upgrade $db_name"
	fi
else
	echo "Fail! Skiped db script $db_script"
	exit;
fi

# eaa10_201209
db_name=eaa10_201209
echo "Upgrading $db_script on $db_name ..."
cmd=`psql -U postgres $db_name -f $db_script`
echo "Result: $cmd"
if [ -n "$cmd" ];
then
	if [[ "$cmd" == *ROLLBACK* ]]
	then
		echo "Fail! Skiped db script $db_script"
		exit;
	else
		echo "Successful upgrade $db_name"
	fi
else
	echo "Fail! Skiped db script $db_script"
	exit;
fi

# eaa10_201208
db_name=eaa10_201208
echo "Upgrading $db_script on $db_name ..."
cmd=`psql -U postgres $db_name -f $db_script`
echo "Result: $cmd"
if [ -n "$cmd" ];
then
	if [[ "$cmd" == *ROLLBACK* ]]
	then
		echo "Fail! Skiped db script $db_script"
		exit;
	else
		echo "Successful upgrade $db_name"
	fi
else
	echo "Fail! Skiped db script $db_script"
	exit;
fi

# eaa10_201207
db_name=eaa10_201207
echo "Upgrading $db_script on $db_name ..."
cmd=`psql -U postgres $db_name -f $db_script`
echo "Result: $cmd"
if [ -n "$cmd" ];
then
	if [[ "$cmd" == *ROLLBACK* ]]
	then
		echo "Fail! Skiped db script $db_script"
		exit;
	else
		echo "Successful upgrade $db_name"
	fi
else
	echo "Fail! Skiped db script $db_script"
	exit;
fi

# eaa10_201206
db_name=eaa10_201206
echo "Upgrading $db_script on $db_name ..."
cmd=`psql -U postgres $db_name -f $db_script`
echo "Result: $cmd"
if [ -n "$cmd" ];
then
	if [[ "$cmd" == *ROLLBACK* ]]
	then
		echo "Fail! Skiped db script $db_script"
		exit;
	else
		echo "Successful upgrade $db_name"
	fi
else
	echo "Fail! Skiped db script $db_script"
	exit;
fi
