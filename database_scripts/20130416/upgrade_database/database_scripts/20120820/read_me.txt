This text provides information related to the database script "upgrade_20120820.sql" placed at the same directory.
When run this scipt on target database, it will:
- add column "bbo_id" to "new_orders" and "bbo_infos"
- create one new view: vw_trade_view
- update one view: vw_entry_delays
- delete one function: _check_new_quotes

*The update is needed to run with new release.

How to run script:
1) To upgrade, please copy the file upgrade_20120820.sql to the EAA database host and run the command (please notice that we must run this command in the same folder with the file upgrade_20120820.sql):
	psql -U postgres -h localhost -d eaa10_201208 -f upgrade_20120820.sql