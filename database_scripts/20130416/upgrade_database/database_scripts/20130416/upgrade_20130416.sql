BEGIN;

ALTER TABLE risk_managements ALTER COLUMN total_fee SET DEFAULT 0.00;

-- Upgrade
INSERT INTO release_versions(module_id, version) VALUES (2, '20130416');

-- Function: update_rm()

-- DROP FUNCTION update_rm();

CREATE OR REPLACE FUNCTION update_rm()
  RETURNS trigger AS
$$
DECLARE
	symbolVal		text;
	leave_Shares	integer;
	order_Shares	integer;
	order_Price		double precision;
	avgPrice		double precision;
	totalQty		double precision;
	netPL			double precision;
	totalMktValue	double precision;
	rowcount		integer;
	dateVal			date;
	timeVal			time;
	sideVal			text;
	midVal			int;
	current_shares	int;
	current_price	double precision;
	fill 			record;
	fill_idVal 		int;
	sideBeginDay	character(1);
	sharesBeginDay 	integer;
	priceBeginDay 	double precision;
	accountVal 		integer;
	ecnVal 			text;
	liquidityVal	character;
	totalFee		double precision;
	temp_totalFee 	double precision;
BEGIN
	-- Get symbol name
	if TG_OP = 'INSERT' then
		fill_idVal := NEW.fill_id;
		--Check to see if insert/update the CEE note, we will return 
		if NEW.is_cee IS TRUE then
			return NEW;
		end if;
	else
		fill_idVal := OLD.fill_id;
		--Check to see if insert/update the CEE note, we will return 
		if OLD.is_cee IS TRUE then
			return OLD;
		end if;
	end if;

	SELECT trim(new_orders.symbol), new_orders.date, fills.time_stamp, new_orders.side, account, new_orders.ecn, fills.liquidity into symbolVal, dateVal, timeVal, sideVal, accountVal, ecnVal, liquidityVal
	FROM new_orders
	JOIN fills ON new_orders.oid = fills.oid
	WHERE fills.fill_id = fill_idVal;

	SELECT side, shares, price into sideBeginDay, sharesBeginDay, priceBeginDay
	FROM open_positions 
	WHERE trim(symbol::text) = trim(symbolVal) AND date = dateVal AND account = accountVal;

	if (sharesBeginDay is null) then 
		leave_Shares := 0;
		avgPrice := 0;
		netPL := 0;
		totalMktValue := 0;
		totalQty := 0;
		totalFee := 0.00;
	else
		IF (sideBeginDay = 'S') THEN
			leave_Shares := -1 * sharesBeginDay;
		ELSE
			leave_Shares := sharesBeginDay;
		END IF;	

		avgPrice := priceBeginDay;
		netPL := 0;
		totalMktValue := sharesBeginDay * priceBeginDay;
		totalQty := sharesBeginDay;
		totalFee := 0.00;

	end if;	
	
	--Travel thought fill today and 
	for fill in (select f1.shares, f1.price, f1.fill_id, substring(o.side from 1 for 1) as side, o.ecn, f1.liquidity as liquidity 
					from fills f1 
					join new_orders o on f1.oid = o.oid 
					where o.date = dateVal and trim(o.symbol) = trim(symbolVal) and o.account = accountVal ) loop
				
		select new_shares, new_price into current_shares, current_price from brokens where fill_id = fill.fill_id;
		
		if current_price is null then
			current_shares  := fill.shares;
			current_price   := fill.price;
		end if;

		IF (leave_Shares > 0) THEN -- Equivalent to LONG
			IF (fill.side = 'B') THEN -- BUY
				avgPrice := (avgPrice * leave_Shares + current_price * current_shares) / (current_shares + leave_Shares);
				leave_Shares := leave_Shares + current_shares;
			ELSE -- SELL
				netPL := netPL + ((current_price - avgPrice) * LEAST(current_shares, leave_Shares));				
				leave_Shares := leave_Shares - current_shares;
				IF (leave_Shares < 0) THEN
					avgPrice := current_price;
				ELSIF (leave_Shares = 0) THEN
					avgPrice := 0;
				END IF;
			END IF;
		ELSIF (leave_Shares < 0) THEN -- Equivalent to SHORT
			IF (fill.side = 'B') THEN -- BUY
				netPL := netPL + ((avgPrice - current_price)*LEAST(current_shares, abs(leave_Shares)));				
				leave_Shares := leave_Shares + current_shares;
				IF (leave_Shares = 0) THEN
					avgPrice := 0;
				ELSIF (leave_Shares > 0) THEN
					avgPrice := current_price;
				END IF;
			ELSE -- SELL
				avgPrice := (avgPrice*(-leave_Shares) + current_price * current_shares) / (current_shares + (-leave_Shares));
				leave_Shares := leave_Shares - current_shares;				
			END IF;
		ELSE -- leave_Shares = 0
			avgPrice := current_price;
			IF (fill.side = 'B') THEN
				leave_Shares := current_shares;
			ELSE -- Action is Sell or Short Sell
				leave_Shares := -current_shares;
			END IF;
		END IF;
	
		totalMktValue := avgPrice * abs(leave_Shares);
		totalQty := totalQty + current_shares;	
		
		if (current_shares > 0 AND fill.liquidity IS NOT NULL) then
			select calculate_total_fee(current_price, current_shares, fill.side, fill.liquidity, fill.ecn) into temp_totalFee;
			
			totalFee := totalFee + temp_totalFee;
		end if;
	end loop;
			
	-- update RM
	UPDATE risk_managements
	SET average_price = avgPrice,
		net_quantity = leave_Shares, 
		matched_profit_loss = netPL, 
		total_market_value = totalMktValue, 
		total_quantity = totalQty, 
		last_update = timeVal,
		total_fee = totalFee
	WHERE trim(symbol) = trim(symbolVal) and date = dateVal and account = accountVal;
	
	RETURN NEW;
END $$
  LANGUAGE plpgsql VOLATILE;
  
ALTER FUNCTION update_rm()
  OWNER TO postgres;

-- Function: process_fills_insert()

-- DROP FUNCTION process_fills_insert();

CREATE OR REPLACE FUNCTION process_fills_insert()
  RETURNS trigger AS
$$
DECLARE
	symbolVal	text;
	leave_Shares	integer;
	avgPrice	double precision;
	totalQty	integer;
	netPL		double precision;
	totalMktValue	double precision;
	rowcount	integer;
	dateVal		date;
	timeVal		time;
	sideVal		text;
	accountVal 	integer;
	ecnVal			text;
	liquidity	character;
	totalFee	double precision;
	temp_totalFee double precision;
BEGIN
	-- Get symbol name
	SELECT trim(symbol), date, substring(side from 1 for 1) as side, account, trim(ecn) into symbolVal, dateVal, sideVal, accountVal, ecnVal
	FROM new_orders where oid = NEW.oid;
	
	timeVal := NEW.time_stamp;

	-- Check symbol existent?
	SELECT COUNT(*) into rowcount
	FROM risk_managements
	WHERE trim(symbol) = trim(symbolVal) and date = dateVal and account = accountVal;

	-- If new symbol
	IF (rowcount = 0) THEN
		IF (sideVal = 'B') THEN
			leave_Shares := NEW.shares;
		ELSE
			leave_Shares := -NEW.shares;
		END IF;
		
		totalMktValue := abs(leave_Shares) * NEW.price;
		
		-- totalFee := calculate_total_fee(NEW.price, NEW.shares, side, NEW.liquidity, ecn);
		-- insert into RM
		SELECT calculate_total_fee(NEW.price, NEW.shares, sideVal, NEW.liquidity, ecnVal) into temp_totalFee;
		totalFee := temp_totalFee;
				
		INSERT INTO risk_managements(symbol, average_price, net_quantity, matched_profit_loss, total_market_value, total_quantity, date, last_update, account, total_fee) 
			VALUES (symbolVal , NEW.price, leave_Shares, 0.00, totalMktValue, NEW.shares, dateVal, timeVal, accountVal, totalFee);
	ELSE -- Old symbol
		SELECT average_price, net_quantity, matched_profit_loss, total_market_value, total_quantity, total_fee INTO avgPrice ,leave_Shares, netPL, totalMktValue, totalQty, totalFee
		FROM risk_managements
		WHERE trim(symbol) = (symbolVal) and date = dateVal and account = accountVal;
		
		IF (leave_Shares = 0) THEN
			avgPrice := NEW.price;
			IF (sideVal = 'B') THEN
				leave_Shares := NEW.shares;
			ELSE -- Action is Sell or Short Sell
				leave_Shares := -NEW.shares;
			END IF;
		ELSIF (leave_Shares > 0) THEN
			IF (sideVal = 'B') THEN
				avgPrice := (avgPrice * leave_Shares + NEW.price * NEW.shares) / (NEW.shares + leave_Shares);
				leave_Shares := leave_Shares + NEW.shares;
			ELSE
				netPL := netPL + ((NEW.price - avgPrice) * LEAST(NEW.shares, leave_Shares));				
				leave_Shares := leave_Shares - NEW.shares;
				IF (leave_Shares = 0) THEN
					avgPrice := 0.00;
				ELSIF (leave_Shares < 0) THEN
					avgPrice := NEW.price;
				END IF;
			END IF;
		ELSE 
			IF (sideVal = 'B') THEN
				netPL := netPL + ((avgPrice - NEW.price)*LEAST(NEW.shares, abs(leave_Shares)));				
				leave_Shares := leave_Shares + NEW.shares;
				IF (leave_Shares = 0) THEN
					avgPrice := 0.00;
				ELSIF (leave_Shares > 0) THEN
					avgPrice := NEW.price;
				END IF;
			ELSE
				avgPrice := (avgPrice*(-leave_Shares) + NEW.price * NEW.shares) / (NEW.shares + (-leave_Shares));
				leave_Shares := leave_Shares - NEW.shares;	
			END IF;
		END IF;
		totalMktValue := avgPrice * abs(leave_Shares);
		totalQty := totalQty + NEW.shares;
		
		select calculate_total_fee(NEW.price, NEW.shares, sideVal, NEW.liquidity, ecnVal) into temp_totalFee;
		totalFee := totalFee + temp_totalFee;

		-- update RM
		UPDATE risk_managements
		SET average_price = avgPrice,
			net_quantity = leave_Shares, 
			matched_profit_loss = netPL, 
			total_market_value = totalMktValue, 
			total_quantity = totalQty, 
			last_update = timeVal,
			total_fee = totalFee
		WHERE symbol = symbolVal and date = dateVal and account = accountVal;
	END IF;
	RETURN NEW;
END $$
  LANGUAGE plpgsql VOLATILE;
  
ALTER FUNCTION process_fills_insert()
  OWNER TO postgres;
  
COMMIT;
