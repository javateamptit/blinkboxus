BEGIN;

ALTER TABLE brokens ADD COLUMN time_stamp time without time zone;
ALTER TABLE brokens ALTER COLUMN time_stamp SET DEFAULT now();


--
alter table risk_managements add column total_fee double precision;
--
CREATE OR REPLACE FUNCTION calculate_total_fee(price double precision, shares integer, side character,liquidity character, ecn text) 
RETURNS double precision
LANGUAGE plpgsql
AS $$
	DECLARE
		sec_fee double precision;
		taf_fee double precision;
		wedbus_commission double precision;
		add_venue_fee_with_price_larger_than_1 double precision;
		add_venue_fee_with_price_smaller_than_1 double precision;
		remove_venue_fee_with_price_larger_than_1 double precision;
		remove_venue_fee_with_price_smaller_than_1 double precision;
		auction_venue_fee double precision;
		ARCA_route_venue_fee double precision;
		NDAQ_route_venue_fee double precision;
		
		totalVenueFee double precision;
		totalSecFees double precision;
		totalTafFee double precision;
		totalFee double precision;
		totalWedbushCommission double precision;
	BEGIN
		-- If we changed some fees in fee_config file on AS or TA, we would change those fees here
		sec_fee := 0.00002240;
		taf_fee := 0.00009;
		wedbus_commission := 0.000035;
		add_venue_fee_with_price_larger_than_1 := -0.0028;
		add_venue_fee_with_price_smaller_than_1 := 0.00;
		remove_venue_fee_with_price_larger_than_1 := 0.003;
		remove_venue_fee_with_price_smaller_than_1 := 0.003;
		auction_venue_fee := 0.0005;
		ARCA_route_venue_fee := 0.003;
		NDAQ_route_venue_fee := 0.0035;
		
		-- Initialize fees
		totalVenueFee := 0.00;
		totalSecFees := 0.00;
		totalTafFee := 0.00;
		totalFee := 0.00;
		totalWedbushCommission := 0.00;
		-- Calculate total fee
		if (side = 'S') then
			totalSecFees := shares * price * sec_fee;
		end if;
		
		totalTafFee := shares * taf_fee;
		totalWedbushCommission := shares * wedbus_commission;
		
		if (ecn = 'ARCA') then
			-- Add
			if (liquidity = 'A' or liquidity = 'B' or liquidity = 'M' or liquidity = 'D' or liquidity = 'S') then
				if (price >= 1.00) then
					totalVenueFee := shares * add_venue_fee_with_price_larger_than_1;
				else
					totalVenueFee := shares * add_venue_fee_with_price_smaller_than_1;
				end if;
			-- Remove
			elsif (liquidity = 'R' or liquidity = 'E' or liquidity = 'L') then
				if (price >= 1.00) then
					totalVenueFee := shares * remove_venue_fee_with_price_larger_than_1;
				else
					totalVenueFee := shares * price * remove_venue_fee_with_price_smaller_than_1;
				end if;
			-- Auction
			elsif (liquidity = 'G' or liquidity = 'Z') then
				totalVenueFee := shares * auction_venue_fee;
			elsif (liquidity = 'X' or liquidity = 'F' or liquidity = 'N' or liquidity = 'H' or liquidity = 'C' or liquidity = 'U' or liquidity = 'Y' or liquidity = 'W') then
				totalVenueFee := shares * ARCA_route_venue_fee;
			end if;
			
		elsif (ecn = 'OUCH') then
			-- Add
			if (liquidity = 'A' or liquidity = 'k') then
				if (price >= 1.00) then
					totalVenueFee := shares * add_venue_fee_with_price_larger_than_1;
				else
					totalVenueFee := shares * add_venue_fee_with_price_smaller_than_1;
				end if;
			-- Remove
			elsif (liquidity = 'R' or liquidity = 'm') then
				if (price >= 1.00) then
					totalVenueFee := shares * remove_venue_fee_with_price_larger_than_1;
				else
					totalVenueFee := shares * price * remove_venue_fee_with_price_smaller_than_1;
				end if;
			-- Auction
			elsif (liquidity = 'O' or liquidity = 'M' or liquidity = 'L' or liquidity = 'H' or liquidity = 'K' or liquidity = 'C') then
				totalVenueFee := shares * auction_venue_fee;
			end if;
		
		elsif (ecn = 'RASH') then
			-- Add
			if (liquidity = 'A' or liquidity = 'J' or liquidity = 'V' or liquidity = 'D' or liquidity = 'F' or liquidity = 'U' or liquidity = 'k' or liquidity = '9') then
				if (price >= 1.00) then
					totalVenueFee := shares * add_venue_fee_with_price_larger_than_1;
				else
					totalVenueFee := shares * add_venue_fee_with_price_smaller_than_1;
				end if;
			-- Remove
			elsif (liquidity = 'R' or liquidity = 'm') then
				if (price >= 1.00) then
					totalVenueFee := shares * remove_venue_fee_with_price_larger_than_1;
				else
					totalVenueFee := shares * price * remove_venue_fee_with_price_smaller_than_1;
				end if;		
			-- Auction
			elsif (liquidity = 'G' or liquidity = 'O' or liquidity = 'M' or liquidity = 'C' or liquidity = 'L' or liquidity = 'H' or liquidity = 'K' or liquidity = 'I' or liquidity = 'T' or liquidity = 'Z') then
				totalVenueFee := shares * auction_venue_fee;
			-- Route venue fee
			elsif (liquidity = 'X' or liquidity = 'Y' or liquidity = 'B' or liquidity = 'P' or liquidity = 'Q') then
				totalVenueFee := shares * NDAQ_route_venue_fee;
			end if;

		elsif (ecn = 'NYSE') then
			-- Add (provider)
			if (liquidity = '2' or liquidity = '8') then
				if (price >= 1.00) then
					totalVenueFee := shares * add_venue_fee_with_price_larger_than_1;
				else
					totalVenueFee := shares * add_venue_fee_with_price_smaller_than_1;
				end if;
			-- Remove (taker and blended)
			elsif (liquidity = '1' or liquidity = '9' or liquidity = '3' or liquidity = ' ') then
				if (price >= 1.00) then
					totalVenueFee := shares * remove_venue_fee_with_price_larger_than_1;
				else
					totalVenueFee := shares * price * remove_venue_fee_with_price_smaller_than_1;
				end if;
			-- Auction
			elsif (liquidity = '4' or liquidity = '5' or liquidity = '6' or liquidity = '7' ) then
				totalVenueFee := shares * auction_venue_fee;
			end if;
		end if;
		
		totalFee := totalSecFees + totalTafFee + totalWedbushCommission + totalVenueFee;
		return totalFee;
		
	END $$;
ALTER FUNCTION calculate_total_fee(double precision, integer, character, character, text) OWNER TO postgres;
--
DROP TRIGGER str_update_rm_for_broken ON brokens;
--
CREATE OR REPLACE FUNCTION update_rm()
RETURNS trigger 
LANGUAGE plpgsql
AS $$
	DECLARE
		symbolVal		text;
		leave_Shares	integer;
		order_Shares	integer;
		order_Price		float;
		avgPrice		float;
		totalQty		float;
		netPL			float;
		totalMktValue	float;
		rowcount		integer;
		dateVal			date;
		timeVal			time;
		sideVal			text;
		midVal			int;
		current_shares	int;
		current_price	float;
		fill 			record;
		fill_idVal 		int;
		sideBeginDay	character(1);
		sharesBeginDay 	integer;
		priceBeginDay 	float;
		accountVal 		integer;
		ecnVal 			text;
		liquidityVal	character;
		totalFee		double precision;
		temp_totalFee 	double precision;
	BEGIN
	-- Get symbol name
	if TG_OP = 'INSERT' then
		fill_idVal := NEW.fill_id;
		--Check to see if insert/update the CEE note, we will return 
		if NEW.is_cee IS TRUE then
			return NEW;
		end if;
	else
		fill_idVal := OLD.fill_id;
		--Check to see if insert/update the CEE note, we will return 
		if OLD.is_cee IS TRUE then
			return OLD;
		end if;
	end if;

	SELECT trim(new_orders.symbol), new_orders.date, fills.time_stamp, new_orders.side, account, new_orders.ecn, fills.liquidity into symbolVal, dateVal, timeVal, sideVal, accountVal, ecnVal, liquidityVal
	FROM new_orders
	JOIN fills ON new_orders.oid = fills.oid
	WHERE fills.fill_id = fill_idVal;

	SELECT side, shares, price into sideBeginDay, sharesBeginDay, priceBeginDay
	FROM open_positions 
	WHERE trim(symbol::text) = trim(symbolVal) AND date = dateVal AND account = accountVal;

	if (sharesBeginDay is null) then 
		leave_Shares := 0;
		avgPrice := 0;
		netPL := 0;
		totalMktValue := 0;
		totalQty := 0;
		totalFee := 0.00;
	else
		IF (sideBeginDay = 'S') THEN
			leave_Shares := -1 * sharesBeginDay;
		ELSE
			leave_Shares := sharesBeginDay;
		END IF;	

		avgPrice := priceBeginDay;
		netPL := 0;
		totalMktValue := sharesBeginDay * priceBeginDay;
		totalQty := sharesBeginDay;
		totalFee := 0.00;
		
		SELECT calculate_total_fee(priceBeginDay, sharesBeginDay, sharesBeginDay, liquidity, ecnVal) into temp_totalFee;
		totalFee := temp_totalFee;

	end if;	
	
	--Travel thought fill today and 
	for fill in (select f1.shares, f1.price, f1.fill_id, substring(o.side from 1 for 1) as side, o.ecn, f1.liquidity as liquidity from fills f1 join new_orders o on f1.oid = o.oid 
					where o.date = dateVal and trim(o.symbol) = trim(symbolVal) and o.account = accountVal 
					union all 
					(select cp.shares, cp.price, -1 as "fill_id",substring(o2.side from 1 for 1) as side, o2.ecn, '' as liquidity  from cancel_pendings cp join new_orders o2 on cp.oid = o2.oid where o2.date = dateVal and trim(o2.symbol) = trim(symbolVal) and o2.account = accountVal)) loop
				
		select new_shares, new_price into current_shares, current_price from brokens where fill_id = fill.fill_id;
		
		if current_price is null then
			current_shares  := fill.shares;
			current_price   := fill.price;
		end if;

		IF (leave_Shares > 0) THEN -- Equivalent to LONG
			IF (fill.side = 'B') THEN -- BUY
				avgPrice := (avgPrice * leave_Shares + current_price * current_shares) / (current_shares + leave_Shares);
				leave_Shares := leave_Shares + current_shares;
			ELSE -- SELL
				netPL := netPL + ((current_price - avgPrice) * LEAST(current_shares, leave_Shares));				
				leave_Shares := leave_Shares - current_shares;
				IF (leave_Shares < 0) THEN
					avgPrice := current_price;
				ELSIF (leave_Shares = 0) THEN
					avgPrice := 0;
				END IF;
			END IF;
		ELSIF (leave_Shares < 0) THEN -- Equivalent to SHORT
			IF (fill.side = 'B') THEN -- BUY
				netPL := netPL + ((avgPrice - current_price)*LEAST(current_shares, abs(leave_Shares)));				
				leave_Shares := leave_Shares + current_shares;
				IF (leave_Shares = 0) THEN
					avgPrice := 0;
				ELSIF (leave_Shares > 0) THEN
					avgPrice := current_price;
				END IF;
			ELSE -- SELL
				avgPrice := (avgPrice*(-leave_Shares) + current_price * current_shares) / (current_shares + (-leave_Shares));
				leave_Shares := leave_Shares - current_shares;				
			END IF;
		ELSE -- leave_Shares = 0
			avgPrice := current_price;
			IF (fill.side = 'B') THEN
				leave_Shares := current_shares;
			ELSE -- Action is Sell or Short Sell
				leave_Shares := -current_shares;
			END IF;
		END IF;
	
		totalMktValue := avgPrice * abs(leave_Shares);
		totalQty := totalQty + current_shares;	
		
		if (fill.liquidity <> NULL) then
			select calculate_total_fee(fill.price, fill.shares, fill.side, fill.liquidity, fill.ecn) into temp_totalFee;
			
			totalFee := totalFee + temp_totalFee;
		end if;
	end loop;
			
	-- update RM
	UPDATE risk_managements
	SET average_price = avgPrice,
		net_quantity = leave_Shares, 
		matched_profit_loss = netPL, 
		total_market_value = totalMktValue, 
		total_quantity = totalQty, 
		last_update = timeVal,
		total_fee = totalFee
	WHERE trim(symbol) = trim(symbolVal) and date = dateVal and account = accountVal;
	
	RETURN NEW;
END $$;
--
ALTER FUNCTION update_rm() OWNER TO postgres;

CREATE TRIGGER str_update_rm_for_broken
AFTER INSERT OR UPDATE OR DELETE
ON brokens
FOR EACH ROW
EXECUTE PROCEDURE update_rm();
--
DROP TRIGGER str_update_rm ON fills;

DROP TRIGGER str_update_rm2 ON cancel_pendings;

CREATE OR REPLACE FUNCTION process_fills_insert()
RETURNS trigger 
LANGUAGE plpgsql
AS $$
DECLARE
	symbolVal	text;
	leave_Shares	integer;
	avgPrice	float;
	totalQty	integer;
	netPL		float;
	totalMktValue	float;
	rowcount	integer;
	dateVal		date;
	timeVal		time;
	sideVal		text;
	accountVal 	integer;
	ecnVal			text;
	liquidity	character;
	totalFee	double precision;
	temp_totalFee double precision;
BEGIN
	-- Get symbol name
	SELECT trim(symbol), date, substring(side from 1 for 1) as side, account, trim(ecn) into symbolVal, dateVal, sideVal, accountVal, ecnVal
	FROM new_orders where oid = NEW.oid;
	
	timeVal := NEW.time_stamp;

	-- Check symbol existent?
	SELECT COUNT(*) into rowcount
	FROM risk_managements
	WHERE trim(symbol) = trim(symbolVal) and date = dateVal and account = accountVal;

	-- If new symbol
	IF (rowcount = 0) THEN
		IF (sideVal = 'B') THEN
			leave_Shares := NEW.shares;
		ELSE
			leave_Shares := -NEW.shares;
		END IF;
		
		totalMktValue := abs(leave_Shares) * NEW.price;
		
		-- totalFee := calculate_total_fee(NEW.price, NEW.shares, side, NEW.liquidity, ecn);
		-- insert into RM
		SELECT calculate_total_fee(NEW.price, NEW.shares, sideVal, NEW.liquidity, ecnVal) into temp_totalFee;
		totalFee := temp_totalFee;
				
		INSERT INTO risk_managements(symbol, average_price, net_quantity, matched_profit_loss, total_market_value, total_quantity, date, last_update, account, total_fee) 
			VALUES (symbolVal , NEW.price, leave_Shares, 0.00, totalMktValue, NEW.shares, dateVal, timeVal, accountVal, totalFee);
	ELSE -- Old symbol
		SELECT average_price, net_quantity, matched_profit_loss, total_market_value, total_quantity, total_fee INTO avgPrice ,leave_Shares, netPL, totalMktValue, totalQty, totalFee
		FROM risk_managements
		WHERE trim(symbol) = (symbolVal) and date = dateVal and account = accountVal;
		
		IF (leave_Shares = 0) THEN
			avgPrice := NEW.price;
			IF (sideVal = 'B') THEN
				leave_Shares := NEW.shares;
			ELSE -- Action is Sell or Short Sell
				leave_Shares := -NEW.shares;
			END IF;
		ELSIF (leave_Shares > 0) THEN
			IF (sideVal = 'B') THEN
				avgPrice := (avgPrice * leave_Shares + NEW.price * NEW.shares) / (NEW.shares + leave_Shares);
				leave_Shares := leave_Shares + NEW.shares;
			ELSE
				netPL := netPL + ((NEW.price - avgPrice) * LEAST(NEW.shares, leave_Shares));				
				leave_Shares := leave_Shares - NEW.shares;
				IF (leave_Shares = 0) THEN
					avgPrice := 0.00;
				ELSIF (leave_Shares < 0) THEN
					avgPrice := NEW.price;
				END IF;
			END IF;
		ELSE 
			IF (sideVal = 'B') THEN
				netPL := netPL + ((avgPrice - NEW.price)*LEAST(NEW.shares, abs(leave_Shares)));				
				leave_Shares := leave_Shares + NEW.shares;
				IF (leave_Shares = 0) THEN
					avgPrice := 0.00;
				ELSIF (leave_Shares > 0) THEN
					avgPrice := NEW.price;
				END IF;
			ELSE
				avgPrice := (avgPrice*(-leave_Shares) + NEW.price * NEW.shares) / (NEW.shares + (-leave_Shares));
				leave_Shares := leave_Shares - NEW.shares;	
			END IF;
		END IF;
		totalMktValue := avgPrice * abs(leave_Shares);
		totalQty := totalQty + NEW.shares;
		
		select calculate_total_fee(NEW.price, NEW.shares, sideVal, NEW.liquidity, ecnVal) into temp_totalFee;
		totalFee := totalFee + temp_totalFee;

		-- update RM
		UPDATE risk_managements
		SET average_price = avgPrice,
			net_quantity = leave_Shares, 
			matched_profit_loss = netPL, 
			total_market_value = totalMktValue, 
			total_quantity = totalQty, 
			last_update = timeVal,
			total_fee = totalFee
		WHERE symbol = symbolVal and date = dateVal and account = accountVal;
	END IF;
	RETURN NEW;
END $$;
  
ALTER FUNCTION process_fills_insert() OWNER TO postgres;
    
CREATE TRIGGER str_update_rm
AFTER INSERT
ON fills
FOR EACH ROW
EXECUTE PROCEDURE process_fills_insert();
  
CREATE TRIGGER str_update_rm2
AFTER INSERT
ON cancel_pendings
FOR EACH ROW
EXECUTE PROCEDURE process_fills_insert();
--

CREATE OR REPLACE VIEW view_liquidity AS 
 SELECT fills.shares, fills.price, fills.liquidity, new_orders.ecn, new_orders.side, new_orders.date
   FROM fills
   JOIN new_orders ON new_orders.oid = fills.oid
   ORDER BY new_orders.oid;

ALTER TABLE view_liquidity
  OWNER TO postgres;


COMMIT;

