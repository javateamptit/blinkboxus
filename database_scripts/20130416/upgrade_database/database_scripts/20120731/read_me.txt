This text provides information related to the database script "db_upgrade_script.sql" placed at the same directory.
When run this scipt on target database, it will:
- add column "account" to some tables
- modify some functions on target database
- modify target database contents

*The update is needed to run with new release.
*There is no way to downgrade, so please back up the target database first.

How to run script:
1) To upgrade, please copy the file db_update_script.sql to the EAA database host and run the command (please notice that we must run this command in the same folder with the file db_update_script.sql):
	psql -U postgres -h localhost -d eaa10_201207 -f db_update_script.sql

2) If you have any problems after upgrading, please restore the database from the back up