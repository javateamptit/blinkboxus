-- **********************************************************
-- 	SRCIPTS TO UPGRADE DATABASE
-- **********************************************************
BEGIN;
-- -------------------------------------------------------
-- Add column "account" (integer) to missing tables
-- -------------------------------------------------------

alter table new_orders add column account integer;
alter table risk_managements add column account integer;
alter table open_positions add column account integer;
alter table manual_open_positions add column account integer;
alter table rm_simultaneous_trades add column account integer;

-- -------------------------------------------------------
-- Modify value of "account" column based on symbol name
-- -------------------------------------------------------

update new_orders set account = 1, symbol=substr(symbol, 1, length(symbol)-1) where substr(symbol, length(symbol), 1) = '2';
update new_orders set account = 0, symbol=substr(symbol, 1, length(symbol)-1) where substr(symbol, length(symbol), 1) = '1';
update new_orders set account = 0 where account IS NULL;

update risk_managements set account = 1, symbol=substr(symbol, 1, length(symbol)-1) where substr(symbol, length(symbol), 1) = '2';
update risk_managements set account = 0, symbol=substr(symbol, 1, length(symbol)-1) where substr(symbol, length(symbol), 1) = '1';
update risk_managements set account = 0 where account IS NULL;

update open_positions set account = 1, symbol=substr(symbol, 1, length(symbol)-1) where substr(symbol, length(symbol), 1) = '2';
update open_positions set account = 0, symbol=substr(symbol, 1, length(symbol)-1) where substr(symbol, length(symbol), 1) = '1';
update open_positions set account = 0 where account IS NULL;

update manual_open_positions set account = 1, symbol=substr(symbol, 1, length(symbol)-1) where substr(symbol, length(symbol), 1) = '2';
update manual_open_positions set account = 0, symbol=substr(symbol, 1, length(symbol)-1) where substr(symbol, length(symbol), 1) = '1';
update manual_open_positions set account = 0 where account IS NULL;

update rm_simultaneous_trades set account = 1, symbol=substr(symbol, 1, length(symbol)-1) where substr(symbol, length(symbol), 1) = '2';
update rm_simultaneous_trades set account = 0, symbol=substr(symbol, 1, length(symbol)-1) where substr(symbol, length(symbol), 1) = '1';
update rm_simultaneous_trades set account = 0 where account IS NULL;

-- -------------------------------------------------------
-- modify functions
-- -------------------------------------------------------

CREATE OR REPLACE FUNCTION add_rm(fill_idval integer) RETURNS integer
    AS $$
	DECLARE
	symbolVal	text;
        leave_Shares	integer;
	order_Shares	integer;
	order_Price	float;
	avgPrice	float;
	totalQty	float;
	netPL		float;
	totalMktValue	float;
	rowcount	integer;
	dateVal		date;
	timeVal		time;
	sideVal		text;
	midVal		int;
	current_shares	int;
	current_price	float;
	fill 		record;
	accountVal 	integer;
	BEGIN

	rowcount = 0;
	leave_Shares := 0;
	avgPrice := 0;
	netPL := 0;
	totalMktValue := 0;
	totalQty := 0;

	SELECT trim(new_orders.symbol), new_orders.date, fills.time_stamp, new_orders.side, account into symbolVal, dateVal, timeVal, sideVal, accountVal
	FROM new_orders
	JOIN fills ON new_orders.oid = fills.oid
	WHERE fills.fill_id = fill_idVal;
	
	--symbolVal := 'AEOS';
	--dateVal := cast('2007-01-04' as date);
	--Travel thought fill today and 

	for fill in (select fills.*, substring(new_orders.side from 1 for 1) as side from fills  join new_orders on fills.oid = new_orders.oid 
							where new_orders.date = dateVal and trim(new_orders.symbol) = symbolVal and new_orders.account = accountVal) loop
		
		RAISE NOTICE  'I am in loop';
		rowcount := rowcount + 1;		

		
		select new_shares, new_price into current_shares, current_price from brokens where fill_id = fill.fill_id;
		
		IF current_price is null then
			current_shares := fill.shares;
			current_price := fill.price;
		end if;

		
		IF (leave_Shares > 0) THEN -- Equivalent to LONG
			IF (fill.side = 'B') THEN -- BUY
				avgPrice := (avgPrice * leave_Shares + current_price * current_shares) / (current_shares + leave_Shares);
				leave_Shares := leave_Shares + current_shares;
			ELSE -- SELL
				netPL := netPL + ((current_price - avgPrice) * LEAST(current_shares, leave_Shares));				
				leave_Shares := leave_Shares - current_shares;
				IF (leave_Shares < 0) THEN
					avgPrice := current_price;
				ELSIF (leave_Shares = 0) THEN
					avgPrice := 0;
				END IF;
			END IF;
		ELSIF (leave_Shares < 0) THEN -- Equivalent to SHORT
			IF (fill.side = 'B') THEN -- BUY
				netPL := netPL + ((avgPrice - current_price)*LEAST(current_shares, abs(leave_Shares)));				
				leave_Shares := leave_Shares + current_shares;
				IF (leave_Shares = 0) THEN
					avgPrice := 0;
				ELSIF (leave_Shares > 0) THEN
					avgPrice := current_price;
				END IF;
			ELSE -- SELL
				avgPrice := (avgPrice*(-leave_Shares) + current_price * current_shares) / (current_shares + (-leave_Shares));
				leave_Shares := leave_Shares - current_shares;				
			END IF;
		ELSE -- leave_Shares = 0
			avgPrice := current_price;
			IF (fill.side = 'B') THEN
				leave_Shares := current_shares;
			ELSE -- Action is Sell or Short Sell
				leave_Shares := -current_shares;
			END IF;
		END IF;
	
		totalMktValue := avgPrice * abs(leave_Shares);
		totalQty := totalQty + current_shares;	
	
	end loop;
	RAISE NOTICE  'I am done.';		
		-- update RM
		UPDATE risk_managements
		SET 	average_price = avgPrice,
			net_quantity = leave_Shares, 
			matched_profit_loss = netPL, 
			total_market_value = totalMktValue, 
			total_quantity = totalQty, 
			last_update = timeVal
		WHERE trim(symbol) = symbolVal and date = dateVal and account = accountVal;
	
	RETURN rowcount;
	END;
$$
    LANGUAGE plpgsql;

ALTER FUNCTION add_rm(fill_idval integer) OWNER TO postgres;

-- ---------------------------------------------------------------
CREATE OR REPLACE FUNCTION check_and_create_long_short(td date) RETURNS integer
    AS $$ 
declare
  open_position record;
  long_short_count int;
  manual_long_short_count int;
  max_date date;
  buy_sell character(1);
  total_shares int;
  valTime time;
begin	
  long_short_count := 0;
  SELECT LOCALTIME(2) into valTime;

  SELECT max(date) into max_date FROM risk_managements WHERE date < td;
  SELECT count(*) into long_short_count FROM open_positions WHERE date = td;

  -- Check update long/short
  SELECT count(*) into manual_long_short_count FROM manual_open_positions WHERE date = td;

  IF (manual_long_short_count > 0) THEN
    IF long_short_count > 0 THEN 
    
      return long_short_count;
    ELSE
      for open_position in (select trim(symbol) AS symbol, price, shares, side, account FROM manual_open_positions WHERE date = td) loop
        IF (open_position.symbol <> 'T3ST') THEN
          long_short_count := long_short_count + 1;

          total_shares := open_position.shares;

          IF (open_position.side = 'S') THEN
            total_shares := -1 * total_shares;
          END IF;

          -- Insert "open_positions" table
          INSERT INTO open_positions(side, symbol, shares, price, date, account) 
          VALUES (open_position.side, trim(open_position.symbol), open_position.shares, open_position.price, td, open_position.account);

          --Insert "risk_managements" table
          INSERT INTO risk_managements(symbol, average_price, net_quantity, matched_profit_loss, total_market_value, total_quantity, date, last_update, account) 
          VALUES (trim(open_position.symbol), open_position.price, total_shares, 0.00, open_position.shares * open_position.price, open_position.shares, td, valTime, open_position.account);
        END IF;
      end loop;

      return long_short_count;
    END IF;
  ELSE
    IF long_short_count > 0 THEN 
    
      return long_short_count;
    ELSE
      -- T3ST - Long 0 @ 0.00
      INSERT INTO manual_open_positions(side, symbol, shares, price, date, account) VALUES ('B', 'T3ST', 0, 0.00, td, 0);

      for open_position in (select symbol, average_price AS price, net_quantity AS shares, total_market_value, account FROM risk_managements where date = max_date and net_quantity != 0) loop
        long_short_count := long_short_count + 1;

        buy_sell := 'B';
        total_shares := open_position.shares;

        IF (open_position.shares < 0) THEN
          buy_sell := 'S';
          total_shares := -1 * total_shares;
        END IF;

        -- Insert "manual_open_positions" table
        INSERT INTO manual_open_positions(side, symbol, shares, price, date, account) 
        VALUES (buy_sell, trim(open_position.symbol), total_shares, open_position.price, td, open_position.account);

        -- Insert "open_positions" table
        INSERT INTO open_positions(side, symbol, shares, price, date, account) 
        VALUES (buy_sell, trim(open_position.symbol), total_shares, open_position.price, td, open_position.account);

        --Insert "risk_managements" table
        INSERT INTO risk_managements(symbol, average_price, net_quantity, matched_profit_loss, total_market_value, total_quantity, date, last_update, account) 
        VALUES (trim(open_position.symbol), open_position.price, open_position.shares, 0.00, open_position.total_market_value, total_shares, td, valTime, open_position.account);
      end loop;

      return long_short_count;
    END IF;
  END IF;  
end
$$
    LANGUAGE plpgsql;


ALTER FUNCTION check_and_create_long_short(td date) OWNER TO postgres;

-- ---------------------------
CREATE OR REPLACE FUNCTION create_new_long_short(td date, tmp_symbol text, side integer, shares integer, price double precision, accountVal integer) RETURNS integer
    AS $$ 
declare
  valTime time;
  long_short_count integer;
begin	
  SELECT LOCALTIME(2) into valTime;

  SELECT count(*) into long_short_count FROM risk_managements WHERE date = td and trim(upper(symbol)) = trim(upper(tmp_symbol)) and account = accountVal;

  IF (long_short_count = 0) THEN
    INSERT INTO risk_managements(symbol, average_price, net_quantity, matched_profit_loss, total_market_value, total_quantity, date, last_update, account) 
    VALUES (upper(trim(tmp_symbol)), price, shares * side, 0.00, price * shares, shares, td, valTime, accountVal);
    
    return 0;
  ELSE 
    UPDATE risk_managements
    SET average_price = price, net_quantity = shares * side, total_market_value = price*shares, last_update = valTime
    WHERE date = td and trim(upper(symbol)) = trim(upper(tmp_symbol)) and account = accountVal;

    return 0;
  END IF;

  return -1;
end
$$
    LANGUAGE plpgsql;


ALTER FUNCTION create_new_long_short(td date, tmp_symbol text, side integer, shares integer, price double precision, accountVal integer) OWNER TO postgres;

-- -------------------------------------------------------

CREATE OR REPLACE FUNCTION process_fills_insert() RETURNS "trigger"
    AS $$
	DECLARE
	symbolVal	text;
	leave_Shares	integer;
	avgPrice	float;
	totalQty	integer;
	netPL		float;
	totalMktValue	float;
	rowcount	integer;
	dateVal		date;
	timeVal		time;
	sideVal		text;
	accountVal 	integer;
	BEGIN
	-- Get symbol name
	SELECT trim(symbol), date, substring(side from 1 for 1) as side, account into symbolVal, dateVal, sideVal, accountVal
	FROM new_orders where oid = NEW.oid;
	
	timeVal := NEW.time_stamp;

	-- Check symbol existent?
	SELECT COUNT(*) into rowcount
	FROM risk_managements
	WHERE trim(symbol) = trim(symbolVal) and date = dateVal and account = accountVal;

	-- If new symbol
	IF (rowcount = 0) THEN
		IF (sideVal = 'B') THEN
			leave_Shares := NEW.shares;
		ELSE
			leave_Shares := -NEW.shares;
		END IF;
		
		totalMktValue := abs(leave_Shares) * NEW.price;
		-- insert into RM
		INSERT INTO risk_managements(symbol, average_price, net_quantity, matched_profit_loss, total_market_value, total_quantity, date, last_update, account) 
			VALUES (symbolVal , NEW.price, leave_Shares, 0.00, totalMktValue, NEW.shares, dateVal, timeVal, accountVal);
	ELSE -- Old symbol
		SELECT average_price, net_quantity, matched_profit_loss, total_market_value, total_quantity INTO avgPrice ,leave_Shares, netPL, totalMktValue, totalQty
		FROM risk_managements
		WHERE trim(symbol) = (symbolVal) and date = dateVal and account = accountVal;
		
		IF (leave_Shares = 0) THEN
			avgPrice := NEW.price;
			IF (sideVal = 'B') THEN
				leave_Shares := NEW.shares;
			ELSE -- Action is Sell or Short Sell
				leave_Shares := -NEW.shares;
			END IF;
		ELSIF (leave_Shares > 0) THEN
			IF (sideVal = 'B') THEN
				avgPrice := (avgPrice * leave_Shares + NEW.price * NEW.shares) / (NEW.shares + leave_Shares);
				leave_Shares := leave_Shares + NEW.shares;
			ELSE
				netPL := netPL + ((NEW.price - avgPrice) * LEAST(NEW.shares, leave_Shares));				
				leave_Shares := leave_Shares - NEW.shares;
				IF (leave_Shares = 0) THEN
					avgPrice := 0.00;
				ELSIF (leave_Shares < 0) THEN
					avgPrice := NEW.price;
				END IF;
			END IF;
		ELSE 
			IF (sideVal = 'B') THEN
				netPL := netPL + ((avgPrice - NEW.price)*LEAST(NEW.shares, abs(leave_Shares)));				
				leave_Shares := leave_Shares + NEW.shares;
				IF (leave_Shares = 0) THEN
					avgPrice := 0.00;
				ELSIF (leave_Shares > 0) THEN
					avgPrice := NEW.price;
				END IF;
			ELSE
				avgPrice := (avgPrice*(-leave_Shares) + NEW.price * NEW.shares) / (NEW.shares + (-leave_Shares));
				leave_Shares := leave_Shares - NEW.shares;	
			END IF;
		END IF;
		totalMktValue := avgPrice * abs(leave_Shares);
		totalQty := totalQty + NEW.shares;		
		-- update RM
		UPDATE risk_managements
		SET 	average_price = avgPrice,
			net_quantity = leave_Shares, 
			matched_profit_loss = netPL, 
			total_market_value = totalMktValue, 
			total_quantity = totalQty, 
			last_update = timeVal
		WHERE symbol = symbolVal and date = dateVal and account = accountVal;
	END IF;
	RETURN NEW;
	END;
$$
    LANGUAGE plpgsql;


ALTER FUNCTION process_fills_insert() OWNER TO postgres;

-- -------------------------------------------------------

CREATE OR REPLACE FUNCTION process_fills_simultaneous() RETURNS "trigger"
    AS $$
	DECLARE
	symbolVal	text;
        leave_Shares	integer;
	order_Shares	integer;
	order_Price	float;
	avgPrice	float;
	totalQty	float;
	netPL		float;
	totalMktValue	float;
	rowcount	integer;
	dateVal		date;
	timeVal		time;
	sideVal		text;
	tradeType	integer;
	accountVal 	integer;
	BEGIN
	
	-- Get symbol name
	SELECT trim(symbol), date, substring(side from 1 for 1) as side, trade_type, account into symbolVal, dateVal, sideVal, tradeType, accountVal 
	FROM new_orders where oid = NEW.oid;

	-- Check simultaneous trade
	IF (tradeType = 1) THEN	
		timeVal := NEW.time_stamp;

		-- Check symbol existent?
		SELECT COUNT(*) into rowcount
		FROM rm_simultaneous_trades
		WHERE trim(symbol) = trim(symbolVal) and date = dateVal and account = accountVal;

		-- If new symbol
		IF (rowcount = 0) THEN
			IF (sideVal = 'B') THEN
				leave_Shares := NEW.shares;
			ELSE
				leave_Shares := - NEW.shares;
			END IF;
			totalMktValue := abs(leave_Shares) * NEW.price;

			-- insert into RM
			INSERT INTO rm_simultaneous_trades(symbol, average_price, net_quantity, total_market_value,matched_profit_loss, total_quantity, date, last_update, account) 
				VALUES (symbolVal , NEW.price, leave_Shares, totalMktValue,0, NEW.shares, dateVal, timeVal, accountVal);
		ELSE -- Old symbol
			SELECT average_price, net_quantity, matched_profit_loss, total_market_value, total_quantity INTO avgPrice ,leave_Shares, netPL, totalMktValue, totalQty
			FROM rm_simultaneous_trades
			WHERE trim(symbol) = (symbolVal) and date = dateVal and account = accountVal;

			IF (leave_Shares > 0) THEN -- Equivalent to LONG
				IF (sideVal = 'B') THEN -- BUY
					avgPrice := (avgPrice * leave_Shares + NEW.price * NEW.shares) / (NEW.shares + leave_Shares);
					leave_Shares := leave_Shares + NEW.shares;
				ELSE -- SELL
					netPL := netPL + ((NEW.price - avgPrice) * LEAST(NEW.shares, leave_Shares));				
					leave_Shares := leave_Shares - NEW.shares;
					IF (leave_Shares < 0) THEN
						avgPrice := NEW.price;
					ELSIF (leave_Shares = 0) THEN
						avgPrice := 0;
					END IF;
				END IF;
			ELSIF (leave_Shares < 0) THEN -- Equivalent to SHORT
				IF (sideVal = 'B') THEN -- BUY
					netPL := netPL + ((avgPrice - NEW.price)*LEAST(NEW.shares, abs(leave_Shares)));				
					leave_Shares := leave_Shares + NEW.shares;
					IF (leave_Shares = 0) THEN
						avgPrice := 0;
					ELSIF (leave_Shares > 0) THEN
						avgPrice := NEW.price;
					END IF;
				ELSE -- SELL
					avgPrice := (avgPrice*(-leave_Shares) + NEW.price * NEW.shares) / (NEW.shares + (-leave_Shares));
					leave_Shares := leave_Shares - NEW.shares;				
				END IF;
			ELSE -- leave_Shares = 0
				avgPrice := NEW.price;
				IF (sideVal = 'B') THEN
					leave_Shares := NEW.shares;
				ELSE -- Action is Sell or Short Sell
					leave_Shares := -NEW.shares;
				END IF;
			END IF;
			totalMktValue := avgPrice * abs(leave_Shares);
			totalQty := totalQty + NEW.shares;
		
			-- update RM
			UPDATE rm_simultaneous_trades
			SET 	average_price = avgPrice,
				net_quantity = leave_Shares, 
				matched_profit_loss = netPL, 
				total_market_value = totalMktValue, 
				total_quantity = totalQty, 
				last_update = timeVal
			WHERE symbol = symbolVal and date = dateVal and account = accountVal;
		END IF;
	END IF;

	RETURN NEW;

	END;
$$
    LANGUAGE plpgsql;


ALTER FUNCTION process_fills_simultaneous() OWNER TO postgres;

-- -------------------------------------------------------

CREATE OR REPLACE FUNCTION update_rm() RETURNS "trigger"
    AS $$
	DECLARE
	symbolVal	text;
        leave_Shares	integer;
	order_Shares	integer;
	order_Price	float;
	avgPrice	float;
	totalQty	float;
	netPL		float;
	totalMktValue	float;
	rowcount	integer;
	dateVal		date;
	timeVal		time;
	sideVal		text;
	midVal		int;
	current_shares	int;
	current_price	float;
	fill 		record;
	fill_idVal int;
	sideBeginDay character(1);
	sharesBeginDay integer;
	priceBeginDay float;
	accountVal 	integer;
	BEGIN
	-- Get symbol name
	if TG_OP = 'INSERT' then
		fill_idVal := NEW.fill_id;
		--Check to see if insert/update the CEE note, we will return 
		if NEW.is_cee IS TRUE then
			return NEW;
		end if;
	else
		fill_idVal := OLD.fill_id;
		--Check to see if insert/update the CEE note, we will return 
		if OLD.is_cee IS TRUE then
			return OLD;
		end if;
	end if;

	SELECT trim(new_orders.symbol), new_orders.date, fills.time_stamp, new_orders.side, account into symbolVal, dateVal, timeVal, sideVal, accountVal 
	FROM new_orders
	JOIN fills ON new_orders.oid = fills.oid
	WHERE fills.fill_id = fill_idVal;

	--leave_Shares := 0;
	--avgPrice := 0;
	--netPL := 0;
	--totalMktValue := 0;
	--totalQty := 0;
	
	SELECT side, shares, price into sideBeginDay, sharesBeginDay, priceBeginDay
	FROM open_positions 
	WHERE trim(symbol::text) = trim(symbolVal) AND date = dateVal AND account = accountVal;

	if (sharesBeginDay is null) then 
		leave_Shares := 0;
		avgPrice := 0;
		netPL := 0;
		totalMktValue := 0;
		totalQty := 0;
	else
		IF (sideBeginDay = 'S') THEN
			leave_Shares := -1 * sharesBeginDay;
		ELSE
			leave_Shares := sharesBeginDay;
		END IF;	

		avgPrice := priceBeginDay;
		netPL := 0;
		totalMktValue := sharesBeginDay * priceBeginDay;
		totalQty := sharesBeginDay;
	end if;	
	
	--Travel thought fill today and 

	for fill in (select f1.shares, f1.price, f1.fill_id, substring(o.side from 1 for 1) as side from fills f1 join new_orders o on f1.oid = o.oid 
							where o.date = dateVal and trim(o.symbol) = trim(symbolVal) and o.account = accountVal union all (select cp.shares, cp.price, -1 as "fill_id",substring(o2.side from 1 for 1) as side  from cancel_pendings cp join new_orders o2 on cp.oid = o2.oid where o2.date = dateVal and trim(o2.symbol) = trim(symbolVal) and o2.account = accountVal)) loop
				
		select new_shares, new_price into current_shares, current_price from brokens where fill_id = fill.fill_id;
		
		if current_price is null then
			current_shares  := fill.shares;
			current_price   := fill.price;
		end if;

		IF (leave_Shares > 0) THEN -- Equivalent to LONG
			IF (fill.side = 'B') THEN -- BUY
				avgPrice := (avgPrice * leave_Shares + current_price * current_shares) / (current_shares + leave_Shares);
				leave_Shares := leave_Shares + current_shares;
			ELSE -- SELL
				netPL := netPL + ((current_price - avgPrice) * LEAST(current_shares, leave_Shares));				
				leave_Shares := leave_Shares - current_shares;
				IF (leave_Shares < 0) THEN
					avgPrice := current_price;
				ELSIF (leave_Shares = 0) THEN
					avgPrice := 0;
				END IF;
			END IF;
		ELSIF (leave_Shares < 0) THEN -- Equivalent to SHORT
			IF (fill.side = 'B') THEN -- BUY
				netPL := netPL + ((avgPrice - current_price)*LEAST(current_shares, abs(leave_Shares)));				
				leave_Shares := leave_Shares + current_shares;
				IF (leave_Shares = 0) THEN
					avgPrice := 0;
				ELSIF (leave_Shares > 0) THEN
					avgPrice := current_price;
				END IF;
			ELSE -- SELL
				avgPrice := (avgPrice*(-leave_Shares) + current_price * current_shares) / (current_shares + (-leave_Shares));
				leave_Shares := leave_Shares - current_shares;				
			END IF;
		ELSE -- leave_Shares = 0
			avgPrice := current_price;
			IF (fill.side = 'B') THEN
				leave_Shares := current_shares;
			ELSE -- Action is Sell or Short Sell
				leave_Shares := -current_shares;
			END IF;
		END IF;
	
		totalMktValue := avgPrice * abs(leave_Shares);
		totalQty := totalQty + current_shares;	
	
	end loop;
			
		-- update RM
		UPDATE risk_managements
		SET 	average_price = avgPrice,
			net_quantity = leave_Shares, 
			matched_profit_loss = netPL, 
			total_market_value = totalMktValue, 
			total_quantity = totalQty, 
			last_update = timeVal
		WHERE trim(symbol) = trim(symbolVal) and date = dateVal and account = accountVal;
	
	RETURN NEW;
	END;
$$
    LANGUAGE plpgsql;


ALTER FUNCTION update_rm() OWNER TO postgres;

-- -------------------------------------------------------

CREATE OR REPLACE FUNCTION update_rm_st() RETURNS "trigger"
    AS $$
	DECLARE
	symbolVal	text;
        leave_Shares	integer;
	order_Shares	integer;
	order_Price	float;
	avgPrice	float;
	totalQty	float;
	netPL		float;
	totalMktValue	float;
	rowcount	integer;
	dateVal		date;
	timeVal		time;
	sideVal		text;
	midVal		int;
	current_shares	int;
	current_price	float;
	fill 		record;
	fill_idVal int;
	tradeType	integer;
	accountVal 	integer;
	BEGIN
	-- Get symbol name
	if TG_OP = 'INSERT' then
		fill_idVal := NEW.fill_id;
	else
		fill_idVal := OLD.fill_id;
	end if;

	SELECT trim(new_orders.symbol), new_orders.date, fills.time_stamp, new_orders.side, new_orders.trade_type, account into symbolVal, dateVal, timeVal, sideVal, tradeType, accountVal
	FROM new_orders
	JOIN fills ON new_orders.oid = fills.oid
	WHERE fills.fill_id = fill_idVal;

	-- Check simultaneous trade
	IF (tradeType = 1) THEN
		leave_Shares := 0;
		avgPrice := 0;
		netPL := 0;
		totalMktValue := 0;
		totalQty := 0;	
		
		--Travel thought fill today and 
		for fill in ((select f1.shares, f1.price, f1.fill_id, substring(o.side from 1 for 1) as side from fills f1 join new_orders o on f1.oid = o.oid where o.date = dateVal and trim(o.symbol) = trim(symbolVal) and o.trade_type = 1 and o.account = accountVal) 
			union all (select cp.shares, cp.price, -1 as "fill_id",substring(o2.side from 1 for 1) as side  from cancel_pendings cp join new_orders o2 on cp.oid = o2.oid where o2.date = dateVal and trim(o2.symbol) = trim(symbolVal) and o2.trade_type = 1 and o2.account = accountVal)) loop
					
			select new_shares, new_price into current_shares, current_price from brokens where fill_id = fill.fill_id;
			
			if current_price is null then
				current_shares  := fill.shares;
				current_price   := fill.price;
			end if;

			IF (leave_Shares > 0) THEN -- Equivalent to LONG
				IF (fill.side = 'B') THEN -- BUY
					avgPrice := (avgPrice * leave_Shares + current_price * current_shares) / (current_shares + leave_Shares);
					leave_Shares := leave_Shares + current_shares;
				ELSE -- SELL
					netPL := netPL + ((current_price - avgPrice) * LEAST(current_shares, leave_Shares));				
					leave_Shares := leave_Shares - current_shares;
					IF (leave_Shares < 0) THEN
						avgPrice := current_price;
					ELSIF (leave_Shares = 0) THEN
						avgPrice := 0;
					END IF;
				END IF;
			ELSIF (leave_Shares < 0) THEN -- Equivalent to SHORT
				IF (fill.side = 'B') THEN -- BUY
					netPL := netPL + ((avgPrice - current_price)*LEAST(current_shares, abs(leave_Shares)));				
					leave_Shares := leave_Shares + current_shares;
					IF (leave_Shares = 0) THEN
						avgPrice := 0;
					ELSIF (leave_Shares > 0) THEN
						avgPrice := current_price;
					END IF;
				ELSE -- SELL
					avgPrice := (avgPrice*(-leave_Shares) + current_price * current_shares) / (current_shares + (-leave_Shares));
					leave_Shares := leave_Shares - current_shares;				
				END IF;
			ELSE -- leave_Shares = 0
				avgPrice := current_price;
				IF (fill.side = 'B') THEN
					leave_Shares := current_shares;
				ELSE -- Action is Sell or Short Sell
					leave_Shares := -current_shares;
				END IF;
			END IF;
		
			totalMktValue := avgPrice * abs(leave_Shares);
			totalQty := totalQty + current_shares;	
		
		end loop;
			
		-- update RM
		UPDATE rm_simultaneous_trades
		SET 	average_price = avgPrice,
			net_quantity = leave_Shares, 
			matched_profit_loss = netPL, 
			total_market_value = totalMktValue, 
			total_quantity = totalQty, 
			last_update = timeVal
		WHERE trim(symbol) = trim(symbolVal) and date = dateVal and account = accountVal;
	END IF;
	
	RETURN NEW;
	END;
$$
    LANGUAGE plpgsql;


ALTER FUNCTION update_rm_st() OWNER TO postgres;

-- -------------------------------------------------------
-- Update Views
-- -------------------------------------------------------

DROP VIEW traded_orders;

CREATE OR REPLACE VIEW traded_orders AS 
 SELECT DISTINCT new_orders.oid AS id, new_orders.date AS trade_date, new_orders.symbol, new_orders.account, new_orders.order_id AS seqid, new_orders.entry_exit, new_orders.side AS "action", new_orders.time_stamp AS trade_time, new_orders.ecn, new_orders.shares AS qty, new_orders.price, new_orders.time_in_force AS tif, new_orders.left_shares AS lvs, new_orders.status, new_orders.ts_id AS ts, new_orders.is_iso, brokens.filing_status
   FROM new_orders
   LEFT JOIN brokens ON new_orders.date = brokens.trade_date AND new_orders.oid = brokens.oid
  ORDER BY new_orders.oid, new_orders.date, new_orders.symbol, new_orders.order_id, new_orders.entry_exit, new_orders.side, new_orders.time_stamp, new_orders.ecn, new_orders.shares, new_orders.price, new_orders.time_in_force, new_orders.left_shares, new_orders.status, new_orders.ts_id, new_orders.is_iso, brokens.filing_status, new_orders.account;

ALTER TABLE traded_orders OWNER TO postgres;

COMMIT;
-- -------------------------------------------------------
-- END
-- -------------------------------------------------------
