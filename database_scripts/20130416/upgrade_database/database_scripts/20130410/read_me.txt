* How to run script:
- To upgrade, please copy 2 files: upgrade_20130410.sql and downgrade_20130410.sql to the EAA database host
and run the command (please notice that we must run this command in the same folder with the file upgrade_20130410.sql):
	psql -U postgres -h localhost -d eaa10_201304 -f upgrade_20130410.sql

- If you want downgrade, please run the command:
	psql -U postgres -h localhost -d eaa10_201304 -f downgrade_20130410.sql