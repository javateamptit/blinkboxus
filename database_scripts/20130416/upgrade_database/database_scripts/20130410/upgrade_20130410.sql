BEGIN;

CREATE TABLE IF NOT EXISTS release_versions
(
	id serial,
	module_id integer NOT NULL,
	version text NOT NULL,
	release_date date default now(),
	description text,
	CONSTRAINT pk_release_versions PRIMARY KEY (id),
	CONSTRAINT uk_release_versions UNIQUE (module_id, version)
);

-- Upgrade
INSERT INTO release_versions(module_id, version) VALUES (2, '20130410');


--
-- Name: calculate_total_fee(double precision, integer, character, character, text); Type: FUNCTION; Schema: public; Owner: postgres
--
CREATE OR REPLACE FUNCTION calculate_total_fee(price double precision, shares integer, side character, liquidity character, ecn text) RETURNS double precision
    LANGUAGE plpgsql
    AS $$
	DECLARE
		sec_fee double precision;
		taf_fee double precision;
		wedbus_commission double precision;
		add_venue_fee_with_price_larger_than_1 double precision;
		add_venue_fee_with_price_smaller_than_1 double precision;
		remove_venue_fee_with_price_larger_than_1 double precision;
		remove_venue_fee_with_price_smaller_than_1 double precision;
		auction_venue_fee double precision;
		ARCA_route_venue_fee double precision;
		NDAQ_route_venue_fee double precision;
		BATS_route_venue_fee double precision;
		
		totalVenueFee double precision;
		totalSecFees double precision;
		totalTafFee double precision;
		totalFee double precision;
		totalWedbushCommission double precision;
	BEGIN
		-- If we changed some fees in fee_config file on AS or TA, we would change those fees here
		sec_fee := 0.00002240;
		taf_fee := 0.00009;
		wedbus_commission := 0.000035;
		add_venue_fee_with_price_larger_than_1 := -0.0028;
		add_venue_fee_with_price_smaller_than_1 := 0.00;
		remove_venue_fee_with_price_larger_than_1 := 0.003;
		remove_venue_fee_with_price_smaller_than_1 := 0.003;
		auction_venue_fee := 0.0005;
		ARCA_route_venue_fee := 0.003;
		NDAQ_route_venue_fee := 0.0035;
		BATS_route_venue_fee := 0.002;
		
		-- Initialize fees
		totalVenueFee := 0.00;
		totalSecFees := 0.00;
		totalTafFee := 0.00;
		totalFee := 0.00;
		totalWedbushCommission := 0.00;
		-- Calculate total fee
		if (side = 'S') then
			totalSecFees := shares * price * sec_fee;
		end if;
		
		totalTafFee := shares * taf_fee;
		totalWedbushCommission := shares * wedbus_commission;
		
		if (ecn = 'ARCA') then
			-- Add
			if (liquidity = 'A' or liquidity = 'B' or liquidity = 'M' or liquidity = 'D' or liquidity = 'S') then
				if (price >= 1.00) then
					totalVenueFee := shares * add_venue_fee_with_price_larger_than_1;
				else
					totalVenueFee := shares * add_venue_fee_with_price_smaller_than_1;
				end if;
			-- Remove
			elsif (liquidity = 'R' or liquidity = 'E' or liquidity = 'L') then
				if (price >= 1.00) then
					totalVenueFee := shares * remove_venue_fee_with_price_larger_than_1;
				else
					totalVenueFee := shares * price * remove_venue_fee_with_price_smaller_than_1;
				end if;
			-- Auction
			elsif (liquidity = 'G' or liquidity = 'Z' or liquidity = 'O') then
				totalVenueFee := shares * auction_venue_fee;
			elsif (liquidity = 'X' or liquidity = 'F' or liquidity = 'N' or liquidity = 'H' or liquidity = 'C' or liquidity = 'U' or liquidity = 'Y' or liquidity = 'W') then
				totalVenueFee := shares * ARCA_route_venue_fee;
			end if;
			
		elsif (ecn = 'OUCH') then
			-- Add
			if (liquidity = 'A' or liquidity = 'k' or liquidity = 'J' or liquidity = '7' or liquidity = '8' or liquidity = 'e' or liquidity = 'f') then
				if (price >= 1.00) then
					totalVenueFee := shares * add_venue_fee_with_price_larger_than_1;
				else
					totalVenueFee := shares * add_venue_fee_with_price_smaller_than_1;
				end if;
			-- Remove
			elsif (liquidity = 'R' or liquidity = 'm' or liquidity = '6' or liquidity = 'd') then
				if (price >= 1.00) then
					totalVenueFee := shares * remove_venue_fee_with_price_larger_than_1;
				else
					totalVenueFee := shares * price * remove_venue_fee_with_price_smaller_than_1;
				end if;
			-- Auction
			elsif (liquidity = 'O' or liquidity = 'M' or liquidity = 'L' or liquidity = 'H' or liquidity = 'K' or liquidity = 'C') then
				totalVenueFee := shares * auction_venue_fee;
			end if;
		
		elsif (ecn = 'RASH') then
			-- Add
			if (liquidity = 'A' or liquidity = 'J' or liquidity = 'V' or liquidity = 'D' or liquidity = 'F' or liquidity = 'U' or liquidity = 'k' or liquidity = '9' or liquidity = '7' or liquidity = '8' or liquidity = 'e' or liquidity = 'f') then
				if (price >= 1.00) then
					totalVenueFee := shares * add_venue_fee_with_price_larger_than_1;
				else
					totalVenueFee := shares * add_venue_fee_with_price_smaller_than_1;
				end if;
			-- Remove
			elsif (liquidity = 'R' or liquidity = 'm' or liquidity = 'd' or liquidity = '6') then
				if (price >= 1.00) then
					totalVenueFee := shares * remove_venue_fee_with_price_larger_than_1;
				else
					totalVenueFee := shares * price * remove_venue_fee_with_price_smaller_than_1;
				end if;		
			-- Auction
			elsif (liquidity = 'G' or liquidity = 'O' or liquidity = 'M' or liquidity = 'C' or liquidity = 'L' or liquidity = 'H' or liquidity = 'K' or liquidity = 'I' or liquidity = 'T' or liquidity = 'Z') then
				totalVenueFee := shares * auction_venue_fee;
			-- Route venue fee
			elsif (liquidity = 'X' or liquidity = 'Y' or liquidity = 'B' or liquidity = 'P' or liquidity = 'Q') then
				totalVenueFee := shares * NDAQ_route_venue_fee;
			end if;

		elsif (ecn = 'NYSE') then
			-- Add (provider)
			if (liquidity = '2' or liquidity = '8') then
				if (price >= 1.00) then
					totalVenueFee := shares * add_venue_fee_with_price_larger_than_1;
				else
					totalVenueFee := shares * add_venue_fee_with_price_smaller_than_1;
				end if;
			-- Remove (taker and blended)
			elsif (liquidity = '1' or liquidity = '9' or liquidity = '3' or liquidity = ' ') then
				if (price >= 1.00) then
					totalVenueFee := shares * remove_venue_fee_with_price_larger_than_1;
				else
					totalVenueFee := shares * price * remove_venue_fee_with_price_smaller_than_1;
				end if;
			-- Auction
			elsif (liquidity = '4' or liquidity = '5' or liquidity = '6' or liquidity = '7' ) then
				totalVenueFee := shares * auction_venue_fee;
			end if;
			
		elsif (ecn = 'BATZ') then
			-- Add (provider)
			if (liquidity = 'A') then
				if (price >= 1.00) then
					totalVenueFee := shares * add_venue_fee_with_price_larger_than_1;
				else
					totalVenueFee := shares * add_venue_fee_with_price_smaller_than_1;
				end if;
			-- Remove (taker and blended)
			elsif (liquidity = 'R') then
				if (price >= 1.00) then
					totalVenueFee := shares * remove_venue_fee_with_price_larger_than_1;
				else
					totalVenueFee := shares * price * remove_venue_fee_with_price_smaller_than_1;
				end if;
			-- Auction
			elsif (liquidity = 'C' ) then
				totalVenueFee := shares * auction_venue_fee;
			-- Route
			elsif (liquidity = 'X' ) then
				totalVenueFee := shares * BATS_route_venue_fee;
			end if;
		end if;
		
		totalFee := totalSecFees + totalTafFee + totalWedbushCommission + totalVenueFee;
		return totalFee;
		
	END $$;

ALTER FUNCTION calculate_total_fee(price double precision, shares integer, side character, liquidity character, ecn text) OWNER TO postgres;

COMMIT;