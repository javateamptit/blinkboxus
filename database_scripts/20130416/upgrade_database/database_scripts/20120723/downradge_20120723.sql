ALTER TABLE tt_event_log DROP COLUMN description;

DROP TRIGGER update_tt_activities_description ON tt_event_log;

DROP FUNCTION _insert_tt_activities();