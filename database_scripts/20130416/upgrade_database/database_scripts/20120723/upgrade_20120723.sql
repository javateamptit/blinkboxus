-- Add new column to tt_event_log table
ALTER TABLE tt_event_log ADD COLUMN description text;

-- Function: _insert_tt_activities()
-- DROP FUNCTION _insert_tt_activities();
CREATE OR REPLACE FUNCTION _insert_tt_activities()
  RETURNS "trigger" AS
$BODY$
	DECLARE
	userVal		text;
	typeVal		integer;
	dateVal		date;
	timeVal		time;
	currentState 	text[];
	_currentState integer;
	status		text[];
	_status		integer;
	action	text[];
	_action	integer;
	
	orderId 	text;
	actionType	integer;
	symbol		text;
	buyingPower double precision;
	onWhich		text[];
	_onWhich		integer;
	descriptionVal text;
	BEGIN
	timeVal := NEW.time_stamp;
	userVal := initcap(trim(NEW.username));
	typeVal = NEW.activity_type;
	dateVal = (NEW.date);
	timeVal = (NEW.time_stamp);
	
	--Init status 
	status[0] := 'Off-Line';
	status[1] := 'Active';
	status[2] := 'Active Assistant';
	status[3] := 'Monitoring';
	
	currentState[0] := 'Off-Line';
	currentState[1] := 'Active';
	currentState[2] := 'Active Assistant';
	currentState[3] := 'Monitoring';
	
	--Init action
	action[0] := 'Add';
	action[1] := 'Remove';
	
	--Init enable/disable trading 
	onWhich[100] := 'all trading on TS1';
	onWhich[101] := 'ARCA trading on TS1';
	onWhich[102] := 'OUCH trading on TS1';
	onWhich[103] := 'BATS trading on TS1';
	onWhich[104] := 'RASH trading on TS1';
	onWhich[105] := 'OUBX trading on TS1';
	
	onWhich[200] := 'all trading on TS2';
	onWhich[201] := 'ARCA trading on TS2';
	onWhich[202] := 'OUCH trading on TS2';
	onWhich[203] := 'BATS trading on TS2';
	onWhich[204] := 'RASH trading on TS2';
	onWhich[205] := 'OUBX trading on TS2';
	
	onWhich[300] := 'all trading on TS3';
	onWhich[301] := 'ARCA trading on TS3';
	onWhich[302] := 'OUCH trading on TS3';
	onWhich[303] := 'BATS trading on TS3';
	onWhich[304] := 'RASH trading on TS3';
	onWhich[305] := 'OUBX trading on TS3';
	
	onWhich[400] := 'all trading on TS4';
	onWhich[401] := 'ARCA trading on TS4';
	onWhich[402] := 'OUCH trading on TS4';
	onWhich[403] := 'BATS trading on TS4';
	onWhich[404] := 'RASH trading on TS4';
	onWhich[405] := 'OUBX trading on TS4';
	
	onWhich[500] := 'PM trading';
	
	-- If new symbol
	IF (typeVal = 1) THEN -- Connect to AS
		SELECT substring(NEW.log_detail from 1 for 1)::integer INTO _currentState FROM tt_event_log;
		descriptionVal := userVal || ' ' || currentState[_currentState] || ' Connect to AS ' || dateVal || ' ' || timeVal;
	ELSIF (typeVal = 2) THEN -- Switch trader role
		SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer INTO _currentState, _status FROM tt_event_log;
		descriptionVal := userVal || ' ' || currentState[_currentState] || ' Client became ' || status[_status] || ' Trader ' || dateVal || ' ' || timeVal;
	ELSIF (typeVal = 3) THEN -- Disconnect from AS
		SELECT substring(NEW.log_detail from 1 for 1)::integer INTO _currentState FROM tt_event_log;
		descriptionVal := userVal || ' ' || currentState[_currentState] || ' Disconnect from AS ' || dateVal || ' ' || timeVal;
	ELSIF (typeVal = 4) THEN -- Sent Manual order 
		SELECT substring(NEW.log_detail from 1 for 1)::integer, trim(substring(NEW.log_detail from 3 for 3)) INTO _currentState, orderId FROM tt_event_log;
		descriptionVal := userVal || ' ' || currentState[_currentState] || ' Sent Manual Order, OrderID = ' || orderId || ' ' || dateVal || ' ' || timeVal;
	ELSIF (typeVal = 5)	THEN -- Ignored stock list for TS
		SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, trim(substring(NEW.log_detail from 5 for 8)) INTO _currentState, _action, symbol FROM tt_event_log;
		descriptionVal := userVal || ' ' || currentState[_currentState] || ' ' || action[_action] || ' ignored stock list for TS, symbol = ' || symbol || ' ' || dateVal || ' ' || timeVal;
	ELSIF (typeVal = 6)	THEN -- Halted stock list
		SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, trim(substring(NEW.log_detail from 5 for 8)) INTO _currentState, _action, symbol FROM tt_event_log;
		descriptionVal := userVal || ' ' || currentState[_currentState] || ' ' || action[_action] || ' halted stock list, symbol = ' || symbol || ' ' || dateVal || ' ' || timeVal;
	ELSIF (typeVal = 7) THEN -- PM engage/disengage
		SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, trim(substring(NEW.log_detail from 5 for 8)) INTO _currentState, actionType, symbol FROM tt_event_log;
		IF (actionType = 0) THEN -- engage
			descriptionVal := userVal || ' ' || currentState[_currentState] || ' PM engage, symbol = ' || symbol || ' ' || dateVal || ' ' || timeVal;
		ELSIF (actionType = 1) THEN -- disengage
			descriptionVal := userVal || ' ' || currentState[_currentState] || ' PM disengage, symbol = ' || symbol || ' ' || dateVal || ' ' || timeVal;
		ELSE	
		END IF;
	ELSIF (typeVal = 8) THEN -- set buying power
		SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 9)::double precision INTO _currentState, buyingPower FROM tt_event_log;
		descriptionVal := userVal || ' ' || currentState[_currentState] || ' Set Buying Power = ' || buyingPower || ' ' || dateVal || ' ' || timeVal;
	ELSIF (typeVal = 9)	THEN -- Enable/Disable trading
		SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, substring(NEW.log_detail from 5 for 3) INTO _currentState, actionType, _onWhich FROM tt_event_log;
		IF (actionType = 0) THEN -- Enable
			descriptionVal := userVal || ' ' || currentState[_currentState] || ' Enable ' || onWhich[_onWhich] || ' ' || dateVal || ' ' || timeVal;
		ELSIF (actionType = 1) THEN -- Disable
			descriptionVal := userVal || ' ' || currentState[_currentState] || ' Disable ' || onWhich[_onWhich] || ' ' || dateVal || ' ' || timeVal;
		ELSE		
		END IF;
	ELSIF (typeVal = 10) THEN -- Ignore stock list for PM
		SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, trim(substring(NEW.log_detail from 5 for 8)) INTO _currentState, _action, symbol FROM tt_event_log;
		descriptionVal := userVal || ' ' || currentState[_currentState] || ' ' || action[_action] || ' ignored stock list for PM, symbol = ' || symbol || ' ' || dateVal || ' ' || timeVal;	
	ELSE	
	END IF;

	NEW.description := descriptionVal;
	
	RETURN NEW;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION _insert_tt_activities() OWNER TO postgres;

-- Create a new trigger
CREATE TRIGGER update_tt_activities_description
  BEFORE INSERT
  ON tt_event_log
  FOR EACH ROW
  EXECUTE PROCEDURE _insert_tt_activities();