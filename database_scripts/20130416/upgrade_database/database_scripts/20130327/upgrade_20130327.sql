BEGIN;

-- Column: id
-- ALTER TABLE user_login_info DROP COLUMN id;
ALTER TABLE user_login_info ADD COLUMN id serial;

--
DROP TRIGGER str_update_rm_simultaneous ON fills;
--
DROP TRIGGER str_update_rm_for_broken ON brokens;
--

CREATE OR REPLACE FUNCTION update_rm()
  RETURNS trigger AS
$$
DECLARE
	symbolVal		text;
	leave_Shares	integer;
	order_Shares	integer;
	order_Price		float;
	avgPrice		float;
	totalQty		float;
	netPL			float;
	totalMktValue	float;
	rowcount		integer;
	dateVal			date;
	timeVal			time;
	sideVal			text;
	midVal			int;
	current_shares	int;
	current_price	float;
	fill 			record;
	fill_idVal 		int;
	sideBeginDay	character(1);
	sharesBeginDay 	integer;
	priceBeginDay 	float;
	accountVal 		integer;
	ecnVal 			text;
	liquidityVal	character;
	totalFee		double precision;
	temp_totalFee 	double precision;
BEGIN
	-- Get symbol name
	if TG_OP = 'INSERT' then
		fill_idVal := NEW.fill_id;
		--Check to see if insert/update the CEE note, we will return 
		if NEW.is_cee IS TRUE then
			return NEW;
		end if;
	else
		fill_idVal := OLD.fill_id;
		--Check to see if insert/update the CEE note, we will return 
		if OLD.is_cee IS TRUE then
			return OLD;
		end if;
	end if;

	SELECT trim(new_orders.symbol), new_orders.date, fills.time_stamp, new_orders.side, account, new_orders.ecn, fills.liquidity into symbolVal, dateVal, timeVal, sideVal, accountVal, ecnVal, liquidityVal
	FROM new_orders
	JOIN fills ON new_orders.oid = fills.oid
	WHERE fills.fill_id = fill_idVal;

	SELECT side, shares, price into sideBeginDay, sharesBeginDay, priceBeginDay
	FROM open_positions 
	WHERE trim(symbol::text) = trim(symbolVal) AND date = dateVal AND account = accountVal;

	if (sharesBeginDay is null) then 
		leave_Shares := 0;
		avgPrice := 0;
		netPL := 0;
		totalMktValue := 0;
		totalQty := 0;
		totalFee := 0.00;
	else
		IF (sideBeginDay = 'S') THEN
			leave_Shares := -1 * sharesBeginDay;
		ELSE
			leave_Shares := sharesBeginDay;
		END IF;	

		avgPrice := priceBeginDay;
		netPL := 0;
		totalMktValue := sharesBeginDay * priceBeginDay;
		totalQty := sharesBeginDay;
		totalFee := 0.00;
		
		SELECT calculate_total_fee(priceBeginDay, sharesBeginDay, sharesBeginDay, liquidityVal, ecnVal) into temp_totalFee;
		totalFee := temp_totalFee;

	end if;	
	
	--Travel thought fill today and 
	for fill in (select f1.shares, f1.price, f1.fill_id, substring(o.side from 1 for 1) as side, o.ecn, f1.liquidity as liquidity from fills f1 join new_orders o on f1.oid = o.oid 
					where o.date = dateVal and trim(o.symbol) = trim(symbolVal) and o.account = accountVal 
					union all 
					(select cp.shares, cp.price, -1 as "fill_id",substring(o2.side from 1 for 1) as side, o2.ecn, '' as liquidity  from cancel_pendings cp join new_orders o2 on cp.oid = o2.oid where o2.date = dateVal and trim(o2.symbol) = trim(symbolVal) and o2.account = accountVal)) loop
				
		select new_shares, new_price into current_shares, current_price from brokens where fill_id = fill.fill_id;
		
		if current_price is null then
			current_shares  := fill.shares;
			current_price   := fill.price;
		end if;

		IF (leave_Shares > 0) THEN -- Equivalent to LONG
			IF (fill.side = 'B') THEN -- BUY
				avgPrice := (avgPrice * leave_Shares + current_price * current_shares) / (current_shares + leave_Shares);
				leave_Shares := leave_Shares + current_shares;
			ELSE -- SELL
				netPL := netPL + ((current_price - avgPrice) * LEAST(current_shares, leave_Shares));				
				leave_Shares := leave_Shares - current_shares;
				IF (leave_Shares < 0) THEN
					avgPrice := current_price;
				ELSIF (leave_Shares = 0) THEN
					avgPrice := 0;
				END IF;
			END IF;
		ELSIF (leave_Shares < 0) THEN -- Equivalent to SHORT
			IF (fill.side = 'B') THEN -- BUY
				netPL := netPL + ((avgPrice - current_price)*LEAST(current_shares, abs(leave_Shares)));				
				leave_Shares := leave_Shares + current_shares;
				IF (leave_Shares = 0) THEN
					avgPrice := 0;
				ELSIF (leave_Shares > 0) THEN
					avgPrice := current_price;
				END IF;
			ELSE -- SELL
				avgPrice := (avgPrice*(-leave_Shares) + current_price * current_shares) / (current_shares + (-leave_Shares));
				leave_Shares := leave_Shares - current_shares;				
			END IF;
		ELSE -- leave_Shares = 0
			avgPrice := current_price;
			IF (fill.side = 'B') THEN
				leave_Shares := current_shares;
			ELSE -- Action is Sell or Short Sell
				leave_Shares := -current_shares;
			END IF;
		END IF;
	
		totalMktValue := avgPrice * abs(leave_Shares);
		totalQty := totalQty + current_shares;	
		
		if (fill.liquidity <> NULL) then
			select calculate_total_fee(fill.price, fill.shares, fill.side, fill.liquidity, fill.ecn) into temp_totalFee;
			
			totalFee := totalFee + temp_totalFee;
		end if;
	end loop;
			
	-- update RM
	UPDATE risk_managements
	SET average_price = avgPrice,
		net_quantity = leave_Shares, 
		matched_profit_loss = netPL, 
		total_market_value = totalMktValue, 
		total_quantity = totalQty, 
		last_update = timeVal,
		total_fee = totalFee
	WHERE trim(symbol) = trim(symbolVal) and date = dateVal and account = accountVal;
	
	RETURN NEW;
END $$
  LANGUAGE plpgsql;
  
ALTER FUNCTION update_rm() OWNER TO postgres;

CREATE TRIGGER str_update_rm_for_broken
	AFTER INSERT OR UPDATE OR DELETE
	ON brokens
	FOR EACH ROW
	EXECUTE PROCEDURE update_rm();

COMMIT;