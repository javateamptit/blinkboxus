#!/bin/sh

# DB name
db_name=eaa10_201208
echo "Upgrading for database $db_name ..." 

# 20121004
db_script=database_scripts/20121004/upgrade_20121004.sql
echo "Running db script $db_script ..."
cmd=`psql -U postgres $db_name -f $db_script`
echo "Result: $cmd"
if [ -n "$cmd" ];
then
	if [[ "$cmd" == *ROLLBACK* ]] 
	then
		echo "Fail! Skiped db script $db_script"
		exit;
	else
		echo "Successful run $db_script"
	fi
else
	echo "Fail! Skiped db script $db_script"
	exit;
fi

# 20121016
db_script=database_scripts/20121016/upgrade_20121016.sql
echo "Running db script $db_script ..."
cmd=`psql -U postgres $db_name -f $db_script`
echo "Result: $cmd"
if [ -n "$cmd" ];
then
	if [[ "$cmd" == *ROLLBACK* ]] 
	then
		echo "Fail! Skiped db script $db_script"
		exit;
	else
		echo "Successful run $db_script"
	fi
else
	echo "Fail! Skiped db script $db_script"
	exit;
fi

# 20121025
db_script=database_scripts/20121025/upgrade_20121025.sql
echo "Running db script $db_script ..."
cmd=`psql -U postgres $db_name -f $db_script`
echo "Result: $cmd"
if [ -n "$cmd" ];
then
	if [[ "$cmd" == *ROLLBACK* ]] 
	then
		echo "Fail! Skiped db script $db_script"
		exit;
	else
		echo "Successful run $db_script"
	fi
else
	echo "Fail! Skiped db script $db_script"
	exit;
fi

# 20121113
db_script=database_scripts/20121113/upgrade_20121113.sql
echo "Running db script $db_script ..."
cmd=`psql -U postgres $db_name -f $db_script`
echo "Result: $cmd"
if [ -n "$cmd" ];
then
	if [[ "$cmd" == *ROLLBACK* ]] 
	then
		echo "Fail! Skiped db script $db_script"
		exit;
	else
		echo "Successful run $db_script"
	fi
else
	echo "Fail! Skiped db script $db_script"
	exit;
fi

# 20121122
db_script=database_scripts/20121122/upgrade_20121122.sql
echo "Running db script $db_script ..."
cmd=`psql -U postgres $db_name -f $db_script`
echo "Result: $cmd"
if [ -n "$cmd" ];
then
	if [[ "$cmd" == *ROLLBACK* ]] 
	then
		echo "Fail! Skiped db script $db_script"
		exit;
	else
		echo "Successful run $db_script"
	fi
else
	echo "Fail! Skiped db script $db_script"
	exit;
fi

# 20130114
db_script=database_scripts/20130114/upgrade_20130114.sql
echo "Running db script $db_script ..."
cmd=`psql -U postgres $db_name -f $db_script`
echo "Result: $cmd"
if [ -n "$cmd" ];
then
	if [[ "$cmd" == *ROLLBACK* ]] 
	then
		echo "Fail! Skiped db script $db_script"
		exit;
	else
		echo "Successful run $db_script"
	fi
else
	echo "Fail! Skiped db script $db_script"
	exit;
fi

# 20130227
db_script=database_scripts/20130227/upgrade_20130227.sql
echo "Running db script $db_script ..."
cmd=`psql -U postgres $db_name -f $db_script`
echo "Result: $cmd"
if [ -n "$cmd" ];
then
	if [[ "$cmd" == *ROLLBACK* ]] 
	then
		echo "Fail! Skiped db script $db_script"
		exit;
	else
		echo "Successful run $db_script"
	fi
else
	echo "Fail! Skiped db script $db_script"
	exit;
fi

# 20130312
db_script=database_scripts/20130312/upgrade_20130312.sql
echo "Running db script $db_script ..."
cmd=`psql -U postgres $db_name -f $db_script`
echo "Result: $cmd"
if [ -n "$cmd" ];
then
	if [[ "$cmd" == *ROLLBACK* ]] 
	then
		echo "Fail! Skiped db script $db_script"
		exit;
	else
		echo "Successful run $db_script"
	fi
else
	echo "Fail! Skiped db script $db_script"
	exit;
fi

# 20130327
db_script=database_scripts/20130327/upgrade_20130327.sql
echo "Running db script $db_script ..."
cmd=`psql -U postgres $db_name -f $db_script`
echo "Result: $cmd"
if [ -n "$cmd" ];
then
	if [[ "$cmd" == *ROLLBACK* ]] 
	then
		echo "Fail! Skiped db script $db_script"
		exit;
	else
		echo "Successful run $db_script"
	fi
else
	echo "Fail! Skiped db script $db_script"
	exit;
fi

# 20130410
db_script=database_scripts/20130410/upgrade_20130410.sql
echo "Running db script $db_script ..."
cmd=`psql -U postgres $db_name -f $db_script`
echo "Result: $cmd"
if [ -n "$cmd" ];
then
	if [[ "$cmd" == *ROLLBACK* ]] 
	then
		echo "Fail! Skiped db script $db_script"
		exit;
	else
		echo "Successful run $db_script"
	fi
else
	echo "Fail! Skiped db script $db_script"
	exit;
fi

# Successful upgrade database
echo "Successful upgraded $db_name" 
