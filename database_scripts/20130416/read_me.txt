* Download all scripts to upgrade:
- Please copy the folder "upgrade_database" to the EAA database host
- Change into "upgrade_database" directory (on the EAA database host)

* How to upgrade all historical databases:
- Please run the commands to upgrade all historical databases (eaa10_201206 -> eaa10_201303):
	$chmod 744 upgrade_*
	$./upgrade_database_all.sh

* The update is needed to run with new release, all databases will upgrade (eaa10_201304 -> eaa10_201206):
1) To upgrade, please run the command:
	$./upgrade_20130416.sh

2) If we have "critical issues" of TT and need downgrade it, please run the command:
	$./downgrade_20130416.sh
