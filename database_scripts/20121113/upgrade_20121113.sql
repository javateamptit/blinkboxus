BEGIN;

-- Add column to new_orders, fills, cancels, order_rejects
ALTER TABLE new_orders ADD COLUMN entry_exit_ref integer;
ALTER TABLE fills ADD COLUMN entry_exit_ref integer;
ALTER TABLE cancels ADD COLUMN entry_exit_ref integer;
ALTER TABLE order_rejects ADD COLUMN entry_exit_ref integer;

-- Update funtions
CREATE OR REPLACE FUNCTION insertexecutedorder4variant1(_orderid integer, _date date, _shares integer, _price double precision, _timestamp time without time zone, _seqnum text, _execid text, _msgcontents text, _liquidityindicator character, _status text, _leftshares integer, _avgprice double precision, _entryExitRefNum integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	orderID integer;
BEGIN
	
	SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
	
	IF orderID IS NULL THEN
		RETURN -1;
	END IF;
	
	INSERT INTO fills (oid, shares, price, time_stamp, msgseq_num, origclord_id, exec_id, msg_content, liquidity, entry_exit_ref) VALUES (orderID, _shares, _price, _timeStamp, _seqNum, _orderID, _execID, _msgContents, _liquidityIndicator, _entryExitRefNum);

	UPDATE new_orders SET status = _status, left_shares = _leftShares, avg_price = _avgPrice WHERE oid = orderID;
	
	RETURN 0;
END
$$;


ALTER FUNCTION public.insertexecutedorder4variant1(_orderid integer, _date date, _shares integer, _price double precision, _timestamp time without time zone, _seqnum text, _execid text, _msgcontents text, _liquidityindicator character, _status text, _leftshares integer, _avgprice double precision, _entryExitRefNum integer) OWNER TO postgres;

--------------------------
CREATE OR REPLACE FUNCTION insertexecutedorder4variant2(_orderid integer, _date date, _shares integer, _price double precision, _timestamp time without time zone, _seqnum text, _execid text, _msgcontents text, _liquidityindicator character, _status text, _leftshares integer, _avgprice double precision, _entryExitRefNum integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	orderID integer;
BEGIN
	
	SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
	
	IF orderID IS NULL THEN
		RETURN -1;
	END IF;
	
	INSERT INTO fills (oid, shares, price, time_stamp, msgseq_num, origclord_id, exec_id, msg_content, liquidity, entry_exit_ref) VALUES (orderID, _shares, _price, _timeStamp, _seqNum, _orderID, _execID, _msgContents, _liquidityIndicator, _entryExitRefNum);

	UPDATE new_orders SET status = _status, left_shares = _leftShares, avg_price = _avgPrice WHERE oid = orderID;
	UPDATE new_orders SET price = _price WHERE oid = orderID AND price = 0;
	
	RETURN 0;
END
$$;


ALTER FUNCTION public.insertexecutedorder4variant2(_orderid integer, _date date, _shares integer, _price double precision, _timestamp time without time zone, _seqnum text, _execid text, _msgcontents text, _liquidityindicator character, _status text, _leftshares integer, _avgprice double precision, _entryExitRefNum integer) OWNER TO postgres;

---------------------------
CREATE OR REPLACE FUNCTION insertexecutedorder4variant3(_orderid integer, _date date, _shares integer, _price double precision, _timestamp time without time zone, _seqnum text, _arcaexorderid text, _execid text, _msgcontents text, _liquidityindicator character, _status text, _leftshares integer, _avgprice double precision, _entryExitRefNum integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	orderID integer;
BEGIN
	
	SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
	
	IF orderID IS NULL THEN
		RETURN -1;
	END IF;
	
	INSERT INTO fills (oid, shares, price, time_stamp, msgseq_num, origclord_id, arcaex_order_id, exec_id, msg_content, liquidity, entry_exit_ref) VALUES (orderID, _shares, _price, _timeStamp, _seqNum, _orderID, _arcaExOrderID, _execID, _msgContents, _liquidityIndicator, _entryExitRefNum);

	UPDATE new_orders SET status = _status, left_shares = _leftShares, avg_price = _avgPrice WHERE oid = orderID;
	
	RETURN 0;
END
$$;


ALTER FUNCTION public.insertexecutedorder4variant3(_orderid integer, _date date, _shares integer, _price double precision, _timestamp time without time zone, _seqnum text, _arcaexorderid text, _execid text, _msgcontents text, _liquidityindicator character, _status text, _leftshares integer, _avgprice double precision, _entryExitRefNum integer) OWNER TO postgres;

----------------------------

CREATE OR REPLACE FUNCTION insertfillorder4nyse_ccg(_orderid integer, _date date, _shares integer, _price double precision, _timestamp time without time zone, _seqnum text, _meorderid text, _execid text, _msgcontents text, _billingindicator character, _status text, _leftshares integer, _avgprice double precision, _entryExitRefNum integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	orderID integer;
BEGIN
	
	SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
	
	IF orderID IS NULL THEN
		RETURN -1;
	END IF;
	
	INSERT INTO fills (oid, shares, price, time_stamp, msgseq_num, origclord_id, arcaex_order_id, exec_id, msg_content, liquidity, entry_exit_ref) VALUES (orderID, _shares, _price, _timestamp, _seqnum, _orderID, _meorderid, _execid, _msgcontents, _billingindicator, _entryExitRefNum);

	UPDATE new_orders SET status = _status, left_shares = _leftshares, avg_price = _avgprice WHERE oid = orderID;
	
	RETURN 0;
END$$;


ALTER FUNCTION public.insertfillorder4nyse_ccg(_orderid integer, _date date, _shares integer, _price double precision, _timestamp time without time zone, _seqnum text, _meorderid text, _execid text, _msgcontents text, _billingindicator character, _status text, _leftshares integer, _avgprice double precision, _entryExitRefNum integer) OWNER TO postgres;

-------------------

CREATE OR REPLACE FUNCTION insertmanualcanceledordervariant2(_orderid integer, _date date, _timestamp time without time zone, _seqnum text, _arcaex_orderid text, _msgcontents text, _status text, _entryExitRefNum integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	orderID integer;
BEGIN
	
	SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
	
	IF orderID IS NULL THEN
		RETURN -1;
	END IF;

	INSERT INTO cancels (oid, time_stamp, msgseq_num, origclord_id, arcaex_order_id, msg_content, entry_exit_ref) VALUES (orderID, _timeStamp, _seqNum, _orderID, _arcaex_orderid, _msgContents, _entryExitRefNum);
	  
 	UPDATE new_orders SET status = _status where oid = orderID;
	
	RETURN 0;
END
$$;


ALTER FUNCTION public.insertmanualcanceledordervariant2(_orderid integer, _date date, _timestamp time without time zone, _seqnum text, _arcaex_orderid text, _msgcontents text, _status text, _entryExitRefNum integer) OWNER TO postgres;

------------

CREATE OR REPlACE FUNCTION insertcanceledorder4nasdaq(_orderid integer, _date date, _timestamp time without time zone, _seqnum text, _msgcontents text, _status text, _entryExitRefNum integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	orderID integer;
BEGIN
	
	SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
	
	IF orderID IS NULL THEN
		RETURN -1;
	END IF;

	INSERT INTO cancels (oid, time_stamp, msgseq_num, origclord_id, msg_content, entry_exit_ref) VALUES (orderID, _timeStamp, _seqNum, _orderID, _msgContents, _entryExitRefNum);
	  
 	UPDATE new_orders SET status = _status where oid = orderID;
	
	RETURN 0;
END
$$;


ALTER FUNCTION public.insertcanceledorder4nasdaq(_orderid integer, _date date, _timestamp time without time zone, _seqnum text, _msgcontents text, _status text, _entryExitRefNum integer) OWNER TO postgres;

------------

CREATE OR REPlACE FUNCTION insertmanualcanceledorder4nyse_ccg(_orderid integer, _date date, _timestamp time without time zone, _seqnum text, _meorderid text, _msgcontents text, _status text, _entryExitRefNum integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	orderID integer;
BEGIN
	
	SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderid and date = _date;
	
	IF orderID IS NULL THEN
		RETURN -1;
	END IF;

	INSERT INTO cancels (oid, time_stamp, msgseq_num, origclord_id, arcaex_order_id, msg_content, entry_exit_ref) VALUES (orderID, _timeStamp, _seqNum, _orderid, _meorderid, _msgContents, _entryExitRefNum);
	  
 	UPDATE new_orders SET status = _status where oid = orderID;
	
	RETURN 0;
END$$;


ALTER FUNCTION public.insertmanualcanceledorder4nyse_ccg(_orderid integer, _date date, _timestamp time without time zone, _seqnum text, _meorderid text, _msgcontents text, _status text, _entryExitRefNum integer) OWNER TO postgres;

-----------

CREATE OR REPlACE FUNCTION insertreject4arca_direct(_orderid integer, _date date, _type character, _timestamp time without time zone, _seqnum text, _text text, _msgcontents text, _status text, _entryExitRefNum integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	orderID integer;
BEGIN
	
	SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
	
	IF orderID IS NULL THEN
		RETURN -1;
	END IF;
	
	IF (_type = '1') THEN
	
		INSERT INTO order_rejects (oid, time_stamp, msgseq_num, origclord_id, reason, msg_content, entry_exit_ref) VALUES (orderID, _timeStamp, _seqNum, _orderID, _text, _msgContents, _entryExitRefNum);
		
		UPDATE new_orders SET status = _status where oid = orderID;
		
		RETURN 0;
	ELSIF (_type = '2') THEN 
				
		INSERT INTO cancel_rejects (oid, time_stamp, msgseq_num, origclord_id, reason, msg_content) VALUES (orderID, _timeStamp, _seqNum, _orderID, _text, _msgContents);
	
		UPDATE new_orders SET status = _status where oid = orderID;
		RETURN 0;
	ELSE
		RETURN 0;
	END IF;
END
$$;


ALTER FUNCTION public.insertreject4arca_direct(_orderid integer, _date date, _type character, _timestamp time without time zone, _seqnum text, _text text, _msgcontents text, _status text, _entryExitRefNum integer) OWNER TO postgres;

------------

CREATE OR REPlACE FUNCTION insertrejectedorder4nasdaq(_orderid integer, _date date, _timestamp time without time zone, _seqnum text, _reason text, _msgcontents text, _status text, _entryExitRefNum integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	orderID integer;
BEGIN
	
	SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
	
	IF orderID IS NULL THEN
		RETURN -1;
	END IF;

	INSERT INTO order_rejects (oid, time_stamp, msgseq_num, origclord_id, reason, msg_content, entry_exit_ref) VALUES (orderID, _timeStamp, _seqNum, _orderID, _reason, _msgContents, _entryExitRefNum);
	
 	UPDATE new_orders SET status = _status where oid = orderID;
	
	RETURN 0;
END
$$;


ALTER FUNCTION public.insertrejectedorder4nasdaq(_orderid integer, _date date, _timestamp time without time zone, _seqnum text, _reason text, _msgcontents text, _status text, _entryExitRefNum integer) OWNER TO postgres;
---------------

CREATE OR REPlACE FUNCTION insertreject4nyse_ccg(_orderid integer, _date date, _type character, _timestamp time without time zone, _seqnum text, _text text, _msgcontents text, _status text, _entryExitRefNum integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	orderID integer;
BEGIN
	
	SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
	
	IF orderID IS NULL THEN
		RETURN -1;
	END IF;
	
	IF (_type = '1') THEN
	
		INSERT INTO order_rejects (oid, time_stamp, msgseq_num, origclord_id, reason, msg_content, entry_exit_ref) VALUES (orderID, _timeStamp, _seqNum, _orderID, _text, _msgContents, _entryExitRefNum);
		
		UPDATE new_orders SET status = _status where oid = orderID;
		
		RETURN 0;
	ELSIF (_type = '2') THEN 
				
		INSERT INTO cancel_rejects (oid, time_stamp, msgseq_num, origclord_id, reason, msg_content) VALUES (orderID, _timeStamp, _seqNum, _orderID, _text, _msgContents);
	
		UPDATE new_orders SET status = _status where oid = orderID;
		RETURN 0;
	ELSE
		RETURN 0;
	END IF;
END$$;


ALTER FUNCTION public.insertreject4nyse_ccg(_orderid integer, _date date, _type character, _timestamp time without time zone, _seqnum text, _text text, _msgcontents text, _status text, _entryExitRefNum integer) OWNER TO postgres;

--------------
DROP VIEW vw_exit_delays;
CREATE VIEW vw_exit_delays AS
    SELECT exit.id, exit.date, EXTRACT (MICROSECONDS from (exit.time_stamp - cause.time_stamp)) as delay FROM 
	(SELECT date, ts_id, cross_id, order_rejects.entry_exit_ref, order_rejects.time_stamp FROM new_orders JOIN order_rejects ON new_orders.oid = order_rejects.oid
		UNION SELECT date, ts_id, cross_id, cancels.entry_exit_ref, cancels.time_stamp FROM new_orders JOIN cancels ON new_orders.oid = cancels.oid
		UNION SELECT date, ts_id, cross_id, fills.entry_exit_ref, fills.time_stamp FROM new_orders JOIN fills ON new_orders.oid = fills.oid) cause
	JOIN
	(SELECT date, ts_id, cross_id, entry_exit_ref, min(time_stamp) AS time_stamp, min(id) AS id FROM new_orders WHERE ts_id <> -1 AND entry_exit='Exit' GROUP BY ts_id, cross_id, entry_exit_ref, date) exit
	ON cause.date = exit.date AND cause.ts_id = exit.ts_id AND cause.cross_id = exit.cross_id AND cause.entry_exit_ref = exit.entry_exit_ref order by id;

ALTER TABLE public.vw_exit_delays OWNER TO postgres;

--------------
DROP VIEW vw_fill_delays;
CREATE VIEW vw_fill_delays AS
	SELECT f.fill_id, n.entry_exit, EXTRACT (MICROSECONDS FROM (f.time_stamp - a.time_stamp)) AS fill_delay, n.date 
	FROM ((new_orders n JOIN order_acks a ON ((a.oid = n.oid))) JOIN fills f ON ((f.oid = n.oid))) 
	WHERE ((n.ts_id <> (-1)) AND (n.order_id <> 0))
	ORDER BY f.fill_id, n.date;

ALTER TABLE public.vw_fill_delays OWNER TO postgres;

---------------
DROP VIEW vw_ack_delays;
CREATE VIEW vw_ack_delays AS
	SELECT new_orders.oid, new_orders.entry_exit, new_orders.date, EXTRACT (MICROSECONDS FROM (order_acks.time_stamp - new_orders.time_stamp)) AS ack_delay 
	FROM new_orders JOIN order_acks ON order_acks.oid = new_orders.oid
	WHERE new_orders.ts_id <> -1 AND new_orders.order_id <> 0
	ORDER BY new_orders.oid, new_orders.date;
ALTER TABLE public.vw_ack_delays OWNER TO postgres;

---------------
DROP VIEW vw_cancel_delays;
CREATE VIEW vw_cancel_delays AS
    SELECT c.id, n.entry_exit, EXTRACT (MICROSECONDS FROM (c.time_stamp - a.time_stamp)) AS cancel_delay, n.date FROM ((new_orders n JOIN order_acks a ON ((a.oid = n.oid))) JOIN cancels c ON ((c.oid = n.oid))) WHERE ((n.ts_id <> (-1)) AND (n.order_id <> 0)) ORDER BY c.id;
ALTER TABLE public.vw_cancel_delays OWNER TO postgres;

COMMIT;