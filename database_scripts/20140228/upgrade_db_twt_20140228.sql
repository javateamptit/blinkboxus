BEGIN;

CREATE OR REPLACE FUNCTION clear_database() RETURNS integer
    LANGUAGE plpgsql
    AS $$
BEGIN
	DELETE FROM summary_ack_fills;
	DELETE FROM summary_ack_fills_15;
	DELETE FROM summary_ack_fills_30;
	DELETE FROM summary_ack_fills_60;

	DELETE FROM summary_entry_exits;
	DELETE FROM summary_entry_exits_15;
	DELETE FROM summary_entry_exits_30;
	DELETE FROM summary_entry_exits_60;

	DELETE FROM summary_launch_entry_ecns;
	DELETE FROM summary_launch_entry_ecns_15;
	DELETE FROM summary_launch_entry_ecns_30;
	DELETE FROM summary_launch_entry_ecns_60;
	
	DELETE FROM summary_launch_exit_ecns;
	DELETE FROM summary_launch_exit_ecns_15;
	DELETE FROM summary_launch_exit_ecns_30;
	DELETE FROM summary_launch_exit_ecns_60;

	DELETE FROM summary_stock_types;
	DELETE FROM summary_stock_types_15;
	DELETE FROM summary_stock_types_30;
	DELETE FROM summary_stock_types_60;
    -- 
	DELETE FROM summary_order_types;
	DELETE FROM summary_order_types_15;
	DELETE FROM summary_order_types_30;
	DELETE FROM summary_order_types_60;

	DELETE FROM summary_profit_losses;
	DELETE FROM summary_profit_losses_15;
	DELETE FROM summary_profit_losses_30;
	DELETE FROM summary_profit_losses_60;
	
	DELETE FROM summary_net_profit_losses;
	RETURN 0;
END
$$;

--
-- Name: summary_launch_exit_ecns_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE summary_launch_exit_ecns_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
	
ALTER TABLE public.summary_launch_exit_ecns_id_seq OWNER TO postgres;

--
-- Name: summary_launch_exit_ecns; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE summary_launch_exit_ecns (
    id integer DEFAULT nextval('summary_launch_exit_ecns_id_seq'::regclass) NOT NULL,
    date date NOT NULL,
    time_stamp time without time zone NOT NULL,
    total_second integer,
    ts_id integer,
    ecn_launch_id integer,
    ecn_exit_id integer,
    filled integer,
    total integer,
    ask_filled integer,
    ask_total integer,
    bid_filled integer,
    bid_total integer,
    side integer
);


ALTER TABLE public.summary_launch_exit_ecns OWNER TO postgres;

--
-- Name: summary_launch_exit_ecns_15_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE summary_launch_exit_ecns_15_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
	
ALTER TABLE public.summary_launch_exit_ecns_15_id_seq OWNER TO postgres;
	
--
-- Name: summary_launch_exit_ecns_15; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE summary_launch_exit_ecns_15 (
    id integer DEFAULT nextval('summary_launch_exit_ecns_15_id_seq'::regclass) NOT NULL,
    date date NOT NULL,
    time_stamp time without time zone NOT NULL,
    total_second integer,
    ts_id integer,
    ecn_launch_id integer,
    ecn_exit_id integer,
    filled integer,
    total integer,
    ask_filled integer,
    ask_total integer,
    bid_filled integer,
    bid_total integer,
    side integer
);


ALTER TABLE public.summary_launch_exit_ecns_15 OWNER TO postgres;

--
-- Name: summary_launch_exit_ecns_30_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE summary_launch_exit_ecns_30_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.summary_launch_exit_ecns_30_id_seq OWNER TO postgres;

--
-- Name: summary_launch_exit_ecns_30; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE summary_launch_exit_ecns_30 (
    id integer DEFAULT nextval('summary_launch_exit_ecns_30_id_seq'::regclass) NOT NULL,
    date date NOT NULL,
    time_stamp time without time zone NOT NULL,
    total_second integer,
    ts_id integer,
    ecn_launch_id integer,
    ecn_exit_id integer,
    filled integer,
    total integer,
    ask_filled integer,
    ask_total integer,
    bid_filled integer,
    bid_total integer,
    side integer
);


ALTER TABLE public.summary_launch_exit_ecns_30 OWNER TO postgres;

--
-- Name: summary_launch_exit_ecns_60_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE summary_launch_exit_ecns_60_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.summary_launch_exit_ecns_60_id_seq OWNER TO postgres;

--
-- Name: summary_launch_exit_ecns_60; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE summary_launch_exit_ecns_60 (
    id integer DEFAULT nextval('summary_launch_exit_ecns_60_id_seq'::regclass) NOT NULL,
    date date NOT NULL,
    time_stamp time without time zone NOT NULL,
    total_second integer,
    ts_id integer,
    ecn_launch_id integer,
    ecn_exit_id integer,
    filled integer,
    total integer,
    ask_filled integer,
    ask_total integer,
    bid_filled integer,
    bid_total integer,
    side integer
);


ALTER TABLE public.summary_launch_exit_ecns_60 OWNER TO postgres;

--
-- Name: process_insert_launch_exit_ecns(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION process_insert_launch_exit_ecns() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
	i_filled		integer;
	i_total			integer;	
	i_ask_filled		integer;
	i_ask_total		integer;
	i_bid_filled		integer;
	i_bid_total		integer;

BEGIN	
	IF (NEW.total_second%900=0) THEN	
		SELECT  sum(filled), sum(total), sum(ask_filled), sum(ask_total), sum(bid_filled), sum(bid_total)
			INTO  i_filled, i_total, i_ask_filled, i_ask_total, i_bid_filled, i_bid_total
		FROM summary_launch_exit_ecns
		WHERE "date" = NEW.date
			AND ts_id = NEW.ts_id 
			AND ecn_launch_id = NEW.ecn_launch_id 
			AND ecn_exit_id = NEW.ecn_exit_id 
			AND side = NEW.side
			AND total_second <= NEW.total_second
			AND total_second > NEW.total_second -900;			

		INSERT INTO summary_launch_exit_ecns_15("date", time_stamp, ts_id, ecn_launch_id, ecn_exit_id, total_second, filled, total, ask_filled, ask_total, bid_filled, bid_total,side)
		VALUES (NEW.date, NEW.time_stamp, NEW.ts_id, NEW.ecn_launch_id, NEW.ecn_exit_id, NEW.total_second,  i_filled, i_total, i_ask_filled, i_ask_total, i_bid_filled, i_bid_total,NEW.side);						
	END IF; 	

	RETURN NEW;
END
$$;

--
-- Name: process_insert_launch_exit_ecns_15(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION process_insert_launch_exit_ecns_15() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
	i_filled		integer;
	i_total			integer;	
	i_ask_filled		integer;
	i_ask_total		integer;
	i_bid_filled		integer;
	i_bid_total		integer;

BEGIN	
	IF (NEW.total_second%1800=0) THEN	

		SELECT  sum(filled), sum(total), sum(ask_filled), sum(ask_total), sum(bid_filled), sum(bid_total)
			INTO  i_filled, i_total, i_ask_filled, i_ask_total, i_bid_filled, i_bid_total
		FROM summary_launch_exit_ecns_15
		WHERE "date" = NEW.date
			AND ts_id = NEW.ts_id 
			AND ecn_launch_id = NEW.ecn_launch_id 
			AND ecn_exit_id = NEW.ecn_exit_id 
			AND side = NEW.side
			AND total_second <= NEW.total_second
			AND total_second > NEW.total_second -1800;			

		INSERT INTO summary_launch_exit_ecns_30("date", time_stamp, ts_id, ecn_launch_id, ecn_exit_id, total_second, filled, total, ask_filled, ask_total, bid_filled, bid_total,side)
		VALUES (NEW.date, NEW.time_stamp, NEW.ts_id, NEW.ecn_launch_id, NEW.ecn_exit_id, NEW.total_second,  i_filled, i_total, i_ask_filled, i_ask_total, i_bid_filled, i_bid_total,NEW.side);						
	END IF; 	

	RETURN NEW;
END
$$;


ALTER FUNCTION public.process_insert_launch_exit_ecns_15() OWNER TO postgres;

--
-- Name: process_insert_launch_exit_ecns_30(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION process_insert_launch_exit_ecns_30() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
	i_filled		integer;
	i_total			integer;	
	i_ask_filled		integer;
	i_ask_total		integer;
	i_bid_filled		integer;
	i_bid_total		integer;

BEGIN	
	IF (NEW.total_second%3600=0) THEN	

		SELECT  sum(filled), sum(total), sum(ask_filled), sum(ask_total), sum(bid_filled), sum(bid_total)
			INTO  i_filled, i_total, i_ask_filled, i_ask_total, i_bid_filled, i_bid_total
		FROM summary_launch_exit_ecns_30
		WHERE "date" = NEW.date
			AND ts_id = NEW.ts_id 
			AND ecn_launch_id = NEW.ecn_launch_id 
			AND ecn_exit_id = NEW.ecn_exit_id 
			AND side = NEW.side
			AND total_second <= NEW.total_second
			AND total_second > NEW.total_second - 3600;			

		INSERT INTO summary_launch_exit_ecns_60("date", time_stamp, ts_id, ecn_launch_id, ecn_exit_id, total_second, filled, total, ask_filled, ask_total, bid_filled, bid_total,side)
		VALUES (NEW.date, NEW.time_stamp, NEW.ts_id, NEW.ecn_launch_id, NEW.ecn_exit_id, NEW.total_second,  i_filled, i_total, i_ask_filled, i_ask_total, i_bid_filled, i_bid_total,NEW.side);						
	END IF; 	

	RETURN NEW;
END
$$;


ALTER FUNCTION public.process_insert_launch_exit_ecns_30() OWNER TO postgres;

--
-- Name: process_insert_launch_exit_ecns; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER process_insert_launch_exit_ecns AFTER INSERT ON summary_launch_exit_ecns FOR EACH ROW EXECUTE PROCEDURE process_insert_launch_exit_ecns();


--
-- Name: process_insert_launch_exit_ecns_15; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER process_insert_launch_exit_ecns_15 AFTER INSERT ON summary_launch_exit_ecns_15 FOR EACH ROW EXECUTE PROCEDURE process_insert_launch_exit_ecns_15();


--
-- Name: process_insert_launch_exit_ecns_30; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER process_insert_launch_exit_ecns_30 AFTER INSERT ON summary_launch_exit_ecns_30 FOR EACH ROW EXECUTE PROCEDURE process_insert_launch_exit_ecns_30();


--
-- Name: summary_launch_exit_ecns_15_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY summary_launch_exit_ecns_15
    ADD CONSTRAINT summary_launch_exit_ecns_15_pkey PRIMARY KEY (id);


--
-- Name: summary_launch_exit_ecns_30_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY summary_launch_exit_ecns_30
    ADD CONSTRAINT summary_launch_exit_ecns_30_pkey PRIMARY KEY (id);


--
-- Name: summary_launch_exit_ecns_60_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY summary_launch_exit_ecns_60
    ADD CONSTRAINT summary_launch_exit_ecns_60_pkey PRIMARY KEY (id);


--
-- Name: summary_launch_exit_ecns_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY summary_launch_exit_ecns
    ADD CONSTRAINT summary_launch_exit_ecns_pkey PRIMARY KEY (id);
	
--
-- Name: summary_launch_exit_ecns_15_all; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_15_all ON summary_launch_exit_ecns_15 USING btree (date, time_stamp, ts_id, ecn_launch_id, ecn_exit_id);


--
-- Name: summary_launch_exit_ecns_15_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_15_date ON summary_launch_exit_ecns_15 USING btree (date);


--
-- Name: summary_launch_exit_ecns_15_date_ecn_exit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_15_date_ecn_exit_id ON summary_launch_exit_ecns_15 USING btree (date, ecn_exit_id);


--
-- Name: summary_launch_exit_ecns_15_date_ecn_launch_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_15_date_ecn_launch_id ON summary_launch_exit_ecns_15 USING btree (date, ecn_launch_id);


--
-- Name: summary_launch_exit_ecns_15_date_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_15_date_ts_id ON summary_launch_exit_ecns_15 USING btree (date, ts_id);


--
-- Name: summary_launch_exit_ecns_15_date_ts_id_ecn_exit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_15_date_ts_id_ecn_exit_id ON summary_launch_exit_ecns_15 USING btree (date, ts_id, ecn_exit_id);


--
-- Name: summary_launch_exit_ecns_15_date_ts_id_ecn_launch_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_15_date_ts_id_ecn_launch_id ON summary_launch_exit_ecns_15 USING btree (date, ts_id, ecn_launch_id);


--
-- Name: summary_launch_exit_ecns_15_date_ts_id_ecn_launch_id_ecn_exit; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_15_date_ts_id_ecn_launch_id_ecn_exit ON summary_launch_exit_ecns_15 USING btree (date, ts_id, ecn_launch_id, ecn_exit_id);


--
-- Name: summary_launch_exit_ecns_15_exit_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_15_exit_ecn_id ON summary_launch_exit_ecns_15 USING btree (ecn_exit_id);


--
-- Name: summary_launch_exit_ecns_15_launch_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_15_launch_ecn_id ON summary_launch_exit_ecns_15 USING btree (ecn_launch_id);


--
-- Name: summary_launch_exit_ecns_15_time_stamp; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_15_time_stamp ON summary_launch_exit_ecns_15 USING btree (time_stamp);


--
-- Name: summary_launch_exit_ecns_15_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_15_ts_id ON summary_launch_exit_ecns_15 USING btree (ts_id);


--
-- Name: summary_launch_exit_ecns_30_all; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_30_all ON summary_launch_exit_ecns_30 USING btree (date, time_stamp, ts_id, ecn_launch_id, ecn_exit_id);


--
-- Name: summary_launch_exit_ecns_30_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_30_date ON summary_launch_exit_ecns_30 USING btree (date);


--
-- Name: summary_launch_exit_ecns_30_date_ecn_exit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_30_date_ecn_exit_id ON summary_launch_exit_ecns_30 USING btree (date, ecn_exit_id);


--
-- Name: summary_launch_exit_ecns_30_date_ecn_launch_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_30_date_ecn_launch_id ON summary_launch_exit_ecns_30 USING btree (date, ecn_launch_id);


--
-- Name: summary_launch_exit_ecns_30_date_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_30_date_ts_id ON summary_launch_exit_ecns_30 USING btree (date, ts_id);


--
-- Name: summary_launch_exit_ecns_30_date_ts_id_ecn_exit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_30_date_ts_id_ecn_exit_id ON summary_launch_exit_ecns_30 USING btree (date, ts_id, ecn_exit_id);


--
-- Name: summary_launch_exit_ecns_30_date_ts_id_ecn_launch_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_30_date_ts_id_ecn_launch_id ON summary_launch_exit_ecns_30 USING btree (date, ts_id, ecn_launch_id);


--
-- Name: summary_launch_exit_ecns_30_date_ts_id_ecn_launch_id_ecn_exit; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_30_date_ts_id_ecn_launch_id_ecn_exit ON summary_launch_exit_ecns_30 USING btree (date, ts_id, ecn_launch_id, ecn_exit_id);


--
-- Name: summary_launch_exit_ecns_30_exit_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_30_exit_ecn_id ON summary_launch_exit_ecns_30 USING btree (ecn_exit_id);


--
-- Name: summary_launch_exit_ecns_30_launch_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_30_launch_ecn_id ON summary_launch_exit_ecns_30 USING btree (ecn_launch_id);


--
-- Name: summary_launch_exit_ecns_30_time_stamp; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_30_time_stamp ON summary_launch_exit_ecns_30 USING btree (time_stamp);


--
-- Name: summary_launch_exit_ecns_30_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_30_ts_id ON summary_launch_exit_ecns_30 USING btree (ts_id);


--
-- Name: summary_launch_exit_ecns_60_all; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_60_all ON summary_launch_exit_ecns_60 USING btree (date, time_stamp, ts_id, ecn_launch_id, ecn_exit_id);


--
-- Name: summary_launch_exit_ecns_60_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_60_date ON summary_launch_exit_ecns_60 USING btree (date);


--
-- Name: summary_launch_exit_ecns_60_date_ecn_exit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_60_date_ecn_exit_id ON summary_launch_exit_ecns_60 USING btree (date, ecn_exit_id);


--
-- Name: summary_launch_exit_ecns_60_date_ecn_launch_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_60_date_ecn_launch_id ON summary_launch_exit_ecns_60 USING btree (date, ecn_launch_id);


--
-- Name: summary_launch_exit_ecns_60_date_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_60_date_ts_id ON summary_launch_exit_ecns_60 USING btree (date, ts_id);


--
-- Name: summary_launch_exit_ecns_60_date_ts_id_ecn_exit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_60_date_ts_id_ecn_exit_id ON summary_launch_exit_ecns_60 USING btree (date, ts_id, ecn_exit_id);


--
-- Name: summary_launch_exit_ecns_60_date_ts_id_ecn_launch_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_60_date_ts_id_ecn_launch_id ON summary_launch_exit_ecns_60 USING btree (date, ts_id, ecn_launch_id);


--
-- Name: summary_launch_exit_ecns_60_date_ts_id_ecn_launch_id_ecn_exit; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_60_date_ts_id_ecn_launch_id_ecn_exit ON summary_launch_exit_ecns_60 USING btree (date, ts_id, ecn_launch_id, ecn_exit_id);


--
-- Name: summary_launch_exit_ecns_60_exit_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_60_exit_ecn_id ON summary_launch_exit_ecns_60 USING btree (ecn_exit_id);


--
-- Name: summary_launch_exit_ecns_60_launch_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_60_launch_ecn_id ON summary_launch_exit_ecns_60 USING btree (ecn_launch_id);


--
-- Name: summary_launch_exit_ecns_60_time_stamp; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_60_time_stamp ON summary_launch_exit_ecns_60 USING btree (time_stamp);


--
-- Name: summary_launch_exit_ecns_60_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_60_ts_id ON summary_launch_exit_ecns_60 USING btree (ts_id);


--
-- Name: summary_launch_exit_ecns_all; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_all ON summary_launch_exit_ecns USING btree (date, time_stamp, ts_id, ecn_launch_id, ecn_exit_id);


--
-- Name: summary_launch_exit_ecns_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_date ON summary_launch_exit_ecns USING btree (date);


--
-- Name: summary_launch_exit_ecns_date_ecn_exit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_date_ecn_exit_id ON summary_launch_exit_ecns USING btree (date, ecn_exit_id);


--
-- Name: summary_launch_exit_ecns_date_ecn_launch_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_date_ecn_launch_id ON summary_launch_exit_ecns USING btree (date, ecn_launch_id);


--
-- Name: summary_launch_exit_ecns_date_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_date_ts_id ON summary_launch_exit_ecns USING btree (date, ts_id);


--
-- Name: summary_launch_exit_ecns_date_ts_id_ecn_exit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_date_ts_id_ecn_exit_id ON summary_launch_exit_ecns USING btree (date, ts_id, ecn_exit_id);


--
-- Name: summary_launch_exit_ecns_date_ts_id_ecn_launch_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_date_ts_id_ecn_launch_id ON summary_launch_exit_ecns USING btree (date, ts_id, ecn_launch_id);


--
-- Name: summary_launch_exit_ecns_date_ts_id_ecn_launch_id_ecn_exit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_date_ts_id_ecn_launch_id_ecn_exit_id ON summary_launch_exit_ecns USING btree (date, ts_id, ecn_launch_id, ecn_exit_id);


--
-- Name: summary_launch_exit_ecns_exit_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_exit_ecn_id ON summary_launch_exit_ecns USING btree (ecn_exit_id);


--
-- Name: summary_launch_exit_ecns_launch_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_launch_ecn_id ON summary_launch_exit_ecns USING btree (ecn_launch_id);


--
-- Name: summary_launch_exit_ecns_time_stamp; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_time_stamp ON summary_launch_exit_ecns USING btree (time_stamp);


--
-- Name: summary_launch_exit_ecns_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_launch_exit_ecns_ts_id ON summary_launch_exit_ecns USING btree (ts_id);

COMMIT;