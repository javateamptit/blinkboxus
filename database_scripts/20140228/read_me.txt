* Download all scripts to upgrade:
- Please copy the folder "20140228" to the EAA database host
- Change into "20140228" directory (on the EAA database host)

* The update is needed to run with new release, all databases will upgrade (eaa10_201402 -> eaa10_201206, eaa10_twt2_201402 -> eaa10_twt2_201206 ):
1) To upgrade, please run the command:
	$chmod 744 upgrade_20140228.sh
	$./upgrade_20140228.sh
	
2) If we have "critical issues" of TT and need downgrade it, please run the command:
	$chmod 744 downgrade_20140228.sh
	$./downgrade_20140228.sh

