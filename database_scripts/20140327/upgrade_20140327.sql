BEGIN;

CREATE FUNCTION calculate_total_fee_in_trade_result() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
	totalFee double precision;
BEGIN
	SELECT SUM(total_fee) INTO totalFee
	FROM new_orders
	WHERE date = NEW.date AND ts_id = NEW.ts_id AND cross_id = NEW.ts_cid;
	
	IF (totalFee IS NOT NULL) THEN
		NEW.ts_total_fee := totalFee;
	END IF;
	
	RETURN NEW;
END $$;

ALTER FUNCTION public.calculate_total_fee_in_trade_result() OWNER TO postgres;


--
-- Name: str_update_rm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trg_update_total_fee BEFORE INSERT ON trade_results FOR EACH ROW EXECUTE PROCEDURE calculate_total_fee_in_trade_result();

COMMIT;
