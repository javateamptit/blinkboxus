* Download all scripts to upgrade:
- Please copy the folder "20140425" to the EAA database host
- Change into "20140425" directory (on the EAA database host)

* The update is needed to run with new release, all databases will upgrade (eaa10_201404 -> eaa10_201301):
1) To upgrade, please run the command:
	$chmod 744 upgrade_20140425.sh
	$./upgrade_20140425.sh
	
2) If we have "critical issues" of TT and need downgrade it, please run the command:
	$chmod 744 downgrade_20140425.sh
	$./downgrade_20140425.sh

