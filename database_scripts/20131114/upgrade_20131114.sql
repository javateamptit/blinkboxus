BEGIN;

--
-- Add new column
--
ALTER TABLE new_orders ADD COLUMN routing_inst character(4);
UPDATE new_orders SET routing_inst = substr(msg_content, position(E'\x01-76=' IN msg_content) + 5, 3) WHERE ecn='BATZ' AND msg_content like E'%\x01-76=RL2%';
UPDATE new_orders SET routing_inst = substr(msg_content, position(E'\x01-76=' IN msg_content) + 5, 1) WHERE ecn='BATZ' AND msg_content like E'%\x01-76=B%';

--
-- Name: vw_fill_delays; Type: VIEW; Schema: public; Owner: postgres
--
DROP VIEW vw_fill_delays;

CREATE VIEW vw_fill_delays AS
    SELECT f.fill_id, n.entry_exit, date_part('microseconds'::text, (f.time_stamp - a.time_stamp)) AS fill_delay, n.date, n.ecn, n.ecn_suffix, n.order_id, f.time_stamp, trim(n.routing_inst) as routing_inst 
		FROM new_orders n 
			JOIN order_acks a ON a.oid = n.oid 
			JOIN fills f ON f.oid = n.oid 
		WHERE (n.ts_id <> -1) AND (n.order_id <> 0) 
		ORDER BY f.fill_id, n.date;

ALTER TABLE public.vw_fill_delays OWNER TO postgres;

--
-- Name: vw_ack_delays; Type: VIEW; Schema: public; Owner: postgres
--
DROP VIEW vw_ack_delays;

CREATE VIEW vw_ack_delays AS
    SELECT n.oid, n.entry_exit, n.date, date_part('microseconds'::text, (a.time_stamp - n.time_stamp)) AS ack_delay, n.ecn, n.ecn_suffix, n.order_id, n.time_stamp, trim(n.routing_inst) as routing_inst 
		FROM new_orders n
			JOIN order_acks a ON a.oid = n.oid
		WHERE (n.ts_id <> -1) AND (n.order_id <> 0) 
		ORDER BY n.oid, n.date;


ALTER TABLE public.vw_ack_delays OWNER TO postgres;

CREATE INDEX idx_askbids_date_tsid_tscid ON ask_bids using btree(date, ts_id, ts_cid);

ALTER table trade_results ADD COLUMN spread integer;

CREATE OR REPLACE FUNCTION insert_spread_into_trade_results() RETURNS integer
    LANGUAGE plpgsql
    AS $$ 
DECLARE
	temp record;
BEGIN
	FOR temp in (SELECT tmp.date, tmp.ts_id, tmp.ts_cid, (sum(tmp.price) * 100::double precision)::integer AS spread FROM ( SELECT ask_bids.date, ask_bids.ts_id, ask_bids.ts_cid, ask_bids.ask_bid, CASE WHEN ask_bids.ask_bid = 'ASK'::bpchar THEN min(ask_bids.price) * (-1)::double precision ELSE max(ask_bids.price) END AS price FROM ask_bids GROUP BY ask_bids.date, ask_bids.ts_id, ask_bids.ts_cid, ask_bids.ask_bid) tmp GROUP BY tmp.date, tmp.ts_id, tmp.ts_cid) LOOP
		UPDATE trade_results SET spread = temp.spread WHERE date = temp.date and ts_id = temp.ts_id and ts_cid = temp.ts_cid;
		--INSERT INTO trade_results(spread) VALUES (temp.spread) WHERE date = temp.date and ts_id = temp.ts_id and ts_cid = temp.ts_cid
	END LOOP;

	return 1;
END
$$;

DROP VIEW vwtrade_results_twt2;
CREATE OR REPLACE VIEW vwtrade_results_twt2 AS
	SELECT t.expected_shares, t.expected_pl, t.bought_shares, t.total_bought, t.sold_shares, t.total_sold, t.left_stuck_shares, t.as_cid, t.ts_cid, t.ts_id, t.trade_id, t.symbol, t.date, t.real_value, t.real_value AS real_profit, n.ecn, n.ecn_launch, n.time_stamp, n.price, n.ack_price, n.status, n.entry_exit, n.left_shares, n.shares, n.is_iso, CASE WHEN t.sold_shares = t.bought_shares THEN 0::double precision WHEN LEAST(t.sold_shares, t.bought_shares) <> 0 THEN t.real_value - LEAST(t.sold_shares, t.bought_shares)::double precision * (t.total_sold / t.sold_shares::double precision - t.total_bought / t.bought_shares::double precision) ELSE t.real_value END AS pm_pl, t.spread FROM trade_results t JOIN new_orders n ON t.date = n.date AND t.ts_id = n.ts_id AND t.ts_cid = n.cross_id;
	
ALTER TABLE public.vwtrade_results_twt2 OWNER TO postgres;


-- Name: vwtrade_results; Type: VIEW; Schema: public; Owner: postgres


DROP VIEW vwtrade_results;
CREATE VIEW vwtrade_results AS
    SELECT t.expected_shares, t.expected_pl, t.bought_shares, t.total_bought, t.sold_shares, t.total_sold, t.left_stuck_shares, t.as_cid, t.ts_cid, t.ts_id, t.trade_id, t.symbol, t.date, t.real_value, cal_real_profit(t.total_sold, t.total_bought, t.sold_shares, t.bought_shares) AS real_profit, CASE WHEN t.sold_shares = t.bought_shares THEN 0::double precision WHEN LEAST(t.sold_shares, t.bought_shares) <> 0 THEN t.real_value - LEAST(t.sold_shares, t.bought_shares)::double precision * (t.total_sold / t.sold_shares::double precision - t.total_bought / t.bought_shares::double precision) ELSE t.real_value END AS pm_pl, t.spread FROM trade_results t;

ALTER TABLE public.vwtrade_results OWNER TO postgres;


-- Name: traded_orders; Type: VIEW; Schema: public; Owner: postgres

DROP VIEW traded_orders;
CREATE VIEW traded_orders AS
	SELECT DISTINCT new_orders.oid AS id, new_orders.date AS trade_date, new_orders.symbol, new_orders.account, new_orders.order_id AS seqid, new_orders.entry_exit, new_orders.side AS action, new_orders.time_stamp AS trade_time, new_orders.ecn, new_orders.shares AS qty, new_orders.price, new_orders.time_in_force AS tif, new_orders.left_shares AS lvs, new_orders.status, new_orders.ts_id AS ts, new_orders.is_iso, brokens.filing_status, new_orders.avg_price FROM new_orders LEFT JOIN brokens ON new_orders.date = brokens.trade_date AND new_orders.oid = brokens.oid ORDER BY new_orders.oid, new_orders.date, new_orders.symbol, new_orders.order_id, new_orders.entry_exit, new_orders.side, new_orders.time_stamp, new_orders.ecn, new_orders.shares, new_orders.price, new_orders.time_in_force, new_orders.left_shares, new_orders.status, new_orders.ts_id, new_orders.is_iso, brokens.filing_status, new_orders.account;

ALTER TABLE public.traded_orders OWNER TO postgres;

-- Function: cal_filter_trade_statistic_byecn2_twt2(date, date, time without time zone, time without time zone, integer, double precision, text, integer, integer, text, integer, text, text, integer, text)

DROP FUNCTION cal_filter_trade_statistic_byecn2_twt2(date, date, time without time zone, time without time zone, integer, double precision, text, integer, integer, text, integer, text, text, integer);

CREATE OR REPLACE FUNCTION cal_filter_trade_statistic_byecn2_twt2(to_date date, from_date date, time_from time without time zone, time_to time without time zone, share_size integer, expected_profit double precision, symb text, bt_shares integer, sd_shares integer, ecn_filter text, ts_id integer, cecn text, is_iso text, exclusive_val integer, cross_size text)
  RETURNS trade_result_summary2 AS
$BODY$declare
  trade_result record;
  summary trade_result_summary2;
  shares_filled integer;
  shares_stuck integer;
  profit_loss double precision;
 --statistic missed trade 
  count_ouch_missed_trade integer;
  sum_ouch_expected_pl double precision;
  missed_trade_record_of_ouch record;
  str_sql text;
  str_sql1 text;
  str_sql2 text;	
  str_sql_no text;
  str_sql_no_repriced text;
  str_sql_repriced text;
  str_sql_multi_orders text;
  str_sql_tmp text;
  tmp_str text;
  tmp_ASCid text;
  
begin	
  -- Initialize
  summary.count_miss := 0;
  summary.count_repriced := 0;
  summary.count_stuck_loss := 0;
  summary.count_stuck_profit := 0;
  summary.count_loss := 0;
  summary.count_profit_under := 0;
  summary.count_profit_above := 0;
  summary.count_total := 0;
  summary.expected_miss := 0.00;
  summary.expected_repriced := 0.00;
  summary.expected_stuck_loss := 0.00;
  summary.expected_stuck_profit := 0.00;
  summary.expected_loss := 0.00;
  summary.expected_profit_under := 0.00;
  summary.expected_profit_above := 0.00;
  summary.expected_total := 0.00;
  summary.real_miss := 0.00;
  summary.real_stuck_loss := 0.00;
  summary.real_stuck_profit := 0.00;
  summary.real_loss = 0.00;
  summary.real_profit_under := 0.00;
  summary.real_profit_above := 0.00;
  summary.real_total := 0.00;
  
  summary.pm_miss := 0.00;
  summary.pm_stuck_loss := 0.00;
  summary.pm_stuck_profit := 0.00;
  summary.pm_loss = 0.00;
  summary.pm_profit_under := 0.00;
  summary.pm_profit_above := 0.00;
  summary.pm_total := 0.00;

  str_sql :='';
  str_sql_no :='SELECT (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + cross_id) as tmpcol FROM new_orders WHERE date >='''|| to_date || ''' and date <='''|| from_date ||'''';
  str_sql_no :=str_sql_no || ' and time_stamp between ''' || time_from || ''' and '''|| time_to || '''';
	--filter by ts, ecn launch
	IF ts_id > -1 THEN
		str_sql_no:=str_sql_no || ' and ts_id =' || ts_id;
	END IF;
	IF cecn <> '0' THEN
		str_sql_no:=str_sql_no || ' and ecn_launch =''' || cecn ||'''';
	END IF;
	
	IF ecn_filter = 'arcaen' THEN
		str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Entry''' ;
	ELSIF ecn_filter = 'arcaex' THEN
		str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Exit''' ;
	ELSIF ecn_filter = 'ouchen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'ouchex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Exit''';
	ELSIF ecn_filter = 'rashen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'rashex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Exit''';		
	ELSIF ecn_filter = 'batzen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Entry''';	
	ELSIF ecn_filter = 'batzex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Exit''';	
	ELSIF ecn_filter = 'oubxen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''OUBX'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'oubxex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''OUBX'' and entry_exit=''Exit''';
	ELSIF ecn_filter = 'rabxen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''RABX'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'rabxex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''RABX'' and entry_exit=''Exit''';
	ELSIF ecn_filter = 'nyseen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'nyseex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Exit''';
	ELSIF ecn_filter = 'edgxen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''EDGX'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'edgxex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''EDGX'' and entry_exit=''Exit''';
	END IF;

	--Filter by ISO Odrer
	IF is_iso = '1' THEN
		str_sql_no:=str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
	END IF;
	-- tmp_str := str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
	
	IF is_iso = '2' THEN
		-- str_sql_no:=str_sql_no || ' and (is_iso <> ''Y'' and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + cross_id)) not in (' || tmp_str || '))';
		str_sql_no:=str_sql_no || ' and (is_iso <> ''Y'')';
	END IF;
	
	
	-- filter by Symbol, Bought Shares, Sold Shares
	IF symb <>'' THEN
		str_sql:=' and symbol='''|| symb ||'''';
	END IF;
	IF bt_shares > 0 THEN
		str_sql:=str_sql || ' and bought_shares >=' || bt_shares;
	END IF;
	IF sd_shares > 0 THEN
		str_sql:=str_sql || ' and sold_shares >=' || sd_shares;
	END IF;
	--filter by ts
	IF ts_id > -1 THEN 
		str_sql:=str_sql || ' and ts_id =' || ts_id;
	ELSIF ts_id = -2 THEN
		str_sql := str_sql || ' and bought_shares <> sold_shares ';
	END IF;
	IF cross_size <> '' then
		str_sql := str_sql || ' and ' || cross_size;
	END IF;
	IF ecn_filter <> '0' AND exclusive_val = 1 THEN
		str_sql_multi_orders := get_query_string_array_id_multi_orders(to_date, from_date);
		str_sql := str_sql || ' and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) not in (' || str_sql_multi_orders || ')';
	END IF;
	
	--Calculate repriced
 IF bt_shares = 0 AND sd_shares = 0 THEN   
	str_sql_no_repriced:=str_sql_no || ' and ecn =''OUCH'' and price != ack_price GROUP BY (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + cross_id)' ;
	str_sql_repriced:=str_sql || '  and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) in ('|| str_sql_no_repriced ||')';	
	str_sql1:='SELECT count(*) as count_missed, sum(expected_pl)as sum_expected_pl FROM vwtrade_results WHERE bought_shares = 0 and sold_shares = 0 and date >='''|| to_date || ''' and date <='''|| from_date ||''' and expected_shares >='|| share_size ||' and expected_pl >=' || expected_profit ;
   
   str_sql1:=str_sql1 ||str_sql_repriced;
    for missed_trade_record_of_ouch in EXECUTE str_sql1 loop
		count_ouch_missed_trade := missed_trade_record_of_ouch.count_missed;
		sum_ouch_expected_pl := missed_trade_record_of_ouch.sum_expected_pl;
    end loop;

   IF count_ouch_missed_trade > 0 THEN
	summary.count_repriced := count_ouch_missed_trade;
	summary.expected_repriced := sum_ouch_expected_pl;
	summary.count_miss := -count_ouch_missed_trade;
	summary.expected_miss :=  -sum_ouch_expected_pl;
   END IF;

 END IF;
 
 --calculate for trade_statistics table	
 str_sql_no:=str_sql_no || ' GROUP BY (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + cross_id)' ;
 str_sql:=str_sql || '  and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) in ('|| str_sql_no ||')';
 str_sql2:='SELECT * FROM vwtrade_results WHERE date >='''|| to_date || ''' and date <='''|| from_date ||''' and expected_shares >='|| share_size ||' and expected_pl >=' || expected_profit ;
 str_sql2:=str_sql2 ||str_sql;
FOR trade_result in EXECUTE str_sql2 loop
    shares_filled := trade_result.bought_shares;

    IF (shares_filled > trade_result.sold_shares) THEN
      shares_filled := trade_result.sold_shares;
    END IF;

    shares_stuck := trade_result.bought_shares - trade_result.sold_shares;    

    IF (shares_stuck = 0) THEN
      profit_loss := trade_result.total_sold - trade_result.total_bought;

      IF (shares_filled = 0) THEN
        summary.count_miss := summary.count_miss + 1;
        summary.expected_miss := summary.expected_miss + trade_result.expected_pl;
      ELSE
        IF (profit_loss < 0) THEN
          summary.count_loss := summary.count_loss + 1;
          summary.expected_loss := summary.expected_loss + trade_result.expected_pl;
          summary.real_loss := summary.real_loss + profit_loss;
        ELSE
          IF ( profit_loss::real < trade_result.expected_pl::real) THEN
            summary.count_profit_under := summary.count_profit_under + 1;
            summary.expected_profit_under := summary.expected_profit_under + trade_result.expected_pl;
            summary.real_profit_under := summary.real_profit_under + profit_loss;
          ELSE
            summary.count_profit_above := summary.count_profit_above + 1;
            summary.expected_profit_above := summary.expected_profit_above + trade_result.expected_pl;
            summary.real_profit_above := summary.real_profit_above + profit_loss;
          END IF;
        END IF;
      END IF;
    ELSE
      --Calculate for stuck
      IF trade_result.real_value < 0 THEN
	--stuck_loss
	   summary.count_stuck_loss := summary.count_stuck_loss + 1;
	   summary.expected_stuck_loss := summary.expected_stuck_loss + trade_result.expected_pl;
	   summary.real_stuck_loss := summary.real_stuck_loss + trade_result.real_value;
	   summary.pm_stuck_loss := summary.pm_stuck_loss + trade_result.pm_pl;
      ELSE
	--stuck_profit
	   summary.count_stuck_profit := summary.count_stuck_profit + 1;
	   summary.expected_stuck_profit := summary.expected_stuck_profit + trade_result.expected_pl;
	   summary.real_stuck_profit := summary.real_stuck_profit + trade_result.real_value;
	   summary.pm_stuck_profit := summary.pm_stuck_profit + trade_result.pm_pl;
      END IF;
    END IF;

    summary.count_total := summary.count_total +  1;
    summary.expected_total := summary.expected_total + trade_result.expected_pl  ;
  end loop;
  
  summary.count_total := summary.count_total - summary.count_repriced;
  summary.expected_total := summary.expected_total - summary.expected_repriced;
  
  return summary;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION cal_filter_trade_statistic_byecn2_twt2(date, date, time without time zone, time without time zone, integer, double precision, text, integer, integer, text, integer, text, text, integer, text)
  OWNER TO postgres;

CREATE OR REPLACE FUNCTION get_query_string_array_id_multi_orders(date_from date, date_to date)
  RETURNS text AS
$BODY$ 
declare
ret text;
BEGIN
 ret ='select distinct (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + cross_id) from (
          SELECT tmp.date, tmp.cross_id, tmp.ts_id, tmp.symbol, tmp.entry_exit,count(tmp.entry_exit) as count
          FROM (SELECT date, cross_id, ts_id, symbol, ecn, entry_exit, count(entry_exit) AS count_entryexit
               FROM new_orders
               WHERE new_orders.ts_id <> -1 and date between ''' || date_from || ''' and ''' || date_to ||'''
               GROUP BY cross_id, date, ts_id, symbol, ecn, entry_exit
               ORDER BY cross_id
            ) as tmp
          GROUP BY tmp.date, tmp.cross_id, tmp.ts_id, tmp.symbol, tmp.entry_exit
          HAVING count(tmp.entry_exit) > 1
          ORDER BY tmp.cross_id
        ) as tmp2';

  return ret;
END
$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION get_query_string_array_id_multi_orders(date, date)
  OWNER TO postgres;
  
-- Function: cal_filter_trade_statistic_byecn2_twt2_notime(date, date, integer, double precision, text, integer, integer, text, integer, text, text, integer, text)

CREATE OR REPLACE FUNCTION cal_filter_trade_statistic_byecn2_twt2_notime(to_date date, from_date date, share_size integer, expected_profit double precision, symb text, bt_shares integer, sd_shares integer, ecn_filter text, ts_id integer, cecn text, is_iso text, exclusive_val integer, cross_size text)
  RETURNS trade_result_summary2 AS
$BODY$ declare
  trade_result record;
  summary trade_result_summary2;
  shares_filled integer;
  shares_stuck integer;
  profit_loss double precision;
 --statistic missed trade 
  count_ouch_missed_trade integer;
  sum_ouch_expected_pl double precision;
  missed_trade_record_of_ouch record;
  str_sql text;
  str_sql_no text;
  str_sql_multi_orders text;
  str_sql_tmp text;
  tmp_str text;
  
begin	
  -- Initia
  summary.count_miss := 0;
  summary.count_repriced := 0;
  summary.count_stuck_loss := 0;
  summary.count_stuck_profit := 0;
  summary.count_loss := 0;
  summary.count_profit_under := 0;
  summary.count_profit_above := 0;
  summary.count_total := 0;
  summary.expected_miss := 0.00;
  summary.expected_repriced := 0.00;
  summary.expected_stuck_loss := 0.00;
  summary.expected_stuck_profit := 0.00;
  summary.expected_loss := 0.00;
  summary.expected_profit_under := 0.00;
  summary.expected_profit_above := 0.00;
  summary.expected_total := 0.00;
  summary.real_miss := 0.00;
  summary.real_stuck_loss := 0.00;
  summary.real_stuck_profit := 0.00;
  summary.real_loss = 0.00;
  summary.real_profit_under := 0.00;
  summary.real_profit_above := 0.00;
  summary.real_total := 0.00;
  
  summary.pm_miss := 0.00;
  summary.pm_stuck_loss := 0.00;
  summary.pm_stuck_profit := 0.00;
  summary.pm_loss = 0.00;
  summary.pm_profit_under := 0.00;
  summary.pm_profit_above := 0.00;
  summary.pm_total := 0.00;


	--Calculate repriced
 IF bt_shares = 0 AND sd_shares = 0 THEN
    --Select data from view "vwouch_missed_trades" and calculate total missed trade 	
	str_sql:='SELECT count(*) as count_missed, sum(expected_pl)as sum_expected_pl  FROM vwtrade_results WHERE date >='''|| to_date || ''' and date <='''|| from_date ||''' and expected_shares >='|| share_size ||' and expected_pl >=' || expected_profit ;
	str_sql := str_sql || ' and bought_shares < 1 and sold_shares < 1 ';
		--filter by ts when we get data from view vwtrade_results
		IF ts_id > -1 THEN
			str_sql:=str_sql || ' and ts_id =' || ts_id;
		ELSIF ts_id = -2 THEN
			str_sql := str_sql || ' and bought_shares <> sold_shares ';
		END IF;
	str_sql_no :='SELECT (ts_id * 1000000 + cross_id) as tmpcol FROM new_orders WHERE date >='''|| to_date || ''' and date <='''|| from_date ||'''';	
	str_sql_no :=str_sql_no || ' and ecn =''OUCH'' and price != ack_price ';
		--filter by ts, ecn_launch when we get data from table new_orders
		IF ts_id > -1 THEN
			str_sql_no:=str_sql_no || ' and ts_id =' || ts_id;
		END IF;
		IF cecn <> '0' THEN
			str_sql_no:=str_sql_no || ' and ecn_launch =''' || cecn ||'''';
		END IF;
	IF ecn_filter = 'arcaen' THEN
		str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Entry''' ;
	ELSIF ecn_filter = 'arcaex' THEN
		str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Exit''' ;
	ELSIF ecn_filter = 'ouchen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'ouchex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Exit''';
	ELSIF ecn_filter = 'rashen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'rashex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Exit''';		
	ELSIF ecn_filter = 'batzen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Entry''';	
	ELSIF ecn_filter = 'batzex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Exit''';	
	ELSIF ecn_filter = 'oubxen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''OUBX'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'oubxex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''OUBX'' and entry_exit=''Exit''';
	ELSIF ecn_filter = 'rabxen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''RABX'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'rabxex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''RABX'' and entry_exit=''Exit''';
	ELSIF ecn_filter = 'nyseen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'nyseex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Exit''';
	ELSIF ecn_filter = 'edgxen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''EDGX'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'edgxex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''EDGX'' and entry_exit=''Exit''';
	END IF;
	
	--Filter by ISO Odrer
	IF is_iso = '1' AND from_date > '2010-01-01' THEN
		str_sql_no:=str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
	END IF;	
	-- tmp_str := str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
	
	IF is_iso = '2' AND from_date > '2010-01-01' THEN
		-- str_sql_no:=str_sql_no || ' and (is_iso <> ''Y'' and (ts_id * 1000000 + cross_id) not in (' || tmp_str || '))';
		str_sql_no:=str_sql_no || ' and (is_iso <> ''Y'')';
	END IF;
	
	IF symb <>'' THEN
		str_sql:=str_sql || ' and symbol='''|| symb ||'''';
		str_sql_no:=str_sql_no || ' and symbol='''|| symb ||'''';
	END IF;
	
	if cross_size <> '' then
		str_sql := str_sql || ' and ' || cross_size;
	ELSE
		str_sql := str_sql || ' and bought_shares <> sold_shares ';
	end if;

	str_sql_no:=str_sql_no || ' GROUP BY (ts_id * 1000000 + cross_id)' ;

	IF bt_shares > 0 THEN
		str_sql:=str_sql || ' and bought_shares >=' || bt_shares;
	END IF;
	IF sd_shares > 0 THEN
		str_sql:=str_sql || ' and sold_shares >=' || sd_shares;
	END IF;

	str_sql:=str_sql || ' and (ts_id * 1000000 + ts_cid) in ('|| str_sql_no ||')';
	
	IF ecn_filter <> '0' AND exclusive_val = 1 THEN
		str_sql_multi_orders := get_query_string_array_id_multi_orders(to_date, from_date);
		str_sql_tmp := get_mutiple_exchanges_trade(to_date, from_date); 
		str_sql :=str_sql || ' and (ts_id * 1000000 + ts_cid) not in (' || str_sql_multi_orders || ')'|| ' and (ts_id * 1000000 + ts_cid) not in (' || str_sql_tmp || ')';
	END IF;
	
	for missed_trade_record_of_ouch in EXECUTE str_sql loop
		count_ouch_missed_trade := missed_trade_record_of_ouch.count_missed;
		sum_ouch_expected_pl := missed_trade_record_of_ouch.sum_expected_pl;
	end loop;

	   IF count_ouch_missed_trade > 0 THEN
		summary.count_repriced := count_ouch_missed_trade;
		summary.expected_repriced := sum_ouch_expected_pl;

		summary.count_miss := -count_ouch_missed_trade;
		summary.expected_miss :=  -sum_ouch_expected_pl;
	   END IF;

  END IF;

--filter by  ECN by Entry/Exit
  str_sql:='SELECT * FROM vwtrade_results WHERE date >='''|| to_date || ''' and date <='''|| from_date ||''' and expected_shares >='|| share_size ||' and expected_pl >=' || expected_profit ;
  str_sql_no :='SELECT (ts_id * 1000000 + cross_id) as tmpcol FROM new_orders WHERE date >='''|| to_date || ''' and date <='''|| from_date ||'''';  
	--filter by ts, ecn launch
	IF ts_id > -1 THEN
		str_sql_no:=str_sql_no || ' and ts_id =' || ts_id;
	END IF;
	IF cecn <> '0' THEN
		str_sql_no:=str_sql_no || ' and ecn_launch =''' || cecn ||'''';
	END IF;
	
	IF ecn_filter = 'arcaen' THEN
		str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Entry''' ;
	ELSIF ecn_filter = 'arcaex' THEN
		str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Exit''' ;
	ELSIF ecn_filter = 'ouchen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'ouchex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Exit''';
	ELSIF ecn_filter = 'rashen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'rashex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Exit''';		
	ELSIF ecn_filter = 'batzen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Entry''';	
	ELSIF ecn_filter = 'batzex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Exit''';	
	ELSIF ecn_filter = 'oubxen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''OUBX'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'oubxex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''OUBX'' and entry_exit=''Exit''';
	ELSIF ecn_filter = 'rabxen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''RABX'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'rabxex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''RABX'' and entry_exit=''Exit''';
	ELSIF ecn_filter = 'nyseen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'nyseex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Exit''';
	ELSIF ecn_filter = 'edgxen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''EDGX'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'edgxex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''EDGX'' and entry_exit=''Exit''';
	END IF;

	--Filter by ISO Odrer
	IF is_iso = '1' AND from_date > '2010-01-01' THEN
		str_sql_no:=str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
	END IF;	
	-- tmp_str := str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
	
	IF is_iso = '2' AND from_date > '2010-01-01' THEN
		-- str_sql_no:=str_sql_no || ' and (is_iso <> ''Y'' and (ts_id * 1000000 + cross_id) not in (' || tmp_str || '))';
		str_sql_no:=str_sql_no || ' and (is_iso <> ''Y'')';
	END IF;
	
	if cross_size <> '' then
		str_sql := str_sql || ' and ' || cross_size;
	end if;
	
	str_sql_no:=str_sql_no || ' GROUP BY (ts_id * 1000000 + cross_id)' ;
	
	-- filter by Symbol, Bought Shares, Sold Shares
	IF symb <>'' THEN
		str_sql:=str_sql || ' and symbol='''|| symb ||'''';
	END IF;
	IF bt_shares > 0 THEN
		str_sql:=str_sql || ' and bought_shares >=' || bt_shares;
	END IF;
	IF sd_shares > 0 THEN
		str_sql:=str_sql || ' and sold_shares >=' || sd_shares;
	END IF;
	--filter by ts
	IF ts_id > -1 THEN 
		str_sql:=str_sql || ' and ts_id =' || ts_id;
	ELSIF ts_id = -2 THEN
		str_sql := str_sql || ' and bought_shares <> sold_shares ';
	END IF;
	
	str_sql:=str_sql || ' and (ts_id * 1000000 + ts_cid) in ('|| str_sql_no ||')';
	
	IF ecn_filter <> '0' AND exclusive_val = 1 THEN
		str_sql_multi_orders := get_query_string_array_id_multi_orders(to_date, from_date);
		str_sql_tmp := get_mutiple_exchanges_trade(to_date, from_date); 
		str_sql :=str_sql || ' and (ts_id * 1000000 + ts_cid) not in (' || str_sql_multi_orders || ')'|| ' and (ts_id * 1000000 + ts_cid) not in (' || str_sql_tmp || ')';
	END IF;
	
--calculate for trade_statistics table	
FOR trade_result in EXECUTE str_sql loop
    shares_filled := trade_result.bought_shares;

    IF (shares_filled > trade_result.sold_shares) THEN
      shares_filled := trade_result.sold_shares;
    END IF;

    shares_stuck := trade_result.bought_shares - trade_result.sold_shares;    

    IF (shares_stuck = 0) THEN
      profit_loss := trade_result.total_sold - trade_result.total_bought;

      IF (shares_filled = 0) THEN
        summary.count_miss := summary.count_miss + 1;
        summary.expected_miss := summary.expected_miss + trade_result.expected_pl;
      ELSE
        IF (profit_loss < 0) THEN
          summary.count_loss := summary.count_loss + 1;
          summary.expected_loss := summary.expected_loss + trade_result.expected_pl;
          summary.real_loss := summary.real_loss + profit_loss;
        ELSE
          IF ( profit_loss::real < trade_result.expected_pl::real) THEN
            summary.count_profit_under := summary.count_profit_under + 1;
            summary.expected_profit_under := summary.expected_profit_under + trade_result.expected_pl;
            summary.real_profit_under := summary.real_profit_under + profit_loss;
          ELSE
            summary.count_profit_above := summary.count_profit_above + 1;
            summary.expected_profit_above := summary.expected_profit_above + trade_result.expected_pl;
            summary.real_profit_above := summary.real_profit_above + profit_loss;
          END IF;
        END IF;
      END IF;
    ELSE
      --Calculate for stuck
      IF trade_result.real_value < 0 THEN
	--stuck_loss
	   summary.count_stuck_loss := summary.count_stuck_loss + 1;
	   summary.expected_stuck_loss := summary.expected_stuck_loss + trade_result.expected_pl;
	   summary.real_stuck_loss := summary.real_stuck_loss + trade_result.real_value;
	   summary.pm_stuck_loss := summary.pm_stuck_loss + trade_result.pm_pl;
      ELSE
	--stuck_profit
	   summary.count_stuck_profit := summary.count_stuck_profit + 1;
	   summary.expected_stuck_profit := summary.expected_stuck_profit + trade_result.expected_pl;
	   summary.real_stuck_profit := summary.real_stuck_profit + trade_result.real_value;
	   summary.pm_stuck_profit := summary.pm_stuck_profit + trade_result.pm_pl;
      END IF;
    END IF;

    summary.count_total := summary.count_total +  1;
    summary.expected_total := summary.expected_total + trade_result.expected_pl  ;
  end loop;

  return summary;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION cal_filter_trade_statistic_byecn2_twt2_notime(date, date, integer, double precision, text, integer, integer, text, integer, text, text, integer, text)
  OWNER TO postgres;
  

--
-- Name: cal_filter_trade_statistic_byecn2_twt2_with_step(date, date, time without time zone, time without time zone, integer, double precision, text, integer, integer, text, integer, text, integer, text, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

-- DROP FUNCTION cal_filter_trade_statistic_byecn2_twt2_with_step(date, date, time without time zone, time without time zone, integer, double precision, text, integer, integer, text, integer, text, integer, text, integer);

CREATE OR REPLACE FUNCTION cal_filter_trade_statistic_byecn2_twt2_with_step(to_date date, from_date date, time_from time without time zone, time_to time without time zone, share_size integer, expected_profit double precision, symb text, bt_shares integer, sd_shares integer, ecn_filter text, ts_id integer, cecn text, step integer, is_iso text, exclusive_val integer, cross_size text) RETURNS SETOF trade_result_summary2_struct
    LANGUAGE plpgsql
    AS $$ 
declare
	trade_result 				record;
	summary 				trade_result_summary2_struct;
	shares_filled 				integer;
	shares_stuck 				integer;
	profit_loss 				double precision;
	--statistic missed trade 
	count_ouch_missed_trade 		integer;
	sum_ouch_expected_pl 			double precision;
	missed_trade_record_of_ouch 		record;
	str_sql 				text;
	str_sql_no 				text;
	str_sql_tmp				text;
	str_sql_multi_orders text;
	tmp text;
	tmp_str text;
	min_time				"time";
	max_time				"time";
	temp_time 				"time";
	t_current				"time";
  
begin	
	
	min_time := time_from;
	max_time := time_to;
	WHILE min_time < max_time LOOP
		-- Initia
		
		summary.count_miss := 0;
		summary.count_repriced := 0;
		summary.count_stuck_loss := 0;
		summary.count_stuck_profit := 0;
		summary.count_loss := 0;
		summary.count_profit_under := 0;
		summary.count_profit_above := 0;		
		
		IF (step = 5) THEN
			t_current = min_time + interval '5 minutes';
		ELSIF (step = 15) THEN
			t_current = min_time + interval '15 minutes';
			
		ELSIF (step = 30) THEN
			t_current = min_time + interval '30 minutes';
		ELSIF (step = 60) THEN
			t_current = min_time + interval '60 minutes';
		END IF;
		--raise notice '1(%)', t_current;
		IF ((t_current > max_time) or (t_current = '00:00:00')) THEN
			t_current = max_time;
			--raise notice '2(%)', t_current;
		END IF; 
		temp_time = t_current - interval '1 second';
		
		--Calculate repriced
		IF bt_shares = 0 AND sd_shares = 0 THEN
		
			--Select data from view "vwouch_missed_trades" and calculate total missed trade 	
			str_sql:='SELECT count(*) as count_missed, sum(expected_pl)as sum_expected_pl  FROM trade_results WHERE date >='''|| to_date || ''' and date <='''|| from_date ||''' and expected_shares >='|| share_size ||' and expected_pl >=' || expected_profit ;
			str_sql := str_sql || ' and bought_shares = 0 and sold_shares = 0 ';
			
			--filter by ts when we get data from view vwtrade_results
			IF ts_id > -1 THEN
				str_sql:= str_sql || ' and ts_id =' || ts_id;
			ELSIF ts_id = -2 THEN
				str_sql := str_sql || ' and bought_shares <> sold_shares ';
			END IF;
			
			IF cross_size <> '' then
				str_sql := str_sql || ' and ' || cross_size;
			END IF;
			
			str_sql_no :='SELECT (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + cross_id) as tmpcol FROM new_orders WHERE date >='''|| to_date || ''' and date <='''|| from_date ||'''';
			str_sql_no :=str_sql_no || ' and time_stamp between ''' || min_time || ''' and '''|| temp_time || '''';
			str_sql_no :=str_sql_no || ' and ecn =''OUCH'' and price != ack_price ';
			
			--filter by ts, ecn_launch when we get data from table new_orders
			IF ts_id > -1 THEN
				str_sql_no:=str_sql_no || ' and ts_id =' || ts_id;
			END IF;
			IF cecn <> '0' THEN
				str_sql_no:=str_sql_no || ' and ecn_launch =''' || cecn ||'''';
			END IF;
			
			IF ecn_filter = 'arcaen' THEN
				str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Entry''' ;
			ELSIF ecn_filter = 'arcaex' THEN
				str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Exit''' ;
			ELSIF ecn_filter = 'ouchen' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Entry''';
			ELSIF ecn_filter = 'ouchex' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Exit''';
			ELSIF ecn_filter = 'rashen' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Entry''';
			ELSIF ecn_filter = 'rashex' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Exit''';		
			ELSIF ecn_filter = 'batzen' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Entry''';	
			ELSIF ecn_filter = 'batzex' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Exit''';	
			ELSIF ecn_filter = 'oubxen' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''OUBX'' and entry_exit=''Entry''';
			ELSIF ecn_filter = 'oubxex' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''OUBX'' and entry_exit=''Exit''';
			ELSIF ecn_filter = 'rabxen' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''RABX'' and entry_exit=''Entry''';
			ELSIF ecn_filter = 'rabxex' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''RABX'' and entry_exit=''Exit''';
			ELSIF ecn_filter = 'nyseen' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Entry''';
			ELSIF ecn_filter = 'nyseex' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Exit''';
			ELSIF ecn_filter = 'edgxen' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''EDGX'' and entry_exit=''Entry''';
			ELSIF ecn_filter = 'edgxex' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''EDGX'' and entry_exit=''Exit''';
			END IF;

			IF symb <>'' THEN
				str_sql:=str_sql || ' and symbol='''|| symb ||'''';
				str_sql_no:=str_sql_no || ' and symbol='''|| symb ||'''';
			END IF;
			--Filter by ISO Odrer
			IF is_iso = '1' AND from_date > '2010-01-01' THEN
				str_sql_no:=str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
			END IF;
			-- tmp_str := str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
	
			IF is_iso = '2' AND from_date > '2010-01-01' THEN
				-- str_sql_no:=str_sql_no || ' and (is_iso <> ''Y'' and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + cross_id) not in (' || tmp_str || '))';
				str_sql_no:=str_sql_no || ' and (is_iso <> ''Y'')';
			END IF;
			
			str_sql_no:=str_sql_no || ' GROUP BY (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + cross_id)' ;

			IF bt_shares > 0 THEN
				str_sql:=str_sql || ' and bought_shares >=' || bt_shares;
			END IF;
			IF sd_shares > 0 THEN
				str_sql:=str_sql || ' and sold_shares >=' || sd_shares;
			END IF;

			str_sql:=str_sql || ' and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) in ('|| str_sql_no ||')';
			
			IF ecn_filter <> '0' AND exclusive_val = 1 THEN
				str_sql_multi_orders := get_query_string_array_id_multi_orders(to_date, from_date);
				str_sql_tmp := get_mutiple_exchanges_trade(to_date, from_date); 
				str_sql :=str_sql || ' and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) not in (' || str_sql_multi_orders || ')'|| ' and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) not in (' || str_sql_tmp || ')';
			END IF;
			
			for missed_trade_record_of_ouch in EXECUTE str_sql loop
				count_ouch_missed_trade := missed_trade_record_of_ouch.count_missed;
				sum_ouch_expected_pl := missed_trade_record_of_ouch.sum_expected_pl;
			end loop;

			IF count_ouch_missed_trade > 0 THEN
				summary.count_repriced := count_ouch_missed_trade;				
				summary.count_miss := -count_ouch_missed_trade;				
			END IF;

		END IF;

		--filter by  ECN by Entry/Exit
		str_sql:='SELECT * FROM trade_results WHERE date >='''|| to_date || ''' and date <='''|| from_date ||''' and expected_shares >='|| share_size ||' and expected_pl >=' || expected_profit ;
		str_sql_no :='SELECT (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + cross_id) as tmpcol FROM new_orders WHERE date >='''|| to_date || ''' and date <='''|| from_date ||'''';
		str_sql_no :=str_sql_no || ' and time_stamp between ''' || min_time || ''' and '''|| temp_time || '''';
		
		--filter by ts, ecn launch
		IF ts_id > -1 THEN
			str_sql_no:=str_sql_no || ' and ts_id =' || ts_id;
		END IF;
		
		IF cecn <> '0' THEN
			str_sql_no:=str_sql_no || ' and ecn_launch =''' || cecn ||'''';
		END IF;
		
		IF ecn_filter = 'arcaen' THEN
			str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Entry''' ;
		ELSIF ecn_filter = 'arcaex' THEN
			str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Exit''' ;
		ELSIF ecn_filter = 'ouchen' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Entry''';
		ELSIF ecn_filter = 'ouchex' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Exit''';
		ELSIF ecn_filter = 'rashen' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Entry''';
		ELSIF ecn_filter = 'rashex' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Exit''';		
		ELSIF ecn_filter = 'batzen' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Entry''';	
		ELSIF ecn_filter = 'batzex' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Exit''';	
		ELSIF ecn_filter = 'nyseen' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Entry''';
		ELSIF ecn_filter = 'nyseex' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Exit''';
		ELSIF ecn_filter = 'edgxen' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''EDGX'' and entry_exit=''Entry''';
		ELSIF ecn_filter = 'edgxex' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''EDGX'' and entry_exit=''Exit''';
		END IF;
		
		--Filter by ISO Odrer
		IF is_iso = '1' AND from_date > '2010-01-01' THEN
			str_sql_no:=str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
		END IF;
		-- tmp_str := str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
	
		IF is_iso = '2' AND from_date > '2010-01-01' THEN
			-- str_sql_no:=str_sql_no || ' and (is_iso <> ''Y'' and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + cross_id) not in (' || tmp_str || '))';
			str_sql_no:=str_sql_no || ' and (is_iso <> ''Y'')';
		END IF;
		
		str_sql_no:=str_sql_no || ' GROUP BY (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + cross_id)' ;

		-- filter by Symbol, Bought Shares, Sold Shares
		IF symb <>'' THEN
			str_sql:=str_sql || ' and symbol='''|| symb ||'''';
		END IF;
		IF bt_shares > 0 THEN
			str_sql:=str_sql || ' and bought_shares >=' || bt_shares;
		END IF;
		IF sd_shares > 0 THEN
			str_sql:=str_sql || ' and sold_shares >=' || sd_shares;
		END IF;
		--filter by ts
		IF ts_id > -1 THEN 
			str_sql := str_sql || ' and ts_id =' || ts_id;
		ELSIF ts_id = -2 THEN
			str_sql := str_sql || ' and bought_shares <> sold_shares ';
		END IF;
		
		IF cross_size <> '' then
			str_sql := str_sql || ' and ' || cross_size;
		END IF;
		
		str_sql:=str_sql || ' and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) in ('|| str_sql_no ||')';
		
		IF ecn_filter <> '0' AND exclusive_val = 1 THEN
			str_sql_multi_orders := get_query_string_array_id_multi_orders(to_date, from_date);
			str_sql_tmp := get_mutiple_exchanges_trade(to_date, from_date); 
			str_sql :=str_sql || ' and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) not in (' || str_sql_multi_orders || ')'|| ' and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) not in (' || str_sql_tmp || ')';
		END IF;
		
		--calculate for trade_statistics table	
		FOR trade_result in EXECUTE str_sql loop
			shares_filled := trade_result.bought_shares;
			--RAISE NOTICE 'Variable an_integer was changed.';			
			IF (shares_filled > trade_result.sold_shares) THEN
			  shares_filled := trade_result.sold_shares;
			END IF;

			shares_stuck := trade_result.bought_shares - trade_result.sold_shares;    

			IF (shares_stuck = 0) THEN
				profit_loss := trade_result.total_sold - trade_result.total_bought;

				IF (shares_filled = 0) THEN
				summary.count_miss := summary.count_miss + 1;				
				ELSE
					IF (profit_loss < 0) THEN
						summary.count_loss := summary.count_loss + 1;						
					ELSE
						IF ( profit_loss::real < trade_result.expected_pl::real) THEN
							summary.count_profit_under := summary.count_profit_under + 1;						
						ELSE
							summary.count_profit_above := summary.count_profit_above + 1;							
						END IF;
					END IF;
				END IF;
			ELSE
				--Calculate for stuck
				IF trade_result.real_value < 0 THEN
					--stuck_loss
					summary.count_stuck_loss := summary.count_stuck_loss + 1;					
				ELSE
					--stuck_profit
					summary.count_stuck_profit := summary.count_stuck_profit + 1;				
				END IF;
			END IF;

		END LOOP;

		
		summary.time_stamp = t_current;
		min_time := t_current;	
	
		RETURN NEXT summary;
		
	END LOOP;
	RETURN;
end
$$;


ALTER FUNCTION public.cal_filter_trade_statistic_byecn2_twt2_with_step(to_date date, from_date date, time_from time without time zone, time_to time without time zone, share_size integer, expected_profit double precision, symb text, bt_shares integer, sd_shares integer, ecn_filter text, ts_id integer, cecn text, step integer, is_iso text, exclusive_val integer, cross_size text) OWNER TO postgres;

COMMIT;

select insert_spread_into_trade_results();
