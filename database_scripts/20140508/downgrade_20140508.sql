BEGIN;

-- Downgrade TT version
DELETE FROM release_versions WHERE module_id = 2 AND version = '20140508';

-- DROP MDC/OEC settings from trade_servers relation
ALTER TABLE trade_servers DROP COLUMN IF EXISTS book_edga;
ALTER TABLE trade_servers DROP COLUMN IF EXISTS edga_direct;
ALTER TABLE trade_servers DROP COLUMN IF EXISTS book_nasdaq_bx;
ALTER TABLE trade_servers DROP COLUMN IF EXISTS nasdaq_ouch_bx;

--
-- Name: _insert_tt_activities(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE OR REPLACE FUNCTION _insert_tt_activities() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    DECLARE
		userVal                 text;
		typeVal                 integer;
		dateVal                 date;
		timeVal                 time;
		currentState    text[];
		_currentState   integer;
		status                  text[];
		_status                 integer;
		action                  text[];
		_action                 integer;
		orderId                 text;
		actionType              integer;
		symbol                  text;
		buyingPower     double precision;
		onWhich                 text[];
		_onWhich                integer;
		descriptionVal  text;
		_tsId			integer;
		_tsDescription  text;
		_maxShares		integer;
		_maxConsectutiveLoss integer;
		_maxStuck		integer;
		_count			integer;
		_activityAction text;
		_minSpreadTS	text;
		_maxLoserTS		text;
		_maxBuyingPower text;
		_checkUserOrServer integer;
		_maxCross       integer;
		_asVolatileThreshold text;
		_tsVolatileThreshold text;
		_asDisabledThreshold text;
		_tsDisabledThreshold text;
	
	BEGIN
		timeVal := NEW.time_stamp;
		userVal := initcap(trim(NEW.username));
		typeVal = NEW.activity_type;
		dateVal = (NEW.date);
		timeVal = (NEW.time_stamp);

		--Init status
		status[0] := 'Off-Line';
		status[1] := 'Active';
		status[2] := 'Active Assistant';
		status[3] := 'Monitoring';

		currentState[0] := '[Off-Line]';
		currentState[1] := '[Active]';
		currentState[2] := '[Active Assistant]';
		currentState[3] := '[Monitoring]';

		--Init action
		action[0] := 'Add';
		action[1] := 'Remove';

		--Init enable/disable trading
		onWhich[100] := 'all trading on TS1';
		onWhich[101] := 'ARCA trading on TS1';
		onWhich[102] := 'OUCH trading on TS1';
		onWhich[103] := 'NYSE trading on TS1';
		onWhich[104] := 'RASH trading on TS1';
		onWhich[105] := 'BATZ trading on TS1';
		onWhich[106] := 'EDGX trading on TS1';

		onWhich[200] := 'all trading on TS2';
		onWhich[201] := 'ARCA trading on TS2';
		onWhich[202] := 'OUCH trading on TS2';
		onWhich[203] := 'NYSE trading on TS2';
		onWhich[204] := 'RASH trading on TS2';
		onWhich[205] := 'BATZ trading on TS2';
		onWhich[206] := 'EDGX trading on TS2';

		onWhich[300] := 'all trading on TS3';
		onWhich[301] := 'ARCA trading on TS3';
		onWhich[302] := 'OUCH trading on TS3';
		onWhich[303] := 'NYSE trading on TS3';
		onWhich[304] := 'RASH trading on TS3';
		onWhich[305] := 'BATZ trading on TS3';
		onWhich[306] := 'EDGX trading on TS3';

		onWhich[400] := 'all trading on TS4';
		onWhich[401] := 'ARCA trading on TS4';
		onWhich[402] := 'OUCH trading on TS4';
		onWhich[403] := 'NYSE trading on TS4';
		onWhich[404] := 'RASH trading on TS4';
		onWhich[405] := 'BATZ trading on TS4';
		onWhich[406] := 'EDGX trading on TS4';

		onWhich[500] := 'all trading on TS5';
		onWhich[501] := 'ARCA trading on TS5';
		onWhich[502] := 'OUCH trading on TS5';
		onWhich[503] := 'NYSE trading on TS5';
		onWhich[504] := 'RASH trading on TS5';
		onWhich[505] := 'BATZ trading on TS5';
		onWhich[506] := 'EDGX trading on TS5';

		onWhich[600] := 'all trading on TS6';
		onWhich[601] := 'ARCA trading on TS6';
		onWhich[602] := 'OUCH trading on TS6';
		onWhich[603] := 'NYSE trading on TS6';
		onWhich[604] := 'RASH trading on TS6';
		onWhich[605] := 'BATZ trading on TS6';
		onWhich[606] := 'EDGX trading on TS6';
		
		onWhich[700] := 'all trading on TS7';
		onWhich[701] := 'ARCA trading on TS7';
		onWhich[702] := 'OUCH trading on TS7';
		onWhich[703] := 'NYSE trading on TS7';
		onWhich[704] := 'RASH trading on TS7';
		onWhich[705] := 'BATZ trading on TS7';
		onWhich[706] := 'EDGX trading on TS7';
		
		onWhich[800] := 'all trading on TS8';
		onWhich[801] := 'ARCA trading on TS8';
		onWhich[802] := 'OUCH trading on TS8';
		onWhich[803] := 'NYSE trading on TS8';
		onWhich[804] := 'RASH trading on TS8';
		onWhich[805] := 'BATZ trading on TS8';
		onWhich[806] := 'EDGX trading on TS8';

		onWhich[900] := 'all trading on TS9';
		onWhich[901] := 'ARCA trading on TS9';
		onWhich[902] := 'OUCH trading on TS9';
		onWhich[903] := 'NYSE trading on TS9';
		onWhich[904] := 'RASH trading on TS9';
		onWhich[905] := 'BATZ trading on TS9';
		onWhich[906] := 'EDGX trading on TS9';

		onWhich[99] := 'PM trading';

		--Init connect/disconnect all books and orders, enable/disable trading all orders on all TS
		onWhich[0] := 'trading all orders on all TSs and PM';
		onWhich[1] := 'all books and orders on all TSs and PM';

		--Init connect/disconnect BBO
		onWhich[110] := 'BBO on TS1';
		onWhich[111] := 'CQS on TS1';
		onWhich[112] := 'UQDF on TS1';

		onWhich[210] := 'BBO on TS2';
		onWhich[211] := 'CQS on TS2';
		onWhich[212] := 'UQDF on TS2';

		onWhich[310] := 'BBO on TS3';
		onWhich[311] := 'CQS on TS3';
		onWhich[312] := 'UQDF on TS3';

		onWhich[410] := 'BBO on TS4';
		onWhich[411] := 'CQS on TS4';
		onWhich[412] := 'UQDF on TS4';

		onWhich[510] := 'BBO on TS5';
		onWhich[511] := 'CQS on TS5';
		onWhich[512] := 'UQDF on TS5';

		onWhich[610] := 'BBO on TS6';
		onWhich[611] := 'CQS on TS6';
		onWhich[612] := 'UQDF on TS6';
		
		onWhich[710] := 'BBO on TS7';
		onWhich[711] := 'CQS on TS7';
		onWhich[712] := 'UQDF on TS7';
		
		onWhich[810] := 'BBO on TS8';
		onWhich[811] := 'CQS on TS8';
		onWhich[812] := 'UQDF on TS8';
		
		onWhich[910] := 'BBO on TS9';
		onWhich[911] := 'CQS on TS9';
		onWhich[912] := 'UQDF on TS9';

		onWhich[80] := 'BBO on PM';
		onWhich[81] := 'CTS on PM';
		onWhich[82] := 'UTDF on PM';
		onWhich[83] := 'CQS on PM';
		onWhich[84] := 'UQDF on PM';

		--Init connect/disconnect book
		onWhich[120] := 'all books on TS1';
		onWhich[121] := 'ARCA book on TS1';
		onWhich[122] := 'NDAQ book on TS1';
		onWhich[123] := 'NYSE book on TS1';
		onWhich[124] := 'BATS book on TS1';
		onWhich[125] := 'EDGX book on TS1';

		onWhich[220] := 'all books on TS2';
		onWhich[221] := 'ARCA book on TS2';
		onWhich[222] := 'NDAQ book on TS2';
		onWhich[223] := 'NYSE book on TS2';
		onWhich[224] := 'BATS book on TS2';
		onWhich[225] := 'EDGX book on TS2';

		onWhich[320] := 'all books on TS3';
		onWhich[321] := 'ARCA book on TS3';
		onWhich[322] := 'NDAQ book on TS3';
		onWhich[323] := 'NYSE book on TS3';
		onWhich[324] := 'BATS book on TS3';
		onWhich[325] := 'EDGX book on TS3';

		onWhich[420] := 'all books on TS4';
		onWhich[421] := 'ARCA book on TS4';
		onWhich[422] := 'NDAQ book on TS4';
		onWhich[423] := 'NYSE book on TS4';
		onWhich[424] := 'BATS book on TS4';
		onWhich[425] := 'EDGX book on TS4';

		onWhich[520] := 'all books on TS5';
		onWhich[521] := 'ARCA book on TS5';
		onWhich[522] := 'NDAQ book on TS5';
		onWhich[523] := 'NYSE book on TS5';
		onWhich[524] := 'BATS book on TS5';
		onWhich[525] := 'EDGX book on TS5';

		onWhich[620] := 'all books on TS6';
		onWhich[621] := 'ARCA book on TS6';
		onWhich[622] := 'NDAQ book on TS6';
		onWhich[623] := 'NYSE book on TS6';
		onWhich[624] := 'BATS book on TS6';
		onWhich[625] := 'EDGX book on TS6';
		
		onWhich[720] := 'all books on TS7';
		onWhich[721] := 'ARCA book on TS7';
		onWhich[722] := 'NDAQ book on TS7';
		onWhich[723] := 'NYSE book on TS7';
		onWhich[724] := 'BATS book on TS7';
		onWhich[725] := 'EDGX book on TS7';
		
		onWhich[820] := 'all books on TS8';
		onWhich[821] := 'ARCA book on TS8';
		onWhich[822] := 'NDAQ book on TS8';
		onWhich[823] := 'NYSE book on TS8';
		onWhich[824] := 'BATS book on TS8';
		onWhich[825] := 'EDGX book on TS8';
		
		onWhich[920] := 'all books on TS9';
		onWhich[921] := 'ARCA book on TS9';
		onWhich[922] := 'NDAQ book on TS9';
		onWhich[923] := 'NYSE book on TS9';
		onWhich[924] := 'BATS book on TS9';
		onWhich[925] := 'EDGX book on TS9';

		onWhich[750] := 'all books on PM';
		onWhich[751] := 'ARCA book on PM';
		onWhich[752] := 'NDAQ book on PM';

		--Init connect/disconnect order
		onWhich[130] := 'all orders on TS1';
		onWhich[131] := 'ARCA DIRECT on TS1';
		onWhich[132] := 'NDAQ OUCH on TS1';
		onWhich[133] := 'NYSE CCG on TS1';
		onWhich[134] := 'NDAQ RASH on TS1';
		onWhich[135] := 'BATZ BOE on TS1';
		onWhich[136] := 'EDGX on TS1';

		onWhich[230] := 'all orders on TS2';
		onWhich[231] := 'ARCA DIRECT on TS2';
		onWhich[232] := 'NDAQ OUCH on TS2';
		onWhich[233] := 'NYSE CCG on TS2';
		onWhich[234] := 'NDAQ RASH on TS2';
		onWhich[235] := 'BATZ BOE on TS2';
		onWhich[236] := 'EDGX on TS2';

		onWhich[330] := 'all orders on TS3';
		onWhich[331] := 'ARCA DIRECT on TS3';
		onWhich[332] := 'NDAQ OUCH on TS3';
		onWhich[333] := 'NYSE CCG on TS3';
		onWhich[334] := 'NDAQ RASH on TS3';
		onWhich[335] := 'BATZ BOE on TS3';
		onWhich[336] := 'EDGX on TS3';

		onWhich[430] := 'all orders on TS4';
		onWhich[431] := 'ARCA DIRECT on TS4';
		onWhich[432] := 'NDAQ OUCH on TS4';
		onWhich[433] := 'NYSE CCG on TS4';
		onWhich[434] := 'NDAQ RASH on TS4';
		onWhich[435] := 'BATZ BOE on TS4';
		onWhich[436] := 'EDGX on TS4';

		onWhich[530] := 'all orders on TS5';
		onWhich[531] := 'ARCA DIRECT on TS5';
		onWhich[532] := 'NDAQ OUCH on TS5';
		onWhich[533] := 'NYSE CCG on TS5';
		onWhich[534] := 'NDAQ RASH on TS5';
		onWhich[535] := 'BATZ BOE on TS5';
		onWhich[536] := 'EDGX on TS5';

		onWhich[630] := 'all orders on TS6';
		onWhich[631] := 'ARCA DIRECT on TS6';
		onWhich[632] := 'NDAQ OUCH on TS6';
		onWhich[633] := 'NYSE CCG on TS6';
		onWhich[634] := 'NDAQ RASH on TS6';
		onWhich[635] := 'BATZ BOE on TS6';
		onWhich[636] := 'EDGX on TS6';
		
		onWhich[730] := 'all orders on TS7';
		onWhich[731] := 'ARCA DIRECT on TS7';
		onWhich[732] := 'NDAQ OUCH on TS7';
		onWhich[733] := 'NYSE CCG on TS7';
		onWhich[734] := 'NDAQ RASH on TS7';
		onWhich[735] := 'BATZ BOE on TS7';
		onWhich[736] := 'EDGX on TS7';
		
		onWhich[830] := 'all orders on TS8';
		onWhich[831] := 'ARCA DIRECT on TS8';
		onWhich[832] := 'NDAQ OUCH on TS8';
		onWhich[833] := 'NYSE CCG on TS8';
		onWhich[834] := 'NDAQ RASH on TS8';
		onWhich[835] := 'BATZ BOE on TS8';
		onWhich[836] := 'EDGX on TS8';
		
		onWhich[930] := 'all orders on TS9';
		onWhich[931] := 'ARCA DIRECT on TS9';
		onWhich[932] := 'NDAQ OUCH on TS9';
		onWhich[933] := 'NYSE CCG on TS9';
		onWhich[934] := 'NDAQ RASH on TS9';
		onWhich[935] := 'BATZ BOE on TS9';
		onWhich[936] := 'EDGX on TS9';

		onWhich[10] := 'all orders on AS';
		onWhich[11] := 'ARCA FIX on AS';
		onWhich[12] := 'NDAQ RASH on AS';
		
		onWhich[50] := 'all orders on PM';
		onWhich[51] := 'ARCA FIX on PM';
		onWhich[52] := 'NDAQ RASH on PM';

		-- Connect to AS
		IF (typeVal = 1) THEN
			SELECT substring(NEW.log_detail from 1 for 1)::integer INTO _currentState FROM tt_event_log;
			descriptionVal := userVal || ' ' || currentState[_currentState] || ' Connect to AS ' || dateVal || ' ' || timeVal;

		-- Switch trader role
		ELSIF (typeVal = 2) THEN
			SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer INTO _currentState, _status FROM tt_event_log;
			descriptionVal := userVal || ' ' || currentState[_currentState] || ' Client became ' || status[_status] || ' Trader ' || dateVal || ' ' || timeVal;

		-- Disconnect from AS
		ELSIF (typeVal = 3) THEN
			SELECT substring(NEW.log_detail from 1 for 1)::integer INTO _currentState FROM tt_event_log;
			descriptionVal := userVal || ' ' || currentState[_currentState] || ' Disconnect from AS ' || dateVal || ' ' || timeVal;

		-- Sent Manual order
		ELSIF (typeVal = 4) THEN
			SELECT substring(NEW.log_detail from 1 for 1)::integer, trim(substring(NEW.log_detail from 3 for 3)) INTO _currentState, orderId FROM tt_event_log;
			descriptionVal := userVal || ' ' || currentState[_currentState] || ' Sent Manual Order, OrderID = ' || orderId || ' ' || dateVal || ' ' || timeVal;

		-- Ignored stock list for TS
		ELSIF (typeVal = 5) THEN
			SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, trim(substring(NEW.log_detail from 5 for 8)) INTO _currentState, _action, symbol FROM tt_event_log;
			descriptionVal := userVal || ' ' || currentState[_currentState] || ' ' || action[_action] || ' ignored stock list for TS, symbol = ' || symbol || ' ' || dateVal || ' ' || timeVal;

		-- Halted stock list
		ELSIF (typeVal = 6) THEN
			SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, trim(substring(NEW.log_detail from 5 for 8)) INTO _currentState, _action, symbol FROM tt_event_log;
			descriptionVal := userVal || ' ' || currentState[_currentState] || ' ' || action[_action] || ' halted stock list, symbol = ' || symbol || ' ' || dateVal || ' ' || timeVal;

		-- PM engage/disengage
		ELSIF (typeVal = 7) THEN
			SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, trim(substring(NEW.log_detail from 5 for 8)) INTO _currentState, actionType, symbol FROM tt_event_log;
			-- engage
			IF (actionType = 0) THEN
				descriptionVal := userVal || ' ' || currentState[_currentState] || ' PM engage, symbol = ' || symbol || ' ' || dateVal || ' ' || timeVal;
			-- disengage
			ELSIF (actionType = 1) THEN
				descriptionVal := userVal || ' ' || currentState[_currentState] || ' PM disengage, symbol = ' || symbol || ' ' || dateVal || ' ' || timeVal;
			ELSE
			END IF;

		-- set buying power
		-- use _count variable for accountIndex
		ELSIF (typeVal = 8) THEN
			SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1):: integer, substring(NEW.log_detail, 5)::double precision INTO _currentState, _count, buyingPower FROM tt_event_log;
			
			IF (_count = 0) THEN
				_tsDescription := ' on first account ';
			ELSIF (_count = 1) THEN
				_tsDescription := ' on second account ';
			ELSIF (_count = 2) THEN
				_tsDescription := ' on third account ';
			ELSE
				_tsDescription := ' on fourth account ';
			END IF;
				
			descriptionVal := userVal || ' ' || currentState[_currentState] || ' Set Buying Power = ' || buyingPower || _tsDescription || ' ' || dateVal || ' ' || timeVal;

		-- Enable/Disable trading
		ELSIF (typeVal = 9) THEN
			SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, substring(NEW.log_detail from 5 for 3) INTO _currentState, actionType, _onWhich FROM tt_event_log;
			-- Enable
			IF (actionType = 0) THEN
				descriptionVal := userVal || ' ' || currentState[_currentState] || ' Enable ' || onWhich[_onWhich] || ' ' || dateVal || ' ' || timeVal;
			-- Disable
			ELSIF (actionType = 1) THEN
				descriptionVal := userVal || ' ' || currentState[_currentState] || ' Disable ' || onWhich[_onWhich] || ' ' || dateVal || ' ' || timeVal;
			ELSE
			END IF;

		-- Ignore stock list for PM
		ELSIF (typeVal = 10) THEN
			SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, trim(substring(NEW.log_detail from 5 for 8)) INTO _currentState, _action, symbol FROM tt_event_log;
			descriptionVal := userVal || ' ' || currentState[_currentState] || ' ' || action[_action] || ' ignored stock list for PM, symbol = ' || symbol || ' ' || dateVal || ' ' || timeVal;

		-- Connect/disconnect book(s) on TS and PM
		ELSIF (typeVal = 11) THEN
			SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, substring(NEW.log_detail from 5 for 3) INTO _currentState, actionType, _onWhich FROM tt_event_log;
			-- Connect
			IF (actionType = 0) THEN
				descriptionVal := userVal || ' ' || currentState[_currentState] || ' Connect to ' || onWhich[_onWhich] || ' ' || dateVal || ' ' || timeVal;
			-- Disconnect
			ELSIF (actionType = 1) THEN
				descriptionVal := userVal || ' ' || currentState[_currentState] || ' Disconnect and disable trading ' || onWhich[_onWhich] || ' ' || dateVal || ' ' || timeVal;
			ELSE
			END IF;

		-- Connect/disconnect order(s) on TS, AS and PM
		ELSIF (typeVal = 12) THEN
			SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, substring(NEW.log_detail from 5 for 3) INTO _currentState, actionType, _onWhich FROM tt_event_log;
			-- Connect
			IF (actionType = 0) THEN
				 descriptionVal := userVal || ' ' || currentState[_currentState] || ' Connect to ' || onWhich[_onWhich] || ' ' || dateVal || ' ' || timeVal;
			-- Disconnect
			ELSIF (actionType = 1) THEN
				 descriptionVal := userVal || ' ' || currentState[_currentState] || ' Disconnect and disable trading ' || onWhich[_onWhich] || ' ' || dateVal || ' ' || timeVal;
			ELSE
			END IF;

		-- connect/disconnect BBO on TS and PM
		ELSIF (typeVal = 13) THEN
			SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, substring(NEW.log_detail from 5 for 3) INTO _currentState, actionType, _onWhich FROM tt_event_log;
			-- Connect
			IF (actionType = 0) THEN
				 descriptionVal := userVal || ' ' || currentState[_currentState] || ' Connect to ' || onWhich[_onWhich] || ' ' || dateVal || ' ' || timeVal;
			-- Disconnect
			ELSIF (actionType = 1) THEN
				 descriptionVal := userVal || ' ' || currentState[_currentState] || ' Disconnect ' || onWhich[_onWhich] || ' ' || dateVal || ' ' || timeVal;
			ELSE
			END IF;
			
		-- enable/disable ISO trading on TS
		ELSIF (typeVal = 20) THEN
			SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, substring(NEW.log_detail, 5) INTO _currentState, actionType, _tsId FROM tt_event_log;
			
			IF (_tsId = -1) THEN
				-- Enable
				IF (actionType = 1) THEN
					descriptionVal := userVal || ' ' || currentState[_currentState] || ' Enable ISO trading' || ' ' || dateVal || ' ' || timeVal;
				-- Disable
				ELSIF (actionType = 0) THEN
					descriptionVal := userVal || ' ' || currentState[_currentState] || ' Disable ISO trading' || ' ' || dateVal || ' ' || timeVal;
				ELSE
				END IF;
			ELSE
				-- Enable
				IF (actionType = 1) THEN
					descriptionVal := userVal || ' ' || currentState[_currentState] || ' Enable ISO trading on TS' || _tsId || ' ' || dateVal || ' ' || timeVal;
				-- Disable
				ELSIF (actionType = 0) THEN
					descriptionVal := userVal || ' ' || currentState[_currentState] || ' Disable ISO trading on TS' || _tsId || ' ' || dateVal || ' ' || timeVal;
				ELSE
				END IF;
			END IF;
		
		ELSE
		
		SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer INTO _currentState, _tsId FROM tt_event_log;
		
		IF (_tsId = 1) THEN _tsDescription := ' on TS1 ';
		ELSIF (_tsId = 2) THEN _tsDescription := ' on TS2 ';
		ELSIF (_tsId = 3) THEN _tsDescription := ' on TS3 ';
		ELSIF (_tsId = 4) THEN _tsDescription := ' on TS4 ';
		ELSIF (_tsId = 5) THEN _tsDescription := ' on TS5 ';
		ELSIF (_tsId = 6) THEN _tsDescription := ' on TS6 ';
		ELSIF (_tsId = 7) THEN _tsDescription := ' on TS7 ';
		ELSIF (_tsId = 8) THEN _tsDescription := ' on TS8 ';
		ELSIF (_tsId = 9) THEN _tsDescription := ' on TS9 ';
		ELSE
		END IF;
		
		SELECT count(*) INTO _checkUserOrServer FROM user_login_info WHERE username = NEW.username;
		
		-- Check activity type was inserted on SOD or NOT
		SELECT count(*) INTO _count FROM tt_event_log WHERE date = dateVal AND activity_type = typeVal AND username = NEW.username;
		
		-- ACTIVITY_TYPE_MAX_CONSECUTIVE_LOSS_SETTING 14
		IF (typeVal = 14) THEN
			IF (_checkUserOrServer = 0) THEN
				IF (_count = 0) THEN
					_activityAction := ' SOD: Set max consecutive loss = ';
				ELSE RETURN OLD;
				END IF;
			ELSE
				_activityAction := ' Set max consecutive loss = ';
			END IF;
			
			SELECT substring(NEW.log_detail, 5) INTO _maxConsectutiveLoss FROM tt_event_log ;
	
			descriptionVal := userVal || ' ' || currentState[_currentState] || _activityAction ||  _maxConsectutiveLoss || _tsDescription || dateVal || ' ' || timeVal;
		
		-- ACTIVITY_TYPE_MAX_STUCKS_SETTING 15
		ELSIF (typeVal = 15) THEN
			IF (_checkUserOrServer = 0) THEN
				IF (_count = 0) THEN
					_activityAction := ' SOD: Set max stucks = ';
				ELSE RETURN OLD;
				END IF;
			ELSE
				_activityAction := ' Set max stucks = ';
			END IF;

			SELECT substring(NEW.log_detail, 5) INTO _maxStuck FROM tt_event_log;
		
			descriptionVal := userVal || ' ' || currentState[_currentState] || _activityAction || _maxStuck || _tsDescription || dateVal || ' ' || timeVal;
		
		-- ACTIVITY_TYPE_CURRENCY_MIN_SPREAD_TS_SETTING 16
		ELSIF (typeVal = 16) THEN
			IF (_checkUserOrServer = 0) THEN
				IF (_count = 0) THEN
					_activityAction := ' SOD: Set min spread = ';
				ELSE
					_activityAction := ' Set min spread = ';
				END IF;
			ELSE
				_activityAction := ' Set min spread = ';
			END IF;
		
			SELECT substring(NEW.log_detail, 5) INTO _minSpreadTS FROM tt_event_log;
			
			descriptionVal := userVal || ' ' || currentState[_currentState] || _activityAction || _minSpreadTS || _tsDescription || dateVal || ' ' || timeVal;
	
		-- ACTIVITY_TYPE_CURRENCY_MAX_LOSER_TS_SETTING 17
		ELSIF (typeVal = 17) THEN
			IF (_checkUserOrServer = 0) THEN
				IF (_count = 0) THEN
					_activityAction := ' SOD: Set max loser = ';
				ELSE RETURN OLD;
				END IF;
			ELSE
				_activityAction := ' Set max loser = ';
			END IF;
		
			SELECT substring(NEW.log_detail, 5) INTO _maxLoserTS FROM tt_event_log;
		
			descriptionVal := userVal || ' ' || currentState[_currentState] || _activityAction || _maxLoserTS || _tsDescription || dateVal || ' ' || timeVal;
		
		-- ACTIVITY_TYPE_MAX_BUYING_POWER_SETTING 18
		ELSIF (typeVal = 18) THEN
			IF (_checkUserOrServer = 0) THEN
				IF (_count = 0) THEN
					_activityAction := ' SOD: Set max buying power = ';
				ELSE RETURN OLD;
				END IF;
			ELSE
				_activityAction := ' Set max buying power = ';
			END IF;
		
			SELECT substring(NEW.log_detail, 5) INTO _maxBuyingPower FROM tt_event_log;
		
			descriptionVal := userVal || ' ' || currentState[_currentState] || _activityAction || _maxBuyingPower || _tsDescription || dateVal || ' ' || timeVal;
		
		-- ACTIVITY_TYPE_MAX_SHARES_SETTING 19
		ELSIF (typeVal = 19) THEN
			IF (_checkUserOrServer = 0) THEN
				IF (_count = 0) THEN
					_activityAction := ' SOD: Set max shares = ';
				ELSE RETURN OLD;
				END IF;
			ELSE
				_activityAction := ' Set max shares = ';
			END IF;
		
			SELECT substring(NEW.log_detail, 5) INTO _maxShares FROM tt_event_log;
		
			descriptionVal := userVal || ' ' || currentState[_currentState] || _activityAction || _maxShares || _tsDescription || dateVal || ' ' || timeVal;
		
		-- ACTIVITY_TYPE_MAX_CROSS_PER_MIN_SETTING 21
		ELSIF (typeVal = 21) THEN
			SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail, 3) INTO _currentState, _maxCross FROM tt_event_log;
		
			descriptionVal := userVal || ' ' || currentState[_currentState] || ' Set max crosses per minute = ' || _maxCross || dateVal || ' ' || timeVal;
			
		-- ACTIVITY_TYPE_AS_VOLATILE_THRESHOLD_SETTING 22
		ELSIF (typeVal = 22) THEN
			SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail, 3) INTO _currentState, _asVolatileThreshold FROM tt_event_log;
		
			descriptionVal := userVal || ' ' || currentState[_currentState] || ' Set AS volatile threshold = ' || _asVolatileThreshold || dateVal || ' ' || timeVal;
			
		-- ACTIVITY_TYPE_TS_VOLATILE_THRESHOLD_SETTING 23
		ELSIF (typeVal = 23) THEN
			SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail, 3) INTO _currentState, _tsVolatileThreshold FROM tt_event_log;
		
			descriptionVal := userVal || ' ' || currentState[_currentState] || ' Set TS volatile threshold = ' || _tsVolatileThreshold || dateVal || ' ' || timeVal;
			
		-- ACTIVITY_TYPE_AS_DISABLED_THRESHOLD_SETTING 24
		ELSIF (typeVal = 24) THEN
			SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail, 3) INTO _currentState, _asDisabledThreshold FROM tt_event_log;
		
			descriptionVal := userVal || ' ' || currentState[_currentState] || ' Set AS disabled threshold = ' || _asDisabledThreshold || dateVal || ' ' || timeVal;
			
		-- ACTIVITY_TYPE_TS_DISABLED_THRESHOLD_SETTING 25
		ELSIF (typeVal = 25) THEN
			SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail, 3) INTO _currentState, _tsDisabledThreshold FROM tt_event_log;
		
			descriptionVal := userVal || ' ' || currentState[_currentState] || ' Set TS disabled threshold = ' || _tsDisabledThreshold || dateVal || ' ' || timeVal;
			
		ELSE
		END IF;

		END IF;
		NEW.description := descriptionVal;
		RETURN NEW;
	END;
$$;


ALTER FUNCTION public._insert_tt_activities() OWNER TO postgres;

--
-- Name: cal_filter_trade_statistic_byecn2_twt2(date, date, time without time zone, time without time zone, integer, double precision, text, integer, integer, text, text, text, text, integer, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE OR REPLACE FUNCTION cal_filter_trade_statistic_byecn2_twt2(to_date date, from_date date, time_from time without time zone, time_to time without time zone, share_size integer, expected_profit double precision, symb text, bt_shares integer, sd_shares integer, ecn_filter text, ts_id text, cecn text, is_iso text, exclusive_val integer, cross_size text) RETURNS trade_result_summary_shares_traded
    LANGUAGE plpgsql
    AS $$
declare
  trade_result record;
  summary trade_result_summary_shares_traded;
  shares_filled integer;
  shares_stuck integer;
  profit_loss double precision;
 --statistic missed trade 
  count_ouch_missed_trade integer;
  sum_shares_traded_miss integer;
  sum_ouch_expected_pl double precision;
  missed_trade_record_of_ouch record;
  str_sql text;
  str_sql1 text;
  str_sql2 text;	
  str_sql_no text;
  str_sql_no_repriced text;
  str_sql_repriced text;
  str_sql_multi_orders text;
  str_sql_tmp text;
  tmp_str text;
  tmp_ASCid text;
  new_orders_table text;
  share_size_condition text;
   -- Filter Entry or Exit
  entry_exit_filter text; 
  -- Check number TS
  check_ts integer;
  
begin	
  -- Initialize
  summary.count_miss := 0;
  summary.count_repriced := 0;
  summary.count_stuck_loss := 0;
  summary.count_stuck_profit := 0;
  summary.count_loss := 0;
  summary.count_profit_under := 0;
  summary.count_profit_above := 0;
  summary.count_total := 0;
  summary.shares_traded_miss := 0;
  summary.shares_traded_repriced := 0;
  summary.shares_traded_stuck_loss := 0;
  summary.shares_traded_stuck_profit := 0;
  summary.shares_traded_loss := 0;
  summary.shares_traded_profit_under := 0;
  summary.shares_traded_profit_above := 0;
  summary.shares_traded_total := 0;
  summary.pm_shares_traded_miss := 0;
  summary.pm_shares_traded_repriced := 0;
  summary.pm_shares_traded_stuck_loss := 0;
  summary.pm_shares_traded_stuck_profit := 0;
  summary.pm_shares_traded_loss := 0;
  summary.pm_shares_traded_profit_under := 0;
  summary.pm_shares_traded_profit_above := 0;
  summary.pm_shares_traded_total := 0;
  
  summary.expected_miss := 0.00;
  summary.expected_repriced := 0.00;
  summary.expected_stuck_loss := 0.00;
  summary.expected_stuck_profit := 0.00;
  summary.expected_loss := 0.00;
  summary.expected_profit_under := 0.00;
  summary.expected_profit_above := 0.00;
  summary.expected_total := 0.00;
  summary.real_miss := 0.00;
  summary.real_stuck_loss := 0.00;
  summary.real_stuck_profit := 0.00;
  summary.real_loss = 0.00;
  summary.real_profit_under := 0.00;
  summary.real_profit_above := 0.00;
  summary.real_total := 0.00;
  
  summary.pm_miss := 0.00;
  summary.pm_stuck_loss := 0.00;
  summary.pm_stuck_profit := 0.00;
  summary.pm_loss = 0.00;
  summary.pm_profit_under := 0.00;
  summary.pm_profit_above := 0.00;
  summary.pm_total := 0.00;
  
  entry_exit_filter := '';-- Filter Entry or Exit
  check_ts := 0;
  
  share_size_condition := '';
  IF share_size <> -100 THEN 
	share_size_condition := 'expected_shares >='|| share_size ||'';
  ELSE -- Filter share size < 100 
	share_size_condition := 'expected_shares < 100';   
  END IF;
  
  IF strpos(ts_id,',') <> 0 THEN -- Multi TS
     check_ts := 1;
  END IF;
  
  new_orders_table := '(SELECT n1.date, n1.ts_id, n1.cross_id, n1.ecn, n1.time_stamp, n1.price, n1.ack_price, n1.status, n1.entry_exit, n1.left_shares, n1.shares, n1.is_iso, n2.ecn_launch ' ||
						'FROM new_orders n1 '
						'LEFT JOIN (SELECT DISTINCT date, ts_id, cross_id, ecn_launch '
						'			FROM new_orders '
						'			WHERE entry_exit = ''Entry'' AND ts_id <> -1) n2 '
						'ON n1.date = n2.date AND n1.ts_id = n2.ts_id AND n1.cross_id = n2.cross_id) AS tmp ';
  
  str_sql :='';
  str_sql_no :='SELECT DISTINCT (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + cross_id) as tmpcol FROM ' || new_orders_table || ' WHERE date >='''|| to_date || ''' and date <='''|| from_date ||'''';
  str_sql_no :=str_sql_no || ' and time_stamp between ''' || time_from || ''' and '''|| time_to || '''';
	--filter by ts, ecn launch
	IF check_ts = 0 THEN
		IF ts_id <> '0' AND ts_id <> '-1' THEN
			str_sql_no:=str_sql_no || ' and ts_id + 1 =' || ts_id;
		ELSIF ts_id = '-1' THEN
			str_sql_no:=str_sql_no || ' and bought_shares <> sold_shares ';
		END IF;
	ELSE
		IF strpos(ts_id,'-1') = 0 THEN 
			str_sql_no:=str_sql_no || ' and ts_id + 1 in ('|| ts_id ||')';
		ELSE 
			str_sql_no:=str_sql_no || ' and ((ts_id + 1 in ('|| ts_id ||')) OR (bought_shares <> sold_shares))';
		END IF;
	END IF;
	
	IF cecn <> '0' THEN
		str_sql_no:=str_sql_no || ' and ecn_launch =''' || cecn ||'''';
	END IF;
	
	IF ecn_filter = 'arcaen' THEN
		str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Entry''' ;
		entry_exit_filter:= 'Entry';
	ELSIF ecn_filter = 'arcaex' THEN
		str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Exit''' ;
		entry_exit_filter:= 'Exit';
	ELSIF ecn_filter = 'ouchen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Entry''';
		entry_exit_filter:= 'Entry';
	ELSIF ecn_filter = 'ouchex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Exit''';
		entry_exit_filter:= 'Exit';
	ELSIF ecn_filter = 'rashen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Entry''';
		entry_exit_filter:= 'Entry';
	ELSIF ecn_filter = 'rashex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Exit''';
		entry_exit_filter:= 'Exit';		
	ELSIF ecn_filter = 'batzen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Entry''';
		entry_exit_filter:= 'Entry';		
	ELSIF ecn_filter = 'batzex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Exit''';
		entry_exit_filter:= 'Exit';
	ELSIF ecn_filter = 'oubxen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''OUBX'' and entry_exit=''Entry''';
		entry_exit_filter:= 'Entry';
	ELSIF ecn_filter = 'oubxex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''OUBX'' and entry_exit=''Exit''';
		entry_exit_filter:= 'Exit';
	ELSIF ecn_filter = 'rabxen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''RABX'' and entry_exit=''Entry''';
		entry_exit_filter:= 'Entry';
	ELSIF ecn_filter = 'rabxex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''RABX'' and entry_exit=''Exit''';
		entry_exit_filter:= 'Exit';
	ELSIF ecn_filter = 'nyseen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Entry''';
		entry_exit_filter:= 'Entry';
	ELSIF ecn_filter = 'nyseex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Exit''';
		entry_exit_filter:= 'Exit';
	ELSIF ecn_filter = 'edgxen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''EDGX'' and entry_exit=''Entry''';
		entry_exit_filter:= 'Entry';
	ELSIF ecn_filter = 'edgxex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''EDGX'' and entry_exit=''Exit''';
		entry_exit_filter:= 'Exit';	
	END IF;

	--Filter by ISO Odrer
	IF is_iso = '1' THEN
		str_sql_no:=str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
	END IF;
	-- tmp_str := str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
	
	IF is_iso = '2' THEN
		str_sql_no:=str_sql_no || ' and (is_iso <> ''Y'')';
	END IF;
	
	
	-- filter by Symbol, Bought Shares, Sold Shares
	IF symb <>'' THEN
		str_sql:=' and symbol='''|| symb ||'''';
	END IF;
	IF bt_shares > 0 THEN
		str_sql:=str_sql || ' and bought_shares >=' || bt_shares;
	END IF;
	IF sd_shares > 0 THEN
		str_sql:=str_sql || ' and sold_shares >=' || sd_shares;
	END IF;
	--filter by ts
	
	IF check_ts = 0 THEN
		IF ts_id <> '0' AND ts_id <> '-1' THEN 
			str_sql:=str_sql || ' and ts_id + 1 =' || ts_id;
		ELSIF ts_id = '-1' THEN
			str_sql := str_sql || ' and bought_shares <> sold_shares ';
		END IF;
	ELSE
		IF strpos(ts_id,'-1') = 0 THEN 
			str_sql:=str_sql || ' and ts_id + 1 in ('|| ts_id ||')';
		ELSE 
			str_sql:=str_sql || ' and ((ts_id + 1 in ('|| ts_id ||')) OR (bought_shares <> sold_shares))';
		END IF;
	   
	END IF;
	IF cross_size <> '' then
		str_sql := str_sql || ' and ' || cross_size;
	END IF;
	IF ecn_filter <> '0' AND exclusive_val = 1 THEN
		str_sql_multi_orders := get_query_string_array_id_multi_orders(to_date, from_date, entry_exit_filter);
		str_sql := str_sql || ' and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) not in (' || str_sql_multi_orders || ')';
	END IF;
	
	--Calculate repriced
	
	
 IF ((ecn_filter = 'ouchen' OR ecn_filter = 'ouchex' OR ecn_filter = '0' ) AND (cecn = 'NDAQ' OR cecn = '0')) THEN 
	str_sql_no_repriced:=str_sql_no || ' and ecn =''OUCH'' and price != ack_price ' ;
	str_sql_repriced:=str_sql || '  and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) in ('|| str_sql_no_repriced ||')';	
	str_sql1:='SELECT count(*) as count_missed, sum(expected_pl)as sum_expected_pl, sum(bought_shares) as sum_bought_shares, sum(sold_shares) as sum_sold_shares FROM trade_results WHERE bought_shares = 0 and sold_shares = 0 and date >='''|| to_date || ''' and date <='''|| from_date ||''' and '|| share_size_condition ||' and expected_pl >=' || expected_profit ;
   
   str_sql1:=str_sql1 ||str_sql_repriced;
    for missed_trade_record_of_ouch in EXECUTE str_sql1 loop
		count_ouch_missed_trade := missed_trade_record_of_ouch.count_missed;
		sum_ouch_expected_pl := missed_trade_record_of_ouch.sum_expected_pl;
		sum_shares_traded_miss := missed_trade_record_of_ouch.sum_bought_shares + missed_trade_record_of_ouch.sum_sold_shares;
    end loop;

   IF count_ouch_missed_trade > 0 THEN
	summary.count_repriced := count_ouch_missed_trade;
	summary.expected_repriced := sum_ouch_expected_pl;
	summary.count_miss := -count_ouch_missed_trade;
	summary.expected_miss :=  -sum_ouch_expected_pl;
	summary.shares_traded_miss := -sum_shares_traded_miss;
	summary.shares_traded_repriced := sum_shares_traded_miss;
   END IF;

 END IF;
 
 --calculate for trade_statistics table	
 str_sql:=str_sql || '  and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) in ('|| str_sql_no ||')';
 str_sql2:='SELECT * FROM trade_results WHERE date >='''|| to_date || ''' and date <='''|| from_date ||''' and '|| share_size_condition ||' and expected_pl >=' || expected_profit ;
 str_sql2:=str_sql2 ||str_sql;
FOR trade_result in EXECUTE str_sql2 loop
    shares_filled := trade_result.bought_shares;

    IF (shares_filled > trade_result.sold_shares) THEN
      shares_filled := trade_result.sold_shares;
    END IF;

    shares_stuck := trade_result.bought_shares - trade_result.sold_shares;    

    IF (shares_stuck = 0) THEN
      profit_loss := trade_result.total_sold - trade_result.total_bought - trade_result.ts_total_fee;

      IF (shares_filled = 0) THEN
        summary.count_miss := summary.count_miss + 1;
		summary.shares_traded_miss := summary.shares_traded_miss + trade_result.bought_shares + trade_result.sold_shares;
        summary.expected_miss := summary.expected_miss + trade_result.expected_pl;
      ELSE
        IF (profit_loss < 0) THEN
          summary.count_loss := summary.count_loss + 1;
		  summary.shares_traded_loss := summary.shares_traded_loss + trade_result.bought_shares + trade_result.sold_shares;
          summary.expected_loss := summary.expected_loss + trade_result.expected_pl;
          summary.real_loss := summary.real_loss + profit_loss;
        ELSE
          IF ( profit_loss::real < trade_result.expected_pl::real) THEN
            summary.count_profit_under := summary.count_profit_under + 1;
			summary.shares_traded_profit_under := summary.shares_traded_profit_under + trade_result.bought_shares + trade_result.sold_shares;
            summary.expected_profit_under := summary.expected_profit_under + trade_result.expected_pl;
            summary.real_profit_under := summary.real_profit_under + profit_loss;
          ELSE
            summary.count_profit_above := summary.count_profit_above + 1;
			summary.shares_traded_profit_above := summary.shares_traded_profit_above + trade_result.bought_shares + trade_result.sold_shares;
            summary.expected_profit_above := summary.expected_profit_above + trade_result.expected_pl;
            summary.real_profit_above := summary.real_profit_above + profit_loss;
          END IF;
        END IF;
      END IF;
    ELSE
      --Calculate for stuck
      IF (trade_result.pm_profit_loss - trade_result.pm_total_fee < -0.0001) THEN
	--stuck_loss
	   summary.count_stuck_loss := summary.count_stuck_loss + 1;
	   summary.shares_traded_stuck_loss := summary.shares_traded_stuck_loss + trade_result.bought_shares + trade_result.sold_shares;
	   summary.expected_stuck_loss := summary.expected_stuck_loss + trade_result.expected_pl;
	   summary.real_stuck_loss := summary.real_stuck_loss + trade_result.ts_profit_loss  + trade_result.pm_profit_loss - trade_result.ts_total_fee - trade_result.pm_total_fee;
	   summary.pm_stuck_loss := summary.pm_stuck_loss + trade_result.pm_profit_loss - trade_result.pm_total_fee;
	   IF trade_result.bought_shares < trade_result.sold_shares THEN
		summary.pm_shares_traded_stuck_loss := summary.pm_shares_traded_stuck_loss - trade_result.bought_shares + trade_result.sold_shares - trade_result.left_stuck_shares;
	   ELSE
		summary.pm_shares_traded_stuck_loss := summary.pm_shares_traded_stuck_loss + trade_result.bought_shares - trade_result.sold_shares - trade_result.left_stuck_shares;
	   END IF;
      ELSE
	--stuck_profit
	   summary.count_stuck_profit := summary.count_stuck_profit + 1;
	   summary.shares_traded_stuck_profit := summary.shares_traded_stuck_profit + trade_result.bought_shares + trade_result.sold_shares;
	   summary.expected_stuck_profit := summary.expected_stuck_profit + trade_result.expected_pl;
	   summary.real_stuck_profit := summary.real_stuck_profit + trade_result.ts_profit_loss + trade_result.pm_profit_loss - trade_result.ts_total_fee - trade_result.pm_total_fee;
	   summary.pm_stuck_profit := summary.pm_stuck_profit + trade_result.pm_profit_loss - trade_result.pm_total_fee;
	   IF trade_result.bought_shares < trade_result.sold_shares THEN
		summary.pm_shares_traded_stuck_profit := summary.pm_shares_traded_stuck_profit - trade_result.bought_shares + trade_result.sold_shares - trade_result.left_stuck_shares;
	   ELSE
		summary.pm_shares_traded_stuck_profit := summary.pm_shares_traded_stuck_profit + trade_result.bought_shares - trade_result.sold_shares - trade_result.left_stuck_shares;
	   END IF;
      END IF;
    END IF;

    summary.count_total := summary.count_total +  1;
	summary.shares_traded_total := summary.shares_traded_total + trade_result.bought_shares + trade_result.sold_shares;
    summary.expected_total := summary.expected_total + trade_result.expected_pl  ;
  end loop;
  
  summary.count_total := summary.count_total - summary.count_repriced;
  summary.shares_traded_total := summary.shares_traded_total - summary.shares_traded_repriced;
  summary.expected_total := summary.expected_total - summary.expected_repriced;
  
  return summary;
end
$$;


ALTER FUNCTION public.cal_filter_trade_statistic_byecn2_twt2(to_date date, from_date date, time_from time without time zone, time_to time without time zone, share_size integer, expected_profit double precision, symb text, bt_shares integer, sd_shares integer, ecn_filter text, ts_id text, cecn text, is_iso text, exclusive_val integer, cross_size text) OWNER TO postgres;

--
-- Name: cal_filter_trade_statistic_byecn2_twt2_with_step(date, date, time without time zone, time without time zone, integer, double precision, text, integer, integer, text, text, text, integer, text, integer, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE OR REPLACE FUNCTION cal_filter_trade_statistic_byecn2_twt2_with_step(to_date date, from_date date, time_from time without time zone, time_to time without time zone, share_size integer, expected_profit double precision, symb text, bt_shares integer, sd_shares integer, ecn_filter text, ts_id text, cecn text, step integer, is_iso text, exclusive_val integer, cross_size text) RETURNS SETOF trade_result_summary2_struct
    LANGUAGE plpgsql
    AS $$ 
declare
	trade_result 				record;
	summary 				trade_result_summary2_struct;
	shares_filled 				integer;
	shares_stuck 				integer;
	profit_loss 				double precision;
	--statistic missed trade 
	count_ouch_missed_trade 		integer;
	sum_ouch_expected_pl 			double precision;
	missed_trade_record_of_ouch 		record;
	str_sql 				text;
	str_sql_no 				text;
	str_sql_tmp			   	text;
	str_sql_multi_orders text;
	tmp text;
	tmp_str text;
	min_time				"time";
	max_time				"time";
	temp_time 				"time";
	t_current				"time";
	new_orders_table text;
	share_size_condition text;
	 -- Filter Entry or Exit
	entry_exit_filter text;
	 -- Check number TS
    check_ts integer;
  
begin	
						
	min_time := time_from;
	max_time := time_to;
	
	entry_exit_filter := '';  -- Filter Entry or Exit
	
	share_size_condition := '';	
	IF share_size <> -100 THEN 
		share_size_condition := 'expected_shares >='|| share_size ||'';
	ELSE -- Filter share size < 100 
		share_size_condition := 'expected_shares < 100' ;
	END IF;
	
	check_ts := 0;
	IF strpos(ts_id,',') <> 0 THEN -- Multi TS
		check_ts := 1;
	END IF;
  
	
	WHILE min_time < max_time LOOP
		-- Initia
		
		summary.count_miss := 0;
		summary.count_repriced := 0;
		summary.count_stuck_loss := 0;
		summary.count_stuck_profit := 0;
		summary.count_loss := 0;
		summary.count_profit_under := 0;
		summary.count_profit_above := 0;		
		
		IF (step = 5) THEN
			t_current = min_time + interval '5 minutes';
		ELSIF (step = 15) THEN
			t_current = min_time + interval '15 minutes';
			
		ELSIF (step = 30) THEN
			t_current = min_time + interval '30 minutes';
		ELSIF (step = 60) THEN
			t_current = min_time + interval '60 minutes';
		END IF;
		--raise notice '1(%)', t_current;
		IF ((t_current > max_time) or (t_current = '00:00:00')) THEN
			t_current = max_time;
			--raise notice '2(%)', t_current;
		END IF; 
		temp_time = t_current - interval '1 microsecond';
		
		--Calculate repriced
		IF ((ecn_filter = 'ouchen' OR ecn_filter = 'ouchex' OR ecn_filter = '0' ) AND (cecn = 'NDAQ' OR cecn = '0')) THEN
		
			--Select data from view "vwouch_missed_trades" and calculate total missed trade 	
			str_sql:='SELECT count(*) as count_missed, sum(expected_pl)as sum_expected_pl  FROM trade_results WHERE date >='''|| to_date || ''' and date <='''|| from_date ||''' and '|| share_size_condition ||' and expected_pl >=' || expected_profit ;
			str_sql := str_sql || ' and bought_shares = 0 and sold_shares = 0 ';
			
			--filter by ts when we get data from view trade_results
			IF check_ts = 0 THEN
				IF ts_id <> '0' THEN 
					str_sql:=str_sql || ' and ts_id + 1 =' || ts_id;
				ELSIF ts_id = '-1' THEN
					str_sql := str_sql || ' and bought_shares <> sold_shares ';
				END IF;
			ELSE
				IF strpos(ts_id,'-1') = 0 THEN 
					str_sql:=str_sql || ' and ts_id + 1 in ('|| ts_id ||')';
				ELSE 
					str_sql:=str_sql || ' and ((ts_id + 1 in ('|| ts_id ||')) OR (bought_shares <> sold_shares))';
				END IF;
			   
			END IF;
			IF cross_size <> '' then
				str_sql := str_sql || ' and ' || cross_size;
			END IF;
			
			str_sql_no :='SELECT (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + cross_id) as tmpcol FROM new_orders WHERE date >='''|| to_date || ''' and date <='''|| from_date ||'''';
			str_sql_no :=str_sql_no || ' and time_stamp between ''' || min_time || ''' and '''|| temp_time || '''';
			str_sql_no :=str_sql_no || ' and ecn =''OUCH'' and price != ack_price ';
			
			--filter by ts, ecn_launch when we get data from table new_orders
			IF check_ts = 0 THEN
				IF ts_id <> '0' AND ts_id <> '-1' THEN
					str_sql_no:=str_sql_no || ' and ts_id + 1 =' || ts_id;
				ELSIF ts_id = '-1' THEN
					str_sql_no:=str_sql_no || ' and bought_shares <> sold_shares ';
				END IF;
			ELSE
				IF strpos(ts_id,'-1') = 0 THEN 
					str_sql_no:=str_sql_no || ' and ts_id + 1 in ('|| ts_id ||')';
				ELSE 
					str_sql_no:=str_sql_no || ' and ((ts_id + 1 in ('|| ts_id ||')) OR (bought_shares <> sold_shares))';
				END IF;
			END IF;
			
			IF cecn <> '0' THEN
				str_sql_no:=str_sql_no || ' and ecn_launch =''' || cecn ||'''';
			END IF;
			
			IF ecn_filter = 'arcaen' THEN
				str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Entry''' ;
				entry_exit_filter:= 'Entry';
			ELSIF ecn_filter = 'arcaex' THEN
				str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Exit''' ;
				entry_exit_filter:= 'Exit';
			ELSIF ecn_filter = 'ouchen' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Entry''';
				entry_exit_filter:= 'Entry';
			ELSIF ecn_filter = 'ouchex' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Exit''';
				entry_exit_filter:= 'Exit';
			ELSIF ecn_filter = 'rashen' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Entry''';
				entry_exit_filter:= 'Entry';
			ELSIF ecn_filter = 'rashex' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Exit''';
				entry_exit_filter:= 'Exit';				
			ELSIF ecn_filter = 'batzen' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Entry''';
				entry_exit_filter:= 'Entry';				
			ELSIF ecn_filter = 'batzex' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Exit''';
				entry_exit_filter:= 'Exit';				
			ELSIF ecn_filter = 'oubxen' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''OUBX'' and entry_exit=''Entry''';
				entry_exit_filter:= 'Entry';
			ELSIF ecn_filter = 'oubxex' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''OUBX'' and entry_exit=''Exit''';
				entry_exit_filter:= 'Exit';
			ELSIF ecn_filter = 'rabxen' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''RABX'' and entry_exit=''Entry''';
				entry_exit_filter:= 'Entry';
			ELSIF ecn_filter = 'rabxex' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''RABX'' and entry_exit=''Exit''';
				entry_exit_filter:= 'Exit';
			ELSIF ecn_filter = 'nyseen' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Entry''';
				entry_exit_filter:= 'Entry';
			ELSIF ecn_filter = 'nyseex' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Exit''';
				entry_exit_filter:= 'Exit';
			ELSIF ecn_filter = 'edgxen' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''EDGX'' and entry_exit=''Entry''';
				entry_exit_filter:= 'Entry';
			ELSIF ecn_filter = 'edgxex' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''EDGX'' and entry_exit=''Exit''';
				entry_exit_filter:= 'Exit';
			END IF;

			IF symb <>'' THEN
				str_sql:=str_sql || ' and symbol='''|| symb ||'''';
				str_sql_no:=str_sql_no || ' and symbol='''|| symb ||'''';
			END IF;
			--Filter by ISO Odrer
			IF is_iso = '1' AND from_date > '2010-01-01' THEN
				str_sql_no:=str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
			END IF;
			-- tmp_str := str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
	
			IF is_iso = '2' AND from_date > '2010-01-01' THEN
				str_sql_no:=str_sql_no || ' and (is_iso <> ''Y'')';
			END IF;
			
			str_sql_no:=str_sql_no || ' GROUP BY (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + cross_id)' ;

			IF bt_shares > 0 THEN
				str_sql:=str_sql || ' and bought_shares >=' || bt_shares;
			END IF;
			IF sd_shares > 0 THEN
				str_sql:=str_sql || ' and sold_shares >=' || sd_shares;
			END IF;

			str_sql:=str_sql || ' and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) in ('|| str_sql_no ||')';
			
			IF ecn_filter <> '0' AND exclusive_val = 1 THEN
				str_sql_multi_orders := get_query_string_array_id_multi_orders(to_date, from_date, entry_exit_filter);
				str_sql :=str_sql || ' and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) not in (' || str_sql_multi_orders || ')';
			END IF;
			
			for missed_trade_record_of_ouch in EXECUTE str_sql loop
				count_ouch_missed_trade := missed_trade_record_of_ouch.count_missed;
				sum_ouch_expected_pl := missed_trade_record_of_ouch.sum_expected_pl;
			end loop;

			IF count_ouch_missed_trade > 0 THEN
				summary.count_repriced := count_ouch_missed_trade;				
				summary.count_miss := -count_ouch_missed_trade;				
			END IF;

		END IF;

		--filter by  ECN by Entry/Exit
		new_orders_table := '(SELECT n1.date, n1.ts_id, n1.cross_id, n1.ecn, n1.time_stamp, n1.price, n1.ack_price, n1.status, n1.entry_exit, n1.left_shares, n1.shares, n1.is_iso, n2.ecn_launch ' ||
						'FROM new_orders n1 '
						'LEFT JOIN (SELECT DISTINCT date, ts_id, cross_id, ecn_launch '
						'			FROM new_orders '
						'			WHERE entry_exit = ''Entry'' AND ts_id <> -1 AND time_stamp between ''' || min_time || ''' AND '''|| temp_time || ''') n2 '
						'ON n1.date = n2.date AND n1.ts_id = n2.ts_id AND n1.cross_id = n2.cross_id) AS tmp ';
						
		str_sql:='SELECT * FROM trade_results WHERE date >='''|| to_date || ''' and date <='''|| from_date ||''' and '|| share_size_condition ||' and expected_pl >=' || expected_profit ;
		str_sql_no :='SELECT (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + cross_id) as tmpcol FROM ' || new_orders_table || ' WHERE date >='''|| to_date || ''' and date <='''|| from_date ||'''';
		str_sql_no :=str_sql_no || ' and time_stamp between ''' || min_time || ''' and '''|| temp_time || '''';
		
		--filter by ts, ecn launch
		IF check_ts = 0 THEN
			IF ts_id <> '0' AND ts_id <> '-1' THEN
				str_sql_no:=str_sql_no || ' and ts_id + 1 =' || ts_id;
			ELSIF ts_id = '-1' THEN
				str_sql_no:=str_sql_no || ' and bought_shares <> sold_shares ';
			END IF;
		ELSE
			IF strpos(ts_id,'-1') = 0 THEN 
				str_sql_no:=str_sql_no || ' and ts_id + 1 in ('|| ts_id ||')';
			ELSE 
				str_sql_no:=str_sql_no || ' and ((ts_id + 1 in ('|| ts_id ||')) OR (bought_shares <> sold_shares))';
			END IF;
		END IF;
		
		IF cecn <> '0' THEN
			str_sql_no:=str_sql_no || ' and ecn_launch =''' || cecn ||'''';
		END IF;
		
		IF ecn_filter = 'arcaen' THEN
			str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Entry''' ;
			entry_exit_filter:= 'Entry';
		ELSIF ecn_filter = 'arcaex' THEN
			str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Exit''' ;
			entry_exit_filter:= 'Exit';
		ELSIF ecn_filter = 'ouchen' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Entry''';
			entry_exit_filter:= 'Entry';
		ELSIF ecn_filter = 'ouchex' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Exit''';
			entry_exit_filter:= 'Exit';
		ELSIF ecn_filter = 'rashen' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Entry''';
			entry_exit_filter:= 'Entry';
		ELSIF ecn_filter = 'rashex' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Exit''';	
			entry_exit_filter:= 'Exit';			
		ELSIF ecn_filter = 'batzen' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Entry''';	
			entry_exit_filter:= 'Entry';
		ELSIF ecn_filter = 'batzex' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Exit''';
			entry_exit_filter:= 'Exit';			
		ELSIF ecn_filter = 'nyseen' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Entry''';
			entry_exit_filter:= 'Entry';
		ELSIF ecn_filter = 'nyseex' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Exit''';
			entry_exit_filter:= 'Exit';	
		ELSIF ecn_filter = 'edgxen' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''EDGX'' and entry_exit=''Entry''';
			entry_exit_filter:= 'Entry';
		ELSIF ecn_filter = 'edgxex' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''EDGX'' and entry_exit=''Exit''';
			entry_exit_filter:= 'Exit';	
		END IF;
		
		--Filter by ISO Odrer
		IF is_iso = '1' AND from_date > '2010-01-01' THEN
			str_sql_no:=str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
		END IF;
		-- tmp_str := str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
	
		IF is_iso = '2' AND from_date > '2010-01-01' THEN
			str_sql_no:=str_sql_no || ' and (is_iso <> ''Y'')';
		END IF;
		
		str_sql_no:=str_sql_no || ' GROUP BY (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + cross_id)' ;

		-- filter by Symbol, Bought Shares, Sold Shares
		IF symb <>'' THEN
			str_sql:=str_sql || ' and symbol='''|| symb ||'''';
		END IF;
		IF bt_shares > 0 THEN
			str_sql:=str_sql || ' and bought_shares >=' || bt_shares;
		END IF;
		IF sd_shares > 0 THEN
			str_sql:=str_sql || ' and sold_shares >=' || sd_shares;
		END IF;
		--filter by ts
		IF check_ts = 0 THEN
			IF ts_id <> '0' THEN 
				str_sql:=str_sql || ' and ts_id + 1 =' || ts_id;
			ELSIF ts_id = '-1' THEN
				str_sql := str_sql || ' and bought_shares <> sold_shares ';
			END IF;
		ELSE
			IF strpos(ts_id,'-1') = 0 THEN 
				str_sql:=str_sql || ' and ts_id + 1 in ('|| ts_id ||')';
			ELSE 
				str_sql:=str_sql || ' and ((ts_id + 1 in ('|| ts_id ||')) OR (bought_shares <> sold_shares))';
			END IF;
		   
		END IF;
		IF cross_size <> '' then
			str_sql := str_sql || ' and ' || cross_size;
		END IF;
		
		str_sql:=str_sql || ' and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) in ('|| str_sql_no ||')';
		
		IF ecn_filter <> '0' AND exclusive_val = 1 THEN
			str_sql_multi_orders := get_query_string_array_id_multi_orders(to_date, from_date, entry_exit_filter);
			str_sql :=str_sql || ' and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) not in (' || str_sql_multi_orders || ')';
		END IF;
		
		--calculate for trade_statistics table	
		FOR trade_result in EXECUTE str_sql loop
			shares_filled := trade_result.bought_shares;
			--RAISE NOTICE 'Variable an_integer was changed.';			
			IF (shares_filled > trade_result.sold_shares) THEN
			  shares_filled := trade_result.sold_shares;
			END IF;

			shares_stuck := trade_result.bought_shares - trade_result.sold_shares;    

			IF (shares_stuck = 0) THEN
				profit_loss := trade_result.total_sold - trade_result.total_bought - trade_result.ts_total_fee;

				IF (shares_filled = 0) THEN
				summary.count_miss := summary.count_miss + 1;				
				ELSE
					IF (profit_loss < 0) THEN
						summary.count_loss := summary.count_loss + 1;						
					ELSE
						IF ( profit_loss::real < trade_result.expected_pl::real) THEN
							summary.count_profit_under := summary.count_profit_under + 1;						
						ELSE
							summary.count_profit_above := summary.count_profit_above + 1;							
						END IF;
					END IF;
				END IF;
			ELSE
				--Calculate for stuck
				IF (trade_result.pm_profit_loss - trade_result.pm_total_fee < -0.0001) THEN
					--stuck_loss
					summary.count_stuck_loss := summary.count_stuck_loss + 1;					
				ELSE
					--stuck_profit
					summary.count_stuck_profit := summary.count_stuck_profit + 1;				
				END IF;
			END IF;

		END LOOP;

		
		summary.time_stamp = t_current;
		min_time := t_current;	
	
		RETURN NEXT summary;
		
	END LOOP;
	RETURN;
end
$$;


ALTER FUNCTION public.cal_filter_trade_statistic_byecn2_twt2_with_step(to_date date, from_date date, time_from time without time zone, time_to time without time zone, share_size integer, expected_profit double precision, symb text, bt_shares integer, sd_shares integer, ecn_filter text, ts_id text, cecn text, step integer, is_iso text, exclusive_val integer, cross_size text) OWNER TO postgres;

--
-- Name: calculate_total_fee(double precision, integer, character, text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE OR REPLACE FUNCTION calculate_total_fee(price double precision, shares integer, side character, liquidityval text, ecn text) RETURNS double precision
    LANGUAGE plpgsql
    AS $$
	DECLARE
		sec_fee double precision;
		taf_fee double precision;
		wedbus_commission double precision;
		add_venue_fee_with_price_larger_than_1 double precision;
		add_venue_fee_with_price_smaller_than_1 double precision;
		remove_venue_fee_with_price_larger_than_1 double precision;
		remove_venue_fee_with_price_smaller_than_1 double precision;
		auction_venue_fee double precision;
		ARCA_route_venue_fee double precision;
		NDAQ_route_venue_fee double precision;
		BATS_route_venue_fee double precision;
		EDGX_route_venue_fee double precision;
		
		totalVenueFee double precision;
		totalSecFees double precision;
		totalTafFee double precision;
		totalFee double precision;
		totalWedbushCommission double precision;
		
		liquidity character(2);
	BEGIN
		-- If we changed some fees in fee_config file on AS or TA, we would change those fees here
		sec_fee := 0.00002240;
		taf_fee := 0.00009;
		wedbus_commission := 0.000035;
		add_venue_fee_with_price_larger_than_1 := -0.0028;
		add_venue_fee_with_price_smaller_than_1 := 0.00;
		remove_venue_fee_with_price_larger_than_1 := 0.003;
		remove_venue_fee_with_price_smaller_than_1 := 0.003;
		auction_venue_fee := 0.0005;
		ARCA_route_venue_fee := 0.003;
		NDAQ_route_venue_fee := 0.0035;
		BATS_route_venue_fee := 0.002;
		EDGX_route_venue_fee := 0.0029;
		
		-- Initialize fees
		totalVenueFee := 0.00;
		totalSecFees := 0.00;
		totalTafFee := 0.00;
		totalFee := 0.00;
		totalWedbushCommission := 0.00;
		-- Calculate total fee
		if (side = 'S') then
			totalSecFees := shares * price * sec_fee;
		end if;
		
		totalTafFee := shares * taf_fee;
		totalWedbushCommission := shares * wedbus_commission;
		
		select trim(liquidityVal) into liquidity;
		if (ecn = 'ARCA') then
			-- Add
			if (liquidity = 'A' or liquidity = 'B' or liquidity = 'M' or liquidity = 'D' or liquidity = 'S') then
				if (price >= 1.00) then
					totalVenueFee := shares * add_venue_fee_with_price_larger_than_1;
				else
					totalVenueFee := shares * add_venue_fee_with_price_smaller_than_1;
				end if;
			-- Remove
			elsif (liquidity = 'R' or liquidity = 'E' or liquidity = 'L') then
				if (price >= 1.00) then
					totalVenueFee := shares * remove_venue_fee_with_price_larger_than_1;
				else
					totalVenueFee := shares * price * remove_venue_fee_with_price_smaller_than_1;
				end if;
			-- Auction
			elsif (liquidity = 'G' or liquidity = 'Z' or liquidity = 'O') then
				totalVenueFee := shares * auction_venue_fee;
			elsif (liquidity = 'X' or liquidity = 'F' or liquidity = 'N' or liquidity = 'H' or liquidity = 'C' or liquidity = 'U' or liquidity = 'Y' or liquidity = 'W') then
				totalVenueFee := shares * ARCA_route_venue_fee;
			end if;
			
		elsif (ecn = 'OUCH') then
			-- Add
			if (liquidity = 'A' or liquidity = 'k' or liquidity = 'J' or liquidity = '7' or liquidity = '8' or liquidity = 'e' or liquidity = 'f') then
				if (price >= 1.00) then
					totalVenueFee := shares * add_venue_fee_with_price_larger_than_1;
				else
					totalVenueFee := shares * add_venue_fee_with_price_smaller_than_1;
				end if;
			-- Remove
			elsif (liquidity = 'R' or liquidity = 'm' or liquidity = '6' or liquidity = 'd') then
				if (price >= 1.00) then
					totalVenueFee := shares * remove_venue_fee_with_price_larger_than_1;
				else
					totalVenueFee := shares * price * remove_venue_fee_with_price_smaller_than_1;
				end if;
			-- Auction
			elsif (liquidity = 'O' or liquidity = 'M' or liquidity = 'L' or liquidity = 'H' or liquidity = 'K' or liquidity = 'C') then
				totalVenueFee := shares * auction_venue_fee;
			end if;
		
		elsif (ecn = 'RASH') then
			-- Add
			if (liquidity = 'A' or liquidity = 'J' or liquidity = 'V' or liquidity = 'D' or liquidity = 'F' or liquidity = 'U' or liquidity = 'k' or liquidity = '9' or liquidity = '7' or liquidity = '8' or liquidity = 'e' or liquidity = 'f') then
				if (price >= 1.00) then
					totalVenueFee := shares * add_venue_fee_with_price_larger_than_1;
				else
					totalVenueFee := shares * add_venue_fee_with_price_smaller_than_1;
				end if;
			-- Remove
			elsif (liquidity = 'R' or liquidity = 'm' or liquidity = 'd' or liquidity = '6') then
				if (price >= 1.00) then
					totalVenueFee := shares * remove_venue_fee_with_price_larger_than_1;
				else
					totalVenueFee := shares * price * remove_venue_fee_with_price_smaller_than_1;
				end if;		
			-- Auction
			elsif (liquidity = 'G' or liquidity = 'O' or liquidity = 'M' or liquidity = 'C' or liquidity = 'L' or liquidity = 'H' or liquidity = 'K' or liquidity = 'I' or liquidity = 'T' or liquidity = 'Z') then
				totalVenueFee := shares * auction_venue_fee;
			-- Route venue fee
			elsif (liquidity = 'X' or liquidity = 'Y' or liquidity = 'B' or liquidity = 'P' or liquidity = 'Q') then
				totalVenueFee := shares * NDAQ_route_venue_fee;
			end if;

		elsif (ecn = 'NYSE') then
			-- Add (provider)
			if (liquidity = '2' or liquidity = '8') then
				if (price >= 1.00) then
					totalVenueFee := shares * add_venue_fee_with_price_larger_than_1;
				else
					totalVenueFee := shares * add_venue_fee_with_price_smaller_than_1;
				end if;
			-- Remove (taker and blended)
			elsif (liquidity = '1' or liquidity = '9' or liquidity = '3' or liquidity = ' ') then
				if (price >= 1.00) then
					totalVenueFee := shares * remove_venue_fee_with_price_larger_than_1;
				else
					totalVenueFee := shares * price * remove_venue_fee_with_price_smaller_than_1;
				end if;
			-- Auction
			elsif (liquidity = '4' or liquidity = '5' or liquidity = '6' or liquidity = '7' ) then
				totalVenueFee := shares * auction_venue_fee;
			end if;
			
		elsif (ecn = 'BATZ') then
			-- Add (provider)
			if (liquidity = 'A') then
				if (price >= 1.00) then
					totalVenueFee := shares * add_venue_fee_with_price_larger_than_1;
				else
					totalVenueFee := shares * add_venue_fee_with_price_smaller_than_1;
				end if;
			-- Remove (taker and blended)
			elsif (liquidity = 'R') then
				if (price >= 1.00) then
					totalVenueFee := shares * remove_venue_fee_with_price_larger_than_1;
				else
					totalVenueFee := shares * price * remove_venue_fee_with_price_smaller_than_1;
				end if;
			-- Auction
			elsif (liquidity = 'C' ) then
				totalVenueFee := shares * auction_venue_fee;
			-- Route
			elsif (liquidity = 'X' ) then
				totalVenueFee := shares * BATS_route_venue_fee;
			end if;
			
		elsif (ecn = 'EDGX') then
			-- Add
			if (liquidity = 'EA' or liquidity = 'HA' or liquidity = 'AA' or liquidity = 'CL' or liquidity = 'DM' or liquidity = 'MM' or liquidity = 'PA' or liquidity = 'RB' or liquidity = 'RC' or liquidity = 'RS' or liquidity = 'RW' or liquidity = 'RY' or liquidity = 'RZ' or liquidity = 'A' or liquidity = 'B' or liquidity = 'F' or liquidity = 'M' or liquidity = 'P' or liquidity = 'V' or liquidity = '3' or liquidity = '9' or liquidity = '4' or liquidity = '8' or liquidity = '1' or liquidity = 'E' or liquidity = 'H' or liquidity = 'Y') then
				if (price >= 1.00) then
					totalVenueFee := shares * add_venue_fee_with_price_larger_than_1;
				else
					totalVenueFee := shares * add_venue_fee_with_price_smaller_than_1;
				end if;
			-- Remove
			elsif (liquidity = 'ER' or liquidity = 'BB' or liquidity = 'DT' or liquidity = 'MT' or liquidity = 'PI' or liquidity = 'PT' or liquidity = 'C' or liquidity = 'D' or liquidity = 'G' or liquidity = 'J' or liquidity = 'L' or liquidity = 'N' or liquidity = 'U' or liquidity = 'W' or liquidity = '2' or liquidity = '6') then
				if (price >= 1.00) then
					totalVenueFee := shares * remove_venue_fee_with_price_larger_than_1;
				else
					totalVenueFee := shares * price * remove_venue_fee_with_price_smaller_than_1;
				end if;
			-- Auction
			elsif (liquidity = 'OO' or liquidity = 'O' or liquidity = '5') then
				totalVenueFee := shares * auction_venue_fee;
			-- Route
			elsif (liquidity = 'BY' or liquidity = 'PX' or liquidity = 'RP' or liquidity = 'RQ' or liquidity = 'RR' or iquidityField = 'SW' or liquidity = 'I' or liquidity = 'K' or liquidity = 'Q' or liquidity = 'R' or liquidity = 'T' or liquidity = 'X' or liquidity = 'Z' or liquidity = '7' or liquidity = 'S') then
				totalVenueFee := shares * EDGX_route_venue_fee;
			end if;
		end if;
		
		totalFee := totalSecFees + totalTafFee + totalWedbushCommission + totalVenueFee;
		return totalFee;
		
	END $$;


ALTER FUNCTION public.calculate_total_fee(price double precision, shares integer, side character, liquidityval text, ecn text) OWNER TO postgres;

--
-- Name: cancel_all_hung_order(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE OR REPLACE FUNCTION cancel_all_hung_order(list_order_id text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE

	hung_order_id integer[] = string_to_array(list_order_id, '_');
	count integer;
	not_cancel integer;
	tempOrder record;
	tempCancel record;
	msg_content text;
BEGIN
	count = 1;
	not_cancel = 0;
	while hung_order_id[count] IS NOT NULL LOOP
		select * into tempOrder from new_orders where oid = hung_order_id[count];
		count = count + 1;
		msg_content = '';
		IF tempOrder.ecn = 'ARCA' AND tempOrder.ecn_suffix IS NULL then
			msg_content = msg_content || '-50=4';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-46=' || tempOrder.order_id;
		ELSIF tempOrder.ecn = 'ARCA' AND tempOrder.ecn_suffix = 'FIX' then
			msg_content = msg_content || '35=8';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '11=' || tempOrder.order_id;
		ELSIF tempOrder.ecn = 'OUCH' THEN
			msg_content = msg_content || '-13=O';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-12=' || tempOrder.order_id;	
		ELSIF tempOrder.ecn = 'RASH' THEN
			msg_content = msg_content || '-13=C'; 
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-12=' || tempOrder.order_id;
		ELSIF tempOrder.ecn = 'NYSE' THEN
			msg_content = msg_content || '-50=D1';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-46=' || tempOrder.order_id;
		ELSIF tempOrder.ecn = 'BATZ' THEN
			msg_content = msg_content || '-50=F';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-46=' || tempOrder.order_id;
		ELSIF tempOrder.ecn = 'EDGX' THEN
			msg_content = msg_content || '-13=C';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-12=' || tempOrder.order_id;
		END IF;
		msg_content = msg_content || E'\001';
		
		select * into tempCancel from cancels where oid = tempOrder.oid;
		IF  tempCancel.oid IS NULL THEN
			INSERT INTO cancels(oid, shares, time_stamp, msgseq_num,sending_time, origclord_id , arcaex_order_id, exec_id, ord_status, text, msg_content, manual_update, processed_time_stamp)
			VALUES(tempOrder.oid, tempOrder.shares, tempOrder.time_stamp, tempOrder.msgseq_num, NULL, tempOrder.order_id, NULL, NULL , NULL, NULL, msg_content, 1, tempOrder.time_stamp);
			
			UPDATE new_orders SET status = 'CanceledByECN' where oid = tempOrder.oid;			
			UPDATE trading_status SET hung_order = 1;
		ELSE
			not_cancel = not_cancel + 1;
		END IF;
		
	end loop;

	RETURN not_cancel;
END
$$;


ALTER FUNCTION public.cancel_all_hung_order(list_order_id text) OWNER TO postgres;

--
-- Name: insert_cancel_hung_order(integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE OR REPLACE FUNCTION insert_cancel_hung_order(oid_t integer, cancel_shares integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	temporder record;
	tempCancel record;
	msg_content text;
BEGIN

	select * into tempOrder from new_orders where oid = oid_t;
	
	select * into tempCancel from cancels where oid = oid_t;
	msg_content = '';
	IF tempOrder.ecn = 'ARCA' AND tempOrder.ecn_suffix IS NULL then
		msg_content = msg_content || '-50=4';
		msg_content = msg_content || E'\001';
		msg_content = msg_content || '-46=' || tempOrder.order_id;
	ELSIF tempOrder.ecn = 'ARCA' AND tempOrder.ecn_suffix = 'FIX' then
		msg_content = msg_content || '35=8';
		msg_content = msg_content || E'\001';
		msg_content = msg_content || '11=' || tempOrder.order_id;
	ELSIF tempOrder.ecn = 'OUCH' THEN
		msg_content = msg_content || '-13=O';
		msg_content = msg_content || E'\001';
		msg_content = msg_content || '-12=' || tempOrder.order_id;
	ELSIF tempOrder.ecn = 'RASH' THEN
		msg_content = msg_content || '-13=C'; 
		msg_content = msg_content || E'\001';
		msg_content = msg_content || '-12=' || tempOrder.order_id;
	ELSIF tempOrder.ecn = 'NYSE' THEN
		msg_content = msg_content || '-50=D1';
		msg_content = msg_content || E'\001';
		msg_content = msg_content || '-46=' || tempOrder.order_id;
	ELSIF tempOrder.ecn = 'BATZ' THEN
		msg_content = msg_content || '-50=F';
		msg_content = msg_content || E'\001';
		msg_content = msg_content || '-46=' || tempOrder.order_id;
	ELSIF tempOrder.ecn = 'EDGX' THEN
		msg_content = msg_content || '-13=C';
		msg_content = msg_content || E'\001';
		msg_content = msg_content || '-12=' || tempOrder.order_id;
	END IF;
	
	msg_content = msg_content || E'\001';
	
	IF tempCancel.oid is NULL then
		INSERT INTO cancels(oid, shares, time_stamp, msgseq_num, origclord_id, msg_content, manual_update, processed_time_stamp)
		VALUES(tempOrder.oid, cancel_shares, tempOrder.time_stamp, tempOrder.msgseq_num, tempOrder.order_id , msg_content, 1, tempOrder.time_stamp);
		
		UPDATE new_orders SET status = 'CanceledByECN' WHERE oid = oid_t;
		UPDATE trading_status SET hung_order = 1;	
	ELSE
		return 0;
	END IF;
	
	return 1;
END
$$;


ALTER FUNCTION public.insert_cancel_hung_order(oid_t integer, cancel_shares integer) OWNER TO postgres;

--
-- Name: insert_fill_hung_order(integer, text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE OR REPLACE FUNCTION insert_fill_hung_order(oid_t integer, list_shares text, list_price text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	shares_array integer[] = string_to_array(list_shares, '_');
	price_array double precision[] = string_to_array(list_price, '_');
	tempOrder record;
	tempFill record;
	i integer;
	liquidity character;
	msg_content text;
	id_current integer;
	exce_id_tmp text;
	total_fill_shares integer;
	totalFee double precision;
BEGIN
 
	i = 1;
	total_fill_shares = 0;
	select * into tempOrder from new_orders where oid = oid_t;
	if tempOrder.ecn = 'ARCA' then
		liquidity = 'R';
	elsif tempOrder.ecn = 'OUCH' then
		liquidity = 'R';
	elsif tempOrder.ecn = 'RASH' then
		liquidity = 'R';
	elsif tempOrder.ecn = 'NYSE' then
		liquidity = '1';
	elsif tempOrder.ecn = 'BATZ' then
		liquidity = 'R';
	elsif tempOrder.ecn = 'EDGX' then
		liquidity = '6';
	end if;
	select max(id) into id_current from fills; 
	IF id_current IS NULL THEN
		id_current = 1;
	END IF;
	exce_id_tmp = '';
	WHILE shares_array[i] IS NOT NULL LOOP
		msg_content = '';
		total_fill_shares = total_fill_shares + shares_array[i];
		if tempOrder.ecn = 'ARCA' AND tempOrder.ecn_suffix IS NULL then
			msg_content = msg_content || '-50=2';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-46=' || tempOrder.order_id;
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-44=' || shares_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-43=' || (price_array[i]*100);
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-42=' || '2';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-29=' || '99999';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-28=' || 'R';
		ELSIF tempOrder.ecn = 'ARCA' AND tempOrder.ecn_suffix = 'FIX' then
			msg_content = msg_content || '35=8';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '11=' || tempOrder.order_id;
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '38=' || shares_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '44=' || price_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '17=' || '99999';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '9730=' || 'R';
		ELSIF tempOrder.ecn = 'OUCH' THEN
			msg_content = msg_content || '-13=E';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-12=' || tempOrder.order_id;
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-10=' || shares_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-8=' || price_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-2=' || 'R';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-29=' || '99999';
			msg_content = msg_content || E'\001';
		ELSIF tempOrder.ecn = 'RASH' THEN
			msg_content = msg_content || '-13=E';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-12=' || tempOrder.order_id;
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-10=' || shares_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-8=' || price_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-2=' || 'R';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-29=' || '99999';
			msg_content = msg_content || E'\001';
		ELSIF tempOrder.ecn = 'NYSE' THEN
			msg_content = msg_content || '-50=81';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-10=' || shares_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-43=' || (price_array[i]*100);
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-42=' || '2';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-29=' || '99999';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-28=' || '1';
		ELSIF tempOrder.ecn = 'BATZ' THEN
			msg_content = msg_content || '-50=11';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-81=' || shares_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-83=' || price_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-28=' || 'R';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-29=' || '99999';
			msg_content = msg_content || E'\001';
		ELSIF tempOrder.ecn = 'EDGX' THEN
			msg_content = msg_content || '-13=E';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-10=' || shares_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-8=' || (price_array[i]);
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-2=' || '6';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-29=' || '99999';
			msg_content = msg_content || E'\001';
		END IF;
		
		msg_content = msg_content || E'\001';
		id_current = id_current + 1;
		exce_id_tmp = '99999' || '_' || id_current;
		
		-- Calculate fill fee
		SELECT calculate_total_fee(price_array[i], shares_array[i], substring(tempOrder.side from 1 for 1), liquidity, tempOrder.ecn) into totalFee;
		
		INSERT INTO fills(oid, shares, price, time_stamp, processed_time_stamp, msgseq_num, origclord_id, exec_id, msg_content, liquidity,  manual_update, total_fee)
			VALUES(oid_t, shares_array[i], price_array[i], tempOrder.time_stamp, tempOrder.time_stamp, tempOrder.msgseq_num, tempOrder.order_id, exce_id_tmp, msg_content, liquidity, 1, totalFee);
		
		i = i + 1;
	END LOOP;
	
	IF total_fill_shares < tempOrder.shares THEN
		UPDATE new_orders SET status = 'Lived' where oid = oid_t;
		UPDATE new_orders SET left_shares = (tempOrder.shares - total_fill_shares) where oid = oid_t;
	ELSE
		UPDATE new_orders SET status = 'Closed' where oid = oid_t;
		UPDATE new_orders SET left_shares = 0 where oid = oid_t;
	END IF;
	
	UPDATE trading_status SET hung_order = 1;
	
	return 1;
END
$$;


ALTER FUNCTION public.insert_fill_hung_order(oid_t integer, list_shares text, list_price text) OWNER TO postgres;

COMMIT;
