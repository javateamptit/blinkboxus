-- **********************************************************
-- 	SRCIPTS TO UPGRADE DATABASE
-- **********************************************************
BEGIN;
-- Add bbo_id to new_orders and bbo_infos
alter table new_orders add column bbo_id integer;
alter table bbo_infos add column bbo_id integer;
alter table trade_results add column timestamp integer;

-- Update view: vw_entry_delays
DROP VIEW IF EXISTS vw_entry_delays;
CREATE OR REPLACE VIEW vw_entry_delays AS 
 SELECT DISTINCT new_orders.oid, new_orders.time_stamp, max(split_part(raw_crosses.raw_content::text, ' '::text, 8)) AS quote_time_stamp, new_orders.date
   FROM new_orders
   JOIN raw_crosses ON raw_crosses.date = new_orders.date AND raw_crosses.ts_id = new_orders.ts_id AND raw_crosses.ts_cid = new_orders.cross_id
  WHERE raw_crosses.trade_id > 0 AND raw_crosses.raw_content ~~ '3%'::text AND new_orders.order_id <> 0 AND new_orders.ts_id <> -1 AND new_orders.entry_exit = 'Entry'::bpchar
  GROUP BY new_orders.oid, new_orders.date, new_orders.time_stamp
  ORDER BY new_orders.oid, new_orders.time_stamp, max(split_part(raw_crosses.raw_content::text, ' '::text, 8)), new_orders.date;
ALTER TABLE vw_entry_delays OWNER TO postgres;

-- Remove old function
DROP FUNCTION _check_new_quotes(character, integer);

-- Create new view: vw_trade_view
DROP VIEW IF EXISTS vw_trade_view;
CREATE OR REPLACE VIEW vw_trade_view AS 
        ( SELECT DISTINCT tr.date, tr.as_cid, tr.ts_cid, tr.ts_id, tr.symbol, tr.bought_shares, tr.sold_shares, tr.total_bought, tr.total_sold, tr.trade_id, 0 AS trade_type, '20 '::text || bi.content AS raw_content, bi.bbo_id, bi.id, tr."timestamp"
           FROM trade_results tr
      LEFT JOIN bbo_infos bi ON bi.date = tr.date AND bi.cross_id = tr.ts_cid AND bi.ts_id = tr.ts_id
     ORDER BY bi.id, tr.date, tr.as_cid, tr.ts_cid)
UNION 
        ( SELECT DISTINCT tr.date, tr.as_cid, tr.ts_cid, tr.ts_id, tr.symbol, tr.bought_shares, tr.sold_shares, tr.total_bought, tr.total_sold, tr.trade_id, rc.trade_type, rc.raw_content, (-1) AS bbo_id, rc.id + 100000000 AS id, tr."timestamp"
           FROM trade_results tr
      LEFT JOIN raw_crosses rc ON rc.date = tr.date AND rc.as_cid = tr.as_cid AND rc.ts_cid = tr.ts_cid AND rc.ts_id = tr.ts_id
     ORDER BY rc.id + 100000000, tr.date, tr.as_cid, tr.ts_cid)
  ORDER BY 2, 14;

ALTER TABLE vw_trade_view
  OWNER TO postgres;

  
COMMIT;
-- -------------------------------------------------------
-- END
-- -------------------------------------------------------
