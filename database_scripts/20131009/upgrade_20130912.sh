#!/bin/sh

# DB scripts
db_script=upgrade_20131009.sql

# DB list
db_list=("201310" "201309" "201308" "201307" "201306" "201305" "201304" "201303" "201302" "201301" "201212" "201211" "201210" "201209" "201208" "201207" "201206")
db_len=${#db_list[@]}

# use for loop read all databases
for (( i=0; i<${db_len}; i++ ));
do
	db_name="eaa10_${db_list[$i]}"
	
	echo "Upgrading $db_script on $db_name ..."
	cmd=`psql -U postgres $db_name -f $db_script`
	echo "Result: $cmd"
	if [ -n "$cmd" ];
	then
		if [[ "$cmd" == *ROLLBACK* ]]
		then
			echo "Fail! Skiped db script $db_script"
			exit;
		else
			echo "Successful upgrade $db_name"
		fi
	else
		echo "Fail! Skiped db script $db_script"
		exit;
	fi
done
