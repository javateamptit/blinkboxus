BEGIN;

--
-- Name: vw_exit_delays; Type: VIEW; Schema: public; Owner: postgres
--
DROP VIEW vw_exit_delays;

CREATE VIEW vw_exit_delays AS
    SELECT exit.id, exit.date, cause.order_id, date_part('microseconds'::text, (exit.time_stamp - cause.time_stamp)) AS delay, cause.ecn, cause.ecn_suffix, exit.time_stamp FROM (((SELECT new_orders.date, new_orders.order_id, new_orders.ts_id, new_orders.cross_id, order_rejects.entry_exit_ref, order_rejects.time_stamp, new_orders.ecn, new_orders.ecn_suffix FROM (new_orders JOIN order_rejects ON ((new_orders.oid = order_rejects.oid))) UNION SELECT new_orders.date, new_orders.order_id, new_orders.ts_id, new_orders.cross_id, cancels.entry_exit_ref, cancels.time_stamp, new_orders.ecn, new_orders.ecn_suffix FROM (new_orders JOIN cancels ON ((new_orders.oid = cancels.oid)))) UNION SELECT new_orders.date, new_orders.order_id, new_orders.ts_id, new_orders.cross_id, fills.entry_exit_ref, fills.time_stamp, new_orders.ecn, new_orders.ecn_suffix FROM (new_orders JOIN fills ON ((new_orders.oid = fills.oid)))) cause JOIN (SELECT new_orders.date, new_orders.ts_id, new_orders.cross_id, new_orders.entry_exit_ref, min(new_orders.time_stamp) AS time_stamp, min(new_orders.id) AS id FROM new_orders WHERE ((new_orders.ts_id <> (-1)) AND (new_orders.entry_exit = 'Exit'::bpchar)) GROUP BY new_orders.ts_id, new_orders.cross_id, new_orders.entry_exit_ref, new_orders.date) exit ON (((((cause.date = exit.date) AND (cause.ts_id = exit.ts_id)) AND (cause.cross_id = exit.cross_id)) AND (cause.entry_exit_ref = exit.entry_exit_ref)))) ORDER BY exit.id;


ALTER TABLE public.vw_exit_delays OWNER TO postgres;

--
-- Name: vw_cancel_delays; Type: VIEW; Schema: public; Owner: postgres
--
DROP VIEW vw_cancel_delays;

CREATE VIEW vw_cancel_delays AS
    SELECT c.id, n.entry_exit, date_part('microseconds'::text, (c.time_stamp - a.time_stamp)) AS cancel_delay, n.date, c.time_stamp FROM ((new_orders n JOIN order_acks a ON ((a.oid = n.oid))) JOIN cancels c ON ((c.oid = n.oid))) WHERE ((n.ts_id <> (-1)) AND (n.order_id <> 0)) ORDER BY c.id;


ALTER TABLE public.vw_cancel_delays OWNER TO postgres;

--
-- Name: vw_fill_delays; Type: VIEW; Schema: public; Owner: postgres
--
DROP VIEW vw_fill_delays;

CREATE VIEW vw_fill_delays AS
    SELECT f.fill_id, n.entry_exit, date_part('microseconds'::text, (f.time_stamp - a.time_stamp)) AS fill_delay, n.date, n.ecn, n.ecn_suffix, n.order_id, f.time_stamp FROM ((new_orders n JOIN order_acks a ON ((a.oid = n.oid))) JOIN fills f ON ((f.oid = n.oid))) WHERE ((n.ts_id <> (-1)) AND (n.order_id <> 0)) ORDER BY f.fill_id, n.date;


ALTER TABLE public.vw_fill_delays OWNER TO postgres;

--
-- Name: vw_ack_delays; Type: VIEW; Schema: public; Owner: postgres
--
DROP VIEW vw_ack_delays;

CREATE VIEW vw_ack_delays AS
    SELECT new_orders.oid, new_orders.entry_exit, new_orders.date, date_part('microseconds'::text, (order_acks.time_stamp - new_orders.time_stamp)) AS ack_delay, new_orders.ecn, new_orders.ecn_suffix, new_orders.order_id, order_acks.time_stamp FROM (new_orders JOIN order_acks ON ((order_acks.oid = new_orders.oid))) WHERE ((new_orders.ts_id <> (-1)) AND (new_orders.order_id <> 0)) ORDER BY new_orders.oid, new_orders.date;


ALTER TABLE public.vw_ack_delays OWNER TO postgres;

COMMIT;