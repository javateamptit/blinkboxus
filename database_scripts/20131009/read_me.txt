* Download all scripts to upgrade:
- Please copy the folder "20131009" to the EAA database host
- Change into "20131009" directory (on the EAA database host)

* The update is needed to run with new release, all databases will upgrade (eaa10_201310 -> eaa10_201206):
1) To upgrade, please run the command:
	$chmod 744 upgrade_20131009.sh
	$./upgrade_20131009.sh

