BEGIN;

DROP VIEW IF EXISTS vw_entry_delays;
CREATE OR REPLACE VIEW vw_entry_delays AS
    SELECT new_orders.oid, new_orders.order_id, new_orders.time_stamp, split_part((raw_crosses.raw_content)::text, ' '::text, 10) AS quote_time_stamp, new_orders.date, new_orders.ecn, split_part((raw_crosses.raw_content)::text, ' '::text, 11) AS reason FROM new_orders JOIN raw_crosses ON (raw_crosses.date = new_orders.date) AND (raw_crosses.ts_id = new_orders.ts_id) AND (raw_crosses.ts_cid = new_orders.cross_id) WHERE (raw_crosses.trade_id > 0) AND (raw_crosses.raw_content ~~ '2%'::text) AND (new_orders.order_id <> 0) AND (new_orders.ts_id <> -1) AND (new_orders.entry_exit = 'Entry'::bpchar) ORDER BY new_orders.oid, new_orders.time_stamp, split_part((raw_crosses.raw_content)::text, ' '::text, 10), new_orders.date;

ALTER TABLE public.vw_entry_delays OWNER TO postgres;

CREATE TYPE trade_result_statistic AS (
  count_miss integer,
  count_repriced integer,
  count_stuck_loss integer,
  count_stuck_profit integer,
  count_loss integer,
  count_profit_under integer,
  count_profit_above integer,
  count_total integer,
  shares_traded_miss integer,
  shares_traded_repriced integer,
  shares_traded_stuck_loss integer,
  shares_traded_stuck_profit integer,
  shares_traded_loss integer,
  shares_traded_profit_under integer,
  shares_traded_profit_above integer,
  shares_traded_total integer,
  pm_shares_traded_miss integer,
  pm_shares_traded_repriced integer,
  pm_shares_traded_stuck_loss integer,
  pm_shares_traded_stuck_profit integer,
  pm_shares_traded_loss integer,
  pm_shares_traded_profit_under integer,
  pm_shares_traded_profit_above integer,
  pm_shares_traded_total integer,
  expected_miss double precision,
  expected_repriced double precision,
  expected_stuck_loss double precision,
  expected_stuck_profit double precision,
  expected_loss double precision,
  expected_profit_under double precision,
  expected_profit_above double precision,
  expected_total double precision,
  real_miss double precision,
  real_stuck_loss double precision,
  real_stuck_profit double precision,
  real_loss double precision,
  real_profit_under double precision,
  real_profit_above double precision,
  real_total double precision,
  pm_miss double precision,
  pm_stuck_loss double precision,
  pm_stuck_profit double precision,
  pm_loss double precision,
  pm_profit_under double precision,
  pm_profit_above double precision,
  pm_total double precision,
  launch_shares_miss integer,
  launch_shares_repriced integer,
  launch_shares_stuck_loss integer,
  launch_shares_stuck_profit integer,
  launch_shares_loss integer,
  launch_shares_profit_under integer,
  launch_shares_profit_above integer,
  launch_shares_total integer
);


CREATE OR REPLACE VIEW vwtrade_results_statistics AS
  SELECT t.expected_shares, t.expected_pl, t.bought_shares, t.total_bought, t.sold_shares, t.total_sold, t.left_stuck_shares, t.as_cid, t.ts_cid, t.ts_id, t.trade_id, t.symbol, t.date, t.timestamp, t.spread, t.ts_total_fee, t.pm_total_fee, t.ts_profit_loss, t.pm_profit_loss, l.shares AS launch_shares 
  FROM trade_results t
  LEFT JOIN (
    SELECT a.date, a.ts_id, a.ts_cid, a.ecn_launch, a.shares
    FROM ask_bids a
    LEFT JOIN new_orders n ON n.date = a.date AND n.ts_id = a.ts_id AND n.cross_id = a.ts_cid AND n.ecn_launch = a.ecn_launch 
    WHERE entry_exit = 'Entry'
    GROUP BY a.date, a.ts_id, a.ts_cid, a.ecn_launch, a.shares) l
  ON l.date = t.date AND l.ts_id = t.ts_id AND l.ts_cid = t.ts_cid;

ALTER TABLE public.vwtrade_results_statistics OWNER TO postgres; 


DROP VIEW IF EXISTS vwtrade_results_twt2;
CREATE OR REPLACE VIEW vwtrade_results_twt2 AS
  SELECT t.expected_shares, t.expected_pl, t.bought_shares, t.total_bought, t.sold_shares, t.total_sold, t.left_stuck_shares, t.as_cid, t.ts_cid, t.ts_id, t.trade_id, t.symbol, t.date, t.ts_profit_loss, n.ecn, n.ecn_launch, n.time_stamp, n.price, n.ack_price, n.status, n.entry_exit, n.left_shares, n.shares, n.is_iso, t.pm_profit_loss, t.spread, t.ts_profit_loss + t.pm_profit_loss - t.ts_total_fee - t.pm_total_fee AS net_pl, t.pm_profit_loss - t.pm_total_fee AS pm_net_pl, n.launch_shares, ts_total_fee, pm_total_fee
  FROM trade_results t
  JOIN ( 
    SELECT n1.date, n1.ts_id, n1.cross_id,
      CASE
          WHEN n1.ecn = 'BATZ'::bpchar AND n1.routing_inst = 'RL2'::bpchar THEN 'BZSR'::bpchar
          ELSE n1.ecn
      END AS ecn, n1.time_stamp, n1.price, n1.ack_price, n1.status, n1.entry_exit, n1.left_shares, n1.shares, n1.is_iso, n2.ecn_launch, n1.routing_inst, n2.launch_shares
    FROM new_orders n1
    LEFT JOIN (
      SELECT DISTINCT no.date, no.ts_id, no.cross_id, no.ecn_launch, a.shares AS launch_shares
      FROM new_orders no      
      LEFT JOIN ask_bids a
      ON no.date = a.date AND no.ts_id = a.ts_id AND no.cross_id = a.ts_cid AND no.ecn_launch = a.ecn_launch
      WHERE no.entry_exit = 'Entry'::bpchar AND no.ts_id <> (-1)
    ) n2 
    ON n1.date = n2.date AND n1.ts_id = n2.ts_id AND n1.cross_id = n2.cross_id
  ) n 
  ON t.date = n.date AND t.ts_id = n.ts_id AND t.ts_cid = n.cross_id;
  

ALTER TABLE public.vwtrade_results_twt2 OWNER TO postgres; 


-- Trade Results
--
-- Name: vwack_and_fills; Type: VIEW; Schema: public; Owner: postgres
--
DROP VIEW IF EXISTS vwack_and_fills;
CREATE OR REPLACE VIEW vwack_and_fills AS
  SELECT n2.ts_id, n2.cross_id, n2.ecn_launch, n2.order_id,  
    CASE
      WHEN n2.ecn = 'BATZ'::bpchar AND n2.routing_inst = 'RL2'::bpchar THEN 'BZSR'::bpchar
      ELSE n2.ecn
    END AS ecn, n2.symbol, n2.date, n2.time_stamp, n2.ack_time_stamp, n2.fill_time_stamp, substr((n2.ack_time_stamp - n2.time_stamp)::text, 8, 9)::double precision * 1000000::double precision AS ack_delay, substr((n2.fill_time_stamp - n2.ack_time_stamp)::text, 8, 9)::double precision * 1000000::double precision AS fill_delay, n2.is_iso
  FROM ( 
    SELECT n1.ts_id, n1.cross_id, n1.order_id, n1.ecn, n1.ecn_launch, n1.date, n1.time_stamp, a.time_stamp AS ack_time_stamp, min(f.time_stamp) AS fill_time_stamp, n1.symbol, n1.is_iso, n1.routing_inst
    FROM new_orders n1
    JOIN order_acks a ON a.oid = n1.oid
    JOIN fills f ON f.oid = n1.oid
    WHERE n1.left_shares < n1.shares AND n1.ts_id <> (-1)
    GROUP BY n1.ts_id, n1.cross_id, n1.order_id, ecn, n1.ecn_launch, n1.date, n1.time_stamp, a.time_stamp, n1.symbol, n1.is_iso, n1.routing_inst) n2;

ALTER TABLE public.vwack_and_fills OWNER TO postgres;



-- ORDER VIEW
-- Name: vwtrade_breaks; Type: VIEW; Schema: public; Owner: postgres
--  

DROP VIEW IF EXISTS vwtrade_breaks;
CREATE OR REPLACE VIEW vwtrade_breaks AS
  SELECT new_orders.oid, new_orders.symbol, 
    CASE
      WHEN new_orders.ecn = 'BATZ' AND new_orders.routing_inst = 'RL2' THEN 'BZSR'
      ELSE new_orders.ecn
    END AS ecn, new_orders.date, fills.origclord_id::integer AS cl_order_id, new_orders.shares AS shares_order, new_orders.side AS action, fills.shares AS filled_shares, new_orders.price AS price_order, fills.price AS filled_price, fills.fill_id, fills.time_stamp AS trade_time
  FROM new_orders
  JOIN fills ON new_orders.oid = fills.oid
  ORDER BY new_orders.oid;

ALTER TABLE public.vwtrade_breaks OWNER TO postgres; 


DROP FUNCTION IF EXISTS cal_filter_trade_statistic_byecn2_twt2(date, date, time without time zone, time without time zone, text, double precision, text, integer, integer, text, text, text, text, integer, text);
CREATE OR REPLACE FUNCTION cal_filter_trade_statistic_byecn2_twt2(to_date date, from_date date, time_from time without time zone, time_to time without time zone, share_size text, expected_profit double precision, symb text, bt_shares integer, sd_shares integer, ecn_filter text, ts_id text, cecn text, is_iso text, exclusive_val integer, cross_size text, launch_shares text) RETURNS trade_result_statistic
    LANGUAGE plpgsql
    AS $$
declare
  trade_result record;
  summary trade_result_statistic;
  shares_filled integer;
  shares_stuck integer;
  profit_loss double precision;
 --statistic missed trade 
  count_ouch_missed_trade integer;
  sum_shares_traded_miss integer;
  sum_launch_shares_miss integer;
  sum_ouch_expected_pl double precision;
  missed_trade_record_of_ouch record;
  str_sql text;
  str_sql1 text;
  str_sql2 text;  
  str_sql_no text;
  str_sql_no_repriced text;
  str_sql_repriced text;
  str_sql_multi_orders text;
  str_sql_tmp text;
  tmp_str text;
  tmp_ASCid text;
  new_orders_table text;
  share_size_condition text;
  launch_shares_condition text;
   -- Filter Entry or Exit
  entry_exit_filter text; 
  ecn_filter_conditions text;
  -- Check number TS
  check_ts integer;
  check_ecn_entry_exit integer;
  
begin  
  -- Initialize
  summary.count_miss := 0;
  summary.count_repriced := 0;
  summary.count_stuck_loss := 0;
  summary.count_stuck_profit := 0;
  summary.count_loss := 0;
  summary.count_profit_under := 0;
  summary.count_profit_above := 0;
  summary.count_total := 0;
  summary.shares_traded_miss := 0;
  summary.shares_traded_repriced := 0;
  summary.shares_traded_stuck_loss := 0;
  summary.shares_traded_stuck_profit := 0;
  summary.shares_traded_loss := 0;
  summary.shares_traded_profit_under := 0;
  summary.shares_traded_profit_above := 0;
  summary.shares_traded_total := 0;
  summary.pm_shares_traded_miss := 0;
  summary.pm_shares_traded_repriced := 0;
  summary.pm_shares_traded_stuck_loss := 0;
  summary.pm_shares_traded_stuck_profit := 0;
  summary.pm_shares_traded_loss := 0;
  summary.pm_shares_traded_profit_under := 0;
  summary.pm_shares_traded_profit_above := 0;
  summary.pm_shares_traded_total := 0;
  
  summary.expected_miss := 0.00;
  summary.expected_repriced := 0.00;
  summary.expected_stuck_loss := 0.00;
  summary.expected_stuck_profit := 0.00;
  summary.expected_loss := 0.00;
  summary.expected_profit_under := 0.00;
  summary.expected_profit_above := 0.00;
  summary.expected_total := 0.00;
  summary.real_miss := 0.00;
  summary.real_stuck_loss := 0.00;
  summary.real_stuck_profit := 0.00;
  summary.real_loss = 0.00;
  summary.real_profit_under := 0.00;
  summary.real_profit_above := 0.00;
  summary.real_total := 0.00;
  
  summary.pm_miss := 0.00;
  summary.pm_stuck_loss := 0.00;
  summary.pm_stuck_profit := 0.00;
  summary.pm_loss = 0.00;
  summary.pm_profit_under := 0.00;
  summary.pm_profit_above := 0.00;
  summary.pm_total := 0.00;
  
  summary.launch_shares_miss := 0;
  summary.launch_shares_repriced := 0;
  summary.launch_shares_stuck_loss := 0;
  summary.launch_shares_stuck_profit := 0;
  summary.launch_shares_loss := 0;
  summary.launch_shares_profit_under := 0; 
  summary.launch_shares_profit_above := 0;
  summary.launch_shares_total := 0;
  
  entry_exit_filter := '';-- Filter Entry or Exit
  check_ts := 0;
  check_ecn_entry_exit := 0;
  
  share_size_condition := '';
  IF strpos(share_size,'and') <> 0 THEN  -- Multi Detected Shares
    share_size_condition := share_size;
  ELSE 
    IF share_size <> '-100' THEN 
      share_size_condition := ' and expected_shares >='|| share_size ||'';
    ELSE -- Filter share size < 100 
      share_size_condition := ' and expected_shares < 100';   
    END IF;
  END IF;
   
  launch_shares_condition := '';
  IF strpos(launch_shares,'and') <> 0 THEN  -- Multi  Launch Shares
    launch_shares_condition :=  launch_shares;
  ELSE 
    IF launch_shares <> '-100' THEN 
      launch_shares_condition := ' and launch_shares >='||  launch_shares ||'';
    ELSE -- Filter Launch Shares < 100 
      launch_shares_condition := ' and launch_shares < 100';   
    END IF;
  END IF;
  
  IF strpos(ts_id,',') <> 0 THEN -- Multi TS
     check_ts := 1;
  END IF;
  
  new_orders_table := '(SELECT n1.date, n1.ts_id, n1.cross_id, n1.ecn, n1.time_stamp, n1.price, n1.ack_price, n1.status, n1.entry_exit, n1.left_shares, n1.shares, n1.is_iso, n1. routing_inst, n2.ecn_launch ' ||
            'FROM new_orders n1 '
            'LEFT JOIN (SELECT DISTINCT date, ts_id, cross_id, ecn_launch '
            '      FROM new_orders '
            '      WHERE entry_exit = ''Entry'' AND ts_id <> -1) n2 '
            'ON n1.date = n2.date AND n1.ts_id = n2.ts_id AND n1.cross_id = n2.cross_id) AS tmp ';
  
  str_sql :='';
  str_sql_no :='SELECT DISTINCT (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + cross_id) as tmpcol FROM ' || new_orders_table || ' WHERE date >='''|| to_date || ''' and date <='''|| from_date ||'''';
  str_sql_no :=str_sql_no || ' and time_stamp between ''' || time_from || ''' and '''|| time_to || '''';
  --filter by ts, ecn launch
  IF check_ts = 0 THEN
    IF ts_id <> '0' AND ts_id <> '-1' THEN
      str_sql_no:=str_sql_no || ' and ts_id + 1 =' || ts_id;
    ELSIF ts_id = '-1' THEN
      str_sql_no:=str_sql_no || ' and bought_shares <> sold_shares ';
    END IF;
  ELSE
    IF strpos(ts_id,'-1') = 0 THEN 
      str_sql_no:=str_sql_no || ' and ts_id + 1 in ('|| ts_id ||')';
    ELSE 
      str_sql_no:=str_sql_no || ' and ((ts_id + 1 in ('|| ts_id ||')) OR (bought_shares <> sold_shares))';
    END IF;
  END IF;
  
  IF cecn <> '0' THEN
    str_sql_no:=str_sql_no || ' and ecn_launch =''' || cecn ||'''';
  END IF;
  -- Check: Multi Select ECN Entry/Exit
  ecn_filter_conditions := '';
  IF strpos(ecn_filter,'and') <> 0 THEN 
    check_ecn_entry_exit := 1;
    ecn_filter_conditions := ecn_filter;
  ELSE 
    IF ecn_filter = 'arcaen' THEN
      str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Entry''' ;
      entry_exit_filter:= 'Entry';
    ELSIF ecn_filter = 'arcaex' THEN
      str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Exit''' ;
      entry_exit_filter:= 'Exit';
    ELSIF ecn_filter = 'ouchen' THEN
      str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Entry''';
      entry_exit_filter:= 'Entry';
    ELSIF ecn_filter = 'ouchex' THEN
      str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Exit''';
      entry_exit_filter:= 'Exit';
    ELSIF ecn_filter = 'rashen' THEN
      str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Entry''';
      entry_exit_filter:= 'Entry';
    ELSIF ecn_filter = 'rashex' THEN
      str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Exit''';
      entry_exit_filter:= 'Exit';    
    ELSIF ecn_filter = 'batzen' THEN
      str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Entry''';
      entry_exit_filter:= 'Entry';    
    ELSIF ecn_filter = 'batzex' THEN
      str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Exit'' and routing_inst != ''RL2''';
      entry_exit_filter:= 'Exit';
    ELSIF ecn_filter = 'bzsrex' THEN
      str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Exit'' and routing_inst = ''RL2''';
      entry_exit_filter:= 'Exit';
    ELSIF ecn_filter = 'rabxen' THEN
      str_sql_no:=str_sql_no || ' and ecn = ''RABX'' and entry_exit=''Entry''';
      entry_exit_filter:= 'Entry';
    ELSIF ecn_filter = 'rabxex' THEN
      str_sql_no:=str_sql_no || ' and ecn = ''RABX'' and entry_exit=''Exit''';
      entry_exit_filter:= 'Exit';
    ELSIF ecn_filter = 'nyseen' THEN
      str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Entry''';
      entry_exit_filter:= 'Entry';
    ELSIF ecn_filter = 'nyseex' THEN
      str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Exit''';
      entry_exit_filter:= 'Exit';
    ELSIF ecn_filter = 'edgxen' THEN
      str_sql_no:=str_sql_no || ' and ecn = ''EDGX'' and entry_exit=''Entry''';
      entry_exit_filter:= 'Entry';
    ELSIF ecn_filter = 'edgxex' THEN
      str_sql_no:=str_sql_no || ' and ecn = ''EDGX'' and entry_exit=''Exit''';
      entry_exit_filter:= 'Exit';  
    ELSIF ecn_filter = 'edgaen' THEN
      str_sql_no:=str_sql_no || ' and ecn = ''EDGA'' and entry_exit=''Entry''';
      entry_exit_filter:= 'Entry';
    ELSIF ecn_filter = 'edgaex' THEN
      str_sql_no:=str_sql_no || ' and ecn = ''EDGA'' and entry_exit=''Exit''';
      entry_exit_filter:= 'Exit';
    ELSIF ecn_filter = 'oubxen' THEN
      str_sql_no:=str_sql_no || ' and ecn = ''OUBX'' and entry_exit=''Entry''';
      entry_exit_filter:= 'Entry';
    ELSIF ecn_filter = 'oubxex' THEN
      str_sql_no:=str_sql_no || ' and ecn = ''OUBX'' and entry_exit=''Exit''';
      entry_exit_filter:= 'Exit';
    ELSIF ecn_filter = 'batyen' THEN
      str_sql_no:=str_sql_no || ' and ecn = ''BATY'' and entry_exit=''Entry''';
      entry_exit_filter:= 'Entry';    
    ELSIF ecn_filter = 'batyex' THEN
      str_sql_no:=str_sql_no || ' and ecn = ''BATY'' and entry_exit=''Exit''';
      entry_exit_filter:= 'Exit';
    ELSIF ecn_filter = 'psxen' THEN
      str_sql_no:=str_sql_no || ' and ecn = ''PSX'' and entry_exit=''Entry''';
      entry_exit_filter:= 'Entry';    
    ELSIF ecn_filter = 'psxex' THEN
      str_sql_no:=str_sql_no || ' and ecn = ''PSX'' and entry_exit=''Exit''';
      entry_exit_filter:= 'Exit';  
    END IF;
  END IF;

  --Filter by ISO Odrer
  IF is_iso = '1' THEN
    str_sql_no:=str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
  END IF;
  -- tmp_str := str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
  
  IF is_iso = '2' THEN
    str_sql_no:=str_sql_no || ' and (is_iso <> ''Y'')';
  END IF;
  
  -- filter by Symbol, Bought Shares, Sold Shares
  IF symb <>'' THEN
    str_sql:=' and symbol='''|| symb ||'''';
  END IF;
  IF bt_shares > 0 THEN
    str_sql:=str_sql || ' and bought_shares >=' || bt_shares;
  END IF;
  IF sd_shares > 0 THEN
    str_sql:=str_sql || ' and sold_shares >=' || sd_shares;
  END IF;
  --filter by ts
  
  IF check_ts = 0 THEN
    IF ts_id <> '0' AND ts_id <> '-1' THEN 
      str_sql:=str_sql || ' and ts_id + 1 =' || ts_id;
    ELSIF ts_id = '-1' THEN
      str_sql := str_sql || ' and bought_shares <> sold_shares ';
    END IF;
  ELSE
    IF strpos(ts_id,'-1') = 0 THEN 
      str_sql:=str_sql || ' and ts_id + 1 in ('|| ts_id ||')';
    ELSE 
      str_sql:=str_sql || ' and ((ts_id + 1 in ('|| ts_id ||')) OR (bought_shares <> sold_shares))';
    END IF;
     
  END IF;
  IF cross_size <> '' then
    str_sql := str_sql || ' and ' || cross_size;
  END IF;
  IF check_ecn_entry_exit = 0 AND ecn_filter <> '0' AND exclusive_val = 1 THEN
    str_sql_multi_orders := get_query_string_array_id_multi_orders(to_date, from_date, entry_exit_filter);
    str_sql := str_sql || ' and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) not in (' || str_sql_multi_orders || ')';
  END IF;
  
  --Calculate repriced
  IF ((ecn_filter = 'oubxen' OR ecn_filter = 'oubxex' OR ecn_filter = 'psxen' OR ecn_filter = 'psxex' OR ecn_filter = '0') AND  (cecn = 'NDAQ' OR cecn = 'NDBX' OR cecn = 'PSX' OR cecn = '0')) THEN
    IF bt_shares = 0 AND sd_shares = 0 THEN
    
    str_sql_no_repriced:=str_sql_no || ' and (ecn =''OUCH'' OR ecn =''OUBX'') and price != ack_price ' ;
    str_sql_repriced:=str_sql || '  and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) in ('|| str_sql_no_repriced ||')';  
    str_sql1:='SELECT count(*) as count_missed, sum(expected_pl)as sum_expected_pl, sum(bought_shares) as sum_bought_shares, sum(sold_shares) as sum_sold_shares, sum(launch_shares) as sum_launch_shares FROM vwtrade_results_statistics WHERE bought_shares = 0 and sold_shares = 0 and date >='''|| to_date || ''' and date <='''|| from_date ||''''|| share_size_condition || ecn_filter_conditions || launch_shares_condition;
    IF (expected_profit != 0.0) THEN
      str_sql1 := str_sql1 || ' and expected_pl >= ' || expected_profit;
    END IF;
    str_sql1:=str_sql1 ||str_sql_repriced;
    for missed_trade_record_of_ouch in EXECUTE str_sql1 loop
    count_ouch_missed_trade := missed_trade_record_of_ouch.count_missed;
    sum_ouch_expected_pl := missed_trade_record_of_ouch.sum_expected_pl;
    sum_shares_traded_miss := missed_trade_record_of_ouch.sum_bought_shares + missed_trade_record_of_ouch.sum_sold_shares;
    sum_launch_shares_miss := missed_trade_record_of_ouch.sum_launch_shares;
    end loop;

   IF count_ouch_missed_trade > 0 THEN
    summary.count_repriced := count_ouch_missed_trade;
    summary.expected_repriced := sum_ouch_expected_pl;
    summary.count_miss := -count_ouch_missed_trade;
    summary.expected_miss :=  -sum_ouch_expected_pl;
    summary.shares_traded_miss := -sum_shares_traded_miss;
    summary.shares_traded_repriced := sum_shares_traded_miss;
    summary.launch_shares_miss := -sum_launch_shares_miss;
    summary.launch_shares_repriced := sum_launch_shares_miss;
   END IF;

   END IF;
  END IF;
 --calculate for trade_statistics table  
 str_sql:=str_sql || '  and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) in ('|| str_sql_no ||')';
 str_sql2:='SELECT * FROM vwtrade_results_statistics WHERE date >='''|| to_date || ''' and date <='''|| from_date ||''''|| share_size_condition || ecn_filter_conditions || launch_shares_condition;
 IF (expected_profit != 0.0) THEN
  str_sql2 := str_sql2 || ' and expected_pl >= ' || expected_profit;
 END IF;
 
 str_sql2:=str_sql2 ||str_sql;
 
FOR trade_result in EXECUTE str_sql2 loop
    shares_filled := trade_result.bought_shares;

    IF (shares_filled > trade_result.sold_shares) THEN
      shares_filled := trade_result.sold_shares;
    END IF;

    shares_stuck := trade_result.bought_shares - trade_result.sold_shares;    

    IF (shares_stuck = 0) THEN
      profit_loss := trade_result.total_sold - trade_result.total_bought - trade_result.ts_total_fee;

      IF (shares_filled = 0) THEN
        summary.count_miss := summary.count_miss + 1;
        summary.shares_traded_miss := summary.shares_traded_miss + trade_result.bought_shares + trade_result.sold_shares;
        summary.launch_shares_miss := summary.launch_shares_miss + trade_result.launch_shares;
        summary.expected_miss := summary.expected_miss + trade_result.expected_pl;
      ELSE
        IF (profit_loss < 0) THEN
          summary.count_loss := summary.count_loss + 1;
          summary.shares_traded_loss := summary.shares_traded_loss + trade_result.bought_shares + trade_result.sold_shares;
          summary.launch_shares_loss := summary.launch_shares_loss + trade_result.launch_shares;
          summary.expected_loss := summary.expected_loss + trade_result.expected_pl;
          summary.real_loss := summary.real_loss + profit_loss;
        ELSE
          IF ( profit_loss::real < trade_result.expected_pl::real) THEN
            summary.count_profit_under := summary.count_profit_under + 1;
            summary.shares_traded_profit_under := summary.shares_traded_profit_under + trade_result.bought_shares + trade_result.sold_shares;
            summary.launch_shares_profit_under := summary.launch_shares_profit_under + trade_result.launch_shares;      
            summary.expected_profit_under := summary.expected_profit_under + trade_result.expected_pl;
            summary.real_profit_under := summary.real_profit_under + profit_loss;
          ELSE
            summary.count_profit_above := summary.count_profit_above + 1;
            summary.shares_traded_profit_above := summary.shares_traded_profit_above + trade_result.bought_shares + trade_result.sold_shares;
            summary.launch_shares_profit_above := summary.launch_shares_profit_above + trade_result.launch_shares;
            summary.expected_profit_above := summary.expected_profit_above + trade_result.expected_pl;
            summary.real_profit_above := summary.real_profit_above + profit_loss;
          END IF;
        END IF;
      END IF;
    ELSE
      --Calculate for stuck
      IF (trade_result.pm_profit_loss - trade_result.pm_total_fee < -0.0001) THEN
  --stuck_loss
     summary.count_stuck_loss := summary.count_stuck_loss + 1;
     summary.shares_traded_stuck_loss := summary.shares_traded_stuck_loss + trade_result.bought_shares + trade_result.sold_shares;
      summary.launch_shares_stuck_loss := summary.launch_shares_stuck_loss + trade_result.launch_shares;
     summary.expected_stuck_loss := summary.expected_stuck_loss + trade_result.expected_pl;
     summary.real_stuck_loss := summary.real_stuck_loss + trade_result.ts_profit_loss  + trade_result.pm_profit_loss - trade_result.ts_total_fee - trade_result.pm_total_fee;
     summary.pm_stuck_loss := summary.pm_stuck_loss + trade_result.pm_profit_loss - trade_result.pm_total_fee;
     IF trade_result.bought_shares < trade_result.sold_shares THEN
    summary.pm_shares_traded_stuck_loss := summary.pm_shares_traded_stuck_loss - trade_result.bought_shares + trade_result.sold_shares - trade_result.left_stuck_shares;
     ELSE
    summary.pm_shares_traded_stuck_loss := summary.pm_shares_traded_stuck_loss + trade_result.bought_shares - trade_result.sold_shares - trade_result.left_stuck_shares;
     END IF;
      ELSE
  --stuck_profit
     summary.count_stuck_profit := summary.count_stuck_profit + 1;
     summary.shares_traded_stuck_profit := summary.shares_traded_stuck_profit + trade_result.bought_shares + trade_result.sold_shares;
      summary.launch_shares_stuck_profit := summary.launch_shares_stuck_profit + trade_result.launch_shares;
     summary.expected_stuck_profit := summary.expected_stuck_profit + trade_result.expected_pl;
     summary.real_stuck_profit := summary.real_stuck_profit + trade_result.ts_profit_loss + trade_result.pm_profit_loss - trade_result.ts_total_fee - trade_result.pm_total_fee;
     summary.pm_stuck_profit := summary.pm_stuck_profit + trade_result.pm_profit_loss - trade_result.pm_total_fee;
     IF trade_result.bought_shares < trade_result.sold_shares THEN
    summary.pm_shares_traded_stuck_profit := summary.pm_shares_traded_stuck_profit - trade_result.bought_shares + trade_result.sold_shares - trade_result.left_stuck_shares;
     ELSE
    summary.pm_shares_traded_stuck_profit := summary.pm_shares_traded_stuck_profit + trade_result.bought_shares - trade_result.sold_shares - trade_result.left_stuck_shares;
     END IF;
      END IF;
    END IF;

    summary.count_total := summary.count_total +  1;
    summary.shares_traded_total := summary.shares_traded_total + trade_result.bought_shares + trade_result.sold_shares;
    summary.launch_shares_total := summary.launch_shares_total + trade_result.launch_shares;
    summary.expected_total := summary.expected_total + trade_result.expected_pl  ;
  end loop;
  
  summary.count_total := summary.count_total - summary.count_repriced;
  summary.shares_traded_total := summary.shares_traded_total - summary.shares_traded_repriced;
  summary.expected_total := summary.expected_total - summary.expected_repriced;
  summary.launch_shares_total := summary.launch_shares_total - summary.launch_shares_repriced;
  
  return summary;
end
$$;

ALTER FUNCTION public.cal_filter_trade_statistic_byecn2_twt2(to_date date, from_date date, time_from time without time zone, time_to time without time zone, share_size text, expected_profit double precision, symb text, bt_shares integer, sd_shares integer, ecn_filter text, ts_id text, cecn text, is_iso text, exclusive_val integer, cross_size text, launch_shares text) OWNER TO postgres;


--
-- Name: cal_filter_trade_statistic_byecn2_twt2_with_step(date, date, time without time zone, time without time zone, integer, double precision, text, integer, integer, text, text, text, integer, text, integer, text); Type: FUNCTION; Schema: public; Owner: postgres
--

DROP FUNCTION IF EXISTS cal_filter_trade_statistic_byecn2_twt2_with_step (date, date, time without time zone, time without time zone, text, double precision, text, integer, integer, text, text, text, integer, text, integer, text);
CREATE OR REPLACE FUNCTION cal_filter_trade_statistic_byecn2_twt2_with_step(to_date date, from_date date, time_from time without time zone, time_to time without time zone, share_size text, expected_profit double precision, symb text, bt_shares integer, sd_shares integer, ecn_filter text, ts_id text, cecn text, step integer, is_iso text, exclusive_val integer, cross_size text, launch_shares text) RETURNS SETOF trade_result_summary2_struct
    LANGUAGE plpgsql
    AS $$ 
  declare
  trade_result         record;
  summary         trade_result_summary2_struct;
  shares_filled         integer;
  shares_stuck         integer;
  profit_loss         double precision;
  --statistic missed trade 
  count_ouch_missed_trade     integer;
  sum_ouch_expected_pl       double precision;
  missed_trade_record_of_ouch     record;
  str_sql         text;
  str_sql_no         text;
  str_sql_tmp           text;
  str_sql_multi_orders text;
  tmp text;
  tmp_str text;
  min_time        "time";
  max_time        "time";
  temp_time         "time";
  t_current        "time";
  new_orders_table text;
  share_size_condition text;
  launch_shares_condition text;
   -- Filter Entry or Exit
  entry_exit_filter text;
  ecn_filter_conditions text;
   -- Check number TS
  check_ts integer;
  check_ecn_entry_exit integer;
  
begin  
            
  min_time := time_from;
  max_time := time_to;
  
  entry_exit_filter := '';  -- Filter Entry or Exit
  
  share_size_condition := '';
  IF strpos(share_size,'and') <> 0 THEN -- Multi Detected Shares
    share_size_condition := share_size;
  ELSE 
    IF share_size <> '-100' THEN 
      share_size_condition := ' and expected_shares >='|| share_size ||'';
    ELSE -- Filter share size < 100 
      share_size_condition := ' and expected_shares < 100';   
    END IF;
  END IF;
  
  -- Filter by Launch Shares
  launch_shares_condition := '';
  IF strpos(launch_shares,'and') <> 0 THEN -- Multi Launch Shares
    launch_shares_condition := launch_shares;
  ELSE 
    IF launch_shares <> '-100' THEN 
      launch_shares_condition := ' and launch_shares >='|| launch_shares ||'';
    ELSE -- Filter share size < 100 
      launch_shares_condition := ' and launch_shares < 100';   
    END IF;
  END IF;
  
  check_ts := 0;
  IF strpos(ts_id,',') <> 0 THEN -- Multi TS
    check_ts := 1;
  END IF;
  -- Check: Multi Select ECN Entry/Exit
  check_ecn_entry_exit := 0;
  ecn_filter_conditions := '';
  IF strpos(ecn_filter,'and') <> 0 THEN 
    check_ecn_entry_exit := 1;
    ecn_filter_conditions := ecn_filter;
  END IF; 
  
  WHILE min_time < max_time LOOP
    -- Initia
    
    summary.count_miss := 0;
    summary.count_repriced := 0;
    summary.count_stuck_loss := 0;
    summary.count_stuck_profit := 0;
    summary.count_loss := 0;
    summary.count_profit_under := 0;
    summary.count_profit_above := 0;    
    
    IF (step = 5) THEN
      t_current = min_time + interval '5 minutes';
    ELSIF (step = 15) THEN
      t_current = min_time + interval '15 minutes';
      
    ELSIF (step = 30) THEN
      t_current = min_time + interval '30 minutes';
    ELSIF (step = 60) THEN
      t_current = min_time + interval '60 minutes';
    END IF;
    --raise notice '1(%)', t_current;
    IF ((t_current > max_time) or (t_current = '00:00:00')) THEN
      t_current = max_time;
      --raise notice '2(%)', t_current;
    END IF; 
    temp_time = t_current - interval '1 microsecond';
    
    --Calculate repriced
   IF ((ecn_filter = 'oubxen' OR ecn_filter = 'oubxex' OR ecn_filter = 'psxen' OR ecn_filter = 'psxex' OR ecn_filter = '0') AND  (cecn = 'NDAQ' OR cecn = 'NDBX' OR cecn = 'PSX' OR cecn = '0')) THEN
    IF bt_shares = 0 AND sd_shares = 0 THEN
      --Select data from view "vwouch_missed_trades" and calculate total missed trade   
      str_sql:='SELECT count(*) as count_missed, sum(expected_pl)as sum_expected_pl FROM vwtrade_results_statistics WHERE date >='''|| to_date || ''' and date <='''|| from_date ||''''|| share_size_condition || ecn_filter_conditions || launch_shares_condition;
      IF (expected_profit != 0.0) THEN
        str_sql := str_sql || ' and expected_pl >= ' || expected_profit;
      END IF;
      str_sql := str_sql || ' and bought_shares = 0 and sold_shares = 0 ';
      
      --filter by ts when we get data from view vwtrade_results_statistics
      IF check_ts = 0 THEN
        IF ts_id <> '0' THEN 
          str_sql:=str_sql || ' and ts_id + 1 =' || ts_id;
        ELSIF ts_id = '-1' THEN
          str_sql := str_sql || ' and bought_shares <> sold_shares ';
        END IF;
      ELSE
        IF strpos(ts_id,'-1') = 0 THEN 
          str_sql:=str_sql || ' and ts_id + 1 in ('|| ts_id ||')';
        ELSE 
          str_sql:=str_sql || ' and ((ts_id + 1 in ('|| ts_id ||')) OR (bought_shares <> sold_shares))';
        END IF;
         
      END IF;
      IF cross_size <> '' then
        str_sql := str_sql || ' and ' || cross_size;
      END IF;
      
      str_sql_no :='SELECT (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + cross_id) as tmpcol FROM new_orders WHERE date >='''|| to_date || ''' and date <='''|| from_date ||'''';
      str_sql_no :=str_sql_no || ' and time_stamp between ''' || min_time || ''' and '''|| temp_time || '''';
      str_sql_no :=str_sql_no || ' and bought_shares = 0 and sold_shares = 0 and (ecn =''OUCH'' or ecn =''OUBX'') and price != ack_price ';
      
      --filter by ts, ecn_launch when we get data from table new_orders
      IF check_ts = 0 THEN
        IF ts_id <> '0' AND ts_id <> '-1' THEN
          str_sql_no:=str_sql_no || ' and ts_id + 1 =' || ts_id;
        ELSIF ts_id = '-1' THEN
          str_sql_no:=str_sql_no || ' and bought_shares <> sold_shares ';
        END IF;
      ELSE
        IF strpos(ts_id,'-1') = 0 THEN 
          str_sql_no:=str_sql_no || ' and ts_id + 1 in ('|| ts_id ||')';
        ELSE 
          str_sql_no:=str_sql_no || ' and ((ts_id + 1 in ('|| ts_id ||')) OR (bought_shares <> sold_shares))';
        END IF;
      END IF;
      
      IF cecn <> '0' THEN
        str_sql_no:=str_sql_no || ' and ecn_launch =''' || cecn ||'''';
      END IF;
      
      IF check_ecn_entry_exit = 1 THEN --Filter by multi ECN Entry/Exit
        ecn_filter_conditions := ecn_filter;
      ELSE   
        IF ecn_filter = 'arcaen' THEN
          str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Entry''' ;
          entry_exit_filter:= 'Entry';
        ELSIF ecn_filter = 'arcaex' THEN
          str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Exit''' ;
          entry_exit_filter:= 'Exit';
        ELSIF ecn_filter = 'ouchen' THEN
          str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Entry''';
          entry_exit_filter:= 'Entry';
        ELSIF ecn_filter = 'ouchex' THEN
          str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Exit''';
          entry_exit_filter:= 'Exit';
        ELSIF ecn_filter = 'rashen' THEN
          str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Entry''';
          entry_exit_filter:= 'Entry';
        ELSIF ecn_filter = 'rashex' THEN
          str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Exit''';
          entry_exit_filter:= 'Exit';        
        ELSIF ecn_filter = 'batzen' THEN
          str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Entry''';
          entry_exit_filter:= 'Entry';        
        ELSIF ecn_filter = 'batzex' THEN
          str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Exit'' and routing_inst != ''RL2''';
          entry_exit_filter:= 'Exit';
        ELSIF ecn_filter = 'bzsrex' THEN
          str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Exit'' and routing_inst = ''RL2''';
          entry_exit_filter:= 'Exit';  
        ELSIF ecn_filter = 'rabxen' THEN
          str_sql_no:=str_sql_no || ' and ecn = ''RABX'' and entry_exit=''Entry''';
          entry_exit_filter:= 'Entry';
        ELSIF ecn_filter = 'rabxex' THEN
          str_sql_no:=str_sql_no || ' and ecn = ''RABX'' and entry_exit=''Exit''';
          entry_exit_filter:= 'Exit';
        ELSIF ecn_filter = 'nyseen' THEN
          str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Entry''';
          entry_exit_filter:= 'Entry';
        ELSIF ecn_filter = 'nyseex' THEN
          str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Exit''';
          entry_exit_filter:= 'Exit';
        ELSIF ecn_filter = 'edgxen' THEN
          str_sql_no:=str_sql_no || ' and ecn = ''EDGX'' and entry_exit=''Entry''';
          entry_exit_filter:= 'Entry';
        ELSIF ecn_filter = 'edgxex' THEN
          str_sql_no:=str_sql_no || ' and ecn = ''EDGX'' and entry_exit=''Exit''';
          entry_exit_filter:= 'Exit';
        ELSIF ecn_filter = 'edgaen' THEN
          str_sql_no:=str_sql_no || ' and ecn = ''EDGA'' and entry_exit=''Entry''';
          entry_exit_filter:= 'Entry';
        ELSIF ecn_filter = 'edgaex' THEN
          str_sql_no:=str_sql_no || ' and ecn = ''EDGA'' and entry_exit=''Exit''';
          entry_exit_filter:= 'Exit';
        ELSIF ecn_filter = 'oubxen' THEN
          str_sql_no:=str_sql_no || ' and ecn = ''OUBX'' and entry_exit=''Entry''';
          entry_exit_filter:= 'Entry';
        ELSIF ecn_filter = 'oubxex' THEN
          str_sql_no:=str_sql_no || ' and ecn = ''OUBX'' and entry_exit=''Exit''';
          entry_exit_filter:= 'Exit';
        ELSIF ecn_filter = 'batyen' THEN
          str_sql_no:=str_sql_no || ' and ecn = ''BATY'' and entry_exit=''Entry''';
          entry_exit_filter:= 'Entry';    
        ELSIF ecn_filter = 'batyex' THEN
          str_sql_no:=str_sql_no || ' and ecn = ''BATY'' and entry_exit=''Exit''';
          entry_exit_filter:= 'Exit';  
        ELSIF ecn_filter = 'psxen' THEN
          str_sql_no:=str_sql_no || ' and ecn = ''PSX'' and entry_exit=''Entry''';
          entry_exit_filter:= 'Entry';    
        ELSIF ecn_filter = 'psxex' THEN
          str_sql_no:=str_sql_no || ' and ecn = ''PSX'' and entry_exit=''Exit''';
          entry_exit_filter:= 'Exit';    
        END IF;
      END IF;

      IF symb <>'' THEN
        str_sql:=str_sql || ' and symbol='''|| symb ||'''';
        str_sql_no:=str_sql_no || ' and symbol='''|| symb ||'''';
      END IF;
      --Filter by ISO Odrer
      IF is_iso = '1' AND from_date > '2010-01-01' THEN
        str_sql_no:=str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
      END IF;
      -- tmp_str := str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
  
      IF is_iso = '2' AND from_date > '2010-01-01' THEN
        str_sql_no:=str_sql_no || ' and (is_iso <> ''Y'')';
      END IF;
      
      str_sql_no:=str_sql_no || ' GROUP BY (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + cross_id)' ;

      IF bt_shares > 0 THEN
        str_sql:=str_sql || ' and bought_shares >=' || bt_shares;
      END IF;
      IF sd_shares > 0 THEN
        str_sql:=str_sql || ' and sold_shares >=' || sd_shares;
      END IF;

      str_sql:=str_sql || ' and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) in ('|| str_sql_no ||')';
      
      IF check_ecn_entry_exit = 0 AND ecn_filter <> '0' AND exclusive_val = 1 THEN
        str_sql_multi_orders := get_query_string_array_id_multi_orders(to_date, from_date, entry_exit_filter);
        str_sql :=str_sql || ' and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) not in (' || str_sql_multi_orders || ')';
      END IF;
      
      for missed_trade_record_of_ouch in EXECUTE str_sql loop
        count_ouch_missed_trade := missed_trade_record_of_ouch.count_missed;
        sum_ouch_expected_pl := missed_trade_record_of_ouch.sum_expected_pl;
      end loop;

      IF count_ouch_missed_trade > 0 THEN
        summary.count_repriced := count_ouch_missed_trade;        
        summary.count_miss := -count_ouch_missed_trade;        
      END IF;

    END IF;

    END IF;
    --filter by  ECN by Entry/Exit
    new_orders_table := '(SELECT n1.date, n1.ts_id, n1.cross_id, n1.ecn, n1.time_stamp, n1.price, n1.ack_price, n1.status, n1.entry_exit, n1.left_shares, n1.shares, n1.is_iso, n1. routing_inst, n2.ecn_launch ' ||
            'FROM new_orders n1 '
            'LEFT JOIN (SELECT DISTINCT date, ts_id, cross_id, ecn_launch '
            '      FROM new_orders '
            '      WHERE entry_exit = ''Entry'' AND ts_id <> -1 AND time_stamp between ''' || min_time || ''' AND '''|| temp_time || ''') n2 '
            'ON n1.date = n2.date AND n1.ts_id = n2.ts_id AND n1.cross_id = n2.cross_id) AS tmp ';
            
    str_sql:='SELECT * FROM vwtrade_results_statistics WHERE date >='''|| to_date || ''' and date <='''|| from_date ||''''|| share_size_condition || ecn_filter_conditions || launch_shares_condition;
    IF (expected_profit != 0.0) THEN
      str_sql := str_sql || ' and expected_pl >= ' || expected_profit;
    END IF;
    str_sql_no :='SELECT (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + cross_id) as tmpcol FROM ' || new_orders_table || ' WHERE date >='''|| to_date || ''' and date <='''|| from_date ||'''';
    str_sql_no :=str_sql_no || ' and time_stamp between ''' || min_time || ''' and '''|| temp_time || '''';
    
    --filter by ts, ecn launch
    IF check_ts = 0 THEN
      IF ts_id <> '0' AND ts_id <> '-1' THEN
        str_sql_no:=str_sql_no || ' and ts_id + 1 =' || ts_id;
      ELSIF ts_id = '-1' THEN
        str_sql_no:=str_sql_no || ' and bought_shares <> sold_shares ';
      END IF;
    ELSE
      IF strpos(ts_id,'-1') = 0 THEN 
        str_sql_no:=str_sql_no || ' and ts_id + 1 in ('|| ts_id ||')';
      ELSE 
        str_sql_no:=str_sql_no || ' and ((ts_id + 1 in ('|| ts_id ||')) OR (bought_shares <> sold_shares))';
      END IF;
    END IF;
    
    IF cecn <> '0' THEN
      str_sql_no:=str_sql_no || ' and ecn_launch =''' || cecn ||'''';
    END IF;
    
    ecn_filter_conditions := '';
    IF check_ecn_entry_exit = 1 THEN --Filter by multi ECN Entry/Exit
      str_sql := str_sql || ecn_filter;
    ELSE 
      IF ecn_filter = 'arcaen' THEN
        str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Entry''' ;
        entry_exit_filter:= 'Entry';
      ELSIF ecn_filter = 'arcaex' THEN
        str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Exit''' ;
        entry_exit_filter:= 'Exit';
      ELSIF ecn_filter = 'ouchen' THEN
        str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Entry''';
        entry_exit_filter:= 'Entry';
      ELSIF ecn_filter = 'ouchex' THEN
        str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Exit''';
        entry_exit_filter:= 'Exit';
      ELSIF ecn_filter = 'rashen' THEN
        str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Entry''';
        entry_exit_filter:= 'Entry';
      ELSIF ecn_filter = 'rashex' THEN
        str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Exit''';  
        entry_exit_filter:= 'Exit';      
      ELSIF ecn_filter = 'batzen' THEN
        str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Entry''';  
        entry_exit_filter:= 'Entry';
      ELSIF ecn_filter = 'batzex' THEN
        str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Exit'' and routing_inst != ''RL2''';
        entry_exit_filter:= 'Exit';
      ELSIF ecn_filter = 'bzsrex' THEN
        str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Exit'' and routing_inst = ''RL2''';
        entry_exit_filter:= 'Exit';
      ELSIF ecn_filter = 'nyseen' THEN
        str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Entry''';
        entry_exit_filter:= 'Entry';
      ELSIF ecn_filter = 'nyseex' THEN
        str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Exit''';
        entry_exit_filter:= 'Exit';  
      ELSIF ecn_filter = 'edgxen' THEN
        str_sql_no:=str_sql_no || ' and ecn = ''EDGX'' and entry_exit=''Entry''';
        entry_exit_filter:= 'Entry';
      ELSIF ecn_filter = 'edgxex' THEN
        str_sql_no:=str_sql_no || ' and ecn = ''EDGX'' and entry_exit=''Exit''';
        entry_exit_filter:= 'Exit';
      ELSIF ecn_filter = 'edgaen' THEN
        str_sql_no:=str_sql_no || ' and ecn = ''EDGA'' and entry_exit=''Entry''';
        entry_exit_filter:= 'Entry';
      ELSIF ecn_filter = 'edgaex' THEN
        str_sql_no:=str_sql_no || ' and ecn = ''EDGA'' and entry_exit=''Exit''';
        entry_exit_filter:= 'Exit';
      ELSIF ecn_filter = 'oubxen' THEN
        str_sql_no:=str_sql_no || ' and ecn = ''OUBX'' and entry_exit=''Entry''';
          entry_exit_filter:= 'Entry';
      ELSIF ecn_filter = 'oubxex' THEN
        str_sql_no:=str_sql_no || ' and ecn = ''OUBX'' and entry_exit=''Exit''';
        entry_exit_filter:= 'Exit';  
      ELSIF ecn_filter = 'batyen' THEN
        str_sql_no:=str_sql_no || ' and ecn = ''BATY'' and entry_exit=''Entry''';
        entry_exit_filter:= 'Entry';    
      ELSIF ecn_filter = 'batyex' THEN
        str_sql_no:=str_sql_no || ' and ecn = ''BATY'' and entry_exit=''Exit''';
        entry_exit_filter:= 'Exit';
      ELSIF ecn_filter = 'psxen' THEN
        str_sql_no:=str_sql_no || ' and ecn = ''PSX'' and entry_exit=''Entry''';
        entry_exit_filter:= 'Entry';    
      ELSIF ecn_filter = 'psxex' THEN
        str_sql_no:=str_sql_no || ' and ecn = ''PSX'' and entry_exit=''Exit''';
        entry_exit_filter:= 'Exit';  
      END IF;
    END IF;
    
    --Filter by ISO Odrer
    IF is_iso = '1' AND from_date > '2010-01-01' THEN
      str_sql_no:=str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
    END IF;
    -- tmp_str := str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
  
    IF is_iso = '2' AND from_date > '2010-01-01' THEN
      str_sql_no:=str_sql_no || ' and (is_iso <> ''Y'')';
    END IF;
    
    str_sql_no:=str_sql_no || ' GROUP BY (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + cross_id)' ;

    -- filter by Symbol, Bought Shares, Sold Shares
    IF symb <>'' THEN
      str_sql:=str_sql || ' and symbol='''|| symb ||'''';
    END IF;
    IF bt_shares > 0 THEN
      str_sql:=str_sql || ' and bought_shares >=' || bt_shares;
    END IF;
    IF sd_shares > 0 THEN
      str_sql:=str_sql || ' and sold_shares >=' || sd_shares;
    END IF;
    --filter by ts
    IF check_ts = 0 THEN
      IF ts_id <> '0' THEN 
        str_sql:=str_sql || ' and ts_id + 1 =' || ts_id;
      ELSIF ts_id = '-1' THEN
        str_sql := str_sql || ' and bought_shares <> sold_shares ';
      END IF;
    ELSE
      IF strpos(ts_id,'-1') = 0 THEN 
        str_sql:=str_sql || ' and ts_id + 1 in ('|| ts_id ||')';
      ELSE 
        str_sql:=str_sql || ' and ((ts_id + 1 in ('|| ts_id ||')) OR (bought_shares <> sold_shares))';
      END IF;
       
    END IF;
    IF cross_size <> '' then
      str_sql := str_sql || ' and ' || cross_size;
    END IF;
    
    str_sql:=str_sql || ' and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) in ('|| str_sql_no ||')';
    
    IF check_ecn_entry_exit = 0 AND ecn_filter <> '0' AND exclusive_val = 1 THEN
      str_sql_multi_orders := get_query_string_array_id_multi_orders(to_date, from_date, entry_exit_filter);
      str_sql :=str_sql || ' and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) not in (' || str_sql_multi_orders || ')';
    END IF;
    
    --calculate for trade_statistics table  
    FOR trade_result in EXECUTE str_sql loop
      shares_filled := trade_result.bought_shares;
      --RAISE NOTICE 'Variable an_integer was changed.';      
      IF (shares_filled > trade_result.sold_shares) THEN
        shares_filled := trade_result.sold_shares;
      END IF;

      shares_stuck := trade_result.bought_shares - trade_result.sold_shares;    

      IF (shares_stuck = 0) THEN
        profit_loss := trade_result.total_sold - trade_result.total_bought - trade_result.ts_total_fee;

        IF (shares_filled = 0) THEN
        summary.count_miss := summary.count_miss + 1;        
        ELSE
          IF (profit_loss < 0) THEN
            summary.count_loss := summary.count_loss + 1;            
          ELSE
            IF ( profit_loss::real < trade_result.expected_pl::real) THEN
              summary.count_profit_under := summary.count_profit_under + 1;            
            ELSE
              summary.count_profit_above := summary.count_profit_above + 1;              
            END IF;
          END IF;
        END IF;
      ELSE
        --Calculate for stuck
        IF (trade_result.pm_profit_loss - trade_result.pm_total_fee < -0.0001) THEN
          --stuck_loss
          summary.count_stuck_loss := summary.count_stuck_loss + 1;          
        ELSE
          --stuck_profit
          summary.count_stuck_profit := summary.count_stuck_profit + 1;        
        END IF;
      END IF;

    END LOOP;

    summary.time_stamp = t_current;
    min_time := t_current;  
  
    RETURN NEXT summary;
    
  END LOOP;
  RETURN;
end
$$;

ALTER FUNCTION public.cal_filter_trade_statistic_byecn2_twt2_with_step(to_date date, from_date date, time_from time without time zone, time_to time without time zone, share_size text, expected_profit double precision, symb text, bt_shares integer, sd_shares integer, ecn_filter text, ts_id text, cecn text, step integer, is_iso text, exclusive_val integer, cross_size text, launch_shares text) OWNER TO postgres;

COMMIT;

