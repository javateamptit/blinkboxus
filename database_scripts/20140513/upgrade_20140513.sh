#!/bin/sh

# DB scripts
db_script=upgrade_20140513.sql

oldest_db_suffix=201301
month_ago=0

now=$(date)
db_suffix=$(date -d "$now" "+%Y%m")

# use for loop read all databases
while [[ "$db_suffix" -ge "$oldest_db_suffix" ]];
do
	db_name="eaa10_$db_suffix"
  
  echo "Checking existing database $db_name"
  cmd=`psql template1 -U postgres -c "SELECT 'avaiable' AS result FROM pg_database WHERE datname = '$db_name'"`
  #echo "Result: $cmd"
  if [[ "$cmd" == *avaiable* ]]
  then
    echo "Upgrading $db_script on $db_name"
    cmd=`psql -U postgres $db_name -f $db_script`
    echo "Result: $cmd"
    if [[ "$cmd" == *ROLLBACK* ]]
    then
      echo "Fail! Skiped db script $db_script"
      exit;
    else
      echo "Successful upgrade $db_name"
    fi
  else
    echo "FATAL: database '$db_name' does not exist"
  fi
  
  month_ago=$(( month_ago + 1 ))
  db_suffix=$(date -d "$now -$month_ago months" "+%Y%m")
done
