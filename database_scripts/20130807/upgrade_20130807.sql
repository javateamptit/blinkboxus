BEGIN;

-- Upgrade
INSERT INTO release_versions(module_id, version) VALUES (2, '20130807');

--
-- Name: vw_fill_delays; Type: VIEW; Schema: public; Owner: postgres
--
CREATE OR REPLACE VIEW vw_fill_delays AS
    SELECT f.fill_id, n.entry_exit, date_part('microseconds'::text, (f.time_stamp - a.time_stamp)) AS fill_delay, n.date, n.ecn, n.ecn_suffix, n.order_id FROM ((new_orders n JOIN order_acks a ON ((a.oid = n.oid))) JOIN fills f ON ((f.oid = n.oid))) WHERE ((n.ts_id <> (-1)) AND (n.order_id <> 0)) ORDER BY f.fill_id, n.date;


ALTER TABLE public.vw_fill_delays OWNER TO postgres;

--
-- Name: vw_ack_delays; Type: VIEW; Schema: public; Owner: postgres
--
CREATE OR REPLACE VIEW vw_ack_delays AS
    SELECT new_orders.oid, new_orders.entry_exit, new_orders.date, date_part('microseconds'::text, (order_acks.time_stamp - new_orders.time_stamp)) AS ack_delay, new_orders.ecn, new_orders.ecn_suffix, new_orders.order_id FROM (new_orders JOIN order_acks ON ((order_acks.oid = new_orders.oid))) WHERE ((new_orders.ts_id <> (-1)) AND (new_orders.order_id <> 0)) ORDER BY new_orders.oid, new_orders.date;


ALTER TABLE public.vw_ack_delays OWNER TO postgres;

COMMIT;
