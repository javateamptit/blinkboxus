BEGIN;

-- Upgrade
INSERT INTO release_versions(module_id, version) VALUES (2, '20130502');

ALTER TABLE fills ADD COLUMN manual_update integer DEFAULT 0;
ALTER TABLE cancels ADD COLUMN manual_update integer DEFAULT 0;
ALTER TABLE trading_status ADD COLUMN hung_order integer DEFAULT 0;
ALTER TABLE risk_managements ADD COLUMN manual_update integer DEFAULT 0;
ALTER TABLE risk_managements ADD COLUMN changed_shares integer DEFAULT 0;

DROP FUNCTION IF EXISTS create_new_long_short(date, text, integer, integer, double precision);

CREATE OR REPLACE FUNCTION create_new_long_short(td date, tmp_symbol text, side integer, shares integer, price double precision, accountval integer)
  RETURNS integer AS
$$ 
declare
  valTime time;
  rm record;
begin	
  SELECT LOCALTIME(2) INTO valTime;

  SELECT * INTO rm FROM risk_managements WHERE date = td and trim(upper(symbol)) = trim(upper(tmp_symbol)) and account = accountVal;

  IF (rm.id IS NULL) THEN
    -- Create at begin day
	INSERT INTO risk_managements(symbol, average_price, net_quantity, matched_profit_loss, total_market_value, total_quantity, date, last_update, account, manual_update, changed_shares) 
		VALUES (upper(trim(tmp_symbol)), price, shares * side, 0.00, price * shares, shares, td, valTime, accountVal, 1, shares * side - 0);
    
	UPDATE trading_status SET rmupdated = 1;
    return 0;
  ELSE 
    IF (rm.total_quantity = 0 OR rm.total_quantity <= abs(rm.net_quantity)) THEN
		-- Create/edit at begin day
		UPDATE risk_managements
			SET average_price = price, net_quantity = shares * side, total_quantity = shares, matched_profit_loss = 0.00, total_market_value = price*shares, last_update = valTime, manual_update = 1, changed_shares = shares * side - rm.net_quantity
			WHERE date = td and trim(upper(symbol)) = trim(upper(tmp_symbol)) and account = accountVal;
	ELSE
		-- Create/edit at trading period: total_quantity = rm.total_quantity + abs(rm.net_quantity - shares * side);
		UPDATE risk_managements
			SET average_price = price, net_quantity = shares * side, total_quantity = rm.total_quantity + abs(rm.net_quantity - shares * side), total_market_value = price*shares, last_update = valTime, manual_update = 1, changed_shares = shares * side - rm.net_quantity
			WHERE date = td and trim(upper(symbol)) = trim(upper(tmp_symbol)) and account = accountVal;
	END IF;

	UPDATE trading_status SET rmupdated = 1;
    return 0;
  END IF;

  return -1;
end
$$
  LANGUAGE plpgsql VOLATILE;
  
ALTER FUNCTION create_new_long_short(date, text, integer, integer, double precision, integer)
  OWNER TO postgres;


CREATE OR REPLACE FUNCTION delete_long_short(idVal integer)
  RETURNS integer AS
$$ 
declare
	valTime time;
	rm record;
begin	 
	SELECT LOCALTIME(2) into valTime;
	
	SELECT * INTO rm FROM risk_managements WHERE id = idVal;
	
	IF (rm.id IS NOT NULL) THEN
		IF (rm.total_quantity <= abs(rm.net_quantity)) THEN
			-- Delete at begin day: Reset all to zero
			UPDATE risk_managements
				SET net_quantity = 0.00, total_quantity = 0, matched_profit_loss = 0.00, total_market_value = 0.00, last_update = valTime, manual_update = 1, changed_shares = 0 - rm.net_quantity
				WHERE id = idVal;
		ELSE		
			-- Delete at trading period: total_quantity = rm.total_quantity + abs(rm.net_quantity)
			UPDATE risk_managements
				SET net_quantity = 0.00, total_quantity = rm.total_quantity + abs(rm.net_quantity), total_market_value = 0.00, last_update = valTime, manual_update = 1, changed_shares = 0 - rm.net_quantity
				WHERE id = idVal;
		END IF;
		
		UPDATE trading_status SET rmupdated = 1;
		
		return 0;
	END IF;

	return -1;
end
$$
  LANGUAGE plpgsql VOLATILE;
  
ALTER FUNCTION delete_long_short(integer)
  OWNER TO postgres;


-- Function: update_rm()

-- DROP FUNCTION update_rm();

CREATE OR REPLACE FUNCTION update_rm()
  RETURNS trigger AS
$$
DECLARE
	symbolVal		text;
	leave_Shares	integer;
	order_Shares	integer;
	order_Price		double precision;
	avgPrice		double precision;
	totalQty		double precision;
	netPL			double precision;
	totalMktValue	double precision;
	rowcount		integer;
	dateVal			date;
	timeVal			time;
	sideVal			text;
	midVal			int;
	current_shares	int;
	current_price	double precision;
	fill 			record;
	fill_idVal 		int;
	sideBeginDay	character(1);
	sharesBeginDay 	integer;
	priceBeginDay 	double precision;
	accountVal 		integer;
	ecnVal 			text;
	liquidityVal	character;
	totalFee		double precision;
	temp_totalFee 	double precision;
BEGIN
	-- Get symbol name
	if TG_OP <> 'DELETE' then
		fill_idVal := NEW.fill_id;
		--Check to see if insert/update the CEE note, we will return 
		if NEW.is_cee IS TRUE then
			return NEW;
		end if;
	else
		fill_idVal := OLD.fill_id;
		--Check to see if insert/update the CEE note, we will return 
		if OLD.is_cee IS TRUE then
			return OLD;
		end if;
	end if;

	SELECT trim(new_orders.symbol), new_orders.date, fills.time_stamp, new_orders.side, account, new_orders.ecn, fills.liquidity into symbolVal, dateVal, timeVal, sideVal, accountVal, ecnVal, liquidityVal
	FROM new_orders
	JOIN fills ON new_orders.oid = fills.oid
	WHERE fills.fill_id = fill_idVal;

	SELECT side, shares, price into sideBeginDay, sharesBeginDay, priceBeginDay
	FROM open_positions 
	WHERE trim(symbol::text) = trim(symbolVal) AND date = dateVal AND account = accountVal;

	if (sharesBeginDay is null) then 
		leave_Shares := 0;
		avgPrice := 0;
		netPL := 0;
		totalMktValue := 0;
		totalQty := 0;
		totalFee := 0.00;
	else
		IF (sideBeginDay = 'S') THEN
			leave_Shares := -1 * sharesBeginDay;
		ELSE
			leave_Shares := sharesBeginDay;
		END IF;	

		avgPrice := priceBeginDay;
		netPL := 0;
		totalMktValue := sharesBeginDay * priceBeginDay;
		totalQty := sharesBeginDay;
		totalFee := 0.00;

	end if;	
	
	--Travel thought fill today and 
	for fill in (select f1.shares, f1.price, f1.fill_id, substring(o.side from 1 for 1) as side, o.ecn, f1.liquidity as liquidity 
					from fills f1 
					join new_orders o on f1.oid = o.oid 
					where o.date = dateVal and trim(o.symbol) = trim(symbolVal) and o.account = accountVal ) loop
				
		select new_shares, new_price into current_shares, current_price from brokens where fill_id = fill.fill_id;
		
		if current_price is null then
			current_shares  := fill.shares;
			current_price   := fill.price;
		end if;

		IF (leave_Shares > 0) THEN -- Equivalent to LONG
			IF (fill.side = 'B') THEN -- BUY
				avgPrice := (avgPrice * leave_Shares + current_price * current_shares) / (current_shares + leave_Shares);
				leave_Shares := leave_Shares + current_shares;
			ELSE -- SELL
				netPL := netPL + ((current_price - avgPrice) * LEAST(current_shares, leave_Shares));				
				leave_Shares := leave_Shares - current_shares;
				IF (leave_Shares < 0) THEN
					avgPrice := current_price;
				ELSIF (leave_Shares = 0) THEN
					avgPrice := 0;
				END IF;
			END IF;
		ELSIF (leave_Shares < 0) THEN -- Equivalent to SHORT
			IF (fill.side = 'B') THEN -- BUY
				netPL := netPL + ((avgPrice - current_price)*LEAST(current_shares, abs(leave_Shares)));				
				leave_Shares := leave_Shares + current_shares;
				IF (leave_Shares = 0) THEN
					avgPrice := 0;
				ELSIF (leave_Shares > 0) THEN
					avgPrice := current_price;
				END IF;
			ELSE -- SELL
				avgPrice := (avgPrice*(-leave_Shares) + current_price * current_shares) / (current_shares + (-leave_Shares));
				leave_Shares := leave_Shares - current_shares;				
			END IF;
		ELSE -- leave_Shares = 0
			avgPrice := current_price;
			IF (fill.side = 'B') THEN
				leave_Shares := current_shares;
			ELSE -- Action is Sell or Short Sell
				leave_Shares := -current_shares;
			END IF;
		END IF;
	
		totalMktValue := avgPrice * abs(leave_Shares);
		totalQty := totalQty + current_shares;	
		
		if (current_shares > 0 AND fill.liquidity IS NOT NULL) then
			select calculate_total_fee(current_price, current_shares, fill.side, fill.liquidity, fill.ecn) into temp_totalFee;
			
			totalFee := totalFee + temp_totalFee;
		end if;
	end loop;
			
	-- update RM
	UPDATE risk_managements
	SET average_price = avgPrice,
		net_quantity = leave_Shares, 
		matched_profit_loss = netPL, 
		total_market_value = totalMktValue, 
		total_quantity = totalQty, 
		last_update = timeVal,
		total_fee = totalFee
	WHERE trim(symbol) = trim(symbolVal) and date = dateVal and account = accountVal;
	
	RETURN NEW;
END $$
  LANGUAGE plpgsql VOLATILE;
  
ALTER FUNCTION update_rm()
  OWNER TO postgres;

-- Function: cancel_all_hung_order(text, date)

-- DROP FUNCTION cancel_all_hung_order(text, date);

CREATE OR REPLACE FUNCTION cancel_all_hung_order(list_order_id text, trade_date date)
  RETURNS integer AS
$$
DECLARE

	hung_order_id integer[] = string_to_array(list_order_id, '_');
	count integer;
	not_cancel integer;
	tempOrder record;
	tempCancel record;
	msg_content text;
BEGIN
	count = 1;
	not_cancel = 0;
	while hung_order_id[count] IS NOT NULL LOOP
		select * into tempOrder from new_orders where order_id = hung_order_id[count] and date = trade_date;
		count = count + 1;
		msg_content = '';
		IF tempOrder.ecn = 'ARCA' AND tempOrder.ecn_suffix IS NULL then
			msg_content = msg_content || '-50=A';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-46=' || tempOrder.order_id;
		ELSIF tempOrder.ecn = 'ARCA' AND tempOrder.ecn_suffix = 'FIX' then
			msg_content = msg_content || '35=8';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '11=' || tempOrder.order_id;
		ELSIF tempOrder.ecn = 'OUCH' THEN
			msg_content = msg_content || '-13=O';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-12=' || tempOrder.order_id;
			
		ELSIF tempOrder.ecn = 'RASH' THEN
			msg_content = msg_content || '-13=C'; 
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-12=' || tempOrder.order_id;
		ELSIF tempOrder.ecn = 'NYSE' THEN
			msg_content = msg_content || '-50=D1';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-46=' || tempOrder.order_id;
		ELSIF tempOrder.ecn = 'BATZ' THEN
			msg_content = msg_content || '-50=F';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-46=' || tempOrder.order_id;
		END IF;
		msg_content = msg_content || E'\001';
		
		select * into tempCancel from cancels where oid = tempOrder.oid;
		IF  tempCancel.oid IS NULL THEN
			INSERT INTO cancels(oid, shares, time_stamp, msgseq_num,sending_time,origclord_id , arcaex_order_id, exec_id, ord_status, text, msg_content, manual_update)
			VALUES(tempOrder.oid, tempOrder.shares, tempOrder.time_stamp, tempOrder.msgseq_num, NULL, tempOrder.order_id, NULL, NULL , NULL, NULL, msg_content, 1);
			
			UPDATE new_orders SET status = 'CanceledByECN' where oid = tempOrder.oid;			
			UPDATE trading_status SET hung_order = 1;
		ELSE
			not_cancel = not_cancel + 1;
		END IF;
		
	end loop;

	RETURN not_cancel;
END
$$
  LANGUAGE plpgsql VOLATILE;
  
ALTER FUNCTION cancel_all_hung_order(text, date)
  OWNER TO postgres;

-- Function: insert_cancel_hung_order(integer, integer)

-- DROP FUNCTION insert_cancel_hung_order(integer, integer);

CREATE OR REPLACE FUNCTION insert_cancel_hung_order(oid_t integer, cancel_shares integer)
  RETURNS integer AS
$$
DECLARE
	temporder record;
	tempCancel record;
	msg_content text;
BEGIN

	select * into tempOrder from new_orders where oid = oid_t;
	
	select * into tempCancel from cancels where oid = oid_t;
	msg_content = '';
	IF tempOrder.ecn = 'ARCA' AND tempOrder.ecn_suffix IS NULL then
		msg_content = msg_content || '-50=A';
		msg_content = msg_content || E'\001';
		msg_content = msg_content || '-46=' || tempOrder.order_id;
	ELSIF tempOrder.ecn = 'ARCA' AND tempOrder.ecn_suffix = 'FIX' then
		msg_content = msg_content || '35=8';
		msg_content = msg_content || E'\001';
		msg_content = msg_content || '11=' || tempOrder.order_id;
	ELSIF tempOrder.ecn = 'OUCH' THEN
		msg_content = msg_content || '-13=O';
		msg_content = msg_content || E'\001';
		msg_content = msg_content || '-12=' || tempOrder.order_id;
		
	ELSIF tempOrder.ecn = 'RASH' THEN
		msg_content = msg_content || '-13=C'; 
		msg_content = msg_content || E'\001';
		msg_content = msg_content || '-12=' || tempOrder.order_id;
	ELSIF tempOrder.ecn = 'NYSE' THEN
		msg_content = msg_content || '-50=D1';
		msg_content = msg_content || E'\001';
		msg_content = msg_content || '-46=' || tempOrder.order_id;
	ELSIF tempOrder.ecn = 'BATZ' THEN
		msg_content = msg_content || '-50=F';
		msg_content = msg_content || E'\001';
		msg_content = msg_content || '-46=' || tempOrder.order_id;
	END IF;
	
	msg_content = msg_content || E'\001';
	
	IF tempCancel.oid is NULL then
		INSERT INTO cancels(oid, shares, time_stamp, msgseq_num, origclord_id, msg_content, manual_update)
		VALUES(tempOrder.oid, cancel_shares, tempOrder.time_stamp, tempOrder.msgseq_num, tempOrder.order_id , msg_content, 1);
		
		UPDATE new_orders SET status = 'CanceledByECN' WHERE oid = oid_t;
		UPDATE trading_status SET hung_order = 1;	
	ELSE
		return 0;
	END IF;
	
	return 1;
END
$$
  LANGUAGE plpgsql VOLATILE;

ALTER FUNCTION insert_cancel_hung_order(integer, integer)
  OWNER TO postgres;

-- Function: insert_fill_hung_order(integer, text, text)

-- DROP FUNCTION insert_fill_hung_order(integer, text, text);

CREATE OR REPLACE FUNCTION insert_fill_hung_order(oid_t integer, list_shares text, list_price text)
  RETURNS integer AS
$$
DECLARE

	shares_array integer[] = string_to_array(list_shares, '_');
	price_array double precision[] = string_to_array(list_price, '_');
	tempOrder record;
	tempFill record;
	i integer;
	liquidity character;
	msg_content text;
	id_current integer;
	exce_id_tmp text;
	total_fill_shares integer;
BEGIN
 
	i = 1;
	total_fill_shares = 0;
	select * into tempOrder from new_orders where oid = oid_t;
	if tempOrder.ecn = 'ARCA' then
		liquidity = 'R';
	elsif tempOrder.ecn = 'OUCH' then
		liquidity = 'R';
	elsif tempOrder.ecn = 'RASH' then
		liquidity = 'R';
	elsif tempOrder.ecn = 'NYSE' then
		liquidity = '1';
	elsif tempOrder.ecn = 'BATZ' then
		liquidity = 'R';
	end if;
	select max(id) into id_current from fills; 
	IF id_current IS NULL THEN
		id_current = 1;
	END IF;
	exce_id_tmp = '';
	WHILE shares_array[i] IS NOT NULL LOOP
		msg_content = '';
		total_fill_shares = total_fill_shares + shares_array[i];
		if tempOrder.ecn = 'ARCA' AND tempOrder.ecn_suffix IS NULL then
			msg_content = msg_content || '-50=2';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-46=' || tempOrder.order_id;
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-44=' || shares_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-43=' || (price_array[i]*100);
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-42=' || '2';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-29=' || '99999';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-28=' || 'R';
		ELSIF tempOrder.ecn = 'ARCA' AND tempOrder.ecn_suffix = 'FIX' then
			msg_content = msg_content || '35=8';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '11=' || tempOrder.order_id;
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '38=' || shares_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '44=' || price_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '17=' || '99999';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '9730=' || 'R';
		ELSIF tempOrder.ecn = 'OUCH' THEN
			msg_content = msg_content || '-13=E';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-12=' || tempOrder.order_id;
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-10=' || shares_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-8=' || price_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-2=' || 'R';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-29=' || '99999';
			msg_content = msg_content || E'\001';
		ELSIF tempOrder.ecn = 'RASH' THEN
			msg_content = msg_content || '-13=E';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-12=' || tempOrder.order_id;
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-10=' || shares_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-8=' || price_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-2=' || 'R';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-29=' || '99999';
			msg_content = msg_content || E'\001';
		ELSIF tempOrder.ecn = 'NYSE' THEN
			msg_content = msg_content || '-50=81';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-10=' || shares_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-43=' || (price_array[i]*100);
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-42=' || '2';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-29=' || '99999';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-28=' || '1';
		ELSIF tempOrder.ecn = 'BATZ' THEN
			msg_content = msg_content || '-50=11';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-81=' || shares_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-83=' || price_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-28=' || 'R';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-29=' || '99999';
			msg_content = msg_content || E'\001';
			
		END IF;
		
		msg_content = msg_content || E'\001';
		id_current = id_current + 1;
		exce_id_tmp = '99999' || '_' || id_current;
		
		INSERT INTO fills(oid, shares, price, time_stamp, msgseq_num, origclord_id, exec_id, msg_content, liquidity,  manual_update)
			VALUES(oid_t, shares_array[i], price_array[i], tempOrder.time_stamp, tempOrder.msgseq_num, tempOrder.order_id, exce_id_tmp, msg_content, liquidity, 1 );
		i = i + 1;
	END LOOP;
	IF total_fill_shares < tempOrder.shares THEN
		UPDATE new_orders SET status = 'Lived' where oid = oid_t;
		UPDATE new_orders SET left_shares = (tempOrder.shares - total_fill_shares) where oid = oid_t;
	ELSE
		UPDATE new_orders SET status = 'Closed' where oid = oid_t;
		UPDATE new_orders SET left_shares = 0 where oid = oid_t;
	END IF;
	UPDATE trading_status SET hung_order = 1;
	
	return 1;
END
$$
  LANGUAGE plpgsql VOLATILE;
  
ALTER FUNCTION insert_fill_hung_order(integer, text, text)
  OWNER TO postgres;

COMMIT;
