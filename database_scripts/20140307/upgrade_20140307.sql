BEGIN;
-- Update cancels and order_rejects to add new timestamp and init data for new column
    ALTER TABLE cancels ADD COLUMN processed_time_stamp time without time zone;
	UPDATE cancels set processed_time_stamp=time_stamp;
	
	ALTER TABLE order_rejects ADD COLUMN processed_time_stamp time without time zone;
	UPDATE order_rejects set processed_time_stamp=time_stamp;

CREATE OR REPLACE FUNCTION cancel_all_hung_order(list_order_id text, trade_date date) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE

	hung_order_id integer[] = string_to_array(list_order_id, '_');
	count integer;
	not_cancel integer;
	tempOrder record;
	tempCancel record;
	msg_content text;
BEGIN
	count = 1;
	not_cancel = 0;
	while hung_order_id[count] IS NOT NULL LOOP
		select * into tempOrder from new_orders where order_id = hung_order_id[count] and date = trade_date;
		count = count + 1;
		msg_content = '';
		IF tempOrder.ecn = 'ARCA' AND tempOrder.ecn_suffix IS NULL then
			msg_content = msg_content || '-50=A';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-46=' || tempOrder.order_id;
		ELSIF tempOrder.ecn = 'ARCA' AND tempOrder.ecn_suffix = 'FIX' then
			msg_content = msg_content || '35=8';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '11=' || tempOrder.order_id;
		ELSIF tempOrder.ecn = 'OUCH' THEN
			msg_content = msg_content || '-13=O';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-12=' || tempOrder.order_id;
			
		ELSIF tempOrder.ecn = 'RASH' THEN
			msg_content = msg_content || '-13=C'; 
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-12=' || tempOrder.order_id;
		ELSIF tempOrder.ecn = 'NYSE' THEN
			msg_content = msg_content || '-50=D1';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-46=' || tempOrder.order_id;
		ELSIF tempOrder.ecn = 'BATZ' THEN
			msg_content = msg_content || '-50=F';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-46=' || tempOrder.order_id;
		END IF;
		msg_content = msg_content || E'\001';
		
		select * into tempCancel from cancels where oid = tempOrder.oid;
		IF  tempCancel.oid IS NULL THEN
			INSERT INTO cancels(oid, shares, time_stamp, msgseq_num,sending_time, origclord_id , arcaex_order_id, exec_id, ord_status, text, msg_content, manual_update, processed_time_stamp)
			VALUES(tempOrder.oid, tempOrder.shares, tempOrder.time_stamp, tempOrder.msgseq_num, NULL, tempOrder.order_id, NULL, NULL , NULL, NULL, msg_content, 1, tempOrder.time_stamp);
			
			UPDATE new_orders SET status = 'CanceledByECN' where oid = tempOrder.oid;			
			UPDATE trading_status SET hung_order = 1;
		ELSE
			not_cancel = not_cancel + 1;
		END IF;
		
	end loop;

	RETURN not_cancel;
END
$$;

CREATE OR REPLACE FUNCTION insert_cancel_hung_order(oid_t integer, cancel_shares integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	temporder record;
	tempCancel record;
	msg_content text;
BEGIN

	select * into tempOrder from new_orders where oid = oid_t;
	
	select * into tempCancel from cancels where oid = oid_t;
	msg_content = '';
	IF tempOrder.ecn = 'ARCA' AND tempOrder.ecn_suffix IS NULL then
		msg_content = msg_content || '-50=A';
		msg_content = msg_content || E'\001';
		msg_content = msg_content || '-46=' || tempOrder.order_id;
	ELSIF tempOrder.ecn = 'ARCA' AND tempOrder.ecn_suffix = 'FIX' then
		msg_content = msg_content || '35=8';
		msg_content = msg_content || E'\001';
		msg_content = msg_content || '11=' || tempOrder.order_id;
	ELSIF tempOrder.ecn = 'OUCH' THEN
		msg_content = msg_content || '-13=O';
		msg_content = msg_content || E'\001';
		msg_content = msg_content || '-12=' || tempOrder.order_id;
		
	ELSIF tempOrder.ecn = 'RASH' THEN
		msg_content = msg_content || '-13=C'; 
		msg_content = msg_content || E'\001';
		msg_content = msg_content || '-12=' || tempOrder.order_id;
	ELSIF tempOrder.ecn = 'NYSE' THEN
		msg_content = msg_content || '-50=D1';
		msg_content = msg_content || E'\001';
		msg_content = msg_content || '-46=' || tempOrder.order_id;
	ELSIF tempOrder.ecn = 'BATZ' THEN
		msg_content = msg_content || '-50=F';
		msg_content = msg_content || E'\001';
		msg_content = msg_content || '-46=' || tempOrder.order_id;
	END IF;
	
	msg_content = msg_content || E'\001';
	
	IF tempCancel.oid is NULL then
		INSERT INTO cancels(oid, shares, time_stamp, msgseq_num, origclord_id, msg_content, manual_update, processed_time_stamp)
		VALUES(tempOrder.oid, cancel_shares, tempOrder.time_stamp, tempOrder.msgseq_num, tempOrder.order_id , msg_content, 1, tempOrder.time_stamp);
		
		UPDATE new_orders SET status = 'CanceledByECN' WHERE oid = oid_t;
		UPDATE trading_status SET hung_order = 1;	
	ELSE
		return 0;
	END IF;
	
	return 1;
END
$$;

DROP FUNCTION IF EXISTS insertcanceledorder4arca_bats(integer, date, time without time zone, text, text, text, text);
DROP FUNCTION IF EXISTS insertcanceledorder4nasdaq(integer, date, time without time zone, text, text, text);
DROP FUNCTION IF EXISTS insertcanceledorder4nasdaq(integer, date, time without time zone, text, text, text, integer);
CREATE OR REPLACE FUNCTION insertcanceledorder4nasdaq(_orderid integer, _date date, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _msgcontents text, _status text, _entryexitrefnum integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	orderID integer;
BEGIN
	
	SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
	
	IF orderID IS NULL THEN
		RETURN -1;
	END IF;

	INSERT INTO cancels (oid, time_stamp, processed_time_stamp, msgseq_num, origclord_id, msg_content, entry_exit_ref) VALUES (orderID, _timeStamp, _processedtimestamp, _seqNum, _orderID, _msgContents, _entryExitRefNum);
	  
 	UPDATE new_orders SET status = _status where oid = orderID;
	
	RETURN 0;
END
$$;

DROP FUNCTION IF EXISTS insertmanualcanceledorder4nyse_ccg(integer, date, time without time zone, text, text, text, text);
DROP FUNCTION IF EXISTS insertmanualcanceledorder4nyse_ccg(integer, date, time without time zone, text, text, text, text, integer);
CREATE OR REPLACE FUNCTION insertmanualcanceledorder4nyse_ccg(_orderid integer, _date date, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _meorderid text, _msgcontents text, _status text, _entryexitrefnum integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	orderID integer;
BEGIN
	
	SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderid and date = _date;
	
	IF orderID IS NULL THEN
		RETURN -1;
	END IF;

	INSERT INTO cancels (oid, time_stamp, processed_time_stamp, msgseq_num, origclord_id, arcaex_order_id, msg_content, entry_exit_ref) VALUES (orderID, _timeStamp, _processedtimestamp, _seqNum, _orderid, _meorderid, _msgContents, _entryExitRefNum);
	  
 	UPDATE new_orders SET status = _status where oid = orderID;
	
	RETURN 0;
END$$;

DROP FUNCTION IF EXISTS insertmanualcanceledordervariant2(integer, date, time without time zone, text, text, text, text);
DROP FUNCTION IF EXISTS insertmanualcanceledordervariant2(integer, date, time without time zone, text, text, text, text, integer);
CREATE OR REPLACE FUNCTION insertmanualcanceledordervariant2(_orderid integer, _date date, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _arcaex_orderid text, _msgcontents text, _status text, _entryexitrefnum integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	orderID integer;
BEGIN
	
	SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
	
	IF orderID IS NULL THEN
		RETURN -1;
	END IF;

	INSERT INTO cancels (oid, time_stamp, processed_time_stamp, msgseq_num, origclord_id, arcaex_order_id, msg_content, entry_exit_ref) VALUES (orderID, _timeStamp, _processedtimestamp, _seqNum, _orderID, _arcaex_orderid, _msgContents, _entryExitRefNum);
	  
 	UPDATE new_orders SET status = _status where oid = orderID;
	
	RETURN 0;
END
$$;

DROP FUNCTION IF EXISTS insertreject4arca_direct(integer, date, character, time without time zone, text, text, text, text);
DROP FUNCTION IF EXISTS insertreject4arca_direct(integer, date, character, time without time zone, text, text, text, text, integer);
CREATE OR REPLACE FUNCTION insertreject4arca_direct(_orderid integer, _date date, _type character, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _text text, _msgcontents text, _status text, _entryexitrefnum integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	orderID integer;
BEGIN
	
	SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
	
	IF orderID IS NULL THEN
		RETURN -1;
	END IF;
	
	IF (_type = '1') THEN
	
		INSERT INTO order_rejects (oid, time_stamp, processed_time_stamp, msgseq_num, origclord_id, reason, msg_content, entry_exit_ref) VALUES (orderID, _timeStamp, _processedtimestamp, _seqNum, _orderID, _text, _msgContents, _entryExitRefNum);
		
		UPDATE new_orders SET status = _status where oid = orderID;
		
		RETURN 0;
	ELSIF (_type = '2') THEN 
				
		INSERT INTO cancel_rejects (oid, time_stamp, msgseq_num, origclord_id, reason, msg_content) VALUES (orderID, _timeStamp, _seqNum, _orderID, _text, _msgContents);
	
		UPDATE new_orders SET status = _status where oid = orderID;
		RETURN 0;
	ELSE
		RETURN 0;
	END IF;
END
$$;

DROP FUNCTION IF EXISTS insertreject4nyse_ccg(integer, date, character, time without time zone, text, text, text, text);
DROP FUNCTION IF EXISTS insertreject4nyse_ccg(integer, date, character, time without time zone, text, text, text, text, integer);
CREATE OR REPLACE FUNCTION insertreject4nyse_ccg(_orderid integer, _date date, _type character, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _text text, _msgcontents text, _status text, _entryexitrefnum integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	orderID integer;
BEGIN
	
	SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
	
	IF orderID IS NULL THEN
		RETURN -1;
	END IF;
	
	IF (_type = '1') THEN
	
		INSERT INTO order_rejects (oid, time_stamp, processed_time_stamp, msgseq_num, origclord_id, reason, msg_content, entry_exit_ref) VALUES (orderID, _timeStamp, _processedtimestamp, _seqNum, _orderID, _text, _msgContents, _entryExitRefNum);
		
		UPDATE new_orders SET status = _status where oid = orderID;
		
		RETURN 0;
	ELSIF (_type = '2') THEN 
				
		INSERT INTO cancel_rejects (oid, time_stamp, msgseq_num, origclord_id, reason, msg_content) VALUES (orderID, _timeStamp, _seqNum, _orderID, _text, _msgContents);
	
		UPDATE new_orders SET status = _status where oid = orderID;
		RETURN 0;
	ELSE
		RETURN 0;
	END IF;
END$$;

DROP FUNCTION IF EXISTS insertrejectedorder4arca_bats(integer, date, time without time zone, text, text, text, text, text);
DROP FUNCTION IF EXISTS insertrejectedorder4batsz(integer, date, time without time zone, text, text, text, text, text, integer);
CREATE OR REPLACE FUNCTION insertrejectedorder4batsz(_orderid integer, _date date, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _reason text, _text text, _msgcontents text, _status text, _entryexitrefnum integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	orderID integer;
BEGIN
	SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
	
	IF orderID IS NULL THEN
		RETURN -1;
	END IF;
	
	INSERT INTO order_rejects (oid, time_stamp, processed_time_stamp, msgseq_num, origclord_id, reason, text, msg_content, entry_exit_ref) VALUES (orderID, _timeStamp, _processedtimestamp, _seqNum, _orderID, _reason, _text, _msgContents, _entryExitRefNum);

 	UPDATE new_orders SET status = _status where oid = orderID;
	
	RETURN 0;
END
$$;

DROP FUNCTION IF EXISTS insertrejectedorder4nasdaq(integer, date, time without time zone, text, text, text, text);
DROP FUNCTION IF EXISTS insertrejectedorder4nasdaq(integer, date, time without time zone, text, text, text, text, integer);
CREATE OR REPLACE FUNCTION insertrejectedorder4nasdaq(_orderid integer, _date date, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _reason text, _msgcontents text, _status text, _entryexitrefnum integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	orderID integer;
BEGIN
	
	SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
	
	IF orderID IS NULL THEN
		RETURN -1;
	END IF;

	INSERT INTO order_rejects (oid, time_stamp, processed_time_stamp, msgseq_num, origclord_id, reason, msg_content, entry_exit_ref) VALUES (orderID, _timeStamp, _processedtimestamp, _seqNum, _orderID, _reason, _msgContents, _entryExitRefNum);
	
 	UPDATE new_orders SET status = _status where oid = orderID;
	
	RETURN 0;
END
$$;

-- Update view for exit delay
DROP VIEW IF EXISTS vw_exit_delays;
CREATE OR REPLACE VIEW vw_exit_delays AS
    SELECT _exit.id, _exit.date, _exit.time_stamp, _trigger.order_id, date_part('microseconds', (_exit.time_stamp - _trigger.processed_time_stamp)) AS delay, _trigger.ecn, _trigger.ecn_suffix  FROM 
    (
        (
           SELECT new_orders.date, new_orders.ts_id, new_orders.cross_id, new_orders.order_id, new_orders.ecn, new_orders.ecn_suffix, order_rejects.entry_exit_ref, order_rejects.processed_time_stamp
           FROM new_orders JOIN order_rejects ON new_orders.oid = order_rejects.oid 
         UNION
           SELECT new_orders.date, new_orders.ts_id, new_orders.cross_id, new_orders.order_id, new_orders.ecn, new_orders.ecn_suffix, cancels.entry_exit_ref, cancels.processed_time_stamp 
           FROM new_orders JOIN cancels ON new_orders.oid = cancels.oid
         UNION 
           SELECT new_orders.date, new_orders.ts_id, new_orders.cross_id, new_orders.order_id, new_orders.ecn, new_orders.ecn_suffix, fills.entry_exit_ref, fills.processed_time_stamp 
           FROM new_orders JOIN fills ON new_orders.oid = fills.oid
        ) _trigger 
    JOIN 
        (SELECT new_orders.date, new_orders.ts_id, new_orders.cross_id, min(new_orders.id) AS id, new_orders.entry_exit_ref, min(new_orders.time_stamp) AS time_stamp 
         FROM new_orders 
         WHERE (new_orders.ts_id <> (-1) AND new_orders.entry_exit = 'Exit'::bpchar)
         GROUP BY new_orders.date, new_orders.ts_id, new_orders.cross_id, new_orders.entry_exit_ref
         ) _exit
    ON (((((_trigger.date = _exit.date) AND (_trigger.ts_id = _exit.ts_id)) AND (_trigger.cross_id = _exit.cross_id)) AND (_trigger.entry_exit_ref = _exit.entry_exit_ref))))
    ORDER BY _exit.id;

ALTER TABLE public.vw_exit_delays OWNER TO postgres;

COMMIT;
