BEGIN;

-- Downgrade
DELETE FROM release_versions WHERE module_id = 2 AND version = '20130731';

COMMIT;