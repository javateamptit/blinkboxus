BEGIN;

DROP TABLE IF EXISTS summary_stock_types;
DROP TABLE IF EXISTS summary_stock_types_15;
DROP TABLE IF EXISTS summary_stock_types_30;
DROP TABLE IF EXISTS summary_stock_types_60;

DROP FUNCTION IF EXISTS process_insert_stock_types();
DROP FUNCTION IF EXISTS process_insert_stock_types_15();
DROP FUNCTION IF EXISTS process_insert_stock_types_30();

DROP SEQUENCE IF EXISTS summary_stock_types_id_seq;
DROP SEQUENCE IF EXISTS summary_stock_types_15_id_seq;
DROP SEQUENCE IF EXISTS summary_stock_types_30_id_seq;
DROP SEQUENCE IF EXISTS summary_stock_types_60_id_seq;

DROP SEQUENCE IF EXISTS summary_profit_losses_15_id_seq;
DROP SEQUENCE IF EXISTS summary_profit_losses_30_id_seq;
DROP SEQUENCE IF EXISTS summary_profit_losses_60_id_seq;

CREATE OR REPLACE FUNCTION clear_database() RETURNS integer
    LANGUAGE plpgsql
    AS $$
BEGIN
	DELETE FROM summary_ack_fills;
	DELETE FROM summary_ack_fills_15;
	DELETE FROM summary_ack_fills_30;
	DELETE FROM summary_ack_fills_60;

	DELETE FROM summary_entry_exits;
	DELETE FROM summary_entry_exits_15;
	DELETE FROM summary_entry_exits_30;
	DELETE FROM summary_entry_exits_60;

	DELETE FROM summary_launch_entry_ecns;
	DELETE FROM summary_launch_entry_ecns_15;
	DELETE FROM summary_launch_entry_ecns_30;
	DELETE FROM summary_launch_entry_ecns_60;
	
	DELETE FROM summary_launch_exit_ecns;
	DELETE FROM summary_launch_exit_ecns_15;
	DELETE FROM summary_launch_exit_ecns_30;
	DELETE FROM summary_launch_exit_ecns_60;
    
	DELETE FROM summary_order_types;
	DELETE FROM summary_order_types_15;
	DELETE FROM summary_order_types_30;
	DELETE FROM summary_order_types_60;

	DELETE FROM summary_profit_losses;
	
	RETURN 0;
END
$$;


ALTER FUNCTION public.clear_database() OWNER TO postgres;

CREATE OR REPLACE FUNCTION clear_database_with_date(_pdate date) RETURNS integer
    LANGUAGE plpgsql
    AS $$
BEGIN
	DELETE FROM summary_ack_fills WHERE date = _pdate;
	DELETE FROM summary_ack_fills_15 WHERE date = _pdate;
	DELETE FROM summary_ack_fills_30 WHERE date = _pdate;
	DELETE FROM summary_ack_fills_60 WHERE date = _pdate;

	DELETE FROM summary_entry_exits WHERE date = _pdate;
	DELETE FROM summary_entry_exits_15 WHERE date = _pdate;
	DELETE FROM summary_entry_exits_30 WHERE date = _pdate;
	DELETE FROM summary_entry_exits_60 WHERE date = _pdate;

	DELETE FROM summary_launch_entry_ecns WHERE date = _pdate;
	DELETE FROM summary_launch_entry_ecns_15 WHERE date = _pdate;
	DELETE FROM summary_launch_entry_ecns_30 WHERE date = _pdate;
	DELETE FROM summary_launch_entry_ecns_60 WHERE date = _pdate;

	DELETE FROM summary_order_types WHERE date = _pdate;
	DELETE FROM summary_order_types_15 WHERE date = _pdate;
	DELETE FROM summary_order_types_30 WHERE date = _pdate;
	DELETE FROM summary_order_types_60 WHERE date = _pdate;

	DELETE FROM summary_profit_losses WHERE date = _pdate;
	
	RETURN 0;
END
$$;


ALTER FUNCTION public.clear_database_with_date(_pdate date) OWNER TO postgres;

 CREATE TABLE summary_exit_order_shares (
     id serial NOT NULL,
     date date NOT NULL,
     time_stamp time without time zone NOT NULL,
     total_second integer,
     ecn_id integer,
 	 ts_id integer,
     shares_filled integer,
     order_size integer,
	 CONSTRAINT summary_exit_order_shares_pkey PRIMARY KEY (id )
 );

 CREATE TABLE summary_exit_order_shares_15 (
     id serial NOT NULL,
     date date NOT NULL,
     time_stamp time without time zone NOT NULL,
     total_second integer,
     ecn_id integer,
 	 ts_id integer,
     shares_filled integer,
     order_size integer,
	 CONSTRAINT summary_exit_order_shares_15_pkey PRIMARY KEY (id )
 );

 CREATE TABLE summary_exit_order_shares_30 (
     id serial NOT NULL,
     date date NOT NULL,
     time_stamp time without time zone NOT NULL,
     total_second integer,
     ecn_id integer,
 	 ts_id integer,
     shares_filled integer,
     order_size integer,
	 CONSTRAINT summary_exit_order_shares_30_pkey PRIMARY KEY (id )
 );

CREATE TABLE summary_exit_order_shares_60 (
    id serial NOT NULL,
    date date NOT NULL,
    time_stamp time without time zone NOT NULL,
    total_second integer,
    ecn_id integer,
	ts_id integer,
    shares_filled integer,
    order_size integer,
	CONSTRAINT summary_exit_order_shares_60_pkey PRIMARY KEY (id )
);


 -- Name: process_insert_exit_order_shares(); Type: FUNCTION; Schema: public; Owner: postgres

CREATE OR REPLACE FUNCTION  process_insert_exit_order_shares() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
	p_shares_filled	integer;
	p_order_size		integer;

BEGIN	
	IF (NEW.total_second%900=0) THEN	
		SELECT sum(shares_filled), sum(order_size)		
			INTO p_shares_filled, p_order_size
		FROM summary_exit_order_shares
		WHERE "date" = NEW.date 
			AND ecn_id = NEW.ecn_id 
			AND ts_id = NEW.ts_id
			AND total_second <= NEW.total_second
			AND total_second > NEW.total_second -900;			
		
		INSERT INTO summary_exit_order_shares_15("date", time_stamp, ts_id, ecn_id, total_second, shares_filled, order_size)
		VALUES (NEW.date, NEW.time_stamp, NEW.ts_id, NEW.ecn_id, NEW.total_second, p_shares_filled, p_order_size);					
	END IF; 	

	RETURN NEW;
END
$$;

--
-- Name: process_insert_exit_order_shares_15(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE OR REPLACE FUNCTION process_insert_exit_order_shares_15() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
	p_shares_filled	integer;
	p_order_size		integer;

BEGIN	
	IF (NEW.total_second%1800=0) THEN	
		SELECT sum(shares_filled), sum(order_size)		
			INTO p_shares_filled, p_order_size
		FROM summary_exit_order_shares_15
		WHERE "date" = NEW.date 
			AND ecn_id = NEW.ecn_id 
			AND ts_id = NEW.ts_id
			AND total_second <= NEW.total_second
			AND total_second > NEW.total_second -1800;			
		
		INSERT INTO summary_exit_order_shares_30("date", time_stamp, ts_id, ecn_id, total_second, shares_filled, order_size)
		VALUES (NEW.date, NEW.time_stamp, NEW.ts_id, NEW.ecn_id, NEW.total_second, p_shares_filled, p_order_size);					
	END IF; 	

	RETURN NEW;
END
$$;

--
-- Name: process_insert_exit_order_shares_30(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE OR REPLACE FUNCTION process_insert_exit_order_shares_30() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
	p_shares_filled	integer;
	p_order_size		integer;

BEGIN	
	IF (NEW.total_second%3600=0) THEN	
		SELECT sum(shares_filled), sum(order_size)		
			INTO p_shares_filled, p_order_size
		FROM summary_exit_order_shares_30
		WHERE "date" = NEW.date 
			AND ecn_id = NEW.ecn_id 
			AND ts_id = NEW.ts_id
			AND total_second <= NEW.total_second
			AND total_second > NEW.total_second -3600;			
		
		INSERT INTO summary_exit_order_shares_60("date", time_stamp, ts_id, ecn_id, total_second, shares_filled, order_size)
		VALUES (NEW.date, NEW.time_stamp, NEW.ts_id, NEW.ecn_id, NEW.total_second, p_shares_filled, p_order_size);					
	END IF; 	

	RETURN NEW;
END
$$;

CREATE TRIGGER process_insert_exit_order_shares AFTER INSERT ON summary_exit_order_shares FOR EACH ROW EXECUTE PROCEDURE process_insert_exit_order_shares();


--
-- Name: process_insert_exit_order_shares_15; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER process_insert_exit_order_shares_15 AFTER INSERT ON summary_exit_order_shares_15 FOR EACH ROW EXECUTE PROCEDURE process_insert_exit_order_shares_15();

--
-- Name: process_insert_exit_order_shares_30; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER process_insert_exit_order_shares_30 AFTER INSERT ON summary_exit_order_shares_30 FOR EACH ROW EXECUTE PROCEDURE process_insert_exit_order_shares_30();

--
-- Name: summary_exit_order_shares_15_all_0; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_15_all_0 ON summary_exit_order_shares_15 USING btree (date, ts_id, ecn_id);


--
-- Name: summary_exit_order_shares_15_all_1; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_15_all_1 ON summary_exit_order_shares_15 USING btree (date, ts_id, ecn_id, total_second);


--
-- Name: summary_exit_order_shares_15_all_2; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_15_all_2 ON summary_exit_order_shares_15 USING btree (date, time_stamp, ts_id, ecn_id);


--
-- Name: summary_exit_order_shares_15_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_15_date ON summary_exit_order_shares_15 USING btree (date);


--
-- Name: summary_exit_order_shares_15_date_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_15_date_ecn_id ON summary_exit_order_shares_15 USING btree (date, ecn_id);


--
-- Name: summary_exit_order_shares_15_date_total_second; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_15_date_total_second ON summary_exit_order_shares_15 USING btree (date, total_second);


--
-- Name: summary_exit_order_shares_15_date_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_15_date_ts_id ON summary_exit_order_shares_15 USING btree (date, ts_id);


--
-- Name: summary_exit_order_shares_15_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_15_ecn_id ON summary_exit_order_shares_15 USING btree (ecn_id);


--
-- Name: summary_exit_order_shares_15_time_stamp; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_15_time_stamp ON summary_exit_order_shares_15 USING btree (time_stamp);


--
-- Name: summary_exit_order_shares_15_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_15_ts_id ON summary_exit_order_shares_15 USING btree (ts_id);


--
-- Name: summary_exit_order_shares_30_all_0; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_30_all_0 ON summary_exit_order_shares_30 USING btree (date, ts_id, ecn_id);


--
-- Name: summary_exit_order_shares_30_all_1; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_30_all_1 ON summary_exit_order_shares_30 USING btree (date, ts_id, ecn_id, total_second);


--
-- Name: summary_exit_order_shares_30_all_2; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_30_all_2 ON summary_exit_order_shares_30 USING btree (date, time_stamp, ts_id, ecn_id);


--
-- Name: summary_exit_order_shares_30_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_30_date ON summary_exit_order_shares_30 USING btree (date);


--
-- Name: summary_exit_order_shares_30_date_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_30_date_ecn_id ON summary_exit_order_shares_30 USING btree (date, ecn_id);


--
-- Name: summary_exit_order_shares_30_date_total_second; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_30_date_total_second ON summary_exit_order_shares_30 USING btree (date, total_second);


--
-- Name: summary_exit_order_shares_30_date_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_30_date_ts_id ON summary_exit_order_shares_30 USING btree (date, ts_id);


--
-- Name: summary_exit_order_shares_30_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_30_ecn_id ON summary_exit_order_shares_30 USING btree (ecn_id);


--
-- Name: summary_exit_order_shares_30_time_stamp; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_30_time_stamp ON summary_exit_order_shares_30 USING btree (time_stamp);


--
-- Name: summary_exit_order_shares_30_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_30_ts_id ON summary_exit_order_shares_30 USING btree (ts_id);


--
-- Name: summary_exit_order_shares_60_all_0; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_60_all_0 ON summary_exit_order_shares_60 USING btree (date, ts_id, ecn_id);


--
-- Name: summary_exit_order_shares_60_all_1; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_60_all_1 ON summary_exit_order_shares_60 USING btree (date, ts_id, ecn_id, total_second);


--
-- Name: summary_exit_order_shares_60_all_2; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_60_all_2 ON summary_exit_order_shares_60 USING btree (date, time_stamp, ts_id, ecn_id);


--
-- Name: summary_exit_order_shares_60_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_60_date ON summary_exit_order_shares_60 USING btree (date);


--
-- Name: summary_exit_order_shares_60_date_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_60_date_ecn_id ON summary_exit_order_shares_60 USING btree (date, ecn_id);


--
-- Name: summary_exit_order_shares_60_date_total_second; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_60_date_total_second ON summary_exit_order_shares_60 USING btree (date, total_second);


--
-- Name: summary_exit_order_shares_60_date_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_60_date_ts_id ON summary_exit_order_shares_60 USING btree (date, ts_id);


--
-- Name: summary_exit_order_shares_60_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_60_ecn_id ON summary_exit_order_shares_60 USING btree (ecn_id);


--
-- Name: summary_exit_order_shares_60_time_stamp; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_60_time_stamp ON summary_exit_order_shares_60 USING btree (time_stamp);


--
-- Name: summary_exit_order_shares_60_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_60_ts_id ON summary_exit_order_shares_60 USING btree (ts_id);


--
-- Name: summary_exit_order_shares_all_0; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_all_0 ON summary_exit_order_shares USING btree (date, ts_id, ecn_id);


--
-- Name: summary_exit_order_shares_all_1; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_all_1 ON summary_exit_order_shares USING btree (date, ts_id, ecn_id, total_second);


--
-- Name: summary_exit_order_shares_all_2; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_all_2 ON summary_exit_order_shares USING btree (date, time_stamp, ts_id, ecn_id);


--
-- Name: summary_exit_order_shares_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_date ON summary_exit_order_shares USING btree (date);


--
-- Name: summary_exit_order_shares_date_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_date_ecn_id ON summary_exit_order_shares USING btree (date, ecn_id);


--
-- Name: summary_exit_order_shares_date_total_second; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_date_total_second ON summary_exit_order_shares USING btree (date, total_second);


--
-- Name: summary_exit_order_shares_date_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_date_ts_id ON summary_exit_order_shares USING btree (date, ts_id);


--
-- Name: summary_exit_order_shares_ecn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_ecn_id ON summary_exit_order_shares USING btree (ecn_id);


--
-- Name: summary_exit_order_shares_time_stamp; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_time_stamp ON summary_exit_order_shares USING btree (time_stamp);


--
-- Name: summary_exit_order_shares_ts_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX summary_exit_order_shares_ts_id ON summary_exit_order_shares USING btree (ts_id);


COMMIT;
