BEGIN;


DROP FUNCTION IF EXISTS cal_filter_trade_statistic_byecn2_twt2(date, date, time without time zone, time without time zone, integer, double precision, text, integer, integer, text, integer, text, text, integer, text);


DROP FUNCTION IF EXISTS cal_filter_trade_statistic_byecn2_twt2_with_step(date, date, time without time zone, time without time zone, integer, double precision, text, integer, integer, text, integer, text, integer, text, integer, text) ;


DROP FUNCTION IF EXISTS get_query_string_array_id_multi_orders (date, date);



--
-- Name: traded_orders; Type: VIEW; Schema: public; Owner: postgres
--

CREATE OR REPLACE VIEW traded_orders AS
    SELECT DISTINCT new_orders.oid AS id, new_orders.date AS trade_date, new_orders.symbol, new_orders.account, new_orders.order_id AS seqid, new_orders.entry_exit, new_orders.side AS action, new_orders.time_stamp AS trade_time, new_orders.ecn, new_orders.shares AS qty, new_orders.price, new_orders.time_in_force AS tif, new_orders.left_shares AS lvs, new_orders.status, new_orders.ts_id AS ts, new_orders.is_iso, brokens.filing_status, new_orders.avg_price, 
	CASE WHEN (new_orders.status ~~ 'Closed%'::text) THEN 'Fully Filled'::bpchar 
		WHEN (new_orders.status ~~ 'Cancel%'::text) AND (new_orders.left_shares < new_orders.shares) THEN 'Partially Filled'::bpchar
		WHEN (new_orders.status ~~ 'Cancel%'::text) THEN 'Cancelled'::bpchar
		ELSE new_orders.status END AS status_edit 	
	FROM (new_orders LEFT JOIN brokens ON (((new_orders.date = brokens.trade_date) AND (new_orders.oid = brokens.oid)))) ORDER BY new_orders.oid, new_orders.date, new_orders.symbol, new_orders.order_id, new_orders.entry_exit, new_orders.side, new_orders.time_stamp, new_orders.ecn, new_orders.shares, new_orders.price, new_orders.time_in_force, new_orders.left_shares, new_orders.status, new_orders.ts_id, new_orders.is_iso, brokens.filing_status, new_orders.account;
ALTER TABLE public.traded_orders OWNER TO postgres;

--
-- Name: traded_orders; Type: VIEW; Schema: public; Owner: postgres
--

CREATE OR REPLACE VIEW vw_trade_view AS
    ( 	SELECT DISTINCT tr.date, tr.as_cid, tr.ts_cid, tr.ts_id, tr.symbol, tr.bought_shares, tr.sold_shares, tr.total_bought, tr.total_sold, tr.trade_id, 0 AS trade_type, '0 '::text || bi.content AS raw_content, bi.bbo_id, bi.id, tr."timestamp"::text AS "timestamp"
		FROM trade_results tr
		JOIN bbo_infos bi ON bi.date = tr.date AND bi.cross_id = tr.ts_cid AND bi.ts_id = tr.ts_id
		ORDER BY bi.id, tr.date, tr.as_cid, tr.ts_cid, tr.ts_id, tr.symbol, tr.bought_shares, tr.sold_shares, tr.total_bought, tr.total_sold, tr.trade_id, 0::integer, '0 '::text || bi.content, bi.bbo_id, tr."timestamp"::text)
	UNION
        ( SELECT DISTINCT rc.date, rc.as_cid, rc.ts_cid, rc.ts_id, rc.symbol, tr.bought_shares, tr.sold_shares, tr.total_bought, tr.total_sold, tr.trade_id, rc.trade_type, rc.raw_content, (-1) AS bbo_id, rc.id + 100000000 AS id, split_part(rc.raw_content::text, ' '::text, 3) AS "timestamp"
           FROM raw_crosses rc
		LEFT JOIN trade_results tr ON rc.date = tr.date AND rc.ts_cid = tr.ts_cid AND rc.ts_id = tr.ts_id
		ORDER BY rc.id + 100000000, rc.date, rc.as_cid, rc.ts_cid, rc.ts_id, rc.symbol, tr.bought_shares, tr.sold_shares, tr.total_bought, tr.total_sold, tr.trade_id, rc.trade_type, rc.raw_content, (-1)::integer, split_part(rc.raw_content::text, ' '::text, 3))
	ORDER BY 2, 14;

ALTER TABLE public.vw_trade_view OWNER TO postgres;



--
-- Name: get_query_string_array_id_multi_orders(date, date, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE OR REPLACE FUNCTION get_query_string_array_id_multi_orders(date_from date, date_to date, entry_exit_filter text) RETURNS text
    LANGUAGE plpgsql
    AS $$ 
declare
ret text;
BEGIN
 ret ='SELECT DISTINCT (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + cross_id) from (
          SELECT tmp.date, tmp.cross_id, tmp.ts_id, tmp.symbol, tmp.entry_exit,count(tmp.entry_exit) as count
          FROM (SELECT date, cross_id, ts_id, symbol, ecn, entry_exit, count(entry_exit) AS count_entryexit
               FROM new_orders
               WHERE new_orders.ts_id <> -1 and date between ''' || date_from || ''' and ''' || date_to ||''' and entry_exit = ''' || entry_exit_filter ||'''
               GROUP BY cross_id, date, ts_id, symbol, ecn, entry_exit
               ORDER BY cross_id
            ) as tmp
          GROUP BY tmp.date, tmp.cross_id, tmp.ts_id, tmp.symbol, tmp.entry_exit
          HAVING count(tmp.entry_exit) > 1
          ORDER BY tmp.cross_id
        ) as tmp2';

  return ret;
END
$$;


--
-- Name: cal_filter_trade_statistic_byecn2_twt2(date, date, time without time zone, time without time zone, integer, double precision, text, integer, integer, text, integer, text, text, integer, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE OR REPLACE FUNCTION cal_filter_trade_statistic_byecn2_twt2(to_date date, from_date date, time_from time without time zone, time_to time without time zone, share_size integer, expected_profit double precision, symb text, bt_shares integer, sd_shares integer, ecn_filter text, ts_id text, cecn text, is_iso text, exclusive_val integer, cross_size text) RETURNS trade_result_summary_shares_traded
    LANGUAGE plpgsql
    AS $$
declare
  trade_result record;
  summary trade_result_summary_shares_traded;
  shares_filled integer;
  shares_stuck integer;
  profit_loss double precision;
 --statistic missed trade 
  count_ouch_missed_trade integer;
  sum_shares_traded_miss integer;
  sum_ouch_expected_pl double precision;
  missed_trade_record_of_ouch record;
  str_sql text;
  str_sql1 text;
  str_sql2 text;	
  str_sql_no text;
  str_sql_no_repriced text;
  str_sql_repriced text;
  str_sql_multi_orders text;
  str_sql_tmp text;
  tmp_str text;
  tmp_ASCid text;
  new_orders_table text;
  share_size_condition text;
   -- Filter Entry or Exit
  entry_exit_filter text; 
  -- Check number TS
  check_ts integer;
  
begin	
  -- Initialize
  summary.count_miss := 0;
  summary.count_repriced := 0;
  summary.count_stuck_loss := 0;
  summary.count_stuck_profit := 0;
  summary.count_loss := 0;
  summary.count_profit_under := 0;
  summary.count_profit_above := 0;
  summary.count_total := 0;
  summary.shares_traded_miss := 0;
  summary.shares_traded_repriced := 0;
  summary.shares_traded_stuck_loss := 0;
  summary.shares_traded_stuck_profit := 0;
  summary.shares_traded_loss := 0;
  summary.shares_traded_profit_under := 0;
  summary.shares_traded_profit_above := 0;
  summary.shares_traded_total := 0;
  summary.pm_shares_traded_miss := 0;
  summary.pm_shares_traded_repriced := 0;
  summary.pm_shares_traded_stuck_loss := 0;
  summary.pm_shares_traded_stuck_profit := 0;
  summary.pm_shares_traded_loss := 0;
  summary.pm_shares_traded_profit_under := 0;
  summary.pm_shares_traded_profit_above := 0;
  summary.pm_shares_traded_total := 0;
  
  summary.expected_miss := 0.00;
  summary.expected_repriced := 0.00;
  summary.expected_stuck_loss := 0.00;
  summary.expected_stuck_profit := 0.00;
  summary.expected_loss := 0.00;
  summary.expected_profit_under := 0.00;
  summary.expected_profit_above := 0.00;
  summary.expected_total := 0.00;
  summary.real_miss := 0.00;
  summary.real_stuck_loss := 0.00;
  summary.real_stuck_profit := 0.00;
  summary.real_loss = 0.00;
  summary.real_profit_under := 0.00;
  summary.real_profit_above := 0.00;
  summary.real_total := 0.00;
  
  summary.pm_miss := 0.00;
  summary.pm_stuck_loss := 0.00;
  summary.pm_stuck_profit := 0.00;
  summary.pm_loss = 0.00;
  summary.pm_profit_under := 0.00;
  summary.pm_profit_above := 0.00;
  summary.pm_total := 0.00;
  
  entry_exit_filter := '';-- Filter Entry or Exit
  check_ts := 0;
  
  share_size_condition := '';
  IF share_size <> -100 THEN 
	share_size_condition := 'expected_shares >='|| share_size ||'';
  ELSE -- Filter share size < 100 
	share_size_condition := 'expected_shares < 100';   
  END IF;
  
  IF strpos(ts_id,',') <> 0 THEN -- Multi TS
     check_ts := 1;
  END IF;
  
  new_orders_table := '(SELECT n1.date, n1.ts_id, n1.cross_id, n1.ecn, n1.time_stamp, n1.price, n1.ack_price, n1.status, n1.entry_exit, n1.left_shares, n1.shares, n1.is_iso, n2.ecn_launch ' ||
						'FROM new_orders n1 '
						'LEFT JOIN (SELECT DISTINCT date, ts_id, cross_id, ecn_launch '
						'			FROM new_orders '
						'			WHERE entry_exit = ''Entry'' AND ts_id <> -1) n2 '
						'ON n1.date = n2.date AND n1.ts_id = n2.ts_id AND n1.cross_id = n2.cross_id) AS tmp ';
  
  str_sql :='';
  str_sql_no :='SELECT DISTINCT (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + cross_id) as tmpcol FROM ' || new_orders_table || ' WHERE date >='''|| to_date || ''' and date <='''|| from_date ||'''';
  str_sql_no :=str_sql_no || ' and time_stamp between ''' || time_from || ''' and '''|| time_to || '''';
	--filter by ts, ecn launch
	IF check_ts = 0 THEN
		IF ts_id <> '0' AND ts_id <> '-1' THEN
			str_sql_no:=str_sql_no || ' and ts_id + 1 =' || ts_id;
		ELSIF ts_id = '-1' THEN
			str_sql_no:=str_sql_no || ' and bought_shares <> sold_shares ';
		END IF;
	ELSE
		IF strpos(ts_id,'-1') = 0 THEN 
			str_sql_no:=str_sql_no || ' and ts_id + 1 in ('|| ts_id ||')';
		ELSE 
			str_sql_no:=str_sql_no || ' and ((ts_id + 1 in ('|| ts_id ||')) OR (bought_shares <> sold_shares))';
		END IF;
	END IF;
	
	IF cecn <> '0' THEN
		str_sql_no:=str_sql_no || ' and ecn_launch =''' || cecn ||'''';
	END IF;
	
	IF ecn_filter = 'arcaen' THEN
		str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Entry''' ;
		entry_exit_filter:= 'Entry';
	ELSIF ecn_filter = 'arcaex' THEN
		str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Exit''' ;
		entry_exit_filter:= 'Exit';
	ELSIF ecn_filter = 'ouchen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Entry''';
		entry_exit_filter:= 'Entry';
	ELSIF ecn_filter = 'ouchex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Exit''';
		entry_exit_filter:= 'Exit';
	ELSIF ecn_filter = 'rashen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Entry''';
		entry_exit_filter:= 'Entry';
	ELSIF ecn_filter = 'rashex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Exit''';
		entry_exit_filter:= 'Exit';		
	ELSIF ecn_filter = 'batzen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Entry''';
		entry_exit_filter:= 'Entry';		
	ELSIF ecn_filter = 'batzex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Exit''';
		entry_exit_filter:= 'Exit';
	ELSIF ecn_filter = 'oubxen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''OUBX'' and entry_exit=''Entry''';
		entry_exit_filter:= 'Entry';
	ELSIF ecn_filter = 'oubxex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''OUBX'' and entry_exit=''Exit''';
		entry_exit_filter:= 'Exit';
	ELSIF ecn_filter = 'rabxen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''RABX'' and entry_exit=''Entry''';
		entry_exit_filter:= 'Entry';
	ELSIF ecn_filter = 'rabxex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''RABX'' and entry_exit=''Exit''';
		entry_exit_filter:= 'Exit';
	ELSIF ecn_filter = 'nyseen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Entry''';
		entry_exit_filter:= 'Entry';
	ELSIF ecn_filter = 'nyseex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Exit''';
		entry_exit_filter:= 'Exit';
	ELSIF ecn_filter = 'edgxen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''EDGX'' and entry_exit=''Entry''';
		entry_exit_filter:= 'Entry';
	ELSIF ecn_filter = 'edgxex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''EDGX'' and entry_exit=''Exit''';
		entry_exit_filter:= 'Exit';	
	END IF;

	--Filter by ISO Odrer
	IF is_iso = '1' THEN
		str_sql_no:=str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
	END IF;
	-- tmp_str := str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
	
	IF is_iso = '2' THEN
		str_sql_no:=str_sql_no || ' and (is_iso <> ''Y'')';
	END IF;
	
	
	-- filter by Symbol, Bought Shares, Sold Shares
	IF symb <>'' THEN
		str_sql:=' and symbol='''|| symb ||'''';
	END IF;
	IF bt_shares > 0 THEN
		str_sql:=str_sql || ' and bought_shares >=' || bt_shares;
	END IF;
	IF sd_shares > 0 THEN
		str_sql:=str_sql || ' and sold_shares >=' || sd_shares;
	END IF;
	--filter by ts
	
	IF check_ts = 0 THEN
		IF ts_id <> '0' AND ts_id <> '-1' THEN 
			str_sql:=str_sql || ' and ts_id + 1 =' || ts_id;
		ELSIF ts_id = '-1' THEN
			str_sql := str_sql || ' and bought_shares <> sold_shares ';
		END IF;
	ELSE
		IF strpos(ts_id,'-1') = 0 THEN 
			str_sql:=str_sql || ' and ts_id + 1 in ('|| ts_id ||')';
		ELSE 
			str_sql:=str_sql || ' and ((ts_id + 1 in ('|| ts_id ||')) OR (bought_shares <> sold_shares))';
		END IF;
	   
	END IF;
	IF cross_size <> '' then
		str_sql := str_sql || ' and ' || cross_size;
	END IF;
	IF ecn_filter <> '0' AND exclusive_val = 1 THEN
		str_sql_multi_orders := get_query_string_array_id_multi_orders(to_date, from_date, entry_exit_filter);
		str_sql := str_sql || ' and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) not in (' || str_sql_multi_orders || ')';
	END IF;
	
	--Calculate repriced
	
	
 IF ((ecn_filter = 'ouchen' OR ecn_filter = 'ouchex' OR ecn_filter = '0' ) AND (cecn = 'NDAQ' OR cecn = '0')) THEN 
	str_sql_no_repriced:=str_sql_no || ' and ecn =''OUCH'' and price != ack_price ' ;
	str_sql_repriced:=str_sql || '  and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) in ('|| str_sql_no_repriced ||')';	
	str_sql1:='SELECT count(*) as count_missed, sum(expected_pl)as sum_expected_pl, sum(bought_shares) as sum_bought_shares, sum(sold_shares) as sum_sold_shares FROM trade_results WHERE bought_shares = 0 and sold_shares = 0 and date >='''|| to_date || ''' and date <='''|| from_date ||''' and '|| share_size_condition ||' and expected_pl >=' || expected_profit ;
   
   str_sql1:=str_sql1 ||str_sql_repriced;
    for missed_trade_record_of_ouch in EXECUTE str_sql1 loop
		count_ouch_missed_trade := missed_trade_record_of_ouch.count_missed;
		sum_ouch_expected_pl := missed_trade_record_of_ouch.sum_expected_pl;
		sum_shares_traded_miss := missed_trade_record_of_ouch.sum_bought_shares + missed_trade_record_of_ouch.sum_sold_shares;
    end loop;

   IF count_ouch_missed_trade > 0 THEN
	summary.count_repriced := count_ouch_missed_trade;
	summary.expected_repriced := sum_ouch_expected_pl;
	summary.count_miss := -count_ouch_missed_trade;
	summary.expected_miss :=  -sum_ouch_expected_pl;
	summary.shares_traded_miss := -sum_shares_traded_miss;
	summary.shares_traded_repriced := sum_shares_traded_miss;
   END IF;

 END IF;
 
 --calculate for trade_statistics table	
 str_sql:=str_sql || '  and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) in ('|| str_sql_no ||')';
 str_sql2:='SELECT * FROM trade_results WHERE date >='''|| to_date || ''' and date <='''|| from_date ||''' and '|| share_size_condition ||' and expected_pl >=' || expected_profit ;
 str_sql2:=str_sql2 ||str_sql;
FOR trade_result in EXECUTE str_sql2 loop
    shares_filled := trade_result.bought_shares;

    IF (shares_filled > trade_result.sold_shares) THEN
      shares_filled := trade_result.sold_shares;
    END IF;

    shares_stuck := trade_result.bought_shares - trade_result.sold_shares;    

    IF (shares_stuck = 0) THEN
      profit_loss := trade_result.total_sold - trade_result.total_bought - trade_result.ts_total_fee;

      IF (shares_filled = 0) THEN
        summary.count_miss := summary.count_miss + 1;
		summary.shares_traded_miss := summary.shares_traded_miss + trade_result.bought_shares + trade_result.sold_shares;
        summary.expected_miss := summary.expected_miss + trade_result.expected_pl;
      ELSE
        IF (profit_loss < 0) THEN
          summary.count_loss := summary.count_loss + 1;
		  summary.shares_traded_loss := summary.shares_traded_loss + trade_result.bought_shares + trade_result.sold_shares;
          summary.expected_loss := summary.expected_loss + trade_result.expected_pl;
          summary.real_loss := summary.real_loss + profit_loss;
        ELSE
          IF ( profit_loss::real < trade_result.expected_pl::real) THEN
            summary.count_profit_under := summary.count_profit_under + 1;
			summary.shares_traded_profit_under := summary.shares_traded_profit_under + trade_result.bought_shares + trade_result.sold_shares;
            summary.expected_profit_under := summary.expected_profit_under + trade_result.expected_pl;
            summary.real_profit_under := summary.real_profit_under + profit_loss;
          ELSE
            summary.count_profit_above := summary.count_profit_above + 1;
			summary.shares_traded_profit_above := summary.shares_traded_profit_above + trade_result.bought_shares + trade_result.sold_shares;
            summary.expected_profit_above := summary.expected_profit_above + trade_result.expected_pl;
            summary.real_profit_above := summary.real_profit_above + profit_loss;
          END IF;
        END IF;
      END IF;
    ELSE
      --Calculate for stuck
      IF (trade_result.pm_profit_loss - trade_result.pm_total_fee < -0.0001) THEN
	--stuck_loss
	   summary.count_stuck_loss := summary.count_stuck_loss + 1;
	   summary.shares_traded_stuck_loss := summary.shares_traded_stuck_loss + trade_result.bought_shares + trade_result.sold_shares;
	   summary.expected_stuck_loss := summary.expected_stuck_loss + trade_result.expected_pl;
	   summary.real_stuck_loss := summary.real_stuck_loss + trade_result.ts_profit_loss  + trade_result.pm_profit_loss - trade_result.ts_total_fee - trade_result.pm_total_fee;
	   summary.pm_stuck_loss := summary.pm_stuck_loss + trade_result.pm_profit_loss - trade_result.pm_total_fee;
	   IF trade_result.bought_shares < trade_result.sold_shares THEN
		summary.pm_shares_traded_stuck_loss := summary.pm_shares_traded_stuck_loss - trade_result.bought_shares + trade_result.sold_shares;
	   ELSE
		summary.pm_shares_traded_stuck_loss := summary.pm_shares_traded_stuck_loss + trade_result.bought_shares - trade_result.sold_shares;
	   END IF;
      ELSE
	--stuck_profit
	   summary.count_stuck_profit := summary.count_stuck_profit + 1;
	   summary.shares_traded_stuck_profit := summary.shares_traded_stuck_profit + trade_result.bought_shares + trade_result.sold_shares;
	   summary.expected_stuck_profit := summary.expected_stuck_profit + trade_result.expected_pl;
	   summary.real_stuck_profit := summary.real_stuck_profit + trade_result.ts_profit_loss + trade_result.pm_profit_loss - trade_result.ts_total_fee - trade_result.pm_total_fee;
	   summary.pm_stuck_profit := summary.pm_stuck_profit + trade_result.pm_profit_loss - trade_result.pm_total_fee;
	   IF trade_result.bought_shares < trade_result.sold_shares THEN
		summary.pm_shares_traded_stuck_profit := summary.pm_shares_traded_stuck_profit - trade_result.bought_shares + trade_result.sold_shares;
	   ELSE
		summary.pm_shares_traded_stuck_profit := summary.pm_shares_traded_stuck_profit + trade_result.bought_shares - trade_result.sold_shares;
	   END IF;
      END IF;
    END IF;

    summary.count_total := summary.count_total +  1;
	summary.shares_traded_total := summary.shares_traded_total + trade_result.bought_shares + trade_result.sold_shares;
    summary.expected_total := summary.expected_total + trade_result.expected_pl  ;
  end loop;
  
  summary.count_total := summary.count_total - summary.count_repriced;
  summary.shares_traded_total := summary.shares_traded_total - summary.shares_traded_repriced;
  summary.expected_total := summary.expected_total - summary.expected_repriced;
  
  return summary;
end
$$;

ALTER FUNCTION public.cal_filter_trade_statistic_byecn2_twt2(to_date date, from_date date, time_from time without time zone, time_to time without time zone, share_size integer, expected_profit double precision, symb text, bt_shares integer, sd_shares integer, ecn_filter text, ts_id text, cecn text, is_iso text, exclusive_val integer, cross_size text) OWNER TO postgres;


--
-- Name: cal_filter_trade_statistic_byecn2_twt2_with_step(date, date, time without time zone, time without time zone, integer, double precision, text, integer, integer, text, integer, text, integer, text, integer, text); Type: FUNCTION; Schema: public; Owner: postgres
--


CREATE OR REPLACE FUNCTION cal_filter_trade_statistic_byecn2_twt2_with_step(to_date date, from_date date, time_from time without time zone, time_to time without time zone, share_size integer, expected_profit double precision, symb text, bt_shares integer, sd_shares integer, ecn_filter text, ts_id text, cecn text, step integer, is_iso text, exclusive_val integer, cross_size text) RETURNS SETOF trade_result_summary2_struct
    LANGUAGE plpgsql
    AS $$ 
declare
	trade_result 				record;
	summary 				trade_result_summary2_struct;
	shares_filled 				integer;
	shares_stuck 				integer;
	profit_loss 				double precision;
	--statistic missed trade 
	count_ouch_missed_trade 		integer;
	sum_ouch_expected_pl 			double precision;
	missed_trade_record_of_ouch 		record;
	str_sql 				text;
	str_sql_no 				text;
	str_sql_tmp			   	text;
	str_sql_multi_orders text;
	tmp text;
	tmp_str text;
	min_time				"time";
	max_time				"time";
	temp_time 				"time";
	t_current				"time";
	new_orders_table text;
	share_size_condition text;
	 -- Filter Entry or Exit
	entry_exit_filter text;
	 -- Check number TS
    check_ts integer;
  
begin	
						
	min_time := time_from;
	max_time := time_to;
	
	entry_exit_filter := '';  -- Filter Entry or Exit
	
	share_size_condition := '';	
	IF share_size <> -100 THEN 
		share_size_condition := 'expected_shares >='|| share_size ||'';
	ELSE -- Filter share size < 100 
		share_size_condition := 'expected_shares < 100' ;
	END IF;
	
	check_ts := 0;
	IF strpos(ts_id,',') <> 0 THEN -- Multi TS
		check_ts := 1;
	END IF;
  
	
	WHILE min_time < max_time LOOP
		-- Initia
		
		summary.count_miss := 0;
		summary.count_repriced := 0;
		summary.count_stuck_loss := 0;
		summary.count_stuck_profit := 0;
		summary.count_loss := 0;
		summary.count_profit_under := 0;
		summary.count_profit_above := 0;		
		
		IF (step = 5) THEN
			t_current = min_time + interval '5 minutes';
		ELSIF (step = 15) THEN
			t_current = min_time + interval '15 minutes';
			
		ELSIF (step = 30) THEN
			t_current = min_time + interval '30 minutes';
		ELSIF (step = 60) THEN
			t_current = min_time + interval '60 minutes';
		END IF;
		--raise notice '1(%)', t_current;
		IF ((t_current > max_time) or (t_current = '00:00:00')) THEN
			t_current = max_time;
			--raise notice '2(%)', t_current;
		END IF; 
		temp_time = t_current - interval '1 microsecond';
		
		--Calculate repriced
		IF ((ecn_filter = 'ouchen' OR ecn_filter = 'ouchex' OR ecn_filter = '0' ) AND (cecn = 'NDAQ' OR cecn = '0')) THEN
		
			--Select data from view "vwouch_missed_trades" and calculate total missed trade 	
			str_sql:='SELECT count(*) as count_missed, sum(expected_pl)as sum_expected_pl  FROM trade_results WHERE date >='''|| to_date || ''' and date <='''|| from_date ||''' and '|| share_size_condition ||' and expected_pl >=' || expected_profit ;
			str_sql := str_sql || ' and bought_shares = 0 and sold_shares = 0 ';
			
			--filter by ts when we get data from view trade_results
			IF check_ts = 0 THEN
				IF ts_id <> '0' THEN 
					str_sql:=str_sql || ' and ts_id + 1 =' || ts_id;
				ELSIF ts_id = '-1' THEN
					str_sql := str_sql || ' and bought_shares <> sold_shares ';
				END IF;
			ELSE
				IF strpos(ts_id,'-1') = 0 THEN 
					str_sql:=str_sql || ' and ts_id + 1 in ('|| ts_id ||')';
				ELSE 
					str_sql:=str_sql || ' and ((ts_id + 1 in ('|| ts_id ||')) OR (bought_shares <> sold_shares))';
				END IF;
			   
			END IF;
			IF cross_size <> '' then
				str_sql := str_sql || ' and ' || cross_size;
			END IF;
			
			str_sql_no :='SELECT (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + cross_id) as tmpcol FROM new_orders WHERE date >='''|| to_date || ''' and date <='''|| from_date ||'''';
			str_sql_no :=str_sql_no || ' and time_stamp between ''' || min_time || ''' and '''|| temp_time || '''';
			str_sql_no :=str_sql_no || ' and ecn =''OUCH'' and price != ack_price ';
			
			--filter by ts, ecn_launch when we get data from table new_orders
			IF check_ts = 0 THEN
				IF ts_id <> '0' AND ts_id <> '-1' THEN
					str_sql_no:=str_sql_no || ' and ts_id + 1 =' || ts_id;
				ELSIF ts_id = '-1' THEN
					str_sql_no:=str_sql_no || ' and bought_shares <> sold_shares ';
				END IF;
			ELSE
				IF strpos(ts_id,'-1') = 0 THEN 
					str_sql_no:=str_sql_no || ' and ts_id + 1 in ('|| ts_id ||')';
				ELSE 
					str_sql_no:=str_sql_no || ' and ((ts_id + 1 in ('|| ts_id ||')) OR (bought_shares <> sold_shares))';
				END IF;
			END IF;
			
			IF cecn <> '0' THEN
				str_sql_no:=str_sql_no || ' and ecn_launch =''' || cecn ||'''';
			END IF;
			
			IF ecn_filter = 'arcaen' THEN
				str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Entry''' ;
				entry_exit_filter:= 'Entry';
			ELSIF ecn_filter = 'arcaex' THEN
				str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Exit''' ;
				entry_exit_filter:= 'Exit';
			ELSIF ecn_filter = 'ouchen' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Entry''';
				entry_exit_filter:= 'Entry';
			ELSIF ecn_filter = 'ouchex' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Exit''';
				entry_exit_filter:= 'Exit';
			ELSIF ecn_filter = 'rashen' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Entry''';
				entry_exit_filter:= 'Entry';
			ELSIF ecn_filter = 'rashex' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Exit''';
				entry_exit_filter:= 'Exit';				
			ELSIF ecn_filter = 'batzen' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Entry''';
				entry_exit_filter:= 'Entry';				
			ELSIF ecn_filter = 'batzex' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Exit''';
				entry_exit_filter:= 'Exit';				
			ELSIF ecn_filter = 'oubxen' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''OUBX'' and entry_exit=''Entry''';
				entry_exit_filter:= 'Entry';
			ELSIF ecn_filter = 'oubxex' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''OUBX'' and entry_exit=''Exit''';
				entry_exit_filter:= 'Exit';
			ELSIF ecn_filter = 'rabxen' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''RABX'' and entry_exit=''Entry''';
				entry_exit_filter:= 'Entry';
			ELSIF ecn_filter = 'rabxex' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''RABX'' and entry_exit=''Exit''';
				entry_exit_filter:= 'Exit';
			ELSIF ecn_filter = 'nyseen' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Entry''';
				entry_exit_filter:= 'Entry';
			ELSIF ecn_filter = 'nyseex' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Exit''';
				entry_exit_filter:= 'Exit';
			ELSIF ecn_filter = 'edgxen' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''EDGX'' and entry_exit=''Entry''';
				entry_exit_filter:= 'Entry';
			ELSIF ecn_filter = 'edgxex' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''EDGX'' and entry_exit=''Exit''';
				entry_exit_filter:= 'Exit';
			END IF;

			IF symb <>'' THEN
				str_sql:=str_sql || ' and symbol='''|| symb ||'''';
				str_sql_no:=str_sql_no || ' and symbol='''|| symb ||'''';
			END IF;
			--Filter by ISO Odrer
			IF is_iso = '1' AND from_date > '2010-01-01' THEN
				str_sql_no:=str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
			END IF;
			-- tmp_str := str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
	
			IF is_iso = '2' AND from_date > '2010-01-01' THEN
				str_sql_no:=str_sql_no || ' and (is_iso <> ''Y'')';
			END IF;
			
			str_sql_no:=str_sql_no || ' GROUP BY (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + cross_id)' ;

			IF bt_shares > 0 THEN
				str_sql:=str_sql || ' and bought_shares >=' || bt_shares;
			END IF;
			IF sd_shares > 0 THEN
				str_sql:=str_sql || ' and sold_shares >=' || sd_shares;
			END IF;

			str_sql:=str_sql || ' and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) in ('|| str_sql_no ||')';
			
			IF ecn_filter <> '0' AND exclusive_val = 1 THEN
				str_sql_multi_orders := get_query_string_array_id_multi_orders(to_date, from_date, entry_exit_filter);
				str_sql :=str_sql || ' and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) not in (' || str_sql_multi_orders || ')';
			END IF;
			
			for missed_trade_record_of_ouch in EXECUTE str_sql loop
				count_ouch_missed_trade := missed_trade_record_of_ouch.count_missed;
				sum_ouch_expected_pl := missed_trade_record_of_ouch.sum_expected_pl;
			end loop;

			IF count_ouch_missed_trade > 0 THEN
				summary.count_repriced := count_ouch_missed_trade;				
				summary.count_miss := -count_ouch_missed_trade;				
			END IF;

		END IF;

		--filter by  ECN by Entry/Exit
		new_orders_table := '(SELECT n1.date, n1.ts_id, n1.cross_id, n1.ecn, n1.time_stamp, n1.price, n1.ack_price, n1.status, n1.entry_exit, n1.left_shares, n1.shares, n1.is_iso, n2.ecn_launch ' ||
						'FROM new_orders n1 '
						'LEFT JOIN (SELECT DISTINCT date, ts_id, cross_id, ecn_launch '
						'			FROM new_orders '
						'			WHERE entry_exit = ''Entry'' AND ts_id <> -1 AND time_stamp between ''' || min_time || ''' AND '''|| temp_time || ''') n2 '
						'ON n1.date = n2.date AND n1.ts_id = n2.ts_id AND n1.cross_id = n2.cross_id) AS tmp ';
						
		str_sql:='SELECT * FROM trade_results WHERE date >='''|| to_date || ''' and date <='''|| from_date ||''' and '|| share_size_condition ||' and expected_pl >=' || expected_profit ;
		str_sql_no :='SELECT (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + cross_id) as tmpcol FROM ' || new_orders_table || ' WHERE date >='''|| to_date || ''' and date <='''|| from_date ||'''';
		str_sql_no :=str_sql_no || ' and time_stamp between ''' || min_time || ''' and '''|| temp_time || '''';
		
		--filter by ts, ecn launch
		IF check_ts = 0 THEN
			IF ts_id <> '0' AND ts_id <> '-1' THEN
				str_sql_no:=str_sql_no || ' and ts_id + 1 =' || ts_id;
			ELSIF ts_id = '-1' THEN
				str_sql_no:=str_sql_no || ' and bought_shares <> sold_shares ';
			END IF;
		ELSE
			IF strpos(ts_id,'-1') = 0 THEN 
				str_sql_no:=str_sql_no || ' and ts_id + 1 in ('|| ts_id ||')';
			ELSE 
				str_sql_no:=str_sql_no || ' and ((ts_id + 1 in ('|| ts_id ||')) OR (bought_shares <> sold_shares))';
			END IF;
		END IF;
		
		IF cecn <> '0' THEN
			str_sql_no:=str_sql_no || ' and ecn_launch =''' || cecn ||'''';
		END IF;
		
		IF ecn_filter = 'arcaen' THEN
			str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Entry''' ;
			entry_exit_filter:= 'Entry';
		ELSIF ecn_filter = 'arcaex' THEN
			str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Exit''' ;
			entry_exit_filter:= 'Exit';
		ELSIF ecn_filter = 'ouchen' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Entry''';
			entry_exit_filter:= 'Entry';
		ELSIF ecn_filter = 'ouchex' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Exit''';
			entry_exit_filter:= 'Exit';
		ELSIF ecn_filter = 'rashen' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Entry''';
			entry_exit_filter:= 'Entry';
		ELSIF ecn_filter = 'rashex' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Exit''';	
			entry_exit_filter:= 'Exit';			
		ELSIF ecn_filter = 'batzen' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Entry''';	
			entry_exit_filter:= 'Entry';
		ELSIF ecn_filter = 'batzex' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Exit''';
			entry_exit_filter:= 'Exit';			
		ELSIF ecn_filter = 'nyseen' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Entry''';
			entry_exit_filter:= 'Entry';
		ELSIF ecn_filter = 'nyseex' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Exit''';
			entry_exit_filter:= 'Exit';	
		ELSIF ecn_filter = 'edgxen' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''EDGX'' and entry_exit=''Entry''';
			entry_exit_filter:= 'Entry';
		ELSIF ecn_filter = 'edgxex' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''EDGX'' and entry_exit=''Exit''';
			entry_exit_filter:= 'Exit';	
		END IF;
		
		--Filter by ISO Odrer
		IF is_iso = '1' AND from_date > '2010-01-01' THEN
			str_sql_no:=str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
		END IF;
		-- tmp_str := str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
	
		IF is_iso = '2' AND from_date > '2010-01-01' THEN
			str_sql_no:=str_sql_no || ' and (is_iso <> ''Y'')';
		END IF;
		
		str_sql_no:=str_sql_no || ' GROUP BY (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + cross_id)' ;

		-- filter by Symbol, Bought Shares, Sold Shares
		IF symb <>'' THEN
			str_sql:=str_sql || ' and symbol='''|| symb ||'''';
		END IF;
		IF bt_shares > 0 THEN
			str_sql:=str_sql || ' and bought_shares >=' || bt_shares;
		END IF;
		IF sd_shares > 0 THEN
			str_sql:=str_sql || ' and sold_shares >=' || sd_shares;
		END IF;
		--filter by ts
		IF check_ts = 0 THEN
			IF ts_id <> '0' THEN 
				str_sql:=str_sql || ' and ts_id + 1 =' || ts_id;
			ELSIF ts_id = '-1' THEN
				str_sql := str_sql || ' and bought_shares <> sold_shares ';
			END IF;
		ELSE
			IF strpos(ts_id,'-1') = 0 THEN 
				str_sql:=str_sql || ' and ts_id + 1 in ('|| ts_id ||')';
			ELSE 
				str_sql:=str_sql || ' and ((ts_id + 1 in ('|| ts_id ||')) OR (bought_shares <> sold_shares))';
			END IF;
		   
		END IF;
		IF cross_size <> '' then
			str_sql := str_sql || ' and ' || cross_size;
		END IF;
		
		str_sql:=str_sql || ' and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) in ('|| str_sql_no ||')';
		
		IF ecn_filter <> '0' AND exclusive_val = 1 THEN
			str_sql_multi_orders := get_query_string_array_id_multi_orders(to_date, from_date, entry_exit_filter);
			str_sql :=str_sql || ' and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) not in (' || str_sql_multi_orders || ')';
		END IF;
		
		--calculate for trade_statistics table	
		FOR trade_result in EXECUTE str_sql loop
			shares_filled := trade_result.bought_shares;
			--RAISE NOTICE 'Variable an_integer was changed.';			
			IF (shares_filled > trade_result.sold_shares) THEN
			  shares_filled := trade_result.sold_shares;
			END IF;

			shares_stuck := trade_result.bought_shares - trade_result.sold_shares;    

			IF (shares_stuck = 0) THEN
				profit_loss := trade_result.total_sold - trade_result.total_bought - trade_result.ts_total_fee;

				IF (shares_filled = 0) THEN
				summary.count_miss := summary.count_miss + 1;				
				ELSE
					IF (profit_loss < 0) THEN
						summary.count_loss := summary.count_loss + 1;						
					ELSE
						IF ( profit_loss::real < trade_result.expected_pl::real) THEN
							summary.count_profit_under := summary.count_profit_under + 1;						
						ELSE
							summary.count_profit_above := summary.count_profit_above + 1;							
						END IF;
					END IF;
				END IF;
			ELSE
				--Calculate for stuck
				IF (trade_result.pm_profit_loss - trade_result.pm_total_fee < -0.0001) THEN
					--stuck_loss
					summary.count_stuck_loss := summary.count_stuck_loss + 1;					
				ELSE
					--stuck_profit
					summary.count_stuck_profit := summary.count_stuck_profit + 1;				
				END IF;
			END IF;

		END LOOP;

		
		summary.time_stamp = t_current;
		min_time := t_current;	
	
		RETURN NEXT summary;
		
	END LOOP;
	RETURN;
end
$$;

ALTER FUNCTION public.cal_filter_trade_statistic_byecn2_twt2_with_step(to_date date, from_date date, time_from time without time zone, time_to time without time zone, share_size integer, expected_profit double precision, symb text, bt_shares integer, sd_shares integer, ecn_filter text, ts_id text, cecn text, step integer, is_iso text, exclusive_val integer, cross_size text) OWNER TO postgres;


--
-- Name: cancel_all_hung_order(text, date); Type: FUNCTION; Schema: public; Owner: postgres
--

DROP FUNCTION IF EXISTS cancel_all_hung_order(text, date);

CREATE OR REPLACE FUNCTION cancel_all_hung_order(list_order_id text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE

	hung_order_id integer[] = string_to_array(list_order_id, '_');
	count integer;
	not_cancel integer;
	tempOrder record;
	tempCancel record;
	msg_content text;
BEGIN
	count = 1;
	not_cancel = 0;
	while hung_order_id[count] IS NOT NULL LOOP
		select * into tempOrder from new_orders where oid = hung_order_id[count];
		count = count + 1;
		msg_content = '';
		IF tempOrder.ecn = 'ARCA' AND tempOrder.ecn_suffix IS NULL then
			msg_content = msg_content || '-50=4';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-46=' || tempOrder.order_id;
		ELSIF tempOrder.ecn = 'ARCA' AND tempOrder.ecn_suffix = 'FIX' then
			msg_content = msg_content || '35=8';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '11=' || tempOrder.order_id;
		ELSIF tempOrder.ecn = 'OUCH' THEN
			msg_content = msg_content || '-13=O';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-12=' || tempOrder.order_id;	
		ELSIF tempOrder.ecn = 'RASH' THEN
			msg_content = msg_content || '-13=C'; 
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-12=' || tempOrder.order_id;
		ELSIF tempOrder.ecn = 'NYSE' THEN
			msg_content = msg_content || '-50=D1';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-46=' || tempOrder.order_id;
		ELSIF tempOrder.ecn = 'BATZ' THEN
			msg_content = msg_content || '-50=F';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-46=' || tempOrder.order_id;
		ELSIF tempOrder.ecn = 'EDGX' THEN
			msg_content = msg_content || '-13=C';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-12=' || tempOrder.order_id;
		END IF;
		msg_content = msg_content || E'\001';
		
		select * into tempCancel from cancels where oid = tempOrder.oid;
		IF  tempCancel.oid IS NULL THEN
			INSERT INTO cancels(oid, shares, time_stamp, msgseq_num,sending_time, origclord_id , arcaex_order_id, exec_id, ord_status, text, msg_content, manual_update, processed_time_stamp)
			VALUES(tempOrder.oid, tempOrder.shares, tempOrder.time_stamp, tempOrder.msgseq_num, NULL, tempOrder.order_id, NULL, NULL , NULL, NULL, msg_content, 1, tempOrder.time_stamp);
			
			UPDATE new_orders SET status = 'CanceledByECN' where oid = tempOrder.oid;			
			UPDATE trading_status SET hung_order = 1;
		ELSE
			not_cancel = not_cancel + 1;
		END IF;
		
	end loop;

	RETURN not_cancel;
END
$$;


ALTER FUNCTION public.cancel_all_hung_order(list_order_id text) OWNER TO postgres;



--
-- Name: insert_cancel_hung_order(integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE OR REPLACE FUNCTION insert_cancel_hung_order(oid_t integer, cancel_shares integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	temporder record;
	tempCancel record;
	msg_content text;
BEGIN

	select * into tempOrder from new_orders where oid = oid_t;
	
	select * into tempCancel from cancels where oid = oid_t;
	msg_content = '';
	IF tempOrder.ecn = 'ARCA' AND tempOrder.ecn_suffix IS NULL then
		msg_content = msg_content || '-50=4';
		msg_content = msg_content || E'\001';
		msg_content = msg_content || '-46=' || tempOrder.order_id;
	ELSIF tempOrder.ecn = 'ARCA' AND tempOrder.ecn_suffix = 'FIX' then
		msg_content = msg_content || '35=8';
		msg_content = msg_content || E'\001';
		msg_content = msg_content || '11=' || tempOrder.order_id;
	ELSIF tempOrder.ecn = 'OUCH' THEN
		msg_content = msg_content || '-13=O';
		msg_content = msg_content || E'\001';
		msg_content = msg_content || '-12=' || tempOrder.order_id;
	ELSIF tempOrder.ecn = 'RASH' THEN
		msg_content = msg_content || '-13=C'; 
		msg_content = msg_content || E'\001';
		msg_content = msg_content || '-12=' || tempOrder.order_id;
	ELSIF tempOrder.ecn = 'NYSE' THEN
		msg_content = msg_content || '-50=D1';
		msg_content = msg_content || E'\001';
		msg_content = msg_content || '-46=' || tempOrder.order_id;
	ELSIF tempOrder.ecn = 'BATZ' THEN
		msg_content = msg_content || '-50=F';
		msg_content = msg_content || E'\001';
		msg_content = msg_content || '-46=' || tempOrder.order_id;
	ELSIF tempOrder.ecn = 'EDGX' THEN
		msg_content = msg_content || '-13=C';
		msg_content = msg_content || E'\001';
		msg_content = msg_content || '-12=' || tempOrder.order_id;
	END IF;
	
	msg_content = msg_content || E'\001';
	
	IF tempCancel.oid is NULL then
		INSERT INTO cancels(oid, shares, time_stamp, msgseq_num, origclord_id, msg_content, manual_update, processed_time_stamp)
		VALUES(tempOrder.oid, cancel_shares, tempOrder.time_stamp, tempOrder.msgseq_num, tempOrder.order_id , msg_content, 1, tempOrder.time_stamp);
		
		UPDATE new_orders SET status = 'CanceledByECN' WHERE oid = oid_t;
		UPDATE trading_status SET hung_order = 1;	
	ELSE
		return 0;
	END IF;
	
	return 1;
END
$$;

ALTER FUNCTION public.insert_cancel_hung_order(oid_t integer, cancel_shares integer) OWNER TO postgres;



--
-- Name: insert_fill_hung_order(integer, text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE OR REPLACE FUNCTION insert_fill_hung_order(oid_t integer, list_shares text, list_price text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	shares_array integer[] = string_to_array(list_shares, '_');
	price_array double precision[] = string_to_array(list_price, '_');
	tempOrder record;
	tempFill record;
	i integer;
	liquidity character;
	msg_content text;
	id_current integer;
	exce_id_tmp text;
	total_fill_shares integer;
	totalFee double precision;
BEGIN
 
	i = 1;
	total_fill_shares = 0;
	select * into tempOrder from new_orders where oid = oid_t;
	if tempOrder.ecn = 'ARCA' then
		liquidity = 'R';
	elsif tempOrder.ecn = 'OUCH' then
		liquidity = 'R';
	elsif tempOrder.ecn = 'RASH' then
		liquidity = 'R';
	elsif tempOrder.ecn = 'NYSE' then
		liquidity = '1';
	elsif tempOrder.ecn = 'BATZ' then
		liquidity = 'R';
	elsif tempOrder.ecn = 'EDGX' then
		liquidity = '6';
	end if;
	select max(id) into id_current from fills; 
	IF id_current IS NULL THEN
		id_current = 1;
	END IF;
	exce_id_tmp = '';
	WHILE shares_array[i] IS NOT NULL LOOP
		msg_content = '';
		total_fill_shares = total_fill_shares + shares_array[i];
		if tempOrder.ecn = 'ARCA' AND tempOrder.ecn_suffix IS NULL then
			msg_content = msg_content || '-50=2';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-46=' || tempOrder.order_id;
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-44=' || shares_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-43=' || (price_array[i]*100);
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-42=' || '2';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-29=' || '99999';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-28=' || 'R';
		ELSIF tempOrder.ecn = 'ARCA' AND tempOrder.ecn_suffix = 'FIX' then
			msg_content = msg_content || '35=8';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '11=' || tempOrder.order_id;
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '38=' || shares_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '44=' || price_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '17=' || '99999';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '9730=' || 'R';
		ELSIF tempOrder.ecn = 'OUCH' THEN
			msg_content = msg_content || '-13=E';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-12=' || tempOrder.order_id;
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-10=' || shares_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-8=' || price_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-2=' || 'R';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-29=' || '99999';
			msg_content = msg_content || E'\001';
		ELSIF tempOrder.ecn = 'RASH' THEN
			msg_content = msg_content || '-13=E';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-12=' || tempOrder.order_id;
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-10=' || shares_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-8=' || price_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-2=' || 'R';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-29=' || '99999';
			msg_content = msg_content || E'\001';
		ELSIF tempOrder.ecn = 'NYSE' THEN
			msg_content = msg_content || '-50=81';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-10=' || shares_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-43=' || (price_array[i]*100);
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-42=' || '2';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-29=' || '99999';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-28=' || '1';
		ELSIF tempOrder.ecn = 'BATZ' THEN
			msg_content = msg_content || '-50=11';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-81=' || shares_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-83=' || price_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-28=' || 'R';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-29=' || '99999';
			msg_content = msg_content || E'\001';
		ELSIF tempOrder.ecn = 'EDGX' THEN
			msg_content = msg_content || '-13=E';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-10=' || shares_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-8=' || (price_array[i]);
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-2=' || '6';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-29=' || '99999';
			msg_content = msg_content || E'\001';
		END IF;
		
		msg_content = msg_content || E'\001';
		id_current = id_current + 1;
		exce_id_tmp = '99999' || '_' || id_current;
		
		-- Calculate fill fee
		SELECT calculate_total_fee(price_array[i], shares_array[i], substring(tempOrder.side from 1 for 1), liquidity, tempOrder.ecn) into totalFee;
		
		INSERT INTO fills(oid, shares, price, time_stamp, processed_time_stamp, msgseq_num, origclord_id, exec_id, msg_content, liquidity,  manual_update, total_fee)
			VALUES(oid_t, shares_array[i], price_array[i], tempOrder.time_stamp, tempOrder.time_stamp, tempOrder.msgseq_num, tempOrder.order_id, exce_id_tmp, msg_content, liquidity, 1, totalFee);
		
		i = i + 1;
	END LOOP;
	
	IF total_fill_shares < tempOrder.shares THEN
		UPDATE new_orders SET status = 'Lived' where oid = oid_t;
		UPDATE new_orders SET left_shares = (tempOrder.shares - total_fill_shares) where oid = oid_t;
	ELSE
		UPDATE new_orders SET status = 'Closed' where oid = oid_t;
		UPDATE new_orders SET left_shares = 0 where oid = oid_t;
	END IF;
	
	UPDATE trading_status SET hung_order = 1;
	
	return 1;
END
$$;
ALTER FUNCTION public.insert_fill_hung_order(oid_t integer, list_shares text, list_price text) OWNER TO postgres;



COMMIT;