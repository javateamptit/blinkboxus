* Download all scripts to upgrade:
- Please copy the folder "20140113" to the EAA database host
- Change into "20140113" directory (on the EAA database host)

* The update is needed to run with new release, all databases will upgrade (eaa10_201401 -> eaa10_201206):
1) To upgrade, please run the command:
	$chmod 744 upgrade_20140113.sh
	$./upgrade_20140113.sh
