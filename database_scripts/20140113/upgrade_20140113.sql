BEGIN;

CREATE OR REPLACE FUNCTION update_ecn_launch() RETURNS INTEGER
	LANGUAGE plpgsql
	AS $$
DECLARE
	_tmp_no RECORD;
	_array_raw_content TEXT[];
	_launch CHARACTER(4);

BEGIN
	FOR _tmp_no IN (SELECT ts_id, ts_cid, date, raw_content FROM raw_crosses WHERE raw_content LIKE '2%') LOOP
	
		_array_raw_content := string_to_array(rtrim(_tmp_no.raw_content, ' '), ' ', NULL);
		IF _array_raw_content[6] = '0' THEN _launch := 'ARCA';
		ELSIF _array_raw_content[6] = '1' THEN _launch := 'NDAQ';
		ELSIF _array_raw_content[6] = '2' THEN _launch := 'NYSE';
		ELSIF _array_raw_content[6] = '3' THEN _launch := 'BATZ';
		ELSIF _array_raw_content[6] = '4' THEN _launch := 'EDGX';
		ELSE
		END IF;
		
		UPDATE new_orders SET ecn_launch = _launch WHERE ts_id = _tmp_no.ts_id AND cross_id = _tmp_no.ts_cid AND date = _tmp_no.date AND entry_exit = 'Entry';
		
	END LOOP;
	
	RETURN 0;
END
$$;

COMMIT;

SELECT update_ecn_launch();