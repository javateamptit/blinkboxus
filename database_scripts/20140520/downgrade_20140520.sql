BEGIN;

-- Downgrade TT version
DELETE FROM release_versions WHERE module_id = 2 AND version = '20140520';

-- DROP MDC/OEC settings from trade_servers relation
ALTER TABLE trade_servers DROP COLUMN IF EXISTS book_amex;

COMMIT;
