BEGIN;

INSERT INTO release_versions(module_id, version) VALUES (2, '20140520');

ALTER TABLE trade_servers ADD COLUMN book_amex INTEGER DEFAULT 1;

ALTER TABLE new_orders ADD COLUMN visible_shares integer DEFAULT 0;

DROP TRIGGER str_update_ack_price ON order_acks;
DROP FUNCTION update_ack_price();

CREATE OR REPLACE FUNCTION _insert_tt_activities() RETURNS trigger
  LANGUAGE plpgsql AS $$
DECLARE
    userVal                 text;
    typeVal                 integer;
    dateVal                 date;
    timeVal                 time;
    currentState    text[];
    _currentState   integer;
    status                  text[];
    _status                 integer;
    action                  text[];
    _action                 integer;
    orderId                 text;
    actionType              integer;
    symbol                  text;
    buyingPower     double precision;
    onWhich                 text[];
    _onWhich                integer;
    descriptionVal  text;
    _tsId     integer;
    _ecnId      integer;
    _tsDescription  text;
    _ecnName    text;
    _maxShares    integer;
    _maxConsectutiveLoss integer;
    _maxStuck   integer;
    _count      integer;
    _activityAction text;
    _minSpreadTS  text;
    _maxLoserTS   text;
    _maxBuyingPower text;
    _checkUserOrServer integer;
    _maxCross       integer;
    _asVolatileThreshold text;
    _tsVolatileThreshold text;
    _asDisabledThreshold text;
    _tsDisabledThreshold text;
    ts_name_mapping          text[];
    as_name_mapping          text[];
    venue_book_mapping         text[];
    venue_order_mapping        text[];
    _tmp RECORD;
  BEGIN
    timeVal := NEW.time_stamp;
    userVal := initcap(trim(NEW.username));
    typeVal = NEW.activity_type;
    dateVal = (NEW.date);
    timeVal = (NEW.time_stamp);

    --Init status
    status[0] := 'Off-Line';
    status[1] := 'Active';
    status[2] := 'Active Assistant';
    status[3] := 'Monitoring';

    currentState[0] := '[Off-Line]';
    currentState[1] := '[Active]';
    currentState[2] := '[Active Assistant]';
    currentState[3] := '[Monitoring]';

    --Init action
    action[0] := 'Add';
    action[1] := 'Remove';
    
    -- Venue orders
    venue_order_mapping := ARRAY['ARCA', 'NDAQ', 'NYSE', 'RASH', 'BZX', 'EDGX', 'EDGA', 'BX', 'BYX', 'PSX'];
    
    -- Venue books
    venue_book_mapping := ARRAY['ARCA', 'ISLD', 'NYSE', 'BATZ', 'EDGX', 'EDGA', 'BX', 'BYX', 'PSX', 'AMEX'];

    FOR _tmp IN (SELECT ts_id, description FROM trade_servers ORDER BY ts_id ASC) LOOP
      ts_name_mapping[_tmp.ts_id - 1] := _tmp.description;
    END LOOP;
    
    -- AS: mid 0, PM: mid 1
    FOR _tmp IN (SELECT mid, description FROM as_settings ORDER BY mid ASC) LOOP
      as_name_mapping[_tmp.mid] := _tmp.description;
    END LOOP;
    
    --Init enable/disable trading
    _tsId := 1;
    FOREACH _tsDescription IN ARRAY ts_name_mapping LOOP
      onWhich[_tsId * 100 + 0 + 0] := ' trading all orders on ' || _tsDescription;
      
      _ecnId := 1;
      FOREACH _ecnName IN ARRAY venue_order_mapping LOOP
        --RAISE NOTICE 'Init enable/disable trading: %, %', _tsDescription, _ecnName;
        onWhich[_tsId * 100 + 0 + _ecnId] := _ecnName || ' trading on ' || _tsDescription;
        
        _ecnId := _ecnId + 1;
      END LOOP;
      
      _tsId := _tsId + 1;
    END LOOP;

    onWhich[99] :=  as_name_mapping[1] || ' trading';

    --Init connect/disconnect all MDCs and orders, enable/disable trading all orders on all TS
    onWhich[0] := 'trading all orders on all TSs and PM';
    onWhich[1] := 'all MDCs and orders on all TSs and PM';

    --Init connect/disconnect BBO
    onWhich[160] := 'BBO on ' || ts_name_mapping[0];
    onWhich[161] := 'CQS on ' || ts_name_mapping[0];
    onWhich[162] := 'UQDF on ' || ts_name_mapping[0];

    onWhich[260] := 'BBO on ' || ts_name_mapping[1];
    onWhich[261] := 'CQS on ' || ts_name_mapping[1];
    onWhich[262] := 'UQDF on ' || ts_name_mapping[1];

    onWhich[360] := 'BBO on ' || ts_name_mapping[2];
    onWhich[361] := 'CQS on ' || ts_name_mapping[2];
    onWhich[362] := 'UQDF on ' || ts_name_mapping[2];

    onWhich[460] := 'BBO on ' || ts_name_mapping[3];
    onWhich[461] := 'CQS on ' || ts_name_mapping[3];
    onWhich[462] := 'UQDF on ' || ts_name_mapping[3];

    onWhich[560] := 'BBO on ' || ts_name_mapping[4];
    onWhich[561] := 'CQS on ' || ts_name_mapping[4];
    onWhich[562] := 'UQDF on ' || ts_name_mapping[4];

    onWhich[660] := 'BBO on ' || ts_name_mapping[5];
    onWhich[661] := 'CQS on ' || ts_name_mapping[5];
    onWhich[662] := 'UQDF on ' || ts_name_mapping[5];
    
    onWhich[760] := 'BBO on ' || ts_name_mapping[6];
    onWhich[761] := 'CQS on ' || ts_name_mapping[6];
    onWhich[762] := 'UQDF on ' || ts_name_mapping[6];
    
    onWhich[860] := 'BBO on ' || ts_name_mapping[7];
    onWhich[861] := 'CQS on ' || ts_name_mapping[7];
    onWhich[862] := 'UQDF on ' || ts_name_mapping[7];
    
    onWhich[960] := 'BBO on ' || ts_name_mapping[8];
    onWhich[961] := 'CQS on ' || ts_name_mapping[8];
    onWhich[962] := 'UQDF on ' || ts_name_mapping[8];

    onWhich[80] := 'BBO on ' || as_name_mapping[1];
    onWhich[81] := 'CTS on ' || as_name_mapping[1];
    onWhich[82] := 'UTDF on ' || as_name_mapping[1];
    onWhich[83] := 'CQS on ' || as_name_mapping[1];
    onWhich[84] := 'UQDF on ' || as_name_mapping[1];

    --Init connect/disconnect MDC
    _tsId := 1;
    FOREACH _tsDescription IN ARRAY ts_name_mapping LOOP
      onWhich[_tsId * 100 + 20 + 0] := ' all MDCs on ' || _tsDescription;
      
      _ecnId := 1;
      FOREACH _ecnName IN ARRAY venue_book_mapping LOOP
        --RAISE NOTICE 'Init connect/disconnect book: %, %', _tsDescription, _ecnName;
        onWhich[_tsId * 100 + 20 + _ecnId] := _ecnName || ' MDC on ' || _tsDescription;
        
        _ecnId := _ecnId + 1;
      END LOOP;
      
      _tsId := _tsId + 1;
    END LOOP;

    onWhich[750] := 'all MDCs on ' || as_name_mapping[1];
    onWhich[751] := 'ARCA MDC on ' || as_name_mapping[1];
    onWhich[752] := 'NDAQ MDC on ' || as_name_mapping[1];

    --Init connect/disconnect OEC
    _tsId := 1;
    FOREACH _tsDescription IN ARRAY ts_name_mapping LOOP
      onWhich[_tsId * 100 + 40 + 0] := ' all OECs on ' || _tsDescription;
      
      _ecnId := 1;
      FOREACH _ecnName IN ARRAY venue_order_mapping LOOP
        --RAISE NOTICE 'Init connect/disconnect order: %, %', _tsDescription, _ecnName;
        onWhich[_tsId * 100 + 40 + _ecnId] := _ecnName || ' OEC on ' || _tsDescription;
        
        _ecnId := _ecnId + 1;
      END LOOP;
      
      _tsId := _tsId + 1;
    END LOOP;

    onWhich[10] := 'all OECs on ' || as_name_mapping[0];
    onWhich[11] := 'ARCA OEC on ' || as_name_mapping[0];
    onWhich[12] := 'RASH OEC on ' || as_name_mapping[0];
    
    onWhich[50] := 'all OECs on ' || as_name_mapping[1];
    onWhich[51] := 'ARCA OEC on ' || as_name_mapping[1];
    onWhich[52] := 'RASH OEC on ' || as_name_mapping[1];

    -- Connect to AS
    IF (typeVal = 1) THEN
      SELECT substring(NEW.log_detail from 1 for 1)::integer INTO _currentState FROM tt_event_log;
      descriptionVal := userVal || ' ' || currentState[_currentState] || ' Connect to AS ' || dateVal || ' ' || timeVal;

    -- Switch trader role
    ELSIF (typeVal = 2) THEN
      SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer INTO _currentState, _status FROM tt_event_log;
      descriptionVal := userVal || ' ' || currentState[_currentState] || ' Client became ' || status[_status] || ' Trader ' || dateVal || ' ' || timeVal;

    -- Disconnect from AS
    ELSIF (typeVal = 3) THEN
      SELECT substring(NEW.log_detail from 1 for 1)::integer INTO _currentState FROM tt_event_log;
      descriptionVal := userVal || ' ' || currentState[_currentState] || ' Disconnect from AS ' || dateVal || ' ' || timeVal;

    -- Sent Manual OEC
    ELSIF (typeVal = 4) THEN
      SELECT substring(NEW.log_detail from 1 for 1)::integer, trim(substring(NEW.log_detail from 3 for 3)) INTO _currentState, orderId FROM tt_event_log;
      descriptionVal := userVal || ' ' || currentState[_currentState] || ' Sent Manual Order, OrderID = ' || orderId || ' ' || dateVal || ' ' || timeVal;

    -- Ignored stock list for TS
    ELSIF (typeVal = 5) THEN
      SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, trim(substring(NEW.log_detail from 5 for 8)) INTO _currentState, _action, symbol FROM tt_event_log;
      descriptionVal := userVal || ' ' || currentState[_currentState] || ' ' || action[_action] || ' ignored stock list for TS, symbol = ' || symbol || ' ' || dateVal || ' ' || timeVal;

    -- Halted stock list
    ELSIF (typeVal = 6) THEN
      SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, trim(substring(NEW.log_detail from 5 for 8)) INTO _currentState, _action, symbol FROM tt_event_log;
      descriptionVal := userVal || ' ' || currentState[_currentState] || ' ' || action[_action] || ' halted stock list, symbol = ' || symbol || ' ' || dateVal || ' ' || timeVal;

    -- PM engage/disengage
    ELSIF (typeVal = 7) THEN
      SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, trim(substring(NEW.log_detail from 5 for 8)) INTO _currentState, actionType, symbol FROM tt_event_log;
      -- engage
      IF (actionType = 0) THEN
        descriptionVal := userVal || ' ' || currentState[_currentState] || ' PM engage, symbol = ' || symbol || ' ' || dateVal || ' ' || timeVal;
      -- disengage
      ELSIF (actionType = 1) THEN
        descriptionVal := userVal || ' ' || currentState[_currentState] || ' PM disengage, symbol = ' || symbol || ' ' || dateVal || ' ' || timeVal;
      ELSE
      END IF;

    -- set buying power
    -- use _count variable for accountIndex
    ELSIF (typeVal = 8) THEN
      SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1):: integer, substring(NEW.log_detail, 5)::double precision INTO _currentState, _count, buyingPower FROM tt_event_log;
      
      IF (_count = 0) THEN
        _tsDescription := ' on first account ';
      ELSIF (_count = 1) THEN
        _tsDescription := ' on second account ';
      ELSIF (_count = 2) THEN
        _tsDescription := ' on third account ';
      ELSE
        _tsDescription := ' on fourth account ';
      END IF;
        
      descriptionVal := userVal || ' ' || currentState[_currentState] || ' Set Buying Power = ' || buyingPower || _tsDescription || ' ' || dateVal || ' ' || timeVal;

    -- Enable/Disable trading
    ELSIF (typeVal = 9) THEN
      SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, substring(NEW.log_detail from 5 for 3) INTO _currentState, actionType, _onWhich FROM tt_event_log;
      -- Enable
      IF (actionType = 0) THEN
        descriptionVal := userVal || ' ' || currentState[_currentState] || ' Enable ' || onWhich[_onWhich] || ' ' || dateVal || ' ' || timeVal;
      -- Disable
      ELSIF (actionType = 1) THEN
        descriptionVal := userVal || ' ' || currentState[_currentState] || ' Disable ' || onWhich[_onWhich] || ' ' || dateVal || ' ' || timeVal;
      ELSE
      END IF;

    -- Ignore stock list for PM
    ELSIF (typeVal = 10) THEN
      SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, trim(substring(NEW.log_detail from 5 for 8)) INTO _currentState, _action, symbol FROM tt_event_log;
      descriptionVal := userVal || ' ' || currentState[_currentState] || ' ' || action[_action] || ' ignored stock list for PM, symbol = ' || symbol || ' ' || dateVal || ' ' || timeVal;

    -- Connect/disconnect book(s) on TS and PM
    ELSIF (typeVal = 11) THEN
      SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, substring(NEW.log_detail from 5 for 3) INTO _currentState, actionType, _onWhich FROM tt_event_log;
      -- Connect
      IF (actionType = 0) THEN
        descriptionVal := userVal || ' ' || currentState[_currentState] || ' Connect to ' || onWhich[_onWhich] || ' ' || dateVal || ' ' || timeVal;
      -- Disconnect
      ELSIF (actionType = 1) THEN
        descriptionVal := userVal || ' ' || currentState[_currentState] || ' Disconnect and disable trading ' || onWhich[_onWhich] || ' ' || dateVal || ' ' || timeVal;
      ELSE
      END IF;

    -- Connect/disconnect order(s) on TS, AS and PM
    ELSIF (typeVal = 12) THEN
      SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, substring(NEW.log_detail from 5 for 3) INTO _currentState, actionType, _onWhich FROM tt_event_log;
      -- Connect
      IF (actionType = 0) THEN
         descriptionVal := userVal || ' ' || currentState[_currentState] || ' Connect to ' || onWhich[_onWhich] || ' ' || dateVal || ' ' || timeVal;
      -- Disconnect
      ELSIF (actionType = 1) THEN
         descriptionVal := userVal || ' ' || currentState[_currentState] || ' Disconnect and disable trading ' || onWhich[_onWhich] || ' ' || dateVal || ' ' || timeVal;
      ELSE
      END IF;

    -- connect/disconnect BBO on TS and PM
    ELSIF (typeVal = 13) THEN
      SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, substring(NEW.log_detail from 5 for 3) INTO _currentState, actionType, _onWhich FROM tt_event_log;
      -- Connect
      IF (actionType = 0) THEN
         descriptionVal := userVal || ' ' || currentState[_currentState] || ' Connect to ' || onWhich[_onWhich] || ' ' || dateVal || ' ' || timeVal;
      -- Disconnect
      ELSIF (actionType = 1) THEN
         descriptionVal := userVal || ' ' || currentState[_currentState] || ' Disconnect ' || onWhich[_onWhich] || ' ' || dateVal || ' ' || timeVal;
      ELSE
      END IF;
      
    -- enable/disable ISO trading on TS
    ELSIF (typeVal = 20) THEN
      SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, substring(NEW.log_detail, 5) INTO _currentState, actionType, _tsId FROM tt_event_log;
      
      IF (_tsId = -1) THEN
        -- Enable
        IF (actionType = 1) THEN
          descriptionVal := userVal || ' ' || currentState[_currentState] || ' Enable ISO trading ' || dateVal || ' ' || timeVal;
        -- Disable
        ELSIF (actionType = 0) THEN
          descriptionVal := userVal || ' ' || currentState[_currentState] || ' Disable ISO trading ' || dateVal || ' ' || timeVal;
        ELSE
        END IF;
      ELSE
        -- Enable
        IF (actionType = 1) THEN
          descriptionVal := userVal || ' ' || currentState[_currentState] || ' Enable ISO trading on ' || ts_name_mapping[_tsId - 1] || ' ' || dateVal || ' ' || timeVal;
        -- Disable
        ELSIF (actionType = 0) THEN
          descriptionVal := userVal || ' ' || currentState[_currentState] || ' Disable ISO trading on ' || ts_name_mapping[_tsId - 1] || ' ' || dateVal || ' ' || timeVal;
        ELSE
        END IF;
      END IF;
    
    -- ACTIVITY_TYPE_MAX_CROSS_PER_MIN_SETTING 21
    ELSIF (typeVal = 21) THEN
      SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail, 3) INTO _currentState, _maxCross FROM tt_event_log;
    
      descriptionVal := userVal || ' ' || currentState[_currentState] || ' Set max crosses per minute = ' || _maxCross || dateVal || ' ' || timeVal;
      
    -- ACTIVITY_TYPE_AS_VOLATILE_THRESHOLD_SETTING 22
    ELSIF (typeVal = 22) THEN
      SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail, 3) INTO _currentState, _asVolatileThreshold FROM tt_event_log;
    
      descriptionVal := userVal || ' ' || currentState[_currentState] || ' Set AS volatile threshold = ' || _asVolatileThreshold || dateVal || ' ' || timeVal;
      
    -- ACTIVITY_TYPE_TS_VOLATILE_THRESHOLD_SETTING 23
    ELSIF (typeVal = 23) THEN
      SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail, 3) INTO _currentState, _tsVolatileThreshold FROM tt_event_log;
    
      descriptionVal := userVal || ' ' || currentState[_currentState] || ' Set TS volatile threshold = ' || _tsVolatileThreshold || dateVal || ' ' || timeVal;
      
    -- ACTIVITY_TYPE_AS_DISABLED_THRESHOLD_SETTING 24
    ELSIF (typeVal = 24) THEN
      SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail, 3) INTO _currentState, _asDisabledThreshold FROM tt_event_log;
    
      descriptionVal := userVal || ' ' || currentState[_currentState] || ' Set AS disabled threshold = ' || _asDisabledThreshold || dateVal || ' ' || timeVal;
      
    -- ACTIVITY_TYPE_TS_DISABLED_THRESHOLD_SETTING 25
    ELSIF (typeVal = 25) THEN
      SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail, 3) INTO _currentState, _tsDisabledThreshold FROM tt_event_log;
    
      descriptionVal := userVal || ' ' || currentState[_currentState] || ' Set TS disabled threshold = ' || _tsDisabledThreshold || dateVal || ' ' || timeVal;
    
    ELSE
    
      SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer INTO _currentState, _tsId FROM tt_event_log;
      
      IF (_tsId >= 1) THEN 
        _tsDescription := ' on ' || ts_name_mapping[_tsId - 1];
      END IF;
      
      SELECT count(*) INTO _checkUserOrServer FROM user_login_info WHERE username = NEW.username;
      
      -- Check activity type was inserted on SOD or NOT
      SELECT count(*) INTO _count FROM tt_event_log WHERE date = dateVal AND activity_type = typeVal AND username = NEW.username;
      
      -- ACTIVITY_TYPE_MAX_CONSECUTIVE_LOSS_SETTING 14
      IF (typeVal = 14) THEN
        IF (_checkUserOrServer = 0) THEN
          IF (_count = 0) THEN
            _activityAction := ' SOD: Set max consecutive loss = ';
          ELSE 
            RETURN OLD;
          END IF;
        ELSE
          _activityAction := ' Set max consecutive loss = ';
        END IF;
        
        SELECT substring(NEW.log_detail, 5) INTO _maxConsectutiveLoss FROM tt_event_log ;
    
        descriptionVal := userVal || ' ' || currentState[_currentState] || _activityAction ||  _maxConsectutiveLoss || _tsDescription || dateVal || ' ' || timeVal;
      
      -- ACTIVITY_TYPE_MAX_STUCKS_SETTING 15
      ELSIF (typeVal = 15) THEN
        IF (_checkUserOrServer = 0) THEN
          IF (_count = 0) THEN
            _activityAction := ' SOD: Set max stucks = ';
          ELSE 
            RETURN OLD;
          END IF;
        ELSE
          _activityAction := ' Set max stucks = ';
        END IF;

        SELECT substring(NEW.log_detail, 5) INTO _maxStuck FROM tt_event_log;
      
        descriptionVal := userVal || ' ' || currentState[_currentState] || _activityAction || _maxStuck || _tsDescription || dateVal || ' ' || timeVal;
      
      -- ACTIVITY_TYPE_CURRENCY_MIN_SPREAD_TS_SETTING 16
      ELSIF (typeVal = 16) THEN
        IF (_checkUserOrServer = 0) THEN
          IF (_count = 0) THEN
            _activityAction := ' SOD: Set min spread = ';
          ELSE
            _activityAction := ' Set min spread = ';
          END IF;
        ELSE
          _activityAction := ' Set min spread = ';
        END IF;
      
        SELECT substring(NEW.log_detail, 5) INTO _minSpreadTS FROM tt_event_log;
        
        descriptionVal := userVal || ' ' || currentState[_currentState] || _activityAction || _minSpreadTS || _tsDescription || dateVal || ' ' || timeVal;
    
      -- ACTIVITY_TYPE_CURRENCY_MAX_LOSER_TS_SETTING 17
      ELSIF (typeVal = 17) THEN
        IF (_checkUserOrServer = 0) THEN
          IF (_count = 0) THEN
            _activityAction := ' SOD: Set max loser = ';
          ELSE RETURN OLD;
          END IF;
        ELSE
          _activityAction := ' Set max loser = ';
        END IF;
      
        SELECT substring(NEW.log_detail, 5) INTO _maxLoserTS FROM tt_event_log;
      
        descriptionVal := userVal || ' ' || currentState[_currentState] || _activityAction || _maxLoserTS || _tsDescription || dateVal || ' ' || timeVal;
      
      -- ACTIVITY_TYPE_MAX_BUYING_POWER_SETTING 18
      ELSIF (typeVal = 18) THEN
        IF (_checkUserOrServer = 0) THEN
          IF (_count = 0) THEN
            _activityAction := ' SOD: Set max buying power = ';
          ELSE 
            RETURN OLD;
          END IF;
        ELSE
          _activityAction := ' Set max buying power = ';
        END IF;
      
        SELECT substring(NEW.log_detail, 5) INTO _maxBuyingPower FROM tt_event_log;
      
        descriptionVal := userVal || ' ' || currentState[_currentState] || _activityAction || _maxBuyingPower || _tsDescription || dateVal || ' ' || timeVal;
      
      -- ACTIVITY_TYPE_MAX_SHARES_SETTING 19
      ELSIF (typeVal = 19) THEN
        IF (_checkUserOrServer = 0) THEN
          IF (_count = 0) THEN
            _activityAction := ' SOD: Set max shares = ';
          ELSE 
            RETURN OLD;
          END IF;
        ELSE
          _activityAction := ' Set max shares = ';
        END IF;
      
        SELECT substring(NEW.log_detail, 5) INTO _maxShares FROM tt_event_log;
      
        descriptionVal := userVal || ' ' || currentState[_currentState] || _activityAction || _maxShares || _tsDescription || dateVal || ' ' || timeVal;
      
      END IF;

    END IF;
    
    NEW.description := descriptionVal;
    RETURN NEW;
  END;
$$;


ALTER FUNCTION public._insert_tt_activities() OWNER TO postgres;

CREATE FUNCTION insert_order_accepted(_orderid integer, _date date, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _exOrderId text, _msgContents text, _ackPrice double precision, _ackShares integer, _status text) RETURNS integer
  LANGUAGE plpgsql AS $$
DECLARE
  newOrder record;
BEGIN
  
  SELECT * INTO newOrder 
  FROM new_orders 
  WHERE order_id = _orderID AND date = _date;
  
  IF newOrder IS NULL THEN
    RETURN -1;
  END IF;
  
  INSERT INTO order_acks (oid, time_stamp, processed_time_stamp, msgseq_num, origclord_id, arcaex_order_id, msg_content, price) 
  VALUES (newOrder.oid, _timeStamp, _processedTimestamp, _seqNum, _orderID, _exOrderId, _msgContents, _ackPrice);
  
  IF (_ackShares = 0) THEN
    _ackShares := newOrder.shares;
  END IF;
  
  IF (_ackPrice = 0) THEN
    _ackPrice := newOrder.price;
  END IF;
  
  UPDATE new_orders 
  SET status = _status, ack_price = _ackPrice, left_shares = _ackShares, avg_price =  _ackPrice 
  WHERE oid = newOrder.oid;
  
  RETURN 0;
END
$$;

ALTER FUNCTION public.insert_order_accepted(_orderid integer, _date date, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _exOrderId text, _msgContents text, _ackPrice double precision, _ackShares integer, _status text) OWNER TO postgres;

CREATE FUNCTION insert_order_executed(_orderid integer, _date date, _shares integer, _price double precision, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _exorderid text, _execid text, _msgcontents text, _liquidity character, _status text, _leftshares integer, _avgprice double precision, _entryexitrefnum integer, _totalfee double precision) RETURNS integer
  LANGUAGE plpgsql AS $$
DECLARE
  orderID integer;
BEGIN
  
  SELECT oid INTO orderID 
  FROM new_orders 
  WHERE order_id = _orderID AND date = _date;
  
  IF orderID IS NULL THEN
    RETURN -1;
  END IF;
  
  INSERT INTO fills (oid, shares, price, time_stamp, processed_time_stamp, msgseq_num, origclord_id, arcaex_order_id, exec_id, msg_content, liquidity, entry_exit_ref, total_fee) 
  VALUES (orderID, _shares, _price, _timestamp, _processedTimestamp, _seqnum, _orderID, _exorderid, _execid, _msgcontents, _liquidity, _entryExitRefNum, _totalFee);

  UPDATE new_orders 
  SET status = _status, left_shares = _leftshares, avg_price = _avgprice 
  WHERE oid = orderID;
  
  RETURN 0;
END$$;

ALTER FUNCTION public.insert_order_executed(_orderid integer, _date date, _shares integer, _price double precision, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _exorderid text, _execid text, _msgcontents text, _liquidity character, _status text, _leftshares integer, _avgprice double precision, _entryexitrefnum integer, _totalfee double precision) OWNER TO postgres;


CREATE FUNCTION insert_order_canceled(_orderid integer, _date date, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _arcaex_orderid text, _msgcontents text, _status text, _entryexitrefnum integer) RETURNS integer
    LANGUAGE plpgsql AS $$
DECLARE
  orderID integer;
BEGIN
  
  SELECT oid INTO orderID 
  FROM new_orders 
  WHERE order_id = _orderID AND date = _date;
  
  IF orderID IS NULL THEN
    RETURN -1;
  END IF;

  INSERT INTO cancels (oid, time_stamp, processed_time_stamp, msgseq_num, origclord_id, arcaex_order_id, msg_content, entry_exit_ref) 
  VALUES (orderID, _timeStamp, _processedtimestamp, _seqNum, _orderID, _arcaex_orderid, _msgContents, _entryExitRefNum);
    
  UPDATE new_orders 
  SET status = _status 
  WHERE oid = orderID;
  
  RETURN 0;
END
$$;

ALTER FUNCTION public.insert_order_canceled(_orderid integer, _date date, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _arcaex_orderid text, _msgcontents text, _status text, _entryexitrefnum integer) OWNER TO postgres;


CREATE FUNCTION insert_cancel_request(_orderid integer, _date date, _timestamp time without time zone, _seqnum text, _exorderid text, _msgcontents text, _status text) RETURNS integer
  LANGUAGE plpgsql AS $$
DECLARE
  orderID integer;
BEGIN
  
  SELECT oid INTO orderID 
  FROM new_orders 
  WHERE order_id = _orderID AND date = _date;
  
  IF orderID IS NULL THEN
    RETURN -1;
  END IF;

  INSERT INTO cancel_requests (oid, time_stamp, msgseq_num, origclord_id, arcaex_order_id, msg_content) 
  VALUES (orderID, _timeStamp, _seqNum, _orderID, _exOrderId, _msgContents);
  
  UPDATE new_orders 
  SET status = _status 
  WHERE oid = orderID;
  
  RETURN 0;
END
$$;

ALTER FUNCTION public.insert_cancel_request(_orderid integer, _date date, _timestamp time without time zone, _seqnum text, _exorderid text, _msgcontents text, _status text) OWNER TO postgres;


CREATE FUNCTION insert_cancel_ack(_orderid integer, _date date, _timestamp time without time zone, _seqnum text, _exorderid text, _msgcontents text, _status text) RETURNS integer
  LANGUAGE plpgsql AS $$
DECLARE
  orderID integer;
BEGIN
  
  SELECT oid INTO orderID 
  FROM new_orders 
  WHERE order_id = _orderID AND date = _date;
  
  IF orderID IS NULL THEN
    RETURN -1;
  END IF;
  
  INSERT INTO cancel_acks (oid, time_stamp, msgseq_num, origclord_id, arcaex_order_id, msg_content) 
  VALUES (orderID, _timeStamp, _seqNum, _orderID, _exorderid, _msgContents);
  
  UPDATE new_orders 
  SET status = _status 
  WHERE oid = orderID;
  
  RETURN 0;
END
$$;

ALTER FUNCTION public.insert_cancel_ack(_orderid integer, _date date, _timestamp time without time zone, _seqnum text, _exorderid text, _msgcontents text, _status text) OWNER TO postgres;


CREATE FUNCTION insert_cancel_pending(_orderid integer, _date date, _timestamp time without time zone, _seqnum text, _exorderid text, _shares integer, _price double precision, _msgcontents text, _status text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
  orderID integer;
BEGIN
  
  SELECT oid INTO orderID 
  FROM new_orders 
  WHERE order_id = _orderID AND date = _date;
  
  IF orderID IS NULL THEN
    RETURN -1;
  END IF;

  INSERT INTO cancel_pendings (oid, time_stamp, msgseq_num, origclord_id, arcaex_order_id, shares, price, msg_content) 
  VALUES (orderID, _timeStamp, _seqNum, _orderID, _exorderid, _shares, _price, _msgContents);
  
  UPDATE new_orders 
  SET status = _status 
  WHERE oid = orderID;
  
  RETURN 0;
END
$$;

ALTER FUNCTION public.insert_cancel_pending(_orderid integer, _date date, _timestamp time without time zone, _seqnum text, _exorderid text, _shares integer, _price double precision, _msgcontents text, _status text) OWNER TO postgres;


CREATE FUNCTION insert_bust_or_correct(_orderid integer, _date date, _shares integer, _price double precision, _timestamp time without time zone, _seqnum text, _executionid text, _msgcontents text, _status text) RETURNS integer
  LANGUAGE plpgsql AS $$
DECLARE
  orderID integer;
BEGIN
  
  SELECT oid INTO orderID 
  FROM new_orders 
  WHERE order_id = _orderID AND date = _date;
  
  IF orderID IS NULL THEN
    RETURN -1;
  END IF;
  
  INSERT INTO broken_trades (oid, shares, price, time_stamp, msgseq_num, origclord_id, exec_id, msg_content) 
  VALUES (orderID, _shares, _price, _timeStamp, _seqNum, _orderID, _executionId, _msgContents);
  
  UPDATE new_orders 
  SET status = _status 
  WHERE oid = orderID;
  
  RETURN 0;
END
$$;

ALTER FUNCTION public.insert_bust_or_correct(_orderid integer, _date date, _shares integer, _price double precision, _timestamp time without time zone, _seqnum text, _executionid text, _msgcontents text, _status text) OWNER TO postgres;


CREATE FUNCTION insert_cancel_rejected(_orderid integer, _date date, _timestamp time without time zone, _seqnum text, _exorderid text, _reason text, _cancelreject text, _msgcontents text, _status text) RETURNS integer
  LANGUAGE plpgsql AS $$
DECLARE
  orderID integer;
BEGIN
  
  SELECT oid INTO orderID 
  FROM new_orders 
  WHERE order_id = _orderID AND date = _date;
  
  IF orderID IS NULL THEN
    RETURN -1;
  END IF;

  INSERT INTO cancel_rejects (oid, time_stamp, msgseq_num, origclord_id, arcaex_order_id, reason, cancel_reject, msg_content) 
  VALUES (orderID, _timeStamp, _seqNum, _orderID, _exorderid, _reason, _cancelReject, _msgContents);
  
  UPDATE new_orders 
  SET status = _status 
  WHERE oid = orderID;
  
  RETURN 0;
END
$$;

ALTER FUNCTION public.insert_cancel_rejected(_orderid integer, _date date, _timestamp time without time zone, _seqnum text, _exorderid text, _reason text, _cancelreject text, _msgcontents text, _status text) OWNER TO postgres;


CREATE FUNCTION insert_order_rejected(_orderid integer, _date date, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _reason text, _text text, _msgcontents text, _status text, _entryexitrefnum integer) RETURNS integer
  LANGUAGE plpgsql AS $$
DECLARE
  orderID integer;
BEGIN
  SELECT oid INTO orderID 
  FROM new_orders 
  WHERE order_id = _orderID AND date = _date;
  
  IF orderID IS NULL THEN
    RETURN -1;
  END IF;
  
  INSERT INTO order_rejects (oid, time_stamp, processed_time_stamp, msgseq_num, origclord_id, reason, text, msg_content, entry_exit_ref) 
  VALUES (orderID, _timeStamp, _processedtimestamp, _seqNum, _orderID, _reason, _text, _msgContents, _entryExitRefNum);

  UPDATE new_orders 
  SET status = _status 
  WHERE oid = orderID;
  
  RETURN 0;
END
$$;

ALTER FUNCTION public.insert_order_rejected(_orderid integer, _date date, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _reason text, _text text, _msgcontents text, _status text, _entryexitrefnum integer) OWNER TO postgres;

COMMIT;
