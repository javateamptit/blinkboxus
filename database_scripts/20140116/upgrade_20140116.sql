BEGIN;

INSERT INTO release_versions(module_id, version) VALUES (2, '20140116');

DROP VIEW IF EXISTS vwstatistics_twt;
CREATE VIEW vwstatistics_twt AS
    SELECT new_orders.id, new_orders.oid, new_orders.tid, new_orders.order_id, new_orders.entry_exit, new_orders.ask_bid, new_orders.side, new_orders.time_stamp, new_orders.ecn, new_orders.shares, new_orders.price, new_orders.time_in_force, new_orders.left_shares, new_orders.status, new_orders.msgseq_num, new_orders.msg_content, new_orders.symbol, new_orders.avg_price, new_orders.date, new_orders.ts_id, new_orders.stock_type, new_orders.ecn_launch, new_orders.cross_id, new_orders.ecn_suffix, new_orders.trade_type, new_orders.is_iso, order_acks.price AS ack_price, new_orders.routing_inst FROM (new_orders JOIN order_acks ON ((new_orders.oid = order_acks.oid)));


ALTER TABLE public.vwstatistics_twt OWNER TO postgres;

COMMIT;
