--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'SQL_ASCII';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: ack_fill_struct; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE ack_fill_struct AS (
  ack integer[],
  ack_count integer[],
  fill integer[],
  fill_count integer[]
);


ALTER TYPE public.ack_fill_struct OWNER TO postgres;

--
-- Name: ask_bid_struct; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE ask_bid_struct AS (
  ask_success integer[],
  ask_missed integer[],
  bid_success integer[],
  bid_missed integer[]
);


ALTER TYPE public.ask_bid_struct OWNER TO postgres;

--
-- Name: avg_struct; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE avg_struct AS (
  ack integer[],
  fill integer[]
);


ALTER TYPE public.avg_struct OWNER TO postgres;

--
-- Name: entry_exit_struct; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE entry_exit_struct AS (
  entry_success integer[],
  entry_missed integer[],
  exit_success integer[],
  exit_missed integer[]
);


ALTER TYPE public.entry_exit_struct OWNER TO postgres;

--
-- Name: orders_statistic; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE orders_statistic AS (
  success integer[],
  missed integer[]
);


ALTER TYPE public.orders_statistic OWNER TO postgres;

--
-- Name: orders_status_struc; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE orders_status_struc AS (
  buy_fill integer[],
  buy_cancel integer[],
  buy_reject integer[],
  sell_fill integer[],
  sell_cancel integer[],
  sell_reject integer[]
);


ALTER TYPE public.orders_status_struc OWNER TO postgres;

--
-- Name: stock_type_struct; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE stock_type_struct AS (
  buy_success integer[],
  buy_missed integer[],
  sell_success integer[],
  sell_missed integer[]
);


ALTER TYPE public.stock_type_struct OWNER TO postgres;

--
-- Name: trade_result_statistic; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE trade_result_statistic AS (
	count_miss integer,
	count_repriced integer,
	count_stuck_loss integer,
	count_stuck_profit integer,
	count_loss integer,
	count_profit_under integer,
	count_profit_above integer,
	count_total integer,
	shares_traded_miss integer,
	shares_traded_repriced integer,
	shares_traded_stuck_loss integer,
	shares_traded_stuck_profit integer,
	shares_traded_loss integer,
	shares_traded_profit_under integer,
	shares_traded_profit_above integer,
	shares_traded_total integer,
	pm_shares_traded_miss integer,
	pm_shares_traded_repriced integer,
	pm_shares_traded_stuck_loss integer,
	pm_shares_traded_stuck_profit integer,
	pm_shares_traded_loss integer,
	pm_shares_traded_profit_under integer,
	pm_shares_traded_profit_above integer,
	pm_shares_traded_total integer,
	expected_miss double precision,
	expected_repriced double precision,
	expected_stuck_loss double precision,
	expected_stuck_profit double precision,
	expected_loss double precision,
	expected_profit_under double precision,
	expected_profit_above double precision,
	expected_total double precision,
	real_miss double precision,
	real_stuck_loss double precision,
	real_stuck_profit double precision,
	real_loss double precision,
	real_profit_under double precision,
	real_profit_above double precision,
	real_total double precision,
	pm_miss double precision,
	pm_stuck_loss double precision,
	pm_stuck_profit double precision,
	pm_loss double precision,
	pm_profit_under double precision,
	pm_profit_above double precision,
	pm_total double precision,
	launch_shares_miss integer,
	launch_shares_repriced integer,
	launch_shares_stuck_loss integer,
	launch_shares_stuck_profit integer,
	launch_shares_loss integer,
	launch_shares_profit_under integer,
	launch_shares_profit_above integer,
	launch_shares_total integer
);


ALTER TYPE public.trade_result_statistic OWNER TO postgres;

--
-- Name: trade_result_summary; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE trade_result_summary AS (
  count_miss integer,
  count_repriced integer,
  count_stuck integer,
  count_loss integer,
  count_profit_under integer,
  count_profit_above integer,
  count_total integer,
  expected_miss double precision,
  expected_repriced double precision,
  expected_stuck double precision,
  expected_loss double precision,
  expected_profit_under double precision,
  expected_profit_above double precision,
  expected_total double precision,
  real_miss double precision,
  real_stuck double precision,
  real_loss double precision,
  real_profit_under double precision,
  real_profit_above double precision,
  real_total double precision
);


ALTER TYPE public.trade_result_summary OWNER TO postgres;

--
-- Name: trade_result_summary2; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE trade_result_summary2 AS (
  count_miss integer,
  count_repriced integer,
  count_stuck_loss integer,
  count_stuck_profit integer,
  count_loss integer,
  count_profit_under integer,
  count_profit_above integer,
  count_total integer,
  expected_miss double precision,
  expected_repriced double precision,
  expected_stuck_loss double precision,
  expected_stuck_profit double precision,
  expected_loss double precision,
  expected_profit_under double precision,
  expected_profit_above double precision,
  expected_total double precision,
  real_miss double precision,
  real_stuck_loss double precision,
  real_stuck_profit double precision,
  real_loss double precision,
  real_profit_under double precision,
  real_profit_above double precision,
  real_total double precision,
  pm_miss double precision,
  pm_stuck_loss double precision,
  pm_stuck_profit double precision,
  pm_loss double precision,
  pm_profit_under double precision,
  pm_profit_above double precision,
  pm_total double precision
);


ALTER TYPE public.trade_result_summary2 OWNER TO postgres;

--
-- Name: trade_result_summary2_struct; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE trade_result_summary2_struct AS (
  time_stamp time without time zone,
  count_miss integer,
  count_repriced integer,
  count_stuck_loss integer,
  count_stuck_profit integer,
  count_loss integer,
  count_profit_under integer,
  count_profit_above integer
);


ALTER TYPE public.trade_result_summary2_struct OWNER TO postgres;

--
-- Name: trade_result_summary_shares_traded; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE trade_result_summary_shares_traded AS (
  count_miss integer,
  count_repriced integer,
  count_stuck_loss integer,
  count_stuck_profit integer,
  count_loss integer,
  count_profit_under integer,
  count_profit_above integer,
  count_total integer,
  shares_traded_miss integer,
  shares_traded_repriced integer,
  shares_traded_stuck_loss integer,
  shares_traded_stuck_profit integer,
  shares_traded_loss integer,
  shares_traded_profit_under integer,
  shares_traded_profit_above integer,
  shares_traded_total integer,
  pm_shares_traded_miss integer,
  pm_shares_traded_repriced integer,
  pm_shares_traded_stuck_loss integer,
  pm_shares_traded_stuck_profit integer,
  pm_shares_traded_loss integer,
  pm_shares_traded_profit_under integer,
  pm_shares_traded_profit_above integer,
  pm_shares_traded_total integer,
  expected_miss double precision,
  expected_repriced double precision,
  expected_stuck_loss double precision,
  expected_stuck_profit double precision,
  expected_loss double precision,
  expected_profit_under double precision,
  expected_profit_above double precision,
  expected_total double precision,
  real_miss double precision,
  real_stuck_loss double precision,
  real_stuck_profit double precision,
  real_loss double precision,
  real_profit_under double precision,
  real_profit_above double precision,
  real_total double precision,
  pm_miss double precision,
  pm_stuck_loss double precision,
  pm_stuck_profit double precision,
  pm_loss double precision,
  pm_profit_under double precision,
  pm_profit_above double precision,
  pm_total double precision
);


ALTER TYPE public.trade_result_summary_shares_traded OWNER TO postgres;

--
-- Name: _insert_or_update_cee_filing_notes_to_broken_table(text, date, character, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION _insert_or_update_cee_filing_notes_to_broken_table(_list_fill_ids text, _tradedate date, _status character, _note text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
  tempFill record;
  tempBroken record;
  fill_id_list integer[] = string_to_array(_list_fill_ids, ',');
  count integer;
  
BEGIN
  count = 1;
  while fill_id_list[count] IS NOT NULL LOOP
    select * into tempFill from fills where fill_id = fill_id_list[count];
    count = count + 1;
    
    select * into tempBroken from brokens where oid = tempFill.oid and exec_id = tempFill.exec_id;
      IF  tempBroken.oid IS NULL THEN
        insert into brokens(fill_id, new_shares, new_price, broken_shares,trade_date,oid, exec_id, filing_status, cee_note, is_cee)
          values(tempFill.fill_id, tempFill.shares, tempFill.price, tempFill.shares, _tradeDate, tempFill.oid, tempFill.exec_id, _status, _note, true);
      ELSE
        update brokens set new_shares = tempFill.shares, new_price = tempFill.price, trade_date = _tradeDate, filing_status = _status, cee_note = _note, is_cee = true 
          where oid = tempFill.oid and exec_id = tempFill.exec_id;
      END IF;
    
  end loop;

  RETURN 0;
END
$$;


ALTER FUNCTION public._insert_or_update_cee_filing_notes_to_broken_table(_list_fill_ids text, _tradedate date, _status character, _note text) OWNER TO postgres;

--
-- Name: _insert_tt_activities(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION _insert_tt_activities() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    userVal                 text;
    typeVal                 integer;
    dateVal                 date;
    timeVal                 time;
    currentState    text[];
    _currentState   integer;
    status                  text[];
    _status                 integer;
    action                  text[];
    _action                 integer;
    orderId                 text;
    actionType              integer;
    symbol                  text;
    buyingPower     double precision;
    onWhich                 text[];
    _onWhich                integer;
    descriptionVal  text;
    _tsId     integer;
    _ecnId      integer;
    _tsDescription  text;
    _ecnName    text;
    _maxShares    integer;
    _maxConsectutiveLoss integer;
    _maxStuck   integer;
    _count      integer;
    _activityAction text;
    _minSpreadTS  text;
    _maxLoserTS   text;
    _maxBuyingPower text;
    _checkUserOrServer integer;
    _maxCross       integer;
    _asVolatileThreshold text;
    _tsVolatileThreshold text;
    _asDisabledThreshold text;
    _tsDisabledThreshold text;
    ts_name_mapping          text[];
    as_name_mapping          text[];
    venue_book_mapping         text[];
    venue_order_mapping        text[];
    _tmp RECORD;
  BEGIN
    timeVal := NEW.time_stamp;
    userVal := initcap(trim(NEW.username));
    typeVal = NEW.activity_type;
    dateVal = (NEW.date);
    timeVal = (NEW.time_stamp);

    --Init status
    status[0] := 'Off-Line';
    status[1] := 'Active';
    status[2] := 'Active Assistant';
    status[3] := 'Monitoring';

    currentState[0] := '[Off-Line]';
    currentState[1] := '[Active]';
    currentState[2] := '[Active Assistant]';
    currentState[3] := '[Monitoring]';

    --Init action
    action[0] := 'Add';
    action[1] := 'Remove';
    
    -- Venue orders
    venue_order_mapping := ARRAY['ARCA', 'NDAQ', 'NYSE', 'RASH', 'BZX', 'EDGX', 'EDGA', 'BX', 'BYX', 'PSX'];
    
    -- Venue books
    venue_book_mapping := ARRAY['ARCA', 'ISLD', 'NYSE', 'BATZ', 'EDGX', 'EDGA', 'BX', 'BYX', 'PSX', 'AMEX'];

    FOR _tmp IN (SELECT ts_id, description FROM trade_servers ORDER BY ts_id ASC) LOOP
      ts_name_mapping[_tmp.ts_id - 1] := _tmp.description;
    END LOOP;
    
    -- AS: mid 0, PM: mid 1
    FOR _tmp IN (SELECT mid, description FROM as_settings ORDER BY mid ASC) LOOP
      as_name_mapping[_tmp.mid] := _tmp.description;
    END LOOP;
    
    --Init enable/disable trading
    _tsId := 1;
    FOREACH _tsDescription IN ARRAY ts_name_mapping LOOP
      onWhich[_tsId * 100 + 0 + 0] := ' trading all orders on ' || _tsDescription;
      
      _ecnId := 1;
      FOREACH _ecnName IN ARRAY venue_order_mapping LOOP
        --RAISE NOTICE 'Init enable/disable trading: %, %', _tsDescription, _ecnName;
        onWhich[_tsId * 100 + 0 + _ecnId] := _ecnName || ' trading on ' || _tsDescription;
        
        _ecnId := _ecnId + 1;
      END LOOP;
      
      _tsId := _tsId + 1;
    END LOOP;

    onWhich[99] :=  as_name_mapping[1] || ' trading';

    --Init connect/disconnect all MDCs and orders, enable/disable trading all orders on all TS
    onWhich[0] := 'trading all orders on all TSs and PM';
    onWhich[1] := 'all MDCs and orders on all TSs and PM';

    --Init connect/disconnect BBO
    onWhich[160] := 'BBO on ' || ts_name_mapping[0];
    onWhich[161] := 'CQS on ' || ts_name_mapping[0];
    onWhich[162] := 'UQDF on ' || ts_name_mapping[0];

    onWhich[260] := 'BBO on ' || ts_name_mapping[1];
    onWhich[261] := 'CQS on ' || ts_name_mapping[1];
    onWhich[262] := 'UQDF on ' || ts_name_mapping[1];

    onWhich[360] := 'BBO on ' || ts_name_mapping[2];
    onWhich[361] := 'CQS on ' || ts_name_mapping[2];
    onWhich[362] := 'UQDF on ' || ts_name_mapping[2];

    onWhich[460] := 'BBO on ' || ts_name_mapping[3];
    onWhich[461] := 'CQS on ' || ts_name_mapping[3];
    onWhich[462] := 'UQDF on ' || ts_name_mapping[3];

    onWhich[560] := 'BBO on ' || ts_name_mapping[4];
    onWhich[561] := 'CQS on ' || ts_name_mapping[4];
    onWhich[562] := 'UQDF on ' || ts_name_mapping[4];

    onWhich[660] := 'BBO on ' || ts_name_mapping[5];
    onWhich[661] := 'CQS on ' || ts_name_mapping[5];
    onWhich[662] := 'UQDF on ' || ts_name_mapping[5];
    
    onWhich[760] := 'BBO on ' || ts_name_mapping[6];
    onWhich[761] := 'CQS on ' || ts_name_mapping[6];
    onWhich[762] := 'UQDF on ' || ts_name_mapping[6];
    
    onWhich[860] := 'BBO on ' || ts_name_mapping[7];
    onWhich[861] := 'CQS on ' || ts_name_mapping[7];
    onWhich[862] := 'UQDF on ' || ts_name_mapping[7];
    
    onWhich[960] := 'BBO on ' || ts_name_mapping[8];
    onWhich[961] := 'CQS on ' || ts_name_mapping[8];
    onWhich[962] := 'UQDF on ' || ts_name_mapping[8];

    onWhich[80] := 'BBO on ' || as_name_mapping[1];
    onWhich[81] := 'CTS on ' || as_name_mapping[1];
    onWhich[82] := 'UTDF on ' || as_name_mapping[1];
    onWhich[83] := 'CQS on ' || as_name_mapping[1];
    onWhich[84] := 'UQDF on ' || as_name_mapping[1];

    --Init connect/disconnect MDC
    _tsId := 1;
    FOREACH _tsDescription IN ARRAY ts_name_mapping LOOP
      onWhich[_tsId * 100 + 20 + 0] := ' all MDCs on ' || _tsDescription;
      
      _ecnId := 1;
      FOREACH _ecnName IN ARRAY venue_book_mapping LOOP
        --RAISE NOTICE 'Init connect/disconnect book: %, %', _tsDescription, _ecnName;
        onWhich[_tsId * 100 + 20 + _ecnId] := _ecnName || ' MDC on ' || _tsDescription;
        
        _ecnId := _ecnId + 1;
      END LOOP;
      
      _tsId := _tsId + 1;
    END LOOP;

    onWhich[750] := 'all MDCs on ' || as_name_mapping[1];
    onWhich[751] := 'ARCA MDC on ' || as_name_mapping[1];
    onWhich[752] := 'NDAQ MDC on ' || as_name_mapping[1];

    --Init connect/disconnect OEC
    _tsId := 1;
    FOREACH _tsDescription IN ARRAY ts_name_mapping LOOP
      onWhich[_tsId * 100 + 40 + 0] := ' all OECs on ' || _tsDescription;
      
      _ecnId := 1;
      FOREACH _ecnName IN ARRAY venue_order_mapping LOOP
        --RAISE NOTICE 'Init connect/disconnect order: %, %', _tsDescription, _ecnName;
        onWhich[_tsId * 100 + 40 + _ecnId] := _ecnName || ' OEC on ' || _tsDescription;
        
        _ecnId := _ecnId + 1;
      END LOOP;
      
      _tsId := _tsId + 1;
    END LOOP;

    onWhich[10] := 'all OECs on ' || as_name_mapping[0];
    onWhich[11] := 'ARCA OEC on ' || as_name_mapping[0];
    onWhich[12] := 'RASH OEC on ' || as_name_mapping[0];
    
    onWhich[50] := 'all OECs on ' || as_name_mapping[1];
    onWhich[51] := 'ARCA OEC on ' || as_name_mapping[1];
    onWhich[52] := 'RASH OEC on ' || as_name_mapping[1];

    -- Connect to AS
    IF (typeVal = 1) THEN
      SELECT substring(NEW.log_detail from 1 for 1)::integer INTO _currentState FROM tt_event_log;
      descriptionVal := userVal || ' ' || currentState[_currentState] || ' Connect to AS ' || dateVal || ' ' || timeVal;

    -- Switch trader role
    ELSIF (typeVal = 2) THEN
      SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer INTO _currentState, _status FROM tt_event_log;
      descriptionVal := userVal || ' ' || currentState[_currentState] || ' Client became ' || status[_status] || ' Trader ' || dateVal || ' ' || timeVal;

    -- Disconnect from AS
    ELSIF (typeVal = 3) THEN
      SELECT substring(NEW.log_detail from 1 for 1)::integer INTO _currentState FROM tt_event_log;
      descriptionVal := userVal || ' ' || currentState[_currentState] || ' Disconnect from AS ' || dateVal || ' ' || timeVal;

    -- Sent Manual OEC
    ELSIF (typeVal = 4) THEN
      SELECT substring(NEW.log_detail from 1 for 1)::integer, trim(substring(NEW.log_detail from 3 for 3)) INTO _currentState, orderId FROM tt_event_log;
      descriptionVal := userVal || ' ' || currentState[_currentState] || ' Sent Manual Order, OrderID = ' || orderId || ' ' || dateVal || ' ' || timeVal;

    -- Ignored stock list for TS
    ELSIF (typeVal = 5) THEN
      SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, trim(substring(NEW.log_detail from 5 for 8)) INTO _currentState, _action, symbol FROM tt_event_log;
      descriptionVal := userVal || ' ' || currentState[_currentState] || ' ' || action[_action] || ' ignored stock list for TS, symbol = ' || symbol || ' ' || dateVal || ' ' || timeVal;

    -- Halted stock list
    ELSIF (typeVal = 6) THEN
      SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, trim(substring(NEW.log_detail from 5 for 8)) INTO _currentState, _action, symbol FROM tt_event_log;
      descriptionVal := userVal || ' ' || currentState[_currentState] || ' ' || action[_action] || ' halted stock list, symbol = ' || symbol || ' ' || dateVal || ' ' || timeVal;

    -- PM engage/disengage
    ELSIF (typeVal = 7) THEN
      SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, trim(substring(NEW.log_detail from 5 for 8)) INTO _currentState, actionType, symbol FROM tt_event_log;
      -- engage
      IF (actionType = 0) THEN
        descriptionVal := userVal || ' ' || currentState[_currentState] || ' PM engage, symbol = ' || symbol || ' ' || dateVal || ' ' || timeVal;
      -- disengage
      ELSIF (actionType = 1) THEN
        descriptionVal := userVal || ' ' || currentState[_currentState] || ' PM disengage, symbol = ' || symbol || ' ' || dateVal || ' ' || timeVal;
      ELSE
      END IF;

    -- set buying power
    -- use _count variable for accountIndex
    ELSIF (typeVal = 8) THEN
      SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1):: integer, substring(NEW.log_detail, 5)::double precision INTO _currentState, _count, buyingPower FROM tt_event_log;
      
      IF (_count = 0) THEN
        _tsDescription := ' on first account ';
      ELSIF (_count = 1) THEN
        _tsDescription := ' on second account ';
      ELSIF (_count = 2) THEN
        _tsDescription := ' on third account ';
      ELSE
        _tsDescription := ' on fourth account ';
      END IF;
        
      descriptionVal := userVal || ' ' || currentState[_currentState] || ' Set Buying Power = ' || buyingPower || _tsDescription || ' ' || dateVal || ' ' || timeVal;

    -- Enable/Disable trading
    ELSIF (typeVal = 9) THEN
      SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, substring(NEW.log_detail from 5 for 3) INTO _currentState, actionType, _onWhich FROM tt_event_log;
      -- Enable
      IF (actionType = 0) THEN
        descriptionVal := userVal || ' ' || currentState[_currentState] || ' Enable ' || onWhich[_onWhich] || ' ' || dateVal || ' ' || timeVal;
      -- Disable
      ELSIF (actionType = 1) THEN
        descriptionVal := userVal || ' ' || currentState[_currentState] || ' Disable ' || onWhich[_onWhich] || ' ' || dateVal || ' ' || timeVal;
      ELSE
      END IF;

    -- Ignore stock list for PM
    ELSIF (typeVal = 10) THEN
      SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, trim(substring(NEW.log_detail from 5 for 8)) INTO _currentState, _action, symbol FROM tt_event_log;
      descriptionVal := userVal || ' ' || currentState[_currentState] || ' ' || action[_action] || ' ignored stock list for PM, symbol = ' || symbol || ' ' || dateVal || ' ' || timeVal;

    -- Connect/disconnect book(s) on TS and PM
    ELSIF (typeVal = 11) THEN
      SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, substring(NEW.log_detail from 5 for 3) INTO _currentState, actionType, _onWhich FROM tt_event_log;
      -- Connect
      IF (actionType = 0) THEN
        descriptionVal := userVal || ' ' || currentState[_currentState] || ' Connect to ' || onWhich[_onWhich] || ' ' || dateVal || ' ' || timeVal;
      -- Disconnect
      ELSIF (actionType = 1) THEN
        descriptionVal := userVal || ' ' || currentState[_currentState] || ' Disconnect and disable trading ' || onWhich[_onWhich] || ' ' || dateVal || ' ' || timeVal;
      ELSE
      END IF;

    -- Connect/disconnect order(s) on TS, AS and PM
    ELSIF (typeVal = 12) THEN
      SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, substring(NEW.log_detail from 5 for 3) INTO _currentState, actionType, _onWhich FROM tt_event_log;
      -- Connect
      IF (actionType = 0) THEN
         descriptionVal := userVal || ' ' || currentState[_currentState] || ' Connect to ' || onWhich[_onWhich] || ' ' || dateVal || ' ' || timeVal;
      -- Disconnect
      ELSIF (actionType = 1) THEN
         descriptionVal := userVal || ' ' || currentState[_currentState] || ' Disconnect and disable trading ' || onWhich[_onWhich] || ' ' || dateVal || ' ' || timeVal;
      ELSE
      END IF;

    -- connect/disconnect BBO on TS and PM
    ELSIF (typeVal = 13) THEN
      SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, substring(NEW.log_detail from 5 for 3) INTO _currentState, actionType, _onWhich FROM tt_event_log;
      -- Connect
      IF (actionType = 0) THEN
         descriptionVal := userVal || ' ' || currentState[_currentState] || ' Connect to ' || onWhich[_onWhich] || ' ' || dateVal || ' ' || timeVal;
      -- Disconnect
      ELSIF (actionType = 1) THEN
         descriptionVal := userVal || ' ' || currentState[_currentState] || ' Disconnect ' || onWhich[_onWhich] || ' ' || dateVal || ' ' || timeVal;
      ELSE
      END IF;
      
    -- enable/disable ISO trading on TS
    ELSIF (typeVal = 20) THEN
      SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, substring(NEW.log_detail, 5) INTO _currentState, actionType, _tsId FROM tt_event_log;
      
      IF (_tsId = -1) THEN
        -- Enable
        IF (actionType = 1) THEN
          descriptionVal := userVal || ' ' || currentState[_currentState] || ' Enable ISO trading ' || dateVal || ' ' || timeVal;
        -- Disable
        ELSIF (actionType = 0) THEN
          descriptionVal := userVal || ' ' || currentState[_currentState] || ' Disable ISO trading ' || dateVal || ' ' || timeVal;
        ELSE
        END IF;
      ELSE
        -- Enable
        IF (actionType = 1) THEN
          descriptionVal := userVal || ' ' || currentState[_currentState] || ' Enable ISO trading on ' || ts_name_mapping[_tsId - 1] || ' ' || dateVal || ' ' || timeVal;
        -- Disable
        ELSIF (actionType = 0) THEN
          descriptionVal := userVal || ' ' || currentState[_currentState] || ' Disable ISO trading on ' || ts_name_mapping[_tsId - 1] || ' ' || dateVal || ' ' || timeVal;
        ELSE
        END IF;
      END IF;
    
    -- ACTIVITY_TYPE_MAX_CROSS_PER_MIN_SETTING 21
    ELSIF (typeVal = 21) THEN
      SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail, 3) INTO _currentState, _maxCross FROM tt_event_log;
    
      descriptionVal := userVal || ' ' || currentState[_currentState] || ' Set max crosses per minute = ' || _maxCross || dateVal || ' ' || timeVal;
      
    -- ACTIVITY_TYPE_AS_VOLATILE_THRESHOLD_SETTING 22
    ELSIF (typeVal = 22) THEN
      SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail, 3) INTO _currentState, _asVolatileThreshold FROM tt_event_log;
    
      descriptionVal := userVal || ' ' || currentState[_currentState] || ' Set AS volatile threshold = ' || _asVolatileThreshold || dateVal || ' ' || timeVal;
      
    -- ACTIVITY_TYPE_TS_VOLATILE_THRESHOLD_SETTING 23
    ELSIF (typeVal = 23) THEN
      SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail, 3) INTO _currentState, _tsVolatileThreshold FROM tt_event_log;
    
      descriptionVal := userVal || ' ' || currentState[_currentState] || ' Set TS volatile threshold = ' || _tsVolatileThreshold || dateVal || ' ' || timeVal;
      
    -- ACTIVITY_TYPE_AS_DISABLED_THRESHOLD_SETTING 24
    ELSIF (typeVal = 24) THEN
      SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail, 3) INTO _currentState, _asDisabledThreshold FROM tt_event_log;
    
      descriptionVal := userVal || ' ' || currentState[_currentState] || ' Set AS disabled threshold = ' || _asDisabledThreshold || dateVal || ' ' || timeVal;
      
    -- ACTIVITY_TYPE_TS_DISABLED_THRESHOLD_SETTING 25
    ELSIF (typeVal = 25) THEN
      SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail, 3) INTO _currentState, _tsDisabledThreshold FROM tt_event_log;
    
      descriptionVal := userVal || ' ' || currentState[_currentState] || ' Set TS disabled threshold = ' || _tsDisabledThreshold || dateVal || ' ' || timeVal;
    
    ELSE
    
      SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer INTO _currentState, _tsId FROM tt_event_log;
      
      IF (_tsId >= 1) THEN 
        _tsDescription := ' on ' || ts_name_mapping[_tsId - 1];
      END IF;
      
      SELECT count(*) INTO _checkUserOrServer FROM user_login_info WHERE username = NEW.username;
      
      -- Check activity type was inserted on SOD or NOT
      SELECT count(*) INTO _count FROM tt_event_log WHERE date = dateVal AND activity_type = typeVal AND username = NEW.username;
      
      -- ACTIVITY_TYPE_MAX_CONSECUTIVE_LOSS_SETTING 14
      IF (typeVal = 14) THEN
        IF (_checkUserOrServer = 0) THEN
          IF (_count = 0) THEN
            _activityAction := ' SOD: Set max consecutive loss = ';
          ELSE 
            RETURN OLD;
          END IF;
        ELSE
          _activityAction := ' Set max consecutive loss = ';
        END IF;
        
        SELECT substring(NEW.log_detail, 5) INTO _maxConsectutiveLoss FROM tt_event_log ;
    
        descriptionVal := userVal || ' ' || currentState[_currentState] || _activityAction ||  _maxConsectutiveLoss || _tsDescription || dateVal || ' ' || timeVal;
      
      -- ACTIVITY_TYPE_MAX_STUCKS_SETTING 15
      ELSIF (typeVal = 15) THEN
        IF (_checkUserOrServer = 0) THEN
          IF (_count = 0) THEN
            _activityAction := ' SOD: Set max stucks = ';
          ELSE 
            RETURN OLD;
          END IF;
        ELSE
          _activityAction := ' Set max stucks = ';
        END IF;

        SELECT substring(NEW.log_detail, 5) INTO _maxStuck FROM tt_event_log;
      
        descriptionVal := userVal || ' ' || currentState[_currentState] || _activityAction || _maxStuck || _tsDescription || dateVal || ' ' || timeVal;
      
      -- ACTIVITY_TYPE_CURRENCY_MIN_SPREAD_TS_SETTING 16
      ELSIF (typeVal = 16) THEN
        IF (_checkUserOrServer = 0) THEN
          IF (_count = 0) THEN
            _activityAction := ' SOD: Set min spread = ';
          ELSE
            _activityAction := ' Set min spread = ';
          END IF;
        ELSE
          _activityAction := ' Set min spread = ';
        END IF;
      
        SELECT substring(NEW.log_detail, 5) INTO _minSpreadTS FROM tt_event_log;
        
        descriptionVal := userVal || ' ' || currentState[_currentState] || _activityAction || _minSpreadTS || _tsDescription || dateVal || ' ' || timeVal;
    
      -- ACTIVITY_TYPE_CURRENCY_MAX_LOSER_TS_SETTING 17
      ELSIF (typeVal = 17) THEN
        IF (_checkUserOrServer = 0) THEN
          IF (_count = 0) THEN
            _activityAction := ' SOD: Set max loser = ';
          ELSE RETURN OLD;
          END IF;
        ELSE
          _activityAction := ' Set max loser = ';
        END IF;
      
        SELECT substring(NEW.log_detail, 5) INTO _maxLoserTS FROM tt_event_log;
      
        descriptionVal := userVal || ' ' || currentState[_currentState] || _activityAction || _maxLoserTS || _tsDescription || dateVal || ' ' || timeVal;
      
      -- ACTIVITY_TYPE_MAX_BUYING_POWER_SETTING 18
      ELSIF (typeVal = 18) THEN
        IF (_checkUserOrServer = 0) THEN
          IF (_count = 0) THEN
            _activityAction := ' SOD: Set max buying power = ';
          ELSE 
            RETURN OLD;
          END IF;
        ELSE
          _activityAction := ' Set max buying power = ';
        END IF;
      
        SELECT substring(NEW.log_detail, 5) INTO _maxBuyingPower FROM tt_event_log;
      
        descriptionVal := userVal || ' ' || currentState[_currentState] || _activityAction || _maxBuyingPower || _tsDescription || dateVal || ' ' || timeVal;
      
      -- ACTIVITY_TYPE_MAX_SHARES_SETTING 19
      ELSIF (typeVal = 19) THEN
        IF (_checkUserOrServer = 0) THEN
          IF (_count = 0) THEN
            _activityAction := ' SOD: Set max shares = ';
          ELSE 
            RETURN OLD;
          END IF;
        ELSE
          _activityAction := ' Set max shares = ';
        END IF;
      
        SELECT substring(NEW.log_detail, 5) INTO _maxShares FROM tt_event_log;
      
        descriptionVal := userVal || ' ' || currentState[_currentState] || _activityAction || _maxShares || _tsDescription || dateVal || ' ' || timeVal;
      
      END IF;

    END IF;
    
    NEW.description := descriptionVal;
    RETURN NEW;
  END;
$$;


ALTER FUNCTION public._insert_tt_activities() OWNER TO postgres;

--
-- Name: add_rm(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION add_rm(fill_idval integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
  DECLARE
  symbolVal text;
        leave_Shares  integer;
  order_Shares  integer;
  order_Price float;
  avgPrice  float;
  totalQty  float;
  netPL   float;
  totalMktValue float;
  rowcount  integer;
  dateVal   date;
  timeVal   time;
  sideVal   text;
  midVal    int;
  current_shares  int;
  current_price float;
  fill    record;
  accountVal  integer;
  BEGIN

  rowcount = 0;
  leave_Shares := 0;
  avgPrice := 0;
  netPL := 0;
  totalMktValue := 0;
  totalQty := 0;

  SELECT trim(new_orders.symbol), new_orders.date, fills.time_stamp, new_orders.side, account into symbolVal, dateVal, timeVal, sideVal, accountVal
  FROM new_orders
  JOIN fills ON new_orders.oid = fills.oid
  WHERE fills.fill_id = fill_idVal;
  
  --symbolVal := 'AEOS';
  --dateVal := cast('2007-01-04' as date);
  --Travel thought fill today and 

  for fill in (select fills.*, substring(new_orders.side from 1 for 1) as side from fills  join new_orders on fills.oid = new_orders.oid 
              where new_orders.date = dateVal and trim(new_orders.symbol) = symbolVal and new_orders.account = accountVal) loop
    
    RAISE NOTICE  'I am in loop';
    rowcount := rowcount + 1;   

    
    select new_shares, new_price into current_shares, current_price from brokens where fill_id = fill.fill_id;
    
    IF current_price is null then
      current_shares := fill.shares;
      current_price := fill.price;
    end if;

    
    IF (leave_Shares > 0) THEN -- Equivalent to LONG
      IF (fill.side = 'B') THEN -- BUY
        avgPrice := (avgPrice * leave_Shares + current_price * current_shares) / (current_shares + leave_Shares);
        leave_Shares := leave_Shares + current_shares;
      ELSE -- SELL
        netPL := netPL + ((current_price - avgPrice) * LEAST(current_shares, leave_Shares));        
        leave_Shares := leave_Shares - current_shares;
        IF (leave_Shares < 0) THEN
          avgPrice := current_price;
        ELSIF (leave_Shares = 0) THEN
          avgPrice := 0;
        END IF;
      END IF;
    ELSIF (leave_Shares < 0) THEN -- Equivalent to SHORT
      IF (fill.side = 'B') THEN -- BUY
        netPL := netPL + ((avgPrice - current_price)*LEAST(current_shares, abs(leave_Shares)));       
        leave_Shares := leave_Shares + current_shares;
        IF (leave_Shares = 0) THEN
          avgPrice := 0;
        ELSIF (leave_Shares > 0) THEN
          avgPrice := current_price;
        END IF;
      ELSE -- SELL
        avgPrice := (avgPrice*(-leave_Shares) + current_price * current_shares) / (current_shares + (-leave_Shares));
        leave_Shares := leave_Shares - current_shares;        
      END IF;
    ELSE -- leave_Shares = 0
      avgPrice := current_price;
      IF (fill.side = 'B') THEN
        leave_Shares := current_shares;
      ELSE -- Action is Sell or Short Sell
        leave_Shares := -current_shares;
      END IF;
    END IF;
  
    totalMktValue := avgPrice * abs(leave_Shares);
    totalQty := totalQty + current_shares;  
  
  end loop;
  RAISE NOTICE  'I am done.';   
    -- update RM
    UPDATE risk_managements
    SET   average_price = avgPrice,
      net_quantity = leave_Shares, 
      matched_profit_loss = netPL, 
      total_market_value = totalMktValue, 
      total_quantity = totalQty, 
      last_update = timeVal
    WHERE trim(symbol) = symbolVal and date = dateVal and account = accountVal;
  
  RETURN rowcount;
  END;
$$;


ALTER FUNCTION public.add_rm(fill_idval integer) OWNER TO postgres;

--
-- Name: array_sort(anyarray); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION array_sort(anyarray) RETURNS anyarray
    LANGUAGE sql
    AS $_$
SELECT ARRAY(
    SELECT $1[s.i] AS "foo"
    FROM
        generate_series(array_lower($1,1), array_upper($1,1)) AS s(i)
    ORDER BY foo
);
$_$;


ALTER FUNCTION public.array_sort(anyarray) OWNER TO postgres;

--
-- Name: cal_filter_trade_statistic_byecn2_twt2(date, date, time without time zone, time without time zone, text, double precision, text, integer, integer, text, text, text, text, integer, text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION cal_filter_trade_statistic_byecn2_twt2(to_date date, from_date date, time_from time without time zone, time_to time without time zone, share_size text, expected_profit double precision, symb text, bt_shares integer, sd_shares integer, ecn_filter text, ts_id text, cecn text, is_iso text, exclusive_val integer, cross_size text, launch_shares text) RETURNS trade_result_statistic
    LANGUAGE plpgsql
    AS $$
declare
  trade_result record;
  summary trade_result_statistic;
  shares_filled integer;
  shares_stuck integer;
  profit_loss double precision;
 --statistic missed trade 
  count_ouch_missed_trade integer;
  sum_shares_traded_miss integer;
  sum_launch_shares_miss integer;
  sum_ouch_expected_pl double precision;
  missed_trade_record_of_ouch record;
  str_sql text;
  str_sql1 text;
  str_sql2 text;  
  str_sql_no text;
  str_sql_no_repriced text;
  str_sql_repriced text;
  str_sql_multi_orders text;
  str_sql_tmp text;
  tmp_str text;
  tmp_ASCid text;
  new_orders_table text;
  share_size_condition text;
  launch_shares_condition text;
   -- Filter Entry or Exit
  entry_exit_filter text; 
  ecn_filter_conditions text;
  -- Check number TS
  check_ts integer;
  check_ecn_entry_exit integer;
  
begin  
  -- Initialize
  summary.count_miss := 0;
  summary.count_repriced := 0;
  summary.count_stuck_loss := 0;
  summary.count_stuck_profit := 0;
  summary.count_loss := 0;
  summary.count_profit_under := 0;
  summary.count_profit_above := 0;
  summary.count_total := 0;
  summary.shares_traded_miss := 0;
  summary.shares_traded_repriced := 0;
  summary.shares_traded_stuck_loss := 0;
  summary.shares_traded_stuck_profit := 0;
  summary.shares_traded_loss := 0;
  summary.shares_traded_profit_under := 0;
  summary.shares_traded_profit_above := 0;
  summary.shares_traded_total := 0;
  summary.pm_shares_traded_miss := 0;
  summary.pm_shares_traded_repriced := 0;
  summary.pm_shares_traded_stuck_loss := 0;
  summary.pm_shares_traded_stuck_profit := 0;
  summary.pm_shares_traded_loss := 0;
  summary.pm_shares_traded_profit_under := 0;
  summary.pm_shares_traded_profit_above := 0;
  summary.pm_shares_traded_total := 0;
  
  summary.expected_miss := 0.00;
  summary.expected_repriced := 0.00;
  summary.expected_stuck_loss := 0.00;
  summary.expected_stuck_profit := 0.00;
  summary.expected_loss := 0.00;
  summary.expected_profit_under := 0.00;
  summary.expected_profit_above := 0.00;
  summary.expected_total := 0.00;
  summary.real_miss := 0.00;
  summary.real_stuck_loss := 0.00;
  summary.real_stuck_profit := 0.00;
  summary.real_loss = 0.00;
  summary.real_profit_under := 0.00;
  summary.real_profit_above := 0.00;
  summary.real_total := 0.00;
  
  summary.pm_miss := 0.00;
  summary.pm_stuck_loss := 0.00;
  summary.pm_stuck_profit := 0.00;
  summary.pm_loss = 0.00;
  summary.pm_profit_under := 0.00;
  summary.pm_profit_above := 0.00;
  summary.pm_total := 0.00;
  
  summary.launch_shares_miss := 0;
  summary.launch_shares_repriced := 0;
  summary.launch_shares_stuck_loss := 0;
  summary.launch_shares_stuck_profit := 0;
  summary.launch_shares_loss := 0;
  summary.launch_shares_profit_under := 0; 
  summary.launch_shares_profit_above := 0;
  summary.launch_shares_total := 0;
  
  entry_exit_filter := '';-- Filter Entry or Exit
  check_ts := 0;
  check_ecn_entry_exit := 0;
  
  share_size_condition := '';
  IF strpos(share_size,'and') <> 0 THEN  -- Multi Detected Shares
    share_size_condition := share_size;
  ELSE 
    IF share_size <> '-100' THEN 
      share_size_condition := ' and expected_shares >='|| share_size ||'';
    ELSE -- Filter share size < 100 
      share_size_condition := ' and expected_shares < 100';   
    END IF;
  END IF;
   
  launch_shares_condition := '';
  IF strpos(launch_shares,'and') <> 0 THEN  -- Multi  Launch Shares
    launch_shares_condition :=  launch_shares;
  ELSE 
    IF launch_shares <> '-100' THEN 
      launch_shares_condition := ' and launch_shares >='||  launch_shares ||'';
    ELSE -- Filter Launch Shares < 100 
      launch_shares_condition := ' and launch_shares < 100';   
    END IF;
  END IF;
  
  IF strpos(ts_id,',') <> 0 THEN -- Multi TS
     check_ts := 1;
  END IF;
  
  new_orders_table := '(SELECT n1.date, n1.ts_id, n1.cross_id, n1.ecn, n1.time_stamp, n1.price, n1.ack_price, n1.status, n1.entry_exit, n1.left_shares, n1.shares, n1.is_iso, n1. routing_inst, n2.ecn_launch ' ||
            'FROM new_orders n1 '
            'LEFT JOIN (SELECT DISTINCT date, ts_id, cross_id, ecn_launch '
            '      FROM new_orders '
            '      WHERE entry_exit = ''Entry'' AND ts_id <> -1) n2 '
            'ON n1.date = n2.date AND n1.ts_id = n2.ts_id AND n1.cross_id = n2.cross_id) AS tmp ';
  
  str_sql :='';
  str_sql_no :='SELECT DISTINCT (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + cross_id) as tmpcol FROM ' || new_orders_table || ' WHERE date >='''|| to_date || ''' and date <='''|| from_date ||'''';
  str_sql_no :=str_sql_no || ' and time_stamp between ''' || time_from || ''' and '''|| time_to || '''';
  --filter by ts, ecn launch
  IF check_ts = 0 THEN
    IF ts_id <> '0' AND ts_id <> '-1' THEN
      str_sql_no:=str_sql_no || ' and ts_id + 1 =' || ts_id;
    ELSIF ts_id = '-1' THEN
      str_sql_no:=str_sql_no || ' and bought_shares <> sold_shares ';
    END IF;
  ELSE
    IF strpos(ts_id,'-1') = 0 THEN 
      str_sql_no:=str_sql_no || ' and ts_id + 1 in ('|| ts_id ||')';
    ELSE 
      str_sql_no:=str_sql_no || ' and ((ts_id + 1 in ('|| ts_id ||')) OR (bought_shares <> sold_shares))';
    END IF;
  END IF;
  
  IF cecn <> '0' THEN
    str_sql_no:=str_sql_no || ' and ecn_launch =''' || cecn ||'''';
  END IF;
  -- Check: Multi Select ECN Entry/Exit
  ecn_filter_conditions := '';
  IF strpos(ecn_filter,'and') <> 0 THEN 
    check_ecn_entry_exit := 1;
    ecn_filter_conditions := ecn_filter;
  ELSE 
    IF ecn_filter = 'arcaen' THEN
      str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Entry''' ;
      entry_exit_filter:= 'Entry';
    ELSIF ecn_filter = 'arcaex' THEN
      str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Exit''' ;
      entry_exit_filter:= 'Exit';
    ELSIF ecn_filter = 'ouchen' THEN
      str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Entry''';
      entry_exit_filter:= 'Entry';
    ELSIF ecn_filter = 'ouchex' THEN
      str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Exit''';
      entry_exit_filter:= 'Exit';
    ELSIF ecn_filter = 'rashen' THEN
      str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Entry''';
      entry_exit_filter:= 'Entry';
    ELSIF ecn_filter = 'rashex' THEN
      str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Exit''';
      entry_exit_filter:= 'Exit';    
    ELSIF ecn_filter = 'batzen' THEN
      str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Entry''';
      entry_exit_filter:= 'Entry';    
    ELSIF ecn_filter = 'batzex' THEN
      str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Exit'' and routing_inst != ''RL2''';
      entry_exit_filter:= 'Exit';
    ELSIF ecn_filter = 'bzsrex' THEN
      str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Exit'' and routing_inst = ''RL2''';
      entry_exit_filter:= 'Exit';
    ELSIF ecn_filter = 'rabxen' THEN
      str_sql_no:=str_sql_no || ' and ecn = ''RABX'' and entry_exit=''Entry''';
      entry_exit_filter:= 'Entry';
    ELSIF ecn_filter = 'rabxex' THEN
      str_sql_no:=str_sql_no || ' and ecn = ''RABX'' and entry_exit=''Exit''';
      entry_exit_filter:= 'Exit';
    ELSIF ecn_filter = 'nyseen' THEN
      str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Entry''';
      entry_exit_filter:= 'Entry';
    ELSIF ecn_filter = 'nyseex' THEN
      str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Exit''';
      entry_exit_filter:= 'Exit';
    ELSIF ecn_filter = 'edgxen' THEN
      str_sql_no:=str_sql_no || ' and ecn = ''EDGX'' and entry_exit=''Entry''';
      entry_exit_filter:= 'Entry';
    ELSIF ecn_filter = 'edgxex' THEN
      str_sql_no:=str_sql_no || ' and ecn = ''EDGX'' and entry_exit=''Exit''';
      entry_exit_filter:= 'Exit';  
    ELSIF ecn_filter = 'edgaen' THEN
      str_sql_no:=str_sql_no || ' and ecn = ''EDGA'' and entry_exit=''Entry''';
      entry_exit_filter:= 'Entry';
    ELSIF ecn_filter = 'edgaex' THEN
      str_sql_no:=str_sql_no || ' and ecn = ''EDGA'' and entry_exit=''Exit''';
      entry_exit_filter:= 'Exit';
    ELSIF ecn_filter = 'oubxen' THEN
      str_sql_no:=str_sql_no || ' and ecn = ''OUBX'' and entry_exit=''Entry''';
      entry_exit_filter:= 'Entry';
    ELSIF ecn_filter = 'oubxex' THEN
      str_sql_no:=str_sql_no || ' and ecn = ''OUBX'' and entry_exit=''Exit''';
      entry_exit_filter:= 'Exit';
    ELSIF ecn_filter = 'batyen' THEN
      str_sql_no:=str_sql_no || ' and ecn = ''BATY'' and entry_exit=''Entry''';
      entry_exit_filter:= 'Entry';    
    ELSIF ecn_filter = 'batyex' THEN
      str_sql_no:=str_sql_no || ' and ecn = ''BATY'' and entry_exit=''Exit''';
      entry_exit_filter:= 'Exit';
    ELSIF ecn_filter = 'psxen' THEN
      str_sql_no:=str_sql_no || ' and ecn = ''PSX'' and entry_exit=''Entry''';
      entry_exit_filter:= 'Entry';    
    ELSIF ecn_filter = 'psxex' THEN
      str_sql_no:=str_sql_no || ' and ecn = ''PSX'' and entry_exit=''Exit''';
      entry_exit_filter:= 'Exit';  
    END IF;
  END IF;

  --Filter by ISO Odrer
  IF is_iso = '1' THEN
    str_sql_no:=str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
  END IF;
  -- tmp_str := str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
  
  IF is_iso = '2' THEN
    str_sql_no:=str_sql_no || ' and (is_iso <> ''Y'')';
  END IF;
  
  -- filter by Symbol, Bought Shares, Sold Shares
  IF symb <>'' THEN
    str_sql:=' and symbol='''|| symb ||'''';
  END IF;
  IF bt_shares > 0 THEN
    str_sql:=str_sql || ' and bought_shares >=' || bt_shares;
  END IF;
  IF sd_shares > 0 THEN
    str_sql:=str_sql || ' and sold_shares >=' || sd_shares;
  END IF;
  --filter by ts
  
  IF check_ts = 0 THEN
    IF ts_id <> '0' AND ts_id <> '-1' THEN 
      str_sql:=str_sql || ' and ts_id + 1 =' || ts_id;
    ELSIF ts_id = '-1' THEN
      str_sql := str_sql || ' and bought_shares <> sold_shares ';
    END IF;
  ELSE
    IF strpos(ts_id,'-1') = 0 THEN 
      str_sql:=str_sql || ' and ts_id + 1 in ('|| ts_id ||')';
    ELSE 
      str_sql:=str_sql || ' and ((ts_id + 1 in ('|| ts_id ||')) OR (bought_shares <> sold_shares))';
    END IF;
     
  END IF;
  IF cross_size <> '' then
    str_sql := str_sql || ' and ' || cross_size;
  END IF;
  IF check_ecn_entry_exit = 0 AND ecn_filter <> '0' AND exclusive_val = 1 THEN
    str_sql_multi_orders := get_query_string_array_id_multi_orders(to_date, from_date, entry_exit_filter);
    str_sql := str_sql || ' and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) not in (' || str_sql_multi_orders || ')';
  END IF;
  
  --Calculate repriced
  IF ((ecn_filter = 'oubxen' OR ecn_filter = 'oubxex' OR ecn_filter = 'psxen' OR ecn_filter = 'psxex' OR ecn_filter = '0') AND  (cecn = 'NDAQ' OR cecn = 'NDBX' OR cecn = 'PSX' OR cecn = '0')) THEN
    IF bt_shares = 0 AND sd_shares = 0 THEN
    
    str_sql_no_repriced:=str_sql_no || ' and (ecn =''OUCH'' OR ecn =''OUBX'') and price != ack_price ' ;
    str_sql_repriced:=str_sql || '  and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) in ('|| str_sql_no_repriced ||')';  
    str_sql1:='SELECT count(*) as count_missed, sum(expected_pl)as sum_expected_pl, sum(bought_shares) as sum_bought_shares, sum(sold_shares) as sum_sold_shares, sum(launch_shares) as sum_launch_shares FROM vwtrade_results_statistics WHERE bought_shares = 0 and sold_shares = 0 and date >='''|| to_date || ''' and date <='''|| from_date ||''''|| share_size_condition || ecn_filter_conditions || launch_shares_condition;
    IF (expected_profit != 0.0) THEN
      str_sql1 := str_sql1 || ' and expected_pl >= ' || expected_profit;
    END IF;
    str_sql1:=str_sql1 ||str_sql_repriced;
    for missed_trade_record_of_ouch in EXECUTE str_sql1 loop
    count_ouch_missed_trade := missed_trade_record_of_ouch.count_missed;
    sum_ouch_expected_pl := missed_trade_record_of_ouch.sum_expected_pl;
    sum_shares_traded_miss := missed_trade_record_of_ouch.sum_bought_shares + missed_trade_record_of_ouch.sum_sold_shares;
    sum_launch_shares_miss := missed_trade_record_of_ouch.sum_launch_shares;
    end loop;

   IF count_ouch_missed_trade > 0 THEN
    summary.count_repriced := count_ouch_missed_trade;
    summary.expected_repriced := sum_ouch_expected_pl;
    summary.count_miss := -count_ouch_missed_trade;
    summary.expected_miss :=  -sum_ouch_expected_pl;
    summary.shares_traded_miss := -sum_shares_traded_miss;
    summary.shares_traded_repriced := sum_shares_traded_miss;
    summary.launch_shares_miss := -sum_launch_shares_miss;
    summary.launch_shares_repriced := sum_launch_shares_miss;
   END IF;

   END IF;
  END IF;
 --calculate for trade_statistics table  
 str_sql:=str_sql || '  and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) in ('|| str_sql_no ||')';
 str_sql2:='SELECT * FROM vwtrade_results_statistics WHERE date >='''|| to_date || ''' and date <='''|| from_date ||''''|| share_size_condition || ecn_filter_conditions || launch_shares_condition;
 IF (expected_profit != 0.0) THEN
  str_sql2 := str_sql2 || ' and expected_pl >= ' || expected_profit;
 END IF;
 
 str_sql2:=str_sql2 ||str_sql;
 
FOR trade_result in EXECUTE str_sql2 loop
    shares_filled := trade_result.bought_shares;

    IF (shares_filled > trade_result.sold_shares) THEN
      shares_filled := trade_result.sold_shares;
    END IF;

    shares_stuck := trade_result.bought_shares - trade_result.sold_shares;    

    IF (shares_stuck = 0) THEN
      profit_loss := trade_result.total_sold - trade_result.total_bought - trade_result.ts_total_fee;

      IF (shares_filled = 0) THEN
        summary.count_miss := summary.count_miss + 1;
        summary.shares_traded_miss := summary.shares_traded_miss + trade_result.bought_shares + trade_result.sold_shares;
        summary.launch_shares_miss := summary.launch_shares_miss + trade_result.launch_shares;
        summary.expected_miss := summary.expected_miss + trade_result.expected_pl;
      ELSE
        IF (profit_loss < 0) THEN
          summary.count_loss := summary.count_loss + 1;
          summary.shares_traded_loss := summary.shares_traded_loss + trade_result.bought_shares + trade_result.sold_shares;
          summary.launch_shares_loss := summary.launch_shares_loss + trade_result.launch_shares;
          summary.expected_loss := summary.expected_loss + trade_result.expected_pl;
          summary.real_loss := summary.real_loss + profit_loss;
        ELSE
          IF ( profit_loss::real < trade_result.expected_pl::real) THEN
            summary.count_profit_under := summary.count_profit_under + 1;
            summary.shares_traded_profit_under := summary.shares_traded_profit_under + trade_result.bought_shares + trade_result.sold_shares;
            summary.launch_shares_profit_under := summary.launch_shares_profit_under + trade_result.launch_shares;      
            summary.expected_profit_under := summary.expected_profit_under + trade_result.expected_pl;
            summary.real_profit_under := summary.real_profit_under + profit_loss;
          ELSE
            summary.count_profit_above := summary.count_profit_above + 1;
            summary.shares_traded_profit_above := summary.shares_traded_profit_above + trade_result.bought_shares + trade_result.sold_shares;
            summary.launch_shares_profit_above := summary.launch_shares_profit_above + trade_result.launch_shares;
            summary.expected_profit_above := summary.expected_profit_above + trade_result.expected_pl;
            summary.real_profit_above := summary.real_profit_above + profit_loss;
          END IF;
        END IF;
      END IF;
    ELSE
      --Calculate for stuck
      IF (trade_result.pm_profit_loss - trade_result.pm_total_fee < -0.0001) THEN
  --stuck_loss
     summary.count_stuck_loss := summary.count_stuck_loss + 1;
     summary.shares_traded_stuck_loss := summary.shares_traded_stuck_loss + trade_result.bought_shares + trade_result.sold_shares;
      summary.launch_shares_stuck_loss := summary.launch_shares_stuck_loss + trade_result.launch_shares;
     summary.expected_stuck_loss := summary.expected_stuck_loss + trade_result.expected_pl;
     summary.real_stuck_loss := summary.real_stuck_loss + trade_result.ts_profit_loss  + trade_result.pm_profit_loss - trade_result.ts_total_fee - trade_result.pm_total_fee;
     summary.pm_stuck_loss := summary.pm_stuck_loss + trade_result.pm_profit_loss - trade_result.pm_total_fee;
     IF trade_result.bought_shares < trade_result.sold_shares THEN
    summary.pm_shares_traded_stuck_loss := summary.pm_shares_traded_stuck_loss - trade_result.bought_shares + trade_result.sold_shares - trade_result.left_stuck_shares;
     ELSE
    summary.pm_shares_traded_stuck_loss := summary.pm_shares_traded_stuck_loss + trade_result.bought_shares - trade_result.sold_shares - trade_result.left_stuck_shares;
     END IF;
      ELSE
  --stuck_profit
     summary.count_stuck_profit := summary.count_stuck_profit + 1;
     summary.shares_traded_stuck_profit := summary.shares_traded_stuck_profit + trade_result.bought_shares + trade_result.sold_shares;
      summary.launch_shares_stuck_profit := summary.launch_shares_stuck_profit + trade_result.launch_shares;
     summary.expected_stuck_profit := summary.expected_stuck_profit + trade_result.expected_pl;
     summary.real_stuck_profit := summary.real_stuck_profit + trade_result.ts_profit_loss + trade_result.pm_profit_loss - trade_result.ts_total_fee - trade_result.pm_total_fee;
     summary.pm_stuck_profit := summary.pm_stuck_profit + trade_result.pm_profit_loss - trade_result.pm_total_fee;
     IF trade_result.bought_shares < trade_result.sold_shares THEN
    summary.pm_shares_traded_stuck_profit := summary.pm_shares_traded_stuck_profit - trade_result.bought_shares + trade_result.sold_shares - trade_result.left_stuck_shares;
     ELSE
    summary.pm_shares_traded_stuck_profit := summary.pm_shares_traded_stuck_profit + trade_result.bought_shares - trade_result.sold_shares - trade_result.left_stuck_shares;
     END IF;
      END IF;
    END IF;

    summary.count_total := summary.count_total +  1;
    summary.shares_traded_total := summary.shares_traded_total + trade_result.bought_shares + trade_result.sold_shares;
    summary.launch_shares_total := summary.launch_shares_total + trade_result.launch_shares;
    summary.expected_total := summary.expected_total + trade_result.expected_pl  ;
  end loop;
  
  summary.count_total := summary.count_total - summary.count_repriced;
  summary.shares_traded_total := summary.shares_traded_total - summary.shares_traded_repriced;
  summary.expected_total := summary.expected_total - summary.expected_repriced;
  summary.launch_shares_total := summary.launch_shares_total - summary.launch_shares_repriced;
  
  return summary;
end
$$;


ALTER FUNCTION public.cal_filter_trade_statistic_byecn2_twt2(to_date date, from_date date, time_from time without time zone, time_to time without time zone, share_size text, expected_profit double precision, symb text, bt_shares integer, sd_shares integer, ecn_filter text, ts_id text, cecn text, is_iso text, exclusive_val integer, cross_size text, launch_shares text) OWNER TO postgres;

--
-- Name: cal_filter_trade_statistic_byecn2_twt2_with_step(date, date, time without time zone, time without time zone, text, double precision, text, integer, integer, text, text, text, integer, text, integer, text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION cal_filter_trade_statistic_byecn2_twt2_with_step(to_date date, from_date date, time_from time without time zone, time_to time without time zone, share_size text, expected_profit double precision, symb text, bt_shares integer, sd_shares integer, ecn_filter text, ts_id text, cecn text, step integer, is_iso text, exclusive_val integer, cross_size text, launch_shares text) RETURNS SETOF trade_result_summary2_struct
    LANGUAGE plpgsql
    AS $$ 
declare
  trade_result         record;
  summary         trade_result_summary2_struct;
  shares_filled         integer;
  shares_stuck         integer;
  profit_loss         double precision;
  --statistic missed trade 
  count_ouch_missed_trade     integer;
  sum_ouch_expected_pl       double precision;
  missed_trade_record_of_ouch     record;
  str_sql         text;
  str_sql_no         text;
  str_sql_tmp           text;
  str_sql_multi_orders text;
  tmp text;
  tmp_str text;
  min_time        "time";
  max_time        "time";
  temp_time         "time";
  t_current        "time";
  new_orders_table text;
  share_size_condition text;
  launch_shares_condition text;
   -- Filter Entry or Exit
  entry_exit_filter text;
  ecn_filter_conditions text;
   -- Check number TS
  check_ts integer;
  check_ecn_entry_exit integer;
  
begin  
            
  min_time := time_from;
  max_time := time_to;
  
  entry_exit_filter := '';  -- Filter Entry or Exit
  
  share_size_condition := '';
  IF strpos(share_size,'and') <> 0 THEN -- Multi Detected Shares
    share_size_condition := share_size;
  ELSE 
    IF share_size <> '-100' THEN 
      share_size_condition := ' and expected_shares >='|| share_size ||'';
    ELSE -- Filter share size < 100 
      share_size_condition := ' and expected_shares < 100';   
    END IF;
  END IF;
  
  -- Filter by Launch Shares
  launch_shares_condition := '';
  IF strpos(launch_shares,'and') <> 0 THEN -- Multi Launch Shares
    launch_shares_condition := launch_shares;
  ELSE 
    IF launch_shares <> '-100' THEN 
      launch_shares_condition := ' and launch_shares >='|| launch_shares ||'';
    ELSE -- Filter share size < 100 
      launch_shares_condition := ' and launch_shares < 100';   
    END IF;
  END IF;
  
  check_ts := 0;
  IF strpos(ts_id,',') <> 0 THEN -- Multi TS
    check_ts := 1;
  END IF;
  -- Check: Multi Select ECN Entry/Exit
  check_ecn_entry_exit := 0;
  ecn_filter_conditions := '';
  IF strpos(ecn_filter,'and') <> 0 THEN 
    check_ecn_entry_exit := 1;
    ecn_filter_conditions := ecn_filter;
  END IF; 
  
  WHILE min_time < max_time LOOP
    -- Initia
    
    summary.count_miss := 0;
    summary.count_repriced := 0;
    summary.count_stuck_loss := 0;
    summary.count_stuck_profit := 0;
    summary.count_loss := 0;
    summary.count_profit_under := 0;
    summary.count_profit_above := 0;    
    
    IF (step = 5) THEN
      t_current = min_time + interval '5 minutes';
    ELSIF (step = 15) THEN
      t_current = min_time + interval '15 minutes';
      
    ELSIF (step = 30) THEN
      t_current = min_time + interval '30 minutes';
    ELSIF (step = 60) THEN
      t_current = min_time + interval '60 minutes';
    END IF;
    --raise notice '1(%)', t_current;
    IF ((t_current > max_time) or (t_current = '00:00:00')) THEN
      t_current = max_time;
      --raise notice '2(%)', t_current;
    END IF; 
    temp_time = t_current - interval '1 microsecond';
    
    --Calculate repriced
   IF ((ecn_filter = 'oubxen' OR ecn_filter = 'oubxex' OR ecn_filter = 'psxen' OR ecn_filter = 'psxex' OR ecn_filter = '0') AND  (cecn = 'NDAQ' OR cecn = 'NDBX' OR cecn = 'PSX' OR cecn = '0')) THEN
    IF bt_shares = 0 AND sd_shares = 0 THEN
      --Select data from view "vwouch_missed_trades" and calculate total missed trade   
      str_sql:='SELECT count(*) as count_missed, sum(expected_pl)as sum_expected_pl FROM vwtrade_results_statistics WHERE date >='''|| to_date || ''' and date <='''|| from_date ||''''|| share_size_condition || ecn_filter_conditions || launch_shares_condition;
      IF (expected_profit != 0.0) THEN
        str_sql := str_sql || ' and expected_pl >= ' || expected_profit;
      END IF;
      str_sql := str_sql || ' and bought_shares = 0 and sold_shares = 0 ';
      
      --filter by ts when we get data from view vwtrade_results_statistics
      IF check_ts = 0 THEN
        IF ts_id <> '0' THEN 
          str_sql:=str_sql || ' and ts_id + 1 =' || ts_id;
        ELSIF ts_id = '-1' THEN
          str_sql := str_sql || ' and bought_shares <> sold_shares ';
        END IF;
      ELSE
        IF strpos(ts_id,'-1') = 0 THEN 
          str_sql:=str_sql || ' and ts_id + 1 in ('|| ts_id ||')';
        ELSE 
          str_sql:=str_sql || ' and ((ts_id + 1 in ('|| ts_id ||')) OR (bought_shares <> sold_shares))';
        END IF;
         
      END IF;
      IF cross_size <> '' then
        str_sql := str_sql || ' and ' || cross_size;
      END IF;
      
      str_sql_no :='SELECT (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + cross_id) as tmpcol FROM new_orders WHERE date >='''|| to_date || ''' and date <='''|| from_date ||'''';
      str_sql_no :=str_sql_no || ' and time_stamp between ''' || min_time || ''' and '''|| temp_time || '''';
      str_sql_no :=str_sql_no || ' and bought_shares = 0 and sold_shares = 0 and (ecn =''OUCH'' or ecn =''OUBX'') and price != ack_price ';
      
      --filter by ts, ecn_launch when we get data from table new_orders
      IF check_ts = 0 THEN
        IF ts_id <> '0' AND ts_id <> '-1' THEN
          str_sql_no:=str_sql_no || ' and ts_id + 1 =' || ts_id;
        ELSIF ts_id = '-1' THEN
          str_sql_no:=str_sql_no || ' and bought_shares <> sold_shares ';
        END IF;
      ELSE
        IF strpos(ts_id,'-1') = 0 THEN 
          str_sql_no:=str_sql_no || ' and ts_id + 1 in ('|| ts_id ||')';
        ELSE 
          str_sql_no:=str_sql_no || ' and ((ts_id + 1 in ('|| ts_id ||')) OR (bought_shares <> sold_shares))';
        END IF;
      END IF;
      
      IF cecn <> '0' THEN
        str_sql_no:=str_sql_no || ' and ecn_launch =''' || cecn ||'''';
      END IF;
      
      IF check_ecn_entry_exit = 1 THEN --Filter by multi ECN Entry/Exit
        ecn_filter_conditions := ecn_filter;
      ELSE   
        IF ecn_filter = 'arcaen' THEN
          str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Entry''' ;
          entry_exit_filter:= 'Entry';
        ELSIF ecn_filter = 'arcaex' THEN
          str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Exit''' ;
          entry_exit_filter:= 'Exit';
        ELSIF ecn_filter = 'ouchen' THEN
          str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Entry''';
          entry_exit_filter:= 'Entry';
        ELSIF ecn_filter = 'ouchex' THEN
          str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Exit''';
          entry_exit_filter:= 'Exit';
        ELSIF ecn_filter = 'rashen' THEN
          str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Entry''';
          entry_exit_filter:= 'Entry';
        ELSIF ecn_filter = 'rashex' THEN
          str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Exit''';
          entry_exit_filter:= 'Exit';        
        ELSIF ecn_filter = 'batzen' THEN
          str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Entry''';
          entry_exit_filter:= 'Entry';        
        ELSIF ecn_filter = 'batzex' THEN
          str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Exit'' and routing_inst != ''RL2''';
          entry_exit_filter:= 'Exit';
        ELSIF ecn_filter = 'bzsrex' THEN
          str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Exit'' and routing_inst = ''RL2''';
          entry_exit_filter:= 'Exit';  
        ELSIF ecn_filter = 'rabxen' THEN
          str_sql_no:=str_sql_no || ' and ecn = ''RABX'' and entry_exit=''Entry''';
          entry_exit_filter:= 'Entry';
        ELSIF ecn_filter = 'rabxex' THEN
          str_sql_no:=str_sql_no || ' and ecn = ''RABX'' and entry_exit=''Exit''';
          entry_exit_filter:= 'Exit';
        ELSIF ecn_filter = 'nyseen' THEN
          str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Entry''';
          entry_exit_filter:= 'Entry';
        ELSIF ecn_filter = 'nyseex' THEN
          str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Exit''';
          entry_exit_filter:= 'Exit';
        ELSIF ecn_filter = 'edgxen' THEN
          str_sql_no:=str_sql_no || ' and ecn = ''EDGX'' and entry_exit=''Entry''';
          entry_exit_filter:= 'Entry';
        ELSIF ecn_filter = 'edgxex' THEN
          str_sql_no:=str_sql_no || ' and ecn = ''EDGX'' and entry_exit=''Exit''';
          entry_exit_filter:= 'Exit';
        ELSIF ecn_filter = 'edgaen' THEN
          str_sql_no:=str_sql_no || ' and ecn = ''EDGA'' and entry_exit=''Entry''';
          entry_exit_filter:= 'Entry';
        ELSIF ecn_filter = 'edgaex' THEN
          str_sql_no:=str_sql_no || ' and ecn = ''EDGA'' and entry_exit=''Exit''';
          entry_exit_filter:= 'Exit';
        ELSIF ecn_filter = 'oubxen' THEN
          str_sql_no:=str_sql_no || ' and ecn = ''OUBX'' and entry_exit=''Entry''';
          entry_exit_filter:= 'Entry';
        ELSIF ecn_filter = 'oubxex' THEN
          str_sql_no:=str_sql_no || ' and ecn = ''OUBX'' and entry_exit=''Exit''';
          entry_exit_filter:= 'Exit';
        ELSIF ecn_filter = 'batyen' THEN
          str_sql_no:=str_sql_no || ' and ecn = ''BATY'' and entry_exit=''Entry''';
          entry_exit_filter:= 'Entry';    
        ELSIF ecn_filter = 'batyex' THEN
          str_sql_no:=str_sql_no || ' and ecn = ''BATY'' and entry_exit=''Exit''';
          entry_exit_filter:= 'Exit';  
        ELSIF ecn_filter = 'psxen' THEN
          str_sql_no:=str_sql_no || ' and ecn = ''PSX'' and entry_exit=''Entry''';
          entry_exit_filter:= 'Entry';    
        ELSIF ecn_filter = 'psxex' THEN
          str_sql_no:=str_sql_no || ' and ecn = ''PSX'' and entry_exit=''Exit''';
          entry_exit_filter:= 'Exit';    
        END IF;
      END IF;

      IF symb <>'' THEN
        str_sql:=str_sql || ' and symbol='''|| symb ||'''';
        str_sql_no:=str_sql_no || ' and symbol='''|| symb ||'''';
      END IF;
      --Filter by ISO Odrer
      IF is_iso = '1' AND from_date > '2010-01-01' THEN
        str_sql_no:=str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
      END IF;
      -- tmp_str := str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
  
      IF is_iso = '2' AND from_date > '2010-01-01' THEN
        str_sql_no:=str_sql_no || ' and (is_iso <> ''Y'')';
      END IF;
      
      str_sql_no:=str_sql_no || ' GROUP BY (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + cross_id)' ;

      IF bt_shares > 0 THEN
        str_sql:=str_sql || ' and bought_shares >=' || bt_shares;
      END IF;
      IF sd_shares > 0 THEN
        str_sql:=str_sql || ' and sold_shares >=' || sd_shares;
      END IF;

      str_sql:=str_sql || ' and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) in ('|| str_sql_no ||')';
      
      IF check_ecn_entry_exit = 0 AND ecn_filter <> '0' AND exclusive_val = 1 THEN
        str_sql_multi_orders := get_query_string_array_id_multi_orders(to_date, from_date, entry_exit_filter);
        str_sql :=str_sql || ' and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) not in (' || str_sql_multi_orders || ')';
      END IF;
      
      for missed_trade_record_of_ouch in EXECUTE str_sql loop
        count_ouch_missed_trade := missed_trade_record_of_ouch.count_missed;
        sum_ouch_expected_pl := missed_trade_record_of_ouch.sum_expected_pl;
      end loop;

      IF count_ouch_missed_trade > 0 THEN
        summary.count_repriced := count_ouch_missed_trade;        
        summary.count_miss := -count_ouch_missed_trade;        
      END IF;

    END IF;

    END IF;
    --filter by  ECN by Entry/Exit
    new_orders_table := '(SELECT n1.date, n1.ts_id, n1.cross_id, n1.ecn, n1.time_stamp, n1.price, n1.ack_price, n1.status, n1.entry_exit, n1.left_shares, n1.shares, n1.is_iso, n1. routing_inst, n2.ecn_launch ' ||
            'FROM new_orders n1 '
            'LEFT JOIN (SELECT DISTINCT date, ts_id, cross_id, ecn_launch '
            '      FROM new_orders '
            '      WHERE entry_exit = ''Entry'' AND ts_id <> -1 AND time_stamp between ''' || min_time || ''' AND '''|| temp_time || ''') n2 '
            'ON n1.date = n2.date AND n1.ts_id = n2.ts_id AND n1.cross_id = n2.cross_id) AS tmp ';
            
    str_sql:='SELECT * FROM vwtrade_results_statistics WHERE date >='''|| to_date || ''' and date <='''|| from_date ||''''|| share_size_condition || ecn_filter_conditions || launch_shares_condition;
    IF (expected_profit != 0.0) THEN
      str_sql := str_sql || ' and expected_pl >= ' || expected_profit;
    END IF;
    str_sql_no :='SELECT (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + cross_id) as tmpcol FROM ' || new_orders_table || ' WHERE date >='''|| to_date || ''' and date <='''|| from_date ||'''';
    str_sql_no :=str_sql_no || ' and time_stamp between ''' || min_time || ''' and '''|| temp_time || '''';
    
    --filter by ts, ecn launch
    IF check_ts = 0 THEN
      IF ts_id <> '0' AND ts_id <> '-1' THEN
        str_sql_no:=str_sql_no || ' and ts_id + 1 =' || ts_id;
      ELSIF ts_id = '-1' THEN
        str_sql_no:=str_sql_no || ' and bought_shares <> sold_shares ';
      END IF;
    ELSE
      IF strpos(ts_id,'-1') = 0 THEN 
        str_sql_no:=str_sql_no || ' and ts_id + 1 in ('|| ts_id ||')';
      ELSE 
        str_sql_no:=str_sql_no || ' and ((ts_id + 1 in ('|| ts_id ||')) OR (bought_shares <> sold_shares))';
      END IF;
    END IF;
    
    IF cecn <> '0' THEN
      str_sql_no:=str_sql_no || ' and ecn_launch =''' || cecn ||'''';
    END IF;
    
    ecn_filter_conditions := '';
    IF check_ecn_entry_exit = 1 THEN --Filter by multi ECN Entry/Exit
      str_sql := str_sql || ecn_filter;
    ELSE 
      IF ecn_filter = 'arcaen' THEN
        str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Entry''' ;
        entry_exit_filter:= 'Entry';
      ELSIF ecn_filter = 'arcaex' THEN
        str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Exit''' ;
        entry_exit_filter:= 'Exit';
      ELSIF ecn_filter = 'ouchen' THEN
        str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Entry''';
        entry_exit_filter:= 'Entry';
      ELSIF ecn_filter = 'ouchex' THEN
        str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Exit''';
        entry_exit_filter:= 'Exit';
      ELSIF ecn_filter = 'rashen' THEN
        str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Entry''';
        entry_exit_filter:= 'Entry';
      ELSIF ecn_filter = 'rashex' THEN
        str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Exit''';  
        entry_exit_filter:= 'Exit';      
      ELSIF ecn_filter = 'batzen' THEN
        str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Entry''';  
        entry_exit_filter:= 'Entry';
      ELSIF ecn_filter = 'batzex' THEN
        str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Exit'' and routing_inst != ''RL2''';
        entry_exit_filter:= 'Exit';
      ELSIF ecn_filter = 'bzsrex' THEN
        str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Exit'' and routing_inst = ''RL2''';
        entry_exit_filter:= 'Exit';
      ELSIF ecn_filter = 'nyseen' THEN
        str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Entry''';
        entry_exit_filter:= 'Entry';
      ELSIF ecn_filter = 'nyseex' THEN
        str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Exit''';
        entry_exit_filter:= 'Exit';  
      ELSIF ecn_filter = 'edgxen' THEN
        str_sql_no:=str_sql_no || ' and ecn = ''EDGX'' and entry_exit=''Entry''';
        entry_exit_filter:= 'Entry';
      ELSIF ecn_filter = 'edgxex' THEN
        str_sql_no:=str_sql_no || ' and ecn = ''EDGX'' and entry_exit=''Exit''';
        entry_exit_filter:= 'Exit';
      ELSIF ecn_filter = 'edgaen' THEN
        str_sql_no:=str_sql_no || ' and ecn = ''EDGA'' and entry_exit=''Entry''';
        entry_exit_filter:= 'Entry';
      ELSIF ecn_filter = 'edgaex' THEN
        str_sql_no:=str_sql_no || ' and ecn = ''EDGA'' and entry_exit=''Exit''';
        entry_exit_filter:= 'Exit';
      ELSIF ecn_filter = 'oubxen' THEN
        str_sql_no:=str_sql_no || ' and ecn = ''OUBX'' and entry_exit=''Entry''';
          entry_exit_filter:= 'Entry';
      ELSIF ecn_filter = 'oubxex' THEN
        str_sql_no:=str_sql_no || ' and ecn = ''OUBX'' and entry_exit=''Exit''';
        entry_exit_filter:= 'Exit';  
      ELSIF ecn_filter = 'batyen' THEN
        str_sql_no:=str_sql_no || ' and ecn = ''BATY'' and entry_exit=''Entry''';
        entry_exit_filter:= 'Entry';    
      ELSIF ecn_filter = 'batyex' THEN
        str_sql_no:=str_sql_no || ' and ecn = ''BATY'' and entry_exit=''Exit''';
        entry_exit_filter:= 'Exit';
      ELSIF ecn_filter = 'psxen' THEN
        str_sql_no:=str_sql_no || ' and ecn = ''PSX'' and entry_exit=''Entry''';
        entry_exit_filter:= 'Entry';    
      ELSIF ecn_filter = 'psxex' THEN
        str_sql_no:=str_sql_no || ' and ecn = ''PSX'' and entry_exit=''Exit''';
        entry_exit_filter:= 'Exit';  
      END IF;
    END IF;
    
    --Filter by ISO Odrer
    IF is_iso = '1' AND from_date > '2010-01-01' THEN
      str_sql_no:=str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
    END IF;
    -- tmp_str := str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
  
    IF is_iso = '2' AND from_date > '2010-01-01' THEN
      str_sql_no:=str_sql_no || ' and (is_iso <> ''Y'')';
    END IF;
    
    str_sql_no:=str_sql_no || ' GROUP BY (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + cross_id)' ;

    -- filter by Symbol, Bought Shares, Sold Shares
    IF symb <>'' THEN
      str_sql:=str_sql || ' and symbol='''|| symb ||'''';
    END IF;
    IF bt_shares > 0 THEN
      str_sql:=str_sql || ' and bought_shares >=' || bt_shares;
    END IF;
    IF sd_shares > 0 THEN
      str_sql:=str_sql || ' and sold_shares >=' || sd_shares;
    END IF;
    --filter by ts
    IF check_ts = 0 THEN
      IF ts_id <> '0' THEN 
        str_sql:=str_sql || ' and ts_id + 1 =' || ts_id;
      ELSIF ts_id = '-1' THEN
        str_sql := str_sql || ' and bought_shares <> sold_shares ';
      END IF;
    ELSE
      IF strpos(ts_id,'-1') = 0 THEN 
        str_sql:=str_sql || ' and ts_id + 1 in ('|| ts_id ||')';
      ELSE 
        str_sql:=str_sql || ' and ((ts_id + 1 in ('|| ts_id ||')) OR (bought_shares <> sold_shares))';
      END IF;
       
    END IF;
    IF cross_size <> '' then
      str_sql := str_sql || ' and ' || cross_size;
    END IF;
    
    str_sql:=str_sql || ' and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) in ('|| str_sql_no ||')';
    
    IF check_ecn_entry_exit = 0 AND ecn_filter <> '0' AND exclusive_val = 1 THEN
      str_sql_multi_orders := get_query_string_array_id_multi_orders(to_date, from_date, entry_exit_filter);
      str_sql :=str_sql || ' and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) not in (' || str_sql_multi_orders || ')';
    END IF;
    
    --calculate for trade_statistics table  
    FOR trade_result in EXECUTE str_sql loop
      shares_filled := trade_result.bought_shares;
      --RAISE NOTICE 'Variable an_integer was changed.';      
      IF (shares_filled > trade_result.sold_shares) THEN
        shares_filled := trade_result.sold_shares;
      END IF;

      shares_stuck := trade_result.bought_shares - trade_result.sold_shares;    

      IF (shares_stuck = 0) THEN
        profit_loss := trade_result.total_sold - trade_result.total_bought - trade_result.ts_total_fee;

        IF (shares_filled = 0) THEN
        summary.count_miss := summary.count_miss + 1;        
        ELSE
          IF (profit_loss < 0) THEN
            summary.count_loss := summary.count_loss + 1;            
          ELSE
            IF ( profit_loss::real < trade_result.expected_pl::real) THEN
              summary.count_profit_under := summary.count_profit_under + 1;            
            ELSE
              summary.count_profit_above := summary.count_profit_above + 1;              
            END IF;
          END IF;
        END IF;
      ELSE
        --Calculate for stuck
        IF (trade_result.pm_profit_loss - trade_result.pm_total_fee < -0.0001) THEN
          --stuck_loss
          summary.count_stuck_loss := summary.count_stuck_loss + 1;          
        ELSE
          --stuck_profit
          summary.count_stuck_profit := summary.count_stuck_profit + 1;        
        END IF;
      END IF;

    END LOOP;

    summary.time_stamp = t_current;
    min_time := t_current;  
  
    RETURN NEXT summary;
    
  END LOOP;
  RETURN;
end
$$;


ALTER FUNCTION public.cal_filter_trade_statistic_byecn2_twt2_with_step(to_date date, from_date date, time_from time without time zone, time_to time without time zone, share_size text, expected_profit double precision, symb text, bt_shares integer, sd_shares integer, ecn_filter text, ts_id text, cecn text, step integer, is_iso text, exclusive_val integer, cross_size text, launch_shares text) OWNER TO postgres;

--
-- Name: calculate_total_fee(double precision, integer, character, text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION calculate_total_fee(price double precision, shares integer, side character, liquidityval text, ecn text) RETURNS double precision
    LANGUAGE plpgsql
    AS $$
  DECLARE
    sec_fee double precision;
    taf_fee double precision;
    wedbus_commission double precision;
    add_venue_fee_with_price_larger_than_1 double precision;
    add_venue_fee_with_price_smaller_than_1 double precision;
    remove_venue_fee_with_price_larger_than_1 double precision;
    remove_venue_fee_with_price_smaller_than_1 double precision;
    auction_venue_fee double precision;
    ARCA_route_venue_fee double precision;
    NDAQ_route_venue_fee double precision;
    BATS_route_venue_fee double precision;
    EDGX_route_venue_fee double precision;
    
    totalVenueFee double precision;
    totalSecFees double precision;
    totalTafFee double precision;
    totalFee double precision;
    totalWedbushCommission double precision;
    
    liquidity character(2);
  BEGIN
    -- If we changed some fees in fee_config file on AS or TA, we would change those fees here
    sec_fee := 0.00002240;
    taf_fee := 0.00009;
    wedbus_commission := 0.000035;
    add_venue_fee_with_price_larger_than_1 := -0.0028;
    add_venue_fee_with_price_smaller_than_1 := 0.00;
    remove_venue_fee_with_price_larger_than_1 := 0.003;
    remove_venue_fee_with_price_smaller_than_1 := 0.003;
    auction_venue_fee := 0.0005;
    ARCA_route_venue_fee := 0.003;
    NDAQ_route_venue_fee := 0.0035;
    BATS_route_venue_fee := 0.002;
    EDGX_route_venue_fee := 0.0029;
    
    -- Initialize fees
    totalVenueFee := 0.00;
    totalSecFees := 0.00;
    totalTafFee := 0.00;
    totalFee := 0.00;
    totalWedbushCommission := 0.00;
    -- Calculate total fee
    if (side = 'S') then
      totalSecFees := shares * price * sec_fee;
    end if;
    
    totalTafFee := shares * taf_fee;
    totalWedbushCommission := shares * wedbus_commission;
    
    select trim(liquidityVal) into liquidity;
    if (ecn = 'ARCA') then
      -- Add
      if (liquidity = 'A' or liquidity = 'B' or liquidity = 'M' or liquidity = 'D' or liquidity = 'S') then
        if (price >= 1.00) then
          totalVenueFee := shares * add_venue_fee_with_price_larger_than_1;
        else
          totalVenueFee := shares * add_venue_fee_with_price_smaller_than_1;
        end if;
      -- Remove
      elsif (liquidity = 'R' or liquidity = 'E' or liquidity = 'L') then
        if (price >= 1.00) then
          totalVenueFee := shares * remove_venue_fee_with_price_larger_than_1;
        else
          totalVenueFee := shares * price * remove_venue_fee_with_price_smaller_than_1;
        end if;
      -- Auction
      elsif (liquidity = 'G' or liquidity = 'Z' or liquidity = 'O') then
        totalVenueFee := shares * auction_venue_fee;
      elsif (liquidity = 'X' or liquidity = 'F' or liquidity = 'N' or liquidity = 'H' or liquidity = 'C' or liquidity = 'U' or liquidity = 'Y' or liquidity = 'W') then
        totalVenueFee := shares * ARCA_route_venue_fee;
      end if;
      
    elsif (ecn = 'OUCH' or ecn = 'OUBX' or ecn = 'PSX') then
      -- Add
      if (liquidity = 'A' or liquidity = 'k' or liquidity = 'J' or liquidity = '7' or liquidity = '8' or liquidity = 'e' or liquidity = 'f') then
        if (price >= 1.00) then
          totalVenueFee := shares * add_venue_fee_with_price_larger_than_1;
        else
          totalVenueFee := shares * add_venue_fee_with_price_smaller_than_1;
        end if;
      -- Remove
      elsif (liquidity = 'R' or liquidity = 'm' or liquidity = '6' or liquidity = 'd') then
        if (price >= 1.00) then
          totalVenueFee := shares * remove_venue_fee_with_price_larger_than_1;
        else
          totalVenueFee := shares * price * remove_venue_fee_with_price_smaller_than_1;
        end if;
      -- Auction
      elsif (liquidity = 'O' or liquidity = 'M' or liquidity = 'L' or liquidity = 'H' or liquidity = 'K' or liquidity = 'C') then
        totalVenueFee := shares * auction_venue_fee;
      end if;
    
    elsif (ecn = 'RASH') then
      -- Add
      if (liquidity = 'A' or liquidity = 'J' or liquidity = 'V' or liquidity = 'D' or liquidity = 'F' or liquidity = 'U' or liquidity = 'k' or liquidity = '9' or liquidity = '7' or liquidity = '8' or liquidity = 'e' or liquidity = 'f') then
        if (price >= 1.00) then
          totalVenueFee := shares * add_venue_fee_with_price_larger_than_1;
        else
          totalVenueFee := shares * add_venue_fee_with_price_smaller_than_1;
        end if;
      -- Remove
      elsif (liquidity = 'R' or liquidity = 'm' or liquidity = 'd' or liquidity = '6') then
        if (price >= 1.00) then
          totalVenueFee := shares * remove_venue_fee_with_price_larger_than_1;
        else
          totalVenueFee := shares * price * remove_venue_fee_with_price_smaller_than_1;
        end if;   
      -- Auction
      elsif (liquidity = 'G' or liquidity = 'O' or liquidity = 'M' or liquidity = 'C' or liquidity = 'L' or liquidity = 'H' or liquidity = 'K' or liquidity = 'I' or liquidity = 'T' or liquidity = 'Z') then
        totalVenueFee := shares * auction_venue_fee;
      -- Route venue fee
      elsif (liquidity = 'X' or liquidity = 'Y' or liquidity = 'B' or liquidity = 'P' or liquidity = 'Q') then
        totalVenueFee := shares * NDAQ_route_venue_fee;
      end if;

    elsif (ecn = 'NYSE') then
      -- Add (provider)
      if (liquidity = '2' or liquidity = '8') then
        if (price >= 1.00) then
          totalVenueFee := shares * add_venue_fee_with_price_larger_than_1;
        else
          totalVenueFee := shares * add_venue_fee_with_price_smaller_than_1;
        end if;
      -- Remove (taker and blended)
      elsif (liquidity = '1' or liquidity = '9' or liquidity = '3' or liquidity = ' ') then
        if (price >= 1.00) then
          totalVenueFee := shares * remove_venue_fee_with_price_larger_than_1;
        else
          totalVenueFee := shares * price * remove_venue_fee_with_price_smaller_than_1;
        end if;
      -- Auction
      elsif (liquidity = '4' or liquidity = '5' or liquidity = '6' or liquidity = '7' ) then
        totalVenueFee := shares * auction_venue_fee;
      end if;
      
    elsif (ecn = 'BATZ' or ecn = 'BATY') then
      -- Add (provider)
      if (liquidity = 'A') then
        if (price >= 1.00) then
          totalVenueFee := shares * add_venue_fee_with_price_larger_than_1;
        else
          totalVenueFee := shares * add_venue_fee_with_price_smaller_than_1;
        end if;
      -- Remove (taker and blended)
      elsif (liquidity = 'R') then
        if (price >= 1.00) then
          totalVenueFee := shares * remove_venue_fee_with_price_larger_than_1;
        else
          totalVenueFee := shares * price * remove_venue_fee_with_price_smaller_than_1;
        end if;
      -- Auction
      elsif (liquidity = 'C' ) then
        totalVenueFee := shares * auction_venue_fee;
      -- Route
      elsif (liquidity = 'X' ) then
        totalVenueFee := shares * BATS_route_venue_fee;
      end if;
      
    elsif (ecn = 'EDGX') then
      -- Add
      if (liquidity = 'EA' or liquidity = 'HA' or liquidity = 'AA' or liquidity = 'CL' or liquidity = 'DM' or liquidity = 'MM' or liquidity = 'PA' or liquidity = 'RB' or liquidity = 'RC' or liquidity = 'RS' or liquidity = 'RW' or liquidity = 'RY' or liquidity = 'RZ' or liquidity = 'A' or liquidity = 'B' or liquidity = 'F' or liquidity = 'M' or liquidity = 'P' or liquidity = 'V' or liquidity = '3' or liquidity = '9' or liquidity = '4' or liquidity = '8' or liquidity = '1' or liquidity = 'E' or liquidity = 'H' or liquidity = 'Y') then
        if (price >= 1.00) then
          totalVenueFee := shares * add_venue_fee_with_price_larger_than_1;
        else
          totalVenueFee := shares * add_venue_fee_with_price_smaller_than_1;
        end if;
      -- Remove
      elsif (liquidity = 'ER' or liquidity = 'BB' or liquidity = 'DT' or liquidity = 'MT' or liquidity = 'PI' or liquidity = 'PT' or liquidity = 'C' or liquidity = 'D' or liquidity = 'G' or liquidity = 'J' or liquidity = 'L' or liquidity = 'N' or liquidity = 'U' or liquidity = 'W' or liquidity = '2' or liquidity = '6') then
        if (price >= 1.00) then
          totalVenueFee := shares * remove_venue_fee_with_price_larger_than_1;
        else
          totalVenueFee := shares * price * remove_venue_fee_with_price_smaller_than_1;
        end if;
      -- Auction
      elsif (liquidity = 'OO' or liquidity = 'O' or liquidity = '5') then
        totalVenueFee := shares * auction_venue_fee;
      -- Route
      elsif (liquidity = 'BY' or liquidity = 'PX' or liquidity = 'RP' or liquidity = 'RQ' or liquidity = 'RR' or iquidityField = 'SW' or liquidity = 'I' or liquidity = 'K' or liquidity = 'Q' or liquidity = 'R' or liquidity = 'T' or liquidity = 'X' or liquidity = 'Z' or liquidity = '7' or liquidity = 'S') then
        totalVenueFee := shares * EDGX_route_venue_fee;
      end if;
    elsif (ecn = 'EDGA') then
      -- Add
      if (liquidity = 'EA' or liquidity = 'HA' or liquidity = 'AA' or liquidity = 'CL' or liquidity = 'DM' or liquidity = 'MM' or liquidity = 'PA' or liquidity = 'RB' or liquidity = 'RC' or liquidity = 'RS' or liquidity = 'RW' or liquidity = 'RY' or liquidity = 'RZ' or liquidity = 'A' or liquidity = 'B' or liquidity = 'F' or liquidity = 'M' or liquidity = 'P' or liquidity = 'V' or liquidity = '3' or liquidity = '9' or liquidity = '4' or liquidity = '8' or liquidity = '1' or liquidity = 'E' or liquidity = 'H' or liquidity = 'Y') then
        if (price >= 1.00) then
          totalVenueFee := shares * add_venue_fee_with_price_larger_than_1;
        else
          totalVenueFee := shares * add_venue_fee_with_price_smaller_than_1;
        end if;
      -- Remove
      elsif (liquidity = 'ER' or liquidity = 'BB' or liquidity = 'DT' or liquidity = 'MT' or liquidity = 'PI' or liquidity = 'PT' or liquidity = 'HR' or liquidity = 'CR' or liquidity = 'PR' or liquidity = 'C' or liquidity = 'D' or liquidity = 'G' or liquidity = 'J' or liquidity = 'L' or liquidity = 'N' or liquidity = 'U' or liquidity = 'W' or liquidity = '2' or liquidity = '6') then
        if (price >= 1.00) then
          totalVenueFee := shares * remove_venue_fee_with_price_larger_than_1;
        else
          totalVenueFee := shares * price * remove_venue_fee_with_price_smaller_than_1;
        end if;
      -- Auction
      elsif (liquidity = 'OO' or liquidity = 'O' or liquidity = '5') then
        totalVenueFee := shares * auction_venue_fee;
      -- Route
      elsif (liquidity = 'BY' or liquidity = 'PX' or liquidity = 'RP' or liquidity = 'RQ' or liquidity = 'RR' or iquidityField = 'SW' or liquidity = 'RT' or liquidity = 'RX' or liquidity = 'XR' or liquidity = 'I' or liquidity = 'K' or liquidity = 'Q' or liquidity = 'R' or liquidity = 'T' or liquidity = 'X' or liquidity = 'Z' or liquidity = '7' or liquidity = 'S') then
        totalVenueFee := shares * EDGX_route_venue_fee;
      end if;
    end if;
    
    totalFee := totalSecFees + totalTafFee + totalWedbushCommission + totalVenueFee;
    return totalFee;
    
  END $$;


ALTER FUNCTION public.calculate_total_fee(price double precision, shares integer, side character, liquidityval text, ecn text) OWNER TO postgres;

--
-- Name: calculate_total_fee_in_trade_result(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION calculate_total_fee_in_trade_result() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  totalFee double precision;
BEGIN
  SELECT SUM(total_fee) INTO totalFee
  FROM new_orders
  WHERE date = NEW.date AND ts_id = NEW.ts_id AND cross_id = NEW.ts_cid;
  
  IF (totalFee IS NOT NULL) THEN
    NEW.ts_total_fee := totalFee;
  END IF;
  
  RETURN NEW;
END $$;


ALTER FUNCTION public.calculate_total_fee_in_trade_result() OWNER TO postgres;

--
-- Name: cancel_all_hung_order(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION cancel_all_hung_order(list_order_id text) RETURNS integer
     LANGUAGE plpgsql
    AS $$
DECLARE

	hung_order_id integer[] = string_to_array(list_order_id, '_');
	count integer;
	not_cancel integer;
	tempOrder record;
	tempCancel record;
	msg_content text;
BEGIN
	count = 1;
	not_cancel = 0;
	while hung_order_id[count] IS NOT NULL LOOP
		select * into tempOrder from new_orders where oid = hung_order_id[count];
		count = count + 1;
		msg_content = '';
		IF tempOrder.ecn = 'ARCA' AND tempOrder.ecn_suffix IS NULL then
			msg_content = msg_content || '-50=4';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-46=' || tempOrder.order_id;
		ELSIF tempOrder.ecn = 'OUCH' OR tempOrder.ecn = 'PSX' OR tempOrder.ecn = 'OUBX' THEN
			msg_content = msg_content || '-13=O';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-12=' || tempOrder.order_id;	
		ELSIF tempOrder.ecn = 'NYSE' THEN
			msg_content = msg_content || '-50=D1';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-46=' || tempOrder.order_id;
		ELSIF tempOrder.ecn = 'BATZ' OR tempOrder.ecn = 'BATY' THEN
			msg_content = msg_content || '-50=F';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-46=' || tempOrder.order_id;
		ELSIF tempOrder.ecn = 'EDGX' OR empOrder.ecn = 'EDGA' OR tempOrder.ecn = 'RASH' THEN
			msg_content = msg_content || '-13=C';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-12=' || tempOrder.order_id;
		END IF;
		msg_content = msg_content || E'\001';
		
		select * into tempCancel from cancels where oid = tempOrder.oid;
		IF  tempCancel.oid IS NULL THEN
			INSERT INTO cancels(oid, shares, time_stamp, msgseq_num,sending_time, origclord_id , arcaex_order_id, exec_id, ord_status, text, msg_content, manual_update, processed_time_stamp)
			VALUES(tempOrder.oid, tempOrder.shares, tempOrder.time_stamp, tempOrder.msgseq_num, NULL, tempOrder.order_id, NULL, NULL , NULL, NULL, msg_content, 1, tempOrder.time_stamp);
			
			UPDATE new_orders SET status = 'CanceledByECN' where oid = tempOrder.oid;			
			UPDATE trading_status SET hung_order = 1;
		ELSE
			not_cancel = not_cancel + 1;
		END IF;
		
	end loop;

	RETURN not_cancel;
END
$$;


ALTER FUNCTION public.cancel_all_hung_order(list_order_id text) OWNER TO postgres;

--
-- Name: check_allow_modify_long_short(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION check_allow_modify_long_short() RETURNS date
    LANGUAGE plpgsql
    AS $$ 
declare
  valDate date;
  lastDate date;
begin
  SELECT LOCALTIMESTAMP(2) into valDate;
  select max(date) into lastDate from manual_open_positions;

  IF (lastDate < valDate) THEN
    -- Call check_and_create_long_short
    perform check_and_create_long_short(valDate);

    return valDate;
  END IF;

  return lastDate;
end
$$;


ALTER FUNCTION public.check_allow_modify_long_short() OWNER TO postgres;

--
-- Name: check_and_create_long_short(date); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION check_and_create_long_short(td date) RETURNS integer
    LANGUAGE plpgsql
    AS $$ 
declare
  open_position record;
  long_short_count int;
  manual_long_short_count int;
  max_date date;
  buy_sell character(1);
  total_shares int;
  valTime time;
begin 
  long_short_count := 0;
  SELECT LOCALTIME(2) into valTime;

  SELECT max(date) into max_date FROM risk_managements WHERE date < td;
  SELECT count(*) into long_short_count FROM open_positions WHERE date = td;

  -- Check update long/short
  SELECT count(*) into manual_long_short_count FROM manual_open_positions WHERE date = td;

  IF (manual_long_short_count > 0) THEN
    IF long_short_count > 0 THEN 
    
      return long_short_count;
    ELSE
      for open_position in (select trim(symbol) AS symbol, price, shares, side, account FROM manual_open_positions WHERE date = td) loop
        IF (open_position.symbol <> 'T3ST') THEN
          long_short_count := long_short_count + 1;

          total_shares := open_position.shares;

          IF (open_position.side = 'S') THEN
            total_shares := -1 * total_shares;
          END IF;

          -- Insert "open_positions" table
          INSERT INTO open_positions(side, symbol, shares, price, date, account) 
          VALUES (open_position.side, trim(open_position.symbol), open_position.shares, open_position.price, td, open_position.account);

          --Insert "risk_managements" table
          INSERT INTO risk_managements(symbol, average_price, net_quantity, matched_profit_loss, total_market_value, total_quantity, date, last_update, account) 
          VALUES (trim(open_position.symbol), open_position.price, total_shares, 0.00, open_position.shares * open_position.price, open_position.shares, td, valTime, open_position.account);
        END IF;
      end loop;

      return long_short_count;
    END IF;
  ELSE
    IF long_short_count > 0 THEN 
    
      return long_short_count;
    ELSE
      -- T3ST - Long 0 @ 0.00
      INSERT INTO manual_open_positions(side, symbol, shares, price, date, account) VALUES ('B', 'T3ST', 0, 0.00, td, 0);

      for open_position in (select symbol, average_price AS price, net_quantity AS shares, total_market_value, account FROM risk_managements where date = max_date and net_quantity != 0) loop
        long_short_count := long_short_count + 1;

        buy_sell := 'B';
        total_shares := open_position.shares;

        IF (open_position.shares < 0) THEN
          buy_sell := 'S';
          total_shares := -1 * total_shares;
        END IF;

        -- Insert "manual_open_positions" table
        INSERT INTO manual_open_positions(side, symbol, shares, price, date, account) 
        VALUES (buy_sell, trim(open_position.symbol), total_shares, open_position.price, td, open_position.account);

        -- Insert "open_positions" table
        INSERT INTO open_positions(side, symbol, shares, price, date, account) 
        VALUES (buy_sell, trim(open_position.symbol), total_shares, open_position.price, td, open_position.account);

        --Insert "risk_managements" table
        INSERT INTO risk_managements(symbol, average_price, net_quantity, matched_profit_loss, total_market_value, total_quantity, date, last_update, account) 
        VALUES (trim(open_position.symbol), open_position.price, open_position.shares, 0.00, open_position.total_market_value, total_shares, td, valTime, open_position.account);
      end loop;

      return long_short_count;
    END IF;
  END IF;  
end
$$;


ALTER FUNCTION public.check_and_create_long_short(td date) OWNER TO postgres;

--
-- Name: create_new_long_short(date, text, integer, integer, double precision, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION create_new_long_short(td date, tmp_symbol text, side integer, shares integer, price double precision, accountval integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$ 
declare
  valTime time;
  rm record;
begin 
  SELECT LOCALTIME(2) INTO valTime;

  SELECT * INTO rm FROM risk_managements WHERE date = td and trim(upper(symbol)) = trim(upper(tmp_symbol)) and account = accountVal;

  IF (rm.id IS NULL) THEN
    -- Create at begin day
  INSERT INTO risk_managements(symbol, average_price, net_quantity, matched_profit_loss, total_market_value, total_quantity, date, last_update, account, manual_update, changed_shares) 
    VALUES (upper(trim(tmp_symbol)), price, shares * side, 0.00, price * shares, shares, td, valTime, accountVal, 1, shares * side - 0);
    
  UPDATE trading_status SET rmupdated = 1;
    return 0;
  ELSE 
    IF (rm.total_quantity = 0 OR rm.total_quantity <= abs(rm.net_quantity)) THEN
    -- Create/edit at begin day
    UPDATE risk_managements
      SET average_price = price, net_quantity = shares * side, total_quantity = shares, matched_profit_loss = 0.00, total_market_value = price*shares, last_update = valTime, manual_update = 1, changed_shares = shares * side - rm.net_quantity
      WHERE date = td and trim(upper(symbol)) = trim(upper(tmp_symbol)) and account = accountVal;
  ELSE
    -- Create/edit at trading period: total_quantity = rm.total_quantity + abs(rm.net_quantity - shares * side);
    UPDATE risk_managements
      SET average_price = price, net_quantity = shares * side, total_quantity = rm.total_quantity + abs(rm.net_quantity - shares * side), total_market_value = price*shares, last_update = valTime, manual_update = 1, changed_shares = shares * side - rm.net_quantity
      WHERE date = td and trim(upper(symbol)) = trim(upper(tmp_symbol)) and account = accountVal;
  END IF;

  UPDATE trading_status SET rmupdated = 1;
    return 0;
  END IF;

  return -1;
end
$$;


ALTER FUNCTION public.create_new_long_short(td date, tmp_symbol text, side integer, shares integer, price double precision, accountval integer) OWNER TO postgres;

--
-- Name: delete_all(date); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION delete_all(td date) RETURNS integer
    LANGUAGE plpgsql
    AS $$ 
declare
oids int[];
oid_count int;

begin

--clear risk management
--  delete from risk_managements where date = td;
--delete brokens
--  delete from brokens where trade_date = td;
--get all 

  delete from raw_crosses where date = td;
  delete from trade_results where date = td;
  delete from risk_managements where date = td;
  delete from rm_simultaneous_trades where date = td;
  delete from open_positions where date = td;
  delete from brokens where trade_date = td;
  delete from ask_bids where date = td;
  delete from bbo_infos where date = td;
  
  oid_count := 0;
  select count(*) into oid_count from new_orders where date = td;

    IF oid_count > 0 THEN 
  select ARRAY(select oid as order_id from new_orders where date = td) into oids ;  

  --clear broken_trades
  EXECUTE  'delete from broken_trades where (oid in (' || array_to_string(oids,',') || '))';
  --clear cancel_pendings
  EXECUTE  'delete from cancel_pendings where (oid in (' || array_to_string(oids,',') || '))';
  --clear cancel_rejects
  EXECUTE  'delete from cancel_rejects where (oid in (' || array_to_string(oids,',') || '))';
  --clear cancel_acks
  EXECUTE  'delete from cancel_acks where (oid in (' || array_to_string(oids,',') || '))';
  --clear cancel_requests
  EXECUTE  'delete from cancel_requests where (oid in (' || array_to_string(oids,',') || '))';
  --clear cancels
  EXECUTE  'delete from cancels where (oid in (' || array_to_string(oids,',') || '))';
  --clear fills
  EXECUTE  'delete from fills where (oid in (' || array_to_string(oids,',') || '))';
  --clear order_acks
  EXECUTE  'delete from order_acks where (oid in (' || array_to_string(oids,',') || '))';
  --clear order_rejects
  EXECUTE  'delete from order_rejects where (oid in (' || array_to_string(oids,',') || '))';

  delete from new_orders where date = td; 
  --clear asks
  --delete from asks where cid in(select cid from crosses where date=td);
  --bids
  --delete from bids where cid in(select cid from crosses where date=td);

  return oid_count;
  ELSE
  return oid_count;
  END IF;
end
$$;


ALTER FUNCTION public.delete_all(td date) OWNER TO postgres;

--
-- Name: delete_long_short(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION delete_long_short(idval integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$ 
declare
  valTime time;
  rm record;
begin  
  SELECT LOCALTIME(2) into valTime;
  
  SELECT * INTO rm FROM risk_managements WHERE id = idVal;
  
  IF (rm.id IS NOT NULL) THEN
    IF (rm.total_quantity <= abs(rm.net_quantity)) THEN
      -- Delete at begin day: Reset all to zero
      UPDATE risk_managements
        SET net_quantity = 0.00, total_quantity = 0, matched_profit_loss = 0.00, total_market_value = 0.00, last_update = valTime, manual_update = 1, changed_shares = 0 - rm.net_quantity
        WHERE id = idVal;
    ELSE    
      -- Delete at trading period: total_quantity = rm.total_quantity + abs(rm.net_quantity)
      UPDATE risk_managements
        SET net_quantity = 0.00, total_quantity = rm.total_quantity + abs(rm.net_quantity), total_market_value = 0.00, last_update = valTime, manual_update = 1, changed_shares = 0 - rm.net_quantity
        WHERE id = idVal;
    END IF;
    
    UPDATE trading_status SET rmupdated = 1;
    
    return 0;
  END IF;

  return -1;
end
$$;


ALTER FUNCTION public.delete_long_short(idval integer) OWNER TO postgres;

--
-- Name: get_mutiple_exchanges_trade(date, date); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION get_mutiple_exchanges_trade(date_from date, date_to date) RETURNS text
    LANGUAGE plpgsql
    AS $$ 
declare
ret text;
BEGIN
  ret ='(-1)';

  return ret;
END
$$;


ALTER FUNCTION public.get_mutiple_exchanges_trade(date_from date, date_to date) OWNER TO postgres;

--
-- Name: get_query_string_array_id_multi_orders(date, date, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION get_query_string_array_id_multi_orders(date_from date, date_to date, entry_exit_filter text) RETURNS text
    LANGUAGE plpgsql
    AS $$ 
declare
ret text;
BEGIN
 ret ='SELECT DISTINCT (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + cross_id) from (
          SELECT tmp.date, tmp.cross_id, tmp.ts_id, tmp.symbol, tmp.entry_exit,count(tmp.entry_exit) as count
          FROM (SELECT date, cross_id, ts_id, symbol, ecn, entry_exit, count(entry_exit) AS count_entryexit
               FROM new_orders
               WHERE new_orders.ts_id <> -1 and date between ''' || date_from || ''' and ''' || date_to ||''' and entry_exit = ''' || entry_exit_filter ||'''
               GROUP BY cross_id, date, ts_id, symbol, ecn, entry_exit
               ORDER BY cross_id
            ) as tmp
          GROUP BY tmp.date, tmp.cross_id, tmp.ts_id, tmp.symbol, tmp.entry_exit
          HAVING count(tmp.entry_exit) > 1
          ORDER BY tmp.cross_id
        ) as tmp2';

  return ret;
END
$$;


ALTER FUNCTION public.get_query_string_array_id_multi_orders(date_from date, date_to date, entry_exit_filter text) OWNER TO postgres;

--
-- Name: insert_bust_or_correct(integer, date, integer, double precision, time without time zone, text, text, text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION insert_bust_or_correct(_orderid integer, _date date, _shares integer, _price double precision, _timestamp time without time zone, _seqnum text, _executionid text, _msgcontents text, _status text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
  orderID integer;
BEGIN
  
  SELECT oid INTO orderID 
  FROM new_orders 
  WHERE order_id = _orderID AND date = _date;
  
  IF orderID IS NULL THEN
    RETURN -1;
  END IF;
  
  INSERT INTO broken_trades (oid, shares, price, time_stamp, msgseq_num, origclord_id, exec_id, msg_content) 
  VALUES (orderID, _shares, _price, _timeStamp, _seqNum, _orderID, _executionId, _msgContents);
  
  UPDATE new_orders 
  SET status = _status 
  WHERE oid = orderID;
  
  RETURN 0;
END
$$;


ALTER FUNCTION public.insert_bust_or_correct(_orderid integer, _date date, _shares integer, _price double precision, _timestamp time without time zone, _seqnum text, _executionid text, _msgcontents text, _status text) OWNER TO postgres;

--
-- Name: insert_cancel_ack(integer, date, time without time zone, text, text, text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION insert_cancel_ack(_orderid integer, _date date, _timestamp time without time zone, _seqnum text, _exorderid text, _msgcontents text, _status text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
  orderID integer;
BEGIN
  
  SELECT oid INTO orderID 
  FROM new_orders 
  WHERE order_id = _orderID AND date = _date;
  
  IF orderID IS NULL THEN
    RETURN -1;
  END IF;
  
  INSERT INTO cancel_acks (oid, time_stamp, msgseq_num, origclord_id, arcaex_order_id, msg_content) 
  VALUES (orderID, _timeStamp, _seqNum, _orderID, _exorderid, _msgContents);
  
  UPDATE new_orders 
  SET status = _status 
  WHERE oid = orderID;
  
  RETURN 0;
END
$$;


ALTER FUNCTION public.insert_cancel_ack(_orderid integer, _date date, _timestamp time without time zone, _seqnum text, _exorderid text, _msgcontents text, _status text) OWNER TO postgres;

--
-- Name: insert_cancel_hung_order(integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION insert_cancel_hung_order(oid_t integer, cancel_shares integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	temporder record;
	tempCancel record;
	msg_content text;
BEGIN

	select * into tempOrder from new_orders where oid = oid_t;
	
	select * into tempCancel from cancels where oid = oid_t;
	msg_content = '';
	IF tempOrder.ecn = 'ARCA' AND tempOrder.ecn_suffix IS NULL then
		msg_content = msg_content || '-50=4';
		msg_content = msg_content || E'\001';
		msg_content = msg_content || '-46=' || tempOrder.order_id;
	ELSIF tempOrder.ecn = 'OUCH' OR tempOrder.ecn = 'PSX' OR tempOrder.ecn = 'OUBX' THEN
		msg_content = msg_content || '-13=O';
		msg_content = msg_content || E'\001';
		msg_content = msg_content || '-12=' || tempOrder.order_id;
	ELSIF tempOrder.ecn = 'NYSE' THEN
		msg_content = msg_content || '-50=D1';
		msg_content = msg_content || E'\001';
		msg_content = msg_content || '-46=' || tempOrder.order_id;
	ELSIF tempOrder.ecn = 'BATZ' OR tempOrder.ecn = 'BATY' THEN
		msg_content = msg_content || '-50=F';
		msg_content = msg_content || E'\001';
		msg_content = msg_content || '-46=' || tempOrder.order_id;
	ELSIF tempOrder.ecn = 'EDGX' OR tempOrder.ecn = 'EDGA' OR tempOrder.ecn = 'RASH' THEN
		msg_content = msg_content || '-13=C';
		msg_content = msg_content || E'\001';
		msg_content = msg_content || '-12=' || tempOrder.order_id;
	END IF;
	
	msg_content = msg_content || E'\001';
	
	IF tempCancel.oid is NULL then
		INSERT INTO cancels(oid, shares, time_stamp, msgseq_num, origclord_id, msg_content, manual_update, processed_time_stamp)
		VALUES(tempOrder.oid, cancel_shares, tempOrder.time_stamp, tempOrder.msgseq_num, tempOrder.order_id , msg_content, 1, tempOrder.time_stamp);
		
		UPDATE new_orders SET status = 'CanceledByECN' WHERE oid = oid_t;
		UPDATE trading_status SET hung_order = 1;	
	ELSE
		return 0;
	END IF;
	
	return 1;
END
$$;


ALTER FUNCTION public.insert_cancel_hung_order(oid_t integer, cancel_shares integer) OWNER TO postgres;

--
-- Name: insert_cancel_pending(integer, date, time without time zone, text, text, integer, double precision, text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION insert_cancel_pending(_orderid integer, _date date, _timestamp time without time zone, _seqnum text, _exorderid text, _shares integer, _price double precision, _msgcontents text, _status text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
  orderID integer;
BEGIN
  
  SELECT oid INTO orderID 
  FROM new_orders 
  WHERE order_id = _orderID AND date = _date;
  
  IF orderID IS NULL THEN
    RETURN -1;
  END IF;

  INSERT INTO cancel_pendings (oid, time_stamp, msgseq_num, origclord_id, arcaex_order_id, shares, price, msg_content) 
  VALUES (orderID, _timeStamp, _seqNum, _orderID, _exorderid, _shares, _price, _msgContents);
  
  UPDATE new_orders 
  SET status = _status 
  WHERE oid = orderID;
  
  RETURN 0;
END
$$;


ALTER FUNCTION public.insert_cancel_pending(_orderid integer, _date date, _timestamp time without time zone, _seqnum text, _exorderid text, _shares integer, _price double precision, _msgcontents text, _status text) OWNER TO postgres;

--
-- Name: insert_cancel_rejected(integer, date, time without time zone, text, text, text, text, text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION insert_cancel_rejected(_orderid integer, _date date, _timestamp time without time zone, _seqnum text, _exorderid text, _reason text, _cancelreject text, _msgcontents text, _status text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
  orderID integer;
BEGIN
  
  SELECT oid INTO orderID 
  FROM new_orders 
  WHERE order_id = _orderID AND date = _date;
  
  IF orderID IS NULL THEN
    RETURN -1;
  END IF;

  INSERT INTO cancel_rejects (oid, time_stamp, msgseq_num, origclord_id, arcaex_order_id, reason, cancel_reject, msg_content) 
  VALUES (orderID, _timeStamp, _seqNum, _orderID, _exorderid, _reason, _cancelReject, _msgContents);
  
  UPDATE new_orders 
  SET status = _status 
  WHERE oid = orderID;
  
  RETURN 0;
END
$$;


ALTER FUNCTION public.insert_cancel_rejected(_orderid integer, _date date, _timestamp time without time zone, _seqnum text, _exorderid text, _reason text, _cancelreject text, _msgcontents text, _status text) OWNER TO postgres;

--
-- Name: insert_cancel_request(integer, date, time without time zone, text, text, text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION insert_cancel_request(_orderid integer, _date date, _timestamp time without time zone, _seqnum text, _exorderid text, _msgcontents text, _status text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
  orderID integer;
BEGIN
  
  SELECT oid INTO orderID 
  FROM new_orders 
  WHERE order_id = _orderID AND date = _date;
  
  IF orderID IS NULL THEN
    RETURN -1;
  END IF;

  INSERT INTO cancel_requests (oid, time_stamp, msgseq_num, origclord_id, arcaex_order_id, msg_content) 
  VALUES (orderID, _timeStamp, _seqNum, _orderID, _exOrderId, _msgContents);
  
  UPDATE new_orders 
  SET status = _status 
  WHERE oid = orderID;
  
  RETURN 0;
END
$$;


ALTER FUNCTION public.insert_cancel_request(_orderid integer, _date date, _timestamp time without time zone, _seqnum text, _exorderid text, _msgcontents text, _status text) OWNER TO postgres;

--
-- Name: insert_fill_hung_order(integer, text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION insert_fill_hung_order(oid_t integer, list_shares text, list_price text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	shares_array integer[] = string_to_array(list_shares, '_');
	price_array double precision[] = string_to_array(list_price, '_');
	tempOrder record;
	tempFill record;
	i integer;
	liquidity character;
	msg_content text;
	id_current integer;
	exce_id_tmp text;
	total_fill_shares integer;
	totalFee double precision;
BEGIN
 
	i = 1;
	total_fill_shares = 0;
	select * into tempOrder from new_orders where oid = oid_t;
	if tempOrder.ecn = 'ARCA' or tempOrder.ecn = 'OUCH' or tempOrder.ecn = 'OUBX' or tempOrder.ecn = 'PSX' or tempOrder.ecn = 'RASH' or tempOrder.ecn = 'BATZ' or tempOrder.ecn = 'BATY' then
		liquidity = 'R';
	elsif tempOrder.ecn = 'NYSE' then
		liquidity = '1';
	elsif tempOrder.ecn = 'EDGX' or tempOrder.ecn = 'EDGA' then
		liquidity = '6';
	end if;
	select max(id) into id_current from fills; 
	IF id_current IS NULL THEN
		id_current = 1;
	END IF;
	exce_id_tmp = '';
	WHILE shares_array[i] IS NOT NULL LOOP
		msg_content = '';
		total_fill_shares = total_fill_shares + shares_array[i];
		if tempOrder.ecn = 'ARCA' AND tempOrder.ecn_suffix IS NULL then
			msg_content = msg_content || '-50=2';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-46=' || tempOrder.order_id;
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-44=' || shares_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-43=' || (price_array[i]*100);
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-42=' || '2';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-29=' || '99999';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-28=' || 'R';
		ELSIF tempOrder.ecn = 'OUCH' OR tempOrder.ecn = 'OUBX' OR tempOrder.ecn = 'PSX' THEN
			msg_content = msg_content || '-13=E';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-12=' || tempOrder.order_id;
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-10=' || shares_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-8=' || price_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-2=' || 'R';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-29=' || '99999';
			msg_content = msg_content || E'\001';
		ELSIF tempOrder.ecn = 'RASH' THEN
			msg_content = msg_content || '-13=E';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-12=' || tempOrder.order_id;
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-10=' || shares_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-8=' || price_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-2=' || 'R';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-29=' || '99999';
			msg_content = msg_content || E'\001';
		ELSIF tempOrder.ecn = 'NYSE' THEN
			msg_content = msg_content || '-50=81';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-10=' || shares_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-43=' || (price_array[i]*100);
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-42=' || '2';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-29=' || '99999';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-28=' || '1';
		ELSIF tempOrder.ecn = 'BATZ' OR tempOrder.ecn = 'BATY'THEN
			msg_content = msg_content || '-50=11';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-81=' || shares_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-83=' || price_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-28=' || 'R';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-29=' || '99999';
			msg_content = msg_content || E'\001';
		ELSIF tempOrder.ecn = 'EDGX' OR tempOrder.ecn = 'EDGA' THEN
			msg_content = msg_content || '-13=E';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-10=' || shares_array[i];
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-8=' || (price_array[i]);
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-2=' || '6';
			msg_content = msg_content || E'\001';
			msg_content = msg_content || '-29=' || '99999';
			msg_content = msg_content || E'\001';
      
		END IF;
		
		msg_content = msg_content || E'\001';
		id_current = id_current + 1;
		exce_id_tmp = '99999' || '_' || id_current;
		
		-- Calculate fill fee
		SELECT calculate_total_fee(price_array[i], shares_array[i], substring(tempOrder.side from 1 for 1), liquidity, tempOrder.ecn) into totalFee;
		
		INSERT INTO fills(oid, shares, price, time_stamp, processed_time_stamp, msgseq_num, origclord_id, exec_id, msg_content, liquidity,  manual_update, total_fee)
			VALUES(oid_t, shares_array[i], price_array[i], tempOrder.time_stamp, tempOrder.time_stamp, tempOrder.msgseq_num, tempOrder.order_id, exce_id_tmp, msg_content, liquidity, 1, totalFee);
		
		i = i + 1;
	END LOOP;
	
	IF total_fill_shares < tempOrder.shares THEN
		UPDATE new_orders SET status = 'Lived' where oid = oid_t;
		UPDATE new_orders SET left_shares = (tempOrder.shares - total_fill_shares) where oid = oid_t;
	ELSE
		UPDATE new_orders SET status = 'Closed' where oid = oid_t;
		UPDATE new_orders SET left_shares = 0 where oid = oid_t;
	END IF;
	
	UPDATE trading_status SET hung_order = 1;
	
	return 1;
END
$$;

ALTER FUNCTION public.insert_fill_hung_order(oid_t integer, list_shares text, list_price text) OWNER TO postgres;


--
-- Name: insert_order_accepted(integer, date, time without time zone, time without time zone, text, text, text, double precision, integer, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION insert_order_accepted(_orderid integer, _date date, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _exorderid text, _msgcontents text, _ackprice double precision, _ackshares integer, _status text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
  newOrder record;
BEGIN
  
  SELECT * INTO newOrder 
  FROM new_orders 
  WHERE order_id = _orderID AND date = _date;
  
  IF newOrder IS NULL THEN
    RETURN -1;
  END IF;
  
  INSERT INTO order_acks (oid, time_stamp, processed_time_stamp, msgseq_num, origclord_id, arcaex_order_id, msg_content, price) 
  VALUES (newOrder.oid, _timeStamp, _processedTimestamp, _seqNum, _orderID, _exOrderId, _msgContents, _ackPrice);
  
  IF (_ackShares = 0) THEN
    _ackShares := newOrder.shares;
  END IF;
  
  IF (_ackPrice = 0) THEN
    _ackPrice := newOrder.price;
  END IF;
  
  UPDATE new_orders 
  SET status = _status, ack_price = _ackPrice, left_shares = _ackShares, avg_price =  _ackPrice 
  WHERE oid = newOrder.oid;
  
  RETURN 0;
END
$$;


ALTER FUNCTION public.insert_order_accepted(_orderid integer, _date date, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _exorderid text, _msgcontents text, _ackprice double precision, _ackshares integer, _status text) OWNER TO postgres;

--
-- Name: insert_order_canceled(integer, date, time without time zone, time without time zone, text, text, text, text, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION insert_order_canceled(_orderid integer, _date date, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _arcaex_orderid text, _msgcontents text, _status text, _entryexitrefnum integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
  orderID integer;
BEGIN
  
  SELECT oid INTO orderID 
  FROM new_orders 
  WHERE order_id = _orderID AND date = _date;
  
  IF orderID IS NULL THEN
    RETURN -1;
  END IF;

  INSERT INTO cancels (oid, time_stamp, processed_time_stamp, msgseq_num, origclord_id, arcaex_order_id, msg_content, entry_exit_ref) 
  VALUES (orderID, _timeStamp, _processedtimestamp, _seqNum, _orderID, _arcaex_orderid, _msgContents, _entryExitRefNum);
    
  UPDATE new_orders 
  SET status = _status 
  WHERE oid = orderID;
  
  RETURN 0;
END
$$;


ALTER FUNCTION public.insert_order_canceled(_orderid integer, _date date, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _arcaex_orderid text, _msgcontents text, _status text, _entryexitrefnum integer) OWNER TO postgres;

--
-- Name: insert_order_executed(integer, date, integer, double precision, time without time zone, time without time zone, text, text, text, text, character, text, integer, double precision, integer, double precision); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION insert_order_executed(_orderid integer, _date date, _shares integer, _price double precision, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _exorderid text, _execid text, _msgcontents text, _liquidity character, _status text, _leftshares integer, _avgprice double precision, _entryexitrefnum integer, _totalfee double precision) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
  orderID integer;
BEGIN
  
  SELECT oid INTO orderID 
  FROM new_orders 
  WHERE order_id = _orderID AND date = _date;
  
  IF orderID IS NULL THEN
    RETURN -1;
  END IF;
  
  INSERT INTO fills (oid, shares, price, time_stamp, processed_time_stamp, msgseq_num, origclord_id, arcaex_order_id, exec_id, msg_content, liquidity, entry_exit_ref, total_fee) 
  VALUES (orderID, _shares, _price, _timestamp, _processedTimestamp, _seqnum, _orderID, _exorderid, _execid, _msgcontents, _liquidity, _entryExitRefNum, _totalFee);

  UPDATE new_orders 
  SET status = _status, left_shares = _leftshares, avg_price = _avgprice 
  WHERE oid = orderID;
  
  RETURN 0;
END$$;


ALTER FUNCTION public.insert_order_executed(_orderid integer, _date date, _shares integer, _price double precision, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _exorderid text, _execid text, _msgcontents text, _liquidity character, _status text, _leftshares integer, _avgprice double precision, _entryexitrefnum integer, _totalfee double precision) OWNER TO postgres;

--
-- Name: insert_order_rejected(integer, date, time without time zone, time without time zone, text, text, text, text, text, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION insert_order_rejected(_orderid integer, _date date, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _reason text, _text text, _msgcontents text, _status text, _entryexitrefnum integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
  orderID integer;
BEGIN
  SELECT oid INTO orderID 
  FROM new_orders 
  WHERE order_id = _orderID AND date = _date;
  
  IF orderID IS NULL THEN
    RETURN -1;
  END IF;
  
  INSERT INTO order_rejects (oid, time_stamp, processed_time_stamp, msgseq_num, origclord_id, reason, text, msg_content, entry_exit_ref) 
  VALUES (orderID, _timeStamp, _processedtimestamp, _seqNum, _orderID, _reason, _text, _msgContents, _entryExitRefNum);

  UPDATE new_orders 
  SET status = _status 
  WHERE oid = orderID;
  
  RETURN 0;
END
$$;


ALTER FUNCTION public.insert_order_rejected(_orderid integer, _date date, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _reason text, _text text, _msgcontents text, _status text, _entryexitrefnum integer) OWNER TO postgres;

--
-- Name: insert_spread_into_trade_results(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION insert_spread_into_trade_results() RETURNS integer
    LANGUAGE plpgsql
    AS $$ 
DECLARE
  temp record;
BEGIN
  FOR temp in (SELECT tmp.date, tmp.ts_id, tmp.ts_cid, (sum(tmp.price) * 100::double precision)::integer AS spread FROM ( SELECT ask_bids.date, ask_bids.ts_id, ask_bids.ts_cid, ask_bids.ask_bid, CASE WHEN ask_bids.ask_bid = 'ASK'::bpchar THEN min(ask_bids.price) * (-1)::double precision ELSE max(ask_bids.price) END AS price FROM ask_bids WHERE date >= '2013-11-15' GROUP BY ask_bids.date, ask_bids.ts_id, ask_bids.ts_cid, ask_bids.ask_bid) tmp GROUP BY tmp.date, tmp.ts_id, tmp.ts_cid) LOOP
    UPDATE trade_results SET spread = temp.spread WHERE date = temp.date and ts_id = temp.ts_id and ts_cid = temp.ts_cid;
    --INSERT INTO trade_results(spread) VALUES (temp.spread) WHERE date = temp.date and ts_id = temp.ts_id and ts_cid = temp.ts_cid
  END LOOP;

  return 1;
END
$$;


ALTER FUNCTION public.insert_spread_into_trade_results() OWNER TO postgres;

--
-- Name: insertacceptedorder4arca_bats(integer, date, time without time zone, time without time zone, text, text, text, double precision, text, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION insertacceptedorder4arca_bats(_orderid integer, _date date, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _arcaexorderid text, _msgcontents text, _price double precision, _status text, _shares integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
  orderID integer;
BEGIN
  
  SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
  
  IF orderID IS NULL THEN
    RETURN -1;
  END IF;
  
  INSERT INTO order_acks (oid, time_stamp, processed_time_stamp, msgseq_num, origclord_id, arcaex_order_id, msg_content, price) VALUES (orderID, _timeStamp, _processedTimestamp, _seqNum, _orderID, _arcaExOrderId, _msgContents, _price);
  
  UPDATE new_orders SET status = _status, left_shares = _shares, avg_price =  _price WHERE oid = orderID;
  
  RETURN 0;
END
$$;


ALTER FUNCTION public.insertacceptedorder4arca_bats(_orderid integer, _date date, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _arcaexorderid text, _msgcontents text, _price double precision, _status text, _shares integer) OWNER TO postgres;

--
-- Name: insertacceptedorder4nasdaq(integer, date, time without time zone, time without time zone, text, text, text, double precision, text, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION insertacceptedorder4nasdaq(_orderid integer, _date date, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _arcaexorderid text, _msgcontents text, _price double precision, _status text, _shares integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
  orderID integer;
BEGIN
  
  SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
  
  IF orderID IS NULL THEN
    RETURN -1;
  END IF;
  
  INSERT INTO order_acks (oid, time_stamp, processed_time_stamp, msgseq_num, origclord_id, arcaex_order_id, msg_content, price) VALUES (orderID, _timeStamp, _processedTimestamp, _seqNum, _orderID, _arcaExOrderId, _msgContents, _price);
  
  UPDATE new_orders SET status = _status, shares = _shares WHERE oid = orderID;
  
  RETURN 0;
END
$$;


ALTER FUNCTION public.insertacceptedorder4nasdaq(_orderid integer, _date date, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _arcaexorderid text, _msgcontents text, _price double precision, _status text, _shares integer) OWNER TO postgres;

--
-- Name: insertbrokentrade4nasdaq(integer, date, time without time zone, text, text, text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION insertbrokentrade4nasdaq(_orderid integer, _date date, _timestamp time without time zone, _seqnum text, _executionid text, _msgcontents text, _status text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
  orderID integer;
BEGIN
  
  SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
  
  IF orderID IS NULL THEN
    RETURN -1;
  END IF;
  
  INSERT INTO broken_trades (oid, time_stamp, msgseq_num, origclord_id, exec_id, msg_content) VALUES (orderID, _timeStamp, _seqNum, _orderID, _executionId, _msgContents);
  
  UPDATE new_orders SET status = _status WHERE oid = orderID;
  
  RETURN 0;
END
$$;


ALTER FUNCTION public.insertbrokentrade4nasdaq(_orderid integer, _date date, _timestamp time without time zone, _seqnum text, _executionid text, _msgcontents text, _status text) OWNER TO postgres;

--
-- Name: insertbustorcorrect4arca_direct(integer, date, integer, double precision, time without time zone, text, text, text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION insertbustorcorrect4arca_direct(_orderid integer, _date date, _shares integer, _price double precision, _timestamp time without time zone, _seqnum text, _executionid text, _msgcontents text, _status text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
  orderID integer;
BEGIN
  
  SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
  
  IF orderID IS NULL THEN
    RETURN -1;
  END IF;
  
  INSERT INTO broken_trades (oid, shares, price, time_stamp, msgseq_num, origclord_id, exec_id, msg_content) VALUES (orderID, _shares, _price, _timeStamp, _seqNum, _orderID, _executionId, _msgContents);
  
  UPDATE new_orders SET status = _status WHERE oid = orderID;
  
  RETURN 0;
END
$$;


ALTER FUNCTION public.insertbustorcorrect4arca_direct(_orderid integer, _date date, _shares integer, _price double precision, _timestamp time without time zone, _seqnum text, _executionid text, _msgcontents text, _status text) OWNER TO postgres;

--
-- Name: insertbustorcorrect4nyse_ccg(integer, date, integer, double precision, time without time zone, text, text, text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION insertbustorcorrect4nyse_ccg(_orderid integer, _date date, _shares integer, _price double precision, _timestamp time without time zone, _seqnum text, _executionid text, _msgcontents text, _status text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
  orderID integer;
BEGIN
  
  SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
  
  IF orderID IS NULL THEN
    RETURN -1;
  END IF;
  
  INSERT INTO broken_trades (oid, shares, price, time_stamp, msgseq_num, origclord_id, exec_id, msg_content) VALUES (orderID, _shares, _price, _timeStamp, _seqNum, _orderID, _executionId, _msgContents);
  
  UPDATE new_orders SET status = _status WHERE oid = orderID;
  
  RETURN 0;
END
$$;


ALTER FUNCTION public.insertbustorcorrect4nyse_ccg(_orderid integer, _date date, _shares integer, _price double precision, _timestamp time without time zone, _seqnum text, _executionid text, _msgcontents text, _status text) OWNER TO postgres;

--
-- Name: insertcancelack4arca_direct(integer, date, time without time zone, text, text, text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION insertcancelack4arca_direct(_orderid integer, _date date, _timestamp time without time zone, _seqnum text, _arcaexorderid text, _msgcontents text, _status text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
  orderID integer;
BEGIN
  
  SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
  
  IF orderID IS NULL THEN
    RETURN -1;
  END IF;
  
  INSERT INTO cancel_acks (oid, time_stamp, msgseq_num, origclord_id, arcaex_order_id, msg_content) VALUES (orderID, _timeStamp, _seqNum, _orderID, _arcaExOrderId, _msgContents);
  
  UPDATE new_orders SET status = _status WHERE oid = orderID;
  
  RETURN 0;
END
$$;


ALTER FUNCTION public.insertcancelack4arca_direct(_orderid integer, _date date, _timestamp time without time zone, _seqnum text, _arcaexorderid text, _msgcontents text, _status text) OWNER TO postgres;

--
-- Name: insertcancelack4nyse_ccg(integer, date, time without time zone, text, text, text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION insertcancelack4nyse_ccg(_orderid integer, _date date, _timestamp time without time zone, _seqnum text, _meorderid text, _msgcontents text, _status text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
  orderID integer;
BEGIN
  
  SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
  
  IF orderID IS NULL THEN
    RETURN -1;
  END IF;
  
  INSERT INTO cancel_acks (oid, time_stamp, msgseq_num, origclord_id, arcaex_order_id, msg_content) VALUES (orderID, _timeStamp, _seqNum, _orderID, _meorderid, _msgContents);
  
  UPDATE new_orders SET status = _status WHERE oid = orderID;
  
  RETURN 0;
END
$$;


ALTER FUNCTION public.insertcancelack4nyse_ccg(_orderid integer, _date date, _timestamp time without time zone, _seqnum text, _meorderid text, _msgcontents text, _status text) OWNER TO postgres;

--
-- Name: insertcanceledorder4nasdaq(integer, date, time without time zone, time without time zone, text, text, text, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION insertcanceledorder4nasdaq(_orderid integer, _date date, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _msgcontents text, _status text, _entryexitrefnum integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
  orderID integer;
BEGIN
  
  SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
  
  IF orderID IS NULL THEN
    RETURN -1;
  END IF;

  INSERT INTO cancels (oid, time_stamp, processed_time_stamp, msgseq_num, origclord_id, msg_content, entry_exit_ref) VALUES (orderID, _timeStamp, _processedtimestamp, _seqNum, _orderID, _msgContents, _entryExitRefNum);
    
  UPDATE new_orders SET status = _status where oid = orderID;
  
  RETURN 0;
END
$$;


ALTER FUNCTION public.insertcanceledorder4nasdaq(_orderid integer, _date date, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _msgcontents text, _status text, _entryexitrefnum integer) OWNER TO postgres;

--
-- Name: insertcancelpending4arca_bats(integer, date, time without time zone, text, text, integer, double precision, text, text, integer, double precision); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION insertcancelpending4arca_bats(_orderid integer, _date date, _timestamp time without time zone, _seqnum text, _executionid text, _shares integer, _price double precision, _msgcontents text, _status text, _leftshares integer, _avgprice double precision) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
  orderID integer;
BEGIN
  
  SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
  
  IF orderID IS NULL THEN
    RETURN -1;
  END IF;

  INSERT INTO cancel_pendings (oid, time_stamp, msgseq_num, origclord_id, arcaex_order_id, shares, price, msg_content) VALUES (orderID, _timeStamp, _seqNum, _orderID, _executionId, _shares, _price, _msgContents);
  
  UPDATE new_orders SET status = _status, left_shares = _leftShares, avg_price= _avgPrice where oid = orderID;
  
  RETURN 0;
END
$$;


ALTER FUNCTION public.insertcancelpending4arca_bats(_orderid integer, _date date, _timestamp time without time zone, _seqnum text, _executionid text, _shares integer, _price double precision, _msgcontents text, _status text, _leftshares integer, _avgprice double precision) OWNER TO postgres;

--
-- Name: insertcancelpending4nasdaq(integer, date, time without time zone, text, text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION insertcancelpending4nasdaq(_orderid integer, _date date, _timestamp time without time zone, _seqnum text, _msgcontents text, _status text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
  orderID integer;
BEGIN
  
  SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
  
  IF orderID IS NULL THEN
    RETURN -1;
  END IF;

  INSERT INTO cancel_pendings (oid, time_stamp, msgseq_num, origclord_id, shares, price, msg_content) VALUES (orderID, _timeStamp, _seqNum, _orderID, 0, 0.00, _msgContents);

  UPDATE new_orders SET status = _status where oid = orderID;
  
  RETURN 0;
END
$$;


ALTER FUNCTION public.insertcancelpending4nasdaq(_orderid integer, _date date, _timestamp time without time zone, _seqnum text, _msgcontents text, _status text) OWNER TO postgres;

--
-- Name: insertcancelreject4nasdaq(integer, date, time without time zone, text, text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION insertcancelreject4nasdaq(_orderid integer, _date date, _timestamp time without time zone, _seqnum text, _msgcontents text, _status text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
  orderID integer;
BEGIN
  
  SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
  
  IF orderID IS NULL THEN
    RETURN -1;
  END IF;

  INSERT INTO cancel_rejects (oid, time_stamp, msgseq_num, origclord_id, msg_content) VALUES (orderID, _timeStamp, _seqNum, _orderID, _msgContents);
  
  UPDATE new_orders SET status = _status where oid = orderID;
  
  RETURN 0;
END
$$;


ALTER FUNCTION public.insertcancelreject4nasdaq(_orderid integer, _date date, _timestamp time without time zone, _seqnum text, _msgcontents text, _status text) OWNER TO postgres;

--
-- Name: insertcancelrequest4arca_bats(integer, date, time without time zone, text, text, text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION insertcancelrequest4arca_bats(_orderid integer, _date date, _timestamp time without time zone, _seqnum text, _arcaexorderid text, _msgcontents text, _status text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
  orderID integer;
BEGIN
  
  SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
  
  IF orderID IS NULL THEN
    RETURN -1;
  END IF;

  INSERT INTO cancel_requests (oid, time_stamp, msgseq_num, origclord_id, arcaex_order_id, msg_content) VALUES (orderID, _timeStamp, _seqNum, _orderID, _arcaExOrderId, _msgContents);
  
  UPDATE new_orders SET status = _status where oid = orderID;
  
  RETURN 0;
END
$$;


ALTER FUNCTION public.insertcancelrequest4arca_bats(_orderid integer, _date date, _timestamp time without time zone, _seqnum text, _arcaexorderid text, _msgcontents text, _status text) OWNER TO postgres;

--
-- Name: insertcancelrequest4nasdaq(integer, date, time without time zone, text, text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION insertcancelrequest4nasdaq(_orderid integer, _date date, _timestamp time without time zone, _seqnum text, _msgcontents text, _status text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
  orderID integer;
BEGIN
  
  SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
  
  IF orderID IS NULL THEN
    RETURN -1;
  END IF;

  INSERT INTO cancel_requests (oid, time_stamp, msgseq_num, origclord_id, msg_content) VALUES (orderID, _timeStamp, _seqNum, _orderID, _msgContents);
    
  UPDATE new_orders SET status = _status where oid = orderID;
  
  RETURN 0;
END
$$;


ALTER FUNCTION public.insertcancelrequest4nasdaq(_orderid integer, _date date, _timestamp time without time zone, _seqnum text, _msgcontents text, _status text) OWNER TO postgres;

--
-- Name: insertcancelrequest4nyse_ccg(integer, date, time without time zone, text, text, text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION insertcancelrequest4nyse_ccg(_orderid integer, _date date, _timestamp time without time zone, _seqnum text, _meorderid text, _msgcontents text, _status text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
  orderID integer;
BEGIN
  
  SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
  
  IF orderID IS NULL THEN
    RETURN -1;
  END IF;

  INSERT INTO cancel_requests (oid, time_stamp, msgseq_num, origclord_id, arcaex_order_id, msg_content) VALUES (orderID, _timeStamp, _seqNum, _orderID, _meorderid, _msgContents);
  
  UPDATE new_orders SET status = _status where oid = orderID;
  
  RETURN 0;
END
$$;


ALTER FUNCTION public.insertcancelrequest4nyse_ccg(_orderid integer, _date date, _timestamp time without time zone, _seqnum text, _meorderid text, _msgcontents text, _status text) OWNER TO postgres;

--
-- Name: insertexecutedorder4directedge(integer, date, integer, double precision, time without time zone, time without time zone, text, text, text, text, text, integer, double precision, integer, double precision); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION insertexecutedorder4directedge(_orderid integer, _date date, _shares integer, _price double precision, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _execid text, _msgcontents text, _liquidityindicator text, _status text, _leftshares integer, _avgprice double precision, _entryexitrefnum integer, _totalfee double precision) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
  orderID integer;
BEGIN
  
  SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
  
  IF orderID IS NULL THEN
    RETURN -1;
  END IF;
  
  INSERT INTO fills (oid, shares, price, time_stamp, processed_time_stamp, msgseq_num, origclord_id, exec_id, msg_content, liquidity, entry_exit_ref, total_fee) VALUES (orderID, _shares, _price, _timeStamp, _processedTimestamp, _seqNum, _orderID, _execID, _msgContents, _liquidityIndicator, _entryExitRefNum, _totalFee);

  UPDATE new_orders SET status = _status, left_shares = _leftShares, avg_price = _avgPrice WHERE oid = orderID;
  UPDATE new_orders SET price = _price WHERE oid = orderID AND price = 0;
  
  RETURN 0;
END
$$;


ALTER FUNCTION public.insertexecutedorder4directedge(_orderid integer, _date date, _shares integer, _price double precision, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _execid text, _msgcontents text, _liquidityindicator text, _status text, _leftshares integer, _avgprice double precision, _entryexitrefnum integer, _totalfee double precision) OWNER TO postgres;

--
-- Name: insertexecutedorder4variant1(integer, date, integer, double precision, time without time zone, time without time zone, text, text, text, character, text, integer, double precision, integer, double precision); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION insertexecutedorder4variant1(_orderid integer, _date date, _shares integer, _price double precision, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _execid text, _msgcontents text, _liquidityindicator character, _status text, _leftshares integer, _avgprice double precision, _entryexitrefnum integer, _totalfee double precision) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
  orderID integer;
BEGIN
  
  SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
  
  IF orderID IS NULL THEN
    RETURN -1;
  END IF;
  
  INSERT INTO fills (oid, shares, price, time_stamp, processed_time_stamp, msgseq_num, origclord_id, exec_id, msg_content, liquidity, entry_exit_ref, total_fee) VALUES (orderID, _shares, _price, _timeStamp, _processedTimestamp, _seqNum, _orderID, _execID, _msgContents, _liquidityIndicator, _entryExitRefNum, _totalFee);

  UPDATE new_orders SET status = _status, left_shares = _leftShares, avg_price = _avgPrice WHERE oid = orderID;
  
  RETURN 0;
END
$$;


ALTER FUNCTION public.insertexecutedorder4variant1(_orderid integer, _date date, _shares integer, _price double precision, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _execid text, _msgcontents text, _liquidityindicator character, _status text, _leftshares integer, _avgprice double precision, _entryexitrefnum integer, _totalfee double precision) OWNER TO postgres;

--
-- Name: insertexecutedorder4variant2(integer, date, integer, double precision, time without time zone, time without time zone, text, text, text, character, text, integer, double precision, integer, double precision); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION insertexecutedorder4variant2(_orderid integer, _date date, _shares integer, _price double precision, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _execid text, _msgcontents text, _liquidityindicator character, _status text, _leftshares integer, _avgprice double precision, _entryexitrefnum integer, _totalfee double precision) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
  orderID integer;
BEGIN
  
  SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
  
  IF orderID IS NULL THEN
    RETURN -1;
  END IF;
  
  INSERT INTO fills (oid, shares, price, time_stamp, processed_time_stamp, msgseq_num, origclord_id, exec_id, msg_content, liquidity, entry_exit_ref, total_fee) VALUES (orderID, _shares, _price, _timeStamp, _processedTimestamp, _seqNum, _orderID, _execID, _msgContents, _liquidityIndicator, _entryExitRefNum, _totalFee);

  UPDATE new_orders SET status = _status, left_shares = _leftShares, avg_price = _avgPrice WHERE oid = orderID;
  UPDATE new_orders SET price = _price WHERE oid = orderID AND price = 0;
  
  RETURN 0;
END
$$;


ALTER FUNCTION public.insertexecutedorder4variant2(_orderid integer, _date date, _shares integer, _price double precision, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _execid text, _msgcontents text, _liquidityindicator character, _status text, _leftshares integer, _avgprice double precision, _entryexitrefnum integer, _totalfee double precision) OWNER TO postgres;

--
-- Name: insertexecutedorder4variant3(integer, date, integer, double precision, time without time zone, time without time zone, text, text, text, text, character, text, integer, double precision, integer, double precision); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION insertexecutedorder4variant3(_orderid integer, _date date, _shares integer, _price double precision, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _arcaexorderid text, _execid text, _msgcontents text, _liquidityindicator character, _status text, _leftshares integer, _avgprice double precision, _entryexitrefnum integer, _totalfee double precision) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
  orderID integer;
BEGIN
  
  SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
  
  IF orderID IS NULL THEN
    RETURN -1;
  END IF;
  
  INSERT INTO fills (oid, shares, price, time_stamp, processed_time_stamp, msgseq_num, origclord_id, arcaex_order_id, exec_id, msg_content, liquidity, entry_exit_ref, total_fee) VALUES (orderID, _shares, _price, _timeStamp, _processedTimestamp, _seqNum, _orderID, _arcaExOrderID, _execID, _msgContents, _liquidityIndicator, _entryExitRefNum, _totalFee);

  UPDATE new_orders SET status = _status, left_shares = _leftShares, avg_price = _avgPrice WHERE oid = orderID;
  
  RETURN 0;
END
$$;


ALTER FUNCTION public.insertexecutedorder4variant3(_orderid integer, _date date, _shares integer, _price double precision, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _arcaexorderid text, _execid text, _msgcontents text, _liquidityindicator character, _status text, _leftshares integer, _avgprice double precision, _entryexitrefnum integer, _totalfee double precision) OWNER TO postgres;

--
-- Name: insertfillorder4nyse_ccg(integer, date, integer, double precision, time without time zone, time without time zone, text, text, text, text, character, text, integer, double precision, integer, double precision); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION insertfillorder4nyse_ccg(_orderid integer, _date date, _shares integer, _price double precision, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _meorderid text, _execid text, _msgcontents text, _billingindicator character, _status text, _leftshares integer, _avgprice double precision, _entryexitrefnum integer, _totalfee double precision) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
  orderID integer;
BEGIN
  
  SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
  
  IF orderID IS NULL THEN
    RETURN -1;
  END IF;
  
  INSERT INTO fills (oid, shares, price, time_stamp, processed_time_stamp, msgseq_num, origclord_id, arcaex_order_id, exec_id, msg_content, liquidity, entry_exit_ref, total_fee) VALUES (orderID, _shares, _price, _timestamp, _processedTimestamp, _seqnum, _orderID, _meorderid, _execid, _msgcontents, _billingindicator, _entryExitRefNum, _totalFee);

  UPDATE new_orders SET status = _status, left_shares = _leftshares, avg_price = _avgprice WHERE oid = orderID;
  
  RETURN 0;
END$$;


ALTER FUNCTION public.insertfillorder4nyse_ccg(_orderid integer, _date date, _shares integer, _price double precision, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _meorderid text, _execid text, _msgcontents text, _billingindicator character, _status text, _leftshares integer, _avgprice double precision, _entryexitrefnum integer, _totalfee double precision) OWNER TO postgres;

--
-- Name: insertmanualcanceledorder4nyse_ccg(integer, date, time without time zone, time without time zone, text, text, text, text, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION insertmanualcanceledorder4nyse_ccg(_orderid integer, _date date, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _meorderid text, _msgcontents text, _status text, _entryexitrefnum integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
  orderID integer;
BEGIN
  
  SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderid and date = _date;
  
  IF orderID IS NULL THEN
    RETURN -1;
  END IF;

  INSERT INTO cancels (oid, time_stamp, processed_time_stamp, msgseq_num, origclord_id, arcaex_order_id, msg_content, entry_exit_ref) VALUES (orderID, _timeStamp, _processedtimestamp, _seqNum, _orderid, _meorderid, _msgContents, _entryExitRefNum);
    
  UPDATE new_orders SET status = _status where oid = orderID;
  
  RETURN 0;
END$$;


ALTER FUNCTION public.insertmanualcanceledorder4nyse_ccg(_orderid integer, _date date, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _meorderid text, _msgcontents text, _status text, _entryexitrefnum integer) OWNER TO postgres;

--
-- Name: insertmanualcanceledordervariant2(integer, date, time without time zone, time without time zone, text, text, text, text, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION insertmanualcanceledordervariant2(_orderid integer, _date date, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _arcaex_orderid text, _msgcontents text, _status text, _entryexitrefnum integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
  orderID integer;
BEGIN
  
  SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
  
  IF orderID IS NULL THEN
    RETURN -1;
  END IF;

  INSERT INTO cancels (oid, time_stamp, processed_time_stamp, msgseq_num, origclord_id, arcaex_order_id, msg_content, entry_exit_ref) VALUES (orderID, _timeStamp, _processedtimestamp, _seqNum, _orderID, _arcaex_orderid, _msgContents, _entryExitRefNum);
    
  UPDATE new_orders SET status = _status where oid = orderID;
  
  RETURN 0;
END
$$;


ALTER FUNCTION public.insertmanualcanceledordervariant2(_orderid integer, _date date, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _arcaex_orderid text, _msgcontents text, _status text, _entryexitrefnum integer) OWNER TO postgres;

--
-- Name: insertorderack4arca_direct(integer, date, time without time zone, time without time zone, text, text, text, double precision, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION insertorderack4arca_direct(_orderid integer, _date date, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _arcaexorderid text, _msgcontents text, _price double precision, _status text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
  orderID integer;
BEGIN
  
  SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
  
  IF orderID IS NULL THEN
    RETURN -1;
  END IF;
  
  INSERT INTO order_acks (oid, time_stamp, processed_time_stamp, msgseq_num, origclord_id, arcaex_order_id, msg_content, price) VALUES (orderID, _timeStamp, _processedTimestamp, _seqNum, _orderID, _arcaExOrderId, _msgContents, _price);
    
  UPDATE new_orders SET status = _status WHERE oid = orderID;
  
  RETURN 0;
END
$$;


ALTER FUNCTION public.insertorderack4arca_direct(_orderid integer, _date date, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _arcaexorderid text, _msgcontents text, _price double precision, _status text) OWNER TO postgres;

--
-- Name: insertorderack4nyse_ccg(integer, date, time without time zone, time without time zone, text, text, text, double precision, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION insertorderack4nyse_ccg(_orderid integer, _date date, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _meorderid text, _msgcontents text, _price double precision, _status text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE 
  orderID integer;
BEGIN
  SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;

  IF orderID IS NULL THEN
    RETURN -1;
  END IF;

  INSERT INTO order_acks (oid, time_stamp, processed_time_stamp, msgseq_num, origclord_id, arcaex_order_id, msg_content, price) VALUES (orderID, _timeStamp, _processedTimestamp, _seqNum, _orderID, _meorderid, _msgcontents, _price);
  
  UPDATE new_orders SET status = _status WHERE oid = orderID;

  RETURN 0;
END$$;


ALTER FUNCTION public.insertorderack4nyse_ccg(_orderid integer, _date date, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _meorderid text, _msgcontents text, _price double precision, _status text) OWNER TO postgres;

--
-- Name: insertreject4arca_direct(integer, date, character, time without time zone, time without time zone, text, text, text, text, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION insertreject4arca_direct(_orderid integer, _date date, _type character, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _text text, _msgcontents text, _status text, _entryexitrefnum integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
  orderID integer;
BEGIN
  
  SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
  
  IF orderID IS NULL THEN
    RETURN -1;
  END IF;
  
  IF (_type = '1') THEN
  
    INSERT INTO order_rejects (oid, time_stamp, processed_time_stamp, msgseq_num, origclord_id, reason, msg_content, entry_exit_ref) VALUES (orderID, _timeStamp, _processedtimestamp, _seqNum, _orderID, _text, _msgContents, _entryExitRefNum);
    
    UPDATE new_orders SET status = _status where oid = orderID;
    
    RETURN 0;
  ELSIF (_type = '2') THEN 
        
    INSERT INTO cancel_rejects (oid, time_stamp, msgseq_num, origclord_id, reason, msg_content) VALUES (orderID, _timeStamp, _seqNum, _orderID, _text, _msgContents);
  
    UPDATE new_orders SET status = _status where oid = orderID;
    RETURN 0;
  ELSE
    RETURN 0;
  END IF;
END
$$;


ALTER FUNCTION public.insertreject4arca_direct(_orderid integer, _date date, _type character, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _text text, _msgcontents text, _status text, _entryexitrefnum integer) OWNER TO postgres;

--
-- Name: insertreject4nyse_ccg(integer, date, character, time without time zone, time without time zone, text, text, text, text, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION insertreject4nyse_ccg(_orderid integer, _date date, _type character, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _text text, _msgcontents text, _status text, _entryexitrefnum integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
  orderID integer;
BEGIN
  
  SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
  
  IF orderID IS NULL THEN
    RETURN -1;
  END IF;
  
  IF (_type = '1') THEN
  
    INSERT INTO order_rejects (oid, time_stamp, processed_time_stamp, msgseq_num, origclord_id, reason, msg_content, entry_exit_ref) VALUES (orderID, _timeStamp, _processedtimestamp, _seqNum, _orderID, _text, _msgContents, _entryExitRefNum);
    
    UPDATE new_orders SET status = _status where oid = orderID;
    
    RETURN 0;
  ELSIF (_type = '2') THEN 
        
    INSERT INTO cancel_rejects (oid, time_stamp, msgseq_num, origclord_id, reason, msg_content) VALUES (orderID, _timeStamp, _seqNum, _orderID, _text, _msgContents);
  
    UPDATE new_orders SET status = _status where oid = orderID;
    RETURN 0;
  ELSE
    RETURN 0;
  END IF;
END$$;


ALTER FUNCTION public.insertreject4nyse_ccg(_orderid integer, _date date, _type character, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _text text, _msgcontents text, _status text, _entryexitrefnum integer) OWNER TO postgres;

--
-- Name: insertrejectedcancel4arca_bats(integer, date, time without time zone, text, text, text, text, text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION insertrejectedcancel4arca_bats(_orderid integer, _date date, _timestamp time without time zone, _seqnum text, _arcaexorderid text, _reason text, _cancelreject text, _msgcontents text, _status text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
  orderID integer;
BEGIN
  
  SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
  
  IF orderID IS NULL THEN
    RETURN -1;
  END IF;

  INSERT INTO cancel_rejects (oid, time_stamp, msgseq_num, origclord_id, arcaex_order_id, reason, cancel_reject, msg_content) VALUES (orderID, _timeStamp, _seqNum, _orderID, _arcaExOrderId, _reason, _cancelReject, _msgContents);
  
  UPDATE new_orders SET status = _status where oid = orderID;
  
  RETURN 0;
END
$$;


ALTER FUNCTION public.insertrejectedcancel4arca_bats(_orderid integer, _date date, _timestamp time without time zone, _seqnum text, _arcaexorderid text, _reason text, _cancelreject text, _msgcontents text, _status text) OWNER TO postgres;

--
-- Name: insertrejectedorder4batsz(integer, date, time without time zone, time without time zone, text, text, text, text, text, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION insertrejectedorder4batsz(_orderid integer, _date date, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _reason text, _text text, _msgcontents text, _status text, _entryexitrefnum integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
  orderID integer;
BEGIN
  SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
  
  IF orderID IS NULL THEN
    RETURN -1;
  END IF;
  
  INSERT INTO order_rejects (oid, time_stamp, processed_time_stamp, msgseq_num, origclord_id, reason, text, msg_content, entry_exit_ref) VALUES (orderID, _timeStamp, _processedtimestamp, _seqNum, _orderID, _reason, _text, _msgContents, _entryExitRefNum);

  UPDATE new_orders SET status = _status where oid = orderID;
  
  RETURN 0;
END
$$;


ALTER FUNCTION public.insertrejectedorder4batsz(_orderid integer, _date date, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _reason text, _text text, _msgcontents text, _status text, _entryexitrefnum integer) OWNER TO postgres;

--
-- Name: insertrejectedorder4nasdaq(integer, date, time without time zone, time without time zone, text, text, text, text, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION insertrejectedorder4nasdaq(_orderid integer, _date date, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _reason text, _msgcontents text, _status text, _entryexitrefnum integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
  orderID integer;
BEGIN
  
  SELECT oid INTO orderID FROM new_orders WHERE order_id = _orderID and date = _date;
  
  IF orderID IS NULL THEN
    RETURN -1;
  END IF;

  INSERT INTO order_rejects (oid, time_stamp, processed_time_stamp, msgseq_num, origclord_id, reason, msg_content, entry_exit_ref) VALUES (orderID, _timeStamp, _processedtimestamp, _seqNum, _orderID, _reason, _msgContents, _entryExitRefNum);
  
  UPDATE new_orders SET status = _status where oid = orderID;
  
  RETURN 0;
END
$$;


ALTER FUNCTION public.insertrejectedorder4nasdaq(_orderid integer, _date date, _timestamp time without time zone, _processedtimestamp time without time zone, _seqnum text, _reason text, _msgcontents text, _status text, _entryexitrefnum integer) OWNER TO postgres;

--
-- Name: modify_as_cid_in_trade_results(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION modify_as_cid_in_trade_results() RETURNS integer
    LANGUAGE plpgsql
    AS $$ 
declare
  retVal int;
  tmp_as_cid int;
  tmp_date date;
  tmp_record record;
begin
  retVal := 0;

  for tmp_date in (select distinct date from trade_results) loop
    for tmp_record in (select ts_id, ts_cid from trade_results where date = tmp_date) loop
      retVal := retVal + 1;

      select max(as_cid) into tmp_as_cid from raw_crosses where (date = tmp_date and ts_id = tmp_record.ts_id and ts_cid = tmp_record.ts_cid);

      update trade_results set as_cid = tmp_as_cid where (date = tmp_date and ts_id = tmp_record.ts_id and ts_cid = tmp_record.ts_cid);
    end loop;
  end loop;

  return retVal;
end
$$;


ALTER FUNCTION public.modify_as_cid_in_trade_results() OWNER TO postgres;

--
-- Name: percentile_cont(real[], real); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION percentile_cont(myarray real[], percentile real) RETURNS real
    LANGUAGE plpgsql IMMUTABLE
    AS $$

DECLARE
  ary_cnt INTEGER;
  row_num real;
  crn real;
  frn real;
  calc_result real;
  new_array real[];
BEGIN
  ary_cnt = array_length(myarray,1);
  row_num = 1 + ( percentile * ( ary_cnt - 1 ));
  new_array = array_sort(myarray);

  crn = ceiling(row_num);
  frn = floor(row_num);

  if crn = frn and frn = row_num then
    calc_result = new_array[row_num];
  else
    calc_result = (crn - row_num) * new_array[frn] 
            + (row_num - frn) * new_array[crn];
  end if;

  RETURN calc_result;
END;
$$;


ALTER FUNCTION public.percentile_cont(myarray real[], percentile real) OWNER TO postgres;

--
-- Name: process_fills_insert(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION process_fills_insert() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  symbolVal text;
  leave_Shares  integer;
  avgPrice  double precision;
  totalQty  integer;
  netPL   double precision;
  totalMktValue double precision;
  rowcount  integer;
  dateVal   date;
  sideVal   text;
  accountVal  integer;
  tsIdVal   integer;
  crossIdVal  integer;
  ecnVal    text;
  liquidity character;
  totalFee  double precision;
BEGIN
  -- Get symbol name
  SELECT trim(symbol), date, substring(side from 1 for 1) as side, account, trim(ecn), ts_id, cross_id into symbolVal, dateVal, sideVal, accountVal, ecnVal, tsIdVal, crossIdVal
  FROM new_orders WHERE oid = NEW.oid;

  -- Check symbol existent?
  SELECT COUNT(*) into rowcount
  FROM risk_managements
  WHERE trim(symbol) = trim(symbolVal) and date = dateVal and account = accountVal;
  
  UPDATE new_orders 
    SET total_fee = total_fee + NEW.total_fee 
  WHERE oid = NEW.oid;
  
  IF (tsIdVal <> -1) THEN
    SELECT SUM(total_fee) INTO totalFee
    FROM new_orders
    WHERE date = dateVal AND ts_id = tsIdVal AND cross_id = crossIdVal;
    
    UPDATE trade_results
      SET ts_total_fee = totalFee
    WHERE date = dateVal AND ts_id = tsIdVal AND ts_cid = crossIdVal;
  END IF;
  
  -- If new symbol
  IF (rowcount = 0) THEN
    IF (sideVal = 'B') THEN
      leave_Shares := NEW.shares;
    ELSE
      leave_Shares := -NEW.shares;
    END IF;
    
    totalMktValue := abs(leave_Shares) * NEW.price;
    
    -- insert into RM
    totalFee := NEW.total_fee;
        
    INSERT INTO risk_managements(symbol, average_price, net_quantity, matched_profit_loss, total_market_value, total_quantity, date, last_update, account, total_fee) 
      VALUES (symbolVal , NEW.price, leave_Shares, 0.00, totalMktValue, NEW.shares, dateVal, NEW.time_stamp, accountVal, totalFee);
  ELSE -- Old symbol
    SELECT average_price, net_quantity, matched_profit_loss, total_market_value, total_quantity, total_fee INTO avgPrice, leave_Shares, netPL, totalMktValue, totalQty, totalFee
    FROM risk_managements
    WHERE trim(symbol) = symbolVal AND date = dateVal AND account = accountVal;
        
    IF (leave_Shares = 0) THEN
      avgPrice := NEW.price;
      IF (sideVal = 'B') THEN
        leave_Shares := NEW.shares;
      ELSE -- Action is Sell or Short Sell
        leave_Shares := -NEW.shares;
      END IF;
    ELSIF (leave_Shares > 0) THEN
      IF (sideVal = 'B') THEN
        avgPrice := (avgPrice * leave_Shares + NEW.price * NEW.shares) / (NEW.shares + leave_Shares);
        leave_Shares := leave_Shares + NEW.shares;
      ELSE
        netPL := netPL + ((NEW.price - avgPrice) * LEAST(NEW.shares, leave_Shares));        
        leave_Shares := leave_Shares - NEW.shares;
        IF (leave_Shares = 0) THEN
          avgPrice := 0.00;
        ELSIF (leave_Shares < 0) THEN
          avgPrice := NEW.price;
        END IF;
      END IF;
    ELSE 
      IF (sideVal = 'B') THEN
        netPL := netPL + ((avgPrice - NEW.price)*LEAST(NEW.shares, abs(leave_Shares)));       
        leave_Shares := leave_Shares + NEW.shares;
        IF (leave_Shares = 0) THEN
          avgPrice := 0.00;
        ELSIF (leave_Shares > 0) THEN
          avgPrice := NEW.price;
        END IF;
      ELSE
        avgPrice := (avgPrice*(-leave_Shares) + NEW.price * NEW.shares) / (NEW.shares + (-leave_Shares));
        leave_Shares := leave_Shares - NEW.shares;  
      END IF;
    END IF;
    
    totalMktValue := avgPrice * abs(leave_Shares);
    totalQty := totalQty + NEW.shares;    
    totalFee := totalFee + NEW.total_fee;

    -- update RM
    UPDATE risk_managements
    SET average_price = avgPrice,
      net_quantity = leave_Shares, 
      matched_profit_loss = netPL, 
      total_market_value = totalMktValue, 
      total_quantity = totalQty, 
      last_update = NEW.time_stamp,
      total_fee = totalFee
    WHERE symbol = symbolVal and date = dateVal and account = accountVal;
  END IF;
  
  RETURN NEW;
END $$;


ALTER FUNCTION public.process_fills_insert() OWNER TO postgres;

--
-- Name: process_fills_simultaneous(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION process_fills_simultaneous() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  DECLARE
  symbolVal text;
        leave_Shares  integer;
  order_Shares  integer;
  order_Price float;
  avgPrice  float;
  totalQty  float;
  netPL   float;
  totalMktValue float;
  rowcount  integer;
  dateVal   date;
  timeVal   time;
  sideVal   text;
  tradeType integer;
  accountVal  integer;
  BEGIN
  
  -- Get symbol name
  SELECT trim(symbol), date, substring(side from 1 for 1) as side, trade_type, account into symbolVal, dateVal, sideVal, tradeType, accountVal 
  FROM new_orders where oid = NEW.oid;

  -- Check simultaneous trade
  IF (tradeType = 1) THEN 
    timeVal := NEW.time_stamp;

    -- Check symbol existent?
    SELECT COUNT(*) into rowcount
    FROM rm_simultaneous_trades
    WHERE trim(symbol) = trim(symbolVal) and date = dateVal and account = accountVal;

    -- If new symbol
    IF (rowcount = 0) THEN
      IF (sideVal = 'B') THEN
        leave_Shares := NEW.shares;
      ELSE
        leave_Shares := - NEW.shares;
      END IF;
      totalMktValue := abs(leave_Shares) * NEW.price;

      -- insert into RM
      INSERT INTO rm_simultaneous_trades(symbol, average_price, net_quantity, total_market_value,matched_profit_loss, total_quantity, date, last_update, account) 
        VALUES (symbolVal , NEW.price, leave_Shares, totalMktValue,0, NEW.shares, dateVal, timeVal, accountVal);
    ELSE -- Old symbol
      SELECT average_price, net_quantity, matched_profit_loss, total_market_value, total_quantity INTO avgPrice ,leave_Shares, netPL, totalMktValue, totalQty
      FROM rm_simultaneous_trades
      WHERE trim(symbol) = (symbolVal) and date = dateVal and account = accountVal;

      IF (leave_Shares > 0) THEN -- Equivalent to LONG
        IF (sideVal = 'B') THEN -- BUY
          avgPrice := (avgPrice * leave_Shares + NEW.price * NEW.shares) / (NEW.shares + leave_Shares);
          leave_Shares := leave_Shares + NEW.shares;
        ELSE -- SELL
          netPL := netPL + ((NEW.price - avgPrice) * LEAST(NEW.shares, leave_Shares));        
          leave_Shares := leave_Shares - NEW.shares;
          IF (leave_Shares < 0) THEN
            avgPrice := NEW.price;
          ELSIF (leave_Shares = 0) THEN
            avgPrice := 0;
          END IF;
        END IF;
      ELSIF (leave_Shares < 0) THEN -- Equivalent to SHORT
        IF (sideVal = 'B') THEN -- BUY
          netPL := netPL + ((avgPrice - NEW.price)*LEAST(NEW.shares, abs(leave_Shares)));       
          leave_Shares := leave_Shares + NEW.shares;
          IF (leave_Shares = 0) THEN
            avgPrice := 0;
          ELSIF (leave_Shares > 0) THEN
            avgPrice := NEW.price;
          END IF;
        ELSE -- SELL
          avgPrice := (avgPrice*(-leave_Shares) + NEW.price * NEW.shares) / (NEW.shares + (-leave_Shares));
          leave_Shares := leave_Shares - NEW.shares;        
        END IF;
      ELSE -- leave_Shares = 0
        avgPrice := NEW.price;
        IF (sideVal = 'B') THEN
          leave_Shares := NEW.shares;
        ELSE -- Action is Sell or Short Sell
          leave_Shares := -NEW.shares;
        END IF;
      END IF;
      totalMktValue := avgPrice * abs(leave_Shares);
      totalQty := totalQty + NEW.shares;
    
      -- update RM
      UPDATE rm_simultaneous_trades
      SET   average_price = avgPrice,
        net_quantity = leave_Shares, 
        matched_profit_loss = netPL, 
        total_market_value = totalMktValue, 
        total_quantity = totalQty, 
        last_update = timeVal
      WHERE symbol = symbolVal and date = dateVal and account = accountVal;
    END IF;
  END IF;

  RETURN NEW;

  END;
$$;


ALTER FUNCTION public.process_fills_simultaneous() OWNER TO postgres;

--
-- Name: process_rm_insert(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION process_rm_insert() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  
BEGIN 
  INSERT INTO risk_managements_twt2(symbol, matched_profit_loss, total_market_value, total_quantity, date, last_update)
  VALUES (NEW.symbol, NEW.matched_profit_loss, NEW.total_market_value, NEW.total_quantity, NEW.date, NEW.last_update);
  
  RETURN NEW;
END
$$;


ALTER FUNCTION public.process_rm_insert() OWNER TO postgres;

--
-- Name: process_rm_update(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION process_rm_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  i_total_quantity  integer;
  i_total_market_value  integer;
  f_profit_loss   float;
  
BEGIN 
  SELECT total_quantity, total_market_value, matched_profit_loss
    INTO i_total_quantity, i_total_market_value, f_profit_loss
  FROM risk_managements
  WHERE date = NEW.date and symbol = NEW.symbol;

  INSERT INTO risk_managements_twt2(symbol, matched_profit_loss, total_market_value, total_quantity, date, last_update)
  VALUES (NEW.symbol, NEW.matched_profit_loss - f_profit_loss, NEW.total_market_value - i_total_market_value, NEW.total_quantity - i_total_quantity, NEW.date, NEW.last_update);
  RETURN NEW;
END
$$;


ALTER FUNCTION public.process_rm_update() OWNER TO postgres;

--
-- Name: update_rm(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION update_rm() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  symbolVal   text;
  leave_Shares  integer;
  order_Shares  integer;
  order_Price   double precision;
  avgPrice    double precision;
  totalQty    double precision;
  netPL     double precision;
  totalMktValue double precision;
  rowcount    integer;
  dateVal     date;
  timeVal     time;
  sideVal     text;
  midVal      int;
  current_shares  int;
  current_price double precision;
  fill      record;
  fill_idVal    int;
  sideBeginDay  character(1);
  sharesBeginDay  integer;
  priceBeginDay   double precision;
  accountVal    integer;
  ecnVal      text;
  liquidityVal  character;
  totalFee    double precision;
  temp_totalFee   double precision;
BEGIN
  -- Get symbol name
  if TG_OP <> 'DELETE' then
    fill_idVal := NEW.fill_id;
    --Check to see if insert/update the CEE note, we will return 
    if NEW.is_cee IS TRUE then
      return NEW;
    end if;
  else
    fill_idVal := OLD.fill_id;
    --Check to see if insert/update the CEE note, we will return 
    if OLD.is_cee IS TRUE then
      return OLD;
    end if;
  end if;

  SELECT trim(new_orders.symbol), new_orders.date, fills.time_stamp, new_orders.side, account, new_orders.ecn, fills.liquidity into symbolVal, dateVal, timeVal, sideVal, accountVal, ecnVal, liquidityVal
  FROM new_orders
  JOIN fills ON new_orders.oid = fills.oid
  WHERE fills.fill_id = fill_idVal;

  SELECT side, shares, price into sideBeginDay, sharesBeginDay, priceBeginDay
  FROM open_positions 
  WHERE trim(symbol::text) = trim(symbolVal) AND date = dateVal AND account = accountVal;

  if (sharesBeginDay is null) then 
    leave_Shares := 0;
    avgPrice := 0;
    netPL := 0;
    totalMktValue := 0;
    totalQty := 0;
    totalFee := 0.00;
  else
    IF (sideBeginDay = 'S') THEN
      leave_Shares := -1 * sharesBeginDay;
    ELSE
      leave_Shares := sharesBeginDay;
    END IF; 

    avgPrice := priceBeginDay;
    netPL := 0;
    totalMktValue := sharesBeginDay * priceBeginDay;
    totalQty := sharesBeginDay;
    totalFee := 0.00;

  end if; 
  
  --Travel thought fill today and 
  for fill in (select f1.shares, f1.price, f1.fill_id, substring(o.side from 1 for 1) as side, o.ecn, f1.liquidity as liquidity 
          from fills f1 
          join new_orders o on f1.oid = o.oid 
          where o.date = dateVal and trim(o.symbol) = trim(symbolVal) and o.account = accountVal ) loop
        
    select new_shares, new_price into current_shares, current_price from brokens where fill_id = fill.fill_id;
    
    if current_price is null then
      current_shares  := fill.shares;
      current_price   := fill.price;
    end if;

    IF (leave_Shares > 0) THEN -- Equivalent to LONG
      IF (fill.side = 'B') THEN -- BUY
        avgPrice := (avgPrice * leave_Shares + current_price * current_shares) / (current_shares + leave_Shares);
        leave_Shares := leave_Shares + current_shares;
      ELSE -- SELL
        netPL := netPL + ((current_price - avgPrice) * LEAST(current_shares, leave_Shares));        
        leave_Shares := leave_Shares - current_shares;
        IF (leave_Shares < 0) THEN
          avgPrice := current_price;
        ELSIF (leave_Shares = 0) THEN
          avgPrice := 0;
        END IF;
      END IF;
    ELSIF (leave_Shares < 0) THEN -- Equivalent to SHORT
      IF (fill.side = 'B') THEN -- BUY
        netPL := netPL + ((avgPrice - current_price)*LEAST(current_shares, abs(leave_Shares)));       
        leave_Shares := leave_Shares + current_shares;
        IF (leave_Shares = 0) THEN
          avgPrice := 0;
        ELSIF (leave_Shares > 0) THEN
          avgPrice := current_price;
        END IF;
      ELSE -- SELL
        avgPrice := (avgPrice*(-leave_Shares) + current_price * current_shares) / (current_shares + (-leave_Shares));
        leave_Shares := leave_Shares - current_shares;        
      END IF;
    ELSE -- leave_Shares = 0
      avgPrice := current_price;
      IF (fill.side = 'B') THEN
        leave_Shares := current_shares;
      ELSE -- Action is Sell or Short Sell
        leave_Shares := -current_shares;
      END IF;
    END IF;
  
    totalMktValue := avgPrice * abs(leave_Shares);
    totalQty := totalQty + current_shares;  
    
    if (current_shares > 0 AND fill.liquidity IS NOT NULL) then
      select calculate_total_fee(current_price, current_shares, fill.side, fill.liquidity, fill.ecn) into temp_totalFee;
      
      totalFee := totalFee + temp_totalFee;
    end if;
  end loop;
      
  -- update RM
  UPDATE risk_managements
  SET average_price = avgPrice,
    net_quantity = leave_Shares, 
    matched_profit_loss = netPL, 
    total_market_value = totalMktValue, 
    total_quantity = totalQty, 
    last_update = timeVal,
    total_fee = totalFee
  WHERE trim(symbol) = trim(symbolVal) and date = dateVal and account = accountVal;
  
  RETURN NEW;
END $$;


ALTER FUNCTION public.update_rm() OWNER TO postgres;

--
-- Name: update_rm_st(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION update_rm_st() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  DECLARE
  symbolVal text;
        leave_Shares  integer;
  order_Shares  integer;
  order_Price float;
  avgPrice  float;
  totalQty  float;
  netPL   float;
  totalMktValue float;
  rowcount  integer;
  dateVal   date;
  timeVal   time;
  sideVal   text;
  midVal    int;
  current_shares  int;
  current_price float;
  fill    record;
  fill_idVal int;
  tradeType integer;
  accountVal  integer;
  BEGIN
  -- Get symbol name
  if TG_OP = 'INSERT' then
    fill_idVal := NEW.fill_id;
  else
    fill_idVal := OLD.fill_id;
  end if;

  SELECT trim(new_orders.symbol), new_orders.date, fills.time_stamp, new_orders.side, new_orders.trade_type, account into symbolVal, dateVal, timeVal, sideVal, tradeType, accountVal
  FROM new_orders
  JOIN fills ON new_orders.oid = fills.oid
  WHERE fills.fill_id = fill_idVal;

  -- Check simultaneous trade
  IF (tradeType = 1) THEN
    leave_Shares := 0;
    avgPrice := 0;
    netPL := 0;
    totalMktValue := 0;
    totalQty := 0;  
    
    --Travel thought fill today and 
    for fill in ((select f1.shares, f1.price, f1.fill_id, substring(o.side from 1 for 1) as side from fills f1 join new_orders o on f1.oid = o.oid where o.date = dateVal and trim(o.symbol) = trim(symbolVal) and o.trade_type = 1 and o.account = accountVal) 
      union all (select cp.shares, cp.price, -1 as "fill_id",substring(o2.side from 1 for 1) as side  from cancel_pendings cp join new_orders o2 on cp.oid = o2.oid where o2.date = dateVal and trim(o2.symbol) = trim(symbolVal) and o2.trade_type = 1 and o2.account = accountVal)) loop
          
      select new_shares, new_price into current_shares, current_price from brokens where fill_id = fill.fill_id;
      
      if current_price is null then
        current_shares  := fill.shares;
        current_price   := fill.price;
      end if;

      IF (leave_Shares > 0) THEN -- Equivalent to LONG
        IF (fill.side = 'B') THEN -- BUY
          avgPrice := (avgPrice * leave_Shares + current_price * current_shares) / (current_shares + leave_Shares);
          leave_Shares := leave_Shares + current_shares;
        ELSE -- SELL
          netPL := netPL + ((current_price - avgPrice) * LEAST(current_shares, leave_Shares));        
          leave_Shares := leave_Shares - current_shares;
          IF (leave_Shares < 0) THEN
            avgPrice := current_price;
          ELSIF (leave_Shares = 0) THEN
            avgPrice := 0;
          END IF;
        END IF;
      ELSIF (leave_Shares < 0) THEN -- Equivalent to SHORT
        IF (fill.side = 'B') THEN -- BUY
          netPL := netPL + ((avgPrice - current_price)*LEAST(current_shares, abs(leave_Shares)));       
          leave_Shares := leave_Shares + current_shares;
          IF (leave_Shares = 0) THEN
            avgPrice := 0;
          ELSIF (leave_Shares > 0) THEN
            avgPrice := current_price;
          END IF;
        ELSE -- SELL
          avgPrice := (avgPrice*(-leave_Shares) + current_price * current_shares) / (current_shares + (-leave_Shares));
          leave_Shares := leave_Shares - current_shares;        
        END IF;
      ELSE -- leave_Shares = 0
        avgPrice := current_price;
        IF (fill.side = 'B') THEN
          leave_Shares := current_shares;
        ELSE -- Action is Sell or Short Sell
          leave_Shares := -current_shares;
        END IF;
      END IF;
    
      totalMktValue := avgPrice * abs(leave_Shares);
      totalQty := totalQty + current_shares;  
    
    end loop;
      
    -- update RM
    UPDATE rm_simultaneous_trades
    SET   average_price = avgPrice,
      net_quantity = leave_Shares, 
      matched_profit_loss = netPL, 
      total_market_value = totalMktValue, 
      total_quantity = totalQty, 
      last_update = timeVal
    WHERE trim(symbol) = trim(symbolVal) and date = dateVal and account = accountVal;
  END IF;
  
  RETURN NEW;
  END;
$$;


ALTER FUNCTION public.update_rm_st() OWNER TO postgres;

--
-- Name: update_status_for_user_login_info(text, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION update_status_for_user_login_info(_username text, _status integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$ 
declare
numberTrader integer;
tmpRecord record;
currentStatus integer;
BEGIN
  IF (_status = 1) THEN
  
    numberTrader := 0;

    for tmpRecord in (SELECT username, status FROM user_login_info) loop
      numberTrader := numberTrader + 1;
      IF (tmpRecord.status = 1) THEN
        UPDATE user_login_info 
        SET status = 3
        WHERE lower(username) = lower(tmpRecord.username);

        UPDATE user_login_info 
        SET status = _status
        WHERE lower(username) = lower(_username);
      ELSE
        UPDATE user_login_info 
        SET status = _status
        WHERE lower(username) = lower(_username);
      END IF;
    end loop;
    
  ELSIF (_status = 3 OR _status = 0) THEN
    currentStatus = 0;
    SELECT status INTO currentStatus FROM user_login_info WHERE lower(username) = lower(_username);

    IF (currentStatus = 1) THEN 
    
      numberTrader := 0;

      for tmpRecord in (SELECT username, status FROM user_login_info) loop
        numberTrader := numberTrader + 1;
        
        IF (tmpRecord.status = 2) THEN
          UPDATE user_login_info 
          SET status = 1
          WHERE lower(username) = lower(tmpRecord.username);
        
          UPDATE user_login_info 
          SET status = _status
          WHERE lower(username) = lower(_username);
        
          return 1;
        ELSE
          UPDATE user_login_info 
          SET status = _status
          WHERE lower(username) = lower(_username);

        END IF;
      end loop;
      return 2;
    ELSE
      UPDATE user_login_info 
      SET status = _status
      WHERE lower(username) = lower(_username);
    END IF;
  ELSE
    UPDATE user_login_info 
    SET status = _status
    WHERE lower(username) = lower(_username);
  END IF;

  return 0;
END
$$;


ALTER FUNCTION public.update_status_for_user_login_info(_username text, _status integer) OWNER TO postgres;

--
-- Name: update_time_stamp_of_raw_cross(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION update_time_stamp_of_raw_cross() RETURNS integer
    LANGUAGE plpgsql
    AS $$ 
DECLARE
  recordTmp record;
  retVal integer;
BEGIN  
  retVal := 0;
  
  FOR recordTmp IN (SELECT date, ts_id, ts_cid, max(split_part(raw_crosses.raw_content::text, ' '::text, 8)) AS quote_time_stamp
          FROM raw_crosses WHERE raw_crosses.raw_content ~~ '3%'::text 
          GROUP BY date, ts_id, ts_cid ORDER BY date, ts_id, ts_cid) LOOP
    retVal = retVal + 1;
    
    UPDATE raw_crosses
      SET raw_content = trim(raw_content::text) || ' ' || recordTmp.quote_time_stamp
    WHERE date=recordTmp.date AND ts_id=recordTmp.ts_id AND ts_cid=recordTmp.ts_cid AND raw_crosses.raw_content ~~ '2%'::text;
  END LOOP;

  RETURN retVal;
END
$$;


ALTER FUNCTION public.update_time_stamp_of_raw_cross() OWNER TO postgres;

--
-- Name: array_agg(anyelement); Type: AGGREGATE; Schema: public; Owner: postgres
--

CREATE AGGREGATE array_agg(anyelement) (
    SFUNC = array_append,
    STYPE = anyarray,
    INITCOND = '{}'
);


ALTER AGGREGATE public.array_agg(anyelement) OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: brokens; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE brokens (
    id integer NOT NULL,
    oid integer NOT NULL,
    new_shares integer,
    new_price double precision,
    broken_shares integer DEFAULT 0,
    trade_date date,
    exec_id text NOT NULL,
    fill_id integer NOT NULL,
    filing_status character(4),
    cee_note text,
    is_cee boolean,
    time_stamp time without time zone DEFAULT now()
);


ALTER TABLE public.brokens OWNER TO postgres;

--
-- Name: cancel_pendings; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cancel_pendings (
    id integer NOT NULL,
    oid integer NOT NULL,
    time_stamp time without time zone,
    msgseq_num character(12),
    origclord_id integer,
    arcaex_order_id text NOT NULL,
    exec_id text,
    shares integer,
    price double precision,
    exec_type character(4),
    text text,
    msg_content text
);


ALTER TABLE public.cancel_pendings OWNER TO postgres;

--
-- Name: fills; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE fills (
    id integer NOT NULL,
    oid integer NOT NULL,
    shares integer,
    price double precision,
    time_stamp time without time zone,
    msgseq_num text,
    sending_time text,
    origclord_id text,
    arcaex_order_id text,
    exec_id text NOT NULL,
    ord_status text,
    exec_type text,
    text text,
    msg_content text,
    fill_id integer NOT NULL,
    liquidity character(6),
    filing_status character(4),
    cee_note text,
    entry_exit_ref integer,
    manual_update integer DEFAULT 0,
    processed_time_stamp time without time zone,
    total_fee double precision DEFAULT 0.00
);


ALTER TABLE public.fills OWNER TO postgres;

--
-- Name: new_orders; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE new_orders (
    id integer NOT NULL,
    oid integer NOT NULL,
    tid integer,
    order_id integer,
    entry_exit character(6),
    ask_bid character(4),
    side character(2),
    time_stamp time without time zone,
    ecn character(4),
    shares integer,
    price double precision,
    time_in_force character(4),
    left_shares integer,
    status character(20),
    msgseq_num character(12),
    msg_content text,
    symbol character(8),
    avg_price double precision,
    date date,
    ts_id integer,
    stock_type character(8),
    ecn_launch character(4),
    cross_id integer,
    ecn_suffix character(4),
    trade_type integer,
    ack_price double precision,
    is_iso character(1),
    account integer,
    bbo_id integer,
    entry_exit_ref integer,
    routing_inst character(4),
    total_fee double precision DEFAULT 0.00,
    visible_shares integer DEFAULT 0
);


ALTER TABLE public.new_orders OWNER TO postgres;

--
-- Name: abcv; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW abcv AS
    SELECT new_orders.date, new_orders.tid, new_orders.oid, brokens.oid AS broken_id, btrim((new_orders.symbol)::text) AS btrim, new_orders.entry_exit, new_orders.side, new_orders.time_stamp, new_orders.ecn, new_orders.shares, new_orders.price, new_orders.time_in_force, new_orders.left_shares, new_orders.status, fills.shares AS filled_shares, fills.price AS filled_price, brokens.new_price AS changed_price, brokens.exec_id, fills.liquidity FROM ((new_orders JOIN fills ON ((new_orders.oid = fills.oid))) LEFT JOIN brokens ON (((fills.oid = brokens.oid) AND (fills.exec_id = brokens.exec_id)))) WHERE ((brokens.oid IS NULL) OR ((brokens.oid IS NOT NULL) AND (brokens.new_shares > 0))) UNION ALL SELECT new_orders.date, new_orders.tid, new_orders.oid, brokens.oid AS broken_id, btrim((new_orders.symbol)::text) AS btrim, new_orders.entry_exit, new_orders.side, new_orders.time_stamp, new_orders.ecn, new_orders.shares, new_orders.price, new_orders.time_in_force, new_orders.left_shares, new_orders.status, cancel_pendings.shares AS filled_shares, cancel_pendings.price AS filled_price, brokens.new_price AS changed_price, brokens.exec_id, 'R'::bpchar AS liquidity FROM ((new_orders JOIN cancel_pendings ON ((new_orders.oid = cancel_pendings.oid))) LEFT JOIN brokens ON (((cancel_pendings.oid = brokens.oid) AND (cancel_pendings.exec_id = brokens.exec_id)))) WHERE ((brokens.oid IS NULL) OR ((brokens.oid IS NOT NULL) AND (brokens.new_shares > 0)));


ALTER TABLE public.abcv OWNER TO postgres;

--
-- Name: as_settings; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE as_settings (
    id integer NOT NULL,
    mid integer NOT NULL,
    description text,
    book_arca integer,
    book_nasdaq integer,
    book_bats integer,
    arca_direct integer,
    nasdaq_ouch integer,
    bats_fix integer,
    nasdaq_rash integer,
    arca_fix integer,
    cts integer,
    utdf integer,
    cqs integer,
    uqdf integer
);


ALTER TABLE public.as_settings OWNER TO postgres;

--
-- Name: as_settings_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE as_settings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.as_settings_id_seq OWNER TO postgres;

--
-- Name: as_settings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE as_settings_id_seq OWNED BY as_settings.id;


--
-- Name: ask_bids; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ask_bids (
    id integer NOT NULL,
    ask_bid character(4),
    ecn_launch character(4),
    shares integer,
    price double precision,
    ts_id integer,
    ts_cid integer,
    date date
);


ALTER TABLE public.ask_bids OWNER TO postgres;

--
-- Name: ask_bids_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE ask_bids_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ask_bids_id_seq OWNER TO postgres;

--
-- Name: ask_bids_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE ask_bids_id_seq OWNED BY ask_bids.id;


--
-- Name: bbo_infos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE bbo_infos (
    id integer NOT NULL,
    date date,
    ts_id integer,
    cross_id integer,
    content text,
    bbo_id integer
);


ALTER TABLE public.bbo_infos OWNER TO postgres;

--
-- Name: bbo_infos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE bbo_infos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bbo_infos_id_seq OWNER TO postgres;

--
-- Name: bbo_infos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE bbo_infos_id_seq OWNED BY bbo_infos.id;


--
-- Name: broken_trades; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE broken_trades (
    id integer NOT NULL,
    oid integer NOT NULL,
    time_stamp time without time zone,
    msgseq_num text,
    sending_time text,
    origclord_id text,
    arcaex_order_id text,
    exec_id text NOT NULL,
    shares integer,
    price double precision,
    ord_status text,
    text text,
    msg_content text
);


ALTER TABLE public.broken_trades OWNER TO postgres;

--
-- Name: broken_trades_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE broken_trades_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.broken_trades_id_seq OWNER TO postgres;

--
-- Name: broken_trades_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE broken_trades_id_seq OWNED BY broken_trades.id;


--
-- Name: brokens_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE brokens_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.brokens_id_seq OWNER TO postgres;

--
-- Name: brokens_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE brokens_id_seq OWNED BY brokens.id;


--
-- Name: cancel_acks; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cancel_acks (
    id integer NOT NULL,
    oid integer NOT NULL,
    time_stamp time without time zone,
    msgseq_num character(12),
    origclord_id integer,
    arcaex_order_id text,
    msg_content text
);


ALTER TABLE public.cancel_acks OWNER TO postgres;

--
-- Name: cancel_acks_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cancel_acks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cancel_acks_id_seq OWNER TO postgres;

--
-- Name: cancel_acks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cancel_acks_id_seq OWNED BY cancel_acks.id;


--
-- Name: cancel_pendings_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cancel_pendings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cancel_pendings_id_seq OWNER TO postgres;

--
-- Name: cancel_pendings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cancel_pendings_id_seq OWNED BY cancel_pendings.id;


--
-- Name: cancel_rejects; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cancel_rejects (
    id integer NOT NULL,
    oid integer NOT NULL,
    time_stamp time without time zone,
    msgseq_num text,
    sending_time text,
    clord_id text,
    origclord_id text,
    arcaex_order_id text,
    exec_id text,
    ord_status text,
    reject_type text,
    reason text,
    text text,
    cancel_reject text,
    msg_content text
);


ALTER TABLE public.cancel_rejects OWNER TO postgres;

--
-- Name: cancel_rejects_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cancel_rejects_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cancel_rejects_id_seq OWNER TO postgres;

--
-- Name: cancel_rejects_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cancel_rejects_id_seq OWNED BY cancel_rejects.id;


--
-- Name: cancel_requests; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cancel_requests (
    id integer NOT NULL,
    oid integer NOT NULL,
    time_stamp time without time zone,
    msgseq_num text,
    sending_time text,
    origclord_id text,
    arcaex_order_id text,
    exec_id text,
    shares integer,
    msg_content text
);


ALTER TABLE public.cancel_requests OWNER TO postgres;

--
-- Name: cancel_requests_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cancel_requests_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cancel_requests_id_seq OWNER TO postgres;

--
-- Name: cancel_requests_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cancel_requests_id_seq OWNED BY cancel_requests.id;


--
-- Name: cancels; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cancels (
    id integer NOT NULL,
    oid integer NOT NULL,
    shares integer,
    time_stamp time without time zone,
    msgseq_num text,
    sending_time text,
    origclord_id text,
    arcaex_order_id text,
    exec_id text,
    ord_status text,
    text text,
    msg_content text,
    entry_exit_ref integer,
    manual_update integer DEFAULT 0,
    processed_time_stamp time without time zone
);


ALTER TABLE public.cancels OWNER TO postgres;

--
-- Name: cancels_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cancels_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cancels_id_seq OWNER TO postgres;

--
-- Name: cancels_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cancels_id_seq OWNED BY cancels.id;


--
-- Name: order_acks; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE order_acks (
    id integer NOT NULL,
    oid integer NOT NULL,
    time_stamp time without time zone,
    msgseq_num text,
    sending_time text,
    origclord_id text,
    arcaex_order_id text,
    exec_id text,
    ord_status text,
    msg_content text,
    price double precision,
    processed_time_stamp time without time zone
);


ALTER TABLE public.order_acks OWNER TO postgres;

--
-- Name: details; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW details AS
    SELECT new_orders.oid, new_orders.order_id, new_orders.date, btrim((new_orders.symbol)::text) AS symbol, new_orders.side, new_orders.price, new_orders.shares, new_orders.left_shares, new_orders.ecn, new_orders.time_in_force, order_acks.arcaex_order_id, order_acks.time_stamp AS order_ack_time_stamp, fills.time_stamp AS filled_time_stamp, fills.shares AS filled_shares, 1 AS type FROM ((new_orders JOIN order_acks ON ((new_orders.oid = order_acks.oid))) JOIN fills ON ((new_orders.oid = fills.oid))) WHERE (new_orders.status <> 'Rejected'::bpchar) UNION ALL SELECT new_orders.oid, new_orders.order_id, new_orders.date, btrim((new_orders.symbol)::text) AS symbol, new_orders.side, new_orders.price, new_orders.shares, new_orders.left_shares, new_orders.ecn, new_orders.time_in_force, order_acks.arcaex_order_id, order_acks.time_stamp AS order_ack_time_stamp, cancels.time_stamp AS filled_time_stamp, cancels.shares AS filled_shares, 2 AS type FROM ((new_orders JOIN order_acks ON ((new_orders.oid = order_acks.oid))) JOIN cancels ON ((new_orders.oid = cancels.oid))) WHERE (new_orders.status <> 'Rejected'::bpchar);


ALTER TABLE public.details OWNER TO postgres;

--
-- Name: entries; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE entries (
    cross_id integer,
    time_stamp time without time zone
);


ALTER TABLE public.entries OWNER TO postgres;

--
-- Name: exits; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE exits (
    cross_id integer,
    time_stamp time without time zone
);


ALTER TABLE public.exits OWNER TO postgres;

--
-- Name: fills_fill_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE fills_fill_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.fills_fill_id_seq OWNER TO postgres;

--
-- Name: fills_fill_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE fills_fill_id_seq OWNED BY fills.fill_id;


--
-- Name: fills_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE fills_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.fills_id_seq OWNER TO postgres;

--
-- Name: fills_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE fills_id_seq OWNED BY fills.id;


--
-- Name: lifecycle; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW lifecycle AS
    (SELECT new_orders.oid, new_orders.order_id, new_orders.date, new_orders.time_stamp, btrim((new_orders.symbol)::text) AS symbol, new_orders.side, new_orders.price, new_orders.shares, new_orders.left_shares, new_orders.ecn, new_orders.time_in_force, order_acks.arcaex_order_id, btrim((new_orders.status)::text) AS status, fills.shares AS filled_shares, 1 AS type FROM ((new_orders JOIN order_acks ON ((new_orders.oid = order_acks.oid))) JOIN fills ON ((new_orders.oid = fills.oid))) WHERE (new_orders.status <> 'Rejected'::bpchar) UNION ALL SELECT new_orders.oid, new_orders.order_id, new_orders.date, new_orders.time_stamp, btrim((new_orders.symbol)::text) AS symbol, new_orders.side, new_orders.price, new_orders.shares, new_orders.left_shares, new_orders.ecn, new_orders.time_in_force, order_acks.arcaex_order_id, btrim((new_orders.status)::text) AS status, cancels.shares AS filled_shares, 2 AS type FROM ((new_orders JOIN order_acks ON ((new_orders.oid = order_acks.oid))) JOIN cancels ON ((new_orders.oid = cancels.oid))) WHERE (new_orders.status <> 'Rejected'::bpchar)) UNION ALL SELECT new_orders.oid, new_orders.order_id, new_orders.date, new_orders.time_stamp, btrim((new_orders.symbol)::text) AS symbol, new_orders.side, new_orders.price, new_orders.shares, new_orders.left_shares, new_orders.ecn, new_orders.time_in_force, order_acks.arcaex_order_id, btrim((new_orders.status)::text) AS status, cancel_pendings.shares AS filled_shares, 3 AS type FROM ((new_orders JOIN order_acks ON ((new_orders.oid = order_acks.oid))) JOIN cancel_pendings ON ((new_orders.oid = cancel_pendings.oid))) WHERE (new_orders.status <> 'Rejected'::bpchar);


ALTER TABLE public.lifecycle OWNER TO postgres;

--
-- Name: manual_open_positions; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE manual_open_positions (
    id integer NOT NULL,
    side character(1),
    symbol character(8) NOT NULL,
    shares integer,
    price double precision,
    date date NOT NULL,
    account integer NOT NULL
);


ALTER TABLE public.manual_open_positions OWNER TO postgres;

--
-- Name: manual_open_positions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE manual_open_positions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.manual_open_positions_id_seq OWNER TO postgres;

--
-- Name: manual_open_positions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE manual_open_positions_id_seq OWNED BY manual_open_positions.id;


--
-- Name: new_orders_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE new_orders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.new_orders_id_seq OWNER TO postgres;

--
-- Name: new_orders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE new_orders_id_seq OWNED BY new_orders.id;


--
-- Name: new_orders_oid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE new_orders_oid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.new_orders_oid_seq OWNER TO postgres;

--
-- Name: new_orders_oid_seq1; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE new_orders_oid_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.new_orders_oid_seq1 OWNER TO postgres;

--
-- Name: new_orders_oid_seq1; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE new_orders_oid_seq1 OWNED BY new_orders.oid;


--
-- Name: oats; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW oats AS
    SELECT DISTINCT ON (new_orders.oid) new_orders.date, new_orders.tid, new_orders.oid, cancel_requests.oid AS cancel_request_id, cancels.oid AS cancel_id, new_orders.order_id, new_orders.entry_exit, new_orders.side, btrim((new_orders.symbol)::text) AS btrim, new_orders.time_stamp, new_orders.ecn, new_orders.shares, new_orders.price, new_orders.time_in_force, new_orders.left_shares, new_orders.status, cancel_requests.time_stamp AS cancel_request_time_stamp, cancels.time_stamp AS canceled_time_stamp, cancels.shares AS canceled_shares, new_orders.is_iso FROM ((new_orders LEFT JOIN cancel_requests ON ((new_orders.oid = cancel_requests.oid))) LEFT JOIN cancels ON ((new_orders.oid = cancels.oid))) ORDER BY new_orders.oid;


ALTER TABLE public.oats OWNER TO postgres;

--
-- Name: open_positions; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE open_positions (
    id integer NOT NULL,
    side character(1),
    symbol character(8) NOT NULL,
    shares integer,
    price double precision,
    date date NOT NULL,
    account integer NOT NULL
);


ALTER TABLE public.open_positions OWNER TO postgres;

--
-- Name: open_positions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE open_positions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.open_positions_id_seq OWNER TO postgres;

--
-- Name: open_positions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE open_positions_id_seq OWNED BY open_positions.id;


--
-- Name: order_acks_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE order_acks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.order_acks_id_seq OWNER TO postgres;

--
-- Name: order_acks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE order_acks_id_seq OWNED BY order_acks.id;


--
-- Name: order_rejects; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE order_rejects (
    id integer NOT NULL,
    oid integer NOT NULL,
    reject_type text,
    reason text,
    time_stamp time without time zone,
    msgseq_num text,
    sending_time text,
    origclord_id text,
    arcaex_order_id text,
    exec_id text,
    ord_status text,
    text text,
    msg_content text,
    entry_exit_ref integer,
    processed_time_stamp time without time zone
);


ALTER TABLE public.order_rejects OWNER TO postgres;

--
-- Name: order_rejects_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE order_rejects_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.order_rejects_id_seq OWNER TO postgres;

--
-- Name: order_rejects_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE order_rejects_id_seq OWNED BY order_rejects.id;


--
-- Name: orex; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW orex AS
    SELECT tmp.date, tmp.side, tmp.symbol, tmp.shares, (tmp.price / (tmp.shares)::double precision) AS price FROM (SELECT abcv.date, substr((abcv.side)::text, 1, 1) AS side, btrim(abcv.btrim) AS symbol, sum(abcv.filled_shares) AS shares, sum((abcv.filled_price * (abcv.filled_shares)::double precision)) AS price FROM (SELECT new_orders.date, new_orders.tid, new_orders.oid, brokens.oid AS broken_id, btrim((new_orders.symbol)::text) AS btrim, new_orders.entry_exit, new_orders.side, new_orders.time_stamp, new_orders.ecn, new_orders.shares, new_orders.price, new_orders.time_in_force, new_orders.left_shares, new_orders.status, fills.shares AS filled_shares, fills.price AS filled_price, brokens.new_price AS changed_price, brokens.exec_id FROM ((new_orders JOIN fills ON ((new_orders.oid = fills.oid))) LEFT JOIN brokens ON (((fills.oid = brokens.oid) AND (fills.exec_id = brokens.exec_id)))) WHERE ((brokens.oid IS NULL) OR ((brokens.oid IS NOT NULL) AND (brokens.new_shares > 0))) UNION ALL SELECT new_orders.date, new_orders.tid, new_orders.oid, brokens.oid AS broken_id, btrim((new_orders.symbol)::text) AS btrim, new_orders.entry_exit, new_orders.side, new_orders.time_stamp, new_orders.ecn, new_orders.shares, new_orders.price, new_orders.time_in_force, new_orders.left_shares, new_orders.status, cancel_pendings.shares AS filled_shares, cancel_pendings.price AS filled_price, brokens.new_price AS changed_price, brokens.exec_id FROM ((new_orders JOIN cancel_pendings ON ((new_orders.oid = cancel_pendings.oid))) LEFT JOIN brokens ON (((cancel_pendings.oid = brokens.oid) AND (cancel_pendings.exec_id = brokens.exec_id)))) WHERE (((brokens.oid IS NULL) OR ((brokens.oid IS NOT NULL) AND (brokens.new_shares > 0))) AND (cancel_pendings.shares > 0))) abcv GROUP BY abcv.date, btrim(abcv.btrim), substr((abcv.side)::text, 1, 1)) tmp;


ALTER TABLE public.orex OWNER TO postgres;

--
-- Name: ouch_cancelbyecn; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW ouch_cancelbyecn AS
    SELECT new_orders.oid, new_orders.order_id, new_orders.symbol, new_orders.ts_id, new_orders.ecn, new_orders.price, new_orders.date, (substr(split_part(order_acks.msg_content, '='::text, 8), 0, 9))::double precision AS price_order_ack FROM (new_orders JOIN order_acks ON ((new_orders.oid = order_acks.oid))) WHERE (((new_orders.status = 'CanceledByECN'::bpchar) AND (new_orders.ecn = 'OUCH'::bpchar)) AND (new_orders.left_shares = new_orders.shares));


ALTER TABLE public.ouch_cancelbyecn OWNER TO postgres;

--
-- Name: ouch_cancelbyecn_statistic; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW ouch_cancelbyecn_statistic AS
    SELECT tmp3.date, tmp3.cancel_orders, tmp3.repriced_orders, (((tmp3.repriced_orders * 100))::double precision / tmp3.cancel_orders) AS percentage FROM (SELECT tmp1.date, (tmp1.cancel_orders)::double precision AS cancel_orders, tmp2.repriced_orders FROM ((SELECT temp1.date, count(*) AS cancel_orders FROM (SELECT new_orders.oid, new_orders.order_id, new_orders.symbol, new_orders.ts_id, new_orders.ecn, new_orders.price, new_orders.date, (substr(split_part(order_acks.msg_content, '='::text, 8), 0, 9))::double precision AS price_order_ack FROM (new_orders JOIN order_acks ON ((new_orders.oid = order_acks.oid))) WHERE (((new_orders.status = 'CanceledByECN'::bpchar) AND (new_orders.ecn = 'OUCH'::bpchar)) AND (new_orders.left_shares = new_orders.shares))) temp1 GROUP BY temp1.date) tmp1 LEFT JOIN (SELECT temp2.date AS date_repriced, count(*) AS repriced_orders FROM (SELECT new_orders.oid, new_orders.order_id, new_orders.symbol, new_orders.ts_id, new_orders.ecn, new_orders.price, new_orders.date, (substr(split_part(order_acks.msg_content, '='::text, 8), 0, 9))::double precision AS price_order_ack FROM (new_orders JOIN order_acks ON ((new_orders.oid = order_acks.oid))) WHERE (((new_orders.status = 'CanceledByECN'::bpchar) AND (new_orders.ecn = 'OUCH'::bpchar)) AND (new_orders.left_shares = new_orders.shares))) temp2 WHERE (temp2.price <> temp2.price_order_ack) GROUP BY temp2.date) tmp2 ON ((tmp1.date = tmp2.date_repriced))) ORDER BY tmp1.date) tmp3;


ALTER TABLE public.ouch_cancelbyecn_statistic OWNER TO postgres;

--
-- Name: raw_crosses; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE raw_crosses (
    id integer NOT NULL,
    as_cid integer,
    ts_cid integer,
    ts_id integer,
    symbol character(12),
    raw_content character(80),
    date date,
    trade_id integer,
    trade_type integer
);


ALTER TABLE public.raw_crosses OWNER TO postgres;

--
-- Name: raw_crosses_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE raw_crosses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.raw_crosses_id_seq OWNER TO postgres;

--
-- Name: raw_crosses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE raw_crosses_id_seq OWNED BY raw_crosses.id;


--
-- Name: release_versions; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE release_versions (
    id integer NOT NULL,
    module_id integer NOT NULL,
    version text NOT NULL,
    release_date date DEFAULT now(),
    description text
);


ALTER TABLE public.release_versions OWNER TO postgres;

--
-- Name: release_versions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE release_versions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.release_versions_id_seq OWNER TO postgres;

--
-- Name: release_versions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE release_versions_id_seq OWNED BY release_versions.id;


--
-- Name: risk_managements; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE risk_managements (
    id integer NOT NULL,
    risk_id integer,
    symbol text,
    average_price double precision,
    net_quantity integer,
    matched_profit_loss double precision,
    unmatched_profit_loss double precision,
    last_price double precision,
    total_market_value double precision,
    total_quantity integer,
    date date,
    last_update time without time zone,
    account integer,
    total_fee double precision DEFAULT 0.00,
    manual_update integer DEFAULT 0,
    changed_shares integer DEFAULT 0
);


ALTER TABLE public.risk_managements OWNER TO postgres;

--
-- Name: risk_managements_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE risk_managements_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.risk_managements_id_seq OWNER TO postgres;

--
-- Name: risk_managements_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE risk_managements_id_seq OWNED BY risk_managements.id;


--
-- Name: risk_users; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE risk_users (
    id integer NOT NULL,
    user_id integer NOT NULL,
    username text,
    password text,
    user_group text,
    activated boolean,
    description text
);


ALTER TABLE public.risk_users OWNER TO postgres;

--
-- Name: risk_users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE risk_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.risk_users_id_seq OWNER TO postgres;

--
-- Name: risk_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE risk_users_id_seq OWNED BY risk_users.id;


--
-- Name: rm_simultaneous_trades; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE rm_simultaneous_trades (
    id integer NOT NULL,
    symbol text,
    average_price double precision,
    net_quantity integer,
    matched_profit_loss double precision,
    unmatched_profit_loss double precision,
    last_price double precision,
    total_market_value double precision,
    total_quantity integer,
    date date,
    last_update time without time zone,
    account integer
);


ALTER TABLE public.rm_simultaneous_trades OWNER TO postgres;

--
-- Name: rm_simultaneous_trades_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE rm_simultaneous_trades_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rm_simultaneous_trades_id_seq OWNER TO postgres;

--
-- Name: rm_simultaneous_trades_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE rm_simultaneous_trades_id_seq OWNED BY rm_simultaneous_trades.id;


--
-- Name: trade1; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW trade1 AS
    SELECT new_orders.date, new_orders.tid, new_orders.oid, brokens.oid AS broken_id, btrim((new_orders.symbol)::text) AS btrim, new_orders.entry_exit, new_orders.side, new_orders.time_stamp, new_orders.ecn, new_orders.shares, new_orders.price, new_orders.time_in_force, new_orders.left_shares, new_orders.status, fills.shares AS filled_shares, fills.price AS filled_price, brokens.new_price AS changed_price, brokens.exec_id FROM ((new_orders JOIN fills ON ((new_orders.oid = fills.oid))) LEFT JOIN brokens ON (((fills.oid = brokens.oid) AND (fills.exec_id = brokens.exec_id)))) WHERE ((brokens.oid IS NULL) OR ((brokens.oid IS NOT NULL) AND (brokens.new_shares > 0))) UNION ALL SELECT new_orders.date, new_orders.tid, new_orders.oid, brokens.oid AS broken_id, btrim((new_orders.symbol)::text) AS btrim, new_orders.entry_exit, new_orders.side, new_orders.time_stamp, new_orders.ecn, new_orders.shares, new_orders.price, new_orders.time_in_force, new_orders.left_shares, new_orders.status, cancel_pendings.shares AS filled_shares, cancel_pendings.price AS filled_price, brokens.new_price AS changed_price, brokens.exec_id FROM ((new_orders JOIN cancel_pendings ON ((new_orders.oid = cancel_pendings.oid))) LEFT JOIN brokens ON (((cancel_pendings.oid = brokens.oid) AND (cancel_pendings.exec_id = brokens.exec_id)))) WHERE (((brokens.oid IS NULL) OR ((brokens.oid IS NOT NULL) AND (brokens.new_shares > 0))) AND (cancel_pendings.shares > 0));


ALTER TABLE public.trade1 OWNER TO postgres;

--
-- Name: trade; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW trade AS
    SELECT tmp.date, tmp.side, tmp.symbol, tmp.shares, (tmp.price / (tmp.shares)::double precision) AS price FROM (SELECT abcv.date, substr((abcv.side)::text, 1, 1) AS side, btrim(abcv.btrim) AS symbol, sum(abcv.filled_shares) AS shares, sum((abcv.filled_price * (abcv.filled_shares)::double precision)) AS price FROM (SELECT trade1.date, trade1.side, trade1.btrim, trade1.filled_shares, trade1.filled_price FROM trade1 WHERE (trade1.changed_price IS NULL) UNION ALL SELECT trade1.date, trade1.side, trade1.btrim, trade1.filled_shares, trade1.changed_price AS filled_price FROM trade1 WHERE (trade1.changed_price IS NOT NULL)) abcv GROUP BY abcv.date, btrim(abcv.btrim), substr((abcv.side)::text, 1, 1)) tmp;


ALTER TABLE public.trade OWNER TO postgres;

--
-- Name: trade_results; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE trade_results (
    id integer NOT NULL,
    expected_shares integer,
    expected_pl double precision,
    bought_shares integer,
    total_bought double precision,
    sold_shares integer,
    total_sold double precision,
    left_stuck_shares integer,
    as_cid integer,
    ts_cid integer,
    ts_id integer,
    trade_id integer,
    symbol character(8),
    date date,
    "timestamp" integer,
    spread integer,
    ts_total_fee double precision DEFAULT 0.00,
    pm_total_fee double precision DEFAULT 0.00,
    ts_profit_loss double precision DEFAULT 0.00,
    pm_profit_loss double precision DEFAULT 0.00
);


ALTER TABLE public.trade_results OWNER TO postgres;

--
-- Name: trade_results_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE trade_results_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.trade_results_id_seq OWNER TO postgres;

--
-- Name: trade_results_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE trade_results_id_seq OWNED BY trade_results.id;


--
-- Name: trade_servers; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE trade_servers (
    id integer NOT NULL,
    ts_id integer NOT NULL,
    description text,
    ip text,
    port integer,
    book_arca integer,
    book_nasdaq integer,
    book_bats integer,
    arca_direct integer,
    nasdaq_ouch integer,
    bats_fix integer,
    nasdaq_rash integer,
    cqs integer,
    uqdf integer,
    nasdaq_bx integer,
    ouch_bx integer,
    rash_bx integer,
    book_nyse_open integer DEFAULT 1,
    nyse_ccg integer DEFAULT 1,
    book_edgx integer DEFAULT 1,
    edgx_direct integer DEFAULT 1,
    book_edga integer DEFAULT 1,
    edga_direct integer DEFAULT 1,
    book_nasdaq_bx integer DEFAULT 1,
    nasdaq_ouch_bx integer DEFAULT 1,
    book_byx integer DEFAULT 1,
    byx_boe integer DEFAULT 1,
    book_psx integer DEFAULT 1,
    psx_ouch integer DEFAULT 1,
    book_amex integer DEFAULT 1
);


ALTER TABLE public.trade_servers OWNER TO postgres;

--
-- Name: trade_servers_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE trade_servers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.trade_servers_id_seq OWNER TO postgres;

--
-- Name: trade_servers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE trade_servers_id_seq OWNED BY trade_servers.id;


--
-- Name: traded_orders; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW traded_orders AS
    SELECT DISTINCT n.oid AS id, n.date AS trade_date, n.symbol, n.account, n.order_id AS seqid, n.entry_exit, n.side AS action, n.time_stamp AS trade_time, CASE WHEN ((n.ecn = 'BATZ'::bpchar) AND (n.routing_inst = 'RL2'::bpchar)) THEN 'BZSR'::bpchar ELSE n.ecn END AS ecn, n.shares AS qty, n.price, n.time_in_force AS tif, n.left_shares AS lvs, n.status, n.ts_id AS ts, n.is_iso, b.filing_status, CASE WHEN ((n.shares - n.left_shares) = 0) THEN (0)::double precision ELSE n.avg_price END AS avg_price, CASE WHEN (n.status ~~ 'Closed%'::text) THEN 'Fully Filled'::bpchar WHEN ((n.status ~~ 'Cancel%'::text) AND (n.left_shares < n.shares)) THEN 'Partially Filled'::bpchar WHEN (n.status ~~ 'Cancel%'::text) THEN 'Cancelled'::bpchar ELSE n.status END AS status_edit, CASE WHEN (n.ts_id <> (-1)) THEN n.visible_shares ELSE 0 END AS visible_shares, CASE WHEN (((n.ts_id <> (-1)) AND (n.visible_shares > 0)) AND ((n.shares - n.left_shares) > n.visible_shares)) THEN ((n.shares - n.left_shares) - n.visible_shares) ELSE 0 END AS oversize_shares, (n.shares - n.left_shares) AS filled_shares, CASE WHEN ((((n.ts_id <> (-1)) AND (n.visible_shares > 0)) AND (n.shares > n.left_shares)) AND ((n.shares - n.left_shares) > n.visible_shares)) THEN ((((n.shares - n.left_shares) - n.visible_shares))::double precision / ((n.shares - n.left_shares))::double precision) ELSE (0)::double precision END AS percent_hidden_liquidity FROM (new_orders n LEFT JOIN brokens b ON (((n.date = b.trade_date) AND (n.oid = b.oid)))) ORDER BY n.oid, n.date, n.symbol, n.order_id, n.entry_exit, n.side, n.time_stamp, CASE WHEN ((n.ecn = 'BATZ'::bpchar) AND (n.routing_inst = 'RL2'::bpchar)) THEN 'BZSR'::bpchar ELSE n.ecn END, n.shares, n.price, n.time_in_force, n.left_shares, n.status, n.ts_id, n.is_iso, b.filing_status, n.account, CASE WHEN (n.ts_id <> (-1)) THEN n.visible_shares ELSE 0 END, CASE WHEN (((n.ts_id <> (-1)) AND (n.visible_shares > 0)) AND ((n.shares - n.left_shares) > n.visible_shares)) THEN ((n.shares - n.left_shares) - n.visible_shares) ELSE 0 END;


ALTER TABLE public.traded_orders OWNER TO postgres;

--
-- Name: trading_status; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE trading_status (
    status integer,
    updated integer DEFAULT 0,
    rmupdated integer DEFAULT 0,
    hung_order integer DEFAULT 0
);


ALTER TABLE public.trading_status OWNER TO postgres;

--
-- Name: tt_event_log; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tt_event_log (
    username character(24) NOT NULL,
    activity_type integer NOT NULL,
    log_detail text,
    date date NOT NULL,
    time_stamp time without time zone NOT NULL,
    description text
);


ALTER TABLE public.tt_event_log OWNER TO postgres;

--
-- Name: twt_event_log; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE twt_event_log (
    id integer NOT NULL,
    ip_address character(24) NOT NULL,
    event text NOT NULL,
    date date,
    time_stamp character(16)
);


ALTER TABLE public.twt_event_log OWNER TO postgres;

--
-- Name: twt_event_log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE twt_event_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.twt_event_log_id_seq OWNER TO postgres;

--
-- Name: twt_event_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE twt_event_log_id_seq OWNED BY twt_event_log.id;


--
-- Name: user_login_info; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE user_login_info (
    username character(24) NOT NULL,
    password character(80) NOT NULL,
    status integer,
    id integer NOT NULL
);


ALTER TABLE public.user_login_info OWNER TO postgres;

--
-- Name: user_login_info_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE user_login_info_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_login_info_id_seq OWNER TO postgres;

--
-- Name: user_login_info_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE user_login_info_id_seq OWNED BY user_login_info.id;


--
-- Name: version_control; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE version_control (
    id integer NOT NULL,
    module character(6) NOT NULL,
    ver character(10) NOT NULL,
    trader character(20) NOT NULL,
    trader_note text,
    date date
);


ALTER TABLE public.version_control OWNER TO postgres;

--
-- Name: version_control_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE version_control_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.version_control_id_seq OWNER TO postgres;

--
-- Name: version_control_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE version_control_id_seq OWNED BY version_control.id;


--
-- Name: view_avg_times; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW view_avg_times AS
    SELECT new_orders.oid, new_orders.order_id, new_orders.ecn, new_orders.ts_id, new_orders.date, new_orders.time_stamp, (order_acks.time_stamp - new_orders.time_stamp) AS time_ack, min((fills.time_stamp - order_acks.time_stamp)) AS time_fill FROM ((new_orders JOIN order_acks ON ((new_orders.oid = order_acks.oid))) JOIN fills ON ((new_orders.oid = fills.oid))) WHERE ((new_orders.ts_id <> (-1)) AND ((order_acks.time_stamp - new_orders.time_stamp) < '00:00:15'::interval)) GROUP BY new_orders.oid, new_orders.order_id, new_orders.ecn, new_orders.ts_id, new_orders.date, new_orders.time_stamp, (order_acks.time_stamp - new_orders.time_stamp);


ALTER TABLE public.view_avg_times OWNER TO postgres;

--
-- Name: view_liquidity; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW view_liquidity AS
    SELECT fills.shares, fills.price, fills.liquidity, new_orders.ecn, new_orders.side, new_orders.date FROM (fills JOIN new_orders ON ((new_orders.oid = fills.oid))) ORDER BY new_orders.oid;


ALTER TABLE public.view_liquidity OWNER TO postgres;

--
-- Name: view_trade_view; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW view_trade_view AS
    SELECT DISTINCT raw_crosses.id, raw_crosses.as_cid, raw_crosses.ts_cid, raw_crosses.ts_id, raw_crosses.symbol, raw_crosses.raw_content, raw_crosses.date, raw_crosses.trade_id, raw_crosses.trade_type, trade_results.bought_shares, trade_results.sold_shares, trade_results.total_bought, trade_results.total_sold, bbo_infos.content FROM ((raw_crosses LEFT JOIN trade_results ON (((((raw_crosses.date = trade_results.date) AND (raw_crosses.as_cid = trade_results.as_cid)) AND (raw_crosses.ts_cid = trade_results.ts_cid)) AND (raw_crosses.ts_id = trade_results.ts_id)))) LEFT JOIN bbo_infos ON ((((raw_crosses.date = bbo_infos.date) AND (raw_crosses.ts_id = bbo_infos.ts_id)) AND (raw_crosses.ts_cid = bbo_infos.cross_id)))) ORDER BY raw_crosses.id, raw_crosses.as_cid, raw_crosses.ts_cid, raw_crosses.ts_id, raw_crosses.symbol, raw_crosses.raw_content, raw_crosses.date, raw_crosses.trade_id, raw_crosses.trade_type, trade_results.bought_shares, trade_results.sold_shares, trade_results.total_bought, trade_results.total_sold, bbo_infos.content;


ALTER TABLE public.view_trade_view OWNER TO postgres;

--
-- Name: vw_ack_delays; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW vw_ack_delays AS
    SELECT n.oid, n.entry_exit, n.date, date_part('microseconds'::text, (a.time_stamp - n.time_stamp)) AS ack_delay, date_part('microseconds'::text, (a.processed_time_stamp - a.time_stamp)) AS ack_internal_delay, n.ecn, n.ecn_suffix, n.order_id, n.time_stamp, btrim((n.routing_inst)::text) AS routing_inst FROM (new_orders n JOIN order_acks a ON ((a.oid = n.oid))) WHERE ((n.ts_id <> (-1)) AND (n.order_id <> 0)) ORDER BY n.oid, n.date;


ALTER TABLE public.vw_ack_delays OWNER TO postgres;

--
-- Name: vw_cancel_delays; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW vw_cancel_delays AS
    SELECT c.id, n.entry_exit, date_part('microseconds'::text, (c.time_stamp - a.time_stamp)) AS cancel_delay, n.date, c.time_stamp FROM ((new_orders n JOIN order_acks a ON ((a.oid = n.oid))) JOIN cancels c ON ((c.oid = n.oid))) WHERE ((n.ts_id <> (-1)) AND (n.order_id <> 0)) ORDER BY c.id;


ALTER TABLE public.vw_cancel_delays OWNER TO postgres;

--
-- Name: vw_entry_delays; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW vw_entry_delays AS
    SELECT new_orders.oid, new_orders.order_id, new_orders.time_stamp, split_part((raw_crosses.raw_content)::text, ' '::text, 10) AS quote_time_stamp, new_orders.date, new_orders.ecn, split_part((raw_crosses.raw_content)::text, ' '::text, 11) AS reason FROM (new_orders JOIN raw_crosses ON ((((raw_crosses.date = new_orders.date) AND (raw_crosses.ts_id = new_orders.ts_id)) AND (raw_crosses.ts_cid = new_orders.cross_id)))) WHERE (((((raw_crosses.trade_id > 0) AND (raw_crosses.raw_content ~~ '2%'::text)) AND (new_orders.order_id <> 0)) AND (new_orders.ts_id <> (-1))) AND (new_orders.entry_exit = 'Entry'::bpchar)) ORDER BY new_orders.oid, new_orders.time_stamp, split_part((raw_crosses.raw_content)::text, ' '::text, 10), new_orders.date;


ALTER TABLE public.vw_entry_delays OWNER TO postgres;

--
-- Name: vw_exit_delays; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW vw_exit_delays AS
    SELECT _exit.id, _exit.date, _exit.time_stamp, _trigger.order_id, date_part('microseconds'::text, (_exit.time_stamp - _trigger.processed_time_stamp)) AS delay, _trigger.ecn, _trigger.ecn_suffix FROM (((SELECT new_orders.date, new_orders.ts_id, new_orders.cross_id, new_orders.order_id, new_orders.ecn, new_orders.ecn_suffix, order_rejects.entry_exit_ref, order_rejects.processed_time_stamp FROM (new_orders JOIN order_rejects ON ((new_orders.oid = order_rejects.oid))) UNION SELECT new_orders.date, new_orders.ts_id, new_orders.cross_id, new_orders.order_id, new_orders.ecn, new_orders.ecn_suffix, cancels.entry_exit_ref, cancels.processed_time_stamp FROM (new_orders JOIN cancels ON ((new_orders.oid = cancels.oid)))) UNION SELECT new_orders.date, new_orders.ts_id, new_orders.cross_id, new_orders.order_id, new_orders.ecn, new_orders.ecn_suffix, fills.entry_exit_ref, fills.processed_time_stamp FROM (new_orders JOIN fills ON ((new_orders.oid = fills.oid)))) _trigger JOIN (SELECT new_orders.date, new_orders.ts_id, new_orders.cross_id, min(new_orders.id) AS id, new_orders.entry_exit_ref, min(new_orders.time_stamp) AS time_stamp FROM new_orders WHERE ((new_orders.ts_id <> (-1)) AND (new_orders.entry_exit = 'Exit'::bpchar)) GROUP BY new_orders.date, new_orders.ts_id, new_orders.cross_id, new_orders.entry_exit_ref) _exit ON (((((_trigger.date = _exit.date) AND (_trigger.ts_id = _exit.ts_id)) AND (_trigger.cross_id = _exit.cross_id)) AND (_trigger.entry_exit_ref = _exit.entry_exit_ref)))) ORDER BY _exit.id;


ALTER TABLE public.vw_exit_delays OWNER TO postgres;

--
-- Name: vw_fill_delays; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW vw_fill_delays AS
    SELECT f.fill_id, n.entry_exit, date_part('microseconds'::text, (f.time_stamp - a.time_stamp)) AS fill_delay, date_part('microseconds'::text, (f.processed_time_stamp - f.time_stamp)) AS fill_internal_delay, n.date, n.ecn, n.ecn_suffix, n.order_id, f.time_stamp, btrim((n.routing_inst)::text) AS routing_inst FROM ((new_orders n JOIN order_acks a ON ((a.oid = n.oid))) JOIN fills f ON ((f.oid = n.oid))) WHERE ((n.ts_id <> (-1)) AND (n.order_id <> 0)) ORDER BY f.fill_id, n.date;


ALTER TABLE public.vw_fill_delays OWNER TO postgres;

--
-- Name: vw_trade_view; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW vw_trade_view AS
    (SELECT DISTINCT tr.date, tr.as_cid, tr.ts_cid, tr.ts_id, tr.symbol, tr.bought_shares, tr.sold_shares, tr.total_bought, tr.total_sold, tr.trade_id, 0 AS trade_type, ('0 '::text || bi.content) AS raw_content, bi.bbo_id, bi.id, (tr."timestamp")::text AS "timestamp" FROM (trade_results tr JOIN bbo_infos bi ON ((((bi.date = tr.date) AND (bi.cross_id = tr.ts_cid)) AND (bi.ts_id = tr.ts_id)))) ORDER BY bi.id, tr.date, tr.as_cid, tr.ts_cid, tr.ts_id, tr.symbol, tr.bought_shares, tr.sold_shares, tr.total_bought, tr.total_sold, tr.trade_id, 0::integer, ('0 '::text || bi.content), bi.bbo_id, (tr."timestamp")::text) UNION (SELECT DISTINCT rc.date, rc.as_cid, rc.ts_cid, rc.ts_id, rc.symbol, tr.bought_shares, tr.sold_shares, tr.total_bought, tr.total_sold, tr.trade_id, rc.trade_type, rc.raw_content, (-1) AS bbo_id, (rc.id + 100000000) AS id, split_part((rc.raw_content)::text, ' '::text, 3) AS "timestamp" FROM (raw_crosses rc LEFT JOIN trade_results tr ON ((((rc.date = tr.date) AND (rc.ts_cid = tr.ts_cid)) AND (rc.ts_id = tr.ts_id)))) ORDER BY (rc.id + 100000000), rc.date, rc.as_cid, rc.ts_cid, rc.ts_id, rc.symbol, tr.bought_shares, tr.sold_shares, tr.total_bought, tr.total_sold, tr.trade_id, rc.trade_type, rc.raw_content, (-1)::integer, split_part((rc.raw_content)::text, ' '::text, 3)) ORDER BY 2, 14;


ALTER TABLE public.vw_trade_view OWNER TO postgres;

--
-- Name: vwack_and_fills; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW vwack_and_fills AS
    SELECT n2.ts_id, n2.cross_id, n2.ecn_launch, n2.order_id, CASE WHEN ((n2.ecn = 'BATZ'::bpchar) AND (n2.routing_inst = 'RL2'::bpchar)) THEN 'BZSR'::bpchar ELSE n2.ecn END AS ecn, n2.symbol, n2.date, n2.time_stamp, n2.ack_time_stamp, n2.fill_time_stamp, ((substr(((n2.ack_time_stamp - n2.time_stamp))::text, 8, 9))::double precision * (1000000)::double precision) AS ack_delay, ((substr(((n2.fill_time_stamp - n2.ack_time_stamp))::text, 8, 9))::double precision * (1000000)::double precision) AS fill_delay, n2.is_iso FROM (SELECT n1.ts_id, n1.cross_id, n1.order_id, n1.ecn, n1.ecn_launch, n1.date, n1.time_stamp, a.time_stamp AS ack_time_stamp, min(f.time_stamp) AS fill_time_stamp, n1.symbol, n1.is_iso, n1.routing_inst FROM ((new_orders n1 JOIN order_acks a ON ((a.oid = n1.oid))) JOIN fills f ON ((f.oid = n1.oid))) WHERE ((n1.left_shares < n1.shares) AND (n1.ts_id <> (-1))) GROUP BY n1.ts_id, n1.cross_id, n1.order_id, n1.ecn, n1.ecn_launch, n1.date, n1.time_stamp, a.time_stamp, n1.symbol, n1.is_iso, n1.routing_inst) n2;


ALTER TABLE public.vwack_and_fills OWNER TO postgres;

--
-- Name: vwbrokens; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW vwbrokens AS
    SELECT n.symbol, b.id, n.order_id AS cl_order_id, f.shares AS filled_shares, f.price AS filled_price, b.new_price AS adjusted_price, n.ecn, n.time_stamp AS trade_time, n.side, b.trade_date, b.filing_status, b.cee_note, b.is_cee FROM ((new_orders n JOIN fills f ON ((n.oid = f.oid))) JOIN brokens b ON ((f.fill_id = b.fill_id)));


ALTER TABLE public.vwbrokens OWNER TO postgres;

--
-- Name: vwentry_reserve; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW vwentry_reserve AS
    SELECT new_orders.oid, new_orders.side, new_orders.ecn, new_orders.date, new_orders.ts_id, new_orders.price, new_orders.shares, ask_bids.price AS price_ab, ask_bids.shares AS shares_ab, tmp.reserve FROM ((new_orders JOIN ask_bids ON (((((new_orders.ts_id = ask_bids.ts_id) AND (new_orders.cross_id = ask_bids.ts_cid)) AND (new_orders.date = ask_bids.date)) AND (new_orders.ask_bid <> ask_bids.ask_bid)))) LEFT JOIN (SELECT DISTINCT new_orders.oid, new_orders.date, 1 AS reserve FROM ((new_orders JOIN ask_bids ON (((((new_orders.ts_id = ask_bids.ts_id) AND (new_orders.cross_id = ask_bids.ts_cid)) AND (new_orders.date = ask_bids.date)) AND (new_orders.ask_bid <> ask_bids.ask_bid)))) JOIN (SELECT fills.oid, fills.price, sum(fills.shares) AS shares_fill FROM fills GROUP BY fills.oid, fills.price) fills_tmp ON ((fills_tmp.oid = new_orders.oid))) WHERE (((((new_orders.entry_exit = 'Entry'::bpchar) AND (new_orders.shares > new_orders.left_shares)) AND (new_orders.price = ask_bids.price)) AND (new_orders.shares > ask_bids.shares)) AND (fills_tmp.shares_fill > ask_bids.shares)) ORDER BY new_orders.oid, new_orders.date, 1::integer) tmp ON (((tmp.oid = new_orders.oid) AND (tmp.date = new_orders.date)))) WHERE ((new_orders.entry_exit = 'Entry'::bpchar) AND (new_orders.shares > new_orders.left_shares)) ORDER BY new_orders.oid, new_orders.side, new_orders.ecn, new_orders.date, new_orders.ts_id, new_orders.price, new_orders.shares, ask_bids.price, ask_bids.shares, tmp.reserve;


ALTER TABLE public.vwentry_reserve OWNER TO postgres;

--
-- Name: vwouch_missed_trades; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW vwouch_missed_trades AS
    SELECT trade_results.expected_shares, trade_results.expected_pl, trade_results.bought_shares, trade_results.sold_shares, trade_results.symbol, trade_results.date, new_orders.ecn, new_orders.entry_exit, new_orders.price, new_orders.ack_price FROM (trade_results JOIN new_orders ON ((((trade_results.date = new_orders.date) AND (trade_results.ts_cid = new_orders.cross_id)) AND (trade_results.ts_id = new_orders.ts_id)))) WHERE ((((new_orders.ecn = 'OUCH'::bpchar) AND (new_orders.price <> new_orders.ack_price)) AND (trade_results.bought_shares = 0)) AND (trade_results.sold_shares = 0));


ALTER TABLE public.vwouch_missed_trades OWNER TO postgres;

--
-- Name: vwraw_crosses_bbo_infos; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW vwraw_crosses_bbo_infos AS
    SELECT raw_crosses.id, raw_crosses.as_cid, raw_crosses.ts_cid, raw_crosses.ts_id, raw_crosses.symbol, raw_crosses.raw_content, raw_crosses.date, raw_crosses.trade_id, raw_crosses.trade_type, bbo_infos.content FROM (raw_crosses LEFT JOIN bbo_infos ON ((((raw_crosses.date = bbo_infos.date) AND (raw_crosses.ts_id = bbo_infos.ts_id)) AND (raw_crosses.ts_cid = bbo_infos.cross_id))));


ALTER TABLE public.vwraw_crosses_bbo_infos OWNER TO postgres;

--
-- Name: vwraw_crosses_trade_results; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW vwraw_crosses_trade_results AS
    SELECT DISTINCT raw_crosses.id, raw_crosses.as_cid, raw_crosses.ts_cid, raw_crosses.ts_id, raw_crosses.symbol, raw_crosses.raw_content, raw_crosses.date, raw_crosses.trade_id, raw_crosses.trade_type, trade_results.bought_shares, trade_results.sold_shares, trade_results.total_bought, trade_results.total_sold, bbo_infos.content, new_orders.ecn, new_orders.entry_exit FROM (((raw_crosses LEFT JOIN trade_results ON (((((raw_crosses.date = trade_results.date) AND (raw_crosses.as_cid = trade_results.as_cid)) AND (raw_crosses.ts_cid = trade_results.ts_cid)) AND (raw_crosses.ts_id = trade_results.ts_id)))) LEFT JOIN bbo_infos ON ((((raw_crosses.date = bbo_infos.date) AND (raw_crosses.ts_id = bbo_infos.ts_id)) AND (raw_crosses.ts_cid = bbo_infos.cross_id)))) LEFT JOIN new_orders ON ((((raw_crosses.date = new_orders.date) AND (raw_crosses.ts_id = new_orders.ts_id)) AND (raw_crosses.ts_cid = new_orders.cross_id)))) ORDER BY raw_crosses.id, raw_crosses.as_cid, raw_crosses.ts_cid, raw_crosses.ts_id, raw_crosses.symbol, raw_crosses.raw_content, raw_crosses.date, raw_crosses.trade_id, raw_crosses.trade_type, trade_results.bought_shares, trade_results.sold_shares, trade_results.total_bought, trade_results.total_sold, bbo_infos.content, new_orders.ecn, new_orders.entry_exit;


ALTER TABLE public.vwraw_crosses_trade_results OWNER TO postgres;

--
-- Name: vwstatistics; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW vwstatistics AS
    SELECT new_orders.id, new_orders.oid, new_orders.tid, new_orders.order_id, new_orders.entry_exit, new_orders.ask_bid, new_orders.side, new_orders.time_stamp, new_orders.ecn, new_orders.shares, new_orders.price, new_orders.time_in_force, new_orders.left_shares, new_orders.status, new_orders.msgseq_num, new_orders.msg_content, new_orders.symbol, new_orders.avg_price, new_orders.date, new_orders.ts_id, new_orders.stock_type, new_orders.ecn_launch, new_orders.cross_id, new_orders.ecn_suffix, new_orders.trade_type, order_acks.price AS ack_price FROM (new_orders JOIN order_acks ON ((new_orders.oid = order_acks.oid))) WHERE ((new_orders.ecn = 'ARCA'::bpchar) OR ((new_orders.price = order_acks.price) AND (new_orders.ecn <> 'ARCA'::bpchar)));


ALTER TABLE public.vwstatistics OWNER TO postgres;

--
-- Name: vwstatistics_twt; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW vwstatistics_twt AS
    SELECT new_orders.id, new_orders.oid, new_orders.tid, new_orders.order_id, new_orders.entry_exit, new_orders.ask_bid, new_orders.side, new_orders.time_stamp, new_orders.ecn, new_orders.shares, new_orders.price, new_orders.time_in_force, new_orders.left_shares, new_orders.status, new_orders.msgseq_num, new_orders.msg_content, new_orders.symbol, new_orders.avg_price, new_orders.date, new_orders.ts_id, new_orders.stock_type, new_orders.ecn_launch, new_orders.cross_id, new_orders.ecn_suffix, new_orders.trade_type, new_orders.is_iso, order_acks.price AS ack_price, new_orders.routing_inst FROM (new_orders JOIN order_acks ON ((new_orders.oid = order_acks.oid)));


ALTER TABLE public.vwstatistics_twt OWNER TO postgres;

--
-- Name: vwtrade_breaks; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW vwtrade_breaks AS
    SELECT new_orders.oid, new_orders.symbol, CASE WHEN ((new_orders.ecn = 'BATZ'::bpchar) AND (new_orders.routing_inst = 'RL2'::bpchar)) THEN 'BZSR'::bpchar ELSE new_orders.ecn END AS ecn, new_orders.date, (fills.origclord_id)::integer AS cl_order_id, new_orders.shares AS shares_order, new_orders.side AS action, fills.shares AS filled_shares, new_orders.price AS price_order, fills.price AS filled_price, fills.fill_id, fills.time_stamp AS trade_time FROM (new_orders JOIN fills ON ((new_orders.oid = fills.oid))) ORDER BY new_orders.oid;


ALTER TABLE public.vwtrade_breaks OWNER TO postgres;

--
-- Name: vwtrade_breaks_695; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW vwtrade_breaks_695 AS
    SELECT new_orders.oid, new_orders.symbol, new_orders.ecn, new_orders.date, (fills.origclord_id)::integer AS cl_order_id, new_orders.shares AS shares_order, fills.shares AS filled_shares, new_orders.price AS price_order, fills.price AS filled_price, fills.fill_id, fills.time_stamp AS trade_time FROM (new_orders JOIN fills ON ((new_orders.oid = fills.oid))) ORDER BY new_orders.oid;


ALTER TABLE public.vwtrade_breaks_695 OWNER TO postgres;


CREATE VIEW vwtrade_results_statistics AS
    SELECT t.expected_shares, t.expected_pl, t.bought_shares, t.total_bought, t.sold_shares, t.total_sold, t.left_stuck_shares, t.as_cid, t.ts_cid, t.ts_id, t.trade_id, t.symbol, t.date, t."timestamp", t.spread, t.ts_total_fee, t.pm_total_fee, t.ts_profit_loss, t.pm_profit_loss, l.shares AS launch_shares FROM (trade_results t LEFT JOIN (SELECT a.date, a.ts_id, a.ts_cid, a.ecn_launch, a.shares FROM (ask_bids a LEFT JOIN new_orders n ON (((((n.date = a.date) AND (n.ts_id = a.ts_id)) AND (n.cross_id = a.ts_cid)) AND (n.ecn_launch = a.ecn_launch)))) WHERE (n.entry_exit = 'Entry'::bpchar) GROUP BY a.date, a.ts_id, a.ts_cid, a.ecn_launch, a.shares) l ON ((((l.date = t.date) AND (l.ts_id = t.ts_id)) AND (l.ts_cid = t.ts_cid))));


ALTER TABLE public.vwtrade_results_statistics OWNER TO postgres;

--
-- Name: vwtrade_results_twt2; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW vwtrade_results_twt2 AS
    SELECT t.expected_shares, t.expected_pl, t.bought_shares, t.total_bought, t.sold_shares, t.total_sold, t.left_stuck_shares, t.as_cid, t.ts_cid, t.ts_id, t.trade_id, t.symbol, t.date, t.ts_profit_loss, n.ecn, n.ecn_launch, n.time_stamp, n.price, n.ack_price, n.status, n.entry_exit, n.left_shares, n.shares, n.is_iso, t.pm_profit_loss, t.spread, (((t.ts_profit_loss + t.pm_profit_loss) - t.ts_total_fee) - t.pm_total_fee) AS net_pl, (t.pm_profit_loss - t.pm_total_fee) AS pm_net_pl, n.launch_shares, t.ts_total_fee, t.pm_total_fee FROM (trade_results t JOIN (SELECT n1.date, n1.ts_id, n1.cross_id, CASE WHEN ((n1.ecn = 'BATZ'::bpchar) AND (n1.routing_inst = 'RL2'::bpchar)) THEN 'BZSR'::bpchar ELSE n1.ecn END AS ecn, n1.time_stamp, n1.price, n1.ack_price, n1.status, n1.entry_exit, n1.left_shares, n1.shares, n1.is_iso, n2.ecn_launch, n1.routing_inst, n2.launch_shares FROM (new_orders n1 LEFT JOIN (SELECT DISTINCT no.date, no.ts_id, no.cross_id, no.ecn_launch, a.shares AS launch_shares FROM (new_orders no LEFT JOIN ask_bids a ON (((((no.date = a.date) AND (no.ts_id = a.ts_id)) AND (no.cross_id = a.ts_cid)) AND (no.ecn_launch = a.ecn_launch)))) WHERE ((no.entry_exit = 'Entry'::bpchar) AND (no.ts_id <> (-1)))) n2 ON ((((n1.date = n2.date) AND (n1.ts_id = n2.ts_id)) AND (n1.cross_id = n2.cross_id))))) n ON ((((t.date = n.date) AND (t.ts_id = n.ts_id)) AND (t.ts_cid = n.cross_id))));


ALTER TABLE public.vwtrade_results_twt2 OWNER TO postgres;

--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY as_settings ALTER COLUMN id SET DEFAULT nextval('as_settings_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ask_bids ALTER COLUMN id SET DEFAULT nextval('ask_bids_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY bbo_infos ALTER COLUMN id SET DEFAULT nextval('bbo_infos_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY broken_trades ALTER COLUMN id SET DEFAULT nextval('broken_trades_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY brokens ALTER COLUMN id SET DEFAULT nextval('brokens_id_seq'::regclass);


--
-- Name: fill_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY brokens ALTER COLUMN fill_id SET DEFAULT nextval('brokens_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cancel_acks ALTER COLUMN id SET DEFAULT nextval('cancel_acks_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cancel_pendings ALTER COLUMN id SET DEFAULT nextval('cancel_pendings_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cancel_rejects ALTER COLUMN id SET DEFAULT nextval('cancel_rejects_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cancel_requests ALTER COLUMN id SET DEFAULT nextval('cancel_requests_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cancels ALTER COLUMN id SET DEFAULT nextval('cancels_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY fills ALTER COLUMN id SET DEFAULT nextval('fills_id_seq'::regclass);


--
-- Name: fill_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY fills ALTER COLUMN fill_id SET DEFAULT nextval('fills_fill_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY manual_open_positions ALTER COLUMN id SET DEFAULT nextval('manual_open_positions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY new_orders ALTER COLUMN id SET DEFAULT nextval('new_orders_id_seq'::regclass);


--
-- Name: oid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY new_orders ALTER COLUMN oid SET DEFAULT nextval('new_orders_oid_seq1'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY open_positions ALTER COLUMN id SET DEFAULT nextval('open_positions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY order_acks ALTER COLUMN id SET DEFAULT nextval('order_acks_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY order_rejects ALTER COLUMN id SET DEFAULT nextval('order_rejects_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY raw_crosses ALTER COLUMN id SET DEFAULT nextval('raw_crosses_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY release_versions ALTER COLUMN id SET DEFAULT nextval('release_versions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY risk_managements ALTER COLUMN id SET DEFAULT nextval('risk_managements_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY risk_users ALTER COLUMN id SET DEFAULT nextval('risk_users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY rm_simultaneous_trades ALTER COLUMN id SET DEFAULT nextval('rm_simultaneous_trades_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY trade_results ALTER COLUMN id SET DEFAULT nextval('trade_results_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY trade_servers ALTER COLUMN id SET DEFAULT nextval('trade_servers_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY twt_event_log ALTER COLUMN id SET DEFAULT nextval('twt_event_log_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_login_info ALTER COLUMN id SET DEFAULT nextval('user_login_info_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY version_control ALTER COLUMN id SET DEFAULT nextval('version_control_id_seq'::regclass);


--
-- Name: ask_bids_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ask_bids
    ADD CONSTRAINT ask_bids_pkey PRIMARY KEY (id);


--
-- Name: bbo_infos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY bbo_infos
    ADD CONSTRAINT bbo_infos_pkey PRIMARY KEY (id);


--
-- Name: broken_trades_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY broken_trades
    ADD CONSTRAINT broken_trades_pkey PRIMARY KEY (oid, exec_id);


--
-- Name: brokens_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY brokens
    ADD CONSTRAINT brokens_pkey PRIMARY KEY (oid, exec_id);


--
-- Name: cancel_acks_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cancel_acks
    ADD CONSTRAINT cancel_acks_pkey PRIMARY KEY (oid);


--
-- Name: cancel_pendings_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cancel_pendings
    ADD CONSTRAINT cancel_pendings_pkey PRIMARY KEY (oid, arcaex_order_id);


--
-- Name: cancel_rejects_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cancel_rejects
    ADD CONSTRAINT cancel_rejects_pkey PRIMARY KEY (id);


--
-- Name: cancel_requests_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cancel_requests
    ADD CONSTRAINT cancel_requests_pkey PRIMARY KEY (id);


--
-- Name: fills_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY fills
    ADD CONSTRAINT fills_pkey PRIMARY KEY (oid, exec_id);


--
-- Name: order_acks_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY order_acks
    ADD CONSTRAINT order_acks_pkey PRIMARY KEY (oid);


--
-- Name: order_rejects_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY order_rejects
    ADD CONSTRAINT order_rejects_pkey PRIMARY KEY (oid);


--
-- Name: pk_as_setting; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY as_settings
    ADD CONSTRAINT pk_as_setting PRIMARY KEY (mid);


--
-- Name: pk_manual_open_position; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY manual_open_positions
    ADD CONSTRAINT pk_manual_open_position PRIMARY KEY (symbol, date, account);


--
-- Name: pk_open_position; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY open_positions
    ADD CONSTRAINT pk_open_position PRIMARY KEY (symbol, date, account);


--
-- Name: pk_order; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY new_orders
    ADD CONSTRAINT pk_order PRIMARY KEY (oid);


--
-- Name: pk_order_id_date; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY new_orders
    ADD CONSTRAINT pk_order_id_date UNIQUE (order_id, date);


--
-- Name: pk_release_versions; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY release_versions
    ADD CONSTRAINT pk_release_versions PRIMARY KEY (id);


--
-- Name: pk_trade_server; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY trade_servers
    ADD CONSTRAINT pk_trade_server PRIMARY KEY (ts_id);


--
-- Name: pk_user; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY risk_users
    ADD CONSTRAINT pk_user PRIMARY KEY (user_id);


--
-- Name: raw_crosses_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY raw_crosses
    ADD CONSTRAINT raw_crosses_pkey PRIMARY KEY (id);


--
-- Name: trade_results_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY trade_results
    ADD CONSTRAINT trade_results_pkey PRIMARY KEY (id);


--
-- Name: twt_event_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY twt_event_log
    ADD CONSTRAINT twt_event_log_pkey PRIMARY KEY (id);


--
-- Name: uk_release_versions; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY release_versions
    ADD CONSTRAINT uk_release_versions UNIQUE (module_id, version);


--
-- Name: user_login_info_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY user_login_info
    ADD CONSTRAINT user_login_info_pkey PRIMARY KEY (username);


--
-- Name: version_control_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY version_control
    ADD CONSTRAINT version_control_pkey PRIMARY KEY (id);


--
-- Name: bbo_infos_date_tsid_crossid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX bbo_infos_date_tsid_crossid ON bbo_infos USING btree (date, ts_id, cross_id);


--
-- Name: fki_ack_oid_new_orders; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fki_ack_oid_new_orders ON order_acks USING btree (oid);


--
-- Name: fki_brokentrade_fills; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fki_brokentrade_fills ON broken_trades USING btree (oid, exec_id);


--
-- Name: fki_can_oid_new_orders; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fki_can_oid_new_orders ON cancels USING btree (oid);


--
-- Name: fki_canack_oid_new_orders; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fki_canack_oid_new_orders ON cancel_acks USING btree (oid);


--
-- Name: fki_canpending_exec_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fki_canpending_exec_id ON cancel_pendings USING btree (exec_id);


--
-- Name: fki_canpending_oid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fki_canpending_oid ON cancel_pendings USING btree (oid);


--
-- Name: fki_canpending_oid_exec_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fki_canpending_oid_exec_id ON cancel_pendings USING btree (oid, exec_id);


--
-- Name: fki_canreject_oid_new_orders; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fki_canreject_oid_new_orders ON cancel_rejects USING btree (oid);


--
-- Name: fki_canrequest_oid_new_orders; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fki_canrequest_oid_new_orders ON cancel_requests USING btree (oid);


--
-- Name: fki_reject_oid_new_orders; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fki_reject_oid_new_orders ON order_rejects USING btree (oid);


--
-- Name: fki_tt_event_log_date_username; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fki_tt_event_log_date_username ON tt_event_log USING btree (date, username);


--
-- Name: fki_tt_event_log_date_username_activity_type; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fki_tt_event_log_date_username_activity_type ON tt_event_log USING btree (date, username, activity_type);


--
-- Name: idx_askbids_date_tsid_tscid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idx_askbids_date_tsid_tscid ON ask_bids USING btree (date, ts_id, ts_cid);


--
-- Name: idx_brokens_tradedate; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idx_brokens_tradedate ON brokens USING btree (trade_date);


--
-- Name: idx_new_orders_date_tsid_crossid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idx_new_orders_date_tsid_crossid ON new_orders USING btree (date, ts_id, cross_id);


--
-- Name: idx_new_orders_oid_price; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idx_new_orders_oid_price ON new_orders USING btree (oid, price);


--
-- Name: idx_rawcrosses_date_tsid_tscid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idx_rawcrosses_date_tsid_tscid ON raw_crosses USING btree (date, ts_id, ts_cid);


--
-- Name: idx_risk_managements_date_symbol; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idx_risk_managements_date_symbol ON risk_managements USING btree (date, symbol);


--
-- Name: idx_traderesults_date_tsid_tscid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idx_traderesults_date_tsid_tscid ON trade_results USING btree (date, ts_id, ts_cid);


--
-- Name: rm_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX rm_date ON risk_managements USING btree (date);


--
-- Name: rm_st_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX rm_st_date ON rm_simultaneous_trades USING btree (date);


--
-- Name: trade_result_date_ascid_tscid_tsid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX trade_result_date_ascid_tscid_tsid ON trade_results USING btree (date, as_cid, ts_cid, ts_id);


--
-- Name: trade_server_pk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX trade_server_pk ON trade_servers USING btree (ts_id);


--
-- Name: user_pk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX user_pk ON risk_users USING btree (user_id);


--
-- Name: str_update_rm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER str_update_rm AFTER INSERT ON fills FOR EACH ROW EXECUTE PROCEDURE process_fills_insert();


--
-- Name: str_update_rm_for_broken; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER str_update_rm_for_broken AFTER INSERT OR DELETE OR UPDATE ON brokens FOR EACH ROW EXECUTE PROCEDURE update_rm();


--
-- Name: trg_update_total_fee; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trg_update_total_fee BEFORE INSERT ON trade_results FOR EACH ROW EXECUTE PROCEDURE calculate_total_fee_in_trade_result();


--
-- Name: update_tt_activities_description; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER update_tt_activities_description BEFORE INSERT ON tt_event_log FOR EACH ROW EXECUTE PROCEDURE _insert_tt_activities();


--
-- Name: brokens_oid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY brokens
    ADD CONSTRAINT brokens_oid_fkey FOREIGN KEY (oid, exec_id) REFERENCES fills(oid, exec_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

