BEGIN;

-- Downgrade TT version
DELETE FROM release_versions WHERE module_id = 2 AND version = '20130606';

-- Delete 2 rows (TS7 + TS8) into trade_servers table
DELETE FROM trade_servers
	WHERE ts_id = 7 OR ts_id = 8;

COMMIT;