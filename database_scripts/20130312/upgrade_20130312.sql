BEGIN;
CREATE OR REPLACE FUNCTION _insert_tt_activities()
  RETURNS trigger AS
$BODY$
    DECLARE
		userVal                 text;
		typeVal                 integer;
		dateVal                 date;
		timeVal                 time;
		currentState    text[];
		_currentState   integer;
		status                  text[];
		_status                 integer;
		action                  text[];
		_action                 integer;
		orderId                 text;
		actionType              integer;
		symbol                  text;
		buyingPower     double precision;
		onWhich                 text[];
		_onWhich                integer;
		descriptionVal  text;
		_tsId			integer;
		_tsDescription  text;
		_maxShares		integer;
		_maxConsectutiveLoss integer;
		_maxStuck		integer;
		_count			integer;
		_activityAction text;
		_minSpreadTS	text;
		_maxLoserTS		text;
		_maxBuyingPower text;
		_checkUserOrServer integer;
	
	BEGIN
		timeVal := NEW.time_stamp;
		userVal := initcap(trim(NEW.username));
		typeVal = NEW.activity_type;
		dateVal = (NEW.date);
		timeVal = (NEW.time_stamp);

		--Init status
		status[0] := 'Off-Line';
		status[1] := 'Active';
		status[2] := 'Active Assistant';
		status[3] := 'Monitoring';

		currentState[0] := '[Off-Line]';
		currentState[1] := '[Active]';
		currentState[2] := '[Active Assistant]';
		currentState[3] := '[Monitoring]';

		--Init action
		action[0] := 'Add';
		action[1] := 'Remove';

		--Init enable/disable trading
		onWhich[100] := 'all trading on TS1';
		onWhich[101] := 'ARCA trading on TS1';
		onWhich[102] := 'OUCH trading on TS1';
		onWhich[103] := 'NYSE trading on TS1';
		onWhich[104] := 'RASH trading on TS1';
		onWhich[105] := 'BATZ trading on TS1';

		onWhich[200] := 'all trading on TS2';
		onWhich[201] := 'ARCA trading on TS2';
		onWhich[202] := 'OUCH trading on TS2';
		onWhich[203] := 'NYSE trading on TS2';
		onWhich[204] := 'RASH trading on TS2';
		onWhich[205] := 'BATZ trading on TS2';

		onWhich[300] := 'all trading on TS3';
		onWhich[301] := 'ARCA trading on TS3';
		onWhich[302] := 'OUCH trading on TS3';
		onWhich[303] := 'NYSE trading on TS3';
		onWhich[304] := 'RASH trading on TS3';
		onWhich[305] := 'BATZ trading on TS3';

		onWhich[400] := 'all trading on TS4';
		onWhich[401] := 'ARCA trading on TS4';
		onWhich[402] := 'OUCH trading on TS4';
		onWhich[403] := 'NYSE trading on TS4';
		onWhich[404] := 'RASH trading on TS4';
		onWhich[405] := 'BATZ trading on TS4';

		onWhich[500] := 'all trading on TS5';
		onWhich[501] := 'ARCA trading on TS5';
		onWhich[502] := 'OUCH trading on TS5';
		onWhich[503] := 'NYSE trading on TS5';
		onWhich[504] := 'RASH trading on TS5';
		onWhich[505] := 'BATZ trading on TS5';

		onWhich[600] := 'all trading on TS6';
		onWhich[601] := 'ARCA trading on TS6';
		onWhich[602] := 'OUCH trading on TS6';
		onWhich[603] := 'NYSE trading on TS6';
		onWhich[604] := 'RASH trading on TS6';
		onWhich[605] := 'BATZ trading on TS6';

		onWhich[700] := 'PM trading';

		--Init connect/disconnect all books and orders, enable/disable trading all orders on all TS
		onWhich[0] := 'trading all orders on all TSs and PM';
		onWhich[1] := 'all books and orders on all TSs and PM';

		--Init connect/disconnect BBO
		onWhich[110] := 'BBO on TS1';
		onWhich[111] := 'CQS on TS1';
		onWhich[112] := 'UQDF on TS1';

		onWhich[210] := 'BBO on TS2';
		onWhich[211] := 'CQS on TS2';
		onWhich[212] := 'UQDF on TS2';

		onWhich[310] := 'BBO on TS3';
		onWhich[311] := 'CQS on TS3';
		onWhich[312] := 'UQDF on TS3';

		onWhich[410] := 'BBO on TS4';
		onWhich[411] := 'CQS on TS4';
		onWhich[412] := 'UQDF on TS4';

		onWhich[510] := 'BBO on TS5';
		onWhich[511] := 'CQS on TS5';
		onWhich[512] := 'UQDF on TS5';

		onWhich[610] := 'BBO on TS6';
		onWhich[611] := 'CQS on TS6';
		onWhich[612] := 'UQDF on TS6';

		onWhich[710] := 'BBO on PM';
		onWhich[711] := 'CTS on PM';
		onWhich[712] := 'UTDF on PM';
		onWhich[713] := 'CQS on PM';
		onWhich[714] := 'UQDF on PM';

		--Init connect/disconnect book
		onWhich[120] := 'all books on TS1';
		onWhich[121] := 'ARCA book on TS1';
		onWhich[122] := 'NDAQ book on TS1';
		onWhich[123] := 'NYSE book on TS1';
		onWhich[124] := 'BATS book on TS1';

		onWhich[220] := 'all books on TS2';
		onWhich[221] := 'ARCA book on TS2';
		onWhich[222] := 'NDAQ book on TS2';
		onWhich[223] := 'NYSE book on TS2';
		onWhich[224] := 'BATS book on TS2';

		onWhich[320] := 'all books on TS3';
		onWhich[321] := 'ARCA book on TS3';
		onWhich[322] := 'NDAQ book on TS3';
		onWhich[323] := 'NYSE book on TS3';
		onWhich[324] := 'BATS book on TS3';

		onWhich[420] := 'all books on TS4';
		onWhich[421] := 'ARCA book on TS4';
		onWhich[422] := 'NDAQ book on TS4';
		onWhich[423] := 'NYSE book on TS4';
		onWhich[424] := 'BATS book on TS4';

		onWhich[520] := 'all books on TS5';
		onWhich[521] := 'ARCA book on TS5';
		onWhich[522] := 'NDAQ book on TS5';
		onWhich[523] := 'NYSE book on TS5';
		onWhich[524] := 'BATS book on TS5';

		onWhich[620] := 'all books on TS6';
		onWhich[621] := 'ARCA book on TS6';
		onWhich[622] := 'NDAQ book on TS6';
		onWhich[623] := 'NYSE book on TS6';
		onWhich[624] := 'BATS book on TS6';

		onWhich[720] := 'all books on PM';
		onWhich[721] := 'ARCA book on PM';
		onWhich[722] := 'NDAQ book on PM';

		--Init connect/disconnect order
		onWhich[130] := 'all orders on TS1';
		onWhich[131] := 'ARCA DIRECT on TS1';
		onWhich[132] := 'NDAQ OUCH on TS1';
		onWhich[133] := 'NYSE CCG on TS1';
		onWhich[134] := 'NDAQ RASH on TS1';
		onWhich[135] := 'BATZ BOE on TS1';

		onWhich[230] := 'all orders on TS2';
		onWhich[231] := 'ARCA DIRECT on TS2';
		onWhich[232] := 'NDAQ OUCH on TS2';
		onWhich[233] := 'NYSE CCG on TS2';
		onWhich[234] := 'NDAQ RASH on TS2';
		onWhich[235] := 'BATZ BOE on TS2';

		onWhich[330] := 'all orders on TS3';
		onWhich[331] := 'ARCA DIRECT on TS3';
		onWhich[332] := 'NDAQ OUCH on TS3';
		onWhich[333] := 'NYSE CCG on TS3';
		onWhich[334] := 'NDAQ RASH on TS3';
		onWhich[335] := 'BATZ BOE on TS3';

		onWhich[430] := 'all orders on TS4';
		onWhich[431] := 'ARCA DIRECT on TS4';
		onWhich[432] := 'NDAQ OUCH on TS4';
		onWhich[433] := 'NYSE CCG on TS4';
		onWhich[434] := 'NDAQ RASH on TS4';
		onWhich[435] := 'BATZ BOE on TS4';

		onWhich[530] := 'all orders on TS5';
		onWhich[531] := 'ARCA DIRECT on TS5';
		onWhich[532] := 'NDAQ OUCH on TS5';
		onWhich[533] := 'NYSE CCG on TS5';
		onWhich[534] := 'NDAQ RASH on TS5';
		onWhich[535] := 'BATZ BOE on TS5';

		onWhich[630] := 'all orders on TS6';
		onWhich[631] := 'ARCA DIRECT on TS6';
		onWhich[632] := 'NDAQ OUCH on TS6';
		onWhich[633] := 'NYSE CCG on TS6';
		onWhich[634] := 'NDAQ RASH on TS6';
		onWhich[635] := 'BATZ BOE on TS6';

		onWhich[10] := 'all orders on AS';
		onWhich[11] := 'ARCA FIX on AS';
		onWhich[12] := 'NDAQ RASH on AS';
		
		onWhich[50] := 'all orders on PM';
		onWhich[51] := 'ARCA FIX on PM';
		onWhich[52] := 'NDAQ RASH on PM';

		-- Connect to AS
		IF (typeVal = 1) THEN
			SELECT substring(NEW.log_detail from 1 for 1)::integer INTO _currentState FROM tt_event_log;
			descriptionVal := userVal || ' ' || currentState[_currentState] || ' Connect to AS ' || dateVal || ' ' || timeVal;

		-- Switch trader role
		ELSIF (typeVal = 2) THEN
			SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer INTO _currentState, _status FROM tt_event_log;
			descriptionVal := userVal || ' ' || currentState[_currentState] || ' Client became ' || status[_status] || ' Trader ' || dateVal || ' ' || timeVal;

		-- Disconnect from AS
		ELSIF (typeVal = 3) THEN
			SELECT substring(NEW.log_detail from 1 for 1)::integer INTO _currentState FROM tt_event_log;
			descriptionVal := userVal || ' ' || currentState[_currentState] || ' Disconnect from AS ' || dateVal || ' ' || timeVal;

		-- Sent Manual order
		ELSIF (typeVal = 4) THEN
			SELECT substring(NEW.log_detail from 1 for 1)::integer, trim(substring(NEW.log_detail from 3 for 3)) INTO _currentState, orderId FROM tt_event_log;
			descriptionVal := userVal || ' ' || currentState[_currentState] || ' Sent Manual Order, OrderID = ' || orderId || ' ' || dateVal || ' ' || timeVal;

		-- Ignored stock list for TS
		ELSIF (typeVal = 5) THEN
			SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, trim(substring(NEW.log_detail from 5 for 8)) INTO _currentState, _action, symbol FROM tt_event_log;
			descriptionVal := userVal || ' ' || currentState[_currentState] || ' ' || action[_action] || ' ignored stock list for TS, symbol = ' || symbol || ' ' || dateVal || ' ' || timeVal;

		-- Halted stock list
		ELSIF (typeVal = 6) THEN
			SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, trim(substring(NEW.log_detail from 5 for 8)) INTO _currentState, _action, symbol FROM tt_event_log;
			descriptionVal := userVal || ' ' || currentState[_currentState] || ' ' || action[_action] || ' halted stock list, symbol = ' || symbol || ' ' || dateVal || ' ' || timeVal;

		-- PM engage/disengage
		ELSIF (typeVal = 7) THEN
			SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, trim(substring(NEW.log_detail from 5 for 8)) INTO _currentState, actionType, symbol FROM tt_event_log;
			-- engage
			IF (actionType = 0) THEN
				descriptionVal := userVal || ' ' || currentState[_currentState] || ' PM engage, symbol = ' || symbol || ' ' || dateVal || ' ' || timeVal;
			-- disengage
			ELSIF (actionType = 1) THEN
				descriptionVal := userVal || ' ' || currentState[_currentState] || ' PM disengage, symbol = ' || symbol || ' ' || dateVal || ' ' || timeVal;
			ELSE
			END IF;

		-- set buying power
		-- use _count variable for accountIndex
		ELSIF (typeVal = 8) THEN
			SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1):: integer, substring(NEW.log_detail, 5)::double precision INTO _currentState, _count, buyingPower FROM tt_event_log;
			
			IF (_count = 0) THEN
				_tsDescription := ' on first account ';
			ELSIF (_count = 1) THEN
				_tsDescription := ' on second account ';
			ELSE
				_tsDescription := ' on third account ';
			END IF;
				
			descriptionVal := userVal || ' ' || currentState[_currentState] || ' Set Buying Power = ' || buyingPower || _tsDescription || ' ' || dateVal || ' ' || timeVal;

		-- Enable/Disable trading
		ELSIF (typeVal = 9) THEN
			SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, substring(NEW.log_detail from 5 for 3) INTO _currentState, actionType, _onWhich FROM tt_event_log;
			-- Enable
			IF (actionType = 0) THEN
				descriptionVal := userVal || ' ' || currentState[_currentState] || ' Enable ' || onWhich[_onWhich] || ' ' || dateVal || ' ' || timeVal;
			-- Disable
			ELSIF (actionType = 1) THEN
				descriptionVal := userVal || ' ' || currentState[_currentState] || ' Disable ' || onWhich[_onWhich] || ' ' || dateVal || ' ' || timeVal;
			ELSE
			END IF;

		-- Ignore stock list for PM
		ELSIF (typeVal = 10) THEN
			SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, trim(substring(NEW.log_detail from 5 for 8)) INTO _currentState, _action, symbol FROM tt_event_log;
			descriptionVal := userVal || ' ' || currentState[_currentState] || ' ' || action[_action] || ' ignored stock list for PM, symbol = ' || symbol || ' ' || dateVal || ' ' || timeVal;

		-- Connect/disconnect book(s) on TS and PM
		ELSIF (typeVal = 11) THEN
			SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, substring(NEW.log_detail from 5 for 3) INTO _currentState, actionType, _onWhich FROM tt_event_log;
			-- Connect
			IF (actionType = 0) THEN
				descriptionVal := userVal || ' ' || currentState[_currentState] || ' Connect to ' || onWhich[_onWhich] || ' ' || dateVal || ' ' || timeVal;
			-- Disconnect
			ELSIF (actionType = 1) THEN
				descriptionVal := userVal || ' ' || currentState[_currentState] || ' Disconnect and disable trading ' || onWhich[_onWhich] || ' ' || dateVal || ' ' || timeVal;
			ELSE
			END IF;

		-- Connect/disconnect order(s) on TS, AS and PM
		ELSIF (typeVal = 12) THEN
			SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, substring(NEW.log_detail from 5 for 3) INTO _currentState, actionType, _onWhich FROM tt_event_log;
			-- Connect
			IF (actionType = 0) THEN
				 descriptionVal := userVal || ' ' || currentState[_currentState] || ' Connect to ' || onWhich[_onWhich] || ' ' || dateVal || ' ' || timeVal;
			-- Disconnect
			ELSIF (actionType = 1) THEN
				 descriptionVal := userVal || ' ' || currentState[_currentState] || ' Disconnect and disable trading ' || onWhich[_onWhich] || ' ' || dateVal || ' ' || timeVal;
			ELSE
			END IF;

		-- connect/disconnect BBO on TS and PM
		ELSIF (typeVal = 13) THEN
			SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, substring(NEW.log_detail from 5 for 3) INTO _currentState, actionType, _onWhich FROM tt_event_log;
			-- Connect
			IF (actionType = 0) THEN
				 descriptionVal := userVal || ' ' || currentState[_currentState] || ' Connect to ' || onWhich[_onWhich] || ' ' || dateVal || ' ' || timeVal;
			-- Disconnect
			ELSIF (actionType = 1) THEN
				 descriptionVal := userVal || ' ' || currentState[_currentState] || ' Disconnect ' || onWhich[_onWhich] || ' ' || dateVal || ' ' || timeVal;
			ELSE
			END IF;
			
		-- enable/disable ISO trading on TS
		ELSIF (typeVal = 20) THEN
			SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer, substring(NEW.log_detail, 5) INTO _currentState, actionType, _tsId FROM tt_event_log;
			
			IF (_tsId = -1) THEN
				-- Enable
				IF (actionType = 1) THEN
					descriptionVal := userVal || ' ' || currentState[_currentState] || ' Enable ISO trading' || ' ' || dateVal || ' ' || timeVal;
				-- Disable
				ELSIF (actionType = 0) THEN
					descriptionVal := userVal || ' ' || currentState[_currentState] || ' Disable ISO trading' || ' ' || dateVal || ' ' || timeVal;
				ELSE
				END IF;
			ELSE
				-- Enable
				IF (actionType = 1) THEN
					descriptionVal := userVal || ' ' || currentState[_currentState] || ' Enable ISO trading on TS' || _tsId || ' ' || dateVal || ' ' || timeVal;
				-- Disable
				ELSIF (actionType = 0) THEN
					descriptionVal := userVal || ' ' || currentState[_currentState] || ' Disable ISO trading on TS' || _tsId || ' ' || dateVal || ' ' || timeVal;
				ELSE
				END IF;
			END IF;
		
		ELSE
		
			SELECT substring(NEW.log_detail from 1 for 1)::integer, substring(NEW.log_detail from 3 for 1)::integer INTO _currentState, _tsId FROM tt_event_log;
			
			IF (_tsId = 1) THEN _tsDescription := ' on TS1 ';
			ELSIF (_tsId = 2) THEN _tsDescription := ' on TS2 ';
			ELSIF (_tsId = 3) THEN _tsDescription := ' on TS3 ';
			ELSIF (_tsId = 4) THEN _tsDescription := ' on TS4 ';
			ELSIF (_tsId = 5) THEN _tsDescription := ' on TS5 ';
			ELSIF (_tsId = 6) THEN _tsDescription := ' on TS6 ';
			ELSE
			END IF;
			
			SELECT count(*) INTO _checkUserOrServer FROM user_login_info WHERE username = NEW.username;
			
			-- Check activity type was inserted on SOD or NOT
			SELECT count(*) INTO _count FROM tt_event_log WHERE date = dateVal AND activity_type = typeVal AND username = NEW.username;
			
			-- ACTIVITY_TYPE_MAX_CONSECUTIVE_LOSS_SETTING 14
			IF (typeVal = 14) THEN
				IF (_checkUserOrServer = 0) THEN
					IF (_count = 0) THEN
						_activityAction := ' SOD: Set max consecutive loss = ';
					ELSE RETURN OLD;
					END IF;
				ELSE
					_activityAction := ' Set max consecutive loss = ';
				END IF;
				
				SELECT substring(NEW.log_detail, 5) INTO _maxConsectutiveLoss FROM tt_event_log ;
		
				descriptionVal := userVal || ' ' || currentState[_currentState] || _activityAction ||  _maxConsectutiveLoss || _tsDescription || dateVal || ' ' || timeVal;
			
			-- ACTIVITY_TYPE_MAX_STUCKS_SETTING 15
			ELSIF (typeVal = 15) THEN
				IF (_checkUserOrServer = 0) THEN
					IF (_count = 0) THEN
						_activityAction := ' SOD: Set max stucks = ';
					ELSE RETURN OLD;
					END IF;
				ELSE
					_activityAction := ' Set max stucks = ';
				END IF;

				SELECT substring(NEW.log_detail, 5) INTO _maxStuck FROM tt_event_log;
			
				descriptionVal := userVal || ' ' || currentState[_currentState] || _activityAction || _maxStuck || _tsDescription || dateVal || ' ' || timeVal;
			
			-- ACTIVITY_TYPE_CURRENCY_MIN_SPREAD_TS_SETTING 16
			ELSIF (typeVal = 16) THEN
				IF (_checkUserOrServer = 0) THEN
					IF (_count = 0) THEN
						_activityAction := ' SOD: Set min spread = ';
					ELSE
						_activityAction := ' Set min spread = ';
					END IF;
				ELSE
					_activityAction := ' Set min spread = ';
				END IF;
			
				SELECT substring(NEW.log_detail, 5) INTO _minSpreadTS FROM tt_event_log;
				
				descriptionVal := userVal || ' ' || currentState[_currentState] || _activityAction || _minSpreadTS || _tsDescription || dateVal || ' ' || timeVal;
		
			-- ACTIVITY_TYPE_CURRENCY_MAX_LOSER_TS_SETTING 17
			ELSIF (typeVal = 17) THEN
				IF (_checkUserOrServer = 0) THEN
					IF (_count = 0) THEN
						_activityAction := ' SOD: Set max loser = ';
					ELSE RETURN OLD;
					END IF;
				ELSE
					_activityAction := ' Set max loser = ';
				END IF;
			
				SELECT substring(NEW.log_detail, 5) INTO _maxLoserTS FROM tt_event_log;
			
				descriptionVal := userVal || ' ' || currentState[_currentState] || _activityAction || _maxLoserTS || _tsDescription || dateVal || ' ' || timeVal;
			
			-- ACTIVITY_TYPE_MAX_BUYING_POWER_SETTING 18
			ELSIF (typeVal = 18) THEN
				IF (_checkUserOrServer = 0) THEN
					IF (_count = 0) THEN
						_activityAction := ' SOD: Set max buying power = ';
					ELSE RETURN OLD;
					END IF;
				ELSE
					_activityAction := ' Set max buying power = ';
				END IF;
			
				SELECT substring(NEW.log_detail, 5) INTO _maxBuyingPower FROM tt_event_log;
			
				descriptionVal := userVal || ' ' || currentState[_currentState] || _activityAction || _maxBuyingPower || _tsDescription || dateVal || ' ' || timeVal;
			
			-- ACTIVITY_TYPE_MAX_SHARES_SETTING 19
			ELSIF (typeVal = 19) THEN
				IF (_checkUserOrServer = 0) THEN
					IF (_count = 0) THEN
						_activityAction := ' SOD: Set max shares = ';
					ELSE RETURN OLD;
					END IF;
				ELSE
					_activityAction := ' Set max shares = ';
				END IF;
			
				SELECT substring(NEW.log_detail, 5) INTO _maxShares FROM tt_event_log;
			
				descriptionVal := userVal || ' ' || currentState[_currentState] || _activityAction || _maxShares || _tsDescription || dateVal || ' ' || timeVal;
			
			ELSE
			END IF;

		END IF;
		NEW.description := descriptionVal;
		RETURN NEW;
	END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION _insert_tt_activities()
  OWNER TO postgres;
  
-- Function: calculate_total_fee(double precision, integer, character, character, text)

-- DROP FUNCTION calculate_total_fee(double precision, integer, character, character, text);

CREATE OR REPLACE FUNCTION calculate_total_fee(price double precision, shares integer, side character, liquidity character, ecn text)
  RETURNS double precision AS
$BODY$
	DECLARE
		sec_fee double precision;
		taf_fee double precision;
		wedbus_commission double precision;
		add_venue_fee_with_price_larger_than_1 double precision;
		add_venue_fee_with_price_smaller_than_1 double precision;
		remove_venue_fee_with_price_larger_than_1 double precision;
		remove_venue_fee_with_price_smaller_than_1 double precision;
		auction_venue_fee double precision;
		ARCA_route_venue_fee double precision;
		NDAQ_route_venue_fee double precision;
		BATS_route_venue_fee double precision;
		
		totalVenueFee double precision;
		totalSecFees double precision;
		totalTafFee double precision;
		totalFee double precision;
		totalWedbushCommission double precision;
	BEGIN
		-- If we changed some fees in fee_config file on AS or TA, we would change those fees here
		sec_fee := 0.00002240;
		taf_fee := 0.00009;
		wedbus_commission := 0.000035;
		add_venue_fee_with_price_larger_than_1 := -0.0028;
		add_venue_fee_with_price_smaller_than_1 := 0.00;
		remove_venue_fee_with_price_larger_than_1 := 0.003;
		remove_venue_fee_with_price_smaller_than_1 := 0.003;
		auction_venue_fee := 0.0005;
		ARCA_route_venue_fee := 0.003;
		NDAQ_route_venue_fee := 0.0035;
		BATS_route_venue_fee := 0.002;
		
		-- Initialize fees
		totalVenueFee := 0.00;
		totalSecFees := 0.00;
		totalTafFee := 0.00;
		totalFee := 0.00;
		totalWedbushCommission := 0.00;
		-- Calculate total fee
		if (side = 'S') then
			totalSecFees := shares * price * sec_fee;
		end if;
		
		totalTafFee := shares * taf_fee;
		totalWedbushCommission := shares * wedbus_commission;
		
		if (ecn = 'ARCA') then
			-- Add
			if (liquidity = 'A' or liquidity = 'B' or liquidity = 'M' or liquidity = 'D' or liquidity = 'S') then
				if (price >= 1.00) then
					totalVenueFee := shares * add_venue_fee_with_price_larger_than_1;
				else
					totalVenueFee := shares * add_venue_fee_with_price_smaller_than_1;
				end if;
			-- Remove
			elsif (liquidity = 'R' or liquidity = 'E' or liquidity = 'L') then
				if (price >= 1.00) then
					totalVenueFee := shares * remove_venue_fee_with_price_larger_than_1;
				else
					totalVenueFee := shares * price * remove_venue_fee_with_price_smaller_than_1;
				end if;
			-- Auction
			elsif (liquidity = 'G' or liquidity = 'Z') then
				totalVenueFee := shares * auction_venue_fee;
			elsif (liquidity = 'X' or liquidity = 'F' or liquidity = 'N' or liquidity = 'H' or liquidity = 'C' or liquidity = 'U' or liquidity = 'Y' or liquidity = 'W') then
				totalVenueFee := shares * ARCA_route_venue_fee;
			end if;
			
		elsif (ecn = 'OUCH') then
			-- Add
			if (liquidity = 'A' or liquidity = 'k' or liquidity = 'J' or liquidity = '7' or liquidity = '8' or liquidity = 'e' or liquidity = 'f') then
				if (price >= 1.00) then
					totalVenueFee := shares * add_venue_fee_with_price_larger_than_1;
				else
					totalVenueFee := shares * add_venue_fee_with_price_smaller_than_1;
				end if;
			-- Remove
			elsif (liquidity = 'R' or liquidity = 'm' or liquidity = '6' or liquidity = 'd') then
				if (price >= 1.00) then
					totalVenueFee := shares * remove_venue_fee_with_price_larger_than_1;
				else
					totalVenueFee := shares * price * remove_venue_fee_with_price_smaller_than_1;
				end if;
			-- Auction
			elsif (liquidity = 'O' or liquidity = 'M' or liquidity = 'L' or liquidity = 'H' or liquidity = 'K' or liquidity = 'C') then
				totalVenueFee := shares * auction_venue_fee;
			end if;
		
		elsif (ecn = 'RASH') then
			-- Add
			if (liquidity = 'A' or liquidity = 'J' or liquidity = 'V' or liquidity = 'D' or liquidity = 'F' or liquidity = 'U' or liquidity = 'k' or liquidity = '9' or liquidity = '7' or liquidity = '8' or liquidity = 'e' or liquidity = 'f') then
				if (price >= 1.00) then
					totalVenueFee := shares * add_venue_fee_with_price_larger_than_1;
				else
					totalVenueFee := shares * add_venue_fee_with_price_smaller_than_1;
				end if;
			-- Remove
			elsif (liquidity = 'R' or liquidity = 'm' or liquidity = 'd' or liquidity = '6') then
				if (price >= 1.00) then
					totalVenueFee := shares * remove_venue_fee_with_price_larger_than_1;
				else
					totalVenueFee := shares * price * remove_venue_fee_with_price_smaller_than_1;
				end if;		
			-- Auction
			elsif (liquidity = 'G' or liquidity = 'O' or liquidity = 'M' or liquidity = 'C' or liquidity = 'L' or liquidity = 'H' or liquidity = 'K' or liquidity = 'I' or liquidity = 'T' or liquidity = 'Z') then
				totalVenueFee := shares * auction_venue_fee;
			-- Route venue fee
			elsif (liquidity = 'X' or liquidity = 'Y' or liquidity = 'B' or liquidity = 'P' or liquidity = 'Q') then
				totalVenueFee := shares * NDAQ_route_venue_fee;
			end if;

		elsif (ecn = 'NYSE') then
			-- Add (provider)
			if (liquidity = '2' or liquidity = '8') then
				if (price >= 1.00) then
					totalVenueFee := shares * add_venue_fee_with_price_larger_than_1;
				else
					totalVenueFee := shares * add_venue_fee_with_price_smaller_than_1;
				end if;
			-- Remove (taker and blended)
			elsif (liquidity = '1' or liquidity = '9' or liquidity = '3' or liquidity = ' ') then
				if (price >= 1.00) then
					totalVenueFee := shares * remove_venue_fee_with_price_larger_than_1;
				else
					totalVenueFee := shares * price * remove_venue_fee_with_price_smaller_than_1;
				end if;
			-- Auction
			elsif (liquidity = '4' or liquidity = '5' or liquidity = '6' or liquidity = '7' ) then
				totalVenueFee := shares * auction_venue_fee;
			end if;
			
		elsif (ecn = 'BATZ') then
			-- Add (provider)
			if (liquidity = 'A') then
				if (price >= 1.00) then
					totalVenueFee := shares * add_venue_fee_with_price_larger_than_1;
				else
					totalVenueFee := shares * add_venue_fee_with_price_smaller_than_1;
				end if;
			-- Remove (taker and blended)
			elsif (liquidity = 'R') then
				if (price >= 1.00) then
					totalVenueFee := shares * remove_venue_fee_with_price_larger_than_1;
				else
					totalVenueFee := shares * price * remove_venue_fee_with_price_smaller_than_1;
				end if;
			-- Auction
			elsif (liquidity = 'C' ) then
				totalVenueFee := shares * auction_venue_fee;
			-- Route
			elsif (liquidity = 'X' ) then
				totalVenueFee := shares * BATS_route_venue_fee;
			end if;
		end if;
		
		totalFee := totalSecFees + totalTafFee + totalWedbushCommission + totalVenueFee;
		return totalFee;
		
	END $BODY$
  LANGUAGE plpgsql VOLATILE;
  
ALTER FUNCTION calculate_total_fee(double precision, integer, character, character, text)
  OWNER TO postgres;
  
  
DROP FUNCTION get_mutiple_exchanges_trade(date, date);

DROP FUNCTION cal_filter_trade_statistic_byecn2_twt2(date, date, time without time zone, time without time zone, integer, double precision, text, integer, integer, text, integer, text, text);

DROP FUNCTION cal_filter_trade_statistic_byecn2_twt2_notime(date, date, integer, double precision, text, integer, integer, text, integer, text, text);

DROP FUNCTION cal_filter_trade_statistic_byecn2_twt2_with_step(date, date, time without time zone, time without time zone, integer, double precision, text, integer, integer, text, integer, text, integer, text);

DROP FUNCTION cal_trade_statistic_date_range(date, date, time without time zone, time without time zone, integer, double precision, text, integer, integer, text, integer, text, text);

-- Function: get_mutiple_exchanges_trade(date, date)

-- DROP FUNCTION get_mutiple_exchanges_trade(date, date);

CREATE OR REPLACE FUNCTION get_mutiple_exchanges_trade(date_from date, date_to date)
  RETURNS text AS
$BODY$ 
declare
ret text;
BEGIN
  ret ='(-1)';

  return ret;
END
$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION get_mutiple_exchanges_trade(date, date)
  OWNER TO postgres;
  
  

-- Function: cal_filter_trade_statistic_byecn2_twt2(date, date, time without time zone, time without time zone, integer, double precision, text, integer, integer, text, integer, text, text, integer)

-- DROP FUNCTION cal_filter_trade_statistic_byecn2_twt2(date, date, time without time zone, time without time zone, integer, double precision, text, integer, integer, text, integer, text, text, integer);

CREATE OR REPLACE FUNCTION cal_filter_trade_statistic_byecn2_twt2(to_date date, from_date date, time_from time without time zone, time_to time without time zone, share_size integer, expected_profit double precision, symb text, bt_shares integer, sd_shares integer, ecn_filter text, ts_id integer, cecn text, is_iso text, exclusive_val integer)
  RETURNS trade_result_summary2 AS
$BODY$ 
declare
  trade_result record;
  summary trade_result_summary2;
  shares_filled integer;
  shares_stuck integer;
  profit_loss double precision;
 --statistic missed trade 
  count_ouch_missed_trade integer;
  sum_ouch_expected_pl double precision;
  missed_trade_record_of_ouch record;
  str_sql text;
  str_sql_no text;
  str_sql_multi_orders text;
	str_sql_tmp text;
	tmp_str text;
  
begin	
  -- Initialize
  summary.count_miss := 0;
  summary.count_repriced := 0;
  summary.count_stuck_loss := 0;
  summary.count_stuck_profit := 0;
  summary.count_loss := 0;
  summary.count_profit_under := 0;
  summary.count_profit_above := 0;
  summary.count_total := 0;
  summary.expected_miss := 0.00;
  summary.expected_repriced := 0.00;
  summary.expected_stuck_loss := 0.00;
  summary.expected_stuck_profit := 0.00;
  summary.expected_loss := 0.00;
  summary.expected_profit_under := 0.00;
  summary.expected_profit_above := 0.00;
  summary.expected_total := 0.00;
  summary.real_miss := 0.00;
  summary.real_stuck_loss := 0.00;
  summary.real_stuck_profit := 0.00;
  summary.real_loss = 0.00;
  summary.real_profit_under := 0.00;
  summary.real_profit_above := 0.00;
  summary.real_total := 0.00;


	--Calculate repriced
 IF bt_shares = 0 AND sd_shares = 0 THEN
    --Select data from view "vwouch_missed_trades" and calculate total missed trade 	
	str_sql:='SELECT count(*) as count_missed, sum(expected_pl)as sum_expected_pl  FROM vwtrade_results WHERE date >='''|| to_date || ''' and date <='''|| from_date ||''' and expected_shares >='|| share_size ||' and expected_pl >=' || expected_profit ;
	str_sql := str_sql || ' and bought_shares < 1 and sold_shares < 1 ';
		--filter by ts when we get data from view vwtrade_results
		IF ts_id > -1 THEN
			str_sql:=str_sql || ' and ts_id =' || ts_id;
		END IF;
	str_sql_no :='SELECT (ts_id * 1000000 + cross_id) as tmpcol FROM new_orders WHERE date >='''|| to_date || ''' and date <='''|| from_date ||'''';
	str_sql_no :=str_sql_no || ' and time_stamp between ''' || time_from || ''' and '''|| time_to || '''';
	str_sql_no :=str_sql_no || ' and ecn =''OUCH'' and price != ack_price ';
		--filter by ts, ecn_launch when we get data from table new_orders
		IF ts_id > -1 THEN
			str_sql_no:=str_sql_no || ' and ts_id =' || ts_id;
		END IF;
		IF cecn <> '0' THEN
			str_sql_no:=str_sql_no || ' and ecn_launch =''' || cecn ||'''';
		END IF;
	IF ecn_filter = 'arcaen' THEN
		str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Entry''' ;
	ELSIF ecn_filter = 'arcaex' THEN
		str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Exit''' ;
	ELSIF ecn_filter = 'ouchen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'ouchex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Exit''';
	ELSIF ecn_filter = 'rashen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'rashex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Exit''';		
	ELSIF ecn_filter = 'batzen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Entry''';	
	ELSIF ecn_filter = 'batzex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Exit''';	
	ELSIF ecn_filter = 'oubxen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''OUBX'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'oubxex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''OUBX'' and entry_exit=''Exit''';
	ELSIF ecn_filter = 'rabxen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''RABX'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'rabxex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''RABX'' and entry_exit=''Exit''';
	ELSIF ecn_filter = 'nyseen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'nyseex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Exit''';
	END IF;
	
	--Filter by ISO Odrer
	IF is_iso = '1' AND from_date > '2010-01-01' THEN
		str_sql_no:=str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
	END IF;
	
	-- tmp_str := str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
	
	IF is_iso = '2' AND from_date > '2010-01-01' THEN
		-- str_sql_no:=str_sql_no || ' and (is_iso <> ''Y'' and (ts_id * 1000000 + cross_id) not in (' || tmp_str || '))';
		str_sql_no:=str_sql_no || ' and (is_iso <> ''Y'')';
	END IF;

	IF symb <>'' THEN
		str_sql:=str_sql || ' and symbol='''|| symb ||'''';
		str_sql_no:=str_sql_no || ' and symbol='''|| symb ||'''';
	END IF;

		str_sql_no:=str_sql_no || ' GROUP BY (ts_id * 1000000 + cross_id)' ;

	IF bt_shares > 0 THEN
		str_sql:=str_sql || ' and bought_shares >=' || bt_shares;
	END IF;
	IF sd_shares > 0 THEN
		str_sql:=str_sql || ' and sold_shares >=' || sd_shares;
	END IF;

	str_sql:=str_sql || ' and (ts_id * 1000000 + ts_cid) in ('|| str_sql_no ||')';

	IF ecn_filter <> '0' AND exclusive_val = 1 THEN
		str_sql_multi_orders := get_query_string_array_id_multi_orders(to_date, from_date);
		str_sql_tmp := get_mutiple_exchanges_trade(to_date, from_date);
		str_sql :=str_sql || ' and (ts_id * 1000000 + ts_cid) not in (' || str_sql_multi_orders || ')'|| ' and (ts_id * 1000000 + ts_cid) not in (' || str_sql_tmp || ')';
	END IF;
	
	for missed_trade_record_of_ouch in EXECUTE str_sql loop
		count_ouch_missed_trade := missed_trade_record_of_ouch.count_missed;
		sum_ouch_expected_pl := missed_trade_record_of_ouch.sum_expected_pl;
	end loop;

	   IF count_ouch_missed_trade > 0 THEN
		summary.count_repriced := count_ouch_missed_trade;
		summary.expected_repriced := sum_ouch_expected_pl;

		summary.count_miss := -count_ouch_missed_trade;
		summary.expected_miss :=  -sum_ouch_expected_pl;
	   END IF;

  END IF;
	 
--filter by  ECN by Entry/Exit
  str_sql:='SELECT * FROM vwtrade_results WHERE date >='''|| to_date || ''' and date <='''|| from_date ||''' and expected_shares >='|| share_size ||' and expected_pl >=' || expected_profit ;
  str_sql_no :='SELECT (ts_id * 1000000 + cross_id) as tmpcol FROM new_orders WHERE date >='''|| to_date || ''' and date <='''|| from_date ||'''';
  str_sql_no :=str_sql_no || ' and time_stamp between ''' || time_from || ''' and '''|| time_to || '''';
	--filter by ts, ecn launch
	IF ts_id > -1 THEN
		str_sql_no:=str_sql_no || ' and ts_id =' || ts_id;
	END IF;
	IF cecn <> '0' THEN
		str_sql_no:=str_sql_no || ' and ecn_launch =''' || cecn ||'''';
	END IF;
	
	IF ecn_filter = 'arcaen' THEN
		str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Entry''' ;
	ELSIF ecn_filter = 'arcaex' THEN
		str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Exit''' ;
	ELSIF ecn_filter = 'ouchen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'ouchex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Exit''';
	ELSIF ecn_filter = 'rashen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'rashex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Exit''';		
	ELSIF ecn_filter = 'batzen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Entry''';	
	ELSIF ecn_filter = 'batzex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Exit''';	
	ELSIF ecn_filter = 'oubxen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''OUBX'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'oubxex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''OUBX'' and entry_exit=''Exit''';
	ELSIF ecn_filter = 'rabxen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''RABX'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'rabxex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''RABX'' and entry_exit=''Exit''';
	ELSIF ecn_filter = 'nyseen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'nyseex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Exit''';
	END IF;

	--Filter by ISO Odrer
	IF is_iso = '1' AND from_date > '2010-01-01' THEN
		str_sql_no:=str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
	END IF;
	-- tmp_str := str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
	
	IF is_iso = '2' AND from_date > '2010-01-01' THEN
		-- str_sql_no:=str_sql_no || ' and (is_iso <> ''Y'' and (ts_id * 1000000 + cross_id) not in (' || tmp_str || '))';
		str_sql_no:=str_sql_no || ' and (is_iso <> ''Y'')';
	END IF;
	str_sql_no:=str_sql_no || ' GROUP BY (ts_id * 1000000 + cross_id)' ;
	
	
	-- filter by Symbol, Bought Shares, Sold Shares
	IF symb <>'' THEN
		str_sql:=str_sql || ' and symbol='''|| symb ||'''';
	END IF;
	IF bt_shares > 0 THEN
		str_sql:=str_sql || ' and bought_shares >=' || bt_shares;
	END IF;
	IF sd_shares > 0 THEN
		str_sql:=str_sql || ' and sold_shares >=' || sd_shares;
	END IF;
	--filter by ts
	IF ts_id > -1 THEN 
		str_sql:=str_sql || ' and ts_id =' || ts_id;
	END IF;
	str_sql:=str_sql || ' and (ts_id * 1000000 + ts_cid) in ('|| str_sql_no ||')';
	
	IF ecn_filter <> '0' AND exclusive_val = 1 THEN
		str_sql_multi_orders := get_query_string_array_id_multi_orders(to_date, from_date);
		str_sql_tmp := get_mutiple_exchanges_trade(to_date, from_date);
		str_sql :=str_sql || ' and (ts_id * 1000000 + ts_cid) not in (' || str_sql_multi_orders || ')'|| ' and (ts_id * 1000000 + ts_cid) not in (' || str_sql_tmp || ')';
	END IF;

--calculate for trade_statistics table	
FOR trade_result in EXECUTE str_sql loop
    shares_filled := trade_result.bought_shares;

    IF (shares_filled > trade_result.sold_shares) THEN
      shares_filled := trade_result.sold_shares;
    END IF;

    shares_stuck := trade_result.bought_shares - trade_result.sold_shares;    

    IF (shares_stuck = 0) THEN
      profit_loss := trade_result.total_sold - trade_result.total_bought;

      IF (shares_filled = 0) THEN
        summary.count_miss := summary.count_miss + 1;
        summary.expected_miss := summary.expected_miss + trade_result.expected_pl;
      ELSE
        IF (profit_loss < 0) THEN
          summary.count_loss := summary.count_loss + 1;
          summary.expected_loss := summary.expected_loss + trade_result.expected_pl;
          summary.real_loss := summary.real_loss + profit_loss;
        ELSE
          IF ( profit_loss::real < trade_result.expected_pl::real) THEN
            summary.count_profit_under := summary.count_profit_under + 1;
            summary.expected_profit_under := summary.expected_profit_under + trade_result.expected_pl;
            summary.real_profit_under := summary.real_profit_under + profit_loss;
          ELSE
            summary.count_profit_above := summary.count_profit_above + 1;
            summary.expected_profit_above := summary.expected_profit_above + trade_result.expected_pl;
            summary.real_profit_above := summary.real_profit_above + profit_loss;
          END IF;
        END IF;
      END IF;
    ELSE
      --Calculate for stuck
      IF trade_result.real_value < 0 THEN
	--stuck_loss
	   summary.count_stuck_loss := summary.count_stuck_loss + 1;
	   summary.expected_stuck_loss := summary.expected_stuck_loss + trade_result.expected_pl;
	   summary.real_stuck_loss := summary.real_stuck_loss + trade_result.real_value;
      ELSE
	--stuck_profit
	   summary.count_stuck_profit := summary.count_stuck_profit + 1;
	   summary.expected_stuck_profit := summary.expected_stuck_profit + trade_result.expected_pl;
	   summary.real_stuck_profit := summary.real_stuck_profit + trade_result.real_value;
      END IF;
    END IF;

    summary.count_total := summary.count_total +  1;
    summary.expected_total := summary.expected_total + trade_result.expected_pl  ;
  end loop;

  return summary;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION cal_filter_trade_statistic_byecn2_twt2(date, date, time without time zone, time without time zone, integer, double precision, text, integer, integer, text, integer, text, text, integer)
  OWNER TO postgres;

  
  
-- Function: cal_filter_trade_statistic_byecn2_twt2_notime(date, date, integer, double precision, text, integer, integer, text, integer, text, text, integer)

-- DROP FUNCTION cal_filter_trade_statistic_byecn2_twt2_notime(date, date, integer, double precision, text, integer, integer, text, integer, text, text, integer);

CREATE OR REPLACE FUNCTION cal_filter_trade_statistic_byecn2_twt2_notime(to_date date, from_date date, share_size integer, expected_profit double precision, symb text, bt_shares integer, sd_shares integer, ecn_filter text, ts_id integer, cecn text, is_iso text, exclusive_val integer)
  RETURNS trade_result_summary2 AS
$BODY$ 
declare
  trade_result record;
  summary trade_result_summary2;
  shares_filled integer;
  shares_stuck integer;
  profit_loss double precision;
 --statistic missed trade 
  count_ouch_missed_trade integer;
  sum_ouch_expected_pl double precision;
  missed_trade_record_of_ouch record;
  str_sql text;
  str_sql_no text;
  str_sql_multi_orders text;
  str_sql_tmp text;
  tmp_str text;
  
begin	
  -- Initia
  summary.count_miss := 0;
  summary.count_repriced := 0;
  summary.count_stuck_loss := 0;
  summary.count_stuck_profit := 0;
  summary.count_loss := 0;
  summary.count_profit_under := 0;
  summary.count_profit_above := 0;
  summary.count_total := 0;
  summary.expected_miss := 0.00;
  summary.expected_repriced := 0.00;
  summary.expected_stuck_loss := 0.00;
  summary.expected_stuck_profit := 0.00;
  summary.expected_loss := 0.00;
  summary.expected_profit_under := 0.00;
  summary.expected_profit_above := 0.00;
  summary.expected_total := 0.00;
  summary.real_miss := 0.00;
  summary.real_stuck_loss := 0.00;
  summary.real_stuck_profit := 0.00;
  summary.real_loss = 0.00;
  summary.real_profit_under := 0.00;
  summary.real_profit_above := 0.00;
  summary.real_total := 0.00;


	--Calculate repriced
 IF bt_shares = 0 AND sd_shares = 0 THEN
    --Select data from view "vwouch_missed_trades" and calculate total missed trade 	
	str_sql:='SELECT count(*) as count_missed, sum(expected_pl)as sum_expected_pl  FROM vwtrade_results WHERE date >='''|| to_date || ''' and date <='''|| from_date ||''' and expected_shares >='|| share_size ||' and expected_pl >=' || expected_profit ;
	str_sql := str_sql || ' and bought_shares < 1 and sold_shares < 1 ';
		--filter by ts when we get data from view vwtrade_results
		IF ts_id > -1 THEN
			str_sql:=str_sql || ' and ts_id =' || ts_id;
		END IF;
	str_sql_no :='SELECT (ts_id * 1000000 + cross_id) as tmpcol FROM new_orders WHERE date >='''|| to_date || ''' and date <='''|| from_date ||'''';	
	str_sql_no :=str_sql_no || ' and ecn =''OUCH'' and price != ack_price ';
		--filter by ts, ecn_launch when we get data from table new_orders
		IF ts_id > -1 THEN
			str_sql_no:=str_sql_no || ' and ts_id =' || ts_id;
		END IF;
		IF cecn <> '0' THEN
			str_sql_no:=str_sql_no || ' and ecn_launch =''' || cecn ||'''';
		END IF;
	IF ecn_filter = 'arcaen' THEN
		str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Entry''' ;
	ELSIF ecn_filter = 'arcaex' THEN
		str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Exit''' ;
	ELSIF ecn_filter = 'ouchen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'ouchex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Exit''';
	ELSIF ecn_filter = 'rashen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'rashex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Exit''';		
	ELSIF ecn_filter = 'batzen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Entry''';	
	ELSIF ecn_filter = 'batzex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Exit''';	
	ELSIF ecn_filter = 'oubxen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''OUBX'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'oubxex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''OUBX'' and entry_exit=''Exit''';
	ELSIF ecn_filter = 'rabxen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''RABX'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'rabxex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''RABX'' and entry_exit=''Exit''';
	ELSIF ecn_filter = 'nyseen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'nyseex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Exit''';
	END IF;
	
	--Filter by ISO Odrer
	IF is_iso = '1' AND from_date > '2010-01-01' THEN
		str_sql_no:=str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
	END IF;	
	-- tmp_str := str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
	
	IF is_iso = '2' AND from_date > '2010-01-01' THEN
		-- str_sql_no:=str_sql_no || ' and (is_iso <> ''Y'' and (ts_id * 1000000 + cross_id) not in (' || tmp_str || '))';
		str_sql_no:=str_sql_no || ' and (is_iso <> ''Y'')';
	END IF;
	
	IF symb <>'' THEN
		str_sql:=str_sql || ' and symbol='''|| symb ||'''';
		str_sql_no:=str_sql_no || ' and symbol='''|| symb ||'''';
	END IF;

		str_sql_no:=str_sql_no || ' GROUP BY (ts_id * 1000000 + cross_id)' ;

	IF bt_shares > 0 THEN
		str_sql:=str_sql || ' and bought_shares >=' || bt_shares;
	END IF;
	IF sd_shares > 0 THEN
		str_sql:=str_sql || ' and sold_shares >=' || sd_shares;
	END IF;

	str_sql:=str_sql || ' and (ts_id * 1000000 + ts_cid) in ('|| str_sql_no ||')';
	
	IF ecn_filter <> '0' AND exclusive_val = 1 THEN
		str_sql_multi_orders := get_query_string_array_id_multi_orders(to_date, from_date);
		str_sql_tmp := get_mutiple_exchanges_trade(to_date, from_date); 
		str_sql :=str_sql || ' and (ts_id * 1000000 + ts_cid) not in (' || str_sql_multi_orders || ')'|| ' and (ts_id * 1000000 + ts_cid) not in (' || str_sql_tmp || ')';
	END IF;
	
	for missed_trade_record_of_ouch in EXECUTE str_sql loop
		count_ouch_missed_trade := missed_trade_record_of_ouch.count_missed;
		sum_ouch_expected_pl := missed_trade_record_of_ouch.sum_expected_pl;
	end loop;

	   IF count_ouch_missed_trade > 0 THEN
		summary.count_repriced := count_ouch_missed_trade;
		summary.expected_repriced := sum_ouch_expected_pl;

		summary.count_miss := -count_ouch_missed_trade;
		summary.expected_miss :=  -sum_ouch_expected_pl;
	   END IF;

  END IF;

--filter by  ECN by Entry/Exit
  str_sql:='SELECT * FROM vwtrade_results WHERE date >='''|| to_date || ''' and date <='''|| from_date ||''' and expected_shares >='|| share_size ||' and expected_pl >=' || expected_profit ;
  str_sql_no :='SELECT (ts_id * 1000000 + cross_id) as tmpcol FROM new_orders WHERE date >='''|| to_date || ''' and date <='''|| from_date ||'''';  
	--filter by ts, ecn launch
	IF ts_id > -1 THEN
		str_sql_no:=str_sql_no || ' and ts_id =' || ts_id;
	END IF;
	IF cecn <> '0' THEN
		str_sql_no:=str_sql_no || ' and ecn_launch =''' || cecn ||'''';
	END IF;
	
	IF ecn_filter = 'arcaen' THEN
		str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Entry''' ;
	ELSIF ecn_filter = 'arcaex' THEN
		str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Exit''' ;
	ELSIF ecn_filter = 'ouchen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'ouchex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Exit''';
	ELSIF ecn_filter = 'rashen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'rashex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Exit''';		
	ELSIF ecn_filter = 'batzen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Entry''';	
	ELSIF ecn_filter = 'batzex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Exit''';	
	ELSIF ecn_filter = 'oubxen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''OUBX'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'oubxex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''OUBX'' and entry_exit=''Exit''';
	ELSIF ecn_filter = 'rabxen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''RABX'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'rabxex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''RABX'' and entry_exit=''Exit''';
	ELSIF ecn_filter = 'nyseen' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Entry''';
	ELSIF ecn_filter = 'nyseex' THEN
		str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Exit''';
	END IF;

	--Filter by ISO Odrer
	IF is_iso = '1' AND from_date > '2010-01-01' THEN
		str_sql_no:=str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
	END IF;	
	-- tmp_str := str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
	
	IF is_iso = '2' AND from_date > '2010-01-01' THEN
		-- str_sql_no:=str_sql_no || ' and (is_iso <> ''Y'' and (ts_id * 1000000 + cross_id) not in (' || tmp_str || '))';
		str_sql_no:=str_sql_no || ' and (is_iso <> ''Y'')';
	END IF;
	
	str_sql_no:=str_sql_no || ' GROUP BY (ts_id * 1000000 + cross_id)' ;
	
	-- filter by Symbol, Bought Shares, Sold Shares
	IF symb <>'' THEN
		str_sql:=str_sql || ' and symbol='''|| symb ||'''';
	END IF;
	IF bt_shares > 0 THEN
		str_sql:=str_sql || ' and bought_shares >=' || bt_shares;
	END IF;
	IF sd_shares > 0 THEN
		str_sql:=str_sql || ' and sold_shares >=' || sd_shares;
	END IF;
	--filter by ts
	IF ts_id > -1 THEN 
		str_sql:=str_sql || ' and ts_id =' || ts_id;
	END IF;
	
	str_sql:=str_sql || ' and (ts_id * 1000000 + ts_cid) in ('|| str_sql_no ||')';
	
	IF ecn_filter <> '0' AND exclusive_val = 1 THEN
		str_sql_multi_orders := get_query_string_array_id_multi_orders(to_date, from_date);
		str_sql_tmp := get_mutiple_exchanges_trade(to_date, from_date); 
		str_sql :=str_sql || ' and (ts_id * 1000000 + ts_cid) not in (' || str_sql_multi_orders || ')'|| ' and (ts_id * 1000000 + ts_cid) not in (' || str_sql_tmp || ')';
	END IF;
	
--calculate for trade_statistics table	
FOR trade_result in EXECUTE str_sql loop
    shares_filled := trade_result.bought_shares;

    IF (shares_filled > trade_result.sold_shares) THEN
      shares_filled := trade_result.sold_shares;
    END IF;

    shares_stuck := trade_result.bought_shares - trade_result.sold_shares;    

    IF (shares_stuck = 0) THEN
      profit_loss := trade_result.total_sold - trade_result.total_bought;

      IF (shares_filled = 0) THEN
        summary.count_miss := summary.count_miss + 1;
        summary.expected_miss := summary.expected_miss + trade_result.expected_pl;
      ELSE
        IF (profit_loss < 0) THEN
          summary.count_loss := summary.count_loss + 1;
          summary.expected_loss := summary.expected_loss + trade_result.expected_pl;
          summary.real_loss := summary.real_loss + profit_loss;
        ELSE
          IF ( profit_loss::real < trade_result.expected_pl::real) THEN
            summary.count_profit_under := summary.count_profit_under + 1;
            summary.expected_profit_under := summary.expected_profit_under + trade_result.expected_pl;
            summary.real_profit_under := summary.real_profit_under + profit_loss;
          ELSE
            summary.count_profit_above := summary.count_profit_above + 1;
            summary.expected_profit_above := summary.expected_profit_above + trade_result.expected_pl;
            summary.real_profit_above := summary.real_profit_above + profit_loss;
          END IF;
        END IF;
      END IF;
    ELSE
      --Calculate for stuck
      IF trade_result.real_value < 0 THEN
	--stuck_loss
	   summary.count_stuck_loss := summary.count_stuck_loss + 1;
	   summary.expected_stuck_loss := summary.expected_stuck_loss + trade_result.expected_pl;
	   summary.real_stuck_loss := summary.real_stuck_loss + trade_result.real_value;
      ELSE
	--stuck_profit
	   summary.count_stuck_profit := summary.count_stuck_profit + 1;
	   summary.expected_stuck_profit := summary.expected_stuck_profit + trade_result.expected_pl;
	   summary.real_stuck_profit := summary.real_stuck_profit + trade_result.real_value;
      END IF;
    END IF;

    summary.count_total := summary.count_total +  1;
    summary.expected_total := summary.expected_total + trade_result.expected_pl  ;
  end loop;

  return summary;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION cal_filter_trade_statistic_byecn2_twt2_notime(date, date, integer, double precision, text, integer, integer, text, integer, text, text, integer)
  OWNER TO postgres;

  
  
-- Function: cal_filter_trade_statistic_byecn2_twt2_with_step(date, date, time without time zone, time without time zone, integer, double precision, text, integer, integer, text, integer, text, integer, text, integer)

-- DROP FUNCTION cal_filter_trade_statistic_byecn2_twt2_with_step(date, date, time without time zone, time without time zone, integer, double precision, text, integer, integer, text, integer, text, integer, text, integer);

CREATE OR REPLACE FUNCTION cal_filter_trade_statistic_byecn2_twt2_with_step(to_date date, from_date date, time_from time without time zone, time_to time without time zone, share_size integer, expected_profit double precision, symb text, bt_shares integer, sd_shares integer, ecn_filter text, ts_id integer, cecn text, step integer, is_iso text, exclusive_val integer)
  RETURNS SETOF trade_result_summary2_struct AS
$BODY$ 
declare
	trade_result 				record;
	summary 				trade_result_summary2_struct;
	shares_filled 				integer;
	shares_stuck 				integer;
	profit_loss 				double precision;
	--statistic missed trade 
	count_ouch_missed_trade 		integer;
	sum_ouch_expected_pl 			double precision;
	missed_trade_record_of_ouch 		record;
	str_sql 				text;
	str_sql_no 				text;
	str_sql_tmp				text;
	str_sql_multi_orders text;
	tmp text;
	tmp_str text;
	min_time				"time";
	max_time				"time";
	temp_time 				"time";
	t_current				"time";
  
begin	
	
	min_time := time_from;
	max_time := time_to;
	WHILE min_time < max_time LOOP
		-- Initia
		
		summary.count_miss := 0;
		summary.count_repriced := 0;
		summary.count_stuck_loss := 0;
		summary.count_stuck_profit := 0;
		summary.count_loss := 0;
		summary.count_profit_under := 0;
		summary.count_profit_above := 0;		
		
		IF (step = 5) THEN
			t_current = min_time + interval '5 minutes';
		ELSIF (step = 15) THEN
			t_current = min_time + interval '15 minutes';
			
		ELSIF (step = 30) THEN
			t_current = min_time + interval '30 minutes';
		ELSIF (step = 60) THEN
			t_current = min_time + interval '60 minutes';
		END IF;
		--raise notice '1(%)', t_current;
		IF ((t_current > max_time) or (t_current = '00:00:00')) THEN
			t_current = max_time;
			--raise notice '2(%)', t_current;
		END IF; 
		temp_time = t_current - interval '1 second';
		
		--Calculate repriced
		IF bt_shares = 0 AND sd_shares = 0 THEN
		
			--Select data from view "vwouch_missed_trades" and calculate total missed trade 	
			str_sql:='SELECT count(*) as count_missed, sum(expected_pl)as sum_expected_pl  FROM trade_results WHERE date >='''|| to_date || ''' and date <='''|| from_date ||''' and expected_shares >='|| share_size ||' and expected_pl >=' || expected_profit ;
			str_sql := str_sql || ' and bought_shares < 1 and sold_shares < 1 ';
			
			--filter by ts when we get data from view vwtrade_results
			IF ts_id > -1 THEN
				str_sql:=str_sql || ' and ts_id =' || ts_id;
			END IF;
			
			str_sql_no :='SELECT (ts_id * 1000000 + cross_id) as tmpcol FROM new_orders WHERE date >='''|| to_date || ''' and date <='''|| from_date ||'''';
			str_sql_no :=str_sql_no || ' and time_stamp between ''' || min_time || ''' and '''|| temp_time || '''';
			str_sql_no :=str_sql_no || ' and ecn =''OUCH'' and price != ack_price ';
			
			--filter by ts, ecn_launch when we get data from table new_orders
			IF ts_id > -1 THEN
				str_sql_no:=str_sql_no || ' and ts_id =' || ts_id;
			END IF;
			IF cecn <> '0' THEN
				str_sql_no:=str_sql_no || ' and ecn_launch =''' || cecn ||'''';
			END IF;
			
			IF ecn_filter = 'arcaen' THEN
				str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Entry''' ;
			ELSIF ecn_filter = 'arcaex' THEN
				str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Exit''' ;
			ELSIF ecn_filter = 'ouchen' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Entry''';
			ELSIF ecn_filter = 'ouchex' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Exit''';
			ELSIF ecn_filter = 'rashen' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Entry''';
			ELSIF ecn_filter = 'rashex' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Exit''';		
			ELSIF ecn_filter = 'batzen' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Entry''';	
			ELSIF ecn_filter = 'batzex' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Exit''';	
			ELSIF ecn_filter = 'oubxen' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''OUBX'' and entry_exit=''Entry''';
			ELSIF ecn_filter = 'oubxex' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''OUBX'' and entry_exit=''Exit''';
			ELSIF ecn_filter = 'rabxen' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''RABX'' and entry_exit=''Entry''';
			ELSIF ecn_filter = 'rabxex' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''RABX'' and entry_exit=''Exit''';
			ELSIF ecn_filter = 'nyseen' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Entry''';
			ELSIF ecn_filter = 'nyseex' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Exit''';
			END IF;


			IF symb <>'' THEN
				str_sql:=str_sql || ' and symbol='''|| symb ||'''';
				str_sql_no:=str_sql_no || ' and symbol='''|| symb ||'''';
			END IF;
			--Filter by ISO Odrer
			IF is_iso = '1' AND from_date > '2010-01-01' THEN
				str_sql_no:=str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
			END IF;
			-- tmp_str := str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
	
			IF is_iso = '2' AND from_date > '2010-01-01' THEN
				-- str_sql_no:=str_sql_no || ' and (is_iso <> ''Y'' and (ts_id * 1000000 + cross_id) not in (' || tmp_str || '))';
				str_sql_no:=str_sql_no || ' and (is_iso <> ''Y'')';
			END IF;
			
			str_sql_no:=str_sql_no || ' GROUP BY (ts_id * 1000000 + cross_id)' ;

			IF bt_shares > 0 THEN
				str_sql:=str_sql || ' and bought_shares >=' || bt_shares;
			END IF;
			IF sd_shares > 0 THEN
				str_sql:=str_sql || ' and sold_shares >=' || sd_shares;
			END IF;

			str_sql:=str_sql || ' and (ts_id * 1000000 + ts_cid) in ('|| str_sql_no ||')';
			
			IF ecn_filter <> '0' AND exclusive_val = 1 THEN
				str_sql_multi_orders := get_query_string_array_id_multi_orders(to_date, from_date);
				str_sql_tmp := get_mutiple_exchanges_trade(to_date, from_date); 
				str_sql :=str_sql || ' and (ts_id * 1000000 + ts_cid) not in (' || str_sql_multi_orders || ')'|| ' and (ts_id * 1000000 + ts_cid) not in (' || str_sql_tmp || ')';
			END IF;
			
			for missed_trade_record_of_ouch in EXECUTE str_sql loop
				count_ouch_missed_trade := missed_trade_record_of_ouch.count_missed;
				sum_ouch_expected_pl := missed_trade_record_of_ouch.sum_expected_pl;
			end loop;

			IF count_ouch_missed_trade > 0 THEN
				summary.count_repriced := count_ouch_missed_trade;				
				summary.count_miss := -count_ouch_missed_trade;				
			END IF;

		END IF;

		--filter by  ECN by Entry/Exit
		str_sql:='SELECT * FROM trade_results WHERE date >='''|| to_date || ''' and date <='''|| from_date ||''' and expected_shares >='|| share_size ||' and expected_pl >=' || expected_profit ;
		str_sql_no :='SELECT (ts_id * 1000000 + cross_id) as tmpcol FROM new_orders WHERE date >='''|| to_date || ''' and date <='''|| from_date ||'''';
		str_sql_no :=str_sql_no || ' and time_stamp between ''' || min_time || ''' and '''|| temp_time || '''';
		
		--filter by ts, ecn launch
		IF ts_id > -1 THEN
			str_sql_no:=str_sql_no || ' and ts_id =' || ts_id;
		END IF;
		IF cecn <> '0' THEN
			str_sql_no:=str_sql_no || ' and ecn_launch =''' || cecn ||'''';
		END IF;
		
		IF ecn_filter = 'arcaen' THEN
			str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Entry''' ;
		ELSIF ecn_filter = 'arcaex' THEN
			str_sql_no:=str_sql_no || ' and ecn =' ||'''ARCA'''|| ' and entry_exit=''Exit''' ;
		ELSIF ecn_filter = 'ouchen' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Entry''';
		ELSIF ecn_filter = 'ouchex' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''OUCH'' and entry_exit=''Exit''';
		ELSIF ecn_filter = 'rashen' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Entry''';
		ELSIF ecn_filter = 'rashex' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''RASH'' and entry_exit=''Exit''';		
		ELSIF ecn_filter = 'batzen' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Entry''';	
		ELSIF ecn_filter = 'batzex' THEN
				str_sql_no:=str_sql_no || ' and ecn = ''BATZ'' and entry_exit=''Exit''';	
		ELSIF ecn_filter = 'oubxen' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''OUBX'' and entry_exit=''Entry''';
		ELSIF ecn_filter = 'oubxex' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''OUBX'' and entry_exit=''Exit''';
		ELSIF ecn_filter = 'rabxen' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''RABX'' and entry_exit=''Entry''';
		ELSIF ecn_filter = 'rabxex' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''RABX'' and entry_exit=''Exit''';
		ELSIF ecn_filter = 'nyseen' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Entry''';
		ELSIF ecn_filter = 'nyseex' THEN
			str_sql_no:=str_sql_no || ' and ecn = ''NYSE'' and entry_exit=''Exit''';
		END IF;
		
		--Filter by ISO Odrer
		IF is_iso = '1' AND from_date > '2010-01-01' THEN
			str_sql_no:=str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
		END IF;
		-- tmp_str := str_sql_no || ' and is_iso is not NULL and is_iso =''Y''';
	
		IF is_iso = '2' AND from_date > '2010-01-01' THEN
			-- str_sql_no:=str_sql_no || ' and (is_iso <> ''Y'' and (ts_id * 1000000 + cross_id) not in (' || tmp_str || '))';
			str_sql_no:=str_sql_no || ' and (is_iso <> ''Y'')';
		END IF;
		
		str_sql_no:=str_sql_no || ' GROUP BY (ts_id * 1000000 + cross_id)' ;

		-- filter by Symbol, Bought Shares, Sold Shares
		IF symb <>'' THEN
			str_sql:=str_sql || ' and symbol='''|| symb ||'''';
		END IF;
		IF bt_shares > 0 THEN
			str_sql:=str_sql || ' and bought_shares >=' || bt_shares;
		END IF;
		IF sd_shares > 0 THEN
			str_sql:=str_sql || ' and sold_shares >=' || sd_shares;
		END IF;
		--filter by ts
		IF ts_id > -1 THEN 
			str_sql:=str_sql || ' and ts_id =' || ts_id;
		END IF;
		
		str_sql:=str_sql || ' and (ts_id * 1000000 + ts_cid) in ('|| str_sql_no ||')';
		
		IF ecn_filter <> '0' AND exclusive_val = 1 THEN
			str_sql_multi_orders := get_query_string_array_id_multi_orders(to_date, from_date);
			str_sql_tmp := get_mutiple_exchanges_trade(to_date, from_date); 
			str_sql :=str_sql || ' and (ts_id * 1000000 + ts_cid) not in (' || str_sql_multi_orders || ')'|| ' and (ts_id * 1000000 + ts_cid) not in (' || str_sql_tmp || ')';
		END IF;
		
		--calculate for trade_statistics table	
		FOR trade_result in EXECUTE str_sql loop
			shares_filled := trade_result.bought_shares;
			--RAISE NOTICE 'Variable an_integer was changed.';			
			IF (shares_filled > trade_result.sold_shares) THEN
			  shares_filled := trade_result.sold_shares;
			END IF;

			shares_stuck := trade_result.bought_shares - trade_result.sold_shares;    

			IF (shares_stuck = 0) THEN
				profit_loss := trade_result.total_sold - trade_result.total_bought;

				IF (shares_filled = 0) THEN
				summary.count_miss := summary.count_miss + 1;				
				ELSE
					IF (profit_loss < 0) THEN
						summary.count_loss := summary.count_loss + 1;						
					ELSE
						IF ( profit_loss::real < trade_result.expected_pl::real) THEN
							summary.count_profit_under := summary.count_profit_under + 1;						
						ELSE
							summary.count_profit_above := summary.count_profit_above + 1;							
						END IF;
					END IF;
				END IF;
			ELSE
				--Calculate for stuck
				IF trade_result.real_value < 0 THEN
					--stuck_loss
					summary.count_stuck_loss := summary.count_stuck_loss + 1;					
				ELSE
					--stuck_profit
					summary.count_stuck_profit := summary.count_stuck_profit + 1;				
				END IF;
			END IF;

		END LOOP;

		
		summary.time_stamp = t_current;
		min_time := t_current;	
	
		RETURN NEXT summary;
		
	END LOOP;
	RETURN;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION cal_filter_trade_statistic_byecn2_twt2_with_step(date, date, time without time zone, time without time zone, integer, double precision, text, integer, integer, text, integer, text, integer, text, integer)
  OWNER TO postgres;


-- Function: cal_trade_statistic_date_range(date, date, time without time zone, time without time zone, integer, double precision, text, integer, integer, text, integer, text, text, integer)

-- DROP FUNCTION cal_trade_statistic_date_range(date, date, time without time zone, time without time zone, integer, double precision, text, integer, integer, text, integer, text, text, integer);

CREATE OR REPLACE FUNCTION cal_trade_statistic_date_range(to_date date, from_date date, time_from time without time zone, time_to time without time zone, share_size integer, expected_profit double precision, symb text, bt_shares integer, sd_shares integer, ecn_filter text, ts_id integer, cecn text, is_iso text, exclusive_val integer)
  RETURNS trade_result_summary2 AS
$BODY$ 
declare 
	dt2 date;
	tmp trade_result_summary2;
	summary trade_result_summary2;
begin
	dt2 := to_date;
	  summary.count_miss := 0;
	  summary.count_repriced := 0;
	  summary.count_stuck_loss := 0;
	  summary.count_stuck_profit := 0;
	  summary.count_loss := 0;
	  summary.count_profit_under := 0;
	  summary.count_profit_above := 0;
	  summary.count_total := 0;
	  summary.expected_miss := 0.00;
	  summary.expected_repriced := 0.00;
	  summary.expected_stuck_loss := 0.00;
	  summary.expected_stuck_profit := 0.00;
	  summary.expected_loss := 0.00;
	  summary.expected_profit_under := 0.00;
	  summary.expected_profit_above := 0.00;
	  summary.expected_total := 0.00;
	  summary.real_miss := 0.00;
	  summary.real_stuck_loss := 0.00;
	  summary.real_stuck_profit := 0.00;
	  summary.real_loss = 0.00;
	  summary.real_profit_under := 0.00;
	  summary.real_profit_above := 0.00;
	  summary.real_total := 0.00;
	  
	while dt2 <= from_date loop
		tmp := cal_filter_trade_statistic_byecn2_twt2(dt2, dt2, time_from , time_to, share_size , expected_profit , symb , bt_shares , sd_shares , ecn_filter , ts_id , cecn, is_iso, exclusive_val);
		
	
		  summary.count_miss := summary.count_miss + tmp.count_miss;
		  summary.count_repriced := summary.count_repriced + tmp.count_repriced;
		  summary.count_stuck_loss :=  summary.count_stuck_loss + tmp.count_stuck_loss ;
		  summary.count_stuck_profit := summary.count_stuck_profit + tmp.count_stuck_profit;
		  summary.count_loss := summary.count_loss + tmp.count_loss;
		  summary.count_profit_under := summary.count_profit_under + tmp.count_profit_under;
		  summary.count_profit_above := summary.count_profit_above + tmp.count_profit_above;
		  summary.count_total := summary.count_total + tmp.count_total;
		  summary.expected_miss := summary.expected_miss + tmp.expected_miss;
		  summary.expected_repriced := summary.expected_repriced + tmp.expected_repriced;
		  summary.expected_stuck_loss := summary.expected_stuck_loss + tmp.expected_stuck_loss;
		  summary.expected_stuck_profit := summary.expected_stuck_profit + tmp.expected_stuck_profit;
		  summary.expected_loss := summary.expected_loss + tmp.expected_loss;
		  summary.expected_profit_under := summary.expected_profit_under + tmp.expected_profit_under;
		  summary.expected_profit_above := summary.expected_profit_above + tmp.expected_profit_above;
		  summary.expected_total := summary.expected_total + tmp.expected_total;
		  summary.real_miss := summary.real_miss + tmp.real_miss;
		  summary.real_stuck_loss := summary.real_stuck_loss + tmp.real_stuck_loss;
		  summary.real_stuck_profit := summary.real_stuck_profit + tmp.real_stuck_profit;
		  summary.real_loss = summary.real_loss + tmp.real_loss;
		  summary.real_profit_under := summary.real_profit_under + tmp.real_profit_under;
		  summary.real_profit_above := summary.real_profit_above + tmp.real_profit_above;
		  summary.real_total := summary.real_total + tmp.real_total;
		dt2 := dt2 + interval '1 day';
		
	end loop;
	
	 return summary;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION cal_trade_statistic_date_range(date, date, time without time zone, time without time zone, integer, double precision, text, integer, integer, text, integer, text, text, integer)
  OWNER TO postgres;
COMMIT;
