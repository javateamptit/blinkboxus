/* Copyright (C) and/or used under license by RGM Advisors, LLC. All Rights Reserved Worldwide. */

#ifndef _common_environment_h_
#define _common_environment_h_

#include <string>

class Environment {
public:
    Environment();
    std::string get_root_data_dir();
    std::string get_root_conf_dir();
    std::string get_root_bin_dir();

    std::string get_data_filename(char const* name);
    std::string get_home_filename(char const* name);
	std::string get_hbi_filename(char const* name);

    std::string get_book_conf_filename(char const* name);
    std::string get_order_conf_filename(char const* name);
    std::string get_symbol_conf_filename(char const* name);
    std::string get_misc_conf_filename(char const* name);
	std::string get_ta2_conf_filename(char const* name);
	std::string get_ta_conf_filename(char const* name);
	
	std::string get_preserved_hbi_filename(char const* name);
	std::string get_symbol_config_hbi_filename(char const* name);

    std::string get_version_log();

    void reset();
    static Environment& instance();

private:
    std::string get_conf_filename(char const* subdir, char const* name);
    std::string get_hbi_filename(char const* subdir, char const* name);

    std::string _bb_home;
    std::string _bb_data;
	std::string _hbi_root;

};

class EnvironmentSaver {
public:
    EnvironmentSaver(char const* name);
    ~EnvironmentSaver();

private:
    std::string _name;
    bool _was_found;
    std::string _saved_value;
};

#endif
