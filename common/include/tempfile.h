/* Copyright (C) and/or used under license by RGM Advisors, LLC. All Rights Reserved Worldwide. */

#ifndef _common_tempfile_h_
#define _common_tempfile_h_

#include <string>

class Tempfile {
public:
    Tempfile();
    ~Tempfile();
    char const* get_name();
    bool write(char const* contents);

private:
    std::string _name;

};

class Tempdir {
public:
    Tempdir();
    ~Tempdir();
    char const* get_name();
    bool mkdir(char const* name);
    bool write_file(char const* name, char const* contents);

private:
    std::string _name;
};

#endif

