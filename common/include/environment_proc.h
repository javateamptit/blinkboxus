/* Copyright (C) and/or used under license by RGM Advisors, LLC. All Rights Reserved Worldwide. */

#ifndef _common_environment_proc_h_
#define _common_environment_proc_h_

/** @file
 * @brief C functions for accessing the environment.
 * These functions access the BLINKBOX_HOME, BLINKBOX_DATA and HBI_ROOT environment variables.
 */

#ifdef __cplusplus
extern "C" {
#endif

/**
  * Stores the name of the specified book configuration file into @a buffer.
  * The filename will be @c \$(BLINKBOX_HOME)/config/book/@a name.
  * @param buffer A buffer to hold the output.
  * @param len The length of @a buffer.
  * @return @c 0 in case of error, @a buffer otherwise.
  */
char* Environment_get_book_conf_filename(char const* name, char* buffer, int len);

/**
  * Stores the name of the specified order configuration file into @a buffer.
  * The filename will be @c \$(BLINKBOX_HOME)/config/order/@a name.
  * @param buffer A buffer to hold the output.
  * @param len The length of @a buffer.
  * @return @c 0 in case of error, @a buffer otherwise.
  */
char* Environment_get_order_conf_filename(char const* name, char* buffer, int len);

/**
  * Stores the name of the specified symbol configuration file into @a buffer.
  * The filename will be @c \$(BLINKBOX_HOME)/config/symbol/@a name.
  * @param buffer A buffer to hold the output.
  * @param len The length of @a buffer.
  * @return @c 0 in case of error, @a buffer otherwise.
  */
char* Environment_get_symbol_conf_filename(char const* name, char* buffer, int len);

/**
  * Stores the name of the specified misc configuration file into @a buffer.
  * The filename will be @c \$(BLINKBOX_HOME)/config/misc/@a name.
  * @param buffer A buffer to hold the output.
  * @param len The length of @a buffer.
  * @return @c 0 in case of error, @a buffer otherwise.
  */
char* Environment_get_misc_conf_filename(char const* name, char* buffer, int len);

/**
  * Stores the name of the specified ta2 configuration file into @a buffer.
  * The filename will be @c \$(BLINKBOX_HOME)/config/ta2/@a name.
  * @param buffer A buffer to hold the output.
  * @param len The length of @a buffer.
  * @return @c 0 in case of error, @a buffer otherwise.
  */
char* Environment_get_ta2_conf_filename(char const* name, char* buffer, int len);

/**
  * Stores the name of the specified ta configuration file into @a buffer.
  * The filename will be @c \$(BLINKBOX_HOME)/config/ta/@a name.
  * @param buffer A buffer to hold the output.
  * @param len The length of @a buffer.
  * @return @c 0 in case of error, @a buffer otherwise.
  */
char* Environment_get_ta_conf_filename(char const* name, char* buffer, int len);

/**
  * Stores the name of the root data directory into @a buffer.
  * The root data directory is @c \$(BLINKBOX_DATA).
  * @param buffer A buffer to hold the output.
  * @param len The length of @a buffer.
  * @return @c 0 in case of error, @a buffer otherwise.
  */
char* Environment_get_root_data_dir(char* buffer, int len);

/**
  * Stores the name of the specified data file into @a buffer.
  * The filename will be @c \$(BLINKBOX_DATA)/@a name.
  * @param buffer A buffer to hold the output.
  * @param len The length of @a buffer.
  * @return @c 0 in case of error, @a buffer otherwise.
  */
char* Environment_get_data_filename(char const* name, char* buffer, int len);

/**
  * Stores the name of the specified configuration file into @a buffer.
  * The filename will be @c \$(BLINKBOX_HOME)/@a name.
  * @param buffer A buffer to hold the output.
  * @param len The length of @a buffer.
  * @return @c 0 in case of error, @a buffer otherwise.
  */
char* Environment_get_home_filename(char const* name, char* buffer, int len);

/**
  * Stores the name of the specified configuration file into @a buffer.
  * The filename will be @c \$(HBI_ROOT)/@a name.
  * @param buffer A buffer to hold the output.
  * @param len The length of @a buffer.
  * @return @c 0 in case of error, @a buffer otherwise.
  */
char* Environment_get_hbi_filename(char const* name, char* buffer, int len);

/**
  * Stores the name of the specified configuration file into @a buffer.
  * The filename will be @c \$(HBI_ROOT)/@a name.
  * @param buffer A buffer to hold the output.
  * @param len The length of @a buffer.
  * @return @c 0 in case of error, @a buffer otherwise.
  */
char* Environment_get_hbi_preserved_filename(char const* name, char* buffer, int len);

/**
  * Stores the name of the specified configuration file into @a buffer.
  * The filename will be @c \$(HBI_ROOT)/@a name.
  * @param buffer A buffer to hold the output.
  * @param len The length of @a buffer.
  * @return @c 0 in case of error, @a buffer otherwise.
  */
char* Environment_get_hbi_symbol_config_filename(char const* name, char* buffer, int len);

/**
 * Rereads the environment variables.
 */
void Environment_reset();

#ifdef __cplusplus
}
#endif

#endif
