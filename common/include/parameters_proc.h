/* Copyright (C) and/or used under license by RGM Advisors, LLC. All Rights Reserved Worldwide. */

#ifndef _common_parameters_proc_h_
#define _common_parameters_proc_h_

/** @file
 * @brief C functions for accessing properties files.
 * These functions read values from files containing name-value pairs.
 */

#ifdef __cplusplus
extern "C" {
#endif

/**
  * Retrieves the value of the parameter @a pname from @a filename.
  * If @a pname is not found in the file, @a default_value is returned.
  * The returned pointer is valid until the @c reset function is called.
  * @param filename The name of the configuration file to read.
  * @param pname The name of the parameter to retrieve.
  * @param default_value The value to return if either @a filename or @a pname is not found.
  * @return The parameter value or @a default_value, if it is not found.
  */
char const* Parameters_get(char const* filename, char const* pname, char const* default_value);

/**
  * Retrieves the integer value of the parameter @a pname from @a filename.
  * If @a pname is not found in the file or cannot be interpreted as an integer, @a default_value is returned.
  * @param filename The name of the configuration file to read.
  * @param pname The name of the parameter to retrieve.
  * @param default_value The value to return if @a pname is not found or cannot be interpreted as an integer.
  * @return The parameter value or @a default_value, if it is not found.
  */
int Parameters_get_int(char const* filename, char const* pname, int default_value);

long Parameters_get_long(char const* filename, char const* pname, long default_value);

/**
  * Retrieves the integer value of the parameter @a pname from @a filename.
  * If @a pname is not found in the file or cannot be interpreted as a double, @a default_value is returned.
  * @param filename The name of the configuration file to read.
  * @param pname The name of the parameter to retrieve.
  * @param default_value The value to return if @a pname is not found or cannot be interpreted as a double.
  * @return The parameter value or @a default_value, if it is not found.
  */
double Parameters_get_double(char const* filename, char const* pname, double default_value);

/**
  * Empties the cache of parameter values.
  */
void Parameters_reset();

#ifdef __cplusplus
}
#endif

#endif
