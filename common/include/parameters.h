/* Copyright (C) and/or used under license by RGM Advisors, LLC. All Rights Reserved Worldwide. */

#ifndef _common_parameters_h_
#define _common_parameters_h_

#include <cstdio>
#include <map>
#include <set>
#include <string>

class Parameters {
public:
    char const* get(char const* filename, char const* pname, char const* default_value);
    int get_int(char const* filename, char const* pname, int default_value);
	long get_long(char const* filename, char const* pname, long default_value);
    double get_double(char const* filename, char const* pname, double default_value);
    void reset();

    typedef std::pair<std::string, std::string> Pair;
    static Pair parse_line(std::string const& line);
    static std::string trim(std::string data);

    static Parameters& instance();

private:
    void load_file(char const* filename, FILE* fp);
    void add(char const* filename, Pair nv);
    typedef std::map<Pair, std::string> Cache;
    Cache _cache;
    std::set<std::string> _files;
};

#endif
