/* Copyright (C) and/or used under license by RGM Advisors, LLC. All Rights Reserved Worldwide. */

#ifndef _common_hbitime_h_
#define _common_hbitime_h_

#include <time.h>
#include <sys/time.h>

/** @file
 * @brief C functions for accessing the current system time.
 * These functions access the current system time but subtract an offset
 * configured via the @c HBI_TM_OFFSET environment variable.
 */

#ifdef __cplusplus
extern "C" {
#endif

// do not call this from a static context

/** @cond IMPLEMENTATION */
extern long hbi_tm_offset_micros;
extern int hbi_tm_offset_seconds;
extern int hbi_tm_offset_micros_part;
extern double hbi_tm_offset_frac_seconds;

static inline long nano_time() {
  struct timespec time1;
  clock_gettime(CLOCK_MONOTONIC_RAW, &time1);
  return time1.tv_sec*1000000000L + time1.tv_nsec;
}


static inline void hbitime_add_offset_to_parts(int* seconds_part, int* micros_part) {
    *seconds_part -= hbi_tm_offset_seconds;
    *micros_part -= hbi_tm_offset_micros_part;
    if (*micros_part < 0) {
        *seconds_part -= 1;
        *micros_part += (1000 * 1000);
    }
}
/** @endcond*/

/**
  * Gets the current adjusted time as microseconds since the epoch.
  * The epoch is defined as 1970-01-01 00:00:00 UTC.
  */
static inline long hbitime_micros() {
    struct timeval tv;
    gettimeofday(&tv, 0);
    long sec = tv.tv_sec;
    return sec * 1000 * 1000 + tv.tv_usec - hbi_tm_offset_micros;
}

/**
  * Gets the current adjusted time as microseconds since the epoch.
  * The epoch is defined as 1970-01-01 00:00:00 UTC.
  */
static inline void hbitime_micros_parts(int* seconds_part, int* micros_part) {
    struct timeval tv;
    gettimeofday(&tv, 0);
    *seconds_part = tv.tv_sec;
    *micros_part = tv.tv_usec;
    hbitime_add_offset_to_parts(seconds_part, micros_part);
}

/**
  * Gets the current adjusted time as seconds since the epoch.
  * The epoch is defined as 1970-01-01 00:00:00 UTC.
  */
static inline int hbitime_seconds() {
    time_t now;
    time(&now);
    return now - hbi_tm_offset_seconds; 
}

/**
  * Gets the current adjusted time as fractional seconds since the epoch.
  * The epoch is defined as 1970-01-01 00:00:00 UTC.
  */
static inline double hbitime_frac_seconds() {
    struct timeval tv;
    gettimeofday(&tv, 0);
    return tv.tv_sec + tv.tv_usec * 1.0e-6 - hbi_tm_offset_frac_seconds;
}

/**
 * Gets the value of the HBI_TM_OFFSET in microseconds.
 */
static inline long hbitime_get_offset_micros() {
    return hbi_tm_offset_micros;
}

/**
 * Gets the value of the HBI_TM_OFFSET in seconds.
 */
static inline int hbitime_get_offset_seconds() {
    return hbi_tm_offset_seconds;
}

/**
 * Gets the value of the microseconds part of HBI_TM_OFFSET in microseconds.
 */
static inline int hbitime_get_offset_micros_part() {
    return hbi_tm_offset_micros_part;
}

/**
 * Gets the value of the HBI_TM_OFFSET in fractional seconds.
 */
static inline double hbitime_get_offset_frac_seconds() {
    return hbi_tm_offset_frac_seconds;
}

/**
 * Rereads the @c HBI_TM_OFFSET environment variable.
 */
void hbitime_reset();

#ifdef __cplusplus
}
#endif

#endif
