/* Copyright (C) and/or used under license by RGM Advisors, LLC. All Rights Reserved Worldwide. */

#ifndef _common_logging_h_
#define _common_logging_h_

#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

extern FILE* logging_stream;

void logging_printf(char const* level, char const* fmt, ...);

void logging_set_stream(FILE* stream);

FILE* logging_get_stream();

// These macros can be used just like printf, but they automatically handle newlines

#define USAGEF(fmt, params...)   do { logging_printf("usage",  fmt, ## params); } while (0)
#define FATALF(fmt, params...)   do { logging_printf("fatal",  fmt, ## params); } while (0)
#define ERRORF(fmt, params...)   do { logging_printf("error",  fmt, ## params); } while (0)
#define WARNF(fmt, params...)    do { logging_printf("warn",   fmt, ## params); } while (0)
#define NOTEF(fmt, params...)    do { logging_printf("note",   fmt, ## params); } while (0)
#define STATSF(fmt, params...)   do { logging_printf("stats",  fmt, ## params); } while (0)
#define INFOF(fmt, params...)    do { logging_printf("info",   fmt, ## params); } while (0)
#define DEBUGF(fmt, params...)   do { logging_printf("debug",  fmt, ## params); } while (0)
#if defined(DEVLOG_ENABLE)
#define DEVLOGF(fmt, params...)  do { logging_printf("devlog", fmt, ## params); } while (0)
#else
#define DEVLOGF(fmt, params...)
#endif

#ifdef __cplusplus
}
#endif

#endif
