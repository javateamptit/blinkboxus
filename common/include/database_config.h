/* Copyright (C) and/or used under license by RGM Advisors, LLC. All Rights Reserved Worldwide. */

#ifndef _common_database_config_h_
#define _common_database_config_h_

/** @file
 * @brief C functions for accessing database configuration files.
 * These functions read values from files in the @c \$(BLINKBOX_HOME)/config/misc directory.
 * For a given database name, the file name will be the database with a @c _db suffix.
 */

#ifdef __cplusplus
extern "C" {
#endif

/**
  * Stores the monthly database connection string into @a buffer.
  * @param buffer A buffer to hold the output.
  * @param len The length of @a buffer.
  * @return @c 0 in case of error, @a buffer otherwise.
  */
char* get_monthly_db_connection_string(char const* db_name, int year, int month, char* buffer, int len);

/**
  * Stores the monthly database name into @a buffer.
  * @param buffer A buffer to hold the output.
  * @param len The length of @a buffer.
  * @return 0 in case of error, @a buffer otherwise.
  */
char* get_monthly_db_name(char const* db_name, int year, int month, char* buffer, int len);

/**
  * Returns the database hostname.
  * @return 0 in case of error, the database hostname otherwise.
  */
char const* get_db_hostname(char const* db_name);

#ifdef __cplusplus
}
#endif

#endif
