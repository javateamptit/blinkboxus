/* Copyright (C) and/or used under license by RGM Advisors, LLC. All Rights Reserved Worldwide. */

#include "logging.h"

#include "tempfile.h"

#include <gtest/gtest.h>

namespace {

class LoggingTest : public testing::Test
{
protected:
    LoggingTest() {
        _saved_stream = logging_get_stream();
        char const* fn = _tf.get_name();
        _logging_stream = fopen(fn, "w");
        if (_logging_stream) {
            logging_set_stream(_logging_stream);
        }
    }
    ~LoggingTest() {
        if (_logging_stream) {
            fclose(_logging_stream);
        }
        logging_set_stream(_saved_stream);
    }

    void check(char const* expected) {
        ASSERT_TRUE(_logging_stream);
        fclose(_logging_stream);
        logging_set_stream(_saved_stream);
        _logging_stream = 0;
        std::string input;
        char buffer[4096];
        char const* fn = _tf.get_name();
        FILE* fp = fopen(fn, "r");
        ASSERT_TRUE(fp);
        while (!feof(fp)) {
            input.append(buffer, fread(buffer, 1, sizeof buffer, fp));
            ASSERT_FALSE(ferror(fp));
        }
        fclose(fp);
        EXPECT_STREQ(expected, input.c_str());
    }

    Tempfile _tf;
    FILE* _logging_stream;
    FILE* _saved_stream;
};


TEST_F(LoggingTest, logging) {
    logging_printf("no_arg", "testing");
    logging_printf("one_arg", "test%s", "ing");
    logging_printf("lines", "line1\nline2\nline3");
    logging_printf("empty", "");
    logging_printf("newline", "\n");
    logging_printf("extra", "word\n");
    check(
        "no_arg: testing\n"
        "one_arg: testing\n"
        "lines: line1\n"
        "lines: line2\n"
        "lines: line3\n"
        "empty: \n"
        "newline: \n"
        "extra: word\n"
    );
}

TEST_F(LoggingTest, levels) {
    ERRORF("testing");
    ERRORF("test%s", "ing");
    WARNF("testing");
    WARNF("test%s", "ing");
    NOTEF("testing");
    NOTEF("test%s", "ing");
    INFOF("testing");
    INFOF("test%s", "ing");
    DEBUGF("testing");
    DEBUGF("test%s", "ing");
    check(
        "error: testing\n"
        "error: testing\n"
        "warn: testing\n"
        "warn: testing\n"
        "note: testing\n"
        "note: testing\n"
        "info: testing\n"
        "info: testing\n"
        "debug: testing\n"
        "debug: testing\n"
    );
}

}


