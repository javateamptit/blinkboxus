/* Copyright (C) and/or used under license by RGM Advisors, LLC. All Rights Reserved Worldwide. */

#include "tempfile.h"

#include <cstdlib>
#include <cstdio>
#include <unistd.h>

Tempfile::Tempfile() {
    char fn[] = "tmp.XXXXXX";
    int fd = mkstemp(fn);
    if (fd >= 0) {
        close(fd);
        _name = fn;
    }
}

Tempfile::~Tempfile() {
    if (!_name.empty()) {
        unlink(_name.c_str());
    }
}

char const* Tempfile::get_name() {
    return _name.c_str();
}

bool Tempfile::write(char const* contents) {
    if (_name.empty()) return false;
    FILE* fp = fopen(_name.c_str(), "w");
    if (!fp) return false;
    fprintf(fp, contents);
    fclose(fp);
    return true;
}

Tempdir::Tempdir() {
    char fn[] = "tmp.XXXXXX";
    char* res = mkdtemp(fn);
    if (res) _name = fn;
}

Tempdir::~Tempdir() {
    if (!_name.empty()) {
        std::string cmd("rm -rf ");
        cmd += _name;
        system(cmd.c_str());
    }
}

char const* Tempdir::get_name() {
    return _name.c_str();
}

bool Tempdir::mkdir(char const* name) {
    if (_name.empty()) return false;
    std::string cmd = std::string("mkdir -p ") + _name.c_str() + "/" + name;
    int res = system(cmd.c_str());
    return (res == 0);
}

bool Tempdir::write_file(char const* name, char const* contents) {
    if (_name.empty()) return false;
    std::string fn = _name + "/" + name;
    FILE* fp = fopen(fn.c_str(), "w");
    if (!fp) return false;
    fprintf(fp, contents);
    fclose(fp);
    return true;
}

