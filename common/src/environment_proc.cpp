/* Copyright (C) and/or used under license by RGM Advisors, LLC. All Rights Reserved Worldwide. */

#include "environment_proc.h"

#include "environment.h"

#include <cstdio>

char* Environment_get_book_conf_filename(char const* name, char* buffer, int len) {
    int res = snprintf(buffer, len, Environment::instance().get_book_conf_filename(name).c_str());
    return res >= len ? 0 : buffer;
}

char* Environment_get_order_conf_filename(char const* name, char* buffer, int len) {
    int res = snprintf(buffer, len, Environment::instance().get_order_conf_filename(name).c_str());
    return res >= len ? 0 : buffer;
}

char* Environment_get_symbol_conf_filename(char const* name, char* buffer, int len) {
    int res = snprintf(buffer, len, Environment::instance().get_symbol_conf_filename(name).c_str());
    return res >= len ? 0 : buffer;
}

char* Environment_get_misc_conf_filename(char const* name, char* buffer, int len) {
    int res = snprintf(buffer, len, Environment::instance().get_misc_conf_filename(name).c_str());
    return res >= len ? 0 : buffer;
}

char* Environment_get_ta2_conf_filename(char const* name, char* buffer, int len) {
    int res = snprintf(buffer, len, Environment::instance().get_ta2_conf_filename(name).c_str());
    return res >= len ? 0 : buffer;
}

char* Environment_get_ta_conf_filename(char const* name, char* buffer, int len) {
    int res = snprintf(buffer, len, Environment::instance().get_ta_conf_filename(name).c_str());
    return res >= len ? 0 : buffer;
}

char* Environment_get_root_data_dir(char* buffer, int len) {
    int res = snprintf(buffer, len, Environment::instance().get_root_data_dir().c_str());
    return res >= len ? 0 : buffer;
}

char* Environment_get_data_filename(char const* name, char* buffer, int len) {
    int res = snprintf(buffer, len, Environment::instance().get_data_filename(name).c_str());
    return res >= len ? 0 : buffer;
}

char* Environment_get_home_filename(char const* name, char* buffer, int len) {
    int res = snprintf(buffer, len, Environment::instance().get_home_filename(name).c_str());
    return res >= len ? 0 : buffer;
}

char* Environment_get_hbi_filename(char const* name, char* buffer, int len) {
    int res = snprintf(buffer, len, Environment::instance().get_hbi_filename(name).c_str());
    return res >= len ? 0 : buffer;
}

char* Environment_get_hbi_preserved_filename(char const* name, char* buffer, int len) {
    int res = snprintf(buffer, len, Environment::instance().get_preserved_hbi_filename(name).c_str());
    return res >= len ? 0 : buffer;
}

char* Environment_get_hbi_symbol_config_filename(char const* name, char* buffer, int len) {
    int res = snprintf(buffer, len, Environment::instance().get_symbol_config_hbi_filename(name).c_str());
    return res >= len ? 0 : buffer;
}

void Environment_reset() {
    Environment::instance().reset();
}
