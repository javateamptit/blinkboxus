/* Copyright (C) and/or used under license by RGM Advisors, LLC. All Rights Reserved Worldwide. */

#include "environment_proc.h"

#include "environment.h"

#include <gtest/gtest.h>

namespace {

TEST(EnvironmentProc, ok_buffer_conf) { 
    EnvironmentSaver saver1("BLINKBOX_HOME");
    EnvironmentSaver saver2("BLINKBOX_DATA");
    unsetenv("BLINKBOX_HOME");
    unsetenv("BLINKBOX_DATA");
    Environment_reset();
    char buffer[100];
    char* res = Environment_get_book_conf_filename("test1", buffer, 100);
    EXPECT_EQ(std::string("/home/hbi/blink/config/book/test1"), std::string(buffer));
    EXPECT_EQ(buffer, res);
}

TEST(EnvironmentProc, short_buffer_conf) {
    EnvironmentSaver saver1("BLINKBOX_HOME");
    EnvironmentSaver saver2("BLINKBOX_DATA");
    unsetenv("BLINKBOX_HOME");
    unsetenv("BLINKBOX_DATA");
    Environment_reset();
    char buffer[10];
    char* res = Environment_get_book_conf_filename("test1", buffer, 10);
    EXPECT_EQ(0, res);
}

TEST(EnvironmentProc, zero_buffer_conf) {
    EnvironmentSaver saver1("BLINKBOX_HOME");
    EnvironmentSaver saver2("BLINKBOX_DATA");
    unsetenv("BLINKBOX_HOME");
    unsetenv("BLINKBOX_DATA");
    Environment_reset();
    char buffer[0];
    char* res = Environment_get_book_conf_filename("test1", buffer, 0);
    EXPECT_EQ(0, res);
}

TEST(EnvironmentProc, ok_buffer_data) { 
    EnvironmentSaver saver1("BLINKBOX_HOME");
    EnvironmentSaver saver2("BLINKBOX_DATA");
    unsetenv("BLINKBOX_HOME");
    unsetenv("BLINKBOX_DATA");
    Environment_reset();
    char buffer[100];
    char* res = Environment_get_data_filename("test1", buffer, 100);
    EXPECT_EQ(std::string("/home/hbi/hbi/logs/test1"), std::string(buffer));
    EXPECT_EQ(buffer, res);
}

TEST(EnvironmentProc, short_buffer_data) {
    EnvironmentSaver saver1("BLINKBOX_HOME");
    EnvironmentSaver saver2("BLINKBOX_DATA");
    unsetenv("BLINKBOX_HOME");
    unsetenv("BLINKBOX_DATA");
    Environment_reset();
    char buffer[10];
    char* res = Environment_get_data_filename("test1", buffer, 10);
    EXPECT_EQ(0, res);
}

TEST(EnvironmentProc, zero_buffer_data) {
    EnvironmentSaver saver1("BLINKBOX_HOME");
    EnvironmentSaver saver2("BLINKBOX_DATA");
    unsetenv("BLINKBOX_HOME");
    unsetenv("BLINKBOX_DATA");
    Environment_reset();
    char buffer[0];
    char* res = Environment_get_data_filename("test1", buffer, 0);
    EXPECT_EQ(0, res);
}

TEST(EnvironmentProc, ok_buffer_home) { 
    EnvironmentSaver saver1("BLINKBOX_HOME");
    EnvironmentSaver saver2("BLINKBOX_DATA");
    unsetenv("BLINKBOX_HOME");
    unsetenv("BLINKBOX_DATA");
    Environment_reset();
    char buffer[100];
    char* res = Environment_get_home_filename("test1", buffer, 100);
    EXPECT_EQ(std::string("/home/hbi/blink/test1"), std::string(buffer));
    EXPECT_EQ(buffer, res);
}

TEST(EnvironmentProc, short_buffer_home) {
    EnvironmentSaver saver1("BLINKBOX_HOME");
    EnvironmentSaver saver2("BLINKBOX_DATA");
    unsetenv("BLINKBOX_HOME");
    unsetenv("BLINKBOX_DATA");
    Environment_reset();
    char buffer[10];
    char* res = Environment_get_home_filename("test1", buffer, 10);
    EXPECT_EQ(0, res);
}

TEST(EnvironmentProc, zero_buffer_home) {
    EnvironmentSaver saver1("BLINKBOX_HOME");
    EnvironmentSaver saver2("BLINKBOX_DATA");
    unsetenv("BLINKBOX_HOME");
    unsetenv("BLINKBOX_DATA");
    Environment_reset();
    char buffer[0];
    char* res = Environment_get_home_filename("test1", buffer, 0);
    EXPECT_EQ(0, res);
}

}

