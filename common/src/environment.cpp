/* Copyright (C) and/or used under license by RGM Advisors, LLC. All Rights Reserved Worldwide. */

#include "environment.h"

#include <cstdlib>

namespace {

Environment _environment;

}

Environment& Environment::instance() {
    return _environment;
}

Environment::Environment() {
    reset();
}

std::string Environment::get_root_data_dir() {
    return _bb_data;
}

std::string Environment::get_root_conf_dir() {
    return _bb_home + "/config";
}

std::string Environment::get_root_bin_dir() {
    return _bb_home + "/bin";
}

std::string Environment::get_data_filename(char const* name) {
    return _bb_data + "/" + name;
}

std::string Environment::get_home_filename(char const* name) {
    return _bb_home + "/" + name;
}

std::string Environment::get_book_conf_filename(char const* name) {
    return get_conf_filename("book", name);
}

std::string Environment::get_order_conf_filename(char const* name) {
    return get_conf_filename("order", name);
}

std::string Environment::get_symbol_conf_filename(char const* name) {
    return get_conf_filename("symbol", name);
}

std::string Environment::get_misc_conf_filename(char const* name) {
    return get_conf_filename("misc", name);
}

std::string Environment::get_ta2_conf_filename(char const* name) {
    return get_conf_filename("ta2", name);
}

std::string Environment::get_ta_conf_filename(char const* name) {
    return get_conf_filename("ta", name);
}

std::string Environment::get_preserved_hbi_filename(char const* name) {
    return get_hbi_filename("preserved", name);
}

std::string Environment::get_symbol_config_hbi_filename(char const* name) {
    return get_hbi_filename("config/symbol_config", name);
}

std::string Environment::get_conf_filename(char const* subdir, char const* name) {
    return _bb_home + "/config/" + subdir + "/" + name;
}

std::string Environment::get_hbi_filename(char const* subdir, char const* name) {
    return _hbi_root + "/" + subdir + "/" + name;
}

std::string Environment::get_hbi_filename(char const* name) {
    return _hbi_root + "/" + name;
}

void Environment::reset() {
    char const* const bb_home = getenv("BLINKBOX_HOME");
    _bb_home = bb_home == 0 ? "/home/hbi/blink" : bb_home;
    char const* const bb_data = getenv("BLINKBOX_DATA");
    _bb_data = bb_data == 0 ? "/home/hbi/hbi/logs" : bb_data;
	char const* const hbi_root = getenv("HBI_ROOT");
    _hbi_root = hbi_root == 0 ? "/home/hbi/hbi" : hbi_root;
}

EnvironmentSaver::EnvironmentSaver(char const* name)
    : _name(name)
{
    char* value = getenv(_name.c_str());
    if (value) {
        _was_found = true;
        _saved_value = value;
    } else {
        _was_found = false;
    }
}

EnvironmentSaver::~EnvironmentSaver() {
    if (_was_found) {
        setenv(_name.c_str(), _saved_value.c_str(), 1);
    } else {
        unsetenv(_name.c_str());
    }
}
