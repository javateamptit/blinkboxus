/* Copyright (C) and/or used under license by RGM Advisors, LLC. All Rights Reserved Worldwide. */

#include "logging.h"

#include <stdarg.h>
#include <stdlib.h>
#include <string.h>

FILE* logging_stream = stderr;

void logging_printf(char const* level, char const* fmt, ...) {

    // get the entire formatted message
    char* buffer;
    int rc;
    va_list ap;
    va_start (ap, fmt);
    rc = vasprintf(&buffer, fmt, ap);
    va_end (ap);
    if (rc == -1 || buffer == 0) {
	// Most likely problem is failure to allocate memory.
	//throw std::bad_alloc();
    }

    size_t len = strlen(buffer);
    char* end = buffer + len; // pointer to trailing null byte
    char* line = buffer;

    do {
        char* line_end = strchrnul(line, '\n');
        *line_end = 0;
        fprintf(logging_stream, "%s: %s\n", level, line);
        line = line_end + 1; // skip newline
    } while (line < end);

    free(buffer);

}

void logging_set_stream(FILE* stream) {
    logging_stream = stream;
}

FILE* logging_get_stream() {
    return logging_stream;
}
