/* Copyright (C) and/or used under license by RGM Advisors, LLC. All Rights Reserved Worldwide. */

#include "database_config.h"

#include "environment.h"
#include "parameters.h"
#include "tempfile.h"

#include <gtest/gtest.h>

namespace {

TEST(DatabaseConfig, load_file_basic) {
    EnvironmentSaver saver1("BLINKBOX_HOME");
    Tempdir tempdir;
    char const* tmp_dn = tempdir.get_name();
    setenv("BLINKBOX_HOME", tmp_dn, 1);
    Environment::instance().reset();
    Parameters::instance().reset();
    ASSERT_TRUE(tempdir.mkdir("config/misc"));
    ASSERT_TRUE(tempdir.write_file("config/misc/eaa_db",
        "pgsql_dir = dir1/\n"
        "db_hostname = host1\n"
        "db_name = pref1\n"
        "db_username = user1\n"
        "db_password = pass1\n"
    ));
    char buffer[100];
    EXPECT_STREQ("host1", get_db_hostname("eaa"));
    EXPECT_STREQ("pref1_201311", get_monthly_db_name("eaa", 2013, 11, buffer, 100));
    EXPECT_STREQ("dbname=pref1_201311 user=user1 password=pass1 host=host1", get_monthly_db_connection_string("eaa", 2013, 11, buffer, 100));
}

}

