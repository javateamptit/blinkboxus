/* Copyright (C) and/or used under license by RGM Advisors, LLC. All Rights Reserved Worldwide. */

#include "database_config.h"

#include "environment.h"
#include "logging.h"
#include "parameters.h"

char* get_monthly_db_connection_string(char const* db_name, int year, int month, char* buffer, int len) {
    Environment& env = Environment::instance();
    std::string fn(db_name);
    fn += "_db";
    fn = env.get_misc_conf_filename(fn.c_str());
    DEBUGF("file name of database configuration = %s", fn.c_str());
	
    Parameters& p = Parameters::instance();
    char const* db_hostname = p.get(fn.c_str(), "db_hostname", 0);
    char const* db_username = p.get(fn.c_str(), "db_username", 0);
    char const* db_password = p.get(fn.c_str(), "db_password", 0);
    char const* db_name_prefix = p.get(fn.c_str(), "db_name", 0);
    if (db_hostname == 0) return 0;
    if (db_username == 0) return 0;
    if (db_password == 0) return 0;
    if (db_name_prefix == 0) return 0;
    int res = snprintf(buffer, len, "dbname=%s_%04d%02d user=%s password=%s host=%s",
        db_name_prefix,
        year,
        month,
        db_username,
        db_password,
        db_hostname
    );
    return res >= len ? 0 : buffer;
}

char* get_monthly_db_name(char const* db_name, int year, int month, char* buffer, int len) {
    Environment& env = Environment::instance();
    std::string fn(db_name);
    fn += "_db";
    fn = env.get_misc_conf_filename(fn.c_str());
    Parameters& p = Parameters::instance();
    char const* db_name_prefix = p.get(fn.c_str(), "db_name", 0);
    if (db_name_prefix == 0) return 0;
    int res = snprintf(buffer, len, "%s_%04d%02d",
        db_name_prefix,
        year,
        month
    );
    return res >= len ? 0 : buffer;
}

char const* get_db_hostname(char const* db_name) {
    Environment& env = Environment::instance();
    std::string fn(db_name);
    fn += "_db";
    fn = env.get_misc_conf_filename(fn.c_str());
    Parameters& p = Parameters::instance();
    return p.get(fn.c_str(), "db_hostname", 0);
}

