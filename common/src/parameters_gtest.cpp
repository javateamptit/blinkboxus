/* Copyright (C) and/or used under license by RGM Advisors, LLC. All Rights Reserved Worldwide. */

#include "parameters.h"

#include "tempfile.h"

#include <cstdlib>
#include <gtest/gtest.h>

namespace {

TEST(Parameters, trim) {
    EXPECT_EQ("abc", Parameters::trim("  abc  "));
    EXPECT_EQ("abc", Parameters::trim("abc  "));
    EXPECT_EQ("abc", Parameters::trim("  abc"));
    EXPECT_EQ("abc", Parameters::trim(" \t abc \t "));
    EXPECT_EQ("abc  def", Parameters::trim("  abc  def  "));
    EXPECT_EQ("", Parameters::trim("  "));
}

TEST(Parameters, parse_line_basic) {
    typedef std::pair<std::string, std::string> Pair;
    EXPECT_EQ(Pair("ab", "cd"), Parameters::parse_line("ab=cd"));
    EXPECT_EQ(Pair("ab", "cd"), Parameters::parse_line("ab = cd"));
    EXPECT_EQ(Pair("ab", "cd"), Parameters::parse_line(" ab = cd "));
}

TEST(Parameters, parse_line_no_value) {
    typedef std::pair<std::string, std::string> Pair;
    EXPECT_EQ(Pair("ab", ""), Parameters::parse_line("ab"));
    EXPECT_EQ(Pair("ab", ""), Parameters::parse_line("ab "));
    EXPECT_EQ(Pair("ab", ""), Parameters::parse_line(" ab "));
    EXPECT_EQ(Pair("ab", ""), Parameters::parse_line("ab="));
    EXPECT_EQ(Pair("ab", ""), Parameters::parse_line("ab ="));
    EXPECT_EQ(Pair("ab", ""), Parameters::parse_line(" ab ="));
    EXPECT_EQ(Pair("ab", ""), Parameters::parse_line(" ab = "));
}

TEST(Parameters, parse_line_empty) {
    typedef std::pair<std::string, std::string> Pair;
    EXPECT_EQ(Pair("", ""), Parameters::parse_line(""));
    EXPECT_EQ(Pair("", ""), Parameters::parse_line("  "));
}

TEST(Parameters, parse_line_complex) {
    typedef std::pair<std::string, std::string> Pair;
    EXPECT_EQ(Pair("ab", "cd = ef"), Parameters::parse_line("ab=cd = ef"));
    EXPECT_EQ(Pair("ab", "cd = ef"), Parameters::parse_line("ab = cd = ef"));
    EXPECT_EQ(Pair("ab", "cd = ef"), Parameters::parse_line(" ab = cd = ef "));
    EXPECT_EQ(Pair("ab cd", "ef"), Parameters::parse_line(" ab cd = ef "));
}

TEST(Parameters, load_file_basic) {
    Parameters parameters;
    Tempfile tf1;
    char const* fn1 = tf1.get_name();
    FILE* file1 = fopen(fn1, "w");
    fprintf(file1, "n1 = v1\n");
    fprintf(file1, "n2 = v2\n");
    fclose(file1);
    EXPECT_STREQ("v1", parameters.get(fn1, "n1", 0));
    EXPECT_STREQ("v2", parameters.get(fn1, "n2", 0));
    EXPECT_STREQ(0, parameters.get(fn1, "n3", 0));
    EXPECT_STREQ("v3", parameters.get(fn1, "n3", "v3"));
    EXPECT_STREQ(0, parameters.get("missing", "n1", 0));
}

TEST(Parameters, load_file_int) {
    Parameters parameters;
    Tempfile tf1;
    char const* fn1 = tf1.get_name();
    FILE* file1 = fopen(fn1, "w");
    fprintf(file1, "n1 = 1\n");
    fprintf(file1, "n2 = 2 \n");
    fprintf(file1, "n3 = a3\n");
    fprintf(file1, "n4 = 4a\n");
    fprintf(file1, "n5 = 5.5\n");
    fclose(file1);
    EXPECT_EQ(42, parameters.get_int(fn1, "n0", 42));
    EXPECT_EQ(1, parameters.get_int(fn1, "n1", 42));
    EXPECT_EQ(2, parameters.get_int(fn1, "n2", 42));
    EXPECT_EQ(42, parameters.get_int(fn1, "n3", 42));
    EXPECT_EQ(42, parameters.get_int(fn1, "n4", 42));
    EXPECT_EQ(42, parameters.get_int(fn1, "n5", 42));
    EXPECT_EQ(42, parameters.get_int("missing", "n1", 42));
}

TEST(Parameters, load_file_double) {
    Parameters parameters;
    Tempfile tf1;
    char const* fn1 = tf1.get_name();
    FILE* file1 = fopen(fn1, "w");
    fprintf(file1, "n1 = 1.1\n");
    fprintf(file1, "n2 = 2.2 \n");
    fprintf(file1, "n3 = a3.3\n");
    fprintf(file1, "n4 = 4.4a\n");
    fclose(file1);
    EXPECT_NEAR(42, parameters.get_double(fn1, "n0", 42), 0.01);
    EXPECT_NEAR(1.1, parameters.get_double(fn1, "n1", 42), 0.01);
    EXPECT_NEAR(2.2, parameters.get_double(fn1, "n2", 42), 0.01);
    EXPECT_NEAR(42, parameters.get_double(fn1, "n3", 42), 0.01);
    EXPECT_NEAR(42, parameters.get_double(fn1, "n4", 42), 0.01);
    EXPECT_NEAR(42, parameters.get_double("missing", "n1", 42), 0.01);
}

TEST(Parameters, load_file_comments) {
    Parameters parameters;
    Tempfile tf1;
    char const* fn1 = tf1.get_name();
    FILE* file1 = fopen(fn1, "w");
    fprintf(file1, "n1 = v1\n");
    fprintf(file1, "\n");
    fprintf(file1, "#comment\n");
    fprintf(file1, "# = this is a comment\n");
    fprintf(file1, "#this = is also a comment\n");
    fprintf(file1, "n2 = v2\n");
    fclose(file1);
    EXPECT_STREQ("v1", parameters.get(fn1, "n1", 0));
    EXPECT_STREQ("v2", parameters.get(fn1, "n2", 0));
    EXPECT_STREQ(0, parameters.get(fn1, "n3", 0));
    EXPECT_STREQ("v3", parameters.get(fn1, "n3", "v3"));
    EXPECT_STREQ(0, parameters.get(fn1, "", 0));
    EXPECT_STREQ(0, parameters.get(fn1, "#", 0));
    EXPECT_STREQ(0, parameters.get(fn1, "#comment", 0));
    EXPECT_STREQ(0, parameters.get(fn1, "#this", 0));
    EXPECT_STREQ(0, parameters.get("missing", "n1", 0));
}

TEST(Parameters, load_file_no_nl) {
    Parameters parameters;
    Tempfile tf1;
    char const* fn1 = tf1.get_name();
    FILE* file1 = fopen(fn1, "w");
    fprintf(file1, "n1 = v1\n");
    fprintf(file1, "n2 = v2");
    fclose(file1);
    EXPECT_STREQ("v1", parameters.get(fn1, "n1", 0));
    EXPECT_STREQ("v2", parameters.get(fn1, "n2", 0));
    EXPECT_STREQ(0, parameters.get(fn1, "n3", 0));
    EXPECT_STREQ("v3", parameters.get(fn1, "n3", "v3"));
    EXPECT_STREQ(0, parameters.get("missing", "n1", 0));
}

TEST(Parameters, load_file_missing) {
    Parameters parameters;
    EXPECT_STREQ(0, parameters.get("file1", "n3", 0));
    EXPECT_STREQ("v3", parameters.get("file1", "n3", "v3"));
}

TEST(Parameters, load_file_multi) {
    Parameters parameters;
    Tempfile tf1;
    char const* fn1 = tf1.get_name();
    FILE* file1 = fopen(fn1, "w");
    fprintf(file1, "n1 = v1\n");
    fprintf(file1, "n2 = v2\n");
    fclose(file1);
    Tempfile tf2;
    char const* fn2 = tf2.get_name();
    FILE* file2 = fopen(fn2, "w");
    fprintf(file2, "n1 = v3\n");
    fprintf(file2, "n2 = v4\n");
    fclose(file2);
    EXPECT_STREQ("v1", parameters.get(fn1, "n1", 0));
    EXPECT_STREQ("v2", parameters.get(fn1, "n2", 0));
    EXPECT_STREQ("v3", parameters.get(fn2, "n1", 0));
    EXPECT_STREQ("v4", parameters.get(fn2, "n2", 0));
}

TEST(Parameters, load_file_dos) {
    Parameters parameters;
    Tempfile tf1;
    char const* fn1 = tf1.get_name();
    FILE* file1 = fopen(fn1, "w");
    fprintf(file1, "n1 = v1\r\n");
    fprintf(file1, "n2 = v2\r\n");
    fclose(file1);
    Tempfile tf2;
    char const* fn2 = tf2.get_name();
    FILE* file2 = fopen(fn2, "w");
    fprintf(file2, "n1 = v3\r\n");
    fprintf(file2, "n2 = v4\r\n");
    fclose(file2);
    EXPECT_STREQ("v1", parameters.get(fn1, "n1", 0));
    EXPECT_STREQ("v2", parameters.get(fn1, "n2", 0));
    EXPECT_STREQ("v3", parameters.get(fn2, "n1", 0));
    EXPECT_STREQ("v4", parameters.get(fn2, "n2", 0));
}

}

