/* Copyright (C) and/or used under license by RGM Advisors, LLC. All Rights Reserved Worldwide. */

#include "hbitime.h"

#include <cstdlib>

long hbi_tm_offset_micros = 0;
int hbi_tm_offset_seconds = 0;
int hbi_tm_offset_micros_part = 0;
double hbi_tm_offset_frac_seconds = 0;

class HbiTimeManager {
public:
    HbiTimeManager();
    void reset();

private:
    static void static_init();

};

HbiTimeManager::HbiTimeManager() {
    reset();
}

void HbiTimeManager::reset() {
    static_init();
}


void HbiTimeManager::static_init() {
    hbi_tm_offset_frac_seconds = 0;
    char const* offset_string = getenv("HBI_TM_OFFSET");
    if (offset_string) {
        hbi_tm_offset_frac_seconds = atof(offset_string);
        hbi_tm_offset_seconds = (int) hbi_tm_offset_frac_seconds;
        hbi_tm_offset_micros = (long) (hbi_tm_offset_frac_seconds * 1000 * 1000);
        hbi_tm_offset_micros_part = hbi_tm_offset_micros % (1000 * 1000);
    } else {
        hbi_tm_offset_frac_seconds = 0;
        hbi_tm_offset_seconds = 0;
        hbi_tm_offset_micros = 0;
        hbi_tm_offset_micros_part = 0;
    }
}

namespace {

HbiTimeManager _hbi_tm_manager;

}

void hbitime_reset() {
    _hbi_tm_manager.reset();
}

