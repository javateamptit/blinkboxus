/* Copyright (C) and/or used under license by RGM Advisors, LLC. All Rights Reserved Worldwide. */

#include "environment.h"

#include <gtest/gtest.h>

namespace {

TEST(Environment, default_conf) {
    EnvironmentSaver saver1("BLINKBOX_HOME");
    EnvironmentSaver saver2("BLINKBOX_DATA");
    unsetenv("BLINKBOX_HOME");
    unsetenv("BLINKBOX_DATA");
    Environment environment;
    EXPECT_EQ("/home/hbi/blink/config/book/test1", environment.get_book_conf_filename("test1"));
    EXPECT_EQ("/home/hbi/blink/config/order/test1", environment.get_order_conf_filename("test1"));
    EXPECT_EQ("/home/hbi/blink/config/symbol/test1", environment.get_symbol_conf_filename("test1"));
    EXPECT_EQ("/home/hbi/blink/config/misc/test1", environment.get_misc_conf_filename("test1"));
	EXPECT_EQ("/home/hbi/blink/config/ta2/test1", environment.get_ta2_conf_filename("test1"));
	EXPECT_EQ("/home/hbi/blink/config/ta/test1", environment.get_ta_conf_filename("test1"));
}

TEST(Environment, user_conf) {
    EnvironmentSaver saver1("BLINKBOX_HOME");
    EnvironmentSaver saver2("BLINKBOX_DATA");
    setenv("BLINKBOX_HOME", "/etc/bbtest1", 1);
    setenv("BLINKBOX_DATA", "/etc/bbtest2", 1);
    Environment environment;
    EXPECT_EQ("/etc/bbtest1/config/book/test1", environment.get_book_conf_filename("test1"));
    EXPECT_EQ("/etc/bbtest1/config/order/test1", environment.get_order_conf_filename("test1"));
    EXPECT_EQ("/etc/bbtest1/config/symbol/test1", environment.get_symbol_conf_filename("test1"));
    EXPECT_EQ("/etc/bbtest1/config/misc/test1", environment.get_misc_conf_filename("test1"));
    EXPECT_EQ("/etc/bbtest1/config/ta2/test1", environment.get_ta2_conf_filename("test1"));
    EXPECT_EQ("/etc/bbtest1/config/ta/test1", environment.get_ta_conf_filename("test1"));
}

TEST(Environment, default_data) {
    EnvironmentSaver saver1("BLINKBOX_HOME");
    EnvironmentSaver saver2("BLINKBOX_DATA");
    unsetenv("BLINKBOX_DATA");
    unsetenv("BLINKBOX_DATA");
    Environment environment;
    EXPECT_EQ("/home/hbi/hbi/logs", environment.get_root_data_dir());
}

TEST(Environment, user_data) {
    EnvironmentSaver saver1("BLINKBOX_HOME");
    EnvironmentSaver saver2("BLINKBOX_DATA");
    setenv("BLINKBOX_HOME", "/etc/bbtest1", 1);
    setenv("BLINKBOX_DATA", "/etc/bbtest2", 1);
    Environment environment;
    EXPECT_EQ("/etc/bbtest2", environment.get_root_data_dir());
}

TEST(Environment, default_home_filename) {
    EnvironmentSaver saver1("BLINKBOX_HOME");
    EnvironmentSaver saver2("BLINKBOX_DATA");
    unsetenv("BLINKBOX_DATA");
    unsetenv("BLINKBOX_DATA");
    Environment environment;
    EXPECT_EQ("/home/hbi/blink/test1", environment.get_home_filename("test1"));
}

TEST(Environment, user_home_filename) {
    EnvironmentSaver saver1("BLINKBOX_HOME");
    EnvironmentSaver saver2("BLINKBOX_DATA");
    setenv("BLINKBOX_HOME", "/etc/bbtest1", 1);
    setenv("BLINKBOX_DATA", "/etc/bbtest2", 1);
    Environment environment;
    EXPECT_EQ("/etc/bbtest1/test1", environment.get_home_filename("test1"));
}

TEST(Environment, default_data_filename) {
    EnvironmentSaver saver1("BLINKBOX_HOME");
    EnvironmentSaver saver2("BLINKBOX_DATA");
    unsetenv("BLINKBOX_DATA");
    unsetenv("BLINKBOX_DATA");
    Environment environment;
    EXPECT_EQ("/home/hbi/hbi/logs/test1", environment.get_data_filename("test1"));
}

TEST(Environment, user_data_filename) {
    EnvironmentSaver saver1("BLINKBOX_HOME");
    EnvironmentSaver saver2("BLINKBOX_DATA");
    setenv("BLINKBOX_HOME", "/etc/bbtest1", 1);
    setenv("BLINKBOX_DATA", "/etc/bbtest2", 1);
    Environment environment;
    EXPECT_EQ("/etc/bbtest2/test1", environment.get_data_filename("test1"));
}

}

