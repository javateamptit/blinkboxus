/* Copyright (C) and/or used under license by RGM Advisors, LLC. All Rights Reserved Worldwide. */

#include "parameters.h"

#include <cstdlib>

namespace {

Parameters _parameters;

}

Parameters& Parameters::instance() {
    return _parameters;
}

char const* Parameters::get(char const* filename, char const* pname, char const* default_value) {
    if (_files.find(filename) == _files.end()) {
        FILE* fp = fopen(filename, "r");
        if (!fp) return default_value;
        load_file(filename, fp);
        fclose(fp);
    }
    Pair key(filename, pname);
    Cache::iterator it = _cache.find(key);
    if (it == _cache.end()) {
        return default_value;
    }
    return it->second.c_str();
}

int Parameters::get_int(char const* filename, char const* pname, int default_value) {
    char const* value = get(filename, pname, 0);
    if (!value) return default_value;
    char* end = 0;
    int result = strtol(value, &end, 0);
    if (*value != 0 && *end == 0) {
        return result;
    } else {
        return default_value;
    }
}

long Parameters::get_long(char const* filename, char const* pname, long default_value) {
    char const* value = get(filename, pname, 0);
    if (!value) return default_value;
    char* end = 0;
    long result = strtol(value, &end, 0);
    if (*value != 0 && *end == 0) {
        return result;
    } else {
        return default_value;
    }
}

double Parameters::get_double(char const* filename, char const* pname, double default_value) {
    char const* value = get(filename, pname, 0);
    if (!value) return default_value;
    char* end = 0;
    double result = strtod(value, &end);
    if (*value != 0 && *end == 0) {
        return result;
    } else {
        return default_value;
    }
}

void Parameters::load_file(char const* filename, FILE* fp) {
    if (_files.find(filename) != _files.end()) return;
    _files.insert(filename);
    std::string line;
    while (true) {
        int c = fgetc(fp);
        if (c == EOF) {
            if (!line.empty()) add(filename, parse_line(line));
            return;
        } else if (c == '\n' || c == '\r') {
            add(filename, parse_line(line));
            line.clear();
        } else {
            line.push_back(c);
        }
    }
}

void Parameters::add(char const* filename, Pair nv) {
    if (nv.first.empty() || nv.first[0] == '#') return;
    Pair key(filename, nv.first);
    _cache.insert(std::make_pair(key, nv.second));
}

Parameters::Pair Parameters::parse_line(std::string const& line) {
    size_t pos = line.find_first_of('=');
    if (pos == std::string::npos) {
        return std::make_pair(trim(line), trim(""));
    } else {
        std::string name = trim(line.substr(0, pos));
        std::string value = trim(line.substr(pos + 1));
        return std::make_pair(name, value);
    }
}

std::string Parameters::trim(std::string data) {
    data.erase(0, data.find_first_not_of(" \t"));
    data.erase(data.find_last_not_of(" \t") + 1);
    return data;
}

void Parameters::reset() {
    _cache.clear();
    _files.clear();
}
