/* Copyright (C) and/or used under license by RGM Advisors, LLC. All Rights Reserved Worldwide. */

#include "parameters_proc.h"

#include "parameters.h"

#include <cstdio>

char const* Parameters_get(char const* filename, char const* pname, char const* default_value) {
    return Parameters::instance().get(filename, pname, default_value);
}

int Parameters_get_int(char const* filename, char const* pname, int default_value) {
    return Parameters::instance().get_int(filename, pname, default_value);
}

long Parameters_get_long(char const* filename, char const* pname, long default_value) {
    return Parameters::instance().get_long(filename, pname, default_value);
}

double Parameters_get_double(char const* filename, char const* pname, double default_value) {
    return Parameters::instance().get_double(filename, pname, default_value);
}

void Parameters_reset() {
    Parameters::instance().reset();
}

