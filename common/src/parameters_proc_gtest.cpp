/* Copyright (C) and/or used under license by RGM Advisors, LLC. All Rights Reserved Worldwide. */

#include "parameters_proc.h"

#include "tempfile.h"

#include <gtest/gtest.h>

namespace {

TEST(ParametersProc, load_file_basic) {
    Parameters_reset();
    Tempfile tf1;
    char const* fn1 = tf1.get_name();
    FILE* file1 = fopen(fn1, "w");
    fprintf(file1, "n1 = v1\n");
    fprintf(file1, "n2 = v2\n");
    fclose(file1);
    EXPECT_STREQ("v1", Parameters_get(fn1, "n1", 0));
    EXPECT_STREQ("v2", Parameters_get(fn1, "n2", 0));
    EXPECT_STREQ(0, Parameters_get(fn1, "n3", 0));
    EXPECT_STREQ("v3", Parameters_get(fn1, "n3", "v3"));
    EXPECT_STREQ(0, Parameters_get("missing", "n1", 0));
}

TEST(ParametersProc, load_file_int) {
    Parameters_reset();
    Tempfile tf1;
    char const* fn1 = tf1.get_name();
    FILE* file1 = fopen(fn1, "w");
    fprintf(file1, "n1 = 1\n");
    fprintf(file1, "n2 = 2 \n");
    fprintf(file1, "n3 = a3\n");
    fprintf(file1, "n4 = 4a\n");
    fprintf(file1, "n5 = 5.5\n");
    fclose(file1);
    EXPECT_EQ(42, Parameters_get_int(fn1, "n0", 42));
    EXPECT_EQ(1, Parameters_get_int(fn1, "n1", 42));
    EXPECT_EQ(2, Parameters_get_int(fn1, "n2", 42));
    EXPECT_EQ(42, Parameters_get_int(fn1, "n3", 42));
    EXPECT_EQ(42, Parameters_get_int(fn1, "n4", 42));
    EXPECT_EQ(42, Parameters_get_int(fn1, "n5", 42));
    EXPECT_EQ(42, Parameters_get_int("missing", "n1", 42));
}

TEST(ParametersProc, load_file_double) {
    Parameters_reset();
    Tempfile tf1;
    char const* fn1 = tf1.get_name();
    FILE* file1 = fopen(fn1, "w");
    fprintf(file1, "n1 = 1.1\n");
    fprintf(file1, "n2 = 2.2 \n");
    fprintf(file1, "n3 = a3.3\n");
    fprintf(file1, "n4 = 4.4a\n");
    fclose(file1);
    EXPECT_NEAR(42, Parameters_get_double(fn1, "n0", 42), 0.01);
    EXPECT_NEAR(1.1, Parameters_get_double(fn1, "n1", 42), 0.01);
    EXPECT_NEAR(2.2, Parameters_get_double(fn1, "n2", 42), 0.01);
    EXPECT_NEAR(42, Parameters_get_double(fn1, "n3", 42), 0.01);
    EXPECT_NEAR(42, Parameters_get_double(fn1, "n4", 42), 0.01);
    EXPECT_NEAR(42, Parameters_get_double("missing", "n1", 42), 0.01);
}

}

