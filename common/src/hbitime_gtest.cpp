/* Copyright (C) and/or used under license by RGM Advisors, LLC. All Rights Reserved Worldwide. */

#include "hbitime.h"

#include "environment.h"
#include "logging.h"

#include <gtest/gtest.h>

namespace {

TEST(HbiTime, no_offset) {
    EnvironmentSaver saver("HBI_TM_OFFSET");
    unsetenv("HBI_TM_OFFSET");
    hbitime_reset();

    double tm_fs = hbitime_frac_seconds();
    int tm_s = hbitime_seconds();
    long tm_us = hbitime_micros();
    int tm_s_part, tm_us_part;
    hbitime_micros_parts(&tm_s_part, &tm_us_part);

    // we would have to be really unlucky to be 2 sec apart
    // but 1 sec would be quite easy depending on the sampling times
    EXPECT_NEAR(tm_fs, tm_s * 1.0, 2.0);
    EXPECT_NEAR(tm_fs, tm_us * 1.0e-6, 2.0);
    EXPECT_NEAR(tm_fs, tm_s_part * 1.0, 2.0);
    EXPECT_NEAR((tm_us % (1000 * 1000)) * 1.0e-6, tm_us_part * 1.0e-6, 2.0);
}

TEST(HbiTime, with_offset) {
    EnvironmentSaver saver("HBI_TM_OFFSET");
    setenv("HBI_TM_OFFSET", "100009.25", 1);
    hbitime_reset();

    double tm_fs = hbitime_frac_seconds();
    int tm_s = hbitime_seconds();
    long tm_us = hbitime_micros();
    int tm_s_part, tm_us_part;
    hbitime_micros_parts(&tm_s_part, &tm_us_part);

    // we would have to be really unlucky to be 2 sec apart
    // but 1 sec would be quite easy depending on the sampling times
    EXPECT_NEAR(tm_fs, tm_s * 1.0, 2.0);
    EXPECT_NEAR(tm_fs, tm_us * 1.0e-6, 2.0);
    EXPECT_NEAR(tm_fs, tm_s_part * 1.0, 2.0);
    EXPECT_NEAR((tm_us % (1000 * 1000)) * 1.0e-6, tm_us_part * 1.0e-6, 2.0);
    EXPECT_LT(tm_us_part, 1000 * 1000);
    EXPECT_GE(tm_us_part, 0);

    time_t now;
    time(&now);

    // we should be within a couple seconds of the correct offset
    EXPECT_NEAR(tm_s * 1.0, now - 100009.0, 2.0);
}

TEST(HbiTime, parts_no_rollover) {
    EnvironmentSaver saver("HBI_TM_OFFSET");
    setenv("HBI_TM_OFFSET", "100009.25", 1);
    hbitime_reset();

    int tm_s_part, tm_us_part;
    int tm_base = 1386200000;
    tm_s_part = tm_base;
    tm_us_part = 400000;
    hbitime_add_offset_to_parts(&tm_s_part, &tm_us_part);
    EXPECT_EQ(tm_s_part, tm_base - 100009);
    EXPECT_EQ(tm_us_part, 150000);
}

TEST(HbiTime, parts_no_remainder) {
    EnvironmentSaver saver("HBI_TM_OFFSET");
    setenv("HBI_TM_OFFSET", "100009.25", 1);
    hbitime_reset();

    int tm_s_part, tm_us_part;
    int tm_base = 1386200000;
    tm_s_part = tm_base;
    tm_us_part = 250000;
    hbitime_add_offset_to_parts(&tm_s_part, &tm_us_part);
    EXPECT_EQ(tm_s_part, tm_base - 100009);
    EXPECT_EQ(tm_us_part, 0);
}

TEST(HbiTime, parts_rollover) {
    EnvironmentSaver saver("HBI_TM_OFFSET");
    setenv("HBI_TM_OFFSET", "100009.25", 1);
    hbitime_reset();

    int tm_s_part, tm_us_part;
    int tm_base = 1386200000;
    tm_s_part = tm_base;
    tm_us_part = 100000;
    hbitime_add_offset_to_parts(&tm_s_part, &tm_us_part);
    EXPECT_EQ(tm_s_part, tm_base - 100009 - 1);
    EXPECT_EQ(tm_us_part, 850000);
}

TEST(HbiTime, read_zero_offset) {
    EnvironmentSaver saver("HBI_TM_OFFSET");
    unsetenv("HBI_TM_OFFSET");
    hbitime_reset();

    EXPECT_EQ(0, hbitime_get_offset_seconds());
    EXPECT_EQ(0, hbitime_get_offset_micros());
    EXPECT_EQ(0, hbitime_get_offset_frac_seconds());
    EXPECT_EQ(0, hbitime_get_offset_micros_part());
}

TEST(HbiTime, read_frac_offset) {
    EnvironmentSaver saver("HBI_TM_OFFSET");
    setenv("HBI_TM_OFFSET", "100009.25", 1);
    hbitime_reset();

    EXPECT_EQ(100009, hbitime_get_offset_seconds());
    EXPECT_EQ(100009250000, hbitime_get_offset_micros());
    EXPECT_NEAR(100009.25, hbitime_get_offset_frac_seconds(), 0.001);
    EXPECT_EQ(250000, hbitime_get_offset_micros_part());
}

TEST(HbiTime, time_time_x1000000) {
    EnvironmentSaver saver("HBI_TM_OFFSET");
    setenv("HBI_TM_OFFSET", "100009.25", 1);
    hbitime_reset();

    int tm_s;
    time_t now;
    for (long i = 0; i < 1000000; ++i) {
        time(&now);
    }
    tm_s = now;
    DEBUGF("tm_s = %d", tm_s);
}

TEST(HbiTime, time_gettimeofday_x1000000) {
    EnvironmentSaver saver("HBI_TM_OFFSET");
    setenv("HBI_TM_OFFSET", "100009.25", 1);
    hbitime_reset();

    long tm_us;
    struct timeval tv;
    for (long i = 0; i < 1000000; ++i) {
        gettimeofday(&tv, 0);
    }
    tm_us = tv.tv_sec;
    tm_us = tm_us * 1000 * 1000 + tv.tv_usec;
    DEBUGF("tm_us = %ld", tm_us);
}

TEST(HbiTime, time_seconds_x1000000) {
    EnvironmentSaver saver("HBI_TM_OFFSET");
    setenv("HBI_TM_OFFSET", "100009.25", 1);
    hbitime_reset();

    int tm_s;
    for (long i = 0; i < 1000000; ++i) {
        tm_s = hbitime_seconds();
    }
    DEBUGF("tm_s = %d", tm_s);
}
TEST(HbiTime, time_frac_seconds_x1000000) {
    EnvironmentSaver saver("HBI_TM_OFFSET");
    setenv("HBI_TM_OFFSET", "100009.25", 1);
    hbitime_reset();

    double tm_fs;
    for (long i = 0; i < 1000000; ++i) {
        tm_fs = hbitime_frac_seconds();
    }
    DEBUGF("tm_fs = %.6f", tm_fs);
}

TEST(HbiTime, time_micros_x1000000) {
    EnvironmentSaver saver("HBI_TM_OFFSET");
    setenv("HBI_TM_OFFSET", "100009.25", 1);
    hbitime_reset();

    long tm_us;
    for (long i = 0; i < 1000000; ++i) {
        tm_us = hbitime_micros();
    }
    DEBUGF("tm_us = %ld", tm_us);
}

TEST(HbiTime, time_micros_divide_x1000000) {
    EnvironmentSaver saver("HBI_TM_OFFSET");
    setenv("HBI_TM_OFFSET", "100009.25", 1);
    hbitime_reset();

    int tm_s_part, tm_us_part;
    for (long i = 0; i < 1000000; ++i) {
        long tm_us = hbitime_micros();
        tm_s_part = tm_us / (1000 * 1000);
        tm_us_part = tm_us % (1000 * 1000);
    }
    DEBUGF("tm_s_part.tm_us_part = %d.%06d", tm_s_part, tm_us_part);
}

TEST(HbiTime, time_micros_parts_x1000000) {
    EnvironmentSaver saver("HBI_TM_OFFSET");
    setenv("HBI_TM_OFFSET", "100009.25", 1);
    hbitime_reset();

    int tm_s_part, tm_us_part;
    for (long i = 0; i < 1000000; ++i) {
        hbitime_micros_parts(&tm_s_part, &tm_us_part);
    }
    DEBUGF("tm_s_part.tm_us_part = %d.%06d", tm_s_part, tm_us_part);
}

}

