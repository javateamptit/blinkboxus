# Copyright (C) and/or used under license by RGM Advisors, LLC. All Rights Reserved Worldwide.

BIN_TGZ = release/blink-bin.tgz
LIB_TGZ = release/blink-lib.tgz
TWT_ZIP = release/blink-twt.zip
DB_TGZ = release/blink-db.tgz
SHIP_DIR = ship

HELPER_SUBDIRS =
HELPER_SUBDIRS += gtest
HELPER_SUBDIRS += common

SUBDIRS =
SUBDIRS += as
SUBDIRS += gap_proxy
SUBDIRS += pm
SUBDIRS += ta
SUBDIRS += ts
SUBDIRS += ouch42sim
SUBDIRS += oec_sim

TWT_DEPS := $(shell find twt | sed 's/ /\\ /g')
DB_DEPS := $(shell find database_scripts | sed 's/ /\\ /g')

.PHONY: all
all: $(BIN_TGZ) $(LIB_TGZ) $(TWT_ZIP) $(DB_TGZ)

.PHONY: clean
clean:
	rm -rf $(SHIP_DIR) $(dir $(BIN_TGZ))
	for s in $(HELPER_SUBDIRS) $(SUBDIRS); do $(MAKE) -C $$s clean; done

$(BIN_TGZ): $(SHIP_DIR)/bin
	mkdir -p $(dir $@)
	tar czf $@ -C $(SHIP_DIR) bin

$(LIB_TGZ): $(SHIP_DIR)/lib
	mkdir -p $(dir $@)
	tar czf $@ -C $(SHIP_DIR) lib

$(TWT_ZIP): $(SHIP_DIR)/twt.done
	mkdir -p $(dir $@)
	rm -f $@
	zfn=$$(readlink -f $@); cd $(SHIP_DIR) && zip -r $$zfn twt

$(DB_TGZ): $(SHIP_DIR)/db.done
	mkdir -p $(dir $@)
	rm -f $@
	tar czf $@ -C $(SHIP_DIR) database_scripts

.PHONY: $(SHIP_DIR)/bin
$(SHIP_DIR)/bin: $(SUBDIRS)
	rm -rf $@
	mkdir -p $@
	for s in $(SUBDIRS); do cp $$s/bin/* $@; done

.PHONY: $(SHIP_DIR)/lib
$(SHIP_DIR)/lib:
	rm -rf $@
	mkdir -p $@
	cp ts/lib/* $@

$(SHIP_DIR)/twt.done: $(TWT_DEPS)
	rm -rf $(SHIP_DIR)/twt
	mkdir -p $(SHIP_DIR)/twt
	cp -a twt/* $(SHIP_DIR)/twt
	touch $@

$(SHIP_DIR)/db.done: $(DB_DEPS)
	rm -rf $(SHIP_DIR)/database_scripts
	mkdir -p $(SHIP_DIR)/database_scripts
	cp -a database_scripts/* $(SHIP_DIR)/database_scripts
	touch $@

.PHONY: $(HELPER_SUBDIRS) $(SUBDIRS)
$(HELPER_SUBDIRS) $(SUBDIRS):
	$(MAKE) -C $@

$(SUBDIRS): common

common: gtest

