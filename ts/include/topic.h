
#ifndef __TOPIC_H__
#define __TOPIC_H__

/* Topic is 18 bits. We organize it like this:

  symbol_id (13 bits)
        |
        |     venue_id (4 bits)
        |           |
        |           |
  |-------------|-|----|
                 |
                 |
        OE/MD flag (1 bit)

  The idea is that when we lookup GOOG/arca (or whatever) we also pull
  GOOG/isld, GOOG/nyse, etc into cache.
*/

// Order Entry Topics

static inline uint32_t gen_oe_topic_id(uint32_t venue_id, uint32_t symbol_id) {
  assert(venue_id <= 0x0F);
  assert(symbol_id <= 0x1FFF);
  return ((uint32_t) symbol_id) << 5 | 0x10 | venue_id;
}

static inline uint32_t symbol_from_oe_topic_id(uint32_t topic_id) {
  return topic_id >> 5;
}

static inline uint32_t venue_from_oe_topic_id(uint32_t topic_id) {
  return topic_id & 0xF;
}

// Market Data Topics

static inline uint32_t gen_md_topic_id(uint32_t venue_id, uint32_t symbol_id) {
  assert(venue_id <= 0x0F);
  assert(symbol_id <= 0x1FFF);
  return ((uint32_t) symbol_id) << 5 | venue_id;
}

static inline uint32_t symbol_from_md_topic_id(uint32_t topic_id) {
  return topic_id >> 5;
}

static inline uint32_t venue_from_md_topic_id(uint32_t topic_id) {
  return topic_id & 0xF;
}

// Admin topics

static inline uint32_t gen_admin_topic_id(uint32_t venue_id) {
  assert(venue_id <= 0x0F);
  return venue_id;
}

static inline uint8_t venue_from_admin_topic_id(uint32_t topic_id) {
  return topic_id & 0x0F;
}



#endif // __TOPIC_H_

