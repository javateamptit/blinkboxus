/****************************************************************************
** Project name:  Equity Arbitrage Application
** File name:     arca_book_proc.h
** Description:   This file contains some declarations for arca_book_proc.c

** Author:    Danh Thai - Hoang
** First created on 29 September 2007
** Last updated on 01 October 2007
****************************************************************************/
#ifndef _ARCA_BOOK_PROC_H_
#define _ARCA_BOOK_PROC_H_

#include "configuration.h"

//Arca Integrated XDP Control Msgs
#define ARCA_VENDOR_MAPPING_MSG          4
#define ARCA_SYMBOL_CLEAR_MSG           32
#define ARCA_TRADING_SESSION_CHANGE_MSG 33
#define ARCA_SECURITY_STATUS_MSG        34

//This msg may exists in any type of packet???
#define ARCA_SOURCE_TIME_REF_MSG        2
#define ARCA_SYMBOL_INDEX_MAPPING_MSG   3

//Arca Integrated XDP Book Msgs
#define ARCA_ADD_MSG_BODY             100
#define ARCA_MOD_MSG_BODY             101
#define ARCA_DEL_MSG_BODY             102
#define ARCA_EXEC_MSG_BODY            103
#define ARCA_IMB_MSG_BODY             105

//ARCA Integrated XDP Trade Msgs (ignored all)
#define ARCA_TRADE_MSG                220
#define ARCA_TRADE_CANCELED_MSG       221
#define ARCA_TRADE_CORRECTION_MSG     222

//ARCA Integrated XDP other msg not related to Book data (ignored all)
#define ARCA_STOCK_SUMMARY_MSG        223
#define ARCA_PBBO_MSG                 104
#define ARCA_ADD_REFRESH_MSG          106

/* ARCA XDP Integrated Message Structure Definition*/
typedef struct gsxBook_Header
{
  unsigned short msgSize;
  unsigned short msgType;
}gsxBook_Header;

typedef struct gsxBook_Add_Message_Body
{
  unsigned short msgSize;
  unsigned short msgType;
  
  unsigned int sourceTimeNS;
  unsigned int symbolIndex;
  unsigned int symbolSeqNum;
  unsigned int orderID;
  unsigned int priceNumerator;
  unsigned int volume;
  unsigned char side;
  unsigned char GTCIndicator;
  unsigned char tradeSession;
}gsxBook_Add_Message_Body;

typedef struct gsxBook_Modify_Message_Body
{
  unsigned short msgSize;
  unsigned short msgType;
  
  unsigned int sourceTimeNS;
  unsigned int symbolIndex;
  unsigned int symbolSeqNum;
  unsigned int orderID;
  unsigned int priceNumerator;
  unsigned int volume;
  unsigned char side;
  unsigned char GTCIndicator;
  unsigned char reasonCode;
}gsxBook_Modify_Message_Body;

typedef struct gsxBook_Delete_Message_Body
{
  unsigned short msgSize;
  unsigned short msgType;
  
  unsigned int sourceTimeNS;
  unsigned int symbolIndex;
  unsigned int symbolSeqNum;
  unsigned int orderID;
  unsigned char side;
  unsigned char GTCIndicator;
  unsigned char reasonCode;
  unsigned char _padding;
}gsxBook_Delete_Message_Body;

typedef struct gsxBook_Execute_Message_Body
{
  unsigned short msgSize;
  unsigned short msgType;
  
  unsigned int sourceTimeNS;
  unsigned int symbolIndex;
  unsigned int symbolSeqNum;
  unsigned int orderID;
  unsigned int priceNumerator;
  unsigned int volume;
  unsigned char GTCIndicator;
  unsigned char reasonCode;
  //unsigned char tradeId;
}gsxBook_Execute_Message_Body;

typedef struct gsxBook_Symbol_Clear_Message_Body
{
  unsigned short msgSize;
  unsigned short msgType;  
  
  unsigned int sourceTime;
  unsigned int sourceTimeNS;
  unsigned int symbolIndex; 
  unsigned int symbolSeqNum;
}gsxBook_Symbol_Clear_Message_Body;

typedef struct gsxBook_Trading_Session_Change_Message_Body
{
  unsigned short msgSize;
  unsigned short msgType; 
                    
  unsigned int sourceTime;
  unsigned int sourceTimeNS;
  unsigned int symbolIndex; 
  unsigned int symbolSeqNum;
  unsigned char tradeSession;
  unsigned char _padding[3];
}gsxBook_Trading_Session_Change_Message_Body;

typedef struct gsxBook_Symbol_Mapping_Message_Body
{
  unsigned short msgSize;
  unsigned short msgType;
  unsigned int symbolIndex;
  unsigned char symbol[12]; //11 bytes and 1 byte filler
  unsigned short marketId;
  unsigned char systemId;
  unsigned char exchangeCode;
  unsigned char priceScaleCode;
  unsigned char securityType;
  unsigned short uot;
}gsxBook_Symbol_Mapping_Message_Body;

typedef struct gsxBook_Security_Status_Message_Body
{
  unsigned short msgSize;
  unsigned short msgType; 
  
  unsigned int sourceTimeNS;
  unsigned int symbolIndex; 
  unsigned int symbolSeqNum; 
  unsigned char securityStatus;   
  unsigned char haltCondition;
  
  unsigned char _padding[3];
}gsxBook_Security_Status_Message_Body;

typedef struct gsxBook_Trade_Message_Body
{
  unsigned short msgSize;
  unsigned short msgType; 
  
  unsigned int symbolIndex;
}gsxBook_Trade_Message_Body;

union book_message_union_binary
{
  struct gsxBook_Header mHeader;
  struct gsxBook_Add_Message_Body mAdd;
  struct gsxBook_Modify_Message_Body mModify;
  struct gsxBook_Delete_Message_Body mDelete;
  struct gsxBook_Execute_Message_Body mExecute;
  struct gsxBook_Symbol_Clear_Message_Body mSymbolClear;
  struct gsxBook_Symbol_Mapping_Message_Body mSymbolMapping;
  struct gsxBook_Trading_Session_Change_Message_Body mTradingSessionChange;
  struct gsxBook_Security_Status_Message_Body mSecurityStatus;
  struct gsxBook_Trade_Message_Body mTrade;
}book_message_union_binary;

/* End of ARCA XDP Integrated Message Structure Definition*/

extern t_ARCA_Book_Conf ARCA_Book_Conf;
/*****************************************************************************************************/
int CheckArcaStaleData(int stockSymbolIndex, unsigned long arcaNanos, unsigned long arrivalTime);
int InitARCA_BOOK_DataStructure(void);
int32_t ParseBookMsg(union book_message_union_binary *dstMsg, unsigned char *tempBuffer);
int ProcessDecodedAddMessage(struct gsxBook_Add_Message_Body *msg, unsigned long arrivalTime, unsigned int timeStatusSeconds, uint32_t symbolId);
int ProcessDecodedDeleteMessage(struct gsxBook_Delete_Message_Body *msg, unsigned long arrivalTime, unsigned int timeStatusSeconds, uint32_t symbolId);
int ProcessDecodedModifyMessage(struct gsxBook_Modify_Message_Body *msg, unsigned long arrivalTime, unsigned int timeStatusSeconds, uint32_t symbolId);
int ProcessDecodedExecuteMessage(struct gsxBook_Execute_Message_Body *msg, unsigned long arrivalTime, unsigned int timeStatusSeconds, uint32_t symbolId);
int ProcessDecodedSymbolClearMessage(struct gsxBook_Symbol_Clear_Message_Body *msg, uint32_t symbolId);
int ProcessTradingSessionChangeMessage(struct gsxBook_Trading_Session_Change_Message_Body *msg, uint32_t symbolId);
void ProcessSecurityStatusMessage(struct gsxBook_Security_Status_Message_Body *msg, unsigned long arrivalTime, uint32_t symbolId);
void ProcessTradeMessage(struct gsxBook_Trade_Message_Body *msg, unsigned long arrivalTime, uint32_t symbolId);
void *ThreadReceiveData(void *args);
int LoadAllSymbolMappings(char *destFile);

int ARCA_ProcessBookMessage(unsigned char *buffer, unsigned long arrivalTime, unsigned int timeStatusSeconds, uint32_t symbolId);
#endif
