#ifndef _NYSE_BOOK_PROC_H_
#define _NYSE_BOOK_PROC_H_

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include "configuration.h"
#include "book_mgmt.h"

//Product ID used for sending message to TCP (heartbeat response, retrans request, symbol mapping request)
#define NYSE_BOOK_PRODUCT_ID                        115

//NYSE Book Message ID
#define NYSE_BOOK_RESTRANSMISSION_RESPONSE_MSG      10    //TCP
#define NYSE_BOOK_RETRANSMISION_REQUEST_MSG         20    //TCP
#define NYSE_BOOK_HEARTBEAT_RESPONSE_MSG            24    //TCP
#define NYSE_BOOK_HEARTBEAT_MSG                      2    //TCP, UDP
#define NYSE_BOOK_SEQ_NUMBER_RESET_MSG               1    //UDP
#define NYSE_BOOK_MESSAGE_UNAVAILABLE_MSG            5    //UDP
#define NYSE_BOOK_SYMBOL_INDEX_MAPPING_MSG          35    //UDP
#define NYSE_BOOK_SYMBOL_INDEX_MAPPING_REQUEST_MSG  34    //UDP
//#define NYSE_BOOK_BOOK_REFRESH_MSG                22    //UDP
#define NYSE_BOOK_FULL_UPDATE_MSG                  230    //UDP
#define NYSE_BOOK_DELTA_UPDATE_MSG                 231    //UDP

//NYSE Book message length
//Note: msgLen = msgSize + 2
#define NYSE_BOOK_HEARTBEAT_RESPONSE_MSG_LEN            36
#define NYSE_BOOK_RETRANSMISSION_REQUEST_MSG_LEN        44
#define NYSE_BOOK_SYMBOL_INDEX_MAPPING_REQUEST_MSG_LEN  38
//NYSE Book Retransmission Flag
#define NYSE_BOOK_ORIGINAL_MSG_FLAG                      1
#define NYSE_BOOK_END_OF_REFRESH_RETRANS_FLAG            6
#define NYSE_BOOK_MSG_WAS_ACCEPTED                      '0'

//message length
#define NYSE_BOOK_MSG_HEADER_LEN                  16
#define NYSE_BOOK_DELTA_MSG_LEN_AT_LEAST          34
#define NYSE_BOOK_DELTA_MSG_HEADER_LEN            18
#define NYSE_BOOK_DELTA_MSG_PRICE_POINT_LEN       28
#define NYSE_BOOK_FULL_MSG_LEN_AT_LEAST           48
#define NYSE_BOOK_FULL_MSG_HEADER_LEN             32
#define NYSE_BOOK_FULL_MSG_PRICE_POINT_LEN        12

/****************************************************************************
** Data structure definitions
****************************************************************************/

/****************************************************************************
** Global variable declarations
****************************************************************************/
extern t_NYSE_Book_Conf NyseBookConf;
extern t_NYSE_Book_Conf AmexBookConf;

/****************************************************************************
** Function declarations
****************************************************************************/
int NYSE_InitDataStructure(int ecnIndex);
int NYSE_LoadAllSymbolMappings(char *destFile);

int NYSE_DeleteQuoteFromStockBook(const unsigned char ecnIndex, double price, int side, int stockSymbolIndex);
int NYSE_UpdateQuotesToStockBook(const unsigned char ecnIndex, t_OrderDetail *order, int stockSymbolIndex, int side);
int NYSE_ProcessBookMessage(const unsigned char ecnIndex, uint32_t symbolId, char *buffer, unsigned long arrivalTime, unsigned short msgType);
int NYSE_ParseAndProcessDeltaMessage_From_DBLFilter(const unsigned char ecnIndex, char* buffer, unsigned long arrivalTime, uint32_t symbolId);
int NYSE_ParseAndProcessFullMessage_From_DBLFilter(const unsigned char ecnIndex, char* buffer, unsigned long arrivalTime, uint32_t symbolId);
int NYSE_CheckStaleData(const unsigned char ecnIndex, char *buffer, int stockSymbolIndex, unsigned long arrivalTime);
int GetNyseMessageSize(const unsigned char ecnIndex, const unsigned char *buffer, unsigned short msgType);
#endif
