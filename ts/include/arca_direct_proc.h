/****************************************************************************
** Project name:  Equity Arbitrage Application
** File name:     arca_direct_proc.h
** Description:   This file contains some declarations for arca_direct_proc.c

** Author:    Sang Nguyen-Minh
** First created on 20 September 2007
** Last updated on 20 September 2007
****************************************************************************/

#ifndef __ARCA_DIRECT_PROC_H__
#define __ARCA_DIRECT_PROC_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include "global_definition.h"
#include "configuration.h"
#include "stock_symbol.h"

#define ARCA_DIRECT_MSG_HEADER_LEN 4

// Receive timeout for ARCA DIRECT is in second(s)
#define ARCA_DIRECT_RECV_TIMEOUT 65

/*
Some message length and specific field index
that will be used usually
*/
#define NEW_ORDER_MSG_LEN     76
#define CANCEL_ORDER_MSG_LEN  72

#define MSG_TYPE_INDEX 0
#define OUTGOING_SEQ_NUM_INDEX 8

// Message types are used by Client
#define NEW_ORDER_TYPE        'D'
#define CANCEL_ORDER_TYPE     'F'

// Message types are used by Server
#define LOGON_REJECTED_TYPE   'L'
#define ORDER_ACK_TYPE        'a'
#define ORDER_FILLED_TYPE     '2'
#define CANCEL_ACK_TYPE       '6'
#define ORDER_REJECTED_TYPE   '8'
#define ORDER_KILLED_TYPE     '4'
#define BUST_OR_CORRECT_TYPE  'C'

// Message types are used by both Client and Server
#define LOGON_TYPE            'A'
#define TEST_REQUEST_TYPE     '1'
#define HEARTBEAT_TYPE        '0'

// Message length are used by client after TS connected to ARCA DIRECT
#define HEARTBEAT_MSG_LEN       12
#define TEST_REQUEST_MSG_LEN    12
#define ORDER_ACK_MSG_LEN       48
#define ORDER_FILLED_MSG_LEN    88
#define CANCEL_ACK_MSG_LEN      40
#define ORDER_KILLED_MSG_LEN    40
#define ORDER_REJECTED_MSG_LEN  80
#define BUST_OR_CORRECT_MSG_LEN 48
#define LOGON_REJECTED_MSG_LEN  60
#define LOGON_TYPE_MSG_LEN      48

/*
Constant values are used to 
build messages to sent to Server
*/
#define ORDER_TYPE_MARKET         '1'
#define ORDER_TYPE_LIMIT          '2'

#define PRICE_SCALE               '2'

#define TRADING_SESSION_ID        "123\0"
#define RULE80A                   'P' //Change A to P, related to ticket 1112  

#define VERSION_PROFILE           "L\001D\001G\001E\001a\0012\0016\0014\001\0"
#define EXEXCHANGE_DESTINATION    102

/****************************************************************************
** Function declarations
****************************************************************************/
int Init_ARCA_DIRECT_DataStructure();
int Connect_ARCA_DIRECT();
void Add_ARCA_DIRECT_DataBlockToCollection(t_DataBlock *dataBlock);
int ProcessARCA_DIRECT_DataBlock(uint32_t symbolId, t_DataBlock *dataBlock);

// Session messages
int BuildAndSend_ARCA_DIRECT_LogonRequestMsg(void);
int BuildAndSend_ARCA_DIRECT_HeartbeatMsg(void);
int BuildAndSend_ARCA_DIRECT_TestRequestMsg(void);

// Application messages
int BuildAndSend_ARCA_DIRECT_OrderMsg(t_OrderPlacementInfo *newOrder);

int ProcessARCA_DIRECT_OrderAckMsg(uint32_t symbolId, t_DataBlock *dataBlock);
int ProcessARCA_DIRECT_OrderFilledMsg(uint32_t symbolId, t_DataBlock *dataBlock);
int ProcessARCA_DIRECT_OrderCancelAckMsg(uint32_t symbolId, t_DataBlock *dataBlock);
int ProcessARCA_DIRECT_OrderRejectedMsg(uint32_t symbolId, t_DataBlock *dataBlock);
int ProcessARCA_DIRECT_OrderKilledMsg(uint32_t symbolId, t_DataBlock *dataBlock);

int ProcessARCA_DIRECT_BustOrCorrectMsg(uint32_t symbolId, t_DataBlock *dataBlock);
int ProcessARCA_DIRECT_TestRequestMsg(t_DataBlock *dataBlock);
int ProcessARCA_DIRECT_HeartbeatMsg(t_DataBlock *dataBlock);

int ProcessARCA_DIRECT_LogonAcceptedMsg(t_DataBlock *dataBlock);
int ProcessARCA_DIRECT_LogonRejectedMsg();

long Send_ARCA_DIRECT_SequencedMsg(char *message, int msgLen);
int Process_ARCA_DIRECT_Msg(t_DataBlock *dataBlock);
int Receive_ARCA_DIRECT_Msg(int numBytesToReceive);
int GetLatestSequenceNumbers_ARCA_DIRECT(int *incomingSeqNum);
#endif
