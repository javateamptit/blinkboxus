/****************************************************************************
** Project name:  Equity Arbitrage Application
** File name:     nasdaq_book_proc.h
** Description:   This file contains some declarations for nasdaq_book_proc.c

** Author:    Danh Thai - Hoang
** First created on 29 September 2007
** Last updated on 01 October 2007
****************************************************************************/

#ifndef _NASDAQ_BOOK_PROC_H_
#define _NASDAQ_BOOK_PROC_H_

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include "configuration.h"
#include "book_mgmt.h"
#include <semaphore.h>

/****************************************************************************
** Data structure definitions
****************************************************************************/

/****************************************************************************
** Global variable declarations
****************************************************************************/
extern t_NASDAQ_Book_Conf NASDAQ_Book_Conf;
extern t_NASDAQ_Book_Conf NDBX_Book_Conf;
extern t_NASDAQ_Book_Conf PSX_Book_Conf;
/****************************************************************************
** Function declarations
****************************************************************************/
int ProcessAddOrderMessage(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime, unsigned int timeStatusSeconds);

int ProcessExecutedOrderMessage(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime, unsigned int timeStatusSeconds);
int ProcessExecutedOrderWithPriceMessage(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime, unsigned int timeStatusSeconds);

int ProcessCancelOrderMessage(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime, unsigned int timeStatusSeconds);

int ProcessDeleteOrderMessage(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime, unsigned int timeStatusSeconds);

int ProcessReplaceOrderMessage(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime, unsigned int timeStatusSeconds);

void ProcessStockTradingActionMessage(uint32_t symbolId, char *buffer, unsigned long arrivalTime);
void ProcessCrossTradeMessage(unsigned char ecn_book_id, uint32_t symbolId, char *buffer);

int CheckNasdaqStaleData(unsigned char ecn_book_id, char *buffer, int stockSymbolIndex, unsigned long arrivalTime, unsigned int timeStatusSeconds);

void InitNASDAQ_BOOK_DataStructure(void);
void InitDataStructure_NDBX(void);
void InitDataStructure_PSX(void);

int NASDAQ_ProcessBookMessage(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime, unsigned int timeStatusSeconds);
#endif
