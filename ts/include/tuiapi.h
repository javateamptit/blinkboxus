#ifndef __TUIAPI_H__
#define __TUIAPI_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#define bool    unsigned char
#define byte    unsigned char

#define false     0
#define true    1

#define mFailed   printf
#define mSizeOf(array,structType)     (sizeof(array)/sizeof(structType))

#define cMaxParam   10
#define cCmdNameLen   30

/****************************************************************************
** Data structure definitions
****************************************************************************/
typedef struct tCmdTable
{
  char  commandName[cCmdNameLen];
  char  *description;
  char  *usage;
  byte  numParam;
  int   (*CmdHandler)(char, char **);
} tCmdTable; 

/****************************************************************************
** Global variable declarations
****************************************************************************/
extern unsigned char  cmdTabLen;
extern tCmdTable  cmdTab[];

/****************************************************************************
** Function declarations
****************************************************************************/
void TuiMain(int *isStop, char *strDefaultPrompt);
int Help(char argc, char **argv);

#endif

