/****************************************************************************
** Project name:  Equity Arbitrage Application
** File name:     trade_server.h
** Description:   This file contains some declarations for trade_server.c

** Author:    Sang Nguyen-Minh
** First created on 13 September 2007
** Last updated on 13 September 2007
****************************************************************************/

#ifndef __TRADE_SERVER_H__
#define __TRADE_SERVER_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include "global_definition.h"
#include "configuration.h"
#include "aggregation_server_proc.h"

#include "order_mgmt.h"

#include "arca_direct_proc.h"
#include "nyse_ccg_proc.h"
#include "bats_boe_proc.h"
#include "ouch_oec_proc.h"

/****************************************************************************
** Data structure definitions
****************************************************************************/

/****************************************************************************
** Global variable definition
****************************************************************************/

/****************************************************************************
** Function declarations
****************************************************************************/
int main(int argc, char *argv[]);

int ProcessUserInput(int argc, char *argv[]);

int LoadConfiguration(void);

int InitDataStructures(void);
int InitializeMessageConsumers(void);

void InitMonitorArcaBook(void);
void InitMonitorNasdaqBook(void);
void InitMonitorNdbxBook(void);
void InitMonitorBatszBook(void);
void InitMonitorBYXBook(void);
void InitMonitorNyseBook(void);
void InitMonitorAmexBook(void);
void InitMonitorEdgxBook(void);
void InitMonitorEdgaBook(void);
void InitMonitorPsxBook(void);
void InitMonitorCQS(void);
void InitMonitorUQDF(void);
     
int RegisterSignalCatching(void);
void Process_INTERRUPT_Signal(int signalno, siginfo_t *siginfo, void *context);
void Process_BROKEN_PIPE_Signal(int sig_num);
int SaveVersionInfoToFile(void);
int StartUpMainThreads(void);      
void *Thread_DBLFilter_Data_Consumer(void *args);
void *Thread_StartProgramWithOptions(void *args);
void *Thread_VenueActivityMonitoring(void* args);
int StartThread(char threadType);
int getHourMin(char *opt_arg, int *hour, int *min);
void PrintHelp();
int RegisterSignalHandlers(void);
void Process_SEGV_Signal(int signalno, siginfo_t *siginfo, void *context);
#endif
