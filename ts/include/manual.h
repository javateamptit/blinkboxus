#ifndef __MANUAL_H__
#define __MANUAL_H__
/****************************************************************************
** Include files and define several constants
****************************************************************************/

/****************************************************************************
** Data structure definitions
****************************************************************************/

/****************************************************************************
** Global variable declarations
****************************************************************************/

/****************************************************************************
** Function declarations
****************************************************************************/
int ConnectOec(char argc, char *argv[]);
int ConnectMdc(char argc, char *argv[]);
int DisconnectOec(char argc, char *argv[]);
int DisconnectMdc(char argc, char *argv[]);

int ShowSymbolsHaveQuotes(char argc, char *argv[]);
int ViewStockStatus(char argc, char *argv[]);
int ViewSymbolHaltStatus(char argc, char *argv[]);

int __Arca_NewOrder(char argc, char *argv[]);
int __Ouch_NewOrder(char argc, char *argv[]);
int __Rash_NewOrder(char argc, char *argv[]);
int __Bats_NewOrder(char argc, char *argv[]);
int __BYX_NewOrder(char argc, char *argv[]);
int __Edgx_NewOrder(char argc, char *argv[]);
int __Edgx_NewExtOrder(char argc, char *argv[]);
int __Edga_NewOrder(char argc, char *argv[]);  
int __Edga_NewExtOrder(char argc, char *argv[]);
int __Edga_CancelOrder(char argc, char *argv[]);
int __Nyse_NewOrder(char argc, char *argv[]);
int __Bx_NewOrder(char argc, char *argv[]);
int __Psx_NewOrder(char argc, char *argv[]);
#endif
