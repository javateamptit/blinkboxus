#ifndef __OUCH_OEC_PROC_H__
#define __OUCH_OEC_PROC_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include "global_definition.h"
#include "configuration.h"
#include "stock_symbol.h"

// Soupbin TCP
#define SOUPBIN_TCP_SEQUENCED_MSG         'S'
#define SOUPBIN_TCP_UNSEQUENCED_MSG       'U'
#define SOUPBIN_TCP_LOGIN_REQUEST         'L'
#define SOUPBIN_TCP_LOGOUT_REQUEST        'O'
#define SOUPBIN_TCP_LOGIN_ACCEPTED        'A'
#define SOUPBIN_TCP_LOGIN_REJECTED        'J'
#define SOUPBIN_TCP_SERVER_HEARTBEAT      'H'
#define SOUPBIN_TCP_CLIENT_HEARTBEAT      'R'
#define SOUPBIN_TCP_END_OF_SESSION        'Z'
#define SOUPBIN_TCP_DEBUG_MSG             '+'

#define SOUPBIN_TCP_HEARTBEAT_LEN          3
#define SOUPBIN_TCP_LOGIN_ACCEPTED_LEN    33
#define SOUPBIN_TCP_LOGIN_REJECTED_LEN     4
#define SOUPBIN_END_OF_SESSION_LEN         3
#define SOUPBIN_LOGIN_REQUEST_LEN         49

// UFO (UDP For Orders)
#define UFO_SEQUENCED_MSG                 'S'
#define UFO_UNSEQUENCED_MSG               'U'
#define UFO_LOGIN_REQUEST                 'L'
#define UFO_LOGOUT_REQUEST                'O'
#define UFO_LOGIN_ACCEPTED                'A'
#define UFO_LOGIN_REJECTED                'J'
#define UFO_HEARTBEAT                     'R'
#define UFO_RETRANSMISSION_REQUEST        'T'
#define UFO_END_OF_SESSION                'E'

#define UFO_LOGIN_ACCEPTED_LEN            15
#define UFO_LOGIN_REJECTED_LEN             2
#define UFO_END_OF_SESSION_LEN             5
#define UFO_LOGIN_REQUEST_LEN             29

// Common for both SoupbinTCP and UFO
#define ____________SEQUENCED_MSG         'S'
#define ____________UNSEQUENCED_MSG       'U'
#define ____________LOGIN_REQUEST         'L'
#define ____________LOGOUT_REQUEST        'O'
#define ____________LOGIN_ACCEPTED        'A'
#define ____________LOGIN_REJECTED        'J'

// OUCH msgs
#define OUCH_ORDER_ACCEPTED               'A'
#define OUCH_ORDER_EXECUTED               'E'
#define OUCH_ORDER_CANCELED               'C'
#define OUCH_ORDER_REJECTED               'J'
#define OUCH_BROKEN_TRADE                 'B'
#define OUCH_PRICE_CORRECTION             'K'
#define OUCH_CANCEL_PENDING               'P'
#define OUCH_CANCEL_REJECT                'I'
#define OUCH_ORDER_REPLACE                'U'
#define OUCH_SYSTEM_EVENT                 'S'

#define OUCH_EXECUTED_LEN                 40
#define OUCH_CANCELED_LEN                 28
#define OUCH_REJECTED_LEN                 24
#define OUCH_BROKEN_TRADE_LEN             32
#define OUCH_PRICE_CORRECTION_LEN         36
#define OUCH_CANCEL_PENDING_LEN           23
#define OUCH_CANCEL_REJECT_LEN            23
#define OUCH_SYSTEM_EVENT_LEN             10

#define OUCH_NEW_ORDER_MSG_LEN            51
#define OUCH_ORDER_ACCEPTED_LEN           66

/****************************************************************************
** Data structure definitions
****************************************************************************/

/****************************************************************************
** Global variable declarations
****************************************************************************/

/****************************************************************************
** Function declarations
****************************************************************************/
int Connect_Ouch(int oecType);

// Session messages
int BuildAndSend_SoupbinTcp_LoginMessage(int oecType);
int BuildAndSend_UFO_LoginMessage(int oecType);
int BuildAndSend_Ouch_LogoffMessage(int oecType);
int BuildAndSend_SoupbinTcp_Heartbeat(t_OecConfig *venueConfig);


// Application messages
int BuildAndSend_Ouch_OrderMsg(int oecType, t_OrderPlacementInfo *newOrder);

int ProcessOuchMessages(int oecType, uint32_t symbolId, const unsigned char *buf, const uint64_t arrivalTime);
int ProcessOuchUfo_SequencedMessages(int oecType, uint32_t symbolId, const unsigned char *buf, const uint64_t arrivalTime);

int ProcessOuch_OrderAcceptedMsg(int oecType, uint32_t symbolId, const unsigned char *msg, const uint64_t arrivalTime);
int ProcessOuch_OrderExecutedMsg(int oecType, uint32_t symbolId, const unsigned char *msg, const uint64_t arrivalTime);
int ProcessOuch_OrderCanceledMsg(int oecType, uint32_t symbolId, const unsigned char *msg, const uint64_t arrivalTime);
int ProcessOuch_OrderRejectedMsg(int oecType, uint32_t symbolId, const unsigned char *msg, const uint64_t arrivalTime);

int ProcessOuchSoupbinMessages(int oecType, uint32_t symbolId, const unsigned char *buf, const uint64_t arrivalTime);
int ProcessOuchUfoMessages(int oecType, uint32_t symbolId, const unsigned char *buf, const uint64_t arrivalTime);
int ProcessOuchSoupbin_SequencedMessages(int oecType, uint32_t symbolId, const unsigned char *buf, const uint64_t arrivalTime);

void Add_Ouch_DataBlockToCollection(int oecType, t_DataBlock *dataBlock);

#endif
