/****************************************************************************
** Project name:  Equity Arbitrage Application
** Filename:    batsz_book_proc.c
** Description:   BATS-Z Multicast PITCH
** Author:      Luan Vo Kinh
** First created on 10 July 2009
** Last updated on 10 July 2009
****************************************************************************/

#ifndef _BATSZ_BOOK_PROC_H_
#define _BATSZ_BOOK_PROC_H_

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include "configuration.h"
#include "book_mgmt.h"

// Message type
/*
  0x01  Login
  0x02  Login Response
  0x03  Gap Request
  0x04  Gap Response

  0x20  Time
  0x21  Add Order - Long
  0x22  Add Order - Short
  0x2F  Add Order - Expanded
  0x23  Order Executed
  0x24  Order Executed at Price/Size
  0x25  Reduce Size - Long
  0x26  Reduce Size - Short
  0x27  Modify Order - Long
  0x28  Modify Order - Short
  0x29  Delete Order
  0x2A  Trade - Long
  0x2B  Trade - Short
  0x30  Trade - Expanded
  0x2C  Trade Break
  0x2D  End of Session
  0x2E  Symbol Mapping (Options Only)
  0x31  Trading Status (Equities Only)

  0x95  Auction Update (BATS-Z Exchange Only)
  0x96  Auction Summary (BATS-Z Exchange Only)
  0x97  Unit Clear
  0x98  Retail Price Improvement Message (BATS-Y Exchange Only)
*/

#define BATS_MSG_TYPE_LOGIN                          0x01
#define BATS_MSG_TYPE_LOGIN_RESPONSE                 0x02
#define BATS_MSG_TYPE_GAP_REQUEST                    0x03
#define BATS_MSG_TYPE_GAP_RESPONSE                   0x04

#define BATS_MSG_TYPE_TIME                           0x20
#define BATS_MSG_TYPE_ADD_ORDER_LONG                 0x21
#define BATS_MSG_TYPE_ADD_ORDER_SHORT                0x22
#define BATS_MSG_TYPE_ADD_ORDER_EXPANDED             0x2F
#define BATS_MSG_TYPE_ORDER_EXECUTED                 0x23
#define BATS_MSG_TYPE_ORDER_EXECUTED_AT_PRICE        0x24
#define BATS_MSG_TYPE_REDUCE_SIZE_LONG               0x25
#define BATS_MSG_TYPE_REDUCE_SIZE_SHORT              0x26
#define BATS_MSG_TYPE_MODIFY_ORDER_LONG              0x27
#define BATS_MSG_TYPE_MODIFY_ORDER_SHORT             0x28
#define BATS_MSG_TYPE_DELETE_ORDER                   0x29
#define BATS_MSG_TYPE_TRADE_LONG                     0x2A
#define BATS_MSG_TYPE_TRADE_SHORT                    0x2B
#define BATS_MSG_TYPE_TRADE_EXPANDED                 0x30
#define BATS_MSG_TYPE_TRADE_BREAK                    0x2C
#define BATS_MSG_TYPE_END_OF_SESSION                 0x2D
#define BATS_MSG_TYPE_TRADING_STATUS                 0x31
#define BATS_MSG_TYPE_AUCTION_UPDATE                 0x95
#define BATS_MSG_TYPE_AUCTION_SUMMARY                0x96
#define BATS_MSG_TYPE_UNIT_CLEAR                     0x97
#define BYX_MSG_TYPE_RETAILS_PRICE_IMPROVEMENT       0x98

// Message length
#define BATSZ_MSG_LEN_LOGIN                       22
#define BATSZ_MSG_LEN_LOGIN_ACCEPTED               3
#define BATSZ_MSG_LEN_GAP_REQUEST                  9
#define BATSZ_MSG_LEN_GAP_RESPONSE                10

#define BATSZ_MSG_LEN_TIME                         6
#define BATSZ_MSG_LEN_ADD_ORDER_LONG              34
#define BATSZ_MSG_LEN_ADD_ORDER_SHORT             26
#define BATSZ_MSG_LEN_ADD_ORDER_EXPANDED          40
#define BATSZ_MSG_LEN_ORDER_EXECUTED              26
#define BATSZ_MSG_LEN_ORDER_EXECUTED_AT_PRICE     38
#define BATSZ_MSG_LEN_REDUCE_SIZE_LONG            18
#define BATSZ_MSG_LEN_REDUCE_SIZE_SHORT           16
#define BATSZ_MSG_LEN_MODIFY_ORDER_LONG           27
#define BATSZ_MSG_LEN_MODIFY_ORDER_SHORT          19
#define BATSZ_MSG_LEN_DELETE_ORDER                14

#define BATSZ_MSG_LEN_TRADE_LONG                  41
#define BATSZ_MSG_LEN_TRADE_SHORT                 33
#define BATSZ_MSG_LEN_TRADE_EXPANDED              43
#define BATSZ_MSG_LEN_TRADE_BREAK                 14
#define BATSZ_MSG_LEN_END_OF_SESSION               6

#define BATSZ_MSG_LEN_TRADING_STATUS              18
#define BATSZ_MSG_LEN_AUCTION_UPDATE              47
#define BATSZ_MSG_LEN_AUCTION_SUMMARY             27
#define BATSZ_MSG_LEN_UNIT_CLEAR                   6

// Sequenced Unit Header
#define BATSZ_UNIT_HEADER_LEN         8
#define BATSZ_HEADER_LENGTH_INDEX     0
#define BATSZ_HEADER_COUNT_INDEX      2
#define BATSZ_HEADER_UNIT_INDEX       3
#define BATSZ_HEADER_SEQUENCE_INDEX   4

/****************************************************************************
** Data structure definitions
****************************************************************************/

/****************************************************************************
** Global variable declarations
****************************************************************************/
extern t_BATS_Book_Conf BATSZ_Book_Conf;
extern t_BATS_Book_Conf BYX_Book_Conf;

/****************************************************************************
** Function declarations
****************************************************************************/
void InitBATSZ_BOOK_DataStructure(void);
void InitBYX_BOOK_DataStructure(void);

int BATS_ProcessBookMessage(unsigned char ecn_book_id, uint32_t symbolId, char *msg, unsigned long arrivalTime, unsigned int timeStatusSeconds);
int ProcessAddOrderMessageShort_BATS(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime, unsigned int timeStatusSeconds);
int ProcessAddOrderMessageLong_BATS(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime, unsigned int timeStatusSeconds);
int ProcessAddOrderMessageExpanded_BATS(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime, unsigned int timeStatusSeconds);
int ProcessOrderExecutedMessage_BATS(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime, unsigned int timeStatusSeconds);
int ProcessOrderExecutedAtPriceMessage_BATS(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime, unsigned int timeStatusSeconds);
int ProcessDeleteOrderMessage_BATS(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime, unsigned int timeStatusSeconds);
int ProcessModifyOrderMessageShort_BATS(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime, unsigned int timeStatusSeconds);
int ProcessModifyOrderMessageLong_BATS(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime, unsigned int timeStatusSeconds);
int ProcessReduceSizeMessageShort_BATS(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime, unsigned int timeStatusSeconds);
int ProcessReduceSizeMessageLong_BATS(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime, unsigned int timeStatusSeconds);
int ProcessUnitClearMessage_BATS(unsigned char ecn_book_id, uint32_t symbol_id);

int ProcessTradingStatusMessage_BATS(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime);

int CheckStaleData_BATS(unsigned char ecn_book_id, char *buffer, int stockSymbolIndex, unsigned long arrivalTime, unsigned int timeStatusSeconds);
#endif
