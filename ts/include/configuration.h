/****************************************************************************
** Project name:  Equity Arbitrage Application
** File name:     configuration.h
** Description:   This file contains some declarations for configuration.c

** Author:      Sang Nguyen-Minh
** First created on 13 September 2007
** Last updated on 13 September 2007
****************************************************************************/

#ifndef __CONFIGURATION_H__
#define __CONFIGURATION_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include "global_definition.h"
#include "aggregation_server_proc.h"
#include "utility.h"
#include "quote_mgmt.h"

#include "arca_direct_proc.h"
#include "nyse_ccg_proc.h"
#include "bats_boe_proc.h"

#include "nasdaq_rash_proc.h"
#include "edge_order_proc.h"
#include "dbl.h"

/****************************************************************************
** Data structure definitions
****************************************************************************/
// Network filter constants
#define DBL_FILTER_LOG_SIZE         (1048576)     // 1 MB ring buffer
#define DBLFILTER_NUM_QUEUES        11
#define DBLFILTER_MD_QUEUE_SIZE     (16*1048576)  // 16 MB ring buffer
#define DBLFILTER_OE_QUEUE_SIZE     (64*1024)     // 1 MB ring buffer
#define DATABLOCK_QUEUE_SIZE        (16*1048576)  // 16 MB ring buffer
#define TRADEOUTPUT_QUEUE_SIZE      (16*1048576)  // 16 MB ring buffer

// Symbol mapping file name for ARCA and NYSE Books
#define NYSE_BOOK_SYMBOL_INDEX_MAPPING_FILE "NYSESymbolMapping.xml"
#define ARCA_BOOK_SYMBOL_INDEX_MAPPING_FILE "ARCASymbolMapping.txt"

#define MAX_ARCA_STOCK_SYMBOL         80000
#define MAX_NYSE_BOOK_STOCK_SYMBOL    80000

// For MPID
#define MPID_NASDAQ_OUCH              "HBIF"
#define MPID_NASDAQ_RASH              "HBIF"
#define MPID_NASDAQ_BX                "HBIF"
#define MPID_NASDAQ_PSX               "HBIF"
#define MPID_NYSE_CCG                 "HBI"
#define MPID_BATSZ_BOE                "BZYU"
#define MPID_BYX_BOE                  "BZYU"

/*
Server roles
*/
#define SERVER_ROLE_ARCA              0
#define SERVER_ROLE_NASDAQ            1
#define SERVER_ROLE_BATS              2
#define SERVER_ROLE_EDGE              3
#define SERVER_ROLE_STAND_ALONE       4
#define MAX_SERVER_ROLE               5
/*
These are sections will be supported 
from algorithm configuration file
*/

#define SEC_GLOBAL_CONFIG           0
#define SEC_BOOK_ROLE               1
#define SEC_MAX_SPREAD              2
#define SEC_BOOK_PRIORITY           3
#define SEC_OVERSIZE_SCORE          4
#define SEC_ORDER_ENTRY             5
#define SEC_SPLIT_CONFIG            6
#define SEC_NASDAQ_STRATEGY         7
#define SEC_MAXIMUM_SPREAD          8
#define SEC_ORDER_TOKEN_BUCKET      9

#define DEFAULT    -1
#define ENABLE      1
#define DISABLE     0

#define NDAQ_ADD_MSG                    'A'
#define NDAQ_ADD_MPID_MSG               'F'
#define NDAQ_EXECUTED_MSG               'E'
#define NDAQ_EXECUTED_PRICE_MSG         'C'
#define NDAQ_CANCEL_MSG                 'X'
#define NDAQ_DELETE_MSG                 'D'
#define NDAQ_REPLACE_MSG                'U'
#define NDAQ_STOCK_TRADING_ACTION_MSG   'H'
#define NDAQ_CROSS_TRADE_MSG            'Q'

//For kind of thread to start from command line
#define ALL_THREADS                     'L'
#define ALL_BOOK_THREADS                'Y'
#define ALL_ORDER_THREADS               'O'

#define ARCA_BOOK_THREAD                'A'
#define NASDAQ_BOOK_THREAD              'N'
#define BATSZ_BOOK_THREAD               'B'
#define BYX_BOOK_THREAD                 'C'
#define NYSE_BOOK_THREAD                'Z'
#define EDGX_BOOK_THREAD                'E'
#define EDGA_BOOK_THREAD                'D'
#define NDBX_BOOK_THREAD                'X'
#define PSX_BOOK_THREAD                 'P'
#define AMEX_BOOK_THREAD                'M'

#define ARCA_DIRECT_THREAD              'a'
#define NASDAQ_OUCH_THREAD              'n'
#define NASDAQ_BX_THREAD                'x'
#define NASDAQ_PSX_THREAD               'p'
#define NASDAQ_RASH_THREAD              'r'
#define BATSZ_BOE_THREAD                'b'
#define BYX_BOE_THREAD                  'y'
#define NYSE_CCG_THREAD                 'z'
#define EDGX_THREAD                     'e'
#define EDGA_THREAD                     'd'

#define CQS_THREAD                      'Q'
#define UQDF_THREAD                     'U'

#define MAX_MARKET_TIME_TYPE    3
#define PRE_MARKET              0
#define INTRADAY                1
#define POST_MARKET             2

#define PRE_MARKET_SESSION      "PRE-MARKET"
#define INTRADAY_SESSION        "INTRADAY"
#define POST_MARKET_SESSION     "POST-MARKET"

#define MAX_TIMELINE_INTRADAY_OF_MIN_SPREAD   10
#define MAX_MIN_SPREAD_OF_DAY                 24 * 3600

#define FILE_NAME_OF_EXCHANGES_CONF           "exchange_list"
#define FILE_NAME_OF_ISO_ALGORITHM            "iso_algorithm"
#define FILE_NAME_OF_SPLIT_SYMBOL_INFO        "split_symbol.csv"
#define FILE_NAME_OF_TRADING_SCHEDULE         "trading_schedule"
#define FILE_NAME_OF_STALE_DATA_THRESHOLD     "data_delay_threshold"

// For UQDF
#define MAX_UQDF_CHANNELS           6

#define MAX_UQDF_HEADER_LEN        24
#define MAX_UQDF_MSG_LEN          128

// For CQS
#define MAX_CQS_MULTICAST_LINES    24

#define MAX_CQS_HEADER_LEN         24
#define MAX_CQS_MSG_LEN           512

#define MAX_TIMELINE_OF_SHORTABILITY_SCHEDULE 16

// For OEC information at the command line
#define OEC_KEYWORD_USERNAME                0
#define OEC_KEYWORD_PASSWORD                1
#define OEC_KEYWORD_SENDER_SUB_ID           2
#define OEC_KEYWORD_CANCEL_ON_DISCONNECT    3
#define OEC_KEYWORD_UNIT_REPLAY             4
#define OEC_KEYWORD_COMP_ID                 5
#define OEC_KEYWORD_SENDER_COMP_ID          6

// For Symbol Split Info
#define MAX_SYMBOL_SPLIT_RANGE_ID           8 //10^8 < 2^32, split symbol range id will send to AS as 4 bytes

#define FN_SEQUENCE_NUM_ARCA    "arca_direct_seqs"
#define FN_SEQUENCE_NUM_OUCH    "nasdaq_ouch_seqs"
#define FN_SEQUENCE_NUM_BX      "nasdaq_bx_seqs"
#define FN_SEQUENCE_NUM_PSX     "nasdaq_psx_seqs"
#define FN_SEQUENCE_NUM_RASH    "nasdaq_rash_seqs"
#define FN_SEQUENCE_NUM_EDGX    "edgx_seqs"
#define FN_SEQUENCE_NUM_EDGA    "edga_seqs"
#define FN_SEQUENCE_NUM_NYSE    "nyse_ccg_seqs"
#define FN_SEQUENCE_NUM_BATSZ   "batsz_boe_seqs"
#define FN_SEQUENCE_NUM_BYX     "byx_boe_seqs"

#define FN_OEC_CONF_ARCA        "arca_direct_conf"
#define FN_OEC_CONF_OUCH        "nasdaq_ouch"
#define FN_OEC_CONF_BX          "nasdaq_bx"
#define FN_OEC_CONF_PSX         "nasdaq_psx"
#define FN_OEC_CONF_RASH        "nasdaq_rash"
#define FN_OEC_CONF_NYSE        "nyse_ccg_conf"
#define FN_OEC_CONF_EDGX        "edgx_conf"
#define FN_OEC_CONF_EDGA        "edga_conf"
#define FN_OEC_CONF_BATSZ       "batsz_boe_conf"
#define FN_OEC_CONF_BYX         "byx_boe_conf"

#define FN_RAWDATA_ARCA  "arca_direct.txt"
#define FN_RAWDATA_ISLD  "nasdaq_ouch.txt"
#define FN_RAWDATA_BX    "nasdaq_bx.txt"
#define FN_RAWDATA_PSX   "nasdaq_psx.txt"
#define FN_RAWDATA_RASH  "nasdaq_rash.txt"
#define FN_RAWDATA_NYSE  "nyse_ccg.txt"
#define FN_RAWDATA_EDGX  "edgx.txt"
#define FN_RAWDATA_EDGA  "edga.txt"
#define FN_RAWDATA_BATSZ "batsz_boe.txt"
#define FN_RAWDATA_BYX   "byx_boe.txt"


#define FN_TRADE_OUTPUT_LOG     "tradeOutputLog.txt"
#define FN_BBO_INFO             "bbo_info.txt"
/****************************************************************************
** Data structure definitions
****************************************************************************/
typedef struct t_MonitorArcaBook
{
  int isReceiving[MAX_ARCA_GROUPS];
  int recvCounter[MAX_ARCA_GROUPS];
  int isProcessing;
  int procCounter;
}t_MonitorArcaBook;

typedef struct t_MonitorNasdaqBook
{
  int isReceiving;
  int isProcessing;
  int recvCounter;
  int procCounter;
}t_MonitorNasdaqBook;

typedef struct t_MonitorNyseBook
{
  int isReceiving[MAX_NYSE_BOOK_GROUPS];
  int recvCounter[MAX_NYSE_BOOK_GROUPS];
  int isProcessing;
  int procCounter;
}t_MonitorNyseBook;

typedef struct t_MonitorBatszBook
{
  int isReceiving[MAX_BATSZ_UNIT];
  int recvCounter[MAX_BATSZ_UNIT];
  int isProcessing;
  int procCounter;
}t_MonitorBatszBook;

typedef struct t_MonitorBYXBook
{
  int isReceiving[MAX_BYX_UNIT];
  int recvCounter[MAX_BYX_UNIT];
  int isProcessing;
  int procCounter;
} t_MonitorBYXBook;

typedef struct t_MonitorEdgxBook
{
  int isReceiving[MAX_EDGX_BOOK_GROUP];
  int recvCounter[MAX_EDGX_BOOK_GROUP];
  int isProcessing;
  int procCounter;
}t_MonitorEdgxBook;

typedef struct t_MonitorEdgaBook
{
  int isReceiving[MAX_EDGA_BOOK_GROUP];
  int recvCounter[MAX_EDGA_BOOK_GROUP];
  int isProcessing;
  int procCounter;
}t_MonitorEdgaBook;

typedef struct t_MonitorCQS
{
  int isReceiving[MAX_CQS_MULTICAST_LINES];
  int recvCounter[MAX_CQS_MULTICAST_LINES];
  int isProcessing;
  int procCounter;
}t_MonitorCQS;

typedef struct t_MonitorUQDF
{
  int isReceiving[MAX_UQDF_CHANNELS];
  int recvCounter[MAX_UQDF_CHANNELS];
  int isProcessing;
  int procCounter;
}t_MonitorUQDF;

typedef struct t_Global_Config
{
  double maxBuyingPower;
  int maxShareVolume; 
  int maxConsecutiveLoser;
  int maxStuckPosition;
  int serverRole;
  double minSpread;
  double maxSpread;
  double secFee;
  double commission;
  double loserRate;
  double exRatio;
  int nasdaqAuctionBeginTime;
  int nasdaqAuctionEndTime;
} t_Global_Config;

// Some description here
typedef struct t_Book_Role
{
  char status[MAX_ECN_BOOK];
} t_Book_Role;

// Some description here
typedef struct t_Book_MaxSpread
{
  char status[MAX_ECN_BOOK];  
} t_Book_MaxSpread;

typedef struct t_Book_OversizeScore
{
  double oversizeScore[MAX_ECN_BOOK]; 
} t_Book_OversizeScore;

// Some description here
typedef struct t_Entry_Order_Placement
{
  int status[MAX_ECN_BOOK]; 
} t_Entry_Order_Placement;

// Some description here
typedef struct t_Book_Priority
{
  char status[MAX_ECN_BOOK];
} t_Book_Priority;

// Some description here
typedef struct t_SymbolSplitConfiguration
{
  /*
    Symbols could be splitted to n parts, it bases on the configuration files
    The variable will store the part number indicating symbol range TS will process.
    
    Suppose, they are splitted to 3 parts, the configuration looks like this
    + Group 1: For symbol range A-F
      Symbol Range ID: 1
      Start of Range: A
      End of Range: F
    + Group 2: For symbol range G-O
      Symbol Range ID: 2
      Start of Range: G
      End of Range: O
    + Group 3: For symbol range P-Z
      Symbol Range ID: 3
      Start of Range: P
      End of Range: Z
  */
  int symbolRangeID;
  char startOfRange;
  char endOfRange;
  char __padding[2];
} t_SymbolSplitConfiguration;

typedef struct t_SymbolSplitInfo
{
  // char arca[MAX_ARCA_GROUPS];
  // char nasdaq;
  char nyse[MAX_NYSE_BOOK_GROUPS];
  char batsz[MAX_BATSZ_UNIT];
  char byx[MAX_BYX_UNIT];
  char edgx[MAX_EDGX_BOOK_GROUP];
  char cqs[MAX_CQS_MULTICAST_LINES];
  char uqdf[MAX_UQDF_CHANNELS];
  char edga[MAX_EDGA_BOOK_GROUP];
} t_SymbolSplitInfo;

// Some description here
typedef struct t_NASDAQ_StrategyConfiguration
{
  char RASH_strategy[MAX_TRADE_SIDE][MAX_MARKET_TIME_TYPE][8];
} t_NASDAQ_StrategyConfiguration;

typedef struct t_MaxSpreadInfo
{
  double execPriceUnder_25USD;
  double execPriceUnder_50USD;
  double execPriceOver_50USD;
} t_MaxSpreadInfo;

typedef struct t_MaxSpreadConfiguration
{
  t_MaxSpreadInfo maxSpread[MAX_MARKET_TIME_TYPE];
} t_MaxSpreadConfiguration;

typedef struct t_OrderTokenBucket
{
  unsigned int bucketSize;
  unsigned int refillPerSec;
  int tokens;  //must be a signed integer!
  unsigned long lastUpdatedTime;
} t_OrderTokenBucket;

// Some description here
typedef struct t_Algorithm_Config
{
  t_Global_Config globalConfig;
  t_Book_Role bookRole;
  t_Book_MaxSpread bookMaxSpread;
  t_Book_Priority bookPriority;
  t_Book_OversizeScore bookOversizeScore;
  t_Entry_Order_Placement entryOrderPlacement[MAX_SERVER_ROLE];
  t_SymbolSplitConfiguration splitConfig;
  t_NASDAQ_StrategyConfiguration NASDAQStrategyConfig;
  t_MaxSpreadConfiguration maxSpreadConfig;
  t_OrderTokenBucket orderBucket;
} t_Algorithm_Config;

typedef struct t_Min_Spread_Switch
{
  int timeInSeconds;
  double minSpread;
} t_Min_Spread_Switch;

typedef struct t_Min_Spread_Intraday
{
  int countTimes;
  t_Min_Spread_Switch minSpreadList[MAX_TIMELINE_INTRADAY_OF_MIN_SPREAD];
} t_Min_Spread_Intraday;

typedef struct t_Min_Spread_Conf
{
  double preMarket;
  double postMarket;
  t_Min_Spread_Intraday intraday;
} t_Min_Spread_Conf;

// Data structure for a multicast group
typedef struct t_DBLConnectionInfo
{
  char ip[MAX_LINE_LEN];
  char NICName[MAX_LINE_LEN];
  int port;
  int _unused;  //For memory alignment;
} t_DBLConnectionInfo;

typedef struct t_ConnectionInfo
{
  char ip[80];
  int port;
  int socket;
} t_ConnectionInfo;

typedef struct t_IP_Addr
{
  char NICName[80];
  char ip[80];
  int port;
} t_IP_Addr;

typedef struct t_NASDAQ_Book_Conf
{
  t_DBLConnectionInfo Data;
  t_IP_Addr     Backup;

  int recoveryThreshold;
  unsigned short lossCounter[DBLFILTER_NUM_QUEUES];
} t_NASDAQ_Book_Conf;

typedef struct t_ARCA_Book_GroupInfo
{
  char dataIP[MAX_LINE_LEN];
  char backupIP[MAX_LINE_LEN];

  int groupIndex;
  
  unsigned short lossCounter[DBLFILTER_NUM_QUEUES];
    
  int dataPort;
  int backupPort;
  
}t_ARCA_Book_GroupInfo;

// Some description here
typedef struct t_ARCA_Book_Conf
{
  char NICName[MAX_LINE_LEN];       //NIC name used for Data Multicast Groups + Retransmission Multicast Groups
  char backupNIC[MAX_LINE_LEN];
  char currentFeedName;

  int recoveryThreshold;
  FILE *symbolMappingFp;

  //Info specific to a group
  t_ARCA_Book_GroupInfo group[MAX_ARCA_GROUPS];
    
}t_ARCA_Book_Conf;

typedef struct t_NYSE_Book_Group_Info
{
  char receiveDataIP[MAX_LINE_LEN];   //IP of Multicast Server for sending Data, IP is read from config file
  int receiveDataPort;          //port of Multicast Server for sending Data, port is read from config file

  char backupIP[MAX_LINE_LEN];
  int backupPort;
  
  unsigned short lossCounter[DBLFILTER_NUM_QUEUES];
  
  int groupIndex;             //Group 1 has index 0. Group 2 has index 1, ....
  int groupPermission;          //Depend on Symbol Split configuration, the group will be allowed to joined into the multicast or not
}t_NYSE_Book_Group_Info;

// Some description here
typedef struct t_NYSE_Book_Conf
{
  char refIP[300][16];
  int refPort[300];
  int countRef;
  int recoveryThreshold;
  
  char NICName[MAX_LINE_LEN];       //NIC Name used for Data Multicast Groups + Retransmission Multicast Groups
  char backupNIC[MAX_LINE_LEN];

  //Info specific to a group
  t_NYSE_Book_Group_Info group[MAX_NYSE_BOOK_GROUPS];
}t_NYSE_Book_Conf;

typedef struct t_BATS_Book_Group_Info
{
  char dataIP[MAX_LINE_LEN];
  char backupIP[MAX_LINE_LEN];
  
  unsigned short lossCounter[DBLFILTER_NUM_QUEUES];
  
  int unitId;
  int dataPort;
  int backupPort;
  int groupPermission;
} t_BATS_Book_Group_Info;

// Some description here
typedef struct t_BATS_Book_Conf
{
  char refIP[300][16];
  int refPort[300];
  int countRef;
  int recoveryThreshold;

  // NIC Name used for Data Multicast Groups + Retransmission Multicast Groups
  char NICName[MAX_LINE_LEN];
  char backupNIC[MAX_LINE_LEN];
  
  //Info specific to a unit
  t_BATS_Book_Group_Info group[MAX_BATSZ_UNIT];
}t_BATS_Book_Conf;

// EDGX/EDGA BOOK GROUP info
typedef struct t_EDGE_Book_Group_Info
{
  char dataIP[MAX_LINE_LEN];
  char backupIP[MAX_LINE_LEN];
  
  unsigned short lossCounter[DBLFILTER_NUM_QUEUES];
  
  int dataPort;
  int backupPort;
  int groupPermission;
} t_EDGE_Book_Group_Info;

typedef struct t_EDGE_Book_Conf
{
  char refIP[300][16];
  int refPort[300];
  int countRef;
  
  char NICName[MAX_LINE_LEN];
  char backupNIC[MAX_LINE_LEN];
  int recoveryThreshold;
  
  //Info specific to a unit
  #if MAX_EDGX_BOOK_GROUP != MAX_EDGA_BOOK_GROUP
    #error EDGX and EDGA have different number of groups
  #endif
  t_EDGE_Book_Group_Info group[MAX_EDGX_BOOK_GROUP];
} t_EDGE_Book_Conf;

/*
The structure is used to manage 
status/behavior of a connection to ECN Book
*/
typedef struct t_BookStatus
{
  int isConnected;
} t_BookStatus;

/*
The structure is used to manage 
status/behavior of a connection to ECN Order
*/
typedef struct t_OrderStatus
{
  /*
  The field is used to store status of a connection that was made to 
  an ECN Order successfully (CONNECTED) or disconnect to (DISCONNECTED)
  (Default value = DISCONNECTED)
  */
  int isConnected; 
  
  /*
  The field is used to store request from AS: YES or NO
  YES: Trade Server should makes a connection to an ECN Order Server
  NO: Trade Server should disconnect connection to an ECN Order Server
  (Default value = YES)
  */
  int shouldConnect;
} t_OrderStatus;

typedef struct t_CQS_Msg
{ //Please be careful with memory alignment! it will slow down speed of application alot!
  int msgSeqNum;
  char msgHeader[MAX_CQS_HEADER_LEN + 1];
  char _unused; //For memory alignment
  char msgCategory;
  char msgType;
  char msgBody[MAX_CQS_MSG_LEN];
  
} t_CQS_Msg;

typedef struct t_CQS_Group
{
  char ip[MAX_LINE_LEN];

  int port;
  int primaryLastSequenceNumber;
  int index;
  int groupPermission;
  
  // Thread variables
  pthread_t primaryProcessThreadID;
} t_CQS_Group;


typedef struct t_CQS_Conf
{
  t_CQS_Group group[MAX_CQS_MULTICAST_LINES];
  
  //Shared
  char NICName[MAX_LINE_LEN];
}t_CQS_Conf;

typedef struct t_UQDF_Msg
{ 
  int msgSeqNum;
  char msgHeader[MAX_UQDF_HEADER_LEN + 1];
  char _unused; //For memory alignment
  char msgCategory;
  char msgType;
  char msgBody[MAX_UQDF_MSG_LEN];
} t_UQDF_Msg;

typedef struct t_UQDF_Group
{ char ip[MAX_LINE_LEN];
  int port;
  int primaryLastSequenceNumber;
  int index;
  int groupPermission;
  
  pthread_t primaryProcessThreadID;
} t_UQDF_Group;

typedef struct t_UQDF_Conf
{
  //specific
  t_UQDF_Group group[MAX_UQDF_CHANNELS];
  
  //shared
  char NICName[MAX_LINE_LEN];

}t_UQDF_Conf;

// For OEC Info
//This structure is used to save the address of keyword & it's value which found from raw string of MDC or OEC
typedef struct t_Keyword_Value_Pointer
{
  char keyword[MAX_LINE_LEN]; // Keyword
  char *posKeyword;     // Position of Keyword
  char *posValue;       // Position of Keyword's Value
}t_Keyword_Value_Pointer;

#define MAX_OEC_INFO_FIELD 10
typedef struct t_OEC_Info
{
  int orderIndex;

  t_Keyword_Value_Pointer positions[MAX_OEC_INFO_FIELD];
  
  char ipAddress[IP_ADDRESS_FIELD_LEN]; // keyword: address
  int port;                             // keyword: address
  
  char userName[USER_NAME_FIELD_LEN];   // username, sender_comp_id
  char password[PASSWORD_FIELD_LEN];    // password
  
  char compid[MAX_COMP_GROUP_ID_LEN];   // compid
  
  char sessionSubId[MAX_LINE_LEN];      // sender_sub_id
  
  int autoCancelOnDisconnect;           // cancel_on_disconnect
  int noUnspecifiedUnitReplay;          // no_unspecified_unit_replay, in BATSZ config structure it is unsigned char 
}t_OEC_Info;

// For MDC Info
#define MAX_MDC_ARCA_INFO_FIELD (4 + 3 * MAX_ARCA_GROUPS + 1)
typedef struct t_MDC_ARCA_Info
{
  t_Keyword_Value_Pointer positions[MAX_MDC_ARCA_INFO_FIELD];
                                
  char NICName[MAX_LINE_LEN];       //dbl_nic
  char backupNIC[MAX_LINE_LEN];

  t_ConnectionInfo primaryMulticast[MAX_ARCA_GROUPS]; //group[1-4]_primary_mc_address
  t_ConnectionInfo backupMulticast[MAX_ARCA_GROUPS];  //group[1-4]_backup_mc_address
}t_MDC_ARCA_Info;

#define MAX_MDC_NASDAQ_INFO_FIELD (7 + 1)
typedef struct t_MDC_NASDAQ_Info
{
  t_Keyword_Value_Pointer positions[MAX_MDC_NASDAQ_INFO_FIELD];

  char DataInterface[MAX_LINE_LEN];
  char BackupInterface[MAX_LINE_LEN];
  int maxMsgLoss;           //max_msg_loss
  t_ConnectionInfo primaryMulticast;  //group1_primary_mc_address
  t_ConnectionInfo backupMulticast; //group1_retrans_mc_address
}t_MDC_NASDAQ_Info;

#define MAX_MDC_NYSE_INFO_FIELD (3 + 4 * MAX_NYSE_BOOK_GROUPS + 1)
typedef struct t_MDC_NYSE_Info
{
  t_Keyword_Value_Pointer positions[MAX_MDC_NYSE_INFO_FIELD];
  
  char NICName[MAX_LINE_LEN];       //dbl_nic
  char backupNIC[MAX_LINE_LEN];
  t_ConnectionInfo primaryMulticast[MAX_NYSE_BOOK_GROUPS]; //group[1-20]_primary_mc_address
  t_ConnectionInfo backupMulticast[MAX_NYSE_BOOK_GROUPS]; //group[1-20]_backup_mc_address
}t_MDC_NYSE_Info;

#define MAX_MDC_BATSZ_INFO_FIELD (6 + 3 * MAX_BATSZ_UNIT + 1)
typedef struct t_MDC_BATSZ_Info
{
  t_Keyword_Value_Pointer positions[MAX_MDC_BATSZ_INFO_FIELD];
  
  char NICName[MAX_LINE_LEN];       //dbl_nic
  char backupNIC[MAX_LINE_LEN];
  t_ConnectionInfo primaryMulticast[MAX_BATSZ_UNIT]; //group[1-32]_primary_mc_address
  t_ConnectionInfo backupMulticast[MAX_BATSZ_UNIT]; //group[1-32]_retrans_mc_address
}t_MDC_BATSZ_Info;

#define MAX_MDC_BYX_INFO_FIELD (6 + 3 * MAX_BYX_UNIT + 1)
typedef struct t_MDC_BYX_Info
{
  t_Keyword_Value_Pointer positions[MAX_MDC_BYX_INFO_FIELD];

  char NICName[MAX_LINE_LEN];       //dbl_nic
  char backupNIC[MAX_LINE_LEN];
  t_ConnectionInfo primaryMulticast[MAX_BYX_UNIT]; //group[1-32]_primary_mc_address
  t_ConnectionInfo backupMulticast[MAX_BYX_UNIT]; //group[1-32]_retrans_mc_address
} t_MDC_BYX_Info;

#define MAX_MDC_EDGX_INFO_FIELD (5 + 3 * MAX_EDGX_BOOK_GROUP + 1)
typedef struct t_MDC_EDGX_Info
{
  t_Keyword_Value_Pointer positions[MAX_MDC_EDGX_INFO_FIELD];
  
  char NICName[MAX_LINE_LEN];       //dbl_nic
  char backupNIC[MAX_LINE_LEN];
  t_ConnectionInfo primaryMulticast[MAX_EDGX_BOOK_GROUP]; //group[1-10]_primary_mc_address
  t_ConnectionInfo backupMulticast[MAX_EDGX_BOOK_GROUP];  //group[1-10]_retrans_mc_address
}t_MDC_EDGX_Info;

#define MAX_MDC_EDGA_INFO_FIELD (5 + 3 * MAX_EDGA_BOOK_GROUP + 1)
typedef struct t_MDC_EDGA_Info
{
  t_Keyword_Value_Pointer positions[MAX_MDC_EDGA_INFO_FIELD];
  
  char NICName[MAX_LINE_LEN];
  char backupNIC[MAX_LINE_LEN];
  t_ConnectionInfo primaryMulticast[MAX_EDGA_BOOK_GROUP]; //group[1-10]_primary_mc_address
  t_ConnectionInfo backupMulticast[MAX_EDGA_BOOK_GROUP];  //group[1-10]_retrans_mc_address
}t_MDC_EDGA_Info;

#define MAX_MDC_CQS_INFO_FIELD (1 + MAX_CQS_MULTICAST_LINES + 1)
typedef struct t_MDC_CQS_Info
{
  t_Keyword_Value_Pointer positions[MAX_MDC_CQS_INFO_FIELD];
  
  char NICName[MAX_LINE_LEN];       //dbl_nic
  t_ConnectionInfo eMulticast[MAX_CQS_MULTICAST_LINES / 2]; //group[1-12]_e_mc_address
  t_ConnectionInfo fMulticast[MAX_CQS_MULTICAST_LINES / 2]; //group[1-12]_f_mc_address
}t_MDC_CQS_Info;

#define MAX_MDC_UQDF_INFO_FIELD (1 + MAX_UQDF_CHANNELS + 1)
typedef struct t_MDC_UQDF_Info
{
  t_Keyword_Value_Pointer positions[MAX_MDC_UQDF_INFO_FIELD];
  
  char NICName[MAX_LINE_LEN];       //dbl_nic
  t_ConnectionInfo primaryMulticast[MAX_UQDF_CHANNELS]; //group[1-6]_primary_mc_address
}t_MDC_UQDF_Info;

typedef struct t_TradingAccount
{
  char accountType;
  char ArcaDirectAccount[31]; //currently, TS uses ARCA DIRECT's account as its full trading account to compare with AS's Trading Account
  char NyseCCGAccount[32];
  char OUCHAccount[32];
  char RASHAccount[32];
  char BATSZBOEAccount[32];
  char BYXBOEAccount[32];
  char EDGXAccount[32];
  char EDGAAccount[32];
  char BXAccount[32];
  char PSXAccount[32];
} t_TradingAccount;

typedef struct t_TradingSchedule
{
  int timeToEndOfPreMarket; // seconds since start of day
  int timeToEndOfIntraday;  // seconds since start of day
  
  // market open/close time for all venues (in nanoseconds since epoch)
  long arca_open;
  long arca_close;

  long nasdaq_open;
  long nasdaq_close;

  long bx_open;
  long bx_close;

  long psx_open;
  long psx_close;

  long rash_open;
  long rash_close;

  long bzx_open;
  long bzx_close;

  long byx_open;
  long byx_close;

  long nyse_open;
  long nyse_close;

  long edgx_open;
  long edgx_close;

  long edga_open;
  long edga_close;

} t_TradingSchedule;

typedef struct t_FilterBuffers
{
  int                 index;
  volatile short*     flushTrigger;
  
  unsigned char*      orderEntryReceiveQueue;
  long*               orderEntryReceiveSeq;   
  spsc_read_handle    orderEntryReceiveReader;
  
  unsigned char*      orderEntrySendQueue;
  long*               orderEntrySendSeq; 
  spsc_write_handle   orderEntrySendWriter;
  
  unsigned char*      marketDataReceiveQueue;
  long*               marketDataReceiveSeq;  
  spsc_read_handle    marketDataReceiveReader;
  
  unsigned char*      dataBlockQueue;
  long*               dataBlockSeq;
  spsc_read_handle    dataBlockReader;
  spsc_write_handle   dataBlockWriter;
  
  unsigned char*      tradeOutputQueue;
  long*               tradeOutputSeq;
  spsc_read_handle    tradeOutputReader;
  spsc_write_handle   tradeOutputWriter;
  
  t_OrderTokenBucket* orderBucket;
} t_FilterBuffers;

typedef enum EnumOecConfValues
{
  OEC_CONF_FIELD_IP = 1,
  OEC_CONF_FIELD_PORT,
  OEC_CONF_FIELD_USERNAME,
  OEC_CONF_FIELD_PASSWORD,
  OEC_CONF_FIELD_COMPID,
  OEC_CONF_FIELD_AUTO_CANCEL,
  OEC_CONF_FIELD_SESSION_SUB_ID,
  OEC_CONF_FIELD_NO_UNSPECIFIED_UNIT_REPLAY,
  OEC_CONF_FIELD_PROTOCOL
} EnumOecConfValues;
 
#define PROTOCOL_UDP 0
#define PROTOCOL_TCP 1
typedef struct t_OecConfig
{
  int incomingSeqNum;
  int outgoingSeqNum;
  int numbersOfBatszUnits;
  int protocol;
  int autoCancelOnDisconnect;
  int noUnspecifiedUnitReplay;
  int enableTrading;
  int isConnected;
  int port;
  int sessionId;
  int socket;
  
  char ipAddress[32];
  char userName[32];
  char password[32];
  char compGroupID[32];
  char sessionSubId[32];
  
} t_OecConfig;

/****************************************************************************
** Global variable definitions
****************************************************************************/
t_OecConfig OecConfig[MAX_ECN_ORDER];
extern t_Algorithm_Config AlgorithmConfig;
extern t_BookStatus BookStatusMgmt[MAX_ECN_BOOK];
extern t_OrderStatus OrderStatusMgmt[MAX_ECN_ORDER];

extern t_BookStatus CQS_StatusMgmt;
extern t_CQS_Conf CQS_Conf;

extern t_BookStatus UQDF_StatusMgmt;
extern t_UQDF_Conf UQDF_Conf;

extern int ISO_Trading;

// Role of Books in the ISO algorithm
extern char BookISO_Trading[MAX_ECN_BOOK];
extern char BookPrePostISOTrading[MAX_ECN_BOOK];

extern t_TradingAccount TradingAccount;

extern t_Min_Spread_Conf MinSpreadConf;
extern double MinSpreadInSecond[MAX_MIN_SPREAD_OF_DAY];

// For OEC Information
extern int IsGottenOrderConfig[MAX_ECN_ORDER];

// For MDC Information
extern int IsGottenBookConfig[MAX_ECN_BOOK];
extern int IsGottenFeedConfig[MAX_BBO_SOURCE];

extern int IsGottenASConfig;
extern int IsGottenTradingAccountConfig;

extern t_SymbolSplitInfo SymbolSplitInfo[MAX_SYMBOL_SPLIT_RANGE_ID];

extern t_TradingSchedule TradingSchedule;

// reserve one extra send queue for manual mode
extern t_FilterBuffers FilterBuffers[DBLFILTER_NUM_QUEUES+1];

extern int ManualMode;

extern __thread int _consumerId; // thread local global so everyone knows which consumer thread they are running in

extern int NextSessionId;

extern long DataDelayThreshold[MAX_ECN_BOOK];
extern int TotalTimeOffsetInSeconds;

extern volatile char MarketDataDeviceInitialized;

/****************************************************************************
** Function declarations
****************************************************************************/

// Order configuration management
int LoadOecConfigFromFile(const char *fileName, t_OecConfig *config, const EnumOecConfValues valueFlag[], const int numValue);
int LoadAllOrdersConfig(void);
int LoadSequenceNumber(const char *fileName, int oecId);
int LoadIncomingAndOutgoingSeqNum(const char *fullPath, int oecId);
int LoadIncomingSeqNum(const char *fullPath, int oecId);
int LoadBatsSeqNum(const char *fullPath, const int oecId);

int LoadAllBooksConfig(void);
int LoadARCA_Book_Config(const char *fileName, t_ARCA_Book_GroupInfo group[MAX_ARCA_GROUPS]);
int LoadNyseBookConfig(const char *fileName, const int mdcIndex);
int LoadNASDAQBookConfig(const char *fileName, t_NASDAQ_Book_Conf *config);
int LoadNDBXBookConfig(const char *fileName);
int LoadNYSE_CCG_Config(const char *fileName, void *configInfo);
int LoadBATSZ_Book_Config(const char *fileName);
int LoadBYX_Book_Config(const char *fileName);
int Load_EDGX_BookConfig(const char *fileName);
int Load_EDGA_BookConfig(const char *fileName);

int LoadAS_Config(const char *fileName, void *configInfo);

int LoadAlgorithmConfig(const char *fileName, t_Algorithm_Config *configInfo);
int LoadGlobalConfiguration(t_Global_Config *globalConfig, FILE *fileDesc);
int LoadBookRoleConfiguration(t_Book_Role *bookRole, FILE *fileDesc);
int LoadBookMaxSpread(t_Book_MaxSpread *bookMaxSpread, FILE *fileDesc);
int LoadBookPriority(t_Book_Priority *bookPriority, FILE *fileDesc);
int LoadEntryOrderPlacement(t_Entry_Order_Placement entryOrderPlacement[MAX_SERVER_ROLE], FILE *fileDesc);
int LoadSplitConfiguration(t_SymbolSplitConfiguration *splitConfig, FILE *fileDesc);
int LoadNASDAQStrategyConfiguration(t_NASDAQ_StrategyConfiguration *NASDAQStrategyConfig, FILE *fileDesc);
int LoadMaxSpreadConfiguration(t_MaxSpreadConfiguration *maxSpreadConfig, FILE *fileDesc);
int LoadOrderTokenBucketConfiguration(t_OrderTokenBucket *orderBucket, FILE *fileDesc);

int LoadSplitSymbolInfo();
int LoadMinSpreadConfig(const char *fileName);

int LoadOrderIDAndCrossIDFromFile(char const *filename, int *orderID, int *crossID);
int SaveOrderIDAndCrossIDFromFile(char const *filename, int orderID, int crossID);

int LoadCQS_Config(const char *fileName, t_CQS_Group group[MAX_CQS_MULTICAST_LINES]);
int LoadUQDF_Config(const char *fileName);
int LoadExchangesConf(void);
int SaveExchangesConf(void);

int LoadISO_Algorithm(void);
int SaveISO_Algorithm(void);

int LoadTradingAccountFromFile(const char *fileName, void *_tradingAccount);
int LoadBookOversizeScore(t_Book_OversizeScore *bookOversizeScore, FILE *fileDesc);

int LoadTradingSchedule();
int LoadStaleMarketDataConfig();
int ProcessToInitTotalTimeOffset();

// For OEC Information
int ProcessOecInformation(char* arg);
int ParseRawOecConfig(char *rawConfig, t_OEC_Info *oecConfig);
int GetIpAddressAndPortFromOec(char *rawConfig, t_OEC_Info *oecConfig, int *posIndex);
int GetOecField(char *rawConfig, int keywordType, t_OEC_Info *oecConfig, int *posIndex);
int GetOecConfig(t_OEC_Info *oecConfig);

// For MDC Information
int ProcessMdcInformation(char* arg);
int IsValidParameter(const char *parameterStr, const char *keywords);
int GetIpAddressAndPortFromMdc(char *rawConfig, const char* keywords, t_ConnectionInfo *connectionInfo, t_Keyword_Value_Pointer *resultPosition);
int GetValueFromKeyword(char *rawConfig, const char* keywords, char value[MAX_LINE_LEN], t_Keyword_Value_Pointer *resultPosition);
int ParseArcaBookConfig(char *rawConfig);
int ParseNasdaqMdcConfig(char *rawConfig, int ecnIndex, t_NASDAQ_Book_Conf *config);
int ParseNyseBookConfig(char *rawConfig, int ecnIndex);
int ParseBatszBookConfig(char *rawConfig);
int ParseBYXBookConfig(char *rawConfig);
int ParseEdgxBookConfig(char *rawConfig);
int ParseEdgaBookConfig(char *rawConfig);
int ParseCqsFeedConfig(char *rawConfig);
int ParseUqdfFeedConfig(char *rawConfig);

// For Misc Information
int ProcessASConfigInfo(char *optarg);
int ProcessTradingAccountInfo(char *optarg);
#endif
