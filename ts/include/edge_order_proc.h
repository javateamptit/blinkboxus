/****************************************************************************
** Project name:  Equity Arbitrage Application
** File name:     edgx_order_proc.h
** Description:   N/A
** Author:      Dung Tran-Dinh
** First created: ------------------
** Last updated:  ------------------
****************************************************************************/

#ifndef __EDGE_ORDER_PROC_H__
#define __EDGE_ORDER_PROC_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include "global_definition.h"
#include "configuration.h"
#include "stock_symbol.h"

// EDGE Sequence Messages
#define SEQUENCE_MSG 'S'

#define EDGE_ORDER_LOGIN_REQUEST      'L'
#define EDGE_ENTER_ORDER_SHORT_FORM   'O'

#define EDGE_ORDER_ACCEPTED           'A'
#define EDGE_ORDER_CLIENT_HEARTBEAT   'R'
#define EDGE_ORDER_EXECUTED           'E'
#define EDGE_ORDER_CANCELED           'C'
#define EDGE_REJECTED                 'J'
#define EDGE_REJECTED_EXTENDED        'L'
#define EDGE_BROKEN_TRADE             'B'
#define EDGE_SYSTEM_EVENT             'S'

// EDGE Unsequence Messages
#define EDGE_HEARTBEAT                'H'
#define EDGE_DEBUG                    '+'
#define EDGE_LOGIN_ACCEPTED           'A'
#define EDGE_LOGIN_REJECTED           'J'
#define EDGE_END_OF_SESSION           'Z'

// EDGE Sequence Message lengths
#define EDGE_ORDER_LOGIN_REQUEST_MSG_LEN    49

#define EDGE_ORDER_ACCEPTED_LEN             56
#define EDGE_ORDER_EXECUTED_LEN             47
#define EDGE_ORDER_CANCELED_LEN             31
#define EDGE_ORDER_REJECTED_LEN             27
#define EDGE_BROKEN_TRADE_LEN               35
#define EDGE_SYSTEM_EVENT_LEN               13

// EDGE Unsequence Message lengths
#define EDGE_HEARTBEAT_LEN                   3
#define EDGE_LOGIN_ACCEPTED_LEN             33
#define EDGE_LOGIN_REJECTED_LEN              4

// For certification
#define EDGE_ENTER_ORDER_EXT                'N'
#define EDGE_CANCEL_PENDING                 'I'
#define EDGE_CANCEL_PENDING_MSG_LEN         26
#define EDGE_REPLACE_ORDER                  'U'
#define EDGE_REPLACE_ORDER_MSG_LEN          40
#define EDGE_ORDER_ACCEPTED_EXTENDED        'P'
#define EDGE_ORDER_ACCEPTED_EXT_MSG_LEN     83
#define EDGE_REPLACED_MSG                   'R'
#define EDGE_REPLACED_MSG_LEN               64
#define EDGE_PENDING_REPLACE                'D'
#define EDGE_PENDING_REPLACE_MSG_LEN        26
#define EDGE_PRICE_CORRECTION               'K'
#define EDGE_PRICE_CORRECTION_MSG_LEN       39

#define EDGE_MINIMUM_REQUIRED_RECEIVED_BYTES 2


/****************************************************************************
** Function declarations
****************************************************************************/
int Connect_EDGE(int oecType);

int Process_EDGE_DataBlock(int oecType, uint32_t symbolId, t_DataBlock *dataBlock);
void Add_EDGE_DataBlockToCollection(int oecType, t_DataBlock *dataBlock);

// Session messages
int BuildAndSend_EDGE_LogonRequest(int oecType);
int BuildAndSend_EDGE_Heartbeat(int oecType);
int BuildAndSend_LogoutRequestMsg_EDGE(int oecType);

// Application messages
int BuildAndSend_EDGE_Order(t_OrderPlacementInfo *newOrder);

int BuildAndSend_EDGE_OrderExtendedMsg(int oecType, t_OrderPlacementInfo *newOrder);
int BuildAndSend_EDGE_OrderShortMsg(int oecType, t_OrderPlacementInfo *newOrder);


int ProcessEDGE_OrderAcceptedMsg(int oecType, uint32_t symbolId, t_DataBlock *dataBlock);
int ProcessEDGE_OrderExecutedMsg(int oecType, uint32_t symbolId, t_DataBlock *dataBlock);
int ProcessEDGE_OrderCanceledMsg(int oecType, uint32_t symbolId, t_DataBlock *dataBlock);
int ProcessEDGE_OrderRejectedMsg(int oecType, uint32_t symbolId, t_DataBlock *dataBlock);
int ConvertSymbolFromComstockToCMS(const char *comstock, char *root, char *suffix);

#endif
