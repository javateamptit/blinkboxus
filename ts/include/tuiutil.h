#ifndef __TUIUTIL_H__
#define __TUIUTIL_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/

/****************************************************************************
** Data structure definitions
****************************************************************************/

/****************************************************************************
** Global variable declarations
****************************************************************************/

/****************************************************************************
** Function declarations
****************************************************************************/
char** ToToken(const char *str,char *argc);
int IsEmptyString(const char *str);
int IsComment(const char *str);
int IsValidName(const char *name);
int IsDescriptStart(const char *str);
int IsDescriptEnd(const char *str);
int IsAlpha(const char *str);
int IsNumeric(const char *str);
#endif
