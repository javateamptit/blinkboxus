/****************************************************************************
** Project name:  Equity Arbitrage Application
** File name:     nasdaq_rash_proc.h
** Description:   This file contains some declarations for nasdaq_rash_proc.c

** Author:    Sang Nguyen-Minh
** First created on 26 September 2007
** Last updated on 26 September 2007
****************************************************************************/

#ifndef __NASDAQ_RASH_PROC_H__
#define __NASDAQ_RASH_PROC_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include "global_definition.h"
#include "configuration.h"
#include "stock_symbol.h"

#define NASDAQ_RASH_NEW_ORDER_MSG_LEN         141
#define NASDAQ_RASH_NEW_ORDER_CROSS_MSG_LEN   143
#define NASDAQ_RASH_CANCEL_MSG_LEN             23

// NASDAQ RASH Sequence Messages
#define SEQUENCE_MSG                          'S'

#define NASDAQ_RASH_ORDER_ACCEPTED            'A'
#define NASDAQ_RASH_ORDER_ACCEPTED_WITH_CROSS 'R'
#define NASDAQ_RASH_ORDER_EXECUTED            'E'
#define NASDAQ_RASH_ORDER_CANCELED            'C'
#define NASDAQ_RASH_ORDER_REJECTED            'J'
#define NASDAQ_RASH_BROKEN_TRADE              'B'
#define NASDAQ_RASH_SYSTEM_EVENT              'S'

// NASDAQ RASH Unsequence Messages
#define NASDAQ_RASH_HEARTBEAT                 'H'
#define NASDAQ_RASH_DEBUG                     '+'
#define NASDAQ_RASH_LOGIN_ACCEPTED            'A'
#define NASDAQ_RASH_LOGIN_REJECTED            'J'
#define NASDAQ_RASH_END_OF_SESSION            'Z'

// NASDAQ RASH Sequence Message lengths
#define NASDAQ_RASH_ORDER_ACCEPTED_LEN              158
#define NASDAQ_RASH_ORDER_ACCEPTED_WITH_CROSS_LEN   160
#define NASDAQ_RASH_ORDER_EXECUTED_LEN               51
#define NASDAQ_RASH_ORDER_CANCELED_LEN               32
#define NASDAQ_RASH_ORDER_REJECTED_LEN               26
#define NASDAQ_RASH_BROKEN_TRADE_LEN                 35
#define NASDAQ_RASH_SYSTEM_EVENT_LEN                 12

// NASDAQ RASH Unsequence Message lengths
#define NASDAQ_RASH_HEARTBEAT_LEN                     2
#define NASDAQ_RASH_LOGIN_ACCEPTED_LEN               22
#define NASDAQ_RASH_LOGIN_REJECTED_LEN                3

/****************************************************************************
** Data structure definitions
****************************************************************************/

typedef struct t_NASDAQ_RASH_CancelOrder
{
  char *orderToken;
  int share;
  int _padding;
} t_NASDAQ_RASH_CancelOrder;

/****************************************************************************
** Global variable declarations
****************************************************************************/


/****************************************************************************
** Function declarations
****************************************************************************/
int Connect_NASDAQ_RASH();
int Process_NASDAQ_RASH_DataBlock(uint32_t symbolId, t_DataBlock *dataBlock);
void Add_NASDAQ_RASH_DataBlockToCollection(t_DataBlock *dataBlock);

void Initialize_NASDAQ_RASH(void);

// Session messages
int BuildAndSend_NASDAQ_RASH_LogonRequest(void);
int BuildAndSend_NASDAQ_RASH_Heartbeat(void);

// Application messages
int BuildAndSend_Multiple_NASDAQ_RASH_OrderMsg(t_OrderPlacement *orders);
int BuildAndSend_NASDAQ_RASH_OrderMsg(t_OrderPlacementInfo *newOrder);

int ProcessNASDAQ_RASH_OrderAcceptedMsg(uint32_t symbolId, t_DataBlock *dataBlock);
int ProcessNASDAQ_RASH_OrderExecutedMsg(uint32_t symbolId, t_DataBlock *dataBlock);
int ProcessNASDAQ_RASH_OrderCanceledMsg(uint32_t symbolId, t_DataBlock *dataBlock);
int ProcessNASDAQ_RASH_OrderRejectedMsg(uint32_t symbolId, t_DataBlock *dataBlock);

#endif
