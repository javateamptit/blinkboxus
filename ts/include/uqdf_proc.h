/****************************************************************************
** Project name:  Equity Arbitrage Application
** File name:     UQDF_proc.h
** Description:   This file contains some declarations for UQDF_proc.c
** Author:      Luan Vo-Kinh
** First created on May 02, 2008
** Last updated on May 02, 2008
****************************************************************************/

#ifndef _UQDF_PROC_H_
#define _UQDF_PROC_H_

#include "configuration.h"
#include "quote_mgmt.h"

/*****************************************************************************************************/
void InitUQDF_DataStructure(void);
int ProcessUQDF_BBOLong(uint32_t symbolId, t_UQDF_Msg *msg);
int ProcessUQDF_BBOShort(uint32_t symbolId, t_UQDF_Msg *msg);
int ProcessUQDF_RegSHOShortSalePriceTestRestrictedIndicator(uint32_t symbolId, t_UQDF_Msg *msg);

void ProcessUQDF_ShortNationalBBOAppendage(char *msg, t_NationalAppendage *nationalAppendage);
void ProcessUQDF_LongNationalBBOAppendage(char *msg, t_NationalAppendage *nationalAppendage);
int UQDF_ProcessMessage(uint32_t symbolId, char *buffer, int maxBytes, unsigned short *msgSize);
#endif
