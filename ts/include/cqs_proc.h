/****************************************************************************
** Project name:  Equity Arbitrage Application
** File name:     CQS_proc.h
** Description:   This file contains some declarations for CQS_proc.c
** Author:      Luan Vo-Kinh
** First created on Oct 03, 2008
** Last updated on Oct 03, 2008
****************************************************************************/

#ifndef _CQS_PROC_H_
#define _CQS_PROC_H_

#include "configuration.h"
#include "quote_mgmt.h"

/*****************************************************************************************************/
void InitCQS_DataStructure(void);
int ProcessCQS_LongQuoteMsg(uint32_t symbolId, t_CQS_Msg *msg);
int ProcessCQS_ShortQuoteMsg(uint32_t symbolId, t_CQS_Msg *msg);
void ProcessCQS_LongNationalBBOAppendage(char *msg, t_NationalAppendage *nationalAppendage);
void ProcessCQS_ShortNationalBBOAppendage(char *msg, t_NationalAppendage *nationalAppendage);
int CQS_ProcessMessage(uint32_t symbolId, char *buffer, int maxBytes, unsigned short *msgSize);
#endif
