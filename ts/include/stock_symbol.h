/****************************************************************************
** Project name:  Equity Arbitrage Application
** File name:     stock_symbol.h
** Description:   This file contains some declarations for stock_symbol.c

** Author:      Sang Nguyen-Minh
** First created on 18 September 2007
** Last updated on 18 September 2007
****************************************************************************/

#ifndef __STOCK_SYMBOL_H__
#define __STOCK_SYMBOL_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include "global_definition.h"
#include "utility.h"
#include "aggregation_server_proc.h"


#define SYMBOL_LEN               8
#define MAX_CHARSET_MEMBER_NO   31
#define INDEX_OF_A_CHAR         60

#define MAX_PARAMETER           64
#define MAX_LEN_PARAMETER       80

#define MAX_LONG_PARAMETER      32
#define MAX_LEN_OF_LONG_PARAMETER 10000

#define SHORT_SALE_ENABLE_BIT_FLAG        0b00000001  // Symbol is in ETB list
#define SHORT_SALE_RESTRICTION_BIT_FLAG   0b00000010  // Symbol is short sale activated (from CQS/UQDF)
#define SHORT_SALE_ALLOWED                0b00000011  // Trade Server is allowed to short symbol at entry

/****************************************************************************
** Data structure definitions
****************************************************************************/
typedef struct t_Symbol
{
  /*  This structure stores symbol's information
    filterQueueIndex: the queue id that the symbol belongs to (used with OECFilter and MDCFilter), in the range [0, DBLFILTER_NUM_QUEUES - 1]
    haltTime: the latest time that halt status is changed, measured by seconds from 1/1/1970 (EPOCH)
    isTempDisabled: this flag is used for controlling trading contention. value 0: tradable, 1: temp disabled
    tradingStatusInNyseBook: symbol status on NYSE and AMEX market (their symbol lists are not overlapped)
    symbol: string representation of the symbol in NASDAQ Integrated Trading System format (comstock format ?)
    haltStatus: Currently symbol is in the halted/normal/partial resumed state
    haltStatusSent: Has TS sent halt-status for this symbol to AS? (0: has not sent to AS, 1: sent to AS)
    shortSaleStatus: Does symbol currently have short-sell ability?
    isVolatile: Is symbol volatile-activated?
  */

  unsigned int haltTime[MAX_ECN_BOOK];
  char symbol[SYMBOL_LEN];
  char haltStatus[MAX_ECN_BOOK];
  char isStale[MAX_ECN_BOOK];
  char tradingStatusInNyseBook; //P: Pre-opening
                                //O: Opened: we will trade on both side
                                //C: Closed
                                //H: Halted
                                //S: Slow Mode on both side: BID & ASK
                                //F: Slow Mode on ASK side: we will trade on BID side only
                                //E: Slow Mode on BID side: we will trade on ASK side only
  char haltStatusSent;
  unsigned char shortSaleStatus;
  char isVolatile;
  char isTempDisabled;
  char mdcId;                   //This is either BOOK_AMEX or BOOK_NYSE, used to identify what market symbol belongs to
  //char filterQueueIndex;
} t_Symbol;

typedef struct t_SymbolMgmt
{
  //----------------------------------------------------------------------------
  // This symbol management structure can store symbols up to 8 chars length!
  //----------------------------------------------------------------------------
  
  //This contains the list of symbols
  t_Symbol symbolList[MAX_STOCK_SYMBOL];
  
  //This array stores index of symbols from 1 to 5 characters.
  short symbolIndexArray[MAX_CHARSET_MEMBER_NO]
                        [MAX_CHARSET_MEMBER_NO]
                        [MAX_CHARSET_MEMBER_NO]
                        [MAX_CHARSET_MEMBER_NO]
                        [MAX_CHARSET_MEMBER_NO];
  
  //This array stores index of symbols from 6 to 7 characters.
  short longSymbolIndexArray[MAX_STOCK_SYMBOL][MAX_CHARSET_MEMBER_NO][MAX_CHARSET_MEMBER_NO];
  
  //This array stores index of symbols with exactly 8 characters.
  short extraLongSymbolIndexArray[MAX_STOCK_SYMBOL][MAX_CHARSET_MEMBER_NO];
  
  //This is the index of last symbol in the list (number of symbols in the list)
  short nextSymbolIndex;
} t_SymbolMgmt;

typedef struct t_QueueSymbolList
{
  short stockSymbolIndexes[MAX_STOCK_SYMBOL];
  char symbols[MAX_STOCK_SYMBOL][9];
  int count;
} t_QueueSymbolList;

typedef struct t_Status
{
  char isDisable; // YES/NO
  char isUsingMaxSpreadConfig; // YES/NO
  char count;
  char symbolIsDisabledBy;   //R/S/N/A
} t_Status;

typedef struct t_Parameters
{
  char parameterList[MAX_PARAMETER][MAX_LEN_PARAMETER];
  int countParameters;
} t_Parameters;

typedef struct t_LongParameters
{
  char paramList[MAX_LONG_PARAMETER][MAX_LEN_OF_LONG_PARAMETER];
  int count;
} t_LongParameters;

typedef struct t_FileInfo
{
  char sllFileString[MAX_PATH_LEN];
  long sllTimestamp;
} t_FileInfo;

/****************************************************************************
** Global variable declarations
****************************************************************************/
extern t_SymbolMgmt SymbolMgmt;
extern t_Status SymbolStatus[MAX_STOCK_SYMBOL];
extern int SplitSymbolMapping[256];
extern char AllSymbols[MAX_STOCK_SYMBOL][9];
extern int TotalSymbolCount;

/****************************************************************************
** Function declarations
****************************************************************************/

// Stock symbol management
int InitStockSymbolDataStructure(void);
int UpdateStockSymbolIndex(const char *stockSymbol);
int GetStockSymbolIndex(const char *stockSymbol);

int LoadMaxSpreadSymbolFromFile(const char *fileName);

char Lrc_Split(const char *str, char c, void *parameter);
int Lrc_Split_String(char *strSource, char *strToFind, void *parameter);

int RequestAndProcessShortSellSymbol(void);

int UpdateNewSymbolStatus(int stockSymbolIndex);
void UpdateHaltStatusForASymbol(const int stockSymbolIndex, const int haltStatus, const unsigned int timeStamp, const int ecnIndex);

int ConvertCMSSymbolToComstock(const char *cms, char *symbol);
int ConvertCQSSymbolToComstock(char *cqs, char *comstock);
#endif
