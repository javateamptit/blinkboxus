/****************************************************************************
** Project name:  Equity Arbitrage Application
** File name:     global_definition.h
** Description:   This file contains some declarations for global_definition.c

** Author:    Sang Nguyen-Minh
** First created on 14 September 2007
** Last updated on 14 September 2007
****************************************************************************/
#ifndef __GLOBAL_DEFINITION_H__
#define __GLOBAL_DEFINITION_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <math.h>
#include <errno.h>
#include <float.h>

#include <sys/stat.h>
#include <sys/types.h> 
#include <sys/socket.h> 
#include <netdb.h>

#include <arpa/inet.h> 
#include <pthread.h>
#include <signal.h>

#define VERSION "20140605"

// Return values
#define ERROR     -1
#define SUCCESS    0
#define TRADE_DETECTED_SELF_CROSSED 99

#define MIN_PORT 1024
#define MAX_PORT 65535

// Path types
#define CONF_BOOK   0 
#define CONF_ORDER  1
#define CONF_SYMBOL 2
#define CONF_MISC   3

// Maximum length of path/file's line
#define MAX_PATH_LEN   256
#define MAX_LINE_LEN   80

// Maximum of supported ECN Orders
#define MAX_ECN_ORDER   10

// Maximum of supported ECN Books
#define MAX_ECN_BOOK    10
#define MAX_SIDE        2
#define BID_SIDE        1
#define ASK_SIDE        0
#define SHORT_SIDE      2 

// Trade actions
#define TRADE_ACTION_BUY    1
#define TRADE_ACTION_SELL   2

#define HALF_MAX_BUFFER_LEN 512

// Define some special characters
#define USED_CHAR_CODE              32
#define CHAR_CODE_NEW_LINE          10
#define CHAR_CODE_CARRIAGE_RETURN   13
#define CHAR_CODE_QUOTATION         '"'

// Null pointer
#define NULL_POINTER 0

// Index of ECN Orders
#define ORDER_ARCA_DIRECT   0
#define ORDER_NASDAQ_OUCH   1
#define ORDER_NYSE_CCG      2
#define ORDER_NASDAQ_RASH   3
#define ORDER_BATSZ_BOE     4
#define ORDER_EDGX          5
#define ORDER_EDGA          6
#define ORDER_NASDAQ_BX     7
#define ORDER_BYX_BOE       8
#define ORDER_NASDAQ_PSX    9

// Index of ECN Books
#define BOOK_ARCA           0
#define BOOK_NASDAQ         1
#define BOOK_NYSE           2
#define BOOK_BATSZ          3
#define BOOK_EDGX           4
#define BOOK_EDGA           5
#define BOOK_NDBX           6
#define BOOK_BYX            7
#define BOOK_PSX            8
#define BOOK_AMEX           9
#define ANY_ECN             -2 

// Index of ECN Venue Ids
#define VENUE_ARCA          0
#define VENUE_NASDAQ        1
#define VENUE_NYSE          2
#define VENUE_BATSZ         3
#define VENUE_EDGX          4
#define VENUE_RASH          5 
#define VENUE_EDGA          6
#define VENUE_NDBX          7
#define VENUE_BYX           8
#define VENUE_PSX           9
#define VENUE_AMEX          10
  
#define VENUE_CQS           13
#define VENUE_UQDF          14
#define VENUE_ADMIN         15    // this is fixed in dblfilter!

#define FEED_CQS            13
#define FEED_UQDF           14

#define IP_ADDRESS_FIELD_LEN  80
#define USER_NAME_FIELD_LEN   80
#define PASSWORD_FIELD_LEN    80
#define MAX_COMP_GROUP_ID_LEN  6

// Connect/Disconnect connection
#define CONNECTED       1
#define DISCONNECTED    0

#define YES             'Y'
#define NO              'N'

#define SYMBOL_DISABLE_REASON_BY_RANGES_LIST  'R'
#define SYMBOL_DISABLE_REASON_BY_SYMBOL_LIST  'S'
#define SYMBOL_DISABLE_REASON_BY_ALL          'A'
#define SYMBOL_DISABLE_REASON_BY_NONE         'N'

// Trading status
#define TRADING_NORMAL  0
#define TRADING_HALTED  1

#define MAX_CLIENT_ORDER_ID 1000000

/*  Stuck locking mechanism:
  status = NO_LOCK: symbol currently has no stuck, free to buy/sell
  status >= SHORT_LOCK: symbol currently stuck LONG, SHORT Lock, so free to Buy
  status <= LONG_LOCK: symbol currently stuck SHORT, LONG Lock, so free to Sell Short
  each time symbol is stuck, status will be increased/decreased by SHORT_LOCK/LONG_LOCK
*/
#define NO_LOCK       0
#define SHORT_LOCK    1
#define LONG_LOCK     -1

// Important that this fits in 13 bits. Topic stuff will need refactoring to
// increase this quantity.
#define MAX_STOCK_SYMBOL 8190

//NYSE BOOK related
#define NYSE_BOOK_FAST_MODE                   ' '
#define NYSE_BOOK_SLOW_MODE                   'S' //this flag is set to current trading status of symbol
                                                  //to determine this symbol should be trade or not
//Slow Mode Definition
#define NYSE_BOOK_SLOW_ON_BID                 'E' //Slow Mode on BID side only
#define NYSE_BOOK_SLOW_ON_ASK                 'F' //Slow Mode on ASK side only
#define NYSE_BOOK_SLOW_DUE_TO_LRP_GAP         'U'
#define NYSE_BOOK_SLOW_DUE_TO_SET_SLOW        'W'

//NYSE symbol Trading Status
#define NYSE_BOOK_TRADING_STATUS_PRE_OPENING  'P'
#define NYSE_BOOK_TRADING_STATUS_OPENED       'O'
#define NYSE_BOOK_TRADING_STATUS_CLOSED       'C'
#define NYSE_BOOK_TRADING_STATUS_HALTED       'H'

#define MAX_TRADE_SIDE  2
#define ENTRY           0
#define EXIT            1

#define MAX_QUOTE_COLLECTED_AT_EXIT 1000

#define MAX_ARCA_GROUPS          4
#define MAX_NYSE_BOOK_GROUPS    20
#define MAX_BATSZ_UNIT          32
#define MAX_BYX_UNIT            32
#define MAX_EDGX_BOOK_GROUP     10
#define MAX_EDGA_BOOK_GROUP     10

#define NANOSECOND_PER_DAY    86400000000000L
#define TEN_RAISE_TO_9        1000000000L
/****************************************************************************
** Data structure definitions
****************************************************************************/
typedef union t_ShortConverter
{
  short value;
  char c[2];
} t_ShortConverter;

typedef union t_IntConverter
{
  int value;
  char c[4];
} t_IntConverter;

typedef union t_LongConverter
{
  long value;
  char c[8];
} t_LongConverter;

typedef union t_DoubleConverter
{
  double value;
  char c[8];
} t_DoubleConverter;

/****************************************************************************
** Global variable declarations
****************************************************************************/

extern char *VenueIdToText[VENUE_UQDF+1];
extern char *MdcIdToText[VENUE_UQDF+1];

/****************************************************************************
** Function declarations
****************************************************************************/

#endif
