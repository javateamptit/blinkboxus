/****************************************************************************
** Project name:  Equity Arbitrage Application
** File name:     order_mgmt.h
** Description:   This file contains some declarations for order_mgmt.c

** Author:    Sang Nguyen-Minh
** First created on 15 September 2007
** Last updated on 15 September 2007
****************************************************************************/

#ifndef __ORDER_MGMT_H__
#define __ORDER_MGMT_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include "global_definition.h"
#include "utility.h"
#include "dbl_filter_wrapper.h"

typedef struct t_Order
{
  double sharePrice;
  char *symbol;
  char *routeDest; //Used for RASH)
  
  int ecnToPlaceID;
  int crossID;
  int orderID;
  int stockSymbolIndex;
  int shareVolume;
  int side;
  int ecnAskBidLaunch;
  unsigned short bboId;
  char ISO_Flag;
  char ISO_Directed_Flag;
} t_Order;

/* t_Datablock constant */
#define DATABLOCK_MAX_MSG_LEN                512 - 48   // Maximum length of rawdata (message) from ECN Orders
#define DATABLOCK_ADDITIONAL_INFO_LEN        40 // Const length of additional information
#define MAX_NUM_ORDER_MSG                    30000        // Maximum number of items in the rawdata collection

#define DATABLOCK_OFFSET_FILTER_TIME         0 // 8 bytes: 0 -> 7
#define DATABLOCK_OFFSET_APP_TIME            8 // 8 bytes: 8 -> 15
#define DATABLOCK_OFFSET_VISIBLE_SIZE        16 // 4 bytes: 16 -> 19
#define DATABLOCK_OFFSET_LAUNCH              29 // 1 byte: 29
#define DATABLOCK_OFFSET_CROSS_ID            30 // 4 bytes: 30 -> 33
#define DATABLOCK_OFFSET_ACCOUNT_ID          35 // 1 byte: 35
#define DATABLOCK_OFFSET_BBO_ID              36 // 2 bytes: 36 -> 37
#define DATABLOCK_OFFSET_ENTRY_EXIT_REF      38 // 2 bytes: 38 -> 39

/****************************************************************************
** Data structure definitions
****************************************************************************/
/*
The structure is used to store sent/received 
messages to/from ECN Order servers.
*/
typedef struct t_DataBlock
{
  // Length of data block excluded block length
  int blockLen;
  
  // Length of message content
  int msgLen;
  
  // Content of message will be stored here
  char msgContent[DATABLOCK_MAX_MSG_LEN];
  
  // Content of additional information
  char addContent[DATABLOCK_ADDITIONAL_INFO_LEN];
}t_DataBlock;

/*
The structure will be used to mgmt Rawdata to/from 
an ECN Order.
*/
typedef struct t_OrderRawDataMgmt
{
  // File pointer contains file descriptor to raw data file
  FILE *fileDesc;
  
  int id; // Identifier of ECN Order
  int lastSavedIndex;
  int countRawDataBlock;
  int lastSentIndex;
    
  // All rawdata blocks will be stored at the collection
  t_DataBlock dataBlockCollection[MAX_NUM_ORDER_MSG];
}t_OrderRawDataMgmt;


#define MAX_ORDER_PER_CROSS 20
typedef struct t_OrderPlacementInfo
{
  int ecnToPlaceID;
  int orderID;
  
  int visibleSize;
  int shareVolume;
  int side;  
  int tradeAction;
  
  int ecnAskBidLaunch;
  int crossID;
    
  double sharePrice;
  
  char *routeDest;  //Used for RASH
  
  char *symbol;
  
  char rootSymbol[6];     //For EDGX/EDGA
  char symbolSuffix[6];   //For EDGX/EDGA
  
  int stockSymbolIndex;
  unsigned short bboId;
  unsigned short entryExitRefNum;
  char placementResult;
  
  int outputLogSide;

  char isRoutable;
  char ISO_Flag;
  char ISO_Directed_Flag;
  
  t_DataBlock dataBlock;
  
} t_OrderPlacementInfo;

typedef struct t_OrderPlacement
{
  int index;
  char _padding[4];
  t_OrderPlacementInfo order[MAX_ORDER_PER_CROSS];
} t_OrderPlacement;


/****************************************************************************
** Global variable declarations
****************************************************************************/
extern t_OrderRawDataMgmt OrderMgmt[MAX_ECN_ORDER];
extern int clientOrderID;

/****************************************************************************
** Function declarations
****************************************************************************/
void *StartOrderEntryWriteThread(void *args);
void *StartOrderEntryReadThread(void *args);
void *StartOrderEntryAdminConsumerThread(void *args);
void *StartOrderEntryLogConsumerThread(void *args);
void *StartDataBlockAggregationThread(void *args);
void *StartTradeOutputAggregationThread(void *args);

int InitOrderMgmtDataStructure(void);  
int InitializeOrderEntryComponents();
int LoadAllDataBlocksFromFiles(void);
unsigned short ProcessOrderEntryMessage(unsigned char venue_id, uint32_t symbolId, uint64_t arrival_time, const unsigned char *buffer_ptr);
int GetNewClientOrderID(void);
int LoadDataBlockFromFile(const char *fileName, void *orderRawDataMgmt);
int AddDataBlockToQueue(uint16_t venueId, const void *dataBlock);
int AddDataBlockToCollection(const void *dataBlock, void *orderRawDataMgmt);
int SaveDataBlockToFile(void *orderRawDataMgmt);
void *Thread_SaveDataBlockAndSeqNumToFile(void *threadArgs);
int SaveSeqNumToFile(const char *fileName, int oecId);
int SaveIncomingAndOutgoingSeqNumToFile(const char *fileName, int oecId);
int SaveIncomingSeqNumToFile(const char *fileName, int oecId);
int SaveBatsSeqNumToFile(const char *fileName, const int oecId);
int OrderBucketConsume();
#endif
