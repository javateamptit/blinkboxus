/****************************************************************************
** Project name:  Equity Arbitrage Application
** File name:     utility.h
** Description:   This file contains some declarations for utility.c

** Author:    Sang Nguyen-Minh
** First created on 14 September 2007
** Last updated on 14 September 2007
****************************************************************************/

#ifndef __UTILITY_H__
#define __UTILITY_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include <unistd.h>
#include <assert.h>
#include <sys/time.h>
#include <stdio.h>
#include <string.h>
#include <poll.h>
#include <fcntl.h>

#include "global_definition.h"
#include "dbl_filter_wrapper.h"

// To turn on print out console, we must uncoment the line below
#define TraceLog(LVL, FMT, PARMS...)                              \
  do {                                                 \
    LrcPrintfLogFormat(LVL, FMT, ## PARMS); }  \
  while (0)

#define LRC_SECS_PER_MINUTE   ((time_t)60)
#define LRC_SECS_PER_HOUR     ((time_t)(60 * LRC_SECS_PER_MINUTE))
#define LRC_SECS_PER_DAY      ((time_t)(24 * LRC_SECS_PER_HOUR))
#define LRC_SECS_PER_YEAR     ((time_t)(365 * LRC_SECS_PER_DAY))
#define LRC_SECS_PER_LEAP     ((time_t)(LRC_SECS_PER_YEAR + LRC_SECS_PER_DAY))

#define MIN(a,b) ((a) < (b) ? a : b)
#define MAX(a,b) ((a) > (b) ? a : b)
#define OPPOSITE_SIDE(a) (!a)

#define NO_LOG_LEVEL  -1
#define DEBUG_LEVEL   1
#define NOTE_LEVEL    2
#define WARN_LEVEL    3
#define ERROR_LEVEL   4
#define FATAL_LEVEL   5
#define USAGE_LEVEL   6
#define STATS_LEVEL   7

#define DEBUG_STR   "debug: "
#define NOTE_STR    "note: "
#define WARN_STR    "warn: "
#define ERROR_STR   "error: "
#define FATAL_STR   "fatal: "
#define USAGE_STR   "usage: "
#define STATS_STR   "stats: "

/****************************************************************************
** Data structure definitions
****************************************************************************/

typedef struct t_DBLFilterMgmt
{
  void *dbl_device;
  void *topic_registry;

  spsc_read_handle  marketDataLogReceiveReader;
} t_DBLFilterMgmt;

typedef struct t_OEFilterMgmt
{
  oenfc_handle      oe_network_config;
  
  unsigned char*    orderEntryAdminReceiveQueue;
  long*             orderEntryAdminReceiveSeq;   
  spsc_read_handle  orderEntryAdminReceiveReader; 
  
  unsigned char*    orderEntryLogReceiveQueue;
  long*             orderEntryLogReceiveSeq;
  spsc_read_handle  orderEntryLogReceiveReader;
} t_OEFilterMgmt;

/****************************************************************************
** Global variable declarations
****************************************************************************/

extern t_DBLFilterMgmt DBLFilterMgmt;
extern t_OEFilterMgmt OEFilterMgmt;

/****************************************************************************
** Function declarations
****************************************************************************/
// Socket utility functions
int ListenToClient(int port, int maxConcurrentConnections);
int Connect2Server(char *ip, int port, const char* serverName);
int JoinMulticastGroup(char *ipAddress, int port, int currentSocket);
int SetSocketRecvTimeout(int socket, int seconds);
int SetSocketSndTimeout(const int socket, const int sec, const int usec);
int DisableNagleAlgo(int socket);

int GetMacAddress(char *ifName, char *mac);
int GetIPAddress(char *if_name, char *ip);
int CheckValidIpv4Address(char* ip);
int CheckValidPort(int port);
int CheckErrno(int errno_args);
int CheckValidNumberString(const char *numberStr);
int IsSameMacAddress(char *mac, char *interface);

// String processing functions
char *GetFullConfigPath(const char *fileName, int pathType, char *fullConfigPath);
char *GetFullRawDataPath(const char *fileName, char *fullRawDataPath);
int CreateRawDataPath(void);
int CreateAbsolutePath(const char *path);
int IsFileTimestampToday(char *fullPath);

int TrimRight( char * buffer );
char *RemoveCharacters(char *buffer, int bufferLen);
int CountFirstBlanks(char *buffer);

void LrcPrintfLogFormat(int level, char *fmt, ...);
void PrintErrStr(int _level, const char *_str, int _errno);

int GetBookIndexByName(const char *bookName);
const char* GetBookNameByIndex(int ecn_book_id);
const char* GetOrderNameByIndex(int ecn_order_id);
int GetServerIndexByName(const char *serverRole);
int GetTradeSessionIndexByName(const char *tradSession);

int getRand(int maxValue);
long GetTimeFromEpoch();
void AddDataBlockTime(char *formattedTime);
char *GetLocalTimeString(char *timeString);
int GetNumberOfSecondFromTimeString(const char *timeString);
long GetEpochNanosFromTimeString(const char *timeString);
int GetTimeInSecondsFromFile(const char *fullPath, int *numSeconds, const char *key);
int GetTimeInEpochNanosFromFile(const char *fullPath, long *numNanos, const char *key);

inline int Get_secs_in_years(time_t *tp, int *year, int *secs_in_years);

// Numeric processing functions
int GetShortNumber(const char *buffer);
int GetShortNumberBigEndian(const char *buffer);
int GetIntNumber(const char *buffer);
int GetIntNumberBigEndian(const char *buffer);
long GetLongNumber(const char *buffer);
long GetLongNumberBigEndian(const char *buffer);
int GetUnsignedShortNumberBigEndian(const unsigned char *buffer);
int GetUnsignedIntNumberBigEndian(const unsigned char *buffer);
int GetUnsignedShortNumber(const unsigned char *buffer);
int GetUnsignedIntNumber(const unsigned char *buffer);
inline uint64_t ulongAt(const uint8_t* buf, size_t offset);
inline uint32_t uintAt(const uint8_t* buf, size_t offset);
inline uint32_t ushortAt(const uint8_t* buf, size_t offset);
inline unsigned short __bswap16(unsigned short x);
inline unsigned short __bswap16Ptr(unsigned short *ptr);

inline void strcncpy(unsigned char *des, const char *src, char pad, int totalLen);
inline int Lrc_atoi(const char * buffer, int len);
int Lrc_itoa(int value, char *strResult);
int Lrc_itoaf(int value, const char pad, const int len, char *strResult);
void Lrc_itoaf_stripped(int value, const char pad, const int len, char *strResult);
int Lrc_itoafl(int value, const char pad, const int len, char * strResult);
inline int Lrc_is_leap(int year);
inline struct tm *Lrc_gmtime (time_t *tp, int years, int secs_in_years);
void __iToStrWithLeftPad(int value, const char pad, const int len, char *strResult);

int LoadArcaSymbolsFromMappingFileToList(char *fileName, int *count, char arcaSymbolList[][9], int arcaSymbolIndex[]);
int LoadNyseSymbolsFromMappingFileToList(char *fileName, int *nyseCount, int *amexCount, char nyseSymbolList[][9], char amexSymbolList[][9], int nyseSymbolIndex[], int amexSymbolIndex[]);

static const double EPSILON = 1.0e-06;
static inline int feq(double a, double b) {   return __builtin_fabs(a-b) < EPSILON; }
static inline int fgt(double a, double b) {   return a - b >  EPSILON; }
static inline int fge(double a, double b) {   return a - b > -EPSILON; }
static inline int flt(double a, double b) {   return a - b < -EPSILON; }
static inline int fle(double a, double b) {   return a - b <  EPSILON; }

#endif
