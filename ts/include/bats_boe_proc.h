/****************************************************************************
** Project name:  Equity Arbitrage Application
** File name:     bats_boe_proc.h
** Description:   This file contains some declarations for batsz_boe_proc.c

** Author:    Khoa Pham-Tan
** First created on 31 August 2012
** Last updated  on 31 August 2012
****************************************************************************/

#ifndef __BATS_BOE_PROC__
#define __BATS_BOE_PROC__

#include "global_definition.h"
#include "configuration.h"
#include "stock_symbol.h"

/*define message types*/

// Member to BATS
#define BATS_BOE_LOGIN_REQUEST           0x01
#define BATS_BOE_LOGOUT_REQUEST          0x02
#define BATS_BOE_CLIENT_HEARTBEAT        0x03
#define BATS_BOE_NEW_ORDER               0x04
#define BATS_BOE_ORDER_CANCEL_REQUEST    0x05

//BATS to Member
#define BATS_BOE_LOGIN_RESPONSE          0x07
#define BATS_BOE_LOGOUT                  0x08
#define BATS_BOE_SERVER_HEARTBEAT        0x09
#define BATS_BOE_REPLAY_COMPLETE         0x13
#define BATS_BOE_ORDER_ACK               0x0A
#define BATS_BOE_ORDER_REJECTED          0x0B
#define BATS_BOE_ORDER_CANCELLED         0x0F
#define BATS_BOE_ORDER_EXECUTION         0x11
#define BATS_BOE_CANCEL_REJECTED         0x10
#define BATS_BOE_TRADE_CANCEL_OR_CORRECT 0x12
#define BATS_BOE_ORDER_RESTATED          0x0D

/*end of define message types*/

#define MAX_BATSZ_NUM_UNIT              255
#define MAX_BYX_NUM_UNIT                255

#if MAX_BATSZ_NUM_UNIT < MAX_BYX_NUM_UNIT
  #define MAX_BATS_NUM_UNIT MAX_BYX_NUM_UNIT
#else
  #define MAX_BATS_NUM_UNIT MAX_BATSZ_NUM_UNIT
#endif

#define BATS_BOE_RECV_TIMEOUT            10

#define BATS_BOE_HEADER_LEN              4

/*define message length*/
#define BATS_BOE_HEARDER_MSG_LEN         10

//for client
#define BATS_BOE_LOGOUT_REQUEST_MSG_LEN                10
#define BATS_BOE_LOGIN_REQUEST_MSG_LEN_FIXED           118
#define BATS_BOE_NEW_ORDER_MSG_LEN_FIXED               41
#define BATS_BOE_ORDER_CANCEN_REQUEST_MSG_LEN_FIXED    32

//for server
#define BATS_BOE_LOGIN_RESPONSE_MSG_LEN_FIXED          165
#define BATS_BOE_LOGOUT_MSG_LEN_FIXED                  76

#define BATS_BOE_ORDER_ACK_MSG_LEN_FIXED               54
#define BATS_BOE_EXECUTION_ORDER_MSG_LEN_FIXED         84
#define BATS_BOE_CANCELLED_ORDER_MSG_LEN_FIXED         47
#define BATS_BOE_REJECTED_ORDER_MSG_LEN_FIXED          107
#define BATS_BOE_REJECTED_CANCEL_MSG_LEN_FIXED         107
#define BATS_BOE_TRADE_CANCEL_OR_CORRECT_MSG_LEN_FIXED 100

/*end of define message length*/

#define BATS_BOE_START_OF_MSG            0xBABA

#define BATS_BOE_ORDER_TYPE_LIMIT        '2'

#define BATS_BOE_LOGOUT_REQUEST_YES      'Y'
#define BATS_BOE_LOGOUT_REQUEST_NO       'N'

#define BATS_BOE_OUTGOING_SEQ_NUM_INDEX  160


#define DECIMAL_NUM_PRICE                 10000
#define DECIMAL_NUM_ACCESSFEE             100000

/****************************************************************************
** Data structure definitions
****************************************************************************/
typedef struct t_BATS_BOE_SequenceOfUnit
{
  int incomingSeqNum;
}t_BATS_BOE_SequenceOfUnit;

extern t_BATS_BOE_SequenceOfUnit BATSZ_BOE_SequenceOfUnit[MAX_BATS_NUM_UNIT];
extern t_BATS_BOE_SequenceOfUnit BYX_BOE_SequenceOfUnit[MAX_BATS_NUM_UNIT];
/****************************************************************************
** Function declarations
****************************************************************************/
void Init_BATS_BOE_DataStructure(int oecType);
void Connect_BATS_BOE(int oecType);
void Add_BATS_BOE_DataBlockToCollection(int oecType, t_DataBlock *dataBlock);

int Process_BATS_BOE_DataBlock(int oecType, uint32_t symbolId, t_DataBlock *dataBlock);

// Session messages
int Process_BATS_BOE_Login(int oecType);
int Process_BATS_BOE_MessageRecovery(int oecType);
int BuildAndSend_BATS_BOE_LoginRequestMsg(int oecType);
int BuildAndSend_BATS_BOE_LogoutRequestMsg(int oecType);
int BuildAndSend_BATS_BOE_Client_HeartbeatMsg(int oecType);
void ProcessBATS_BOE_LogOutdMsg(int oecType, t_DataBlock *dataBlock);

// Application messages
int BuildAndSend_BATS_BOE_OrderMsg(int oecType, t_OrderPlacementInfo *newOrder);

int ProcessBATS_BOE_OrderAckMsg(int oecType, uint32_t symbolId, t_DataBlock *dataBlock);
int ProcessBATS_BOE_OrderExecutionMsg(int oecType, uint32_t symbolId, t_DataBlock *dataBlock);
int ProcessBATS_BOE_OrderRejectedMsg(int oecType, uint32_t symbolId, t_DataBlock *dataBlock);
int ProcessBATS_BOE_OrderCanceledMsg(int oecType, uint32_t symbolId, t_DataBlock *dataBlock);
int ProcessBATS_BOE_CancelRejectedMsg(int oecType, uint32_t symbolId, t_DataBlock *dataBlock);

//additional message
int ProcessBATS_BOE_Trace_Cancel_Or_CorrectMsg(int oecType, uint32_t symbolId, t_DataBlock *dataBlock);
int ProcessBATS_BOE_Order_RestatedMsg(int oecType, uint32_t symbolId, t_DataBlock *dataBlock);

int ProcessBATS_BOE_LoginReponseMsg(int oecType, t_DataBlock *dataBlock);

long Send_BATS_BOE_SequencedMsg(int oecType, char *message, int msgLen);

#endif /* __BATS_BOE_PROC__ */
