/****************************************************************************
** Project name:  Equity Arbitrage Application
** File name:     book_mgmt.h
** Description:   This file contains some declarations for book_mgmt.c

** Author:    Sang Nguyen-Minh
** First created on 26 September 2007
** Last updated on 26 September 2007
****************************************************************************/

#ifndef __BOOK_MGMT_H__
#define __BOOK_MGMT_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include "global_definition.h"
#include "stock_symbol.h"
#include "utility.h"
#include "quote_mgmt.h"
#include "configuration.h"
#include <pthread.h>

#define PRICE_HASHMAP_BUCKET_SIZE 512

#define __FLUSH_ARCA    0x01
#define __FLUSH_BATSZ   0x02
#define __FLUSH_EDGX    0x04
#define __FLUSH_NASDAQ  0x08
#define __FLUSH_NYSE    0x10
#define __FLUSH_EDGA    0x20
#define __FLUSH_NDBX    0x40
#define __FLUSH_BYX     0x80
#define __FLUSH_PSX     0x100
#define __FLUSH_AMEX    0x200

/****************************************************************************
** Data structure definitions
****************************************************************************/
typedef struct t_OrderDetail
{
  int shareVolume;
  unsigned char tradeSession;
  char _padding[3];
  double sharePrice;
  unsigned long refNum;
} t_OrderDetail;

typedef struct t_QuoteNode            // specific node in t_QuoteList, which is an order(quote) from MDC
{
  struct t_QuoteNode *prev;
  struct t_QuoteNode *next;
  struct t_QuoteList *myList;
  t_OrderDetail info;
} t_QuoteNode;

typedef struct t_QuoteList            // this is a price point, a linked list of same price nodes which maintains aggregate volume at the price
{
  t_QuoteNode     *head;
  double          price;
  int             totalVolume;
  
  struct t_QuoteList  *next;
  struct t_QuoteList  *prev;
} t_QuoteList;

typedef struct t_TopOrder
{
  double sharePrice;
  int shareVolume;
  char ecnIndicator;
} t_TopOrder;

typedef struct t_AggregatedOrder
{
  int ecnIndicator;
  int totalShare;
  int shareBest;
  int shareToPlace;
  double priceWorse;
  double priceBest;
  double priceAverage;
} t_AggregatedOrder;

typedef struct t_AggregatedQuotes
{
  int ecnIndicator;
  int shareVolume;
  double sharePrice;
}t_AggregatedQuotes;

typedef struct t_PriceHashingItem   // This is a linkedlist
{
  double price;                     // The key to compare
  t_QuoteList *list;                // List of quotes having same price
  
  struct t_PriceHashingItem *prev;  // The previous item in bucket
  struct t_PriceHashingItem *next;  // The next item in bucket
} t_PriceHashingItem;

typedef struct t_QuoteNeedDelete
{
  int ecnID;
  int side;
  int stockSymbolIndex;
  double priceToDelete;
} t_QuoteNeedDelete;

typedef struct t_QuoteToDelete
{
  int stockSymbolIndex;
  int side;
  double sharePrice;
} t_QuoteToDelete;

typedef struct t_QuoteNodeInfo {
  uint8_t side; 
  t_QuoteNode *node;
} t_QuoteNodeInfo;                   

typedef struct t_OrderMapCacheEntry {
  uint64_t key;       
  uint8_t side;
  t_QuoteNode *node;
  uint16_t count;
} t_OrderMapCacheEntry;

typedef struct t_OrderMapEntry {
  uint64_t key;      
  uint8_t side;
  t_QuoteNode *node;
} t_OrderMapEntry;

typedef struct t_OrderMap {
  uint32_t bucket_bit;
  uint32_t bucket_max;
  uint32_t bucket_mask;
  int32_t count;
  t_OrderMapEntry *extra_slots;
  t_OrderMapCacheEntry *bucket;
} t_OrderMap;


/****************************************************************************
** Global variable declarations
****************************************************************************/
extern t_PriceHashingItem* Price_HashMap[MAX_STOCK_SYMBOL][MAX_ECN_BOOK][MAX_SIDE][PRICE_HASHMAP_BUCKET_SIZE];
extern t_QuoteList *StockBook[MAX_STOCK_SYMBOL][MAX_ECN_BOOK][MAX_SIDE];
extern t_TopOrder BestQuote[MAX_STOCK_SYMBOL][MAX_SIDE];
extern t_QuoteNode *DeletedQuotePlaceholder;

extern t_OrderMap *ARCA_OrderMap[DBLFILTER_NUM_QUEUES];
extern t_OrderMap *NASDAQ_OrderMap[DBLFILTER_NUM_QUEUES];
extern t_OrderMap *BATSZ_OrderMap[DBLFILTER_NUM_QUEUES];
extern t_OrderMap *BYX_OrderMap[DBLFILTER_NUM_QUEUES];
extern t_OrderMap *EDGX_OrderMap[DBLFILTER_NUM_QUEUES];
extern t_OrderMap *EDGA_OrderMap[DBLFILTER_NUM_QUEUES];
extern t_OrderMap *NDBX_OrderMap[DBLFILTER_NUM_QUEUES];
extern t_OrderMap *PSX_OrderMap[DBLFILTER_NUM_QUEUES];
extern t_OrderMap *OrderMaps[MAX_ECN_BOOK][DBLFILTER_NUM_QUEUES];

/****************************************************************************
** Function declarations
****************************************************************************/
void InitBookMgmtDataStructure(void);
void InitOrderMaps(void);
void DestroyQuoteList(t_QuoteList *list);
void DestroyAllLinkedListDataStructure(void);

unsigned short ProcessMarketDataMessage(const unsigned char venue_id, const uint32_t symbolId, const uint64_t arrival_time, const uint32_t time_status_seconds, const unsigned char *buffer_ptr, const int buffer_readable_bytes, const int consumerId);

t_QuoteNode *InsertQuote(const int ecnId, const int side, const int stockSymbolIndex, const t_OrderDetail *info);
int ModifyQuote(t_QuoteNode *node, const int newShareVolume, const int side, const int stockSymbolIndex);
int DeleteBetterPrice(const double price, const int ecnBookID, const int side, const int stockSymbolIndex);
int DeleteFromPrice(const double price, const int ecnBookID, const int side, const int stockSymbolIndex);
int DeleteQuote(t_QuoteNode *node, const int ecnId, const int side, const int stockSymbolIndex);
int DeleteNysePriceLevel(const unsigned char ecnIndex, const double price, const int side, const int stockSymbolIndex);
int InsertOrUpdateNysePriceLevel(const unsigned char ecnIndex, const t_OrderDetail *info, const int side, const int stockSymbolIndex);

void UpdateMaxBid(int stockSymbolIndex);
void UpdateMinAsk(int stockSymbolIndex);

void GetTopQuotes(t_TopOrder topOrderArray[MAX_ECN_BOOK], int side, int stockSymbolIndex);
t_TopOrder *GetBestQuote(t_TopOrder topOrderArray[MAX_ECN_BOOK], int side);
void GetBestOrder(t_TopOrder *bestOrder, int side, int stockSymbolIndex);
int GetBestServerRoleOrder(t_TopOrder *bestOrder, int side, int stockSymbolIndex, int serverRole);

void DeleteAllPricePointsFromHashSlot (t_PriceHashingItem *node);

int DeleteAllQuoteOnASide(int ecnBookID, int stockSymbolIndex, int side);
int DeleteAllExpiredArcaOrders(const int stockSymbolIndex, const unsigned char validSession, const int sideToDelete);

int GetNumberOfSymbolsWithQuotes(int ecnBookID);
int AggregateTotalSharesAtEntry(t_OrderDetail *newOrder, t_AggregatedOrder quotes[MAX_ECN_BOOK], int side, int stockSymbolIndex, int ecnLaunchID, int *bestIndex);
int GetBBOSnapShot(int stockSymbolIndex, t_BBOSnapShot BBOSnapShot[MAX_EXCHANGE]);
int GetBBOSnapShotAtEntry(t_OrderDetail *newOrder, int newOrderSide, t_AggregatedOrder quotes[MAX_ECN_BOOK], int stockSymbolIndex, int ecnLaunchID, t_BBOSnapShot BBOSnapShot[MAX_EXCHANGE]);

int InitializeMarketDataComponents();
void UpdateDBLFilterForNewVenueAddress(const int venue_id);
int ConnectToVenue(int venueId);
int DisconnectFromVenue(int venueId, int excludedConsumerId);
void AddNewFlush(const short venueBit, const int excludedConsumerId);
void FlushVenueSymbol(const int bookId, const int stockSymbolIndex);
void FlushVenueSymbolsOnQueue(const int bookId, const int consumerId);
void CheckBookCrossToEnableSymbol(const int stockSymbolIndex);

uint32_t OrderMapSize(uint32_t);
void OrderMapInit(t_OrderMap*, uint32_t);
void OrderMapInsert(t_OrderMap* hash, uint64_t key, t_QuoteNode *node, uint8_t side);
t_QuoteNodeInfo OrderMapRemove(t_OrderMap*, uint64_t);
t_QuoteNodeInfo OrderMapFind(t_OrderMap* hash, uint64_t key);

void *StartMarketDataLogConsumerThread(void *args);
void *StartMarketDataReadThread(void *args);
int FinalizeDBLFilter();

#endif
