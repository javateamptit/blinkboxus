/****************************************************************************
** Project name:	Equity Arbitrage Application
** File name: 		edge_book_proc.h
** Description: 	N/A
** Author: 			Dung Tran-Dinh
** First created:	------------------
** Last updated:	------------------
****************************************************************************/
#ifndef EDGE_BOOK_PROC_H_
#define EDGE_BOOK_PROC_H_

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include "configuration.h"
#include "book_mgmt.h"

#define EDGE_SYMBOL_REGULAR_TRADING 'T'
// -----------------------------------------
// Message types - Administrative messages
// -----------------------------------------
#define EDGE_BOOK_MSG_LOGIN_REQUEST 		0x01
#define EDGE_BOOK_MSG_LOGIN_RESPONSE 		0x02
#define EDGE_BOOK_MSG_RETRANMISSION_REQUEST	0x03
#define EDGE_BOOK_MSG_REPLAY_RESPONSE		0x04
#define EDGE_BOOK_MSG_LOGOUT_REQUEST		0x05

// -----------------------------------------
// Message types - Application messages
// -----------------------------------------
#define EDGE_BOOK_MSG_TIME							0x20
#define EDGE_BOOK_MSG_ADD_ORDER_LONG_FORM			0x21
#define EDGE_BOOK_MSG_ADD_ORDER_SHORT_FORM			0x22
#define EDGE_BOOK_MSG_ADD_ORDER_EXTENDED_FORM		0x2F
#define EDGE_BOOK_MSG_ADD_ORDER_WITH_ATTRIBUTION	0x34

#define EDGE_BOOK_MSG_CANCELED_ORDER				0x29
#define EDGE_BOOK_MSG_EXECUTED_ORDER				0x23
#define EDGE_BOOK_MSG_EXECUTED_AT_ORDER				0x24
#define EDGE_BOOK_MSG_SECURITY_STATUS				0x2E
#define EDGE_BOOK_MSG_MODIFIED_ORDER_LONG_FORM		0x27
#define EDGE_BOOK_MSG_MODIFIED_ORDER_SHORT_FORM		0x28
#define EDGE_BOOK_MSG_TRADE_LONG_FORM				0x2A
#define EDGE_BOOK_MSG_TRADE_SHORT_FORM				0x2B
#define EDGE_BOOK_MSG_TRADE_EXTENDED_FORM			0x30
#define EDGE_BOOK_MSG_TRADE_BREAK					0x2C
#define EDGE_BOOK_MSG_END_OF_SESSION				0x2D

// -----------------------------------------
// Unit header
// -----------------------------------------
#define EDGE_BOOK_UNIT_HEADER_LEN 				8
#define EDGE_BOOK_HEADER_LENGTH_INDEX 			0
#define EDGE_BOOK_HEADER_MSG_COUNT_INDEX 		2
#define EDGE_BOOK_HEADER_MARKET_DATA_GROUP_INDEX 3
#define EDGE_BOOK_HEADER_SEQUENCE_INDEX			4

#define EDGE_BOOK_LEN_OF_LOGIN_REQUEST_MSG		22
#define EDGE_BOOK_LEN_OF_RETRANSMISSION_REQUEST_MSG		9
#define EDGE_BOOK_LEN_OF_LOGOUT_REQUEST_MSG		2
#define EDGE_BOOK_MARKET_ORDER_FLAG 			0x10

/****************************************************************************
** Data structure definitions
****************************************************************************/

/****************************************************************************
** Global variable declarations
****************************************************************************/
extern t_EDGE_Book_Conf EDGX_BOOK_Conf;
extern t_EDGE_Book_Conf EDGA_BOOK_Conf;

/****************************************************************************
** Function declarations
****************************************************************************/
int Init_DataStructure_EDGX_BOOK(void);
int Init_DataStructure_EDGA_BOOK(void);

int ProcessAddOrderMessage_EDGE_BOOK(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime, unsigned int timeStatusSeconds);

int ProcessOrderExecutedMessage_EDGE_BOOK(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime, unsigned int timeStatusSeconds);

int ProcessOrderExecutedAtMessage_EDGE_BOOK(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime, unsigned int timeStatusSeconds);

int ProcessCanceledOrderMessage_EDGE_BOOK(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime, unsigned int timeStatusSeconds);

int ProcessModifyOrderMessage_EDGE_BOOK(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime, unsigned int timeStatusSeconds);

int CheckStaleData_EDGE(unsigned char ecn_book_id, char *buffer, int stockSymbolIndex, unsigned long arrivalTime, unsigned int timeStatusSeconds);

int ProcessSymbolStatusMessage_EDGE_BOOK(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime);
int ProcessTradeLongMessage_EDGE_BOOK(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime);
int ProcessTradeShortMessage_EDGE_BOOK(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime);
int ProcessTradeExtendedMessage_EDGE_BOOK(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime);
int EDGE_ProcessBookMessage(unsigned char ecn_book_id, uint32_t symbolId, char *msg, unsigned long arrivalTime, unsigned int timeStatusSeconds);
/***************************************************************************/

#endif /* EDGE_BOOK_PROC_H_ */
