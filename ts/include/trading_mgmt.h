/****************************************************************************
** Project name:  Equity Arbitrage Application
** File name:     trading_mgmt.h
** Description:   This file contains some declarations for trading_mgmt.c

** Author:    Sang Nguyen-Minh
** First created on 01 October 2007
** Last updated on 01 October 2007
****************************************************************************/

#ifndef __TRADING_MGMT_H__
#define __TRADING_MGMT_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include "global_definition.h"
#include "configuration.h"
#include "stock_symbol.h"
#include "book_mgmt.h"
#include "quote_mgmt.h"
#include "ouch_oec_proc.h"

#define MAX_OPEN_ORDERS 1000

#define REASON_CROSS_ENTRY           0
#define REASON_CROSS_EXIT            1
#define REASON_CROSS_DD_ENTRY        2

#define MAX_LOG_TYPE                16

#define MAX_LEN_PER_LOG_ENTRY       64

#define MAX_LOG_ENTRY          2000000

#define MAX_SIDE_VALUE_TYPE          3

#define LONG      0
#define SHORT     1

#define MAX_BID   0
#define MIN_ASK   1

#define PROFIT    0
#define LOSS      1

#define BUY       0
#define BOUGHT    1
#define SELL      2
#define SOLD      3
#define CANCELED  4

#define MAX_CROSS_ID 100000

#define NORMALLY_TRADE      0
#define SIMULTANEOUS_TRADE  1

#define MAX_BBO_ENTRY 200000

/****************************************************************************
** Data structure definitions
****************************************************************************/
typedef struct t_OpenOrder
{
  int orderID;
  int shareVolume;      //This is the remaining volume of the order
  double sharePrice;
  int side;    
  int tradeAction;
  int stockSymbolIndex;  
  int reason;  
  char isISO;
  char isRoutable;
  char coalesceFills;
} t_OpenOrder;

typedef struct t_TradingInfo
{
  double filledEntryPrice;
  double totalBought;
  double totalSold;
  
  int crossID;
  int isTrading;
  int side;
  int lockedSide;
  int entryCounter;
  int exitCounter;
  int filledEntryShareVolume;
  int longShareVolume;
  int shortShareVolume;
  int exitSideHoldingShareVolume;
  
  unsigned short entryExitRefNum;
  char routedLiquidateAttempted;
  
  t_OpenOrder openOrders[MAX_OPEN_ORDERS];
} t_TradingInfo;

typedef struct t_TradingStatus
{
  int *status[MAX_ECN_ORDER];
} t_TradingStatus;


/*These structures are used for order 
status management at Entry and Exit
*/
typedef struct t_ExecutionReport
{
  unsigned char ecnID;
  unsigned short entryExitRefNum;
  int stockSymbolIndex;
  
  int clientOrderID;
  int canceledShare;
  
  int filledShare;
  int acceptedShare;
  
  double acceptedPrice;
  double filledPrice;
  long time;
} t_ExecutionReport;

typedef struct t_TradeOutputLogMgmt
{
  char tradeOutputLogCollection[MAX_LOG_ENTRY][MAX_LEN_PER_LOG_ENTRY];
  int countLogEntry;
  int countSentLogEntry;
  int countSavedLogEntry;
  FILE *fileDesc;
} t_TradeOutputLogMgmt;

typedef struct t_BBOQuoteItem
{
  int crossID;
  unsigned short bboId;
  t_BBOSnapShot BBOSnapShot[MAX_EXCHANGE];
} t_BBOQuoteItem;

typedef struct t_BBOInfoMgmt
{
  t_BBOQuoteItem BBOInfoCollection[MAX_BBO_ENTRY];
  int countBBOEntry;
  int countSentBBOEntry;
  int countSavedBBOEntry;
  FILE *fileDesc;
} t_BBOInfoMgmt;

typedef struct t_TradingSessionMgmt
{
  short RegularSessionBeginHour;
  short RegularSessionBeginMin;
  short RegularSessionEndHour;
  short RegularSessionEndMin;
  int CurrentSessionType;
} t_TradingSessionMgmt;

/****************************************************************************
** Global variable declarations
****************************************************************************/
extern t_TradingInfo TradingCollection[MAX_STOCK_SYMBOL];

extern t_TradingStatus TradingStatus;

extern t_TradeOutputLogMgmt TradeOutputLogMgmt;

extern int CurrentCrossID;

extern int CurrentOrderID;

extern t_BBOInfoMgmt BBOInfoMgmt;

extern t_TradingSessionMgmt TradingSessionMgmt;

extern int OrderToBookMapping[MAX_ECN_ORDER];
/****************************************************************************
** Function declarations
****************************************************************************/
void *Thread_SaveTradingInformation(void *threadArgs);

int InitTradingDataStructures(void);
void InitTradeOutputLogMapTable(void);
int InitTradeOutputLogMgmt(void);
                                                   
int ProcessAcceptResponse(t_ExecutionReport *status);
int ProcessRejectResponse(t_ExecutionReport *status);
int ProcessCancelResponse(t_ExecutionReport *status);
int ProcessFillResponse(t_ExecutionReport *status);

int DetectCrossTrade(t_OrderDetail *newOrder, int side, int stockSymbolIndex, int ecnOrderID, long timeStamp, int reason);
int CrossTradeMgmt(t_OrderDetail *newOrder, int newOrderSide, int stockSymbolIndex, int ecnLaunchID, int crossID, int bestQuoteIndicator);

int EnterCrossTrade(t_OrderDetail *newOrder, t_AggregatedOrder *bestQuotes, int bestIndex, int totalSharesAggregated, int newOrderSide, int ecnLaunchID, int stockSymbolIndex, int crossID);
int CompleteCrossTrade(const int stockSymbolIndex, t_ExecutionReport *status);

double ComputeWorstLiquidatePrice(t_TradingInfo *tradingInfo);
int Liquidate(int symbolIndex, t_TradingInfo *tradingInfo, t_ExecutionReport *status);   
int RoutedLiquidate(int symbolIndex, t_TradingInfo *tradingInfo, t_ExecutionReport *status);       

int LoadTradeOutputLogFromFile(void);
int SaveTradeOutputLogToFile();

int GetNewCrossID(void);
int GetNewOrderID(void);
double GetMaxSpread(double sharePrice, int stockSymbolIndex);

char Tradable(int symbolIndex, int mdcId, int side);
void ShortMark(t_OrderPlacementInfo *order, int stockSymbolIndex);
int RiskApprove(t_OrderPlacementInfo *order, int stockSymbolIndex);
void ReturnBuyingPower(double amount);
inline char FlightContainsOrderForVenue(t_OrderPlacement *orders, int ecnId);

int PlaceISOFlight(int stockSymbolIndex, int crossID, int bboId, int reason, t_OrderPlacement *orders, int directedShareAllocation, t_BBOSnapShot BBOSnapShot[MAX_EXCHANGE], char routeOnFailure, double routePrice);  
int PlaceOrder(t_OrderPlacementInfo *order, int stockSymbolIndex, int reason);

void UpdateCurrentMarketTime(void);

int InitBBOInfoMgmt(void);
int LoadBBOInfoFromFile(void);
int AddBBOInfoToFile(int crossId, t_BBOSnapShot BBOSnapShot[MAX_EXCHANGE], int bboId);
int SaveBBOInfoToFile(void);
void UpdateCurrentSessionType(void);

void AddPlacementInfoForOrder(t_OrderPlacementInfo *newOrder);
void AddPlacementInfoForOrders(t_OrderPlacement *orders);

int GetLatestCrossIDFromOutputLog(int *crossID);

void AddTradeOutputLogEntry_04080911(const uint64_t time, const int logType, int const crossID);
void AddTradeOutputLogEntry_02(const char *symbol, const uint64_t time, const long launchTime, const int crossID, const char ecnLaunchID, const char newOrderSide, const char ecnIndicator, const unsigned char reason);
void AddTradeOutputLogEntry_03(const uint64_t time, const double sharePrice, const int crossID, const int shareVolume, const char side, const char venueId);
void AddTradeOutputLogEntry_05(const uint64_t time, const char *symbol, const double sharePrice, const int crossID, const int shareVolume, const int venueId);
void AddTradeOutputLogEntry_12(const uint64_t time, const int crossID, const char ecnID);
void AddTradeOutputLogEntry_10(const uint64_t time, const char *symbol, const double sharePrice, const int crossID, const int shareVolume, const char action, const char ecnOrderToPlaceID);
void AddTradeOutputLogEntry_13(const uint64_t time, const char *symbol, const double totalBought, const double totalSold, const int crossID, const int longShareVolume, const int shortShareVolume);
void AddTradeOutputLogEntry_14(const uint64_t time, const char *symbol, const double profitGained, const int crossID, const char profitLoss);
void AddTradeOutputLogEntry_15(const uint64_t time, const char *symbol, const double sharePrice, const int crossID, const int numStuck, const int stuckShareVolume, const char askBidIndicator, const char stuckType);

int ResetStatusForHungOrder(const int clientOrderID, const char *stockSymbol);

#endif
