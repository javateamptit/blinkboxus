/****************************************************************************
** Project name:  Equity Arbitrage Application
** File name:     aggregation_server.h
** Description:   This file contains some declarations for aggregation_server.c

** Author:    Sang Nguyen-Minh
** First created on 14 September 2007
** Last updated on 14 September 2007
****************************************************************************/

#ifndef __AGGREGATION_SERVER_PROC_H__
#define __AGGREGATION_SERVER_PROC_H__

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#include "global_definition.h"
#include "order_mgmt.h"
#include "utility.h"

#define MAX_RECEIVE_MSG_LEN     1024 * 1024
#define MSG_HEADER_LEN 6

#define MAX_LOG_ENTRY_CAN_BE_SENT 1000
#define LEN_OF_LOG_ENTRY            80
#define LOG_ENTRY_INDEX              9

// Message body length index
#define MSG_BODY_LEN_INDEX  2

// Receive timeout in seconds for aggregation server connections
#define AS_RECV_TIMEOUT    15

// Pending for some information from another
#define PENDING 1

// Maximum length of message that can be sent
#define MAX_RESPONSE_MSG_LEN 1024 * 512

// AS request message Types
#define BEGIN_AS_REQUEST_MSG_TYPE           30001
#define END_AS_REQUEST_MSG_TYPE             45000

#define BEGIN_AS_REQUEST_CONTROLS_MSG_TYPE  30001
#define END_AS_REQUEST_CONTROLS_MSG_TYPE    31000

#define BEGIN_AS_REQUEST_GETS_MSG_TYPE      31001
#define END_AS_REQUEST_GETS_MSG_TYPE        32000

#define BEGIN_AS_REQUEST_SETS_MSG_TYPE      32001
#define END_AS_REQUEST_SETS_MSG_TYPE        33000

#define AS_REQ_CONNECT_BOOK                 30001
#define AS_REQ_DISCONNECT_BOOK              30003
#define AS_REQ_FLUSH_BOOK_OF_ALL_SYMBOLS    32005
#define AS_REQ_FLUSH_BOOK_OF_A_SYMBOL       32004

#define AS_REQ_CONNECT_ORDER                30005
#define AS_REQ_DISCONNECT_ORDER             30007
#define AS_REQ_ENABLE_TRADING               30009
#define AS_REQ_DISABLE_TRADING              30011

#define AS_REQ_GET_RAWDATA                  31006
#define AS_REQ_GET_TRADE_OUTPUT_LOG         31007

#define AS_REQ_ARCA_DIRECT_CANCEL_REQUEST   31008

#define AS_REQ_GET_BBO_INFO                 31009

#define AS_REQ_SET_BOOK_CONFIG              32001
#define AS_REQ_SET_ORDER_CONFIG             32002
#define AS_REQ_SET_GLOBAL_CONFIG            32003
#define AS_REQ_RESET_CURRENT_STATUS         32006
#define AS_REQ_UPDATE_CURRENT_STATUS        32007
#define AS_REQ_UPDATE_DISABLE_SYMBOL_LIST   32008
#define AS_REQ_UPDATE_DISABLE_SYMBOL        32009
#define AS_REQ_HALTED_SYMBOL_LIST           32010
#define AS_REQ_A_HALTED_SYMBOL              32011
#define AS_REQ_UPDATE_DISABLE_SYMBOL_RANGES_LIST 32012
#define AS_REQ_UPDATE_DISABLE_SYMBOL_RANGES 32013
#define AS_REQ_SET_ARCA_BOOK_FEED           32014
#define AS_REQ_SET_SYMBOL_STUCK_STATUS      32015
#define AS_REQ_SET_STUCK_SYMBOL_LIST        32016
#define AS_REQ_SET_SYMBOL_LOCK_COUNTER      32018
#define AS_REQ_SET_MIN_SPREAD_CONF          32019
#define AS_REQ_SET_VOLATILE_SYMBOL_LIST     32020
#define AS_REQ_SET_ALL_VOLATILE_TOGGLE_VALUE 32021
#define AS_REQ_SET_ETB_SYMBOL_LIST          32022
#define AS_REQ_SET_TRADING_ACCOUNT_LIST     32030
#define AS_REQ_GET_STOCK_SYMBOL_STATUS      31001
#define AS_REQ_GET_HEARTBEAT                31020
#define AS_REQ_RESET_HUNG_ORDER 32025

// Price Update Request
#define AS_REQ_GET_PRICE_UPDATE             32024
#define TS_RES_PRICE_UPDATE                 46020

// For AS request to get number of symbols with quotes
#define AS_REQ_GET_NUMBER_OF_SYMBOLS_WITH_QUOTES 31002

// TS response message type
#define TS_RES_BOOK_CONFIG                  46002
#define TS_RES_ORDER_CONFIG                 46003
#define TS_RES_GLOBAL_CONFIG                46004
#define TS_RES_MIN_SPREAD_CONF              46012
#define TS_RES_SPLIT_SYMBOL_CONF            46013

#define TS_RES_CURRENT_STATUS               46005
#define TS_RES_NEW_RAWDATA                  46006
#define TS_RES_GET_STOCK_SYMBOL_STATUS      46001

// For response the number of symbols with quotes to AS
#define TS_RES_NUMBER_OF_SYMBOLS_WITH_QUOTES 46007

#define TS_RES_TRADE_OUTPUT_LOG             46008
#define TS_RES_BOOK_IDLE_WARNING            46009
#define TS_RES_BBO_INFO                     46010
#define NUM_STUCK_INDEX       0
#define BUYING_POWER_INDEX    1
#define NUM_LOSER_INDEX       2

#define ADD_DISABLED_SYMBOL     0
#define REMOVE_DISABLED_SYMBOL  1

#define UPDATE_SOURCE_ID              0
#define UPDATE_RETRANSMISSION_REQUEST 1

// Connect/disconnect to CQS/UQDF
// CQS/UQDF: 1 byte; 0 for CQS, and 1 for UQDF
#define MSG_TYPE_FOR_REQUEST_CONNECT_TO_BBO_FROM_AS     30100
#define MSG_TYPE_FOR_REQUEST_DISCONNECT_TO_BBO_FROM_AS  30101

// Enable/disable ISO algorithm
#define MSG_TYPE_FOR_SET_ISO_TRADING_FROM_AS            30102

// Get/set exchange configuration
// EXCHANGE_CONF
#define MSG_TYPE_FOR_SET_EXCHANGE_CONF_FROM_AS          32100
#define MSG_TYPE_FOR_SEND_EXCHANGE_CONF_TO_AS           46100

// Flush BBO quote for a exchange
// exchange id: 1 byte
#define MSG_TYPE_FOR_REQUEST_FLUSH_BBO_QUOTES_FROM_AS 32101

// Books for ISO Trading
#define MSG_TYPE_FOR_SET_BOOK_ISO_TRADING_FROM_AS   32102
#define MSG_TYPE_FOR_SEND_BOOK_ISO_TRADING_TO_AS    46102

// Get BBO quotes
// symbol: 8 bytes
#define MSG_TYPE_FOR_GET_BBO_QUOTE_LIST_FROM_AS     31100
#define MSG_TYPE_FOR_SEND_BBO_QUOTE_LIST_TO_AS      46101

//For alerting: these messages are sent actively to AS
#define TS_RES_SERVICE_DISCONNECT     46015 
#define TS_RES_TRADING_DISABLE        46016
#define TS_RES_STALE_MARKET_DATA      46017
#define TS_RES_UNHANDLED_DATA         46018
#define TS_RES_SYMBOL_HALT_STATUS     46019

//Service index
#define SERVICE_ARCA_BOOK     10000
#define SERVICE_NASDAQ_BOOK   10001
#define SERVICE_NYSE_BOOK     10002
#define SERVICE_BATSZ_BOOK    10003
#define SERVICE_EDGX_BOOK     10004
#define SERVICE_EDGA_BOOK     10005
#define SERVICE_NDBX_BOOK     10006
#define SERVICE_BYX_BOOK      10007
#define SERVICE_PSX_BOOK      10008
#define SERVICE_AMEX_BOOK     10009

#define SERVICE_CQS_FEED      11000
#define SERVICE_UQDF_FEED     11001

#define SERVICE_ALL_OECs         -1
#define SERVICE_ARCA_DIRECT   20000
#define SERVICE_NASDAQ_OUCH   20001
#define SERVICE_NYSE_CCG      20002
#define SERVICE_NASDAQ_RASH   20003
#define SERVICE_BATSZ_BOE     20004
#define SERVICE_EDGX_ORDER    20005
#define SERVICE_EDGA_ORDER    20006
#define SERVICE_NASDAQ_BX     20007
#define SERVICE_BYX_BOE       20008
#define SERVICE_NASDAQ_PSX    20009

//Trading disabled reason code
#define TRADING_DISABLED_NO_ACTIVE_OR_ACTIVE_ASSIST_TRADER    -12
#define TRADING_DISABLED_AS_SCHEDULE                          -11
#define TRADING_DISABLED_MAX_CROSS_LIMIT                      -10
#define TRADING_DISABLED_WEDBUSH_REQUEST                       -9
#define TRADING_DISABLED_NO_TT_CONNECTION                      -8
#define TRADING_DISABLED_TWT_REQUEST                           -7
#define TRADING_DISABLED_BOOK_DISCONNECT                       -6
#define TRADING_DISABLED_ORDER_DISCONNECT                      -5
#define TRADING_DISABLED_EXCEED_MAX_STUCK_LIMIT                -4
#define TRADING_DISABLED_EXCEED_MAX_LOSS_LIMIT                 -3
#define TRADING_DISABLED_EXCEED_MAX_STUCK_LIMIT_BY_AS_REQUEST  -2
#define TRADING_DISABLED_EXCEED_MAX_LOSS_LIMIT_BY_AS_REQUEST   -1

#define MAX_IGNORE_SYMBOL 10000
#define SYMBOL_LEN 8

// For reload configuration type
#define GLOBAL_CONFIGURATION      1
#define ALGORITHM_CONFIGURATION   2
#define MDC_CONFIGURATION         3
#define OEC_CONFIGURATION         4
#define MIN_SPREAD_CONFIGURATION  5
#define SHORTABILITY_SCHEDULE     6
/****************************************************************************
** Data structure definitions
****************************************************************************/
// This structure is used to contain ignore symbol ranges list
typedef struct t_IgnoreStockRangesList
{
  int countStock;
  char stockList[MAX_IGNORE_SYMBOL][SYMBOL_LEN];
} t_IgnoreStockRangesList;

/*
The structure is used to contain 
a message from Aggregation Server
*/
typedef struct t_Message
{
  int msgLen;
  char msgContent[MAX_RECEIVE_MSG_LEN];
}t_Message;

/*
The structure is used to contain 
configuration for aggregation server thread
*/
typedef struct t_ASConfig
{
  int port;
  int maxConnections;
  
  int socket;
  pthread_mutex_t sendingLock;
}t_ASConfig;

typedef struct t_CurrentStatus
{
  long buyingPower; // in cents
  int numStuck;
  int numLoser;

  char status_ARCA_Book;
  char status_NASDAQ_Book;
  char status_NYSE_Book;
  char status_BATSZ_Book;
  char status_EDGX_Book;
  char status_EDGA_Book;
  char status_NDBX_Book;
  char status_BYX_Book;
  char status_PSX_Book;
  char status_AMEX_Book;

  char status_ARCA_DIRECT_Order;
  char status_NASDAQ_OUCH_Order;
  char status_NYSE_CCG_Order;
  char status_NASDAQ_RASH_Order;
  char status_BATSZ_BOE_Order;
  char status_EDGX_Order;
  char status_EDGA_Order;
  char status_NASDAQ_BX_Order;
  char status_BYX_BOE_Order;
  char status_PSX_Order;

  char status_ARCA_DIRECT_Trading;
  char status_NASDAQ_OUCH_Trading;
  char status_NYSE_CCG_Trading;
  char status_NASDAQ_RASH_Trading;
  char status_BATSZ_BOE_Trading;
  char status_EDGX_Trading;
  char status_EDGA_Trading;
  char status_NASDAQ_BX_Trading;
  char status_BYX_BOE_Trading;
  char status_PSX_Trading;

  char statusCQS;
  char statusUQDF;
  char statusISOTrading;
} t_CurrentStatus;

/****************************************************************************
** Global variable definition
****************************************************************************/
extern t_ASConfig ASConfig;
extern t_CurrentStatus TSCurrentStatus;
extern int IsUpdateBuyingPower;
extern double ReceivedBuyingPower;
extern t_IgnoreStockRangesList IgnoreStockRangesList;
extern int WillDisableTrading[MAX_ECN_ORDER];
/****************************************************************************
** Function declarations
****************************************************************************/
int InitASDataStructure(void);

void *Thread_WaitForASConnection(void *threadArg);

int TalkToAggregationServer(void *ASConfig);

int ReceiveMessage(void *ASConfig, void *message);
int ProcessControlMessageTypes(int msgType, char *message);
int ProcessGetMessageTypes(int msgType, char *message);
int ProcessSetMessageTypes(int msgType, char *message);

int SendMessageToAS(void *message, int len);

int BuildGeneralMessages(void *message);

int ParseMessage(void *message);
int ParseGetTradingInformation(void *message);
int ParseGetStockSymbolStatus(char *message);
int ParseGetPriceUpdateForSymbolList(char *message);
int ParseGetTradeOutputLog(void *message);

int ProcessConnectBookMessage(char *message);
int ProcessDisconnectBookMessage(char *message);

int ProcessConnectOrderMessage(char *message);
int ProcessDisconnectOrderMessage(char *message);

int ProcessEnableTradingMessage(char *message);
int ProcessDisableTradingMessage(char *message);

void *SendDataToASMgmt(void *threadArgs);

int SendCurrentStatusToAS(void);

int SendECNDataBlockToAS(const void *configInfo, void *orderRawDataMgmt);
int SendDataBlockFromFileToAS(const void *configInfo, void *orderRawDataMgmt);
int SendDataBlockFromCollectionToAS(const void *configInfo, void *orderRawDataMgmt);

int SendBookConfiguration(void);

int SendOrderConfiguration(void);
int BuildSend_ARCA_DIRECT_Configuration(void);
int BuildSend_NYSE_CCG_Configuration(void);
int BuildSend_NASDAQ_OUCH_Configuration(void);
int BuildSend_NASDAQ_RASH_Configuration(void);
int BuildSend_BATSZ_BOE_Configuration(void);
int BuildSend_BYX_BOE_Configuration(void);
int BuildSend_BATS_FIX_Configuration(void);
int BuildSend_EDGX_Configuration(void);
int BuildSend_EDGA_Configuration(void);
int BuildSend_NASDAQ_BX_Configuration(int oecType);

int BuildSend_Book_IdleWarning(int, int);
int BuildSendUnhandledDataOnGroup(int serviceIndex);

int BuildSend_BATSZ_Book_Configuration(void);
int BuildSend_BYX_Book_Configuration(void);
int BuildSend_ARCA_Book_Configuration(void);
int BuildSend_NASDAQ_MDC_Configuration(int ecnIndex, void *config);
int BuildSend_NYSE_Book_Configuration(void);
int BuildSend_EDGX_Book_Configuration(void);
int BuildSend_EDGA_Book_Configuration(void);

int SendStockSymbolStatusToAS(int TT_ID, char *stockSymbol, char *listMdc);
int SendPriceUpdateResponseToAS(int maxSymbol, char symbolList[][SYMBOL_LEN]);

int BuildSend_Global_Configuration(void);
int BuildSend_MinSpread_Configuration(void);

int ProcessSet_ARCA_DIRECT_Config(char *message);
int ProcessSet_NYSE_CCG_Config(char *message);
int ProcessSet_NASDAQ_OUCH_Config(char *message);
int ProcessSet_NASDAQ_RASH_Config(char *message);
int ProcessSet_BATSZ_BOE_Config(char *message);
int ProcessSet_BATS_FIX_Config(char *message);
int ProcessSet_EDGX_Config(char *message);
int ProcessSet_EDGA_Config(char *message);
//int ProcessSet_NASDAQ_BX_Config(int oecType, char *message);

int ProcessSetBookMessage(char *message);
int ProcessSetOrderMessage(char *message);
int ProcessSetGlobalConfiguration(char *message);
int ProcessSet_ARCA_Book_Configuration(char *message);
int ProcessSet_BATSZ_Book_Configuration();
int ProcessSet_NASDAQ_Book_Configuration(char *message);
int ProcessSet_NDBX_Book_Configuration(char *message);
int ProcessSet_NYSE_Book_Configuration(char *message);
int ProcessSet_EDGX_Book_Configuration();
int ProcessSet_EDGA_Book_Configuration();

int ProcessSetMinSpreadConf(char *message);
int ProcessSetCurrentStatus(char *message);
int ProcessSet_UpdateStatus(char *message);
int ProcessSet_UpdateDisableSymbolList(char *message);
int ProcessSet_UpdateDisableSymbol(char *message);
int ProcessSet_UpdateDisableSymbolRangesList(char *message);
int ProcessSet_UpdateDisableSymbolRanges(char *message);

int ProcessFlushBookOfAllSymbols(char *message);
int ProcessFlushBookOfASymbol(char *message);

int SendTradeOutputLogToAS();

int ProcessSet_HaltedSymbolList(char *message);
int ProcessSet_AHaltedSymbol(char *message);

int ProcessConnectCQSnUQDFMessage(char *message);
int ProcessDisconnectCQSnUQDFMessage(char *message);
int ParseGetBBOQuotes(char *message);
int BuilSendExchangeConfigToAS(void);
int ProcessSetExchangeConf(char *message);
int ProcessFlushBBOQuotes(char *message);
int SendBBOQuotesToAS(int TT_ID, char *stockSymbol);
int ProcessSetISO_TradingMessage(char *message);

int BuilSendBook_ISO_TradingToAS(void);
int ProcessSetBook_ISO_Trading(char *message);
int SendBBOInfoToAS();
int ParseGetBBOInfo(void *message);
int InitializeIgnoreStockRangesList(void);

// For get the number of symbols with quotes
int ParseGetNumberOfSymbolsWithQuotes(char *message);
int SendNumberOfSymbolsWithQuotesToAS(int ttIndex);

// For TS default global conf
int ProcessSetArcaFeedMessage(char *message);
int ProcessSetStuckUnlockForASymbol(char *message);
int ProcessSetStuckForSymbolList(char *message);
int ProcessSetSymbolLockCounter(char *message);
int ProcessSetVolatileSymbolList(char *message);
int ProcessSetAllVolatileToggle(char *message);
int ProcessSetEtbSymbolList(char *message);
int ProcessTradingAccountList(char *message);

int BuildSendServiceDisconnect(short serviceIndex);
int BuildSendTradingDisabled(short serviceIndex, short reasonCode);
int SendHaltStockListToAS(void);
int BuildSendStaleMarketNotification(int ecnIndex, int stockSymbolIndex, long delay);

int BuildSend_SplitSymbol_Configuration(void);

int ProcessHungOrderReset(char *message);
#endif
