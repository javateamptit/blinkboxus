/****************************************************************************
** Project name:  Equity Arbitrage Application
** File name:     nyse_ccg_proc.h
** Description:   This file contains some declarations for nyse_ccg_proc.c

** Author:    Khoa Pham-Tan
** First created on 12 August 2012
** Last updated  on 12 August 2012
****************************************************************************/

#ifndef __NYSE_CCG_PROC__
#define __NYSE_CCG_PROC__

#include "global_definition.h"
#include "configuration.h"
#include "stock_symbol.h"


/*define Message types of NYSE using UTP direct*/
// for session message
#define NYSE_HEART_BEAT_TYPE        0x0001
#define NYSE_TEST_REQUEST_TYPE      0x0011
#define NYSE_LOGON_TYPE             0x0021
#define NYSE_LOGON_REJECT_TYPE      0x0141

// for application message
#define NYSE_NEW_ORDER_TYPE         0x0041
#define NYSE_ORDER_FILLED_TYPE      0x0081
#define NYSE_ORDER_ACK_TYPE         0x0091
#define NYSE_UROUT_TYPE             0x00D1 // Order Killed Message
#define NYSE_ORDER_REJECT_TYPE      0x00F1
#define NYSE_REPLACED_TYPE          0x00E1
#define NYSE_BUST_CORRECT_TYPE      0x0101


/*define length of message types*/
// for session message
#define NYSE_LOGON_MSG_LEN          60
#define NYSE_LOGON_REJECT_MSG_LEN   60
#define NYSE_TEST_REQUET_MSG_LEN     8
#define NYSE_HEART_BEAT_MSG_LEN      8

// for application message
#define NYSE_NEW_ORDER_MSG_LEN         84

#define NYSE_ORDER_ACK_MSG_LEN         56
#define NYSE_UROUT_MSG_LEN             84 // Order Killed Message
#define NYSE_ORDER_FILL_MSG_LEN       116
#define NYSE_ORDER_REPLACED_MSG_LEN    60
#define NYSE_BUST_OR_CORRECT_MSG_LEN   96

#define NYSE_ORDER_CANCEL_MSG_LEN      92
#define NYSE_CANCEL_ORDER_TYPE         0x0061

#define NYSE_CANCEL_REQUEST_ACK_MSG_LEN   56
#define NYSE_CANCEL_REQUEST_ACK_TYPE      0x00A1

#define MASK_4_BYTES    0x000000FF
#define MASK_2_BYTES    0x00FF

#define NYSE_CCG_HEADER_LEN           4
#define NYSE_OUTGOING_SEQ_NUM_INDEX   8

// time out
#define NYSE_CCG_RECV_TIMEOUT         65

#define NYSE_PRICE_SCALE      '2'
#define NYSE_ORDER_TYPE_LIMIT '2'
#define NYSE_RULE80A          'P'
#define DOTRESERVE            'N'
#define EXECINST              'E'

// Time in second to generate heartbeat message
#define NYSE_CCG_HEARTBEAT_IN_SECOND 30

/****************************************************************************
** Data structure definitions
****************************************************************************/

typedef struct t_NYSE_CCG_CancelOrder
{
  char *symbol;
  int originalClientOrderID;
  int clOrdID;
  char side;
  char _padding[7];
} t_NYSE_CCG_CancelOrder;


/****************************************************************************
** Global variable declarations
****************************************************************************/

/****************************************************************************
** Function declarations
****************************************************************************/
void Init_NYSE_CCG_DataStructure();
int Connect_NYSE_CCG();
void Add_NYSE_CCG_DataBlockToCollection(t_DataBlock *dataBlock);

int Process_NYSE_CCG_DataBlock(uint32_t symbolId, t_DataBlock *dataBlock);

// Session messages
int BuildAndSend_NYSE_CCG_LogonRequestMsg(void);
int BuildAndSend_NYSE_CCG_HeartbeatMsg(void);

// Application messages
int BuildAndSend_NYSE_CCG_OrderMsg(t_OrderPlacementInfo *newOrder);

int ProcessNYSE_CCG_OrderAckMsg(uint32_t symbolId, t_DataBlock *dataBlock);
int ProcessNYSE_CCG_OrderFilledMsg(uint32_t symbolId, t_DataBlock *dataBlock);
int ProcessNYSE_CCG_OrderRejectedMsg(uint32_t symbolId, t_DataBlock *dataBlock);
int Process_NYSE_CCG_OrderCancelAckMsg(uint32_t symbolId, t_DataBlock *dataBlock);
int ProcessNYSE_CCG_OrderCanceledMsg(uint32_t symbolId, t_DataBlock *dataBlock);
int ProcessNYSE_CCG_LogonAcceptedMsg(t_DataBlock *dataBlock);

long Send_NYSE_CCG_SequencedMsg(char *message, int msgLen);

void Convert_ClOrdID_To_NYSE_Format(int idOrder, char *strResult);
int Convert_NYSECCG_Format_To_ClOrdID(char *valueFiled);

void *Send_NYSE_CCG_HeartBeat_Thread(void *arg);

#endif // __NYSE_CCG_PROC__
