//
//  dbl_filter_wrapper.h
//  DblFilter
//
//  Created by Lucas McGrew on 8/8/13.
//
//

#ifndef __DblFilter__dbl_filter_wrapper__
#define __DblFilter__dbl_filter_wrapper__

#include <stdint.h>
#include <stddef.h>
#include <netinet/in.h>

#include "dbl.h"

typedef void* sfc_handle;
typedef void* nfc_handle;
typedef void* spsc_read_handle;
typedef void* spsc_write_handle;

typedef void* oesfc_handle;
typedef void* oenfc_handle;
typedef void* oe_device;

#define DBL_MD_HDR_LEN 16
#define DBL_OE_HDR_LEN 12

#ifdef __cplusplus
extern "C" {
#endif
  /*
   Common Methods
   */
  
  void register_symbol(const char* buf, int32_t id);
  void register_venue(const char* buf, int32_t id);

  void register_spsc_queue(void *ctx, uint32_t qidx, int64_t addr, int32_t size, int64_t seqaddr);
  void register_spsc_send_queue(void *ctx, uint32_t qidx, int64_t addr, int32_t size, int64_t seqaddr);

  uint8_t* spscAllocate(size_t size);
  void spscDeallocate(uint8_t *ptr, size_t size);
  
  void waitForSpscReadableItemsAvailable(spsc_read_handle ctx, int32_t count);
  uint8_t *spscReadPtr(spsc_read_handle ctx);
  void spscCommitItemsRead(spsc_read_handle ctx, int32_t count);
  void spscSyncNumReadableItems(spsc_read_handle ctx);
  int32_t spscGetNumReadableItems(spsc_read_handle ctx);
  
  void waitForSpscWritableItemsAvailable(spsc_write_handle, int32_t count);
  uint8_t *spscWritePtr(spsc_write_handle handle);
  void spscCommitItemsWritten(spsc_write_handle, int32_t count);
  
  
  static inline uint64_t ulongAt(const uint8_t* buf, size_t offset) { return __builtin_bswap64(*(uint64_t*)(buf+offset)); }
  static inline uint32_t uintAt(const uint8_t* buf, size_t offset) { return __builtin_bswap32(*(uint32_t*)(buf+offset)); }
  static inline uint32_t ushortAt(const uint8_t* buf, size_t offset) {
    uint16_t x = *(uint16_t*)(buf+offset);
    asm("xchgb %b0, %h0" : "=Q" (x) : "0" (x));
    return x;
  }

  /*
   Multicast Methods (Market Data)
   */
  
  sfc_handle createSymbolFilterConfig(uint64_t date, void* addr, int32_t size, void* seqaddr);
  void registerAsyncNode(sfc_handle ctx, long addr, int signalGroupId, int color, int nodes);
  void registerAsyncNotification(sfc_handle ctx, int color, uint32_t qidx);
  void addQSymbolToFilterConfig(sfc_handle ctx, const char* symbol, const char* venue);

  void set_amex_symbol_mapping(void* ctx, int* mapping, int len);
  void set_arca_symbol_mapping(void* ctx, int* mapping, int len);
  void set_nyse_symbol_mapping(void* ctx, int* mapping, int len);
  

  void* init_registry();
  
  void setAutoJoin(nfc_handle, int auto_join);
  void setDblAddr(nfc_handle ctx, const char* addr_str);
  
  void register_multicast_topic_source(void* ctx, int32_t topic_source_id, int32_t group_id, const char* description,
                                       const char* local_ipaddr, int32_t port, const char* mcast_ipaddr);
  void register_backup_multicast_topic_source(void* ctx, int32_t topic_source_id, int32_t group_id, const char* description,
                                              const char* local_ipaddr, int32_t port, const char* mcast_ipaddr, int32_t buffer_size);
  void register_dev_null_topic(void* ctx, const int32_t* topic_sources, int32_t len);

  void register_qsymbol_topic(void* ctx, int32_t unique_key, const char* symbol, const char* venue, int32_t topic_source, uint32_t topic_id);
  void register_oe_qsymbol_topic(void*ctx, int32_t unique_key, const char* symbol, const char* venue, int32_t topic_source, uint32_t topic_id);

  void register_queue_for_topic_id(void* ctx, uint32_t qidx, int32_t unique_key);

  void set_logging_queue(void* ctx, int64_t addr, int32_t size, int64_t seqaddr);
  void init_logging_queue(void* ctx, int64_t addr, int32_t size, int64_t seqaddr);
  void init_oe_logging_queue(void* ctx, int64_t addr, int32_t size, int64_t seqaddr);

  void register_log_queue(void *ctx, uint32_t qidx);
  void register_oe_log_queue(void *ctx, uint32_t qidx);

  void build_filters(void* ctx, int64_t date);

  
  spsc_read_handle createSpscReader(uint8_t *addr, int32_t size, int64_t *seqaddr);
  spsc_write_handle createSpscWriter(uint8_t* addr, int32_t size, int64_t* seqaddr);
  
  void destroySpscReader(spsc_read_handle ctx);
  void destroySpscWriter(spsc_write_handle ctx);
  

  void* create_dbl_device(void* registry, const char* dbl_addr);
  void* create_epoll_device(void *registry);
  void* create_efvi_device(void *registry, const char* local_addr, int autojoin);
  void start_device_with_offset(void* device, int64_t time_offset);
  void start_device(void* device);
  void stop_device(void* device);
  void start_source(void* device, int32_t source_id);
  void stop_source(void * device, int32_t source_id);
  
  /*
   TCP Methods (Order Entry)
   */
  // oesfc_handle createOrderEntrySymbolFilterConfig(uint64_t date, void* adminAddr, int32_t adminSize, void* adminSeqaddr, void* logAddr, int32_t logSize, void* logSeqaddr);
  void addQSymbolToOrderEntryFilterConfig(oesfc_handle ctx, const char* symbol, const char* venue);
  void setReceiveQueueForSymbol(oesfc_handle ctx, const char* symbol, uint32_t qidx);
  
  void* createOrderEntryNetworkConfig();
  void addOrderEntrySession(void *network_config, int32_t sessionId, const char* venue, const char* remote_ipaddr, int32_t port, int isDatagram);
  void setAdminTopicInfo(void *network_config, const char *venue, uint32_t topic_id);

  void createOrderEntryDevice(void *registry, void *adminQueue, void *networkConfig);
  // oe_device createOrderEntryDevice(oesfc_handle symbol_filter_config, oenfc_handle networkConfig);
  
  void startReader(void *registry, void *network_config);
  void startReaderWithOffset(void *registry, void *network_config, int64_t time_offset);
  void startWriter(void *registry, void *network_config);
  
  int connectSession(void *registry, void *networkConfig, int32_t session_id);
  void disconnectSession(void *registry, void *networkConfig, int32_t session_id);

  
#ifdef __cplusplus
  }
#endif


#endif /* defined(__DblFilter__dbl_filter_wrapper__) */
