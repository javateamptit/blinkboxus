/****************************************************************************
** Include files and define several constants
****************************************************************************/
#define _BSD_SOURCE

#include <pthread.h>
#include <time.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <math.h>
#include <string.h>
#include <unistd.h>

#include "edge_book_proc.h"
#include "trading_mgmt.h"
#include "kernel_algorithm.h"
#include "book_mgmt.h"
#include "utility.h"
#include "configuration.h"
#include "hbitime.h"
#include "logging.h"

/****************************************************************************
** Global variable declarations
****************************************************************************/
t_EDGE_Book_Conf EDGX_BOOK_Conf;
t_EDGE_Book_Conf EDGA_BOOK_Conf;

extern t_MonitorEdgxBook MonitorEdgxBook;
extern t_MonitorEdgaBook MonitorEdgaBook;

/****************************************************************************
** Function declarations
****************************************************************************/

/****************************************************************************
- Function name:  Init_DataStructure_EDGX_BOOK
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int Init_DataStructure_EDGX_BOOK(void)
{
  TraceLog(DEBUG_LEVEL, "Initialize EDGX Book...\n");
  
  int i, j;
  MonitorEdgxBook.procCounter = 0;
  
  for (i = 0; i < MAX_EDGX_BOOK_GROUP; i++)
  {
    MonitorEdgxBook.recvCounter[i] = 0;
    EDGX_BOOK_Conf.group[i].groupPermission = SymbolSplitInfo[AlgorithmConfig.splitConfig.symbolRangeID].edgx[i];
    for (j = 0; j < DBLFILTER_NUM_QUEUES; j++)
      EDGX_BOOK_Conf.group[i].lossCounter[j] = 0;
  }
  
  int stockSymbolIndex;
  for (stockSymbolIndex = 0; stockSymbolIndex < MAX_STOCK_SYMBOL; stockSymbolIndex++)
  {
    for (j = 0; j < PRICE_HASHMAP_BUCKET_SIZE; j++)
    {
      DeleteAllPricePointsFromHashSlot(Price_HashMap[stockSymbolIndex][BOOK_EDGX][ASK_SIDE][j]);
      Price_HashMap[stockSymbolIndex][BOOK_EDGX][ASK_SIDE][j] = NULL;
      
      DeleteAllPricePointsFromHashSlot(Price_HashMap[stockSymbolIndex][BOOK_EDGX][BID_SIDE][j]);
      Price_HashMap[stockSymbolIndex][BOOK_EDGX][BID_SIDE][j] = NULL;
    }
  }
  
  return SUCCESS;
}

int Init_DataStructure_EDGA_BOOK(void)
{
  TraceLog(DEBUG_LEVEL, "Initialize EDGA Book...\n");
  
  int i, j;
  MonitorEdgaBook.procCounter = 0;
  
  for (i = 0; i < MAX_EDGA_BOOK_GROUP; i++)
  {
    MonitorEdgaBook.recvCounter[i] = 0;
    EDGA_BOOK_Conf.group[i].groupPermission = SymbolSplitInfo[AlgorithmConfig.splitConfig.symbolRangeID].edga[i];
    for (j = 0; j < DBLFILTER_NUM_QUEUES; j++)
      EDGA_BOOK_Conf.group[i].lossCounter[j] = 0;
  }
  
  int stockSymbolIndex;
  for (stockSymbolIndex = 0; stockSymbolIndex < MAX_STOCK_SYMBOL; stockSymbolIndex++)
  {
    for (j = 0; j < PRICE_HASHMAP_BUCKET_SIZE; j++)
    {
      DeleteAllPricePointsFromHashSlot(Price_HashMap[stockSymbolIndex][BOOK_EDGA][ASK_SIDE][j]);
      Price_HashMap[stockSymbolIndex][BOOK_EDGA][ASK_SIDE][j] = NULL;
      
      DeleteAllPricePointsFromHashSlot(Price_HashMap[stockSymbolIndex][BOOK_EDGA][BID_SIDE][j]);
      Price_HashMap[stockSymbolIndex][BOOK_EDGA][BID_SIDE][j] = NULL;
    }
  }
  
  return SUCCESS;
}

int EDGE_ProcessBookMessage(unsigned char ecn_book_id, uint32_t symbolId, char *msg, unsigned long arrivalTime, unsigned int timeStatusSeconds)
{
  if (BookStatusMgmt[ecn_book_id].isConnected == DISCONNECTED) return SUCCESS;  //If book is disconnected, we do not process remaining message in the buffer
  switch (msg[1])
  {
    case EDGE_BOOK_MSG_ADD_ORDER_LONG_FORM:
      if (ProcessAddOrderMessage_EDGE_BOOK(ecn_book_id, symbolId, msg, arrivalTime, timeStatusSeconds) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "%s Book: Problem in processing 'Add Order Long Form' msg\n", GetBookNameByIndex(ecn_book_id));
        return ERROR;
      }
      break;
    
    case EDGE_BOOK_MSG_ADD_ORDER_SHORT_FORM:
      if (ProcessAddOrderMessage_EDGE_BOOK(ecn_book_id, symbolId, msg, arrivalTime, timeStatusSeconds) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "%s Book: Problem in processing 'Add Order Short Form' msg\n", GetBookNameByIndex(ecn_book_id));
        return ERROR;
      }
      break;
    
    case EDGE_BOOK_MSG_ADD_ORDER_EXTENDED_FORM:
      if (ProcessAddOrderMessage_EDGE_BOOK(ecn_book_id, symbolId, msg, arrivalTime, timeStatusSeconds) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "%s Book: Problem in processing 'Add Order Extended Form' msg\n", GetBookNameByIndex(ecn_book_id));
        return ERROR;
      }
      break;
      

    case EDGE_BOOK_MSG_EXECUTED_ORDER:
      if (ProcessOrderExecutedMessage_EDGE_BOOK(ecn_book_id, symbolId, msg, arrivalTime, timeStatusSeconds) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "%s Book: Problem in processing 'Order Executed' msg\n", GetBookNameByIndex(ecn_book_id));
        return ERROR;
      }
      break;

    case EDGE_BOOK_MSG_EXECUTED_AT_ORDER:
      if (ProcessOrderExecutedAtMessage_EDGE_BOOK(ecn_book_id, symbolId, msg, arrivalTime, timeStatusSeconds) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "%s Book: Problem in processing 'Order Executed At Price' msg\n", GetBookNameByIndex(ecn_book_id));
        return ERROR;
      }
      break;

    case EDGE_BOOK_MSG_CANCELED_ORDER:
      if (ProcessCanceledOrderMessage_EDGE_BOOK(ecn_book_id, symbolId, msg, arrivalTime, timeStatusSeconds) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "%s Book: Problem in processing 'Delete Order' msg\n", GetBookNameByIndex(ecn_book_id));
        return ERROR;
      }
      break;

    case EDGE_BOOK_MSG_MODIFIED_ORDER_LONG_FORM:
      if (ProcessModifyOrderMessage_EDGE_BOOK(ecn_book_id, symbolId, msg, arrivalTime, timeStatusSeconds) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "%s Book: Problem in processing 'Modify Order Long Form' msg\n", GetBookNameByIndex(ecn_book_id));
        return ERROR;
      }
      break;
      
    case EDGE_BOOK_MSG_MODIFIED_ORDER_SHORT_FORM:
      if (ProcessModifyOrderMessage_EDGE_BOOK(ecn_book_id, symbolId, msg, arrivalTime, timeStatusSeconds) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "%s Book: Problem in processing 'Modify Order Short Form' msg\n", GetBookNameByIndex(ecn_book_id));
        return ERROR;
      }
      break;

    case EDGE_BOOK_MSG_SECURITY_STATUS:
      ProcessSymbolStatusMessage_EDGE_BOOK(ecn_book_id, symbolId, msg, arrivalTime);
      break;
    case EDGE_BOOK_MSG_TRADE_LONG_FORM:
      ProcessTradeLongMessage_EDGE_BOOK(ecn_book_id, symbolId, msg, arrivalTime);
      break;
    case EDGE_BOOK_MSG_TRADE_SHORT_FORM:
      ProcessTradeShortMessage_EDGE_BOOK(ecn_book_id, symbolId, msg, arrivalTime);
      break;
    case EDGE_BOOK_MSG_TRADE_EXTENDED_FORM:
      ProcessTradeExtendedMessage_EDGE_BOOK(ecn_book_id, symbolId, msg, arrivalTime);
      break;
    case EDGE_BOOK_MSG_TRADE_BREAK:
    case EDGE_BOOK_MSG_ADD_ORDER_WITH_ATTRIBUTION:
    case EDGE_BOOK_MSG_TIME:
      break;
      
    case EDGE_BOOK_MSG_END_OF_SESSION:
      TraceLog(DEBUG_LEVEL, "%s Book: Received 'End Of Session' msg\n", GetBookNameByIndex(ecn_book_id));
      break;
    default:
      TraceLog(WARN_LEVEL, "%s Book: Received unknown message type (skipped): %X\n", GetBookNameByIndex(ecn_book_id), msg[1]);
      break;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  CheckStaleData_EDGE
****************************************************************************/
int CheckStaleData_EDGE(unsigned char ecn_book_id, char *buffer, int stockSymbolIndex, unsigned long arrivalTime, unsigned int timeStatusSeconds)
{
  // For EDGE: timeStatusSeconds: number of seconds since Epoch
  unsigned int msgNanos = *(unsigned int*)(&buffer[2]);
  unsigned long edgeNanos = (timeStatusSeconds * TEN_RAISE_TO_9) + msgNanos;
  long delay = arrivalTime - edgeNanos;
  
  if (delay > DataDelayThreshold[ecn_book_id])
  {
    if (SymbolMgmt.symbolList[stockSymbolIndex].isStale[ecn_book_id] == 0)
    {
      SymbolMgmt.symbolList[stockSymbolIndex].isStale[ecn_book_id] = 1;
      BuildSendStaleMarketNotification(ecn_book_id, stockSymbolIndex, delay);
      TraceLog(WARN_LEVEL, "%s Book: '%.8s' data is delayed for %ld nanoseconds\n", GetBookNameByIndex(ecn_book_id), SymbolMgmt.symbolList[stockSymbolIndex].symbol, delay);
    }
  }
  else
  {
    if (SymbolMgmt.symbolList[stockSymbolIndex].isStale[ecn_book_id] == 0) return SUCCESS;
    
    if (SymbolMgmt.symbolList[stockSymbolIndex].isStale[ecn_book_id] == 1)
    {
      SymbolMgmt.symbolList[stockSymbolIndex].isStale[ecn_book_id] = 0;
      BuildSendStaleMarketNotification(ecn_book_id, stockSymbolIndex, delay);
      TraceLog(WARN_LEVEL, "%s Book: '%.8s' data is fast again\n", GetBookNameByIndex(ecn_book_id), SymbolMgmt.symbolList[stockSymbolIndex].symbol);
    }
    else
    {
      SymbolMgmt.symbolList[stockSymbolIndex].isStale[ecn_book_id] = 0;
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessAddOrderMessage_EDGE_BOOK
****************************************************************************/
int ProcessAddOrderMessage_EDGE_BOOK(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime, unsigned int timeStatusSeconds)
{
  t_OrderDetail EDGE_Order;
  char msgType = buffer[1];
  switch (msgType)
  {
    case EDGE_BOOK_MSG_ADD_ORDER_LONG_FORM:
      {
        EDGE_Order.shareVolume = *(int*)(&buffer[15]);
        EDGE_Order.sharePrice = (*(unsigned long*)(&buffer[25])) * 0.0001;
      }
      break;
      
    case EDGE_BOOK_MSG_ADD_ORDER_SHORT_FORM:
      {
        EDGE_Order.shareVolume = *(unsigned short*)(&buffer[15]);
        EDGE_Order.sharePrice = (*(unsigned short*)(&buffer[23])) * 0.01;
      }
      break;
      
    case EDGE_BOOK_MSG_ADD_ORDER_EXTENDED_FORM:
      {
        EDGE_Order.shareVolume = *(int*)(&buffer[15]);
        EDGE_Order.sharePrice = (*(unsigned long*)(&buffer[27])) * 0.0001;
      }
      break;
    default:
      TraceLog(ERROR_LEVEL, "ProcessAddOrderMessage_EDGE_BOOK() invalid msg type: %d\n", msgType);
      return SUCCESS;
  }
  
  CheckStaleData_EDGE(ecn_book_id, buffer, symbolId, arrivalTime, timeStatusSeconds);
  
  EDGE_Order.refNum = *(unsigned long*)(&buffer[6]);
  int side = (buffer[14] == 'B') ? BID_SIDE : ASK_SIDE;
  
  if (SymbolMgmt.symbolList[symbolId].haltStatus[ecn_book_id] == TRADING_NORMAL)
  {
    int tradeRet = DetectCrossTrade(&EDGE_Order, side, symbolId, ecn_book_id, arrivalTime/1000, REASON_CROSS_ENTRY);
    
    if (tradeRet == TRADE_DETECTED_SELF_CROSSED)
    {
      DeleteFromPrice(EDGE_Order.sharePrice, ecn_book_id, !side, symbolId);
    }
  }
  
  t_QuoteNode *retNode = InsertQuote(ecn_book_id, side, symbolId, &EDGE_Order);
  if (retNode)
  {
    OrderMapInsert(OrderMaps[ecn_book_id][_consumerId], EDGE_Order.refNum, retNode,(side == BID_SIDE)?'B':'S');
  }
  else
    return ERROR;

  CheckBookCrossToEnableSymbol(symbolId);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessOrderExecutedMessage_EDGE_BOOK
****************************************************************************/
int ProcessOrderExecutedMessage_EDGE_BOOK(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime, unsigned int timeStatusSeconds)
{
  unsigned long refNumber = *(unsigned long*)(&buffer[6]);

  t_QuoteNodeInfo nodeInfo = OrderMapRemove(OrderMaps[ecn_book_id][_consumerId], refNumber);

  if (nodeInfo.node != NULL && nodeInfo.node != DeletedQuotePlaceholder)
  {
    CheckStaleData_EDGE(ecn_book_id, buffer, symbolId, arrivalTime, timeStatusSeconds);
    int side = (nodeInfo.side == 'S')?ASK_SIDE:BID_SIDE;
    int execShares = *(int*)(&buffer[14]);
    if (execShares >= nodeInfo.node->info.shareVolume)
    {
      DeleteQuote(nodeInfo.node, ecn_book_id, side, symbolId);
      CheckBookCrossToEnableSymbol(symbolId);
    }
    else
    {
      OrderMapInsert(OrderMaps[ecn_book_id][_consumerId], refNumber, nodeInfo.node, nodeInfo.side);
      ModifyQuote(nodeInfo.node, nodeInfo.node->info.shareVolume - execShares, side, symbolId);
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessOrderExecutedAtMessage_EDGE_BOOK
****************************************************************************/
int ProcessOrderExecutedAtMessage_EDGE_BOOK(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime, unsigned int timeStatusSeconds)
{
  unsigned long refNumber = *(unsigned long*)(&buffer[6]);

  t_QuoteNodeInfo nodeInfo = OrderMapRemove(OrderMaps[ecn_book_id][_consumerId], refNumber);   

  if (nodeInfo.node != NULL && nodeInfo.node != DeletedQuotePlaceholder)
  {
    CheckStaleData_EDGE(ecn_book_id, buffer, symbolId, arrivalTime, timeStatusSeconds);

    int side = (nodeInfo.side == 'S')?ASK_SIDE:BID_SIDE;
    int newDisplayShare = *(int*)(&buffer[18]);
    
    if (newDisplayShare <= 0)
    {
      // Delete old node
      DeleteQuote(nodeInfo.node, ecn_book_id, side, symbolId);
      CheckBookCrossToEnableSymbol(symbolId);
    }
    else
    {
      OrderMapInsert(OrderMaps[ecn_book_id][_consumerId], refNumber, nodeInfo.node, nodeInfo.side);
      ModifyQuote(nodeInfo.node, newDisplayShare, side, symbolId);
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessCanceledOrderMessage_EDGE_BOOK
****************************************************************************/
int ProcessCanceledOrderMessage_EDGE_BOOK(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime, unsigned int timeStatusSeconds)
{
  unsigned long refNumber = *(unsigned long*)(&buffer[6]);

  t_QuoteNodeInfo nodeInfo = OrderMapRemove(OrderMaps[ecn_book_id][_consumerId], refNumber);

  if (nodeInfo.node != NULL && nodeInfo.node != DeletedQuotePlaceholder)
  {
    CheckStaleData_EDGE(ecn_book_id, buffer, symbolId, arrivalTime, timeStatusSeconds);
    
    int side = (nodeInfo.side == 'S')?ASK_SIDE:BID_SIDE;
    
    DeleteQuote(nodeInfo.node, ecn_book_id, side, symbolId);
    CheckBookCrossToEnableSymbol(symbolId);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessModifyOrderMessage_EDGE_BOOK
****************************************************************************/
int ProcessModifyOrderMessage_EDGE_BOOK(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime, unsigned int timeStatusSeconds)
{
  t_OrderDetail EDGE_Order;
  
  char msgType = buffer[1];
  
  // Order Id
  EDGE_Order.refNum = *(unsigned long*)(&buffer[6]);

  if (msgType == EDGE_BOOK_MSG_MODIFIED_ORDER_LONG_FORM)
  {
    // Get shares
    EDGE_Order.shareVolume = *(int*)(&buffer[14]);
    
    // Get Share Price
    EDGE_Order.sharePrice = (*(unsigned long*)(&buffer[18])) * 0.0001;
  }
  else if (msgType == EDGE_BOOK_MSG_MODIFIED_ORDER_SHORT_FORM)
  {
    // Get shares
    EDGE_Order.shareVolume = *(unsigned short*)(&buffer[14]);
    
    // Get Share Price
    EDGE_Order.sharePrice = (*(unsigned short*)&buffer[16]) * 0.01;
  }
  
  /* We will delete old quote */
  t_QuoteNodeInfo nodeInfo = OrderMapRemove(OrderMaps[ecn_book_id][_consumerId], EDGE_Order.refNum);

  if(nodeInfo.node != NULL)
  {
    int side = (nodeInfo.side == 'S')?ASK_SIDE:BID_SIDE;
    if(nodeInfo.node != DeletedQuotePlaceholder) DeleteQuote(nodeInfo.node, ecn_book_id, side, symbolId);

    CheckStaleData_EDGE(ecn_book_id, buffer, symbolId, arrivalTime, timeStatusSeconds);
  
    /* We have all information needed for cross detection and adding new info to book */
    if (SymbolMgmt.symbolList[symbolId].haltStatus[ecn_book_id] == TRADING_NORMAL)
    {
      int tradeRet = DetectCrossTrade(&EDGE_Order, side, symbolId, ecn_book_id, arrivalTime/1000, REASON_CROSS_ENTRY);

      if (tradeRet == TRADE_DETECTED_SELF_CROSSED)
      {
        DeleteFromPrice(EDGE_Order.sharePrice, ecn_book_id, !side, symbolId);
      }
    }

    /* Add new quote to book */ 
    t_QuoteNode *retNode = InsertQuote(ecn_book_id, side, symbolId, &EDGE_Order);
  
    if (retNode)
    {
      OrderMapInsert(OrderMaps[ecn_book_id][_consumerId], EDGE_Order.refNum, retNode, (side == BID_SIDE)?'B':'S');
    }
    else
      return ERROR;
  }
  CheckBookCrossToEnableSymbol(symbolId);

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessSymbolStatusMessage_EDGE_BOOK
****************************************************************************/
int ProcessSymbolStatusMessage_EDGE_BOOK(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime)
{
  switch (buffer[19])
  {
    case 'T': //Security is Trading
      UpdateHaltStatusForASymbol(symbolId, TRADING_NORMAL, arrivalTime/TEN_RAISE_TO_9, ecn_book_id);
      break;
    case 'H': //Security is Halted
      UpdateHaltStatusForASymbol(symbolId, TRADING_HALTED, arrivalTime/TEN_RAISE_TO_9, MAX_ECN_BOOK);
      break;
  }
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTradeLongMessage_EDGE_BOOK
****************************************************************************/
int ProcessTradeLongMessage_EDGE_BOOK(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime)
{
  UpdateHaltStatusForASymbol(symbolId, TRADING_NORMAL, arrivalTime/TEN_RAISE_TO_9, ecn_book_id);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTradeShortMessage_EDGE_BOOK
****************************************************************************/
int ProcessTradeShortMessage_EDGE_BOOK(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime)
{
  UpdateHaltStatusForASymbol(symbolId, TRADING_NORMAL, arrivalTime/TEN_RAISE_TO_9, ecn_book_id);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTradeExtendedMessage_EDGE_BOOK
****************************************************************************/
int ProcessTradeExtendedMessage_EDGE_BOOK(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime)
{
  UpdateHaltStatusForASymbol(symbolId, TRADING_NORMAL, arrivalTime/TEN_RAISE_TO_9, ecn_book_id);
  return SUCCESS;
}
