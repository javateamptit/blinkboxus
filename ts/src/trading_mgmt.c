/****************************************************************************
** Project name:  Equity Arbitrage Application
** File name:     trading_mgmt.c
** Description:   This file contains function definitions that were declared
          in trading_mgmt.h
** Author:      Sang Nguyen-Minh
** First created on 08 October 2007
** Last updated on 08 October 2007
****************************************************************************/
#define _ISOC9X_SOURCE

#include <stdarg.h>

#include "trading_mgmt.h"
#include "book_mgmt.h"
#include "quote_mgmt.h"
#include "arca_book_proc.h"
#include "kernel_algorithm.h"
#include "aggregation_server_proc.h"
#include "environment_proc.h"
#include "hbitime.h"
#include "logging.h"

/****************************************************************************
            GLOBAL VARIABLES
****************************************************************************/
t_TradingInfo         TradingCollection[MAX_STOCK_SYMBOL];
t_TradeOutputLogMgmt  TradeOutputLogMgmt;
t_TradingStatus       TradingStatus;
t_BBOInfoMgmt         BBOInfoMgmt;
t_TradingSessionMgmt  TradingSessionMgmt;

static char TradeOutputLogMapTable[MAX_LOG_TYPE][MAX_LEN_PER_LOG_ENTRY];

int CurrentCrossID = 0;
int CurrentOrderID = 0;
int AllVolatileStatus = 0;
int MdcToOecMapping[MAX_ECN_BOOK];
int OrderToBookMapping[MAX_ECN_ORDER];
static int IsInNasdaqAuction = NO;
static int EcnHasAskBidLaunchCollection[MAX_ECN_BOOK][2];
static int CurrentMarketTimeType = -1;
static int Side[MAX_ECN_ORDER][MAX_SIDE_VALUE_TYPE];
double SecFeeAndCommissionInCent = 0.00;

static unsigned short BBOCounterList[MAX_CROSS_ID];

int TradingCounter = 0;
extern volatile int IsTradeServerStopped;
/****************************************************************************
            LIST OF FUNCTIONS
****************************************************************************/

/****************************************************************************
- Function name:  InitTradingDataStructures
****************************************************************************/
int InitTradingDataStructures(void)
{
  int tradingInfoIndex;
  int index;
  
  // Set default All-Volatile Toggle value = 0
  AllVolatileStatus = 0;
  
  // Initialize BBO counter list
  memset(BBOCounterList, 0, sizeof(short) * MAX_CROSS_ID);  
  
  for (tradingInfoIndex = 0; tradingInfoIndex < MAX_STOCK_SYMBOL; tradingInfoIndex++)
  {
    TradingCollection[tradingInfoIndex].crossID = -1;
    TradingCollection[tradingInfoIndex].isTrading = NO;
    TradingCollection[tradingInfoIndex].lockedSide = NO_LOCK;
    
    TradingCollection[tradingInfoIndex].side = -1;
    TradingCollection[tradingInfoIndex].totalBought = 0.00;
    TradingCollection[tradingInfoIndex].totalSold = 0.00;
    TradingCollection[tradingInfoIndex].longShareVolume = 0;
    TradingCollection[tradingInfoIndex].shortShareVolume = 0;
    TradingCollection[tradingInfoIndex].filledEntryPrice = 0.00;
    TradingCollection[tradingInfoIndex].filledEntryShareVolume = 0;
    TradingCollection[tradingInfoIndex].exitSideHoldingShareVolume = 0;  
    TradingCollection[tradingInfoIndex].entryExitRefNum = 0;
    TradingCollection[tradingInfoIndex].routedLiquidateAttempted = NO;
    
    // Initialize entry and exit collections
    for (index = 0; index < MAX_OPEN_ORDERS; index++)
    {
      TradingCollection[tradingInfoIndex].openOrders[index].orderID = -1;
      TradingCollection[tradingInfoIndex].openOrders[index].shareVolume = 0;
      TradingCollection[tradingInfoIndex].openOrders[index].sharePrice = 0.00;
      TradingCollection[tradingInfoIndex].openOrders[index].side = -1;
      TradingCollection[tradingInfoIndex].entryCounter = 0;
      TradingCollection[tradingInfoIndex].exitCounter = 0;
    }
  }
  
  // Initialize trading status
  for (index = 0; index < MAX_ECN_ORDER; index++)
  {
    OecConfig[index].enableTrading = DISABLE;
  }

  // Reserve one memory-address for each pointer of Trading Status;
  for(index = 0; index < MAX_ECN_ORDER; index++)
  {
    TradingStatus.status[index] = &(OecConfig[index].enableTrading);
  }

  // Initialize SIDE values
  Side[ORDER_ARCA_DIRECT][ASK_SIDE] =   '2';
  Side[ORDER_ARCA_DIRECT][BID_SIDE] =   '1';
  Side[ORDER_ARCA_DIRECT][SHORT_SIDE] = '5';
  
  Side[ORDER_NASDAQ_OUCH][ASK_SIDE] =   'S';
  Side[ORDER_NASDAQ_OUCH][BID_SIDE] =   'B';
  Side[ORDER_NASDAQ_OUCH][SHORT_SIDE] = 'T';
                                          
  Side[ORDER_NYSE_CCG][ASK_SIDE] =      '2';
  Side[ORDER_NYSE_CCG][BID_SIDE] =      '1';
  Side[ORDER_NYSE_CCG][SHORT_SIDE] =    '5';
   
  Side[ORDER_NASDAQ_RASH][ASK_SIDE] =   'S';
  Side[ORDER_NASDAQ_RASH][BID_SIDE] =   'B';
  Side[ORDER_NASDAQ_RASH][SHORT_SIDE] = 'T';
     
  Side[ORDER_BATSZ_BOE][ASK_SIDE] =     '2';
  Side[ORDER_BATSZ_BOE][BID_SIDE] =     '1';
  Side[ORDER_BATSZ_BOE][SHORT_SIDE] =   '5';
  
  Side[ORDER_EDGX][ASK_SIDE] =          'S';
  Side[ORDER_EDGX][BID_SIDE] =          'B';
  Side[ORDER_EDGX][SHORT_SIDE] =        'T';
  
  Side[ORDER_EDGA][ASK_SIDE] =          'S';
  Side[ORDER_EDGA][BID_SIDE] =          'B';
  Side[ORDER_EDGA][SHORT_SIDE] =        'T';

  Side[ORDER_NASDAQ_BX][ASK_SIDE] =     'S';
  Side[ORDER_NASDAQ_BX][BID_SIDE] =     'B';
  Side[ORDER_NASDAQ_BX][SHORT_SIDE] =   'T';

  Side[ORDER_BYX_BOE][ASK_SIDE] =       '2';
  Side[ORDER_BYX_BOE][BID_SIDE] =       '1';
  Side[ORDER_BYX_BOE][SHORT_SIDE] =     '5';
  
  Side[ORDER_NASDAQ_PSX][ASK_SIDE] =    'S';
  Side[ORDER_NASDAQ_PSX][BID_SIDE] =    'B';
  Side[ORDER_NASDAQ_PSX][SHORT_SIDE] =  'T';
  
  // Initialize EcnHasAskBidLaunchCollection
  EcnHasAskBidLaunchCollection[BOOK_ARCA][ASK_SIDE] =   1;
  EcnHasAskBidLaunchCollection[BOOK_ARCA][BID_SIDE] =   2;
  
  EcnHasAskBidLaunchCollection[BOOK_NASDAQ][ASK_SIDE] = 3;
  EcnHasAskBidLaunchCollection[BOOK_NASDAQ][BID_SIDE] = 4;
  
  /*  This one is completely wrong!
    We never have Book launch = ORDER_NASDAQ_RASH
  //EcnHasAskBidLaunchCollection[ORDER_NASDAQ_RASH][ASK_SIDE] = 5;
  //EcnHasAskBidLaunchCollection[ORDER_NASDAQ_RASH][BID_SIDE] = 6;
  */
  
  EcnHasAskBidLaunchCollection[BOOK_BATSZ][ASK_SIDE] =  7;
  EcnHasAskBidLaunchCollection[BOOK_BATSZ][BID_SIDE] =  8;
  
  EcnHasAskBidLaunchCollection[BOOK_NYSE][ASK_SIDE] =   9;
  EcnHasAskBidLaunchCollection[BOOK_NYSE][BID_SIDE] =  10;
  
  EcnHasAskBidLaunchCollection[BOOK_EDGX][ASK_SIDE] =  11;
  EcnHasAskBidLaunchCollection[BOOK_EDGX][BID_SIDE] =  12;
  
  EcnHasAskBidLaunchCollection[BOOK_EDGA][ASK_SIDE] =  13;
  EcnHasAskBidLaunchCollection[BOOK_EDGA][BID_SIDE] =  14;
  
  EcnHasAskBidLaunchCollection[BOOK_NDBX][ASK_SIDE] =  15;
  EcnHasAskBidLaunchCollection[BOOK_NDBX][BID_SIDE] =  16;
  
  EcnHasAskBidLaunchCollection[BOOK_BYX][ASK_SIDE] =   17;
  EcnHasAskBidLaunchCollection[BOOK_BYX][BID_SIDE] =   18;
  
  EcnHasAskBidLaunchCollection[BOOK_PSX][ASK_SIDE] =   19;
  EcnHasAskBidLaunchCollection[BOOK_PSX][BID_SIDE] =   20;
  
  EcnHasAskBidLaunchCollection[BOOK_AMEX][ASK_SIDE] =  21;
  EcnHasAskBidLaunchCollection[BOOK_AMEX][BID_SIDE] =  22;
  
  MdcToOecMapping[BOOK_ARCA] =   ORDER_ARCA_DIRECT;
  MdcToOecMapping[BOOK_NASDAQ] = ORDER_NASDAQ_OUCH;
  MdcToOecMapping[BOOK_NYSE] =   ORDER_NYSE_CCG;
  MdcToOecMapping[BOOK_AMEX] =   ORDER_NYSE_CCG;
  MdcToOecMapping[BOOK_BATSZ] =  ORDER_BATSZ_BOE;
  MdcToOecMapping[BOOK_BYX] =    ORDER_BYX_BOE;
  MdcToOecMapping[BOOK_EDGX] =   ORDER_EDGX;
  MdcToOecMapping[BOOK_EDGA] =   ORDER_EDGA;
  MdcToOecMapping[BOOK_NDBX] =   ORDER_NASDAQ_BX;
  MdcToOecMapping[BOOK_PSX] =    ORDER_NASDAQ_PSX;
  
  OrderToBookMapping[ORDER_ARCA_DIRECT] = BOOK_ARCA;
  OrderToBookMapping[ORDER_NASDAQ_OUCH] = BOOK_NASDAQ;
  OrderToBookMapping[ORDER_NASDAQ_RASH] = BOOK_NASDAQ;
  OrderToBookMapping[ORDER_NYSE_CCG] =    BOOK_NYSE;    //Careful when use this, it might be BOOK_AMEX
  OrderToBookMapping[ORDER_BATSZ_BOE] =   BOOK_BATSZ;
  OrderToBookMapping[ORDER_BYX_BOE] =     BOOK_BYX;
  OrderToBookMapping[ORDER_EDGX] =        BOOK_EDGX;
  OrderToBookMapping[ORDER_EDGA] =        BOOK_EDGA;
  OrderToBookMapping[ORDER_NASDAQ_BX] =   BOOK_NDBX;
  OrderToBookMapping[ORDER_NASDAQ_PSX] =  BOOK_PSX;
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  initTradeOutputLogMapTable
****************************************************************************/
void InitTradeOutputLogMapTable(void)
{
  //TradeOutputLogMapTable[MAX_LOG_TYPE][MAX_LEN_PER_LOG_ENTRY];
  int logTypeIndex = 0;
  
  /*
  Log Type:
  + 0
  Description: 
  + Multi-leve trading -  Multiple entry side order placement
  */
  memset(TradeOutputLogMapTable[logTypeIndex], 0, MAX_LEN_PER_LOG_ENTRY);
  logTypeIndex++;
  
  /*
  Log Type:
  + 1
  Description: 
  + Multi-leve trading
  */
  memset(TradeOutputLogMapTable[logTypeIndex], 0, MAX_LEN_PER_LOG_ENTRY);
  logTypeIndex++;
  
  /*
  Log Type:
  + 2
  Description: 
  + Trade: Stock Symbol [symbol name], [NASDAQ/ARCA/BATS/STAND_ALONE] Server, 
  [ARCA/NASDAQ/NYSE/BATSZ] [Ask/Bid] Launch (ECN of Best Quote) [Trade type]
  */
  strcpy(TradeOutputLogMapTable[logTypeIndex], "sddddd");
  logTypeIndex++;
  
  /*
  Log Type:
  + 3
  Description: 
  + [ASK/BID]: [shareVolume] [ARCA/NASDAQ/NYSE/BATSZ] at [share Price]
  */
  strcpy(TradeOutputLogMapTable[logTypeIndex], "dddf");
  logTypeIndex++;
  
  /*
  Log Type:
  + 4
  Description: 
  + Maximum spread exceeded. Trading for this cross is disabled
  */
  memset(TradeOutputLogMapTable[logTypeIndex], 0, MAX_LEN_PER_LOG_ENTRY);
  logTypeIndex++;
  
  /*
  Log Type:
  + 5
  Description: 
  + Maximum spread exceeded. Trading is disabled
  */
  memset(TradeOutputLogMapTable[logTypeIndex], 0, MAX_LEN_PER_LOG_ENTRY);
  logTypeIndex++;
  
  /*
  Log Type:
  + 6
  Description: 
  + Fatal error inside [ARCA/NASDAQ/NYSE/BATSZ]
  */
  strcpy(TradeOutputLogMapTable[logTypeIndex], "d");
  logTypeIndex++;
  
  /*
  Log Type:
  + 7
  Description: 
  + Fatal error! [ARCA/NASDAQ/NYSE/BATSZ] partial hashing data structure is full!!!
  */
  strcpy(TradeOutputLogMapTable[logTypeIndex], "d");
  logTypeIndex++;
  
  /*
  Log Type:
  + 8
  Description: 
  + This side of the trade is not yet ready. We cannot trade
  */
  memset(TradeOutputLogMapTable[logTypeIndex], 0, MAX_LEN_PER_LOG_ENTRY);
  logTypeIndex++;
  
  /*
  Log Type:
  + 9
  Description: 
  + Other side of the trade is not yet ready. We cannot trade
  */
  memset(TradeOutputLogMapTable[logTypeIndex], 0, MAX_LEN_PER_LOG_ENTRY);
  logTypeIndex++;
  
  /*
  Log Type:
  + 10
  Description: 
  + [S/Sd/B/Bt/Cd] [share volume] [symbol name] at [share price] [ARCA/NASDAQ/NYSE/BATSZ]
  */
  strcpy(TradeOutputLogMapTable[logTypeIndex], "ddsfd");
  logTypeIndex++;
  
  /*
  Log Type:
  + 11
  Description: 
  + Aghh: Missed the trade
  */
  memset(TradeOutputLogMapTable[logTypeIndex], 0, MAX_LEN_PER_LOG_ENTRY);
  logTypeIndex++;
    
  /*
  Log Type:
  + 12
  Description: 
  + [ARCA/NASDAQ/NYSE/BATSZ]: Missed the trade: Rejected!!!
  */
  strcpy(TradeOutputLogMapTable[logTypeIndex], "d");
  logTypeIndex++;
  
  /*
  Log Type:
  + 13
  Description: 
  + Trade completed: Stock symbol: [symbol name] Total Bought = [double value] Total Sold = [double value]
  Bought Shares = [share volume] Sold Shares = [share volume]
  */
  strcpy(TradeOutputLogMapTable[logTypeIndex], "sffdd");
  logTypeIndex++;
  
  /*
  Log Type:
  + 14
  Description: 
  + [Profit/Loss] - $[double value] [symbol name]
  */
  strcpy(TradeOutputLogMapTable[logTypeIndex], "dfs");
  logTypeIndex++;
  
  /*
  Log Type:
  + 15
  Description: 
  + Stuck([the order of stuck]): [LONG/SHORT] [share Volume] [symbol name]. 
  Best price [MaxBid/MinAsk] I saw was [share price]
  */
  strcpy(TradeOutputLogMapTable[logTypeIndex], "dddsdf");
  logTypeIndex++;
}

/****************************************************************************
- Function name:  InitTradeOutputLogMgmt
****************************************************************************/
int InitTradeOutputLogMgmt(void)
{
  // Initialize Trade Output Log collection
  memset(TradeOutputLogMgmt.tradeOutputLogCollection, 0, MAX_LOG_ENTRY * MAX_LEN_PER_LOG_ENTRY);
  
  // Load trade Output Log from file
  TradeOutputLogMgmt.countLogEntry = 0;
  TradeOutputLogMgmt.countSentLogEntry = -1;
  TradeOutputLogMgmt.countSavedLogEntry = 0;
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadTradeOutputLogFromFile
****************************************************************************/
int LoadTradeOutputLogFromFile(void)
{
  char fullTradeOutputLogPath[MAX_PATH_LEN];
  Environment_get_data_filename(FN_TRADE_OUTPUT_LOG, fullTradeOutputLogPath, MAX_PATH_LEN);
  // Open or create new file pointer to Trade Output Log file for current day
  if (IsFileTimestampToday(fullTradeOutputLogPath) == 0)
  {
    TraceLog(ERROR_LEVEL, "'%s' is old, please check correctness of data files\n", fullTradeOutputLogPath);
    exit(0);
  }
  
  TradeOutputLogMgmt.fileDesc = fopen(fullTradeOutputLogPath, "a+");
  
  if (TradeOutputLogMgmt.fileDesc != NULL)
  {
    // Read all log entry in file
    while (fread(TradeOutputLogMgmt.tradeOutputLogCollection[TradeOutputLogMgmt.countLogEntry % MAX_LOG_ENTRY], MAX_LEN_PER_LOG_ENTRY, 1, TradeOutputLogMgmt.fileDesc) != 0)
    {
      TradeOutputLogMgmt.tradeOutputLogCollection[TradeOutputLogMgmt.countLogEntry % MAX_LOG_ENTRY][MAX_LEN_PER_LOG_ENTRY - 1] = YES;
        
      TradeOutputLogMgmt.countLogEntry++;
    }
        
    // Now, number of saved log Entry is same as number of log entry in buffer
    TradeOutputLogMgmt.countSavedLogEntry = TradeOutputLogMgmt.countLogEntry;
  }
  else
  {
    TraceLog(ERROR_LEVEL, "Critical! Could not open/create file to store Trade Output Log information (%s). Trade Server could not continue.\n", fullTradeOutputLogPath);
    PrintErrStr(ERROR_LEVEL, "calling fopen(): ", errno);
    sleep(3); exit(0);
    return ERROR;
  }
  
  TraceLog(DEBUG_LEVEL, "TradeOutputLogMgmt.countLogEntry = %d\n", TradeOutputLogMgmt.countLogEntry);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  DetectCrossTrade
- Input:      + newOrder
              + side
              + stock Symbol Index
- Output:     N/A
- Return:     value > 0 : value is reference number which is self-crossed
              value = 0 : Detected cross with another books
              value = -1 : Cross was not detected with another books
- Description:
- Usage:      
****************************************************************************/
int DetectCrossTrade(t_OrderDetail *newOrder, int newOrderSide, int stockSymbolIndex, int ecnLaunchID, long timeStamp, int reason)
{
  if (SymbolMgmt.symbolList[stockSymbolIndex].isStale[ecnLaunchID] != 0) return ERROR;
  
  if (TradingCollection[stockSymbolIndex].isTrading == YES) return ERROR;
  
  if (SymbolStatus[stockSymbolIndex].isDisable == YES) return ERROR;
  
  // make sure the symbol isn't locked because another trade server is already trading it
  if (SymbolMgmt.symbolList[stockSymbolIndex].isTempDisabled == 1) return ERROR;
  
  // If ecnLaunchID is disabled, we should skip detect cross trade
  if (AlgorithmConfig.bookRole.status[ecnLaunchID] == DISABLE) return ERROR;
  
  t_TopOrder bestQuote, bestInsideQuote;
  
  double currentSpread, insideSpread;
  double maxSpread;
  int crossID;
  
  /*
  Get best quote from the opposite side of the incoming quote.
  If side == ASK_SIDE then we will get best quote on BID_SIDE
  otherwise if side == BID_SIDE then we will get best quote on ASK_SIDE
  */
  GetBestOrder(&bestQuote, !newOrderSide, stockSymbolIndex);
  
  // if there is no best quote on the other side
  if ((bestQuote.ecnIndicator == -1) || (bestQuote.sharePrice < 0.01)) return ERROR;
  
  // Calculate current spread
  currentSpread = (newOrderSide == BID_SIDE)?(newOrder->sharePrice - bestQuote.sharePrice):(bestQuote.sharePrice - newOrder->sharePrice);
  
  // if the best quote from the opposite side is on the same venue and the spread is greater than 0 we are self crossed
  if ((ecnLaunchID == bestQuote.ecnIndicator) && (fge(currentSpread, 0.00)))
  {
    TraceLog(WARN_LEVEL, "consumer thread %d, venue_id %d self-crossed: %s Launch (%d@%lf) vs Best (%d@%lf), symbol: %.8s\n", 
                          _consumerId, ecnLaunchID, (newOrderSide == ASK_SIDE) ? "ASK":"BID", newOrder->shareVolume, 
                          newOrder->sharePrice, bestQuote.shareVolume, bestQuote.sharePrice, SymbolMgmt.symbolList[stockSymbolIndex].symbol);
    return TRADE_DETECTED_SELF_CROSSED;
  }
  
  /*Skip cross trade detect when 1 or both side is disconnected
  - BestQuote side: Book and Order ports
  - NewOrder side: Book and Order ports
  */
  if ((BookStatusMgmt[(int)bestQuote.ecnIndicator].isConnected == DISCONNECTED) || 
      (BookStatusMgmt[ecnLaunchID].isConnected == DISCONNECTED))
  {
    return ERROR;
  }
  
  // Check current spread with Min Spread
  if (currentSpread >= AlgorithmConfig.globalConfig.minSpread)
  {
    // Get new cross Id
    crossID = GetNewCrossID();
  
    if (AlgorithmConfig.bookMaxSpread.status[ecnLaunchID] == ENABLE)
    {
      /*
      Check maximum spread exceeding - Lucas's comment:
      (BEST BID - BEST ASK) / BEST BID if we are entering with a sell, 
      or (BEST BID - BEST ASK) / BEST ASK if we are entering with a buy  
      
      This ensures that we apply our max spread logic to the actual inside and not just the crossed price of the incoming quote,
      as we will be price improved to the inside
      */      
      double bestInsidePrice = newOrder->sharePrice;
      insideSpread = currentSpread;
      
      GetBestOrder(&bestInsideQuote, newOrderSide, stockSymbolIndex);
      
      if (bestInsideQuote.ecnIndicator != -1)
      {
        if (newOrderSide == BID_SIDE)
        {
          if (fgt(bestInsideQuote.sharePrice, bestInsidePrice))
          {
            bestInsidePrice = bestInsideQuote.sharePrice;
          }
          
          insideSpread = bestInsidePrice - bestQuote.sharePrice;
        }
        else
        {
          if (flt(bestInsideQuote.sharePrice, bestInsidePrice))
          {
            bestInsidePrice = bestInsideQuote.sharePrice;
          }
          
          insideSpread = bestQuote.sharePrice - bestInsidePrice;
        }
      }
      
      // compute max spread based on inside price (some percentage)
      maxSpread = GetMaxSpread(bestInsidePrice, stockSymbolIndex);
                                                                   
      // if our inside spread exceeds the max spread, give up
      if (fgt(insideSpread, maxSpread))
      {
        /*  No trading in the case
          + Maximum spread exceeded
          + Trade: Stock Symbol [symbol name], [NASDAQ/ARCA/STAND_ALONE] Server, [ARCA/NASDAQ/NYSE/BATSZ] [Ask/Bid] Launch (ECN of Best Quote) [Trade type]
        */
        SymbolMgmt.symbolList[stockSymbolIndex].isTempDisabled = 1;
        TraceLog(DEBUG_LEVEL, "Maximum spread exceeded, symbol %.8s, side %d, bestInsidePrice %lf, bestOppositePrice %lf, insideSpread %lf, maxSpread %lf\n", 
                              SymbolMgmt.symbolList[stockSymbolIndex].symbol, newOrderSide, 
                              bestInsidePrice, bestQuote.sharePrice, 
                              insideSpread, maxSpread);
        
        AddTradeOutputLogEntry_04080911(timeStamp * 1000, 4, crossID);
        AddTradeOutputLogEntry_02(SymbolMgmt.symbolList[stockSymbolIndex].symbol, timeStamp * 1000, timeStamp, crossID, ecnLaunchID, newOrderSide, bestQuote.ecnIndicator, reason);
        AddTradeOutputLogEntry_03(timeStamp * 1000, newOrder->sharePrice, crossID, newOrder->shareVolume, newOrderSide, ecnLaunchID);
        AddTradeOutputLogEntry_03(timeStamp * 1000, bestQuote.sharePrice, crossID, bestQuote.shareVolume, !newOrderSide, bestQuote.ecnIndicator);
        
        return SUCCESS;
      }
    }
    
    // Convert ecnBookID to ecnOrderID
    int tmpEcnLaunchID = MdcToOecMapping[ecnLaunchID];
    
    if ( (*(TradingStatus.status[tmpEcnLaunchID]) == ENABLE) && 
         (OrderStatusMgmt[tmpEcnLaunchID].isConnected == CONNECTED)
       )
    {
      // Verify that we are allowed to launch on the ECN that is sending us the crossing quote
      if (AlgorithmConfig.entryOrderPlacement[AlgorithmConfig.globalConfig.serverRole].status[ecnLaunchID] != ENABLE)
      {
        /* Show trade detected only, do not trade
           Also, mark this symbol disabled due to trading contention
           Another Trade Server will trade this cross
        */
        SymbolMgmt.symbolList[stockSymbolIndex].isTempDisabled = 1;
        //TraceLog(DEBUG_LEVEL, "Symbol %.8s is locked for trade contention ecnLaunchID: %d\n", SymbolMgmt.symbolList[stockSymbolIndex].symbol, ecnLaunchID);
        AddTradeOutputLogEntry_02(SymbolMgmt.symbolList[stockSymbolIndex].symbol, timeStamp * 1000, timeStamp, crossID, ecnLaunchID, newOrderSide, bestQuote.ecnIndicator, reason);
        AddTradeOutputLogEntry_03(timeStamp * 1000, newOrder->sharePrice, crossID, newOrder->shareVolume, newOrderSide, ecnLaunchID);
        AddTradeOutputLogEntry_03(timeStamp * 1000, bestQuote.sharePrice, crossID, bestQuote.shareVolume, !newOrderSide, bestQuote.ecnIndicator);
        return SUCCESS;
      }
      
      if (CrossTradeMgmt(newOrder, newOrderSide, stockSymbolIndex, ecnLaunchID, crossID, bestQuote.ecnIndicator) == ERROR) {
        return ERROR;
      }
    }
    else
    {
      /* Report trading was disabled
       This side of the trade is not yet ready. We cannot trade 
       TraceLog(DEBUG_LEVEL, "This side of the trade is not yet ready. We can not trade\n"); */
      
      AddTradeOutputLogEntry_04080911(timeStamp * 1000, 8, crossID);
    }
    
    /*
    + Trade: Stock Symbol [symbol name], [NASDAQ/ARCA/BATS/STAND_ALONE] Server, [ARCA/NASDAQ/NYSE/BATS] [Ask/Bid] Launch (ECN of Best Quote) [Trade type]
    + [ASK/BID]: [shareVolume] [ARCA/NASDAQ/NYSE/BATS] at [share Price]
    + [ASK/BID]: [shareVolume] [ARCA/NASDAQ/NYSE/BATS] at [share Price]
    */
    
    AddTradeOutputLogEntry_02(SymbolMgmt.symbolList[stockSymbolIndex].symbol, timeStamp * 1000, timeStamp, crossID, ecnLaunchID, newOrderSide, bestQuote.ecnIndicator, reason);
    AddTradeOutputLogEntry_03(timeStamp * 1000, newOrder->sharePrice, crossID, newOrder->shareVolume, newOrderSide, ecnLaunchID);
    AddTradeOutputLogEntry_03(timeStamp * 1000, bestQuote.sharePrice, crossID, bestQuote.shareVolume, !newOrderSide, bestQuote.ecnIndicator);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  CrossTradeMgmt
****************************************************************************/
int CrossTradeMgmt(t_OrderDetail *newOrder, int newOrderSide, int stockSymbolIndex, int ecnLaunchID, int crossID, int bestQuoteIndex)
{
  t_AggregatedOrder bestQuotes[MAX_ECN_BOOK]; //Aggregated shares till a specific price
  memset(bestQuotes, 0, sizeof(t_AggregatedOrder) * MAX_ECN_BOOK);
  
  long time = hbitime_micros() * 1000+1;
  
  int bestIndex = -1;
  int totalSharesAggregated = AggregateTotalSharesAtEntry(newOrder, bestQuotes, !newOrderSide, stockSymbolIndex, ecnLaunchID, &bestIndex);
  
  if (bestIndex < 0)
  {
    if (bestIndex == -2)
    {                                      
      /* Other side of the trade is not yet ready */
      AddTradeOutputLogEntry_04080911(time, 9, crossID);
    }
    else if (totalSharesAggregated < 0)
    {
      TraceLog(ERROR_LEVEL, "CrossID (%d): Invalid shares aggregated (%d)\n", crossID, totalSharesAggregated);
    }
    
    return ERROR;
  }
  
  TradingCollection[stockSymbolIndex].entryCounter = 0;
  TradingCollection[stockSymbolIndex].exitCounter = 0;
  
  EnterCrossTrade(newOrder, bestQuotes, bestIndex, totalSharesAggregated, newOrderSide, ecnLaunchID, stockSymbolIndex, crossID);

  int i;
  for (i = 0; i < MAX_ECN_BOOK; i++)
  {
    if ((bestQuotes[i].ecnIndicator != -1) && (i != bestQuoteIndex) &&
      (bestQuotes[i].shareBest > 0) && (bestQuotes[i].priceBest > 0.000001))
    {
      AddTradeOutputLogEntry_03(time, bestQuotes[i].priceWorse, crossID, bestQuotes[i].totalShare, !newOrderSide, bestQuotes[i].ecnIndicator);
    }
  }
  
  return SUCCESS;
} 

int EnterCrossTrade(t_OrderDetail *newOrder, t_AggregatedOrder *bestQuotes, int bestIndex, int totalSharesAggregated, int newOrderSide, int ecnLaunchID, int stockSymbolIndex, int crossID)
{ 
  /*
  Calculate maximum size we can place based on buying power and risk limits
  */
  
  long globalOrderSizeLimit = AlgorithmConfig.globalConfig.maxShareVolume;  

  /*
    If we are entering a trade, then the market is crossed and the BID is greater than the ASK
    
    If the new order is on the bid side, our max committed on entry would be on a sell to the bidder (new order),
    and get price improved to the new best bid
    
    old quote: ASK 400 @ $30.00
    new quote: BID 400 @ $30.06
    
    we might SELL to the BIDDER at $30.00 + $.04 = $30.04 and get FILLED at $30.06

    If the new order is on the ask side, our max committed on entry would be on a sell to the bidder (opposite side),
    and get price improved to the best bid on the opposite side
    
    old quote: BID 400 @ $40.00
    new quote: ASK 400 @ $39.00
    
    we might SELL to the BIDDER at $39.00 + $.04 = $39.04 and get FILLED at $40.00  
  */                                                                              
  
  double pessimisticFillPrice = newOrderSide == BID_SIDE ? newOrder->sharePrice : bestQuotes[bestIndex].priceBest;
  long potentialDirectedSize = ((MAX_EXCHANGE - FIRST_AWAY_EXCHANGE) * 100);
  long maxEntrySize = TSCurrentStatus.buyingPower/(long)(pessimisticFillPrice * 100.0);  
  long orderSizeLimit = MIN(globalOrderSizeLimit, (maxEntrySize - potentialDirectedSize));
  
  if (orderSizeLimit < 1)
  {    
    TraceLog(ERROR_LEVEL, "Buying power constrained, will not enter position, buyingPower = $%f\n", (double)TSCurrentStatus.buyingPower / 100.0);
    return SUCCESS;
  }
  
  int newSideSizePlaceable = MIN(newOrder->shareVolume, orderSizeLimit);
  int oppositeSideSizePlaceable = MIN(totalSharesAggregated, orderSizeLimit); 
  
  char   isEntryMultiLevel = NO;             
  int    entryExchange = -1;
  int    entrySide = -1;   
  int    entrySize = 0;
  int    visibleSize = 0;
  double entryPrice = 0.0; 

  t_OrderPlacement orders;
  orders.index = 0;
  int ecnIndex;
  
  /* 
    determine entry side
    if launch size is greater than or equal to the old size by the given ratio, we will enter
    on the old side (potentially multi-level)
  */
  
  if (fge((double)newSideSizePlaceable, (double)oppositeSideSizePlaceable * AlgorithmConfig.globalConfig.exRatio) == 1)
    entrySide = OPPOSITE_SIDE(newOrderSide);
  else
    entrySide = newOrderSide;
  
  int switchedEntryDueToETB = 0;
  long time;
  if (entrySide == BID_SIDE && SymbolMgmt.symbolList[stockSymbolIndex].shortSaleStatus != SHORT_SALE_ALLOWED)
  {
    // always enter on the Buy side if we aren't allowed to short sell, multi-level might apply
    entrySide = ASK_SIDE;
    switchedEntryDueToETB = 1;
    time = hbitime_micros() * 1000;
  }
  
  if (entrySide == OPPOSITE_SIDE(newOrderSide))
  {
    entryPrice = (entrySide == ASK_SIDE) ? newOrder->sharePrice - AlgorithmConfig.globalConfig.minSpread
                                         : newOrder->sharePrice + AlgorithmConfig.globalConfig.minSpread;
    
    if (bestQuotes[bestIndex].totalShare < oppositeSideSizePlaceable)
    {                      
      // determine multiple entry sizes and exchanges later
      isEntryMultiLevel = YES;
    }
    else
    {
      entryExchange     = bestIndex; 
      visibleSize       = bestQuotes[bestIndex].totalShare;
      entrySize         = newSideSizePlaceable;
    }
  } 
  else
  {
    // if old size is larger by configured ratio, we will enter on the new side (single order)    
    entryExchange = ecnLaunchID;
    visibleSize   = newSideSizePlaceable;
    entrySize     = oppositeSideSizePlaceable;
    entryPrice    = entrySide == ASK_SIDE ? bestQuotes[bestIndex].priceWorse - AlgorithmConfig.globalConfig.minSpread
                                          : bestQuotes[bestIndex].priceWorse + AlgorithmConfig.globalConfig.minSpread;
  }
  
  /***************************************************************************************************************************
    NOTICES RELATED TO NASDAQ AUCTION:
    - At specified timeframe (right before Market Open, NASDAQ has its auction period, this algorithm is applied)
    - If NASDAQ Launch, NASDAQ Entry is dected, will not trade
  *******************************************************************************************************************/
  if (IsInNasdaqAuction == YES && ecnLaunchID == BOOK_NASDAQ && entryExchange == BOOK_NASDAQ)
  {
    DEVLOGF("Cross ID %d: Nasdaq Launch/Nasdaq Entry not traded during Nasdaq Auction\n", crossID);
    return SUCCESS;
  }
    
   
  t_TradingInfo *tInfo = &TradingCollection[stockSymbolIndex];
  if (tInfo->lockedSide != NO_LOCK)
  {
    if (tInfo->lockedSide >= SHORT_LOCK && entrySide == BID_SIDE) // We cannot sell, not enough info to short mark
    {
      //Symbol is stuck (short) and being processed in AS/PM, so wait for it to finish
      TraceLog(DEBUG_LEVEL, "Detected cross for %.8s, however it's locked (SHORT locked) for stuck resolving\n", SymbolMgmt.symbolList[stockSymbolIndex].symbol);
      return SUCCESS;
    }
    else if(tInfo->lockedSide <= LONG_LOCK && entrySide == ASK_SIDE) //Free to short sell, currently we have Short Shell at entry only, no Sell
    {
      //Symbol is stuck and being processed in AS/PM, so wait for it to finish
      TraceLog(DEBUG_LEVEL, "Detected cross for %.8s, however it's locked (LONG locked) for stuck resolving\n", SymbolMgmt.symbolList[stockSymbolIndex].symbol);
      return SUCCESS;
    }
  }

  int bboId = BBOCounterList[crossID % MAX_CROSS_ID]++;

  if (isEntryMultiLevel == NO)
  {   
    // single-level entry 
    t_OrderPlacementInfo *order = &(orders.order[orders.index++]);   
    memset(order, 0, sizeof(t_OrderPlacementInfo));
    
    order->placementResult = -1;  
    order->crossID = crossID;
    order->orderID = GetNewOrderID();
    order->visibleSize = visibleSize;
    order->shareVolume = entrySize;
    order->ecnToPlaceID = MdcToOecMapping[entryExchange];
    order->side = Side[order->ecnToPlaceID][!entrySide];  
    order->tradeAction = entrySide == ASK_SIDE ? TRADE_ACTION_BUY : TRADE_ACTION_SELL;
    order->ecnAskBidLaunch = EcnHasAskBidLaunchCollection[ecnLaunchID][newOrderSide];
    order->sharePrice = entryPrice;
    order->symbol = SymbolMgmt.symbolList[stockSymbolIndex].symbol;
    order->stockSymbolIndex = stockSymbolIndex;
    order->bboId = bboId;
    order->isRoutable = NO;
    
    tInfo->crossID = crossID;
    tInfo->side = !entrySide;
  }
  else
  {
    // multi-level entry  (might be risk limited!!), do not assign more than desiredSize (risk adjusted)
    int desiredSize = newSideSizePlaceable;    
    
    if(desiredSize - oppositeSideSizePlaceable < 100)   
    {      
      bestQuotes[bestIndex].shareToPlace = MIN(desiredSize, bestQuotes[bestIndex].totalShare); 
      desiredSize -= bestQuotes[bestIndex].shareToPlace;
      
      for (ecnIndex = 0; ecnIndex < MAX_ECN_BOOK; ecnIndex++)
      {     
        if(ecnIndex != bestIndex && bestQuotes[ecnIndex].ecnIndicator != -1 && desiredSize > 0)
        {    
          bestQuotes[ecnIndex].shareToPlace = MIN(desiredSize, bestQuotes[ecnIndex].totalShare);        
          desiredSize -= bestQuotes[ecnIndex].totalShare;
        }
      }
    }
    else
    {
      double priceScore[MAX_ECN_BOOK];    
      double totalPriceScore = 0.00; 
      int extraSize = desiredSize - oppositeSideSizePlaceable;
       
      // determine cross amounts and total oversize score
      for (ecnIndex = 0; ecnIndex < MAX_ECN_BOOK; ecnIndex++)
      {
        if (bestQuotes[ecnIndex].ecnIndicator != -1)
        {
          double crossAmount = (entrySide == BID_SIDE) ? (bestQuotes[ecnIndex].priceAverage - newOrder->sharePrice) : (newOrder->sharePrice - bestQuotes[ecnIndex].priceAverage);
          double shareWeightedCrossAmount = crossAmount * bestQuotes[ecnIndex].totalShare;
          priceScore[ecnIndex] = shareWeightedCrossAmount * AlgorithmConfig.bookOversizeScore.oversizeScore[ecnIndex]; 
          totalPriceScore += priceScore[ecnIndex];
        }
      }   
      
      // allocate shares
      double invertedTotalPriceScore = 1.0 / totalPriceScore;
      
      for (ecnIndex = 0; ecnIndex < MAX_ECN_BOOK; ecnIndex++)
      {
        if (bestQuotes[ecnIndex].ecnIndicator != -1)
        {
          double allocationPercentage = priceScore[ecnIndex] * invertedTotalPriceScore;
          int mixedLot = bestQuotes[ecnIndex].totalShare + (allocationPercentage * extraSize);
          
          bestQuotes[ecnIndex].shareToPlace = (mixedLot / 100) * 100;  
          desiredSize -=  bestQuotes[ecnIndex].shareToPlace;   
        }
      }  
      
      if(desiredSize >= 100) 
      {
        bestQuotes[bestIndex].shareToPlace += (desiredSize / 100) * 100; 
      }
    }

    // build the orders
    tInfo->crossID = crossID;
    tInfo->side = !entrySide;
    
    for (ecnIndex = 0; ecnIndex < MAX_ECN_BOOK; ecnIndex++)
    {
      if (bestQuotes[ecnIndex].ecnIndicator != -1 && bestQuotes[ecnIndex].shareToPlace > 0)
      {                     
        t_OrderPlacementInfo *order = &(orders.order[orders.index++]);   
        memset(order, 0, sizeof(t_OrderPlacementInfo));
        order->placementResult = -1;
        order->crossID = crossID;
        order->orderID = GetNewOrderID(); 
        order->ecnToPlaceID = MdcToOecMapping[ecnIndex];
        order->tradeAction = entrySide == ASK_SIDE ? TRADE_ACTION_BUY : TRADE_ACTION_SELL;
        order->side = Side[order->ecnToPlaceID][!entrySide];
        order->ecnAskBidLaunch = EcnHasAskBidLaunchCollection[ecnLaunchID][newOrderSide];
        order->symbol = SymbolMgmt.symbolList[stockSymbolIndex].symbol;
        order->stockSymbolIndex = stockSymbolIndex;
        order->bboId = bboId; 
        order->isRoutable = NO;
        order->visibleSize = bestQuotes[ecnIndex].totalShare; 
        order->shareVolume = bestQuotes[ecnIndex].shareToPlace; 
        order->sharePrice = entryPrice;                         
      }
    }
  } 
  
  t_BBOSnapShot BBOSnapShot[MAX_EXCHANGE];
  GetBBOSnapShotAtEntry(newOrder, newOrderSide, bestQuotes, stockSymbolIndex, ecnLaunchID, BBOSnapShot);
  int ret = PlaceISOFlight(stockSymbolIndex, crossID, bboId, REASON_CROSS_ENTRY, &orders, potentialDirectedSize, BBOSnapShot, NO, entryPrice);
  if (switchedEntryDueToETB == 1)
  {
    // Add the trade output log type 5 here, so the trade will be counted on TWT
    double ssPrice;
    int ssVol;
    int ssVenue;
    if (entrySide == newOrderSide)  //the shortsell was on old side
    {
      ssPrice = (entrySide == ASK_SIDE) ? newOrder->sharePrice - AlgorithmConfig.globalConfig.minSpread
                                        : newOrder->sharePrice + AlgorithmConfig.globalConfig.minSpread;
      ssVol = newSideSizePlaceable;
      ssVenue = MdcToOecMapping[bestIndex];
    }
    else //the shortsell was on launch side
    {
      ssPrice = entrySide == ASK_SIDE ? bestQuotes[bestIndex].priceWorse - AlgorithmConfig.globalConfig.minSpread
                                      : bestQuotes[bestIndex].priceWorse + AlgorithmConfig.globalConfig.minSpread;
      ssVol = oppositeSideSizePlaceable;
      ssVenue = MdcToOecMapping[ecnLaunchID];
    }
    
    AddTradeOutputLogEntry_05(time, SymbolMgmt.symbolList[stockSymbolIndex].symbol, ssPrice, crossID, ssVol, ssVenue);
    TraceLog(DEBUG_LEVEL,"The symbol %.8s can not short sell: crossID = %d\n", SymbolMgmt.symbolList[stockSymbolIndex].symbol, crossID);
  }
  return ret;
}

inline char FlightContainsOrderForVenue(t_OrderPlacement *orders, int mdcIndex)
{
  int orderIndex;
  for (orderIndex = 0; orderIndex < orders->index; orderIndex++) 
  {          
    if(orders->order[orderIndex].ecnToPlaceID == MdcToOecMapping[mdcIndex])
    {
      return YES;
    }
  }
  
  return NO;
}

/*
  NB: PlaceISOFlight assumes that all orders in the flight will have the same limit price and the same side
*/
int PlaceISOFlight(int stockSymbolIndex, int crossID, int bboId, int reason, t_OrderPlacement *orders, int directedShareAllocation, t_BBOSnapShot BBOSnapShot[MAX_EXCHANGE], char routeOnFailure, double routePrice)
{    
  int ecnIndex, orderIndex;    
  int tradeAction = orders->order[0].tradeAction;
  double worstLimitPrice = orders->order[0].sharePrice;
                                           
  /*
    Look for better prices than our worst limit price at exchanges we are not connected to.
    We will use the directedShareAllocation parameter to determine how many away shares we are allowed to place.
    
    If the number of visible, better priced shares at away venues exceeds this allocation, we will send the flight non-ISO.
  */
  
  t_OrderPlacement directedOrders; 
  directedOrders.index = 0;
  int canSendISO = YES;
  long sharesPlaced = 0;
    
  for (ecnIndex = 0; ecnIndex < MAX_EXCHANGE; ecnIndex++) 
  { 
    char shouldPlaceDirectedOrder = NO;
    int directedOrderSize = 0;
    double directedOrderPrice = 0.0;
    
    if (tradeAction == TRADE_ACTION_BUY && BBOSnapShot[ecnIndex].offerSize > 0 && flt(BBOSnapShot[ecnIndex].offerPrice, worstLimitPrice))
    {
      if(ecnIndex < FIRST_AWAY_EXCHANGE)
      {
        if(FlightContainsOrderForVenue(orders, ecnIndex) == NO)
        {
          canSendISO = NO;
          break;          
        }
      }
      else if(BBOSnapShot[ecnIndex].offerSize > directedShareAllocation)
      {
        canSendISO = NO;
        break;
      }                  
      else
      { 
        directedOrderPrice = BBOSnapShot[ecnIndex].offerPrice;    
        directedOrderSize = BBOSnapShot[ecnIndex].offerSize;
        shouldPlaceDirectedOrder = YES;
      }
    } 
    else if(tradeAction == TRADE_ACTION_SELL && BBOSnapShot[ecnIndex].bidSize > 0 && fgt(BBOSnapShot[ecnIndex].bidPrice, worstLimitPrice))
    {
      if(ecnIndex < FIRST_AWAY_EXCHANGE)
      {
        if(FlightContainsOrderForVenue(orders, ecnIndex) == NO)
        {
          canSendISO = NO;
          break;          
        }
      }     
      else if(BBOSnapShot[ecnIndex].bidSize > directedShareAllocation)
      {
        canSendISO = NO;
        break;
      }
      else
      {
        directedOrderPrice = BBOSnapShot[ecnIndex].bidPrice;    
        directedOrderSize = BBOSnapShot[ecnIndex].bidSize;
        shouldPlaceDirectedOrder = YES;
      }
    }

    if(shouldPlaceDirectedOrder == YES)
    {
      t_OrderPlacementInfo *order = &(directedOrders.order[directedOrders.index++]); 
      memset(order, 0, sizeof(t_OrderPlacementInfo));
      
      order->placementResult = -1;       
      order->crossID = crossID;
      order->ecnAskBidLaunch = orders->order[0].ecnAskBidLaunch;
      order->ecnToPlaceID = ORDER_NASDAQ_RASH;
      order->side = tradeAction == TRADE_ACTION_SELL ? Side[ORDER_NASDAQ_RASH][ASK_SIDE] : Side[ORDER_NASDAQ_RASH][BID_SIDE];    
      order->tradeAction = tradeAction;
      order->orderID = GetNewOrderID();
      order->sharePrice = directedOrderPrice;     
      order->ISO_Flag = YES;
      order->visibleSize = directedOrderSize;
      order->shareVolume = directedOrderSize;
      order->ISO_Directed_Flag = ecnIndex;
      order->stockSymbolIndex = stockSymbolIndex;
      order->symbol = SymbolMgmt.symbolList[stockSymbolIndex].symbol;
      order->bboId = bboId;    
      order->isRoutable = NO;
      
      directedShareAllocation -= order->shareVolume;
    }
  } 
  
  if(CurrentMarketTimeType != INTRADAY)
  {                  
    // always send ISOs pre/post market  
    canSendISO = YES;
    directedOrders.index = 0;
  }
  
  if(canSendISO == YES && 
     ISO_Trading == ENABLE && 
     OrderStatusMgmt[ORDER_NASDAQ_RASH].isConnected &&  
     *(TradingStatus.status[ORDER_NASDAQ_RASH]) == ENABLE)
  {
    // send ISO flight
    for (orderIndex = 0; orderIndex < orders->index; orderIndex++) 
    {      
      t_OrderPlacementInfo *order = &orders->order[orderIndex];    
      order->ISO_Flag = YES;
      order->ISO_Directed_Flag = 0;        
      if(PlaceOrder(order, stockSymbolIndex, reason) == SUCCESS)
      {
        sharesPlaced += order->shareVolume;
      }
    }                                                           
    
    AddPlacementInfoForOrders(orders);
    
    // send directed ISO orders
    for (orderIndex = 0; orderIndex < directedOrders.index; orderIndex++) 
    {                                                                
      t_OrderPlacementInfo *order = &directedOrders.order[orderIndex];                                 
      if(PlaceOrder(order, stockSymbolIndex, reason) == SUCCESS)
      {
        sharesPlaced += order->shareVolume;
      }
    }
    
    AddPlacementInfoForOrders(&directedOrders);
    AddBBOInfoToFile(crossID, BBOSnapShot, bboId);
  }
  else
  {
    // If we cannot send the flight ISO, send the flight non-iso
    for (orderIndex = 0; orderIndex < orders->index; orderIndex++) 
    {                                                                
      t_OrderPlacementInfo *order = &orders->order[orderIndex];      
      order->ISO_Flag = NO;   
      order->ISO_Directed_Flag = 0; 
      
      if(routeOnFailure == YES)
      {
        // we should take all of our desired size and send to the intended exchange
        // routable with a (potentially) more aggressive limit price        
        order->isRoutable = YES;
        order->sharePrice = routePrice;        
      }
      
      if(PlaceOrder(order, stockSymbolIndex, reason) == SUCCESS)
      {
        sharesPlaced += order->shareVolume;
      }
    }   
    AddPlacementInfoForOrders(orders);
  } 
  
  if(sharesPlaced > 0)
  {                                    
    if(reason == REASON_CROSS_ENTRY) __sync_add_and_fetch(&TradingCounter, 1);      
    return SUCCESS;
  }  
  else
  {
    return ERROR;
  }
}

inline void ReturnBuyingPower(double amount)
{
  TSCurrentStatus.buyingPower += (long)(amount * 100.0);
  long maxBuyingPower = (long)(AlgorithmConfig.globalConfig.maxBuyingPower * 100.0);

  if(TSCurrentStatus.buyingPower > maxBuyingPower)
  {
    TSCurrentStatus.buyingPower = maxBuyingPower;
  }  
}

int ProcessAcceptResponse(t_ExecutionReport *status)
{
  int symbolIndex = status->stockSymbolIndex;
  t_TradingInfo *tradingInfo = &TradingCollection[symbolIndex];              
  t_OpenOrder *openOrder = &tradingInfo->openOrders[status->clientOrderID % MAX_OPEN_ORDERS];
                                                                    
  if(openOrder->orderID != status->clientOrderID)
  {
    TraceLog(ERROR_LEVEL, "Received accept response for unknown order id: %d\n", status->clientOrderID);
    return ERROR;
  } 
  
  status->entryExitRefNum = ++(tradingInfo->entryExitRefNum);
  
  switch(openOrder->reason)
  {
    case REASON_CROSS_EXIT:
      tradingInfo->exitSideHoldingShareVolume += (openOrder->shareVolume - status->acceptedShare);
      break;
  }   
  
  openOrder->shareVolume = status->acceptedShare;
  openOrder->sharePrice = status->acceptedPrice;

  return SUCCESS;
}

int ProcessRejectResponse(t_ExecutionReport *status)
{
  int symbolIndex = status->stockSymbolIndex;
  t_TradingInfo *tradingInfo = &TradingCollection[symbolIndex];
  t_OpenOrder *openOrder = &tradingInfo->openOrders[status->clientOrderID % MAX_OPEN_ORDERS];
                                                                    
  if(openOrder->orderID != status->clientOrderID)
  {
    TraceLog(ERROR_LEVEL, "Received reject response for unknown order id: %d\n", status->clientOrderID);
    return ERROR;
  }
  
  status->entryExitRefNum = ++(tradingInfo->entryExitRefNum);
  
  switch(openOrder->reason)
  {
    case REASON_CROSS_ENTRY:
      ReturnBuyingPower((double)openOrder->shareVolume * openOrder->sharePrice);
      tradingInfo->entryCounter--;
      break;
    case REASON_CROSS_EXIT:
      tradingInfo->exitSideHoldingShareVolume += openOrder->shareVolume;
      tradingInfo->exitCounter--;
      break;
  }
    
  openOrder->orderID = -1;  
  
  int bookId = OrderToBookMapping[status->ecnID];
  if (bookId == BOOK_NYSE)
  {
    // Need to know it's available on NYSE or AMEX
    bookId = SymbolMgmt.symbolList[symbolIndex].mdcId;
  }
  
  DeleteFromPrice(openOrder->sharePrice, bookId, !openOrder->side, symbolIndex);

  int crossId = tradingInfo->crossID;
  
  Liquidate(symbolIndex, tradingInfo, status);
    
  AddTradeOutputLogEntry_12(status->time, crossId, status->ecnID);
  
  return SUCCESS; 
}

int ProcessCancelResponse(t_ExecutionReport *status)
{
  int symbolIndex = status->stockSymbolIndex;
  t_TradingInfo *tradingInfo = &TradingCollection[symbolIndex];
  t_OpenOrder *openOrder = &tradingInfo->openOrders[status->clientOrderID % MAX_OPEN_ORDERS];
                                                                    
  if(openOrder->orderID != status->clientOrderID)
  {
    TraceLog(ERROR_LEVEL, "Received cancel response for unknown order id: %d\n", status->clientOrderID);
    return ERROR;
  }
  
  status->entryExitRefNum = ++(tradingInfo->entryExitRefNum);
  
  int bookId = OrderToBookMapping[status->ecnID];
  if (bookId == BOOK_NYSE)
  {
    // Need to know it's available on NYSE or AMEX
    bookId = SymbolMgmt.symbolList[symbolIndex].mdcId;
  }
  
  switch(openOrder->reason)
  {
    case REASON_CROSS_ENTRY:
      ReturnBuyingPower((double)openOrder->shareVolume * openOrder->sharePrice);  
      tradingInfo->entryCounter--;
      DeleteFromPrice(openOrder->sharePrice, bookId, !openOrder->side, symbolIndex);
      break;
    case REASON_CROSS_EXIT:
      tradingInfo->exitSideHoldingShareVolume += openOrder->shareVolume;
      tradingInfo->exitCounter--;
      if(openOrder->isISO == YES) DeleteFromPrice(openOrder->sharePrice, bookId, !openOrder->side, symbolIndex);
      break;
  }       
  
  openOrder->orderID = -1;
  
  int crossId = tradingInfo->crossID;
  
  if(openOrder->isRoutable == NO)
  {
    Liquidate(symbolIndex, tradingInfo, status);    
  } 
  else
  {
    CompleteCrossTrade(symbolIndex, status);
  }
  
  AddTradeOutputLogEntry_10(status->time, SymbolMgmt.symbolList[symbolIndex].symbol, openOrder->sharePrice, crossId, status->canceledShare, CANCELED, status->ecnID);
  
  return SUCCESS;
}

int ProcessFillResponse(t_ExecutionReport *status)
{
  int symbolIndex = status->stockSymbolIndex;
  t_TradingInfo *tradingInfo = &TradingCollection[symbolIndex];
  t_OpenOrder *openOrder = &tradingInfo->openOrders[status->clientOrderID % MAX_OPEN_ORDERS];
                                                                    
  if(openOrder->orderID != status->clientOrderID)
  {
    TraceLog(ERROR_LEVEL, "Received fill response for unknown order id: %d\n", status->clientOrderID);
    return ERROR;
  }
  
  status->entryExitRefNum = ++(tradingInfo->entryExitRefNum);
  openOrder->shareVolume -= status->filledShare; 
  
  switch(openOrder->reason)
  {
    case REASON_CROSS_ENTRY:    
      tradingInfo->filledEntryPrice = 
               (((double)tradingInfo->filledEntryShareVolume * tradingInfo->filledEntryPrice) + ((double)status->filledShare * status->filledPrice)) /
               (double)(tradingInfo->filledEntryShareVolume + status->filledShare);
      tradingInfo->filledEntryShareVolume += status->filledShare;    
      tradingInfo->exitSideHoldingShareVolume += status->filledShare; 
    
      if (openOrder->shareVolume < 1) tradingInfo->entryCounter--;
      break;
    case REASON_CROSS_EXIT:
      ReturnBuyingPower((double)status->filledShare * status->filledPrice);    
      if (openOrder->shareVolume < 1) tradingInfo->exitCounter--;
      break;
  }
  
  double filledValue = status->filledShare * status->filledPrice;
  
  if (openOrder->tradeAction == TRADE_ACTION_BUY)
  {
    tradingInfo->longShareVolume += status->filledShare;
    tradingInfo->totalBought += filledValue;
  }
  else if (openOrder->tradeAction == TRADE_ACTION_SELL)
  {
    tradingInfo->shortShareVolume += status->filledShare;
    tradingInfo->totalSold += filledValue;
  }
  else
  {
    TraceLog(ERROR_LEVEL, "ProcessFillResponse: Unknown trade action (expecting BUY or SELL): %d for orderId: %d\n", openOrder->tradeAction, openOrder->orderID);
  }
  
  int bookId = OrderToBookMapping[status->ecnID];
  if (bookId == BOOK_NYSE)
  {
    // Need to know it's available on NYSE or AMEX
    bookId = SymbolMgmt.symbolList[symbolIndex].mdcId;
  }
  
  DeleteBetterPrice(status->filledPrice, bookId, !openOrder->side, symbolIndex);     

  int crossId = tradingInfo->crossID;

  if(openOrder->coalesceFills == NO || openOrder->shareVolume < 1)
  {
    Liquidate(symbolIndex, tradingInfo, status);
  }

  if (openOrder->shareVolume < 1) openOrder->orderID = -1; 
  
  AddTradeOutputLogEntry_10(status->time, SymbolMgmt.symbolList[symbolIndex].symbol, status->filledPrice, crossId, status->filledShare, (openOrder->side == BID_SIDE) ? BOUGHT : SOLD, status->ecnID);

  return SUCCESS; 
}

char Tradable(int symbolIndex, int mdcId, int side)
{
  int oecId = MdcToOecMapping[mdcId];
  
  if((mdcId == BOOK_NYSE || mdcId == BOOK_AMEX) && SymbolMgmt.symbolList[symbolIndex].tradingStatusInNyseBook != NYSE_BOOK_TRADING_STATUS_OPENED)
  {
    if((SymbolMgmt.symbolList[symbolIndex].tradingStatusInNyseBook == NYSE_BOOK_SLOW_ON_ASK && side == ASK_SIDE) ||
       (SymbolMgmt.symbolList[symbolIndex].tradingStatusInNyseBook == NYSE_BOOK_SLOW_ON_BID && side == BID_SIDE))
    {
      return NO;
    }    
  } 
  else if (SymbolMgmt.symbolList[symbolIndex].haltStatus[mdcId] == TRADING_NORMAL &&
           AlgorithmConfig.bookRole.status[mdcId] == ENABLE && 
           OrderStatusMgmt[oecId].isConnected == CONNECTED &&
           *(TradingStatus.status[oecId]) == ENABLE &&
           SymbolMgmt.symbolList[symbolIndex].isStale[mdcId] == 0)
  {
    return YES;
  }
  
  return NO;
}

inline char BetterPrice(int side, double price, double bestPrice)
{
  if((side == BID_SIDE && fgt(price, bestPrice)) ||
     (side == ASK_SIDE && flt(price, bestPrice)))
  {
    return YES;
  }
  
  return NO;
}

inline char BetterQuote(int side, double price, int size, double bestPrice, int bestSize)
{
  if(bestSize == 0) 
  {
    return YES;
  }
  else if(feq(price, bestPrice) && size > bestSize)
  {
    return YES;
  }
  else if(BetterPrice(side, price, bestPrice) == YES)
  {
    return YES;
  }
  
  return NO;
}

double ComputeWorstLiquidatePrice(t_TradingInfo *tradingInfo)
{
  double maxLossPerShare = tradingInfo->filledEntryPrice * AlgorithmConfig.globalConfig.loserRate;  
  double maxPotentialLoss = maxLossPerShare * (double)tradingInfo->exitSideHoldingShareVolume;
  
  /*
    We set sane bounds on our max potential loss
  */
  if(fgt(maxPotentialLoss, 30000.00))
  {
    maxLossPerShare = 30000.00 / (double)tradingInfo->exitSideHoldingShareVolume;
  }
    
  double worstLiquidatePrice = tradingInfo->side == BID_SIDE ? tradingInfo->filledEntryPrice - maxLossPerShare
                                           : tradingInfo->filledEntryPrice + maxLossPerShare;
                                           
  return worstLiquidatePrice;  
}

static inline int EcnForRoutedLiquidate(t_ExecutionReport *status)
{
  if (status->time >= TradingSchedule.bzx_open && status->time <= TradingSchedule.bzx_close)
    return ORDER_BATSZ_BOE;
  return -1;
}
 
/****************************************************************************
- Function name:  RoutedLiquidate
- Input:      
- Output:   
- Return:   
- Description:  
- Usage:    
****************************************************************************/
int RoutedLiquidate(int symbolIndex, t_TradingInfo *tradingInfo, t_ExecutionReport *status)
{
  if (EcnForRoutedLiquidate(status) != ORDER_BATSZ_BOE)
  {
    TraceLog(WARN_LEVEL, "RoutedLiquidate: Could not liquidate, not in batsz trading time.\n");
    return ERROR;
  }
  
  if (SymbolMgmt.symbolList[symbolIndex].haltStatus[BOOK_BATSZ] != TRADING_NORMAL || 
           OrderStatusMgmt[ORDER_BATSZ_BOE].isConnected != CONNECTED ||
           *(TradingStatus.status[ORDER_BATSZ_BOE]) != ENABLE)
  {
    TraceLog(WARN_LEVEL, "RoutedLiquidate: Could not liquidate, no bats boe connection.\n");
    return ERROR;
  }
  
  int tradeAction = tradingInfo->side == ASK_SIDE ? TRADE_ACTION_BUY : TRADE_ACTION_SELL;
  double liquidatePrice = ComputeWorstLiquidatePrice(tradingInfo);
  int liquidateSize = tradingInfo->exitSideHoldingShareVolume;
  
  if(liquidateSize <= 0 || fle(liquidatePrice, 0.0))
  {
    TraceLog(WARN_LEVEL, "RoutedLiquidate: Attempted to liquidate %d shares at price: %f, for symbol %.8s\n", liquidateSize, liquidatePrice, SymbolMgmt.symbolList[symbolIndex].symbol);
    return ERROR;
  }  
  
  
  t_OrderPlacementInfo order;
  memset(&order, 0, sizeof(t_OrderPlacementInfo));
  
  order.placementResult = -1;       
  order.crossID = tradingInfo->crossID;
  order.ecnToPlaceID = ORDER_BATSZ_BOE;
  order.side = tradeAction == TRADE_ACTION_SELL ? Side[ORDER_BATSZ_BOE][ASK_SIDE] : Side[ORDER_BATSZ_BOE][BID_SIDE];    
  order.tradeAction = tradeAction;
  order.orderID = GetNewOrderID();
  order.sharePrice = liquidatePrice;      
  order.ISO_Flag = NO;
  order.ISO_Directed_Flag = 0;
  order.shareVolume = liquidateSize;
  order.stockSymbolIndex = symbolIndex;
  order.symbol = SymbolMgmt.symbolList[symbolIndex].symbol;
  order.bboId = BBOCounterList[tradingInfo->crossID % MAX_CROSS_ID]++;;    
  order.isRoutable = YES;
  order.entryExitRefNum = status->entryExitRefNum;
  
  int ret = PlaceOrder(&order, symbolIndex, REASON_CROSS_EXIT);
  
  AddPlacementInfoForOrder(&order); 
  
  return ret;   
}

/****************************************************************************
- Function name:  Liquidate
- Input:      
- Output:   
- Return:   
- Description:  
- Usage:    
****************************************************************************/
int Liquidate(int symbolIndex, t_TradingInfo *tradingInfo, t_ExecutionReport *status)
{                                                                                           
  /*
    We want to abort liquidation if we are holding < 30 shares and < $10,000 and there are still outstanding entry orders
  */                                                                                                                 
  double investedValue = (double)tradingInfo->exitSideHoldingShareVolume * tradingInfo->filledEntryPrice;
  if(tradingInfo->exitSideHoldingShareVolume < 30 &&
     flt(investedValue, 10000) &&
     tradingInfo->entryCounter > 0)
  {
    return SUCCESS;
  }
  
  /*
    We want to look at our invested size (from entry) - the size from our oustanding exit orders and build a set of orders equal to this remaining size      
    We never want to send an order that would cause us to lose more than .005 * this invested amount
  */
  
  int side = tradingInfo->side;  
  int tradeAction = side == ASK_SIDE ? TRADE_ACTION_BUY : TRADE_ACTION_SELL;
  
  int desiredSize = tradingInfo->exitSideHoldingShareVolume;     
  double worstLiquidatePrice = ComputeWorstLiquidatePrice(tradingInfo);
  
  int crossId = tradingInfo->crossID;  
  int bboId = BBOCounterList[crossId % MAX_CROSS_ID]++;                                                    
  t_OrderPlacement orders;
  orders.index = 0;
  
  int ecnIndex;
  t_QuoteList *books[MAX_ECN_BOOK];
  t_OrderPlacementInfo *orderReferences[MAX_ECN_BOOK];
  
  double limitPrice = 0.0;

  if(desiredSize <= 0)
  {
    return CompleteCrossTrade(symbolIndex, status);
  }                                                
  
  for(ecnIndex = 0; ecnIndex < MAX_ECN_BOOK; ecnIndex++)
  {
    orderReferences[ecnIndex] = NULL;
    if(Tradable(symbolIndex, ecnIndex, side) == YES)
    {
      books[ecnIndex] = StockBook[symbolIndex][ecnIndex][side];
    }
    else
    {
      books[ecnIndex] = NULL;
    }
  }
  
  while(desiredSize > 0)
  {
    int bestEcnIndex = -1;
           
    for(ecnIndex = 0; ecnIndex < MAX_ECN_BOOK; ecnIndex++)
    {
      if(books[ecnIndex] != NULL && 
         (bestEcnIndex == -1 || 
         BetterQuote(side, books[ecnIndex]->price, books[ecnIndex]->totalVolume, books[bestEcnIndex]->price, books[bestEcnIndex]->totalVolume) == YES))
      {     
        bestEcnIndex = ecnIndex;
      }
    }
    
    if(bestEcnIndex < 0)
    {
      TraceLog(WARN_LEVEL, "Liquidate: No books available for symbol %.8s, giving up\n", SymbolMgmt.symbolList[symbolIndex].symbol);
      if(orders.index > 0)
      {
        orders.order[0].shareVolume += desiredSize;
      }
      
      desiredSize = 0;
    }
    else if(BetterPrice(side, worstLiquidatePrice, books[bestEcnIndex]->price) == YES)
    {
      TraceLog(DEBUG_LEVEL, "Liquidate: No exit within max loss bounds for symbol %.8s, giving up and allocating remaining shares to ecn with the best price.\n", SymbolMgmt.symbolList[symbolIndex].symbol);
      if(orders.index > 0)
      {
        orders.order[0].shareVolume += desiredSize;
      }
      
      desiredSize = 0;
    }
    else
    {        
      int roundPortion = books[bestEcnIndex]->totalVolume / 100 * 100;      
      int sizeToPlace = MIN(roundPortion, desiredSize);               
      
      if(sizeToPlace == 0 && books[bestEcnIndex]->totalVolume >= desiredSize)
      {
        // if the best quote is an odd lot but has enough size for our exit order, use it
        sizeToPlace = desiredSize;
      }
      
      if(sizeToPlace > 0)
      {
        if(orderReferences[bestEcnIndex] == NULL)
        {
          orderReferences[bestEcnIndex] = &(orders.order[orders.index++]);
          memset(orderReferences[bestEcnIndex], 0, sizeof(t_OrderPlacementInfo));

          int oecId = MdcToOecMapping[bestEcnIndex];
          orderReferences[bestEcnIndex]->placementResult = -1;
          orderReferences[bestEcnIndex]->crossID = crossId;
          orderReferences[bestEcnIndex]->orderID = GetNewOrderID();   
          orderReferences[bestEcnIndex]->ecnToPlaceID = oecId;     
          orderReferences[bestEcnIndex]->tradeAction = tradeAction;
          orderReferences[bestEcnIndex]->side = Side[oecId][!side];
          orderReferences[bestEcnIndex]->symbol = SymbolMgmt.symbolList[symbolIndex].symbol;
          orderReferences[bestEcnIndex]->stockSymbolIndex = symbolIndex;
          orderReferences[bestEcnIndex]->bboId = bboId;
          orderReferences[bestEcnIndex]->isRoutable = NO;
          orderReferences[bestEcnIndex]->entryExitRefNum = status->entryExitRefNum;
        }

        orderReferences[bestEcnIndex]->visibleSize += roundPortion;
        orderReferences[bestEcnIndex]->shareVolume += sizeToPlace;
        orderReferences[bestEcnIndex]->sharePrice = books[bestEcnIndex]->price;
        limitPrice = books[bestEcnIndex]->price;        
      }

      desiredSize -= sizeToPlace;
       
      if(sizeToPlace >= roundPortion) books[bestEcnIndex] = books[bestEcnIndex]->next;
    }
  }
  
  
  /* 
    we have moved the pointer to the next price level on every exchange
    that we will be sending orders to   
    
    the best price we find remaining will be the most aggressive limit price we
    can set without trading through any protected quotes
  */                      
  
  if(orders.index > 0)
  {
    t_BBOSnapShot BBOSnapShot[MAX_EXCHANGE];
    GetBBOSnapShot(symbolIndex, BBOSnapShot);
     
    /* 
    double bestPrice = limitPrice;
    
    for (ecnIndex = FIRST_AWAY_EXCHANGE; ecnIndex < MAX_EXCHANGE; ecnIndex++) 
    { 
      if (tradeAction == TRADE_ACTION_BUY && BBOSnapShot[ecnIndex].offerSize > 0 &&  BetterPrice(side, BBOSnapShot[ecnIndex].offerPrice, bestPrice) == YES)
      {
        bestPrice = BBOSnapShot[ecnIndex].offerPrice;
      } 
      else if(tradeAction == TRADE_ACTION_SELL && BBOSnapShot[ecnIndex].bidSize > 0 && BetterPrice(side, BBOSnapShot[ecnIndex].bidPrice, bestPrice) == YES)
      {
        bestPrice = BBOSnapShot[ecnIndex].bidPrice;
      }
    }
        
    for(ecnIndex = 0; ecnIndex < MAX_ECN_BOOK; ecnIndex++)
    { 
      while(books[ecnIndex] != NULL && books[ecnIndex]->totalVolume < 100) books[ecnIndex] = books[ecnIndex]->next; 
      
      if(books[ecnIndex] != NULL && 
         (BetterPrice(side, books[ecnIndex]->price, bestPrice) == YES))
      {                
        bestPrice = books[ecnIndex]->price;
      }
    } 

    if(bestEcnIndex >= 0 && BetterPrice(side, bestPrice, worstLiquidatePrice) == YES)
    {
      limitPrice = bestPrice;
    }
    */
    
    int orderIndex;
    for(orderIndex = 0; orderIndex < orders.index; orderIndex++)
    {
      orders.order[orderIndex].sharePrice = limitPrice;
    } 

    if(PlaceISOFlight(symbolIndex, crossId, bboId, REASON_CROSS_EXIT, &orders, 0, BBOSnapShot, YES, worstLiquidatePrice) == SUCCESS)
    {
      return SUCCESS;
    }
    else
    {
      return CompleteCrossTrade(symbolIndex, status);
    }
  }
  else
  {
    return CompleteCrossTrade(symbolIndex, status);
  }
  
}


/****************************************************************************
- Function name:  ShortMark
- Description: Takes a pointer to an order and marks it short if necessary
****************************************************************************/
void ShortMark(t_OrderPlacementInfo *order, int symbolIndex)
{       
  t_TradingInfo *tradingInfo = &TradingCollection[symbolIndex];
  int ecnId = order->ecnToPlaceID;
  
  if(order->side == Side[ecnId][ASK_SIDE] && 
    tradingInfo->shortShareVolume >= tradingInfo->longShareVolume)
  {
    order->side = Side[ecnId][SHORT_SIDE];
  }
}

/****************************************************************************
- Function name:  RiskApprove
- Description:    Check that order does not violate risk rules
- Return:         SUCCESS if approved, ERROR if sending the order would violate any risk rules
****************************************************************************/
int RiskApprove(t_OrderPlacementInfo *order, int symbolIndex)
{   
  t_TradingInfo *tradingInfo = &TradingCollection[symbolIndex];
  
  if(order->shareVolume > AlgorithmConfig.globalConfig.maxShareVolume)
  {
    TraceLog(ERROR_LEVEL, "RiskApprove: Attempted to exceed max order size on symbol %.8s: crossID = %d, orderID = %d, maxOrderSize = %d, orderSize = %d, orderPrice = %f\n", 
    order->symbol, order->crossID, order->orderID, AlgorithmConfig.globalConfig.maxShareVolume, order->shareVolume, order->sharePrice);  
    return ERROR;
  } 
  
  if(order->shareVolume < 0)
  {
    TraceLog(ERROR_LEVEL, "RiskApprove: Attempted to send order with negative size on symbol %.8s: crossID = %d, orderID = %d, orderSize = %d, orderPrice = %f\n",
    order->symbol, order->crossID, order->orderID, order->shareVolume, order->sharePrice);  
    return ERROR;
  }         
  
  if(flt(order->sharePrice, 0.0))
  {
    TraceLog(ERROR_LEVEL, "RiskAPprove: Attempted to send order with negative price on symbol %.8s: crossID = %d, orderID = %d, orderSize = %d, orderPrice = %f\n",
    order->symbol, order->crossID, order->orderID, order->shareVolume, order->sharePrice);  
    return ERROR;
  }
  
  if(order->side == Side[order->ecnToPlaceID][SHORT_SIDE] && SymbolMgmt.symbolList[symbolIndex].shortSaleStatus != SHORT_SALE_ALLOWED)
  {                     
    // todo: this should change to error level when we fix the logic to buy first when we cannot short
    TraceLog(DEBUG_LEVEL, "RiskApprove: The symbol %.8s can not short sell: crossID = %d, orderID = %d\n", order->symbol, order->crossID, order->orderID);    
    order->placementResult = 5;
    return ERROR;
  }       
  
  if(order->side == Side[order->ecnToPlaceID][SHORT_SIDE] ||
     (order->side == Side[order->ecnToPlaceID][BID_SIDE] && tradingInfo->longShareVolume >= tradingInfo->shortShareVolume))
  {
    // if we are selling short, or we are buying and we are currently long, we are opening
    // our position and need a buying power check
    long requiredBuyingPower = (order->shareVolume * order->sharePrice * 100.0);
    if(__sync_sub_and_fetch(&(TSCurrentStatus.buyingPower), requiredBuyingPower) < 0)
    {
      TraceLog(ERROR_LEVEL, "RiskApprove: Attempted to exceed buying power on symbol %.8s: crossID = %d, orderID = %d, buyingPower = $%f, orderSize = %d, orderPrice = %f\n", 
                                                order->symbol, order->crossID, order->orderID, (double)TSCurrentStatus.buyingPower/100.0, order->shareVolume, order->sharePrice);
      TSCurrentStatus.buyingPower += requiredBuyingPower;
      return ERROR;
    }    
  }                
  
  return SUCCESS;
}  


/****************************************************************************
- Function name:  PlaceOrder
- Input:      
- Output:         N/A
- Return:         ERROR or SUCCESS
****************************************************************************/
int PlaceOrder(t_OrderPlacementInfo *order, int symbolIndex, int reason)
{ 
  t_TradingInfo *tradingInfo = &TradingCollection[symbolIndex];
                          
  order->placementResult = -1;  
  
  int placeResult = ERROR;
  int tradeAction = order->side == Side[order->ecnToPlaceID][ASK_SIDE] ? TRADE_ACTION_SELL : TRADE_ACTION_BUY;

  ShortMark(order, symbolIndex);                                                              
  
  if(RiskApprove(order, symbolIndex) == SUCCESS)
  {  
    switch (order->ecnToPlaceID)
    {
      case ORDER_ARCA_DIRECT:         
        placeResult = BuildAndSend_ARCA_DIRECT_OrderMsg(order);
        break;

      case ORDER_NYSE_CCG: 
        placeResult = BuildAndSend_NYSE_CCG_OrderMsg(order);
        break;

      case ORDER_NASDAQ_OUCH:                 
      case ORDER_NASDAQ_BX:
      case ORDER_NASDAQ_PSX:
        placeResult = BuildAndSend_Ouch_OrderMsg(order->ecnToPlaceID, order);
        break;

      case ORDER_BATSZ_BOE:
      case ORDER_BYX_BOE:
        placeResult = BuildAndSend_BATS_BOE_OrderMsg(order->ecnToPlaceID, order);     
        break;

      case ORDER_EDGX: 
      case ORDER_EDGA:
        placeResult = BuildAndSend_EDGE_Order(order);     
        break; 
        
      case ORDER_NASDAQ_RASH: 
        placeResult = BuildAndSend_NASDAQ_RASH_OrderMsg(order);
        break;
        
      default:
        TraceLog(ERROR_LEVEL, "PlaceOrder: Unknown ecnEntryID (%d)\n", order->ecnToPlaceID);
        break;
    }
    
    if(placeResult == ERROR)
    {
      TraceLog(ERROR_LEVEL, "Failed to send order for symbol %.8s: crossID = %d, orderID = %d, ecnId = %d\n", order->symbol, order->crossID, order->orderID, order->ecnToPlaceID);          
      if(reason == REASON_CROSS_ENTRY) 
      {
        ReturnBuyingPower((double)order->shareVolume * order->sharePrice);
      }
    }    
  }
  
  if (placeResult == SUCCESS)
  {   
    t_OpenOrder *openOrderInfo = &TradingCollection[symbolIndex].openOrders[order->orderID % MAX_OPEN_ORDERS];
    openOrderInfo->stockSymbolIndex = symbolIndex;
    openOrderInfo->orderID = order->orderID;
    openOrderInfo->shareVolume = order->shareVolume;
    openOrderInfo->sharePrice = order->sharePrice;
    openOrderInfo->tradeAction = tradeAction;
    openOrderInfo->side = tradeAction == TRADE_ACTION_SELL ? ASK_SIDE : BID_SIDE;
    openOrderInfo->reason = reason;
    openOrderInfo->isRoutable = order->isRoutable;
    openOrderInfo->isISO = order->ISO_Flag;
    
    switch(order->ecnToPlaceID)
    {
      case ORDER_NASDAQ_OUCH:
      case ORDER_NASDAQ_BX:
      case ORDER_NASDAQ_PSX:
      case ORDER_EDGX:
      case ORDER_EDGA:
        openOrderInfo->coalesceFills = YES;
        break;
      default:
        openOrderInfo->coalesceFills = NO;
        break;
    }
      
    order->placementResult = 10;  
    order->outputLogSide = tradeAction == TRADE_ACTION_SELL ? SELL : BUY;
    
    switch(reason)
    {
      case REASON_CROSS_ENTRY:
        TradingCollection[symbolIndex].isTrading = YES;     
        tradingInfo->entryCounter++;
        break;
      case REASON_CROSS_EXIT:
        tradingInfo->exitSideHoldingShareVolume -= order->shareVolume;
        tradingInfo->exitCounter++;
        break;
    }
    
    return SUCCESS;
  }
  else
  { 
    *((long*)&order->dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]) = hbitime_micros() * 1000;    
    return ERROR;
  } 
}


/****************************************************************************
- Function name:  CompleteCrossTrade
****************************************************************************/
int CompleteCrossTrade(const int stockSymbolIndex, t_ExecutionReport *status)
{
  t_TradingInfo *tradingInfo = &TradingCollection[stockSymbolIndex];
  t_Symbol *symbolInfo = &SymbolMgmt.symbolList[stockSymbolIndex];
  double profitGained = 0.0;

  if(tradingInfo->entryCounter > 0 || tradingInfo->exitCounter > 0)
  {
    return SUCCESS;
  }
  
  
  int _lockedSide = NO_LOCK;
  
  if (tradingInfo->longShareVolume > tradingInfo->shortShareVolume) 
  {
    // Stuck Long
    _lockedSide = SHORT_LOCK;
  }
  else if (tradingInfo->shortShareVolume > tradingInfo->longShareVolume)
  {
    // Stuck Short
    _lockedSide = LONG_LOCK;
  }  
  
  if(_lockedSide != NO_LOCK)
  {
    if(tradingInfo->routedLiquidateAttempted == NO)
    {
      tradingInfo->routedLiquidateAttempted = YES;
      if(RoutedLiquidate(stockSymbolIndex, tradingInfo, status) == SUCCESS)
      {
        return SUCCESS;
      }
    }
    
    AddTradeOutputLogEntry_13(hbitime_micros() * 1000, symbolInfo->symbol, tradingInfo->totalBought, tradingInfo->totalSold, tradingInfo->crossID, tradingInfo->longShareVolume, tradingInfo->shortShareVolume);
    
    if (++TSCurrentStatus.numStuck >= AlgorithmConfig.globalConfig.maxStuckPosition)
    {
      TraceLog(ERROR_LEVEL, "Exceeded maximum number of sequential stuck positions: %d >= %d\n", TSCurrentStatus.numStuck, AlgorithmConfig.globalConfig.maxStuckPosition);
      // Disable Trading
      int ecnIndex, isAlert = 0;
      for (ecnIndex = 0; ecnIndex < MAX_ECN_ORDER; ecnIndex++)
      {
        if (*(TradingStatus.status[ecnIndex]) == ENABLE)
        {
          *(TradingStatus.status[ecnIndex]) = DISABLE;
          isAlert = 1;
        }
      }
      
      //notify AS that trading is disabled
      if (ASConfig.socket != -1 && isAlert == 1)
      {
        if (BuildSendTradingDisabled(SERVICE_ALL_OECs, TRADING_DISABLED_EXCEED_MAX_STUCK_LIMIT) == SUCCESS)
        {
          TraceLog(DEBUG_LEVEL, "Notify trading disabled of all OECs is sent to AS\n");
        }
      }
    }
    
    t_TopOrder bestQuote;
      
    GetBestOrder(&bestQuote, tradingInfo->side, stockSymbolIndex);
    
    switch(_lockedSide)
    {
      case SHORT_LOCK:
        AddTradeOutputLogEntry_15(hbitime_micros() * 1000, symbolInfo->symbol, bestQuote.sharePrice, tradingInfo->crossID, TSCurrentStatus.numStuck, tradingInfo->longShareVolume - tradingInfo->shortShareVolume, MAX_BID, LONG);
        break;
      case LONG_LOCK:
        AddTradeOutputLogEntry_15(hbitime_micros() * 1000, symbolInfo->symbol, bestQuote.sharePrice, tradingInfo->crossID, TSCurrentStatus.numStuck, tradingInfo->shortShareVolume - tradingInfo->longShareVolume, MIN_ASK, SHORT);
        break;
      default:
        break;
    }    
  }
  else
  {
    // Not Stuck
    AddTradeOutputLogEntry_13(hbitime_micros() * 1000, symbolInfo->symbol, tradingInfo->totalBought, tradingInfo->totalSold, tradingInfo->crossID, tradingInfo->longShareVolume, tradingInfo->shortShareVolume);
    
    profitGained = (tradingInfo->totalSold - tradingInfo->totalBought);
    
    if (fge(profitGained, 0.00))
    {               
      if (tradingInfo->totalBought == 0.00)
      {        
        // Missed the trade       
        AddTradeOutputLogEntry_04080911(hbitime_micros() * 1000, 11, tradingInfo->crossID);
      }
      else
      {    
        // Profit
        AddTradeOutputLogEntry_14(hbitime_micros() * 1000, symbolInfo->symbol, profitGained, tradingInfo->crossID, PROFIT);
      }
      TSCurrentStatus.numLoser = 0;       
    }
    else
    {
      // Loss
      AddTradeOutputLogEntry_14(hbitime_micros() * 1000, symbolInfo->symbol, profitGained, tradingInfo->crossID, LOSS);
      
      TSCurrentStatus.numLoser++;
      if (TSCurrentStatus.numLoser >= AlgorithmConfig.globalConfig.maxConsecutiveLoser)
      {
        // Disable Trading
        int ecnIndex, isAlert = 0;
        for (ecnIndex = 0; ecnIndex < MAX_ECN_ORDER; ecnIndex++)
        {
          if (*(TradingStatus.status[ecnIndex]) == ENABLE)
          {
            *(TradingStatus.status[ecnIndex]) = DISABLE;
            isAlert = 1;
          }
        }
        
        //notify AS that trading is disabled
        if (ASConfig.socket != -1 && isAlert == 1)
        {
          if (BuildSendTradingDisabled(SERVICE_ALL_OECs, TRADING_DISABLED_EXCEED_MAX_LOSS_LIMIT) == SUCCESS)
          {
            TraceLog(DEBUG_LEVEL, "Notify trading disabled of all OECs is sent to AS\n");
          }
        }
      }
      
    }
  }
  
  if (_lockedSide != NO_LOCK)
  {
    TraceLog(DEBUG_LEVEL, "Trade completed with a stuck, so %.8s is now locked (%s)\n", symbolInfo->symbol, (_lockedSide == SHORT_LOCK)?"SHORT LOCK":"LONG LOCK");
    tradingInfo->lockedSide += _lockedSide;
  }
  
  /// Reset current trading info to default value
  tradingInfo->crossID = 0;
  tradingInfo->side = 0;
  tradingInfo->filledEntryPrice = 0.00;
  tradingInfo->totalBought = 0.00;
  tradingInfo->totalSold = 0.00;
  tradingInfo->filledEntryShareVolume = 0;
  tradingInfo->longShareVolume = 0;
  tradingInfo->shortShareVolume = 0;
  tradingInfo->exitSideHoldingShareVolume = 0;
  tradingInfo->routedLiquidateAttempted = NO;
  tradingInfo->isTrading = NO;
  __sync_sub_and_fetch(&TradingCounter, 1);     
  
  // Check to update current buying power
  if (IsUpdateBuyingPower == YES)
  {
    if (TradingCounter <= 0)
    {
      TSCurrentStatus.buyingPower = (long)(ReceivedBuyingPower * 100.0);
      IsUpdateBuyingPower = NO;
      TraceLog(DEBUG_LEVEL, "Current BP: %lf (updated)\n", ReceivedBuyingPower);
    }
  }
                                
  // you only double dip if you make money, and only on one side!
  if (_lockedSide == NO_LOCK && fgt(profitGained, 0.00))
  {
    if (SymbolStatus[stockSymbolIndex].isDisable == YES)
      return SUCCESS;
    
    // Check MinAsk for changes to trade
    t_TopOrder bestAskQuote;

    // Double Dipping
    GetBestServerRoleOrder(&bestAskQuote, ASK_SIDE, stockSymbolIndex, AlgorithmConfig.globalConfig.serverRole);
    
      
    if (bestAskQuote.ecnIndicator != -1)
    {
      t_OrderDetail order;
      order.shareVolume = bestAskQuote.shareVolume;
      order.sharePrice = bestAskQuote.sharePrice;
      return DetectCrossTrade(&order, ASK_SIDE, stockSymbolIndex, bestAskQuote.ecnIndicator, hbitime_micros(), REASON_CROSS_DD_ENTRY);
    }
    
    // Check MaxBid for changes to trade
    t_TopOrder bestBidQuote;

    // Double Dipping
    GetBestServerRoleOrder(&bestBidQuote, BID_SIDE, stockSymbolIndex, AlgorithmConfig.globalConfig.serverRole);
    
    if (bestBidQuote.ecnIndicator != -1)
    {
      t_OrderDetail order;
      order.shareVolume = bestBidQuote.shareVolume;
      order.sharePrice = bestBidQuote.sharePrice;
      return DetectCrossTrade(&order, BID_SIDE, stockSymbolIndex, bestBidQuote.ecnIndicator, hbitime_micros(), REASON_CROSS_DD_ENTRY);
    }
  } 
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  AddTradeOutputLogEntry_04080911
****************************************************************************/
void AddTradeOutputLogEntry_04080911(const uint64_t time, const int logType, int const crossID)
{
  uint8_t *logEntry;
  spsc_write_handle writer;
  int consumerId = _consumerId;
  if (consumerId != -1)
  {
    writer = FilterBuffers[consumerId].tradeOutputWriter;
    waitForSpscWritableItemsAvailable(writer, MAX_LEN_PER_LOG_ENTRY);
    logEntry = spscWritePtr(writer);
  }
  else
  {
    int logEntryIndex = __sync_fetch_and_add(&TradeOutputLogMgmt.countLogEntry, 1);
    logEntry = (uint8_t *)TradeOutputLogMgmt.tradeOutputLogCollection[logEntryIndex % MAX_LOG_ENTRY];
  }
  
  // Log Type
  logEntry[0] = logType;
  
  // Cross ID
  memcpy(&logEntry[1], &crossID, 4);
  
  // Timestamp
  *((uint64_t *) &logEntry[5]) = time;
  
  logEntry[MAX_LEN_PER_LOG_ENTRY - 1] = YES;
  
  if (consumerId != -1)
    spscCommitItemsWritten(writer, MAX_LEN_PER_LOG_ENTRY);
}

/****************************************************************************
- Function name:  AddTradeOutputLogEntry_02
****************************************************************************/
void AddTradeOutputLogEntry_02(const char *symbol, const uint64_t time, const long launchTime, const int crossID, const char ecnLaunchID, const char newOrderSide, const char ecnIndicator, const unsigned char reason)
{ 
  uint8_t *logEntry;
  spsc_write_handle writer;
  int consumerId = _consumerId;
  if (consumerId != -1)
  {
    writer = FilterBuffers[consumerId].tradeOutputWriter;
    waitForSpscWritableItemsAvailable(writer, MAX_LEN_PER_LOG_ENTRY);
    logEntry = spscWritePtr(writer);
  }
  else
  {
    int logEntryIndex = __sync_fetch_and_add(&TradeOutputLogMgmt.countLogEntry, 1);
    logEntry = (uint8_t *)TradeOutputLogMgmt.tradeOutputLogCollection[logEntryIndex % MAX_LOG_ENTRY];
  }

  // Log Type
  logEntry[0] = 2;
  
  // Cross ID
  memcpy(&logEntry[1], &crossID, 4);
  
  // Timestamp
  *((uint64_t *) &logEntry[5]) = time;
    
  //Symbol
  strncpy((char*)&logEntry[13], symbol, SYMBOL_LEN);
  
  //ServerRole
  logEntry[21] = AlgorithmConfig.globalConfig.serverRole;
  
  //ecnLaunchID
  logEntry[22] = ecnLaunchID;
  
  //newOrderSide  
  logEntry[23] = newOrderSide;
  
  //ecnIndicator
  logEntry[24] = ecnIndicator;
  
  //Time when quote is received
  memcpy(&logEntry[25], &launchTime, 8);
  
  logEntry[33] = reason;
  
  logEntry[MAX_LEN_PER_LOG_ENTRY - 1] = YES;
  
  if (consumerId != -1)
    spscCommitItemsWritten(writer, MAX_LEN_PER_LOG_ENTRY);
}

/****************************************************************************
- Function name:  AddTradeOutputLogEntry_03
****************************************************************************/
void AddTradeOutputLogEntry_03(const uint64_t time, const double sharePrice, const int crossID, const int shareVolume, const char side, const char venueId)
{ 
  uint8_t *logEntry;
  spsc_write_handle writer;
  int consumerId = _consumerId;
  if (consumerId != -1)
  {
    writer = FilterBuffers[consumerId].tradeOutputWriter;
    waitForSpscWritableItemsAvailable(writer, MAX_LEN_PER_LOG_ENTRY);
    logEntry = spscWritePtr(writer);
  }
  else
  {
    int logEntryIndex = __sync_fetch_and_add(&TradeOutputLogMgmt.countLogEntry, 1);
    logEntry = (uint8_t *)TradeOutputLogMgmt.tradeOutputLogCollection[logEntryIndex % MAX_LOG_ENTRY];
  }
  
  // Log Type
  logEntry[0] = 3;
  
  // Cross ID
  memcpy(&logEntry[1], &crossID, 4);
  
  // Timestamp
  *((uint64_t *) &logEntry[5]) = time;
   
  //side  
  logEntry[13] = side;
  
  //shareVolume
  memcpy(&logEntry[14], &shareVolume, 4);
  
  //venueId
  logEntry[18] = venueId;
  
  //sharePrice
  memcpy(&logEntry[19], &sharePrice, 8);
  
  logEntry[MAX_LEN_PER_LOG_ENTRY - 1] = YES;
  
  if (consumerId != -1)
    spscCommitItemsWritten(writer, MAX_LEN_PER_LOG_ENTRY);
}

/****************************************************************************
- Function name:  AddTradeOutputLogEntry_05
****************************************************************************/
void AddTradeOutputLogEntry_05(const uint64_t time, const char *symbol, const double sharePrice, const int crossID, const int shareVolume, const int venueId)
{ 
  uint8_t *logEntry;
  spsc_write_handle writer;
  int consumerId = _consumerId;
  if (consumerId != -1)
  {
    writer = FilterBuffers[consumerId].tradeOutputWriter;
    waitForSpscWritableItemsAvailable(writer, MAX_LEN_PER_LOG_ENTRY);
    logEntry = spscWritePtr(writer);
  }
  else
  {
    int logEntryIndex = __sync_fetch_and_add(&TradeOutputLogMgmt.countLogEntry, 1);
    logEntry = (uint8_t *)TradeOutputLogMgmt.tradeOutputLogCollection[logEntryIndex % MAX_LOG_ENTRY];
  }

  // Log Type
  logEntry[0] = 5;
  
  // Cross ID
  memcpy(&logEntry[1], &crossID, 4);
  
  // Timestamp
  *((uint64_t *) &logEntry[5]) = time;
  
  //shareVolume
  memcpy(&logEntry[13], &shareVolume, 4);

  //symbol
  strncpy((char*)&logEntry[17], symbol, SYMBOL_LEN);

  //sharePrice
  memcpy(&logEntry[25], &sharePrice, 8);

  //ecnOrderToPlaceID
  logEntry[33] = venueId;
    
  logEntry[MAX_LEN_PER_LOG_ENTRY - 1] = YES;
  
  if (consumerId != -1)
    spscCommitItemsWritten(writer, MAX_LEN_PER_LOG_ENTRY);
}

/****************************************************************************
- Function name:  AddTradeOutputLogEntry_12
****************************************************************************/
void AddTradeOutputLogEntry_12(const uint64_t time, const int crossID, const char ecnID)
{
  uint8_t *logEntry;
  spsc_write_handle writer;
  int consumerId = _consumerId;
  if (consumerId != -1)
  {
    writer = FilterBuffers[consumerId].tradeOutputWriter;
    waitForSpscWritableItemsAvailable(writer, MAX_LEN_PER_LOG_ENTRY);
    logEntry = spscWritePtr(writer);
  }
  else
  {
    int logEntryIndex = __sync_fetch_and_add(&TradeOutputLogMgmt.countLogEntry, 1);
    logEntry = (uint8_t *)TradeOutputLogMgmt.tradeOutputLogCollection[logEntryIndex % MAX_LOG_ENTRY];
  }
  
  // Log Type
  logEntry[0] = 12;
  
  // Cross ID
  memcpy(&logEntry[1], &crossID, 4);
  
  // Timestamp
  *((uint64_t *) &logEntry[5]) = time;
  
  // ecnID
  logEntry[13] = ecnID;

  logEntry[MAX_LEN_PER_LOG_ENTRY - 1] = YES;
  
  if (consumerId != -1)
    spscCommitItemsWritten(writer, MAX_LEN_PER_LOG_ENTRY);
}

/****************************************************************************
- Function name:  AddTradeOutputLogEntry_10
****************************************************************************/
void AddTradeOutputLogEntry_10(const uint64_t time, const char *symbol, const double sharePrice, const int crossID, const int shareVolume, const char action, const char ecnOrderToPlaceID)
{ 
  uint8_t *logEntry;
  spsc_write_handle writer;
  int consumerId = _consumerId;
  if (consumerId != -1)
  {
    writer = FilterBuffers[consumerId].tradeOutputWriter;
    waitForSpscWritableItemsAvailable(writer, MAX_LEN_PER_LOG_ENTRY);
    logEntry = spscWritePtr(writer);
  }
  else
  {
    int logEntryIndex = __sync_fetch_and_add(&TradeOutputLogMgmt.countLogEntry, 1);
    logEntry = (uint8_t *)TradeOutputLogMgmt.tradeOutputLogCollection[logEntryIndex % MAX_LOG_ENTRY];
  }

  // Log Type
  logEntry[0] = 10;
  
  // Cross ID
  memcpy(&logEntry[1], &crossID, 4);
  
  // Timestamp
  *((uint64_t *) &logEntry[5]) = time;
  
  //action
  logEntry[13] = action;

  //shareVolume
  memcpy(&logEntry[14], &shareVolume, 4);

  //symbol
  strncpy((char*)&logEntry[18], symbol, SYMBOL_LEN);

  //sharePrice
  memcpy(&logEntry[26], &sharePrice, 8);

  //ecnOrderToPlaceID
  logEntry[34] = ecnOrderToPlaceID;
  
  logEntry[MAX_LEN_PER_LOG_ENTRY - 1] = YES;
  
  if (consumerId != -1)
    spscCommitItemsWritten(writer, MAX_LEN_PER_LOG_ENTRY);
}

/****************************************************************************
- Function name:  AddTradeOutputLogEntry_13
****************************************************************************/
void AddTradeOutputLogEntry_13(const uint64_t time, const char *symbol, const double totalBought, const double totalSold, const int crossID, const int longShareVolume, const int shortShareVolume)
{ 
  uint8_t *logEntry;
  spsc_write_handle writer;
  int consumerId = _consumerId;
  if (consumerId != -1)
  {
    writer = FilterBuffers[consumerId].tradeOutputWriter;
    waitForSpscWritableItemsAvailable(writer, MAX_LEN_PER_LOG_ENTRY);
    logEntry = spscWritePtr(writer);
  }
  else
  {
    int logEntryIndex = __sync_fetch_and_add(&TradeOutputLogMgmt.countLogEntry, 1);
    logEntry = (uint8_t *)TradeOutputLogMgmt.tradeOutputLogCollection[logEntryIndex % MAX_LOG_ENTRY];
  }

  // Log Type
  logEntry[0] = 13;
  
  // Cross ID
  memcpy(&logEntry[1], &crossID, 4);
  
  // Timestamp
  *((uint64_t *) &logEntry[5]) = time;
  
  // symbol
    strncpy((char*)&logEntry[13], symbol, SYMBOL_LEN);
  
  // totalBought
  memcpy(&logEntry[21], &totalBought, 8);
  
  // totalSold
  memcpy(&logEntry[29], &totalSold, 8);
  
  // longShareVolume
  memcpy(&logEntry[37], &longShareVolume, 4);
  
  // shortShareVolume
  memcpy(&logEntry[41], &shortShareVolume, 4);
  
  logEntry[MAX_LEN_PER_LOG_ENTRY - 1] = YES;
  
  if (consumerId != -1)
    spscCommitItemsWritten(writer, MAX_LEN_PER_LOG_ENTRY);
}

/****************************************************************************
- Function name:  AddTradeOutputLogEntry_14
****************************************************************************/
void AddTradeOutputLogEntry_14(const uint64_t time, const char *symbol, const double profitGained, const int crossID, const char profitLoss)
{ 
  uint8_t *logEntry;
  spsc_write_handle writer;
  int consumerId = _consumerId;
  if (consumerId != -1)
  {
    writer = FilterBuffers[consumerId].tradeOutputWriter;
    waitForSpscWritableItemsAvailable(writer, MAX_LEN_PER_LOG_ENTRY);
    logEntry = spscWritePtr(writer);
  }
  else
  {
    int logEntryIndex = __sync_fetch_and_add(&TradeOutputLogMgmt.countLogEntry, 1);
    logEntry = (uint8_t *)TradeOutputLogMgmt.tradeOutputLogCollection[logEntryIndex % MAX_LOG_ENTRY];
  }

  // Log Type
  logEntry[0] = 14;
  
  // Cross ID
  memcpy(&logEntry[1], &crossID, 4);
  
  // Timestamp
  *((uint64_t *) &logEntry[5]) = time;
  
  // Profit/Loss
  logEntry[13] = profitLoss;

  // profitGained
  memcpy(&logEntry[14], &profitGained, 8);
  
  // symbol
  strncpy((char*)&logEntry[22], symbol, SYMBOL_LEN);
  
  logEntry[MAX_LEN_PER_LOG_ENTRY - 1] = YES;
  
  if (consumerId != -1)
    spscCommitItemsWritten(writer, MAX_LEN_PER_LOG_ENTRY);
}

/****************************************************************************
- Function name:  AddTradeOutputLogEntry_15
****************************************************************************/
void AddTradeOutputLogEntry_15(const uint64_t time, const char *symbol, const double sharePrice, const int crossID, const int numStuck, const int stuckShareVolume, const char askBidIndicator, const char stuckType)
{ 
  uint8_t *logEntry;
  spsc_write_handle writer;
  int consumerId = _consumerId;
  if (consumerId != -1)
  {
    writer = FilterBuffers[consumerId].tradeOutputWriter;
    waitForSpscWritableItemsAvailable(writer, MAX_LEN_PER_LOG_ENTRY);
    logEntry = spscWritePtr(writer);
  }
  else
  {
    int logEntryIndex = __sync_fetch_and_add(&TradeOutputLogMgmt.countLogEntry, 1);
    logEntry = (uint8_t *)TradeOutputLogMgmt.tradeOutputLogCollection[logEntryIndex % MAX_LOG_ENTRY];
  }

  // Log Type
  logEntry[0] = 15;
  
  // Cross ID
  memcpy(&logEntry[1], &crossID, 4);
  
  // Timestamp
  *((uint64_t *) &logEntry[5]) = time;
  
  // numStuck
  memcpy(&logEntry[13], &numStuck, 4);
  
  // stuckType
  logEntry[17] = stuckType;
  
  // stuckShareVolume,
  memcpy(&logEntry[18], &stuckShareVolume, 4);
  
  // symbol
  strncpy((char*)&logEntry[22], symbol, SYMBOL_LEN);
  
  // maxBid
  logEntry[30] = askBidIndicator;
  
  // sharePrice
  memcpy(&logEntry[31], &sharePrice, 8);

  logEntry[MAX_LEN_PER_LOG_ENTRY - 1] = YES;
  
  if (consumerId != -1)
    spscCommitItemsWritten(writer, MAX_LEN_PER_LOG_ENTRY);
}

/****************************************************************************
- Function name:  AddOrderPlacementInfoForSingleEntryOrder
****************************************************************************/
void AddPlacementInfoForOrder(t_OrderPlacementInfo *newOrder)
{
  if (newOrder->placementResult == 5)     //Could not short-sell
  {
    AddTradeOutputLogEntry_05(*((long*)&newOrder->dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]), newOrder->symbol, newOrder->sharePrice, newOrder->crossID, newOrder->shareVolume, newOrder->ecnToPlaceID);
  }
  else if (newOrder->placementResult == 10) //Placed order successfully
  {
    //Add rawdata
    t_DataBlock *dataBlock = &(newOrder->dataBlock);
                                              
    dataBlock->addContent[DATABLOCK_OFFSET_LAUNCH] = newOrder->ecnAskBidLaunch;       
    memcpy(&dataBlock->addContent[DATABLOCK_OFFSET_VISIBLE_SIZE], &(newOrder->visibleSize), 4);
    memcpy(&dataBlock->addContent[DATABLOCK_OFFSET_CROSS_ID], &(newOrder->crossID), 4);
    memcpy(&dataBlock->addContent[DATABLOCK_OFFSET_BBO_ID], &(newOrder->bboId), 2);   
    *(unsigned short *)&dataBlock->addContent[DATABLOCK_OFFSET_ENTRY_EXIT_REF] = newOrder->entryExitRefNum;
    
    switch (newOrder->ecnToPlaceID)
    {
      case ORDER_ARCA_DIRECT:
        dataBlock->msgLen = NEW_ORDER_MSG_LEN;
        Add_ARCA_DIRECT_DataBlockToCollection(dataBlock);
        break;                                        
        
      case ORDER_NYSE_CCG:
        dataBlock->msgLen = NYSE_NEW_ORDER_MSG_LEN + SYMBOL_LEN;  //Add extra 8 bytes for original symbol, since NYSE use CMS symbology
        Add_NYSE_CCG_DataBlockToCollection(dataBlock);
        break;
        
      case ORDER_NASDAQ_OUCH:
      case ORDER_NASDAQ_BX:
      case ORDER_NASDAQ_PSX:
        dataBlock->msgLen = OUCH_NEW_ORDER_MSG_LEN - 2;
        memcpy(dataBlock->msgContent, &dataBlock->msgContent[2], dataBlock->msgLen);        
        Add_Ouch_DataBlockToCollection(newOrder->ecnToPlaceID, dataBlock);
        break;
        
      case ORDER_BATSZ_BOE:
      case ORDER_BYX_BOE:
        Add_BATS_BOE_DataBlockToCollection(newOrder->ecnToPlaceID, dataBlock);
        break;                                         
        
      case ORDER_NASDAQ_RASH:
        dataBlock->msgLen = newOrder->ISO_Flag == YES ? NASDAQ_RASH_NEW_ORDER_CROSS_MSG_LEN
                                                      : NASDAQ_RASH_NEW_ORDER_MSG_LEN;
        Add_NASDAQ_RASH_DataBlockToCollection(dataBlock);
        break;
      
      case ORDER_EDGX:
      case ORDER_EDGA:
        Add_EDGE_DataBlockToCollection(newOrder->ecnToPlaceID, dataBlock);
        break;
    }
    
    //Add output log
    AddTradeOutputLogEntry_10(*((long*)&dataBlock->addContent[DATABLOCK_OFFSET_APP_TIME]), newOrder->symbol, newOrder->sharePrice, newOrder->crossID, newOrder->shareVolume, newOrder->outputLogSide, newOrder->ecnToPlaceID);
  }
}

/****************************************************************************
- Function name:  AddPlacementInfoForOrders
****************************************************************************/
void AddPlacementInfoForOrders(t_OrderPlacement *orders)
{
  int i;
  for (i = 0; i < orders->index; i++)
  {
    AddPlacementInfoForOrder(&(orders->order[i]));
  }
}


/****************************************************************************
- Function name:  GetNewCrossID
****************************************************************************/
inline int GetNewCrossID(void)
{
  return __sync_fetch_and_add(&CurrentCrossID, 1);
}

/****************************************************************************
- Function name:  GetNewOrderID
****************************************************************************/
inline int GetNewOrderID(void)
{
  return __sync_fetch_and_add(&CurrentOrderID, 1);
}

/****************************************************************************
- Function name:  GetMaxSpread
****************************************************************************/
double GetMaxSpread(double sharePrice, int stockSymbolIndex)
{
  if (SymbolStatus[stockSymbolIndex].isUsingMaxSpreadConfig == YES)
  {
    return AlgorithmConfig.globalConfig.maxSpread;
  }

  //- $0-$25.00 --------- 10% ----- 20%
  //- $25.01-$50.00 ----- 5% ------ 10%
  //- $50.01 and up ----- 3% ------ 6%
  
  //MaxSpread = 10% * price
  if (flt(sharePrice, 25.01))
  {
    return sharePrice * AlgorithmConfig.maxSpreadConfig.maxSpread[TradingSessionMgmt.CurrentSessionType].execPriceUnder_25USD;
  }
  //MaxSpread = 5% * price
  else if (flt(sharePrice, 50.01))
  {
    return sharePrice * AlgorithmConfig.maxSpreadConfig.maxSpread[TradingSessionMgmt.CurrentSessionType].execPriceUnder_50USD;
  }
  //MaxSpread = 3% * price
  else
  {
    return sharePrice * AlgorithmConfig.maxSpreadConfig.maxSpread[TradingSessionMgmt.CurrentSessionType].execPriceOver_50USD;
  }
}

/****************************************************************************
- Function name:  Thread_SaveTradingInformation
****************************************************************************/
void *Thread_SaveTradingInformation(void *threadArgs)
{
  SetKernelAlgorithm("[SAVE_TRADING_INFO]");
  
  int tempOrderID = 0;
  int tempCrossID = 0;
  int timeCounter = 0;
  tempOrderID = CurrentOrderID;
  tempCrossID = CurrentCrossID;
  
  while (1)
  {
    timeCounter++;
    if (timeCounter >= 10)
    {
      timeCounter = 0;
      if (tempOrderID != CurrentOrderID || tempCrossID != CurrentCrossID)
      {
        tempOrderID = CurrentOrderID;
        tempCrossID = CurrentCrossID;
        
        SaveOrderIDAndCrossIDFromFile("orderID_crossID", tempOrderID, tempCrossID);
      }
      
      SaveTradeOutputLogToFile();
      
      SaveBBOInfoToFile();
      
      if (IsTradeServerStopped == 1) return NULL;
    }
    
    usleep(100000); //sleep for 0.1 second
    
    // Update current market time
    UpdateCurrentMarketTime();
    
    // Update current session type
    UpdateCurrentSessionType();
  }

  return NULL;
}

/****************************************************************************
- Function name:  SaveTradeOutputLogToFile
*********************************************************************************/
int SaveTradeOutputLogToFile()
{
  if (TradeOutputLogMgmt.countLogEntry == 0)
  {
    return SUCCESS;
  }
    
  int i = 0;
  
  int tempCurrentLogEntryIndex = TradeOutputLogMgmt.countLogEntry;
  
  for (i = TradeOutputLogMgmt.countSavedLogEntry; i < tempCurrentLogEntryIndex; i++)
  {
    if (TradeOutputLogMgmt.tradeOutputLogCollection[i % MAX_LOG_ENTRY][MAX_LEN_PER_LOG_ENTRY - 1] == YES)
    {
      fwrite(TradeOutputLogMgmt.tradeOutputLogCollection[i % MAX_LOG_ENTRY], MAX_LEN_PER_LOG_ENTRY, 1, TradeOutputLogMgmt.fileDesc);
      
      TradeOutputLogMgmt.countSavedLogEntry++;
    }
    else
    {
      break;
    }
  }
  
  fflush(TradeOutputLogMgmt.fileDesc);
  
  return SUCCESS;
}

int GetLatestCrossIDFromOutputLog(int *crossID)
{
  *crossID = 0;
  
  char fullTradeOutputLogPath[MAX_PATH_LEN];
  Environment_get_data_filename(FN_TRADE_OUTPUT_LOG, fullTradeOutputLogPath, MAX_PATH_LEN);

  FILE *fileDesc = fopen(fullTradeOutputLogPath, "r");
  
  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Critical! Could not open file (%s). Trade Server could not continue.\n", fullTradeOutputLogPath);
    PrintErrStr(ERROR_LEVEL, "calling fopen(): ", errno);
    sleep(3); exit(0);
    return ERROR;
  }
  
  char buff[MAX_LEN_PER_LOG_ENTRY + 1];
  
  while (fread(buff, MAX_LEN_PER_LOG_ENTRY, 1, fileDesc) != 0)
  {
    memcpy(crossID, &buff[1], 4);
  }
  
  *crossID = (*crossID) + 1;
  fclose(fileDesc);
  return SUCCESS;
}


/****************************************************************************
- Function name:  UpdateCurrentMarketTime
- Input:      N/A
- Output:   
- Return:     N/A
- Description:  
- Usage:      N/A 
****************************************************************************/
void UpdateCurrentMarketTime(void)
{
  time_t now = hbitime_seconds();

  struct tm local;
  
  // Convert to local time
  localtime_r(&now, &local);
  int currentTimeInNumber = local.tm_hour * 3600 + local.tm_min * 60 + local.tm_sec;

  double currentMinSpread = AlgorithmConfig.globalConfig.minSpread;

  // PRE-MARKET
  if (currentTimeInNumber < TradingSchedule.timeToEndOfPreMarket)
  {
    if (CurrentMarketTimeType != PRE_MARKET)
    {
      CurrentMarketTimeType = PRE_MARKET;
    }
    AlgorithmConfig.globalConfig.minSpread = MinSpreadConf.preMarket;
  }
  // INTRADAY
  else if (currentTimeInNumber < TradingSchedule.timeToEndOfIntraday)
  {
    if (CurrentMarketTimeType != INTRADAY)
    {
      CurrentMarketTimeType = INTRADAY;
    }
    
    AlgorithmConfig.globalConfig.minSpread = MinSpreadInSecond[currentTimeInNumber];
  }
  // POST-MARKET
  else
  {
    if (CurrentMarketTimeType != POST_MARKET)
    {
      CurrentMarketTimeType = POST_MARKET;
    }
    AlgorithmConfig.globalConfig.minSpread = MinSpreadConf.postMarket;
  }
  
  if (AlgorithmConfig.globalConfig.minSpread != currentMinSpread)
  {
    if (ASConfig.socket != -1)
    {
      // After min_spread updating, send global configuration to AS
      BuildSend_Global_Configuration();
    }
  }
}

/****************************************************************************
- Function name:  UpdateCurrentSessionType
- Input:      N/A
- Output:   
- Return:     N/A
- Description:  
- Usage:      N/A 
****************************************************************************/
void UpdateCurrentSessionType(void)
{
  // Get current time
  time_t now = hbitime_seconds();

  struct tm local;
  
  // Convert to local time
  localtime_r(&now, &local);

  // Outside Regular session (PRE MARKET TIME)
  if ( (local.tm_hour < TradingSessionMgmt.RegularSessionBeginHour) ||
       ((local.tm_hour == TradingSessionMgmt.RegularSessionBeginHour) && (local.tm_min < TradingSessionMgmt.RegularSessionBeginMin)) )
  {
    if (TradingSessionMgmt.CurrentSessionType != PRE_MARKET)
    {
      TradingSessionMgmt.CurrentSessionType = PRE_MARKET;
    }
  }
  // Outside Regular session (POST MARKET TIME)
  else if ( (local.tm_hour > TradingSessionMgmt.RegularSessionEndHour) ||
            ((local.tm_hour == TradingSessionMgmt.RegularSessionEndHour) && (local.tm_min >= TradingSessionMgmt.RegularSessionEndMin)) )
  {
    if (TradingSessionMgmt.CurrentSessionType != POST_MARKET)
    {
      TradingSessionMgmt.CurrentSessionType = POST_MARKET;
    }
  }
  else  //Regular session (INTRADAY)
  {
    if (TradingSessionMgmt.CurrentSessionType != INTRADAY)
    {
      TradingSessionMgmt.CurrentSessionType = INTRADAY;
    }
  }
  
  int currentTimeInNumber = local.tm_hour * 3600 + local.tm_min * 60 + local.tm_sec;
  if ((AlgorithmConfig.globalConfig.nasdaqAuctionBeginTime <= currentTimeInNumber) &&
      (currentTimeInNumber < AlgorithmConfig.globalConfig.nasdaqAuctionEndTime))
  {
    if (IsInNasdaqAuction == NO)
      IsInNasdaqAuction = YES;
  }
  else
  {
    if (IsInNasdaqAuction == YES)
      IsInNasdaqAuction = NO;
  }
}

/****************************************************************************
- Function name:  InitBBOInfoMgmt
****************************************************************************/
int InitBBOInfoMgmt(void)
{
  // Initialize BBO quote collection
  memset(BBOInfoMgmt.BBOInfoCollection, 0, MAX_BBO_ENTRY * sizeof(t_BBOQuoteItem));
  
  // Load trade BBO quote from file
  BBOInfoMgmt.countBBOEntry = 0;  //Number of entries in collection
  BBOInfoMgmt.countSentBBOEntry = -1; //Number of entries sent
  BBOInfoMgmt.countSavedBBOEntry = 0;

  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadBBOInfoFromFile
****************************************************************************/
int LoadBBOInfoFromFile(void)
{
  char fullBBOQuotePath[MAX_PATH_LEN];
    
  // Build full data path
  Environment_get_data_filename(FN_BBO_INFO, fullBBOQuotePath, MAX_PATH_LEN);
  if (IsFileTimestampToday(fullBBOQuotePath) == 0)
  {
    TraceLog(ERROR_LEVEL, "'%s' is old, please check correctness of data files\n", fullBBOQuotePath);
    exit(0);
  }
  
  // Open or create new file pointer to BBO quote file for current day
  BBOInfoMgmt.fileDesc = fopen(fullBBOQuotePath, "a+");
  
  if (BBOInfoMgmt.fileDesc != NULL)
  {
    // Read all log entry in file
    while (fread(&BBOInfoMgmt.BBOInfoCollection[BBOInfoMgmt.countBBOEntry % MAX_BBO_ENTRY], sizeof(t_BBOQuoteItem), 1, BBOInfoMgmt.fileDesc) != 0)
    {
      BBOInfoMgmt.countBBOEntry++;
    }
        
    // Now, number of saved log Entry is same as number of log entry in buffer
    BBOInfoMgmt.countSavedBBOEntry = BBOInfoMgmt.countBBOEntry;
  }
  else
  {
    TraceLog(ERROR_LEVEL, "Critical! Could not open/create file to store BBO information (%s). Trade Server could not continue.\n", fullBBOQuotePath);
    PrintErrStr(ERROR_LEVEL, "calling fopen(): ", errno);
    sleep(3); exit(0);
    return ERROR;
  }
  
  TraceLog(DEBUG_LEVEL, "BBOInfoMgmt.countBBOEntry = %d\n", BBOInfoMgmt.countBBOEntry);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  AddBBOInfoToFile
- Input:    
- Output:   
- Return:   
- Description:  
- Usage:    
*********************************************************************************/
int AddBBOInfoToFile(int crossId, t_BBOSnapShot BBOSnapShot[MAX_EXCHANGE], int bboId)
{
  int index = __sync_fetch_and_add(&BBOInfoMgmt.countBBOEntry, 1) % MAX_BBO_ENTRY ;
  BBOInfoMgmt.BBOInfoCollection[index].crossID = -1;  //This is very important, acts as synchronization method with thread that sends BBO to AS
  memcpy(BBOInfoMgmt.BBOInfoCollection[index].BBOSnapShot, BBOSnapShot, MAX_EXCHANGE * sizeof(t_BBOSnapShot));
  BBOInfoMgmt.BBOInfoCollection[index].bboId = bboId;
  BBOInfoMgmt.BBOInfoCollection[index].crossID = crossId; //This line indicates that the item is now completely saved into memory
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SaveBBOInfoToFile
- Input:    
- Output:   
- Return:   
- Description:  
- Usage:    
*********************************************************************************/
int SaveBBOInfoToFile(void)
{ 
  int i, retry = 3;
  int willFlush = 0;
  int total;
  
RETRY:

  total = BBOInfoMgmt.countBBOEntry;
  for (i = BBOInfoMgmt.countSavedBBOEntry; i < total; i++)
  {
    if (BBOInfoMgmt.BBOInfoCollection[i % MAX_BBO_ENTRY].crossID != -1) //this item was completely saved into memory
    {
      fwrite(&BBOInfoMgmt.BBOInfoCollection[i % MAX_BBO_ENTRY], sizeof(t_BBOQuoteItem), 1, BBOInfoMgmt.fileDesc);
    
      BBOInfoMgmt.countSavedBBOEntry++;
      willFlush = 1;
    }
    else
    {
      if (retry > 0)
      {
        retry --; //We should try again
        goto RETRY;
      }
      
      break;  //This is not saved into memory, we should stop saving
    }
  }
  
  if (willFlush == 1)
  {
    fflush(BBOInfoMgmt.fileDesc);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ResetStatusForHungOrder
- Description:    Reset state for a specific order, in order to avoid trading
                  disabled on a symbol when TS sent order and venue did not
                  response anything
****************************************************************************/
int ResetStatusForHungOrder(const int clientOrderID, const char *stockSymbol)
{
  int stockSymbolIndex = GetStockSymbolIndex(stockSymbol);
  if (stockSymbolIndex == -1)
  {
    return SUCCESS;
  }
  
  t_TradingInfo *tradingInfo = &TradingCollection[stockSymbolIndex];
  
  // Reset open order
  t_OpenOrder *openOrder = &tradingInfo->openOrders[clientOrderID % MAX_OPEN_ORDERS];
  if (openOrder->orderID == clientOrderID)
  {
    openOrder->orderID = -1;
    
    int index, isCompleted = 1;
    for (index = 0; index < MAX_OPEN_ORDERS; index++)
    {
      if (TradingCollection[stockSymbolIndex].openOrders[index].orderID != -1)
      {
        isCompleted = 0;
        break;
      }
    }
    
    if (isCompleted)
    {
      // Reset current trading info to default value
      tradingInfo->crossID = 0;
      tradingInfo->side = 0;
      tradingInfo->filledEntryPrice = 0.00;
      tradingInfo->totalBought = 0.00;
      tradingInfo->totalSold = 0.00;
      tradingInfo->filledEntryShareVolume = 0;
      tradingInfo->longShareVolume = 0;
      tradingInfo->shortShareVolume = 0;
      tradingInfo->exitSideHoldingShareVolume = 0;
      tradingInfo->routedLiquidateAttempted = NO;
      tradingInfo->isTrading = NO;
      __sync_sub_and_fetch(&TradingCounter, 1);
    }
    
    TraceLog(DEBUG_LEVEL, "Handled Hung-Order request from AS - orderId: %d, symbol: %.8s\n", clientOrderID, stockSymbol);
  }
  
  return SUCCESS;
}
