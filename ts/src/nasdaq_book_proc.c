/****************************************************************************
** Project name:  Equity Arbitrage Application
** File name:     nasdaq_book_proc.c
** Description:   

** Author:    Danh Thai - Hoang
** First created on 29 September 2007
** Last updated on 01 October 2007
****************************************************************************/
#define _GNU_SOURCE

#include <pthread.h>
#include <time.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <math.h>
#include <semaphore.h>
     
#include "arca_book_proc.h"
#include "configuration.h"
#include "utility.h"
#include "nasdaq_book_proc.h"
#include "trading_mgmt.h"
#include "book_mgmt.h"
#include "kernel_algorithm.h"
#include "hbitime.h"

/****************************************************************************
            GLOBAL VARIABLES
****************************************************************************/
t_NASDAQ_Book_Conf NASDAQ_Book_Conf;
t_NASDAQ_Book_Conf NDBX_Book_Conf;
t_NASDAQ_Book_Conf PSX_Book_Conf;

extern t_MonitorNasdaqBook MonitorNasdaqBook;
extern t_MonitorNasdaqBook MonitorNdbxBook;
extern t_MonitorNasdaqBook MonitorPsxBook;

/****************************************************************************
            LIST OF FUNCTIONS
****************************************************************************/

/****************************************************************************
- Function name:  InitNASDAQ_BOOK_DataStructure
****************************************************************************/
void InitNASDAQ_BOOK_DataStructure(void)
{
  TraceLog(DEBUG_LEVEL, "Initialize NASDAQ Book...\n");
  
  MonitorNasdaqBook.recvCounter = 0;
  MonitorNasdaqBook.procCounter = 0;
  
  int i;
  for (i = 0; i < DBLFILTER_NUM_QUEUES; i++)
    NASDAQ_Book_Conf.lossCounter[i] = 0;
    
  // Initialize NASDAQ partial hashing
  int divisorIndex;
  int stockSymbolIndex;
  for (stockSymbolIndex = 0; stockSymbolIndex < MAX_STOCK_SYMBOL; stockSymbolIndex++)
  {
    for (divisorIndex = 0; divisorIndex < PRICE_HASHMAP_BUCKET_SIZE; divisorIndex++)
    {
      DeleteAllPricePointsFromHashSlot(Price_HashMap[stockSymbolIndex][BOOK_NASDAQ][ASK_SIDE][divisorIndex]);
      Price_HashMap[stockSymbolIndex][BOOK_NASDAQ][ASK_SIDE][divisorIndex] = NULL;
      
      DeleteAllPricePointsFromHashSlot(Price_HashMap[stockSymbolIndex][BOOK_NASDAQ][BID_SIDE][divisorIndex]);
      Price_HashMap[stockSymbolIndex][BOOK_NASDAQ][BID_SIDE][divisorIndex] = NULL;
    }
  }
}

/****************************************************************************
- Function name:  InitDataStructure_NDBX
****************************************************************************/
void InitDataStructure_NDBX(void)
{
  TraceLog(DEBUG_LEVEL, "Initialize BX Book...\n");
  
  MonitorNdbxBook.recvCounter = 0;
  MonitorNdbxBook.procCounter = 0;
  
  int i;
  for (i = 0; i < DBLFILTER_NUM_QUEUES; i++)
    NDBX_Book_Conf.lossCounter[i] = 0;
    
  // Initialize BX partial hashing
  int divisorIndex;
  int stockSymbolIndex;
  for (stockSymbolIndex = 0; stockSymbolIndex < MAX_STOCK_SYMBOL; stockSymbolIndex++)
  {
    for (divisorIndex = 0; divisorIndex < PRICE_HASHMAP_BUCKET_SIZE; divisorIndex++)
    {
      DeleteAllPricePointsFromHashSlot(Price_HashMap[stockSymbolIndex][BOOK_NDBX][ASK_SIDE][divisorIndex]);
      Price_HashMap[stockSymbolIndex][BOOK_NDBX][ASK_SIDE][divisorIndex] = NULL;
      
      DeleteAllPricePointsFromHashSlot(Price_HashMap[stockSymbolIndex][BOOK_NDBX][BID_SIDE][divisorIndex]);
      Price_HashMap[stockSymbolIndex][BOOK_NDBX][BID_SIDE][divisorIndex] = NULL;
    }
  }
}

/****************************************************************************
- Function name:  InitDataStructure_PSX
****************************************************************************/
void InitDataStructure_PSX(void)
{
  TraceLog(DEBUG_LEVEL, "Initialize PSX Book...\n");
  
  MonitorPsxBook.recvCounter = 0;
  MonitorPsxBook.procCounter = 0;
  
  int i;
  for (i = 0; i < DBLFILTER_NUM_QUEUES; i++)
    PSX_Book_Conf.lossCounter[i] = 0;
    
  // Initialize PSX partial hashing
  int divisorIndex;
  int stockSymbolIndex;
  for (stockSymbolIndex = 0; stockSymbolIndex < MAX_STOCK_SYMBOL; stockSymbolIndex++)
  {
    for (divisorIndex = 0; divisorIndex < PRICE_HASHMAP_BUCKET_SIZE; divisorIndex++)
    {
      DeleteAllPricePointsFromHashSlot(Price_HashMap[stockSymbolIndex][BOOK_PSX][ASK_SIDE][divisorIndex]);
      Price_HashMap[stockSymbolIndex][BOOK_PSX][ASK_SIDE][divisorIndex] = NULL;
      
      DeleteAllPricePointsFromHashSlot(Price_HashMap[stockSymbolIndex][BOOK_PSX][BID_SIDE][divisorIndex]);
      Price_HashMap[stockSymbolIndex][BOOK_PSX][BID_SIDE][divisorIndex] = NULL;
    }
  }
}

/****************************************************************************
- Function name:  NASDAQ_ProcessBookMessage
****************************************************************************/
int NASDAQ_ProcessBookMessage(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime, unsigned int timeStatusSeconds)
{
  if (BookStatusMgmt[ecn_book_id].isConnected == DISCONNECTED) return SUCCESS;  //If book is disconnected, we do not process remaining message in the buffer
  char messageType = *buffer;
  switch(messageType)
  {
    // Add order message
    case NDAQ_ADD_MSG:
      if(ProcessAddOrderMessage(ecn_book_id, symbolId, buffer, arrivalTime, timeStatusSeconds) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "%s Book: Problem in processing 'Add Order' msg\n", GetBookNameByIndex(ecn_book_id));
        return ERROR;
      }
      break;
    
    // Add order with MPID attribution message 
    case NDAQ_ADD_MPID_MSG:
      if(ProcessAddOrderMessage(ecn_book_id, symbolId, buffer, arrivalTime, timeStatusSeconds) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "%s Book: Problem in processing 'Add Order With MPID' msg\n", GetBookNameByIndex(ecn_book_id));
        return ERROR;
      }     
      break;

    // Order executed message
    case NDAQ_EXECUTED_MSG:
      if(ProcessExecutedOrderMessage(ecn_book_id, symbolId, buffer, arrivalTime, timeStatusSeconds) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "%s Book: Problem in processing 'Executed' msg\n", GetBookNameByIndex(ecn_book_id));
        return ERROR;
      }
      
      break;

    // Order executed with price message
    case NDAQ_EXECUTED_PRICE_MSG:
      if(ProcessExecutedOrderWithPriceMessage(ecn_book_id, symbolId, buffer, arrivalTime, timeStatusSeconds) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "%s Book: Problem in processing 'Executed with Price' msg\n", GetBookNameByIndex(ecn_book_id));
        return ERROR;
      }
      
      break;

    // Order cancel message
    case NDAQ_CANCEL_MSG:
      if(ProcessCancelOrderMessage(ecn_book_id, symbolId, buffer, arrivalTime, timeStatusSeconds) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "%s Book: Problem in processing 'Cancel Order' msg\n", GetBookNameByIndex(ecn_book_id));
        return ERROR;
      }
      
      break;
    
    // Order delete message
    case NDAQ_DELETE_MSG:
      if(ProcessDeleteOrderMessage(ecn_book_id, symbolId, buffer, arrivalTime, timeStatusSeconds) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "%s Book: Problem in processing 'Delete Order' msg\n", GetBookNameByIndex(ecn_book_id));
        return ERROR;
      }
      
      break;
    
    // Order replace message
    case NDAQ_REPLACE_MSG:
      if(ProcessReplaceOrderMessage(ecn_book_id, symbolId, buffer, arrivalTime, timeStatusSeconds) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "%s Book: Problem in processing 'Replace Order' msg\n", GetBookNameByIndex(ecn_book_id));
        return ERROR;
      }
      
      break;
    
    case NDAQ_STOCK_TRADING_ACTION_MSG:
      ProcessStockTradingActionMessage(symbolId, buffer, arrivalTime);
      break;
      
    case NDAQ_CROSS_TRADE_MSG:
      ProcessCrossTradeMessage(ecn_book_id, symbolId, buffer);
      break;
    case 'S': // System event message
    case 'R': // Stock directory message
    //case 'T': // Timestamp - Second message (Removed from ITCH 5.0)
    case 'L': // Market participant position message
    case 'P': // Trade message (Non-cross)
    case 'B': // Broken trade / Order exection message
    case 'I': // Net order imbalance indicator message
    case 'Y': // Reg SHO Short Sale Price Test Restricted Indicator
    case 'N': // Retail Price Improvement Indicator (RPII)
    case 'V': // MWCB Decline Level Message
    case 'W': // MWCB Status Message
    case 'K': // IPO Quoting Period Update
      break;
    default:
      TraceLog(ERROR_LEVEL, "%s Book: Unknown message type(%c - %d)\n", GetBookNameByIndex(ecn_book_id), messageType, messageType);
      break;
  }
  return SUCCESS;
}

/****************************************************************************
- Function name:  CheckNasdaqStaleData
****************************************************************************/
int CheckNasdaqStaleData(unsigned char ecn_book_id, char *buffer, int stockSymbolIndex, unsigned long arrivalTime, unsigned int timeStatusSeconds)
{
  unsigned long tmpNano = __builtin_bswap64(*(unsigned long*)(&buffer[5]));
  unsigned long nasdaqNanos = (tmpNano >> 16) + (TotalTimeOffsetInSeconds * TEN_RAISE_TO_9);  //Shift right 16bits to to convert from 8 to 6 bytes binary
  unsigned long arrivalNanos = arrivalTime % NANOSECOND_PER_DAY;
  long delay = arrivalNanos - nasdaqNanos;
  
  if (delay > DataDelayThreshold[ecn_book_id])
  {
    if (SymbolMgmt.symbolList[stockSymbolIndex].isStale[ecn_book_id] == 0)
    {
      SymbolMgmt.symbolList[stockSymbolIndex].isStale[ecn_book_id] = 1;
      TraceLog(WARN_LEVEL, "%s Book: '%.8s' data is delayed for %ld nanoseconds\n", GetBookNameByIndex(ecn_book_id), SymbolMgmt.symbolList[stockSymbolIndex].symbol, delay);
      BuildSendStaleMarketNotification(ecn_book_id, stockSymbolIndex, delay);
    }
  }
  else
  {
    if (SymbolMgmt.symbolList[stockSymbolIndex].isStale[ecn_book_id] == 0) return SUCCESS;
    
    if (SymbolMgmt.symbolList[stockSymbolIndex].isStale[ecn_book_id] == 1)
    {
      SymbolMgmt.symbolList[stockSymbolIndex].isStale[ecn_book_id] = 0;
      TraceLog(WARN_LEVEL, "%s Book: '%.8s' data is fast again\n", GetBookNameByIndex(ecn_book_id), SymbolMgmt.symbolList[stockSymbolIndex].symbol);
      BuildSendStaleMarketNotification(ecn_book_id, stockSymbolIndex, delay);
    }
    else
    {
      SymbolMgmt.symbolList[stockSymbolIndex].isStale[ecn_book_id] = 0;
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessAddOrderMessage
****************************************************************************/
int ProcessAddOrderMessage(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime, unsigned int timeStatusSeconds)
{
  CheckNasdaqStaleData(ecn_book_id, buffer, symbolId, arrivalTime, timeStatusSeconds);
  
  // Get order reference number
  t_OrderDetail NDAQ_Order;
  NDAQ_Order.refNum = __builtin_bswap64(*(unsigned long*)&buffer[11]);
    
  // Get buy sell indicator
  char buySellIndicator = buffer[19];
  
  // Get shares
  NDAQ_Order.shareVolume = __builtin_bswap32(*(unsigned int*)&buffer[20]);
  
  // Get Price
  NDAQ_Order.sharePrice = __builtin_bswap32(*(unsigned int*)&buffer[32]) * 0.0001;
  
  /*
  Detect cross trade before add new order to list
  */
  int side = (buySellIndicator == 'S')?ASK_SIDE:BID_SIDE;
  
  if (SymbolMgmt.symbolList[symbolId].haltStatus[ecn_book_id] == TRADING_NORMAL)
  {
    int tradeRet = DetectCrossTrade(&NDAQ_Order, side, symbolId, ecn_book_id, arrivalTime/1000, REASON_CROSS_ENTRY);
    
    if (tradeRet == TRADE_DETECTED_SELF_CROSSED)
    {
      DeleteFromPrice(NDAQ_Order.sharePrice, ecn_book_id, !side, symbolId);
    }
  }
  
  t_QuoteNode *retNode = InsertQuote(ecn_book_id, side, symbolId, &NDAQ_Order);
  
  if (retNode)
  {
    OrderMapInsert(OrderMaps[ecn_book_id][_consumerId], NDAQ_Order.refNum, retNode, buySellIndicator);
  }
  else
  {
    return ERROR;
  }
  
  CheckBookCrossToEnableSymbol(symbolId);
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessExecutedOrderWithPriceMessage                      
- Usage:      Treat this msg as Order Executed msg
****************************************************************************/
int ProcessExecutedOrderWithPriceMessage(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime, unsigned int timeStatusSeconds)
{
  // Get Order Reference Number
  unsigned long orderRefNumber = __builtin_bswap64(*(unsigned long*)&buffer[11]);
    
  t_QuoteNodeInfo nodeInfo = OrderMapRemove(OrderMaps[ecn_book_id][_consumerId], orderRefNumber);
  
  if (nodeInfo.node != NULL && nodeInfo.node != DeletedQuotePlaceholder)
  {
    int executedShares = __builtin_bswap32(*(unsigned int*)&buffer[19]);
    CheckNasdaqStaleData(ecn_book_id, buffer, symbolId, arrivalTime, timeStatusSeconds);
    
    if ((SymbolMgmt.symbolList[symbolId].haltStatus[ecn_book_id] == TRADING_HALTED) && (buffer[31] == 'Y'))
    {
      UpdateHaltStatusForASymbol(symbolId, TRADING_NORMAL, arrivalTime/TEN_RAISE_TO_9, ecn_book_id);
    }

    int side = (nodeInfo.side == 'S')?ASK_SIDE:BID_SIDE;
    if (executedShares >= nodeInfo.node->info.shareVolume)
    {
      DeleteQuote(nodeInfo.node, ecn_book_id, side, symbolId);  
      CheckBookCrossToEnableSymbol(symbolId);
    }
    else
    {
      OrderMapInsert(OrderMaps[ecn_book_id][_consumerId], orderRefNumber, nodeInfo.node, nodeInfo.side);        
      ModifyQuote(nodeInfo.node, nodeInfo.node->info.shareVolume - executedShares, side, symbolId);
    }
  }
    
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessExecutedOrderMessage
****************************************************************************/
int ProcessExecutedOrderMessage(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime, unsigned int timeStatusSeconds)
{
  // Get Order Reference Number
  unsigned long orderRefNumber = __builtin_bswap64(*(unsigned long*)&buffer[11]);
    
  t_QuoteNodeInfo nodeInfo = OrderMapRemove(OrderMaps[ecn_book_id][_consumerId], orderRefNumber);
  
  if (nodeInfo.node != NULL && nodeInfo.node != DeletedQuotePlaceholder)
  {
    // Get Executed Shares
    int executedShares = __builtin_bswap32(*(unsigned int*)&buffer[19]);
    
    CheckNasdaqStaleData(ecn_book_id, buffer, symbolId, arrivalTime, timeStatusSeconds);
    
    int side = (nodeInfo.side == 'S')?ASK_SIDE:BID_SIDE;
    
    if (executedShares >= nodeInfo.node->info.shareVolume)
    {
      DeleteQuote(nodeInfo.node, ecn_book_id, side, symbolId); 
      CheckBookCrossToEnableSymbol(symbolId);
    }
    else
    {
      OrderMapInsert(OrderMaps[ecn_book_id][_consumerId], orderRefNumber, nodeInfo.node, nodeInfo.side);
      ModifyQuote(nodeInfo.node, nodeInfo.node->info.shareVolume - executedShares, side, symbolId);
    }
  }
    
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessCancelOrderMessage
****************************************************************************/
int ProcessCancelOrderMessage(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime, unsigned int timeStatusSeconds)
{
  // Get Order Reference Number
  unsigned long orderRefNumber = __builtin_bswap64(*(unsigned long*)&buffer[11]);
  
  t_QuoteNodeInfo nodeInfo = OrderMapRemove(OrderMaps[ecn_book_id][_consumerId], orderRefNumber);
  
  if (nodeInfo.node != NULL && nodeInfo.node != DeletedQuotePlaceholder)
  {
    // Get Cancel Shares
    int canceledShares = __builtin_bswap32(*(unsigned int*)&buffer[19]);

    CheckNasdaqStaleData(ecn_book_id, buffer, symbolId, arrivalTime, timeStatusSeconds);
    
    int side = (nodeInfo.side == 'S')?ASK_SIDE:BID_SIDE;
        
    if (canceledShares >= nodeInfo.node->info.shareVolume)
    {
      DeleteQuote(nodeInfo.node, ecn_book_id, side, symbolId);
      CheckBookCrossToEnableSymbol(symbolId);
    }
    else
    {
      OrderMapInsert(OrderMaps[ecn_book_id][_consumerId], orderRefNumber, nodeInfo.node, nodeInfo.side);
      ModifyQuote(nodeInfo.node, nodeInfo.node->info.shareVolume - canceledShares, side, symbolId);
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessDeleteOrderMessage
****************************************************************************/
int ProcessDeleteOrderMessage(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime, unsigned int timeStatusSeconds)
{
  // Get Order Reference Number
  unsigned long orderRefNumber = __builtin_bswap64(*(unsigned long*)&buffer[11]);
  
  t_QuoteNodeInfo nodeInfo = OrderMapRemove(OrderMaps[ecn_book_id][_consumerId], orderRefNumber);
  
  if (nodeInfo.node != NULL && nodeInfo.node != DeletedQuotePlaceholder)
  {
    CheckNasdaqStaleData(ecn_book_id, buffer, symbolId, arrivalTime, timeStatusSeconds);
    
    int side = (nodeInfo.side == 'S')?ASK_SIDE:BID_SIDE;
    if (nodeInfo.node != NULL)
    {
      DeleteQuote(nodeInfo.node, ecn_book_id, side, symbolId);
      CheckBookCrossToEnableSymbol(symbolId);
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessCrossTradeMessage
****************************************************************************/
void ProcessCrossTradeMessage(unsigned char ecn_book_id, uint32_t symbolId, char *buffer)
{
  /*  Remove all BIDs with a price greater than the crossing price
    Remove all ASKs with a price less than the crossing price */
  
  unsigned char crossType = buffer[39];
  double crossPrice = __builtin_bswap32(*(unsigned int*)&buffer[27]) * 0.0001;
  if(crossType == 'I' && fgt(crossPrice, 0.00))
  {
    DeleteBetterPrice(crossPrice, ecn_book_id, BID_SIDE, symbolId);
    DeleteBetterPrice(crossPrice, ecn_book_id, ASK_SIDE, symbolId);
    CheckBookCrossToEnableSymbol(symbolId);
  }
}

/****************************************************************************
- Function name:  ProcessStockTradingActionMessage                
- Usage:      Check to halt symbol
          Do not use this message to resume symbol, since it is unrealiable
****************************************************************************/
void ProcessStockTradingActionMessage(uint32_t symbolId, char *buffer, unsigned long arrivalTime)
{
  if (buffer[19] != 'T')
  {
    UpdateHaltStatusForASymbol(symbolId, TRADING_HALTED, arrivalTime/TEN_RAISE_TO_9, MAX_ECN_BOOK);
  }
}

/****************************************************************************
- Function name:  ProcessReplaceOrderMessage
****************************************************************************/
int ProcessReplaceOrderMessage(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime, unsigned int timeStatusSeconds)
{
  // Get Order Reference Number
  unsigned long oldRefNumber = __builtin_bswap64(*(unsigned long*)&buffer[11]);
  
  t_OrderDetail NDAQ_Order;
  
  // Get order reference number
  NDAQ_Order.refNum = __builtin_bswap64(*(unsigned long*)&buffer[19]);
  
  // Get shares
  NDAQ_Order.shareVolume = __builtin_bswap32(*(unsigned int*)&buffer[27]);
  
  // Get Price
  NDAQ_Order.sharePrice = __builtin_bswap32(*(unsigned int*)&buffer[31]) * 0.0001;
  
  /* We will delete old quote */
  t_QuoteNodeInfo nodeInfo = OrderMapRemove(OrderMaps[ecn_book_id][_consumerId], oldRefNumber);
  
  if (nodeInfo.node != NULL)
  {
    int side = (nodeInfo.side == 'S')?ASK_SIDE:BID_SIDE;
    
    if(nodeInfo.node != DeletedQuotePlaceholder) DeleteQuote(nodeInfo.node, ecn_book_id, side, symbolId);

    CheckNasdaqStaleData(ecn_book_id, buffer, symbolId, arrivalTime, timeStatusSeconds);
  
    /* We have all information needed for cross detection and adding new info to book */
    if (SymbolMgmt.symbolList[symbolId].haltStatus[ecn_book_id] == TRADING_NORMAL)
    {
      int tradeRet = DetectCrossTrade(&NDAQ_Order, side, symbolId, ecn_book_id, arrivalTime/1000, REASON_CROSS_ENTRY);
    
      if (tradeRet == TRADE_DETECTED_SELF_CROSSED)
      {
        DeleteFromPrice(NDAQ_Order.sharePrice, ecn_book_id, !side, symbolId);
      }
    }
    
    /* Add new quote to book */
    t_QuoteNode *retNode = InsertQuote(ecn_book_id, side, symbolId, &NDAQ_Order);
    if (retNode)
    {
      OrderMapInsert(OrderMaps[ecn_book_id][_consumerId], NDAQ_Order.refNum, retNode,  (side == ASK_SIDE)?'S':'B');
    }
    else
    {
      return ERROR;
    }
  }
  
  CheckBookCrossToEnableSymbol(symbolId);

  return SUCCESS;
}
