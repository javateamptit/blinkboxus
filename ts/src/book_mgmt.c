/****************************************************************************
** Project name:  Equity Arbitrage Application
** File name:   book_mgmt.c
** Description:   This file contains function definitions that were declared
        in book_mgmt.h

** Author:    Sang Nguyen-Minh
** First created on 26 September 2007
** Last updated on 27 September 2007
****************************************************************************/

/****************************************************************************
** Include files
****************************************************************************/
#define _ISOC9X_SOURCE
#include "book_mgmt.h"
#include "trading_mgmt.h"
#include "kernel_algorithm.h"
#include "arca_book_proc.h"
#include "nasdaq_book_proc.h"
#include "nyse_book_proc.h"
#include "bats_book_proc.h"

#include "edge_book_proc.h"

#include "cqs_proc.h"
#include "uqdf_proc.h"
#include "hbitime.h"
#include "logging.h"
#include "topic.h"

/****************************************************************************
          GLOBAL VARIABLES
****************************************************************************/
/*#define GARBAGE_COLLECTION_SIZE 10000000
static void *GarbageCollection[GARBAGE_COLLECTION_SIZE];
static unsigned long CurrentGarbageProcessedIndex = 0, CurrentGarbageQueueIndex = 0;*/

t_BookStatus BookStatusMgmt[MAX_ECN_BOOK];
t_QuoteList *StockBook[MAX_STOCK_SYMBOL][MAX_ECN_BOOK][MAX_SIDE];
t_TopOrder BestQuote[MAX_STOCK_SYMBOL][MAX_SIDE];
t_PriceHashingItem *Price_HashMap[MAX_STOCK_SYMBOL][MAX_ECN_BOOK][MAX_SIDE][PRICE_HASHMAP_BUCKET_SIZE];
t_QuoteNode *DeletedQuotePlaceholder;

t_OrderMap *ARCA_OrderMap[DBLFILTER_NUM_QUEUES];
t_OrderMap *NASDAQ_OrderMap[DBLFILTER_NUM_QUEUES];
t_OrderMap *BATSZ_OrderMap[DBLFILTER_NUM_QUEUES];
t_OrderMap *BYX_OrderMap[DBLFILTER_NUM_QUEUES];
t_OrderMap *EDGX_OrderMap[DBLFILTER_NUM_QUEUES];
t_OrderMap *EDGA_OrderMap[DBLFILTER_NUM_QUEUES];
t_OrderMap *NDBX_OrderMap[DBLFILTER_NUM_QUEUES];
t_OrderMap *PSX_OrderMap[DBLFILTER_NUM_QUEUES];
t_OrderMap *OrderMaps[MAX_ECN_BOOK][DBLFILTER_NUM_QUEUES];

t_DBLFilterMgmt DBLFilterMgmt;

/*static unsigned int g_seed = 1234567890; 
static inline int fastrand() 
{ 
  g_seed = (214013*g_seed+2531011); 
  return (g_seed>>16)&0x7FFF; 
}*/

extern int MdcToOecMapping[MAX_ECN_BOOK];
extern double SecFeeAndCommissionInCent;

extern t_MonitorArcaBook   MonitorArcaBook;
extern t_MonitorNasdaqBook MonitorNasdaqBook;
extern t_MonitorNyseBook   MonitorNyseBook;
extern t_MonitorNyseBook   MonitorAmexBook;
extern t_MonitorBatszBook MonitorBatszBook;
extern t_MonitorBYXBook MonitorBYXBook;
extern t_MonitorEdgxBook MonitorEdgxBook;
extern t_MonitorEdgaBook MonitorEdgaBook;
extern t_MonitorNasdaqBook MonitorNdbxBook;
extern t_MonitorNasdaqBook MonitorPsxBook;
extern t_MonitorCQS MonitorCQS;
extern t_MonitorUQDF MonitorUQDF;
extern t_QueueSymbolList QueueSymbolList[DBLFILTER_NUM_QUEUES];

static int VenueFeedMapping[30][512]; //This should big enough to hold all venues plus cqs, uqdf and all feeds
static int SourceVenueMapping[512];
static int IgnoredFeeds[512];
static int IgnoredFeedIndex = 0;

static struct {
  int count;
  int sources[80];
} VenueSources[VENUE_UQDF+1];



/****************************************************************************
      LIST OF FUNCTIONS
****************************************************************************/
/*void RunGarbageCollector()
{
  unsigned long index;
  unsigned long begin = CurrentGarbageProcessedIndex;
  unsigned long last = __sync_fetch_and_add(&CurrentGarbageQueueIndex, 0);
  
  for (index = begin; index < last; index++)
  {
    free(GarbageCollection[index % GARBAGE_COLLECTION_SIZE]);
  }
  
  CurrentGarbageProcessedIndex = last;
}*/

static void ignore_feed(int feed_id)
{
  IgnoredFeeds[IgnoredFeedIndex++]=feed_id;
}

static void register_ignored_feeds(void *topic_registry)
{
  register_dev_null_topic(topic_registry, IgnoredFeeds, IgnoredFeedIndex);
}

static void register_qsymbols(void *ctx, int feed_id, int venue_id)
{
  /* 1. Create topic for qualified symbol.
     2. Associate topic with designated queue. */

  int qidx, i;

  for (qidx = 0; qidx < DBLFILTER_NUM_QUEUES; qidx++) {
    for (i = 0; i < QueueSymbolList[qidx].count; i++) {
      int stock_symbol_index = QueueSymbolList[qidx].stockSymbolIndexes[i];
      const char* symbol = QueueSymbolList[qidx].symbols[i];
      uint32_t topic_id = gen_md_topic_id(venue_id, stock_symbol_index);

      register_qsymbol_topic(ctx, topic_id, symbol, VenueIdToText[venue_id], feed_id, topic_id);
      register_queue_for_topic_id(ctx, qidx, topic_id);
    }
  }
}

static void addVenueFeed(void *ctx, int32_t feed_id, int venue_id, const char* local_ipaddr, int32_t port, const char* mcast_ipaddr)
{
  register_multicast_topic_source(ctx, feed_id, 0, VenueIdToText[venue_id], local_ipaddr, port, mcast_ipaddr);
  register_qsymbols(ctx, feed_id, venue_id);
  VenueSources[venue_id].sources[VenueSources[venue_id].count++] = feed_id;
}

static void addIgnoredFeed(void *ctx, int32_t feed_id, int venue_id, const char* local_ipaddr, int32_t port, const char* mcast_ipaddr)
{
  register_multicast_topic_source(ctx, feed_id, 0, VenueIdToText[venue_id], local_ipaddr, port, mcast_ipaddr);
  ignore_feed(feed_id);
  VenueSources[venue_id].sources[VenueSources[venue_id].count++] = feed_id;
}

static void addCompanionVenueFeed(void *ctx, int32_t primary_feed_id, int venue_id, const char* local_ipaddr, int32_t port, const char* mcast_ipaddr)
{
  register_backup_multicast_topic_source(ctx, primary_feed_id, 0, VenueIdToText[venue_id], local_ipaddr, port, mcast_ipaddr, 0);
}

static void startVenue(void *ctx, int venue_id) {
  int i;
  for (i = 0; i < VenueSources[venue_id].count; i++) {
    start_source(ctx, VenueSources[venue_id].sources[i]);
  }
}

static void stopVenue(void *ctx, int venue_id) {
  int i;
  for (i = 0; i < VenueSources[venue_id].count; i++) {
    stop_source(ctx, VenueSources[venue_id].sources[i]);
  }
}

/****************************************************************************
- Function name:  InitBookMgmtDataStructure
- Description:
****************************************************************************/
void InitBookMgmtDataStructure(void)
{
  int ecnBookIndex;
  int sideIndex;
  int stockSymbolIndex;
    
  // Initialize book status
  for (ecnBookIndex = 0; ecnBookIndex < MAX_ECN_BOOK; ecnBookIndex++)
  {
    BookStatusMgmt[ecnBookIndex].isConnected = DISCONNECTED;
  }
  
  // Initialize CQS and UQDF status
  CQS_StatusMgmt.isConnected = DISCONNECTED;
  
  UQDF_StatusMgmt.isConnected = DISCONNECTED;
  
  // Initialize stock books
  for (ecnBookIndex = 0; ecnBookIndex < MAX_ECN_BOOK; ecnBookIndex++)
  {
    for (sideIndex = 0; sideIndex < MAX_SIDE; sideIndex++)
    {
      for (stockSymbolIndex = 0; stockSymbolIndex < MAX_STOCK_SYMBOL; stockSymbolIndex++)
      {
        StockBook[stockSymbolIndex][ecnBookIndex][sideIndex] = NULL;
      }
    }
  }
  
  // Initialize best quote
  for (stockSymbolIndex = 0; stockSymbolIndex < MAX_STOCK_SYMBOL; stockSymbolIndex++)
  {
    for (sideIndex = 0; sideIndex < MAX_SIDE; sideIndex++)
    {
      BestQuote[stockSymbolIndex][sideIndex].ecnIndicator = -1;
      BestQuote[stockSymbolIndex][sideIndex].shareVolume = 0;
      BestQuote[stockSymbolIndex][sideIndex].sharePrice = 0;
    }
  }
  
  DeletedQuotePlaceholder = (t_QuoteNode*)malloc(sizeof(t_QuoteNode));
}

/****************************************************************************
- Function name:  InitOrderMaps
- Description:
****************************************************************************/
void InitOrderMaps(void) {
  // NASDAQ ARRAY HASH
  uint32_t nasdaq_map_size = OrderMapSize(3145728/DBLFILTER_NUM_QUEUES);
  NASDAQ_OrderMap[_consumerId] = (t_OrderMap*)calloc(1, sizeof(t_OrderMap));
  NASDAQ_OrderMap[_consumerId]->bucket = (t_OrderMapCacheEntry*)calloc(1, sizeof(t_OrderMapCacheEntry) * nasdaq_map_size);
  NASDAQ_OrderMap[_consumerId]->extra_slots = (t_OrderMapEntry*)calloc(1, sizeof(t_OrderMapEntry) * nasdaq_map_size);
  OrderMapInit(NASDAQ_OrderMap[_consumerId], nasdaq_map_size);
  
  // BX ARRAY HASH
  uint32_t ndbx_map_size = OrderMapSize(3145728/DBLFILTER_NUM_QUEUES);
  NDBX_OrderMap[_consumerId] = (t_OrderMap*)calloc(1, sizeof(t_OrderMap));
  NDBX_OrderMap[_consumerId]->bucket = (t_OrderMapCacheEntry*)calloc(1, sizeof(t_OrderMapCacheEntry) * ndbx_map_size);
  NDBX_OrderMap[_consumerId]->extra_slots = (t_OrderMapEntry*)calloc(1, sizeof(t_OrderMapEntry) * ndbx_map_size);
  OrderMapInit(NDBX_OrderMap[_consumerId], ndbx_map_size);
  
  // PSX ARRAY HASH
  uint32_t psx_map_size = OrderMapSize(3145728/DBLFILTER_NUM_QUEUES);
  PSX_OrderMap[_consumerId] = (t_OrderMap*)calloc(1, sizeof(t_OrderMap));
  PSX_OrderMap[_consumerId]->bucket = (t_OrderMapCacheEntry*)calloc(1, sizeof(t_OrderMapCacheEntry) * psx_map_size);
  PSX_OrderMap[_consumerId]->extra_slots = (t_OrderMapEntry*)calloc(1, sizeof(t_OrderMapEntry) * psx_map_size);
  OrderMapInit(PSX_OrderMap[_consumerId], psx_map_size);
  
  // BATSZ ARRAY HASH      
  uint32_t batsz_map_size = OrderMapSize(16777216/DBLFILTER_NUM_QUEUES);
  BATSZ_OrderMap[_consumerId] = (t_OrderMap*)calloc(1, sizeof(t_OrderMap));
  BATSZ_OrderMap[_consumerId]->bucket = (t_OrderMapCacheEntry*)calloc(1, sizeof(t_OrderMapCacheEntry) * batsz_map_size);
  BATSZ_OrderMap[_consumerId]->extra_slots = (t_OrderMapEntry*)calloc(1, sizeof(t_OrderMapEntry) * batsz_map_size);
  OrderMapInit(BATSZ_OrderMap[_consumerId], batsz_map_size);
  
  // BYX ARRAY HASH      
  uint32_t byx_map_size = OrderMapSize(16777216/DBLFILTER_NUM_QUEUES);
  BYX_OrderMap[_consumerId] = (t_OrderMap*)calloc(1, sizeof(t_OrderMap));
  BYX_OrderMap[_consumerId]->bucket = (t_OrderMapCacheEntry*)calloc(1, sizeof(t_OrderMapCacheEntry) * byx_map_size);
  BYX_OrderMap[_consumerId]->extra_slots = (t_OrderMapEntry*)calloc(1, sizeof(t_OrderMapEntry) * byx_map_size);
  OrderMapInit(BYX_OrderMap[_consumerId], byx_map_size);
  
  // EDGX ARRAY HASH
  uint32_t edgx_map_size = OrderMapSize(5242880/DBLFILTER_NUM_QUEUES);
  EDGX_OrderMap[_consumerId] = (t_OrderMap*)calloc(1, sizeof(t_OrderMap)); 
  EDGX_OrderMap[_consumerId]->bucket = (t_OrderMapCacheEntry*)calloc(1, sizeof(t_OrderMapCacheEntry) * edgx_map_size);
  EDGX_OrderMap[_consumerId]->extra_slots = (t_OrderMapEntry*)calloc(1, sizeof(t_OrderMapEntry) * edgx_map_size);
  OrderMapInit(EDGX_OrderMap[_consumerId], edgx_map_size);
  
  // EDGA ARRAY HASH
  uint32_t edga_map_size = OrderMapSize(5242880/DBLFILTER_NUM_QUEUES);
  EDGA_OrderMap[_consumerId] = (t_OrderMap*)calloc(1, sizeof(t_OrderMap)); 
  EDGA_OrderMap[_consumerId]->bucket = (t_OrderMapCacheEntry*)calloc(1, sizeof(t_OrderMapCacheEntry) * edga_map_size);
  EDGA_OrderMap[_consumerId]->extra_slots = (t_OrderMapEntry*)calloc(1, sizeof(t_OrderMapEntry) * edga_map_size);
  OrderMapInit(EDGA_OrderMap[_consumerId], edga_map_size);
  
  // ARCA ARRAY HASH
  uint32_t arca_map_size = OrderMapSize(8388608/DBLFILTER_NUM_QUEUES);
  ARCA_OrderMap[_consumerId] = (t_OrderMap*)calloc(1, sizeof(t_OrderMap));
  ARCA_OrderMap[_consumerId]->bucket = (t_OrderMapCacheEntry*)calloc(1, sizeof(t_OrderMapCacheEntry) * arca_map_size);
  ARCA_OrderMap[_consumerId]->extra_slots = (t_OrderMapEntry*)calloc(1, sizeof(t_OrderMapEntry) * arca_map_size);
  OrderMapInit(ARCA_OrderMap[_consumerId], arca_map_size);
  
  OrderMaps[BOOK_NASDAQ][_consumerId] = NASDAQ_OrderMap[_consumerId];
  OrderMaps[BOOK_NDBX][_consumerId] =   NDBX_OrderMap[_consumerId];
  OrderMaps[BOOK_PSX][_consumerId] =    PSX_OrderMap[_consumerId];
  OrderMaps[BOOK_BATSZ][_consumerId] =  BATSZ_OrderMap[_consumerId];
  OrderMaps[BOOK_BYX][_consumerId] =    BYX_OrderMap[_consumerId];
  OrderMaps[BOOK_EDGX][_consumerId] =   EDGX_OrderMap[_consumerId];
  OrderMaps[BOOK_EDGA][_consumerId] =   EDGA_OrderMap[_consumerId];
  OrderMaps[BOOK_ARCA][_consumerId] =   ARCA_OrderMap[_consumerId]; 
}

/****************************************************************************
- Function name:  UpdateMaxBid
- Description:
****************************************************************************/
void UpdateMaxBid(int stockSymbolIndex)
{
  t_TopOrder *currentTopOrder = &BestQuote[stockSymbolIndex][BID_SIDE];
  t_TopOrder topOrderArray[MAX_ECN_BOOK];
  
  // Get Top Quotes
  GetTopQuotes(topOrderArray, BID_SIDE, stockSymbolIndex);
  
  // Get best quote
  t_TopOrder *retValue = GetBestQuote(topOrderArray, BID_SIDE);
  
  if (retValue != NULL)
  {
    *currentTopOrder = *retValue;
  }
  else
  {
    currentTopOrder->ecnIndicator = -1;
    currentTopOrder->sharePrice = 0.00;
    currentTopOrder->shareVolume = 0;
  }
}

/****************************************************************************
- Function name:  UpdateMinAsk
- Description:
****************************************************************************/
void UpdateMinAsk(int stockSymbolIndex)
{
  t_TopOrder *currentTopOrder = &BestQuote[stockSymbolIndex][ASK_SIDE];
  t_TopOrder topOrderArray[MAX_ECN_BOOK];

  // Get Top Quotes
  GetTopQuotes(topOrderArray, ASK_SIDE, stockSymbolIndex);
  
  // Get Best Quote
  t_TopOrder *retValue = GetBestQuote(topOrderArray, ASK_SIDE);
  
  if (retValue != NULL)
  {
    *currentTopOrder = *retValue;
  }
  else
  {
    currentTopOrder->ecnIndicator = -1;
    currentTopOrder->sharePrice = 0.00;
    currentTopOrder->shareVolume = 0;
  }
}

/****************************************************************************
- Function name:  GetTopQuotes
- Description:
****************************************************************************/
void GetTopQuotes(t_TopOrder topOrderArray[MAX_ECN_BOOK], int side, int stockSymbolIndex)
{
  int ecnIndex;
  t_QuoteList *firstList;
  
  for (ecnIndex = 0; ecnIndex < MAX_ECN_BOOK; ecnIndex++)
  {
    // Check to know the stock book of ecn book is allowed to cross trade or not
    if (AlgorithmConfig.bookRole.status[ecnIndex] == ENABLE && BookStatusMgmt[ecnIndex].isConnected == CONNECTED)
    {
      if (ecnIndex == BOOK_NYSE || ecnIndex == BOOK_AMEX)
      {
        if (SymbolMgmt.symbolList[stockSymbolIndex].tradingStatusInNyseBook != NYSE_BOOK_TRADING_STATUS_OPENED)
        {
          if (SymbolMgmt.symbolList[stockSymbolIndex].tradingStatusInNyseBook != ((side == ASK_SIDE) ? NYSE_BOOK_SLOW_ON_BID : NYSE_BOOK_SLOW_ON_ASK))
          {
            topOrderArray[ecnIndex].ecnIndicator = -1;
            continue;
          }
        }
      }
      
      if (SymbolMgmt.symbolList[stockSymbolIndex].haltStatus[ecnIndex] != TRADING_NORMAL ||
          SymbolMgmt.symbolList[stockSymbolIndex].isStale[ecnIndex] == 1)
      {
        topOrderArray[ecnIndex].ecnIndicator = -1;
        continue;
      }
      
      firstList = StockBook[stockSymbolIndex][ecnIndex][side];
      
      if (firstList == NULL)
      {
        topOrderArray[ecnIndex].ecnIndicator = -1;
        continue;
      }
      
      if (firstList->totalVolume < 1)
      {
        topOrderArray[ecnIndex].ecnIndicator = -1;
      }
      else
      {
        topOrderArray[ecnIndex].shareVolume = firstList->totalVolume;
        topOrderArray[ecnIndex].sharePrice = firstList->price;
        topOrderArray[ecnIndex].ecnIndicator = ecnIndex;
      }
    }
    else
    {
      // If ecnIndicator equals -1 that means the top order cannot be used
      topOrderArray[ecnIndex].ecnIndicator = -1;
    }
  }
}
  
/****************************************************************************
- Function name:  GetBestQuote
- Description:
****************************************************************************/
t_TopOrder *GetBestQuote(t_TopOrder topOrderArray[MAX_ECN_BOOK], int side)
{
  int ecnIndex;
  
  // We assume that the first item in array is best
  t_TopOrder *bestQuote = &topOrderArray[0];
  
  if (side == BID_SIDE)
  {
    for (ecnIndex = 1; ecnIndex < MAX_ECN_BOOK; ecnIndex++)
    {
      // Check Top Order to know that it is available to access
      if (topOrderArray[ecnIndex].ecnIndicator != -1)
      {
        if (bestQuote->ecnIndicator == -1)
        {
          bestQuote = &topOrderArray[ecnIndex];
          continue;
        }
        else
        {
          if (flt(bestQuote->sharePrice, topOrderArray[ecnIndex].sharePrice))
          {
            bestQuote = &topOrderArray[ecnIndex];
          }
        }
      }
    }
  }
  else
  {
    for (ecnIndex = 1; ecnIndex < MAX_ECN_BOOK; ecnIndex++)
    {
      // Check Top Order to know that it is available to access
      if (topOrderArray[ecnIndex].ecnIndicator != -1)
      {
        if (bestQuote->ecnIndicator == -1)
        {
          bestQuote = &topOrderArray[ecnIndex];
          continue;
        }
        else
        {
          if (fgt(bestQuote->sharePrice, topOrderArray[ecnIndex].sharePrice))
          {
            bestQuote = &topOrderArray[ecnIndex];
          }
        }
      }
    }
  }
  
  // Check before return for calling the routine
  if (bestQuote->ecnIndicator != -1)
  {
    return bestQuote;
  }
  else
  {
    return NULL;
  }
}

/****************************************************************************
- Function name:  GetBestServerRoleOrder
****************************************************************************/
int GetBestServerRoleOrder(t_TopOrder *bestOrder, int side, int stockSymbolIndex, int serverRole)
{
  int ecnIndex;
  t_TopOrder topOrderArray[MAX_ECN_BOOK];
  t_QuoteList *currentList;
  
  bestOrder->ecnIndicator = -1;
  
  if (side == ASK_SIDE)
  {
    for (ecnIndex = 0; ecnIndex < MAX_ECN_BOOK; ecnIndex++)
    {
      if (AlgorithmConfig.entryOrderPlacement[serverRole].status[ecnIndex] != ENABLE)
      {
        topOrderArray[ecnIndex].ecnIndicator = -1;
        continue;
      }
      
      // Check to know the stock book of ecn book is allowed to cross trade or not
      if (AlgorithmConfig.bookRole.status[ecnIndex] == ENABLE && BookStatusMgmt[ecnIndex].isConnected == CONNECTED)
      {
        if (ecnIndex == BOOK_NYSE || ecnIndex == BOOK_AMEX)
        {
          if (SymbolMgmt.symbolList[stockSymbolIndex].tradingStatusInNyseBook != NYSE_BOOK_TRADING_STATUS_OPENED)
          {
            if (SymbolMgmt.symbolList[stockSymbolIndex].tradingStatusInNyseBook != NYSE_BOOK_SLOW_ON_BID)
            {
              topOrderArray[ecnIndex].ecnIndicator = -1;
              continue;
            }
          }
        }
        
        if (SymbolMgmt.symbolList[stockSymbolIndex].haltStatus[ecnIndex] != TRADING_NORMAL ||
          SymbolMgmt.symbolList[stockSymbolIndex].isStale[ecnIndex] == 1)
        {
          topOrderArray[ecnIndex].ecnIndicator = -1;
          continue;
        }
        
        currentList = StockBook[stockSymbolIndex][ecnIndex][ASK_SIDE];
        
        if (currentList == NULL)
        {
          topOrderArray[ecnIndex].ecnIndicator = -1;
          continue;
        }
        
        if (currentList->totalVolume < 1)
        {
          topOrderArray[ecnIndex].ecnIndicator = -1;
        }
        else
        {
          topOrderArray[ecnIndex].shareVolume = currentList->totalVolume;
          topOrderArray[ecnIndex].sharePrice = currentList->price;
          topOrderArray[ecnIndex].ecnIndicator = ecnIndex;
        }
      }
      else
      {
        // If ecnIndicator equals -1 that means the top order cannot be used
        topOrderArray[ecnIndex].ecnIndicator = -1;
      }
    }
  }
  else
  {
    for (ecnIndex = 0; ecnIndex < MAX_ECN_BOOK; ecnIndex++)
    {
      if (AlgorithmConfig.entryOrderPlacement[serverRole].status[ecnIndex] != ENABLE)
      {
        topOrderArray[ecnIndex].ecnIndicator = -1;
        continue;
      }
      
      // Check to know the stock book of ecn book is allowed to cross trade or not
      if (AlgorithmConfig.bookRole.status[ecnIndex] == ENABLE && BookStatusMgmt[ecnIndex].isConnected == CONNECTED)
      {
        if (ecnIndex == BOOK_NYSE || ecnIndex == BOOK_AMEX)
        {
          if (SymbolMgmt.symbolList[stockSymbolIndex].tradingStatusInNyseBook != NYSE_BOOK_TRADING_STATUS_OPENED)
          {
            if (SymbolMgmt.symbolList[stockSymbolIndex].tradingStatusInNyseBook != NYSE_BOOK_SLOW_ON_ASK)
            {
              topOrderArray[ecnIndex].ecnIndicator = -1;
              continue;
            }
          }
        }
        
        if (SymbolMgmt.symbolList[stockSymbolIndex].haltStatus[ecnIndex] != TRADING_NORMAL ||
          SymbolMgmt.symbolList[stockSymbolIndex].isStale[ecnIndex] == 1)
        {
          topOrderArray[ecnIndex].ecnIndicator = -1;
          continue;
        }
        
        currentList = StockBook[stockSymbolIndex][ecnIndex][BID_SIDE];
        
        if (currentList == NULL)
        {
          topOrderArray[ecnIndex].ecnIndicator = -1;
          continue;
        }
        
        if (currentList->totalVolume < 1)
        {
          topOrderArray[ecnIndex].ecnIndicator = -1;
        }
        else
        {
          topOrderArray[ecnIndex].shareVolume = currentList->totalVolume;
          topOrderArray[ecnIndex].sharePrice = currentList->price;
          topOrderArray[ecnIndex].ecnIndicator = ecnIndex;
        }
      }
      else
      {
        // If ecnIndicator equals -1 that means the top order cannot be used
        topOrderArray[ecnIndex].ecnIndicator = -1;
      }
    }
  }
  
  // Find the best quote
  if (side == BID_SIDE)
  {
    for (ecnIndex = 0; ecnIndex < MAX_ECN_BOOK; ecnIndex++)
    {
      // Check Top Order to know that it is available to access
      if (topOrderArray[ecnIndex].ecnIndicator != -1)
      {
        if (bestOrder->ecnIndicator == -1)
        {
          *bestOrder = topOrderArray[ecnIndex];
          
          continue;
        }
        else
        {
          if (flt(bestOrder->sharePrice, topOrderArray[ecnIndex].sharePrice))
          {
            *bestOrder = topOrderArray[ecnIndex];
          }
        }
      }
    }
  }
  else
  {
    for (ecnIndex = 0; ecnIndex < MAX_ECN_BOOK; ecnIndex++)
    {
      // Check Top Order to know that it is available to access
      if (topOrderArray[ecnIndex].ecnIndicator != -1)
      {
        if (bestOrder->ecnIndicator == -1)
        {
          *bestOrder = topOrderArray[ecnIndex];
          
          continue;
        }
        else
        {
          if (fgt(bestOrder->sharePrice, topOrderArray[ecnIndex].sharePrice))
          {
            *bestOrder = topOrderArray[ecnIndex];
          }
        }
      }
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  GetBestOrder
****************************************************************************/
inline void GetBestOrder(t_TopOrder *bestOrder, int side, int stockSymbolIndex)
{
  *bestOrder = BestQuote[stockSymbolIndex][side];
}

/****************************************************************************
- Function name:  AggregateTotalSharesAtEntry
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int AggregateTotalSharesAtEntry(t_OrderDetail *newOrder, t_AggregatedOrder quotes[MAX_ECN_BOOK], int side, int stockSymbolIndex, int ecnLaunchID, int *bestIndex)
{
  int ecnIndex;
  int totalShares = 0;                                        
    
  t_Symbol *symbolInfo = &SymbolMgmt.symbolList[stockSymbolIndex];
  char nyseSymbolStatus = symbolInfo->tradingStatusInNyseBook;

  int bestEcn = -1;
  int bestShare = 0;
  double bestPrice = 0.0;
  
  for (ecnIndex = 0; ecnIndex < MAX_ECN_BOOK; ecnIndex++) 
  {
    t_AggregatedOrder *_quote = &quotes[ecnIndex];
    _quote->ecnIndicator = -1;
    
    if (ecnIndex == ecnLaunchID) continue;
    
    if (symbolInfo->haltStatus[ecnIndex] != TRADING_NORMAL || symbolInfo->isStale[ecnIndex] == 1) continue;
    
    if ((ecnIndex == BOOK_NYSE || ecnIndex == BOOK_AMEX) && (nyseSymbolStatus != NYSE_BOOK_TRADING_STATUS_OPENED)) //NYSE symbol is not opened
    {
      // if we are aggregating the bid side and book is slow on the ask
      if (!(nyseSymbolStatus == NYSE_BOOK_SLOW_ON_ASK && side == BID_SIDE) &&
          !(nyseSymbolStatus == NYSE_BOOK_SLOW_ON_BID && side == ASK_SIDE))
      {
        continue;
      }
    }
    
    if (AlgorithmConfig.bookRole.status[ecnIndex] == ENABLE)
    {
      int oecIndex = MdcToOecMapping[ecnIndex];
      
      if ((OrderStatusMgmt[oecIndex].isConnected == CONNECTED) && (*(TradingStatus.status[oecIndex]) == ENABLE))
      {
        // Get best price level in stock book
        t_QuoteList *list = StockBook[stockSymbolIndex][ecnIndex][side];
        
        // It must not null to be continue
        while (list)
        {
          int currentShare = list->totalVolume;
          double currentPrice = list->price;
          
          if (currentPrice <= 0.000001 || currentShare <= 0) break;
                              
          // Calculate current spread
          double currentSpread = (side == BID_SIDE) ? (currentPrice - newOrder->sharePrice) 
                                             : (newOrder->sharePrice - currentPrice);

          // Check current spread with min spread
          if (flt(currentSpread, AlgorithmConfig.globalConfig.minSpread)) break;                                 

          totalShares += currentShare;
                    
          _quote->priceAverage += (currentShare * currentPrice);
          _quote->totalShare += currentShare;
          _quote->priceWorse = currentPrice;
          
          if (_quote->shareBest == 0)
          {
            _quote->shareBest = currentShare;
            _quote->priceBest = currentPrice; 
            
            if(bestEcn == -1 ||
              (side == BID_SIDE && fgt(currentPrice, bestPrice)) ||
              (side == ASK_SIDE && flt(currentPrice, bestPrice)) ||
              (currentShare > bestShare && feq(currentPrice, bestPrice)) ||
              (currentShare == bestShare && feq(currentPrice, bestPrice) && AlgorithmConfig.bookPriority.status[ecnIndex] < AlgorithmConfig.bookPriority.status[bestEcn]))
            {
              bestShare = currentShare;
              bestPrice = currentPrice;   
              bestEcn = ecnIndex;
            }                          
          }
    
          // Next list
          list = list->next;
        }
        
        if ((_quote->totalShare > 0) && (_quote->priceWorse > 0.0001))
        {
          _quote->ecnIndicator = ecnIndex;
          _quote->priceAverage /= _quote->totalShare;
        }
      }
    }
  }                                    
  
  if (bestEcn == -1)
  {
    *bestIndex = -2;  //other side is not ready
  } 
  else
  {
    *bestIndex = bestEcn;
  }

  return totalShares;
}

/****************************************************************************
- Function name:  GetBBOSnapShotAtEntry
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int GetBBOSnapShotAtEntry(t_OrderDetail *newOrder, int newOrderSide, t_AggregatedOrder quotes[MAX_ECN_BOOK], int stockSymbolIndex, int ecnLaunchID, t_BBOSnapShot BBOSnapShot[MAX_EXCHANGE])
{  
  int ecnIndex;
  t_Symbol *symbolInfo = &SymbolMgmt.symbolList[stockSymbolIndex];
  
  char nyseSymbolStatus = symbolInfo->tradingStatusInNyseBook;
  
  //Save BBO Snapshot for Books
  BBOSnapShot[BOOK_ARCA].participantId = 'P';
  BBOSnapShot[BOOK_NASDAQ].participantId = 'T';
  BBOSnapShot[BOOK_NYSE].participantId = 'N';
  BBOSnapShot[BOOK_BATSZ].participantId = 'Z';
  BBOSnapShot[BOOK_BYX].participantId = 'Y';
  BBOSnapShot[BOOK_EDGX].participantId = 'K';
  BBOSnapShot[BOOK_EDGA].participantId = 'J';
  BBOSnapShot[BOOK_NDBX].participantId = 'B';
  BBOSnapShot[BOOK_PSX].participantId = 'X';
  BBOSnapShot[BOOK_AMEX].participantId = 'A';
  
  // Get BBO for direct connected venues
  for (ecnIndex = 0; ecnIndex < FIRST_AWAY_EXCHANGE; ecnIndex++)
  {
    if (symbolInfo->haltStatus[ecnIndex] != TRADING_NORMAL || symbolInfo->isStale[ecnIndex] == 1)
    {    
      // disregard both sides of halted or stale books 
      BBOSnapShot[ecnIndex].offerSize = 0;
      BBOSnapShot[ecnIndex].offerPrice = 0.0;
      BBOSnapShot[ecnIndex].bidSize = 0;
      BBOSnapShot[ecnIndex].bidPrice = 0.0;      
      continue;
    }

    t_AggregatedOrder *_quote = &quotes[ecnIndex];

    //ASK SIDE    
    if ((ecnIndex == BOOK_NYSE || ecnIndex == BOOK_AMEX) && 
        nyseSymbolStatus != NYSE_BOOK_TRADING_STATUS_OPENED &&
        nyseSymbolStatus != NYSE_BOOK_SLOW_ON_BID)
    {                                            
      // disregard offer on slow or unopened nyse book  
      BBOSnapShot[ecnIndex].offerSize = 0;
      BBOSnapShot[ecnIndex].offerPrice = 0.0;
    }
    else if (newOrderSide == BID_SIDE && _quote->shareBest >= 100)
    {
      // use best offer from aggregated quotes on opposite side if it is a round lot
      BBOSnapShot[ecnIndex].offerSize = (_quote->shareBest / 100) * 100;
      BBOSnapShot[ecnIndex].offerPrice = _quote->priceBest;
    }
    else
    {
      // get the best offer from our book
      t_QuoteList *list = StockBook[stockSymbolIndex][ecnIndex][ASK_SIDE];
      while (list)
      {
        int currentShare = list->totalVolume;
        double currentPrice = list->price;
          
        if (currentShare >= 100 && currentPrice > 0.000001)
        {
          BBOSnapShot[ecnIndex].offerSize = (currentShare / 100) * 100;
          BBOSnapShot[ecnIndex].offerPrice = currentPrice;
          break;
        }
        
        list = list->next;
      }
      
      if (!list)
      {
        // could not find protected offer in our book
        BBOSnapShot[ecnIndex].offerSize = 0;
        BBOSnapShot[ecnIndex].offerPrice = 0.0;       
      }
    }
    
    //BID SIDE    
    if ((ecnIndex == BOOK_NYSE || ecnIndex == BOOK_AMEX) && 
        nyseSymbolStatus != NYSE_BOOK_TRADING_STATUS_OPENED &&
        nyseSymbolStatus != NYSE_BOOK_SLOW_ON_ASK)
    {
      // disregard bid if book is not opened and book is not just slow on the ask  
      BBOSnapShot[ecnIndex].bidSize = 0;
      BBOSnapShot[ecnIndex].bidPrice = 0.0;     
    } 
    else if (newOrderSide == ASK_SIDE && _quote->shareBest >= 100)
    {
      // use best bid from aggreaged quotes on opposite side if it is a round lot
      BBOSnapShot[ecnIndex].bidSize = (_quote->shareBest / 100) * 100;
      BBOSnapShot[ecnIndex].bidPrice = _quote->priceBest;
    }
    else
    {
      // get the best bid from our book
      t_QuoteList *list = StockBook[stockSymbolIndex][ecnIndex][BID_SIDE];
      while (list)
      {
        int currentShare = list->totalVolume;
        double currentPrice = list->price;
        
        if ((currentShare >= 100) && (currentPrice > 0.000001))
        {
          BBOSnapShot[ecnIndex].bidSize = (currentShare / 100) * 100;
          BBOSnapShot[ecnIndex].bidPrice = currentPrice;
          break;
        }
        
        list = list->next;
      }
      
      if (!list)
      {
        // could not find protected bid in our book
        BBOSnapShot[ecnIndex].bidSize = 0;
        BBOSnapShot[ecnIndex].bidPrice = 0.0;       
      }     
    }
  }
  
  //Launch quote is currently not in stock book, so we must check if launch quote is round lot 
  if (newOrder->shareVolume >= 100)
  {
    if (newOrderSide == BID_SIDE)
    {
      BBOSnapShot[ecnLaunchID].bidPrice = newOrder->sharePrice;
      BBOSnapShot[ecnLaunchID].bidSize = (newOrder->shareVolume - (newOrder->shareVolume % 100));     
    } 
    else
    {
      BBOSnapShot[ecnLaunchID].offerPrice = newOrder->sharePrice;
      BBOSnapShot[ecnLaunchID].offerSize = (newOrder->shareVolume - (newOrder->shareVolume % 100));     
    }
  }
   
  t_BBOQuoteList *symbolBBO = &BBOQuoteMgmt[stockSymbolIndex];    
  int sipSource = -1;
  
  if(CQS_StatusMgmt.isConnected == CONNECTED && 
    (symbolBBO->nationalBBO[CQS_SOURCE].offerSize > 0 || symbolBBO->nationalBBO[CQS_SOURCE].bidSize > 0))
  {
    sipSource = CQS_SOURCE;
  }
  else if(UQDF_StatusMgmt.isConnected == CONNECTED &&
    (symbolBBO->nationalBBO[UQDF_SOURCE].offerSize > 0 || symbolBBO->nationalBBO[UQDF_SOURCE].bidSize > 0))
  {
    sipSource = UQDF_SOURCE;
  }                         
  
  if(sipSource >= 0)
  { 
    memcpy(&BBOSnapShot[FIRST_AWAY_EXCHANGE], &symbolBBO->BBOQuoteInfo[sipSource][FIRST_AWAY_EXCHANGE], sizeof(t_BBOSnapShot) * (MAX_EXCHANGE - FIRST_AWAY_EXCHANGE));
  }
  else
  {  
    memset(&BBOSnapShot[FIRST_AWAY_EXCHANGE], 0, sizeof(t_BBOSnapShot) * (MAX_EXCHANGE - FIRST_AWAY_EXCHANGE));
  }
  
  return SUCCESS;
}
     
/****************************************************************************
- Function name:  GetBBOSnapShot
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int GetBBOSnapShot(int stockSymbolIndex, t_BBOSnapShot BBOSnapShot[MAX_EXCHANGE])
{  
  int ecnIndex;
  t_Symbol *symbolInfo = &SymbolMgmt.symbolList[stockSymbolIndex];
  
  char nyseSymbolStatus = symbolInfo->tradingStatusInNyseBook;
  
  //Save BBO Snapshot for Books
  BBOSnapShot[BOOK_ARCA].participantId = 'P';
  BBOSnapShot[BOOK_NASDAQ].participantId = 'T';
  BBOSnapShot[BOOK_NYSE].participantId = 'N';
  BBOSnapShot[BOOK_BATSZ].participantId = 'Z';
  BBOSnapShot[BOOK_BYX].participantId = 'Y';
  BBOSnapShot[BOOK_EDGX].participantId = 'K';
  BBOSnapShot[BOOK_EDGA].participantId = 'J';
  BBOSnapShot[BOOK_NDBX].participantId = 'B';
  BBOSnapShot[BOOK_PSX].participantId = 'X';
  BBOSnapShot[BOOK_AMEX].participantId = 'A';
  
  // Get BBO for direct connected venues
  for (ecnIndex = 0; ecnIndex < FIRST_AWAY_EXCHANGE; ecnIndex++)
  {
    if (symbolInfo->haltStatus[ecnIndex] != TRADING_NORMAL || symbolInfo->isStale[ecnIndex] == 1)
    {    
      // disregard both sides of halted or stale books 
      BBOSnapShot[ecnIndex].offerSize = 0;
      BBOSnapShot[ecnIndex].offerPrice = 0.0;
      BBOSnapShot[ecnIndex].bidSize = 0;
      BBOSnapShot[ecnIndex].bidPrice = 0.0;      
      continue;
    }

    //ASK SIDE    
    if ((ecnIndex == BOOK_NYSE || ecnIndex == BOOK_AMEX) && 
        nyseSymbolStatus != NYSE_BOOK_TRADING_STATUS_OPENED &&
        nyseSymbolStatus != NYSE_BOOK_SLOW_ON_BID)
    {                                            
      // disregard offer on slow or unopened nyse book  
      BBOSnapShot[ecnIndex].offerSize = 0;
      BBOSnapShot[ecnIndex].offerPrice = 0.0;
    }
    else
    {
      // get the best offer from our book
      t_QuoteList *list = StockBook[stockSymbolIndex][ecnIndex][ASK_SIDE];
      while (list)
      {
        int currentShare = list->totalVolume;
        double currentPrice = list->price;
          
        if (currentShare >= 100 && currentPrice > 0.000001)
        {
          BBOSnapShot[ecnIndex].offerSize = (currentShare / 100) * 100;
          BBOSnapShot[ecnIndex].offerPrice = currentPrice;
          break;
        }
        
        list = list->next;
      }
      
      if (!list)
      {
        // could not find protected offer in our book
        BBOSnapShot[ecnIndex].offerSize = 0;
        BBOSnapShot[ecnIndex].offerPrice = 0.0;       
      }
    }
    
    //BID SIDE    
    if ((ecnIndex == BOOK_NYSE || ecnIndex == BOOK_AMEX) && 
        nyseSymbolStatus != NYSE_BOOK_TRADING_STATUS_OPENED &&
        nyseSymbolStatus != NYSE_BOOK_SLOW_ON_ASK)
    {
      // disregard bid if book is not opened and book is not just slow on the ask  
      BBOSnapShot[ecnIndex].bidSize = 0;
      BBOSnapShot[ecnIndex].bidPrice = 0.0;     
    } 
    else
    {
      // get the best bid from our book
      t_QuoteList *list = StockBook[stockSymbolIndex][ecnIndex][BID_SIDE];
      while (list)
      {
        int currentShare = list->totalVolume;
        double currentPrice = list->price;
        
        if ((currentShare >= 100) && (currentPrice > 0.000001))
        {
          BBOSnapShot[ecnIndex].bidSize = (currentShare / 100) * 100;
          BBOSnapShot[ecnIndex].bidPrice = currentPrice;
          break;
        }
        
        list = list->next;
      }
      
      if (!list)
      {
        // could not find protected bid in our book
        BBOSnapShot[ecnIndex].bidSize = 0;
        BBOSnapShot[ecnIndex].bidPrice = 0.0;       
      }     
    }
  }
     
  t_BBOQuoteList *symbolBBO = &BBOQuoteMgmt[stockSymbolIndex];    
  int sipSource = -1;
  
  if(CQS_StatusMgmt.isConnected == CONNECTED && 
    (symbolBBO->nationalBBO[CQS_SOURCE].offerSize > 0 || symbolBBO->nationalBBO[CQS_SOURCE].bidSize > 0))
  {
    sipSource = CQS_SOURCE;
  }
  else if(UQDF_StatusMgmt.isConnected == CONNECTED &&
    (symbolBBO->nationalBBO[UQDF_SOURCE].offerSize > 0 || symbolBBO->nationalBBO[UQDF_SOURCE].bidSize > 0))
  {
    sipSource = UQDF_SOURCE;
  }                         
  
  if(sipSource >= 0)
  { 
    memcpy(&BBOSnapShot[FIRST_AWAY_EXCHANGE], &symbolBBO->BBOQuoteInfo[sipSource][FIRST_AWAY_EXCHANGE], sizeof(t_BBOSnapShot) * (MAX_EXCHANGE - FIRST_AWAY_EXCHANGE));
  }
  else
  {  
    memset(&BBOSnapShot[FIRST_AWAY_EXCHANGE], 0, sizeof(t_BBOSnapShot) * (MAX_EXCHANGE - FIRST_AWAY_EXCHANGE));
  }
  
  return SUCCESS;
}


/****************************************************************************
- Function name:  GetNumberOfSymbolsWithQuotes
****************************************************************************/
int GetNumberOfSymbolsWithQuotes(int ecnBookID)
{
  int numberOfSymbolsWithQuotes = 0;
  int stockSymbolIndex;
  for (stockSymbolIndex = 0; stockSymbolIndex < SymbolMgmt.nextSymbolIndex; stockSymbolIndex++)
  {
    t_QuoteList *askList = StockBook[stockSymbolIndex][ecnBookID][ASK_SIDE];
    
    // Check empty for the list
    if (askList)
    {
      numberOfSymbolsWithQuotes++;
    }
    else
    {
      t_QuoteList *bidList = StockBook[stockSymbolIndex][ecnBookID][BID_SIDE];
      
      // Check empty for the list
      if (bidList)
      {
        numberOfSymbolsWithQuotes++;
      }
    }
  }
  
  return numberOfSymbolsWithQuotes;
}

void ____DBL_Filter_Related_____(){}

/****************************************************************************
- Function name:  StartDBLFilterThread
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
void *StartDBLFilterThread(void *dbl_device)
{
  SetKernelAlgorithm("[DBLFILTER_RECEIVER]");
  TraceLog(DEBUG_LEVEL, "Starting dbl device.\n");
  start_device_with_offset(dbl_device, -hbitime_get_offset_micros() * 1000);
  return NULL;
}

/****************************************************************************
- Function name:  UpdateDBLFilterForNewVenueAddress
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
void UpdateDBLFilterForNewVenueAddress(const int venue_id)
{
  /*Note: Update venue info during runtime is broken with DBLFilter lib!*/
  switch (venue_id)
  {
    case BOOK_ARCA:
      {
        TraceLog(ERROR_LEVEL, "If you have switched ARCA to another feed, please restart TS to take affect\n");
      }
      break;
    case BOOK_NASDAQ:
      {
        TraceLog(ERROR_LEVEL, "If you have changed NASDAQ *Data Multicast* address, please restart TS to take affect\n");
      }
    break;
  }
}

void IgnoreVenueFeeds(const int venue_id, const int countSubscribed, char subscribedIP[][16], int subscribedPort[], char subsribedInterface[][16], void *topic_registry, int *feed_id)
{
  int i, j;
  int shouldIgnore = 0;
  char *iface;
  switch (venue_id)
  {
    case BOOK_BATSZ:
      for (i = 0; i < BATSZ_Book_Conf.countRef; i++)
      {
        shouldIgnore = 0;
        for (j = 0; j < countSubscribed; j++)
        {
          if (strcmp(BATSZ_Book_Conf.refIP[i], subscribedIP[j]) == 0)   // same ip
          {
            if (BATSZ_Book_Conf.refPort[i] == subscribedPort[j])
            {
              shouldIgnore = 0;
              break;
            }
            else
            {
              shouldIgnore = 1;
              iface = subsribedInterface[j];
            }
          }
        }
        
        if (shouldIgnore == 1)
        {
          addIgnoredFeed(topic_registry, *feed_id, VENUE_BATSZ, iface, BATSZ_Book_Conf.refPort[i], BATSZ_Book_Conf.refIP[i]);
          *feed_id = (*feed_id + 1);
          TraceLog(DEBUG_LEVEL, "InitializeMarketDataComponents(batsz): Ignore %s:%d (interface: %s)\n", BATSZ_Book_Conf.refIP[i], BATSZ_Book_Conf.refPort[i], iface);
        }
      }
      break;
    case BOOK_NYSE:
      for (i = 0; i < NyseBookConf.countRef; i++)
      {
        shouldIgnore = 0;
        for (j = 0; j < countSubscribed; j++)
        {
          if (strcmp(NyseBookConf.refIP[i], subscribedIP[j]) == 0)    // same ip
          {
            if (NyseBookConf.refPort[i] == subscribedPort[j])
            {
              shouldIgnore = 0;
              break;
            }
            else
            {
              shouldIgnore = 1;
              iface = subsribedInterface[j];
            }
          }
        }
        
        if (shouldIgnore == 1)
        {
          addIgnoredFeed(topic_registry, *feed_id, VENUE_NYSE, iface, NyseBookConf.refPort[i], NyseBookConf.refIP[i]);
          *feed_id = (*feed_id + 1);
          TraceLog(DEBUG_LEVEL, "InitializeMarketDataComponents(nyse): Ignore %s:%d (interface: %s)\n", NyseBookConf.refIP[i], NyseBookConf.refPort[i], iface);
        }
      }
      break;
    case BOOK_EDGX:
      for (i = 0; i < EDGX_BOOK_Conf.countRef; i++)
      {
        shouldIgnore = 0;
        for (j = 0; j < countSubscribed; j++)
        {
          if (strcmp(EDGX_BOOK_Conf.refIP[i], subscribedIP[j]) == 0)    // same ip
          {
            if (EDGX_BOOK_Conf.refPort[i] == subscribedPort[j])
            {
              shouldIgnore = 0;
              break;
            }
            else
            {
              shouldIgnore = 1;
              iface = subsribedInterface[j];
            }
          }
        }
        
        if (shouldIgnore == 1)
        {
          addIgnoredFeed(topic_registry, *feed_id, VENUE_EDGX, iface, EDGX_BOOK_Conf.refPort[i], EDGX_BOOK_Conf.refIP[i]);
          *feed_id = (*feed_id + 1);
          TraceLog(DEBUG_LEVEL, "InitializeMarketDataComponents(edgx): Ignore %s:%d (interface: %s)\n", EDGX_BOOK_Conf.refIP[i], EDGX_BOOK_Conf.refPort[i], iface);
        }
      }
      break;
    case BOOK_EDGA:
      for (i = 0; i < EDGA_BOOK_Conf.countRef; i++)
      {
        shouldIgnore = 0;
        for (j = 0; j < countSubscribed; j++)
        {
          if (strcmp(EDGA_BOOK_Conf.refIP[i], subscribedIP[j]) == 0)    // same ip
          {
            if (EDGA_BOOK_Conf.refPort[i] == subscribedPort[j])
            {
              shouldIgnore = 0;
              break;
            }
            else
            {
              shouldIgnore = 1;
              iface = subsribedInterface[j];
            }
          }
        }
        
        if (shouldIgnore == 1)
        {
          addIgnoredFeed(topic_registry, *feed_id, VENUE_EDGA, iface, EDGA_BOOK_Conf.refPort[i], EDGA_BOOK_Conf.refIP[i]);
          *feed_id = (*feed_id + 1);
          TraceLog(DEBUG_LEVEL, "InitializeMarketDataComponents(edga): Ignore %s:%d (interface: %s)\n", EDGA_BOOK_Conf.refIP[i], EDGA_BOOK_Conf.refPort[i], iface);
        }
      }
      break;
    case BOOK_BYX:
      for (i = 0; i < BYX_Book_Conf.countRef; i++)
      {
        shouldIgnore = 0;
        for (j = 0; j < countSubscribed; j++)
        {
          if (strcmp(BYX_Book_Conf.refIP[i], subscribedIP[j]) == 0)   // same ip
          {
            if (BYX_Book_Conf.refPort[i] == subscribedPort[j])
            {
              shouldIgnore = 0;
              break;
            }
            else
            {
              shouldIgnore = 1;
              iface = subsribedInterface[j];
            }
          }
        }
        
        if (shouldIgnore == 1)
        {
          addIgnoredFeed(topic_registry, *feed_id, VENUE_BYX, iface, BYX_Book_Conf.refPort[i], BYX_Book_Conf.refIP[i]);
          *feed_id = (*feed_id + 1);
          TraceLog(DEBUG_LEVEL, "InitializeMarketDataComponents(byx): Ignore %s:%d (interface: %s)\n", BYX_Book_Conf.refIP[i], BYX_Book_Conf.refPort[i], iface);
        }
      }
      break;
  }
}

/****************************************************************************
- Function name:  InitializeMarketDataComponents
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      This function must be call after all venue config loading
              functions are called. Because it uses ip/address config
              from each venue
****************************************************************************/
int InitializeMarketDataComponents()
{
  TraceLog(DEBUG_LEVEL, "Initializing DBL Filter Market Data Components...\n");
  
  if (InitARCA_BOOK_DataStructure() == ERROR) return ERROR;
  if (NYSE_InitDataStructure(BOOK_NYSE) == ERROR) return ERROR;
  if (NYSE_InitDataStructure(BOOK_AMEX) == ERROR) return ERROR;
  if (Init_DataStructure_EDGX_BOOK() == ERROR) return ERROR;
  if (Init_DataStructure_EDGA_BOOK() == ERROR) return ERROR;
  InitBATSZ_BOOK_DataStructure();
  InitBYX_BOOK_DataStructure();
  InitNASDAQ_BOOK_DataStructure();
  InitDataStructure_NDBX();
  InitDataStructure_PSX();
  InitUQDF_DataStructure();
  InitCQS_DataStructure();
  
  
  //-------------------------------------------------------------------
  //  Get all possible symbols from symbol mapping files (ARCA & NYSE)
  //-------------------------------------------------------------------
  int ret;
  int i,j;
  
  int arcaSymbolFilterId[MAX_STOCK_SYMBOL];
  int nyseSymbolFilterId[MAX_STOCK_SYMBOL];
  int amexSymbolFilterId[MAX_STOCK_SYMBOL];
  
  int largestArcaStockIndex = -1;
  int largestNyseStockIndex = -1;
  int largestAmexStockIndex = -1;

  char arcaSymbolList[MAX_STOCK_SYMBOL][9];
  char nyseSymbolList[MAX_STOCK_SYMBOL][9];
  char amexSymbolList[MAX_STOCK_SYMBOL][9];
  char dblMacAddress[32];
  int arcaSymbolIndex[MAX_STOCK_SYMBOL];
  int nyseSymbolIndex[MAX_STOCK_SYMBOL];
  int amexSymbolIndex[MAX_STOCK_SYMBOL];
  int arcaSymbolCount = 0;
  int nyseSymbolCount = 0;
  int amexSymbolCount = 0;
  int stockSymbolIndex;
  int feed_id = 0;
  
  TotalSymbolCount = 0;
  
  ret = LoadArcaSymbolsFromMappingFileToList(ARCA_BOOK_SYMBOL_INDEX_MAPPING_FILE, &arcaSymbolCount, arcaSymbolList, arcaSymbolIndex);
  if (ret == ERROR)
  {
    return ERROR;
  }
  
  ret = LoadNyseSymbolsFromMappingFileToList(NYSE_BOOK_SYMBOL_INDEX_MAPPING_FILE, &nyseSymbolCount, &amexSymbolCount, nyseSymbolList, amexSymbolList, nyseSymbolIndex, amexSymbolIndex);
  if (ret == ERROR)
  {
    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Loaded %d ARCA symbols from mapping file\n", arcaSymbolCount);
  TraceLog(DEBUG_LEVEL, "Loaded %d symbols from mapping file (%d NYSE, %d AMEX)\n", nyseSymbolCount + amexSymbolCount, nyseSymbolCount, amexSymbolCount);

  // -----------------------------------------------------------------------------------------------------
  // from Arca symbol list and Nyse symbol list, we will generate the whole complete comstock symbols
  // -----------------------------------------------------------------------------------------------------
  for (i = 0; i < MAX_STOCK_SYMBOL; i++)
  {
    arcaSymbolFilterId[i] = 0;
    nyseSymbolFilterId[i] = 0;
    amexSymbolFilterId[i] = 0;
  }

  for (i = 0; i < arcaSymbolCount; i++)
  {
    strncpy(AllSymbols[TotalSymbolCount], arcaSymbolList[i], SYMBOL_LEN);
    AllSymbols[TotalSymbolCount][8] = 0;
    
    stockSymbolIndex = GetStockSymbolIndex(AllSymbols[TotalSymbolCount]);
    arcaSymbolFilterId[stockSymbolIndex] = arcaSymbolIndex[i];
    if (largestArcaStockIndex < stockSymbolIndex)
    {
      largestArcaStockIndex = stockSymbolIndex;
    }
    TotalSymbolCount++;
  }
  largestArcaStockIndex++;
  
  // Merge NYSE symbols to the list in order to have the full list
  // also update nyse symbol mapping for using with the filter
  int isDuplicated;
  for (i = 0; i < nyseSymbolCount; i++)
  {
    isDuplicated = 0;
    for (j = 0; j < TotalSymbolCount; j++)
    {
      if (strncmp(AllSymbols[j], nyseSymbolList[i], SYMBOL_LEN) == 0)
      {
        isDuplicated = 1;
        break;
      }
    }
    
    stockSymbolIndex = GetStockSymbolIndex(nyseSymbolList[i]);
    if (isDuplicated == 1)
    {
      nyseSymbolFilterId[stockSymbolIndex] = nyseSymbolIndex[i];
    }
    else
    {
      strncpy(AllSymbols[TotalSymbolCount], nyseSymbolList[i], SYMBOL_LEN);
      AllSymbols[TotalSymbolCount][8] = 0;
      nyseSymbolFilterId[stockSymbolIndex] = nyseSymbolIndex[i];
      TotalSymbolCount++;
    }
    
    if (largestNyseStockIndex < stockSymbolIndex)
      largestNyseStockIndex = stockSymbolIndex;
  }
  largestNyseStockIndex++;
  
  // Merge AMEX symbols to the list in order to have the full list
  // also update amex symbol mapping for using with the filter
  for (i = 0; i < amexSymbolCount; i++)
  {
    isDuplicated = 0;
    for (j = 0; j < TotalSymbolCount; j++)
    {
      if (strncmp(AllSymbols[j], amexSymbolList[i], SYMBOL_LEN) == 0)
      {
        isDuplicated = 1;
        break;
      }
    }
    
    stockSymbolIndex = GetStockSymbolIndex(amexSymbolList[i]);
    if (isDuplicated == 1)
    {
      amexSymbolFilterId[stockSymbolIndex] = amexSymbolIndex[i];
    }
    else
    {
      strncpy(AllSymbols[TotalSymbolCount], amexSymbolList[i], SYMBOL_LEN);
      AllSymbols[TotalSymbolCount][8] = 0;
      amexSymbolFilterId[stockSymbolIndex] = amexSymbolIndex[i];
      TotalSymbolCount++;
    }
    
    if (largestAmexStockIndex < stockSymbolIndex)
      largestAmexStockIndex = stockSymbolIndex;
  }
  largestAmexStockIndex++;
  
  TraceLog(DEBUG_LEVEL, "Registered %d symbols to DBLFilter\n", TotalSymbolCount);

  //-------------------------------------------------------------------
  //    Done getting symbols
  //-------------------------------------------------------------------

    /* 
     ======================================
     Setup TopicRegistry config

     Basic steps:
     1) Create TopicRegistry
     2) Create SPSC queues
     3) Register SPSC queues
     4) Create mcast sources (with backups)
     5) Register mcast sources
     6) Create QSymbolMessageTopic
     7) Register topic (associates source with topic)
     8) Register queue for topic
     9) Build filters
     ======================================
     */
    
  // setup log queue 
  // this is where feed related message will be delivered 
  // e.g. sequence duplicates

  unsigned char* logq_ptr = spscAllocate(DBL_FILTER_LOG_SIZE);
  long* logq_seq = (long *) calloc(1, sizeof(long) * 1024); // 1MB buffer used to store reader/writer indexes without crossing page boundaries
  memset(logq_seq, 0, (sizeof(long) * 1024));

  DBLFilterMgmt.topic_registry = init_registry();

  init_logging_queue(DBLFilterMgmt.topic_registry, (int64_t) logq_ptr, DBL_FILTER_LOG_SIZE, (int64_t) logq_seq);

  DBLFilterMgmt.marketDataLogReceiveReader = createSpscReader((unsigned char*)logq_ptr, DBL_FILTER_LOG_SIZE, (long*)logq_seq);

  // register venues (do not change these names, they are important)
  
  register_venue(VenueIdToText[VENUE_NASDAQ], VENUE_NASDAQ);     // nasdaq
  register_venue(VenueIdToText[VENUE_ARCA],   VENUE_ARCA);       // arca xdp
  register_venue(VenueIdToText[VENUE_BATSZ],  VENUE_BATSZ);      // bats bzx
  register_venue(VenueIdToText[VENUE_BYX],    VENUE_BYX);        // bats byx
  register_venue(VenueIdToText[VENUE_EDGX],   VENUE_EDGX);       // direct edge-x
  register_venue(VenueIdToText[VENUE_EDGA],   VENUE_EDGA);       // direct edge-a
  register_venue(VenueIdToText[VENUE_NYSE],   VENUE_NYSE);       // nyse
  register_venue(VenueIdToText[VENUE_AMEX],   VENUE_AMEX);       // nyse amex
  register_venue(VenueIdToText[VENUE_RASH],   VENUE_RASH);       // rash
  register_venue(VenueIdToText[VENUE_NDBX],   VENUE_NDBX);       // nasdaq bx
  register_venue(VenueIdToText[VENUE_PSX],    VENUE_PSX);        // psx
  register_venue(VenueIdToText[VENUE_UQDF],   VENUE_UQDF);       // uqdf (nasdaq sip quote feed)
  register_venue(VenueIdToText[VENUE_CQS],    VENUE_CQS);        // cqs (nyse sip quote feed)
    
  for (i = 0; i < DBLFILTER_NUM_QUEUES; i++)
  {
    register_spsc_queue(DBLFilterMgmt.topic_registry, i, (long)FilterBuffers[i].marketDataReceiveQueue, DBLFILTER_MD_QUEUE_SIZE, (long)FilterBuffers[i].marketDataReceiveSeq);
    register_log_queue(DBLFilterMgmt.topic_registry, i);
    QueueSymbolList[i].count = 0; //Initially there is no symbol in the queue.
  }


  // add venue/symbol pairs that we are interested in
  int num_symbol_per_queue = TotalSymbolCount / DBLFILTER_NUM_QUEUES;
  int currentQueueIndex = -1;
  
  for (i = 0; i < TotalSymbolCount; i++)
  {
    stockSymbolIndex = GetStockSymbolIndex(AllSymbols[i]);
    register_symbol(AllSymbols[i], stockSymbolIndex);
    
    /*  Update Stock Symbol Index vs Feed ID mapping for all book
      This is needed for symbol flushing and recovery processing
      The mapping is based on the venue specification, so check if the spec is updated!
      - For Arca, impossible to map
      - For Nasdaq, we have only one feed, so no need to map */
      
    if ((i % num_symbol_per_queue == 0) && ((currentQueueIndex+1) != DBLFILTER_NUM_QUEUES))
    {
      currentQueueIndex++;
    }

    int j = QueueSymbolList[currentQueueIndex].count;
    QueueSymbolList[currentQueueIndex].stockSymbolIndexes[j] = stockSymbolIndex;
    strcpy(QueueSymbolList[currentQueueIndex].symbols[j], AllSymbols[i]);
    QueueSymbolList[currentQueueIndex].count++;
  }
  
  set_arca_symbol_mapping(DBLFilterMgmt.topic_registry, arcaSymbolFilterId, largestArcaStockIndex);
  set_nyse_symbol_mapping(DBLFilterMgmt.topic_registry, nyseSymbolFilterId, largestNyseStockIndex);
  set_amex_symbol_mapping(DBLFilterMgmt.topic_registry, amexSymbolFilterId, largestAmexStockIndex);
  
  /*
   ======================================
   Setup network config
   ======================================
   */
  
  // add feeds we are interested in
  // additionally add "ignored feeds"
  //    ignored feeds are feeds we are not interested in which unfortunately share the same multicast address (but different port)
  //    with feeds we are subscribed to -- we do not want the dbl firmware to deal with these packets so we subscribe to them with a no-op handler
  // array of symbol ids we are interested in for nyse/arca, empty array for other venues

  char ip_addr[32];
  char subscribedIP[100][16];
  char subsribedInterface[100][16];
  int subscribedPort[100];
  int countSubscribed = 0;
  
  // ----------------
  // arca
  // ----------------
  if (GetMacAddress(ARCA_Book_Conf.NICName, dblMacAddress) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Could not get MAC Address for interface named '%s'\n", ARCA_Book_Conf.NICName);
    return ERROR;
  }

  for (i = 0; i < MAX_ARCA_GROUPS; i++)
  {
    if (GetIPAddress(ARCA_Book_Conf.NICName, ip_addr) == ERROR)
    {
      TraceLog(ERROR_LEVEL, "Could not get IP Address for interface named '%s'\n", ARCA_Book_Conf.NICName);
      return ERROR;
    }

    addVenueFeed(DBLFilterMgmt.topic_registry, feed_id, VENUE_ARCA, ip_addr, ARCA_Book_Conf.group[i].dataPort, ARCA_Book_Conf.group[i].dataIP);
    SourceVenueMapping[feed_id] = BOOK_ARCA;
    VenueFeedMapping[BOOK_ARCA][feed_id++] = i;
    TraceLog(DEBUG_LEVEL, "InitializeMarketDataComponents(arca): Add %s:%d (interface %s)\n", ARCA_Book_Conf.group[i].dataIP, ARCA_Book_Conf.group[i].dataPort, ip_addr);
    
    if (ARCA_Book_Conf.group[i].backupIP[0] != 0)
    {
      if (IsSameMacAddress(dblMacAddress, ARCA_Book_Conf.backupNIC) == 0)
      {
        TraceLog(ERROR_LEVEL, "Currently DBLFilter only accepts network config for all MDCs on the same DBL port, please check the config for interface named: %s\n", ARCA_Book_Conf.backupNIC);
        return ERROR;
      }
      
      if (GetIPAddress(ARCA_Book_Conf.backupNIC, ip_addr) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "Could not get IP Address for interface named '%s'\n", ARCA_Book_Conf.backupNIC);
        return ERROR;
      }

      addCompanionVenueFeed(DBLFilterMgmt.topic_registry, feed_id - 1, VENUE_ARCA, ip_addr, ARCA_Book_Conf.group[i].backupPort, ARCA_Book_Conf.group[i].backupIP);
      TraceLog(DEBUG_LEVEL, "InitializeMarketDataComponents(arca): Add %s:%d (backup - interface %s)\n", ARCA_Book_Conf.group[i].backupIP, ARCA_Book_Conf.group[i].backupPort, ip_addr);
    }
  }
  
  // ----------------
  // nasdaq
  // ----------------
  if (IsSameMacAddress(dblMacAddress, NASDAQ_Book_Conf.Data.NICName) == 0)
  {
    TraceLog(ERROR_LEVEL, "Currently DBLFilter only accepts network config for all MDCs on the same DBL port, please check the config for interface named: %s\n", NASDAQ_Book_Conf.Data.NICName);
    return ERROR;
  }
  
  if (GetIPAddress(NASDAQ_Book_Conf.Data.NICName, ip_addr) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Could not get IP Address for interface named '%s'\n", NASDAQ_Book_Conf.Data.NICName);
    return ERROR;
  }

  addVenueFeed(DBLFilterMgmt.topic_registry, feed_id, VENUE_NASDAQ, ip_addr, NASDAQ_Book_Conf.Data.port, NASDAQ_Book_Conf.Data.ip);
  SourceVenueMapping[feed_id] = BOOK_NASDAQ;
  VenueFeedMapping[BOOK_NASDAQ][feed_id++] = 0;
  TraceLog(DEBUG_LEVEL, "InitializeMarketDataComponents(isld): Add %s:%d (interface: %s)\n", NASDAQ_Book_Conf.Data.ip, NASDAQ_Book_Conf.Data.port, ip_addr);
  
  if (NASDAQ_Book_Conf.Backup.ip[0] != 0)
  {
    if (IsSameMacAddress(dblMacAddress, NASDAQ_Book_Conf.Backup.NICName) == 0)
    {
      TraceLog(ERROR_LEVEL, "Currently DBLFilter only accepts network config for all MDCs on the same DBL port, please check the config for interface named: %s\n", NASDAQ_Book_Conf.Backup.NICName);
      return ERROR;
    }
    
    if (GetIPAddress(NASDAQ_Book_Conf.Backup.NICName, ip_addr) == ERROR)
    {
      TraceLog(ERROR_LEVEL, "Could not get IP Address for interface named '%s'\n", NASDAQ_Book_Conf.Backup.NICName);
      return ERROR;
    }
    
    addCompanionVenueFeed(DBLFilterMgmt.topic_registry, feed_id - 1, VENUE_NASDAQ, ip_addr, NASDAQ_Book_Conf.Backup.port, NASDAQ_Book_Conf.Backup.ip);
    TraceLog(DEBUG_LEVEL, "InitializeMarketDataComponents(isld): Add %s:%d (backup - interface: %s)\n", NASDAQ_Book_Conf.Backup.ip, NASDAQ_Book_Conf.Backup.port, ip_addr);
  }
  
  // ----------------
  // nasdaq bx
  // ----------------
  if (IsSameMacAddress(dblMacAddress, NDBX_Book_Conf.Data.NICName) == 0)
  {
    TraceLog(ERROR_LEVEL, "Currently DBLFilter only accepts network config for all MDCs on the same DBL port, please check the config for interface named: %s\n", NDBX_Book_Conf.Data.NICName);
    return ERROR;
  }
  
  if (GetIPAddress(NDBX_Book_Conf.Data.NICName, ip_addr) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Could not get IP Address for interface named '%s'\n", NDBX_Book_Conf.Data.NICName);
    return ERROR;
  }

  addVenueFeed(DBLFilterMgmt.topic_registry, feed_id, VENUE_NDBX, ip_addr, NDBX_Book_Conf.Data.port, NDBX_Book_Conf.Data.ip);
  SourceVenueMapping[feed_id] = BOOK_NDBX;
  VenueFeedMapping[BOOK_NDBX][feed_id++] = 0;
  TraceLog(DEBUG_LEVEL, "InitializeMarketDataComponents(bx): Add %s:%d (interface: %s)\n", NDBX_Book_Conf.Data.ip, NDBX_Book_Conf.Data.port, ip_addr);
  
  if (NDBX_Book_Conf.Backup.ip[0] != 0)
  {
    if (IsSameMacAddress(dblMacAddress, NDBX_Book_Conf.Backup.NICName) == 0)
    {
      TraceLog(ERROR_LEVEL, "Currently DBLFilter only accepts network config for all MDCs on the same DBL port, please check the config for interface named: %s\n", NDBX_Book_Conf.Backup.NICName);
      return ERROR;
    }
    
    if (GetIPAddress(NDBX_Book_Conf.Backup.NICName, ip_addr) == ERROR)
    {
      TraceLog(ERROR_LEVEL, "Could not get IP Address for interface named '%s'\n", NDBX_Book_Conf.Backup.NICName);
      return ERROR;
    }
    
    addCompanionVenueFeed(DBLFilterMgmt.topic_registry, feed_id - 1, VENUE_NDBX, ip_addr, NDBX_Book_Conf.Backup.port, NDBX_Book_Conf.Backup.ip);
    TraceLog(DEBUG_LEVEL, "InitializeMarketDataComponents(bx): Add %s:%d (backup - interface: %s)\n", NDBX_Book_Conf.Backup.ip, NDBX_Book_Conf.Backup.port, ip_addr);
  }
  
  // ----------------
  // PSX
  // ----------------
  if (IsSameMacAddress(dblMacAddress, PSX_Book_Conf.Data.NICName) == 0)
  {
    TraceLog(ERROR_LEVEL, "Currently DBLFilter only accepts network config for all MDCs on the same DBL port, please check the config for interface named: %s\n", PSX_Book_Conf.Data.NICName);
    return ERROR;
  }
  
  if (GetIPAddress(PSX_Book_Conf.Data.NICName, ip_addr) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Could not get IP Address for interface named '%s'\n", PSX_Book_Conf.Data.NICName);
    return ERROR;
  }

  addVenueFeed(DBLFilterMgmt.topic_registry, feed_id, VENUE_PSX, ip_addr, PSX_Book_Conf.Data.port, PSX_Book_Conf.Data.ip);
  SourceVenueMapping[feed_id] = BOOK_PSX;
  VenueFeedMapping[BOOK_PSX][feed_id++] = 0;
  TraceLog(DEBUG_LEVEL, "InitializeMarketDataComponents(isld): Add %s:%d (interface: %s)\n", PSX_Book_Conf.Data.ip, PSX_Book_Conf.Data.port, ip_addr);
  
  if (PSX_Book_Conf.Backup.ip[0] != 0)
  {
    if (IsSameMacAddress(dblMacAddress, PSX_Book_Conf.Backup.NICName) == 0)
    {
      TraceLog(ERROR_LEVEL, "Currently DBLFilter only accepts network config for all MDCs on the same DBL port, please check the config for interface named: %s\n", PSX_Book_Conf.Backup.NICName);
      return ERROR;
    }
    
    if (GetIPAddress(PSX_Book_Conf.Backup.NICName, ip_addr) == ERROR)
    {
      TraceLog(ERROR_LEVEL, "Could not get IP Address for interface named '%s'\n", PSX_Book_Conf.Backup.NICName);
      return ERROR;
    }
    
    addCompanionVenueFeed(DBLFilterMgmt.topic_registry, feed_id - 1, VENUE_PSX, ip_addr, PSX_Book_Conf.Backup.port, PSX_Book_Conf.Backup.ip);
    TraceLog(DEBUG_LEVEL, "InitializeMarketDataComponents(isld): Add %s:%d (backup - interface: %s)\n", PSX_Book_Conf.Backup.ip, PSX_Book_Conf.Backup.port, ip_addr);
  }
  
  // ----------------
  // batsz
  // ----------------
  countSubscribed = 0;
  if (IsSameMacAddress(dblMacAddress, BATSZ_Book_Conf.NICName) == 0)
  {
    TraceLog(ERROR_LEVEL, "Currently DBLFilter only accepts network config for all MDCs on the same DBL port, please check the config for interface named: %s\n", BATSZ_Book_Conf.NICName);
    return ERROR;
  }
  
  for (i = 0; i < MAX_BATSZ_UNIT; i++)
  {
    if (BATSZ_Book_Conf.group[i].groupPermission == ENABLE)
    {
      if (GetIPAddress(BATSZ_Book_Conf.NICName, ip_addr) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "Could not get IP Address for interface named '%s'\n", BATSZ_Book_Conf.NICName);
        return ERROR;
      }
      
      addVenueFeed(DBLFilterMgmt.topic_registry, feed_id, VENUE_BATSZ, ip_addr, BATSZ_Book_Conf.group[i].dataPort, BATSZ_Book_Conf.group[i].dataIP);
      SourceVenueMapping[feed_id] = BOOK_BATSZ;
      VenueFeedMapping[BOOK_BATSZ][feed_id++] = i;
      TraceLog(DEBUG_LEVEL, "InitializeMarketDataComponents(batsz): Add %s:%d (interface: %s)\n", BATSZ_Book_Conf.group[i].dataIP, BATSZ_Book_Conf.group[i].dataPort, ip_addr);
      
      strcpy(subscribedIP[countSubscribed], BATSZ_Book_Conf.group[i].dataIP);
      strcpy(subsribedInterface[countSubscribed], ip_addr);
      subscribedPort[countSubscribed++] = BATSZ_Book_Conf.group[i].dataPort;
      
      if (BATSZ_Book_Conf.group[i].backupIP[0] != 0)
      {
        if (IsSameMacAddress(dblMacAddress, BATSZ_Book_Conf.backupNIC) == 0)
        {
          TraceLog(ERROR_LEVEL, "Currently DBLFilter only accepts network config for all MDCs on the same DBL port, please check the config for interface named: %s\n", BATSZ_Book_Conf.backupNIC);
          return ERROR;
        }
        
        if (GetIPAddress(BATSZ_Book_Conf.backupNIC, ip_addr) == ERROR)
        {
          TraceLog(ERROR_LEVEL, "Could not get IP Address for interface named '%s'\n", BATSZ_Book_Conf.backupNIC);
          return ERROR;
        }
        
        addCompanionVenueFeed(DBLFilterMgmt.topic_registry, feed_id - 1, VENUE_BATSZ, ip_addr, BATSZ_Book_Conf.group[i].backupPort, BATSZ_Book_Conf.group[i].backupIP);
        TraceLog(DEBUG_LEVEL, "InitializeMarketDataComponents(batsz): Add %s:%d (backup - interface %s)\n", BATSZ_Book_Conf.group[i].backupIP, BATSZ_Book_Conf.group[i].backupPort, ip_addr);
        
        strcpy(subscribedIP[countSubscribed], BATSZ_Book_Conf.group[i].backupIP);
        strcpy(subsribedInterface[countSubscribed], ip_addr);
        subscribedPort[countSubscribed++] = BATSZ_Book_Conf.group[i].backupPort;
      }
    }
  }
  
  IgnoreVenueFeeds(BOOK_BATSZ, countSubscribed, subscribedIP, subscribedPort, subsribedInterface, DBLFilterMgmt.topic_registry, &feed_id);
  
  // ----------------
  // byx
  // ----------------
  countSubscribed = 0;
  if (IsSameMacAddress(dblMacAddress, BYX_Book_Conf.NICName) == 0)
  {
    TraceLog(ERROR_LEVEL, "Currently DBLFilter only accepts network config for all MDCs on the same DBL port, please check the config for interface named: %s\n", BYX_Book_Conf.NICName);
    return ERROR;
  }
  
  for (i = 0; i < MAX_BYX_UNIT; i++)
  {
    // TraceLog(DEBUG_LEVEL, "(%s) group %d have permission %d\n", __func__, i, BYX_Book_Conf.group[i].groupPermission);
    if (BYX_Book_Conf.group[i].groupPermission == ENABLE)
    {
      if (GetIPAddress(BYX_Book_Conf.NICName, ip_addr) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "Could not get IP Address for interface named '%s'\n", BYX_Book_Conf.NICName);
        return ERROR;
      }
      
      addVenueFeed(DBLFilterMgmt.topic_registry, feed_id, VENUE_BYX, ip_addr, BYX_Book_Conf.group[i].dataPort, BYX_Book_Conf.group[i].dataIP);
      SourceVenueMapping[feed_id] = BOOK_BYX;
      VenueFeedMapping[BOOK_BYX][feed_id++] = i;
      TraceLog(DEBUG_LEVEL, "InitializeMarketDataComponents(byx): Add %s:%d (interface: %s)\n", BYX_Book_Conf.group[i].dataIP, BYX_Book_Conf.group[i].dataPort, ip_addr);
      
      strcpy(subscribedIP[countSubscribed], BYX_Book_Conf.group[i].dataIP);
      strcpy(subsribedInterface[countSubscribed], ip_addr);
      subscribedPort[countSubscribed++] = BYX_Book_Conf.group[i].dataPort;
      
      if (BYX_Book_Conf.group[i].backupIP[0] != 0)
      {
        if (IsSameMacAddress(dblMacAddress, BYX_Book_Conf.backupNIC) == 0)
        {
          TraceLog(ERROR_LEVEL, "Currently DBLFilter only accepts network config for all MDCs on the same DBL port, please check the config for interface named: %s\n", BYX_Book_Conf.backupNIC);
          return ERROR;
        }
        
        if (GetIPAddress(BYX_Book_Conf.backupNIC, ip_addr) == ERROR)
        {
          TraceLog(ERROR_LEVEL, "Could not get IP Address for interface named '%s'\n", BYX_Book_Conf.backupNIC);
          return ERROR;
        }
        
        addCompanionVenueFeed(DBLFilterMgmt.topic_registry, feed_id - 1, VENUE_BYX, ip_addr, BYX_Book_Conf.group[i].backupPort, BYX_Book_Conf.group[i].backupIP);
        TraceLog(DEBUG_LEVEL, "InitializeMarketDataComponents(byx): Add %s:%d (backup - interface %s)\n", BYX_Book_Conf.group[i].backupIP, BYX_Book_Conf.group[i].backupPort, ip_addr);
        
        strcpy(subscribedIP[countSubscribed], BYX_Book_Conf.group[i].backupIP);
        strcpy(subsribedInterface[countSubscribed], ip_addr);
        subscribedPort[countSubscribed++] = BYX_Book_Conf.group[i].backupPort;
      }
    }
  }
  
  IgnoreVenueFeeds(BOOK_BYX, countSubscribed, subscribedIP, subscribedPort, subsribedInterface, DBLFilterMgmt.topic_registry, &feed_id);
  
  // ----------------
  // nyse
  // ----------------
  countSubscribed = 0;
  if (IsSameMacAddress(dblMacAddress, NyseBookConf.NICName) == 0)
  {
    TraceLog(ERROR_LEVEL, "Currently DBLFilter only accepts network config for all MDCs on the same DBL port, please check the config for interface named: %s\n", NyseBookConf.NICName);
    return ERROR;
  }
  
  for (i = 0; i < MAX_NYSE_BOOK_GROUPS; i++)
  {
    if (NyseBookConf.group[i].groupPermission == ENABLE)
    {
      if (GetIPAddress(NyseBookConf.NICName, ip_addr) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "Could not get IP Address for interface named '%s'\n", NyseBookConf.NICName);
        return ERROR;
      }
      
      addVenueFeed(DBLFilterMgmt.topic_registry, feed_id, VENUE_NYSE, ip_addr, NyseBookConf.group[i].receiveDataPort, NyseBookConf.group[i].receiveDataIP);
      SourceVenueMapping[feed_id] = BOOK_NYSE;
      VenueFeedMapping[BOOK_NYSE][feed_id++] = i;
      TraceLog(DEBUG_LEVEL, "InitializeMarketDataComponents(nyse): Add %s:%d (interface %s)\n", NyseBookConf.group[i].receiveDataIP, NyseBookConf.group[i].receiveDataPort, ip_addr);
      
      strcpy(subscribedIP[countSubscribed], NyseBookConf.group[i].receiveDataIP);
      strcpy(subsribedInterface[countSubscribed], ip_addr);
      subscribedPort[countSubscribed++] = NyseBookConf.group[i].receiveDataPort;
      
      if (NyseBookConf.group[i].backupIP[0] != 0)
      {
        if (IsSameMacAddress(dblMacAddress, NyseBookConf.backupNIC) == 0)
        {
          TraceLog(ERROR_LEVEL, "Currently DBLFilter only accepts network config for all MDCs on the same DBL port, please check the config for interface named: %s\n", NyseBookConf.backupNIC);
          return ERROR;
        }
        
        if (GetIPAddress(NyseBookConf.backupNIC, ip_addr) == ERROR)
        {
          TraceLog(ERROR_LEVEL, "Could not get IP Address for interface named '%s'\n", NyseBookConf.backupNIC);
          return ERROR;
        }
        
        addCompanionVenueFeed(DBLFilterMgmt.topic_registry, feed_id - 1, VENUE_NYSE, ip_addr, NyseBookConf.group[i].backupPort, NyseBookConf.group[i].backupIP);
        TraceLog(DEBUG_LEVEL, "InitializeMarketDataComponents(nyse): Add %s:%d (backup - interface %s)\n", NyseBookConf.group[i].backupIP, NyseBookConf.group[i].backupPort, ip_addr);
        
        strcpy(subscribedIP[countSubscribed], NyseBookConf.group[i].backupIP);
        strcpy(subsribedInterface[countSubscribed], ip_addr);
        subscribedPort[countSubscribed++] = NyseBookConf.group[i].backupPort;
      }
    }
  }
  
  IgnoreVenueFeeds(BOOK_NYSE, countSubscribed, subscribedIP, subscribedPort, subsribedInterface, DBLFilterMgmt.topic_registry, &feed_id);
  
  // ----------------
  // nyse amex
  // ----------------
  if (IsSameMacAddress(dblMacAddress, AmexBookConf.NICName) == 0)
  {
    TraceLog(ERROR_LEVEL, "Currently DBLFilter only accepts network config for all MDCs on the same DBL port, please check the config for interface named: %s\n", AmexBookConf.NICName);
    return ERROR;
  }
  
  if (GetIPAddress(AmexBookConf.NICName, ip_addr) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Could not get IP Address for interface named '%s'\n", AmexBookConf.NICName);
    return ERROR;
  }
  
  i = 0;  //amex has only one group
  addVenueFeed(DBLFilterMgmt.topic_registry, feed_id, VENUE_AMEX, ip_addr, AmexBookConf.group[i].receiveDataPort, AmexBookConf.group[i].receiveDataIP);
  SourceVenueMapping[feed_id] = BOOK_AMEX;
  VenueFeedMapping[BOOK_AMEX][feed_id++] = i;
  TraceLog(DEBUG_LEVEL, "InitializeMarketDataComponents(amex): Add %s:%d (interface %s)\n", AmexBookConf.group[i].receiveDataIP, AmexBookConf.group[i].receiveDataPort, ip_addr);
  
  if (AmexBookConf.group[i].backupIP[0] != 0)
  {
    if (IsSameMacAddress(dblMacAddress, AmexBookConf.backupNIC) == 0)
    {
      TraceLog(ERROR_LEVEL, "Currently DBLFilter only accepts network config for all MDCs on the same DBL port, please check the config for interface named: %s\n", AmexBookConf.backupNIC);
      return ERROR;
    }
    
    if (GetIPAddress(AmexBookConf.backupNIC, ip_addr) == ERROR)
    {
      TraceLog(ERROR_LEVEL, "Could not get IP Address for interface named '%s'\n", AmexBookConf.backupNIC);
      return ERROR;
    }
    
    addCompanionVenueFeed(DBLFilterMgmt.topic_registry, feed_id - 1, VENUE_AMEX, ip_addr, AmexBookConf.group[i].backupPort, AmexBookConf.group[i].backupIP);
    TraceLog(DEBUG_LEVEL, "InitializeMarketDataComponents(amex): Add %s:%d (backup - interface %s)\n", AmexBookConf.group[i].backupIP, AmexBookConf.group[i].backupPort, ip_addr);
  }
  
  // ----------------
  // edgx
  // ----------------
  countSubscribed = 0;
  
  if (IsSameMacAddress(dblMacAddress, EDGX_BOOK_Conf.NICName) == 0)
  {
    TraceLog(ERROR_LEVEL, "Currently DBLFilter only accepts network config for all MDCs on the same DBL port, please check the config for interface named: %s\n", EDGX_BOOK_Conf.NICName);
    return ERROR;
  }
  
  for (i = 0; i < MAX_EDGX_BOOK_GROUP; i++)
  {
    if (EDGX_BOOK_Conf.group[i].groupPermission == ENABLE)
    {
      if (GetIPAddress(EDGX_BOOK_Conf.NICName, ip_addr) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "Could not get IP Address for interface named '%s'\n", EDGX_BOOK_Conf.NICName);
        return ERROR;
      }

      addVenueFeed(DBLFilterMgmt.topic_registry, feed_id, VENUE_EDGX, ip_addr, EDGX_BOOK_Conf.group[i].dataPort, EDGX_BOOK_Conf.group[i].dataIP);
      SourceVenueMapping[feed_id] = BOOK_EDGX;
      VenueFeedMapping[BOOK_EDGX][feed_id++] = i;
      TraceLog(DEBUG_LEVEL, "InitializeMarketDataComponents(edgx): Add %s:%d (interface %s)\n", EDGX_BOOK_Conf.group[i].dataIP, EDGX_BOOK_Conf.group[i].dataPort, ip_addr);
      
      strcpy(subscribedIP[countSubscribed], EDGX_BOOK_Conf.group[i].dataIP);
      strcpy(subsribedInterface[countSubscribed], ip_addr);
      subscribedPort[countSubscribed++] = EDGX_BOOK_Conf.group[i].dataPort;
        
      if (EDGX_BOOK_Conf.group[i].backupIP[0] != 0)
      {
        if (IsSameMacAddress(dblMacAddress, EDGX_BOOK_Conf.backupNIC) == 0)
        {
          TraceLog(ERROR_LEVEL, "Currently DBLFilter only accepts network config for all MDCs on the same DBL port, please check the config for interface named: %s\n", EDGX_BOOK_Conf.backupNIC);
          return ERROR;
        }
        
        if (GetIPAddress(EDGX_BOOK_Conf.backupNIC, ip_addr) == ERROR)
        {
          TraceLog(ERROR_LEVEL, "Could not get IP Address for interface named '%s'\n", EDGX_BOOK_Conf.backupNIC);
          return ERROR;
        }
        
        addCompanionVenueFeed(DBLFilterMgmt.topic_registry, feed_id - 1, VENUE_EDGX, ip_addr, EDGX_BOOK_Conf.group[i].backupPort, EDGX_BOOK_Conf.group[i].backupIP);
        TraceLog(DEBUG_LEVEL, "InitializeMarketDataComponents(edgx): Add %s:%d (backup - interface %s)\n", EDGX_BOOK_Conf.group[i].backupIP, EDGX_BOOK_Conf.group[i].backupPort, ip_addr);
        
        strcpy(subscribedIP[countSubscribed], EDGX_BOOK_Conf.group[i].backupIP);
        strcpy(subsribedInterface[countSubscribed], ip_addr);
        subscribedPort[countSubscribed++] = EDGX_BOOK_Conf.group[i].backupPort;
      }
    }
  }

  IgnoreVenueFeeds(BOOK_EDGX, countSubscribed, subscribedIP, subscribedPort, subsribedInterface, DBLFilterMgmt.topic_registry, &feed_id);
  
  // ----------------
  // edga
  // ----------------
  countSubscribed = 0;
  
  if (IsSameMacAddress(dblMacAddress, EDGA_BOOK_Conf.NICName) == 0)
  {
    TraceLog(ERROR_LEVEL, "Currently DBLFilter only accepts network config for all MDCs on the same DBL port, please check the config for interface named: %s\n", EDGA_BOOK_Conf.NICName);
    return ERROR;
  }
  
  for (i = 0; i < MAX_EDGA_BOOK_GROUP; i++)
  {
    if (EDGA_BOOK_Conf.group[i].groupPermission == ENABLE)
    {
      if (GetIPAddress(EDGA_BOOK_Conf.NICName, ip_addr) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "Could not get IP Address for interface named '%s'\n", EDGA_BOOK_Conf.NICName);
        return ERROR;
      }

      addVenueFeed(DBLFilterMgmt.topic_registry, feed_id, VENUE_EDGA, ip_addr, EDGA_BOOK_Conf.group[i].dataPort, EDGA_BOOK_Conf.group[i].dataIP);
      SourceVenueMapping[feed_id] = BOOK_EDGA;
      VenueFeedMapping[BOOK_EDGA][feed_id++] = i;
      TraceLog(DEBUG_LEVEL, "InitializeMarketDataComponents(edga): Add %s:%d (interface %s)\n", EDGA_BOOK_Conf.group[i].dataIP, EDGA_BOOK_Conf.group[i].dataPort, ip_addr);
      
      strcpy(subscribedIP[countSubscribed], EDGA_BOOK_Conf.group[i].dataIP);
      strcpy(subsribedInterface[countSubscribed], ip_addr);
      subscribedPort[countSubscribed++] = EDGA_BOOK_Conf.group[i].dataPort;
        
      if (EDGA_BOOK_Conf.group[i].backupIP[0] != 0)
      {
        if (IsSameMacAddress(dblMacAddress, EDGA_BOOK_Conf.backupNIC) == 0)
        {
          TraceLog(ERROR_LEVEL, "Currently DBLFilter only accepts network config for all MDCs on the same DBL port, please check the config for interface named: %s\n", EDGA_BOOK_Conf.backupNIC);
          return ERROR;
        }
        
        if (GetIPAddress(EDGA_BOOK_Conf.backupNIC, ip_addr) == ERROR)
        {
          TraceLog(ERROR_LEVEL, "Could not get IP Address for interface named '%s'\n", EDGA_BOOK_Conf.backupNIC);
          return ERROR;
        }
        
        addCompanionVenueFeed(DBLFilterMgmt.topic_registry, feed_id - 1, VENUE_EDGA, ip_addr, EDGA_BOOK_Conf.group[i].backupPort, EDGA_BOOK_Conf.group[i].backupIP);
        TraceLog(DEBUG_LEVEL, "InitializeMarketDataComponents(edga): Add %s:%d (backup - interface %s)\n", EDGA_BOOK_Conf.group[i].backupIP, EDGA_BOOK_Conf.group[i].backupPort, ip_addr);
        
        strcpy(subscribedIP[countSubscribed], EDGA_BOOK_Conf.group[i].backupIP);
        strcpy(subsribedInterface[countSubscribed], ip_addr);
        subscribedPort[countSubscribed++] = EDGA_BOOK_Conf.group[i].backupPort;
      }
    }
  }

  IgnoreVenueFeeds(BOOK_EDGA, countSubscribed, subscribedIP, subscribedPort, subsribedInterface, DBLFilterMgmt.topic_registry, &feed_id);
  
  // ----------------
  // cqs
  // ----------------
  if (IsSameMacAddress(dblMacAddress, CQS_Conf.NICName) == 0)
  {
    TraceLog(ERROR_LEVEL, "Currently DBLFilter only accepts network config for all MDCs on the same DBL port, please check the config for interface named: %s\n", CQS_Conf.NICName);
    return ERROR;
  }
  
  if (GetIPAddress(CQS_Conf.NICName, ip_addr) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Could not get IP Address for interface named '%s'\n", CQS_Conf.NICName);
    return ERROR;
  }
  
  for (i = 0; i < MAX_CQS_MULTICAST_LINES; i++)
  {
    //Check duplicate ip address in multicast groups
    char isDuplicated = 0;
    
    for (j = 0; j < MAX_CQS_MULTICAST_LINES; j++)
    {
      if (j != i && CQS_Conf.group[j].groupPermission == ENABLE)
      {
        if (strcmp(CQS_Conf.group[i].ip, CQS_Conf.group[j].ip) == 0) //Duplicated
        {
          isDuplicated = 1;
          break;
        }
      }
    }
    
    if (CQS_Conf.group[i].groupPermission == DISABLE)
    {
      if (isDuplicated == 1)
      {
        addIgnoredFeed(DBLFilterMgmt.topic_registry, feed_id, VENUE_CQS, ip_addr, CQS_Conf.group[i].port, CQS_Conf.group[i].ip);
        SourceVenueMapping[feed_id] = FEED_CQS;
        VenueFeedMapping[FEED_CQS][feed_id++] = i;
        TraceLog(DEBUG_LEVEL, "InitializeMarketDataComponents(cqs): Ignore %s:%d (interface %s)\n", CQS_Conf.group[i].ip, CQS_Conf.group[i].port, ip_addr);
      }
    }
    else
    {
      addVenueFeed(DBLFilterMgmt.topic_registry, feed_id, VENUE_CQS, ip_addr, CQS_Conf.group[i].port, CQS_Conf.group[i].ip);
      SourceVenueMapping[feed_id] = FEED_CQS;
      VenueFeedMapping[FEED_CQS][feed_id++] = i;
      TraceLog(DEBUG_LEVEL, "InitializeMarketDataComponents(cqs): Add %s:%d (interface %s)\n", CQS_Conf.group[i].ip, CQS_Conf.group[i].port, ip_addr);
    }
  }

  // ----------------
  // uqdf
  // ----------------
  if (IsSameMacAddress(dblMacAddress, UQDF_Conf.NICName) == 0)
  {
    TraceLog(ERROR_LEVEL, "Currently DBLFilter only accepts network config for all MDCs on the same DBL port, please check the config for interface named: %s\n", UQDF_Conf.NICName);
    return ERROR;
  }
  
  if (GetIPAddress(UQDF_Conf.NICName, ip_addr) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Could not get IP Address for interface named '%s'\n", UQDF_Conf.NICName);
    return ERROR;
  }
  
  for (i = 0; i < MAX_UQDF_CHANNELS; i++)
  {
    //Check duplicate ip address in multicast groups
    char isDuplicated = 0;
    
    for (j = 0; j < MAX_UQDF_CHANNELS; j++)
    {
      if (j != i && UQDF_Conf.group[j].groupPermission == ENABLE)
      {
        if (strcmp(UQDF_Conf.group[i].ip, UQDF_Conf.group[j].ip) == 0) //Duplicated
        {
          isDuplicated = 1;
          break;
        }
      }
    }
    
    if (UQDF_Conf.group[i].groupPermission == DISABLE)
    {
      if (isDuplicated == 1)
      {
        addIgnoredFeed(DBLFilterMgmt.topic_registry, feed_id, VENUE_UQDF, ip_addr, UQDF_Conf.group[i].port, UQDF_Conf.group[i].ip);
        SourceVenueMapping[feed_id] = FEED_UQDF;
        VenueFeedMapping[FEED_UQDF][feed_id++] = i;
        TraceLog(DEBUG_LEVEL, "InitializeMarketDataComponents(uqdf): Ignore %s:%d (interface %s)\n", UQDF_Conf.group[i].ip, UQDF_Conf.group[i].port, ip_addr);
      }
    }
    else
    {
      addVenueFeed(DBLFilterMgmt.topic_registry, feed_id, VENUE_UQDF, ip_addr, UQDF_Conf.group[i].port, UQDF_Conf.group[i].ip);
      SourceVenueMapping[feed_id] = FEED_UQDF;
      VenueFeedMapping[FEED_UQDF][feed_id++] = i;
      TraceLog(DEBUG_LEVEL, "InitializeMarketDataComponents(uqdf): Add %s:%d (interface %s)\n", UQDF_Conf.group[i].ip, UQDF_Conf.group[i].port, ip_addr);
    }
  }
  
  register_ignored_feeds(DBLFilterMgmt.topic_registry);
  
  TraceLog(DEBUG_LEVEL, "InitializeMarketDataComponents: Added %d feeds.\n", feed_id);

  return SUCCESS;
}

int FinalizeDBLFilter() {

  // Note: nano_time is not attached to the wall clock, that's why we do this
  // instead of just calling nano_time.
  long now = hbitime_micros() * 1000;
  build_filters(DBLFilterMgmt.topic_registry, now);

  return SUCCESS;
}


int32_t DBLFilter_ParsePacketSequenceError(const uint8_t *buffer_ptr, int queueId)
{
  const uint8_t *start_ptr = buffer_ptr;

  uint32_t source_id = *((uint32_t*)buffer_ptr);
  int venue_id = SourceVenueMapping[source_id];
  int feed_id = VenueFeedMapping[venue_id][source_id];
  buffer_ptr += 4;

  // uint32_t topic_id = *((uint32_t*)buffer_ptr);
  buffer_ptr += 4;

  //session id 10 bytes (valid for NASDAQ only, empty string if it's other venue)
  buffer_ptr += 10;

  uint64_t time = *((uint64_t*)buffer_ptr);
  buffer_ptr += 8;

  uint64_t expected = *((uint64_t*)buffer_ptr);
  buffer_ptr += 8;

  uint64_t received = *((uint64_t*)buffer_ptr);
  buffer_ptr += 8;

  uint8_t error_type = *buffer_ptr;
  buffer_ptr += 1;

  /**
    DBL Filter drops packet sequence error log into all Data Consumer threads and Log consumer thread:
      - On the Log-Consumer thread, just show the log
      - On Consumer thread: flush symbols belong to that filter queue when cumulative gap exceeds the threshold
  **/
  
  switch (error_type)
  {
    case 'D': // Sequence duplicate
      if (queueId == -1)
      {
        TraceLog(ERROR_LEVEL, "venue: %s, feed_id: %d, sequence duplicate at %lu: expected %lu, received %lu\n", MdcIdToText[venue_id], feed_id, time, expected, received);
        DisconnectFromVenue(venue_id, -1);
      }
      break;
    case 'G': // Gap
      if (queueId == -1)
      {
        switch (venue_id)
        {
          case BOOK_ARCA:
          case BOOK_NASDAQ:
          case BOOK_NYSE:
          case BOOK_AMEX:
          case BOOK_EDGX:
          case BOOK_EDGA:
          case BOOK_NDBX:
          case BOOK_PSX:
          case BOOK_BATSZ:
          case BOOK_BYX:
            TraceLog(WARN_LEVEL, "venue: %s, feed_id: %d sequence gap at %lu: (%lu..%lu), missed %lu.\n", MdcIdToText[venue_id], feed_id, time, expected, received, received-expected);
            break;
          default: //should be CQS/UQDF
            break;
        }
        
        break;  //This is very important, we do not handle gap on log-consumer thread, just show the log!
      }
      
      switch (venue_id)
      {
        case BOOK_ARCA:
          if (BookStatusMgmt[BOOK_ARCA].isConnected == DISCONNECTED) break;
          
          ARCA_Book_Conf.group[feed_id].lossCounter[queueId] += (received - expected);
          
          if (ARCA_Book_Conf.group[feed_id].lossCounter[queueId] > ARCA_Book_Conf.recoveryThreshold)
          {
            FlushVenueSymbolsOnQueue(BOOK_ARCA, queueId);
            ARCA_Book_Conf.group[feed_id].lossCounter[queueId] = 0;
          }
          break;
        case BOOK_NASDAQ:
          if (BookStatusMgmt[BOOK_NASDAQ].isConnected == DISCONNECTED) break;
          
          NASDAQ_Book_Conf.lossCounter[queueId] += (received - expected);
          
          if (NASDAQ_Book_Conf.lossCounter[queueId] > NASDAQ_Book_Conf.recoveryThreshold)
          {
            FlushVenueSymbolsOnQueue(BOOK_NASDAQ, queueId);
            NASDAQ_Book_Conf.lossCounter[queueId] = 0;
          }
          break;
        case BOOK_NDBX:
          if (BookStatusMgmt[BOOK_NDBX].isConnected == DISCONNECTED) break;
          
          NDBX_Book_Conf.lossCounter[queueId] += (received - expected);
          
          if (NDBX_Book_Conf.lossCounter[queueId] > NDBX_Book_Conf.recoveryThreshold)
          {
            FlushVenueSymbolsOnQueue(BOOK_NDBX, queueId);
            NDBX_Book_Conf.lossCounter[queueId] = 0;
          }
          break;
        case BOOK_PSX:
          if (BookStatusMgmt[BOOK_PSX].isConnected == DISCONNECTED) break;
          
          PSX_Book_Conf.lossCounter[queueId] += (received - expected);
          
          if (PSX_Book_Conf.lossCounter[queueId] > PSX_Book_Conf.recoveryThreshold)
          {
            FlushVenueSymbolsOnQueue(BOOK_PSX, queueId);
            PSX_Book_Conf.lossCounter[queueId] = 0;
          }
          break;
        case BOOK_EDGX:
          if (BookStatusMgmt[BOOK_EDGX].isConnected == DISCONNECTED) break;
          
          EDGX_BOOK_Conf.group[feed_id].lossCounter[queueId] += (received - expected);
          
          if (EDGX_BOOK_Conf.group[feed_id].lossCounter[queueId] > EDGX_BOOK_Conf.recoveryThreshold)
          {
            FlushVenueSymbolsOnQueue(BOOK_EDGX, queueId);
            EDGX_BOOK_Conf.group[feed_id].lossCounter[queueId] = 0;
          }
          break;
        case BOOK_EDGA:
          if (BookStatusMgmt[BOOK_EDGA].isConnected == DISCONNECTED) break;
          
          EDGA_BOOK_Conf.group[feed_id].lossCounter[queueId] += (received - expected);
          
          if (EDGA_BOOK_Conf.group[feed_id].lossCounter[queueId] > EDGA_BOOK_Conf.recoveryThreshold)
          {
            FlushVenueSymbolsOnQueue(BOOK_EDGA, queueId);
            EDGA_BOOK_Conf.group[feed_id].lossCounter[queueId] = 0;
          }
          break;
        case BOOK_BATSZ:
          if (BookStatusMgmt[BOOK_BATSZ].isConnected == DISCONNECTED) break;

          BATSZ_Book_Conf.group[feed_id].lossCounter[queueId] += (received - expected);
          
          if (BATSZ_Book_Conf.group[feed_id].lossCounter[queueId] > BATSZ_Book_Conf.recoveryThreshold)
          {
            FlushVenueSymbolsOnQueue(BOOK_BATSZ, queueId);
            BATSZ_Book_Conf.group[feed_id].lossCounter[queueId] = 0;
          }
          break;
        case BOOK_BYX:
          if (BookStatusMgmt[BOOK_BYX].isConnected == DISCONNECTED) break;

          BYX_Book_Conf.group[feed_id].lossCounter[queueId] += (received - expected);
          
          if (BYX_Book_Conf.group[feed_id].lossCounter[queueId] > BYX_Book_Conf.recoveryThreshold)
          {
            FlushVenueSymbolsOnQueue(BOOK_BYX, queueId);
            BYX_Book_Conf.group[feed_id].lossCounter[queueId] = 0;
          }
          break;
        case BOOK_NYSE:
          if (BookStatusMgmt[BOOK_NYSE].isConnected == DISCONNECTED) break;

          NyseBookConf.group[feed_id].lossCounter[queueId] += (received - expected);
          
          if (NyseBookConf.group[feed_id].lossCounter[queueId] > NyseBookConf.recoveryThreshold)
          {
            FlushVenueSymbolsOnQueue(BOOK_NYSE, queueId);
            NyseBookConf.group[feed_id].lossCounter[queueId] = 0;
          }
          break;
        case BOOK_AMEX:
          if (BookStatusMgmt[BOOK_AMEX].isConnected == DISCONNECTED) break;

          AmexBookConf.group[feed_id].lossCounter[queueId] += (received - expected);
          
          if (AmexBookConf.group[feed_id].lossCounter[queueId] > AmexBookConf.recoveryThreshold)
          {
            FlushVenueSymbolsOnQueue(BOOK_AMEX, queueId);
            AmexBookConf.group[feed_id].lossCounter[queueId] = 0;
          }
          break;
        default:  //Should be CQS or UQDF
          //TraceLog(WARN_LEVEL, "venue: %s, feed_id: %d sequence gap at %lu: (%lu..%lu), missed %lu\n", MdcIdToText[venue_id], feed_id, time, expected, received, received-expected);
          break;
      }
      break;
    case 'F': // First sequence
      //TraceLog(DEBUG_LEVEL, "venue: %s, feed_id: %d first sequence number at %lu: %lu\n", MdcIdToText[venue_id], feed_id, time, received);
      break;
    case 'R': // Sequence reset
      if (queueId == 0)
        TraceLog(DEBUG_LEVEL, "venue: %s, feed_id: %d sequence number reset at %lu: %lu\n", MdcIdToText[venue_id], feed_id, time, received);
      break;
    case 'Q':
      if (queueId == 0)
        TraceLog(ERROR_LEVEL, "venue: %s, feed_id: %d SPSC queue full at %lu! Highest seqn dropped %lu\n", MdcIdToText[venue_id], feed_id, time, received);
      break;
    default:
      if (queueId == 0)
        TraceLog(ERROR_LEVEL, "venue_id: %d, feed_id: %d unknown error code %c (time,expected,received) = (%lu, %lu, %lu)\n", venue_id, feed_id, error_type, time, expected, received);
      break;
    }    
    
    return buffer_ptr - start_ptr;
}

int32_t DBLFilter_ParseRawLogMessage(const uint8_t *buffer_ptr)
{
  const uint8_t *start_ptr = buffer_ptr;

  uint32_t source_id = *((uint32_t*)buffer_ptr);
  int venue_id = SourceVenueMapping[source_id];
  
  int feed_id = VenueFeedMapping[venue_id][source_id];
  buffer_ptr += 4;

  //uint64_t time = *((uint64_t*)buffer_ptr);
  buffer_ptr += 8;
       
  // log levels: 0 - trace, 1 - debug, 2 - info, 3 - warn, 4 - error
  uint32_t log_level = *((uint32_t*)buffer_ptr);
  buffer_ptr += 4;

  uint32_t msg_len = *((uint32_t*)buffer_ptr);
  buffer_ptr += 4;

  char msg[msg_len+1];
  strncpy(msg, (char *)buffer_ptr, msg_len);
  buffer_ptr += msg_len;

  int level;
  switch (log_level) {
    case 0:
    case 1:
      level = DEBUG_LEVEL;
      break;
    case 2:
      level = NOTE_LEVEL;
      break;
    case 3:
      level = WARN_LEVEL;
      break;
    case 4:
    default:
      level = ERROR_LEVEL;
  }
  TraceLog(level, "venue: %s, feed_id: %d, log_level: %d,  msg: %s\n", MdcIdToText[venue_id], feed_id, log_level, msg);    

  return buffer_ptr - start_ptr;
}

int32_t DBLFilter_ParseDelayedPacketError(const uint8_t *buffer_ptr)
{
  const uint8_t *start_ptr = buffer_ptr;

  uint64_t time = *((uint64_t*)buffer_ptr);
  buffer_ptr += 8;

  uint32_t count = *((uint32_t*)buffer_ptr);
  buffer_ptr += 4;

  uint64_t max_nanos = *((uint64_t*)buffer_ptr);
  buffer_ptr += 8;

  uint8_t underrun = *buffer_ptr;
  buffer_ptr += 1;

  TraceLog(WARN_LEVEL, "%lu delayed %d packets, max %lu nanos, underrun: %d\n", time, count, max_nanos, underrun);

  return buffer_ptr - start_ptr;
}

int32_t DBLFilter_ParseFeedHeartbeatMessage(const uint8_t *buffer_ptr)
{
  const uint8_t *start_ptr = buffer_ptr;

  //uint64_t time = *((uint64_t*)buffer_ptr);
  buffer_ptr += 8;

  uint32_t source_id = *((uint32_t*)buffer_ptr);
  int venue_id = SourceVenueMapping[source_id];
  int feed_id = VenueFeedMapping[venue_id][source_id];
  buffer_ptr += 4;

  //TraceLog(DEBUG_LEVEL, "%lu received heartbeat on venue: %d feed id: %d\n", time, venue_id, feed_id);    
  switch (venue_id)
  {
    case BOOK_ARCA:
      if (feed_id >= MAX_ARCA_GROUPS)
        TraceLog(ERROR_LEVEL, "DBLFilter_ParseFeedHeartbeatMessage() - Invalid venue (%s), feed_id (%d)\n", MdcIdToText[venue_id], feed_id);
      else
      {
        __sync_val_compare_and_swap(&MonitorArcaBook.isReceiving[feed_id], 0, 1);
        MonitorArcaBook.isProcessing = 1;
      }
      break;
    case BOOK_NASDAQ:
      if (feed_id != 0)
        TraceLog(ERROR_LEVEL, "DBLFilter_ParseFeedHeartbeatMessage() - Invalid venue (%s), feed_id (%d)\n", MdcIdToText[venue_id], feed_id);
      else
      {
        __sync_val_compare_and_swap(&MonitorNasdaqBook.isReceiving, 0, 1);
        MonitorNasdaqBook.isProcessing = 1;
      }
      break;
    case BOOK_NDBX:
      if (feed_id != 0)
        TraceLog(ERROR_LEVEL, "DBLFilter_ParseFeedHeartbeatMessage() - Invalid venue (%s), feed_id (%d)\n", MdcIdToText[venue_id], feed_id);
      else
      {
        __sync_val_compare_and_swap(&MonitorNdbxBook.isReceiving, 0, 1);
        MonitorNdbxBook.isProcessing = 1;
      }
      break;
    case BOOK_PSX:
      if (feed_id != 0)
        TraceLog(ERROR_LEVEL, "DBLFilter_ParseFeedHeartbeatMessage() - Invalid venue (%s), feed_id (%d)\n", MdcIdToText[venue_id], feed_id);
      else
      {
        __sync_val_compare_and_swap(&MonitorPsxBook.isReceiving, 0, 1);
        MonitorPsxBook.isProcessing = 1;
      }
      break;
    case BOOK_NYSE:
      if (feed_id >= MAX_NYSE_BOOK_GROUPS)
        TraceLog(ERROR_LEVEL, "DBLFilter_ParseFeedHeartbeatMessage() - Invalid venue (%s), feed_id (%d)\n", MdcIdToText[venue_id], feed_id);
      else
      {
        __sync_val_compare_and_swap(&MonitorNyseBook.isReceiving[feed_id], 0, 1);
        MonitorNyseBook.isProcessing = 1;
      }
      break;
    case BOOK_AMEX:
      if (feed_id != 0)
        TraceLog(ERROR_LEVEL, "DBLFilter_ParseFeedHeartbeatMessage() - Invalid venue (%s), feed_id (%d)\n", MdcIdToText[venue_id], feed_id);
      else
      {
        __sync_val_compare_and_swap(&MonitorAmexBook.isReceiving[feed_id], 0, 1);
        MonitorAmexBook.isProcessing = 1;
      }
      break;
    case BOOK_BATSZ:
      if (feed_id >= MAX_BATSZ_UNIT)
        TraceLog(ERROR_LEVEL, "DBLFilter_ParseFeedHeartbeatMessage() - Invalid venue (%s), feed_id (%d)\n", MdcIdToText[venue_id], feed_id);
      else
      {
        __sync_val_compare_and_swap(&MonitorBatszBook.isReceiving[feed_id], 0, 1);
        MonitorBatszBook.isProcessing = 1;
      }
      break;
    case BOOK_BYX:
      if (feed_id >= MAX_BYX_UNIT)
        TraceLog(ERROR_LEVEL, "DBLFilter_ParseFeedHeartbeatMessage() - Invalid venue (%s), feed_id (%d)\n", MdcIdToText[venue_id], feed_id);
      else
      {
        __sync_val_compare_and_swap(&MonitorBYXBook.isReceiving[feed_id], 0, 1);
        MonitorBYXBook.isProcessing = 1;
      }
      break;
    case BOOK_EDGX:
      if (feed_id >= MAX_EDGX_BOOK_GROUP)
        TraceLog(ERROR_LEVEL, "DBLFilter_ParseFeedHeartbeatMessage() - Invalid venue (%s), feed_id (%d)\n", MdcIdToText[venue_id], feed_id);
      else
      {
        __sync_val_compare_and_swap(&MonitorEdgxBook.isReceiving[feed_id], 0, 1);
        MonitorEdgxBook.isProcessing = 1;
      }
      break;
    case BOOK_EDGA:
      if (feed_id >= MAX_EDGA_BOOK_GROUP)
        TraceLog(ERROR_LEVEL, "DBLFilter_ParseFeedHeartbeatMessage() - Invalid venue (%s), feed_id (%d)\n", MdcIdToText[venue_id], feed_id);
      else
      {
        __sync_val_compare_and_swap(&MonitorEdgaBook.isReceiving[feed_id], 0, 1);
        MonitorEdgaBook.isProcessing = 1;
      }
      break;
    case FEED_CQS:
      if (feed_id >= MAX_CQS_MULTICAST_LINES)
        TraceLog(ERROR_LEVEL, "DBLFilter_ParseFeedHeartbeatMessage() - Invalid venue (%s), feed_id (%d)\n", MdcIdToText[venue_id], feed_id);
      else
      {
        __sync_val_compare_and_swap(&MonitorCQS.isReceiving[feed_id], 0, 1);
        MonitorCQS.isProcessing = 1;
      }
      break;
    case FEED_UQDF:
      if (feed_id >= MAX_UQDF_CHANNELS)
        TraceLog(ERROR_LEVEL, "DBLFilter_ParseFeedHeartbeatMessage() - Invalid venue (%s), feed_id (%d)\n", MdcIdToText[venue_id], feed_id);
      else
      {
        __sync_val_compare_and_swap(&MonitorUQDF.isReceiving[feed_id], 0, 1);
        MonitorUQDF.isProcessing = 1;
      }
      break;
    default:
      TraceLog(ERROR_LEVEL, "DBLFilter_ParseFeedHeartbeatMessage() - Invalid venue_id (%d), feed_id (%d)\n", venue_id, feed_id);
  }
  
  return buffer_ptr - start_ptr;
}

int32_t DBLFilter_ProcessLogMessage(const uint8_t *buffer_ptr, int queueId)
{
  const uint8_t *start_ptr = buffer_ptr;

  // uint32_t topic_id = *((uint32_t *) buffer_ptr);
  buffer_ptr += 4;

  char msg_type[5];
  strncpy(msg_type, (char*)buffer_ptr, 4);
  buffer_ptr += 4;

  if(strncmp(msg_type, "psem", 4) == 0)
  {
    // packet sequence error message
    buffer_ptr += DBLFilter_ParsePacketSequenceError(buffer_ptr, queueId);
  }
  else if(strncmp(msg_type, "rlem", 4) == 0)
  {
    // raw log message
    buffer_ptr += DBLFilter_ParseRawLogMessage(buffer_ptr);
  }
  else if(strncmp(msg_type, "dpem", 4) == 0)
  {
    // delayed packet error message
    buffer_ptr += DBLFilter_ParseDelayedPacketError(buffer_ptr);
  }
  else if(strncmp(msg_type, "fhbm", 4) == 0)
  {
    // feed heartbeat message - if you stop seeing these, we have stopped receiving data from the feed referenced
    buffer_ptr += DBLFilter_ParseFeedHeartbeatMessage(buffer_ptr);
  }
  else if(strncmp(msg_type, "rpcp", 4) == 0)
  {
    TraceLog(DEBUG_LEVEL, "DBLFilter: received replay complete log msg\n");
  }
  else
  {
    TraceLog(FATAL_LEVEL, "DBLFilter (Process Log Message) - Received unknown log message type (%.4s)\n", msg_type);
    int i;
    for (i = 0; i < MAX_ECN_BOOK; i++)
    {
      DisconnectFromVenue(i, queueId);
      FlushVenueSymbolsOnQueue(i, queueId);
    }
    
    DisconnectFromVenue(FEED_CQS, queueId);
    DisconnectFromVenue(FEED_UQDF, queueId);
  }
    
  return buffer_ptr - start_ptr;   
}

/****************************************************************************
- Function name:  StartMarketDataLogConsumerThread
****************************************************************************/
void *StartMarketDataLogConsumerThread(void *args)
{
  SetKernelAlgorithm("[DBLFILTER_LOG_CONSUMER]");

  TraceLog(DEBUG_LEVEL, "DBLFilter - Launching log message consumer thread.\n");

  void *spsc_reader = DBLFilterMgmt.marketDataLogReceiveReader;

  while(1)
  {
    spscSyncNumReadableItems(spsc_reader);
    int32_t buffer_readable_bytes = spscGetNumReadableItems(spsc_reader);
    int32_t length = buffer_readable_bytes;
    const uint8_t *buffer_ptr = spscReadPtr(spsc_reader);
    
    while(buffer_readable_bytes > 0)
    {
      int32_t bytes_read = DBLFilter_ProcessLogMessage(buffer_ptr, -1);
      buffer_ptr += bytes_read;
      buffer_readable_bytes -= bytes_read;
    }
    
    spscCommitItemsRead(spsc_reader, length);
    
    usleep(500000);
  }
  
  return NULL;
}

void *StartMarketDataReadThread(void *args)
{
  SetKernelAlgorithm("[DBLFILTER_RECEIVER]");
  // while (1) {}
  if (DBLFilterMgmt.topic_registry != NULL)
  {
    // Here we are assuming that all marketdata arrives on the same nic. Use of
    // ARCA is arbitrary choice.
    char ip_addr[32];
    if (GetIPAddress(ARCA_Book_Conf.NICName, ip_addr) == ERROR) {
      TraceLog(ERROR_LEVEL, "Could not get IP Address for interface named '%s'\n", ARCA_Book_Conf.NICName);
      return NULL;
    }

    if (getenv("BLINK_USE_SOLARFLARE_MARKETDATA")) {
      TraceLog(DEBUG_LEVEL, "Creating ef_vi device on local interface: %s\n", ip_addr);
      DBLFilterMgmt.dbl_device = create_efvi_device(DBLFilterMgmt.topic_registry, ip_addr, 0);
    } else {
      TraceLog(DEBUG_LEVEL, "Creating dbl device on local interface: %s\n", ip_addr);
      DBLFilterMgmt.dbl_device = create_dbl_device(DBLFilterMgmt.topic_registry, ip_addr);
    }
      
    TraceLog(DEBUG_LEVEL, "DBLFilter - Launching dbl read thread.\n");
    start_device_with_offset(DBLFilterMgmt.dbl_device, -hbitime_get_offset_micros() * 1000);
    
    MarketDataDeviceInitialized = YES;
  }
  else
  {
    TraceLog(ERROR_LEVEL, "Invalid DBL Device.  Could not start DBL Receive thread.\n");
  }

  return NULL;
}

/****************************************************************************
- Function name:  ProcessMarketDataMessage
****************************************************************************/
unsigned short ProcessMarketDataMessage(const unsigned char venue_id, const uint32_t symbol_id, const uint64_t arrival_time, const uint32_t time_status_seconds, const unsigned char *buffer_ptr, const int buffer_readable_bytes, const int consumerId)
{
  const unsigned char *message_ptr = buffer_ptr + DBL_MD_HDR_LEN;
  switch (venue_id) 
  {
    case VENUE_NASDAQ:
    {
      unsigned short msgSize = __bswap16(*(unsigned short *)(message_ptr));
      
      NASDAQ_ProcessBookMessage(BOOK_NASDAQ, symbol_id, (char *)(message_ptr + 2), arrival_time, time_status_seconds);
      
      return msgSize + DBL_MD_HDR_LEN + 2;
    }
    
    case VENUE_NDBX:
    {
      unsigned short msgSize = __bswap16(*(unsigned short *)(message_ptr));
      
      NASDAQ_ProcessBookMessage(BOOK_NDBX, symbol_id, (char *)(message_ptr + 2), arrival_time, time_status_seconds);
      
      return msgSize + DBL_MD_HDR_LEN + 2;
    }
    
    case VENUE_PSX:
    {
      unsigned short msgSize = __bswap16(*(unsigned short *)(message_ptr));
      
      NASDAQ_ProcessBookMessage(BOOK_PSX, symbol_id, (char *)(message_ptr + 2), arrival_time, time_status_seconds);
      
      return msgSize + DBL_MD_HDR_LEN + 2;
    }
    
    case VENUE_ARCA:
    {
      unsigned short msgSize = *((uint16_t*)(message_ptr));
      
      ARCA_ProcessBookMessage((unsigned char *)(message_ptr), arrival_time, time_status_seconds, symbol_id);
      
      return msgSize + DBL_MD_HDR_LEN;
    }
    
    case VENUE_BATSZ:
    case VENUE_BYX:
    {
      unsigned short msgSize = *(message_ptr);
      
      BATS_ProcessBookMessage(venue_id == VENUE_BATSZ ? BOOK_BATSZ : BOOK_BYX, symbol_id, (char *)(message_ptr), arrival_time, time_status_seconds);

      return msgSize + DBL_MD_HDR_LEN;
    }
    
    case VENUE_AMEX:
    case VENUE_NYSE:
    {
      unsigned short msgType = *((unsigned short*)(message_ptr));
      unsigned int msgSize = GetNyseMessageSize(venue_id == VENUE_NYSE ? BOOK_NYSE : BOOK_AMEX, message_ptr + 2, msgType);
      if (msgSize == -1)  //Could not get msg size
      {
        int i;
        for (i=0; i<MAX_ECN_BOOK; i++)
        {
          DisconnectFromVenue(i, consumerId);
          FlushVenueSymbolsOnQueue(i, consumerId);
        }
        
        DisconnectFromVenue(FEED_CQS, consumerId);
        DisconnectFromVenue(FEED_UQDF, consumerId);
        return 0;
      }
      
      NYSE_ProcessBookMessage(venue_id == VENUE_NYSE ? BOOK_NYSE : BOOK_AMEX, symbol_id, (char *)(message_ptr + 2), arrival_time, msgType);
      
      return msgSize + DBL_MD_HDR_LEN + 2;
    }
    
    case VENUE_EDGX:
    case VENUE_EDGA:
    {
      unsigned short msgSize = *((uint8_t*)(message_ptr));
      
      EDGE_ProcessBookMessage(venue_id == VENUE_EDGX ? BOOK_EDGX : BOOK_EDGA, symbol_id, (char *)(message_ptr), arrival_time, time_status_seconds);

      return msgSize  + DBL_MD_HDR_LEN;
    }
    
    case VENUE_CQS:
    {
      unsigned short msgSize = 0;
      CQS_ProcessMessage(symbol_id, (char *)(message_ptr), buffer_readable_bytes, &msgSize);
      if (msgSize == 0)
      {
        int i;
        for (i=0; i<MAX_ECN_BOOK; i++)
        {
          DisconnectFromVenue(i, consumerId);
          FlushVenueSymbolsOnQueue(i, consumerId);
        }

        DisconnectFromVenue(FEED_CQS, consumerId);
        DisconnectFromVenue(FEED_UQDF, consumerId);
        return 0;
      }
      
      return msgSize + DBL_MD_HDR_LEN;
    }
    
    case VENUE_UQDF:
    {
      unsigned short msgSize = 0;
      UQDF_ProcessMessage(symbol_id, (char *)(message_ptr), buffer_readable_bytes, &msgSize);
      
      if (msgSize == 0)
      {
        int i;
        for (i=0; i<MAX_ECN_BOOK; i++)
        {
          DisconnectFromVenue(i, consumerId);
          FlushVenueSymbolsOnQueue(i, consumerId);
        }
        
        DisconnectFromVenue(FEED_CQS, consumerId);
        DisconnectFromVenue(FEED_UQDF, consumerId);
        return 0;
      }
      
      return msgSize + DBL_MD_HDR_LEN;
    }
    
    case VENUE_ADMIN:   // packet sequence error
    {
      return DBLFilter_ProcessLogMessage(buffer_ptr, consumerId);
    }
    
    default:
    {
      TraceLog(FATAL_LEVEL, "DBLFilter (Data Consumer Buffer) - Got unknown venue, venue ID: %d\n", venue_id);
      TraceLog(FATAL_LEVEL, "Last 316 processed bytes from data buffer:\n");
      const unsigned char *_buff = buffer_ptr - 300;
      int i, _offset = 0;
      char _output[316 * 3 + 1];
      for (i = 0; i < 314; i++)
      {
        sprintf(&_output[_offset], "%02X ", *_buff);
        _buff++;
        _offset += 3;
      }
      
      _output[_offset] = 0;
      TraceLog(FATAL_LEVEL, "%s\n", _output);
      
      for (i=0; i<MAX_ECN_BOOK; i++)
      {
        DisconnectFromVenue(i, consumerId);
        FlushVenueSymbolsOnQueue(i, consumerId);
      }
        
      DisconnectFromVenue(FEED_CQS, consumerId);
      DisconnectFromVenue(FEED_UQDF, consumerId);
      TraceLog(FATAL_LEVEL, "Please restart Trade Server.\n");
      return 0;
    }
  }
}

/****************************************************************************
- Function name:  ConnectToVenue
****************************************************************************/
int ConnectToVenue(int venueId)
{
  static pthread_mutex_t mutex_arca = PTHREAD_MUTEX_INITIALIZER;
  static pthread_mutex_t mutex_nasdaq = PTHREAD_MUTEX_INITIALIZER;
  static pthread_mutex_t mutex_nyse = PTHREAD_MUTEX_INITIALIZER;
  static pthread_mutex_t mutex_amex = PTHREAD_MUTEX_INITIALIZER;
  static pthread_mutex_t mutex_edgx = PTHREAD_MUTEX_INITIALIZER;
  static pthread_mutex_t mutex_batsz = PTHREAD_MUTEX_INITIALIZER;
  static pthread_mutex_t mutex_byx = PTHREAD_MUTEX_INITIALIZER;
  static pthread_mutex_t mutex_edga = PTHREAD_MUTEX_INITIALIZER;
  static pthread_mutex_t mutex_ndbx = PTHREAD_MUTEX_INITIALIZER;
  static pthread_mutex_t mutex_psx = PTHREAD_MUTEX_INITIALIZER;
  static pthread_mutex_t mutex_cqs = PTHREAD_MUTEX_INITIALIZER;
  static pthread_mutex_t mutex_uqdf = PTHREAD_MUTEX_INITIALIZER;
  int i;
  
  switch (venueId)
  {
    case BOOK_ARCA:
      pthread_mutex_lock(&mutex_arca);
      if (BookStatusMgmt[BOOK_ARCA].isConnected == CONNECTED)
      {
        TraceLog(DEBUG_LEVEL, "ARCA Book was already connected\n");
        pthread_mutex_unlock(&mutex_arca);
        return ERROR;
      }
      
      if (InitARCA_BOOK_DataStructure() == ERROR)
      {
        pthread_mutex_unlock(&mutex_arca);
        return ERROR;
      }
      
      startVenue(DBLFilterMgmt.dbl_device, VENUE_ARCA);
      BookStatusMgmt[BOOK_ARCA].isConnected = CONNECTED;
      
      pthread_mutex_unlock(&mutex_arca);
      break;
    case BOOK_NASDAQ:
      pthread_mutex_lock(&mutex_nasdaq);
      if (BookStatusMgmt[BOOK_NASDAQ].isConnected == CONNECTED)
      {
        TraceLog(DEBUG_LEVEL, "NASDAQ Book was already connected\n");
        pthread_mutex_unlock(&mutex_nasdaq);
        return ERROR;
      }
      
      InitNASDAQ_BOOK_DataStructure();

      startVenue(DBLFilterMgmt.dbl_device, VENUE_NASDAQ);
      BookStatusMgmt[BOOK_NASDAQ].isConnected = CONNECTED;
      pthread_mutex_unlock(&mutex_nasdaq);
      break;
    case BOOK_NDBX:
      pthread_mutex_lock(&mutex_ndbx);
      if (BookStatusMgmt[BOOK_NDBX].isConnected == CONNECTED)
      {
        TraceLog(DEBUG_LEVEL, "NDBX Book was already connected\n");
        pthread_mutex_unlock(&mutex_ndbx);
        return ERROR;
      }
      
      InitDataStructure_NDBX();

      startVenue(DBLFilterMgmt.dbl_device, VENUE_NDBX);
      BookStatusMgmt[BOOK_NDBX].isConnected = CONNECTED;
      pthread_mutex_unlock(&mutex_ndbx);
      break;
    case BOOK_PSX:
      pthread_mutex_lock(&mutex_psx);
      if (BookStatusMgmt[BOOK_PSX].isConnected == CONNECTED)
      {
        TraceLog(DEBUG_LEVEL, "PSX Book was already connected\n");
        pthread_mutex_unlock(&mutex_psx);
        return ERROR;
      }
      
      InitDataStructure_PSX();

      startVenue(DBLFilterMgmt.dbl_device, VENUE_PSX);
      BookStatusMgmt[BOOK_PSX].isConnected = CONNECTED;
      pthread_mutex_unlock(&mutex_psx);
      break;
    case BOOK_NYSE:
      pthread_mutex_lock(&mutex_nyse);
      if (BookStatusMgmt[BOOK_NYSE].isConnected == CONNECTED)
      {
        TraceLog(DEBUG_LEVEL, "NYSE Book was already connected\n");
        pthread_mutex_unlock(&mutex_nyse);
        return ERROR;
      }
      
      if (NYSE_InitDataStructure(BOOK_NYSE) == ERROR)
      {
        pthread_mutex_unlock(&mutex_nyse);
        return ERROR;
      }
      
      for (i = 0; i < MAX_NYSE_BOOK_GROUPS; i++)
      {
        NyseBookConf.group[i].groupIndex = i;
        
        // ---------------------------------------------------------------------------
        // Connect to TCP Retransmission server
        // ---------------------------------------------------------------------------
        if (NyseBookConf.group[i].groupPermission == DISABLE)
        {
          continue; //This group is not allowed to be connected depending on current Symbol Split Configuration
        }
      }
      
      startVenue(DBLFilterMgmt.dbl_device, VENUE_NYSE);
      BookStatusMgmt[BOOK_NYSE].isConnected = CONNECTED;
      pthread_mutex_unlock(&mutex_nyse);
      break;
    case BOOK_AMEX:
      pthread_mutex_lock(&mutex_amex);
      if (BookStatusMgmt[BOOK_AMEX].isConnected == CONNECTED)
      {
        TraceLog(DEBUG_LEVEL, "AMEX Book was already connected\n");
        pthread_mutex_unlock(&mutex_amex);
        return ERROR;
      }
      
      if (NYSE_InitDataStructure(BOOK_AMEX) == ERROR)
      {
        pthread_mutex_unlock(&mutex_amex);
        return ERROR;
      }
      
      AmexBookConf.group[0].groupIndex = 0;
      
      startVenue(DBLFilterMgmt.dbl_device, VENUE_AMEX);
      BookStatusMgmt[BOOK_AMEX].isConnected = CONNECTED;
      pthread_mutex_unlock(&mutex_amex);
      break;
    case BOOK_BATSZ:
      pthread_mutex_lock(&mutex_batsz);
      if (BookStatusMgmt[BOOK_BATSZ].isConnected == CONNECTED)
      {
        TraceLog(DEBUG_LEVEL, "BATSZ Book was already connected\n");
        pthread_mutex_unlock(&mutex_batsz);
        return ERROR;
      }
      
      InitBATSZ_BOOK_DataStructure();
      
      startVenue(DBLFilterMgmt.dbl_device, VENUE_BATSZ);
      BookStatusMgmt[BOOK_BATSZ].isConnected = CONNECTED;
      pthread_mutex_unlock(&mutex_batsz);
      break;
    case BOOK_BYX:
      pthread_mutex_lock(&mutex_byx);
      if (BookStatusMgmt[BOOK_BYX].isConnected == CONNECTED)
      {
        TraceLog(DEBUG_LEVEL, "BYX Book was already connected\n");
        pthread_mutex_unlock(&mutex_byx);
        return ERROR;
      }
      
      InitBYX_BOOK_DataStructure();
      
      startVenue(DBLFilterMgmt.dbl_device, VENUE_BYX);
      BookStatusMgmt[BOOK_BYX].isConnected = CONNECTED;
      pthread_mutex_unlock(&mutex_byx);
      break;
    case BOOK_EDGX:
      pthread_mutex_lock(&mutex_edgx);
      if (BookStatusMgmt[BOOK_EDGX].isConnected == CONNECTED)
      {
        TraceLog(DEBUG_LEVEL, "EDGX Book was already connected\n");
        pthread_mutex_unlock(&mutex_edgx);
        return ERROR;
      }
      
      if (Init_DataStructure_EDGX_BOOK() != SUCCESS)
      {
        pthread_mutex_unlock(&mutex_edgx);
        return ERROR;
      }
      
      // ---------------------------------------------------------------------------
      // Connect to TCP Retrans server, EDGX uses only 1 TCP retrans server
      // ---------------------------------------------------------------------------
      
      startVenue(DBLFilterMgmt.dbl_device, VENUE_EDGX);
      BookStatusMgmt[BOOK_EDGX].isConnected = CONNECTED;
      pthread_mutex_unlock(&mutex_edgx);
      break;
    case BOOK_EDGA:
      pthread_mutex_lock(&mutex_edga);
      if (BookStatusMgmt[BOOK_EDGA].isConnected == CONNECTED)
      {
        TraceLog(DEBUG_LEVEL, "EDGA Book was already connected\n");
        pthread_mutex_unlock(&mutex_edga);
        return ERROR;
      }
      
      if (Init_DataStructure_EDGA_BOOK() != SUCCESS)
      {
        pthread_mutex_unlock(&mutex_edga);
        return ERROR;
      }
      
      // ---------------------------------------------------------------------------
      // Connect to TCP Retrans server, EDGA uses only 1 TCP retrans server
      // ---------------------------------------------------------------------------
      
      startVenue(DBLFilterMgmt.dbl_device, VENUE_EDGA);
      BookStatusMgmt[BOOK_EDGA].isConnected = CONNECTED;
      pthread_mutex_unlock(&mutex_edga);
      break;
    case FEED_CQS:
      pthread_mutex_lock(&mutex_cqs);
      if (CQS_StatusMgmt.isConnected == CONNECTED)
      {
        TraceLog(DEBUG_LEVEL, "CQS was already connected\n");
        pthread_mutex_unlock(&mutex_cqs);
        return ERROR;
      }
      
      InitCQS_DataStructure();
      
      startVenue(DBLFilterMgmt.dbl_device, VENUE_CQS);
      CQS_StatusMgmt.isConnected = CONNECTED;
      pthread_mutex_unlock(&mutex_cqs);
      break;
    case FEED_UQDF:
      pthread_mutex_lock(&mutex_uqdf);
      if (UQDF_StatusMgmt.isConnected == CONNECTED)
      {
        TraceLog(DEBUG_LEVEL, "UQDF was already connected\n");
        pthread_mutex_unlock(&mutex_uqdf);
        return ERROR;
      }
      
      InitUQDF_DataStructure();
            
      startVenue(DBLFilterMgmt.dbl_device, VENUE_UQDF);
      UQDF_StatusMgmt.isConnected = CONNECTED;
      pthread_mutex_unlock(&mutex_uqdf);
      break;
    default:
      TraceLog(ERROR_LEVEL, "ConnectToVenue() - Invalid venue id: %d\n", venueId);
      return ERROR;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  DisconnectFromVenue
****************************************************************************/
int DisconnectFromVenue(int venueId, int excludedConsumerId)
{
  static pthread_mutex_t mutex_arca = PTHREAD_MUTEX_INITIALIZER;
  static pthread_mutex_t mutex_nasdaq = PTHREAD_MUTEX_INITIALIZER;
  static pthread_mutex_t mutex_ndbx = PTHREAD_MUTEX_INITIALIZER;
  static pthread_mutex_t mutex_psx = PTHREAD_MUTEX_INITIALIZER;
  static pthread_mutex_t mutex_nyse = PTHREAD_MUTEX_INITIALIZER;
  static pthread_mutex_t mutex_amex = PTHREAD_MUTEX_INITIALIZER;
  static pthread_mutex_t mutex_edgx = PTHREAD_MUTEX_INITIALIZER;
  static pthread_mutex_t mutex_edga = PTHREAD_MUTEX_INITIALIZER;
  static pthread_mutex_t mutex_batsz = PTHREAD_MUTEX_INITIALIZER;
  static pthread_mutex_t mutex_byx = PTHREAD_MUTEX_INITIALIZER;
  static pthread_mutex_t mutex_cqs = PTHREAD_MUTEX_INITIALIZER;
  static pthread_mutex_t mutex_uqdf = PTHREAD_MUTEX_INITIALIZER;
  switch (venueId)
  {
    case BOOK_ARCA:
      pthread_mutex_lock(&mutex_arca);
      if (BookStatusMgmt[BOOK_ARCA].isConnected == DISCONNECTED)
      {
        TraceLog(DEBUG_LEVEL, "ARCA Book is not currently connected\n");
        pthread_mutex_unlock(&mutex_arca);
        return ERROR;
      }
      BookStatusMgmt[BOOK_ARCA].isConnected = DISCONNECTED;
      stopVenue(DBLFilterMgmt.dbl_device, VENUE_ARCA);
      if (*(TradingStatus.status[ORDER_ARCA_DIRECT]) == ENABLE)
      {
        *(TradingStatus.status[ORDER_ARCA_DIRECT]) = DISABLE;
        if (ASConfig.socket != -1)
        {
          if (BuildSendTradingDisabled(SERVICE_ARCA_BOOK, TRADING_DISABLED_BOOK_DISCONNECT) == SUCCESS)
          {
            TraceLog(DEBUG_LEVEL, "Notify trading disabled of ARCA DIRECT is sent to AS\n");
          }
        }
      }
      
      if (ARCA_Book_Conf.symbolMappingFp != NULL)
      {
        fclose (ARCA_Book_Conf.symbolMappingFp);
        ARCA_Book_Conf.symbolMappingFp = NULL;
      }
      
      if (ASConfig.socket != -1)
      {
        if (BuildSendServiceDisconnect(SERVICE_ARCA_BOOK) == SUCCESS)
        {
          TraceLog(DEBUG_LEVEL, "Notify disconnect ARCA Book is sent to AS\n");
        }
      }
      
      AddNewFlush(__FLUSH_ARCA, excludedConsumerId);
      TraceLog(DEBUG_LEVEL, "ARCA Book is disconnected\n");
      
      pthread_mutex_unlock(&mutex_arca);
      break;
    case BOOK_NASDAQ:
      pthread_mutex_lock(&mutex_nasdaq);
      
      if (BookStatusMgmt[BOOK_NASDAQ].isConnected == DISCONNECTED)
      {
        TraceLog(DEBUG_LEVEL, "NASDAQ Book is not currently connected\n");
        pthread_mutex_unlock(&mutex_nasdaq);
        return ERROR;
      }
      
      BookStatusMgmt[BOOK_NASDAQ].isConnected = DISCONNECTED;
      stopVenue(DBLFilterMgmt.dbl_device, VENUE_NASDAQ);
      
      {
        int  willAlertAS = 0;
        if (*(TradingStatus.status[ORDER_NASDAQ_RASH]) == ENABLE)
        {
          *(TradingStatus.status[ORDER_NASDAQ_RASH]) = DISABLE;
          willAlertAS = 1;
        }
        
        if (*(TradingStatus.status[ORDER_NASDAQ_OUCH]) == ENABLE)
        {
          *(TradingStatus.status[ORDER_NASDAQ_OUCH]) = DISABLE;
          willAlertAS = 1;
        }
        
        if ((ASConfig.socket != -1) && (willAlertAS == 1))
        {
          if (BuildSendTradingDisabled(SERVICE_NASDAQ_BOOK, TRADING_DISABLED_BOOK_DISCONNECT) == SUCCESS)
          {
            TraceLog(DEBUG_LEVEL, "Notify trading disabled of OUCH and RASH is sent to AS\n");
          }
        }
      }
      
      if (ASConfig.socket != -1)
      {
        if (BuildSendServiceDisconnect(SERVICE_NASDAQ_BOOK) == SUCCESS)
        {
          TraceLog(DEBUG_LEVEL, "Notify disconnect NASDAQ Book is sent to AS\n");
        }
      }
      
      AddNewFlush(__FLUSH_NASDAQ, excludedConsumerId);
      TraceLog(DEBUG_LEVEL, "NASDAQ Book is disconnected\n");
      pthread_mutex_unlock(&mutex_nasdaq);
      break;
    case BOOK_NDBX:
      pthread_mutex_lock(&mutex_ndbx);
      
      if (BookStatusMgmt[BOOK_NDBX].isConnected == DISCONNECTED)
      {
        TraceLog(DEBUG_LEVEL, "NDBX Book is not currently connected\n");
        pthread_mutex_unlock(&mutex_ndbx);
        return ERROR;
      }
      
      BookStatusMgmt[BOOK_NDBX].isConnected = DISCONNECTED;
      stopVenue(DBLFilterMgmt.dbl_device, VENUE_NDBX);

      if (*(TradingStatus.status[ORDER_NASDAQ_BX]) == ENABLE)
      {
        *(TradingStatus.status[ORDER_NASDAQ_BX]) = DISABLE;
        if ((ASConfig.socket != -1))
        {
          if (BuildSendTradingDisabled(SERVICE_NDBX_BOOK, TRADING_DISABLED_BOOK_DISCONNECT) == SUCCESS)
          {
            TraceLog(DEBUG_LEVEL, "Notify trading disabled of BX is sent to AS\n");
          }
        }
      }
      
      if (ASConfig.socket != -1)
      {
        if (BuildSendServiceDisconnect(SERVICE_NDBX_BOOK) == SUCCESS)
        {
          TraceLog(DEBUG_LEVEL, "Notify disconnect NDBX Book is sent to AS\n");
        }
      }
      
      AddNewFlush(__FLUSH_NDBX, excludedConsumerId);
      TraceLog(DEBUG_LEVEL, "NDBX Book is disconnected\n");
      pthread_mutex_unlock(&mutex_ndbx);
      break;
    case BOOK_PSX:
      pthread_mutex_lock(&mutex_psx);
      
      if (BookStatusMgmt[BOOK_PSX].isConnected == DISCONNECTED)
      {
        TraceLog(DEBUG_LEVEL, "PSX Book is not currently connected\n");
        pthread_mutex_unlock(&mutex_psx);
        return ERROR;
      }
      
      BookStatusMgmt[BOOK_PSX].isConnected = DISCONNECTED;
      stopVenue(DBLFilterMgmt.dbl_device, VENUE_PSX);

      if (*(TradingStatus.status[ORDER_NASDAQ_PSX]) == ENABLE)
      {
        *(TradingStatus.status[ORDER_NASDAQ_PSX]) = DISABLE;
        if ((ASConfig.socket != -1))
        {
          if (BuildSendTradingDisabled(SERVICE_PSX_BOOK, TRADING_DISABLED_BOOK_DISCONNECT) == SUCCESS)
          {
            TraceLog(DEBUG_LEVEL, "Notify trading disabled of PSX is sent to AS\n");
          }
        }
      }
      
      if (ASConfig.socket != -1)
      {
        if (BuildSendServiceDisconnect(SERVICE_PSX_BOOK) == SUCCESS)
        {
          TraceLog(DEBUG_LEVEL, "Notify disconnect PSX Book is sent to AS\n");
        }
      }
      
      AddNewFlush(__FLUSH_PSX, excludedConsumerId);
      TraceLog(DEBUG_LEVEL, "PSX Book is disconnected\n");
      pthread_mutex_unlock(&mutex_psx);
      break;
    case BOOK_NYSE:
      pthread_mutex_lock(&mutex_nyse);
      if (BookStatusMgmt[BOOK_NYSE].isConnected == DISCONNECTED)
      {
        TraceLog(DEBUG_LEVEL, "NYSE Book is not currently connected\n");
        pthread_mutex_unlock(&mutex_nyse);
        return ERROR;
      }
      
      BookStatusMgmt[BOOK_NYSE].isConnected = DISCONNECTED;
      stopVenue(DBLFilterMgmt.dbl_device, VENUE_NYSE);
      if (*(TradingStatus.status[ORDER_NYSE_CCG]) == ENABLE &&
          BookStatusMgmt[BOOK_AMEX].isConnected == DISCONNECTED)
      {
        *(TradingStatus.status[ORDER_NYSE_CCG]) = DISABLE;
        if (ASConfig.socket != -1)
        {
          if (BuildSendTradingDisabled(SERVICE_NYSE_BOOK, TRADING_DISABLED_BOOK_DISCONNECT) == SUCCESS)
          {
            TraceLog(DEBUG_LEVEL, "Notify trading disabled of NYSE CCG is sent to AS\n");
          }
        }
      }
      
      if (ASConfig.socket != -1)
      {
        if (BuildSendServiceDisconnect(SERVICE_NYSE_BOOK) == SUCCESS)
        {
          TraceLog(DEBUG_LEVEL, "Notify disconnect NYSE Book is sent to AS\n");
        }
      }
      
      AddNewFlush(__FLUSH_NYSE, excludedConsumerId);
      TraceLog(DEBUG_LEVEL, "NYSE Book is disconnected\n");
      pthread_mutex_unlock(&mutex_nyse);
      break;
    case BOOK_AMEX:
      pthread_mutex_lock(&mutex_amex);
      if (BookStatusMgmt[BOOK_AMEX].isConnected == DISCONNECTED)
      {
        TraceLog(DEBUG_LEVEL, "AMEX Book is not currently connected\n");
        pthread_mutex_unlock(&mutex_amex);
        return ERROR;
      }
      
      BookStatusMgmt[BOOK_AMEX].isConnected = DISCONNECTED;
      stopVenue(DBLFilterMgmt.dbl_device, VENUE_AMEX);

      if (*(TradingStatus.status[ORDER_NYSE_CCG]) == ENABLE &&
          BookStatusMgmt[BOOK_NYSE].isConnected == DISCONNECTED)
      {
        *(TradingStatus.status[ORDER_NYSE_CCG]) = DISABLE;
        if (ASConfig.socket != -1)
        {
         if (BuildSendTradingDisabled(SERVICE_AMEX_BOOK, TRADING_DISABLED_BOOK_DISCONNECT) == SUCCESS)
         {
           TraceLog(DEBUG_LEVEL, "Notify trading disabled of NYSE CCG is sent to AS\n");
         }
        }
      }
      
      if (ASConfig.socket != -1)
      {
        if (BuildSendServiceDisconnect(SERVICE_AMEX_BOOK) == SUCCESS)
        {
          TraceLog(DEBUG_LEVEL, "Notify disconnect AMEX Book is sent to AS\n");
        }
      }
      
      AddNewFlush(__FLUSH_AMEX, excludedConsumerId);
      TraceLog(DEBUG_LEVEL, "AMEX Book is disconnected\n");
      pthread_mutex_unlock(&mutex_amex);
      break;
    case BOOK_BATSZ:
      pthread_mutex_lock(&mutex_batsz);
      if (BookStatusMgmt[BOOK_BATSZ].isConnected == DISCONNECTED)
      {
        TraceLog(DEBUG_LEVEL, "BATSZ Book is not currently connected\n");
        pthread_mutex_unlock(&mutex_batsz);
        return ERROR;
      }
      
      BookStatusMgmt[BOOK_BATSZ].isConnected = DISCONNECTED;
      stopVenue(DBLFilterMgmt.dbl_device, VENUE_BATSZ);
      if (*(TradingStatus.status[ORDER_BATSZ_BOE]) == ENABLE)
      {
        *(TradingStatus.status[ORDER_BATSZ_BOE]) = DISABLE;
        if (ASConfig.socket != -1)
        {
          if (BuildSendTradingDisabled(SERVICE_BATSZ_BOOK, TRADING_DISABLED_BOOK_DISCONNECT) == SUCCESS)
          {
            TraceLog(DEBUG_LEVEL, "Notify trading disabled of BATSZ BOE is sent to AS\n");
          }
        }
      }
      
      if (ASConfig.socket != -1)
      {
        if (BuildSendServiceDisconnect(SERVICE_BATSZ_BOOK) == SUCCESS)
        {
          TraceLog(DEBUG_LEVEL, "Notify disconnect BATSZ Book is sent to AS\n");
        }
      }
      
      AddNewFlush(__FLUSH_BATSZ, excludedConsumerId);
      TraceLog(DEBUG_LEVEL, "BATSZ Book is disconnected\n");
      pthread_mutex_unlock(&mutex_batsz);
      break;
    case BOOK_BYX:
      pthread_mutex_lock(&mutex_byx);
      if (BookStatusMgmt[BOOK_BYX].isConnected == DISCONNECTED)
      {
        TraceLog(DEBUG_LEVEL, "BYX Book is not currently connected\n");
        pthread_mutex_unlock(&mutex_byx);
        return ERROR;
      }
      
      BookStatusMgmt[BOOK_BYX].isConnected = DISCONNECTED;
      stopVenue(DBLFilterMgmt.dbl_device, VENUE_BYX);
      if (*(TradingStatus.status[ORDER_BYX_BOE]) == ENABLE)
      {
        *(TradingStatus.status[ORDER_BYX_BOE]) = DISABLE;
        if (ASConfig.socket != -1)
        {
          if (BuildSendTradingDisabled(SERVICE_BYX_BOOK, TRADING_DISABLED_BOOK_DISCONNECT) == SUCCESS)
          {
            TraceLog(DEBUG_LEVEL, "Notify trading disabled of BYX BOE is sent to AS\n");
          }
        }
      }
      
      if (ASConfig.socket != -1)
      {
        if (BuildSendServiceDisconnect(SERVICE_BYX_BOOK) == SUCCESS)
        {
          TraceLog(DEBUG_LEVEL, "Notify disconnect BYX Book is sent to AS\n");
        }
      }
      
      AddNewFlush(__FLUSH_BYX, excludedConsumerId);
      TraceLog(DEBUG_LEVEL, "BYX Book is disconnected\n");
      pthread_mutex_unlock(&mutex_byx);
      break;
    case BOOK_EDGX:
      pthread_mutex_lock(&mutex_edgx);
      if (BookStatusMgmt[BOOK_EDGX].isConnected == DISCONNECTED)
      {
        TraceLog(DEBUG_LEVEL, "EDGX Book is not currently connected\n");
        pthread_mutex_unlock(&mutex_edgx);
        return ERROR;
      }
      
      BookStatusMgmt[BOOK_EDGX].isConnected = DISCONNECTED;
      stopVenue(DBLFilterMgmt.dbl_device, VENUE_EDGX);
      if (*(TradingStatus.status[ORDER_EDGX]) == ENABLE)
      {
        *(TradingStatus.status[ORDER_EDGX]) = DISABLE;
        if (ASConfig.socket != -1)
        {
          if (BuildSendTradingDisabled(SERVICE_EDGX_BOOK, TRADING_DISABLED_BOOK_DISCONNECT) == SUCCESS)
          {
            TraceLog(DEBUG_LEVEL, "Notify trading disabled of EDGX Order is sent to AS\n");
          }
        }
      }
      
      if (ASConfig.socket != -1)
      {
        if (BuildSendServiceDisconnect(SERVICE_EDGX_BOOK) == SUCCESS)
        {
          TraceLog(DEBUG_LEVEL, "Notify disconnect EDGX Book is sent to AS\n");
        }
      }
      
      AddNewFlush(__FLUSH_EDGX, excludedConsumerId);
      TraceLog(DEBUG_LEVEL, "EDGX Book is disconnected\n");
      pthread_mutex_unlock(&mutex_edgx);
      break;
    case BOOK_EDGA:
      pthread_mutex_lock(&mutex_edga);
      if (BookStatusMgmt[BOOK_EDGA].isConnected == DISCONNECTED)
      {
        TraceLog(DEBUG_LEVEL, "EDGA Book is not currently connected\n");
        pthread_mutex_unlock(&mutex_edga);
        return ERROR;
      }
      
      BookStatusMgmt[BOOK_EDGA].isConnected = DISCONNECTED;
      stopVenue(DBLFilterMgmt.dbl_device, VENUE_EDGA);
      if (*(TradingStatus.status[ORDER_EDGA]) == ENABLE)
      {
        *(TradingStatus.status[ORDER_EDGA]) = DISABLE;
        if (ASConfig.socket != -1)
        {
          if (BuildSendTradingDisabled(SERVICE_EDGA_BOOK, TRADING_DISABLED_BOOK_DISCONNECT) == SUCCESS)
          {
            TraceLog(DEBUG_LEVEL, "Notify trading disabled of EDGA Order is sent to AS\n");
          }
        }
      }
      
      if (ASConfig.socket != -1)
      {
        if (BuildSendServiceDisconnect(SERVICE_EDGA_BOOK) == SUCCESS)
        {
          TraceLog(DEBUG_LEVEL, "Notify disconnect EDGA Book is sent to AS\n");
        }
      }
      
      AddNewFlush(__FLUSH_EDGA, excludedConsumerId);
      TraceLog(DEBUG_LEVEL, "EDGA Book is disconnected\n");
      pthread_mutex_unlock(&mutex_edga);
      break;
    case FEED_CQS:
      pthread_mutex_lock(&mutex_cqs);
      if (CQS_StatusMgmt.isConnected == DISCONNECTED)
      {
        TraceLog(DEBUG_LEVEL, "CQS is not currently connected\n");
        pthread_mutex_unlock(&mutex_cqs);
        return ERROR;
      }
      
      CQS_StatusMgmt.isConnected = DISCONNECTED;
      stopVenue(DBLFilterMgmt.dbl_device, VENUE_CQS);
      
      if (ASConfig.socket != -1)
      {
        if (BuildSendServiceDisconnect(SERVICE_CQS_FEED) == SUCCESS)
        {
          TraceLog(DEBUG_LEVEL, "Notify disconnect CQS is sent to AS\n");
        }
      }
      
      InitCQS_DataStructure();
      TraceLog(DEBUG_LEVEL, "CQS is disconnected\n");
      
      pthread_mutex_unlock(&mutex_cqs);
      break;
    case FEED_UQDF:
      pthread_mutex_lock(&mutex_uqdf);
      if (UQDF_StatusMgmt.isConnected == DISCONNECTED)
      {
        TraceLog(DEBUG_LEVEL, "UQDF is not currently connected\n");
        pthread_mutex_unlock(&mutex_uqdf);
        return ERROR;
      }
      
      UQDF_StatusMgmt.isConnected = DISCONNECTED;
      stopVenue(DBLFilterMgmt.dbl_device, VENUE_UQDF);
      
      if (ASConfig.socket != -1)
      {
        if (BuildSendServiceDisconnect(SERVICE_UQDF_FEED) == SUCCESS)
        {
          TraceLog(DEBUG_LEVEL, "Notify disconnect UQDF is sent to AS\n");
        }
      }
      
      InitUQDF_DataStructure();
      TraceLog(DEBUG_LEVEL, "UQDF is disconnected\n");
      
      pthread_mutex_unlock(&mutex_uqdf);
      break;
    default:
      TraceLog(ERROR_LEVEL, "ConnectToVenue() - Invalid venue id: %d\n", venueId);
      return ERROR;
  }
  
  return SUCCESS;
}

void AddNewFlush(const short venueBit, const int excludedConsumerId)
{
  int i;
  for (i = 0; i < DBLFILTER_NUM_QUEUES; i++)
  {
    if (i == excludedConsumerId) continue;
    
    while (__sync_bool_compare_and_swap(FilterBuffers[i].flushTrigger, 0, venueBit) == 0)
    {
      usleep(500);
    }
  }
}

void FlushVenueSymbol(const int bookId, const int stockSymbolIndex)
{
  DeleteAllQuoteOnASide(bookId, stockSymbolIndex, ASK_SIDE);
  DeleteAllQuoteOnASide(bookId, stockSymbolIndex, BID_SIDE);
}

void FlushVenueSymbolsOnQueue(const int bookId, const int consumerId)
{
  int i;
  t_QueueSymbolList *queueSymbols = &QueueSymbolList[consumerId];
  
  for (i = 0; i < queueSymbols->count; i++)
  {
    DeleteAllQuoteOnASide(bookId, queueSymbols->stockSymbolIndexes[i], ASK_SIDE);
    DeleteAllQuoteOnASide(bookId, queueSymbols->stockSymbolIndexes[i], BID_SIDE);
    
    // Reset stale-state to initial
    SymbolMgmt.symbolList[queueSymbols->stockSymbolIndexes[i]].isStale[bookId] = 2;
  }
  
  //DEVLOGF("Flushed %d symbols on venue %d on consumer thread %d\n", queueSymbols->count, bookId, consumerId);
}

void ___Price_Hash_Map_Utilities___(){return;}

static inline int PriceHashFunction(double price)
{
  /** method from Lucas **/
  unsigned int iPrice = (unsigned int)(price * 10000.00L + 0.01L);
  
  iPrice ^= ((iPrice >> 20) ^ (iPrice >> 12));
  
  return ((iPrice ^ (iPrice >> 7) ^ (iPrice >> 4)) & (PRICE_HASHMAP_BUCKET_SIZE - 1));
}

void DeleteAllPricePointsFromHashSlot (t_PriceHashingItem *node)
{
  t_PriceHashingItem *nextNode;
  while(node)
  {
    nextNode = node->next;
    free(node);
    node = nextNode;
  }
}

int DeletePricePointFromHashMap(const int ecnBookID, const int stockSymbolIndex, const int side, const double price)  //Reviewed
{
  int slotIndex = PriceHashFunction(price);
  t_PriceHashingItem *node = Price_HashMap[stockSymbolIndex][ecnBookID][side][slotIndex];
  
  while(node)
  {
    if (feq(node->price, price))
    {
      if (node->prev)
        node->prev->next = node->next;
      else
        Price_HashMap[stockSymbolIndex][ecnBookID][side][slotIndex] = node->next;   //Delete the head

      if (node->next)
        node->next->prev = node->prev;
      
      free(node);
      
      return SUCCESS;
    }
    
    node = node->next;
  }
  
  DEVLOGF("DeletePricePointFromHashMap(venue: %d, stockSymbolIndex: %d (%.8s), side: %d, price: %lf), could not remove from hash map", ecnBookID, stockSymbolIndex, SymbolMgmt.symbolList[stockSymbolIndex].symbol, side, price);
  return ERROR;
}

t_QuoteList* FindPricePointFromHashMapForDeletion(const int ecnBookID, const int stockSymbolIndex, const int side, const double price)  //called by NYSE/AMEX
{
  int slotIndex = PriceHashFunction(price);
  t_PriceHashingItem *node = Price_HashMap[stockSymbolIndex][ecnBookID][side][slotIndex];
  
  if (node == NULL) return NULL;    // List is empty
  
  if (side == BID_SIDE)
  {
    while (node)
    {
      if (fge(node->price, price))
      {
        if (feq(node->price, price))
          return node->list;
        else
          node = node->next;
      }
      else
        return NULL;
    }
  }
  else  // ASK_SIDE
  {
    while (node)
    {
      if (fle(node->price, price))
      {
        if (feq(node->price, price))
          return node->list;
        else
          node = node->next;
      }
      else
        return NULL;
    }
  }
  
  return NULL;
}

t_QuoteList* FindPricePointFromHashMapForInsertion(const int ecnBookID, const int stockSymbolIndex, const int side, const double price, t_PriceHashingItem **newPricePoint)
{
  int slotIndex = PriceHashFunction(price);
  t_PriceHashingItem *cur = Price_HashMap[stockSymbolIndex][ecnBookID][side][slotIndex];
  
  if (cur == NULL)
  {
    // There is no item in the bucket. We will create a new item
    *newPricePoint = calloc(1, sizeof(t_PriceHashingItem));
    if (*newPricePoint == NULL)
    {
      TraceLog(ERROR_LEVEL, "FindPricePointFromHashMapForInsertion: calloc() error\n");
      return NULL;
    }
    
    Price_HashMap[stockSymbolIndex][ecnBookID][side][slotIndex] = *newPricePoint;
    return NULL;
  }
  
  if (side == BID_SIDE)
  {
    while (cur)
    {
      if (fge(cur->price, price))
      {
        if (feq(cur->price, price))
        {
          return cur->list;
        }
        else
        {
          if (cur->next == NULL)
          {
            // New node is inserted at the end of the list (and after the current position)
            *newPricePoint = calloc(1, sizeof(t_PriceHashingItem));
            if (*newPricePoint == NULL)
            {
              TraceLog(ERROR_LEVEL, "FindPricePointFromHashMapForInsertion: calloc() error\n");
              return NULL;
            }
            
            (*newPricePoint)->prev = cur;
            cur->next = *newPricePoint;
            return NULL;
          }
          else
          {
            cur = cur->next;
          }
        }
      }
      else
      {
        // Price point is not in the list, we will add one (before current position)
        *newPricePoint = calloc(1, sizeof(t_PriceHashingItem));
        if (*newPricePoint == NULL)
        {
          TraceLog(ERROR_LEVEL, "FindPricePointFromHashMapForInsertion: calloc() error\n");
          return NULL;
        }
        
        (*newPricePoint)->prev = cur->prev;
        (*newPricePoint)->next = cur;
        
        if (cur->prev == NULL)
          Price_HashMap[stockSymbolIndex][ecnBookID][side][slotIndex] = *newPricePoint;   //New node is inserted at begining for the list
        else
          cur->prev->next = *newPricePoint; //New node is inserted in the middle of the list
        
        cur->prev = *newPricePoint;
        return NULL;
      }
    }
  }
  else  // ASK_SIDE
  {
    while (cur)
    {
      if (fle(cur->price, price))
      {
        if (feq(cur->price, price))
        {
          return cur->list;
        }
        else
        {
          if (cur->next == NULL)
          {
            // New node is inserted at the end of the list
            *newPricePoint = calloc(1, sizeof(t_PriceHashingItem));
            if(*newPricePoint == NULL)
            {
              TraceLog(ERROR_LEVEL, "FindPricePointFromHashMapForInsertion: calloc() error\n");
              return NULL;
            }
            
            (*newPricePoint)->prev = cur;
            cur->next = *newPricePoint;
            return NULL;
          }
          else
          {
            cur = cur->next;
          }
        }
      }
      else
      {
        // Price point is not in the list, we will add one (before current node)
        *newPricePoint = calloc(1, sizeof(t_PriceHashingItem));
        if(*newPricePoint == NULL)
        {
          TraceLog(ERROR_LEVEL, "FindPricePointFromHashMapForInsertion: calloc() error\n");
          return NULL;
        }
        
        (*newPricePoint)->prev = cur->prev;
        (*newPricePoint)->next = cur;
        
        if (cur->prev == NULL)
          Price_HashMap[stockSymbolIndex][ecnBookID][side][slotIndex] = *newPricePoint;   //New node is inserted at begining for the list
        else
          cur->prev->next = *newPricePoint; //New node is inserted in the middle of the list
        
        cur->prev = *newPricePoint;
        return NULL;
      }
    }
  }
  
  return NULL;
}

int InsertPricePointToHashMap(t_QuoteList *list, const int ecnBookID, const int stockSymbolIndex, const int side) //called by NYSE/AMEX
{
  double searchKey = list->price;
  t_PriceHashingItem *newNode = calloc(1, sizeof(t_PriceHashingItem));    if (newNode == NULL) {TraceLog(ERROR_LEVEL, "Calling calloc() failed\n"); return ERROR;}
  newNode->price = searchKey;
  newNode->list = list;
  
  int slotIndex = PriceHashFunction(searchKey);
  t_PriceHashingItem *head = Price_HashMap[stockSymbolIndex][ecnBookID][side][slotIndex];
  
  if (head == NULL)
  {
    //There is no price point in the bucket, we will add it
    Price_HashMap[stockSymbolIndex][ecnBookID][side][slotIndex] = newNode;
    return SUCCESS;
  }
  
  // This bucket is not empty, we will find a position to add the new price point
  t_PriceHashingItem *cur = head;
  if (side == BID_SIDE)
  {
    while(cur)
    {
      if (fgt(searchKey, cur->price))
      {
        newNode->prev = cur->prev;
        newNode->next = cur;
        
        if (cur->prev == NULL)
          Price_HashMap[stockSymbolIndex][ecnBookID][side][slotIndex] = newNode;    //Newcomer is inserted at begining for the list
        else
          cur->prev->next = newNode;  //Newcomer is inserted in the middle of the list
        
        cur->prev = newNode;

        return SUCCESS;
      }
      else
      {
        if (cur->next == NULL)
        {
          // Newcomer is inserted at the end of the list
          newNode->prev = cur;
          cur->next = newNode;
          return SUCCESS;
        }
        else
        {
          cur = cur->next;
        }
      }
    }
  }
  else // ASK_SIDE
  {
    while (cur)
    {
      if (flt(searchKey, cur->price))
      {
        newNode->prev = cur->prev;
        newNode->next = cur;
        
        if (cur->prev == NULL)
          Price_HashMap[stockSymbolIndex][ecnBookID][side][slotIndex] = newNode;    //Newcomer is inserted at begining for the list
        else
          cur->prev->next = newNode;  //Newcomer is inserted in the middle of the list
        
        cur->prev = newNode;
        return SUCCESS;
      }
      else
      {
        if (cur->next == NULL)
        {
          // Newcomer is inserted at the end of the list
          newNode->prev = cur;
          cur->next = newNode;
          break;
        }
        else
        {
          cur = cur->next;
        }
      }
    }
  }
  
  return SUCCESS;
}

void ___LinkedList_Utilities___(){return;}

/****************************************************************************
- Function name:  DestroyAllLinkedListDataStructure
****************************************************************************/
void DestroyAllLinkedListDataStructure(void)
{
  int ecnBookIndex;
  int sideIndex;
  int stockSymbolIndex;

  t_QuoteList *currentList, *nextList;
  for (ecnBookIndex = 0; ecnBookIndex < MAX_ECN_BOOK; ecnBookIndex++)
  {
    for (sideIndex = 0; sideIndex < MAX_SIDE; sideIndex++)
    {
      for (stockSymbolIndex = 0; stockSymbolIndex < MAX_STOCK_SYMBOL; stockSymbolIndex++)
      {
        currentList = StockBook[stockSymbolIndex][ecnBookIndex][sideIndex];
        while (currentList)
        {
          nextList = currentList->next;
          DestroyQuoteList(currentList);
          currentList = nextList;
        }
      }
    }
  }
}

void DestroyQuoteList(t_QuoteList *list)
{
  if (list == NULL) return;
  
  t_QuoteNode *cur;
  t_QuoteNode *node = list->head;
  
  while (node)
  {
    cur = node;
    node = node->next;
    free(cur);
    //GarbageCollection[__sync_fetch_and_add(&CurrentGarbageQueueIndex, 1) % GARBAGE_COLLECTION_SIZE] = cur;
  }

    free(list);
  //GarbageCollection[__sync_fetch_and_add(&CurrentGarbageQueueIndex, 1) % GARBAGE_COLLECTION_SIZE] = list;
}

int DeleteAllQuoteOnASide(int ecnBookID, int stockSymbolIndex, int side)
{
  t_QuoteList *list, *tmpList;
  t_QuoteNode *quote, *tmpQuote;
  
  list = StockBook[stockSymbolIndex][ecnBookID][side];

  while (list) //Travel all price points
  {
    tmpList = list->next;
    
    // Delete all hashing/backup-hashing items and quotes in the list
    quote = list->head;
    while(quote) //Travel all quotes in the current price point
    {
      tmpQuote = quote->next;
      if (ecnBookID != BOOK_NYSE && ecnBookID != BOOK_AMEX) OrderMapRemove(OrderMaps[ecnBookID][_consumerId], quote->info.refNum);
      free(quote);
      quote = tmpQuote;
    }
    
    // Delete this price and all other prices from same price-hash-map slot
    int slotIndex = PriceHashFunction(list->price);
    DeleteAllPricePointsFromHashSlot(Price_HashMap[stockSymbolIndex][ecnBookID][side][slotIndex]);
    Price_HashMap[stockSymbolIndex][ecnBookID][side][slotIndex] = NULL;
    
    // Continue to delete next price point
    free (list);
    list = tmpList;
  }

  //Reset price point list data structure
  StockBook[stockSymbolIndex][ecnBookID][side] = NULL;
  
  //Reset Max-Bid / Min-Ask
  if (side == ASK_SIDE)
    UpdateMinAsk(stockSymbolIndex);
  else
    UpdateMaxBid(stockSymbolIndex);
  
  return SUCCESS;
}

int DeleteQuote(t_QuoteNode *node, const int ecnId, const int side, const int stockSymbolIndex) //call from book & DeleteAllQuotesFromASide
{
  if(node == DeletedQuotePlaceholder) {
    // this is a place holder, don't actually delete it       
    return SUCCESS;
  }
  
  if (node->next == node->prev) // This is the only one node in the quote-list, so the list will be deleted
  {
    t_QuoteList *list = node->myList;
    
    /** Update master list **/
    if (list->prev)
      list->prev->next = list->next;
    else
      StockBook[stockSymbolIndex][ecnId][side] = list->next;
      
    if (list->next)
      list->next->prev = list->prev;
        
    /** Remove from hash map **/
    DeletePricePointFromHashMap(ecnId, stockSymbolIndex, side, list->price);
    
    /** Update Maxbid/minask **/
    if (side == BID_SIDE)
    {
      if (feq(node->info.sharePrice, BestQuote[stockSymbolIndex][BID_SIDE].sharePrice))
      {
        UpdateMaxBid(stockSymbolIndex);
      }
    }
    else
    {
      if (feq(node->info.sharePrice, BestQuote[stockSymbolIndex][ASK_SIDE].sharePrice))
      {
        UpdateMinAsk(stockSymbolIndex);
      }
    }
    
    /** Free memory **/
    free(node);
    free(list);
    //GarbageCollection[__sync_fetch_and_add(&CurrentGarbageQueueIndex, 1) % GARBAGE_COLLECTION_SIZE] = list;
    //GarbageCollection[__sync_fetch_and_add(&CurrentGarbageQueueIndex, 1) % GARBAGE_COLLECTION_SIZE] = node;

    return SUCCESS;
  }
  
  /** Delete the quote in a list that has more than one quote**/
  node->myList->totalVolume -= node->info.shareVolume;

  if (node->prev)
    node->prev->next = node->next;
  else
    node->myList->head = node->next;  //delete at beginning of the list
  
  if (node->next)
    node->next->prev = node->prev;
  
  /** Update Maxbid/minask **/
  if (side == BID_SIDE)
  {
    if (feq(node->info.sharePrice, BestQuote[stockSymbolIndex][BID_SIDE].sharePrice))
    {
      UpdateMaxBid(stockSymbolIndex);
    }
  }
  else
  {
    if (feq(node->info.sharePrice, BestQuote[stockSymbolIndex][ASK_SIDE].sharePrice))
    {
      UpdateMinAsk(stockSymbolIndex);
    }
  }
  
  free (node);
  //GarbageCollection[__sync_fetch_and_add(&CurrentGarbageQueueIndex, 1) % GARBAGE_COLLECTION_SIZE] = node;
  return SUCCESS;
}

/****************************************************************************
- Function name:  DeleteNysePriceLevel
- Description:    Currently Nyse update with price level, all volumes are 
                  aggregated into one single quote, so this function just
                  delete only one node which has same price as input
****************************************************************************/
int DeleteNysePriceLevel(const unsigned char ecnIndex, const double price, const int side, const int stockSymbolIndex)
{
  t_QuoteList *quoteList = FindPricePointFromHashMapForDeletion(ecnIndex, stockSymbolIndex, side, price);
  if (quoteList == NULL) return ERROR;
  
  t_QuoteNode *node = quoteList->head;
  
  if (node->next == node->prev) // This is the only one node in the quote-list, so the list will be deleted
  {
    /** Update master list **/
    if (quoteList->prev)
      quoteList->prev->next = quoteList->next;
    else
      StockBook[stockSymbolIndex][ecnIndex][side] = quoteList->next;
      
    if (quoteList->next)
      quoteList->next->prev = quoteList->prev;
    
    /** Remove from hash map **/
    DeletePricePointFromHashMap(ecnIndex, stockSymbolIndex, side, price);
    
    /** Update Maxbid/minask **/
    if (side == BID_SIDE)
    {
      if (feq(price, BestQuote[stockSymbolIndex][BID_SIDE].sharePrice))
      {
        UpdateMaxBid(stockSymbolIndex);
      }
    }
    else
    {
      if (feq(price, BestQuote[stockSymbolIndex][ASK_SIDE].sharePrice))
      {
        UpdateMinAsk(stockSymbolIndex);
      }
    }
    
    /** Free memory **/
    free(quoteList);
    free(node);
    //GarbageCollection[__sync_fetch_and_add(&CurrentGarbageQueueIndex, 1) % GARBAGE_COLLECTION_SIZE] = quoteList;
    //GarbageCollection[__sync_fetch_and_add(&CurrentGarbageQueueIndex, 1) % GARBAGE_COLLECTION_SIZE] = node;
    
    return SUCCESS;
  }
  
  TraceLog(DEBUG_LEVEL, "%s: DeleteNysePriceLevel %lf failed\n", GetBookNameByIndex(ecnIndex), price);
  return ERROR;
}

/****************************************************************************
- Function name:  InsertOrUpdateNysePriceLevel
- Description:    Currently Nyse update with price level, all volumes are 
                  aggregated into one single quote, so this function just
                  insert only one node, or update that node(if found)
                  which has same price as input
****************************************************************************/
int InsertOrUpdateNysePriceLevel(const unsigned char ecnIndex, const t_OrderDetail *info, const int side, const int stockSymbolIndex)
{
  double searchKey = info->sharePrice;
  
  if (StockBook[stockSymbolIndex][ecnIndex][side] == NULL) //List is empty
  {
    t_QuoteList *newList = calloc(1, sizeof(t_QuoteList));    if (newList == NULL) { TraceLog(FATAL_LEVEL, "Calling calloc() failed\n");  return ERROR; }
    t_QuoteNode *newQuote = calloc(1, sizeof(t_QuoteNode));   if (newQuote == NULL) { TraceLog(FATAL_LEVEL, "Calling calloc() failed\n"); return ERROR; }
    newQuote->info = *info;
    newQuote->myList = newList;
    
    newList->price = searchKey;
    newList->totalVolume = info->shareVolume;
    newList->head = newQuote;
    
    /** Add to HashMap **/
    if (InsertPricePointToHashMap(newList, ecnIndex, stockSymbolIndex, side) == ERROR)
    {
      return ERROR; // hash map is full!
    }
    
    StockBook[stockSymbolIndex][ecnIndex][side] = newList;
    
    // Update MaxBid, MinAsk
    if (side == BID_SIDE)
    {
      if (fge(searchKey, BestQuote[stockSymbolIndex][BID_SIDE].sharePrice))
      {
        UpdateMaxBid(stockSymbolIndex);
      }
    }
    else
    {
      if (fle(searchKey, BestQuote[stockSymbolIndex][ASK_SIDE].sharePrice) || (BestQuote[stockSymbolIndex][ASK_SIDE].sharePrice < 0.0001) )
      {
        UpdateMinAsk(stockSymbolIndex);
      }
    }
    return SUCCESS;
  }
  
  /** Price point list is not empty, we need to find a location to insert/or update new price point **/
  t_PriceHashingItem *newPriceNode = NULL;
  t_QuoteList *list = FindPricePointFromHashMapForInsertion(ecnIndex, stockSymbolIndex, side, searchKey, &newPriceNode);
  if (list)
  {
    // Price level is existing, we will update
    list->totalVolume = info->shareVolume;
    list->head->info.shareVolume = info->shareVolume;
    
    if (side == BID_SIDE)
    {
      if (feq(searchKey, BestQuote[stockSymbolIndex][BID_SIDE].sharePrice))
      {
        UpdateMaxBid(stockSymbolIndex);
      }
    }
    else
    {
      if (feq(searchKey, BestQuote[stockSymbolIndex][ASK_SIDE].sharePrice))
      {
        UpdateMinAsk(stockSymbolIndex);
      }
    }
    
    return SUCCESS;
  }
  
  if (newPriceNode == NULL) return ERROR;
  
  /** Price point is not existing, we will need to find location to add it **/
  t_QuoteList *quoteList = calloc(1, sizeof(t_QuoteList));    if (quoteList == NULL) { TraceLog(FATAL_LEVEL, "Calling calloc() failed\n");  return ERROR; }
  t_QuoteNode *newQuote = calloc(1, sizeof(t_QuoteNode));     if (newQuote == NULL) { TraceLog(FATAL_LEVEL, "Calling calloc() failed\n");   return ERROR; }
  newQuote->info = *info;
  newQuote->myList = quoteList;
  
  quoteList->totalVolume = info->shareVolume;
  quoteList->price = searchKey;
  quoteList->head = newQuote;
  
  /** Add to HashMap **/
  newPriceNode->price = searchKey;
  newPriceNode->list = quoteList;
  
  // Look for position to insert the quote-list into master list (list of price points)
  t_QuoteList *cur = StockBook[stockSymbolIndex][ecnIndex][side];
  
  if (side == BID_SIDE)
  {
    while (cur != NULL)
    {
      if (fge(searchKey, cur->price))
      {
        quoteList->prev = cur->prev;
        quoteList->next = cur;
        
        if (cur->prev == NULL)
          StockBook[stockSymbolIndex][ecnIndex][side] = quoteList; // Newcomer is inserted at begining for the list
        else
          cur->prev->next = quoteList;  // Newcomer is inserted in the middle of the list
        
        cur->prev = quoteList;
        break;
      }
      else
      {
        if (cur->next == NULL)
        {
          // Newcomer is inserted at the end of the list
          quoteList->prev = cur;
          cur->next = quoteList;
          break;
        }
        else
        {
          cur = cur->next;
        }
      }
    }
  }
  else  // ask side
  {
    while (cur != NULL)
    {
      if (fle(searchKey, cur->price))
      {
        quoteList->prev = cur->prev;
        quoteList->next = cur;
        
        if (cur->prev == NULL)
          StockBook[stockSymbolIndex][ecnIndex][side] = quoteList; // Newcomer is inserted at begining for the list
        else
          cur->prev->next = quoteList;  // Newcomer is inserted in the middle of the list
        
        cur->prev = quoteList;
        break;
      }
      else
      {
        if (cur->next == NULL)
        {
          // Newcomer is inserted at the end of the list
          quoteList->prev = cur;
          cur->next = quoteList;
          break;
        }
        else
        {
          cur = cur->next;
        }
      }
    }
  }
  
  if (side == BID_SIDE)
  {
    if (fge(searchKey, BestQuote[stockSymbolIndex][BID_SIDE].sharePrice))
    {
      UpdateMaxBid(stockSymbolIndex);
    }
  }
  else
  {
    if (fle(searchKey, BestQuote[stockSymbolIndex][ASK_SIDE].sharePrice) || (BestQuote[stockSymbolIndex][ASK_SIDE].sharePrice < 0.0001))
    {
      UpdateMinAsk(stockSymbolIndex);
    }
  }
  
  return SUCCESS;
}

int DeleteFromPrice(const double price, const int ecnBookID, const int side, const int stockSymbolIndex)
{
  t_QuoteList *cur = StockBook[stockSymbolIndex][ecnBookID][side];
  t_QuoteList *listToDelete[300];
  
  int i = 0, count = 0;
  
  if (side == BID_SIDE)
  {
    while (cur)
    {
      if (fge(cur->price, price))
      {
        if (cur->prev)
          cur->prev->next = cur->next;
        else
          StockBook[stockSymbolIndex][ecnBookID][side] = cur->next;   //Delete the head

        if (cur->next)
          cur->next->prev = cur->prev;
        
        listToDelete[count++] = cur;
        if (count > 299)
        {
          TraceLog(ERROR_LEVEL, "DeleteFromPrice(): Too many nodes to be deleted");
          break;
        }
        cur = cur->next;
      }
      else
      {
        break;  // There will be no more list to delete
      }
    }
  }
  else  //Ask side
  {
    while (cur)
    {
      if (fle(cur->price, price))
      {
        if (cur->prev)
          cur->prev->next = cur->next;
        else
          StockBook[stockSymbolIndex][ecnBookID][side] = cur->next;   //Delete the head

        if (cur->next)
          cur->next->prev = cur->prev;
        
        listToDelete[count++] = cur;
        if (count > 299)
        {
          TraceLog(ERROR_LEVEL, "DeleteFromPrice(): Too many nodes to be deleted");
          break;
        }
        cur = cur->next;
      }
      else
      {
        break;  // There will be no more list to delete
      }
    }
  }
  
  switch (ecnBookID)
  {
    case BOOK_ARCA:
      for (i = 0; i < count; i++)
      {
        t_QuoteNode *quote = listToDelete[i]->head;
        t_QuoteNode *nextQuote;
        while(quote)
        {
          nextQuote = quote->next;
          t_QuoteNodeInfo nodeInfo = OrderMapRemove(ARCA_OrderMap[_consumerId], quote->info.refNum);
          if(nodeInfo.node != NULL) {
            OrderMapInsert(ARCA_OrderMap[_consumerId], quote->info.refNum, DeletedQuotePlaceholder, nodeInfo.side);
          }
          free(quote);
          //GarbageCollection[__sync_fetch_and_add(&CurrentGarbageQueueIndex, 1) % GARBAGE_COLLECTION_SIZE] = quote;
          quote = nextQuote;
        }
        
        DeletePricePointFromHashMap(BOOK_ARCA, stockSymbolIndex, side, listToDelete[i]->price);
        free(listToDelete[i]);
        //GarbageCollection[__sync_fetch_and_add(&CurrentGarbageQueueIndex, 1) % GARBAGE_COLLECTION_SIZE] = listToDelete[i];
      }
      break;
    case BOOK_NASDAQ:
      for (i = 0; i < count; i++)
      {
        t_QuoteNode *quote = listToDelete[i]->head;
        t_QuoteNode *nextQuote;
        while(quote)
        {
          nextQuote = quote->next;
          t_QuoteNodeInfo nodeInfo = OrderMapRemove(NASDAQ_OrderMap[_consumerId], quote->info.refNum);
          if(nodeInfo.node != NULL) {
            OrderMapInsert(NASDAQ_OrderMap[_consumerId], quote->info.refNum, DeletedQuotePlaceholder, nodeInfo.side);
          }
          
          free(quote);
          //GarbageCollection[__sync_fetch_and_add(&CurrentGarbageQueueIndex, 1) % GARBAGE_COLLECTION_SIZE] = quote;
          quote = nextQuote;
        }
        
        DeletePricePointFromHashMap(BOOK_NASDAQ, stockSymbolIndex, side, listToDelete[i]->price);
        free(listToDelete[i]);
        //GarbageCollection[__sync_fetch_and_add(&CurrentGarbageQueueIndex, 1) % GARBAGE_COLLECTION_SIZE] = listToDelete[i];
      }
      break;
    case BOOK_NDBX:
      for (i = 0; i < count; i++)
      {
        t_QuoteNode *quote = listToDelete[i]->head;
        t_QuoteNode *nextQuote;
        while(quote)
        {
          nextQuote = quote->next;
          t_QuoteNodeInfo nodeInfo = OrderMapRemove(NDBX_OrderMap[_consumerId], quote->info.refNum);
          if(nodeInfo.node != NULL) {
            OrderMapInsert(NDBX_OrderMap[_consumerId], quote->info.refNum, DeletedQuotePlaceholder, nodeInfo.side);
          }
          
          free(quote);
          //GarbageCollection[__sync_fetch_and_add(&CurrentGarbageQueueIndex, 1) % GARBAGE_COLLECTION_SIZE] = quote;
          quote = nextQuote;
        }
        
        DeletePricePointFromHashMap(BOOK_NDBX, stockSymbolIndex, side, listToDelete[i]->price);
        free(listToDelete[i]);
        //GarbageCollection[__sync_fetch_and_add(&CurrentGarbageQueueIndex, 1) % GARBAGE_COLLECTION_SIZE] = listToDelete[i];
      }
      break;
    case BOOK_PSX:
      for (i = 0; i < count; i++)
      {
        t_QuoteNode *quote = listToDelete[i]->head;
        t_QuoteNode *nextQuote;
        while(quote)
        {
          nextQuote = quote->next;
          t_QuoteNodeInfo nodeInfo = OrderMapRemove(PSX_OrderMap[_consumerId], quote->info.refNum);
          if(nodeInfo.node != NULL) {
            OrderMapInsert(PSX_OrderMap[_consumerId], quote->info.refNum, DeletedQuotePlaceholder, nodeInfo.side);
          }
          
          free(quote);
          //GarbageCollection[__sync_fetch_and_add(&CurrentGarbageQueueIndex, 1) % GARBAGE_COLLECTION_SIZE] = quote;
          quote = nextQuote;
        }
        
        DeletePricePointFromHashMap(BOOK_PSX, stockSymbolIndex, side, listToDelete[i]->price);
        free(listToDelete[i]);
        //GarbageCollection[__sync_fetch_and_add(&CurrentGarbageQueueIndex, 1) % GARBAGE_COLLECTION_SIZE] = listToDelete[i];
      }
      break;
    case BOOK_BATSZ:
      for (i = 0; i < count; i++)
      {
        t_QuoteNode *quote = listToDelete[i]->head;
        t_QuoteNode *nextQuote;
        while(quote)
        {
          nextQuote = quote->next;
          t_QuoteNodeInfo nodeInfo = OrderMapRemove(BATSZ_OrderMap[_consumerId], quote->info.refNum);  
          if(nodeInfo.node != NULL) {
            OrderMapInsert(BATSZ_OrderMap[_consumerId], quote->info.refNum, DeletedQuotePlaceholder, nodeInfo.side);
          }
          
          free(quote);
          //GarbageCollection[__sync_fetch_and_add(&CurrentGarbageQueueIndex, 1) % GARBAGE_COLLECTION_SIZE] = quote;
          quote = nextQuote;
        }
        
        DeletePricePointFromHashMap(BOOK_BATSZ, stockSymbolIndex, side, listToDelete[i]->price);
        free(listToDelete[i]);
        ///GarbageCollection[__sync_fetch_and_add(&CurrentGarbageQueueIndex, 1) % GARBAGE_COLLECTION_SIZE] = listToDelete[i];
      }
      break;
    case BOOK_BYX:
      for (i = 0; i < count; i++)
      {
        t_QuoteNode *quote = listToDelete[i]->head;
        t_QuoteNode *nextQuote;
        while(quote)
        {
          nextQuote = quote->next;
          t_QuoteNodeInfo nodeInfo = OrderMapRemove(BYX_OrderMap[_consumerId], quote->info.refNum);  
          if(nodeInfo.node != NULL) {
            OrderMapInsert(BYX_OrderMap[_consumerId], quote->info.refNum, DeletedQuotePlaceholder, nodeInfo.side);
          }
          
          free(quote);
          //GarbageCollection[__sync_fetch_and_add(&CurrentGarbageQueueIndex, 1) % GARBAGE_COLLECTION_SIZE] = quote;
          quote = nextQuote;
        }
        
        DeletePricePointFromHashMap(BOOK_BYX, stockSymbolIndex, side, listToDelete[i]->price);
        free(listToDelete[i]);
        ///GarbageCollection[__sync_fetch_and_add(&CurrentGarbageQueueIndex, 1) % GARBAGE_COLLECTION_SIZE] = listToDelete[i];
      }
      break;
    case BOOK_EDGX:
      for (i = 0; i < count; i++)
      {
        t_QuoteNode *quote = listToDelete[i]->head;
        t_QuoteNode *nextQuote;
        while(quote)
        {
          nextQuote = quote->next;
          t_QuoteNodeInfo nodeInfo = OrderMapRemove(EDGX_OrderMap[_consumerId], quote->info.refNum);  
          if(nodeInfo.node != NULL) {
            OrderMapInsert(EDGX_OrderMap[_consumerId], quote->info.refNum, DeletedQuotePlaceholder, nodeInfo.side);
          }
    
          free(quote);
          //GarbageCollection[__sync_fetch_and_add(&CurrentGarbageQueueIndex, 1) % GARBAGE_COLLECTION_SIZE] = quote;
          quote = nextQuote;
        }
        
        DeletePricePointFromHashMap(BOOK_EDGX, stockSymbolIndex, side, listToDelete[i]->price);
        free(listToDelete[i]);
        //GarbageCollection[__sync_fetch_and_add(&CurrentGarbageQueueIndex, 1) % GARBAGE_COLLECTION_SIZE] = listToDelete[i];
      }
      break;
    case BOOK_EDGA:
      for (i = 0; i < count; i++)
      {
        t_QuoteNode *quote = listToDelete[i]->head;
        t_QuoteNode *nextQuote;
        while(quote)
        {
          nextQuote = quote->next;
          t_QuoteNodeInfo nodeInfo = OrderMapRemove(EDGA_OrderMap[_consumerId], quote->info.refNum);  
          if(nodeInfo.node != NULL) {
            OrderMapInsert(EDGA_OrderMap[_consumerId], quote->info.refNum, DeletedQuotePlaceholder, nodeInfo.side);
          }
    
          free(quote);
          //GarbageCollection[__sync_fetch_and_add(&CurrentGarbageQueueIndex, 1) % GARBAGE_COLLECTION_SIZE] = quote;
          quote = nextQuote;
        }
        
        DeletePricePointFromHashMap(BOOK_EDGA, stockSymbolIndex, side, listToDelete[i]->price);
        free(listToDelete[i]);
        //GarbageCollection[__sync_fetch_and_add(&CurrentGarbageQueueIndex, 1) % GARBAGE_COLLECTION_SIZE] = listToDelete[i];
      }
      break;
    default:  //Should be NYSE or AMEX
      for (i = 0; i < count; i++)
      {
        t_QuoteNode *quote = listToDelete[i]->head;
        t_QuoteNode *nextQuote;
        while(quote)
        {
          nextQuote = quote->next;
          free(quote);
          //GarbageCollection[__sync_fetch_and_add(&CurrentGarbageQueueIndex, 1) % GARBAGE_COLLECTION_SIZE] = quote;
          quote = nextQuote;
        }
        
        DeletePricePointFromHashMap(ecnBookID, stockSymbolIndex, side, listToDelete[i]->price);
        free(listToDelete[i]);
        //GarbageCollection[__sync_fetch_and_add(&CurrentGarbageQueueIndex, 1) % GARBAGE_COLLECTION_SIZE] = listToDelete[i];
      }
      break;
  }
  
  // Update MaxBid, MinAsk
  if (side == BID_SIDE) // Update MaxBid
  {
    if (fle(price, BestQuote[stockSymbolIndex][BID_SIDE].sharePrice))
    {
      UpdateMaxBid(stockSymbolIndex);
    }
  }
  else  // Update MinAsk
  {
    if (fge(price, BestQuote[stockSymbolIndex][ASK_SIDE].sharePrice))
    {
      UpdateMinAsk(stockSymbolIndex);
    }
  }

  return SUCCESS;
}

int DeleteBetterPrice(const double price, const int ecnBookID, const int side, const int stockSymbolIndex)
{
  int consumerId = _consumerId;
  t_QuoteList *cur = StockBook[stockSymbolIndex][ecnBookID][side];
  t_QuoteList *listToDelete[200];
  
  int i = 0, count = 0;
  
  if (side == BID_SIDE)
  {
    while (cur)
    {
      if (fgt(cur->price, price))
      {
        if (cur->prev)
          cur->prev->next = cur->next;
        else
          StockBook[stockSymbolIndex][ecnBookID][side] = cur->next;   //Delete the head

        if (cur->next)
          cur->next->prev = cur->prev;
        
        listToDelete[count++] = cur;
        if (count > 199) break;
        cur = cur->next;
      }
      else
      {
        break;  // There will be no more list to delete
      }
    }
  }
  else  //Ask side
  {
    while (cur)
    {
      if (flt(cur->price, price))
      {
        if (cur->prev)
          cur->prev->next = cur->next;
        else
          StockBook[stockSymbolIndex][ecnBookID][side] = cur->next;   //Delete the head

        if (cur->next)
          cur->next->prev = cur->prev;
        
        listToDelete[count++] = cur;
        if (count > 199) break;
        cur = cur->next;
      }
      else
      {
        break;  // There will be no more list to delete
      }
    }
  }
  
  switch (ecnBookID)
  {
    case BOOK_ARCA:
      for (i = 0; i < count; i++)
      {
        t_QuoteNode *quote = listToDelete[i]->head;
        t_QuoteNode *nextQuote;
        while(quote)
        {
          nextQuote = quote->next;
          t_QuoteNodeInfo nodeInfo = OrderMapRemove(ARCA_OrderMap[consumerId], quote->info.refNum);  
          if(nodeInfo.node != NULL) {
            OrderMapInsert(ARCA_OrderMap[consumerId], quote->info.refNum, DeletedQuotePlaceholder, nodeInfo.side);
          }
          free(quote);
          //GarbageCollection[__sync_fetch_and_add(&CurrentGarbageQueueIndex, 1) % GARBAGE_COLLECTION_SIZE] = quote;
          quote = nextQuote;
        }
        
        DeletePricePointFromHashMap(BOOK_ARCA, stockSymbolIndex, side, listToDelete[i]->price);
        free(listToDelete[i]);
        //GarbageCollection[__sync_fetch_and_add(&CurrentGarbageQueueIndex, 1) % GARBAGE_COLLECTION_SIZE] = listToDelete[i];
      }
      break;
    case BOOK_NASDAQ:
      for (i = 0; i < count; i++)
      {
        t_QuoteNode *quote = listToDelete[i]->head;
        t_QuoteNode *nextQuote;
        while(quote)
        {
          nextQuote = quote->next;
          t_QuoteNodeInfo nodeInfo = OrderMapRemove(NASDAQ_OrderMap[consumerId], quote->info.refNum);  
          if(nodeInfo.node != NULL) {
            OrderMapInsert(NASDAQ_OrderMap[consumerId], quote->info.refNum, DeletedQuotePlaceholder, nodeInfo.side);
          }
          free(quote);
          //GarbageCollection[__sync_fetch_and_add(&CurrentGarbageQueueIndex, 1) % GARBAGE_COLLECTION_SIZE] = quote;
          quote = nextQuote;
        }
        
        DeletePricePointFromHashMap(BOOK_NASDAQ, stockSymbolIndex, side, listToDelete[i]->price);
        free(listToDelete[i]);
        //GarbageCollection[__sync_fetch_and_add(&CurrentGarbageQueueIndex, 1) % GARBAGE_COLLECTION_SIZE] = listToDelete[i];
      }
      break;
    case BOOK_NDBX:
      for (i = 0; i < count; i++)
      {
        t_QuoteNode *quote = listToDelete[i]->head;
        t_QuoteNode *nextQuote;
        while(quote)
        {
          nextQuote = quote->next;
          t_QuoteNodeInfo nodeInfo = OrderMapRemove(NDBX_OrderMap[consumerId], quote->info.refNum);  
          if(nodeInfo.node != NULL) {
            OrderMapInsert(NDBX_OrderMap[consumerId], quote->info.refNum, DeletedQuotePlaceholder, nodeInfo.side);
          }
          free(quote);
          //GarbageCollection[__sync_fetch_and_add(&CurrentGarbageQueueIndex, 1) % GARBAGE_COLLECTION_SIZE] = quote;
          quote = nextQuote;
        }
        
        DeletePricePointFromHashMap(BOOK_NDBX, stockSymbolIndex, side, listToDelete[i]->price);
        free(listToDelete[i]);
        //GarbageCollection[__sync_fetch_and_add(&CurrentGarbageQueueIndex, 1) % GARBAGE_COLLECTION_SIZE] = listToDelete[i];
      }
      break;
    case BOOK_PSX:
      for (i = 0; i < count; i++)
      {
        t_QuoteNode *quote = listToDelete[i]->head;
        t_QuoteNode *nextQuote;
        while(quote)
        {
          nextQuote = quote->next;
          t_QuoteNodeInfo nodeInfo = OrderMapRemove(PSX_OrderMap[consumerId], quote->info.refNum);  
          if(nodeInfo.node != NULL) {
            OrderMapInsert(PSX_OrderMap[consumerId], quote->info.refNum, DeletedQuotePlaceholder, nodeInfo.side);
          }
          free(quote);
          //GarbageCollection[__sync_fetch_and_add(&CurrentGarbageQueueIndex, 1) % GARBAGE_COLLECTION_SIZE] = quote;
          quote = nextQuote;
        }
        
        DeletePricePointFromHashMap(BOOK_PSX, stockSymbolIndex, side, listToDelete[i]->price);
        free(listToDelete[i]);
        //GarbageCollection[__sync_fetch_and_add(&CurrentGarbageQueueIndex, 1) % GARBAGE_COLLECTION_SIZE] = listToDelete[i];
      }
      break;
    case BOOK_BATSZ:
      for (i = 0; i < count; i++)
      {
        t_QuoteNode *quote = listToDelete[i]->head;
        t_QuoteNode *nextQuote;
        while(quote)
        {
          nextQuote = quote->next;
          t_QuoteNodeInfo nodeInfo = OrderMapRemove(BATSZ_OrderMap[consumerId], quote->info.refNum);  
          if(nodeInfo.node != NULL) {
            OrderMapInsert(BATSZ_OrderMap[consumerId], quote->info.refNum, DeletedQuotePlaceholder, nodeInfo.side);
          } 
          free(quote);
          //GarbageCollection[__sync_fetch_and_add(&CurrentGarbageQueueIndex, 1) % GARBAGE_COLLECTION_SIZE] = quote;
          quote = nextQuote;
        }
        
        DeletePricePointFromHashMap(BOOK_BATSZ, stockSymbolIndex, side, listToDelete[i]->price);
        free(listToDelete[i]);
        //GarbageCollection[__sync_fetch_and_add(&CurrentGarbageQueueIndex, 1) % GARBAGE_COLLECTION_SIZE] = listToDelete[i];
      }
      break;
    case BOOK_BYX:
      for (i = 0; i < count; i++)
      {
        t_QuoteNode *quote = listToDelete[i]->head;
        t_QuoteNode *nextQuote;
        while(quote)
        {
          nextQuote = quote->next;
          t_QuoteNodeInfo nodeInfo = OrderMapRemove(BYX_OrderMap[consumerId], quote->info.refNum);  
          if(nodeInfo.node != NULL) {
            OrderMapInsert(BYX_OrderMap[consumerId], quote->info.refNum, DeletedQuotePlaceholder, nodeInfo.side);
          } 
          free(quote);
          //GarbageCollection[__sync_fetch_and_add(&CurrentGarbageQueueIndex, 1) % GARBAGE_COLLECTION_SIZE] = quote;
          quote = nextQuote;
        }
        
        DeletePricePointFromHashMap(BOOK_BYX, stockSymbolIndex, side, listToDelete[i]->price);
        free(listToDelete[i]);
        //GarbageCollection[__sync_fetch_and_add(&CurrentGarbageQueueIndex, 1) % GARBAGE_COLLECTION_SIZE] = listToDelete[i];
      }
      break;
    case BOOK_EDGX:
      for (i = 0; i < count; i++)
      {
        t_QuoteNode *quote = listToDelete[i]->head;
        t_QuoteNode *nextQuote;
        while(quote)
        {
          nextQuote = quote->next;
          t_QuoteNodeInfo nodeInfo = OrderMapRemove(EDGX_OrderMap[consumerId], quote->info.refNum);  
          if(nodeInfo.node != NULL) {
            OrderMapInsert(EDGX_OrderMap[consumerId], quote->info.refNum, DeletedQuotePlaceholder, nodeInfo.side);
          } 
          free(quote);
          //GarbageCollection[__sync_fetch_and_add(&CurrentGarbageQueueIndex, 1) % GARBAGE_COLLECTION_SIZE] = quote;
          quote = nextQuote;
        }
        
        DeletePricePointFromHashMap(BOOK_EDGX, stockSymbolIndex, side, listToDelete[i]->price);
        
        free(listToDelete[i]);
        //GarbageCollection[__sync_fetch_and_add(&CurrentGarbageQueueIndex, 1) % GARBAGE_COLLECTION_SIZE] = listToDelete[i];
      }
      break;
    case BOOK_EDGA:
      for (i = 0; i < count; i++)
      {
        t_QuoteNode *quote = listToDelete[i]->head;
        t_QuoteNode *nextQuote;
        while(quote)
        {
          nextQuote = quote->next;
          t_QuoteNodeInfo nodeInfo = OrderMapRemove(EDGA_OrderMap[consumerId], quote->info.refNum);  
          if(nodeInfo.node != NULL) {
            OrderMapInsert(EDGA_OrderMap[consumerId], quote->info.refNum, DeletedQuotePlaceholder, nodeInfo.side);
          } 
          free(quote);
          //GarbageCollection[__sync_fetch_and_add(&CurrentGarbageQueueIndex, 1) % GARBAGE_COLLECTION_SIZE] = quote;
          quote = nextQuote;
        }
        
        DeletePricePointFromHashMap(BOOK_EDGA, stockSymbolIndex, side, listToDelete[i]->price);
        
        free(listToDelete[i]);
        //GarbageCollection[__sync_fetch_and_add(&CurrentGarbageQueueIndex, 1) % GARBAGE_COLLECTION_SIZE] = listToDelete[i];
      }
      break;
    default: //NYSE or AMEX
      for (i = 0; i < count; i++)
      {
        t_QuoteNode *quote = listToDelete[i]->head;
        t_QuoteNode *nextQuote;
        while(quote)
        {
          nextQuote = quote->next;
          free(quote);
          //GarbageCollection[__sync_fetch_and_add(&CurrentGarbageQueueIndex, 1) % GARBAGE_COLLECTION_SIZE] = quote;
          quote = nextQuote;
        }
        
        DeletePricePointFromHashMap(ecnBookID, stockSymbolIndex, side, listToDelete[i]->price);
        free(listToDelete[i]);
        //GarbageCollection[__sync_fetch_and_add(&CurrentGarbageQueueIndex, 1) % GARBAGE_COLLECTION_SIZE] = listToDelete[i];
      }
      break;
  }
  
  // Update MaxBid, MinAsk
  if (side == BID_SIDE) // Update MaxBid
  {
    if (flt(price, BestQuote[stockSymbolIndex][BID_SIDE].sharePrice))
    {
      UpdateMaxBid(stockSymbolIndex);
    }
  }
  else  // Update MinAsk
  {
    if (fgt(price, BestQuote[stockSymbolIndex][ASK_SIDE].sharePrice))
    {
      UpdateMinAsk(stockSymbolIndex);
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  InsertQuote
- Input:      
- Output:     
- Return:     
- Description:    1. Use hash map to find the quote-list
                  2. If not available, create the quote-list + insert hash map
                  3. Add quote to begin of the quote-list
                  4. Update total volume of quote-list
                  5. Update price point list if step 2 is executed
- Usage:      
****************************************************************************/
t_QuoteNode *InsertQuote(const int ecnId, const int side, const int stockSymbolIndex, const t_OrderDetail *info)
{
  double searchKey = info->sharePrice;
  
  t_QuoteNode *newQuote;
  t_PriceHashingItem *newPriceNode = NULL;
  t_QuoteList *quoteList = FindPricePointFromHashMapForInsertion(ecnId, stockSymbolIndex, side, searchKey, &newPriceNode);
  
  if (quoteList == NULL)  // This price point is not available in the list, we will add it
  {
    if (newPriceNode == NULL) return NULL;
    
    quoteList = calloc(1, sizeof(t_QuoteList));     if (quoteList == NULL) { TraceLog(FATAL_LEVEL, "Calling calloc() failed\n"); return NULL; }
    newQuote = calloc(1, sizeof(t_QuoteNode));      if (newQuote == NULL) { TraceLog(FATAL_LEVEL, "Calling calloc() failed\n"); return NULL; }
    newQuote->info = *info;
    newQuote->myList = quoteList;
    
    quoteList->totalVolume = info->shareVolume;
    quoteList->price = searchKey;
    quoteList->head = newQuote;
    
    /** Add to HashMap **/
    newPriceNode->price = searchKey;
    newPriceNode->list = quoteList;
    
    /** Update the master List **/
    if (StockBook[stockSymbolIndex][ecnId][side] == NULL)
    {
      // The master list is currently empty
      StockBook[stockSymbolIndex][ecnId][side] = quoteList;
    }
    else
    {
      // Look for position to insert the quote-list into master list (list of price points)
      t_QuoteList *cur = StockBook[stockSymbolIndex][ecnId][side];
      
      if (side == BID_SIDE)
      {
        while (cur != NULL)
        {
          if (fge(searchKey, cur->price))
          {
            quoteList->prev = cur->prev;
            quoteList->next = cur;
            
            if (cur->prev == NULL)
              StockBook[stockSymbolIndex][ecnId][side] = quoteList; // Newcomer is inserted at begining for the list
            else
              cur->prev->next = quoteList;  // Newcomer is inserted in the middle of the list
            
            cur->prev = quoteList;
            break;
          }
          else
          {
            if (cur->next == NULL)
            {
              // Newcomer is inserted at the end of the list (after current position)
              quoteList->prev = cur;
              cur->next = quoteList;
              break;
            }
            else
            {
              cur = cur->next;
            }
          }
        }
      }
      else  // ask side
      {
        while (cur != NULL)
        {
          if (fle(searchKey, cur->price))
          {
            quoteList->prev = cur->prev;
            quoteList->next = cur;
            
            if (cur->prev == NULL)
              StockBook[stockSymbolIndex][ecnId][side] = quoteList; // Newcomer is inserted at begining for the list
            else
              cur->prev->next = quoteList;  // Newcomer is inserted in the middle of the list
            
            cur->prev = quoteList;
            break;
          }
          else
          {
            if (cur->next == NULL)
            {
              // Newcomer is inserted at the end of the list
              quoteList->prev = cur;
              cur->next = quoteList;
              break;
            }
            else
            {
              cur = cur->next;
            }
          }
        }
      }
    }
  }
  else
  {
    //This price point is currently in the list. We will add the quote at begin of the list
    newQuote = calloc(1, sizeof(t_QuoteNode));      if (newQuote == NULL) { TraceLog(FATAL_LEVEL, "Calling calloc() failed\n"); return NULL; }
    newQuote->next = quoteList->head;
    newQuote->next->prev = newQuote;
    newQuote->info = *info;
    newQuote->myList = quoteList;
    
    quoteList->head = newQuote;
    quoteList->totalVolume += info->shareVolume;
  }
  
  // Update MaxBid, MinAsk
  if (side == BID_SIDE)
  {
    if (fge(searchKey, BestQuote[stockSymbolIndex][BID_SIDE].sharePrice))
    {
      UpdateMaxBid(stockSymbolIndex);
    }
  }
  else
  {
    if ( fle(searchKey, BestQuote[stockSymbolIndex][ASK_SIDE].sharePrice) || (BestQuote[stockSymbolIndex][ASK_SIDE].sharePrice < 0.0001) )
    {
      UpdateMinAsk(stockSymbolIndex);
    }
  }
  
  return newQuote;
}

int ModifyQuote(t_QuoteNode *node, const int newShareVolume, const int side, const int stockSymbolIndex)
{
  if(node == DeletedQuotePlaceholder) {
    // this is a place holder, don't actually modify anything       
    return SUCCESS;
  }
  
  node->myList->totalVolume += (newShareVolume - node->info.shareVolume);
  node->info.shareVolume = newShareVolume;
  
  if (side == BID_SIDE)
  {
    if (feq(node->info.sharePrice, BestQuote[stockSymbolIndex][BID_SIDE].sharePrice))
    {
      UpdateMaxBid(stockSymbolIndex);
    }
  }
  else
  {
    if (feq(node->info.sharePrice, BestQuote[stockSymbolIndex][ASK_SIDE].sharePrice))
    {
      UpdateMinAsk(stockSymbolIndex);
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  DeleteAllExpiredArcaOrders
- Input:          stock symbol index
                  valid trade sessions
- Description:    Delete all invalid (expired) orders from ARCA Stock Book
- Usage:          Call this function when receive Trading Session Change msg
                  from ARCA
****************************************************************************/
int DeleteAllExpiredArcaOrders(const int stockSymbolIndex, const unsigned char validSession, const int sideToDelete)
{
  if (StockBook[stockSymbolIndex][BOOK_ARCA][sideToDelete] == NULL)
  {
    return SUCCESS;
  }

  // Store nodes need to be deleted
  t_QuoteNode *nodeToDelete[100000];
  
  t_QuoteList *quoteList = StockBook[stockSymbolIndex][BOOK_ARCA][sideToDelete];
  int count = 0, i;
  int needMoreDelete = 0;
  
  while (quoteList)
  {
    t_QuoteList *nextList = quoteList->next;
    t_QuoteNode *cur = quoteList->head;
    t_QuoteNode *nextQuote;
    while (cur)
    {
      nextQuote = cur->next;
      if ((cur->info.tradeSession & validSession) == 0) //Delete this expired order
      {
        if (cur->prev == cur->next)
        {
          // The quote-list has only one node, we will delete the list
          // also delete the hash map, as well as update the master list
          
          /** Update master list **/
          if (quoteList->prev)
            quoteList->prev->next = quoteList->next;
          else
            StockBook[stockSymbolIndex][BOOK_ARCA][sideToDelete] = quoteList->next;
            
          if (quoteList->next)
            quoteList->next->prev = quoteList->prev;
          
          /** Remove from hash map **/
          DeletePricePointFromHashMap(BOOK_ARCA, stockSymbolIndex, sideToDelete, cur->info.sharePrice);
          
          /** Free memory **/
          free(quoteList);
          //GarbageCollection[__sync_fetch_and_add(&CurrentGarbageQueueIndex, 1) % GARBAGE_COLLECTION_SIZE] = quoteList;
          nodeToDelete[count++] = cur;

          if (count > 99999)
          {
            needMoreDelete = 1;
            break;
          }
        }
        else
        {
          /** Delete the quote in a list that has more than one quote**/
          quoteList->totalVolume -= cur->info.shareVolume;
          
          if (cur->prev)
            cur->prev->next = cur->next;
          else
            quoteList->head = cur->next;  //delete at beginning of the list
          
          if (cur->next)
            cur->next->prev = cur->prev;
          
          nodeToDelete[count++] = cur;
          
          if (count > 99999)
          {
            needMoreDelete = 1;
            break;
          }
        }
      }
      
      cur = nextQuote;
    }
    
    if (count > 99999)
    {
      break;
    }
    
    quoteList = nextList;
  }

  int updateFlag = -1;
  if (count > 0)
  {
    if (sideToDelete == BID_SIDE) // Update MaxBid
    {
      if (fle(nodeToDelete[0]->info.sharePrice, BestQuote[stockSymbolIndex][BID_SIDE].sharePrice))
      {
        updateFlag = 1;
      }
    }
    else  // Update MinAsk
    {
      if (fge(nodeToDelete[0]->info.sharePrice, BestQuote[stockSymbolIndex][ASK_SIDE].sharePrice))
      {
        updateFlag = 2;
      }
    }
  } 

  // Delete quotes
  for (i = 0; i < count; i++)
  {                     
        OrderMapRemove(ARCA_OrderMap[_consumerId],nodeToDelete[i]->info.refNum);      
    free(nodeToDelete[i]);
    //GarbageCollection[__sync_fetch_and_add(&CurrentGarbageQueueIndex, 1) % GARBAGE_COLLECTION_SIZE] = nodeToDelete[i];
  }
  
  if (updateFlag == 1)
    UpdateMaxBid(stockSymbolIndex);
  else if (updateFlag == 2)
    UpdateMinAsk(stockSymbolIndex);
  
  if (needMoreDelete == 1) return ERROR;
  
  return SUCCESS;
}

uint32_t OrderMapSize(uint32_t initial_size) {
  uint32_t bit = 16;
  uint32_t size = 1 << bit;
  while (size < initial_size) {
    ++bit;
    size = 1 << bit;
  }
  return size;
}         

void OrderMapInit(t_OrderMap* hash, uint32_t initial_size) {
  uint32_t bit = 1;
  uint32_t size = 1 << bit;
  while (size < initial_size) {
    ++bit;
    size = 1 << bit;
  }
  hash->bucket_bit = bit;
  hash->bucket_max = size;
  hash->bucket_mask = size - 1;
  hash->count = 0;
        
  uint32_t i;
  for (i=0; i < size; ++i) {
    hash->bucket[i].key = ~0ULL; 
    hash->bucket[i].side = 2;
    hash->bucket[i].node = NULL; 
    hash->bucket[i].count = 0;

    hash->extra_slots[i].key = ~0ULL; 
    hash->extra_slots[i].side = 2;
    hash->extra_slots[i].node = NULL;
  }
}

// Assumes that duplicate keys won't happen.       
void OrderMapInsert(t_OrderMap* hash, uint64_t key, t_QuoteNode *node, uint8_t side) {
  uint32_t idx = ((uint32_t)key) & hash->bucket_mask;
  ++(hash->count);
  if (hash->bucket[idx].node == NULL) {
    hash->bucket[idx].key = key;
    hash->bucket[idx].side = side;
    hash->bucket[idx].node = node;
    return;
  }
  t_OrderMapEntry* const extra = hash->extra_slots;
  
  uint64_t oldKey = hash->bucket[idx].key;
  uint8_t oldSide = hash->bucket[idx].side;
  t_QuoteNode *oldNode = hash->bucket[idx].node;
  
  hash->bucket[idx].key = key;
  hash->bucket[idx].side = side;
  hash->bucket[idx].node = node;
  ++(hash->bucket[idx].count);
  
  while(extra[idx].node != NULL)
    idx = (idx + 1) & hash->bucket_mask;
  
  extra[idx].key = oldKey;
  extra[idx].side = oldSide;
  extra[idx].node = oldNode;
}

t_QuoteNodeInfo OrderMapFind(t_OrderMap* hash, uint64_t key) {   
  uint32_t idx = ((uint32_t)key) & hash->bucket_mask;
  t_QuoteNodeInfo ret;
  
  if (hash->bucket[idx].key == key) {  
    ret.node = hash->bucket[idx].node;
    ret.side = hash->bucket[idx].side;
    return ret;
  }
  
  t_OrderMapEntry* const extra = hash->extra_slots;
  if (hash->bucket[idx].count) {
    while (extra[idx].node != NULL) {
      if (extra[idx].key == key) { 
        ret.side = extra[idx].side; 
        ret.node = extra[idx].node;
        return ret;
      }
      idx = (idx + 1) & hash->bucket_mask;
    }
  }                
  
  ret.node = NULL;
  return ret;
}

t_QuoteNodeInfo OrderMapRemove(t_OrderMap* hash, uint64_t key)
{   
  uint32_t idx = ((uint32_t)key) & hash->bucket_mask;
    
  t_QuoteNodeInfo ret; 
  ret.node = NULL;
  
  if (hash->bucket[idx].key == key) {  
    ret.side = hash->bucket[idx].side;
    ret.node = hash->bucket[idx].node;   
    hash->bucket[idx].node = NULL;
    hash->bucket[idx].key = ~0ULL;
    hash->bucket[idx].side = 2;
    return ret;
  }
  
  t_OrderMapEntry* const extra = hash->extra_slots;
  if (hash->bucket[idx].count) {
    uint32_t i = idx;
    while (extra[i].node != NULL) {
      if (extra[i].key == key) {
        --(hash->bucket[idx].count);
        ret.side = extra[i].side;
        ret.node = extra[i].node;
        uint32_t j,r;
        //This is Knuth's algorithm to remove, from TAOCP V3 2nd edition pg 533-534
        while(1) {
          extra[i].node = NULL;
          extra[i].key = ~0ULL;
          extra[i].side = 2;
          j = i;
          do {
            i = (i+1) & hash->bucket_mask;
            if (extra[i].node == NULL) {
              return ret;
            }
            r = (extra[i].key) & hash->bucket_mask;
          } while((i >= r && r > j) || (r > j && j > i) || (j > i && i >= r));  //43574525639
          extra[j].node = extra[i].node;
          extra[j].side = extra[i].side;
          extra[j].key = extra[i].key;
        }
      }
      i = (i + 1) & hash->bucket_mask;
    }
  }   
                                                              
  ret.node = NULL;
  return ret;
}

/****************************************************************************
- Function name:  CheckBookCrossToEnableSymbol
****************************************************************************/
inline void CheckBookCrossToEnableSymbol(const int stockSymbolIndex)
{         
  // see if the book in uncrossed yet
  if (SymbolMgmt.symbolList[stockSymbolIndex].isTempDisabled == 1)
  {
    // Check to see if book is still crossing, in order to enable trading on this symbol
    t_TopOrder ask, bid;
    GetBestOrder(&ask, ASK_SIDE, stockSymbolIndex);
    GetBestOrder(&bid, BID_SIDE, stockSymbolIndex);
    
    if ((ask.ecnIndicator == -1) || (bid.ecnIndicator == -1))
    {
      // either bid/ask not available, so book is not crossing
      SymbolMgmt.symbolList[stockSymbolIndex].isTempDisabled = 0;
      //TraceLog(DEBUG_LEVEL, "Symbol %.8s is unlocked for trade contention on consumer: %d (no book on ask or bid)\n", SymbolMgmt.symbolList[stockSymbolIndex].symbol, _consumerId);
    }
    else if (fgt(ask.sharePrice, bid.sharePrice))
    {
      // Book is not crossing
      SymbolMgmt.symbolList[stockSymbolIndex].isTempDisabled = 0;
      //TraceLog(DEBUG_LEVEL, "Symbol %.8s is unlocked for trade contention on consumer: %d (cross cleared) ask: %d @ %f on ecn: %d, bid: %d @ %f on ecn: %d\n", SymbolMgmt.symbolList[stockSymbolIndex].symbol, _consumerId, ask.shareVolume, ask.sharePrice, ask.ecnIndicator, bid.shareVolume, bid.sharePrice, bid.ecnIndicator);
    }
  }

  return;
}
