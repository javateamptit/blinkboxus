/****************************************************************************
** Project name:  Equity Arbitrage Application
** File name:   nyse_ccg_proc.c
** Description:   This file contains function definitions that were declared
        in nyse_ccg_proc.h

** Author:    Khoa Pham-Tan
** First created on 12 August 2012
** Last updated on 12 August 2012
****************************************************************************/

/****************************************************************************
** Include files and define several variables
****************************************************************************/
#define _ISOC9X_SOURCE

#include <unistd.h>
#include <sys/time.h>

#include "nyse_ccg_proc.h"
#include "trading_mgmt.h"
#include "kernel_algorithm.h"
#include "hbitime.h"
#include "network.h"
#include "logging.h"

/****************************************************************************
            GLOBAL VARIABLES
****************************************************************************/
static t_OecConfig *venueConfig = &OecConfig[ORDER_NYSE_CCG];
static pthread_spinlock_t *NYSE_CCG_NewOrderLock = NULL; 

static int _secs_in_years = 0;
static int _current_year = 0;
static double _CCG_PRICE_PORTION[5] = {1.0, 0.1, 0.01, 0.001, 0.0001};
static char _Nyse_CCG_Stamptime[10];

extern char NYSE_StockIndex_To_Symbol_Mapping[MAX_STOCK_SYMBOL][12];

/****************************************************************************
            LIST OF FUNCTIONS
****************************************************************************/

/****************************************************************************
- Function name:  Init_NYSE_CCG_DataStructure
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
void Init_NYSE_CCG_DataStructure()
{
  if(NYSE_CCG_NewOrderLock == NULL)
  {
    NYSE_CCG_NewOrderLock = malloc(sizeof(pthread_spinlock_t));
    pthread_spin_init(NYSE_CCG_NewOrderLock,0);
  }

  if (_current_year == 0) //avoid duplicated processing between multiple threads
  {
    time_t now;
    now = hbitime_seconds();
    Get_secs_in_years(&now, &_current_year, &_secs_in_years);

    //Stamptime
    time_t rawtime;
    struct tm * ptm;
    rawtime = hbitime_seconds();
    ptm = Lrc_gmtime(&rawtime, _current_year, _secs_in_years);

    // MM
    Lrc_itoaf(ptm->tm_mon, '0', 2, &_Nyse_CCG_Stamptime[0]);
    // DD
    Lrc_itoaf(ptm->tm_mday, '0', 2, &_Nyse_CCG_Stamptime[2]);
    // YYYY
    Lrc_itoaf(ptm->tm_year, '0', 4, &_Nyse_CCG_Stamptime[4]);

    // for safe
    _Nyse_CCG_Stamptime[8] = 0;
  }

  venueConfig->socket = -1;

  return;
}

int Connect_NYSE_CCG() 
{
  Init_NYSE_CCG_DataStructure();

  // Load incoming and outgoing sequence number from file
  if (LoadSequenceNumber(FN_SEQUENCE_NUM_NYSE, ORDER_NYSE_CCG) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "NYSE CCG: Could not load seqs file\n");
    OrderStatusMgmt[ORDER_NYSE_CCG].shouldConnect = NO;
    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Connecting to NYSE CCG Server: %s:%d (%s)...\n", venueConfig->ipAddress, venueConfig->port, venueConfig->userName);

  // Connect to NYSE_CCG Server
  venueConfig->socket = connectSession(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, venueConfig->sessionId);

  if (venueConfig->socket == -1)
  {
    TraceLog(ERROR_LEVEL, "Could not connect to NYSE CCG Server\n");
    OrderStatusMgmt[ORDER_NYSE_CCG].shouldConnect = NO;
    return ERROR;
  }

  // set time out is 65 seconds when receiving data
  if (SetSocketRecvTimeout(venueConfig->socket, NYSE_CCG_RECV_TIMEOUT) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Could not set socket receive-timeout for NYSE socket\n");
    disconnectSession(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, venueConfig->sessionId);
    OrderStatusMgmt[ORDER_NYSE_CCG].shouldConnect = NO;
    return ERROR;
  }

  if (SetSocketSndTimeout(venueConfig->socket, 1, 0) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Could not set socket send-timeout for Nyse CCG socket\n");
    disconnectSession(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, venueConfig->sessionId);
    OrderStatusMgmt[ORDER_NYSE_CCG].shouldConnect = NO;
    return ERROR;
  }
  
  /* Disable the Nagle (TCP No Delay) algorithm */
  if (DisableNagleAlgo(venueConfig->socket) == -1)
  {
    TraceLog(ERROR_LEVEL, "Could not set TCP_NODELAY to NYSE CCG socket\n");
    disconnectSession(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, venueConfig->sessionId);
    OrderStatusMgmt[ORDER_NYSE_CCG].shouldConnect = NO;
    return ERROR;
  }
  
  // Send Logon message to server
  if (BuildAndSend_NYSE_CCG_LogonRequestMsg() == ERROR)
  {
    TraceLog(ERROR_LEVEL, "NYSE CCG: Could not send logon request message to server\n");
    OrderStatusMgmt[ORDER_NYSE_CCG].shouldConnect = NO;
    return ERROR;
  }
  
  return SUCCESS;  
}

int BuildAndSend_NYSE_CCG_TestRequestMsg(void)
{
  t_ShortConverter shortUnion;
  char message[8];
  memset(message, 0, 8);

  // MessageType: 2 bytes
  shortUnion.value = 0x0011;
  message[0] = shortUnion.c[1]; 
  message[1] = shortUnion.c[0]; 

  // MsgLength: 2 bytes;
  shortUnion.value = 8;
  message[2] = shortUnion.c[1];
  message[3] = shortUnion.c[0];

  // Sequence: 4 bytes --> IGNORE

  return SocketSend(venueConfig->socket, message, 8);
}

int Process_NYSE_CCG_DataBlock(uint32_t symbolId, t_DataBlock *dataBlock) {
  // Get message type
  t_ShortConverter msgType;
  msgType.c[0] = dataBlock->msgContent[1];
  msgType.c[1] = dataBlock->msgContent[0];

  switch(msgType.value)
  {
    case NYSE_ORDER_ACK_TYPE: // Order has been booked
      __sync_add_and_fetch(&venueConfig->incomingSeqNum, 1);
      ProcessNYSE_CCG_OrderAckMsg(symbolId, dataBlock);
      break;

    case NYSE_ORDER_FILLED_TYPE: // Order has been filled
      __sync_add_and_fetch(&venueConfig->incomingSeqNum, 1);
      ProcessNYSE_CCG_OrderFilledMsg(symbolId, dataBlock);
      break;

    case NYSE_ORDER_REJECT_TYPE: // New-Order/Cancel-Order has been rejected
      __sync_add_and_fetch(&venueConfig->incomingSeqNum, 1);
      ProcessNYSE_CCG_OrderRejectedMsg(symbolId, dataBlock);
      break;

    case NYSE_UROUT_TYPE: // New-Order/Cancel-Order confirmation
      __sync_add_and_fetch(&venueConfig->incomingSeqNum, 1);
      ProcessNYSE_CCG_OrderCanceledMsg(symbolId, dataBlock);
      break;

    case NYSE_CANCEL_REQUEST_ACK_TYPE: // Cancel Request Ack Message
      __sync_add_and_fetch(&venueConfig->incomingSeqNum, 1);
      Process_NYSE_CCG_OrderCancelAckMsg(symbolId, dataBlock);
      break;

    case NYSE_TEST_REQUEST_TYPE:
      if (BuildAndSend_NYSE_CCG_HeartbeatMsg() == ERROR)
      {
        return ERROR;
      }
      break;

    case NYSE_HEART_BEAT_TYPE:  //Must response with a heartbeat msg within 60 seconds
      /*if (BuildAndSend_NYSE_CCG_HeartbeatMsg() == ERROR)
      {
        return;
      }*/
      break;

    case NYSE_LOGON_TYPE: // Logon Accepted Message
      TraceLog(DEBUG_LEVEL,  "NYSE CCG: Received logon accepted from Server\n");
      ProcessNYSE_CCG_LogonAcceptedMsg(dataBlock);
      OrderStatusMgmt[ORDER_NYSE_CCG].isConnected = CONNECTED;
      TraceLog(DEBUG_LEVEL, "NYSE CCG: incomingSeqNum = %d\n", venueConfig->incomingSeqNum);
      TraceLog(DEBUG_LEVEL, "NYSE CCG: outgoingSeqNum = %d\n", venueConfig->outgoingSeqNum);
      break;

    case NYSE_LOGON_REJECT_TYPE: // Logon Reject Message
      TraceLog(DEBUG_LEVEL, "NYSE CCG: Received logon rejected from Server\n");
      TraceLog(DEBUG_LEVEL, "\t Reject Type: %X\n", (short)__bswap16(*(unsigned short *)&dataBlock->msgContent[16]));
      TraceLog(DEBUG_LEVEL, "\t Reject Text: '%s'\n", &dataBlock->msgContent[18]);
      return ERROR;

    default:
      TraceLog(WARN_LEVEL, "NYSE CCG: Unknown Message type = %X\n", msgType.value);
      break;
  }  
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  Send_NYSE_CCG_HeartBeat_Thread
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
void *Send_NYSE_CCG_HeartBeat_Thread(void *arg)
{
  SetKernelAlgorithm ("[NYSE_CCG_SEND_HEARTBEAT]");

  TraceLog(DEBUG_LEVEL, "NYSE CCG: Start thread to auto send HeartBeat message ...\n");
  unsigned int countTimeInSencond = 0;
  while(OrderStatusMgmt[ORDER_NYSE_CCG].shouldConnect == YES)
  {
    //Send Heartbeat after 30 seconds
    if(countTimeInSencond % NYSE_CCG_HEARTBEAT_IN_SECOND == 0)
    {
      //if (BuildAndSend_NYSE_CCG_HeartbeatMsg() == ERROR)
      if (BuildAndSend_NYSE_CCG_TestRequestMsg() == ERROR)
      {
        TraceLog(ERROR_LEVEL, "NYSE CCG: Killed thread send HeartBeat due to error send HeartBeat\n");
        return NULL;
      }

      //TraceLog(DEBUG_LEVEL, "NYSE CCG: Sent HeartBeat message successful\n");
    }

    countTimeInSencond++;
    sleep(1);
  }

  TraceLog(DEBUG_LEVEL, "NYSE CCG: Killed thread send HeartBeat due to disconnect from AS\n");

  return NULL;
}

/****************************************************************************
- Function name:  BuildAndSend_NYSE_CCG_LogonRequestMsg
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildAndSend_NYSE_CCG_LogonRequestMsg(void)
{
  t_IntConverter integerUnion;
  t_ShortConverter shortUnion;

  char message[NYSE_LOGON_MSG_LEN];
  memset(message, 0, NYSE_LOGON_MSG_LEN);

  // Message Type: 2 bytes(0-1)
  shortUnion.value = NYSE_LOGON_TYPE;
  message[0] = shortUnion.c[1] ;
  message[1] = shortUnion.c[0];

  // Message Length: 2 bytes(2-3)
  shortUnion.value = NYSE_LOGON_MSG_LEN;
  message[2] = shortUnion.c[1] ;
  message[3] = shortUnion.c[0];

  // Message Sequence Number: 4 bytes(4-7) -> Not use

  // LastMsgSeqNumReceived should start with 0: 4 bytes(8-11)
  integerUnion.value = venueConfig->incomingSeqNum;
  message[8] = integerUnion.c[3];
  message[9] = integerUnion.c[2];
  message[10] = integerUnion.c[1];
  message[11] = integerUnion.c[0];

  // SenderCompID: 12 bytes(12 - 23)
  strncpy(&message[12], venueConfig->userName, 12);

  /*MessageVersionProfile : 32 bytes, 2 bytes for per type*/
  // message[24] -> message[55]
  
  // Logon Ack
  shortUnion.value = NYSE_LOGON_TYPE;
  message[24] = shortUnion.c[1];
  message[25] = shortUnion.c[0];

  // Logon Reject
  shortUnion.value = NYSE_LOGON_REJECT_TYPE;
  message[26] = shortUnion.c[1];
  message[27] = shortUnion.c[0];

  // New Order Ack
  shortUnion.value = NYSE_ORDER_ACK_TYPE;
  message[28] = shortUnion.c[1];
  message[29] = shortUnion.c[0];

  // UROUT (cancelled order)
  shortUnion.value = NYSE_UROUT_TYPE;
  message[30] = shortUnion.c[1];
  message[31] = shortUnion.c[0];

  // Order Fill (Short)
  shortUnion.value = NYSE_ORDER_FILLED_TYPE;
  message[32] = shortUnion.c[1];
  message[33] = shortUnion.c[0];

  // Order Reject
  shortUnion.value = NYSE_ORDER_REJECT_TYPE;
  message[34] = shortUnion.c[1];
  message[35] = shortUnion.c[0];

  // Cancel Request Ack
  shortUnion.value = NYSE_CANCEL_REQUEST_ACK_TYPE;
  message[36] = shortUnion.c[1];
  message[37] =  shortUnion.c[0];

  // CancelOnDisconnect: 1 byte
  message[56] = 48 + venueConfig->autoCancelOnDisconnect;  /*Convert to ASCII*/

  // Filler: 3 bytes (57-59)

  return SocketSend(venueConfig->socket, message, NYSE_LOGON_MSG_LEN);
}

/****************************************************************************
- Function name:  Convert_ClOrdID_To_NYSE_Format
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
void Convert_ClOrdID_To_NYSE_Format(int idOrder, char *strResult)
{
  /*
   * A = 0, B = 1, C = 2, D = 3, E = 4, F = 5, G = 6, H = 7, I = 8, J = 9
   * To avoid all zeros in the sequence number, we must plus 1
   * For example: idOrder = 1234567 is converted to BCD 4691
  */

  int seqNumber = idOrder % 9999;
  int branchCode = (idOrder -  seqNumber) / 9999;
  seqNumber = seqNumber + 1; // Avoid all zeros in the sequence number

  // branch code
  strResult[2] = 65 + branchCode % 10; // ASCII 'A' = 65
  branchCode = branchCode/10;
  strResult[1] = 65 + branchCode % 10;
  branchCode = branchCode/10;
  strResult[0] = 65 + branchCode;

  // space
  strResult[3] =  32; // ASCII ' ' = 32

  // sequence number
  strResult[7] = 48 + seqNumber % 10; // // ASCII '0' = 48
  seqNumber = seqNumber/10;
  strResult[6] = 48 + seqNumber % 10;
  seqNumber = seqNumber/10;
  strResult[5] = 48 + seqNumber % 10;
  seqNumber = seqNumber/10;
  strResult[4] = 48 + seqNumber;

  // add character '/'
  strResult[8] = 47;

  // add Stamptime
  strncpy(&strResult[9], _Nyse_CCG_Stamptime, 8);

}

/****************************************************************************
- Function name:  Convert_NYSECCG_Format_To_ClOrdID
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int Convert_NYSECCG_Format_To_ClOrdID(char *valueFiled)
{
  int branhCode = 0, seqNumber = 0;
  //We known 8 characters in advance

  // branch code
  branhCode = (valueFiled[0] - 65)* 100;
  branhCode += (valueFiled[1] - 65)* 10;
  branhCode += valueFiled[2] - 65;

  // skip space: valueFiled[3]

  // sequence number
  seqNumber += (valueFiled[4] - 48)* 1000;
  seqNumber += (valueFiled[5] - 48)* 100;
  seqNumber += (valueFiled[6] - 48)* 10;
  seqNumber += (valueFiled[7] - 48);

  // we must minus 1 because we have plus 1 in clientOrderID of Convert_ClOrdID_To_NYSE_Format
  seqNumber -= 1;

  return (branhCode * 9999 + seqNumber);
}

/****************************************************************************
- Function name:  BuildAndSend_NYSE_CCG_OrderMsg
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildAndSend_NYSE_CCG_OrderMsg(t_OrderPlacementInfo *newOrder)
{
  t_IntConverter integerUnion;
  t_ShortConverter shortUnion;
  
  char *msgContent = newOrder->dataBlock.msgContent;

  //Fill null-characters for new order
  memset(newOrder->dataBlock.msgContent, 0, NYSE_NEW_ORDER_MSG_LEN);

  //MessageType: 2 bytes(0-1)
  shortUnion.value = NYSE_NEW_ORDER_TYPE;
  msgContent[0] = shortUnion.c[1];
  msgContent[1] = shortUnion.c[0];

  //MsgLength: 2 bytes(2-3)
  shortUnion.value = NYSE_NEW_ORDER_MSG_LEN;
  msgContent[2] = shortUnion.c[1];
  msgContent[3] = shortUnion.c[0];

  // MsgSeqNum: 4 bytes(4-7)  (this will be set later, right before the order goes out the door)

  // Order Quantity: 4 bytes(8-11)
  integerUnion.value = newOrder->shareVolume;
  msgContent[8] = integerUnion.c[3];
  msgContent[9] =  integerUnion.c[2];
  msgContent[10] = integerUnion.c[1];
  msgContent[11] = integerUnion.c[0];

  /*
   * MaxFloorQty: 4 bytes(12-15)
   * Currently, EAA will trade with TIF = IOC, so we NOT use MaxFloorQty
  */

  // Price: 4 bytes(16-19)
  if (newOrder->side == '1')  //BUY SIDE
  {
    integerUnion.value = (int)(newOrder->sharePrice * 100.00 + 0.500001);
  }
  else
  {
    integerUnion.value = (int)(newOrder->sharePrice * 100.00 + 0.000001);
  }
  msgContent[16] = integerUnion.c[3];
  msgContent[17] = integerUnion.c[2];
  msgContent[18] = integerUnion.c[1];
  msgContent[19] = integerUnion.c[0];

  // PriceScale: 1 byte
  msgContent[20] = NYSE_PRICE_SCALE;

  // Symbol: 11 bytes(21 - 31)
  strncpy(&msgContent[21], NYSE_StockIndex_To_Symbol_Mapping[newOrder->stockSymbolIndex], 11);

  // ExecInst: 1 byte --> Not use
  // msgContent[32]

  //Side : 1 byte
  msgContent[33] = newOrder->side;

  // OrderType: 1 byte
  msgContent[34] = NYSE_ORDER_TYPE_LIMIT;

  // TimeInForce: 1 byte
  msgContent[35] = '3'; //IOC

  // Rule80A: 1 byte
  msgContent[36] = NYSE_RULE80A;

  // RoutingInstruction: 1 byte
  if (newOrder->ISO_Flag == YES)
  {
    msgContent[37] = 'I';
  }
  else
  {
    if (newOrder->isRoutable == YES)
    {
      msgContent[37] = 0;
    }
    else
    {
      msgContent[37] = 'S';
    }    
  }

  /*
   * DOTReserve: 1 byte
   * set DOTReserve value to 'N' if Maxfloor is not used, otherwise set to 'Y'
   * currently, EAA will trade without Maxfloor, so We set DOTReserve value to 'N'
  */
  msgContent[38] = DOTRESERVE;

  // OnBehalfOfCompID: 5 bytes
  strncpy(&msgContent[39], MPID_NYSE_CCG, 5);

  // SenderSubID: 5 bytes(44-48) --> Not use

  // ClearingFirm: 5 bytes(49-53) -> Not use

  //Account: 10 bytes(54-63)
  strcpy(&msgContent[54], TradingAccount.NyseCCGAccount);

  // ClientOrderID: 17 bytes(64-80)
  Convert_ClOrdID_To_NYSE_Format(newOrder->orderID, &msgContent[64]);

  // Filler: 3 bytes(81-83) --> Not use

  if (OrderBucketConsume() == ERROR)
  {
    TraceLog(WARN_LEVEL, "NYSE CCG order is not allowed: Order bucket is currently empty\n");
    return ERROR;
  }
  
  long time = Send_NYSE_CCG_SequencedMsg(msgContent, NYSE_NEW_ORDER_MSG_LEN);
  if (time > 0)
  {
    *((long*)&newOrder->dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]) = time;
    
    newOrder->ecnToPlaceID = ORDER_NYSE_CCG;
    
    /* Special notice for NYSE:
      We will save the original symbol to data at special offet, so that AS will take this original symbol to update data structure
      Special offset: In dataBlock.msgContent, right after dataBlock.msgLen, size: SYMBOL_LEN
    */
    
    memcpy(&newOrder->dataBlock.msgContent[NYSE_NEW_ORDER_MSG_LEN], newOrder->symbol, SYMBOL_LEN);
    
    DEVLOGF("Sent %c order to NYSE with id: %d for %s, %d @ $%lf\n", newOrder->side, newOrder->orderID, newOrder->symbol, newOrder->shareVolume, newOrder->sharePrice);
    
    return SUCCESS;
  }
  else
  {
    return ERROR;
  }
}

/****************************************************************************
- Function name:  BuildAndSend_NYSE_CCG_HeartbeatMsg
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildAndSend_NYSE_CCG_HeartbeatMsg(void)
{
  t_ShortConverter shortUnion;
  char message[NYSE_HEART_BEAT_MSG_LEN];
  memset(message, 0, NYSE_HEART_BEAT_MSG_LEN);

  // MessageType: 2 bytes
  shortUnion.value = NYSE_HEART_BEAT_TYPE;
  message[0] = shortUnion.c[1]; 
  message[1] = shortUnion.c[0]; 

  // MsgLength: 2 bytes;
  shortUnion.value = NYSE_HEART_BEAT_MSG_LEN;
  message[2] = shortUnion.c[1];
  message[3] = shortUnion.c[0];

  // Sequence: 4 bytes --> IGNORE

  return SocketSend(venueConfig->socket, message, NYSE_HEART_BEAT_MSG_LEN);
}

/****************************************************************************
- Function name:  Add_NYSE_CCG_DataBlockToCollection
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
void Add_NYSE_CCG_DataBlockToCollection(t_DataBlock *dataBlock)
{
  dataBlock->blockLen = 12 + DATABLOCK_ADDITIONAL_INFO_LEN + dataBlock->msgLen;
  AddDataBlockToQueue(ORDER_NYSE_CCG, dataBlock);
}

/****************************************************************************
- Function name:  ProcessNYSE_CCG_OrderAckMsg
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessNYSE_CCG_OrderAckMsg(uint32_t symbolId, t_DataBlock *dataBlock)
{
  Add_NYSE_CCG_DataBlockToCollection(dataBlock);

  DEVLOGF("Received Order Accepted from NYSE\n");
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessNYSE_CCG_OrderFilledMsg
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessNYSE_CCG_OrderFilledMsg(uint32_t symbolId, t_DataBlock *dataBlock)
{
  t_ExecutionReport status;

  status.ecnID = ORDER_NYSE_CCG;
  status.stockSymbolIndex = symbolId;
  status.clientOrderID = Convert_NYSECCG_Format_To_ClOrdID( &(dataBlock->msgContent[99]));
  status.filledShare = __builtin_bswap32(*(unsigned int*)(&(dataBlock->msgContent[20])));
  
  int tempPriceNumerator = __builtin_bswap32(*(unsigned int*)(&(dataBlock->msgContent[24])));
  int tempPriceScale = dataBlock->msgContent[28] - 48;

  status.filledPrice = tempPriceNumerator * _CCG_PRICE_PORTION[tempPriceScale];

  // Arrival Time
  memcpy(&status.time, &dataBlock->addContent[DATABLOCK_OFFSET_FILTER_TIME], 8);
  
  ProcessFillResponse(&status);
  
  *(unsigned short *)&dataBlock->addContent[DATABLOCK_OFFSET_ENTRY_EXIT_REF] = status.entryExitRefNum;
  
#ifdef DEVLOG_ENABLE
  // Change LastMarket for testing only
  if (SymbolMgmt.symbolList[symbolId].mdcId == BOOK_NYSE) dataBlock->msgContent[31] = 'N';
  else dataBlock->msgContent[31] = 'A';
#endif

  // Add data block to collection
  Add_NYSE_CCG_DataBlockToCollection(dataBlock);
  
  DEVLOGF("Received Fill from NYSE, %d shares at $%lf\n", status.filledShare, status.filledPrice);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessNYSE_CCG_OrderRejectedMsg
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessNYSE_CCG_OrderRejectedMsg(uint32_t symbolId, t_DataBlock *dataBlock)
{
  // RejectMsgType
  char rejectMsgType = dataBlock->msgContent[18];

  /*
   * '1'= Order Reject
   * '2'= Cancel Reject
   * '3'= Cancel Replace Reject
  */

  if (rejectMsgType == '2') // Cancel Reject
  {
    // Add data block to collection
    Add_NYSE_CCG_DataBlockToCollection(dataBlock);

    return SUCCESS;
  }
  
  t_ExecutionReport status;

  status.ecnID = ORDER_NYSE_CCG;
  status.stockSymbolIndex = symbolId;
  status.clientOrderID = Convert_NYSECCG_Format_To_ClOrdID( &(dataBlock->msgContent[39]) );
    
  // Arrival Time
  memcpy(&status.time, &dataBlock->addContent[DATABLOCK_OFFSET_FILTER_TIME], 8);
  
  ProcessRejectResponse(&status);
  
  *(unsigned short *)&dataBlock->addContent[DATABLOCK_OFFSET_ENTRY_EXIT_REF] = status.entryExitRefNum;
  
  // Add data block to collection
  Add_NYSE_CCG_DataBlockToCollection(dataBlock);

  DEVLOGF("Received Reject from NYSE, reason: %s\n", &dataBlock->msgContent[73]);

  return SUCCESS;

}

/****************************************************************************
- Function name:  ProcessNYSE_CCG_OrderCanceledMsg
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessNYSE_CCG_OrderCanceledMsg(uint32_t symbolId, t_DataBlock *dataBlock)
{
  t_ExecutionReport status;

  status.ecnID = ORDER_NYSE_CCG;
  status.stockSymbolIndex = symbolId;
  
  //OrigClientOrderID
  status.clientOrderID = Convert_NYSECCG_Format_To_ClOrdID( &(dataBlock->msgContent[37]) );
  
  // Arrival Time
  memcpy(&status.time, &dataBlock->addContent[DATABLOCK_OFFSET_FILTER_TIME], 8);
  
  status.canceledShare = TradingCollection[symbolId].openOrders[status.clientOrderID % MAX_OPEN_ORDERS].shareVolume;
  
  ProcessCancelResponse(&status);
  
  *(unsigned short *)&dataBlock->addContent[DATABLOCK_OFFSET_ENTRY_EXIT_REF] = status.entryExitRefNum;
  
  // Add data block to collection
  Add_NYSE_CCG_DataBlockToCollection(dataBlock);

  DEVLOGF("Received Order Cancelled from NYSE for order id: %d, cancelled shares: %d\n", status.clientOrderID, status.canceledShare);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessNYSE_CCG_LogonAcceptedMsg
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int ProcessNYSE_CCG_LogonAcceptedMsg(t_DataBlock *dataBlock)
{
  TraceLog(DEBUG_LEVEL, "Logon to NYSE CCG was successful\n");

  /*
  Update Outgoing Sequence Number before send any messages to NYSE CCG Server
  */
  venueConfig->outgoingSeqNum = __builtin_bswap32(*((unsigned int*)(&dataBlock->msgContent[NYSE_OUTGOING_SEQ_NUM_INDEX])));

  return SUCCESS;
}

/****************************************************************************
- Function name:  Send_NYSE_CCG_Msg
****************************************************************************/
long Send_NYSE_CCG_SequencedMsg(char *message, int msgLen)
{
  pthread_spin_lock(NYSE_CCG_NewOrderLock);
    *((int*)&message[4]) = __builtin_bswap32(venueConfig->outgoingSeqNum++);
    long time = AsyncSend(venueConfig->sessionId, message, msgLen);
  pthread_spin_unlock(NYSE_CCG_NewOrderLock);
  return time;
}

/****************************************************************************
- Function name:  BuildAndSend_NYSE_CCG_CancelMsg
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int BuildAndSend_NYSE_CCG_CancelMsg(const t_NYSE_CCG_CancelOrder *cancelOrder)
{
  t_ShortConverter shortUnion;

  char message[NYSE_ORDER_CANCEL_MSG_LEN + SYMBOL_LEN];
  memset(message, 0, NYSE_ORDER_CANCEL_MSG_LEN + SYMBOL_LEN);

  // message type: 2 bytes(0-1)
  shortUnion.value = NYSE_CANCEL_ORDER_TYPE;
  message[0] = shortUnion.c[1];
  message[1] = shortUnion.c[0];

  // message length: 2 bytes(2-3)
  shortUnion.value = NYSE_ORDER_CANCEL_MSG_LEN;
  message[2] = shortUnion.c[1];
  message[3] = shortUnion.c[0];

  //  MsgSeqNum: 4 bytes(4-7)   (this will be set right before the order is sent)

  //  MEOrderID: 4 bytes(8-11) -> Not use
  //  integerUnion.value = cancelOrder->MEOrderID;
  //  message[8] = integerUnion.c[3];
  //  message[9] = integerUnion.c[2];
  //  message[10] = integerUnion.c[1];
  //  message[11] = integerUnion.c[0];

  //OriginalOrderQty: 4 bytes(12 - 15)-> Not use
  //CancelQty: 4 bytes( 16 - 19)-> Not use
  //LeavesQty: 4bytes (20 - 23)-> Not use

  // Symbol: 11 bytes( 24 - 34)
  int stockSymbolIndex = GetStockSymbolIndex(cancelOrder->symbol);
  strncpy(&message[24], NYSE_StockIndex_To_Symbol_Mapping[stockSymbolIndex], 11);

  // Side: 1 byte
  message[35] = cancelOrder->side;

  //OnBehalfOfCompID: 5 bytes(36- 40)
  strncpy(&message[36], MPID_NYSE_CCG, 5);

  //SenderSubID:5 bytes( 41 - 45)-> Not use

  //Account: 10 bytes(46 - 55)
  strcpy(&message[46], TradingAccount.NyseCCGAccount);

  //ClientOrderID: 17 bytes (56-72)
  Convert_ClOrdID_To_NYSE_Format(cancelOrder->clOrdID, &message[56]);

  //OrigClientOrderID: 17 bytes(73-89)
  Convert_ClOrdID_To_NYSE_Format(cancelOrder->originalClientOrderID, &message[73]);

  //Filler: 2 bytes (90 - 91)-> Not use

  long time = Send_NYSE_CCG_SequencedMsg(message, NYSE_ORDER_CANCEL_MSG_LEN);
  if (time > 0)
  {
    t_DataBlock dataBlock;
    dataBlock.msgLen = NYSE_ORDER_CANCEL_MSG_LEN + SYMBOL_LEN;  //Add extra 8 bytes for original symbol, since NYSE use CMS symbology

    /* Special notice for NYSE:
      We will save the original symbol to data at special offet, so that AS will take this original symbol to update data structure
      Special offset: In dataBlock.msgContent, right after dataBlock.msgLen, size: SYMBOL_LEN
    */
    
    memcpy(&message[NYSE_ORDER_CANCEL_MSG_LEN], cancelOrder->symbol, SYMBOL_LEN);
    
    // Set timestamp for current rawdata
    *((long*)&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]) = time;
    memcpy(dataBlock.msgContent, message, NYSE_ORDER_CANCEL_MSG_LEN + SYMBOL_LEN);
    
    Add_NYSE_CCG_DataBlockToCollection(&dataBlock);

    return SUCCESS;
  }
  else
  {
    return ERROR;
  }
}

/****************************************************************************
- Function name:  Process_NYSE_CCG_OrderCancelAckMsg
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int Process_NYSE_CCG_OrderCancelAckMsg(uint32_t symbolId, t_DataBlock *dataBlock)
{
  // Set timestamp for current rawdata
  Add_NYSE_CCG_DataBlockToCollection(dataBlock);

  return SUCCESS;
}
