/****************************************************************************
** Project name:  Equity Arbitrage Application
** File name:     uqdf_proc.c
** Description:   This file contains function definitions that were declared
          in uqdf_proc.h
** Author:      Luan Vo-Kinh
** First created on May 02, 2008
** Last updated on May 02, 2008
****************************************************************************/

#define _BSD_SOURCE

#include <pthread.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>

#include "utility.h"
#include "quote_mgmt.h"
#include "stock_symbol.h"
#include "uqdf_proc.h"
#include "kernel_algorithm.h"
#include "logging.h"


static double _UQDF_PRICE_PORTION[3];
extern t_MonitorUQDF MonitorUQDF;


/****************************************************************************
- Function name:  InitUQDF_DataStructure
- Input:      
- Output:                     
- Return:     
- Description:                                    
- Usage:      
****************************************************************************/
void InitUQDF_DataStructure(void)
{ 
  TraceLog(DEBUG_LEVEL, "Initialize UQDF ...\n");

  _UQDF_PRICE_PORTION[0] = 0.01; //B
  _UQDF_PRICE_PORTION[1] = 0.001; //C
  _UQDF_PRICE_PORTION[2] = 0.0001; //D
  
  int i;
  MonitorUQDF.procCounter = 0;
  for (i = 0; i < MAX_UQDF_CHANNELS; i++)
  {
    MonitorUQDF.recvCounter[i] = 0;
    UQDF_Conf.group[i].index = i;
  
    UQDF_Conf.group[i].groupPermission = SymbolSplitInfo[AlgorithmConfig.splitConfig.symbolRangeID].uqdf[i];
  }
}

unsigned short GetUQDFMsgLen(char *buffer)
{
  unsigned short msgSize = 0;
  
  switch (buffer[0])  // Category
  {
    case 'Q':
      switch (buffer[1])
      {
        case 'E':
          msgSize = 24 + 30;
          
          //National BBO Appendage Indicator
          if ((buffer[MAX_UQDF_HEADER_LEN + 27] - 48) == 2) //Short Form National BBO Appendage Attached
            msgSize = msgSize + 22;
          else if ((buffer[MAX_UQDF_HEADER_LEN + 27] - 48) == 3) //Long Form National BBO Appendage Attached
            msgSize = msgSize + 43;
          
          //FINRA ADF MPID Appendage Indicator
          if ((buffer[MAX_UQDF_HEADER_LEN + 29] - 48) == 2) //ADF MPID(s) attached
            msgSize = msgSize + 8;
            
          break;
          
        case 'F':
          msgSize = 24 + 58;
          
          //National BBO Appendage Indicator
          if ((buffer[MAX_UQDF_HEADER_LEN + 55] - 48) == 2) //Short Form National BBO Appendage Attached
            msgSize = msgSize + 22;
          else if ((buffer[MAX_UQDF_HEADER_LEN + 55] - 48) == 3) //Long Form National BBO Appendage Attached
            msgSize = msgSize + 43;
          
          //FINRA ADF MPID Appendage Indicator
          if ((buffer[MAX_UQDF_HEADER_LEN + 57] - 48) == 2) //ADF MPID(s) attached
            msgSize = msgSize + 8;
          
          break;
        default:
          TraceLog(ERROR_LEVEL, "UQDF invalid message: category = %c, msgType = %c\n", buffer[0], buffer[1]);
          msgSize = 0;
          break;
      }
      break;
    case 'A':
      switch (buffer[1])
      {
        case 'A': // General Administrative Message
          TraceLog(ERROR_LEVEL, "UQDF: We should not receive message 'A' from category 'A'!\n");
          msgSize = 0;
          break;
        case 'B': // Issue Symbol Directory Message
          msgSize = 24 + 62;
          break;
        case 'R': // Closing Quote Recap
          msgSize = 24 + 56 + (37 * Lrc_atoi(&buffer[MAX_UQDF_HEADER_LEN + 54], 2));
          break;
        case 'H': // Cross SRO Trading Action
          msgSize = 24 + 25;
          break;
        case 'K': // Non-Regulatory Market Center Action
          msgSize = 24 + 20;
          break;
        case 'V': // Reg SHO Short Sale Price Test Restricted Indicato
          msgSize = 24 +12;
          break;
        case 'C': // Market Wide Circuit Breaker Decline Level Message
          msgSize = 24 + 46;
          break;
        case 'D': // Market Wide Circuit Breaker Status Message
          msgSize = 24 + 4;
          break;
        case 'P': // Price Band Message
          msgSize = 24 + 43;
          break;
        default:
          TraceLog(ERROR_LEVEL, "UQDF invalid message: category = %c, msgType = %c\n", buffer[0], buffer[1]);
          msgSize = 0;
          break;
      }
      break;
    case 'C':
      msgSize = 24;
      switch (buffer[1])
      {
        case 'I': // General Administrative Message
        case 'J': // Issue Symbol Directory Message
        case 'O': // Closing Quote Recap
        case 'C': // Cross SRO Trading Action
        case 'A': // Non-Regulatory Market Center Action
        case 'R': // Reg SHO Short Sale Price Test Restricted Indicato
        case 'B': // Market Wide Circuit Breaker Decline Level Message
        case 'K': // Market Wide Circuit Breaker Status Message
        case 'Z': // Price Band Message
        case 'T': // Price Band Message
        case 'L': // Price Band Message
        case 'P': // Price Band Message
          break;
        default:
          TraceLog(ERROR_LEVEL, "UQDF invalid message: category = %c, msgType = %c\n", buffer[0], buffer[1]);
          msgSize = 0;
          break;
      }
      break;
    default:  //Invalid
      TraceLog(ERROR_LEVEL, "UQDF invalid message: category = %c, msgType = %c\n", buffer[0], buffer[1]);
      msgSize = 0;
      break;
  }
  
  return msgSize;
}

int UQDF_ProcessMessage(uint32_t symbolId, char *buffer, int maxBytes, unsigned short *msgSize)
{
  t_UQDF_Msg msgTmp;
  if (maxBytes > MAX_UQDF_MSG_LEN) maxBytes = MAX_UQDF_MSG_LEN;

  // Check if this is the needed packet: Retransmission Requester is original
  if ( (buffer[4] == 32)  && 
    ((buffer[3] == 'O') || (buffer[3] == 'R')) )
  {
    // Get message header
    msgTmp.msgCategory = buffer[0];
    msgTmp.msgType = buffer[1];
    
    switch (msgTmp.msgCategory)
    {
      case 'Q': // Q: Quotation Messages
        // Q - C: UTP Participant BBO Quote Short Form (retired)
        // Q - D: UTP Participant BBO Quote Long Form (retired)
        // Q - E: UTP Participant BBO Short Form
        // Q - F: UTP Participant BBO Long Form
        switch (msgTmp.msgType)
        {
          case 'E':
            memcpy(msgTmp.msgHeader, &buffer[0], MAX_UQDF_HEADER_LEN);
            memcpy(msgTmp.msgBody, &buffer[MAX_UQDF_HEADER_LEN], maxBytes - MAX_UQDF_HEADER_LEN);
            msgTmp.msgBody[maxBytes - MAX_UQDF_HEADER_LEN] = 0;
            
            if (UQDF_StatusMgmt.isConnected != DISCONNECTED)
              ProcessUQDF_BBOShort(symbolId, &msgTmp);
            
            *msgSize = 24 + 30;
            
            //National BBO Appendage Indicator
            if ((msgTmp.msgBody[27] - 48) == 2) //Short Form National BBO Appendage Attached
              *msgSize = *msgSize + 22;
            else if ((msgTmp.msgBody[27] - 48) == 3) //Long Form National BBO Appendage Attached
              *msgSize = *msgSize + 43;
            
            //FINRA ADF MPID Appendage Indicator
            if ((msgTmp.msgBody[29] - 48) == 2) //ADF MPID(s) attached
              *msgSize = *msgSize + 8;
              
            break;
          case 'F':
            
            memcpy(msgTmp.msgHeader, &buffer[0], MAX_UQDF_HEADER_LEN);
            memcpy(msgTmp.msgBody, &buffer[MAX_UQDF_HEADER_LEN], maxBytes - MAX_UQDF_HEADER_LEN);
            msgTmp.msgBody[maxBytes - MAX_UQDF_HEADER_LEN] = 0;
            
            if (UQDF_StatusMgmt.isConnected != DISCONNECTED)
              ProcessUQDF_BBOLong(symbolId, &msgTmp);
            
            *msgSize = 24 + 58;
            
            //National BBO Appendage Indicator
            if ((msgTmp.msgBody[55] - 48) == 2) //Short Form National BBO Appendage Attached
              *msgSize = *msgSize + 22;
            else if ((msgTmp.msgBody[55] - 48) == 3) //Long Form National BBO Appendage Attached
              *msgSize = *msgSize + 43;
            
            //FINRA ADF MPID Appendage Indicator
            if ((msgTmp.msgBody[57] - 48) == 2) //ADF MPID(s) attached
              *msgSize = *msgSize + 8;
            
            break;
          default:
            TraceLog(ERROR_LEVEL, "UQDF invalid message: category = %c, msgType = %c\n", msgTmp.msgCategory, msgTmp.msgType);
            *msgSize = 0;
            return ERROR;
            break;
        }
        break;
      case 'A':
        switch (msgTmp.msgType)
        {
          case 'A': // General Administrative Message
            TraceLog(ERROR_LEVEL, "UQDF: We should not receive message 'A' from category 'A'!\n");
            *msgSize = 0;
            return ERROR;
            break;
          case 'B': // Issue Symbol Directory Message
            *msgSize = 24 + 62;
            break;
          case 'R': // Closing Quote Recap
            *msgSize = 24 + 56 + (37 * Lrc_atoi(&buffer[MAX_UQDF_HEADER_LEN + 54], 2));
            break;
          case 'H': // Cross SRO Trading Action
            *msgSize = 24 + 25;
            break;
          case 'K': // Non-Regulatory Market Center Action
            *msgSize = 24 + 20;
            break;
          case 'V': // Reg SHO Short Sale Price Test Restricted Indicator
            //memcpy(msgTmp.msgHeader, &buffer[0], MAX_UQDF_HEADER_LEN);
            memcpy(msgTmp.msgBody, &buffer[MAX_UQDF_HEADER_LEN], maxBytes - MAX_UQDF_HEADER_LEN);
            msgTmp.msgBody[maxBytes - MAX_UQDF_HEADER_LEN] = 0;
            
            if (UQDF_StatusMgmt.isConnected != DISCONNECTED)
              ProcessUQDF_RegSHOShortSalePriceTestRestrictedIndicator(symbolId, &msgTmp);
            
            *msgSize = 24 +12;
            break;
          case 'C': // Market Wide Circuit Breaker Decline Level Message
            *msgSize = 24 + 46;
            break;
          case 'D': // Market Wide Circuit Breaker Status Message
            *msgSize = 24 + 4;
            break;
          case 'P': // Price Band Message
            *msgSize = 24 + 43;
            break;
          default:
            TraceLog(ERROR_LEVEL, "UQDF invalid message: category = %c, msgType = %c\n", msgTmp.msgCategory, msgTmp.msgType);
            *msgSize = 0;
            return ERROR;
        }
        break;
      case 'C':
        *msgSize = 24;
        switch (msgTmp.msgType)
        {
          case 'I': // General Administrative Message
          case 'J': // Issue Symbol Directory Message
          case 'O': // Closing Quote Recap
          case 'C': // Cross SRO Trading Action
          case 'A': // Non-Regulatory Market Center Action
          case 'R': // Reg SHO Short Sale Price Test Restricted Indicator
          case 'B': // Market Wide Circuit Breaker Decline Level Message
          case 'K': // Market Wide Circuit Breaker Status Message
          case 'Z': // Price Band Message
          case 'T': // Price Band Message
          case 'L': // Price Band Message
          case 'P': // Price Band Message
            break;
          default:
            TraceLog(ERROR_LEVEL, "UQDF invalid message: category = %c, msgType = %c\n", msgTmp.msgCategory, msgTmp.msgType);
            *msgSize = 0;
            return ERROR;
        }
        break;
      default:  //Invalid
      {
        // Other message in other category that is not documented !
        char buffTemp[MAX_UQDF_MSG_LEN];
        memcpy(buffTemp, &buffer[0], maxBytes);
        buffTemp[maxBytes] = 0;

        TraceLog(ERROR_LEVEL, "UQDF invalid message: category = %c, msgType = %c, msg = '%s'\n", msgTmp.msgCategory, msgTmp.msgType, buffTemp);
        *msgSize = 0;
        return ERROR;
        break;
      }
    }
  }
  else
  {
    *msgSize = GetUQDFMsgLen(buffer);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessUQDF_BBOLong
- Input:      
- Output:                     
- Return:     
- Description:                                    
- Usage:      
****************************************************************************/
int ProcessUQDF_BBOLong(uint32_t symbolId, t_UQDF_Msg *msg)
{
  t_BBOQuoteInfo quoteInfo;

  msg->msgHeader[MAX_UQDF_HEADER_LEN] = 0;
  //TraceLog(DEBUG_LEVEL, "Long form trade: %s%s\n", msg->msgHeader, msg->msgBody);
  
  // PARTICIPANT ID
  quoteInfo.participantId = msg->msgHeader[13];

  // TIME STAMP
  //quoteInfo.timeStamp = (Lrc_atoi(&msg->msgHeader[14], 2) * 3600 + Lrc_atoi(&msg->msgHeader[16], 2) * 60 + Lrc_atoi(&msg->msgHeader[18], 2)) * 1000 + Lrc_atoi(&msg->msgHeader[20], 3);

  // QUOTE CONDITIONS: 1 byte
  quoteInfo.quoteCondition = msg->msgBody[13];
  
  // Bid Price Denominator: 1 byte
  int numerator = msg->msgBody[16] - 'B'; 
  
  // Bid Price 10 bytes
  int whole = Lrc_atoi(&msg->msgBody[17], 10);
  quoteInfo.bidPrice = whole * _UQDF_PRICE_PORTION[numerator];

  // Bid Size: 7 bytes
  quoteInfo.bidSize = Lrc_atoi(&msg->msgBody[27], 7);
  
  // Offer Price Denominator: 1 byte
  numerator = msg->msgBody[34] - 'B'; 
  
  // Offer Price 10 bytes
  whole = Lrc_atoi(&msg->msgBody[35], 10);
  quoteInfo.offerPrice = whole * _UQDF_PRICE_PORTION[numerator];

  // Offer Size: 7 bytes
  quoteInfo.offerSize = Lrc_atoi(&msg->msgBody[45], 7);

  //TraceLog(DEBUG_LEVEL, "UQDF Long-Form: %s %c %d %lf %d %lf\n", symbol, quoteInfo.participantId, quoteInfo.bidSize, quoteInfo.bidPrice, quoteInfo.offerSize, quoteInfo.offerPrice);

  t_NationalAppendage nationalAppendage;
  
  // Initialize ...
  nationalAppendage.bidSize = -1;
  nationalAppendage.offerSize = -1;
  
  // National BBO Appendage Indicator   
  switch (msg->msgBody[55] - 48)
  {
    case 2:
      /* 2: Short Form National BBO Appendage Attached � A new National BBO
        was generated as a result of the UTP participant�s quote update and the new
        National BBO information is contained in the attached short form appendage.*/
      ProcessUQDF_ShortNationalBBOAppendage(&msg->msgBody[58], &nationalAppendage);
      break;
    case 3:
      /* 3: Long Form National BBO Appendage Attached � A new National BBO
        was generated as a result of the UTP participant�s quote update and the new
        information is contained in the attached long form appendage.*/
      ProcessUQDF_LongNationalBBOAppendage(&msg->msgBody[58], &nationalAppendage);
      break;
    case 4:
      /* 4: Quote Contains All National BBO Information � Current UTP
        participant�s quote is itself the National BBO. Vendors should update
        National BBO to reflect this new quote and single market center. (National
        Best Bid Market Center and National Best Ask Market Center fields should be
        updated to reflect the Market Center Originator ID value in the UQDF
        message header.) No appendage is required.*/
      nationalAppendage.bidParticipantId = quoteInfo.participantId;
      nationalAppendage.bidPrice = quoteInfo.bidPrice;
      nationalAppendage.bidSize = quoteInfo.bidSize;    
      nationalAppendage.offerParticipantId = quoteInfo.participantId;
      nationalAppendage.offerPrice = quoteInfo.offerPrice;
      nationalAppendage.offerSize = quoteInfo.offerSize;
      break;
    case 1:
      /* 1:   No National BBO Can be Calculated � The National BBO cannot be
        calculated therefore vendors should show National BBO fields as blank. No
        appendage is required.*/
      memset (&nationalAppendage, 0, sizeof(t_NationalAppendage));
      break;
  
    /* 0: No National BBO Change � The UTP participant�s quote does not affect the
        National BBO. Vendors should continue to show the existing National BBO.
        No appendage is required.*/
  }
  
  // Add trade to list
  AddQuoteToBBOQuoteMgmt(symbolId, &quoteInfo, &nationalAppendage, UQDF_SOURCE);

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessUQDF_BBOShort
- Input:      
- Output:                     
- Return:     
- Description:                                    
- Usage:      
****************************************************************************/
int ProcessUQDF_BBOShort(uint32_t symbolId, t_UQDF_Msg *msg)
{
  t_BBOQuoteInfo quoteInfo;

  msg->msgHeader[MAX_UQDF_HEADER_LEN] = 0;
  
  // PARTICIPANT ID
  quoteInfo.participantId = msg->msgHeader[13];

  // TIME STAMP
  //quoteInfo.timeStamp = (Lrc_atoi(&msg->msgHeader[14], 2) * 3600 + Lrc_atoi(&msg->msgHeader[16], 2) * 60 + Lrc_atoi(&msg->msgHeader[18], 2)) * 1000 + Lrc_atoi(&msg->msgHeader[20], 3);

  
  // QUOTE CONDITIONS: 1 byte
  quoteInfo.quoteCondition = msg->msgBody[7];
  
  // Bid Price Denominator: 1 byte
  int numerator = msg->msgBody[9] - 'B';  
  
  // Bid Price 6 bytes
  int whole = Lrc_atoi(&msg->msgBody[10], 6);
  quoteInfo.bidPrice = whole * _UQDF_PRICE_PORTION[numerator];

  // Bid Size: 2 bytes
  quoteInfo.bidSize = Lrc_atoi(&msg->msgBody[16], 2);
  
  // Offer Price Denominator: 1 byte
  numerator = msg->msgBody[18] - 'B'; 
  
  // Offer Price 6 bytes
  whole = Lrc_atoi(&msg->msgBody[19], 6);
  quoteInfo.offerPrice = whole * _UQDF_PRICE_PORTION[numerator];

  // Offer Size: 2 bytes
  quoteInfo.offerSize = Lrc_atoi(&msg->msgBody[25], 2);

  //TraceLog(DEBUG_LEVEL, "UQDF Short-Form: %s %c %d %lf %d %lf\n", symbol, quoteInfo.participantId, quoteInfo.bidSize, quoteInfo.bidPrice, quoteInfo.offerSize, quoteInfo.offerPrice);

  t_NationalAppendage nationalAppendage;
  
  // Initialize ...
  nationalAppendage.bidSize = -1;
  nationalAppendage.offerSize = -1;
    
  // National BBO Appendage Indicator   
  switch (msg->msgBody[27] - 48)
  {
    case 2:
      /* 2: Short Form National BBO Appendage Attached � A new National BBO
        was generated as a result of the UTP participant�s quote update and the new
        National BBO information is contained in the attached short form appendage*/
      ProcessUQDF_ShortNationalBBOAppendage(&msg->msgBody[30], &nationalAppendage);
      break;
    case 3:
      /* 3: Long Form National BBO Appendage Attached � A new National BBO
        was generated as a result of the UTP participant�s quote update and the new
        information is contained in the attached long form appendage.*/
      ProcessUQDF_LongNationalBBOAppendage(&msg->msgBody[30], &nationalAppendage);
      break;
    case 4:
      /* 4: Quote Contains All National BBO Information � Current UTP
        participant�s quote is itself the National BBO. Vendors should update
        National BBO to reflect this new quote and single market center. (National
        Best Bid Market Center and National Best Ask Market Center fields should be
        updated to reflect the Market Center Originator ID value in the UQDF
        message header.) No appendage is required.*/
      nationalAppendage.bidParticipantId = quoteInfo.participantId;
      nationalAppendage.bidPrice = quoteInfo.bidPrice;
      nationalAppendage.bidSize = quoteInfo.bidSize;    
      nationalAppendage.offerParticipantId = quoteInfo.participantId;
      nationalAppendage.offerPrice = quoteInfo.offerPrice;
      nationalAppendage.offerSize = quoteInfo.offerSize;
      break;
    case 1:
      /* 1:   No National BBO Can be Calculated � The National BBO cannot be
        calculated therefore vendors should show National BBO fields as blank. No
        appendage is required.*/
      memset (&nationalAppendage, 0, sizeof(t_NationalAppendage));
      break;
  
      /* 0: No National BBO Change � The UTP participant�s quote does not affect the
        National BBO. Vendors should continue to show the existing National BBO.
        No appendage is required.*/
  }
  
  // Add trade to list
  AddQuoteToBBOQuoteMgmt(symbolId, &quoteInfo, &nationalAppendage, UQDF_SOURCE);

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessUQDF_RegSHOShortSalePriceTestRestrictedIndicator
- Usage:      Determine short sale restriction
****************************************************************************/
int ProcessUQDF_RegSHOShortSalePriceTestRestrictedIndicator(uint32_t symbolId, t_UQDF_Msg *msg)
{
  unsigned char oldValue = SymbolMgmt.symbolList[symbolId].shortSaleStatus;
  unsigned char newValue = oldValue;
  
  // Reg SHO Action: 1 byte
  char regSHOAction = msg->msgBody[11];
  switch (regSHOAction)
  {
    case '0': // No price test in effect
      newValue = oldValue | SHORT_SALE_RESTRICTION_BIT_FLAG;
      break;
    case '1': // RegSHO Short Sale Price Test Restriction in effect due to an intra-day price drop in security
    case '2': // Reg SHO Short Sale Price Test Restriction remains in effect
      newValue = oldValue & ~SHORT_SALE_RESTRICTION_BIT_FLAG;
      break;
    default:
      TraceLog(WARN_LEVEL, "UQDF Reg SHO Short Sale Price Test Restricted Indicator: Invalid 'Reg SHO Action' value: '%c'\n", regSHOAction);
      break;
  }
  
  if (oldValue != newValue)
  {
    SymbolMgmt.symbolList[symbolId].shortSaleStatus = newValue;
    //DEVLOGF("UQDF Reg SHO Short Sale Price Test Restricted Indicator ('%c'): Symbol %.8s, old value: %d, new value: %d\n", regSHOAction, symbol, oldValue, newValue);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessUQDF_ShortNationalBBOAppendage
- Input:      
- Output:                     
- Return:     
- Description:                                    
- Usage:      
****************************************************************************/
void ProcessUQDF_ShortNationalBBOAppendage(char *msg, t_NationalAppendage *nationalAppendage)
{
  //BEST BID PARTICIPANT ID: 1 Byte
  nationalAppendage->bidParticipantId = msg[1];
  
  // Bid Price Denominator: 1 byte
  int numerator = msg[2] - 'B'; 
  
  // Bid Price 6 bytes
  int whole = Lrc_atoi(&msg[3], 6);
  nationalAppendage->bidPrice = whole * _UQDF_PRICE_PORTION[numerator];

  // Bid Size: 2 bytes
  nationalAppendage->bidSize = Lrc_atoi(&msg[9], 2);
  
  //RESERVED: 1
  
  //BEST Offer PARTICIPANT ID: 1 Byte
  nationalAppendage->offerParticipantId = msg[12];
  
  // Offer Price Denominator: 1 byte
  numerator = msg[13] - 'B';
  
  // Offer Price 6 bytes
  whole = Lrc_atoi(&msg[14], 6);
  nationalAppendage->offerPrice = whole * _UQDF_PRICE_PORTION[numerator];

  // Offer Size: 2 bytes
  nationalAppendage->offerSize = Lrc_atoi(&msg[20], 2);
  
  //TraceLog(DEBUG_LEVEL, "NATIONAL SHORT: %c %d %lf %c %d %lf\n", nationalAppendage->bidParticipantId, nationalAppendage->bidSize, nationalAppendage->bidPrice, nationalAppendage->offerParticipantId, nationalAppendage->offerSize, nationalAppendage->offerPrice);
  
  //RESERVED : 1
  return;
}

/****************************************************************************
- Function name:  ProcessUQDF_LongNationalBBOAppendage
- Input:      
- Output:                     
- Return:     
- Description:                                    
- Usage:      
****************************************************************************/
void ProcessUQDF_LongNationalBBOAppendage(char *msg, t_NationalAppendage *nationalAppendage)
{
  //BEST BID PARTICIPANT ID: 1 Byte
  nationalAppendage->bidParticipantId = msg[1];
  
  // Bid Price Denominator: 1 byte
  int numerator = msg[2] - 'B'; 
  
  // Bid Price 10 bytes
  int whole = Lrc_atoi(&msg[3], 10);
  nationalAppendage->bidPrice = whole * _UQDF_PRICE_PORTION[numerator];

  // Bid Size: 7 bytes
  nationalAppendage->bidSize = Lrc_atoi(&msg[13], 7);
  
  //RESERVED: 1
  
  //BEST Offer PARTICIPANT ID: 1 Byte
  nationalAppendage->offerParticipantId = msg[21];
  
  // Offer Price Denominator: 1 byte
  numerator = msg[22] - 'B';
  
  // Offer Price 10 bytes
  whole = Lrc_atoi(&msg[23], 10);
  nationalAppendage->offerPrice = whole * _UQDF_PRICE_PORTION[numerator];

  // Offer Size: 7 bytes
  nationalAppendage->offerSize = Lrc_atoi(&msg[33], 7);
  
  //TraceLog(DEBUG_LEVEL, "NATIONAL LONG: %c %d %lf %c %d %lf\n", nationalAppendage->bidParticipantId, nationalAppendage->bidSize, nationalAppendage->bidPrice, nationalAppendage->offerParticipantId, nationalAppendage->offerSize, nationalAppendage->offerPrice);
  
  return;
}

