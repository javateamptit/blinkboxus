/****************************************************************************
** Include files and define several variables
****************************************************************************/
#include "manual.h"

#include "utility.h"
#include "tuiutil.h"
#include "book_mgmt.h"
#include "order_mgmt.h"
#include "trading_mgmt.h"
#include "arca_book_proc.h"
#include "nasdaq_book_proc.h"
#include "nyse_book_proc.h"
#include "bats_book_proc.h"
#include "arca_direct_proc.h"
#include "ouch_oec_proc.h"
#include "nasdaq_rash_proc.h"
#include "nyse_ccg_proc.h"
#include "edge_order_proc.h"
#include "bats_boe_proc.h"

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdarg.h>
#include <errno.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include <pthread.h>
#include <signal.h>
#include <semaphore.h>

#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h> 
#include <sys/socket.h> 
#include <arpa/inet.h> 
#include <sys/time.h>

/****************************************************************************
- Function name:  ConnectOec
- Input:      
- Output:   
- Return:   
- Description:  
****************************************************************************/
int ConnectOec(char argc, char *argv[])
{
    //"oec_connect", "Connect to oec", "oec_connect <arca/ouch/rash/edgx/bats/nyse>
  printf("Process command for connecting to OEC %s ...\n", argv[1]);
  
  if (strncasecmp(argv[1], "arca", 4) == 0) 
  {
    if ((OrderStatusMgmt[ORDER_ARCA_DIRECT].isConnected == DISCONNECTED) && 
        (OrderStatusMgmt[ORDER_ARCA_DIRECT].shouldConnect != YES))
        {
          OrderStatusMgmt[ORDER_ARCA_DIRECT].shouldConnect = YES;
          
          Connect_ARCA_DIRECT();
          
          TraceLog(DEBUG_LEVEL, "Received request to start ARCA DIRECT\n");
        }
  }
  else if (strncasecmp(argv[1], "ouch", 4) == 0)
  {
    if ((OrderStatusMgmt[ORDER_NASDAQ_OUCH].isConnected == DISCONNECTED) &&
        (OrderStatusMgmt[ORDER_NASDAQ_OUCH].shouldConnect != YES))
      {
        OrderStatusMgmt[ORDER_NASDAQ_OUCH].shouldConnect = YES;
        
        Connect_Ouch(ORDER_NASDAQ_OUCH);
        
        TraceLog(DEBUG_LEVEL, "Received request to start NASDAQ OUCH\n");
      }
  }
  else if (strncasecmp(argv[1], "bx", 2) == 0)
  {
    if ((OrderStatusMgmt[ORDER_NASDAQ_BX].isConnected == DISCONNECTED) &&
        (OrderStatusMgmt[ORDER_NASDAQ_BX].shouldConnect != YES))
      {
        OrderStatusMgmt[ORDER_NASDAQ_BX].shouldConnect = YES;
        
        Connect_Ouch(ORDER_NASDAQ_BX);
        
        TraceLog(DEBUG_LEVEL, "Received request to start NASDAQ BX\n");
      }
  }
  else if (strncasecmp(argv[1], "psx", 3) == 0)
  {
    if ((OrderStatusMgmt[ORDER_NASDAQ_PSX].isConnected == DISCONNECTED) &&
        (OrderStatusMgmt[ORDER_NASDAQ_PSX].shouldConnect != YES))
      {
        OrderStatusMgmt[ORDER_NASDAQ_PSX].shouldConnect = YES;
        
        Connect_Ouch(ORDER_NASDAQ_PSX);
        
        TraceLog(DEBUG_LEVEL, "Received request to start NASDAQ PSX\n");
      }
  }
  else if (strncasecmp(argv[1], "rash", 4) == 0)
  {
    if ((OrderStatusMgmt[ORDER_NASDAQ_RASH].isConnected == DISCONNECTED) &&
        (OrderStatusMgmt[ORDER_NASDAQ_RASH].shouldConnect != YES))
      {
        OrderStatusMgmt[ORDER_NASDAQ_RASH].shouldConnect = YES;
        
        Connect_NASDAQ_RASH();
        
        TraceLog(DEBUG_LEVEL, "Received request to start NASDAQ RASH\n");
      }
  }
  else if (strncasecmp(argv[1], "batsz", 5) == 0)
  {
    if ((OrderStatusMgmt[ORDER_BATSZ_BOE].isConnected == DISCONNECTED) &&
        (OrderStatusMgmt[ORDER_BATSZ_BOE].shouldConnect != YES))
      {
        OrderStatusMgmt[ORDER_BATSZ_BOE].shouldConnect = YES;
        
        Connect_BATS_BOE(ORDER_BATSZ_BOE);
        
        TraceLog(DEBUG_LEVEL, "Received request to start BATSZ BOE\n");
      } 
  }
  else if (strncasecmp(argv[1], "byx", 3) == 0)
  {
    if ((OrderStatusMgmt[ORDER_BYX_BOE].isConnected == DISCONNECTED) &&
        (OrderStatusMgmt[ORDER_BYX_BOE].shouldConnect != YES))
      {
        OrderStatusMgmt[ORDER_BYX_BOE].shouldConnect = YES;
        
        Connect_BATS_BOE(ORDER_BYX_BOE);
        
        TraceLog(DEBUG_LEVEL, "Received request to start BYX BOE\n");
      } 
  }
  else if (strncasecmp(argv[1], "nyse", 4) == 0)
  {
    if ((OrderStatusMgmt[ORDER_NYSE_CCG].isConnected == DISCONNECTED) &&
        (OrderStatusMgmt[ORDER_NYSE_CCG].shouldConnect != YES))
      {
        OrderStatusMgmt[ORDER_NYSE_CCG].shouldConnect = YES;
        
        Connect_NYSE_CCG();
        
        TraceLog(DEBUG_LEVEL, "Received request to start NYSE CCG\n");
      }
  }
  else if (strncasecmp(argv[1], "edgx", 4) == 0)
  {
    if ((OrderStatusMgmt[ORDER_EDGX].isConnected == DISCONNECTED) &&
        (OrderStatusMgmt[ORDER_EDGX].shouldConnect != YES))
      {
        OrderStatusMgmt[ORDER_EDGX].shouldConnect = YES;
        
        Connect_EDGE(ORDER_EDGX);
        
        TraceLog(DEBUG_LEVEL, "Received request to start EDGX\n");
      } 
  }
  else if (strncasecmp(argv[1], "edga", 4) == 0)
  {
    if ((OrderStatusMgmt[ORDER_EDGA].isConnected == DISCONNECTED) &&
        (OrderStatusMgmt[ORDER_EDGA].shouldConnect != YES))
      {
        OrderStatusMgmt[ORDER_EDGA].shouldConnect = YES;
        
        Connect_EDGE(ORDER_EDGA);
        
        TraceLog(DEBUG_LEVEL, "Received request to start EDGA\n");
      } 
  }
  else
  {
    printf("%s: Invalid server %s\n", __func__, argv[1]);
    return -1;
  }
  
  return 0;
}

int DisconnectOec(char argc, char *argv[])
{
  printf("Process command for disconnecting from OEC %s ...\n", argv[1]);
  
  if (strncasecmp(argv[1], "arca", 4) == 0) 
  {
    if(OrderStatusMgmt[ORDER_ARCA_DIRECT].isConnected == CONNECTED)
        {
          TraceLog(DEBUG_LEVEL, "Received request to disconnect ARCA DIRECT\n");
          OrderStatusMgmt[ORDER_ARCA_DIRECT].shouldConnect = NO;
          disconnectSession(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, OecConfig[ORDER_ARCA_DIRECT].sessionId);
        }
  }
  else if (strncasecmp(argv[1], "ouch", 4) == 0)
  {
    if(OrderStatusMgmt[ORDER_NASDAQ_OUCH].isConnected == CONNECTED)
        {
          TraceLog(DEBUG_LEVEL, "Received request to disconnect NASDAQ OUCH\n");
          OrderStatusMgmt[ORDER_NASDAQ_OUCH].shouldConnect = NO;
          
          BuildAndSend_Ouch_LogoffMessage(ORDER_NASDAQ_OUCH);
          disconnectSession(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, OecConfig[ORDER_NASDAQ_OUCH].sessionId);
        }
  }
  else if (strncasecmp(argv[1], "bx", 2) == 0)
  {
    if(OrderStatusMgmt[ORDER_NASDAQ_BX].isConnected == CONNECTED)
        {
            TraceLog(DEBUG_LEVEL, "Received request to disconnect NASDAQ BX\n");
            OrderStatusMgmt[ORDER_NASDAQ_BX].shouldConnect = NO;
            
            BuildAndSend_Ouch_LogoffMessage(ORDER_NASDAQ_BX);
            disconnectSession(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, OecConfig[ORDER_NASDAQ_BX].sessionId);
        }
  }
  else if (strncasecmp(argv[1], "psx", 3) == 0)
  {
    if(OrderStatusMgmt[ORDER_NASDAQ_PSX].isConnected == CONNECTED)
        {
            TraceLog(DEBUG_LEVEL, "Received request to disconnect NASDAQ ORDER_NASDAQ_PSX\n");
            OrderStatusMgmt[ORDER_NASDAQ_PSX].shouldConnect = NO;
            
            BuildAndSend_Ouch_LogoffMessage(ORDER_NASDAQ_PSX);
            disconnectSession(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, OecConfig[ORDER_NASDAQ_PSX].sessionId);
        }
  }
  else if (strncasecmp(argv[1], "rash", 4) == 0)
  {
    if(OrderStatusMgmt[ORDER_NASDAQ_RASH].isConnected == CONNECTED)
        {
          TraceLog(DEBUG_LEVEL, "Received request to disconnect NASDAQ RASH\n");
          OrderStatusMgmt[ORDER_NASDAQ_RASH].shouldConnect = NO;
          
          disconnectSession(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, OecConfig[ORDER_NASDAQ_RASH].sessionId);
        }
  }
  else if (strncasecmp(argv[1], "bats", 4) == 0)
  {
    if(OrderStatusMgmt[ORDER_BATSZ_BOE].isConnected == CONNECTED)
        {
          OrderStatusMgmt[ORDER_BATSZ_BOE].shouldConnect = NO;
          
          if(BuildAndSend_BATS_BOE_LogoutRequestMsg(ORDER_BATSZ_BOE) == ERROR)
          {
            TraceLog(ERROR_LEVEL, "Could not send Logout Request to BATSZ_BOE, forcing disconnect\n");
            disconnectSession(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, OecConfig[ORDER_BATSZ_BOE].sessionId);
          }
          else
          {
            TraceLog(DEBUG_LEVEL, "BATSZ BOE: Send logout message to server successfully\n");
            TraceLog(DEBUG_LEVEL, "BATSZ BOE: Waiting for logout response from server\n");
          }
        }
        
        TraceLog(DEBUG_LEVEL, "Received request to disconnect BATSZ BOE\n");
  }
  else if (strncasecmp(argv[1], "byx", 3) == 0)
  {
    if(OrderStatusMgmt[ORDER_BYX_BOE].isConnected == CONNECTED)
        {
            OrderStatusMgmt[ORDER_BYX_BOE].shouldConnect = NO;
            
            if(BuildAndSend_BATS_BOE_LogoutRequestMsg(ORDER_BYX_BOE) == ERROR)
            {
                TraceLog(ERROR_LEVEL, "Could not send Logout Request to BYX_BOE, forcing disconnect\n");
								disconnectSession(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, OecConfig[ORDER_BYX_BOE].sessionId);
            }
            else
            {
                TraceLog(DEBUG_LEVEL, "BYX BOE: Send logout message to server successfully\n");
                TraceLog(DEBUG_LEVEL, "BYX BOE: Waiting for logout response from server\n");
            }
        }
        
        TraceLog(DEBUG_LEVEL, "Received request to disconnect BYX BOE\n");
  }
  else if (strncasecmp(argv[1], "nyse", 4) == 0)
  {
    if(OrderStatusMgmt[ORDER_NYSE_CCG].isConnected == CONNECTED)
        {
          TraceLog(DEBUG_LEVEL, "Received request to disconnect NYSE CCG\n");
          OrderStatusMgmt[ORDER_NYSE_CCG].shouldConnect = NO;
          disconnectSession(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, OecConfig[ORDER_NYSE_CCG].sessionId);
        }
  }
  else if (strncasecmp(argv[1], "edgx", 4) == 0)
  {
    if(OrderStatusMgmt[ORDER_EDGX].isConnected == CONNECTED)
        {
          if (BuildAndSend_LogoutRequestMsg_EDGE(ORDER_EDGX) == ERROR)
          {
            TraceLog(ERROR_LEVEL, "Could not send Logout Request to EDGX, forcing disconnect\n");
            disconnectSession(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, OecConfig[ORDER_EDGX].sessionId);
          }       
          OrderStatusMgmt[ORDER_EDGX].shouldConnect = NO;   //TS will automatically send logout request
        }
        
        TraceLog(DEBUG_LEVEL, "Received request to disconnect EDGX\n"); 
  }
  else if (strncasecmp(argv[1], "edga", 4) == 0)
  {
    if(OrderStatusMgmt[ORDER_EDGA].isConnected == CONNECTED)
        {
            if (BuildAndSend_LogoutRequestMsg_EDGE(ORDER_EDGA) == ERROR)
            {
                TraceLog(ERROR_LEVEL, "Could not send Logout Request to EDGA, forcing disconnect\n");
                disconnectSession(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, OecConfig[ORDER_EDGA].sessionId);
            }       
            OrderStatusMgmt[ORDER_EDGA].shouldConnect = NO;   //TS will automatically send logout request
        }
        
        TraceLog(DEBUG_LEVEL, "Received request to disconnect EDGA\n"); 
  }
  else
  {
    printf("%s: Invalid server %s\n", __func__, argv[1]);
    return -1;
  }
  
  return 0;
}

int ConnectMdc(char argc, char *argv[])
{
    //mdc_connect <arca/nasdaq/edgx/bats/nyse>
  printf("Process command for connecting to MDC %s ...\n", argv[1]);
  
  if (strncasecmp(argv[1], "arca", 4) == 0) 
  {
    TraceLog(DEBUG_LEVEL, "Received request to start ARCA BOOK\n");
    ConnectToVenue(BOOK_ARCA);
  }
  else if (strncasecmp(argv[1], "nasdaq", 6) == 0)
  {
    TraceLog(DEBUG_LEVEL, "Received request to start NASDAQ BOOK\n");
    ConnectToVenue(BOOK_NASDAQ);
  }
  else if (strncasecmp(argv[1], "bx", 2) == 0)
  {
    TraceLog(DEBUG_LEVEL, "Received request to start BX BOOK\n");
    ConnectToVenue(BOOK_NDBX);
  }
  else if (strncasecmp(argv[1], "psx", 3) == 0)
  {
    TraceLog(DEBUG_LEVEL, "Received request to start PSX BOOK\n");
    ConnectToVenue(BOOK_PSX);
  }
  else if (strncasecmp(argv[1], "edgx", 4) == 0)
  {
    TraceLog(DEBUG_LEVEL, "Received request to start EDGX BOOK\n");
    ConnectToVenue(BOOK_EDGX);
  }
  else if (strncasecmp(argv[1], "edga", 4) == 0)
  {
    TraceLog(DEBUG_LEVEL, "Received request to start EDGA BOOK\n");
        ConnectToVenue(BOOK_EDGA);
  }
  else if (strncasecmp(argv[1], "bats", 4) == 0)
  {
    TraceLog(DEBUG_LEVEL, "Received request to start BATS-Z BOOK\n");
    ConnectToVenue(BOOK_BATSZ); 
  }
  else if (strncasecmp(argv[1], "byx", 3) == 0)
  {
    TraceLog(DEBUG_LEVEL, "Received request to start BYX BOOK\n");
    ConnectToVenue(BOOK_BYX); 
  }
  else if (strncasecmp(argv[1], "nyse", 4) == 0)
  {
    TraceLog(DEBUG_LEVEL, "Received request to start NYSE BOOK\n");
    ConnectToVenue(BOOK_NYSE);
  }
  else if (strncasecmp(argv[1], "amex", 4) == 0)
  {
    TraceLog(DEBUG_LEVEL, "Received request to start AMEX BOOK\n");
    ConnectToVenue(BOOK_AMEX);
  }
  else
  {
    printf("%s: Invalid server %s\n", __func__, argv[1]);
    return -1;
  }
  
  return 0;
}

int DisconnectMdc(char argc, char *argv[])
{
  printf("Process command for disconnecting to MDC %s ...\n", argv[1]);
  
  if (strncasecmp(argv[1], "arca", 4) == 0) 
  {
    TraceLog(DEBUG_LEVEL, "Received request to disconnect ARCA BOOK\n");
    DisconnectFromVenue(BOOK_ARCA, -1);
  }
  else if (strncasecmp(argv[1], "nasdaq", 6) == 0)
  {
    TraceLog(DEBUG_LEVEL, "Received request to disconnect NASDAQ BOOK\n");
    DisconnectFromVenue(BOOK_NASDAQ, -1);
  }
  else if (strncasecmp(argv[1], "bx", 2) == 0)
  {
    TraceLog(DEBUG_LEVEL, "Received request to disconnect BX BOOK\n");
    DisconnectFromVenue(BOOK_NDBX, -1);
  }
  else if (strncasecmp(argv[1], "psx", 3) == 0)
  {
    TraceLog(DEBUG_LEVEL, "Received request to disconnect PSX BOOK\n");
    DisconnectFromVenue(BOOK_PSX, -1);
  }
  else if (strncasecmp(argv[1], "edgx", 4) == 0)
  {
    TraceLog(DEBUG_LEVEL, "Received request to disconnect EDGX BOOK\n");
    DisconnectFromVenue(BOOK_EDGX, -1);
  }
  else if (strncasecmp(argv[1], "edga", 4) == 0)
  {
    TraceLog(DEBUG_LEVEL, "Received request to disconnect EDGA BOOK\n");
        DisconnectFromVenue(BOOK_EDGA, -1);
  }
  else if (strncasecmp(argv[1], "bats", 4) == 0)
  {
    TraceLog(DEBUG_LEVEL, "Received request to disconnect BATS-Z BOOK\n");
    DisconnectFromVenue(BOOK_BATSZ, -1);  
  }
  else if (strncasecmp(argv[1], "byx", 3) == 0)
  {
    TraceLog(DEBUG_LEVEL, "Received request to disconnect BYX BOOK\n");
    DisconnectFromVenue(BOOK_BYX, -1);  
  }
  else if (strncasecmp(argv[1], "nyse", 4) == 0)
  {
    TraceLog(DEBUG_LEVEL, "Received request to disconnect NYSE BOOK\n");
    DisconnectFromVenue(BOOK_NYSE, -1);
  }
  else if (strncasecmp(argv[1], "amex", 4) == 0)
  {
    TraceLog(DEBUG_LEVEL, "Received request to disconnect AMEX BOOK\n");
    DisconnectFromVenue(BOOK_AMEX, -1);
  }
  else
  {
    printf("%s: Invalid server %s\n", __func__, argv[1]);
    return -1;
  }
  
  return 0;
}

int ShowSymbolsHaveQuotes(char argc, char *argv[])
{
  printf("Process command for getting symbols have quotes of '%s'...\n", argv[1]);
  
  int ecnBookID = MAX_ECN_BOOK;
  if (strncasecmp(argv[1], "arca", 4) == 0) 
  {
    ecnBookID = BOOK_ARCA;
  }
  else if (strncasecmp(argv[1], "nasdaq", 6) == 0)
  {
    ecnBookID = BOOK_NASDAQ;
  }
  else if (strncasecmp(argv[1], "bx", 2) == 0)
  {
    ecnBookID = BOOK_NDBX;
  }
  else if (strncasecmp(argv[1], "psx", 3) == 0)
  {
    ecnBookID = BOOK_PSX;
  }
  else if (strncasecmp(argv[1], "edgx", 4) == 0)
  {
    ecnBookID = BOOK_EDGX;
  }
  else if (strncasecmp(argv[1], "edga", 4) == 0)
  {
    ecnBookID = BOOK_EDGA;
  }
  else if (strncasecmp(argv[1], "bats", 4) == 0)
  {
    ecnBookID = BOOK_BATSZ;
  }
  else if (strncasecmp(argv[1], "byx", 3) == 0)
  {
    ecnBookID = BOOK_BYX;
  }
  else if (strncasecmp(argv[1], "nyse", 4) == 0)
  {
    ecnBookID = BOOK_NYSE;
  }
  else if (strncasecmp(argv[1], "amex", 4) == 0)
  {
    ecnBookID = BOOK_AMEX;
  }
  else
  {
    printf("%s: Invalid server '%s'\n", __func__, argv[1]);
    return -1;
  }
  
  int numberOfSymbolsWithQuotes = 0;
  int stockSymbolIndex;
  for (stockSymbolIndex = 0; stockSymbolIndex < SymbolMgmt.nextSymbolIndex; stockSymbolIndex++)
  {
    t_QuoteList *askList = StockBook[stockSymbolIndex][ecnBookID][ASK_SIDE];
    
    // Check empty for the list
    if (askList)
    {
      numberOfSymbolsWithQuotes++;
      printf("'%9s' ", SymbolMgmt.symbolList[stockSymbolIndex].symbol);
      if (numberOfSymbolsWithQuotes % 8 == 0) printf("\n");
    }
    else
    {
      t_QuoteList *bidList = StockBook[stockSymbolIndex][ecnBookID][BID_SIDE];
      
      // Check empty for the list
      if (bidList)
      {
        numberOfSymbolsWithQuotes++;
        printf("'%9s' ", SymbolMgmt.symbolList[stockSymbolIndex].symbol);
        if (numberOfSymbolsWithQuotes % 8 == 0) printf("\n");
      }
    }
  }
  
  printf("Total symbols: %d\n", numberOfSymbolsWithQuotes);
  return 0;
}

int ViewStockStatus(char argc, char *argv[])
{
  printf("Process command for getting stock status of '%s'...\n", argv[1]);
  
  char stockSymbol[8];
  strncpy(stockSymbol, argv[1], 8);
  int stockSymbolIndex = GetStockSymbolIndex(stockSymbol);
  if(stockSymbolIndex == -1)
  {
    printf("Stock Symbol %s did not exist in stock book\n", stockSymbol);
    return 0;
  }
  
  t_QuoteNode *node;
  t_QuoteList *list;
  int numItem, bookIndex;
  for(bookIndex = 0; bookIndex < MAX_ECN_BOOK; bookIndex++)
  {
    numItem = 0;
    list = StockBook[stockSymbolIndex][bookIndex][ASK_SIDE];
    
    while (list)
    {
      node = list->head;
      while(node)
      {
        printf("ASK %10d@%lf %d\n", node->info.shareVolume, node->info.sharePrice, bookIndex);
        
        numItem++;
        if (numItem == 10) break;
        
        node = node->next;
      }
      
      if (numItem == 10) break;
      
      list = list->next;
    }
  }
  
  //BID Block
  for(bookIndex = 0; bookIndex < MAX_ECN_BOOK; bookIndex++)
  {
    numItem = 0;
    list = StockBook[stockSymbolIndex][bookIndex][BID_SIDE];
    
    while(list)
    {
      node = list->head;
      while(node)
      {
        printf("BID %10d@%lf %d\n", node->info.shareVolume, node->info.sharePrice, bookIndex);
        numItem++;
        
        if (numItem == 10) break;
        
        node = node->next;
      }
      
      if (numItem == 10) break;
      
      list = list->next;
    }
  }
  return 0;
}

int ViewSymbolHaltStatus(char argc, char *argv[])
{
  printf("Process command for getting halt status of '%s'...\n", argv[1]);
  
  char stockSymbol[8];
  strncpy(stockSymbol, argv[1], 8);
  int stockSymbolIndex = GetStockSymbolIndex(stockSymbol);
  
  printf("Halt status of '%s' are:",stockSymbol);
  int ecnBookID;
  for (ecnBookID = 0; ecnBookID < MAX_ECN_BOOK; ecnBookID++)
  {
    printf(" %d", SymbolMgmt.symbolList[stockSymbolIndex].haltStatus[ecnBookID]);
  }
  printf("\nNyse trading status of this symbol: '%c' %d\n",
    SymbolMgmt.symbolList[stockSymbolIndex].tradingStatusInNyseBook,
    SymbolMgmt.symbolList[stockSymbolIndex].tradingStatusInNyseBook);
  
  return 0;
}

int __Arca_NewOrder(char argc, char *argv[]) {
  //arca_new <buy/sell> <symbol> <volume> <price>
  
  t_OrderPlacementInfo newOrder;
  
  newOrder.ISO_Flag = NO;
  newOrder.orderID = GetNewOrderID();
  newOrder.shareVolume = atoi(argv[3]);
  newOrder.sharePrice = atof(argv[4]);
  newOrder.symbol = &argv[2][0];
    
  if (strncasecmp(argv[1], "buy", 3) == 0)
  {
    newOrder.side = '1';
  }
  else if (strncasecmp(argv[1], "sell", 4) == 0)
  {
    newOrder.side = '2';
  }
  else if (strncasecmp(argv[1], "short", 5) == 0)
  {
    newOrder.side = '5';
  }
  else
  {
    puts("Invalid buy/sell (Buy/Sell/Short)");
    return 0;
  }
  
  BuildAndSend_ARCA_DIRECT_OrderMsg(&newOrder);
  
  return 0;
}

int __Ouch_NewOrder(char argc, char *argv[]) {
  //ouch_new <buy/sell> <symbol> <volume> <price>
  
  t_OrderPlacementInfo newOrder;
  
  newOrder.ISO_Flag = NO;
  newOrder.orderID = GetNewOrderID();
  newOrder.shareVolume = atoi(argv[3]);
  newOrder.sharePrice = atof(argv[4]);
  newOrder.symbol = &argv[2][0];
    
  if (strncasecmp(argv[1], "buy", 3) == 0)
  {
    newOrder.side = 'B';
  }
  else if (strncasecmp(argv[1], "sell", 4) == 0)
  {
    newOrder.side = 'S';
  }
  else if (strncasecmp(argv[1], "short", 5) == 0)
  {
    newOrder.side = 'T';
  }
  else
  {
    puts("Invalid buy/sell (Buy/Sell/Short)");
    return 0;
  }
  
  if(BuildAndSend_Ouch_OrderMsg(ORDER_NASDAQ_OUCH, &newOrder) == SUCCESS)
  {
    newOrder.dataBlock.msgLen = OUCH_NEW_ORDER_MSG_LEN;
    Add_Ouch_DataBlockToCollection(ORDER_NASDAQ_OUCH, &newOrder.dataBlock);
  }
  return 0;
}

int __Rash_NewOrder(char argc, char *argv[]) {
  //rash_new <buy/sell> <symbol> <volume> <price>

  t_OrderPlacementInfo newOrder;
  
  newOrder.ISO_Flag = NO;
  newOrder.orderID = GetNewOrderID();
  newOrder.shareVolume = atoi(argv[3]);
  newOrder.sharePrice = atof(argv[4]);
  newOrder.symbol = &argv[2][0];
  newOrder.routeDest = "SCAN";
  
  if (strncasecmp(argv[1], "buy", 3) == 0)
  {
    newOrder.side = 'B';
  }
  else if (strncasecmp(argv[1], "sell", 4) == 0)
  {
    newOrder.side = 'S';
  }
  else if (strncasecmp(argv[1], "short", 5) == 0)
  {
    newOrder.side = 'T';
  }
  else
  {
    puts("Invalid buy/sell (Buy/Sell/Short)");
    return 0;
  }
  
  BuildAndSend_NASDAQ_RASH_OrderMsg(&newOrder);
  
  return 0;
}

int __Nyse_NewOrder(char argc, char *argv[]) {
  //nyse_new <buy/sell> <symbol> <volume> <price>

  t_OrderPlacementInfo newOrder;
  
  newOrder.ISO_Flag = NO;
  newOrder.orderID = GetNewOrderID();
  newOrder.shareVolume = atoi(argv[3]);
  newOrder.sharePrice = atof(argv[4]);
  newOrder.symbol = &argv[2][0];
  newOrder.stockSymbolIndex = GetStockSymbolIndex(newOrder.symbol);
  
  if (strncasecmp(argv[1], "buy", 3) == 0)
  {
    newOrder.side = '1';
  }
  else if (strncasecmp(argv[1], "sell", 4) == 0)
  {
    newOrder.side = '2';
  }
  else if (strncasecmp(argv[1], "short", 5) == 0)
  {
    newOrder.side = '5';
  }
  else
  {
    puts("Invalid buy/sell (Buy/Sell/Short)");
    return 0;
  }
  
  BuildAndSend_NYSE_CCG_OrderMsg(&newOrder);
  
  return 0;
}

int __Edgx_NewOrder(char argc, char *argv[]) {
  //edgx_new <buy/sell> <symbol> <volume> <price>

  t_OrderPlacementInfo newOrder;
  
  newOrder.ISO_Flag = NO;
  newOrder.orderID = GetNewOrderID();
  newOrder.shareVolume = atoi(argv[3]);
  newOrder.sharePrice = atof(argv[4]);
  newOrder.symbol = &argv[2][0];
  
  if (strncasecmp(argv[1], "buy", 3) == 0)
  {
    newOrder.side = 'B';
  }
  else if (strncasecmp(argv[1], "sell", 4) == 0)
  {
    newOrder.side = 'S';
  }
  else if (strncasecmp(argv[1], "short", 5) == 0)
  {
    newOrder.side = 'T';
  }
  else
  {
    puts("Invalid buy/sell (Buy/Sell/Short)");
    return 0;
  }
  
  BuildAndSend_EDGE_OrderShortMsg(ORDER_EDGX, &newOrder);
  
  return 0;
}  

int __Edgx_NewExtOrder(char argc, char *argv[]) {
  //edgx_new <buy/sell> <symbol> <suffix> <volume> <price>

  t_OrderPlacementInfo newOrder;
  
  newOrder.ISO_Flag = NO;
  newOrder.orderID = GetNewOrderID();
  newOrder.shareVolume = atoi(argv[4]);
  newOrder.sharePrice = atof(argv[5]);
  strncpy(newOrder.rootSymbol,  &argv[2][0], 6);
  strncpy(newOrder.symbolSuffix, &argv[3][0], 6);
  
  char symbol[13];
  
  sprintf(symbol, "%s %s", newOrder.rootSymbol, newOrder.symbolSuffix);
    
  if (strncasecmp(argv[1], "buy", 3) == 0)
  {
    newOrder.side = 'B';
  }
  else if (strncasecmp(argv[1], "sell", 4) == 0)
  {
    newOrder.side = 'S';
  }
  else if (strncasecmp(argv[1], "short", 5) == 0)
  {
    newOrder.side = 'T';
  }
  else
  {
    puts("Invalid buy/sell (Buy/Sell/Short)");
    return 0;
  }
  
  BuildAndSend_EDGE_OrderExtendedMsg(ORDER_EDGX, &newOrder);
  
  return 0;
}

#define EDGA_ASK 13
#define EDGA_BID 14

int __Edga_NewOrder(char argc, char *argv[]) {
  //edga_new <buy/sell> <symbol> <volume> <price>

  t_OrderPlacementInfo newOrder;
  
  newOrder.ISO_Flag = NO;
  newOrder.orderID = GetNewOrderID();
  newOrder.shareVolume = atoi(argv[3]);
  newOrder.sharePrice = atof(argv[4]);
  newOrder.symbol = &argv[2][0];
    
  if (strncasecmp(argv[1], "buy", 3) == 0)
  {
    newOrder.side = 'B';
    newOrder.ecnAskBidLaunch = EDGA_BID;
  }
  else if (strncasecmp(argv[1], "sell", 4) == 0)
  {
    newOrder.side = 'S';
    newOrder.ecnAskBidLaunch = EDGA_ASK;
  }
  else if (strncasecmp(argv[1], "short", 5) == 0)
  {
    newOrder.side = 'T';
    newOrder.ecnAskBidLaunch = EDGA_ASK;
  }
  else
  {
    puts("Invalid buy/sell (Buy/Sell/Short)");
    return 0;
  }
  
  //newOrder.ecnAskBidLaunch = EcnHasAskBidLaunchCollection[BOOK_EDGA][newOrder.side == 'B'?BID_SIDE:ASK_SIDE];
  
  BuildAndSend_EDGE_OrderShortMsg(ORDER_EDGA, &newOrder);
  
  newOrder.dataBlock.addContent[DATABLOCK_OFFSET_LAUNCH] = newOrder.ecnAskBidLaunch;
  Add_EDGE_DataBlockToCollection(ORDER_EDGA, &newOrder.dataBlock);
  
  return 0;
}  

int __Edga_NewExtOrder(char argc, char *argv[]) {
  //edga_new <buy/sell> <symbol> <suffix> <volume> <price>

  t_OrderPlacementInfo newOrder;
  
  newOrder.ISO_Flag = NO;
  newOrder.orderID = GetNewOrderID();
  newOrder.shareVolume = atoi(argv[4]);
  newOrder.sharePrice = atof(argv[5]);
  strncpy(newOrder.rootSymbol,  &argv[2][0], 6);
  strncpy(newOrder.symbolSuffix, &argv[3][0], 6);
    
  char symbol[13];
    
  sprintf(symbol, "%s %s", newOrder.rootSymbol, newOrder.symbolSuffix);
  newOrder.symbol = symbol;
    
  if (strncasecmp(argv[1], "buy", 3) == 0)
  {
    newOrder.side = 'B';
    newOrder.ecnAskBidLaunch = EDGA_BID;
  }
  else if (strncasecmp(argv[1], "sell", 4) == 0)
  {
    newOrder.side = 'S';
    newOrder.ecnAskBidLaunch = EDGA_ASK;
  }
  else if (strncasecmp(argv[1], "short", 5) == 0)
  {
    newOrder.side = 'T';
    newOrder.ecnAskBidLaunch = EDGA_ASK;
  }
  else
  {
    puts("Invalid buy/sell (Buy/Sell/Short)");
    return 0;
  }
  
  
  //newOrder.ecnAskBidLaunch = EcnHasAskBidLaunchCollection[BOOK_EDGA][newOrder.side == 'B'?BID_SIDE:ASK_SIDE];
  
  BuildAndSend_EDGE_OrderExtendedMsg(ORDER_EDGA, &newOrder);
  
  newOrder.dataBlock.addContent[DATABLOCK_OFFSET_LAUNCH] = newOrder.ecnAskBidLaunch;
  Add_EDGE_DataBlockToCollection(ORDER_EDGA, &newOrder.dataBlock);
  
  return 0;
}

int __Bats_NewOrder(char argc, char *argv[]) {
  //bats_new <buy/sell> <symbol> <volume> <price>

  t_OrderPlacementInfo newOrder;

  newOrder.ISO_Flag = NO;
  newOrder.orderID = GetNewOrderID();
  newOrder.shareVolume = atoi(argv[3]);
  newOrder.sharePrice = atof(argv[4]);
  newOrder.symbol = &argv[2][0];
  newOrder.isRoutable = NO;
  
  if (strncasecmp(argv[1], "buy", 3) == 0)
  {
    newOrder.side = '1';
  }
  else if (strncasecmp(argv[1], "sell", 4) == 0)
  {
    newOrder.side = '2';
  }
  else if (strncasecmp(argv[1], "short", 5) == 0)
  {
    newOrder.side = '5';
  }
  else
  {
    puts("Invalid buy/sell (Buy/Sell/Short)");
    return 0;
  }
  
  BuildAndSend_BATS_BOE_OrderMsg(ORDER_BATSZ_BOE, &newOrder);
  
  return 0;
}

int __BYX_NewOrder(char argc, char *argv[]) {
  //bats_new <buy/sell> <symbol> <volume> <price>

  t_OrderPlacementInfo newOrder;
  
    newOrder.ISO_Flag = NO;
    newOrder.orderID = GetNewOrderID();
    newOrder.shareVolume = atoi(argv[3]);
    newOrder.sharePrice = atof(argv[4]);
    newOrder.symbol = &argv[2][0];
    newOrder.isRoutable = NO;
    
  if (strncasecmp(argv[1], "buy", 3) == 0)
  {
    newOrder.side = '1';
  }
  else if (strncasecmp(argv[1], "sell", 4) == 0)
  {
    newOrder.side = '2';
  }
  else if (strncasecmp(argv[1], "short", 5) == 0)
  {
    newOrder.side = '5';
  }
  else
  {
    puts("Invalid buy/sell (Buy/Sell/Short)");
    return 0;
  }
  
  BuildAndSend_BATS_BOE_OrderMsg(ORDER_BYX_BOE, &newOrder);
  
  return 0;
}

int __Bx_NewOrder(char argc, char *argv[]) {
  //ouch_new <buy/sell> <symbol> <volume> <price>
  
  t_OrderPlacementInfo newOrder;
  
  newOrder.ISO_Flag = NO;
  newOrder.orderID = GetNewOrderID();
  newOrder.shareVolume = atoi(argv[3]);
  newOrder.sharePrice = atof(argv[4]);
  newOrder.symbol = &argv[2][0];
  
  if (strncasecmp(argv[1], "buy", 3) == 0)
  {
    newOrder.side = 'B';
  }
  else if (strncasecmp(argv[1], "sell", 4) == 0)
  {
    newOrder.side = 'S';
  }
  else if (strncasecmp(argv[1], "short", 5) == 0)
  {
    newOrder.side = 'T';
  }
  else
  {
    puts("Invalid buy/sell (Buy/Sell/Short)");
    return 0;
  }
  
  if(BuildAndSend_Ouch_OrderMsg(ORDER_NASDAQ_BX, &newOrder) == SUCCESS)
  {
    newOrder.dataBlock.msgLen = OUCH_NEW_ORDER_MSG_LEN;
    Add_Ouch_DataBlockToCollection(ORDER_NASDAQ_BX, &newOrder.dataBlock);
  }
  
  return 0;
}

int __Psx_NewOrder(char argc, char *argv[]) {
  //ouch_new <buy/sell> <symbol> <volume> <price>
  
  t_OrderPlacementInfo newOrder;
  
  newOrder.ISO_Flag = NO;
  newOrder.orderID = GetNewOrderID();
  newOrder.shareVolume = atoi(argv[3]);
  newOrder.sharePrice = atof(argv[4]);
  newOrder.symbol = &argv[2][0];
  
  if (strncasecmp(argv[1], "buy", 3) == 0)
  {
    newOrder.side = 'B';
  }
  else if (strncasecmp(argv[1], "sell", 4) == 0)
  {
    newOrder.side = 'S';
  }
  else if (strncasecmp(argv[1], "short", 5) == 0)
  {
    newOrder.side = 'T';
  }
  else
  {
    puts("Invalid buy/sell (Buy/Sell/Short)");
    return 0;
  }
  
  if(BuildAndSend_Ouch_OrderMsg(ORDER_NASDAQ_PSX, &newOrder) == SUCCESS)
  {
    newOrder.dataBlock.msgLen = OUCH_NEW_ORDER_MSG_LEN;
    Add_Ouch_DataBlockToCollection(ORDER_NASDAQ_PSX, &newOrder.dataBlock);
  }
  
  return 0;
}

