/****************************************************************************
** Project name:  Equity Arbitrage Application
** File name:   arca_direct_proc.c
** Description:   This file contains function definitions that were declared
        in arca_direct_proc.h

** Author:    Sang Nguyen-Minh
** First created on 18 September 2007
** Last updated on 18 September 2007
****************************************************************************/

#define _ISOC9X_SOURCE

#include <unistd.h>
#include <sys/time.h>

#include "arca_direct_proc.h"
#include "trading_mgmt.h"
#include "kernel_algorithm.h"
#include "network.h"
#include "logging.h"

/****************************************************************************
          GLOBAL VARIABLES
****************************************************************************/
static double _DIRECT_PRICE_PORTION[10] = {1.0, 0.1, 0.01, 0.001, 0.0001, 0.00001, 0.000001, 0.0000001, 0.00000001, 0.000000001};

static t_OecConfig *venueConfig = &OecConfig[ORDER_ARCA_DIRECT];
static pthread_spinlock_t *ADNewOrderLock = NULL;

/****************************************************************************
** Function definitions
****************************************************************************/

/****************************************************************************
- Function name:  Init_ARCA_DIRECT_DataStructure
****************************************************************************/
int Init_ARCA_DIRECT_DataStructure(void)
{
  if(ADNewOrderLock == NULL)
  {
    ADNewOrderLock = malloc(sizeof(pthread_spinlock_t));
    pthread_spin_init(ADNewOrderLock,0); 
  }

  // Load incoming and outgoing sequence number from file
  if (LoadSequenceNumber(FN_SEQUENCE_NUM_ARCA, ORDER_ARCA_DIRECT) == ERROR)
  { 
    TraceLog(ERROR_LEVEL, "Could not load arca_direct_seqs file\n");
    return ERROR;
  }

  if (venueConfig->socket < 1)
  {
    venueConfig->socket = -1;
  }
  else
  {
    close(venueConfig->socket);
    venueConfig->socket = -1;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  GetLatestSequenceNumbers_ARCA_DIRECT
- Description:
****************************************************************************/
int GetLatestSequenceNumbers_ARCA_DIRECT(int *incomingSeqNum)
{
  *incomingSeqNum = 0;
  char fullPath[MAX_PATH_LEN] = "\0";
  if (GetFullRawDataPath(FN_RAWDATA_ARCA, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Have some problem in get full raw data path operation (%s)\n", FN_RAWDATA_ARCA);
    return ERROR;
  }
  
  FILE *fp = fopen(fullPath, "r");
  if (fp == NULL)
  {
    //TraceLog(ERROR_LEVEL, "Critical! Could not open file (%s).\n", fullPath);
    //PrintErrStr(ERROR_LEVEL, "calling fopen(): ", errno);
    return ERROR;
  }
  
  t_DataBlock dataBlock;

  while (fread(&dataBlock, sizeof(t_DataBlock), 1, fp) == 1)
  {
    // Get incoming sequence number
    switch (dataBlock.msgContent[MSG_TYPE_INDEX])
    {
      case ORDER_ACK_TYPE:
      case ORDER_FILLED_TYPE:
      case CANCEL_ACK_TYPE:
      case ORDER_REJECTED_TYPE:
      case ORDER_KILLED_TYPE:
      case BUST_OR_CORRECT_TYPE:
        *incomingSeqNum = __builtin_bswap32(*(unsigned int*)&dataBlock.msgContent[4]);
        break;
    
      case TEST_REQUEST_TYPE:
      case HEARTBEAT_TYPE:
      case LOGON_TYPE:
      case LOGON_REJECTED_TYPE:
      case NEW_ORDER_TYPE:
        break;
        
      default:
        TraceLog(WARN_LEVEL, "ARCA DIRECT - Unknown message type %d\n", dataBlock.msgContent[MSG_TYPE_INDEX]);
        break;
    }
  }
  
  fclose(fp);
  return SUCCESS;
}

/****************************************************************************
- Function name:  Connect_ARCA_DIRECT
****************************************************************************/
int Connect_ARCA_DIRECT()
{
  if (Init_ARCA_DIRECT_DataStructure() == ERROR)
  { 
    OrderStatusMgmt[ORDER_ARCA_DIRECT].shouldConnect = NO;
    return ERROR;
  }
  
  TraceLog(DEBUG_LEVEL, "Trying connect to ARCA DIRECT Server\n");
  venueConfig->socket = connectSession(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, venueConfig->sessionId);
  
  if (venueConfig->socket == -1)
  {
    TraceLog(ERROR_LEVEL, "Could not connect to ARCA DIRECT Server\n");
    OrderStatusMgmt[ORDER_ARCA_DIRECT].shouldConnect = NO;
    return ERROR;
  }
  
  if (SetSocketRecvTimeout(venueConfig->socket, ARCA_DIRECT_RECV_TIMEOUT) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Could not set socket receive-timeout for ARCA DIRECT socket\n");
    disconnectSession(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, venueConfig->sessionId);
    OrderStatusMgmt[ORDER_ARCA_DIRECT].shouldConnect = NO;
    return ERROR;
  }
  
  if (SetSocketSndTimeout(venueConfig->socket, 1, 0) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Could not set socket send-timeout for Arca Direct socket\n");
    disconnectSession(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, venueConfig->sessionId);
    OrderStatusMgmt[ORDER_ARCA_DIRECT].shouldConnect = NO;
    return ERROR;

  }
  
  /* Disable the Nagle (TCP No Delay) algorithm */
  if (DisableNagleAlgo(venueConfig->socket) == -1)
  {
    TraceLog(ERROR_LEVEL, "Could not set TCP_NODELAY for ARCA DIRECT socket\n");
    disconnectSession(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, venueConfig->sessionId);
    OrderStatusMgmt[ORDER_ARCA_DIRECT].shouldConnect = NO;
    return ERROR;
  }
  
  if (BuildAndSend_ARCA_DIRECT_LogonRequestMsg() == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Could not send logon request message to ARCA DIRECT server\n");
    disconnectSession(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, venueConfig->sessionId);
    OrderStatusMgmt[ORDER_ARCA_DIRECT].shouldConnect = NO;
    return ERROR;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessARCA_DIRECT_DataBlock
****************************************************************************/
int ProcessARCA_DIRECT_DataBlock(uint32_t symbolId, t_DataBlock *dataBlock)
{
  
  switch (dataBlock->msgContent[MSG_TYPE_INDEX])
  {
    case ORDER_ACK_TYPE:
      __sync_add_and_fetch(&venueConfig->incomingSeqNum, 1);
      if (ProcessARCA_DIRECT_OrderAckMsg(symbolId, dataBlock) == ERROR)
      {
        return ERROR;
      }
      break;
      
    case ORDER_FILLED_TYPE:
      __sync_add_and_fetch(&venueConfig->incomingSeqNum, 1);
      
      if (ProcessARCA_DIRECT_OrderFilledMsg(symbolId, dataBlock) == ERROR)
      {
        return ERROR;
      }
      break;
      
    case CANCEL_ACK_TYPE:
      __sync_add_and_fetch(&venueConfig->incomingSeqNum, 1);
      
      if (ProcessARCA_DIRECT_OrderCancelAckMsg(symbolId, dataBlock) == ERROR)
      {
        return ERROR;
      }
      break;
      
    case ORDER_REJECTED_TYPE:
      __sync_add_and_fetch(&venueConfig->incomingSeqNum, 1);
      
      if (ProcessARCA_DIRECT_OrderRejectedMsg(symbolId, dataBlock) == ERROR)
      {
        return ERROR;
      }
      break;
    
    case ORDER_KILLED_TYPE:
      __sync_add_and_fetch(&venueConfig->incomingSeqNum, 1);
      
      if (ProcessARCA_DIRECT_OrderKilledMsg(symbolId, dataBlock) == ERROR)
      {
        return ERROR;
      }
      break;      
    
    case BUST_OR_CORRECT_TYPE:
      __sync_add_and_fetch(&venueConfig->incomingSeqNum, 1);
      
      if (ProcessARCA_DIRECT_BustOrCorrectMsg(symbolId, dataBlock) == ERROR)
      {
        return ERROR;
      }
      break;
    
    case TEST_REQUEST_TYPE:
      if (BuildAndSend_ARCA_DIRECT_HeartbeatMsg() == ERROR)
      {
        return ERROR;
      }
      break;
      
    case HEARTBEAT_TYPE:
      if (BuildAndSend_ARCA_DIRECT_HeartbeatMsg() == ERROR)
      {
        return ERROR;
      }
      break;
      
    case LOGON_TYPE:
      TraceLog(DEBUG_LEVEL, "Received logon accepted from ARCA DIRECT Server\n");
      
      ProcessARCA_DIRECT_LogonAcceptedMsg(dataBlock);
      
      OrderStatusMgmt[ORDER_ARCA_DIRECT].isConnected = CONNECTED;
      
      TraceLog(DEBUG_LEVEL, "incomingSeqNum = %d\n", venueConfig->incomingSeqNum);
      TraceLog(DEBUG_LEVEL, "outgoingSeqNum = %d\n", venueConfig->outgoingSeqNum);
      break;
      
    case LOGON_REJECTED_TYPE:
      // We must stop here and try to connect to server again
      TraceLog(DEBUG_LEVEL, "Received logon rejected from ARCA DIRECT Server\n");
      
      TraceLog(DEBUG_LEVEL, "\t Reject Type %d\n", __bswap16(*(unsigned short *)&dataBlock->msgContent[16]));
      TraceLog(DEBUG_LEVEL, "\t Reject Text = %s\n", &dataBlock->msgContent[18]);
      return ERROR;
      
    default:
      TraceLog(WARN_LEVEL, "ARCA DIRECT - Unknown message type %d\n", dataBlock->msgContent[MSG_TYPE_INDEX]);
      break;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  BuildLogonRequestMessage
- Description:
****************************************************************************/
int BuildAndSend_ARCA_DIRECT_LogonRequestMsg(void)
{
  /*
  - API version: ArcaDirect 4.1g
  - Date updated: 03-Jan-2013
  - Modified by: Long NKH
  - Logon Messeage (and Logon Accepted), it includes the following fields:
    + Message Type              (1 byte) = 'A'
    + Variant                   (1 byte) = 1
    + Length                    (2 bytes) = 48
    + SeqNum                    (4 bytes) = null (should be ignored)
    + Last Sequence Number      (4 bytes) = Last sequence number processed
    + UserName                  (5 bytes) = ArcaDirect Login ID
    + Symbology                 (1 byte) = null (use for Options orders only.)
    + Message Version Profile   (28 bytes) = 'L',1,'D',1,'G',1,'E',1,'a',1,'2',1,'6',1,'4',1 (16 bytes)
    + Cancel On Disconnect      (1 byte binary) 0: Do not cancel when disconnect, 1: Auto Cancel when disconnect
    + Message Terminator        (1 byte) = '\n'
    => Total byte = 48
  */
  
  char message[LOGON_TYPE_MSG_LEN + 1] = "\0";
  
  t_IntConverter lastSeqNum;
  t_ShortConverter tmpLength;
  
  // Message type is 'A'
  message[0] = LOGON_TYPE;

  // Variant
  message[1] = 1;

  // Length
  tmpLength.value = LOGON_TYPE_MSG_LEN;
  message[2] = tmpLength.c[1];
  message[3] = tmpLength.c[0];
  
  // SeqNum (4 bytes)
  
  /*
  Last sequence number
  Message last sequence number should start at 0
  */
  lastSeqNum.value = venueConfig->incomingSeqNum;
  message[8] = lastSeqNum.c[3];
  message[9] = lastSeqNum.c[2];
  message[10] = lastSeqNum.c[1];
  message[11] = lastSeqNum.c[0];

  // Company Group ID (User name)
  memcpy(&message[12], venueConfig->userName, 5);

  // Symbology (1 byte)
  
  // Message Version Profile
  strncpy(&message[18], VERSION_PROFILE, 28);
  
  // Cancel On Disconnect (1 bytes binary). Should be 0 or 1
  message[46] = venueConfig->autoCancelOnDisconnect;

  // Terminator
  message[47] = '\n';
  
  // Send logon message
  return SocketSend(venueConfig->socket, message, LOGON_TYPE_MSG_LEN);
}

/****************************************************************************
- Function name:  BuildAndSend_ARCA_DIRECT_HeartbeatMsg
- Description:
****************************************************************************/
int BuildAndSend_ARCA_DIRECT_HeartbeatMsg(void)
{
  /*
  - API version: ArcaDirect 4.1g
  - Date updated: 03-Jan-2013
  - Modified by: Long NKH
  - Heartbeat Message, it includes the following fields:
    + Message Type        (1 byte) = '0'
    + Variant             (1 byte) = 1
    + Length              (2 bytes) = 12
    + SeqNum              (4 bytes) = null (should be ignored)
    + Filler              (3 byte) = null
    + Message Terminator  (1 byte) = '\n'
    => Total byte = 12
  */
  
  char message[HEARTBEAT_MSG_LEN] = "\0";
  t_ShortConverter tmpLength;
    
  // Message type is '0'
  message[0] = HEARTBEAT_TYPE;

  // Variant (1 byte) = 1
  message[1] = 1;

  // Length
  tmpLength.value = HEARTBEAT_MSG_LEN;
  message[2] = tmpLength.c[1];
  message[3] = tmpLength.c[0];

  // SeqNum (4 bytes)
  
  // Filler (3 byte)

  // Terminator
  message[11] = '\n';
  
  // Send heartbeat
  return SocketSend(venueConfig->socket, message, HEARTBEAT_MSG_LEN);
}

/****************************************************************************
- Function name:  BuildAndSend_ARCA_DIRECT_OrderMsg
- Description:
****************************************************************************/
int BuildAndSend_ARCA_DIRECT_OrderMsg(t_OrderPlacementInfo *newOrder)
{
  /*
  - API version: ArcaDirect 4.1g
  - Date updated: 03-Jan-2013
  - Modified by: Long NKH
  - New Order Message � (Variant 1: formerly V2 API � small message), it includes the following fields:
    + Message Type            (1 byte) = 'D'
    + Variant                 (1 byte) = 1
    + Length                  (2 bytes) = 76
    + Sequence Number         (4 bytes) = Client-assigned sequence number
    + Client Order Id         (4 bytes) = A client-assigned ID for this order
    + PCS Link ID             (4 bytes) = null
    + Order Quantity          (4 bytes) = The number of shares
    + Price                   (4 bytes) = The price as a long value
    + ExDestination           (2 bytes) = 102
    + Price Scale             (1 byte) = '2'
    + Symbol                  (8 bytes) = Stock symbol
    + Company Group ID        (5 bytes) = HBIWB
    + Deliver To Comp ID      (5 bytes) = null
    + SenderSubID             (5 bytes) = null
    + Execution Instructions  (1 byte) = '1'
    + Side                    (1 byte) = '1', '2', or '5'
    + Order Type              (1 byte) = '2'
    + Time In Force           (1 byte) = '0' or '3'
    + Rule80A                 (1 byte) = 'P' --> change A to P (related to ticket 1112)
    + Trading Session Id      (4 bytes) = "123"
    + Account                 (10 bytes) = null
    + ISO                     (1 byte) = 'N'
    + Extended Exec Instr     (1 byte) = null
    + ExtendedPNP             (1 byte) = null
    + NoSelfTrade             (1 byte) = 'O'
    + ProactiveIfLocked       (1 byte) = null
    + Filler                  (1 byte) = null
    + Message Terminator      (1 byte) = '\n'
    => Total byte = 76
  */
  
  t_IntConverter tmpValue;
  t_ShortConverter tmpLength;

  char *msgContent = newOrder->dataBlock.msgContent;

  // Message type is 'D'
  msgContent[0] = NEW_ORDER_TYPE;

  // Variant (1 byte) = 1
  msgContent[1] = 1;

  // Length
  tmpLength.value = NEW_ORDER_MSG_LEN;
  msgContent[2]  = tmpLength.c[1];
  msgContent[3]  = tmpLength.c[0];

  // Sequence number   (we set this later, right before sending)
  /*
  int seqNum = Get_ARCA_DIRECT_OutgoingSeqNum();
  tmpValue.value = seqNum;
  msgContent[4] = tmpValue.c[3];
  msgContent[5] = tmpValue.c[2];
  msgContent[6] = tmpValue.c[1];
  msgContent[7] = tmpValue.c[0];
  */
  
  // Client order Id
  tmpValue.value =  newOrder->orderID;
  msgContent[8] = tmpValue.c[3];
  msgContent[9] = tmpValue.c[2];
  msgContent[10] = tmpValue.c[1];
  msgContent[11] = tmpValue.c[0];
  
  // PCS Link ID
  msgContent[12] = 0;
  msgContent[13] = 0;
  msgContent[14] = 0;
  msgContent[15] = 0;

  // Order Quantity
  tmpValue.value = newOrder->shareVolume;
  msgContent[16] = tmpValue.c[3];
  msgContent[17] = tmpValue.c[2];
  msgContent[18] = tmpValue.c[1];
  msgContent[19] = tmpValue.c[0];

  // Price
  if (newOrder->side == '1')  //BUY SIDE
  {
    tmpValue.value = (int)(newOrder->sharePrice * 100.00 + 0.500001);
  }
  else
  {
    tmpValue.value = (int)(newOrder->sharePrice * 100.00 + 0.000001);
  }
  msgContent[20] = tmpValue.c[3];
  msgContent[21] = tmpValue.c[2];
  msgContent[22] = tmpValue.c[1];
  msgContent[23] = tmpValue.c[0];
  
  // ExDestination (2 bytes) = 102
  tmpLength.value = EXEXCHANGE_DESTINATION;
  msgContent[24]  = tmpLength.c[1];
  msgContent[25]  = tmpLength.c[0];

  // Price scale
  msgContent[26] = PRICE_SCALE;

  // Symbol
  strncpy(&msgContent[27], newOrder->symbol, SYMBOL_LEN);
  
  // Company group ID
  memcpy(&msgContent[35], venueConfig->compGroupID, 5);
  
  // Deliver Comp ID
  msgContent[40] = 0;
  msgContent[41] = 0;
  msgContent[42] = 0;
  msgContent[43] = 0;
  msgContent[44] = 0;
  
  // SenderSubID (5 bytes) = null
  msgContent[45] = 0;
  msgContent[46] = 0;
  msgContent[47] = 0;
  msgContent[48] = 0;
  msgContent[49] = 0;

  // Execution Instructions
  // 1: NOW - Routes
  // 6: No Other Venues - Will not route
  // M: Midpoint execution
  if (newOrder->ISO_Flag == NO && newOrder->isRoutable == YES)
  {
    msgContent[50] = '1';
  }
  else
  {
    msgContent[50] = '6';
  }

  // Side
  msgContent[51] = newOrder->side;

  // Order Type
  msgContent[52] = ORDER_TYPE_LIMIT;

  // Time In Force
  msgContent[53] = '3'; //IOC

  // Rule80A: A, P, E
  msgContent[54] = RULE80A;
  
  // Trading session Id: "1", "2", "3"
  memcpy(&msgContent[55], TRADING_SESSION_ID, 4);
  
  // Account
  strcpy(&msgContent[59], TradingAccount.ArcaDirectAccount);
  memset(&msgContent[59 + strlen(TradingAccount.ArcaDirectAccount)], 0, 10 - strlen(TradingAccount.ArcaDirectAccount));
  
  // ISO Flag
  if (newOrder->ISO_Flag == YES)
  {
    msgContent[69] = 'Y';
  }
  else
  {
    msgContent[69] = 'N';
  }
  
  // Extended Execution instruction
  msgContent[70] = 0;
  
  // ExtendedPNP (1 byte) = null
  msgContent[71] = 0;
  
  // NoSelfTrade (1 byte) = 'O'
  msgContent[72] = 'O';
  
  // ProactiveIfLocked (1 byte) = null
  msgContent[73] = 0;
  
  // Filler (1 byte)
  msgContent[74] = 0;
  
  //Terminator
  msgContent[75] = '\n';
  
  if (OrderBucketConsume() == ERROR)
  {
    TraceLog(WARN_LEVEL, "Arca Direct order is not allowed: Order bucket is currently empty\n");
    return ERROR;
  }
  
  long time = Send_ARCA_DIRECT_SequencedMsg(msgContent, NEW_ORDER_MSG_LEN);
  if (time > 0)
  {
    // Set timestamp for current rawdata
    *((long*)&newOrder->dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]) = time;
    
    DEVLOGF("Sent %c order to ARCA with id: %d for %s, %d @ $%lf\n", newOrder->side, newOrder->orderID, newOrder->symbol, newOrder->shareVolume, newOrder->sharePrice);
    
    return SUCCESS;
  }
  else
  {
    return ERROR;
  }
}

/****************************************************************************
- Function name:  ProcessARCA_DIRECT_OrderAckMsg
- Description:
****************************************************************************/
int ProcessARCA_DIRECT_OrderAckMsg(uint32_t symbolId, t_DataBlock *dataBlock)
{
  /*
  - API version: ArcaDirect 4.1g
  - Date updated: 03-Jan-2013
  - Modified by: Long NKH
  - Order Ack Message (Variant 1), it includes the following fields:
    + Message Type        1   Alpha/Numeric   �a�
    + Variant             1   Binary    Value = 1
    + Length              2   Binary    Binary length of the message. Value = 48.
    + Sequence Number     4   Binary    Arca-assigned sequence number
    + Sending Time        8   Binary    UTC Time the message was sent in microseconds since Midnight
    + Transaction Time    8   Binary    UTC Time the message was sent in microseconds since Midnight 
    + Client Order Id     4   Binary    Client order ID
    + Order ID            8   Binary    Exchange assigned Order ID
    + Price               4   Binary    Price at which the order was acked
    + Price Scale         1   Alpha/Numeric "0" through "5"
    + Liquidity           1   Alpha/Numeric null, 1 = Candidate for Liquidity Indicator �S�, 2 = Blind, 3 = Not Blind
    + Filler              5   Ignore
    + Message Terminator  1    Alpha   ASCII new line character: �\n�
    ==> Total byte        48
  */
  
  // Add data block to collection
  Add_ARCA_DIRECT_DataBlockToCollection(dataBlock);
  
  DEVLOGF("Received Order Ack from ARCA\n");
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessARCA_DIRECT_OrderFilledMsg
- Description:
****************************************************************************/
int ProcessARCA_DIRECT_OrderFilledMsg(uint32_t symbolId, t_DataBlock *dataBlock)
{
  /*
  - API version: ArcaDirect 4.1g
  - Date updated: 03-Jan-2013
  - Modified by: Long NKH
  - Order Fill Message (Variant 1), it includes the following fields:
    + Message Type              1   Alpha Numeric   �2�
    + Variant                   1   Binary    Value = 1
    + Length                    2   Binary    Binary length of the message. Value = 88.
    + Sequence Number           4   Binary    Arca-assigned sequence number
    + Sending Time              8   Binary    UTC Time the message was sent in microseconds since Midnight
    + Transaction Time          8   Binary    UTC Time the message was sent in microseconds since Midnight 
    + Client Order Id           4   Binary    Client order ID
    + Order ID                  8   Binary    Exchange assigned Order ID
    + Execution ID              8   Binary    Exchange assigned Execution ID
    + ArcaExID                  20  Alpha/Numeric Trade record
    + Last Shares or Contracts  4   Binary    Number of equity shares or option contracts filled
    + Last Price                4   Binary    Price at which the shares or contracts were filled
    + Price Scale               1   Alpha Numeric   �0� through �5�
    + Liquidity Indicator       1   Alpha Numeric
    + Side                      1   Alpha Numeric
    + LastMkt                   2   Alpha
    + Filler                    10    Ignore
    + Message Terminator        1   Alpha     ASCII new line character: �\n�
    ==>Total byte   88
  */
  
  /*
  WARNING: We must check cross trade first
  */
  
  t_ExecutionReport status;
  status.ecnID = ORDER_ARCA_DIRECT;
  status.stockSymbolIndex = symbolId;
  status.clientOrderID = __builtin_bswap32(*(unsigned int*) &(dataBlock->msgContent[24]));
  status.filledShare = __builtin_bswap32(*(unsigned int*) &(dataBlock->msgContent[64]));
    
  int tempPriceNumerator = __builtin_bswap32(*(unsigned int*) &(dataBlock->msgContent[68]));
  int tempPriceScale = dataBlock->msgContent[72];
  
  status.filledPrice = tempPriceNumerator * _DIRECT_PRICE_PORTION[tempPriceScale - 48];
    
  // Arrival time
  memcpy(&status.time, &dataBlock->addContent[DATABLOCK_OFFSET_FILTER_TIME], 8);
  
  ProcessFillResponse(&status);                                  

  *(unsigned short *)&dataBlock->addContent[DATABLOCK_OFFSET_ENTRY_EXIT_REF] = status.entryExitRefNum;

  // Add data block to collection
  Add_ARCA_DIRECT_DataBlockToCollection(dataBlock);

  DEVLOGF("Received Fill from ARCA, %d shares at $%lf\n", status.filledShare, status.filledPrice);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessARCA_DIRECT_BustOrCorrectMsg
- Description:
****************************************************************************/
int ProcessARCA_DIRECT_BustOrCorrectMsg(uint32_t symbolId, t_DataBlock *dataBlock)
{
  /*
  - API version: ArcaDirect 4.1g
  - Date updated: 03-Jan-2013
  - Modified by: Long NKH
  - Bust or Correct Message (Variant 1) , it includes the following fields:
    + Message Type            1   Alpha Numeric   �C�
    + Variant                 1   Binary    Value = 1
    + Length                  2   Binary    Binary length of the message. Value = 48.
    + Sequence Number         4   Binary    Exchange-assigned sequence number
    + Sending Time            8   Binary    UTC Time the message was sent in microseconds since Midnight
    + Transaction Time        8   Binary    UTC Time the message was sent in microseconds since Midnight 
    + Client Order Id         4   Binary    Client order ID of the order that is being busted or corrected.
    + Execution Id            8   Binary    Exchange assigned Execution ID from the order fill message (64 bit binary)
    + Order Quantity          4   Binary    Shares or contracts executed
    + Price                   4   Binary    Corrected price in a correct message
    + Price Scale             1   Alpha/Numeric �0� through �5�
    + Type                    1   Alpha/Numeric �1�=Bust  �2�=Correct 
    + Filler                  1   Ignore
    + Message Terminator      1    Alpha   ASCII new line character: �\n�
    ==>Total byte   48
  */
  
  // Add data block to collection

  Add_ARCA_DIRECT_DataBlockToCollection(dataBlock);
  
  DEVLOGF("Received Bust or Correct from ARCA\n");
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessARCA_DIRECT_OrderCancelAckMsg
- Description:
****************************************************************************/
int ProcessARCA_DIRECT_OrderCancelAckMsg(uint32_t symbolId, t_DataBlock *dataBlock)
{
  /*
  - API version: ArcaDirect 4.1g
  - Date updated: 03-Jan-2013
  - Modified by: Long NKH
  - Cancel Request Ack Message (Variant 1) , it includes the following fields:
    + Message Type        1   Alpha Numeric   '6'
    + Variant             1   Binary    Value = 1
    + Length              2   Binary    Binary length of the message. Value = 40.
    + Sequence Number     4   Binary    Exchange-assigned sequence number
    + Sending Time        8   Binary    UTC Time the message was sent in microseconds since Midnight
    + Transaction Time    8   Binary    UTC Time the message was sent in microseconds since Midnight 
    + Client Order Id     4   Binary    Client order ID of the order to be canceled.
    + Order ID            8   Binary    Exchange assigned Order ID
    + Filler              3   Ignore
    + Message Terminator  1    Alpha   ASCII new line character: �\n�
    ==> Total byte    40
  */
  
  // Add data block to collection
  
  Add_ARCA_DIRECT_DataBlockToCollection(dataBlock);
  
  DEVLOGF("Received Cancel Ack from ARCA\n");
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessARCA_DIRECT_OrderRejectedMsg
- Description:
****************************************************************************/
int ProcessARCA_DIRECT_OrderRejectedMsg(uint32_t symbolId, t_DataBlock *dataBlock)
{
  /*
  - API version: ArcaDirect 4.1g
  - Date updated: 03-Jan-2013
  - Modified by: Long NKH
  - Order Cancel/Replace Reject Message (Variant 1), it includes the following fields:
    + Message Type              1   Alpha Numeric   �8�
    + Variant                   1   Binary    Value = 1
    + Length                    2   Binary    Binary length of the message. Value = 80.
    + Sequence Number           4   Binary    Exchange-assigned sequence number
    + Sending Time              8   Binary    UTC Time the message was sent in microseconds since Midnight
    + Transaction Time          8   Binary    UTC Time the message was sent in microseconds since Midnight 
    + Client Order Id           4   Binary    Client order id of the order, cancel, or cancel replace that was sent.
    + Original Client Order ID  4   Binary    ID of original order
    + Rejected Message Type     1   Numeric   �1�=order reject  �2�=cancel reject  �3�=cancel replace reject
    + Text                      40  Alpha     Reason for the rejection
    + Reject Reason             1   Alpha     Fix Tag 102 or 103
    + Filler                    5   Ignore
    + Message Terminator        1   Alpha     ASCII new line character: �\n�
    ==> Total byte      80
  */
  
  /*
  WARNING: We must check cross trade first
  */
    
  t_ExecutionReport status;
  status.ecnID = ORDER_ARCA_DIRECT;
  status.stockSymbolIndex = symbolId;
  status.clientOrderID = __builtin_bswap32(*(unsigned int*) &(dataBlock->msgContent[24]) );
  
  // Arrival time
  memcpy(&status.time, &dataBlock->addContent[DATABLOCK_OFFSET_FILTER_TIME], 8);
  
  ProcessRejectResponse(&status);                                   
  
  *(unsigned short *)&dataBlock->addContent[DATABLOCK_OFFSET_ENTRY_EXIT_REF] = status.entryExitRefNum;
  
  // Add data block to collection
  Add_ARCA_DIRECT_DataBlockToCollection(dataBlock);
  
  //Check reject text to halt symbol
  if ((strncmp(&dataBlock->msgContent[33], "Price too far outside", 21) == 0) ||
      (strncmp(&dataBlock->msgContent[33], "Mkt/Symbol not open", 19) == 0) ||
      (strncmp(&dataBlock->msgContent[33], "Symbol closed", 13) == 0))
  {
    long nsec;
    memcpy(&nsec, &dataBlock->addContent[DATABLOCK_OFFSET_FILTER_TIME], 8);
    UpdateHaltStatusForASymbol(symbolId, TRADING_HALTED, nsec / TEN_RAISE_TO_9, BOOK_ARCA);
  }
  
  DEVLOGF("Received Reject from ARCA, reason: %s\n", &dataBlock->msgContent[33]);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessARCA_DIRECT_OrderKilledMsg
- Description:
****************************************************************************/
int ProcessARCA_DIRECT_OrderKilledMsg(uint32_t symbolId, t_DataBlock *dataBlock)
{
  /*
  - API version: ArcaDirect 4.1g
  - Date updated: 03-Jan-2013
  - Modified by: Long NKH
  - Order Killed Message (Variant 1), it includes the following fields:
    + Message Type            1   Alpha Numeric   �4�
    + Variant                 1   Binary    Value = 1
    + Length                  2   Binary    Binary length of the message. Value = 40.
    + Sequence Number         4   Binary    Exchange-assigned sequence number
    + Sending Time            8   Binary    UTC Time the message was sent in microseconds since Midnight
    + Transaction Time        8   Binary    UTC Time the message was sent in microseconds since Midnight 
    + Client Order Id         4   Binary    Client order ID of the canceled order.
    + Order ID                8   Binary    Exchange assigned Order ID
    + Information Text        1   Binary    Indicates whether the kill was initiated by the user or by exchange rules.
    + Filler                  2   Ignore
    + Message Terminator      1   Alpha     ASCII new line character: �\n�
    ==>Total byte   40
  */
  
  /*
  WARNING: We must check cross trade first
  */
  
  t_ExecutionReport status;
  status.ecnID = ORDER_ARCA_DIRECT;
  status.stockSymbolIndex = symbolId;
  status.clientOrderID = __builtin_bswap32(*(unsigned int*) &(dataBlock->msgContent[24]) );
  
  // Arrival time
  memcpy(&status.time, &dataBlock->addContent[DATABLOCK_OFFSET_FILTER_TIME], 8);
  
  // Cancelled Shares
  t_OpenOrder *openOrder = &TradingCollection[symbolId].openOrders[status.clientOrderID % MAX_OPEN_ORDERS];
  if(openOrder->orderID != status.clientOrderID)
  {
    TraceLog(ERROR_LEVEL, "ProcessARCA_DIRECT_OrderKilledMsg: Received cancel response for unknown order id: %d\n", status.clientOrderID);
    return ERROR;
  }               
  
  status.canceledShare = openOrder->shareVolume;
  
  // Process Canceled status
  ProcessCancelResponse(&status);

  *(unsigned short *)&dataBlock->addContent[DATABLOCK_OFFSET_ENTRY_EXIT_REF] = status.entryExitRefNum;
  
  // Add data block to collection
  Add_ARCA_DIRECT_DataBlockToCollection(dataBlock);

  DEVLOGF("Received Order Cancelled from ARCA for order id: %d, cancelled shares: %d\n", status.clientOrderID, status.canceledShare);

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessARCA_DIRECT_LogonAcceptedMsg
- Description:
****************************************************************************/
int ProcessARCA_DIRECT_LogonAcceptedMsg(t_DataBlock *dataBlock)
{
  TraceLog(DEBUG_LEVEL, "Logon to ARCA DIRECT was successful\n");
        
  /*
  Update Outgoing Sequence Number before send any messages to ARCA DIRECT Server
  */
  venueConfig->outgoingSeqNum = __builtin_bswap32(*(unsigned int*)&dataBlock->msgContent[OUTGOING_SEQ_NUM_INDEX]);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  Send_ARCA_DIRECT_SequencedMsg
****************************************************************************/
long Send_ARCA_DIRECT_SequencedMsg(char *message, int msgLen)
{
  pthread_spin_lock(ADNewOrderLock);
    *((int*)&message[4]) = __builtin_bswap32(venueConfig->outgoingSeqNum++);
    long time = AsyncSend(venueConfig->sessionId, message, msgLen);
  pthread_spin_unlock(ADNewOrderLock);
  
  return time;
}

/****************************************************************************
- Function name:  Add_ARCA_DIRECT_DataBlockToCollection
- Description:
****************************************************************************/
void Add_ARCA_DIRECT_DataBlockToCollection(t_DataBlock *dataBlock)
{
  // Update Block Length
  dataBlock->blockLen = 12 + DATABLOCK_ADDITIONAL_INFO_LEN + dataBlock->msgLen;
  
  // Add data block to collection
  AddDataBlockToQueue(ORDER_ARCA_DIRECT, dataBlock);
}
