/****************************************************************************
** Project name:  Equity Arbitrage Application
** File name:   nasdaq_rash_proc.c
** Description:   This file contains function definitions that were declared
        in nasdaq_rash_proc.h

** Author:    Sang Nguyen-Minh
** First created on 26 September 2007
** Last updated on 26 September 2007
****************************************************************************/

/****************************************************************************
** Include files and define several variables
****************************************************************************/
#define _ISOC9X_SOURCE
#define _GNU_SOURCE

#include "nasdaq_rash_proc.h"
#include "trading_mgmt.h"
#include "kernel_algorithm.h"
#include "network.h"   
#include "logging.h"

static t_OecConfig *venueConfig = &OecConfig[ORDER_NASDAQ_RASH];

/****************************************************************************
** Function definitions
****************************************************************************/

/****************************************************************************
- Function name:  Initialize_NASDAQ_RASH
****************************************************************************/
void Initialize_NASDAQ_RASH(void)
{
  venueConfig->socket = -1;
}

/****************************************************************************
- Function name:  Connect_NASDAQ_RASH
****************************************************************************/
int Connect_NASDAQ_RASH()
{
  // Initialize data structure 
  Initialize_NASDAQ_RASH();
  
  // Load incoming sequence number from file
  if (LoadSequenceNumber(FN_SEQUENCE_NUM_RASH, ORDER_NASDAQ_RASH) == ERROR)
  {
    OrderStatusMgmt[ORDER_NASDAQ_RASH].shouldConnect = NO;
    return ERROR;
  }
  
  TraceLog(DEBUG_LEVEL, "Trying connect to NASDAQ RASH Server Ip (%s) Port (%d)\n", venueConfig->ipAddress, venueConfig->port);

  // Connect to server
  venueConfig->socket = connectSession(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, venueConfig->sessionId);
  
  if (venueConfig->socket == -1)
  {
    OrderStatusMgmt[ORDER_NASDAQ_RASH].shouldConnect = NO;
    TraceLog(ERROR_LEVEL, "Could not connect to NASDAQ RASH Server\n");
    return ERROR;
  }
  
  if (SetSocketSndTimeout(venueConfig->socket, 1, 0) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Could not set socket send-timeout for RASH socket\n");
    disconnectSession(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, venueConfig->sessionId);
    OrderStatusMgmt[ORDER_NASDAQ_RASH].shouldConnect = NO;
    return ERROR;
  }
  
  /* Disable the Nagle (TCP No Delay) algorithm */
  if (DisableNagleAlgo(venueConfig->socket) == -1)
  {
    TraceLog(ERROR_LEVEL, "Could not set TCP_NODELAY for RASH socket\n");
    disconnectSession(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, venueConfig->sessionId);
    OrderStatusMgmt[ORDER_NASDAQ_RASH].shouldConnect = NO;
    return ERROR;
  }
  
  // Send Logon message to server
  if (BuildAndSend_NASDAQ_RASH_LogonRequest() == ERROR)
  {
    TraceLog(ERROR_LEVEL, "RASH: Could not send Login msg to server\n");
    OrderStatusMgmt[ORDER_NASDAQ_RASH].shouldConnect = NO;
    return ERROR;
  }             
  
  return SUCCESS;
}

int Process_NASDAQ_RASH_DataBlock(uint32_t symbolId, t_DataBlock *dataBlock) {  
  int packetType = dataBlock->msgContent[0];   
  if (packetType == 'S')
  {
    int msgType = dataBlock->msgContent[9];

    switch (msgType)
    {
      case NASDAQ_RASH_ORDER_ACCEPTED: 
        __sync_add_and_fetch(&venueConfig->incomingSeqNum, 1);
        ProcessNASDAQ_RASH_OrderAcceptedMsg(symbolId, dataBlock);
        break;

      case NASDAQ_RASH_ORDER_ACCEPTED_WITH_CROSS:
        __sync_add_and_fetch(&venueConfig->incomingSeqNum, 1);
        ProcessNASDAQ_RASH_OrderAcceptedMsg(symbolId, dataBlock);
        break;                                         

      case NASDAQ_RASH_ORDER_EXECUTED:
        __sync_add_and_fetch(&venueConfig->incomingSeqNum, 1);
        ProcessNASDAQ_RASH_OrderExecutedMsg(symbolId, dataBlock);
        break;

      case NASDAQ_RASH_ORDER_CANCELED:
        __sync_add_and_fetch(&venueConfig->incomingSeqNum, 1);
        ProcessNASDAQ_RASH_OrderCanceledMsg(symbolId, dataBlock);
        break;

      case NASDAQ_RASH_ORDER_REJECTED:
        __sync_add_and_fetch(&venueConfig->incomingSeqNum, 1);
        ProcessNASDAQ_RASH_OrderRejectedMsg(symbolId, dataBlock);
        break;

      case NASDAQ_RASH_BROKEN_TRADE:
        __sync_add_and_fetch(&venueConfig->incomingSeqNum, 1);
        Add_NASDAQ_RASH_DataBlockToCollection(dataBlock);
        break;

      case NASDAQ_RASH_SYSTEM_EVENT:
        __sync_add_and_fetch(&venueConfig->incomingSeqNum, 1);
        Add_NASDAQ_RASH_DataBlockToCollection(dataBlock);
        break;

      default:
        __sync_add_and_fetch(&venueConfig->incomingSeqNum, 1);
        Add_NASDAQ_RASH_DataBlockToCollection(dataBlock);
        TraceLog(ERROR_LEVEL, "NASDAQ RASH: received unknown message type: %d - %c\n", msgType, msgType);
        break;    
    } 
  }
  else
  {
    switch (packetType)
    {
      case NASDAQ_RASH_HEARTBEAT:
        if (BuildAndSend_NASDAQ_RASH_Heartbeat() == ERROR)
        {
          return ERROR;
        }
        break;

      case NASDAQ_RASH_DEBUG:
        TraceLog(DEBUG_LEVEL, "Received debug message from NASDAQ RASH Server\n");
        break;

      case NASDAQ_RASH_LOGIN_ACCEPTED:
        // Update flag to indicate that we connected to server successfully
        TraceLog(DEBUG_LEVEL, "Received logon accepted message from NASDAQ RASH Server\n");
        OrderStatusMgmt[ORDER_NASDAQ_RASH].isConnected = CONNECTED;
        break;

      case NASDAQ_RASH_LOGIN_REJECTED:
        // Update flag to indicate that we cannot connect to server
        TraceLog(DEBUG_LEVEL, "Received logon rejected message from NASDAQ RASH Server\n");
        return ERROR; 

      case NASDAQ_RASH_END_OF_SESSION:
        TraceLog(DEBUG_LEVEL, "Received end of session message from NASDAQ RASH Server\n");
        return ERROR;

      default:
        TraceLog(DEBUG_LEVEL, "NASDAQ RASH Order: Received unknown packet type: %d - %c\n", packetType, packetType); 
        return ERROR;
        break;
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  BuildAndSend_NASDAQ_RASH_LogonRequest
****************************************************************************/
int BuildAndSend_NASDAQ_RASH_LogonRequest(void)
{
  char message[38];
  
  int currentIndex;
  
  // len is used to store the length of various strings
  int len = 0;
  
  /*
  Both fields username and password are insensitive, 
  and must be filled with spaces on the right
  */
  memset(message, ' ', 38);
  
  // First character is the message type. 'L' means login request
  message[0] = 'L';
  
  /*
  Next is the 6 bytes which store the username
  We assume the length of the username is OK
  */
  len = strlen(venueConfig->userName);

  for (currentIndex = 0; currentIndex < len; currentIndex++)  
  {
    if (currentIndex < 6)
    {
      message[1 + currentIndex] = venueConfig->userName[currentIndex];
    }
  }
  
  /*
  Next is the 10 bytes which store the password
  We assume the length of the password is OK
  */
  len = strlen(venueConfig->password);

  for (currentIndex = 0; currentIndex < len; currentIndex++)
  {
    if (currentIndex < 10)
    {
      message[7 + currentIndex] = venueConfig->password[currentIndex];
    }
  }

  // All zero will do me good
  memset(&message[27], ' ', 10);

  char temp[10];

  sprintf(temp, "%10d", venueConfig->incomingSeqNum);
  
  memcpy(&message[27], temp, 10); 

  // Terminating linefeed
  message[37] = 10;
  
  return SocketSend(venueConfig->socket, message, 38);  
}

/****************************************************************************
- Function name:  BuildAndSend_NASDAQ_RASH_OrderMsg
****************************************************************************/
int BuildAndSend_NASDAQ_RASH_OrderMsg(t_OrderPlacementInfo *newOrder)
{
  //t_DataBlock dataBlock;
  
  //memset(dataBlock.msgContent, ' ', NASDAQ_RASH_NEW_ORDER_MSG_LEN);
  
  // Unsequence data packet
  char *msgContent = newOrder->dataBlock.msgContent;
  msgContent[0] = 'U';
  
  // Order message type
  msgContent[1] = 'O';
  
  // Order token (2 digit account + orderID --> 14 char)
  msgContent[2] = TradingAccount.RASHAccount[0];
  msgContent[3] = TradingAccount.RASHAccount[1];
  Lrc_itoafl(newOrder->orderID, 32, 12, &msgContent[4]);
  
  // Buy/Sell indicator 
  msgContent[16] = newOrder->side;
  
  // Share    
  Lrc_itoaf_stripped(newOrder->shareVolume, '0', 6, &msgContent[17]);
  
  // Stock
  //memset(&msgContent[23], 32, SYMBOL_LEN);
  //strncpy(&msgContent[23], newOrder->symbol, strnlen(newOrder->symbol, SYMBOL_LEN));
  strcncpy((unsigned char *)&msgContent[23], newOrder->symbol, 32, SYMBOL_LEN);
  
  // Price
  if (fge(newOrder->sharePrice, 1.00))
  {
    if (newOrder->side == 'B')  //BUY SIDE
    {
      Lrc_itoaf_stripped((int)(newOrder->sharePrice * 100.00 + 0.500001) * 100, '0', 10, &msgContent[31]);
    }
    else
    {
      Lrc_itoaf_stripped((int)(newOrder->sharePrice * 100.00 + 0.000001) * 100, '0', 10, &msgContent[31]);
    }
  }
  else
  {
    Lrc_itoaf_stripped((int)(newOrder->sharePrice * 10000.00 + 0.500001), '0', 10, &msgContent[31]);
  }
  
  // Time in Force  (IOC: "00000")
  msgContent[41] = '0';
  msgContent[42] = '0';
  msgContent[43] = '0';
  msgContent[44] = '0';
  msgContent[45] = '0';

  // Firm
  strncpy(&msgContent[46], MPID_NASDAQ_RASH, 4);

  // Display
  msgContent[50] = 'Y';
  
  /***************************************************************
  - MinQty: Minimum Fill Amount Allowed. 
  If a non-zero value is put in this field, TIF must be IOC (or 0)
  ***************************************************************/
  memset(&msgContent[51], '0', 6);

  /***************************************************************
  - Max Floor: Shares to Display. If zero this field will default 
  to the order qty. Use the display field to specify a hidden order.
  ***************************************************************/
  memset(&msgContent[57], '0', 6);
  
  /***************************************************************
  - Peg Type: N  No Peg
        P  Market
        R  Primary
  ***************************************************************/
  msgContent[63] = 'N';

  /***************************************************************
  - Peg Difference Sign:  +
              -
  If peg type is set to N, specify +
  ***************************************************************/
  msgContent[64] = '+';
  
  /***************************************************************
  - Peg Difference: Amount; 6.4 (implied decimal). If peg type is 
  set to N, specify 0
  ***************************************************************/
  memset(&msgContent[65], '0', 10);
  
  /***************************************************************
  - Discretion Price: Discretion Price for Discretionary Order. 
  If set to 0, then this order does not have discretion.
  ***************************************************************/
  memset(&msgContent[75], '0', 10);
  
  /***************************************************************
  - Discretion Peg Type:  N  No Peg
              P  Market
              R  Primary
  ***************************************************************/
  msgContent[85] = 'N';

  /***************************************************************
  - Discretion Peg Difference Sign: +
                    -
  If peg type is set to N, specify +
  ***************************************************************/
  msgContent[86] = '+';
  
  /***************************************************************
  - Discretion Peg Difference: Amount; 6.4 (implied decimal). 
  If peg type is set to N, specify 0
  ***************************************************************/ 
  memset(&msgContent[87], '0', 10);

  /***************************************************************
  - Capacity/Rule 80A Indicator: Capacity Code
  ***************************************************************/
  msgContent[97] = 'P';

  /***************************************************************
  - Random Reserve: Shares to do random reserve with
  ***************************************************************/
  memset(&msgContent[98], '0', 6);

  /***************************************************************
  - Route Dest / Exec Broker: Target ID (INET, DOTN, DOTA, DOTM, 
  DOTP, STGY and SCAN)
  ***************************************************************/
  memcpy(&msgContent[104], "SCAN", 4);

  /***************************************************************
  - Cust / Terminal ID / Sender SubID: Client Initiated; Pass-thru. 
  Must be left justified and may not start with a space.
  ***************************************************************/
  memset(&msgContent[108], ' ', 32);

  // Terminating linefeed
  msgContent[140] = 10;
  
  int lenMsg = NASDAQ_RASH_NEW_ORDER_MSG_LEN;
  if (newOrder->ISO_Flag == YES)
  {
    // Order message type
    msgContent[1] = 'Q';
    
    if (newOrder->ISO_Directed_Flag >= FIRST_AWAY_EXCHANGE)
    {
      /***************************************************************
      - Route Dest / Exec Broker: Target ID (INET, DOTN, DOTA, DOTM, DOTP, STGY, SCAN and Directed ISO)
      Directed Order Destination Codes 
      Market Center   Destination Code
      AMEX      ISAM      
      ARCA/PCX  ISPA      
      BOSX      ISBX      
      CBOE      ISCB      
      CHSX      ISCX      
      CINN/NSX  ISCN      
      DATA      ISDA      
      EDGA      ISNA      
      EDGX      ISNX      
      ISE       ISIS      
      LavaFlow  ISLF      
      NYSE      ISNY      
      PHLX      ISPX      
      TRAC      ISTR      
      BATS      ISBZ      
      ***************************************************************/
      switch (newOrder->ISO_Directed_Flag)
      {
        case BOOK_AMEX:
          memcpy(&msgContent[104], "ISAM", 4);
          TraceLog(ERROR_LEVEL, "Attempting to send RASH directed ISO to AMEX\n");
          break;
        case National_Stock_Exchange:
          memcpy(&msgContent[104], "ISCN", 4);
          break;
        case International_Securities_Exchange:
          memcpy(&msgContent[104], "ISIS", 4);
          break;
        case Chicago_Stock_Exchange:
          memcpy(&msgContent[104], "ISCX", 4);
          break;
        case Chicago_Board_Options_Exchange:
          memcpy(&msgContent[104], "ISCB", 4);
          break;
        case BOOK_PSX:
          TraceLog(ERROR_LEVEL, "Attempting to send RASH directed ISO to PSX\n");
          memcpy(&msgContent[104], "ISPX", 4);
          break;
        case FINRA_ADF:
          memcpy(&msgContent[104], "ISLF", 4);
          break;
        case BOOK_BYX:
          TraceLog(ERROR_LEVEL, "Attempting to send RASH directed ISO to BYX\n");
          memcpy(&msgContent[104], "ISBY", 4);
          break;
        default: 
          TraceLog(ERROR_LEVEL, "Unknown RASH directed ISO destination: %d.  Defaulting to 'MOPP'\n", newOrder->ISO_Directed_Flag);
          break;
      }
    }
    
    // Inter-market sweep eligibility
    msgContent[140] = 'Y';
    
    // Cross type
    /*
    O  opening cross 
    C  closing cross 
    I  Intraday cross 
    N  order is immediately live 
    (dont wait for a cross)
    */
    msgContent[141] = 'N';
    
    // Terminating linefeed
    msgContent[142] = 10;
    
    lenMsg = NASDAQ_RASH_NEW_ORDER_CROSS_MSG_LEN;
  }

  if (OrderBucketConsume() == ERROR)
  {
    TraceLog(WARN_LEVEL, "RASH order is not allowed: Order bucket is currently empty\n");
    return ERROR;
  }
  
  // Send Enter Order Message
  long time = AsyncSend(venueConfig->sessionId, msgContent, lenMsg);
  if (time > 0)
  {
    // Set timestamp for current rawdata
    *((long*)&newOrder->dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]) = time;
    newOrder->ecnToPlaceID = ORDER_NASDAQ_RASH;
    
    DEVLOGF("Sent %c order to RASH with id: %d for %s, %d @ $%lf\n", newOrder->side, newOrder->orderID, newOrder->symbol, newOrder->shareVolume, newOrder->sharePrice);
    
    return SUCCESS;
  }
  else
  {
    return ERROR;
  }
}

/****************************************************************************
- Function name:  BuildAndSend_NASDAQ_RASH_Heartbeat
****************************************************************************/
int BuildAndSend_NASDAQ_RASH_Heartbeat(void)
{
  char message[2];
  message[0] = 'R';
  message[1] = 10;
  
  return SocketSend(venueConfig->socket, message, 2);
}

/****************************************************************************
- Function name:  ProcessNASDAQ_RASH_OrderAcceptedMsg
****************************************************************************/
int ProcessNASDAQ_RASH_OrderAcceptedMsg(uint32_t symbolId, t_DataBlock *dataBlock)
{
  /*
  WARNING: We must check cross trade first
  */
  
  char tempBuffer[16] = "\0";
  
  t_ExecutionReport status;
  status.stockSymbolIndex = symbolId;
  
  // Client Order ID (Token, ignore first 2 char: account)
  memcpy(tempBuffer, &dataBlock->msgContent[12], 12);
  tempBuffer[12] = 0;
  status.clientOrderID = atoi(tempBuffer);
  
  
  // Accepted share
  memcpy(tempBuffer, &dataBlock->msgContent[25], 6);
  tempBuffer[6] = 0;
  status.acceptedShare = atoi(tempBuffer);

  // Accepted price
  memcpy(tempBuffer, &dataBlock->msgContent[39], 10);
  tempBuffer[10] = 0;
  status.acceptedPrice = atoi(tempBuffer) * 0.0001;
  
  // Arrival Time
  memcpy(&status.time, &dataBlock->addContent[DATABLOCK_OFFSET_FILTER_TIME], 8);
      
  ProcessAcceptResponse(&status);
  
  Add_NASDAQ_RASH_DataBlockToCollection(dataBlock);
  
  DEVLOGF("Received Order Ack from RASH\n");
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessNASDAQ_RASH_OrderExecutedMsg
****************************************************************************/
int ProcessNASDAQ_RASH_OrderExecutedMsg(uint32_t symbolId, t_DataBlock *dataBlock)
{
  /*
  WARNING: We must check cross trade first
  */
  
  char tempBuffer[16] = "\0";
  
  t_ExecutionReport status;
  status.ecnID = ORDER_NASDAQ_RASH;
  status.stockSymbolIndex = symbolId;
  
  // Client Order ID (Token, ignore first 2 chars: account)
  memcpy(tempBuffer, &dataBlock->msgContent[12], 12);
  tempBuffer[12] = 0;
  status.clientOrderID = atoi(tempBuffer);
    
  // Filled share
  memcpy(tempBuffer, &dataBlock->msgContent[24], 6);
  tempBuffer[6] = 0;
  status.filledShare = atoi(tempBuffer);
  
  // Filled price
  memcpy(tempBuffer, &dataBlock->msgContent[30], 10);
  tempBuffer[10] = 0;
  status.filledPrice = atoi(tempBuffer) * 0.0001;
      
  // Arrival Time
  memcpy(&status.time, &dataBlock->addContent[DATABLOCK_OFFSET_FILTER_TIME], 8);
  
  ProcessFillResponse(&status);
  
  *(unsigned short *)&dataBlock->addContent[DATABLOCK_OFFSET_ENTRY_EXIT_REF] = status.entryExitRefNum;
  
  Add_NASDAQ_RASH_DataBlockToCollection(dataBlock);
  
  DEVLOGF("Received Fill from RASH, %d shares at $%lf\n", status.filledShare, status.filledPrice);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessNASDAQ_RASH_OrderCanceledMsg
****************************************************************************/
int ProcessNASDAQ_RASH_OrderCanceledMsg(uint32_t symbolId, t_DataBlock *dataBlock)
{
  /*
  WARNING: We must check cross trade first
  */
  
  char tempBuffer[16] = "\0";
  
  t_ExecutionReport status;
  status.ecnID = ORDER_NASDAQ_RASH;
  status.stockSymbolIndex = symbolId;
  
  // Client Order ID (Token, ignore first 2 chars: account)
  memcpy(tempBuffer, &dataBlock->msgContent[12], 12);
  tempBuffer[12] = 0;
  status.clientOrderID = atoi(tempBuffer);
    
  // Canceled share
  memcpy(tempBuffer, &dataBlock->msgContent[24], 6);
  tempBuffer[6] = 0;
  status.canceledShare = atoi(tempBuffer);
      
  // Arrival Time
  memcpy(&status.time, &dataBlock->addContent[DATABLOCK_OFFSET_FILTER_TIME], 8);

  ProcessCancelResponse(&status);
  
  *(unsigned short *)&dataBlock->addContent[DATABLOCK_OFFSET_ENTRY_EXIT_REF] = status.entryExitRefNum;
  
  Add_NASDAQ_RASH_DataBlockToCollection(dataBlock);
  
  DEVLOGF("Received Cancel from RASH for order id: %d, cancelled shares: %d\n", status.clientOrderID, status.canceledShare);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessNASDAQ_RASH_OrderRejectedMsg
****************************************************************************/
int ProcessNASDAQ_RASH_OrderRejectedMsg(uint32_t symbolId, t_DataBlock *dataBlock)
{
  /*
  WARNING: We must check cross trade first
  */
  char tempBuffer[16] = "\0";
  
  t_ExecutionReport status;
  status.ecnID = ORDER_NASDAQ_RASH;
  status.stockSymbolIndex = symbolId;
  
  // Client Order ID (Token)
  memcpy(tempBuffer, &dataBlock->msgContent[12], 12);
  tempBuffer[12] = 0;
  status.clientOrderID = atoi(tempBuffer);
        
  // Arrival Time
  memcpy(&status.time, &dataBlock->addContent[DATABLOCK_OFFSET_FILTER_TIME], 8);
  
  ProcessRejectResponse(&status);
  
  *(unsigned short *)&dataBlock->addContent[DATABLOCK_OFFSET_ENTRY_EXIT_REF] = status.entryExitRefNum;
  
  Add_NASDAQ_RASH_DataBlockToCollection(dataBlock);
  
  //Check to halt symbol
  switch (dataBlock->msgContent[24])
  {
    case 'H': // 'Halt', we will halt on all venues
      {
        long nsec;
        memcpy(&nsec, &dataBlock->addContent[DATABLOCK_OFFSET_FILTER_TIME], 8);
        
        UpdateHaltStatusForASymbol(symbolId, TRADING_HALTED, nsec / TEN_RAISE_TO_9, MAX_ECN_BOOK);
      }
      break;
    case 'X': // 'Invalid price', we will halt on NASDAQ only
      {
        long nsec;
        memcpy(&nsec, &dataBlock->addContent[DATABLOCK_OFFSET_FILTER_TIME], 8);
        
        UpdateHaltStatusForASymbol(symbolId, TRADING_HALTED, nsec / TEN_RAISE_TO_9, BOOK_NASDAQ);
      }
      break;
  }
  
  DEVLOGF("Received Reject from RASH, reason: %c\n", dataBlock->msgContent[24]);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  Add_NASDAQ_RASH_DataBlockToCollection
****************************************************************************/
void Add_NASDAQ_RASH_DataBlockToCollection(t_DataBlock *dataBlock)
{
  dataBlock->blockLen = 12 + DATABLOCK_ADDITIONAL_INFO_LEN + dataBlock->msgLen;
  AddDataBlockToQueue(ORDER_NASDAQ_RASH, dataBlock);
}
