/****************************************************************************
** Project name:  Equity Arbitrage Application
** File name:     configuration.c
** Description:   This file contains function definitions that were declared
        in configuration.h
** Author:    Sang Nguyen-Minh
** First created on 13 September 2007
** Last updated on 26 September 2007
****************************************************************************/
#define _GNU_SOURCE

#include <string.h>
#include <ctype.h>
     
#include "configuration.h"
#include "nasdaq_book_proc.h"
#include "arca_book_proc.h"
#include "nyse_book_proc.h"
#include "bats_book_proc.h"
#include "edge_book_proc.h"
#include "trading_mgmt.h"
#include "hbitime.h"
#include "parameters_proc.h"

/****************************************************************************
              GLOBAL VARIABLES
****************************************************************************/
t_Algorithm_Config AlgorithmConfig;

t_BookStatus CQS_StatusMgmt;
t_CQS_Conf CQS_Conf;

t_BookStatus UQDF_StatusMgmt;
t_UQDF_Conf UQDF_Conf;

t_OecConfig OecConfig[MAX_ECN_ORDER];

t_TradingAccount TradingAccount;
t_Min_Spread_Conf MinSpreadConf;

int ISO_Trading = DISABLE;
char BookISO_Trading[MAX_ECN_BOOK];
char BookPrePostISOTrading[MAX_ECN_BOOK];

double MinSpreadInSecond[MAX_MIN_SPREAD_OF_DAY];

// Flags indicating wether configurations were taken from command line input
int IsGottenOrderConfig[MAX_ECN_ORDER];
int IsGottenBookConfig[MAX_ECN_BOOK];
int IsGottenFeedConfig[MAX_BBO_SOURCE];
int IsGottenASConfig;
int IsGottenTradingAccountConfig;
int ManualMode;

extern double SecFeeAndCommissionInCent;

t_SymbolSplitInfo SymbolSplitInfo[MAX_SYMBOL_SPLIT_RANGE_ID];

t_TradingSchedule TradingSchedule;

int NextSessionId = 256; // High enough so it doesn't overlap marketdata feed ids

t_FilterBuffers FilterBuffers[DBLFILTER_NUM_QUEUES+1];
long DataDelayThreshold[MAX_ECN_BOOK];
int TotalTimeOffsetInSeconds; // Determine the total seconds adjust between UTC and EST time zone

volatile char MarketDataDeviceInitialized = NO;

/****************************************************************************
** Function definitions
****************************************************************************/
static inline int GetAddressFromConfig(char *line, char *ip, int *port);

/****************************************************************************
- Function name:  LoadAllOrdersConfig
****************************************************************************/
int LoadAllOrdersConfig(void)
{
  if (IsGottenOrderConfig[ORDER_ARCA_DIRECT] == NO)
  {
    //if ARCA DIRECT config hasn't parsed from command line successfully, load it from file
    TraceLog(DEBUG_LEVEL, "Loading ARCA DIRECT config from file\n");
    EnumOecConfValues arcaValues[] = {OEC_CONF_FIELD_IP, OEC_CONF_FIELD_PORT, OEC_CONF_FIELD_USERNAME, OEC_CONF_FIELD_PASSWORD, OEC_CONF_FIELD_COMPID, OEC_CONF_FIELD_AUTO_CANCEL};

    if (LoadOecConfigFromFile(FN_OEC_CONF_ARCA, &OecConfig[ORDER_ARCA_DIRECT], arcaValues, 6) == ERROR)
    {
      return ERROR;
    }
  }
  
  if (IsGottenOrderConfig[ORDER_NASDAQ_OUCH] == NO)
  {
    TraceLog(DEBUG_LEVEL, "Loading NASDAQ OUCH config from file\n");
    EnumOecConfValues ouchValues[] = {OEC_CONF_FIELD_IP, OEC_CONF_FIELD_PORT, OEC_CONF_FIELD_USERNAME, OEC_CONF_FIELD_PASSWORD, OEC_CONF_FIELD_PROTOCOL};
    if (LoadOecConfigFromFile(FN_OEC_CONF_OUCH, &OecConfig[ORDER_NASDAQ_OUCH], ouchValues, 5) == ERROR)
    {
      return ERROR;
    }
  }
  
  if (IsGottenOrderConfig[ORDER_NASDAQ_BX] == NO)
  {
    TraceLog(DEBUG_LEVEL, "Loading NASDAQ BX config from file\n");
    EnumOecConfValues bxValues[] = {OEC_CONF_FIELD_IP, OEC_CONF_FIELD_PORT, OEC_CONF_FIELD_USERNAME, OEC_CONF_FIELD_PASSWORD, OEC_CONF_FIELD_PROTOCOL};
    if (LoadOecConfigFromFile(FN_OEC_CONF_BX, &OecConfig[ORDER_NASDAQ_BX], bxValues, 5) == ERROR)
    {
      return ERROR;
    }
  }
  
  if (IsGottenOrderConfig[ORDER_NASDAQ_PSX] == NO)
  {
    TraceLog(DEBUG_LEVEL, "Loading NASDAQ PSX config from file\n");
    EnumOecConfValues psxValues[] = {OEC_CONF_FIELD_IP, OEC_CONF_FIELD_PORT, OEC_CONF_FIELD_USERNAME, OEC_CONF_FIELD_PASSWORD, OEC_CONF_FIELD_PROTOCOL};
    if (LoadOecConfigFromFile(FN_OEC_CONF_PSX, &OecConfig[ORDER_NASDAQ_PSX], psxValues, 5) == ERROR)
    {
      return ERROR;
    }
  }
  
  if (IsGottenOrderConfig[ORDER_NASDAQ_RASH] == NO)
  {
    TraceLog(DEBUG_LEVEL, "Loading NASDAQ RASH config from file\n");
    EnumOecConfValues rashValues[] = {OEC_CONF_FIELD_IP, OEC_CONF_FIELD_PORT, OEC_CONF_FIELD_USERNAME, OEC_CONF_FIELD_PASSWORD};
    if (LoadOecConfigFromFile(FN_OEC_CONF_RASH, &OecConfig[ORDER_NASDAQ_RASH], rashValues, 4) == ERROR)
    {
      return ERROR;
    }
  }

  if (IsGottenOrderConfig[ORDER_NYSE_CCG] == NO)
  {
    TraceLog(DEBUG_LEVEL, "Loading NYSE CCG config from file\n");
    EnumOecConfValues nyseValues[] = {OEC_CONF_FIELD_IP, OEC_CONF_FIELD_PORT, OEC_CONF_FIELD_USERNAME, OEC_CONF_FIELD_PASSWORD, OEC_CONF_FIELD_AUTO_CANCEL};
    if (LoadOecConfigFromFile(FN_OEC_CONF_NYSE, &OecConfig[ORDER_NYSE_CCG], nyseValues, 5) == ERROR)
    {
      return ERROR;
    }
  }

  if (IsGottenOrderConfig[ORDER_BATSZ_BOE] == NO)
  {
    TraceLog(DEBUG_LEVEL, "Loading BATSZ BOE config from file\n");
    EnumOecConfValues batszValues[] = {OEC_CONF_FIELD_IP, OEC_CONF_FIELD_PORT, OEC_CONF_FIELD_SESSION_SUB_ID, OEC_CONF_FIELD_USERNAME, OEC_CONF_FIELD_PASSWORD, OEC_CONF_FIELD_NO_UNSPECIFIED_UNIT_REPLAY};
    if (LoadOecConfigFromFile(FN_OEC_CONF_BATSZ, &OecConfig[ORDER_BATSZ_BOE], batszValues, 6) == ERROR)
    {
      return ERROR;
    }
  }

  if (IsGottenOrderConfig[ORDER_BYX_BOE] == NO)
  {
    TraceLog(DEBUG_LEVEL, "Loading BYX BOE config from file\n");
    EnumOecConfValues byxValues[] = {OEC_CONF_FIELD_IP, OEC_CONF_FIELD_PORT, OEC_CONF_FIELD_SESSION_SUB_ID, OEC_CONF_FIELD_USERNAME, OEC_CONF_FIELD_PASSWORD, OEC_CONF_FIELD_NO_UNSPECIFIED_UNIT_REPLAY};
    if (LoadOecConfigFromFile(FN_OEC_CONF_BYX, &OecConfig[ORDER_BYX_BOE], byxValues, 6) == ERROR)
    {
      return ERROR;
    }
  }
  
  if (IsGottenOrderConfig[ORDER_EDGX] == NO)
  {
    TraceLog(DEBUG_LEVEL, "Loading EDGX Order config from file\n");
    EnumOecConfValues edgxValues[] = {OEC_CONF_FIELD_IP, OEC_CONF_FIELD_PORT, OEC_CONF_FIELD_USERNAME, OEC_CONF_FIELD_PASSWORD};
    if (LoadOecConfigFromFile(FN_OEC_CONF_EDGX, &OecConfig[ORDER_EDGX], edgxValues, 4) == ERROR)
    {
      return ERROR;
    }
  }
  
  if (IsGottenOrderConfig[ORDER_EDGA] == NO)
  {
    TraceLog(DEBUG_LEVEL, "Loading EDGA Order config from file\n");
    EnumOecConfValues edgaValues[] = {OEC_CONF_FIELD_IP, OEC_CONF_FIELD_PORT, OEC_CONF_FIELD_USERNAME, OEC_CONF_FIELD_PASSWORD};
    if (LoadOecConfigFromFile(FN_OEC_CONF_EDGA, &OecConfig[ORDER_EDGA], edgaValues, 4) == ERROR)
    {
      return ERROR;
    }
  }
  
  return SUCCESS;
}


/****************************************************************************
- Function name:  LoadOecConfigFromFile
****************************************************************************/
int LoadOecConfigFromFile(const char *fileName, t_OecConfig *config, const EnumOecConfValues valueFlag[], const int numValue)
{
  char fullPath[MAX_PATH_LEN];
  
  if (GetFullConfigPath(fileName, CONF_ORDER, fullPath) == NULL_POINTER) return ERROR;
  
  FILE *fileDesc = fopen(fullPath, "r");
  
  if(fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Critical! Cannot open file (%s). Trade Server can not continue.\n", fullPath);
    PrintErrStr(ERROR_LEVEL, "calling fopen(): ", errno);
    sleep(3); exit(0);
    return ERROR;
  }

  char buffer[1024];
  int  valueIndex = 0;
  
  // Default protocol for all oec is TCP
  config->protocol = PROTOCOL_TCP;
  
  while (fgets(buffer, 1024, fileDesc) != NULL && valueIndex < numValue)
  {
    if (strchr(buffer, '#')) continue;
    RemoveCharacters(buffer, strlen(buffer));
    if (strlen(buffer) == 0) continue;

    switch(valueFlag[valueIndex])
    {
      case OEC_CONF_FIELD_IP:
        strncpy(config->ipAddress, buffer, 32);
        break;
      case OEC_CONF_FIELD_PORT:
        config->port = atoi(buffer);
        break; 
      case OEC_CONF_FIELD_USERNAME:
        strncpy(config->userName, buffer, 32);
        break;
      case OEC_CONF_FIELD_PASSWORD:
        strncpy(config->password, buffer, 32);
        break;
      case OEC_CONF_FIELD_COMPID:
        strncpy(config->compGroupID, buffer, 32);
        break;
      case OEC_CONF_FIELD_AUTO_CANCEL:
        config->autoCancelOnDisconnect = atoi(buffer);
        break;
      case OEC_CONF_FIELD_SESSION_SUB_ID:
        strncpy(config->sessionSubId, buffer, 32);
        break;
      case OEC_CONF_FIELD_NO_UNSPECIFIED_UNIT_REPLAY:
        config->noUnspecifiedUnitReplay = atoi(buffer);
        break;
      case OEC_CONF_FIELD_PROTOCOL:
        if(strcmp(buffer, "tcp") == 0) {
          TraceLog(DEBUG_LEVEL, "Setting protocol to tcp\n");
          config->protocol = PROTOCOL_TCP; 
        }
        else if(strcmp(buffer, "udp") == 0) {
          TraceLog(DEBUG_LEVEL, "Setting protocol to udp\n");
          config->protocol = PROTOCOL_UDP;
        }
        else
        {
          fclose(fileDesc);
          TraceLog(ERROR_LEVEL, "Critical! protocol(%s) is invalid. It must be tcp or udp.\n", buffer);
          TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format. Trade Server can not continue.\n", fullPath);
          sleep(3); exit(0);
          return ERROR;
        }
        break;
      default:
        fclose(fileDesc);
        TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format. Trade Server can not continue.\n", fullPath);
        sleep(3); exit(0);
        return ERROR;
    }
      
    valueIndex++;
    memset(buffer, 0, 1024);
  }
  
  fclose(fileDesc);
  
  if (valueIndex != numValue)
  {
    TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format. Trade Server can not continue.\n", fullPath);
    sleep(3); exit(0);
  }
  
  config->sessionId = NextSessionId++;
  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadAllBooksConfig
****************************************************************************/
int LoadAllBooksConfig(void)
{
  if (IsGottenBookConfig[BOOK_NASDAQ] == NO)
  {
    TraceLog(DEBUG_LEVEL, "Loading NASDAQ Book configuration from file\n");
    if (LoadNASDAQBookConfig("nasdaq", &NASDAQ_Book_Conf) == ERROR)
    {
      return ERROR;
    }
  }

  if (IsGottenBookConfig[BOOK_NDBX] == NO)
  {
    TraceLog(DEBUG_LEVEL, "Loading BX Book configuration from file\n");
    if (LoadNASDAQBookConfig("bx", &NDBX_Book_Conf) == ERROR)
    {
      return ERROR;
    }
  }

  if (IsGottenBookConfig[BOOK_PSX] == NO)
  {
    TraceLog(DEBUG_LEVEL, "Loading PSX Book configuration from file\n");
    if (LoadNASDAQBookConfig("psx", &PSX_Book_Conf) == ERROR)
    {
      return ERROR;
    }
  }

  if (IsGottenBookConfig[BOOK_BATSZ] == NO)
  {
    TraceLog(DEBUG_LEVEL, "Loading BATS-Z Book configuration from file\n");
    if (LoadBATSZ_Book_Config("batsz") == ERROR)
    {
      return ERROR;
    }
  }
  
  if (IsGottenBookConfig[BOOK_BYX] == NO)
  {
    TraceLog(DEBUG_LEVEL, "Loading BYX Book configuration from file\n");
    if (LoadBYX_Book_Config("byx") == ERROR)
    {
      return ERROR;
    }
  }
  
  if (IsGottenBookConfig[BOOK_ARCA] == NO)
  {
    TraceLog(DEBUG_LEVEL, "Loading ARCA Book configuration from file\n");
    
    if (LoadARCA_Book_Config("arca_switch", ARCA_Book_Conf.group) == ERROR)
    {
      return ERROR;
    }
    
    int i;
    
    TraceLog(DEBUG_LEVEL, "Arca book Primary Interface name: %s\n", ARCA_Book_Conf.NICName);
    TraceLog(DEBUG_LEVEL, "Arca book Backup Interface name: %s\n", ARCA_Book_Conf.backupNIC);
    TraceLog(DEBUG_LEVEL, "Cumulative packet loss: %d\n", ARCA_Book_Conf.recoveryThreshold);          
    
    for (i = 0; i < MAX_ARCA_GROUPS; i++)
    {
      TraceLog(DEBUG_LEVEL, "Arca book group %d. Primary data feed: %s:%d\n", i+1, ARCA_Book_Conf.group[i].dataIP, ARCA_Book_Conf.group[i].dataPort);
      if (ARCA_Book_Conf.group[i].backupIP[0] != 0)
      {
        TraceLog(DEBUG_LEVEL, "Arca book group %d. Backup feed: %s:%d\n", i+1, ARCA_Book_Conf.group[i].backupIP, ARCA_Book_Conf.group[i].backupPort);
      }
    }
  }
  
  if (IsGottenBookConfig[BOOK_NYSE] == NO)
  {
    TraceLog(DEBUG_LEVEL, "Loading NYSE Book configuration from file\n");
    if (LoadNyseBookConfig("nyse_book", BOOK_NYSE) == ERROR)
    {
      return ERROR;
    }

    TraceLog(DEBUG_LEVEL, "NYSE Book: Primary Interface name: %s\n", NyseBookConf.NICName);
    TraceLog(DEBUG_LEVEL, "NYSE Book: Backup Interface name: %s\n", NyseBookConf.backupNIC);
    TraceLog(DEBUG_LEVEL, "Cumulative packet loss: %d\n", NyseBookConf.recoveryThreshold);
    int i;
    for (i = 0; i < MAX_NYSE_BOOK_GROUPS; i++)
    {
      TraceLog(DEBUG_LEVEL, "NYSE Book group %d. Data feed: %s:%d\n", i+1, NyseBookConf.group[i].receiveDataIP, NyseBookConf.group[i].receiveDataPort);
      if (NyseBookConf.group[i].backupIP[0] != 0)
      {
        TraceLog(DEBUG_LEVEL, "NYSE Book group %d. Backup feed: %s:%d\n", i+1, NyseBookConf.group[i].backupIP, NyseBookConf.group[i].backupPort);
      }
    }
  }
  
  if (IsGottenBookConfig[BOOK_AMEX] == NO)
  {
    TraceLog(DEBUG_LEVEL, "Loading AMEX Book configuration from file\n");
    if (LoadNyseBookConfig("amex_book", BOOK_AMEX) == ERROR)
    {
      return ERROR;
    }

    TraceLog(DEBUG_LEVEL, "AMEX Book: Primary Interface name: %s\n", AmexBookConf.NICName);
    TraceLog(DEBUG_LEVEL, "AMEX Book: Backup Interface name: %s\n", AmexBookConf.backupNIC);
    TraceLog(DEBUG_LEVEL, "Cumulative packet loss: %d\n", AmexBookConf.recoveryThreshold);
    int i = 0;
    TraceLog(DEBUG_LEVEL, "AMEX Book Data feed: %s:%d\n", AmexBookConf.group[i].receiveDataIP, AmexBookConf.group[i].receiveDataPort);
    if (AmexBookConf.group[i].backupIP[0] != 0)
    {
      TraceLog(DEBUG_LEVEL, "AMEX Book Backup feed: %s:%d\n", AmexBookConf.group[i].backupIP, AmexBookConf.group[i].backupPort);
    }
  }

  if (IsGottenBookConfig[BOOK_EDGX] == NO)
  {
    TraceLog(DEBUG_LEVEL, "Loading EDGX Book configuration from file\n");
    if (Load_EDGX_BookConfig("edgx_book") == ERROR)
    {
      return ERROR;
    }
  }

  if (IsGottenBookConfig[BOOK_EDGA] == NO)
  {
    TraceLog(DEBUG_LEVEL, "Loading EDGA Book configuration from file\n");
    if (Load_EDGA_BookConfig("edga_book") == ERROR)
    {
      return ERROR;
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadARCA_Book_Config
****************************************************************************/
int LoadARCA_Book_Config(const char *fileName, t_ARCA_Book_GroupInfo group[MAX_ARCA_GROUPS])
{
  char fullPath[MAX_PATH_LEN];
  FILE  *f;
  char line[1024];
  int sectionIndex = 0;
  int groupIndex = 0;
  char feedName = 0;
  
  //------------------------------------------------------------------
  // Get value indicating A or B Feed from first file (arca_switch)
  //------------------------------------------------------------------
  if (GetFullConfigPath(fileName, CONF_BOOK, fullPath) == NULL_POINTER)
  {
    return ERROR;
  }
  
  f = fopen(fullPath, "r");
  if (f == NULL)
  {
    TraceLog(ERROR_LEVEL, "Critical! Could not open file (%s). Trade Server can not continue.\n", fullPath);
    PrintErrStr(ERROR_LEVEL, "calling fopen(): ", errno);
    sleep(3); exit(0);
    return ERROR;
  }

  memset(line, 0, 1024);
  while (fgets(line, 1024, f) != NULL)
  {   
    if (!strchr(line, '#'))
    { 
      if (strlen(line) == 0) continue;
      RemoveCharacters(line, strlen(line));
      
      if (strlen(line) == 0) continue;
      
      //Read the contents
      feedName = line[0];
    }
  }
  
  fclose(f);
  f = NULL;
      

  //------------------------------------------------------------------
  // Load the actual config from file depending on what read above
  //    arca.xdp.afeed 
  //or  arca.xdp.bfeed
  //------------------------------------------------------------------
  char feedFile[32];
  if ((feedName == 'a') || (feedName == 'A'))
  {
    ARCA_Book_Conf.currentFeedName = 'A';
    strcpy(feedFile, "arca.xdp.afeed");
  }
  else
  {
    ARCA_Book_Conf.currentFeedName = 'B';
    strcpy(feedFile, "arca.xdp.bfeed");
  }
  
  TraceLog(DEBUG_LEVEL, "Loading ARCA Book config from '%s'...\n", feedFile);
  
  if (GetFullConfigPath(feedFile, CONF_BOOK, fullPath) == NULL_POINTER)
  {
    return ERROR;
  }
  
  f = fopen(fullPath, "r");
  if (f == NULL)
  {
    TraceLog(ERROR_LEVEL, "Critical! Could not open file (%s). Trade Server can not continue.\n", fullPath);
    PrintErrStr(ERROR_LEVEL, "calling fopen(): ", errno);
    sleep(3); exit(0);
      return ERROR;
  }

  memset(line, 0, 1024);
  
  while (fgets(line, 1024, f) != NULL)
  {   
    if (!strchr(line, '#'))
    { 
      if (strlen(line) == 0) continue;
      
      /*
      File structure:
      - Interface Name for used with DBL connections
      - Interface used for backup connections
      - Retransmission Request IP
      - Retransmission Request Port
      - Retransmission Source Name (user name)
      - 4 groups info: each has the following info
        + IP Address Primary data feed UDP
        + Port Primary data feed UDP
        + IP Address Retransmission UDP
        + Port Retransmission UDP
        + IP Address Refresh Data Feed UDP
        + Port Refresh Data Feed UDP
      */
      
      RemoveCharacters(line, strlen(line));
      if (strlen(line) == 0) continue;
      
      switch (sectionIndex)
      {
        case 0:
          ARCA_Book_Conf.recoveryThreshold = atoi(line);
          sectionIndex++;
          break;
        case 1:
          strcpy(ARCA_Book_Conf.NICName, line);
          sectionIndex++;
          break;
        case 2:
          strcpy(ARCA_Book_Conf.backupNIC, line);
          sectionIndex++;
          break;
        case 3:
          if (strncmp(line, "request:", 8) != 0)
          {
            TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format (Could not find 'request:' field). Trade Server can not continue.\n", fullPath);
            sleep(3); exit(0);
          }

          sectionIndex++;
          break;
        case 4: //Retransmission Request Source
          sectionIndex++;
          break;
        case 5:
          if (groupIndex == MAX_ARCA_GROUPS)
          {
            if (strncmp(line, "ref:", 4) != 0)
            {
              TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format. Trade Server can not continue.\n", fullPath);
              sleep(3); exit(0);
              return ERROR;
            }
            break;
          }
          
          if (strncmp(line, "primary:", 8) != 0)
          {
            TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format (Could not find 'primary:' field). Trade Server can not continue.\n", fullPath);
            sleep(3); exit(0);
          }
          
          if (GetAddressFromConfig(&line[8], group[groupIndex].dataIP, &group[groupIndex].dataPort) == ERROR)
          {
            TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format ('primary:' field). Trade Server can not continue.\n", fullPath);
            sleep(3); exit(0);
          }
          
          sectionIndex++;
          break;
        case 6: //Backup feed or retransmission feed
          if (strncmp(line, "backup:", 7) == 0)
          {
            if (GetAddressFromConfig(&line[7], group[groupIndex].backupIP, &group[groupIndex].backupPort) == ERROR)
            {
              TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format ('backup:' field). Trade Server can not continue.\n", fullPath);
              sleep(3); exit(0);
            }
          }
          else
          {
            if (strncmp(line, "recovery:", 9) != 0)
            {
              TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format (Could not find 'recovery:' field). Trade Server can not continue.\n", fullPath);
              sleep(3); exit(0);
            }
            
            groupIndex++;
            sectionIndex = 5;
          }
          break;
      }
    }
  }
    
  // Close the file
  fclose(f);

  if (groupIndex != MAX_ARCA_GROUPS)
  {
    TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format. Trade Server can not continue.\n", fullPath);
    sleep(3); exit(0);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadNyseBookConfig
****************************************************************************/
int LoadNyseBookConfig(const char *fileName, const int mdcIndex)
{
  t_NYSE_Book_Conf *config;
  int maxGroup;
  if (mdcIndex == BOOK_NYSE)
  {
    config = &NyseBookConf;
    maxGroup = MAX_NYSE_BOOK_GROUPS;
  }
  else
  {
    config = &AmexBookConf;
    maxGroup = 1;
  }
  
  char fullPath[MAX_PATH_LEN];
  FILE  *f;
  char line[1024];
  int sectionIndex = 0;
  int groupIndex = 0;

  TraceLog(DEBUG_LEVEL, "Loading %s Book config from '%s'...\n", GetBookNameByIndex(mdcIndex), fileName);

  if (GetFullConfigPath(fileName, CONF_BOOK, fullPath) == NULL_POINTER)
  {
    return ERROR;
  }

  f = fopen(fullPath, "r");
  if (f == NULL)
  {
    TraceLog(ERROR_LEVEL, "Critical! Could not open file (%s). Trade Server can not continue.\n", fullPath);
    PrintErrStr(ERROR_LEVEL, "calling fopen(): ", errno);
    sleep(3); exit(0);
    return ERROR;
  }

  memset(line, 0, 1024);
  config->countRef = 0;
  
  while (fgets(line, 1024, f) != NULL)
  {
    if (!strchr(line, '#'))
    {
      if (strlen(line) == 0) continue;

      /*
      File structure:
      - NIC Name for used with DBL connections
      - NIC Name used for backup connections
      - Source ID is used for sending request to TCP Server
      - 20 groups info: each has the following info
        + Retransmission Request IP
        + Retransmission Request Port
        + IP Address Primary data feed UDP
        + Port Primary data feed UDP
        + IP Address Retransmission UDP
        + Port Retransmission UDP
        + IP Address Refresh Data Feed UDP
        + Port Refresh Data Feed UDP
      */

      RemoveCharacters(line, strlen(line));

      if (strlen(line) == 0) continue;

      switch (sectionIndex)
      {
        case 0:
          config->recoveryThreshold = atoi(line);
          sectionIndex++;
          break;
        case 1:
          strcpy(config->NICName, line);
          sectionIndex++;
          break;
        case 2:
          strcpy(config->backupNIC, line);
          sectionIndex++;
          break;
        case 3: //Retransmission Request Source
          sectionIndex++;
          break;
        case 4:
          if (groupIndex == maxGroup)
          {
            if (strncmp(line, "ref:", 4) == 0)
            {
              if (GetAddressFromConfig(&line[4], config->refIP[config->countRef], &config->refPort[config->countRef]) == ERROR)
              {
                TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format. Trade Server can not continue.\n", fullPath);
                sleep(3); exit(0);
                return ERROR;
              }
              
              config->countRef++;
              break;
            }
            else
            {
              TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format. Trade Server can not continue.\n", fullPath);
              sleep(3); exit(0);
              return ERROR;
            }
          }
          
          if (strncmp(line, "request:", 8) != 0)
          {
            TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format (Could not find 'request:' field). Trade Server can not continue.\n", fullPath);
            sleep(3); exit(0);
          }
          
          sectionIndex++;
          break;
        case 5:
          if (strncmp(line, "primary:", 8) != 0)
          {
            TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format (Could not find 'primary:' field). Trade Server can not continue.\n", fullPath);
            sleep(3); exit(0);
          }
          
          if (GetAddressFromConfig(&line[8], config->group[groupIndex].receiveDataIP, &config->group[groupIndex].receiveDataPort) == ERROR)
          {
            TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format ('primary:' field). Trade Server can not continue.\n", fullPath);
            sleep(3); exit(0);
          }
          
          strcpy(config->refIP[config->countRef], config->group[groupIndex].receiveDataIP);
          config->refPort[config->countRef++] = config->group[groupIndex].receiveDataPort;
          
          sectionIndex++;
          break;
        case 6:
          if (strncmp(line, "backup:", 7) == 0)
          {
            if (GetAddressFromConfig(&line[7], config->group[groupIndex].backupIP, &config->group[groupIndex].backupPort) == ERROR)
            {
              TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format ('backup:' field). Trade Server can not continue.\n", fullPath);
              sleep(3); exit(0);
            }
            
            strcpy(config->refIP[config->countRef], config->group[groupIndex].backupIP);
            config->refPort[config->countRef++] = config->group[groupIndex].backupPort;
          }
          else
          {
            if (strncmp(line, "recovery:", 9) != 0)
            {
              TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format (Could not find 'recovery:' field). Trade Server can not continue.\n", fullPath);
              sleep(3); exit(0);
            }
            
            groupIndex++;
            sectionIndex = 4;
          }
          break;
      }
    }
  }

  fclose(f);

  if (groupIndex != maxGroup)
  {
    TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format. Trade Server can not continue.\n", fullPath);
    sleep(3); exit(0);
  }
  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadNASDAQ_MoldUDP_Config
****************************************************************************/
int LoadNASDAQBookConfig(const char *fileName, t_NASDAQ_Book_Conf *config)
{
  char fullPath[MAX_PATH_LEN];
  
  if (GetFullConfigPath(fileName, CONF_BOOK, fullPath) == NULL_POINTER)
  {
    return ERROR;
  }
  
  FILE *f;

  //Buffer to store each line in the configuration file
  char line[1024];
  int sectionIndex = 0;

  //Open file configuration
  f = fopen(fullPath, "r");
  if(f == NULL)
  {
    TraceLog(ERROR_LEVEL, "Critical! Could not open file (%s). Trade Server can not continue.\n", fullPath);
    PrintErrStr(ERROR_LEVEL, "calling fopen(): ", errno);
    sleep(3); exit(0);
    return -1;
  }
      
  memset(line, 0, 1024);
  config->Backup.ip[0] = 0;
  
  //Get each line from the configuration file
  while (fgets(line, 1024, f) != NULL)
  {
    if (!strchr(line, '#'))
    {
      RemoveCharacters(line, strlen(line));
      if (strlen(line) == 0) continue;
      
      switch (sectionIndex)
      {
        case 0:
          strcpy(config->Data.NICName, line);
          sectionIndex++;
          break;
        case 1:
          strcpy(config->Backup.NICName, line);
          sectionIndex++;
          break;
        case 2: // Primary feed
          if (strncmp(line, "primary:", 8) != 0)
          {
            TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format (Could not find 'primary:' field). Trade Server can not continue.\n", fullPath);
            sleep(3); exit(0);
          }
          
          if (GetAddressFromConfig(&line[8], config->Data.ip, &config->Data.port) == ERROR)
          {
            TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format ('primary:' field). Trade Server can not continue.\n", fullPath);
            sleep(3); exit(0);
          }
          
          sectionIndex++;
          break;
        case 3: // Backup feed (optional)
          if (strncmp(line, "backup:", 7) == 0)
          {
            if (GetAddressFromConfig(&line[7], config->Backup.ip, &config->Backup.port) == ERROR)
            {
              TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format ('backup:' field). Trade Server can not continue.\n", fullPath);
              sleep(3); exit(0);
            }
            break;
          }
          else
          {
            // don't break, fall through next case
            
            sectionIndex++;
          }
        case 4:
          config->recoveryThreshold = atoi(line);
          sectionIndex++;
          break;
        default:
          if (strncmp(line, "ref:", 4) != 0)
          {
            TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format. Trade Server can not continue.\n", fullPath);
            sleep(3); exit(0);
            return ERROR;
          }
          break;
      }
    }
  }

  if (sectionIndex != 5)
  {
    TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format. Trade Server can not continue.\n", fullPath);
    sleep(3); exit(0);
    return ERROR;
  }
  
  TraceLog(DEBUG_LEVEL, "\t Cumulative packet loss: %d\n", config->recoveryThreshold);
  TraceLog(DEBUG_LEVEL, "\t Interface used for Data Feed: '%s'\n", config->Data.NICName);
  TraceLog(DEBUG_LEVEL, "\t Interface used for Backup: '%s'\n", config->Backup.NICName);
  TraceLog(DEBUG_LEVEL, "\t Primary feed %s:%d \n", config->Data.ip, config->Data.port);
  if (config->Backup.ip[0] != 0)
    TraceLog(DEBUG_LEVEL, "\t Backup feed %s:%d \n", config->Backup.ip, config->Backup.port);
  
  //Close the file
  fclose(f);
  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadBATSZ_Book_Config
****************************************************************************/
int LoadBATSZ_Book_Config(const char *fileName)
{
  char fullPath[MAX_PATH_LEN];
  
  if (GetFullConfigPath(fileName, CONF_BOOK, fullPath) == NULL_POINTER)
  {
    return ERROR;
  }
  
  FILE* fileDesc;
  
  /*
  Try to open BATS-Z Book configuration file
  Please be aware of the requirements for the configuration file here
  */
  fileDesc = fopen(fullPath, "r");
  
  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Critical! Could not open file (%s). Trade Server can not continue.\n", fullPath);
    PrintErrStr(ERROR_LEVEL, "calling fopen(): ", errno);
    sleep(3); exit(0);
    return ERROR;
  }

  //File opened successfully

  int i;
  int groupIndex = 0;
  for (i=0; i<MAX_BATSZ_UNIT; i++)
  {
    //clear primary connection contents
    memset(BATSZ_Book_Conf.group[i].dataIP, 0, MAX_LINE_LEN);
    BATSZ_Book_Conf.group[i].dataPort = 0;
    
    memset(BATSZ_Book_Conf.group[i].backupIP, 0, MAX_LINE_LEN);
    BATSZ_Book_Conf.group[i].backupPort = 0;
  }
  
  // Clear NIC Name for used with DBL connections
  memset(BATSZ_Book_Conf.NICName, 0, MAX_LINE_LEN);
  
  // Buffer to store each line in the configuration file
  char line[1024];
  
  // Number of lines read from the configuration file
  int sectionIndex = 0;
  
  //Clear the buffer pointed to by line
  memset(line, 0, 1024);
  BATSZ_Book_Conf.countRef = 0;
  
  // Get each line from the configuration file
  while (fgets(line, 1024, fileDesc) != NULL)
  { 
    RemoveCharacters(line, strlen(line));
    if (!strchr(line, '#') && (strlen(line) != 0))
    { 
      switch (sectionIndex)
      {
        case 0:
          BATSZ_Book_Conf.recoveryThreshold = atoi(line);
          sectionIndex++;
          break;
        case 1:
          strcpy(BATSZ_Book_Conf.NICName, line);
          sectionIndex++;
          break;
        case 2:
          strcpy(BATSZ_Book_Conf.backupNIC, line);
          sectionIndex++;
          break;
        case 3: //Session Sub ID
          sectionIndex++;
          break;
        case 4: //Username
          sectionIndex++;
          break;
        case 5: //Password
          sectionIndex++;
          break;
        case 6:
          if (strncmp(line, "request:", 8) != 0)
          {
            TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format (Could not find 'request:' field). Trade Server can not continue.\n", fullPath);
            sleep(3); exit(0);
          }
          
          sectionIndex++;
          break;
        case 7:
          if (groupIndex == MAX_BATSZ_UNIT)
          {
            if (strncmp(line, "ref:", 4) == 0)
            {
              if (GetAddressFromConfig(&line[4], BATSZ_Book_Conf.refIP[BATSZ_Book_Conf.countRef], &BATSZ_Book_Conf.refPort[BATSZ_Book_Conf.countRef]) == ERROR)
              {
                TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format. Trade Server can not continue.\n", fullPath);
                sleep(3); exit(0);
                return ERROR;
              }
              
              BATSZ_Book_Conf.countRef++;
              break;
            }
            else
            {
              TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format. Trade Server can not continue.\n", fullPath);
              sleep(3); exit(0);
              return ERROR;
            }
          }
          
          if (strncmp(line, "primary:", 8) != 0)
          {
            TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format (Could not find 'primary:' field). Trade Server can not continue.\n", fullPath);
            sleep(3); exit(0);
          }
          
          if (GetAddressFromConfig(&line[8], BATSZ_Book_Conf.group[groupIndex].dataIP, &BATSZ_Book_Conf.group[groupIndex].dataPort) == ERROR)
          {
            TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format ('primary:' field). Trade Server can not continue.\n", fullPath);
            sleep(3); exit(0);
          }
          
          strcpy(BATSZ_Book_Conf.refIP[BATSZ_Book_Conf.countRef], BATSZ_Book_Conf.group[groupIndex].dataIP);
          BATSZ_Book_Conf.refPort[BATSZ_Book_Conf.countRef++] = BATSZ_Book_Conf.group[groupIndex].dataPort;
          
          sectionIndex++;
          break;
        case 8: //Backup feed or retransmission feed
          if (strncmp(line, "backup:", 7) == 0)
          {
            if (GetAddressFromConfig(&line[7], BATSZ_Book_Conf.group[groupIndex].backupIP, &BATSZ_Book_Conf.group[groupIndex].backupPort) == ERROR)
            {
              TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format ('backup:' field). Trade Server can not continue.\n", fullPath);
              sleep(3); exit(0);
            }
            
            strcpy(BATSZ_Book_Conf.refIP[BATSZ_Book_Conf.countRef], BATSZ_Book_Conf.group[groupIndex].backupIP);
            BATSZ_Book_Conf.refPort[BATSZ_Book_Conf.countRef++] = BATSZ_Book_Conf.group[groupIndex].backupPort;
          }
          else
          {
            if (strncmp(line, "recovery:", 9) != 0)
            {
              TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format (Could not find 'primary:' field). Trade Server can not continue.\n", fullPath);
              sleep(3); exit(0);
            }

            groupIndex++;
            sectionIndex = 7;
          }
          break;
      }
    }
  }
  
  // Close the file
  fclose(fileDesc);
  if (groupIndex == MAX_BATSZ_UNIT) // Number of lines are exact what we expect
  {
    TraceLog(DEBUG_LEVEL, "BATS-Z Book Configuration:\n");
    TraceLog(DEBUG_LEVEL, "Primary Interface: %s\n", BATSZ_Book_Conf.NICName);
    TraceLog(DEBUG_LEVEL, "Backup Interface: %s\n", BATSZ_Book_Conf.backupNIC);
    TraceLog(DEBUG_LEVEL, "Cumulative packet loss: %d\n", BATSZ_Book_Conf.recoveryThreshold);
    
    for (i=0; i< MAX_BATSZ_UNIT; i++)
    {
      TraceLog(DEBUG_LEVEL, "BATS-Z Unit %d Primary feed: %s:%d\n", i+1, BATSZ_Book_Conf.group[i].dataIP, BATSZ_Book_Conf.group[i].dataPort);
      if (BATSZ_Book_Conf.group[i].backupIP[0] != 0)
        TraceLog(DEBUG_LEVEL, "BATS-Z Unit %d Backup feed: %s:%d\n", i+1, BATSZ_Book_Conf.group[i].backupIP, BATSZ_Book_Conf.group[i].backupPort);
    }
    return SUCCESS;
  }
  else  //Indicates that file has not enough information
  {
    TraceLog(ERROR_LEVEL, "BATS-Z Book configuration file (%s) has not enough information!\n", fullPath);
    sleep(3); exit(0);
    return ERROR;
  }
}

/****************************************************************************
- Function name:  LoadBYX_Book_Config
****************************************************************************/
int LoadBYX_Book_Config(const char *fileName)
{
  char fullPath[MAX_PATH_LEN];
  
  if (GetFullConfigPath(fileName, CONF_BOOK, fullPath) == NULL_POINTER)
  {
    return ERROR;
  }
  
  FILE* fileDesc;
  
  /*
  Try to open BATS-Z Book configuration file
  Please be aware of the requirements for the configuration file here
  */
  fileDesc = fopen(fullPath, "r");
  
  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Critical! Could not open file (%s). Trade Server can not continue.\n", fullPath);
    PrintErrStr(ERROR_LEVEL, "calling fopen(): ", errno);
    sleep(3); exit(0);
    return ERROR;
  }

  //File opened successfully

  int i;
  int groupIndex = 0;
  for (i=0; i<MAX_BYX_UNIT; i++)
  {
    //clear primary connection contents
    memset(BYX_Book_Conf.group[i].dataIP, 0, MAX_LINE_LEN);
    BYX_Book_Conf.group[i].dataPort = 0;
    
    memset(BYX_Book_Conf.group[i].backupIP, 0, MAX_LINE_LEN);
    BYX_Book_Conf.group[i].backupPort = 0;
  }
  
  // Clear NIC Name for used with DBL connections
  memset(BYX_Book_Conf.NICName, 0, MAX_LINE_LEN);
  
  // Buffer to store each line in the configuration file
  char line[1024];
  
  // Number of lines read from the configuration file
  int sectionIndex = 0;
  
  //Clear the buffer pointed to by line
  memset(line, 0, 1024);
  BYX_Book_Conf.countRef = 0;
  
  // Get each line from the configuration file
  while (fgets(line, 1024, fileDesc) != NULL)
  { 
    RemoveCharacters(line, strlen(line));
    if (!strchr(line, '#') && (strlen(line) != 0))
    { 
      switch (sectionIndex)
      {
        case 0:
          BYX_Book_Conf.recoveryThreshold = atoi(line);
          sectionIndex++;
          break;
        case 1:
          strcpy(BYX_Book_Conf.NICName, line);
          sectionIndex++;
          break;
        case 2:
          strcpy(BYX_Book_Conf.backupNIC, line);
          sectionIndex++;
          break;
        case 3: //Session Sub ID
          sectionIndex++;
          break;
        case 4: //Username
          sectionIndex++;
          break;
        case 5: //Password
          sectionIndex++;
          break;
        case 6:
          if (strncmp(line, "request:", 8) != 0)
          {
            TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format (Could not find 'request:' field). Trade Server can not continue.\n", fullPath);
            sleep(3); exit(0);
          }
          
          sectionIndex++;
          break;
        case 7:
          if (groupIndex == MAX_BYX_UNIT)
          {
            if (strncmp(line, "ref:", 4) == 0)
            {
              if (GetAddressFromConfig(&line[4], BYX_Book_Conf.refIP[BYX_Book_Conf.countRef], &BYX_Book_Conf.refPort[BYX_Book_Conf.countRef]) == ERROR)
              {
                TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format. Trade Server can not continue.\n", fullPath);
                sleep(3); exit(0);
                return ERROR;
              }
              
              BYX_Book_Conf.countRef++;
              break;
            }
            else
            {
              TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format. Trade Server can not continue.\n", fullPath);
              sleep(3); exit(0);
              return ERROR;
            }
          }
          
          if (strncmp(line, "primary:", 8) != 0)
          {
            TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format (Could not find 'primary:' field). Trade Server can not continue.\n", fullPath);
            sleep(3); exit(0);
          }
          
          if (GetAddressFromConfig(&line[8], BYX_Book_Conf.group[groupIndex].dataIP, &BYX_Book_Conf.group[groupIndex].dataPort) == ERROR)
          {
            TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format ('primary:' field). Trade Server can not continue.\n", fullPath);
            sleep(3); exit(0);
          }
          
          strcpy(BYX_Book_Conf.refIP[BYX_Book_Conf.countRef], BYX_Book_Conf.group[groupIndex].dataIP);
          BYX_Book_Conf.refPort[BYX_Book_Conf.countRef++] = BYX_Book_Conf.group[groupIndex].dataPort;
          
          sectionIndex++;
          break;
        case 8: //Backup feed or retransmission feed
          if (strncmp(line, "backup:", 7) == 0)
          {
            if (GetAddressFromConfig(&line[7], BYX_Book_Conf.group[groupIndex].backupIP, &BYX_Book_Conf.group[groupIndex].backupPort) == ERROR)
            {
              TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format ('backup:' field). Trade Server can not continue.\n", fullPath);
              sleep(3); exit(0);
            }
            
            strcpy(BYX_Book_Conf.refIP[BYX_Book_Conf.countRef], BYX_Book_Conf.group[groupIndex].backupIP);
            BYX_Book_Conf.refPort[BYX_Book_Conf.countRef++] = BYX_Book_Conf.group[groupIndex].backupPort;
          }
          else
          {
            if (strncmp(line, "recovery:", 9) != 0)
            {
              TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format (Could not find 'primary:' field). Trade Server can not continue.\n", fullPath);
              sleep(3); exit(0);
            }

            groupIndex++;
            sectionIndex = 7;
          }
          break;
      }
    }
  }
  
  // Close the file
  fclose(fileDesc);
  if (groupIndex == MAX_BYX_UNIT) // Number of lines are exact what we expect
  {
    TraceLog(DEBUG_LEVEL, "BYX Book Configuration:\n");
    TraceLog(DEBUG_LEVEL, "Primary Interface: %s\n", BYX_Book_Conf.NICName);
    TraceLog(DEBUG_LEVEL, "Backup Interface: %s\n", BYX_Book_Conf.backupNIC);
    TraceLog(DEBUG_LEVEL, "Cumulative packet loss: %d\n", BYX_Book_Conf.recoveryThreshold);
    
    for (i=0; i< MAX_BYX_UNIT; i++)
    {
      TraceLog(DEBUG_LEVEL, "BYX Unit %d Primary feed: %s:%d\n", i+1, BYX_Book_Conf.group[i].dataIP, BYX_Book_Conf.group[i].dataPort);
      if (BYX_Book_Conf.group[i].backupIP[0] != 0)
        TraceLog(DEBUG_LEVEL, "BYX Unit %d Backup feed: %s:%d\n", i+1, BYX_Book_Conf.group[i].backupIP, BYX_Book_Conf.group[i].backupPort);
    }
    return SUCCESS;
  }
  else  //Indicates that file has not enough information
  {
    TraceLog(ERROR_LEVEL, "BYX Book configuration file (%s) has not enough information!\n", fullPath);
    sleep(3); exit(0);
    return ERROR;
  }
}

/****************************************************************************
- Function name:  Load_EDGX_BookConfig
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int Load_EDGX_BookConfig(const char *fileName)
{
  char fullPath[MAX_PATH_LEN];

  if (GetFullConfigPath(fileName, CONF_BOOK, fullPath) == NULL_POINTER)
  {
    return ERROR;
  }

  FILE* fileDesc = fopen(fullPath, "r");
  
  TraceLog(DEBUG_LEVEL, "Loading %s...\n", fullPath);

  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Critical! Could not open file (%s). Trade Server can not continue.\n", fullPath);
    PrintErrStr(ERROR_LEVEL, "calling fopen(): ", errno);
    sleep(3); exit(0);
    return ERROR;
  }

  //Open file successfully
  int i;
  for (i = 0; i < MAX_EDGX_BOOK_GROUP; i++)
  {
    //clear primary connection contents
    memset(EDGX_BOOK_Conf.group[i].dataIP, 0, MAX_LINE_LEN);
    EDGX_BOOK_Conf.group[i].dataPort = 0;

    memset(EDGX_BOOK_Conf.group[i].backupIP, 0, MAX_LINE_LEN);
    EDGX_BOOK_Conf.group[i].backupPort = 0;
  }
  
  // Clear NIC Name for used with DBL connections
  memset(EDGX_BOOK_Conf.NICName, 0, MAX_LINE_LEN);

  // Buffer to store each line in the configuration file
  char line[1024];

  // Number of lines read from the configuration file
  int  sectionIndex = 0;

  //Clear the buffer pointed to by line
  memset(line, 0, 1024);
  
  int groupIndex = 0;

  EDGX_BOOK_Conf.countRef = 0;
  
  // Get each line from the configuration file
  while (fgets(line, 1024, fileDesc) != NULL)
  {
    RemoveCharacters(line, strlen(line));
    if (!strchr(line, '#') && (strlen(line) != 0))
    {
      switch (sectionIndex)
      {
        case 0:
          EDGX_BOOK_Conf.recoveryThreshold = atoi(line);
          sectionIndex++;
          break;
        case 1:
          strcpy(EDGX_BOOK_Conf.NICName, line);
          sectionIndex++;
          break;
        case 2:
          strcpy(EDGX_BOOK_Conf.backupNIC, line);
          sectionIndex++;
          break;
        case 3:
          if (strncmp(line, "request:", 8) != 0)
          {
            TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format (Could not find 'request:' field). Trade Server can not continue.\n", fullPath);
            sleep(3); exit(0);
          }
          sectionIndex++;
          break;
        case 4: //username
          sectionIndex++;
          break;
        case 5: //password
          sectionIndex++;
          break;
        case 6:
          if (groupIndex == MAX_EDGX_BOOK_GROUP)
          {
            if (strncmp(line, "ref:", 4) == 0)
            {
              if (GetAddressFromConfig(&line[4], EDGX_BOOK_Conf.refIP[EDGX_BOOK_Conf.countRef], &EDGX_BOOK_Conf.refPort[EDGX_BOOK_Conf.countRef]) == ERROR)
              {
                TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format. Trade Server can not continue.\n", fullPath);
                sleep(3); exit(0);
                return ERROR;
              }
              
              EDGX_BOOK_Conf.countRef++;
              break;
            }
            else
            {
              TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format. Trade Server can not continue.\n", fullPath);
              sleep(3); exit(0);
              return ERROR;
            }
          }
          
          if (strncmp(line, "primary:", 8) != 0)
          {
            TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format (Could not find 'primary:' field). Trade Server can not continue.\n", fullPath);
            sleep(3); exit(0);
          }
          
          if (GetAddressFromConfig(&line[8], EDGX_BOOK_Conf.group[groupIndex].dataIP, &EDGX_BOOK_Conf.group[groupIndex].dataPort) == ERROR)
          {
            TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format ('primary:' field). Trade Server can not continue.\n", fullPath);
            sleep(3); exit(0);
          }
          
          strcpy(EDGX_BOOK_Conf.refIP[EDGX_BOOK_Conf.countRef], EDGX_BOOK_Conf.group[groupIndex].dataIP);
          EDGX_BOOK_Conf.refPort[EDGX_BOOK_Conf.countRef++] = EDGX_BOOK_Conf.group[groupIndex].dataPort;
          
          sectionIndex++;
          break;
        case 7: //Backup feed or retransmission feed
          if (strncmp(line, "backup:", 7) == 0)
          {
            if (GetAddressFromConfig(&line[7], EDGX_BOOK_Conf.group[groupIndex].backupIP, &EDGX_BOOK_Conf.group[groupIndex].backupPort) == ERROR)
            {
              TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format ('backup:' field). Trade Server can not continue.\n", fullPath);
              sleep(3); exit(0);
            }
            
            strcpy(EDGX_BOOK_Conf.refIP[EDGX_BOOK_Conf.countRef], EDGX_BOOK_Conf.group[groupIndex].backupIP);
            EDGX_BOOK_Conf.refPort[EDGX_BOOK_Conf.countRef++] = EDGX_BOOK_Conf.group[groupIndex].backupPort;
          }
          else
          {
            if (strncmp(line, "recovery:", 9) != 0)
            {
              TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format (Could not find 'recovery:' field). Trade Server can not continue.\n", fullPath);
              sleep(3); exit(0);
            }
          
            groupIndex++;
            sectionIndex = 6;
          }
          break;
      }
    }
  }

  // Close the file
  fclose(fileDesc);

  if (groupIndex == MAX_EDGX_BOOK_GROUP)  // Number of lines are exact what we expect
  {
    TraceLog(DEBUG_LEVEL, "EDGX Configuration:\n");
    TraceLog(DEBUG_LEVEL, "Primary Interface: %s\n", EDGX_BOOK_Conf.NICName);
    TraceLog(DEBUG_LEVEL, "Backup Interface: %s\n", EDGX_BOOK_Conf.backupNIC);
    TraceLog(DEBUG_LEVEL, "Cumulative packet loss: %d\n", EDGX_BOOK_Conf.recoveryThreshold);
    
    for (i=0; i < MAX_EDGX_BOOK_GROUP; i++)
    {
      TraceLog(DEBUG_LEVEL, "EDGX Group %d Primary feed: %s:%d\n", i+1, EDGX_BOOK_Conf.group[i].dataIP, EDGX_BOOK_Conf.group[i].dataPort);
      if (EDGX_BOOK_Conf.group[i].backupIP[0] != 0)
      {
        TraceLog(DEBUG_LEVEL, "EDGX Group %d backup feed: %s:%d\n", i+1, EDGX_BOOK_Conf.group[i].backupIP, EDGX_BOOK_Conf.group[i].backupPort);
      }
    }
    return SUCCESS;
  }
  else  //Indicates that file has not enough information
  {
    TraceLog(ERROR_LEVEL, "EDGX Book configuration file (%s) has not enough information!\n", fullPath);
    sleep(3); exit(0);
    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  Load_EDGA_BookConfig
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int Load_EDGA_BookConfig(const char *fileName)
{
  char fullPath[MAX_PATH_LEN];

  if (GetFullConfigPath(fileName, CONF_BOOK, fullPath) == NULL_POINTER)
  {
    return ERROR;
  }

  FILE* fileDesc = fopen(fullPath, "r");
  
  TraceLog(DEBUG_LEVEL, "Loading %s...\n", fullPath);

  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Critical! Could not open file (%s). Trade Server can not continue.\n", fullPath);
    PrintErrStr(ERROR_LEVEL, "calling fopen(): ", errno);
    sleep(3); exit(0);
    return ERROR;
  }

  //Open file successfully
  int i;
  for (i = 0; i < MAX_EDGA_BOOK_GROUP; i++)
  {
    //clear primary connection contents
    memset(EDGA_BOOK_Conf.group[i].dataIP, 0, MAX_LINE_LEN);
    EDGA_BOOK_Conf.group[i].dataPort = 0;

    memset(EDGA_BOOK_Conf.group[i].backupIP, 0, MAX_LINE_LEN);
    EDGA_BOOK_Conf.group[i].backupPort = 0;
  }
  
  // Clear NIC Name for used with DBL connections
  memset(EDGA_BOOK_Conf.NICName, 0, MAX_LINE_LEN);

  // Buffer to store each line in the configuration file
  char line[1024];

  // Number of lines read from the configuration file
  int  sectionIndex = 0;

  //Clear the buffer pointed to by line
  memset(line, 0, 1024);
  
  int groupIndex = 0;

  EDGA_BOOK_Conf.countRef = 0;
  
  // Get each line from the configuration file
  while (fgets(line, 1024, fileDesc) != NULL)
  {
    RemoveCharacters(line, strlen(line));
    if (!strchr(line, '#') && (strlen(line) != 0))
    {
      switch (sectionIndex)
      {
        case 0:
          EDGA_BOOK_Conf.recoveryThreshold = atoi(line);
          sectionIndex++;
          break;
        case 1:
          strcpy(EDGA_BOOK_Conf.NICName, line);
          sectionIndex++;
          break;
        case 2:
          strcpy(EDGA_BOOK_Conf.backupNIC, line);
          sectionIndex++;
          break;
        case 3:
          if (strncmp(line, "request:", 8) != 0)
          {
            TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format (Could not find 'request:' field). Trade Server can not continue.\n", fullPath);
            sleep(3); exit(0);
          }
          sectionIndex++;
          break;
        case 4: //username
          sectionIndex++;
          break;
        case 5: //password
          sectionIndex++;
          break;
        case 6:
          if (groupIndex == MAX_EDGA_BOOK_GROUP)
          {
            if (strncmp(line, "ref:", 4) == 0)
            {
              if (GetAddressFromConfig(&line[4], EDGA_BOOK_Conf.refIP[EDGA_BOOK_Conf.countRef], &EDGA_BOOK_Conf.refPort[EDGA_BOOK_Conf.countRef]) == ERROR)
              {
                TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format. Trade Server can not continue.\n", fullPath);
                sleep(3); exit(0);
                return ERROR;
              }
              
              EDGA_BOOK_Conf.countRef++;
              break;
            }
            else
            {
              TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format. Trade Server can not continue.\n", fullPath);
              sleep(3); exit(0);
              return ERROR;
            }
          }
          
          if (strncmp(line, "primary:", 8) != 0)
          {
            TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format (Could not find 'primary:' field). Trade Server can not continue.\n", fullPath);
            sleep(3); exit(0);
          }
          
          if (GetAddressFromConfig(&line[8], EDGA_BOOK_Conf.group[groupIndex].dataIP, &EDGA_BOOK_Conf.group[groupIndex].dataPort) == ERROR)
          {
            TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format ('primary:' field). Trade Server can not continue.\n", fullPath);
            sleep(3); exit(0);
          }
          
          strcpy(EDGA_BOOK_Conf.refIP[EDGA_BOOK_Conf.countRef], EDGA_BOOK_Conf.group[groupIndex].dataIP);
          EDGA_BOOK_Conf.refPort[EDGA_BOOK_Conf.countRef++] = EDGA_BOOK_Conf.group[groupIndex].dataPort;
          
          sectionIndex++;
          break;
        case 7: //Backup feed or retransmission feed
          if (strncmp(line, "backup:", 7) == 0)
          {
            if (GetAddressFromConfig(&line[7], EDGA_BOOK_Conf.group[groupIndex].backupIP, &EDGA_BOOK_Conf.group[groupIndex].backupPort) == ERROR)
            {
              TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format ('backup:' field). Trade Server can not continue.\n", fullPath);
              sleep(3); exit(0);
            }
            
            strcpy(EDGA_BOOK_Conf.refIP[EDGA_BOOK_Conf.countRef], EDGA_BOOK_Conf.group[groupIndex].backupIP);
            EDGA_BOOK_Conf.refPort[EDGA_BOOK_Conf.countRef++] = EDGA_BOOK_Conf.group[groupIndex].backupPort;
          }
          else
          {
            if (strncmp(line, "recovery:", 9) != 0)
            {
              TraceLog(ERROR_LEVEL, "Critical! file (%s) has invalid format (Could not find 'recovery:' field). Trade Server can not continue.\n", fullPath);
              sleep(3); exit(0);
            }
          
            groupIndex++;
            sectionIndex = 6;
          }
          break;
      }
    }
  }

  // Close the file
  fclose(fileDesc);

  if (groupIndex == MAX_EDGA_BOOK_GROUP)  // Number of lines are exact what we expect
  {
    TraceLog(DEBUG_LEVEL, "EDGA Configuration:\n");
    TraceLog(DEBUG_LEVEL, "Primary Interface: %s\n", EDGA_BOOK_Conf.NICName);
    TraceLog(DEBUG_LEVEL, "Backup Interface: %s\n", EDGA_BOOK_Conf.backupNIC);
    TraceLog(DEBUG_LEVEL, "Cumulative packet loss: %d\n", EDGA_BOOK_Conf.recoveryThreshold);
    
    for (i=0; i < MAX_EDGA_BOOK_GROUP; i++)
    {
      TraceLog(DEBUG_LEVEL, "EDGA Group %d Primary feed: %s:%d\n", i+1, EDGA_BOOK_Conf.group[i].dataIP, EDGA_BOOK_Conf.group[i].dataPort);
      if (EDGA_BOOK_Conf.group[i].backupIP[0] != 0)
      {
        TraceLog(DEBUG_LEVEL, "EDGA Group %d backup feed: %s:%d\n", i+1, EDGA_BOOK_Conf.group[i].backupIP, EDGA_BOOK_Conf.group[i].backupPort);
      }
    }
    return SUCCESS;
  }
  else  //Indicates that file has not enough information
  {
    TraceLog(ERROR_LEVEL, "EDGA Book configuration file (%s) has not enough information!\n", fullPath);
    sleep(3); exit(0);
    return ERROR;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadAlgorithmConfig
****************************************************************************/
int LoadAlgorithmConfig(const char *fileName, t_Algorithm_Config *configInfo)
{
  // Get full configuration path
  char fullPath[MAX_PATH_LEN];
  
  if (GetFullConfigPath(fileName, CONF_MISC, fullPath) == NULL_POINTER)
  {
    return ERROR;
  }

  FILE *fileDesc = fopen(fullPath, "r");
  
  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Critical! Could not open file '%s' to read. Trade Server could not continue.\n", fullPath);
    PrintErrStr(ERROR_LEVEL, "calling fopen(): ", errno);
    sleep(3); exit(0);
    
    return ERROR;
  }
  
  int numSection = SEC_GLOBAL_CONFIG;
  
  while (1)
  {
    switch (numSection)
    {
      case SEC_GLOBAL_CONFIG:
        // Load global configuration section
        if (LoadGlobalConfiguration(&(configInfo->globalConfig), fileDesc) == ERROR)
        {
          fclose(fileDesc);
          TraceLog(ERROR_LEVEL, "Critical! file '%s', section %d has invalid format. Trade Server could not continue.\n", fullPath, numSection);
          sleep(3); exit(0);
          return ERROR;
        }
        break;
      case SEC_BOOK_ROLE:
        // Load Book role configuration section
        if (LoadBookRoleConfiguration(&(configInfo->bookRole), fileDesc) == ERROR)
        {
          fclose(fileDesc);
          TraceLog(ERROR_LEVEL, "Critical! file '%s', section %d has invalid format. Trade Server could not continue.\n", fullPath, numSection);
          sleep(3); exit(0);
          return ERROR;
        }
        break;
      case SEC_MAX_SPREAD:
        // Load "Maximum spread is used by Books" section
        if (LoadBookMaxSpread(&(configInfo->bookMaxSpread), fileDesc) == ERROR)
        {
          fclose(fileDesc);
          TraceLog(ERROR_LEVEL, "Critical! file '%s', section %d has invalid format. Trade Server could not continue.\n", fullPath, numSection);
          sleep(3); exit(0);
          return ERROR;
        }
        break;
      case SEC_BOOK_PRIORITY:
        // Load "Book Priority" section
        if (LoadBookPriority(&(configInfo->bookPriority), fileDesc) == ERROR)
        {
          fclose(fileDesc);
          TraceLog(ERROR_LEVEL, "Critical! file '%s', section %d has invalid format. Trade Server could not continue.\n", fullPath, numSection);
          sleep(3); exit(0);
          return ERROR;
        }
        break;
      case SEC_OVERSIZE_SCORE:
        // Load "Book oversize score" section
        if (LoadBookOversizeScore(&(configInfo->bookOversizeScore), fileDesc) == ERROR)
        {
          fclose(fileDesc);
          TraceLog(ERROR_LEVEL, "Critical! file '%s', section %d has invalid format. Trade Server could not continue.\n", fullPath, numSection);
          sleep(3); exit(0);
          return ERROR;
        }
        break;
      case SEC_ORDER_ENTRY:
        // Load "Order Entry placement"
        if (LoadEntryOrderPlacement(configInfo->entryOrderPlacement, fileDesc) == ERROR)
        {
          fclose(fileDesc);
          TraceLog(ERROR_LEVEL, "Critical! file '%s', section %d has invalid format. Trade Server could not continue.\n", fullPath, numSection);
          sleep(3); exit(0);
          return ERROR;
        }
        
        break;
      case SEC_SPLIT_CONFIG:
        // Load split configuration
        if (LoadSplitConfiguration(&configInfo->splitConfig, fileDesc) == ERROR)
        {
          fclose(fileDesc);
          TraceLog(ERROR_LEVEL, "Critical! file '%s', section %d has invalid format. Trade Server could not continue.\n", fullPath, numSection);
          sleep(3); exit(0);
          return ERROR;
        }
        break;
      case SEC_NASDAQ_STRATEGY:
        // Load NASDAQ Strategy
        if (LoadNASDAQStrategyConfiguration(&configInfo->NASDAQStrategyConfig, fileDesc) == ERROR)
        {
          fclose(fileDesc);
          TraceLog(ERROR_LEVEL, "Critical! file '%s', section %d has invalid format. Trade Server could not continue.\n", fullPath, numSection);
          sleep(3); exit(0);
          return ERROR;
        }
        break;
      case SEC_MAXIMUM_SPREAD:
        // Maximum spread
        if (LoadMaxSpreadConfiguration(&configInfo->maxSpreadConfig, fileDesc) == ERROR)
        {
          fclose(fileDesc);
          TraceLog(ERROR_LEVEL, "Critical! file '%s', section %d has invalid format. Trade Server could not continue.\n", fullPath, numSection);
          sleep(3); exit(0);
          return ERROR;
        }
        break;
      case SEC_ORDER_TOKEN_BUCKET:
        if (LoadOrderTokenBucketConfiguration(&configInfo->orderBucket, fileDesc) == ERROR)
        {
          fclose(fileDesc);
          TraceLog(ERROR_LEVEL, "Critical! file '%s', section %d has invalid format. Trade Server could not continue.\n", fullPath, numSection);
          sleep(3); exit(0);
          return ERROR;
        }
        
        fclose(fileDesc);
        return SUCCESS;
        break;
      default:
        fclose(fileDesc);
        TraceLog(ERROR_LEVEL, "Critical! file '%s', section %d has invalid format. Trade Server could not continue.\n", fullPath, numSection);
        sleep(3); exit(0);
        return ERROR;
    }
    
    numSection++;
  }
}

/****************************************************************************
- Function name:  LoadGlobalConfiguration
****************************************************************************/
int LoadGlobalConfiguration(t_Global_Config *globalConfig, FILE *fileDesc)
{
  char tempBuffer[1024];
  int numField = 0;
  char *fieldValue;
  int countSection = 0;
  
  while (fgets(tempBuffer, 1024, fileDesc) != NULL)
  {
    // Ignore comment lines
    if (strchr(tempBuffer, '#') != NULL) continue;
    
    // Check for new section
    if (strchr(tempBuffer, '[') != NULL)
    {
      if (countSection == 1)
      {
        return SUCCESS;
      }
      else
      {
        countSection++;
        continue;
      }
    }
            
    // Remove unused character
    RemoveCharacters(tempBuffer, strlen(tempBuffer));
    
    // Remove blank lines
    if (strlen(tempBuffer) == 0) continue;
    
    // Get value field
    fieldValue = strchr(tempBuffer, '=');
        
    // Skip '='
    fieldValue++;
    
    t_Parameters paraList;
    
    switch (numField) 
    {
      case 0: // Maximum shares
        globalConfig->maxShareVolume = atoi(fieldValue);
        break;
      case 1: // Maximum buying power
        globalConfig->maxBuyingPower = atof(fieldValue);
        TSCurrentStatus.buyingPower = (long)(AlgorithmConfig.globalConfig.maxBuyingPower * 100.0);
        break;
      case 2: // Maximum consecutive losers
        globalConfig->maxConsecutiveLoser = atoi(fieldValue);
        break;
      case 3: // Maximum stuck positions
        globalConfig->maxStuckPosition = atoi(fieldValue);
        break;
      case 4: // Minimum spread
        globalConfig->minSpread = atof(fieldValue);
        break;
      case 5: // Maximum spread
        globalConfig->maxSpread = atof(fieldValue);
        break;
      case 6: // SEC Fee
        globalConfig->secFee = atof(fieldValue);
        break;
      case 7: // Commission
        globalConfig->commission = atof(fieldValue);
        SecFeeAndCommissionInCent = (100.00 * (globalConfig->commission + globalConfig->secFee));
        break;
      case 8: //loser rate
        globalConfig->loserRate = atof(fieldValue);
        break;
      case 9: // EX Ratio
        globalConfig->exRatio = atof(fieldValue);
        break;
      case 10: // Server role
        while (*fieldValue == 32)
        {
          fieldValue++;
        }
        
        if (strncmp(fieldValue , "ARCA", 4) == 0)
        {
          globalConfig->serverRole = SERVER_ROLE_ARCA;
        }
        else if (strncmp(fieldValue , "NASDAQ", 6) == 0)
        {
          globalConfig->serverRole = SERVER_ROLE_NASDAQ;
        }
        else if (strncmp(fieldValue , "BATS", 4) == 0)
        {
          globalConfig->serverRole = SERVER_ROLE_BATS;
        }
        else if (strncmp(fieldValue , "EDGE", 4) == 0)
        {
          globalConfig->serverRole = SERVER_ROLE_EDGE;
        }
        else if (strncmp(fieldValue , "STAND_ALONE", 11) == 0)
        {
          globalConfig->serverRole = SERVER_ROLE_STAND_ALONE;
        }
        else
        {
          TraceLog(ERROR_LEVEL, "Invalid server role value field. Only ARCA, NASDAQ, BATS, EDGE and STAND_ALONE values are supported\n");
          return ERROR;
        }
        memset(tempBuffer, 0, 1024);
        break;
      case 11:  //Nasdaq Auction Begin-time
        paraList.countParameters = Lrc_Split(fieldValue, ':', &paraList);
        globalConfig->nasdaqAuctionBeginTime = atoi(paraList.parameterList[0]) * 3600 
          + atoi(paraList.parameterList[1]) * 60 + atoi(paraList.parameterList[2]);
        if (globalConfig->nasdaqAuctionBeginTime > 9*3600+30*60)
        {
          TraceLog(ERROR_LEVEL, "Invalid Nasdaq Auction begin-time value\n");
          return ERROR;
        }
        break;
      case 12:  //Nasdaq Auction End-time
        paraList.countParameters = Lrc_Split(fieldValue, ':', &paraList);
        globalConfig->nasdaqAuctionEndTime = atoi(paraList.parameterList[0]) * 3600 
          + atoi(paraList.parameterList[1]) * 60 + atoi(paraList.parameterList[2]);
        if (globalConfig->nasdaqAuctionEndTime > 9*3600+30*60)
        {
          TraceLog(ERROR_LEVEL, "Invalid Nasdaq Auction end-time value\n");
          return ERROR;
        }
        memset(tempBuffer, 0, 1024);
        break;
      default:
        
        TraceLog(ERROR_LEVEL, "Invalid Global configuration section %s\n", tempBuffer);
        return ERROR;
    }
    
    numField++;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadBookRoleConfiguration
****************************************************************************/
int LoadBookRoleConfiguration(t_Book_Role *bookRole, FILE *fileDesc)
{
  char tempBuffer[1024];
  char *fieldValue;
  
  // Initialize book role configuration
  int i;
  for (i = 0; i < MAX_ECN_BOOK; i++)
  {
    bookRole->status[i] = DEFAULT;
  }
  
  while (fgets(tempBuffer, 1024, fileDesc) != NULL)
  {
    // Ignore comment lines
    if (strchr(tempBuffer, '#') != NULL)
    {
      continue;
    }
    
    // Check for new section
    if (strchr(tempBuffer, '[') != NULL)
    {
      return SUCCESS;
    }
    
    // Remove unused characters
    RemoveCharacters(tempBuffer, strlen(tempBuffer));
    
    // Remove blank lines
    if (strlen(tempBuffer) == 0)
    {
      continue;
    }
    
    // Get value field
    fieldValue = strchr(tempBuffer, '=');
        
    // Skip '='
    fieldValue++;
    
    // Get Book index by name
    
    int bookIndex = GetBookIndexByName(tempBuffer);
  
    if (bookIndex != -1)
    {
      while (*fieldValue == 32)
      {
        fieldValue++;
      }
      
      if (strncmp(fieldValue, "ENABLE", 6) == 0)
      {
        bookRole->status[bookIndex] = ENABLE;
      }
      else if (strncmp(fieldValue, "DISABLE", 7) == 0)
      {
        bookRole->status[bookIndex] = DISABLE;
      }
      else
      {
        return ERROR;
      }
    }
    else
    {
      return ERROR;
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadBookMaxSpread
****************************************************************************/
int LoadBookMaxSpread(t_Book_MaxSpread *bookMaxSpread, FILE *fileDesc)
{
  char tempBuffer[1024];
  char *fieldValue;
  
  // Initialize book role configuration
  int i;
  
  for (i = 0; i < MAX_ECN_BOOK; i++)
  {
    bookMaxSpread->status[i] = DEFAULT;
  }
  
  while (fgets(tempBuffer, 1024, fileDesc) != NULL)
  {
    // Ignore comment lines
    if (strchr(tempBuffer, '#') != NULL)
    {
      continue;
    }
    
    // Check for new section
    if (strchr(tempBuffer, '[') != NULL)
    {
      return SUCCESS;
    }
        
    // Remove unused character
    RemoveCharacters(tempBuffer, strlen(tempBuffer));
    
    // Remove blank lines
    if (strlen(tempBuffer) == 0)
    {
      continue;
    }
    
    // Get value field
    fieldValue = strchr(tempBuffer, '=');
        
    // Skip '='
    fieldValue++;
    
    // Get Book index by name
    int bookIndex = GetBookIndexByName(tempBuffer);
    
    if (bookIndex != -1)
    {
      // Remove white space characters
      while (*fieldValue == 32)
      {
        fieldValue++;
      }
      
      if (strncmp(fieldValue, "ENABLE", 6) == 0)
      {
        bookMaxSpread->status[bookIndex] = ENABLE;
      }
      else if (strncmp(fieldValue, "DISABLE", 7) == 0)
      {
        bookMaxSpread->status[bookIndex] = DISABLE;
      }
      else
      {
        return ERROR;
      }
    }
    else
    {
      return ERROR;
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadBookPriority
****************************************************************************/
int LoadBookPriority(t_Book_Priority *bookPriority, FILE *fileDesc)
{
  char tempBuffer[1024];
  char *fieldValue;
  
  // Initialize book role configuration
  int i;
  
  for (i = 0; i < MAX_ECN_BOOK; i++)
  {
    bookPriority->status[i] = MAX_ECN_BOOK;
  }
  
  while (fgets(tempBuffer, 1024, fileDesc) != NULL)
  {
    // Ignore comment lines
    if (strchr(tempBuffer, '#') != NULL)
    {
      continue;
    }
    
    // Check for new section
    if (strchr(tempBuffer, '[') != NULL)
    {
      return SUCCESS;
    }
        
    // Remove unused character
    RemoveCharacters(tempBuffer, strlen(tempBuffer));
    
    // Remove blank lines
    if (strlen(tempBuffer) == 0)
    {
      continue;
    }
    
    // Get value field
    fieldValue = strchr(tempBuffer, '=');
        
    // Skip '='
    fieldValue++;
    
    // Get Book index by name
    int bookIndex = GetBookIndexByName(tempBuffer);
    
    if (bookIndex != -1)
    {
      bookPriority->status[bookIndex] = atoi(fieldValue);
    }
    else
    {
      return ERROR;
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadBookOversizeScore
****************************************************************************/
int LoadBookOversizeScore(t_Book_OversizeScore *bookOversizeScore, FILE *fileDesc)
{
  char tempBuffer[1024];
  char *fieldValue;
  
  // Initialize book role configuration
  int i;  
  for (i = 0; i < MAX_ECN_BOOK; i++)
  {
    bookOversizeScore->oversizeScore[i] = 1.00;
  }
  
  while (fgets(tempBuffer, 1024, fileDesc) != NULL)
  {
    // Ignore comment lines
    if (strchr(tempBuffer, '#') != NULL)
    {
      continue;
    }
    
    // Check for new section
    if (strchr(tempBuffer, '[') != NULL)
    {
      break;
    }
        
    // Remove unused character
    RemoveCharacters(tempBuffer, strlen(tempBuffer));
    
    // Remove blank lines
    if (strlen(tempBuffer) == 0)
    {
      continue;
    }
    
    // Get value field
    fieldValue = strchr(tempBuffer, '=');
        
    // Skip '='
    fieldValue++;
    
    // Get Book index by name
    int bookIndex = GetBookIndexByName(tempBuffer);
    
    if (bookIndex != -1)
    {
      bookOversizeScore->oversizeScore[bookIndex] = atof(fieldValue);
      if (fle(bookOversizeScore->oversizeScore[bookIndex], 0.00))
      {
        bookOversizeScore->oversizeScore[bookIndex] = 1.00;
      }
    }
    else
    {
      return ERROR;
    }
  }
  
  TraceLog(DEBUG_LEVEL, "Book oversize score:\n");
  TraceLog(DEBUG_LEVEL, "ARCA oversize score = %.2lf\n", bookOversizeScore->oversizeScore[BOOK_ARCA]);
  TraceLog(DEBUG_LEVEL, "NASDAQ oversize score = %.2lf\n", bookOversizeScore->oversizeScore[BOOK_NASDAQ]);
  TraceLog(DEBUG_LEVEL, "BX oversize score = %.2lf\n", bookOversizeScore->oversizeScore[BOOK_NDBX]);
  TraceLog(DEBUG_LEVEL, "NYSE oversize score = %.2lf\n", bookOversizeScore->oversizeScore[BOOK_NYSE]);
  TraceLog(DEBUG_LEVEL, "AMEX oversize score = %.2lf\n", bookOversizeScore->oversizeScore[BOOK_AMEX]);
  TraceLog(DEBUG_LEVEL, "BATS-Z oversize score = %.2lf\n", bookOversizeScore->oversizeScore[BOOK_BATSZ]);
  TraceLog(DEBUG_LEVEL, "BYX oversize score = %.2lf\n", bookOversizeScore->oversizeScore[BOOK_BYX]);
  TraceLog(DEBUG_LEVEL, "EDGX oversize score = %.2lf\n", bookOversizeScore->oversizeScore[BOOK_EDGX]);
  TraceLog(DEBUG_LEVEL, "EDGA oversize score = %.2lf\n\n", bookOversizeScore->oversizeScore[BOOK_EDGA]);
  TraceLog(DEBUG_LEVEL, "PSX oversize score = %.2lf\n", bookOversizeScore->oversizeScore[BOOK_PSX]);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadSplitConfiguration
****************************************************************************/
int LoadSplitConfiguration(t_SymbolSplitConfiguration *splitConfig, FILE *fileDesc)
{
  char tempBuffer[1024];
  char *fieldValue;
  int numField = 0;
  int rangeID = -1;
  char startOfRange = 0;
  char endOfRange = 0;
  
  splitConfig->symbolRangeID = -1;
  splitConfig->startOfRange = 0;
  splitConfig->endOfRange = 0;
  int i;
  for (i = 0; i < 256; i++)
  {
    SplitSymbolMapping[i] = DISABLE;
  }
  
  while (fgets(tempBuffer, 1024, fileDesc) != NULL)
  {
    // Ignore comment lines
    if (strchr(tempBuffer, '#') != NULL)
    {
      continue;
    }
    
    // Check for new section
    if (strchr(tempBuffer, '[') != NULL)
    {
      break;
    }
        
    // Remove unused character
    RemoveCharacters(tempBuffer, strlen(tempBuffer));
    
    // Remove blank lines
    if (strlen(tempBuffer) == 0)
    {
      continue;
    }
    
    // Get value field
    fieldValue = strchr(tempBuffer, '=');
        
    // Skip '='
    fieldValue++;
    while (*fieldValue == 32)
    {
      fieldValue++;
    }
    
    if (numField == 0) // SYMBOL-RANGE-ID
    {
      if (strncmp(tempBuffer, "SYMBOL-RANGE-ID", 15) != 0)
      {
        TraceLog(ERROR_LEVEL, "Could not find/invalid Symbol Split information from configuration file\n");
        return ERROR;
      }
      
      rangeID = atoi(fieldValue);
      if (rangeID < 1 || rangeID > MAX_SYMBOL_SPLIT_RANGE_ID)
      {
        TraceLog(ERROR_LEVEL, "Invalid 'SYMBOL-RANGE-ID' from configuration file, allowed values: 1 to %d\n", MAX_SYMBOL_SPLIT_RANGE_ID);
        return ERROR;
      }
    }
    else if (numField == 1) //START-OF-RANGE
    {
      if (strncmp(tempBuffer, "START-OF-RANGE", 14) != 0)
      {
        TraceLog(ERROR_LEVEL, "Could not find 'START-OF-RANGE' of Symbol Split information from configuration file\n");
        return ERROR;
      }
      
      if (*fieldValue < 'A' || *fieldValue > 'Z')
      {
        TraceLog(ERROR_LEVEL, "Invalid 'START-OF-RANGE' from configuration file, allowed values: 'A' to 'Z'\n");
        return ERROR;
      }
      startOfRange = *fieldValue;
    }
    else if (numField == 2) //END-OF-RANGE
    {
      if (strncmp(tempBuffer, "END-OF-RANGE", 12) != 0)
      {
        TraceLog(ERROR_LEVEL, "Could not find 'END-OF-RANGE' of Symbol Split information from configuration file\n");
        return ERROR;
      }
      
      if (*fieldValue < 'A' || *fieldValue > 'Z')
      {
        TraceLog(ERROR_LEVEL, "Invalid 'END-OF-RANGE' from configuration file, allowed values: 'A' to 'Z'\n");
        return ERROR;
      }
      endOfRange = *fieldValue;
    }
    else
    {
      TraceLog(ERROR_LEVEL, "Invalid Symbol Split configuration, allowed 3 fields: range ID, start, end\n");
      return ERROR;
    }
    numField++;
  }
  
  if (endOfRange < startOfRange)
  {
    TraceLog(ERROR_LEVEL, "Invalid Symbol Split Range: start of Range (%d), end of range (%d)\n", startOfRange, endOfRange);
    return ERROR;
  }
  
  if (rangeID == -1 || startOfRange == 0 || endOfRange == 0)
  {
    TraceLog(ERROR_LEVEL, "Could not find/invalid Symbol Split information from configuration file\n");
    return ERROR;
  }
  
  splitConfig->symbolRangeID = rangeID - 1;
  splitConfig->startOfRange = startOfRange;
  splitConfig->endOfRange = endOfRange;
  for (i = startOfRange; i <= endOfRange; i++)
  {
    SplitSymbolMapping[i] = ENABLE;
  }
  
  TraceLog(DEBUG_LEVEL, "Split Symbol Range ID: %d\n", splitConfig->symbolRangeID + 1);
  TraceLog(DEBUG_LEVEL, "Start of Range: %c\n", splitConfig->startOfRange);
  TraceLog(DEBUG_LEVEL, "End of Range:   %c\n", splitConfig->endOfRange);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadNASDAQStrategyConfiguration
****************************************************************************/
int LoadNASDAQStrategyConfiguration(t_NASDAQ_StrategyConfiguration *NASDAQStrategyConfig, FILE *fileDesc)
{
  char tempBuffer[1024];
  char *fieldValue;
  int currentTradeSide = 0;
  int countLine = 0;
  
  while (fgets(tempBuffer, 1024, fileDesc) != NULL)
  {
    // Ignore comment lines
    if (strchr(tempBuffer, '#') != NULL)
    {
      continue;
    }
    
    // Check for new section
    if (strchr(tempBuffer, '[') != NULL)
    {
      break;
    }
    
    // Remove unused character
    RemoveCharacters(tempBuffer, strlen(tempBuffer));
    
    // Remove blank lines
    if (strlen(tempBuffer) == 0)
    {
      continue;
    }
    
    // Get value field
    fieldValue = strchr(tempBuffer, '=');
    
    // Skip '='
    fieldValue++;
    while (*fieldValue == 32)
    {
      fieldValue++;
    }
    
    //TraceLog(DEBUG_LEVEL, "fieldValue = %s, tempBuffer = %s\n\n", fieldValue, tempBuffer);
    
    // Check Trade Side
    if ( strncmp(tempBuffer, "Trade side", 10) == 0)
    {
      if (strncmp(fieldValue, "ENTRY", 5) == 0)
      {
        currentTradeSide = ENTRY;
      }
      else
      {
        currentTradeSide = EXIT;
      }
    }
    else if (strncmp(tempBuffer, "pre-market-RASH-strategy", 24) == 0)
    {
      strcpy(NASDAQStrategyConfig->RASH_strategy[currentTradeSide][PRE_MARKET], fieldValue);
    }
    else if (strncmp(tempBuffer, "intraday-RASH-strategy", 22) == 0)
    {
      strcpy(NASDAQStrategyConfig->RASH_strategy[currentTradeSide][INTRADAY], fieldValue);
    }
    else if (strncmp(tempBuffer, "post-market-RASH-strategy", 25) == 0)
    {
      strcpy(NASDAQStrategyConfig->RASH_strategy[currentTradeSide][POST_MARKET], fieldValue);
    }
    else
    {
      TraceLog(WARN_LEVEL, "Unknown line %s\n", tempBuffer);
    }
    
    countLine++;
  }
  
  if (countLine != 8)
  {
    TraceLog(ERROR_LEVEL, "Invalid NASDAQ Strategy configuration section\n");
    return ERROR;
  } 
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadMaxSpreadConfiguration
****************************************************************************/
int LoadMaxSpreadConfiguration(t_MaxSpreadConfiguration *maxSpreadConfig, FILE *fileDesc)
{
  char tempBuffer[1024];
  char *fieldValue;
  int currentMarketTime = 0;
  double maxSpread;
  int countLine = 0;
  
  while (fgets(tempBuffer, 1024, fileDesc) != NULL)
  {
    // Ignore comment lines
    if (strchr(tempBuffer, '#') != NULL)
    {
      continue;
    }
    
    // Check for new section
    if (strchr(tempBuffer, '[') != NULL)
    {
      break;
    }
        
    // Remove unused character
    RemoveCharacters(tempBuffer, strlen(tempBuffer));
    
    // Remove blank lines
    if (strlen(tempBuffer) == 0)
    {
      continue;
    }
    
    // Get value field
    fieldValue = strchr(tempBuffer, '=');
        
    // Skip '='
    fieldValue++;
    while (*fieldValue == 32)
    {
      fieldValue++;
    }
    
    //TraceLog(DEBUG_LEVEL, "fieldValue = %s, tempBuffer = %s\n\n", fieldValue, tempBuffer);
    
    // Check Trade Side
    if ( strncmp(tempBuffer, "Market type", 11) == 0)
    {
      if (strncmp(fieldValue, "Regular Session", 5) == 0)
      {
        currentMarketTime = INTRADAY;
      }
      else
      {
        currentMarketTime = PRE_MARKET;
      }
    }
    else if (strncmp(tempBuffer, "Execution price is $25 and under", 32) == 0)
    {
      maxSpread = atof(fieldValue);

      if (fle(maxSpread, 0.00) || fge(maxSpread, 0.50))
      {
        TraceLog(ERROR_LEVEL, "Invalid max spread with execution price is $25 and under, maxSpread = %lf\n", maxSpread);

        return ERROR;
      }

      if (currentMarketTime == INTRADAY)
      {
        maxSpreadConfig->maxSpread[INTRADAY].execPriceUnder_25USD = maxSpread;
      }
      else
      {
        maxSpreadConfig->maxSpread[PRE_MARKET].execPriceUnder_25USD = maxSpread;
        maxSpreadConfig->maxSpread[POST_MARKET].execPriceUnder_25USD = maxSpread;
      }
    }
    else if (strncmp(tempBuffer, "Execution price is over $25 and up to $50", 41) == 0)
    {
      maxSpread = atof(fieldValue);

      if (fle(maxSpread, 0.00) || fge(maxSpread, 0.50))
      {
        TraceLog(ERROR_LEVEL, "Invalid max spread with execution price is over $25 and up to $50, maxSpread = %lf\n", maxSpread);

        return ERROR;
      }

      if (currentMarketTime == INTRADAY)
      {
        maxSpreadConfig->maxSpread[INTRADAY].execPriceUnder_50USD = maxSpread;
      }
      else
      {
        maxSpreadConfig->maxSpread[PRE_MARKET].execPriceUnder_50USD = maxSpread;
        maxSpreadConfig->maxSpread[POST_MARKET].execPriceUnder_50USD = maxSpread;
      }
    }
    else if (strncmp(tempBuffer, "Execution price is over $50", 27) == 0)
    {
      maxSpread = atof(fieldValue);

      if (fle(maxSpread, 0.00) || fge(maxSpread, 0.50))
      {
        TraceLog(ERROR_LEVEL, "Invalid max spread with execution price is over $50, maxSpread = %lf\n", maxSpread);

        return ERROR;
      }

      if (currentMarketTime == INTRADAY)
      {
        maxSpreadConfig->maxSpread[INTRADAY].execPriceOver_50USD = maxSpread;
      }
      else
      {
        maxSpreadConfig->maxSpread[PRE_MARKET].execPriceOver_50USD = maxSpread;
        maxSpreadConfig->maxSpread[POST_MARKET].execPriceOver_50USD = maxSpread;
      }
    }
    else if (strncmp(tempBuffer, "Regular session begin hour", 26) == 0)
    {
      TradingSessionMgmt.RegularSessionBeginHour = atoi(fieldValue);

      if ((TradingSessionMgmt.RegularSessionBeginHour < 0) || (TradingSessionMgmt.RegularSessionBeginHour > 24))
      {
        TraceLog(ERROR_LEVEL, "Invalid Regular Session Begin Hour value (%d)\n", TradingSessionMgmt.RegularSessionBeginHour);

        return ERROR;
      }
    }
    else if (strncmp(tempBuffer, "Regular session begin min", 25) == 0)
    {
      TradingSessionMgmt.RegularSessionBeginMin = atoi(fieldValue);

      if ((TradingSessionMgmt.RegularSessionBeginMin < 0) || (TradingSessionMgmt.RegularSessionBeginMin > 60))
      {
        TraceLog(ERROR_LEVEL, "Invalid Regular Session Begin Min value (%d)\n", TradingSessionMgmt.RegularSessionBeginMin);

        return ERROR;
      }
    }
    else if (strncmp(tempBuffer, "Regular session end hour", 24) == 0)
    {
      TradingSessionMgmt.RegularSessionEndHour = atoi(fieldValue);

      if ((TradingSessionMgmt.RegularSessionEndHour < 0) || (TradingSessionMgmt.RegularSessionEndHour > 24))
      {
        TraceLog(ERROR_LEVEL, "Invalid Regular Session End Hour value (%d)\n", TradingSessionMgmt.RegularSessionEndHour);

        return ERROR;
      }
    }
    else if (strncmp(tempBuffer, "Regular session end min", 23) == 0)
    {
      TradingSessionMgmt.RegularSessionEndMin = atoi(fieldValue);

      if ((TradingSessionMgmt.RegularSessionEndMin < 0) || (TradingSessionMgmt.RegularSessionEndMin > 60))
      {
        TraceLog(ERROR_LEVEL, "Invalid Regular Session End Min value (%d)\n", TradingSessionMgmt.RegularSessionEndMin);

        return ERROR;
      }
    }
    else
    {
      TraceLog(ERROR_LEVEL, "LoadMaxSpreadConfiguration: Unknown line %s\n", tempBuffer);
    }
    
    countLine++;
  }
  
  if (countLine != 12)
  {
    TraceLog(ERROR_LEVEL, "Invalid Max Spread configuration section\n");
    return ERROR;
  }
  
  TraceLog(DEBUG_LEVEL, "Max spread for Regular Session\n");
  TraceLog(DEBUG_LEVEL, "Execution price is $25 and under, maxSpread = %lf\n", maxSpreadConfig->maxSpread[INTRADAY].execPriceUnder_25USD);
  TraceLog(DEBUG_LEVEL, "Execution price is over $25 and up to $50, maxSpread = %lf\n", maxSpreadConfig->maxSpread[INTRADAY].execPriceUnder_50USD);
  TraceLog(DEBUG_LEVEL, "Execution price is over $50, maxSpread = %lf\n\n", maxSpreadConfig->maxSpread[INTRADAY].execPriceOver_50USD);

  TraceLog(DEBUG_LEVEL, "Max spread for Outside Regular Session\n");
  TraceLog(DEBUG_LEVEL, "Execution price is $25 and under, maxSpread = %lf\n", maxSpreadConfig->maxSpread[PRE_MARKET].execPriceUnder_25USD);
  TraceLog(DEBUG_LEVEL, "Execution price is over $25 and up to $50, maxSpread = %lf\n", maxSpreadConfig->maxSpread[PRE_MARKET].execPriceUnder_50USD);
  TraceLog(DEBUG_LEVEL, "Execution price is over $50, maxSpread = %lf\n\n", maxSpreadConfig->maxSpread[PRE_MARKET].execPriceOver_50USD);
  
  TraceLog(DEBUG_LEVEL, "Regular session begin hour = %d\n", TradingSessionMgmt.RegularSessionBeginHour);
  TraceLog(DEBUG_LEVEL, "Regular session begin min = %d\n", TradingSessionMgmt.RegularSessionBeginMin);
  TraceLog(DEBUG_LEVEL, "Regular session end hour = %d\n", TradingSessionMgmt.RegularSessionEndHour);
  TraceLog(DEBUG_LEVEL, "Regular session end min = %d\n\n", TradingSessionMgmt.RegularSessionEndMin);
  UpdateCurrentSessionType();
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadOrderTokenBucketConfiguration
- Usage:      N/A
****************************************************************************/
int LoadOrderTokenBucketConfiguration(t_OrderTokenBucket *orderBucket, FILE *fileDesc)
{
  char tempBuffer[1024];
  char *fieldValue;
  int countLine = 0;
  orderBucket->bucketSize = 0;
  orderBucket->refillPerSec = 0;
  
  while (fgets(tempBuffer, 1024, fileDesc) != NULL)
  {
    // Ignore comment lines
    if (strchr(tempBuffer, '#') != NULL)
    {
      continue;
    }
    
    // Check for new section
    if (strchr(tempBuffer, '[') != NULL)
    {
      break;
    }
        
    // Remove unused character
    RemoveCharacters(tempBuffer, strlen(tempBuffer));
    
    // Remove blank lines
    if (strlen(tempBuffer) == 0)
    {
      continue;
    }
    
    // Get value field
    fieldValue = strchr(tempBuffer, '=');
    
    if (!fieldValue)
    {
      TraceLog(ERROR_LEVEL, "Invalid Order Limit configuration section\n");
      return ERROR;
    }
    
    // Skip '='
    fieldValue++;
    while (*fieldValue == 32)
    {
      fieldValue++;
    }
    
    if ( strncmp(tempBuffer, "Bucket size", 11) == 0)
    {
      orderBucket->bucketSize = atoi(fieldValue);
    }
    else if (strncmp(tempBuffer, "Refill per second", 17) == 0)
    {
      orderBucket->refillPerSec = atoi(fieldValue);
    }
    else
    {
      TraceLog(ERROR_LEVEL, "LoadOrderTokenBucketConfiguration: Unknown line %s\n", tempBuffer);
    }
    
    countLine++;
  }
  
  if (countLine != 2)
  {
    TraceLog(ERROR_LEVEL, "Invalid Order Limit configuration section\n");
    return ERROR;
  }
  
  if (orderBucket->bucketSize == 0)
  {
    TraceLog(ERROR_LEVEL, "Invalid Order Limit configuration section, 'bucket size' cannot be zero\n");
    return ERROR;
  }
  
  if (orderBucket->refillPerSec == 0)
  {
    TraceLog(ERROR_LEVEL, "Invalid Order Limit configuration section, 'refill' cannot be zero\n");
    return ERROR;
  }
  
  // Set initial values
  orderBucket->tokens = orderBucket->bucketSize;
  orderBucket->lastUpdatedTime = 0;
  
  TraceLog(DEBUG_LEVEL, "Order Limit Configuration\n");
  TraceLog(DEBUG_LEVEL, "Bucket limit = %d\n", orderBucket->bucketSize);
  TraceLog(DEBUG_LEVEL, "Refill per second = %d\n", orderBucket->refillPerSec);
  
  int i;
  for (i=0; i<DBLFILTER_NUM_QUEUES; i++)
  {
    FilterBuffers[i].orderBucket->bucketSize = orderBucket->bucketSize;
    FilterBuffers[i].orderBucket->tokens = orderBucket->bucketSize;
    FilterBuffers[i].orderBucket->lastUpdatedTime = 0;
    FilterBuffers[i].orderBucket->refillPerSec = orderBucket->refillPerSec;
  }
  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadEntryOrderPlacement
****************************************************************************/
int LoadEntryOrderPlacement(t_Entry_Order_Placement entryOrderPlacement[MAX_SERVER_ROLE], FILE *fileDesc)
{
  char tempBuffer[1024];
  char *fieldValue;
  
  int serverRoleIndex;
  int ecnBookLaunchIndex;
  
  for (serverRoleIndex = 0; serverRoleIndex < MAX_SERVER_ROLE; serverRoleIndex++)
  {
    for (ecnBookLaunchIndex = 0; ecnBookLaunchIndex < MAX_ECN_BOOK; ecnBookLaunchIndex++)
    {
      entryOrderPlacement[serverRoleIndex].status[ecnBookLaunchIndex] = DISABLE;
    }
  }
  
  serverRoleIndex = -1;
  ecnBookLaunchIndex = -1;
    
  while (fgets(tempBuffer, 1024, fileDesc) != NULL)
  {
    // Ignore comment lines
    if (strchr(tempBuffer, '#') != NULL)
    {
      continue;
    }
    
    // Check for new section
    if (strchr(tempBuffer, '[') != NULL)
    {
      return SUCCESS;
    }
    
    // Check for new server role
    if (strcasestr(tempBuffer, "Server Role") != NULL_POINTER)
    {
      fieldValue = strchr(tempBuffer, '=');
      fieldValue++;
      
      // Remove white space characters
      while (*fieldValue == 32)
      {
        fieldValue++;
      }
      
      serverRoleIndex = GetServerIndexByName(fieldValue);
      
      if (serverRoleIndex == -1)
      {
        return ERROR;
      }
    }
    
    // Check for new ECN Book Launch
    if (strcasestr(tempBuffer, "Launch") != NULL_POINTER)
    {
      fieldValue = strchr(tempBuffer, '=');
      fieldValue++;
      
      // Remove white space characters
      while (*fieldValue == 32)
      {
        fieldValue++;
      }
      
      ecnBookLaunchIndex = GetBookIndexByName(fieldValue);
      
      if (ecnBookLaunchIndex == -1)
      {
        return ERROR;
      }
    }
    
    // Update data structure
    if (serverRoleIndex != -1)
    {
      if (ecnBookLaunchIndex == -1)
      {
        continue;
      }
      else if (ecnBookLaunchIndex == ANY_ECN)
      {
        int i;
        for (i = 0; i < MAX_ECN_BOOK; i++)
        {
          entryOrderPlacement[serverRoleIndex].status[i] = ENABLE;
        }
        
        TraceLog(DEBUG_LEVEL, "Server role index %d: Enabling order placement for all MDCs\n", serverRoleIndex);
      }
      else
      {
        TraceLog(DEBUG_LEVEL, "Server role index %d: Enabling order placement for book launch index %d\n", serverRoleIndex, ecnBookLaunchIndex);
        entryOrderPlacement[serverRoleIndex].status[ecnBookLaunchIndex] = ENABLE;
      }
      
      ecnBookLaunchIndex = -1;
    }       
  }
    
  return SUCCESS;
}

/****************************************************************************
- Function name:    LoadSplitSymbolInfo
- Input:      
- Output:     
- Return:     
- Description:  Load configuration of which channel will be joined
- Usage:      
****************************************************************************/
int LoadSplitSymbolInfo()
{
  TraceLog(DEBUG_LEVEL, "Load Split Symbol Config ...\n");
  
  memset(SymbolSplitInfo, DISABLE, sizeof(SymbolSplitInfo));
  
  char fullPath[MAX_PATH_LEN];
  
  if (GetFullConfigPath(FILE_NAME_OF_SPLIT_SYMBOL_INFO, CONF_MISC, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Could not get the path of file '%s'\n", FILE_NAME_OF_SPLIT_SYMBOL_INFO);
    return ERROR;
  }

  FILE *fileDesc = fopen(fullPath, "r");
  
  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Critical! Could not open file '%s' to read. Trade Server could not continue.\n", fullPath);
    PrintErrStr(ERROR_LEVEL, "calling fopen(): ", errno);
    sleep(3); exit(0);
    return ERROR;
  }
  
  // Create and initialize temporary buffer to contain a line of configuration information
  char tempBuffer[1024] = "\0";
  int numLine = 0;
  int groupID, rangeID, j;
  
  // Read configuration info and check valid configuration format
  while (fgets(tempBuffer, 1024, fileDesc) != NULL)
  {
    // Skip comment lines
    if (tempBuffer[0] == '#')
    {
      continue;
    }
    
    // Remove used characters
    RemoveCharacters(tempBuffer, strlen(tempBuffer));
    
    // Remove blank lines
    if (strlen(tempBuffer) == 0)
    {
      continue;
    }
    
    numLine++;
    
    if (numLine == 1) //header line
    {
      continue;
    }
    
    // Parse the line
    t_Parameters mdcParams;
    mdcParams.countParameters = Lrc_Split(tempBuffer, ',', &mdcParams);
    
    if (mdcParams.countParameters < 2)
    {
      TraceLog(ERROR_LEVEL, "Symbol Split Info: Invalid data '%s'\n", tempBuffer);
      return ERROR;
    }
    
    if (strncmp(mdcParams.parameterList[0], "NYSE", 4) == 0)
    {
      int maxNyseParam = 1 + MAX_NYSE_BOOK_GROUPS;
      if (mdcParams.countParameters < maxNyseParam)
      {
        TraceLog(ERROR_LEVEL, "Invalid Split Symbol Info for NYSE, require %d parameters\n", maxNyseParam);
        return ERROR;
      }
      
      for (groupID = 1; groupID < maxNyseParam; groupID++)
      {
        if (strchr(mdcParams.parameterList[groupID], '-') == NULL)
        {
          rangeID = atoi(mdcParams.parameterList[groupID]);
          if (rangeID >= 1 && rangeID <= MAX_SYMBOL_SPLIT_RANGE_ID)
          {
            SymbolSplitInfo[rangeID-1].nyse[groupID-1] = ENABLE;
          }
          else
          {
            TraceLog(ERROR_LEVEL, "Invalid Split Symbol Info for NYSE (groupID=%d rangeID=%d), range ID allows from 1 to %d\n", groupID, rangeID, MAX_SYMBOL_SPLIT_RANGE_ID);
            return ERROR;
          }
        }
        else
        {
          // Split to get all ranges for groupID
          t_Parameters groupParams;
          groupParams.countParameters = Lrc_Split(mdcParams.parameterList[groupID], '-', &groupParams);
          for (j = 0; j < groupParams.countParameters; j++)
          {
            rangeID = atoi(groupParams.parameterList[j]);
            if (rangeID >= 1 && rangeID <= MAX_SYMBOL_SPLIT_RANGE_ID)
            {
              SymbolSplitInfo[rangeID-1].nyse[groupID-1] = ENABLE;
            }
            else
            {
              TraceLog(ERROR_LEVEL, "Invalid Split Symbol Info for NYSE (groupID=%d rangeID=%d), range ID allows: 1 to %d\n", groupID, rangeID, MAX_SYMBOL_SPLIT_RANGE_ID);
              return ERROR;
            }
          }
        }
      }
    }
    else if (strncmp(mdcParams.parameterList[0], "BATSZ", 5) == 0)
    {
      int maxBatszParams = 1 + MAX_BATSZ_UNIT;
      if (mdcParams.countParameters < maxBatszParams)
      {
        TraceLog(ERROR_LEVEL, "Invalid Split Symbol Info for BATSZ, require %d parameters\n", maxBatszParams);
        return ERROR;
      }
      
      for (groupID = 1; groupID < maxBatszParams; groupID++)
      {
        if (strchr(mdcParams.parameterList[groupID], '-') == NULL)
        {
          rangeID = atoi(mdcParams.parameterList[groupID]);
          if (rangeID >= 1 && rangeID <= MAX_SYMBOL_SPLIT_RANGE_ID)
          {
            SymbolSplitInfo[rangeID-1].batsz[groupID-1] = ENABLE;
          }
          else
          {
            TraceLog(ERROR_LEVEL, "Invalid Split Symbol Info for BATSZ (groupID=%d rangeID=%d), range ID allows from 1 to %d\n", groupID, rangeID, MAX_SYMBOL_SPLIT_RANGE_ID);
            return ERROR;
          }
        }
        else
        {
          // Split to get all ranges for groupID
          t_Parameters groupParams;
          groupParams.countParameters = Lrc_Split(mdcParams.parameterList[groupID], '-', &groupParams);
          for (j = 0; j < groupParams.countParameters; j++)
          {
            rangeID = atoi(groupParams.parameterList[j]);
            if (rangeID >= 1 && rangeID <= MAX_SYMBOL_SPLIT_RANGE_ID)
            {
              SymbolSplitInfo[rangeID-1].batsz[groupID-1] = ENABLE;
            }
            else
            {
              TraceLog(ERROR_LEVEL, "Invalid Split Symbol Info for BATSZ (groupID=%d rangeID=%d), range ID allows: 1 to %d\n", groupID, rangeID, MAX_SYMBOL_SPLIT_RANGE_ID);
              return ERROR;
            }
          }
        }
      }
    }
    else if (strncmp(mdcParams.parameterList[0], "BYX", 3) == 0)
    {
      int maxBYXParams = 1 + MAX_BYX_UNIT;
      if (mdcParams.countParameters < maxBYXParams)
      {
        TraceLog(ERROR_LEVEL, "Invalid Split Symbol Info for BYX, require %d parameters\n", maxBYXParams);
        return ERROR;
      }
      
      for (groupID = 1; groupID < maxBYXParams; groupID++)
      {
        if (strchr(mdcParams.parameterList[groupID], '-') == NULL)
        {
          rangeID = atoi(mdcParams.parameterList[groupID]);
          if (rangeID >= 1 && rangeID <= MAX_SYMBOL_SPLIT_RANGE_ID)
          {
            SymbolSplitInfo[rangeID-1].byx[groupID-1] = ENABLE;
          }
          else
          {
            TraceLog(ERROR_LEVEL, "Invalid Split Symbol Info for BYX (groupID=%d rangeID=%d), range ID allows from 1 to %d\n", groupID, rangeID, MAX_SYMBOL_SPLIT_RANGE_ID);
            return ERROR;
          }
        }
        else
        {
          // Split to get all ranges for groupID
          t_Parameters groupParams;
          groupParams.countParameters = Lrc_Split(mdcParams.parameterList[groupID], '-', &groupParams);
          for (j = 0; j < groupParams.countParameters; j++)
          {
            rangeID = atoi(groupParams.parameterList[j]);
            if (rangeID >= 1 && rangeID <= MAX_SYMBOL_SPLIT_RANGE_ID)
            {
              SymbolSplitInfo[rangeID-1].byx[groupID-1] = ENABLE;
            }
            else
            {
              TraceLog(ERROR_LEVEL, "Invalid Split Symbol Info for BYX (groupID=%d rangeID=%d), range ID allows: 1 to %d\n", groupID, rangeID, MAX_SYMBOL_SPLIT_RANGE_ID);
              return ERROR;
            }
          }
        }
      }
    }
    else if (strncmp(mdcParams.parameterList[0], "EDGX", 4) == 0)
    {
      int maxEdgxParams = 1 + MAX_EDGX_BOOK_GROUP;
      if (mdcParams.countParameters < maxEdgxParams)
      {
        TraceLog(ERROR_LEVEL, "Invalid Split Symbol Info for EDGX, require %d parameters\n", maxEdgxParams);
        return ERROR;
      }
      
      for (groupID = 1; groupID < maxEdgxParams; groupID++)
      {
        if (strchr(mdcParams.parameterList[groupID], '-') == NULL)
        {
          rangeID = atoi(mdcParams.parameterList[groupID]);
          if (rangeID >= 1 && rangeID <= MAX_SYMBOL_SPLIT_RANGE_ID)
          {
            SymbolSplitInfo[rangeID-1].edgx[groupID-1] = ENABLE;
          }
          else
          {
            TraceLog(ERROR_LEVEL, "Invalid Split Symbol Info for EDGX (groupID=%d rangeID=%d), range ID allows from 1 to %d\n", groupID, rangeID, MAX_SYMBOL_SPLIT_RANGE_ID);
            return ERROR;
          }
        }
        else
        {
          // Split to get all ranges for groupID
          t_Parameters groupParams;
          groupParams.countParameters = Lrc_Split(mdcParams.parameterList[groupID], '-', &groupParams);
          for (j = 0; j < groupParams.countParameters; j++)
          {
            rangeID = atoi(groupParams.parameterList[j]);
            if (rangeID >= 1 && rangeID <= MAX_SYMBOL_SPLIT_RANGE_ID)
            {
              SymbolSplitInfo[rangeID-1].edgx[groupID-1] = ENABLE;
            }
            else
            {
              TraceLog(ERROR_LEVEL, "Invalid Split Symbol Info for EDGX (groupID=%d rangeID=%d), range ID allows: 1 to %d\n", groupID, rangeID, MAX_SYMBOL_SPLIT_RANGE_ID);
              return ERROR;
            }
          }
        }
      }
    }
    else if (strncmp(mdcParams.parameterList[0], "EDGA", 4) == 0)
    {
      int maxEdgaParams = 1 + MAX_EDGA_BOOK_GROUP;
      if (mdcParams.countParameters < maxEdgaParams)
      {
        TraceLog(ERROR_LEVEL, "Invalid Split Symbol Info for EDGA, require %d parameters\n", maxEdgaParams);
        return ERROR;
      }
      
      for (groupID = 1; groupID < maxEdgaParams; groupID++)
      {
        if (strchr(mdcParams.parameterList[groupID], '-') == NULL)
        {
          rangeID = atoi(mdcParams.parameterList[groupID]);
          if (rangeID >= 1 && rangeID <= MAX_SYMBOL_SPLIT_RANGE_ID)
          {
            SymbolSplitInfo[rangeID-1].edga[groupID-1] = ENABLE;
          }
          else
          {
            TraceLog(ERROR_LEVEL, "Invalid Split Symbol Info for EDGA (groupID=%d rangeID=%d), range ID allows from 1 to %d\n", groupID, rangeID, MAX_SYMBOL_SPLIT_RANGE_ID);
            return ERROR;
          }
        }
        else
        {
          // Split to get all ranges for groupID
          t_Parameters groupParams;
          groupParams.countParameters = Lrc_Split(mdcParams.parameterList[groupID], '-', &groupParams);
          for (j = 0; j < groupParams.countParameters; j++)
          {
            rangeID = atoi(groupParams.parameterList[j]);
            if (rangeID >= 1 && rangeID <= MAX_SYMBOL_SPLIT_RANGE_ID)
            {
              SymbolSplitInfo[rangeID-1].edga[groupID-1] = ENABLE;
            }
            else
            {
              TraceLog(ERROR_LEVEL, "Invalid Split Symbol Info for EDGA (groupID=%d rangeID=%d), range ID allows: 1 to %d\n", groupID, rangeID, MAX_SYMBOL_SPLIT_RANGE_ID);
              return ERROR;
            }
          }
        }
      }
    }
    else if (strncmp(mdcParams.parameterList[0], "CQS", 3) == 0)
    {
      int maxCqsParams = 1 + MAX_CQS_MULTICAST_LINES;
      if (mdcParams.countParameters < maxCqsParams)
      {
        TraceLog(ERROR_LEVEL, "Invalid Split Symbol Info for CQS, require %d parameters\n", maxCqsParams);
        return ERROR;
      }
      
      for (groupID = 1; groupID < maxCqsParams; groupID++)
      {
        if (strchr(mdcParams.parameterList[groupID], '-') == NULL)
        {
          rangeID = atoi(mdcParams.parameterList[groupID]);
          if (rangeID >= 1 && rangeID <= MAX_SYMBOL_SPLIT_RANGE_ID)
          {
            SymbolSplitInfo[rangeID-1].cqs[groupID-1] = ENABLE;
          }
          else
          {
            TraceLog(ERROR_LEVEL, "Invalid Split Symbol Info for CQS (groupID=%d rangeID=%d), range ID allows from 1 to %d\n", groupID, rangeID, MAX_SYMBOL_SPLIT_RANGE_ID);
            return ERROR;
          }
        }
        else
        {
          // Split to get all ranges for groupID
          t_Parameters groupParams;
          groupParams.countParameters = Lrc_Split(mdcParams.parameterList[groupID], '-', &groupParams);
          for (j = 0; j < groupParams.countParameters; j++)
          {
            rangeID = atoi(groupParams.parameterList[j]);
            if (rangeID >= 1 && rangeID <= MAX_SYMBOL_SPLIT_RANGE_ID)
            {
              SymbolSplitInfo[rangeID-1].cqs[groupID-1] = ENABLE;
            }
            else
            {
              TraceLog(ERROR_LEVEL, "Invalid Split Symbol Info for CQS (groupID=%d rangeID=%d), range ID allows: 1 to %d\n", groupID, rangeID, MAX_SYMBOL_SPLIT_RANGE_ID);
              return ERROR;
            }
          }
        }
      }
    }
    else if (strncmp(mdcParams.parameterList[0], "UQDF", 4) == 0)
    {
      int maxUqdfParams = 1 + MAX_UQDF_CHANNELS;
      if (mdcParams.countParameters < maxUqdfParams)
      {
        TraceLog(ERROR_LEVEL, "Invalid Split Symbol Info for UQDF, require %d parameters\n", maxUqdfParams);
        return ERROR;
      }
      
      for (groupID = 1; groupID < maxUqdfParams; groupID++)
      {
        if (strchr(mdcParams.parameterList[groupID], '-') == NULL)
        {
          rangeID = atoi(mdcParams.parameterList[groupID]);
          if (rangeID >= 1 && rangeID <= MAX_SYMBOL_SPLIT_RANGE_ID)
          {
            SymbolSplitInfo[rangeID-1].uqdf[groupID-1] = ENABLE;
          }
          else
          {
            TraceLog(ERROR_LEVEL, "Invalid Split Symbol Info for UQDF (groupID=%d rangeID=%d), range ID allows from 1 to %d\n", groupID, rangeID, MAX_SYMBOL_SPLIT_RANGE_ID);
            return ERROR;
          }
        }
        else
        {
          // Split to get all ranges for groupID
          t_Parameters groupParams;
          groupParams.countParameters = Lrc_Split(mdcParams.parameterList[groupID], '-', &groupParams);
          for (j = 0; j < groupParams.countParameters; j++)
          {
            rangeID = atoi(groupParams.parameterList[j]);
            if (rangeID >= 1 && rangeID <= MAX_SYMBOL_SPLIT_RANGE_ID)
            {
              SymbolSplitInfo[rangeID-1].uqdf[groupID-1] = ENABLE;
            }
            else
            {
              TraceLog(ERROR_LEVEL, "Invalid Split Symbol Info for UQDF (groupID=%d rangeID=%d), range ID allows: 1 to %d\n", groupID, rangeID, MAX_SYMBOL_SPLIT_RANGE_ID);
              return ERROR;
            }
          }
        }
      }
    }
  }
  
  // Close file to release resources
  fclose(fileDesc);
  
  TraceLog(DEBUG_LEVEL, "Symbol Split Info:\n");
  TraceLog(DEBUG_LEVEL, "NYSE will join group number:");
  for (groupID = 0; groupID < MAX_NYSE_BOOK_GROUPS; groupID++)
  {
    if (SymbolSplitInfo[AlgorithmConfig.splitConfig.symbolRangeID].nyse[groupID] == ENABLE)
    {
      TraceLog(NO_LOG_LEVEL, " %d", groupID + 1);
    }
  }
  TraceLog(NO_LOG_LEVEL, "\n");
  
  TraceLog(DEBUG_LEVEL, "BATSZ will join group number:");
  for (groupID = 0; groupID < MAX_BATSZ_UNIT; groupID++)
  {
    if (SymbolSplitInfo[AlgorithmConfig.splitConfig.symbolRangeID].batsz[groupID] == ENABLE)
    {
      TraceLog(NO_LOG_LEVEL, " %d", groupID + 1);
    }
  }
  TraceLog(NO_LOG_LEVEL, "\n");
  
  TraceLog(DEBUG_LEVEL, "BYX will join group number:");
  for (groupID = 0; groupID < MAX_BYX_UNIT; groupID++)
  {
    // printf("groupID: %d - %d \n", groupID, SymbolSplitInfo[AlgorithmConfig.splitConfig.symbolRangeID].byx[groupID]);
    if (SymbolSplitInfo[AlgorithmConfig.splitConfig.symbolRangeID].byx[groupID] == ENABLE)
    {
      TraceLog(NO_LOG_LEVEL, " %d", groupID + 1);
    }
  }
  TraceLog(NO_LOG_LEVEL, "\n");
  
  TraceLog(DEBUG_LEVEL, "EDGX will join group number:");
  for (groupID = 0; groupID < MAX_EDGX_BOOK_GROUP; groupID++)
  {
    if (SymbolSplitInfo[AlgorithmConfig.splitConfig.symbolRangeID].edgx[groupID] == ENABLE)
    {
      TraceLog(NO_LOG_LEVEL, " %d", groupID + 1);
    }
  }
  TraceLog(NO_LOG_LEVEL, "\n");
  
  TraceLog(DEBUG_LEVEL, "EDGA will join group number:");
  for (groupID = 0; groupID < MAX_EDGA_BOOK_GROUP; groupID++)
  {
    if (SymbolSplitInfo[AlgorithmConfig.splitConfig.symbolRangeID].edga[groupID] == ENABLE)
    {
      TraceLog(NO_LOG_LEVEL, " %d", groupID + 1);
    }
  }
  TraceLog(NO_LOG_LEVEL, "\n");
  
  TraceLog(DEBUG_LEVEL, "CQS will join group number:");
  for (groupID = 0; groupID < MAX_CQS_MULTICAST_LINES; groupID++)
  {
    if (SymbolSplitInfo[AlgorithmConfig.splitConfig.symbolRangeID].cqs[groupID] == ENABLE)
    {
      TraceLog(NO_LOG_LEVEL, " %d", groupID + 1);
    }
  }
  TraceLog(NO_LOG_LEVEL, "\n");
  
  TraceLog(DEBUG_LEVEL, "UQDF will join group number:");
  for (groupID = 0; groupID < MAX_UQDF_CHANNELS; groupID++)
  {
    if (SymbolSplitInfo[AlgorithmConfig.splitConfig.symbolRangeID].uqdf[groupID] == ENABLE)
    {
      TraceLog(NO_LOG_LEVEL, " %d", groupID + 1);
    }
  }
  TraceLog(NO_LOG_LEVEL, "\n");
  
  return SUCCESS;
}

/****************************************************************************
- Function name:    LoadMinSpreadConfig
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int LoadMinSpreadConfig(const char *fileName)
{
  TraceLog(DEBUG_LEVEL, "Load Minimum Spread Config ...\n");
  
  int i;
  
  // Initial data 
  MinSpreadConf.preMarket = AlgorithmConfig.globalConfig.minSpread;
  MinSpreadConf.postMarket = AlgorithmConfig.globalConfig.minSpread;
  
  for (i = 0; i < MAX_TIMELINE_INTRADAY_OF_MIN_SPREAD; i++)
  {
    MinSpreadConf.intraday.minSpreadList[i].timeInSeconds = TradingSchedule.timeToEndOfPreMarket;
    MinSpreadConf.intraday.minSpreadList[i].minSpread = AlgorithmConfig.globalConfig.minSpread;
  }
  MinSpreadConf.intraday.countTimes = 0;
  
  /*
  Get full configuration path. If have any problem ERROR status will be returned
  for calling routine
  */
  char fullPath[MAX_PATH_LEN];
  
  if (GetFullConfigPath(fileName, CONF_MISC, fullPath) == NULL_POINTER)
  {
    return ERROR;
  }
  else
  {
    /*
    Try to open file with plain text mode to read. If we cannot open, report ERROR
    otherwise try to read configuration info
    */
    FILE *fileDesc = fopen(fullPath, "r");
    
    if (fileDesc == NULL)
    {
      TraceLog(ERROR_LEVEL, "Critical! Could not open file '%s' to read min-spread values. Trade Server could not continue.\n", fullPath);
      PrintErrStr(ERROR_LEVEL, "calling fopen(): ", errno);
      sleep(3); exit(0);
      return ERROR;
    }
    
    /*
    Create and initialize temporary buffer to 
    contain a line of configuration information
    */
    char tempBuffer[1024] = "\0";   
    char *fieldValue;
    t_Parameters paraList;
    int sessionIndex = 0, timeInSeconds;
    double minSpread;
    int isNewTimeline = 0, timeline = 0;
    
    // Read configuration info and check valid configuration format
    while (fgets(tempBuffer, 1024, fileDesc) != NULL)
    {
      // Skip comment lines
      if (tempBuffer[0] == '#')
      {
        continue;
      }
      
      // Remove used characters
      RemoveCharacters(tempBuffer, strlen(tempBuffer));
      
      // Remove blank lines
      if (strlen(tempBuffer) == 0)
      {
        continue;
      }
      
      // Check for new session
      if (strncasecmp(tempBuffer, "session", 7) == 0)
      {
        fieldValue = strchr(tempBuffer, '=');
        fieldValue++;
        
        // Remove white space characters
        while (*fieldValue == 32)
        {
          fieldValue++;
        }
        
        sessionIndex = GetTradeSessionIndexByName(fieldValue);
        
        if (sessionIndex == -1)
        {
          TraceLog(ERROR_LEVEL, "Invalid trade session: %s\n", fieldValue);
          return ERROR;
        }
      }
      
      // Check min spread
      if (strncasecmp(tempBuffer, "min spread", 10) == 0)
      {
        fieldValue = strchr(tempBuffer, '=');
        fieldValue++;
        
        // Remove white space characters
        while (*fieldValue == 32)
        {
          fieldValue++;
        }
        
        minSpread = atof(fieldValue);
        
        if (fle(minSpread, 0.00))
        {
          TraceLog(ERROR_LEVEL, "Invalid min spread: %s\n", fieldValue);
          return ERROR;
        }
        
        if (sessionIndex == PRE_MARKET)
        {
          MinSpreadConf.preMarket = minSpread;
        }
        else if (sessionIndex == POST_MARKET)
        {
          MinSpreadConf.postMarket = minSpread;
        }
        else
        {
          if (isNewTimeline == 1)
          {
            isNewTimeline = 0;
            MinSpreadConf.intraday.minSpreadList[timeline].minSpread = minSpread;
          }
          else
          {
            TraceLog(ERROR_LEVEL, "Invalid conf file: %s\n", fullPath);
            return ERROR;
          }
        }
      }
      
      // Check time switch
      if (strncasecmp(tempBuffer, "time switch", 11) == 0)
      {
        fieldValue = strchr(tempBuffer, '=');
        fieldValue++;
        
        // Remove white space characters
        while (*fieldValue == 32)
        {
          fieldValue++;
        }
        
        paraList.countParameters = Lrc_Split(fieldValue, ':', &paraList);
        timeInSeconds = atoi(paraList.parameterList[0]) * 3600 + atoi(paraList.parameterList[1]) * 60 + atoi(paraList.parameterList[2]);
        
        if (isNewTimeline == 0)
        {
          isNewTimeline = 1;
          timeline = MinSpreadConf.intraday.countTimes++;
          MinSpreadConf.intraday.minSpreadList[timeline].timeInSeconds = timeInSeconds;
        }
        else
        {
          TraceLog(ERROR_LEVEL, "Invalid conf file: %s\n", fullPath);
          return ERROR;
        }
      }
    }
    
    // Close file to release resources
    fclose(fileDesc);
  }
  
  // Sort data
  t_Min_Spread_Switch tmpMinSpread;
  
  int j;
  for (i = 0; i < MinSpreadConf.intraday.countTimes - 1; i++)
  {
    for (j = i+1; j < MinSpreadConf.intraday.countTimes; j++)
    {
      if (MinSpreadConf.intraday.minSpreadList[j].timeInSeconds < MinSpreadConf.intraday.minSpreadList[i].timeInSeconds) 
      {
        memcpy(&tmpMinSpread, &MinSpreadConf.intraday.minSpreadList[i], sizeof(t_Min_Spread_Switch));
        memcpy(&MinSpreadConf.intraday.minSpreadList[i], &MinSpreadConf.intraday.minSpreadList[j], sizeof(t_Min_Spread_Switch));
        memcpy(&MinSpreadConf.intraday.minSpreadList[j], &tmpMinSpread, sizeof(t_Min_Spread_Switch));
      }
    }
  }
  
  TraceLog(DEBUG_LEVEL, "Minimum Spread Config: preMarket = %lf\n", MinSpreadConf.preMarket);
  TraceLog(DEBUG_LEVEL, "Minimum Spread Config: postMarket = %lf\n", MinSpreadConf.postMarket);
  for (i = 0; i < MinSpreadConf.intraday.countTimes; i++)
  {
    TraceLog(DEBUG_LEVEL, "%d timeInSeconds: %d minSpread: %lf\n", i, 
          MinSpreadConf.intraday.minSpreadList[i].timeInSeconds,
          MinSpreadConf.intraday.minSpreadList[i].minSpread);
  }
  
  // Calculate minimum spread
  // Pre-market
  int limit = TradingSchedule.timeToEndOfPreMarket;
  for (i = 0; i < limit; i++)
  {
    MinSpreadInSecond[i] = MinSpreadConf.preMarket;
  }
  
  // Post-market
  limit = TradingSchedule.timeToEndOfIntraday;
  for (i = limit; i < MAX_MIN_SPREAD_OF_DAY; i++)
  {
    MinSpreadInSecond[i] = MinSpreadConf.postMarket;
  }
  
  // Intraday
  int startIndex;
  for (i = MinSpreadConf.intraday.countTimes - 1; i >= 0; i--)
  {
    if (i == MinSpreadConf.intraday.countTimes - 1)
    {
      limit = TradingSchedule.timeToEndOfIntraday;
    }
    else
    {
      limit = MinSpreadConf.intraday.minSpreadList[i + 1].timeInSeconds;
    }
    
    if (i > 0)
    {
      startIndex = MinSpreadConf.intraday.minSpreadList[i].timeInSeconds;
    }
    else
    {
      startIndex = TradingSchedule.timeToEndOfPreMarket;
    }
    
    for (j = startIndex; j < limit; j++)
    {
      MinSpreadInSecond[j] = MinSpreadConf.intraday.minSpreadList[i].minSpread;
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadAS_Config
****************************************************************************/
int LoadAS_Config(const char *fileName, void *configInfo)
{
  TraceLog(DEBUG_LEVEL, "Inside LoadAS_Config\n");
  
  t_ASConfig *config = (t_ASConfig *)configInfo;
  
  /*
  Get full configuration path. If have any problem ERROR status will be returned
  for calling routine
  */
  char fullPath[MAX_PATH_LEN];
  
  if (GetFullConfigPath(fileName, CONF_MISC, fullPath) == NULL_POINTER)
  {
    return ERROR;
  }
  else
  {
    /*
    Try to open file with plain text mode to read. If we cannot open, report ERROR
    otherwise try to read configuration info
    */
    FILE *fileDesc = fopen(fullPath, "r");
    
    if (fileDesc == NULL)
    {
      TraceLog(ERROR_LEVEL, "Critical! Could not open file '%s' to read. Trade Server could not continue.\n", fullPath);
      PrintErrStr(ERROR_LEVEL, "calling fopen(): ", errno);
      sleep(3); exit(0);
      return ERROR;
    }
    
    /*
    Create and initialize temporary buffer to 
    contain a line of configuration information
    */
    char tempBuffer[1024] = "\0";
    int currentReadLine = 0;
    
    // Read configuration info and check valid configuration format
    while (fgets(tempBuffer, 1024, fileDesc) != NULL)
    {
      // Skip comment lines
      if (tempBuffer[0] == '#')
      {
        continue;
      }
      
      // Remove used characters
      RemoveCharacters(tempBuffer, strlen(tempBuffer));
      
      if (strlen(tempBuffer) == 0 ) continue;
      
      // Get port
      if (currentReadLine == 0)
      {
        config->port = atoi(tempBuffer);
        if (CheckValidPort(config->port) == ERROR)
        {
          // Close file to release resources
          TraceLog(ERROR_LEVEL, "Critical! '%s' has invalid port value '%d'. Trade Server could not continue.\n", fullPath, config->port);
          fclose(fileDesc);
          sleep(3); exit(0);
          return ERROR;
        }
        
        currentReadLine++;
      }
      // Get maximum concurrent connections
      else if (currentReadLine == 1)
      {
        config->maxConnections = atoi(tempBuffer);
        if (config->maxConnections <= 0)
        {
          // Close file to release resources
          fclose(fileDesc);
          
          return ERROR;
        }
        
        currentReadLine++;
      }
      /*
      If we can get more line here, maybe the file is containing invalid
      format information
      */
      else
      {
        // Close file to release resources
        TraceLog(ERROR_LEVEL, "Critical! '%s' has invalid format. Trade Server could not continue.\n", fullPath, config->port);
        fclose(fileDesc);
        sleep(3); exit(0);
        
        return ERROR;
      }
    }
    
    // Close file to release resources
    fclose(fileDesc);
    
    if (currentReadLine != 2)
    {
      TraceLog(ERROR_LEVEL, "Critical! '%s' has invalid format. Trade Server could not continue.\n", fullPath, config->port);
      sleep(3); exit(0);
    }
  }
  
  TraceLog(DEBUG_LEVEL, "Port to listen AS %d\n", config->port);
  TraceLog(DEBUG_LEVEL, "Max connections %d\n\n", config->maxConnections);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadSequenceNumber
****************************************************************************/
int LoadSequenceNumber(const char *fileName, int oecId)
{
  char fullPath[MAX_PATH_LEN];
  
  if (GetFullConfigPath(fileName, CONF_ORDER, fullPath) == NULL_POINTER)
  {
    return ERROR;
  }
  
  switch (oecId)
  {
    case ORDER_ARCA_DIRECT:
    case ORDER_NYSE_CCG:
      return LoadIncomingAndOutgoingSeqNum(fullPath, oecId);
    case ORDER_NASDAQ_OUCH:
    case ORDER_NASDAQ_BX:
    case ORDER_NASDAQ_PSX:
    case ORDER_NASDAQ_RASH:
    case ORDER_EDGX:
    case ORDER_EDGA:
      return LoadIncomingSeqNum(fullPath, oecId);
    case ORDER_BATSZ_BOE:
    case ORDER_BYX_BOE:
      return LoadBatsSeqNum(fullPath, oecId);
    default:
      return ERROR;     
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadIncomingAndOutgoingSeqNum
****************************************************************************/
int LoadIncomingAndOutgoingSeqNum(const char *fullPath, int oecId)
{
  FILE *fileDesc = fopen(fullPath, "r");
  char tempBuffer[1024];
  int fieldIndex = 0;
  
  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Critical! Could not open file '%s' to read incoming/outgoing sequence numbers. Trade Server could not continue.\n", fullPath);
    PrintErrStr(ERROR_LEVEL, "calling fopen(): ", errno);
    sleep(3); exit(0);
    return ERROR;
  }
  
  while (fgets(tempBuffer, 1024, fileDesc) != NULL)
  {
    // Skip comment lines
    if (tempBuffer[0] == '#') continue;
    RemoveCharacters(tempBuffer, strlen(tempBuffer));
    if (strlen(tempBuffer) == 0) continue;
    
    if (fieldIndex == 0)
    {
      OecConfig[oecId].incomingSeqNum = atoi(tempBuffer);
      TraceLog(DEBUG_LEVEL, "Oec: %d, incoming seq num: %d\n", oecId, OecConfig[oecId].incomingSeqNum);
    }
    else if (fieldIndex == 1)
    {
      OecConfig[oecId].outgoingSeqNum = atoi(tempBuffer);
      TraceLog(DEBUG_LEVEL, "Oec: %d, outgoing seq num: %d\n", oecId, OecConfig[oecId].outgoingSeqNum);
    }
    else
    {
      TraceLog(ERROR_LEVEL, "Critical! Invalid configuration file '%s'. Trade Server could not continue.\n", fullPath);
      fclose(fileDesc);
      sleep(3); exit(0);
    }
    
    fieldIndex++;
  }
  
  // Close file and release resource
  fclose(fileDesc);
  
  if (fieldIndex != 2)
  {
    TraceLog(ERROR_LEVEL, "Critical! Invalid configuration file '%s'. Trade Server could not continue.\n", fullPath);
    
    if (oecId == ORDER_ARCA_DIRECT)
    {
      int incomingSeqNum = 0;
      if (GetLatestSequenceNumbers_ARCA_DIRECT(&incomingSeqNum) == SUCCESS)
      {
        TraceLog(ERROR_LEVEL, "Please update the file for incoming and outgoing sequence number values\n");
        TraceLog(ERROR_LEVEL, "Suggested values based on current rawdata:\n");
        TraceLog(ERROR_LEVEL, "Incoming sequence number should be: %d\n", incomingSeqNum);
        TraceLog(ERROR_LEVEL, "Outgoing sequence number should always be 0 (zero), it will be updated automatically when logged on to the server\n");
      }
    }
    sleep(3); exit(0);
  }
  
  if (oecId == ORDER_ARCA_DIRECT)
  {
    int incomingSeqNum = 0;
    if (GetLatestSequenceNumbers_ARCA_DIRECT(&incomingSeqNum) == SUCCESS)
    {
      if (OecConfig[ORDER_ARCA_DIRECT].incomingSeqNum < incomingSeqNum)
      {
        TraceLog(ERROR_LEVEL, "Based on current rawdata, TS has detected that ARCA Incoming Sequence Number is currently invalid.\n");
        TraceLog(ERROR_LEVEL, "Please update the file with following suggested values:\n");
        TraceLog(ERROR_LEVEL, "Incoming sequence number should be: %d\n", incomingSeqNum);
        TraceLog(ERROR_LEVEL, "Outgoing sequence number should always be 0 (zero), it will be updated automatically when logged on to the server\n");
        sleep(3); exit(0);
      }
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadBatsSeqNum
- Usage:      N/A
****************************************************************************/
int LoadBatsSeqNum(const char *fullPath, const int oecId)
{
  FILE *fileDesc = fopen(fullPath, "r");
  char tempBuffer[1024];
  int fieldIndex = 0, value;

  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Critical! Could not open file '%s' to read incoming sequence number. Trade Server could not continue.\n", fullPath);
    PrintErrStr(ERROR_LEVEL, "calling fopen(): ", errno);
    sleep(3); exit(0);
    
    return ERROR;
  }

  while (fgets(tempBuffer, 1024, fileDesc) != NULL)
  {
    // Skip comment lines
    if (tempBuffer[0] == '#') continue;

    RemoveCharacters(tempBuffer, strlen(tempBuffer));

    if (strlen(tempBuffer) == 0) continue;

    if (oecId == ORDER_BATSZ_BOE)
    {
      if (fieldIndex == 0)
      {
        OecConfig[oecId].outgoingSeqNum = atoi(tempBuffer);
      }
      else if (fieldIndex == 1)
      {
        value = atoi(tempBuffer);
        if(value < 0 || value > MAX_BATSZ_NUM_UNIT)
        {
          TraceLog(ERROR_LEVEL, "BATSZ BOE: Number of Units must be in range 0 to %d\n", MAX_BATSZ_NUM_UNIT);
          TraceLog(ERROR_LEVEL, "BATSZ BOE: Please open batsz_boe_seqs and set valid value\n");

          // Close file and release resource
          fclose(fileDesc);
          sleep(3); exit(0);
          return ERROR;
        }

        OecConfig[oecId].numbersOfBatszUnits = value;
      }
      else if(fieldIndex <= (MAX_BATSZ_NUM_UNIT + 1))
      {
        BATSZ_BOE_SequenceOfUnit[fieldIndex - 2].incomingSeqNum = atoi(tempBuffer);
        if (BATSZ_BOE_SequenceOfUnit[fieldIndex - 2].incomingSeqNum != 0)
          TraceLog(DEBUG_LEVEL, "BATSZ (Unit %d) - incoming seq num: %d\n", fieldIndex - 1, BATSZ_BOE_SequenceOfUnit[fieldIndex - 2].incomingSeqNum);
      }
      else
      {
        TraceLog(ERROR_LEVEL, "Invalid configuration file '%s'. Trade Server can not continue.\n", fullPath);
        fclose(fileDesc);
        sleep(3); exit(0);
      }

      fieldIndex++;
    }
    else
    {
      if (fieldIndex == 0)
      {
        OecConfig[oecId].outgoingSeqNum = atoi(tempBuffer);
      }
      else if (fieldIndex == 1)
      {
        value = atoi(tempBuffer);
        if(value < 0 || value > MAX_BYX_NUM_UNIT)
        {
          TraceLog(ERROR_LEVEL, "BYX BOE: Number of Units must be in range 0 to %d\n", MAX_BYX_NUM_UNIT);
          TraceLog(ERROR_LEVEL, "BYX BOE: Please open batsz_boe_seqs and set valid value\n");

          // Close file and release resource
          fclose(fileDesc);
          sleep(3); exit(0);
          return ERROR;
        }

        OecConfig[oecId].numbersOfBatszUnits = value;
      }
      else if(fieldIndex <= (MAX_BYX_NUM_UNIT + 1))
      {
        BYX_BOE_SequenceOfUnit[fieldIndex - 2].incomingSeqNum = atoi(tempBuffer);
        if (BYX_BOE_SequenceOfUnit[fieldIndex - 2].incomingSeqNum != 0)
          TraceLog(DEBUG_LEVEL, "BYX (Unit %d) - incoming seq num: %d\n", fieldIndex - 1, BYX_BOE_SequenceOfUnit[fieldIndex - 2].incomingSeqNum);
      }
      else
      {
        TraceLog(ERROR_LEVEL, "Invalid configuration file '%s'. Trade Server can not continue.\n", fullPath);
        fclose(fileDesc);
        sleep(3); exit(0);
      }

      fieldIndex++;
    }
  }
  
  // Close file and release resource
  fclose(fileDesc);
  
  if (oecId == ORDER_BATSZ_BOE)
  {
    if (fieldIndex != (MAX_BATSZ_NUM_UNIT + 2))
    {
      TraceLog(ERROR_LEVEL, "Invalid configuration file '%s'. Trade Server can not continue.\n", fullPath);
      sleep(3); exit(0);
    }
  }
  else
  {
    if (fieldIndex != (MAX_BYX_NUM_UNIT + 2))
    {
      TraceLog(ERROR_LEVEL, "Invalid configuration file '%s'. Trade Server can not continue.\n", fullPath);
      sleep(3); exit(0);
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadIncomingSeqNum
****************************************************************************/
int LoadIncomingSeqNum(const char *fullPath, int oecId)
{
  FILE *fileDesc = fopen(fullPath, "r");
  char tempBuffer[1024];
  int fieldIndex = 0;
  
  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Critical! Could not open file '%s' to read incoming sequence number. Trade Server could not continue.\n", fullPath);
    PrintErrStr(ERROR_LEVEL, "calling fopen(): ", errno);
    sleep(3); exit(0);
    
    return ERROR;
  }
  
  while (fgets(tempBuffer, 1024, fileDesc) != NULL)
  {
    // Skip comment lines
    if (tempBuffer[0] == '#') continue;
    RemoveCharacters(tempBuffer, strlen(tempBuffer));
    if (strlen(tempBuffer) == 0) continue;
    
    if (fieldIndex == 0)
    {
      OecConfig[oecId].incomingSeqNum = atoi(tempBuffer);
      TraceLog(DEBUG_LEVEL, "Oec: %d, incoming seq num: %d\n", oecId, OecConfig[oecId].incomingSeqNum);
    }
    else
    {
      fclose(fileDesc);
      TraceLog(ERROR_LEVEL, "Critical! Invalid configuration file '%s' (There are more than 1 value). Trade Server could not continue.\n", fullPath);
      sleep(3); exit(0);
    }
    
    fieldIndex++;
  }
  
  // Close file and release resource
  if (fclose(fileDesc) != 0)
  {
    TraceLog(ERROR_LEVEL, "Critical! Could not open file '%s' to read incoming sequence number. Trade Server could not continue.\n", fullPath);
    PrintErrStr(ERROR_LEVEL, "calling fopen(): ", errno);
    sleep(3); exit(0);
  }
  
  if (fieldIndex != 1)
  {
    TraceLog(ERROR_LEVEL, "Critical! Invalid configuration file '%s' (There are more than 1 value). Trade Server could not continue.\n", fullPath);
    sleep(3); exit(0);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadOrderIDAndCrossIDFromFile
****************************************************************************/
int LoadOrderIDAndCrossIDFromFile(char const *fileName, int *orderID, int *crossID)
{
  char fullPath[MAX_PATH_LEN];
  
  if (GetFullConfigPath(fileName, CONF_MISC, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Critical! Could not open file '%s' to read. Trade Server could not continue.\n", fileName);
    sleep(3); exit(0);
    return ERROR;
  }
  
  FILE *fileDesc = fopen(fullPath, "r");
  char tempBuffer[1024];
  int fieldIndex = 0;
    
  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Critical! Could not open file '%s' to read. Trade Server could not continue.\n", fullPath);
    PrintErrStr(ERROR_LEVEL, "calling fopen(): ", errno);
    sleep(3); exit(0);

    return ERROR;
  }
  
  while (fgets(tempBuffer, 1024, fileDesc) != NULL)
  {
    // Skip comment lines
    if (tempBuffer[0] == '#')
    {
      continue;
    }
    
    RemoveCharacters(tempBuffer, strlen(tempBuffer));
    
    if (strlen(tempBuffer) == 0)
    {
      continue;
    }
    
    if (fieldIndex == 0)
    {
      *orderID = atoi(tempBuffer);
      fieldIndex++;
    }
    else if (fieldIndex == 1)
    {
      *crossID = atoi(tempBuffer);
      fieldIndex++;
    }
    else
    {
      fclose(fileDesc);
      TraceLog(ERROR_LEVEL, "Critical! '%s' has invalid format. Trade Server could not continue.\n", fullPath);
      sleep(3); exit(0);
      return ERROR;
    }
  }
  
  // Close file and release resource
  if (fclose(fileDesc) != 0)
  {
    TraceLog(ERROR_LEVEL, "Critical! Could not open file '%s' to read. Trade Server could not continue.\n", fullPath);
    PrintErrStr(ERROR_LEVEL, "calling fclose(): ", errno);
    sleep(3); exit(0);
  }
  
  if (fieldIndex != 2)
  {
    TraceLog(ERROR_LEVEL, "Critical! '%s' has invalid format. Trade Server could not continue.\n", fullPath);
    sleep(3); exit(0);
  }
  
  if (*orderID < 1000000)
  {
    TraceLog(ERROR_LEVEL, "Critical! '%s' has invalid Order ID value (%d). Trade Server could not continue.\n", fullPath, *orderID);
    TraceLog(ERROR_LEVEL, "Please update Order ID value in this file in order to continue.\n");
    TraceLog(ERROR_LEVEL, "Hint: Use TWT to filter what is the maximum current Order ID for this Trade Server\n");
    TraceLog(ERROR_LEVEL, "then assign Order ID value to any value that is higher than value obtained from TWT\n");
    sleep(3); exit(0);
  }
  
  if (*crossID < 0)
  {
    TraceLog(ERROR_LEVEL, "Critical! '%s' has invalid Cross ID value. Trade Server could not continue.\n", fullPath);
    sleep(3); exit(0);
  }
  
  int _crossID = 1;
  if (GetLatestCrossIDFromOutputLog(&_crossID) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Critical! Could not load trade output log file.\n");
    sleep(3); exit(0);
  }
  
  if (*crossID < _crossID)
  {
    TraceLog(ERROR_LEVEL, "Critical! Base on current Trade Output Log, TS has detected that the Cross ID value (%d) in '%s' is invalid.\n", *crossID, fullPath);
    TraceLog(ERROR_LEVEL, "This would cause database processing error on AS side. So please update Cross ID value to the following suggested value:\n");
    TraceLog(ERROR_LEVEL, "Suggested value: any value that is equal or larger than %d\n", _crossID);
    sleep(3); exit(0);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SaveOrderIDAndCrossIDFromFile
****************************************************************************/
int SaveOrderIDAndCrossIDFromFile(char const *fileName, int orderID, int crossID)
{
  char fullPath[MAX_PATH_LEN];
  
  if (GetFullConfigPath(fileName, CONF_MISC, fullPath) == NULL_POINTER)
  {
    return ERROR;
  }
  
  FILE *fileDesc = fopen(fullPath, "w");
  
  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Critical! Could not save order id/cross id to file '%s'\n", fullPath);
    PrintErrStr(ERROR_LEVEL, "calling fopen(): ", errno);
    return ERROR;
  }
  
  fprintf(fileDesc, "#Order ID\n");
  if (fprintf(fileDesc, "%d\n", orderID) < 0)
  {
    TraceLog(ERROR_LEVEL, "Critical! Could not save order id/cross id to file '%s'\n", fullPath);
    PrintErrStr(ERROR_LEVEL, "calling fprintf(): ", errno);
  }
  
  fprintf(fileDesc, "#Cross ID\n");
  if (fprintf(fileDesc, "%d\n", crossID) < 0)
  {
    TraceLog(ERROR_LEVEL, "Critical! Could not save order id/cross id to file '%s'\n", fullPath);
    PrintErrStr(ERROR_LEVEL, "calling fprintf(): ", errno);
  }
  
  // Close file and release resource
  if (fflush(fileDesc) != 0)
  {
    TraceLog(ERROR_LEVEL, "Critical! Could not save order id/cross id to file '%s'\n", fullPath);
    PrintErrStr(ERROR_LEVEL, "calling fflush(): ", errno);
  }
  
  if (fclose(fileDesc) != 0)
  {
    TraceLog(ERROR_LEVEL, "Critical! Could not save order id/cross id to file '%s'\n", fullPath);
    PrintErrStr(ERROR_LEVEL, "calling fclose(): ", errno);
  }
  
  return SUCCESS;
}


/****************************************************************************
- Function name:  LoadUQDF_Config
****************************************************************************/
int LoadUQDF_Config(const char *fileName)
{
  char fullPath[MAX_PATH_LEN];
  
  if (GetFullConfigPath(fileName, CONF_BOOK, fullPath) == NULL_POINTER)
  {
    return ERROR;
  }
  
  FILE  *f;

  // Buffer to store each line in the configuration file
  char line[1024];

  //Number of lines read from the configuration file
  int numLine = 0;

  // Open file configuration
  f = fopen(fullPath, "r");

  // Check file pointer
  if (f == NULL)
  {
        TraceLog(ERROR_LEVEL, "Could not open '%s' to read\n", fullPath);
        
    return ERROR;
  }
    
  // Opened file sucessfully
  memset(line, 0, 1024);

  // Get each line from the configuration file
  while (fgets(line, 1024, f) != NULL)
  {   
    if (!strchr(line, '#'))
    { 
      RemoveCharacters(line, strlen(line));
      
      if (strlen(line) == 0)
      {
        continue;
      }
      
      numLine++;
      
      if (numLine == 1)
      {
        strcpy (UQDF_Conf.NICName, line);
        continue;
      }
      
      // Primary data feed multicast group
      if (numLine <= MAX_UQDF_CHANNELS * 2 + 1) //NIC Name + COUNT *(ip:port)
      {
        if (numLine % 2 == 0)
        {
          strcpy(UQDF_Conf.group[numLine/2 - 1].ip, line);
        }
        else
        {
          UQDF_Conf.group[numLine/2 - 1].port = atoi(line);
        }
      }
      else
      {
        TraceLog(DEBUG_LEVEL, "UQDF configuration is invalid\n");
        
        // Close the file
        fclose(f);

        return ERROR;
      }

      // Initialize buffer
      memset(line, 0, 1024);
    }
  }
    
  // Close the file
  fclose(f);

  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadCQS_Config
****************************************************************************/
int LoadCQS_Config(const char *fileName, t_CQS_Group group[MAX_CQS_MULTICAST_LINES])
{
  char fullPath[MAX_PATH_LEN];
  
  if (GetFullConfigPath(fileName, CONF_BOOK, fullPath) == NULL_POINTER)
  {
    return ERROR;
  }
  
  FILE  *f;

  // Buffer to store each line in the configuration file
  char line[1024];

  //Number of lines read from the configuration file
  int numLine = 0;

  // Open file configuration
  f = fopen(fullPath, "r");

  // Check file pointer
  if (f == NULL)
  {
    TraceLog(ERROR_LEVEL, "Could not open '%s' to read\n", fullPath);
        
    return ERROR;
  }

  // Opened file sucessfully
  memset(line, 0, 1024);

  // Get each line from the configuration file
  while (fgets(line, 1024, f) != NULL)
  {   
    if (!strchr(line, '#'))
    { 
      RemoveCharacters(line, strlen(line));
      
      if (strlen(line) == 0)
      {
        continue;
      }
      
      numLine++;
      
      if (numLine == 1)
      {
        strcpy(CQS_Conf.NICName, line);
        continue;
      }
      
      // Primary data feed multicast group
      if (numLine <= MAX_CQS_MULTICAST_LINES * 2 + 1)
      {
        if ((numLine % 2) == 0)
        {
          strcpy(group[numLine/2 - 1].ip, line);
        }
        else
        {
          group[numLine/2 - 1].port = atoi(line);
        }
      }
      else
      {
        TraceLog(ERROR_LEVEL, "CQS Feed configuration is invalid\n");
        
        // Close the file
        fclose(f);

        return ERROR;
      }

      // Initialize buffer
      memset(line, 0, 1024);
    }
  }
    
  // Close the file
  fclose(f);

  return SUCCESS;
}

/****************************************************************************
- Function name:    LoadExchangesConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int LoadExchangesConf(void)
{
  char fullPath[MAX_PATH_LEN];
  
  if (GetFullConfigPath(FILE_NAME_OF_EXCHANGES_CONF, CONF_MISC, fullPath) == NULL_POINTER)
  {
    return ERROR;
  }
  
  FILE  *f;

  // Buffer to store each line in the configuration file
  char line[1024];

  //Number of lines read from the configuration file
  int numLine = 0;

  // Open file configuration
  f = fopen(fullPath, "r");

  // Check file pointer
  if (f == NULL)
  {
    TraceLog(ERROR_LEVEL, "Can not open '%s' to read\n", fullPath);
    return ERROR;
  }

  // Opened file sucessfully
  memset(line, 0, 1024);
  
  // Get each line from the configuration file
  while (fgets(line, 1024, f) != NULL)
  {
    if (!strchr(line, '#'))
    { 
      RemoveCharacters(line, strlen(line));
      
      if (strlen(line) == 0)
      {
        continue;
      }
      
      numLine++;
            
      // Primary data feed multicast group
      if (numLine <= MAX_EXCHANGE * 2)
      {
        if ((numLine - 1) % 2 == 0)
        {
          // CQS status
          if (strncmp(line, "ENABLE", 6) == 0)
          {
            ExchangeStatusList[(numLine - 1) / 2].status[CQS_SOURCE].visible = TRUE;
            ExchangeStatusList[(numLine - 1) / 2].status[CQS_SOURCE].enable = TRUE;
          }
          else if (strncmp(line, "DISABLE", 7) == 0)
          {
            ExchangeStatusList[(numLine - 1) / 2].status[CQS_SOURCE].visible = TRUE;
            ExchangeStatusList[(numLine - 1) / 2].status[CQS_SOURCE].enable = FALSE;
          }
          else
          {
            ExchangeStatusList[(numLine - 1) / 2].status[CQS_SOURCE].visible = FALSE;
            ExchangeStatusList[(numLine - 1) / 2].status[CQS_SOURCE].enable = FALSE;
          }
        }
        else
        {
          // UQDF status
          if (strncmp(line, "ENABLE", 6) == 0)
          {
            ExchangeStatusList[(numLine - 1) / 2].status[UQDF_SOURCE].visible = TRUE;
            ExchangeStatusList[(numLine - 1) / 2].status[UQDF_SOURCE].enable = TRUE;
          }
          else if (strncmp(line, "DISABLE", 7) == 0)
          {
            ExchangeStatusList[(numLine - 1) / 2].status[UQDF_SOURCE].visible = TRUE;
            ExchangeStatusList[(numLine - 1) / 2].status[UQDF_SOURCE].enable = FALSE;
          }
          else
          {
            ExchangeStatusList[(numLine - 1) / 2].status[UQDF_SOURCE].visible = FALSE;
            ExchangeStatusList[(numLine - 1) / 2].status[UQDF_SOURCE].enable = FALSE;
          }
        }
      }
      else
      {
        TraceLog(ERROR_LEVEL, "Exchanges configuration is invalid\n");
        
        // Close the file
        fclose(f);

        return ERROR;
      }

      // Initialize buffer
      memset(line, 0, 1024);
    }
  }
    
  // Close the file
  fclose(f);

  return SUCCESS;
}

/****************************************************************************
- Function name:    SaveExchangesConf
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SaveExchangesConf(void)
{
  char fullPath[MAX_PATH_LEN];
  
  if (GetFullConfigPath(FILE_NAME_OF_EXCHANGES_CONF, CONF_MISC, fullPath) == NULL_POINTER)
  {
    return ERROR;
  }
  
  FILE  *f;

  // Open file configuration
  f = fopen(fullPath, "w+");
  
  // Check file pointer
  if (f == NULL)
  {
    TraceLog(ERROR_LEVEL, "Can not open '%s' to read\n", fullPath);
    return ERROR;
  }
  
  /*
  # Checkmark list of exchanges
  # Exchange name
  # Status: ENABLE, DISABLE, N/A
  */
  fprintf(f, "# Checkmark list of exchanges\n");
  fprintf(f, "# Exchange name\n");
  fprintf(f, "# Status: ENABLE, DISABLE, N/A\n\n");
  
  int i;
  for (i = 0; i < MAX_EXCHANGE; i++)
  {
    switch (i)
    {
      case BOOK_ARCA:
        fprintf(f, "# P  NYSE Arca Exchange\n");
        break;
      
      case BOOK_NASDAQ:
        fprintf(f, "# Q/T  Nasdaq Stock Exchange\n");
        break;
      
      case BOOK_BATSZ:
        fprintf(f, "# Z  BATS-Z Exchange Inc\n");
        break;
      
      case BOOK_AMEX:
        fprintf(f, "# A  American Stock Exchange\n");
        break;
      
      case BOOK_NDBX:
        fprintf(f, "# B  Boston Stock Exchange\n");
        break;
      
      case National_Stock_Exchange:
        fprintf(f, "# C  National Stock Exchange\n");
        break;
      
      case FINRA_ADF:
        fprintf(f, "# D  FINRA\n");
        break;
      
      case Market_Independent:
        fprintf(f, "# E  CQS: Consolidated Quote System\n");
        fprintf(f, "# UQDF: Market Independent (Generated by SIP) \n");
        break;
      
      case International_Securities_Exchange:
        fprintf(f, "# I  International Securities Exchange\n");
        break;
      
      case BOOK_EDGA:
        fprintf(f, "# J   EDGA Exchange, Inc\n");
        break;
      
      case BOOK_EDGX:
        fprintf(f, "# K  EDGX Exchange, Inc\n");
        break;
      
      case Chicago_Stock_Exchange:
        fprintf(f, "# M  Chicago Stock Exchange\n");
        break;
      
      case BOOK_NYSE:
        fprintf(f, "# # N  CQS: New York Stock Exchange - UQDF: NYSE Euronext\n");
        break;
      
      case Chicago_Board_Options_Exchange:
        fprintf(f, "# W  Chicago Board Options Exchange\n");
        break;
      
      case BOOK_PSX:
        fprintf(f, "# X  Philadelphia Stock Exchange\n");
        break;
      
      case BOOK_BYX:
        fprintf(f, "# Y  BATS-Y Exchange, Inc\n");
        break;
    }
    
    fprintf(f, "# For CQS status\n");
    if (ExchangeStatusList[i].status[CQS_SOURCE].visible == TRUE)
    {
      if (ExchangeStatusList[i].status[CQS_SOURCE].enable == TRUE)
      {
        fprintf(f, "ENABLE\n");
      }
      else
      {
        fprintf(f, "DISABLE\n");
      }
    }
    else
    {
      fprintf(f, "N/A\n");
    }
    
    fprintf(f, "# For UQDF staus\n");
    if (ExchangeStatusList[i].status[UQDF_SOURCE].visible == TRUE)
    {
      if (ExchangeStatusList[i].status[UQDF_SOURCE].enable == TRUE)
      {
        fprintf(f, "ENABLE\n\n");
      }
      else
      {
        fprintf(f, "DISABLE\n\n");
      }
    }
    else
    {
      fprintf(f, "N/A\n\n");
    }
  }
    
  // Close the file
  fclose(f);

  return SUCCESS;
}

/****************************************************************************
- Function name:    LoadISO_Algorithm
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int LoadISO_Algorithm(void)
{
  char fullPath[MAX_PATH_LEN];
  
  if (GetFullConfigPath(FILE_NAME_OF_ISO_ALGORITHM, CONF_MISC, fullPath) == NULL_POINTER)
  {
    return ERROR;
  }
  
  FILE *fileDesc;
  
  // Open file configuration
  fileDesc = fopen(fullPath, "r");

  // Check file pointer
  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Critical! Could not open '%s' to read. Trade Server can not continue.\n", fullPath);
    PrintErrStr(ERROR_LEVEL, "calling fopen(): ", errno);
    sleep(3); exit(0);
    return ERROR;
  }
  
  char tempBuffer[1024];
  char *fieldValue;
  char value = DISABLE;
  int lineCount = 0;
  
  // Initialize book role configuration
  int i;
  for (i = 0; i < MAX_ECN_BOOK; i++)
  {
    BookISO_Trading[i] = DISABLE;
  }
  
  // Initialize Pre/Post Algorithm
  for (i = 0; i < MAX_ECN_BOOK; i++)
  {
    BookPrePostISOTrading[i] = ENABLE;
  }
  
  while (fgets(tempBuffer, 1024, fileDesc) != NULL)
  {
    // Ignore comment lines
    if (strchr(tempBuffer, '#') != NULL)
    {
      continue;
    }
    
    // Remove unused characters
    RemoveCharacters(tempBuffer, strlen(tempBuffer));
    
    // Remove blank lines
    if (strlen(tempBuffer) == 0)
    {
      continue;
    }
    
    // Get value field
    fieldValue = strchr(tempBuffer, '=');
        
    // Skip '='
    fieldValue++;
    
    while (*fieldValue == 32)
    {
      fieldValue++;
    }
    
    lineCount++;
    
    if (strncmp(fieldValue, "ENABLE", 6) == 0)
    {
      value = ENABLE;
    }
    else if (strncmp(fieldValue, "DISABLE", 7) == 0)
    {
      value = DISABLE;
    }
    else
    {
      TraceLog(ERROR_LEVEL, "Error(Load ISO Algorithm Config file '%s'): Invalid value (ENABLE/DISABLE)\n", fullPath);
      sleep(3); exit(0);
      return ERROR;
    }
    
    switch (lineCount)
    {
      case 1: //ISO trading for Arca book
        BookISO_Trading[BOOK_ARCA] = value;
        break;
      case 2: //ISO trading for Nasdaq book
        BookISO_Trading[BOOK_NASDAQ] = value;
        break;
      case 3:
        BookISO_Trading[BOOK_NYSE] = value;
        break;
      case 4: //ISO trading for BATS-Z Book
        BookISO_Trading[BOOK_BATSZ] = value;
        break;
      case 5: //ISO trading for EDGX book
        BookISO_Trading[BOOK_EDGX] = value;
        break;
      case 6: //ISO trading for EDGA book
        BookISO_Trading[BOOK_EDGA] = value;
        break;
      case 7: //ISO trading for BX book
        BookISO_Trading[BOOK_NDBX] = value;
        break;
      case 8: //ISO trading for BYX Book
        BookISO_Trading[BOOK_BYX] = value;
        break;
      case 9: //ISO trading for PSX Book
        BookISO_Trading[BOOK_PSX] = value;
        break;
      case 10: //ISO trading for AMEX Book
        BookISO_Trading[BOOK_AMEX] = value;
        break;
      case 11: //Pre/Post for Arca book
        BookPrePostISOTrading[BOOK_ARCA] = value;
        break;
      case 12:  //Pre/Post for Nasdaq book
        BookPrePostISOTrading[BOOK_NASDAQ] = value;
        break;
      case 13:
        BookPrePostISOTrading[BOOK_NYSE] = value;
        break;
      case 14: //Pre/Post for BATS-Z Book
        BookPrePostISOTrading[BOOK_BATSZ] = value;
        break;
      case 15: //Pre/Post for EDGX book
        BookPrePostISOTrading[BOOK_EDGX] = value;
        break;
      case 16: //Pre/Post for EDGA book
        BookPrePostISOTrading[BOOK_EDGA] = value;
        break;
      case 17: //Pre/Post for BX book
        BookPrePostISOTrading[BOOK_NDBX] = value;
        break;
      case 18: //Pre/Post for BYX Book
        BookPrePostISOTrading[BOOK_BYX] = value;
        break;
      case 19: //Pre/Post for PSX Book
        BookPrePostISOTrading[BOOK_PSX] = value;
        break;
      case 20: //Pre/Post for AMEX Book
        BookPrePostISOTrading[BOOK_AMEX] = value;
        break;
      default:
        fclose(fileDesc);
        TraceLog(ERROR_LEVEL, "Critical! Invalid configuration file '%s'. Trade Server can not continue.\n", fullPath);
        sleep(3); exit(0);
        return ERROR;
    }
    
    // Initialize buffer
    memset(tempBuffer, 0, 1024);
  }
  
  fclose(fileDesc);
  
  if (lineCount != MAX_ECN_BOOK*2)
  {
    TraceLog(ERROR_LEVEL, "Critical! Invalid configuration file '%s'. Trade Server can not continue.\n", fullPath);
    sleep(3); exit(0);
    return ERROR;
  }
  else
  {
    return SUCCESS;
  }
}

/****************************************************************************
- Function name:    SaveISO_Algorithm
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int SaveISO_Algorithm(void)
{
  char fullPath[MAX_PATH_LEN];
  
  if (GetFullConfigPath(FILE_NAME_OF_ISO_ALGORITHM, CONF_MISC, fullPath) == NULL_POINTER)
  {
    return ERROR;
  }
  
  FILE *fileDesc;

  // Open file configuration
  fileDesc = fopen(fullPath, "w+");

  // Check file pointer
  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Critical! Could not open '%s to write.\n", fullPath);
    PrintErrStr(ERROR_LEVEL, "calling fopen(): ", errno);
    return ERROR;
  }
  
  fprintf(fileDesc, "# Please respect the order of Books in this file: ARCA -> NASDAQ -> BATSZ\n");
  
  fprintf(fileDesc, "# Role of Books in the ISO algorithm\n");
  fprintf(fileDesc, "ARCA = %s\n", (BookISO_Trading[BOOK_ARCA] == ENABLE)?"ENABLE":"DISABLE");
  fprintf(fileDesc, "NASDAQ = %s\n", (BookISO_Trading[BOOK_NASDAQ] == ENABLE)?"ENABLE":"DISABLE");
  fprintf(fileDesc, "NYSE = %s\n", (BookISO_Trading[BOOK_NYSE] == ENABLE)?"ENABLE":"DISABLE");
  fprintf(fileDesc, "BATSZ = %s\n", (BookISO_Trading[BOOK_BATSZ] == ENABLE)?"ENABLE":"DISABLE");
  fprintf(fileDesc, "EDGX = %s\n", (BookISO_Trading[BOOK_EDGX] == ENABLE)?"ENABLE":"DISABLE");
  fprintf(fileDesc, "EDGA = %s\n", (BookISO_Trading[BOOK_EDGA] == ENABLE)?"ENABLE":"DISABLE");
  fprintf(fileDesc, "BX = %s\n", (BookISO_Trading[BOOK_NDBX] == ENABLE)?"ENABLE":"DISABLE");
  fprintf(fileDesc, "BYX = %s\n", (BookISO_Trading[BOOK_BYX] == ENABLE)?"ENABLE":"DISABLE");
  fprintf(fileDesc, "PSX = %s\n", (BookISO_Trading[BOOK_PSX] == ENABLE)?"ENABLE":"DISABLE");
  fprintf(fileDesc, "AMEX = %s\n", (BookISO_Trading[BOOK_AMEX] == ENABLE)?"ENABLE":"DISABLE");
  
  fprintf(fileDesc, "# Pre/Post Algorithm\n");
  fprintf(fileDesc, "ARCA = %s\n", (BookPrePostISOTrading[BOOK_ARCA] == ENABLE)?"ENABLE":"DISABLE");
  fprintf(fileDesc, "NASDAQ = %s\n", (BookPrePostISOTrading[BOOK_NASDAQ] == ENABLE)?"ENABLE":"DISABLE");
  fprintf(fileDesc, "NYSE = %s\n", (BookPrePostISOTrading[BOOK_NYSE] == ENABLE)?"ENABLE":"DISABLE");
  fprintf(fileDesc, "BATSZ = %s\n", (BookPrePostISOTrading[BOOK_BATSZ] == ENABLE)?"ENABLE":"DISABLE");
  fprintf(fileDesc, "EDGX = %s\n", (BookPrePostISOTrading[BOOK_EDGX] == ENABLE)?"ENABLE":"DISABLE");
  fprintf(fileDesc, "EDGA = %s\n", (BookPrePostISOTrading[BOOK_EDGA] == ENABLE)?"ENABLE":"DISABLE");
  fprintf(fileDesc, "BX = %s\n", (BookPrePostISOTrading[BOOK_NDBX] == ENABLE)?"ENABLE":"DISABLE");
  fprintf(fileDesc, "BYX = %s\n", (BookPrePostISOTrading[BOOK_BYX] == ENABLE)?"ENABLE":"DISABLE");
  fprintf(fileDesc, "PSX = %s\n", (BookPrePostISOTrading[BOOK_PSX] == ENABLE)?"ENABLE":"DISABLE");
  fprintf(fileDesc, "AMEX = %s\n", (BookPrePostISOTrading[BOOK_AMEX] == ENABLE)?"ENABLE":"DISABLE");
    
  // Close the file
  fclose(fileDesc);

  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadTradingAccountFromFile
****************************************************************************/
int LoadTradingAccountFromFile(const char *fileName, void *_tradingAccount)
{
  t_TradingAccount *tradingAccount = (t_TradingAccount *)_tradingAccount;
  
  /*
  Get full configuration path. If have any problem ERROR status will be returned
  for calling routine
  */
  
  char fullPath[MAX_PATH_LEN];
  
  if (GetFullConfigPath(fileName, CONF_MISC, fullPath) == NULL_POINTER)
  {
    return ERROR;
  }

  /*
  Try to open file with plain text mode to read. If we cannot open, report ERROR
  otherwise try to read configuration info
  */
  FILE *fileDesc = fopen(fullPath, "r");
  
  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Critical! Could not open file (%s). Trade Server can not continue.\n", fullPath);
    PrintErrStr(ERROR_LEVEL, "calling fopen(): ", errno);
    sleep(3); exit(0);
    return ERROR;
  }
  
  /*
  Create and initialize temporary buffer to 
  contain a line of configuration information
  */
  char tempBuffer[1024] = "\0";
  int currentReadLine = 0;
  
  // Read configuration info and check valid configuration format
  while (fgets(tempBuffer, 1024, fileDesc) != NULL)
  {
    // Skip comment lines
    if (tempBuffer[0] == '#')
    {
      continue;
    }
    
    // Remove used characters
    RemoveCharacters(tempBuffer, strlen(tempBuffer));
    if (strlen(tempBuffer) == 0) continue;
    
    if (currentReadLine == 0)
    {
      if (tempBuffer[0] == '1')
      {
        tradingAccount->accountType = 0;
      }
      else if (tempBuffer[0] == '2')
      {
        tradingAccount->accountType = 1;
      }
      else if (tempBuffer[0] == '3')
      {
        tradingAccount->accountType = 2;
      }
      else if (tempBuffer[0] == '4')
      {
        tradingAccount->accountType = 3;
      }
      else
      {
        fclose(fileDesc);
        TraceLog(ERROR_LEVEL, "Critical! %s has invalid format!, account type must be either 1, 2, 3 or 4\n", fullPath);
        sleep(3); exit(0);
        return ERROR;
      }
    }
    else if (currentReadLine == 1)
    {
      strcpy(tradingAccount->ArcaDirectAccount, tempBuffer);
    }
    else if (currentReadLine == 2)
    {
      char fullAccount[32];
      strcpy(fullAccount, tempBuffer);
      int offset = strlen(fullAccount) - 2;
      
      tradingAccount->OUCHAccount[0] = fullAccount[offset];
      tradingAccount->OUCHAccount[1] = fullAccount[offset+1];
      tradingAccount->OUCHAccount[2] = 0;
    }
    else if (currentReadLine == 3)
    {
      char fullAccount[32];
      strcpy(fullAccount, tempBuffer);
      int offset = strlen(fullAccount) - 2;
      
      tradingAccount->RASHAccount[0] = fullAccount[offset];
      tradingAccount->RASHAccount[1] = fullAccount[offset+1];
      tradingAccount->RASHAccount[2] = 0;
    }
    else if (currentReadLine == 4) // NYSE
    {
      strcpy(tradingAccount->NyseCCGAccount, tempBuffer);
    }
    else if (currentReadLine == 5) // BATSZ BOE
    {
      strcpy(tradingAccount->BATSZBOEAccount, tempBuffer);
    }
    else if (currentReadLine == 6) // EDGX
    {
      char fullAccount[32];
      strcpy(fullAccount, tempBuffer);
      int offset = strlen(fullAccount) - 2;
      
      tradingAccount->EDGXAccount[0] = fullAccount[offset];
      tradingAccount->EDGXAccount[1] = fullAccount[offset+1];
      tradingAccount->EDGXAccount[2] = 0;
    }
    else if (currentReadLine == 7) // EDGA
    {
      char fullAccount[32];
      strcpy(fullAccount, tempBuffer);
      int offset = strlen(fullAccount) - 2;
      
      tradingAccount->EDGAAccount[0] = fullAccount[offset];
      tradingAccount->EDGAAccount[1] = fullAccount[offset+1];
      tradingAccount->EDGAAccount[2] = 0;
    }
    else if (currentReadLine == 8) // BX
    {
      char fullAccount[32];
      strcpy(fullAccount, tempBuffer);
      int offset = strlen(fullAccount) - 2;
      
      tradingAccount->BXAccount[0] = fullAccount[offset];
      tradingAccount->BXAccount[1] = fullAccount[offset+1];
      tradingAccount->BXAccount[2] = 0;
    }
    else if (currentReadLine == 9) // BYX BOE
    {
      strcpy(tradingAccount->BYXBOEAccount, tempBuffer);
    }
    else if (currentReadLine == 10) // PSX
    {
      char fullAccount[32];
      strcpy(fullAccount, tempBuffer);
      int offset = strlen(fullAccount) - 2;
      
      tradingAccount->PSXAccount[0] = fullAccount[offset];
      tradingAccount->PSXAccount[1] = fullAccount[offset+1];
      tradingAccount->PSXAccount[2] = 0;
    }
    else
    {
      // Close file to release resources
      fclose(fileDesc);
      TraceLog(ERROR_LEVEL, "Critical! %s has invalid format! Trade Server can not continue.\n", fullPath);
      sleep(3); exit(0);
      return ERROR;
    }
    
    currentReadLine++;
  }
  
  // Close file to release resources
  fclose(fileDesc);
  
  if (currentReadLine != 11)
  {
    TraceLog(ERROR_LEVEL, "Critical! %s has invalid format! Trade Server can not continue.\n", fullPath);
    sleep(3); exit(0);
  }
  
  TraceLog(DEBUG_LEVEL, "Trading account type: %d\n", tradingAccount->accountType);
  TraceLog(DEBUG_LEVEL, "Trading account for Arca Direct: %s\n", tradingAccount->ArcaDirectAccount);
  TraceLog(DEBUG_LEVEL, "Trading account for NYSE CCG: %s\n", tradingAccount->NyseCCGAccount);
  TraceLog(DEBUG_LEVEL, "Trading account for OUCH (just token): %s\n", tradingAccount->OUCHAccount);
  TraceLog(DEBUG_LEVEL, "Trading account for RASH (just token): %s\n", tradingAccount->RASHAccount);
  TraceLog(DEBUG_LEVEL, "Trading account for BATSZ BOE: %s\n", tradingAccount->BATSZBOEAccount);
  TraceLog(DEBUG_LEVEL, "Trading account for BYX BOE: %s\n", tradingAccount->BYXBOEAccount);
  TraceLog(DEBUG_LEVEL, "Trading account for EDGX (just token): %s\n", tradingAccount->EDGXAccount);
  TraceLog(DEBUG_LEVEL, "Trading account for EDGA (just token): %s\n", tradingAccount->EDGAAccount);
  TraceLog(DEBUG_LEVEL, "Trading account for NASDAQ BX (just token): %s\n", tradingAccount->BXAccount);
  TraceLog(DEBUG_LEVEL, "Trading account for NASDAQ PSX (just token): %s\n", tradingAccount->PSXAccount);
  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadTradingSchedule
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int LoadTradingSchedule()
{
  TraceLog(DEBUG_LEVEL, "Loading %s\n", FILE_NAME_OF_TRADING_SCHEDULE);

  char fullPath[MAX_PATH_LEN];
  if (GetFullConfigPath(FILE_NAME_OF_TRADING_SCHEDULE, CONF_MISC, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Could not get full path of file '%s'\n", FILE_NAME_OF_TRADING_SCHEDULE);
    return ERROR;
  }
  
  //--------------------------------
  // Get the values from files
  //--------------------------------
  int retVal = 0;
 
  retVal += GetTimeInSecondsFromFile(fullPath, &TradingSchedule.timeToEndOfPreMarket, "time_to_end_of_pre_market");
  retVal += GetTimeInSecondsFromFile(fullPath, &TradingSchedule.timeToEndOfIntraday, "time_to_end_of_intraday");
  
  retVal += GetTimeInEpochNanosFromFile(fullPath, &TradingSchedule.bzx_open, "bzx_open");
  retVal += GetTimeInEpochNanosFromFile(fullPath, &TradingSchedule.bzx_close, "bzx_close");
  
  // folowings are optional for now, so don't count the error
  GetTimeInEpochNanosFromFile(fullPath, &TradingSchedule.edga_open, "edga_open");
  GetTimeInEpochNanosFromFile(fullPath, &TradingSchedule.edga_close, "edga_close");
  
  GetTimeInEpochNanosFromFile(fullPath, &TradingSchedule.edgx_open, "edgx_open");
  GetTimeInEpochNanosFromFile(fullPath, &TradingSchedule.edgx_close, "edgx_close");
  
  GetTimeInEpochNanosFromFile(fullPath, &TradingSchedule.nyse_open, "nyse_open");
  GetTimeInEpochNanosFromFile(fullPath, &TradingSchedule.nyse_close, "nyse_close");
  
  GetTimeInEpochNanosFromFile(fullPath, &TradingSchedule.byx_open, "byx_open");
  GetTimeInEpochNanosFromFile(fullPath, &TradingSchedule.byx_close, "byx_close");
  
  GetTimeInEpochNanosFromFile(fullPath, &TradingSchedule.rash_open, "rash_open");
  GetTimeInEpochNanosFromFile(fullPath, &TradingSchedule.rash_close, "rash_close");
  
  GetTimeInEpochNanosFromFile(fullPath, &TradingSchedule.psx_open, "psx_open");
  GetTimeInEpochNanosFromFile(fullPath, &TradingSchedule.psx_close, "psx_close");
  
  GetTimeInEpochNanosFromFile(fullPath, &TradingSchedule.bx_open, "bx_open");
  GetTimeInEpochNanosFromFile(fullPath, &TradingSchedule.bx_close, "bx_close");
  
  GetTimeInEpochNanosFromFile(fullPath, &TradingSchedule.nasdaq_open, "nasdaq_open");
  GetTimeInEpochNanosFromFile(fullPath, &TradingSchedule.nasdaq_close, "nasdaq_close");
  
  GetTimeInEpochNanosFromFile(fullPath, &TradingSchedule.arca_open, "arca_open");
  GetTimeInEpochNanosFromFile(fullPath, &TradingSchedule.arca_close, "arca_close");

  TraceLog(DEBUG_LEVEL, "Time (seconds since midnight) to end of Pre Market: %d\n", TradingSchedule.timeToEndOfPreMarket);
  TraceLog(DEBUG_LEVEL, "Time (seconds since midnight) to end of Intraday: %d\n", TradingSchedule.timeToEndOfIntraday);
  
  TraceLog(DEBUG_LEVEL, "bzx_open (nanos since epoch): %ld\n", TradingSchedule.bzx_open);
  TraceLog(DEBUG_LEVEL, "bzx_close (nanos since epoch): %ld\n", TradingSchedule.bzx_close);
  
  TraceLog(DEBUG_LEVEL, "arca_open (nanos since epoch): %ld\n", TradingSchedule.arca_open);
  TraceLog(DEBUG_LEVEL, "arca_close (nanos since epoch): %ld\n", TradingSchedule.arca_close);
  
  TraceLog(DEBUG_LEVEL, "nasdaq_open (nanos since epoch): %ld\n", TradingSchedule.nasdaq_open);
  TraceLog(DEBUG_LEVEL, "nasdaq_close (nanos since epoch): %ld\n", TradingSchedule.nasdaq_close);
  
  TraceLog(DEBUG_LEVEL, "bx_open (nanos since epoch): %ld\n", TradingSchedule.bx_open);
  TraceLog(DEBUG_LEVEL, "bx_close (nanos since epoch): %ld\n", TradingSchedule.bx_close);
  
  TraceLog(DEBUG_LEVEL, "psx_open (nanos since epoch): %ld\n", TradingSchedule.psx_open);
  TraceLog(DEBUG_LEVEL, "psx_close (nanos since epoch): %ld\n", TradingSchedule.psx_close);
  
  TraceLog(DEBUG_LEVEL, "rash_open (nanos since epoch): %ld\n", TradingSchedule.rash_open);
  TraceLog(DEBUG_LEVEL, "rash_close (nanos since epoch): %ld\n", TradingSchedule.rash_close);
  
  TraceLog(DEBUG_LEVEL, "byx_open (nanos since epoch): %ld\n", TradingSchedule.byx_open);
  TraceLog(DEBUG_LEVEL, "byx_close (nanos since epoch): %ld\n", TradingSchedule.byx_close);
  
  TraceLog(DEBUG_LEVEL, "nyse_open (nanos since epoch): %ld\n", TradingSchedule.nyse_open);
  TraceLog(DEBUG_LEVEL, "nyse_close (nanos since epoch): %ld\n", TradingSchedule.nyse_close);
  
  TraceLog(DEBUG_LEVEL, "edgx_open (nanos since epoch): %ld\n", TradingSchedule.edgx_open);
  TraceLog(DEBUG_LEVEL, "edgx_close (nanos since epoch): %ld\n", TradingSchedule.edgx_close);
  
  TraceLog(DEBUG_LEVEL, "edga_open (nanos since epoch): %ld\n", TradingSchedule.edga_open);
  TraceLog(DEBUG_LEVEL, "edga_close (nanos since epoch): %ld\n", TradingSchedule.edga_close);
  
  if (retVal != 0)
  {
    return ERROR;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadStaleMarketDataConfig
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int LoadStaleMarketDataConfig()
{
  TraceLog(DEBUG_LEVEL, "Loading %s\n", FILE_NAME_OF_STALE_DATA_THRESHOLD);

  char fullPath[MAX_PATH_LEN];
  if (GetFullConfigPath(FILE_NAME_OF_STALE_DATA_THRESHOLD, CONF_MISC, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Could not get full path of file '%s'\n", FILE_NAME_OF_STALE_DATA_THRESHOLD);
    return ERROR;
  }
  
  //--------------------------------
  // Get the values from files
  //--------------------------------
  DataDelayThreshold[BOOK_ARCA] = Parameters_get_long(fullPath, "arca_data_delay_threshold", 0);
  DataDelayThreshold[BOOK_NASDAQ] = Parameters_get_long(fullPath, "nasdaq_data_delay_threshold", 0);
  DataDelayThreshold[BOOK_NDBX] = Parameters_get_long(fullPath, "bx_data_delay_threshold", 0);
  DataDelayThreshold[BOOK_NYSE] = Parameters_get_long(fullPath, "nyse_data_delay_threshold", 0);
  DataDelayThreshold[BOOK_AMEX] = Parameters_get_long(fullPath, "amex_data_delay_threshold", 0);
  DataDelayThreshold[BOOK_BATSZ] = Parameters_get_long(fullPath, "batsz_data_delay_threshold", 0);
  DataDelayThreshold[BOOK_EDGX] = Parameters_get_long(fullPath, "edgx_data_delay_threshold", 0);
  DataDelayThreshold[BOOK_EDGA] = Parameters_get_long(fullPath, "edga_data_delay_threshold", 0);
  DataDelayThreshold[BOOK_BYX] = Parameters_get_long(fullPath, "byx_data_delay_threshold", 0);
  DataDelayThreshold[BOOK_PSX] = Parameters_get_long(fullPath, "psx_data_delay_threshold", 0);
  
  int bookIndex;
  for (bookIndex = 0; bookIndex < MAX_ECN_BOOK; bookIndex++)
  {
    if (DataDelayThreshold[bookIndex] > 0)
    {
      TraceLog(DEBUG_LEVEL, "Data delay threshold for book index %d is %ld\n", bookIndex, DataDelayThreshold[bookIndex]);
    }
    else
    {
      TraceLog(ERROR_LEVEL, "Invalid data delay threshold for book index %d: %d\n", bookIndex, DataDelayThreshold[bookIndex]);
      return ERROR;
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessToInitTotalTimeOffset
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessToInitTotalTimeOffset()
{
  time_t now = hbitime_seconds();
  
  struct tm local;
  localtime_r(&now, &local);
  TraceLog(DEBUG_LEVEL, "dst info: %ld %d %d %d %d\n",
                          now, local.tm_hour, local.tm_min, local.tm_sec, local.tm_isdst);
    
  // Without DST: EST = GMT -5, UTC = GMT 0
  if (local.tm_isdst == 0)
  {
    // Daylight saving time does not effect
    TotalTimeOffsetInSeconds = 5 * 3600;
    TraceLog(DEBUG_LEVEL, "Daylight saving time is not in effect.\n");
  }
  else if (local.tm_isdst > 0)
  {
    // Daylight saving time is in effect
    TotalTimeOffsetInSeconds = 4 * 3600;
    TraceLog(DEBUG_LEVEL, "Daylight saving time is in effect\n");
  }
  else
  {
    // The information is not available
    TraceLog(WARN_LEVEL, "Could not get the Daylight Saving Time information, 5 hours time offset between EST and GMT will be used\n");
    TotalTimeOffsetInSeconds = 5 * 3600;
  }
  
  TraceLog(DEBUG_LEVEL, "Total offset seconds: %d\n", TotalTimeOffsetInSeconds);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessOecInformation
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessOecInformation(char* arg)
{
  t_LongParameters oecParams;
  oecParams.count = Lrc_Split_String(arg, "orderentry::", &oecParams);
  t_OEC_Info oecConfig;
  
  OecConfig[ORDER_NASDAQ_OUCH].protocol = -1;
  OecConfig[ORDER_NASDAQ_BX].protocol = PROTOCOL_UDP;
  OecConfig[ORDER_NASDAQ_PSX].protocol = PROTOCOL_UDP;
  OecConfig[ORDER_NASDAQ_RASH].protocol = PROTOCOL_TCP;
  OecConfig[ORDER_ARCA_DIRECT].protocol = PROTOCOL_TCP;
  OecConfig[ORDER_NYSE_CCG].protocol = PROTOCOL_TCP;
  OecConfig[ORDER_BATSZ_BOE].protocol = PROTOCOL_TCP;
  OecConfig[ORDER_BYX_BOE].protocol = PROTOCOL_TCP;
  OecConfig[ORDER_EDGX].protocol = PROTOCOL_TCP;
  OecConfig[ORDER_EDGA].protocol = PROTOCOL_TCP;
  
  int i;
  
  //can ignore the first parameter due to it doesn't include 'orderentry::'
  for (i = 0; i < oecParams.count; i++)
  {
    if (ParseRawOecConfig(oecParams.paramList[i], &oecConfig) == SUCCESS) //read keyword-value pairs from commandline into oecConfig structure
    {
      GetOecConfig(&oecConfig);
    }
  }
  
  for (i = 0; i < MAX_ECN_ORDER; i++)
  {
    if (IsGottenOrderConfig[i] == NO)
    {
      TraceLog(DEBUG_LEVEL, "Configuration will be loaded from file for oecId %d\n", i);
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ParseRawConfig
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ParseRawOecConfig(char *rawConfig, t_OEC_Info *oecConfig)
{
  //Init OEC Config
  oecConfig->orderIndex = -1;
  oecConfig->port = -1;
  memset(oecConfig->positions, 0, sizeof(t_Keyword_Value_Pointer) * MAX_OEC_INFO_FIELD);
  memset(oecConfig->ipAddress, 0, IP_ADDRESS_FIELD_LEN);
  memset(oecConfig->userName, 0, USER_NAME_FIELD_LEN);
  memset(oecConfig->password, 0, PASSWORD_FIELD_LEN);
  memset(oecConfig->compid, 0, MAX_COMP_GROUP_ID_LEN);
  memset(oecConfig->sessionSubId, 0, MAX_LINE_LEN);
  oecConfig->autoCancelOnDisconnect = -1;
  oecConfig->noUnspecifiedUnitReplay = -1;
  
  int posIndex = 0; //counter of position
  
  // Determine order index
  if (strstr(rawConfig, "OEARCADIRECT") != NULL)
  {
    oecConfig->orderIndex = ORDER_ARCA_DIRECT;
  }
  else if (strstr(rawConfig, "OENASDAQRASH") != NULL)
  {
    oecConfig->orderIndex = ORDER_NASDAQ_RASH;
  }
  else if (strstr(rawConfig, "OENASDAQOUCH") != NULL)
  {
    oecConfig->orderIndex = ORDER_NASDAQ_OUCH;
  }
  else if (strstr(rawConfig, "OENASDAQBX") != NULL)
  {
    oecConfig->orderIndex = ORDER_NASDAQ_BX;
  }
  else if (strstr(rawConfig, "OENASDAQPSX") != NULL)
  {
    oecConfig->orderIndex = ORDER_NASDAQ_PSX;
  }
  else if (strstr(rawConfig, "OENYSECCG") != NULL)
  {
    oecConfig->orderIndex = ORDER_NYSE_CCG;
  }
  else if (strstr(rawConfig, "OEBATSBOE") != NULL)
  {
    oecConfig->orderIndex = ORDER_BATSZ_BOE;
  }
  else if (strstr(rawConfig, "OEBYXBOE") != NULL)
  {
    oecConfig->orderIndex = ORDER_BYX_BOE;
  }
  else if (strstr(rawConfig, "OEEDGX") != NULL)
  {
    oecConfig->orderIndex = ORDER_EDGX;
  }
  else if (strstr(rawConfig, "OEEDGA") != NULL)
  {
    oecConfig->orderIndex = ORDER_EDGA;
  }
  else
  {
    return ERROR;
  }
  
  // Get IP & Port
  if (GetIpAddressAndPortFromOec(rawConfig, oecConfig, &posIndex) == ERROR)
  {
    //All OEC have the same keyword 'address'
    return ERROR;
  }
  
  //Get Username
  if (oecConfig->orderIndex != ORDER_NYSE_CCG)
  {
    GetOecField(rawConfig, OEC_KEYWORD_USERNAME, oecConfig, &posIndex);
  }
  else
  {
    //Get Sender Comp ID (this is username of NYSE CCG)
    GetOecField(rawConfig, OEC_KEYWORD_SENDER_COMP_ID, oecConfig, &posIndex);
  }
  
  //Get Password
  GetOecField(rawConfig, OEC_KEYWORD_PASSWORD, oecConfig, &posIndex);
  
  if (oecConfig->orderIndex == ORDER_BATSZ_BOE || oecConfig->orderIndex == ORDER_BYX_BOE)
  {
    //Get Sender Sub ID: for BATSZ BOE only
    GetOecField(rawConfig, OEC_KEYWORD_SENDER_SUB_ID, oecConfig, &posIndex);
  }
  
  if (oecConfig->orderIndex == ORDER_NYSE_CCG ||
    oecConfig->orderIndex == ORDER_ARCA_DIRECT)
  {
    //Get Cancel On Disconnect: for ARCA & NYSE
    GetOecField(rawConfig, OEC_KEYWORD_CANCEL_ON_DISCONNECT, oecConfig, &posIndex);
  }
  
  if (oecConfig->orderIndex == ORDER_BATSZ_BOE || oecConfig->orderIndex == ORDER_BYX_BOE)
  {
    //Get no_unspecified_unit_replay: for BATS BOE only
    GetOecField(rawConfig, OEC_KEYWORD_UNIT_REPLAY, oecConfig, &posIndex);
  }
  
  if (oecConfig->orderIndex == ORDER_ARCA_DIRECT)
  {
    //Get Compid: for ARCA only
    GetOecField(rawConfig, OEC_KEYWORD_COMP_ID, oecConfig, &posIndex);
  }
  
  //--------------------------------------------------------------------------
  //Check validity of OEC information structure base on position (index) list
  //--------------------------------------------------------------------------
  // Find keywords 'port='
  strcpy(oecConfig->positions[posIndex].keyword, "port");
  oecConfig->positions[posIndex].posKeyword = strstr(rawConfig, "port=");
  oecConfig->positions[posIndex].posValue = oecConfig->positions[posIndex].posKeyword;
  posIndex++;
  int i;
  for (i = 0; i < MAX_OEC_INFO_FIELD - 1; i++)
  {
    if (oecConfig->positions[i].posKeyword != NULL &&
      oecConfig->positions[i].posValue != NULL)
    {
      int j;
      for (j = i + 1; j < MAX_OEC_INFO_FIELD; j++)
      {
        if (oecConfig->positions[i].posKeyword == oecConfig->positions[j].posKeyword ||
            oecConfig->positions[i].posKeyword == oecConfig->positions[j].posValue ||
            oecConfig->positions[i].posValue == oecConfig->positions[j].posKeyword ||
            oecConfig->positions[i].posValue == oecConfig->positions[j].posValue)
        {
          TraceLog(WARN_LEVEL, "Invalid OEC (orderIndex=%d) structure. Please check between keyword '%s' and '%s'\n",
                                oecConfig->orderIndex, oecConfig->positions[i].keyword, oecConfig->positions[j].keyword);
          return ERROR;
        }
      }
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  GetIpAddressAndPortFromOec
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int GetIpAddressAndPortFromOec(char *rawConfig, t_OEC_Info *oecConfig, int *posIndex)
{
  char* pBegin = NULL;
  char* pEnd = NULL;
  char tempChar = 0;
  int countBlank;
  char* posKeyword = NULL; //save the position of keyword 'address' in the rawConfig
  char* posValue = NULL; //save the position of value of keyword 'address' in the rawConfig
  
  //Find tag of address
  if ((pBegin = (strstr(rawConfig, "TcpPort"))))
  {
    OecConfig[oecConfig->orderIndex].protocol = PROTOCOL_TCP;
  }
  else if ((pBegin = (strstr(rawConfig, "UdpPort"))))
  {
    OecConfig[oecConfig->orderIndex].protocol = PROTOCOL_UDP;
  }
  else
  {
    TraceLog(WARN_LEVEL, "Could not find address field in OEC Config, order index =%d\n", oecConfig->orderIndex);
    return ERROR;
  }
  
  //Get IP address & Port
  pBegin = strstr(pBegin, "address");
  if (pBegin == NULL)
  {
    TraceLog(WARN_LEVEL, "Could not find address field in OEC Config, order index: %d\n", oecConfig->orderIndex);
    return ERROR;
  }
  posKeyword = pBegin; //save the position of keyword in the raw config string
  
  //Check valid input parameter
  if (IsValidParameter(pBegin, "address") == ERROR)
  {
    TraceLog(WARN_LEVEL, "Invalid parameters for 'address', order index: %d\n", oecConfig->orderIndex);
    return ERROR;
  }

  pBegin = strchr(pBegin, '=');
  if (pBegin == NULL)
  {
    TraceLog(WARN_LEVEL, "Could not find address in OEC Config, order index: %d\n", oecConfig->orderIndex);
    return ERROR;
  }
  pBegin++; //Ignore '=', after this pBegin will point to IP & Port, e.g: 159.125.65.149:54207]
  
  //Ignore blanks
  countBlank = CountFirstBlanks(pBegin);
  pBegin += countBlank; //this is start of address without blanks
  
  //Find ':' which split IP & Port, e.g: 159.125.65.149:54207]
  pEnd = strchr(pBegin, ':');
  if (pEnd == NULL)
    return ERROR;
    
  //Copy IP
  posValue = pBegin; //save the position of keyword in the raw config string
  tempChar = *pEnd; //save pEnd character to copy
  *pEnd = 0; // now string is 159.125.65.149\054207]
  strncpy(oecConfig->ipAddress, pBegin, IP_ADDRESS_FIELD_LEN);
  RemoveCharacters(oecConfig->ipAddress, strlen(oecConfig->ipAddress));
  *pEnd = tempChar; //return orginal character which pEnd is pointing to
  pBegin = ++pEnd; //ignore ':', move pBegin to port
  
  //Find the end tag of address
  pEnd = strchr(pBegin, ']');
  if (pEnd == NULL)
    return ERROR;
  
  //Copy Port, string is 54207]
  tempChar = *pEnd;
  *pEnd = 0;
  if (CheckValidNumberString(pBegin) == ERROR)
  {
    TraceLog(WARN_LEVEL, "Invalid port number: '%s', order index: %d\n", pBegin, oecConfig->orderIndex);
    return ERROR;
  }
  oecConfig->port = atoi(pBegin); //atoi will trim blank of input string
  *pEnd = tempChar; //return original character which pEnd is pointing to
  
  if (*posIndex < MAX_OEC_INFO_FIELD)
  {
    strcpy(oecConfig->positions[*posIndex].keyword, "address");
    oecConfig->positions[*posIndex].posKeyword = posKeyword;
    oecConfig->positions[*posIndex].posValue = posValue;
    (*posIndex)++;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  GetOecField
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int GetOecField(char *rawConfig, int keywordType, t_OEC_Info *oecConfig, int *posIndex)
{
  char keyword[MAX_LINE_LEN];
  char* posKeyword = NULL; //save the position of keyword 'address' in the rawConfig
  char* posValue = NULL; //save the position of value of keyword 'address' in the rawConfig
  
  switch (keywordType)
  {
    case OEC_KEYWORD_USERNAME:
      strncpy(keyword, "username", MAX_LINE_LEN);
      break;
    case OEC_KEYWORD_PASSWORD:
      strncpy(keyword, "password", MAX_LINE_LEN);
      break;
    case OEC_KEYWORD_SENDER_SUB_ID:
      strncpy(keyword, "sender_sub_id", MAX_LINE_LEN);
      break;
    case OEC_KEYWORD_CANCEL_ON_DISCONNECT:
      strncpy(keyword, "cancel_on_disconnect", MAX_LINE_LEN);
      break;
    case OEC_KEYWORD_UNIT_REPLAY:
      strncpy(keyword, "no_unspecified_unit_replay", MAX_LINE_LEN);
      break;
    case OEC_KEYWORD_COMP_ID:
      strncpy(keyword, "comp_id", MAX_LINE_LEN);
      break;
    case OEC_KEYWORD_SENDER_COMP_ID:
      strncpy(keyword, "sender_comp_id", MAX_LINE_LEN);
      break;
    default:
      TraceLog(ERROR_LEVEL, "Invalid Keyword Type: %d\n", keywordType);
      return ERROR;
  }
  
  char* pBegin = NULL;
  char* pEnd = NULL;
  char tempChar = 0;
  int countBlank = 0;
  
  //-------------------------------
  // Find keyword
  //-------------------------------
  pBegin = strstr(rawConfig, keyword);
  if (pBegin == NULL)
  {
    // TraceLog(DEBUG_LEVEL, "Could not find field '%s' in OEC Config, order index: %d\n", keyword, oecConfig->orderIndex);
    return ERROR;
  }
  posKeyword = pBegin; //save the position of keyword in the raw config string
  
  //Check valid input parameter
  if (IsValidParameter(pBegin, keyword) == ERROR)
  {
    TraceLog(WARN_LEVEL, "Invalid parameters for '%s'\n", keyword);
    return ERROR;
  }

  pBegin = strchr(pBegin, '=');
  if (pBegin == NULL)
  {
    TraceLog(WARN_LEVEL, "Could not find value of '%s' in OEC Config, order index: %d\n", keyword, oecConfig->orderIndex);
    return ERROR;
  }
  pBegin++; //ignore character '='
  
  //Ignore blanks
  countBlank = CountFirstBlanks(pBegin);
  pBegin += countBlank; //ignore all blanks before value
  
  //Find end of value
  pEnd = pBegin;
  while(pEnd != NULL)
  {
    if (*pEnd == USED_CHAR_CODE ||      //blank
        *pEnd == CHAR_CODE_NEW_LINE ||  //end of line
        *pEnd == CHAR_CODE_QUOTATION || //close of quotation
        *pEnd == ']'                    //close of OEC tag
        )
    {
      //found end of value
      break;
    }
    
    pEnd++;
  }
  
  //-------------------------------
  // Copy value of keyword
  //-------------------------------
  tempChar = *pEnd; //save character
  *pEnd = 0; //set to NULL to copy string
  posValue = pBegin;
  
  switch (keywordType)
  {
    case OEC_KEYWORD_USERNAME:
      //this does not apply for NYSE CCG
      if (oecConfig->orderIndex != ORDER_NYSE_CCG)
      {
        strncpy(oecConfig->userName, pBegin, USER_NAME_FIELD_LEN);
        RemoveCharacters(oecConfig->userName, strlen(oecConfig->userName));
      }
      break;
    case OEC_KEYWORD_SENDER_COMP_ID:
      //this applies for NYSE CCG only
      if (oecConfig->orderIndex == ORDER_NYSE_CCG)
      {
        strncpy(oecConfig->userName, pBegin, USER_NAME_FIELD_LEN);
        RemoveCharacters(oecConfig->userName, strlen(oecConfig->userName));
      }
      break;
    case OEC_KEYWORD_PASSWORD:
      strncpy(oecConfig->password, pBegin, PASSWORD_FIELD_LEN);
      RemoveCharacters(oecConfig->password, strlen(oecConfig->password));
      break;
    case OEC_KEYWORD_COMP_ID:
      strncpy(oecConfig->compid, pBegin, MAX_COMP_GROUP_ID_LEN);
      RemoveCharacters(oecConfig->compid, strlen(oecConfig->compid));
      break;
    case OEC_KEYWORD_SENDER_SUB_ID:
      strncpy(oecConfig->sessionSubId, pBegin, MAX_LINE_LEN);
      RemoveCharacters(oecConfig->sessionSubId, strlen(oecConfig->sessionSubId));
      break;
    case OEC_KEYWORD_CANCEL_ON_DISCONNECT:
      if (*pBegin == '0')
        oecConfig->autoCancelOnDisconnect = 0;
      else if (*pBegin == '1')
        oecConfig->autoCancelOnDisconnect = 1;
      else
      {
        TraceLog(ERROR_LEVEL, "Invalid 'Cancel On Disconnect': '%s'\n", pBegin);
        oecConfig->autoCancelOnDisconnect = -1; //this is ERROR, it will be detected when copy to NYSE, ...
      }
      break;
    case OEC_KEYWORD_UNIT_REPLAY:
      if (*pBegin == '0')
        oecConfig->noUnspecifiedUnitReplay = 0;
      else if (*pBegin == '1')
        oecConfig->noUnspecifiedUnitReplay = 1;
      else
      {
        TraceLog(WARN_LEVEL, "Invalid 'No Unspecified Unit Replay': '%s'\n", pBegin);
        oecConfig->noUnspecifiedUnitReplay = -1; //this is ERROR, it will be detected when copy to BATSZ
      }
      break;
    default:
      TraceLog(ERROR_LEVEL, "Invalid keyword type: %d\n", keywordType);
      return ERROR;
  }
  
  if (*posIndex < MAX_OEC_INFO_FIELD)
  {
    strcpy(oecConfig->positions[*posIndex].keyword, keyword);
    oecConfig->positions[*posIndex].posKeyword = posKeyword; //save the position of value in the raw config string
    oecConfig->positions[*posIndex].posValue = posValue; //save the position of value in the raw config string
    (*posIndex)++;
  }

  *pEnd = tempChar; //return original character
  
  return SUCCESS;
}

int GetOecConfig(t_OEC_Info *oecConfig)
{
  int venueId = oecConfig->orderIndex;
  TraceLog(DEBUG_LEVEL, "Configuration values from commandline for oec %d:\n", venueId);
  
  // All venues have following configurations
  if (CheckValidIpv4Address(oecConfig->ipAddress) == SUCCESS)
  {
    strncpy(OecConfig[venueId].ipAddress, oecConfig->ipAddress, 32);
    TraceLog(DEBUG_LEVEL, "\t IP address: %s\n", OecConfig[venueId].ipAddress);
  }
  else
  {
    TraceLog(WARN_LEVEL, "\t Could not get IP Address\n");
    return ERROR;
  }
  
  if (CheckValidPort(oecConfig->port) == SUCCESS)
  {
    OecConfig[venueId].port = oecConfig->port;
    TraceLog(DEBUG_LEVEL, "\t Port: %d\n", OecConfig[venueId].port);
  }
  else
  {
    TraceLog(WARN_LEVEL, "\t Invalid port: %d\n", oecConfig->port);
    return ERROR;
  }
  
  if (strlen(oecConfig->userName) > 0)
  {
    strncpy(OecConfig[venueId].userName, oecConfig->userName, 32);
    TraceLog(DEBUG_LEVEL, "\t Username: %s\n", OecConfig[venueId].userName);
  }
  else
  {
    TraceLog(WARN_LEVEL, "\t Could not get Username\n");
    return ERROR;
  }
  
  if (strlen(oecConfig->password) > 0)
  {
    strncpy(OecConfig[venueId].password, oecConfig->password, 32);
    TraceLog(DEBUG_LEVEL, "\t Password: %s\n", OecConfig[venueId].password);
  }
  else
  {
    TraceLog(WARN_LEVEL, "\t Could not get Password\n");
    return ERROR;
  }

  // Followings are additional values for specific venues
  switch (venueId)
  {
    case ORDER_ARCA_DIRECT:
      if (strlen(oecConfig->compid) > 0)
      {
        strncpy(OecConfig[venueId].compGroupID, oecConfig->compid, 32);
        TraceLog(DEBUG_LEVEL, "\t Comp Group ID: %s\n", OecConfig[venueId].compGroupID);
      }
      else
      {
        TraceLog(WARN_LEVEL, "\t Could not get Comp Group ID\n");
        return ERROR;
      }
      
      if (oecConfig->autoCancelOnDisconnect == 0 || oecConfig->autoCancelOnDisconnect == 1)
      {
        OecConfig[venueId].autoCancelOnDisconnect = oecConfig->autoCancelOnDisconnect;
        TraceLog(DEBUG_LEVEL, "\t Auto cancel on disconnect: %d\n", OecConfig[venueId].autoCancelOnDisconnect);
      }
      else
      {
        TraceLog(ERROR_LEVEL, "\t Invalid Auto Cancel On Disconnect (%d), accept only 0 or 1\n", oecConfig->autoCancelOnDisconnect);
        return ERROR;
      }
      break;
    case ORDER_NASDAQ_OUCH:
      if( !(OecConfig[ORDER_NASDAQ_OUCH].protocol == PROTOCOL_TCP || OecConfig[ORDER_NASDAQ_OUCH].protocol == PROTOCOL_UDP) )
      {
        TraceLog(WARN_LEVEL, "\t Invalid NASDAQ OUCH protocol\n");
        return ERROR;
      }
      break;
    case ORDER_NYSE_CCG:
      if (oecConfig->autoCancelOnDisconnect == 0 || oecConfig->autoCancelOnDisconnect == 1)
      {
        OecConfig[venueId].autoCancelOnDisconnect = oecConfig->autoCancelOnDisconnect;
        TraceLog(DEBUG_LEVEL, "\t Auto cancel on disconnect: %d\n", OecConfig[venueId].autoCancelOnDisconnect);
      }
      else
      {
        TraceLog(ERROR_LEVEL, "\t Invalid Auto Cancel On Disconnect (%d), accept only 0 or 1\n", oecConfig->autoCancelOnDisconnect);
        return ERROR;
      }
      break;
    case ORDER_BATSZ_BOE:
    case ORDER_BYX_BOE:
      if (strlen(oecConfig->sessionSubId) > 0)
      {
        strncpy(OecConfig[venueId].sessionSubId, oecConfig->sessionSubId, 32);
        TraceLog(DEBUG_LEVEL, "\t SessionSubID: %s\n", OecConfig[venueId].sessionSubId);
      }
      else
      {
        TraceLog(WARN_LEVEL, "\t Could not get Session Sub ID\n");
        return ERROR;
      }
      
      if (oecConfig->noUnspecifiedUnitReplay == 0 || oecConfig->noUnspecifiedUnitReplay == 1)
      {
        OecConfig[venueId].noUnspecifiedUnitReplay = oecConfig->noUnspecifiedUnitReplay;
        TraceLog(DEBUG_LEVEL, "\t No Unspecified Unit Replay: %d\n", OecConfig[venueId].noUnspecifiedUnitReplay);
      }
      else
      {
        TraceLog(WARN_LEVEL, "\t Invalid No Unspecified Unit Replay (%d), accept only 0 or 1\n", oecConfig->noUnspecifiedUnitReplay);
        return ERROR;
      }
      break;
  }
  
  OecConfig[venueId].sessionId = NextSessionId++;
  IsGottenOrderConfig[venueId] = YES;
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessMdcInformation
****************************************************************************/
int ProcessMdcInformation(char* arg)
{
  t_LongParameters mdcParams;
  mdcParams.count = Lrc_Split_String(arg, "marketdata::", &mdcParams);
  
  char rawConfig[MAX_LEN_OF_LONG_PARAMETER];
  int i;
  for (i = 0; i < mdcParams.count; i++)
  {
    //can ignore the first parameter due to it doesn't include 'marketdata::'
    strcpy(rawConfig, mdcParams.paramList[i]);
    
    // Parse MDC Info base on MDC Name
    if (strstr(rawConfig, "MDArcaXDP") != NULL)
    {
      if (ParseArcaBookConfig(rawConfig) == SUCCESS)
      {
        IsGottenBookConfig[BOOK_ARCA] = YES;
      }
    }
    else if (strstr(rawConfig, "MDNasdaqITCH") != NULL)
    {
      if (ParseNasdaqMdcConfig(rawConfig, BOOK_NASDAQ, &NASDAQ_Book_Conf) == SUCCESS)
      {
        IsGottenBookConfig[BOOK_NASDAQ] = YES;
      }
    }
    else if (strstr(rawConfig, "MDNdbxITCH") != NULL)
    {
      if (ParseNasdaqMdcConfig(rawConfig, BOOK_NDBX, &NDBX_Book_Conf) == SUCCESS)
      {
        IsGottenBookConfig[BOOK_NDBX] = YES;
      }
    }
    else if (strstr(rawConfig, "MDPsxITCH") != NULL)
    {
      if (ParseNasdaqMdcConfig(rawConfig, BOOK_PSX, &PSX_Book_Conf) == SUCCESS)
      {
        IsGottenBookConfig[BOOK_NDBX] = YES;
      }
    }
    else if (strstr(rawConfig, "MDNYSEXDP") != NULL)
    {
      if (ParseNyseBookConfig(rawConfig, BOOK_NYSE) == SUCCESS)
      {
        IsGottenBookConfig[BOOK_NYSE] = YES;
      }
    }
    else if (strstr(rawConfig, "MDAMEXXDP") != NULL)
    {
      if (ParseNyseBookConfig(rawConfig, BOOK_AMEX) == SUCCESS)
      {
        IsGottenBookConfig[BOOK_AMEX] = YES;
      }
    }
    else if (strstr(rawConfig, "MDBatsPitch") != NULL)
    {
      if (ParseBatszBookConfig(rawConfig) == SUCCESS)
      {
        IsGottenBookConfig[BOOK_BATSZ] = YES;
      }
    }
    else if (strstr(rawConfig, "MDBYXPitch") != NULL)
    {
      if (ParseBYXBookConfig(rawConfig) == SUCCESS)
      {
        IsGottenBookConfig[BOOK_BYX] = YES;
      }
    }
    else if (strstr(rawConfig, "MDEDGE") != NULL)
    {
      if (ParseEdgxBookConfig(rawConfig) == SUCCESS)
      {
        IsGottenBookConfig[BOOK_EDGX] = YES;
      }
    }
    else if (strstr(rawConfig, "MDEDGA") != NULL)
    {
      if (ParseEdgaBookConfig(rawConfig) == SUCCESS)
      {
        IsGottenBookConfig[BOOK_EDGA] = YES;
      }
    }
    else if (strstr(rawConfig, "MDCQS") != NULL)
    {
      if (ParseCqsFeedConfig(rawConfig) == SUCCESS)
      {
        IsGottenFeedConfig[CQS_SOURCE] = YES;
      }
    }
    else if (strstr(rawConfig, "MDUQDF") != NULL)
    {
      if (ParseUqdfFeedConfig(rawConfig) == SUCCESS)
      {
        IsGottenFeedConfig[UQDF_SOURCE] = YES;
      }
    }
  }
  
  for (i = 0; i < MAX_ECN_BOOK; i++)
  {
    if (IsGottenBookConfig[i] == NO)
    {
      TraceLog(DEBUG_LEVEL, "%s MDC configuration will be loaded from file\n", GetBookNameByIndex(i));
    }
  }
  
  if (IsGottenFeedConfig[CQS_SOURCE] == NO)
  {
    TraceLog(DEBUG_LEVEL, "CQS Feed configuration will be loaded from file\n");
  }
  
  if (IsGottenFeedConfig[UQDF_SOURCE] == NO)
  {
    TraceLog(DEBUG_LEVEL, "UQDF Feed configuration will be loaded from file\n");
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  GetIpAddressAndPortFromMdc
- Input:          + Raw String which contains Address required to get
                  + Address Keyword appears in Raw String
- Output:         + IP & Port of Address Keyword
                  + Index of keyword and its value in raw string
- Return:         SUCCESS or ERROR
- Description:    This function is used to get Address from Raw String which
                  contains Address Keyword. IP & Port is retained in
                  Connection Info if they are successfully gotten.
- Usage:          Use for MDC Info only
****************************************************************************/
int GetIpAddressAndPortFromMdc(char *rawConfig, const char* keywords, t_ConnectionInfo *connectionInfo, t_Keyword_Value_Pointer *resultPosition)
{
  char* pBegin = NULL;
  char* pEnd = NULL;
  char tempChar = 0;
  int countBlank;
  char *posKeyword = NULL;
  char *posValue = NULL;
  
  //Find tag of keyword
  pBegin = strstr(rawConfig, keywords);
  if (pBegin == NULL)
  {
    // TraceLog(WARN_LEVEL, "Could not find parameter'%s' in MDC Config\n", keywords);
    return ERROR;
  }
  posKeyword = pBegin;
  
  //Check valid input parameter
  if (IsValidParameter(pBegin, keywords) == ERROR)
  {
    TraceLog(WARN_LEVEL, "Invalid parameters for '%s'\n", keywords);
    return ERROR;
  }
  
  pBegin = strchr(pBegin, '=');
  if (pBegin == NULL)
  {
    TraceLog(WARN_LEVEL, "Could not find address value for '%s' in MDC Config\n", keywords);
    return ERROR;
  }
  pBegin++; //Ignore '=', after this pBegin will point to IP & Port, e.g: 159.125.65.149:54207]
  
  //Ignore blanks
  countBlank = CountFirstBlanks(pBegin);
  pBegin += countBlank; //this is start of address without blanks
  posValue = pBegin;
  
  //Find ':' which split IP & Port, e.g: 159.125.65.149:54207]
  pEnd = strchr(pBegin, ':');
  if (pEnd == NULL)
    return ERROR;
    
  //Copy IP
  tempChar = *pEnd; //save pEnd character to copy
  *pEnd = 0; // now string is 159.125.65.149\054207]
  strncpy(connectionInfo->ip, pBegin, IP_ADDRESS_FIELD_LEN);
  RemoveCharacters(connectionInfo->ip, strlen(connectionInfo->ip));
  *pEnd = tempChar; //return original character which pEnd is pointing to
  pBegin = ++pEnd; //ignore ':', move pBegin to port
  
  //--------------------------------------------------------------------------
  //Find Port: End address's tag maybe ']' or another "UdpPort" or "TcpPort"
  //                     or new line or blank
  //--------------------------------------------------------------------------
  //Ignore blanks
  countBlank = CountFirstBlanks(pBegin);
  pBegin += countBlank; //now, pBegin point to first digit of port
  if (*pBegin > '9' || *pBegin < '0')
  {
    TraceLog(WARN_LEVEL, "Could not find port for '%s' in MDC Config\n", keywords);
    return ERROR;
  }
  
  //Continue finding the end of port value
  pEnd = pBegin;
  while (*pEnd >= '0' && *pEnd <= '9')
  {
    pEnd++;
  }
  
  //Check end of port or not
  //at this time, *pEnd is not in range '0' - '9'
  //so if *pEnd is not end of port value, then port is invalid
  if (*pEnd == USED_CHAR_CODE || //blank
    *pEnd == CHAR_CODE_NEW_LINE || //end of line
    *pEnd == CHAR_CODE_QUOTATION || //close of quotation
    *pEnd == ']' //close of OEC tag
    )
  {
    //this end of port, port is valid
  }
  else
  {
    TraceLog(DEBUG_LEVEL, "Current character code: %d\n", *pEnd);
    TraceLog(WARN_LEVEL, "Invalid port of '%s'\n", keywords);
    return ERROR;
  }
  
  //Copy Port
  tempChar = *pEnd;
  *pEnd = 0;
  char portStr[MAX_LINE_LEN];
  strncpy(portStr, pBegin, MAX_LINE_LEN);
  RemoveCharacters(portStr, strlen(portStr));
  connectionInfo->port = atoi(portStr);
  *pEnd = tempChar; //return original character which pEnd is pointing to
  
  //Check validity of IP & Port
  if (CheckValidIpv4Address(connectionInfo->ip) == ERROR)
  {
    TraceLog(WARN_LEVEL, "Try to get %s, invalid IP: '%s'\n", keywords, connectionInfo->ip);
    return ERROR;
  }
    
  if (CheckValidPort(connectionInfo->port) == ERROR)
  {
    TraceLog(WARN_LEVEL, "Try to get %s, invalid port: %d\n", keywords, connectionInfo->port);
    return ERROR;
  }
  
  strcpy(resultPosition->keyword, keywords);
  resultPosition->posKeyword = posKeyword;
  resultPosition->posValue = posValue;

  return SUCCESS;
}

/****************************************************************************
- Function name:  GetValueFromKeyword
- Input:          + Raw String which contains Keyword required to get
                  + Keyword to get value from Raw String
- Output:         + Value of Keyword.
                  IMPORTANT!!!! value has MAX_LINE_LEN characters
                  + Index of keyword and its value in raw string
- Return:         SUCCESS or ERROR
- Description:    This function is use to get value of a keyword from
                  raw string. Value is a continuous string which does not
                  contains any stopped word
                  Stopped word is: blank, new line, close of quotation '"'
                  or close of square bracket ']'
- Usage:      
****************************************************************************/
int GetValueFromKeyword(char *rawConfig, const char* keywords, char value[MAX_LINE_LEN], t_Keyword_Value_Pointer *resultPosition)
{
  char* pBegin = NULL;
  char* pEnd = NULL;
  char tempChar = 0;
  int countBlank = 0;
  memset(value, 0, MAX_LINE_LEN);
  char* posKeyword = NULL;
  char* posValue = NULL;
  
  // Find keywords
  pBegin = strstr(rawConfig, keywords);
  if (pBegin == NULL)
  {
    // TraceLog(WARN_LEVEL, "Could not find field '%s' in MDC Config\n", keywords);
    return ERROR;
  }
  posKeyword = pBegin;
  
  // Check valid input parameter
  if (IsValidParameter(pBegin, keywords) == ERROR)
  {
    TraceLog(WARN_LEVEL, "Invalid parameters for '%s'\n", keywords);
    return ERROR;
  }

  pBegin = strchr(pBegin, '=');
  if (pBegin == NULL)
  {
    TraceLog(WARN_LEVEL, "Could not find value of '%s' in MDC Config\n", keywords);
    return ERROR;
  }
  pBegin++; //ignore character '='
  
  //Ignore blanks
  countBlank = CountFirstBlanks(pBegin);
  pBegin += countBlank; //ignore all blanks before value
  
  //Find end of value
  pEnd = pBegin;
  while(pEnd != NULL)
  {
    if (*pEnd == USED_CHAR_CODE || //blank
      *pEnd == CHAR_CODE_NEW_LINE || //end of line
      *pEnd == CHAR_CODE_QUOTATION || //close of quotation
      *pEnd == ']' //tag's close
      )
    {
      //found end of value
      break;
    }
    
    pEnd++;
  }
  
  // Copy value of keyword
  tempChar = *pEnd; //save character
  *pEnd = 0; //set to NULL to copy string
  strncpy(value, pBegin, MAX_LINE_LEN);
  RemoveCharacters(value, strlen(value));
  posValue = pBegin;
  *pEnd = tempChar; //return original character

  //Successfully parsed information, save keyword & value position
  strcpy(resultPosition->keyword, keywords);
  resultPosition->posKeyword = posKeyword;
  resultPosition->posValue = posValue;

  return SUCCESS;
}

/****************************************************************************
- Function name:  ParseArcaBookConfig
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ParseArcaBookConfig(char *rawConfig)
{
  //--------------------------------------------------------------------------
  //Parse ARCA Book Configuration
  //--------------------------------------------------------------------------
  t_MDC_ARCA_Info arcaInfo;
  memset(&arcaInfo, 0, sizeof(t_MDC_ARCA_Info));
  
  int posCounter = 0; //counter of position array of arcaInfo
  int result = SUCCESS;

  //Get Primary Interface
  result = GetValueFromKeyword(rawConfig, "dbl_nic", arcaInfo.NICName, &arcaInfo.positions[posCounter++]);
  if (result == ERROR)
  {
    TraceLog(WARN_LEVEL, "Could not get DBL NIC Name for ARCA Book\n");
    return ERROR;
  }
  
  //Get Backup Interface
  result = GetValueFromKeyword(rawConfig, "backup_nic", arcaInfo.backupNIC, &arcaInfo.positions[posCounter++]);
  if (result == ERROR)
  {
    TraceLog(WARN_LEVEL, "Could not get DBL Backup Interface name for ARCA Book\n");
    return ERROR;
  }
  
  //Get Source ID
  posCounter++;
  
  // Get Retransmission Request IP & Port
  posCounter++;
  
  int i;
  for (i = 0; i < MAX_ARCA_GROUPS; i++)
  {
    char primaryMulticast[80];
    char backupMulticast[80];
    
    sprintf(primaryMulticast, "group%d_primary_mc_address", i + 1);
    sprintf(backupMulticast, "group%d_backup_mc_address", i + 1);
    
    // Get Primary Multicast IP & Port
    if (GetIpAddressAndPortFromMdc(rawConfig, primaryMulticast, &arcaInfo.primaryMulticast[i], &arcaInfo.positions[posCounter++]) == ERROR)
    {
      TraceLog(WARN_LEVEL, "Could not get ARCA Book's Primary Multicast address for group %d\n", i + 1);
      return ERROR;
    }
    
    // Get Retransmission Multicast IP & Port
    posCounter++;
    
    // Get Refresh Multicast IP & Port
    if (GetIpAddressAndPortFromMdc(rawConfig, backupMulticast, &arcaInfo.backupMulticast[i], &arcaInfo.positions[posCounter++]) == ERROR)
    {
      arcaInfo.backupMulticast[i].ip[0] = 0;
    }
  }
  
  //--------------------------------------------------------------------------
  //Check validity of ARCA MDC information structure base on position list
  //--------------------------------------------------------------------------
  // Find keywords 'port='
  strcpy(arcaInfo.positions[posCounter].keyword, "port");
  arcaInfo.positions[posCounter].posKeyword = strstr(rawConfig, "port=");
  arcaInfo.positions[posCounter].posValue = arcaInfo.positions[posCounter].posKeyword;
  posCounter++;
  for (i = 0; i < MAX_MDC_ARCA_INFO_FIELD - 1; i++)
  {
    if (arcaInfo.positions[i].posKeyword != NULL &&
      arcaInfo.positions[i].posValue != NULL)
    {
      int j;
      for (j = i + 1; j < MAX_MDC_ARCA_INFO_FIELD; j++)
      {
        if (arcaInfo.positions[i].posKeyword == arcaInfo.positions[j].posKeyword ||
          arcaInfo.positions[i].posKeyword == arcaInfo.positions[j].posValue ||
          arcaInfo.positions[i].posValue == arcaInfo.positions[j].posKeyword ||
          arcaInfo.positions[i].posValue == arcaInfo.positions[j].posValue)
        {
          TraceLog(WARN_LEVEL, "Invalid ARCA MDC structure. Please check between keyword '%s' and '%s'\n",
              arcaInfo.positions[i].keyword, arcaInfo.positions[j].keyword);
          return ERROR;
        }
      }
    }
  }

  //--------------------------------------------------------------------------
  //Copy to ARCA Book Configuration
  //--------------------------------------------------------------------------
  TraceLog(DEBUG_LEVEL, "Parsed ARCA Book configuration from command line successfully\n");
  strncpy(ARCA_Book_Conf.NICName, arcaInfo.NICName, MAX_LINE_LEN);
  strncpy(ARCA_Book_Conf.backupNIC, arcaInfo.backupNIC, MAX_LINE_LEN);
  
  for (i = 0; i < MAX_ARCA_GROUPS; i++)
  {
    strncpy(ARCA_Book_Conf.group[i].dataIP, arcaInfo.primaryMulticast[i].ip, MAX_LINE_LEN);
    memcpy(&ARCA_Book_Conf.group[i].dataPort, &arcaInfo.primaryMulticast[i].port, 4);

    if (arcaInfo.backupMulticast[i].ip[0] != 0)
    {
      strncpy(ARCA_Book_Conf.group[i].backupIP, arcaInfo.backupMulticast[i].ip, MAX_LINE_LEN);
      memcpy(&ARCA_Book_Conf.group[i].backupPort, &arcaInfo.backupMulticast[i].port, 4);
    }
  }

  //--------------------------------------------------------------------------
  //Debug code
  //--------------------------------------------------------------------------
  TraceLog(DEBUG_LEVEL, "Arca primary interface: '%s'\n", ARCA_Book_Conf.NICName);
  TraceLog(DEBUG_LEVEL, "Arca backup interface: '%s'\n", ARCA_Book_Conf.backupNIC);

  for (i = 0; i < MAX_ARCA_GROUPS; i++)
  {
    TraceLog(DEBUG_LEVEL, "Arca group %d. Primary Data Feed Multicast: %s:%d\n", i+1, ARCA_Book_Conf.group[i].dataIP, ARCA_Book_Conf.group[i].dataPort);
    if (ARCA_Book_Conf.group[i].backupIP[0] != 0)
      TraceLog(DEBUG_LEVEL, "Arca group %d. Backup Data Feed Multicast: %s:%d\n", i+1, ARCA_Book_Conf.group[i].backupIP, ARCA_Book_Conf.group[i].backupPort);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ParseNasdaqMdcConfig
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ParseNasdaqMdcConfig(char *rawConfig, int ecnIndex, t_NASDAQ_Book_Conf *config)
{
  //--------------------------------------------------------------------------
  //Parse NASDAQ Book Configuration
  //--------------------------------------------------------------------------
  t_MDC_NASDAQ_Info nasdaqInfo;
  memset(&nasdaqInfo, 0, sizeof(t_MDC_NASDAQ_Info));
  
  int posCounter = 0; //counter of position array
  int result = SUCCESS;

  //Get Interface for Nasdaq Data feed (Multicast)
  result = GetValueFromKeyword(rawConfig, "dbl_nic_data", nasdaqInfo.DataInterface, &nasdaqInfo.positions[posCounter++]);
  if (result == ERROR)
  {
    TraceLog(WARN_LEVEL, "Could not get DBL Interface name for Nasdaq (ecn id %d)\n", ecnIndex);
    return ERROR;
  }
  
  //Get Interface for Nasdaq Recovery feed (Unicast)
  posCounter++;
  
  //Get Interface for backup Data feed (Multicast)
  result = GetValueFromKeyword(rawConfig, "backup_nic", nasdaqInfo.BackupInterface, &nasdaqInfo.positions[posCounter++]);
  if (result == ERROR)
  {
    TraceLog(WARN_LEVEL, "Could not get DBL Backup Interface name for Nasdaq (ecn id %d)\n", ecnIndex);
    return ERROR;
  }
  
  char primaryMulticast[80] = "group1_primary_mc_address";
  char backupMulticast[80] = "group1_backup_mc_address";
  
  // Get Primary Multicast IP & Port
  if (GetIpAddressAndPortFromMdc(rawConfig, primaryMulticast, &nasdaqInfo.primaryMulticast, &nasdaqInfo.positions[posCounter++]) == ERROR)
  {
    TraceLog(WARN_LEVEL, "Could not get Nasdaq (ecn id %d) Primary Multicast address\n", ecnIndex);
    return ERROR;
  }
  
  // Get Retransmission Multicast IP & Port
  posCounter++;
  
  // Get backup feed
  if (GetIpAddressAndPortFromMdc(rawConfig, backupMulticast, &nasdaqInfo.backupMulticast, &nasdaqInfo.positions[posCounter++]) == ERROR)
  {
    nasdaqInfo.backupMulticast.ip[0] = 0;
  }
  
  //--------------------------------------------------------------------------
  //Check validity of NASDAQ MDC information structure base on position list
  //--------------------------------------------------------------------------
  // Find keywords 'port='
  strcpy(nasdaqInfo.positions[posCounter].keyword, "port");
  nasdaqInfo.positions[posCounter].posKeyword = strstr(rawConfig, "port=");
  nasdaqInfo.positions[posCounter].posValue = nasdaqInfo.positions[posCounter].posKeyword;
  posCounter++;
  int i;
  for (i = 0; i < MAX_MDC_NASDAQ_INFO_FIELD - 1; i++)
  {
    if (nasdaqInfo.positions[i].posKeyword != NULL &&
        nasdaqInfo.positions[i].posValue != NULL)
    {
      int j;
      for (j = i + 1; j < MAX_MDC_NASDAQ_INFO_FIELD; j++)
      {
        if (nasdaqInfo.positions[i].posKeyword == nasdaqInfo.positions[j].posKeyword ||
          nasdaqInfo.positions[i].posKeyword == nasdaqInfo.positions[j].posValue ||
          nasdaqInfo.positions[i].posValue == nasdaqInfo.positions[j].posKeyword ||
          nasdaqInfo.positions[i].posValue == nasdaqInfo.positions[j].posValue)
        {
          TraceLog(WARN_LEVEL, "Invalid Nasdaq (ecn id %d) structure. Please check between keyword '%s' and '%s'\n",
                               ecnIndex, nasdaqInfo.positions[i].keyword, nasdaqInfo.positions[j].keyword);
          return ERROR;
        }
      }
    }
  }
  
  //--------------------------------------------------------------------------
  //Copy to NASDAQ Book Configuration
  //--------------------------------------------------------------------------
  TraceLog(DEBUG_LEVEL, "Parsed Nasdaq (ecn id %d) configuration from command line successfully\n", ecnIndex);
  strncpy(config->Data.NICName, nasdaqInfo.DataInterface, MAX_LINE_LEN);
  strncpy(config->Backup.NICName, nasdaqInfo.BackupInterface, MAX_LINE_LEN);
  
  strncpy(config->Data.ip, nasdaqInfo.primaryMulticast.ip, MAX_LINE_LEN);
  memcpy(&config->Data.port, &nasdaqInfo.primaryMulticast.port, 4);

  if (nasdaqInfo.backupMulticast.ip[0] != 0)
  {
    strncpy(config->Backup.ip, nasdaqInfo.backupMulticast.ip, MAX_LINE_LEN);
    memcpy(&config->Backup.port, &nasdaqInfo.backupMulticast.port, 4);
  }
  
  //--------------------------------------------------------------------------
  //Debug code
  //--------------------------------------------------------------------------
  TraceLog(DEBUG_LEVEL, "Nasdaq (ecn id %d) Primary Interface: '%s', Primary Multicast Address: %s:%d\n", ecnIndex, config->Data.NICName, config->Data.ip, config->Data.port);
  TraceLog(DEBUG_LEVEL, "Nasdaq (ecn id %d) Backup Interface: '%s'\n", ecnIndex, config->Backup.NICName);
  if (config->Backup.ip[0] != 0)
    TraceLog(DEBUG_LEVEL, "Nasdaq (ecn id %d) Backup feed: %s:%d\n", ecnIndex, config->Backup.ip, config->Backup.port);

  return SUCCESS;
}

/****************************************************************************
- Function name:  ParseNyseBookConfig
****************************************************************************/
int ParseNyseBookConfig(char *rawConfig, int ecnIndex)
{
  t_NYSE_Book_Conf *config;
  int maxGroup;
  char bookName[8];
  if (ecnIndex == BOOK_NYSE)
  {
    config = &NyseBookConf;
    maxGroup = MAX_NYSE_BOOK_GROUPS;
    strcpy(bookName, "NYSE");
  }
  else
  {
    config = &AmexBookConf;
    maxGroup = 1;
    strcpy(bookName, "AMEX");
  }
  //--------------------------------------------------------------------------
  //Parse NYSE Book Configuration
  //--------------------------------------------------------------------------
  t_MDC_NYSE_Info nyseInfo;
  memset(&nyseInfo, 0, sizeof(t_MDC_NYSE_Info));
  
  int posCounter = 0; //counter of position array
  int result = SUCCESS;

  //Get Primary Interface
  result = GetValueFromKeyword(rawConfig, "dbl_nic", nyseInfo.NICName, &nyseInfo.positions[posCounter++]);
  if (result == ERROR)
  {
    TraceLog(WARN_LEVEL, "Could not get DBL NIC Name for %s Book\n", bookName);
    return ERROR;
  }
  
  //Get backup Interface
  result = GetValueFromKeyword(rawConfig, "backup_nic", nyseInfo.backupNIC, &nyseInfo.positions[posCounter++]);
  if (result == ERROR)
  {
    TraceLog(WARN_LEVEL, "Could not get DBL Backup Interface for %s Book\n", bookName);
    return ERROR;
  }
  
  //Get Source ID
  posCounter++;
  
  int i;
  for (i = 0; i < maxGroup; i++)
  {
    char primaryMulticast[80];
    char backupMulticast[80];
    
    sprintf(primaryMulticast, "group%d_primary_mc_address", i + 1);
    sprintf(backupMulticast, "group%d_backup_mc_address", i + 1);
    
    // Get Retransmission Request IP & Port
    posCounter++;
    
    // Get Primary Multicast IP & Port
    if (GetIpAddressAndPortFromMdc(rawConfig, primaryMulticast, &nyseInfo.primaryMulticast[i], &nyseInfo.positions[posCounter++]) == ERROR)
    {
      TraceLog(WARN_LEVEL, "Could not get %s Book's Primary Multicast address for group %d\n", bookName, i + 1);
      return ERROR;
    }
    
    // Get backup Multicast IP & Port
    if (GetIpAddressAndPortFromMdc(rawConfig, backupMulticast, &nyseInfo.backupMulticast[i], &nyseInfo.positions[posCounter++]) == ERROR)
    {
      nyseInfo.backupMulticast[i].ip[0] = 0;
    }
    
    // Get Retransmission Multicast IP & Port
    posCounter++;
  }
  
  //--------------------------------------------------------------------------
  //Check validity of NYSE MDC information structure base on position list
  //--------------------------------------------------------------------------
  // Find keywords 'port='
  strcpy(nyseInfo.positions[posCounter].keyword, "port");
  nyseInfo.positions[posCounter].posKeyword = strstr(rawConfig, "port=");
  nyseInfo.positions[posCounter].posValue = nyseInfo.positions[posCounter].posKeyword;
  posCounter++;
  int maxFields = (3 + 4 * maxGroup + 1);
  for (i = 0; i < maxFields - 1; i++)
  {
    if (nyseInfo.positions[i].posKeyword != NULL &&
      nyseInfo.positions[i].posValue != NULL)
    {
      int j;
      for (j = i + 1; j < maxFields; j++)
      {
        if (nyseInfo.positions[i].posKeyword == nyseInfo.positions[j].posKeyword ||
            nyseInfo.positions[i].posKeyword == nyseInfo.positions[j].posValue ||
            nyseInfo.positions[i].posValue == nyseInfo.positions[j].posKeyword ||
            nyseInfo.positions[i].posValue == nyseInfo.positions[j].posValue)
        {
          TraceLog(WARN_LEVEL, "Invalid %s MDC structure. Please check between keyword '%s' and '%s'\n",
                      bookName, nyseInfo.positions[i].keyword, nyseInfo.positions[j].keyword);
          return ERROR;
        }
      }
    }
  }
  
  //--------------------------------------------------------------------------
  //Copy to NYSE Book Configuration
  //--------------------------------------------------------------------------
  TraceLog(DEBUG_LEVEL, "Parsed %s Book configuration from command line successfully\n", bookName);
  strncpy(config->NICName, nyseInfo.NICName, MAX_LINE_LEN);
  
  for (i = 0; i < maxGroup; i++)
  {
    strncpy(config->group[i].receiveDataIP, nyseInfo.primaryMulticast[i].ip, MAX_LINE_LEN);
    memcpy(&config->group[i].receiveDataPort, &nyseInfo.primaryMulticast[i].port, 4);

    if (nyseInfo.backupMulticast[i].ip[0] != 0)
    {
      strncpy(config->group[i].backupIP, nyseInfo.backupMulticast[i].ip, MAX_LINE_LEN);
      memcpy(&config->group[i].backupPort, &nyseInfo.backupMulticast[i].port, 4);
    }
  }
  
  //--------------------------------------------------------------------------
  //Debug code
  //--------------------------------------------------------------------------
  TraceLog(DEBUG_LEVEL, "%s Book: primary Interface: %s\n", bookName, config->NICName);
  TraceLog(DEBUG_LEVEL, "%s Book: backup Interface: %s\n", bookName, config->backupNIC);

  for (i = 0; i < maxGroup; i++)
  {
    TraceLog(DEBUG_LEVEL, "%s Book group %d. Primary Data Feed Multicast: %s:%d\n", bookName, i+1, config->group[i].receiveDataIP, config->group[i].receiveDataPort);
    if (config->group[i].backupIP[0] != 0)
    {
      TraceLog(DEBUG_LEVEL, "%s Book group %d. backup Data Feed Multicast: %s:%d\n", bookName, i+1, config->group[i].backupIP, config->group[i].backupPort);
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ParseBatszBookConfig
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ParseBatszBookConfig(char *rawConfig)
{
  //--------------------------------------------------------------------------
  //Parse BATS-Z Book Configuration
  //--------------------------------------------------------------------------
  t_MDC_BATSZ_Info batszInfo;
  memset(&batszInfo, 0, sizeof(t_MDC_BATSZ_Info));
  
  int posCounter = 0; //counter of position array
  int result = SUCCESS;

  //Get primary interface
  result = GetValueFromKeyword(rawConfig, "dbl_nic", batszInfo.NICName, &batszInfo.positions[posCounter++]);
  if (result == ERROR)
  {
    TraceLog(WARN_LEVEL, "Could not get DBL primary Interface for BATS-Z Book\n");
    return ERROR;
  }
  
  //Get backup interface
  result = GetValueFromKeyword(rawConfig, "backup_nic", batszInfo.backupNIC, &batszInfo.positions[posCounter++]);
  if (result == ERROR)
  {
    TraceLog(WARN_LEVEL, "Could not get DBL backup Interface for BATS-Z Book\n");
    return ERROR;
  }
  
  //Get Session ID
  posCounter++;
  
  //Get Username
  posCounter++;
    
  //Get Password
  posCounter++;
  
  // Get GRP IP & Port
  posCounter++;

  int i;
  for (i = 0; i < MAX_BATSZ_UNIT; i++)
  {
    char primaryMulticast[80];
    char backupMulticast[80];

    sprintf(primaryMulticast, "group%d_primary_mc_address", i + 1);
    sprintf(backupMulticast, "group%d_backup_mc_address", i + 1);

    // Get Primary Multicast IP & Port
    if (GetIpAddressAndPortFromMdc(rawConfig, primaryMulticast, &batszInfo.primaryMulticast[i], &batszInfo.positions[posCounter++]) == ERROR)
    {
      TraceLog(WARN_LEVEL, "Could not get BATS-Z Book's Primary Multicast address for unit %d\n", i + 1);
      return ERROR;
    }
    
    if (GetIpAddressAndPortFromMdc(rawConfig, backupMulticast, &batszInfo.backupMulticast[i], &batszInfo.positions[posCounter++]) == ERROR)
    {
      batszInfo.backupMulticast[i].ip[0] = 0;
    }

    // Get Retransmission Multicast IP & Port
    posCounter++;
  }
  
  //--------------------------------------------------------------------------
  //Check validity of BATSZ MDC information structure base on position list
  //--------------------------------------------------------------------------
  // Find keywords 'port='
  strcpy(batszInfo.positions[posCounter].keyword, "port");
  batszInfo.positions[posCounter].posKeyword = strstr(rawConfig, "port=");
  batszInfo.positions[posCounter].posValue = batszInfo.positions[posCounter].posKeyword;
  posCounter++;
  for (i = 0; i < MAX_MDC_BATSZ_INFO_FIELD - 1; i++)
  {
    if (batszInfo.positions[i].posKeyword != NULL &&
      batszInfo.positions[i].posValue != NULL)
    {
      int j;
      for (j = i + 1; j < MAX_MDC_BATSZ_INFO_FIELD; j++)
      {
        if (batszInfo.positions[i].posKeyword == batszInfo.positions[j].posKeyword ||
            batszInfo.positions[i].posKeyword == batszInfo.positions[j].posValue ||
            batszInfo.positions[i].posValue == batszInfo.positions[j].posKeyword ||
            batszInfo.positions[i].posValue == batszInfo.positions[j].posValue)
        {
          TraceLog(WARN_LEVEL, "Invalid BATS-Z MDC structure. Please check between keyword '%s' and '%s'\n",
                                batszInfo.positions[i].keyword, batszInfo.positions[j].keyword);
          return ERROR;
        }
      }
    }
  }

  //--------------------------------------------------------------------------
  //Copy to BATS-Z Book Configuration
  //--------------------------------------------------------------------------
  TraceLog(DEBUG_LEVEL, "Parsed BATSZ Book configuration from command line successfully\n");
  strncpy(BATSZ_Book_Conf.NICName, batszInfo.NICName, MAX_LINE_LEN);
  strncpy(BATSZ_Book_Conf.backupNIC, batszInfo.backupNIC, MAX_LINE_LEN);

  for (i = 0; i < MAX_BATSZ_UNIT; i++)
  {
    strncpy(BATSZ_Book_Conf.group[i].dataIP, batszInfo.primaryMulticast[i].ip, MAX_LINE_LEN);
    memcpy(&BATSZ_Book_Conf.group[i].dataPort, &batszInfo.primaryMulticast[i].port, 4);
    
    if (batszInfo.backupMulticast[i].ip[0] != 0)
    {
      strncpy(BATSZ_Book_Conf.group[i].backupIP, batszInfo.backupMulticast[i].ip, MAX_LINE_LEN);
      memcpy(&BATSZ_Book_Conf.group[i].backupPort, &batszInfo.backupMulticast[i].port, 4);
    }
  }

  //--------------------------------------------------------------------------
  //Debug code
  //--------------------------------------------------------------------------
  TraceLog(DEBUG_LEVEL, "Interface Name: %s\n", BATSZ_Book_Conf.NICName);
  TraceLog(DEBUG_LEVEL, "Backup Interface Name: %s\n", BATSZ_Book_Conf.backupNIC);
  
  for (i=0; i< MAX_BATSZ_UNIT; i++)
  {
    TraceLog(DEBUG_LEVEL, "BATS-Z Unit %d Primary Data: %s:%d\n", i+1, BATSZ_Book_Conf.group[i].dataIP, BATSZ_Book_Conf.group[i].dataPort);
    if (BATSZ_Book_Conf.group[i].backupIP[0] != 0)
    {
      TraceLog(DEBUG_LEVEL, "BATS-Z Unit %d Backup feed: %s:%d\n", i+1, BATSZ_Book_Conf.group[i].backupIP, BATSZ_Book_Conf.group[i].backupPort);
    }
  }

  return SUCCESS;
}


/****************************************************************************
- Function name:  ParseBYXBookConfig
****************************************************************************/
int ParseBYXBookConfig(char *rawConfig)
{
  //--------------------------------------------------------------------------
  //Parse BYX Book Configuration
  //--------------------------------------------------------------------------
  t_MDC_BYX_Info byxInfo;
  memset(&byxInfo, 0, sizeof(t_MDC_BYX_Info));
  
  int posCounter = 0; //counter of position array
  int result = SUCCESS;

  //Get primary interface
  result = GetValueFromKeyword(rawConfig, "dbl_nic", byxInfo.NICName, &byxInfo.positions[posCounter++]);
  if (result == ERROR)
  {
    TraceLog(WARN_LEVEL, "Could not get DBL primary Interface for BYX Book\n");
    return ERROR;
  }
  
  //Get backup interface
  result = GetValueFromKeyword(rawConfig, "backup_nic", byxInfo.backupNIC, &byxInfo.positions[posCounter++]);
  if (result == ERROR)
  {
    TraceLog(WARN_LEVEL, "Could not get DBL backup Interface for BYX Book\n");
    return ERROR;
  }
  
  //Get Session ID
  posCounter++;
  
  //Get Username
  posCounter++;
    
  //Get Password
  posCounter++;
  
  // Get GRP IP & Port
  posCounter++;

  int i;
  for (i = 0; i < MAX_BYX_UNIT; i++)
  {
    char primaryMulticast[80];
    char backupMulticast[80];

    sprintf(primaryMulticast, "group%d_primary_mc_address", i + 1);
    sprintf(backupMulticast, "group%d_backup_mc_address", i + 1);

    // Get Primary Multicast IP & Port
    if (GetIpAddressAndPortFromMdc(rawConfig, primaryMulticast, &byxInfo.primaryMulticast[i], &byxInfo.positions[posCounter++]) == ERROR)
    {
      TraceLog(WARN_LEVEL, "Could not get BYX Book's Primary Multicast address for unit %d\n", i + 1);
      return ERROR;
    }
    
    if (GetIpAddressAndPortFromMdc(rawConfig, backupMulticast, &byxInfo.backupMulticast[i], &byxInfo.positions[posCounter++]) == ERROR)
    {
      byxInfo.backupMulticast[i].ip[0] = 0;
    }

    // Get Retransmission Multicast IP & Port
    posCounter++;
  }
  
  //--------------------------------------------------------------------------
  //Check validity of BYX MDC information structure base on position list
  //--------------------------------------------------------------------------
  // Find keywords 'port='
  strcpy(byxInfo.positions[posCounter].keyword, "port");
  byxInfo.positions[posCounter].posKeyword = strstr(rawConfig, "port=");
  byxInfo.positions[posCounter].posValue = byxInfo.positions[posCounter].posKeyword;
  posCounter++;
  for (i = 0; i < MAX_MDC_BYX_INFO_FIELD - 1; i++)
  {
    if (byxInfo.positions[i].posKeyword != NULL &&
      byxInfo.positions[i].posValue != NULL)
    {
      int j;
      for (j = i + 1; j < MAX_MDC_BYX_INFO_FIELD; j++)
      {
        if (byxInfo.positions[i].posKeyword == byxInfo.positions[j].posKeyword ||
          byxInfo.positions[i].posKeyword == byxInfo.positions[j].posValue ||
          byxInfo.positions[i].posValue == byxInfo.positions[j].posKeyword ||
          byxInfo.positions[i].posValue == byxInfo.positions[j].posValue)
        {
          TraceLog(WARN_LEVEL, "Invalid BYX MDC structure. Please check between keyword '%s' and '%s'\n",
                                byxInfo.positions[i].keyword, byxInfo.positions[j].keyword);
          return ERROR;
        }
      }
    }
  }

  //--------------------------------------------------------------------------
  //Copy to BYX Book Configuration
  //--------------------------------------------------------------------------
  TraceLog(DEBUG_LEVEL, "Parsed BYX Book configuration from command line successfully\n");
  strncpy(BYX_Book_Conf.NICName, byxInfo.NICName, MAX_LINE_LEN);
  strncpy(BYX_Book_Conf.backupNIC, byxInfo.backupNIC, MAX_LINE_LEN);

  for (i = 0; i < MAX_BYX_UNIT; i++)
  {
    strncpy(BYX_Book_Conf.group[i].dataIP, byxInfo.primaryMulticast[i].ip, MAX_LINE_LEN);
    memcpy(&BYX_Book_Conf.group[i].dataPort, &byxInfo.primaryMulticast[i].port, 4);
    
    if (byxInfo.backupMulticast[i].ip[0] != 0)
    {
      strncpy(BYX_Book_Conf.group[i].backupIP, byxInfo.backupMulticast[i].ip, MAX_LINE_LEN);
      memcpy(&BYX_Book_Conf.group[i].backupPort, &byxInfo.backupMulticast[i].port, 4);
    }
  }

  //--------------------------------------------------------------------------
  //Debug code
  //--------------------------------------------------------------------------
  TraceLog(DEBUG_LEVEL, "Interface Name: %s\n", BYX_Book_Conf.NICName);
  TraceLog(DEBUG_LEVEL, "Backup Interface Name: %s\n", BYX_Book_Conf.backupNIC);
  
  for (i=0; i< MAX_BYX_UNIT; i++)
  {
    TraceLog(DEBUG_LEVEL, "BYX Unit %d Primary Data: %s:%d\n", i+1, BYX_Book_Conf.group[i].dataIP, BYX_Book_Conf.group[i].dataPort);
    if (BYX_Book_Conf.group[i].backupIP[0] != 0)
    {
      TraceLog(DEBUG_LEVEL, "BYX Unit %d Backup feed: %s:%d\n", i+1, BYX_Book_Conf.group[i].backupIP, BYX_Book_Conf.group[i].backupPort);
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ParseEdgxBookConfig
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ParseEdgxBookConfig(char *rawConfig)
{
  //--------------------------------------------------------------------------
  //Parse EDGX Book Configuration
  //--------------------------------------------------------------------------
  t_MDC_EDGX_Info edgxInfo;
  memset(&edgxInfo, 0, sizeof(t_MDC_EDGX_Info));
  
  int posCounter = 0; //counter of position array
  int result = SUCCESS;

  //Get NICname
  result = GetValueFromKeyword(rawConfig, "dbl_nic", edgxInfo.NICName, &edgxInfo.positions[posCounter++]);
  if (result == ERROR)
  {
    TraceLog(WARN_LEVEL, "Could not get DBL NIC Name for EDGX Book\n");
    return ERROR;
  }
  
  result = GetValueFromKeyword(rawConfig, "backup_nic", edgxInfo.backupNIC, &edgxInfo.positions[posCounter++]);
  if (result == ERROR)
  {
    TraceLog(WARN_LEVEL, "Could not get DBL Backup Interface for EDGX Book\n");
    return ERROR;
  }
  
  //Get Username
  posCounter++;
  
  //Get Password
  posCounter++;
  
  // Get Retransmission Request IP & Port
  posCounter++;

  int i;
  for (i = 0; i < MAX_EDGX_BOOK_GROUP; i++)
  {
    char primaryMulticast[80];
    char backupMulticast[80];

    sprintf(primaryMulticast, "group%d_primary_mc_address", i + 1);
    sprintf(backupMulticast, "group%d_backup_mc_address", i + 1);

    // Get Primary Multicast IP & Port
    if (GetIpAddressAndPortFromMdc(rawConfig, primaryMulticast, &edgxInfo.primaryMulticast[i], &edgxInfo.positions[posCounter++]) == ERROR)
    {
      TraceLog(WARN_LEVEL, "Could not get EDGX Book's Primary Multicast address for group %d\n", i + 1);
      return ERROR;
    }
    
    if (GetIpAddressAndPortFromMdc(rawConfig, backupMulticast, &edgxInfo.backupMulticast[i], &edgxInfo.positions[posCounter++]) == ERROR)
    {
      edgxInfo.backupMulticast[i].ip[0] = 0;
    }

    // Get Retransmission Multicast IP & Port
    posCounter++;
  }
  
  //--------------------------------------------------------------------------
  //Check validity of EDGX MDC information structure base on position list
  //--------------------------------------------------------------------------
  // Find keywords 'port='
  strcpy(edgxInfo.positions[posCounter].keyword, "port");
  edgxInfo.positions[posCounter].posKeyword = strstr(rawConfig, "port=");
  edgxInfo.positions[posCounter].posValue = edgxInfo.positions[posCounter].posKeyword;
  posCounter++;
  for (i = 0; i < MAX_MDC_EDGX_INFO_FIELD - 1; i++)
  {
    if (edgxInfo.positions[i].posKeyword != NULL &&
        edgxInfo.positions[i].posValue != NULL)
    {
      int j;
      for (j = i + 1; j < MAX_MDC_EDGX_INFO_FIELD; j++)
      {
        if (edgxInfo.positions[i].posKeyword == edgxInfo.positions[j].posKeyword ||
            edgxInfo.positions[i].posKeyword == edgxInfo.positions[j].posValue ||
            edgxInfo.positions[i].posValue == edgxInfo.positions[j].posKeyword ||
            edgxInfo.positions[i].posValue == edgxInfo.positions[j].posValue)
        {
          TraceLog(WARN_LEVEL, "Invalid EDGX MDC structure. Please check between keyword '%s' and '%s'\n",
                                edgxInfo.positions[i].keyword, edgxInfo.positions[j].keyword);
          return ERROR;
        }
      }
    }
  }

  //--------------------------------------------------------------------------
  //Copy to EDGX Book Configuration
  //--------------------------------------------------------------------------
  TraceLog(DEBUG_LEVEL, "Parsed EDGX Book configuration from command line successfully\n");
  strncpy(EDGX_BOOK_Conf.NICName, edgxInfo.NICName, MAX_LINE_LEN);
  strncpy(EDGX_BOOK_Conf.backupNIC, edgxInfo.backupNIC, MAX_LINE_LEN);

  for (i = 0; i < MAX_EDGX_BOOK_GROUP; i++)
  {
    strncpy(EDGX_BOOK_Conf.group[i].dataIP, edgxInfo.primaryMulticast[i].ip, MAX_LINE_LEN);
    memcpy(&EDGX_BOOK_Conf.group[i].dataPort, &edgxInfo.primaryMulticast[i].port, 4);
    
    if (edgxInfo.backupMulticast[i].ip[0] != 0)
    {
      strncpy(EDGX_BOOK_Conf.group[i].backupIP, edgxInfo.backupMulticast[i].ip, MAX_LINE_LEN);
      memcpy(&EDGX_BOOK_Conf.group[i].backupPort, &edgxInfo.backupMulticast[i].port, 4);
    }
  }

  //--------------------------------------------------------------------------
  //Debug code
  //--------------------------------------------------------------------------
  TraceLog(DEBUG_LEVEL, "Primary Interface: %s\n", EDGX_BOOK_Conf.NICName);
  TraceLog(DEBUG_LEVEL, "Backup Interface: %s\n", EDGX_BOOK_Conf.backupNIC);
  
  for (i=0; i < MAX_EDGX_BOOK_GROUP; i++)
  {
    TraceLog(DEBUG_LEVEL, "EDGX Group %d Primary Data: %s:%d\n", i+1, EDGX_BOOK_Conf.group[i].dataIP, EDGX_BOOK_Conf.group[i].dataPort);
    if (EDGX_BOOK_Conf.group[i].backupIP[0] != 0)
    {
      TraceLog(DEBUG_LEVEL, "EDGX Group %d Backup Feed: %s:%d\n", i+1, EDGX_BOOK_Conf.group[i].backupIP, EDGX_BOOK_Conf.group[i].backupPort);
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ParseEdgaBookConfig
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ParseEdgaBookConfig(char *rawConfig)
{
  //--------------------------------------------------------------------------
  //Parse EDGA Book Configuration
  //--------------------------------------------------------------------------
  t_MDC_EDGA_Info edgaInfo;
  memset(&edgaInfo, 0, sizeof(t_MDC_EDGA_Info));
  
  int posCounter = 0; //counter of position array
  int result = SUCCESS;

  //Get NICname
  result = GetValueFromKeyword(rawConfig, "dbl_nic", edgaInfo.NICName, &edgaInfo.positions[posCounter++]);
  if (result == ERROR)
  {
    TraceLog(WARN_LEVEL, "Could not get DBL NIC Name for EDGA Book\n");
    return ERROR;
  }
  
  result = GetValueFromKeyword(rawConfig, "backup_nic", edgaInfo.backupNIC, &edgaInfo.positions[posCounter++]);
  if (result == ERROR)
  {
    TraceLog(WARN_LEVEL, "Could not get DBL Backup Interface for EDGA Book\n");
    return ERROR;
  }
  
  //Get Username
  posCounter++;
  
  //Get Password
  posCounter++;
  
  // Get Retransmission Request IP & Port
  posCounter++;

  int i;
  for (i = 0; i < MAX_EDGA_BOOK_GROUP; i++)
  {
    char primaryMulticast[80];
    char backupMulticast[80];

    sprintf(primaryMulticast, "group%d_primary_mc_address", i + 1);
    sprintf(backupMulticast, "group%d_backup_mc_address", i + 1);

    // Get Primary Multicast IP & Port
    if (GetIpAddressAndPortFromMdc(rawConfig, primaryMulticast, &edgaInfo.primaryMulticast[i], &edgaInfo.positions[posCounter++]) == ERROR)
    {
      TraceLog(WARN_LEVEL, "Could not get EDGA Book's Primary Multicast address for group %d\n", i + 1);
      return ERROR;
    }
    
    if (GetIpAddressAndPortFromMdc(rawConfig, backupMulticast, &edgaInfo.backupMulticast[i], &edgaInfo.positions[posCounter++]) == ERROR)
    {
      edgaInfo.backupMulticast[i].ip[0] = 0;
    }

    // Get Retransmission Multicast IP & Port
    posCounter++;
  }
  
  //--------------------------------------------------------------------------
  //Check validity of EDGA MDC information structure base on position list
  //--------------------------------------------------------------------------
  // Find keywords 'port='
  strcpy(edgaInfo.positions[posCounter].keyword, "port");
  edgaInfo.positions[posCounter].posKeyword = strstr(rawConfig, "port=");
  edgaInfo.positions[posCounter].posValue = edgaInfo.positions[posCounter].posKeyword;
  posCounter++;
  for (i = 0; i < MAX_MDC_EDGA_INFO_FIELD - 1; i++)
  {
    if (edgaInfo.positions[i].posKeyword != NULL &&
      edgaInfo.positions[i].posValue != NULL)
    {
      int j;
      for (j = i + 1; j < MAX_MDC_EDGA_INFO_FIELD; j++)
      {
        if (edgaInfo.positions[i].posKeyword == edgaInfo.positions[j].posKeyword ||
          edgaInfo.positions[i].posKeyword == edgaInfo.positions[j].posValue ||
          edgaInfo.positions[i].posValue == edgaInfo.positions[j].posKeyword ||
          edgaInfo.positions[i].posValue == edgaInfo.positions[j].posValue)
        {
          TraceLog(WARN_LEVEL, "Invalid EDGA MDC structure. Please check between keyword '%s' and '%s'\n",
              edgaInfo.positions[i].keyword, edgaInfo.positions[j].keyword);
          return ERROR;
        }
      }
    }
  }

  //--------------------------------------------------------------------------
  //Copy to EDGA Book Configuration
  //--------------------------------------------------------------------------
  TraceLog(DEBUG_LEVEL, "Parsed EDGA Book configuration from command line successfully\n");
  strncpy(EDGA_BOOK_Conf.NICName, edgaInfo.NICName, MAX_LINE_LEN);
  strncpy(EDGA_BOOK_Conf.backupNIC, edgaInfo.backupNIC, MAX_LINE_LEN);

  for (i = 0; i < MAX_EDGA_BOOK_GROUP; i++)
  {
    strncpy(EDGA_BOOK_Conf.group[i].dataIP, edgaInfo.primaryMulticast[i].ip, MAX_LINE_LEN);
    memcpy(&EDGA_BOOK_Conf.group[i].dataPort, &edgaInfo.primaryMulticast[i].port, 4);
    
    if (edgaInfo.backupMulticast[i].ip[0] != 0)
    {
      strncpy(EDGA_BOOK_Conf.group[i].backupIP, edgaInfo.backupMulticast[i].ip, MAX_LINE_LEN);
      memcpy(&EDGA_BOOK_Conf.group[i].backupPort, &edgaInfo.backupMulticast[i].port, 4);
    }
  }

  //--------------------------------------------------------------------------
  //Debug code
  //--------------------------------------------------------------------------
  TraceLog(DEBUG_LEVEL, "Primary Interface: %s\n", EDGA_BOOK_Conf.NICName);
  TraceLog(DEBUG_LEVEL, "Backup Interface: %s\n", EDGA_BOOK_Conf.backupNIC);
  
  for (i=0; i < MAX_EDGA_BOOK_GROUP; i++)
  {
    TraceLog(DEBUG_LEVEL, "EDGA Group %d Primary Data: %s:%d\n", i+1, EDGA_BOOK_Conf.group[i].dataIP, EDGA_BOOK_Conf.group[i].dataPort);
    if (EDGA_BOOK_Conf.group[i].backupIP[0] != 0)
    {
      TraceLog(DEBUG_LEVEL, "EDGA Group %d Backup Feed: %s:%d\n", i+1, EDGA_BOOK_Conf.group[i].backupIP, EDGA_BOOK_Conf.group[i].backupPort);
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ParseCqsFeedConfig
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ParseCqsFeedConfig(char *rawConfig)
{
  //--------------------------------------------------------------------------
  //Parse CQS Feed Configuration
  //--------------------------------------------------------------------------
  t_MDC_CQS_Info cqsInfo;
  memset(&cqsInfo, 0, sizeof(t_MDC_CQS_Info));
  
  int posCounter = 0; //counter of position array
  int result = SUCCESS;

  //Get NICname
  result = GetValueFromKeyword(rawConfig, "dbl_nic", cqsInfo.NICName, &cqsInfo.positions[posCounter++]);
  if (result == ERROR)
  {
    TraceLog(WARN_LEVEL, "Could not get DBL NIC Name for CQS Feed\n");
    return ERROR;
  }

  int i;
  const int maxEMulticastGroup = MAX_CQS_MULTICAST_LINES / 2;
  for (i = 0; i < maxEMulticastGroup; i++)
  {
    char eMulticast[80];
    char fMulticast[80];

    sprintf(eMulticast, "group%d_e_mc_address", i + 1);
    sprintf(fMulticast, "group%d_f_mc_address", i + 1);

    // Get E Multicast IP & Port
    if (GetIpAddressAndPortFromMdc(rawConfig, eMulticast, &cqsInfo.eMulticast[i], &cqsInfo.positions[posCounter++]) == ERROR)
    {
      TraceLog(WARN_LEVEL, "Could not get CQS's E Multicast %d address\n", i + 1);
      return ERROR;
    }

    // Get F Multicast IP & Port
    if (GetIpAddressAndPortFromMdc(rawConfig, fMulticast, &cqsInfo.fMulticast[i], &cqsInfo.positions[posCounter++]) == ERROR)
    {
      TraceLog(WARN_LEVEL, "Could not get CQS's F Multicast %d address\n", i + 1);
      return ERROR;
    }
  }
  
  //--------------------------------------------------------------------------
  //Check validity of CQS MDC information structure base on position list
  //--------------------------------------------------------------------------
  // Find keywords 'port='
  strcpy(cqsInfo.positions[posCounter].keyword, "port");
  cqsInfo.positions[posCounter].posKeyword = strstr(rawConfig, "port=");
  cqsInfo.positions[posCounter].posValue = cqsInfo.positions[posCounter].posKeyword;
  posCounter++;
  for (i = 0; i < MAX_MDC_CQS_INFO_FIELD - 1; i++)
  {
    if (cqsInfo.positions[i].posKeyword != NULL &&
      cqsInfo.positions[i].posValue != NULL)
    {
      int j;
      for (j = i + 1; j < MAX_MDC_CQS_INFO_FIELD; j++)
      {
        if (cqsInfo.positions[i].posKeyword == cqsInfo.positions[j].posKeyword ||
            cqsInfo.positions[i].posKeyword == cqsInfo.positions[j].posValue ||
            cqsInfo.positions[i].posValue == cqsInfo.positions[j].posKeyword ||
            cqsInfo.positions[i].posValue == cqsInfo.positions[j].posValue)
        {
          TraceLog(WARN_LEVEL, "Invalid CQS structure. Please check between keyword '%s' and '%s'\n",
                                cqsInfo.positions[i].keyword, cqsInfo.positions[j].keyword);
          return ERROR;
        }
      }
    }
  }

  //--------------------------------------------------------------------------
  //Copy to CQS Feed Configuration
  //--------------------------------------------------------------------------
  strncpy(CQS_Conf.NICName, cqsInfo.NICName, MAX_LINE_LEN);
  
  //E multicast line from 0  --> 11
  //F multicast line from 12 --> 23
  for (i = 0; i < maxEMulticastGroup; i++)
  {
    strncpy(CQS_Conf.group[i].ip, cqsInfo.eMulticast[i].ip, MAX_LINE_LEN);
    memcpy(&CQS_Conf.group[i].port, &cqsInfo.eMulticast[i].port, 4);
  }

  for (i = maxEMulticastGroup; i < MAX_CQS_MULTICAST_LINES; i++)
  {
    strncpy(CQS_Conf.group[i].ip, cqsInfo.fMulticast[i - maxEMulticastGroup].ip, MAX_LINE_LEN);
    memcpy(&CQS_Conf.group[i].port, &cqsInfo.fMulticast[i - maxEMulticastGroup].port, 4);
  }

  //--------------------------------------------------------------------------
  //Debug code
  //--------------------------------------------------------------------------
  TraceLog(DEBUG_LEVEL, "Parsed CQS Feed configuration from command line successfully\n");
  TraceLog(DEBUG_LEVEL, "Interface Name: '%s'\n", CQS_Conf.NICName);
  for (i = 0; i < maxEMulticastGroup; i++)
  {
    TraceLog(DEBUG_LEVEL, "E Multicast %2d Address: %s:%d\n", i+1, CQS_Conf.group[i].ip, CQS_Conf.group[i].port);
  }

  for (i = maxEMulticastGroup; i < MAX_CQS_MULTICAST_LINES; i++)
  {
    TraceLog(DEBUG_LEVEL, "F Multicast %2d Address: %s:%d\n", i+1 - maxEMulticastGroup, CQS_Conf.group[i].ip, CQS_Conf.group[i].port);
  }
  return SUCCESS;
}

/****************************************************************************
- Function name:  ParseUqdfFeedConfig
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ParseUqdfFeedConfig(char *rawConfig)
{
  //--------------------------------------------------------------------------
  //Parse UQDF Feed Configuration
  //--------------------------------------------------------------------------
  t_MDC_UQDF_Info uqdfInfo;
  memset(&uqdfInfo, 0, sizeof(t_MDC_UQDF_Info));
  
  int posCounter = 0; //counter of position array
  int result = SUCCESS;

  //Get NICname
  result = GetValueFromKeyword(rawConfig, "dbl_nic", uqdfInfo.NICName, &uqdfInfo.positions[posCounter++]);
  if (result == ERROR)
  {
    TraceLog(WARN_LEVEL, "Could not get DBL NIC Name for UQDF Feed\n");
    return ERROR;
  }

  int i;
  for (i = 0; i < MAX_UQDF_CHANNELS; i++)
  {
    char primaryMulticast[80];

    sprintf(primaryMulticast, "group%d_primary_mc_address", i + 1);

    // Get Primary Multicast IP & Port
    if (GetIpAddressAndPortFromMdc(rawConfig, primaryMulticast, &uqdfInfo.primaryMulticast[i], &uqdfInfo.positions[posCounter++]) == ERROR)
    {
      TraceLog(WARN_LEVEL, "Could not get UQDF's Multicast %d address\n", i + 1);
      return ERROR;
    }
  }
  
  //--------------------------------------------------------------------------
  //Check validity of UQDF MDC information structure base on position list
  //--------------------------------------------------------------------------
  // Find keywords 'port='
  strcpy(uqdfInfo.positions[posCounter].keyword, "port");
  uqdfInfo.positions[posCounter].posKeyword = strstr(rawConfig, "port=");
  uqdfInfo.positions[posCounter].posValue = uqdfInfo.positions[posCounter].posKeyword;
  posCounter++;
  for (i = 0; i < MAX_MDC_UQDF_INFO_FIELD - 1; i++)
  {
    if (uqdfInfo.positions[i].posKeyword != NULL &&
      uqdfInfo.positions[i].posValue != NULL)
    {
      int j;
      for (j = i + 1; j < MAX_MDC_UQDF_INFO_FIELD; j++)
      {
        if (uqdfInfo.positions[i].posKeyword == uqdfInfo.positions[j].posKeyword ||
            uqdfInfo.positions[i].posKeyword == uqdfInfo.positions[j].posValue ||
            uqdfInfo.positions[i].posValue == uqdfInfo.positions[j].posKeyword ||
            uqdfInfo.positions[i].posValue == uqdfInfo.positions[j].posValue)
        {
          TraceLog(WARN_LEVEL, "Invalid UQDF structure. Please check between keyword '%s' and '%s'\n",
                                uqdfInfo.positions[i].keyword, uqdfInfo.positions[j].keyword);
          return ERROR;
        }
      }
    }
  }

  //--------------------------------------------------------------------------
  //Copy to UQDF Feed Configuration
  //--------------------------------------------------------------------------
  strncpy(UQDF_Conf.NICName, uqdfInfo.NICName, MAX_LINE_LEN);
  for (i = 0; i < MAX_UQDF_CHANNELS; i++)
  {
    strncpy(UQDF_Conf.group[i].ip, uqdfInfo.primaryMulticast[i].ip, MAX_LINE_LEN);
    memcpy(&UQDF_Conf.group[i].port, &uqdfInfo.primaryMulticast[i].port, 4);
  }

  //--------------------------------------------------------------------------
  //Debug code
  //--------------------------------------------------------------------------
  TraceLog(DEBUG_LEVEL, "Parsed UQDF Feed configuration from command line successfully\n");
  TraceLog(DEBUG_LEVEL, "Interface Name: '%s'\n", UQDF_Conf.NICName);
  for (i = 0; i < MAX_UQDF_CHANNELS; i++)
  {
    TraceLog(DEBUG_LEVEL, "Primary Multicast %d Address: %s:%d\n", i+1, UQDF_Conf.group[i].ip, UQDF_Conf.group[i].port);
  }
  return SUCCESS;
}

/****************************************************************************
- Function name:  IsValidParameter
- Input:          + parameterStr: MDC/OEC string is required to check
                  + keywords: keyword appears in parameterStr
- Output:     
- Return:         SUCCESS or ERROR
- Description:    
- Usage:          Check valid input parameter for MDC or OEC information
****************************************************************************/
int IsValidParameter(const char *parameterStr, const char *keywords)
{
  //Check valid input parameter for MDC or OEC information
  //valid parameter:   grp_address=10.132.180.12:30099
  //invalid parameter: grp_address a=10.132.180.12:30099
  //parameterStr must include keyword: eg; parameterStr="grp_address=10.132.180.12:30099"
  
  parameterStr += strlen(keywords);
  
  if (*parameterStr != '=' &&
      *parameterStr != ' ' &&
      *parameterStr != CHAR_CODE_NEW_LINE &&
      *parameterStr != CHAR_CODE_CARRIAGE_RETURN)
  {
    TraceLog(WARN_LEVEL, "Character right after parameter '%s' is invalid, character code (%d)\n", keywords, *parameterStr);
    return ERROR;
  }
  
  if (strchr(parameterStr, '=') == NULL)
  {
    // Invalid Parameter
    return ERROR;
  }
  
  int isFoundBlank = NO;
  int len = strlen(parameterStr);
  
  int i;
  for (i = 0; i < len; i++)
  {
    if (parameterStr[i] == USED_CHAR_CODE || //blank
        parameterStr[i] == CHAR_CODE_NEW_LINE || 
        parameterStr[i] == CHAR_CODE_CARRIAGE_RETURN)
    {
      //check current character is blank/New Line or not
      isFoundBlank = YES;
    }
    else if (parameterStr[i] == '=')
    {
      return SUCCESS;
    }
    else if (isprint(parameterStr[i])) 
    {
      //printable character, including ' ' and '=' but they are checked first
      if (isFoundBlank == YES)
      {
        return ERROR;
      }
    }
  }
  
  return ERROR;
}

/****************************************************************************
- Function name:  ProcessASConfigInfo
- Input:      
- Output:     
- Return:     SUCCESS or ERROR
- Description:    
- Usage:      
****************************************************************************/
int ProcessASConfigInfo(char *optarg)
{
  if (CheckValidNumberString(optarg) == ERROR)
  {
    TraceLog(WARN_LEVEL, "Invalid AS port: '%s'\n", optarg);
    return ERROR;
  }
  
  int port = atoi(optarg);
  if (CheckValidPort(port) == ERROR)
  {
    TraceLog(WARN_LEVEL, "Invalid AS port: %d\n", port);
    return ERROR;
  }
  
  ASConfig.port = port;
  ASConfig.maxConnections = 1;
  
  TraceLog(DEBUG_LEVEL, "Parsed AS configuration from command line successfully\n");
  TraceLog(DEBUG_LEVEL, "Port to listen AS: %d\n", ASConfig.port);
  TraceLog(DEBUG_LEVEL, "Max connections: %d\n", ASConfig.maxConnections);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTradingAccountInfo
- Input:      
- Output:     
- Return:     SUCCESS or ERROR
- Description:    
- Usage:      
****************************************************************************/
int ProcessTradingAccountInfo(char *optarg)
{
  int len = strlen(optarg);
  if (len < 2)
  {
    TraceLog(WARN_LEVEL, "Trading account '%s' is too short\n", optarg);
    return ERROR;
  }
  else if (len > 31)
  {
    TraceLog(WARN_LEVEL, "Trading account '%s' is too long\n", optarg);
    return ERROR;
  }
  
  t_TradingAccount tmpAccount;
  memset(&tmpAccount, 0, sizeof(t_TradingAccount));
  tmpAccount.accountType = -1; //if accountType = -1, TS will get account type from AS
  
  strncpy(tmpAccount.ArcaDirectAccount, optarg, 31);
  strncpy(tmpAccount.NyseCCGAccount, optarg, 31);
  strncpy(tmpAccount.BATSZBOEAccount, optarg, 31);
  strncpy(tmpAccount.BYXBOEAccount, optarg, 31);
  
  int offset = len - 2;
  tmpAccount.OUCHAccount[0] = optarg[offset];
  tmpAccount.OUCHAccount[1] = optarg[offset+1];
  tmpAccount.OUCHAccount[2] = 0;
  
  tmpAccount.RASHAccount[0] = optarg[offset];
  tmpAccount.RASHAccount[1] = optarg[offset+1];
  tmpAccount.RASHAccount[2] = 0;
  
  tmpAccount.EDGXAccount[0] = optarg[offset];
  tmpAccount.EDGXAccount[1] = optarg[offset+1];
  tmpAccount.EDGXAccount[2] = 0;
  
  tmpAccount.EDGAAccount[0] = optarg[offset];
  tmpAccount.EDGAAccount[1] = optarg[offset+1];
  tmpAccount.EDGAAccount[2] = 0;
  
  tmpAccount.BXAccount[0] = optarg[offset];
  tmpAccount.BXAccount[1] = optarg[offset+1];
  tmpAccount.BXAccount[2] = 0;
  
  tmpAccount.PSXAccount[0] = optarg[offset];
  tmpAccount.PSXAccount[1] = optarg[offset+1];
  tmpAccount.PSXAccount[2] = 0;
  
  memcpy(&TradingAccount, &tmpAccount, sizeof(t_TradingAccount));
  
  TraceLog(DEBUG_LEVEL, "Parsed Trading Account configuration from command line successfully\n");
  TraceLog(DEBUG_LEVEL, "Initialize Trading Account type: %d\n", TradingAccount.accountType);
  TraceLog(DEBUG_LEVEL, "Trading account for Arca Direct: '%s'\n", TradingAccount.ArcaDirectAccount);
  TraceLog(DEBUG_LEVEL, "Trading account for NYSE CCG: '%s'\n", TradingAccount.NyseCCGAccount);
  TraceLog(DEBUG_LEVEL, "Trading account for OUCH (just token): '%s'\n", TradingAccount.OUCHAccount);
  TraceLog(DEBUG_LEVEL, "Trading account for RASH (just token): '%s'\n", TradingAccount.RASHAccount);
  TraceLog(DEBUG_LEVEL, "Trading account for BATSZ BOE: '%s'\n", TradingAccount.BATSZBOEAccount);
  TraceLog(DEBUG_LEVEL, "Trading account for BYX BOE: '%s'\n", TradingAccount.BYXBOEAccount);
  TraceLog(DEBUG_LEVEL, "Trading account for EDGX (just token): '%s'\n", TradingAccount.EDGXAccount);
  TraceLog(DEBUG_LEVEL, "Trading account for EDGA (just token): '%s'\n", TradingAccount.EDGAAccount);
  TraceLog(DEBUG_LEVEL, "Trading account for NASDAQ BX (just token): '%s'\n", TradingAccount.BXAccount);
  TraceLog(DEBUG_LEVEL, "Trading account for NASDAQ PSX (just token): '%s'\n", TradingAccount.PSXAccount);
  
  return SUCCESS;
}

static inline int GetAddressFromConfig(char *line, char *ip, int *port)
{
  char *_brk;
  _brk = strchr(line, ':');
  if (_brk == NULL)
  {
    return ERROR;
  }
          
  *_brk = 0;
  _brk++;
  strcpy(ip, line);
  *port = atoi(_brk);
  
  return CheckValidIpv4Address(ip);
}
