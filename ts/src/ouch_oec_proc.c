#define _ISOC9X_SOURCE
#define _GNU_SOURCE

#include "ouch_oec_proc.h"
#include "trading_mgmt.h"
#include "kernel_algorithm.h"
#include "network.h"
#include "logging.h"
#include "order_mgmt.h"

/****************************************************************************
          GLOBAL VARIABLES
****************************************************************************/

/****************************************************************************
- Function name:  Init
****************************************************************************/
static int Init(t_OecConfig *venueConfig)
{
  venueConfig->socket = -1;
  return SUCCESS;
}

/****************************************************************************
- Function name:  Connect_Ouch
****************************************************************************/
int Connect_Ouch(int oecType)
{
  t_OecConfig *venueConfig = &OecConfig[oecType];
  
  // Init Data Structure
  Init(venueConfig);
  
  // Load incoming sequence number from file
  char *fn = NULL;
  switch (oecType)
  {
    case ORDER_NASDAQ_OUCH:
      fn = FN_SEQUENCE_NUM_OUCH;
      break;
    case ORDER_NASDAQ_BX:
      fn = FN_SEQUENCE_NUM_BX;
      break;
    case ORDER_NASDAQ_PSX:
      fn = FN_SEQUENCE_NUM_PSX;
      break;
    default:
      return ERROR;
  }
  
  if (LoadSequenceNumber(fn, oecType) == ERROR)
  {
    OrderStatusMgmt[oecType].shouldConnect = NO;
    return ERROR;
  }
  
  TraceLog(DEBUG_LEVEL, "Connecting to %s Server (%s:%d)\n", GetOrderNameByIndex(oecType), venueConfig->ipAddress, venueConfig->port);

  // Connect to server       
  venueConfig->socket = connectSession(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, venueConfig->sessionId);
  
  if (venueConfig->socket == -1)
  {
    TraceLog(ERROR_LEVEL, "Could not connect to %s Server\n", GetOrderNameByIndex(oecType));
    OrderStatusMgmt[oecType].shouldConnect = NO;
    return ERROR;
  }
  
  if(venueConfig->protocol == PROTOCOL_TCP)
  {
    if (SetSocketRecvTimeout(venueConfig->socket, 5) == ERROR)
    {
      TraceLog(ERROR_LEVEL, "Could not set socket receive-timeout for %s socket\n", GetOrderNameByIndex(oecType));
      disconnectSession(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, venueConfig->sessionId);
      OrderStatusMgmt[oecType].shouldConnect = NO;
      return ERROR;
    }
    
    if (SetSocketSndTimeout(venueConfig->socket, 1, 0) == ERROR)
    {
      TraceLog(ERROR_LEVEL, "Could not set socket send-timeout for %s socket\n", GetOrderNameByIndex(oecType));
      disconnectSession(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, venueConfig->sessionId);
      OrderStatusMgmt[oecType].shouldConnect = NO;
      return ERROR;
    }
    
    /* Disable the Nagle (TCP No Delay) algorithm */
    if (DisableNagleAlgo(venueConfig->socket) == -1)
    {
      TraceLog(ERROR_LEVEL, "Could not set TCP_NODELAY for %s socket\n", GetOrderNameByIndex(oecType));
      disconnectSession(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, venueConfig->sessionId);
      OrderStatusMgmt[oecType].shouldConnect = NO;
      return ERROR;
    }
  
    if (BuildAndSend_SoupbinTcp_LoginMessage(oecType) == ERROR)
    {
      TraceLog(ERROR_LEVEL, "%s: Could not send Login msg to server\n", GetOrderNameByIndex(oecType));
      OrderStatusMgmt[oecType].shouldConnect = NO;
      return ERROR;
    }
  }
  else // UFO
  {
    if (BuildAndSend_UFO_LoginMessage(oecType) == ERROR)
    {
      TraceLog(ERROR_LEVEL, "%s: Could not send Login msg to server\n", GetOrderNameByIndex(oecType));
      OrderStatusMgmt[oecType].shouldConnect = NO;
      return ERROR;
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessOuchMessages
****************************************************************************/
int ProcessOuchMessages(int oecType, uint32_t symbolId, const unsigned char *buf, const uint64_t arrivalTime)
{
  if (OecConfig[oecType].protocol == PROTOCOL_TCP)
  {
    return ProcessOuchSoupbinMessages(oecType, symbolId, buf, arrivalTime);
  }
  else
  {
    return ProcessOuchUfoMessages(oecType, symbolId, buf, arrivalTime);
  }
}

/****************************************************************************
- Function name:  ProcessOuchSoupbinMessages
****************************************************************************/
int ProcessOuchSoupbinMessages(int oecType, uint32_t symbolId, const unsigned char *buf, const uint64_t arrivalTime)
{
  t_OecConfig *venueConfig = &OecConfig[oecType];
  char messageType = buf[2];

  switch(messageType)
  {
    case ____________SEQUENCED_MSG:
      __sync_add_and_fetch(&venueConfig->incomingSeqNum, 1);
      return ProcessOuchSoupbin_SequencedMessages(oecType, symbolId, buf+3, arrivalTime);
      
    case ____________LOGIN_ACCEPTED:
    {
      TraceLog(DEBUG_LEVEL, "%s: Received Login Accepted\n", GetOrderNameByIndex(oecType));
      TraceLog(DEBUG_LEVEL, "\t Session'%.10s'\n", &buf[3]);
      TraceLog(DEBUG_LEVEL, "\t Sequence Number '%.20s'\n", &buf[13]);
      
      char seq[21] = "\0";
      memcpy(seq, &buf[13], 20);
      venueConfig->incomingSeqNum = atol(seq);
      
      OrderStatusMgmt[oecType].isConnected = CONNECTED;
      return SOUPBIN_TCP_LOGIN_ACCEPTED_LEN;  
    }
    
    case ____________LOGIN_REJECTED:
      TraceLog(DEBUG_LEVEL, "%s: Received Logon Rejected\n", GetOrderNameByIndex(oecType));
      TraceLog(DEBUG_LEVEL, "\t Reject Reason Code = %c\n", buf[3]);
      
      if(buf[3] == 'A')
        TraceLog(DEBUG_LEVEL, "\t Not Authorized. There was an invalid username and password combination in the Login Request Message.\n");
      else if(buf[3] == 'S')
        TraceLog(DEBUG_LEVEL, "\t Session not available. The Requested Session in the Login Request Packet was either invalid or not available.\n");
      
      disconnectSession(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, venueConfig->sessionId);
      return SOUPBIN_TCP_LOGIN_REJECTED_LEN;
      
    case SOUPBIN_TCP_SERVER_HEARTBEAT:
      BuildAndSend_SoupbinTcp_Heartbeat(venueConfig);
      return SOUPBIN_TCP_HEARTBEAT_LEN;
      break;
      
    case SOUPBIN_TCP_END_OF_SESSION:
      TraceLog(DEBUG_LEVEL, "%s: Received End Of Session message\n", GetOrderNameByIndex(oecType));
      disconnectSession(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, venueConfig->sessionId);
      return SOUPBIN_END_OF_SESSION_LEN;
      
    case SOUPBIN_TCP_DEBUG_MSG:
    {
      int len = ushortAt(buf, 0) + 2;
      TraceLog(DEBUG_LEVEL,"%s: Received debug packet, length = %d\n", GetOrderNameByIndex(oecType), len);
      return len;
    }
    
    default:
      TraceLog(ERROR_LEVEL, "%s: Received Unknown message type: %c\n", GetOrderNameByIndex(oecType), messageType);

      // we have to disconnect since we do not know the message size
      disconnectSession(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, venueConfig->sessionId);
      return 0;
  }
}

/****************************************************************************
- Function name:  ProcessOuchUfoMessages
****************************************************************************/
int ProcessOuchUfoMessages(int oecType, uint32_t symbolId, const unsigned char *buf, const uint64_t arrivalTime)
{
  t_OecConfig *venueConfig = &OecConfig[oecType];
  
  char messageType = *buf;

  switch(messageType)
  {
    case ____________SEQUENCED_MSG:
      return ProcessOuchUfo_SequencedMessages(oecType, symbolId, buf+7, arrivalTime);
      
    case ____________LOGIN_ACCEPTED:
      TraceLog(DEBUG_LEVEL, "%s: Received Login Accepted\n", GetOrderNameByIndex(oecType));
      OrderStatusMgmt[oecType].isConnected = CONNECTED;
      return UFO_LOGIN_ACCEPTED_LEN;
      
    case ____________LOGIN_REJECTED:
      TraceLog(DEBUG_LEVEL, "%s: Received Logon Rejected\n", GetOrderNameByIndex(oecType));
      disconnectSession(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, venueConfig->sessionId);
      return UFO_LOGIN_REJECTED_LEN;
      
    case UFO_END_OF_SESSION:
      TraceLog(DEBUG_LEVEL, "%s: Received End Of Session message\n", GetOrderNameByIndex(oecType));
      disconnectSession(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, venueConfig->sessionId);
      return UFO_END_OF_SESSION_LEN; 
      
    default:
      TraceLog(ERROR_LEVEL, "%s: Received Unknown message type: %c\n", GetOrderNameByIndex(oecType), messageType);

      // we have to disconnect since we do not know the message size
      disconnectSession(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, venueConfig->sessionId);
      return 0;
  }
}


/****************************************************************************
- Function name:  ProcessOuchSoupbin_SequencedMessages
****************************************************************************/
int ProcessOuchSoupbin_SequencedMessages(int oecType, uint32_t symbolId, const unsigned char *buf, const uint64_t arrivalTime)
{
  char messageType = *buf;

  switch(messageType)
  {
    case OUCH_ORDER_ACCEPTED:
      ProcessOuch_OrderAcceptedMsg(oecType, symbolId, buf, arrivalTime);
      return OUCH_ORDER_ACCEPTED_LEN + 3;
      
    case OUCH_ORDER_EXECUTED:
      ProcessOuch_OrderExecutedMsg(oecType, symbolId, buf, arrivalTime);
      return OUCH_EXECUTED_LEN + 3;
      
    case OUCH_ORDER_CANCELED:
      ProcessOuch_OrderCanceledMsg(oecType, symbolId, buf, arrivalTime);
      return OUCH_CANCELED_LEN + 3;
    
    case OUCH_ORDER_REJECTED:
      ProcessOuch_OrderRejectedMsg(oecType, symbolId, buf, arrivalTime);
      return OUCH_REJECTED_LEN + 3;
    
    case OUCH_BROKEN_TRADE:
      {
        t_DataBlock dataBlock;
        memcpy(&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME], &arrivalTime, sizeof(uint64_t));
        AddDataBlockTime(&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]);
        dataBlock.msgLen = OUCH_BROKEN_TRADE_LEN + 1;
        dataBlock.msgContent[0] = 'S';
        memcpy(&dataBlock.msgContent[1], buf, OUCH_BROKEN_TRADE_LEN);
      
        Add_Ouch_DataBlockToCollection(oecType, &dataBlock);
      }
      return OUCH_BROKEN_TRADE_LEN + 3;
    
    case OUCH_PRICE_CORRECTION:
      {
        t_DataBlock dataBlock;
        memcpy(&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME], &arrivalTime, sizeof(uint64_t));
        AddDataBlockTime(&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]);
        dataBlock.msgLen = OUCH_PRICE_CORRECTION_LEN + 1;
        dataBlock.msgContent[0] = 'S';
        memcpy(&dataBlock.msgContent[1], buf, OUCH_PRICE_CORRECTION_LEN);
          
        Add_Ouch_DataBlockToCollection(oecType, &dataBlock);
      }
      return OUCH_PRICE_CORRECTION_LEN + 3;
      
    case OUCH_CANCEL_PENDING:
      {
        t_DataBlock dataBlock;
        memcpy(&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME], &arrivalTime, sizeof(uint64_t));
        AddDataBlockTime(&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]);
        dataBlock.msgLen = OUCH_CANCEL_PENDING_LEN + 1;
        dataBlock.msgContent[0] = 'S';
        memcpy(&dataBlock.msgContent[1], buf, OUCH_CANCEL_PENDING_LEN);

        Add_Ouch_DataBlockToCollection(oecType, &dataBlock);
      }
      return OUCH_CANCEL_PENDING_LEN + 3;
      
    case OUCH_CANCEL_REJECT:
      {
        t_DataBlock dataBlock;
        memcpy(&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME], &arrivalTime, sizeof(uint64_t));
        AddDataBlockTime(&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]);
        dataBlock.msgLen = OUCH_CANCEL_REJECT_LEN + 1;
        dataBlock.msgContent[0] = 'S';
        memcpy(&dataBlock.msgContent[1], buf, OUCH_CANCEL_REJECT_LEN);
        
        Add_Ouch_DataBlockToCollection(oecType, &dataBlock);
      }
      return OUCH_CANCEL_REJECT_LEN + 3;
      
    case OUCH_SYSTEM_EVENT:
      if (*(buf+9) == 'S')
      {
        TraceLog(DEBUG_LEVEL, "Received 'Start of Day' message\n");
      }
      return OUCH_SYSTEM_EVENT_LEN + 3;
      
    default:
      TraceLog(FATAL_LEVEL, "%s: Received unknown message (%c, ASCII: %d)\n", GetOrderNameByIndex(oecType), messageType, messageType);
      return 0;    
  }
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessOuchUfo_SequencedMessages
****************************************************************************/
int ProcessOuchUfo_SequencedMessages(int oecType, uint32_t symbolId, const unsigned char *buf, const uint64_t arrivalTime)
{
  char messageType = *buf;

  switch(messageType)
  {
    case OUCH_ORDER_ACCEPTED:
      ProcessOuch_OrderAcceptedMsg(oecType, symbolId, buf, arrivalTime);
      return OUCH_ORDER_ACCEPTED_LEN + 7;
      
    case OUCH_ORDER_EXECUTED:
      ProcessOuch_OrderExecutedMsg(oecType, symbolId, buf, arrivalTime);
      return OUCH_EXECUTED_LEN + 7;
      
    case OUCH_ORDER_CANCELED:
      ProcessOuch_OrderCanceledMsg(oecType, symbolId, buf, arrivalTime);
      return OUCH_CANCELED_LEN + 7;
    
    case OUCH_ORDER_REJECTED:
      ProcessOuch_OrderRejectedMsg(oecType, symbolId, buf, arrivalTime);
      return OUCH_REJECTED_LEN + 7;
    
    case OUCH_BROKEN_TRADE:
      {
        t_DataBlock dataBlock;
        memcpy(&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME], &arrivalTime, sizeof(uint64_t));
        AddDataBlockTime(&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]);
        dataBlock.msgLen = OUCH_BROKEN_TRADE_LEN + 1;
        dataBlock.msgContent[0] = 'S';
        memcpy(&dataBlock.msgContent[1], buf, OUCH_BROKEN_TRADE_LEN);
      
        Add_Ouch_DataBlockToCollection(oecType, &dataBlock);
      }
      return OUCH_BROKEN_TRADE_LEN + 7;
    
    case OUCH_PRICE_CORRECTION:
      {
        t_DataBlock dataBlock;
        memcpy(&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME], &arrivalTime, sizeof(uint64_t));
        AddDataBlockTime(&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]);
        dataBlock.msgLen = OUCH_PRICE_CORRECTION_LEN + 1;
        dataBlock.msgContent[0] = 'S';
        memcpy(&dataBlock.msgContent[1], buf, OUCH_PRICE_CORRECTION_LEN);
          
        Add_Ouch_DataBlockToCollection(oecType, &dataBlock);
      }
      return OUCH_PRICE_CORRECTION_LEN + 7;
      
    case OUCH_CANCEL_PENDING:
      {
        t_DataBlock dataBlock;
        memcpy(&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME], &arrivalTime, sizeof(uint64_t));
        AddDataBlockTime(&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]);
        dataBlock.msgLen = OUCH_CANCEL_PENDING_LEN + 1;
        dataBlock.msgContent[0] = 'S';
        memcpy(&dataBlock.msgContent[1], buf, OUCH_CANCEL_PENDING_LEN);

        Add_Ouch_DataBlockToCollection(oecType, &dataBlock);
      }
      return OUCH_CANCEL_PENDING_LEN + 7;
      
    case OUCH_CANCEL_REJECT:
      {
        t_DataBlock dataBlock;
        memcpy(&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME], &arrivalTime, sizeof(uint64_t));
        AddDataBlockTime(&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]);
        dataBlock.msgLen = OUCH_CANCEL_REJECT_LEN + 1;
        dataBlock.msgContent[0] = 'S';
        memcpy(&dataBlock.msgContent[1], buf, OUCH_CANCEL_REJECT_LEN);
        
        Add_Ouch_DataBlockToCollection(oecType, &dataBlock);
      }
      return OUCH_CANCEL_REJECT_LEN + 7;
      
    case OUCH_SYSTEM_EVENT:
      if (*(buf+9) == 'S')
      {
        TraceLog(DEBUG_LEVEL, "%s: Received 'Start of Day' message\n", GetOrderNameByIndex(oecType));
      }
      return OUCH_SYSTEM_EVENT_LEN + 7;
      
    default:
      TraceLog(FATAL_LEVEL, "%s: Received unknown message (%c, ASCII: %d)\n", GetOrderNameByIndex(oecType), messageType, messageType);
      return 0;    
  }
  
  return 0;
}

/****************************************************************************
- Function name:  ProcessOuch_OrderAcceptedMsg
****************************************************************************/
int ProcessOuch_OrderAcceptedMsg(int oecType, uint32_t symbolId, const unsigned char *msg, const uint64_t arrivalTime)
{
  t_DataBlock dataBlock;

  // Set timestamp for current rawdata
  memcpy(&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME], &arrivalTime, sizeof(uint64_t));
  AddDataBlockTime(&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]);
  
  int msgLen = OUCH_ORDER_ACCEPTED_LEN;
  
  if(_consumerId == -1)
  {
    // This could come from recovery 
    dataBlock.msgLen = msgLen + 1;
    dataBlock.msgContent[0] = 'S';
    memcpy(&dataBlock.msgContent[1], msg, msgLen);
    Add_Ouch_DataBlockToCollection(oecType, &dataBlock);
    
    TraceLog(DEBUG_LEVEL, "%s: Calling %s() from Admin Thread\n", GetOrderNameByIndex(oecType), __func__);
    return SUCCESS;
  }
  
  char tempBuffer[16];
  
  // Check order state is dead ('D')
  if (msg[64] == 'D')
  {
    t_ExecutionReport status;
    
    status.ecnID = oecType;
    status.stockSymbolIndex = symbolId;
    
    // Client Order ID (Token) (ignore first 2 chars: account)
    memcpy(tempBuffer, &msg[11], 12);
    tempBuffer[12] = 0;
    status.clientOrderID = atoi(tempBuffer);
    status.canceledShare = __builtin_bswap32(*(unsigned int*)&msg[24]);
    status.time = arrivalTime;
    
    ProcessCancelResponse(&status);
    
    *(unsigned short *)&dataBlock.addContent[DATABLOCK_OFFSET_ENTRY_EXIT_REF] = status.entryExitRefNum;

    DEVLOGF("Received Atomic Cancel from %s for order id: %d, cancelled shares: %d\n", GetOrderNameByIndex(oecType), status.clientOrderID, status.canceledShare);
  }
  else  // Order state = LIVE
  {
    t_ExecutionReport status;
    status.stockSymbolIndex = symbolId;
    
    // Client Order ID (Token) (ignore first 2 chars: account)
    memcpy(tempBuffer, &msg[11], 12);
    tempBuffer[12] = 0;
    status.clientOrderID = atoi(tempBuffer);
    
    // Accepted share
    status.acceptedShare = __builtin_bswap32(*(unsigned int*)&msg[24]);
    status.acceptedPrice = __builtin_bswap32(*(unsigned int*)&msg[36]) * 0.0001;
    status.time = arrivalTime;
    
    ProcessAcceptResponse(&status);
    
    DEVLOGF("Received Order Ack from %s\n", GetOrderNameByIndex(oecType));
  }
  
  dataBlock.msgLen = msgLen + 1;
  dataBlock.msgContent[0] = 'S';
  memcpy(&dataBlock.msgContent[1], msg, msgLen);
  
  Add_Ouch_DataBlockToCollection(oecType, &dataBlock);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessOuch_OrderExecutedMsg
****************************************************************************/
int ProcessOuch_OrderExecutedMsg(int oecType, uint32_t symbolId, const unsigned char *msg, const uint64_t arrivalTime)
{
  t_DataBlock dataBlock;
  
  // Set timestamp for current rawdata
  memcpy(&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME], &arrivalTime, sizeof(uint64_t));
  AddDataBlockTime(&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]);
  
  if(_consumerId == -1)
  {
    // This could come from recovery 
    dataBlock.msgLen = OUCH_EXECUTED_LEN + 1;
    dataBlock.msgContent[0] = 'S';
    memcpy(&dataBlock.msgContent[1], msg, OUCH_EXECUTED_LEN);
    Add_Ouch_DataBlockToCollection(oecType, &dataBlock);
  
    TraceLog(DEBUG_LEVEL, "Calling %s() from Admin Thread\n", __func__);
    return SUCCESS;
  }
    
  t_ExecutionReport status;
  status.ecnID = oecType;
  status.stockSymbolIndex = symbolId;
  
  // Client Order ID (Token, ignore first 2 chars, account)
  char tempBuffer[16];
  memcpy(tempBuffer, &msg[11], 12);
  tempBuffer[12] = 0;
  status.clientOrderID = atoi(tempBuffer);
    
  // Filled share
  status.filledShare  = __builtin_bswap32(*(unsigned int*)&msg[23]);
  
  // Filled price
  status.filledPrice = __builtin_bswap32(*(unsigned int*)&msg[27]) * 0.0001;
      
  // Process Partially Filled status
  status.time = arrivalTime;
  
  ProcessFillResponse(&status);
  
  dataBlock.msgLen = OUCH_EXECUTED_LEN + 1;
  dataBlock.msgContent[0] = 'S';
  memcpy(&dataBlock.msgContent[1], msg, OUCH_EXECUTED_LEN);
  *(unsigned short *)&dataBlock.addContent[DATABLOCK_OFFSET_ENTRY_EXIT_REF] = status.entryExitRefNum;
  Add_Ouch_DataBlockToCollection(oecType, &dataBlock);
  
  DEVLOGF("Received Fill from %s, %d shares at $%lf\n", GetOrderNameByIndex(oecType), status.filledShare, status.filledPrice);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessOuch_OrderCanceledMsg
****************************************************************************/
int ProcessOuch_OrderCanceledMsg(int oecType, uint32_t symbolId, const unsigned char *msg, const uint64_t arrivalTime)
{
  t_DataBlock dataBlock;
  
  // Set timestamp for current rawdata
  memcpy(&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME], &arrivalTime, sizeof(uint64_t));
  AddDataBlockTime(&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]);
  
  if(_consumerId == -1)
  {
    // This could come from recovery 
    dataBlock.msgLen = OUCH_CANCELED_LEN + 1;
    dataBlock.msgContent[0] = 'S';
    memcpy(&dataBlock.msgContent[1], msg, OUCH_CANCELED_LEN);
    Add_Ouch_DataBlockToCollection(oecType, &dataBlock);
  
    TraceLog(DEBUG_LEVEL, "%s: Calling %s() from Admin Thread\n", GetOrderNameByIndex(oecType), __func__);
    return SUCCESS;
  }
  
  t_ExecutionReport status;
  status.ecnID = oecType;
  status.stockSymbolIndex = symbolId;
  
  // Client Order ID (Token, ignore first 2 char: account)
  char tempBuffer[16];
  memcpy(tempBuffer, &msg[11], 12);
  tempBuffer[12] = 0;
  status.clientOrderID = atoi(tempBuffer);
  
  // Canceled share
  status.canceledShare  = __builtin_bswap32(*(unsigned int*)&msg[23]);
      
  // Arrival time
  status.time = arrivalTime;
  
  ProcessCancelResponse(&status);
  
  *(unsigned short *)&dataBlock.addContent[DATABLOCK_OFFSET_ENTRY_EXIT_REF] = status.entryExitRefNum;
  dataBlock.msgLen = OUCH_CANCELED_LEN + 1;
  dataBlock.msgContent[0] = 'S';
  memcpy(&dataBlock.msgContent[1], msg, OUCH_CANCELED_LEN);
  
  Add_Ouch_DataBlockToCollection(oecType, &dataBlock);
  
  DEVLOGF("Received Cancel from %s for order id: %d, cancelled shares: %d\n", GetOrderNameByIndex(oecType), status.clientOrderID, status.canceledShare);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessOuch_OrderRejectedMsg
****************************************************************************/
int ProcessOuch_OrderRejectedMsg(int oecType, uint32_t symbolId, const unsigned char *msg, const uint64_t arrivalTime)
{
  t_DataBlock dataBlock;
  
  // Set timestamp for current rawdata
  memcpy(&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME], &arrivalTime, sizeof(uint64_t));
  AddDataBlockTime(&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]);
  
  if(_consumerId == -1)
  {
    // This could come from recovery 
    dataBlock.msgLen = OUCH_REJECTED_LEN + 1;
    dataBlock.msgContent[0] = 'S';
    memcpy(&dataBlock.msgContent[1], msg, OUCH_REJECTED_LEN);
    Add_Ouch_DataBlockToCollection(oecType, &dataBlock);
    
    TraceLog(DEBUG_LEVEL, "%s: Calling %s() from Admin Thread\n", GetOrderNameByIndex(oecType), __func__);
    return SUCCESS;
  }
  
  t_ExecutionReport status;
  status.ecnID = oecType;
  status.stockSymbolIndex = symbolId;
  
  // Client Order ID (Token, ignore first 2 char: account)
  char tempBuffer[16];
  memcpy(tempBuffer, &msg[11], 12);
  tempBuffer[12] = 0;
  status.clientOrderID = atoi(tempBuffer);
  status.time = arrivalTime;              
  
  ProcessRejectResponse(&status);

  *(unsigned short *)&dataBlock.addContent[DATABLOCK_OFFSET_ENTRY_EXIT_REF] = status.entryExitRefNum;
  dataBlock.msgLen = OUCH_REJECTED_LEN + 1;
  dataBlock.msgContent[0] = 'S';
  memcpy(&dataBlock.msgContent[1], msg, OUCH_REJECTED_LEN);
  
  Add_Ouch_DataBlockToCollection(oecType, &dataBlock);
  
  //Check to halt symbol
  switch (dataBlock.msgContent[24])
  {
    case 'H': // 'Halt', we will halt on all venues
      {
        UpdateHaltStatusForASymbol(symbolId, TRADING_HALTED, arrivalTime / TEN_RAISE_TO_9, MAX_ECN_BOOK);
      }
      break;
    case 'X': // 'Invalid price', we will halt on NASDAQ only
      {
        UpdateHaltStatusForASymbol(symbolId, TRADING_HALTED, arrivalTime / TEN_RAISE_TO_9, OrderToBookMapping[oecType]);
      }
      break;
  }
  
  DEVLOGF("Received Reject from %s for order id: %d, reason: %c\n", GetOrderNameByIndex(oecType), status.clientOrderID, dataBlock.msgContent[24]);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  BuildAndSend_SoupbinTcp_LoginMessage
****************************************************************************/
int BuildAndSend_SoupbinTcp_LoginMessage(int oecType)
{
  t_OecConfig *venueConfig = &OecConfig[oecType];
  char message[SOUPBIN_LOGIN_REQUEST_LEN];
  int len = 0;
  
  /*
  Both fields username and password are insensitive, 
  and must be filled with spaces on the right
  */
  memset(message, ' ', SOUPBIN_LOGIN_REQUEST_LEN);
  
  // Packet Length
  t_ShortConverter converter;
  converter.value = SOUPBIN_LOGIN_REQUEST_LEN - 2;
  message[0] = converter.c[1];
  message[1] = converter.c[0];
  
  // First character is the message type. 'L' means login request
  message[2] = 'L';
  
  /*
  Next is the 6 bytes which store the username
  We assume the length of the username is OK
  */
  len = strlen(venueConfig->userName);
  memcpy(&message[3], venueConfig->userName, len);
  
  /*
  Next is the 10 bytes which store the password
  We assume the length of the password is OK
  */
  len = strlen(venueConfig->password);
  memcpy(&message[9], venueConfig->password, len);

  // Requested sessions 10 bytes
  memset(&message[19], ' ', 10);
  
  // Requested Sequence Number(Left padding)
  __iToStrWithLeftPad(venueConfig->incomingSeqNum, ' ', 20, &message[29]);
  
  TraceLog(DEBUG_LEVEL, "%s: Send logon message to ouch fd: %d\n", GetOrderNameByIndex(oecType), venueConfig->socket);
  
  return SocketSend(venueConfig->socket, message, SOUPBIN_LOGIN_REQUEST_LEN);
}

/****************************************************************************
- Function name:  BuildAndSend_UFO_LoginMessage
****************************************************************************/
int BuildAndSend_UFO_LoginMessage(int oecType)
{
  t_OecConfig *venueConfig = &OecConfig[oecType];
  char message[29];
  int len = 0;
  
  /*
  Both fields username and password are insensitive, 
  and must be filled with spaces on the right
  */
  memset(message, ' ', 29);
  
  // Len
  t_ShortConverter converter;
  converter.value = 27;
  message[0] = converter.c[1];
  message[1] = converter.c[0];
  
  // msg type
  message[2] = 'L';
  
  /*
  Next is the 6 bytes which store the username
  We assume the length of the username is OK
  */
  len = strlen(venueConfig->userName);
  memcpy(&message[3], venueConfig->userName, len);
  
  /*
  Next is the 10 bytes which store the password
  We assume the length of the password is OK
  */
  len = strlen(venueConfig->password);
  memcpy(&message[9], venueConfig->password, len);

  // Requested sessions 10 bytes
  memset(&message[19], ' ', 10);
  
  TraceLog(DEBUG_LEVEL, "%s: Send logon message to ouch fd: %d\n", GetOrderNameByIndex(oecType), venueConfig->socket);
  
  return SocketSend(venueConfig->socket, message, 29);
}

/****************************************************************************
- Function name:  BuildAndSend_Ouch_OrderMsg
****************************************************************************/
int BuildAndSend_Ouch_OrderMsg(int oecType, t_OrderPlacementInfo *newOrder)
{
  t_OecConfig *venueConfig = &OecConfig[oecType];
  
  t_ShortConverter converter;
  char *msgContent = newOrder->dataBlock.msgContent;
  t_IntConverter iConverter;
  
  /* Note:
    NASDAQ is currently using OUCH UDP 4.1,  OUCH TCP 4.2
    PSX, BX are using UDP 4.2
    So there are differences
  */
  
  int length = OUCH_NEW_ORDER_MSG_LEN;
      
  converter.value = length - 2;
  msgContent[0] = converter.c[1];
  msgContent[1] = converter.c[0];
  
  // Packet Type
  msgContent[2] = 'U';
  
  // Order message type
  msgContent[3] = 'O';
  
  // Order token (2 digit account + orderID --> 14 char)
  switch (oecType)
  {
    case ORDER_NASDAQ_OUCH:
      msgContent[4] = TradingAccount.OUCHAccount[0];
      msgContent[5] = TradingAccount.OUCHAccount[1];
      break;
    case ORDER_NASDAQ_BX:
      msgContent[4] = TradingAccount.BXAccount[0];
      msgContent[5] = TradingAccount.BXAccount[1];
      break;
    case ORDER_NASDAQ_PSX:
      msgContent[4] = TradingAccount.PSXAccount[0];
      msgContent[5] = TradingAccount.PSXAccount[1];
      break;
  }
  Lrc_itoafl(newOrder->orderID, 32, 12, &msgContent[6]);
  
  // Buy/Sell indicator
  msgContent[18] = newOrder->side;
  
  // Share
  iConverter.value = newOrder->shareVolume;
  msgContent[19] = iConverter.c[3];
  msgContent[20] = iConverter.c[2];
  msgContent[21] = iConverter.c[1];
  msgContent[22] = iConverter.c[0];
  
  // Stock
  strcncpy((unsigned char *)&msgContent[23], newOrder->symbol, 32, SYMBOL_LEN);
  
  // Price
  if (fge(newOrder->sharePrice, 1.00))
  {
    if (newOrder->side == 'B')  //BUY SIDE
    {
      iConverter.value = (int)(newOrder->sharePrice * 100.00 + 0.500001) * 100;
    }
    else
    {
      iConverter.value = (int)(newOrder->sharePrice * 100.00 + 0.000001) * 100;
    }
  }
  else
  {
    iConverter.value = (int)(newOrder->sharePrice * 10000.00 + 0.500001);
  }
  
  msgContent[31] = iConverter.c[3];
  msgContent[32] = iConverter.c[2];
  msgContent[33] = iConverter.c[1];
  msgContent[34] = iConverter.c[0];
  
  // Time in Force: IOC (0)
  msgContent[35] = 0;
  msgContent[36] = 0;
  msgContent[37] = 0;
  msgContent[38] = 0;
  
  // Firm
  switch (oecType)
  {
    case ORDER_NASDAQ_OUCH:
      memcpy(&msgContent[39], MPID_NASDAQ_OUCH, 4);
      break;
    case ORDER_NASDAQ_BX:
      memcpy(&msgContent[39], MPID_NASDAQ_BX, 4);
      break;
    case ORDER_NASDAQ_PSX:
      memcpy(&msgContent[39], MPID_NASDAQ_PSX, 4);
      break;
  }

  // Display
  msgContent[43] = 'Y';
  
  // Capacity
  msgContent[44] = 'P';
  
  // Intermarket sweep eligibility
  if (newOrder->ISO_Flag == YES)
  {
    msgContent[45] = 'Y';
  }
  else
  {
    msgContent[45] = 'N';
  }
  
  msgContent[46] = 0;
  msgContent[47] = 0;
  msgContent[48] = 0;
  msgContent[49] = 0;

  // Cross Type
  msgContent[50] = 'N';
  
  // Customer Type: This field is optional, we will not add it
  //msgContent[51] = ' ';
  
  if (OrderBucketConsume() == ERROR)
  {
    TraceLog(WARN_LEVEL, "%s order is not allowed: Order bucket is currently empty\n", GetOrderNameByIndex(oecType));
    return ERROR;
  }
  
  // Send Enter Order Message
  long time = AsyncSend(venueConfig->sessionId, msgContent, length);
  if (time > 0)
  {
    // Set timestamp for current rawdata
    *((long*)&newOrder->dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]) = time;
    newOrder->ecnToPlaceID = oecType;
    
    DEVLOGF("Sent %c order to %s with id: %d for %s, %d @ $%lf\n", newOrder->side, GetOrderNameByIndex(oecType), newOrder->orderID, newOrder->symbol, newOrder->shareVolume, newOrder->sharePrice);
    
    return SUCCESS;
  }
  else
  {
    return ERROR;
  }
}

/****************************************************************************
- Function name:  BuildAndSend_SoupbinTcp_Heartbeat
****************************************************************************/
int BuildAndSend_SoupbinTcp_Heartbeat(t_OecConfig *venueConfig)
{
  char message[3];
  
  // Message Length
  t_ShortConverter converter;
  converter.value = 1;
  message[0] = converter.c[1];
  message[1] = converter.c[0];
  
  // Packet Type
  message[2] = 'R';

  return SocketSend(venueConfig->socket, message, 3);
}

/****************************************************************************
- Function name:  BuildAndSend_Ouch_LogoffMessage
****************************************************************************/
int BuildAndSend_Ouch_LogoffMessage(int oecType)
{
  t_OecConfig *venueConfig = &OecConfig[oecType];
  
  char message[3];
  
  // Message Length
  t_ShortConverter converter;
  converter.value = 1;
  message[0] = converter.c[1];
  message[1] = converter.c[0];
  
  // Packet Type
  message[2] = 'O';
  
  TraceLog(DEBUG_LEVEL, "Send logoff message to %s\n", GetOrderNameByIndex(oecType));
  
  return SocketSend(venueConfig->socket, message, 3);
}

/****************************************************************************
- Function name:  Add_Ouch_DataBlockToCollection
****************************************************************************/
void Add_Ouch_DataBlockToCollection(int oecType, t_DataBlock *dataBlock)
{
  dataBlock->blockLen = 12 + DATABLOCK_ADDITIONAL_INFO_LEN + dataBlock->msgLen;
  AddDataBlockToQueue(oecType, dataBlock);
}
