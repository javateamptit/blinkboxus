#include "configuration.h"
#include "dbl_filter_wrapper.h"
#include "network.h"

int SocketSend(int socket, const char *message, int msgLen)
{         
  ssize_t bytesSent = 0;
  while(bytesSent < msgLen) {
    bytesSent = send(socket, message, msgLen, MSG_NOSIGNAL);
    if(bytesSent < 0) {
      if(errno != EAGAIN) {
        printf("error: error sending to socket: %d\n", socket);
        perror("error: could not socket send oe data");
        return ERROR;
      } else {
         printf("debug: could not socket send data, trying again, sent bytes: %zu\n", bytesSent);
      }
    } else if(bytesSent < msgLen) {
      printf("debug: could not socket send data, trying again, sent bytes: %zu\n", bytesSent);
    }
  }
             
  return SUCCESS;
}


long AsyncSend(int sessionId, const char *message, int msgLen)
{                
  if(_consumerId < 0) { 
    TraceLog(ERROR_LEVEL, "attempting to send async from non-worker thread for session id: %d\n", sessionId);
    return -1;
  } else {
    int64_t now = hbitime_micros() * 1000L;
    spsc_write_handle writer = FilterBuffers[_consumerId].orderEntrySendWriter;
    waitForSpscWritableItemsAvailable(writer, msgLen + 12);
    
    uint8_t *write_ptr = spscWritePtr(writer);  
    *((int64_t*)write_ptr) = now;
    *((uint16_t*)(write_ptr+8)) = (uint16_t)sessionId;
    *((uint16_t*)(write_ptr+10)) = (uint16_t)msgLen;   
    memcpy(write_ptr + 12, message, msgLen);           
    
    spscCommitItemsWritten(writer, msgLen + 12);  
                                                         
    return now; 
  }
}
