/****************************************************************************
** Project name:  Equity Arbitrage Application
** File name:     trade_server.c
** Description:   This file contains function definitions that were declared
          in trade_server.h

** Author:      Sang Nguyen-Minh
** First created on 13 September 2007
** Last updated on 13 September 2007
****************************************************************************/

#define _GNU_SOURCE
#include <getopt.h>
#include <stdlib.h>
#include <unistd.h>
#include <execinfo.h>
#include <sys/mman.h>

#include "trade_server.h"
#include "bats_book_proc.h"

#include "arca_book_proc.h"
#include "nyse_book_proc.h"
#include "nasdaq_book_proc.h"
#include "edge_book_proc.h"
#include "quote_mgmt.h"
#include "cqs_proc.h"
#include "uqdf_proc.h"
#include "trading_mgmt.h"
#include "kernel_algorithm.h"
#include "stock_symbol.h"
#include "environment_proc.h"
#include "hbitime.h"
#include "logging.h"
#include "tuiapi.h"
#include "topic.h"

/****************************************************************************
            GLOBAL VARIABLES
****************************************************************************/
#define MAX_THREAD_TO_START 50

t_MonitorArcaBook MonitorArcaBook;
t_MonitorNasdaqBook MonitorNasdaqBook;
t_MonitorNasdaqBook MonitorNdbxBook;
t_MonitorNasdaqBook MonitorPsxBook;
t_MonitorNyseBook MonitorNyseBook;
t_MonitorNyseBook MonitorAmexBook;
t_MonitorBatszBook MonitorBatszBook;
t_MonitorBYXBook MonitorBYXBook;
t_MonitorEdgxBook MonitorEdgxBook;
t_MonitorEdgaBook MonitorEdgaBook;
t_MonitorCQS MonitorCQS;
t_MonitorUQDF MonitorUQDF;

//if there is at least 1 option of book/order is valid, this flag is turn on
static int HasValidOption = NO;
static int TimeOfOpt[MAX_THREAD_TO_START];
volatile int IsTradeServerStopped = 0;
extern int TradingCounter;
__thread int _consumerId = -1;  //Note: this is Thread Local Storage (TLS) variable, so there will be performance issue if we use it a lot.

/****************************************************************************
** Function definitions
****************************************************************************/

/****************************************************************************
- Function name:  main
- Input:      
                  + argc
                  + argv
- Return:         Success or Failure
- Description:    + This is an entry point of the trade_server program. 
                  + The routine hides the following facts
                    - The user inputs processing method
                    - Load configuration information from files
                    - Global data structures initialization
                    - Register Signal handlers
                      - Create and start some main threads
                  + There are no preconditions guaranteed to the routine
                  + The input to the routine will be passed by user
                  + The routine guarantees that the status value will 
                    have a value of either
                    - Success
                    or
                    - Failure
- Usage:          The routine will be called by runtime environment
****************************************************************************/
int main(int argc, char *argv[])
{
  DEVLOGF("Running with extra debugging output");

  if (mlockall(MCL_CURRENT | MCL_FUTURE) < 0) {
    perror("error: mlockall");
    exit(1);
  }
  
  // Create raw data path
  CreateRawDataPath();

  SaveVersionInfoToFile();
  
  InitKernelAlgorithmConfig();
  if (LoadKernelAlgorithmConfigFromFile("kernel_algorithm") == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Could not load Kernel Algorithm configuration\n");
    return ERROR;
  }
  SetKernelAlgorithm("[TRADE_SERVER_BASE]");
    
  // Process user inputs
  ProcessUserInput(argc, argv);
  
  // Initialize all data structures
  InitDataStructures();
  
  // Load configuration information from files
  if (LoadConfiguration() == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Could not load all configuration information\n");
    
    return ERROR;
  }
  
  if (InitializeMarketDataComponents() == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Could not initialize and configure market data components.\n");
    return ERROR;
  }
  
  if (InitializeOrderEntryComponents() == ERROR) //This must be called after InitializeMarketDataComponents()
  {
    TraceLog(ERROR_LEVEL, "Could not initialize and configure order entry components.\n");
    return ERROR;
  }

  if (FinalizeDBLFilter() == ERROR) //This must be called after InitializeMarketDataComponents() and InitializeOrderEntryComponents()
  {
    TraceLog(ERROR_LEVEL, "Could not initialize and configure order entry components.\n");
    return ERROR;
  }       

  
  if (InitializeMessageConsumers() == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Could not initialize md/oe messages consumers.\n");
  }
  
  // Register signal handlers
  RegisterSignalHandlers();
  
  // Create and start all main threads
  StartUpMainThreads();
    
    return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessUserInput
- Input:      
          + argc
          + argv
- Output:     N/A
- Return:     Success or Failure
- Description:    + The routine hides the following facts
                    - Check valid of user inputs
                    - Parse user inputs
                    - Use parsed info to create monitoring threads
                  + There are no preconditions guaranteed to the routine
                  + The input to the routine will be passed through by 
                    main routine
                  + The routine guarantees that the return value will 
                    have a value of either
                    - Success
                    or
                    - Failure
- Usage:          The routine will be called by main routine
****************************************************************************/
int ProcessUserInput(int argc, char *argv[])
{
  const char *optString = "vh1:2:3:C:U:A:N:B:Z:O:E:Y:P:X:d:o:r:s:z:y:n:Gc:e:m:p:a:MF:f:b:";
  const struct option long_opt[] = {
    {"help", 0, NULL, 'h'},
    {"all", 1, NULL, '1'},
    {"all-book", 1, NULL, '2'},
    {"all-order", 1, NULL, '3'},
    {"cqs", 1, NULL, 'C'},
    {"uqdf", 1, NULL, 'U'},
    {"arca-book", 1, NULL, 'A'},
    {"ndaq-book", 1, NULL, 'N'},
    {"bx-book", 1, NULL, 'B'},
    {"psx-book", 1, NULL, 'P'},
    {"batsz-book", 1, NULL, 'Z'},
    {"nyse-book", 1, NULL, 'O'},
    {"amex-book", 1, NULL, 'X'},
    {"edgx-book", 1, NULL, 'E'},
    {"edga-book", 1, NULL, 'F'},
    {"byx-book", 1, NULL, 'Y'},
    {"arca-direct", 1, NULL, 'd'},
    {"ndaq-ouch", 1, NULL, 'o'},
    {"ndaq-rash", 1, NULL, 'r'},
    {"batsz-boe", 1, NULL, 'z'},
    {"byx-boe", 1, NULL, 'y'},
    {"nyse-ccg", 1, NULL, 'n'},
    {"edgx", 1, NULL, 'e'},
    {"edga", 1, NULL, 'f'},
    {"nasdaq-bx", 1, NULL, 'b'},
    {"ndaq-psx", 1, NULL, 's'},
    {"oec-config", 1, NULL, 'c'},
    {"mdc-config", 1, NULL, 'm'},
    {"as-listen-port", 1, NULL, 'p'},
    {"trading_account", 1, NULL, 'a'},
    {"no-gap-request", 0, NULL, 'G'},
    {"manual", 0, NULL, 'M'},
    {"version", 0 , NULL,'v'},    
    {NULL, 0, NULL, 0}
  };
  
  // Initialize time of options
  int i;
  for (i = 0; i < MAX_THREAD_TO_START; i++)
  {
    TimeOfOpt[i] = -1;
  }
  
  //Init OEC flags
  for (i = 0; i < MAX_ECN_ORDER; i++)
  {
    IsGottenOrderConfig[i] = NO;
  }
  
  //Initialize MDC flags
  for (i = 0; i < MAX_ECN_BOOK; i++)
  {
    IsGottenBookConfig[i] = NO;
  }
  for (i = 0; i < MAX_BBO_SOURCE; i++)
  {
    IsGottenFeedConfig[i] = NO;
  }
  
  //Init Misc flags:
  IsGottenASConfig = NO;
  IsGottenTradingAccountConfig = NO;
  ManualMode = NO;
  
  //Parse the commandline Opstring
  int nextOption;
  
  do
  {
    nextOption = getopt_long(argc, argv, optString, long_opt, NULL);
    
    switch(nextOption)
    {
      case 'h':
        PrintHelp();
        exit(0);
        break;
      case '1':
        if (getHourMin(optarg, &TimeOfOpt[0], &TimeOfOpt[1]) == ERROR)
        {
          exit(0);
        }
        else
        {
          TraceLog(DEBUG_LEVEL, "Time to start all ECN Books and ECN Orders: %d:%d\n", TimeOfOpt[0], TimeOfOpt[1]);
        }
        
        HasValidOption = YES;
        break;
        
      case '2':
        if (getHourMin(optarg, &TimeOfOpt[2], &TimeOfOpt[3]) == ERROR)
        {
          exit(0);
        }
        else
        {
          TraceLog(DEBUG_LEVEL, "Time to start all ECN Books: %d:%d\n", TimeOfOpt[2], TimeOfOpt[3]);
        }
        
        HasValidOption = YES;
        break;
        
      case '3':
        if (getHourMin(optarg, &TimeOfOpt[4], &TimeOfOpt[5]) == ERROR)
        {
          exit(0);
        }
        else
        {
          TraceLog(DEBUG_LEVEL, "Time to start all ECN Orders %d:%d\n", TimeOfOpt[4], TimeOfOpt[5]);
        }
        
        HasValidOption = YES;
        break;
        
      case 'A':
        if (getHourMin(optarg, &TimeOfOpt[6], &TimeOfOpt[7]) == ERROR)
        {
          exit(0);
        }
        else
        {
          TraceLog(DEBUG_LEVEL, "Time to start ARCA Book %d:%d\n", TimeOfOpt[6], TimeOfOpt[7]);
        }
        
        HasValidOption = YES;
        break;
        
      case 'N':
        if (getHourMin(optarg, &TimeOfOpt[8], &TimeOfOpt[9]) == ERROR)
        {
          exit(0);
        }
        else
        {
          TraceLog(DEBUG_LEVEL, "Time to start NASDAQ Books: %d:%d\n", TimeOfOpt[8], TimeOfOpt[9]);
        }
        
        HasValidOption = YES;
        break;
        
      case 'Z':
        if (getHourMin(optarg, &TimeOfOpt[10], &TimeOfOpt[11]) == ERROR)
        {
          exit(0);
        }
        else
        {
          TraceLog(DEBUG_LEVEL, "Time to start BATS-Z Book: %d:%d\n", TimeOfOpt[10], TimeOfOpt[11]);
        }
        
        HasValidOption = YES;
        break;
        
      case 'Y':
        if (getHourMin(optarg, &TimeOfOpt[40], &TimeOfOpt[41]) == ERROR)
        {
          exit(0);
        }
        else
        {
          TraceLog(DEBUG_LEVEL, "Time to start BYX Book: %d:%d\n", TimeOfOpt[40], TimeOfOpt[41]);
        }
        
        HasValidOption = YES;
        break;
        
      case 'd':
        if (getHourMin(optarg, &TimeOfOpt[12], &TimeOfOpt[13]) == ERROR)
        {
          exit(0);
        }
        else
        {
          TraceLog(DEBUG_LEVEL, "Time to start ARCA Direct: %d:%d\n", TimeOfOpt[12], TimeOfOpt[13]);
        }
        
        HasValidOption = YES;
        break;
        
      case 'o':
        if (getHourMin(optarg, &TimeOfOpt[14], &TimeOfOpt[15]) == ERROR)
        {
          exit(0);
        }
        else
        {
          TraceLog(DEBUG_LEVEL, "Time to start NASDAQ OUCH: %d:%d\n", TimeOfOpt[14], TimeOfOpt[15]);
        }
        
        HasValidOption = YES;
        break;
        
      case 'r':
        if (getHourMin(optarg, &TimeOfOpt[16], &TimeOfOpt[17]) == ERROR)
        {
          exit(0);
        }
        else
        {
          TraceLog(DEBUG_LEVEL, "Time to start NASDAQ RASH: %d:%d\n", TimeOfOpt[16], TimeOfOpt[17]);
        }
        
        HasValidOption = YES;
        break;
        
      case 'z':
        if (getHourMin(optarg, &TimeOfOpt[18], &TimeOfOpt[19]) == ERROR)
        {
          exit(0);
        }
        else
        {
          TraceLog(DEBUG_LEVEL, "Time to start BATSZ BOE: %d:%d\n", TimeOfOpt[18], TimeOfOpt[19]);
        }
        
        HasValidOption = YES;
        break;
        
      case 'y':
        if (getHourMin(optarg, &TimeOfOpt[42], &TimeOfOpt[43]) == ERROR)
        {
          exit(0);
        }
        else
        {
          TraceLog(DEBUG_LEVEL, "Time to start BYX BOE: %d:%d\n", TimeOfOpt[42], TimeOfOpt[43]);
        }
        
        HasValidOption = YES;
        break;
        
      case 'X':
        if (getHourMin(optarg, &TimeOfOpt[48], &TimeOfOpt[49]) == ERROR)
        {
          exit(0);
        }
        else
        {
          TraceLog(DEBUG_LEVEL, "Time to start AMEX Book: %d:%d\n", TimeOfOpt[48], TimeOfOpt[49]);
        }
        
        HasValidOption = YES;
        break;
      
      case 'O':
        if (getHourMin(optarg, &TimeOfOpt[20], &TimeOfOpt[21]) == ERROR)
        {
          exit(0);
        }
        else
        {
          TraceLog(DEBUG_LEVEL, "Time to start NYSE Book: %d:%d\n", TimeOfOpt[20], TimeOfOpt[21]);
        }
        
        HasValidOption = YES;
        break;
        
      case 'n':
        if (getHourMin(optarg, &TimeOfOpt[22], &TimeOfOpt[23]) == ERROR)
        {
          exit(0);
        }
        else
        {
          TraceLog(DEBUG_LEVEL, "Time to start NYSE CCG: %d:%d\n", TimeOfOpt[22], TimeOfOpt[23]);
        }
        
        HasValidOption = YES;
        break;
      
      case 'C':
        if (getHourMin(optarg, &TimeOfOpt[24], &TimeOfOpt[25]) == ERROR)
        {
          exit(0);
        }
        else
        {
          TraceLog(DEBUG_LEVEL, "Time to start CQS: %d:%d\n", TimeOfOpt[24], TimeOfOpt[25]);
        }
        
        HasValidOption = YES;
        break;
        
      case 'U':
        if (getHourMin(optarg, &TimeOfOpt[26], &TimeOfOpt[27]) == ERROR)
        {
          exit(0);
        }
        else
        {
          TraceLog(DEBUG_LEVEL, "Time to start UQDF: %d:%d\n", TimeOfOpt[26], TimeOfOpt[27]);
        }
        
        HasValidOption = YES;
        break;
      
      case 'e':
        if (getHourMin(optarg, &TimeOfOpt[28], &TimeOfOpt[29]) == ERROR)
        {
          exit(0);
        }
        else
        {
          TraceLog(DEBUG_LEVEL, "Time to start EDGX: %d:%d\n", TimeOfOpt[28], TimeOfOpt[29]);
        }
        HasValidOption = YES;
        break;
      
      case 'E':
        if (getHourMin(optarg, &TimeOfOpt[30], &TimeOfOpt[31]) == ERROR)
        {
          exit(0);
        }
        else
        {
          TraceLog(DEBUG_LEVEL, "Time to start EDGX Book: %d:%d\n", TimeOfOpt[30], TimeOfOpt[31]);
        }
        HasValidOption = YES;
        break;
        
      case 'f':
        if (getHourMin(optarg, &TimeOfOpt[32], &TimeOfOpt[33]) == ERROR)
        {
          exit(0);
        }
        else
        {
          TraceLog(DEBUG_LEVEL, "Time to start EDGA: %d:%d\n", TimeOfOpt[32], TimeOfOpt[33]);
        }
        HasValidOption = YES;
        break;
      
      case 'F':
        if (getHourMin(optarg, &TimeOfOpt[34], &TimeOfOpt[35]) == ERROR)
        {
          exit(0);
        }
        else
        {
          TraceLog(DEBUG_LEVEL, "Time to start EDGA Book: %d:%d\n", TimeOfOpt[34], TimeOfOpt[35]);
        }
        HasValidOption = YES;
        break;
        
      case 'B':
        if (getHourMin(optarg, &TimeOfOpt[36], &TimeOfOpt[37]) == ERROR)
        {
          exit(0);
        }
        else
        {
          TraceLog(DEBUG_LEVEL, "Time to start BX Books: %d:%d\n", TimeOfOpt[36], TimeOfOpt[37]);
        }
        
        HasValidOption = YES;
        break;
        
      case 'b':
        if (getHourMin(optarg, &TimeOfOpt[38], &TimeOfOpt[39]) == ERROR)
        {
          exit(0);
        }
        else
        {
          TraceLog(DEBUG_LEVEL, "Time to start NASDAQ BX OUCH: %d:%d\n", TimeOfOpt[38], TimeOfOpt[39]);
        }
        
        HasValidOption = YES;
        break;
      case 's':
        if (getHourMin(optarg, &TimeOfOpt[44], &TimeOfOpt[45]) == ERROR)
        {
          exit(0);
        }
        else
        {
          TraceLog(DEBUG_LEVEL, "Time to start PSX OEC: %d:%d\n", TimeOfOpt[44], TimeOfOpt[45]);
        }
        
        HasValidOption = YES;
        break;
      case 'P':
        if (getHourMin(optarg, &TimeOfOpt[46], &TimeOfOpt[47]) == ERROR)
        {
          exit(0);
        }
        else
        {
          TraceLog(DEBUG_LEVEL, "Time to start PSX MDC: %d:%d\n", TimeOfOpt[46], TimeOfOpt[47]);
        }
        
        HasValidOption = YES;
        break;
        
      case 'v':
          TraceLog(DEBUG_LEVEL, "************************************************************\n");
          TraceLog(DEBUG_LEVEL, "Trade Server: version %s \n", VERSION);
          TraceLog(DEBUG_LEVEL, "************************************************************\n");
        
        exit(0) ;
      case 'G':
        break;
      case 'M':
        TraceLog(DEBUG_LEVEL, "Starting trade server in manual mode\n");
        ManualMode = YES;
        break;
      case 'c':
        ProcessOecInformation(optarg);
        break;
      case 'm':
        ProcessMdcInformation(optarg);
        break;
      case 'a':
        if (ProcessTradingAccountInfo(optarg) == SUCCESS)
        {
          IsGottenTradingAccountConfig = YES;
        }
        else
        {
          TraceLog(DEBUG_LEVEL, "Trading Account configuration will be loaded from file\n");
        }
        break;
      case 'p':
        if (ProcessASConfigInfo(optarg) == SUCCESS)
        {
          IsGottenASConfig = YES;
        }
        else
        {
          TraceLog(DEBUG_LEVEL, "AS configuration will be loaded from file\n");
        }
        break;
      case -1 :
        //Option done
        break;
        
      default:
        TraceLog(ERROR_LEVEL, "Invalid option\n");
        continue;
    }
  }  while(nextOption != -1);
  
  //Check number of options on comandline
  if(argc == 1)
  {
    return ERROR;
  }
  else
  {
    return SUCCESS;
  }
}

/****************************************************************************
- Function name:  Thread_StartProgramWithOptions
- Usage:      The routine will be called by main routine
****************************************************************************/
void *Thread_StartProgramWithOptions(void* args)
{
  //int *TimeOfOpt = (int*) args;
  //Local Time
  time_t now;
  struct tm *timeNow;
  int currentHour, currentMin;
  
  while(MarketDataDeviceInitialized == NO)
  {
    sleep(1);
  } 
  
  while(1)
  { 
    now = (time_t)hbitime_seconds();
  
    timeNow = (struct tm *)localtime(&now);
    
    currentHour = timeNow->tm_hour;
    currentMin = timeNow->tm_min;

    //Check value of time option for all Books and Orders
    if(TimeOfOpt[0] != -1)
    {
      //Start all Books and Order
      //Check time to start them
      if((TimeOfOpt[0] * 60 + TimeOfOpt[1]) <= (currentHour * 60 + currentMin))
      {
        TimeOfOpt[0] = -1;
        TimeOfOpt[1] = -1;

        StartThread(ALL_THREADS);
      }
    }
    else
    {
      // Check value of time option for all Books
      if(TimeOfOpt[2] != -1)
      {
        //Start All Books
        if((TimeOfOpt[2] * 60 + TimeOfOpt[3]) <= (currentHour * 60 + currentMin))
        {
          TimeOfOpt[2] = -1;
          TimeOfOpt[3] = -1;

          StartThread(ALL_BOOK_THREADS);
        }
      }
      else
      {
        if(TimeOfOpt[6] != -1)
        {
          //Start ARCA Book
          if((TimeOfOpt[6] * 60 + TimeOfOpt[7]) <= (currentHour * 60 + currentMin))
          {
            TimeOfOpt[6] = -1;
            TimeOfOpt[7] = -1;

            StartThread(ARCA_BOOK_THREAD);
          }
        }       
        else if(TimeOfOpt[8] != -1)
        {
          //Start NDAQ Book
          if((TimeOfOpt[8] * 60 + TimeOfOpt[9]) <= (currentHour * 60 + currentMin))
          {
            TimeOfOpt[8] = -1;
            TimeOfOpt[9] = -1;
            
            StartThread(NASDAQ_BOOK_THREAD);
          }
        }       
        else if(TimeOfOpt[10] != -1)
        {
          //Start BATS-Z Book
          if((TimeOfOpt[10] * 60 + TimeOfOpt[11]) <= (currentHour * 60 + currentMin))
          {
            TimeOfOpt[10] = -1;
            TimeOfOpt[11] = -1;
            
            StartThread(BATSZ_BOOK_THREAD);
          }
        }       
        else if(TimeOfOpt[20] != -1)
        {
          //Start NYSE Book
          if((TimeOfOpt[20] * 60 + TimeOfOpt[21]) <= (currentHour * 60 + currentMin))
          {
            TimeOfOpt[20] = -1;
            TimeOfOpt[21] = -1;
            
            StartThread(NYSE_BOOK_THREAD);
          }
        }
        else if(TimeOfOpt[48] != -1)
        {
          //Start AMEX Book
          if((TimeOfOpt[48] * 60 + TimeOfOpt[49]) <= (currentHour * 60 + currentMin))
          {
            TimeOfOpt[48] = -1;
            TimeOfOpt[49] = -1;
            
            StartThread(AMEX_BOOK_THREAD);
          }
        }
        else if(TimeOfOpt[30] != -1)
        {
          //Start EDGX Book
          if((TimeOfOpt[30] * 60 + TimeOfOpt[31]) <= (currentHour * 60 + currentMin))
          {
            TimeOfOpt[30] = -1;
            TimeOfOpt[31] = -1;
            
            StartThread(EDGX_BOOK_THREAD);
          }
        }
        else if(TimeOfOpt[34] != -1)
        {
          //Start EDGA Book
          if((TimeOfOpt[34] * 60 + TimeOfOpt[35]) <= (currentHour * 60 + currentMin))
          {
            TimeOfOpt[34] = -1;
            TimeOfOpt[35] = -1;
            
            StartThread(EDGA_BOOK_THREAD);
          }
        }
        else if(TimeOfOpt[36] != -1)
        {
          //Start BX Book
          if((TimeOfOpt[36] * 60 + TimeOfOpt[37]) <= (currentHour * 60 + currentMin))
          {
            TimeOfOpt[36] = -1;
            TimeOfOpt[37] = -1;
            
            StartThread(NDBX_BOOK_THREAD);
          }
        }
        else if(TimeOfOpt[40] != -1)
        {
          //Start BX Book
          if((TimeOfOpt[40] * 60 + TimeOfOpt[41]) <= (currentHour * 60 + currentMin))
          {
            TimeOfOpt[40] = -1;
            TimeOfOpt[41] = -1;
            
            StartThread(BYX_BOOK_THREAD);
          }
        }
        else if(TimeOfOpt[46] != -1)
        {
          //Start PSX Book
          if((TimeOfOpt[46] * 60 + TimeOfOpt[47]) <= (currentHour * 60 + currentMin))
          {
            TimeOfOpt[46] = -1;
            TimeOfOpt[47] = -1;
            
            StartThread(PSX_BOOK_THREAD);
          }
        }
      }
      
      // Check value of time option for all Orders
      if(TimeOfOpt[4] != -1)
      {
        //Start All Orders
        if((TimeOfOpt[4] * 60 + TimeOfOpt[5]) <= (currentHour * 60 + currentMin))
        {
          TimeOfOpt[4] = -1;
          TimeOfOpt[5] = -1;
          
          StartThread(ALL_ORDER_THREADS);
        }
      }
      else
      {
        if(TimeOfOpt[12] != -1)
        {
          //Start ARCA DIRECT Order
          if((TimeOfOpt[12] * 60 + TimeOfOpt[13]) <= (currentHour * 60 + currentMin))
          {
            TimeOfOpt[12] = -1;
            TimeOfOpt[13] = -1;
            
            StartThread(ARCA_DIRECT_THREAD);
          }
        }       
        else if(TimeOfOpt[14] != -1)
        {
          //Start NDAQ OUCH Order
          if((TimeOfOpt[14] * 60 + TimeOfOpt[15]) <= (currentHour * 60 + currentMin))
          {
            TimeOfOpt[14] = -1;
            TimeOfOpt[15] = -1;
            
            StartThread(NASDAQ_OUCH_THREAD);
          }
        }       
        else if(TimeOfOpt[16] != -1)
        {
          //Start NDAQ RASH Order
          if((TimeOfOpt[16] * 60 + TimeOfOpt[17]) <= (currentHour * 60 + currentMin))
          {
            TimeOfOpt[16] = -1;
            TimeOfOpt[17] = -1;
            
            StartThread(NASDAQ_RASH_THREAD);
          }
        }       
        else if(TimeOfOpt[18] != -1)
        {
          //Start BATSZ BOE Order
          if((TimeOfOpt[18] * 60 + TimeOfOpt[19]) <= (currentHour * 60 + currentMin))
          {
            TimeOfOpt[18] = -1;
            TimeOfOpt[19] = -1;
            
            StartThread(BATSZ_BOE_THREAD);
          }
        }
        else if(TimeOfOpt[42] != -1)
        {
          //Start BATSZ BOE Order
          if((TimeOfOpt[42] * 60 + TimeOfOpt[43]) <= (currentHour * 60 + currentMin))
          {
            TimeOfOpt[42] = -1;
            TimeOfOpt[43] = -1;
            
            StartThread(BYX_BOE_THREAD);
          }
        }
        else if(TimeOfOpt[22] != -1)
        {
          //Start NYSE CCG
          if((TimeOfOpt[22] * 60 + TimeOfOpt[23]) <= (currentHour * 60 + currentMin))
          {
            TimeOfOpt[22] = -1;
            TimeOfOpt[23] = -1;
            
            StartThread(NYSE_CCG_THREAD);
          }
        }
        else if(TimeOfOpt[28] != -1)
        {
          //Start EDGX
          if((TimeOfOpt[28] * 60 + TimeOfOpt[29]) <= (currentHour * 60 + currentMin))
          {
            TimeOfOpt[28] = -1;
            TimeOfOpt[29] = -1;
            
            StartThread(EDGX_THREAD);
          }
        }
        else if(TimeOfOpt[32] != -1)
        {
          //Start EDGA
          if((TimeOfOpt[32] * 60 + TimeOfOpt[33]) <= (currentHour * 60 + currentMin))
          {
            TimeOfOpt[32] = -1;
            TimeOfOpt[33] = -1;
            
            StartThread(EDGA_THREAD);
          }
        }
        else if(TimeOfOpt[38] != -1)
        {
          //Start NASDAQ BX OUCH
          if((TimeOfOpt[38] * 60 + TimeOfOpt[39]) <= (currentHour * 60 + currentMin))
          {
            TimeOfOpt[38] = -1;
            TimeOfOpt[39] = -1;
            
            StartThread(NASDAQ_BX_THREAD);
          }
        }
        else if(TimeOfOpt[44] != -1)
        {
          if((TimeOfOpt[44] * 60 + TimeOfOpt[45]) <= (currentHour * 60 + currentMin))
          {
            TimeOfOpt[44] = -1;
            TimeOfOpt[45] = -1;
            
            StartThread(NASDAQ_PSX_THREAD);
          }
        }
        else if(TimeOfOpt[24] != -1)
        {
          if((TimeOfOpt[24] * 60 + TimeOfOpt[25]) <= (currentHour * 60 + currentMin))
          {
            TimeOfOpt[24] = -1;
            TimeOfOpt[25] = -1;
            
            StartThread(CQS_THREAD);
          }
        }
        else if(TimeOfOpt[26] != -1)
        {
          if((TimeOfOpt[26] * 60 + TimeOfOpt[27]) <= (currentHour * 60 + currentMin))
          {
            TimeOfOpt[26] = -1;
            TimeOfOpt[27] = -1;
            
            StartThread(UQDF_THREAD);
          }
        }
      }
    }

    // Delay in second(s)
    sleep(2);
    
    int i;    
    int count = 0;
    for (i = 0; i < MAX_THREAD_TO_START; i++)
    {
      if (TimeOfOpt[i] != -1)
      {
        break;
      }
      else
      {
        count++;
      }
    }
    
    if (count == MAX_THREAD_TO_START)
    {
      return NULL;
    }
  }
    
  return NULL;
  
}

/****************************************************************************
- Function name:  StartThread
****************************************************************************/
int StartThread(char threadType)
{
  switch(threadType)
  {
    case ALL_THREADS:
      //-----------------
      //  Start books
      //-----------------
      ConnectToVenue(BOOK_ARCA);
      ConnectToVenue(BOOK_NASDAQ);
      ConnectToVenue(BOOK_NDBX);
      ConnectToVenue(BOOK_PSX);
      ConnectToVenue(BOOK_NYSE);
      ConnectToVenue(BOOK_AMEX);
      ConnectToVenue(BOOK_BATSZ);
      ConnectToVenue(BOOK_BYX);
      ConnectToVenue(BOOK_EDGX);
      ConnectToVenue(BOOK_EDGA);
      
      //-----------------
      //  Start Orders
      //-----------------
      if (OrderStatusMgmt[ORDER_ARCA_DIRECT].isConnected == DISCONNECTED &&
          OrderStatusMgmt[ORDER_ARCA_DIRECT].shouldConnect == NO)
      {
        OrderStatusMgmt[ORDER_ARCA_DIRECT].shouldConnect = YES;
        Connect_ARCA_DIRECT();
      }

      if (OrderStatusMgmt[ORDER_NASDAQ_OUCH].isConnected == DISCONNECTED &&
          OrderStatusMgmt[ORDER_NASDAQ_OUCH].shouldConnect == NO)
      {
        OrderStatusMgmt[ORDER_NASDAQ_OUCH].shouldConnect = YES;
        Connect_Ouch(ORDER_NASDAQ_OUCH);
      }

      if (OrderStatusMgmt[ORDER_NASDAQ_BX].isConnected == DISCONNECTED &&
          OrderStatusMgmt[ORDER_NASDAQ_BX].shouldConnect == NO)
      {
        OrderStatusMgmt[ORDER_NASDAQ_BX].shouldConnect = YES;
        Connect_Ouch(ORDER_NASDAQ_BX);
      }
      
      if (OrderStatusMgmt[ORDER_NASDAQ_PSX].isConnected == DISCONNECTED &&
          OrderStatusMgmt[ORDER_NASDAQ_PSX].shouldConnect == NO)
      {
        OrderStatusMgmt[ORDER_NASDAQ_PSX].shouldConnect = YES;
        Connect_Ouch(ORDER_NASDAQ_PSX);
      }

      if (OrderStatusMgmt[ORDER_NASDAQ_RASH].isConnected == DISCONNECTED &&
          OrderStatusMgmt[ORDER_NASDAQ_RASH].shouldConnect == NO)
      {
        OrderStatusMgmt[ORDER_NASDAQ_RASH].shouldConnect = YES;
        Connect_NASDAQ_RASH();
      }

      if (OrderStatusMgmt[ORDER_NYSE_CCG].isConnected == DISCONNECTED &&
          OrderStatusMgmt[ORDER_NYSE_CCG].shouldConnect == NO)
      {
        OrderStatusMgmt[ORDER_NYSE_CCG].shouldConnect = YES;
        Connect_NYSE_CCG();
      }
      
      if (OrderStatusMgmt[ORDER_BATSZ_BOE].isConnected == DISCONNECTED &&
          OrderStatusMgmt[ORDER_BATSZ_BOE].shouldConnect == NO)
      {
        OrderStatusMgmt[ORDER_BATSZ_BOE].shouldConnect = YES;
        Connect_BATS_BOE(ORDER_BATSZ_BOE);
      }
      
      if (OrderStatusMgmt[ORDER_BYX_BOE].isConnected == DISCONNECTED &&
        OrderStatusMgmt[ORDER_BYX_BOE].shouldConnect == NO)
      {
        OrderStatusMgmt[ORDER_BYX_BOE].shouldConnect = YES;
        Connect_BATS_BOE(ORDER_BYX_BOE);
      }
      
      if (OrderStatusMgmt[ORDER_EDGX].isConnected == DISCONNECTED &&
          OrderStatusMgmt[ORDER_EDGX].shouldConnect == NO)
      {
        OrderStatusMgmt[ORDER_EDGX].shouldConnect = YES;
        Connect_EDGE(ORDER_EDGX);
      }
      
      if (OrderStatusMgmt[ORDER_EDGA].isConnected == DISCONNECTED &&
          OrderStatusMgmt[ORDER_EDGA].shouldConnect == NO)
      {
        OrderStatusMgmt[ORDER_EDGA].shouldConnect = YES;
        Connect_EDGE(ORDER_EDGA);
      }
      
      break;
      
    case ALL_BOOK_THREADS:
      ConnectToVenue(BOOK_ARCA);
      ConnectToVenue(BOOK_NASDAQ);
      ConnectToVenue(BOOK_NDBX);
      ConnectToVenue(BOOK_PSX);
      ConnectToVenue(BOOK_NYSE);
      ConnectToVenue(BOOK_AMEX);
      ConnectToVenue(BOOK_BATSZ);
      ConnectToVenue(BOOK_BYX);
      ConnectToVenue(BOOK_EDGX);
      ConnectToVenue(BOOK_EDGA);
      break;
      
    case ALL_ORDER_THREADS:
      if (OrderStatusMgmt[ORDER_ARCA_DIRECT].isConnected == DISCONNECTED &&
          OrderStatusMgmt[ORDER_ARCA_DIRECT].shouldConnect == NO)
      {
        OrderStatusMgmt[ORDER_ARCA_DIRECT].shouldConnect = YES;
        Connect_ARCA_DIRECT();
      }

      if (OrderStatusMgmt[ORDER_NASDAQ_OUCH].isConnected == DISCONNECTED &&
          OrderStatusMgmt[ORDER_NASDAQ_OUCH].shouldConnect == NO)
      {
        OrderStatusMgmt[ORDER_NASDAQ_OUCH].shouldConnect = YES;
        Connect_Ouch(ORDER_NASDAQ_OUCH);
      }

      if (OrderStatusMgmt[ORDER_NASDAQ_BX].isConnected == DISCONNECTED &&
          OrderStatusMgmt[ORDER_NASDAQ_BX].shouldConnect == NO)
      {
        OrderStatusMgmt[ORDER_NASDAQ_BX].shouldConnect = YES;
        Connect_Ouch(ORDER_NASDAQ_BX);
      }
      
       if (OrderStatusMgmt[ORDER_NASDAQ_PSX].isConnected == DISCONNECTED &&
          OrderStatusMgmt[ORDER_NASDAQ_PSX].shouldConnect == NO)
      {
        OrderStatusMgmt[ORDER_NASDAQ_PSX].shouldConnect = YES;
        Connect_Ouch(ORDER_NASDAQ_PSX);
      }

      if (OrderStatusMgmt[ORDER_NASDAQ_RASH].isConnected == DISCONNECTED &&
          OrderStatusMgmt[ORDER_NASDAQ_RASH].shouldConnect == NO)
      {
        OrderStatusMgmt[ORDER_NASDAQ_RASH].shouldConnect = YES;
        Connect_NASDAQ_RASH();
      }

      if (OrderStatusMgmt[ORDER_NYSE_CCG].isConnected == DISCONNECTED &&
          OrderStatusMgmt[ORDER_NYSE_CCG].shouldConnect == NO)
      {
        OrderStatusMgmt[ORDER_NYSE_CCG].shouldConnect = YES;
        Connect_NYSE_CCG();
      }

      if (OrderStatusMgmt[ORDER_BATSZ_BOE].isConnected == DISCONNECTED &&
          OrderStatusMgmt[ORDER_BATSZ_BOE].shouldConnect == NO)
      {
        OrderStatusMgmt[ORDER_BATSZ_BOE].shouldConnect = YES;
        Connect_BATS_BOE(ORDER_BATSZ_BOE);
      }
      
      if (OrderStatusMgmt[ORDER_BYX_BOE].isConnected == DISCONNECTED &&
        OrderStatusMgmt[ORDER_BYX_BOE].shouldConnect == NO)
      {
        OrderStatusMgmt[ORDER_BYX_BOE].shouldConnect = YES;
        Connect_BATS_BOE(ORDER_BYX_BOE);
      }
      
      if (OrderStatusMgmt[ORDER_EDGX].isConnected == DISCONNECTED &&
          OrderStatusMgmt[ORDER_EDGX].shouldConnect == NO)
      {
        OrderStatusMgmt[ORDER_EDGX].shouldConnect = YES;
        Connect_EDGE(ORDER_EDGX);
      }
      
      if (OrderStatusMgmt[ORDER_EDGA].isConnected == DISCONNECTED &&
          OrderStatusMgmt[ORDER_EDGA].shouldConnect == NO)
      {
        OrderStatusMgmt[ORDER_EDGA].shouldConnect = YES;
        Connect_EDGE(ORDER_EDGA);
      }
      break;
      
    case ARCA_BOOK_THREAD:
      ConnectToVenue(BOOK_ARCA);
      break;
      
    case NYSE_BOOK_THREAD:
      ConnectToVenue(BOOK_NYSE);
      break;
    
    case AMEX_BOOK_THREAD:
      ConnectToVenue(BOOK_AMEX);
      break;
      
    case NASDAQ_BOOK_THREAD:
      ConnectToVenue(BOOK_NASDAQ);
      break;
      
    case NDBX_BOOK_THREAD:
      ConnectToVenue(BOOK_NDBX);
      break;
    
    case PSX_BOOK_THREAD:
      ConnectToVenue(BOOK_PSX);
      break;
      
    case BATSZ_BOOK_THREAD:
      ConnectToVenue(BOOK_BATSZ);
      break;
      
    case BYX_BOOK_THREAD:
      ConnectToVenue(BOOK_BYX);
      break;
      
    case EDGX_BOOK_THREAD:
      ConnectToVenue(BOOK_EDGX);
      break;
      
    case EDGA_BOOK_THREAD:
      ConnectToVenue(BOOK_EDGA);
      break;
      
    case ARCA_DIRECT_THREAD:
      if (OrderStatusMgmt[ORDER_ARCA_DIRECT].isConnected == DISCONNECTED &&
          OrderStatusMgmt[ORDER_ARCA_DIRECT].shouldConnect == NO)
      {
        OrderStatusMgmt[ORDER_ARCA_DIRECT].shouldConnect = YES;
        
        Connect_ARCA_DIRECT();
      }
      break;
      
    case NASDAQ_OUCH_THREAD:
      if (OrderStatusMgmt[ORDER_NASDAQ_OUCH].isConnected == DISCONNECTED &&
          OrderStatusMgmt[ORDER_NASDAQ_OUCH].shouldConnect == NO)
      {
        OrderStatusMgmt[ORDER_NASDAQ_OUCH].shouldConnect = YES;
        
        Connect_Ouch(ORDER_NASDAQ_OUCH);
      }
      break;
      
    case NASDAQ_BX_THREAD:
      if (OrderStatusMgmt[ORDER_NASDAQ_BX].isConnected == DISCONNECTED &&
          OrderStatusMgmt[ORDER_NASDAQ_BX].shouldConnect == NO)
      {
        OrderStatusMgmt[ORDER_NASDAQ_BX].shouldConnect = YES;
        
        Connect_Ouch(ORDER_NASDAQ_BX);
      }
      break;
      
    case NASDAQ_PSX_THREAD:
      if (OrderStatusMgmt[ORDER_NASDAQ_PSX].isConnected == DISCONNECTED &&
          OrderStatusMgmt[ORDER_NASDAQ_PSX].shouldConnect == NO)
      {
        OrderStatusMgmt[ORDER_NASDAQ_PSX].shouldConnect = YES;
        
        Connect_Ouch(ORDER_NASDAQ_PSX);
      }
      break;
      
    case NASDAQ_RASH_THREAD:
      if (OrderStatusMgmt[ORDER_NASDAQ_RASH].isConnected == DISCONNECTED &&
          OrderStatusMgmt[ORDER_NASDAQ_RASH].shouldConnect == NO)
      {
        OrderStatusMgmt[ORDER_NASDAQ_RASH].shouldConnect = YES;
        
        Connect_NASDAQ_RASH();
      }
      break;

    case NYSE_CCG_THREAD:
      if (OrderStatusMgmt[ORDER_NYSE_CCG].isConnected == DISCONNECTED &&
          OrderStatusMgmt[ORDER_NYSE_CCG].shouldConnect == NO)
      {
        OrderStatusMgmt[ORDER_NYSE_CCG].shouldConnect = YES;
        
        Connect_NYSE_CCG();
      }
      break;
    case BATSZ_BOE_THREAD:
      if (OrderStatusMgmt[ORDER_BATSZ_BOE].isConnected == DISCONNECTED &&
          OrderStatusMgmt[ORDER_BATSZ_BOE].shouldConnect == NO)
      {
        OrderStatusMgmt[ORDER_BATSZ_BOE].shouldConnect = YES;

        Connect_BATS_BOE(ORDER_BATSZ_BOE);
      }
      break;
    case BYX_BOE_THREAD:
      if (OrderStatusMgmt[ORDER_BYX_BOE].isConnected == DISCONNECTED &&
        OrderStatusMgmt[ORDER_BYX_BOE].shouldConnect == NO)
      {
        OrderStatusMgmt[ORDER_BYX_BOE].shouldConnect = YES;

        Connect_BATS_BOE(ORDER_BYX_BOE);
      }
      break;
    case EDGX_THREAD:
      if (OrderStatusMgmt[ORDER_EDGX].isConnected == DISCONNECTED &&
          OrderStatusMgmt[ORDER_EDGX].shouldConnect == NO)
      {
        OrderStatusMgmt[ORDER_EDGX].shouldConnect = YES;
        
        Connect_EDGE(ORDER_EDGX);
      }
      break;
    case EDGA_THREAD:
      if (OrderStatusMgmt[ORDER_EDGA].isConnected == DISCONNECTED &&
          OrderStatusMgmt[ORDER_EDGA].shouldConnect == NO)
      {
        OrderStatusMgmt[ORDER_EDGA].shouldConnect = YES;
        
        Connect_EDGE(ORDER_EDGA);
      }
      break;
      
    case CQS_THREAD:
      ConnectToVenue(FEED_CQS);
      break;
    case UQDF_THREAD:
      ConnectToVenue(FEED_UQDF);
      break;
    default:
      break;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  getHourMin
****************************************************************************/
int getHourMin(char *opt_arg, int *hour, int *min)
{
  char *temp;
        
  char strHour[3];

  char strMin[3];

  memset(strHour, 0, 3);
  memset(strMin, 0, 3);

  temp = strstr(opt_arg, ":");

  if (temp == NULL)
  {
    TraceLog(DEBUG_LEVEL, "Time argument for command is invalid: %s\n", opt_arg);
    TraceLog(DEBUG_LEVEL, "-h --help for list commands\n");

    return ERROR;
  }
  else
  {
    char *iTemp = index(opt_arg, ':');
    
    int i;
    i = strlen(opt_arg) - strlen(iTemp);

    if ((i == 0) || (i > 2))
    {
      TraceLog(DEBUG_LEVEL, "Time argument for command is invalid: %s\n", opt_arg);
      TraceLog(DEBUG_LEVEL, "-h --help for list commands\n");

      return ERROR;
    }
    else
    {
      int j = 0;

      for (j = 0; j < i; j++)
      {
        //We must check hour string, it must is string of digits (0, 1, 2...9)
        if (isdigit(opt_arg[j]) == 0)
        {
          TraceLog(DEBUG_LEVEL, "Time argument for command is invalid: %s\n", opt_arg);
          TraceLog(DEBUG_LEVEL, "-h --help for list commands\n");

          return ERROR;
        }
        else
        {
          strHour[j] = opt_arg[j];
        }
      }

      //Hour must >= 0 and <= 23
      *hour = atoi(strHour);

      if ((*hour < 0) || (*hour > 23))
      {
        TraceLog(DEBUG_LEVEL, "Time argument  for command is invalid: %s\n", opt_arg);
        TraceLog(DEBUG_LEVEL, "-h --help for list commands\n");

        return ERROR;
      }

      for (j = i + 1; j < strlen(opt_arg); j++)
      {
        //We must check hour string, it must is string of digits (0, 1, 2...9)
        if (isdigit(opt_arg[j]) == 0)
        {
          TraceLog(DEBUG_LEVEL, "Time argument for command is invalid: %s\n", opt_arg);
          TraceLog(DEBUG_LEVEL, "-h --help for list commands\n");

          return ERROR;
        }
        else
        {
          strMin[j-(i+1)] = opt_arg[j];
        }
      }
      
      //Min must >=0 and <= 59
      *min = atoi(strMin);

      if ((*min < 0) || (*min > 59))
      {
        TraceLog(DEBUG_LEVEL, "Time argument  for command is invalid: %s\n", opt_arg);
        TraceLog(DEBUG_LEVEL, "-h --help for list commands\n");

        return ERROR;
      }
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadConfiguration
****************************************************************************/
int LoadConfiguration(void)
{
  if (LoadAlgorithmConfig("algorithm", &AlgorithmConfig) == ERROR)
  {
    return ERROR;
  }
  
  if (LoadSplitSymbolInfo() == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Could not load %s\n", FILE_NAME_OF_SPLIT_SYMBOL_INFO);
    return ERROR;
  }
  
  if (LoadTradeOutputLogFromFile() == ERROR)
  {
    return ERROR;
  }
  
  if (LoadOrderIDAndCrossIDFromFile("orderID_crossID", &CurrentOrderID, &CurrentCrossID) == ERROR)
  {
    return ERROR;
  }
  
  if (LoadBBOInfoFromFile() == ERROR)
  {
    return ERROR;
  }
  
  LoadMaxSpreadSymbolFromFile("maxspread_symbol");
  
  if (LoadAllDataBlocksFromFiles() == ERROR)
  {
    return ERROR;
  }
  
  if (LoadAllBooksConfig() == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Could not load books configuration\n");
    return ERROR;
  }
  
  // Load Exchanges Conf
  if (LoadExchangesConf() == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Could not load Exchanged Config\n");
    return ERROR;
  }
  
  // Load ISO algorithm
  if (LoadISO_Algorithm() == ERROR)
  {
    return ERROR;
  }
  
  // Load CQS configuration
  if (IsGottenFeedConfig[CQS_SOURCE] == NO)
  {
    if (LoadCQS_Config("cqs_conf", CQS_Conf.group) == ERROR)
    {
      return ERROR;
    }
  }
  
  // Load UQDF configuration
  if (IsGottenFeedConfig[UQDF_SOURCE] == NO)
  {
    if (LoadUQDF_Config("uqdf_conf") == ERROR)
    {
      return ERROR;
    }
  }
  
  if (LoadAllOrdersConfig() == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Could not load orders configuration\n");
    return ERROR;
  }
    
  if (IsGottenASConfig == NO)
  {
    if (LoadAS_Config("as_config", &ASConfig) == ERROR)
    {
      return ERROR;
    }
  }
  
  if (LoadMinSpreadConfig("min_spread") == ERROR)
  {
    return ERROR;
  }
      
  // Load incoming sequence number from file
  if (LoadSequenceNumber(FN_SEQUENCE_NUM_OUCH, ORDER_NASDAQ_OUCH) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Could not load Nasdaq Ouch Seqs\n");
    return ERROR;
  }
  
  if (LoadSequenceNumber(FN_SEQUENCE_NUM_BX, ORDER_NASDAQ_BX) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Could not load Nasdaq Bx Seqs\n");
    return ERROR;
  }
  
  if (LoadSequenceNumber(FN_SEQUENCE_NUM_PSX, ORDER_NASDAQ_PSX) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Could not load Nasdaq PSX Seqs\n");
    return ERROR;
  }
  
  // Load incoming sequence number from file
  if (LoadSequenceNumber(FN_SEQUENCE_NUM_RASH, ORDER_NASDAQ_RASH) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Could not load Nasdaq Rash Seqs\n");
    return ERROR;
  }
  
  if (LoadSequenceNumber(FN_SEQUENCE_NUM_ARCA, ORDER_ARCA_DIRECT) == ERROR)
  { 
    TraceLog(ERROR_LEVEL, "Could not load Arca Direct Seqs\n");
    return ERROR;
  }

  if (LoadSequenceNumber(FN_SEQUENCE_NUM_NYSE, ORDER_NYSE_CCG) == ERROR)
  { 
    TraceLog(ERROR_LEVEL, "Could not load Nyse CCG Seqs\n");
    return ERROR;
  }
  
  if (LoadSequenceNumber(FN_SEQUENCE_NUM_BATSZ, ORDER_BATSZ_BOE) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Could not load Batsz BOE Seqs\n");
    return ERROR;
  }
  
  if (LoadSequenceNumber(FN_SEQUENCE_NUM_BYX, ORDER_BYX_BOE) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Could not load BYX BOE Seqs\n");
    return ERROR;
  }
  
  // Load incoming sequence number from file
  if (LoadSequenceNumber(FN_SEQUENCE_NUM_EDGX, ORDER_EDGX) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Could not load EDGX Seqs\n");
    return ERROR;
  }

  // Load incoming sequence number from file
  if (LoadSequenceNumber(FN_SEQUENCE_NUM_EDGA, ORDER_EDGA) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Could not load EDGA Seqs\n");
    return ERROR;
  }

  //Load trading account
  if (IsGottenTradingAccountConfig == NO)
  {
    if (LoadTradingAccountFromFile("trading_account", &TradingAccount) == ERROR)
    {
      TraceLog(ERROR_LEVEL, "Could not load Trading Account configuration!\n");
      return ERROR;
    }
  }
  
  if (LoadTradingSchedule() == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Could not load Trading Schedule configuration\n");
    return ERROR;
  }
  
  if (LoadStaleMarketDataConfig() == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Could not load Stale Data Threshold configuration\n");
    return ERROR;
  }
  
  return SUCCESS;
}
       
void InitFilterStructures(void) 
{
  int i;

  for (i = 0; i < DBLFILTER_NUM_QUEUES+1; i++)
  {             
    FilterBuffers[i].index = i;
    
    FilterBuffers[i].marketDataReceiveQueue = spscAllocate(DBLFILTER_MD_QUEUE_SIZE);
    FilterBuffers[i].marketDataReceiveSeq = (long *) calloc(1, sizeof(long) * 256);
    memset(FilterBuffers[i].marketDataReceiveSeq, 0, (sizeof(long) * 256));
    
    FilterBuffers[i].orderEntryReceiveQueue = spscAllocate(DBLFILTER_OE_QUEUE_SIZE);
    FilterBuffers[i].orderEntryReceiveSeq = (long *) calloc(1, sizeof(long) * 256);
    memset(FilterBuffers[i].orderEntryReceiveSeq, 0, (sizeof(long) * 256));
    
    FilterBuffers[i].orderEntrySendQueue = spscAllocate(DBLFILTER_OE_QUEUE_SIZE);
    FilterBuffers[i].orderEntrySendSeq = (long *) calloc(1, sizeof(long) * 256);
    memset(FilterBuffers[i].orderEntrySendSeq, 0, (sizeof(long) * 256));
    
    FilterBuffers[i].dataBlockQueue = spscAllocate(DATABLOCK_QUEUE_SIZE);
    FilterBuffers[i].dataBlockSeq = (long *) calloc(1, sizeof(long) * 256);
    memset(FilterBuffers[i].dataBlockSeq, 0, (sizeof(long) * 256));
    FilterBuffers[i].dataBlockReader = createSpscReader(FilterBuffers[i].dataBlockQueue, DATABLOCK_QUEUE_SIZE, FilterBuffers[i].dataBlockSeq);
    
    FilterBuffers[i].tradeOutputQueue = spscAllocate(TRADEOUTPUT_QUEUE_SIZE);
    FilterBuffers[i].tradeOutputSeq = (long *) calloc(1, sizeof(long) * 256);
    memset(FilterBuffers[i].tradeOutputSeq, 0, (sizeof(long) * 256));
    FilterBuffers[i].tradeOutputReader = createSpscReader(FilterBuffers[i].tradeOutputQueue, TRADEOUTPUT_QUEUE_SIZE, FilterBuffers[i].tradeOutputSeq);
    
    FilterBuffers[i].flushTrigger = calloc(1, sizeof(short));
    *(FilterBuffers[i].flushTrigger) = (short)0;
    
    FilterBuffers[i].orderBucket = calloc(1, sizeof(t_OrderTokenBucket));
  }
}

/****************************************************************************
- Function name:  InitDataStructures
****************************************************************************/
int InitDataStructures(void)
{
  InitFilterStructures();
  TraceLog(DEBUG_LEVEL, "Initialize network filter data structures (done)\n");
  
  InitMonitorArcaBook();
  InitMonitorNasdaqBook();
  InitMonitorNdbxBook();
  InitMonitorBatszBook();
  InitMonitorBYXBook();
  InitMonitorNyseBook();
  InitMonitorAmexBook();
  InitMonitorEdgxBook();
  InitMonitorEdgaBook();
  InitMonitorPsxBook();
  InitMonitorCQS();
  InitMonitorUQDF();
  TraceLog(DEBUG_LEVEL, "Initialize data structures to monitor venue activity (done)\n");
  
  InitBookMgmtDataStructure();
  TraceLog(DEBUG_LEVEL, "Initialize book data structure (done)\n");
  
  InitStockSymbolDataStructure();
  TraceLog(DEBUG_LEVEL, "Initialize stock symbol data structure (done)\n");
  
  InitASDataStructure();
  TraceLog(DEBUG_LEVEL, "Initialize AS data structure (done)\n");
  
  InitializeIgnoreStockRangesList();
  TraceLog(DEBUG_LEVEL, "Initialize ignore-stock data structure (done)\n");
  
  InitOrderMgmtDataStructure();
  TraceLog(DEBUG_LEVEL, "Initialize order data structure (done)\n");
  
  InitTradingDataStructures();
  TraceLog(DEBUG_LEVEL, "Initialize trading data structure (done)\n");

  InitTradeOutputLogMgmt(); 
  
  InitBBOInfoMgmt();
  
  InitializeQuoteSymbolStructure();
  TraceLog(DEBUG_LEVEL, "Initialize bbo data structure (done)\n");
  
  InitializeExchangeMapping();
  
  InitializeExchangeStatusList();
  
  Init_ARCA_DIRECT_DataStructure();
  
  ProcessToInitTotalTimeOffset();
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  RegisterSignalHandlers
****************************************************************************/
int RegisterSignalHandlers(void)
{
  signal(SIGPIPE, SIG_IGN);
  
  struct sigaction actInt;
  memset (&actInt, 0, sizeof(actInt));
  actInt.sa_sigaction = &Process_INTERRUPT_Signal;
  sigfillset(&actInt.sa_mask);
  actInt.sa_flags = SA_SIGINFO;
  
  if (sigaction(SIGINT, &actInt, NULL) < 0)
  {
    PrintErrStr(ERROR_LEVEL, "Could not register signal handler for SIGINT: ", errno);
  }

  if (sigaction(SIGTERM, &actInt, NULL) < 0)
  {
    PrintErrStr(ERROR_LEVEL, "Could not register signal handler for SIGTERM: ", errno);
  }
  
  // Segmentation fault handling:
  struct sigaction actSegv;
  memset (&actSegv, 0, sizeof(actSegv));
  actSegv.sa_sigaction = &Process_SEGV_Signal;
  sigfillset(&actSegv.sa_mask);
  actSegv.sa_flags = SA_SIGINFO;
  if (sigaction(SIGSEGV, &actSegv, NULL) < 0)
  {
    PrintErrStr(ERROR_LEVEL, "Could not register signal handler for SIGSEGV: ", errno);
  }
  
  return SUCCESS;
}

int StartCommandLineTool()
{
  sleep(2);
  _consumerId = DBLFILTER_NUM_QUEUES;
  FilterBuffers[_consumerId].orderEntrySendWriter = createSpscWriter(FilterBuffers[_consumerId].orderEntrySendQueue, DBLFILTER_OE_QUEUE_SIZE, FilterBuffers[_consumerId].orderEntrySendSeq);
  
  printf("\n*******************************************************************************\n");
  printf("Everything is OK. Begin handling user command line ...\n");
  
  int isStop = 0;
  while(isStop == 0)
  {
    TuiMain(&isStop, "$ ");
  }
  
  printf("End of handling user command line!\n");
  printf("*******************************************************************************\n");
  _consumerId = -1;
  return 0;
}

/****************************************************************************
- Function name:  StartUpMainThreads
- Input:      N/A
- Output:     N/A
- Return:     Failure
- Description:    + The routine hides the following facts
                    - Create and start main threads of the program
                    - The program will be blocked at the routine if all threads
                    were created successfully otherwise it will be returned
                  + There are no preconditions guaranteed to the routine
                  + The routine guarantees that the return value will 
                    have a value failure if it has any failures 
- Usage:      The routine will be called by main routine
****************************************************************************/
int StartUpMainThreads(void)
{
  // Create and start other threads here
  pthread_t saveTradingInfoThread;
  pthread_create(&saveTradingInfoThread, NULL, (void *)&Thread_SaveTradingInformation, NULL);
  
  pthread_t saveDataBlockAndSeqNumToFileThread;
  pthread_create(&saveDataBlockAndSeqNumToFileThread, NULL, (void *)Thread_SaveDataBlockAndSeqNumToFile, NULL);
  
  pthread_t monitorThread;
  pthread_create(&monitorThread, NULL, (void *)Thread_VenueActivityMonitoring, NULL);
  
  pthread_t dataBlockAggregatorThread;
  pthread_create(&dataBlockAggregatorThread, NULL, (void *)StartDataBlockAggregationThread, NULL);
  
  pthread_t tradeOutputAggregatorThread;
  pthread_create(&tradeOutputAggregatorThread, NULL, (void *)StartTradeOutputAggregationThread, NULL);

  pthread_t marketDataLogConsumerThread;
  pthread_create(&marketDataLogConsumerThread, NULL, (void *)StartMarketDataLogConsumerThread, NULL);

  pthread_t marketDataReadThread;
  pthread_create(&marketDataReadThread, NULL, (void *)StartMarketDataReadThread, NULL);
  
  pthread_t orderEntryLogConsumerThread;
  pthread_create(&orderEntryLogConsumerThread, NULL, (void *)StartOrderEntryLogConsumerThread, NULL);
                         
  pthread_t orderEntryAdminReadThread;
  pthread_create(&orderEntryAdminReadThread, NULL, (void *)StartOrderEntryAdminConsumerThread, NULL);
  
  pthread_t orderEntryWriteThread;
  pthread_create(&orderEntryWriteThread, NULL, (void *)StartOrderEntryWriteThread, NULL);
  
  pthread_t orderEntryReadThread;
  pthread_create(&orderEntryReadThread, NULL, (void *)StartOrderEntryReadThread, NULL);
  
  if (HasValidOption == YES)
  {
    pthread_t threadProcessUserInput;
    
    TraceLog(DEBUG_LEVEL, "Begin to start program with options\n");
    pthread_create(&threadProcessUserInput, NULL,  (void*) &Thread_StartProgramWithOptions, NULL);
  }
  
  if(ManualMode == YES) 
  {
    StartCommandLineTool();
  }
  else
  {
      // Create and start thread to communicate with Aggregation Server
      pthread_t asThread;
    pthread_create(&asThread, NULL, (void *)&Thread_WaitForASConnection, (void *)&ASConfig);
    
    // Wait for thread execution completion
    pthread_join(asThread, NULL);
  }
      
  return SUCCESS;
}

/****************************************************************************
- Function name:  Process_BROKEN_PIPE_Signal
****************************************************************************/
void Process_BROKEN_PIPE_Signal(int sig_num)
{
  TraceLog(DEBUG_LEVEL, "Processing Broken Pipe Signal \n");
  
  close(ASConfig.socket);
    
  signal(SIGPIPE, Process_BROKEN_PIPE_Signal);
    
  DestroyAllLinkedListDataStructure();

  exit(0);
}

/****************************************************************************
- Function name:  Process_INTERRUPT_Signal
****************************************************************************/
void Process_INTERRUPT_Signal(int signalno, siginfo_t *siginfo, void *context)
{
  TraceLog(DEBUG_LEVEL, "Processing signal %d (reference: SIGINT (%d), SIGTERM (%d))\n", signalno, SIGINT, SIGTERM);
  IsTradeServerStopped = 1;
  
  //------------------------------------------------------------------
  //        DISCONNECT BOOKS
  //------------------------------------------------------------------
  
  DisconnectFromVenue(BOOK_ARCA, -1);
  DisconnectFromVenue(BOOK_NASDAQ, -1);
  DisconnectFromVenue(BOOK_NDBX, -1);
  DisconnectFromVenue(BOOK_PSX, -1);
  DisconnectFromVenue(BOOK_NYSE, -1);
  DisconnectFromVenue(BOOK_AMEX, -1);
  DisconnectFromVenue(BOOK_EDGX, -1);
  DisconnectFromVenue(BOOK_EDGA, -1);
  DisconnectFromVenue(BOOK_BATSZ, -1);
  DisconnectFromVenue(BOOK_BYX, -1);
  DisconnectFromVenue(FEED_CQS, -1);
  DisconnectFromVenue(FEED_UQDF, -1);
  
  sleep(2);
  
  //------------------------------------------------------------------
  //        SAVE INCOMING & OUTGOING SEQUENCE NUMBERS TO FILES
  //------------------------------------------------------------------
  if (OrderStatusMgmt[ORDER_ARCA_DIRECT].isConnected == CONNECTED)
  {
    if (SaveSeqNumToFile(FN_SEQUENCE_NUM_ARCA, ORDER_ARCA_DIRECT) == ERROR)
    {
      TraceLog(ERROR_LEVEL, "Could not save sequence number of 'ARCA DIRECT' to file\n");
    }
  }

  if (OrderStatusMgmt[ORDER_NYSE_CCG].isConnected == CONNECTED)
  {
    if (SaveSeqNumToFile(FN_SEQUENCE_NUM_NYSE, ORDER_NYSE_CCG) == ERROR)
    {
      TraceLog(ERROR_LEVEL, "Could not save sequence number of 'NYSE CCG' to file\n");
    }
  }
  
  if (OrderStatusMgmt[ORDER_BATSZ_BOE].isConnected == CONNECTED)
  {
    if (SaveSeqNumToFile(FN_SEQUENCE_NUM_BATSZ, ORDER_BATSZ_BOE) == ERROR)
    {
      TraceLog(ERROR_LEVEL, "Could not save sequence number of 'BATS BOE' to file\n");
    }
  }
  
  if (OrderStatusMgmt[ORDER_BYX_BOE].isConnected == CONNECTED)
  {
    if (SaveSeqNumToFile(FN_SEQUENCE_NUM_BYX, ORDER_BYX_BOE) == ERROR)
    {
      TraceLog(ERROR_LEVEL, "Could not save sequence number of 'BYX BOE' to file\n");
    }
  }

  if (OrderStatusMgmt[ORDER_NASDAQ_OUCH].isConnected == CONNECTED)
  {
    if (SaveSeqNumToFile(FN_SEQUENCE_NUM_OUCH, ORDER_NASDAQ_OUCH) == ERROR)
    {
      TraceLog(ERROR_LEVEL, "Could not save sequence number of 'OUCH' to file\n");
    }
  }
      
  if (OrderStatusMgmt[ORDER_NASDAQ_BX].isConnected == CONNECTED)
  {
    if (SaveSeqNumToFile(FN_SEQUENCE_NUM_BX, ORDER_NASDAQ_BX) == ERROR)
    {
      TraceLog(ERROR_LEVEL, "Could not save sequence number of 'BX' to file\n");
    }
  }
  
  if (OrderStatusMgmt[ORDER_NASDAQ_PSX].isConnected == CONNECTED)
  {
    if (SaveSeqNumToFile(FN_SEQUENCE_NUM_PSX, ORDER_NASDAQ_PSX) == ERROR)
    {
      TraceLog(ERROR_LEVEL, "Could not save sequence number of 'PSX' to file\n");
    }
  }
      
  if (OrderStatusMgmt[ORDER_NASDAQ_RASH].isConnected == CONNECTED)
  {
    if (SaveSeqNumToFile(FN_SEQUENCE_NUM_RASH, ORDER_NASDAQ_RASH) == ERROR)
    {
      TraceLog(ERROR_LEVEL, "Could not save sequence number of 'RASH' to file\n");
    }
  }
  
  if (OrderStatusMgmt[ORDER_EDGX].isConnected == CONNECTED)
  {
    if (SaveSeqNumToFile(FN_SEQUENCE_NUM_EDGX, ORDER_EDGX) == ERROR)
    {
      TraceLog(ERROR_LEVEL, "Could not save sequence number of 'EDGX' to file\n");
    }
  }
  
  if (OrderStatusMgmt[ORDER_EDGA].isConnected == CONNECTED)
  {
    if (SaveSeqNumToFile(FN_SEQUENCE_NUM_EDGA, ORDER_EDGA) == ERROR)
    {
      TraceLog(ERROR_LEVEL, "Could not save sequence number of 'EDGA' to file\n");
    }
  }
  
  //------------------------------------------------------------------
  //        SAVE RAWDATA TO FILES
  //------------------------------------------------------------------
  int i;
  for (i = 0; i < MAX_ECN_ORDER; i++)
  {
    SaveDataBlockToFile(&OrderMgmt[i]);
  }
  
  //------------------------------------------------------------------
  //        SAVE TRADE OUTPUT LOG TO FILE
  //------------------------------------------------------------------
  SaveTradeOutputLogToFile();

  //------------------------------------------------------------------
  //        SAVE CURRENT ORDER_ID AND CROSS_ID VALUES TO FILE
  //------------------------------------------------------------------
  SaveOrderIDAndCrossIDFromFile("orderID_crossID", CurrentOrderID, CurrentCrossID);
  
  //------------------------------------------------------------------
  //        SAVE BBO DATA TO FILE
  //------------------------------------------------------------------
  SaveBBOInfoToFile();
  
  close(ASConfig.socket);

  exit(0);  //This code also auto flushes all opening streams
}

/****************************************************************************
- Function name:  DumpCurrentStackFrames
- Input:      N/A
- Output:     N/A
- Return:     N/A
- Description:    Show information about current stack frames
- Usage:      N/A
****************************************************************************/
void DumpCurrentStackFrames()
{
  void *bt[80];
  size_t btsize;

  // get void*'s for all entries on the stack
  btsize = backtrace(bt, 80);

  fprintf(stderr, "\nSTACK BACKTRACKING INFORMATION:\n");

  backtrace_symbols_fd(bt, btsize, 2);

}

/****************************************************************************
- Function name:  ShowFaultInfo
- Input:          siginfo_t *, ucontext_t*
- Description:    Show information about segmentation fault
****************************************************************************/
void ShowFaultInfo(siginfo_t *siginfo, ucontext_t *context)
{
  fprintf(stderr, "********FAULT INFORMATION********\n");
  
  fprintf(stderr, "si_signo: %d (%s)\n", siginfo->si_signo, strsignal(siginfo->si_signo));
  
  fprintf(stderr, "si_errno: %d\n", siginfo->si_errno);

  fprintf(stderr, "si_code (Cause of fault): %d ", siginfo->si_code); 
  switch (siginfo->si_code)
  {
    case SI_USER:
      fprintf(stderr, "(SI_USER) -> Caused by kill(), sigsend(), or raise()\n");
      break;
    case SI_KERNEL:
      fprintf(stderr, "(SI_KERNEL) -> The kernel\n");
      break;
    case SI_QUEUE:
      fprintf(stderr, "(SI_QUEUE) -> sigqueue() \n");
      break;
    case SI_TIMER:
      fprintf(stderr, "(SI_TIMER) -> POSIX timer expired\n");
      break;
    case SI_MESGQ:
      fprintf(stderr, "(SI_MESGQ) -> POSIX message queue state changed (since Linux 2.6.6)\n");
      break;
    case SI_ASYNCIO:
      fprintf(stderr, "(SI_ASYNCIO) ->  AIO completed\n");
      break;
    case SI_SIGIO:
      fprintf(stderr, "(SI_SIGIO) -> queued SIGIO \n");
      break;
    case SI_TKILL:
      fprintf(stderr, "(SI_TKILL) -> tkill() or tgkill() (since Linux 2.4.19) \n");
      break;
    case SEGV_MAPERR:
      fprintf(stderr, "(SEGV_MAPERR) -> address not mapped to object \n");
      break;

    case SEGV_ACCERR:
      fprintf(stderr, "(SEGV_ACCERR) -> invalid permissions for mapped object \n");
      break;
    default:
      fprintf(stderr, "(Unknown si_error!)\n");
  }
  
  fprintf(stderr, "si_addr (Memory location which caused fault): %p\n", siginfo->si_addr);
  
#if __WORDSIZE == 64
  
  fprintf(stderr, "RIP (IP register): %p\n", (void *) context->uc_mcontext.gregs[REG_RIP]);
  fprintf(stderr, "RSP (SP register): %p\n", (void *) context->uc_mcontext.gregs[REG_RSP]);

#else

  fprintf(stderr, "EIP (IP register): %p\n", (void *) context->uc_mcontext.gregs[REG_EIP]);
  fprintf(stderr, "ESP (SP register): %p\n", (void *) context->uc_mcontext.gregs[REG_ESP]);
  
#endif

  DumpCurrentStackFrames();
  
  fprintf(stderr, "*********************************\n");
}

/****************************************************************************
- Function name:  Process_SEGV_Signal
****************************************************************************/
void Process_SEGV_Signal(int signalno, siginfo_t *siginfo, void *context)
{
  IsTradeServerStopped = 1;
  ShowFaultInfo(siginfo, (ucontext_t *)context);
  DisconnectFromVenue(BOOK_ARCA, -1);
  DisconnectFromVenue(BOOK_NASDAQ, -1);
  DisconnectFromVenue(BOOK_NDBX, -1);
  DisconnectFromVenue(BOOK_PSX, -1);
  DisconnectFromVenue(BOOK_NYSE, -1);
  DisconnectFromVenue(BOOK_AMEX, -1);
  DisconnectFromVenue(BOOK_EDGX, -1);
  DisconnectFromVenue(BOOK_EDGA, -1);
  DisconnectFromVenue(BOOK_BATSZ, -1);
  DisconnectFromVenue(BOOK_BYX, -1);
  DisconnectFromVenue(FEED_CQS, -1);
  DisconnectFromVenue(FEED_UQDF, -1);
  sleep(2); //Sleep for other threads to finish
  
  //------------------------------------------------------------------
  //        SAVE INCOMING & OUTGOING SEQUENCE NUMBERS TO FILES
  //------------------------------------------------------------------
  if (OrderStatusMgmt[ORDER_ARCA_DIRECT].isConnected == CONNECTED)
  {
    if (SaveSeqNumToFile(FN_SEQUENCE_NUM_ARCA, ORDER_ARCA_DIRECT) == ERROR)
    {
      TraceLog(ERROR_LEVEL, "Could not save sequence number of 'ARCA DIRECT' to file\n");
    }
  }

  if (OrderStatusMgmt[ORDER_NYSE_CCG].isConnected == CONNECTED)
  {
    if (SaveSeqNumToFile(FN_SEQUENCE_NUM_NYSE, ORDER_NYSE_CCG) == ERROR)
    {
      TraceLog(ERROR_LEVEL, "Could not save sequence number of 'NYSE CCG' to file\n");
    }
  }
  
  if (OrderStatusMgmt[ORDER_BATSZ_BOE].isConnected == CONNECTED)
  {
    if (SaveSeqNumToFile(FN_SEQUENCE_NUM_BATSZ, ORDER_BATSZ_BOE) == ERROR)
    {
      TraceLog(ERROR_LEVEL, "Could not save sequence number of 'BATSZ BOE' to file\n");
    }
  }
  
  if (OrderStatusMgmt[ORDER_BYX_BOE].isConnected == CONNECTED)
  {
    if (SaveSeqNumToFile(FN_SEQUENCE_NUM_BYX, ORDER_BYX_BOE) == ERROR)
    {
      TraceLog(ERROR_LEVEL, "Could not save sequence number of 'BYX BOE' to file\n");
    }
  }
  
  if (OrderStatusMgmt[ORDER_NASDAQ_OUCH].isConnected == CONNECTED)
  {
    if (SaveSeqNumToFile(FN_SEQUENCE_NUM_OUCH, ORDER_NASDAQ_OUCH) == ERROR)
    {
      TraceLog(ERROR_LEVEL, "Could not save sequence number of 'OUCH' to file\n");
    }
  }
      
  if (OrderStatusMgmt[ORDER_NASDAQ_BX].isConnected == CONNECTED)
  {
    if (SaveSeqNumToFile(FN_SEQUENCE_NUM_BX, ORDER_NASDAQ_BX) == ERROR)
    {
      TraceLog(ERROR_LEVEL, "Could not save sequence number of 'BX' to file\n");
    }
  }
  
  if (OrderStatusMgmt[ORDER_NASDAQ_PSX].isConnected == CONNECTED)
  {
    if (SaveSeqNumToFile(FN_SEQUENCE_NUM_PSX, ORDER_NASDAQ_PSX) == ERROR)
    {
      TraceLog(ERROR_LEVEL, "Could not save sequence number of 'PSX' to file\n");
    }
  }
  
  if (OrderStatusMgmt[ORDER_NASDAQ_RASH].isConnected == CONNECTED)
  {
    if (SaveSeqNumToFile(FN_SEQUENCE_NUM_RASH, ORDER_NASDAQ_RASH) == ERROR)
    {
      TraceLog(ERROR_LEVEL, "Could not save sequence number of 'RASH' to file\n");
    }
  }
  
  if (OrderStatusMgmt[ORDER_EDGX].isConnected == CONNECTED)
  {
    if (SaveSeqNumToFile(FN_SEQUENCE_NUM_EDGX, ORDER_EDGX) == ERROR)
    {
      TraceLog(ERROR_LEVEL, "Could not save sequence number of 'EDGX' to file\n");
    }
  }
  
  if (OrderStatusMgmt[ORDER_EDGA].isConnected == CONNECTED)
  {
    if (SaveSeqNumToFile(FN_SEQUENCE_NUM_EDGA, ORDER_EDGA) == ERROR)
    {
      TraceLog(ERROR_LEVEL, "Could not save sequence number of 'EDGA' to file\n");
    }
  }
  
  //------------------------------------------------------------------
  //        SAVE RAWDATA TO FILES
  //------------------------------------------------------------------
  int i;
  for (i = 0; i < MAX_ECN_ORDER; i++)
  {
    SaveDataBlockToFile(&OrderMgmt[i]);
  } 
  
  //------------------------------------------------------------------
  //        SAVE TRADE OUTPUT LOG TO FILE
  //------------------------------------------------------------------
  SaveTradeOutputLogToFile();

  //------------------------------------------------------------------
  //        SAVE CURRENT ORDER_ID AND CROSS_ID VALUES TO FILE
  //------------------------------------------------------------------
  SaveOrderIDAndCrossIDFromFile("orderID_crossID", CurrentOrderID, CurrentCrossID);

  //------------------------------------------------------------------
  //        SAVE BBO DATA TO FILE
  //------------------------------------------------------------------
  SaveBBOInfoToFile();
  
  fflush(NULL); //Flush all opening stream in process
  
    close(ASConfig.socket);
  
    signal(SIGSEGV, SIG_DFL);
}

/****************************************************************************
- Function name:  print_Help
****************************************************************************/
void PrintHelp()
{
  int cnt = 1;
  TraceLog(USAGE_LEVEL, "%3d. -h --help                           Display this help\n", cnt++);
  TraceLog(USAGE_LEVEL, "%3d. -A --arca-book [0..23]:[0..59]      Schedule time to start ARCA BOOK\n", cnt++);
  TraceLog(USAGE_LEVEL, "%3d. -N --ndaq-book [0..23]:[0..59]      Schedule time to start NASDAQ BOOK\n", cnt++);
  TraceLog(USAGE_LEVEL, "%3d. -B --bx-book [0..23]:[0..59]        Schedule time to start BX BOOK\n", cnt++);
  TraceLog(USAGE_LEVEL, "%3d. -P --psx-book [0..23]:[0..59]       Schedule time to start PSX BOOK\n", cnt++);
  TraceLog(USAGE_LEVEL, "%3d. -O --nyse-book [0..23]:[0..59]      Schedule time to start NYSE BOOK\n", cnt++);
  TraceLog(USAGE_LEVEL, "%3d. -X --amex-book [0..23]:[0..59]      Schedule time to start AMEX BOOK\n", cnt++);
  TraceLog(USAGE_LEVEL, "%3d. -Z --batsz-book [0..23]:[0..59]     Schedule time to start BATS-Z BOOK\n", cnt++);
  TraceLog(USAGE_LEVEL, "%3d. -Y --byx-book [0..23]:[0..59]       Schedule time to start BYX BOOK\n", cnt++);
  TraceLog(USAGE_LEVEL, "%3d. -E --edgx-book [0..23]:[0..59]      Schedule time to start EDGX BOOK\n", cnt++);
  TraceLog(USAGE_LEVEL, "%3d. -F --edga-book [0..23]:[0..59]      Schedule time to start EDGA BOOK\n", cnt++);
  TraceLog(USAGE_LEVEL, "%3d. -C --cqs [0..23]:[0..59]            Schedule time to start CQS\n", cnt++);
  TraceLog(USAGE_LEVEL, "%3d. -U --uqdf [0..23]:[0..59]           Schedule time to start UQDF\n", cnt++);

  TraceLog(USAGE_LEVEL, "%3d. -d --arca-direct [0..23]:[0..59]    Schedule time to start ARCA Direct\n", cnt++);
  TraceLog(USAGE_LEVEL, "%3d. -o --ndaq-ouch [0..23]:[0..59]      Schedule time to start NASDAQ\n", cnt++);
  TraceLog(USAGE_LEVEL, "%3d. -r --ndaq-rash [0..23]:[0..59]      Schedule time to start RASH\n", cnt++);
  TraceLog(USAGE_LEVEL, "%3d. -s --ndaq-psx  [0..23]:[0..59]      Schedule time to start PSX\n", cnt++);
  TraceLog(USAGE_LEVEL, "%3d. -n --nyse-ccg [0..23]:[0..59]       Schedule time to start NYSE CCG\n", cnt++);
  TraceLog(USAGE_LEVEL, "%3d. -z --batsz-boe [0..23]:[0..59]      Schedule time to start BATSZ-BOE\n", cnt++);
  TraceLog(USAGE_LEVEL, "%3d. -y --byx-boe [0..23]:[0..59]        Schedule time to start BYX-BOE\n", cnt++);
  TraceLog(USAGE_LEVEL, "%3d. -e --edgx [0..23]:[0..59]           Schedule time to start EDGX\n", cnt++);
  TraceLog(USAGE_LEVEL, "%3d. -f --edga [0..23]:[0..59]           Schedule time to start EDGA\n", cnt++);
  TraceLog(USAGE_LEVEL, "%3d. -b --nasdaq-bx [0..23]:[0..59]      Schedule time to start NASDAQ BX OUCH\n", cnt++);

  TraceLog(USAGE_LEVEL, "%3d. --all  [0..23]:[0..59]              Schedule time to start all ECN Books and ECN Orders\n", cnt++);
  TraceLog(USAGE_LEVEL, "%3d. --all-book [0..23]:[0..59]          Schedule time to start all ECN Books\n", cnt++);
  TraceLog(USAGE_LEVEL, "%3d. --all-order [0..23]:[0..59]         Schedule time to start all ECN Orders\n", cnt++);
  
  TraceLog(USAGE_LEVEL, "%3d. -G --no-gap-request                 Do not connect to retransmission server (all books), if gap is detected, flush book\n", cnt++);

  TraceLog(USAGE_LEVEL, "%3d. -p --as-listen-port <Port>          Port to connect to AS\n", cnt++);
  TraceLog(USAGE_LEVEL, "%3d. -a --trading_account <account>      Trading account is served for trading\n", cnt++);
  
  TraceLog(USAGE_LEVEL, "%3d. -M --manual                         Start trading server in manual mode\n", cnt++);
  
  TraceLog(USAGE_LEVEL, "%3d. -v --version                        Display TS version and exit\n\n", cnt++);
  
  TraceLog(USAGE_LEVEL, "%3d. -c --oec-config [orderentry::<OEC Type>\n", cnt++);
  TraceLog(USAGE_LEVEL, "                   venue=<Venue Name>\n");
  TraceLog(USAGE_LEVEL, "                   port=[<Port Type> address=<IP>:<Port>]\n");
  TraceLog(USAGE_LEVEL, "                   username/sender_comp_id=<Username>\n");
  TraceLog(USAGE_LEVEL, "                   password=<Password>\n");
  TraceLog(USAGE_LEVEL, "                   comp_id=<Comp Group ID>\n");
  TraceLog(USAGE_LEVEL, "                   sender_sub_id=<Sender Sub ID>\n");
  TraceLog(USAGE_LEVEL, "                   cancel_on_disconnect=<Integer Value (0 or 1)>\n");
  TraceLog(USAGE_LEVEL, "                   no_unspecified_unit_replay=<Integer Value (0 or 1)>]\n");
  
  TraceLog(USAGE_LEVEL, "  Notes:\n");
  TraceLog(USAGE_LEVEL, "    + Possible values for <OEC Type>:\n");
  TraceLog(USAGE_LEVEL, "        OEARCADIRECT                    ARCA DIRECT\n");
  TraceLog(USAGE_LEVEL, "        OENASDAQRASH                    NASDAQ RASH\n");
  TraceLog(USAGE_LEVEL, "        OENASDAQOUCH                    NASDAQ OUCH\n");
  TraceLog(USAGE_LEVEL, "        OENYSECCG                       NYSE CCG\n");
  TraceLog(USAGE_LEVEL, "        OEBATSBOE                       BATSZ BOE\n");
  TraceLog(USAGE_LEVEL, "        OEEDGX                          EDGX\n");
  TraceLog(USAGE_LEVEL, "        OEEDGA                          EDGA\n");
  TraceLog(USAGE_LEVEL, "        OENASDAQBX                      NASDAQ BX OUCH\n");
  TraceLog(USAGE_LEVEL, "        OENASDAQPSX                     NASDAQ PSX OUCH\n");
  TraceLog(USAGE_LEVEL, "        OEBYXBOE                        BYX BOE\n");
  TraceLog(USAGE_LEVEL, "    + Possible values for <Venue Name>: arca, rash, isld, nyse, bats\n");
  TraceLog(USAGE_LEVEL, "    + Possible values for <Port Type>:\n");
  TraceLog(USAGE_LEVEL, "        TcpPort                         This uses for ARCA DIRECT, RASH, CCG, BATSZ BOE, EDGX\n");
  TraceLog(USAGE_LEVEL, "        UdpPort                         This uses for NASDAQ OUCH\n");
  TraceLog(USAGE_LEVEL, "    + Other keywords explanation:\n");
  TraceLog(USAGE_LEVEL, "        address                         Pair of IP & port, example: 159.125.67.184:52205\n");
  TraceLog(USAGE_LEVEL, "        username                        Username used to login to OEC (not used for NYSE CCG)\n");
  TraceLog(USAGE_LEVEL, "        sender_comp_id                  This is equivalent to username, used for NYSE CCG only\n");
  TraceLog(USAGE_LEVEL, "        password                        Password used to login to OEC\n");
  TraceLog(USAGE_LEVEL, "        comp_id                         Comp Group ID (for ARCA DIRECT only)\n");
  TraceLog(USAGE_LEVEL, "        sender_sub_id                   Sender Sub ID (for BATSZ BOE only)\n");
  TraceLog(USAGE_LEVEL, "        dbl_nic                         DBL Interface used for OUCH connection (for NASDAQ OUCH only)\n");
  TraceLog(USAGE_LEVEL, "        cancel_on_disconnect            Auto Cancel On Disconnect, value is either 1 or 0, used for ARCA DIRECT and NYSE CCG only\n");
  TraceLog(USAGE_LEVEL, "        no_unspecified_unit_replay      No Unspecified Unit Replay, value is either 1 or 0 (initial: 1), used for BATSZ BOE only\n");
  
  TraceLog(USAGE_LEVEL, "%3d. -m --mdc-config=\"[marketdata::<MDC Type>\n", cnt++);
  TraceLog(USAGE_LEVEL, "                   venue=<Venue Name>\n");
  TraceLog(USAGE_LEVEL, "                   port=[<Port Type> <address_type>=<IP>:<Port>\n");
  TraceLog(USAGE_LEVEL, "                         <Port Type> <address_type>=<IP>:<Port>\n");
  TraceLog(USAGE_LEVEL, "                         ...\n");
  TraceLog(USAGE_LEVEL, "                         <Port Type> <address_type>=<IP>:<Port>]\n");
  TraceLog(USAGE_LEVEL, "                   dbl_nic=<DBL Interface for both Data Feed and Recovery Feed (*use this for all mdcs except NASDAQ)>\n");
  TraceLog(USAGE_LEVEL, "                   dbl_nic_data=<DBL Interface for Data Feed only (*use this for NASDAQ only)>\n");
  TraceLog(USAGE_LEVEL, "                   dbl_nic_recovery=<DBL Interface for Recovery Feed only (*use this for NASDAQ only)>\n");
  TraceLog(USAGE_LEVEL, "                   backup_nic=<DBL Interface for backup feeds>\n");
  TraceLog(USAGE_LEVEL, "                   source_id=<Source ID>\n");
  TraceLog(USAGE_LEVEL, "                   session_id=<Session ID>\n");
  TraceLog(USAGE_LEVEL, "                   max_msg_loss=<Maximum Lost Messages>\n");
  TraceLog(USAGE_LEVEL, "                   username=<Username>\n");
  TraceLog(USAGE_LEVEL, "                   password=<Password>\n");
  TraceLog(USAGE_LEVEL, "                   retrans_username=<Retransmission Username>\n");
  TraceLog(USAGE_LEVEL, "                   retrans_password=<Retransmission Password>]\"\n");
  
  TraceLog(USAGE_LEVEL, "  Notes:\n");
  TraceLog(USAGE_LEVEL, "    + Possible values for <MDC Type>:\n");
  TraceLog(USAGE_LEVEL, "        MDArcaXDP                       ARCA Book\n");
  TraceLog(USAGE_LEVEL, "        MDNasdaqITCH                    NASDAQ Book\n");
  TraceLog(USAGE_LEVEL, "        MDNdbxITCH                      NASDAQ BX Book\n");
  TraceLog(USAGE_LEVEL, "        MDPsxITCH                       PSX Book\n");
  TraceLog(USAGE_LEVEL, "        MDNYSEXDP                       NYSE Book\n");
  TraceLog(USAGE_LEVEL, "        MDAMEXXDP                       AMEX Book\n");
  TraceLog(USAGE_LEVEL, "        MDBatsPitch                     BATSZ Book\n");
  TraceLog(USAGE_LEVEL, "        MDBYXPitch                      BYX Book\n");
  TraceLog(USAGE_LEVEL, "        MDEDGE                          EDGX Book\n");
  TraceLog(USAGE_LEVEL, "        MDEDGA                          EDGA Book\n");
  TraceLog(USAGE_LEVEL, "        MDCQS                           CQS Feed\n");
  TraceLog(USAGE_LEVEL, "        MDUQDF                          UQDF Feed\n");
  TraceLog(USAGE_LEVEL, "    + Possible values for <Venue Name>: arca, nasdaq, nyse, batsz, edgx, cqs, uqdf\n");
  TraceLog(USAGE_LEVEL, "    + Possible values for <Port Type>:\n");
  TraceLog(USAGE_LEVEL, "        TcpPort                         To indicate it's a TCP connection\n");
  TraceLog(USAGE_LEVEL, "        UdpPort                         To indicate it's a UDP connection\n");
  TraceLog(USAGE_LEVEL, "    + Possible values for <address_type>:\n");
  TraceLog(USAGE_LEVEL, "        retrans_address\n");
  TraceLog(USAGE_LEVEL, "        grp_address\n");
  TraceLog(USAGE_LEVEL, "        group1_retrans_address, group2_retrans_address, ... , group20_retrans_address\n");
  TraceLog(USAGE_LEVEL, "        group1_primary_mc_address, group2_primary_mc_address, ... , group32_primary_mc_address\n");
  TraceLog(USAGE_LEVEL, "        group1_backup_mc_address, group2_backup_mc_address, ... , group32_backup_mc_address\n");
  TraceLog(USAGE_LEVEL, "        group1_retrans_mc_address, group2_retrans_mc_address, ... , group32_retrans_mc_address\n");
  TraceLog(USAGE_LEVEL, "        group1_e_mc_address, group2_e_mc_address, ... , group12_e_mc_address\n");
  TraceLog(USAGE_LEVEL, "        group1_f_mc_address, group2_f_mc_address, ... , group12_f_mc_address\n");
  TraceLog(USAGE_LEVEL, "    + Other keywords explanation:\n");
  TraceLog(USAGE_LEVEL, "        dbl_nic                         DBL Interface used for all mdcs except NASDAQ! Do not use this for NASDAQ config\n");
  TraceLog(USAGE_LEVEL, "        dbl_nic_data                    DBL Interface used for NASDAQ data feed\n");
  TraceLog(USAGE_LEVEL, "        dbl_nic_recovery                DBL Interface used for NASDAQ recovery feed\n");
  TraceLog(USAGE_LEVEL, "        backup_nic                      DBL Interface used for backup feeds\n");
  TraceLog(USAGE_LEVEL, "        source_id                       Source ID to login Retransmission Request (for ARCA & NYSE)\n");
  TraceLog(USAGE_LEVEL, "        session_id                      Session Sub ID to request Retransmission of Lost Messages (for BATS-Z only)\n");
  TraceLog(USAGE_LEVEL, "        max_msg_loss                    Maximum lost message to request Retransmission (for NASDAQ only)\n");
  TraceLog(USAGE_LEVEL, "        username                        Username to request Retransmission of Lost Messages (for BATS-Z only)\n");
  TraceLog(USAGE_LEVEL, "        password                        Password to request Retransmission of Lost Messages (for BATS-Z only)\n");
  TraceLog(USAGE_LEVEL, "        retrans_username                Username to login Retransmission Request (for EDGX only)\n");
  TraceLog(USAGE_LEVEL, "        retrans_password                Password to login Retransmission Request (for EDGX only)\n");
  
  TraceLog(USAGE_LEVEL, "  *** Examples: \n");
  TraceLog(USAGE_LEVEL, "  1. To schedule time to start ARCA/NDAQ BOOK, with ARCA at 6:30 AM, NDAQ at 6:50 AM:\n");
  TraceLog(USAGE_LEVEL, "        $./trade_server -A 06:30 -N 06:50\n");
  TraceLog(USAGE_LEVEL, "  2. To set ARCA DIRECT configuration:\n");
  TraceLog(USAGE_LEVEL, "        $./trade_server --oec-config=\"[orderentry::OEARCADIRECT\n");
  TraceLog(USAGE_LEVEL, "                                        venue=arca\n");
  TraceLog(USAGE_LEVEL, "                                        port=[TcpPort address=159.125.67.184:52205]\n");
  TraceLog(USAGE_LEVEL, "                                        username=PFS23\n");
  TraceLog(USAGE_LEVEL, "                                        password=ETCD\n");
  TraceLog(USAGE_LEVEL, "                                        comp_id=HBIWB\n");
  TraceLog(USAGE_LEVEL, "                                        cancel_on_disconnect=1]\"\n");
  TraceLog(USAGE_LEVEL, "  3. To set ARCA Book configuration (with one backup feed):\n");
  TraceLog(USAGE_LEVEL, "        $./trade_server --mdc-config=\"[marketdata::MDArcaXDP\n");
  TraceLog(USAGE_LEVEL, "                                        venue=arca\n");
  TraceLog(USAGE_LEVEL, "                                        dbl_nic=vlan894\n");
  TraceLog(USAGE_LEVEL, "                                        backup_nic=vlan892\n");
  TraceLog(USAGE_LEVEL, "                                        source_id=blnkxdp1\n");
  TraceLog(USAGE_LEVEL, "                                        port=[TcpPort retrans_address=159.125.67.12:1069\n");
  TraceLog(USAGE_LEVEL, "                                              UdpPort group1_primary_mc_address=239.10.1.0:11110\n");
  TraceLog(USAGE_LEVEL, "                                              UdpPort group1_backup_mc_address=239.10.2.0:21110\n");
  TraceLog(USAGE_LEVEL, "                                              UdpPort group1_retrans_mc_address=224.0.59.68:11068\n");
  TraceLog(USAGE_LEVEL, "                                              UdpPort group2_primary_mc_address=239.10.1.1:11111\n");
  TraceLog(USAGE_LEVEL, "                                              UdpPort group2_retrans_mc_address=224.0.59.69:11069\n");
  TraceLog(USAGE_LEVEL, "                                              UdpPort group3_primary_mc_address=239.10.1.2:11112\n");
  TraceLog(USAGE_LEVEL, "                                              UdpPort group3_retrans_mc_address=224.0.59.70:11070\n");
  TraceLog(USAGE_LEVEL, "                                              UdpPort group4_primary_mc_address=239.10.1.3:11113\n");
  TraceLog(USAGE_LEVEL, "                                              UdpPort group4_retrans_mc_address=224.0.59.71:11071\n");
}

void InitMonitorArcaBook(void)
{
  int i;
  MonitorArcaBook.isProcessing = 0;
  MonitorArcaBook.procCounter = 0;
  for (i = 0; i < MAX_ARCA_GROUPS; i++)
  {
    MonitorArcaBook.isReceiving[i] = 0;
    MonitorArcaBook.recvCounter[i] = 0;
  }
}

void InitMonitorNasdaqBook(void)
{
  MonitorNasdaqBook.isReceiving = 0;
  MonitorNasdaqBook.isProcessing = 0;
  MonitorNasdaqBook.recvCounter = 0;
  MonitorNasdaqBook.procCounter = 0;
}

void InitMonitorNdbxBook(void)
{
  MonitorNdbxBook.isReceiving = 0;
  MonitorNdbxBook.isProcessing = 0;
  MonitorNdbxBook.recvCounter = 0;
  MonitorNdbxBook.procCounter = 0;
}

void InitMonitorPsxBook(void)
{
  MonitorPsxBook.isReceiving = 0;
  MonitorPsxBook.isProcessing = 0;
  MonitorPsxBook.recvCounter = 0;
  MonitorPsxBook.procCounter = 0;
}

void InitMonitorNyseBook(void)
{
  int i;
  for (i = 0; i < MAX_NYSE_BOOK_GROUPS; i++)
  {
    MonitorNyseBook.isReceiving[i] = 0;
    MonitorNyseBook.recvCounter[i] = 0;
  }
  
  MonitorNyseBook.isProcessing = 0;
  MonitorNyseBook.procCounter = 0;
}

void InitMonitorAmexBook(void)
{
  MonitorAmexBook.isReceiving[0] = 0;
  MonitorAmexBook.recvCounter[0] = 0;
  MonitorAmexBook.isProcessing = 0;
  MonitorAmexBook.procCounter = 0;
}

void InitMonitorEdgxBook(void)
{
  int i;
  for (i = 0; i < MAX_EDGX_BOOK_GROUP; i++)
  {
    MonitorEdgxBook.isReceiving[i] = 0;
    MonitorEdgxBook.recvCounter[i] = 0;
  }
  
  MonitorEdgxBook.isProcessing = 0;
  MonitorEdgxBook.procCounter = 0;
}

void InitMonitorEdgaBook(void)
{
  int i;
  for (i = 0; i < MAX_EDGA_BOOK_GROUP; i++)
  {
    MonitorEdgaBook.isReceiving[i] = 0;
    MonitorEdgaBook.recvCounter[i] = 0;
  }
  
  MonitorEdgaBook.isProcessing = 0;
  MonitorEdgaBook.procCounter = 0;
}

void InitMonitorBatszBook(void)
{
  int i;
  for (i = 0; i < MAX_BATSZ_UNIT; i++)
  {
    MonitorBatszBook.isReceiving[i] = 0;
    MonitorBatszBook.recvCounter[i] = 0;
  }
  
  MonitorBatszBook.procCounter = 0;
  MonitorBatszBook.isProcessing = 0;
}

void InitMonitorBYXBook(void)
{
  int i;
  for (i = 0; i < MAX_BYX_UNIT; i++)
  {
    MonitorBYXBook.isReceiving[i] = 0;
    MonitorBYXBook.recvCounter[i] = 0;
  }
  
  MonitorBYXBook.procCounter = 0;
  MonitorBYXBook.isProcessing = 0;
}


void InitMonitorCQS(void)
{
  int i;
  for (i = 0; i < MAX_CQS_MULTICAST_LINES; i++)
  {
    MonitorCQS.isReceiving[i] = 0;
    MonitorCQS.recvCounter[i] = 0;
  }
  MonitorCQS.procCounter = 0;
  MonitorCQS.isProcessing = 0;
}

void InitMonitorUQDF(void)
{
  int i;
  for (i = 0; i < MAX_UQDF_CHANNELS; i++)
  {
    MonitorUQDF.isReceiving[i] = 0;
    MonitorUQDF.recvCounter[i] = 0;
  }
  MonitorUQDF.isProcessing = 0;
  MonitorUQDF.procCounter = 0;
}

/****************************************************************************
- Function name:  Thread_VenueActivityMonitoring
- Input:    N/A
- Output:   N/A
- Return:   N/A
- Description:  N/A
- Usage:    N/A
****************************************************************************/
void *Thread_VenueActivityMonitoring(void* args)
{
  SetKernelAlgorithm("[MONITOR_THREAD]");
  
  int i;
  unsigned int counter = 0;
  const int TIME_TO_ALERT_DATA_UNHANDLED = 60;
  
  /*Note about heartbeat message on various Feeds:
    - ARCA XDP Integrated: Heartbeat is sent after 60 seconds if there is no data
    - Nasdaq Book: Heartbeat is sent after 1 second if there is no data
    - Nasdaq BX Book: Heartbeat is sent after 1 second if there is no data
    - BATSZ Book: Heartbeat is sent after 1 second if there is no data
    - EDGX Book: Heartbeat is sent after 5 seconds if there is no data
    - EDGA Book: Heartbeat is sent after 5 seconds if there is no data
    - CQS: Line-Integrity is sent after approximately 1 minute
    - UQDF: Line-Integrity is sent after approximately 1 minute
  */
  
  while(1)
  {
    for (i = 0; i < MAX_ECN_ORDER; i++)
    {
      if (WillDisableTrading[i] == 1)
      {
        if (TradingCounter > 0) continue;
        
        TraceLog(DEBUG_LEVEL, "Begin to disable trading for order id %d\n", i);
        if (OrderStatusMgmt[i].isConnected == CONNECTED)
        {
          *(TradingStatus.status[i]) = DISABLE;
          WillDisableTrading[i] = 0;
          
          short service;
          switch (i)
          {
            case ORDER_ARCA_DIRECT:
              service = SERVICE_ARCA_DIRECT;
              break;
            case ORDER_NASDAQ_OUCH:
              service = SERVICE_NASDAQ_OUCH;
              break;
            case ORDER_NASDAQ_BX:
              service = SERVICE_NASDAQ_BX;
              break;
            case ORDER_NASDAQ_PSX:
              service = SERVICE_NASDAQ_PSX;
              break;
            case ORDER_NASDAQ_RASH:
              service = SERVICE_NASDAQ_RASH;
              break;
            case ORDER_NYSE_CCG:
              service = SERVICE_NYSE_CCG;
              break;
            case ORDER_BATSZ_BOE:
              service = SERVICE_BATSZ_BOE;
              break;
            case ORDER_BYX_BOE:
              service = SERVICE_BYX_BOE;
              break;
            case ORDER_EDGX:
              service = SERVICE_EDGX_ORDER;
              break;
            case ORDER_EDGA:
              service = SERVICE_EDGA_ORDER;
              break;
            default:
              continue;
          }
          
          if (ASConfig.socket != -1)
          {
            if (BuildSendTradingDisabled(service, TRADING_DISABLED_AS_SCHEDULE) == SUCCESS)
            {
              TraceLog(DEBUG_LEVEL, "Notify trading disabled of order %d is sent to AS\n", i);
            }
          }
        }
      }
    }
    
    usleep(100000); //sleep for 0.1 second resolution
    
    counter++;
    if (counter % 10 != 0)
    {
      continue;
    }
    /*
    BATS Exchanges Trading Hours
    Pre-Opening Session 8:00 a.m. to 9:30 a.m. Eastern Time
    Core Trading Session  9:30 a.m. to 4:00 p.m. Eastern Time
    Post-Close Session   4:00 p.m. to 5:00 p.m. Eastern Time
    */
    //Check if the time is 16:30 EDT, we will disconnect BATS-Z book
    /*
    if (currentHour == 16)
    {
      if (currentMin >= 30)
      {
        if (BookStatusMgmt[BOOK_BATSZ].isConnected == CONNECTED)
        {
          TraceLog(DEBUG_LEVEL, "The time is %d:%d. We are now forcing to disconnect BATS-Z book\n", currentHour, currentMin);
          DisconnectFromVenue(BOOK_BATSZ, -1);
        }
      }
    }
    */
    
    //***********************************************************************************************
    //          MONITORING ARCA BOOK STATUS
    //***********************************************************************************************
    if (BookStatusMgmt[BOOK_ARCA].isConnected == CONNECTED)
    {
      for (i = 0; i < MAX_ARCA_GROUPS; i++)
      {
        if (__sync_fetch_and_add(&MonitorArcaBook.isReceiving[i], 0) == 0)
        {
          MonitorArcaBook.recvCounter[i]++;
          if (MonitorArcaBook.recvCounter[i] > 60)
          {
            MonitorArcaBook.recvCounter[i] = 0;
            if (BuildSend_Book_IdleWarning(BOOK_ARCA, i) == ERROR)
            {
              TraceLog(DEBUG_LEVEL, "Could not send message to AS for warning Arca group (%d).\n", i+1);
            }
          }
        }
        else
        {
          __sync_fetch_and_and(&MonitorArcaBook.isReceiving[i], 0);
          MonitorArcaBook.recvCounter[i] = 0;
        }
      }
      
      if (MonitorArcaBook.isProcessing == 1)
      {
        MonitorArcaBook.isProcessing = 0;
        MonitorArcaBook.procCounter = 0; //reset counter
      }
      else
      {
        if (MonitorArcaBook.procCounter < TIME_TO_ALERT_DATA_UNHANDLED)
        {
          MonitorArcaBook.procCounter++;
        }
        else
        {
          if (ASConfig.socket != -1)
          {
            BuildSendUnhandledDataOnGroup(SERVICE_ARCA_BOOK);
          }
          MonitorArcaBook.procCounter = 0;
        }
      }
    }
    
    //***********************************************************************************************
    //          MONITORING NASDAQ BOOK STATUS
    //***********************************************************************************************
    if (BookStatusMgmt[BOOK_NASDAQ].isConnected == CONNECTED)
    {
      if (__sync_fetch_and_add(&MonitorNasdaqBook.isReceiving, 0) == 0)
      {
        MonitorNasdaqBook.recvCounter++;
        if (MonitorNasdaqBook.recvCounter > 10)
        {
          TraceLog(WARN_LEVEL, "NASDAQ Book: could not received anything within 10 secs\n");
          
          MonitorNasdaqBook.recvCounter = 0;
          DisconnectFromVenue(BOOK_NASDAQ, -1);
        }
      }
      else
      {
        __sync_fetch_and_and(&MonitorNasdaqBook.isReceiving, 0);
        MonitorNasdaqBook.recvCounter = 0;
        
        //this group received data, so we check if it processed data or not
        if (MonitorNasdaqBook.isProcessing == 1)
        {
          MonitorNasdaqBook.isProcessing = 0;
          MonitorNasdaqBook.procCounter = 0; //reset counter
        }
        else
        {
          if (MonitorNasdaqBook.procCounter < TIME_TO_ALERT_DATA_UNHANDLED)
          {
            MonitorNasdaqBook.procCounter++;
          }
          else
          {
            if (ASConfig.socket != -1)
            {
              BuildSendUnhandledDataOnGroup(SERVICE_NASDAQ_BOOK);
            }
            MonitorNasdaqBook.procCounter = 0;
            
            //TraceLog(WARN_LEVEL, "NASDAQ Book: does not processing anything within %d seconds\n", TIME_TO_ALERT_DATA_UNHANDLED);
          }
        }
      }
    }

    //***********************************************************************************************
    //          MONITORING BX BOOK STATUS
    //***********************************************************************************************
    if (BookStatusMgmt[BOOK_NDBX].isConnected == CONNECTED)
    {
      if (__sync_fetch_and_add(&MonitorNdbxBook.isReceiving, 0) == 0)
      {
        MonitorNdbxBook.recvCounter++;
        if (MonitorNdbxBook.recvCounter > 10)
        {
          TraceLog(WARN_LEVEL, "BX Book: could not received anything within 10 secs\n");
          
          MonitorNdbxBook.recvCounter = 0;
          DisconnectFromVenue(BOOK_NDBX, -1);
        }
      }
      else
      {
        __sync_fetch_and_and(&MonitorNdbxBook.isReceiving, 0);
        MonitorNdbxBook.recvCounter = 0;
        
        //this group received data, so we check if it processed data or not
        if (MonitorNdbxBook.isProcessing == 1)
        {
          MonitorNdbxBook.isProcessing = 0;
          MonitorNdbxBook.procCounter = 0; //reset counter
        }
        else
        {
          if (MonitorNdbxBook.procCounter < TIME_TO_ALERT_DATA_UNHANDLED)
          {
            MonitorNdbxBook.procCounter++;
          }
          else
          {
            if (ASConfig.socket != -1)
            {
              BuildSendUnhandledDataOnGroup(SERVICE_NDBX_BOOK);
            }
            MonitorNdbxBook.procCounter = 0;
            
            //TraceLog(WARN_LEVEL, "BX Book: does not processing anything within %d seconds\n", TIME_TO_ALERT_DATA_UNHANDLED);
          }
        }
      }
    }

    //***********************************************************************************************
    //          MONITORING PSX BOOK STATUS
    //***********************************************************************************************
    if (BookStatusMgmt[BOOK_PSX].isConnected == CONNECTED)
    {
      if (__sync_fetch_and_add(&MonitorPsxBook.isReceiving, 0) == 0)
      {
        MonitorPsxBook.recvCounter++;
        if (MonitorPsxBook.recvCounter > 10)
        {
          TraceLog(WARN_LEVEL, "PSX Book: could not received anything within 10 secs\n");
          
          MonitorPsxBook.recvCounter = 0;
          DisconnectFromVenue(BOOK_PSX, -1);
        }
      }
      else
      {
        __sync_fetch_and_and(&MonitorPsxBook.isReceiving, 0);
        MonitorPsxBook.recvCounter = 0;
        
        //this group received data, so we check if it processed data or not
        if (MonitorPsxBook.isProcessing == 1)
        {
          MonitorPsxBook.isProcessing = 0;
          MonitorPsxBook.procCounter = 0; //reset counter
        }
        else
        {
          if (MonitorPsxBook.procCounter < TIME_TO_ALERT_DATA_UNHANDLED)
          {
            MonitorPsxBook.procCounter++;
          }
          else
          {
            if (ASConfig.socket != -1)
            {
              BuildSendUnhandledDataOnGroup(SERVICE_PSX_BOOK);
            }
            MonitorPsxBook.procCounter = 0;
            
            //TraceLog(WARN_LEVEL, "PSX Book: does not processing anything within %d seconds\n", TIME_TO_ALERT_DATA_UNHANDLED);
          }
        }
      }
    }

    //***********************************************************************************************
    //          MONITORING NYSE BOOK STATUS
    //***********************************************************************************************
    if (BookStatusMgmt[BOOK_NYSE].isConnected == CONNECTED)
    {
      for (i = 0; i < MAX_NYSE_BOOK_GROUPS; i++)
      {
        if (NyseBookConf.group[i].groupPermission == DISABLE)
        {
          continue;
        }
        
        if (__sync_fetch_and_add(&MonitorNyseBook.isReceiving[i], 0) == 0)
        {
          MonitorNyseBook.recvCounter[i]++;
          
          if (MonitorNyseBook.recvCounter[i] > 90)
          {
            MonitorNyseBook.recvCounter[i] = 0;
            TraceLog(WARN_LEVEL, "NYSE BOOK GROUP %d not received anything within 90 secs\n", i+1 );
            DisconnectFromVenue(BOOK_NYSE, -1);
          }
        }
        else
        {
          MonitorNyseBook.recvCounter[i] = 0;
          __sync_fetch_and_and(&MonitorNyseBook.isReceiving[i], 0);
        }
      }
      
      if (MonitorNyseBook.isProcessing == 1)
      {
        MonitorNyseBook.isProcessing = 0;
        MonitorNyseBook.procCounter = 0; //reset counter
      }
      else
      {
        if (MonitorNyseBook.procCounter < TIME_TO_ALERT_DATA_UNHANDLED)
        {
          MonitorNyseBook.procCounter++;
        }
        else
        {
          if (ASConfig.socket != -1)
          {
            BuildSendUnhandledDataOnGroup(SERVICE_NYSE_BOOK);
          }
          MonitorNyseBook.procCounter = 0;
        }
      }
    }
    
    //***********************************************************************************************
    //          MONITORING AMEX BOOK STATUS
    //***********************************************************************************************
    if (BookStatusMgmt[BOOK_AMEX].isConnected == CONNECTED)
    {
      i = 0;
      if (__sync_fetch_and_add(&MonitorAmexBook.isReceiving[i], 0) == 0)
      {
        MonitorAmexBook.recvCounter[i]++;
        
        if (MonitorAmexBook.recvCounter[i] > 90)
        {
          MonitorAmexBook.recvCounter[i] = 0;
          TraceLog(WARN_LEVEL, "AMEX BOOK not received anything within 90 secs\n");
          DisconnectFromVenue(BOOK_AMEX, -1);
        }
      }
      else
      {
        MonitorAmexBook.recvCounter[i] = 0;
        __sync_fetch_and_and(&MonitorAmexBook.isReceiving[i], 0);
      }
      
      if (MonitorAmexBook.isProcessing == 1)
      {
        MonitorAmexBook.isProcessing = 0;
        MonitorAmexBook.procCounter = 0; //reset counter
      }
      else
      {
        if (MonitorAmexBook.procCounter < TIME_TO_ALERT_DATA_UNHANDLED)
        {
          MonitorAmexBook.procCounter++;
        }
        else
        {
          if (ASConfig.socket != -1)
          {
            BuildSendUnhandledDataOnGroup(SERVICE_AMEX_BOOK);
          }
          MonitorAmexBook.procCounter = 0;
        }
      }
    }
    
    //***********************************************************************************************
    //          MONITORING BATS BOOK STATUS
    //***********************************************************************************************
    if (BookStatusMgmt[BOOK_BATSZ].isConnected == CONNECTED)
    {
      for (i = 0; i < MAX_BATSZ_UNIT; i++)
      {
        if (BATSZ_Book_Conf.group[i].groupPermission == DISABLE)
        {
          continue;
        }
        
        if (__sync_fetch_and_add(&MonitorBatszBook.isReceiving[i], 0) == 0)
        {
          MonitorBatszBook.recvCounter[i]++;
          if (MonitorBatszBook.recvCounter[i] > 10)
          {
            MonitorBatszBook.recvCounter[i] = 0;
            TraceLog(WARN_LEVEL, "BATSZ BOOK GROUP %d not received anything within 10 secs\n", i+1 );
            DisconnectFromVenue(BOOK_BATSZ, -1);
          }
        }
        else
        {
          MonitorBatszBook.recvCounter[i] = 0;
          __sync_fetch_and_and(&MonitorBatszBook.isReceiving[i], 0);
        }
      }
      
      if (MonitorBatszBook.isProcessing == 1)
      {
        MonitorBatszBook.isProcessing = 0;
        MonitorBatszBook.procCounter = 0; //reset counter
      }
      else
      {
        if (MonitorBatszBook.procCounter < TIME_TO_ALERT_DATA_UNHANDLED)
        {
          MonitorBatszBook.procCounter++;
        }
        else
        {
          if (ASConfig.socket != -1)
          {
            BuildSendUnhandledDataOnGroup(SERVICE_BATSZ_BOOK);
          }
          MonitorBatszBook.procCounter = 0;
        }
      }
    }
    
    //***********************************************************************************************
    //          MONITORING BYX BOOK STATUS
    //***********************************************************************************************
    if (BookStatusMgmt[BOOK_BYX].isConnected == CONNECTED)
    {
      for (i = 0; i < MAX_BYX_UNIT; i++)
      {
        if (BYX_Book_Conf.group[i].groupPermission == DISABLE)
        {
          continue;
        }
        
        if (__sync_fetch_and_add(&MonitorBYXBook.isReceiving[i], 0) == 0)
        {
          MonitorBYXBook.recvCounter[i]++;
          if (MonitorBYXBook.recvCounter[i] > 10)
          {
            MonitorBYXBook.recvCounter[i] = 0;
            TraceLog(WARN_LEVEL, "BYX BOOK GROUP %d not received anything within 10 secs\n", i+1 );
            DisconnectFromVenue(BOOK_BYX, -1);
          }
        }
        else
        {
          MonitorBYXBook.recvCounter[i] = 0;
          __sync_fetch_and_and(&MonitorBYXBook.isReceiving[i], 0);
        }
      }
      
      if (MonitorBYXBook.isProcessing == 1)
      {
        MonitorBYXBook.isProcessing = 0;
        MonitorBYXBook.procCounter = 0; //reset counter
      }
      else
      {
        if (MonitorBYXBook.procCounter < TIME_TO_ALERT_DATA_UNHANDLED)
        {
          MonitorBYXBook.procCounter++;
        }
        else
        {
          if (ASConfig.socket != -1)
          {
            BuildSendUnhandledDataOnGroup(SERVICE_BYX_BOOK);
          }
          MonitorBYXBook.procCounter = 0;
        }
      }
    }
    
    //***********************************************************************************************
    //          MONITORING EDGX BOOK IDLE STATUS
    //***********************************************************************************************
    if (BookStatusMgmt[BOOK_EDGX].isConnected == CONNECTED)
    {
      for (i = 0; i < MAX_EDGX_BOOK_GROUP; i++)
      {
        if (EDGX_BOOK_Conf.group[i].groupPermission == DISABLE)
        {
          continue;
        }
        
        if (__sync_fetch_and_add(&MonitorEdgxBook.isReceiving[i], 0) == 0)
        {
          MonitorEdgxBook.recvCounter[i]++;
          if (MonitorEdgxBook.recvCounter[i] > 10)
          {
            MonitorEdgxBook.recvCounter[i] = 0;
            TraceLog(WARN_LEVEL, "EDGX BOOK GROUP %d not received anything within 10 secs\n", i+1 );
            DisconnectFromVenue(BOOK_EDGX, -1);
          }
        }
        else
        {
          MonitorEdgxBook.recvCounter[i] = 0;
          __sync_fetch_and_and(&MonitorEdgxBook.isReceiving[i], 0);
        }
      }

      if (MonitorEdgxBook.isProcessing == 1)
      {
        MonitorEdgxBook.isProcessing = 0;
        MonitorEdgxBook.procCounter = 0; //reset counter
      }
      else
      {
        if (MonitorEdgxBook.procCounter < TIME_TO_ALERT_DATA_UNHANDLED)
        {
          MonitorEdgxBook.procCounter++;
        }
        else
        {
          if (ASConfig.socket != -1)
          {
            BuildSendUnhandledDataOnGroup(SERVICE_EDGX_BOOK);
          }
          MonitorEdgxBook.procCounter = 0;
        }
      }
    }
    
    //***********************************************************************************************
    //          MONITORING EDGA BOOK IDLE STATUS
    //***********************************************************************************************
    if (BookStatusMgmt[BOOK_EDGA].isConnected == CONNECTED)
    {
      for (i = 0; i < MAX_EDGA_BOOK_GROUP; i++)
      {
        if (EDGA_BOOK_Conf.group[i].groupPermission == DISABLE)
        {
          continue;
        }
        
        if (__sync_fetch_and_add(&MonitorEdgaBook.isReceiving[i], 0) == 0)
        {
          MonitorEdgaBook.recvCounter[i]++;
          if (MonitorEdgaBook.recvCounter[i] > 10)
          {
            MonitorEdgaBook.recvCounter[i] = 0;
            TraceLog(WARN_LEVEL, "EDGA BOOK GROUP %d not received anything within 10 secs\n", i+1 );
            DisconnectFromVenue(BOOK_EDGA, -1);
          }
        }
        else
        {
          MonitorEdgaBook.recvCounter[i] = 0;
          __sync_fetch_and_and(&MonitorEdgaBook.isReceiving[i], 0);
        }
      }

      if (MonitorEdgaBook.isProcessing == 1)
      {
        MonitorEdgaBook.isProcessing = 0;
        MonitorEdgaBook.procCounter = 0; //reset counter
      }
      else
      {
        if (MonitorEdgaBook.procCounter < TIME_TO_ALERT_DATA_UNHANDLED)
        {
          MonitorEdgaBook.procCounter++;
        }
        else
        {
          if (ASConfig.socket != -1)
          {
            BuildSendUnhandledDataOnGroup(SERVICE_EDGA_BOOK);
          }
          MonitorEdgaBook.procCounter = 0;
        }
      }
    }
    
    //***********************************************************************************************
    //          MONITORING CQS BOOK STATUS
    //***********************************************************************************************
    if (CQS_StatusMgmt.isConnected == CONNECTED)
    {
      for (i = 0; i < MAX_CQS_MULTICAST_LINES; i++)
      {
        if (CQS_Conf.group[i].groupPermission == DISABLE)
        {
          continue;
        }
        
        if (__sync_fetch_and_add(&MonitorCQS.isReceiving[i], 0) == 0)
        {
          MonitorCQS.recvCounter[i]++;
          if (MonitorCQS.recvCounter[i] > 90)
          {
            MonitorCQS.recvCounter[i] = 0;
            TraceLog(WARN_LEVEL, "CQS GROUP %d not received anything within 90 secs\n", i+1 );
            DisconnectFromVenue(FEED_CQS, -1);
          }
        }
        else
        {
          MonitorCQS.recvCounter[i] = 0;
          __sync_fetch_and_and(&MonitorCQS.isReceiving[i], 0);
        }
      }
      
      if (MonitorCQS.isProcessing == 1)
      {
        MonitorCQS.isProcessing = 0;
        MonitorCQS.procCounter = 0; //reset counter
      }
      else
      {
        if (MonitorCQS.procCounter < TIME_TO_ALERT_DATA_UNHANDLED)
        {
          MonitorCQS.procCounter++;
        }
        else
        {
          if (ASConfig.socket != -1)
          {
            BuildSendUnhandledDataOnGroup(SERVICE_CQS_FEED);
          }
          MonitorCQS.procCounter = 0;
        }
      }
    }
    
    //***********************************************************************************************
    //          MONITORING UQDF STATUS
    //***********************************************************************************************
    if (UQDF_StatusMgmt.isConnected == CONNECTED)
    {
      for (i = 0; i < MAX_UQDF_CHANNELS; i++)
      {
        if (UQDF_Conf.group[i].groupPermission == DISABLE)
        {
          continue;
        }
        
        if (__sync_fetch_and_add(&MonitorUQDF.isReceiving[i], 0) == 0)
        {
          MonitorUQDF.recvCounter[i]++;
          if (MonitorUQDF.recvCounter[i] > 90)
          {
            MonitorUQDF.recvCounter[i] = 0;
            TraceLog(WARN_LEVEL, "UQDF GROUP %d not received anything within 90 secs\n", i+1 );
            DisconnectFromVenue(FEED_UQDF, -1);
          }
        }
        else
        {
          MonitorUQDF.recvCounter[i] = 0;
          __sync_fetch_and_and(&MonitorUQDF.isReceiving[i], 0); //reset to zero
        }
      }
      
      if (MonitorUQDF.isProcessing == 1)
      {
        MonitorUQDF.isProcessing = 0;
        MonitorUQDF.procCounter = 0; //reset counter
      }
      else
      {
        if (MonitorUQDF.procCounter < TIME_TO_ALERT_DATA_UNHANDLED)
        {
          MonitorUQDF.procCounter++;
        }
        else
        {
          if (ASConfig.socket != -1)
          {
            BuildSendUnhandledDataOnGroup(SERVICE_UQDF_FEED);
          }
          MonitorUQDF.procCounter = 0;
        }
      }
    }

    if (IsTradeServerStopped == 1) return NULL;
  }
  
  return NULL;
}

void *Thread_DBLFilter_Data_Consumer(void *arg)
{ 
  t_FilterBuffers *filterBuffers = (t_FilterBuffers *)arg;
  
  _consumerId = filterBuffers->index;     //Note: we will limit the use of _consumerId, since it's TLS variable, there will be performance issue
  const int consumerId = filterBuffers->index;

  InitOrderMaps();
  
  filterBuffers->marketDataReceiveReader = createSpscReader(filterBuffers->marketDataReceiveQueue, DBLFILTER_MD_QUEUE_SIZE, filterBuffers->marketDataReceiveSeq);
  filterBuffers->orderEntryReceiveReader = createSpscReader(filterBuffers->orderEntryReceiveQueue, DBLFILTER_OE_QUEUE_SIZE, filterBuffers->orderEntryReceiveSeq);
  filterBuffers->orderEntrySendWriter = createSpscWriter(filterBuffers->orderEntrySendQueue, DBLFILTER_OE_QUEUE_SIZE, filterBuffers->orderEntrySendSeq);
  filterBuffers->dataBlockWriter = createSpscWriter(filterBuffers->dataBlockQueue, DATABLOCK_QUEUE_SIZE, filterBuffers->dataBlockSeq);  
  filterBuffers->tradeOutputWriter = createSpscWriter(filterBuffers->tradeOutputQueue, TRADEOUTPUT_QUEUE_SIZE, filterBuffers->tradeOutputSeq);
  
  char _threadDesc[32];
  sprintf(_threadDesc, "[DBLFILTER_DATA_CONSUMER_%d]", consumerId+1);
  SetKernelAlgorithm(_threadDesc);
  
  const unsigned char *oe_buffer_ptr;
  const unsigned char *oe_buffer_ptr_start;
  uint32_t oe_topic_id;
  uint8_t oe_venue_id;
  uint32_t oe_symbol_id;
  int oe_buffer_readable_bytes;
  int oe_length = 0;
  int oe_bytes_read;
  unsigned short oe_bytes_processed;
  const unsigned char *md_buffer_ptr;
  const unsigned char *md_buffer_ptr_start;
  uint32_t md_topic_id;
  uint8_t md_venue_id;
  uint32_t md_symbol_id;
  int md_buffer_readable_bytes;
  int md_length = 0;
  int md_bytes_read;
  unsigned short md_bytes_processed;
  
  spsc_read_handle oeReader = filterBuffers->orderEntryReceiveReader;
  spsc_read_handle mdReader = filterBuffers->marketDataReceiveReader;
  
  volatile short *flushTrigger = filterBuffers->flushTrigger;

  while(1)
  {
    /********************************************
       check if we need to flush book
    ********************************************/
    if(*flushTrigger != 0)
    {
      if(*flushTrigger & __FLUSH_ARCA) {
        FlushVenueSymbolsOnQueue(BOOK_ARCA, consumerId);
      }
      
      if(*flushTrigger & __FLUSH_NASDAQ) {
        FlushVenueSymbolsOnQueue(BOOK_NASDAQ, consumerId);
      }
      
      if(*flushTrigger & __FLUSH_NDBX) {
        FlushVenueSymbolsOnQueue(BOOK_NDBX, consumerId);
      }
      
      if(*flushTrigger & __FLUSH_PSX) {
        FlushVenueSymbolsOnQueue(BOOK_PSX, consumerId);
      }

      if(*flushTrigger & __FLUSH_NYSE) {
        FlushVenueSymbolsOnQueue(BOOK_NYSE, consumerId);
      }
      
      if(*flushTrigger & __FLUSH_AMEX) {
        FlushVenueSymbolsOnQueue(BOOK_AMEX, consumerId);
      }
      
      if(*flushTrigger & __FLUSH_BATSZ) {
        FlushVenueSymbolsOnQueue(BOOK_BATSZ, consumerId);
      }
      
      if(*flushTrigger & __FLUSH_BYX) {
        FlushVenueSymbolsOnQueue(BOOK_BYX, consumerId);
      }
      
      if(*flushTrigger & __FLUSH_EDGX) {
        FlushVenueSymbolsOnQueue(BOOK_EDGX, consumerId);
      }
      
      if(*flushTrigger & __FLUSH_EDGA) {
        FlushVenueSymbolsOnQueue(BOOK_EDGA, consumerId);
      }
      
      *flushTrigger = 0;
      
      //DEVLOGF("Finished flushing on consumer thread %d\n", consumerId);
    }
    
    /********************************************
       check order entry receive queue
    ********************************************/
    spscSyncNumReadableItems(oeReader);
    oe_buffer_readable_bytes = spscGetNumReadableItems(oeReader);
    
    if(oe_buffer_readable_bytes > 0)
    {
      oe_buffer_ptr = spscReadPtr(oeReader);
      oe_length = oe_buffer_readable_bytes;
    }

    while(oe_length != 0)
    {
      do 
      {

        /** Header format: Total size = 12 bytes
          Offset  Field Name    Size & Format                     Description
          0       topic_id      binary 4 byte                     topic_id encodes symbol and venue
          4       arrival_time  binary 8 bytes (Little Endian)    this is the long timestamp
          12      message       binary unknown length
        **/

        oe_buffer_ptr_start = oe_buffer_ptr;

        oe_topic_id = *((uint32_t*) oe_buffer_ptr);
        oe_symbol_id = symbol_from_oe_topic_id(oe_topic_id);
        oe_venue_id = venue_from_oe_topic_id(oe_topic_id);
        uint64_t oe_arrival_time = *((uint64_t*)(oe_buffer_ptr+4));   // this is the long timestamp of the tcp message receipt

        oe_bytes_processed = ProcessOrderEntryMessage(oe_venue_id, oe_symbol_id, oe_arrival_time, oe_buffer_ptr + DBL_OE_HDR_LEN);
        if(oe_bytes_processed <= 0)
        {
          TraceLog(FATAL_LEVEL, "Processing thread unable to process order entry message!  Restart trade server!\n");
          return NULL;
        }
        oe_buffer_ptr += oe_bytes_processed + DBL_OE_HDR_LEN;
        oe_bytes_read = oe_buffer_ptr - oe_buffer_ptr_start;
        oe_buffer_readable_bytes -= oe_bytes_read;
      } while(oe_buffer_readable_bytes > 0);
            
      spscCommitItemsRead(oeReader, oe_length);
      spscSyncNumReadableItems(oeReader);
      oe_buffer_readable_bytes = spscGetNumReadableItems(oeReader);
      oe_buffer_ptr = spscReadPtr(oeReader);
      oe_length = oe_buffer_readable_bytes;
    }

    /********************************************
      check market data receive queue
    ********************************************/
    spscSyncNumReadableItems(mdReader);
    md_buffer_readable_bytes = spscGetNumReadableItems(mdReader);
    if(md_buffer_readable_bytes > 0)
    {
      md_buffer_ptr = spscReadPtr(mdReader);
      md_length = md_buffer_readable_bytes;
    }

    while(md_length != 0)
    {
      do 
      {
        /** Header format: Total size = 16 bytes
          Offset  Field Name      Size & Format                     Description
          0       topic_id        binary 4 bytes (Little Endian)    topic id
          4       arrival_time    binary 8 bytes (Little Endian)    this is the long timestamp given in the dbl recv struct
          12      seconds         binary 4 bytes (Little Endian)    this is seconds since midnight from the venue's packet header
        **/
        md_buffer_ptr_start = md_buffer_ptr;
        md_topic_id = *((uint32_t*)md_buffer_ptr);
        if (md_topic_id == 0x3ffff) {
          md_venue_id = VENUE_ADMIN;
          md_symbol_id = 0;
        } else {
          md_venue_id = venue_from_md_topic_id(md_topic_id);
          md_symbol_id = symbol_from_md_topic_id(md_topic_id);
        }
        uint64_t md_arrival_time = *((uint64_t*)(md_buffer_ptr+4));      // this is the long timestamp given in the dbl recv struct
        uint32_t md_seconds = *((uint32_t*)(md_buffer_ptr+12));        // this is seconds since midnight from the venue's packet header

        md_bytes_processed = ProcessMarketDataMessage(md_venue_id, md_symbol_id, md_arrival_time, md_seconds, md_buffer_ptr, md_buffer_readable_bytes, consumerId);

        if(md_bytes_processed <= 0)
        {
          TraceLog(FATAL_LEVEL, "Processing thread unable to process market data message!  Restart trade server!\n");
          return NULL;
        }

        md_buffer_ptr += md_bytes_processed;
        md_bytes_read = md_buffer_ptr - md_buffer_ptr_start;
        md_buffer_readable_bytes -= md_bytes_read;
      } while(md_buffer_readable_bytes > 0);

      spscCommitItemsRead(mdReader, md_length);
      spscSyncNumReadableItems(mdReader);
      md_buffer_readable_bytes = spscGetNumReadableItems(mdReader);
      md_buffer_ptr = spscReadPtr(mdReader);
      md_length = md_buffer_readable_bytes;
    }
  }

  return NULL;
}

int InitializeMessageConsumers(void)
{ 
  //=============================================================================================
  // Start message consumer thread(s) (these threads will consume messages from md/oe spsc queues)
  // =============================================================================================

  int i;
  for(i = 0; i < DBLFILTER_NUM_QUEUES; i++)
  {
    pthread_t message_consumer_thread;
    TraceLog(DEBUG_LEVEL, "DBLFilter - Launching consumer thread (%d).\n", i+1);
    pthread_create(&message_consumer_thread, 0, Thread_DBLFilter_Data_Consumer, (void *)&FilterBuffers[i]);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  SaveVersionInfoToFile
****************************************************************************/
int SaveVersionInfoToFile()
{
  // Initialize version
  #ifdef VERSION
    TraceLog(DEBUG_LEVEL, "Trade Server: version %s\n", VERSION);
  #else 
    TraceLog(DEBUG_LEVEL, "Trade Server: unknown version\n");
  #endif

  int hour, min, sec, date, year, month;
  time_t now = (time_t)hbitime_seconds();
  struct tm *timeNow = (struct tm *) localtime(&now);
  char fullPath[MAX_PATH_LEN] = "\0";
  Environment_get_data_filename("version_log.txt", fullPath, MAX_PATH_LEN);
  int file_desc;
  struct flock region_to_lock;
  int res;
  char byte_write[80];
  memset(byte_write, 0, 80);

  //Get time when TS started
  hour  = timeNow->tm_hour;
  min   = timeNow->tm_min;
  sec   = timeNow->tm_sec;
  date  = timeNow->tm_mday;
  month = 1 + timeNow->tm_mon;
  year  = 1900 + timeNow->tm_year;

  //Initialize locking file
  region_to_lock.l_type = F_WRLCK;
  region_to_lock.l_whence = SEEK_SET;
  file_desc = open(fullPath, O_RDWR | O_CREAT | O_APPEND, 0666);
  if (!file_desc)
  {
    TraceLog(ERROR_LEVEL, "Could not open file '%s' to read or we don't have permission to create\n", fullPath);
    return -1;
  }
  res = fcntl(file_desc, F_SETLKW, &region_to_lock);
  if (res == -1) 
  {
    fprintf(stderr, "Failed to lock region \n");
  }
  sprintf(byte_write,"%d-%.2d-%.2d %.2d:%.2d:%.2d - TS \t VERSION: %s \n", year, month, date, hour, min, sec,VERSION);
  write(file_desc, byte_write, strlen(byte_write));
  //fprintf(fp,"%d-%.2d-%.2d   %.2d:%.2d:%.2d  -   TS \t VERSION: '%s' \n",year,month,date,hour,min,sec,VERSION);
  close(file_desc);

  return 0;
}
