/****************************************************************************
** Project name:  Equity Arbitrage Application
** Filename:  kernel_algorithm.c
** Description:   CPU alfinity / scheduler type / priority for each thread in TS
** Author:    Khanh Ngo Duy
** First created on 20 July 2009
** Last updated on 22 July 2009
****************************************************************************/
#define _GNU_SOURCE 
#include <sys/time.h>
#include <sys/resource.h>
#include <sched.h>
#include <pthread.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/syscall.h>
#include <errno.h>
#include "kernel_algorithm.h"

t_KernelConfig KernelConfig;

/****************************************************************************
- Function name:  gettid
****************************************************************************/
#define gettid() syscall(__NR_gettid)

/****************************************************************************
- Function name:  InitKernelAlgorithmConfig
****************************************************************************/
void InitKernelAlgorithmConfig(void)
{
  memset (&KernelConfig, 0, sizeof(t_KernelConfig));
  
  //Get number of CPU
  KernelConfig.maxSystemCPU = sysconf(_SC_NPROCESSORS_ONLN);
  
  if (KernelConfig.maxSystemCPU <= 0)
  {
    TraceLog(WARN_LEVEL, "Could not get the number of CPU.\n");
  }
  
  return;
}

/****************************************************************************
- Function name:  LoadKernelAlgorithmConfigFromFile
****************************************************************************/
int LoadKernelAlgorithmConfigFromFile(const char *fileName)
{
  //load from file
  char fullPath[MAX_PATH_LEN];
  
  if (GetFullConfigPath(fileName, CONF_MISC, fullPath) == NULL_POINTER)
  {
    return SUCCESS;
  }
  
  FILE* fileDesc;
  fileDesc = fopen(fullPath, "r");
  
  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Critical! Could not open file (%s).\n", fullPath);
    PrintErrStr(ERROR_LEVEL, "calling fopen(): ", errno);
    return SUCCESS;
  }
  
  //File opened successfully
  //Clear the buffer pointed to by line
  char line[1024];
  memset(line, 0, 1024);  
  
  // Get each line from the configuration file
  int lineCount = 0;
  int currentThread = 0;
  while (fgets(line, 1024, fileDesc) != NULL)
  { 
    RemoveCharacters(line, strlen(line));

    if (strchr(line, '#') || (strlen(line) == 0))
    {
      continue;
    }
    
    if (line[0] == '[') //Thread identifier
    {
      currentThread = KernelConfig.numberOfThread;
      KernelConfig.numberOfThread++;
      lineCount = 0;
      
      strcpy(KernelConfig.threadAlgorithm[currentThread].threadDescription, line);
    }
    else
    {
      lineCount++;
      switch (lineCount)
      {
        case 1:   //Cpu position
          KernelConfig.threadAlgorithm[currentThread].cpuPosition = atoi(line);
          break;
        case 2:   //Scheduler type
          KernelConfig.threadAlgorithm[currentThread].schedulerType = atoi(line);
          break;
        case 3:   //Nice value or priority
          KernelConfig.threadAlgorithm[currentThread].priority = atoi(line);
          break;
        default:
          break;
      }
    }
  }
  
  // Close the file
  fclose(fileDesc);
  return SUCCESS;
}

/****************************************************************************
- Function name:  SetKernelAlgorithm
****************************************************************************/
int SetKernelAlgorithm(const char *threadDescription)
{
  int i;
  cpu_set_t cpuMask;
  struct sched_param param;
  
  //Print out the thread ID
  TraceLog(DEBUG_LEVEL, "***New thread is created: %s, Thread ID: %d\n", threadDescription, gettid());
      
  for (i=0; i<KernelConfig.numberOfThread; i++)
  {
    if (strncmp(KernelConfig.threadAlgorithm[i].threadDescription, threadDescription, strlen(threadDescription)) == 0)
    {
    
      // CPU Affinity
      if (KernelConfig.threadAlgorithm[i].cpuPosition >= KernelConfig.maxSystemCPU)
      {
        TraceLog(WARN_LEVEL, "%s has invalid number of CPU Position.\n", threadDescription);
      }
      
      CPU_ZERO(&cpuMask);
      CPU_SET(KernelConfig.threadAlgorithm[i].cpuPosition, &cpuMask);
      
      if (sched_setaffinity(0, sizeof(cpuMask), &cpuMask) != 0)   //set the process to cpu 0
      {
        PrintErrStr(ERROR_LEVEL, "Calling sched_setaffinity(): ", errno);
        return ERROR;
      }
      TraceLog(DEBUG_LEVEL, "*Successful set CPU affinity(%d) for %s\n", KernelConfig.threadAlgorithm[i].cpuPosition, threadDescription);
      
      //Scheduler type and priority
      if (KernelConfig.threadAlgorithm[i].schedulerType == 0)   //SCHED_OTHER
      {
        //Scheduler type
        param.sched_priority = 0;
        if(sched_setscheduler(0, SCHED_OTHER, &param) == -1) 
        {
          PrintErrStr(ERROR_LEVEL, "Calling sched_setscheduler(): ", errno);
          return ERROR;
        }
        TraceLog(DEBUG_LEVEL, "*Successful set scheduler type(SCHED_OTHER) for %s\n", threadDescription);
        
        //Nice value
        if(setpriority(PRIO_PROCESS, 0, KernelConfig.threadAlgorithm[i].priority) == -1)
        {
          PrintErrStr(ERROR_LEVEL, "Calling setpriority(): ", errno);
          return ERROR;
        }
        TraceLog(DEBUG_LEVEL, "*Successful set Nice value(%d) for %s\n", KernelConfig.threadAlgorithm[i].priority, threadDescription);
        
      }
      else if (KernelConfig.threadAlgorithm[i].schedulerType == 1)  //SCHED_FIFO
      {
        param.sched_priority = KernelConfig.threadAlgorithm[i].priority;
        if(sched_setscheduler(0, SCHED_FIFO, &param) == -1) 
        {
          PrintErrStr(ERROR_LEVEL, "Calling sched_setscheduler(): ", errno);
          return ERROR;
        }
        
        TraceLog(DEBUG_LEVEL, "*Successful set scheduler type(SCHED_FIFO) and priority(%d) for %s\n", KernelConfig.threadAlgorithm[i].priority, threadDescription);
      }
      else
      {
        TraceLog(ERROR_LEVEL, "(%s): We don't support the scheduler type %d.\n", threadDescription, KernelConfig.threadAlgorithm[i].schedulerType);
        return ERROR;
      }
      
      //TraceLog(DEBUG_LEVEL, "*Successfully set CPU Affinity (%d), Scheduler type(%d), Priority(%d) for %s.\n", KernelConfig.threadAlgorithm[i].cpuPosition, KernelConfig.threadAlgorithm[i].schedulerType, KernelConfig.threadAlgorithm[i].priority, threadDescription);
      return SUCCESS;
    }
  }
  
  TraceLog(WARN_LEVEL, "%s is not set for CPU Affnity, scheduler type, priority.\n", threadDescription);
  return SUCCESS;
}
