/****************************************************************************
** Include files and define several variables
****************************************************************************/
#include "tuiutil.h"

#include "utility.h"

#include <ctype.h>
#include <string.h>
#include <stdlib.h>

/*--------------------------- Define -----------------------------------------*/ 

/*--------------------------- Local Typedefs ---------------------------------*/

  
/*--------------------------- Global variables -------------------------------*/ 

/*--------------------------- Forward declaration ----------------------------*/
 
/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Entries ----------------------------------------*/ 

/*-----------------------------------------------------------------------------
Function Name   : ToToken
Description   :   List all tokens of input string
Input     :   str-input string
Output      : argc-number of params               
Return Code   :   pointer point to address of 2D token-array if success
          NULL if fail
------------------------------------------------------------------------------*/
char** ToToken(const char *str,char *argc)
{   
  int i;
  int words = 0;
  char *retval;
  char buff[strlen(str) + 1];
  char **result;  
  
  if(str == NULL || strlen(str) == 0)
  {
    *argc = -1;
    return NULL;
  }
    
  for(i = 0; i < strlen(str); i++)
  {
    if(isspace(str[i]) != 0) 
    {
      buff[i] = ' ';
    }
    else
    {
      buff[i] = str[i];
    }
  }
  buff[i] = '\0';
  
  for (i = 0; buff[i] != '\0'; i++) 
  {
    if (!isspace(buff[i]) && 
        (i == 0 || isspace(buff[i-1])))
    {
      words++;
    }
  }
  
  if (words == 0)
  {
    *argc = 0;
    return NULL;
  }

  /* Build a list of vectors */

  result = (char**)malloc((words+1)*sizeof(char*));
  
  i = 1;
  result[0]= strdup(strtok(buff, " ")); 
  while ((retval = strtok(NULL, " ")) != NULL)
  {
    result[i] = strdup(retval);
    i++; 
  }
  
  result[i] = NULL;
  *argc=(char)words;
  return result;
}

/*-----------------------------------------------------------------------------
Function Name   : IsComment
Description   : check a string for empty
Input     : str-input string
Output      :
Return Code   :   1 - true
          0 - not
------------------------------------------------------------------------------*/  
int IsEmptyString(const char *str)
{
  int i;
  
  for(i = 0;i < strlen(str); i++)
  {
    if(isspace(str[i]) == 0)
    {
      return 0;
    }
  }
  return 1;
}
  
/*-----------------------------------------------------------------------------
Function Name   : IsComment
Description   : check a string for shell comment line
Inputs      : str-input string
Outputs     :           
Return Code   :   1 - true
          0 - not
------------------------------------------------------------------------------*/
int IsComment(const char *str)
{
  int i;
  
  for (i = 0; i < strlen(str); i++)
  {
    if ((!isspace(str[i])) && (str[i] == '#'))
    {
      return 1;
    }
    else
    {
      break;
    }
  }
  return 0;
}
  
/*-----------------------------------------------------------------------------
Function Name :     IsValidName
Description   :     cheack a string for valid name
Parameters    :     input       str-input string
Return value  :     1 if true
            0 if not
------------------------------------------------------------------------------*/
int IsValidName(const char *name)
{
  int i;
  
  for (i = strlen(name) - 1; i >= 0; i--)
  {
    if (isalnum(name[i]) == 0 && name[i] != '_')
    {
      return 0;
    }
  }
  return 1;
}
  
/*-----------------------------------------------------------------------------
Function Name   : IsDescriptStart
Description   :   cheack a string which ending with \0 for start of description
Inputs      :   str-input string
Outputs     :
Return Code   :   1 if this line is a starting of description
          0 if not
------------------------------------------------------------------------------*/
int IsDescriptStart(const char *str)
{
  int i;
  
  for (i = 0; i < strlen(str) - 1; i++)
  {
    if (!isspace(str[i]))
    {
      if ((str[i] == '/') && (str[i+1] == '*'))
      {
        return 1;
      }
      break;
    }
  }
  return 0;
}

/*-----------------------------------------------------------------------------
Function Name   : IsDescriptEnd
Description   :   cheack a string which ending with \0 for end of description
Inputs      :   str-input string
Outputs     : 
Return value  :   1 if this line is a ending line of description
          0 if not
------------------------------------------------------------------------------*/
int IsDescriptEnd(const char *str)
{
  int i;
  for (i = strlen(str) - 1; i > 0; i--)
  {
    if (!isspace(str[i]))
    {
      if ((str[i] == '/') && (str[i - 1] == '*'))
      {
        return 1;
      }
      break;
    }
  }
  return 0;
}
  
/*-----------------------------------------------------------------------------
Function Name :     IsAlpha
Description   :     cheack a string for alpha
Parameters    :     input       str-input string
Return value  :     1 if true
            0 if not
------------------------------------------------------------------------------*/
int IsAlpha(const char *str)
{
  int i;
  
  for (i = strlen(str) - 1; i >= 0; i--)
  {
    if (isalpha(str[i]) == 0 || isupper(str[i]) == 0)
    {
      return 0;
    }
  }
  
  return 1;
}

/*-----------------------------------------------------------------------------
Function Name :     IsNumeric
Description   :     cheack a string for numeric
Parameters    :     input       str-input string
Return value  :     1 if true
            0 if not
------------------------------------------------------------------------------*/
int IsNumeric(const char *str)
{
  int i, itime = 0;
  
  for (i = strlen(str) - 1; i >= 0; i--)
  {
    if (isdigit(str[i]) == 0)
    {
      if (str[i] == '.' && itime++ == 0)
      {
        continue;
      }
      
      return 0;
    }
  }
  
  return 1;
}
