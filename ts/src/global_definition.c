/****************************************************************************
** Project name:  Equity Arbitrage Application
** File name:   global_definition.c
** Description:   This file contains function definitions that were declared
        in global_definition.h

** Author:    Sang Nguyen-Minh
** First created on 15 September 2007
** Last updated on 15 September 2007
****************************************************************************/

/****************************************************************************
** Include files and define several variables
****************************************************************************/
#include "global_definition.h"
#include "aggregation_server_proc.h"

char *VenueIdToText[VENUE_UQDF+1] = {
  "arca", /* #define VENUE_ARCA 0    */
  "isld", /* #define VENUE_NASDAQ 1  */
  "nyse", /* #define VENUE_NYSE 2    */
  "bats", /* #define VENUE_BATSZ 3   */
  "edgx", /* #define VENUE_EDGX 4    */
  "rash", /* #define VENUE_RASH 5    */
  "edga", /* #define VENUE_EDGA 6    */
  "bx",   /* #define VENUE_NDBX 7    */
  "byx",  /* #define VENUE_BYX 8     */
  "psx",  /* #define VENUE_PSX 9     */
  "amex", /* #define VENUE_AMEX 10   */
  "",     /* 11                      */
  "",     /* 12                      */
  "cqs",  /* #define VENUE_CQS 13    */
  "utp"   /* #define VENUE_UQDF 14   */
};

char *MdcIdToText[VENUE_UQDF+1] = {
  "arca", /* #define BOOK_ARCA 0    */
  "isld", /* #define BOOK_NASDAQ 1  */
  "nyse", /* #define BOOK_NYSE 2    */
  "bats", /* #define BOOK_BATSZ 3   */
  "edgx", /* #define BOOK_EDGX 4    */
  "edga", /* #define BOOK_EDGA 5    */
  "bx",   /* #define BOOK_NDBX 6    */
  "byx",  /* #define BOOK_BYX 7     */
  "psx",  /* #define BOOK_BYX 8     */
  "amex", /* #define BOOK_AMEX 9    */
  "",     /* 10                     */
  "",     /* 11                     */
  "",     /* 12                     */
  "cqs",  /* #define FEED_CQS 13    */
  "uqdf"  /* #define FEED_UQDF 14   */
};
