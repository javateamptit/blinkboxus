/****************************************************************************
** Project name:  Equity Arbitrage Application
** File name:   utility.c
** Description:   This file contains function definitions that were declared
        in utility.h

** Author:    Sang Nguyen-Minh
** First created on 14 September 2007
** Last updated on 14 September 2007
****************************************************************************/

/****************************************************************************
** Include files and define several variables
****************************************************************************/
#define _GNU_SOURCE

#include <sys/time.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <ctype.h>
#include <netinet/tcp.h>

#include "utility.h"
#include "book_mgmt.h"
#include "configuration.h"
#include "environment_proc.h"
#include "hbitime.h"
#include "parameters_proc.h"
    
/****************************************************************************
            GLOBAL VARIABLES
****************************************************************************/  
t_OEFilterMgmt      OEFilterMgmt;

/****************************************************************************
** Function definitions
****************************************************************************/

/****************************************************************************
- Function name:  ListenToClient
****************************************************************************/
int ListenToClient(int port, int maxConcurrentConnections)
{
  // Check valid port
  if (CheckValidPort(port) == ERROR)
  {
    return ERROR;
  }
  
  int serverSocket;
  struct sockaddr_in servAddr;

  // Create socket
  serverSocket = socket(AF_INET, SOCK_STREAM, 0);
  
  if (serverSocket < 0) 
  {
    PrintErrStr(ERROR_LEVEL, "Inside ListenToClient(), calling socket(): ", errno);
    return ERROR;
  }
  
  // Set SO_REUSEADDR on a socket to true (1):
  int optval = 1;
  
  if (setsockopt(serverSocket, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof optval) != SUCCESS)
  {
    TraceLog(ERROR_LEVEL, "Could not set SO_REUSEADDR for socket (%d)\n", port);
    return ERROR;
  }

  // Bind server port
  servAddr.sin_family = AF_INET;
  servAddr.sin_addr.s_addr = htonl(INADDR_ANY);
  servAddr.sin_port = htons(port);
  
  if (bind(serverSocket, (struct sockaddr *) &servAddr, sizeof(servAddr)) < 0)
  {
    PrintErrStr(ERROR_LEVEL, "Inside ListenToClient(), calling bind(): ", errno);
    
    close(serverSocket);
    return ERROR;
  }

  // We allow at most concurrent clients
  if (listen(serverSocket, maxConcurrentConnections) == -1)
  {
    return ERROR;
  }
  
  return serverSocket;
}

/****************************************************************************
- Function name:  Connect2Server
****************************************************************************/
int Connect2Server(char *ip, int port, const char* serverName)
{
  int sock, rc, valopt;
  struct sockaddr_in localAddr, servAddr; 
  struct hostent *h; 
  long arg;
  fd_set myset;
  socklen_t lon;
  
  h = gethostbyname(ip);
  
  if (h == NULL) 
  { 
    TraceLog(ERROR_LEVEL, "%s: Unknown host '%s'\n", serverName, ip); 
    return ERROR; 
  } 
  
  servAddr.sin_family = h->h_addrtype; 
  memcpy((char *) &servAddr.sin_addr.s_addr, h->h_addr_list[0], h->h_length); 
  servAddr.sin_port = htons(port); 
  
  //Create socket
  sock = socket (AF_INET, SOCK_STREAM, 0); 
  
  if (sock < 0) 
  { 
    PrintErrStr(ERROR_LEVEL, "Inside Connect2Server(), calling socket(): ", errno);
    
    return ERROR; 
  } 
  
  // Set non-blocking 
  arg = fcntl(sock, F_GETFL, NULL); 
  arg |= O_NONBLOCK; 
  fcntl(sock, F_SETFL, arg);
  
  //Bind to any port number
  localAddr.sin_family = AF_INET; 
  localAddr.sin_addr.s_addr = htonl(INADDR_ANY); 
  localAddr.sin_port = htons(0);

  struct timeval tv;
  tv.tv_sec = 120000;
  tv.tv_usec = 0;
  
  int test = setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv, sizeof(tv));
  
  if (test < 0)
  {
    PrintErrStr(ERROR_LEVEL, "setsockopt(): ", errno);
    
    return ERROR;
  }
  
  rc = bind (sock, (struct sockaddr *) &localAddr, sizeof(localAddr)); 
  
  if (rc < 0) 
  { 
    TraceLog(ERROR_LEVEL, "%s: Could not bind to TCP port %u\n", serverName, port);
    close(sock);
    return ERROR; 
  } 
  
  //Connect to server
  rc = connect(sock, (struct sockaddr *) &servAddr, sizeof(servAddr)); 
  
  if (rc < 0) 
  { 
    sleep(1);

    if (errno == EINPROGRESS)
    { 
      tv.tv_sec = 5; 
      tv.tv_usec = 0;
      FD_ZERO(&myset); 
      FD_SET(sock, &myset); 
      
      if (select(sock + 1, NULL, &myset, NULL, &tv) > 0)
      { 
         lon = sizeof(int);
         
         getsockopt(sock, SOL_SOCKET, SO_ERROR, (void*)(&valopt), &lon); 
         
         if (valopt)
         { 
          TraceLog(ERROR_LEVEL, "%s: No route to host\n", serverName); 

          return ERROR; 
         } 
      } 
      else
      { 
         TraceLog(ERROR_LEVEL, "%s: Connection timeout\n", serverName); 

         return ERROR; 
      } 
    }
    else
    { 
      TraceLog(ERROR_LEVEL, "%s: No route to host\n", serverName); 

      return ERROR;
    }
  }

  // Set to blocking mode again... 
  arg = fcntl(sock, F_GETFL, NULL); 
  arg &= (~O_NONBLOCK);
  fcntl(sock, F_SETFL, arg);
  
  TraceLog(DEBUG_LEVEL, "%s: Connection established.\n", serverName);
  return sock; 
}

/****************************************************************************
- Function name:  SetSocketRecvTimeout
****************************************************************************/
int SetSocketRecvTimeout(int socket, int seconds)
{
  struct timeval tv;
  tv.tv_sec = seconds;
  tv.tv_usec = 0;
  
  return setsockopt(socket, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv, sizeof(tv));
}

int SetSocketSndTimeout(const int socket, const int sec, const int usec)
{
  struct timeval tv;
  tv.tv_sec = sec;
  tv.tv_usec = usec;
  
  socklen_t optlen = sizeof(tv);
  
  return setsockopt(socket, SOL_SOCKET, SO_SNDTIMEO, &tv, optlen);
}

int DisableNagleAlgo(int socket)
{
  int disableNagle = 1;
  return setsockopt(socket, IPPROTO_TCP, TCP_NODELAY, (char *)&disableNagle, sizeof(disableNagle));
}

/****************************************************************************
- Function name:  GetFullConfigPath
****************************************************************************/
char *GetFullConfigPath(const char *fileName, int pathType, char *fullConfigPath)
{
  /* 
  Base on path type, fullConfigPath will be
  updated appropriately
  */ 
  switch (pathType)
  {
    case CONF_BOOK:
      return Environment_get_book_conf_filename(fileName, fullConfigPath, MAX_PATH_LEN);
    case CONF_ORDER:
      return Environment_get_order_conf_filename(fileName, fullConfigPath, MAX_PATH_LEN);
    case CONF_SYMBOL:
      return Environment_get_symbol_conf_filename(fileName, fullConfigPath, MAX_PATH_LEN);
    case CONF_MISC:
      return Environment_get_misc_conf_filename(fileName, fullConfigPath, MAX_PATH_LEN);
    default:
      return NULL;
  }

  //TraceLog(DEBUG_LEVEL, "FullConfigPath = %s\n", fullConfigPath);
  
  return fullConfigPath;
}

/****************************************************************************
- Function name:  GetFullRawDataPath
****************************************************************************/
char *GetFullRawDataPath(const char *fileName, char *fullRawDataPath)
{
  // Build full raw data path
  return Environment_get_data_filename(fileName, fullRawDataPath, MAX_PATH_LEN);
}

/****************************************************************************
- Function name:  CreateRawDataPath
****************************************************************************/
int CreateRawDataPath(void)
{
  char fullRawDataPath[512] = "\0";
  
  // Build full raw data path
  Environment_get_root_data_dir(fullRawDataPath, MAX_PATH_LEN);
  
  // Function to create directories
  if (CreateAbsolutePath (fullRawDataPath) == ERROR)
  {
    return ERROR;
  }
  
  TraceLog(DEBUG_LEVEL, "fullRawDataPath = %s\n", fullRawDataPath);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  RemoveCharacters
- Input:        + buffer
                + bufferLen
- Output:       + buffer
- Return:       Buffer with removed used characters
- Description:  + The routine hides the following facts
                  - Check byte by byte with ASCII code 32 (SPACE) and remove
                  any characters less than or equal
                + There are preconditions guaranteed to the routine
                  - buffer must be allocated and initiazed before
                  - buffeLen must be indicated exactly length of 
                  buffer
                + The routine guarantees that the status value will 
                  have value of
                  - buffer
- Usage:      N/A
****************************************************************************/
char *RemoveCharacters(char *buffer, int bufferLen)
{
  int i;

  for (i = bufferLen -1 ; i > -1; i--)
  {
    if (buffer[i] <= USED_CHAR_CODE)
    { 
      buffer[i] = 0;
    }
    else
    {
      break;
    }
  }
  
  return buffer;
}

/****************************************************************************
- Function name:  CountFirstBlanks
- Input:      
- Output:     
- Return:         The total first blank/new_line/carriage_return in buffer
- Description:    Count the first n blank/new_line/carriage_return in buffer until reach character
- Usage:    
****************************************************************************/
int CountFirstBlanks(char *buffer)
{
  if (buffer == NULL)
    return 0;
  
  int len = strlen(buffer);
  
  int i = 0;
  for (i = 0; i < len; i++)
  {
    if (buffer[i] == USED_CHAR_CODE ||
        buffer[i] == CHAR_CODE_NEW_LINE ||
        buffer[i] == CHAR_CODE_CARRIAGE_RETURN)
    {
      continue;
    }
    else
    {
      break;
    }
  }
  
  return i;
}

/****************************************************************************
- Function name:  CheckValidNumberString
- Input:          string
- Output:   
- Return:         Success or Failure
- Description:    Return SUCCESS if string contains only digt from 0 to 9
                  otherwise, return ERROR
- Usage:          N/A
****************************************************************************/
int CheckValidNumberString(const char *numberStr)
{
  int len = strlen(numberStr);
  
  int i;
  for (i = 0; i < len; i++)
  {
    if (isdigit(numberStr[i]) == 0)
    {
      //numberStr[i] is not digit
      return ERROR;
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  CheckValidIpv4Address
****************************************************************************/
int CheckValidIpv4Address(char* ip)
{
  if (strlen(ip) > 15)
  {
    TraceLog(ERROR_LEVEL, "Invalid IP Address: '%s'\n", ip);
    return ERROR;
  }
  
  t_Parameters paraList;
  paraList.countParameters = Lrc_Split(ip, '.', &paraList);
  
  if (paraList.countParameters != 4)
  {
    TraceLog(ERROR_LEVEL, "Invalid IP Address: '%s'\n", ip);
    return ERROR;
  }
  
  int i, j, len;
  for (i = 0; i < paraList.countParameters; i++)
  {
    len = strlen(paraList.parameterList[i]);
    if ((len < 1) || (len > 3))
    {
      TraceLog(ERROR_LEVEL, "Invalid IP Address: '%s'\n", ip);
      return ERROR;
    }
    
    for (j = 0; j < len; j++)
    {
      if (paraList.parameterList[i][j] >= '0' && paraList.parameterList[i][j] <= '9')
        continue;
      else
      {
        TraceLog(ERROR_LEVEL, "Invalid IP Address: '%s'\n", ip);
        return ERROR;
      }
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  CheckValidPort
****************************************************************************/
int CheckValidPort(int port)
{
  if (port < MIN_PORT || port > MAX_PORT)
  {
    return ERROR;
  }
  else
  {
    return SUCCESS;
  }
}


/****************************************************************************
- Function name:  PrintErrStr
- Description:    Print on stdout the following:
                  level description, then the input string _str, then the 
                  string description of errno _errno, and a new line character
                  After that, flush stdout
****************************************************************************/
void PrintErrStr(int _level, const char *_str, int _errno)
{
  char errStr[128];
  errStr[0] = 0;

#if ((_POSIX_C_SOURCE >= 200112L || _XOPEN_SOURCE >= 600) && !defined (_GNU_SOURCE))
  strerror_r(_errno, errStr, 128);
  switch (_level)
  {
    case DEBUG_LEVEL:
      printf("%s%s%s\n", DEBUG_STR, _str, errStr);
      break;
    case NOTE_LEVEL:
      printf("%s%s%s\n", NOTE_STR, _str, errStr);
      break;
    case WARN_LEVEL:
      printf("%s%s%s\n", WARN_STR, _str, errStr);
      break;
    case ERROR_LEVEL:
      printf("%s%s%s\n", ERROR_STR, _str, errStr);
      break;
    case FATAL_LEVEL:
      printf("%s%s%s\n", FATAL_STR, _str, errStr);
      break;
    case USAGE_LEVEL:
      printf("%s%s%s\n", USAGE_STR, _str, errStr);
      break;
    case STATS_LEVEL:
      printf("%s%s%s\n", STATS_STR, _str, errStr);
      break;
    default:
      printf("%s%s\n", _str, errStr);
      break;
  }
#else
  char *_errString;
  _errString = strerror_r(_errno, errStr, 128);
  switch (_level)
  {
    case DEBUG_LEVEL:
      printf("%s%s%s\n", DEBUG_STR, _str, _errString);
      break;
    case NOTE_LEVEL:
      printf("%s%s%s\n", NOTE_STR, _str, _errString);
      break;
    case WARN_LEVEL:
      printf("%s%s%s\n", WARN_STR, _str, _errString);
      break;
    case ERROR_LEVEL:
      printf("%s%s%s\n", ERROR_STR, _str, _errString);
      break;
    case FATAL_LEVEL:
      printf("%s%s%s\n", FATAL_STR, _str, _errString);
      break;
    case USAGE_LEVEL:
      printf("%s%s%s\n", USAGE_STR, _str, _errString);
      break;
    case STATS_LEVEL:
      printf("%s%s%s\n", STATS_STR, _str, _errString);
      break;
    default:
      printf("%s%s\n", _str, _errString);
      break;
  }
#endif
  fflush(stdout);
}

/****************************************************************************
- Function name:  LrcPrintfLogFormat
- Input:
- Output:     N/A
- Return:     N/A
- Description:
- Usage:      N/A
****************************************************************************/
void LrcPrintfLogFormat(int level, char *fmt, ...)
{
  char formatedStr[5120] = "\0";
  switch (level)
  {
    case DEBUG_LEVEL:
      strcpy(formatedStr, DEBUG_STR);
      break;
    case NOTE_LEVEL:
      strcpy(formatedStr, NOTE_STR);
      break;
    case WARN_LEVEL:
      strcpy(formatedStr, WARN_STR);
      break;
    case ERROR_LEVEL:
      strcpy(formatedStr, ERROR_STR);
      break;
    case FATAL_LEVEL:
      strcpy(formatedStr, FATAL_STR);
      break;
    case USAGE_LEVEL:
      strcpy(formatedStr, USAGE_STR);
      break;
    case STATS_LEVEL:
      strcpy(formatedStr, STATS_STR);
      break;
    default:
      formatedStr[0] = 0;
      break;
  }
  strcat(formatedStr, fmt);
  
  va_list ap;
  va_start (ap, fmt);
  
  // vprintf (fmt, ap);
  vprintf (formatedStr, ap);

  va_end (ap);

  fflush(stdout);
}

/****************************************************************************
- Function name:  GetShortNumber
- Description:  Convert 2 bytes in buffer to short value
****************************************************************************/
int __attribute__ ((deprecated)) GetShortNumber(const char *buffer)
{
  t_ShortConverter converter;
  
  //converter.c[0] = buffer[0];
  //converter.c[1] = buffer[1];
  memcpy(converter.c, buffer, 2);
  
  return converter.value;
}

/****************************************************************************
- Function name:  GetShortNumberBigEndian
- Description:  Convert 2 bytes in buffer to short value
****************************************************************************/
int GetShortNumberBigEndian(const char *buffer)
{
  t_ShortConverter converter;
  
  converter.c[1] = buffer[0];
  converter.c[0] = buffer[1];
  
  return converter.value;
}

/****************************************************************************
- Function name:  GetIntNumber
- Description:  Convert 4 bytes in buffer to short value
****************************************************************************/
int __attribute__ ((deprecated)) GetIntNumber(const char *buffer)
{
  t_IntConverter converter;
  
  //converter.c[0] = buffer[0];
  //converter.c[1] = buffer[1];
  //converter.c[2] = buffer[2];
  //converter.c[3] = buffer[3];
  memcpy(converter.c, buffer, 4);
  
  return converter.value;
}

/****************************************************************************
- Function name:  GetIntNumberBigEndian
- Description:    Convert 4 bytes in buffer to short value
****************************************************************************/
int __attribute__ ((deprecated)) GetIntNumberBigEndian(const char *buffer)
{
  t_IntConverter converter;
  converter.c[3] = buffer[0];
  converter.c[2] = buffer[1];
  converter.c[1] = buffer[2];
  converter.c[0] = buffer[3];
  
  return converter.value;
}

/****************************************************************************
- Function name:  GetLongNumber
- Description:    Convert 8 bytes in buffer to long value
****************************************************************************/
long __attribute__ ((deprecated)) GetLongNumber(const char *buffer)
{
  t_LongConverter converter;
  
  memcpy(converter.c, buffer, 8);
  
  return converter.value;
}

/****************************************************************************
- Function name:  GetLongNumberBigEndian
- Description:  Convert 8 bytes in buffer to long value
****************************************************************************/
long __attribute__ ((deprecated)) GetLongNumberBigEndian(const char *buffer)
{
  t_LongConverter converter;
  
  converter.c[7] = buffer[0];
  converter.c[6] = buffer[1];
  converter.c[5] = buffer[2];
  converter.c[4] = buffer[3];
  converter.c[3] = buffer[4];
  converter.c[2] = buffer[5];
  converter.c[1] = buffer[6];
  converter.c[0] = buffer[7];
  
  return converter.value;
}

/****************************************************************************
- Function name:  GetUnsignedShortNumberBigEndian
- Description:    Convert 2 bytes in buffer to unsiged short value
****************************************************************************/
int __attribute__ ((deprecated)) GetUnsignedShortNumberBigEndian(const unsigned char *buffer)
{
  t_ShortConverter converter;
  
  converter.c[1] = buffer[0];
  converter.c[0] = buffer[1];
  
  return (unsigned short)converter.value;
}

inline unsigned short __bswap16(unsigned short x)
{
  asm("xchgb %b0, %h0" : "=Q" (x) : "0" (x));
  return x;
}

inline unsigned short __bswap16Ptr(unsigned short *ptr)
{
  unsigned short x = *ptr;
  asm("xchgb %b0, %h0" : "=Q" (x) : "0" (x));
  return x;
}

int __attribute__ ((deprecated)) GetUnsignedShortNumber(const unsigned char *buffer)
{
  t_ShortConverter converter;
  
  converter.c[0] = buffer[0];
  converter.c[1] = buffer[1];
  
  return (unsigned short)converter.value;
}

/****************************************************************************
- Function name:  GetUnsignedIntNumberBigEndian
- Description:    Convert 4 bytes in buffer to unsiged int value
****************************************************************************/
int __attribute__ ((deprecated)) GetUnsignedIntNumberBigEndian(const unsigned char *buffer)
{
  t_IntConverter converter;
  converter.c[3] = buffer[0];
  converter.c[2] = buffer[1];
  converter.c[1] = buffer[2];
  converter.c[0] = buffer[3];
  
  return (unsigned int)converter.value;
}

int __attribute__ ((deprecated)) GetUnsignedIntNumber(const unsigned char *buffer)
{
  t_IntConverter converter;
  converter.c[0] = buffer[0];
  converter.c[1] = buffer[1];
  converter.c[2] = buffer[2];
  converter.c[3] = buffer[3];
  
  return (unsigned int)converter.value;
}

/****************************************************************************
- Function name:  AddDataBlockTime
****************************************************************************/
void AddDataBlockTime(char *formattedTime)
{
  *((long*)formattedTime) = hbitime_micros() * 1000;
  /*int sec, usec;
  hbitime_micros_parts(&sec, &usec);
  
  memcpy(formattedTime, &sec, 4);
  memcpy(&formattedTime[4], &usec, 4);
  formattedTime[8] = 0;*/
}

void __iToStrWithLeftPad(int value, const char pad, const int len, char *strResult)
{
  register int i = 0;
  if (value == 0)
  {
    while (i < len) strResult[i++] = pad;
    strResult[len - 1] = '0';
    return;
  }

  register int index = 0;

  while (value != 0)
  {
    strResult[len - index - 1] = 48 + value % 10;
    value = value / 10;
    index++;
  }

  while (i < len-index) strResult[i++] = pad;
  
  return;
}

/****************************************************************************
- Function name:  Lrc_atoi
- Description:    Convert value from string data type to integer
****************************************************************************/ 

inline int Lrc_atoi(const char * buffer, int len)
{
  int retval = 0;

  while (*buffer == 32)
  {
    buffer++;
    len--;
  }
  
  for (; len > 0; buffer++, len--)
  {
    retval = retval * 10 + ((*buffer) - 48);
  }
  return retval;
}  

/****************************************************************************
- Function name:  Lrc_itoa
- Description:    Convert value from integer data type to 'string'
****************************************************************************/
int Lrc_itoa(int value, char *strResult)
{
  char temp[11];
  int index = 0;
  int i = 0;
  
  if (value > 0)
  {
    while (value != 0)
    {
      temp[index] = (48 + value % 10);
      value = value / 10; 
      index++;
    }

    for (i = index - 1; i > -1; i--)
    {
      *strResult++ = (char)temp[i];
    }

    *strResult = 0;
    
    return index;
  }
  else if (value < 0)
  {
    *strResult++ = '-';

    value = -1 * value;

    while (value != 0)
    {
      temp[index] = (char)(48 + value % 10);
      value = value / 10; 
      index++;
    }

    for (i = index - 1; i > -1; i--)
    {
      *strResult++ = (char)temp[i];
    }

    *strResult = 0;
    
    return (index + 1);
  }
  else
  {
    strResult[0] = 48;
    strResult[1] = 0;
    
    return 1;
  }
}

/****************************************************************************
- Function name:  Lrc_itoaf
- Description:  Convert value from integer data type to 'string'
****************************************************************************/
int Lrc_itoaf(int value, const char pad, const int len, char *strResult)
{
  int i = 0;
  if (value == 0)
  {
    for (i = 0; i < len; i++)
    {
      strResult[i] = pad;
    }
    strResult[len] = 0;

    return len;
  }

  int index = 0;

  while (value != 0)
  {
    strResult[len - index - 1] = 48 + value % 10;
    value = value / 10; 
    index++;
  }
  
  for (i = 0; i < len - index; i++)
  {
    strResult[i] = pad;
  }

  strResult[len] = 0;

  return len;
}

/****************************************************************************
- Function name:  Lrc_itoaf_stripped
- Description:    Convert value from integer data type to 'string'
****************************************************************************/
void Lrc_itoaf_stripped(int value, const char pad, const int len, char *strResult)
{
  int i = 0;
  if (value == 0)
  {
    for (i = 0; i < len; i++)
    {
      strResult[i] = pad;
    }
    
    return;
  }

  int index = 0;

  while (value != 0)
  {
    strResult[len - index - 1] = 48 + value % 10;
    value = value / 10; 
    index++;
  }
  
  for (i = 0; i < len - index; i++)
  {
    strResult[i] = pad;
  }

  return;
}

/****************************************************************************
- Function name:  Lrc_itoafl
- Description:    Convert value from integer data type to 'string'
****************************************************************************/
int Lrc_itoafl(int value, const char pad, const int len, char * strResult)
{
  char temp[len + 1];

  int i = 0;
  if (value == 0)
  {
    strResult[0] = '0';

    for (i = 1; i < len; i++)
    {
      strResult[i] = pad;
    }
    strResult[len] = 0;

    return len;
  }

  int index = 0;

  while (value != 0)
  {
    temp[index] = 48 + value % 10;
    value = value / 10; 
    index++;
  }

  for (i = 0; i < index; i++)
  {
    strResult[i] = temp[index - i - 1];
  }
  
  for (i = index; i < len; i++)
  {
    strResult[i] = pad;
  }

  strResult[len] = 0;
  
  return len;
}

/****************************************************************************
- Function name:  Lrc_gmtime
****************************************************************************/
inline struct tm *Lrc_gmtime (time_t *tp, int years, int secs_in_years)
{
  static int __days_per_month[] = {31,28,31,30,31,30,31,31,30,31,30,31};
  static struct tm tm2;
  time_t t;

  t = *tp;
  tm2.tm_sec  = 0;
  tm2.tm_min  = 0;
  tm2.tm_hour = 0;
  tm2.tm_mday = 1;
  tm2.tm_mon  = 0;
  tm2.tm_year = years;

  t -= secs_in_years;

  tm2.tm_yday = t / LRC_SECS_PER_DAY;       /* days since Jan 1 */

  if ( Lrc_is_leap(tm2.tm_year) )         /* leap year ? */
    __days_per_month[1]++;

  while ( t >= __days_per_month[tm2.tm_mon] * LRC_SECS_PER_DAY ) {
    t -= __days_per_month[tm2.tm_mon++] * LRC_SECS_PER_DAY;
  }
  
  tm2.tm_mon++;

  if ( Lrc_is_leap(tm2.tm_year) )         /* leap year ? */
    __days_per_month[1]--;

  tm2.tm_mday = t / LRC_SECS_PER_DAY + 1;
  t = t % LRC_SECS_PER_DAY;

  tm2.tm_hour = t / LRC_SECS_PER_HOUR;
  t = t % LRC_SECS_PER_HOUR;
  
  tm2.tm_min = t / LRC_SECS_PER_MINUTE;
  tm2.tm_sec = t % LRC_SECS_PER_MINUTE;

  return( &tm2);
}

/****************************************************************************
- Function name:  Lrc_is_leap
****************************************************************************/
inline int Lrc_is_leap(int year)
{
  year += 1900; /* Get year, as ordinary humans know it */

  /*
   *   The rules for leap years are not
   *   as simple as "every fourth year
   *   is leap year":
   */

  if( (unsigned int)year % 100 == 0 ) {
    return (unsigned int)year % 400 == 0;
  }

  return (unsigned int)year % 4 == 0;
}

/****************************************************************************
- Function name:  Get_secs_in_years
****************************************************************************/
inline int Get_secs_in_years(time_t *tp, int *year, int *secs_in_years)
{
  static struct tm tm2;
  time_t t,secs_this_year;

  t = *tp;
  tm2.tm_year = 70;

  /*
   *  This loop handles dates in 1970 and later
   */
  while ( t >= ( secs_this_year =
           Lrc_is_leap(tm2.tm_year) ?
           LRC_SECS_PER_LEAP :
           LRC_SECS_PER_YEAR ) ) {
    t -= secs_this_year;
    tm2.tm_year++;
  }

  /*
   *  This loop handles dates before 1970
   */
  while ( t < 0 )
    t += Lrc_is_leap(--tm2.tm_year) ? LRC_SECS_PER_LEAP : LRC_SECS_PER_YEAR;
  
  *secs_in_years = (*tp - t);
  *year = tm2.tm_year + 1900;

  return (*tp - t);
}

/****************************************************************************
- Function name:  GetBookIndexByName
****************************************************************************/
int GetBookIndexByName(const char *bookName)
{
  if (strncmp(bookName, "ARCA", 4) == 0)
  {
    return BOOK_ARCA;
  }
  else if (strncmp(bookName, "NASDAQ", 6) == 0)
  {
    return BOOK_NASDAQ;
  }
  else if (strncmp(bookName, "BX", 2) == 0)
  {
    return BOOK_NDBX;
  }
  else if (strncmp(bookName, "PSX", 3) == 0)
  {
    return BOOK_PSX;
  }
  else if (strncmp(bookName, "BATSZ", 5) == 0)
  {
    return BOOK_BATSZ;
  }
  else if (strncmp(bookName, "BYX", 3) == 0)
  {
    return BOOK_BYX;
  }
  else if (strncmp(bookName, "NYSE", 4) == 0)
  {
    return BOOK_NYSE;
  }
  else if (strncmp(bookName, "AMEX", 4) == 0)
  {
    return BOOK_AMEX;
  }
  else if (strncmp(bookName, "EDGX", 4) == 0)
  {
    return BOOK_EDGX;
  }
  else if (strncmp(bookName, "EDGA", 4) == 0)
  {
    return BOOK_EDGA;
  }
  else if (strncmp(bookName, "ANY", 3) == 0)
  {
    return ANY_ECN;
  }
  
  return ERROR;
}

const char* GetBookNameByIndex(int ecn_book_id)
{
  switch(ecn_book_id)
  {
    case BOOK_ARCA:   return "ARCA";
    case BOOK_NASDAQ: return "ISLD";
    case BOOK_NYSE:   return "NYSE";
    case BOOK_BATSZ:  return "BATSZ";
    case BOOK_BYX:    return "BYX";
    case BOOK_EDGX:   return "EDGX";
    case BOOK_EDGA:   return "EDGA";
    case BOOK_NDBX:   return "BX";
    case BOOK_PSX:    return "PSX";
    case BOOK_AMEX:   return "AMEX";
  }
  
  return "UNKNOWN";
}

const char* GetOrderNameByIndex(int ecn_order_id)
{
  switch(ecn_order_id)
  {
    case ORDER_ARCA_DIRECT: return "ARCA";
    case ORDER_NASDAQ_OUCH: return "ISLD";
    case ORDER_NASDAQ_RASH: return "RASH";
    case ORDER_NYSE_CCG:    return "NYSE";
    case ORDER_BATSZ_BOE:   return "BATSZ";
    case ORDER_BYX_BOE:     return "BYX";
    case ORDER_EDGX:        return "EDGX";
    case ORDER_EDGA:        return "EDGA";
    case ORDER_NASDAQ_BX:   return "BX";
    case ORDER_NASDAQ_PSX:  return "PSX";
  }
  
  return "UNKNOWN";
}

/****************************************************************************
- Function name:  GetServerIndexByName
****************************************************************************/
int GetServerIndexByName(const char *serverRole)
{
  if (strncasecmp(serverRole, "ARCA", 4) == 0)
  {
    return SERVER_ROLE_ARCA;
  }
  else if (strncasecmp(serverRole, "NASDAQ", 6) == 0)
  {
    return SERVER_ROLE_NASDAQ;
  }
  else if (strncasecmp(serverRole, "BATS", 4) == 0)
  {
    return SERVER_ROLE_BATS;
  }
  else if (strncasecmp(serverRole, "EDGE", 4) == 0)
  {
    return SERVER_ROLE_EDGE;
  }
  else if (strncasecmp(serverRole, "STAND_ALONE", 11) == 0)
  {
    return SERVER_ROLE_STAND_ALONE;
  }
  
  return ERROR;
}

/*************************************************
- Function Name:  GetTradeSessionIndexByName
- Input:      
- Output:     
- Return:         
- Description:   
- Usage:   
*************************************************/
int GetTradeSessionIndexByName(const char *tradSession)
{
  if (strncasecmp(tradSession, PRE_MARKET_SESSION, strlen(PRE_MARKET_SESSION)) == 0)
  {
    return PRE_MARKET;
  }
  else if (strncasecmp(tradSession, INTRADAY_SESSION, strlen(INTRADAY_SESSION)) == 0)
  {
    return INTRADAY;
  }
  else if (strncasecmp(tradSession, POST_MARKET_SESSION, strlen(POST_MARKET_SESSION)) == 0)
  {
    return POST_MARKET;
  }
  
  return ERROR;
}

/*************************************************
- Function Name:  GetMacAddress
- Input:      
- Output:     
- Return:         ERROR or SUCCESS
- Description:   
- Usage:   
*************************************************/
int GetMacAddress(char *ifName, char *mac)
{
  struct ifreq s;
  int fd = socket(PF_INET, SOCK_DGRAM, IPPROTO_IP);
  if (fd == -1) return ERROR;
  char tmp[6];
  strcpy(s.ifr_name, ifName);
  if (0 == ioctl(fd, SIOCGIFHWADDR, &s))
  {
    memcpy(tmp, s.ifr_addr.sa_data, 6);
    sprintf(mac, "%02X:%02X:%02X:%02X:%02X:%02X", (unsigned char) tmp[0], (unsigned char)tmp[1], (unsigned char)tmp[2], (unsigned char)tmp[3], (unsigned char)tmp[4], (unsigned char)tmp[5]);
    close(fd);
    return SUCCESS;
  }
  
  close(fd);
  return ERROR;
}

/*************************************************
- Function Name:  GetIPAddress
- Input:      
- Output:     
- Return:         
- Description:   
- Usage:   
*************************************************/
int GetIPAddress(char *if_name, char *ip)
{
  struct ifreq ifr;
  size_t if_name_len = strlen(if_name);
  
  if (if_name_len < sizeof(ifr.ifr_name))
  {
    memcpy(ifr.ifr_name, if_name, if_name_len);
    ifr.ifr_name[if_name_len] = 0;
  }
  else
  {
    TraceLog(ERROR_LEVEL, "Interface name is too long\n");
    ip[0] = 0;
    return ERROR;
  }
  
  int fd = socket(AF_INET, SOCK_DGRAM, 0);
  if (fd == -1)
  {
    PrintErrStr(ERROR_LEVEL, "GetIPAddress(): ", errno);

    ip[0] = 0;
    return ERROR;
  }
  
  ifr.ifr_addr.sa_family = AF_INET; /* I want to get an IPv4 IP address */
  
  if (ioctl(fd, SIOCGIFADDR, &ifr) == -1)
  {
    PrintErrStr(ERROR_LEVEL, "ioctl(): ", errno);
    ip[0] = 0;
    return ERROR;
  }
  close(fd);
  
  struct sockaddr_in* ipaddr = (struct sockaddr_in*)&ifr.ifr_addr;
  strcpy(ip, inet_ntoa(ipaddr->sin_addr));
  
  return SUCCESS;
}

/*************************************************
- Function Name:  JoinMulticasGroup
- Return:         socket descriptor
- Description:   
- Usage:   
*************************************************/
int JoinMulticastGroup(char *ipAddress, int port, int currentSocket)
{
  int sd;
  struct sockaddr_in cliAddr;
  
  if (currentSocket == -1)
  {
    /* create socket */
    sd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sd < 0)
    {
      TraceLog(ERROR_LEVEL, "Could not open socket\n");
      return ERROR;
    }
    
    // Set SO_REUSEADDR on a socket to true (1):
    int optval = 1;
    
    if (setsockopt(sd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof optval) != SUCCESS)
    {
      TraceLog(ERROR_LEVEL, "Could not set SO_REUSEADDR for socket (%s - %d)\n", ipAddress, port);
      return ERROR;
    }
    
    int rcvBuffSize = 16777216; // 
    if (setsockopt(sd, SOL_SOCKET, SO_RCVBUF, &rcvBuffSize, sizeof rcvBuffSize) != SUCCESS)
    {
      TraceLog(ERROR_LEVEL, "Could not set SO_RCVBUF for socket (%s - %d)\n", ipAddress, port);
      return ERROR;
    }

    /* bind any port number */
    cliAddr.sin_family = AF_INET;
    cliAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    cliAddr.sin_port = htons(port);
    
    
    if (bind(sd, (struct sockaddr *) &cliAddr, sizeof(cliAddr)) < 0)
    {
      TraceLog(ERROR_LEVEL, "Could not bind to port: %d\n", port);
      return -1;
    }
  }
  else
  {
    sd = currentSocket;
    TraceLog(DEBUG_LEVEL, "Current Socket = %d\n", currentSocket);
  }
  
  struct sockaddr_in remoteServAddr;
  struct hostent *h;
  struct ip_mreq mreq;
  
  //Check ip address
  h = gethostbyname(ipAddress); 

  if (h == NULL)
  {
    TraceLog(ERROR_LEVEL, "Unknown host '%s'\n", ipAddress);
    return -1;
  }
  
  memcpy((char *) &remoteServAddr.sin_addr.s_addr, h->h_addr_list[0], h->h_length);
  
  mreq.imr_multiaddr.s_addr = remoteServAddr.sin_addr.s_addr;
  mreq.imr_interface.s_addr = htonl(INADDR_ANY);
  
  //Join 1 multicast group
  if (setsockopt(sd, IPPROTO_IP, IP_ADD_MEMBERSHIP, (void *) &mreq, sizeof(mreq)) < 0)
  {
    TraceLog(ERROR_LEVEL, "Could not join multicast group '%s'\n", inet_ntoa(remoteServAddr.sin_addr));
    return -1;
  }
  else
  {
    //TraceLog(DEBUG_LEVEL, "join successfully from multicast group '%s' and port = %u\n", inet_ntoa(remoteServAddr.sin_addr), htons(remoteServAddr.sin_port));
  }

  return sd;
}

/****************************************************************************
- Function name:  CheckErrno
- Input:      
- Output:                     
- Return:     
- Description:                        
- Usage:      
****************************************************************************/
int CheckErrno(int errno_args)
{
  switch (errno_args)
  {
    case EAGAIN:
      //TraceLog(ERROR_LEVEL, "The socket's file descriptor is marked O_NONBLOCK and no data is waiting to be received\n");
      return SUCCESS;
    case EIO:
      TraceLog(ERROR_LEVEL, "An I/O error occurred while reading from or writing to the file system\n");
      return ERROR;
    case ENOBUFS:
      TraceLog(ERROR_LEVEL, "Insufficient resources were available in the system to perform the operation\n");
      return ERROR;
    case ENOMEM:
      TraceLog(ERROR_LEVEL, "Insufficient memory was available to fulfill the request\n");
      return ERROR;
    case 88:
      TraceLog(ERROR_LEVEL, "Socket operation in non-socket\n");
      return ERROR;
    case 104:
      TraceLog(ERROR_LEVEL, "Connection reset by peer\n");
      return ERROR;
    default:
    {
      char msg[1024] = "\0";
  
      strerror_r(errno_args, msg, 1024);
  
      TraceLog(WARN_LEVEL, "Ignore errno %d (%s)\n", errno_args, msg);
      break;
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  GetLocalTimeString
- Input:      
- Output:   
- Return:   
- Description:
- Usage:    
****************************************************************************/
char *GetLocalTimeString(char *timeString)
{
  int seconds, micros;
  hbitime_micros_parts(&seconds, &micros);
  time_t now = seconds;
  struct tm *l_time = (struct tm *)localtime(&now);

  // Get time string
  strftime(timeString, 16, "%H:%M:%S", l_time);
  sprintf(timeString + strlen(timeString), ".%06d", micros);

  return timeString;
}

/****************************************************************************
- Function name:  GetTimeFromEpoch
- Input:      
- Output:   
- Return:   
- Description:    Return microseconds from Epoch (1970)
- Usage:    
****************************************************************************/
long __attribute__ ((deprecated)) GetTimeFromEpoch()
{
  struct timeval tv;  
  gettimeofday(&tv, NULL);
  return (tv.tv_sec * 1000000 + tv.tv_usec);
}

/****************************************************************************
- Function name:  GetNumberOfSecondFromTimeString
- Input:      
- Output:   
- Return:     The number of second from time format hh:mm:ss
- Description:    
- Usage:    
****************************************************************************/
int GetNumberOfSecondFromTimeString(const char *timeString)
{
  // time string format: hh:mm:ss
  struct tm tm;
  memset(&tm, 0, sizeof(struct tm));
  char *retVal = strptime(timeString, "%H:%M:%S", &tm);
  if (retVal == NULL)
  {
    return -1;
  }
  
  int numSeconds = tm.tm_sec + tm.tm_min * 60 + tm.tm_hour * 3600;
  return numSeconds;
}

long GetEpochNanosFromTimeString(const char *timeString)
{
  struct tm tmp, real;
  
  // parse time values from string
  memset(&real, 0, sizeof(struct tm));
  char *retVal = strptime(timeString, "%H:%M:%S", &real);
  if (retVal == NULL)
  {
    return -1;
  }
  
  time_t now = hbitime_seconds(); // get *adjusted* time since epoch
  localtime_r(&now, &tmp);        // convert to localtime
  
  // modify hr:min:sec to the desired values
  tmp.tm_hour = real.tm_hour;
  tmp.tm_min  = real.tm_min;
  tmp.tm_sec  = real.tm_sec;
  
  // convert the back to epoch time
  long sec = mktime(&tmp);
  return sec * 1000000000;
}

/****************************************************************************
- Function name:  GetTimeInSecondsFromFile
- Input:      
- Output:   
- Return:   
- Description:
- Usage:    
****************************************************************************/
int GetTimeInSecondsFromFile(const char *fullPath, int *numSeconds, const char *key)
{
  char const* timeString = Parameters_get(fullPath, key, 0);
  if (timeString != 0)
  {
    *numSeconds = GetNumberOfSecondFromTimeString(timeString);
    if (*numSeconds == -1)
    {
      TraceLog(ERROR_LEVEL, "Invalid time of '%s' from file '%s'\n", key, fullPath);
      return -1;
    }
  }
  else
  {
    TraceLog(ERROR_LEVEL, "Could not get '%s' from file '%s'\n", key, fullPath);
    return -1;
  }
  
  return 0;
}

int GetTimeInEpochNanosFromFile(const char *fullPath, long *numNanos, const char *key)
{
  char const* timeString = Parameters_get(fullPath, key, 0);
  if (timeString != 0)
  {
    *numNanos = GetEpochNanosFromTimeString(timeString);
    if (*numNanos == -1)
    {
      TraceLog(ERROR_LEVEL, "Invalid time of '%s' from file '%s'\n", key, fullPath);
      return -1;
    }
  }
  else
  {
    TraceLog(ERROR_LEVEL, "Could not get '%s' from file '%s'\n", key, fullPath);
    return -1;
  }
  
  return 0;
}

/****************************************************************************
- Function name:  TrimRight
- Input:      + buffer
- Output:   
- Return:   
- Description:  + 
- Usage:    
****************************************************************************/
int TrimRight( char * buffer )
{
  unsigned int i;
  for( i = 0; i < strlen( buffer ); i ++ )
  {
    if( buffer[i] == ' ' )
    {
      buffer[i] = 0;

      break;
    } 
  }
  
  return 0;
}

/****************************************************************************
- Function name:  CreateAbsolutePath
- Input:      + Absolute path dir
- Output:     N/A
- Return:     SUCESS or Failure
- Description:
- Usage:    
****************************************************************************/
int CreateAbsolutePath(const char *path)
{
  char paths[10][32];
  int i, len;
  int tokenCount = 0;
  int offset = 0;
  
  memset(paths, 0, sizeof(paths));
  
  if (path[0] != '/')
  {
    TraceLog(ERROR_LEVEL, "This is not an absolute path (%s)\n", path);
    return ERROR;
  }
  
  //Split the full path to tokens
  len = strlen(path);
  for (i = 1; i < len; i++)
  {
    if (path[i] == '/')
    {
      tokenCount ++;
      offset = 0;
      continue;
    }
    
    paths[tokenCount][offset] = path[i];
    offset++;
  }
  
  //Now we create path by path
  int ret;
  char directory[512];
  memset(directory, 0, 512);

  for (i = 0; i <= tokenCount; i++)
  {
    if (paths[i][0] != 0)
    {
      sprintf(directory, "%s/%s", directory, paths[i]);
      
      TraceLog(DEBUG_LEVEL, "Creating: %s\n", directory);
      ret = mkdir(directory, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
      
      if((ret == -1) && (errno != 17))
      {
        PrintErrStr(ERROR_LEVEL, "mkdir(): ", errno);
        return ERROR;
      }
    }
    else
    {
      TraceLog(DEBUG_LEVEL, "Creating directory is done\n");
      return SUCCESS;
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  strcncpy
- Input:      + Absolute path dir
- Output:     N/A
- Return:     SUCESS or Failure
- Description:
- Usage:    
****************************************************************************/
inline void strcncpy(unsigned char *des, const char *src, char pad, int totalLen)
{
  while(totalLen--)
  {
    if (*src != 0)
    {
      *des++ = *src++;
    }
    else
    {
      *des++ = pad;
    }
  }
}

/****************************************************************************
- Function name:  getRand
- Input:      
- Output:     
- Return:     
- Description:  
- Usage:    
****************************************************************************/
int getRand(int maxValue)
{
  if (maxValue == 0)
  {
    return 0;
  }
  
  //srand(time(NULL));
  return (rand() % maxValue);
}

/****************************************************************************
- Function name:  IsFileTimestampToday
- Input:      full file path
- Output:     
- Return:     -1: could not get current time
           0: not same date
           1: same date
           2: file does not exist
- Description:    Check file modification time to see if it's of the same 
          date of current date or not
- Usage:    
****************************************************************************/
int IsFileTimestampToday(char *fullPath)
{
  //Check if file already existed
  if (access(fullPath, F_OK) != 0)
  {
    return 2;
  }
  
  struct stat fileInfo;
  stat(fullPath, &fileInfo);
  
  // Check modification time fileInfo.st_mtime vs current date
  // Convert to local time
  struct tm fileTime;
  time_t seconds = fileInfo.st_mtime - (time_t)hbitime_get_offset_seconds();
  localtime_r(&seconds, &fileTime);
  
  //Get current date
  time_t currentTime = hbitime_seconds();
  
  struct tm local;
  // Convert to local time
  localtime_r(&currentTime, &local);
  
  if ((local.tm_year != fileTime.tm_year) ||
      (local.tm_mday != fileTime.tm_mday) ||
      (local.tm_mon != fileTime.tm_mon))
  {
    return 0;
  }
  
  return 1;
}

/****************************************************************************
- Function name:  LoadArcaSymbolsFromMappingFileToList
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int LoadArcaSymbolsFromMappingFileToList(char *fileName, int *count, char arcaSymbolList[][9], int arcaSymbolIndex[])
{
  char fullPath[MAX_PATH_LEN] = "\0";
  
  if (GetFullConfigPath(fileName, CONF_SYMBOL, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Have some problem in getting config path for file: '%s'\n", fileName);
    return ERROR;
  }
  
  //---------------------------------------------------------------------------------
  // Check if file is downloaded today
  //---------------------------------------------------------------------------------
  int ret = IsFileTimestampToday(fullPath);
  if (ret == 2)
  {
    TraceLog(ERROR_LEVEL, "Could not find symbol mapping file: %s\n", fullPath);
    return ERROR;
  }
  else if (ret != 1)
  {
    TraceLog(ERROR_LEVEL, "%s is old, please redownload the file\n", fullPath);
    return ERROR;
  }
      
  FILE *fp = fopen(fullPath, "r");
  if (fp == NULL)
  {
    TraceLog(ERROR_LEVEL, "Critical! Could not open ARCA Symbol Index Mapping file (%s). Trade Server can not continue.\n", fullPath);
    PrintErrStr(ERROR_LEVEL, "calling fopen(): ", errno);
    sleep(3); exit(0);
    return ERROR;
  }
  
  char line[512];
  memset(line, 0, 512);
  
  char *b, *e;
  
  while (fgets(line, 512, fp) != NULL)
  {
    //Parse the line
    /*  Line format:
         Symbol|CQS symbol|SymbolIndex|NYSE Market|Listed Market|TickerDesignation|UOT|PriceScaleCode|SystemID|Bloomberg BSID|Bloomberg Global ID
      Example of a line:
           ALP PRP|ALPpP|4421|N|N|A|100|4|0|627065504986|BBG000007MM8
      We need only following info: CQS symbol, Symbol Index + PriceScaleCode
    */
    
    /*Get CQS symbol*/
    b = strchr(line, '|');
    if (b == NULL)
    {
      TraceLog(ERROR_LEVEL, "ARCA Symbol Mapping file: Wrong line format: '%s'\n", line);
      memset(line, 0, 512);
      continue;
    }
    b++;
    
    //CQS symbol|SymbolIndex
    e = strchr(b, '|');
    if (e == NULL)
    {
      TraceLog(ERROR_LEVEL, "ARCA Symbol Mapping file: Wrong line format: '%s'\n", line);
      memset(line, 0, 512);
      continue;
    }
    *e = 0;
    
    //Now take the CQS format symbol
    char arcaSymbol[32];
    memset (arcaSymbol, 0, 32);
    strncpy(arcaSymbol, b, 31);
    
    char symbol[SYMBOL_LEN];
    memset(symbol, 0, SYMBOL_LEN);
    strncpy(symbol, arcaSymbol, SYMBOL_LEN);
    
    *e = '|';
  
    /*Get symbol index*/
    b = ++e;
    e = strchr(b, '|');
    if (e == NULL)
    {
      TraceLog(ERROR_LEVEL, "ARCA Symbol Mapping file: Wrong line format: '%s'\n", line);
      memset(line, 0, 512);
      continue;
    }
    *e=0;
    int symbolIndex = atoi(b);
    
    /***********************************************************
      Finish parsing: arca symbol/cqs symbol/symbol index
    ***********************************************************/
    if (symbolIndex <= 0 || symbolIndex >= MAX_ARCA_STOCK_SYMBOL)
    {
      TraceLog(ERROR_LEVEL, "ARCA symbol mapping: stock symbol (%.8s) index (%d) less than 0 or greater than defined const MAX_ARCA_STOCK_SYMBOL (%d)\n", symbol, symbolIndex, MAX_ARCA_STOCK_SYMBOL);
      memset(line, 0, 512);
      continue;
    }
    
    if ((strlen(arcaSymbol) > SYMBOL_LEN) || strstr(arcaSymbol, ".TEST"))
    {
      TraceLog(WARN_LEVEL, "ARCA XDP: Ignore symbol: %s\n", arcaSymbol);
      memset(line, 0, 512);
      continue;
    }
    
    if (SplitSymbolMapping[(int)symbol[0]] == DISABLE)
    {
      //This symbol is not in the symbol split range that current Trade Server is in
      memset(line, 0, 512);
      continue;
    }
    
    // Update Symbol String
    int stockSymbolIndex = GetStockSymbolIndex(symbol);
      
    // If stock symbol is not in SymbolList
    if (stockSymbolIndex == -1)
    {
      // Try to add it to Symbol List
      stockSymbolIndex = UpdateStockSymbolIndex(symbol);
    }

    if (stockSymbolIndex != -1)
    {
      strncpy(arcaSymbolList[*count], symbol, SYMBOL_LEN);
      arcaSymbolList[*count][8] = 0;
      arcaSymbolIndex[*count] = symbolIndex;
      *count = ((*count) + 1);
    }
    
    memset(line, 0, 512);
  }

  fclose(fp);
  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadNyseSymbolsFromMappingFileToList
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int LoadNyseSymbolsFromMappingFileToList(char *fileName, int *nyseCount, int *amexCount, char nyseSymbolList[][9], char amexSymbolList[][9], int nyseSymbolIndex[], int amexSymbolIndex[])
{
  char fullPath[MAX_PATH_LEN] = "\0";

  if (GetFullConfigPath(fileName, CONF_SYMBOL, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Have some problem in getting config path for file: '%s'\n", fileName);
    return ERROR;
  }

  //Check if file already existed
  if (access(fullPath, F_OK) != 0)
  {
    TraceLog(ERROR_LEVEL, "Could not find symbol mapping file: %s\n", fullPath);
    return ERROR;
  }

  //---------------------------------------------------------------------------------
  // Check if file is downloaded today
  //---------------------------------------------------------------------------------
  int ret = IsFileTimestampToday(fullPath);
  if (ret == 2)
  {
    TraceLog(ERROR_LEVEL, "Could not find symbol mapping file: %s\n", fullPath);
    return ERROR;
  }
  else if (ret != 1)
  {
    TraceLog(ERROR_LEVEL, "%s is old, please redownload the file\n", fullPath);
    return ERROR;
  }

  //---------------------------------------
  // Process the file for index mapping
  //---------------------------------------
  FILE *fp = fopen(fullPath, "r");
  if (fp == NULL)
  {
    TraceLog(ERROR_LEVEL, "(critical)! Could not open file (%s).\n", fullPath);
    PrintErrStr(ERROR_LEVEL, "calling fopen(): ", errno);
    return ERROR;
  }

  /* ******************************************
   * XML NYSE Symbol Index Mapping File format
   * <SymbolMappingFile>
   *   <SymbolMap>
   *     <Symbol>ABW PRA</Symbol><Index>1573</Index><Channel>AA</Channel><ExchangeID>N</ExchangeID>
   *   </SymbolMap>
   *   ..........
   * </SymbolMappingFile>
   * *******************************************/

  char line[512];
  memset(line, 0, 512);

  char *pBegin, *pEnd;
  char *strSymbolFlag = "<Symbol>";
  char isIgnored = 0;
  int len = strlen(strSymbolFlag);

  char tempChar;
  char nyseSymbol[32];

  while (fgets(line, 512, fp) != NULL)
  {
    isIgnored = 0;
    
    pBegin = strchr(line, '<'); //get the first tag
    if (pBegin == NULL)
    {
      TraceLog(ERROR_LEVEL, "NYSE Symbol Index Mapping file: Wrong line format: %s\n", line);
      memset(line, 0, 512);
      continue;
    }

    // Check line contains <Symbol> or not
    if (strncmp(pBegin, strSymbolFlag, len) != 0)
    {
      // line does not contain strSymbolFlag, so ignore it
      memset(line, 0, 512);
      continue;
    }

    pBegin += len; //move to the first character of Symbol
    pEnd = strchr(pBegin, '<'); //find the close of Tag Symbol
    if(pEnd == NULL)
    {
      TraceLog(ERROR_LEVEL, "NYSE Symbol Index Mapping file: Wrong line format: %s\n", line);
      memset(line, 0, 512);
      continue;
    }

    /**********************Get Stock Symbol**************************/
    tempChar = *pEnd;
    *pEnd = '\0';
    
    strncpy(nyseSymbol, pBegin, 31); //copy symbol string includes SYMBOL & ROOT
    nyseSymbol[31] = 0;
    
    if (strlen(nyseSymbol) > 15)
    {
      memset(line, 0, 512); //ignore long symbol
      continue;
    }
    
    *pEnd = tempChar; //return orginal character

    char symbol[SYMBOL_LEN];
    
    if (ConvertCMSSymbolToComstock(nyseSymbol, symbol) == ERROR)
    {
      isIgnored = 1;
    }

    if (SplitSymbolMapping[(int)nyseSymbol[0]] == DISABLE)
    {
      isIgnored = 1;
    }
    
    /**********************Get Symbol Index**************************/
    pEnd = strchr(pBegin, '<');//find the close of Tag Symbol
    pEnd++;
    pBegin = strchr(pEnd, '<');//find the open of Tag Index
    if(pBegin == NULL) {memset(line, 0, 512); continue;}
    
    pEnd = strchr(pBegin, '>');
    if(pEnd == NULL) {memset(line, 0, 512); continue;}
    pBegin = pEnd + 1;//move the the first digit of Index
    pEnd = strchr(pBegin, '<');//find the close of Tag Index
    if(pEnd == NULL)
    {
      TraceLog(ERROR_LEVEL, "NYSE Symbol Index Mapping file: Wrong line format: %s\n", line);
      memset(line, 0, 512);
      continue;
    }
    *pEnd = 0;
    int symbolIndex = atoi(pBegin);
    *pEnd = '<'; //return to its original character

    /**********************Get Channel******************************/
    /*pBegin = strchr(++pEnd, '<'); //find the open tag of Tag Channel
    pEnd = strchr(pBegin, '>');
    pBegin = pEnd + 1;  //pBegin is point to the first character of ChannelId
    pEnd = strchr(pBegin, '<'); //find the close tag of Tag Channel
    *pEnd = 0; //set to null for copying string
    char strChannel[3];
    strncpy(strChannel, pBegin, 3);

    if (strcmp(strChannel, "AZ") == 0 || strcmp(strChannel, "") == 0)
    {
      //ignore MKT Securities & Uknown Channel
      memset(line, 0, 512);
      continue;
    }

    *pEnd = '<'; //return to its original value
    */

    /**********************Identify the <ExchangeID> *********************/
    pBegin = strstr(pEnd, "<ExchangeID>");
    if (pBegin == NULL)
    {
      TraceLog(ERROR_LEVEL, "NYSE Symbol Index Mapping file: Wrong line format (could not find <ExchangeID>): %s\n", line);
      memset(line, 0, 512);
      continue;
    }
    pBegin += 12;
    int mdcId;
    if (*pBegin == 'N')
      mdcId = BOOK_NYSE;
    else if (*pBegin == 'A')
      mdcId = BOOK_AMEX;
    else
    {
      TraceLog(ERROR_LEVEL, "NYSE Symbol Index Mapping file: Wrong line format (Unknown <ExchangeID>): %s\n", line);
      memset(line, 0, 512);
      continue;
    }
    
    /**********************Add to Data Structure*************************/
    if (isIgnored == 1)
    {
      memset(line, 0, 512);
      continue;
    }

    if (symbolIndex <= 0 || symbolIndex >= MAX_NYSE_BOOK_STOCK_SYMBOL)
    {
      TraceLog(ERROR_LEVEL, "NYSE symbol mapping: stock symbol (%.8s) index (%d) less than 0 or greater than defined const MAX_NYSE_BOOK_STOCK_SYMBOL (%d)\n",
                            symbol, symbolIndex, MAX_NYSE_BOOK_STOCK_SYMBOL);
      memset(line, 0, 512);
      continue;
    }

    // Update Symbol String
    int stockSymbolIndex = GetStockSymbolIndex(symbol);
    if (stockSymbolIndex == -1)
    {
      stockSymbolIndex = UpdateStockSymbolIndex(symbol);
    }

    // Check duplicate stock symbol
    if (stockSymbolIndex != -1)
    {
      if (mdcId == BOOK_NYSE)
      {
        strncpy(nyseSymbolList[*nyseCount], symbol, SYMBOL_LEN);
        nyseSymbolList[*nyseCount][8] = 0;
        nyseSymbolIndex[*nyseCount] = symbolIndex;
        *nyseCount = ((*nyseCount) + 1);
      }
      else  //amex
      {
        strncpy(amexSymbolList[*amexCount], symbol, SYMBOL_LEN);
        amexSymbolList[*amexCount][8] = 0;
        amexSymbolIndex[*amexCount] = symbolIndex;
        *amexCount = ((*amexCount) + 1);
      }
      
      SymbolMgmt.symbolList[stockSymbolIndex].mdcId = mdcId;
    }

    memset(line, 0, 512);
  }

  fclose(fp);
  return SUCCESS;
}

int IsSameMacAddress(char *mac, char *interface)
{
  char tmpMac[32];
  if (GetMacAddress(interface, tmpMac) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Could not get MAC Address for interface named '%s'\n", interface);
    return 0;
  }
  
  if (strcmp(mac, tmpMac) != 0)
  {
    return 0;
  }
  
  return 1;
}
