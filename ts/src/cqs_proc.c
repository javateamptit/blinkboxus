/****************************************************************************
** Project name:  Equity Arbitrage Application
** File name:     CQS_proc.c
** Description:   This file contains function definitions that were declared
          in CQS_proc.h
** Author:      Luan Vo-Kinh
** First created on Oct 03, 2008
** Last updated on Oct 03, 2008
****************************************************************************/

#define _BSD_SOURCE

#include <pthread.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>

#include "utility.h"
#include "quote_mgmt.h"
#include "stock_symbol.h"
#include "cqs_proc.h"
#include "kernel_algorithm.h"
#include "logging.h"

/****************************************************************************
          GLOBAL VARIABLES
****************************************************************************/
static int CQS_DenominatorValue[26];
static int CQS_NumeratorByte[26];
extern t_MonitorCQS MonitorCQS;

/****************************************************************************
          LIST OF FUNCTIONS
****************************************************************************/

/****************************************************************************
- Function name:  InitCQS_Quote_DataStructure
- Input:      
- Output:                     
- Return:     
- Description:                                    
- Usage:      
****************************************************************************/
void InitCQS_Quote_DataStructure(void)
{ 
  int symbolIndex, exchangeIndex;
  for (symbolIndex = 0; symbolIndex < MAX_STOCK_SYMBOL; symbolIndex++)
  {
    for (exchangeIndex = 0; exchangeIndex < MAX_EXCHANGE; exchangeIndex++)
    {
      memset(&BBOQuoteMgmt[symbolIndex].BBOQuoteInfo[CQS_SOURCE][exchangeIndex], 0, sizeof(t_BBOQuoteInfo));
    }
    
    memset(&BBOQuoteMgmt[symbolIndex].nationalBBO[CQS_SOURCE], 0, sizeof(t_NationalAppendage));
  }
}

/****************************************************************************
- Function name:  InitCQS_DataStructure
- Input:      
- Output:                     
- Return:     
- Description:                                    
- Usage:      
****************************************************************************/
void InitCQS_DataStructure(void)
{ 
  TraceLog(DEBUG_LEVEL, "Initialize CQS Feed\n");

  // Initialize denominator value
  CQS_DenominatorValue[1] = 1;
  CQS_DenominatorValue[2] = 1;
  CQS_DenominatorValue[9] = 1;
  CQS_DenominatorValue[10] = 1;
  CQS_DenominatorValue[11] = 1;
  CQS_DenominatorValue[12] = 1;
  CQS_DenominatorValue[13] = 1;
  CQS_DenominatorValue[14] = 1;
  CQS_DenominatorValue[15] = 1;
  CQS_DenominatorValue[16] = 1;
  CQS_NumeratorByte[1] = 1;
  CQS_NumeratorByte[2] = 1;
  CQS_NumeratorByte[9] = 1;
  CQS_NumeratorByte[10] = 1;
  CQS_NumeratorByte[11] = 1;
  CQS_NumeratorByte[12] = 1;
  CQS_NumeratorByte[13] = 1;
  CQS_NumeratorByte[14] = 1;
  CQS_NumeratorByte[15] = 1;
  CQS_NumeratorByte[16] = 1;
  
  CQS_DenominatorValue[0] = 1;
  CQS_DenominatorValue[3] = 8;
  CQS_DenominatorValue[4] = 16;
  CQS_DenominatorValue[5] = 32;
  CQS_DenominatorValue[6] = 64;
  CQS_DenominatorValue[7] = 128;
  CQS_DenominatorValue[8] = 256;
  CQS_DenominatorValue[17] = 10;
  CQS_DenominatorValue[18] = 100;
  CQS_DenominatorValue[19] = 1000;
  CQS_DenominatorValue[20] = 10000;
  CQS_DenominatorValue[21] = 100000;
  CQS_DenominatorValue[22] = 1000000;
  CQS_DenominatorValue[23] = 10000000;
  CQS_DenominatorValue[24] = 100000000;
  CQS_DenominatorValue[25] = 1;
  
  CQS_NumeratorByte[0] = 1;
  CQS_NumeratorByte[3] = 1;
  CQS_NumeratorByte[4] = 2;
  CQS_NumeratorByte[5] = 2;
  CQS_NumeratorByte[6] = 2;
  CQS_NumeratorByte[7] = 3;
  CQS_NumeratorByte[8] = 3;
  CQS_NumeratorByte[17] = 1;
  CQS_NumeratorByte[18] = 2;
  CQS_NumeratorByte[19] = 3;
  CQS_NumeratorByte[20] = 4;
  CQS_NumeratorByte[21] = 5;
  CQS_NumeratorByte[22] = 6;
  CQS_NumeratorByte[23] = 7;
  CQS_NumeratorByte[24] = 8;
  CQS_NumeratorByte[25] = 0;
  
  // Init quote list
  InitCQS_Quote_DataStructure();
  
  int i;
  MonitorCQS.procCounter = 0;
  for (i = 0; i < MAX_CQS_MULTICAST_LINES; i++)
  {
    MonitorCQS.recvCounter[i] = 0;
    CQS_Conf.group[i].index = i;
  
    //Enable group permission
    CQS_Conf.group[i].groupPermission = SymbolSplitInfo[AlgorithmConfig.splitConfig.symbolRangeID].cqs[i];
  }
}

unsigned short GetCQSMsgLen(char *buffer)
{
  unsigned short msgSize = 0;
  
  switch (buffer[0])    // Category
  {
    case 'E':
      if (buffer[1] == 'B') //Equity long quote
      {
        msgSize = 24 + 78;
        
        // National BBO Indicator
        if ((buffer[MAX_CQS_HEADER_LEN + 76] - 48) == 4) //Long appendage
          msgSize = msgSize + 58;
        else if ((buffer[MAX_CQS_HEADER_LEN + 76] - 48) == 6) //Short appendage
          msgSize = msgSize + 28;
        
        // FINRA BBO Indicator
        if ((buffer[MAX_CQS_HEADER_LEN + 77] - 48) == 3) //There is appendage
          msgSize = msgSize + 56;
          
      }
      else if (buffer[1] == 'D') // Equity price short quote
      {
        msgSize = 24 + 34;
        
        // National BBO Indicator
        if ((buffer[MAX_CQS_HEADER_LEN + 32] - 48) == 4) //Long appendage
          msgSize = msgSize + 58;
        else if ((buffer[MAX_CQS_HEADER_LEN + 32] - 48) == 6) //Short appendage
          msgSize = msgSize + 28;
        
        // FINRA BBO Indicator
        if ((buffer[MAX_CQS_HEADER_LEN + 33] - 48) == 3) //There is appendage
          msgSize = msgSize + 56;
      }
      else
      {
        //Some other messages belong to category 'E' but not documented !
        TraceLog(ERROR_LEVEL, "CQS: Unknown msgType (%c - %c)\n", buffer[0], buffer[1]);
        msgSize = 0;
      }
      
      break;
    case 'A':
      TraceLog(ERROR_LEVEL, "CQS: We should not receive any message from category 'A'!\n");
      msgSize = 0;
      break;
    case 'B':
      TraceLog(ERROR_LEVEL, "CQS: We should not receive any message from category 'B'!\n");
      msgSize = 0;
      break;
    case 'M':
      msgSize = 46 + 24;
      break;
    case 'L':
      TraceLog(ERROR_LEVEL, "CQS: We should not receive any message from category 'L'!\n");
      msgSize = 0;
      break;
    case 'C':
      msgSize = 24;
      switch (buffer[1])
      {
        case 'L': //Reset sequence number
        case 'C': // FINRA Close message --> Remove quotes
        case 'T': // for "Line integrity"
        case 'M': // for "Start of Test Cycle"
        case 'I': // for "Start of Day"
        case 'N': // for "End of Test Cycle"
        case 'Z': // for "End of Transmission"
        case 'O': // FINRA Open
          break;
        default:
          TraceLog(ERROR_LEVEL, "CQS other message: category = %c, msgType = %c\n", buffer[0], buffer[1]);
          msgSize = 0;
          break;
      }
      break;
    default:
      TraceLog(ERROR_LEVEL, "CQS other message: category = %c, msgType = %c, msg = '%s'\n", buffer[0], buffer[1]);
      msgSize = 0;
      break;
  }
  
  return msgSize;
}

int CQS_ProcessMessage(uint32_t symbolId, char *buffer, int maxBytes, unsigned short *msgSize)
{
  t_CQS_Msg msgTmp;
  if (maxBytes > MAX_CQS_MSG_LEN) maxBytes = MAX_CQS_MSG_LEN;
  
  // Check if this is the needed packet: Retransmission Requester is original
  if ( (buffer[4] == 32)  && 
       ((buffer[3] == 'O') || (buffer[3] == 'V')) )
  {
    // Get message header
    msgTmp.msgCategory = buffer[0];
    msgTmp.msgType = buffer[1];
    
    switch (msgTmp.msgCategory)
    {
      case 'E':
        if (msgTmp.msgType == 'B')
        {
          //Equity long quote
          memcpy(msgTmp.msgHeader, &buffer[0], MAX_CQS_HEADER_LEN);
          memcpy(msgTmp.msgBody, &buffer[MAX_CQS_HEADER_LEN], maxBytes - MAX_CQS_HEADER_LEN);
          msgTmp.msgBody[maxBytes - MAX_CQS_HEADER_LEN] = 0;
          
          if (CQS_StatusMgmt.isConnected != DISCONNECTED)
          {         
            ProcessCQS_LongQuoteMsg(symbolId, &msgTmp);
          }
          
          *msgSize = 24 + 78;
          
          // National BBO Indicator
          if ((msgTmp.msgBody[76] - 48) == 4) //Long appendage
            *msgSize = *msgSize + 58;
          else if ((msgTmp.msgBody[76] - 48) == 6) //Short appendage
            *msgSize = *msgSize + 28;
          
          // FINRA BBO Indicator
          if ((msgTmp.msgBody[77] - 48) == 3) //There is appendage
            *msgSize = *msgSize + 56;
        }
        else if (msgTmp.msgType == 'D')
        {
          // Equity price short quote
          memcpy(msgTmp.msgHeader, &buffer[0], MAX_CQS_HEADER_LEN);
          memcpy(msgTmp.msgBody, &buffer[MAX_CQS_HEADER_LEN], maxBytes - MAX_CQS_HEADER_LEN);
          msgTmp.msgBody[maxBytes - MAX_CQS_HEADER_LEN] = 0;

          if (CQS_StatusMgmt.isConnected != DISCONNECTED)
            ProcessCQS_ShortQuoteMsg(symbolId, &msgTmp);
          
          *msgSize = 24 + 34;
          
          // National BBO Indicator
          if ((msgTmp.msgBody[32] - 48) == 4)  //Long appendage
            *msgSize = *msgSize + 58;
          else if ((msgTmp.msgBody[32] - 48) == 6)  //Short appendage
            *msgSize = *msgSize + 28;
          
          // FINRA BBO Indicator
          if ((msgTmp.msgBody[33] - 48) == 3) //There is appendage
            *msgSize = *msgSize + 56;
        }
        else
        {
          //Do nothing...
          //Some other messages belong to category 'E' but not documented !
          TraceLog(ERROR_LEVEL, "CQS: Unknown msgType (%c - %c)\n", msgTmp.msgCategory, msgTmp.msgType);
          return ERROR;
        }
        
        break;
      case 'A':
        TraceLog(ERROR_LEVEL, "CQS: We should not receive any message from category 'A'!\n");
        return ERROR;
        break;
      case 'B':
        TraceLog(ERROR_LEVEL, "CQS: We should not receive any message from category 'B'!\n");
        return ERROR;
        break;
      case 'M':
        *msgSize = 46 + 24;
        break;
      case 'L':
        TraceLog(ERROR_LEVEL, "CQS: We should not receive any message from category 'L'!\n");
        return ERROR;
        break;
      case 'C':
        *msgSize = 24;
        if (CQS_StatusMgmt.isConnected != DISCONNECTED)
        {
          switch (msgTmp.msgType)
          {
            case 'L': //Reset sequence number
              TraceLog(DEBUG_LEVEL, "CQS: Received message sequence number reset\n");
              break;
            case 'C': // FINRA Close message --> Remove quotes
            {
              // Get participant id
              int participantId = buffer[17];
              int exchangeIndex = ExchangeIndexList[CQS_SOURCE][participantId - 65];
            
              TraceLog(DEBUG_LEVEL, "CQS: Received FINRA Close message, we will close all Market Maker quotes with exchange = %c\n", participantId);
              if ((exchangeIndex >= 0) && (exchangeIndex < MAX_EXCHANGE))
              { 
                FlushBBOQuotes(exchangeIndex);
              }
            }
              break;
            case 'T': // for "Line integrity"
            case 'M': // for "Start of Test Cycle"
            case 'I': // for "Start of Day"
            case 'N': // for "End of Test Cycle"
            case 'Z': // for "End of Transmission"
            case 'O': // FINRA Open
              break;
            default:
              TraceLog(ERROR_LEVEL, "CQS other message: category = %c, msgType = %c\n", msgTmp.msgCategory, msgTmp.msgType);
              return ERROR;
          }
        }
        break;
      default:
        {
          // Other message in other category that is not documented !
          char buffTemp[MAX_CQS_MSG_LEN];
          memcpy(buffTemp, &buffer[0], maxBytes);
          buffTemp[maxBytes] = 0;

          TraceLog(ERROR_LEVEL, "CQS other message: category = %c, msgType = %c, msg = '%s'\n", msgTmp.msgCategory, msgTmp.msgType, buffTemp);
          return ERROR;
        }
    }
  }
  else
  {
    *msgSize = GetCQSMsgLen(buffer);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessCQS_LongQuoteMsg
- Input:      
- Output:                     
- Return:     
- Description:                                    
- Usage:      
****************************************************************************/
int ProcessCQS_LongQuoteMsg(uint32_t symbolId, t_CQS_Msg *msg)
{
  /** CQS SPECIFICATION: October 4, 2013, Version 54
    FIELD NAME                                                 SIZE    OFFSET
    --------------------------------------------------------------------------
    Message Header                                               24       0
    --------------------------------------------------------------------------
    Security Symbol                                              11       0
    Temporary Suffix                                              1      11
    Test Message Indicator                                        1      12
    Primary Listing Market Participant Identifier                 1      13
    SIP Generated Message Identifier                              1      14
    Reserved                                                      1      15
    Financial Status                                              1      16
    Currency Indicator                                            3      17
    Instrument Type                                               1      20
    Cancel/Correction Indicator                                   1      21
    Settlement Condition                                          1      22
    Market Condition                                              1      23
    Quote Condition                                               1      24
    Limit Up-Limit Down (LULD) Indicator                          1      25
    Retail Interest Indicator                                     1      26
    Bid Price / Lower Limit Price Band Denominator Indicator      1      27
    Bid Price / Lower Limit Price Band                           12      28
    Bid Size in Units of Trade                                    7      40
    Offer Price / Upper Limit Price Band Denominator Indicator    1      47
    Offer Price / Upper Limit Price Band                         12      48
    Offer Size in Units of Trade                                  7      60
    FINRA Market Maker ID                                         4      67
    Reserved                                                      1      71
    National BBO LULD Indicator                                   1      72
    FINRA BBO LULD Indicator                                      1      73
    Short Sale Restriction Indicator                              1      74
    Reserved                                                      1      75
    National BBO Indicator                                        1      76
    FINRA BBO Indicator                                           1      77
  **/
  
  t_BBOQuoteInfo quote;
  
  msg->msgHeader[MAX_CQS_HEADER_LEN] = 0;

  // PARTICIPANT ID
  quote.participantId = msg->msgHeader[17];
  
  // TIME STAMP
  //quote.timeStamp = ((msg->msgHeader[18] - 48) * 3600 + (msg->msgHeader[19] - 48) * 60 + (msg->msgHeader[20] - 48)) * 1000 + Lrc_atoi(&msg->msgHeader[21], 3);

  // Parse message body 
  
  //QUOTE CONDITION: 1 Byte
  quote.quoteCondition = msg->msgBody[24];

  //BID PRICE DENOMINATOR INDICATOR: 1 Byte
  int priceIndicator = msg->msgBody[27] - '0';
  if (priceIndicator == 0 || priceIndicator > 25)
  {
    quote.bidPrice = 0.00;
    quote.bidSize = 0;
  }
  else
  { 
    //BID PRICE: 12 Bytes
    int numberOfByteOfNumerator = CQS_NumeratorByte[priceIndicator];
    int whole = Lrc_atoi(&msg->msgBody[28], 12 - numberOfByteOfNumerator);
    int numerator = Lrc_atoi(&msg->msgBody[28 + 12 - numberOfByteOfNumerator], numberOfByteOfNumerator);
    quote.bidPrice = whole + (double) numerator / CQS_DenominatorValue[priceIndicator];
  
    //BID SIZE IN UNITS OF TRADE: 7 Bytes
    quote.bidSize = Lrc_atoi(&msg->msgBody[40], 7);
  }
  
  //OFFER PRICE DENOMINATOR INDICATOR: 1 Byte
  priceIndicator = msg->msgBody[47] - '0';
  if (priceIndicator == 0 || priceIndicator > 25)
  {
    quote.offerPrice = 0.00;
    quote.offerSize = 0;
  }
  else
  {
    //OFFER PRICE: 12 Bytes
    int numberOfByteOfNumerator = CQS_NumeratorByte[priceIndicator];
    int whole = Lrc_atoi(&msg->msgBody[48], 12 - numberOfByteOfNumerator);
    int numerator = Lrc_atoi(&msg->msgBody[48 + 12 - numberOfByteOfNumerator], numberOfByteOfNumerator);
    quote.offerPrice = whole + (double) numerator / CQS_DenominatorValue[priceIndicator];

    //OFFER SIZE IN UNITS OF TRADE: 7 Bytes
    quote.offerSize = Lrc_atoi(&msg->msgBody[60], 7);
  }
  
  //NATIONAL BBO INDICATOR: 1 Byte
  t_NationalAppendage nationalAppendage;
  
  //Short Sale Restriction Indicator: 1 byte
  unsigned char oldValue = SymbolMgmt.symbolList[symbolId].shortSaleStatus;
  unsigned char newValue = oldValue;
  
  char shortSaleRestriction = msg->msgBody[74];
  switch (shortSaleRestriction)
  {
    case ' ': // Short Sale Restriction Not in Effect
    case 'D': // Short Sale Restriction Deactivated
      newValue = oldValue | SHORT_SALE_RESTRICTION_BIT_FLAG;
      break;
    case 'A': // Short Sale Restriction Activated
    case 'C': // Short Sale Restriction Continued
    case 'E': // Short Sale Restriction in Effect
      newValue = oldValue & ~SHORT_SALE_RESTRICTION_BIT_FLAG;
      break;
    default:
      TraceLog(WARN_LEVEL, "CQS Long quote: invalid 'Short Sale Restriction' value: '%c'\n", shortSaleRestriction);
      break;
  }
  
  if (oldValue != newValue)
  {
    SymbolMgmt.symbolList[symbolId].shortSaleStatus = newValue;
    //DEVLOGF("CQS Long quote (short sale restriction '%c'): Symbol %.8s, old value: %d, new value: %d\n", shortSaleRestriction, symbol, oldValue, newValue);
  }
  
  // Initialize ...
  nationalAppendage.bidSize = -1;
  nationalAppendage.offerSize = -1;

  // NATIONAL BBO APPENDAGE
  switch (msg->msgBody[76] - 48)
  {
    case 4:   // Long Format of National BBO Appendage
      ProcessCQS_LongNationalBBOAppendage(&msg->msgBody[78], &nationalAppendage);
      break;
    case 6:   // Short Format of National BBO Appendage
      ProcessCQS_ShortNationalBBOAppendage(&msg->msgBody[78], &nationalAppendage);
      break;
    case 1:   // Quote contains all National BBO information
      nationalAppendage.bidParticipantId = quote.participantId;
      nationalAppendage.bidPrice = quote.bidPrice;
      nationalAppendage.bidSize = quote.bidSize;    
      nationalAppendage.offerParticipantId = quote.participantId;
      nationalAppendage.offerPrice = quote.offerPrice;
      nationalAppendage.offerSize = quote.offerSize;
    
      //TraceLog(DEBUG_LEVEL, "NATIONAL: %c %d %lf %c %d %lf\n", nationalAppendage.bidParticipantId, nationalAppendage.bidSize, nationalAppendage.bidPrice, nationalAppendage.offerParticipantId, nationalAppendage.offerSize, nationalAppendage.offerPrice);
      break;
  
    // 0: No national BBO changes
    // 2: No National BBO
  }
    
  //Add quote to list
  AddQuoteToBBOQuoteMgmt(symbolId, &quote, &nationalAppendage, CQS_SOURCE);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessCQS_ShortQuoteMsg
- Input:      
- Output:                     
- Return:     
- Description:                                    
- Usage:      
****************************************************************************/
int ProcessCQS_ShortQuoteMsg(uint32_t symbolId, t_CQS_Msg *msg)
{
  t_BBOQuoteInfo quote;
  
  msg->msgHeader[MAX_CQS_HEADER_LEN] = 0;
  //TraceLog(DEBUG_LEVEL, "Equity short quote message: %s%s\n", msg->msgHeader, msg->msgBody);

  // PARTICIPANT ID
  quote.participantId = msg->msgHeader[17]; 
  
  // TIME STAMP
  //quote.timeStamp = ((msg->msgHeader[18] - 48) * 3600 + (msg->msgHeader[19] - 48) * 60 + (msg->msgHeader[20] - 48)) * 1000 + Lrc_atoi(&msg->msgHeader[21], 3);

  // Parse message body 
  
  // QUOTE CONDITIONS: 1 byte
  quote.quoteCondition = msg->msgBody[3];
  
  //BID PRICE DENOMINATOR INDICATOR: 1 byte
  int priceIndicator = msg->msgBody[6] - '0';
  if (priceIndicator == 0 || priceIndicator > 25)
  {
    quote.bidPrice = 0.00;
    quote.bidSize = 0;
  }
  else
  {
    //BID SHORT PRICE: 8 Bytes
    int numberOfByteOfNumerator = CQS_NumeratorByte[priceIndicator];
  
    int whole = Lrc_atoi(&msg->msgBody[7], 8 - numberOfByteOfNumerator);
    int numerator = Lrc_atoi(&msg->msgBody[7 + 8 - numberOfByteOfNumerator], numberOfByteOfNumerator);
    quote.bidPrice = whole + (double) numerator / CQS_DenominatorValue[priceIndicator];
    
    //BID SIZE IN UNITS OF TRADE: 3 Bytes
    quote.bidSize = Lrc_atoi(&msg->msgBody[15], 3);
  }
    
  //OFFER PRICE DENOMINATOR INDICATOR: 1 Byte
  priceIndicator = msg->msgBody[19] - '0';
  if (priceIndicator == 0 || priceIndicator > 25)
  {
    quote.offerPrice = 0.00;
    quote.offerSize = 0;
  }
  else
  { 
    //OFFER SHORT PRICE: 8 Bytes
    int numberOfByteOfNumerator = CQS_NumeratorByte[priceIndicator];
    int whole = Lrc_atoi(&msg->msgBody[20], 8 - numberOfByteOfNumerator);
    int numerator = Lrc_atoi(&msg->msgBody[20 + 8 - numberOfByteOfNumerator], numberOfByteOfNumerator);
    quote.offerPrice = whole + (double) numerator / CQS_DenominatorValue[priceIndicator];
    
    //OFFER SIZE IN UNITS OF TRADE: 3 Bytes
    quote.offerSize = Lrc_atoi(&msg->msgBody[28], 3);
  }
  
  //NATIONAL BBO INDICATOR: 1 Byte
  t_NationalAppendage nationalAppendage;
  
  // Initialize ...
  nationalAppendage.bidSize = -1;
  nationalAppendage.offerSize = -1;
  
  // NATIONAL BBO APPENDAGE
  switch (msg->msgBody[32] - 48)
  {
    case 4:   // Long Format of National BBO Appendage
      ProcessCQS_LongNationalBBOAppendage(&msg->msgBody[34], &nationalAppendage);
      break;
    case 6:   // Short Format of National BBO Appendage
      ProcessCQS_ShortNationalBBOAppendage(&msg->msgBody[34], &nationalAppendage);
      break;
    case 1:   // Quote contains all National BBO information
      nationalAppendage.bidParticipantId = quote.participantId;
      nationalAppendage.bidPrice = quote.bidPrice;
      nationalAppendage.bidSize = quote.bidSize;    
      nationalAppendage.offerParticipantId = quote.participantId;
      nationalAppendage.offerPrice = quote.offerPrice;
      nationalAppendage.offerSize = quote.offerSize;
      
      //TraceLog(DEBUG_LEVEL, "NATIONAL: %c %d %lf %c %d %lf\n", nationalAppendage.bidParticipantId, nationalAppendage.bidSize, nationalAppendage.bidPrice, nationalAppendage.offerParticipantId, nationalAppendage.offerSize, nationalAppendage.offerPrice);
      break;
    // 0: No national BBO changes
    // 2: No National BBO
  }
    
  // Add quote to list
  AddQuoteToBBOQuoteMgmt(symbolId, &quote, &nationalAppendage, CQS_SOURCE);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessCQS_ShortNationalBBOAppendage
- Input:      
- Output:                     
- Return:     
- Description:                                    
- Usage:      
****************************************************************************/
void ProcessCQS_ShortNationalBBOAppendage(char *msg, t_NationalAppendage *nationalAppendage)
{
  //BEST BID PARTICIPANT ID: 1 Byte
  nationalAppendage->bidParticipantId = msg[0];
  
  //BEST BID PRICE DENOMINATOR INDICATOR: 1
  int priceIndicator = msg[1] - '0';
  
  if (priceIndicator == 0 || priceIndicator > 25)
  {
    nationalAppendage->bidPrice = 0.00;
    nationalAppendage->bidSize = 0;
  }
  else
  {
    //BEST SHORT BID PRICE: 8
    int numberOfByteOfNumerator = CQS_NumeratorByte[priceIndicator];
    int whole = Lrc_atoi(&msg[2], 8 - numberOfByteOfNumerator);
    int numerator = Lrc_atoi(&msg[2 + 8 - numberOfByteOfNumerator], numberOfByteOfNumerator);
    nationalAppendage->bidPrice = whole + (double) numerator / CQS_DenominatorValue[priceIndicator];

    //BEST BID SIZE IN UNITS OF TRADE: 3
    nationalAppendage->bidSize = Lrc_atoi(&msg[10], 3);
  }
  
  //BEST OFFER PARTICIPANT ID: 1
  nationalAppendage->offerParticipantId = msg[14];
  
  //BEST OFFER PRICE DENOMINATOR INDICATOR: 1
  priceIndicator = msg[15] - '0';
  if (priceIndicator == 0 || priceIndicator > 25)
  {
    nationalAppendage->offerPrice = 0.00;
    nationalAppendage->offerSize = 0;
  }
  else
  { 
    //BEST SHORT OFFER PRICE: 8
    int numberOfByteOfNumerator = CQS_NumeratorByte[priceIndicator];
    int whole = Lrc_atoi(&msg[16], 8 - numberOfByteOfNumerator);
    int numerator = Lrc_atoi(&msg[16 + 8 - numberOfByteOfNumerator], numberOfByteOfNumerator);
    nationalAppendage->offerPrice = whole + (double) numerator / CQS_DenominatorValue[priceIndicator];

    //BEST OFFER SIZE IN UNITS OF TRADE: 3
    nationalAppendage->offerSize = Lrc_atoi(&msg[24],3);
  }
  
  //TraceLog(DEBUG_LEVEL, "NATIONAL SHORT: %c %d %lf %c %d %lf\n", nationalAppendage->bidParticipantId, nationalAppendage->bidSize, nationalAppendage->bidPrice, nationalAppendage->offerParticipantId, nationalAppendage->offerSize, nationalAppendage->offerPrice);
  
  //RESERVED : 1
  return;
}

/****************************************************************************
- Function name:  ProcessCQS_LongNationalBBOAppendage
- Input:      
- Output:                     
- Return:     
- Description:                                    
- Usage:      
****************************************************************************/
void ProcessCQS_LongNationalBBOAppendage(char *msg, t_NationalAppendage *nationalAppendage)
{
  //RESERVED 2
  //BEST BID PRICE PARTICIPANT ID 1
  nationalAppendage->bidParticipantId = msg[2];
  
  //BEST BID PRICE DENOMINATOR INDICATOR 1
  int priceIndicator = msg[3] - '0';
  
  if (priceIndicator == 0 || priceIndicator > 25)
  {
    nationalAppendage->bidPrice = 0.00;
    nationalAppendage->bidSize = 0;
  }
  else
  {
    //BEST BID PRICE 12
    int numberOfByteOfNumerator = CQS_NumeratorByte[priceIndicator];
    int whole = Lrc_atoi(&msg[4], 12 - numberOfByteOfNumerator);
    int numerator = Lrc_atoi(&msg[4 + 12 - numberOfByteOfNumerator], numberOfByteOfNumerator);
    nationalAppendage->bidPrice = whole + (double) numerator / CQS_DenominatorValue[priceIndicator];

    //BEST BID SIZE IN UNITS OF TRADE 7
    nationalAppendage->bidSize = Lrc_atoi(&msg[16], 7);
  }
  
  //BEST OFFER PARTICIPANT ID 1
  nationalAppendage->offerParticipantId = msg[30];
  
  //BEST OFFER PRICE DENOMINATOR INDICATOR 1
  priceIndicator = msg[31] - '0';
  if (priceIndicator == 0 || priceIndicator > 25)
  {
    nationalAppendage->offerPrice = 0.00;
    nationalAppendage->offerSize = 0;
  }
  else
  {
    //BEST OFFER PRICE 12
    int numberOfByteOfNumerator = CQS_NumeratorByte[priceIndicator];
    int whole = Lrc_atoi(&msg[32], 12 - numberOfByteOfNumerator);
    int numerator = Lrc_atoi(&msg[32 + 12 - numberOfByteOfNumerator], numberOfByteOfNumerator);
    nationalAppendage->offerPrice = whole + (double) numerator / CQS_DenominatorValue[priceIndicator];

    //BEST OFFER SIZE IN UNITS OF TRADE 7
    nationalAppendage->offerSize = Lrc_atoi(&msg[44], 7);
  }
  
  return;
}
