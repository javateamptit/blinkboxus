/****************************************************************************
** Include files and define several variables
****************************************************************************/
#include "aggregation_server_proc.h"
#include "arca_direct_proc.h"
#include "nyse_ccg_proc.h"
#include "bats_boe_proc.h"
#include "nasdaq_book_proc.h"
#include "bats_book_proc.h"
#include "arca_book_proc.h"
#include "nyse_book_proc.h"
#include "edge_book_proc.h"
#include "edge_order_proc.h"

#include "cqs_proc.h"
#include "uqdf_proc.h"
#include "quote_mgmt.h"
// #include "configuration.h"
#include "kernel_algorithm.h"
#include "trading_mgmt.h"
#include "hbitime.h"
#include "logging.h"

#include <sys/time.h>

/****************************************************************************
** Global variables
****************************************************************************/

t_ASConfig ASConfig;
t_CurrentStatus TSCurrentStatus;
t_IgnoreStockRangesList IgnoreStockRangesList;

int         IsUpdateBuyingPower = NO;
static int  IsReceivedEtbList = NO;
int         WillDisableTrading[MAX_ECN_ORDER];
double      ReceivedBuyingPower = 0.00;

extern t_SymbolMgmt   SymbolMgmt;
extern double         SecFeeAndCommissionInCent;
extern int            AllVolatileStatus;
extern int            TradingCounter;
/****************************************************************************
** Function definitions
****************************************************************************/

/****************************************************************************
- Function name:  InitASDataStructure
- Description:  
****************************************************************************/
int InitASDataStructure(void)
{
  if (pthread_mutex_init(&ASConfig.sendingLock, NULL) != SUCCESS)
  {
    return ERROR;
  }
  
  ASConfig.socket = -1;
  
  // Default value for current status of TS
  TSCurrentStatus.buyingPower = 0;
  TSCurrentStatus.numStuck = 0;
  TSCurrentStatus.numLoser = 0;
    
  return SUCCESS;
}

/****************************************************************************
- Function name:  Thread_WaitForASConnection
- Description:
****************************************************************************/
void *Thread_WaitForASConnection(void *threadArg)
{
  SetKernelAlgorithm("[WAIT_FOR_AS_CONNECTION]");
  t_ASConfig *config = (t_ASConfig *)threadArg;

  // Initialize socket to listen connection from Aggregation Server
  int serverSocket = -1;
  serverSocket = ListenToClient(config->port, config->maxConnections);
  
  if (serverSocket == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Could not listen on port %d %d\n", config->port, config->maxConnections);
    return NULL;
  }
    
  // Waiting for connection from Aggregation Server
  while (1)
  {
    int clientSocket = -1;
    clientSocket = accept(serverSocket, NULL, NULL);
    
    if (clientSocket != ERROR)
    {
      TraceLog(DEBUG_LEVEL, "New client was accepted: %d\n", clientSocket);
      
      if (SetSocketRecvTimeout(clientSocket, AS_RECV_TIMEOUT) == ERROR)
      {
        TraceLog(DEBUG_LEVEL, "Could not set receive timeout option for client socket\n");
      }
      else
      {
        // Set timeout for client socket
        struct timeval tv;
        tv.tv_sec = 3;
        tv.tv_usec = 0;
        
        socklen_t optlen = sizeof(tv);
        
        if (setsockopt(clientSocket, SOL_SOCKET, SO_SNDTIMEO, &tv, optlen) == ERROR)
        {
          TraceLog(ERROR_LEVEL, "Could not set send timeout for AS socket %d\n", clientSocket);
        }
        
        if (getsockopt(clientSocket, SOL_SOCKET, SO_SNDTIMEO, &tv, &optlen) == ERROR)
        {
          TraceLog(ERROR_LEVEL, "Could not get send timeout for AS socket %d\n", clientSocket);
        }
        else
        {
          TraceLog(DEBUG_LEVEL, "getsockopt with SO_SNDTIMEO for socket descriptor %d: %d(s) + %d(us)\n", clientSocket, tv.tv_sec, tv.tv_usec);
        }
        
        config->socket = clientSocket;
        
        int rcvBuffSize = 1024 * 1024;
        int sndBuffSize = 1024 * 1024;
        
        if (setsockopt(config->socket, SOL_SOCKET, SO_RCVBUF, &rcvBuffSize, sizeof rcvBuffSize) != SUCCESS)
        {
          TraceLog(ERROR_LEVEL, "Could not set SO_RCVBUF for socket (%d)\n", clientSocket);
        }
        
        if (setsockopt(config->socket, SOL_SOCKET, SO_SNDBUF, &sndBuffSize, sizeof sndBuffSize) != SUCCESS)
        {
          TraceLog(ERROR_LEVEL, "Could not set SO_SNDBUF for socket (%d)\n", clientSocket);
        }
        
        pthread_t sendDataThread;
        pthread_create(&sendDataThread, NULL, (void *)&SendDataToASMgmt, (void *)config);
                
        // Talk to client now
        TalkToAggregationServer(config);
        
        // Disable trading
        int i;
        for (i = 0; i < MAX_ECN_ORDER; i++)
        {
          *(TradingStatus.status[i]) = DISABLE;
        }
        
        // Close client socket
        close(config->socket);
        
        config->socket = -1;
        
        //Clear stuck status for all symbols, AS will resend when TS connects
        int tradingInfoIndex;
        for (tradingInfoIndex = 0; tradingInfoIndex < MAX_STOCK_SYMBOL; tradingInfoIndex++)
        {
          if (TradingCollection[tradingInfoIndex].lockedSide != NO_LOCK)
          {
            TradingCollection[tradingInfoIndex].lockedSide = NO_LOCK;
          }
        }
        
        //Clear volatile status for all symbol, AS will resend when TS connects
        for (i = 0; i < MAX_STOCK_SYMBOL; i++)
        {
          if (SymbolMgmt.symbolList[i].isVolatile == YES)
          {
            SymbolMgmt.symbolList[i].isVolatile = NO;
          }
        }
      }
    }
  }
  
  close(serverSocket);
  
  return NULL;
}

/****************************************************************************
- Function name:  TalkToAggregationServer
- Description:
****************************************************************************/
int TalkToAggregationServer(void *configInfo)
{
  IsUpdateBuyingPower = NO;
  ReceivedBuyingPower = 0.00;
  IsReceivedEtbList = NO;
  memset(WillDisableTrading, 0, sizeof(WillDisableTrading));
    
  t_ASConfig *config = (t_ASConfig *)configInfo;
  t_Message clientMsg;
  unsigned short msgType;
  
  /*
  Send configuration to AS
  */
  SendOrderConfiguration(); //This must be the first msg sent to AS!
  
  BuildSend_MinSpread_Configuration();
  
  BuilSendExchangeConfigToAS();
  
  BuilSendBook_ISO_TradingToAS();
  
  SendBookConfiguration();
  
  BuildSend_SplitSymbol_Configuration();
  
  int ret = SUCCESS;
  // Receive and process all messages from AS
  while (ReceiveMessage(config, &clientMsg) == SUCCESS)
  {
    memcpy(&msgType, clientMsg.msgContent, 2);
    
    // Check valid supported message types by Trade Server
    if (msgType >= BEGIN_AS_REQUEST_MSG_TYPE && msgType <= END_AS_REQUEST_MSG_TYPE)
    {
      // Control message types
      if (msgType >= BEGIN_AS_REQUEST_CONTROLS_MSG_TYPE && msgType <= END_AS_REQUEST_CONTROLS_MSG_TYPE)
      {
        if (ProcessControlMessageTypes(msgType, clientMsg.msgContent) == ERROR)
        {
          ret = ERROR;
          break;
        }
      }
      // Get message types
      else if (msgType >= BEGIN_AS_REQUEST_GETS_MSG_TYPE && msgType <= END_AS_REQUEST_GETS_MSG_TYPE)
      {
        if (ProcessGetMessageTypes(msgType, clientMsg.msgContent) == ERROR)
        {
          ret = ERROR;
          break;
        }
      }
      // Set message types
      else if (msgType >= BEGIN_AS_REQUEST_SETS_MSG_TYPE && msgType <= END_AS_REQUEST_SETS_MSG_TYPE)
      {
        if (ProcessSetMessageTypes(msgType, clientMsg.msgContent) == ERROR)
        {
          ret = ERROR;
          break;
        }
      }
    }
    else
    {
      TraceLog(ERROR_LEVEL, "Unsupported message type: %d\n", msgType);
      ret = ERROR;
      break;
    }
  }
  
  TraceLog(DEBUG_LEVEL, "Return out of TalkToAggregationServer\n");
  
  int i;
  for (i = 0; i < SymbolMgmt.nextSymbolIndex; i++)
  {
    SymbolMgmt.symbolList[i].haltStatusSent = 0;
  }
  
  return ret;
}

/****************************************************************************
- Function name:  SendMessageToAS
- Description:  
****************************************************************************/
int SendMessageToAS(void *message, int len)
{
  // Send message to AS
  pthread_mutex_lock(&ASConfig.sendingLock);
  
  int countSentByte = send(ASConfig.socket, message, len, 0);
  
  pthread_mutex_unlock(&ASConfig.sendingLock);
  
  if (countSentByte != len)
  {
    TraceLog(ERROR_LEVEL, "Could not send message to AS\n");
    return ERROR;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessControlMessageTypes
- Description:
****************************************************************************/
int ProcessControlMessageTypes(int msgType, char *message)
{ 
  switch (msgType)
  {
    case AS_REQ_CONNECT_BOOK:
      return ProcessConnectBookMessage(message);
    case AS_REQ_DISCONNECT_BOOK:
      return ProcessDisconnectBookMessage(message);
    case AS_REQ_CONNECT_ORDER:
      return ProcessConnectOrderMessage(message);
    case AS_REQ_DISCONNECT_ORDER:
      return ProcessDisconnectOrderMessage(message);
    case AS_REQ_ENABLE_TRADING:
      return ProcessEnableTradingMessage(message);
    case AS_REQ_DISABLE_TRADING:
      return ProcessDisableTradingMessage(message);
    case MSG_TYPE_FOR_REQUEST_CONNECT_TO_BBO_FROM_AS:
      return ProcessConnectCQSnUQDFMessage(message);
    case MSG_TYPE_FOR_REQUEST_DISCONNECT_TO_BBO_FROM_AS:
      return ProcessDisconnectCQSnUQDFMessage(message);
    case MSG_TYPE_FOR_SET_ISO_TRADING_FROM_AS:
      return ProcessSetISO_TradingMessage(message);
    default:
      return ERROR;
  }
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessConnectBookMessage
- Description:
****************************************************************************/
int ProcessConnectBookMessage(char *message)
{
  int ecnBookID = message[10];
  if (ecnBookID >=0 && ecnBookID < MAX_ECN_BOOK)
  {
    TraceLog(DEBUG_LEVEL, "Received request to start MDC (%s)\n", GetBookNameByIndex(ecnBookID));
    ConnectToVenue(ecnBookID);
    return SUCCESS;
  }
  
  TraceLog(ERROR_LEVEL, "Received request to start MDC (invalid MDC id: %d)\n", ecnBookID);
  
  return ERROR;
}

/****************************************************************************
- Function name:  ProcessConnectOrderMessage
- Description:
****************************************************************************/
int ProcessConnectOrderMessage(char *message)
{
  int ecnOrderID = message[10];
  
  int goAhead = 0;
  if (ecnOrderID >= 0 && ecnOrderID < MAX_ECN_ORDER)
  {
    if ((OrderStatusMgmt[ecnOrderID].isConnected == DISCONNECTED) && 
        (OrderStatusMgmt[ecnOrderID].shouldConnect != YES))
    {
      OrderStatusMgmt[ecnOrderID].shouldConnect = YES;
      TraceLog(DEBUG_LEVEL, "Received request to start OEC (%s)\n", GetOrderNameByIndex(ecnOrderID));
      goAhead = 1;
    }
  }
  
  if (goAhead == 0) return SUCCESS;
  
  switch (ecnOrderID)
  {
    case ORDER_ARCA_DIRECT:
      Connect_ARCA_DIRECT();
      break;
    case ORDER_NASDAQ_OUCH:
    case ORDER_NASDAQ_BX:
    case ORDER_NASDAQ_PSX:
      Connect_Ouch(ecnOrderID);
      break;
    case ORDER_NASDAQ_RASH:
      Connect_NASDAQ_RASH();
      break;
    case ORDER_BATSZ_BOE:
    case ORDER_BYX_BOE:
      Connect_BATS_BOE(ecnOrderID);
      break;
    case ORDER_NYSE_CCG:
      Connect_NYSE_CCG();
      break;
    case ORDER_EDGX:
    case ORDER_EDGA:
      Connect_EDGE(ecnOrderID);
      break;
    default:
      return ERROR;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessDisconnectBookMessage
- Description:
****************************************************************************/
int ProcessDisconnectBookMessage(char *message)
{
  // ECN Book ID
  int ecnBookID = message[10];
  
  if (ecnBookID >=0 && ecnBookID < MAX_ECN_BOOK)
  {
    TraceLog(DEBUG_LEVEL, "Received request to stop MDC (%s)\n", GetBookNameByIndex(ecnBookID));
    DisconnectFromVenue(ecnBookID, -1);
    return SUCCESS;
  }
  
  TraceLog(ERROR_LEVEL, "Received request to stop MDC (invalid MDC id: %d)\n", ecnBookID);
  return ERROR;
}

/****************************************************************************
- Function name:  ProcessDisconnectOrderMessage
- Description:
****************************************************************************/
int ProcessDisconnectOrderMessage(char *message)
{
  // ECN Order ID
  int ecnOrderID = message[10];
  
  switch (ecnOrderID)
  {
    case ORDER_ARCA_DIRECT:
      if(OrderStatusMgmt[ecnOrderID].isConnected == CONNECTED)
      {
        TraceLog(DEBUG_LEVEL, "Received request to disconnect ARCA DIRECT\n");
        OrderStatusMgmt[ecnOrderID].shouldConnect = NO;
        disconnectSession(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, OecConfig[ecnOrderID].sessionId);
      }
      break;
    case ORDER_NYSE_CCG:
      if(OrderStatusMgmt[ecnOrderID].isConnected == CONNECTED)
      {
        TraceLog(DEBUG_LEVEL, "Received request to disconnect NYSE CCG\n");
        OrderStatusMgmt[ecnOrderID].shouldConnect = NO;
        disconnectSession(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, OecConfig[ecnOrderID].sessionId);
      }
      break;
    case ORDER_NASDAQ_OUCH:
    case ORDER_NASDAQ_BX:
    case ORDER_NASDAQ_PSX:
      if(OrderStatusMgmt[ecnOrderID].isConnected == CONNECTED)
      {
        TraceLog(DEBUG_LEVEL, "Received request to disconnect %s\n", GetOrderNameByIndex(ecnOrderID));
        OrderStatusMgmt[ecnOrderID].shouldConnect = NO;
        
        BuildAndSend_Ouch_LogoffMessage(ecnOrderID);
        disconnectSession(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, OecConfig[ecnOrderID].sessionId);
      }     
      break;
    case ORDER_NASDAQ_RASH:
      if(OrderStatusMgmt[ecnOrderID].isConnected == CONNECTED)
      {
        TraceLog(DEBUG_LEVEL, "Received request to disconnect NASDAQ RASH\n");
        OrderStatusMgmt[ecnOrderID].shouldConnect = NO;
        disconnectSession(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, OecConfig[ecnOrderID].sessionId);
      }
      break;
    case ORDER_BATSZ_BOE:
      if(OrderStatusMgmt[ecnOrderID].isConnected == CONNECTED)
      {
        OrderStatusMgmt[ecnOrderID].shouldConnect = NO;
        
        if(BuildAndSend_BATS_BOE_LogoutRequestMsg(ecnOrderID) == ERROR)
        {
          TraceLog(ERROR_LEVEL, "Could not send Logout Request to BATSZ_BOE, forcing disconnect\n");
          disconnectSession(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, OecConfig[ecnOrderID].sessionId);
        }
        else
        {
          TraceLog(DEBUG_LEVEL, "BATSZ BOE: Send logout message to server successfully\n");
          TraceLog(DEBUG_LEVEL, "BATSZ BOE: Waiting for logout response from server\n");
        }
      }
      
      TraceLog(DEBUG_LEVEL, "Received request to disconnect BATSZ BOE\n");
      break;
    
    case ORDER_BYX_BOE:
      if(OrderStatusMgmt[ecnOrderID].isConnected == CONNECTED)
      {
        OrderStatusMgmt[ecnOrderID].shouldConnect = NO;
        
        if(BuildAndSend_BATS_BOE_LogoutRequestMsg(ecnOrderID) == ERROR)
        {
          TraceLog(ERROR_LEVEL, "Could not send Logout Request to BYX_BOE, forcing disconnect\n");
          disconnectSession(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, OecConfig[ecnOrderID].sessionId);
        }
        else
        {
          TraceLog(DEBUG_LEVEL, "BYX BOE: Send logout message to server successfully\n");
          TraceLog(DEBUG_LEVEL, "BYX BOE: Waiting for logout response from server\n");
        }
      }
      
      TraceLog(DEBUG_LEVEL, "Received request to disconnect BYX BOE\n");
      break;
      
    case ORDER_EDGX:
      if(OrderStatusMgmt[ecnOrderID].isConnected == CONNECTED)
      {
        if (BuildAndSend_LogoutRequestMsg_EDGE(ecnOrderID) == ERROR)
        {
          TraceLog(ERROR_LEVEL, "Could not send Logout Request to EDGX, forcing disconnect\n");
          disconnectSession(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, OecConfig[ecnOrderID].sessionId);
        }       
        OrderStatusMgmt[ecnOrderID].shouldConnect = NO;   //TS will automatically send logout request
      }
      
      TraceLog(DEBUG_LEVEL, "Received request to disconnect EDGX\n");
      break;

    case ORDER_EDGA:
      if(OrderStatusMgmt[ecnOrderID].isConnected == CONNECTED)
      {
        if (BuildAndSend_LogoutRequestMsg_EDGE(ecnOrderID) == ERROR)
        {
          TraceLog(ERROR_LEVEL, "Could not send Logout Request to EDGA, forcing disconnect\n");
          disconnectSession(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, OecConfig[ecnOrderID].sessionId);
        }
        OrderStatusMgmt[ecnOrderID].shouldConnect = NO;   //TS will automatically send logout request
      }
      
      TraceLog(DEBUG_LEVEL, "Received request to disconnect EDGA\n");
      break;

    default:
      return ERROR;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessEnableTradingMessage
- Description:
****************************************************************************/
int ProcessEnableTradingMessage(char *message)
{
  int ecnOrderID = message[10];
  
  switch (ecnOrderID)
  {
    case ORDER_ARCA_DIRECT:
      if (OrderStatusMgmt[ORDER_ARCA_DIRECT].isConnected == CONNECTED &&
          BookStatusMgmt[BOOK_ARCA].isConnected == CONNECTED)
      {
        *(TradingStatus.status[ecnOrderID]) = ENABLE;
      }
    
      break;
      
    case ORDER_NYSE_CCG:
      if (OrderStatusMgmt[ORDER_NYSE_CCG].isConnected == CONNECTED &&
          (BookStatusMgmt[BOOK_NYSE].isConnected == CONNECTED || BookStatusMgmt[BOOK_AMEX].isConnected == CONNECTED) )
      {
        *(TradingStatus.status[ecnOrderID]) = ENABLE;
      }
    
      break;
      
    case ORDER_NASDAQ_OUCH:
      if (OrderStatusMgmt[ORDER_NASDAQ_OUCH].isConnected == CONNECTED &&
          BookStatusMgmt[BOOK_NASDAQ].isConnected == CONNECTED)
      {
        *(TradingStatus.status[ecnOrderID]) = ENABLE;
      }
      break;
      
    case ORDER_NASDAQ_BX:
      if (OrderStatusMgmt[ORDER_NASDAQ_BX].isConnected == CONNECTED &&
          BookStatusMgmt[BOOK_NDBX].isConnected == CONNECTED)
      {
        *(TradingStatus.status[ecnOrderID]) = ENABLE;
      }
      break;
      
    case ORDER_NASDAQ_PSX:
      if (OrderStatusMgmt[ORDER_NASDAQ_PSX].isConnected == CONNECTED &&
          BookStatusMgmt[BOOK_PSX].isConnected == CONNECTED)
      {
        *(TradingStatus.status[ecnOrderID]) = ENABLE;
      }
      break;
      
    case ORDER_NASDAQ_RASH:
      if (OrderStatusMgmt[ORDER_NASDAQ_RASH].isConnected == CONNECTED &&
          BookStatusMgmt[BOOK_NASDAQ].isConnected == CONNECTED)
      {
        *(TradingStatus.status[ecnOrderID]) = ENABLE;
      }
      break;
      
    case ORDER_BATSZ_BOE:
      if (OrderStatusMgmt[ORDER_BATSZ_BOE].isConnected == CONNECTED &&
          BookStatusMgmt[BOOK_BATSZ].isConnected == CONNECTED)
      {
        *(TradingStatus.status[ecnOrderID]) = ENABLE;
      }
      break;
      
    case ORDER_BYX_BOE:
      if (OrderStatusMgmt[ORDER_BYX_BOE].isConnected == CONNECTED &&
        BookStatusMgmt[BOOK_BYX].isConnected == CONNECTED)
      {
        *(TradingStatus.status[ecnOrderID]) = ENABLE;
      }
      break;
      
    case ORDER_EDGX:
      if (OrderStatusMgmt[ORDER_EDGX].isConnected == CONNECTED &&
          BookStatusMgmt[BOOK_EDGX].isConnected == CONNECTED)
      {
        *(TradingStatus.status[ecnOrderID]) = ENABLE;
      }
      break;
      
    case ORDER_EDGA:
      if (OrderStatusMgmt[ORDER_EDGA].isConnected == CONNECTED &&
          BookStatusMgmt[BOOK_EDGA].isConnected == CONNECTED)
      {
        *(TradingStatus.status[ecnOrderID]) = ENABLE;
      }
      break;
      
    default:
      return ERROR;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessDisableTradingMessage
- Description:
****************************************************************************/
int ProcessDisableTradingMessage(char *message)
{
  // ECN Order ID
  int ecnOrderID = message[10];
  short reasonCode = *(short*)(&message[11]);
  int ecnIndex, isAlert = 0;
  
  if (reasonCode == TRADING_DISABLED_AS_SCHEDULE && TradingCounter > 0)
  {
    if (ecnOrderID == SERVICE_ALL_OECs)
    {
      TraceLog(DEBUG_LEVEL, "Disable-Trading requests (auto scheduled) for all orders are pending due to unfinished trade\n");
      for (ecnIndex = 0; ecnIndex < MAX_ECN_ORDER; ecnIndex++)
      {
        WillDisableTrading[ecnIndex] = 1;
      }
    }
    else if (ecnOrderID > -1 && ecnOrderID < MAX_ECN_ORDER)
    {
      TraceLog(DEBUG_LEVEL, "Disable-Trading request (auto scheduled) for order id %d is pending due to unfinished trade\n", ecnOrderID);
      WillDisableTrading[ecnOrderID] = 1;
    }
    return SUCCESS;
  }
  
  switch (ecnOrderID)
  {
    /*
    Process request disconnect to Orders
    */
    case SERVICE_ALL_OECs:
      for (ecnIndex = 0; ecnIndex < MAX_ECN_ORDER; ecnIndex++)
      {
        if (*(TradingStatus.status[ecnIndex]) == ENABLE)
        {
          *(TradingStatus.status[ecnIndex]) = DISABLE;
          isAlert = 1;
        }
      }
      
      if (isAlert == 1)
      {
        //notify AS that trading is disabled
        if (BuildSendTradingDisabled(SERVICE_ALL_OECs, reasonCode) == SUCCESS)
        {
          TraceLog(DEBUG_LEVEL, "Notify trading disabled of all OECs is sent to AS\n");
        }
      }
      
      break;
      
    case ORDER_ARCA_DIRECT:
      if(OrderStatusMgmt[ORDER_ARCA_DIRECT].isConnected == CONNECTED)
      {
        *(TradingStatus.status[ecnOrderID]) = DISABLE;
        
        //notify AS that trading is disabled
        if (BuildSendTradingDisabled(SERVICE_ARCA_DIRECT, reasonCode) == SUCCESS)
        {
          TraceLog(DEBUG_LEVEL, "Notify trading disabled of ARCA DIRECT is sent to AS\n");
        }
      }
    
      break;
      
    case ORDER_NYSE_CCG:
      if(OrderStatusMgmt[ORDER_NYSE_CCG].isConnected == CONNECTED)
      {
        *(TradingStatus.status[ecnOrderID]) = DISABLE;
        
        //notify AS that trading is disabled
        if (BuildSendTradingDisabled(SERVICE_NYSE_CCG, reasonCode) == SUCCESS)
        {
          TraceLog(DEBUG_LEVEL, "Notify trading disabled of NYSE CCG is sent to AS\n");
        }
      }

      break;

    case ORDER_NASDAQ_OUCH:
      if(OrderStatusMgmt[ORDER_NASDAQ_OUCH].isConnected == CONNECTED)
      {
        *(TradingStatus.status[ecnOrderID]) = DISABLE;
        
        //notify AS that trading is disabled
        if (BuildSendTradingDisabled(SERVICE_NASDAQ_OUCH, reasonCode) == SUCCESS)
        {
          TraceLog(DEBUG_LEVEL, "Notify trading disabled of NASDAQ OUCH is sent to AS\n");
        }
      }
      
      break;
      
    case ORDER_NASDAQ_BX:
      if(OrderStatusMgmt[ORDER_NASDAQ_BX].isConnected == CONNECTED)
      {
        *(TradingStatus.status[ecnOrderID]) = DISABLE;
        
        //notify AS that trading is disabled
        if (BuildSendTradingDisabled(SERVICE_NASDAQ_BX, reasonCode) == SUCCESS)
        {
          TraceLog(DEBUG_LEVEL, "Notify trading disabled of NASDAQ BX is sent to AS\n");
        }
      }
      
      break;
      
    case ORDER_NASDAQ_PSX:
      if(OrderStatusMgmt[ORDER_NASDAQ_PSX].isConnected == CONNECTED)
      {
        *(TradingStatus.status[ecnOrderID]) = DISABLE;
        
        //notify AS that trading is disabled
        if (BuildSendTradingDisabled(SERVICE_NASDAQ_PSX, reasonCode) == SUCCESS)
        {
          TraceLog(DEBUG_LEVEL, "Notify trading disabled of NASDAQ PSX is sent to AS\n");
        }
      }
      
      break;
      
    case ORDER_NASDAQ_RASH:
      if(OrderStatusMgmt[ORDER_NASDAQ_RASH].isConnected == CONNECTED)
      {
        *(TradingStatus.status[ecnOrderID]) = DISABLE;
        
        //notify AS that trading is disabled
        if (BuildSendTradingDisabled(SERVICE_NASDAQ_RASH, reasonCode) == SUCCESS)
        {
          TraceLog(DEBUG_LEVEL, "Notify trading disabled of NASDAQ RASH is sent to AS\n");
        }
      }
      
      break;
      
    case ORDER_BATSZ_BOE:
      if(OrderStatusMgmt[ORDER_BATSZ_BOE].isConnected == CONNECTED)
      {
        *(TradingStatus.status[ecnOrderID]) = DISABLE;
        
        //notify AS that trading is disabled
        if (BuildSendTradingDisabled(SERVICE_BATSZ_BOE, reasonCode) == SUCCESS)
        {
          TraceLog(DEBUG_LEVEL, "Notify trading disabled of BZX BOE is sent to AS\n");
        }
      }
      break;  
    
    case ORDER_BYX_BOE:
      if(OrderStatusMgmt[ORDER_BYX_BOE].isConnected == CONNECTED)
      {
        *(TradingStatus.status[ecnOrderID]) = DISABLE;
        
        //notify AS that trading is disabled
        if (BuildSendTradingDisabled(SERVICE_BYX_BOE, reasonCode) == SUCCESS)
        {
          TraceLog(DEBUG_LEVEL, "Notify trading disabled of BYX BOE is sent to AS\n");
        }
      }
      break;
      
    case ORDER_EDGX:
      if(OrderStatusMgmt[ORDER_EDGX].isConnected == CONNECTED)
      {
        *(TradingStatus.status[ecnOrderID]) = DISABLE;
        
        //notify AS that trading is disabled
        if (BuildSendTradingDisabled(SERVICE_EDGX_ORDER, reasonCode) == SUCCESS)
        {
          TraceLog(DEBUG_LEVEL, "Notify trading disabled of EDGX is sent to AS\n");
        }
      }
      break;  
      
    case ORDER_EDGA:
      if(OrderStatusMgmt[ORDER_EDGA].isConnected == CONNECTED)
      {
        *(TradingStatus.status[ecnOrderID]) = DISABLE;
        
        //notify AS that trading is disabled
        if (BuildSendTradingDisabled(SERVICE_EDGA_ORDER, reasonCode) == SUCCESS)
        {
          TraceLog(DEBUG_LEVEL, "Notify trading disabled of EDGA is sent to AS\n");
        }
      }
      break;
      
    /*
    Some error
    */
    default:
      return ERROR;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessFlushBookOfAllSymbol
- Description:
****************************************************************************/
int ProcessFlushBookOfAllSymbols(char *message)
{
  int ecnId = message[10];
  short flushTrigger = 0;
  
  //Flush Books of all stock symbols
  switch (ecnId)
  {
    case BOOK_ARCA:
      TraceLog(DEBUG_LEVEL, "Received request to flush ARCA BOOK\n");
      flushTrigger |= __FLUSH_ARCA;
      break;
    case BOOK_NASDAQ:
      TraceLog(DEBUG_LEVEL, "Received request to flush NASDAQ BOOK\n");
      flushTrigger |= __FLUSH_NASDAQ;
      break;
    case BOOK_NDBX:
      TraceLog(DEBUG_LEVEL, "Received request to flush BX BOOK\n");
      flushTrigger |= __FLUSH_NDBX;
      break;
    case BOOK_PSX:
      TraceLog(DEBUG_LEVEL, "Received request to flush PSX BOOK\n");
      flushTrigger |= __FLUSH_PSX;
      break;
    case BOOK_BATSZ:
      TraceLog(DEBUG_LEVEL, "Received request to flush BATS-Z BOOK\n");
      flushTrigger |= __FLUSH_BATSZ;
      break;
    case BOOK_BYX:
      TraceLog(DEBUG_LEVEL, "Received request to flush BYX BOOK\n");
      flushTrigger |= __FLUSH_BYX;
      break;
    case BOOK_NYSE:
      TraceLog(DEBUG_LEVEL, "Received request to flush NYSE BOOK\n");
      flushTrigger |= __FLUSH_NYSE;
      break;
    case BOOK_AMEX:
      TraceLog(DEBUG_LEVEL, "Received request to flush AMEX BOOK\n");
      flushTrigger |= __FLUSH_AMEX;
      break;
    case BOOK_EDGX:
      TraceLog(DEBUG_LEVEL, "Received request to flush EDGX BOOK\n");
      flushTrigger |= __FLUSH_EDGX;
      break;
    case BOOK_EDGA:
      TraceLog(DEBUG_LEVEL, "Received request to flush EDGA BOOK\n");
      flushTrigger |= __FLUSH_EDGA;
      break;
    default:
      TraceLog(ERROR_LEVEL, "Received invalid book flush request, book id: %d\n", ecnId);
      break;
  }
  
  AddNewFlush(flushTrigger, -1);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessFlushBookOfASymbol
- Description:
****************************************************************************/
int ProcessFlushBookOfASymbol(char *message)
{
  //ECN ID
  int ecnID = message[10];
  
  //Stocksymbol
  char stockSymbol[SYMBOL_LEN];
  memcpy(stockSymbol, &message[11], SYMBOL_LEN);
  
  //Flush of Stock Symbol
  TraceLog(WARN_LEVEL, "Received request to flush book of symbol %.8s of ECN Book %d (This feature is no longer supported)\n", stockSymbol, ecnID);
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessGetMessageTypes
- Description:
****************************************************************************/
int ProcessGetMessageTypes(int msgType, char *message)
{
  switch (msgType)
  {
    case AS_REQ_GET_RAWDATA:
      return ParseGetTradingInformation(message);
    case AS_REQ_GET_STOCK_SYMBOL_STATUS:
      return ParseGetStockSymbolStatus(message);
    case AS_REQ_GET_HEARTBEAT:
      return SUCCESS;
    case AS_REQ_GET_TRADE_OUTPUT_LOG:
      return ParseGetTradeOutputLog(message);
    case AS_REQ_GET_BBO_INFO:
      return ParseGetBBOInfo(message);
    case MSG_TYPE_FOR_GET_BBO_QUOTE_LIST_FROM_AS:
      return ParseGetBBOQuotes(message);
    case AS_REQ_GET_NUMBER_OF_SYMBOLS_WITH_QUOTES:
      return ParseGetNumberOfSymbolsWithQuotes(message);
    case AS_REQ_ARCA_DIRECT_CANCEL_REQUEST:
      return SUCCESS;
    default:
      return ERROR;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessSetMessageTypes
- Description:
****************************************************************************/
int ProcessSetMessageTypes(int msgType, char *message)
{
  switch (msgType)
  {
    case AS_REQ_SET_BOOK_CONFIG:
      return ProcessSetBookMessage(message);
    case AS_REQ_SET_ARCA_BOOK_FEED:
      return ProcessSetArcaFeedMessage(message);
    case AS_REQ_SET_ORDER_CONFIG:
      return ProcessSetOrderMessage(message);
    case AS_REQ_SET_GLOBAL_CONFIG:
      return ProcessSetGlobalConfiguration(message);
    case AS_REQ_GET_PRICE_UPDATE:
      return ParseGetPriceUpdateForSymbolList(message);
    case AS_REQ_FLUSH_BOOK_OF_ALL_SYMBOLS:
      return ProcessFlushBookOfAllSymbols(message);
    case AS_REQ_FLUSH_BOOK_OF_A_SYMBOL:
      return ProcessFlushBookOfASymbol(message);
    case AS_REQ_RESET_CURRENT_STATUS:
      return ProcessSetCurrentStatus(message);
    case AS_REQ_UPDATE_CURRENT_STATUS:
      return ProcessSet_UpdateStatus(message);
    case AS_REQ_UPDATE_DISABLE_SYMBOL_LIST:
      return ProcessSet_UpdateDisableSymbolList(message);
    case AS_REQ_UPDATE_DISABLE_SYMBOL:
      return ProcessSet_UpdateDisableSymbol(message);
    case AS_REQ_UPDATE_DISABLE_SYMBOL_RANGES_LIST:
      return ProcessSet_UpdateDisableSymbolRangesList(message);
    case AS_REQ_UPDATE_DISABLE_SYMBOL_RANGES:
      return ProcessSet_UpdateDisableSymbolRanges(message); 
    case AS_REQ_HALTED_SYMBOL_LIST:
      return ProcessSet_HaltedSymbolList(message);
    case AS_REQ_A_HALTED_SYMBOL:
      return ProcessSet_AHaltedSymbol(message);
    case MSG_TYPE_FOR_SET_EXCHANGE_CONF_FROM_AS:
      return ProcessSetExchangeConf(message);
    case MSG_TYPE_FOR_SET_BOOK_ISO_TRADING_FROM_AS:
      return ProcessSetBook_ISO_Trading(message);
    case MSG_TYPE_FOR_REQUEST_FLUSH_BBO_QUOTES_FROM_AS:
      return ProcessFlushBBOQuotes(message);
    case AS_REQ_SET_SYMBOL_STUCK_STATUS:
      return ProcessSetStuckUnlockForASymbol(message);
    case AS_REQ_SET_STUCK_SYMBOL_LIST:
      return ProcessSetStuckForSymbolList(message);
    case AS_REQ_SET_SYMBOL_LOCK_COUNTER:
      return ProcessSetSymbolLockCounter(message);
    case AS_REQ_SET_MIN_SPREAD_CONF:
      return ProcessSetMinSpreadConf(message);
    case AS_REQ_SET_VOLATILE_SYMBOL_LIST:
      return ProcessSetVolatileSymbolList(message);
    case AS_REQ_SET_ALL_VOLATILE_TOGGLE_VALUE:
      return ProcessSetAllVolatileToggle(message);
    case AS_REQ_SET_ETB_SYMBOL_LIST:
      return ProcessSetEtbSymbolList(message);
    case AS_REQ_SET_TRADING_ACCOUNT_LIST:
      return ProcessTradingAccountList(message);
    case AS_REQ_RESET_HUNG_ORDER:
      return ProcessHungOrderReset(message);
    default:
      break;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessSetArcaFeedMessage
- Description:
****************************************************************************/
int ProcessSetArcaFeedMessage(char *message)
{
  char feedName = message[10];
  if ((feedName == 'A') || (feedName == 'a'))
  {
    ARCA_Book_Conf.currentFeedName = 'A';
  }
  else if ((feedName == 'B') || (feedName == 'b'))
  {
    ARCA_Book_Conf.currentFeedName = 'B';
  }
  else
  {
    ARCA_Book_Conf.currentFeedName = 'B';
    TraceLog(ERROR_LEVEL, "Received invalid ARCA feed value from TT (ASCII value: %d), assigned to feed 'B' as default\n", feedName);
  }
  
  //Save new config to file (arca_switch)
  char fullPath[MAX_PATH_LEN];
  if (GetFullConfigPath("arca_switch", CONF_BOOK, fullPath) == NULL_POINTER)
  {
    return ERROR;
  }
  
  FILE *fp = fopen(fullPath, "w+");
  if (fp == NULL)
  {
    TraceLog(ERROR_LEVEL, "Could not open file to write (%s)\n", fullPath);
    return ERROR;
  }

  fprintf(fp, "# ARCA Integrated XDP Book Feed Name to use (Correct value is a single character A or B)\n%c\n", feedName);
  fclose(fp);
  
  //Reload the config
  if (LoadARCA_Book_Config("arca_switch", ARCA_Book_Conf.group) == ERROR)
  {
    return ERROR;
  }
  
  UpdateDBLFilterForNewVenueAddress(BOOK_ARCA);
  
  //Send back the config to AS and TT
  BuildSend_ARCA_Book_Configuration();
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessSetBookMessage
- Description:
****************************************************************************/
int ProcessSetBookMessage(char *message)
{
  int ecnID = message[10];
  
  switch(ecnID)
  {
    case BOOK_ARCA:
      return ProcessSet_ARCA_Book_Configuration(message);
    case BOOK_BATSZ:
      return ProcessSet_BATSZ_Book_Configuration();
    case BOOK_PSX:
    case BOOK_BYX:
    case BOOK_AMEX:
      // This feature is no longer supported
      break;
    case BOOK_NASDAQ:
      return ProcessSet_NASDAQ_Book_Configuration(message);
    case BOOK_NDBX:
      return ProcessSet_NDBX_Book_Configuration(message);
    case BOOK_NYSE:
      return ProcessSet_NYSE_Book_Configuration(message);
    case BOOK_EDGX:
      return ProcessSet_EDGX_Book_Configuration();
    case BOOK_EDGA:
      return ProcessSet_EDGA_Book_Configuration();
    default:
      break;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessSet_ARCA_Book_Configuration
- Description:
****************************************************************************/
int ProcessSet_ARCA_Book_Configuration(char *message)
{
  int index = 11;

  int type = message[index];
  index += 1;

  char fullPath[MAX_PATH_LEN];
  if (ARCA_Book_Conf.currentFeedName == 'A')
  {
    GetFullConfigPath("arca.xdp.afeed", CONF_BOOK, fullPath);
  }
  else
  {
    GetFullConfigPath("arca.xdp.bfeed", CONF_BOOK, fullPath);
  }
  
  if (type == UPDATE_SOURCE_ID)
  {
    //Removed recovery
  }
  else if (type == UPDATE_RETRANSMISSION_REQUEST)
  {
    //Now ARCA Book XDP use only 1 retransmission request server, so we do not update all groups
    //So, some of following lines of code should be removed (AS and TT needs to be updated)
    //For now, we have not changed AS and TT, so we just take what we want and ignore other things
    
    //int groupIndex = message[index];
    index += 1;

    // Retransmission Request TCP/IP
    index += MAX_LINE_LEN;

    // Retransmission port
    index += 4;
  }
  else
  {
    TraceLog(ERROR_LEVEL, "Set ARCA Book config, type = %d\n", type);

    return ERROR;
  }

  FILE *fileDesc = fopen(fullPath, "w");
  if (fileDesc != NULL)
  {
    fprintf(fileDesc, "# Note: In this file, every record is mandatory, *except backup feed*\n");
    fprintf(fileDesc, "# Backup (companion) feed, is *optional*, if used, TS will subscribe to this backup feed\n");
    fprintf(fileDesc, "#  along with primary feed, TS will take data which come first among the two feeds\n");
    fprintf(fileDesc, "# In order to add a backup feed for a specific primary feed, please add it *immediately after* that primary feed\n");
    fprintf(fileDesc, "#  with the following syntax:  backup:<ip>:<port>\n");
    fprintf(fileDesc, "# If you do not use any backup feed, you can put any value for 'Backup Interface'\n");
    fprintf(fileDesc, "#  however, once you use one (or more), you must specify correct Interface for backup feed(s)\n\n");
    
    fprintf(fileDesc, "# Cumulative number of packet loss without doing book flush (non-recovery mode)\n");
    fprintf(fileDesc, "%d\n", ARCA_Book_Conf.recoveryThreshold);
    
    fprintf(fileDesc, "# Interface used with primary/recovery feeds\n");
    fprintf(fileDesc, "%s\n", ARCA_Book_Conf.NICName);
    
    fprintf(fileDesc, "# Interface used with backup feed(s)\n");
    fprintf(fileDesc, "%s\n", ARCA_Book_Conf.backupNIC);
    
    fprintf(fileDesc, "# Retransmission Request server)\n");
    fprintf(fileDesc, "request:%s:%d\n", "0.0.0.0", 0);
    
    fprintf(fileDesc, "# Source ID\n");
    fprintf(fileDesc, "%s\n", "UNUSED");
    
    for (index = 0; index < MAX_ARCA_GROUPS; index++)
    {
      fprintf(fileDesc, "# Group %d\n", index + 1);
      fprintf(fileDesc, "primary:%s:%d\n", ARCA_Book_Conf.group[index].dataIP, ARCA_Book_Conf.group[index].dataPort);
      if (ARCA_Book_Conf.group[index].backupIP[0] != 0)
      {
        fprintf(fileDesc, "backup:%s:%d\n", ARCA_Book_Conf.group[index].backupIP, ARCA_Book_Conf.group[index].backupPort);
      }
      fprintf(fileDesc, "recovery:%s:%d\n", "0.0.0.0", 0);
    }
    
    fclose(fileDesc);
  }
  else
  {
    TraceLog(ERROR_LEVEL, "Could not save ARCA Book configuration to file\n");

    return ERROR;
  }
  
  BuildSend_ARCA_Book_Configuration();
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessSet_BATSZ_Book_Configuration
****************************************************************************/
int ProcessSet_BATSZ_Book_Configuration()
{
  int index = 11;
  int i;
  
  //sessionSubId
  index += MAX_LINE_LEN;
  
  //Username
  index += MAX_LINE_LEN;
  
  //Password
  index += MAX_LINE_LEN;
  
  //GAP IP
  index += MAX_LINE_LEN;
  
  //GAP Port
  index += 4;
  
  // Update BATS PITCH Book configuration
  char fullPath[MAX_PATH_LEN];
  
  GetFullConfigPath("batsz", CONF_BOOK, fullPath);
  
  FILE *fileDesc = fopen(fullPath, "w");
  
  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Could not open file '%s' to write\n", fullPath);
    return SUCCESS;
  }
  
  fprintf(fileDesc, "# Note: In this file, every record is mandatory, *except backup feed*\n");
  fprintf(fileDesc, "# Backup (companion) feed, is *optional*, if used, TS will subscribe to this backup feed\n");
  fprintf(fileDesc, "#  along with primary feed, TS will take data which come first among the two feeds\n");
  fprintf(fileDesc, "# In order to add a backup feed for a specific primary feed, please add it *immediately after* that primary feed\n");
  fprintf(fileDesc, "#  with the following syntax:  backup:<ip>:<port>\n");
  fprintf(fileDesc, "# If you do not use any backup feed, you can put any value for 'Backup Interface'\n");
  fprintf(fileDesc, "#  however, once you use one (or more), you must specify correct Interface for backup feed(s)\n\n");

  fprintf(fileDesc, "# Cumulative number of packet loss without doing book flush (non-recovery mode)\n");
  fprintf(fileDesc, "%d\n", BATSZ_Book_Conf.recoveryThreshold);
  
  //NIC Address for used with DBL connections
  fprintf(fileDesc, "# Interface used with primary/recovery feeds\n");
  fprintf(fileDesc, "%s\n", BATSZ_Book_Conf.NICName);
  
  fprintf(fileDesc, "# Interface used with backup feed(s)\n");
  fprintf(fileDesc, "%s\n", BATSZ_Book_Conf.backupNIC);
  
  //Session
  fprintf(fileDesc, "# Session\n");
  fprintf(fileDesc, "%s\n", "UNUSED");
  
  //Username
  fprintf(fileDesc, "# Username\n");
  fprintf(fileDesc, "%s\n", "UNUSED");
      
  //  password
  fprintf(fileDesc, "# Password\n");
  fprintf(fileDesc, "%s\n", "UNUSED");
  
  // GAP request IP/Port
  fprintf(fileDesc, "# Retransmission Request server\n");
  fprintf(fileDesc, "request:%s:%d\n", "0.0.0.0", 0);
  
  for(i=0; i<MAX_BATSZ_UNIT; i++)
  {
    fprintf(fileDesc, "# Unit %d:\n", i+1);
    fprintf(fileDesc, "primary:%s:%d\n", BATSZ_Book_Conf.group[i].dataIP, BATSZ_Book_Conf.group[i].dataPort);
    if (BATSZ_Book_Conf.group[i].backupIP[0] != 0)
    fprintf(fileDesc, "backup:%s:%d\n", BATSZ_Book_Conf.group[i].backupIP, BATSZ_Book_Conf.group[i].backupPort);
    fprintf(fileDesc, "recovery:%s:%d\n", "0.0.0.0", 0);
    
  }
  
  // Close file
  fclose(fileDesc);

  BuildSend_BATSZ_Book_Configuration();
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessSet_NASDAQ_Book_Configuration
****************************************************************************/
int ProcessSet_NASDAQ_Book_Configuration(char *message)
{
  int index = 11;
  
  /*
  IP and Port
  */
  memcpy(NASDAQ_Book_Conf.Data.ip, &message[index], 80);
  index += 80;
  
  memcpy(&NASDAQ_Book_Conf.Data.port, &message[index], 4);
  index += 4;
  
  /*
  index += 80; //Recovery IP
  
  index += 4;  //Recovery port

  memcpy(&NASDAQ_Book_Conf.maxMsgLoss, &message[index], 4);
  index += 4; */
  
  char fullPath[MAX_PATH_LEN];
  
  GetFullConfigPath("nasdaq", CONF_BOOK, fullPath);
  
  FILE *fileDesc = fopen(fullPath, "w");
  
  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Could not open file '%s' to write\n", fullPath);
    return SUCCESS;
  }
  
  /*
  IP and Port
  */
  fprintf(fileDesc, "# Note: In this file, every record is mandatory, *except backup feed*\n");
  fprintf(fileDesc, "# Backup (companion) feed, is *optional*, if used, TS will subscribe to this backup feed\n");
  fprintf(fileDesc, "#  along with primary feed, TS will take data which come first among the two feeds\n");
  fprintf(fileDesc, "# In order to add a backup feed for a specific primary feed, please add it *immediately after* that primary feed\n");
  fprintf(fileDesc, "#  with the following syntax:  backup:<ip>:<port>\n");
  fprintf(fileDesc, "# If you do not use any backup feed, you can put any value for 'Backup Interface'\n");
  fprintf(fileDesc, "#  however, once you use one (or more), you must specify correct Interface for backup feed(s)\n\n");

  fprintf(fileDesc, "# Interface used with primary feed\n");
  fprintf(fileDesc, "%s\n", NASDAQ_Book_Conf.Data.NICName);
  
  fprintf(fileDesc, "# Interface used with backup feed\n");
  fprintf(fileDesc, "%s\n", NASDAQ_Book_Conf.Backup.NICName);
  
  fprintf(fileDesc, "# Prinary data feed (primary:) and backup feed(backup:)\n");
  fprintf(fileDesc, "primary:%s:%d\n", NASDAQ_Book_Conf.Data.ip, NASDAQ_Book_Conf.Data.port);
  
  if (NASDAQ_Book_Conf.Backup.ip[0] != 0)
  {
    fprintf(fileDesc, "backup:%s:%d\n", NASDAQ_Book_Conf.Backup.ip, NASDAQ_Book_Conf.Backup.port);
  }

  fprintf(fileDesc, "# Cumulative number of packet loss without doing book flush (non-recovery mode)\n");
  fprintf(fileDesc, "%d\n", NASDAQ_Book_Conf.recoveryThreshold);
  
  // Close file
  fclose(fileDesc);

  UpdateDBLFilterForNewVenueAddress(BOOK_NASDAQ);
  BuildSend_NASDAQ_MDC_Configuration(BOOK_NASDAQ, (void*)&NASDAQ_Book_Conf);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessSet_NDBX_Book_Configuration
****************************************************************************/
int ProcessSet_NDBX_Book_Configuration(char *message)
{
  int index = 11;
  
  /*
  IP and Port
  */
  memcpy(NDBX_Book_Conf.Data.ip, &message[index], 80);
  index += 80;
  
  memcpy(&NDBX_Book_Conf.Data.port, &message[index], 4);
  index += 4;
  
  char fullPath[MAX_PATH_LEN];
  
  GetFullConfigPath("bx", CONF_BOOK, fullPath);
  
  FILE *fileDesc = fopen(fullPath, "w");
  
  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Could not open file '%s' to write\n", fullPath);
    return SUCCESS;
  }
  
  /*
  IP and Port
  */
  fprintf(fileDesc, "# Note: In this file, every record is mandatory, *except backup feed*\n");
  fprintf(fileDesc, "# Backup (companion) feed, is *optional*, if used, TS will subscribe to this backup feed\n");
  fprintf(fileDesc, "#  along with primary feed, TS will take data which come first among the two feeds\n");
  fprintf(fileDesc, "# In order to add a backup feed for a specific primary feed, please add it *immediately after* that primary feed\n");
  fprintf(fileDesc, "#  with the following syntax:  backup:<ip>:<port>\n");
  fprintf(fileDesc, "# If you do not use any backup feed, you can put any value for 'Backup Interface'\n");
  fprintf(fileDesc, "#  however, once you use one (or more), you must specify correct Interface for backup feed(s)\n\n");

  fprintf(fileDesc, "# Interface used with primary feed\n");
  fprintf(fileDesc, "%s\n", NDBX_Book_Conf.Data.NICName);
  
  fprintf(fileDesc, "# Interface used with backup feed\n");
  fprintf(fileDesc, "%s\n", NDBX_Book_Conf.Backup.NICName);
  
  fprintf(fileDesc, "# Prinary data feed (primary:) and backup feed(backup:)\n");
  fprintf(fileDesc, "primary:%s:%d\n", NDBX_Book_Conf.Data.ip, NDBX_Book_Conf.Data.port);
  
  if (NDBX_Book_Conf.Backup.ip[0] != 0)
  {
    fprintf(fileDesc, "backup:%s:%d\n", NDBX_Book_Conf.Backup.ip, NDBX_Book_Conf.Backup.port);
  }
  
  fprintf(fileDesc, "# Cumulative number of packet loss without doing book flush (non-recovery mode)\n");
  fprintf(fileDesc, "%d\n", NDBX_Book_Conf.recoveryThreshold);
  
  // Close file
  fclose(fileDesc);

  // UpdateDBLFilterForNewVenueAddress(BOOK_NDBX);
  BuildSend_NASDAQ_MDC_Configuration(BOOK_NDBX, (void*)&NDBX_Book_Conf);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessSet_NYSE_Book_Configuration
- Description:
****************************************************************************/
int ProcessSet_NYSE_Book_Configuration(char *message)
{
  int index = 11;
  int groupIndex;

  int msgType = message[index];
  index += 1;

  char fullPath[MAX_PATH_LEN];
  GetFullConfigPath("nyse_book", CONF_BOOK, fullPath);

  if (msgType == UPDATE_SOURCE_ID)
  {
    //Retransmission Request Source ID
    index += MAX_LINE_LEN;
  }
  else if (msgType == UPDATE_RETRANSMISSION_REQUEST)
  {
    groupIndex = message[index];
    index += 1;

    // Retransmission Request TCP/IP
    index += MAX_LINE_LEN;
    
    // Port
    index += 4;
  }
  else
  {
    TraceLog(ERROR_LEVEL, "Set NYSE Book config, type = %d\n", msgType);
    return ERROR;
  }

  //update config to file
  FILE* fileDesc = fopen(fullPath, "w");
  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Could not change NYSE Book configuration\n");
    return ERROR;
  }

  fprintf(fileDesc, "# Note: In this file, every record is mandatory, *except backup feed*\n");
  fprintf(fileDesc, "# Backup (companion) feed, is *optional*, if used, TS will subscribe to this backup feed\n");
  fprintf(fileDesc, "#  along with primary feed, TS will take data which come first among the two feeds\n");
  fprintf(fileDesc, "# In order to add a backup feed for a specific primary feed, please add it *immediately after* that primary feed\n");
  fprintf(fileDesc, "#  with the following syntax:  backup:<ip>:<port>\n");
  fprintf(fileDesc, "# If you do not use any backup feed, you can put any value for 'Backup Interface'\n");
  fprintf(fileDesc, "#  however, once you use one (or more), you must specify correct Interface for backup feed(s)\n\n");

  fprintf(fileDesc, "# Cumulative number of packet loss without doing book flush (non-recovery mode)\n");
  fprintf(fileDesc, "%d\n", NyseBookConf.recoveryThreshold);
  
  fprintf(fileDesc, "# Interface used with primary/recovery feeds\n%s\n", NyseBookConf.NICName);
  fprintf(fileDesc, "# Interface used with backup feeds\n%s\n", NyseBookConf.backupNIC);
  
  //write Source ID
  fprintf(fileDesc, "# Source ID (used to send request to Retransmission Request Server)\n%s\n\n", "UNUSED");

  //write information of each group
  for (groupIndex = 0; groupIndex < MAX_NYSE_BOOK_GROUPS; groupIndex++)
  {
    fprintf(fileDesc, "# Group %d\n", groupIndex + 1);
    fprintf(fileDesc, "request:%s:%d\n", "0.0.0.0", 0);
    fprintf(fileDesc, "primary:%s:%d\n", NyseBookConf.group[groupIndex].receiveDataIP, NyseBookConf.group[groupIndex].receiveDataPort);
    if (NyseBookConf.group[groupIndex].backupIP[0] != 0)
    {
      fprintf(fileDesc, "backup:%s:%d\n", NyseBookConf.group[groupIndex].backupIP, NyseBookConf.group[groupIndex].backupPort);
    }
    fprintf(fileDesc, "recovery:%s:%d\n", "0.0.0.0", 0);
  }

  fclose(fileDesc);

  BuildSend_NYSE_Book_Configuration();

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessSet_EDGX_Book_Configuration
- Description:
****************************************************************************/
int ProcessSet_EDGX_Book_Configuration()
{
  int index = 11;
  int i;

  // Retransmission IP
  index += MAX_LINE_LEN;
  
  // Retransmission Port
  index += 4;
  
  //Username
  index += MAX_LINE_LEN;

  //Password
  index += MAX_LINE_LEN;

  // Update EDGX Book configuration to file
  char fullPath[MAX_PATH_LEN];

  GetFullConfigPath("edgx_book", CONF_BOOK, fullPath);

  FILE *fileDesc = fopen(fullPath, "w");

  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Could not open file '%s' to write\n", fullPath);
    return SUCCESS;
  }

  fprintf(fileDesc, "# Note: In this file, every record is mandatory, *except backup feed*\n");
  fprintf(fileDesc, "# Backup (companion) feed, is *optional*, if used, TS will subscribe to this backup feed\n");
  fprintf(fileDesc, "#  along with primary feed, TS will take data which come first among the two feeds\n");
  fprintf(fileDesc, "# In order to add a backup feed for a specific primary feed, please add it *immediately after* that primary feed\n");
  fprintf(fileDesc, "#  with the following syntax:  backup:<ip>:<port>\n");
  fprintf(fileDesc, "# If you do not use any backup feed, you can put any value for 'Backup Interface'\n");
  fprintf(fileDesc, "#  however, once you use one (or more), you must specify correct Interface for backup feed(s)\n\n");

  fprintf(fileDesc, "# Cumulative number of packet loss without doing book flush (non-recovery mode)\n");
  fprintf(fileDesc, "%d\n", EDGX_BOOK_Conf.recoveryThreshold);
  
  fprintf(fileDesc, "#Interface used with primary/recovery feeds\n");
  fprintf(fileDesc, "%s\n", EDGX_BOOK_Conf.NICName);

  fprintf(fileDesc, "#Interface used with backup feed(s)\n");
  fprintf(fileDesc, "%s\n", EDGX_BOOK_Conf.backupNIC);
  
  fprintf(fileDesc, "#Retransmission Request Server\n");
  fprintf(fileDesc, "request:%s:%d\n", "0.0.0.0", 0);
  
  fprintf(fileDesc, "#Retransmission Request username and password\n");
  fprintf(fileDesc, "%s\n", "UNUSED");
  fprintf(fileDesc, "%s\n", "UNUSED");
  
  for(i = 0; i < MAX_EDGX_BOOK_GROUP; i++)
  {
    fprintf(fileDesc, "\n#Group %d\n", i+1);
    fprintf(fileDesc, "primary:%s:%d\n", EDGX_BOOK_Conf.group[i].dataIP, EDGX_BOOK_Conf.group[i].dataPort);
    if (EDGX_BOOK_Conf.group[i].backupIP[0] != 0)
    {
      fprintf(fileDesc, "backup:%s:%d\n", EDGX_BOOK_Conf.group[i].backupIP, EDGX_BOOK_Conf.group[i].backupPort);
    }
    fprintf(fileDesc, "recovery:%s:%d\n", "0.0.0.0", 0);
  }

  // Close file
  fclose(fileDesc);

  BuildSend_EDGX_Book_Configuration();
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessSet_EDGA_Book_Configuration
****************************************************************************/
int ProcessSet_EDGA_Book_Configuration()
{
  int index = 11;
  int i;

  // Retransmission IP
  index += MAX_LINE_LEN;
  
  // Retransmission Port
  index += 4;
  
  //Username
  index += MAX_LINE_LEN;

  //Password
  index += MAX_LINE_LEN;

  // Update EDGA Book configuration to file
  char fullPath[MAX_PATH_LEN];

  GetFullConfigPath("edga_book", CONF_BOOK, fullPath);

  FILE *fileDesc = fopen(fullPath, "w");

  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Could not open file '%s' to write\n", fullPath);
    return SUCCESS;
  }

  fprintf(fileDesc, "# Note: In this file, every record is mandatory, *except backup feed*\n");
  fprintf(fileDesc, "# Backup (companion) feed, is *optional*, if used, TS will subscribe to this backup feed\n");
  fprintf(fileDesc, "#  along with primary feed, TS will take data which come first among the two feeds\n");
  fprintf(fileDesc, "# In order to add a backup feed for a specific primary feed, please add it *immediately after* that primary feed\n");
  fprintf(fileDesc, "#  with the following syntax:  backup:<ip>:<port>\n");
  fprintf(fileDesc, "# If you do not use any backup feed, you can put any value for 'Backup Interface'\n");
  fprintf(fileDesc, "#  however, once you use one (or more), you must specify correct Interface for backup feed(s)\n\n");

  fprintf(fileDesc, "# Cumulative number of packet loss without doing book flush (non-recovery mode)\n");
  fprintf(fileDesc, "%d\n", EDGA_BOOK_Conf.recoveryThreshold);
  
  fprintf(fileDesc, "#Interface used with primary/recovery feeds\n");
  fprintf(fileDesc, "%s\n", EDGA_BOOK_Conf.NICName);

  fprintf(fileDesc, "#Interface used with backup feed(s)\n");
  fprintf(fileDesc, "%s\n", EDGA_BOOK_Conf.backupNIC);
  
  fprintf(fileDesc, "#Retransmission Request Server\n");
  fprintf(fileDesc, "request:%s:%d\n", "0.0.0.0", 0);
  
  fprintf(fileDesc, "#Retransmission Request username and password\n");
  fprintf(fileDesc, "%s\n", "UNUSED");
  fprintf(fileDesc, "%s\n", "UNUSED");
  
  for(i = 0; i < MAX_EDGA_BOOK_GROUP; i++)
  {
    fprintf(fileDesc, "\n#Group %d\n", i+1);
    fprintf(fileDesc, "primary:%s:%d\n", EDGA_BOOK_Conf.group[i].dataIP, EDGA_BOOK_Conf.group[i].dataPort);
    if (EDGA_BOOK_Conf.group[i].backupIP[0] != 0)
    {
      fprintf(fileDesc, "backup:%s:%d\n", EDGA_BOOK_Conf.group[i].backupIP, EDGA_BOOK_Conf.group[i].backupPort);
    }
    fprintf(fileDesc, "recovery:%s:%d\n", "0.0.0.0", 0);
  }

  // Close file
  fclose(fileDesc);

  BuildSend_EDGA_Book_Configuration();
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessSetOrderMessage
- Description:
****************************************************************************/
int ProcessSetOrderMessage(char *message)
{
  // ECN ID
  int ecnID = message[10];
  
  switch (ecnID)
  {
    case ORDER_ARCA_DIRECT:
      return ProcessSet_ARCA_DIRECT_Config(message);
    case ORDER_NYSE_CCG:
      return ProcessSet_NYSE_CCG_Config(message);
    case ORDER_NASDAQ_OUCH:
      return ProcessSet_NASDAQ_OUCH_Config(message);
    case ORDER_NASDAQ_BX:
    case ORDER_NASDAQ_PSX:
      break; //do not support
    case ORDER_NASDAQ_RASH:
      return ProcessSet_NASDAQ_RASH_Config(message);
    case ORDER_BATSZ_BOE:
      return ProcessSet_BATSZ_BOE_Config(message);
    case ORDER_BYX_BOE:
      break;
    case ORDER_EDGX:
      return ProcessSet_EDGX_Config(message);
    case ORDER_EDGA:
      return ProcessSet_EDGA_Config(message);

    default:
      break;
  }
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessSet_NYSE_CCG_Config
- Description:
****************************************************************************/
int ProcessSet_NYSE_CCG_Config(char *message)
{
  t_IntConverter intNum;
  int index = 11;
  
  // IP Address
  char ipAddress[MAX_LINE_LEN];
  memcpy(ipAddress, &message[index], 80);
  index += 80;
  
  // Port
  memcpy(intNum.c, &message[index], 4);
  int port = intNum.value;
  index += 4;
    
  // Username
  char userName[MAX_LINE_LEN];
  memcpy(userName, &message[index], 80);
  index += 80;

  RemoveCharacters(userName, strlen(userName));
  
  // Password
  char password[MAX_LINE_LEN];
  memcpy(password, &message[index], 80);
  index += 80;

  RemoveCharacters(password, strlen(password));
  
  // Update NYSE CCG configuration file
  char fullPath[MAX_PATH_LEN];
  
  if (GetFullConfigPath(FN_OEC_CONF_NYSE, CONF_ORDER, fullPath) == NULL_POINTER)
  {
    return ERROR;
  }
  
  FILE *fileDesc = fopen(fullPath, "w");

  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Could not write to file '%s'\n", fullPath);
    return SUCCESS;
  }
  
  fprintf(fileDesc, "#IP Address\n");
  fprintf(fileDesc, "%s\n", ipAddress);
  
  fprintf(fileDesc, "#Port\n");
  fprintf(fileDesc, "%d\n", port);
  
  fprintf(fileDesc, "#UserName\n");
  fprintf(fileDesc, "%s\n", userName);
  
  fprintf(fileDesc, "#Password\n");
  fprintf(fileDesc, "%s\n", password);
  
  fprintf(fileDesc, "#Auto Cancel on Disconnect: 0 or 1\n");
  fprintf(fileDesc, "%d\n", OecConfig[ORDER_NYSE_CCG].autoCancelOnDisconnect);
  
  fclose(fileDesc);
  
  //Update configuration structure
  //Ip Address
  strncpy(OecConfig[ORDER_NYSE_CCG].ipAddress, ipAddress, 32);
  
  //Port
  OecConfig[ORDER_NYSE_CCG].port = port;
  
  //Username
  strncpy(OecConfig[ORDER_NYSE_CCG].userName, userName, 32);
  
  //Password
  strncpy(OecConfig[ORDER_NYSE_CCG].password, password, 32);
  
  //Send Order Configuration to As
  BuildSend_NYSE_CCG_Configuration();
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessSet_ARCA_DIRECT_Config
- Description:
****************************************************************************/
int ProcessSet_ARCA_DIRECT_Config(char *message)
{
  t_IntConverter intNum;
  int index = 11;
  
  // IP Address
  char ipAddress[MAX_LINE_LEN];
  memcpy(ipAddress, &message[index], 80);
  index += 80;
  
  // Port
  memcpy(intNum.c, &message[index], 4);
  int port = intNum.value;
  index += 4;
    
  // Username
  char userName[MAX_LINE_LEN];
  memcpy(userName, &message[index], 80);
  index += 80;

  RemoveCharacters(userName, strlen(userName));
  
  // Password
  char password[MAX_LINE_LEN];
  memcpy(password, &message[index], 80);
  index += 80;

  RemoveCharacters(password, strlen(password));
  
  // Update ARCA DIRECT configuration file
  char fullPath[MAX_PATH_LEN];
  
  if (GetFullConfigPath(FN_OEC_CONF_ARCA, CONF_ORDER, fullPath) == NULL_POINTER)
  {
    return ERROR;
  }
  
  FILE *fileDesc = fopen(fullPath, "w");

  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Could not write to file '%s'\n", fullPath);
    return SUCCESS;
  }
  
  fprintf(fileDesc, "#IP Address\n");
  fprintf(fileDesc, "%s\n", ipAddress);
  
  fprintf(fileDesc, "#Port\n");
  fprintf(fileDesc, "%d\n", port);
  
  fprintf(fileDesc, "#UserName\n");
  fprintf(fileDesc, "%s\n", userName);
  
  fprintf(fileDesc, "#Password\n");
  fprintf(fileDesc, "%s\n", password);
  
  fprintf(fileDesc, "#Company Group ID\n");
  fprintf(fileDesc, "%s\n", OecConfig[ORDER_ARCA_DIRECT].compGroupID);
  
  fprintf(fileDesc, "#Auto Cancel on Disconnect: 0 or 1\n");
  fprintf(fileDesc, "%d\n", OecConfig[ORDER_ARCA_DIRECT].autoCancelOnDisconnect);
  
  fclose(fileDesc);
  
  //Update configuration structure
  //Ip Address
  strncpy(OecConfig[ORDER_ARCA_DIRECT].ipAddress, ipAddress, 32);
  
  //Port
  OecConfig[ORDER_ARCA_DIRECT].port = port;
  
  //Username
  strncpy(OecConfig[ORDER_ARCA_DIRECT].userName, userName, 32);
  
  //Password
  strncpy(OecConfig[ORDER_ARCA_DIRECT].password, password, 32);
  
  //Send Order Configuration to As
  BuildSend_ARCA_DIRECT_Configuration();
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessSet_NASDAQ_OUCH_Config
- Description:
****************************************************************************/
int ProcessSet_NASDAQ_OUCH_Config(char *message)
{
  t_IntConverter intNum;
  int index = 11;
  
  // IP Address
  char ipAddress[MAX_LINE_LEN];
  memcpy(ipAddress, &message[index], 80);
  index += 80;
  
  // Port
  memcpy(intNum.c, &message[index], 4);
  int port = intNum.value;
  index += 4;
    
  // Username
  char userName[MAX_LINE_LEN];
  memcpy(userName, &message[index], 80);
  index += 80;
  
  // Password
  char password[MAX_LINE_LEN];
  memcpy(password, &message[index], 80);
  index += 80;
  
  // Update NASDAQ OUCH configuration file
  char fullPath[MAX_PATH_LEN];
  
  if (GetFullConfigPath(FN_OEC_CONF_OUCH, CONF_ORDER, fullPath) == NULL_POINTER)
  {
    return ERROR;
  }
  
  FILE *fileDesc = fopen(fullPath, "w");

  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Could not write to file '%s'\n", fullPath);
    return SUCCESS;
  }
  
  fprintf(fileDesc, "#IP Address\n");
  fprintf(fileDesc, "%s\n", ipAddress);
  
  fprintf(fileDesc, "#Port\n");
  fprintf(fileDesc, "%d\n", port);
  
  fprintf(fileDesc, "#userName\n");
  fprintf(fileDesc, "%s\n", userName);
  
  fprintf(fileDesc, "#password\n");
  fprintf(fileDesc, "%s\n", password);
  
  fprintf(fileDesc, "#protocol (must be tcp or udp)\n");
  fprintf(fileDesc, "%s\n", (OecConfig[ORDER_NASDAQ_OUCH].protocol == PROTOCOL_TCP) ? "tcp" : "udp");
  
  fclose(fileDesc);
  
  //Update configuration structure
  //Ip Address
  strncpy(OecConfig[ORDER_NASDAQ_OUCH].ipAddress, ipAddress, 32);
  
  //Port
  OecConfig[ORDER_NASDAQ_OUCH].port = port;
  
  //Username
  strncpy(OecConfig[ORDER_NASDAQ_OUCH].userName, userName, 32);
  
  //Password
  strncpy(OecConfig[ORDER_NASDAQ_OUCH].password, password, 32);
  
  //Send Order Configuration to As
  BuildSend_NASDAQ_OUCH_Configuration();
  
  return SUCCESS;
}

/*int ProcessSet_NASDAQ_BX_Config(int oecType, char *message)
{
  t_IntConverter intNum;
  int index = 11;
  
  // IP Address
  char ipAddress[MAX_LINE_LEN];
  memcpy(ipAddress, &message[index], 80);
  index += 80;
  
  // Port
  memcpy(intNum.c, &message[index], 4);
  int port = intNum.value;
  index += 4;
    
  // Username
  char userName[MAX_LINE_LEN];
  memcpy(userName, &message[index], 80);
  index += 80;
  
  // Password
  char password[MAX_LINE_LEN];
  memcpy(password, &message[index], 80);
  index += 80;
  
  // Update NASDAQ OUCH configuration file
  char fullPath[MAX_PATH_LEN];
  
  if (GetFullConfigPath(oecType == ORDER_NASDAQ_BX ? FN_OEC_CONF_BX : FN_OEC_CONF_PSX, CONF_ORDER, fullPath) == NULL_POINTER)
  {
    return ERROR;
  }
  
  FILE *fileDesc = fopen(fullPath, "w");

  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Cannot write to file '%s'\n", fullPath);
    return SUCCESS;
  }
  
  fprintf(fileDesc, "#IP Address\n");
  fprintf(fileDesc, "%s\n", ipAddress);
  
  fprintf(fileDesc, "#Port\n");
  fprintf(fileDesc, "%d\n", port);
  
  fprintf(fileDesc, "#userName\n");
  fprintf(fileDesc, "%s\n", userName);
  
  fprintf(fileDesc, "#password\n");
  fprintf(fileDesc, "%s\n", password);
  
  fclose(fileDesc);
  
  //Update configuration structure
  //Ip Address
  strncpy(OecConfig[oecType].ipAddress, ipAddress, 32);
  
  //Port
  OecConfig[oecType].port = port;
  
  //Username
  strncpy(OecConfig[oecType].userName, userName, 32);
  
  //Password
  strncpy(OecConfig[oecType].password, password, 32);
  
  //Send Order Configuration to As
  BuildSend_NASDAQ_BX_Configuration(oecType);
  
  return SUCCESS;
}*/

/****************************************************************************
- Function name:  ProcessSet_NASDAQ_RASH_Config
- Description:
****************************************************************************/
int ProcessSet_NASDAQ_RASH_Config(char *message)
{
  t_IntConverter intNum;
  int index = 11;
  
  // IP Address
  char ipAddress[MAX_LINE_LEN];
  memcpy(ipAddress, &message[index], 80);
  index += 80;
  
  // Port
  memcpy(intNum.c, &message[index], 4);
  int port = intNum.value;
  index += 4;
    
  // Username
  char userName[MAX_LINE_LEN];
  memcpy(userName, &message[index], 80);
  index += 80;
  
  // Password
  char password[MAX_LINE_LEN];
  memcpy(password, &message[index], 80);
  index += 80;
  
  // Update NASDAQ RASH configuration file
  char fullPath[MAX_PATH_LEN];
  
  if (GetFullConfigPath(FN_OEC_CONF_RASH, CONF_ORDER, fullPath) == NULL_POINTER)
  {
    return ERROR;
  }
  
  FILE *fileDesc = fopen(fullPath, "w");

  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Could not write to file '%s'\n", fullPath);
    return SUCCESS;
  }
  
  fprintf(fileDesc, "#IP Address\n");
  fprintf(fileDesc, "%s\n", ipAddress);
  
  fprintf(fileDesc, "#Port\n");
  fprintf(fileDesc, "%d\n", port);
  
  fprintf(fileDesc, "#userName\n");
  fprintf(fileDesc, "%s\n", userName);
  
  fprintf(fileDesc, "#password\n");
  fprintf(fileDesc, "%s\n", password);
  
  fclose(fileDesc);
  
  //Update configuration structure
  //Ip Address
  strncpy(OecConfig[ORDER_NASDAQ_RASH].ipAddress, ipAddress, 32);
  
  //Port
  OecConfig[ORDER_NASDAQ_RASH].port = port;
  
  //Username
  strncpy(OecConfig[ORDER_NASDAQ_RASH].userName, userName, 32);
  
  //Password
  strncpy(OecConfig[ORDER_NASDAQ_RASH].password, password, 32);
  
  //Send Order Configuration to As
  BuildSend_NASDAQ_RASH_Configuration();
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessSet_BATSZ_BOE_Config
- Description:
****************************************************************************/
int ProcessSet_BATSZ_BOE_Config(char *message)
{
  t_IntConverter intNum;
  int index = 11;
  
  // IP Address
  char ipAddress[MAX_LINE_LEN];
  memcpy(ipAddress, &message[index], 80);
  index += 80;
  
  // Port
  memcpy(intNum.c, &message[index], 4);
  int port = intNum.value;
  index += 4;
    
  // Username
  char userName[MAX_LINE_LEN];
  memcpy(userName, &message[index], 80);
  index += 80;

  RemoveCharacters(userName, strlen(userName));
  
  // Password
  char password[MAX_LINE_LEN];
  memcpy(password, &message[index], 80);
  index += 80;

  RemoveCharacters(password, strlen(password));
  
  // Update BATSZ BOE configuration file
  char fullPath[MAX_PATH_LEN];
  
  if (GetFullConfigPath(FN_OEC_CONF_BATSZ, CONF_ORDER, fullPath) == NULL_POINTER)
  {
    return ERROR;
  }
  
  FILE *fileDesc = fopen(fullPath, "w");

  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Could not write to file '%s'\n", fullPath);
    return SUCCESS;
  }
  
  fprintf(fileDesc, "#IP Address\n");
  fprintf(fileDesc, "%s\n", ipAddress);
  
  fprintf(fileDesc, "#Port\n");
  fprintf(fileDesc, "%d\n", port);
  
  fprintf(fileDesc, "#sessionSubID\n");
  fprintf(fileDesc, "%s\n", OecConfig[ORDER_BATSZ_BOE].sessionSubId);
  
  fprintf(fileDesc, "#UserName\n");
  fprintf(fileDesc, "%s\n", userName);
  
  fprintf(fileDesc, "#Password\n");
  fprintf(fileDesc, "%s\n", password);
  
  fprintf(fileDesc, "#NoUnspecifiedUnitReplay: 0 or 1(initial: 1)\n");
  fprintf(fileDesc, "%d\n", OecConfig[ORDER_BATSZ_BOE].noUnspecifiedUnitReplay);
  
  fclose(fileDesc);
  
  //Update configuration structure
  //Ip Address
  strncpy(OecConfig[ORDER_BATSZ_BOE].ipAddress, ipAddress, 32);
  
  //Port
  OecConfig[ORDER_BATSZ_BOE].port = port;
  
  //Username
  strncpy(OecConfig[ORDER_BATSZ_BOE].userName, userName, 32);
  
  //Password
  strncpy(OecConfig[ORDER_BATSZ_BOE].password, password, 32);
  
  //Send Order Configuration to As
  BuildSend_BATSZ_BOE_Configuration();
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessSet_EDGX_Config
- Description:
****************************************************************************/
int ProcessSet_EDGX_Config(char *message)
{
  t_IntConverter intNum;
  int index = 11;
  
  // IP Address
  char ipAddress[MAX_LINE_LEN];
  memcpy(ipAddress, &message[index], 80);
  index += 80;
  
  // Port
  memcpy(intNum.c, &message[index], 4);
  int port = intNum.value;
  index += 4;
    
  // Username
  char userName[MAX_LINE_LEN];
  memcpy(userName, &message[index], 80);
  index += 80;
  
  // Password
  char password[MAX_LINE_LEN];
  memcpy(password, &message[index], 80);
  index += 80;
  
  // Update EDGX configuration file
  char fullPath[MAX_PATH_LEN];
  
  if (GetFullConfigPath(FN_OEC_CONF_EDGX, CONF_ORDER, fullPath) == NULL_POINTER)
  {
    return ERROR;
  }
  
  FILE *fileDesc = fopen(fullPath, "w");

  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Could not write to file '%s'\n", fullPath);
    return SUCCESS;
  }
  
  fprintf(fileDesc, "#IP Address\n");
  fprintf(fileDesc, "%s\n", ipAddress);
  
  fprintf(fileDesc, "#Port\n");
  fprintf(fileDesc, "%d\n", port);
  
  fprintf(fileDesc, "#userName\n");
  fprintf(fileDesc, "%s\n", userName);
  
  fprintf(fileDesc, "#password\n");
  fprintf(fileDesc, "%s\n", password);
  
  fclose(fileDesc);
  
  //Update configuration structure
  //Ip Address
  strncpy(OecConfig[ORDER_EDGX].ipAddress, ipAddress, 32);
  
  //Port
  OecConfig[ORDER_EDGX].port = port;
  
  //Username
  strncpy(OecConfig[ORDER_EDGX].userName, userName, 32);
  
  //Password
  strncpy(OecConfig[ORDER_EDGX].password, password, 32);
  
  //Send Order Configuration to As
  BuildSend_EDGX_Configuration();
  
  return SUCCESS;
}
/****************************************************************************
- Function name:  ProcessSet_EDGA_Config
****************************************************************************/
int ProcessSet_EDGA_Config(char *message)
{
  t_IntConverter intNum;
  int index = 11;
  
  // IP Address
  char ipAddress[MAX_LINE_LEN];
  memcpy(ipAddress, &message[index], 80);
  index += 80;
  
  // Port
  memcpy(intNum.c, &message[index], 4);
  int port = intNum.value;
  index += 4;
    
  // Username
  char userName[MAX_LINE_LEN];
  memcpy(userName, &message[index], 80);
  index += 80;
  
  // Password
  char password[MAX_LINE_LEN];
  memcpy(password, &message[index], 80);
  index += 80;
  
  // Update EDGA configuration file
  char fullPath[MAX_PATH_LEN];
  
  if (GetFullConfigPath(FN_OEC_CONF_EDGA, CONF_ORDER, fullPath) == NULL_POINTER)
  {
    return ERROR;
  }
  
  FILE *fileDesc = fopen(fullPath, "w");

  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Cannot write to file '%s'\n", fullPath);
    return SUCCESS;
  }
  
  fprintf(fileDesc, "#IP Address\n");
  fprintf(fileDesc, "%s\n", ipAddress);
  
  fprintf(fileDesc, "#Port\n");
  fprintf(fileDesc, "%d\n", port);
  
  fprintf(fileDesc, "#userName\n");
  fprintf(fileDesc, "%s\n", userName);
  
  fprintf(fileDesc, "#password\n");
  fprintf(fileDesc, "%s\n", password);
  
  fclose(fileDesc);
  
  //Update configuration structure
  //Ip Address
  strncpy(OecConfig[ORDER_EDGA].ipAddress, ipAddress, 32);
  
  //Port
  OecConfig[ORDER_EDGA].port = port;
  
  //Username
  strncpy(OecConfig[ORDER_EDGA].userName, userName, 32);
  
  //Password
  strncpy(OecConfig[ORDER_EDGA].password, password, 32);
  
  //Send Order Configuration to As
  BuildSend_EDGA_Configuration();
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessSetGlobalConfiguration
- Description:
****************************************************************************/
int ProcessSetGlobalConfiguration(char *message)
{
  // !!! IMPORTANT: Please make sure that the buffer can contain whole the file contents
  char contentsBuffer[50000];
  memset(contentsBuffer, 0 , 50000);

  // Open file for reading
  char fullPath[MAX_PATH_LEN];

  if (GetFullConfigPath("algorithm", CONF_MISC, fullPath) == NULL_POINTER)
  {
    return ERROR;
  }

  FILE *file = fopen(fullPath, "r");

  if(file == NULL)
  {
    TraceLog(ERROR_LEVEL, "Could not open file to read\n");
    return ERROR;
  }
  
  // Read whole file contents into contentsBuffer[]
  fread(contentsBuffer, 1, 50000, file);
  fclose(file);
  
  // algorithm file data structure:
  /*
    [Global configuration]
    [Role of Books in cross detecting]
    [Maximum spread is used by Books]
    [Priority of Books]
    [Book oversize score]
    [How to orders at Entry can be placed]
    [Split symbol configuration]
    [NASDAQ strategy configuration]
    [Maximum spread configuration]
  */
  
  /*
  Originally, the function only updates the [Global configuration] section
  But now, due to the extension on TT, this function also update the [Maximum spread configuration] section
  So the flow should be like following:
  1. Write [Global configuration] section, by using value from settings (say PART 1)
  2. Write the following blocks as they are (say PART 2)
    [Role of Books in cross detecting]
    [Maximum spread is used by Books]
    [Priority of Books]
    [How to orders at Entry can be placed]
    [Split symbol configuration]
    [NASDAQ strategy configuration]
  3. Write the [Maximum spread configuration] section by using values from settings (say PART 3)
  */
  
  // Buffer for PART 2
  char part2Buffer[50000];
  memset(part2Buffer, 0 , 50000);
  
  //Now we store data into PART 2
  //Start position of PART 2:
  char *pointer = strstr(contentsBuffer, "[Role of Books in cross detecting]");
  if(pointer == NULL)
  {
    TraceLog(ERROR_LEVEL, "Could not locate [Role of Books in cross detecting] section in the file algorithm\n");
    return ERROR;
  }
  strcpy(part2Buffer, pointer);
  
  //End position of PART 2
  pointer = strstr(part2Buffer, "[Maximum spread configuration]");
  if(pointer == NULL)
  {
    TraceLog(ERROR_LEVEL, "Could not locate [Maximum spread configuration] section in the file algorithm\n");
    return ERROR;
  }
  *pointer = 0; //Assign NULL to make it end at right position
  
  // --------------------------------------------------------
  //  Now we are updating [Global configuration] section
  //  This is the PART 1
  // --------------------------------------------------------
  t_IntConverter intNum;
  t_DoubleConverter doubleNum;
  int index = 10;     //Index to get value from AS message

  // We do not update the Max Buying Power field
  // maxBuyingPower
  //memcpy(doubleNum.c, &message[index], 8);
  
  // Update maxBuyingPower
  //AlgorithmConfig.globalConfig.maxBuyingPower = doubleNum.value;

  TraceLog(DEBUG_LEVEL, "maxBuyingPower = %lf\n", AlgorithmConfig.globalConfig.maxBuyingPower);
  index += 8;

  // maxShareVolume
  memcpy(intNum.c, &message[index], 4);
  index += 4;
  AlgorithmConfig.globalConfig.maxShareVolume = intNum.value;
  TraceLog(DEBUG_LEVEL, "maxShareVolume = %d\n", AlgorithmConfig.globalConfig.maxShareVolume);

  // maxConsecutiveLoser
  memcpy(intNum.c, &message[index], 4);
  index += 4;
  AlgorithmConfig.globalConfig.maxConsecutiveLoser = intNum.value;
  TraceLog(DEBUG_LEVEL, "maxConsecutiveLoser = %d\n", AlgorithmConfig.globalConfig.maxConsecutiveLoser);

  // maxStuckPosition
  memcpy(intNum.c, &message[index], 4);
  index += 4;
  AlgorithmConfig.globalConfig.maxStuckPosition = intNum.value;
  TraceLog(DEBUG_LEVEL, "maxStuckPosition = %d\n", AlgorithmConfig.globalConfig.maxStuckPosition);
  
  // serverRole
  memcpy(intNum.c, &message[index], 4);
  index += 4;
  AlgorithmConfig.globalConfig.serverRole = intNum.value;
  TraceLog(DEBUG_LEVEL, "serverRole = %d\n", AlgorithmConfig.globalConfig.serverRole);
  
  // minSpread
  memcpy(doubleNum.c, &message[index], 8);
  index += 8;
  
  /*
  // Update minSpread => this value will be updated by min spread strategy
  AlgorithmConfig.globalConfig.minSpread = doubleNum.value;
  TraceLog(DEBUG_LEVEL, "minSpread = %lf\n", AlgorithmConfig.globalConfig.minSpread);
  */
  
  // maxSpread
  memcpy(doubleNum.c, &message[index], 8);
  index += 8;
  AlgorithmConfig.globalConfig.maxSpread = doubleNum.value;
  TraceLog(DEBUG_LEVEL, "maxSpread = %lf\n", AlgorithmConfig.globalConfig.maxSpread);
  
  // secFee
  memcpy(doubleNum.c, &message[index], 8);
  index += 8;
  AlgorithmConfig.globalConfig.secFee = doubleNum.value;
  TraceLog(DEBUG_LEVEL, "secFee = %lf\n", AlgorithmConfig.globalConfig.secFee);

  // commission
  memcpy(doubleNum.c, &message[index], 8);
  index += 8;
  AlgorithmConfig.globalConfig.commission = doubleNum.value;
  TraceLog(DEBUG_LEVEL, "commission = %lf\n", AlgorithmConfig.globalConfig.commission);
  
  SecFeeAndCommissionInCent = (AlgorithmConfig.globalConfig.commission + AlgorithmConfig.globalConfig.secFee) * 100.00;
  
  // loser rate
  memcpy(doubleNum.c, &message[index], 8);
  index += 8;
  AlgorithmConfig.globalConfig.loserRate = doubleNum.value;
  TraceLog(DEBUG_LEVEL, "loser rate = %lf\n", AlgorithmConfig.globalConfig.loserRate);
  
  // exRatio
  memcpy(doubleNum.c, &message[index], 8);
  index += 8;
  AlgorithmConfig.globalConfig.exRatio = doubleNum.value;
  TraceLog(DEBUG_LEVEL, "exRatio = %lf\n", AlgorithmConfig.globalConfig.exRatio);
  
  // nasdaqAuctionBeginTime
  memcpy(intNum.c, &message[index], 4);
  index += 4;
  AlgorithmConfig.globalConfig.nasdaqAuctionBeginTime = intNum.value;
  if (AlgorithmConfig.globalConfig.nasdaqAuctionBeginTime > 9*3600+30*60)
  {
    TraceLog(WARN_LEVEL, "Invalid value for Nasdaq Auction begin-time! Default value is taken: 09:28:00\n");
    AlgorithmConfig.globalConfig.nasdaqAuctionBeginTime = 9*3600 + 28*60;
  }
  else
  {
    TraceLog(DEBUG_LEVEL, "nasdaqAuctionBeginTime = %d\n", AlgorithmConfig.globalConfig.nasdaqAuctionBeginTime);
  }

  // nasdaqAuctionEndTime
  memcpy(intNum.c, &message[index], 4);
  index += 4;
  AlgorithmConfig.globalConfig.nasdaqAuctionEndTime = intNum.value;
  if (AlgorithmConfig.globalConfig.nasdaqAuctionEndTime > 9*3600+30*60)
  {
    TraceLog(WARN_LEVEL, "Invalid value for Nasdaq Auction end-time! Default value is taken: 09:30:00\n");
    AlgorithmConfig.globalConfig.nasdaqAuctionEndTime = 9*3600 + 30*60;
  }
  else
  { 
    TraceLog(DEBUG_LEVEL, "nasdaqAuctionEndTime = %d\n", AlgorithmConfig.globalConfig.nasdaqAuctionEndTime);
  }
  
  FILE *fileDesc = fopen(fullPath, "w");

  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Could not write to file '%s'\n", fullPath);
    return SUCCESS;
  }

  fprintf(fileDesc, "[Global configuration]\n");
  fprintf(fileDesc, "Maximum shares = %d\n", AlgorithmConfig.globalConfig.maxShareVolume);
  fprintf(fileDesc, "Maximum buying power = %lf\n", AlgorithmConfig.globalConfig.maxBuyingPower);
  fprintf(fileDesc, "Maximum consecutive losers = %d\n", AlgorithmConfig.globalConfig.maxConsecutiveLoser);
  fprintf(fileDesc, "Maximum stuck positions = %d\n", AlgorithmConfig.globalConfig.maxStuckPosition);
  fprintf(fileDesc, "Minimum spread = %lf\n", AlgorithmConfig.globalConfig.minSpread);
  fprintf(fileDesc, "Maximum spread = %lf\n", AlgorithmConfig.globalConfig.maxSpread);
  fprintf(fileDesc, "Sec Fee = %lf\n", AlgorithmConfig.globalConfig.secFee);
  fprintf(fileDesc, "Commission = %lf\n", AlgorithmConfig.globalConfig.commission);
  fprintf(fileDesc, "Loser rate = %lf\n", AlgorithmConfig.globalConfig.loserRate);
  fprintf(fileDesc, "EX Ratio = %lf\n", AlgorithmConfig.globalConfig.exRatio);

  if(AlgorithmConfig.globalConfig.serverRole == 0)
  {
    fprintf(fileDesc, "Server Role = %s\n", "ARCA");
  }
  else if(AlgorithmConfig.globalConfig.serverRole == 1)
  {
    fprintf(fileDesc, "Server Role = %s\n", "NASDAQ");
  }
  else if(AlgorithmConfig.globalConfig.serverRole == 2)
  {
    fprintf(fileDesc, "Server Role = %s\n", "BATS");
  }
  else if(AlgorithmConfig.globalConfig.serverRole == 3)
  {
    fprintf(fileDesc, "Server Role = %s\n", "EDGE");
  }
  else
  {
    fprintf(fileDesc, "Server Role = %s\n", "STAND_ALONE");
  }
  
  fprintf(fileDesc, "Nasdaq Auction begin time = %02d:%02d:%02d\n", 
    AlgorithmConfig.globalConfig.nasdaqAuctionBeginTime / 3600, 
    (AlgorithmConfig.globalConfig.nasdaqAuctionBeginTime % 3600) / 60,
    ((AlgorithmConfig.globalConfig.nasdaqAuctionBeginTime % 3600) % 60));
    
  fprintf(fileDesc, "Nasdaq Auction end time = %02d:%02d:%02d\n", 
    AlgorithmConfig.globalConfig.nasdaqAuctionEndTime / 3600, 
    (AlgorithmConfig.globalConfig.nasdaqAuctionEndTime % 3600) / 60,
    ((AlgorithmConfig.globalConfig.nasdaqAuctionEndTime % 3600) % 60));
    
    
  // --------------------------------------------------------
  //    Now we write PART 2 as it is
  // --------------------------------------------------------
  fprintf(fileDesc, "%s", part2Buffer);
  
  // --------------------------------------------------------
  //    Now we update PART 3 from setting values
  //    [Maximum spread configuration]
  // --------------------------------------------------------
  
  // ---- REGULAR SESSION TIME ----
  // Regular session begin time
  t_ShortConverter shortNum;
  memcpy(shortNum.c, &message[index], 2);
  index += 2;
  
  if ((shortNum.value < 0) || (shortNum.value > 24*60))
  {
    TraceLog(WARN_LEVEL, "Invalid regular session begin time (time = %d:%d). It will be unchanged.\n", shortNum.value / 60, shortNum.value % 60);
  }
  else
  {
    TradingSessionMgmt.RegularSessionBeginHour = shortNum.value / 60;
    TradingSessionMgmt.RegularSessionBeginMin = shortNum.value % 60;
  }
  
  // Regular session end time
  memcpy(shortNum.c, &message[index], 2);
  index += 2;
  
  if ((shortNum.value < 0) || (shortNum.value > 24*60))
  {
    TraceLog(WARN_LEVEL, "Invalid regular session end time (time = %d:%d). It will be unchanged.\n", shortNum.value / 60, shortNum.value % 60);
  }
  else
  {
    TradingSessionMgmt.RegularSessionEndHour = shortNum.value / 60;
    TradingSessionMgmt.RegularSessionEndMin = shortNum.value % 60;
  }
    
  fprintf(fileDesc, "[Maximum spread configuration]\n");
  fprintf(fileDesc, "###############################\n");
  fprintf(fileDesc, "#Numerical Thresholds for Regular Session\n");
  fprintf(fileDesc, "###############################\n");
  fprintf(fileDesc, "Market type = Regular Session\n");
  fprintf(fileDesc, "Execution price is $25 and under = %lf\n", AlgorithmConfig.maxSpreadConfig.maxSpread[INTRADAY].execPriceUnder_25USD);
  fprintf(fileDesc, "Execution price is over $25 and up to $50 = %lf\n", AlgorithmConfig.maxSpreadConfig.maxSpread[INTRADAY].execPriceUnder_50USD);
  fprintf(fileDesc, "Execution price is over $50 = %lf\n\n", AlgorithmConfig.maxSpreadConfig.maxSpread[INTRADAY].execPriceOver_50USD);

  fprintf(fileDesc, "###############################\n");
  fprintf(fileDesc, "#Numerical Thresholds for Outside Regular Session\n");
  fprintf(fileDesc, "###############################\n");
  fprintf(fileDesc, "Market type = Outside Regular Session\n");
  fprintf(fileDesc, "Execution price is $25 and under = %lf\n", AlgorithmConfig.maxSpreadConfig.maxSpread[PRE_MARKET].execPriceUnder_25USD);
  fprintf(fileDesc, "Execution price is over $25 and up to $50 = %lf\n", AlgorithmConfig.maxSpreadConfig.maxSpread[PRE_MARKET].execPriceUnder_50USD);
  fprintf(fileDesc, "Execution price is over $50 = %lf\n\n", AlgorithmConfig.maxSpreadConfig.maxSpread[PRE_MARKET].execPriceOver_50USD);
  
  fprintf(fileDesc, "###############################\n");
  fprintf(fileDesc, "#Regular Session Time\n");
  fprintf(fileDesc, "###############################\n");
  
  fprintf(fileDesc, "Regular session begin hour = %d\n", TradingSessionMgmt.RegularSessionBeginHour);
  fprintf(fileDesc, "Regular session begin min = %d\n", TradingSessionMgmt.RegularSessionBeginMin);
  fprintf(fileDesc, "Regular session end hour = %d\n", TradingSessionMgmt.RegularSessionEndHour);
  fprintf(fileDesc, "Regular session end min = %d\n\n", TradingSessionMgmt.RegularSessionEndMin);
  
  fprintf(fileDesc, "[Order limit configuration]\n");
  fprintf(fileDesc, "Bucket size = %d\n", AlgorithmConfig.orderBucket.bucketSize);
  fprintf(fileDesc, "Refill per second = %d\n\n", AlgorithmConfig.orderBucket.refillPerSec);
  
  //Printf them out on the screen
  TraceLog(DEBUG_LEVEL, "Regular Session Time:\n");
  TraceLog(DEBUG_LEVEL, "Regular session begin hour = %d\n", TradingSessionMgmt.RegularSessionBeginHour);
  TraceLog(DEBUG_LEVEL, "Regular session begin min = %d\n", TradingSessionMgmt.RegularSessionBeginMin);
  TraceLog(DEBUG_LEVEL, "Regular session end hour = %d\n", TradingSessionMgmt.RegularSessionEndHour);
  TraceLog(DEBUG_LEVEL, "Regular session end min = %d\n", TradingSessionMgmt.RegularSessionEndMin);
  
  //  Close the file
  fclose(fileDesc);

  // Call the function to update CurrentSessionType
  UpdateCurrentSessionType();
  
  // After updating, send them back to AS for confirmation!
  BuildSend_Global_Configuration();

  TraceLog(DEBUG_LEVEL, "sent global configuration to AS\n");

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessSetMinSpreadConf
- Description:
****************************************************************************/
int ProcessSetMinSpreadConf(char *message)
{
  int index = 10; //Index to get value from AS message
  
  // Min spread in pre-market
  memcpy(&MinSpreadConf.preMarket, &message[index], 8);
  index += 8;
  
  TraceLog(DEBUG_LEVEL, "preMarket = %lf\n", MinSpreadConf.preMarket);
  
  // Min spread in post-market
  memcpy(&MinSpreadConf.postMarket, &message[index], 8);
  index += 8;
  
  TraceLog(DEBUG_LEVEL, "postMarket = %lf\n", MinSpreadConf.postMarket);
  
  // Num times of intraday
  memcpy(&MinSpreadConf.intraday.countTimes, &message[index], 4);
  index += 4;
  
  TraceLog(DEBUG_LEVEL, "countTimes = %d\n", MinSpreadConf.intraday.countTimes);
  
  int i;
  for (i = 0; i < MinSpreadConf.intraday.countTimes; i++)
  {
    memcpy(&MinSpreadConf.intraday.minSpreadList[i].timeInSeconds, &message[index], 4);
    index += 4;
    
    memcpy(&MinSpreadConf.intraday.minSpreadList[i].minSpread, &message[index], 8);
    index += 8;
    
    TraceLog(DEBUG_LEVEL, "%d timeInSeconds: %d minSpread: %lf\n", i, 
            MinSpreadConf.intraday.minSpreadList[i].timeInSeconds,
            MinSpreadConf.intraday.minSpreadList[i].minSpread);
  }
  
  // Sort data
  t_Min_Spread_Switch tmpMinSpread;
  
  int j;
  for (i = 0; i < MinSpreadConf.intraday.countTimes - 1; i++)
  {
    for (j = i+1; j < MinSpreadConf.intraday.countTimes; j++)
    {
      if (MinSpreadConf.intraday.minSpreadList[j].timeInSeconds < MinSpreadConf.intraday.minSpreadList[i].timeInSeconds) 
      {
        memcpy(&tmpMinSpread, &MinSpreadConf.intraday.minSpreadList[i], sizeof(t_Min_Spread_Switch));
        memcpy(&MinSpreadConf.intraday.minSpreadList[i], &MinSpreadConf.intraday.minSpreadList[j], sizeof(t_Min_Spread_Switch));
        memcpy(&MinSpreadConf.intraday.minSpreadList[j], &tmpMinSpread, sizeof(t_Min_Spread_Switch));
      }
    }
  }
  
  // Open file for reading
  char fullPath[MAX_PATH_LEN];

  if (GetFullConfigPath("min_spread", CONF_MISC, fullPath) == NULL_POINTER)
  {
    return ERROR;
  }
  
  /*
  # Minimum spread strategy
  # 3 sessions: PRE-MARKET, INTRADAY, POST-MARKET
  # Pre-market
  session = PRE-MARKET
  min spread = 0.05

  # Post-market
  session = POST-MARKET
  min spread = 0.05

  # Intraday
  session = INTRADAY
  time switch 1 = 08:30:00
  min spread 1 = 0.03

  time switch 2 = 10:30:00
  min spread 2 = 0.04

  time switch 3 = 12:30:00
  min spread 3 = 0.03
  */

  FILE *fileDesc = fopen(fullPath, "w");

  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Could not write to file '%s'\n", fullPath);
    return SUCCESS;
  }
  
  char tradeSession[MAX_MARKET_TIME_TYPE][32] = {"PRE-MARKET", "INTRADAY", "POST-MARKET"};
  
  fprintf(fileDesc, "# Minimum spread strategy\n");
  fprintf(fileDesc, "# 3 sessions: PRE-MARKET, INTRADAY, POST-MARKET\n");
  fprintf(fileDesc, "# Pre-market\n");
  fprintf(fileDesc, "session = %s\n", tradeSession[PRE_MARKET]);
  fprintf(fileDesc, "min spread = %lf\n\n", MinSpreadConf.preMarket);
  
  fprintf(fileDesc, "# Post-market\n");
  fprintf(fileDesc, "session = %s\n", tradeSession[POST_MARKET]);
  fprintf(fileDesc, "min spread = %lf\n\n", MinSpreadConf.postMarket);
  
  fprintf(fileDesc, "# Intraday\n");
  fprintf(fileDesc, "session = %s\n", tradeSession[INTRADAY]);
  
  int hours, mins, seconds;
  for (i = 0; i < MinSpreadConf.intraday.countTimes; i++)
  {
    hours = MinSpreadConf.intraday.minSpreadList[i].timeInSeconds / 3600;
    mins = (MinSpreadConf.intraday.minSpreadList[i].timeInSeconds - hours * 3600) / 60;
    seconds = (MinSpreadConf.intraday.minSpreadList[i].timeInSeconds - hours * 3600) % 60;
    
    fprintf(fileDesc, "time switch %d = %02d:%02d:%02d\n", i + 1, hours, mins, seconds);
    fprintf(fileDesc, "min spread %d= %lf\n\n", i + 1, MinSpreadConf.intraday.minSpreadList[i].minSpread);
  }
  
  //  Close the file
  fclose(fileDesc);
  
  BuildSend_MinSpread_Configuration();
  
  TraceLog(DEBUG_LEVEL, "sent minimum spread strategy to AS\n");
  
  // Calculate minimum spread
  // Calculate minimum spread
  // Pre-market
  int limit = TradingSchedule.timeToEndOfPreMarket;
  for (i = 0; i < limit; i++)
  {
    MinSpreadInSecond[i] = MinSpreadConf.preMarket;
  }
  
  // Post-market
  limit = TradingSchedule.timeToEndOfIntraday;
  for (i = limit; i < MAX_MIN_SPREAD_OF_DAY; i++)
  {
    MinSpreadInSecond[i] = MinSpreadConf.postMarket;
  }
  
  // Intraday
  int startIndex;
  for (i = MinSpreadConf.intraday.countTimes - 1; i >= 0; i--)
  {
    if (i == MinSpreadConf.intraday.countTimes - 1)
    {
      limit = TradingSchedule.timeToEndOfIntraday;
    }
    else
    {
      limit = MinSpreadConf.intraday.minSpreadList[i + 1].timeInSeconds;
    }
    
    if (i > 0)
    {
      startIndex = MinSpreadConf.intraday.minSpreadList[i].timeInSeconds;
    }
    else
    {
      startIndex = TradingSchedule.timeToEndOfPreMarket;
    }
    
    for (j = startIndex; j < limit; j++)
    {
      MinSpreadInSecond[j] = MinSpreadConf.intraday.minSpreadList[i].minSpread;
    }
  }
  
  // Update Current Market Time after received minimum spread
  UpdateCurrentMarketTime();
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessSetCurrentStatus
- Description:
****************************************************************************/
int ProcessSetCurrentStatus(char *message)
{
  int typeId  = message[10];

  
  switch (typeId)
  {
    case NUM_STUCK_INDEX:
      TraceLog(DEBUG_LEVEL, "Received request to reset current stuck position\n");
      TSCurrentStatus.numStuck = 0;
      break;
    case BUYING_POWER_INDEX:
      TraceLog(DEBUG_LEVEL, "Received request to reset current buying power\n");
      TSCurrentStatus.buyingPower = (long)(AlgorithmConfig.globalConfig.maxBuyingPower * 100.0);
      break;
    case NUM_LOSER_INDEX:
      TraceLog(DEBUG_LEVEL, "Received request to reset current consecutive loser\n");
      TSCurrentStatus.numLoser = 0;
      break;
    default:
      TraceLog(ERROR_LEVEL, "ProcessSetCurrentStatus\n");
      return ERROR;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessSet_UpdateStatus
- Description:
****************************************************************************/
int ProcessSet_UpdateStatus(char *message)
{
  t_DoubleConverter myDouble;
  
  memcpy(myDouble.c, &message[10], 8);
  
  int maxBPFlag;
  memcpy(&maxBPFlag, &message[18], 4);
  
  if (maxBPFlag == 1)
  {
    AlgorithmConfig.globalConfig.maxBuyingPower = myDouble.value;
    BuildSend_Global_Configuration();
    
    return SUCCESS;
  }
  
  if (TradingCounter <= 0)
  {
    TraceLog(DEBUG_LEVEL, "New Current BP: %lf (updated)\n", myDouble.value);
    TSCurrentStatus.buyingPower = (long)(myDouble.value * 100.0);
  }
  else
  {
    TraceLog(DEBUG_LEVEL, "New Current BP: %lf (pending)\n", myDouble.value);
    ReceivedBuyingPower = myDouble.value;
    IsUpdateBuyingPower = YES;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessSet_UpdateDisableSymbolList
- Description:
****************************************************************************/
int ProcessSet_UpdateDisableSymbolList(char *message)
{
  // Get Block length
  int blockLen = *(int*)(&message[6]);
  int symbolListLen = blockLen - 4;
  int countSymbol = symbolListLen / 8;
  
  int i = 0;
  int index = 6 + 4;
  char tempBuffer[SYMBOL_LEN];
  int stockSymbolIndex;
  
  for (stockSymbolIndex = 0; stockSymbolIndex < MAX_STOCK_SYMBOL; stockSymbolIndex++)
  {
    SymbolStatus[stockSymbolIndex].isDisable = NO;
    SymbolStatus[stockSymbolIndex].count = 0;
    SymbolStatus[stockSymbolIndex].symbolIsDisabledBy = SYMBOL_DISABLE_REASON_BY_NONE;
    int tempIndex;
    for (tempIndex = 0; tempIndex < MAX_OPEN_ORDERS; tempIndex++)
    {
      TradingCollection[stockSymbolIndex].openOrders[tempIndex].orderID = -1;
      TradingCollection[stockSymbolIndex].entryCounter = 0;
      TradingCollection[stockSymbolIndex].exitCounter = 0;
    }
  }
  
  for (i = 0; i < countSymbol; i++)
  {
    strncpy(tempBuffer, &message[index], SYMBOL_LEN);
    stockSymbolIndex = GetStockSymbolIndex(tempBuffer);
    
    if (stockSymbolIndex != -1)
    {
      SymbolStatus[stockSymbolIndex].isDisable = YES;
      SymbolStatus[stockSymbolIndex].symbolIsDisabledBy = SYMBOL_DISABLE_REASON_BY_SYMBOL_LIST;
    }
    
    index += SYMBOL_LEN;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessSet_UpdateDisableSymbolRangesList
- Description:
****************************************************************************/
int ProcessSet_UpdateDisableSymbolRangesList(char *message)
{
  
  // Get Block length
  int blockLen = *(int*)(&message[6]);
  int symbolListLen = blockLen - 4;
  int countSymbol = symbolListLen / 8;
  
  int i;
  int index = 6 + 4;
  char tempBuffer[SYMBOL_LEN];
  int stockSymbolIndex;
  char begin;
  char end;

  IgnoreStockRangesList.countStock = countSymbol;
  for (i = 0; i < countSymbol; i++) 
  {
    strncpy(tempBuffer, &message[index], SYMBOL_LEN);
    begin = tempBuffer[0];
    end = tempBuffer[2];
    strncpy(IgnoreStockRangesList.stockList[i],&message[index],SYMBOL_LEN);
    
    if ((end > 64) && (end < 91)) // Ranges have 3 character
    {
      for(stockSymbolIndex = 0; stockSymbolIndex < MAX_STOCK_SYMBOL; stockSymbolIndex ++)
      {
        if((SymbolMgmt.symbolList[stockSymbolIndex].symbol[0] <= end) && (SymbolMgmt.symbolList[stockSymbolIndex].symbol[0] >= begin))
        {
          
          if(SymbolStatus[stockSymbolIndex].symbolIsDisabledBy == SYMBOL_DISABLE_REASON_BY_NONE)
          {
            SymbolStatus[stockSymbolIndex].isDisable = YES;
            SymbolStatus[stockSymbolIndex].symbolIsDisabledBy = SYMBOL_DISABLE_REASON_BY_RANGES_LIST;
          }
          else if(SymbolStatus[stockSymbolIndex].symbolIsDisabledBy == SYMBOL_DISABLE_REASON_BY_SYMBOL_LIST)
          {
            SymbolStatus[stockSymbolIndex].symbolIsDisabledBy = SYMBOL_DISABLE_REASON_BY_ALL;
          }
          
        }
      }
    }
    else // Ranges only have 1 character
    {
      for(stockSymbolIndex = 0; stockSymbolIndex < MAX_STOCK_SYMBOL; stockSymbolIndex ++)
      {
        if(SymbolMgmt.symbolList[stockSymbolIndex].symbol[0] == begin)
        {
          if(SymbolStatus[stockSymbolIndex].symbolIsDisabledBy == SYMBOL_DISABLE_REASON_BY_NONE)
          {
            SymbolStatus[stockSymbolIndex].isDisable = YES;
            SymbolStatus[stockSymbolIndex].symbolIsDisabledBy = SYMBOL_DISABLE_REASON_BY_RANGES_LIST;
          }
          else if(SymbolStatus[stockSymbolIndex].symbolIsDisabledBy == SYMBOL_DISABLE_REASON_BY_SYMBOL_LIST)
          {
            SymbolStatus[stockSymbolIndex].symbolIsDisabledBy = SYMBOL_DISABLE_REASON_BY_ALL;
          }
        }
      }
    }

    index += SYMBOL_LEN;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessSet_UpdateDisableSymbol
- Description:
****************************************************************************/
int ProcessSet_UpdateDisableSymbol(char *message)
{
  // Get Block length
  int stockSymbolIndex;
  char tempBuffer[SYMBOL_LEN];
  int action = message[10];
  
  strncpy(tempBuffer, &message[11], SYMBOL_LEN);
  stockSymbolIndex = GetStockSymbolIndex(tempBuffer);
  
  if (stockSymbolIndex == -1)
  {
    return SUCCESS;
  }
  
  if (action == ADD_DISABLED_SYMBOL)
  {
    if(SymbolStatus[stockSymbolIndex].symbolIsDisabledBy == SYMBOL_DISABLE_REASON_BY_NONE)
    {
      SymbolStatus[stockSymbolIndex].isDisable = YES;
      SymbolStatus[stockSymbolIndex].symbolIsDisabledBy = SYMBOL_DISABLE_REASON_BY_SYMBOL_LIST;
    }
    else
    {
      SymbolStatus[stockSymbolIndex].symbolIsDisabledBy = SYMBOL_DISABLE_REASON_BY_ALL;
    }
  }
  else //Remove symbol
  {
    if(SymbolStatus[stockSymbolIndex].symbolIsDisabledBy == SYMBOL_DISABLE_REASON_BY_ALL)
    {
      SymbolStatus[stockSymbolIndex].symbolIsDisabledBy = SYMBOL_DISABLE_REASON_BY_RANGES_LIST;
    }
    else
    {
      // Clean Entry and Exit Collections
      int index;
      for (index = 0; index < MAX_OPEN_ORDERS; index++)
      {
        TradingCollection[stockSymbolIndex].openOrders[index].orderID = -1;
        TradingCollection[stockSymbolIndex].exitCounter = 0;
        TradingCollection[stockSymbolIndex].entryCounter = 0;
      }
    
      // Enable current stock symbol for trading
      SymbolStatus[stockSymbolIndex].isDisable = NO;
      SymbolStatus[stockSymbolIndex].symbolIsDisabledBy = SYMBOL_DISABLE_REASON_BY_NONE;
      TradingCollection[stockSymbolIndex].isTrading = NO;
    }
  }
  
  TraceLog(DEBUG_LEVEL, "action = %d\n", action);
  TraceLog(DEBUG_LEVEL, "symbol to be disabled/enabled = %.8s index = %d\n", tempBuffer, stockSymbolIndex);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessSet_UpdateDisableSymbolRanges
- Description:
****************************************************************************/
int ProcessSet_UpdateDisableSymbolRanges(char *message)
{ 
  int stockSymbolIndex;
  char tempBuffer[SYMBOL_LEN];
  int action = message[10];
  strncpy(tempBuffer, &message[11], SYMBOL_LEN);
  char begin = message[11];
  char end = message[13];
  
  // Get Block length
  int blockLen = *(int*)(&message[6]);
  int symbolListLen = blockLen - 13;
  int countSymbol = symbolListLen / 8;

  int index_list = 6 + 13;
  char symbolTempBuffer[SYMBOL_LEN];
  char stock_begin;
  char stock_end;
  int i;

  if (action == ADD_DISABLED_SYMBOL)
  {
    IgnoreStockRangesList.countStock = countSymbol;
    strncpy(IgnoreStockRangesList.stockList[countSymbol - 1],&message[11],SYMBOL_LEN);
    
    if ((end > 64) && (end < 91)) // Ranges have 3 character
    {
      for(stockSymbolIndex = 0; stockSymbolIndex < SymbolMgmt.nextSymbolIndex; stockSymbolIndex ++)
      {
        if((SymbolMgmt.symbolList[stockSymbolIndex].symbol[0] <= end) && (SymbolMgmt.symbolList[stockSymbolIndex].symbol[0] >= begin))
        {
          if(SymbolStatus[stockSymbolIndex].symbolIsDisabledBy == SYMBOL_DISABLE_REASON_BY_NONE)
          {
            SymbolStatus[stockSymbolIndex].isDisable = YES;
            SymbolStatus[stockSymbolIndex].symbolIsDisabledBy = SYMBOL_DISABLE_REASON_BY_RANGES_LIST;
          }
          else if(SymbolStatus[stockSymbolIndex].symbolIsDisabledBy == SYMBOL_DISABLE_REASON_BY_SYMBOL_LIST)
          {
            SymbolStatus[stockSymbolIndex].symbolIsDisabledBy = SYMBOL_DISABLE_REASON_BY_ALL;
          }
        }
      }
    }
    else // Ranges only have 1 character
    {
      for(stockSymbolIndex = 0; stockSymbolIndex < SymbolMgmt.nextSymbolIndex; stockSymbolIndex ++)
      {
        if(SymbolMgmt.symbolList[stockSymbolIndex].symbol[0] == begin)
        {
          if(SymbolStatus[stockSymbolIndex].symbolIsDisabledBy == SYMBOL_DISABLE_REASON_BY_NONE)
          {
            SymbolStatus[stockSymbolIndex].isDisable = YES;
            SymbolStatus[stockSymbolIndex].symbolIsDisabledBy = SYMBOL_DISABLE_REASON_BY_RANGES_LIST;
          }
          else if(SymbolStatus[stockSymbolIndex].symbolIsDisabledBy == SYMBOL_DISABLE_REASON_BY_SYMBOL_LIST)
          {
            SymbolStatus[stockSymbolIndex].symbolIsDisabledBy = SYMBOL_DISABLE_REASON_BY_ALL;
          }
        }
      }
    }
  }
  else //Remove ranges
  {
    if ((end > 64) && (end < 91)) // Ranges have 3 character
    {
      for(stockSymbolIndex = 0; stockSymbolIndex < SymbolMgmt.nextSymbolIndex; stockSymbolIndex ++)
      {
        if((SymbolMgmt.symbolList[stockSymbolIndex].symbol[0] <= end) && (SymbolMgmt.symbolList[stockSymbolIndex].symbol[0] >= begin))
        {
          if(SymbolStatus[stockSymbolIndex].symbolIsDisabledBy == SYMBOL_DISABLE_REASON_BY_ALL)
          {
            SymbolStatus[stockSymbolIndex].symbolIsDisabledBy = SYMBOL_DISABLE_REASON_BY_SYMBOL_LIST;
          }
          else if(SymbolStatus[stockSymbolIndex].symbolIsDisabledBy == SYMBOL_DISABLE_REASON_BY_RANGES_LIST)
          {
            // Clean Entry and Exit Collections
            int index;
            for (index = 0; index < MAX_OPEN_ORDERS; index++)
            {
              TradingCollection[stockSymbolIndex].openOrders[index].orderID = -1;
              TradingCollection[stockSymbolIndex].exitCounter = 0;
              TradingCollection[stockSymbolIndex].entryCounter = 0;
            }
            // Enable current stock symbol for trading
            SymbolStatus[stockSymbolIndex].isDisable = NO;
            SymbolStatus[stockSymbolIndex].symbolIsDisabledBy = SYMBOL_DISABLE_REASON_BY_NONE;
            TradingCollection[stockSymbolIndex].isTrading = NO;
          }
        }
      }
    }
    else //Ranges only have 1 character
    {
      for(stockSymbolIndex = 0; stockSymbolIndex < SymbolMgmt.nextSymbolIndex; stockSymbolIndex ++)
      {
        if(SymbolMgmt.symbolList[stockSymbolIndex].symbol[0] == begin)
        {
          if(SymbolStatus[stockSymbolIndex].symbolIsDisabledBy == SYMBOL_DISABLE_REASON_BY_ALL)
          {
            SymbolStatus[stockSymbolIndex].symbolIsDisabledBy = SYMBOL_DISABLE_REASON_BY_SYMBOL_LIST;
          }
          else if(SymbolStatus[stockSymbolIndex].symbolIsDisabledBy == SYMBOL_DISABLE_REASON_BY_RANGES_LIST)
          {
            // Clean Entry and Exit Collections
            int index;
            for (index = 0; index < MAX_OPEN_ORDERS; index++)
            {
              TradingCollection[stockSymbolIndex].openOrders[index].orderID = -1;
              TradingCollection[stockSymbolIndex].exitCounter = 0;
              TradingCollection[stockSymbolIndex].entryCounter = 0;
            }
            // Enable current stock symbol for trading
            SymbolStatus[stockSymbolIndex].isDisable = NO;
            SymbolStatus[stockSymbolIndex].symbolIsDisabledBy = SYMBOL_DISABLE_REASON_BY_NONE;
            TradingCollection[stockSymbolIndex].isTrading = NO;
          }
        }
      }
    }

    IgnoreStockRangesList.countStock = countSymbol;
    for (i = 0; i < countSymbol; i++) //Update ranges list after add or remove a range
    {
      strncpy(IgnoreStockRangesList.stockList[i],&message[index_list],SYMBOL_LEN);
      
      strncpy(symbolTempBuffer, &message[index_list], SYMBOL_LEN);
      stock_begin = symbolTempBuffer[0];
      stock_end = symbolTempBuffer[2];
      
      if ((stock_end > 64) && (stock_end < 91))// ranges have 3 character
      {
        for(stockSymbolIndex = 0; stockSymbolIndex < SymbolMgmt.nextSymbolIndex; stockSymbolIndex ++)
        {
          if((SymbolMgmt.symbolList[stockSymbolIndex].symbol[0] <= stock_end) && (SymbolMgmt.symbolList[stockSymbolIndex].symbol[0] >= stock_begin))
          {
            if(SymbolStatus[stockSymbolIndex].symbolIsDisabledBy == SYMBOL_DISABLE_REASON_BY_NONE)
            {
              SymbolStatus[stockSymbolIndex].isDisable = YES;
              SymbolStatus[stockSymbolIndex].symbolIsDisabledBy = SYMBOL_DISABLE_REASON_BY_RANGES_LIST;
            }
            else if(SymbolStatus[stockSymbolIndex].symbolIsDisabledBy == SYMBOL_DISABLE_REASON_BY_SYMBOL_LIST)
            {
              SymbolStatus[stockSymbolIndex].symbolIsDisabledBy = SYMBOL_DISABLE_REASON_BY_ALL;
            }
          }
        }
      }
      else // ranges only have 1 ranges
      {
        for(stockSymbolIndex = 0; stockSymbolIndex < MAX_STOCK_SYMBOL; stockSymbolIndex ++)
        {
          if(SymbolMgmt.symbolList[stockSymbolIndex].symbol[0] == stock_begin)
          {
            if(SymbolStatus[stockSymbolIndex].symbolIsDisabledBy == SYMBOL_DISABLE_REASON_BY_NONE)
            {
              SymbolStatus[stockSymbolIndex].isDisable = YES;
              SymbolStatus[stockSymbolIndex].symbolIsDisabledBy = SYMBOL_DISABLE_REASON_BY_RANGES_LIST;
            }
            else if(SymbolStatus[stockSymbolIndex].symbolIsDisabledBy == SYMBOL_DISABLE_REASON_BY_SYMBOL_LIST)
            {
              SymbolStatus[stockSymbolIndex].symbolIsDisabledBy = SYMBOL_DISABLE_REASON_BY_ALL;
            }
          }
        }
      }
      index_list+= SYMBOL_LEN;
    }
  }
  
  TraceLog(DEBUG_LEVEL, "action = %d\n", action);
  TraceLog(DEBUG_LEVEL, "symbol ranges to be disabled/enabled = %.8s \n", tempBuffer);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ReceiveMessage
- Description:
****************************************************************************/
int ReceiveMessage(void *configInfo, void *message)
{
  t_ASConfig *config = (t_ASConfig *)configInfo;
  
  t_Message *clientMsg = (t_Message *)message;
  
  // Timeout counter
  int timeoutCounter = 0;
  
  // Current messsae body length
  int currentMsgBodyLen = 0;
  
  // Initialize message
  clientMsg->msgLen = 0;
  
  /*
  Try to receive message header
  
  If cannot receive message header when reached timeout, return and report error
  
  otherwise calculate message body
  
  If message body has length 0, return Success
  
  Otherwise try to receive message body
  
  If cannot receive message body when reached timeout, return and report error
  
  Otherwise return Success
   */
  
  // Try to receive message header
  while (clientMsg->msgLen < MSG_HEADER_LEN)
  {
    int receivedBytes = recv(config->socket, &clientMsg->msgContent[clientMsg->msgLen], MSG_HEADER_LEN - clientMsg->msgLen, 0);
    
    if (receivedBytes > 0)
    {
      timeoutCounter = 0;
      
      clientMsg->msgLen += receivedBytes;
    }
    else
    {
      if ((receivedBytes == 0) && (errno == 0))
      {
        TraceLog(DEBUG_LEVEL, "AS: Socket is close\n");
        return ERROR;
      }
      else
      {
        if (timeoutCounter < 3)
        {
          timeoutCounter += 1;          
          TraceLog(DEBUG_LEVEL, "AS: Waiting. Sleep num = %d\n", timeoutCounter);
        }
        else
        {
          TraceLog(DEBUG_LEVEL, "AS: TIME OUT!\n");
          return ERROR;
        }
      }
    }
  }
  
  // Get message body length
  currentMsgBodyLen = *(int*)(&clientMsg->msgContent[MSG_BODY_LEN_INDEX]);
  
  if (currentMsgBodyLen == 0)
  {
    return SUCCESS;
  }
  
  // Try to receive message body
  while (clientMsg->msgLen < currentMsgBodyLen + MSG_HEADER_LEN)
  {
    int receivedBytes = recv(config->socket, &clientMsg->msgContent[clientMsg->msgLen], currentMsgBodyLen - (clientMsg->msgLen - MSG_HEADER_LEN), 0);

    if (receivedBytes > 0)
    {
      timeoutCounter = 0;
      clientMsg->msgLen += receivedBytes;
    }
    else
    {
      if ((receivedBytes == 0) && (errno == 0))
      {
        TraceLog(DEBUG_LEVEL, "AS: Socket is close\n");
        return ERROR;
      }
      else
      {
        if (timeoutCounter < 3)
        {
          timeoutCounter += 1;          
          TraceLog(DEBUG_LEVEL, "AS: Waiting. Sleep num = %d\n", timeoutCounter);
        }
        else
        {
          TraceLog(DEBUG_LEVEL, "AS: TIME OUT!\n");
          return ERROR;
        }
      }
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ParseGetTradingInformation
- Description:
****************************************************************************/
int ParseGetTradingInformation(void *message)
{
  char *workingMessage = (char *)message;
  /* 
  Message format of the message type
  + Message Type
  + Body Length
  + Block Length
  + ECN Order ID
  + Count raw data blocks
  + ... (more block here base on number of ECN Orders)
  */
  
  // Message type
  
  // Body length
  int bodyLength = *(int*)(&workingMessage[2]);
  
  // Get information of per block
  int blockIndex;
  int index = 6;
  
  for (blockIndex = 0; blockIndex < bodyLength / 9; blockIndex++)
  {
    //TraceLog(DEBUG_LEVEL, "blockIndex = %d\n", blockIndex);
    //TraceLog(DEBUG_LEVEL, "bodyLength = %d\n", bodyLength);
    
    // Block length
    // GetIntNumber(&workingMessage[index]);
    index += 4;
    
    // ECN order id
    int ecnOrderId = workingMessage[index];
    index += 1;

    // Count raw data blocks
    int countRawDataBlocks = *(int*)(&workingMessage[index]);
    index += 4;
    
    /*
    TraceLog(DEBUG_LEVEL, "Block Information: %d\n", blockIndex);
    TraceLog(DEBUG_LEVEL, "Block Length: %d\n", blockLength);
    TraceLog(DEBUG_LEVEL, "ECN Order ID: %d\n", ecnOrderId);
    TraceLog(DEBUG_LEVEL, "Count raw data blocks: %d\n\n", countRawDataBlocks);
    */
    
    // Update OrderMgmt
    OrderMgmt[ecnOrderId].lastSentIndex = countRawDataBlocks;
    
    TraceLog(DEBUG_LEVEL, "From AS: OrderMgmt[%d].lastSentIndex = %d\n", ecnOrderId, OrderMgmt[ecnOrderId].lastSentIndex);
    if ((OrderMgmt[ecnOrderId].lastSentIndex + 1) > OrderMgmt[ecnOrderId].countRawDataBlock)
    {
      TraceLog(DEBUG_LEVEL, "Resynchronize rawdata sent-index between AS and TS, ECN ID: %d, index from %d to %d\n", OrderMgmt[ecnOrderId].id, OrderMgmt[ecnOrderId].lastSentIndex, OrderMgmt[ecnOrderId].countRawDataBlock - 1);
      OrderMgmt[ecnOrderId].lastSentIndex = OrderMgmt[ecnOrderId].countRawDataBlock - 1;
    }
  }
  
  return 0;
}


/****************************************************************************
- Function name:  ParseGetTradeOutputLog
- Description:
****************************************************************************/
int ParseGetTradeOutputLog(void *message)
{
  char *workingMessage = (char *)message;
  int countSentLogEntry = *(int*)(&workingMessage[10]);
  
  TraceLog(DEBUG_LEVEL, "Trade Output Log received-information from AS: TS sent %d Trade Output Log entry to AS\n", countSentLogEntry);
  TradeOutputLogMgmt.countSentLogEntry = countSentLogEntry;
  
  if (TradeOutputLogMgmt.countSentLogEntry > TradeOutputLogMgmt.countLogEntry)
  {
    TraceLog(DEBUG_LEVEL, "Resynchronize Trade Output Log sent-counter between AS and TS, reset counter from %d to %d\n", TradeOutputLogMgmt.countSentLogEntry, TradeOutputLogMgmt.countLogEntry);
    TradeOutputLogMgmt.countSentLogEntry = TradeOutputLogMgmt.countLogEntry;
  }
  
  return 0;
}

/****************************************************************************
- Function name:  ParseGetBBOInfo
****************************************************************************/
int ParseGetBBOInfo(void *message)
{
  char *workingMessage = (char *)message;
  int countSentBBOEntry = *(int*)(&workingMessage[10]);
  
  TraceLog(DEBUG_LEVEL, "BBO received-information from AS: TS sent %d BBO entries to AS\n", countSentBBOEntry);
  BBOInfoMgmt.countSentBBOEntry = countSentBBOEntry;
  
  if (BBOInfoMgmt.countSentBBOEntry > BBOInfoMgmt.countBBOEntry)
  {
    TraceLog(DEBUG_LEVEL, "Resynchronize BBO sent-counter between AS and TS, reset counter from %d to %d\n", BBOInfoMgmt.countSentBBOEntry, BBOInfoMgmt.countBBOEntry);
    BBOInfoMgmt.countSentBBOEntry = BBOInfoMgmt.countBBOEntry;
  }
  
  return 0;
}

/****************************************************************************
- Function name:  SendBookConfiguration
- Description:
****************************************************************************/
int SendBookConfiguration(void)
{
  // Send ARCA Book configuration info
  BuildSend_ARCA_Book_Configuration();
  
  BuildSend_NASDAQ_MDC_Configuration(BOOK_NASDAQ, (void*)&NASDAQ_Book_Conf);
  
  BuildSend_NASDAQ_MDC_Configuration(BOOK_NDBX, (void*)&NDBX_Book_Conf);
  
  BuildSend_NASDAQ_MDC_Configuration(BOOK_PSX, (void*)&PSX_Book_Conf);
  
  // Send BATS-Z Book configuration info
  BuildSend_BATSZ_Book_Configuration();
  
  // Send BYX Book configuration info
  BuildSend_BYX_Book_Configuration();

  // Send OPEN Book Ultra Configuration info
  BuildSend_NYSE_Book_Configuration();
  
  // Send EDGX Book Configuration info
  BuildSend_EDGX_Book_Configuration();
  
  // Send EDGA Book Configuration info
  BuildSend_EDGA_Book_Configuration();
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  BuildSend_NASDAQ_MDC_Configuration
****************************************************************************/
int BuildSend_NASDAQ_MDC_Configuration(int ecnIndex, void* _config)
{
  t_NASDAQ_Book_Conf *config = (t_NASDAQ_Book_Conf *) _config;
  char msgToSend[2 + 4 + 4 + 1 + 80 + 4];
  memset(msgToSend, 0, 2 + 4 + 4 + 1 + 80 + 4);
  t_ShortConverter shortNum;
  t_IntConverter intNum;
  int index = 0;
  
  // Message Type
  shortNum.value = TS_RES_BOOK_CONFIG;
  memcpy(&msgToSend[index], shortNum.c, 2);
  index += 2;
  
  // Body Length
  intNum.value = 4 + 1 + 80 + 4;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // Block Length
  intNum.value = 4 + 1 + 80 + 4;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // ECN Book ID
  msgToSend[index] = ecnIndex;
  index += 1;
  
  // UDP multicast info
  // IP Address
  memcpy(&msgToSend[index], config->Data.ip, 80);
  index += 80;
  
  // Port
  intNum.value = config->Data.port;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // Send message to AS
  return SendMessageToAS(msgToSend, index);
}

/****************************************************************************
- Function name:  BuildSend_BATSZ_Book_Configuration
- Description:
****************************************************************************/
int BuildSend_BATSZ_Book_Configuration(void)
{
  
  char msgToSend[2 + 4 + 4 + 1 + 1 + MAX_LINE_LEN+ MAX_LINE_LEN + 4 + MAX_LINE_LEN + MAX_LINE_LEN + MAX_LINE_LEN*MAX_BATSZ_UNIT*2 + 4*MAX_BATSZ_UNIT*2];
  memset(msgToSend, 0, 2 + 4 + 4 + 1 + 1 + MAX_LINE_LEN + MAX_LINE_LEN + 4 + MAX_LINE_LEN + MAX_LINE_LEN + MAX_LINE_LEN *MAX_BATSZ_UNIT*2 + 4*MAX_BATSZ_UNIT*2);

  t_ShortConverter shortNum;
  t_IntConverter intNum;
  int index = 0;
  
  // Message Type
  shortNum.value = TS_RES_BOOK_CONFIG;
  memcpy(&msgToSend[index], shortNum.c, 2);
  index += 2;
  
  // Body Length
  intNum.value = 4 + 1 + 1 + MAX_LINE_LEN + MAX_LINE_LEN + 4 + MAX_LINE_LEN + MAX_LINE_LEN + MAX_LINE_LEN*MAX_BATSZ_UNIT*2 + 4*MAX_BATSZ_UNIT*2;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // Block Length
  intNum.value = 4 + 1 + 1 + MAX_LINE_LEN + MAX_LINE_LEN + 4 + MAX_LINE_LEN + MAX_LINE_LEN + MAX_LINE_LEN*MAX_BATSZ_UNIT*2 + 4*MAX_BATSZ_UNIT*2;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // ECN Book ID
  msgToSend[index] = BOOK_BATSZ;
  index += 1;
  
  //Details
  //- Number of units
  //- Username [32]
  //- Password [32]
  //- IPs [32xn]
  //- Ports [4xn]
  
  // Number of units
  msgToSend[index] = MAX_BATSZ_UNIT;
  index += 1;
  
  //SessionSubId
  index += MAX_LINE_LEN;
  
  // Username
  index += MAX_LINE_LEN;
  
  // Password
  index += MAX_LINE_LEN;
  
  //GAP IP
  strncpy(&msgToSend[index], "0.0.0.0", MAX_LINE_LEN);
  index += MAX_LINE_LEN;
  
  //GAP Port
  intNum.value = 0; //Retransmission feed Port
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
    
  int i;
  for (i=0; i<MAX_BATSZ_UNIT; i++)
  {
    memcpy(&msgToSend[index], BATSZ_Book_Conf.group[i].dataIP, MAX_LINE_LEN); //Data feed IP
    index += MAX_LINE_LEN;
    
    intNum.value = BATSZ_Book_Conf.group[i].dataPort; //Data feed Port
    memcpy(&msgToSend[index], intNum.c, 4);
    index += 4;
    
    memcpy(&msgToSend[index], "0.0.0.0", MAX_LINE_LEN); //Retransmission feed IP
    index += MAX_LINE_LEN;
    
    intNum.value = 0; //Retransmission feed Port
    memcpy(&msgToSend[index], intNum.c, 4);
    index += 4;
  }

  // Send message to AS
  return SendMessageToAS(msgToSend, index);
}

/****************************************************************************
- Function name:  BuildSend_BYX_Book_Configuration
****************************************************************************/
int BuildSend_BYX_Book_Configuration(void)
{
  char msgToSend[2 + 4 + 4 + 1 + 1 + MAX_LINE_LEN+ MAX_LINE_LEN + 4 + MAX_LINE_LEN + MAX_LINE_LEN + MAX_LINE_LEN*MAX_BYX_UNIT*2 + 4*MAX_BYX_UNIT*2];
  memset(msgToSend, 0, 2 + 4 + 4 + 1 + 1 + MAX_LINE_LEN + MAX_LINE_LEN + 4 + MAX_LINE_LEN + MAX_LINE_LEN + MAX_LINE_LEN *MAX_BYX_UNIT*2 + 4*MAX_BYX_UNIT*2);

  t_ShortConverter shortNum;
  t_IntConverter intNum;
  int index = 0;
  
  // Message Type
  shortNum.value = TS_RES_BOOK_CONFIG;
  memcpy(&msgToSend[index], shortNum.c, 2);
  index += 2;
  
  // Body Length
  intNum.value = 4 + 1 + 1 + MAX_LINE_LEN + MAX_LINE_LEN + 4 + MAX_LINE_LEN + MAX_LINE_LEN + MAX_LINE_LEN*MAX_BYX_UNIT*2 + 4*MAX_BYX_UNIT*2;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // Block Length
  intNum.value = 4 + 1 + 1 + MAX_LINE_LEN + MAX_LINE_LEN + 4 + MAX_LINE_LEN + MAX_LINE_LEN + MAX_LINE_LEN*MAX_BYX_UNIT*2 + 4*MAX_BYX_UNIT*2;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // ECN Book ID
  msgToSend[index] = BOOK_BYX;
  index += 1;
  
  //Details
  //- Number of units
  //- Username [32]
  //- Password [32]
  //- IPs [32xn]
  //- Ports [4xn]
  
  // Number of units
  msgToSend[index] = MAX_BYX_UNIT;
  index += 1;
  
  //SessionSubId
  index += MAX_LINE_LEN;
  
  // Username
  index += MAX_LINE_LEN;
  
  // Password
  index += MAX_LINE_LEN;
  
  //GAP IP
  strncpy(&msgToSend[index], "0.0.0.0", MAX_LINE_LEN);
  index += MAX_LINE_LEN;
  
  //GAP Port
  intNum.value = 0; //Retransmission feed Port
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
    
  int i;
  for (i=0; i<MAX_BYX_UNIT; i++)
  {
    memcpy(&msgToSend[index], BYX_Book_Conf.group[i].dataIP, MAX_LINE_LEN); //Data feed IP
    index += MAX_LINE_LEN;
    
    intNum.value = BYX_Book_Conf.group[i].dataPort; //Data feed Port
    memcpy(&msgToSend[index], intNum.c, 4);
    index += 4;
    
    memcpy(&msgToSend[index], "0.0.0.0", MAX_LINE_LEN); //Retransmission feed IP
    index += MAX_LINE_LEN;
    
    intNum.value = 0; //Retransmission feed Port
    memcpy(&msgToSend[index], intNum.c, 4);
    index += 4;
  }

  // Send message to AS
  return SendMessageToAS(msgToSend, index);
}

/****************************************************************************
- Function name:  BuildSend_ARCA_Book_Configuration
- Description:
****************************************************************************/
int BuildSend_ARCA_Book_Configuration(void)
{
  char msgToSend[2 + 4 + 4 + 1 + 1 + 80 + MAX_ARCA_GROUPS * (1 + 80 + 4)];
  memset(msgToSend, 0, 2 + 4 + 4 + 1 + 1 + 80 + MAX_ARCA_GROUPS * (1 + 80 + 4));
  t_ShortConverter shortNum;
  t_IntConverter intNum;
  int index = 0;
  
  // Message Type
  shortNum.value = TS_RES_BOOK_CONFIG;
  memcpy(&msgToSend[index], shortNum.c, 2);
  index += 2;
  
  // Body Length
  intNum.value = 4 + 1 + 1 + 80 + MAX_ARCA_GROUPS * (1 + 80 + 4);
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // Block Length
  intNum.value = 4 + 1 + 1 + 80 + MAX_ARCA_GROUPS * (1 + 80 + 4);
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // ECN Book ID
  msgToSend[index] = BOOK_ARCA;
  index += 1;
  
  // Current Feed (A or B)
  msgToSend[index] = ARCA_Book_Conf.currentFeedName;
  index += 1;
  
  // SourceID
  strncpy(&msgToSend[index], "UNUSED", 80);
  index += 80;
  
  /*
  Details
  - SourceID
  */

  int groupIndex;
  for (groupIndex = 0; groupIndex < MAX_ARCA_GROUPS; groupIndex ++)
  {
    // Retransmission Request TCP/IP
    // Index
    msgToSend[index] = groupIndex;
    index += 1;

    // Recovery IP Address
    strncpy(&msgToSend[index], "0.0.0.0", MAX_LINE_LEN);
    index += MAX_LINE_LEN;
    
    // Recovery Port
    memset(&msgToSend[index], 0, 4);
    index += 4;
  }
  
  // Send message to AS
  return SendMessageToAS(msgToSend, index);
}

/****************************************************************************
- Function name:  BuildSend_NYSE_Book_Configuration
- Description:
****************************************************************************/
int BuildSend_NYSE_Book_Configuration(void)
{
  int msgSize = 2 + 4 + 4 + 1 + MAX_LINE_LEN + MAX_NYSE_BOOK_GROUPS * (1 + MAX_LINE_LEN + 4);
  char msgToSend[msgSize];
  memset(msgToSend, 0, msgSize);
  t_ShortConverter shortNum;
  t_IntConverter intNum;
  int index = 0;

  // Message Type
  shortNum.value = TS_RES_BOOK_CONFIG;
  memcpy(&msgToSend[index], shortNum.c, 2);
  index += 2;

  // Body Length
  intNum.value = msgSize - 6;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;

  // Block Length
  intNum.value = msgSize - 6;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;

  // ECN Book ID
  msgToSend[index] = BOOK_NYSE;
  index += 1;

  // Retransmission Request Source
  index += MAX_LINE_LEN;

  unsigned char groupIndex;
  for (groupIndex = 0; groupIndex < MAX_NYSE_BOOK_GROUPS; groupIndex++)
  {
    // Index
    msgToSend[index] = groupIndex;
    index += 1;

    /*******Retransmission Request TCP Server*******/
    strncpy(&msgToSend[index], "0.0.0.0", MAX_LINE_LEN);
    index += MAX_LINE_LEN;
    
    //port
    memset(&msgToSend[index], 0, 4);
    index += 4;
  }

  // Send message to AS
  return SendMessageToAS(msgToSend, index);
}

/****************************************************************************
- Function name:  BuildSend_EDGX_Book_Configuration
- Description:
****************************************************************************/
int BuildSend_EDGX_Book_Configuration(void)
{
  const int blockSize = 4 + 1 + 4 + 1 + MAX_LINE_LEN*3 + MAX_EDGX_BOOK_GROUP * (2 * MAX_LINE_LEN + 4 + 4);
  const int msgSize = MSG_HEADER_LEN + blockSize;
  char msgToSend[msgSize];
  memset(msgToSend, 0, msgSize);

  t_ShortConverter shortNum;
  t_IntConverter intNum;
  int index = 0;

  // Message Type
  shortNum.value = TS_RES_BOOK_CONFIG;
  memcpy(&msgToSend[index], shortNum.c, 2);
  index += 2;

  // Body Length: sum of blocks' size
  intNum.value = blockSize;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;

  // Block Length
  intNum.value = blockSize;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;

  // ECN Book ID
  msgToSend[index++] = BOOK_EDGX;
  
  // Retransmission Request Server Information
  strncpy(&msgToSend[index], "0.0.0.0", MAX_LINE_LEN);
  index += MAX_LINE_LEN;
  
  // Retransmission Request Port
  memset(&msgToSend[index], 0, 4);
  index += 4;
  
  // Username
  strncpy(&msgToSend[index], "UNUSED", MAX_LINE_LEN);
  index += MAX_LINE_LEN;
  
  // Password
  strncpy(&msgToSend[index], "UNUSED", MAX_LINE_LEN);
  index += MAX_LINE_LEN;
  
  // Number of groups
  msgToSend[index++] = MAX_EDGX_BOOK_GROUP;

  int i;
  for (i = 0; i < MAX_EDGX_BOOK_GROUP; i++)
  {
    //Multicast Data feed IP
    memcpy(&msgToSend[index], EDGX_BOOK_Conf.group[i].dataIP, MAX_LINE_LEN);
    index += MAX_LINE_LEN;

    //Multicast Data feed Port
    intNum.value = EDGX_BOOK_Conf.group[i].dataPort;
    memcpy(&msgToSend[index], intNum.c, 4);
    index += 4;

    //Multicast Retransmission IP
    strncpy(&msgToSend[index], "0.0.0.0", MAX_LINE_LEN);
    index += MAX_LINE_LEN;

    //Multicast Retransmission Port
    intNum.value = 0;
    memcpy(&msgToSend[index], intNum.c, 4);
    index += 4;
  }

  //TraceLog(DEBUG_LEVEL, "Sent EDGX BOOK configuration to AS\n");
  
  // Send message to AS
  return SendMessageToAS(msgToSend, index);
}

/****************************************************************************
- Function name:  BuildSend_EDGA_Book_Configuration
****************************************************************************/
int BuildSend_EDGA_Book_Configuration(void)
{
  const int blockSize = 4 + 1 + 4 + 1 + MAX_LINE_LEN*3 + MAX_EDGA_BOOK_GROUP * (2 * MAX_LINE_LEN + 4 + 4);
  const int msgSize = MSG_HEADER_LEN + blockSize;
  char msgToSend[msgSize];
  memset(msgToSend, 0, msgSize);

  t_ShortConverter shortNum;
  t_IntConverter intNum;
  int index = 0;

  // Message Type
  shortNum.value = TS_RES_BOOK_CONFIG;
  memcpy(&msgToSend[index], shortNum.c, 2);
  index += 2;

  // Body Length: sum of blocks' size
  intNum.value = blockSize;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;

  // Block Length
  intNum.value = blockSize;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;

  // ECN Book ID
  msgToSend[index++] = BOOK_EDGA;
  
  // Retransmission Request Server Information
  strncpy(&msgToSend[index], "0.0.0.0", MAX_LINE_LEN);
  index += MAX_LINE_LEN;
  
  // Retransmission Request Port
  memset(&msgToSend[index], 0, 4);
  index += 4;
  
  // Username
  strncpy(&msgToSend[index], "UNUSED", MAX_LINE_LEN);
  index += MAX_LINE_LEN;
  
  // Password
  strncpy(&msgToSend[index], "UNUSED", MAX_LINE_LEN);
  index += MAX_LINE_LEN;
  
  // Number of groups
  msgToSend[index++] = MAX_EDGA_BOOK_GROUP;

  int i;
  for (i = 0; i < MAX_EDGA_BOOK_GROUP; i++)
  {
    //Multicast Data feed IP
    memcpy(&msgToSend[index], EDGA_BOOK_Conf.group[i].dataIP, MAX_LINE_LEN);
    index += MAX_LINE_LEN;

    //Multicast Data feed Port
    intNum.value = EDGA_BOOK_Conf.group[i].dataPort;
    memcpy(&msgToSend[index], intNum.c, 4);
    index += 4;

    //Multicast Retransmission IP
    strncpy(&msgToSend[index], "0.0.0.0", MAX_LINE_LEN);
    index += MAX_LINE_LEN;

    //Multicast Retransmission Port
    intNum.value = 0;
    memcpy(&msgToSend[index], intNum.c, 4);
    index += 4;
  }

  //TraceLog(DEBUG_LEVEL, "Sent EDGA BOOK configuration to AS\n");
  
  // Send message to AS
  return SendMessageToAS(msgToSend, index);
}

/****************************************************************************
- Function name:  SendOrderConfiguration
- Description:
****************************************************************************/
int SendOrderConfiguration(void)
{
  // Send ARCA DIRECT configuration info
  BuildSend_ARCA_DIRECT_Configuration();
  
  // Send NYSE CCG configuration info
  BuildSend_NYSE_CCG_Configuration();
  
  // Send NASDAQ OUCH configurtion info
  BuildSend_NASDAQ_OUCH_Configuration();
  
  // Send NASDAQ RASH configurtion info
  BuildSend_NASDAQ_RASH_Configuration();
  
  // Send BATSZ BOE configurtion info
  BuildSend_BATSZ_BOE_Configuration();
  
  // Send BYX BOE configurtion info
  BuildSend_BYX_BOE_Configuration();
  
  // Send EDGX configurtion info
  BuildSend_EDGX_Configuration();
  
  // Send EDGA configurtion info
  BuildSend_EDGA_Configuration();
  
  // Send NASDAQ BX configurtion info
  BuildSend_NASDAQ_BX_Configuration(ORDER_NASDAQ_BX);
  BuildSend_NASDAQ_BX_Configuration(ORDER_NASDAQ_PSX);
  return SUCCESS;
}

/****************************************************************************
- Function name:  BuildSend_NYSE_CCG_Configuration
- Description:
****************************************************************************/
int BuildSend_NYSE_CCG_Configuration(void)
{
  char msgToSend[2 + 4 + 4 + 1 + 80 + 4 + 80 + 80];
  t_ShortConverter shortNum;
  t_IntConverter intNum;
  int index = 0;
  
  // Message Type
  shortNum.value = TS_RES_ORDER_CONFIG;
  memcpy(&msgToSend[index], shortNum.c, 2);
  index += 2;
  
  // Body Length
  intNum.value = 4 + 1 + 80 + 4 + 80 + 80;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // Block Length
  intNum.value = 1 + 80 + 4 + 80 + 80;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // ECN ID
  msgToSend[index] = ORDER_NYSE_CCG;
  index += 1;
  
  /*
  Details
  - IP Address
  - Port
  - Username
  - Password
  */
  
  // IP Address
  strncpy(&msgToSend[index], OecConfig[ORDER_NYSE_CCG].ipAddress, 80);
  index += 80;
  
  // Port
  intNum.value = OecConfig[ORDER_NYSE_CCG].port;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // Username
  strncpy(&msgToSend[index], OecConfig[ORDER_NYSE_CCG].userName, 80);
  index += 80;
  
  // Password
  strncpy(&msgToSend[index], OecConfig[ORDER_NYSE_CCG].password, 80);
  index += 80;
  
  // Send Message to AS
  return SendMessageToAS(msgToSend, index);
}

/****************************************************************************
- Function name:  BuildSend_ARCA_DIRECT_Configuration
- Description:
****************************************************************************/
int BuildSend_ARCA_DIRECT_Configuration(void)
{
  char msgToSend[2 + 4 + 4 + 1 + 80 + 4 + 80 + 80];
  t_ShortConverter shortNum;
  t_IntConverter intNum;
  int index = 0;
  
  // Message Type
  shortNum.value = TS_RES_ORDER_CONFIG;
  memcpy(&msgToSend[index], shortNum.c, 2);
  index += 2;
  
  // Body Length
  intNum.value = 4 + 1 + 80 + 4 + 80 + 80;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // Block Length
  intNum.value = 1 + 80 + 4 + 80 + 80;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // ECN ID
  msgToSend[index] = ORDER_ARCA_DIRECT;
  index += 1;
  
  /*
  Details
  - IP Address
  - Port
  - Username
  - Password
  */
  
  // IP Address
  strncpy(&msgToSend[index], OecConfig[ORDER_ARCA_DIRECT].ipAddress, 80);
  index += 80;
  
  // Port
  intNum.value = OecConfig[ORDER_ARCA_DIRECT].port;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // Username
  strncpy(&msgToSend[index], OecConfig[ORDER_ARCA_DIRECT].userName, 80);
  index += 80;
  
  // Password
  strncpy(&msgToSend[index], OecConfig[ORDER_ARCA_DIRECT].password, 80);
  index += 80;
  
  // Send Message to AS
  return SendMessageToAS(msgToSend, index);
}

/****************************************************************************
- Function name:  BuildSend_NASDAQ_OUCH_Configuration
- Description:
****************************************************************************/
int BuildSend_NASDAQ_OUCH_Configuration(void)
{
  char msgToSend[2 + 4 + 4 + 1 + 80 + 4 + 80 + 80];
  t_ShortConverter shortNum;
  t_IntConverter intNum;
  int index = 0;
  
  // Message Type
  shortNum.value = TS_RES_ORDER_CONFIG;
  memcpy(&msgToSend[index], shortNum.c, 2);
  index += 2;
  
  // Body Length
  intNum.value = 4 + 1 + 80 + 4 + 80 + 80;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // Block Length
  intNum.value = 1 + 80 + 4 + 80 + 80;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // ECN ID
  msgToSend[index] = ORDER_NASDAQ_OUCH;
  index += 1;
  
  /*
  Details
  - IP Address
  - Port
  - Username
  - Password
  */
  
  // IP Address
  strncpy(&msgToSend[index], OecConfig[ORDER_NASDAQ_OUCH].ipAddress, 80);
  index += 80;
  
  // Port
  intNum.value = OecConfig[ORDER_NASDAQ_OUCH].port;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // Username
  strncpy(&msgToSend[index], OecConfig[ORDER_NASDAQ_OUCH].userName, 80);
  index += 80;
  
  // Password
  strncpy(&msgToSend[index], OecConfig[ORDER_NASDAQ_OUCH].password, 80);
  index += 80;
  
  // Send Message to AS
  return SendMessageToAS(msgToSend, index);
}

int BuildSend_NASDAQ_BX_Configuration(int oecType)
{
  char msgToSend[2 + 4 + 4 + 1 + 80 + 4 + 80 + 80];
  t_ShortConverter shortNum;
  t_IntConverter intNum;
  int index = 0;
  
  // Message Type
  shortNum.value = TS_RES_ORDER_CONFIG;
  memcpy(&msgToSend[index], shortNum.c, 2);
  index += 2;
  
  // Body Length
  intNum.value = 4 + 1 + 80 + 4 + 80 + 80;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // Block Length
  intNum.value = 1 + 80 + 4 + 80 + 80;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // ECN ID
  msgToSend[index] = oecType;
  index += 1;
  
  /*
  Details
  - IP Address
  - Port
  - Username
  - Password
  */
  
  // IP Address
  strncpy(&msgToSend[index], OecConfig[oecType].ipAddress, 80);
  index += 80;
  
  // Port
  intNum.value = OecConfig[oecType].port;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // Username
  strncpy(&msgToSend[index], OecConfig[oecType].userName, 80);
  index += 80;
  
  // Password
  strncpy(&msgToSend[index], OecConfig[oecType].password, 80);
  index += 80;
  
  // Send Message to AS
  return SendMessageToAS(msgToSend, index);
}

/****************************************************************************
- Function name:  BuildSend_NASDAQ_RASH_Configuration
- Description:
****************************************************************************/
int BuildSend_NASDAQ_RASH_Configuration(void)
{
  char msgToSend[2 + 4 + 4 + 1 + 80 + 4 + 80 + 80];
  t_ShortConverter shortNum;
  t_IntConverter intNum;
  int index = 0;
  
  // Message Type
  shortNum.value = TS_RES_ORDER_CONFIG;
  memcpy(&msgToSend[index], shortNum.c, 2);
  index += 2;
  
  // Body Length
  intNum.value = 4 + 1+ 80 + 4 + 80 + 80;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // Block Length
  intNum.value = 1 + 80 + 4 + 80 + 80;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // ECN ID
  msgToSend[index] = ORDER_NASDAQ_RASH;
  index += 1;
  
  /*
  Details
  - IP Address
  - Port
  - Username
  - Password
  */
  
  // IP Address
  strncpy(&msgToSend[index], OecConfig[ORDER_NASDAQ_RASH].ipAddress, 80);
  index += 80;
  
  // Port
  intNum.value = OecConfig[ORDER_NASDAQ_RASH].port;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // Username
  strncpy(&msgToSend[index], OecConfig[ORDER_NASDAQ_RASH].userName, 80);
  index += 80;
  
  // Password
  strncpy(&msgToSend[index], OecConfig[ORDER_NASDAQ_RASH].password, 80);
  index += 80;
  
  // Send Message to AS
  return SendMessageToAS(msgToSend, index);
}

/****************************************************************************
- Function name:  BuildSend_BATSZ_BOE_Configuration
- Description:
****************************************************************************/
int BuildSend_BATSZ_BOE_Configuration(void)
{
  char msgToSend[2 + 4 + 4 + 1 + 80 + 4 + 80 + 80];
  t_ShortConverter shortNum;
  t_IntConverter intNum;
  int index = 0;
  
  // Message Type
  shortNum.value = TS_RES_ORDER_CONFIG;
  memcpy(&msgToSend[index], shortNum.c, 2);
  index += 2;
  
  // Body Length
  intNum.value = 4 + 1+ 80 + 4 + 80 + 80;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // Block Length
  intNum.value = 1 + 80 + 4 + 80 + 80;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // ECN ID
  msgToSend[index] = ORDER_BATSZ_BOE;
  index += 1;
  
  /*
  Details
  - IP Address
  - Port
  - Username
  - Password
  */
  
  // IP Address
  strncpy(&msgToSend[index],  OecConfig[ORDER_BATSZ_BOE].ipAddress, 80);
  index += 80;
  
  // Port
  intNum.value =  OecConfig[ORDER_BATSZ_BOE].port;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // Username
  strncpy(&msgToSend[index],  OecConfig[ORDER_BATSZ_BOE].userName, 80);
  index += 80;
  
  // Password
  strncpy(&msgToSend[index],  OecConfig[ORDER_BATSZ_BOE].password, 80);
  index += 80;
  
  // Send Message to AS
  return SendMessageToAS(msgToSend, index);
}


/****************************************************************************
- Function name:  BuildSend_BYX_BOE_Configuration
****************************************************************************/
int BuildSend_BYX_BOE_Configuration(void)
{
  char msgToSend[2 + 4 + 4 + 1 + 80 + 4 + 80 + 80];
  t_ShortConverter shortNum;
  t_IntConverter intNum;
  int index = 0;
  
  // Message Type
  shortNum.value = TS_RES_ORDER_CONFIG;
  memcpy(&msgToSend[index], shortNum.c, 2);
  index += 2;
  
  // Body Length
  intNum.value = 4 + 1+ 80 + 4 + 80 + 80;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // Block Length
  intNum.value = 1 + 80 + 4 + 80 + 80;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // ECN ID
  msgToSend[index] = ORDER_BYX_BOE;
  index += 1;
  
  /*
  Details
  - IP Address
  - Port
  - Username
  - Password
  */
  
  // IP Address
  strncpy(&msgToSend[index], OecConfig[ORDER_BYX_BOE].ipAddress, 80);
  index += 80;
  
  // Port
  intNum.value = OecConfig[ORDER_BYX_BOE].port;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // Username
  strncpy(&msgToSend[index], OecConfig[ORDER_BYX_BOE].userName, 80);
  index += 80;
  
  // Password
  strncpy(&msgToSend[index], OecConfig[ORDER_BYX_BOE].password, 80);
  index += 80;
  
  // Send Message to AS
  return SendMessageToAS(msgToSend, index);
}

/****************************************************************************
- Function name:  BuildSend_EDGX_Configuration
****************************************************************************/
int BuildSend_EDGX_Configuration(void)
{
  char msgToSend[2 + 4 + 4 + 1 + 80 + 4 + 80 + 80];
  t_ShortConverter shortNum;
  t_IntConverter intNum;
  int index = 0;
  
  // Message Type
  shortNum.value = TS_RES_ORDER_CONFIG;
  memcpy(&msgToSend[index], shortNum.c, 2);
  index += 2;
  
  // Body Length
  intNum.value = 4 + 1+ 80 + 4 + 80 + 80;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // Block Length
  intNum.value = 4 + 1 + 80 + 4 + 80 + 80;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // ECN ID
  msgToSend[index] = ORDER_EDGX;
  index += 1;
  
  /*
  Details
  - IP Address
  - Port
  - Username
  - Password
  */
  
  // IP Address
  strncpy(&msgToSend[index], OecConfig[ORDER_EDGX].ipAddress, 80);
  index += 80;
  
  // Port
  intNum.value = OecConfig[ORDER_EDGX].port;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // Username
  strncpy(&msgToSend[index], OecConfig[ORDER_EDGX].userName, 80);
  index += 80;
  
  // Password
  strncpy(&msgToSend[index], OecConfig[ORDER_EDGX].password, 80);
  index += 80;
  
  // Send Message to AS
  return SendMessageToAS(msgToSend, index);
}

/****************************************************************************
- Function name:  BuildSend_EDGA_Configuration
****************************************************************************/
int BuildSend_EDGA_Configuration(void)
{
  char msgToSend[2 + 4 + 4 + 1 + 80 + 4 + 80 + 80];
  t_ShortConverter shortNum;
  t_IntConverter intNum;
  int index = 0;
  
  // Message Type
  shortNum.value = TS_RES_ORDER_CONFIG;
  memcpy(&msgToSend[index], shortNum.c, 2);
  index += 2;
  
  // Body Length
  intNum.value = 4 + 1+ 80 + 4 + 80 + 80;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // Block Length
  intNum.value = 4 + 1 + 80 + 4 + 80 + 80;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // ECN ID
  msgToSend[index] = ORDER_EDGA;
  index += 1;
  
  /*
  Details
  - IP Address
  - Port
  - Username
  - Password
  */
  
  // IP Address
  strncpy(&msgToSend[index], OecConfig[ORDER_EDGA].ipAddress, 80);
  index += 80;
  
  // Port
  intNum.value = OecConfig[ORDER_EDGA].port;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // Username
  strncpy(&msgToSend[index], OecConfig[ORDER_EDGA].userName, 80);
  index += 80;
  
  // Password
  strncpy(&msgToSend[index], OecConfig[ORDER_EDGA].password, 80);
  index += 80;
  
  // Send Message to AS
  return SendMessageToAS(msgToSend, index);
}

/****************************************************************************
- Function name:  BuildSend_Global_Configuration
- Description:
****************************************************************************/
int BuildSend_Global_Configuration(void)
{
  // In this message, we will send global config values and Regular session time values (4 bytes)
  
  int globalConfigBlockSize = 8*7+4*6;
  
  // Size = Header + Global config + Regular session time(4 bytes)
  char msgToSend[2 + 4 + 4 + globalConfigBlockSize + 4];
  t_ShortConverter shortNum;
  t_IntConverter intNum;
  
  int index = 0;
  
  // Message Type
  shortNum.value = TS_RES_GLOBAL_CONFIG;
  memcpy(&msgToSend[index], shortNum.c, 2);
  index += 2;
  
  // Body Length
  intNum.value = 4 + globalConfigBlockSize + 4;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // Block Length
  intNum.value = globalConfigBlockSize + 4;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // Block Content
  memcpy(&msgToSend[index], &AlgorithmConfig.globalConfig.maxBuyingPower, 8);
  index += 8;
  memcpy(&msgToSend[index], &AlgorithmConfig.globalConfig.maxShareVolume, 4);
  index += 4;
  memcpy(&msgToSend[index], &AlgorithmConfig.globalConfig.maxConsecutiveLoser, 4);
  index += 4;
  memcpy(&msgToSend[index], &AlgorithmConfig.globalConfig.maxStuckPosition, 4);
  index += 4;
  memcpy(&msgToSend[index], &AlgorithmConfig.globalConfig.serverRole, 4);
  index += 4;
  memcpy(&msgToSend[index], &AlgorithmConfig.globalConfig.minSpread, 8);
  index += 8;
  memcpy(&msgToSend[index], &AlgorithmConfig.globalConfig.maxSpread, 8);
  index += 8;
  memcpy(&msgToSend[index], &AlgorithmConfig.globalConfig.secFee, 8);
  index += 8;
  memcpy(&msgToSend[index], &AlgorithmConfig.globalConfig.commission, 8);
  index += 8;
  memcpy(&msgToSend[index], &AlgorithmConfig.globalConfig.loserRate, 8);
  index += 8;
  memcpy(&msgToSend[index], &AlgorithmConfig.globalConfig.exRatio, 8);
  index += 8;
  memcpy(&msgToSend[index], &AlgorithmConfig.globalConfig.nasdaqAuctionBeginTime, 4);
  index += 4;
  memcpy(&msgToSend[index], &AlgorithmConfig.globalConfig.nasdaqAuctionEndTime, 4);
  index += 4;
  
  shortNum.value = TradingSessionMgmt.RegularSessionBeginHour * 60 + TradingSessionMgmt.RegularSessionBeginMin;
  memcpy(&msgToSend[index], shortNum.c, 2);
  index += 2;

  shortNum.value = TradingSessionMgmt.RegularSessionEndHour * 60 + TradingSessionMgmt.RegularSessionEndMin;
  memcpy(&msgToSend[index], shortNum.c, 2);
  index += 2;
  
  TraceLog(DEBUG_LEVEL, "Sent global conf to AS: msgLen=%d\n", index);
  
  // Send message to AS
  return SendMessageToAS(msgToSend, index);
}

/****************************************************************************
- Function name:  BuildSend_MinSpread_Configuration
- Description:
****************************************************************************/
int BuildSend_MinSpread_Configuration(void)
{
  // In this message, we will send min spread config  
  // Size = Header + min spread config
  char msgToSend[2 + 4 + 4 + (8 + 8 + 4 + MinSpreadConf.intraday.countTimes * (4 + 8))];
  t_ShortConverter shortNum;
  t_IntConverter intNum;
  
  int index = 0;
  
  // Message Type
  shortNum.value = TS_RES_MIN_SPREAD_CONF;
  memcpy(&msgToSend[index], shortNum.c, 2);
  index += 2;
  
  // Body Length
  intNum.value = 4 + (8 + 8 + 4 + MinSpreadConf.intraday.countTimes * (4 + 8));
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // Block Length
  intNum.value = 4 + (8 + 8 + 4 + MinSpreadConf.intraday.countTimes * (4 + 8));
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // Block Content
  // Min spread in pre-market
  memcpy(&msgToSend[index], &MinSpreadConf.preMarket, 8);
  index += 8;
  
  // Min spread in post-market
  memcpy(&msgToSend[index], &MinSpreadConf.postMarket, 8);
  index += 8;
  
  // Num times of intraday
  memcpy(&msgToSend[index], &MinSpreadConf.intraday.countTimes, 4);
  index += 4;
  
  int i;
  for (i = 0; i < MinSpreadConf.intraday.countTimes; i++)
  {
    memcpy(&msgToSend[index], &MinSpreadConf.intraday.minSpreadList[i].timeInSeconds, 4);
    index += 4;
    
    memcpy(&msgToSend[index], &MinSpreadConf.intraday.minSpreadList[i].minSpread, 8);
    index += 8;
  }
  
  // Send message to AS
  return SendMessageToAS(msgToSend, index);
}

/****************************************************************************
- Function name:  BuildSend_SplitSymbol_Configuration
- Description:
****************************************************************************/
int BuildSend_SplitSymbol_Configuration(void)
{
  //--------------------------------------------------------------------------
  //            Message structure
  //    + Message type    2 bytes
  //    + Body Length   4 bytes
  //    + Block Length    4 bytes
  //    + Symbol Range ID 4 bytes
  //    + Start of Range  1 byte
  //    + End of Range    1 byte
  //--------------------------------------------------------------------------
  
  int msgLen = 16;
  char msgToSend[msgLen];
  memset(msgToSend, 0, msgLen);
  
  t_ShortConverter shortNum;
  t_IntConverter intNum;
  int index = 0;
  
  // Message Type
  shortNum.value = TS_RES_SPLIT_SYMBOL_CONF;
  memcpy(&msgToSend[index], shortNum.c, 2);
  index += 2;
  
  // Body Length
  intNum.value = msgLen - MSG_HEADER_LEN;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // Block Length
  intNum.value = msgLen - MSG_HEADER_LEN;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // Symbol Range ID
  intNum.value = AlgorithmConfig.splitConfig.symbolRangeID + 1;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // Start of Range
  msgToSend[index] = AlgorithmConfig.splitConfig.startOfRange;
  index += 1;
  
  // End of Range
  msgToSend[index] = AlgorithmConfig.splitConfig.endOfRange;
  index += 1;
  
  TraceLog(DEBUG_LEVEL, "Send Split Symbol Conf to AS: rangeID=%d, from %c to %c, msgLen=%d\n",
              AlgorithmConfig.splitConfig.symbolRangeID + 1,
              AlgorithmConfig.splitConfig.startOfRange,
              AlgorithmConfig.splitConfig.endOfRange,
              index);
  
  return SendMessageToAS(msgToSend, index);
}

/****************************************************************************
- Function name:  SendCurrentStatusToAS
- Description:
****************************************************************************/
int numOfSkippedTimes = 0;
int SendCurrentStatusToAS(void)
{
  if (numOfSkippedTimes < 30)
  {
    numOfSkippedTimes++;
  }
  else
  {
    numOfSkippedTimes = 0;
    
    TraceLog(DEBUG_LEVEL, "Send current status to AS\n");
  }
  
  int currentStatusBlockSize = sizeof(t_CurrentStatus);
  
  char msgToSend[2 + 4 + 4 + currentStatusBlockSize + 1];
  t_ShortConverter shortNum;
  t_IntConverter intNum;
  int index = 0;
  
  // Message Type
  shortNum.value = TS_RES_CURRENT_STATUS;
  memcpy(&msgToSend[index], shortNum.c, 2);
  index += 2;
  
  // Body Length
  intNum.value = 4 + currentStatusBlockSize;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // Block Length
  intNum.value = currentStatusBlockSize;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  /*
  Get current status and send it to AS
  */
  TSCurrentStatus.status_ARCA_Book = BookStatusMgmt[BOOK_ARCA].isConnected;
  TSCurrentStatus.status_NASDAQ_Book = BookStatusMgmt[BOOK_NASDAQ].isConnected;
  TSCurrentStatus.status_NYSE_Book = BookStatusMgmt[BOOK_NYSE].isConnected;
  TSCurrentStatus.status_BATSZ_Book = BookStatusMgmt[BOOK_BATSZ].isConnected;
  TSCurrentStatus.status_EDGX_Book = BookStatusMgmt[BOOK_EDGX].isConnected;
  TSCurrentStatus.status_EDGA_Book = BookStatusMgmt[BOOK_EDGA].isConnected;
  TSCurrentStatus.status_NDBX_Book = BookStatusMgmt[BOOK_NDBX].isConnected;
  TSCurrentStatus.status_BYX_Book = BookStatusMgmt[BOOK_BYX].isConnected;
  TSCurrentStatus.status_PSX_Book = BookStatusMgmt[BOOK_PSX].isConnected;
  TSCurrentStatus.status_AMEX_Book = BookStatusMgmt[BOOK_AMEX].isConnected;
  
  TSCurrentStatus.status_ARCA_DIRECT_Order = OrderStatusMgmt[ORDER_ARCA_DIRECT].isConnected;
  TSCurrentStatus.status_NASDAQ_OUCH_Order = OrderStatusMgmt[ORDER_NASDAQ_OUCH].isConnected;
  TSCurrentStatus.status_NASDAQ_BX_Order = OrderStatusMgmt[ORDER_NASDAQ_BX].isConnected;
  TSCurrentStatus.status_NYSE_CCG_Order = OrderStatusMgmt[ORDER_NYSE_CCG].isConnected;
  TSCurrentStatus.status_NASDAQ_RASH_Order = OrderStatusMgmt[ORDER_NASDAQ_RASH].isConnected;
  TSCurrentStatus.status_BATSZ_BOE_Order = OrderStatusMgmt[ORDER_BATSZ_BOE].isConnected;
  TSCurrentStatus.status_EDGX_Order = OrderStatusMgmt[ORDER_EDGX].isConnected;
  TSCurrentStatus.status_EDGA_Order = OrderStatusMgmt[ORDER_EDGA].isConnected;
  TSCurrentStatus.status_BYX_BOE_Order = OrderStatusMgmt[ORDER_BYX_BOE].isConnected;
  TSCurrentStatus.status_PSX_Order = OrderStatusMgmt[ORDER_NASDAQ_PSX].isConnected;
  
  TSCurrentStatus.status_ARCA_DIRECT_Trading = *(TradingStatus.status[ORDER_ARCA_DIRECT]);
  TSCurrentStatus.status_NASDAQ_OUCH_Trading = *(TradingStatus.status[ORDER_NASDAQ_OUCH]);
  TSCurrentStatus.status_NASDAQ_BX_Trading = *(TradingStatus.status[ORDER_NASDAQ_BX]);
  TSCurrentStatus.status_NYSE_CCG_Trading = *(TradingStatus.status[ORDER_NYSE_CCG]);
  TSCurrentStatus.status_NASDAQ_RASH_Trading = *(TradingStatus.status[ORDER_NASDAQ_RASH]);
  TSCurrentStatus.status_BATSZ_BOE_Trading = *(TradingStatus.status[ORDER_BATSZ_BOE]);
  TSCurrentStatus.status_EDGX_Trading = *(TradingStatus.status[ORDER_EDGX]);
  TSCurrentStatus.status_EDGA_Trading = *(TradingStatus.status[ORDER_EDGA]);
  TSCurrentStatus.status_BYX_BOE_Trading = *(TradingStatus.status[ORDER_BYX_BOE]);
  TSCurrentStatus.status_PSX_Trading = *(TradingStatus.status[ORDER_NASDAQ_PSX]);
  
  // statusCQS +  statusUQDF + statusISOTrading;
  TSCurrentStatus.statusCQS = CQS_StatusMgmt.isConnected;
  TSCurrentStatus.statusUQDF = UQDF_StatusMgmt.isConnected;
  TSCurrentStatus.statusISOTrading = ISO_Trading;
  
  // Block Content
  double bp = TSCurrentStatus.buyingPower * 0.01;
  memcpy(&msgToSend[index], &bp, 8);
  index += 8;

  memcpy(&msgToSend[index], &TSCurrentStatus.numStuck, 4);
  index += 4;

  memcpy(&msgToSend[index], &TSCurrentStatus.numLoser, 4);
  index += 4;

  msgToSend[index++] = TSCurrentStatus.status_ARCA_Book;
  msgToSend[index++] = TSCurrentStatus.status_NASDAQ_Book;
  msgToSend[index++] = TSCurrentStatus.status_NYSE_Book;
  msgToSend[index++] = TSCurrentStatus.status_BATSZ_Book;
  msgToSend[index++] = TSCurrentStatus.status_EDGX_Book;
  msgToSend[index++] = TSCurrentStatus.status_EDGA_Book;
  msgToSend[index++] = TSCurrentStatus.status_NDBX_Book;
  msgToSend[index++] = TSCurrentStatus.status_BYX_Book;
  msgToSend[index++] = TSCurrentStatus.status_PSX_Book;
  msgToSend[index++] = TSCurrentStatus.status_AMEX_Book;

  msgToSend[index++] = TSCurrentStatus.status_ARCA_DIRECT_Order;
  msgToSend[index++] = TSCurrentStatus.status_NASDAQ_OUCH_Order;
  msgToSend[index++] = TSCurrentStatus.status_NYSE_CCG_Order;
  msgToSend[index++] = TSCurrentStatus.status_NASDAQ_RASH_Order;
  msgToSend[index++] = TSCurrentStatus.status_BATSZ_BOE_Order;
  msgToSend[index++] = TSCurrentStatus.status_EDGX_Order;
  msgToSend[index++] = TSCurrentStatus.status_EDGA_Order;
  msgToSend[index++] = TSCurrentStatus.status_NASDAQ_BX_Order;
  msgToSend[index++] = TSCurrentStatus.status_BYX_BOE_Order;
  msgToSend[index++] = TSCurrentStatus.status_PSX_Order;

  msgToSend[index++] = TSCurrentStatus.status_ARCA_DIRECT_Trading;
  msgToSend[index++] = TSCurrentStatus.status_NASDAQ_OUCH_Trading;
  msgToSend[index++] = TSCurrentStatus.status_NYSE_CCG_Trading;
  msgToSend[index++] = TSCurrentStatus.status_NASDAQ_RASH_Trading;
  msgToSend[index++] = TSCurrentStatus.status_BATSZ_BOE_Trading;
  msgToSend[index++] = TSCurrentStatus.status_EDGX_Trading;
  msgToSend[index++] = TSCurrentStatus.status_EDGA_Trading;
  msgToSend[index++] = TSCurrentStatus.status_NASDAQ_BX_Trading;
  msgToSend[index++] = TSCurrentStatus.status_BYX_BOE_Trading;
  msgToSend[index++] = TSCurrentStatus.status_PSX_Trading;

  msgToSend[index++] = TSCurrentStatus.statusCQS;
  msgToSend[index++] = TSCurrentStatus.statusUQDF;
  msgToSend[index++] = TSCurrentStatus.statusISOTrading;
  
  // padding
  index += 7;

  // Send message to AS
  return SendMessageToAS(msgToSend, index);
}

/****************************************************************************
- Function name:  SendECNDataBlockToAS
- Description:
****************************************************************************/
int SendECNDataBlockToAS(const void *configInfo, void *orderRawDataMgmt)
{
  int status = SUCCESS;
  
  /*
  WARNING:
  + We maybe get problem with cursor in raw data file when we change
  positions of it when read and write
  */
  
  t_OrderRawDataMgmt *workingOrderMgmt = (t_OrderRawDataMgmt *)orderRawDataMgmt;
  
  /*
  Check AS sent last received index to TS or not.
  If not return the calling routine and report pending.
  Otherwise continue the below steps
  */
  if (workingOrderMgmt->lastSentIndex == -2)
  {
    TraceLog(DEBUG_LEVEL, "SendECNDataBlockToAS: Pending\n");
    return PENDING;
  }
  
  int countNewDataBlock = workingOrderMgmt->countRawDataBlock - (workingOrderMgmt->lastSentIndex + 1); 
  
  //TraceLog(DEBUG_LEVEL, "countNewDataBlock %d\n", countNewDataBlock);
  
  /*
  We have some data blocks to send to AS. 
  We should build message header before
  */
  if (countNewDataBlock > 0)
  {
    /*
    Check if total of data blocks greater than size of collection, 
    we will read from file to send to AS
    */
    if (countNewDataBlock > MAX_NUM_ORDER_MSG)
    {
      /*
      Send data blocks from file
      */
      TraceLog(DEBUG_LEVEL, "Begin to load file and send data block to AS\n");
      status = SendDataBlockFromFileToAS(configInfo, orderRawDataMgmt);
      fseek(workingOrderMgmt->fileDesc, 0, SEEK_END);
      TraceLog(DEBUG_LEVEL, "End loading file and send data block to AS\n");
    }
    else
    {
      /*
      Otherwise we will get new data blocks 
      from collection to send to AS
      */
      status = SendDataBlockFromCollectionToAS(configInfo, orderRawDataMgmt);
    }
  }
  
  /*
  Report status value for calling routine
  */  
  return status;
}

/****************************************************************************
- Function name:  SendDataBlockFromFileToAS
- Description:
****************************************************************************/
int SendDataBlockFromFileToAS(const void *configInfo, void *orderRawDataMgmt)
{
  t_OrderRawDataMgmt *workingOrderMgmt = (t_OrderRawDataMgmt *)orderRawDataMgmt;
  t_ASConfig *workingASConfig = (t_ASConfig *)configInfo;
  
  char msgToSend[MSG_HEADER_LEN] = "\0";
  int index = 0;
  int status = SUCCESS;
  t_ShortConverter shortConverter;
  t_IntConverter intConverter;
  
  /*
  Sum total bytes of body message. This variable has default 
  value 5 that is size of block length (4 bytes) and ECN Order ID fields (1 byte)
  */
  int bodyLen = 5;
  int realNumDataBlockToSend = 0;
  int i;
  int retValue;
  
  t_DataBlock dataBlock;
  
  // Set cursor position for file at begin of specific data block
  fseek(workingOrderMgmt->fileDesc, (workingOrderMgmt->lastSentIndex + 1) * sizeof(t_DataBlock), SEEK_SET);
  int numberOfDataBlockToSend = workingOrderMgmt->countRawDataBlock;
  
  for (i = workingOrderMgmt->lastSentIndex + 1; i < numberOfDataBlockToSend; i++)
  {
    retValue = fread(&dataBlock, sizeof(t_DataBlock), 1, workingOrderMgmt->fileDesc);
    
    if (retValue != 1)
    {               
      break;
    }
    else
    {     
      bodyLen += dataBlock.blockLen;
    
      realNumDataBlockToSend++;
      
      if (bodyLen >= MAX_RESPONSE_MSG_LEN)
      { 
        break;
      }
    }
  }
  
  if (realNumDataBlockToSend <= 0)
  {
    return SUCCESS;
  }
  
  /*
  Build message to send to AS
  */
  
  // Message type
  shortConverter.value = TS_RES_NEW_RAWDATA;
  memcpy(&msgToSend[index], shortConverter.c, 2);
  index += 2;
  
  // Body length
  intConverter.value = bodyLen;
  memcpy(&msgToSend[index], intConverter.c, 4);
  index += 4;
  
  /*
  Get mutex lock to synchronize sending because maybe 2 or more threads will use 
  AS socket to send at the same time
  */
  
  pthread_mutex_lock(&workingASConfig->sendingLock);
  
  // Send message header
  retValue = send(workingASConfig->socket, msgToSend, MSG_HEADER_LEN, 0);
  
  int tempSentByte = 0;
  
  if (retValue == MSG_HEADER_LEN)
  {
    // Send Block Length
    intConverter.value = bodyLen;
    retValue = send(workingASConfig->socket, intConverter.c, 4, 0);
    
    // Send ECN Order ID
    retValue += send(workingASConfig->socket, (char *)(&workingOrderMgmt->id), 1, 0);
    
    if (retValue != 5)
    {
      TraceLog(ERROR_LEVEL, "Could not send Block Length and ECN Order ID %d\n", retValue);
      status = ERROR;
    }
    else
    {
      // Set cursor position for file at begin of specific data block
      fseek(workingOrderMgmt->fileDesc, (workingOrderMgmt->lastSentIndex + 1) * sizeof(t_DataBlock), SEEK_SET);
    
      // Send message body
      for (i = 0; i < realNumDataBlockToSend; i++)
      {
        retValue = fread(&dataBlock, sizeof(t_DataBlock), 1, workingOrderMgmt->fileDesc);
        
        if (retValue != 1)
        {
          status = ERROR;
          break;
        }
        else
        {
          retValue = 0;
          
          // Send block length
          intConverter.value = dataBlock.blockLen;
          retValue += send(workingASConfig->socket, intConverter.c, 4, 0);
          
          // Send message length
          intConverter.value = dataBlock.msgLen;
          retValue += send(workingASConfig->socket, intConverter.c, 4, 0);
          
          // Send message content
          retValue += send(workingASConfig->socket, dataBlock.msgContent, dataBlock.msgLen, 0);
          
          // Send additional length
          intConverter.value = DATABLOCK_ADDITIONAL_INFO_LEN;
          retValue += send(workingASConfig->socket, intConverter.c, 4, 0);
          
          // Send additional content
          retValue += send(workingASConfig->socket, dataBlock.addContent, DATABLOCK_ADDITIONAL_INFO_LEN, 0);
          
          // If we sent a data block successfully, we should update list sent index by 1
          if (retValue != dataBlock.blockLen)
          {
            TraceLog(ERROR_LEVEL, "Could not send data block to AS %d dataBlock.blockLen = %d\n", retValue, dataBlock.blockLen);
            status = ERROR;
            break;
          }
          else
          {
            tempSentByte += retValue;
            
            workingOrderMgmt->lastSentIndex++;
            TraceLog(DEBUG_LEVEL, "workingOrderMgmt->lastSentIndex = %d\n", workingOrderMgmt->lastSentIndex);
          }
        }
      }
    }
  }
  else
  {
    TraceLog(ERROR_LEVEL, "Could not send header message to AS\n");
  }
  
  pthread_mutex_unlock(&workingASConfig->sendingLock);
  
  return status;
}

/****************************************************************************
- Function name:  SendDataBlockFromCollectionToAS
****************************************************************************/
int SendDataBlockFromCollectionToAS(const void *configInfo, void *orderRawDataMgmt)
{ 
  t_OrderRawDataMgmt *workingOrderMgmt = (t_OrderRawDataMgmt *)orderRawDataMgmt;
  t_ASConfig *workingASConfig = (t_ASConfig *)configInfo;
  
  char msgToSend[MSG_HEADER_LEN] = "\0";
  int index = 0;
  int status = SUCCESS;
  t_ShortConverter shortConverter;
  t_IntConverter intConverter;
  
  /*
  Sum total bytes of body message. This variable has default 
  value 1 that is size of Block Length (4 bytes) and ECN Order ID field (1 byte)
  */
  int bodyLen = 5;
  int i;
  int actualIndex = workingOrderMgmt->lastSentIndex + 1;
  
  for (i = actualIndex; i < workingOrderMgmt->countRawDataBlock; i++)
  {
    if (workingOrderMgmt->dataBlockCollection[i % MAX_NUM_ORDER_MSG].msgLen > 0)  //This item was completely saved into memory
    {
      bodyLen += workingOrderMgmt->dataBlockCollection[i % MAX_NUM_ORDER_MSG].blockLen;
      actualIndex++;
      if (bodyLen >= MAX_RESPONSE_MSG_LEN)
      { 
        break;
      }
    }
  }
  
  /*
  Build message to send to AS
  */
  
  // Message type
  shortConverter.value = TS_RES_NEW_RAWDATA;
  memcpy(&msgToSend[index], shortConverter.c, 2);
  index += 2;
  
  // Body length
  intConverter.value = bodyLen;
  memcpy(&msgToSend[index], intConverter.c, 4);
  index += 4;
  
  /*  Get mutex lock to synchronize sending because maybe 2 or more threads will use 
    AS socket to send at the same time  */
  pthread_mutex_lock(&workingASConfig->sendingLock);
  
  // Send message header
  int retValue = send(workingASConfig->socket, msgToSend, MSG_HEADER_LEN, 0);
  
  if (retValue == MSG_HEADER_LEN)
  {
    // Send Block Length
    intConverter.value = bodyLen;
    retValue = send(workingASConfig->socket, intConverter.c, 4, 0);
    
    // Send ECN Order ID
    retValue += send(workingASConfig->socket, (char *)(&workingOrderMgmt->id), 1, 0);
    
    if (retValue != 5)
    {
      TraceLog(ERROR_LEVEL, "Could not send Block Length and ECN Order ID\n");
      status = ERROR;
    }
    else
    {
      // Send message body
      for (i = (workingOrderMgmt->lastSentIndex + 1); i < actualIndex; i++)
      {
        retValue = 0;
        
        // Send block length
        intConverter.value = workingOrderMgmt->dataBlockCollection[i % MAX_NUM_ORDER_MSG].blockLen;
        retValue += send(workingASConfig->socket, intConverter.c, 4, 0);
        
        // Send message length
        intConverter.value = workingOrderMgmt->dataBlockCollection[i % MAX_NUM_ORDER_MSG].msgLen;
        retValue += send(workingASConfig->socket, intConverter.c, 4, 0);
        
        // Send message content
        retValue += send(workingASConfig->socket, 
              workingOrderMgmt->dataBlockCollection[i % MAX_NUM_ORDER_MSG].msgContent, 
              workingOrderMgmt->dataBlockCollection[i % MAX_NUM_ORDER_MSG].msgLen, 0);
        
        // Send additional length
        intConverter.value = DATABLOCK_ADDITIONAL_INFO_LEN;
        retValue += send(workingASConfig->socket, intConverter.c, 4, 0);
        
        // Send additional content
        retValue += send(workingASConfig->socket, 
              workingOrderMgmt->dataBlockCollection[i % MAX_NUM_ORDER_MSG].addContent, 
              DATABLOCK_ADDITIONAL_INFO_LEN, 0);
        
        // If we sent a data block successfully, we should update list sent index by 1
        if (retValue != workingOrderMgmt->dataBlockCollection[i % MAX_NUM_ORDER_MSG].blockLen)
        {
          TraceLog(ERROR_LEVEL, "Could not send data block to AS\n");
          status = ERROR;
          break;
        }
        else
        {
          workingOrderMgmt->lastSentIndex++;
        }
      }
    }
  }
  else
  {
    TraceLog(ERROR_LEVEL, "Could not send header message to AS\n");
  }
        
  pthread_mutex_unlock(&workingASConfig->sendingLock);
  
  //TraceLog(DEBUG_LEVEL, "workingOrderMgmt->lastSentIndex = %d\n", workingOrderMgmt->lastSentIndex);
  
  return status;
}

/****************************************************************************
- Function name:  SendDataToASMgmt
- Description:
****************************************************************************/
void *SendDataToASMgmt(void *threadArgs)
{
  SetKernelAlgorithm("[SEND_DATA_TO_AS]");
  t_ASConfig *config = (t_ASConfig *)threadArgs;
  int res;
  int iResend = 0;
  int orderID;
  while (config->socket != -1)
  {
    // Send current status to AS
    if (SendCurrentStatusToAS() == ERROR)
    {
      TraceLog(ERROR_LEVEL, "Could not send current status to AS\n");
      break;
    }
    
    // Send Trade Ouput Log
    iResend = 0;
    res = -2;
    while ((iResend < 10) && (res == -2))
    {
      iResend ++;
      res = SendTradeOutputLogToAS();
      if (res == ERROR)
      {
        TraceLog(ERROR_LEVEL, "Could not send Trade Output Log to AS\n");
        goto outhere;
      }
    }
    
    // Send data block to AS
    for (orderID = 0; orderID < MAX_ECN_ORDER; orderID++)
    {
      if (SendECNDataBlockToAS(config, &OrderMgmt[orderID]) == -1)
      {
        TraceLog(ERROR_LEVEL, "Could not send %s data block to AS\n", GetOrderNameByIndex(orderID));
        goto outhere;
      }
    }
    
    //Send BBO Info to AS, must be sent after rawdata are sent, for ISO Flight file creation
    iResend = 0;
    res = -2;
    while ((iResend < 10) && (res == -2))
    {
      iResend ++;
      res = SendBBOInfoToAS();
      if (res == ERROR)
      {
        TraceLog(ERROR_LEVEL, "Cannnot send BBO Infor to AS\n");
        goto outhere;
      }
    }
    
    // Check and send symbols which having unsent halt status
    SendHaltStockListToAS();
    
    usleep(500000);
  }
outhere:

  // Reset last sent index
  for (orderID = 0; orderID < MAX_ECN_ORDER; orderID++)
  {
    OrderMgmt[orderID].lastSentIndex = -2;
  }

  TradeOutputLogMgmt.countSentLogEntry = -1;
  
  TraceLog(DEBUG_LEVEL, "Return out of SendDataBlockToASMgmt\n");
  
  close(config->socket);
  config->socket = -1;
  
  return NULL;
}

int ParseGetPriceUpdateForSymbolList(char *message)
{
  int offset = 10;
  int totalSymbol = *(int*)(&message[offset]);
  offset += 4;
  
  const int maxSymbol = totalSymbol;
  char symbolList[maxSymbol][SYMBOL_LEN];
  memcpy(symbolList, &message[offset], SYMBOL_LEN * maxSymbol);
  offset += SYMBOL_LEN * maxSymbol;

  // TraceLog(DEBUG_LEVEL, "Received message price update request for %d %s\n",
      // maxSymbol, maxSymbol == 1 ? "symbol":"symbols");

  return SendPriceUpdateResponseToAS(maxSymbol, symbolList);
}

int SendPriceUpdateResponseToAS(int maxSymbol, char symbolList[][SYMBOL_LEN])
{
  //--------------------------------------------------------------------------
  //     Message structure
  //    + Message type      2 bytes
  //    + Body Length       4 bytes
  //    + Block Length      4 bytes
  //    * Number of Symbols 4 bytes
  //    + n blocks:
  //      ++ Symbol         8 bytes
  //      ++ Ask shares     4 bytes
  //      ++ Ask price      8 bytes
  //      ++ Bid shares     4 bytes
  //      ++ Bid price      8 bytes
  //--------------------------------------------------------------------------
  
  const int msgLen = MSG_HEADER_LEN + 4 + 4 + maxSymbol * (32);
  char msgToSend[msgLen];
  memset(msgToSend, 0, msgLen);
  
  t_ShortConverter shortNum;
  t_IntConverter intNum;
  int index = 0;
  
  // Message Type
  shortNum.value = TS_RES_PRICE_UPDATE;
  memcpy(&msgToSend[index], shortNum.c, 2);
  index += 2;
  
  // Body Length
  index += 4;
  
  // Block Length
  index += 4;
  
  // Number of Symbol
  int countSentSymbol = 0;
  int numSymbolIndex = index;
  index += 4;
    
  // Symbol List info
  int i;
  char symbol[SYMBOL_LEN + 1] = "\0";
  int stockSymbolIndex;
  
  double bestAskPrice = 9999999999.0, //first one
         bestBidPrice = 0.00; //first one
         
  int bestAskShares = 0,
      bestBidShares = 0;
      
  int updateAsk = 0,
      updateBid = 0;

  for (i = 0; i < maxSymbol; i++)
  {
    memcpy(symbol, symbolList[i], SYMBOL_LEN);
    
    stockSymbolIndex = GetStockSymbolIndex(symbol);
    if (stockSymbolIndex == -1)
    {
      // TraceLog(DEBUG_LEVEL, "Symbol %s is unavailable in stockbook\n", symbol);
      continue;
    }
    
    bestAskPrice = 9999999999.0;
    bestBidPrice = 0.00;
         
    bestAskShares = 0;
    bestBidShares = 0;
    
    updateAsk = 0;
    updateBid = 0;
    
    // Get best bid and ask from feeds
    t_BBOQuoteList *symbolInfo;
    symbolInfo = &BBOQuoteMgmt[stockSymbolIndex];
    int sourceId, i;
    for (sourceId = 0; sourceId < MAX_BBO_SOURCE; sourceId++)
    {
      for (i = 0; i < MAX_EXCHANGE; i++)
      {
        if (symbolInfo->BBOQuoteInfo[sourceId][i].participantId != 0)
        {
          if (symbolInfo->BBOQuoteInfo[sourceId][i].offerSize > 0)
          {
            if(fgt(bestAskPrice, symbolInfo->BBOQuoteInfo[sourceId][i].offerPrice))
            {
              updateAsk = 1;
              bestAskShares = symbolInfo->BBOQuoteInfo[sourceId][i].offerSize;
              bestAskPrice = symbolInfo->BBOQuoteInfo[sourceId][i].offerPrice;
            }
          }
          
          if (symbolInfo->BBOQuoteInfo[sourceId][i].bidSize > 0)
          {
            if(flt(bestBidPrice, symbolInfo->BBOQuoteInfo[sourceId][i].bidPrice))
            {
              updateBid = 1;
              bestBidShares = symbolInfo->BBOQuoteInfo[sourceId][i].bidSize;
              bestBidPrice = symbolInfo->BBOQuoteInfo[sourceId][i].bidPrice;
            }
          }
        }
      }
    }
    
    // Get best bid and ask from books if feeds do not have
    if (updateAsk == 0)
    {
      // Get ask quote
      int bookIndex;
      t_QuoteNode *node;
      t_QuoteList *list;
      for(bookIndex = 0; bookIndex < MAX_ECN_BOOK; bookIndex++)
      {
        list = StockBook[stockSymbolIndex][bookIndex][ASK_SIDE];
      
        while (list)
        {
          node = list->head;
          if (node)
          {
            updateAsk = 1;
            bestAskShares = node->info.shareVolume;
            bestAskPrice = node->info.sharePrice;
            break;
          }
          
          list = list->next;
        }
        
        if (updateAsk) break;
      }
    }
    
    if (updateBid == 0)
    {
      // Get bid quote
      int bookIndex;
      t_QuoteNode *node;
      t_QuoteList *list;
      for(bookIndex = 0; bookIndex < MAX_ECN_BOOK; bookIndex++)
      {
        list = StockBook[stockSymbolIndex][bookIndex][BID_SIDE];
      
        while (list)
        {
          node = list->head;
          if (node)
          {
            updateBid = 1;
            bestBidShares = node->info.shareVolume;
            bestBidPrice = node->info.sharePrice;
            break;
          }
          
          list = list->next;
        }
        
        if (updateBid) break;
      }
    }
    
    if (updateAsk && updateBid)
    {
      // Add symbol to message
      memcpy(&msgToSend[index], symbol, SYMBOL_LEN);
      index += SYMBOL_LEN;
      countSentSymbol++;
      
      // Add ask quote info
      memcpy(&msgToSend[index], &bestAskShares, 4);
      index += 4;
      
      long price = bestAskPrice * 10000;
      memcpy(&msgToSend[index], &price, 8);
      index += 8;
      
      // Add bid quote info
      memcpy(&msgToSend[index], &bestBidShares, 4);
      index += 4;
      
      price = bestBidPrice * 10000;
      memcpy(&msgToSend[index], &price, 8);
      index += 8;
    }
  }
  
  // Update Number of Symbol to sent
  memcpy(&msgToSend[numSymbolIndex], &countSentSymbol, 4);
  
  // Update body length
  intNum.value = index - MSG_HEADER_LEN;
  memcpy(&msgToSend[2], intNum.c, 4);
  
  // Update block length
  intNum.value = index - MSG_HEADER_LEN;
  memcpy(&msgToSend[6], intNum.c, 4);
  
  return SendMessageToAS(msgToSend, index);
}

/****************************************************************************
- Function name:  ParseGetStockSymbolStatus
- Description:
*********************************************************************************/
int ParseGetStockSymbolStatus(char *message)
{
  //Get TT ID
  int TT_ID = message[10];
  
  //Get Stock Symbol
  char stockSymbol[SYMBOL_LEN];
  strncpy(stockSymbol, &message[11], SYMBOL_LEN);
  
  char listMdc[MAX_ECN_BOOK];
  memcpy(listMdc, &message[11 + SYMBOL_LEN], MAX_ECN_BOOK);
  
  if (SendStockSymbolStatusToAS(TT_ID, stockSymbol, listMdc) == ERROR)
  {
    return ERROR;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendStockSymbolStatusToAS
- Description:
*********************************************************************************/
int SendStockSymbolStatusToAS(int TT_ID, char *stockSymbol, char *listMdc)
{
  //index of symbol in Stock Book
  int stockSymbolIndex = GetStockSymbolIndex(stockSymbol);
  
  if(stockSymbolIndex == -1)
  {
    TraceLog(DEBUG_LEVEL, "Stock Symbol %s did not exist in stock book\n", stockSymbol);
  }
  
  char msgToSend[2 + 4 + 4 + 1 + SYMBOL_LEN + 4 + (1 + 8 + 4) * (MAX_ECN_BOOK * 10) + 4 + (1 + 4 + 8) * (MAX_ECN_BOOK * 10)];
  
  t_ShortConverter shortNum;
  t_IntConverter intNum;
  t_DoubleConverter doubleNum;
  
  int index = 0;
  
  // Message Type
  shortNum.value = TS_RES_GET_STOCK_SYMBOL_STATUS;
  memcpy(&msgToSend[index], shortNum.c, 2);
  index += 2;
  
  // Body Length
  intNum.value = 0;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // TT and Stock symbol information
  intNum.value = 4 + 1 + SYMBOL_LEN;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  msgToSend[index] = TT_ID;
  index += 1;
  
  strncpy(&msgToSend[index], stockSymbol, SYMBOL_LEN);
  index += SYMBOL_LEN;
  
  int numOfAskEntries = 0;
  int numOfBidEntries = 0;
  int askBlockSize = 0;
  int bidBlockSize = 0;
  int bookIndex;
  
  t_QuoteNode *node;
  t_QuoteList *list;
  
  //ASK Block
  char askBlock[(1 + 8 + 4) * (MAX_ECN_BOOK * 10) + 1];
  int askBlockIndex = 0;
  
  if(stockSymbolIndex != -1)
  {
    for(bookIndex = 0; bookIndex < MAX_ECN_BOOK; bookIndex++)
    {
      if (listMdc[bookIndex] != 1)
      {
        // This book is not requested
        // TraceLog(DEV_LEVEL, "ask: do not get stock status of bookIndex=%d mdcType=%d\n", bookIndex, mdcType);
        continue;
      }
      
      numOfAskEntries = 0;
      list = StockBook[stockSymbolIndex][bookIndex][ASK_SIDE];
      
      while (list)
      {
        node = list->head;
        while(node)
        {
          // ECN ID
          askBlock[askBlockIndex] = bookIndex;
          askBlockIndex += 1;
          
          // Share
          intNum.value = node->info.shareVolume;
          memcpy(&askBlock[askBlockIndex], intNum.c, 4);
          askBlockIndex += 4;
          
          // Price
          doubleNum.value = node->info.sharePrice;
          memcpy(&askBlock[askBlockIndex], doubleNum.c, 8);
          askBlockIndex += 8;
          
          askBlockSize += 1 + 4 + 8;
          numOfAskEntries++;
          
          if (numOfAskEntries == 10) break;
          
          node = node->next;
        }
        
        if (numOfAskEntries == 10) break;
        
        list = list->next;
      }
    }
  }
  
  //BID Block
  char bidBlock[(1 + 8 + 4) * (MAX_ECN_BOOK * 10) + 1];
  int bidBlockIndex = 0;
  
  if(stockSymbolIndex != -1)
  {
    for(bookIndex = 0; bookIndex < MAX_ECN_BOOK; bookIndex++)
    {
      if (listMdc[bookIndex] != 1)
      {
        // This book is not requested
        // TraceLog(DEV_LEVEL, "bid: do not get stock status of bookIndex=%d mdcType=%d\n", bookIndex, mdcType);
        continue;
      }
      
      numOfBidEntries = 0;
      list = StockBook[stockSymbolIndex][bookIndex][BID_SIDE];
      
      while(list)
      {
        node = list->head;
        while(node)
        {
          //ECN ID
          bidBlock[bidBlockIndex] = bookIndex;
          bidBlockIndex += 1;
          
          //Share
          intNum.value = node->info.shareVolume;
          memcpy(&bidBlock[bidBlockIndex], intNum.c, 4);
          bidBlockIndex += 4;
          
          //Price
          doubleNum.value = node->info.sharePrice;
          memcpy(&bidBlock[bidBlockIndex], doubleNum.c, 8);
          bidBlockIndex += 8;
          
          bidBlockSize += 1 + 4 + 8;
          numOfBidEntries++;
          
          if (numOfBidEntries == 10) break;
          
          node = node->next;
        }
        
        if (numOfBidEntries == 10) break;
        
        list = list->next;
      }
    }
  }
  
  //Block Length 2
  intNum.value = askBlockSize + 4;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  //ASK BLOCK
  memcpy(&msgToSend[index], askBlock, askBlockSize);
  index += askBlockSize;
  
  //Block Length 3
  intNum.value = bidBlockSize + 4;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  //BID BLOCK
  memcpy(&msgToSend[index], bidBlock, bidBlockSize);
  index += bidBlockSize;
  
  // Update Body Length
  intNum.value = (4 + 1 + SYMBOL_LEN) +  askBlockSize + 4 + bidBlockSize + 4;
  memcpy(&msgToSend[2], intNum.c, 4);
  
  //TraceLog(DEBUG_LEVEL, "Stock Status (%d) (%d)\n", askBlockSize, bidBlockSize);
  
  return SendMessageToAS(msgToSend, index);
}

/****************************************************************************
- Function name:  ParseGetNumberOfSymbolsWithQuotes
- Description:
*********************************************************************************/
int ParseGetNumberOfSymbolsWithQuotes(char *message)
{

  int ttIndex = message[10];
  
  if( SendNumberOfSymbolsWithQuotesToAS(ttIndex) == ERROR)
  {
    return ERROR;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendNumberOfSymbolsWithQuotesToAS
- Description:
*********************************************************************************/
int SendNumberOfSymbolsWithQuotesToAS(int ttIndex)
{
  // Message Header includes Message Type & Body Length
  int msgHeader = 2 + 4;
  // Message Size = Message Header + Sum of (total Block length)
  int msgSize = msgHeader + 4 + 1 + MAX_ECN_BOOK * 4;
  // Block Length: in this message, we only have 1 block, so block length &
  // body length have the same value
  int blockLength = msgSize - msgHeader;

  char msgToSend[msgSize];
  memset(msgToSend, 0, msgSize);
  t_ShortConverter shortNum;
  t_IntConverter intNum;
  int index = 0;
  
  // Message Type
  shortNum.value = TS_RES_NUMBER_OF_SYMBOLS_WITH_QUOTES;
  memcpy(&msgToSend[index], shortNum.c, 2);
  index += 2;
  
  // Body Length
  intNum.value = blockLength;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // Block Length, block content = ttIndex(1) + numberOfSymbolsWithQuotes(MAX_ECN_BOOK * 4)
  intNum.value = blockLength;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // TT index
  msgToSend[index] = ttIndex;
  index += 1;
  
  int bookIndex, numberOfSymbolWithQuotes[MAX_ECN_BOOK];
  for (bookIndex = 0; bookIndex < MAX_ECN_BOOK; bookIndex++)
  {
    numberOfSymbolWithQuotes[bookIndex] = GetNumberOfSymbolsWithQuotes(bookIndex);
    // intNum.value = numberOfSymbolWithQuotes[bookIndex];
    memcpy(&msgToSend[index], &numberOfSymbolWithQuotes[bookIndex], 4);
    index += 4;
  }

  // Send message to AS
  return SendMessageToAS(msgToSend, index);
  
}

/****************************************************************************
- Function name:  SendBBOInfoToAS
- Description:
*********************************************************************************/
int SendBBOInfoToAS()
{
  if (BBOInfoMgmt.countBBOEntry == 0)
    return SUCCESS; //There is nothing to send
  
  if (BBOInfoMgmt.countSentBBOEntry == -1)
  {
    TraceLog(DEBUG_LEVEL, "Pending - Waiting to receive number of BBO entries from AS ...\n");
    return SUCCESS;
  }
  
  // Find actual index of last BBO item that *completely* saved into the memory
  int i;
  int actualIndex = BBOInfoMgmt.countSentBBOEntry;
  int total = BBOInfoMgmt.countBBOEntry;
  
  for (i = BBOInfoMgmt.countSentBBOEntry; i < total; i++)
  {
    if (BBOInfoMgmt.BBOInfoCollection[i % MAX_BBO_ENTRY].crossID != -1)   //This item was completely saved into memory
    {
      actualIndex++;
    }
    else
    {
      break;  //We must stop if find one which was not completely saved
    }
  }
  
  int nEntriesToSend =  actualIndex - BBOInfoMgmt.countSentBBOEntry;
    
  if (nEntriesToSend < 1) //There is nothing to send
  {
    return SUCCESS;
  }
  
  int ret = SUCCESS;
  if (nEntriesToSend > MAX_LOG_ENTRY_CAN_BE_SENT)
  {
    nEntriesToSend = MAX_LOG_ENTRY_CAN_BE_SENT;
    ret = -2;
  }
  
  int bodyLength = 0;
  int blockLength = 0;
  
  char msgToSend[10 + 4 + nEntriesToSend * sizeof(t_BBOQuoteItem) + 1];
  
  t_ShortConverter shortNum;
  t_IntConverter intNum;

  int index = 0;
  
  // Message Type
  shortNum.value = TS_RES_BBO_INFO;
  memcpy(&msgToSend[index], shortNum.c, 2);
  index += 2;
  
  // Sub 4 bytes for Body length and 4 bytes for Block length
  index += 4 + 4;
  
  // Number of entry that was sent
  intNum.value = nEntriesToSend;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // Include 4 bytes for Number of entry that was sent
  blockLength += 4 + 4;
  
  // Initialize Body length
  bodyLength += 4 + 4;
  
  //BBO info
  for(i = BBOInfoMgmt.countSentBBOEntry; i < BBOInfoMgmt.countSentBBOEntry + nEntriesToSend; i++)
  {
    memcpy(&msgToSend[index], &BBOInfoMgmt.BBOInfoCollection[i % MAX_BBO_ENTRY], sizeof(t_BBOQuoteItem));
    
    index += sizeof(t_BBOQuoteItem);
    
    bodyLength += sizeof(t_BBOQuoteItem);
    blockLength += sizeof(t_BBOQuoteItem);
  }
  
  BBOInfoMgmt.countSentBBOEntry += nEntriesToSend;
  
  // Message length(body length)
  intNum.value = bodyLength;
  memcpy(&msgToSend[2], intNum.c, 4);
  
  // Block Length
  intNum.value = blockLength;
  memcpy(&msgToSend[6], intNum.c, 4);

  if(SendMessageToAS(msgToSend, index) != SUCCESS)
  {
    BBOInfoMgmt.countSentBBOEntry -= nEntriesToSend;
    return ERROR;
  }
  else  //send successfully
  {
    return ret; //ret is now either -2 or SUCCESS
  }
}

/****************************************************************************
- Function name:  SendTradeOutputLogToAS
- Description:
*********************************************************************************/
int SendTradeOutputLogToAS()
{ 
  int res = SUCCESS;
  if (TradeOutputLogMgmt.countSentLogEntry == -1)
  {
    TraceLog(DEBUG_LEVEL, "Waiting for Trade Output Log received-information from Aggregation Server...\n");
    return SUCCESS;
  }
  
  int numberOfLogEntryToSend = 0;
  
  int i = 0;
  int tempCurrentLogEntryIndex = TradeOutputLogMgmt.countLogEntry;
  
  for (i = TradeOutputLogMgmt.countSentLogEntry; i < tempCurrentLogEntryIndex; i++)
  {
    if (TradeOutputLogMgmt.tradeOutputLogCollection[i % MAX_LOG_ENTRY][MAX_LEN_PER_LOG_ENTRY - 1] == YES)
    {
      numberOfLogEntryToSend++;
    }
    else
    {
      break;
    }
  }
  
  if (numberOfLogEntryToSend < 1)
  {
    return SUCCESS;
  }
  
  if (numberOfLogEntryToSend > MAX_LOG_ENTRY_CAN_BE_SENT)
  {
    numberOfLogEntryToSend = MAX_LOG_ENTRY_CAN_BE_SENT;
    res = -2;
  }
  
  int logEntryIndex;
  int bodyLength = 0;
  int blockLength = 0;
  
  char msgToSend[10 + MAX_LOG_ENTRY_CAN_BE_SENT * MAX_LEN_PER_LOG_ENTRY];
  
  t_ShortConverter shortNum;
  t_IntConverter intNum;

  int index = 0;
  
  // Message Type
  shortNum.value = TS_RES_TRADE_OUTPUT_LOG;
  memcpy(&msgToSend[index], shortNum.c, 2);
  index += 2;
  
  // Sub 4 bytes for Body length and 4 bytes for Block length
  index += 4 + 4;
  
  // Number of entry that was sent
  intNum.value = numberOfLogEntryToSend;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // Include 4 bytes for Number of entry that was sent
  blockLength += 4 + 4;
  
  // Initialize Body length
  bodyLength += 4 + 4;
  
  // Get Trade OutPut Log
  for(logEntryIndex = TradeOutputLogMgmt.countSentLogEntry; logEntryIndex < TradeOutputLogMgmt.countSentLogEntry + numberOfLogEntryToSend; logEntryIndex++)
  {
    memcpy(&msgToSend[index], TradeOutputLogMgmt.tradeOutputLogCollection[logEntryIndex % MAX_LOG_ENTRY], MAX_LEN_PER_LOG_ENTRY);
    
    index += MAX_LEN_PER_LOG_ENTRY;
    
    bodyLength += MAX_LEN_PER_LOG_ENTRY;
    blockLength += MAX_LEN_PER_LOG_ENTRY;
  }
  
  TradeOutputLogMgmt.countSentLogEntry += numberOfLogEntryToSend;
  
  // Message length(body length)
  intNum.value = bodyLength;
  memcpy(&msgToSend[2], intNum.c, 4);
  
  // Block Length
  intNum.value = blockLength;
  memcpy(&msgToSend[6], intNum.c, 4);

  if(SendMessageToAS(msgToSend, index) != SUCCESS)
  {
    return ERROR;
  }
  else  //SUCCESS
  {
    if (res == -2)
    {
      return -2;
    }
    else
    {
      return SUCCESS;
    }
  }
}

/****************************************************************************
- Function name:  ProcessSet_HaltedSymbolList
- Description:
****************************************************************************/
int ProcessSet_HaltedSymbolList(char *message)
{
  int countSymbol;
  memcpy(&countSymbol, &message[10], 4);
  
  int i, ecnIndex, flag;
  int index = 14;
  int stockSymbolIndex;
  char symbol[SYMBOL_LEN];
  unsigned int haltTime;
  char haltString[128];
  t_Symbol *symbolInfo;
  
  for (i = 0; i < countSymbol; i++)
  {
    flag = 0;
    haltString[0] = 0;
    
    strncpy(symbol, &message[index], SYMBOL_LEN);
    index += SYMBOL_LEN;
    
    stockSymbolIndex = GetStockSymbolIndex(symbol);
    
    if (stockSymbolIndex != -1)
    {
      symbolInfo = &SymbolMgmt.symbolList[stockSymbolIndex];
      
      for (ecnIndex = 0; ecnIndex < MAX_ECN_BOOK; ecnIndex++)
      {
        memcpy(&haltTime, &message[index + 1], 4);
        if ( (haltTime > symbolInfo->haltTime[ecnIndex]) ||
             ((haltTime == symbolInfo->haltTime[ecnIndex]) &&
             (symbolInfo->haltStatus[ecnIndex] != message[index]))
            ) //Just get the latest status
        {
          symbolInfo->haltTime[ecnIndex] = haltTime;
          symbolInfo->haltStatus[ecnIndex] = message[index];
          
          if (message[index] != TRADING_NORMAL)
          {
            flag = 1;
            strcat(haltString, GetBookNameByIndex(ecnIndex));
            strcat(haltString, " ");
          }
        }
        index += 5;
      }
      
      if (flag == 1)
      {
        TraceLog(DEBUG_LEVEL, "Receive a symbol halted (%.8s) at %sfrom AS\n", symbol, haltString);
      }
    }
    else
    {
      index += (5 * MAX_ECN_BOOK);
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessSet_AHaltedSymbol
- Description:
****************************************************************************/
int ProcessSet_AHaltedSymbol(char *message)
{
  int stockSymbolIndex;
  char symbol[SYMBOL_LEN];  
  
  strncpy(symbol, &message[10], SYMBOL_LEN);
  stockSymbolIndex = GetStockSymbolIndex(symbol);
  
  if (stockSymbolIndex == -1)
  {
    return SUCCESS;
  }
  
  t_Symbol *symbolInfo = &SymbolMgmt.symbolList[stockSymbolIndex];
  
  unsigned int haltTime;
  char haltString[128];
  int ecnIndex;
  int index = 10 + SYMBOL_LEN;
  haltString[0] = 0;
  char flag = 0;
  
  int oldStatus = 0;
  for (ecnIndex = 0; ecnIndex < MAX_ECN_BOOK; ecnIndex++)
  {
    oldStatus += symbolInfo->haltStatus[ecnIndex];
  }
  
  for (ecnIndex = 0; ecnIndex < MAX_ECN_BOOK; ecnIndex++)
  {
    memcpy(&haltTime, &message[index + 1], 4);
    if ((haltTime > symbolInfo->haltTime[ecnIndex]) ||
       ((haltTime == symbolInfo->haltTime[ecnIndex]) &&
        (symbolInfo->haltStatus[ecnIndex] != message[index])))//Just get the latest status
    {
      symbolInfo->haltTime[ecnIndex] = haltTime;
      symbolInfo->haltStatus[ecnIndex] = message[index];
          
      if (message[index] != TRADING_NORMAL)
      {
        flag = 1;
        strcat(haltString, GetBookNameByIndex(ecnIndex));
        strcat(haltString, " ");
      }
    }
    
    index += 5;
  }
      
  if (flag == 1)
  {
    TraceLog(DEBUG_LEVEL, "Receive a symbol halted (%.8s) at %sfrom AS\n", symbol, haltString);
  }
  else
  {
    int newStatus = 0;
    for (ecnIndex = 0; ecnIndex < MAX_ECN_BOOK; ecnIndex++)
    {
      newStatus += symbolInfo->haltStatus[ecnIndex];
    }
    
    if ((oldStatus != TRADING_NORMAL) && (newStatus == TRADING_NORMAL))
    {
      // Trading resumed
      TraceLog(DEBUG_LEVEL, "Receive a symbol resumed (%.8s) from AS\n", symbol);
      
      /*
      Do not detect trade here (for thread-safe)
      if (SymbolStatus[stockSymbolIndex].isDisable == YES)
      {
        return SUCCESS;
      }
      
      // Check MaxBid/MinAsk for changes to trade
      t_TopOrder bestQuote;

      GetBestServerRoleOrder(&bestQuote, ASK_SIDE, stockSymbolIndex, AlgorithmConfig.globalConfig.serverRole);
      
      if (bestQuote.ecnIndicator != -1)
      {       
        t_OrderDetail order;
        order.shareVolume = bestQuote.shareVolume;
        order.sharePrice = bestQuote.sharePrice;
        DetectCrossTrade(&order, ASK_SIDE, stockSymbolIndex, bestQuote.ecnIndicator, hbitime_micros(), REASON_CROSS_ENTRY);
      }*/
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  BuildSend_ARCA_IdleWarning
- Description:
****************************************************************************/
int BuildSend_Book_IdleWarning(int ecnIndex, int groupIndex)
{
  char msgToSend[2 + 4 + 4 + 4 + 4];
  memset(msgToSend, 0, 2 + 4 + 4 + 4 + 4);
  t_ShortConverter shortNum;
  t_IntConverter intNum;
  int index = 0;
  
  // Message Type
  shortNum.value = TS_RES_BOOK_IDLE_WARNING;
  memcpy(&msgToSend[index], shortNum.c, 2);
  index += 2;
  
  // Body Length
  intNum.value = 4 + 4 + 4;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // Block Length
  intNum.value = 4 + 4 + 4;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  //Ecn ID
  intNum.value = ecnIndex;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  //Group Index
  intNum.value = groupIndex;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // Send message to AS
  return SendMessageToAS(msgToSend, index);
}

/****************************************************************************
- Function name:  BuildSendUnhandledDataOnGroup
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int BuildSendUnhandledDataOnGroup(int serviceIndex)
{
  //--------------------------------------------------------------------------
  //    Message structure
  //    + Message type    2 bytes
  //    + Body Length     4 bytes
  //    + Block Length    4 bytes
  //    + Service Index   4 bytes
  //    + Group Index     4 bytes
  //--------------------------------------------------------------------------
  
  int msgLen = MSG_HEADER_LEN + 4 + 4;
  char msgToSend[msgLen];
  memset(msgToSend, 0, msgLen);
  
  t_ShortConverter shortNum;
  t_IntConverter intNum;
  int index = 0;
  
  // Message Type
  shortNum.value = TS_RES_UNHANDLED_DATA;
  memcpy(&msgToSend[index], shortNum.c, 2);
  index += 2;
  
  // Body Length
  intNum.value = msgLen - MSG_HEADER_LEN;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // Block Length
  intNum.value = msgLen - MSG_HEADER_LEN;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  //Service Index
  intNum.value = serviceIndex;
  memcpy(&msgToSend[index], intNum.c, 2);
  index += 4;
  
  return SendMessageToAS(msgToSend, index);
}

/****************************************************************************
- Function name:  ProcessConnectCQSnUQDFMessage
- Description:
****************************************************************************/
int ProcessConnectCQSnUQDFMessage(char *message)
{
  if (message[10] == CQS_SOURCE)
  {
    // Connect to CQS
    TraceLog(DEBUG_LEVEL, "Receive request to connect to CQS from AS ...\n");
    ConnectToVenue(FEED_CQS);
  }
  else
  {
    // Connect to UQDF
    TraceLog(DEBUG_LEVEL, "Receive request to connect to UQDF from AS ...\n");
    ConnectToVenue(FEED_UQDF);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessDisconnectCQSnUQDFMessage
- Description:
****************************************************************************/
int ProcessDisconnectCQSnUQDFMessage(char *message)
{
  if (message[10] == CQS_SOURCE)
  {
    // Connect to CQS
    TraceLog(DEBUG_LEVEL, "Receive request to disconnect to CQS from AS ...\n");
    DisconnectFromVenue(FEED_CQS, -1);
  }
  else
  {
    // Connect to UQDF
    TraceLog(DEBUG_LEVEL, "Receive request to disconnect to UQDF from AS ...\n");
    DisconnectFromVenue(FEED_UQDF, -1);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessSetISO_TradingMessage
- Description:
****************************************************************************/
int ProcessSetISO_TradingMessage(char *message)
{ 
  if (message[10] == ENABLE)
  {
    TraceLog(DEBUG_LEVEL, "Receive request to enable ISO algorithm from AS\n");
    ISO_Trading = ENABLE;
  }
  else
  {
    TraceLog(DEBUG_LEVEL, "Receive request to disable ISO algorithm from AS\n");
    ISO_Trading = DISABLE;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ParseGetBBOQuotes
- Description:
****************************************************************************/
int ParseGetBBOQuotes(char *message)
{
  //Get TT ID
  int TT_ID = message[10];
  
  //Get Stock Symbol
  char stockSymbol[SYMBOL_LEN];
  strncpy(stockSymbol, &message[11], SYMBOL_LEN);
  
  TraceLog(DEBUG_LEVEL, "Get BBO quotes, symbol = %.8s\n", stockSymbol);
  
  if (SendBBOQuotesToAS(TT_ID, stockSymbol) == ERROR)
  {
    return ERROR;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  BuilSendExchangeConfigToAS
- Description:
****************************************************************************/
int BuilSendExchangeConfigToAS(void)
{
  t_ShortConverter tmpShort;
  t_IntConverter tmpInt;
  char msgToSend[2 + 4 + 4 + MAX_EXCHANGE * 6];
  
  int currentIndex = 0; 
  
  // Message type
  tmpShort.value = MSG_TYPE_FOR_SEND_EXCHANGE_CONF_TO_AS; 
  msgToSend[currentIndex] = tmpShort.c[0];
  msgToSend[currentIndex + 1] = tmpShort.c[1];
  
  currentIndex += 2;
  
  // Message length
  currentIndex += 4;
  
  // Block length
  currentIndex += 4;
  
  // Block content = 16 * [index + ID + CQS_Status{visible + Status} + UQDF_Status{visible + Status}]
  int i;
  for (i = 0; i < MAX_EXCHANGE; i++)
  {
    // index
    msgToSend[currentIndex] = i;
    currentIndex += 1;
    
    // exchangeId
    msgToSend[currentIndex] = ExchangeStatusList[i].exchangeId;
    currentIndex += 1;
    
    // CQS_Status{visible + Status}
    msgToSend[currentIndex] = ExchangeStatusList[i].status[CQS_SOURCE].visible;
    msgToSend[currentIndex + 1] = ExchangeStatusList[i].status[CQS_SOURCE].enable;
    currentIndex += 2;
    
    // UQDF_Status{visible + Status}
    msgToSend[currentIndex] = ExchangeStatusList[i].status[UQDF_SOURCE].visible;
    msgToSend[currentIndex + 1] = ExchangeStatusList[i].status[UQDF_SOURCE].enable;
    currentIndex += 2;
  }
  
  // Block length
  tmpInt.value = currentIndex - 6;
  msgToSend[6] = tmpInt.c[0];
  msgToSend[7] = tmpInt.c[1];
  msgToSend[8] = tmpInt.c[2];
  msgToSend[9] = tmpInt.c[3];
  
  // Message length
  tmpInt.value = currentIndex - 6;
  msgToSend[2] = tmpInt.c[0];
  msgToSend[3] = tmpInt.c[1];
  msgToSend[4] = tmpInt.c[2];
  msgToSend[5] = tmpInt.c[3];
    
  // Send message to AS
  return SendMessageToAS(msgToSend, currentIndex);
}

/****************************************************************************
- Function name:  ProcessSetExchangeConf
- Description:
****************************************************************************/
int ProcessSetExchangeConf(char *message)
{
  int index = 10;
  
  TraceLog(DEBUG_LEVEL, "Receive message to set exchange conf from AS\n");
  
  int exchangeIndex = 0;
  int count = 0;
  while (count < MAX_EXCHANGE)
  {
    exchangeIndex = message[index];
    
    // CQS_Status{visible + Status}
    ExchangeStatusList[exchangeIndex].status[CQS_SOURCE].enable = message[index + 3];
    
    // UQDF_Status{visible + Status}
    ExchangeStatusList[exchangeIndex].status[UQDF_SOURCE].enable = message[index + 5];
    
    // Increase index
    index += 6;
    count += 1;
  }
  
  SaveExchangesConf();
  
  BuilSendExchangeConfigToAS();
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessFlushBBOQuotes
- Description:
****************************************************************************/
int ProcessFlushBBOQuotes(char *message)
{
  int exchangeIndex = message[10];
  
  TraceLog(DEBUG_LEVEL, "Receive message to flush BBO quotes from AS, exchangeIndex = %d\n", exchangeIndex);
  
  if ((exchangeIndex >= 0) && (exchangeIndex < MAX_EXCHANGE))
  { 
    FlushBBOQuotes(exchangeIndex);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SendBBOQuotesToAS
- Description:
****************************************************************************/
int SendBBOQuotesToAS(int TT_ID, char *stockSymbol)
{
  //index of symbol in Stock Book
  int stockSymbolIndex = GetStockSymbolIndex(stockSymbol);
  int isExisting = YES;
  
  if (stockSymbolIndex == -1)
  {
    TraceLog(DEBUG_LEVEL, "Stock Symbol %.8s did not exist in stock book\n", stockSymbol);
    isExisting = NO;
  }
  
  char msgToSend[26 + MAX_BBO_SOURCE * MAX_EXCHANGE * MAX_SIDE * 13];
  
  t_ShortConverter shortNum;
  t_IntConverter intNum;
  t_DoubleConverter doubleNum;
  
  int index = 0;
  
  // Message Type
  shortNum.value = MSG_TYPE_FOR_SEND_BBO_QUOTE_LIST_TO_AS;
  memcpy(&msgToSend[index], shortNum.c, 2);
  index += 2;
  
  // Body Length
  index += 4;
  
  // Body content = Block 1 + Block 2 + Block 3
  
  // Block 1 = TT and Stock symbol information
  // Block length 1
  intNum.value = 9 + 4;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // tt_id
  msgToSend[index] = TT_ID;
  index += 1;
  
  // Symbol
  strncpy(&msgToSend[index], stockSymbol, SYMBOL_LEN);
  index += SYMBOL_LEN;    
  
  char askBlock[MAX_BBO_SOURCE * MAX_EXCHANGE * 13 + 1];
  int askBlockSize = 0;
  
  char bidBlock[MAX_BBO_SOURCE * MAX_EXCHANGE * 13 + 1];
  int bidBlockSize = 0;
  
  if (isExisting == YES)
  {
    // Get current quote information of current stock symbol
    t_BBOQuoteList *symbolInfo;
    symbolInfo = &BBOQuoteMgmt[stockSymbolIndex];
    
    int sourceId, i;
    for (sourceId = 0; sourceId < MAX_BBO_SOURCE; sourceId++)
    {
      for (i = 0; i < MAX_EXCHANGE; i++)
      {       
        if (symbolInfo->BBOQuoteInfo[sourceId][i].participantId != 0)
        {
          if (symbolInfo->BBOQuoteInfo[sourceId][i].offerSize > 0)
          {
            // participantId
            askBlock[askBlockSize] = symbolInfo->BBOQuoteInfo[sourceId][i].participantId;
            askBlockSize += 1;
            
            // offerSize 
            intNum.value = symbolInfo->BBOQuoteInfo[sourceId][i].offerSize;
            memcpy(&askBlock[askBlockSize], intNum.c, 4);
            askBlockSize += 4;
            
            // offerPrice
            doubleNum.value = symbolInfo->BBOQuoteInfo[sourceId][i].offerPrice;
            memcpy(&askBlock[askBlockSize], doubleNum.c, 8);
            askBlockSize += 8;
          }
          
          if (symbolInfo->BBOQuoteInfo[sourceId][i].bidSize > 0)
          {
            // participantId
            bidBlock[bidBlockSize] = symbolInfo->BBOQuoteInfo[sourceId][i].participantId;
            bidBlockSize += 1;
            
            // bidSize
            intNum.value = symbolInfo->BBOQuoteInfo[sourceId][i].bidSize;
            memcpy(&bidBlock[bidBlockSize], intNum.c, 4);
            bidBlockSize += 4;
            
            // bidPrice
            doubleNum.value = symbolInfo->BBOQuoteInfo[sourceId][i].bidPrice;
            memcpy(&bidBlock[bidBlockSize], doubleNum.c, 8);
            bidBlockSize += 8;
          }       
        }
      }
    }
  }
  
  //Block Length 2
  intNum.value = askBlockSize + 4;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  if (askBlockSize > 0)
  {
    //ASK BLOCK
    memcpy(&msgToSend[index], askBlock, askBlockSize);
    index += askBlockSize;
  }
  
  //Block Length 3
  intNum.value = bidBlockSize + 4;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  if (bidBlockSize > 0)
  {
    memcpy(&msgToSend[index], bidBlock, bidBlockSize);
    index += bidBlockSize;
  }
  
  // Update Body Length
  intNum.value = (4 + 9) +  askBlockSize + 4 + bidBlockSize + 4;
  memcpy(&msgToSend[2], intNum.c, 4);
  
  //TraceLog(DEBUG_LEVEL, "BBO Status (%d) (%d)\n", askBlockSize / 13, bidBlockSize / 13);
  
  return SendMessageToAS(msgToSend, index);
}

/****************************************************************************
- Function name:  BuilSendBook_ISO_TradingToAS
- Description:
****************************************************************************/
int BuilSendBook_ISO_TradingToAS(void)
{
  t_ShortConverter tmpShort;
  t_IntConverter tmpInt;
  char msgToSend[2 + 4 + 4 + MAX_ECN_BOOK + MAX_ECN_BOOK + 1];
  
  int currentIndex = 0; 
  
  // Message type
  tmpShort.value = MSG_TYPE_FOR_SEND_BOOK_ISO_TRADING_TO_AS;  
  msgToSend[currentIndex] = tmpShort.c[0];
  msgToSend[currentIndex + 1] = tmpShort.c[1];
  
  currentIndex += 2;
  
  // Message length
  currentIndex += 4;
  
  // Block length
  currentIndex += 4;
  
  // Block content = Book for ISO trading
  memcpy(&msgToSend[currentIndex], &BookISO_Trading, MAX_ECN_BOOK);
  currentIndex += MAX_ECN_BOOK;
  
  // Block content = Pre/Post Algorithm
  memcpy(&msgToSend[currentIndex], &BookPrePostISOTrading, MAX_ECN_BOOK);
  currentIndex += MAX_ECN_BOOK;
  
  // Block length
  tmpInt.value = currentIndex - 6;
  msgToSend[6] = tmpInt.c[0];
  msgToSend[7] = tmpInt.c[1];
  msgToSend[8] = tmpInt.c[2];
  msgToSend[9] = tmpInt.c[3];
  
  // Message length
  tmpInt.value = currentIndex - 6;
  msgToSend[2] = tmpInt.c[0];
  msgToSend[3] = tmpInt.c[1];
  msgToSend[4] = tmpInt.c[2];
  msgToSend[5] = tmpInt.c[3];
    
  // Send message to AS
  return SendMessageToAS(msgToSend, currentIndex);
}

/****************************************************************************
- Function name:  ProcessSetStuckUnlockForASymbol
- Description:
****************************************************************************/
int ProcessSetStuckUnlockForASymbol(char *message)
{
  //symbol
  char symbol[SYMBOL_LEN];
  memcpy(symbol, &message[10], SYMBOL_LEN);
  
  int stockSymbolIndex = GetStockSymbolIndex(symbol);
  
  if (stockSymbolIndex == -1)
  {
      return SUCCESS;
  }
 
  //The number of lock in a side (positive for SHORT UNLOCK, negative for LONG UNLOCK)
  int _lockCounter = 0;
  memcpy (&_lockCounter, &message[10 + SYMBOL_LEN], 4);
  
  TradingCollection[stockSymbolIndex].lockedSide -= _lockCounter;
  
  if (TradingCollection[stockSymbolIndex].lockedSide == NO_LOCK)
  {
    TraceLog(DEBUG_LEVEL, "Stuck for symbol %.8s has been resolved, it's now unlocked\n", symbol);
    //TradingCollection[stockSymbolIndex].lockedSide = NO_LOCK;
  }
  else
  {
    if ((_lockCounter >= SHORT_LOCK) && (TradingCollection[stockSymbolIndex].lockedSide < 0))
    {
      TraceLog(DEBUG_LEVEL, "Stuck for symbol %.8s has been resolved, it's now unlocked\n", symbol);
      TradingCollection[stockSymbolIndex].lockedSide = NO_LOCK;
    }
    else if ((_lockCounter <= SHORT_LOCK) && (TradingCollection[stockSymbolIndex].lockedSide > 0))
    {
      TraceLog(DEBUG_LEVEL, "Stuck for symbol %.8s has been resolved, it's now unlocked\n", symbol);
      TradingCollection[stockSymbolIndex].lockedSide = NO_LOCK;
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessSetSymbolLockCounter
- Description:
****************************************************************************/
int ProcessSetSymbolLockCounter(char *message)
{
  //symbol
  char symbol[SYMBOL_LEN];
  memcpy(symbol, &message[10], SYMBOL_LEN);
  
  int stockSymbolIndex = GetStockSymbolIndex(symbol);
  
  if (stockSymbolIndex == -1)
  {
      return SUCCESS;
  }

  //The number of lock in a side (positive for SHORT UNLOCK, negative for LONG UNLOCK)
  int lockCounter = 0;
  memcpy (&lockCounter, &message[10 + SYMBOL_LEN], 4);
  
  int lastLockCounter = TradingCollection[stockSymbolIndex].lockedSide;
  TradingCollection[stockSymbolIndex].lockedSide = lockCounter;
  
  if ((lockCounter == NO_LOCK) && (lastLockCounter != NO_LOCK))
  {
    TraceLog(DEBUG_LEVEL, "AS has requested to unlock symbol %.8s\n", symbol);
  }
  else if ((lockCounter != NO_LOCK) && (lastLockCounter == NO_LOCK))
  {
    TraceLog(DEBUG_LEVEL, "AS has requested to lock symbol %.8s (lock counter: %d)\n", symbol, lockCounter);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessSetVolatileSymbolList
- Description:
****************************************************************************/
int ProcessSetVolatileSymbolList(char *message)
{
  char symbol[SYMBOL_LEN];
  int i, stockSymbolIndex, countActive = 0, realActive = 0;
  int offset = 10;
  char activeIndexes[MAX_STOCK_SYMBOL];
  memset (activeIndexes, 0, MAX_STOCK_SYMBOL);
  
  // Current volatile list
  memcpy(&countActive, &message[offset], 4);
  offset += 4;
  
  for (i = 0; i < countActive; i++)
  {
    memcpy(symbol, &message[offset], SYMBOL_LEN);
    offset += SYMBOL_LEN;
    
    stockSymbolIndex = GetStockSymbolIndex(symbol);
    if (stockSymbolIndex == -1)
    {
      continue;
    }
    
    if (SymbolMgmt.symbolList[stockSymbolIndex].isVolatile == NO)
    {
      SymbolMgmt.symbolList[stockSymbolIndex].isVolatile = YES;
    }
    
    if (activeIndexes[stockSymbolIndex] == 0)
    {
      activeIndexes[stockSymbolIndex] = 1;
      realActive++;
    }
  }
  
  // Volatile-Deactivate remaining symbols
  int countRemoved = 0;
  for (stockSymbolIndex = 0; stockSymbolIndex < MAX_STOCK_SYMBOL; stockSymbolIndex++)
  {
    if (SymbolMgmt.symbolList[stockSymbolIndex].isVolatile == YES)
    {
      if (activeIndexes[stockSymbolIndex] == 0)
      {
        SymbolMgmt.symbolList[stockSymbolIndex].isVolatile = NO;
        countRemoved++;
      }
    }
  }
  
  TraceLog(DEBUG_LEVEL, "Received volatile symbol list from AS: %d symbol(s), activated by TS %d, deactivated by TS %d).\n", countActive, realActive, countRemoved);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessSetAllVolatileToggle
- Description:
****************************************************************************/
int ProcessSetAllVolatileToggle(char *message)
{
  int allVolatileValue = message[10];
  TraceLog(DEBUG_LEVEL, "Received All-Volatile %s toggle from AS\n", allVolatileValue == 1 ? "ON" : "OFF");
  if (AllVolatileStatus != allVolatileValue)
  {
    AllVolatileStatus = allVolatileValue;
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessSetEtbSymbolList
- Input:      
- Output:     
- Return:     
- Description:    
- Usage:      
****************************************************************************/
int ProcessSetEtbSymbolList(char *message)
{
  //--------------------------------------------------------------------------
  //    Message structure
  //    + Message type      2 bytes
  //    + Body Length       4 bytes
  //    + Block Length      4 bytes
  //    + Number of Symbol  4 bytes
  //    + ETB Symbol List
  //      ++ symbol         8 bytes
  //      ++ action         1 byte  0 - remove from ETB List
  //                                1 - add to ETB List
  //--------------------------------------------------------------------------
  int offset = 10;
  
  // Get Number of Symbol
  int delta = *(int*)(&message[offset]);
  offset += 4;
  
  int total = *(int*)(&message[offset]);
  offset += 4;
  TraceLog(DEBUG_LEVEL, "Received ETB Symbol List, total symbols:%d, new symbols since previous refresh: %d\n", total, delta);
  
  char symbol[SYMBOL_LEN];
  int i, stockSymbolIndex, action;
  
  if (IsReceivedEtbList == NO)
  {
    IsReceivedEtbList = YES;
    
    //Reset ssEnable when connection between TS and AS is established
    for (i = 0; i < SymbolMgmt.nextSymbolIndex; i++)
    {
      SymbolMgmt.symbolList[i].shortSaleStatus &= ~SHORT_SALE_ENABLE_BIT_FLAG;
    }
  }

  int oldValue = -1, newValue = -1;

  for (i = 0; i < delta; i++)
  {
    memcpy(symbol, &message[offset], SYMBOL_LEN);
    offset += SYMBOL_LEN;
    
    action = message[offset];
    offset += 1;
    
    stockSymbolIndex = GetStockSymbolIndex(symbol);
    
    if (stockSymbolIndex == -1)
    {
      continue;
    }

    oldValue = SymbolMgmt.symbolList[stockSymbolIndex].shortSaleStatus;

    if (action == 0)
    {
      //Remove symbol from ETB List
      __sync_fetch_and_and (&SymbolMgmt.symbolList[stockSymbolIndex].shortSaleStatus, ~SHORT_SALE_ENABLE_BIT_FLAG);
      newValue = SymbolMgmt.symbolList[stockSymbolIndex].shortSaleStatus;
    }
    else if (action == 1)
    {
      //Add symbol to ETB List
      __sync_fetch_and_or (&SymbolMgmt.symbolList[stockSymbolIndex].shortSaleStatus, SHORT_SALE_ENABLE_BIT_FLAG);
      newValue = SymbolMgmt.symbolList[stockSymbolIndex].shortSaleStatus;
    }

    if (oldValue != newValue)
      DEVLOGF("ProcessSetEtbSymbolList: symbol %.8s, old value: %d, new value: %d", symbol, oldValue, newValue);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTradingAccountList
- Description:
****************************************************************************/
int ProcessTradingAccountList(char *message)
{
  // int accountType = *(int*)(&message[10]);
  int accountType = message[10];
  char accountName[16] = "\0";
  strncpy(accountName, &message[11], 16);
  TraceLog(DEBUG_LEVEL, "Received Trading Account Info: accountType=%d, accountName='%.16s'\n", accountType, accountName);
  
  if (strncmp(TradingAccount.ArcaDirectAccount, accountName, 16) != 0)
  {
    TraceLog(WARN_LEVEL, "Trading Account is different between TS (%s) and AS (%.16s)\n",
        TradingAccount.ArcaDirectAccount, accountName);
    return ERROR;
  }
  
  //Trading Account is parsed from command line
  if (TradingAccount.accountType == -1)
  {
    TradingAccount.accountType = accountType;
    TraceLog(DEBUG_LEVEL, "Update Trading Account Type %d for TS\n", TradingAccount.accountType);
    return SUCCESS;
  }
  
  //Account Type is loaded from raw data or from configuration file, compare with AS
  if (TradingAccount.accountType != accountType)
  {
    TraceLog(WARN_LEVEL, "Trading Account Type is different between TS (%d) and AS (%d)\n",
        TradingAccount.accountType, accountType);
    return ERROR;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessSetStuckForSymbolList
- Description:
****************************************************************************/
int ProcessSetStuckForSymbolList(char *message)
{
  // All symbol in the list are having stuck, so must lock them, do not trade them
  int count = 0;
  memcpy(&count, &message[10], 4);
  if (count < 1)
  {
    TraceLog(ERROR_LEVEL, "Received stuck symbol list from AS, but the number of symbols in list is %d\n", count);
    return ERROR;
  }
  
  TraceLog(DEBUG_LEVEL, "Received stuck symbol list from AS, TS will lock the following symbols: ");
  
  // Get all symbol from list
  int iCount = 0;
  int _lockedSide = NO_LOCK;
  char symbol[SYMBOL_LEN];
  for (iCount = 0; iCount < count; iCount++)
  {
    strncpy(symbol, &message[14 + (iCount * (SYMBOL_LEN + 4))], SYMBOL_LEN);
    _lockedSide = message[14 + (iCount + 1) * SYMBOL_LEN + (iCount * 4)];
    TraceLog(NO_LOG_LEVEL, "%.8s (%s); ", symbol, (_lockedSide >= SHORT_LOCK)?"SHORT Lock":"LONG Lock");
    
    int stockSymbolIndex = GetStockSymbolIndex(symbol);
  
    if (stockSymbolIndex == -1)
    {
      continue;
    }
    
    TradingCollection[stockSymbolIndex].lockedSide = _lockedSide;
  }
  TraceLog(NO_LOG_LEVEL, "\n");
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessSetBook_ISO_Trading
- Description:
****************************************************************************/
int ProcessSetBook_ISO_Trading(char *message)
{
  int index = 10;
  
  TraceLog(DEBUG_LEVEL, "Receive message to set Book role/Pre-Post settings for ISO algorithm from AS\n");
  
  memcpy(&BookISO_Trading, &message[index], MAX_ECN_BOOK);
  index += MAX_ECN_BOOK;
  
  memcpy(&BookPrePostISOTrading, &message[index], MAX_ECN_BOOK);
  
  SaveISO_Algorithm();
  
  BuilSendBook_ISO_TradingToAS();
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  InitializeIgnoreStockRangesList
- Description:
****************************************************************************/

int InitializeIgnoreStockRangesList()
{
  IgnoreStockRangesList.countStock = 0;
  memset(IgnoreStockRangesList.stockList, 0, MAX_IGNORE_SYMBOL * SYMBOL_LEN);
  return SUCCESS;
}

/****************************************************************************
- Function name:  BuildSendServiceDisconnect
- Description:
****************************************************************************/
int BuildSendServiceDisconnect(short serviceIndex)
{
  //--------------------------------------------------------------------------
  //            Message structure
  //    + Message type    2 bytes
  //    + Body Length   4 bytes
  //    + Block Length    4 bytes
  //    + Service Index   2 bytes
  //--------------------------------------------------------------------------

  int msgLen = MSG_HEADER_LEN + 4 + 2;
  char msgToSend[msgLen];
  memset(msgToSend, 0, msgLen);
  
  t_ShortConverter shortNum;
  t_IntConverter intNum;
  int index = 0;
  
  // Message Type
  shortNum.value = TS_RES_SERVICE_DISCONNECT;
  memcpy(&msgToSend[index], shortNum.c, 2);
  index += 2;
  
  // Body Length
  intNum.value = msgLen - MSG_HEADER_LEN;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // Block Length
  intNum.value = msgLen - MSG_HEADER_LEN;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  //Service Index
  shortNum.value = serviceIndex;
  memcpy(&msgToSend[index], shortNum.c, 2);
  index += 2;
    
  return SendMessageToAS(msgToSend, index);;
}

/****************************************************************************
- Function name:  BuildSendTradingDisabled
- Description:
****************************************************************************/
int BuildSendTradingDisabled(short serviceIndex, short reasonCode)
{
  //--------------------------------------------------------------------------
  //    Message structure
  //    + Message type    2 bytes
  //    + Body Length     4 bytes
  //    + Block Length    4 bytes
  //    + Service Index   2 bytes
  //    + Reason Code     2 bytes
  //--------------------------------------------------------------------------
  
  int msgLen = MSG_HEADER_LEN + 4 + 2 + 2;
  char msgToSend[msgLen];
  memset(msgToSend, 0, msgLen);
  
  t_ShortConverter shortNum;
  t_IntConverter intNum;
  int index = 0;
  
  // Message Type
  shortNum.value = TS_RES_TRADING_DISABLE;
  memcpy(&msgToSend[index], shortNum.c, 2);
  index += 2;
  
  // Body Length
  intNum.value = msgLen - MSG_HEADER_LEN;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // Block Length
  intNum.value = msgLen - MSG_HEADER_LEN;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  //Service Index
  shortNum.value = serviceIndex;
  memcpy(&msgToSend[index], shortNum.c, 2);
  index += 2;
  
  //Reason Code
  shortNum.value = reasonCode;
  memcpy(&msgToSend[index], shortNum.c, 2);
  index += 2;
  
  return SendMessageToAS(msgToSend, index);
}

/****************************************************************************
- Function name:  BuildSendStaleMarketNotification
- Description:
****************************************************************************/
int BuildSendStaleMarketNotification(int ecnIndex, int stockSymbolIndex, long delay)
{
  if (ASConfig.socket == -1) return SUCCESS;
  //--------------------------------------------------------------------------
  //    Message structure
  //    + Message type    2 bytes
  //    + Body Length     4 bytes
  //    + Block Length    4 bytes
  //    + ECN Index       1 byte
  //    + Symbol          8 byte
  //    + Delay           8 byte
  //    + Status flag     1 byte (0 is normal, 1 is stale)
  //--------------------------------------------------------------------------
  
  int msgLen = 10 + 1 + 8 + 8 + 1;
  char msgToSend[msgLen];
  memset(msgToSend, 0, msgLen);
  
  t_ShortConverter shortNum;
  t_IntConverter intNum;
  int index = 0;
  
  // Message Type
  shortNum.value = TS_RES_STALE_MARKET_DATA;
  memcpy(&msgToSend[index], shortNum.c, 2);
  index += 2;
  
  // Body Length
  intNum.value = msgLen - MSG_HEADER_LEN;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // Block Length
  intNum.value = msgLen - MSG_HEADER_LEN;
  memcpy(&msgToSend[index], intNum.c, 4);
  index += 4;
  
  // ECN Index
  msgToSend[index] = ecnIndex;
  index += 1;
  
  // Symbol
  memcpy(&msgToSend[index], SymbolMgmt.symbolList[stockSymbolIndex].symbol, SYMBOL_LEN);
  index += SYMBOL_LEN;
  
  // Delay
  memcpy(&msgToSend[index], &delay, 8);
  index += 8;
  
  // Status flag
  msgToSend[index] = SymbolMgmt.symbolList[stockSymbolIndex].isStale[ecnIndex];
  index += 1;
  
  return SendMessageToAS(msgToSend, index);
}

/****************************************************************************
- Function name:  SendHaltStockListToAS
- Description:
****************************************************************************/
int SendHaltStockListToAS(void)
{
  //--------------------------------------------------------------------------
  //    Message structure
  //    + Message type            2 bytes
  //    + Body Length             4 bytes
  //    + Block Length            4 bytes
  //    + Number of symbol        4 bytes
  //    + Halt status for each symbol:
  //      + 1st Symbol            8 bytes (SYMBOL_LEN)
  //        - ARCA Halt           1 byte binary
  //        - ARCA update time    4 bytes int
  //        - NASDAQ Halt         1 byte binary
  //        - NASDAQ update time  4 bytes int
  //        - .... other books
  //      + 2nd Symbol
  //        - ....
  //        - ....
  //      + nth symbol
  //        - .....
  //      
  //--------------------------------------------------------------------------
  
  char msg[14 + MAX_STOCK_SYMBOL *(SYMBOL_LEN + MAX_ECN_BOOK + MAX_ECN_BOOK*4)];
  int numSymbol = 0;
  int i, j;
  int offset = 14;
  int backupIndex[MAX_STOCK_SYMBOL];
  
  // Collect symbols' statuses
  for (i = 0; i < SymbolMgmt.nextSymbolIndex; i++)
  {
    if (SymbolMgmt.symbolList[i].haltStatusSent == 0)
    {
      backupIndex[numSymbol++] = i;
      
      memcpy (&msg[offset], SymbolMgmt.symbolList[i].symbol, SYMBOL_LEN);
      offset += SYMBOL_LEN;
      
      for (j = 0; j < MAX_ECN_BOOK; j++)
      {
        msg[offset++] = SymbolMgmt.symbolList[i].haltStatus[j];
        
        memcpy(&msg[offset], &SymbolMgmt.symbolList[i].haltTime[j], 4);
        offset += 4;
      }
    }
  }
  
  if (numSymbol == 0) return SUCCESS; //There is nothing to send
  
  t_ShortConverter shortNum;
  t_IntConverter intNum;
  
  // Message Type
  shortNum.value = TS_RES_SYMBOL_HALT_STATUS;
  memcpy(&msg[0], shortNum.c, 2);
  
  // Body Length
  intNum.value = offset - MSG_HEADER_LEN;
  memcpy(&msg[2], intNum.c, 4);
  
  // Block Length
  intNum.value = offset - MSG_HEADER_LEN;
  memcpy(&msg[6], intNum.c, 4);
  
  // Number of symbol
  intNum.value = numSymbol;
  memcpy(&msg[10], intNum.c, 4);
    
  if (SendMessageToAS(msg, offset) == SUCCESS)
  {
    for (i = 0; i < numSymbol; i++)
    {
      SymbolMgmt.symbolList[backupIndex[i]].haltStatusSent = 1;
    }
    return SUCCESS;
  }
  return ERROR;
}

/****************************************************************************
- Function name:  ProcessHungOrderReset
- Description:    
- Usage:      
****************************************************************************/
int ProcessHungOrderReset(char *message)
{
  int orderId;
  memcpy(&orderId, &message[10], 4);
  
  //Get Stock Symbol
  char stockSymbol[SYMBOL_LEN];
  strncpy(stockSymbol, &message[14], SYMBOL_LEN);
  
  ResetStatusForHungOrder(orderId, stockSymbol);
  
  return SUCCESS;
}
