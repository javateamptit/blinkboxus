/****************************************************************************
** Project name:  Equity Arbitrage Application
** Filename:    batsz_book_proc.c
** Description:   BATS-Z Multicast PITCH
** Author:      Luan Vo Kinh
** First created on 10 July 2009
** Last updated on 10 July 2009
****************************************************************************/
#define _BSD_SOURCE

#include <pthread.h>
#include <time.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <math.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>

#include "utility.h"
#include "bats_book_proc.h"
#include "configuration.h"
#include "book_mgmt.h"
#include "trading_mgmt.h"
#include "kernel_algorithm.h"
#include "hbitime.h"

#define BATSZ_GAP_LOGIN_MSG_LEN 30
#define BATSZ_GAP_REQUEST_MSG_LEN 17

/****************************************************************************
              GLOBAL VARIABLES
****************************************************************************/
t_BATS_Book_Conf BATSZ_Book_Conf;
t_BATS_Book_Conf BYX_Book_Conf;

extern t_MonitorBatszBook MonitorBatszBook;
extern t_MonitorBYXBook MonitorBYXBook;

/****************************************************************************
              LIST OF FUNCTIONS
****************************************************************************/

/****************************************************************************
- Function name:  InitBATSZ_BOOK_DataStructure
****************************************************************************/
void InitBATSZ_BOOK_DataStructure(void)
{ 
  TraceLog(DEBUG_LEVEL, "Initialize BATS-Z Book ...\n");
  
  int i, j;
  MonitorBatszBook.procCounter = 0;
  
  for (i = 0; i < MAX_BATSZ_UNIT; i++)
  {
    MonitorBatszBook.recvCounter[i] = 0;
    BATSZ_Book_Conf.group[i].unitId = i+1;
    BATSZ_Book_Conf.group[i].groupPermission = SymbolSplitInfo[AlgorithmConfig.splitConfig.symbolRangeID].batsz[i];
    
    for (j = 0; j < DBLFILTER_NUM_QUEUES; j++)
      BATSZ_Book_Conf.group[i].lossCounter[j] = 0;
  }
  
  int stockSymbolIndex;
  for (stockSymbolIndex = 0; stockSymbolIndex < MAX_STOCK_SYMBOL; stockSymbolIndex++)
  {
    for (j = 0; j < PRICE_HASHMAP_BUCKET_SIZE; j++)
    {     
      DeleteAllPricePointsFromHashSlot(Price_HashMap[stockSymbolIndex][BOOK_BATSZ][ASK_SIDE][j]);
      Price_HashMap[stockSymbolIndex][BOOK_BATSZ][ASK_SIDE][j] = NULL;
      
      DeleteAllPricePointsFromHashSlot(Price_HashMap[stockSymbolIndex][BOOK_BATSZ][BID_SIDE][j]);
      Price_HashMap[stockSymbolIndex][BOOK_BATSZ][BID_SIDE][j] = NULL;
    }
  }
}

/****************************************************************************
- Function name:  InitBYX_BOOK_DataStructure
****************************************************************************/
void InitBYX_BOOK_DataStructure(void)
{ 
  TraceLog(DEBUG_LEVEL, "Initialize BYX Book ...\n");
  
  int i, j;
  MonitorBYXBook.procCounter = 0;
  
  for (i = 0; i < MAX_BYX_UNIT; i++)
  {
    MonitorBYXBook.recvCounter[i] = 0;
    BYX_Book_Conf.group[i].unitId = i+1;
    BYX_Book_Conf.group[i].groupPermission = SymbolSplitInfo[AlgorithmConfig.splitConfig.symbolRangeID].byx[i];
    
    for (j = 0; j < DBLFILTER_NUM_QUEUES; j++)
      BYX_Book_Conf.group[i].lossCounter[j] = 0;
  }
  
  int stockSymbolIndex;
  for (stockSymbolIndex = 0; stockSymbolIndex < MAX_STOCK_SYMBOL; stockSymbolIndex++)
  {
    for (j = 0; j < PRICE_HASHMAP_BUCKET_SIZE; j++)
    {     
      DeleteAllPricePointsFromHashSlot(Price_HashMap[stockSymbolIndex][BOOK_BYX][ASK_SIDE][j]);
      Price_HashMap[stockSymbolIndex][BOOK_BYX][ASK_SIDE][j] = NULL;
      
      DeleteAllPricePointsFromHashSlot(Price_HashMap[stockSymbolIndex][BOOK_BYX][BID_SIDE][j]);
      Price_HashMap[stockSymbolIndex][BOOK_BYX][BID_SIDE][j] = NULL;
    }
  }
}

/****************************************************************************
- Function name:  BATS_ProcessBookMessage
****************************************************************************/
int BATS_ProcessBookMessage(unsigned char ecn_book_id, uint32_t symbolId, char *msg, unsigned long arrivalTime, unsigned int timeStatusSeconds)
{
  if (BookStatusMgmt[ecn_book_id].isConnected == DISCONNECTED) return SUCCESS; //If book is disconnected, we do not process remaining message in the buffer
    
  switch ((unsigned char)msg[1])
  {
    /*
    0x21  Add Order - Long
    0x22  Add Order - Short
    0x2F  Add Order - Expanded
    0x23  Order Executed
    0x24  Order Executed at Price/Size
    0x25  Reduce Size - Long
    0x26  Reduce Size - Short
    0x27  Modify Order - Long
    0x28  Modify Order - Short
    0x29  Delete Order
    */
    case BATS_MSG_TYPE_ADD_ORDER_SHORT:
      if (ProcessAddOrderMessageShort_BATS(ecn_book_id, symbolId, msg, arrivalTime, timeStatusSeconds) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "%s Book: Problem in processing 'Add Order Short Form' msg\n", GetBookNameByIndex(ecn_book_id));
        return ERROR;
      }
      break;
      
    case BATS_MSG_TYPE_ADD_ORDER_LONG:
      if (ProcessAddOrderMessageLong_BATS(ecn_book_id, symbolId, msg, arrivalTime, timeStatusSeconds) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "%s Book: Problem in processing 'Add Order Long Form' msg\n", GetBookNameByIndex(ecn_book_id));
        return ERROR;
      }
      break;
      
    case BATS_MSG_TYPE_ADD_ORDER_EXPANDED:
      if (ProcessAddOrderMessageExpanded_BATS(ecn_book_id, symbolId, msg, arrivalTime, timeStatusSeconds) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "%s Book: Problem in processing 'Add Order Expanded Form' msg\n", GetBookNameByIndex(ecn_book_id));
        return ERROR;
      }
      break;
      
    case BATS_MSG_TYPE_ORDER_EXECUTED:
      if (ProcessOrderExecutedMessage_BATS(ecn_book_id, symbolId, msg, arrivalTime, timeStatusSeconds) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "%s Book: Problem in processing 'Order Executed' msg\n", GetBookNameByIndex(ecn_book_id));
        return ERROR;
      }
      break;
      
    case BATS_MSG_TYPE_ORDER_EXECUTED_AT_PRICE:
      if (ProcessOrderExecutedAtPriceMessage_BATS(ecn_book_id, symbolId, msg, arrivalTime, timeStatusSeconds) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "%s Book: Problem in processing 'Order Executed At Price' msg\n", GetBookNameByIndex(ecn_book_id));
        return ERROR;
      }
      break;
      
    case BATS_MSG_TYPE_DELETE_ORDER:
      if (ProcessDeleteOrderMessage_BATS(ecn_book_id, symbolId, msg, arrivalTime, timeStatusSeconds) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "%s Book: Problem in processing 'Delete Order' msg\n", GetBookNameByIndex(ecn_book_id));
        return ERROR;
      }
      break;
      
    case BATS_MSG_TYPE_MODIFY_ORDER_SHORT:
      if (ProcessModifyOrderMessageShort_BATS(ecn_book_id, symbolId, msg, arrivalTime, timeStatusSeconds) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "%s Book: Problem in processing 'Modify Order Short Form' msg\n", GetBookNameByIndex(ecn_book_id));
        return ERROR;
      }
      break;
      
    case BATS_MSG_TYPE_MODIFY_ORDER_LONG:
      if (ProcessModifyOrderMessageLong_BATS(ecn_book_id, symbolId, msg, arrivalTime, timeStatusSeconds) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "%s Book: Problem in processing 'Modify Order Long Form' msg\n", GetBookNameByIndex(ecn_book_id));
        return ERROR;
      }
      break;
    
    case BATS_MSG_TYPE_REDUCE_SIZE_SHORT:
      if (ProcessReduceSizeMessageShort_BATS(ecn_book_id, symbolId, msg, arrivalTime, timeStatusSeconds) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "%s Book: Problem in processing 'Reduce Size Short Form' msg\n", GetBookNameByIndex(ecn_book_id));
        return ERROR;
      }
      break;
      
    case BATS_MSG_TYPE_REDUCE_SIZE_LONG:
      if (ProcessReduceSizeMessageLong_BATS(ecn_book_id, symbolId, msg, arrivalTime, timeStatusSeconds) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "%s Book: Problem in processing 'Reduce Size Long Form' msg\n", GetBookNameByIndex(ecn_book_id));
        return ERROR;
      }
      break;
    /*
    0x20  Time
    0x2A  Trade - Long
    0x2B  Trade - Short
    0x30  Trade - Expanded
    0x2C  Trade Break
    0x2D  End of Session
    0x31  Trading Status (Equities Only)
    0x95  Auction Update (BATS-Z Exchange Only)
    0x96  Auction Summary (BATS-Z Exchange Only)
    0x97  Unit Clear
    */
    case BATS_MSG_TYPE_TIME:
      //TraceLog(DEBUG_LEVEL, "%s Book: Time %d at unit %d\n", GetBookNameByIndex(ecn_book_id), *(int*)(&msg[2]), workingGroup->unitId);
      break;
    case BATS_MSG_TYPE_TRADE_LONG:
    case BATS_MSG_TYPE_TRADE_SHORT:
    case BATS_MSG_TYPE_TRADE_EXPANDED:
      UpdateHaltStatusForASymbol(symbolId, TRADING_NORMAL, arrivalTime/TEN_RAISE_TO_9, ecn_book_id);
      break;
    case BATS_MSG_TYPE_TRADE_BREAK:
      break;
    case BATS_MSG_TYPE_END_OF_SESSION:
      TraceLog(DEBUG_LEVEL, "%s Book: Received End of Session message\n", GetBookNameByIndex(ecn_book_id));
      break;
    case BATS_MSG_TYPE_TRADING_STATUS:
      if (ProcessTradingStatusMessage_BATS(ecn_book_id, symbolId, msg, arrivalTime) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "%s Book: Problem in processing Trading Status msg\n", GetBookNameByIndex(ecn_book_id));
        return ERROR;
      }
      break;
    case BATS_MSG_TYPE_AUCTION_UPDATE:
      //TraceLog(DEBUG_LEVEL, "%s Book: Unit %d - Received Auction Update message\n", GetBookNameByIndex(ecn_book_id), workingGroup->unitId);
      break;
    case BATS_MSG_TYPE_AUCTION_SUMMARY:
      //TraceLog(DEBUG_LEVEL, "%s Book: Unit %d - Received Auction Summary message\n", GetBookNameByIndex(ecn_book_id), workingGroup->unitId);
      break;
    case BATS_MSG_TYPE_UNIT_CLEAR:
      //TraceLog(ERROR_LEVEL, "%s Book: Receive Unit Clear msg (unexpected)\n", GetBookNameByIndex(ecn_book_id));
      ProcessUnitClearMessage_BATS(ecn_book_id, symbolId);
      break;
      
    case BYX_MSG_TYPE_RETAILS_PRICE_IMPROVEMENT:
      // this message only exist in BATSY
      break;
    
    // Invalid message type
    default:
      TraceLog(WARN_LEVEL, "%s Book: Unit - Received invalid msg type(0x%02X)\n", GetBookNameByIndex(ecn_book_id), (unsigned char)msg[1]);
      return ERROR;
  }
    
  return SUCCESS;
}

/****************************************************************************
- Function name:  CheckStaleData_BATS
****************************************************************************/
int CheckStaleData_BATS(unsigned char ecn_book_id, char *buffer, int stockSymbolIndex, unsigned long arrivalTime, unsigned int timeStatusSeconds)
{
  unsigned int msgNanos = *(unsigned int*)(&buffer[2]);
  unsigned long arrivalNanos = arrivalTime % NANOSECOND_PER_DAY;
  unsigned long batsNanos = (timeStatusSeconds + TotalTimeOffsetInSeconds) * TEN_RAISE_TO_9 + msgNanos;
  long delay = arrivalNanos - batsNanos;
  
  if (delay > DataDelayThreshold[ecn_book_id])
  {
    if (SymbolMgmt.symbolList[stockSymbolIndex].isStale[ecn_book_id] == 0)
    {
      SymbolMgmt.symbolList[stockSymbolIndex].isStale[ecn_book_id] = 1;
      BuildSendStaleMarketNotification(ecn_book_id, stockSymbolIndex, delay);
      TraceLog(WARN_LEVEL, "%s Book: '%.8s' data is delayed for %ld nanoseconds\n", GetBookNameByIndex(ecn_book_id), SymbolMgmt.symbolList[stockSymbolIndex].symbol, delay);
    }
  }
  else
  {
    if (SymbolMgmt.symbolList[stockSymbolIndex].isStale[ecn_book_id] == 0) return SUCCESS;
    
    if (SymbolMgmt.symbolList[stockSymbolIndex].isStale[ecn_book_id] == 1)
    {
      SymbolMgmt.symbolList[stockSymbolIndex].isStale[ecn_book_id] = 0;
      BuildSendStaleMarketNotification(ecn_book_id, stockSymbolIndex, delay);
      TraceLog(WARN_LEVEL, "%s Book: '%.8s' data is fast again\n", GetBookNameByIndex(ecn_book_id), SymbolMgmt.symbolList[stockSymbolIndex].symbol);
    }
    else
    {
      SymbolMgmt.symbolList[stockSymbolIndex].isStale[ecn_book_id] = 0;
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessAddOrderMessageShort_BATS
****************************************************************************/
int ProcessAddOrderMessageShort_BATS(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime, unsigned int timeStatusSeconds)
{
  /*
  Order Id        6   8   Binary
  Side Indicator  14  1   Alphanumeric
  Shares          15  2   Binary
  Stock Symbol    17  6   Alphanumeric
  Price           23  2   Binary Short Price
  Add Flags       25  1   Bit Field
  */

  CheckStaleData_BATS(ecn_book_id, buffer, symbolId, arrivalTime, timeStatusSeconds);
  
  t_OrderDetail BATS_Order;
  
  // Order Id
  BATS_Order.refNum = *(unsigned long*)(&buffer[6]);

  // Get shares
  BATS_Order.shareVolume = *(unsigned short*)(&buffer[15]);
  
  // Get Share Price
  BATS_Order.sharePrice = (*(unsigned short*)(&buffer[23])) * 0.01;
    
  int side = (buffer[14] == 'B')?BID_SIDE:ASK_SIDE;
  
  if (SymbolMgmt.symbolList[symbolId].haltStatus[ecn_book_id] == TRADING_NORMAL)
  {
    // Check cross trade
    int tradeRet = DetectCrossTrade(&BATS_Order, side, symbolId, ecn_book_id, arrivalTime/1000, REASON_CROSS_ENTRY);
    
    if (tradeRet == TRADE_DETECTED_SELF_CROSSED)
    {
      DeleteFromPrice(BATS_Order.sharePrice, ecn_book_id, !side, symbolId);
    }
  }
  
  t_QuoteNode *retNode = InsertQuote(ecn_book_id, side, symbolId, &BATS_Order);
  if (retNode)
  {
    OrderMapInsert(OrderMaps[ecn_book_id][_consumerId], BATS_Order.refNum, retNode, (side == BID_SIDE)?'B':'S'); 
  }
  else return ERROR;
  
  CheckBookCrossToEnableSymbol(symbolId);
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessAddOrderMessageLong_BATS
****************************************************************************/
int ProcessAddOrderMessageLong_BATS(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime, unsigned int timeStatusSeconds)
{
  /*
  Order Id        6   8 Binary
  Side Indicator  14  1 Alphanumeric
  Shares          15  4 Binary
  Stock Symbol    19  6 Alphanumeric
  Price           25  8 Binary Long Price
  Add Flags       33  1 Bit Field
  */
  
  CheckStaleData_BATS(ecn_book_id, buffer, symbolId, arrivalTime, timeStatusSeconds);
  
  t_OrderDetail BATS_Order;
  
  // Order Id
  BATS_Order.refNum = *(unsigned long*)(&buffer[6]);
  
  // Get shares
  BATS_Order.shareVolume = *(int*)(&buffer[15]);
  
  // Get Share Price
  BATS_Order.sharePrice = (*(unsigned long*)(&buffer[25])) * 0.0001;
    
  int side = (buffer[14] == 'B')?BID_SIDE:ASK_SIDE;

  if (SymbolMgmt.symbolList[symbolId].haltStatus[ecn_book_id] == TRADING_NORMAL)
  {
    // Check cross trade
    int tradeRet = DetectCrossTrade(&BATS_Order, side, symbolId, ecn_book_id, arrivalTime/1000, REASON_CROSS_ENTRY);
    
    if (tradeRet == TRADE_DETECTED_SELF_CROSSED)
    {
      DeleteFromPrice(BATS_Order.sharePrice, ecn_book_id, !side, symbolId);
    }
  }
  
  t_QuoteNode *retNode = InsertQuote(ecn_book_id, side, symbolId, &BATS_Order);
    
  if (retNode)
  {
    OrderMapInsert(OrderMaps[ecn_book_id][_consumerId], BATS_Order.refNum, retNode, (side == BID_SIDE)?'B':'S');
  }
  else return ERROR;
  
  CheckBookCrossToEnableSymbol(symbolId);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessAddOrderMessageExpanded_BATS
****************************************************************************/
int ProcessAddOrderMessageExpanded_BATS(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime, unsigned int timeStatusSeconds)
{
  /*
  Order Id        6   8 Binary
  Side Indicator  14  1 Alphanumeric
  Shares          15  4 Binary
  Stock Symbol    19  8 Alphanumeric padded by space on the right!
  Price           27  8 Binary Long Price
  Add Flags       35  1 Bit Field
  ParticipantID   36  4 Alphanumeric
  */
  
  CheckStaleData_BATS(ecn_book_id, buffer, symbolId, arrivalTime, timeStatusSeconds);
  
  t_OrderDetail BATS_Order;
  
  // Order Id
  BATS_Order.refNum = *(unsigned long*)(&buffer[6]);
 
  // Get shares
  BATS_Order.shareVolume = *(int*)(&buffer[15]);
  
  // Get Share Price
  BATS_Order.sharePrice = (*(unsigned long*)(&buffer[27])) * 0.0001;
    
  int side = (buffer[14] == 'B')?BID_SIDE:ASK_SIDE;

  if (SymbolMgmt.symbolList[symbolId].haltStatus[ecn_book_id] == TRADING_NORMAL)
  {
    // Check cross trade
    int tradeRet = DetectCrossTrade(&BATS_Order, side, symbolId, ecn_book_id, arrivalTime/1000, REASON_CROSS_ENTRY);
    
    if (tradeRet == TRADE_DETECTED_SELF_CROSSED)
    {
      DeleteFromPrice(BATS_Order.sharePrice, ecn_book_id, !side, symbolId);
    }
  }
  
  t_QuoteNode *retNode = InsertQuote(ecn_book_id, side, symbolId, &BATS_Order);
  if (retNode)
  {
    OrderMapInsert(OrderMaps[ecn_book_id][_consumerId], BATS_Order.refNum, retNode, (side == BID_SIDE)?'B':'S');
  }
  else
    return ERROR;
  
  CheckBookCrossToEnableSymbol(symbolId);
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessOrderExecutedMessage_BATS
****************************************************************************/
int ProcessOrderExecutedMessage_BATS(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime, unsigned int timeStatusSeconds)
{
  /*
  Order Id          6   8 Binary
  Executed Shares   14  4 Binary
  Execution Id      18  8 Binary  
  */
  CheckStaleData_BATS(ecn_book_id, buffer, symbolId, arrivalTime, timeStatusSeconds);
  
  unsigned long refNumber = *(unsigned long*)(&buffer[6]);
  
  t_QuoteNodeInfo nodeInfo = OrderMapRemove(OrderMaps[ecn_book_id][_consumerId], refNumber);
  
  if (nodeInfo.node != NULL && nodeInfo.node != DeletedQuotePlaceholder)
  {
    int side = (nodeInfo.side == 'S')?ASK_SIDE:BID_SIDE;
    int execShares = *(int*)(&buffer[14]);
    if (execShares >= nodeInfo.node->info.shareVolume)
    {
      // Delete old node
      DeleteQuote(nodeInfo.node, ecn_book_id, side, symbolId);
      CheckBookCrossToEnableSymbol(symbolId);
    }
    else
    {
      OrderMapInsert(OrderMaps[ecn_book_id][_consumerId], refNumber, nodeInfo.node, nodeInfo.side);
      ModifyQuote(nodeInfo.node, nodeInfo.node->info.shareVolume - execShares, side, symbolId);
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessOrderExecutedAtPriceMessage_BATS
****************************************************************************/
int ProcessOrderExecutedAtPriceMessage_BATS(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime, unsigned int timeStatusSeconds)
{
  /*
  Order Id            6   8 Binary
  Executed Shares     14  4 Binary
  Remaining Shares    18  4 Binary
  Execution Id        22  8 Binary
  Price               30  8 Binary Long Price
  */
  
  CheckStaleData_BATS(ecn_book_id, buffer, symbolId, arrivalTime, timeStatusSeconds);
  
  unsigned long refNumber = *(unsigned long*)(&buffer[6]);
  
  t_QuoteNodeInfo nodeInfo = OrderMapRemove(OrderMaps[ecn_book_id][_consumerId], refNumber);
  
  if (nodeInfo.node != NULL && nodeInfo.node != DeletedQuotePlaceholder)
  {
    int remainingShares = *(int*)(&buffer[18]);
    int side = (nodeInfo.side == 'S')?ASK_SIDE:BID_SIDE;
    if (remainingShares <= 0)
    {
      // Delete old node
      DeleteQuote(nodeInfo.node, ecn_book_id, side, symbolId);
      CheckBookCrossToEnableSymbol(symbolId);
    }
    else
    {
      OrderMapInsert(OrderMaps[ecn_book_id][_consumerId], refNumber, nodeInfo.node, nodeInfo.side);
      ModifyQuote(nodeInfo.node, remainingShares, side, symbolId);
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessDeleteOrderMessage_BATS
****************************************************************************/
int ProcessDeleteOrderMessage_BATS(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime, unsigned int timeStatusSeconds)
{
  /*
  Order Id  6 8 Binary
  */  
  
  CheckStaleData_BATS(ecn_book_id, buffer, symbolId, arrivalTime, timeStatusSeconds);
  
  unsigned long refNumber = *(unsigned long*)(&buffer[6]);

  t_QuoteNodeInfo nodeInfo = OrderMapRemove(OrderMaps[ecn_book_id][_consumerId], refNumber);
  if (nodeInfo.node != NULL && nodeInfo.node != DeletedQuotePlaceholder)
  {
    // Delete old node
    int side = (nodeInfo.side == 'S')?ASK_SIDE:BID_SIDE;
    DeleteQuote(nodeInfo.node, ecn_book_id, side, symbolId);
  }
  
  CheckBookCrossToEnableSymbol(symbolId);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessModifyOrderMessageShort_BATS
****************************************************************************/
int ProcessModifyOrderMessageShort_BATS(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime, unsigned int timeStatusSeconds)
{
  /*
  Order Id      6   8 Binary
  Shares        14  2 Binary
  Price         16  2 Binary Long Price
  Modify Flags  18  1 Bit Field
  */
  CheckStaleData_BATS(ecn_book_id, buffer, symbolId, arrivalTime, timeStatusSeconds);
  
  t_OrderDetail BATS_Order;
  
  // Order Id
  BATS_Order.refNum = *(unsigned long*)(&buffer[6]);
 
  // Get shares
  BATS_Order.shareVolume = *(unsigned short*)(&buffer[14]);
  
  // Get Share Price
  BATS_Order.sharePrice = (*(unsigned short*)(&buffer[16])) * 0.01;

  t_QuoteNodeInfo nodeInfo = OrderMapRemove(OrderMaps[ecn_book_id][_consumerId], BATS_Order.refNum);
  if(nodeInfo.node != NULL)
  {
    int side = (nodeInfo.side == 'S')?ASK_SIDE:BID_SIDE;

    if (nodeInfo.node != DeletedQuotePlaceholder)
      DeleteQuote(nodeInfo.node, ecn_book_id, side, symbolId);
    
    /* We have all information needed for cross detection and adding new info to book */
    if (SymbolMgmt.symbolList[symbolId].haltStatus[ecn_book_id] == TRADING_NORMAL)
    {
      // Check cross trade
      int tradeRet = DetectCrossTrade(&BATS_Order, side, symbolId, ecn_book_id, arrivalTime/1000, REASON_CROSS_ENTRY);
    
      if (tradeRet == TRADE_DETECTED_SELF_CROSSED)
      {
        DeleteFromPrice(BATS_Order.sharePrice, ecn_book_id, !side, symbolId);
      }
    }
  
    t_QuoteNode *retNode = InsertQuote(ecn_book_id, side, symbolId, &BATS_Order);
    if (retNode)
    {
      OrderMapInsert(OrderMaps[ecn_book_id][_consumerId], BATS_Order.refNum, retNode, nodeInfo.side);
    }
    else
      return ERROR;
  }
  
  CheckBookCrossToEnableSymbol(symbolId);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessModifyOrderMessageLong_BATS
****************************************************************************/
int ProcessModifyOrderMessageLong_BATS(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime, unsigned int timeStatusSeconds)
{
  /*
  Order Id        6   8 Binary
  Shares          14  4 Binary
  Price           18  8 Binary Long Price
  Modify Flags    26  1 Bit Field
  */
  
  CheckStaleData_BATS(ecn_book_id, buffer, symbolId, arrivalTime, timeStatusSeconds);

  t_OrderDetail BATS_Order;
    
  // Order Id
  BATS_Order.refNum = *(unsigned long*)(&buffer[6]);
 
  // Get shares
  BATS_Order.shareVolume = *(int*)(&buffer[14]);
  
  // Get Share Price
  BATS_Order.sharePrice = (*(unsigned long*)(&buffer[18])) * 0.0001;
  
  t_QuoteNodeInfo nodeInfo = OrderMapRemove(OrderMaps[ecn_book_id][_consumerId], BATS_Order.refNum);
  if(nodeInfo.node != NULL)
  {
    int side = (nodeInfo.side == 'S')?ASK_SIDE:BID_SIDE;
    if(nodeInfo.node != DeletedQuotePlaceholder) DeleteQuote(nodeInfo.node, ecn_book_id, side, symbolId);
  
    /* We have all information needed for cross detection and adding new info to book */
    if (SymbolMgmt.symbolList[symbolId].haltStatus[ecn_book_id] == TRADING_NORMAL)
    {
      // Check cross trade
      int tradeRet = DetectCrossTrade(&BATS_Order, side, symbolId, ecn_book_id, arrivalTime/1000, REASON_CROSS_ENTRY);
    
      if (tradeRet == TRADE_DETECTED_SELF_CROSSED)
      {
        DeleteFromPrice(BATS_Order.sharePrice, ecn_book_id, !side, symbolId);
      }
    }
  
    /* Add new quote to book */
    t_QuoteNode *retNode = InsertQuote(ecn_book_id, side, symbolId, &BATS_Order);
  
    if (retNode)
    {
      OrderMapInsert(OrderMaps[ecn_book_id][_consumerId], BATS_Order.refNum, retNode, nodeInfo.side);
    }
    else
      return ERROR;
  }
  
  CheckBookCrossToEnableSymbol(symbolId);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessReduceSizeMessageShort_BATS
****************************************************************************/
int ProcessReduceSizeMessageShort_BATS(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime, unsigned int timeStatusSeconds)
{
  /*
  Order Id          6   8 Binary
  Canceled Shares   14  2 Binary
  */
  
  CheckStaleData_BATS(ecn_book_id, buffer, symbolId, arrivalTime, timeStatusSeconds);
  
  unsigned long refNumber = *(unsigned long*)(&buffer[6]);
  
  t_QuoteNodeInfo nodeInfo = OrderMapRemove(OrderMaps[ecn_book_id][_consumerId], refNumber);
  if (nodeInfo.node != NULL && nodeInfo.node != DeletedQuotePlaceholder)
  {
    int side = (nodeInfo.side == 'S')?ASK_SIDE:BID_SIDE;
    int cancelShares = *(unsigned short*)(&buffer[14]);
    
    if (cancelShares >= nodeInfo.node->info.shareVolume)
    {
      // Delete old node
      DeleteQuote(nodeInfo.node, ecn_book_id, side, symbolId);
      CheckBookCrossToEnableSymbol(symbolId);
    }
    else
    {
      OrderMapInsert(OrderMaps[ecn_book_id][_consumerId], refNumber, nodeInfo.node, nodeInfo.side);
      ModifyQuote(nodeInfo.node, nodeInfo.node->info.shareVolume - cancelShares, side, symbolId);
    }
  }
  
  return SUCCESS;
}


/****************************************************************************
- Function name:  ProcessReduceSizeMessageLong_BATS
****************************************************************************/
int ProcessReduceSizeMessageLong_BATS(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime, unsigned int timeStatusSeconds)
{
  /*
  Order Id          6   8 Binary
  Canceled Shares   14  4 Binary
  */
  
  CheckStaleData_BATS(ecn_book_id, buffer, symbolId, arrivalTime, timeStatusSeconds);
  
  unsigned long refNumber = *(unsigned long*)(&buffer[6]);
  
  t_QuoteNodeInfo nodeInfo = OrderMapRemove(OrderMaps[ecn_book_id][_consumerId], refNumber);
  
  if (nodeInfo.node != NULL && nodeInfo.node != DeletedQuotePlaceholder)
  {
    int side = (nodeInfo.side == 'S')?ASK_SIDE:BID_SIDE;
    int cancelShares = *(int*)(&buffer[14]);
    
    if (cancelShares >= nodeInfo.node->info.shareVolume)
    {
      // Delete old node
      DeleteQuote(nodeInfo.node, ecn_book_id, side, symbolId);
      CheckBookCrossToEnableSymbol(symbolId);
    }
    else
    {
      OrderMapInsert(OrderMaps[ecn_book_id][_consumerId], refNumber, nodeInfo.node, nodeInfo.side);
      ModifyQuote(nodeInfo.node, nodeInfo.node->info.shareVolume - cancelShares, side, symbolId);
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessTradingStatusMessage_BATS
****************************************************************************/
int ProcessTradingStatusMessage_BATS(unsigned char ecn_book_id, uint32_t symbolId, char *buffer, unsigned long arrivalTime)
{
  /*
  Symbol          6   8 Binary
  Halted status   14  4 Binary
  Reg SH0 action  15  1 AlphaNumeric
  Reserve 1       16  1 Alpha
  Reserve 2       17  1 Alpha
  */
  
  switch (buffer[14])
  {
    case 'H': //Halted
      UpdateHaltStatusForASymbol(symbolId, TRADING_HALTED, arrivalTime/TEN_RAISE_TO_9, MAX_ECN_BOOK);
      break;
    case 'T': //Trading
    case 'Q': //Quote Only
      UpdateHaltStatusForASymbol(symbolId, TRADING_NORMAL, arrivalTime/TEN_RAISE_TO_9, ecn_book_id);
      break;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessUnitClearMessage_BATS
****************************************************************************/
int ProcessUnitClearMessage_BATS(unsigned char ecn_book_id, uint32_t symbol_id)
{
  FlushVenueSymbol(ecn_book_id, symbol_id);
  return SUCCESS;
}
