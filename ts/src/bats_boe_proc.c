/****************************************************************************
** Project name:  Equity Arbitrage Application
** File name:   bats_boe_proc.c
** Description:   This file contains function definitions that were declared
        in batsz_boe_proc.h

** Author:    Khoa Pham-Tan
** First created on 31 August 2012
** Last updated on 31 August 2012
****************************************************************************/
#define _ISOC9X_SOURCE

#include <unistd.h>
#include <sys/time.h>

#include "bats_boe_proc.h"
#include "trading_mgmt.h"
#include "kernel_algorithm.h"
#include "network.h"
#include "logging.h"

/****************************************************************************
            GLOBAL VARIABLES
****************************************************************************/
t_BATS_BOE_SequenceOfUnit  BATSZ_BOE_SequenceOfUnit[MAX_BATS_NUM_UNIT];
t_BATS_BOE_SequenceOfUnit  BYX_BOE_SequenceOfUnit[MAX_BATS_NUM_UNIT];

static pthread_spinlock_t BATSZ_NewOrderLock;
static pthread_spinlock_t BYX_NewOrderLock;
/****************************************************************************
            LIST OF FUNCTIONS
****************************************************************************/

/****************************************************************************
- Function name:  Init_BATS_BOE_DataStructure
****************************************************************************/
void Init_BATS_BOE_DataStructure(int oecType)
{
  //pthread_spinlock_t *BATS_NewOrderLock = (oecType == ORDER_BATSZ_BOE? BATSZ_NewOrderLock:BYX_NewOrderLock);
  t_BATS_BOE_SequenceOfUnit *BATS_BOE_SequenceOfUnit = (oecType == ORDER_BATSZ_BOE? BATSZ_BOE_SequenceOfUnit:BYX_BOE_SequenceOfUnit);
  
  if(oecType == ORDER_BATSZ_BOE)
    pthread_spin_init(&BATSZ_NewOrderLock, 0);
  else
    pthread_spin_init(&BYX_NewOrderLock, 0);
  
  
  OecConfig[oecType].socket = -1;
  OecConfig[oecType].numbersOfBatszUnits = 0;

  int i = 0;
  for(i = 0; i < MAX_BATS_NUM_UNIT; i++)
  {
    BATS_BOE_SequenceOfUnit[i].incomingSeqNum = 0;
  }
}

/****************************************************************************
- Function name:  Connect_BATS_BOE
****************************************************************************/
void Connect_BATS_BOE(int oecType)
{
  Init_BATS_BOE_DataStructure(oecType);

  if (LoadSequenceNumber(
        oecType == ORDER_BATSZ_BOE? FN_SEQUENCE_NUM_BATSZ:FN_SEQUENCE_NUM_BYX
        , oecType) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "%s: Could not load seqs file\n", GetOrderNameByIndex(oecType));
    OrderStatusMgmt[oecType].shouldConnect = NO;
    return;
  }

  TraceLog(DEBUG_LEVEL, "Connecting to %s Server %s:%d...\n", GetOrderNameByIndex(oecType), OecConfig[oecType].ipAddress, OecConfig[oecType].port);
  OecConfig[oecType].socket = connectSession(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, OecConfig[oecType].sessionId);

  if (OecConfig[oecType].socket == -1)
  {
    TraceLog(ERROR_LEVEL, "%s: Could not connect to Server\n", GetOrderNameByIndex(oecType));
    OrderStatusMgmt[oecType].shouldConnect = NO;
    return;
  }

  if (SetSocketRecvTimeout(OecConfig[oecType].socket, BATS_BOE_RECV_TIMEOUT) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Could not set socket receive-timeout for %s socket\n", GetOrderNameByIndex(oecType));
    disconnectSession(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, OecConfig[oecType].sessionId);
    OrderStatusMgmt[oecType].shouldConnect = NO;
    return;
  }

  if (SetSocketSndTimeout(OecConfig[oecType].socket, 1, 0) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Could not set socket send-timeout for %s socket\n", GetOrderNameByIndex(oecType));
    disconnectSession(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, OecConfig[oecType].sessionId);
    OrderStatusMgmt[oecType].shouldConnect = NO;
    return;
  }
  
  if (DisableNagleAlgo(OecConfig[oecType].socket) == -1)
  {
    TraceLog(ERROR_LEVEL, "Could not disable Nagle algorithm for %s socket\n", GetOrderNameByIndex(oecType));
    disconnectSession(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, OecConfig[oecType].sessionId);
    OrderStatusMgmt[oecType].shouldConnect = NO;
    return;
  }
  
  if (Process_BATS_BOE_Login(oecType) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "%s: could not send logon request message to server\n", GetOrderNameByIndex(oecType));
    OrderStatusMgmt[oecType].shouldConnect = NO;
    return;
  }
}

/****************************************************************************
- Function name:  Process_BATS_BOE_DataBlock
****************************************************************************/
int Process_BATS_BOE_DataBlock(int oecType, uint32_t symbolId, t_DataBlock *dataBlock)
{
  int mu;
  
  t_BATS_BOE_SequenceOfUnit *BATS_BOE_SequenceOfUnit = (oecType == ORDER_BATSZ_BOE? BATSZ_BOE_SequenceOfUnit:BYX_BOE_SequenceOfUnit);
  
  /*
    * Messages is sequenced: Order-ACK, Order-Cancalled, Order-Execution
    * Solution for NumberOfUnits: set numbers of Units = greatest value of Matching-Unit
  */
  
  switch(dataBlock->msgContent[4]) // Get message type
  {
    case BATS_BOE_ORDER_ACK:
      mu = (unsigned char)dataBlock->msgContent[5];
      __sync_add_and_fetch(&BATS_BOE_SequenceOfUnit[mu-1].incomingSeqNum, 1);
      
      if(OecConfig[oecType].numbersOfBatszUnits < mu)
        OecConfig[oecType].numbersOfBatszUnits = mu;
      
      if(OrderStatusMgmt[oecType].isConnected == CONNECTED)
      {
        ProcessBATS_BOE_OrderAckMsg(oecType, symbolId, dataBlock);
      }
      else
      {
        TraceLog(DEBUG_LEVEL, "%s: Received order ACK in Recovery\n", GetOrderNameByIndex(oecType));
        Add_BATS_BOE_DataBlockToCollection(oecType, dataBlock);
      }
      break;

    case BATS_BOE_ORDER_EXECUTION: // Filled Order
      mu = (unsigned char)dataBlock->msgContent[5];
      __sync_add_and_fetch(&BATS_BOE_SequenceOfUnit[mu-1].incomingSeqNum, 1);
      
      if(OecConfig[oecType].numbersOfBatszUnits < mu) 
        OecConfig[oecType].numbersOfBatszUnits = mu;
      
      if(OrderStatusMgmt[oecType].isConnected == CONNECTED)
      {
        ProcessBATS_BOE_OrderExecutionMsg(oecType, symbolId, dataBlock);
      }
      else
      {
        TraceLog(DEBUG_LEVEL, "%s: Received order EXECUTION in Recovery\n", GetOrderNameByIndex(oecType));
        Add_BATS_BOE_DataBlockToCollection(oecType, dataBlock);
      }
      break;

    case BATS_BOE_ORDER_REJECTED: // Order has been rejected
      if(OrderStatusMgmt[oecType].isConnected == CONNECTED)
      {
        ProcessBATS_BOE_OrderRejectedMsg(oecType, symbolId, dataBlock);
      }
      else
      {
        TraceLog(WARN_LEVEL, "%s: Received order REJECTED in Recovery (Unexpected) \n", GetOrderNameByIndex(oecType));
        Add_BATS_BOE_DataBlockToCollection(oecType, dataBlock);
      }
      
      break;

    case BATS_BOE_ORDER_CANCELLED: // Order Cancelled
      mu = (unsigned char)dataBlock->msgContent[5];
      __sync_add_and_fetch(&BATS_BOE_SequenceOfUnit[mu-1].incomingSeqNum, 1);
      
      if(OecConfig[oecType].numbersOfBatszUnits < mu) 
        OecConfig[oecType].numbersOfBatszUnits = mu;
      
      if(OrderStatusMgmt[oecType].isConnected == CONNECTED)
      {
        ProcessBATS_BOE_OrderCanceledMsg(oecType, symbolId, dataBlock);
      }
      else
      {
        TraceLog(DEBUG_LEVEL, "%s: Received order CANCELLED in Recovery\n", GetOrderNameByIndex(oecType));
        Add_BATS_BOE_DataBlockToCollection(oecType, dataBlock);
      }
      break;
      
    case BATS_BOE_CANCEL_REJECTED:  // Cancel Rejected
      if(OrderStatusMgmt[oecType].isConnected == CONNECTED)
      {
        TraceLog(DEBUG_LEVEL, "%s: Received Cancel Rejected\n", GetOrderNameByIndex(oecType));
        ProcessBATS_BOE_CancelRejectedMsg(oecType, symbolId, dataBlock);
      }
      else
      {
        TraceLog(WARN_LEVEL, "%s: Received cancel REJECTED in Recovery (Unexpected) \n", GetOrderNameByIndex(oecType));
        Add_BATS_BOE_DataBlockToCollection(oecType, dataBlock);
      }
      break;
    
    case BATS_BOE_TRADE_CANCEL_OR_CORRECT:
      mu = (unsigned char)dataBlock->msgContent[5];
      __sync_add_and_fetch(&BATS_BOE_SequenceOfUnit[mu-1].incomingSeqNum, 1);
      
      if(OecConfig[oecType].numbersOfBatszUnits < mu) 
        OecConfig[oecType].numbersOfBatszUnits = mu;
      
      if(OrderStatusMgmt[oecType].isConnected == CONNECTED)
      {
        TraceLog(DEBUG_LEVEL, "%s: Received order Trade Cancel Or Correct\n", GetOrderNameByIndex(oecType));
        ProcessBATS_BOE_Trace_Cancel_Or_CorrectMsg(oecType, symbolId, dataBlock);
      }
      else
      {
        TraceLog(DEBUG_LEVEL, "%s: Received order Trade Cancel Or Correct in Recovery\n", GetOrderNameByIndex(oecType));
        Add_BATS_BOE_DataBlockToCollection(oecType, dataBlock);
      }
        break;
      
    case BATS_BOE_ORDER_RESTATED:
      mu = (unsigned char)dataBlock->msgContent[5];
      __sync_add_and_fetch(&BATS_BOE_SequenceOfUnit[mu-1].incomingSeqNum, 1);
      
      if(OecConfig[oecType].numbersOfBatszUnits < mu) 
        OecConfig[oecType].numbersOfBatszUnits = mu;
      
      if(OrderStatusMgmt[oecType].isConnected == CONNECTED)
      {
        TraceLog(DEBUG_LEVEL, "%s: Received order Order Restated\n", GetOrderNameByIndex(oecType));
        ProcessBATS_BOE_Order_RestatedMsg(oecType, symbolId, dataBlock); 
      }
      else
      {
        TraceLog(DEBUG_LEVEL, "%s: Received order Order Restated in Recovery\n", GetOrderNameByIndex(oecType)); 
        Add_BATS_BOE_DataBlockToCollection(oecType, dataBlock);
      }
      break;

    case BATS_BOE_SERVER_HEARTBEAT:
      if (BuildAndSend_BATS_BOE_Client_HeartbeatMsg(oecType) == ERROR)
        return ERROR;
      break;
    case BATS_BOE_LOGIN_RESPONSE:
      if(ProcessBATS_BOE_LoginReponseMsg(oecType, dataBlock) == ERROR)
        return ERROR;

      TraceLog(DEBUG_LEVEL, "%s: Received Login Response, waiting for Replay Complete Message\n", GetOrderNameByIndex(oecType));
      break;
    
    case BATS_BOE_LOGOUT:
      ProcessBATS_BOE_LogOutdMsg(oecType, dataBlock);
      return ERROR;
      break;
    
    case BATS_BOE_REPLAY_COMPLETE:
      TraceLog(DEBUG_LEVEL, "%s: Received Replay Complete Message\n", GetOrderNameByIndex(oecType)); 
      OrderStatusMgmt[oecType].isConnected = CONNECTED; 
      break;
    
    default:
      TraceLog(WARN_LEVEL, "%s: Unknown Message type = %02X\n", GetOrderNameByIndex(oecType), dataBlock->msgContent[4]);
      break;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  Add_BATS_BOE_DataBlockToCollection
****************************************************************************/
void Add_BATS_BOE_DataBlockToCollection(int oecType, t_DataBlock *dataBlock)
{
  dataBlock->blockLen = 12 + DATABLOCK_ADDITIONAL_INFO_LEN + dataBlock->msgLen;
  AddDataBlockToQueue(oecType, dataBlock);
}

/****************************************************************************
- Function name:  Process_BATS_BOE_Login
****************************************************************************/
int Process_BATS_BOE_Login(int oecType)
{
  if(BuildAndSend_BATS_BOE_LoginRequestMsg(oecType) == SUCCESS)
  {
    return SUCCESS;
  }
  else
  {
    TraceLog(ERROR_LEVEL, "%s: Failed in sending login message\n", GetOrderNameByIndex(oecType));
    return ERROR;
  }
}

/****************************************************************************
- Function name:  BuildAndSend_BATS_BOE_LoginRequestMsg
****************************************************************************/
int BuildAndSend_BATS_BOE_LoginRequestMsg(int oecType)
{
  /*
   * ************Why is the 5 * BATSZ_BOE_Config.numbersOfBatszUnits********
   * Each pair of unit/sequence has 5 bytes in which unit has 1 byte and sequence has 4 bytes
  */
  unsigned short maxLength = BATS_BOE_LOGIN_REQUEST_MSG_LEN_FIXED + (5 * OecConfig[oecType].numbersOfBatszUnits);
  
  t_BATS_BOE_SequenceOfUnit *BATS_BOE_SequenceOfUnit = (oecType == ORDER_BATSZ_BOE? BATSZ_BOE_SequenceOfUnit:BYX_BOE_SequenceOfUnit);

  char message[maxLength];
  memset(message, 0, maxLength);

  // Start of Message: 2 bytes(0-1])
  message[0] = 0xBA;
  message[1] = 0xBA;

  // Message length: 2 bytes(2-3)
  unsigned short mgsLength = maxLength - 2; // Not including 2 bytes of start of message
  memcpy(&message[2], &mgsLength, 2);

  // Message type: 1 byte
  message[4] = BATS_BOE_LOGIN_REQUEST;

  // Matching Unit: 1 byte -> always 0
  // Sequence number: 4 bytes(6-9]) -> always 0

  // Session Sub ID: 4 bytes(10-13)
  strncpy(&message[10], OecConfig[oecType].sessionSubId, 4);

  // User name: 4 bytes(14-17)
  strncpy(&message[14], OecConfig[oecType].userName, 4);

  // Password: 10 bytes(18-27)
  strncpy(&message[18], OecConfig[oecType].password, 10);

  /*
   * No unspecified Unit Replay: 1 byte
   * 0x00 = False: Replay Unspecified Units
   * 0x01 = True: Suppress Unspecified Units Replay
   * Set this value to 1(default) in batsz_boe_conf
  */
  message[28] = OecConfig[oecType].noUnspecifiedUnitReplay;

  /* ****************HOW ARE BITFIELDS USED *************************
   * + Currently, EAA will trade with Time In Force = IOC. Therefore we just desire to
   *    receive messages of Order-ACK/Order-Rejected/Order-Cancelled/Order-Execution from BATSZ BOE
   * + Use 7 bytes for each Bitfields. If bit in bitfield set to 1, we will use optional field
   * + There is always 1 byte(Reserved) after each Bitfields
  */

  /***********Order Acknowledgement Bitfield (29-35)*********/
  //Bitfield1 (1:Side, 2:PerDifference, 3:Price, 4:ExecInst, 5:OrdType, 6:TimeInForce, 7:MinQty, 8:MaxRemovePct)
  message[29] = 0xBD; // 10111101 (Side, Price, ExecInst, OrdType, TimeInForce, MaxRemovePct)
  
  //Bitfield2 (1:Symbol, 2:SymbolSfx, 3:RESERVED, 4:RESERVED, 5:RESERVED, 6:RESERVED 7:Capacity, 8:RESERVED)
  message[30] = 0x43; // 01000011 (Symbol, SymbolSfx, Capacity)
  
  //Bitfield3 (1:Account, 2:ClearingFilm, 3:ClearingAccount, 4:DisplayIndicator, 5:MaxFloor, 6:Discretion Amount, 7:OrderQty, 8:PreventMember Match)
  message[31] = 0x7B; // 01111011 (Account, ClearingFilm, DisplayIndicator, Maxfloor, Discretion Amount, OrderQty)
  
  //Bitfield4: not use
  
  //Bitfield5 (1:OrigClOrdID, 2:LeavesQty, 3:LastShares, 4:LastPx, 5:DisplayPrice, 6:WorkingPrice, 7:BaseLiquidity, 8:ExpireTime)
  message[33] = 0x4F; // 01001111(OrigCldOrdID, LeavesQty, LastShares, LastPx, BaseLiquidity)
  
  //Bitfield6: not use
  //Bitfield7: not use
  //Reserved: 1 byte -> Not use
  

  /*************** Order Rejected Bitfields (37-43) *******************/
  //Bitfield1 (1:Side, 2:PerDifference, 3:Price, 4:ExecInst, 5:OrdType 6:TimeInForce, 7:MinQty, 8:MaxRemovePct)
  message[37] = 0xBD; // 10111101 (Side, Price, ExecInst, OrdType, TimeInForce, MaxRemovePct)
  
  //Bitfield2 (1:Symbol, 2:SymbolSfx, 3:RESERVED, 4:RESERVED, 5:RESERVED, 6:RESERVED 7:Capacity, 8:RESERVED)
  message[38] = 0x43; // 01000011 (Symbol, SymbolSfx, Capacity)
  
  //Bitfield3 (1:Account, 2:ClearingFilm, 3:ClearingAccount, 4:DisplayIndicator, 5:MaxFloor, 6:Discretion Amount, 7:OrderQty, 8:PreventMember Match)
  message[39] = 0x7B; // 01111011 (Account, ClearingFilm, DisplayIndicator, Maxfloor, Discretion Amount, OrderQty)
  
  //Bitfield4: not use
  //Bitfield5: not use
  //Bitfield6: not use
  //Bitfield7: not use
  // Reserved 1 byte -> Not use
  
  
  /************OrderModified Bitfields(45-51): not use**************/
  // Reserved 1 byte -> Not use
  
  
  /*************** Order Restated Bitfields (53-58): not use *******************/
  // Reserved 1 byte -> Not use
  
  
  /***********User Modify Rejected Bitfields(61-67) -> Not use**********/
  // Reserved 1 byte -> Not use
  

  /*************Order Cancelled Bitfields (69-75)***************/
  //Bitfield1 (1:Side)
  message[69] = 0x01; // 00000001 (Side)
  
  //Bitfield2 (1:Symbol)
  message[70] = 0x01; // 00000001 (Symbol)
  
  //Bitfield3 (1:Account, 2:ClearingFilm, 3:ClearingAccount, 4:DisplayIndicator, 5:MaxFloor, 6:Discretion Amount, 7:OrderQty, 8:PreventMember Match)
  message[71] = 0x7B; // 01111011 (Account, ClearingFilm, DisplayIndicator, Maxfloor, Discretion Amount, OrderQty)
  
  //Bitfield4: not use
  
  //Bitfield5 (1:OrigClOrdID, 2:LeavesQty, 3:LastShares, 4:LastPx, 5:DisplayPrice, 6:WorkingPrice, 7:BaseLiquidity, 8:ExpireTime)
  message[73] = 0x4F; // 01001111 (OrigCldOrdID, LeavesQty, LastShares, LastPx, BaseLiquidity)
  
  //Bitfield6: not use
  //Bitfield7: not use
  // Reserved 1 byte -> Not use

  // Cancel Reject Bitfields (77-83) -> Filled value to 0 in memset
  // Reserved 1 byte -> Not use

  
  /***************Order Execution Bitfields(85-91)*****************/
  //Bitfield1 (1:Side, 2:PerDifference, 3:Price, 4:ExecInst, 5:OrdType, 6:TimeInForce, 7:MinQty, 8:MaxRemovePct)
  message[85] = 0x3D; // 00111101 (Side, Price, ExecInst, OrdType, TimeInForce)
  
  //Bitfield2 (1:Symbol, 2:SymbolSfx, 3:RESERVED, 4:RESERVED, 5:RESERVED, 6:RESERVED 7:Capacity, 8:RESERVED)
  message[86] = 0x43; // 01000011 (Symbol, SymbolSfx, Capacity)
  
  //Bitfield3 (1:Account, 2:ClearingFilm, 3:ClearingAccount, 4:DisplayIndicator, 5:MaxFloor, 6:Discretion Amount, 7:OrderQty, 8:PreventMember Match)
  message[87] = 0x7B; // 01111011 (Account, ClearingFilm, DisplayIndicator, Maxfloor, Discretion Amount, OrderQty)
  
  //Bitfield4: not use
  //Bitfield5: not use
  //Bitfield6: not use
  //Bitfield7: not use
  // Reserved 1 byte -> Not use

  
  /***********Trade Cancel Or Correct Bitfields(93-99): not use***********/
  // Reserved 1 byte -> Not use

  
  // Skip message(101 -> 116)

  // Number of Units
  message[117] = OecConfig[oecType].numbersOfBatszUnits; // just use one byte

  // Unit Number and Unit Sequence
  int indexOfUnitSequence = BATS_BOE_LOGIN_REQUEST_MSG_LEN_FIXED;
  int i = 0;
  for(i = 0; i < OecConfig[oecType].numbersOfBatszUnits; i++)
  {
    //Unit Number: 1 byte
    message[indexOfUnitSequence] = i + 1;
    indexOfUnitSequence += 1;

    // Unit Sequence: 4 bytes
    memcpy(&message[indexOfUnitSequence], &(BATS_BOE_SequenceOfUnit[i].incomingSeqNum), 4) ;
    indexOfUnitSequence += 4;
  }

  return SocketSend(OecConfig[oecType].socket, message, maxLength);
}

/****************************************************************************
- Function name:  BuildAndSend_BATS_BOE_LogoutRequestMsg
****************************************************************************/
int BuildAndSend_BATS_BOE_LogoutRequestMsg(int oecType)
{
  char message[BATS_BOE_LOGOUT_REQUEST_MSG_LEN];
  memset(message, 0, BATS_BOE_LOGOUT_REQUEST_MSG_LEN);

  // Start of Message: 2 bytes(0-1)
  message[0] = 0xBA;
  message[1] = 0xBA;

  // Message length: 2 bytes(2-3)
  message[2] = 0x08;

  // Message type: 1 byte
  message[4] = BATS_BOE_LOGOUT_REQUEST;

  // Matching Unit 1 byte -> Not use
  // Sequence number 4 bytes(6-9) -> Not use

  return SocketSend(OecConfig[oecType].socket, message, BATS_BOE_HEARDER_MSG_LEN);
}

/****************************************************************************
- Function name:  BuildAndSend_BATS_BOE_Client_HeartbeatMsg
****************************************************************************/
int BuildAndSend_BATS_BOE_Client_HeartbeatMsg(int oecType)
{
  char message[BATS_BOE_HEARDER_MSG_LEN];
  memset(message, 0, BATS_BOE_HEARDER_MSG_LEN);

  // Start of Message: 2 bytes(0-1)
  message[0] = 0xBA;
  message[1] = 0xBA;

  // Message length: 2 bytes(2-3)
  message[2] = 0x08;

  // Message type: 1 byte
  message[4] = BATS_BOE_CLIENT_HEARTBEAT;

  // Matching Unit 1 byte -> Not use
  // Sequence number 4 bytes(6-9) -> Not use

  return SocketSend(OecConfig[oecType].socket, message, BATS_BOE_HEARDER_MSG_LEN);

}

/****************************************************************************
- Function name:  BuildAndSend_BATS_BOE_OrderMsg
****************************************************************************/
int BuildAndSend_BATS_BOE_OrderMsg(int oecType, t_OrderPlacementInfo *newOrder)
{
  char *msgContent = newOrder->dataBlock.msgContent;

  // Fill null-values for BATS_BOE_NEW_ORDER_MSG_LEN_FIXED = 41 fixed-bytes in new-order
  memset(newOrder->dataBlock.msgContent, 0, BATS_BOE_NEW_ORDER_MSG_LEN_FIXED);

  // Start of Message: 2 bytes(0-1)
  msgContent[0] = 0xBA;
  msgContent[1] = 0xBA;

  // Message length: 2 bytes: WILL BE UPDATED AFTER

  // Message type: 1 byte
  msgContent[4] = BATS_BOE_NEW_ORDER;

  // Matching Unit: 1 byte->Not use

  //Sequence number: 4 bytes
  // will be set right before send
  
  //Client Order ID: 20 bytes
  Lrc_itoafl(newOrder->orderID, 0, 20, &msgContent[10]);

  //Side
  msgContent[30] = newOrder->side;

  //OrderQty
  memcpy(&msgContent[31], &newOrder->shareVolume, 4);
  
  /*
   * New order bitfield1
   * bit1:ClearingFilm, bit2:ClearingAccount, bit3:Price, bit4:ExecInst
   * bit5:OrderType, bit6:TimeInForce, bit7:MinQty, bit8:MaxFloor
  */                                
  
   msgContent[35] = 0x3D; // 00111101 (ClearingFilm, Price, ExecInst, OrdType, TimeInForce)
   
  /*
   * New order bitfield2
   * bit1:Symbol, bit2:SymbolSfx, bit3:RESERVED, bit4:RESERVED
   * bit5:RESERVED, bit6:RESERVED, bit7:Capacity, bit8:RoutingInst
  */
  msgContent[36] = 0xC1; // 11000001 (Symbol, Capacity, RoutingInst)

  /*
   * New order bitfield3
   * bit1:Account, bit2:DisplayIndicator, bit3:MaxRemovePct, bit4:DiscretionAmount
   * bit5:PefDifference, bit6:PreventMemberMatch, bit7:LocateReqd, bit8:ExpireTime
  */
  msgContent[37] = 0x03; // 00000011 (Account, DisplayIndicator)

  //bitfield4: Not use
  //bitfield5: Not use
  //bitfield6: Not use

  int indexOptionFields = BATS_BOE_NEW_ORDER_MSG_LEN_FIXED;

  // ClearingFilm
  strncpy(&msgContent[indexOptionFields], (oecType == ORDER_BATSZ_BOE? MPID_BATSZ_BOE:MPID_BYX_BOE), 4);
  indexOptionFields += 4;

  // Price: 8 bytes
  long priceLong;

  if (fge(newOrder->sharePrice, 1.00))
  {
    if (newOrder->side == '1')  //BUY SIDE
    {
      priceLong = (long)(newOrder->sharePrice * 100.00 + 0.500001) * 100;
    }
    else
    {
      priceLong = (long)(newOrder->sharePrice * 100.00 + 0.000001) * 100;
    }
  }
  else
  {
    priceLong = (long)(newOrder->sharePrice * 10000.00 + 0.500001);
  }

  memcpy(&msgContent[indexOptionFields], &priceLong, 8);
  indexOptionFields += 8;

  //ExecInst: 1 bytes
  if (newOrder->ISO_Flag == YES)
  {
    msgContent[indexOptionFields] = 'f';
  } 
  else if (newOrder->isRoutable == YES)
  {
    msgContent[indexOptionFields] = 'w';
  }
  else
  {
    msgContent[indexOptionFields] = 0;  
  }                                     
  
  indexOptionFields += 1;

  //OrdType: 1 byte
  msgContent[indexOptionFields] = BATS_BOE_ORDER_TYPE_LIMIT;
  indexOptionFields += 1;

  // TimeInForce: 1 byte
  msgContent[indexOptionFields] = '3';  //IOC
  indexOptionFields += 1;

  //Symbol: 8 bytes
  strncpy(&msgContent[indexOptionFields], newOrder->symbol, 8);
  indexOptionFields += 8;

  // Capacity: 1 byte
  msgContent[indexOptionFields] = 'P';
  indexOptionFields += 1;

  // RoutingInst: 4 bytes 
  if(newOrder->isRoutable == YES)
  {             
    /*
    RoutingInst 4 Text 
    - (1st)R = Smart Route to visible mkts
    - (2nd)L = Re-Route
        N = Do not Re-Route
    - (3rd)2 = Parallel-2D
    */
    strncpy(&msgContent[indexOptionFields], "RL2", 4);
  }
  else
  {
    strncpy(&msgContent[indexOptionFields], "B", 4);
  } 

  indexOptionFields += 4;   

  //Account
  strcpy(&msgContent[indexOptionFields], (oecType == ORDER_BATSZ_BOE ? TradingAccount.BATSZBOEAccount : TradingAccount.BYXBOEAccount));
  indexOptionFields += 16;
  
  //DisplayIndicator
  msgContent[indexOptionFields] = 'V';
  indexOptionFields += 1;

  // Update message length
  int msgLength = indexOptionFields - 2; /*not including 2 bytes of start of message*/
  memcpy(&msgContent[2], &msgLength, 2);

  if (OrderBucketConsume() == ERROR)
  {
    TraceLog(WARN_LEVEL, "%s order is not allowed: Order bucket is currently empty\n", GetOrderNameByIndex(oecType));
    return ERROR;
  }
  
  long time = Send_BATS_BOE_SequencedMsg(oecType, msgContent, indexOptionFields);
  if (time > 0)
  {
    *((long*)&newOrder->dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]) = time;
    
    newOrder->ecnToPlaceID = oecType;
    newOrder->dataBlock.msgLen = indexOptionFields;
    
    DEVLOGF("Sent %c order to %s with id: %d for %s, %d @ $%lf\n", newOrder->side, GetOrderNameByIndex(oecType), newOrder->orderID, newOrder->symbol, newOrder->shareVolume, newOrder->sharePrice);
    
    return SUCCESS;
  }
  else
  {
    return ERROR;
  }
}

/****************************************************************************
- Function name:  ProcessBATS_BOE_LogOutdMsg
****************************************************************************/
void ProcessBATS_BOE_LogOutdMsg(int oecType, t_DataBlock *dataBlock)
{
  TraceLog(DEBUG_LEVEL, "%s: Received LOGOUT message\n", GetOrderNameByIndex(oecType));
  
  TraceLog(DEBUG_LEVEL, "%s: Logout Reason = %c\n", GetOrderNameByIndex(oecType), dataBlock->msgContent[10]);
  TraceLog(DEBUG_LEVEL, "%s: Logout Reason Text: %.60s\n", GetOrderNameByIndex(oecType), &dataBlock->msgContent[11]);
  TraceLog(DEBUG_LEVEL, "%s: Last Received Sequence Number: %d\n", GetOrderNameByIndex(oecType), *(int*)(&dataBlock->msgContent[71]));
  
  int numOfUnits = (unsigned char)dataBlock->msgContent[75];
  TraceLog(DEBUG_LEVEL, "%s: NumberOfUnits = %d\n", GetOrderNameByIndex(oecType), numOfUnits);

  int offset = BATS_BOE_LOGOUT_MSG_LEN_FIXED;
  int i = 0;
  for(i = 0; i < numOfUnits; i++)
  {
    TraceLog(DEBUG_LEVEL, "%s: UnitNumber%d = ", GetOrderNameByIndex(oecType),(unsigned char)dataBlock->msgContent[offset]);
    offset += 1;
    TraceLog(DEBUG_LEVEL, "%d\n", *(int*)(&dataBlock->msgContent[offset]) );
    offset += 4;
  }
}

/****************************************************************************
- Function name:  ProcessBATS_BOE_LoginReponseMsg
****************************************************************************/
int ProcessBATS_BOE_LoginReponseMsg(int oecType, t_DataBlock *dataBlock)
{
  if(dataBlock->msgContent[10] == 'A')
  {
    TraceLog(DEBUG_LEVEL, "%s: Logon was successful\n", GetOrderNameByIndex(oecType));
    TraceLog(DEBUG_LEVEL, "\t NoUnspecifiedUnitPlay = %02X\n", dataBlock->msgContent[71]);
    
    //Update Outgoing Sequence Number before send any messages to BATS BOE Server
    OecConfig[oecType].outgoingSeqNum = *(int*)(&dataBlock->msgContent[BATS_BOE_OUTGOING_SEQ_NUM_INDEX]);
    TraceLog(DEBUG_LEVEL, "\t LastReceivedSequenceNumber = %d\n", OecConfig[oecType].outgoingSeqNum);

    OecConfig[oecType].outgoingSeqNum += 1;
    
    // Number Of Unit
    int numOfUnits = (unsigned char)dataBlock->msgContent[164];
    TraceLog(DEBUG_LEVEL, "\t NumberOfUnits = %d\n", numOfUnits);

    // UnitNumber/UnitSequence
    int offset = BATS_BOE_LOGIN_RESPONSE_MSG_LEN_FIXED;
    int i = 0;
    for(i = 0; i < numOfUnits; i++)
    {
      TraceLog(DEBUG_LEVEL, "\t UnitNumber%d = %d", (unsigned char)dataBlock->msgContent[offset], *(int*)(&dataBlock->msgContent[offset+1]));
      offset += 5;
    }
    return SUCCESS;
  }
  else
  {
    TraceLog(DEBUG_LEVEL, "%s: Login Response Status = %c\n", GetOrderNameByIndex(oecType), dataBlock->msgContent[10]);
    TraceLog(DEBUG_LEVEL, "\t Login Response Text: %.60s\n", &dataBlock->msgContent[11]);
    return ERROR;
  }
}

/****************************************************************************
- Function name:  ProcessBATS_BOE_OrderAckMsg
****************************************************************************/
int ProcessBATS_BOE_OrderAckMsg(int oecType, uint32_t symbolId, t_DataBlock *dataBlock)
{
  Add_BATS_BOE_DataBlockToCollection(oecType, dataBlock);

  DEVLOGF("Received Order Ack from %s\n", GetOrderNameByIndex(oecType));

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessBATS_BOE_OrderExecutionMsg
****************************************************************************/
int ProcessBATS_BOE_OrderExecutionMsg(int oecType, uint32_t symbolId, t_DataBlock *dataBlock)
{
  char tempBuffer[20];
  memset(tempBuffer, 0, 20);
  
  t_ExecutionReport status;
  status.ecnID = oecType;
  status.stockSymbolIndex = symbolId;
  
  // Client Order ID
  memcpy(tempBuffer, &dataBlock->msgContent[18], 20);
  status.clientOrderID = atoi(tempBuffer);

  //Last Shares
  status.filledShare = *(int*)(&dataBlock->msgContent[46]);

  //Last Price
  status.filledPrice = *(long*)(&dataBlock->msgContent[50]);

  status.filledPrice = status.filledPrice/DECIMAL_NUM_PRICE;
  
  // Arrival time
  memcpy(&status.time, &dataBlock->addContent[DATABLOCK_OFFSET_FILTER_TIME], 8);

  // Process Partially Filled status
  ProcessFillResponse(&status);                                  

  *(unsigned short *)&dataBlock->addContent[DATABLOCK_OFFSET_ENTRY_EXIT_REF] = status.entryExitRefNum;

  // Add data block to collection
  Add_BATS_BOE_DataBlockToCollection(oecType, dataBlock);

  DEVLOGF("Received Fill from %s, %d shares at $%lf\n", GetOrderNameByIndex(oecType), status.filledShare, status.filledPrice);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessBATS_BOE_OrderRejectedMsg
****************************************************************************/
int ProcessBATS_BOE_OrderRejectedMsg(int oecType, uint32_t symbolId, t_DataBlock *dataBlock)
{
  char tempBuffer[20];
  memset(tempBuffer, 0, 20);

  t_ExecutionReport status;
  status.ecnID = oecType;
  status.stockSymbolIndex = symbolId;
  
  // Client Order ID
  memcpy(tempBuffer, &dataBlock->msgContent[18], 20);
  status.clientOrderID = atoi(tempBuffer);
  
  // Arrival time
  memcpy(&status.time, &dataBlock->addContent[DATABLOCK_OFFSET_FILTER_TIME], 8);
    
  ProcessRejectResponse(&status);
   
  *(unsigned short *)&dataBlock->addContent[DATABLOCK_OFFSET_ENTRY_EXIT_REF] = status.entryExitRefNum;

  // Add data block to collection
  Add_BATS_BOE_DataBlockToCollection(oecType, dataBlock);

  DEVLOGF("Received Reject from %s, reason: %s\n", GetOrderNameByIndex(oecType), &dataBlock->msgContent[39]);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessBATS_BOE_OrderCanceledMsg
****************************************************************************/
int ProcessBATS_BOE_OrderCanceledMsg(int oecType, uint32_t symbolId, t_DataBlock *dataBlock)
{
  char tempBuffer[20];
  memset(tempBuffer, 0, 20);

  t_ExecutionReport status;
  status.ecnID = oecType;
  status.stockSymbolIndex = symbolId;
  
  // Client Order ID
  memcpy(tempBuffer, &dataBlock->msgContent[18], 20);
  status.clientOrderID = atoi(tempBuffer);
  
  // Arrival time
  memcpy(&status.time, &dataBlock->addContent[DATABLOCK_OFFSET_FILTER_TIME], 8);

  // Cancelled Shares
  t_OpenOrder *openOrder = &TradingCollection[symbolId].openOrders[status.clientOrderID % MAX_OPEN_ORDERS];
  if(openOrder->orderID != status.clientOrderID)
  {
    TraceLog(ERROR_LEVEL, "%s: Received cancel response for unknown order id: %d\n", GetOrderNameByIndex(oecType), status.clientOrderID);
    return ERROR;
  }
  
  status.canceledShare = openOrder->shareVolume;
    
  ProcessCancelResponse(&status);
  
  *(unsigned short *)&dataBlock->addContent[DATABLOCK_OFFSET_ENTRY_EXIT_REF] = status.entryExitRefNum;
  
  // Add data block to collection
  Add_BATS_BOE_DataBlockToCollection(oecType, dataBlock);

  DEVLOGF("Received Order Cancelled from %s for order id: %d, cancelled shares: %d\n", GetOrderNameByIndex(oecType), status.clientOrderID, status.canceledShare);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessBATS_BOE_CancelRejectedMsg
****************************************************************************/
int ProcessBATS_BOE_CancelRejectedMsg(int oecType, uint32_t symbolId, t_DataBlock *dataBlock)
{
  Add_BATS_BOE_DataBlockToCollection(oecType, dataBlock);

  DEVLOGF("Received Cancel Rejected from %s\n", GetOrderNameByIndex(oecType));

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessBATS_BOE_Trace_Cancel_Or_CorrectMsg
****************************************************************************/
int ProcessBATS_BOE_Trace_Cancel_Or_CorrectMsg(int oecType, uint32_t symbolId, t_DataBlock *dataBlock)
{
  Add_BATS_BOE_DataBlockToCollection(oecType, dataBlock);

  DEVLOGF("Received Trade Cancel Or Correct from %s\n", GetOrderNameByIndex(oecType));
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessBATS_BOE_Order_RestatedMsg
****************************************************************************/
int ProcessBATS_BOE_Order_RestatedMsg(int oecType, uint32_t symbolId, t_DataBlock *dataBlock)
{
  Add_BATS_BOE_DataBlockToCollection(oecType, dataBlock);

  DEVLOGF("Received Order Restated from %s\n", GetOrderNameByIndex(oecType));
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  Send_BATS_BOE_SequencedMsg
****************************************************************************/
long Send_BATS_BOE_SequencedMsg(int oecType, char *message, int msgLen)
{
  long time = 0;
  switch(oecType)
  {
    case ORDER_BATSZ_BOE:
      pthread_spin_lock(&BATSZ_NewOrderLock);
        *((int*)&message[6]) = OecConfig[oecType].outgoingSeqNum++;
        time = AsyncSend(OecConfig[oecType].sessionId, message, msgLen);
      pthread_spin_unlock(&BATSZ_NewOrderLock);
      break;
    case ORDER_BYX_BOE:
      pthread_spin_lock(&BYX_NewOrderLock);
        *((int*)&message[6]) = OecConfig[oecType].outgoingSeqNum++;
        time = AsyncSend(OecConfig[oecType].sessionId, message, msgLen);
      pthread_spin_unlock(&BYX_NewOrderLock);
      break;
  }
  
  return time;
}
