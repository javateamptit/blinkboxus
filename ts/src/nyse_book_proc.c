#define _BSD_SOURCE

#include <pthread.h>
#include <time.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>

#include "utility.h"
#include "book_mgmt.h"
#include "configuration.h"
#include "trading_mgmt.h"
#include "nyse_book_proc.h"
#include "kernel_algorithm.h"
#include "hbitime.h"

/****************************************************************************
            GLOBAL VARIABLES
****************************************************************************/
static double _NYSE_PRICE_PORTION[10] = {1.0, 0.1, 0.01, 0.001, 0.0001, 0.00001, 0.000001, 0.0000001, 0.00000001, 0.000000001};
char NYSE_StockIndex_To_Symbol_Mapping[MAX_STOCK_SYMBOL][12];

t_NYSE_Book_Conf NyseBookConf;
t_NYSE_Book_Conf AmexBookConf;

extern t_MonitorNyseBook MonitorNyseBook;
extern t_MonitorNyseBook MonitorAmexBook;

/****************************************************************************
- Function name:  NYSE_InitDataStructure
****************************************************************************/
int NYSE_InitDataStructure(int ecnIndex)
{
  t_MonitorNyseBook *monitor;
  t_NYSE_Book_Conf *config;
  int maxGroup;
  if (ecnIndex == BOOK_NYSE)
  {
    TraceLog(DEBUG_LEVEL, "Initializing Nyse Book...\n");
    monitor = &MonitorNyseBook;
    config = &NyseBookConf;
    maxGroup = MAX_NYSE_BOOK_GROUPS;
  }
  else
  {
    TraceLog(DEBUG_LEVEL, "Initializing Amex Book...\n");
    monitor = &MonitorAmexBook;
    config = &AmexBookConf;
    maxGroup = 1;
  }
  
  int i, j;
  monitor->procCounter = 0;
  for (i = 0; i < maxGroup; i++)
  {
    monitor->recvCounter[i] = 0;
    config->group[i].groupIndex = i;
    
    if (ecnIndex == BOOK_NYSE)
      config->group[i].groupPermission = SymbolSplitInfo[AlgorithmConfig.splitConfig.symbolRangeID].nyse[i];
    
    for (j = 0; j < DBLFILTER_NUM_QUEUES; j++)
      config->group[i].lossCounter[j] = 0;
  }

  int stockSymbolIndex;
  for (stockSymbolIndex = 0; stockSymbolIndex < MAX_STOCK_SYMBOL; stockSymbolIndex++)
  {
    SymbolStatus[stockSymbolIndex].count = 0;
  }

  int divisorIndex;
  for (stockSymbolIndex = 0; stockSymbolIndex < MAX_STOCK_SYMBOL; stockSymbolIndex++)
  {
    for (divisorIndex = 0; divisorIndex < PRICE_HASHMAP_BUCKET_SIZE; divisorIndex++)
    {
      DeleteAllPricePointsFromHashSlot(Price_HashMap[stockSymbolIndex][ecnIndex][ASK_SIDE][divisorIndex]);
      Price_HashMap[stockSymbolIndex][ecnIndex][ASK_SIDE][divisorIndex] = NULL;
      
      DeleteAllPricePointsFromHashSlot(Price_HashMap[stockSymbolIndex][ecnIndex][BID_SIDE][divisorIndex]);
      Price_HashMap[stockSymbolIndex][ecnIndex][BID_SIDE][divisorIndex] = NULL;
    }
  }
  
  if (ecnIndex == BOOK_NYSE) // we do not need to reload symbol mapping for amex because amex and nyse use the same file
  {
    if (NYSE_LoadAllSymbolMappings(NYSE_BOOK_SYMBOL_INDEX_MAPPING_FILE) == ERROR)
    {
      return ERROR;
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  GetNyseMessageSize
****************************************************************************/
int GetNyseMessageSize(const unsigned char ecnIndex, const unsigned char *buffer, unsigned short msgType)
{
  switch (msgType)
  {
    case NYSE_BOOK_DELTA_UPDATE_MSG:
      return __bswap16(*(unsigned short *)buffer);
    case NYSE_BOOK_FULL_UPDATE_MSG:
      return __bswap16(*(unsigned short *)buffer);
    case NYSE_BOOK_SYMBOL_INDEX_MAPPING_MSG:
    case NYSE_BOOK_SEQ_NUMBER_RESET_MSG:
      return 14;
    case NYSE_BOOK_HEARTBEAT_MSG:
      return 0;
    default:
      TraceLog(ERROR_LEVEL, "Received unknown %s message type %d\n", GetBookNameByIndex(ecnIndex), msgType);
      return -1;
  }
}

/****************************************************************************
- Function name:  NYSE_ProcessBookMessage
****************************************************************************/
int NYSE_ProcessBookMessage(const unsigned char ecnIndex, uint32_t symbolId, char *buffer, unsigned long arrivalTime, unsigned short msgType)
{
  switch (msgType)
  {
    case NYSE_BOOK_DELTA_UPDATE_MSG:
      if (BookStatusMgmt[ecnIndex].isConnected == DISCONNECTED) return SUCCESS;  //If book is disconnected, we do not process remaining message in the buffer
      if (NYSE_ParseAndProcessDeltaMessage_From_DBLFilter(ecnIndex, buffer, arrivalTime, symbolId) == ERROR)
      {
        DisconnectFromVenue(ecnIndex, _consumerId);
        FlushVenueSymbolsOnQueue(ecnIndex, _consumerId);

        return ERROR;
      }
      break;
    case NYSE_BOOK_FULL_UPDATE_MSG:
      if (BookStatusMgmt[ecnIndex].isConnected == DISCONNECTED) return SUCCESS;  //If book is disconnected, we do not process remaining message in the buffer
      if (NYSE_ParseAndProcessFullMessage_From_DBLFilter(ecnIndex, buffer, arrivalTime, symbolId) == ERROR)
      {
        DisconnectFromVenue(ecnIndex, _consumerId);
        FlushVenueSymbolsOnQueue(ecnIndex, _consumerId);
        return ERROR;
      }
      break;
    case NYSE_BOOK_SEQ_NUMBER_RESET_MSG:
      if (BookStatusMgmt[ecnIndex].isConnected == DISCONNECTED) return SUCCESS;  //If book is disconnected, we do not process remaining message in the buffer
      break;
    case NYSE_BOOK_HEARTBEAT_MSG:
      if (BookStatusMgmt[ecnIndex].isConnected == DISCONNECTED) return SUCCESS;  //If book is disconnected, we do not process remaining message in the buffer
      break;
    //case NYSE_BOOK_SYMBOL_INDEX_MAPPING_MSG:
    default:
      if (BookStatusMgmt[ecnIndex].isConnected == DISCONNECTED) return SUCCESS;  //If book is disconnected, we do not process remaining message in the buffer
      TraceLog(ERROR_LEVEL, "Received Unknown %s message type %d\n", GetBookNameByIndex(ecnIndex), msgType);
      return ERROR;
  }
  return SUCCESS;
}

/****************************************************************************
- Function name:  NYSE_CheckStaleData
****************************************************************************/
int NYSE_CheckStaleData(const unsigned char ecnIndex, char *buffer, int stockSymbolIndex, unsigned long arrivalTime)
{
  unsigned long arrivalNanos = arrivalTime % NANOSECOND_PER_DAY;
  unsigned int nyseSourceTimeMilisecs = __builtin_bswap32(*(unsigned int*)&buffer[4]);
  unsigned int nyseSourceTimeMicros = __bswap16(*(unsigned short *)&buffer[8]);
  unsigned int nyseSeconds = nyseSourceTimeMilisecs / 1000;
  unsigned int nanoseconds = ((nyseSourceTimeMilisecs % 1000) * 1000 + nyseSourceTimeMicros) * 1000;
  unsigned long nyseNanos = (nyseSeconds + TotalTimeOffsetInSeconds) * TEN_RAISE_TO_9 + nanoseconds;
  long delay = arrivalNanos - nyseNanos;
  
  if (delay > DataDelayThreshold[ecnIndex])
  {
    if (SymbolMgmt.symbolList[stockSymbolIndex].isStale[ecnIndex] == 0)
    {
      SymbolMgmt.symbolList[stockSymbolIndex].isStale[ecnIndex] = 1;
      BuildSendStaleMarketNotification(ecnIndex, stockSymbolIndex, delay);
      TraceLog(WARN_LEVEL, "%s Book: '%.8s' data is delayed for %ld nanoseconds\n", GetBookNameByIndex(ecnIndex), SymbolMgmt.symbolList[stockSymbolIndex].symbol, delay);
    }
  }
  else
  {
    if (SymbolMgmt.symbolList[stockSymbolIndex].isStale[ecnIndex] == 0) return SUCCESS;
    
    if (SymbolMgmt.symbolList[stockSymbolIndex].isStale[ecnIndex] == 1)
    {
      SymbolMgmt.symbolList[stockSymbolIndex].isStale[ecnIndex] = 0;
      BuildSendStaleMarketNotification(ecnIndex, stockSymbolIndex, delay);
      TraceLog(WARN_LEVEL, "%s Book: '%.8s' data is fast again\n", GetBookNameByIndex(ecnIndex), SymbolMgmt.symbolList[stockSymbolIndex].symbol);
    }
    else
    {
      SymbolMgmt.symbolList[stockSymbolIndex].isStale[ecnIndex] = 0;
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  NYSE_LoadAllSymbolMappings
****************************************************************************/
int NYSE_LoadAllSymbolMappings(char *fileName)
{
  char fullPath[MAX_PATH_LEN] = "\0";

  if (GetFullConfigPath(fileName, CONF_SYMBOL, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "NYSE Book: Have some problem in getting config path for file: '%s'\n", fileName);
    return ERROR;
  }

  //Check if file already existed
  if (access(fullPath, F_OK) != 0)
  {
    TraceLog(ERROR_LEVEL, "NYSE Book: Could not find symbol mapping file: %s\n", fullPath);
    return ERROR;
  }

  //---------------------------------------------------------------------------------
  // Check if file is downloaded today, if not, we should not connect to NYSE Book
  //---------------------------------------------------------------------------------
  int ret = IsFileTimestampToday(fullPath);
  if (ret == 2)
  {
    TraceLog(ERROR_LEVEL, "Could not find symbol mapping file: %s\n", fullPath);
    return ERROR;
  }
  else if (ret != 1)
  {
    TraceLog(ERROR_LEVEL, "NYSE Book: %s is old, please redownload the file\n", fullPath);
    return ERROR;
  }

  //---------------------------------------
  // Process the file for index mapping
  //---------------------------------------
  TraceLog(DEBUG_LEVEL, "NYSE Book: Processing symbol index mapping file...\n");

  FILE *fp = fopen(fullPath, "r");
  if (fp == NULL)
  {
    TraceLog(ERROR_LEVEL, "NYSE Book (critical)! Could not open file (%s).\n", fullPath);
    PrintErrStr(ERROR_LEVEL, "calling fopen(): ", errno);
    return ERROR;
  }

  /* ******************************************
   * XML NYSE Symbol Index Mapping File format
   * <SymbolMappingFile>
   *   <SymbolMap>
   *     <Symbol>ABW PRA</Symbol><Index>1573</Index><Channel>AA</Channel><ExchangeID>N</ExchangeID>
   *   </SymbolMap>
   *   ..........
   * </SymbolMappingFile>
   * *******************************************/

  char line[512];
  memset(line, 0, 512);

  char *pBegin, *pEnd;
  char *strSymbolFlag = "<Symbol>";
  char isIgnored = 0;
  int len = strlen(strSymbolFlag);

  char tempChar;
  char tempSymbol[32];

  while (fgets(line, 512, fp) != NULL)
  {
    isIgnored = 0;
    
    pBegin = strchr(line, '<'); //get the first tag
    if (pBegin == NULL)
    {
      TraceLog(ERROR_LEVEL, "NYSE Symbol Index Mapping file: Wrong line format: %s\n", line);
      memset(line, 0, 512);
      continue;
    }

    // Check line contains <Symbol> or not
    if (strncmp(pBegin, strSymbolFlag, len) != 0)
    {
      // line does not contain strSymbolFlag, so ignore it
      memset(line, 0, 512);
      continue;
    }

    pBegin += len; //move to the first character of Symbol
    pEnd = strchr(pBegin, '<'); //find the close of Tag Symbol
    if(pEnd == NULL)
    {
      TraceLog(ERROR_LEVEL, "NYSE Symbol Index Mapping file: Wrong line format: %s\n", line);
      memset(line, 0, 512);
      continue;
    }

    /**********************Get Stock Symbol**************************/
    tempChar = *pEnd;
    *pEnd = '\0';
    
    strncpy(tempSymbol, pBegin, 31); //copy symbol string includes SYMBOL & ROOT
    tempSymbol[31] = 0;
    
    *pEnd = tempChar; //return orginal character

    char symbol[SYMBOL_LEN];
    
    if (ConvertCMSSymbolToComstock(tempSymbol, symbol) == ERROR)
    {
      isIgnored = 1;
    }

    if (SplitSymbolMapping[(int)tempSymbol[0]] == DISABLE)
    {
      isIgnored = 1;
    }
    
    /**********************Get Symbol Index**************************/
    pEnd = strchr(pBegin, '<');//find the close of Tag Symbol
    pEnd++;
    pBegin = strchr(pEnd, '<');//find the open of Tag Index
    if(pBegin == NULL)
      continue;
    pEnd = strchr(pBegin, '>');
    if(pEnd == NULL)
      continue;
    pBegin = pEnd + 1;//move the the first digit of Index
    pEnd = strchr(pBegin, '<');//find the close of Tag Index
    if(pEnd == NULL)
    {
      TraceLog(ERROR_LEVEL, "NYSE Symbol Index Mapping file: Wrong line format: %s\n", line);
      memset(line, 0, 512);
      continue;
    }
    *pEnd = 0;
    int symbolIndex = atoi(pBegin);
    *pEnd = '<'; //return to its original character

    /**********************Get Channel******************************/
    /*pBegin = strchr(++pEnd, '<'); //find the open tag of Tag Channel
    pEnd = strchr(pBegin, '>');
    pBegin = pEnd + 1;  //pBegin is point to the first character of ChannelId
    pEnd = strchr(pBegin, '<'); //find the close tag of Tag Channel
    *pEnd = 0; //set to null for copying string
    char strChannel[3];
    strncpy(strChannel, pBegin, 3);

    if (strcmp(strChannel, "AZ") == 0 || strcmp(strChannel, "") == 0)
    {
      //ignore MKT Securities & Uknown Channel
      continue;
    }

    *pEnd = '<'; //return to its original value
    */

    /**********************Add to Data Structure*************************/
    if (isIgnored == 1)
    {
      memset(line, 0, 512);
      continue;
    }

    if (symbolIndex <= 0 || symbolIndex >= MAX_NYSE_BOOK_STOCK_SYMBOL)
    {
      TraceLog(ERROR_LEVEL, "NYSE symbol mapping: stock symbol (%.8s) index (%d) less than 0"
          " or greater than defined const MAX_NYSE_BOOK_STOCK_SYMBOL (%d)\n",
          symbol, symbolIndex, MAX_NYSE_BOOK_STOCK_SYMBOL);
      memset(line, 0, 512);
      continue;
    }

    // Update Symbol String
    int stockSymbolIndex = GetStockSymbolIndex(symbol);
    if (stockSymbolIndex == -1)
    {
      stockSymbolIndex = UpdateStockSymbolIndex(symbol);
    }

    // Check duplicate stock symbol
    if (stockSymbolIndex != -1)
    {
      SymbolStatus[stockSymbolIndex].count++;

      if (SymbolStatus[stockSymbolIndex].count > 1)
      {
        TraceLog(ERROR_LEVEL, "NYSE Symbol Mapping file: Duplicate stock symbol index %.8s\n", symbol);
      }
    
      /*Note: We dont update enable flag in stock book,
        because we load symbols from file which contains all symbol mappings
        And This TS will not trade all of them*/
      strcpy(NYSE_StockIndex_To_Symbol_Mapping[stockSymbolIndex], tempSymbol);
    }

    memset(line, 0, 512);
  }

  fclose(fp);
  TraceLog(DEBUG_LEVEL, "NYSE symbol index mapping file has been processed.\n");

  return SUCCESS;
}

/****************************************************************************
- Function name:  NYSE_ParseAndProcessFullMessage_From_DBLFilter
****************************************************************************/
int NYSE_ParseAndProcessFullMessage_From_DBLFilter(const unsigned char ecnIndex, char* buffer, unsigned long arrivalTime, uint32_t symbolId)
{
  /*  
    Please note that the received msg is from DBLFilter.
    DBLFilter has removed the packet header from *buffer* variable
  */
  
  unsigned short msgSize = __bswap16(*(unsigned short *)buffer);
  
  unsigned char quoteCondition;
  unsigned char tradingStatus;
  unsigned char priceScaleCode;
  unsigned char numPricePoint;
  unsigned char allowToTradeOnBID = 0;
  unsigned char allowToTradeOnASK = 0;
  
  int i;
  int priceNumerator;
  
  NYSE_CheckStaleData(ecnIndex, buffer, symbolId, arrivalTime);
  
  //Update Symbol Trading Status
  tradingStatus = buffer[28];
  SymbolMgmt.symbolList[symbolId].tradingStatusInNyseBook = tradingStatus;
  
  switch (tradingStatus)
  {
    case NYSE_BOOK_TRADING_STATUS_PRE_OPENING:
      UpdateHaltStatusForASymbol(symbolId, TRADING_NORMAL, arrivalTime/TEN_RAISE_TO_9, ecnIndex);
      break;
    case NYSE_BOOK_TRADING_STATUS_CLOSED:
      UpdateHaltStatusForASymbol(symbolId, TRADING_NORMAL, arrivalTime/TEN_RAISE_TO_9, ecnIndex);
    
      // ignore this msg and flush book for this symbol
      FlushVenueSymbol(ecnIndex, symbolId);
      return SUCCESS;
      break;
    case NYSE_BOOK_TRADING_STATUS_HALTED:
      if (SymbolMgmt.symbolList[symbolId].haltStatus[ecnIndex] != TRADING_HALTED)
      {
        UpdateHaltStatusForASymbol(symbolId, TRADING_HALTED, arrivalTime/TEN_RAISE_TO_9, MAX_ECN_BOOK);
      }
      break;
    default:
      UpdateHaltStatusForASymbol(symbolId, TRADING_NORMAL, arrivalTime/TEN_RAISE_TO_9, ecnIndex);
      break;
  }

  //update quote condition
  quoteCondition = buffer[27];
  
  if (tradingStatus == NYSE_BOOK_TRADING_STATUS_OPENED)
  {
    switch (quoteCondition)
    {
      case NYSE_BOOK_SLOW_ON_BID:
        SymbolMgmt.symbolList[symbolId].tradingStatusInNyseBook = NYSE_BOOK_SLOW_ON_BID;
        allowToTradeOnASK = 1;
        break;
      case NYSE_BOOK_SLOW_ON_ASK:
        SymbolMgmt.symbolList[symbolId].tradingStatusInNyseBook = NYSE_BOOK_SLOW_ON_ASK;
        allowToTradeOnBID = 1;
        break;
      case NYSE_BOOK_SLOW_DUE_TO_LRP_GAP:
      case NYSE_BOOK_SLOW_DUE_TO_SET_SLOW:
        //Slow Mode on both side
        SymbolMgmt.symbolList[symbolId].tradingStatusInNyseBook = NYSE_BOOK_SLOW_MODE;
        break;
      default:
        allowToTradeOnBID = 1;
        allowToTradeOnASK = 1;
        break;
    }
  }

  /** Parse all price points from the message **/
  numPricePoint = (msgSize - NYSE_BOOK_FULL_MSG_HEADER_LEN) / NYSE_BOOK_FULL_MSG_PRICE_POINT_LEN;
  
  if (numPricePoint == 0) return SUCCESS;
  
  priceScaleCode = buffer[26];
  
  int pricePointOffset = NYSE_BOOK_FULL_MSG_HEADER_LEN;
  int bidNum = 0, askNum = 0;
  t_OrderDetail orders[MAX_SIDE][numPricePoint];    //Buffer to store price points on both sides ASK and BID
      
  for (i = 0; i < numPricePoint; i++)
  {
    priceNumerator = __builtin_bswap32(*(unsigned int*)&buffer[pricePointOffset]);
    
    if (BookStatusMgmt[ecnIndex].isConnected == DISCONNECTED) return SUCCESS;  //If book is disconnected, we do not process remaining message in the buffer
    
    if (buffer[pricePointOffset + 10] == 'B')   //BID_SIDE
    {
      //Detect if volume is zero, if so remove from stock book to avoid BID > ASK
      orders[BID_SIDE][bidNum].shareVolume = __builtin_bswap32(*(unsigned int*)&buffer[pricePointOffset + 4]);
      orders[BID_SIDE][bidNum].sharePrice = priceNumerator * _NYSE_PRICE_PORTION[priceScaleCode];
      
      if (orders[BID_SIDE][bidNum].shareVolume == 0)
      {
        //Delete from stock book right away
        NYSE_DeleteQuoteFromStockBook(ecnIndex, orders[BID_SIDE][bidNum].sharePrice, BID_SIDE, symbolId);
        
        //continue to next price point
        pricePointOffset += NYSE_BOOK_FULL_MSG_PRICE_POINT_LEN;
        continue;
      }
      
      bidNum++;
    }
    else                //ASK_SIDE
    {
      //Detect if volume is zero, if so remove from stock book to avoid BID > ASK
      orders[ASK_SIDE][askNum].shareVolume = __builtin_bswap32(*(unsigned int*)&buffer[pricePointOffset + 4]);
      orders[ASK_SIDE][askNum].sharePrice = priceNumerator * _NYSE_PRICE_PORTION[priceScaleCode];
      
      if (orders[ASK_SIDE][askNum].shareVolume == 0)
      {
        //Delete from stock book right away
        NYSE_DeleteQuoteFromStockBook(ecnIndex, orders[ASK_SIDE][askNum].sharePrice, ASK_SIDE, symbolId);
        
        pricePointOffset += NYSE_BOOK_FULL_MSG_PRICE_POINT_LEN;
        continue;
      }
      
      askNum++;
    }

    pricePointOffset += NYSE_BOOK_FULL_MSG_PRICE_POINT_LEN;
  }
  
  if (tradingStatus == NYSE_BOOK_TRADING_STATUS_PRE_OPENING) return SUCCESS;
  
  //-----------------------------------------------
  //  Now start to detect cross for BID side
  //-----------------------------------------------
  int tradeRet;
  if (allowToTradeOnBID == 1)
  {
    for (i = bidNum-1; i >= 0; i--)
    {
      if (BookStatusMgmt[ecnIndex].isConnected == DISCONNECTED) return SUCCESS;  //If book is disconnected, we do not process remaining message in the buffer
      
      tradeRet = DetectCrossTrade(&orders[BID_SIDE][i], BID_SIDE, symbolId, ecnIndex, arrivalTime/1000, REASON_CROSS_ENTRY);
      if (tradeRet == TRADE_DETECTED_SELF_CROSSED)  //Self-cross on NYSE, so we delete ASK_SIDE
      {
        DeleteFromPrice(orders[BID_SIDE][i].sharePrice, ecnIndex, ASK_SIDE, symbolId);
      }
    }
  }
  
  //-----------------------------------------------
  //  Now start to detect cross for ASK side
  //-----------------------------------------------
  if (allowToTradeOnASK == 1)
  {
    for (i = 0; i < askNum; i++)
    {
      if (BookStatusMgmt[ecnIndex].isConnected == DISCONNECTED) return SUCCESS;  //If book is disconnected, we do not process remaining message in the buffer
      
      tradeRet = DetectCrossTrade(&orders[ASK_SIDE][i], ASK_SIDE, symbolId, ecnIndex, arrivalTime/1000, REASON_CROSS_ENTRY);
      if (tradeRet == TRADE_DETECTED_SELF_CROSSED)  //Self-cross on NYSE, so we delete BID_SIDE
      {
        DeleteFromPrice(orders[ASK_SIDE][i].sharePrice, ecnIndex, BID_SIDE, symbolId);
      }
    }
  }
  
  //-----------------------------------------------
  //  Now update quotes from buffer to stock book
  //-----------------------------------------------
  for (i = bidNum-1; i >= 0; i--)
  {
    if (BookStatusMgmt[ecnIndex].isConnected == DISCONNECTED) return SUCCESS;  //If book is disconnected, we do not process remaining message in the buffer
    
    NYSE_UpdateQuotesToStockBook(ecnIndex, &orders[BID_SIDE][i], symbolId, BID_SIDE);
  }
  
  for (i = 0; i < askNum; i++)
  {
    if (BookStatusMgmt[ecnIndex].isConnected == DISCONNECTED) return SUCCESS;  //If book is disconnected, we do not process remaining message in the buffer
    
    NYSE_UpdateQuotesToStockBook(ecnIndex, &orders[ASK_SIDE][i], symbolId, ASK_SIDE);
  }
  CheckBookCrossToEnableSymbol(symbolId);
  return SUCCESS;
}

/****************************************************************************
- Function name:  NYSE_ParseAndProcessDeltaMessage_From_DBLFilter
****************************************************************************/
int NYSE_ParseAndProcessDeltaMessage_From_DBLFilter(const unsigned char ecnIndex, char* buffer, unsigned long arrivalTime, uint32_t symbolId)
{
  /*
    Please note that the received msg is from DBLFilter.
    DBLFilter has removed the packet header from *buffer* variable
  */
  
  unsigned short msgSize = __bswap16(*(unsigned short *)buffer);
  unsigned char quoteCondition;
  unsigned char tradingStatus;
  unsigned char priceScaleCode;
  unsigned char numPricePoint;
  unsigned char allowToTradeOnBID = 0;
  unsigned char allowToTradeOnASK = 0;
  
  int i;
  int priceNumerator;
  NYSE_CheckStaleData(ecnIndex, buffer, symbolId, arrivalTime);

  //Update Symbol Trading Status
  tradingStatus = buffer[16];
  SymbolMgmt.symbolList[symbolId].tradingStatusInNyseBook = tradingStatus;

  switch (tradingStatus)
  {
    case NYSE_BOOK_TRADING_STATUS_PRE_OPENING:
      UpdateHaltStatusForASymbol(symbolId, TRADING_NORMAL, arrivalTime/TEN_RAISE_TO_9, ecnIndex);
      break;
    case NYSE_BOOK_TRADING_STATUS_CLOSED:
      UpdateHaltStatusForASymbol(symbolId, TRADING_NORMAL, arrivalTime/TEN_RAISE_TO_9, ecnIndex);
      
      // ignore msg and flush book for this symbol
      FlushVenueSymbol(ecnIndex, symbolId);
      return SUCCESS;
      break;
    case NYSE_BOOK_TRADING_STATUS_HALTED:
      if (SymbolMgmt.symbolList[symbolId].haltStatus[ecnIndex] != TRADING_HALTED)
      {
        UpdateHaltStatusForASymbol(symbolId, TRADING_HALTED, arrivalTime/TEN_RAISE_TO_9, MAX_ECN_BOOK);
      }
      break;
    default:  //Should be OPEN/RE-OPEN
      UpdateHaltStatusForASymbol(symbolId, TRADING_NORMAL, arrivalTime/TEN_RAISE_TO_9, ecnIndex);
      break;
  }

  //update quote condition
  quoteCondition = buffer[15];
  
  if (tradingStatus == NYSE_BOOK_TRADING_STATUS_OPENED)
  {
    switch (quoteCondition)
    {
      case NYSE_BOOK_SLOW_ON_BID:
        SymbolMgmt.symbolList[symbolId].tradingStatusInNyseBook = NYSE_BOOK_SLOW_ON_BID;
        allowToTradeOnASK = 1;
        break;
      case NYSE_BOOK_SLOW_ON_ASK:
        SymbolMgmt.symbolList[symbolId].tradingStatusInNyseBook = NYSE_BOOK_SLOW_ON_ASK;
        allowToTradeOnBID = 1;
        break;
      case NYSE_BOOK_SLOW_DUE_TO_LRP_GAP:
      case NYSE_BOOK_SLOW_DUE_TO_SET_SLOW:
        //Slow Mode on both side
        SymbolMgmt.symbolList[symbolId].tradingStatusInNyseBook = NYSE_BOOK_SLOW_MODE;
        break;
      default:
        allowToTradeOnBID = 1;
        allowToTradeOnASK = 1;
        break;
    }
  }

  /** Parse all price points from the message **/
  numPricePoint = (msgSize - NYSE_BOOK_DELTA_MSG_HEADER_LEN) / NYSE_BOOK_DELTA_MSG_PRICE_POINT_LEN;
  
  if (numPricePoint == 0) return SUCCESS;
  
  priceScaleCode = buffer[17];
  
  int pricePointOffset = NYSE_BOOK_DELTA_MSG_HEADER_LEN;
  int bidNum = 0, askNum = 0;
  t_OrderDetail orders[MAX_SIDE][numPricePoint];    //Buffer to store price points on both sides ASK and BID
      
  for (i = 0; i < numPricePoint; i++)
  {
    priceNumerator = __builtin_bswap32(*(unsigned int*)&buffer[pricePointOffset]);
    
    if (BookStatusMgmt[ecnIndex].isConnected == DISCONNECTED) return SUCCESS;  //If book is disconnected, we do not process remaining message in the buffer
    
    if (buffer[pricePointOffset + 14] == 'B')   //BID_SIDE
    {
      //Detect if volume is zero, if so remove from stock book to avoid BID > ASK
      orders[BID_SIDE][bidNum].shareVolume = __builtin_bswap32(*(unsigned int*)&buffer[pricePointOffset + 4]);
      orders[BID_SIDE][bidNum].sharePrice = priceNumerator * _NYSE_PRICE_PORTION[priceScaleCode];
      
      if (orders[BID_SIDE][bidNum].shareVolume == 0)
      {
        //Delete from stock book right away
        NYSE_DeleteQuoteFromStockBook(ecnIndex, orders[BID_SIDE][bidNum].sharePrice, BID_SIDE, symbolId);
        
        //continue to next price point
        pricePointOffset += NYSE_BOOK_DELTA_MSG_PRICE_POINT_LEN;
        continue;
      }
      
      bidNum++;
    }
    else                      //ASK_SIDE
    {
      //Detect if volume is zero, if so remove from stock book to avoid BID > ASK
      orders[ASK_SIDE][askNum].shareVolume = __builtin_bswap32(*(unsigned int*)&buffer[pricePointOffset + 4]);
      orders[ASK_SIDE][askNum].sharePrice = priceNumerator * _NYSE_PRICE_PORTION[priceScaleCode];
      
      if (orders[ASK_SIDE][askNum].shareVolume == 0)
      {
        //Delete from stock book right away
        NYSE_DeleteQuoteFromStockBook(ecnIndex, orders[ASK_SIDE][askNum].sharePrice, ASK_SIDE, symbolId);
        
        pricePointOffset += NYSE_BOOK_DELTA_MSG_PRICE_POINT_LEN;
        continue;
      }
      
      askNum++;
    }
    
    pricePointOffset += NYSE_BOOK_DELTA_MSG_PRICE_POINT_LEN;
  }
  
  if (tradingStatus == NYSE_BOOK_TRADING_STATUS_PRE_OPENING) return SUCCESS;
  
  int tradeRet;
  //-----------------------------------------------
  //  Now start to detect cross for BID side
  //-----------------------------------------------
  if (allowToTradeOnBID == 1)
  {
    for (i = bidNum-1; i >= 0; i--)
    {
      if (BookStatusMgmt[ecnIndex].isConnected == DISCONNECTED) return SUCCESS;  //If book is disconnected, we do not process remaining message in the buffer
        
      tradeRet = DetectCrossTrade(&orders[BID_SIDE][i], BID_SIDE, symbolId, ecnIndex, arrivalTime/1000, REASON_CROSS_ENTRY);
      if (tradeRet == TRADE_DETECTED_SELF_CROSSED)  //Self-cross on NYSE, so we delete ASK_SIDE
      {
        DeleteFromPrice(orders[BID_SIDE][i].sharePrice, ecnIndex, ASK_SIDE, symbolId);
      }
    }
  }
  
  //-----------------------------------------------
  //  Now start to detect cross for ASK side
  //-----------------------------------------------
  if (allowToTradeOnASK == 1)
  {
    for (i = 0; i < askNum; i++)
    {
      if (BookStatusMgmt[ecnIndex].isConnected == DISCONNECTED) return SUCCESS;  //If book is disconnected, we do not process remaining message in the buffer
      
      tradeRet = DetectCrossTrade(&orders[ASK_SIDE][i], ASK_SIDE, symbolId, ecnIndex, arrivalTime/1000, REASON_CROSS_ENTRY);
      
      if (tradeRet == TRADE_DETECTED_SELF_CROSSED)  //Self-cross on NYSE, so we delete BID_SIDE
      {
        DeleteFromPrice(orders[ASK_SIDE][i].sharePrice, ecnIndex, BID_SIDE, symbolId);
      }
    }
  }
  
  //-----------------------------------------------
  //  Now update quotes from buffer to stock book
  //-----------------------------------------------
  for (i = bidNum-1; i >= 0; i--)
  {
    if (BookStatusMgmt[ecnIndex].isConnected == DISCONNECTED) return SUCCESS;  //If book is disconnected, we do not process remaining message in the buffer
    
    NYSE_UpdateQuotesToStockBook(ecnIndex, &(orders[BID_SIDE][i]), symbolId, BID_SIDE);
  }
  
  for (i = 0; i < askNum; i++)
  {
    if (BookStatusMgmt[ecnIndex].isConnected == DISCONNECTED) return SUCCESS;  //If book is disconnected, we do not process remaining message in the buffer
    
    NYSE_UpdateQuotesToStockBook(ecnIndex, &(orders[ASK_SIDE][i]), symbolId, ASK_SIDE);
  }

  CheckBookCrossToEnableSymbol(symbolId);
  return SUCCESS;
}

/****************************************************************************
- Function name:  NYSE_DeleteQuoteFromStockBook
****************************************************************************/
int NYSE_DeleteQuoteFromStockBook(const unsigned char ecnIndex, double price, int side, int stockSymbolIndex)
{
  DeleteNysePriceLevel(ecnIndex, price, side, stockSymbolIndex);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  NYSE_UpdateQuotesToStockBook
****************************************************************************/
int NYSE_UpdateQuotesToStockBook(const unsigned char ecnIndex, t_OrderDetail *order, int stockSymbolIndex, int side)
{
  InsertOrUpdateNysePriceLevel(ecnIndex, order, side, stockSymbolIndex);

  return SUCCESS;
}

