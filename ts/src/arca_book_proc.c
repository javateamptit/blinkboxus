#define _BSD_SOURCE

#include <pthread.h>
#include <time.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>

#include "utility.h"
#include "book_mgmt.h"
#include "configuration.h"
#include "trading_mgmt.h"
#include "arca_book_proc.h"
#include "kernel_algorithm.h"
#include "aggregation_server_proc.h"
#include "hbitime.h"

/****************************************************************************
            GLOBAL VARIABLES
****************************************************************************/
t_ARCA_Book_Conf          ARCA_Book_Conf;
static double             _ARCA_PRICE_PORTION[10];
extern t_MonitorArcaBook  MonitorArcaBook;
static unsigned char      _arcaPriceScaleCode[MAX_STOCK_SYMBOL];
/****************************************************************************
            LIST OF FUNCTIONS
****************************************************************************/

/****************************************************************************
- Function name:  InitARCA_BOOK_DataStructure
- Input:      
- Output:                     
- Return:     
- Description:                                    
- Usage:      
****************************************************************************/
int InitARCA_BOOK_DataStructure(void)
{
  TraceLog(DEBUG_LEVEL, "Initialize ARCA Book\n");
  
  //Init lookup table
  _ARCA_PRICE_PORTION[0] = 1.0;
  _ARCA_PRICE_PORTION[1] = 0.1;
  _ARCA_PRICE_PORTION[2] = 0.01;
  _ARCA_PRICE_PORTION[3] = 0.001;
  _ARCA_PRICE_PORTION[4] = 0.0001;
  _ARCA_PRICE_PORTION[5] = 0.00001;
  _ARCA_PRICE_PORTION[6] = 0.000001;
  _ARCA_PRICE_PORTION[7] = 0.0000001;
  _ARCA_PRICE_PORTION[8] = 0.00000001;
  _ARCA_PRICE_PORTION[9] = 0.000000001;

  int i, j;
  
  MonitorArcaBook.procCounter = 0;
  for (i = 0; i < MAX_ARCA_GROUPS; i++)
  {
    MonitorArcaBook.recvCounter[i] = 0;
    
    for (j=0; j<DBLFILTER_NUM_QUEUES; j++)
      ARCA_Book_Conf.group[i].lossCounter[j] = 0;
  }

  // Initialize ARCA partial hashing
  int divisorIndex;
  int stockSymbolIndex;
  for (stockSymbolIndex = 0; stockSymbolIndex < MAX_STOCK_SYMBOL; stockSymbolIndex++)
  {
    for (divisorIndex = 0; divisorIndex < PRICE_HASHMAP_BUCKET_SIZE; divisorIndex++)
    {
      DeleteAllPricePointsFromHashSlot(Price_HashMap[stockSymbolIndex][BOOK_ARCA][ASK_SIDE][divisorIndex]);
      Price_HashMap[stockSymbolIndex][BOOK_ARCA][ASK_SIDE][divisorIndex] = NULL;
      
      DeleteAllPricePointsFromHashSlot(Price_HashMap[stockSymbolIndex][BOOK_ARCA][BID_SIDE][divisorIndex]);
      Price_HashMap[stockSymbolIndex][BOOK_ARCA][BID_SIDE][divisorIndex] = NULL;
    }
  }
  
  for (stockSymbolIndex = 0; stockSymbolIndex < MAX_STOCK_SYMBOL; stockSymbolIndex++)
  {
    SymbolStatus[stockSymbolIndex].count = 0;
  }
  
  if (LoadAllSymbolMappings(ARCA_BOOK_SYMBOL_INDEX_MAPPING_FILE) == ERROR)
  {
    return ERROR;
  }
  
  if (ARCA_Book_Conf.symbolMappingFp != NULL)
  {
    fclose(ARCA_Book_Conf.symbolMappingFp);
    ARCA_Book_Conf.symbolMappingFp = NULL;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ARCA_ProcessBookMessage
- Input:      
- Output:                     
- Return:     
- Description:                        
- Usage:      
****************************************************************************/
int ARCA_ProcessBookMessage(unsigned char *buffer, unsigned long arrivalTime, unsigned int timeStatusSeconds, uint32_t symbolId)
{
  if (BookStatusMgmt[BOOK_ARCA].isConnected == DISCONNECTED) return SUCCESS;  //If book is disconnected, we do not process remaining message in the buffer
  unsigned short msgType = *((unsigned short*)(buffer + 2));  
  switch (msgType)
  {
    case ARCA_ADD_MSG_BODY:
      if (ProcessDecodedAddMessage((gsxBook_Add_Message_Body*)buffer, arrivalTime, timeStatusSeconds, symbolId) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "ARCA Book: Problem in processing 'Add' msg\n");
        
        return ERROR;
      }
      break;
    case ARCA_MOD_MSG_BODY:
      if (ProcessDecodedModifyMessage((gsxBook_Modify_Message_Body*)buffer, arrivalTime, timeStatusSeconds, symbolId) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "ARCA Book: Problem in processing 'Modify' msg\n");
        
        return ERROR;
      }
      break;
    case ARCA_DEL_MSG_BODY:
      if (ProcessDecodedDeleteMessage((gsxBook_Delete_Message_Body *)buffer, arrivalTime, timeStatusSeconds, symbolId) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "ARCA Book: Problem in processing 'Delete' msg\n");
        
        return ERROR;
      }
      break;
    case ARCA_EXEC_MSG_BODY:
      if (ProcessDecodedExecuteMessage((gsxBook_Execute_Message_Body *)buffer, arrivalTime, timeStatusSeconds, symbolId) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "ARCA Book: Problem in processing 'Execute' msg\n");
        
        return ERROR;
      }
      break;
    case ARCA_SYMBOL_CLEAR_MSG:
      if (ProcessDecodedSymbolClearMessage((gsxBook_Symbol_Clear_Message_Body *)buffer, symbolId) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "ARCA Book: Problem in processing 'Symbol Clear' msg\n");
        return ERROR;
      }
      break;
    case ARCA_TRADING_SESSION_CHANGE_MSG:
      if (ProcessTradingSessionChangeMessage((gsxBook_Trading_Session_Change_Message_Body *)buffer, symbolId) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "ARCA Book: Problem in processing 'Trading Session Change' msg\n");
        return ERROR;
      }
      break;
    case ARCA_SECURITY_STATUS_MSG:
      ProcessSecurityStatusMessage((gsxBook_Security_Status_Message_Body *)buffer, arrivalTime, symbolId);
      break;
    case ARCA_SYMBOL_INDEX_MAPPING_MSG:
      TraceLog(WARN_LEVEL, "(ARCA XDP) Received Symbol Index Mapping msg in DBL filter\n");
      break;
    case ARCA_TRADE_MSG: 
      ProcessTradeMessage((gsxBook_Trade_Message_Body *)buffer, arrivalTime, symbolId);
      break;
    case ARCA_IMB_MSG_BODY:
    case ARCA_VENDOR_MAPPING_MSG:
    case ARCA_SOURCE_TIME_REF_MSG:
    case ARCA_TRADE_CANCELED_MSG:
    case ARCA_TRADE_CORRECTION_MSG:
    case ARCA_STOCK_SUMMARY_MSG:
    case ARCA_PBBO_MSG:
    case ARCA_ADD_REFRESH_MSG:
      break;
    default:
      TraceLog(ERROR_LEVEL, "ARCA Book: Unknown message type(%d)\n", msgType);
      return ERROR;
      break;
  }
  return SUCCESS;
}

/****************************************************************************
- Function name:  CheckArcaStaleData
****************************************************************************/
int CheckArcaStaleData(int stockSymbolIndex, unsigned long arcaNanos, unsigned long arrivalTime)
{
  // Check stale data
  long delay = arrivalTime - arcaNanos;
  
  if (delay > DataDelayThreshold[BOOK_ARCA])
  {
    if (SymbolMgmt.symbolList[stockSymbolIndex].isStale[BOOK_ARCA] == 0)
    {
      SymbolMgmt.symbolList[stockSymbolIndex].isStale[BOOK_ARCA] = 1;
      BuildSendStaleMarketNotification(BOOK_ARCA, stockSymbolIndex, delay);
      TraceLog(WARN_LEVEL, "ARCA Book: '%.8s' data is delayed for %ld nanoseconds\n", SymbolMgmt.symbolList[stockSymbolIndex].symbol, delay);
    }
  }
  else
  {
    if (SymbolMgmt.symbolList[stockSymbolIndex].isStale[BOOK_ARCA] == 0) return SUCCESS;
    
    if (SymbolMgmt.symbolList[stockSymbolIndex].isStale[BOOK_ARCA] == 1)
    {
      SymbolMgmt.symbolList[stockSymbolIndex].isStale[BOOK_ARCA] = 0;
      BuildSendStaleMarketNotification(BOOK_ARCA, stockSymbolIndex, delay);
      TraceLog(WARN_LEVEL, "ARCA Book: '%.8s' data is fast again\n", SymbolMgmt.symbolList[stockSymbolIndex].symbol);
    }
    else
    {
      SymbolMgmt.symbolList[stockSymbolIndex].isStale[BOOK_ARCA] = 0;
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessDecodedAddMessage
- Input:      
- Output:                     
- Return:     
- Description:                        
- Usage:      
****************************************************************************/
int ProcessDecodedAddMessage(struct gsxBook_Add_Message_Body *msg, unsigned long arrivalTime, unsigned int timeStatusSeconds, uint32_t symbolId)
{
  unsigned long arcaNanos = (timeStatusSeconds * TEN_RAISE_TO_9) + msg->sourceTimeNS;
  CheckArcaStaleData(symbolId, arcaNanos, arrivalTime);
  
  /* ARCA XDP now send orderID in 4 bytes, and also result in duplicate order ID
    So we need to find a way to avoid this duplication
    --> construct a new reference number base on the orderID + ARCA symbol index
    refNum = <4Bytes symbolIndex><4Bytes OrderID>
  */
  unsigned long refNum = msg->symbolIndex; 
  refNum = ((refNum << 32) | msg->orderID);
  
  t_OrderDetail order;
  order.shareVolume = msg->volume;
  order.sharePrice = msg->priceNumerator * _ARCA_PRICE_PORTION[_arcaPriceScaleCode[symbolId]];
  order.refNum = refNum;
  order.tradeSession = msg->tradeSession;
  
  // Ask order
  int side = (msg->side == 'S')?ASK_SIDE:BID_SIDE;
  
  if (SymbolMgmt.symbolList[symbolId].haltStatus[BOOK_ARCA] == TRADING_NORMAL)
  {
    int tradeRet = DetectCrossTrade(&order, side, symbolId, BOOK_ARCA, arrivalTime/1000, REASON_CROSS_ENTRY);
    
    if (tradeRet == TRADE_DETECTED_SELF_CROSSED)
    {
      DeleteFromPrice(order.sharePrice, BOOK_ARCA, !side, symbolId);
    }
  }
  
  /** Special note: Arca XDP may reuse the Order ID, so we must delete old order from the book
    before inserting new order.
    If we not do this, duplicate reference number (due to duplicate order id) will be inserted,
    so later, TS might crash.
    Before an Order ID is reused (inserted back to the book), the previous, same order id would be removed first,
    however, when gap occurs, we might missed the "Delete Order message", so duplicated order id might be inserted,
    The same situation applies to "Delete Order message" **/
    
  t_QuoteNodeInfo nodeInfo = OrderMapRemove(ARCA_OrderMap[_consumerId], refNum);

  if (nodeInfo.node != NULL && nodeInfo.node != DeletedQuotePlaceholder)
  {
    /* Duplicate order id is inserted (due to missed message(s))
      So, we must delete the old one from book, otherwise, possible of crash! */
    DeleteQuote(nodeInfo.node, BOOK_ARCA, nodeInfo.side, symbolId);
  }
  
  // Insert quote to book
  t_QuoteNode *retNode = InsertQuote(BOOK_ARCA, side, symbolId, &order);
  if (retNode)
  {
    OrderMapInsert(ARCA_OrderMap[_consumerId], refNum, retNode, msg->side);
  }
  else
    return ERROR;
  
  CheckBookCrossToEnableSymbol(symbolId);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessDecodedModifyMessage
- Input:      
- Output:                     
- Return:     
- Description:                        
- Usage:      
****************************************************************************/
int ProcessDecodedModifyMessage(struct gsxBook_Modify_Message_Body *msg, unsigned long arrivalTime, unsigned int timeStatusSeconds, uint32_t symbolId)
{
  unsigned long arcaNanos = (timeStatusSeconds * TEN_RAISE_TO_9) + msg->sourceTimeNS;
  CheckArcaStaleData(symbolId, arcaNanos, arrivalTime);
  
  /* ARCA XDP now send orderID in 4 bytes, and also result in duplicate order ID
    So we need to find a way to avoid this duplication
    --> construct a new reference number base on the orderID + ARCA symbol index
    refNum = <4Bytes symbolIndex><4Bytes OrderID>
  */
  unsigned long refNum = msg->symbolIndex; 
  refNum = ((refNum << 32) | msg->orderID);
    
  t_OrderDetail order;
  order.shareVolume = msg->volume;
  order.sharePrice = msg->priceNumerator * _ARCA_PRICE_PORTION[_arcaPriceScaleCode[symbolId]];
  order.refNum = refNum;
  order.tradeSession = 0;
  
  //Going to delete old quote, and then insert with new quote
  int side = (msg->side == 'S')? ASK_SIDE:BID_SIDE;
  t_QuoteNodeInfo nodeInfo = OrderMapRemove(ARCA_OrderMap[_consumerId], refNum);
  if (nodeInfo.node != NULL && nodeInfo.node != DeletedQuotePlaceholder)
  {
    DeleteQuote(nodeInfo.node, BOOK_ARCA, side, symbolId);
  }
  
  // finished deleting old quote
  
  // Now, detect cross, and then add modified quote to book
  if (SymbolMgmt.symbolList[symbolId].haltStatus[BOOK_ARCA] == TRADING_NORMAL)
  {
    int tradeRet = DetectCrossTrade(&order, side, symbolId, BOOK_ARCA, arrivalTime/1000, REASON_CROSS_ENTRY);
    
    if (tradeRet == TRADE_DETECTED_SELF_CROSSED)
    {
      DeleteFromPrice(order.sharePrice, BOOK_ARCA, !side, symbolId);
    }
  }
  
  t_QuoteNode *retNode = InsertQuote(BOOK_ARCA, side, symbolId, &order);
  if (retNode)
  {
    OrderMapInsert(ARCA_OrderMap[_consumerId], refNum, retNode, msg->side);
  }
  else
  {
    return ERROR;
  }
  
  CheckBookCrossToEnableSymbol(symbolId);
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessDecodedDeleteMessage
- Input:      
- Output:                     
- Return:     
- Description:                        
- Usage:      
****************************************************************************/
int ProcessDecodedDeleteMessage(struct gsxBook_Delete_Message_Body *msg, unsigned long arrivalTime, unsigned int timeStatusSeconds, uint32_t symbolId)
{
  unsigned long arcaNanos = (timeStatusSeconds * TEN_RAISE_TO_9) + msg->sourceTimeNS;
  CheckArcaStaleData(symbolId, arcaNanos, arrivalTime);
  
  /* ARCA XDP now send orderID in 4 bytes, and also result in duplicate order ID
    So we need to find a way to avoid this duplication
    --> construct a new reference number base on the orderID + ARCA symbol index
    refNum = <4Bytes symbolIndex><4Bytes OrderID>
  */
  
  unsigned long refNum = msg->symbolIndex; 
  refNum = ((refNum << 32) | msg->orderID);
  
  t_QuoteNodeInfo nodeInfo = OrderMapRemove(ARCA_OrderMap[_consumerId], refNum);

  if (nodeInfo.node != NULL && nodeInfo.node != DeletedQuotePlaceholder)
  {
    int side = (nodeInfo.side == 'S')?ASK_SIDE:BID_SIDE;
    DeleteQuote(nodeInfo.node, BOOK_ARCA, side, symbolId);
  }
  
  CheckBookCrossToEnableSymbol(symbolId);
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessDecodedExecuteMessage
- Input:      
- Output:                     
- Return:     
- Description:                        
- Usage:      
****************************************************************************/
int ProcessDecodedExecuteMessage(struct gsxBook_Execute_Message_Body *msg, unsigned long arrivalTime, unsigned int timeStatusSeconds, uint32_t symbolId)
{
  //Check reason code from message and do accordingly
  if (msg->reasonCode == 0) //Default: 0 � Not yet implemented, we ignore the message
  {
    return SUCCESS;
  }
    
  unsigned long arcaNanos = (timeStatusSeconds * TEN_RAISE_TO_9) + msg->sourceTimeNS;
  CheckArcaStaleData(symbolId, arcaNanos, arrivalTime);
  
  /* ARCA XDP now send orderID in 4 bytes, and also result in duplicate order ID
    So we need to find a way to avoid this duplication
    --> construct a new reference number base on the orderID + ARCA symbol index
    refNum = <4Bytes symbolIndex><4Bytes OrderID>
  */
  unsigned long refNum = msg->symbolIndex; 
  refNum = ((refNum << 32) | msg->orderID);
  
  t_QuoteNodeInfo nodeInfo = OrderMapRemove(ARCA_OrderMap[_consumerId], refNum);
  if (nodeInfo.node != NULL && nodeInfo.node != DeletedQuotePlaceholder)
  {
    int side = (nodeInfo.side == 'S')?ASK_SIDE:BID_SIDE;
    
    //Check reason code from message and do accordingly
    if (msg->reasonCode == 7) //7 � Partial Fill (Did not lose position)
    {
      int newShareVolume = nodeInfo.node->info.shareVolume - msg->volume;

      if (newShareVolume < 1)
      {
        DeleteQuote(nodeInfo.node, BOOK_ARCA, side, symbolId);
        CheckBookCrossToEnableSymbol(symbolId);
      }
      else
      {
        OrderMapInsert(ARCA_OrderMap[_consumerId], refNum, nodeInfo.node, nodeInfo.side);
        ModifyQuote(nodeInfo.node, newShareVolume, side, symbolId);
      }
    }
    else if (msg->reasonCode == 3)  //3 � Filled
    {
      //We will delete the symbol from Book
      DeleteQuote(nodeInfo.node, BOOK_ARCA, side, symbolId);
      CheckBookCrossToEnableSymbol(symbolId);
    }
    else
    {
      //Reason code problem!!!
      TraceLog(ERROR_LEVEL, "ARCA Book processing: Execute Msg, unknown reason code: %d\n", msg->reasonCode);
      return ERROR;
    }
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessDecodedSymbolClearMessage
- Input:      
- Output:                     
- Return:     
- Description:                        
- Usage:      
****************************************************************************/
int ProcessDecodedSymbolClearMessage(struct gsxBook_Symbol_Clear_Message_Body *msg, uint32_t symbolId)
{
  FlushVenueSymbol(BOOK_ARCA, symbolId);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessSecurityStatusMessage
- Input:      
- Output:                     
- Return:     
- Description:    Halt or resume symbol
- Usage:      
****************************************************************************/
void ProcessSecurityStatusMessage(struct gsxBook_Security_Status_Message_Body *msg, unsigned long arrivalTime, uint32_t symbolId)
{
  switch (msg->securityStatus)
  {
    case '4': // Trading Halt
      UpdateHaltStatusForASymbol(symbolId, TRADING_HALTED, arrivalTime/TEN_RAISE_TO_9, MAX_ECN_BOOK);
      break;
    case '5': // Resume
      UpdateHaltStatusForASymbol(symbolId, TRADING_NORMAL, arrivalTime/TEN_RAISE_TO_9, BOOK_ARCA);
      break;
    default:
      break;
  }
}


/****************************************************************************
- Function name:  ProcessTradeMessage
- Input:      
- Output:                     
- Return:     
- Description:    Resume symbol
- Usage:      
****************************************************************************/
void ProcessTradeMessage(struct gsxBook_Trade_Message_Body *msg, unsigned long arrivalTime, uint32_t symbolId)
{
  UpdateHaltStatusForASymbol(symbolId, TRADING_NORMAL, arrivalTime/TEN_RAISE_TO_9, BOOK_ARCA);
}

/****************************************************************************
- Function name:  ProcessTradingSessionChangeMessage
- Input:      
- Output:                     
- Return:     
- Description:                        
- Usage:      
****************************************************************************/
int ProcessTradingSessionChangeMessage(struct gsxBook_Trading_Session_Change_Message_Body *msg, uint32_t symbolId)
{
  // ------------------------------------------------
  //    Delete all expired order from book
  // ------------------------------------------------
  int needMoreDelete = 0;
  
  //Delete expired orders on ASK SIDE
  do{
    needMoreDelete = (DeleteAllExpiredArcaOrders(symbolId, msg->tradeSession, ASK_SIDE) == ERROR) ? 1 : 0;
  }
  while (needMoreDelete == 1);
    
  //Delete expired orders on BID SIDE
  needMoreDelete = 0;
  do {
    needMoreDelete = (DeleteAllExpiredArcaOrders(symbolId, msg->tradeSession, BID_SIDE) == ERROR) ? 1 : 0;
  }
  while (needMoreDelete == 1);  
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadAllSymbolMappings
- Input:      fileName
- Output:     
- Return:     
- Description:                                    
- Usage:      
****************************************************************************/
int LoadAllSymbolMappings(char *fileName)
{
  char fullPath[MAX_PATH_LEN] = "\0";
  
  if (GetFullConfigPath(fileName, CONF_SYMBOL, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Have some problem in getting config path for file: '%s'\n", fileName);
    return ERROR;
  }
  
  //---------------------------------------------------------------------------------
  // Check if file is downloaded today, if not, we should not connect to ARCA Book
  //---------------------------------------------------------------------------------
  int ret = IsFileTimestampToday(fullPath);
  if (ret == 2)
  {
    TraceLog(ERROR_LEVEL, "Could not find symbol mapping file: %s\n", fullPath);
    return ERROR;
  }
  else if (ret != 1)
  {
    TraceLog(ERROR_LEVEL, "ARCA Book: %s is old, please redownload the file\n", fullPath);
    return ERROR;
  }
      
  //Process the file for index mapping
  TraceLog(DEBUG_LEVEL, "Processing Arca symbol index mapping file...\n");
  
  FILE *fp = fopen(fullPath, "r");
  if (fp == NULL)
  {
    TraceLog(ERROR_LEVEL, "Critical! Could not open ARCA Symbol Index Mapping file (%s). Trade Server Could not continue.\n", fullPath);
    PrintErrStr(ERROR_LEVEL, "calling fopen(): ", errno);
    sleep(3); exit(0);
    return ERROR;
  }
  
  char line[512];
  memset(line, 0, 512);
  
  char *b, *e;
  
  while (fgets(line, 512, fp) != NULL)
  {
    //Parse the line
    /*  Line format:
         Symbol|CQS symbol|SymbolIndex|NYSE Market|Listed Market|TickerDesignation|UOT|PriceScaleCode|SystemID|Bloomberg BSID|Bloomberg Global ID
      Example of a line:
           ALP PRP|ALPpP|4421|N|N|A|100|4|0|627065504986|BBG000007MM8
      We need only following info: CQS symbol, Symbol Index + PriceScaleCode
    */
    
    /*Get CQS symbol*/
    b = strchr(line, '|');
    if (b == NULL)
    {
      TraceLog(ERROR_LEVEL, "ARCA Symbol Mapping file: Wrong line format: '%s'\n", line);
      memset(line, 0, 512);
      continue;
    }
    b++;
    
    //CQS symbol|SymbolIndex
    e = strchr(b, '|');
    if (e == NULL)
    {
      TraceLog(ERROR_LEVEL, "ARCA Symbol Mapping file: Wrong line format: '%s'\n", line);
      memset(line, 0, 512);
      continue;
    }
    *e = 0;
    
    //Now take the CQS format symbol
    char tmpSymbol[32];
    memset (tmpSymbol, 0, 32);
    strncpy(tmpSymbol, b, 31);
    
    char symbol[SYMBOL_LEN];
    memset(symbol, 0, SYMBOL_LEN);
    strncpy(symbol, tmpSymbol, SYMBOL_LEN);
    
    *e = '|';
  
    /*Get symbol index*/
    b = ++e;
    e = strchr(b, '|');
    if (e == NULL)
    {
      TraceLog(ERROR_LEVEL, "ARCA Symbol Mapping file: Wrong line format: '%s'\n", line);
      memset(line, 0, 512);
      continue;
    }
    *e=0;
    int symbolIndex = atoi(b);
    *e='|';
    
    /*bypass NYSE Market|Listed Market|TickerDesignation|UOT|*/
    e++;
    b = strchr(e, '|');
    if (b == NULL)  {TraceLog(ERROR_LEVEL, "ARCA Symbol Mapping file: Wrong line format: '%s'\n", line); memset(line, 0, 512); continue;}
    b++;
    
    b = strchr(b, '|');
    if (b == NULL)  {TraceLog(ERROR_LEVEL, "ARCA Symbol Mapping file: Wrong line format: '%s'\n", line); memset(line, 0, 512); continue;}
    b++;
    
    b = strchr(b, '|');
    if (b == NULL)  {TraceLog(ERROR_LEVEL, "ARCA Symbol Mapping file: Wrong line format: '%s'\n", line); memset(line, 0, 512); continue;}
    b++;
    
    b = strchr(b, '|');
    if (b == NULL)  {TraceLog(ERROR_LEVEL, "ARCA Symbol Mapping file: Wrong line format: '%s'\n", line); memset(line, 0, 512); continue;}
    b++;
    
    /*Get Price Scale Code*/
    e = strchr(b, '|');
    if (e == NULL)  {TraceLog(ERROR_LEVEL, "ARCA Symbol Mapping file: Wrong line format: '%s'\n", line); memset(line, 0, 512); continue;}
    *e = 0;
    int priceScale = atoi(b);
    *e = '|';
    
    /*****************************
      Add to data structure
    *****************************/
    if (symbolIndex <= 0 || symbolIndex >= MAX_ARCA_STOCK_SYMBOL)
    {
      TraceLog(ERROR_LEVEL, "ARCA symbol mapping: stock symbol (%.8s) index (%d) less than 0 or greater than defined const MAX_ARCA_STOCK_SYMBOL (%d)\n", symbol, symbolIndex, MAX_ARCA_STOCK_SYMBOL);
      memset(line, 0, 512);
      continue;
    }
    
    if ((strlen(tmpSymbol) > SYMBOL_LEN) || (strstr(tmpSymbol, ".TEST")))
    {
      TraceLog(WARN_LEVEL, "ARCA XDP: Ignore symbol: %s\n", tmpSymbol);
      memset(line, 0, 512);
      continue;
    }
    
    // Only enable symbols that is in configured range
    if (SplitSymbolMapping[(int)symbol[0]] == DISABLE)
    {
      memset(line, 0, 512);
      continue;
    }
    
    // Update Symbol String
    int stockSymbolIndex = GetStockSymbolIndex(symbol);
      
    // If stock symbol is not in SymbolList
    if (stockSymbolIndex == -1)
    {
      // Try to add it to Symbol List
      stockSymbolIndex = UpdateStockSymbolIndex(symbol);
    }

    // Check duplicate stock symbol
    if (stockSymbolIndex != -1)
    {
      SymbolStatus[stockSymbolIndex].count++;
      _arcaPriceScaleCode[stockSymbolIndex] = priceScale;   
      if (SymbolStatus[stockSymbolIndex].count > 1)
      {
        TraceLog(ERROR_LEVEL, "ARCA XDP Symbol Mapping file: Duplicate stock symbol index %.8s\n", symbol);
      }
    }
    
    memset(line, 0, 512);
  }

  fclose(fp);
  TraceLog(DEBUG_LEVEL, "Arca symbol index mapping file has been processed.\n");
  return SUCCESS;
}
