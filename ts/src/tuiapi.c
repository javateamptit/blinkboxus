/****************************************************************************
** Include files and define several variables
****************************************************************************/
#include "tuiapi.h"

#include "utility.h"
#include "tuiutil.h"
#include "manual.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <ctype.h>
#include <readline/readline.h>
#include <readline/history.h>

/*--------------------------- Local variables --------------------------------*/
tCmdTable cmdTab[] = 
        {
         {"help", "Display command help ", "help ", 0, Help},
         {"oec_connect", "Connect to oec", "oec_connect <arca/ouch/bx/rash/edgx/edga/batsz/nyse/byx/psx>", 1, ConnectOec},
         {"mdc_connect", "Connect to mdc", "mdc_connect <arca/nasdaq/edgx/edga/batsz/nyse/bx/byx/psx/amex>", 1, ConnectMdc},
         {"oec_disconnect", "Disconnect from oec", "oec_disconnect <arca/ouch/bx/rash/edgx/edga/batsz/nyse/byx/psx>", 1, DisconnectOec},
         {"mdc_disconnect", "Disconnect from mdc", "mdc_disconnect <arca/nasdaq/edgx/edga/batsz/nyse/bx/byx/amex>", 1, DisconnectMdc},
         {"symbol_with_quotes", "Show Symbols Have Quotes", "s <arca/nasdaq/edgx/edga/batsz/nyse/bx/byx/psx/amex>", 1, ShowSymbolsHaveQuotes},
         {"symbol_status", "View Stock Status", "ss <symbol>", 1, ViewStockStatus},
         {"halt_status", "View Halt Status", "hs <symbol>", 1, ViewSymbolHaltStatus},
         {"arca_new", "Send new order", "arca_new <buy/sell/short> <symbol> <volume> <price>", 4, __Arca_NewOrder},
         {"ouch_new", "Send new order", "ouch_new <buy/sell/short> <symbol> <volume> <price>", 4, __Ouch_NewOrder},
         {"rash_new", "Send new order", "rash_new <buy/sell/short> <symbol> <volume> <price>", 4, __Rash_NewOrder},
         {"nyse_new", "Send new order", "nyse_new <buy/sell/short> <symbol> <volume> <price>", 4, __Nyse_NewOrder},
         {"edgx_new", "Send new order", "edgx_new <buy/sell/short> <symbol> <volume> <price>", 4, __Edgx_NewOrder},
         {"edgx_new_ext", "Send new extended order", "edgx_new_ext <buy/sell/short> <symbol> <suffix> <volume> <price>", 5, __Edgx_NewExtOrder},
         {"edga_new", "Send new order", "edga_new <buy/sell/short> <symbol> <volume> <price>", 4, __Edga_NewOrder},
         {"edga_new_ext", "Send new extended order", "edga_new_ext <buy/sell/short> <symbol> <suffix> <volume> <price>", 5, __Edga_NewExtOrder},
         {"bats_new", "Send new order", "bats_new <buy/sell/short> <symbol> <volume> <price>", 4, __Bats_NewOrder},
         {"byx_new", "Send new order", "byx_new <buy/sell/short> <symbol> <volume> <price>", 4, __BYX_NewOrder},
         {"bx_new", "Send new order", "bx_new <buy/sell/short> <symbol> <volume> <price>", 4, __Bx_NewOrder},
         {"psx_new", "Send new order", "psx_new <buy/sell/short> <symbol> <volume> <price>", 4, __Psx_NewOrder},
         {"exit / quit", "Exit normally", "exit / quit", 0, NULL},
        };

unsigned char  cmdTabLen = (unsigned char)(sizeof(cmdTab)/ sizeof(cmdTab[0]));

FILE *myStdOut = NULL;

/*****************************************************************************
Function Name:  TuiBaseCmdProcess
Description: Perform a basic command
Parameters: argc : char --> number of parameter
      argv : char** --> array of parameter
Return value: 0 if success
        > 0 if not found extended command
        < 0 if fail to perform
*****************************************************************************/
int TuiBaseCmdProcess(char argc, char **argv)
{
  int code;
  int i;
  int numpar;
  
  if (argc < 1 || argv == NULL) 
  {
    return -1;  // Not enough argument
  }
  code = -1;
  for(i = 0; i < cmdTabLen; i++)
  {
    if (strcmp(argv[0], cmdTab[i].commandName) == 0)
    {
      code = i;
      break;
    }
  }
  
  if ((code >= 0) && (code < cmdTabLen)) //Right command
  {   
    /* Wrong number of parameter */
    numpar = (int)argc - 1;
    if ((cmdTab[code].numParam != cMaxParam) && (cmdTab[code].numParam != numpar))
    {
      mFailed("\n\tWrong number of parameter!\n");
      return -1;
    }
    if ((cmdTab[code].CmdHandler(argc, argv)) == 1)
    {
      mFailed("\n\tCan't perform this command !.\n");
      return -1;
    }
    return 0;
  }
  return 1; // Not found command
}

/*****************************************************************************
Function Name:  TuiCmdProcess
Description: Perform a command
Parameters: argc : char --> number of parameter
      argv : char** --> array of parameter
Return value: 0 if success do command except "exit"
        1 if do command "exit"
        -1 if fail
*****************************************************************************/
int TuiCmdProcess(char argc, char **argv)
{
  char *strExit = "exit";
  char *strQuit = "quit";
  int exit_code = 0;
  int child = -1;
  int base = 0;

  int numparam;
  int i;
  bool isSwap ;
  
  isSwap = false;
  if (argc < 1 || argv == NULL) 
  {
    return -1;  // Not enough argument  
  }
  
  if ((strcmp(argv[0], strExit) == 0) || (strcmp(argv[0], strQuit) == 0))
  {
    if (argc == 1)
    {
      printf("Processing command '%s'\n", argv[0]);
      return 1; // Exit
    }
    mFailed("\n\tWrong exit command!\n");
    return -1;
  }
  
  numparam = argc;
  if (argc > 2)
  {
    for (i = 0; i < strlen(argv[argc - 2]); i++)
    {
      if (argv[argc - 2][i] == '>')
      {
        isSwap = true;
        numparam = argc - 2;
        myStdOut = stdout;
        stdout = fopen(argv[argc - 1],"w");
        
        if (stdout == NULL)
        {
          stdout = myStdOut;
          isSwap = false;
          printf("Can't create file : '%s'\n",argv[argc-1]);
        }
      }
      
      if (isspace(argv[argc - 2][i]) != 0)
      {
        break;
      }
    }
  }
  
  base = TuiBaseCmdProcess(numparam, argv); // Try perform basic command  
  if (base > 0)  // Not found basic command
  {
    /* Perhaps Linux commands */
    if ((child = fork()) < 0) 
    {
      perror("Error creating child process");
      return -1;
    }
    else if (child == 0) // Child process
    {
      /* execute here */
      /* execvp */
      if (execvp(argv[0], argv) < 0)
      {       
        fprintf(stderr, "Shell run : %s: ", argv[0]);
        perror("");
        exit(1);
      }
    }
    else
    {
      /* parent here */   
      if (waitpid(child, &exit_code, 0) < 0 ) // Wait for process termination.
      {
        fprintf(stderr, "ATVN Shell: waiting for child %d: ", child);
        perror("");       
      }
      if (exit_code != 0)
      {
        return -1;
      }     
    }
  }
  else if (base < 0) // Found basic command but fail to perform
  {
    if (isSwap)
    {
      fclose(stdout);
      stdout = myStdOut;
      isSwap = false;
    }
    return -1;
  }
  
  if (isSwap)
  {
    fclose(stdout);
    stdout = myStdOut;
    isSwap = false;
  } 
  return 0;
}
  
/*****************************************************************************
Function Name:  Help
Description: Print descriptions of command table
Parameters: argc : char --> must be 1
      argv : char** --> argv[0] = "help"
Return value:   0 if right parameter
        1 if wrong parameter
*****************************************************************************/
int Help(char argc, char **argv)
{
  byte i;
  char temp[256];
  
  if (argc != 1)
  {
    return 1;
  }
  
  printf("\n============================= Help =====================================\n");
  memset(temp, 32, 192);
  strncpy(temp, "Command name", strlen("Command name"));
  temp[30] = '|';
  strncpy(&temp[32], "Description", strlen("Description"));
  temp[110] = '|';
  strncpy(&temp[112], "Usage", strlen("Usage"));
  temp[192] = 0;
  printf("%s\n", temp);
  
  for (i = 0; i < cmdTabLen; i++)
  {
    //printf("%.24s|%.80s|%.80s\n", cmdTab[i].commandName, cmdTab[i].description, cmdTab[i].usage);   
    memset(temp, 32, 192);
    strncpy(temp, cmdTab[i].commandName, strlen(cmdTab[i].commandName));
    temp[30] = '|';
    strncpy(&temp[32], cmdTab[i].description, strlen(cmdTab[i].description));
    temp[110] = '|';
    strncpy(&temp[112], cmdTab[i].usage, strlen(cmdTab[i].usage));
    temp[192] = 0;
    printf("%s\n", temp);
  }
  return 0;
}

/****************************************************************************
- Function name:  TuiMain
- Input:      
- Output:   
- Return:   
- Description:  
****************************************************************************/
void TuiMain(int *isStop, char *strDefaultPrompt)
{
  char *cmdBuf; 
  char agc; 
  char **pArgv = NULL;
  int i;
  
  cmdBuf = readline(strDefaultPrompt);
  if (cmdBuf == NULL || strlen(cmdBuf) == 0)
  {
    return;
  }
  
  add_history(cmdBuf);
  
  pArgv = ToToken(cmdBuf, &agc);
  if (pArgv == NULL)
  {
    return;
  }
  
  if (( TuiCmdProcess(agc, pArgv)) == 1)
  {
    *isStop = 1;
  }
  
  for (i = 0; i < agc; i++)
  {
    free(pArgv[i]);
  }
  free(pArgv);
  free(cmdBuf);
  
  return;
}
