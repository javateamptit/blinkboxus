/****************************************************************************
** Project name:  Equity Arbitrage Application
** File name:   order_mgmt.c
** Description:   This file contains function definitions that were declared
        in order_mgmt.h

** Author:    Sang Nguyen-Minh
** First created on 15 September 2007
** Last updated on 15 September 2007
****************************************************************************/

/****************************************************************************
** Include files
****************************************************************************/
#include "order_mgmt.h"

#include "arca_direct_proc.h"
#include "ouch_oec_proc.h"
#include "nasdaq_rash_proc.h"
#include "nyse_ccg_proc.h"
#include "bats_boe_proc.h"
#include "edge_order_proc.h"

#include "kernel_algorithm.h"
#include "hbitime.h"
#include "dbl_filter_wrapper.h"
#include "trading_mgmt.h"
#include "topic.h"
#include "stock_symbol.h"

/****************************************************************************
            GLOBAL VARIABLES
****************************************************************************/
t_OrderRawDataMgmt  OrderMgmt[MAX_ECN_ORDER];
t_OrderStatus       OrderStatusMgmt[MAX_ECN_ORDER];

extern volatile int IsTradeServerStopped;

extern t_QueueSymbolList QueueSymbolList[DBLFILTER_NUM_QUEUES];

/****************************************************************************
            LIST OF FUNCTIONS
****************************************************************************/

// Begin shim functions

static void register_oe_qsymbol(int queue_id, int stock_symbol_index, char *symbol, int venue_id, int session_id)
{
  uint32_t topic_id = gen_oe_topic_id(venue_id, stock_symbol_index);
  register_oe_qsymbol_topic(DBLFilterMgmt.topic_registry, topic_id, symbol, VenueIdToText[venue_id], session_id, topic_id);
  register_queue_for_topic_id(DBLFilterMgmt.topic_registry, queue_id, topic_id);
}

// End shim functions

/****************************************************************************
- Function name:  StartOrderEntryReadThread
****************************************************************************/
void *StartOrderEntryReadThread(void *args)
{
  SetKernelAlgorithm("[OEFILTER_RECEIVE]");
  if (DBLFilterMgmt.topic_registry != NULL)
  {
    TraceLog(DEBUG_LEVEL, "Starting OE Filter Receive Thread.\n");
                startReaderWithOffset(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, -hbitime_get_offset_micros() * 1000);
  }
  else 
  {
    TraceLog(ERROR_LEVEL, "Invalid OE Device.  Could not start OE Receive thread.\n");
  }

  TraceLog(FATAL_LEVEL, "OE Filter Receive Thread exited! Restart Trade Server!\n"); 
  return NULL;
}

/****************************************************************************
- Function name:  StartOrderEntryWriteThread
****************************************************************************/
void *StartOrderEntryWriteThread(void *args)
{
  SetKernelAlgorithm("[OEFILTER_SEND]");
  if(DBLFilterMgmt.topic_registry != NULL)
  {
    TraceLog(DEBUG_LEVEL, "Starting OE Filter Send Thread.\n");
    startWriter(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config);
  }
  else
  {
    TraceLog(ERROR_LEVEL, "Invalid OE Device.  Could not start OE Send thread.\n");
  }
  
  TraceLog(FATAL_LEVEL, "OE Filter Send Thread exited! Restart Trade Server!\n");
  return NULL;
}  

/****************************************************************************
- Function name:  StartOrderEntryLogConsumerThread
****************************************************************************/
void *StartOrderEntryLogConsumerThread(void *args)
{
  SetKernelAlgorithm("[OEFILTER_LOG_CONSUMER]");
  if(DBLFilterMgmt.topic_registry != NULL)
  {
    TraceLog(DEBUG_LEVEL, "Starting OE Filter Log Processing Thread.\n");
    while(1)
    {
      spscSyncNumReadableItems(OEFilterMgmt.orderEntryLogReceiveReader);
      int32_t buffer_readable_bytes = spscGetNumReadableItems(OEFilterMgmt.orderEntryLogReceiveReader);
      int32_t length = buffer_readable_bytes;
      const uint8_t *buffer_ptr = spscReadPtr(OEFilterMgmt.orderEntryLogReceiveReader);

      while(buffer_readable_bytes > 0)
      {
        //TraceLog(DEBUG_LEVEL, "log reader has %d bytes available, entering loop\n", buffer_readable_bytes);
        const uint8_t *start_ptr = buffer_ptr;

        uint32_t topic_id = *((uint32_t *) buffer_ptr);
        buffer_ptr += 4;

        char msg_type[5];
        strncpy(msg_type, (char*)buffer_ptr, 4);
        buffer_ptr += 4;

        if(strncmp(msg_type, "rlem", 4) == 0)
        {
          // raw log message
          uint32_t session_id = *((uint32_t*)buffer_ptr);
          // uint64_t msg_time = *((uint64_t*)(buffer_ptr + 4));
          // log levels: 0 - trace, 1 - debug, 2 - info, 3 - warn, 4 - error
          uint32_t log_level = *((uint32_t*)(buffer_ptr + 12));
          uint32_t msg_len = *((uint32_t*)(buffer_ptr + 16));

          
          TraceLog(WARN_LEVEL, "topic: %d, session_id: %d, log_level: %d,  msg: %.*s\n", topic_id, session_id, log_level, msg_len, buffer_ptr+20);

          buffer_ptr += (msg_len + 20);

          
        }
        else if(strncmp(msg_type, "dscn", 4) == 0) 
        {
          // disconnect message 
          uint32_t venue_id = *((uint32_t*)buffer_ptr);       
          // uint32_t session_id = *((uint32_t*)(buffer_ptr + 4));
          // uint64_t time = *((uint64_t*)(buffer_ptr + 8));
          buffer_ptr += 16;
          int oeVenueId = -1;
          int oeServiceId = -1;

          switch(venue_id) 
          {
            case VENUE_BATSZ:
              OecConfig[ORDER_BATSZ_BOE].socket = -1;
              oeVenueId = ORDER_BATSZ_BOE;
              oeServiceId = SERVICE_BATSZ_BOE;
              break;
            
            case VENUE_BYX:
              OecConfig[ORDER_BYX_BOE].socket = -1;
              oeVenueId = ORDER_BYX_BOE;
              oeServiceId = SERVICE_BYX_BOE;
              break;

            case VENUE_EDGX:  
              OecConfig[ORDER_EDGX].socket = -1;
              oeVenueId = ORDER_EDGX;
              oeServiceId = SERVICE_EDGX_ORDER;
              break;

            case VENUE_EDGA:  
              OecConfig[ORDER_EDGA].socket = -1;
              oeVenueId = ORDER_EDGA;
              oeServiceId = SERVICE_EDGA_ORDER;
              break;

            case VENUE_ARCA:  
              OecConfig[ORDER_ARCA_DIRECT].socket = -1;
              oeVenueId = ORDER_ARCA_DIRECT;
              oeServiceId = SERVICE_ARCA_DIRECT;
              break;

            case VENUE_NYSE: 
              OecConfig[ORDER_NYSE_CCG].socket = -1;
              oeVenueId = ORDER_NYSE_CCG;
              oeServiceId = SERVICE_NYSE_CCG;
              break;

            case VENUE_NASDAQ:       
              OecConfig[ORDER_NASDAQ_OUCH].socket = -1;
              oeVenueId = ORDER_NASDAQ_OUCH;
              oeServiceId = SERVICE_NASDAQ_OUCH;
              break;

            case VENUE_RASH:  
              OecConfig[ORDER_NASDAQ_RASH].socket = -1;
              oeVenueId = ORDER_NASDAQ_RASH;
              oeServiceId = SERVICE_NASDAQ_RASH;
              break;

            case VENUE_NDBX:
              OecConfig[ORDER_NASDAQ_BX].socket = -1;
              oeVenueId = ORDER_NASDAQ_BX;
              oeServiceId = SERVICE_NASDAQ_BX;
              break;
              
            case VENUE_PSX:
              OecConfig[ORDER_NASDAQ_PSX].socket = -1;
              oeVenueId = ORDER_NASDAQ_PSX;
              oeServiceId = SERVICE_NASDAQ_PSX;
              break;

            default:
              oeVenueId = -1;
              oeServiceId = -1;
              break;
          }

          if(oeVenueId >= 0) 
          {
            int prevStatus = OrderStatusMgmt[oeVenueId].isConnected;
            OrderStatusMgmt[oeVenueId].isConnected = DISCONNECTED;
            OrderStatusMgmt[oeVenueId].shouldConnect = NO;

            //notify AS that order is disconnect
            if (prevStatus == CONNECTED)
            {
              if (ASConfig.socket != -1)
              {
                if (BuildSendServiceDisconnect(oeServiceId) == SUCCESS)
                {
                  TraceLog(DEBUG_LEVEL, "Notify disconnect venue %d is sent to AS\n", venue_id);
                }
              }
            }

            if (*(TradingStatus.status[oeVenueId]) == ENABLE)
            {
              *(TradingStatus.status[oeVenueId]) = DISABLE;

              //notify AS that trading is disabled
              if (ASConfig.socket != -1)
              {
                if (BuildSendTradingDisabled(oeServiceId, TRADING_DISABLED_ORDER_DISCONNECT) == SUCCESS)
                {
                  TraceLog(DEBUG_LEVEL, "Notify trading disabled of venue %d is sent to AS\n", venue_id);
                }
              }
            } 

            TraceLog(DEBUG_LEVEL, "venue %d: Connection to Server was disconnected\n", venue_id);            
          }
          else
          {
            TraceLog(ERROR_LEVEL, "OEFilter received disconnect notification for unknown venue id: %d\n", venue_id);
          }
        }
        else
        {
          TraceLog(FATAL_LEVEL, "OEFilter (Process Log Message) - Received unknown log message type (%.4s)\n", msg_type);
        } 

        int32_t bytes_read = buffer_ptr - start_ptr;
        buffer_readable_bytes -= bytes_read;
      }

      spscCommitItemsRead(OEFilterMgmt.orderEntryLogReceiveReader, length);

      usleep(100000);
    }
  }
  else
  {
    TraceLog(FATAL_LEVEL, "Invalid OE Device.  Could not start OE Log Consumer thread.\n");
  }

  TraceLog(FATAL_LEVEL, "OE Log Consumer Thread exited! Restart Trade Server!\n");

  return NULL;
}

/****************************************************************************
- Function name:  StartOrderEntryAdminConsumerThread
****************************************************************************/
void *StartOrderEntryAdminConsumerThread(void *args)
{
  SetKernelAlgorithm("[OEFILTER_ADMIN]");
  if(DBLFilterMgmt.topic_registry != NULL)
  {
    TraceLog(DEBUG_LEVEL, "Starting OE Filter Admin Processing Thread.\n");

    while(1)
    {
      /**
      check order entry admin receive queue
      **/  

      spscSyncNumReadableItems(OEFilterMgmt.orderEntryAdminReceiveReader);
      int32_t buffer_readable_bytes = spscGetNumReadableItems(OEFilterMgmt.orderEntryAdminReceiveReader);
      const uint8_t *buffer_ptr = spscReadPtr(OEFilterMgmt.orderEntryAdminReceiveReader);

      while(buffer_readable_bytes > 0)
      {
        //TraceLog(DEBUG_LEVEL, "admin reader has %d bytes available, entering loop\n", buffer_readable_bytes);

        /** Header format: Total size = 12 bytes
          Offset  Field Name    Size & Format                     Description
          0       topic_id      binary 4 byte                     topic_id encodes symbol and venue
          4       arrival_time  binary 8 bytes (Little Endian)    this is the long timestamp
          12      message       binary unknown length
        **/

        uint32_t topic_id = *((uint32_t*) buffer_ptr);
        int venue_id = venue_from_admin_topic_id(topic_id);
        uint64_t arrival_time = *((uint64_t*)(buffer_ptr+4));   // this is the long timestamp of the tcp message receipt

        unsigned short bytes_processed = ProcessOrderEntryMessage(venue_id, MAX_STOCK_SYMBOL, arrival_time, buffer_ptr + DBL_OE_HDR_LEN); // dummy symbol

        if(bytes_processed <= 0)
        {
          TraceLog(FATAL_LEVEL, "OE Admin Consumer Thread could not process order message from venue: %d bytes_available: %d\n", venue_id, buffer_readable_bytes);
          return NULL;
        }

        int bytes_consumed = bytes_processed + DBL_OE_HDR_LEN;
        //TraceLog(DEBUG_LEVEL, "admin reader processed %d bytes of admin message, incrementing pointer by %d\n", bytes_processed, bytes_consumed);

        buffer_ptr += bytes_consumed;
        buffer_readable_bytes -= bytes_consumed;  

        //TraceLog(DEBUG_LEVEL, "admin reader comitting %d bytes read\n", bytes_consumed);
        spscCommitItemsRead(OEFilterMgmt.orderEntryAdminReceiveReader, bytes_consumed);        
      }

      usleep(100000);
    } 
  }
  else
  {
    TraceLog(FATAL_LEVEL, "Invalid OE Device.  Could not start OE Admin Consumer thread.\n");
  }                                                                                          

  TraceLog(FATAL_LEVEL, "OE Admin Consumer Thread exited!\n");
  return NULL;
}

/****************************************************************************
- Function name:  StartDataBlockAggregationThread
****************************************************************************/
void *StartDataBlockAggregationThread(void *args)
{
  SetKernelAlgorithm("[OE_DATABLOCK_AGGREGATOR]");
  TraceLog(DEBUG_LEVEL, "Starting Data Block Aggregation Thread.\n");
  
  const uint8_t *buffer_ptr;
  spsc_read_handle reader;
  
  int32_t buffer_readable_bytes;
  int dataBlockSize = sizeof(t_DataBlock);
  int i, bytes_consumed;
  uint16_t venue;
  
  while(1){
    for(i=0; i<DBLFILTER_NUM_QUEUES; i++) {
      reader = FilterBuffers[i].dataBlockReader;
      spscSyncNumReadableItems(reader);
        buffer_readable_bytes = spscGetNumReadableItems(reader);
      buffer_ptr = spscReadPtr(reader);
      
      while(buffer_readable_bytes > 0) {
        venue = *((uint16_t*)buffer_ptr); 
        AddDataBlockToCollection(buffer_ptr+2, &OrderMgmt[venue]);
        
        bytes_consumed = dataBlockSize + 2;
        buffer_ptr += bytes_consumed;
        buffer_readable_bytes -= bytes_consumed;
        spscCommitItemsRead(reader, bytes_consumed);
      }
    }
    usleep(100000);
  }
  
  TraceLog(FATAL_LEVEL, "Data Block Aggregation Thread Exited!\n");
  return NULL;  
}

/****************************************************************************
- Function name:  StartTradeOutputAggregationThread
****************************************************************************/
void *StartTradeOutputAggregationThread(void *args)
{
  SetKernelAlgorithm("[TRADE_OUTPUT_AGGREGATOR]");
  TraceLog(DEBUG_LEVEL, "Starting Trade Output Aggregation Thread.\n");
  
  spsc_read_handle reader;
  const uint8_t *buffer_ptr;
  char *logEntry;
  int32_t buffer_readable_bytes;
  
  int i, logEntryIndex;
  while(1) {
    for(i=0; i<DBLFILTER_NUM_QUEUES; i++) {
      reader = FilterBuffers[i].tradeOutputReader;
      spscSyncNumReadableItems(reader);
      buffer_readable_bytes = spscGetNumReadableItems(reader);
      buffer_ptr = spscReadPtr(reader);
      
      while(buffer_readable_bytes >= MAX_LEN_PER_LOG_ENTRY) {
        logEntryIndex = __sync_fetch_and_add(&TradeOutputLogMgmt.countLogEntry, 1);
        logEntry = TradeOutputLogMgmt.tradeOutputLogCollection[logEntryIndex % MAX_LOG_ENTRY];

        memcpy(logEntry, buffer_ptr, MAX_LEN_PER_LOG_ENTRY);
        buffer_ptr += MAX_LEN_PER_LOG_ENTRY;
        buffer_readable_bytes -= MAX_LEN_PER_LOG_ENTRY;
        spscCommitItemsRead(reader, MAX_LEN_PER_LOG_ENTRY);
      }
    }
    usleep(100000);
  }
  
  TraceLog(FATAL_LEVEL, "Trade Output Aggregation Thread Exited!\n");
  return NULL;  
}

/****************************************************************************
- Function name:  InitializeOrderEntryComponents
****************************************************************************/
int InitializeOrderEntryComponents()
{
  TraceLog(DEBUG_LEVEL, "Initializing DBL Filter Order Entry Components...\n");

  int i;

  //Note: nano_time is not attached to the wall clock, that's why we do this instead of just calling nano_time.
  // struct timeval tv;
  // gettimeofday(&tv, 0);
  // hbitime_micros_parts((int*)&tv.tv_sec, (int*)&tv.tv_usec); // Should use this function instead of gettimeofday
  // long now = (tv.tv_sec * 1000000000L + tv.tv_usec * 1000L); 

  // setup log queue: this is where order entry related error messages will be delivered
  OEFilterMgmt.orderEntryLogReceiveQueue = spscAllocate(DBL_FILTER_LOG_SIZE);
  OEFilterMgmt.orderEntryLogReceiveSeq = (long *) calloc(1, sizeof(long) * 1024); // buffer used to store reader/writer indexes without crossing page boundaries
  memset(OEFilterMgmt.orderEntryLogReceiveSeq, 0, (sizeof(long) * 1024));    
  OEFilterMgmt.orderEntryLogReceiveReader = createSpscReader(OEFilterMgmt.orderEntryLogReceiveQueue, DBL_FILTER_LOG_SIZE, OEFilterMgmt.orderEntryLogReceiveSeq);

  // setup admin queue: this is where non symbol specific messages will be delivered (e.g. login/logout messages and heartbeats)
  OEFilterMgmt.orderEntryAdminReceiveQueue = spscAllocate(DBLFILTER_OE_QUEUE_SIZE);
  OEFilterMgmt.orderEntryAdminReceiveSeq = (long *) calloc(1, sizeof(long) * 1024); // buffer used to store reader/writer indexes without crossing page boundaries
  memset(OEFilterMgmt.orderEntryAdminReceiveSeq, 0, (sizeof(long) * 1024));  
  OEFilterMgmt.orderEntryAdminReceiveReader = createSpscReader(OEFilterMgmt.orderEntryAdminReceiveQueue, DBLFILTER_OE_QUEUE_SIZE, OEFilterMgmt.orderEntryAdminReceiveSeq);

  init_oe_logging_queue(DBLFilterMgmt.topic_registry, (int64_t) OEFilterMgmt.orderEntryLogReceiveQueue, DBL_FILTER_LOG_SIZE, (int64_t) OEFilterMgmt.orderEntryLogReceiveSeq);

  for(i = 0; i < DBLFILTER_NUM_QUEUES+1; i++)
  {
    // OE queue indexes follow MD queue indexes
    register_spsc_queue(DBLFilterMgmt.topic_registry, i + DBLFILTER_NUM_QUEUES, (long) FilterBuffers[i].orderEntryReceiveQueue, DBLFILTER_OE_QUEUE_SIZE, (long) FilterBuffers[i].orderEntryReceiveSeq);
    register_oe_log_queue(DBLFilterMgmt.topic_registry, i + DBLFILTER_NUM_QUEUES);
    register_spsc_send_queue(DBLFilterMgmt.topic_registry, i, (long) FilterBuffers[i].orderEntrySendQueue, DBLFILTER_OE_QUEUE_SIZE, (long) FilterBuffers[i].orderEntrySendSeq);
  } 

  OEFilterMgmt.oe_network_config = createOrderEntryNetworkConfig();

  setAdminTopicInfo(OEFilterMgmt.oe_network_config, VenueIdToText[VENUE_ARCA],   gen_admin_topic_id(VENUE_ARCA));
  setAdminTopicInfo(OEFilterMgmt.oe_network_config, VenueIdToText[VENUE_NASDAQ], gen_admin_topic_id(VENUE_NASDAQ));
  setAdminTopicInfo(OEFilterMgmt.oe_network_config, VenueIdToText[VENUE_NYSE],   gen_admin_topic_id(VENUE_NYSE));
  setAdminTopicInfo(OEFilterMgmt.oe_network_config, VenueIdToText[VENUE_BATSZ],  gen_admin_topic_id(VENUE_BATSZ));
  setAdminTopicInfo(OEFilterMgmt.oe_network_config, VenueIdToText[VENUE_EDGX],   gen_admin_topic_id(VENUE_EDGX));
  setAdminTopicInfo(OEFilterMgmt.oe_network_config, VenueIdToText[VENUE_RASH],   gen_admin_topic_id(VENUE_RASH));
  setAdminTopicInfo(OEFilterMgmt.oe_network_config, VenueIdToText[VENUE_EDGA],   gen_admin_topic_id(VENUE_EDGA));
  setAdminTopicInfo(OEFilterMgmt.oe_network_config, VenueIdToText[VENUE_NDBX],   gen_admin_topic_id(VENUE_NDBX));
  setAdminTopicInfo(OEFilterMgmt.oe_network_config, VenueIdToText[VENUE_BYX],    gen_admin_topic_id(VENUE_BYX));
  setAdminTopicInfo(OEFilterMgmt.oe_network_config, VenueIdToText[VENUE_PSX],    gen_admin_topic_id(VENUE_PSX));

  TraceLog(DEBUG_LEVEL, "Adding arca session with id: %d, protocol: %d\n", OecConfig[ORDER_ARCA_DIRECT].sessionId, OecConfig[ORDER_ARCA_DIRECT].protocol);
  TraceLog(DEBUG_LEVEL, "Adding nyse session with id: %d, protocol: %d\n", OecConfig[ORDER_NYSE_CCG].sessionId, OecConfig[ORDER_NYSE_CCG].protocol);
  TraceLog(DEBUG_LEVEL, "Adding edgx session with id: %d, protocol: %d\n", OecConfig[ORDER_EDGX].sessionId, OecConfig[ORDER_EDGX].protocol);
  TraceLog(DEBUG_LEVEL, "Adding edga session with id: %d, protocol: %d\n", OecConfig[ORDER_EDGA].sessionId, OecConfig[ORDER_EDGA].protocol);
  TraceLog(DEBUG_LEVEL, "Adding bats session with id: %d, protocol: %d\n", OecConfig[ORDER_BATSZ_BOE].sessionId, OecConfig[ORDER_BATSZ_BOE].protocol);
  TraceLog(DEBUG_LEVEL, "Adding isld session with id: %d, protocol: %d\n", OecConfig[ORDER_NASDAQ_OUCH].sessionId, OecConfig[ORDER_NASDAQ_OUCH].protocol);  
  TraceLog(DEBUG_LEVEL, "Adding rash session with id: %d, protocol: %d\n", OecConfig[ORDER_NASDAQ_RASH].sessionId, OecConfig[ORDER_NASDAQ_RASH].protocol);
  TraceLog(DEBUG_LEVEL, "Adding bx session with id: %d, protocol: %d\n",   OecConfig[ORDER_NASDAQ_BX].sessionId, OecConfig[ORDER_NASDAQ_BX].protocol);
  TraceLog(DEBUG_LEVEL, "Adding psx session with id: %d, protocol: %d\n",  OecConfig[ORDER_NASDAQ_PSX].sessionId, OecConfig[ORDER_NASDAQ_PSX].protocol);
  TraceLog(DEBUG_LEVEL, "Adding byx session with id: %d, protocol: %d\n",  OecConfig[ORDER_BYX_BOE].sessionId, OecConfig[ORDER_BYX_BOE].protocol);

  addOrderEntrySession(OEFilterMgmt.oe_network_config, OecConfig[ORDER_ARCA_DIRECT].sessionId, "arca", OecConfig[ORDER_ARCA_DIRECT].ipAddress, OecConfig[ORDER_ARCA_DIRECT].port, OecConfig[ORDER_ARCA_DIRECT].protocol == PROTOCOL_TCP ? FALSE : TRUE);
  addOrderEntrySession(OEFilterMgmt.oe_network_config, OecConfig[ORDER_NYSE_CCG].sessionId,    "nyse", OecConfig[ORDER_NYSE_CCG].ipAddress,    OecConfig[ORDER_NYSE_CCG].port,    OecConfig[ORDER_NYSE_CCG].protocol    == PROTOCOL_TCP ? FALSE : TRUE);
  addOrderEntrySession(OEFilterMgmt.oe_network_config, OecConfig[ORDER_EDGX].sessionId,        "edgx", OecConfig[ORDER_EDGX].ipAddress,        OecConfig[ORDER_EDGX].port,        OecConfig[ORDER_EDGX].protocol        == PROTOCOL_TCP ? FALSE : TRUE);
  addOrderEntrySession(OEFilterMgmt.oe_network_config, OecConfig[ORDER_EDGA].sessionId,        "edga", OecConfig[ORDER_EDGA].ipAddress,        OecConfig[ORDER_EDGA].port,        OecConfig[ORDER_EDGA].protocol        == PROTOCOL_TCP ? FALSE : TRUE);
  addOrderEntrySession(OEFilterMgmt.oe_network_config, OecConfig[ORDER_BATSZ_BOE].sessionId,   "bats", OecConfig[ORDER_BATSZ_BOE].ipAddress,   OecConfig[ORDER_BATSZ_BOE].port,   OecConfig[ORDER_BATSZ_BOE].protocol   == PROTOCOL_TCP ? FALSE : TRUE);
  addOrderEntrySession(OEFilterMgmt.oe_network_config, OecConfig[ORDER_NASDAQ_RASH].sessionId, "rash", OecConfig[ORDER_NASDAQ_RASH].ipAddress, OecConfig[ORDER_NASDAQ_RASH].port, OecConfig[ORDER_NASDAQ_RASH].protocol == PROTOCOL_TCP ? FALSE : TRUE);
  addOrderEntrySession(OEFilterMgmt.oe_network_config, OecConfig[ORDER_NASDAQ_OUCH].sessionId, "isld", OecConfig[ORDER_NASDAQ_OUCH].ipAddress, OecConfig[ORDER_NASDAQ_OUCH].port, OecConfig[ORDER_NASDAQ_OUCH].protocol == PROTOCOL_TCP ? FALSE : TRUE);
  addOrderEntrySession(OEFilterMgmt.oe_network_config, OecConfig[ORDER_NASDAQ_BX].sessionId,   "bx",   OecConfig[ORDER_NASDAQ_BX].ipAddress,   OecConfig[ORDER_NASDAQ_BX].port,   OecConfig[ORDER_NASDAQ_BX].protocol   == PROTOCOL_TCP ? FALSE : TRUE);
  addOrderEntrySession(OEFilterMgmt.oe_network_config, OecConfig[ORDER_NASDAQ_PSX].sessionId,  "psx",  OecConfig[ORDER_NASDAQ_PSX].ipAddress,  OecConfig[ORDER_NASDAQ_PSX].port,  OecConfig[ORDER_NASDAQ_PSX].protocol  == PROTOCOL_TCP ? FALSE : TRUE);
  addOrderEntrySession(OEFilterMgmt.oe_network_config, OecConfig[ORDER_BYX_BOE].sessionId,     "byx",  OecConfig[ORDER_BYX_BOE].ipAddress,     OecConfig[ORDER_BYX_BOE].port,     OecConfig[ORDER_BYX_BOE].protocol     == PROTOCOL_TCP ? FALSE : TRUE);

  void *admin_writer = createSpscWriter(OEFilterMgmt.orderEntryAdminReceiveQueue, DBLFILTER_OE_QUEUE_SIZE, OEFilterMgmt.orderEntryAdminReceiveSeq);
  createOrderEntryDevice(DBLFilterMgmt.topic_registry, admin_writer, OEFilterMgmt.oe_network_config);   

  TraceLog(DEBUG_LEVEL, "adding %d symbols to order entry symbol filter config\n", TotalSymbolCount);

  int qidx;
  for (qidx = 0; qidx < DBLFILTER_NUM_QUEUES; qidx++) {
    for (i = 0; i < QueueSymbolList[qidx].count; i++) {
      int stock_symbol_index = QueueSymbolList[qidx].stockSymbolIndexes[i];
      char *symbol = QueueSymbolList[qidx].symbols[i];
      register_oe_qsymbol(qidx + DBLFILTER_NUM_QUEUES, stock_symbol_index, symbol, VENUE_ARCA,   OecConfig[ORDER_ARCA_DIRECT].sessionId);
      register_oe_qsymbol(qidx + DBLFILTER_NUM_QUEUES, stock_symbol_index, symbol, VENUE_NASDAQ, OecConfig[ORDER_NASDAQ_OUCH].sessionId);
      register_oe_qsymbol(qidx + DBLFILTER_NUM_QUEUES, stock_symbol_index, symbol, VENUE_NYSE,   OecConfig[ORDER_NYSE_CCG].sessionId);
      register_oe_qsymbol(qidx + DBLFILTER_NUM_QUEUES, stock_symbol_index, symbol, VENUE_BATSZ,  OecConfig[ORDER_BATSZ_BOE].sessionId);
      register_oe_qsymbol(qidx + DBLFILTER_NUM_QUEUES, stock_symbol_index, symbol, VENUE_EDGX,   OecConfig[ORDER_EDGX].sessionId);
      register_oe_qsymbol(qidx + DBLFILTER_NUM_QUEUES, stock_symbol_index, symbol, VENUE_RASH,   OecConfig[ORDER_NASDAQ_RASH].sessionId);
      register_oe_qsymbol(qidx + DBLFILTER_NUM_QUEUES, stock_symbol_index, symbol, VENUE_EDGA,   OecConfig[ORDER_EDGA].sessionId);
      register_oe_qsymbol(qidx + DBLFILTER_NUM_QUEUES, stock_symbol_index, symbol, VENUE_NDBX,   OecConfig[ORDER_NASDAQ_BX].sessionId);
      register_oe_qsymbol(qidx + DBLFILTER_NUM_QUEUES, stock_symbol_index, symbol, VENUE_BYX,    OecConfig[ORDER_BYX_BOE].sessionId);
      register_oe_qsymbol(qidx + DBLFILTER_NUM_QUEUES, stock_symbol_index, symbol, VENUE_PSX,    OecConfig[ORDER_NASDAQ_PSX].sessionId);
    }
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessOrderEntryMessage
****************************************************************************/
unsigned short ProcessOrderEntryMessage(unsigned char venue_id, uint32_t symbol_id, uint64_t arrival_time, const unsigned char *buffer_ptr) 
{
  switch (venue_id) 
  {
    case VENUE_NASDAQ:
      return ProcessOuchMessages(ORDER_NASDAQ_OUCH, symbol_id, buffer_ptr, arrival_time);
    case VENUE_NDBX:
      return ProcessOuchMessages(ORDER_NASDAQ_BX, symbol_id, buffer_ptr, arrival_time);
    case VENUE_PSX:
      return ProcessOuchMessages(ORDER_NASDAQ_PSX, symbol_id, buffer_ptr, arrival_time);
    case VENUE_ARCA:
    {
      t_DataBlock dataBlock;
      dataBlock.msgLen = ushortAt(buffer_ptr, 2);
      memcpy(dataBlock.msgContent, buffer_ptr, dataBlock.msgLen);
      memcpy(&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME], &arrival_time, sizeof(uint64_t));
      AddDataBlockTime(&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]);
      ProcessARCA_DIRECT_DataBlock(symbol_id, &dataBlock);

      return dataBlock.msgLen;
    }
    case VENUE_BATSZ:
    case VENUE_BYX:
    {
      t_DataBlock dataBlock;
      dataBlock.msgLen = *(uint16_t *)(buffer_ptr+2) + 2;
      memcpy(dataBlock.msgContent, buffer_ptr, dataBlock.msgLen);
      memcpy(&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME], &arrival_time, sizeof(uint64_t));
      AddDataBlockTime(&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]);
      int oecType = (venue_id == VENUE_BATSZ) ? ORDER_BATSZ_BOE : ORDER_BYX_BOE;
      Process_BATS_BOE_DataBlock(oecType, symbol_id, &dataBlock);

      return dataBlock.msgLen;
    }
    case VENUE_NYSE:
    {
      t_DataBlock dataBlock;
      dataBlock.msgLen = ushortAt(buffer_ptr, 2);
      memcpy(dataBlock.msgContent, buffer_ptr, dataBlock.msgLen);
      memcpy(&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME], &arrival_time, sizeof(uint64_t));
      AddDataBlockTime(&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]);
      Process_NYSE_CCG_DataBlock(symbol_id, &dataBlock);

      return dataBlock.msgLen;
    }
    case VENUE_EDGX:
    case VENUE_EDGA:
    {
      t_DataBlock dataBlock;
      dataBlock.msgLen = ushortAt(buffer_ptr, 0) + 2;
      memcpy(dataBlock.msgContent, buffer_ptr, dataBlock.msgLen);
      memcpy(&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME], &arrival_time, sizeof(uint64_t));
      AddDataBlockTime(&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]);
      int oecType = (venue_id == VENUE_EDGX) ? ORDER_EDGX : ORDER_EDGA;
      Process_EDGE_DataBlock(oecType, symbol_id, &dataBlock);

      return dataBlock.msgLen;
    }
    case VENUE_RASH:
    {
      int offset = 0;
      while(*(buffer_ptr + offset) != 0x0A)
      {
        if(offset > DBLFILTER_OE_QUEUE_SIZE)
        {
          TraceLog(FATAL_LEVEL, "DBLFilter (Order Entry Consumer Buffer) - Could not find end of NASDAQ RASH message\n");
          return 0;
        }
        offset++;
      }
      
      t_DataBlock dataBlock;
      dataBlock.msgLen = offset + 1; 
      memcpy(dataBlock.msgContent, buffer_ptr, dataBlock.msgLen);
      memcpy(&dataBlock.addContent[DATABLOCK_OFFSET_FILTER_TIME], &arrival_time, sizeof(uint64_t));
      AddDataBlockTime(&dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]);
      Process_NASDAQ_RASH_DataBlock(symbol_id, &dataBlock);

      return dataBlock.msgLen;
    }
    default:
    {
      TraceLog(FATAL_LEVEL, "DBLFilter (Order Entry Consumer Buffer) - Got unknown venue, venue ID: %d\n", venue_id);

      int i;
      for (i=0; i < MAX_ECN_BOOK; i++)
      {
        DisconnectFromVenue(i, _consumerId);
        FlushVenueSymbolsOnQueue(i, _consumerId);
      }
      
      DisconnectFromVenue(FEED_CQS, _consumerId);
      DisconnectFromVenue(FEED_UQDF, _consumerId); 

      for (i=0; i<MAX_ECN_ORDER; i++) disconnectSession(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, OecConfig[i].sessionId);

      TraceLog(FATAL_LEVEL, "Please restart Trade Server.\n");
      return 0;
    }
  }

  return 0;
}

/****************************************************************************
- Function name:  InitOrderMgmtDataStructure
****************************************************************************/
int InitOrderMgmtDataStructure(void)
{
  // Initialize all data block collections
  int i;
  
  // Initialize order status
  for (i = 0; i < MAX_ECN_ORDER; i++)
  {
    OrderStatusMgmt[i].isConnected = DISCONNECTED;
    OrderStatusMgmt[i].shouldConnect = NO;
  }
  
  for (i = 0; i < MAX_ECN_ORDER; i++)
  {
    OrderMgmt[i].id = -1;
    OrderMgmt[i].fileDesc = NULL;
    OrderMgmt[i].lastSavedIndex = 0;
    OrderMgmt[i].countRawDataBlock = 0;
    OrderMgmt[i].lastSentIndex = -2;
    
    memset(OrderMgmt[i].dataBlockCollection, 0, MAX_NUM_ORDER_MSG * sizeof(t_DataBlock));
  }
  
  OrderMgmt[ORDER_ARCA_DIRECT].id = ORDER_ARCA_DIRECT;
  OrderMgmt[ORDER_NASDAQ_OUCH].id = ORDER_NASDAQ_OUCH;
  OrderMgmt[ORDER_NASDAQ_BX].id = ORDER_NASDAQ_BX;
  OrderMgmt[ORDER_NASDAQ_PSX].id = ORDER_NASDAQ_PSX;
  OrderMgmt[ORDER_NYSE_CCG].id = ORDER_NYSE_CCG;
  OrderMgmt[ORDER_NASDAQ_RASH].id = ORDER_NASDAQ_RASH;
  OrderMgmt[ORDER_BATSZ_BOE].id = ORDER_BATSZ_BOE;
  OrderMgmt[ORDER_BYX_BOE].id = ORDER_BYX_BOE;
  OrderMgmt[ORDER_EDGX].id = ORDER_EDGX;
  OrderMgmt[ORDER_EDGA].id = ORDER_EDGA;
  
  return 0;
}

/****************************************************************************
- Function name:  RefillOrderTokenBucket
****************************************************************************/
static int RefillOrderTokenBucket(t_OrderTokenBucket *orderBucket)
{
  unsigned long usecs = hbitime_micros();
  unsigned long diff = usecs - orderBucket->lastUpdatedTime;
  
  if (diff > 1000L)
  {
    unsigned long fillAmount = (unsigned long) ((double)orderBucket->refillPerSec * ((double)diff / 1000000.00L));
    
    if (fillAmount < 1) return 0;
    
    if (orderBucket->tokens + fillAmount >= orderBucket->bucketSize)
    {
      orderBucket->tokens = orderBucket->bucketSize;
    }
    else
    {
      orderBucket->tokens += fillAmount;
    }
    
    orderBucket->lastUpdatedTime = usecs;
  }
  
  return 0;
}

/****************************************************************************
- Function name:  OrderBucketConsume
****************************************************************************/
int OrderBucketConsume()
{
  int consumerId = _consumerId;
  
  //Refill first
  t_OrderTokenBucket *orderBucket = FilterBuffers[consumerId].orderBucket;
  RefillOrderTokenBucket(orderBucket);

  if (orderBucket->tokens > 0)  //Allow
  {
    orderBucket->tokens--;
    return SUCCESS;
  }
  
  return ERROR;
}

/****************************************************************************
- Function name:  Thread_SaveDataBlockAndSeqNumToFile
****************************************************************************/
void *Thread_SaveDataBlockAndSeqNumToFile(void *threadArgs)
{
  SetKernelAlgorithm("[SAVE_DATA_SEQUENCE_TO_FILE]");
  static unsigned int sleepCounter = 0;
  
  while (1)
  {
    sleepCounter++;
    if (sleepCounter < 1000)
    {
      usleep(100);
      continue;
    }
    else
    {
      sleepCounter = 0;
    }
    
    // Save all data block to files
    int i;
    
    for (i = 0; i < MAX_ECN_ORDER; i++)
    {
      SaveDataBlockToFile(&OrderMgmt[i]);
    }
    
    // Save all sequence numbers to files
    // ARCA DIRECT
    if (OrderStatusMgmt[ORDER_ARCA_DIRECT].isConnected == CONNECTED)
    {
      if (SaveSeqNumToFile(FN_SEQUENCE_NUM_ARCA, ORDER_ARCA_DIRECT) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "Could not save sequence number of 'ARCA DIRECT' to file\n");
      }
    }

    // NYSE CCG
    if (OrderStatusMgmt[ORDER_NYSE_CCG].isConnected == CONNECTED)
    {
      if (SaveSeqNumToFile(FN_SEQUENCE_NUM_NYSE, ORDER_NYSE_CCG) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "Could not save sequence number of 'NYSE CCG' to file\n");
      }
    }
    
    // BATSZ BOE
    if (OrderStatusMgmt[ORDER_BATSZ_BOE].isConnected == CONNECTED)
    {
      if (SaveSeqNumToFile(FN_SEQUENCE_NUM_BATSZ, ORDER_BATSZ_BOE) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "Could not save sequence number of 'BATS BOE' to file\n");
      }
    }
    
    // BYX BOE
    if (OrderStatusMgmt[ORDER_BYX_BOE].isConnected == CONNECTED)
    {
      if (SaveSeqNumToFile(FN_SEQUENCE_NUM_BYX, ORDER_BYX_BOE) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "Could not save sequence number of 'BYX BOE' to file\n");
      }
    }

    // NASDAQ OUCH
    if (OrderStatusMgmt[ORDER_NASDAQ_OUCH].isConnected == CONNECTED)
    {
      if (SaveSeqNumToFile(FN_SEQUENCE_NUM_OUCH, ORDER_NASDAQ_OUCH) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "Could not save sequence number of 'OUCH' to file\n");
      }
    }
        
    // NASDAQ BX
    if (OrderStatusMgmt[ORDER_NASDAQ_BX].isConnected == CONNECTED)
    {
      if (SaveSeqNumToFile(FN_SEQUENCE_NUM_BX, ORDER_NASDAQ_BX) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "Could not save sequence number of 'BX' to file\n");
      }
    }
    
     // NASDAQ PSX
    if (OrderStatusMgmt[ORDER_NASDAQ_PSX].isConnected == CONNECTED)
    {
      if (SaveSeqNumToFile(FN_SEQUENCE_NUM_PSX, ORDER_NASDAQ_PSX) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "Could not save sequence number of 'PSX' to file\n");
      }
    }
        
    // NASDAQ RASH
    if (OrderStatusMgmt[ORDER_NASDAQ_RASH].isConnected == CONNECTED)
    {
      if (SaveSeqNumToFile(FN_SEQUENCE_NUM_RASH, ORDER_NASDAQ_RASH) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "Could not save sequence number of 'RASH' to file\n");
      }
    }
    
    // EDGX
    if (OrderStatusMgmt[ORDER_EDGX].isConnected == CONNECTED)
    {
      if (SaveSeqNumToFile(FN_SEQUENCE_NUM_EDGX, ORDER_EDGX) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "Could not save sequence number of 'EDGX' to file\n");
      }
    }
    
    // EDGA
    if (OrderStatusMgmt[ORDER_EDGA].isConnected == CONNECTED)
    {
      if (SaveSeqNumToFile(FN_SEQUENCE_NUM_EDGA, ORDER_EDGA) == ERROR)
      {
        TraceLog(ERROR_LEVEL, "Could not save sequence number of 'EDGA' to file\n");
      }
    }
    
    // ... Add more line to update sequence number of other ECN
    
    if (IsTradeServerStopped == 1) return NULL;
  }
  
  return NULL;
}

/****************************************************************************
- Function name:  SaveSeqNumToFile
****************************************************************************/
int SaveSeqNumToFile(const char *fileName, int oecId)
{
  char fullPath[MAX_PATH_LEN];
  
  if (GetFullConfigPath(fileName, CONF_ORDER, fullPath) == NULL_POINTER)
  {
    return ERROR;
  }
  
  switch (oecId)
  {
    case ORDER_ARCA_DIRECT:
    case ORDER_NYSE_CCG:
      return SaveIncomingAndOutgoingSeqNumToFile(fullPath, oecId);
    case ORDER_NASDAQ_OUCH:
    case ORDER_NASDAQ_BX:
    case ORDER_NASDAQ_PSX:
    case ORDER_NASDAQ_RASH:
    case ORDER_EDGX:
    case ORDER_EDGA:
      return SaveIncomingSeqNumToFile(fullPath, oecId);
    case ORDER_BATSZ_BOE:
    case ORDER_BYX_BOE:
      return SaveBatsSeqNumToFile(fullPath, oecId);
    default:
      
      TraceLog(ERROR_LEVEL, "Unsupported order id '%d'\n", oecId);
      return ERROR;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  SaveIncomingAndOutgoingSeqNumToFile
****************************************************************************/
int SaveIncomingAndOutgoingSeqNumToFile(const char *fileName, int oecId)
{
  FILE *fileDesc = fopen(fileName, "w");
  
  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Critical! Could not save order incoming/outgoing sequence numbers to file (%s).\n", fileName);
    PrintErrStr(ERROR_LEVEL, "calling fopen(): ", errno);
    return ERROR;
  }
  
  // Truncate file content to zero offset
  if (truncate(fileName, 0) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Critical! Could not save order incoming/outgoing sequence numbers to file (%s).\n", fileName);
    PrintErrStr(ERROR_LEVEL, "calling truncate(): ", errno);
    return ERROR;
  }
    
  fprintf(fileDesc, "#Incoming Sequence Number\n");
  if (fprintf(fileDesc, "%d\n", OecConfig[oecId].incomingSeqNum) < 0)
  {
    TraceLog(ERROR_LEVEL, "Critical! Could not save order incoming/outgoing sequence numbers to file (%s).\n", fileName);
    PrintErrStr(ERROR_LEVEL, "calling fprintf(): ", errno);
  }
  
  fprintf(fileDesc, "#Outgoing Sequence Number\n");
  if (fprintf(fileDesc, "%d\n", OecConfig[oecId].outgoingSeqNum) < 0)
  {
    TraceLog(ERROR_LEVEL, "Critical! Could not save order incoming/outgoing sequence numbers to file (%s).\n", fileName);
    PrintErrStr(ERROR_LEVEL, "calling fprintf(): ", errno);
  }
  
  if (fflush(fileDesc) != 0)
  {
    TraceLog(ERROR_LEVEL, "Critical! Could not save order incoming/outgoing sequence numbers to file (%s).\n", fileName);
    PrintErrStr(ERROR_LEVEL, "calling fflush(): ", errno);
  }
  
  if (fclose(fileDesc) != 0)
  {
    TraceLog(ERROR_LEVEL, "Critical! Could not save order incoming/outgoing sequence numbers to file (%s).\n", fileName);
    PrintErrStr(ERROR_LEVEL, "calling fflush(): ", errno);
  }
  
  return SUCCESS;
}
/****************************************************************************
- Function name:  SaveBatsSeqNumToFile
****************************************************************************/
int SaveBatsSeqNumToFile(const char *fileName, const int oecId)
{
  FILE *fileDesc = fopen(fileName, "w");

  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Critical! Could not save order outgoing sequence number to file (%s).\n", fileName);
    PrintErrStr(ERROR_LEVEL, "calling fopen(): ", errno);
    return ERROR;
  }
  
  if (truncate(fileName, 0) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Critical! Could not save order outgoing sequence number to file (%s).\n", fileName);
    PrintErrStr(ERROR_LEVEL, "calling truncate(): ", errno);
    return ERROR;
  }

  int i = 0;
  if (oecId == ORDER_BATSZ_BOE)
  {
    fprintf(fileDesc, "#Outgoing Sequence Number\n");
    if (fprintf(fileDesc, "%d\n", OecConfig[ORDER_BATSZ_BOE].outgoingSeqNum) < 0)
    {
      TraceLog(ERROR_LEVEL, "Critical! Could not save order outgoing sequence number to file (%s).\n", fileName);
      PrintErrStr(ERROR_LEVEL, "calling fprintf(): ", errno);
    }

    fprintf(fileDesc, "#Number of units: 0 <= value <= 255 (initial: 0)\n");
    if (fprintf(fileDesc, "%d\n", OecConfig[ORDER_BATSZ_BOE].numbersOfBatszUnits) < 0)
    {
      TraceLog(ERROR_LEVEL, "Critical! Could not save order outgoing sequence number to file (%s).\n", fileName);
      PrintErrStr(ERROR_LEVEL, "calling fprintf(): ", errno);
    }


    for(i = 0; i < MAX_BATSZ_NUM_UNIT; i++)
    {
      fprintf(fileDesc, "%s%d\n", "#Unit", i+1);
      if (fprintf(fileDesc, "%d\n", BATSZ_BOE_SequenceOfUnit[i].incomingSeqNum) < 0)
      {
        TraceLog(ERROR_LEVEL, "Critical! Could not save order outgoing sequence number to file (%s).\n", fileName);
        PrintErrStr(ERROR_LEVEL, "calling fprintf(): ", errno);
      }
    }
  }
  else
  {
    fprintf(fileDesc, "#Outgoing Sequence Number\n");
    if (fprintf(fileDesc, "%d\n", OecConfig[ORDER_BYX_BOE].outgoingSeqNum) < 0)
    {
      TraceLog(ERROR_LEVEL, "Critical! Could not save order outgoing sequence number to file (%s).\n", fileName);
      PrintErrStr(ERROR_LEVEL, "calling fprintf(): ", errno);
    }

    fprintf(fileDesc, "#Number of units: 0 <= value <= 255 (initial: 0)\n");
    if (fprintf(fileDesc, "%d\n", OecConfig[ORDER_BYX_BOE].numbersOfBatszUnits) < 0)
    {
      TraceLog(ERROR_LEVEL, "Critical! Could not save order outgoing sequence number to file (%s).\n", fileName);
      PrintErrStr(ERROR_LEVEL, "calling fprintf(): ", errno);
    }
    
    for(i = 0; i < MAX_BYX_NUM_UNIT; i++)
    {
      fprintf(fileDesc, "%s%d\n", "#Unit", i+1);
      if (fprintf(fileDesc, "%d\n", BYX_BOE_SequenceOfUnit[i].incomingSeqNum) < 0)
      {
        TraceLog(ERROR_LEVEL, "Critical! Could not save order outgoing sequence number to file (%s).\n", fileName);
        PrintErrStr(ERROR_LEVEL, "calling fprintf(): ", errno);
      }
    }
  }

  if (fflush(fileDesc) != 0)
  {
    TraceLog(ERROR_LEVEL, "Critical! Could not save order outgoing sequence number to file (%s).\n", fileName);
    PrintErrStr(ERROR_LEVEL, "calling fflush(): ", errno);
  }
  
  if (fclose(fileDesc) != 0)
  {
    TraceLog(ERROR_LEVEL, "Critical! Could not save order outgoing sequence number to file (%s).\n", fileName);
    PrintErrStr(ERROR_LEVEL, "calling fflush(): ", errno);
  }
  return SUCCESS;
}

/****************************************************************************
- Function name:  SaveIncomingSeqNumToFile
****************************************************************************/
int SaveIncomingSeqNumToFile(const char *fileName, int oecId)
{
  FILE *fileDesc = fopen(fileName, "w");
  
  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Critical! Could not save order incoming sequence number to file (%s).\n", fileName);
    PrintErrStr(ERROR_LEVEL, "calling fopen(): ", errno);
    return ERROR;
  }
  
  if (truncate(fileName, 0) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Critical! Could not save order incoming sequence number to file (%s).\n", fileName);
    PrintErrStr(ERROR_LEVEL, "calling truncate(): ", errno);
    return ERROR;
  }
  
  fprintf(fileDesc, "#Incoming Sequence Number\n");
  if (fprintf(fileDesc, "%d\n", OecConfig[oecId].incomingSeqNum) < 0)
  {
    TraceLog(ERROR_LEVEL, "Critical! Could not save order incoming sequence number to file (%s).\n", fileName);
    PrintErrStr(ERROR_LEVEL, "calling fprintf(): ", errno);
  }
  
  if (fflush(fileDesc) != 0)
  {
    TraceLog(ERROR_LEVEL, "Critical! Could not save order incoming sequence number to file (%s).\n", fileName);
    PrintErrStr(ERROR_LEVEL, "calling fflush(): ", errno);
  }
  
  if (fclose(fileDesc) != 0)
  {
    TraceLog(ERROR_LEVEL, "Critical! Could not save order incoming sequence number to file (%s).\n", fileName);
    PrintErrStr(ERROR_LEVEL, "calling fflush(): ", errno);
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadAllDataBlocksFromFiles
****************************************************************************/
int LoadAllDataBlocksFromFiles(void)
{
  TraceLog(DEBUG_LEVEL, "Raw data is being loaded...\n");
  
  OrderMgmt[ORDER_ARCA_DIRECT].id = ORDER_ARCA_DIRECT;
  if (LoadDataBlockFromFile(FN_RAWDATA_ARCA, &OrderMgmt[ORDER_ARCA_DIRECT]) == ERROR)
  {
    OrderMgmt[ORDER_ARCA_DIRECT].id = -1;
    return ERROR;
  }

  OrderMgmt[ORDER_NASDAQ_OUCH].id = ORDER_NASDAQ_OUCH;
  if (LoadDataBlockFromFile(FN_RAWDATA_ISLD, &OrderMgmt[ORDER_NASDAQ_OUCH]) == ERROR)
  {
    OrderMgmt[ORDER_NASDAQ_OUCH].id = -1;
    return ERROR;
  }
  
  OrderMgmt[ORDER_NYSE_CCG].id = ORDER_NYSE_CCG;
  if (LoadDataBlockFromFile(FN_RAWDATA_NYSE, &OrderMgmt[ORDER_NYSE_CCG]) == ERROR)
  {
    OrderMgmt[ORDER_NYSE_CCG].id = -1;
    return ERROR;
  }
  
  OrderMgmt[ORDER_NASDAQ_BX].id = ORDER_NASDAQ_BX;
  if (LoadDataBlockFromFile(FN_RAWDATA_BX, &OrderMgmt[ORDER_NASDAQ_BX]) == ERROR)
  {
    OrderMgmt[ORDER_NASDAQ_BX].id = -1;
    return ERROR;
  }
  
  OrderMgmt[ORDER_NASDAQ_PSX].id = ORDER_NASDAQ_PSX;
  if (LoadDataBlockFromFile(FN_RAWDATA_PSX, &OrderMgmt[ORDER_NASDAQ_PSX]) == ERROR)
  {
    OrderMgmt[ORDER_NASDAQ_PSX].id = -1;
    return ERROR;
  }
  
  OrderMgmt[ORDER_NASDAQ_RASH].id = ORDER_NASDAQ_RASH;
  if (LoadDataBlockFromFile(FN_RAWDATA_RASH, &OrderMgmt[ORDER_NASDAQ_RASH]) == ERROR)
  {
    OrderMgmt[ORDER_NASDAQ_RASH].id = -1;
    return ERROR;
  }

  OrderMgmt[ORDER_BATSZ_BOE].id = ORDER_BATSZ_BOE;
  if (LoadDataBlockFromFile(FN_RAWDATA_BATSZ, &OrderMgmt[ORDER_BATSZ_BOE]) == ERROR)
  {
    OrderMgmt[ORDER_BATSZ_BOE].id = -1;
    return ERROR;
  }
  
  OrderMgmt[ORDER_BYX_BOE].id = ORDER_BYX_BOE;
  if (LoadDataBlockFromFile(FN_RAWDATA_BYX, &OrderMgmt[ORDER_BYX_BOE]) == ERROR)
  {
    OrderMgmt[ORDER_BYX_BOE].id = -1;
    return ERROR;
  }
  
  OrderMgmt[ORDER_EDGX].id = ORDER_EDGX;
  if (LoadDataBlockFromFile(FN_RAWDATA_EDGX, &OrderMgmt[ORDER_EDGX]) == ERROR)
  {
    OrderMgmt[ORDER_EDGX].id = -1;
    return ERROR;
  }

  OrderMgmt[ORDER_EDGA].id = ORDER_EDGA;
  if (LoadDataBlockFromFile(FN_RAWDATA_EDGA, &OrderMgmt[ORDER_EDGA]) == ERROR)
  {
    OrderMgmt[ORDER_EDGA].id = -1;
    return ERROR;
  }

  TraceLog(DEBUG_LEVEL, "Raw data was loaded successfully:\n");
  
  TraceLog(DEBUG_LEVEL, "ARCA DIRECT: total(%d), sent(%d)\n", 
        OrderMgmt[ORDER_ARCA_DIRECT].countRawDataBlock, 
        OrderMgmt[ORDER_ARCA_DIRECT].lastSentIndex);

  TraceLog(DEBUG_LEVEL, "OUCH: total(%d), sent(%d)\n", 
        OrderMgmt[ORDER_NASDAQ_OUCH].countRawDataBlock, 
        OrderMgmt[ORDER_NASDAQ_OUCH].lastSentIndex);

  TraceLog(DEBUG_LEVEL, "NASDAQ BX: total(%d), sent(%d)\n", 
        OrderMgmt[ORDER_NASDAQ_BX].countRawDataBlock, 
        OrderMgmt[ORDER_NASDAQ_BX].lastSentIndex);
        
  TraceLog(DEBUG_LEVEL, "NASDAQ PSX: total(%d), sent(%d)\n", 
        OrderMgmt[ORDER_NASDAQ_PSX].countRawDataBlock, 
        OrderMgmt[ORDER_NASDAQ_PSX].lastSentIndex);

  TraceLog(DEBUG_LEVEL, "RASH: total(%d), sent(%d)\n", 
        OrderMgmt[ORDER_NASDAQ_RASH].countRawDataBlock, 
        OrderMgmt[ORDER_NASDAQ_RASH].lastSentIndex);

  TraceLog(DEBUG_LEVEL, "NYSE CCG: total(%d), sent(%d)\n", 
        OrderMgmt[ORDER_NYSE_CCG].countRawDataBlock, 
        OrderMgmt[ORDER_NYSE_CCG].lastSentIndex);
        
  TraceLog(DEBUG_LEVEL, "BATSZ BOE: total(%d), sent(%d)\n", 
        OrderMgmt[ORDER_BATSZ_BOE].countRawDataBlock, 
        OrderMgmt[ORDER_BATSZ_BOE].lastSentIndex);
        
  TraceLog(DEBUG_LEVEL, "BYX BOE: total(%d), sent(%d)\n", 
        OrderMgmt[ORDER_BYX_BOE].countRawDataBlock, 
        OrderMgmt[ORDER_BYX_BOE].lastSentIndex);
      
  TraceLog(DEBUG_LEVEL, "EDGX: total(%d), sent(%d)\n", 
        OrderMgmt[ORDER_EDGX].countRawDataBlock, 
        OrderMgmt[ORDER_EDGX].lastSentIndex);
  
  TraceLog(DEBUG_LEVEL, "EDGA: total(%d), sent(%d)\n", 
        OrderMgmt[ORDER_EDGA].countRawDataBlock, 
        OrderMgmt[ORDER_EDGA].lastSentIndex);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  LoadDataBlockFromFile
****************************************************************************/
int LoadDataBlockFromFile(const char *fileName, void *orderRawDataMgmt)
{
  t_OrderRawDataMgmt *workingOrderMgmt = (t_OrderRawDataMgmt *)orderRawDataMgmt;
  
  // Get full raw data path
  char fullPath[MAX_PATH_LEN] = "\0";
  
  if (GetFullRawDataPath(fileName, fullPath) == NULL_POINTER)
  {
    TraceLog(ERROR_LEVEL, "Have some problem in get full raw data path operation (%s)\n", fileName);
    return ERROR;
  }
  
  /* 
  Open file to read in BINARY MODE, 
  if it is not existed, we will create it
  */
  
  if (IsFileTimestampToday(fullPath) == 0)
  {
    TraceLog(ERROR_LEVEL, "'%s' is old, please check correctness of data files\n", fullPath);
    exit(0);
  }
  
  FILE *fileDesc = fopen(fullPath, "a+");
  if (fileDesc == NULL)
  {
    TraceLog(ERROR_LEVEL, "Critical! Could not open/create file to store rawdata information (%s). Trade Server could not continue.\n", fullPath);
    PrintErrStr(ERROR_LEVEL, "calling fopen(): ", errno);
    sleep(3);
    exit(0);
  }
  else
  {
    // Store pointer to file that will be used after
    workingOrderMgmt->fileDesc = fileDesc;
    t_DataBlock dataBlock;

    while (fread(&dataBlock, sizeof(t_DataBlock), 1, workingOrderMgmt->fileDesc) == 1)
    {
      workingOrderMgmt->dataBlockCollection[workingOrderMgmt->countRawDataBlock % MAX_NUM_ORDER_MSG] = dataBlock;
      workingOrderMgmt->countRawDataBlock++;
    }
      
    workingOrderMgmt->lastSavedIndex = workingOrderMgmt->countRawDataBlock;
  }

  if (workingOrderMgmt->countRawDataBlock > 0)
  {
    if (IsGottenTradingAccountConfig == YES &&
      TradingAccount.accountType == -1)
    {
      // Get Trading Account Type from raw data once only if trading account is gotten from command line
      TradingAccount.accountType = workingOrderMgmt->dataBlockCollection[0].addContent[DATABLOCK_OFFSET_ACCOUNT_ID];
      TraceLog(DEBUG_LEVEL, "Get Trading Account Type (%d) from raw data (ecnID=%d)\n", TradingAccount.accountType, workingOrderMgmt->id);
    }
  }
        
  TraceLog(DEBUG_LEVEL, "LoadDataBlockFromFile: ECN ID: %d, Count raw data block: %d\n", workingOrderMgmt->id, workingOrderMgmt->countRawDataBlock);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  AddDataBlockToQueue
****************************************************************************/
int AddDataBlockToQueue(uint16_t venueId, const void *dataBlock)
{
  if(_consumerId != -1) {
    spsc_write_handle writer = FilterBuffers[_consumerId].dataBlockWriter;
    int dataBlockSize = sizeof(t_DataBlock);
    waitForSpscWritableItemsAvailable(writer, dataBlockSize);  
    uint8_t *write_ptr = spscWritePtr(writer);

    *((uint16_t*)write_ptr) = venueId;
    memcpy(write_ptr+2, dataBlock, dataBlockSize);
    spscCommitItemsWritten(writer, dataBlockSize+2);
  } else {
    AddDataBlockToCollection(dataBlock, &OrderMgmt[venueId]);
  }

  return SUCCESS;
}

/****************************************************************************
- Function name:  AddDataBlockToCollection
****************************************************************************/
int AddDataBlockToCollection(const void *dataBlock, void *orderRawDataMgmt)
{
  t_OrderRawDataMgmt *workingOrderMgmt = (t_OrderRawDataMgmt *)orderRawDataMgmt;
  t_DataBlock *srcBlock = (t_DataBlock *)dataBlock;
  
  int indexToAdd = __sync_fetch_and_add(&workingOrderMgmt->countRawDataBlock, 1) % MAX_NUM_ORDER_MSG;
  t_DataBlock *dstBlock = &workingOrderMgmt->dataBlockCollection[indexToAdd];
  
  // Add new data block to collection
  dstBlock->msgLen = 0; //This is very important, used for synchronization with another threads
  srcBlock->addContent[DATABLOCK_OFFSET_ACCOUNT_ID] = TradingAccount.accountType;
  dstBlock->blockLen = srcBlock->blockLen;
  memcpy(dstBlock->msgContent, srcBlock->msgContent, srcBlock->msgLen);
  memcpy(dstBlock->addContent, srcBlock->addContent, DATABLOCK_ADDITIONAL_INFO_LEN);
  dstBlock->msgLen = srcBlock->msgLen;  //This indicates that the item is fully saved into the memory

  return 0;
}

/****************************************************************************
- Function name:  SaveDataBlockToFile
****************************************************************************/
int SaveDataBlockToFile(void *orderRawDataMgmt)
{
  t_OrderRawDataMgmt *workingOrderMgmt = (t_OrderRawDataMgmt *)orderRawDataMgmt;
  int i, retry = 3;
  int willFlush = 0;
  int total;
  
RETRY:

  total = workingOrderMgmt->countRawDataBlock;
  for (i = workingOrderMgmt->lastSavedIndex; i < total; i++)
  {
    //fseek(workingOrderMgmt->fileDesc, 0, SEEK_END);
    
    if (workingOrderMgmt->dataBlockCollection[i % MAX_NUM_ORDER_MSG].msgLen != 0) //this item was completely saved into memory
    {
      if (fwrite(&workingOrderMgmt->dataBlockCollection[i % MAX_NUM_ORDER_MSG], sizeof(t_DataBlock), 1, workingOrderMgmt->fileDesc) != 1)
      {
        TraceLog(ERROR_LEVEL, "Could not save rawdata of ecn %d to file\n", workingOrderMgmt->id);
        
        if (retry > 0)
        {
          retry --; //We should try again
          goto RETRY;
        }
        
        break;
      }
      
      willFlush = 1;
      workingOrderMgmt->lastSavedIndex++;
    }
    else
    {
      if (retry > 0)
      {
        retry --; //We should try again
        goto RETRY;
      }

      break;  //This is not saved into memory, we should stop saving
    }
  }
  
  if (willFlush == 1)
  {
    fflush(workingOrderMgmt->fileDesc);
  }
  
  return SUCCESS;
}
