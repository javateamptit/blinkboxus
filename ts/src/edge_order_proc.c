/****************************************************************************
** Project name:  Equity Arbitrage Application
** File name:     edge_order_proc.c
** Description:   N/A
** Author:      Dung Tran-Dinh
** First created: ------------------
** Last updated:  ------------------
****************************************************************************/
#define _ISOC9X_SOURCE
#define _GNU_SOURCE

#include "edge_order_proc.h"
#include "trading_mgmt.h"
#include "kernel_algorithm.h"
#include "network.h"
#include "logging.h"

/****************************************************************************
**          GLOBAL VARIABLES
****************************************************************************/

/****************************************************************************
** Function definitions
****************************************************************************/

/****************************************************************************
- Function name:  Connect_EDGE
****************************************************************************/
int Connect_EDGE(int oecType)
{
  // Initialize data structure 
  OecConfig[oecType].socket = -1;
  
  // Load incoming sequence number from file
  if (LoadSequenceNumber(oecType == ORDER_EDGX ? FN_SEQUENCE_NUM_EDGX : FN_SEQUENCE_NUM_EDGA, oecType) == ERROR)
  {
    OrderStatusMgmt[oecType].shouldConnect = NO;
    return ERROR;
  }
  
  TraceLog(DEBUG_LEVEL, "Connecting to %s Server (%s:%d)\n", GetOrderNameByIndex(oecType), OecConfig[oecType].ipAddress, OecConfig[oecType].port);

  // Connect to server
  OecConfig[oecType].socket = connectSession(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, OecConfig[oecType].sessionId);
  
  if (OecConfig[oecType].socket == ERROR)
  {
    OrderStatusMgmt[oecType].shouldConnect = NO;
    TraceLog(DEBUG_LEVEL, "Connection to %s Server was disconnected (Could not connect to server)\n", GetOrderNameByIndex(oecType));
    return ERROR;
  }
  
  if (SetSocketRecvTimeout(OecConfig[oecType].socket, 5) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Could not set socket receive-timeout for %s socket\n", GetOrderNameByIndex(oecType));
    disconnectSession(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, OecConfig[oecType].sessionId);
    OrderStatusMgmt[oecType].shouldConnect = NO;
    return ERROR;
  }
  
  if (SetSocketSndTimeout(OecConfig[oecType].socket, 1, 0) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Could not set socket send-timeout for %s socket\n", GetOrderNameByIndex(oecType));
    disconnectSession(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, OecConfig[oecType].sessionId);
    OrderStatusMgmt[oecType].shouldConnect = NO;
    return ERROR;
  }
  
  /* Disable the Nagle (TCP No Delay) algorithm */
  if (DisableNagleAlgo(OecConfig[oecType].socket) == -1)
  {
    TraceLog(ERROR_LEVEL, "Could not disable Nagle algorithm for %s socket\n", GetOrderNameByIndex(oecType));
    disconnectSession(DBLFilterMgmt.topic_registry, OEFilterMgmt.oe_network_config, OecConfig[oecType].sessionId);
    OrderStatusMgmt[oecType].shouldConnect = NO;
    return ERROR;
  }
  
  // Send Logon message to server
  if (BuildAndSend_EDGE_LogonRequest(oecType) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "Could not send logon request message to %s server\n", GetOrderNameByIndex(oecType));
    OrderStatusMgmt[oecType].shouldConnect = NO;
    return ERROR;
  }
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  Process_EDGE_DataBlock
****************************************************************************/
int Process_EDGE_DataBlock(int oecType, uint32_t symbolId, t_DataBlock *dataBlock) {
  /*
  Base on message type we will call appropriate functions
  */
  int packetType = dataBlock->msgContent[2];
  
  if (packetType == 'S')
  {
    __sync_add_and_fetch(&OecConfig[oecType].incomingSeqNum, 1);
    
    int msgType = dataBlock->msgContent[3];
    
    switch (msgType)
    {
      case EDGE_ORDER_ACCEPTED:
      case EDGE_ORDER_ACCEPTED_EXTENDED:
        ProcessEDGE_OrderAcceptedMsg(oecType, symbolId, dataBlock);
        break;
        
      case EDGE_ORDER_EXECUTED:
        ProcessEDGE_OrderExecutedMsg(oecType, symbolId, dataBlock);
        break;
        
      case EDGE_ORDER_CANCELED:
        ProcessEDGE_OrderCanceledMsg(oecType, symbolId, dataBlock);
        break;
      
      case EDGE_REJECTED:
      case EDGE_REJECTED_EXTENDED:
        ProcessEDGE_OrderRejectedMsg(oecType, symbolId, dataBlock);
        break;
        
      case EDGE_BROKEN_TRADE:
      case EDGE_SYSTEM_EVENT:
      case EDGE_CANCEL_PENDING:
      case EDGE_REPLACED_MSG:
      case EDGE_PRICE_CORRECTION:
      case EDGE_PENDING_REPLACE:
      case 'G': //AI Additional Info Message
        Add_EDGE_DataBlockToCollection(oecType, dataBlock);
        break;
      default:
        Add_EDGE_DataBlockToCollection(oecType, dataBlock);
        TraceLog(WARN_LEVEL, "%s: Received unknown message type: %d - %c\n", GetOrderNameByIndex(oecType), msgType, msgType);
        break;
    }
  }
  else
  {
    switch (packetType)
    {
      case EDGE_HEARTBEAT:
        if (BuildAndSend_EDGE_Heartbeat(oecType) == ERROR)
        {
          return ERROR;
        }
        break;
      
      case EDGE_DEBUG:
        TraceLog(DEBUG_LEVEL, "%s: Receive debug msg, text: %.10s\n", GetOrderNameByIndex(oecType), &dataBlock->msgContent[3]);
        break;
        
      case EDGE_LOGIN_ACCEPTED:
        // Update flag to indicate that we connected to server successfully
        OecConfig[oecType].incomingSeqNum = atol(&dataBlock->msgContent[13]);
        TraceLog(DEBUG_LEVEL, "%s: Received logon accepted, session: '%.10s', sequence number: '%.20s'\n", GetOrderNameByIndex(oecType), &dataBlock->msgContent[3], &dataBlock->msgContent[13]);
        OrderStatusMgmt[oecType].isConnected = CONNECTED;
        break;
      
      case EDGE_LOGIN_REJECTED:
        // Update flag to indicate that we cannot connect to server
        TraceLog(DEBUG_LEVEL, "%s: Received logon rejected message\n", GetOrderNameByIndex(oecType));
        if (dataBlock->msgContent[3] == 'A')
          printf("\t reason: %c (Not Authorized. Invalid username and password combination.)\n", dataBlock->msgContent[3]);
        else if (dataBlock->msgContent[3] == 'S')
          printf("\t reason: %c (Session not available. The Requested Session in the Login Request Package was either invalid or not available.)\n", dataBlock->msgContent[3]);
        
        return ERROR;
        
      case EDGE_END_OF_SESSION:
        TraceLog(DEBUG_LEVEL, "%s: Received end of session message\n", GetOrderNameByIndex(oecType));
        return ERROR;
        
      default:
        TraceLog(DEBUG_LEVEL, "%s: Received unknown packet type: %d - %c\n", GetOrderNameByIndex(oecType), packetType, packetType);
        break;
    }
  }  
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  BuildAndSend_EDGE_LogonRequest
****************************************************************************/
int BuildAndSend_EDGE_LogonRequest(int oecType)
{
  // length: 49
  char message[EDGE_ORDER_LOGIN_REQUEST_MSG_LEN];
  int currentIndex;
  t_ShortConverter shortUnion;
  
  // len is used to store the length of various strings
  int len = 0;
  
  /*
  Both fields username and password are case insensitive, 
  and must be filled with spaces on the right
  */
  memset(message, ' ', EDGE_ORDER_LOGIN_REQUEST_MSG_LEN);
  
  /* Package Length, 2 bytes Integer, Number of bytes after this field until the next package */
  shortUnion.value = EDGE_ORDER_LOGIN_REQUEST_MSG_LEN - 2;
  message[0] = shortUnion.c[1];
  message[1] = shortUnion.c[0];
  
  /* Package Type: 1 byte "L" */
  message[2] = 'L';
  
  /* Username: 6 bytes Alphanumeric. Case insensitive. */
  len = strlen(OecConfig[oecType].userName);

  for (currentIndex = 0; currentIndex < len; currentIndex++)  
  {
    if (currentIndex < 6)
    {
      message[3 + currentIndex] = OecConfig[oecType].userName[currentIndex];
    }
  }
  
  /* Password: 10 Alphanumeric. Case insensitive. */
  len = strlen(OecConfig[oecType].password);

  for (currentIndex = 0; currentIndex < len; currentIndex++)
  {
    if (currentIndex < 10)
    {
      message[9 + currentIndex] = OecConfig[oecType].password[currentIndex];
    }
  }

  /* Session: 10 bytes Alphanumeric, Specifies the session or all blanks for the currently active session.
   we will use all blanks */
  
  /* Requested Sequence Number: 20 bytes Numeric, Specifies the next sequence number in 0 to start receiving the most recently
   generated message. Note that this field is expressed in ASCII format. (Left padded with spaces) */
  Lrc_itoaf(OecConfig[oecType].incomingSeqNum, ' ', 20, &message[29]);
  
  return SocketSend(OecConfig[oecType].socket, message, EDGE_ORDER_LOGIN_REQUEST_MSG_LEN);
}

/****************************************************************************
- Function name:  BuildAndSend_EDGE_Order
****************************************************************************/
int BuildAndSend_EDGE_Order(t_OrderPlacementInfo *newOrder)
{  
  int oecType = newOrder->ecnToPlaceID;
  
  // According to EDGE spec, the maximum price allowable is through theAPI is $42949.67296
  if (fgt(newOrder->sharePrice, 42949.67296))
  {
    TraceLog(WARN_LEVEL, "%s Order: The price (%lf) is larger than maximum price allowed by the EDGX API(42,949.67296), skip trading for symbol = %.6s\n", GetOrderNameByIndex(oecType), newOrder->sharePrice, newOrder->symbol);
    return ERROR;
  }
  
  if (ConvertSymbolFromComstockToCMS(newOrder->symbol, newOrder->rootSymbol, newOrder->symbolSuffix) == SUCCESS)
  {
    //Symbol is having suffix, we must build msg with extended format
    return BuildAndSend_EDGE_OrderExtendedMsg(oecType, newOrder);
  }
  else
  {
    return BuildAndSend_EDGE_OrderShortMsg(oecType, newOrder);
  }
}

/****************************************************************************
- Function name:  BuildAndSend_EDGE_OrderShortMsg
****************************************************************************/
int BuildAndSend_EDGE_OrderShortMsg(int oecType, t_OrderPlacementInfo *newOrder)
{
  t_ShortConverter shortUnion;
  t_IntConverter intUnion;
  
  // Unsequence data packet
  char *msgContent = newOrder->dataBlock.msgContent;
  shortUnion.value = 38; // packet type (1 byte)
  msgContent[0] = shortUnion.c[1];
  msgContent[1] = shortUnion.c[0];
  
  msgContent[2] = 'U';
  
  /* Order message type 'O':  Identifies this message as an Enter Order � Short Format message type. The Short
  Format of this message allows for less order type features than the Extended Format message. */
  msgContent[3] = EDGE_ENTER_ORDER_SHORT_FORM;
  
  /* Order token: 14 bytes Alphanumeric. This is a free-form, alphanumeric field. You can enter any information you like.
  Token must be day-unique for each Direct Edge API account. Token is case sensitive, but mixing uppercase and lower-case
  Tokens is not recommended.*/
  msgContent[4] = oecType == ORDER_EDGX? TradingAccount.EDGXAccount[0] : TradingAccount.EDGAAccount[0];
  msgContent[5] = oecType == ORDER_EDGX? TradingAccount.EDGXAccount[1] : TradingAccount.EDGAAccount[1];
  Lrc_itoafl(newOrder->orderID, ' ', 12, &msgContent[6]);
  
  /* Buy/Sell indicator */
  msgContent[18] = newOrder->side;
  
  /* Share */
  intUnion.value = newOrder->shareVolume;
  msgContent[19] = intUnion.c[3];
  msgContent[20] = intUnion.c[2];
  msgContent[21] = intUnion.c[1];
  msgContent[22] = intUnion.c[0];
  
  /* Symbol: 6 Bytes Alpha */
  strcncpy((unsigned char *)&msgContent[23], newOrder->symbol, ' ', 6);
  
  /* Price: 4 bytes Integer, The limit price of the order. The price is a 5 digit whole number followed by a 5 decimal digits. */
  if (fge(newOrder->sharePrice, 1.00))
  {
    if (newOrder->side == 1)  //BUY SIDE
    {
      intUnion.value = (int)(newOrder->sharePrice * 100.00 + 0.500001) * 1000;
    }
    else
    {
      intUnion.value = (int)(newOrder->sharePrice * 100.00 + 0.000001) * 1000;
    }
  }
  else
  {
    intUnion.value = (int)(newOrder->sharePrice * 100000.00 + 0.500001);
  }
  msgContent[29] = intUnion.c[3];
  msgContent[30] = intUnion.c[2];
  msgContent[31] = intUnion.c[1];
  msgContent[32] = intUnion.c[0];
  
  /* Time in Force: 1 byte Interger, 0 = IOC, 1 = Day, 2 = Fill or Kill */
  msgContent[33] = 0;
  
  /* Display: 1 byte Alpha, �Y� = Displayed, �N� = Hidden, �A� = Displayed with Attribution */
  msgContent[34] = 'Y';

  /* Special order type: 1 byte Alpha
    �M�= MidPoint Match EDGX/MidPoint Peg EDGA
    �D� = Midpoint Discretionary Order � EDGA Only*
    �U� = Route Peg Order
    Re-Pricing Options:
      �S� = Hide Not Slide
      �P� = Price Adjust
      �R� = Single Re-Price
      �C� = Cancel Back
    (See Appendix B for details on Re-Pricing options.)
    *Midpoint Discretionary: Orders require a �Y� in the Display field.
  
    Enter a space for this field if not applicable.
  */
  msgContent[35] = ' ';
  
  /* Extended hours eligible: 1 byte alpha
    �R� = Regular Session Only
    �P� = Pre-Market and Regular Session Eligible
    �A� = Regular Session and Post-Market Eligible
    �B� = All Sessions Eligible*/
  msgContent[36] = 'B';
  
  /* Capacity: 1 byte Alpha
    �A� = Agency
    �P� = Principal
    �R� = Riskless Principal
  */
  msgContent[37] = 'P';
  
  /* Route out eligiblity: 1 Byte Alpha
    �Y� = Routable (ROUT strategy only)**
    �N� = Book Only***
    �P� = Post-Only*&***
    �S� = Super Aggressive - Cross or Lock (Order will be removed
        from book and routed to any quote that is crossing or locking the order)
    �X� = Aggressive - Cross only (Order will be removed from book
        and routed to any quote that is crossing)
    a = Post to EDGA (for orders originating from EDGX, ROUT, ROUX, ROUE only)
    b = Post to EDGX for orders originating on EDGA (for ROUT, ROUD, ROUE, ROUX, 
       ROUZ, ROUQ, RDOT, RDOX, ROPA, ROBA, ROBX, ROBY, ROBB,ROCO, ROLF, INET, IOCT, IOCX, IOCM, ICMT only)
    c = Post to NYSE Arca (ROUT, ROUX, ROUE only)
    d = Post to NYSE (ROUT, ROUX, ROUE only)
    e = Post to NASDAQ (ROUT, ROUX, ROUE only)
    f = Post to NASDAQ OMX BX (ROUT, ROUX, ROUE only)
    g = Post to NASDAQ OMX PSX (ROUT, ROUX, ROUE only)
    h = Post to BATS BYX (ROUT, ROUX, ROUE only)
    i = Post to BATS BZX (ROUT, ROUX, ROUE only)
    j = Post to LavaFlow (ROUT, ROUX, ROUE only)
    k = Post to CBSX (ROUT, ROUX, ROUE only)
    l = Post to AMEX (ROUT, ROUX, ROUE only)
    n = Post to NSX (ROUT, ROUX, ROUE only)
    
    (Use the Enter Order - Extended Format to enter routing strategies other than ROUT)
    
    Enter a space for this field if not applicable.
    
    *Post Only orders marked HideNotSlide or Hidden may be executed as a Taker if price improved on EDGX.
    Post Only instructions may not be included on Hidden Orders, Discretionary Orders, or Odd Lot quantities on EDGA.

    **All routing strategies are subject to erroneous checks pursuant to 15c3-5 compliance.
    
    ***Book Only and Post Only orders are not subject to erroneous checks pursuant to 15c3-5 compliance.
  */
  if(newOrder->isRoutable == YES)
  {
    msgContent[38] = 'Y';
  }
  else
  {
    msgContent[38] = 'N';
  }
  
  // ISO eligiblity
  msgContent[39] = newOrder->ISO_Flag;
  
  if (OrderBucketConsume() == ERROR)
  {
    TraceLog(WARN_LEVEL, "%s order is not allowed: Order bucket is currently empty\n", GetOrderNameByIndex(oecType));
    return ERROR;
  }
  
  // Send Enter Order Message
  long time = AsyncSend(OecConfig[oecType].sessionId, msgContent, 40);
  if (time > 0)
  {
    // Set timestamp for current rawdata
    *((long*)&newOrder->dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]) = time;
    newOrder->ecnToPlaceID = oecType;
    newOrder->dataBlock.msgLen = 40;
    
    DEVLOGF("Sent %c order to %s with id: %d for %s, %d @ $%lf\n", newOrder->side, GetOrderNameByIndex(oecType), newOrder->orderID, newOrder->symbol, newOrder->shareVolume, newOrder->sharePrice);
    
    return SUCCESS;
  }
  else
  {
    return ERROR;
  }
}

/****************************************************************************
- Function name:  BuildAndSend_EDGE_OrderExtendedMsg
****************************************************************************/
int BuildAndSend_EDGE_OrderExtendedMsg(int oecType, t_OrderPlacementInfo *newOrder)
{
  t_ShortConverter shortUnion;
  t_IntConverter intUnion;
  
  // Unsequence data packet
  char *msgContent = newOrder->dataBlock.msgContent;
  shortUnion.value = 65; // packet type (1 byte)
  msgContent[0] = shortUnion.c[1];
  msgContent[1] = shortUnion.c[0];
  
  msgContent[2] = 'U';
  
  // Order message type 'N'
  msgContent[3] = 'N';
  
  // Order token (2 digit account + orderID --> 14 char)
  msgContent[4] = oecType == ORDER_EDGX? TradingAccount.EDGXAccount[0] : TradingAccount.EDGAAccount[0];
  msgContent[5] = oecType == ORDER_EDGX? TradingAccount.EDGXAccount[1] : TradingAccount.EDGAAccount[1];
  Lrc_itoafl(newOrder->orderID, ' ', 12, &msgContent[6]);
  
  // Buy/Sell indicator
  msgContent[18] = newOrder->side;
  
  // Share    
  intUnion.value = newOrder->shareVolume;
  msgContent[19] = intUnion.c[3];
  msgContent[20] = intUnion.c[2];
  msgContent[21] = intUnion.c[1];
  msgContent[22] = intUnion.c[0];
  
  // Stock
  strcncpy((unsigned char *)&msgContent[23], newOrder->rootSymbol, ' ', 6);
  
  // Price
  if (fge(newOrder->sharePrice, 1.00))
  {
    if (newOrder->side == 1)  //BUY SIDE
    {
      intUnion.value = (int)(newOrder->sharePrice * 100.00 + 0.500001) * 1000;
    }
    else
    {
      intUnion.value = (int)(newOrder->sharePrice * 100.00 + 0.000001) * 1000;
    }
  }
  else
  {
    intUnion.value = (int)(newOrder->sharePrice * 100000.00 + 0.500001);
  }
  msgContent[29] = intUnion.c[3];
  msgContent[30] = intUnion.c[2];
  msgContent[31] = intUnion.c[1];
  msgContent[32] = intUnion.c[0];
  
  // Time in Force
  msgContent[33] = 0; //IOC
  
  // Display
  msgContent[34] = 'Y';

  // Special order type
  msgContent[35] = ' ';
  
  // Extended hours eligible
  msgContent[36] = 'B';
  
  // Capacity
  msgContent[37] = 'P';
  
  // Route out eligiblity
  if(newOrder->isRoutable == YES)
  {
    msgContent[38] = 'Y';
  }
  else
  {
    msgContent[38] = 'N';
  }
  
  // ISO eligiblity
  msgContent[39] = newOrder->ISO_Flag;
  
  // Routing delivery method
  msgContent[40] = 'C';
  
  // Route strategy
  shortUnion.value = 1;
  msgContent[41] = shortUnion.c[1];
  msgContent[42] = shortUnion.c[0];
  
  // Minimum quantity
  intUnion.value = 0;
  msgContent[43] = intUnion.c[3];
  msgContent[44] = intUnion.c[2];
  msgContent[45] = intUnion.c[1];
  msgContent[46] = intUnion.c[0];
  
  // Max floor
  intUnion.value = 0;
  msgContent[47] = intUnion.c[3];
  msgContent[48] = intUnion.c[2];
  msgContent[49] = intUnion.c[1];
  msgContent[50] = intUnion.c[0];
  
  // Peg difference
  msgContent[51] = 0;
  
  // Discretionary offset
  msgContent[52] = 0;
  
  // Expire time
  t_LongConverter longUnion;
  longUnion.value = 0;
  msgContent[53] = longUnion.c[7];
  msgContent[54] = longUnion.c[6];
  msgContent[55] = longUnion.c[5];
  msgContent[56] = longUnion.c[4];
  msgContent[57] = longUnion.c[3];
  msgContent[58] = longUnion.c[2];
  msgContent[59] = longUnion.c[1];
  msgContent[60] = longUnion.c[0];
  
  // Symbol suffix
  strcncpy((unsigned char *)&msgContent[61], newOrder->symbolSuffix, ' ', 6);
  
  if (OrderBucketConsume() == ERROR)
  {
    TraceLog(WARN_LEVEL, "%s order is not allowed: Order bucket is currently empty\n", GetOrderNameByIndex(oecType));
    return ERROR;
  }
  
  // Send Enter Order Message
  long time = AsyncSend(OecConfig[oecType].sessionId, msgContent, 67);
  if (time > 0)
  {
    // Set timestamp for current rawdata
    *((long*)&newOrder->dataBlock.addContent[DATABLOCK_OFFSET_APP_TIME]) = time;
    newOrder->ecnToPlaceID = oecType;
    
    /* Special notice for EDGX/EDGA:
      Currently we don't know exactly the way to convert CMS root/suffix to original symbol (sent from Book)
      So we will save the original symbol to data at special offet, so that AS will take this original symbol to update data structure
      Special offset: In dataBlock.msgContent, right after dataBlock.msgLen, size: SYMBOL_LEN
    */
    
    newOrder->dataBlock.msgLen = 67 + SYMBOL_LEN;
    memcpy(&newOrder->dataBlock.msgContent[67], newOrder->symbol, SYMBOL_LEN);
    
    DEVLOGF("Sent %c order to %s with id: %d for (root: %s, suffix: %s), %d @ $%lf\n", newOrder->side, GetOrderNameByIndex(oecType), newOrder->orderID, newOrder->rootSymbol, newOrder->symbolSuffix, newOrder->shareVolume, newOrder->sharePrice);
    
    return SUCCESS;
  }
  else
  {
    return ERROR;
  }
}

/****************************************************************************
- Function name:  BuildAndSend_EDGE_Heartbeat
****************************************************************************/
int BuildAndSend_EDGE_Heartbeat(int oecType)
{
  t_ShortConverter shortUnion;
  char message[3];
  
  shortUnion.value = 1;
  message[0] = shortUnion.c[1];
  message[1] = shortUnion.c[0];
  
  message[2] = 'R';
  
  return SocketSend(OecConfig[oecType].socket, message, 3);
}

/****************************************************************************
- Function name:  ProcessEDGE_OrderAcceptedMsg
****************************************************************************/
int ProcessEDGE_OrderAcceptedMsg(int oecType, uint32_t symbolId, t_DataBlock *dataBlock)
{
  char tempBuffer[16] = "\0";
  
  t_ExecutionReport status;
  status.stockSymbolIndex = symbolId;
  
  // Client Order ID (Token, ignore first 2 char: account)
  memcpy(tempBuffer, &dataBlock->msgContent[14], 12);
  tempBuffer[12] = 0;
  status.clientOrderID = atoi(tempBuffer);
    
  // Accepted share
  status.acceptedShare = __builtin_bswap32(*(unsigned int*)&dataBlock->msgContent[27]);

  // Accepted price
  status.acceptedPrice = __builtin_bswap32(*(unsigned int*)&dataBlock->msgContent[37]) * 0.00001;
  
  // Arrival time
  memcpy(&status.time, &dataBlock->addContent[DATABLOCK_OFFSET_FILTER_TIME], 8);
  
  ProcessAcceptResponse(&status);
  
  Add_EDGE_DataBlockToCollection(oecType, dataBlock);
  
  DEVLOGF("Received Order Accepted from %s\n", GetOrderNameByIndex(oecType));
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessEDGE_OrderExecutedMsg
****************************************************************************/
int ProcessEDGE_OrderExecutedMsg(int oecType, uint32_t symbolId, t_DataBlock *dataBlock)
{
  char tempBuffer[16] = "\0";
  
  t_ExecutionReport status;
  status.ecnID = oecType;
  status.stockSymbolIndex = symbolId;
  
  // Client Order ID (Token, ignore first 2 chars: account)
  memcpy(tempBuffer, &dataBlock->msgContent[14], 12);
  tempBuffer[12] = 0;
  status.clientOrderID = atoi(tempBuffer);
  
  // Filled share
  status.filledShare = __builtin_bswap32(*(unsigned int*)&dataBlock->msgContent[26]);
  
  // Filled price
  status.filledPrice = __builtin_bswap32(*(unsigned int*)&dataBlock->msgContent[30]) * 0.00001;
      
  // Arrival time
  memcpy(&status.time, &dataBlock->addContent[DATABLOCK_OFFSET_FILTER_TIME], 8);

  ProcessFillResponse(&status);
  
  *(unsigned short *)&dataBlock->addContent[DATABLOCK_OFFSET_ENTRY_EXIT_REF] = status.entryExitRefNum;
  
  Add_EDGE_DataBlockToCollection(oecType, dataBlock);
  
  DEVLOGF("Received Fill from %s, %d shares at $%lf\n", GetOrderNameByIndex(oecType), status.filledShare, status.filledPrice);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessEDGE_OrderCanceledMsg
****************************************************************************/
int ProcessEDGE_OrderCanceledMsg(int oecType, uint32_t symbolId, t_DataBlock *dataBlock)
{
  char tempBuffer[16] = "\0";
  
  t_ExecutionReport status;
  status.ecnID = oecType;
  status.stockSymbolIndex = symbolId;
  
  // Client Order ID (Token, ignore first 2 chars: account)
  memcpy(tempBuffer, &dataBlock->msgContent[14], 12);
  tempBuffer[12] = 0;
  status.clientOrderID = atoi(tempBuffer);
  
  // Canceled share
  status.canceledShare = __builtin_bswap32(*(unsigned int*)&dataBlock->msgContent[26]);
  
  // Arrival time
  memcpy(&status.time, &dataBlock->addContent[DATABLOCK_OFFSET_FILTER_TIME], 8);
  
  ProcessCancelResponse(&status);
  
  *(unsigned short *)&dataBlock->addContent[DATABLOCK_OFFSET_ENTRY_EXIT_REF] = status.entryExitRefNum;
  
  Add_EDGE_DataBlockToCollection(oecType, dataBlock);
  
  DEVLOGF("Received Order Cancelled from %s for order id: %d, cancelled shares: %d\n", GetOrderNameByIndex(oecType), status.clientOrderID, status.canceledShare);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  ProcessEDGE_OrderRejectedMsg
****************************************************************************/
int ProcessEDGE_OrderRejectedMsg(int oecType, uint32_t symbolId, t_DataBlock *dataBlock)
{
  char tempBuffer[16] = "\0";
  
  t_ExecutionReport status;
  status.ecnID = oecType;
  status.stockSymbolIndex = symbolId;
  
  // Client Order ID (Token)
  memcpy(tempBuffer, &dataBlock->msgContent[14], 12);
  tempBuffer[12] = 0;
  status.clientOrderID = atoi(tempBuffer);
  
  // Arrival time
  memcpy(&status.time, &dataBlock->addContent[DATABLOCK_OFFSET_FILTER_TIME], 8);
  
  ProcessRejectResponse(&status);
  
  *(unsigned short *)&dataBlock->addContent[DATABLOCK_OFFSET_ENTRY_EXIT_REF] = status.entryExitRefNum;
  
  Add_EDGE_DataBlockToCollection(oecType, dataBlock);
  
  //Check to halt symbol
  if (dataBlock->msgContent[26] == 'F') // 'Halted'
  {
    long nsec;
    memcpy(&nsec, &dataBlock->addContent[DATABLOCK_OFFSET_FILTER_TIME], 8);
    
    UpdateHaltStatusForASymbol(symbolId, TRADING_HALTED, nsec / TEN_RAISE_TO_9, MAX_ECN_BOOK);
  }
  else if (dataBlock->msgContent[26] == 'X')  // 'Invalid price'
  {
    long nsec;
    memcpy(&nsec, &dataBlock->addContent[DATABLOCK_OFFSET_FILTER_TIME], 8);
    
    UpdateHaltStatusForASymbol(symbolId, TRADING_HALTED, nsec / TEN_RAISE_TO_9, OrderToBookMapping[oecType]);
  }
  
  DEVLOGF("Received Reject from %s, reason: %c\n", GetOrderNameByIndex(oecType), dataBlock->msgContent[26]);
  
  return SUCCESS;
}

/****************************************************************************
- Function name:  Add_EDGE_DataBlockToCollection
****************************************************************************/
void Add_EDGE_DataBlockToCollection(int oecType, t_DataBlock *dataBlock)
{
  dataBlock->blockLen = 12 + DATABLOCK_ADDITIONAL_INFO_LEN + dataBlock->msgLen;
  AddDataBlockToQueue(oecType, dataBlock);
}

/****************************************************************************
- Function name:  BuildAndSend_LogoutRequestMsg_EDGE
****************************************************************************/
int BuildAndSend_LogoutRequestMsg_EDGE(int oecType)
{
  char message[3];
  
  t_ShortConverter shortUnion;
  shortUnion.value = 1;
  message[0] = shortUnion.c[1];
  message[1] = shortUnion.c[0];

  // Message type: 1 byte
  message[2] = 'O';

  return SocketSend(OecConfig[oecType].socket, message, 3);
}

/****************************************************************************
- Function name:  ConvertSymbolFromComstockToCMS
****************************************************************************/
int ConvertSymbolFromComstockToCMS(const char *comstock, char *root, char *suffix)
{
  int i;
  int len = strnlen(comstock, SYMBOL_LEN);
  int isRoot = 1;
  int r = 0, s = 0;
  for (i = 0; i < len; i++)
  {
    switch (comstock[i])
    {
      case '.':           //when encounter '.' we ignore it and start to process suffix
        isRoot = 0;
        break;
      case '+':           //when encounter '+' we take the suffix as "WS"
        isRoot = 0;
        suffix[s++] = 'W';
        suffix[s++] = 'S';
        break;
      case '-':           //when encounter '+' we take the suffix as "PR"
        isRoot = 0;
        suffix[s++] = 'P';
        suffix[s++] = 'R';
        break;
      case '*':           //when encounter '*' we take suffix as "WI"
        isRoot = 0;
        suffix[s++] = 'W';
        suffix[s++] = 'I';
        break;
      default:
        if (isRoot == 1)
          root[r++] = comstock[i];
        else
          suffix[s++] = comstock[i];

        break;
    }
  }
  
  if (isRoot == 1)
    return ERROR;
  else
  {
    root[r] = 0;
    suffix[s] = 0;
    return SUCCESS; //converted to root & suffix format
  }
}
