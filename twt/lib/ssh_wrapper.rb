#================================================================================================
# Project		: EAA 
# File			: ssh_wrapper.rm
# Date created	: 2008-01-18
# Description	: Access file system from remote manchines by uisng SSH
# Developer     : hungpk
#================================================================================================
gem  "net-ssh", "=1.1.2"
gem "net-sftp", "=1.1.0"
require "net/ssh"
require "net/sftp"

class SshWrapper
  def initialize config
    @config = config
	@session = nil
	get_session
	@heartbeat_interval = 60
	heartbeat

  end
#================================================================================================
# Method		: get_shell
# Input			: N/A
# Output        : N/A
# Return        : SyncShell object that allow you execute normal shell command from remtoe machine
# Date created	: 2008-01-18
# Description	: Get Sync Shell 
# Developer     : hungpk
#================================================================================================

  def get_shell
    get_session.shell.sync
  end

#================================================================================================
# Method		: list_dir
# Input			: path - absolute path to remote directory 
# Output        : N/A
# Return        : List of regular files, directores or nil if the directory is not found.
# Date created	: 2008-01-18
# Description	: List remote directory contents
# Developer     : hungpk
#================================================================================================
  def list_dir path
    ret = nil
    
  end
  
#================================================================================================
# Method		: exec	
# Input			: cmd - Linux shell command
# Output        : N/A
# Return        : Output of the command as text
# Date created	: 2008-01-18
# Description	: Execute cmd from remote machine or nil if execution is fails
# Developer     : hungpk
#================================================================================================
  def exec cmd
    ret = nil 
  end

  def heartbeat
	#p  "@session:", @session
	shell = get_session.shell.sync
	t = Thread.new(shell) do |my_shell|		
		while true
			out = my_shell.date
			#puts "#{@config['host']} #{out.stdout}"
			sleep(@heartbeat_interval)		 
		end
		my_shell.exit
	end
 end
  
  private
    def get_session		
       if !@session || !@session.open?
			@session = Net::SSH.start @config['host'],                                   
                                  :username => @config['username'],
                                  :password => @config['password'],
								  :port => @config['port']
	  #elsif !@session.open?
	  #	  @session = @session.open_channel	  
	  end
	  
	  @session
    end
    
end
