$MIN_TIME_FROM = 6 * 3600 # 06:00
$MAX_TIME_TO = 19 * 3600  # 19:00
  
def img_view_steps
  ret = [
    ["05 Minutes",5],
    ["15 Minutes",15],
    ["30 Minutes",30],
    ["01 Hour",60],
    ["01 Day",91],
    ["07 Days",92],
    ["30 Days",93]    
  ]
  return ret
end

def ecn_entry
  ret = [
    ["ARCA",0],
    ["NYSE",2],
    ["ISLD",1],
    ["BX",7],
    ["PSX",9],
    ["BZX",4],
    ["BYX",8],
    ["EDGX",5],
    ["EDGA",6],
    ["RASH",3],  
    ["ALL ECNs",99]   
  ]
  return ret
end

def ecn_entry_map
  ret = [
    ["ARCA",0],
    ["ISLD",1],
    ["NYSE",2],
    ["RASH",3],
    ["BZX",4],
    ["EDGX",5],
    ["EDGA",6],
    ["BX",7],
    ["BYX",8],
    ["PSX",9],
    ["ALL ECNs",99]   
  ]
  return ret
end


def ecn_entry_fill_rates
  ret = [
    ["ARCA",0],
    ["NYSE",2],
    ["ISLD",1],
    ["BX",7],
    ["PSX",9],
    ["BZX",4],
    ["BYX",8],
    ["EDGX",5],
    ["EDGA",6],
    ["RASH",3], 
    ["BZSR",10],
    ["ALL ECNs",99]   
  ]
  return ret
end

def step_type 
  ret = [
    ["5 Minutes", 5],
    ["15 Minutes", 15],
    ["30 Minutes", 30],
    ["1 Hour", 60],
    ["1 Day", 91],
    ["1 Week", 92],
    ["1 Month", 93]   
  ]
  return ret
end

def get_step_type(start_time, end_time) 
  step = [
    ["5 Minutes", 5],
    ["15 Minutes", 15],
    ["30 Minutes", 30],
    ["1 Hour", 60],
    ["1 Day", 91],
    ["1 Week", 92],
    ["1 Month", 93]   
  ]
  period = (end_time - start_time)/(60*60*24)
  if period.to_i>90
    step.delete_if{|key,value| value.to_i<91}
  elsif period.to_i>14
    step.delete_if{|key,value| value.to_i<60}
  elsif period.to_i>7
    step.delete_if{|key,value| value.to_i<30}
  elsif period.to_i>2
    step.delete_if{|key,value| value.to_i<5}
  else
    step.delete_if{|key,value| value.to_i<5}
  end 
  return step
end

def ts_list
  ret = []
  failed = 0
  db_name_suffix = "#{Time.now.year}#{Time.now.month.to_s.rjust(2,'0')}"
  get_database_connection(db_name_suffix, "as")
  queried_ts = RiskManagement.find_by_sql("select description,ts_id from trade_servers order by description")
  if queried_ts.size > 0 then
    ret << ["All servers" , "0"]
    queried_ts.each do |tsIndex|
      ret << [ tsIndex.description , tsIndex.ts_id]     
    end
  else
    ret = [
      ["All Servers","0"],  
      ["carblink7","1"],
      ["carblink8","3"],
      ["carblink9","9"],
      ["snjblink1","7"],
      ["snjblink2","8"],
      ["wahblink3","2"],
      ["wahblink4","4"],
      ["weeblink1","5"],
      ["weeblink2","6"] 
    ]
  end
  
  return ret
end

def ts_list_plus_pm
  ret = []
  failed = 0
  db_name_suffix = "#{Time.now.year}#{Time.now.month.to_s.rjust(2,'0')}"
  get_database_connection(db_name_suffix, "as")
  queried_ts = RiskManagement.find_by_sql("select description,ts_id from trade_servers order by description")
  queried_pm = RiskManagement.find_by_sql("select description from as_settings where mid=1")
  if queried_ts.size > 0 then
    ret << ["All servers" , "0"]
    if queried_pm.size > 0 then     
      ret << [queried_pm.first.description , "-1"]
    else
      ret << ["carblink2" , "-1"]
    end
    queried_ts.each do |tsIndex|
      ret << [ tsIndex.description , tsIndex.ts_id]       
    end
  else  
    ret = [
      ["All Servers","0"],
      ["carblink2" , "-1"],
      ["carblink7","1"],
      ["carblink8","3"],
      ["carblink9","9"],
      ["snjblink1","7"],
      ["snjblink2","8"],
      ["wahblink3","2"],
      ["wahblink4","4"],
      ["weeblink1","5"],
      ["weeblink2","6"]   
    ]
    
  end
  return ret
end

def filing_status_list
  ret = [["",0],["F",1], ["CF",2], ["MR",3]]
end

def entry_exit
  ret =[["Entry",1], ["Exit",0]]
  return ret
end

def buy_sell_indicator
  ret =[["B", 0], ["S", 1], ["SS", 2], ["All", 3]]
  return ret
end

#================================================================================================
# Function    : ecn_list (action controller)
# Input       :N/A
# Description : get list of Supported ECNs
#       : ["arca","ar", "ARCA",true] :3 first elements are deferent form of ECN Name
#       : The last element indicate if the ECN is active
# Developer     : 
# Date      : 27/09/2007
#================================================================================================
def ecn_list
  ret = [
          ["arca","ar", "ARCA",true],
          ["nyse","ny", "NYSE",true],
          ["naou","is", "OUCH",true],
          ["bx","bx", "OUBX",true],
          ["psx","ps", "PSX",true],
          ["batz","bz", "BATZ",true],
          ["baty","by", "BATY",true],
          ["edgx","ex", "EDGX",true],
          ["edga","ea", "EDGA",true],
          ["nara","nr", "RASH",true]  
        ]
  
  return ret
end

def ecn_list_plus_bzx_smart_route
  ret = [
          ["arca","ar", "ARCA",true],
          ["nyse","ny", "NYSE",true],
          ["naou","is", "OUCH",true],
          ["bx","bx", "OUBX",true],
          ["psx","ps", "PSX",true],
          ["batz","bz", "BATZ",true],
          ["baty","by", "BATY",true],
          ["edgx","ex", "EDGX",true],
          ["edga","ea", "EDGA",true],
          ["nara","nr", "RASH",true],
          ["bzsr","br", "BZSR",true]         
        ]

  return ret
end

def book_ecn_list
  ret = [
          ["arca","ar", "ARCA",true],
          ["nyse","ny", "NYSE",true],
          ["amex","am", "AMEX",true],
          ["naou","is", "ISLD",true], 
          ["ndbx","bx", "NDBX",true],
          ["psx","ps", "PSX",true],
          ["batz","bz", "BATZ",true],
          ["baty","by", "BATY",true],
          ["edgx","ex", "EDGX",true],
          ["edga","ea", "EDGA",true]        
        ]
  
  return ret
end

# list ECN used for Trade Result ECN filter
def book_ecns_list 
  ret = [
          ["ARCA", "ARCA"],
          ["NYSE", "NYSE"], 
          ["AMEX", "AMEX"],
          ["ISLD", "NDAQ"],
          ["BX", "NDBX"], 
          ["PSX", "PSX"], 
          ["BZX", "BATZ"], 
          ["BYX", "BATY"], 
          ["EDGX", "EDGX"], 
          ["EDGA", "EDGA"],
          ["All", "0"]
        ]
  
  return ret
end

def launch_type_list
  ret = [
          ["All", 0],
          ["ARCA", 1], 
          ["NYSE", 3], 
          ["AMEX", 10],
          ["ISLD",2], 
          ["BX", 7],
          ["PSX", 9],
          ["BZX", 4], 
          ["BYX", 8],
          ["EDGX", 5], 
          ["EDGA", 6]         
        ]

  return ret
end

def launch_type_list_map
  ret = [
          ["All", 0],
          ["ARCA", 1], 
          ["ISLD",2] , 
          ["NYSE", 3] , 
          ["BZX", 4], 
          ["EDGX", 5], 
          ["EDGA", 6], 
          ["BX", 7],
          ["BYX", 8],
          ["PSX", 9],
          ["AMEX", 10]
        ]

  return ret
end

def display_ecn( ecn_name)
  if ecn_name.strip == "OUBX"
    ret = "BX"
  elsif ecn_name.strip == "OUCH" || ecn_name.strip == "NDAQ"
    ret = "ISLD"
  elsif ecn_name.strip == "BATZ"
    ret = "BZX"
  elsif ecn_name.strip == "BATY"
    ret = "BYX"
  else
    ret = ecn_name
  end

  return ret
end

def list_ecn_filters
  ret = [["All","0"],
    ["ARCA Entry","ARCA_Entry"],
    ["ARCA Exit","ARCA_Exit"],
    ["NYSE Entry","NYSE_Entry"],
    ["NYSE Exit","NYSE_Exit"],
    ["ISLD Entry","OUCH_Entry"],
    ["ISLD Exit","OUCH_Exit"],
    ["BX Entry","OUBX_Entry"],
    ["BX Exit","OUBX_Exit"],
    ["PSX Entry","PSX_Entry"],
    ["PSX Exit","PSX_Exit"],
    ["BZX Entry","BATZ_Entry"],
    ["BZX Exit","BATZ_Exit"],
    ["BYX Entry","BATY_Entry"],
    ["BYX Exit","BATY_Exit"],
    ["EDGX Entry","EDGX_Entry"],
    ["EDGX Exit","EDGX_Exit"],
    ["EDGA Entry","EDGA_Entry"],
    ["EDGA Exit","EDGA_Exit"],
    ["RASH Entry","RASH_Entry"],
    ["RASH Exit","RASH_Exit"],
    ["BYX Exit","BATY_Exit"]
  ]
  return ret
end

def list_ecn_filters_trade_view
  ret = [["All","0"],
    ["ARCA Entry","ARCA_Entry"],
    ["ARCA Exit","ARCA_Exit"],
    ["NYSE Entry","NYSE_Entry"],
    ["NYSE Exit","NYSE_Exit"],
    ["ISLD Entry","OUCH_Entry"],
    ["ISLD Exit","OUCH_Exit"],
    ["BX Entry","OUBX_Entry"],
    ["BX Exit","OUBX_Exit"],
    ["PSX Entry","PSX_Entry"],
    ["PSX Exit","PSX_Exit"],
    ["BZX Entry","BATZ_Entry"],
    ["BZX Exit","BATZ_Exit"],
    ["BYX Entry","BATY_Entry"],
    ["BYX Exit","BATY_Exit"],
    ["EDGX Entry","EDGX_Entry"],
    ["EDGX Exit","EDGX_Exit"],
    ["EDGA Entry","EDGA_Entry"],
    ["EDGA Exit","EDGA_Exit"],
    ["RASH Entry","RASH_Entry"],
    ["RASH Exit","RASH_Exit"],
    ["BZSR Entry","BZSR_Entry"],
    ["BZSR Exit","BZSR_Exit"]
  ]
  return ret
end

def list_ecn_filters_trade_result
  ret =  [["All","0"],
    ["ARCA Entry","arcaen"],
    ["ARCA Exit","arcaex"],
    ["NYSE Entry","nyseen"],
    ["NYSE Exit","nyseex"],
    ["ISLD Entry","ouchen"],
    ["ISLD Exit","ouchex"],
    ["BX Entry","oubxen"],
    ["BX Exit","oubxex"],
    ["PSX Entry","psxen"],
    ["PSX Exit","psxex"],
    ["BZX Entry","batzen"],
    ["BZX Exit","batzex"],
    ["BYX Entry","batyen"],
    ["BYX Exit","batyex"],
    ["EDGX Entry","edgxen"],
    ["EDGX Exit","edgxex"],
    ["EDGA Entry","edgaen"],
    ["EDGA Exit","edgaex"],
    ["RASH Entry","rashen"],
    ["RASH Exit","rashex"],
    ["BZSR Exit","bzsrex"]
  ]
  return ret
end

def account_mapping
  ret = []
  if File::exists?( "config/trading_account" ) then
    arr = IO.readlines("config/trading_account")
    count = 0
    arr.each do |x|
      if (x.lstrip.first != "#") then
        x = x.to_s
        x = x.strip       
        ret << x[(x.length-2)..(x.length-1)].to_i
      end
    end
    
  else
    puts "FILE NOT EXISTS"
    ret = [22 , 23, 15 , 14]
  end
  
  return ret
end
def color_mapping
  ret = ["#B22222" , "#00FF00" , "#0474E4" , "FFA500"]
  return ret
end

def ts_mapping(ts_id)
  #Sort Trade servers by alphabetically 
  ret = 0
  ts_list_temp_temp = ts_list
  number_ts = ts_list_temp_temp.size
  (0..number_ts-1).each do |ts|  
      if  ts_id == ts_list_temp_temp[ts][1].to_i
      ret = ts
      return ret
    end
  end
  return ret
end

def str_to_unix_time(str_date)
  (month,day,year) = str_date.split("/")
  #parse current date part to Unit Time
  ret = Time.mktime(year.to_i,month.to_i,day.to_i)
  if ret.dst? then
    ret += 3600
  end
  return ret
end

def get_prefixes_of_db
  ret = ""
  DB_CONFIG.each do |section , config|
    if section == "development"
      ret = config['database']
      ret = ret.split(ret.split("_").last).first
    end
  end
  return ret
end

def get_prefixes_of_db_external
  ret = ""
  DB_CONFIG.each do |section , config|
    if section == "external_database"
      ret = config['database']
      ret = ret.split(ret.split("_").last).first
    end
  end
  return ret
end

def check_database(db_name)
  result = `psql template1 -U postgres -c "SELECT 'avaiable' AS result  FROM pg_database WHERE datname = '#{db_name}'"`
    
  if !result.include?("avaiable") then
    prefixes = db_name.split(db_name.split("_").last).first
    db_name = prefixes + "#{Time.now.year}#{Time.now.month.to_s.rjust(2,'0')}"
    result = `psql template1 -U postgres -c "SELECT 'avaiable' AS result  FROM pg_database WHERE datname = '#{db_name}'"`
      
    if !result.include?("avaiable") then
      year_month = db_name.split("_").last.to_i
      while (year_month > 201206)
        if ((year_month - 1) % 100 == 0) then
          year_month = year_month - 1 - 100 + 12
        else
          year_month = year_month - 1
        end
        
        db_name = prefixes + year_month.to_s
        result = `psql template1 -U postgres -c "SELECT 'avaiable' AS result  FROM pg_database WHERE datname = '#{db_name}'"`
        if result.include?("avaiable") then
          return db_name
        end
      end
    end
  end
  return db_name
end

def get_database_connection(db_name_suffix, db_type)
  if db_type == "twt" then
    pre_fixes = get_prefixes_of_db_external
    db_name = pre_fixes + db_name_suffix
  else
    pre_fixes = get_prefixes_of_db
    db_name = pre_fixes + db_name_suffix
  end
  db_name = check_database(db_name)
  ActiveRecord::Base.establish_connection(
    :adapter => "postgresql",
      :host => HOST_DB,
    :username => "postgres",
    :password => "",
    :database => db_name
  )

end
