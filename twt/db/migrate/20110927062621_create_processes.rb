class CreateProcesses < ActiveRecord::Migration
  def self.up
    create_table :processes do |t|

      t.timestamps
    end
  end

  def self.down
    drop_table :processes
  end
end
