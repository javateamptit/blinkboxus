class CreateTradeBreaks < ActiveRecord::Migration
  def self.up
    create_table :trade_breaks do |t|

      t.timestamps
    end
  end

  def self.down
    drop_table :trade_breaks
  end
end
