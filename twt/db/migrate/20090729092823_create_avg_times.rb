class CreateAvgTimes < ActiveRecord::Migration
  def self.up
    create_table :avg_times do |t|

      t.timestamps
    end
  end

  def self.down
    drop_table :avg_times
  end
end
