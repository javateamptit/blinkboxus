class CreatePositionSummaries < ActiveRecord::Migration
  def self.up
    create_table :position_summaries do |t|

      t.timestamps
    end
  end

  def self.down
    drop_table :position_summaries
  end
end
