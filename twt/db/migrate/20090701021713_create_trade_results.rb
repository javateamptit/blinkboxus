class CreateTradeResults < ActiveRecord::Migration
  def self.up
    create_table :trade_results do |t|

      t.timestamps
    end
  end

  def self.down
    drop_table :trade_results
  end
end
