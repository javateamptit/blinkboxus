class CreateBrokenTrades < ActiveRecord::Migration
  def self.up
    create_table :broken_trades do |t|

      t.timestamps
    end
  end

  def self.down
    drop_table :broken_trades
  end
end
