class CreateRiskManagements < ActiveRecord::Migration
  def self.up
    create_table :risk_managements do |t|

      t.timestamps
    end
  end

  def self.down
    drop_table :risk_managements
  end
end
