class CreateRawCrosses < ActiveRecord::Migration
  def self.up
    create_table :raw_crosses do |t|

      t.timestamps
    end
  end

  def self.down
    drop_table :raw_crosses
  end
end
