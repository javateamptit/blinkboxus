class CreateRawCross2s < ActiveRecord::Migration
  def self.up
    create_table :raw_cross2s do |t|

      t.timestamps
    end
  end

  def self.down
    drop_table :raw_cross2s
  end
end
