class CreateProcessManagements < ActiveRecord::Migration
  def self.up
    create_table :process_managements do |t|

      t.timestamps
    end
  end

  def self.down
    drop_table :process_managements
  end
end
