class CreateAskBids < ActiveRecord::Migration
  def self.up
    create_table :ask_bids do |t|

      t.timestamps
    end
  end

  def self.down
    drop_table :ask_bids
  end
end
