class CreateAckFills < ActiveRecord::Migration
  def self.up
    create_table :ack_fills do |t|

      t.timestamps
    end
  end

  def self.down
    drop_table :ack_fills
  end
end
