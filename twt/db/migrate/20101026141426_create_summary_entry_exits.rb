class CreateSummaryEntryExits < ActiveRecord::Migration
  def self.up
    create_table :summary_entry_exits do |t|

      t.timestamps
    end
  end

  def self.down
    drop_table :summary_entry_exits
  end
end
