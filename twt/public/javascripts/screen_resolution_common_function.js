function setWidthOfTop() {
	var top = document.getElementsByClassName("lm_top lm_top2");
	if (top && top[0]) {
		top = top[0];
		if (Number(top.style.width.replace("px", "")) >= top.firstElementChild.firstElementChild.clientWidth) {
			top.firstElementChild.firstElementChild.style.width = top.style.width;
		}
		else {	
			top.firstElementChild.firstElementChild.style.width = "100%";
			top.firstElementChild.style.width = "100%";
			top.firstElementChild.style.width = top.firstElementChild.firstElementChild.clientWidth + "px";
			top.style.width = top.firstElementChild.firstElementChild.clientWidth + "px";
		}
	}
}

function setWidthOfScrollZone() {
	var scrollZone = document.getElementsByClassName("lm_center lm_center2 scroll_div_with_chart");
	var top = document.getElementsByClassName("lm_top lm_top2");
	if (scrollZone && scrollZone[0] && top && top[0]) {
		scrollZone[0].style.width = top[0].style.width;
	}
}

//  add event resize on window
var rtime = new Date(1, 1, 2000, 12,00,00);
var timeout = false;
var delta = 200;
window.onresize = function() {
    rtime = new Date();
    if (!timeout) {
        timeout = true;
        setTimeout(resizeend, delta);
    }
}

function resizeend() {
    if (new Date() - rtime < delta) {
        setTimeout(resizeend, delta);
    } else {
        timeout = false;
        setWidthOfTop();
        setWidthOfScrollZone();
    }               
}
// --------------------------------	