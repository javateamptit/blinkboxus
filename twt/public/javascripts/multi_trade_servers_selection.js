function addOrRemoveSelectedClass(object) {
	var value = null;
	if ((" " + object.className + " ").indexOf("li_selected") > -1) {
		removeSelectedClass(object);
		value = false;
	} else {
		addSelectedClass(object);
		value = true;
	}
	changeCheckboxStatus(object, value);
	checkAllTS();
}

function addClass2Object(object, className) {
	if ((" " + object.className + " ").indexOf(" " + className) == -1) {
		object.className += " " + className;
	}
}

function removeClass2Object(object, className) {
	if ((" " + object.className + " ").indexOf(" " + className) > -1) {
		object.className = object.className.replace(new RegExp("\\b" + className + "\\b"),'');
	}
}

function addSelectedClass(object) {
	addClass2Object(object, "li_selected");
	var input_value = "";
	var input = object.querySelector("input");
	if (input) {
		setStatus(document.getElementById("ts_status").innerHTML.replace("All servers", ""));
		if (document.getElementById("ts_status").innerHTML == "") {
			input_value = " " + input.name;
		}
		else {
			input_value = ", " + input.name;
		}
		setStatus(document.getElementById("ts_status").innerHTML + input_value);
	}
}

function removeSelectedClass(object) {
	removeClass2Object(object, "li_selected");
	var input = object.querySelector("input");
	var input_value = "";
	if (input) {
		// setStatus(document.getElementById("ts_status").innerHTML.replace(" All", ""));
		if (document.getElementById("ts_status").innerHTML.indexOf(input.name) == 1) {
			input_value = " " + input.name + ",";
			setStatus(document.getElementById("ts_status").innerHTML.replace(input_value, ""));
		}
		else if (document.getElementById("ts_status").innerHTML.indexOf("All servers") > -1){
			var list_items = document.getElementsByClassName("selections_content");
			if (list_items) {
				for (var i = 0; i < list_items.length; ++i) {
					var ls = list_items[i].querySelectorAll("li");
					for (var j = 0; j < ls.length; ++j) {
						if (ls[j] != object) {
							var input = ls[j].querySelector("input");
							input_value += ", " + input.name;
						}
					}
				}
				input_value = input_value.substr(1, input_value.length);
				setStatus(input_value);
			}
		} else{
			input_value = ", " + input.name;
			setStatus(document.getElementById("ts_status").innerHTML.replace(input_value, ""));
		}
	}
}

function changeCheckboxStatus(parentObject, value) {
	var input = parentObject.querySelector("input");
	if (input) {
		input.checked = value;
	}
}

function clear_selection() {
	var list_items = document.getElementsByClassName("selections_content");
	if (list_items) {
		for (var i = 0; i < list_items.length; ++i) {
			var ls = list_items[i].querySelectorAll("li");
			for (var j = 0; j < ls.length; ++j) {
				if ((" " + ls[j].className + " ").indexOf("li_selected") > -1) {
					removeSelectedClass(ls[j]);
					changeCheckboxStatus(ls[j], false);
				}
			}
		}
	}
	document.getElementsByName("all_ts")[0].checked = true;
	setStatus("All servers");
}

var dropdow_offsetLeft = null;
function show_hide_dropdown() {
	document.getElementsByName("all_ts")[0].disabled = true;
	// document.getElementsByName("all_ts")[0].checked = true;
	if (document.getElementById("ts_status").innerHTML.indexOf("All servers") > -1) {
		setStatus("");
	}
	
	var dropdown = document.getElementById("list_trade_servers");
	if (dropdown.style.display == 'none' || ((" " + dropdown.className + " ").indexOf("display_none") > -1)) {
		removeClass2Object(dropdown, "display_none");
		var btn_dropdown = document.getElementById("btn-dropdown");
		// var btn_dropdown = getPos(document.getElementById("btn-dropdown"));
		// dropdown.style.left = btn_dropdown.x + "px";
		// dropdown.style.top = btn_dropdown.y + "px";
		var btn_group = document.getElementById('btn-group');
		var span_title = btn_group.querySelector("span");
		if ((" " + span_title.className + " ").indexOf("left") == -1) {
			dropdown.style.left = document.getElementById("btn-dropdown").offsetLeft + 2 + "px";
		} else if (!dropdow_offsetLeft){
			dropdown.style.left = dropdown.offsetLeft + span_title.offsetWidth + 2 + "px";
			dropdow_offsetLeft = dropdown.offsetLeft;
		}
		document.getElementById("list_trade_servers").onclick = function (event) {
			event.stopPropagation();
		}
	}
	else {
		addClass2Object(dropdown, "display_none");
	}
	checkAllTS();
}

function checkAllTS() {
	var list_items = document.getElementsByClassName("selections_content");
	if (list_items) {
		for (var i = 0; i < list_items.length; ++i) {
			var ls = list_items[i].querySelectorAll("li");
			//var sum = ls.length;
			var count = 0;
			for (var j = 0; j < ls.length; ++j) {
				if ((" " + ls[j].className + " ").indexOf("li_selected") > -1) {
					count++;
				}
			}

			if (count == 0) {
				document.getElementsByName("all_ts")[0].checked = true;
				setStatus("All servers");
			}
			else {
				document.getElementsByName("all_ts")[0].checked = false;
			}
		}
	}
}

window.onclick = function (event) {
	var target = event.target;
	if (target.id == "") {
		if ((" " + target.className + " ").indexOf("btn") == -1 && (" " + target.className + " ").indexOf("criteria-wrap") == -1 && (" " + target.className + " ").indexOf("caret") == -1 && (" " + target.className + " ").indexOf("ts_status") == -1) {
			addClass2Object(document.getElementById("list_trade_servers"), "display_none");
		}
	}
	else {
		if (target.id != "ts_status" && target.id != "btn-dropdown") {
			addClass2Object(document.getElementById("list_trade_servers"), "display_none");
		}
	}
}

function setStatus(status) {
	document.getElementById("ts_status").innerHTML = status;
	document.getElementById("btn-dropdown").setAttribute("title", status);
}

function getPos(el) {
    
    var xPos = 0,
    	yPos = 0;

    while(el) {
    	xPos += (el.offsetLeft - el.scrollLeft + el.clientLeft);
    	yPos += (el.offsetTop - el.scrollTop + el.clientTop);
    	el = el.offsetParent;
    }

    return {x: xPos, y: yPos};

}

function getTradeServers() {
	var value = "";

	var TS_parentNode = document.getElementById("list_trade_servers");

	if (document.getElementsByName("all_ts")[0].checked){
		value = "0";
	}
	else {

		var ls_input = TS_parentNode.querySelectorAll("input");

		for (var i = 1; i < ls_input.length; ++i) {
			if (ls_input[i].checked) {
				value += "," + ls_input[i].value;
			}
		}
	}

	if (value == "") {
		value = "0";
	}

	return (value != "0" ? value.substr(1, value.length) : value);
}


function setTSfromParams(paramsStr) {
	if (paramsStr == "0") {
		checkAllTS();
	}
	else if (paramsStr != "") {
		var ts_list = paramsStr.split(",");
		var list_input = document.getElementsByClassName("ts_item");

		for (var i = 0; i < ts_list.size(); ++i) {
			for (var j = 0; j < list_input.size(); ++j) {
				if (list_input[j].firstChild && list_input[j].firstChild.value == ts_list[i]) {
					addOrRemoveSelectedClass(list_input[j]);
				}
			}
		}
	}
	// Replace state
	var url = document.URL;
	var state = url.substr(0, url.indexOf("&ts")).split("/").last();
	window.history.replaceState(null, null, state);
}