
// Place your application-specific JavaScript functions and classes here
// This file is automatically included by javascript_include_tag :defaults

  var oldLink = null;
    function check_Time(el){		
        try{
            var	startTime = document.getElementById("start_time");
            var endTime = document.getElementById("end_time");
            var iStartTime = Date.parse(startTime.value);
            var iEndTime = Date.parse(endTime.value);
            
            if (iStartTime > iEndTime){
                    if(el.id == "start_time"){
                            endTime.value = startTime.value;
                    }else{
                            startTime.value = endTime.value;
                    }
            }
        }catch(e){
            alert(e.message);
         }
            return false;
    }

	function disableAll(){
		document.getElementById("home1").className = "menu";
		document.getElementById("fill_rate1").className = "menu";
		document.getElementById("trade_result1").className = "menu";
		document.getElementById("trade_view1").className = "menu";
		document.getElementById("order_view1").className = "menu";
		document.getElementById("position_summary1").className = "menu";
		document.getElementById("utilities1").className = "menu";
		document.getElementById("ack_fill1").className = "menu";		
		document.getElementById("activity1").className = "menu";	
		document.getElementById("server_management1").className = "menu";	
		document.getElementById("hidden_liquidity1").className = "menu";	
	}
	   
   function swap(e){
	disableAll();                  
	e.className = "menu-active";	
   }

//================================================================================================
// Function		: Effect for mouse over and out
// Input		: 
// Description	: 
// Developer	: tuanbd
// Date			: 07/08/2009 10:23:25 AM
// Note			: Nothing here
//================================================================================================
function n_over(e) {	
	e.className = 'm_over';
}

function n_out(e) {		
	var id = e.id.split("__")[0];
	if ( id % 2 == 0)
	{		
		e.className = "table_even_row";
	}
	else
	{
		e.className = "table_odd_row";
	}
	//e.className = 'm_out';
	//e.style.color = 'black';
}

function n_over2(e) {	
	e.className = 'm_over';
}

function n_out2(e) {		
	var id=e.id.split("__")[1];
	var id_status = $('status').value;
	
	if (id != id_status)
	{
		e.className = "table_even_row";	
	}
	else
	{
		e.className = "location m_over";	
	}
	//e.className = "table_even_row";	
}
