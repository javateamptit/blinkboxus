if (detectBrowser().toLowerCase().indexOf("firefox") == -1) {
    var scroll_div = document.getElementsByClassName("scroll_div_with_chart");
    if (scroll_div.length == 1) {
        var mouseWheelEvt = function (e)
        {
            var event = e || window.event;
           var offsetMove = 20;
            // Detect whether OS is Mac and browser is Safari
            if (navigator.appVersion.indexOf("Mac")!=-1 && navigator.userAgent.indexOf("Safari")!=-1) {
                offsetMove = 5;
            }
            if (scroll_div[0].doScroll)
                scroll_div[0].doScroll(event.wheelDelta>0?"left":"right");
            else if ((event.wheelDelta || event.detail) > 0)
               scroll_div[0].scrollTop -= offsetMove;
            else
               scroll_div[0].scrollTop += offsetMove;

            return false;
        }

        // window.onload=function(el){
            if ("onmousewheel" in document.body)
                scroll_div[0].onmousewheel = mouseWheelEvt;
            else
                scroll_div[0].addEventListener("DOMMouseScroll", mouseWheelEvt);
        // }
    }
}

function detectBrowser(){
    var strBrowser = navigator.sayswho = (function(){
        var ua= navigator.userAgent, tem, 
        M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*([\d\.]+)/i) || [];
        if(/trident/i.test(M[1])){
            tem=  /\brv[ :]+(\d+(\.\d+)?)/g.exec(ua) || [];
            return 'IE '+(tem[1] || '');
        }
        M= M[2]? [M[1], M[2]]:[navigator.appName, navigator.appVersion, '-?'];
        if((tem= ua.match(/version\/([\.\d]+)/i))!= null) M[2]= tem[1];
        return M.join(' ');
    })();
    return strBrowser;
}