function setPositionOfTradeViewContent(div_content, div_center, div_container, div_paging_top) {
	var el = document.getElementById(div_content);
	var center_div = document.getElementById(div_center);
	var trade_view_container = document.getElementById(div_container);
	var margin_padding = 4 + 4 + 4 + 4; // Should look at css file
	trade_view_container.style.width = el.style.width = center_div.offsetWidth;
	trade_view_container.style.height = center_div.offsetHeight;
	el.style.height = center_div.offsetHeight - document.getElementById(div_paging_top).offsetHeight * 2 - margin_padding;
}

function resizeScrollDivWhenClick(paging_top_id, paging_bottom_id, div_content, div_center, div_container, div_paging_top) {
	addFunction2Paging(paging_top_id, paging_top_id, paging_bottom_id, div_content, div_center, div_container, div_paging_top);
	addFunction2Paging(paging_bottom_id, paging_top_id, paging_bottom_id, div_content, div_center, div_container, div_paging_top);
}

function addFunction2Paging(paging_id, paging_top_id, paging_bottom_id, div_content, div_center, div_container, div_paging_top) {
	var tradeview_link_list = document.getElementById(paging_id);
	if (tradeview_link_list) {
		var list_a_tags = tradeview_link_list.querySelectorAll("a");
		for(var i = 0; i < list_a_tags.length - 1; i++) {
			if (list_a_tags[i].getAttribute('onclick')) {
				var cur_function = list_a_tags[i].getAttribute('onclick').replace("}); return false;", "");
				append_function = ', onComplete:function(){setPositionOfTradeViewContent("' +  div_content + '","' + div_center + '","' + div_container + '","' + div_paging_top + '");resizeScrollDivWhenClick("' + paging_top_id + '","' + paging_bottom_id + '","' + div_content + '","' + div_center + '","' + div_container + '","' + div_paging_top + '");}}); return false;';
				list_a_tags[i].setAttribute('onclick', cur_function + append_function);
			}
		}
	}
}