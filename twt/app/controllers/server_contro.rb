class ServerManagementController < ApplicationController
  def index
    @uc = get_uc
    render :layout=>"interface2"
  end

  def show_process
    @uc = get_uc
    @as_status = "UP"
    @as_value = 1

    @pm_status = "UP"
    @pm_value = 1

    @ts1_status = "UP"
    @ts1_value = 1

    @ts2_status = "UP"
    @ts2_value = 1

    @ts3_status = "UP"
    @ts3_value = 1

    @ts4_status = "UP"
    @ts4_value = 1

    SERVER_CONFIG.each do |server_name , config|

      if server_name.to_s == "as"
        full_path = config['base_dir']
        ssh = REMOTE_SERVER[server_name]
        if ssh !=nil
          shell = ssh.get_shell
          out = shell.exec "#{full_path}/twt_checkas.pl"
          if out.stdout.size < 5
            @as_status = "DOWN"
            @as_value = 0
          end
        end
      end

      if server_name.to_s == "pm"
        full_path = config['base_dir']
        ssh = REMOTE_SERVER[server_name]
        if ssh !=nil
          shell = ssh.get_shell
          out = shell.exec "#{full_path}/twt_checkpm.pl"
          if out.stdout.size < 5
            @pm_status = "DOWN"
            @pm_value = 0
          end
        end
      end

      if server_name.to_s == "ts1"
        full_path = config['base_dir']
        ssh = REMOTE_SERVER[server_name]

        if ssh !=nil
          shell = ssh.get_shell
          out = shell.exec "#{full_path}/twt_checkts.pl"

          if out.stdout.size < 5
            @ts1_status = "DOWN"
            @ts1_value = 0

          end
        end
      end

      if server_name.to_s == "ts2"
        full_path = config['base_dir']
        ssh = REMOTE_SERVER[server_name]
        if ssh !=nil
          shell = ssh.get_shell
          out = shell.exec "#{full_path}/twt_checkts.pl"

          if out.stdout.size < 5
            @ts2_status = "DOWN"
            @ts2_value = 0

          end
        end
      end

      if server_name.to_s == "ts3"
        full_path = config['base_dir']
        ssh = REMOTE_SERVER[server_name]
        if ssh !=nil
          shell = ssh.get_shell
          out = shell.exec "#{full_path}/twt_checkts.pl"

          if out.stdout.size < 5
            @ts3_status = "DOWN"
            @ts3_value = 0

          end
        end
      end

      if server_name.to_s == "ts4"
        full_path = config['base_dir']
        ssh = REMOTE_SERVER[server_name]
        if ssh !=nil
          shell = ssh.get_shell
          out = shell.exec "#{full_path}/twt_checkts.pl"

          if out.stdout.size < 5
            @ts4_status = "DOWN"
            @ts4_value = 0

          end
        end
      end

    end


  end

  def process_script
    @uc = get_uc
    db_name = "eaa10_#{Time.now.year}#{Time.now.month.to_s.rjust(2,'0')}"

    get_database_connection(db_name)
    time_t = Time.now

    time_stamp = "'"
    time_stamp << time_t.strftime("%H:%M:%S")
    time_stamp << "'"

    date_t = time_t.strftime("%Y-%m-%d")
    date = "'"
    date << date_t.to_s
    date << "'"

    code = params[:code]
    status = params[:status]

    ip_t = request.remote_addr
    ip ="'"
    ip << ip_t.to_s
    ip << "'"
    event = ""

    SERVER_CONFIG.each do |server_name , config|
      if (server_name.to_s == "as") and (code.to_i == 0)
        full_path = config['base_dir']
        ssh = REMOTE_SERVER[server_name]
        if ssh !=nil
          shell = ssh.get_shell
          if status.to_i == 0
            out = shell.exec "#{full_path}/twt_runas.pl"
            event = "'run AS'"
          else
            out = shell.exec "#{full_path}/twt_stopas.pl"
            event = "'stop AS'"
          end

        end
      end

      if (server_name.to_s == "pm") and (code.to_i == -1)
        full_path = config['base_dir']
        ssh = REMOTE_SERVER[server_name]
        if ssh !=nil
          shell = ssh.get_shell
          if status.to_i == 0
            out = shell.exec "#{full_path}/twt_runpm.pl"
            event = "'run PM'"
          else
            out = shell.exec "#{full_path}/twt_stoppm.pl"
            event = "'stop PM'"
          end

        end
      end

      if (server_name.to_s == "ts1") and (code.to_i == 1)
        full_path = config['base_dir']
        ssh = REMOTE_SERVER[server_name]
        if ssh !=nil
          shell = ssh.get_shell
          if status.to_i == 0
            out = shell.exec "#{full_path}/twt_runts.pl"
            event = "'run TS1'"
          else
            out = shell.exec "#{full_path}/twt_stopts.pl"
            event = "'stop TS1'"
          end

        end
      end

      if (server_name.to_s == "ts2") and (code.to_i == 2)
        full_path = config['base_dir']
        ssh = REMOTE_SERVER[server_name]

        if ssh !=nil
          shell = ssh.get_shell
          if status.to_i == 0
            out = shell.exec "#{full_path}/twt_runts.pl"
            event = "'run TS2'"
          else
            out = shell.exec "#{full_path}/twt_stopts.pl"
            event = "'stop TS2'"
          end

        end
      end

      if (server_name.to_s == "ts3") and (code.to_i == 3)
        full_path = config['base_dir']
        ssh = REMOTE_SERVER[server_name]
        if ssh !=nil
          shell = ssh.get_shell
          if status.to_i == 0
            out = shell.exec "#{full_path}/twt_runts.pl"
            event = "'run TS3'"
          else
            out = shell.exec "#{full_path}/twt_stopts.pl"
            event = "'stop TS3'"
          end

        end
      end

      if (server_name.to_s == "ts4") and (code.to_i == 4)
        full_path = config['base_dir']
        ssh = REMOTE_SERVER[server_name]
        if ssh !=nil
          shell = ssh.get_shell
          if status.to_i == 0
            out = shell.exec "#{full_path}/twt_runts.pl"
            event = "'run TS4'"
          else
            out = shell.exec "#{full_path}/twt_stopts.pl"
            event = "'stop TS4'"
          end

        end
      end

    end

    ret = ProcessManagement.find_by_sql("insert into twt_event_log(ip_address,event,date,time_stamp) values(#{ip},#{event},#{date},#{time_stamp})")

  end

  def show_event
    @uc = get_uc
    @page = 1
    @page = (params[:page] || 1).to_i
    if  @page <= 1 
      @page = 1
    end
    @items_per_page = 50
    @offset = (@page - 1) * @items_per_page
    @count = 0
    if params[:trade_date]
      @uc.trade_date = str_to_unix_time(params[:trade_date])
    end

    #Set up connection to database which wants to use
    db_name = "eaa10_#{@uc.trade_date.year}#{@uc.trade_date.month.to_s.rjust(2,'0')}"
    # Connect to database need to query data
    connect_to_database(db_name)

    trade_date = @uc.trade_date.strftime("%Y-%m-%d")

    @data_log = ProcessManagement.find_by_sql("select * from twt_event_log where date = '#{trade_date}'")
    if @data_log.size > 0 
      @count = @data_log.size
    end

    @object_pages = WillPaginate::Collection.new(@page, @items_per_page, @count)
  end

  def log_viewer
    @uc = get_uc

    @log_files = []
    #Number of lines of current log file
    @count = 0
    #contents of selected files. Array of lines

    if params[:trade_date]
      @uc.trade_date = str_to_unix_time(params[:trade_date])
    end


    #Check start_time, end_time more than Time now
    check_datetime

    @log_files = []
    LOG_CONFIG.each do |server_name, config|

      full_path = "#{config['base_dir']}/#{@uc.trade_date.year}/#{sprintf("%.2d", @uc.trade_date.month)}/#{sprintf("%.02d", @uc.trade_date.day)}"
      if config['host'] == 'localhost'
        @log_files << [server_name, Dir["#{full_path}/*.log"]]
      else
        ssh = REMOTE_LOG[server_name]
        if ssh !=nil

          shell = ssh.get_shell
          out = shell.ls " -Am #{full_path}/*.log"
          #    puts server_name ,out.stdout
          log_files = out.stdout.split(",")
          @log_files << [server_name, log_files]
          shell.exit
        end
      end

    end
    render :layout=> "interface2"
  end

  def show_log
    @uc = get_uc

    @lines_per_page = 100
    @page = (params[:page] || 1).to_i
    if  @page <= 1 
      @page = 1
    end
    @offset = @page * @lines_per_page
    #list of log files that will be showed on left panel
    # @log_files = [server_name, [file_list],
    #        ]
    @log_lines = []
    @log_file_name = params[:file_name]
    @server = params[:server]
    @key_word_count=0
    @key_word_array = []

    @current_hit = (params[:hit] || 1).to_i
    if  @current_hit < 1 
      @current_hit = 1
    end

    if params[:key_word] #search 
      key_word=params[:key_word].strip.gsub(/ +/," ").gsub('[', '\[').gsub('*', '\*').gsub('-', '\-').gsub('$', '\$').gsub('"', '\"').gsub('.', '\.')
      if @log_file_name && @server

        config = LOG_CONFIG[@server]
        full_path = "#{config['base_dir']}/#{@uc.trade_date.year}/#{sprintf("%.2d", @uc.trade_date.month)}/#{sprintf("%.02d", @uc.trade_date.day)}"

        log_file_name_with_path = "#{full_path}/#{@log_file_name}"

        #puts log_file_name_with_path
        line_count_cmd = "wc -l #{log_file_name_with_path}"

        if !key_word.index("\t").nil? 
            arr = key_word.split("\t")
        if arr.size > 1 
        str_key = arr[0].to_s + "\t" + arr[arr.size-1].to_s
        else
        str_key = arr[0].to_s
        end
        key_word = str_key
        end
        key_word = '"' + key_word + '"' 

        if config['host'] == 'localhost'
          #puts "Local"
          line_count_output = `#{line_count_cmd}`
          @count = line_count_output.split(" ").first.to_i

          ###############
          key_word_cmd="grep -i -c #{key_word} #{log_file_name_with_path}"
          key_word_output=`#{key_word_cmd}`
       
          @key_word_count = key_word_output.split(" ").first.to_i
        
          total_key_word ="grep -n -m #{@key_word_count} -i #{key_word} #{log_file_name_with_path} | awk -F: '{print $1}'"
          total_key_word_cmd =`#{total_key_word}`
          @key_word_array = total_key_word_cmd.split("\n")


          #go to first hit
          if params[:s]
            issearched = params[:s].strip.to_i
        if  @current_hit > @key_word_count 
        @current_hit = @key_word_count
        end
        #Get current hit line number
        @current_hit_line_number=@key_word_array[@current_hit-1].to_i
            if issearched ==1 #first hit
              if @current_hit_line_number % @lines_per_page == 0
                @page= @current_hit_line_number / @lines_per_page
              else
                @page= (@current_hit_line_number / @lines_per_page) + 1
              end
              if  @page < 1 
                @page = 1
              end
              # calculate offset again
              @offset = @page * @lines_per_page
            end
          end
          ###############

          if @count % @lines_per_page == 0
            number_page = @count / @lines_per_page
          else
            number_page = (@count / @lines_per_page) + 1
          end 

          if @page == number_page
            lines_in_page = @count - (number_page - 1)*100
            cmd = "tail -#{100*(number_page-@page+1)} #{log_file_name_with_path} | tail -#{lines_in_page}"
          else 
            if @page>number_page
              @page=number_page
        @offset = @page * @lines_per_page
            end
            last_page = @count - (number_page - 1)*100
            lines_in_page = 100
            cmd = "tail -#{(100*(number_page-@page)+last_page)} #{log_file_name_with_path} | head -#{lines_in_page}"
          end

          @log_lines = `#{cmd}`
        else
          ssh = REMOTE_LOG[@server]
          shell = ssh.get_shell
          out = shell.wc " -l #{log_file_name_with_path}"
          @count = out.stdout.split(" ").first.to_i

          ###############
          key_word_output=shell.grep "-i -c #{key_word} #{log_file_name_with_path}"
          @key_word_count = key_word_output.stdout.split(" ").first.to_i
        
          total_key_word_cmd =shell.grep "-n -m #{@key_word_count} -i #{key_word} #{log_file_name_with_path} | awk -F: '{print $1}'"
          @key_word_array = total_key_word_cmd.stdout.split("\n")

          #go to first hit
          if params[:s]
            issearched = params[:s].strip.to_i
        if  @current_hit > @key_word_count 
        @current_hit = @key_word_count
        end
        #Get current hit line number
        @current_hit_line_number=@key_word_array[@current_hit-1].to_i
            if issearched ==1 #first hit
              if @current_hit_line_number % @lines_per_page == 0
                @page= @current_hit_line_number / @lines_per_page
              else
                @page= (@current_hit_line_number / @lines_per_page) + 1
              end
              if  @page < 1 
                @page = 1
              end
              # calculate offset again
              @offset = @page * @lines_per_page
            end
          end
          ###############

          if @count % @lines_per_page == 0
            number_page = @count / @lines_per_page
          else
            number_page = (@count / @lines_per_page) + 1
          end

          if @page == number_page
            lines_in_page = @count - (number_page - 1)*100
            out = shell.tail "-#{100*(number_page-@page+1)} #{log_file_name_with_path} | tail -#{lines_in_page}"
          else 
            if @page>number_page
              @page=number_page
        @offset = @page * @lines_per_page
            end
            last_page = @count - (number_page - 1)*100
            lines_in_page = 100
            out = shell.tail "-#{(100*(number_page-@page)+last_page)} #{log_file_name_with_path} | head -#{@lines_per_page}"
          end


          @log_lines = out.stdout
          shell.exit
        end
      end
    else # not search
      if @log_file_name && @server

        config = LOG_CONFIG[@server]
        full_path = "#{config['base_dir']}/#{@uc.trade_date.year}/#{sprintf("%.2d", @uc.trade_date.month)}/#{sprintf("%.02d", @uc.trade_date.day)}"

        log_file_name_with_path = "#{full_path}/#{@log_file_name}"

        line_count_cmd = "wc -l #{log_file_name_with_path}"

        if config['host'] == 'localhost'
          line_count_output = `#{line_count_cmd}`
          @count = line_count_output.split(" ").first.to_i

          if @count % @lines_per_page == 0
            number_page = @count / @lines_per_page
          else
            number_page = (@count / @lines_per_page) + 1
          end 

          if @page == number_page
            lines_in_page = @count - (number_page - 1)*100
            cmd = "tail -#{100*(number_page-@page+1)} #{log_file_name_with_path} | tail -#{lines_in_page}"
          else 
            if @page>number_page
                @page=number_page
          @offset = @page * @lines_per_page
            end
            last_page = @count - (number_page - 1)*100
            lines_in_page = 100
            cmd = "tail -#{(100*(number_page-@page)+last_page)} #{log_file_name_with_path} | head -#{lines_in_page}"
          end

          @log_lines = `#{cmd}`
        else
          ssh = REMOTE_LOG[@server]
          shell = ssh.get_shell
          out = shell.wc " -l #{log_file_name_with_path}"
          @count = out.stdout.split(" ").first.to_i

          if @count % @lines_per_page == 0
            number_page = @count / @lines_per_page
          else
            number_page = (@count / @lines_per_page) + 1
          end

          if @page == number_page
            lines_in_page = @count - (number_page - 1)*100
            out = shell.tail "-#{100*(number_page-@page+1)} #{log_file_name_with_path} | tail -#{lines_in_page}"
          else 
            if @page>number_page
              @page=number_page
        @offset = @page * @lines_per_page
            end
            last_page = @count - (number_page - 1)*100
            lines_in_page = 100
            out = shell.tail "-#{(100*(number_page-@page)+last_page)} #{log_file_name_with_path} | head -#{lines_in_page}"
          end

          @log_lines = out.stdout
          shell.exit
        end
      end
    end

    if  @page < 1 
      @page = 1
    end
    #@object_pages = Paginator.new self, @count, @lines_per_page, @page
    @object_pages = WillPaginate::Collection.new(@page, @lines_per_page ,@count)
    #render :layout=> "interface2"
  end

  def upgrade_version
    @uc = get_uc
    render :layout=>"interface2"
  end

  def process_upgrade
    @uc = get_uc
    # get trader upgrade 
    if params[:trader]
      trader = params[:trader].to_s
    end

    if trader == "mark"
      user = 1
    elsif trader == "nick"
      user = 2
    else 
      user = 3
    end
    #end of get trader upgrade

    # get version to upgrade
    if params[:version]
      version = params[:version]
    end
    # end get

    # get restart upgrade
    if params[:run] == "yes"
      run = "y"
    else 
      run = "n"
    end
    # end get

    #get note 
    if params[:note]
      trader_note = params[:note]
    end
    #end get

    #get module 
    modul = params[:module]
    #en get

    SERVER_CONFIG.each do |server_name , config|

      if (server_name.to_s == "as") and (modul == "as")
        full_path = config['base_dir']
        ssh = REMOTE_SERVER[server_name]
        if ssh !=nil
          shell = ssh.get_shell
          out = shell.exec "#{full_path}/twt_upgrade_new_as.pl #{version} #{user} #{run}"

        end
      end

      if (server_name.to_s == "pm") and (modul == "pm")
        full_path = config['base_dir']
        ssh = REMOTE_SERVER[server_name]
        if ssh !=nil
          shell = ssh.get_shell
          out = shell.exec "#{full_path}/twt_upgrade_new_pm.pl #{version} #{user} #{run}"

        end
      end

      if ((server_name.to_s == "ts1") and (modul == "ts1")) or ((server_name.to_s == "ts1") and (modul == "allts"))
        full_path = config['base_dir']
        ssh = REMOTE_SERVER[server_name]
        if ssh !=nil
          shell = ssh.get_shell
          out = shell.exec "#{full_path}/twt_upgrade_new_ts.pl #{version} #{user} #{run}"
        end
      end

      if ((server_name.to_s == "ts2") and (modul == "ts2")) or ((server_name.to_s == "ts2") and (modul == "allts"))
        full_path = config['base_dir']
        ssh = REMOTE_SERVER[server_name]
        if ssh !=nil
          shell = ssh.get_shell
          out = shell.exec "#{full_path}/twt_upgrade_new_ts.pl #{version} #{user} #{run}"
        end
      end

      if ((server_name.to_s == "ts3") and (modul == "ts3")) or ((server_name.to_s == "ts3") and (modul == "allts"))
        full_path = config['base_dir']
        ssh = REMOTE_SERVER[server_name]
        if ssh !=nil
          shell = ssh.get_shell
          out = shell.exec "#{full_path}/twt_upgrade_new_ts.pl #{version} #{user} #{run}"
        end
      end

      if ((server_name.to_s == "ts4") and (modul == "ts4")) or ((server_name.to_s == "ts4") and (modul == "allts"))
        full_path = config['base_dir']
        ssh = REMOTE_SERVER[server_name]
        if ssh !=nil
          shell = ssh.get_shell
          out = shell.exec "#{full_path}/twt_upgrade_new_ts.pl #{version} #{user} #{run}"
        end
      end
    end

    @strResult = ""
    accept = 0
    accept_ts1 = 0
    accept_ts2 = 0
    accept_ts3 = 0
    accept_ts4 = 0
    SERVER_CONFIG.each do |server_name , config|

      if (server_name.to_s == "as") and (modul == "as")
        full_path = config['base_dir']
        path_trading = config['dir_trading']
        ssh = REMOTE_SERVER[server_name]
        if ssh !=nil
          shell = ssh.get_shell
          if run == 'y'
            out = shell.exec "#{full_path}/twt_checkas.pl"

            if out.stdout.size > 5 
              @strResult << " AS is upgraded successfully with version #{version}"
              accept = 1
            else 
              @strResult << " AS is not upgraded"
            end
          else 
            out = shell.tail "-1 #{path_trading}/version_info.txt"
            str =  out.stdout.to_s.split(" ")
            if str[str.size - 2] == version
              accept = 1
              @strResult << " AS is upgraded successfully with version #{version}"
            else 
              @strResult << " AS is not upgraded"
            end
          end
        end
      end

      if (server_name.to_s == "pm") and (modul == "pm")
        full_path = config['base_dir']
        path_trading = config['dir_trading']
        ssh = REMOTE_SERVER[server_name]
        if ssh !=nil
          shell = ssh.get_shell
          if run == 'y'
            out = shell.exec "#{full_path}/twt_checkpm.pl"

            if out.stdout.size > 5 
              @strResult << " PM is upgraded successfully with version #{version}"
              accept = 1
            else 
              @strResult << " PM is not upgraded"
            end
          else 
            out = shell.tail "-1 #{path_trading}/version_info.txt"
            str =  out.stdout.to_s.split(" ")
            if str[str.size - 2] == version
              accept = 1
              @strResult << " PM is upgraded successfully with version #{version}"
            else 
              @strResult << " PM is not upgraded"
            end
          end
        end
      end

      if ((server_name.to_s == "ts1") and (modul == "ts1")) or ((server_name.to_s == "ts1") and (modul == "allts"))
        full_path = config['base_dir']
        path_trading = config['dir_trading']
        ssh = REMOTE_SERVER[server_name]
        if ssh !=nil
          shell = ssh.get_shell
          if run == 'y'
            out = shell.exec "#{full_path}/twt_checkts.pl"

            if out.stdout.size > 5 
              @strResult << " TS1 is upgraded successfully with version #{version} <br/>"
              accept = 1
              accept_ts1 = 1
            else 
              @strResult << " TS1 is not upgraded <br/>"
            end
          else 
            out = shell.tail "-1 #{path_trading}/version_info.txt"
            str =  out.stdout.to_s.split(" ")
            if str[str.size - 2] == version
              accept = 1
              accept_ts1 = 1
              @strResult << " TS1 is upgraded successfully with version #{version} <br/>"
            else
              @strResult << " TS1 is not upgraded <br/>"
            end
          end
        end
      end

      if ((server_name.to_s == "ts2") and (modul == "ts2")) or ((server_name.to_s == "ts2") and (modul == "allts"))
        full_path = config['base_dir']
        path_trading = config['dir_trading']
        ssh = REMOTE_SERVER[server_name]
        if ssh !=nil
          shell = ssh.get_shell
          if run == 'y'
            out = shell.exec "#{full_path}/twt_checkts.pl"

            if out.stdout.size > 5 
              @strResult << " TS2 is upgraded successfully with version #{version} <br/>"
              accept = 1
              accept_ts2 = 1
            else 
              @strResult << " TS2 is not upgraded <br/>"
            end
          else 
            out = shell.tail "-1 #{path_trading}/version_info.txt"
            str =  out.stdout.to_s.split(" ")
            if str[str.size - 2] == version
              accept = 1
              accept_ts2 = 1
              @strResult << " TS2 is upgraded successfully with version #{version} <br/>"
            else
              @strResult << " TS2 is not upgraded <br/>"
            end
          end
        end
      end

      if ((server_name.to_s == "ts3") and (modul == "ts3")) or ((server_name.to_s == "ts3") and (modul == "allts"))
        full_path = config['base_dir']
        path_trading = config['dir_trading']
        ssh = REMOTE_SERVER[server_name]
        if ssh !=nil
          shell = ssh.get_shell
          if run == 'y'
            out = shell.exec "#{full_path}/twt_checkts.pl"

            if out.stdout.size > 5 
              @strResult << " TS3 is upgraded successfully with version #{version} <br/>"
              accept = 1
              accept_ts3 = 1
            else 
              @strResult << " TS3 is not upgraded <br/>"
            end
          else 
            out = shell.tail "-1 #{path_trading}/version_info.txt"
            str =  out.stdout.to_s.split(" ")
            if str[str.size - 2] == version
              accept = 1
              accept_ts3 = 1
              @strResult << " TS3 is upgraded successfully with version #{version} <br/>"
            else
              @strResult << " TS3 is not upgraded <br/>"
            end
          end
        end
      end

      if ((server_name.to_s == "ts4") and (modul == "ts4")) or ((server_name.to_s == "ts4") and (modul == "allts"))
        full_path = config['base_dir']
        path_trading = config['dir_trading']
        ssh = REMOTE_SERVER[server_name]
        if ssh !=nil
          shell = ssh.get_shell
          if run == 'y'
            out = shell.exec "#{full_path}/twt_checkts.pl"

            if out.stdout.size > 5 
              @strResult << " TS4 is upgraded successfully with version #{version} <br/>"
              accept = 1
              accept_ts4 = 1
            else 
              @strResult << " TS4 is not upgraded <br/>"
            end
          else 
            out = shell.tail "-1 #{path_trading}/version_info.txt"
            str =  out.stdout.to_s.split(" ")
            if str[str.size - 2] == version
              accept = 1
              accept_ts4 = 1
              @strResult << " TS4 is upgraded successfully with version #{version} <br/>"
            else
              @strResult << " TS4 is not upgraded <br/>"
            end
          end
        end
      end
    end

    if ((accept_ts1.to_i == 1) and (accept_ts2.to_i == 1) and (accept_ts3.to_i == 1) and (accept_ts4.to_i == 1) and (modul == "allts"))

      @strResult = " All TSs are upgraded successfully"

    end

    if ((accept_ts1.to_i == 0) and (accept_ts2.to_i == 0) and (accept_ts3.to_i == 0) and (accept_ts4.to_i == 0) and (modul == "allts"))

      @strResult = " All TSs are not upgraded"

    end

    db_name = "eaa10_#{Time.now.year}#{Time.now.month.to_s.rjust(2,'0')}"

    get_database_connection(db_name)
    time_t = Time.now

    date_t = time_t.strftime("%Y-%m-%d")
    date = "'"
    date << date_t.to_s
    date << "'"

    trader_db = "'"
    trader_db << trader.to_s
    trader_db << "'"

    module_db = "'"
    module_db << modul.to_s
    module_db << "'"

    trader_note_db = "'"
    trader_note_db << trader_note.to_s
    trader_note_db << "'"


    version_db = "'"
    version_db << version.to_s
    version_db << "'"

    if ((modul != "allts") and (accept == 1))
      str = "insert into version_control(module,ver,trader,trader_note,date) values(#{module_db},#{version_db},#{trader_db},#{trader_note_db},#{date})"

      ret = VersionControl.find_by_sql("#{str}")
    elsif (modul == "allts")

      module_db = "'"
      module_db << "ts1"
      module_db << "'"
      if (accept_ts1 == 1) 
        ret = VersionControl.find_by_sql("insert into version_control(module,ver,trader,trader_note,date) values(#{module_db},#{version_db},#{trader_db},#{trader_note_db},#{date})")
      end
      module_db = "'"
      module_db << "ts2"
      module_db << "'"
      if (accept_ts2 == 1)
        ret = VersionControl.find_by_sql("insert into version_control(module,ver,trader,trader_note,date) values(#{module_db},#{version_db},#{trader_db},#{trader_note_db},#{date})")
      end

      module_db = "'"
      module_db << "ts3"
      module_db << "'"
      if (accept_ts3 == 1)
        ret = VersionControl.find_by_sql("insert into version_control(module,ver,trader,trader_note,date) values(#{module_db},#{version_db},#{trader_db},#{trader_note_db},#{date})")
      end
      module_db = "'"
      module_db << "ts4"
      module_db << "'"
      if (accept_ts4 == 1)
        ret = VersionControl.find_by_sql("insert into version_control(module,ver,trader,trader_note,date) values(#{module_db},#{version_db},#{trader_db},#{trader_note_db},#{date})")
      end
    end

    render :js =>"show_msg_result('#{@strResult}');"
  end

  def show_upgrade_version
    db_name = "eaa10_#{Time.now.year}#{Time.now.month.to_s.rjust(2,'0')}"
    get_database_connection(db_name)

    @data_as = VersionControl.find_by_sql("select * from version_control where module = 'as' order by id desc limit 7")

    @data_pm = VersionControl.find_by_sql("select * from version_control where module = 'pm' order by id desc limit 7")
    @data_ts1 = VersionControl.find_by_sql("select * from version_control where module = 'ts1' order by id desc limit 7")
    @data_ts2 = VersionControl.find_by_sql("select * from version_control where module = 'ts2' order by id desc limit 7")
    @data_ts3 = VersionControl.find_by_sql("select * from version_control where module = 'ts3' order by id desc limit 7")
    @data_ts4 = VersionControl.find_by_sql("select * from version_control where module = 'ts4' order by id desc limit 7")

  end

  def menu_refresh
    @uc = get_uc
  end
end
