class LogController < ApplicationController

  def index
    @uc = get_uc

  @log_files = []
  #Number of lines of current log file
  @count = 0
  #contents of selected files. Array of lines

  if params[:trade_date]
    @uc.trade_date = str_to_unix_time(params[:trade_date])
  end

  #Add on 2009/05/14
  #Check start_time, end_time more than Time now
  check_datetime

  @log_files = []
  LOG_CONFIG.each do |server_name, config|

    full_path = "#{config['base_dir']}/#{@uc.trade_date.year}/#{sprintf("%.2d", @uc.trade_date.month)}/#{sprintf("%.02d", @uc.trade_date.day)}"
    if config['host'] == 'localhost'
      @log_files << [server_name, Dir["#{full_path}/*.log"]]
    else
      ssh = REMOTE_LOG[server_name]
      if ssh !=nil

        shell = ssh.get_shell
        out = shell.ls " -Am #{full_path}/*.log"
        #    puts server_name ,out.stdout
        log_files = out.stdout.split(",")
        @log_files << [server_name, log_files]
        shell.exit
      end
    end

  end
  render :layout=> "interface2"
 end
 def show_detail
    @uc = get_uc

  @lines_per_page = 100
  @page = (params[:page] || 1).to_i
  if  @page <= 1 
    @page = 1
  end
  @offset = @page * @lines_per_page
  #list of log files that will be showed on left panel
  # @log_files = [server_name, [file_list],
  #        ]
  @log_lines = []
  @log_file_name = params[:file_name]
  @server = params[:server]
  @key_word_count=0
  @key_word_array = []

  @current_hit = (params[:hit] || 1).to_i
  if  @current_hit < 1 
    @current_hit = 1
  end

  if params[:key_word] #search 
    key_word=params[:key_word].strip.gsub(/ +/," ").gsub('[', '\[').gsub('*', '\*').gsub('-', '\-').gsub('$', '\$').gsub('"', '\"').gsub('.', '\.')
    if @log_file_name && @server

      config = LOG_CONFIG[@server]
      full_path = "#{config['base_dir']}/#{@uc.trade_date.year}/#{sprintf("%.2d", @uc.trade_date.month)}/#{sprintf("%.02d", @uc.trade_date.day)}"

      log_file_name_with_path = "#{full_path}/#{@log_file_name}"

      #puts log_file_name_with_path
      line_count_cmd = "wc -l #{log_file_name_with_path}"

      if !key_word.index("\t").nil? 
          arr = key_word.split("\t")
          if arr.size > 1 
            str_key = arr[0].to_s + "\t" + arr[arr.size-1].to_s
          else
            str_key = arr[0].to_s
          end
          key_word = str_key
      end
      key_word = '"' + key_word + '"' 

      if config['host'] == 'localhost'
        #puts "Local"
        line_count_output = `#{line_count_cmd}`
        @count = line_count_output.split(" ").first.to_i

        ###############
        key_word_cmd="grep -i -c #{key_word} #{log_file_name_with_path}"
        key_word_output=`#{key_word_cmd}`
     
        @key_word_count = key_word_output.split(" ").first.to_i
      
        total_key_word ="grep -n -m #{@key_word_count} -i #{key_word} #{log_file_name_with_path} | awk -F: '{print $1}'"
        total_key_word_cmd =`#{total_key_word}`
        @key_word_array = total_key_word_cmd.split("\n")


        #go to first hit
        if params[:s]
          issearched = params[:s].strip.to_i
          if  @current_hit > @key_word_count 
            @current_hit = @key_word_count
          end
          #Get current hit line number
          @current_hit_line_number=@key_word_array[@current_hit-1].to_i
          if issearched ==1 #first hit
            if @current_hit_line_number % @lines_per_page == 0
              @page= @current_hit_line_number / @lines_per_page
            else
              @page= (@current_hit_line_number / @lines_per_page) + 1
            end
            if  @page < 1 
              @page = 1
            end
            # calculate offset again
            @offset = @page * @lines_per_page
          end
        end
        ###############

        if @count % @lines_per_page == 0
          number_page = @count / @lines_per_page
        else
          number_page = (@count / @lines_per_page) + 1
        end 

        if @page == number_page
          lines_in_page = @count - (number_page - 1)*100
          cmd = "tail -#{100*(number_page-@page+1)} #{log_file_name_with_path} | tail -#{lines_in_page}"
        else 
            if @page>number_page
            @page=number_page
            @offset = @page * @lines_per_page
          end
          last_page = @count - (number_page - 1)*100
          lines_in_page = 100
          cmd = "tail -#{(100*(number_page-@page)+last_page)} #{log_file_name_with_path} | head -#{lines_in_page}"
        end

        @log_lines = `#{cmd}`
      else
        ssh = REMOTE_LOG[@server]
        shell = ssh.get_shell
        out = shell.wc " -l #{log_file_name_with_path}"
        @count = out.stdout.split(" ").first.to_i

        ###############
        key_word_output=shell.grep "-i -c #{key_word} #{log_file_name_with_path}"
        @key_word_count = key_word_output.stdout.split(" ").first.to_i
      
        total_key_word_cmd =shell.grep "-n -m #{@key_word_count} -i #{key_word} #{log_file_name_with_path} | awk -F: '{print $1}'"
        @key_word_array = total_key_word_cmd.stdout.split("\n")

        #go to first hit
        if params[:s]
          issearched = params[:s].strip.to_i
          if  @current_hit > @key_word_count 
            @current_hit = @key_word_count
          end
          #Get current hit line number
          @current_hit_line_number=@key_word_array[@current_hit-1].to_i
          if issearched ==1 #first hit
            if @current_hit_line_number % @lines_per_page == 0
              @page= @current_hit_line_number / @lines_per_page
            else
              @page= (@current_hit_line_number / @lines_per_page) + 1
            end
            if  @page < 1 
              @page = 1
            end
            # calculate offset again
            @offset = @page * @lines_per_page
          end
        end
        ###############

        if @count % @lines_per_page == 0
          number_page = @count / @lines_per_page
        else
          number_page = (@count / @lines_per_page) + 1
        end

        if @page == number_page
          lines_in_page = @count - (number_page - 1)*100
          out = shell.tail "-#{100*(number_page-@page+1)} #{log_file_name_with_path} | tail -#{lines_in_page}"
        else 
            if @page>number_page
            @page=number_page
            @offset = @page * @lines_per_page
          end
          last_page = @count - (number_page - 1)*100
          lines_in_page = 100
          out = shell.tail "-#{(100*(number_page-@page)+last_page)} #{log_file_name_with_path} | head -#{@lines_per_page}"
        end


        @log_lines = out.stdout
        shell.exit
      end
    end
  else # not search
    if @log_file_name && @server

      config = LOG_CONFIG[@server]
      full_path = "#{config['base_dir']}/#{@uc.trade_date.year}/#{sprintf("%.2d", @uc.trade_date.month)}/#{sprintf("%.02d", @uc.trade_date.day)}"

      log_file_name_with_path = "#{full_path}/#{@log_file_name}"

      line_count_cmd = "wc -l #{log_file_name_with_path}"

      if config['host'] == 'localhost'
        line_count_output = `#{line_count_cmd}`
        @count = line_count_output.split(" ").first.to_i

        if @count % @lines_per_page == 0
          number_page = @count / @lines_per_page
        else
          number_page = (@count / @lines_per_page) + 1
        end 

        if @page == number_page
          lines_in_page = @count - (number_page - 1)*100
          cmd = "tail -#{100*(number_page-@page+1)} #{log_file_name_with_path} | tail -#{lines_in_page}"
        else 
            if @page>number_page
              @page=number_page
              @offset = @page * @lines_per_page
          end
          last_page = @count - (number_page - 1)*100
          lines_in_page = 100
          cmd = "tail -#{(100*(number_page-@page)+last_page)} #{log_file_name_with_path} | head -#{lines_in_page}"
        end

        @log_lines = `#{cmd}`
      else
        ssh = REMOTE_LOG[@server]
        shell = ssh.get_shell
        out = shell.wc " -l #{log_file_name_with_path}"
        @count = out.stdout.split(" ").first.to_i

        if @count % @lines_per_page == 0
          number_page = @count / @lines_per_page
        else
          number_page = (@count / @lines_per_page) + 1
        end

        if @page == number_page
          lines_in_page = @count - (number_page - 1)*100
          out = shell.tail "-#{100*(number_page-@page+1)} #{log_file_name_with_path} | tail -#{lines_in_page}"
        else 
            if @page>number_page
            @page=number_page
            @offset = @page * @lines_per_page
          end
          last_page = @count - (number_page - 1)*100
          lines_in_page = 100
          out = shell.tail "-#{(100*(number_page-@page)+last_page)} #{log_file_name_with_path} | head -#{lines_in_page}"
        end

        @log_lines = out.stdout
        shell.exit
      end
    end
  end

  if  @page < 1 
    @page = 1
  end
  #@object_pages = Paginator.new self, @count, @lines_per_page, @page
  @object_pages = WillPaginate::Collection.new(@page, @lines_per_page ,@count)
    #render :layout=> "interface2"
  end
  def menu_refresh
  @uc = get_uc
  end
end
