class ServerManagementController < ApplicationController

  def index
    @uc = get_uc
    render :layout=>"interface2"
  end

  def show_tt_version()

    @data_tt = []

    TT_CONFIG.each do |server_name, config|
      tt_file = []
      full_path = config['base_dir']
      number_of_tt = config['number_of_tt_displayed'].to_i


      tt_list_file = Dir["#{full_path}/*.{zip}"]
      tt_list_file.each { |x|
          tt_file << x.split("/").last
      }
      tt_file = tt_file.sort {|x,y|  y<=>x}
      if tt_file.size > number_of_tt then
        tt_file = tt_file[0..(number_of_tt-1)]
      else
        tt_file = tt_file
      end
      @data_tt = tt_file.sort {|x,y|  y<=>x}



    end

  end

  def download_tt 
    TT_CONFIG.each do |server_name , config|
      if (server_name.to_s == "tt")
        full_path = config['base_dir']


        unless File.exist?("#{full_path}/#{params[:file_name]}")
            render:text=>"The version not found!"
        else
          send_file "#{full_path}/#{params[:file_name]}", :type=>"application/zip"
        end

      end
    end
  end

  def menu_refresh
    @uc = get_uc
  end


end
