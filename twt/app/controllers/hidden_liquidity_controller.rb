class HiddenLiquidityController < ApplicationController

  def index
    #get user context 
    @uc = get_uc    
    @uc.ecn = "0"
    if params[:oid]
      @uc.trade_date = Time.now
      @seqidfrom = params[:oid]
      @seqidto = params[:oid]
    end
    @last_date_trading =  RiskManagement.find_by_sql("select max(date) as trade_date from new_orders" ).first.trade_date  
    #check trade_date is null
    if @last_date_trading == nil
      @last_date_trading = Time.now.strftime("%Y-%m-%d")
    end
    @check_date = Time.mktime(@last_date_trading.to_date.year,@last_date_trading.to_date.month,@last_date_trading.to_date.day)
    @uc.last_date_trading = @check_date 
    if @uc.trade_date > @check_date
      @uc.trade_date = @check_date  
    end
    
    render :layout=>"interface2"
  end

  def show_order
    #get user context 
    @uc = get_uc  
    @page = params[:page] && params[:page].to_i > 0 ? params[:page].to_i : 1
    @items_per_page = 100
    @offset = (@page - 1) * @items_per_page
    #sort order
    @dir = params[:dir] || "asc"  
    #filter by sequence ID
    @seqidfrom = params[:seqidfrom] || "0"   
    @seqidto = params[:seqidto] || "5000000"
    #sort field
    @order_field = params[:sort_field] || "trade_time"  
    #take trade date as format mm/dd/yyyy  
    if params[:trade_date]
      @uc.trade_date = str_to_unix_time(params[:trade_date])
    end

    @uc.trade_start_time =  params[:trade_start_time] if params[:trade_start_time]
    @uc.trade_end_time =  params[:trade_end_time] if params[:trade_end_time]
    @uc.trade_start_time = @uc.trade_start_time || "6:00"
    @uc.trade_end_time = @uc.trade_end_time || "19:00"
    @list_traders = params[:list_traders] || "All"
    list_traders = @list_traders
    check_datetime  
    conditions = ["trade_date = ?","trade_time between ? and ?"]
    values = [@uc.trade_date, @uc.trade_start_time, @uc.trade_end_time]  
    db_name_suffix = "#{@uc.trade_date.year}#{@uc.trade_date.month.to_s.rjust(2,'0')}"  
    connect_to_database(db_name_suffix)  
    if @dir == "asc"
      @dir = "desc"
    else
      @dir = "asc"
    end
    #filter by filled price
    if params[:price_from]  && params[:price_from].to_f > 0
      @uc.price_from = params[:price_from]  
    else
      @uc.price_from = nil
    end
    
    if params[:price_to] && params[:price_to].to_f > 0  
      @uc.price_to = params[:price_to]    
    else
      @uc.price_to = nil
    end

    if @uc.price_from || @uc.price_to  
      conditions << " qty > lvs "
    end
    if @uc.price_from && @uc.price_to
      conditions << " exists( select fills.oid from fills where traded_orders.id = fills.oid and fills.price between ? and  ?)"
      values << @uc.price_from.to_f
      values << @uc.price_to.to_f
    elsif @uc.price_to 
      conditions << " exists( select fills.oid from fills where traded_orders.id = fills.oid and fills.price <= ?)"
      values << @uc.price_to.to_f
    elsif @uc.price_from
      conditions << " exists( select fills.oid from fills where traded_orders.id = fills.oid and fills.price >= ?)"
      values << @uc.price_from.to_f
    end  
    
    #filter by symbol  
    if params[:symbol]  && params[:symbol].to_s !=""
      @uc.symbol = params[:symbol].strip
      conditions << "symbol = ?"
      values << @uc.symbol.upcase  
    else
      @uc.symbol = nil
    end

    if (params[:ecn] && params[:ecn].to_s != "0" && params[:ecn].to_s != "") then
      @uc.ecn = params[:ecn]
      conditions << "ecn = ?"
      values << @uc.ecn
    else
      @uc.ecn = "0"
    end
  
    if params[:seqidfrom] && params[:seqidfrom].to_i >0
      @seqidfrom = params[:seqidfrom]    
    end
    if params[:seqidto] && params[:seqidto].to_i > 0 && params[:seqidto].to_i < 10000000 
      @seqidto = params[:seqidto]    
    end

    if @seqidfrom.to_i > 0
      conditions << "seqid >= ?"
      values << @seqidfrom.to_i
    end
    if @seqidto.to_i > 0 &&  @seqidto.to_i < 10000000
      conditions << "seqid <= ?"
      values << @seqidto.to_i
    end
    
    #filter by iso orders
    @iso_order = "0"
    if params[:iso_order] && params[:iso_order].to_s == "1"
      #check trade_date must be more than 01/01/2010
      if @uc.trade_date > str_to_unix_time("01/01/2010")
        @iso_order = "1"
        conditions << "is_iso is not NULL and is_iso ='Y'"
      end
    end
    if params[:iso_order] && params[:iso_order].to_s == "2"
      #check trade_date must be more than 01/01/2010
      if @uc.trade_date > str_to_unix_time("01/01/2010")
        @iso_order = "2"
        conditions << "is_iso is not NULL and is_iso ='N'"
      end
    end
    #filter by entry/exit
    @entry_exit = params[:entry_exit].to_i
    if  @entry_exit == 1
      conditions << "entry_exit = 'Entry'"
    elsif @entry_exit == 2
      conditions << "entry_exit = 'Exit'"
    end

    if list_traders == "All"
      condition_str = conditions.join(" and ")
      conditions = []
      conditions << condition_str
      conditions += values
      
      #number of traded stocks of given day
      @count = Order.count(:conditions=>conditions)
      @object_pages = WillPaginate::Collection.new(@page, @items_per_page, @count)
      @orders = Order.find(  :all,              
                  :conditions=>conditions,
                  :offset=>@offset,
                  :limit => @items_per_page,
                  :order=>"#{@order_field} #{@dir}, trade_time asc "
                  )
    else
      trade_day = @uc.trade_date.year.to_s << "-" << @uc.trade_date.month.to_s.rjust(2,'0') << "-" << @uc.trade_date.day.to_s.rjust(2,'0')   
      if @seqidfrom == ""
        @seqidfrom = "1"
      end
      if @seqidto == ""
        @seqidto = "49999"
      end
      list_traders = list_traders.downcase
      str_sql = "select substring(log_detail from 3 for 5) from tt_event_log where username='#{list_traders}' and date = '#{trade_day}' and activity_type = 4"
      activity_id = Activity.find_by_sql("#{str_sql}")
      str_cond ="("
      activity_id.each { |act|
        id_in_number = act.substring.to_i
        str_cond << act.substring.to_s
        str_cond << ","
      }
      str_cond << "49999)" 
      if activity_id.size > 0
        sqlquery = "select * from traded_orders where seqid in #{str_cond} and trade_date = '#{trade_day}' and trade_time between '#{@uc.trade_start_time}' and '#{@uc.trade_end_time}'"
      else 
        sqlquery = "select * from traded_orders where seqid = 49999 and trade_date = '#{trade_day}' and trade_time between '#{@uc.trade_start_time}' and '#{@uc.trade_end_time}'"   
      end
      if params[:iso_order] == "1" # Not care the ISO ORDERS FILTER, THIS CONDIDION SUBTRACT THE ISO ORDERS
        sqlquery << " and seqid = -1"
      else
        sqlquery << " and seqid >= #{@seqidfrom}"
        sqlquery << " and seqid <= #{@seqidto}"
      end
      if @uc.price_from && @uc.price_to
        sqlquery << "and exists( select fills.oid from fills where traded_orders.id = fills.oid and fills.price between #{@uc.price_from.to_f} and #{@uc.price_to.to_f}) "
      elsif @uc.price_to 
        sqlquery << "and exists( select fills.oid from fills where traded_orders.id = fills.oid and fills.price <= #{@uc.price_to.to_f})"
      elsif @uc.price_from
        sqlquery << "and exists( select fills.oid from fills where traded_orders.id = fills.oid and fills.price >= #{@uc.price_from.to_f})"
      end     
      if params[:ecn] != "0"
        sqlquery << " and ecn = '#{@uc.ecn}'"
      end
      if params[:symbol] != ""
        symbol_t = @uc.symbol.upcase
        sqlquery << " and symbol = '#{symbol_t}'"
      end     
      @orders = Order.find_by_sql("#{sqlquery}")
      @count = @orders.size
      @object_pages = WillPaginate::Collection.new(@page, @items_per_page, @count)  
    end 
    ids = []
    @orders.each{ |order|
      if order.qty.to_i > order.lvs.to_i
        ids << order.id
      end
    }
    cee_fields = ", b.filing_status, b.cee_note, b.is_cee" if (@uc.trade_date > CEE_DATE)
    
    brokens = Order.find_by_sql(["select  f.oid, b.new_price, b.new_shares, b.broken_shares #{cee_fields}  from fills f, brokens b where (f.oid in (?)) and f.fill_id = b.fill_id",ids])

    brokens.each{ |broken|
      broken_order = @orders.find{|o| o.id.to_i == broken.oid.to_i}
      if broken.is_cee == "f"
        broken_order["is_broken"] = true 
        broken_order["new_price"] = broken.new_price    
      
        if broken_order["broken_shares"] 
          broken_order["broken_shares"] += broken.broken_shares.to_i
        else
          broken_order["broken_shares"] = broken.broken_shares.to_i
        end
        broken_order["new_shares"] = broken.new_shares
      end 
    
    }
    
    @orders.each{ |order|
      if order["is_broken"]
        order["status_edit"] = "Broken"
        order["price"] = order.price 
        order["lvs"] = order.lvs + order["broken_shares"].to_i      
      else
        order["status_edit"] = order.status_edit
        order["price"] = order.price
        order["lvs"] = order.lvs
      end  
    }    
    
    end

  #================================================================================================
  # Function    : menu_refresh (action controller)
  # Input        : N/A
  # Date        : 2010/02/25
  #================================================================================================
  def menu_refresh
    @uc = get_uc
  end
  
  #================================================================================================
  # Function Name : xls_export
  # Input          : N/A
  # Developer     : TuanBD
  # Date Created  : August 11, 2010
  #================================================================================================  
  def xls_export
    #get user context 
    @uc = get_uc  
    #sort order
    @dir = params[:dir] || "asc"    
    #filter by sequence ID
    @seqidfrom = params[:seqidfrom] || "0"   
    @seqidto = params[:seqidto] || "5000000"
    #sort field
    @order_field = params[:sort_field] || "trade_time"  
    #take trade date as format mm/dd/yyyy  
    if params[:trade_date]
      @uc.trade_date = str_to_unix_time(params[:trade_date])
    end
    @uc.trade_start_time =  params[:trade_start_time] if params[:trade_start_time]
    @uc.trade_end_time =  params[:trade_end_time] if params[:trade_end_time]
    @uc.trade_start_time = @uc.trade_start_time || "6:00"
    @uc.trade_end_time = @uc.trade_end_time || "19:00"
    check_datetime  
    conditions = ["trade_date = ?","trade_time between ? and ?"]
    values = [@uc.trade_date, @uc.trade_start_time, @uc.trade_end_time]  
    filename_xls_export = [@uc.trade_date.strftime("%Y%m%d").to_s]
    #filter by ECN order  
    if params[:ecn] && params[:ecn].to_s != "0"
      @uc.ecn = params[:ecn]
      conditions << "ecn = ?"
      values << @uc.ecn
      filename_xls_export << @uc.ecn.to_s
    else
      @uc.ecn = "0"
    end
    filename_xls_export << @uc.trade_start_time.to_s
    filename_xls_export << @uc.trade_end_time.to_s
    #Set up connection to database which wants to use
    db_name_suffix = "#{@uc.trade_date.year}#{@uc.trade_date.month.to_s.rjust(2,'0')}"  
    # Connect to database need to query data
    connect_to_database(db_name_suffix)
    #filter by filled price
    if params[:price_from]  && params[:price_from].to_f > 0
      @uc.price_from = params[:price_from]
      filename_xls_export << @uc.price_from.to_s
    else
      @uc.price_from = nil
    end
    if params[:price_to] && params[:price_to].to_f > 0  
      @uc.price_to = params[:price_to]
      filename_xls_export << @uc.price_to.to_s
    else
      @uc.price_to = nil
    end
    if @uc.price_from || @uc.price_to  
      conditions << " qty > lvs "
    end
    if @uc.price_from && @uc.price_to
      conditions << " exists( select fills.oid from fills where traded_orders.id = fills.oid and fills.price between ? and  ?)"
      values << @uc.price_from.to_f
      values << @uc.price_to.to_f
    elsif @uc.price_to 
      conditions << " exists( select fills.oid from fills where traded_orders.id = fills.oid and fills.price <= ?)"
      values << @uc.price_to.to_f
    elsif @uc.price_from
      conditions << " exists( select fills.oid from fills where traded_orders.id = fills.oid and fills.price >= ?)"
      values << @uc.price_from.to_f
    end  
    #filter by symbol  
    if params[:symbol]  && params[:symbol].to_s !=""
      @uc.symbol = params[:symbol].strip
      conditions << "symbol = ?"
      values << @uc.symbol.upcase  
      filename_xls_export << @uc.symbol.upcase
    else
      @uc.symbol = nil
    end  
    #filter by sequence id
    if params[:seqidfrom] && params[:seqidfrom].to_i >0
      @seqidfrom = params[:seqidfrom]    
      filename_xls_export << @seqidfrom.to_s 
    end
    if params[:seqidto] && params[:seqidto].to_i > 0 && params[:seqidto].to_i < 5000000 
      @seqidto = params[:seqidto]    
      filename_xls_export << @seqidto.to_s
    end
    if @seqidfrom.to_i > 0
      conditions << "seqid >= ?"
      values << @seqidfrom.to_i
    end
    if @seqidto.to_i > 0 &&  @seqidto.to_i < 5000000
      conditions << "seqid <= ?"
      values << @seqidto.to_i
    end
    #filter by iso orders
    @iso_order = "0"
    if params[:iso_order] && params[:iso_order].to_s == "1"
      #check trade_date must be more than 01/01/2010
      if @uc.trade_date > str_to_unix_time("01/01/2010")
        @iso_order = "1"
        conditions << "is_iso is not NULL and is_iso ='Y'"
        filename_xls_export << "ISO_Order"
      end    
    end

    if params[:iso_order] && params[:iso_order].to_s == "2"
      #check trade_date must be more than 01/01/2010
      if @uc.trade_date > str_to_unix_time("01/01/2010")
        @iso_order = "2"
        conditions << "is_iso is not NULL and is_iso ='N'"
      end
    end
    
    #filter by entry/exit
    @entry_exit = params[:entry_exit].to_i
    if  @entry_exit == 1
      conditions << "entry_exit = 'Entry'"
      filename_xls_export << "Entry"
    elsif @entry_exit == 2
      conditions << "entry_exit = 'Exit'"
      filename_xls_export << "Exit"
    end
    condition_str = conditions.join(" and ")
    conditions = []
    conditions << condition_str
    conditions += values
    @orders = Order.find(  :all,              
                :conditions=>conditions,
                :order=>"#{@order_field} #{@dir}, trade_time asc "
                )
    ids = []  
    @orders.each{ |order|
      if order.qty.to_i > order.lvs.to_i
        ids << order.id
      end
    }
    
    cee_fields = ", b.filing_status, b.cee_note" if (@uc.trade_date > CEE_DATE)
    brokens = Order.find_by_sql(["select  f.oid, b.new_price, b.new_shares, b.broken_shares #{cee_fields} from fills f, brokens b where (f.oid in (?)) and f.fill_id = b.fill_id",ids])
    brokens.each{ |broken|
      broken_order = @orders.find{|o| o.id.to_i == broken.oid.to_i}
      broken_order["is_broken"] = true
      broken_order["new_price"] = broken.new_price    
      if broken_order["broken_shares"] 
        broken_order["broken_shares"] += broken.broken_shares.to_i
      else
        broken_order["broken_shares"] = broken.broken_shares.to_i
      end    
      broken_order["new_shares"] = broken.new_shares
    }
  
    @orders.each{ |order|
      if order["is_broken"]
        order["status_edit"] = "Broken"
        order["price"] = order.price 
        order["lvs"] = order.lvs + order["broken_shares"].to_i      
      else
        order["status_edit"] = order.status_edit
        order["price"] = order.price
        order["lvs"] = order.lvs
      end
    }
    if !@orders.empty?
      filename_str = filename_xls_export.join("_")
      column_names = ["Symbol", "Account" , "Action", "Visible Shares", "Order Size", "OverSize Shares"  , "%Hidden Liquidity", "Price", "Filled Shares", "Avg Filled Price", "Cancelled Shares", "ECN", "OID", "TIF", "Status", "Time"]    
      @file_path = ExportFile.export_file_to_xls(column_names, @orders, "Hidden_Liquidity", filename_str)  
      if File.exists?(@file_path)
      send_file @file_path, :filename => File.basename(@file_path)
      else
      render :text => "File not found"  
      end
    else
      render :text => "No data for export file."  
    end
 
  end

  
end
