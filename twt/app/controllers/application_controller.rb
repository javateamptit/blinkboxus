# Filters added to this controller apply to all controllers in the application.
# Likewise, all the methods added will be available for all controllers.
require 'ldap'
require 'time'

class ApplicationController < ActionController::Base
  helper :all # include all helpers, all the time
  protect_from_forgery :only=>[:create, :update, :destroy] # See ActionController::RequestForgeryProtection for details

  before_filter :check_datetime
  before_filter :authenticate
  
  # Scrub sensitive parameters from your log
  # filter_parameter_logging :password
  CEE_DATE = Time.mktime(2010,5,1)
  def db_prefixes
    ret = ""
    pool = ""
    DB_CONFIG.each do |section , config|
      if section == "development"
        ret = config['database']
        ret = ret.split(ret.split("_").last).first
        pool = config['pool']
      end
    end
    return ret
  end

  def db_external_prefixes
    ret = ""
    DB_CONFIG.each do |section , config|
      if section == "external_database"
        ret = config['database']
        ret = ret.split(ret.split("_").last).first
      end
    end
    return ret
  end

  def get_color_config
#    if !session[:color_config]
#      session[:color_config] = ColorConfig.new
#    end
#
#    session[:color_config]
    ColorConfig.new

  end

  def get_uc
    if !session[:uc_us]
      session[:uc_us] = UserContext.new        
    end
    session[:uc_us]
  end

  def check_datetime
    #Add on 2009/04/29
    #Check start_time, end_time more than Time now
    @uc = get_uc
     if @uc.trade_date.to_date > Time.now.to_date
      @uc.trade_date = Time.mktime(Time.now.year, Time.now.month, Time.now.mday)
     end   
     if @uc.start_time.to_date > Time.now.to_date
      @uc.start_time = Time.mktime(Time.now.year, Time.now.month, Time.now.mday,6)
     end
     if @uc.end_time.to_date > Time.now.to_date
      @uc.end_time = Time.mktime(Time.now.year, Time.now.month, Time.now.mday,21)
     end
     if @uc.start_time.to_date > @uc.end_time.to_date 
      @uc.start_time = @uc.end_time
     end
  end


  def verify_database(db_name)
    result = `psql template1 -U postgres -c "SELECT 'avaiable' AS result  FROM pg_database WHERE datname = '#{db_name}'"`
    if !result.include?("avaiable") then
      prefixes = db_name.split(db_name.split("_").last).first
      db_name = prefixes + "#{Time.now.year}#{Time.now.month.to_s.rjust(2,'0')}"
      result = `psql template1 -U postgres -c "SELECT 'avaiable' AS result  FROM pg_database WHERE datname = '#{db_name}'"`
      if !result.include?("avaiable") then
        year_month = db_name.split("_").last.to_i
        while (year_month > 201206)
          if ((year_month - 1) % 100 == 0) then
            year_month = year_month - 1 - 100 + 12
          else
            year_month = year_month - 1
          end

          db_name = prefixes + year_month.to_s
          result = `psql template1 -U postgres -c "SELECT 'avaiable' AS result  FROM pg_database WHERE datname = '#{db_name}'"`
          if result.include?("avaiable") then
            return db_name
          end
        end
      end
    end

    return db_name
  end

  def connect_to_database(db_name_suffix)
    db_name = db_prefixes + db_name_suffix
    db_name = verify_database(db_name)
    ActiveRecord::Base.establish_connection(
    :adapter => "postgresql",
    :host => HOST_DB,
    :username => "postgres",
    :password => "",
    :database => db_name
  )
  end

  #Set up connection to the external database for TWT2
  #In case the database is not placed on the local machine
  def connect_to_external_database(db_name_suffix)
    db_name = db_external_prefixes + db_name_suffix
    db_name = verify_database(db_name)
    ActiveRecord::Base.establish_connection(
    :adapter => "postgresql",
    :host => HOST_DB,
    :username => "postgres",
    :password => "",
    :database => db_name
  )
  end

  def authenticate
    hostname = ""
    port = 0
    dn = ""

    LDAP_CONFIG.each do |section, config|
      hostname = config['hostname'].to_s
      port = config['port'].to_i
      dn = config['dn'].to_s
    end

    conn = LDAP::SSLConn.new(hostname, port)
    authenticate_or_request_with_http_basic "RGM Employees only" do |username, password|
      begin
        conn.simple_bind("uid=#{username.strip}, #{dn}", "#{password}")
        conn.unbind
        conn = nil
        break
      rescue
        conn.perror("BIND:")
        conn.unbind
        conn = nil
      end
    end

    trap("INT") {quit_on_int} 
    trap("TERM") {quit_on_int}
  end

  def quit_on_int 
    Process.kill 9, Process.pid
  end 
end
