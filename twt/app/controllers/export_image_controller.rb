require 'RMagick'
class ExportImageController < ApplicationController

  def index
    width = params[:width].to_i
    height = params[:height].to_i

  p params[:width]
  p params[:height]
  #p params
    data = {}
    img = Magick::Image.new(width, height)
    height.times do |y|
      row = params["r#{y}"].split(',')
      row.size.times do |r|
        pixel = row[r].to_s.split(':')
        pixel[0] = pixel[0].to_s.rjust(6, '0')
        if pixel.size == 2
          pixel[1].to_i.times do
            (data[y] ||= []) << pixel[0]
          end
        else
          (data[y] ||= []) << pixel[0]
        end
      end
      width.times do |x|
        img.pixel_color(x, y, "##{data[y][x]}")
      end
    end
    img.format = "JPEG"
    #send_data(img.to_blob , :disposition => 'inline', :type => 'image/jpg', :filename => "chart.jpg?#{rand(99999999).to_i}")
  send_data(img.to_blob , :disposition => 'attachment', :type => 'image/jpg', :filename => "chart_#{rand(99999999).to_i}.jpg")
  end
end
