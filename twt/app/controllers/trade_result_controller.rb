
class TradeResultController < ApplicationController

  #$trade_result = ""
#================================================================================================
# Function    : index (action controller)
# Input        : N/A
#================================================================================================
  def index
    #get user context
    @uc = get_uc
    @uc.cross_size = 0
    @last_date_trading =  RiskManagement.find_by_sql("select max(date) as trade_date from new_orders" ).first.trade_date
    #check trade_date is null
    if @last_date_trading == nil
      @last_date_trading = Time.now.strftime("%Y-%m-%d")
    end
    @check_date = Time.mktime(@last_date_trading.to_date.year,@last_date_trading.to_date.month,@last_date_trading.to_date.day)
    @uc.last_date_trading = @check_date 
    if @uc.end_time > @check_date
      @uc.end_time = @check_date
    end
    if @uc.start_time > @check_date
      @uc.start_time = @check_date
    end
    render :layout=> "interface2"
  end
#================================================================================================
# Function    : show_trade (action controller)
# Input        : start_time,
#                 end_time, 
#                 ts_x[0-4],
#                 ecn_x (ARCA,RASH,NDAQ,0)
#                 cecn_x(ARCA,RASH,NDAQ,0)
# Description  :
# Developer      : 
# Date      : 
#================================================================================================
  def show_trade
    #get user context
    @uc = get_uc

    #Filter Entry or Exit
    entry_exit_filter = ""
    #page
    @page = params[:page] && params[:page].to_i > 0 ? params[:page].to_i : 1
    @items_per_page = 100

    @offset = (@page - 1) * @items_per_page

    #sort order
    sort = "desc"
    @dir1 = params[:dir1] || "asc"
    @dir2 = params[:dir2] || "desc"
    @dir3 = params[:dir3] || "desc"
    @dir4 = params[:dir4] || "asc"
    @dir5 = params[:dir5] || "desc"
    @dir6 = params[:dir6] || "desc"
    @dir7 = params[:dir7] || "desc"
    @dir8 = params[:dir8] || "desc"
    @dir9 = params[:dir9] || "desc"
    @dir10 = params[:dir10] || "desc"
    @dir11 = params[:dir11] || "desc"
    @dir12 = params[:dir12] || "desc"
    @dir13 = params[:dir13] || "desc"
    #sort field
    @order_field = params[:order_field] || "net_pl"

    if params[:exclusive].to_s == "true" then
      @exclusive = 1
    else
      @exclusive = 0
    end

    #tonggle sort order
    if @order_field ==  "symbol"
      if @dir1 == "asc"
        sort = "asc"
        @dir1 = "desc"
      else
        sort = "desc"
        @dir1 = "asc"
      end
    elsif @order_field ==  "expected_shares"
      if @dir2 == "asc"
        sort = "asc"
        @dir2 = "desc"
      else
        sort = "desc"
        @dir2 = "asc"
      end
    elsif @order_field ==  "expected_pl"
      if @dir3 == "asc"
        sort = "asc"
        @dir3 = "desc"
      else
        sort = "desc"
        @dir3 = "asc"
      end
    elsif @order_field ==  "net_pl"
      #when click on column label ==> sort by column name
      if params[:has_sorted_column] && params[:has_sorted_column].to_i == 1
        if @dir4 == "asc"
          sort = "asc"
          @dir4 = "desc"
        else
          sort = "desc"
          @dir4 = "asc"
        end
      else
      # Not sort by column name
        sort = "desc"
        if params[:status] && ( params[:status].to_i == 2 || params[:status].to_i == 4 )
          sort = "asc"
        end
        if params[:status] && ( params[:status].to_i == 0 || params[:status].to_i == 1 )
          @order_field =  "expected_pl"
        end
      end
    elsif @order_field ==  "bought_shares"
      if @dir5 == "asc"
        sort = "asc"
        @dir5 = "desc"
      else
        sort = "desc"
        @dir5 = "asc"
      end
    elsif @order_field ==  "sold_shares"
      if @dir6 == "asc"
        sort = "asc"
        @dir6 = "desc"
      else
        sort = "desc"
        @dir6 = "asc"
      end
    elsif @order_field ==  "as_cid"
      if @dir7 == "asc"
        sort = "asc"
        @dir7 = "desc"
      else
        sort = "desc"
        @dir7 = "asc"
      end
    elsif @order_field ==  "total_sold"
      if @dir8 == "asc"
        sort = "asc"
        @dir8 = "desc"
      else
        sort = "desc"
        @dir8 = "asc"
      end
    elsif @order_field ==  "total_bought"
      if @dir9 == "asc"
        sort = "asc"
        @dir9 = "desc"
      else
        sort = "desc"
        @dir9 = "asc"
      end
    elsif @order_field ==  "time_stamp"
      if @dir10 == "asc"
        sort = "asc"
        @dir10 = "desc"
      else
        sort = "desc"
        @dir10 = "asc"
      end
    elsif @order_field ==  "pm_net_pl"
      if @dir11 == "asc"
        sort = "asc"
        @dir11 = "desc"
      else
        sort = "desc"
        @dir11 = "asc"
      end
    elsif @order_field ==  "date"
      if @dir12 == "asc"
        sort = "asc"
        @dir12 = "desc"
      else
        sort = "desc"
        @dir12 = "asc"
      end
    elsif @order_field ==  "launch_shares"
      if @dir13 == "asc"
        sort = "asc"
        @dir13 = "desc"
      else
        sort = "desc"
        @dir13 = "asc"
      end
    end

    #take trade date as format mm/dd/yyyy
    if params[:start_time]
      @uc.start_time = str_to_unix_time(params[:start_time])
    end
    if params[:end_time]
      @uc.end_time = str_to_unix_time(params[:end_time])
    end
    #filter by time ( start_time, end_time)
    @uc.trade_start_time =  params[:trade_start_time] if params[:trade_start_time]
    @uc.trade_end_time =  params[:trade_end_time] if params[:trade_end_time]
    start_time = @uc.start_time
    #Check start_time, end_time more than Time now
    check_datetime

    #Connect to database
    db_name_suffix = "#{@uc.start_time.year}#{@uc.start_time.month.to_s.rjust(2,'0')}"
    connect_to_database(db_name_suffix)

    condition_no =  ""
    conditions = ["date between ? and ? "]
    values = [@uc.start_time, @uc.end_time]
    condition_no << " (date between '#{@uc.start_time}' and '#{@uc.end_time}')"
    conditions << "(time_stamp between ? and ?)"
    values << @uc.trade_start_time
    values << @uc.trade_end_time
    condition_no << " and (time_stamp between '#{@uc.trade_start_time}' and '#{@uc.trade_end_time}') "

    #filter  by ts_id
    @uc.ts = params[:ts_id]  || "0"
    #check number TS
    check_ts = 0
    if @uc.ts.to_s.include? ","
      check_ts = 1
    end
    if check_ts == 0
      if @uc.ts.to_i > 0
        conditions << "ts_id =?"
        values << @uc.ts.to_i - 1
      elsif @uc.ts.to_i == -1
        conditions << " bought_shares <> sold_shares"
      end
    else
      if @uc.ts.to_s.include? "-1"
        conditions << "((ts_id + 1 in (#{@uc.ts})) or (bought_shares <> sold_shares))"
      else
        conditions << "ts_id + 1 in (#{@uc.ts}) "
      end
    end

    #filter by ecn launch
    if params[:cecn]
      @uc.cecn = params[:cecn]
    else
      @uc.cecn = "0"
    end
    if @uc.cecn && @uc.cecn != "0"
      conditions << "ecn_launch =?"
      values << @uc.cecn.to_s
      condition_no << " and ecn_launch = '#{@uc.cecn.to_s}'"
    end
    #filter by shares size
    share_size_conditions = ""
    @uc.share_size = params[:share_size] || "0"
    if @uc.share_size.to_s.include? "," # Filter by multi Detected Shares
      share_size_conditions = TradeResult.build_conditions_for_filter_by_shares(params[:share_size], 'expected_shares')
    else 
      if params[:share_size]
        if (params[:share_size].to_i != -100) # Add filter < 100
          @uc.share_size = params[:share_size].to_i
          conditions << "expected_shares >= ?"
          values << @uc.share_size.to_i 
          condition_no << " and expected_shares >= #{@uc.share_size.to_i}"  
        else
          @uc.share_size = params[:share_size].to_i
          conditions << "expected_shares < ?"
          values << @uc.share_size.to_i * -1
          condition_no << " and expected_shares < 100"  
        end
      end
    end
    
    #filter by launch shares
    launch_shares_conditions = ""
    @uc.launch_shares = params[:launch_shares] || "0"
    if @uc.launch_shares.to_s.include? "," # Filter by multi launch shares
       launch_shares_conditions = TradeResult.build_conditions_for_filter_by_shares(params[:launch_shares], 'launch_shares')
    else 
      if params[:launch_shares]
        if (params[:launch_shares].to_i != -100) # Add filter < 100
          @uc.launch_shares = params[:launch_shares].to_i
          conditions << "launch_shares >= ?"
          values << @uc.launch_shares.to_i 
          condition_no << " and launch_shares >= #{@uc.launch_shares.to_i}"  
        else
          @uc.launch_shares = params[:launch_shares].to_i
          conditions << "launch_shares < ?"
          values << @uc.launch_shares.to_i * -1
          condition_no << " and launch_shares < 100"  
        end
      end
    end
    #filter by expected profit
    if params[:expected_profit]
      @uc.expected_profit = params[:expected_profit].to_i
      if params[:expected_profit] != "0"
        conditions << "expected_pl >= ?"
        values << @uc.expected_profit.to_f
        condition_no << " and expected_pl >= #{@uc.expected_profit.to_f}"
      end
    end
    
    symbol = ""
    #filter like symbol
    if params[:symbol] 
      @uc.symbol = params[:symbol]
      symbol = @uc.symbol.upcase
      
      if params[:symbol].strip.to_s != ""     
        conditions << "symbol = ?"
        values << @uc.symbol.upcase
        condition_no << " and symbol = '#{@uc.symbol.upcase}'"
      end
    end

    # filter by Bought Shares and Sold Shares
    @uc.bought_shares = ""
    @uc.sold_shares = ""
    if params[:bought_shares] && params[:bought_shares].strip.to_i > 0
      @uc.bought_shares = params[:bought_shares].strip.to_i
      conditions << "bought_shares >= ?"
      values << @uc.bought_shares     
    end

    #filter to sold shares
    if params[:sold_shares] && params[:sold_shares].strip.to_i > 0
      @uc.sold_shares = params[:sold_shares].strip.to_i
      conditions << "sold_shares >= ?"
      values << @uc.sold_shares
    end
    #Filter by status   
    if params[:status] && params[:status].to_i >= 0
      @uc.trade_status = params[:status].to_i
    end

    ecn_filter_conditions = ""
    ecn_filter_munlti_conditions = ""
    @uc.ecn_filter = params[:ecn_filter] || "0"
    if @uc.ecn_filter.to_s.include? "," # Filter by multi ECN Entry/Exit
      ecn_filter_munlti_conditions = TradeResult.build_conditions_for_ecn_entry_exit(@uc.start_time, @uc.end_time, params[:ecn_filter])
      if ecn_filter_munlti_conditions.size > 0
        ecn_filter_conditions = " and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) in (#{ecn_filter_munlti_conditions}) "
      end
    else 
      #filter by ECN Entry/Exit
      if params[:ecn_filter] && params[:ecn_filter].to_s != "0"
        @uc.ecn_filter = params[:ecn_filter].strip.to_s
        if @uc.ecn_filter.size > 2
          ecn_name = @uc.ecn_filter[0, @uc.ecn_filter.size - 2].upcase            
          if @uc.ecn_filter[-2, 2] == "en"
            entry_exit_filter = "Entry"
          else
            entry_exit_filter = "Exit"
          end
          ecn_filter_conditions << " and ecn = '#{ecn_name}' and entry_exit = '#{entry_exit_filter}'"
        end
        #comment this code to add multi exchange trades in filter. Before it is filtered without Multi exchange
        
        if @exclusive == 1 then
          cond_multi_order = get_query_string_for_multi_order(@uc.start_time, @uc.end_time, entry_exit_filter)
          conditions << cond_multi_order
        end
      else
        @uc.ecn_filter = "0"
      end
    end
    
    #Filter by ISO Orders
    @uc.iso_order = "0"
    if params[:iso_order] && params[:iso_order].to_s == "1"
      #check trade_date must be more than 01/01/2010
      if @uc.trade_date > str_to_unix_time("01/01/2010")
        @uc.iso_order = "1"
        conditions << "is_iso is not NULL and is_iso ='Y'"
      end
    end   
    condition_no << " and is_iso = 'Y' and is_iso is not NULL"
    if params[:iso_order] && params[:iso_order].to_s == "2"
      #check trade_date must be more than 01/01/2010
      if @uc.trade_date > str_to_unix_time("01/01/2010")
        @uc.iso_order = "2"         
        conditions << "is_iso <> 'Y'"
      end     
    end

    #sort by status default
    if params[:sort_default].to_i == 1
      if params[:status].to_i == 0
        #Missed
        @order_field =  "expected_pl"
        sort = "desc"
      elsif  params[:status].to_i == 1
        #Ouch reprice
        @order_field =  "expected_pl"
        sort = "desc"
      elsif params[:status].to_i == 2
        #Stuck Loss
        @order_field =  "net_pl"
        sort = "asc"
      elsif params[:status].to_i == 3
        #Stuck Profit
        @order_field =  "net_pl"
        sort = "desc"
      elsif params[:status].to_i == 4
        #loss
        @order_field =  "net_pl"
        sort = "asc"
      elsif params[:status].to_i == 5
        #profit under
        @order_field =  "net_pl"
        sort = "desc"
      elsif params[:status].to_i == 6
        #profit above
        @order_field =  "net_pl"
        sort = "desc"
      elsif params[:status].to_i == 7
        #total
        @order_field =  "net_pl"
        sort = "desc"
      end
    end

    #Filter by Cross Size
    @uc.cross_size = "0"
    @uc.cross_size = params[:cross_size] if params[:cross_size]
    conditions_cross_size = TradeResult.build_conditions_for_cross_size(@uc.cross_size)
    if conditions_cross_size != ""
      conditions << conditions_cross_size
    end
    
    filter_status = @uc.trade_status
    
    filter_conditions = ""
    if filter_status
      #Miss
      if filter_status == 0
       if ((@uc.ecn_filter.first(4) == "ouch" || @uc.ecn_filter.first(4) == "oubx" || @uc.ecn_filter.first(3) == "psx" || @uc.ecn_filter == "0") && (@uc.cecn.to_s == "NDAQ" || @uc.cecn.to_s == "NDBX" || @uc.cecn.to_s == "PSX" || @uc.cecn == "0"))
          tmp_str = "(EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) not in (select (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) from vwtrade_results_twt2 where date between '#{@uc.start_time}' and '#{@uc.end_time}' and (ecn = 'OUCH' or ecn = 'OUBX' or ecn = 'PSX') and price <> ack_price and sold_shares < 1 and bought_shares < 1)"
          filter_conditions = " and sold_shares < 1 and bought_shares < 1 and #{tmp_str}"
        else
        
          filter_conditions = " and sold_shares < 1 and bought_shares < 1 "
        end
      #Repriced
      elsif filter_status == 1
        filter_conditions = " and ((ecn = 'OUCH' or ecn = 'OUBX' or ecn = 'PSX') and price <> ack_price and sold_shares < 1 and bought_shares < 1) "
      #stuck_loss
      elsif filter_status == 2
        filter_conditions = "  and (bought_shares <> sold_shares AND pm_net_pl < -0.0001)"        
      #stuck_profit
      elsif filter_status == 3
        filter_conditions = "  and (bought_shares <> sold_shares AND pm_net_pl >= -0.0001)"       
      #loss
      elsif filter_status == 4
        filter_conditions = "  and (bought_shares = sold_shares  and net_pl < 0)"       
      #profit under
      elsif filter_status == 5
        filter_conditions = " and (bought_shares >0 and bought_shares = sold_shares  and net_pl >= 0 and net_pl::real < expected_pl::real) "        
      #profit above
      elsif filter_status == 6
        filter_conditions = " and (bought_shares >0 and bought_shares = sold_shares  and net_pl >= 0 and net_pl::real >= expected_pl::real )"
        
      #All
      elsif filter_status == 7
         if ((@uc.ecn_filter.first(4) == "ouch" || @uc.ecn_filter.first(4) == "oubx" || @uc.ecn_filter.first(3) == "psx" || @uc.ecn_filter == "0") && (@uc.cecn.to_s == "NDAQ" || @uc.cecn.to_s == "NDBX" || @uc.cecn.to_s == "PSX" || @uc.cecn == "0"))
          tmp_str = "(EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) not in (select (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) from vwtrade_results_twt2 where date between '#{@uc.start_time}' and '#{@uc.end_time}' and (ecn = 'OUCH' or ecn = 'OUBX' or ecn = 'PSX') and price <> ack_price and sold_shares < 1 and bought_shares < 1)"
          filter_conditions = " and ts_id <> -1 and #{tmp_str}"
        else
          filter_conditions = " and ts_id <> -1 "
        end
        
      end
    end
    
    if filter_status != 8
      list_field_string = "distinct date,min(time_stamp) as time_stamp,ts_cid, ts_id,expected_shares, expected_pl, bought_shares, total_bought, sold_shares, total_sold, left_stuck_shares,  as_cid,trade_id, symbol, net_pl, pm_net_pl, launch_shares "
      group_by = "date,ts_cid, ts_id,expected_shares, expected_pl, bought_shares, total_bought, sold_shares, total_sold, left_stuck_shares,  as_cid,trade_id, symbol, net_pl, pm_net_pl, launch_shares "

      @count1 = TradeResult2.find :all,
                      :select=> list_field_string,
                      :conditions => [conditions.join(' and ')  + filter_conditions + ecn_filter_conditions + share_size_conditions + launch_shares_conditions] + values,
                      :group => group_by
              
      @count = @count1.size
      @offset = (@count - 1) / @items_per_page * @items_per_page if @page * @items_per_page > @count
      if @offset < 0
        @offset = 0
      end
      
      @trades = TradeResult2.find :all,
                      :select=> list_field_string,
                      :conditions => [conditions.join(' and ') + filter_conditions + ecn_filter_conditions + share_size_conditions + launch_shares_conditions] + values,
                      :group => group_by,
                      :order => "#{@order_field} #{sort}",                    
                      :limit => @items_per_page,
                      :offset => @offset  
          
      @object_pages = WillPaginate::Collection.new(@page, @items_per_page, @count)    
    else
      @filter_status = filter_status
      @arr_netb = []
      result = []
      if ((check_ts == 0 && @uc.ts.to_i != -1) || check_ts != 0)  && @uc.ecn_filter == "0" && @uc.iso_order == '0' && @uc.bought_shares == "" && @uc.sold_shares == ""
        if @uc.start_time == @uc.end_time
          @arr_netb = TradeResult.calculate_netb(@uc.start_time,@uc.end_time,@uc.trade_start_time,@uc.trade_end_time, @uc.expected_profit, symbol,  @uc.ts, @uc.share_size, @uc.cross_size, @uc.cecn, @uc.launch_shares)
        else
          result = TradeResult.calculate_netb_date_range(@uc.start_time,@uc.end_time,@uc.trade_start_time,@uc.trade_end_time, @uc.expected_profit, symbol, @uc.ts, @uc.share_size, @uc.cross_size, @uc.cecn, @uc.launch_shares)
          @arr_netb = result[2]
        end
        
        # sort this table
        if @order_field == "symbol"
          if @dir1 == "asc"
            @arr_netb = @arr_netb.sort { |x,y| x[1] <=> y[1] }
          else
            @arr_netb = @arr_netb.sort { |x,y| y[1] <=> x[1] }
          end
        elsif @order_field == "expected_shares"
          if @dir2 == "asc"
            @arr_netb = @arr_netb.sort { |x,y| x[2] <=> y[2] }
          else
            @arr_netb = @arr_netb.sort { |x,y| y[2] <=> x[2] }
          end
        elsif @order_field == "expected_pl"
          if @dir3 == "asc"
            @arr_netb = @arr_netb.sort { |x,y| x[3] <=> y[3] }
          else
            @arr_netb = @arr_netb.sort { |x,y| y[3] <=> x[3] }
          end
        elsif @order_field == "time_stamp"
          if @dir10 == "asc"
            @arr_netb = @arr_netb.sort { |x,y| x[9] <=> y[9] }
          else
            @arr_netb = @arr_netb.sort { |x,y| y[9] <=> x[9] }
          end
        elsif @order_field == "as_cid"
          if @dir7 == "asc"
            @arr_netb = @arr_netb.sort { |x,y| x[0] <=> y[0] }
          else
            @arr_netb = @arr_netb.sort { |x,y| y[0] <=> x[0] }
          end
        elsif @order_field ==  "date"
          if @dir12 == "asc"
            @arr_netb = @arr_netb.sort { |x,y| x[7] <=> y[7] }
          else
            @arr_netb = @arr_netb.sort { |x,y| y[7] <=> x[7] }
          end
        elsif @order_field == "launch_shares"
          if @dir13 == "asc"
            @arr_netb = @arr_netb.sort { |x,y| x[10] <=> y[10] }
          else
            @arr_netb = @arr_netb.sort { |x,y| y[10] <=> x[10] }
          end
        end
      end
      
      #end of sort
      @count = @arr_netb.size
      @offset = (@count - 1) / @items_per_page * @items_per_page if @page * @items_per_page > @count
      @object_pages = WillPaginate::Collection.new(@page, @items_per_page, @count)
    end
  end

  #================================================================================================
  # Function    : show_trade_multiple_months (action controller)
  # Input       : start_time,
  #                 end_time, 
  #                 ts_x[0-4],
  #                 ecn_x (ARCA,RASH,NDAQ,0)
  #                 cecn_x(ARCA,RASH,NDAQ,0)
  # Description :
  # Developer     : 
  # Date      : 
  #================================================================================================
  def show_trade_multiple_months
    #get user context
    @uc = get_uc
    
    #Filter Entry or Exit
    entry_exit_filter = ""
    
    #page
    @page = params[:page] && params[:page].to_i > 0 ? params[:page].to_i : 1
    @items_per_page = 100
    @offset = (@page - 1) * @items_per_page
  
    #sort order
    sort = "desc"
    @dir1 = params[:dir1] || "asc"
    @dir2 = params[:dir2] || "desc"
    @dir3 = params[:dir3] || "desc"
    @dir4 = params[:dir4] || "asc"
    @dir5 = params[:dir5] || "desc"
    @dir6 = params[:dir6] || "desc"
    @dir7 = params[:dir7] || "desc"
    @dir8 = params[:dir8] || "desc"
    @dir9 = params[:dir9] || "desc"
    @dir10 = params[:dir10] || "desc"
    @dir11 = params[:dir11] || "desc"
    @dir12 = params[:dir12] || "desc"
    @dir13 = params[:dir13] || "desc"
    #sort field
    @order_field = params[:order_field] || "net_pl"

    if params[:exclusive].to_s == "true" then
      @exclusive = 1
    else
      @exclusive = 0
    end
    
    #tonggle sort order
    if @order_field ==  "symbol"
      if @dir1 == "asc"
        sort = "asc"
        @dir1 = "desc"
      else
        sort = "desc"
        @dir1 = "asc"
      end
    elsif @order_field ==  "expected_shares"
      if @dir2 == "asc"
        sort = "asc"
        @dir2 = "desc"
      else
        sort = "desc"
        @dir2 = "asc"
      end
    elsif @order_field ==  "expected_pl"
      if @dir3 == "asc"
        sort = "asc"
        @dir3 = "desc"
      else
        sort = "desc"
        @dir3 = "asc"
      end
    elsif @order_field ==  "net_pl"
      #when click on column label ==> sort by column name
      if params[:has_sorted_column] && params[:has_sorted_column].to_i == 1
        if @dir4 == "asc"
          sort = "asc"
          @dir4 = "desc"        
        else
          sort = "desc"
          @dir4 = "asc"       
        end
      else
      # Not sort by column name
        sort = "desc"
        if params[:status] && ( params[:status].to_i == 2 || params[:status].to_i == 4 )
          sort = "asc"
        end
        if params[:status] && ( params[:status].to_i == 0 || params[:status].to_i == 1 )
          @order_field =  "expected_pl"         
        end
      end     
    elsif @order_field ==  "bought_shares"
      if @dir5 == "asc"
        sort = "asc"
        @dir5 = "desc"
      else
        sort = "desc"
        @dir5 = "asc"
      end
    elsif @order_field ==  "sold_shares"
      if @dir6 == "asc"
        sort = "asc"
        @dir6 = "desc"
      else
        sort = "desc"
        @dir6 = "asc"
      end
    elsif @order_field ==  "as_cid"
      if @dir7 == "asc"
        sort = "asc"
        @dir7 = "desc"
      else
        sort = "desc"
        @dir7 = "asc"
      end
    elsif @order_field ==  "total_sold"
      if @dir8 == "asc"
        sort = "asc"
        @dir8 = "desc"
      else
        sort = "desc"
        @dir8 = "asc"
      end
    elsif @order_field ==  "total_bought"
      if @dir9 == "asc"
        sort = "asc"
        @dir9 = "desc"
      else
        sort = "desc"
        @dir9 = "asc"
      end
    elsif @order_field ==  "time_stamp"
      if @dir10 == "asc"
        sort = "asc"
        @dir10 = "desc"
      else
        sort = "desc"
        @dir10 = "asc"
      end
    elsif @order_field ==  "pm_net_pl"
      if @dir11 == "asc"
        sort = "asc"
        @dir11 = "desc"
      else
        sort = "desc"
        @dir11 = "asc"
      end
    elsif @order_field ==  "date"
      if @dir12 == "asc"
        sort = "asc"
        @dir12 = "desc"
      else
        sort = "desc"
        @dir12 = "asc"
      end
    elsif @order_field ==  "launch_shares"
      if @dir13 == "asc"
        sort = "asc"
        @dir13 = "desc"
      else
        sort = "desc"
        @dir13 = "asc"
      end
    end

    #take trade date as format mm/dd/yyyy
    if params[:start_time]
      @uc.start_time = str_to_unix_time(params[:start_time])
    end 
    if params[:end_time]
      @uc.end_time = str_to_unix_time(params[:end_time])
    end 
    #filter by time ( start_time, end_time)
    @uc.trade_start_time =  params[:trade_start_time] if params[:trade_start_time]
    @uc.trade_end_time =  params[:trade_end_time] if params[:trade_end_time]
    start_time = @uc.start_time
    #Check start_time, end_time more than Time now
    check_datetime
    
    st = @uc.start_time.month
    en = @uc.end_time.month
    year = @uc.start_time.year      
    year_interval = 0
    if @uc.end_time.year > @uc.start_time.year
      year_interval = @uc.end_time.year - @uc.start_time.year
      en = en + 12 * year_interval
    end 
    st_time = @uc.start_time
    @count_months = 0 
    @trades = Array.new
    @trades_unsort = Array.new
    @arr_netb = []
    
    while st <= en 
        db_name_suffix = "#{Time.now.year}#{Time.now.month.to_s.rjust(2,'0')}"  
        db_name_suffix = "#{Time.now.year}#{Time.now.month.to_s.rjust(2,'0')}"  
      if st <= 12
        db_name_suffix = "#{year}#{st.to_s.rjust(2,'0')}"
      else        
        #p "year_interval  #{year_interval}"
        if year_interval == 1
          db_name_suffix = "#{year + 1}#{(st - 12).to_s.rjust(2,'0')}"
        elsif year_interval == 2
          if st <= 24
            db_name_suffix = "#{year + 1}#{(st - 12).to_s.rjust(2,'0')}"
          else
            db_name_suffix = "#{year + 2}#{(st - 24).to_s.rjust(2,'0')}"
          end
        end
      end

      # lets manually connect to the proper db
      connect_to_database(db_name_suffix)
      condition_no =  ""
      conditions = ["date between ? and ? "]
      values = [@uc.start_time, @uc.end_time]
      condition_no << " (date between '#{@uc.start_time}' and '#{@uc.end_time}')"
      conditions << "(time_stamp between ? and ?)"
      values << @uc.trade_start_time
      values << @uc.trade_end_time
      condition_no << " and (time_stamp between '#{@uc.trade_start_time}' and '#{@uc.trade_end_time}') "
      
      #filter  by ts_id
      @uc.ts = params[:ts_id]  || "0"
      #check number TS
      check_ts = 0
      if @uc.ts.to_s.include? ","
        check_ts = 1
      end
      if check_ts == 0
        if @uc.ts.to_i > 0
          conditions << "ts_id =?"
          values << @uc.ts.to_i - 1   
        elsif @uc.ts.to_i == -1
          conditions << " bought_shares <> sold_shares"
        end
      else
        if @uc.ts.to_s.include? "-1"
          conditions << "((ts_id + 1 in (#{@uc.ts})) or (bought_shares <> sold_shares))"
        else
          conditions << "ts_id + 1 in (#{@uc.ts}) "
        end
      end   
    
      #filter by ecn launch
      if params[:cecn]
        @uc.cecn = params[:cecn]
      else
        @uc.cecn = "0"
      end
      if @uc.cecn && @uc.cecn != "0"
        conditions << "ecn_launch =?"
        values << @uc.cecn.to_s
        condition_no << " and ecn_launch = '#{@uc.cecn.to_s}'"
      end
      
      #filter by shares size
      share_size_conditions = ""
      @uc.share_size = params[:share_size] || "0"
      if @uc.share_size.to_s.include? "," # Filter by multi Detected Shares
        share_size_conditions = TradeResult.build_conditions_for_filter_by_shares(params[:share_size], 'expected_shares')
      else 
        if params[:share_size]
          if (params[:share_size].to_i != -100) # Add filter < 100
            @uc.share_size = params[:share_size].to_i
            conditions << "expected_shares >= ?"
            values << @uc.share_size.to_i 
            condition_no << " and expected_shares >= #{@uc.share_size.to_i}"  
          else
            @uc.share_size = params[:share_size].to_i
            conditions << "expected_shares < ?"
            values << @uc.share_size.to_i * -1
            condition_no << " and expected_shares < 100"  
          end
        end
      end
      
      #filter by launch shares
      launch_shares_conditions = ""
      @uc.launch_shares = params[:launch_shares] || "0"
      if @uc.launch_shares.to_s.include? "," # Filter by multi launch shares
         launch_shares_conditions = TradeResult.build_conditions_for_filter_by_shares(params[:launch_shares], 'launch_shares')
      else 
        if params[:launch_shares]
          if (params[:launch_shares].to_i != -100) # Add filter < 100
            @uc.launch_shares = params[:launch_shares].to_i
            conditions << "launch_shares >= ?"
            values << @uc.launch_shares.to_i 
            condition_no << " and launch_shares >= #{@uc.launch_shares.to_i}"  
          else
            @uc.launch_shares = params[:launch_shares].to_i
            conditions << "launch_shares < ?"
            values << @uc.launch_shares.to_i * -1
            condition_no << " and launch_shares < 100"  
          end
        end
      end
      
      #filter by expected profit
      if params[:expected_profit]
        @uc.expected_profit = params[:expected_profit].to_i
        if params[:expected_profit] != "0"
          conditions << "expected_pl >= ?"
          values << @uc.expected_profit.to_f
          condition_no << " and expected_pl >= #{@uc.expected_profit.to_f}"
        end
      end
      
      symbol = ""
      #filter like symbol
      if params[:symbol] 
        @uc.symbol = params[:symbol]
        symbol = @uc.symbol.upcase
        
        if params[:symbol].strip.to_s != ""
          conditions << "symbol = ?"
          values << @uc.symbol.upcase
          condition_no << " and symbol = '#{@uc.symbol.upcase}'"
        end
      end

      # filter by Bought Shares and Sold Shares
      @uc.bought_shares = ""
      @uc.sold_shares = ""
      if params[:bought_shares] && params[:bought_shares].strip.to_i > 0
        @uc.bought_shares = params[:bought_shares].strip.to_i
        conditions << "bought_shares >= ?"
        values << @uc.bought_shares   
      end

      #filter to sold shares
      if params[:sold_shares] && params[:sold_shares].strip.to_i > 0
        @uc.sold_shares = params[:sold_shares].strip.to_i
        conditions << "sold_shares >= ?"
        values << @uc.sold_shares
      end
      #Filter by status   
      if params[:status] && params[:status].to_i >= 0
        @uc.trade_status = params[:status].to_i
      end

       # Check multi select ECN Entry/Exit
      check_ecn = 0
      ecn_filter_conditions = ""
      ecn_filter_munlti_conditions = ""
      @uc.ecn_filter = params[:ecn_filter] || "0"
      if @uc.ecn_filter.to_s.include? "," # Filter by multi ECN Entry/Exit
        check_ecn = 1
        ecn_filter_munlti_conditions = TradeResult.build_conditions_for_ecn_entry_exit(@uc.start_time, @uc.end_time, params[:ecn_filter])
        if ecn_filter_munlti_conditions.size > 0
          ecn_filter_conditions = " and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) in (#{ecn_filter_munlti_conditions}) "
        end
      else 
        #filter by ECN Entry/Exit
        if params[:ecn_filter] && params[:ecn_filter].to_s != "0"
          @uc.ecn_filter = params[:ecn_filter].strip.to_s
          if @uc.ecn_filter.size > 2
            ecn_name = @uc.ecn_filter[0, @uc.ecn_filter.size - 2].upcase            
            if @uc.ecn_filter[-2, 2] == "en"
              entry_exit_filter = "Entry"
            else
              entry_exit_filter = "Exit"
            end
            ecn_filter_conditions << " and ecn = '#{ecn_name}' and entry_exit = '#{entry_exit_filter}'"
          end
          #comment this code to add multi exchange trades in filter. Before it is filtered without Multi exchange
          
          if @exclusive == 1 then
            cond_multi_order = get_query_string_for_multi_order(@uc.start_time, @uc.end_time, entry_exit_filter)
            conditions << cond_multi_order
          end
        else
          @uc.ecn_filter = "0"
        end
      end
      
      
      #Filter by ISO Orders
      @uc.iso_order = "0"
      if params[:iso_order] && params[:iso_order].to_s == "1"

        #check trade_date must be more than 01/01/2010
        if @uc.trade_date > str_to_unix_time("01/01/2010")
          @uc.iso_order = "1"
          conditions << "is_iso is not NULL and is_iso ='Y'"
        end
        
      end   
      condition_no << " and is_iso = 'Y' and is_iso is not NULL"
      if params[:iso_order] && params[:iso_order].to_s == "2"

        #check trade_date must be more than 01/01/2010
        if @uc.trade_date > str_to_unix_time("01/01/2010")
          @uc.iso_order = "2"       
          #str_where = " (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) from new_orders where #{condition_no})"
          conditions << "is_iso <> 'Y'"
        end     
      end
      

      #sort by status default
      if params[:sort_default].to_i == 1
        if params[:status].to_i == 0
          #Missed
          @order_field =  "expected_pl"
          sort = "desc"
        elsif  params[:status].to_i == 1
          #Ouch reprice
          @order_field =  "expected_pl"
          sort = "desc"
        elsif params[:status].to_i == 2
          #Stuck Loss
          @order_field =  "net_pl"
          sort = "asc"
        elsif params[:status].to_i == 3
          #Stuck Profit
          @order_field =  "net_pl"
          sort = "desc"
        elsif params[:status].to_i == 4
          #loss
          @order_field =  "net_pl"
          sort = "asc"
        elsif params[:status].to_i == 5
          #profit under
          @order_field =  "net_pl"
          sort = "desc"
        elsif params[:status].to_i == 6
          #profit above
          @order_field =  "net_pl"
          sort = "desc"
        elsif params[:status].to_i == 7
          #total
          @order_field =  "net_pl"
          sort = "desc"
        end
      end
      
      #Filter by Cross Size
      @uc.cross_size = "0"
      @uc.cross_size = params[:cross_size] if params[:cross_size]
      conditions_cross_size = TradeResult.build_conditions_for_cross_size(@uc.cross_size)
      if conditions_cross_size != ""
        conditions << conditions_cross_size
      end
      
      filter_status = @uc.trade_status
      
      filter_conditions = ""
      if filter_status
        #Miss
        if filter_status == 0
         if ((@uc.ecn_filter.first(4) == "ouch" || @uc.ecn_filter.first(4) == "oubx" || @uc.ecn_filter.first(3) == "psx" || @uc.ecn_filter == "0") && (@uc.cecn.to_s == "NDAQ" || @uc.cecn.to_s == "NDBX" || @uc.cecn.to_s == "PSX" || @uc.cecn == "0"))
            tmp_str = "(EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) not in (select (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) from vwtrade_results_twt2 where date between '#{@uc.start_time}' and '#{@uc.end_time}' and (ecn = 'OUCH' or ecn = 'OUBX' or ecn = 'PSX') and price <> ack_price and sold_shares < 1 and bought_shares < 1)"
            filter_conditions = " and sold_shares < 1 and bought_shares < 1 and #{tmp_str}"
          else
          
            filter_conditions = " and sold_shares < 1 and bought_shares < 1 "
          end
        #Repriced
        elsif filter_status == 1
          filter_conditions = " and ((ecn = 'OUCH' or ecn = 'OUBX' or ecn = 'PSX') and price <> ack_price and sold_shares < 1 and bought_shares < 1) "
        #stuck_loss
        elsif filter_status == 2
          filter_conditions = "  and (bought_shares <> sold_shares AND pm_net_pl < -0.0001)"        
        #stuck_profit
        elsif filter_status == 3
          filter_conditions = "  and (bought_shares <> sold_shares AND pm_net_pl >= -0.0001)"       
        #loss
        elsif filter_status == 4
          filter_conditions = "  and (bought_shares = sold_shares  and net_pl < 0)"       
        #profit under
        elsif filter_status == 5
          filter_conditions = " and (bought_shares >0 and bought_shares = sold_shares  and net_pl >= 0 and net_pl::real < expected_pl::real) "        
        #profit above
        elsif filter_status == 6
          filter_conditions = " and (bought_shares >0 and bought_shares = sold_shares  and net_pl >= 0 and net_pl::real >= expected_pl::real )"
          
        #All
        elsif filter_status == 7
           if ((@uc.ecn_filter.first(4) == "ouch" || @uc.ecn_filter.first(4) == "oubx" || @uc.ecn_filter.first(3) == "psx" || @uc.ecn_filter == "0") && (@uc.cecn.to_s == "NDAQ" || @uc.cecn.to_s == "NDBX" || @uc.cecn.to_s == "PSX" || @uc.cecn == "0"))
          tmp_str = "(EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) not in (select (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) from vwtrade_results_twt2 where date between '#{@uc.start_time}' and '#{@uc.end_time}' and (ecn = 'OUCH' or ecn = 'OUBX' or ecn = 'PSX') and price <> ack_price and sold_shares < 1 and bought_shares < 1)"
          filter_conditions = " and ts_id <> -1 and #{tmp_str}"
          else
          
            filter_conditions = " and ts_id <> -1 "
          end
        end
      end   
      
      if filter_status != 8
        list_field_string = "distinct date,min(time_stamp) as time_stamp,ts_cid, ts_id,expected_shares, expected_pl, bought_shares, total_bought, sold_shares, total_sold, left_stuck_shares,  as_cid,trade_id, symbol, net_pl, pm_net_pl, launch_shares"
        group_by = "date,ts_cid, ts_id,expected_shares, expected_pl, bought_shares, total_bought, sold_shares, total_sold, left_stuck_shares,  as_cid,trade_id, symbol, net_pl, pm_net_pl, launch_shares "

        @count1 = TradeResult2.find :all,
                    :select=> list_field_string,
                    :conditions => [conditions.join(' and ')  + filter_conditions + ecn_filter_conditions + share_size_conditions + launch_shares_conditions] + values,
                    :group => group_by
            
        @count = @count1.size
        @offset = (@page - 1) * @items_per_page
        @offset = (@count - 1) / @items_per_page * @items_per_page if @page * @items_per_page > @count
        if @offset < 0
          @offset = 0
        end

        @trades_month = TradeResult2.find :all,
                    :select=> list_field_string,
                    :conditions => [conditions.join(' and ') + filter_conditions + ecn_filter_conditions + share_size_conditions + launch_shares_conditions] + values,
                    :group => group_by,
                    :order => "#{@order_field} #{sort}",
                    :limit => @items_per_page +  @offset,
                    :offset => 0
        @trades_month.each do |item|
            date = item.date.strftime("%m/%d/%Y")
            time_stamp = item.time_stamp.strftime("%H:%M:%S") 
            micro_time_stamp = "%06d" % (item.time_stamp.usec.to_i)
            sort_time_stamp = item.time_stamp
            value = [item.ts_cid, item.ts_id, item.as_cid, item.symbol, item.expected_shares, item.expected_pl, item.net_pl, item.pm_net_pl,item.bought_shares,item.sold_shares,item.total_sold,item.total_bought,date, time_stamp, micro_time_stamp, sort_time_stamp, item.launch_shares]

          @trades_unsort << value 
        end
        @count_months = @count_months + @count  
      end 
            
      st = st + 1 
    end #end while
    
    if filter_status != 8
      # Sort trades_unsort
      if @order_field ==  "symbol"
        if @dir1 == "desc"
            @trades_unsort = @trades_unsort.sort { |x,y| x[3] <=> y[3] }
        else
            @trades_unsort = @trades_unsort.sort { |x,y| y[3] <=> x[3] }
        end
      elsif @order_field ==  "expected_shares"
        if @dir2 == "desc"
          @trades_unsort = @trades_unsort.sort { |x,y| x[4] <=> y[4] }
        else
            @trades_unsort = @trades_unsort.sort { |x,y| y[4] <=> x[4] }
        end
      elsif @order_field ==  "expected_pl"
        if @dir3 == "desc"
          @trades_unsort = @trades_unsort.sort { |x,y| x[5] <=> y[5] }
        else
            @trades_unsort = @trades_unsort.sort { |x,y| y[5] <=> x[5] }
        end
      elsif @order_field ==  "net_pl"
        #when click on column label ==> sort by column name
        if @dir4 == "asc"
          @trades_unsort = @trades_unsort.sort { |x,y| y[6] <=> x[6] }
        else
          @trades_unsort = @trades_unsort.sort { |x,y| x[6] <=> y[6] }      
        end 
        
      elsif @order_field ==  "bought_shares"
        if @dir5 == "desc"
          @trades_unsort = @trades_unsort.sort { |x,y| x[8] <=> y[8] }
        else
          @trades_unsort = @trades_unsort.sort { |x,y| y[8] <=> x[8] }
        end
      elsif @order_field ==  "sold_shares"
        if @dir6 == "desc"
          @trades_unsort = @trades_unsort.sort { |x,y| x[9] <=> y[9] }
        else
          @trades_unsort = @trades_unsort.sort { |x,y| y[9] <=> x[9] }
        end
      elsif @order_field ==  "as_cid"
        if @dir7 == "desc"
          @trades_unsort = @trades_unsort.sort { |x,y| x[2] <=> y[2] }
        else
          @trades_unsort = @trades_unsort.sort { |x,y| y[2] <=> x[2] }
        end
      elsif @order_field ==  "total_sold"
        if @dir8 == "desc"
          @trades_unsort = @trades_unsort.sort { |x,y| x[10] <=> y[10] }
        else
          @trades_unsort = @trades_unsort.sort { |x,y| y[10] <=> x[10] }
        end
      elsif @order_field ==  "total_bought"
        if @dir9 == "desc"
          @trades_unsort = @trades_unsort.sort { |x,y| x[11] <=> y[11] }
        else
          @trades_unsort = @trades_unsort.sort { |x,y| y[11] <=> x[11] }
        end
      elsif @order_field ==  "time_stamp"
        if @dir10 == "desc"
          @trades_unsort = @trades_unsort.sort { |x,y| x[15] <=> y[15] }
        else
          @trades_unsort = @trades_unsort.sort { |x,y| y[15] <=> x[15] }
        end
      elsif @order_field ==  "pm_net_pl"
        if @dir11 == "desc"
          @trades_unsort = @trades_unsort.sort { |x,y| x[7] <=> y[7] }
        else
          @trades_unsort = @trades_unsort.sort { |x,y| y[7] <=> x[7] }
        end
      elsif @order_field ==  "date"
        if @dir12 == "asc"
          @trades_unsort = @trades_unsort.sort { |x,y| x[12] <=> y[12] }
        else
          @trades_unsort = @trades_unsort.sort { |x,y| y[12] <=> x[12] }
        end
       elsif @order_field ==  "launch_shares"
        if @dir13 == "asc"
          @trades_unsort = @trades_unsort.sort { |x,y| x[16] <=> y[16] }
        else
          @trades_unsort = @trades_unsort.sort { |x,y| y[16] <=> x[16] }
        end
      end
      
      @offset = (@page - 1) * @items_per_page
      if @page * @items_per_page < @count_months
         @offset_max = @offset + 100 - 1
      else
         @offset_max = @offset + @count_months%100 -1
      end
      
      for item in @offset..@offset_max
        @trades << @trades_unsort[item] 
      end
      
      @object_pages = WillPaginate::Collection.new(@page, @items_per_page, @count_months)
    end
    
    if filter_status == 8
      @filter_status = filter_status
      if ((check_ts == 0 && @uc.ts.to_i != -1) || check_ts != 0)  && @uc.ecn_filter == "0" && @uc.iso_order == '0' && @uc.bought_shares == "" && @uc.sold_shares == ""
        
        @arr_netb = TradeResult.calculate_netb_date_range(@uc.start_time,@uc.end_time,@uc.trade_start_time,@uc.trade_end_time, @uc.expected_profit, symbol,  @uc.ts, @uc.share_size, @uc.cross_size, @uc.cecn, @uc.launch_shares)[2]
          
        if @order_field == "symbol"
          if @dir1 == "asc"
            @arr_netb = @arr_netb.sort { |x,y| x[1] <=> y[1] }
          else
            @arr_netb = @arr_netb.sort { |x,y| y[1] <=> x[1] }
          end
        elsif @order_field == "expected_shares"
          if @dir2 == "asc"
            @arr_netb = @arr_netb.sort { |x,y| x[2] <=> y[2] }
          else
            @arr_netb = @arr_netb.sort { |x,y| y[2] <=> x[2] }
          end
        elsif @order_field == "expected_pl"
          if @dir3 == "asc"
            @arr_netb = @arr_netb.sort { |x,y| x[3] <=> y[3] }
          else
            @arr_netb = @arr_netb.sort { |x,y| y[3] <=> x[3] }
          end
        elsif @order_field == "time_stamp"
          if @dir10 == "asc"
            @arr_netb = @arr_netb.sort { |x,y| x[9] <=> y[9] }
          else
            @arr_netb = @arr_netb.sort { |x,y| y[9] <=> x[9] }
          end
        elsif @order_field == "as_cid"
          if @dir7 == "asc"
            @arr_netb = @arr_netb.sort { |x,y| x[0] <=> y[0] }
          else
            @arr_netb = @arr_netb.sort { |x,y| y[0] <=> x[0] }
          end
        elsif @order_field ==  "date"
          if @dir12 == "asc"
            @arr_netb = @arr_netb.sort { |x,y| x[7] <=> y[7] }
          else
            @arr_netb = @arr_netb.sort { |x,y| y[7] <=> x[7] }
          end
        elsif @order_field == "launch_shares"
          if @dir13 == "asc"
            @arr_netb = @arr_netb.sort { |x,y| x[10] <=> y[10] }
          else
            @arr_netb = @arr_netb.sort { |x,y| y[10] <=> x[10] }
          end
        end
      end
      #end of sort
      @count_months = @arr_netb.size
      @offset = (@page - 1) * @items_per_page
      @offset = (@count_months - 1) / @items_per_page * @items_per_page if @page * @items_per_page > @count_months
      @object_pages = WillPaginate::Collection.new(@page, @items_per_page, @count_months)
    end
    
  end

#================================================================================================
# Function    : show_statistic (action controller)
# Input       : start_time,
#                 end_time, 
#                 ts_x[0-16],
#                 ecn_x (ARCA,RASH,NDAQ,0)
#                 cecn_x(ARCA,RASH,NDAQ,0)
# Description :
# Developer     : 
# Date      : 
#================================================================================================
  def show_statistic
    #get user context
    @uc = get_uc  
    #take trade date as format mm/dd/yyyy
    if params[:start_time]
      @uc.start_time = str_to_unix_time(params[:start_time])
    end
    if params[:end_time]
      @uc.end_time = str_to_unix_time(params[:end_time])
    end   
    #filter by time ( start_time, end_time)
    @uc.trade_start_time =  params[:trade_start_time] if params[:trade_start_time]
    @uc.trade_end_time =  params[:trade_end_time] if params[:trade_end_time]
    
    if params[:ts_id]
      @uc.ts = params[:ts_id]
    end
    
    #filter by ecn launch
    if params[:cecn]
      @uc.cecn = params[:cecn]
    
    else
      @uc.cecn = "0"
    end    
    if params[:share_size]
      @uc.share_size = params[:share_size].to_i 
    end
    
    #filter by shares size
    @uc.share_size = params[:share_size] || "0"
    share_size_conditions = @uc.share_size
     
    #filter by launch size
    @uc.launch_shares = params[:launch_shares] || "0"
    launch_shares_conditions = @uc.launch_shares
   
    if params[:expected_profit]
      @uc.expected_profit = params[:expected_profit].to_i     
    end
    
    @uc.ecn_filter = params[:ecn_filter] || "0"
    ecn_filter_conditions = @uc.ecn_filter
    if @uc.ecn_filter.to_s.include? "," # Filter by multi ECN Entry/Exit
      ecn_filter_munlti_conditions = TradeResult.build_conditions_for_ecn_entry_exit(@uc.start_time, @uc.end_time, params[:ecn_filter])
      if ecn_filter_munlti_conditions.size > 0
        ecn_filter_conditions = " and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) in (#{ecn_filter_munlti_conditions}) "
      end
    end 
    
    if params[:status] && params[:status].to_i >= 0
      @uc.trade_status = params[:status].to_i
    end
    
    # include exclusive
    if params[:exclusive].to_s == "true" then
      @exclusive = 1
    else
      @exclusive = 0
    end
    
    symbol = ""
    #Check filter Or Not Filter
    bought_shares = -1
    sold_shares = -1
    # filter like symbol
    if params[:symbol]
      @symb =  params[:symbol]
      @uc.symbol = params[:symbol]
      if  params[:symbol].strip.to_s !=""     
        symbol = params[:symbol].strip.to_s.upcase      
      end
    end

    #filter to bought_shares
    if params[:bought_shares]
      if params[:bought_shares].strip.size > 0
        bought_shares = params[:bought_shares].strip.to_i
        if bought_shares < 0
          bought_shares = 0
        end
      end 
    end
    
    #filter to sold shares
    if params[:sold_shares] 
      if params[:sold_shares].strip.size > 0
        sold_shares = params[:sold_shares].strip.to_i
        if sold_shares < 0
          sold_shares = 0
        end
      end
    end

    notime = false
    if @uc.start_time.to_date != @uc.end_time.to_date
      notime = true
    end

    #Filter by ISO Orders
    @iso_order = "0"
    if params[:iso_order] && params[:iso_order].to_s == "1"
      #check trade_date must be more than 01/01/2010
      if @uc.trade_date > str_to_unix_time("01/01/2010")
        @iso_order = "1"        
      end     
    end 
    if params[:iso_order] && params[:iso_order].to_s == "2"
      #check trade_date must be more than 01/01/2010
      if @uc.trade_date > str_to_unix_time("01/01/2010")
        @iso_order = "2"        
      end     
    end 
    
    #Filter by Cross Size
    @uc.cross_size = "0"
    @uc.cross_size = params[:cross_size] if params[:cross_size]
    if (@uc.start_time.to_date < @uc.end_time.to_date)        
      @trade_statistics = TradeResult.trade_statistics_with_date_range(@uc.start_time,@uc.end_time,@uc.trade_start_time,@uc.trade_end_time, share_size_conditions, @uc.expected_profit, symbol,bought_shares, sold_shares,ecn_filter_conditions, @uc.ts, @uc.cecn.to_s, notime,@iso_order,@exclusive, @uc.cross_size, launch_shares_conditions)       
    else
      @trade_statistics = TradeResult.filter_trade_statistic_twt2_2(@uc.start_time,@uc.end_time,@uc.trade_start_time,@uc.trade_end_time, share_size_conditions, @uc.expected_profit, symbol,bought_shares, sold_shares, ecn_filter_conditions, @uc.ts, @uc.cecn.to_s, notime,@iso_order,@exclusive, @uc.cross_size, launch_shares_conditions)      
    end
        
    # $trade_results = @trade_statistics
  
  end

  #================================================================================================
  # Function    : order_detail (action controller)
  # Input       : cross_id, ts_id, trade_date
  # Description :use ajax to get informations of orders of a trade
  # Developer     : 
  # Date      : 
  #================================================================================================
  def order_detail  
    cross_id = params[:cross_id]
    ts_id = params[:ts_id]
    pmchecked = params[:pmcheck]
    trade_date = str_to_unix_time(params[:trade_date])
    symbol = params[:symbol]
    order_ids = nil
    
    #Connect to database
    db_name_suffix = "#{trade_date.year}#{trade_date.month.to_s.rjust(2,'0')}"  
    connect_to_database(db_name_suffix)
  
    if pmchecked.to_i == 1
      order_ids = Order.find_by_sql(["select oid from new_orders where date = ? and ((cross_id = ? and ts_id = ? and symbol = ?) OR (cross_id = ? and ts_id = -1 and symbol = ? and left_shares < shares))",trade_date, cross_id.to_i, ts_id.to_i, symbol, cross_id.to_i, symbol])    
    else
      order_ids = Order.find_by_sql(["select oid from new_orders where date = ? and ((cross_id = ? and ts_id = ? and symbol = ?) OR (cross_id = ? and ts_id = -1 and symbol = ?))",trade_date, cross_id.to_i, ts_id.to_i, symbol, cross_id.to_i, symbol])
    end
  
    if order_ids != nil
        order_ids = order_ids.collect{|x| x.oid if x}
    end
  
    @order_detail = Order.find_by_sql(["Select * from traded_orders where trade_date = ? and id in (?) order by trade_time, seqid ",trade_date,order_ids])
    render :partial=>"order_detail"
    
  end

  #================================================================================================
  # Function    : trade_result_pie (action controller)
  # Input       : cross_id, ts_id, trade_date
  # Description :use ajax to get informations of orders of a trade
  # Developer     : 
  # Date      : 
  #================================================================================================
  def trade_result_pie
    @uc = get_uc
    @color_config = get_color_config
    @include_repriced = params[:include_repriced]
    #take trade date as format mm/dd/yyyy
    if params[:start_time]
      @uc.start_time = str_to_unix_time(params[:start_time])
    end
    if params[:end_time]
      @uc.end_time = str_to_unix_time(params[:end_time])
    end   
    #filter by time ( start_time, end_time)
    @uc.trade_start_time =  params[:trade_start_time] if params[:trade_start_time]
    @uc.trade_end_time =  params[:trade_end_time] if params[:trade_end_time]
    
    if params[:ts_id]
      @uc.ts = params[:ts_id]
    end
    
    #filter by ecn launch
    if params[:cecn]
      @uc.cecn = params[:cecn]
    else
      @uc.cecn = "0"
    end    
   
    #filter by shares size
    @uc.share_size = params[:share_size] || "0"
    share_size_conditions = @uc.share_size
    
    #filter by launch size
    @uc.launch_shares = params[:launch_shares] || "0"
    launch_shares_conditions = @uc.launch_shares

    if params[:expected_profit]
      @uc.expected_profit = params[:expected_profit].to_i     
    end
    
    @uc.ecn_filter = params[:ecn_filter] || "0"
    ecn_filter_conditions = @uc.ecn_filter
    if @uc.ecn_filter.to_s.include? "," # Filter by multi ECN Entry/Exit
      ecn_filter_munlti_conditions = TradeResult.build_conditions_for_ecn_entry_exit(@uc.start_time, @uc.end_time, params[:ecn_filter])
      if ecn_filter_munlti_conditions.size > 0
        ecn_filter_conditions = " and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) in (#{ecn_filter_munlti_conditions}) "
      end
    end 
    
    if params[:status] && params[:status].to_i >= 0
      @uc.trade_status = params[:status].to_i
    end
    
    # include exclusive
    if params[:exclusive].to_s == "true" then
      @exclusive = 1
    else
      @exclusive = 0
    end
    
    symbol = ""
    bought_shares = -1
    sold_shares = -1
    # filter like symbol
    if params[:symbol]
      @symb =  params[:symbol]
      @uc.symbol = params[:symbol]  
      if  params[:symbol].strip.to_s !=""     
        symbol = params[:symbol].strip.to_s.upcase      
      end
    end

    #filter to bought_shares
    if params[:bought_shares]
      if params[:bought_shares].strip.size > 0
        bought_shares = params[:bought_shares].strip.to_i
        if bought_shares < 0
          bought_shares = 0
        end
      end 
    end 
    
    #filter to sold shares
    if params[:sold_shares]
      if params[:sold_shares].strip.size > 0
        sold_shares = params[:sold_shares].strip.to_i
        if sold_shares < 0
          sold_shares = 0
        end
      end 
    end

    notime = false
    if @uc.start_time.to_date != @uc.end_time.to_date
      notime = true
    end

    #Filter by ISO Orders
    @iso_order = "0"
    if params[:iso_order] && params[:iso_order].to_s == "1"
      #check trade_date must be more than 01/01/2010
      if @uc.trade_date > str_to_unix_time("01/01/2010")
        @iso_order = "1"        
      end     
    end 
    if params[:iso_order] && params[:iso_order].to_s == "2"
      #check trade_date must be more than 01/01/2010
      if @uc.trade_date > str_to_unix_time("01/01/2010")
        @iso_order = "2"        
      end     
    end 

    #Filter by Cross Size
    @uc.cross_size = "0"
    @uc.cross_size = params[:cross_size] if params[:cross_size]
    if (@uc.start_time.to_date < @uc.end_time.to_date)        
      @trade_statistics_pie = TradeResult.trade_statistics_with_date_range(@uc.start_time,@uc.end_time,@uc.trade_start_time,@uc.trade_end_time, share_size_conditions, @uc.expected_profit, symbol,bought_shares, sold_shares,ecn_filter_conditions,  @uc.ts, @uc.cecn.to_s, notime,@iso_order,@exclusive, @uc.cross_size, launch_shares_conditions)   
    else
      @trade_statistics_pie = TradeResult.filter_trade_statistic_twt2_2(@uc.start_time,@uc.end_time,@uc.trade_start_time,@uc.trade_end_time, share_size_conditions, @uc.expected_profit, symbol,bought_shares, sold_shares,ecn_filter_conditions,  @uc.ts, @uc.cecn.to_s, notime,@iso_order,@exclusive, @uc.cross_size, launch_shares_conditions)
          
    end 
    
    # @trade_statistics_pie = $trade_results
  end

  #================================================================================================
  # Function    : trade_result_pie (action controller)
  # Input       : cross_id, ts_id, trade_date
  # Description :use ajax to get informations of orders of a trade
  # Developer     : 
  # Date      : 
  #================================================================================================
  def trade_result_by_time
    #get user context
    @uc = get_uc
    @color_config = get_color_config
    @include_repriced = params[:include_repriced]
    
    #take trade date as format mm/dd/yyyy
    if params[:trade_date]
      @uc.trade_date = str_to_unix_time(params[:trade_date])
    end

    if params[:start_time]
      @uc.start_time = str_to_unix_time(params[:start_time])
    end 
    if params[:end_time]
      @uc.end_time = str_to_unix_time(params[:end_time])
    end 
    
    #filter by time ( start_time, end_time)
    @uc.trade_start_time =  params[:trade_start_time] if params[:trade_start_time]
    @uc.trade_end_time =  params[:trade_end_time] if params[:trade_end_time]  
    @uc.step_type = params[:step_type]
    #change the step when query data more than a week (864000ms ~ 10 day)
    if (@uc.end_time.to_date - @uc.start_time.to_date ) >= 864000
      #92 is step of a week
      @uc.step_type = 92
    end
    
    if params[:ts_id]
      @uc.ts = params[:ts_id]
    end
    #filter by ecn launch
    if params[:cecn]
      @uc.cecn = params[:cecn]
    else
      @uc.cecn = "0"
    end    
    
    #filter by shares size
    @uc.share_size = params[:share_size] || "0"
    share_size_conditions = @uc.share_size
    if @uc.share_size.to_s.include? "," # Filter by multi Detected Shares
      share_size_conditions = TradeResult.build_conditions_for_filter_by_shares(params[:share_size], 'expected_shares')
    end 
    
    #filter by launch size
    @uc.launch_shares = params[:launch_shares] || "0"
    launch_shares_conditions = @uc.launch_shares
    if @uc.launch_shares.to_s.include? "," # Filter by multi launch shares
      launch_shares_conditions = TradeResult.build_conditions_for_filter_by_shares(params[:launch_shares], 'launch_shares')
    end 

    if params[:expected_profit]
      @uc.expected_profit = params[:expected_profit].to_i     
    end
   
    @uc.ecn_filter = params[:ecn_filter] || "0"
    ecn_filter_conditions = @uc.ecn_filter
    if @uc.ecn_filter.to_s.include? "," # Filter by multi ECN Entry/Exit
      ecn_filter_munlti_conditions = TradeResult.build_conditions_for_ecn_entry_exit(@uc.start_time, @uc.end_time, params[:ecn_filter])
      if ecn_filter_munlti_conditions.size > 0
        ecn_filter_conditions = " and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) in (#{ecn_filter_munlti_conditions}) "
      end
    end 

    if params[:status] && params[:status].to_i >= 0
      @uc.trade_status = params[:status].to_i
    end
    
    symbol = ""
    bought_shares = 0
    sold_shares = 0
    # filter like symbol
    if params[:symbol]
      @symb =  params[:symbol]
      @uc.symbol = params[:symbol]
      if params[:symbol].strip.to_s !=""      
        symbol = params[:symbol].strip.to_s.upcase      
      end
    end

    #filter to bought_shares
    if params[:bought_shares] && params[:bought_shares].strip.to_i > 0      
      bought_shares =  params[:bought_shares].strip.to_i      
    end
    #filter to sold shares
    if params[:sold_shares] && params[:sold_shares].strip.to_i > 0      
      sold_shares = params[:sold_shares].strip.to_i
    end

    notime = false
    if @uc.start_time.to_date != @uc.end_time.to_date
      notime = true
    end
    
    # include exclusive
    if params[:exclusive].to_s == "true" then
      @exclusive = 1
    else
      @exclusive = 0
    end
    
    #Filter by Cross Size
    @uc.cross_size = "0"
    @uc.cross_size = params[:cross_size] if params[:cross_size]
    
    #Filter by ISO Orders
    @iso_order = "0"
    if params[:iso_order] && params[:iso_order].to_s == "1"
      #check trade_date must be more than 01/01/2010
      if @uc.trade_date > str_to_unix_time("01/01/2010")
        @iso_order = "1"        
      end     
    end 
    if params[:iso_order] && params[:iso_order].to_s == "2"
      #check trade_date must be more than 01/01/2010
      if @uc.trade_date > str_to_unix_time("01/01/2010")
        @iso_order = "2"        
      end     
    end 

    @trade_statistics = TradeResult.filter_trade_statistic_twt2_chart(@uc.start_time,@uc.end_time,@uc.trade_start_time,@uc.trade_end_time, share_size_conditions, @uc.expected_profit, symbol,bought_shares, sold_shares, ecn_filter_conditions,  @uc.ts, @uc.cecn.to_s, @uc.step_type.to_i, notime, @iso_order,@exclusive, @uc.cross_size, launch_shares_conditions)
    
  end

  #================================================================================================
  # Function    : menu_refresh (action controller)
  # Input       : N/A
  # Date        : 2010/02/25
  #================================================================================================
  def menu_refresh
    @uc = get_uc
    if params[:start_time]
      @uc.start_time = str_to_unix_time(params[:start_time])
    end
    if params[:end_time]
      @uc.end_time = str_to_unix_time(params[:end_time])
    end 
  end
  
  #================================================================================================
  # Function name: xls_export
  # Input        : start_time, end_time, etc...
  # Description  :
  # Developer    : TuanBD
  # Date created : August, 12 2010
  #================================================================================================
  def xls_export
    #get user context
    @uc = get_uc
    #Filter Entry or Exit
    entry_exit_filter = ""
    #sort order
    sort = "desc"
    @dir1 = params[:dir1] || "asc"
    @dir2 = params[:dir2] || "desc"
    @dir3 = params[:dir3] || "desc"
    @dir4 = params[:dir4] || "desc"
    @dir5 = params[:dir5] || "desc"
    @dir6 = params[:dir6] || "desc"
    @dir7 = params[:dir7] || "desc"
    @dir8 = params[:dir8] || "desc"
    @dir9 = params[:dir9] || "desc"
    @dir10 = params[:dir10] || "desc"
    @dir11 = params[:dir11] || "desc"
    @dir12 = params[:dir12] || "desc"
    @dir13 = params[:dir13] || "desc"
    
    #sort field
    @order_field = params[:order_field] || "net_pl"

    #take trade date as format mm/dd/yyyy
    if params[:start_time]
      @uc.start_time = str_to_unix_time(params[:start_time])
    end 
    start_time = @uc.start_time
    if params[:end_time]
      @uc.end_time = str_to_unix_time(params[:end_time])
    end 
    #filter by time ( start_time, end_time)
    @uc.trade_start_time =  params[:trade_start_time] if params[:trade_start_time]
    @uc.trade_end_time =  params[:trade_end_time] if params[:trade_end_time]
    
    filename_xls_export = [@uc.trade_start_time.to_s, @uc.trade_end_time.to_s, @uc.start_time.strftime("%Y%m%d").to_s,@uc.end_time.strftime("%Y%m%d").to_s]
    #Check start_time, end_time more than Time now
    check_datetime
    
    conditions = ["date between ? and ? "]
    values = [@uc.start_time, @uc.end_time]   
    conditions << "(time_stamp between ? and ?)"
    values << @uc.trade_start_time
    values << @uc.trade_end_time
    #Connect to database
    db_name_suffix = "#{@uc.start_time.year}#{@uc.start_time.month.to_s.rjust(2,'0')}"  
    connect_to_database(db_name_suffix)

    #filter  by ts_id
    @uc.ts = params[:ts_id]  || "0"
    #check number TS
    check_ts = 0
    if @uc.ts.to_s.include? ","
      check_ts = 1
    end
    if check_ts == 0
      if @uc.ts.to_i > 0
        conditions << "ts_id =?"
        values << @uc.ts.to_i - 1 
        filename_xls_export << "Trade Server #{@uc.ts}" 
      elsif @uc.ts.to_i == -1
        conditions << " bought_shares <> sold_shares"
        filename_xls_export << "Trade Server #{@uc.ts}" 
      end
    else
      if @uc.ts.to_s.include? "-1"
        conditions << "((ts_id + 1 in (#{@uc.ts})) or (bought_shares <> sold_shares))"
        filename_xls_export << "Trade Server #{@uc.ts}" 
      else
        conditions << "ts_id + 1 in (#{@uc.ts}) "
        filename_xls_export << "Trade Server #{@uc.ts}" 
      end
    end   
    
    # include exclusive
    if params[:exclusive].to_s == "1" then
      @exclusive = 1
    else
      @exclusive = 0
    end
    
    # Filter by ECN Entry/Exit
    ecn_filter_conditions = ""
    ecn_filter_munlti_conditions = ""
    @uc.ecn_filter = params[:ecn_filter] || "0"
    filename_xls_export << @uc.ecn_filter.upcase 
    if @uc.ecn_filter.to_s.include? "," # Filter by multi ECN Entry/Exit
      ecn_filter_munlti_conditions = TradeResult.build_conditions_for_ecn_entry_exit(@uc.start_time, @uc.end_time, params[:ecn_filter])
      if ecn_filter_munlti_conditions.size > 0
        ecn_filter_conditions = " and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) in (#{ecn_filter_munlti_conditions}) "
      end
    else 
      #filter by ECN Entry/Exit
      if params[:ecn_filter] && params[:ecn_filter].to_s != "0"
        @uc.ecn_filter = params[:ecn_filter].strip.to_s
        if @uc.ecn_filter.size > 2
          ecn_name = @uc.ecn_filter[0, @uc.ecn_filter.size - 2].upcase            
          if @uc.ecn_filter[-2, 2] == "en"
            entry_exit_filter = "Entry"
          else
            entry_exit_filter = "Exit"
          end
          ecn_filter_conditions << " and ecn = '#{ecn_name}' and entry_exit = '#{entry_exit_filter}'"
        end
        #comment this code to add multi exchange trades in filter. Before it is filtered without Multi exchange
        
        if @exclusive == 1 then
          cond_multi_order = get_query_string_for_multi_order(@uc.start_time, @uc.end_time, entry_exit_filter)
          conditions << cond_multi_order
        end
      else
        @uc.ecn_filter = "0"
      end
    end
    
    #filter like symbol
    symbol = ""

    if params[:symbol] && params[:symbol].strip.to_s !=""     
      @uc.symbol = params[:symbol]
      symbol = @uc.symbol.upcase
      conditions << "symbol = ?"
      values << symbol
      filename_xls_export << symbol
    end
    
    # filter by Bought Shares and Sold Shares
    @bt_shares = ""
    @sd_shares = ""
    if params[:bought_shares] && params[:bought_shares].strip.to_i > 0
      @bt_shares = params[:bought_shares].strip.to_i
      conditions << "bought_shares >= ?"
      values << @bt_shares
      filename_xls_export << @bt_shares.to_s
    end

    #filter to sold shares
    if params[:sold_shares] && params[:sold_shares].strip.to_i > 0
      @sd_shares = params[:sold_shares].strip.to_i
      conditions << "sold_shares >= ?"
      values << @sd_shares
      filename_xls_export << @sd_shares.to_s
    end
  
    # Filter by Detected Shares
    share_size_conditions = ""
    @uc.share_size = params[:share_size] || "0"
    if @uc.share_size.to_s.include? "," # Filter by multi Detected Shares
      share_size_conditions = TradeResult.build_conditions_for_filter_by_shares(params[:share_size], 'expected_shares')
      filename_xls_export << @uc.share_size.to_s   
    else 
       if params[:share_size]
        if (params[:share_size].to_i != -100) # Add filter < 100
          @uc.share_size = params[:share_size].to_i
          conditions << "expected_shares >= ?"
          values << @uc.share_size.to_i 
          filename_xls_export << @uc.share_size.to_s    
        else
          @uc.share_size = params[:share_size]
          conditions << "expected_shares < ?"
          values << @uc.share_size.to_i.to_i * -1 
          filename_xls_export << "_100."
        
        end
      end
    end
      
    #filter by launch shares
    launch_shares_conditions = ""
    @uc.launch_shares = params[:launch_shares] || "0"
    if @uc.launch_shares.to_s.include? "," # Filter by multi launch shares
      launch_shares_conditions = TradeResult.build_conditions_for_filter_by_shares(params[:launch_shares], 'launch_shares')
    else 
      if params[:launch_shares]
        if (params[:launch_shares].to_i != -100) # Add filter < 100
          @uc.launch_shares = params[:launch_shares].to_i
          conditions << "launch_shares >= ?"
          values << @uc.launch_shares.to_i 
          filename_xls_export << @uc.launch_shares.to_s   
        else
          @uc.launch_shares = params[:launch_shares].to_i
          conditions << "launch_shares < ?"
          values << @uc.launch_shares.to_i * -1
          filename_xls_export << "_100."
        end
      end
    end
     
    #filter by expected profit
    if params[:expected_profit]
      @uc.expected_profit = params[:expected_profit].to_i  
      if params[:expected_profit] != "0"
        conditions << "expected_pl >= ?"
        values << @uc.expected_profit.to_f
        filename_xls_export << "_100."
      end
    end
    
    #filter by ecn launch
    if params[:cecn]
      @uc.cecn = params[:cecn]      
    else
      @uc.cecn = "0"
    end
    if @uc.cecn && @uc.cecn != "0"
      conditions << "ecn_launch =?"
      values << @uc.cecn.to_s
      filename_xls_export << @uc.cecn.to_s
    end

    #Filter by status   
    if params[:status] && params[:status].to_i >= 0
      @uc.trade_status = params[:status].to_i
    end
    
    #Filter by ISO Orders
    @iso_order = "0"
    if params[:iso_order] && params[:iso_order].to_s == "1"
      #check trade_date must be more than 01/01/2010
      if @uc.trade_date > str_to_unix_time("01/01/2010")
        @iso_order = "1"
        conditions << "is_iso is not NULL and is_iso ='Y'"
        filename_xls_export << "ISO Order"
      end
    end
    if params[:iso_order] && params[:iso_order].to_s == "2"
      #check trade_date must be more than 01/01/2010
      if @uc.trade_date > str_to_unix_time("01/01/2010")
        @iso_order = "2"        
        conditions << "is_iso <> 'Y'"
        filename_xls_export << "NonISO_Order"
      end     
    end 

    #sort by status default
    if params[:sort_default].to_i == 1
      if params[:status].to_i == 0
        #Missed
        @order_field =  "expected_pl"
        sort = "desc"
      elsif  params[:status].to_i == 1
        #Ouch reprice
        @order_field =  "expected_pl"
        sort = "desc"
      elsif params[:status].to_i == 2
        #Stuck Loss
        @order_field =  "net_pl"
        sort = "asc"
      elsif params[:status].to_i == 3
        #Stuck Profit
        @order_field =  "net_pl"
        sort = "desc"
      elsif params[:status].to_i == 4
        #loss
        @order_field =  "net_pl"
        sort = "asc"
      elsif params[:status].to_i == 5
        #profit under
        @order_field =  "net_pl"
        sort = "desc"
      elsif params[:status].to_i == 6
        #profit above
        @order_field =  "net_pl"
        sort = "desc"
      elsif params[:status].to_i == 7
        #total
        @order_field =  "net_pl"
        sort = "desc"
      end
    end

    #Filter by Cross Size
    @uc.cross_size = "0"
    @uc.cross_size = params[:cross_size] if params[:cross_size]
    conditions_cross_size = TradeResult.build_conditions_for_cross_size(@uc.cross_size)
    if conditions_cross_size != ""
      conditions << conditions_cross_size
    end
    #end filter of standing time
    
    filter_status = @uc.trade_status
    @filter_status = filter_status
    filter_conditions = ""
    if filter_status
      #Miss
      if filter_status == 0
        if ((@uc.ecn_filter.first(4) == "ouch" || @uc.ecn_filter.first(4) == "oubx" || @uc.ecn_filter.first(3) == "psx" || @uc.ecn_filter == "0") && (@uc.cecn.to_s == "NDAQ" || @uc.cecn.to_s == "NDBX" || @uc.cecn.to_s == "PSX" || @uc.cecn == "0"))
          tmp_str = "(EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) not in (select (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) from vwtrade_results_twt2 where date between '#{@uc.start_time}' and '#{@uc.end_time}' and (ecn = 'OUCH' or ecn = 'OUBX' or ecn = 'PSX') and price <> ack_price and sold_shares < 1 and bought_shares < 1)"
          filter_conditions = " and sold_shares < 1 and bought_shares < 1 and #{tmp_str}"
        else
          filter_conditions = " and sold_shares < 1 and bought_shares < 1 "
        end
      #Repriced
      elsif filter_status == 1
        filter_conditions = " and ((ecn = 'OUCH' or ecn = 'OUBX' or ecn = 'PSX') and price <> ack_price and sold_shares < 1 and bought_shares < 1) "
      #stuck_loss
      elsif filter_status == 2
        filter_conditions = "  and (bought_shares <> sold_shares AND pm_net_pl < -0.0001)"        
      #stuck_profit
      elsif filter_status == 3
        filter_conditions = "  and (bought_shares <> sold_shares AND pm_net_pl >= -0.0001)"       
      #loss
      elsif filter_status == 4
        filter_conditions = "  and (bought_shares = sold_shares  and net_pl < 0)"       
      #profit under
      elsif filter_status == 5
        filter_conditions = " and (bought_shares >0 and bought_shares = sold_shares  and net_pl >= 0 and net_pl::real < expected_pl::real) "        
      #profit above
      elsif filter_status == 6
        filter_conditions = " and (bought_shares >0 and bought_shares = sold_shares  and net_pl >= 0 and net_pl::real >= expected_pl::real )"
      #All
      elsif filter_status == 7
        if ((@uc.ecn_filter.first(4) == "ouch" || @uc.ecn_filter.first(4) == "oubx" || @uc.ecn_filter.first(3) == "psx" || @uc.ecn_filter == "0") && (@uc.cecn.to_s == "NDAQ" || @uc.cecn.to_s == "NDBX" || @uc.cecn.to_s == "PSX" || @uc.cecn == "0"))
          tmp_str = "(EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) not in (select (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) from vwtrade_results_twt2 where date between '#{@uc.start_time}' and '#{@uc.end_time}' and (ecn = 'OUCH' or ecn = 'OUBX' or ecn = 'PSX') and price <> ack_price and sold_shares < 1 and bought_shares < 1)"
          filter_conditions = " and ts_id <> -1 and #{tmp_str}"
        else
          filter_conditions = " and ts_id <> -1 "
        end
      end
    end
    
    if filter_status != 8
      list_field_string = "distinct date,min(time_stamp) as time_stamp,ts_cid, ts_id,expected_shares, expected_pl, bought_shares, total_bought, sold_shares, total_sold, left_stuck_shares,  as_cid,trade_id, symbol, net_pl, pm_net_pl, launch_shares "
      group_by = "date,ts_cid, ts_id,expected_shares, expected_pl, bought_shares, total_bought, sold_shares, total_sold, left_stuck_shares,  as_cid,trade_id, symbol, net_pl, pm_net_pl, launch_shares "
      @trades = TradeResult2.find( :all,
                      :select=> list_field_string,
                      :conditions => [conditions.join(' and ') + filter_conditions + ecn_filter_conditions + share_size_conditions + launch_shares_conditions] + values,
                      :group => group_by,
                      :order => "#{@order_field} #{sort}"
                      )
      
      filename_str = filename_xls_export.join("_")
      column_names = ["Cross ID" ,"Symbol", "Launch Shares", "Detected Shares", "Expected Profit", "Net P&L", "PM Net P&L", "Bought Shares", "Sold Shares", "Total Sold", "Total Bought", "Date", "Time"]    
      @file_path = ExportFile.export_file_to_xls(column_names, @trades, "Trade_Result", filename_str)     
      if File.exists?(@file_path)
        send_file @file_path, :filename => File.basename(@file_path)
      else
        render :text => "File not found"  
      end    
    else
      @filter_status = filter_status
      @arr_netb = []
      # Check caculation NetB
      if ((check_ts == 0 && @uc.ts.to_i != -1) || check_ts != 0)  && @uc.ecn_filter == "0" && @iso_order == "0" && @bt_shares == "" && @sd_shares == ""
        if @uc.start_time == @uc.end_time
          @arr_netb = TradeResult.calculate_netb(@uc.start_time,@uc.end_time,@uc.trade_start_time,@uc.trade_end_time, @uc.expected_profit, symbol,  @uc.ts, @uc.share_size, @uc.cross_size, @uc.cecn, @uc.launch_shares)
        else
          result = TradeResult.calculate_netb_date_range(@uc.start_time,@uc.end_time,@uc.trade_start_time,@uc.trade_end_time, @uc.expected_profit, symbol,  @uc.ts, @uc.share_size, @uc.cross_size, @uc.cecn, @uc.launch_shares)
          @arr_netb = result[2]
        end   
            end       
      filename_str = filename_xls_export.join("_")
      column_names = ["Cross ID" ,"Symbol", "Launch Shares", "Detected Shares", "Expected Profit", "Net P&L", "PM Net P&L", "Bought Shares", "Sold Shares", "Total Sold", "Total Bought", "Date", "Time"]    
      
      @file_path = ExportFile.export_file_to_xls(column_names, @arr_netb, "Not_Easy_to_Borrow", filename_str)     
      if File.exists?(@file_path)
        send_file @file_path, :filename => File.basename(@file_path)
      else
        render :text => "File not found"  
      end
      
    end
    
    
  end
  
  #================================================================================================
  # Function name: xls_export_mutil_date
  # Input        : start_time, end_time, etc...
  # Description  :
  # Developer    : TuanBD
  # Date created : August, 12 2010
  #================================================================================================
  def xls_export_mutil_date
    #get user context
    @uc = get_uc
    #Filter Entry or Exit
    entry_exit_filter = ""
    #sort order
    sort = "desc"
    @dir1 = params[:dir1] || "asc"
    @dir2 = params[:dir2] || "desc"
    @dir3 = params[:dir3] || "desc"
    @dir4 = params[:dir4] || "desc"
    @dir5 = params[:dir5] || "desc"
    @dir6 = params[:dir6] || "desc"
    @dir7 = params[:dir7] || "desc"
    @dir8 = params[:dir8] || "desc"
    @dir9 = params[:dir9] || "desc"
    @dir10 = params[:dir10] || "desc"
    @dir11 = params[:dir11] || "desc"
    @dir12 = params[:dir12] || "desc"
    @dir13 = params[:dir13] || "desc"
    
    #sort field
    @order_field = params[:order_field] || "net_pl"

    #take trade date as format mm/dd/yyyy
    if params[:start_time]
      @uc.start_time = str_to_unix_time(params[:start_time])
    end 
    start_time = @uc.start_time
    if params[:end_time]
      @uc.end_time = str_to_unix_time(params[:end_time])
    end 
    #filter by time ( start_time, end_time)
    @uc.trade_start_time =  params[:trade_start_time] if params[:trade_start_time]
    @uc.trade_end_time =  params[:trade_end_time] if params[:trade_end_time]
    
    filename_xls_export = [@uc.trade_start_time.to_s, @uc.trade_end_time.to_s, @uc.start_time.strftime("%Y%m%d").to_s,@uc.end_time.strftime("%Y%m%d").to_s]
    #Check start_time, end_time more than Time now
    check_datetime
    conditions = ["date between ? and ? "]
    values = [@uc.start_time, @uc.end_time]   
    conditions << "(time_stamp between ? and ?)"
    values << @uc.trade_start_time
    values << @uc.trade_end_time    
    st = @uc.start_time.month
    en = @uc.end_time.month
    year = @uc.start_time.year      
    year_interval = 0
    if @uc.end_time.year > @uc.start_time.year
      year_interval = @uc.end_time.year - @uc.start_time.year
      en = en + 12 * year_interval
    end 
    st_time = @uc.start_time
        @count_months = 0 
    while st <= en 
        @count_months += 1
        db_name_suffix = "#{Time.now.year}#{Time.now.month.to_s.rjust(2,'0')}"  
        db_name_suffix = "#{Time.now.year}#{Time.now.month.to_s.rjust(2,'0')}"  
      if st <= 12
        db_name_suffix = "#{year}#{st.to_s.rjust(2,'0')}"
      else        
        #p "year_interval  #{year_interval}"
        if year_interval == 1
          db_name_suffix = "#{year + 1}#{(st - 12).to_s.rjust(2,'0')}"
        elsif year_interval == 2
          if st <= 24
            db_name_suffix = "#{year + 1}#{(st - 12).to_s.rjust(2,'0')}"
          else
            db_name_suffix = "#{year + 2}#{(st - 24).to_s.rjust(2,'0')}"
          end
        end
      end

      # lets manually connect to the proper db
      connect_to_database(db_name_suffix)
      #filter  by ts_id   
      @uc.ts = params[:ts_id]  || "0"
      #check number TS
      check_ts = 0
      if @uc.ts.to_s.include? ","
        check_ts = 1
      end
      if check_ts == 0
        if @uc.ts.to_i > 0
          conditions << "ts_id =?"
          values << @uc.ts.to_i - 1 
          filename_xls_export << "Trade Server #{@uc.ts}" 
        elsif @uc.ts.to_i == -1
          conditions << " bought_shares <> sold_shares"
          filename_xls_export << "Trade Server #{@uc.ts}" 
        end
      else
        if @uc.ts.to_s.include? "-1"
          conditions << "((ts_id + 1 in (#{@uc.ts})) or (bought_shares <> sold_shares))"
          filename_xls_export << "Trade Server #{@uc.ts}" 
        else
          conditions << "ts_id + 1 in (#{@uc.ts}) "
          filename_xls_export << "Trade Server #{@uc.ts}" 
        end
      end   
      
      # include exclusive
      if params[:exclusive].to_s == "1" then
        @exclusive = 1
      else
        @exclusive = 0
      end
      
      # Filter by ECN Entry/Exit
      ecn_filter_conditions = ""
      ecn_filter_munlti_conditions = ""
      @uc.ecn_filter = params[:ecn_filter] || "0"
      filename_xls_export << @uc.ecn_filter.upcase 
      if @uc.ecn_filter.to_s.include? "," # Filter by multi ECN Entry/Exit
        ecn_filter_munlti_conditions = TradeResult.build_conditions_for_ecn_entry_exit(@uc.start_time, @uc.end_time, params[:ecn_filter])
        if ecn_filter_munlti_conditions.size > 0
          ecn_filter_conditions = " and (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) in (#{ecn_filter_munlti_conditions}) "
        end
      else 
        #filter by ECN Entry/Exit
        if params[:ecn_filter] && params[:ecn_filter].to_s != "0"
        @uc.ecn_filter = params[:ecn_filter].strip.to_s
        if @uc.ecn_filter.size > 2
          ecn_name = @uc.ecn_filter[0, @uc.ecn_filter.size - 2].upcase            
          if @uc.ecn_filter[-2, 2] == "en"
            entry_exit_filter = "Entry"
          else
            entry_exit_filter = "Exit"
          end
          ecn_filter_conditions << " and ecn = '#{ecn_name}' and entry_exit = '#{entry_exit_filter}'"
          end
          #comment this code to add multi exchange trades in filter. Before it is filtered without Multi exchange
          
          if @exclusive == 1 then
            cond_multi_order = get_query_string_for_multi_order(@uc.start_time, @uc.end_time, entry_exit_filter)
            conditions << cond_multi_order
          end
        else
          @uc.ecn_filter = "0"
        end
      end
     
      #filter like symbol
      symbol = ""
      if params[:symbol] && params[:symbol].strip.to_s !=""     
        @uc.symbol = params[:symbol]
        symbol = @uc.symbol.upcase
        conditions << "symbol = ?"
        values << @uc.symbol.upcase
        
        filename_xls_export << @uc.symbol.upcase
      end
      
      # filter by Bought Shares and Sold Shares
      @bt_shares = ""
      @sd_shares = ""
      if params[:bought_shares] && params[:bought_shares].strip.to_i > 0
        @bt_shares = params[:bought_shares].strip.to_i
        conditions << "bought_shares >= ?"
        values << @bt_shares
        filename_xls_export << @bt_shares.to_s
      end

      #filter to sold shares
      if params[:sold_shares] && params[:sold_shares].strip.to_i > 0
        @sd_shares = params[:sold_shares].strip.to_i
        conditions << "sold_shares >= ?"
        values << @sd_shares
        filename_xls_export << @sd_shares.to_s
      end
      
      # Filter by Detected Shares
      share_size_conditions = ""
      @uc.share_size = params[:share_size] || "0"
      if @uc.share_size.to_s.include? "," # Filter by multi Detected Shares
        share_size_conditions = TradeResult.build_conditions_for_filter_by_shares(params[:share_size], 'expected_shares')
        filename_xls_export << @uc.share_size.to_s   
      else 
         if params[:share_size]
          if (params[:share_size].to_i != -100) # Add filter < 100
            @uc.share_size = params[:share_size].to_i
            conditions << "expected_shares >= ?"
            values << @uc.share_size.to_i 
            filename_xls_export << @uc.share_size.to_s    
          else
            @uc.share_size = params[:share_size]
            conditions << "expected_shares < ?"
            values << @uc.share_size.to_i.to_i * -1 
            filename_xls_export << "_100."
          
          end
        end
      end
      
      #filter by launch shares
      launch_shares_conditions = ""
      @uc.launch_shares = params[:launch_shares] || "0"
      if @uc.launch_shares.to_s.include? "," # Filter by multi launch shares
        launch_shares_conditions = TradeResult.build_conditions_for_filter_by_shares(params[:launch_shares], 'launch_shares')
      else 
        if params[:launch_shares]
          if (params[:launch_shares].to_i != -100) # Add filter < 100
            @uc.launch_shares = params[:launch_shares].to_i
            conditions << "launch_shares >= ?"
            values << @uc.launch_shares.to_i 
            filename_xls_export << @uc.launch_shares.to_s   
          else
            @uc.launch_shares = params[:launch_shares].to_i
            conditions << "launch_shares < ?"
            values << @uc.launch_shares.to_i * -1
            filename_xls_export << @uc.launch_shares.to_s   
          end
        end
      end
     
      #filter by expected profit
      if params[:expected_profit]
        @uc.expected_profit = params[:expected_profit].to_i
        if params[:expected_profit] != "0"
          conditions << "expected_pl >= ?"
          values << @uc.expected_profit.to_f
          filename_xls_export << @uc.expected_profit.to_s
        end
      end
      
      #filter by ecn launch
      if params[:cecn]
        @uc.cecn = params[:cecn]      
      else
        @uc.cecn = "0"
      end
      if @uc.cecn && @uc.cecn != "0"
        conditions << "ecn_launch =?"
        values << @uc.cecn.to_s
        filename_xls_export << @uc.cecn.to_s
      end

      #Filter by status   
      if params[:status] && params[:status].to_i >= 0
        @uc.trade_status = params[:status].to_i
      end

      #Filter by ISO Orders
      @iso_order = "0"
      if params[:iso_order] && params[:iso_order].to_s == "1"
        #check trade_date must be more than 01/01/2010
        if @uc.trade_date > str_to_unix_time("01/01/2010")
          @iso_order = "1"
          conditions << "is_iso is not NULL and is_iso ='Y'"
          filename_xls_export << "ISO Order"
        end
      end
      if params[:iso_order] && params[:iso_order].to_s == "2"
        #check trade_date must be more than 01/01/2010
        if @uc.trade_date > str_to_unix_time("01/01/2010")
          @iso_order = "2"        
          #str_where = " (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) from new_orders where #{condition_no})"
          conditions << "is_iso <> 'Y'"
          filename_xls_export << "NonISO_Order"
        end     
      end 

      #sort by status default
      if params[:sort_default].to_i == 1
        if params[:status].to_i == 0
          #Missed
          @order_field =  "expected_pl"
          sort = "desc"
        elsif  params[:status].to_i == 1
          #Ouch reprice
          @order_field =  "expected_pl"
          sort = "desc"
        elsif params[:status].to_i == 2
          #Stuck Loss
          @order_field =  "net_pl"
          sort = "asc"
        elsif params[:status].to_i == 3
          #Stuck Profit
          @order_field =  "net_pl"
          sort = "desc"
        elsif params[:status].to_i == 4
          #loss
          @order_field =  "net_pl"
          sort = "asc"
        elsif params[:status].to_i == 5
          #profit under
          @order_field =  "net_pl"
          sort = "desc"
        elsif params[:status].to_i == 6
          #profit above
          @order_field =  "net_pl"
          sort = "desc"
        elsif params[:status].to_i == 7
          #total
          @order_field =  "net_pl"
          sort = "desc"
        end
      end
      
      #Filter by Cross Size
      @uc.cross_size = "0"
      @uc.cross_size = params[:cross_size] if params[:cross_size]
      conditions_cross_size = TradeResult.build_conditions_for_cross_size(@uc.cross_size)
      if conditions_cross_size != ""
        conditions << conditions_cross_size
      end

      #end filter of standing time
      filter_status = @uc.trade_status
      @filter_status = filter_status
      filter_conditions = ""
      if filter_status
        #Miss
        if filter_status == 0
          if ((@uc.ecn_filter.first(4) == "ouch" || @uc.ecn_filter.first(4) == "oubx" || @uc.ecn_filter.first(3) == "psx" || @uc.ecn_filter == "0") && (@uc.cecn.to_s == "NDAQ" || @uc.cecn.to_s == "NDBX" || @uc.cecn.to_s == "PSX" || @uc.cecn == "0"))
            tmp_str = "(EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) not in (select (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) from vwtrade_results_twt2 where date between '#{@uc.start_time}' and '#{@uc.end_time}' and (ecn = 'OUCH' or ecn = 'OUBX' or ecn = 'PSX') and price <> ack_price and sold_shares < 1 and bought_shares < 1)"
            filter_conditions = " and sold_shares < 1 and bought_shares < 1 and #{tmp_str}"
          else
          
            filter_conditions = " and sold_shares < 1 and bought_shares < 1 "
          end
        #Repriced
        elsif filter_status == 1
          filter_conditions = " and ((ecn = 'OUCH' or ecn = 'OUBX' or ecn = 'PSX') and price <> ack_price and sold_shares < 1 and bought_shares < 1) "
        #stuck_loss
        elsif filter_status == 2
          filter_conditions = "  and (bought_shares <> sold_shares AND pm_net_pl < -0.0001)"        
        #stuck_profit
        elsif filter_status == 3
          filter_conditions = "  and (bought_shares <> sold_shares AND pm_net_pl >= -0.0001)"       
        #loss
        elsif filter_status == 4
          filter_conditions = "  and (bought_shares = sold_shares  and net_pl < 0)"       
        #profit under
        elsif filter_status == 5
          filter_conditions = " and (bought_shares >0 and bought_shares = sold_shares  and net_pl >= 0 and net_pl::real < expected_pl::real) "        
        #profit above
        elsif filter_status == 6
          filter_conditions = " and (bought_shares >0 and bought_shares = sold_shares  and net_pl >= 0 and net_pl::real >= expected_pl::real )"
            
        #All
        elsif filter_status == 7
          if ((@uc.ecn_filter.first(4) == "ouch" || @uc.ecn_filter.first(4) == "oubx" || @uc.ecn_filter.first(3) == "psx" || @uc.ecn_filter == "0") && (@uc.cecn.to_s == "NDAQ" || @uc.cecn.to_s == "NDBX" || @uc.cecn.to_s == "PSX" || @uc.cecn == "0"))
          tmp_str = "(EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) not in (select (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) from vwtrade_results_twt2 where date between '#{@uc.start_time}' and '#{@uc.end_time}' and (ecn = 'OUCH' or ecn = 'OUBX' or ecn = 'PSX') and price <> ack_price and sold_shares < 1 and bought_shares < 1)"
          filter_conditions = " and ts_id <> -1 and #{tmp_str}"
          else    
            filter_conditions = " and ts_id <> -1 "
          end
        end
      end
      
      if filter_status != 8
        list_field_string = "distinct date,min(time_stamp) as time_stamp,ts_cid, ts_id,expected_shares, expected_pl, bought_shares, total_bought, sold_shares, total_sold, left_stuck_shares,  as_cid,trade_id, symbol, net_pl, pm_net_pl, launch_shares "
        group_by = "date,ts_cid, ts_id,expected_shares, expected_pl, bought_shares, total_bought, sold_shares, total_sold, left_stuck_shares,  as_cid,trade_id, symbol, net_pl, pm_net_pl, launch_shares "
        @trade_month = TradeResult2.find( :all,
                        :select=> list_field_string,
                        :conditions => [conditions.join(' and ') + filter_conditions + ecn_filter_conditions + share_size_conditions + launch_shares_conditions] + values,
                        :group => group_by,
                        :order => "#{@order_field} #{sort}"
                        )
      
        if @count_months == 1
          @trades = @trade_month
        end
  
        if @count_months != 1
          @trades = @trades + @trade_month         
        end
      end       
      st = st +1
    end   # end while
    if filter_status != 8
      if @count_months == 1
        @trades = @trade_month    
      end
      filename_str = filename_xls_export.join("_")
      column_names = ["Cross ID" ,"Symbol","Launch Shares", "Detected Shares", "Expected Profit", "Net P&L", "PM Net P&L", "Bought Shares", "Sold Shares", "Total Sold", "Total Bought", "Date", "Time"]    
      @file_path = ExportFile.export_file_to_xls(column_names, @trades, "Trade_Result", filename_str)     
      if File.exists?(@file_path)
        send_file @file_path, :filename => File.basename(@file_path)
      else
        render :text => "File not found"  
      end
  
    else 
      @filter_status = filter_status
      @arr_netb = []
      # Check caculation NetB
      if ((check_ts == 0 && @uc.ts.to_i != -1) || check_ts != 0)  && @uc.ecn_filter == "0" && @iso_order == "0" && @bt_shares == "" && @sd_shares == ""
        result = TradeResult.calculate_netb_date_range(@uc.start_time,@uc.end_time,@uc.trade_start_time,@uc.trade_end_time, @uc.expected_profit, symbol,  @uc.ts, @uc.share_size, @uc.cross_size, @uc.cecn, @uc.launch_shares)
        @arr_netb = result[2] 
      end       
      filename_str = filename_xls_export.join("_")
      column_names = ["Cross ID" ,"Symbol", "Launch Shares", "Detected Shares", "Expected Profit", "Net P&L", "PM Net P&L", "Bought Shares", "Sold Shares", "Total Sold", "Total Bought", "Date", "Time"]    
      @file_path = ExportFile.export_file_to_xls(column_names, @arr_netb, "Not_Easy_to_Borrow", filename_str)     
      if File.exists?(@file_path)
        send_file @file_path, :filename => File.basename(@file_path)
      else
        render :text => "File not found"  
      end
    end
    
  end
  
  #Multi exchange trades in filter. Before it is filtered without Multi exchange
  def get_query_string_for_multi_order(date_from, date_to, entry_exit_conditions)
    ret = ""    
    ret = <<-SQL
        select distinct (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + cross_id) from (
          SELECT tmp.date, tmp.cross_id, tmp.ts_id, tmp.symbol, tmp.entry_exit,count(tmp.entry_exit) as count
          FROM (SELECT date, cross_id, ts_id, symbol, ecn, entry_exit, count(entry_exit) AS count_entryexit
               FROM new_orders
               WHERE new_orders.ts_id <> -1 and date between '#{date_from}' and '#{date_to}' and entry_exit = '#{entry_exit_conditions}'
               GROUP BY cross_id, date, ts_id, symbol, ecn, entry_exit
               ORDER BY cross_id
            ) as tmp
         GROUP BY tmp.date, tmp.cross_id, tmp.ts_id, tmp.symbol, tmp.entry_exit
     HAVING count(tmp.entry_exit) > 1
         ORDER BY tmp.cross_id
        ) as foo
    SQL
    ret = " (EXTRACT(DAY FROM date) * 10000000 + ts_id * 100000 + ts_cid) not in (#{ret})"
  end
  
  
end
