require 'util'
class AckFillController < ApplicationController
  def index
  #get user context
    @uc = get_uc
  @last_date_trading =  RiskManagement.find_by_sql("select max(date) as trade_date from new_orders" ).first.trade_date
  #check trade_date is null
  if @last_date_trading == nil
    @last_date_trading = Time.now.strftime("%Y-%m-%d")
  end
  @check_date = Time.mktime(@last_date_trading.to_date.year,@last_date_trading.to_date.month,@last_date_trading.to_date.day)
  @uc.last_date_trading = @check_date 
  if @uc.end_time > @check_date
    @uc.end_time = @check_date
  end
  if @uc.start_time > @check_date
    @uc.start_time = @check_date
  end
  render :layout=>"interface2"
  end
  
  #================================================================================================
  # Function    : show_ackfill (action controller)
  # Input       : start_time,
  #                 end_time
  # Description :
  # Developer     : Tuanbd
  # Date      :
  #================================================================================================

  def show_ackfill

    #get user context
    @uc = get_uc
    #page
    @page = params[:page] && params[:page].to_i > 0 ? params[:page].to_i : 1

    @items_per_page = 100
    @offset = (@page - 1) * @items_per_page
      #take trade date as format mm/dd/yyyy
    if params[:date_from]
      @uc.start_time = str_to_unix_time(params[:date_from])
    end
    if params[:date_to]
      @uc.end_time = str_to_unix_time( params[:date_to])
    end
    
  @uc.trade_start_time =  params[:trade_start_time] if params[:trade_start_time]
  @uc.trade_end_time =  params[:trade_end_time] if params[:trade_end_time]


    #@uc.trade_start_time = @uc.trade_start_time || "06:00"
    #@uc.trade_end_time = @uc.trade_end_time || "19:00"

  conditions = ["date between ? and ?"]
  values = [@uc.start_time, @uc.end_time]

  if @uc.trade_start_time.to_s != "6:00" || @uc.trade_end_time != "19:00"
    conditions << "time_stamp between ? and ?"
    values << @uc.trade_start_time
    values << @uc.trade_end_time
  else

  end

  #Check start_time, end_time more than Time now
  check_datetime
  #Connect to database
  db_name_suffix = "#{@uc.start_time.year}#{@uc.start_time.month.to_s.rjust(2,'0')}"
  connect_to_database(db_name_suffix)

    #sort field
    @order_field = params[:sort_field] || "ack_delay"

    
    #@uc.ecn = params[:ecn] if params[:ecn]
  if params[:ecn] && params[:ecn].to_s != ""
      @uc.ecn = params[:ecn]
    else
      @uc.ecn = "0"
    end
    #sort order
    @dir = params[:dir] || "asc"

    #tonggle sort order
    if @dir == "asc"
      @dir = "desc"
    else
      @dir = "asc"
    end

    # Multi trade servers selections
  @uc.ts = params[:ts_id]  || "0"
  #check number TS
  check_ts = 0

  if @uc.ts.to_s.include? ","
    check_ts = 1
  end
  if check_ts == 0
    if @uc.ts.to_i > 0
      conditions << "ts_id = ?"
      values << @uc.ts.to_i - 1
    end
  else
    conditions << "ts_id + 1 in (#{@uc.ts}) "
  end

    #filter by ecn launch
    if @uc.ecn && @uc.ecn.to_s !="0"
     conditions << "ecn = ?"
    values << @uc.ecn.strip.upcase
    end      

  #filter by ISO orders
  iso_order = params[:iso_order].to_i
  if iso_order == 1
    conditions << "is_iso = 'Y'"
  elsif iso_order == 2
    conditions << "is_iso = 'N'"
  end
   
    #number of recorde of given by from day1 to day2
  @count = AvgTime.count(:conditions =>[conditions.join(' and ')] + values )

    #instantiate gaping object
    #@object_pages = Paginator.new self, @count, @items_per_page, @page
    #@page = (@count - 1) / @items_per_page + 1 if @count > 0 and @page * @items_per_page > @count
    @object_pages = WillPaginate::Collection.new(@page, @items_per_page, @count)
  @orders = AvgTime.find( :all,
                :conditions => [conditions.join(' and ')] + values,
              :order => "#{@order_field} #{@dir}",
              :limit => @items_per_page,
                   :offset => @offset
              )

  end
  
  
  #================================================================================================
  # Function    : show_ackfill_multiple_months (action controller)
  # Input       : start_time,
  #               end_time
  # Description :
  # Developer   : 
  # Date        :  08/04/2014
  #================================================================================================

  def show_ackfill_multiple_months

    #get user context
    @uc = get_uc
    #page
    @page = params[:page] && params[:page].to_i > 0 ? params[:page].to_i : 1
    @items_per_page = 100
    @offset = (@page - 1) * @items_per_page
    #take trade date as format mm/dd/yyyy
    if params[:date_from]
      @uc.start_time = str_to_unix_time(params[:date_from])
    end
    if params[:date_to]
      @uc.end_time = str_to_unix_time( params[:date_to])
    end

  @uc.trade_start_time =  params[:trade_start_time] if params[:trade_start_time]
  @uc.trade_end_time =  params[:trade_end_time] if params[:trade_end_time]
  conditions = ["date between ? and ?"]
  values = [@uc.start_time, @uc.end_time]
  if @uc.trade_start_time.to_s != "6:00" || @uc.trade_end_time != "19:00"
    conditions << "time_stamp between ? and ?"
    values << @uc.trade_start_time
    values << @uc.trade_end_time
  end
  #Check start_time, end_time more than Time now
  check_datetime
    #sort field
    @order_field = params[:sort_field] || "ack_delay"
    #@uc.ecn = params[:ecn] if params[:ecn]
  if params[:ecn] && params[:ecn].to_s != ""
    @uc.ecn = params[:ecn]
  else

      @uc.ecn = "0"
     
  end
    #sort order
    @dir = params[:dir] || "asc"
    #tonggle sort order
    if @dir == "asc"
      @dir = "desc"
    else
      @dir = "asc"
    end
    # Multi trade servers selections
  @uc.ts = params[:ts_id]  || "0"
  #check number TS
  check_ts = 0
  if @uc.ts.to_s.include? ","
    check_ts = 1
  end
  if check_ts == 0
    if @uc.ts.to_i > 0
      conditions << "ts_id = ?"
      values << @uc.ts.to_i - 1
    end
  else
    conditions << "ts_id + 1 in (#{@uc.ts}) "
  end
    #filter by ecn launch
    if @uc.ecn && @uc.ecn.to_s !="0"
     conditions << "ecn = ?"
    values << @uc.ecn.strip.upcase
    end      
  #filter by ISO orders
  iso_order = params[:iso_order].to_i
  if iso_order == 1
    conditions << "is_iso = 'Y'"
  elsif iso_order == 2
    conditions << "is_iso = 'N'"
  end
    @count = 0
  @orders = []
  @orders_sort = []
  start_month = @uc.start_time.month
  end_month = @uc.end_time.month
  year = @uc.start_time.year
  year_interval = 0
  if @uc.end_time.year > @uc.start_time.year
    year_interval = @uc.end_time.year - @uc.start_time.year
    end_month = end_month + 12 * year_interval
  end
  #add white loop to process multi month for ticket 1108
  while start_month <= end_month
    current_time = Time.now
    db_name_suffix = "#{current_time.year}#{current_time.month.to_s.rjust(2,'0')}"
    if start_month <= 12
      db_name_suffix = "#{year}#{start_month.to_s.rjust(2,'0')}"
    else
      if year_interval == 1
        db_name_suffix = "#{year + 1}#{(start_month - 12).to_s.rjust(2,'0')}"
      elsif year_interval == 2
        if start_month <= 24
          db_name_suffix = "#{year + 1}#{(start_month - 12).to_s.rjust(2,'0')}"
        else
          db_name_suffix = "#{year + 2}#{(start_month - 24).to_s.rjust(2,'0')}"
        end
      end
    end
    #Connect to database
      connect_to_database(db_name_suffix)
      start_month = start_month + 1
      @count_each_month = AvgTime.count(:conditions =>[conditions.join(' and ')] + values )
      @count_month = @count_each_month
      @offset = (@page - 1) * @items_per_page
      @offset = (@count_month - 1) / @items_per_page * @items_per_page if @page * @items_per_page > @count_month
      if @offset < 0
        @offset = 0
      end
      @orders_month = AvgTime.find( :all,
                  :conditions => [conditions.join(' and ')] + values,
                  :order => "#{@order_field} #{@dir}",
                  :limit => @items_per_page + @offset,
                  :offset => 0
                )
      @count = @count + @count_month
      @orders_month.each { |x|
        @orders << x
      }

    end

    #sort table
    if @order_field == 'ecn' 
      if @dir == 'asc'
        @orders = @orders.sort {|x,y| x.ecn <=> y.ecn }
      else
        @orders = @orders.sort {|x,y| y.ecn <=> x.ecn }
      end

    elsif @order_field == 'symbol' 
      if @dir == 'asc'
        @orders = @orders.sort {|x,y| x.symbol <=> y.symbol }
      else
        @orders = @orders.sort {|x,y| y.symbol <=> x.symbol }
      end

    elsif @order_field == 'order_id' 
      if @dir == 'asc'
        @orders = @orders.sort {|x,y| x.order_id <=> y.order_id }
      else
        @orders = @orders.sort {|x,y| y.order_id <=> x.order_id }
      end

    elsif @order_field == 'ack_delay' 
      if @dir == 'asc'
        @orders = @orders.sort {|x,y| x.ack_delay <=> y.ack_delay }
      else
        @orders = @orders.sort {|x,y| y.ack_delay <=> x.ack_delay }
      end

    elsif @order_field == 'fill_delay' 
      if @dir == 'asc'
        @orders = @orders.sort {|x,y| x.fill_delay <=> y.fill_delay }
      else
        @orders = @orders.sort {|x,y| y.fill_delay <=> x.fill_delay }
      end

    elsif @order_field == 'date' 
      if @dir == 'asc'
        @orders = @orders.sort {|x,y| x.date <=> y.date }
      else
        @orders = @orders.sort {|x,y| y.date <=> x.date }
      end

    elsif @order_field == 'time_stamp' 
      if @dir == 'asc'
        @orders = @orders.sort {|x,y| x.time_stamp <=> y.time_stamp }
      else
        @orders = @orders.sort {|x,y| y.time_stamp <=> x.time_stamp }
      end
    else
      if @dir == 'asc'
        @orders = @orders.sort {|x,y| x.ack_delay <=> y.ack_delay }
      else
        @orders = @orders.sort {|x,y| y.ack_delay <=> x.ack_delay }
      end
    end

    @offset = (@page - 1) * @items_per_page
    if @page * @items_per_page <= @count
       @offset_max = @offset + 100 - 1
    else
       @offset_max = @offset + @count%100 -1
    end

    for item in @offset..@offset_max
      @orders_sort << @orders[item]
    end

    @object_pages = WillPaginate::Collection.new(@page, @items_per_page, @count)

    end
    
  
  def show_chart

  #-------------- Variables declaration ---------------------------------------
  @uc = get_uc
  @color_config = get_color_config

  #Get params value and recalculate its value if needed
  trade_start_time = params[:trade_start_time]
  trade_end_time = params[:trade_end_time]
  start_time = params[:date_from]
  end_time = params[:date_to]
  # Multi trade servers selections
  ts_id = params[:ts_id]  || "0"
  
  ecn_id = 99
  case params[:ecn_id].to_s
    when "ARCA"
      ecn_id = 0
    when "OUCH"
      ecn_id = 1
    when "NYSE"
      ecn_id = 2
    when "RASH"
      ecn_id = 3
    when "BATZ"
      ecn_id = 4
    when "EDGX"
      ecn_id = 5
    when "EDGA"
      ecn_id = 6
    when "OUBX"
      ecn_id = 7
    when "BATY"
      ecn_id = 8
    when "PSX"
      ecn_id = 9
  end

  @step = params[:step_type].to_i
  data_range = params[:date_range].to_i

  if params[:date_from]
      @uc.start_time = str_to_unix_time(params[:date_from])
    end
    if params[:date_to]
      @uc.end_time = str_to_unix_time( params[:date_to])
    end

    @uc.trade_start_time =  params[:trade_start_time] if params[:trade_start_time]
    @uc.trade_end_time =  params[:trade_end_time] if params[:trade_end_time]
    @uc.trade_start_time = @uc.trade_start_time || "06:00"
    @uc.trade_end_time = @uc.trade_end_time || "19:00"
  @uc.step_type = params[:step_type]
  @uc.ts = params[:ts] if params[:ts]

  #-------------Do not need to check date/time because of the above checking funcitons
  if start_time
    start_time = str_to_unix_time(start_time)
    start_time += 6*60*60
    @uc.start_time = start_time
  else
    @uc.start_time = Time.mktime(@uc.start_time.year, @uc.start_time.mon, @uc.start_time.mday,6)
  end

  if end_time
    end_time = str_to_unix_time(end_time)
    end_time += 21*60*60
    @uc.end_time = end_time
  else
    @uc.end_time = Time.mktime(@uc.end_time.year, @uc.end_time.mon, @uc.end_time.mday,21)
  end

  check_datetime



  #-------------- Get data for Ack and Fill chart ----------------------------

  @summary_ack_fill = AckFill.get_ack_fill(trade_start_time, trade_end_time, start_time, end_time, ts_id, ecn_id, @step, data_range)
  @color_config.set_frequency(@summary_ack_fill.size)
  #puts "@summary_ack_fill.size: #{@summary_ack_fill.size}"

  end

    #================================================================================================
  # Function    : menu_refresh (action controller)
  # Input        : N/A
  # Date        : 2010/02/25
  #================================================================================================
  def menu_refresh
    @uc = get_uc
    if params[:start_time]
      @uc.start_time = str_to_unix_time(params[:start_time])
    end
    if params[:end_time]
      @uc.end_time = str_to_unix_time(params[:end_time])
    end
  end
end
