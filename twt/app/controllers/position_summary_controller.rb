class PositionSummaryController < ApplicationController
  #layout "interface2"
#================================================================================================
# Function    : index (action controller)
# Input        : trade_date, page, user context (session)
# Description  : Initialize User context
# Developer      : Tuanbd
# Date      : 07/08/2009
#================================================================================================       
  def index
  #get user context 
  @uc = get_uc
  render :layout=>"interface2"
  end

  def show_position
    #get user context 
  @uc = get_uc
  #sort order
  dir =  "desc"
  @dir0 = params[:dir0] || "asc"
  @dir1 = params[:dir1] || "desc"
  @dir2 = params[:dir2] || "desc"
  @dir3 = params[:dir3] || "desc"
  @dir4 = params[:dir4] || "desc"
  #sort field
  @order_field = params[:order_field] || "net_quantity"

  #tonggle sort order

  if @order_field == "net_quantity"
    @order_field = "abs(net_quantity) desc, net_quantity "
    dir = "desc"
  elsif @order_field == "total_market_value"
    dir = "desc"
  elsif @order_field == "matched_profit_loss" 
    @order_field = "(matched_profit_loss - total_fee)" 
    if  @dir1 == "desc"
      dir = "desc"
      @dir1 = "asc"
    else
      dir = "asc"
      @dir1 = "desc"
    end
  elsif @order_field == "total_quantity" 
    if  @dir2 == "desc"
      dir = "desc"
      @dir2 = "asc"
    else
      dir = "asc"
      @dir2 = "desc"
    end
  elsif @order_field == "last_update"
    if  @dir3 == "desc"
      dir = "desc"
      @dir3 = "asc"
    else
      dir = "asc"
      @dir3 = "desc"
    end
  elsif  @order_field == "symbol"
    if @dir0 == "asc"
      dir = "asc"
      @dir0 = "desc"
    else
      dir = "desc"
      @dir0 = "asc"
    end
  elsif  @order_field == "account"
    if @dir4 == "asc"
      dir = "asc"
      @dir4 = "desc"
    else
      dir = "desc"
      @dir4 = "asc"
    end
  end

  #take trade date as format mm/dd/yyyy
    if params[:trade_date]
    @uc.trade_date = str_to_unix_time(params[:trade_date])
  end

  db_name_suffix = "#{@uc.trade_date.year}#{@uc.trade_date.month.to_s.rjust(2, '0')}"
  connect_to_database(db_name_suffix)

  @traded_stocks = PositionSummary.find(  :all,
                    :conditions=>["date=?", @uc.trade_date],
                    :offset=>@offset,
                    :order=>"#{@order_field} #{dir}"
                    )
    @total = [0.0,0.0,0.0]

  @traded_stocks.each{|stock|
    @total[0] += stock.total_quantity.to_i
    @total[1] += stock.matched_profit_loss.to_f - stock.total_fee.to_f
    @total[2] += stock.total_market_value.to_f
  }
  end

  def stop_trading
    ret = PositionSummary.find_by_sql("update trading_status set status = 0, updated = 1");
  render :text => ret.first.to_s
  end
  
  def get_trading_status
    ret = PositionSummary.find_by_sql("select * from trading_status limit 1").first;
  render :text => ret.status
  end

  #================================================================================================
  # Function    : menu_refresh (action controller)
  # Input        : N/A
  # Date        : 2010/02/25
  #================================================================================================
  def menu_refresh
    @uc = get_uc
  end
end
