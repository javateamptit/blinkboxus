class ActivityController < ApplicationController
    def index
     @uc = get_uc
   @last_date_trading =  RiskManagement.find_by_sql("select max(date) as trade_date from new_orders" ).first.trade_date
  #check trade_date is null
  if @last_date_trading == nil
    @last_date_trading = Time.now.strftime("%Y-%m-%d")
  end
  @check_date = Time.mktime(@last_date_trading.to_date.year,@last_date_trading.to_date.month,@last_date_trading.to_date.day)
  @uc.last_date_trading = @check_date 
  if @uc.start_time > @check_date
    @uc.start_time = @check_date
  end
     render :layout=>"interface2"
   end
   
   def show_activity
       @uc = get_uc
   
       @page = params[:page] && params[:page].to_i > 0 ? params[:page].to_i : 1
  
      @items_per_page = 50
    
      @offset = (@page - 1) * @items_per_page
            #take trade date as format mm/dd/yyyy
      if params[:start_time]
        @uc.start_time = str_to_unix_time(params[:start_time])
      end 
     
      #filter by time ( start_time, end_time)
      @uc.trade_start_time =  params[:trade_start_time] if params[:trade_start_time]
      @uc.trade_end_time =  params[:trade_end_time] if params[:trade_end_time]
      from_time = @uc.trade_start_time.to_s.split(":")
      to_time = @uc.trade_end_time.to_s.split(":")
      #check from_time input is correct, if not reset it
      if from_time.length == 1 && from_time.first.to_i >= 0 && from_time.first.to_i < 24
      @uc.trade_start_time =  "#{from_time.first.to_i}:00"
    elsif from_time.length == 1 && from_time.first.to_i < 0 || from_time.first.to_i > 23
      @uc.trade_start_time = "6:00"
    elsif  from_time.length == 2 
      if from_time.first.to_i < 0 || from_time.first.to_i > 23
        @uc.trade_start_time = "6:00" 
      elsif from_time.last.to_s != "00" && from_time.last.to_i < 0 || from_time.last.to_i > 59
        @uc.trade_start_time = "6:00"
      end
   end
      #check to_time input is correct, if not reset it
      if to_time.length == 1 && to_time.first.to_i >= 0 && to_time.first.to_i < 24 
      @uc.trade_end_time =  "#{to_time.first.to_i}:00"
    elsif to_time.length == 1 && to_time.first.to_i < 0 || to_time.first.to_i > 23
      @uc.trade_end_time = "19:00"
    elsif  to_time.length == 2 
      if to_time.first.to_i < 0 || to_time.first.to_i > 23
        @uc.trade_end_time = "19:00"
      elsif to_time.last.to_s != "00" && to_time.last.to_i < 0 || to_time.last.to_i > 59
        @uc.trade_end_time = "19:00"
      end
    end
      #filter by trader 

      if params[:list_traders]
          @list_traders = params[:list_traders]
          
      end
      #Check start_time, end_time more than Time now
      
      trade_date1 = @uc.start_time.strftime("%Y-%m-%d")
     
      conditions = ["date = ?","time_stamp between ? and ?"]
      values = [trade_date1, @uc.trade_start_time, @uc.trade_end_time] 
      

      if @list_traders == "All"
          
      else
          conditions << "username = ?"
         
          values << @list_traders.to_s.downcase
      end
      
     
        condition_str = conditions.join(" and ")
        conditions = []
        conditions << condition_str
        conditions += values

        db_name_suffix = "#{@uc.start_time.year}#{@uc.start_time.month.to_s.rjust(2,'0')}" 
        # Connect to database need to query data
        connect_to_database(db_name_suffix)
        
        field = "date"
        dir = "asc"
      
        @count = Activity.count(:conditions=>conditions)

          #@page = (@count - 1) / @items_per_page + 1 if @count > 0 and @page * @items_per_page > @count
          #instantiate gaping object 
          #@object_pages = Paginator.new self, @count, @items_per_page, @page
        @object_pages = WillPaginate::Collection.new(@page, @items_per_page, @count)
          #traded Stocks of given trade date

        @activity = Activity.find( :all,             
                      :conditions=>conditions,
                      :offset=> @offset,
                      :limit => @items_per_page,
                      :order=>"#{field} #{dir}, #{"time_stamp"} #{"asc"} "
                      )
         @act_view = []
         @activity.each { |value|
            act_t = []
      tmp_description = ""
            act_t << value.username
            state = ""
            temp_type = value.log_detail.split(",").first.to_i
            if temp_type == 0
                state = "Off-Line"
            elsif temp_type == 1
                state = "Active"
            elsif temp_type == 2
                state = "Active Assistant"
            else 
                state = "Monitoring"
            end
            act_t << state
            
  
      # process the case value description is nil
      if value.description != nil then
        tmp_description << value.description
        indexTemp = tmp_description.index("]")
        tmp_description = tmp_description[(indexTemp+1)..(tmp_description.length-1)]


        # delete two blanks at the left and right of string
        tmp_description.strip
      else
        tmp_description = " "
      end

      time_t = value.date.strftime("%Y-%m-%d").to_s + "   " + value.time_stamp.strftime("%H:%M:%S").to_s
      tmp_description.slice! value.date.strftime("%Y-%m-%d").to_s 
      time_stamp_t = value.time_stamp.strftime("%H:%M:%S").to_s
      indexTemp = tmp_description.index(time_stamp_t)
      if indexTemp != nil
        tmp_description[indexTemp..(tmp_description.length-1)] = ""
      end


            act_t << tmp_description
            act_t << time_t
            @act_view << act_t
         }
     
     
      
   end
   #================================================================================================
  # Function    : menu_refresh (action controller)
  # Input        : N/A
  #================================================================================================
  def menu_refresh
    @uc = get_uc
  end
end
