
require 'util'
class TradeViewController < ApplicationController
  #================================================================================================
  # Function    : show_detail (action controller)
  # Input        : N/A
  #================================================================================================
  def index
    #get user context 

    temp_cross_status = params[:cross_status]
    if params[:cross_status] == "6"

      temp_cross_status = 6
      @uc = get_uc
    else
      temp_cross_status = params[:cross_status].to_i
      @uc = get_uc
    end

    @uc.cross_status = temp_cross_status

    @trade_id = "0"
    if params[:trade_id] && params[:trade_id].to_i > 0
      @trade_id = params[:trade_id]
    end

    if params[:symbol] && params[:symbol].to_s != ""
      @uc.symbol = params[:symbol]
    else
      @uc.symbol = nil
    end
    @ecn_entryexit = params[:ecn_filter] if params[:ecn_filter]

    @sort_type = "desc"
    @sort_type = params[:sort_filter] if params[:sort_filter]

    @last_date_trading =  RiskManagement.find_by_sql("select max(date) as trade_date from new_orders" ).first.trade_date
    #check trade_date is null
    if @last_date_trading == nil
      @last_date_trading = Time.now.strftime("%Y-%m-%d")
    end
    @check_date = Time.mktime(@last_date_trading.to_date.year,@last_date_trading.to_date.month,@last_date_trading.to_date.day)
    @uc.last_date_trading = @check_date 
    if @uc.trade_date > @check_date
      @uc.trade_date = @check_date
    end

    if params[:trade_date]
    @uc.trade_date = str_to_unix_time(params[:trade_date])
    end

    render :layout=>"interface2"
  end
  #================================================================================================
  # Function    : get_as_cid_for_netb (action controller)
  # Input        : N/A
  #================================================================================================

  def get_as_cid_for_netb (start_time,end_time,time_from,time_to,expected_pl,symbol,ts_id)
    arr_t = "("
    netb_arr = TradeResult.calculate_netb(start_time,end_time,time_from,time_to,expected_pl,symbol,ts_id, 0, '0', '0')
    if netb_arr.size > 0
      netb_arr.each { |value|
        arr_t << value[0].to_s
        arr_t << ","
      }
      arr_t[arr_t.size-1] = ')'
    else
      arr_t = "(-1)"
    end

    return arr_t
  end 

  #================================================================================================
  # Function    : show_detail (action controller)
  # Input        : N/A
  #================================================================================================
  def show_detail
    @uc = get_uc

    #@page = (params[:page] || 1).to_i
    @page = params[:page] && params[:page].to_i > 0 ? params[:page].to_i : 1
    @items_per_page = 100
    @offset = (@page - 1) * @items_per_page

    #take trade date as format mm/dd/yyyy
      if params[:trade_date]
      @uc.trade_date = str_to_unix_time(params[:trade_date])
    end
    trade_date_str = @uc.trade_date.strftime("%F")
    # trade_date_str << @uc.trade_date.year.to_s << "-" <<@uc.trade_date.month.to_s << "-" <<@uc.trade_date.day.to_s
    # trade_date_str = trade_date_str.strftime("%F")

    #the date update bbo_infos
    trade_date_str = DateTime.parse(trade_date_str)
    bbo_info_date =  DateTime.parse("2012-08-22")

    @iso_start_time = Time.mktime(@uc.trade_date.year,@uc.trade_date.month,@uc.trade_date.day,8,30).to_i
    @iso_end_time = Time.mktime(@uc.trade_date.year,@uc.trade_date.month,@uc.trade_date.day,15,0).to_i

    @oldView = 0
    if trade_date_str < bbo_info_date
      @oldView = 1
    end

    #Check start_time, end_time more than Time now
    check_datetime
    #Set up connection to database which wants to use
    db_name_suffix = "#{@uc.trade_date.year}#{@uc.trade_date.month.to_s.rjust(2,'0')}"
    # Connect to database need to query data
    connect_to_database(db_name_suffix)

    if @oldView == 1
      conditions = ["date=?"]
      condition_values = [@uc.trade_date]

      #filter by symbol
      if params[:symbol] && params[:symbol].to_s != ""
        @uc.symbol = params[:symbol]
      else
        @uc.symbol = nil
      end


      @trade_id = "0"
      if  params[:trade_id] && params[:trade_id].to_i > 0
        @trade_id = params[:trade_id]
      end

      @uc.ts = params[:ts]  || "0"
      #check number TS
      check_ts = 0
      if @uc.ts.to_s.include? ","
        check_ts = 1
      end

      if params[:cross_status]
        @uc.cross_status = params[:cross_status].to_i
      end

      #filter by symbol
      if @uc.symbol && @uc.symbol.strip != ""
        conditions << " symbol = ?"
        condition_values << @uc.symbol.strip.to_s.upcase
      end
      #filter by trade_id
      if @trade_id && @trade_id.strip.to_i >= 0
        conditions << "as_cid >= ?"
        condition_values << @trade_id.to_i
      end

      #filter by ts
      if  @uc.cross_status.to_i != 1
        if check_ts == 0
          if @uc.ts.to_i > 0
            conditions << "ts_id=?"
            condition_values << @uc.ts.to_i - 1
          end
        else
          conditions << "ts_id + 1 in (#{@uc.ts}) "
        end
      end
        
      # get trade_time --process filter by time
      @uc.trade_start_time = params[:trade_start_time] if params[:trade_start_time]
      @uc.trade_end_time = params[:trade_end_time] if params[:trade_end_time]

      from_time = @uc.trade_start_time.to_s.split(":")
      to_time = @uc.trade_end_time.to_s.split(":")
      #check from_time input is correct, if not reset it
      if from_time.length == 1 && from_time.first.to_i >= 0 && from_time.first.to_i < 24
        @uc.trade_start_time =  "#{from_time.first.to_i}:00"
      elsif from_time.length == 1 && from_time.first.to_i < 0 || from_time.first.to_i > 23
        @uc.trade_start_time = "6:00"
      elsif  from_time.length == 2 
        if from_time.first.to_i < 0 || from_time.first.to_i > 23
          @uc.trade_start_time = "6:00" 
        elsif from_time.last.to_s != "00" && from_time.last.to_i < 0 || from_time.last.to_i > 59
          @uc.trade_start_time = "6:00"
        end
      end

      #check to_time input is correct, if not reset it
      if to_time.length == 1 && to_time.first.to_i >= 0 && to_time.first.to_i < 24 
        @uc.trade_end_time =  "#{to_time.first.to_i}:00"
      elsif to_time.length == 1 && to_time.first.to_i < 0 || to_time.first.to_i > 23
        @uc.trade_end_time = "19:00"
      elsif  to_time.length == 2 
        if to_time.first.to_i < 0 || to_time.first.to_i > 23
          @uc.trade_end_time = "19:00"
        elsif to_time.last.to_s != "00" && to_time.last.to_i < 0 || to_time.last.to_i > 59
          @uc.trade_end_time = "19:00"
        end
      end

      @sort_code = 0
      # get the input tot sort data
      if params[:sort_filter]
        sort_way = params[:sort_filter]
        if sort_way == "asc"
          @sort_code = 0
        else
          @sort_code = 1
        end
      end

      @start_iso_time = Time.gm(2012,1,1,8,30)
      @end_iso_time = Time.gm(2012,1,1,15,0)
      #Filter by time ==> in case: trade_start_time != 6:00 and trade_end_time != 19:00
      if @uc.trade_start_time.strip != "" && @uc.trade_end_time.strip != "" && (@uc.trade_start_time != "6:00" || @uc.trade_end_time != "19:00")
        conditions << "((to_char(to_timestamp(split_part(raw_content,' ',3)::double precision),'HH24:MI:SS'))::time without time zone) between ('#{@uc.trade_start_time}'::time without time zone) and ('#{@uc.trade_end_time}'::time without time zone)"
      end
      #filter by ECN entry/exit
      @ecn_entryexit = params[:ecn_filter] if params[:ecn_filter] 
      #Filter by status
      #All_traded
      if @uc.cross_status == 0 
        conditions << "trade_id is not null"
      #Cross detected
      elsif @uc.cross_status.to_i == -1
        conditions << "trade_id is null and as_cid not in (select distinct as_cid from view_trade_view where date = '#{@uc.trade_date}' and raw_content like '5%')"
        conditions << "as_cid not in (select distinct as_cid from trade_results where date = '#{@uc.trade_date}')"
      #Missed Trade
      elsif @uc.cross_status == 2
        #tr_conditions << "bought_shares  = 0 and sold_shares  = 0 " 
        conditions << "bought_shares  < 1 and sold_shares  < 1 " 
      #Stuck
      elsif @uc.cross_status == 3
        conditions << "bought_shares <> sold_shares"
      #Loss
      elsif @uc.cross_status == 4
        conditions << " bought_shares = sold_shares and total_sold < total_bought"
      #Profit
      elsif @uc.cross_status == 5
        conditions << " bought_shares = sold_shares and sold_shares > 0 and total_sold >= total_bought"
      #NETB
      elsif @uc.cross_status == 6
        ts_id = @uc.ts
        symbol = ""
        start_time = @uc.trade_date
        end_time = @uc.trade_date
        expected_pl = 0
        time_from = @uc.trade_start_time
        time_to = @uc.trade_end_time
        as_cid_arr = '(-1)'
        if @ecn_entryexit.to_s == "0" # Check filter by Ecn Entry/Exit
          as_cid_arr = get_as_cid_for_netb(start_time,end_time,time_from,time_to,expected_pl,symbol,ts_id)
        end
        conditions << " as_cid in #{as_cid_arr} "
      end

      #All : @uc.cross_status == 1

      #Filter by ISO Orders
      @iso_order = "0"
      if params[:iso_order] && params[:iso_order].to_s == "1"

        #check trade_date must be more than 01/01/2010
        if @uc.trade_date > str_to_unix_time("01/01/2010")
          @iso_order = "1"
          conditions << "content is not null"
        end
      end
      #filter by ECN entry/exit
      if (@ecn_entryexit.to_s != "0") and (@uc.cross_status != 6)

        ecn_name, entry_exit = @ecn_entryexit.split('_')
        if (ecn_name == "BZSR")
          conditions << "(ts_id * 1000000 + ts_cid) IN (SELECT DISTINCT (ts_id * 1000000 + cross_id) FROM new_orders WHERE date='#{@uc.trade_date}' AND ts_id != -1 AND ecn = 'BATZ' and entry_exit = '#{entry_exit}' and routing_inst = 'RL2')"
        elsif (ecn_name == "BATZ")
          conditions << "(ts_id * 1000000 + ts_cid) IN (SELECT DISTINCT (ts_id * 1000000 + cross_id) FROM new_orders WHERE date='#{@uc.trade_date}' AND ts_id != -1 AND ecn = 'BATZ' and entry_exit = '#{entry_exit}' and routing_inst != 'RL2')"
        else
          conditions << "(ts_id * 1000000 + ts_cid) IN (SELECT DISTINCT (ts_id * 1000000 + cross_id) FROM new_orders WHERE date='#{@uc.trade_date}' AND ts_id != -1 AND ecn = '#{ecn_name}' and entry_exit = '#{entry_exit}')"
        end
      end
      #get data from database
      count_cross_ids = TradeView.find(:all,
                      :select=> "distinct as_cid",
                       :conditions => [conditions.join(' and ')] + condition_values,
                       :order=>" (split_part(raw_content,' ',3)::integer) #{sort_way}"
                    )

      @count = count_cross_ids.size
      count_cross_ids = count_cross_ids.collect{|x| x.as_cid if x}
      @cross_ids = []
      @cross_ids = count_cross_ids.slice(@offset,@items_per_page)


      field_names = "id, as_cid, ts_cid, ts_id, symbol, raw_content, date, trade_id, trade_type, bought_shares, sold_shares, total_bought, total_sold, content"
      raw_crosses = nil
      raw_crosses = TradeView.find(:all, 
              :select => "distinct #{field_names}",
                    :conditions=>["date=? and as_cid in (?)", @uc.trade_date, @cross_ids],
                    :order=>"id"
                    )

      #@object_pages = Paginator.new self, @count, @items_per_page, @page
      #@page = (@count - 1) / @items_per_page + 1 if @count > 0 and @page * @items_per_page > @count
      @object_pages = WillPaginate::Collection.new(@page, @items_per_page, @count)

      @data = {}
      raw_crosses.each{|cross|

        if @data[cross.as_cid.to_i]
          if @data[cross.as_cid.to_i][cross.ts_id.to_i]
            @data[cross.as_cid.to_i][cross.ts_id.to_i] << cross
          else
            @data[cross.as_cid.to_i][cross.ts_id.to_i] = [cross]

          end
        else

          @data[cross.as_cid.to_i] = {"symbol"=>cross.symbol,cross.ts_id.to_i=>[cross]}

        end

      }

    else # process with new view
      conditions = ["date=?"]
      condition_values = [@uc.trade_date]

      #filter by symbol
      if params[:symbol] && params[:symbol].to_s != ""
        @uc.symbol = params[:symbol]
      else
        @uc.symbol = nil
      end


      @trade_id = "0"
      if  params[:trade_id] && params[:trade_id].to_i > 0
        @trade_id = params[:trade_id]
      end

      # Multi trade servers selections
      @uc.ts = params[:ts]  || "0"
      #check number TS
      check_ts = 0
      if @uc.ts.to_s.include? ","
        check_ts = 1
      end

      if params[:cross_status]
        @uc.cross_status = params[:cross_status].to_i
      end

      #filter by symbol
      if @uc.symbol && @uc.symbol.strip != ""
        conditions << " symbol = ?"
        condition_values << @uc.symbol.strip.to_s.upcase
      end
      #filter by trade_id
      if @trade_id && @trade_id.strip.to_i >= 0
        conditions << "as_cid >= ?"
        condition_values << @trade_id.to_i
      end

      #filter by ts
      if  @uc.cross_status.to_i != 1
        if check_ts == 0
          if @uc.ts.to_i > 0
            conditions << "ts_id = ?"
            condition_values << @uc.ts.to_i - 1
          end
        else
          conditions << "ts_id + 1 in (#{@uc.ts}) "
        end
      end
      
      # get trade_time --process filter by time
      @uc.trade_start_time = params[:trade_start_time] if params[:trade_start_time]
      @uc.trade_end_time = params[:trade_end_time] if params[:trade_end_time]

      from_time = @uc.trade_start_time.to_s.split(":")
      to_time = @uc.trade_end_time.to_s.split(":")
      #check from_time input is correct, if not reset it
      if from_time.length == 1 && from_time.first.to_i >= 0 && from_time.first.to_i < 24
        @uc.trade_start_time =  "#{from_time.first.to_i}:00"
      elsif from_time.length == 1 && from_time.first.to_i < 0 || from_time.first.to_i > 23
        @uc.trade_start_time = "6:00"
      elsif  from_time.length == 2 
        if from_time.first.to_i < 0 || from_time.first.to_i > 23
          @uc.trade_start_time = "6:00" 
        elsif from_time.last.to_s != "00" && from_time.last.to_i < 0 || from_time.last.to_i > 59
          @uc.trade_start_time = "6:00"
        end
      end

      #check to_time input is correct, if not reset it
      if to_time.length == 1 && to_time.first.to_i >= 0 && to_time.first.to_i < 24 
        @uc.trade_end_time =  "#{to_time.first.to_i}:00"
      elsif to_time.length == 1 && to_time.first.to_i < 0 || to_time.first.to_i > 23
        @uc.trade_end_time = "19:00"
      elsif  to_time.length == 2 
        if to_time.first.to_i < 0 || to_time.first.to_i > 23
          @uc.trade_end_time = "19:00"
        elsif to_time.last.to_s != "00" && to_time.last.to_i < 0 || to_time.last.to_i > 59
          @uc.trade_end_time = "19:00"
        end
      end

      @sort_code = 0
      # get the input tot sort data
      if params[:sort_filter]
        sort_way = params[:sort_filter]
        if sort_way == "asc"
          @sort_code = 0
        else
          @sort_code = 1
        end
      end

      #Filter by time ==> in case: trade_start_time != 6:00 and trade_end_time != 19:00
      if @uc.trade_start_time.strip != "" && @uc.trade_end_time.strip != "" && (@uc.trade_start_time != "6:00" || @uc.trade_end_time != "19:00")

        conditions << "((to_char(to_timestamp(timestamp::double precision),'HH24:MI:SS'))::time without time zone) >= ('#{@uc.trade_start_time}'::time without time zone) and  ((to_char(to_timestamp(timestamp::double precision),'HH24:MI:SS'))::time without time zone) < ('#{@uc.trade_end_time}'::time without time zone)"
      end
      #filter by ECN entry/exit
      @ecn_entryexit = params[:ecn_filter] if params[:ecn_filter] 
      #Filter by status
      #All_traded
      if @uc.cross_status == 0 
        conditions << "trade_id is not null"
      #Cross detected
      elsif @uc.cross_status.to_i == -1
        conditions << "trade_id is null and as_cid not in (select distinct as_cid from view_trade_view where date = '#{@uc.trade_date}' and raw_content like '5%')"
        conditions << "as_cid not in (select distinct as_cid from trade_results where date = '#{@uc.trade_date}')"
      #Missed Trade
      elsif @uc.cross_status == 2
        #tr_conditions << "bought_shares  = 0 and sold_shares  = 0 " 
        conditions << "bought_shares  < 1 and sold_shares  < 1 " 
      #Stuck
      elsif @uc.cross_status == 3
        conditions << "bought_shares <> sold_shares"
      #Loss
      elsif @uc.cross_status == 4
        conditions << " bought_shares = sold_shares and total_sold < total_bought"
      #Profit
      elsif @uc.cross_status == 5
        conditions << " bought_shares = sold_shares and sold_shares > 0 and total_sold >= total_bought"
      #NETB
      elsif @uc.cross_status == 6
        ts_id = @uc.ts
        symbol = ""
        start_time = @uc.trade_date
        end_time = @uc.trade_date
        expected_pl = 0
        time_from = @uc.trade_start_time
        time_to = @uc.trade_end_time
        as_cid_arr = '(-1)'
        if @ecn_entryexit.to_s == "0" # Check filter by Ecn Entry/Exit
          as_cid_arr = get_as_cid_for_netb(start_time,end_time,time_from,time_to,expected_pl,symbol,ts_id)
        end
        conditions << " as_cid in #{as_cid_arr} "
      end

      #All : @uc.cross_status == 1

      #Filter by ISO Orders
      @iso_order = "0"
      if params[:iso_order] && params[:iso_order].to_s == "1"
        ts_condition = ""
        # Multi trade servers selections
        if check_ts == 0
          if @uc.ts.to_i > 0
            ts_condition = "and ts_id = #{ @uc.ts.to_i - 1}"
          end
        else
          ts_condition = "and ts_id + 1 in (#{@uc.ts}) "
        end

        conditions << " (ts_id || '_' || ts_cid) in (select distinct (ts_id || '_' || cross_id) from bbo_infos where bbo_infos.date = '#{trade_date_str}' #{ts_condition}) "

      end
      #filter by ECN entry/exit
      if (@ecn_entryexit.to_s != "0") and (@uc.cross_status != 6)
        ecn_name, entry_exit = @ecn_entryexit.split('_')
        if (ecn_name == "BZSR")
          conditions << "(ts_id * 1000000 + ts_cid) IN (SELECT DISTINCT (ts_id * 1000000 + cross_id) FROM new_orders WHERE date='#{@uc.trade_date}' AND ts_id != -1 AND ecn = 'BATZ' and entry_exit = '#{entry_exit}' and routing_inst = 'RL2')"
        elsif (ecn_name == "BATZ")
          conditions << "(ts_id * 1000000 + ts_cid) IN (SELECT DISTINCT (ts_id * 1000000 + cross_id) FROM new_orders WHERE date='#{@uc.trade_date}' AND ts_id != -1 AND ecn = 'BATZ' and entry_exit = '#{entry_exit}' and routing_inst != 'RL2')"
        else
          conditions << "(ts_id * 1000000 + ts_cid) IN (SELECT DISTINCT (ts_id * 1000000 + cross_id) FROM new_orders WHERE date='#{@uc.trade_date}' AND ts_id != -1 AND ecn = '#{ecn_name}' and entry_exit = '#{entry_exit}')"
        end
      end
      #get data from database
      count_cross_ids = TradeInfo.find(:all,
                      :select=> "distinct as_cid",
                      :conditions => [conditions.join(' and ')] + condition_values,
                      :order=>" as_cid #{sort_way}"
                    )

      @count = count_cross_ids.size
      count_cross_ids = count_cross_ids.collect{|x| x.as_cid if x}
      @cross_ids = []
      @cross_ids = count_cross_ids.slice(@offset,@items_per_page)


      field_names = "id, as_cid, ts_cid, ts_id, symbol, raw_content, date, trade_id, trade_type, bought_shares, sold_shares, total_bought, total_sold,bbo_id,timestamp"
      raw_crosses = nil
      raw_crosses = TradeInfo.find(:all, 
              :select => "distinct #{field_names}",
                    :conditions=>["date=? and as_cid in (?)", @uc.trade_date, @cross_ids],
                    :order=>"  timestamp #{sort_way},id,as_cid"
                    )

      #@object_pages = Paginator.new self, @count, @items_per_page, @page
      #@page = (@count - 1) / @items_per_page + 1 if @count > 0 and @page * @items_per_page > @count
      @object_pages = WillPaginate::Collection.new(@page, @items_per_page, @count)

      @data = {}
      raw_crosses.each{|cross|

        if @data[cross.as_cid.to_i]
          if @data[cross.as_cid.to_i][cross.ts_id.to_i]
            @data[cross.as_cid.to_i][cross.ts_id.to_i] << cross
          else
            @data[cross.as_cid.to_i][cross.ts_id.to_i] = [cross]

          end
        else

          @data[cross.as_cid.to_i] = {"symbol"=>cross.symbol,cross.ts_id.to_i=>[cross]}

        end

      }
    end


  end



  #================================================================================================
  # Function    : menu_refresh (action controller)
  # Input        : N/A
  #================================================================================================
  def menu_refresh
    @uc = get_uc
  end

end

