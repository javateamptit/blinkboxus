class UtilitiesController < ApplicationController
 
  def bulk
    #get user context
    @uc = get_uc
  @last_date_trading =  RiskManagement.find_by_sql("select max(date) as trade_date from new_orders" ).first.trade_date
  #check trade_date is null
  if @last_date_trading == nil
    @last_date_trading = Time.now.strftime("%Y-%m-%d")
  end
  @check_date = Time.mktime(@last_date_trading.to_date.year,@last_date_trading.to_date.month,@last_date_trading.to_date.day)
  @uc.last_date_trading = @check_date 
  if @uc.end_time > @check_date
    @uc.end_time = @check_date
  end
  if @uc.start_time > @check_date
    @uc.start_time = @check_date
  end
  if @uc.trade_date > @check_date
    @uc.trade_date = @check_date
  end
  render :layout=>"interface2"
  end
 #================================================================================================
  # Function    : trade_break (action controller)
  # Input        : symbol
  #          : price_from
  #          : price_to
  #          : time_from
  #          : time_to
  # Description  : search fills of the symbol that has the price  need to broken
  # Developer      : KhanhTT
  # Date      : 2011-08-29
  #================================================================================================
  def trade_break
    #get user context
    @uc = get_uc

    #page
    @page = params[:page] && params[:page].to_i > 0 ? params[:page].to_i : 1
    @items_per_page = 30
    @offset = (@page - 1) * @items_per_page

    #sort order
    @dir = params[:dir] || "asc"
    #sort field
    @order_field = params[:sort_field] || "trade_time"

    #tonggle sort order
    if @dir == "asc"
      @dir = "desc"
    else
      @dir = "asc"
    end
    #take trade date as format mm/dd/yyyy
    if params[:trade_date]
      @uc.trade_date = str_to_unix_time(params[:trade_date])
    end

  if params[:symbol] && params[:symbol].to_s != ""
    @uc.symbol = params[:symbol].strip
  else
    @uc.symbol = ""
  end

    @uc.trade_start_time =  params[:trade_start_time] if params[:trade_start_time]
    @uc.trade_end_time =  params[:trade_end_time] if params[:trade_end_time]
    @uc.trade_start_time = @uc.trade_start_time || "02:00"
    @uc.trade_end_time = @uc.trade_end_time || "19:00"

  #Check start_time, end_time more than Time now
  check_datetime

    #filter by time_stamp
    conditions = ["date = '#{@uc.trade_date}' ", "and (trade_time between '#{@uc.trade_start_time}' and '#{@uc.trade_end_time}') "]
  
  
  #filter by ECN order
  if params[:ecn] && params[:ecn].to_s != ""
    @uc.ecn = params[:ecn]
  else
      @uc.ecn = "0"
  end
  if @uc.ecn && @uc.ecn != "0"
    conditions << "and ecn = '#{@uc.ecn}' "
    #values << @uc.ecn
  end
  
    if params[:price_from]  && params[:price_from].to_f > 0
      @uc.price_from = params[:price_from]
      conditions << "and filled_price >= #{@uc.price_from} "
      #values << @uc.price_from
    else
      @uc.price_from = nil
    end

  if params[:symbol] && params[:symbol].to_s != ""
       @uc.symbol = params[:symbol].upcase.strip
    conditions << "and symbol = '#{@uc.symbol}' "
    else
      @uc.symbol = nil
    end
  #Filer by filled shares
  @fill_share = params[:fill_share].to_i || 0

  if @fill_share.to_i > 0
    conditions << " and filled_shares >= #{@fill_share.to_i} "
    #values << @fill_share.to_i 
  end
  #Filter by Buy/Sell
  @buysell = params[:buysell] || 0
  if @buysell.to_i > 0
    if @buysell.to_i == 2
      conditions << " and (action = 'S') "
      #values << "S"
      #values << "SS"
    elsif @buysell.to_i == 3
      conditions << " and (action = 'SS') "
    else
      conditions << "and (action = 'BC' or action = 'BO') "
      # values << "BC"
      # values << "BO"
    end
  end
  #Filter  by filled price_to
    if params[:price_to] && params[:price_to].to_f > 0
      @uc.price_to = params[:price_to]
      conditions << " and filled_price <= #{@uc.price_to} "
      #values << @uc.price_to
    else
      @uc.price_to = nil
    end
    conditions << "and fill_id not in (select fill_id from brokens where is_cee = 'f') "




  sql_str = "SELECT count(*) as count_t, sum(filled_shares) as total_shares, array_to_string(array_agg(fill_id), '_') as fill_ids FROM (SELECT * FROM vwtrade_breaks where #{conditions}) as temp"
    @fill_ids = "0"
  @total_filled_shares = 0
  @count = 0
  @data = TradeBreak.find_by_sql("#{sql_str}")
  count_t = 0
  total = 0
  fill_ids_t = "0"
  @data.each { |x|

    count_t = x.count_t.to_i
    total = x.total_shares.to_i

    fill_ids_t << "_" 
    fill_ids_t << x.fill_ids.to_s
  }
  @fill_ids = fill_ids_t.to_s
  @total_filled_shares = total.to_i
  @count = count_t.to_i


  @object_pages = WillPaginate::Collection.new(@page, @items_per_page, @count)
    #fills given date, time_stamp

    @tradebreaks = TradeBreak.find_by_sql("select * from vwtrade_breaks where #{conditions} order by #{@order_field} #{@dir}  limit 30 offset #{@offset};")

    
    
    #render :layout=>"interface2"
  end
  #================================================================================================
  # Function    : break_all_trade (action controller)
  # Input        : fill_id #list fill_ids
  # Description  : break all trades have the list fill_id , USING for AJAX
  # Developer      : Tuanbd
  # Date      : 22/09/2008
  #================================================================================================
  def break_all_trade
    @uc = get_uc
    list_fill_ids = []
    if params[:fill_id]
      list_fill_ids = params[:fill_id].split('_')
    end
    status_mapping = {}
    filing_status_list().each{ |status|
      status_mapping[status[1]] = status[0]
    }

    status = status_mapping[params[:status].strip.to_i] if params[:status]
    
    note = params[:note] if params[:note]
    
    if list_fill_ids.size.to_i > 0

      broken = Order.find_by_sql(["select max(id) as max_broken_id from brokens"]).first
      new_broken_id = 1
      if broken && broken.max_broken_id
        new_broken_id = broken.max_broken_id.to_i  + 1
      end
      new_shares = 0
      broken_shares = 0

      if list_fill_ids.size.to_i >= 1
       
         fills = Order.find_by_sql(["select * from fills where fill_id in (?)", list_fill_ids])

        fills.each{|fill|

              exist_broken = Order.find_by_sql(["select * from brokens where oid = ? and exec_id=?", fill.oid, fill.exec_id])
                
              if exist_broken.empty?

              Order.find_by_sql(["insert into brokens(fill_id, new_shares, new_price, broken_shares,trade_date,oid, exec_id, filing_status, cee_note, is_cee) values(?,?,?,?,?,?,?,?,?,?)",fill.fill_id,0, fill.price, fill.shares, @uc.trade_date, fill.oid.to_i, fill.exec_id.to_s, status, note, false])
              else

              Order.find_by_sql(["update brokens set new_shares=?, new_price=?, broken_shares=?,trade_date=?, filing_status=?, cee_note=?, is_cee=? where oid=? and exec_id=?", 0, fill.price, fill.shares, @uc.trade_date, status, note, false, fill.oid.to_i, fill.exec_id.to_s])

              end

            }
          end

      render :text=> "OK"
    else
      render :text=> "Invalid Parameters"
    end
  end
  
  #================================================================================================
  # Function    : filing_status
  # Input        : fill_id #list fill_ids
  # Description  : add filing status
  # Developer      : KhanhTT
  # Date      : 2011-09-16
  #================================================================================================
  def filing_status
    @uc = get_uc
    list_fill_ids = []

    if params[:fill_id]
      list_fill_ids = params[:fill_id].gsub('_',',')
    end
    list_fill_ids[0..1] = "'"
    list_fill_ids << "'"
    status_mapping = {}
    filing_status_list().each{ |status|
      status_mapping[status[1]] = status[0]
    }
    status = "'"
    status << status_mapping[params[:status].strip.to_i] if params[:status]
    status << "'"

    note = "'"
    note << params[:note] if params[:note]
    note << "'"
    trade_date = "'"
    trade_date << @uc.trade_date.strftime("%F")
    trade_date << "'"

    if list_fill_ids.size.to_i > 0


        Order.find_by_sql("select _insert_or_update_cee_filing_notes_to_broken_table(#{list_fill_ids}, #{trade_date}, #{status}, #{note})")
      render :text=> "OK"
    else
      render :text=> "Invalid Parameters"
    end
  end
  
  #================================================================================================
  # Function    : broken (action controller)
  # Input        : 
  # Description      : get uc
  # Developer      : 
  # Date      : 
  #================================================================================================
  def index
    #get user context
    @uc = get_uc
    render :layout=>"interface2"
  end
  #================================================================================================
  # Function    : show_broken (action controller)
  # Input        : 
  # Description      : get uc
  # Developer      : 
  # Date      : 
  #================================================================================================
  def show_broken
    #get user context
    @uc = get_uc
    #page
    @page = params[:page] && params[:page].to_i > 0 ? params[:page].to_i : 1
    @items_per_page = 100

    @offset = (@page - 1) * @items_per_page

    #sort order
    @dir = params[:dir] || "asc"
    #sort field
    @order_field = params[:sort_field] || "trade_time"

    #tonggle sort order
    if @dir == "asc"
      @dir = "desc"
    else
      @dir = "asc"
    end
    #take start date, end date as format mm/dd/yyyy
    if params[:start_time]
      @uc.start_time = str_to_unix_time(params[:start_time])      
    end
    if params[:end_time]
      @uc.end_time = str_to_unix_time(params[:end_time])      
    end
    st = @uc.start_time.month
    en = @uc.end_time.month
    year = @uc.start_time.year

    year_interval = 0
    if @uc.end_time.year > @uc.start_time.year
      year_interval = @uc.end_time.year - @uc.start_time.year
      en = en + 12 * year_interval
    end

    @count = 0
    type = params[:brokentype] || 0
    #filter by broken type [0:broken, 1:adjustd]
    if type.to_i  == 0
      adjusted_price = "adjusted_price = filled_price and is_cee = false"
      @brokentype = 0
    elsif type.to_i == 1      
      adjusted_price = "adjusted_price != filled_price and is_cee = false"
      @brokentype = 1
    elsif type.to_i == 2 && (@uc.trade_date > CEE_DATE)
      adjusted_price = " filing_status is not null"
      @brokentype = 2
    end

    hash_status = { "0" => "All", "1" => "F", "2" => "CF", "3"=>"MR"}
    @filing_status = "0"
    @filing_status = params[:status].strip.to_s if params[:status]
    if @filing_status != "0"
      adjusted_price = "#{adjusted_price} and filing_status = '#{hash_status[@filing_status]}' "
    end

    @count = 0
    @brokens = []
    #add white loop to process multi month for ticket 1108
    while st <= en
      db_name_suffix = "#{Time.now.year}#{Time.now.month.to_s.rjust(2,'0')}"
      if st <= 12
        db_name_suffix = "#{year}#{st.to_s.rjust(2,'0')}"
      else
        #p "year_interval : #{year_interval}"
        if year_interval == 1
          db_name_suffix = "#{year + 1}#{(st - 12).to_s.rjust(2,'0')}"
        elsif year_interval == 2
          if st <= 24
            db_name_suffix = "#{year + 1}#{(st - 12).to_s.rjust(2,'0')}"
          else
            db_name_suffix = "#{year + 2}#{(st - 24).to_s.rjust(2,'0')}"
          end
        end
      end

      get_database_connection(db_name_suffix, "as")
      st = st + 1

      if params[:undo] && params[:broken_id]
        BrokenTrade.find_by_sql(["delete from brokens where id=?",params[:broken_id]])
      end

      @count_t = BrokenTrade.count(:conditions=>["#{adjusted_price} and trade_date between ? and ? ", @uc.start_time, @uc.end_time])
        #@offset = (@count - 1) / @items_per_page * @items_per_page if @page * @items_per_page > @count
      @brokens_t = BrokenTrade.find(:all, 
                    :conditions=>["#{adjusted_price} and trade_date between ? and ? ", @uc.start_time, @uc.end_time], 
                    :offset=>@offset, 
                    :limit => @items_per_page, 
                    :order=>"#{@order_field} #{@dir}")

      @count = @count + @count_t
      @brokens_t.each { |x|
        @brokens << x
      }

    end

    if @order_field == 'cl_order_id' 
      if @dir == 'asc'
        @brokens = @brokens.sort {|x,y| x.cl_order_id <=> y.cl_order_id }
      else
        @brokens = @brokens.sort {|x,y| y.cl_order_id <=> x.cl_order_id }
      end
    end
    if @order_field == 'symbol' 
      if @dir == 'asc'
        @brokens = @brokens.sort {|x,y| x.symbol <=> y.symbol }
      else
        @brokens = @brokens.sort {|x,y| y.symbol <=> x.symbol }
      end
    end

    if @order_field == 'side' 
      if @dir == 'asc'
        @brokens = @brokens.sort {|x,y| x.side <=> y.side }
      else
        @brokens = @brokens.sort {|x,y| y.side <=> x.side }
      end
    end
    if @order_field == 'filled_shares' 
      if @dir == 'asc'
        @brokens = @brokens.sort {|x,y| x.filled_shares <=> y.filled_shares }
      else
        @brokens = @brokens.sort {|x,y| y.filled_shares <=> x.filled_shares }
      end
    end

    if @order_field == 'filled_price' 
      if @dir == 'asc'
        @brokens = @brokens.sort {|x,y| x.filled_price <=> y.filled_price }
      else
        @brokens = @brokens.sort {|x,y| y.filled_price <=> x.filled_price }
      end
    end
    if @order_field == 'adjusted_price' 
      if @dir == 'asc'
        @brokens = @brokens.sort {|x,y| x.adjusted_price <=> y.adjusted_price }
      else
        @brokens = @brokens.sort {|x,y| y.adjusted_price <=> x.adjusted_price }
      end
    end

    if @order_field == 'ecn' 
      if @dir == 'asc'
        @brokens = @brokens.sort {|x,y| x.ecn <=> y.ecn }
      else
        @brokens = @brokens.sort {|x,y| y.ecn <=> x.ecn }
      end
    end
    if @order_field == 'trade_date' 
      if @dir == 'asc'
        @brokens = @brokens.sort {|x,y| x.trade_date <=> y.trade_date }
      else
        @brokens = @brokens.sort {|x,y| y.trade_date <=> x.trade_date }
      end
    end

    if @order_field == 'trade_time' 
      if @dir == 'asc'
        @brokens = @brokens.sort {|x,y| x.trade_time <=> y.trade_time }
      else
        @brokens = @brokens.sort {|x,y| y.trade_time <=> x.trade_time }
      end
    end
    if @order_field == 'filing_status' 
      if @dir == 'asc'
        @brokens = @brokens.sort {|x,y| x.filing_status <=> y.filing_status }
      else
        @brokens = @brokens.sort {|x,y| y.filing_status <=> x.filing_status }
      end
    end
    if @order_field == 'cee_note' 
      if @dir == 'asc'
        @brokens = @brokens.sort {|x,y| x.cee_note <=> y.cee_note }
      else
        @brokens = @brokens.sort {|x,y| y.cee_note <=> x.cee_note }
      end
    end

    @object_pages = WillPaginate::Collection.new(@page, @items_per_page, @count)

  end
  #================================================================================================
  # Function    : broken (action controller)
  # Input        : 
  # Description      : get uc
  # Developer      : 
  # Date      : 
  #================================================================================================
  def position
    #get user context
    @uc = get_uc
    render :layout=>"interface2"
  end
  #================================================================================================
  # Function    : broken (action controller)
  # Input        : 
  # Description      : get uc
  # Developer      : 
  # Date      : 
  #================================================================================================
  def show_position
    #get user context 
    @uc = get_uc
    #sort order
    @dir = params[:dir] || "asc"
    #sort field
    @order_field = params[:order_field] || "symbol"

    #tonggle sort order
    if @dir == "asc"
      @dir = "desc"
    else
      @dir = "asc"
    end

    account_value = params[:account_value].to_i
    @AccountSelect = account_value

    #take trade date as format mm/dd/yyyy
    if params[:trade_date]
      @uc.trade_date = str_to_unix_time(params[:trade_date])
    end

    #Check start_time, end_time more than Time now
    #check_datetime
    #@last_trade_date ="2008-01-30"
    @last_trade_date = RiskManagement.find_by_sql(["select check_allow_modify_long_short();"]).first.check_allow_modify_long_short
    #p "@last_trade_date: #{@last_trade_date}"
    if  @last_trade_date == nil
      @last_trade_date  = Time.now
    end
    #Set up connection to database which wants to use
    db_name_suffix = "#{@last_trade_date.to_date.year}#{@last_trade_date.to_date.month.to_s.rjust(2,'0')}"
    #Connect to a database on remote server
    connect_to_database(db_name_suffix)

    #Update long short
    if params[:updateLongShort] && params[:price].to_f > 0.0
      # check inputs ...

      # Update long short
      # query_str = "update risk_managements set net_quantity = #{(params[:shares].to_i * params[:side_select].to_i).to_s}, total_quantity = #{(params[:shares].to_i).to_s}  "
      # query_str = "#{query_str}, average_price = #{params[:price]} "
      # query_str = "#{query_str}, total_market_value = #{(params[:price].to_f * params[:shares].to_i).to_s} "
      # query_str = "#{query_str}, account = #{account_value}"
      # query_str = "#{query_str} where id = #{params[:id]}"

      # RiskManagement.find_by_sql([query_str])
      # RiskManagement.find_by_sql("UPDATE trading_status SET rmupdated = 1")

      RiskManagement.find_by_sql(["select create_new_long_short(?, ?, ?, ?, ?, ?);", @last_trade_date, params[:symbol].to_s, params[:side_select].to_i, params[:shares].to_i, params[:price].to_f , account_value])
    end

    @buy_sell =[["Buy", 1],["Sell", -1]]





    @edit_long_short = nil
    if params[:edit]
      #@edit_long_short = OpenPosition.find_by_sql(["select * from risk_managements where id=?",params[:edit_id]])
      @edit_long_short = RiskManagement.find(  :all,
                      :conditions=>["id = ?", params[:edit_id]]
                      )
      #puts @edit_long_short.first.symbol
    end

    @add_long_short = nil
    if params[:createLongShortSubmit]
      @add_long_short = true
    end

    if params[:insertLongShortSubmit] && params[:symbol] && params[:symbol].strip != "" && params[:shares].to_i > 0 && params[:price].to_f > 0
      # Check symbol
      # Insert long short
      RiskManagement.find_by_sql(["select create_new_long_short(?, ?, ?, ?, ?, ?);", @last_trade_date, params[:symbol], params[:side_select].to_i, params[:shares].to_i, params[:price].to_f,account_value])
      @add_long_short = true
    end

    #delete_long_short 
    if params[:del]
      RiskManagement.find_by_sql(["select delete_long_short(?);",params[:del_id].to_i])

    end

    #puts @last_trade_date.to_s
    #p "@last_trade_date ffffffff: #{@last_trade_date.to_date}"
    @long_shorts = RiskManagement.find(  :all,
                      :conditions=>["date=? and net_quantity <> 0 ",@last_trade_date.to_date],
                      :order=>"#{@order_field} #{@dir}"
                      )
  end

  #================================================================================================
  # Function    : menu_refresh (action controller)
  # Input        : N/A
  # Date        : 2010/02/25
  #================================================================================================
  def menu_refresh
    @uc = get_uc
    if params[:start_time]
      @uc.start_time = str_to_unix_time(params[:start_time])
    end
    if params[:end_time]
      @uc.end_time = str_to_unix_time(params[:end_time])
    end
  end

  #================================================================================================
  # Function    : xls_export
  # Input        :   
  # Description  : export data to xls file
  # Developer    : TuanBD
  # Modifire     : Khanhtt
  # Date created: August, 12 2010
  # Date updated: September 19, 2011
  #================================================================================================
  def xls_export
    #get user context
    @uc = get_uc
    
    #sort order
    @dir = params[:dir] || "asc"
    #sort field
    @order_field = params[:sort_field] || "trade_time"
    #take trade date as format mm/dd/yyyy
    if params[:trade_date]
      @uc.trade_date = str_to_unix_time(params[:trade_date])
    end

    if params[:symbol] && params[:symbol].to_s != ""
      @uc.symbol = params[:symbol].strip
    else
      @uc.symbol = ""
    end
    @uc.trade_start_time =  params[:trade_start_time] if params[:trade_start_time]
    @uc.trade_end_time =  params[:trade_end_time] if params[:trade_end_time]
    @uc.trade_start_time = @uc.trade_start_time || "02:00"
    @uc.trade_end_time = @uc.trade_end_time || "19:00"
    filename_xls_export = [@uc.trade_date.strftime("%Y%m%d").to_s]
    
    #Check start_time, end_time more than Time now
    check_datetime
    #filter by time_stamp
    conditions = ["date = ?", "trade_time between ? and ?"]
    values = [@uc.trade_date, @uc.trade_start_time, @uc.trade_end_time]

    #filter by ECN order
    if params[:ecn] && params[:ecn].to_s != ""
      @uc.ecn = params[:ecn]
    end
    if @uc.ecn && @uc.ecn != "0"
      conditions << "ecn = ?"
      values << @uc.ecn
      filename_xls_export << @uc.ecn.to_s
    else
      @uc.ecn = "0"
    end
    filename_xls_export << @uc.trade_start_time.to_s
    filename_xls_export << @uc.trade_end_time.to_s
    
    if params[:symbol] && params[:symbol].to_s != ""
      @uc.symbol = params[:symbol].upcase.strip
      conditions << " symbol = ? "
      values << @uc.symbol      
      filename_xls_export << @uc.symbol
    else
      @uc.symbol = nil
    end

    #Filer by filled shares
    @fill_share = params[:fill_share].to_i || 0
    if @fill_share.to_i > 0
      conditions << " filled_shares >= ? "
      values << @fill_share.to_i 
      filename_xls_export << @fill_share.to_s
    end
    
    if params[:price_from]  && params[:price_from].to_f > 0
      @uc.price_from = params[:price_from]
      conditions << " filled_price >= ? "
      values << @uc.price_from
      filename_xls_export << @uc.price_from.to_s
    else
      @uc.price_from = nil
    end
    
    #Filter  by filled price_to
    if params[:price_to] && params[:price_to].to_f > 0
      @uc.price_to = params[:price_to]
      conditions << " filled_price <= ? "
      values << @uc.price_to
      filename_xls_export << @uc.price_to.to_s
    else
      @uc.price_to = nil
    end
    #Filter by Buy/Sell
    @buysell = params[:buysell] || 0
    if @buysell.to_i > 0
      if @buysell.to_i == 2
        conditions << "(action = ?)"
        values << "S"
        filename_xls_export << "Sell"
      elsif @buysell.to_i == 3
        conditions << "(action = ?)"
        values << "SS"
        filename_xls_export << "ShortSell"
      elsif @buysell.to_i == 1
        conditions << "(action = ? or action = ?)"
        values << "BC"
         values << "BO"
        filename_xls_export << "Buy"
      end
    end
    conditions << " fill_id not in (select fill_id from brokens where brokens.is_cee = 'FALSE')"

    condition_str = conditions.join(" and ")
    conditions = []
    conditions << condition_str
    conditions += values
      
    @tradebreaks = TradeBreak.find(  :all,
      :conditions=>conditions,      
      :order=>"#{@order_field} #{@dir}, trade_time asc "
    )
    if !@tradebreaks.empty?
        filename_str = filename_xls_export.join("_")
        column_names = ["ClOrdID", "Symbol", "Filled Shares", "Filled Price", "ECN", "Type", "Trade Time"]
        @file_path = ExportFile.export_file_to_xls(column_names, @tradebreaks, "Trade_Break", filename_str)       
        if File.exists?(@file_path)
          send_file @file_path, :filename => File.basename(@file_path)
        else
          render :text => "File not found"  
        end
    else
        render :text => "No data for export file."
    end

  
  end
  
  #================================================================================================
  # Function  name: xls_export_broken
  # Input        : 
  # Description  : 
  # Developer    : TuanBD, KhanhTT
  # Date created: August, 12 2010
  # Date edited: May 25, 2012
  #================================================================================================
  def xls_export_broken
    #get user context
    @uc = get_uc

    #sort order
    @dir = params[:dir] || "asc"
    #sort field
    @order_field = params[:sort_field] || "trade_time"

    #take trade date as format mm/dd/yyyy
    if params[:start_time]
      @uc.start_time = str_to_unix_time(params[:start_time])
    end
    if params[:end_time]
      @uc.end_time = str_to_unix_time(params[:end_time])
    end
    filename_xls_export = [@uc.trade_date.strftime("%Y%m%d").to_s]

    st = @uc.start_time.month
    en = @uc.end_time.month
    year = @uc.start_time.year

    year_interval = 0
    if @uc.end_time.year > @uc.start_time.year
      year_interval = @uc.end_time.year - @uc.start_time.year
      en = en + 12 * year_interval
    end


    @count = 0
    @brokens = []
    #filter by broken type [0:broken, 1:adjustd, 2:filed]
    type = params[:brokentype] || 0
    #filter by broken type [0:broken, 1:adjustd]
    if type.to_i  == 0
      adjusted_price = "adjusted_price = filled_price and is_cee = false"
      @brokentype = 0
      filename_xls_export << "Broken"
    elsif type.to_i == 1      
      adjusted_price = "adjusted_price != filled_price and is_cee = false"
      @brokentype = 1
      filename_xls_export << "Adjusted"
    elsif type.to_i == 2 && (@uc.trade_date > CEE_DATE)
      adjusted_price = "filing_status is not null"
      @brokentype = 2
      filename_xls_export << "Filed"
    end
    
    hash_status = { "0" => "All", "1" => "F", "2" => "CF", "3"=>"MR"}
    @filing_status = "0"
    @filing_status = params[:status].strip.to_s if params[:status]
    if @filing_status != "0"
      adjusted_price = "#{adjusted_price} and filing_status = '#{hash_status[@filing_status]}' "
    end

    while st <= en
      db_name_suffix = "#{Time.now.year}#{Time.now.month.to_s.rjust(2,'0')}"
      if st <= 12
        db_name_suffix = "#{year}#{st.to_s.rjust(2,'0')}"
      else
        #p "year_interval : #{year_interval}"
        if year_interval == 1
          db_name_suffix = "#{year + 1}#{(st - 12).to_s.rjust(2,'0')}"
        elsif year_interval == 2
          if st <= 24
            db_name_suffix = "#{year + 1}#{(st - 12).to_s.rjust(2,'0')}"
          else
            db_name_suffix = "#{year + 2}#{(st - 24).to_s.rjust(2,'0')}"
          end
        end
      end

      get_database_connection(db_name_suffix, "as")
      st = st + 1

      @brokens_t = BrokenTrade.find(:all, 
                                :conditions=>["#{adjusted_price} and trade_date between ? and ?", @uc.start_time, @uc.end_time], 
                                :order=>"#{@order_field} #{@dir}")

      @brokens_t.each { |x|
        @brokens << x
      }
    end

    if !@brokens.empty?
        filename_str = filename_xls_export.join("_")
        column_names = ["ClOrdID", "Symbol", "Action", "Filled Shares", "Filled Price", "ECN", "Date","Trade Time", "Filing Status", "CEE Note"]
        if @brokentype == 1
          column_names = ["ClOrdID", "Symbol", "Action", "Filled Shares", "Filled Price", "Adjusted Price", "ECN", "Date","Trade Time", "Filing Status", "CEE Note"]
          @file_path = ExportFile.export_file_to_xls(column_names, @brokens, "Adjusted_Price", filename_str)
        else
          @file_path = ExportFile.export_file_to_xls(column_names, @brokens, "Broken_Trade", filename_str)
        end

        if File.exists?(@file_path)
        send_file @file_path, :filename => File.basename(@file_path)
      else
        render :text => "File not found"  
      end
    else
      render :text => "No data for export file." 
    end
  end
  #================================================================================================
  # Function Name : get_filing_status
  # Input          : fill_id
  # Developer     : KhanhTT
  # Date Created  : September 12, 2011
  #================================================================================================
  def get_filing_status

    @uc = get_uc

    #take trade date as format mm/dd/yyyy
    if params[:trade_date]
      @uc.trade_date = str_to_unix_time(params[:trade_date])
    end

    if params[:symbol] && params[:symbol].to_s != ""
      @uc.symbol = params[:symbol].strip
    else
      @uc.symbol = ""
    end

    @uc.trade_start_time =  params[:trade_start_time] if params[:trade_start_time]
    @uc.trade_end_time =  params[:trade_end_time] if params[:trade_end_time]
    @uc.trade_start_time = @uc.trade_start_time || "02:00"
    @uc.trade_end_time = @uc.trade_end_time || "19:00"

    #Check start_time, end_time more than Time now
    check_datetime

    #filter by time_stamp
    conditions = ["date = '#{@uc.trade_date}' ", "and (trade_time between '#{@uc.trade_start_time}' and '#{@uc.trade_end_time}') "]
    
    #filter by ECN order
    if params[:ecn] && params[:ecn].to_s != ""
      @uc.ecn = params[:ecn]
    else
        @uc.ecn = "0"
    end
    if @uc.ecn && @uc.ecn != "0"
      conditions << "and ecn = '#{@uc.ecn}' "
    end
    
    if params[:price_from]  && params[:price_from].to_f > 0
      @uc.price_from = params[:price_from]
      conditions << "and filled_price >= #{@uc.price_from} "
    else
      @uc.price_from = nil
    end

    if params[:symbol] && params[:symbol].to_s != ""
       @uc.symbol = params[:symbol].upcase.strip
      conditions << "and symbol = '#{@uc.symbol}' "
    else
      @uc.symbol = nil
    end
    #Filer by filled shares
    @fill_share = params[:fill_share].to_i || 0

    if @fill_share.to_i > 0
      conditions << " and filled_shares >= #{@fill_share.to_i} "
    end
    #Filter by Buy/Sell
    @buysell = params[:buysell] || 0
    if @buysell.to_i > 0
      if @buysell.to_i == 2
        conditions << " and (action = 'S') "

      elsif @buysell.to_i == 3
        conditions << " and (action = 'SS') "
      else
        conditions << "and (action = 'BC' or action = 'BO') "
      end
    end
    #Filter  by filled price_to
    if params[:price_to] && params[:price_to].to_f > 0
      @uc.price_to = params[:price_to]
      conditions << " and filled_price <= #{@uc.price_to} "
    else
      @uc.price_to = nil
    end
    conditions << "and fill_id not in (select fill_id from brokens where is_cee = 'f') "

    sql_str = "SELECT array_to_string(array_agg(fill_id), ',') as fill_ids FROM (SELECT * FROM vwtrade_breaks where #{conditions}) as temp"
    fill_ids = "0"

    @data = TradeBreak.find_by_sql("#{sql_str}")

    list_fill_ids = "(0"

    @data.each { |x|
      list_fill_ids << "," 
      list_fill_ids << x.fill_ids.to_s
    }
    list_fill_ids << ")"

    @type_num = params[:type_num]

    if list_fill_ids.size > 4 then # ignore (0,)
      info = Order.find_by_sql("select * from brokens where fill_id in #{list_fill_ids}")
    end

    flag_first = 1
    status_check = ""
    note_check = ""
    flag_error = 0

    if !info.nil? && (list_fill_ids.split(",").size - 1) == info.size
      info.each { |value|
        if flag_first == 1
          status_check = value.filing_status.to_s
          note_check = value.cee_note.to_s
          flag_first = 0
        else
          if (value.filing_status.to_s != status_check.to_s) or (note_check.to_s != value.cee_note.to_s)
            flag_error = 1
            break
          end
        end
      }
    else
      flag_error=0
    end


    @cee_info = []
      
    status_mapping = {}
    filing_status_list().each{ |status|
      status_mapping[status[0]] = status[1]
    }

    if !info.nil? && status_check != "" && flag_error == 0
      @cee_info << status_mapping[status_check.strip.to_s]
      @cee_info << note_check
    else
      @cee_info << 0
      @cee_info << ""
    end
    render :partial=>"filing_status"
  end

  def hung_order 
    @uc = get_uc
    if params[:symbol] then
      @symbol = params[:symbol].to_s
      @uc.symbol = @symbol
      @uc.trade_date = Time.mktime(Time.now.year, Time.now.month, Time.now.mday)
    end
    render :layout=>"interface2"
  end

  def show_hung_order
    @uc = get_uc
    @page = params[:page] && params[:page].to_i > 0 ? params[:page].to_i : 1

    @items_per_page = 100
    @offset = (@page - 1) * @items_per_page
    @dir = params[:dir] || "asc"
    @order_field = params[:sort_field] || "time_stamp"

    @buysell = params[:side].to_i || 0
    @symbol = params[:symbol].to_s || ""
    if params[:trade_date]
      @uc.trade_date = str_to_unix_time(params[:trade_date])
    end

    @last_trade_date = Order.find_by_sql(["select max(date) from new_orders where status = 'Sent' "]).first
    #p "@last_trade_date: #{@last_trade_date}"
    if  @last_trade_date == nil
      @last_trade_date  = Time.now
    end
    sql_str = "select * from new_orders where status like 'S%'"
    if @symbol.size > 0 then
      sql_str << " and symbol = '#{@symbol}'"
    end

    if @buysell.to_i != 0 then
      buysells=["B","S","SS"]
      if @buysell.to_i > 1 then
        sql_str << " and side = '#{buysells[@buysell.to_i - 1]}'"
      else
        sql_str << " and side like 'B%' "
      end

    end

    @uc.trade_date = Time.mktime(Time.now.year, Time.now.month, Time.now.mday)
    trade_date = @uc.trade_date.strftime("%Y-%m-%d")
    @last_trade_date = trade_date

    sql_str << " and date = '#{trade_date}'"
    db_name_suffix = "#{@uc.trade_date.year}#{@uc.trade_date.month.to_s.rjust(2,'0')}"
    #Connect to a database on remote server
    connect_to_database(db_name_suffix)

    if @dir == "asc"
      @dir = "desc"
    else
      @dir = "asc"
    end
    sql_str << "ORDER BY #{@order_field} #{@dir} "
    @hung_orders = Order.find_by_sql(sql_str)
    @count = @hung_orders.size
    @object_pages = WillPaginate::Collection.new(@page, @items_per_page, @count)
  end

  def cancel_all_checked
    list_order_id = params[:list_order].to_s

    ret = Order.find_by_sql(["select cancel_all_hung_order(?)", list_order_id])
    result = -1
    ret.each { |x|
      result = x.cancel_all_hung_order
    }
    if result.to_i == 0 then
      render :js =>"show_msg_result('Cancel all checked OrderID successfully.');"
    else

      render :js =>"show_msg_result('Unsuccessful with some orders ');"
    end
  end

  def order_info
    @uc = get_uc
    @count_fill = 0
    oid = params[:oid]
    @uc.trade_date = Time.mktime(Time.now.year, Time.now.month, Time.now.mday)
    trade_date = @uc.trade_date.strftime("%Y-%m-%d")
    @an_order = Order.find_by_sql("select * from new_orders where oid = #{oid} and date = '#{@uc.trade_date}';")

    render :partial =>"order_info"
  end

  def process_fill_cancel
    oid = params[:oid].to_i
    list_shares_fill = params[:fill_shares_list].to_s
    list_price_fill = params[:fill_price_list].to_s
    cancel_shares = params[:cancel_shares].to_i

    # process insert fills
    if list_shares_fill.size > 0 then
      list_shares_fill[0] = ""
    end

    if list_price_fill.size > 0 then
      list_price_fill[0] = ""
    end

    if list_shares_fill.size > 0
      ret1 = Order.find_by_sql(["select insert_fill_hung_order(?, ? , ? )", oid , list_shares_fill , list_price_fill ])
    end
    # process insert cancels

    if params[:cancel_shares].strip != ""
      ret2 = Order.find_by_sql(["select insert_cancel_hung_order(?, ? )", oid, cancel_shares])
    end
  end


end
