
class HomeController < ApplicationController
  def index
  #-------------- Variables declaration ------------------------------------
  @uc = get_uc
  @color_config = get_color_config

  #check time 
  check_datetime

  #Set up database connection
  connect_to_database(get_db_suffix)

  #-------------- Get current trading date----------------------------------
  @last_date_trading =  RiskManagement.find_by_sql("select max(date) as trade_date from new_orders" ).first.trade_date
  #check trade_date is null
  if @last_date_trading == nil
    @last_date_trading = Time.now.strftime("%Y-%m-%d")
  end

  if params[:trade_date]
    @last_date_trading = str_to_unix_time(params[:trade_date])
    if @last_date_trading.year >= 2008 &&  @last_date_trading.to_date <= Time.now.to_date
      @last_date_trading = @last_date_trading.strftime("%Y-%m-%d")
    else
      @last_date_trading = Time.mktime(Time.now.year, Time.now.month, Time.now.mday).strftime("%Y-%m-%d")
    end

  end
  @uc.trade_date = Time.mktime(@last_date_trading.to_date.year,@last_date_trading.to_date.month,@last_date_trading.to_date.day)
  @uc.last_date_trading = @uc.trade_date
  #Set up database connection
  connect_to_external_database(get_db_suffix1(@last_date_trading))

  #-------------- Get data for Profit/Loss chart ---------------------------
  @profit_loss = ProfitLoss.find(:all, :conditions => ["date=?", @last_date_trading], :order => "id ASC")

  #-------------- Get data for Entry/exit chart ----------------------------
  sql="select time_stamp, ts_id, sum(entry_filled) as entry_filled, sum(entry_total) as entry_total, sum(exit_filled) as exit_filled, sum(exit_total) as exit_total from summary_entry_exits_15 where date = '#{@last_date_trading}'"
  group_by = " group by time_stamp, ts_id "
  order_by = " order by time_stamp ASC "

  @entry_exit_ts1 = EntryExit.find_by_sql(sql + " and ts_id =0 " + group_by + order_by)
  @entry_exit_ts2 = EntryExit.find_by_sql(sql + " and ts_id =1 " + group_by + order_by)
  @entry_exit_ts3 = EntryExit.find_by_sql(sql + " and ts_id =2 " + group_by + order_by)
  @entry_exit_ts4 = EntryExit.find_by_sql(sql + " and ts_id =3 " + group_by + order_by)
  @entry_exit_ts5 = EntryExit.find_by_sql(sql + " and ts_id =4 " + group_by + order_by)
  @entry_exit_ts6 = EntryExit.find_by_sql(sql + " and ts_id =5 " + group_by + order_by)
  @entry_exit_ts7 = EntryExit.find_by_sql(sql + " and ts_id =6 " + group_by + order_by)
  @entry_exit_ts8 = EntryExit.find_by_sql(sql + " and ts_id =7 " + group_by + order_by)
  @entry_exit_ts9 = EntryExit.find_by_sql(sql + " and ts_id =8 " + group_by + order_by)
  @entry_exit_all = EntryExit.find_by_sql("select time_stamp, sum(entry_filled) as entry_filled, sum(entry_total) as entry_total, sum(exit_filled) as exit_filled, sum(exit_total) as exit_total from summary_entry_exits_15 where date = '#{@last_date_trading}'  group by time_stamp " + order_by)

  #Specify the layout to render
  render :layout => "interface2"

  end

  def trade_statistic

  @include_repriced = params[:include_repriced]
  #puts "@include_repriced = " + @include_repriced
  @color_config = get_color_config
  @uc = get_uc

  #Set up database connection
  connect_to_database(get_db_suffix)
  trade_date =  RiskManagement.find_by_sql("select max(date) as trade_date from new_orders" ).first.trade_date
  if params[:trade_date]
    trade_date = str_to_unix_time(params[:trade_date])
    if trade_date.year >= 2008 &&  trade_date.to_date <= Time.now.to_date
      trade_date = trade_date.strftime("%Y-%m-%d")
    else
      trade_date = Time.mktime(Time.now.year, Time.now.month, Time.now.mday).strftime("%Y-%m-%d")
    end
  end

  #Set up database connection
  connect_to_database(get_db_suffix1(trade_date))

  #-------------- Get trade statistic summary information  -----------------

  start_time = Time.mktime(trade_date.split("-")[0].to_i,trade_date.split("-")[1].to_i, trade_date.split("-")[2].to_i, 6)
  end_time =   Time.mktime(trade_date.split("-")[0].to_i,trade_date.split("-")[1].to_i, trade_date.split("-")[2].to_i,21)

  # Home Pie Chart only get data with real time and direct data.
  trade_start_time = "0:00"
  trade_end_time = "23:59"

  @trade_statistics_pie = TradeResult.filter_trade_statistic_twt2_2(start_time, start_time, trade_start_time, trade_end_time, '0', 0, '', - 1, - 1, '0', '0', '0', false, '0', 0, '', '0')

  render :layout => false

  end

  def get_db_suffix
  return "#{Time.now.year}#{Time.now.month.to_s.rjust(2,'0')}"
  end
  def get_db_suffix1(trade_date)
  return trade_date.split("-")[0] + trade_date.split("-")[1]
  end

end
