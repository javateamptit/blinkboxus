class FillRateController < ApplicationController

 def index  
  @uc = get_uc
  @uc.entry_exit = "1"
  #Specify the layout to render
  @last_date_trading =  RiskManagement.find_by_sql("select max(date) as trade_date from new_orders" ).first.trade_date  
  #check trade_date is null
  if @last_date_trading == nil
    @last_date_trading = Time.now.strftime("%Y-%m-%d")
  end
  @check_date = Time.mktime(@last_date_trading.to_date.year,@last_date_trading.to_date.month,@last_date_trading.to_date.day)
  @uc.last_date_trading = @check_date 
  if @uc.end_time > @check_date
    @uc.end_time = @check_date  
  end
  if @uc.start_time > @check_date
    @uc.start_time = @check_date  
  end
  render :layout => "interface2"
 end

 def index2
  @uc = get_uc
  @last_date_trading =  RiskManagement.find_by_sql("select max(date) as trade_date from new_orders" ).first.trade_date  
  #check trade_date is null
  if @last_date_trading == nil
    @last_date_trading = Time.now.strftime("%Y-%m-%d")
  end
  @check_date = Time.mktime(@last_date_trading.to_date.year,@last_date_trading.to_date.month,@last_date_trading.to_date.day)
  @uc.last_date_trading = @check_date 
  if @uc.end_time > @check_date
    @uc.end_time = @check_date  
  end
  if @uc.start_time > @check_date
    @uc.start_time = @check_date  
  end

  #Specify the layout to render
  render :layout => "interface2"
 end
 
  def index3
  @uc = get_uc
  @last_date_trading =  RiskManagement.find_by_sql("select max(date) as trade_date from new_orders" ).first.trade_date  
  #check trade_date is null
  if @last_date_trading == nil
    @last_date_trading = Time.now.strftime("%Y-%m-%d")
  end
  @check_date = Time.mktime(@last_date_trading.to_date.year,@last_date_trading.to_date.month,@last_date_trading.to_date.day)
  @uc.last_date_trading = @check_date 
  if @uc.end_time > @check_date
    @uc.end_time = @check_date  
  end
  if @uc.start_time > @check_date
    @uc.start_time = @check_date  
  end

  #Specify the layout to render
  render :layout => "interface2"
 end
 
 
  def fill_rate_by_shares
  @uc = get_uc
  @last_date_trading =  RiskManagement.find_by_sql("select max(date) as trade_date from new_orders" ).first.trade_date  
  #check trade_date is null
  if @last_date_trading == nil
    @last_date_trading = Time.now.strftime("%Y-%m-%d")
  end
  @check_date = Time.mktime(@last_date_trading.to_date.year,@last_date_trading.to_date.month,@last_date_trading.to_date.day)
  @uc.last_date_trading = @check_date 
  if @uc.end_time > @check_date
    @uc.end_time = @check_date  
  end
  if @uc.start_time > @check_date
    @uc.start_time = @check_date  
  end

  #Specify the layout to render
  render :layout => "interface2"
 end


  def index_ajax
    
      @uc = get_uc
      @color_config = get_color_config

      trade_start_time = params[:trade_start_time]
      trade_end_time = params[:trade_end_time]
      start_time = params[:start_time]
      end_time = params[:end_time]
    iso_order = params[:iso_order]
    iso_order = iso_order.to_i

      @step = params[:step_type].to_i || 5  
      date_range = params[:date_range].to_i

    if start_time
      @uc.start_time = str_to_unix_time(params[:start_time])
      end

      if end_time
      @uc.end_time = str_to_unix_time( params[:end_time])
      end
   
      @uc.trade_start_time =  params[:trade_start_time] if params[:trade_start_time]
      @uc.trade_end_time =  params[:trade_end_time] if params[:trade_end_time]

      @uc.step_type = params[:step_type]

      #check date time: if date is chosen more than now date, it will return now date 
      check_datetime
      @uc.ts = params[:ts] || "0"
      @uc.ecn_entry = params[:ecn_entry]  || "99" 
    @uc.entry_exit = params[:entry_exit] || "1"
    @uc.side = params[:side] || "3"
    #p @uc.side
    #p params[:entry_exit]
      
      
      @uc.img_view_type = params[:img_view_type] if params[:img_view_type]
      @uc.img_view_step= params[:img_view_step] if params[:img_view_step]

      ts_list = @uc.ts.gsub(/\d+/){|ts_id| ts_id.to_i - 1}
    @summary = NewOrder.get_entry_exit(@uc.start_time, @uc.end_time, @uc.trade_start_time, @uc.trade_end_time, ts_list, @uc.trade_type.to_i, @uc.side.to_i,iso_order)

      #-------------- Get data for Entry/exit in CSV format ---------------------------
      @summary_fill_rate = FillRate.get_entry_exit_order_type_fill_rate(trade_start_time, trade_end_time, @uc.start_time, @uc.end_time, @uc.ecn_entry , @uc.entry_exit.to_i, @uc.side.to_i, @step,date_range)
      
    
    @ecn_f_id = @uc.ecn_entry.to_i
      # @ts_filter_id = @uc.ts.to_i # LuuNN: Need to change
    @isEntryExit = @uc.entry_exit.to_i
    @side_ind = @uc.side.to_i

    end
 
  def index_ajax2
  
    @uc = get_uc
    @color_config = get_color_config
    
    trade_start_time = params[:trade_start_time]
    trade_end_time = params[:trade_end_time]
    start_time = params[:start_time]
    end_time = params[:end_time]
    launch_type = params[:launch_type]
    iso_order = params[:iso_order]
    iso_order = iso_order.to_i
    @step = params[:step_type].to_i || 5  
    date_range = params[:date_range].to_i

    if params[:start_time]
        @uc.start_time = str_to_unix_time(params[:start_time])
      end
      if params[:end_time]
        @uc.end_time = str_to_unix_time( params[:end_time])
      end

      @uc.trade_start_time =  params[:trade_start_time] if params[:trade_start_time]
      @uc.trade_end_time =  params[:trade_end_time] if params[:trade_end_time]  
      @uc.step_type = params[:step_type]
    #@uc.entry_exit = entry_exit
      
    #check date time: if date is chosen more than now date, it will return now date 
    check_datetime
    @uc.ts = params[:ts]  || "0"
      @uc.ecn_entry = params[:ecn_entry] || "99"
      @uc.side = params[:side] || "3"

      @uc.img_view_type = params[:img_view_type] if params[:img_view_type]
      @uc.img_view_step= params[:img_view_step] if params[:img_view_step]
    
    # LuuNN add multi trade servers
      ts_list = @uc.ts.gsub(/\d+/){|ts_id| ts_id.to_i - 1}
    @summary = NewOrder.get_entry_exit_launch(@uc.start_time, @uc.end_time, @uc.trade_start_time, @uc.trade_end_time, ts_list, @uc.trade_type.to_i, @uc.side.to_i, launch_type,iso_order)

    #-------------- Get data for Fill Rates Charts ----------------------------
    @summary_fill_rate = FillRate.get_launch_entry_fill_rate(trade_start_time, trade_end_time, @uc.start_time, @uc.end_time, @uc.ecn_entry.to_i,@uc.side.to_i,  @step, date_range)
    # @ts_filter_id = @uc.ts.to_i
    @ecn_entry = @uc.ecn_entry.to_i
    @ecn_launch_filter = launch_type
  
    end

  #================================================================================================
  # Function    : menu_refresh (action controller)
  # Input       : N/A
  # Date        : 2010/02/25
  #================================================================================================
  
  def entry_exit_fill_rate_by_time
    @uc = get_uc
    
    @color_config = get_color_config
    start_time = params[:start_time]
    end_time = params[:end_time]
    @step = params[:step_type].to_i || 5
    date_range = params[:date_range].to_i
    launch_type = params[:launch_type].to_i
    if start_time
      start_time = str_to_unix_time(start_time)
      start_time += 2*60*60
      @uc.start_time = start_time
    else
      @uc.start_time = Time.mktime(@uc.start_time.year, @uc.start_time.mon, @uc.start_time.mday, 2)
    end
    if end_time
      end_time = str_to_unix_time(end_time)
      @uc.end_time = end_time + 19*60*60
    else
      @uc.end_time = Time.mktime(@uc.end_time.year, @uc.end_time.mon, @uc.end_time.mday, 19)
    end
    #Use to Check start_time, end_time more than Time now
    if @uc.start_time.to_date > Time.now.to_date
      @uc.start_time = Time.mktime(Time.now.year, Time.now.month, Time.now.mday,6)
    end
    if @uc.end_time.to_date > Time.now.to_date
      @uc.end_time = Time.mktime(Time.now.year, Time.now.month, Time.now.mday,21)
    end
    if @uc.start_time.to_date > @uc.end_time.to_date 
      @uc.start_time = @uc.end_time
    end
    
    @uc.step_type = params[:step_type]
    @uc.ts = params[:ts] || "0"
    #-------------- Get data for Entry/exit in CSV format ---------------------------
    
    @launch_type = launch_type
    @ecn_launch_filter = launch_type
    if launch_type == 0
      @summary_fill_rate = FillRate.get_fill_rate_entry_exit(@uc.start_time, @uc.end_time, @step)
    else
      @summary_fill_rate = FillRate.get_launch_fill_rate(@uc.start_time, @uc.end_time, @step)
    end
  end
  
  # Get data for Chart Fill Rate by Shares
  def exit_fill_rate_by_shares
    
    @uc = get_uc
    @color_config = get_color_config
    #Filter by time
    trade_start_time = params[:trade_start_time]
    trade_end_time = params[:trade_end_time]
    start_time = params[:start_time]
    end_time = params[:end_time]    
    @step = params[:step_type].to_i || 5  
    launch_type = params[:launch_type]
    iso_order = params[:iso_order]
    iso_order = iso_order.to_i
    if params[:end_time]
      @uc.start_time = str_to_unix_time(params[:start_time])
      end
      if params[:end_time]
      @uc.end_time = str_to_unix_time( params[:end_time])
      end
   
    @uc.trade_start_time =  params[:trade_start_time] if params[:trade_start_time]
    @uc.trade_end_time =  params[:trade_end_time] if params[:trade_end_time]
    @uc.step_type = params[:step_type]

    #check date time: if date is chosen more than now date, it will return now date 
    check_datetime
    @uc.ecn_entry = params[:ecn_entry]  || "99" 
        @uc.ts = params[:ts] || "0"
    
    @summary_fill_rate_by_shares = FillRate.get_fill_rate_by_shares(trade_start_time, trade_end_time, @uc.start_time, @uc.end_time, @uc.ecn_entry.to_i, @step, iso_order)
    @ecn_launch_filter = launch_type
  end

  
  # Get data for Table Fill Rate by Shares
  def show_statistic
    
    @uc = get_uc
    #Filter by time
    trade_start_time = params[:trade_start_time]
    trade_end_time = params[:trade_end_time]
    start_time = params[:start_time]
    end_time = params[:end_time]
    if params[:end_time]
      @uc.start_time = str_to_unix_time(params[:start_time])
      end
      if params[:end_time]
      @uc.end_time = str_to_unix_time( params[:end_time])
      end
    @uc.trade_start_time =  params[:trade_start_time] if params[:trade_start_time]
    @uc.trade_end_time =  params[:trade_end_time] if params[:trade_end_time]
    launch_type = params[:launch_type]
    iso_order = params[:iso_order]
    iso_order = iso_order.to_i

    #check date time: if date is chosen more than now date, it will return now date 
    check_datetime
    @uc.ts = params[:ts] || "0"
    
    #if  @uc.end_time < Time.now
    ts_list = @uc.ts.gsub(/\d+/){|ts_id| ts_id.to_i - 1}
    @summary = NewOrder.get_entry_exit_statistic(@uc.start_time, @uc.end_time, @uc.trade_start_time, @uc.trade_end_time, ts_list, launch_type,iso_order)
    
  end
  
  
  
  def get_db_suffix
    return "#{Time.now.year}#{Time.now.month.to_s.rjust(2,'0')}"
  end
  
  def menu_refresh
    @uc = get_uc
    if params[:start_time]
      @uc.start_time = str_to_unix_time(params[:start_time])
    end
    if params[:end_time]
      @uc.end_time = str_to_unix_time(params[:end_time])
    end
  end
end
