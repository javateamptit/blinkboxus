class OrderViewController < ApplicationController

#================================================================================================
# Function    : index (action controller)
# Input        : start_time,
#                 end_time, 
#                 ts_x[0-16],
#                 ecn_x (ARCA,RASH,NDAQ,0)
#                 cecn_x(ARCA,RASH,NDAQ,0)
#                 img_view_type(0=absolute,1=relative)
#                 img_time_step(5,30,60, 1 day, 7 days)
# Description  :
# Developer      : 
# Date      : 
#================================================================================================
  def index
    #get user context 
    @uc = get_uc    
    @uc.ecn = "0"
    if params[:oid]
      @uc.trade_date = Time.now
      @seqidfrom = params[:oid]
      @seqidto = params[:oid]
    end
    @last_date_trading =  RiskManagement.find_by_sql("select max(date) as trade_date from new_orders" ).first.trade_date  
    #check trade_date is null
    if @last_date_trading == nil
      @last_date_trading = Time.now.strftime("%Y-%m-%d")
    end
    @check_date = Time.mktime(@last_date_trading.to_date.year,@last_date_trading.to_date.month,@last_date_trading.to_date.day)
    @uc.last_date_trading = @check_date 
    if @uc.trade_date > @check_date
      @uc.trade_date = @check_date  
    end
    
    render :layout=>"interface2"
  end

  def show_order
    #get user context 
  @uc = get_uc  
  #page
  #@page = (params[:page] || 1).to_i
  @page = params[:page] && params[:page].to_i > 0 ? params[:page].to_i : 1
  
  @items_per_page = 100

  @offset = (@page - 1) * @items_per_page
  
  #sort order
  @dir = params[:dir] || "asc"  
  
  #filter by sequence ID
  @seqidfrom = params[:seqidfrom] || "0"   
  @seqidto = params[:seqidto] || "5000000"
  
  #sort field
  @order_field = params[:sort_field] || "trade_time"
  
  #take trade date as format mm/dd/yyyy  
    if params[:trade_date]
    @uc.trade_date = str_to_unix_time(params[:trade_date])
  end

  @uc.trade_start_time =  params[:trade_start_time] if params[:trade_start_time]
  @uc.trade_end_time =  params[:trade_end_time] if params[:trade_end_time]
  @uc.trade_start_time = @uc.trade_start_time || "6:00"
  @uc.trade_end_time = @uc.trade_end_time || "19:00"
  @list_traders = params[:list_traders] || "All"
  list_traders = @list_traders
  
  #Check start_time, end_time more than Time now
  check_datetime  
  #if sort in price
  conditions = ["trade_date = ?","trade_time between ? and ?"]
  values = [@uc.trade_date, @uc.trade_start_time, @uc.trade_end_time]  
  
  #Set up connection to database which wants to use
  db_name_suffix = "#{@uc.trade_date.year}#{@uc.trade_date.month.to_s.rjust(2,'0')}"  
  # Connect to database need to query data
  connect_to_database(db_name_suffix)  

  #tonggle sort order
  if @dir == "asc"
    @dir = "desc"
  else
    @dir = "asc"
  end
  #filter by filled price
  if params[:price_from]  && params[:price_from].to_f > 0
    @uc.price_from = params[:price_from]
    
  else
    @uc.price_from = nil
  end
  
  if params[:price_to] && params[:price_to].to_f > 0  
    @uc.price_to = params[:price_to]
    
  else
    @uc.price_to = nil
  end

  if @uc.price_from || @uc.price_to  
    conditions << " qty > lvs "
  end
  if @uc.price_from && @uc.price_to
    conditions << " exists( select fills.oid from fills where traded_orders.id = fills.oid and fills.price between ? and  ?)"
    values << @uc.price_from.to_f
    values << @uc.price_to.to_f
  elsif @uc.price_to 
    conditions << " exists( select fills.oid from fills where traded_orders.id = fills.oid and fills.price <= ?)"
    values << @uc.price_to.to_f

  elsif @uc.price_from
    conditions << " exists( select fills.oid from fills where traded_orders.id = fills.oid and fills.price >= ?)"
    values << @uc.price_from.to_f
  end  

  #filter by symbol  
  if params[:symbol]  && params[:symbol].to_s !=""
    @uc.symbol = params[:symbol].strip
    conditions << "symbol = ?"
    values << @uc.symbol.upcase  
  else
    @uc.symbol = nil
  end
  
  #filter by ECN order  

  if (params[:ecn] && params[:ecn].to_s != "0" && params[:ecn].to_s != "") then
    @uc.ecn = params[:ecn]
    conditions << "ecn = ?"
    values << @uc.ecn
  else
    @uc.ecn = "0"
  end

  #filter by sequence id
  if params[:seqidfrom] && params[:seqidfrom].to_i >0
    @seqidfrom = params[:seqidfrom]    
  end
  if params[:seqidto] && params[:seqidto].to_i > 0 && params[:seqidto].to_i < 10000000 
    @seqidto = params[:seqidto]    
  end

  if @seqidfrom.to_i > 0
    conditions << "seqid >= ?"
    values << @seqidfrom.to_i
  end
  if @seqidto.to_i > 0 &&  @seqidto.to_i < 10000000
    conditions << "seqid <= ?"
    values << @seqidto.to_i
  end
  
  #filter by iso orders
  @iso_order = "0"
  if params[:iso_order] && params[:iso_order].to_s == "1"

    #check trade_date must be more than 01/01/2010
    if @uc.trade_date > str_to_unix_time("01/01/2010")
      @iso_order = "1"
      conditions << "is_iso is not NULL and is_iso ='Y'"
    end
    
  end
  if params[:iso_order] && params[:iso_order].to_s == "2"

    #check trade_date must be more than 01/01/2010
    if @uc.trade_date > str_to_unix_time("01/01/2010")
      @iso_order = "2"
      conditions << "is_iso is not NULL and is_iso ='N'"
    end
    
  end
  
  #filter by entry/exit
  @entry_exit = params[:entry_exit].to_i
  if  @entry_exit == 1
    conditions << "entry_exit = 'Entry'"
  elsif @entry_exit == 2
    conditions << "entry_exit = 'Exit'"
  end

   if list_traders == "All"
        condition_str = conditions.join(" and ")
        conditions = []
        conditions << condition_str
        conditions += values
        
        #number of traded stocks of given day
        @count = Order.count(:conditions=>conditions)
        #@page = (@count - 1) / @items_per_page + 1 if @count > 0 and @page * @items_per_page > @count
        #instantiate gaping object
        #@object_pages = Paginator.new self, @count, @items_per_page, @page
        @object_pages = WillPaginate::Collection.new(@page, @items_per_page, @count)
        #traded Stocks of given trade date
        
        @orders = Order.find(  :all,              
          :conditions=>conditions,
          :offset=>@offset,
          :limit => @items_per_page,
          :order=>"#{@order_field} #{@dir}, trade_time asc "
          )
   else
      trade_day = @uc.trade_date.year.to_s << "-" << @uc.trade_date.month.to_s.rjust(2,'0') << "-" << @uc.trade_date.day.to_s.rjust(2,'0')
     
      if @seqidfrom == ""
          @seqidfrom = "1"
      end
      if @seqidto == ""
          @seqidto = "49999"
      end
      list_traders = list_traders.downcase
      str_sql = "select substring(log_detail from 3 for 5) from tt_event_log where username='#{list_traders}' and date = '#{trade_day}' and activity_type = 4"
      activity_id = Activity.find_by_sql("#{str_sql}")
      str_cond ="("
      activity_id.each { |act|
          id_in_number = act.substring.to_i
          str_cond << act.substring.to_s
          str_cond << ","
      }
      str_cond << "49999)"
     
      if activity_id.size > 0
          sqlquery = "select * from traded_orders where seqid in #{str_cond} and trade_date = '#{trade_day}' and trade_time between '#{@uc.trade_start_time}' and '#{@uc.trade_end_time}'"
      else 
          sqlquery = "select * from traded_orders where seqid = 49999 and trade_date = '#{trade_day}' and trade_time between '#{@uc.trade_start_time}' and '#{@uc.trade_end_time}'"   
      end
      
      if params[:iso_order] == "1" # Not care the ISO ORDERS FILTER, THIS CONDIDION SUBTRACT THE ISO ORDERS
          sqlquery << " and seqid = -1"
      else
            sqlquery << " and seqid >= #{@seqidfrom}"
            sqlquery << " and seqid <= #{@seqidto}"
      end
      
      if @uc.price_from && @uc.price_to
          sqlquery << "and exists( select fills.oid from fills where traded_orders.id = fills.oid and fills.price between #{@uc.price_from.to_f} and #{@uc.price_to.to_f}) "
      elsif @uc.price_to 
          sqlquery << "and exists( select fills.oid from fills where traded_orders.id = fills.oid and fills.price <= #{@uc.price_to.to_f})"
      elsif @uc.price_from
          sqlquery << "and exists( select fills.oid from fills where traded_orders.id = fills.oid and fills.price >= #{@uc.price_from.to_f})"
      end 
      
      if params[:ecn] != "0"
        sqlquery << " and ecn = '#{@uc.ecn}'"
      end
      
      if params[:symbol] != ""
        symbol_t = @uc.symbol.upcase
        sqlquery << " and symbol = '#{symbol_t}'"
      end
       
      @orders = Order.find_by_sql("#{sqlquery}")
      @count = @orders.size
      @object_pages = WillPaginate::Collection.new(@page, @items_per_page, @count)  

   end 

    ids = []
  
  @orders.each{ |order|
    if order.qty.to_i > order.lvs.to_i
      ids << order.id
    end
  }
  cee_fields = ", b.filing_status, b.cee_note, b.is_cee" if (@uc.trade_date > CEE_DATE)
  
  brokens = Order.find_by_sql(["select  f.oid, b.new_price, b.new_shares, b.broken_shares #{cee_fields}  from fills f, brokens b where (f.oid in (?)) and f.fill_id = b.fill_id",ids])

  brokens.each{ |broken|
    broken_order = @orders.find{|o| o.id.to_i == broken.oid.to_i}
    if broken.is_cee == "f"
        broken_order["is_broken"] = true 
        broken_order["new_price"] = broken.new_price    
    
        if broken_order["broken_shares"] 
          broken_order["broken_shares"] += broken.broken_shares.to_i
        else
          broken_order["broken_shares"] = broken.broken_shares.to_i
        end
        broken_order["new_shares"] = broken.new_shares
    end 
    #broken_order["filing_status"] = broken.filing_status || ""
  }
  
  @orders.each{ |order|
    if order["is_broken"]
      order["status_edit"] = "Broken"
      order["price"] = order.price 
      order["lvs"] = order.lvs + order["broken_shares"].to_i      
    else
      order["status_edit"] = order.status_edit
      order["price"] = order.price
      order["lvs"] = order.lvs
    end
    
  }    
  end
#================================================================================================
# Function    : msg_type_list (action controller)
# Input        : order_id,
#        :      
# Description  :
# Developer      : 
# Date      : 
#================================================================================================

  def msg_type_list
    order_id = params[:order_id]
    
    msg_list = [
      ["new_order", "New Order"],
      ["order_ack","Order Ack"],
      ["order_reject", "Order Rejected"],
      ["fill","Filled"],
      ["cancel_request","Cancel Request"],
      ["cancel_ack", "Cancel Ack"],
      ["cancel_pending", "Cancel Pending"],
      ["cancel_reject","Cancel Rejected"],
      ["cancel","Cancelled"],
      ["broken_trade","Broken Trade"]
    ]

    @messages = []
    
    broken_fills = []
    msg_list.each{|msg|
        
        if msg[0] == "new_order"
          msgs = Order.find_by_sql(["select * from new_orders where oid = ?", order_id])
          @ecn = msgs[0].ecn if msgs[0]

        elsif msg[0] == "fill"
          msgs = Order.find_by_sql(["select * from #{msg[0]}s where  oid=? order by id", order_id])
          
          msgs.each{  |msg_obj|
            broken = Order.find_by_sql(["select * from brokens where fill_id=?", msg_obj.fill_id ]).first

            if broken && broken.new_price &&  broken.broken_shares.to_f > 0 && broken.is_cee == "f"
              broken_fills << msg_obj
            elsif broken && broken.is_cee == "f"
              #msg_obj.price = broken.new_price
              msg_obj['new_price'] = broken.new_price
            end
            #Get filing status 
            msg_obj['filing_status'] = broken.filing_status if (broken && broken.filing_status)
            msg_obj['cee_note'] = broken.cee_note if (broken && broken.cee_note)
          }
        else
      
          msgs = Order.find_by_sql(["select * from #{msg[0]}s where oid=?",order_id])
        end

        msgs.each{ |msg_obj|
          id_field =   "id"
          if msg[0] == "new_order"
            id_field = "order_id"
          end
          #Rename if the fill is broken
          if broken_fills.index(msg_obj)
            order_status = 'Removed Fill'
          else
            order_status = msg[1]
          end
          @messages << [order_status, msg_obj, "#{msg[0]}_#{msg_obj.attributes[id_field]}",msg_obj.time_stamp]
        }
    } 
    #@messages.sort!{|x,y| x[3].to_i <=> y[3].to_i }
  
    render :partial=>"msg_detail"
  end
#================================================================================================
# Function    : adjust_trade (action controller)
# Input        : fill_id,
#        :      
# Description  :
# Developer      : 
# Date      : 
#================================================================================================

  def adjust_trade

    @uc = get_uc
    
    status_mapping = {}
    filing_status_list().each{ |status|
      status_mapping[status[1]] = status[0]      
    }
    
    fill_id = params[:fill_id]
  new_price = params[:new_price]
  status = status_mapping[params[:status].to_i] if params[:status]  
  note = params[:note] || ""
  
  if fill_id && new_price
    broken = Order.find_by_sql(["select max(id) as max_broken_id from brokens"]).first
    new_broken_id = 1
    if broken && broken.max_broken_id
      new_broken_id = broken.max_broken_id.to_i  + 1
    end
    new_shares = 0
    broken_shares = 0

    if fill_id.split(",").size > 1
      fills = Order.find_by_sql(["select * from fills where fill_id in (?)", fill_id.split(",")])
  
      fills.each{|fill|
        #check if a fill was broken or saved filing_status
        exist_broken = Order.find_by_sql(["select * from brokens where oid = ? and exec_id=?", fill.oid, fill.exec_id])
        if exist_broken.empty?
          Order.find_by_sql(["insert into brokens(fill_id, new_shares, new_price, broken_shares,trade_date,oid, exec_id, filing_status, cee_note, is_cee) values(?,?,?,?,?,?,?,?,?,?)",fill.fill_id,new_shares, fill.price, fill.shares, @uc.trade_date, fill.oid.to_i, fill.exec_id.to_s, status, note, false])
        else
          Order.find_by_sql(["update brokens set new_shares=?, new_price=?, broken_shares=?,trade_date=?, filing_status=?, cee_note=?, is_cee=? where oid=? and exec_id=?",new_shares, fill.price, fill.shares, @uc.trade_date, status, note, false, fill.oid.to_i, fill.exec_id.to_s])
        end
        new_broken_id += 1
        #update filing_status, cee_note for fills table
        #Order.find_by_sql(["update fills set filing_status = ?, cee_note = ? where oid = ? and exec_id = ?", status.strip.upcase.to_s, note.to_s, fill.oid.to_i, fill.exec_id ])
      }
    else
      fill = Order.find_by_sql(["select * from fills where fill_id =?", fill_id]).first
      if new_price.to_f == 0
        #broken trade so price unchange
        new_price = fill.price
        broken_shares = fill.shares
      else
        # Price Adjustment so filled shares unchange
        new_shares = fill.shares
      end
      
      exist_broken = Order.find_by_sql(["select * from brokens where fill_id = ?", fill_id])
      if exist_broken.empty?
        Order.find_by_sql(["insert into brokens(fill_id, new_shares, new_price, broken_shares,trade_date,oid, exec_id, filing_status, cee_note, is_cee) values(?,?,?,?,?,?,?,?,?,?)",fill_id,new_shares, new_price, broken_shares, @uc.trade_date, fill.oid.to_i, fill.exec_id.to_s, status, note, false])
      else
        Order.find_by_sql(["update brokens set new_shares=?, new_price=?, broken_shares=?,trade_date=?, filing_status=?, cee_note=?, is_cee=? where oid=? and exec_id=?",new_shares, new_price, broken_shares, @uc.trade_date, status, note, false, fill.oid.to_i, fill.exec_id.to_s])
      end
    end

    render :text=> "OK"
    else
      render :text=> "Invalid Parameters"
    end
  end
  #================================================================================================
  # Function    : menu_refresh (action controller)
  # Input        : N/A
  # Date        : 2010/02/25
  #================================================================================================
  def menu_refresh
    @uc = get_uc
  end
  
  #================================================================================================
  # Function Name : xls_export
  # Input          : N/A
  # Developer     : TuanBD
  # Date Created  : August 11, 2010
  #================================================================================================  
  def xls_export
    #get user context 
    @uc = get_uc  
   
    #sort order
    @dir = params[:dir] || "asc"    
    #filter by sequence ID
    @seqidfrom = params[:seqidfrom] || "0"   
    @seqidto = params[:seqidto] || "5000000"
  
    #sort field
    @order_field = params[:sort_field] || "trade_time"  
    #take trade date as format mm/dd/yyyy  
    if params[:trade_date]
      @uc.trade_date = str_to_unix_time(params[:trade_date])
    end
    
    @uc.trade_start_time =  params[:trade_start_time] if params[:trade_start_time]
    @uc.trade_end_time =  params[:trade_end_time] if params[:trade_end_time]
    @uc.trade_start_time = @uc.trade_start_time || "6:00"
    @uc.trade_end_time = @uc.trade_end_time || "19:00"
  
    #Add on 2009/05/14 #Check start_time, end_time more than Time now
    check_datetime  
        
    conditions = ["trade_date = ?","trade_time between ? and ?"]
    values = [@uc.trade_date, @uc.trade_start_time, @uc.trade_end_time]  
    
    filename_xls_export = [@uc.trade_date.strftime("%Y%m%d").to_s]
    
    #filter by ECN order  
    if params[:ecn] && params[:ecn].to_s != "0"
      @uc.ecn = params[:ecn]
      conditions << "ecn = ?"
      values << @uc.ecn
      filename_xls_export << @uc.ecn.to_s
    else
      @uc.ecn = "0"
    end
    filename_xls_export << @uc.trade_start_time.to_s
    filename_xls_export << @uc.trade_end_time.to_s
    
    #Set up connection to database which wants to use
    db_name_suffix = "#{@uc.trade_date.year}#{@uc.trade_date.month.to_s.rjust(2,'0')}"  
    # Connect to database need to query data
    connect_to_database(db_name_suffix)
    
    #filter by filled price
    if params[:price_from]  && params[:price_from].to_f > 0
      @uc.price_from = params[:price_from]
      filename_xls_export << @uc.price_from.to_s
    else
      @uc.price_from = nil
    end
  
    if params[:price_to] && params[:price_to].to_f > 0  
      @uc.price_to = params[:price_to]
      filename_xls_export << @uc.price_to.to_s
    else
      @uc.price_to = nil
    end

    if @uc.price_from || @uc.price_to  
      conditions << " qty > lvs "
    end
    if @uc.price_from && @uc.price_to
      conditions << " exists( select fills.oid from fills where traded_orders.id = fills.oid and fills.price between ? and  ?)"
      values << @uc.price_from.to_f
      values << @uc.price_to.to_f
    elsif @uc.price_to 
      conditions << " exists( select fills.oid from fills where traded_orders.id = fills.oid and fills.price <= ?)"
      values << @uc.price_to.to_f
    elsif @uc.price_from
      conditions << " exists( select fills.oid from fills where traded_orders.id = fills.oid and fills.price >= ?)"
      values << @uc.price_from.to_f
    end  
    
    #filter by symbol  
    if params[:symbol]  && params[:symbol].to_s !=""
      @uc.symbol = params[:symbol].strip
      conditions << "symbol = ?"
      values << @uc.symbol.upcase  
      filename_xls_export << @uc.symbol.upcase
    else
      @uc.symbol = nil
    end
  
    
    #filter by sequence id
    if params[:seqidfrom] && params[:seqidfrom].to_i >0
      @seqidfrom = params[:seqidfrom]    
      filename_xls_export << @seqidfrom.to_s 
    end
    if params[:seqidto] && params[:seqidto].to_i > 0 && params[:seqidto].to_i < 5000000 
      @seqidto = params[:seqidto]    
      filename_xls_export << @seqidto.to_s
    end

    if @seqidfrom.to_i > 0
      conditions << "seqid >= ?"
      values << @seqidfrom.to_i
    end
    if @seqidto.to_i > 0 &&  @seqidto.to_i < 5000000
      conditions << "seqid <= ?"
      values << @seqidto.to_i
    end
  
    #filter by iso orders
    @iso_order = "0"
    if params[:iso_order] && params[:iso_order].to_s == "1"
      #check trade_date must be more than 01/01/2010
      if @uc.trade_date > str_to_unix_time("01/01/2010")
        @iso_order = "1"
        conditions << "is_iso is not NULL and is_iso ='Y'"
        filename_xls_export << "ISO_Order"
      end    
    end
    # Filter by Non-Iso
    if params[:iso_order] && params[:iso_order].to_s == "2"
      @iso_order = "2"
      conditions << "is_iso is not NULL and is_iso ='N'"  
      filename_xls_export << "Non_ISO_Order"
    end
    
    #filter by entry/exit
    @entry_exit = params[:entry_exit].to_i
    if  @entry_exit == 1
    conditions << "entry_exit = 'Entry'"
    filename_xls_export << @entry_exit.to_s
    elsif @entry_exit == 2
    conditions << "entry_exit = 'Exit'"
    filename_xls_export << @entry_exit.to_s
    end
    
    condition_str = conditions.join(" and ")
    conditions = []
    conditions << condition_str
    conditions += values
  
    @orders = Order.find(  :all,              
                :conditions=>conditions,
                :order=>"#{@order_field} #{@dir}, trade_time asc "
                )
    ids = []  
    @orders.each{ |order|
      if order.qty.to_i > order.lvs.to_i
        ids << order.id
      end
    }
    
    cee_fields = ", b.filing_status, b.cee_note" if (@uc.trade_date > CEE_DATE)
    brokens = Order.find_by_sql(["select  f.oid, b.new_price, b.new_shares, b.broken_shares #{cee_fields} from fills f, brokens b where (f.oid in (?)) and f.fill_id = b.fill_id",ids])
    brokens.each{ |broken|
      broken_order = @orders.find{|o| o.id.to_i == broken.oid.to_i}
    
      broken_order["is_broken"] = true
      broken_order["new_price"] = broken.new_price    
    
      if broken_order["broken_shares"] 
        broken_order["broken_shares"] += broken.broken_shares.to_i
      else
        broken_order["broken_shares"] = broken.broken_shares.to_i
      end    
      broken_order["new_shares"] = broken.new_shares
      #broken_order["filing_status"] = broken.filing_status       
      
    }
  
    @orders.each{ |order|
      if order["is_broken"]
        order["status_edit"] = "Broken"
        order["price"] = order.price 
        order["lvs"] = order.lvs + order["broken_shares"].to_i      
      else
        order["status_edit"] = order.status_edit
        order["price"] = order.price
        order["lvs"] = order.lvs
        #order["filing_status"] = ""
      end
    }
    if !@orders.empty?
        filename_str = filename_xls_export.join("_")
        column_names = ["Symbol", "Account" , "Action", "Qty", "Exec", "LVS"  , "Price", "ECN", "OID", "TIF", "Status", "Filing Status", "Time"]    
        @file_path = ExportFile.export_file_to_xls(column_names, @orders, "Order_View", filename_str)
           
        if File.exists?(@file_path)
          send_file @file_path, :filename => File.basename(@file_path)
        else
          render :text => "File not found"  
        end
    else
        render :text => "No data for export file."  
    end

    
  end
  
  #================================================================================================
  # Function Name : cee_filing
  # Input          : fill_id, status, note 
  # Developer     : TuanBD
  # Date Created  : September 08, 2010
  #================================================================================================
  def cee_filing
    @uc = get_uc
    
    status_mapping = {}
    filing_status_list().each{ |status|
      status_mapping[status[1]] = status[0]      
    }
    
    fill_id = params[:fill_id]
    status = status_mapping[params[:status].to_i] if params[:status]
    note = params[:note]
    is_cee = params[:is_cee]

    if fill_id.to_i > 0
      
      #update status, note for brokens table
      broken = Order.find_by_sql(["select * from brokens where fill_id = ?", fill_id.to_i]).first
      if !broken.nil?
        #update 
        Order.find_by_sql(["update brokens set filing_status = ?, cee_note = ? where oid = ? and exec_id = ?", status, note.to_s, broken.oid.to_i, broken.exec_id ])
      else
        #insert new record
        fill = Order.find_by_sql(["select * from fills where fill_id =?", fill_id.to_i]).first
        Order.find_by_sql(["insert into brokens(fill_id, new_shares, new_price, broken_shares,trade_date,oid, exec_id, filing_status, cee_note, is_cee) values(?,?,?,?,?,?,?,?,?,?)",fill.fill_id, fill.shares, fill.price, fill.shares, @uc.trade_date, fill.oid.to_i, fill.exec_id.to_i, status, note, is_cee])
      end      
      render :text=>"OK"
    else
      render :text=>"Invalid Parameters"
    end
  end
  #================================================================================================
  # Function Name : get_filing_status
  # Input          : fill_id
  # Developer     : TuanBD
  # Date Created  : September 10, 2010
  #================================================================================================
  def get_filing_status
    fill_id = params[:fill_id]
    @type_num = params[:type_num]
    info = Order.find_by_sql(["select * from brokens where fill_id = ?", fill_id.to_i]).first
    @cee_info = []
    
    status_mapping = {}
    filing_status_list().each{ |status|
      status_mapping[status[0]] = status[1]      
    }
    
    if !info.nil? && info.filing_status && info.filing_status.strip.to_s != ""
      @cee_info << status_mapping[info.filing_status.strip.to_s]
      @cee_info << info.cee_note
    else
      @cee_info << 0
      @cee_info << ""
    end
    render :partial=>"filing_status"
  end
end
