#=================================================================================
#  * Name: ExportFile
#  * Description: Class for processing export data to file with format CSV and XLS   
#  * Created by: TuanBD
#  * Date Created: August 10, 2010
#  * Last Modified: 
#  * Modified by: 
#=================================================================================
require 'spreadsheet'
require 'util'
class ExportFile 
  
  #XLS_FORMAT = "SLS"
  #CSV_FORMAT = "CSV"
  #=================================================================================
  #  * Method name: export_file_to_xls
  #  * Input:  arg: columns, data, view_name
  #  * Output: full_xls_file_path
  #  * Developer: TuanBD
  #  * Date created: August, 10 2010
  #  * Description
  #=================================================================================
  def self.export_file_to_xls(columns, items, view_name, filename_export)
      # Define file information
       db_dir = "#{RAILS_ROOT}/public/data"
       FileUtils.mkdir(db_dir) if !File.exists?(db_dir)
       dir_path = "#{db_dir}/exported_files"
       FileUtils.mkdir(dir_path) if !File.exists?(dir_path)
       file_name = "#{view_name}_#{filename_export}_#{Time.now.strftime('%Y%m%d%H%M%S')}"
       count = items.size 
       full_path_file_name = process_file_excel(columns, count,dir_path,file_name,items, view_name)
       full_path_file_name
  end  
  
  #=================================================================================
  #  * Method name: process_file_excel
  #  * Input:  array columns, count, dir path, data
  #  * Output: path of XLS file created
  #  * Date created: August, 11 2010
  #  * Developer: TuanBD
  #  * Description
  #=================================================================================
  def self.process_file_excel (columns,  count,dir_path,file_name,items, view_name)
    full_path_file_name =  "#{dir_path}/#{file_name}.xls" #full path to the file
    Spreadsheet.client_encoding = 'UTF-8'
    book = Spreadsheet::Workbook.new 
    range = 0...count.to_i
    item_per_sheet = 10000
    m = 0
    processed_count = 0
    total_item = items.length
    range.step(item_per_sheet) do |j|
      m += 1
      sheet = book.create_worksheet
      if count.to_i < item_per_sheet*m
        max = count.to_i
      else
        max = item_per_sheet*m
      end     
      sheet.name = "#{view_name} #{j+1} - #{max}"
      sheet.row(0).concat columns
      sheet.row(0).height = 18
      bold = Spreadsheet::Format.new :weight => :bold
      sheet.row(0).default_format = bold         
      i = 0
      items.each do |item|
        item_arr = convert_to_arr(item, view_name)
        #Because excel format doesn't accept all-nil values so
        # convert nil => empty string is required        
        item_arr.map!{|x| x || ""}        
        sheet.row(i+1).concat item_arr
        processed_count += 1
        percent = 20 + (processed_count*70)/(total_item.to_f)
        i += 1
      end
    end
    #if have data, file will be created
    if !book.worksheets.empty?
      book.write(full_path_file_name)
    end
    return full_path_file_name
  end
 
  #=================================================================================
  #  * Method name: convert_to_arr
  #  * Input:  item, view_name (name of screen)
  #  * Output: field array
  #  * Date created: August, 11 2010
  #  * Developer: TuanBD
  #  * Description
  #=================================================================================
  def self.convert_to_arr(item, view_name)
    ret = []
    if view_name.to_s == "Order_View"
      #column_names = ["Symbol", "Action", "Qty","Exec" ,"LVS", "Price", "ECN", "OID", "TIF", "Status", "Time"]
      account = account_mapping[item.account.to_i] 
      qty = helpers.number_with_delimiter( item.qty , :delimiter => ",")
      lvs = helpers.number_with_delimiter( item["lvs"],  :delimiter => ",")
      price = helpers.number_to_currency(item["price"].to_f,{:unit=>"", :precision=>2, :delimiter => ","}) 
      time = item.trade_time.strftime("%H:%M:%S.").to_s + "%06d" % (item.trade_time.usec.to_i)
      filing_status = item.filing_status.strip if (!item.filing_status.nil?)
      ecn = display_ecn(item.ecn.strip)
      ret = [item.symbol.strip, account, item.action2, qty, item.qty.to_i - item["lvs"].to_i ,lvs, price, ecn, item.seqid, item.tif.strip, item["status_edit"], filing_status, time]    
    elsif view_name.to_s == "Trade_Break"
      #column_names = ["ClOrdID", "Symbol", "Filled Shares", "Filled Price", "ECN", "Type", "Trade Time"]
      share = helpers.number_with_delimiter(item.filled_shares)
      price = helpers.number_to_currency(item.filled_price,{:unit=>"", :precision=>2, :delimiter => ","})
      time = item.trade_time.strftime("%H:%M:%S:").to_s + "%06d" % (item.trade_time.usec.to_i)
      if item.action.to_s == "SS"
        action_t = "SS"
      else
        action_t = item.action.first
      end
      ecn = display_ecn(item.ecn.strip)
      ret = [item.cl_order_id, item.symbol.strip, share, price , ecn, action_t , time]    
    elsif view_name.to_s == "Broken_Trade"
      #column_names = ["ClOrdID", "Symbol", "Filled Shares", "Filled Price", "ECN", "Trade Time"]
      filled_share = helpers.number_with_delimiter(item.filled_shares)
      filled_price = helpers.number_to_currency(item.filled_price,{:unit=>"", :precision=>2, :delimiter => ","})
      time = item.trade_time.strftime("%H:%M:%S:").to_s + "%06d" % (item.trade_time.usec.to_i)
      status = item.filing_status.strip if item.filing_status
      date = item.trade_date.strftime("%Y-%m-%d").to_s
      ecn = display_ecn(item.ecn.strip)
      ret = [item.cl_order_id, item.symbol.strip, item.side2, filled_share, filled_price, ecn, date,time, status, item.cee_note]  
    elsif view_name.to_s == "Adjusted_Price"
      #column_names = ["ClOrdID", "Symbol", "Filled Shares", "Filled Price", "Adjusted Price", "ECN", "Trade Time"]
      filled_share = helpers.number_with_delimiter(item.filled_shares)
      filled_price = helpers.number_to_currency(item.filled_price,{:unit=>"", :precision=>2, :delimiter => ","})
      adjusted_price = helpers.number_to_currency(item.adjusted_price,{:unit=>"", :precision=>2, :delimiter => ","})
      time = item.trade_time.strftime("%H:%M:%S:").to_s + "%06d" % (item.trade_time.usec.to_i)
      status = item.filing_status.strip if item.filing_status
      date = item.trade_date.strftime("%Y-%m-%d").to_s
      ecn = display_ecn(item.ecn.strip)
      ret = [item.cl_order_id, item.symbol.strip, item.side2, filled_share, filled_price, adjusted_price, ecn, date, time, status, item.cee_note] 
    elsif view_name.to_s == "Trade_Result"
      #column_names = ["Cross ID" ,"Symbol", "Detected Shares", "Expected Profit", "Net P&L", "PM Net P&L ","Bought Shares", "Sold Shares", "Total Sold", "Total Bought", "Time"]
      time = item.time_stamp.strftime("%H:%M:%S.").to_s + "%06d" % (item.time_stamp.usec.to_i)
      date = item.date.strftime("%m/%d/%Y").to_s
      expected_shares = helpers.number_with_delimiter(item.expected_shares)
      launch_shares = helpers.number_with_delimiter(item.launch_shares)
      expected_pl = helpers.number_to_currency(item.expected_pl,{:unit=>"", :precision=>2, :delimiter => ","})
      bought_shares = helpers.number_with_delimiter(item.bought_shares)
      sold_shares = helpers.number_with_delimiter(item.sold_shares)
      total_sold = helpers.number_to_currency(item.total_sold,{:unit=>"", :precision=>2, :delimiter => ","})
      total_bought = helpers.number_to_currency(item.total_bought,{:unit=>"", :precision=>2, :delimiter => ","})
      if item.net_pl >= 0      
      net_pl = helpers.number_to_currency(item.net_pl.abs,{:unit=>"", :precision=>2, :delimiter => ","})
      else
      net_pl = "(" + helpers.number_to_currency(item.net_pl.abs,{:unit=>"", :precision=>2, :delimiter => ","})  + ")"
      end
      
      if item.pm_net_pl >= -0.0001         
      pm_pl = helpers.number_to_currency(item.pm_net_pl.abs,{:unit=>"", :precision=>2, :delimiter => ","})
      else
      pm_pl = "(" + helpers.number_to_currency(item.pm_net_pl.abs,{:unit=>"", :precision=>2, :delimiter => ","})  + ")"
      end
      ret = [item.as_cid, item.symbol.strip, launch_shares, expected_shares, expected_pl, net_pl, pm_pl, bought_shares, sold_shares, total_sold, total_bought, date, time]  
    elsif view_name.to_s == "Not_Easy_to_Borrow"
      as_cid = item[0]
      symbol = item[1]
      expected_shares = item[2]
      expected_pl = item[3]
      time_stamp = item[4].to_s << "." << item[8].to_s
      time = time_stamp
      matched_pl = 0
      bought_shares = 0
      sold_shares = 0
      total_sold = 0
      total_bought = 0
      pm_pl = 0
      date = item[7]
      launch_shares = item[10]
      ret = [as_cid, symbol, launch_shares, expected_shares, expected_pl, matched_pl, pm_pl, bought_shares, sold_shares, total_sold, total_bought, date.strftime("%Y-%m-%d"), time]
    elsif view_name.to_s == "Hidden_Liquidity"
      #column_names = ["Symbol", "Account" , "Action", "Visible Shares", "Order Size", "OverSize Shares"  , "%Hidden Liquidity", "Price", "Filled Shares", "Avg Filled Price", "Canceled Shares", "ECN", "OID", "TIF", "Status", "Time"]
      account = account_mapping[item.account.to_i] 
      lvs = helpers.number_with_delimiter( item["lvs"],  :delimiter => ",")
      price = helpers.number_to_currency(item["price"].to_f,{:unit=>"", :precision=>2, :delimiter => ","}) 
      avg_price = helpers.number_to_currency(item.avg_price.to_f,{:unit=>"", :precision=>2, :delimiter => ","}) 
      time = item.trade_time.strftime("%H:%M:%S.").to_s + "%06d" % (item.trade_time.usec.to_i)
      qty = item.qty.to_i
      percent_hidden_liquidity = helpers.number_to_currency((item.percent_hidden_liquidity * 100).to_f,{:unit=>"", :precision=>2, :delimiter => ","}) 
      ecn = display_ecn(item.ecn.strip)
      ret = [item.symbol.strip, account, item.action2, item.visible_shares, item.qty, item.oversize_shares, percent_hidden_liquidity, price, item.filled_shares, avg_price, lvs, ecn, item.seqid, item.tif.strip, item["status_edit"], time]
    else  
  
    end
    ret
  end
 
  def self.helpers 
    ActionController::Base.helpers
  end

end

