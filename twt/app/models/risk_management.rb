class RiskManagement < ActiveRecord::Base
  #establish_connection :development
  OLD_COMISSION = 0.00425
  OLD_COMISSION1 = 0.0034
  COMISSION = 0.00313
#================================================================================================
# Method    : summary
# Input         : from - begin date; 
#                 end - end date
# Date created  : 02/10/2007
# Description  : calculate trading summary by given dates
# Developer     : hungpk
#================================================================================================
  def self.days_in_month(month)
    month == 2 ? 28 : 30 + ((month * 1.126).to_i % 2)
  end

  def self.summary(from, to)
    #p from 
    st = from.month
    en = to.month
    year = from.year

    date_st = from
    date_en = to
    if (from.month < to.month && from.year == to.year) || (from.year < to.year)
      if from.month == 2 && ((from.year % 4 == 0 && from.year % 100 !=0) || from.year % 400 == 0)
        #leap year
        date_en = Time.mktime(from.year, from.month,days_in_month(from.month)+1,21) 
      else
        date_en = Time.mktime(from.year, from.month,days_in_month(from.month),21) 
      end
    end
    #initial 
    trade_summary_all = {
      "gross_profit" => 0.00,
      "total_shares_traded" => 0,
      "market_value" => 0.00,
      "net_profit" =>  0.00
    }
    #puts "st <= en :#{st} #{en}"
    k = 0
    if to.year > from.year
      k = to.year - from.year
      en = en + 12 * k
    end

    #puts "st <= en :#{st} #{en}"
    while st <= en 
      db_name_suffix = "#{Time.now.year}#{Time.now.month.to_s.rjust(2,'0')}"
      if st <= 12
        db_name_suffix = "#{year}#{st.to_s.rjust(2,'0')}"
      else 
        if k == 1
          db_name_suffix = "#{year + 1}#{(st - 12).to_s.rjust(2,'0')}"
        elsif k == 2
          if st <= 24
            db_name_suffix = "#{year + 1}#{(st - 12).to_s.rjust(2,'0')}"
          else
            db_name_suffix = "#{year + 2}#{(st - 24).to_s.rjust(2,'0')}"
          end
        end
      end

      # lets manually connect to the proper db
      get_database_connection(db_name_suffix, "as")

      st = st + 1

      conditions = []
      values = []

      if from
        conditions << "date >= ?"
        values << date_st
      end

      if to
        conditions << "date <= ?"
        values << date_en
      end
      sql_check_totalfee_col = " SELECT column_name FROM information_schema.columns WHERE table_name='risk_managements' and column_name='total_fee'"
      total_fee_col = RiskManagement.find_by_sql(sql_check_totalfee_col)

      if total_fee_col.size > 0 then
        sql_str = "select sum(matched_profit_loss) as gross_profit, sum(total_quantity) as total_shares_traded, sum(total_market_value) as market_value, sum(total_fee) as total_fee from risk_managements where "
      else
        sql_str = "select sum(matched_profit_loss) as gross_profit, sum(total_quantity) as total_shares_traded, sum(total_market_value) as market_value from risk_managements where "
      end

      sql_str << conditions.join(" and ")

      sql_cmd = [sql_str] + values 
      #puts sql_cmd
      ret = RiskManagement.find_by_sql(sql_cmd).first

      sql_str_max_date = "select  max(date)::date as max_date from risk_managements where   "
      sql_str_max_date << conditions.join(" and ")
      sql_cmd_max_date = [sql_str_max_date] + values

      max_date = RiskManagement.find_by_sql(sql_cmd_max_date).first.max_date
      #puts max_date
      ret2 = RiskManagement.find_by_sql(["select  sum(total_market_value) as market_value from risk_managements where total_market_value >0 and date = ?;",max_date]).first


      last_commission = COMISSION
      #puts last_commission
      if date_st < Time.mktime(2008,12,1)
        last_commission = OLD_COMISSION
        #puts last_commission
      elsif date_st < Time.mktime(2009,06,23)
        last_commission = OLD_COMISSION1
      end

      if date_st < Time.mktime(2013,04,01)
        # check view_liquidity exists 
        view_check = RiskManagement.find_by_sql("select * from pg_views where schemaname='public' and viewname = 'view_liquidity'")

        # calculate total fee
        totalfee = 0.0

        if view_check.size > 0 then
          totalfee = calculate_total_fee(date_st,date_en)
        else
          totalfee = 0
        end


        if view_check.size > 0 then
          trade_summary = {
            "gross_profit" => ret.gross_profit.to_f,
            "total_shares_traded" => ret.total_shares_traded.to_i,
            "market_value" => ret2.market_value.to_f,
            "net_profit" =>  ret.gross_profit.to_f - totalfee

          }
        else
          trade_summary = {
            "gross_profit" => ret.gross_profit.to_f,
            "total_shares_traded" => ret.total_shares_traded.to_i,
            "market_value" => ret2.market_value.to_f,
            "net_profit" =>  ret.gross_profit.to_f - ret.total_shares_traded.to_i * last_commission
          }
        end
      else

        trade_summary = {
          "gross_profit" => ret.gross_profit.to_f,
          "total_shares_traded" => ret.total_shares_traded.to_i,
          "market_value" => ret2.market_value.to_f,
          "net_profit" =>  ret.gross_profit.to_f - ret.total_fee.to_f

        }

      end


      trade_summary_all["gross_profit"] = trade_summary_all["gross_profit"] + trade_summary["gross_profit"]
      trade_summary_all["total_shares_traded"] = trade_summary_all["total_shares_traded"] + trade_summary["total_shares_traded"]
      trade_summary_all["market_value"] = 0.00 + trade_summary["market_value"]
      trade_summary_all["net_profit"] = trade_summary_all["net_profit"] + trade_summary["net_profit"]

      #increase date_st, date_en
      if en - st > 0
        if (date_en.month+1) == 2 && ((date_en.year % 4 == 0 && date_en.year % 100 != 0) || date_en.year % 400 == 0)
          #leap year
          date_en = Time.mktime(date_en.year, date_en.month+1,days_in_month(date_en.month+1)+1,21) 
        else
          if date_en.month == 12
            date_en = Time.mktime(date_en.year + 1, 1, 31,21) 
          else
            date_en = Time.mktime(date_en.year, date_en.month+1,days_in_month(date_en.month+1),21) 
          end
        end 
      else
        date_en = to
      end
      #increase date_st one month
      if date_st.month == 12
        date_st = Time.mktime(date_st.year + 1,  1,1,6)
      else
        date_st = Time.mktime(date_st.year, date_st.month + 1,1,6) 
      end

    end

    return trade_summary_all
  end
#================================================================================================
# Method    : calculate_total_fee
# Input         : N/A
#                 
# Date created  : 
# Description  : cal total fees
# Developer     : KhanhTT
#================================================================================================
  def self.calculate_total_fee(date_st,date_en)
    totalfee = 0.00
    conditions = []
    values = []

    if date_st
      conditions << "date >= ?"
      values << date_st
    end

    if date_en
      conditions << "date <= ?"
      values << date_en
    end

    sql_str = "select * from view_liquidity where "
    sql_str << conditions.join(" and ")
    sql_cmd = [sql_str] + values 

    ret = RiskManagement.find_by_sql(sql_cmd)
    if ret.size <= 0
      return 0.00
    end

    sec_fee = 0.0
    taf_fee = 0.0
    commission = 0.0
    add_venue_larger_1 = 0.0
    add_venue_smaller_1 = 0.0
    remove_venue_larger_1 = 0.0
    remove_venue_smaller_1 = 0.0
    auction_venue = 0.0
    route_venue_arca = 0.0
    route_venue_nasdaq = 0.0
    route_venue_batsz = 0.0
    if File::exists?( "config/fee_config" ) then
      arr = IO.readlines("config/fee_config")
      count = 0

      arr.each do |x|
        if (x.lstrip.first != "#") then
          if count == 0
            sec_fee = x.to_f
          elsif count == 1
            taf_fee = x.to_f
          elsif count == 2
            commission = x.to_f
          elsif count == 3
            add_venue_larger_1 = x.to_f
          elsif count == 4
            add_venue_smaller_1 = x.to_f
          elsif count == 5
            remove_venue_larger_1 = x.to_f
          elsif count == 6
            remove_venue_smaller_1 = x.to_f
          elsif count == 7
            auction_venue = x.to_f
          elsif count == 8
            route_venue_arca = x.to_f
          elsif count == 9
            route_venue_nasdaq = x.to_f
          elsif count == 10
            route_venue_batsz = x.to_f
          end
          count = count + 1
        end
      end

    else
      puts "FILE NOT EXISTS"
    end


    totalSecFees = 0.00
    totalTafFee = 0.00
    totalWedbushCommission = 0.0 
    totalVenueFee = 0.0
    temp = 0.0
    ret.each  { |record|
      size = record.shares.to_i
      price =  record.price.to_f
      side = record.side.to_s

      if side[0].chr == "S" then
        totalSecFees = totalSecFees + (size * price * sec_fee)
      end
      totalTafFee = totalTafFee + (size * taf_fee);
      totalWedbushCommission = totalWedbushCommission + (size * commission)

      liquidity = record.liquidity.to_s

      ecn = record.ecn.to_s
      if ecn == "ARCA" then
        if (liquidity == "A" || liquidity == "B" || liquidity == "D" || liquidity == "M" || liquidity == "S") then #add

          temp = (price >= 1.00) ? (size * add_venue_larger_1).to_f : (size * add_venue_smaller_1).to_f
          totalVenueFee = totalVenueFee + temp
        elsif (liquidity == "R" || liquidity == "E" || liquidity == "L") # Remove

          temp = (price >= 1.00) ? (size * remove_venue_larger_1) : (size * price * remove_venue_smaller_1)
          totalVenueFee = totalVenueFee + temp
        elsif (liquidity == "G" || liquidity == "Z") #auction

          temp = (size * auction_venue)
          totalVenueFee = totalVenueFee + temp
        elsif (liquidity == "X" || liquidity == "F" || liquidity == "N" || liquidity == "H" || liquidity == "C" || liquidity == "U" || liquidity == "Y" || liquidity == "W") #Route

          temp = (size * route_venue_arca)
          totalVenueFee = totalVenueFee + temp
        end

      elsif ecn == "OUCH" then 
        if (liquidity == "A" || liquidity == "k" || liquidity == "J" || liquidity == "7" || liquidity == "8" || liquidity == "e" || liquidity == "f") #add

          temp = (price >= 1.00) ? (size * add_venue_larger_1).to_f : (size * add_venue_smaller_1).to_f
          totalVenueFee = totalVenueFee + temp

        elsif (liquidity == 'R' || liquidity == 'm' || liquidity == "6" || liquidity == "d") #Remove

          temp = (price >= 1) ? (size * remove_venue_larger_1) : (size * price * remove_venue_smaller_1)
          totalVenueFee = totalVenueFee + temp

        elsif (liquidity == 'O' || liquidity == 'M' || liquidity == 'L' || liquidity == 'H' || liquidity == 'K' || liquidity == 'C') #auction

          temp = (size * auction_venue)
          totalVenueFee = totalVenueFee + temp
        end

      elsif (ecn == "RASH") then

        if (liquidity == "A" || liquidity == "J" || liquidity == "V" || liquidity == "D" || liquidity == "F" || liquidity == "U" || liquidity == "k" || liquidity == "9" || liquidity == "7" || liquidity == "8" || liquidity == "e" || liquidity == "f") then #add

          temp = (price >= 1.00) ? (size * add_venue_larger_1).to_f : (size * add_venue_smaller_1).to_f
          totalVenueFee = totalVenueFee + temp

        elsif (liquidity == "R" || liquidity == "m" || liquidity == "d" || liquidity == "6") # Remove

          temp = (price >= 1) ? (size  * remove_venue_larger_1) : (size  * price * remove_venue_smaller_1)
          totalVenueFee = totalVenueFee + temp

        elsif (liquidity == "G" || liquidity == "O" || liquidity == "M" || liquidity == "C" || liquidity == "L" || liquidity == "H" || liquidity == "K" || liquidity == "I" || liquidity == "T" || liquidity == "Z") #auction

          temp = (size * auction_venue)
          totalVenueFee = totalVenueFee + temp

        elsif (liquidity == "X" || liquidity == "Y" || liquidity == "B" || liquidity == "P" || liquidity == "Q") #Route

          temp = (size * route_venue_nasdaq)
          totalVenueFee = totalVenueFee + temp
        end

      elsif (ecn == "NYSE") then
        if (liquidity == "2" || liquidity == "8") #add

          temp = (price >= 1.00) ? (size * add_venue_larger_1).to_f : (size * add_venue_smaller_1).to_f
          totalVenueFee = totalVenueFee + temp

        elsif (liquidity == "1" || liquidity == "9" || liquidity == "3" || liquidity == " ") #Remove

          temp = (price >= 1) ? (size  * remove_venue_larger_1) : (size * price * remove_venue_smaller_1)
          totalVenueFee = totalVenueFee + temp

        elsif (liquidity == "4" || liquidity == "5" || liquidity == "6" || liquidity == "7" ) #auction
          temp = (size * auction_venue)
          totalVenueFee = totalVenueFee + temp
        end
      elsif (ecn == "BATZ") then
        if (liquidity == "A") #add

          temp = (price >= 1.00) ? (size * add_venue_larger_1).to_f : (size * add_venue_smaller_1).to_f
          totalVenueFee = totalVenueFee + temp

        elsif (liquidity == "R") #Remove

          temp = (price >= 1) ? (size  * remove_venue_larger_1) : (size * price * remove_venue_smaller_1).to_f
          totalVenueFee = totalVenueFee + temp

        elsif (liquidity == "C" ) #auction
          temp = (size * auction_venue)
          totalVenueFee = totalVenueFee + temp
        elsif (liquidity == "X" ) #route
          temp = (size * route_venue_batsz)
          totalVenueFee = totalVenueFee + temp
        end


      end


    }

    totalfee = totalSecFees + totalTafFee + totalWedbushCommission + totalVenueFee
    return totalfee
  end


#================================================================================================
# Method    : get_list_open_position
# Input         : N/A
#                 
# Date created  : 
# Description  : get open postion
# Developer     : 
#================================================================================================
  def self.get_list_open_position

    positons = RiskManagement.find(:all,
                     :conditions=>" total_market_value >0 and date = (select max(date) from risk_managements) ",
                     :order=>"symbol , net_quantity ")
    return  positons
  end
#================================================================================================
# Method    : trade_statistic_filter_by_tradetype
# Input         : start_time - begin date; 
#                 end_time - end date
#          trade_type (0:all, 1:normal, 2:simultaneous)
# Date created  : 02/07/2008 (July 02 2008)
# Last Modify   : 11/08/2008
# Description  : calculate trading statistic by given dates
# Developer     : tuanbd
#================================================================================================

  def self.trade_statistic_filter_by_tradetype start_time, end_time, share_size = 0, expected_pl = 0, trade_type = 0
    #puts "get trade_statistic_filter_by_tradetype!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    st = start_time.month
    en = end_time.month
    year = start_time.year

    date_st = start_time
    date_en = end_time
    if start_time.month < end_time.month
      if start_time.month == 2 && ((start_time.year % 4 == 0 && start_time.year % 100 != 0)|| start_time.year % 400 == 0)
        #leap year
        date_en = Time.mktime(start_time.year, start_time.month,days_in_month(start_time.month) + 1,21) 
      else
        date_en = Time.mktime(start_time.year, start_time.month,days_in_month(start_time.month),21) 
      end
    end
        ret_all = {
                "stuck_loss"=>[0,0,0,2, "stuck_loss"], #[count, expected profit, real profit]
        "stuck_profit"=>[0,0,0,3, "stuck_profit"], 
                "loss" =>[0,0,0,4, "loss"] ,
                "profit_under_expected" => [0,0,0,5,"profit_under_expected"],
                "profit_equal_to_or_above_expected" => [0,0,0,6,"profit_equal_to_or_above_expected"],
                "total" =>[0,0,0,7,"total"],
                "missed" =>[0,0,0,0,"missed"],
        "repriced" =>[0,0,0,1,"repriced"]
            }

    #puts "st <= en :#{st} #{en}"
    k = 0
    if end_time.year > start_time.year
      k = end_time.year - end_time.year
      en = en + 12 * k
    end
    #puts "st <= en :#{st} #{en}"

    while st <= en 
      db_name_suffix = "#{Time.now.year}#{Time.now.month.to_s.rjust(2,'0')}"

      if st <= 12
        db_name_suffix = "#{year}#{st.to_s.rjust(2,'0')}"
      else 
        if k == 1
          db_name_suffix = "#{year + 1}#{(st - 12).to_s.rjust(2,'0')}"
        elsif k == 2
          if st <= 24
            db_name_suffix = "#{year + 1}#{(st - 12).to_s.rjust(2,'0')}"
          else
            db_name_suffix = "#{year + 2}#{(st - 24).to_s.rjust(2,'0')}"
          end
        end
      end

      # lets manually connect to the proper db
      get_database_connection(db_name_suffix, "as")

      st = st + 1

      #puts date_st.strftime("%m/%d/%Y")
      #puts date_en.strftime("%m/%d/%Y")
      ret = RiskManagement.find_by_sql(["select cal_trade_statistic_filter_by_tradetype2(?, ?, ?, ? ,?);", date_st, date_en, share_size, expected_pl, trade_type]).first.cal_trade_statistic_filter_by_tradetype2.gsub(/\(|\)/, "");

      count_miss,count_repriced,count_stuck_loss,count_stuck_profit,count_loss,count_profit_under,count_profit_above,count_total,expected_miss,expected_repriced,expected_stuck_loss,expected_stuck_profit,expected_loss,expected_profit_under,expected_profit_above,expected_total,real_miss,real_stuck_loss,real_stuck_profit,real_loss,real_profit_under,real_profit_above,real_total = ret.split(",")

      ret = {
          "stuck_loss"=>[count_stuck_loss.to_i,expected_stuck_loss.to_f,real_stuck_loss.to_f,2, "stuck_loss"], #[count, expected profit, real profit]
          "stuck_profit"=>[count_stuck_profit.to_i,expected_stuck_profit.to_f,real_stuck_profit.to_f,3, "stuck_profit"], #[count, expected profit, real profit]
          "loss" =>[count_loss.to_i,expected_loss.to_f,real_loss.to_f,4, "loss"] ,
          "profit_under_expected" => [count_profit_under.to_i,expected_profit_under.to_f,real_profit_under.to_f,5,"profit_under_expected"],
          "profit_equal_to_or_above_expected" => [count_profit_above.to_i,expected_profit_above.to_f,real_profit_above.to_f,6,"profit_equal_to_or_above_expected"],
          "total" =>[0,0,0,7,"total"],
          "missed" =>[count_miss.to_i,expected_miss.to_f,real_miss.to_f,0,"missed"],
          "repriced" =>[count_repriced.to_i,expected_repriced.to_f,0,1,"repriced"]
        }

      ret.each{ |k,v|
        if k != "total"
          ret["total"][0] += v[0]  if k !="repriced"  
          ret["total"][1] += v[1] if k !="repriced"
          ret["total"][2] += v[2]

        end
      }

      if share_size == 0 and  expected_pl == 0 and trade_type !=2 and date_st.month < 7 and date_st.year <= 2008                      
          summary = self.summary(date_st, date_en)
          ret['total'][2] = summary["gross_profit"]
          ret['stuck_loss'][2] = summary["gross_profit"]-  ret['loss'][2] - ret['profit_under_expected'][2] - ret['profit_equal_to_or_above_expected'][2]

      end

      ret_all["stuck_loss"][0] = ret_all["stuck_loss"][0] + ret["stuck_loss"][0]
      ret_all["stuck_loss"][1] = ret_all["stuck_loss"][1] + ret["stuck_loss"][1]
      ret_all["stuck_loss"][2] = ret_all["stuck_loss"][2] + ret["stuck_loss"][2]

      ret_all["stuck_profit"][0] = ret_all["stuck_profit"][0] + ret["stuck_profit"][0]
      ret_all["stuck_profit"][1] = ret_all["stuck_profit"][1] + ret["stuck_profit"][1]
      ret_all["stuck_profit"][2] = ret_all["stuck_profit"][2] + ret["stuck_profit"][2]

      ret_all["loss"][0] = ret_all["loss"][0] + ret["loss"][0]
      ret_all["loss"][1] = ret_all["loss"][1] + ret["loss"][1]
      ret_all["loss"][2] = ret_all["loss"][2] + ret["loss"][2]

      ret_all["profit_under_expected"][0] = ret_all["profit_under_expected"][0] + ret["profit_under_expected"][0]
      ret_all["profit_under_expected"][1] = ret_all["profit_under_expected"][1] + ret["profit_under_expected"][1]
      ret_all["profit_under_expected"][2] = ret_all["profit_under_expected"][2] + ret["profit_under_expected"][2]

      ret_all["profit_equal_to_or_above_expected"][0] = ret_all["profit_equal_to_or_above_expected"][0] + ret["profit_equal_to_or_above_expected"][0]
      ret_all["profit_equal_to_or_above_expected"][1] = ret_all["profit_equal_to_or_above_expected"][1] + ret["profit_equal_to_or_above_expected"][1]
      ret_all["profit_equal_to_or_above_expected"][2] = ret_all["profit_equal_to_or_above_expected"][2] + ret["profit_equal_to_or_above_expected"][2]

      ret_all["total"][0] = ret_all["total"][0] + ret["total"][0]
      ret_all["total"][1] = ret_all["total"][1] + ret["total"][1]
      ret_all["total"][2] = ret_all["total"][2] + ret["total"][2]

      ret_all["missed"][0] = ret_all["missed"][0] + ret["missed"][0]
      ret_all["missed"][1] = ret_all["missed"][1] + ret["missed"][1]
      ret_all["missed"][2] = ret_all["missed"][2] + ret["missed"][2]

      ret_all["repriced"][0] = ret_all["repriced"][0] + ret["repriced"][0]
      ret_all["repriced"][1] = ret_all["repriced"][1] + ret["repriced"][1]
      ret_all["repriced"][2] = ret_all["repriced"][2] + ret["repriced"][2]

      #increase date_st, date_en       
      if en - st > 0
        if (date_st.month+1) == 2 && ((date_st.year % 4 == 0 && date_st.year % 100 != 0) || date_st.year % 400 == 0)
          #leap year
          date_en = Time.mktime(date_st.year, date_st.month+1,days_in_month(date_st.month+1)+1,21) 
        else
          if date_st.month == 12
            date_en = Time.mktime(date_st.year + 1, 1,31,21) 
          else
            date_en = Time.mktime(date_st.year, date_st.month+1,days_in_month(date_st.month+1),21) 
          end

        end 
      else
        date_en = end_time
      end
      #date_st = Time.mktime(start_time.year, date_st.month + 1,1,6)

      if date_st.month == 12
        date_st = Time.mktime(start_time.year + 1,  1,1,6)
      else
        date_st = Time.mktime(start_time.year, date_st.month + 1,1,6) 
      end
    end

        ret_all
    end

#================================================================================================
# Method    : trade_statistic_filter_by_tradetype2
# Input         : start_time - begin date; 
#                 end_time - end date
#          trade_type (0:all, 1:normal, 2:simultaneous)
# Date created  : 8/10/2008
# Last Modify   : 18/18/2008
# Description  : calculate trading statistic by given dates
# Developer     : tuanbd
#================================================================================================

  def self.trade_statistic_filter_by_tradetype2 start_time, end_time, share_size = 0, expected_pl = 0, trade_type = 0
    #puts "get trade_statistic_filter_by_tradetype2 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    st = start_time.month
    en = end_time.month
    year = start_time.year

    date_st = start_time
    date_en = end_time
    if start_time.month < end_time.month
      if start_time.month == 2 && ((start_time.year % 4 == 0 && start_time.year % 100 != 0)|| start_time.year % 400 == 0)
        #leap year
        date_en = Time.mktime(start_time.year, start_time.month,days_in_month(start_time.month) + 1,21) 
      else
        date_en = Time.mktime(start_time.year, start_time.month,days_in_month(start_time.month),21) 
      end
    end
        ret_all = {
                "stuck_loss"=>[0,0,0,2, "stuck_loss"], #[count, expected profit, real profit]
        "stuck_profit"=>[0,0,0,3, "stuck_profit"], 
                "loss" =>[0,0,0,4, "loss"] ,
                "profit_under_expected" => [0,0,0,5,"profit_under_expected"],
                "profit_equal_to_or_above_expected" => [0,0,0,6,"profit_equal_to_or_above_expected"],
                "total" =>[0,0,0,7,"total"],
                "missed" =>[0,0,0,0,"missed"],
        "repriced" =>[0,0,0,1,"repriced"]
            }

    #puts "st <= en :#{st} #{en}"
    k = 0
    if end_time.year > start_time.year
      k = end_time.year - start_time.year
      en = en + 12 * k
    end
    #puts "st <= en :#{st} #{en}"
    while st <= en 
      db_name_suffix = "#{Time.now.year}#{Time.now.month.to_s.rjust(2,'0')}"

      if st <= 12
        db_name_suffix = "#{year}#{st.to_s.rjust(2,'0')}"
      else
        if k == 1
          db_name_suffix = "#{year + 1}#{(st - 12).to_s.rjust(2,'0')}"
        elsif k == 2
          if st <= 24
            db_name_suffix = "#{year + 1}#{(st - 12).to_s.rjust(2,'0')}"
          else
            db_name_suffix = "#{year + 2}#{(st - 24).to_s.rjust(2,'0')}"
          end
        end
      end

      # lets manually connect to the proper db
      get_database_connection(db_name_suffix, "as")

      st = st + 1
      #puts date_st.strftime("%m/%d/%Y")
      #puts date_en.strftime("%m/%d/%Y")

      count_miss = 0
      count_repriced = 0
      count_stuck_loss = 0
      count_stuck_profit = 0
      count_loss  = 0
      count_profit_under = 0
      count_profit_above = 0
      count_total = 0
      expected_miss = 0.0
      expected_repriced = 0.0
      expected_stuck_loss = 0.0
      expected_stuck_profit = 0.0
      expected_loss = 0.0
      expected_profit_under = 0.0
      expected_profit_above = 0.0
      expected_total = 0.0
      real_miss = 0.0
      real_stuck_loss = 0.0
      real_stuck_profit = 0.0
      real_loss = 0.0
      real_profit_under = 0.0
      real_profit_above = 0.0
      real_total  = 0.0

      #filter by trade_type
      condition_trade_type =""
      if trade_type && trade_type.to_i  == 2
        condition_trade_type = " and trade_type = 2 "
      elsif trade_type && trade_type.to_i  == 1
        if date_st.month == 6
          condition_trade_type = " and trade_type = 1 "
        else
          condition_trade_type = " and trade_type = 0 "
        end
      else 
        condition_trade_type = " and trade_type = 0 "
      end

      sql_str = " select name_id, sum(count) as count, sum(expected_pl) as expected_pl, sum(real_pl) as real_pl "
      sql_str = "#{sql_str} from summary_trade_statistics where date >='#{date_st}' and date <='#{date_en}' #{condition_trade_type} group by name_id order by name_id "

      ret_rows = RiskManagement.find_by_sql(" #{sql_str} ")
      ret_rows.each {|row|
        #Miss
        if row.name_id.to_i == 1 
          count_miss = row.count
          expected_miss = row.expected_pl
        #Ouch reprice
        elsif row.name_id.to_i == 2
          count_repriced = row.count
          expected_repriced = row.expected_pl
        #Stuck Loss
        elsif row.name_id.to_i == 3
          count_stuck_loss = row.count
          expected_stuck_loss = row.expected_pl
          real_stuck_loss = row.real_pl
        #Stuck Profit
        elsif row.name_id.to_i == 4
          count_stuck_profit = row.count
          expected_stuck_profit = row.expected_pl
          real_stuck_profit = row.real_pl
        #Loss
        elsif row.name_id.to_i == 5
          count_loss = row.count
          expected_loss = row.expected_pl
          real_loss = row.real_pl
        #Prifit under
        elsif row.name_id.to_i == 6
          count_profit_under = row.count
          expected_profit_under = row.expected_pl
          real_profit_under = row.real_pl
        #Profit above
        elsif row.name_id.to_i == 7
          count_profit_above = row.count
          expected_profit_above = row.expected_pl
          real_profit_above = row.real_pl
        #Total
        elsif row.name_id.to_i == 8

        end
      }



      ret = {
          "stuck_loss"=>[count_stuck_loss.to_i,expected_stuck_loss.to_f,real_stuck_loss.to_f,2, "stuck_loss"], #[count, expected profit, real profit]
          "stuck_profit"=>[count_stuck_profit.to_i,expected_stuck_profit.to_f,real_stuck_profit.to_f,3, "stuck_profit"], #[count, expected profit, real profit]
          "loss" =>[count_loss.to_i,expected_loss.to_f,real_loss.to_f,4, "loss"] ,
          "profit_under_expected" => [count_profit_under.to_i,expected_profit_under.to_f,real_profit_under.to_f,5,"profit_under_expected"],
          "profit_equal_to_or_above_expected" => [count_profit_above.to_i,expected_profit_above.to_f,real_profit_above.to_f,6,"profit_equal_to_or_above_expected"],
          "total" =>[0,0,0,7,"total"],
          "missed" =>[count_miss.to_i,expected_miss.to_f,real_miss.to_f,0,"missed"],
          "repriced" =>[count_repriced.to_i,expected_repriced.to_f,0,1,"repriced"]
        }

      ret.each{ |k,v|
        if k != "total"
          ret["total"][0] += v[0]  if k !="repriced"  
          ret["total"][1] += v[1] if k !="repriced"
          ret["total"][2] += v[2]

        end
      }

      if share_size == 0 and  expected_pl == 0 and trade_type !=2 and date_st.month < 7 and date_st.year <= 2008                     
          summary = self.summary(date_st, date_en)
          ret['total'][2] = summary["gross_profit"]
          ret['stuck_loss'][2] = summary["gross_profit"]-  ret['loss'][2] - ret['profit_under_expected'][2] - ret['profit_equal_to_or_above_expected'][2]

      end

      ret_all["stuck_loss"][0] = ret_all["stuck_loss"][0] + ret["stuck_loss"][0]
      ret_all["stuck_loss"][1] = ret_all["stuck_loss"][1] + ret["stuck_loss"][1]
      ret_all["stuck_loss"][2] = ret_all["stuck_loss"][2] + ret["stuck_loss"][2]

      ret_all["stuck_profit"][0] = ret_all["stuck_profit"][0] + ret["stuck_profit"][0]
      ret_all["stuck_profit"][1] = ret_all["stuck_profit"][1] + ret["stuck_profit"][1]
      ret_all["stuck_profit"][2] = ret_all["stuck_profit"][2] + ret["stuck_profit"][2]

      ret_all["loss"][0] = ret_all["loss"][0] + ret["loss"][0]
      ret_all["loss"][1] = ret_all["loss"][1] + ret["loss"][1]
      ret_all["loss"][2] = ret_all["loss"][2] + ret["loss"][2]

      ret_all["profit_under_expected"][0] = ret_all["profit_under_expected"][0] + ret["profit_under_expected"][0]
      ret_all["profit_under_expected"][1] = ret_all["profit_under_expected"][1] + ret["profit_under_expected"][1]
      ret_all["profit_under_expected"][2] = ret_all["profit_under_expected"][2] + ret["profit_under_expected"][2]

      ret_all["profit_equal_to_or_above_expected"][0] = ret_all["profit_equal_to_or_above_expected"][0] + ret["profit_equal_to_or_above_expected"][0]
      ret_all["profit_equal_to_or_above_expected"][1] = ret_all["profit_equal_to_or_above_expected"][1] + ret["profit_equal_to_or_above_expected"][1]
      ret_all["profit_equal_to_or_above_expected"][2] = ret_all["profit_equal_to_or_above_expected"][2] + ret["profit_equal_to_or_above_expected"][2]

      ret_all["total"][0] = ret_all["total"][0] + ret["total"][0]
      ret_all["total"][1] = ret_all["total"][1] + ret["total"][1]
      ret_all["total"][2] = ret_all["total"][2] + ret["total"][2]

      ret_all["missed"][0] = ret_all["missed"][0] + ret["missed"][0]
      ret_all["missed"][1] = ret_all["missed"][1] + ret["missed"][1]
      ret_all["missed"][2] = ret_all["missed"][2] + ret["missed"][2]

      ret_all["repriced"][0] = ret_all["repriced"][0] + ret["repriced"][0]
      ret_all["repriced"][1] = ret_all["repriced"][1] + ret["repriced"][1]
      ret_all["repriced"][2] = ret_all["repriced"][2] + ret["repriced"][2]

      #increase date_st, date_en
      if en - st > 0
        if (date_st.month+1) == 2 && ((date_st.year % 4 == 0 && date_st.year % 100 != 0) || date_st.year % 400 == 0)
          #leap year
          date_en = Time.mktime(date_st.year, date_st.month+1,days_in_month(date_st.month+1)+1,21) 
        else
          if date_st.month == 12
            date_en = Time.mktime(date_st.year + 1, 1, 31,21) 
          else
            date_en = Time.mktime(date_st.year, date_st.month+1,days_in_month(date_st.month+1),21) 
          end
        end 
      else
        date_en = end_time
      end
      #date_st = Time.mktime(start_time.year, date_st.month + 1,1,6)
      if date_st.month == 12
        date_st = Time.mktime(start_time.year + 1,  1,1,6)
      else
        date_st = Time.mktime(start_time.year, date_st.month + 1,1,6) 
      end
    end

        ret_all
    end #end function trade_statistic_filter_by_tradetype2
end
