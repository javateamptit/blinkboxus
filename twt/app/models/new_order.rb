class NewOrder < ActiveRecord::Base
  set_table_name :new_orders
  def self.days_in_month(month)
    month == 2 ? 28 : 30 + ((month * 1.126).to_i % 2)
  end
  
  def self.number_day_in_month(month, year)
    month == 2 ? ((year % 4 == 0 )|| (year % 400 == 0 && year % 100 != 0 ) ? 29 : 28) : 30 + ((month * 1.126).to_i % 2)
  end
  
  #================================================================================================
  # Method    : Function build conditions of ECN Entry/Exit
  # Developer : 
  #================================================================================================
  def self.build_conditions_for_ecn_launch(ecn_launch)
    ret = ""
    if ecn_launch.to_i == 1
     ret = "(ecn_launch = 'ARCA')"
    elsif ecn_launch.to_i == 2
      ret = "(ecn_launch = 'NDAQ')"
    elsif ecn_launch.to_i == 3
      ret = "(ecn_launch = 'NYSE')"
    elsif ecn_launch.to_i == 4
      ret = "(ecn_launch = 'BATZ')"
    elsif ecn_launch.to_i == 5
      ret = "(ecn_launch = 'EDGX')"
    elsif ecn_launch.to_i == 6
      ret = "(ecn_launch = 'EDGA')"
    elsif ecn_launch.to_i == 7
      ret = "(ecn_launch = 'NDBX')"
    elsif ecn_launch.to_i == 8
      ret = "(ecn_launch = 'BATY')"
    elsif ecn_launch.to_i == 9
      ret = "(ecn_launch = 'PSX')"
    elsif ecn_launch.to_i == 10
      ret = "(ecn_launch = 'AMEX')"
    end
    return ret
  end
  
  #================================================================================================
  # Function    : get_entry_exit
  # Input       : start_date
  #       : end_date
  #       : ts - either -1:AS 0, 1, 2, 3: TS1 - TS4#
  #       : trade_type: 0:all, 1:normal, 2:simultaneous # tuanbd modify on 2008/05/29
  # Description : get entry exit statistic
  # Developer     : Hungpk
  # Date      : 01/02/2008
  #================================================================================================ 
  def self.get_entry_exit (start_date, end_date, trade_start_time, trade_end_time, ts=nil, trade_type = nil, side = nil, order_type = 0)
    st = start_date.month
    en = end_date.month
    year = start_date.year
    date_st = start_date
    date_en = end_date
    if start_date.month < end_date.month
      if start_date.month == 2 && ((start_date.year % 4 == 0 && start_date.year % 100 !=0)|| start_date.year % 400 == 0)
        #leap year
        date_en = Time.mktime(start_date.year, start_date.month,days_in_month(start_date.month) + 1,21) 
      else
        date_en = Time.mktime(start_date.year, start_date.month,days_in_month(start_date.month),21) 
      end     
    end
    #init variable returned
    ecns = ecn_list_plus_bzx_smart_route().collect{|ecn| ecn[0] if ecn.last }
    ecn_name_mapping = {}
    ecn_list_plus_bzx_smart_route().each{ |ecn|
      ecn_name_mapping[ecn[2]] = ecn[0]
    }

    ecns.delete_if{|x| x == nil}
    summary_all = {}
    ecns.each{ |ecn|
      summary_all[ecn] = {"entry_succ" => 0,
              "entry_miss" => 0,
              "exit_succ" =>0,
              "exit_miss" =>0}
    }
    #subtotal rows
    summary_all["0"] = {"entry_succ" => 0,
              "entry_miss" => 0,
              "exit_succ" =>0,
              "exit_miss" =>0}
    k = 0
    if end_date.year > start_date.year
      k = end_date.year - start_date.year
      en = en + 12 * k
    end
   
    while st <= en 
      db_name_suffix = "#{Time.now.year}#{Time.now.month.to_s.rjust(2,'0')}"     
      if st <= 12
        db_name_suffix = "#{year}#{st.to_s.rjust(2,'0')}"
      else        
        if k == 1         
          db_name_suffix = "#{year + 1}#{(st - 12).to_s.rjust(2,'0')}"        
        elsif k == 2
          if st <= 24
            db_name_suffix = "#{year + 1}#{(st - 12).to_s.rjust(2,'0')}"
          else
            db_name_suffix = "#{year + 2}#{(st - 24).to_s.rjust(2,'0')}"
          end
        end
      end
      
      # lets manually connect to the proper db
      get_database_connection(db_name_suffix, "as")
      st = st + 1
      conditions = ["date >= ? and date <= ? and ts_id <> -1 "]   
      condition_values = [date_st, date_en]
      # Modified to add multiple trade servers
      if ts != "-1"
        if ts.index(",")
          con = ""
          ts.split(",").each do |t|
            condition_values << t.to_i
            con += "?,"
          end
          con = con[0..(con.length - 2)]
          conditions << "ts_id in (" + con + ")"
        else
          conditions << "ts_id=?"
          condition_values << ts
        end
      end
      if trade_type and  trade_type.to_i == 1
        # statistics with trade type is normal
        conditions << " (trade_type != 1 or trade_type is null ) "
      elsif trade_type and  trade_type.to_i == 2
        # statistics with trade type is simultaneous
        conditions << "trade_type = 1"
      end
      if side
        if side == 0
          conditions << "(side = 'BO' or side = 'BC')"
        elsif side == 1
          conditions << "side = 'S'"
        elsif side != 3 
          conditions << "side = 'SS'"
        end
      end
      
      if order_type == 1
        conditions << "(is_iso = 'Y' and order_id >= 1000000)"
      elsif order_type == 2
        conditions << "(is_iso = 'N' and order_id >= 1000000)"
      end
      
      if trade_start_time  && trade_end_time
        conditions << "(time_stamp between '#{trade_start_time}' and '#{trade_end_time}')" 
      end
      
      summary = {}
      ecns.each{ |ecn|
        summary[ecn] = {"entry_succ" => 0,
                "entry_miss" => 0,
                "exit_succ" =>0,
                "exit_miss" =>0}
      }
      #subtotal rows
      summary["0"] = {"entry_succ" => 0,
                "entry_miss" => 0,
                "exit_succ" =>0,
                "exit_miss" =>0}
      
      new_orders = NewOrder.find :all,
                 :conditions => [conditions.join(' and '), condition_values].flatten
      
      new_orders.each do |order|
        ecn_map = ""
        ecn_map = order.ecn.strip
        if ecn_map == "BATZ" && order.routing_inst
          if  order.routing_inst.strip == "RL2"
            ecn_map = "BZSR"
          end
        end
        
        if order[:entry_exit] && order[:entry_exit].strip == "Entry"
          if order.shares.to_i > order.left_shares.to_i
            summary[ecn_name_mapping[ecn_map]]["entry_succ"] += 1
          #elsif (order.price.to_f != order.ack_price.to_f && (order.ecn.strip == 'OUCH'))
            # not calculate misses have re-price    
          else 
            summary[ecn_name_mapping[ecn_map]]["entry_miss"] += 1
          end
        else
          if order.shares.to_i > order.left_shares.to_i
            summary[ecn_name_mapping[ecn_map]]["exit_succ"] += 1  
          #elsif (order.price.to_f != order.ack_price.to_f && (order.ecn.strip == 'OUCH'))
            # not calculate misses have re-price      
          else 
            summary[ecn_name_mapping[ecn_map]]["exit_miss"] += 1
          end
        end 
      end
      
      summary.each do |ecn, v|
        if ecn != "0"
          summary["0"]["entry_succ"] += v["entry_succ"]
          summary["0"]["entry_miss"] += v["entry_miss"]
          summary["0"]["exit_succ"] += v["exit_succ"]
          summary["0"]["exit_miss"] += v["exit_miss"]
        end
      end
      summary_all.each do |ecn, v|
         summary_all[ecn]["entry_succ"] = summary_all[ecn]["entry_succ"] + summary[ecn]["entry_succ"]
         summary_all[ecn]["entry_miss"] = summary_all[ecn]["entry_miss"] + summary[ecn]["entry_miss"]
         summary_all[ecn]["exit_succ"] = summary_all[ecn]["exit_succ"] + summary[ecn]["exit_succ"]
         summary_all[ecn]["exit_miss"] = summary_all[ecn]["exit_miss"] + summary[ecn]["exit_miss"]
      end
    
      if en - st > 0
        if (date_st.month+1) == 2 && ((date_st.year % 4 ==0 && date_st.year % 100 != 0)|| date_st.year % 400 ==0)
          #leap year
          date_en = Time.mktime(date_st.year, date_st.month+1,days_in_month(date_st.month+1) + 1,21) 
        else
          date_en = Time.mktime(date_st.year, date_st.month+1,days_in_month(date_st.month+1),21) 
        end       
      else
        date_en = end_date
      end
      #date_st = Time.mktime(date_st.year, date_st.month + 1,1,6) 
      if date_st.month == 12
        date_st = Time.mktime(date_st.year + 1,  1,1,6) 
      else
        date_st = Time.mktime(date_st.year, date_st.month + 1,1,6)  
      end
    end
    
    return summary_all
  end
  
  #================================================================================================
  # Function    : get_entry_exit_launch
  # Input       : start_date
  #             : end_date
  # Description : get entry exit statistic with launch filter
  # Developer   : 
  # Update        : 28/03/2014
  #================================================================================================ 
  def self.get_entry_exit_launch (start_date, end_date, trade_start_time, trade_end_time, ts=nil, trade_type = nil, side = nil,launch_type=0, order_type = 0)
    st = start_date.month
    en = end_date.month
    year = start_date.year
    date_st = start_date
    date_en = end_date
    if start_date.month < end_date.month
      if start_date.month == 2 && ((start_date.year % 4 == 0 && start_date.year % 100 !=0)|| start_date.year % 400 == 0)
        #leap year
        date_en = Time.mktime(start_date.year, start_date.month,days_in_month(start_date.month) + 1,21) 
      else
        date_en = Time.mktime(start_date.year, start_date.month,days_in_month(start_date.month),21) 
      end     
    end
    #init variable returned
    ecns = ecn_list_plus_bzx_smart_route().collect{|ecn| ecn[0] if ecn.last }
    ecn_name_mapping = {}
    ecn_list_plus_bzx_smart_route().each{ |ecn|
      ecn_name_mapping[ecn[2]] = ecn[0]
    }
    
    ecns.delete_if{|x| x == nil}
    summary_all = {}
    ecns.each{ |ecn|
      summary_all[ecn] = {"entry_succ" => 0,
              "entry_miss" => 0,
              "exit_succ" =>0,
              "exit_miss" =>0}
    }
    #subtotal rows
    summary_all["0"] = {"entry_succ" => 0,
              "entry_miss" => 0,
              "exit_succ" =>0,
              "exit_miss" =>0}
  
    k = 0
    if end_date.year > start_date.year
      k = end_date.year - start_date.year
      en = en + 12 * k
    end
    
    while st <= en 
      db_name_suffix = "#{Time.now.year}#{Time.now.month.to_s.rjust(2,'0')}"    
      if st <= 12
        db_name_suffix = "#{year}#{st.to_s.rjust(2,'0')}"
      else        
        if k == 1         
          db_name_suffix = "#{year + 1}#{(st - 12).to_s.rjust(2,'0')}"        
        elsif k == 2
          if st <= 24
            db_name_suffix = "#{year + 1}#{(st - 12).to_s.rjust(2,'0')}"
          else
            db_name_suffix = "#{year + 2}#{(st - 24).to_s.rjust(2,'0')}"
          end
        end
      end
      # lets manually connect to the proper db
      get_database_connection(db_name_suffix, "as")
      st = st + 1
      conditions = ["date >= ? and date <= ? and ts_id <> -1 "]
      condition_values = [date_st, date_en]
      if ts != "-1"
        if ts.index(",")
          con = ""
          ts.split(",").each do |t|
            condition_values << t.to_i
            con += "?,"
          end
          con = con[0..(con.length - 2)]
          conditions << "ts_id in (" + con + ")"
        else
          conditions << "ts_id=?"
          condition_values << ts
        end
      end
    
      if trade_type and  trade_type.to_i == 1
        # statistics with trade type is normal
        conditions << " (trade_type != 1 or trade_type is null ) "
      elsif trade_type and  trade_type.to_i == 2
        # statistics with trade type is simultaneous
        conditions << "trade_type = 1"
      end
      if launch_type.to_i != 0
        conditions << build_conditions_for_ecn_launch(launch_type)  
      end
      if order_type == 1
        conditions << "(is_iso = 'Y' and order_id >= 1000000)"
      elsif order_type == 2
        conditions << "(is_iso = 'N' and order_id >= 1000000)"
      end
      
      if trade_start_time  && trade_end_time
        conditions << "(time_stamp between '#{trade_start_time}' and '#{trade_end_time}')" 
      end
     
      if side
        if side == 0
          conditions << "(side = 'BO' or side = 'BC')"
        elsif side == 1
          conditions << "side = 'S'"
        elsif side != 3 #  Modified to check if not all side
          conditions << "side = 'SS'"
        end
      end
      
      summary = {}
      ecns.each{ |ecn|
        summary[ecn] = {"entry_succ" => 0,
                "entry_miss" => 0,
                "exit_succ" =>0,
                "exit_miss" =>0}
      }
      #subtotal rows
      summary["0"] = {"entry_succ" => 0,
                "entry_miss" => 0,
                "exit_succ" =>0,
                "exit_miss" =>0}
      
      new_orders = NewOrder.find :all,
                 :conditions => [conditions.join(' and '), condition_values].flatten
      
      if (launch_type.to_i != 0)
        conditions = ["date >= ? and date <= ? and ts_id <> -1 "]
        condition_values = [date_st, date_en]
        # Modified to add multiple trade servers
        if ts != "-1"
          if ts.index(",")
            con = ""
            ts.split(",").each do |t|
              condition_values << t.to_i
              con += "?,"
            end
            con = con[0..(con.length - 2)]
            conditions << "ts_id in (" + con + ")"
          else
            conditions << "ts_id=?"
            condition_values << ts
          end
        end
      
        if trade_type and  trade_type.to_i == 1
          # statistics with trade type is normal
          conditions << " (trade_type != 1 or trade_type is null ) "
        elsif trade_type and  trade_type.to_i == 2
          # statistics with trade type is simultaneous
          conditions << "trade_type = 1"
        end
        
        #Conditions by ISO 
        conditions_iso = ""
        if order_type == 1
          conditions_iso << " and (is_iso = 'Y' and order_id >= 1000000)"
        elsif order_type == 2
          conditions_iso << " and (is_iso = 'N' and order_id >= 1000000)"
        end
        if launch_type.to_i != 0
          conditions << build_conditions_for_ecn_launch(launch_type)
        end
        new_orders_exit = NewOrder.find :all,
                 :conditions => [conditions.join(' and '), condition_values].flatten

        cross_id = "("
        new_orders_exit.each { |x|
        cross_id << "'"
        cross_id << x.ts_id.to_s
        cross_id << " "
        cross_id << x.cross_id.to_s 
        cross_id << " "
        cross_id << x.date.to_s
        cross_id << "'"
        cross_id << ","
        }
        cross_id << "'0 2012-01-03')"   
        if side
          if side == 0
            side_sql = " and (side = 'BC' or side = 'BO')"
          elsif side == 1
            side_sql = "and side = 'S'"
          elsif side != 3 # LuuNN modified to check if not all side
            side_sql = "and side = 'SS'"
          end
        end
        
        sql_str = "select * from new_orders where date >= '#{date_st}' and date <= '#{date_en}' and (time_stamp between '#{trade_start_time}' and '#{trade_end_time}') #{conditions_iso} and ts_id <> -1 "
        sql_where = "and entry_exit = 'Exit' and (ts_id || ' ' ||cross_id || ' ' || date) in #{cross_id} #{side_sql}"
        sql_str = "#{sql_str} #{sql_where} order by oid"
        exit_launch = NewOrder.find_by_sql("#{sql_str}")
        exit_launch.each do |order|
        
          ecn_map = ""
          ecn_map = order.ecn.strip
          if ecn_map == "BATZ" && order.routing_inst
            if order.routing_inst.strip == "RL2"
              ecn_map = "BZSR"
            end
          end   
          if order.shares.to_i > order.left_shares.to_i
            summary[ecn_name_mapping[ecn_map]]["exit_succ"] += 1
          #elsif (order.price.to_f != order.ack_price.to_f && (order.ecn.strip == 'OUCH'))
            # not calculate misses have re-price    
          else  
            summary[ecn_name_mapping[ecn_map]]["exit_miss"] += 1
          end
        end
        
      end
      
      new_orders.each do |order|  
        ecn_map = order.ecn.strip
        if ecn_map == "BATZ" && order.routing_inst
          if order.routing_inst.strip == "RL2"
            ecn_map = "BZSR"
          end
        end
              
        if order[:entry_exit] && order[:entry_exit].strip == "Entry"
          if order.shares.to_i > order.left_shares.to_i
            summary[ecn_name_mapping[ecn_map]]["entry_succ"] += 1
          #elsif (order.price.to_f != order.ack_price.to_f && (order.ecn.strip == 'OUCH'))
            # not calculate misses have re-price    
          else 
            # calculate misses have re-price 
            summary[ecn_name_mapping[ecn_map]]["entry_miss"] += 1
          end
        else
          if order.shares.to_i > order.left_shares.to_i
            summary[ecn_name_mapping[ecn_map]]["exit_succ"] += 1
          #elsif (order.price.to_f != order.ack_price.to_f && (order.ecn.strip == 'OUCH'))
            # not calculate misses have re-price    
          else
            summary[ecn_name_mapping[ecn_map]]["exit_miss"] += 1
          end
          
        end 
      end
      
      summary.each do |ecn, v|
        if ecn != "0"
          summary["0"]["entry_succ"] += v["entry_succ"]
          summary["0"]["entry_miss"] += v["entry_miss"]
          summary["0"]["exit_succ"] += v["exit_succ"]
          summary["0"]["exit_miss"] += v["exit_miss"]
        end
      end
      summary_all.each do |ecn, v|
         summary_all[ecn]["entry_succ"]= summary_all[ecn]["entry_succ"] + summary[ecn]["entry_succ"]
         summary_all[ecn]["entry_miss"]= summary_all[ecn]["entry_miss"] + summary[ecn]["entry_miss"]
         summary_all[ecn]["exit_succ"]= summary_all[ecn]["exit_succ"] + summary[ecn]["exit_succ"]
         summary_all[ecn]["exit_miss"]= summary_all[ecn]["exit_miss"] + summary[ecn]["exit_miss"]
      end

      if en - st > 0
        if (date_st.month+1) == 2 && ((date_st.year % 4 ==0 && date_st.year % 100 != 0)|| date_st.year % 400 ==0)
          #leap year
          date_en = Time.mktime(date_st.year, date_st.month+1,days_in_month(date_st.month+1) + 1,21) 
        else
          date_en = Time.mktime(date_st.year, date_st.month+1,days_in_month(date_st.month+1),21) 
        end       
      else
        date_en = end_date
      end
      #date_st = Time.mktime(date_st.year, date_st.month + 1,1,6) 
      if date_st.month == 12
        date_st = Time.mktime(date_st.year + 1,  1,1,6) 
      else
        date_st = Time.mktime(date_st.year, date_st.month + 1,1,6)  
      end
    end
    return summary_all
  end

  
  #================================================================================================
  # Function    : get_exit_statistic
  # Input       : Date, time, ts
  # Description : Get data for table Exit statistic
  # Developer     : Hungln
  # Date      : 17/03/2014
  #================================================================================================   
  def self.get_entry_exit_statistic (start_date, end_date, trade_start_time, trade_end_time, ts=nil, launch_type=0, iso_order = 0)
    # Filter by date
    start_month = start_date.month
    end_month = end_date.month
    year = start_date.year
    date_st = start_date
    date_en = end_date
    if start_date.month < end_date.month
      if start_date.month == 2 && ((start_date.year % 4 == 0 && start_date.year % 100 !=0)|| start_date.year % 400 == 0)
        #leap year
        date_en = Time.mktime(start_date.year, start_date.month,days_in_month(start_date.month) + 1,21) 
      else
        date_en = Time.mktime(start_date.year, start_date.month,days_in_month(start_date.month),21) 
      end     
    end
    
    #init variable returned
    ecns = ecn_list_plus_bzx_smart_route().collect{|ecn| ecn[0] if ecn.last }
    ecn_name_mapping = {}
    ecn_list_plus_bzx_smart_route().each{ |ecn|
      ecn_name_mapping[ecn[2]] = ecn[0]
    }
    
    ecns.delete_if{|x| x == nil}
    summary_all = {}
    ecns.each{ |ecn|
      summary_all[ecn] = {"entry_succ" => 0,
                "entry_miss" => 0,
                "exit_succ" =>0,
                "exit_miss" =>0}
    }
    #subtotal rows
    summary_all["0"] = {"entry_succ" => 0,
                "entry_miss" => 0,
                "exit_succ" =>0,
                "exit_miss" =>0}
    k = 0
    if end_date.year > start_date.year
      k = end_date.year - start_date.year
      end_month = end_month + 12 * k
    end
    # Connect to database by each month
    while start_month <= end_month 
        # Name database
      db_name_suffix = "#{Time.now.year}#{Time.now.month.to_s.rjust(2,'0')}"    
      if start_month <= 12
        db_name_suffix = "#{year}#{start_month.to_s.rjust(2,'0')}"
      else        
        if k == 1         
          db_name_suffix = "#{year + 1}#{(start_month - 12).to_s.rjust(2,'0')}"       
        elsif k == 2
          if start_month <= 24
            db_name_suffix = "#{year + 1}#{(start_month - 12).to_s.rjust(2,'0')}"
          else
            db_name_suffix = "#{year + 2}#{(start_month - 24).to_s.rjust(2,'0')}"
          end
        end
      end  
      # Connect to database of AS
      get_database_connection(db_name_suffix, "as")
      start_month = start_month + 1
      conditions = ["date >= ? and date <= ? and ts_id <> -1 "]
      condition_values = [date_st, date_en]
      # Conditions TS
      if ts != "-1"
        if ts.index(",")
          con = ""
          ts.split(",").each do |t|
            condition_values << t.to_i
            con += "?,"
          end
          con = con[0..(con.length - 2)]
          conditions << "ts_id in (" + con + ")"
        else
          conditions << "ts_id=?"
          condition_values << ts
        end
      end
      
      # Conditions time stamp
      if trade_start_time  && trade_end_time
        conditions << "(time_stamp between '#{trade_start_time}' and '#{trade_end_time}')" 
      end 
      # Conditions iso_order
      if iso_order == 1
        conditions << "(is_iso = 'Y' and order_id >= 1000000)"
      elsif iso_order == 2
        conditions << "(is_iso = 'N' and order_id >= 1000000)"
      end     
      # Conditions launch
      if launch_type.to_i != 0
        conditions << build_conditions_for_ecn_launch(launch_type)   
      end
      summary = {}
      ecns.each{ |ecn|
        summary[ecn] = {"entry_succ" => 0,
                "entry_miss" => 0,
                "exit_succ" =>0,
                "exit_miss" =>0}
      }
      
      #subtotal rows
      summary["0"] = {"entry_succ" => 0,
                "entry_miss" => 0,
                "exit_succ" =>0,
                "exit_miss" =>0}
      
      new_orders = NewOrder.find :all,
                 :conditions => [conditions.join(' and '), condition_values].flatten
      
      # when filter by launch
      if (launch_type.to_i != 0) # calculate exit when filter by launch
        cross_id = []
        # Conditions get launch by exit
        conditions_launch_exit = ["date >= ? and date <= ? and ts_id <> -1 "]
        # Conditions launch
        if launch_type.to_i != 0
          conditions_launch_exit << build_conditions_for_ecn_launch(launch_type)   
        end
        condition_values_exit = [date_st, date_en]
        # Conditions TS
        if ts != "-1"
          if ts.index(",")
            con = ""
            ts.split(",").each do |t|
              condition_values_exit << t.to_i
              con += "?,"
            end
            con = con[0..(con.length - 2)]
            conditions_launch_exit << "ts_id in (" + con + ")"
          else
            conditions_launch_exit << "ts_id=?"
            condition_values_exit << ts
          end
        end
        
        # Conditions iso_order
        conditions_iso = ""
        if iso_order == 1
          conditions_iso = " and (is_iso = 'Y' and order_id >= 1000000)"
        elsif iso_order == 2
          conditions_iso =" and (is_iso = 'N' and order_id >= 1000000)"
        end
          
        new_orders_exit = NewOrder.find :all,
                 :conditions => [conditions_launch_exit.join(' and '), condition_values_exit].flatten
        
        new_orders_exit.each { |x|
          cross_id << "'" + x.ts_id.to_s + " " + x.cross_id.to_s + " " + x.date.to_s + "'"
        }
        
        cross_ids = cross_id.join(',')
        if cross_id.size > 0
          sql_str = "select * from new_orders where date >= '#{date_st}' and date <= '#{date_en}' and (time_stamp between '#{trade_start_time}' and '#{trade_end_time}') #{conditions_iso} and ts_id <> -1 "
          sql_where = "and entry_exit = 'Exit' and (ts_id || ' ' ||cross_id || ' ' || date) in (#{cross_ids}) "
          sql_str = "#{sql_str} #{sql_where} order by oid"
          # Get data for Table Exit
          exit_launch = NewOrder.find_by_sql("#{sql_str}")
          exit_launch.each do |order|
            ecn_map = ""
            ecn_map = order.ecn.strip
            if ecn_map == "BATZ" && order.routing_inst
              if order.routing_inst.strip == "RL2"
                ecn_map = "BZSR"
              end
            end   
            if order.shares.to_i > order.left_shares.to_i
              summary[ecn_name_mapping[ecn_map]]["exit_succ"] +=  order.shares.to_i - order.left_shares.to_i
              summary[ecn_name_mapping[ecn_map]]["exit_miss"] += order.left_shares.to_i 
            else
              summary[ecn_name_mapping[ecn_map]]["exit_miss"] += order.left_shares.to_i
            end
          end           
        end
        # Get data for Table Entry
        new_orders.each do |order|  
          ecn_map = order.ecn.strip
          if ecn_map == "BATZ" && order.routing_inst
            if order.routing_inst.strip == "RL2"
              ecn_map = "BZSR"
            end
          end    
          if order[:entry_exit].strip == "Entry"
            if order.shares.to_i > order.left_shares.to_i
              summary[ecn_name_mapping[ecn_map]]["entry_succ"] +=  order.shares.to_i - order.left_shares.to_i
              summary[ecn_name_mapping[ecn_map]]["entry_miss"] += order.left_shares.to_i
            else
              summary[ecn_name_mapping[ecn_map]]["entry_miss"] += order.left_shares.to_i
            end
          end 
        end
              
      else  # when filter by launch is ALL
        new_orders.each do |order|
          ecn_map = ""
          ecn_map = order.ecn.strip
          if ecn_map == "BATZ" && order.routing_inst
            if  order.routing_inst.strip == "RL2"
              ecn_map = "BZSR"
            end
          end
          if order.entry_exit.strip == "Exit"
            if order.shares.to_i > order.left_shares.to_i
              summary[ecn_name_mapping[ecn_map]]["exit_succ"] +=  order.shares.to_i - order.left_shares.to_i
              summary[ecn_name_mapping[ecn_map]]["exit_miss"] += order.left_shares.to_i
            #elsif (order.price.to_f != order.ack_price.to_f && (order.ecn.strip == 'OUCH'))
              # not calculate misses have re-price    
            else
              summary[ecn_name_mapping[ecn_map]]["exit_miss"] += order.left_shares.to_i
            end
          else
            if order.shares.to_i > order.left_shares.to_i
              summary[ecn_name_mapping[ecn_map]]["entry_succ"] +=  order.shares.to_i - order.left_shares.to_i
              summary[ecn_name_mapping[ecn_map]]["entry_miss"] += order.left_shares.to_i
            else
              summary[ecn_name_mapping[ecn_map]]["entry_miss"] += order.left_shares.to_i
            end
          end
        end
      end
      
      summary.each do |ecn, v|
        if ecn != "0"
          summary["0"]["exit_succ"] += v["exit_succ"]
          summary["0"]["exit_miss"] += v["exit_miss"]
          summary["0"]["entry_succ"] += v["entry_succ"]
          summary["0"]["entry_miss"] += v["entry_miss"]
        end
      end
      summary_all.each do |ecn, v|
        summary_all[ecn]["exit_succ"] = summary_all[ecn]["exit_succ"] + summary[ecn]["exit_succ"]
        summary_all[ecn]["exit_miss"] = summary_all[ecn]["exit_miss"] + summary[ecn]["exit_miss"]
        summary_all[ecn]["entry_succ"] = summary_all[ecn]["entry_succ"] + summary[ecn]["entry_succ"]
        summary_all[ecn]["entry_miss"] = summary_all[ecn]["entry_miss"] + summary[ecn]["entry_miss"]
      end
  
      if end_month - start_month > 0
        if (date_st.month+1) == 2 && ((date_st.year % 4 ==0 && date_st.year % 100 != 0)|| date_st.year % 400 ==0)
          #leap year
          date_en = Time.mktime(date_st.year, date_st.month+1,days_in_month(date_st.month+1) + 1,21) 
        else
          date_en = Time.mktime(date_st.year, date_st.month+1,days_in_month(date_st.month+1),21) 
        end       
      else
        date_en = end_date
      end
      #date_st = Time.mktime(date_st.year, date_st.month + 1,1,6) 
      if date_st.month == 12
        date_st = Time.mktime(date_st.year + 1,  1,1,6) 
      else
        date_st = Time.mktime(date_st.year, date_st.month + 1,1,6)  
      end
    end
    
    return summary_all
  end
  
end
