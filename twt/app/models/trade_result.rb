require 'util'

class TradeResult < ActiveRecord::Base
  set_table_name :trade_results
  @count = 0
  
    
  #================================================================================================
  # Method    : Function build conditions of Cross Size
  # Developer : 
  #================================================================================================
  def self.build_conditions_for_cross_size cross_size = ''
    conditions_cross_size = ""
    if cross_size.to_s.include? "," # Filter by multi_cross
      if cross_size.to_s.include? "11"  # Filter by cross size >.10
        conditions_cross_size << "((spread in (#{cross_size})) or (spread > 10))"
      else
        conditions_cross_size << "spread in (#{cross_size}) "
      end
    else 
      if cross_size.to_i != 0
        if (cross_size.to_i <= 10)
          conditions_cross_size << "spread = #{cross_size.to_i}"
        else
          conditions_cross_size << "spread > 10"
        end
      end
    end
    return conditions_cross_size
  end
#================================================================================================
  # Method    : Function build conditions of ECN Entry/Exit
  # Developer : 
  #================================================================================================
  def self.build_conditions_for_ecn_entry_exit start_time, end_time, entry_exit = '', 
    conditions_entry_exit = ""
    mane_ecn_from = ""
    conditions = ""
    ecn = 0
    entry_exit.split(",").each do |ecn_filter|
      ecn += 1
      if ecn == 1
        conditions_entry_exit << "SELECT distinct (EXTRACT(DAY FROM #{ecn_filter}.date) * 10000000 + #{ecn_filter}.ts_id * 100000 + #{ecn_filter}.cross_id) FROM new_orders  AS #{ecn_filter}"
        mane_ecn_from = "#{ecn_filter}"
      else
        conditions_entry_exit <<  " INNER JOIN new_orders AS #{ecn_filter} ON #{mane_ecn_from}.date = #{ecn_filter}.date AND #{mane_ecn_from}.ts_id = #{ecn_filter}.ts_id AND #{mane_ecn_from}.cross_id = #{ecn_filter}.cross_id"
      end
      if ecn_filter.size > 2
        routing_condition = ""
        ecn_name = ecn_filter[0, ecn_filter.size - 2].upcase            
        if ecn_filter[-2, 2] == "en"
          entry_exit_filter = "Entry"
        else
          entry_exit_filter = "Exit"
          if ecn_name == "BZSR"
            ecn_name = "BATZ"
            routing_condition = "and trim(#{ecn_filter}.routing_inst) = 'RL2'"
          elsif ecn_name == "BATZ"
            routing_condition = "and trim(#{ecn_filter}.routing_inst) != 'RL2'"
          end
        end
        conditions << " and #{ecn_filter}.ecn = '#{ecn_name}' and #{ecn_filter}.entry_exit = '#{entry_exit_filter}' #{routing_condition}"
      end
    end
   
    conditions_entry_exit << " where #{mane_ecn_from}.date between '#{start_time}' and  '#{end_time}' #{conditions} "
    return conditions_entry_exit  
  end
  #================================================================================================
  # Method    : Function build conditions of Detected Shares, Launch Shares
  # Developer : 
  #================================================================================================
  def self.build_conditions_for_filter_by_shares filter_conditions = '', filter_name = ''
    conditions_filters = ""
    filter_conditions.split(",").each do |filter_size|
      if conditions_filters != "" 
        conditions_filters << " OR "
      end
      
      case filter_size.to_i
      when -100
        conditions_filters << "(#{filter_name} < 100)"  
      when 100
        conditions_filters << "(#{filter_name} between 100 and 199)"  
      when 200
        conditions_filters << "(#{filter_name} between 200 and 299)"   
      when 300
        conditions_filters << "(#{filter_name} between 300 and 499)"  
      when 500
        conditions_filters << "(#{filter_name} between 500 and 999)"  
      when 1000
        conditions_filters << "(#{filter_name} between 1000 and 1999)"  
      when 2000
        conditions_filters << "(#{filter_name} between 2000 and 4999)"   
      when 5000
        conditions_filters << "(#{filter_name} between 5000 and 9999)"  
      when 10000
        conditions_filters << "(#{filter_name} between 10000 and 19999)"    
      else   #20000
        conditions_filters << "(#{filter_name} >= 20000)"   
      end
    end

    if conditions_filters != ""     
      return " AND (#{conditions_filters})"
    else
      return ""
    end
  end  
  
  
  #================================================================================================
  # Method    : Function build conditions of  Detected Shares, Launch Shares
  # Developer : 
  #================================================================================================
  def self.check_conditions_of_filter_by_shares_for_netb filter_list = '', filter_shares = 0
    check_filter = 0
    filter_list.split(",").each do |item|
      case item.to_i
      when -100 # Filter by filter_shares < 100
        next if filter_shares >= 100 or filter_shares < 0 
      when 100
        next if filter_shares < 100 or filter_shares >= 200 
      when 200
        next if filter_shares < 200 or filter_shares >= 300 
      when 300
        next if filter_shares < 300 or filter_shares >= 500 
      when 500
        next if filter_shares < 500 or filter_shares >= 1000 
      when 1000
        next if filter_shares < 1000 or filter_shares >= 2000 
      when 2000
        next if filter_shares < 2000 or filter_shares >= 5000 
      when 5000
        next if filter_shares < 5000 or filter_shares >= 10000 
      when 10000
        next if filter_shares < 10000 or filter_shares >= 20000 
      else   # Filter by filter_shares >= 20000
        next if filter_shares < 20000
      end
      
      check_filter = 1 
    end
    
    return check_filter
    
  end
  
  #================================================================================================
  # Method    : filter_trade_statistic_twt2_2
  # Developer     : 
  #================================================================================================
  def self.filter_trade_statistic_twt2_2 start_time, end_time, time_from, time_to, share_size = '', expected_pl = 0, symbol ='',  bought_shares = 0, sold_shares = 0, ecn_filter = '0', ts_id = '0', cecn ='0', notime = false, iso_order = '0', exclusive = 0, cross_size = '', launch_shares = ''
    
    conditions_cross_size = build_conditions_for_cross_size(cross_size)
    share_size_conditions = share_size
    if share_size.to_s.include? "," # Filter by multi Detected Shares
      share_size_conditions = TradeResult.build_conditions_for_filter_by_shares(share_size, 'expected_shares')
    end 
    
    launch_shares_conditions = launch_shares
    if launch_shares.to_s.include? "," # Filter by multi Launch Shares
      launch_shares_conditions = TradeResult.build_conditions_for_filter_by_shares(launch_shares, 'launch_shares')
    end 
    # Check filter for NET.B: Filter by ECN Entry/Exit, Iso, Bt Shares Sd Shares then NET.B =0
    check_bought_shares = -1
    check_sold_shares = -1
    if bought_shares == -1
      bought_shares = 0
      check_bought_shares = 0
      end
    if sold_shares == -1
      sold_shares = 0
      check_sold_shares = 0
      end
    
    ret_all = {
        "stuck_loss"=>[0,0,0,0,0,0,0.0,2, "stuck_loss"], #[count, expected profit, real profit]
        "stuck_profit"=>[0,0,0,0,0,0,0.0,3, "stuck_profit"], 
        "loss" =>[0,0,0,0,0,0,0.0,4, "loss"] ,
        "profit_under_expected" => [0,0,0,0,0,0,0.0,5,"profit_under_expected"],
        "profit_equal_to_or_above_expected" => [0,0,0,0,0,0,0.0,6,"profit_equal_to_or_above_expected"],
        "total" =>[0,0,0,0,0,0,0.0,7,"total"],
        "missed" =>[0,0,0,0,0,0,0.0,0,"missed"],      
        "repriced" =>[0,0,0,0,0,0,0.0,1,"repriced"],
        "Skipped_Ss" => [0,0,0,0,0,0,0.0,8,"Skipped_Ss"]
            
      }
  
    date_st = start_time
    count_ss = 0
    count_ss_expected = 0
    sum_expected = 0.0
    count_launch_shares = 0
    if time_from.class == Time
      time_from = time_from.strftime("%H:%M")
    end
    if time_to.class == Time
      time_to = time_to.strftime("%H:%M")
    end
    
    #check number TS
    check_ts = 0
    if ts_id.to_s.include? ","
      check_ts = 1
    end
    
    while date_st <= end_time
      
      db_name_suffix = "#{date_st.year}#{date_st.month.to_s.rjust(2,'0')}"
      get_database_connection(db_name_suffix, "as")
      
      timeInput = Time.now.to_f
      ret = TradeResult.find_by_sql(["select cal_filter_trade_statistic_byecn2_twt2(?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?, ?, ?, ?, ?);", date_st, date_st, time_from, time_to, share_size_conditions, expected_pl, symbol,  bought_shares, sold_shares, ecn_filter.to_s, ts_id, cecn.to_s, iso_order.to_s, exclusive.to_i, conditions_cross_size, launch_shares_conditions ]).first.cal_filter_trade_statistic_byecn2_twt2.gsub(/\(|\)/, "");
    
      name_symbol = symbol
      start_time_in_date  = date_st.year.to_s << "-" << date_st.month.to_s.rjust(2,'0') << "-" << date_st.day.to_s.rjust(2,'0')
      
      end_time_in_date  = end_time.year.to_s << "-" << end_time.month.to_s.rjust(2,'0') << "-" << end_time.day.to_s.rjust(2,'0')
      
      # Check filter for NET.B
      if ((check_ts == 0 && ts_id.to_i != -1) || check_ts != 0)  && ecn_filter == '0' && iso_order == '0' && check_bought_shares == 0 && check_sold_shares == 0
        result = TradeResult.calculate_ss(start_time,end_time,time_from,time_to,expected_pl,symbol,ts_id, share_size, cross_size, cecn, launch_shares)
        count_ss_expected = result[0]
        sum_expected = result[1]
        count_launch_shares = result[2]
        
      end
      
      
      count_miss,count_repriced,count_stuck_loss,count_stuck_profit,count_loss,count_profit_under,count_profit_above,count_total,shares_traded_miss,shares_traded_repriced,shares_traded_stuck_loss,shares_traded_stuck_profit,shares_traded_loss,shares_traded_profit_under,shares_traded_profit_above,shares_traded_total,pm_shares_traded_miss,pm_shares_traded_repriced,pm_shares_traded_stuck_loss,pm_shares_traded_stuck_profit,pm_shares_traded_loss,pm_shares_traded_profit_under,pm_shares_traded_profit_above,pm_shares_traded_total,expected_miss,expected_repriced,expected_stuck_loss,expected_stuck_profit,expected_loss,expected_profit_under,expected_profit_above,expected_total,real_miss,real_stuck_loss,real_stuck_profit,real_loss,real_profit_under,real_profit_above,real_total,pm_miss,pm_stuck_loss,pm_stuck_profit,pm_loss,pm_profit_under,pm_profit_above,pm_total,launch_shares_miss,launch_shares_repriced,launch_shares_stuck_loss,launch_shares_stuck_profit,launch_shares_loss,launch_shares_profit_under,launch_shares_profit_above,launch_shares_total = ret.split(",")
      
      ret = {
          "stuck_loss"=>[count_stuck_loss.to_i,launch_shares_stuck_loss.to_i,shares_traded_stuck_loss.to_i,pm_shares_traded_stuck_loss.to_i,expected_stuck_loss.to_f,real_stuck_loss.to_f,pm_stuck_loss.to_f, 2, "stuck_loss"], #[count, expected profit, real profit]
          "stuck_profit"=>[count_stuck_profit.to_i,launch_shares_stuck_profit.to_i,shares_traded_stuck_profit.to_i,pm_shares_traded_stuck_profit.to_i,expected_stuck_profit.to_f,real_stuck_profit.to_f,pm_stuck_profit.to_f, 3, "stuck_profit"], #[count, expected profit, real profit]
          "loss" =>[count_loss.to_i,launch_shares_loss.to_i,shares_traded_loss.to_i,pm_shares_traded_loss.to_i,expected_loss.to_f,real_loss.to_f,0.0,4, "loss"] ,
          "profit_under_expected" => [count_profit_under.to_i,launch_shares_profit_under.to_i,shares_traded_profit_under.to_i,pm_shares_traded_profit_under.to_i,expected_profit_under.to_f,real_profit_under.to_f,0.0,5,"profit_under_expected"],
          "profit_equal_to_or_above_expected" => [count_profit_above.to_i,launch_shares_profit_above.to_i,shares_traded_profit_above.to_i,pm_shares_traded_profit_above.to_i,expected_profit_above.to_f,real_profit_above.to_f,0.0,6,"profit_equal_to_or_above_expected"],
          "total" =>[0,0,0,0,0,0,0.0,7,"total"],
          "missed" =>[count_miss.to_i,launch_shares_miss.to_i,shares_traded_miss.to_i,pm_shares_traded_miss.to_i,expected_miss.to_f,real_miss.to_f,0.0,0,"missed"],     
          "repriced" =>[count_repriced.to_i,launch_shares_repriced.to_i,shares_traded_repriced.to_i,pm_shares_traded_repriced.to_i,expected_repriced.to_f,0,0.0,1,"repriced"],
          
        } 
      
      ret.each{ |k,v|
      
        if k != "total"       
          ret["total"][0] += v[0]  if k !="repriced"  
          ret["total"][1] += v[1].to_f if k !="repriced"       
          ret["total"][2] += v[2] if k !="repriced" 
          ret["total"][3] += v[3] if k !="repriced" 
          ret["total"][4] += v[4].to_f if k !="repriced" 
          ret["total"][5] += v[5].to_f
          ret["total"][6] += v[6].to_f
        end
      }
    

      ret_all["stuck_loss"][0] += ret["stuck_loss"][0]
      ret_all["stuck_loss"][1] += ret["stuck_loss"][1]
      ret_all["stuck_loss"][2] += ret["stuck_loss"][2]
      ret_all["stuck_loss"][3] += ret["stuck_loss"][3]
      ret_all["stuck_loss"][4] += ret["stuck_loss"][4]
      ret_all["stuck_loss"][5] += ret["stuck_loss"][5]
      ret_all["stuck_loss"][6] += ret["stuck_loss"][6]
                                      
      ret_all["stuck_profit"][0] += ret["stuck_profit"][0]
      ret_all["stuck_profit"][1] += ret["stuck_profit"][1]
      ret_all["stuck_profit"][2] += ret["stuck_profit"][2]
      ret_all["stuck_profit"][3] += ret["stuck_profit"][3]
      ret_all["stuck_profit"][4] += ret["stuck_profit"][4]
      ret_all["stuck_profit"][5] += ret["stuck_profit"][5]
      ret_all["stuck_profit"][6] += ret["stuck_profit"][6]

      ret_all["loss"][0] += ret["loss"][0]
      ret_all["loss"][1] += ret["loss"][1]
      ret_all["loss"][2] += ret["loss"][2]
      ret_all["loss"][3] += ret["loss"][3]
      ret_all["loss"][4] += ret["loss"][4]
      ret_all["loss"][5] += ret["loss"][5]

      ret_all["profit_under_expected"][0] += ret["profit_under_expected"][0]
      ret_all["profit_under_expected"][1] += ret["profit_under_expected"][1]
      ret_all["profit_under_expected"][2] += ret["profit_under_expected"][2]
      ret_all["profit_under_expected"][3] += ret["profit_under_expected"][3]
      ret_all["profit_under_expected"][4] += ret["profit_under_expected"][4]
      ret_all["profit_under_expected"][5] += ret["profit_under_expected"][5]

      ret_all["profit_equal_to_or_above_expected"][0] += ret["profit_equal_to_or_above_expected"][0]
      ret_all["profit_equal_to_or_above_expected"][1] += ret["profit_equal_to_or_above_expected"][1]
      ret_all["profit_equal_to_or_above_expected"][2] += ret["profit_equal_to_or_above_expected"][2]
      ret_all["profit_equal_to_or_above_expected"][3] += ret["profit_equal_to_or_above_expected"][3]
      ret_all["profit_equal_to_or_above_expected"][4] += ret["profit_equal_to_or_above_expected"][4]
      ret_all["profit_equal_to_or_above_expected"][5] += ret["profit_equal_to_or_above_expected"][5]
      
      ret_all["total"][0] += ret["total"][0]
      ret_all["total"][1] += ret["total"][1]
      ret_all["total"][2] += ret["total"][2]
      ret_all["total"][3] += ret["total"][3]
      ret_all["total"][4] += ret["total"][4]
      ret_all["total"][5] += ret["total"][5]
      ret_all["total"][6] += ret["total"][6]

      ret_all["missed"][0] += ret["missed"][0]
      ret_all["missed"][1] += ret["missed"][1]
      ret_all["missed"][2] += ret["missed"][2]
      ret_all["missed"][3] += ret["missed"][3]
      ret_all["missed"][4] += ret["missed"][4]
      ret_all["missed"][5] += ret["missed"][5]

      ret_all["repriced"][0] += ret["repriced"][0]
      ret_all["repriced"][1] += ret["repriced"][1]
      ret_all["repriced"][2] += ret["repriced"][2]  
      ret_all["repriced"][3] += ret["repriced"][3]
      ret_all["repriced"][4] += ret["repriced"][4]    
      ret_all["repriced"][5] += ret["repriced"][5]          
      
      #increase 1 day
      #date_st = Time.mktime(date_st.year, date_st.month,date_st.day + 1) 
    
      date_st = date_st + 86400 # 86400= 60*60*24
    end
    
    # KhanhTT add
    
    ret_all["Skipped_Ss"] = [count_ss_expected.to_i,count_launch_shares,0,0,sum_expected.to_f,0,0.0,8,"Skipped_Ss"]
    #khanhTT end add

    ret_all

    end

  #===============================================================================================
  # Method : calculate the time of skipped_ss
  # Developer : KhanhTT
  # Description : Convert Epoch time to time format hour-minutes-seconds
  #===============================================================================================
  def self.convert_time_epoch (time_stamp)
    arr_time = Time.at(time_stamp.to_i / 1000000).to_a
    result = Time.utc(*arr_time)
    return result
  end
    
  #===============================================================================================
  # Method  : calculate_netb
  # Developer : KhanhTT
  # Description : Used to cal the value of NETB
  #================================================================================================
  def self.calculate_netb start_time, end_time, time_from, time_to, expected_pl = 0, symbol ='',  ts_id = '0', share_size = '',  cross_size = '', ecn_launch = '', launch_shares = ''
      date_st = start_time
      db_name_suffix = "#{date_st.year}#{date_st.month.to_s.rjust(2,'0')}"      
      get_database_connection(db_name_suffix, "as")
      
      $MIN_TIME_FROM = time_from.split(':')[0].to_i * 3600 + time_from.split(':')[1].to_i * 60
      $MAX_TIME_TO = time_to.split(':')[0].to_i * 3600 + time_to.split(':')[1].to_i * 60
      
      sql_str_stand = ""
      as_cid_standing = []
      # stop process NETB for standing time
      
      date_st = start_time
      count_ss = 0
      count_ss_expected = 0
      sum_expected = 0.0
      cross_netb = []
      @arr_netb = []
      as_cross_id = 0
      name_ts = ts_id
      name_symbol = symbol
      #check number TS
      check_ts = 0
      if ts_id.to_s.include? ","
        check_ts = 1
      end
      start_time_in_date  = date_st.year.to_s << "-" << date_st.month.to_s.rjust(2,'0') << "-" << date_st.day.to_s.rjust(2,'0')
      end_time_in_date = end_time.year.to_s << "-" << end_time.month.to_s.rjust(2,'0') << "-" << end_time.day.to_s.rjust(2,'0')
      
      sql_ask_bids_conditions = ""
      sql_raw_crosses_conditions = ""
      if check_ts == 0
        if symbol == '' and ts_id.to_i == 0
          sql_ask_bids_conditions = "date >= '#{start_time_in_date}' and date <= '#{end_time_in_date}'"
          sql_raw_crosses_conditions = "date >= '#{start_time_in_date}' and date <= '#{end_time_in_date}'"
        else
          if symbol != '' and ts_id.to_i != 0
            sql_ask_bids_conditions = "date >= '#{start_time_in_date}' and date <= '#{end_time_in_date}' AND ts_id +1 = #{name_ts}"
            sql_raw_crosses_conditions = "date >= '#{start_time_in_date}' and date <= '#{end_time_in_date}' AND ts_id + 1 = #{name_ts} AND symbol = '#{name_symbol}'"
          else
            if symbol == ''
              sql_ask_bids_conditions = "date >= '#{start_time_in_date}' and date <= '#{end_time_in_date}' AND ts_id + 1 = #{name_ts}"
              sql_raw_crosses_conditions = "date >= '#{start_time_in_date}' and date <= '#{end_time_in_date}' AND ts_id + 1 = #{name_ts}"
            else
              sql_ask_bids_conditions = "date >= '#{start_time_in_date}' and date <= '#{end_time_in_date}'"
              sql_raw_crosses_conditions = "date >= '#{start_time_in_date}' and date <= '#{end_time_in_date}' AND symbol = '#{name_symbol}'"
            end 
          end 
        end
        
      else
        if symbol == ''    
          sql_ask_bids_conditions = "date >= '#{start_time_in_date}' and date <= '#{end_time_in_date}' AND ts_id + 1 in (#{name_ts})"
          sql_raw_crosses_conditions = "date >= '#{start_time_in_date}' and date <= '#{end_time_in_date}' AND ts_id +1 in (#{name_ts})"     
        else
          sql_ask_bids_conditions = "date >= '#{start_time_in_date}' and date <= '#{end_time_in_date}' AND ts_id + 1 in (#{name_ts})"
          sql_raw_crosses_conditions = "date >= '#{start_time_in_date}' and date <= '#{end_time_in_date}' AND ts_id + 1 in (#{name_ts}) AND symbol = '#{name_symbol}'"
        
        end
      end
      
      ask_shares = 0
      ask_price = 0.0
      
      sql_str = "WITH tmp_ask_bids AS (SELECT ts_cid, ts_id, date, sum(shares) as ab_shares, sum(price * shares)/sum(shares) as ab_price, ask_bid FROM ask_bids WHERE #{sql_ask_bids_conditions} GROUP BY date, ts_cid, ts_id, ask_bid), tmp_raw_crosses AS (select date, ts_id, ts_cid, as_cid, symbol, cast(split_part(raw_content,' ',3) as numeric ) as time_stamp, raw_content from raw_crosses WHERE #{sql_raw_crosses_conditions} and raw_content like '5%' AND ((to_char(to_timestamp(split_part(raw_content,' ',3)::numeric),'HH24:MI:SS'))::time without time zone) between ('#{time_from}'::time without time zone) and ('#{time_to}'::time without time zone)), ecn_launch_raw_crosses AS (select ts_cid, ts_id, split_part(raw_crosses.raw_content, ' ', 6)::numeric as ecn_launch, split_part(raw_crosses.raw_content, ' ', 7)::numeric as ask_bid_raw_crosses from raw_crosses WHERE #{sql_raw_crosses_conditions} and raw_content like '2%' AND ((to_char(to_timestamp(split_part(raw_content,' ',3)::numeric),'HH24:MI:SS'))::time without time zone) between ('#{time_from}'::time without time zone) and ('#{time_to}'::time without time zone)), time_raw_crosses AS (select split_part(raw_content,' ',10)::numeric as time_stamp, ts_id, ts_cid from raw_crosses WHERE #{sql_raw_crosses_conditions} and raw_content like '2%' AND ((to_char(to_timestamp(split_part(raw_content,' ',3)::numeric),'HH24:MI:SS'))::time without time zone) between ('#{time_from}'::time without time zone) and ('#{time_to}'::time without time zone)) SELECT tmp_raw_crosses.date, tmp_raw_crosses.ts_id, tmp_raw_crosses.ts_cid, tmp_raw_crosses.as_cid, tmp_raw_crosses.symbol, time_raw_crosses.time_stamp as time_stamp_netb, tmp_raw_crosses.time_stamp, tmp_ask_bids.ask_bid, tmp_ask_bids.ab_shares, tmp_ask_bids.ab_price, tmp_raw_crosses.raw_content, ecn_launch_raw_crosses.ecn_launch, ecn_launch_raw_crosses.ask_bid_raw_crosses FROM tmp_raw_crosses JOIN tmp_ask_bids ON tmp_ask_bids.ts_cid = tmp_raw_crosses.ts_cid AND tmp_ask_bids.ts_id = tmp_raw_crosses.ts_id JOIN time_raw_crosses ON time_raw_crosses.ts_cid = tmp_raw_crosses.ts_cid AND time_raw_crosses.ts_id = tmp_raw_crosses.ts_id  JOIN ecn_launch_raw_crosses ON ecn_launch_raw_crosses.ts_cid = tmp_raw_crosses.ts_cid AND ecn_launch_raw_crosses.ts_id = tmp_raw_crosses.ts_id ORDER BY tmp_raw_crosses.date, tmp_raw_crosses.ts_id, tmp_raw_crosses.symbol, tmp_raw_crosses.time_stamp, tmp_raw_crosses.as_cid, tmp_ask_bids.ask_bid;"
    
      ret_rows2 = RawCross2.find_by_sql(" #{sql_str} ")
       
      start_sec = 0
      flag = 0
      start_sym = ''
      start_ts = -1
      launch_shares_size = 0
      ret_rows2.each { |value|
        if value[:ask_bid].strip == "ASK"
          flag = 0
          if value.ask_bid_raw_crosses.to_i == 0
            launch_shares_size = value.ab_shares.to_i
          end
          if start_sec == 0 or start_sym == ''
            flag = 1
            start_sec = value.time_stamp.to_i
            start_sym = value.symbol.to_s
            start_ts = value.ts_id
            trade_server_id = value.ts_id
            cross_id = value.ts_cid
            as_cross_id = value.as_cid
            ask_shares = value.ab_shares.to_i
            ask_price = value.ab_price.to_f
          else 
            flag = 0
          end
          
          if (value.time_stamp.to_i - start_sec.to_i > 30) or (value.symbol != start_sym) or (start_ts != value.ts_id)
            flag = 1
            cross_id = value.ts_cid
            trade_server_id = value.ts_id
            start_sec = value.time_stamp.to_i
            start_sym = value.symbol.to_s
            start_ts = value.ts_id
            as_cross_id = value.as_cid
            ask_shares = value.ab_shares.to_i
            ask_price = value.ab_price.to_f
          end 
          
        elsif (flag == 1) 
          if value.ask_bid_raw_crosses.to_i == 1
            launch_shares_size = value.ab_shares.to_i
          end
          bid_shares = value.ab_shares.to_i
          bid_price = value.ab_price.to_f
          cross = ((bid_price - ask_price + 0.0001)*100).to_i
          expected_t = [ask_shares , bid_shares].min * (cross * 0.01)
          
          # Get detected shares and check
          detected_shares = [ask_shares , bid_shares].min
          
          # Check cross size
          check_detected_shares = 0
          if share_size.to_s == "" || share_size.to_s == "0"
            check_detected_shares = 1
          end
          if share_size.to_s != "" && share_size.to_s != "0"
            check_detected_shares = check_conditions_of_filter_by_shares_for_netb(share_size.to_s, detected_shares.to_i)
          end
          next if check_detected_shares == 0     
          
          # Check Launch Shares
          check_launch_shares = 0
          if launch_shares.to_s == "" || launch_shares.to_s == "0"
            check_launch_shares = 1
          end
          if launch_shares.to_s != "" && launch_shares.to_s != "0"
            check_launch_shares = check_conditions_of_filter_by_shares_for_netb(launch_shares.to_s, launch_shares_size.to_i)
          end
          next if check_launch_shares == 0
          
          # Check cross size
          check_cross_size = 0
          if cross_size.to_s == "" || cross_size.to_s == "0"
            check_cross_size = 1
          end
          if cross_size.to_s != "" && cross_size.to_s != "0"
            cross_size.split(",").each do |item|
              case item.to_i
              when 11 # Filter by cross size >.10
                next if cross <= 10
              else
                next if cross != item.to_i
              end
              check_cross_size = 1
            end 
          end
          next if check_cross_size == 0
          # Get cecn and check
          cecn = value.ecn_launch
          case ecn_launch
          when "0"
            # Do nothing
          else
            tmp_ecn_entry = book_ecns_list
            index_cecn = tmp_ecn_entry.index{|x| x[1].to_s == ecn_launch.to_s}
            next if cecn.to_i != index_cecn.to_i
          end
          if (expected_t > expected_pl)           
            if (as_cid_standing.size == 0 or  (as_cid_standing.size > 0 and as_cid_standing.index(as_cross_id.to_i) != nil )) 
              count_ss_expected += 1
              sum_expected += expected_t.to_f
              cross_netb << as_cross_id.to_i
              cross_netb << value.symbol.to_s
              cross_netb << [ask_shares , bid_shares].min.to_i
              expected_temp = "%.02f" % expected_t
              cross_netb << expected_temp.to_f 
              
              timestamp = TradeResult.convert_time_epoch(value.time_stamp_netb)
              cross_netb << timestamp
              cross_netb << cross_id.to_s
              cross_netb << trade_server_id.to_s
              cross_netb << start_time
              #end of add
              size = value.time_stamp_netb.size
              cross_netb << "%06d" % (value.time_stamp_netb.to_i % 1000000) # get microseconds
              #use to sort timestamp
              cross_netb << value.time_stamp_netb
              cross_id = ""
              trade_server_id = ""
              as_cross_id = ""
              cross_netb << launch_shares_size
              @arr_netb << cross_netb
              cross_netb = []
              ask_shares = 0
              ask_price = 0
              bid_shares = 0
              bid_price = 0
            end
            
          end     
        end 
      }
    
    return @arr_netb
  end

  
  #================================================================================================
  # Method    : filter_trade_statistic_twt2_chart
  # Developer     : 
  # Description   : Used for getting data for the "Trade Statistic by Time Chart"
  #         There are two different ways to get data for chart: 1. Get for day/week/month using hash; 2. get data for other step using array
  #================================================================================================
  def self.filter_trade_statistic_twt2_chart(start_time, end_time, time_from, time_to, share_size = '', expected_pl = 0, symbol ='',  bought_shares = 0, sold_shares = 0, ecn_filter = '0', ts_id = '0', cecn ='0', step_type=5, notime = false, iso_order = '0', exclusive = 0, cross_size = '', launch_shares = '')
    summary = {}
    summary1 = []
    step = step_type  
    day_in_second = 86400   
    conditions_cross_size = build_conditions_for_cross_size(cross_size)
    while start_time <= end_time
      #------------Set up database connection
      db_name_suffix = "#{start_time.year}#{start_time.month.to_s.rjust(2,'0')}"                    
      # lets manually connect to the proper db
      get_database_connection(db_name_suffix, "as")
      d_current = start_time + day_in_second      
      min_time = Time.parse(time_from)
      max_time = Time.parse(time_to)
      
      #----------Calculate information
      if (step == 91)       
        key = Time.mktime(start_time.year, start_time.mon, start_time.mday, min_time.hour, min_time.min)
        
        summary["#{key}"] = filter_trade_statistic_twt2_2(start_time, start_time, min_time, max_time, share_size, expected_pl, symbol,bought_shares, sold_shares, ecn_filter,  ts_id , cecn, notime, iso_order, exclusive.to_i, cross_size, launch_shares)
      
      elsif (step > 91)

        last_day = get_last_day(start_time, step)
        if last_day > end_time  
          last_day = end_time
        end
        
        summary["#{last_day}"] = filter_trade_statistic_twt2_2(start_time, last_day, min_time, max_time, share_size, expected_pl, symbol,bought_shares, sold_shares, ecn_filter,  ts_id , cecn, notime, iso_order, exclusive.to_i, cross_size, launch_shares)         
        d_current = last_day + day_in_second
      else
        records = TradeResult.find_by_sql(["select * from cal_filter_trade_statistic_byecn2_twt2_with_step(?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?, ?,?, ?, ?);", start_time, start_time, time_from, time_to, share_size, expected_pl, symbol,  bought_shares, sold_shares, ecn_filter.to_s, ts_id, cecn.to_s, step, iso_order, exclusive.to_i, conditions_cross_size, launch_shares]);
        
        flag = 0
        records.each do |item|
          if ((item.count_miss.to_i + item.count_repriced.to_i + item.count_stuck_loss.to_i + item.count_stuck_profit.to_i + item.count_loss.to_i + item.count_profit_under.to_i + item.count_profit_above.to_i) > 0)
            value = [item.time_stamp.first(5), item.count_miss.to_i, item.count_repriced.to_i, item.count_stuck_loss.to_i, item.count_stuck_profit.to_i, item.count_loss.to_i,item.count_profit_under.to_i,item.count_profit_above.to_i]
            
            flag = 1
            summary1 << value
          end
        end
        
        if start_time < end_time && flag == 1
          value = [Time.mktime(d_current.year, d_current.mon, d_current.mday).strftime("%m-%d-%Y"), 0, 0, 0, 0, 0, 0, 0]
          summary1 << value
        end
      end     
      
      #Increase the start time for the next processing.     
      start_time = d_current
    end
    
    if step < 90
      return summary1
    else
      return summary
    end 
  end

 
  #================================================================================================
  # Method    : get_last_day
  # Developer     : 
  # Description   : Used for getting the last day of specific week or month
  #================================================================================================
  def self.get_last_day(start_date, step)   
    last_day = start_date
    day_in_second = 86400
    if (step == 92)
      for i in 1..7 do
        temp_day = last_day + day_in_second
        if temp_day.strftime("%W").to_i == start_date.strftime("%W").to_i
          last_day = temp_day
        end
      end
    elsif (step == 93)
      for i in 1..31 do
        temp_day = last_day + day_in_second
        if temp_day.strftime("%m").to_i == start_date.strftime("%m").to_i
          last_day = temp_day
        end
      end
    end

    return last_day
  end

  #================================================================================================
  # Method    : trade_statistic_filter_by_tradetype
  # Input         : start_time - begin date; 
  #               : end_time - end date
  #               : share_size 
  #               : expected_pl
  #               : trade_type
  # Date created  : 10/21/2008
  # Last Modify   : 10/21/2009
  # Description  : calculate trading statistic by given dates
  # Developer     : tuanbd
  #================================================================================================
  def self.days_in_month(month)
    month == 2 ? 28 : 30 + ((month * 1.126).to_i % 2)
  end

  #================================================================================================
  # Method    : `
  # Developer     : khanhtt
  # Description   : use to cal the NETB
  #================================================================================================ 
  def self.calculate_ss start_time, end_time, time_from, time_to, expected_pl = 0, symbol ='', ts_id = '0', share_size = '',  cross_size = '', ecn_launch = '', launch_shares = ''
    # Returns second since epoch which includes microseconds  
    timeInput = Time.now.to_f
    
    $MIN_TIME_FROM = time_from.split(':')[0].to_i * 3600 + time_from.split(':')[1].to_i * 60
    $MAX_TIME_TO = time_to.split(':')[0].to_i * 3600 + time_to.split(':')[1].to_i * 60
    date_st = start_time
    start_time_in_date  = date_st.year.to_s << "-" << date_st.month.to_s.rjust(2,'0') << "-" << date_st.day.to_s.rjust(2,'0')
    end_time_in_date  = end_time.year.to_s << "-" << end_time.month.to_s.rjust(2,'0') << "-" << end_time.day.to_s.rjust(2,'0')
    
    db_name_suffix = "#{date_st.year}#{date_st.month.to_s.rjust(2,'0')}"      
    get_database_connection(db_name_suffix, "as")
    
    count_ss = 0
    count_ss_expected = 0
    count_launch_shares  = 0
    sum_expected = 0.0
    name_ts = ts_id
    name_symbol = symbol
    #check number TS
    check_ts = 0
    if ts_id.to_s.include? ","
      check_ts = 1
    end
    
    sql_ask_bids_conditions = ""
    sql_raw_crosses_conditions = ""
    
    if check_ts == 0
      if symbol == '' and ts_id.to_i == 0
        sql_ask_bids_conditions = "date = '#{start_time_in_date}'"
        sql_raw_crosses_conditions = "date = '#{start_time_in_date}'"
      else
        if symbol != '' and ts_id.to_i != 0
          sql_ask_bids_conditions = "date = '#{start_time_in_date}' AND ts_id + 1 = #{name_ts}"
          sql_raw_crosses_conditions = "date = '#{start_time_in_date}' AND ts_id +1 = #{name_ts} AND symbol = '#{name_symbol}'"
        else
          if symbol == ''
            sql_ask_bids_conditions = "date = '#{start_time_in_date}' AND ts_id + 1 = #{name_ts}"
            sql_raw_crosses_conditions = "date = '#{start_time_in_date}' AND ts_id + 1 = #{name_ts}"
          
          else
            sql_ask_bids_conditions = "date = '#{start_time_in_date}'"
            sql_raw_crosses_conditions = "date = '#{start_time_in_date}' AND symbol = '#{name_symbol}'"
          end 
        end
      end
      
    else
      if symbol == '' 
        sql_ask_bids_conditions = "date = '#{start_time_in_date}' AND ts_id +1 in (#{name_ts})"
        sql_raw_crosses_conditions = "date = '#{start_time_in_date}' AND ts_id +1 in (#{name_ts})"
      else
        sql_ask_bids_conditions = "date = '#{start_time_in_date}' AND ts_id +1 in (#{name_ts})"
        sql_raw_crosses_conditions = "date = '#{start_time_in_date}' AND ts_id +1 in (#{name_ts}) AND symbol = '#{name_symbol}'"
      
      end
    end
    
    sql_str = "WITH tmp_ask_bids AS (SELECT ts_cid, ts_id, date, sum(shares) as ab_shares, sum(price * shares)/sum(shares) as ab_price, ask_bid FROM ask_bids WHERE #{sql_ask_bids_conditions} GROUP BY date, ts_cid, ts_id, ask_bid), tmp_raw_crosses AS (select date, ts_id, ts_cid, as_cid, symbol, cast(split_part(raw_content,' ',3) as numeric ) as time_stamp, raw_content from raw_crosses WHERE #{sql_raw_crosses_conditions} and raw_content like '5%' AND ((to_char(to_timestamp(split_part(raw_content,' ',3)::numeric),'HH24:MI:SS'))::time without time zone) between ('#{time_from}'::time without time zone) and ('#{time_to}'::time without time zone)), ecn_launch_raw_crosses AS (select ts_cid, ts_id, split_part(raw_crosses.raw_content, ' ', 6)::numeric as ecn_launch, split_part(raw_crosses.raw_content, ' ', 7)::numeric as ask_bid_raw_crosses from raw_crosses WHERE #{sql_raw_crosses_conditions} and raw_content like '2%' AND ((to_char(to_timestamp(split_part(raw_content,' ',3)::numeric),'HH24:MI:SS'))::time without time zone) between ('#{time_from}'::time without time zone) and ('#{time_to}'::time without time zone)) SELECT tmp_raw_crosses.date, tmp_raw_crosses.ts_id, tmp_raw_crosses.ts_cid, tmp_raw_crosses.as_cid, tmp_raw_crosses.symbol, tmp_raw_crosses.time_stamp, tmp_ask_bids.ask_bid, tmp_ask_bids.ab_shares, tmp_ask_bids.ab_price, tmp_raw_crosses.raw_content, ecn_launch_raw_crosses.ecn_launch, ecn_launch_raw_crosses.ask_bid_raw_crosses FROM tmp_raw_crosses JOIN tmp_ask_bids ON tmp_ask_bids.ts_cid = tmp_raw_crosses.ts_cid AND tmp_ask_bids.ts_id = tmp_raw_crosses.ts_id JOIN ecn_launch_raw_crosses ON ecn_launch_raw_crosses.ts_cid = tmp_raw_crosses.ts_cid AND ecn_launch_raw_crosses.ts_id = tmp_raw_crosses.ts_id ORDER BY tmp_raw_crosses.date, tmp_raw_crosses.ts_id, tmp_raw_crosses.symbol, tmp_raw_crosses.time_stamp, tmp_raw_crosses.as_cid, tmp_ask_bids.ask_bid;"
    
    ret_rows2 = RawCross2.find_by_sql(" #{sql_str} ")
    
    count_ss += ret_rows2.size
    
    start_sec = 0
    flag = 0
    start_sym = ''
    start_ts = -1
    ask_shares = 0
    ask_price = 0.0
    as_cross_id = 0
    launch_shares_size = 0
    ret_rows2.each { |value|
      if value[:ask_bid].strip == "ASK"
        flag = 0
        if value.ask_bid_raw_crosses.to_i == 0
          launch_shares_size = value.ab_shares.to_i
        end
        if start_sec == 0 or start_sym == ''
          flag = 1
          start_sec = value.time_stamp.to_i
          start_sym = value.symbol.to_s
          start_ts = value.ts_id
          trade_server_id = value.ts_id
          cross_id = value.ts_cid
          as_cross_id = value.as_cid
          ask_shares = value.ab_shares.to_i
          ask_price = value.ab_price.to_f
        else 
          flag = 0
        end
        if (value.time_stamp.to_i - start_sec.to_i > 30) or (value.symbol != start_sym) or (start_ts != value.ts_id)
          flag = 1
          cross_id = value.ts_cid
          trade_server_id = value.ts_id
          start_sec = value.time_stamp.to_i
          start_sym = value.symbol.to_s
          start_ts = value.ts_id
          as_cross_id = value.as_cid
          ask_shares = value.ab_shares.to_i
          ask_price = value.ab_price.to_f
        end 
      elsif (flag == 1)
        if value.ask_bid_raw_crosses.to_i == 1
          launch_shares_size = value.ab_shares.to_i
        end
        bid_shares = value.ab_shares.to_i
        bid_price = value.ab_price.to_f
        detected_shares = [ask_shares , bid_shares].min
        cross = ((bid_price - ask_price + 0.0001)*100).to_i
        expected_t = detected_shares * (cross * 0.01)
      
        # Check cross size
        check_detected_shares = 0
        if share_size.to_s == "" || share_size.to_s == "0"
          check_detected_shares = 1
        end
        if share_size.to_s != "" && share_size.to_s != "0"
          check_detected_shares = check_conditions_of_filter_by_shares_for_netb(share_size.to_s, detected_shares)
        end
        next if check_detected_shares == 0
        
         # Check Launch Shares
        check_launch_shares = 0
        if launch_shares.to_s == "" || launch_shares.to_s == "0"
          check_launch_shares = 1
        end
        if launch_shares.to_s != "" && launch_shares.to_s != "0"
          check_launch_shares = check_conditions_of_filter_by_shares_for_netb(launch_shares.to_s, launch_shares_size.to_i)
        end
        next if check_launch_shares == 0
          
        check_cross_size = 0
        if cross_size.to_s == "" || cross_size.to_s == "0"
          check_cross_size = 1
        end
        if cross_size.to_s != "" && cross_size.to_s != "0"
          cross_size.split(",").each do |item|
            case item.to_i
            when 11 # Filter by cross size >.10
              next if cross <= 10
            else
              next if cross != item.to_i
            end
            
            check_cross_size = 1
            break
          end
        end
        next if check_cross_size == 0
      
        # Get cecn and check
        cecn = value.ecn_launch
        case ecn_launch
          when "0"
            # Do nothing
          else
            tmp_ecn_entry = book_ecns_list
            index_cecn = tmp_ecn_entry.index{|x| x[1].to_s == ecn_launch.to_s}
            next if cecn.to_i != index_cecn.to_i
        end
        
        if (expected_t > expected_pl)
          count_ss_expected += 1
          count_launch_shares += launch_shares_size.to_i
          sum_expected += expected_t.to_f
        end
      end 
      # end
    }
   
    result = []
    result << count_ss_expected.to_i
    result << sum_expected
    result << count_launch_shares
    return result
  end
  
  #================================================================================================
  # Method    : number_day_in_month
  # Developer     : khanhtt
  # Description   : to calculate number of day in a month
  # Date Start  : 2012-03-02
  #================================================================================================ 
  def self.number_day_in_month(month, year)
    month == 2 ? ((year % 4 == 0 )|| (year % 400 == 0 && year % 100 != 0 ) ? 29 : 28) : 30 + ((month * 1.126).to_i % 2)
  end
  
  #================================================================================================
  # Method    : trade_statistics_with_date_range
  # Developer     : khanhtt
  # Description   : to calculate trade statistics with date range with time from, time to
  # Date Start  : 2012-03-02
  #================================================================================================ 
  def self.trade_statistics_with_date_range start_time, end_time, time_from, time_to, share_size = '', expected_pl = 0, symbol ='',  bought_shares = 0, sold_shares = 0, ecn_filter = '0', ts_id = '0', cecn ='0', notime = false, iso_order = '0', exclusive = 0, cross_size = '', launch_shares = ''
    
    # Check filter for NET.B: Filter by ECN Entry/Exit, Iso, Bt Shares Sd Shares then NET.B =0
    check_bought_shares = -1
    check_sold_shares = -1
    if bought_shares == -1
      bought_shares = 0
      check_bought_shares = 0
      end
    if sold_shares == -1
      sold_shares = 0
      check_sold_shares = 0
    end
    # Conditions cross size
    conditions_cross_size = build_conditions_for_cross_size(cross_size)
    st_year = start_time.year
    en_year = end_time.year
    if time_from.class == Time
      time_from = time_from.strftime("%H:%M")
    end
    if time_to.class == Time
      time_to = time_to.strftime("%H:%M")
    end
    
    # Conditions shares size
    share_size_conditions = share_size
    if share_size.to_s.include? "," # Filter by multi Detected Shares
      share_size_conditions = TradeResult.build_conditions_for_filter_by_shares(share_size, 'expected_shares')
    end 
    
    # Conditions launch shares
    launch_shares_conditions = launch_shares
    if launch_shares.to_s.include? "," # Filter by multi L aunch Shares
      launch_shares_conditions = TradeResult.build_conditions_for_filter_by_shares(launch_shares, 'launch_shares')
    end 
    
    notime = false
    
    st_month = start_time.month
    en_month = end_time.month
    indexYear = st_year.to_i
    indexMonth = st_month.to_i
    ret_all = {
        "stuck_loss"=>[0,0,0,0,0,0,0.0,2, "stuck_loss"], 
        "stuck_profit"=>[0,0,0,0,0,0,0.0,3, "stuck_profit"], 
        "loss" =>[0,0,0,0,0,0,0.0,4, "loss"] ,
        "profit_under_expected" => [0,0,0,0,0,0,0.0,5,"profit_under_expected"],
        "profit_equal_to_or_above_expected" => [0,0,0,0,0,0,0.0,6,"profit_equal_to_or_above_expected"],
        "total" =>[0,0,0,0,0,0,0.0,7,"total"],
        "missed" =>[0,0,0,0,0,0,0.0,0,"missed"],      
        "repriced" =>[0,0,0,0,0,0,0.0,1,"repriced"],
        "Skipped_Ss" => [0,0,0,0,0,0,0.0,8,"Skipped_Ss"]
            
      }
    count_ss_expected = 0
    sum_expected = 0.0
    count_launch_shares = 0
    #check number TS
    check_ts = 0
    if ts_id.include? ","
      check_ts = 1
    end
    
    while (indexYear <= en_year.to_i)
      
      while (indexYear < en_year.to_i && indexMonth <= 12) || (indexYear == en_year.to_i && indexMonth <= en_month.to_i) 
        flag = 0
        if (start_time.month == end_time.month && start_time.year == end_time.year)
          date_st = start_time
          date_en = end_time
          flag = 1
        else
          if (indexMonth == start_time.month.to_i && indexYear == start_time.year.to_i)
            date_st = start_time
            day_in_month = number_day_in_month(indexMonth.to_i,indexYear.to_i)
            date_en = Time.mktime(date_st.year.to_s,date_st.month,day_in_month.to_s)
            flag = 1
          end
          
          if (indexMonth == end_time.month.to_i && indexYear == end_time.year.to_i)
            date_en = end_time
            date_st = Time.mktime(date_en.year.to_s,date_en.month,"01")
            flag = 1
          end
          
        end
        
        if (flag==0)
          date_st = indexYear.to_s + "-" + indexMonth.to_s + "-01"
          day_in_month = number_day_in_month(indexMonth.to_i,indexYear.to_i)
          date_en = indexYear.to_s + "-" + indexMonth.to_s + "-" + day_in_month.to_s
          date_st = Time.mktime(indexYear.to_s,indexMonth.to_s,"01")
          date_en = Time.mktime(indexYear.to_s,indexMonth.to_s,day_in_month.to_s)
        end
        
        #connect to database
        db_name_suffix = "#{date_st.year}#{date_st.month.to_s.rjust(2,'0')}"      

        get_database_connection(db_name_suffix, "as")
      
        ret = TradeResult.find_by_sql(["select cal_filter_trade_statistic_byecn2_twt2(?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?, ?,?,?,?);", date_st, date_en, time_from, time_to, share_size_conditions, expected_pl, symbol,  bought_shares, sold_shares, ecn_filter.to_s, ts_id, cecn.to_s, iso_order.to_s, exclusive.to_i, conditions_cross_size, launch_shares_conditions]).first.cal_filter_trade_statistic_byecn2_twt2.gsub(/\(|\)/, "");      
         
        name_symbol = symbol
        start_time_in_date  = date_st.year.to_s << "-" << date_st.month.to_s.rjust(2,'0') << "-" << date_st.day.to_s.rjust(2,'0')
        end_time_in_date  = end_time.year.to_s << "-" << end_time.month.to_s.rjust(2,'0') << "-" << end_time.day.to_s.rjust(2,'0')
        
        count_miss,count_repriced,count_stuck_loss,count_stuck_profit,count_loss,count_profit_under,count_profit_above,count_total,shares_traded_miss,shares_traded_repriced,shares_traded_stuck_loss,shares_traded_stuck_profit,shares_traded_loss,shares_traded_profit_under,shares_traded_profit_above,shares_traded_total,pm_shares_traded_miss,pm_shares_traded_repriced,pm_shares_traded_stuck_loss,pm_shares_traded_stuck_profit,pm_shares_traded_loss,pm_shares_traded_profit_under,pm_shares_traded_profit_above,pm_shares_traded_total,expected_miss,expected_repriced,expected_stuck_loss,expected_stuck_profit,expected_loss,expected_profit_under,expected_profit_above,expected_total,real_miss,real_stuck_loss,real_stuck_profit,real_loss,real_profit_under,real_profit_above,real_total,pm_miss,pm_stuck_loss,pm_stuck_profit,pm_loss,pm_profit_under,pm_profit_above,pm_total,launch_shares_miss,launch_shares_repriced,launch_shares_stuck_loss,launch_shares_stuck_profit,launch_shares_loss,launch_shares_profit_under,launch_shares_profit_above,launch_shares_total = ret.split(",")
      
        ret = {
          "stuck_loss"=>[count_stuck_loss.to_i,launch_shares_stuck_loss.to_i,shares_traded_stuck_loss.to_i,pm_shares_traded_stuck_loss.to_i,expected_stuck_loss.to_f,real_stuck_loss.to_f,pm_stuck_loss.to_f, 2, "stuck_loss"], #[count, expected profit, real profit]
          "stuck_profit"=>[count_stuck_profit.to_i,launch_shares_stuck_profit.to_i,shares_traded_stuck_profit.to_i,pm_shares_traded_stuck_profit.to_i,expected_stuck_profit.to_f,real_stuck_profit.to_f,pm_stuck_profit.to_f, 3, "stuck_profit"], #[count, expected profit, real profit]
          "loss" =>[count_loss.to_i,launch_shares_loss.to_i,shares_traded_loss.to_i,pm_shares_traded_loss.to_i,expected_loss.to_f,real_loss.to_f,0.0,4, "loss"] ,
          "profit_under_expected" => [count_profit_under.to_i,launch_shares_profit_under.to_i,shares_traded_profit_under.to_i,pm_shares_traded_profit_under.to_i,expected_profit_under.to_f,real_profit_under.to_f,0.0,5,"profit_under_expected"],
          "profit_equal_to_or_above_expected" => [count_profit_above.to_i,launch_shares_profit_above.to_i,shares_traded_profit_above.to_i,pm_shares_traded_profit_above.to_i,expected_profit_above.to_f,real_profit_above.to_f,0.0,6,"profit_equal_to_or_above_expected"],
          "total" =>[0,0,0,0,0,0,0.0,7,"total"],
          "missed" =>[count_miss.to_i,launch_shares_miss.to_i,shares_traded_miss.to_i,pm_shares_traded_miss.to_i,expected_miss.to_f,real_miss.to_f,0.0,0,"missed"],     
          "repriced" =>[count_repriced.to_i,launch_shares_repriced.to_i,shares_traded_repriced.to_i,pm_shares_traded_repriced.to_i,expected_repriced.to_f,0,0.0,1,"repriced"],
          
        } 
      
        ret.each{ |k,v|
        
          
          if k != "total"       
            ret["total"][0] += v[0]  if k !="repriced"  
            ret["total"][1] += v[1] if k !="repriced"       
            ret["total"][2] += v[2] if k !="repriced" 
            ret["total"][3] += v[3] if k !="repriced" 
            ret["total"][4] += v[4].to_f if k !="repriced" 
            ret["total"][5] += v[5].to_f
            ret["total"][6] += v[6].to_f
          end
        }
      

        ret_all["stuck_loss"][0] += ret["stuck_loss"][0]
        ret_all["stuck_loss"][1] += ret["stuck_loss"][1]
        ret_all["stuck_loss"][2] += ret["stuck_loss"][2]
        ret_all["stuck_loss"][3] += ret["stuck_loss"][3]
        ret_all["stuck_loss"][4] += ret["stuck_loss"][4]
        ret_all["stuck_loss"][5] += ret["stuck_loss"][5]
        ret_all["stuck_loss"][6] += ret["stuck_loss"][6]
                                        
        ret_all["stuck_profit"][0] += ret["stuck_profit"][0]
        ret_all["stuck_profit"][1] += ret["stuck_profit"][1]
        ret_all["stuck_profit"][2] += ret["stuck_profit"][2]
        ret_all["stuck_profit"][3] += ret["stuck_profit"][3]
        ret_all["stuck_profit"][4] += ret["stuck_profit"][4]
        ret_all["stuck_profit"][5] += ret["stuck_profit"][5]
        ret_all["stuck_profit"][6] += ret["stuck_profit"][6]

        ret_all["loss"][0] += ret["loss"][0]
        ret_all["loss"][1] += ret["loss"][1]
        ret_all["loss"][2] += ret["loss"][2]
        ret_all["loss"][3] += ret["loss"][3]
        ret_all["loss"][4] += ret["loss"][4]
        ret_all["loss"][5] += ret["loss"][5]

        ret_all["profit_under_expected"][0] += ret["profit_under_expected"][0]
        ret_all["profit_under_expected"][1] += ret["profit_under_expected"][1]
        ret_all["profit_under_expected"][2] += ret["profit_under_expected"][2]
        ret_all["profit_under_expected"][3] += ret["profit_under_expected"][3]
        ret_all["profit_under_expected"][4] += ret["profit_under_expected"][4]
        ret_all["profit_under_expected"][5] += ret["profit_under_expected"][5]

        ret_all["profit_equal_to_or_above_expected"][0] += ret["profit_equal_to_or_above_expected"][0]
        ret_all["profit_equal_to_or_above_expected"][1] += ret["profit_equal_to_or_above_expected"][1]
        ret_all["profit_equal_to_or_above_expected"][2] += ret["profit_equal_to_or_above_expected"][2]
        ret_all["profit_equal_to_or_above_expected"][3] += ret["profit_equal_to_or_above_expected"][3]
        ret_all["profit_equal_to_or_above_expected"][4] += ret["profit_equal_to_or_above_expected"][4]
        ret_all["profit_equal_to_or_above_expected"][5] += ret["profit_equal_to_or_above_expected"][5]
        
        ret_all["total"][0] += ret["total"][0]
        ret_all["total"][1] += ret["total"][1]
        ret_all["total"][2] += ret["total"][2]
        ret_all["total"][3] += ret["total"][3]
        ret_all["total"][4] += ret["total"][4]
        ret_all["total"][5] += ret["total"][5]
        ret_all["total"][6] += ret["total"][6]

        ret_all["missed"][0] += ret["missed"][0]
        ret_all["missed"][1] += ret["missed"][1]
        ret_all["missed"][2] += ret["missed"][2]
        ret_all["missed"][3] += ret["missed"][3]
        ret_all["missed"][4] += ret["missed"][4]
        ret_all["missed"][5] += ret["missed"][5]

        ret_all["repriced"][0] += ret["repriced"][0]
        ret_all["repriced"][1] += ret["repriced"][1]
        ret_all["repriced"][2] += ret["repriced"][2]  
        ret_all["repriced"][3] += ret["repriced"][3]
        ret_all["repriced"][4] += ret["repriced"][4]    
        ret_all["repriced"][5] += ret["repriced"][5]          
         
        indexMonth = indexMonth + 1
      end
      
      if (indexMonth > 12)
        indexMonth = 1
      end
      
      indexYear = indexYear + 1
    end
    
    # Check filter for NET.B
    if ((check_ts == 0 && ts_id.to_i != -1) || check_ts != 0) && ecn_filter == '0' && iso_order == '0' && check_bought_shares == 0 && check_sold_shares == 0
      result = TradeResult.calculate_netb_date_range(start_time,end_time,time_from,time_to,expected_pl,symbol,ts_id, share_size, cross_size, cecn, launch_shares)
          
      count_ss_expected = result[0]
      sum_expected = result[1]
      count_launch_shares = result[3] 
    end
    
    ret_all["Skipped_Ss"] = [count_ss_expected.to_i,count_launch_shares,0,0,sum_expected.to_f,0,0.0,8,"Skipped_Ss"]
    ret_all
  end
  
  #================================================================================================
  # Method    : calculate_netb_date_range
  # Developer     : 
  # Description   : Used for calculate NETB with date range.
  #================================================================================================
  def self.calculate_netb_date_range start_time, end_time, time_from, time_to, expected_pl = 0, symbol ='', ts_id = '0', share_size = '',  cross_size = '', ecn_launch = '', launch_shares = ''
    st = start_time.month
    en = end_time.month
    year = start_time.year
    
    $MIN_TIME_FROM = time_from.split(':')[0].to_i * 3600 + time_from.split(':')[1].to_i * 60
    $MAX_TIME_TO = time_to.split(':')[0].to_i * 3600 + time_to.split(':')[1].to_i * 60
    
    year_interval = 0
    if end_time.year > start_time.year
      year_interval = end_time.year - start_time.year
      en = en + 12 * year_interval
    end
    
    st_time = start_time
    count_ss_expected = 0
    sum_expected = 0.0
    count_launch_shares = 0
    cross_netb = []
    @arr_netb = []
    #check number TS
    check_ts = 0
    if ts_id.to_s.include? ","
      check_ts = 1
    end
    
    while st <= en 
      #p "st: #{st}"
      db_name_suffix = "#{Time.now.year}#{Time.now.month.to_s.rjust(2,'0')}"
      
      if st <= 12
        db_name_suffix = "#{year}#{st.to_s.rjust(2,'0')}"
      else        
        #p "year_interval : #{year_interval}"
        if year_interval == 1
          db_name_suffix = "#{year + 1}#{(st - 12).to_s.rjust(2,'0')}"
        elsif year_interval == 2
          if st <= 24
            db_name_suffix = "#{year + 1}#{(st - 12).to_s.rjust(2,'0')}"
          else
            db_name_suffix = "#{year + 2}#{(st - 24).to_s.rjust(2,'0')}"
          end
        end
      end

      # lets manually connect to the proper db
      get_database_connection(db_name_suffix, "as")
            
      st = st + 1
      count_ss = 0      
      
      start_time_in_date  = st_time.year.to_s << "-" << st_time.month.to_s.rjust(2,'0') << "-" << st_time.day.to_s.rjust(2,'0')
      end_time_in_date  = end_time.year.to_s << "-" << end_time.month.to_s.rjust(2,'0') << "-" << end_time.day.to_s.rjust(2,'0')
      
      sql_ask_bids_conditions = "date >= '#{start_time_in_date}' and date <= '#{end_time_in_date}'"
      sql_raw_crosses_conditions = "date >= '#{start_time_in_date}' AND date <= '#{end_time_in_date}'"
         
      sql_str = ""

      if check_ts == 0
        if ts_id.to_i == 0
          sql_str = " SELECT c.date ,c.ts_id, c.ts_cid, c.as_cid, trim(c.symbol) as symbol, c.time_stamp, c.time_stamp_netb, ask_shares, ask_price, bid_shares, bid_price, c.ecn_launch, c.ask_bid_raw_crosses
          FROM 
            (SELECT r1.ts_cid, r1.ts_id, r1.date, r1.as_cid, r1.symbol, to_char(to_timestamp(split_part(r1.raw_content,' ',3)::numeric),'HH24:MI:SS')::time without time zone as time_stamp, time_stamp_netb, r2.ecn_launch, r2.ask_bid_raw_crosses
            FROM (SELECT * FROM raw_crosses
              WHERE date >= '#{start_time_in_date}' AND date <= '#{end_time_in_date}'  AND raw_content like '5%') r1 
            INNER JOIN (SELECT date, ts_id, ts_cid, split_part(raw_content,' ',10) as time_stamp_netb, split_part(raw_crosses.raw_content, ' ', 6)::numeric as ecn_launch, split_part(raw_crosses.raw_content, ' ', 7)::numeric as ask_bid_raw_crosses FROM raw_crosses
              WHERE date >= '#{start_time_in_date}' AND date <= '#{end_time_in_date}'  AND raw_content like '2%') r2
            ON r2.date = r1.date AND r2.ts_id = r1.ts_id AND r2.ts_cid = r1.ts_cid) AS c 
          JOIN (SELECT ask.ts_cid, ask.ts_id, ask.date, ask_shares, ask_price, bid_shares, bid_price 
            FROM (SELECT date, ts_cid, ts_id, sum(shares) as ask_shares, sum(price * shares)/sum(shares) as ask_price FROM ask_bids 
              WHERE date >= '#{start_time_in_date}' AND date <= '#{end_time_in_date}' AND ask_bid ='ASK'
              GROUP BY date, ts_cid, ts_id) ask 
            INNER JOIN (SELECT date, ts_cid, ts_id, sum(shares) as bid_shares, sum(price * shares)/sum(shares) as bid_price FROM ask_bids 
              WHERE date >= '#{start_time_in_date}' AND date <= '#{end_time_in_date}' AND ask_bid ='BID'
              GROUP BY date, ts_cid, ts_id) bid 
            ON bid.date = ask.date AND bid.ts_id = ask.ts_id AND bid.ts_cid = ask.ts_cid) b 
            ON c.ts_cid = b.ts_cid AND c.ts_id = b.ts_id AND c.date = b.date 
          ORDER BY date, ts_id, symbol, time_stamp, as_cid;"
            
        else      
          sql_str = " SELECT c.date ,c.ts_id, c.ts_cid, c.as_cid, trim(c.symbol) as symbol, c.time_stamp, c.time_stamp_netb, ask_shares, ask_price, bid_shares, bid_price, c.ecn_launch, c.ask_bid_raw_crosses
            FROM 
            (SELECT r1.ts_cid, r1.ts_id, r1.date, r1.as_cid, r1.symbol, to_char(to_timestamp(split_part(r1.raw_content,' ',3)::numeric),'HH24:MI:SS')::time without time zone as time_stamp, time_stamp_netb, r2.ecn_launch, r2.ask_bid_raw_crosses
            FROM (SELECT * FROM raw_crosses
              WHERE date >= '#{start_time_in_date}' AND date <= '#{end_time_in_date}'  AND raw_content like '5%' AND ts_id + 1 = #{ts_id}) r1 
            INNER JOIN (SELECT date, ts_id, ts_cid, split_part(raw_content,' ',10) as time_stamp_netb, split_part(raw_crosses.raw_content, ' ', 6)::numeric as ecn_launch, split_part(raw_crosses.raw_content, ' ', 7)::numeric as ask_bid_raw_crosses FROM raw_crosses
              WHERE date >= '#{start_time_in_date}' AND date <= '#{end_time_in_date}'  AND raw_content like '2%' AND ts_id + 1 = #{ts_id}) r2
            ON r2.date = r1.date AND r2.ts_id = r1.ts_id AND r2.ts_cid = r1.ts_cid) AS c 
          JOIN (SELECT ask.ts_cid, ask.ts_id, ask.date, ask_shares, ask_price, bid_shares, bid_price 
            FROM (SELECT date, ts_cid, ts_id, sum(shares) as ask_shares, sum(price * shares)/sum(shares) as ask_price FROM ask_bids 
              WHERE date >= '#{start_time_in_date}' AND date <= '#{end_time_in_date}' AND ask_bid ='ASK'  AND ts_id + 1 = #{ts_id}
              GROUP BY date, ts_cid, ts_id) ask 
            INNER JOIN (SELECT date, ts_cid, ts_id, sum(shares) as bid_shares, sum(price * shares)/sum(shares) as bid_price FROM ask_bids 
              WHERE date >= '#{start_time_in_date}' AND date <= '#{end_time_in_date}' AND ask_bid ='BID'  AND ts_id + 1 = #{ts_id}
              GROUP BY date, ts_cid, ts_id) bid 
            ON bid.date = ask.date AND bid.ts_id = ask.ts_id AND bid.ts_cid = ask.ts_cid) b 
            ON c.ts_cid = b.ts_cid AND c.ts_id = b.ts_id AND c.date = b.date 
          ORDER BY date, ts_id, symbol, time_stamp, as_cid;"
          
        end
        
      else
        sql_str = " SELECT c.date ,c.ts_id, c.ts_cid, c.as_cid, trim(c.symbol) as symbol, c.time_stamp, c.time_stamp_netb, ask_shares, ask_price, bid_shares, bid_price, c.ecn_launch, c.ask_bid_raw_crosses
          FROM 
          (SELECT r1.ts_cid, r1.ts_id, r1.date, r1.as_cid, r1.symbol, to_char(to_timestamp(split_part(r1.raw_content,' ',3)::numeric),'HH24:MI:SS')::time without time zone as time_stamp, time_stamp_netb, r2.ecn_launch, r2.ask_bid_raw_crosses
          FROM (SELECT * FROM raw_crosses
            WHERE date >= '#{start_time_in_date}' AND date <= '#{end_time_in_date}'  AND raw_content like '5%' AND ts_id + 1 in (#{ts_id})) r1 
          INNER JOIN (SELECT date, ts_id, ts_cid, split_part(raw_content,' ',10) as time_stamp_netb, split_part(raw_crosses.raw_content, ' ', 6)::numeric as ecn_launch, split_part(raw_crosses.raw_content, ' ', 7)::numeric as ask_bid_raw_crosses FROM raw_crosses
            WHERE date >= '#{start_time_in_date}' AND date <= '#{end_time_in_date}'  AND raw_content like '2%' AND ts_id + 1 in (#{ts_id})) r2
          ON r2.date = r1.date AND r2.ts_id = r1.ts_id AND r2.ts_cid = r1.ts_cid) AS c 
        JOIN (SELECT ask.ts_cid, ask.ts_id, ask.date, ask_shares, ask_price, bid_shares, bid_price 
          FROM (SELECT date, ts_cid, ts_id, sum(shares) as ask_shares, sum(price * shares)/sum(shares) as ask_price FROM ask_bids 
            WHERE date >= '#{start_time_in_date}' AND date <= '#{end_time_in_date}' AND ask_bid ='ASK'  AND ts_id + 1 in (#{ts_id})
            GROUP BY date, ts_cid, ts_id) ask 
          INNER JOIN (SELECT date, ts_cid, ts_id, sum(shares) as bid_shares, sum(price * shares)/sum(shares) as bid_price FROM ask_bids 
            WHERE date >= '#{start_time_in_date}' AND date <= '#{end_time_in_date}' AND ask_bid ='BID'  AND ts_id + 1 in (#{ts_id})
            GROUP BY date, ts_cid, ts_id) bid 
          ON bid.date = ask.date AND bid.ts_id = ask.ts_id AND bid.ts_cid = ask.ts_cid) b 
          ON c.ts_cid = b.ts_cid AND c.ts_id = b.ts_id AND c.date = b.date 
        ORDER BY date, ts_id, symbol, time_stamp, as_cid;"
      
      end
            
      ret_rows2 = RawCross2.find_by_sql(" #{sql_str} ")
      count_ss += ret_rows2.size
      
      start_sec = 0
      start_sym = ''
      start_ts = -1
      start_date = ''
      as_cross_id = 0
      test = 0
      launch_shares_size = 0
      ret_rows2.each { |value|
        if (symbol != "")
          next if (value.symbol != symbol)
        end
        
        if (start_date != value.date)
          start_date = value.date
          start_sec = 0
          start_sym = ''
          start_ts = -1
        end
        
        time_stamp = value.time_stamp.split(':')[0].to_i * 3600 + value.time_stamp.split(':')[1].to_i * 60 + value.time_stamp.split(':')[2].to_i
        # time_stamp = value.time_stamp
        if ((time_stamp >= $MIN_TIME_FROM) && (time_stamp <=  $MAX_TIME_TO))                
          if (time_stamp - start_sec > 30) or (value.symbol != start_sym) or (start_ts != value.ts_id)
            start_sec = time_stamp
            start_sym = value.symbol
            start_ts = value.ts_id
            detected_shares = [value.ask_shares , value.bid_shares].min;
            cross = ((value.bid_price.to_f - value.ask_price.to_f + 0.0001)*100).to_i
            expected_t = detected_shares.to_i * (cross * 0.01)
             # Get value of Launch Shares  
            if value.ask_bid_raw_crosses.to_i == 0
              launch_shares_size = value.ask_shares.to_i
            else # Bid
              launch_shares_size = value.bid_shares.to_i
            end
            
            # Check Launch Shares
            check_launch_shares = 0
            if launch_shares.to_s == "" || launch_shares.to_s == "0"
              check_launch_shares = 1
            end
            if launch_shares.to_s != "" && launch_shares.to_s != "0"
              check_launch_shares = check_conditions_of_filter_by_shares_for_netb(launch_shares.to_s, launch_shares_size.to_i)
            end
            next if check_launch_shares == 0
        
            # Get detected shares and check     
            check_detected_shares = 0
            if share_size.to_s == "" || share_size.to_s == "0"
              check_detected_shares = 1
            end
            if share_size.to_s != "" && share_size.to_s != "0"
              check_detected_shares = check_conditions_of_filter_by_shares_for_netb(share_size.to_s, detected_shares.to_i)
            end
            next if check_detected_shares == 0
            
            # Check cross size
            check_cross_size = 0
            if cross_size.to_s == "" || cross_size.to_s == "0"
              check_cross_size = 1
            end
            if cross_size.to_s != "" && cross_size.to_s != "0"
              cross_size.split(",").each do |item|
                case item.to_i
                  when 11 # Filter by cross size >.10
                    next if cross <= 10
                  else
                    next if cross != item.to_i
                end
                
                check_cross_size = 1
                break
              end
            end
            next if check_cross_size == 0
            
            # Get cecn and check
            cecn = value.ecn_launch
            case ecn_launch
              when "0"
                # Do nothing
              else
                tmp_ecn_entry = book_ecns_list
                index_cecn = tmp_ecn_entry.index{|x| x[1].to_s == ecn_launch.to_s}
                next if cecn.to_i != index_cecn.to_i
            end
            
            if (expected_t > expected_pl)
              count_ss_expected += 1
              count_launch_shares += launch_shares_size.to_i
              sum_expected += expected_t.to_f
              
              cross_netb << value.as_cid.to_i
              cross_netb << value.symbol.to_s
              cross_netb << detected_shares
              expected_temp = "%.02f" % expected_t
              cross_netb << expected_temp.to_f 
              timestamp = TradeResult.convert_time_epoch(value.time_stamp_netb)
              cross_netb << timestamp
              cross_netb << value.ts_cid.to_s  # Cross ID
              cross_netb << value.ts_id.to_s  #trade_server_id
              cross_netb << value.date 
              cross_netb << "%06d" % (value.time_stamp_netb.to_i % 1000000) # get microseconds
              cross_netb << value.time_stamp_netb #use to sort timestamp
              cross_id = ""
              trade_server_id = ""
              as_cross_id = ""
              cross_netb << launch_shares_size # Launch Shares
              @arr_netb << cross_netb
              cross_netb = []           
            end
          end
        end                                 
      }
    end
    
    result = []
    result << count_ss_expected.to_i
    result << sum_expected
    result << @arr_netb 
    result << count_launch_shares
    
    return result
  end
  
  
end