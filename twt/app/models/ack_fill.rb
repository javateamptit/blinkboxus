class AckFill < ExternalDatabase
  set_table_name "summary_ack_fills"

  def self.days_in_month(month)
    month == 2 ? 28 : 30 + ((month * 1.126).to_i % 2)
  end

  def self.get_ack_fill(start_time, end_time, start_date, end_date, ts_id, ecn_id, step_type, drange)

    #Set table name based on step
    step = step_type.to_i
    date_range = drange.to_i
    step_suffix = ""

    # Set suffix of table 
    if step == 5
      step_suffix = ""
    elsif (step == 15 || step == 30 || step == 60)
      step_suffix = "_"+  step.to_s
    else
      #step_suffix = "_60"
      step_suffix = ""
    end

    #Set table name
    tbl_ack_fills = "summary_ack_fills" + step_suffix

    st = start_date.month
    en = end_date.month
    year = start_date.year

    date_st = start_date
    date_en = end_date
    if start_date.month < end_date.month
      if start_date.month == 2 && ((start_date.year % 4 == 0 && start_date.year % 100 !=0)|| start_date.year % 400 == 0)
        #leap year
        date_en = Time.mktime(start_date.year, start_date.month,days_in_month(start_date.month) + 1,21) 
      else
        date_en = Time.mktime(start_date.year, start_date.month,days_in_month(start_date.month),21) 
      end
    end

    #Declare a variable to store results
    summary_ack_fill = []

    k = 0
    if end_date.year > start_date.year
      k = end_date.year - start_date.year
      en = en + 12 * k
    end
    #puts "st <= en :#{st} #{en}"
    while st <= en 
      db_name_suffix = "#{Time.now.year}#{Time.now.month.to_s.rjust(2,'0')}"
      if st <= 12
        db_name_suffix = "#{year}#{st.to_s.rjust(2,'0')}"
      else
        if k == 1
          db_name_suffix = "#{year + 1}#{(st - 12).to_s.rjust(2,'0')}"
        elsif k == 2
          if st <= 24
            db_name_suffix = "#{year + 1}#{(st - 12).to_s.rjust(2,'0')}"
          else
            db_name_suffix = "#{year + 2}#{(st - 24).to_s.rjust(2,'0')}"
          end
        end
      end

      # lets manually connect to the proper db
      get_database_connection(db_name_suffix, "twt")

      st = st + 1

      # puts "Accessing to the database to calculate summary information..."
      # puts "start date: #{start_date}"
      # puts "end_date: #{end_date}"
      # puts "step: #{step}"

      select_query = " total_time_ack, sum(ack_count) as ack_count, sum(total_time_fill) as total_time_fill, sum(fill_count) as fill_count "
      query = ""
      filter_conditions = ""


      if (date_range ==0)
        filter_conditions = " date >= '#{start_date}' and date <= '#{end_date}' and time_stamp >= '#{start_time}' and time_stamp <= '#{end_time}' "
      else
        filter_conditions = " date >= '#{start_date}' and date <= '#{end_date}' "
      end

      #check number TS
      check_ts = 0
      # Check multi trade servers selections
      if ts_id.to_s.include? ","
        check_ts = 1
      end

      if check_ts == 0
        if ts_id.to_i > 0
          filter_conditions << " and ts_id + 1 = #{ts_id.to_i} "
        end
      else
        filter_conditions << " and ts_id + 1 in (#{ts_id}) "
      end

      if (ecn_id.to_i != 99)
        filter_conditions << " and ecn_id = #{ecn_id}"
      end

      # Get sum of total_time_ack with sum of ack_count
      # and sum of total_time_fill with sum of fill_count
      strSql = "SELECT date, time_stamp, SUM(total_time_ack * ack_count) AS total_ack_delay, SUM(ack_count) AS count_acks, SUM(total_time_fill * fill_count) AS total_fill_delay, SUM(fill_count) AS count_fills FROM #{tbl_ack_fills} WHERE #{filter_conditions} GROUP BY date, time_stamp ORDER BY date, time_stamp"

      # Execute SQL
      records_with_timestamp = AckFill.find_by_sql(strSql)

      case step
      when 91 # 1 day
        records_with_timestamp = records_with_timestamp.group_by(&:date)
      when 92 # 1 week
        records_with_timestamp = records_with_timestamp.group_by { |r| r.date.strftime("%U")}
      when 93 # 1 month
        records_with_timestamp = records_with_timestamp.group_by { |r| r.date.strftime("%m")}
      else # < 1 day
      end

      if step < 91

        median_ack_time = 0
        meadian_fill_time = 0
        records_with_timestamp.each do |r|

          median_ack_time = (r.count_acks.to_i == 0 ? 0 : r.total_ack_delay.to_i / r.count_acks.to_i)
          meadian_fill_time = (r.count_fills.to_i == 0 ? 0 : r.total_fill_delay.to_i / r.count_fills.to_i)

          summary_ack_fill << {:time_stamp=>r.time_stamp, :median_time_ack => median_ack_time, :median_time_fill=> meadian_fill_time }

        end

      else

        # Loop with timestamp to add its to summary_ack_fill
        index = 0
        time_stamp_str = ""
        records_with_timestamp.each do |key, r|

          median_ack_time = 0
          meadian_fill_time = 0

          r.each do |item|

            # Set median time to 0 if count = 0, otherwise set median = total time / count
            median_ack_time += (item.count_acks.to_i == 0 ? 0 : item.total_ack_delay.to_i / item.count_acks.to_i)
            meadian_fill_time += (item.count_fills.to_i == 0 ? 0 : item.total_fill_delay.to_i / item.count_fills.to_i)

          end

          size_r = r.size
          if size_r > 0
            median_ack_time = median_ack_time / size_r 
            meadian_fill_time = meadian_fill_time / size_r
          end

          case step
          when 91
            summary_ack_fill << {:date=>key, :median_time_ack => median_ack_time, :median_time_fill=> meadian_fill_time }
          when 92
            summary_ack_fill << {:week_num => (key.to_i + 1).to_s, :median_time_ack => median_ack_time, :median_time_fill=> meadian_fill_time }
          when 93
            summary_ack_fill << {:year_num=> (r.first && r.first.date) ? r.first.date.strftime("%Y") : key, :month_num => key, :median_time_ack => median_ack_time, :median_time_fill=> meadian_fill_time }
          end

          # summary_ack_fill << {:time_stamp=>key, :median_time_ack => median_ack_time, :median_time_fill=> meadian_fill_time }

        end

      end

      if en - st > 0
        if (date_st.month+1) == 2 && ((date_st.year % 4 ==0 && date_st.year % 100 != 0)|| date_st.year % 400 ==0)
          #leap year
          date_en = Time.mktime(date_st.year, date_st.month+1,days_in_month(date_st.month+1) + 1,21) 
        else
          if date_st.month == 12
            date_en = Time.mktime(date_st.year + 1, 1,31,21) 
          else
            date_en = Time.mktime(date_st.year, date_st.month+1,days_in_month(date_st.month+1),21) 
          end
        end
      else
        date_en = end_date
      end
      #date_st = Time.mktime(date_st.year, date_st.month + 1,1,6) 
      if date_st.month == 12
        date_st = Time.mktime(date_st.year + 1,  1,1,6) 
      else
        date_st = Time.mktime(date_st.year, date_st.month + 1,1,6) 
      end
    end

    return summary_ack_fill
  end

end
