class ColorConfig
  attr_reader :global_config,
        :line_config,
        :column_config,
        :column_frequency,
        :pie_config,
        :chart_background,
        :reloader,
        :pie_config_repriced

  def line_config
    @line_config
  end

  def column_config
    return "<settings>" + @column_config + @column_frequency + "</settings>"
  end

  def pie_config
    @pie_config
  end

  def pie_config_repriced
    @pie_config_repriced
  end

  def chart_background
    @chart_background
  end

  def reloader
    @reloader
  end

  def initialize
    #open('#{RAILS_ROOT}/color_config.txt') { |f| lines=f.readline }
    #lineconfig = lines

    file_name = "#{RAILS_ROOT}/color_config.txt"
    f = File.open(file_name,"r")

    @line_config="<settings>"
    #@column_config="<settings>"
    @column_config=""
    @pie_config="<settings>"

    f.each_line { |line|

      is_global_config=0
      is_line_config=0
      is_pie_config=0
      column_config_item = ""
      current_tag=""

      line = line.strip
      if (line != nil and line !="" and line[0,1]!="#" and line.slice(0,2) !="##")
        items = line.strip.split(":")
        tag_name = items[0].strip
        tag_value = items[1].strip

        #Global configuration 
        if (tag_name == "background")
          is_global_config=1
          current_tag = "<background><color>" + tag_value + "</color></background>"
          @chart_background = "##{tag_value}"
        elsif (tag_name == "text")
          is_global_config=1
          current_tag = "<text_color>" + tag_value + "</text_color>"
        elsif (tag_name == "legend_text")
          is_global_config=1
          current_tag = "<legend><text_color>" + tag_value + "</text_color></legend>"
        elsif (tag_name == "labels_text")
          is_global_config=1
          current_tag = "<labels><label lid='0'><text_color>" +tag_value + "</text_color></label></labels>"
          @reloader = "##{tag_value}"
        #Line configuration
        elsif (tag_name == "grid_x")
          is_line_config = 1
          current_tag = "<grid><x><color>" +tag_value + "</color></x></grid>"
          column_config_item = "<grid><category><color>" +tag_value + "</color></category></grid>"
        elsif (tag_name == "grid_y_left")
          is_line_config = 1
          current_tag = "<grid><y_left><color>" +tag_value + "</color></y_left></grid>"
          column_config_item = "<grid><value><color>" +tag_value + "</color></value></grid>"
        elsif (tag_name == "grid_y_right")
          is_line_config = 1
          current_tag = "<grid><y_right><color>" +tag_value + "</color></y_right></grid>"
        elsif (tag_name == "axis_x")
          is_line_config = 1
          current_tag = "<axes><x><color>"+tag_value+"</color></x></axes>"
          column_config_item = "<axes><category><color>"+tag_value+"</color></category></axes>"
        elsif (tag_name == "axis_y_left")
          is_line_config = 1
          current_tag = "<axes><y_left><color>"+tag_value+"</color></y_left></axes>"
          column_config_item = "<axes><value><color>"+tag_value+"</color></value></axes>"
        elsif (tag_name == "axis_y_right")
          is_line_config = 1
          current_tag = "<axes><y_right><color>"+tag_value+"</color></y_right></axes>"
        elsif (tag_name == "values_x")
          is_line_config = 1
          current_tag = "<values><x><color>"+tag_value+"</color></x></values>"
          column_config_item = "<values><category><color>"+tag_value+"</color></category></values>"
        elsif (tag_name == "values_y_left")
          is_line_config = 1
          current_tag = "<values><y_left><color>"+tag_value+"</color></y_left></values>"
          column_config_item = "<values><value><color>"+tag_value+"</color></value></values>"
        elsif (tag_name == "values_y_right")
          is_line_config = 1
          current_tag = "<values><y_right><color>"+tag_value+"</color></y_right></values>"
        elsif (tag_name == "graphs")
          is_line_config = 1
          #current_tag = "<colors>"+ tag_value +"</colors>"
          color_arr=tag_value.strip.split(",")
          current_tag="<graphs>"
          color_arr.each_with_index {|item, index|
            #puts index
            current_tag = current_tag + "<graph gid='#{index}'><color>"+item+"</color></graph>"
          }
          current_tag= current_tag + "</graphs>"
          column_config_item = "<colors>"+ tag_value +"</colors>"
        elsif (tag_name == "plot_area")
          is_line_config = 1
          current_tag = "<plot_area><color>"+tag_value+"</color></plot_area>"
          column_config_item = "<plot_area><color>"+tag_value+"</color></plot_area>"
        elsif (tag_name == "scroller")
          is_line_config = 1
          current_tag = "<scroller><color>"+tag_value+"</color></scroller>"
        elsif (tag_name == "scroller_bg")
          is_line_config = 1
          current_tag = "<scroller><bg_color>"+tag_value+"</bg_color></scroller>"
        elsif (tag_name == "indicator_bg")
          is_line_config = 1
          current_tag = "<indicator><color>"+tag_value+"</color></indicator>"
        elsif (tag_name == "indicator_text")
          is_line_config = 1
          current_tag = "<indicator><x_balloon_text_color>"+tag_value+"</x_balloon_text_color></indicator>"
        elsif (tag_name == "indicator_selection")
          is_line_config = 1
          current_tag = "<indicator><selection_color>"+tag_value+"</selection_color></indicator>"
        elsif (tag_name == "zoom_out_button_bg")
          is_line_config = 1
          current_tag = "<zoom_out_button><color>"+tag_value+"</color></zoom_out_button>"
        elsif (tag_name == "zoom_out_button_text")
          is_line_config = 1
          current_tag = "<zoom_out_button><text_color>"+tag_value+"</text_color></zoom_out_button>"
        elsif (tag_name == "zoom_out_button_text_hover")
          is_line_config = 1
          current_tag = "<zoom_out_button><text_color_hover>"+tag_value+"</text_color_hover></zoom_out_button>"
        elsif (tag_name == "help_button_bg")
          is_line_config = 1
          current_tag = "<help><button><color>"+tag_value+"</color></button></help>"
        elsif (tag_name == "help_button_text")
          is_line_config = 1
          current_tag = "<help><button><text_color>"+tag_value+"</text_color></button></help>"
        elsif (tag_name == "help_button_text_hover")
          is_line_config = 1
          current_tag = "<help><button><text_color_hover>"+tag_value+"</text_color_hover></button></help>"
        elsif (tag_name == "help_balloon_bg")
          is_line_config = 1
          current_tag = "<help><balloon><color>"+tag_value+"</color></balloon></help>"
        elsif (tag_name == "help_balloon_text")
          is_line_config = 1
          current_tag = "<help><balloon><text_color>"+tag_value+"</text_color></balloon></help>"

        #Pie (3D donut) configuration
        elsif (tag_name == "slices")
          is_pie_config = 1 
          current_tag = "<pie><colors>" + tag_value + "</colors></pie>"
        elsif (tag_name == "data_labels_text")
          is_pie_config = 1 
          current_tag = "<data_labels><text_color>" + tag_value + "</text_color></data_labels>"
        elsif (tag_name == "data_labels_line")
          is_pie_config = 1 
          current_tag = "<data_labels><line_color>" + tag_value + "</line_color></data_labels>"
        end
      end

      if (is_global_config==1 or is_line_config==1)
        @line_config = @line_config + current_tag
      end 

      if (is_global_config==1)
        @column_config = @column_config + current_tag
      else
        @column_config = @column_config + column_config_item
      end

      if (is_global_config==1 or is_pie_config==1)
        @pie_config = @pie_config + current_tag
      end 
    }
    f.close()

    f = File.open(file_name,"r")
    @pie_config_repriced="<settings>"
    f.each_line { |line|

      is_global_config=0
      is_line_config=0
      is_pie_config=0
      column_config_item = ""
      current_tag=""

      line = line.strip
      if (line != nil and line !="" and line[0,1]!="#" and line.slice(0,2) !="##")
        items = line.strip.split(":")
        tag_name = items[0].strip
        tag_value = items[1].strip

        #Global configuration 
        if (tag_name == "background")
          is_global_config=1
          current_tag = "<background><color>" + tag_value + "</color></background>"
          @chart_background = "##{tag_value}"
        elsif (tag_name == "text")
          is_global_config=1
          current_tag = "<text_color>" + tag_value + "</text_color>"
        elsif (tag_name == "legend_text")
          is_global_config=1
          current_tag = "<legend><text_color>" + tag_value + "</text_color></legend>"
        elsif (tag_name == "labels_text")
          is_global_config=1
          current_tag = "<labels><label lid='0'><text_color>" +tag_value + "</text_color></label></labels>"
          @reloader = "##{tag_value}"
        #Line configuration
        elsif (tag_name == "grid_x")
          is_line_config = 1
          current_tag = "<grid><x><color>" +tag_value + "</color></x></grid>"
          column_config_item = "<grid><category><color>" +tag_value + "</color></category></grid>"
        elsif (tag_name == "grid_y_left")
          is_line_config = 1
          current_tag = "<grid><y_left><color>" +tag_value + "</color></y_left></grid>"
          column_config_item = "<grid><value><color>" +tag_value + "</color></value></grid>"
        elsif (tag_name == "grid_y_right")
          is_line_config = 1
          current_tag = "<grid><y_right><color>" +tag_value + "</color></y_right></grid>"
        elsif (tag_name == "axis_x")
          is_line_config = 1
          current_tag = "<axes><x><color>"+tag_value+"</color></x></axes>"
          column_config_item = "<axes><category><color>"+tag_value+"</color></category></axes>"
        elsif (tag_name == "axis_y_left")
          is_line_config = 1
          current_tag = "<axes><y_left><color>"+tag_value+"</color></y_left></axes>"
          column_config_item = "<axes><value><color>"+tag_value+"</color></value></axes>"
        elsif (tag_name == "axis_y_right")
          is_line_config = 1
          current_tag = "<axes><y_right><color>"+tag_value+"</color></y_right></axes>"
        elsif (tag_name == "values_x")
          is_line_config = 1
          current_tag = "<values><x><color>"+tag_value+"</color></x></values>"
          column_config_item = "<values><category><color>"+tag_value+"</color></category></values>"
        elsif (tag_name == "values_y_left")
          is_line_config = 1
          current_tag = "<values><y_left><color>"+tag_value+"</color></y_left></values>"
          column_config_item = "<values><value><color>"+tag_value+"</color></value></values>"
        elsif (tag_name == "values_y_right")
          is_line_config = 1
          current_tag = "<values><y_right><color>"+tag_value+"</color></y_right></values>"
        elsif (tag_name == "graphs")
          is_line_config = 1
          #current_tag = "<colors>"+ tag_value +"</colors>"
          color_arr=tag_value.strip.split(",")
          current_tag="<graphs>"
          color_arr.each_with_index {|item, index|
            #puts index
            current_tag = current_tag + "<graph gid='#{index}'><color>"+item+"</color></graph>"
          }
          current_tag= current_tag + "</graphs>"
          column_config_item = "<colors>"+ tag_value +"</colors>"
        elsif (tag_name == "plot_area")
          is_line_config = 1
          current_tag = "<plot_area><color>"+tag_value+"</color></plot_area>"
          column_config_item = "<plot_area><color>"+tag_value+"</color></plot_area>"
        elsif (tag_name == "scroller")
          is_line_config = 1
          current_tag = "<scroller><color>"+tag_value+"</color></scroller>"
        elsif (tag_name == "scroller_bg")
          is_line_config = 1
          current_tag = "<scroller><bg_color>"+tag_value+"</bg_color></scroller>"
        elsif (tag_name == "indicator_bg")
          is_line_config = 1
          current_tag = "<indicator><color>"+tag_value+"</color></indicator>"
        elsif (tag_name == "indicator_text")
          is_line_config = 1
          current_tag = "<indicator><x_balloon_text_color>"+tag_value+"</x_balloon_text_color></indicator>"
        elsif (tag_name == "indicator_selection")
          is_line_config = 1
          current_tag = "<indicator><selection_color>"+tag_value+"</selection_color></indicator>"
        elsif (tag_name == "zoom_out_button_bg")
          is_line_config = 1
          current_tag = "<zoom_out_button><color>"+tag_value+"</color></zoom_out_button>"
        elsif (tag_name == "zoom_out_button_text")
          is_line_config = 1
          current_tag = "<zoom_out_button><text_color>"+tag_value+"</text_color></zoom_out_button>"
        elsif (tag_name == "zoom_out_button_text_hover")
          is_line_config = 1
          current_tag = "<zoom_out_button><text_color_hover>"+tag_value+"</text_color_hover></zoom_out_button>"
        elsif (tag_name == "help_button_bg")
          is_line_config = 1
          current_tag = "<help><button><color>"+tag_value+"</color></button></help>"
        elsif (tag_name == "help_button_text")
          is_line_config = 1
          current_tag = "<help><button><text_color>"+tag_value+"</text_color></button></help>"
        elsif (tag_name == "help_button_text_hover")
          is_line_config = 1
          current_tag = "<help><button><text_color_hover>"+tag_value+"</text_color_hover></button></help>"
        elsif (tag_name == "help_balloon_bg")
          is_line_config = 1
          current_tag = "<help><balloon><color>"+tag_value+"</color></balloon></help>"
        elsif (tag_name == "help_balloon_text")
          is_line_config = 1
          current_tag = "<help><balloon><text_color>"+tag_value+"</text_color></balloon></help>"

        #Pie (3D donut) configuration
        elsif (tag_name == "slices_repriced")
          is_pie_config = 1 
          current_tag = "<pie><colors>" + tag_value + "</colors></pie>"
        elsif (tag_name == "data_labels_text")
          is_pie_config = 1 
          current_tag = "<data_labels><text_color>" + tag_value + "</text_color></data_labels>"
        elsif (tag_name == "data_labels_line")
          is_pie_config = 1 
          current_tag = "<data_labels><line_color>" + tag_value + "</line_color></data_labels>"
        end
      end

      if (is_global_config==1 or is_pie_config==1)
        @pie_config_repriced = @pie_config_repriced + current_tag
      end 
    }
    f.close()

    @line_config = @line_config + "</settings>"
    #@column_config = @column_config + "</settings>"
    @column_config = @column_config + ""
    @pie_config = @pie_config + "</settings>"
    @pie_config_repriced = @pie_config_repriced + "</settings>"

    #puts @column_config
  end

  def set_frequency(size)
    frequency = (size/15).to_i
    @column_frequency = "<values><category><frequency>" + frequency.to_s + "</frequency></category></values>"
  end

end
