class FillRate < ExternalDatabase
  set_table_name "summary_entry_exits"

  def self.days_in_month(month)
    month == 2 ? 28 : 30 + ((month * 1.126).to_i % 2)
  end

  #-----------------------------------------------------------------------------------------------------------------
  # Get data for Fill Rates chart - Tab 1
  #-----------------------------------------------------------------------------------------------------------------

  def self.get_entry_exit_order_type_fill_rate(start_time, end_time, start_date, end_date, ecn_fil_id,isentry_exit, side_indicator, step_type, date_range)

    #Set table name based on step
    step = step_type.to_i
    step_suffix = ""
    if step == 5
      step_suffix = ""
    elsif (step == 15 || step == 30 || step == 60)
      step_suffix = "_"+  step.to_s
    else
      step_suffix = "_60"
    end
    tbl_entry_exits = "summary_entry_exits" + step_suffix
    tbl_order_types = "summary_order_types" + step_suffix

    st = start_date.month
    en = end_date.month
    year = start_date.year

    date_st = start_date
    date_en = end_date
    if start_date.month < end_date.month
      if start_date.month == 2 && ((start_date.year % 4 == 0 && start_date.year % 100 !=0)|| start_date.year % 400 == 0)
        #leap year
        date_en = Time.mktime(start_date.year, start_date.month,days_in_month(start_date.month) + 1,21) 
      else
        date_en = Time.mktime(start_date.year, start_date.month,days_in_month(start_date.month),21) 
      end
    end

    ts_list_temp = ts_list
    # Check number of TS
    num_of_ts = ts_list_temp.size
    num_of_ts = num_of_ts -1
    #Declare variables --------------------------------------------------
    summary_fill_rate = {
      "entry_exit"  => {},
      "order_type"  => {}
    }
    
    ts_list_temp.each do |ts|
      if ts[1].to_i == 0 then
        summary_fill_rate["entry_exit"]["all_ts"] = []
        summary_fill_rate["order_type"]["all_ts"] = []
      else
        summary_fill_rate["entry_exit"]["ts#{ts[1].to_i}"] = []
        summary_fill_rate["order_type"]["ts#{ts[1].to_i}"] = []
      end
    end

    #--------------------------------------------------------------------
    k = 0
    if end_date.year > start_date.year
      k = end_date.year - start_date.year
      en = en + 12 * k
    end
    #puts "st <= en :#{st} #{en}"
    while st <= en 
      db_name_suffix = "#{Time.now.year}#{Time.now.month.to_s.rjust(2,'0')}"

      if st <= 12
        db_name_suffix = "#{year}#{st.to_s.rjust(2,'0')}"
      else
        if k == 1
          db_name_suffix = "#{year + 1}#{(st - 12).to_s.rjust(2,'0')}"
        elsif k == 2
          if st <= 24
            db_name_suffix = "#{year + 1}#{(st - 12).to_s.rjust(2,'0')}"
          else
            db_name_suffix = "#{year + 2}#{(st - 24).to_s.rjust(2,'0')}"
          end
        end
      end

      # lets manually connect to the proper db
      get_database_connection(db_name_suffix, "twt")

      st = st + 1

      filter_conditions = ""

      filter_conditions = " date >= '#{start_date}' and date <= '#{end_date}' and time_stamp > '#{start_time}' and time_stamp <= '#{end_time}' "

      if (ecn_fil_id.to_i != 99)
        filter_conditions = "#{filter_conditions} and ecn_id = #{ecn_fil_id}"
      end
      if (side_indicator != 3)
        en_ex_filter_conditions = "#{filter_conditions} and side = #{side_indicator} "
      else
        en_ex_filter_conditions = filter_conditions
      end

      if step == 5

        #Generate sql string for Entry/Exit chart
        sql_entry_exit = "select date, time_stamp, ts_id, sum(entry_filled) as entry_filled, sum(entry_total) as entry_total, sum(exit_filled) as exit_filled, sum(exit_total) as exit_total"
        sql_entry_exit << " from #{tbl_entry_exits} " 
        sql_entry_exit << " where #{en_ex_filter_conditions} "
        sql_entry_exit << " group by date, time_stamp, ts_id "
        sql_entry_exit << " order by date, time_stamp, ts_id "

        sql_entry_exit_all_ts = "select date, time_stamp, sum(entry_filled) as entry_filled, sum(entry_total) as entry_total, sum(exit_filled) as exit_filled, sum(exit_total) as exit_total"
        sql_entry_exit_all_ts << " from #{tbl_entry_exits} " 
        sql_entry_exit_all_ts << " where #{en_ex_filter_conditions} "
        sql_entry_exit_all_ts << " group by date, time_stamp "
        sql_entry_exit_all_ts << " order by date, time_stamp ASC "

        #Generate sql string for order type
        if isentry_exit == 1 #Entry
          sql_order_type = "select date, time_stamp, ts_id, sum(buy_filled) as buy_filled, sum(buy_total) as buy_total, sum(shortsell_filled) as sell_filled, sum(shortsell_total) as sell_total"
          sql_order_type << " from #{tbl_order_types} " 
          sql_order_type << " where #{filter_conditions}  AND isentry_exit = #{isentry_exit}"
          sql_order_type << " group by date, time_stamp, ts_id "
          sql_order_type << " order by date, time_stamp, ts_id "

          sql_order_type_all_ts = "select date, time_stamp, sum(buy_filled) as buy_filled, sum(buy_total) as buy_total, sum(shortsell_filled) as sell_filled, sum(shortsell_total) as sell_total"
          sql_order_type_all_ts << " from #{tbl_order_types} " 
          sql_order_type_all_ts << " where #{filter_conditions} AND isentry_exit = #{isentry_exit}"
          sql_order_type_all_ts << " group by date, time_stamp "
          sql_order_type_all_ts << " order by date, time_stamp "
        else
          sql_order_type = "select date, time_stamp, ts_id, sum(buy_filled) as buy_filled, sum(buy_total) as buy_total, sum(sell_filled) as sell_filled, sum(sell_total) as sell_total"
          sql_order_type << " from #{tbl_order_types} " 
          sql_order_type << " where #{filter_conditions}  AND isentry_exit = #{isentry_exit}"
          sql_order_type << " group by date, time_stamp, ts_id "
          sql_order_type << " order by date, time_stamp, ts_id "

          sql_order_type_all_ts = "select date, time_stamp, sum(buy_filled) as buy_filled, sum(buy_total) as buy_total, sum(sell_filled) as sell_filled, sum(sell_total) as sell_total"
          sql_order_type_all_ts << " from #{tbl_order_types} " 
          sql_order_type_all_ts << " where #{filter_conditions} AND isentry_exit = #{isentry_exit}"
          sql_order_type_all_ts << " group by date, time_stamp "
          sql_order_type_all_ts << " order by date, time_stamp "
        end

      elsif step == 15 || step == 30 || step == 60
        #Generate sql string for Entry/Exit chart
        sql_entry_exit = "select date, time_stamp, ts_id, sum(entry_filled) as entry_filled, sum(entry_total) as entry_total, sum(exit_filled) as exit_filled, sum(exit_total) as exit_total"
        sql_entry_exit << " from #{tbl_entry_exits} " 
        sql_entry_exit << " where #{en_ex_filter_conditions}"
        sql_entry_exit << " group by date, time_stamp, ts_id "
        sql_entry_exit << " order by date, time_stamp, ts_id "

        sql_entry_exit_all_ts = "select date, time_stamp, sum(entry_filled) as entry_filled, sum(entry_total) as entry_total, sum(exit_filled) as exit_filled, sum(exit_total) as exit_total"
        sql_entry_exit_all_ts << " from #{tbl_entry_exits} " 
        sql_entry_exit_all_ts << " where #{en_ex_filter_conditions}"
        sql_entry_exit_all_ts << " group by date, time_stamp "
        sql_entry_exit_all_ts << " order by date, time_stamp ASC "

        #Generate sql string for order type
        if isentry_exit == 1 #Entry
          sql_order_type = "select date, time_stamp, ts_id, sum(buy_filled) as buy_filled, sum(buy_total) as buy_total, sum(shortsell_filled) as sell_filled, sum(shortsell_total) as sell_total"
          sql_order_type << " from #{tbl_order_types} " 
          sql_order_type << " where #{filter_conditions} AND isentry_exit = #{isentry_exit}"
          sql_order_type << " group by date, time_stamp, ts_id "
          sql_order_type << " order by date, time_stamp, ts_id "

          sql_order_type_all_ts = "select date, time_stamp, sum(buy_filled) as buy_filled, sum(buy_total) as buy_total, sum(shortsell_filled) as sell_filled, sum(shortsell_total) as sell_total"
          sql_order_type_all_ts << " from #{tbl_order_types} " 
          sql_order_type_all_ts << " where #{filter_conditions} AND isentry_exit = #{isentry_exit}"
          sql_order_type_all_ts << " group by date, time_stamp "
          sql_order_type_all_ts << " order by date, time_stamp "
        else
          sql_order_type = "select date, time_stamp, ts_id, sum(buy_filled) as buy_filled, sum(buy_total) as buy_total, sum(sell_filled) as sell_filled, sum(sell_total) as sell_total"
          sql_order_type << " from #{tbl_order_types} " 
          sql_order_type << " where #{filter_conditions} AND isentry_exit = #{isentry_exit}"
          sql_order_type << " group by date, time_stamp, ts_id "
          sql_order_type << " order by date, time_stamp, ts_id "

          sql_order_type_all_ts = "select date, time_stamp, sum(buy_filled) as buy_filled, sum(buy_total) as buy_total, sum(sell_filled) as sell_filled, sum(sell_total) as sell_total"
          sql_order_type_all_ts << " from #{tbl_order_types} " 
          sql_order_type_all_ts << " where #{filter_conditions} AND isentry_exit = #{isentry_exit}"
          sql_order_type_all_ts << " group by date, time_stamp "
          sql_order_type_all_ts << " order by date, time_stamp "
        end

      elsif step == 91 # 1 day
        #Generate sql string for Entry/Exit chart
        sql_entry_exit = "select date,  ts_id, sum(entry_filled) as entry_filled, sum(entry_total) as entry_total, sum(exit_filled) as exit_filled, sum(exit_total) as exit_total"
        sql_entry_exit << " from #{tbl_entry_exits} " 
        sql_entry_exit << " where #{en_ex_filter_conditions}"
        sql_entry_exit << " group by date,  ts_id "
        sql_entry_exit << " order by date,  ts_id "

        sql_entry_exit_all_ts = "select date,  sum(entry_filled) as entry_filled, sum(entry_total) as entry_total, sum(exit_filled) as exit_filled, sum(exit_total) as exit_total"
        sql_entry_exit_all_ts << " from #{tbl_entry_exits} " 
        sql_entry_exit_all_ts << " where #{en_ex_filter_conditions}"
        sql_entry_exit_all_ts << " group by date "
        sql_entry_exit_all_ts << " order by date "

        #Generate sql string for order type
        if isentry_exit == 1 #Entry
          sql_order_type = "select date,  ts_id, sum(buy_filled) as buy_filled, sum(buy_total) as buy_total, sum(shortsell_filled) as sell_filled, sum(shortsell_total) as sell_total"
          sql_order_type << " from #{tbl_order_types} " 
          sql_order_type << " where #{filter_conditions} AND isentry_exit = #{isentry_exit}"
          sql_order_type << " group by date,  ts_id "
          sql_order_type << " order by date,  ts_id "

          sql_order_type_all_ts = "select date,  sum(buy_filled) as buy_filled, sum(buy_total) as buy_total, sum(shortsell_filled) as sell_filled, sum(shortsell_total) as sell_total"
          sql_order_type_all_ts << " from #{tbl_order_types} " 
          sql_order_type_all_ts << " where #{filter_conditions} AND isentry_exit = #{isentry_exit}"
          sql_order_type_all_ts << " group by date "
          sql_order_type_all_ts << " order by date "
        else
          sql_order_type = "select date,  ts_id, sum(buy_filled) as buy_filled, sum(buy_total) as buy_total, sum(sell_filled) as sell_filled, sum(sell_total) as sell_total"
          sql_order_type << " from #{tbl_order_types} " 
          sql_order_type << " where #{filter_conditions} AND isentry_exit = #{isentry_exit}"
          sql_order_type << " group by date,  ts_id "
          sql_order_type << " order by date,  ts_id "

          sql_order_type_all_ts = "select date,  sum(buy_filled) as buy_filled, sum(buy_total) as buy_total, sum(sell_filled) as sell_filled, sum(sell_total) as sell_total"
          sql_order_type_all_ts << " from #{tbl_order_types} " 
          sql_order_type_all_ts << " where #{filter_conditions} AND isentry_exit = #{isentry_exit}"
          sql_order_type_all_ts << " group by date "
          sql_order_type_all_ts << " order by date "
        end

      elsif step == 92 # 1 week
        #Generate sql string for Entry/Exit chart
        sql_entry_exit = "select EXTRACT(WEEK FROM date) as week_num,  ts_id, sum(entry_filled) as entry_filled, sum(entry_total) as entry_total, sum(exit_filled) as exit_filled, sum(exit_total) as exit_total"
        sql_entry_exit << " from #{tbl_entry_exits} " 
        sql_entry_exit << " where #{en_ex_filter_conditions}"
        sql_entry_exit << " group by EXTRACT(WEEK FROM date),  ts_id "
        sql_entry_exit << " order by EXTRACT(WEEK FROM date),  ts_id "

        sql_entry_exit_all_ts = "select EXTRACT(WEEK FROM date) as week_num,  sum(entry_filled) as entry_filled, sum(entry_total) as entry_total, sum(exit_filled) as exit_filled, sum(exit_total) as exit_total"
        sql_entry_exit_all_ts << " from #{tbl_entry_exits} " 
        sql_entry_exit_all_ts << " where #{en_ex_filter_conditions}"
        sql_entry_exit_all_ts << " group by EXTRACT(WEEK FROM date) "
        sql_entry_exit_all_ts << " order by EXTRACT(WEEK FROM date) "

        #Generate sql string for order type
        if isentry_exit == 1 #Entry
          sql_order_type = "select EXTRACT(WEEK FROM date) as week_num,  ts_id, sum(buy_filled) as buy_filled, sum(buy_total) as buy_total, sum(shortsell_filled) as sell_filled, sum(shortsell_total) as sell_total"
          sql_order_type << " from #{tbl_order_types} " 
          sql_order_type << " where #{filter_conditions} AND isentry_exit = #{isentry_exit}"
          sql_order_type << " group by EXTRACT(WEEK FROM date),  ts_id "
          sql_order_type << " order by EXTRACT(WEEK FROM date),  ts_id "

          sql_order_type_all_ts = "select EXTRACT(WEEK FROM date) as week_num,  sum(buy_filled) as buy_filled, sum(buy_total) as buy_total, sum(shortsell_filled) as sell_filled, sum(shortsell_total) as sell_total"
          sql_order_type_all_ts << " from #{tbl_order_types} " 
          sql_order_type_all_ts << " where #{filter_conditions} AND isentry_exit = #{isentry_exit}"
          sql_order_type_all_ts << " group by EXTRACT(WEEK FROM date) "
          sql_order_type_all_ts << " order by EXTRACT(WEEK FROM date) "
        else
          sql_order_type = "select EXTRACT(WEEK FROM date) as week_num,  ts_id, sum(buy_filled) as buy_filled, sum(buy_total) as buy_total, sum(sell_filled) as sell_filled, sum(sell_total) as sell_total"
          sql_order_type << " from #{tbl_order_types} " 
          sql_order_type << " where #{filter_conditions} AND isentry_exit = #{isentry_exit}"
          sql_order_type << " group by EXTRACT(WEEK FROM date),  ts_id "
          sql_order_type << " order by EXTRACT(WEEK FROM date),  ts_id "

          sql_order_type_all_ts = "select EXTRACT(WEEK FROM date) as week_num,  sum(buy_filled) as buy_filled, sum(buy_total) as buy_total, sum(sell_filled) as sell_filled, sum(sell_total) as sell_total"
          sql_order_type_all_ts << " from #{tbl_order_types} " 
          sql_order_type_all_ts << " where #{filter_conditions} AND isentry_exit = #{isentry_exit}"
          sql_order_type_all_ts << " group by EXTRACT(WEEK FROM date) "
          sql_order_type_all_ts << " order by EXTRACT(WEEK FROM date) "
        end

      elsif step == 93 # 1 month
        #Generate sql string for Entry/Exit chart
        sql_entry_exit = "select EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date) as month_num,  ts_id, sum(entry_filled) as entry_filled, sum(entry_total) as entry_total, sum(exit_filled) as exit_filled, sum(exit_total) as exit_total"
        sql_entry_exit << " from #{tbl_entry_exits} " 
        sql_entry_exit << " where #{en_ex_filter_conditions}"
        sql_entry_exit << " group by EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date),  ts_id "
        sql_entry_exit << " order by EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date),  ts_id "

        sql_entry_exit_all_ts = "select EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date) as month_num,  sum(entry_filled) as entry_filled, sum(entry_total) as entry_total, sum(exit_filled) as exit_filled, sum(exit_total) as exit_total"
        sql_entry_exit_all_ts << " from #{tbl_entry_exits} " 
        sql_entry_exit_all_ts << " where #{en_ex_filter_conditions}"
        sql_entry_exit_all_ts << " group by EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date) "
        sql_entry_exit_all_ts << " order by EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date) "

        if isentry_exit == 1 #Entry
        #Generate sql string for order type
          sql_order_type = "select EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date) as month_num,  ts_id, sum(buy_filled) as buy_filled, sum(buy_total) as buy_total, sum(shortsell_filled) as sell_filled, sum(shortsell_total) as sell_total"
          sql_order_type << " from #{tbl_order_types} " 
          sql_order_type << " where #{filter_conditions} AND isentry_exit = #{isentry_exit}"
          sql_order_type << " group by EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date),  ts_id "
          sql_order_type << " order by EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date),  ts_id "

          sql_order_type_all_ts = "select EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date) as month_num,  sum(buy_filled) as buy_filled, sum(buy_total) as buy_total, sum(shortsell_filled) as sell_filled, sum(shortsell_total) as sell_total"
          sql_order_type_all_ts << " from #{tbl_order_types} " 
          sql_order_type_all_ts << " where #{filter_conditions} AND isentry_exit = #{isentry_exit}"
          sql_order_type_all_ts << " group by EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date) "
          sql_order_type_all_ts << " order by EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date) "
        else
          sql_order_type = "select EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date) as month_num,  ts_id, sum(buy_filled) as buy_filled, sum(buy_total) as buy_total, sum(sell_filled) as sell_filled, sum(sell_total) as sell_total"
          sql_order_type << " from #{tbl_order_types} " 
          sql_order_type << " where #{filter_conditions} AND isentry_exit = #{isentry_exit}"
          sql_order_type << " group by EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date),  ts_id "
          sql_order_type << " order by EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date),  ts_id "

          sql_order_type_all_ts = "select EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date) as month_num,  sum(buy_filled) as buy_filled, sum(buy_total) as buy_total, sum(sell_filled) as sell_filled, sum(sell_total) as sell_total"
          sql_order_type_all_ts << " from #{tbl_order_types} " 
          sql_order_type_all_ts << " where #{filter_conditions} AND isentry_exit = #{isentry_exit}"
          sql_order_type_all_ts << " group by EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date) "
          sql_order_type_all_ts << " order by EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date) "
        end

      end

      #Excute sql query------------------------------
      entry_exit = FillRate.find_by_sql(sql_entry_exit)
      entry_exit_all_ts = FillRate.find_by_sql(sql_entry_exit_all_ts)

      order_type = FillRate.find_by_sql(sql_order_type)
      order_type_all_ts = FillRate.find_by_sql(sql_order_type_all_ts)
      
      #Process Entry exit data
      entry_exit.each do |row|
        if row.ts_id.to_i < num_of_ts
          summary_fill_rate["entry_exit"]["ts#{row.ts_id.to_i + 1}"] << row
        end
      end

      entry_exit_all_ts.each do |row|
          summary_fill_rate["entry_exit"]["all_ts"] << row
      end

      #Process Order type
      order_type.each do |row|
        if row.ts_id.to_i < num_of_ts
          summary_fill_rate["order_type"]["ts#{row.ts_id.to_i + 1}"] << row
        end
      end
      order_type_all_ts.each do |row|
        summary_fill_rate["order_type"]["all_ts"] << row
      end

      if en - st > 0
        if (date_st.month+1) == 2 && ((date_st.year % 4 ==0 && date_st.year % 100 != 0)|| date_st.year % 400 ==0)
          #leap year
          date_en = Time.mktime(date_st.year, date_st.month+1,days_in_month(date_st.month+1) + 1,21) 
        else
          if date_st.month == 12
            date_en = Time.mktime(date_st.year + 1, 1,31,21) 
          else
            date_en = Time.mktime(date_st.year, date_st.month+1,days_in_month(date_st.month+1),21) 
          end
        end
      else
        date_en = end_date
      end
      #date_st = Time.mktime(date_st.year, date_st.month + 1,1,6) 
      if date_st.month == 12
        date_st = Time.mktime(date_st.year + 1,  1,1,6) 
      else
        date_st = Time.mktime(date_st.year, date_st.month + 1,1,6) 
      end
    end

    return summary_fill_rate
  end

  #-----------------------------------------------------------------------------------------------------------------
  ### Get data for Fill Rates chart - Tab 2
  #-----------------------------------------------------------------------------------------------------------------
  def self.get_launch_entry_fill_rate(start_time, end_time, start_date, end_date, ecn_entry_id, side_indicator, step_type, date_range)
    #Set table name based on step
    step = step_type.to_i
    step_suffix = ""

    # Set suffix of table 
    if step == 5
      step_suffix = ""
    elsif (step == 15 || step == 30 || step == 60)
      step_suffix = "_"+  step.to_s
    else
      step_suffix = "_60"
    end

    #Set table name
    tbl_launch_entry_ecns = "summary_launch_entry_ecns" + step_suffix

    st = start_date.month
    en = end_date.month
    year = start_date.year

    date_st = start_date
    date_en = end_date
    if start_date.month < end_date.month
      if start_date.month == 2 && ((start_date.year % 4 == 0 && start_date.year % 100 !=0)|| start_date.year % 400 == 0)
        #leap year
        date_en = Time.mktime(start_date.year, start_date.month,days_in_month(start_date.month) + 1,21) 
      else
        date_en = Time.mktime(start_date.year, start_date.month,days_in_month(start_date.month),21) 
      end
    end

    #Declare variables --------------------------------------------------
    summary_fill_rate = {
      "entry_exit"  => {},
      "launch_entry"  => {}
    }
    
    ts_list_temp = ts_list
    # Check number of TS
    num_of_ts = ts_list_temp.size
    num_of_ts = num_of_ts -1
    launch_list_temp = launch_type_list
    ecn_launch_mapping = []
    launch_list_temp.each do |ecn|
      ecn_name = ""
      if ecn[1].to_i == 0 then
        ecn_name = "all_ecn"
      else
        ecn_name = ecn[0].downcase
        ecn_launch_mapping << ecn_name
      end
      ts_list_temp.each do |ts|
        if ts[1].to_i == 0 then
          summary_fill_rate["launch_entry"]["#{ecn_name}_all_ts"] = []
        else
          summary_fill_rate["launch_entry"]["#{ecn_name}_ts#{ts[1].to_i}"] = []
        end
      end
    end
   
    #--------------------------------------------------------------------
    k = 0
    if end_date.year > start_date.year
      k = end_date.year - start_date.year
      en = en + 12 * k
    end
    #puts "st <= en :#{st} #{en}"
    while st <= en 
      db_name_suffix = "#{Time.now.year}#{Time.now.month.to_s.rjust(2,'0')}"
      if st <= 12
        db_name_suffix = "#{year}#{st.to_s.rjust(2,'0')}"
      else
        if k == 1
          db_name_suffix = "#{year + 1}#{(st - 12).to_s.rjust(2,'0')}"
        elsif k == 2
          if st <= 24
            db_name_suffix = "#{year + 1}#{(st - 12).to_s.rjust(2,'0')}"
          else
            db_name_suffix = "#{year + 2}#{(st - 24).to_s.rjust(2,'0')}"
          end
        end
      end

      # lets manually connect to the proper db
      get_database_connection(db_name_suffix, "twt")
      st = st + 1
      filter_conditions = ""
      filter_conditions = " date >= '#{start_date}' and date <= '#{end_date}' and time_stamp > '#{start_time}' and time_stamp <= '#{end_time}' "
      if (side_indicator != 3)
        filter_conditions = "#{filter_conditions} and side = #{side_indicator} "
      end

      # Filter by step
      if step == 5
        #Generate sql string from Launch/Entry ECNs charts
        if (ecn_entry_id.to_i == 99) # All Entry ECNs
          sql_launch_entry = "select date, time_stamp, ts_id, ecn_launch_id, sum(filled) as filled, sum(total) as total"
          sql_launch_entry << " from #{tbl_launch_entry_ecns} " 
          sql_launch_entry << " where #{filter_conditions}  "
          sql_launch_entry << " group by date, time_stamp, ts_id, ecn_launch_id"
          sql_launch_entry << " order by date, time_stamp, ts_id, ecn_launch_id"

          sql_launch_entry_all_ts = "select date, time_stamp, ecn_launch_id, sum(filled) as filled, sum(total) as total"
          sql_launch_entry_all_ts << " from #{tbl_launch_entry_ecns} " 
          sql_launch_entry_all_ts << " where #{filter_conditions}  "
          sql_launch_entry_all_ts << " group by date, time_stamp, ecn_launch_id"
          sql_launch_entry_all_ts << " order by date, time_stamp, ecn_launch_id"

          sql_launch_entry_all_ecn = "select date, time_stamp, ts_id, sum(filled) as filled, sum(total) as total"
          sql_launch_entry_all_ecn << " from #{tbl_launch_entry_ecns} " 
          sql_launch_entry_all_ecn << " where #{filter_conditions}  "
          sql_launch_entry_all_ecn << " group by date, time_stamp, ts_id"
          sql_launch_entry_all_ecn << " order by date, time_stamp, ts_id"

          sql_launch_entry_all_ecn_all_ts = "select date, time_stamp, sum(filled) as filled, sum(total) as total"
          sql_launch_entry_all_ecn_all_ts << " from #{tbl_launch_entry_ecns} " 
          sql_launch_entry_all_ecn_all_ts << " where #{filter_conditions}  "
          sql_launch_entry_all_ecn_all_ts << " group by date, time_stamp "
          sql_launch_entry_all_ecn_all_ts << " order by date, time_stamp "
        else
          sql_launch_entry = "select date, time_stamp, ts_id, ecn_launch_id, sum(filled) as filled, sum(total) as total"
          sql_launch_entry << " from #{tbl_launch_entry_ecns} " 
          sql_launch_entry << " where #{filter_conditions}  and ecn_entry_id = #{ecn_entry_id}"
          sql_launch_entry << " group by date, time_stamp, ts_id, ecn_launch_id"
          sql_launch_entry << " order by date, time_stamp, ts_id, ecn_launch_id"

          sql_launch_entry_all_ts = "select date, time_stamp, ecn_launch_id, sum(filled) as filled, sum(total) as total"
          sql_launch_entry_all_ts << " from #{tbl_launch_entry_ecns} " 
          sql_launch_entry_all_ts << " where #{filter_conditions}  and ecn_entry_id = #{ecn_entry_id}"
          sql_launch_entry_all_ts << " group by date, time_stamp, ecn_launch_id"
          sql_launch_entry_all_ts << " order by date, time_stamp, ecn_launch_id"

          sql_launch_entry_all_ecn = "select date, time_stamp, ts_id, sum(filled) as filled, sum(total) as total"
          sql_launch_entry_all_ecn << " from #{tbl_launch_entry_ecns} " 
          sql_launch_entry_all_ecn << " where #{filter_conditions}  and ecn_entry_id = #{ecn_entry_id}"
          sql_launch_entry_all_ecn << " group by date, time_stamp, ts_id"
          sql_launch_entry_all_ecn << " order by date, time_stamp, ts_id"

          sql_launch_entry_all_ecn_all_ts = "select date, time_stamp, sum(filled) as filled, sum(total) as total"
          sql_launch_entry_all_ecn_all_ts << " from #{tbl_launch_entry_ecns} " 
          sql_launch_entry_all_ecn_all_ts << " where #{filter_conditions}  and ecn_entry_id = #{ecn_entry_id}"
          sql_launch_entry_all_ecn_all_ts << " group by date, time_stamp "
          sql_launch_entry_all_ecn_all_ts << " order by date, time_stamp "
        end

      elsif step == 15 || step == 30 || step == 60
        #Generate sql string from Launch/Entry ECNs charts
        if (ecn_entry_id.to_i == 99) # All Entry ECNs
          sql_launch_entry = "select date, time_stamp, ts_id, ecn_launch_id, sum(filled) as filled, sum(total) as total"
          sql_launch_entry << " from #{tbl_launch_entry_ecns} " 
          sql_launch_entry << " where #{filter_conditions} "
          sql_launch_entry << " group by date, time_stamp, ts_id, ecn_launch_id"
          sql_launch_entry << " order by date, time_stamp, ts_id, ecn_launch_id"

          sql_launch_entry_all_ts = "select date, time_stamp, ecn_launch_id, sum(filled) as filled, sum(total) as total"
          sql_launch_entry_all_ts << " from #{tbl_launch_entry_ecns} " 
          sql_launch_entry_all_ts << " where #{filter_conditions} "
          sql_launch_entry_all_ts << " group by date, time_stamp, ecn_launch_id"
          sql_launch_entry_all_ts << " order by date, time_stamp, ecn_launch_id"

          sql_launch_entry_all_ecn = "select date, time_stamp, ts_id, sum(filled) as filled, sum(total) as total"
          sql_launch_entry_all_ecn << " from #{tbl_launch_entry_ecns} " 
          sql_launch_entry_all_ecn << " where #{filter_conditions} "
          sql_launch_entry_all_ecn << " group by date, time_stamp, ts_id"
          sql_launch_entry_all_ecn << " order by date, time_stamp, ts_id"

          sql_launch_entry_all_ecn_all_ts = "select date, time_stamp, sum(filled) as filled, sum(total) as total"
          sql_launch_entry_all_ecn_all_ts << " from #{tbl_launch_entry_ecns} " 
          sql_launch_entry_all_ecn_all_ts << " where #{filter_conditions} "
          sql_launch_entry_all_ecn_all_ts << " group by date, time_stamp "
          sql_launch_entry_all_ecn_all_ts << " order by date, time_stamp "
        else
          sql_launch_entry = "select date, time_stamp, ts_id, ecn_launch_id, sum(filled) as filled, sum(total) as total"
          sql_launch_entry << " from #{tbl_launch_entry_ecns} " 
          sql_launch_entry << " where #{filter_conditions} and ecn_entry_id = #{ecn_entry_id}"
          sql_launch_entry << " group by date, time_stamp, ts_id, ecn_launch_id"
          sql_launch_entry << " order by date, time_stamp, ts_id, ecn_launch_id"

          sql_launch_entry_all_ts = "select date, time_stamp, ecn_launch_id, sum(filled) as filled, sum(total) as total"
          sql_launch_entry_all_ts << " from #{tbl_launch_entry_ecns} " 
          sql_launch_entry_all_ts << " where #{filter_conditions} and ecn_entry_id = #{ecn_entry_id}"
          sql_launch_entry_all_ts << " group by date, time_stamp, ecn_launch_id"
          sql_launch_entry_all_ts << " order by date, time_stamp, ecn_launch_id"

          sql_launch_entry_all_ecn = "select date, time_stamp, ts_id, sum(filled) as filled, sum(total) as total"
          sql_launch_entry_all_ecn << " from #{tbl_launch_entry_ecns} " 
          sql_launch_entry_all_ecn << " where #{filter_conditions} and ecn_entry_id = #{ecn_entry_id}"
          sql_launch_entry_all_ecn << " group by date, time_stamp, ts_id"
          sql_launch_entry_all_ecn << " order by date, time_stamp, ts_id"

          sql_launch_entry_all_ecn_all_ts = "select date, time_stamp, sum(filled) as filled, sum(total) as total"
          sql_launch_entry_all_ecn_all_ts << " from #{tbl_launch_entry_ecns} " 
          sql_launch_entry_all_ecn_all_ts << " where #{filter_conditions} and ecn_entry_id = #{ecn_entry_id}"
          sql_launch_entry_all_ecn_all_ts << " group by date, time_stamp "
          sql_launch_entry_all_ecn_all_ts << " order by date, time_stamp "

        end

      elsif step == 91 # 1 day
        #Generate sql string from Launch/Entry ECNs charts
        if (ecn_entry_id.to_i == 99) # All Entry ECNs
          sql_launch_entry = "select date,  ts_id, ecn_launch_id, sum(filled) as filled, sum(total) as total"
          sql_launch_entry << " from #{tbl_launch_entry_ecns} " 
          sql_launch_entry << " where #{filter_conditions} "
          sql_launch_entry << " group by date,  ts_id, ecn_launch_id"
          sql_launch_entry << " order by date,  ts_id, ecn_launch_id"

          sql_launch_entry_all_ts = "select date,  ecn_launch_id, sum(filled) as filled, sum(total) as total"
          sql_launch_entry_all_ts << " from #{tbl_launch_entry_ecns} " 
          sql_launch_entry_all_ts << " where #{filter_conditions} "
          sql_launch_entry_all_ts << " group by date,  ecn_launch_id"
          sql_launch_entry_all_ts << " order by date,  ecn_launch_id"

          sql_launch_entry_all_ecn = "select date,  ts_id, sum(filled) as filled, sum(total) as total"
          sql_launch_entry_all_ecn << " from #{tbl_launch_entry_ecns} " 
          sql_launch_entry_all_ecn << " where #{filter_conditions} "
          sql_launch_entry_all_ecn << " group by date,  ts_id"
          sql_launch_entry_all_ecn << " order by date,  ts_id"

          sql_launch_entry_all_ecn_all_ts = "select date,  sum(filled) as filled, sum(total) as total"
          sql_launch_entry_all_ecn_all_ts << " from #{tbl_launch_entry_ecns} " 
          sql_launch_entry_all_ecn_all_ts << " where #{filter_conditions} "
          sql_launch_entry_all_ecn_all_ts << " group by date "
          sql_launch_entry_all_ecn_all_ts << " order by date "
        else
          sql_launch_entry = "select date,  ts_id, ecn_launch_id, sum(filled) as filled, sum(total) as total"
          sql_launch_entry << " from #{tbl_launch_entry_ecns} " 
          sql_launch_entry << " where #{filter_conditions} and ecn_entry_id = #{ecn_entry_id}"
          sql_launch_entry << " group by date,  ts_id, ecn_launch_id"
          sql_launch_entry << " order by date,  ts_id, ecn_launch_id"

          sql_launch_entry_all_ts = "select date,  ecn_launch_id, sum(filled) as filled, sum(total) as total"
          sql_launch_entry_all_ts << " from #{tbl_launch_entry_ecns} " 
          sql_launch_entry_all_ts << " where #{filter_conditions} and ecn_entry_id = #{ecn_entry_id}"
          sql_launch_entry_all_ts << " group by date,  ecn_launch_id"
          sql_launch_entry_all_ts << " order by date,  ecn_launch_id"

          sql_launch_entry_all_ecn = "select date,  ts_id, sum(filled) as filled, sum(total) as total"
          sql_launch_entry_all_ecn << " from #{tbl_launch_entry_ecns} " 
          sql_launch_entry_all_ecn << " where #{filter_conditions} and ecn_entry_id = #{ecn_entry_id}"
          sql_launch_entry_all_ecn << " group by date,  ts_id"
          sql_launch_entry_all_ecn << " order by date,  ts_id"

          sql_launch_entry_all_ecn_all_ts = "select date,  sum(filled) as filled, sum(total) as total"
          sql_launch_entry_all_ecn_all_ts << " from #{tbl_launch_entry_ecns} " 
          sql_launch_entry_all_ecn_all_ts << " where #{filter_conditions} and ecn_entry_id = #{ecn_entry_id}"
          sql_launch_entry_all_ecn_all_ts << " group by date "
          sql_launch_entry_all_ecn_all_ts << " order by date "

        end

      elsif step == 92 # 1 week
        #Generate sql string from Launch/Entry ECNs charts
        if (ecn_entry_id.to_i == 99) # All Entry ECNs
          sql_launch_entry = "select EXTRACT(WEEK FROM date) as week_num,  ts_id, ecn_launch_id, sum(filled) as filled, sum(total) as total"
          sql_launch_entry << " from #{tbl_launch_entry_ecns} " 
          sql_launch_entry << " where #{filter_conditions} "
          sql_launch_entry << " group by EXTRACT(WEEK FROM date),  ts_id, ecn_launch_id"
          sql_launch_entry << " order by EXTRACT(WEEK FROM date),  ts_id, ecn_launch_id"

          sql_launch_entry_all_ts = "select EXTRACT(WEEK FROM date) as week_num,  ecn_launch_id, sum(filled) as filled, sum(total) as total"
          sql_launch_entry_all_ts << " from #{tbl_launch_entry_ecns} " 
          sql_launch_entry_all_ts << " where #{filter_conditions} "
          sql_launch_entry_all_ts << " group by EXTRACT(WEEK FROM date),  ecn_launch_id"
          sql_launch_entry_all_ts << " order by EXTRACT(WEEK FROM date),  ecn_launch_id"

          sql_launch_entry_all_ecn = "select EXTRACT(WEEK FROM date) as week_num,  ts_id, sum(filled) as filled, sum(total) as total"
          sql_launch_entry_all_ecn << " from #{tbl_launch_entry_ecns} " 
          sql_launch_entry_all_ecn << " where #{filter_conditions} "
          sql_launch_entry_all_ecn << " group by EXTRACT(WEEK FROM date),  ts_id"
          sql_launch_entry_all_ecn << " order by EXTRACT(WEEK FROM date),  ts_id"

          sql_launch_entry_all_ecn_all_ts = "select EXTRACT(WEEK FROM date) as week_num,  sum(filled) as filled, sum(total) as total"
          sql_launch_entry_all_ecn_all_ts << " from #{tbl_launch_entry_ecns} " 
          sql_launch_entry_all_ecn_all_ts << " where #{filter_conditions} "
          sql_launch_entry_all_ecn_all_ts << " group by EXTRACT(WEEK FROM date) "
          sql_launch_entry_all_ecn_all_ts << " order by EXTRACT(WEEK FROM date) "
        else
          sql_launch_entry = "select EXTRACT(WEEK FROM date) as week_num,  ts_id, ecn_launch_id, sum(filled) as filled, sum(total) as total"
          sql_launch_entry << " from #{tbl_launch_entry_ecns} " 
          sql_launch_entry << " where #{filter_conditions} and ecn_entry_id = #{ecn_entry_id}"
          sql_launch_entry << " group by EXTRACT(WEEK FROM date),  ts_id, ecn_launch_id"
          sql_launch_entry << " order by EXTRACT(WEEK FROM date),  ts_id, ecn_launch_id"

          sql_launch_entry_all_ts = "select EXTRACT(WEEK FROM date) as week_num,  ecn_launch_id, sum(filled) as filled, sum(total) as total"
          sql_launch_entry_all_ts << " from #{tbl_launch_entry_ecns} " 
          sql_launch_entry_all_ts << " where #{filter_conditions} and ecn_entry_id = #{ecn_entry_id}"
          sql_launch_entry_all_ts << " group by EXTRACT(WEEK FROM date),  ecn_launch_id"
          sql_launch_entry_all_ts << " order by EXTRACT(WEEK FROM date),  ecn_launch_id"

          sql_launch_entry_all_ecn = "select EXTRACT(WEEK FROM date) as week_num,  ts_id, sum(filled) as filled, sum(total) as total"
          sql_launch_entry_all_ecn << " from #{tbl_launch_entry_ecns} " 
          sql_launch_entry_all_ecn << " where #{filter_conditions} and ecn_entry_id = #{ecn_entry_id}"
          sql_launch_entry_all_ecn << " group by EXTRACT(WEEK FROM date),  ts_id"
          sql_launch_entry_all_ecn << " order by EXTRACT(WEEK FROM date),  ts_id"

          sql_launch_entry_all_ecn_all_ts = "select EXTRACT(WEEK FROM date) as week_num,  sum(filled) as filled, sum(total) as total"
          sql_launch_entry_all_ecn_all_ts << " from #{tbl_launch_entry_ecns} " 
          sql_launch_entry_all_ecn_all_ts << " where #{filter_conditions} and ecn_entry_id = #{ecn_entry_id}"
          sql_launch_entry_all_ecn_all_ts << " group by EXTRACT(WEEK FROM date) "
          sql_launch_entry_all_ecn_all_ts << " order by EXTRACT(WEEK FROM date) "
        end

      elsif step == 93 # 1 month
        #Generate sql string from Launch/Entry ECNs charts
        sql_launch_entry = "select EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date) as month_num,  ts_id, ecn_launch_id, sum(filled) as filled, sum(total) as total"
        sql_launch_entry << " from #{tbl_launch_entry_ecns} " 
        sql_launch_entry << " where #{filter_conditions} and ecn_entry_id = #{ecn_entry_id}"
        sql_launch_entry << " group by EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date),  ts_id, ecn_launch_id"
        sql_launch_entry << " order by EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date),  ts_id, ecn_launch_id"

        sql_launch_entry_all_ts = "select EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date) as month_num,  ecn_launch_id, sum(filled) as filled, sum(total) as total"
        sql_launch_entry_all_ts << " from #{tbl_launch_entry_ecns} " 
        sql_launch_entry_all_ts << " where #{filter_conditions} and ecn_entry_id = #{ecn_entry_id}"
        sql_launch_entry_all_ts << " group by EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date),  ecn_launch_id"
        sql_launch_entry_all_ts << " order by EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date),  ecn_launch_id"

        sql_launch_entry_all_ecn = "select EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date) as month_num,  ts_id, sum(filled) as filled, sum(total) as total"
        sql_launch_entry_all_ecn << " from #{tbl_launch_entry_ecns} " 
        sql_launch_entry_all_ecn << " where #{filter_conditions} and ecn_entry_id = #{ecn_entry_id}"
        sql_launch_entry_all_ecn << " group by EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date),  ts_id"
        sql_launch_entry_all_ecn << " order by EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date),  ts_id"

        sql_launch_entry_all_ecn_all_ts = "select EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date) as month_num,  sum(filled) as filled, sum(total) as total"
        sql_launch_entry_all_ecn_all_ts << " from #{tbl_launch_entry_ecns} " 
        sql_launch_entry_all_ecn_all_ts << " where #{filter_conditions} and ecn_entry_id = #{ecn_entry_id}"
        sql_launch_entry_all_ecn_all_ts << " group by EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date) "
        sql_launch_entry_all_ecn_all_ts << " order by EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date) "

      end

      #Excute sql query------------------------------
      launch_entry = FillRate.find_by_sql(sql_launch_entry)
      launch_entry_all_ts = FillRate.find_by_sql(sql_launch_entry_all_ts)
      launch_entry_all_ecn = FillRate.find_by_sql(sql_launch_entry_all_ecn)
      launch_entry_all_ecn_all_ts = FillRate.find_by_sql(sql_launch_entry_all_ecn_all_ts)

      launch_entry.each do |row|
        if row.ts_id.to_i < num_of_ts
          ecn = ecn_launch_mapping[row.ecn_launch_id.to_i]
          summary_fill_rate["launch_entry"]["#{ecn}_ts#{row.ts_id.to_i + 1}"] << row
        end
      end

      launch_entry_all_ts.each do |row|
        ecn = ecn_launch_mapping[row.ecn_launch_id.to_i]
        summary_fill_rate["launch_entry"]["#{ecn}_all_ts"] << row
      end

      launch_entry_all_ecn.each do |row|
        if row.ts_id.to_i < num_of_ts
          summary_fill_rate["launch_entry"]["all_ecn_ts#{row.ts_id.to_i + 1}"] << row
        end
      end

      launch_entry_all_ecn_all_ts.each do |row|
        summary_fill_rate["launch_entry"]["all_ecn_all_ts"] << row
      end

      if en - st > 0
        if (date_st.month+1) == 2 && ((date_st.year % 4 ==0 && date_st.year % 100 != 0)|| date_st.year % 400 ==0)
          #leap year
          date_en = Time.mktime(date_st.year, date_st.month+1,days_in_month(date_st.month+1) + 1,21) 
        else
          if date_st.month == 12
            date_en = Time.mktime(date_st.year + 1, 1,31,21) 
          else
            date_en = Time.mktime(date_st.year, date_st.month+1,days_in_month(date_st.month+1),21) 
          end
        end
      else
        date_en = end_date
      end
      #date_st = Time.mktime(date_st.year, date_st.month + 1,1,6) 
      if date_st.month == 12
        date_st = Time.mktime(date_st.year + 1,  1,1,6) 
      else
        date_st = Time.mktime(date_st.year, date_st.month + 1,1,6) 
      end
    end

    return summary_fill_rate
  end


  def self.get_fill_rate_entry_exit(start_date, end_date, step_type)
    step = step_type.to_i
    step_suffix = ""
    if step == 5
      step_suffix = ""
    elsif (step == 15 || step == 30 || step == 60)
      step_suffix = "_"+ step.to_s
    else
      step_suffix = "_60"
    end
    #Set table name
    tbl_entry_exits = "summary_entry_exits" + step_suffix
    # Start date and end date
    start_month = start_date.month
    end_month = end_date.month
    year = start_date.year
    date_st = start_date
    date_en = end_date
    date_en = Time.mktime(start_date.year, start_date.month, start_date.day)

    ts_list_temp = ts_list
    # Check number of TS
    num_of_ts = ts_list_temp.size
    num_of_ts = num_of_ts -1
    #Declare variables --------------------------------------------------
    summary_fill_rate = {
      "entry_exit"  => {}
    }
    
    ts_list_temp.each do |ts|
      if ts[1].to_i == 0 then
        summary_fill_rate["entry_exit"]["all_ts"] = []
      else
        summary_fill_rate["entry_exit"]["ts#{ts[1].to_i}"] = []
      end
    end

    #--------------------------------------------------------------------
    check_year = 0
    if end_date.year > start_date.year
      check_year = end_date.year - start_date.year
      end_month = end_month + 12 * check_year
    end

    while start_month <= end_month
      db_name_suffix = "#{Time.now.year}#{Time.now.month.to_s.rjust(2, '0')}"
      if start_month <= 12
        db_name_suffix = "#{year}#{start_month.to_s.rjust(2, '0')}"
      else
        if check_year == 1
          db_name_suffix = "#{year + 1}#{(start_month - 12).to_s.rjust(2, '0')}"
        elsif check_year == 2
          if start_month <= 24
            db_name_suffix = "#{year + 1}#{(start_month - 12).to_s.rjust(2, '0')}"
          else
            db_name_suffix = "#{year + 2}#{(start_month - 24).to_s.rjust(2, '0')}"
          end
        end
      end
      # lets manually connect to the proper db
      get_database_connection(db_name_suffix, "twt")
      start_month = start_month + 1
      en_ex_filter_conditions = " date >= '#{start_date}' and date <= '#{end_date}' and date != '2013-11-7' "  
      if step == 5
      #Generate sql string for Entry/Exit chart
        sql_entry_exit = "select date, time_stamp, ts_id, sum(entry_filled) as entry_filled, sum(entry_total) as entry_total, sum(exit_filled) as exit_filled, sum(exit_total) as exit_total"
        sql_entry_exit << " from #{tbl_entry_exits} "
        sql_entry_exit << " where #{en_ex_filter_conditions} "
        sql_entry_exit << " group by date, time_stamp, ts_id "
        sql_entry_exit << " order by date, time_stamp, ts_id "

        sql_entry_exit_all_ts = "select date, time_stamp, sum(entry_filled) as entry_filled, sum(entry_total) as entry_total, sum(exit_filled) as exit_filled, sum(exit_total) as exit_total"
        sql_entry_exit_all_ts << " from #{tbl_entry_exits} "
        sql_entry_exit_all_ts << " where #{en_ex_filter_conditions} "
        sql_entry_exit_all_ts << " group by date, time_stamp "
        sql_entry_exit_all_ts << " order by date, time_stamp ASC "

      elsif step == 15 || step == 30 || step == 60
      #Generate sql string for Entry/Exit chart
        sql_entry_exit = "select date, time_stamp, ts_id, sum(entry_filled) as entry_filled, sum(entry_total) as entry_total, sum(exit_filled) as exit_filled, sum(exit_total) as exit_total"
        sql_entry_exit << " from #{tbl_entry_exits} "
        sql_entry_exit << " where #{en_ex_filter_conditions}"
        sql_entry_exit << " group by date, time_stamp, ts_id "
        sql_entry_exit << " order by date, time_stamp, ts_id "

        sql_entry_exit_all_ts = "select date, time_stamp, sum(entry_filled) as entry_filled, sum(entry_total) as entry_total, sum(exit_filled) as exit_filled, sum(exit_total) as exit_total"
        sql_entry_exit_all_ts << " from #{tbl_entry_exits} "
        sql_entry_exit_all_ts << " where #{en_ex_filter_conditions}"
        sql_entry_exit_all_ts << " group by date, time_stamp "
        sql_entry_exit_all_ts << " order by date, time_stamp ASC "

      elsif step == 91 # 1 day
        #Generate sql string for Entry/Exit chart
        sql_entry_exit = "select date,  ts_id, sum(entry_filled) as entry_filled, sum(entry_total) as entry_total, sum(exit_filled) as exit_filled, sum(exit_total) as exit_total"
        sql_entry_exit << " from #{tbl_entry_exits} "
        sql_entry_exit << " where #{en_ex_filter_conditions}"
        sql_entry_exit << " group by date,  ts_id "
        sql_entry_exit << " order by date,  ts_id "

        sql_entry_exit_all_ts = "select date,  sum(entry_filled) as entry_filled, sum(entry_total) as entry_total, sum(exit_filled) as exit_filled, sum(exit_total) as exit_total"
        sql_entry_exit_all_ts << " from #{tbl_entry_exits} "
        sql_entry_exit_all_ts << " where #{en_ex_filter_conditions}"
        sql_entry_exit_all_ts << " group by date "
        sql_entry_exit_all_ts << " order by date "

      elsif step == 92 # 1 week
        #Generate sql string for Entry/Exit chart
        sql_entry_exit = "select EXTRACT(MONTH FROM date) as month_num, EXTRACT(WEEK FROM date) as week_num, ts_id, sum(entry_filled) as entry_filled, sum(entry_total) as entry_total, sum(exit_filled) as exit_filled, sum(exit_total) as exit_total"
        sql_entry_exit << " from #{tbl_entry_exits} "
        sql_entry_exit << " where #{en_ex_filter_conditions}"
        sql_entry_exit << " group by EXTRACT(MONTH FROM date), EXTRACT(WEEK FROM date),  ts_id "
        sql_entry_exit << " order by EXTRACT(MONTH FROM date), EXTRACT(WEEK FROM date),  ts_id "

        sql_entry_exit_all_ts = "select EXTRACT(MONTH FROM date) as month_num, EXTRACT(WEEK FROM date) as week_num, sum(entry_filled) as entry_filled, sum(entry_total) as entry_total, sum(exit_filled) as exit_filled, sum(exit_total) as exit_total"
        sql_entry_exit_all_ts << " from #{tbl_entry_exits} "
        sql_entry_exit_all_ts << " where #{en_ex_filter_conditions}"
        sql_entry_exit_all_ts << " group by EXTRACT(MONTH FROM date), EXTRACT(WEEK FROM date) "
        sql_entry_exit_all_ts << " order by EXTRACT(MONTH FROM date), EXTRACT(WEEK FROM date) "

      elsif step == 93 # 1 month
        #Generate sql string for Entry/Exit chart
        sql_entry_exit = "select EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date) as month_num,  ts_id, sum(entry_filled) as entry_filled, sum(entry_total) as entry_total, sum(exit_filled) as exit_filled, sum(exit_total) as exit_total"
        sql_entry_exit << " from #{tbl_entry_exits} "
        sql_entry_exit << " where #{en_ex_filter_conditions}"
        sql_entry_exit << " group by EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date),  ts_id "
        sql_entry_exit << " order by EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date),  ts_id "

        sql_entry_exit_all_ts = "select EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date) as month_num,  sum(entry_filled) as entry_filled, sum(entry_total) as entry_total, sum(exit_filled) as exit_filled, sum(exit_total) as exit_total"
        sql_entry_exit_all_ts << " from #{tbl_entry_exits} "
        sql_entry_exit_all_ts << " where #{en_ex_filter_conditions}"
        sql_entry_exit_all_ts << " group by EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date) "
        sql_entry_exit_all_ts << " order by EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date) "

      end

      entry_exit = EntryExit.find_by_sql(sql_entry_exit)
      entry_exit_all_ts = EntryExit.find_by_sql(sql_entry_exit_all_ts)
      entry_exit.each do |row|
        if row.ts_id.to_i < num_of_ts
          summary_fill_rate["entry_exit"]["ts#{row.ts_id.to_i + 1}"] << row
        end
      end

      entry_exit_all_ts.each do |row|
        summary_fill_rate["entry_exit"]["all_ts"] << row
      end

      if end_month - start_month > 0
        date_en = Time.mktime(date_st.year, date_st.month+1, date_st.day)
      else
        date_en = end_date
      end
      if date_st.month == 12
        date_st = Time.mktime(date_st.year + 1, 1, 1, 6)
      else
        date_st = Time.mktime(date_st.year, date_st.month + 1, 1, 6)
      end
    end
    return summary_fill_rate
  end

  
  # Get data for Launch Fill Rates Entry chart
  #--------------------------------------------------------------------------------------------------------------
  def self.get_launch_fill_rate(start_date, end_date, step_type)
    #Set table name based on step
    step = step_type.to_i
    step_suffix = ""
    # Set suffix of table 
    if step == 5
      step_suffix = ""
    elsif (step == 15 || step == 30 || step == 60)
      step_suffix = "_"+  step.to_s
    else
      step_suffix = "_60"
    end
    #Set table name
    tbl_launch_entry_ecns = "summary_launch_entry_ecns" + step_suffix
    tbl_launch_exit_ecns = "summary_launch_exit_ecns" + step_suffix
    st = start_date.month
    en = end_date.month
    year = start_date.year
    date_st = start_date
    date_en = end_date
    if start_date.month < end_date.month
      if start_date.month == 2 && ((start_date.year % 4 == 0 && start_date.year % 100 !=0)|| start_date.year % 400 == 0)
        date_en = Time.mktime(start_date.year, start_date.month,days_in_month(start_date.month) + 1,21) 
      else
        date_en = Time.mktime(start_date.year, start_date.month,days_in_month(start_date.month),21) 
      end
    end

    #Declare variables --------------------------------------------------
    ts_list_temp = ts_list
    # Check number of TS
    num_of_ts = ts_list_temp.size
    num_of_ts = num_of_ts -1
    launch_list_temp = launch_type_list

    summary_fill_rate = {
      "entry_exit"  => {},
      "launch_entry"  => {},
      "launch_exit"  => {}
    }

    ecn_launch_mapping = []
    launch_list_temp.each do |ecn|
      ecn_name = ""
      if ecn[1].to_i == 0 then
        ecn_name = "all_ecn"
      else
        ecn_name = ecn[0].downcase
        ecn_launch_mapping << ecn_name
      end

      ts_list_temp.each do |ts|
        if ts[1].to_i == 0 then
          summary_fill_rate["launch_entry"]["#{ecn_name}_all_ts"] = []
          summary_fill_rate["launch_exit"]["#{ecn_name}_all_ts"] = []
        else
          summary_fill_rate["launch_entry"]["#{ecn_name}_ts#{ts[1].to_i}"] = []
          summary_fill_rate["launch_exit"]["#{ecn_name}_ts#{ts[1].to_i}"] = []
        end
      end
    end
    #--------------------------------------------------------------------
    k = 0
    if end_date.year > start_date.year
      k = end_date.year - start_date.year
      en = en + 12 * k
    end
    while st <= en 
      db_name_suffix = "#{Time.now.year}#{Time.now.month.to_s.rjust(2,'0')}"
      if st <= 12
        db_name_suffix = "#{year}#{st.to_s.rjust(2,'0')}"
      else
        if k == 1
          db_name_suffix = "#{year + 1}#{(st - 12).to_s.rjust(2,'0')}"
        elsif k == 2
          if st <= 24
            db_name_suffix = "#{year + 1}#{(st - 12).to_s.rjust(2,'0')}"
          else
            db_name_suffix = "#{year + 2}#{(st - 24).to_s.rjust(2,'0')}"
          end
        end
      end

      # lets manually connect to the proper db
      get_database_connection(db_name_suffix, "twt")
      st = st + 1
      filter_conditions = " date >= '#{start_date}' and date <= '#{end_date}' and date != '2013-11-7'"
      if step == 5
        sql_launch_entry = "select date, time_stamp, ts_id, ecn_launch_id, sum(filled) as filled, sum(total) as total"
        sql_launch_entry << " from #{tbl_launch_entry_ecns} " 
        sql_launch_entry << " where #{filter_conditions}  "
        sql_launch_entry << " group by date, time_stamp, ts_id, ecn_launch_id"
        sql_launch_entry << " order by date, time_stamp, ts_id, ecn_launch_id"

        sql_launch_entry_all_ts = "select date, time_stamp, ecn_launch_id, sum(filled) as filled, sum(total) as total"
        sql_launch_entry_all_ts << " from #{tbl_launch_entry_ecns} " 
        sql_launch_entry_all_ts << " where #{filter_conditions}  "
        sql_launch_entry_all_ts << " group by date, time_stamp, ecn_launch_id"
        sql_launch_entry_all_ts << " order by date, time_stamp, ecn_launch_id"

        sql_launch_entry_all_ecn = "select date, time_stamp, ts_id, sum(filled) as filled, sum(total) as total"
        sql_launch_entry_all_ecn << " from #{tbl_launch_entry_ecns} " 
        sql_launch_entry_all_ecn << " where #{filter_conditions}  "
        sql_launch_entry_all_ecn << " group by date, time_stamp, ts_id"
        sql_launch_entry_all_ecn << " order by date, time_stamp, ts_id"

        sql_launch_entry_all_ecn_all_ts = "select date, time_stamp, sum(filled) as filled, sum(total) as total"
        sql_launch_entry_all_ecn_all_ts << " from #{tbl_launch_entry_ecns} " 
        sql_launch_entry_all_ecn_all_ts << " where #{filter_conditions}  "
        sql_launch_entry_all_ecn_all_ts << " group by date, time_stamp "
        sql_launch_entry_all_ecn_all_ts << " order by date, time_stamp "

        # Launch Exit
        sql_launch_exit = "select date, time_stamp, ts_id, ecn_launch_id, sum(filled) as filled, sum(total) as total"
        sql_launch_exit << " from #{tbl_launch_exit_ecns} " 
        sql_launch_exit << " where #{filter_conditions}  "
        sql_launch_exit << " group by date, time_stamp, ts_id, ecn_launch_id"
        sql_launch_exit << " order by date, time_stamp, ts_id, ecn_launch_id"

        sql_launch_exit_all_ts = "select date, time_stamp, ecn_launch_id, sum(filled) as filled, sum(total) as total"
        sql_launch_exit_all_ts << " from #{tbl_launch_exit_ecns} " 
        sql_launch_exit_all_ts << " where #{filter_conditions}  "
        sql_launch_exit_all_ts << " group by date, time_stamp, ecn_launch_id"
        sql_launch_exit_all_ts << " order by date, time_stamp, ecn_launch_id"

        sql_launch_exit_all_ecn = "select date, time_stamp, ts_id, sum(filled) as filled, sum(total) as total"
        sql_launch_exit_all_ecn << " from #{tbl_launch_exit_ecns} " 
        sql_launch_exit_all_ecn << " where #{filter_conditions}  "
        sql_launch_exit_all_ecn << " group by date, time_stamp, ts_id"
        sql_launch_exit_all_ecn << " order by date, time_stamp, ts_id"

        sql_launch_exit_all_ecn_all_ts = "select date, time_stamp, sum(filled) as filled, sum(total) as total"
        sql_launch_exit_all_ecn_all_ts << " from #{tbl_launch_exit_ecns} " 
        sql_launch_exit_all_ecn_all_ts << " where #{filter_conditions}  "
        sql_launch_exit_all_ecn_all_ts << " group by date, time_stamp "
        sql_launch_exit_all_ecn_all_ts << " order by date, time_stamp "

      elsif step == 15 || step == 30 || step == 60
        sql_launch_entry = "select date, time_stamp, ts_id, ecn_launch_id, sum(filled) as filled, sum(total) as total"
        sql_launch_entry << " from #{tbl_launch_entry_ecns} " 
        sql_launch_entry << " where #{filter_conditions} "
        sql_launch_entry << " group by date, time_stamp, ts_id, ecn_launch_id"
        sql_launch_entry << " order by date, time_stamp, ts_id, ecn_launch_id"

        sql_launch_entry_all_ts = "select date, time_stamp, ecn_launch_id, sum(filled) as filled, sum(total) as total"
        sql_launch_entry_all_ts << " from #{tbl_launch_entry_ecns} " 
        sql_launch_entry_all_ts << " where #{filter_conditions} "
        sql_launch_entry_all_ts << " group by date, time_stamp, ecn_launch_id"
        sql_launch_entry_all_ts << " order by date, time_stamp, ecn_launch_id"

        sql_launch_entry_all_ecn = "select date, time_stamp, ts_id, sum(filled) as filled, sum(total) as total"
        sql_launch_entry_all_ecn << " from #{tbl_launch_entry_ecns} " 
        sql_launch_entry_all_ecn << " where #{filter_conditions} "
        sql_launch_entry_all_ecn << " group by date, time_stamp, ts_id"
        sql_launch_entry_all_ecn << " order by date, time_stamp, ts_id"

        sql_launch_entry_all_ecn_all_ts = "select date, time_stamp, sum(filled) as filled, sum(total) as total"
        sql_launch_entry_all_ecn_all_ts << " from #{tbl_launch_entry_ecns} " 
        sql_launch_entry_all_ecn_all_ts << " where #{filter_conditions} "
        sql_launch_entry_all_ecn_all_ts << " group by date, time_stamp "
        sql_launch_entry_all_ecn_all_ts << " order by date, time_stamp "

        # Launch Exit
        sql_launch_exit = "select date, time_stamp, ts_id, ecn_launch_id, sum(filled) as filled, sum(total) as total"
        sql_launch_exit << " from #{tbl_launch_exit_ecns} " 
        sql_launch_exit << " where #{filter_conditions} "
        sql_launch_exit << " group by date, time_stamp, ts_id, ecn_launch_id"
        sql_launch_exit << " order by date, time_stamp, ts_id, ecn_launch_id"

        sql_launch_exit_all_ts = "select date, time_stamp, ecn_launch_id, sum(filled) as filled, sum(total) as total"
        sql_launch_exit_all_ts << " from #{tbl_launch_exit_ecns} " 
        sql_launch_exit_all_ts << " where #{filter_conditions} "
        sql_launch_exit_all_ts << " group by date, time_stamp, ecn_launch_id"
        sql_launch_exit_all_ts << " order by date, time_stamp, ecn_launch_id"

        sql_launch_exit_all_ecn = "select date, time_stamp, ts_id, sum(filled) as filled, sum(total) as total"
        sql_launch_exit_all_ecn << " from #{tbl_launch_exit_ecns} " 
        sql_launch_exit_all_ecn << " where #{filter_conditions} "
        sql_launch_exit_all_ecn << " group by date, time_stamp, ts_id"
        sql_launch_exit_all_ecn << " order by date, time_stamp, ts_id"

        sql_launch_exit_all_ecn_all_ts = "select date, time_stamp, sum(filled) as filled, sum(total) as total"
        sql_launch_exit_all_ecn_all_ts << " from #{tbl_launch_exit_ecns} " 
        sql_launch_exit_all_ecn_all_ts << " where #{filter_conditions} "
        sql_launch_exit_all_ecn_all_ts << " group by date, time_stamp "
        sql_launch_exit_all_ecn_all_ts << " order by date, time_stamp "

      elsif step == 91 
        sql_launch_entry = "select date,  ts_id, ecn_launch_id, sum(filled) as filled, sum(total) as total"
        sql_launch_entry << " from #{tbl_launch_entry_ecns} " 
        sql_launch_entry << " where #{filter_conditions} "
        sql_launch_entry << " group by date,  ts_id, ecn_launch_id"
        sql_launch_entry << " order by date,  ts_id, ecn_launch_id"

        sql_launch_entry_all_ts = "select date,  ecn_launch_id, sum(filled) as filled, sum(total) as total"
        sql_launch_entry_all_ts << " from #{tbl_launch_entry_ecns} " 
        sql_launch_entry_all_ts << " where #{filter_conditions} "
        sql_launch_entry_all_ts << " group by date,  ecn_launch_id"
        sql_launch_entry_all_ts << " order by date,  ecn_launch_id"

        sql_launch_entry_all_ecn = "select date,  ts_id, sum(filled) as filled, sum(total) as total"
        sql_launch_entry_all_ecn << " from #{tbl_launch_entry_ecns} " 
        sql_launch_entry_all_ecn << " where #{filter_conditions} "
        sql_launch_entry_all_ecn << " group by date,  ts_id"
        sql_launch_entry_all_ecn << " order by date,  ts_id"

        sql_launch_entry_all_ecn_all_ts = "select date,  sum(filled) as filled, sum(total) as total"
        sql_launch_entry_all_ecn_all_ts << " from #{tbl_launch_entry_ecns} " 
        sql_launch_entry_all_ecn_all_ts << " where #{filter_conditions} "
        sql_launch_entry_all_ecn_all_ts << " group by date "
        sql_launch_entry_all_ecn_all_ts << " order by date "

        #launch exit
        sql_launch_exit = "select date,  ts_id, ecn_launch_id, sum(filled) as filled, sum(total) as total"
        sql_launch_exit << " from #{tbl_launch_exit_ecns} " 
        sql_launch_exit << " where #{filter_conditions} "
        sql_launch_exit << " group by date,  ts_id, ecn_launch_id"
        sql_launch_exit << " order by date,  ts_id, ecn_launch_id"

        sql_launch_exit_all_ts = "select date,  ecn_launch_id, sum(filled) as filled, sum(total) as total"
        sql_launch_exit_all_ts << " from #{tbl_launch_exit_ecns} " 
        sql_launch_exit_all_ts << " where #{filter_conditions} "
        sql_launch_exit_all_ts << " group by date,  ecn_launch_id"
        sql_launch_exit_all_ts << " order by date,  ecn_launch_id"

        sql_launch_exit_all_ecn = "select date,  ts_id, sum(filled) as filled, sum(total) as total"
        sql_launch_exit_all_ecn << " from #{tbl_launch_exit_ecns} " 
        sql_launch_exit_all_ecn << " where #{filter_conditions} "
        sql_launch_exit_all_ecn << " group by date,  ts_id"
        sql_launch_exit_all_ecn << " order by date,  ts_id"

        sql_launch_exit_all_ecn_all_ts = "select date,  sum(filled) as filled, sum(total) as total"
        sql_launch_exit_all_ecn_all_ts << " from #{tbl_launch_exit_ecns} " 
        sql_launch_exit_all_ecn_all_ts << " where #{filter_conditions} "
        sql_launch_exit_all_ecn_all_ts << " group by date "
        sql_launch_exit_all_ecn_all_ts << " order by date "

      elsif step == 92 # 1 week
        sql_launch_entry = "select EXTRACT(WEEK FROM date) as week_num,  ts_id, ecn_launch_id, sum(filled) as filled, sum(total) as total"
        sql_launch_entry << " from #{tbl_launch_entry_ecns} " 
        sql_launch_entry << " where #{filter_conditions} "
        sql_launch_entry << " group by EXTRACT(WEEK FROM date),  ts_id, ecn_launch_id"
        sql_launch_entry << " order by EXTRACT(WEEK FROM date),  ts_id, ecn_launch_id"

        sql_launch_entry_all_ts = "select EXTRACT(WEEK FROM date) as week_num,  ecn_launch_id, sum(filled) as filled, sum(total) as total"
        sql_launch_entry_all_ts << " from #{tbl_launch_entry_ecns} " 
        sql_launch_entry_all_ts << " where #{filter_conditions} "
        sql_launch_entry_all_ts << " group by EXTRACT(WEEK FROM date),  ecn_launch_id"
        sql_launch_entry_all_ts << " order by EXTRACT(WEEK FROM date),  ecn_launch_id"

        sql_launch_entry_all_ecn = "select EXTRACT(WEEK FROM date) as week_num,  ts_id, sum(filled) as filled, sum(total) as total"
        sql_launch_entry_all_ecn << " from #{tbl_launch_entry_ecns} " 
        sql_launch_entry_all_ecn << " where #{filter_conditions} "
        sql_launch_entry_all_ecn << " group by EXTRACT(WEEK FROM date),  ts_id"
        sql_launch_entry_all_ecn << " order by EXTRACT(WEEK FROM date),  ts_id"

        sql_launch_entry_all_ecn_all_ts = "select EXTRACT(WEEK FROM date) as week_num,  sum(filled) as filled, sum(total) as total"
        sql_launch_entry_all_ecn_all_ts << " from #{tbl_launch_entry_ecns} " 
        sql_launch_entry_all_ecn_all_ts << " where #{filter_conditions} "
        sql_launch_entry_all_ecn_all_ts << " group by EXTRACT(WEEK FROM date) "
        sql_launch_entry_all_ecn_all_ts << " order by EXTRACT(WEEK FROM date) "

        #Launch exit
        sql_launch_exit = "select EXTRACT(WEEK FROM date) as week_num,  ts_id, ecn_launch_id, sum(filled) as filled, sum(total) as total"
        sql_launch_exit << " from #{tbl_launch_exit_ecns} " 
        sql_launch_exit << " where #{filter_conditions} "
        sql_launch_exit << " group by EXTRACT(WEEK FROM date),  ts_id, ecn_launch_id"
        sql_launch_exit << " order by EXTRACT(WEEK FROM date),  ts_id, ecn_launch_id"

        sql_launch_exit_all_ts = "select EXTRACT(WEEK FROM date) as week_num,  ecn_launch_id, sum(filled) as filled, sum(total) as total"
        sql_launch_exit_all_ts << " from #{tbl_launch_exit_ecns} " 
        sql_launch_exit_all_ts << " where #{filter_conditions} "
        sql_launch_exit_all_ts << " group by EXTRACT(WEEK FROM date),  ecn_launch_id"
        sql_launch_exit_all_ts << " order by EXTRACT(WEEK FROM date),  ecn_launch_id"

        sql_launch_exit_all_ecn = "select EXTRACT(WEEK FROM date) as week_num,  ts_id, sum(filled) as filled, sum(total) as total"
        sql_launch_exit_all_ecn << " from #{tbl_launch_exit_ecns} " 
        sql_launch_exit_all_ecn << " where #{filter_conditions} "
        sql_launch_exit_all_ecn << " group by EXTRACT(WEEK FROM date),  ts_id"
        sql_launch_exit_all_ecn << " order by EXTRACT(WEEK FROM date),  ts_id"

        sql_launch_exit_all_ecn_all_ts = "select EXTRACT(WEEK FROM date) as week_num,  sum(filled) as filled, sum(total) as total"
        sql_launch_exit_all_ecn_all_ts << " from #{tbl_launch_exit_ecns} " 
        sql_launch_exit_all_ecn_all_ts << " where #{filter_conditions} "
        sql_launch_exit_all_ecn_all_ts << " group by EXTRACT(WEEK FROM date) "
        sql_launch_exit_all_ecn_all_ts << " order by EXTRACT(WEEK FROM date) "

      elsif step == 93 # 1 month
        sql_launch_entry = "select EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date) as month_num,  ts_id, ecn_launch_id, sum(filled) as filled, sum(total) as total"
        sql_launch_entry << " from #{tbl_launch_entry_ecns} " 
        sql_launch_entry << " where #{filter_conditions} "
        sql_launch_entry << " group by EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date),  ts_id, ecn_launch_id"
        sql_launch_entry << " order by EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date),  ts_id, ecn_launch_id"

        sql_launch_entry_all_ts = "select EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date) as month_num,  ecn_launch_id, sum(filled) as filled, sum(total) as total"
        sql_launch_entry_all_ts << " from #{tbl_launch_entry_ecns} " 
        sql_launch_entry_all_ts << " where #{filter_conditions} "
        sql_launch_entry_all_ts << " group by EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date),  ecn_launch_id"
        sql_launch_entry_all_ts << " order by EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date),  ecn_launch_id"

        sql_launch_entry_all_ecn = "select EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date) as month_num,  ts_id, sum(filled) as filled, sum(total) as total"
        sql_launch_entry_all_ecn << " from #{tbl_launch_entry_ecns} " 
        sql_launch_entry_all_ecn << " where #{filter_conditions} "
        sql_launch_entry_all_ecn << " group by EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date),  ts_id"
        sql_launch_entry_all_ecn << " order by EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date),  ts_id"

        sql_launch_entry_all_ecn_all_ts = "select EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date) as month_num,  sum(filled) as filled, sum(total) as total"
        sql_launch_entry_all_ecn_all_ts << " from #{tbl_launch_entry_ecns} " 
        sql_launch_entry_all_ecn_all_ts << " where #{filter_conditions} "
        sql_launch_entry_all_ecn_all_ts << " group by EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date) "
        sql_launch_entry_all_ecn_all_ts << " order by EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date) "

        #Launch exit
        sql_launch_exit = "select EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date) as month_num,  ts_id, ecn_launch_id, sum(filled) as filled, sum(total) as total"
        sql_launch_exit << " from #{tbl_launch_exit_ecns} " 
        sql_launch_exit << " where #{filter_conditions} "
        sql_launch_exit << " group by EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date),  ts_id, ecn_launch_id"
        sql_launch_exit << " order by EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date),  ts_id, ecn_launch_id"

        sql_launch_exit_all_ts = "select EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date) as month_num,  ecn_launch_id, sum(filled) as filled, sum(total) as total"
        sql_launch_exit_all_ts << " from #{tbl_launch_exit_ecns} " 
        sql_launch_exit_all_ts << " where #{filter_conditions} "
        sql_launch_exit_all_ts << " group by EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date),  ecn_launch_id"
        sql_launch_exit_all_ts << " order by EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date),  ecn_launch_id"

        sql_launch_exit_all_ecn = "select EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date) as month_num,  ts_id, sum(filled) as filled, sum(total) as total"
        sql_launch_exit_all_ecn << " from #{tbl_launch_exit_ecns} " 
        sql_launch_exit_all_ecn << " where #{filter_conditions} "
        sql_launch_exit_all_ecn << " group by EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date),  ts_id"
        sql_launch_exit_all_ecn << " order by EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date),  ts_id"

        sql_launch_exit_all_ecn_all_ts = "select EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date) as month_num,  sum(filled) as filled, sum(total) as total"
        sql_launch_exit_all_ecn_all_ts << " from #{tbl_launch_exit_ecns} " 
        sql_launch_exit_all_ecn_all_ts << " where #{filter_conditions}"
        sql_launch_exit_all_ecn_all_ts << " group by EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date) "
        sql_launch_exit_all_ecn_all_ts << " order by EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date) "

      end

      #Excute sql query------------------------------
      launch_entry = EntryExit.find_by_sql(sql_launch_entry)
      launch_entry_all_ts = EntryExit.find_by_sql(sql_launch_entry_all_ts)
      launch_entry_all_ecn = EntryExit.find_by_sql(sql_launch_entry_all_ecn)
      launch_entry_all_ecn_all_ts = EntryExit.find_by_sql(sql_launch_entry_all_ecn_all_ts)

      launch_entry.each do |row|
        if row.ts_id.to_i < num_of_ts
          ecn = ecn_launch_mapping[row.ecn_launch_id.to_i]
          summary_fill_rate["launch_entry"]["#{ecn}_ts#{row.ts_id.to_i + 1}"] << row
        end
      end

      launch_entry_all_ts.each do |row|
        ecn = ecn_launch_mapping[row.ecn_launch_id.to_i]
        summary_fill_rate["launch_entry"]["#{ecn}_all_ts"] << row
      end

      launch_entry_all_ecn.each do |row|
        if row.ts_id.to_i < num_of_ts
          summary_fill_rate["launch_entry"]["all_ecn_ts#{row.ts_id.to_i + 1}"] << row
        end
      end

      launch_entry_all_ecn_all_ts.each do |row|
        summary_fill_rate["launch_entry"]["all_ecn_all_ts"] << row
      end

      #Launch exit
      launch_exit = EntryExit.find_by_sql(sql_launch_exit)
      launch_exit_all_ts = EntryExit.find_by_sql(sql_launch_exit_all_ts)
      launch_exit_all_ecn = EntryExit.find_by_sql(sql_launch_exit_all_ecn)
      launch_exit_all_ecn_all_ts = EntryExit.find_by_sql(sql_launch_exit_all_ecn_all_ts)

      launch_exit.each do |row|
        if row.ts_id.to_i < num_of_ts
          ecn = ecn_launch_mapping[row.ecn_launch_id.to_i]
          summary_fill_rate["launch_exit"]["#{ecn}_ts#{row.ts_id.to_i + 1}"] << row
        end
      end

      launch_exit_all_ts.each do |row|
        ecn = ecn_launch_mapping[row.ecn_launch_id.to_i]
        summary_fill_rate["launch_exit"]["#{ecn}_all_ts"] << row
      end

      launch_exit_all_ecn.each do |row|
        if row.ts_id.to_i < num_of_ts
          summary_fill_rate["launch_exit"]["all_ecn_ts#{row.ts_id.to_i + 1}"] << row
        end
      end

      launch_exit_all_ecn_all_ts.each do |row|
          summary_fill_rate["launch_exit"]["all_ecn_all_ts"] << row
      end

      if en - st > 0
        date_en = Time.mktime(date_st.year, date_st.month+1, date_st.day)
      else
        date_en = end_date
      end

      if date_st.month == 12
        date_st = Time.mktime(date_st.year + 1, 1, 1, 6) 
      else
        date_st = Time.mktime(date_st.year, date_st.month + 1, 1, 6) 
      end
    end

    return summary_fill_rate
  end

   
  #================================================================================================
  # Function    : get_fill_rate_by_shares
  # Input        : Date, time, ts, Launch, Iso
  # Description  : Get data for Chart Fill Rate
  # Developer      : Hungln
  # Date      : 06/06/2014
  #================================================================================================   
  def self.get_fill_rate_by_shares(start_time, end_time, start_date, end_date, ecn_fil_id, step_type, iso_order)
    #Set table name based on step
    step = step_type.to_i
    step_suffix = ""
    # Set suffix of table 
    if step == 5
      step_suffix = ""
    elsif (step == 15 || step == 30 || step == 60)
      step_suffix = "_"+  step.to_s
    else
      step_suffix = "_60"
    end
    #Set table name
    table_exit = "summary_exit_order_shares" + step_suffix
    start_month = start_date.month
    end_month = end_date.month
    year = start_date.year
    date_st = start_date
    date_en = end_date
    if start_date.month < end_date.month
      if start_date.month == 2 && ((start_date.year % 4 == 0 && start_date.year % 100 !=0)|| start_date.year % 400 == 0)
        date_en = Time.mktime(start_date.year, start_date.month,days_in_month(start_date.month) + 1,21) 
      else
        date_en = Time.mktime(start_date.year, start_date.month,days_in_month(start_date.month),21) 
      end
    end

    #Declare variables --------------------------------------------------
    ts_list_temp = ts_list
    # Check number of TS
    num_of_ts = ts_list_temp.size
    num_of_ts = num_of_ts -1
    launch_list_temp = launch_type_list

    summary_fill_rate = {
      "entry_exit"  => {},
      "launch_entry"  => {},
      "launch_exit"  => {}
    }

    ecn_launch_mapping = []
    launch_list_temp.each do |ecn|
      ecn_name = ""
      if ecn[1].to_i == 0 then
        ecn_name = "all_ecn"
      else
        ecn_name = ecn[0].downcase
        ecn_launch_mapping << ecn_name
      end
      
      ts_list_temp.each do |ts|
        if ts[1].to_i == 0 then
          summary_fill_rate["launch_entry"]["#{ecn_name}_all_ts"] = []
          summary_fill_rate["launch_exit"]["#{ecn_name}_all_ts"] = []
        else
          summary_fill_rate["launch_entry"]["#{ecn_name}_ts#{ts[1].to_i}"] = []
          summary_fill_rate["launch_exit"]["#{ecn_name}_ts#{ts[1].to_i}"] = []
        end
      end
    end

    check_year = 0
    if end_date.year > start_date.year
      check_year = end_date.year - start_date.year
      end_month = end_month + 12 * check_year
    end
    while start_month <= end_month 
      db_name_suffix = "#{Time.now.year}#{Time.now.month.to_s.rjust(2,'0')}"

      if start_month <= 12
        db_name_suffix = "#{year}#{start_month.to_s.rjust(2,'0')}"
      else
        if check_year == 1
          db_name_suffix = "#{year + 1}#{(start_month - 12).to_s.rjust(2,'0')}"
        elsif check_year == 2
          if start_month <= 24
            db_name_suffix = "#{year + 1}#{(start_month - 12).to_s.rjust(2,'0')}"
          else
            db_name_suffix = "#{year + 2}#{(start_month - 24).to_s.rjust(2,'0')}"
          end
        end
      end

      # lets manually connect to the proper db
      get_database_connection(db_name_suffix, "twt")
      start_month = start_month + 1
    
      filter_conditions_exit = " date >= '#{start_date}' and date <= '#{end_date}' and time_stamp > '#{start_time}' and time_stamp <= '#{end_time}' and entry_exit = 0"
      filter_conditions_entry = " date >= '#{start_date}' and date <= '#{end_date}' and time_stamp > '#{start_time}' and time_stamp <= '#{end_time}' and entry_exit = 1"
      if (ecn_fil_id.to_i != 99)
        filter_conditions_exit << " and ecn_id = #{ecn_fil_id}"
        filter_conditions_entry << " and ecn_id = #{ecn_fil_id}"
      end
      conditions_iso = ""
      # Filter by Iso
      if iso_order == 1 # Filter by Iso
        conditions_iso = " and is_iso = 1"
      end
      if iso_order == 2 # Filter by Non_Iso
        conditions_iso = " and is_iso = 0"
      end
      filter_conditions_exit << conditions_iso
      filter_conditions_entry << conditions_iso
      
      if step == 5 || step == 15 || step == 30 || step == 60
        select_sql_launch  = "select date, time_stamp, ts_id, ecn_launch, sum(shares_filled) as shares_filled, sum(order_size) as order_size"
        select_sql_launch_all_ts  = "select date, time_stamp, ecn_launch, sum(shares_filled) as shares_filled, sum(order_size) as order_size"
        select_sql_launch_all_ecn  = "select date, time_stamp, ts_id, sum(shares_filled) as shares_filled, sum(order_size) as order_size"
        select_sql_launch_all_ecn_all_ts = "select date, time_stamp, sum(shares_filled) as shares_filled, sum(order_size) as order_size"
        group_sql_launch  = "date, time_stamp, ts_id, ecn_launch"
        group_sql_launch_all_ts  = "date, time_stamp, ecn_launch"
        group_sql_launch_all_ecn  = "date, time_stamp, ts_id"
        group_sql_launch_all_ecn_all_ts  ="date, time_stamp "
        order_by_sql_launch  = "date, time_stamp, ts_id, ecn_launch"
        order_by_sql_launch_all_ts  = "date, time_stamp, ecn_launch"
        order_by_sql_launch_all_ecn  = "date, time_stamp, ts_id"
        order_by_sql_launch_all_ecn_all_ts  = "date, time_stamp "
      elsif step == 91 # 1 day
        select_sql_launch  = "select date, ts_id, ecn_launch, sum(shares_filled) as shares_filled, sum(order_size) as order_size"
        select_sql_launch_all_ts  =  "select date, ecn_launch, sum(shares_filled) as shares_filled, sum(order_size) as order_size"
        select_sql_launch_all_ecn  = "select date, ts_id, sum(shares_filled) as shares_filled, sum(order_size) as order_size"
        select_sql_launch_all_ecn_all_ts  =  "select date,  sum(shares_filled) as shares_filled, sum(order_size) as order_size"
        group_sql_launch  = " date, ts_id, ecn_launch"
        group_sql_launch_all_ts  = " date, ecn_launch"
        group_sql_launch_all_ecn  = " date, ts_id"
        group_sql_launch_all_ecn_all_ts  =" date "
        order_by_sql_launch  = "  date, ts_id, ecn_launch"
        order_by_sql_launch_all_ts  = "  date, ecn_launch"
        order_by_sql_launch_all_ecn  = "  date, ts_id"
        order_by_sql_launch_all_ecn_all_ts  = "  date "
      elsif step == 92 # 1 week
        select_sql_launch  = "select EXTRACT(WEEK FROM date) as week_num, ts_id, ecn_launch, sum(shares_filled) as shares_filled, sum(order_size) as order_size"
        select_sql_launch_all_ts  =  "select EXTRACT(WEEK FROM date) as week_num, ecn_launch, sum(shares_filled) as shares_filled, sum(order_size) as order_size"
        select_sql_launch_all_ecn  = "select EXTRACT(WEEK FROM date) as week_num, ts_id, sum(shares_filled) as shares_filled, sum(order_size) as order_size"
        select_sql_launch_all_ecn_all_ts  =  "select EXTRACT(WEEK FROM date) as week_num,  sum(shares_filled) as shares_filled, sum(order_size) as order_size"
        group_sql_launch  = "EXTRACT(WEEK FROM date), ts_id, ecn_launch"
        group_sql_launch_all_ts  = "EXTRACT(WEEK FROM date), ecn_launch"
        group_sql_launch_all_ecn  = "EXTRACT(WEEK FROM date), ts_id"
        group_sql_launch_all_ecn_all_ts  =" EXTRACT(WEEK FROM date) "
        order_by_sql_launch  = "EXTRACT(WEEK FROM date), ts_id, ecn_launch"
        order_by_sql_launch_all_ts  = "EXTRACT(WEEK FROM date), ecn_launch"
        order_by_sql_launch_all_ecn  = "EXTRACT(WEEK FROM date), ts_id"
        order_by_sql_launch_all_ecn_all_ts  = "EXTRACT(WEEK FROM date) "
      elsif step == 93 # 1 month
        select_sql_launch  = "select EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date) as month_num,  ts_id, ecn_launch, sum(shares_filled) as shares_filled, sum(order_size) as order_size"
        select_sql_launch_all_ts  =  "select EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date) as month_num, ecn_launch, sum(shares_filled) as shares_filled, sum(order_size) as order_size"
        select_sql_launch_all_ecn  = "select EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date) as month_num, ts_id, sum(shares_filled) as shares_filled, sum(order_size) as order_size"
        select_sql_launch_all_ecn_all_ts  =  "select EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date) as month_num,  sum(shares_filled) as shares_filled, sum(order_size) as order_size"
        group_sql_launch  = "EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date), ts_id, ecn_launch"
        group_sql_launch_all_ts  = "EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date), ecn_launch"
        group_sql_launch_all_ecn  = "EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date), ts_id"
        group_sql_launch_all_ecn_all_ts  =" EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date) "
        order_by_sql_launch  = "EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date), ts_id, ecn_launch"
        order_by_sql_launch_all_ts  = "EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date), ecn_launch"
        order_by_sql_launch_all_ecn  = "EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date), ts_id"
        order_by_sql_launch_all_ecn_all_ts  = "EXTRACT(YEAR FROM date), EXTRACT(MONTH FROM date) "
      end
      
      # SQL query------------------------------
      sql_launch_entry = "#{select_sql_launch}"
      sql_launch_entry << " from #{table_exit} " 
      sql_launch_entry << " where #{filter_conditions_entry}"
      sql_launch_entry << " group by #{group_sql_launch}"
      sql_launch_entry << " order by #{order_by_sql_launch}"
      
      sql_launch_entry_all_ts = "#{select_sql_launch_all_ts}"
      sql_launch_entry_all_ts << " from #{table_exit} " 
      sql_launch_entry_all_ts << " where #{filter_conditions_entry}"
      sql_launch_entry_all_ts << " group by #{group_sql_launch_all_ts}"
      sql_launch_entry_all_ts << " order by #{order_by_sql_launch_all_ts}"

      sql_launch_entry_all_ecn = "#{select_sql_launch_all_ecn}"
      sql_launch_entry_all_ecn << " from #{table_exit}" 
      sql_launch_entry_all_ecn << " where #{filter_conditions_entry}"
      sql_launch_entry_all_ecn << " group by #{group_sql_launch_all_ecn}"
      sql_launch_entry_all_ecn << " order by #{order_by_sql_launch_all_ecn}"

      sql_launch_entry_all_ecn_all_ts = "#{select_sql_launch_all_ecn_all_ts}"
      sql_launch_entry_all_ecn_all_ts << " from #{table_exit} " 
      sql_launch_entry_all_ecn_all_ts << " where #{filter_conditions_entry}"
      sql_launch_entry_all_ecn_all_ts << " group by #{group_sql_launch_all_ecn_all_ts}"
      sql_launch_entry_all_ecn_all_ts << " order by #{order_by_sql_launch_all_ecn_all_ts}"
      
      # Launch Exit
      sql_launch_exit = "#{select_sql_launch}"
      sql_launch_exit << " from #{table_exit}" 
      sql_launch_exit << " where #{filter_conditions_exit}"
      sql_launch_exit << " group by #{group_sql_launch}"
      sql_launch_exit << " order by #{order_by_sql_launch}"
      
      sql_launch_exit_all_ts = "#{select_sql_launch_all_ts}"
      sql_launch_exit_all_ts << " from #{table_exit} " 
      sql_launch_exit_all_ts << " where #{filter_conditions_exit}"
      sql_launch_exit_all_ts << " group by #{group_sql_launch_all_ts}"
      sql_launch_exit_all_ts << " order by #{order_by_sql_launch_all_ts}"

      sql_launch_exit_all_ecn = "#{select_sql_launch_all_ecn}"
      sql_launch_exit_all_ecn << " from #{table_exit} " 
      sql_launch_exit_all_ecn << " where #{filter_conditions_exit}"
      sql_launch_exit_all_ecn << " group by #{group_sql_launch_all_ecn}"
      sql_launch_exit_all_ecn << " order by #{order_by_sql_launch_all_ecn}"

      sql_launch_exit_all_ecn_all_ts = "#{select_sql_launch_all_ecn_all_ts}"
      sql_launch_exit_all_ecn_all_ts << " from #{table_exit} " 
      sql_launch_exit_all_ecn_all_ts << " where #{filter_conditions_exit}  "
      sql_launch_exit_all_ecn_all_ts << " group by #{group_sql_launch_all_ecn_all_ts}"
      sql_launch_exit_all_ecn_all_ts << " order by #{order_by_sql_launch_all_ecn_all_ts}"

      #Excute sql query------------------------------
      launch_entry = EntryExit.find_by_sql(sql_launch_entry)
      launch_entry_all_ts = EntryExit.find_by_sql(sql_launch_entry_all_ts)
      launch_entry_all_ecn = EntryExit.find_by_sql(sql_launch_entry_all_ecn)
      launch_entry_all_ecn_all_ts = EntryExit.find_by_sql(sql_launch_entry_all_ecn_all_ts)

      launch_entry.each do |row|
        if row.ts_id.to_i < num_of_ts
          ecn = ecn_launch_mapping[row.ecn_launch.to_i]
          summary_fill_rate["launch_entry"]["#{ecn}_ts#{row.ts_id.to_i + 1}"] << row
        end
      end

      launch_entry_all_ts.each do |row|
        ecn = ecn_launch_mapping[row.ecn_launch.to_i]
        summary_fill_rate["launch_entry"]["#{ecn}_all_ts"] << row
      end

      launch_entry_all_ecn.each do |row|
        if row.ts_id.to_i < num_of_ts
          summary_fill_rate["launch_entry"]["all_ecn_ts#{row.ts_id.to_i + 1}"] << row
        end
      end

      launch_entry_all_ecn_all_ts.each do |row|
        summary_fill_rate["launch_entry"]["all_ecn_all_ts"] << row
      end

      #Launch exit
      launch_exit = EntryExit.find_by_sql(sql_launch_exit)
      launch_exit_all_ts = EntryExit.find_by_sql(sql_launch_exit_all_ts)
      launch_exit_all_ecn = EntryExit.find_by_sql(sql_launch_exit_all_ecn)
      launch_exit_all_ecn_all_ts = EntryExit.find_by_sql(sql_launch_exit_all_ecn_all_ts)

      launch_exit.each do |row|
        if row.ts_id.to_i < num_of_ts
          ecn = ecn_launch_mapping[row.ecn_launch.to_i]
          summary_fill_rate["launch_exit"]["#{ecn}_ts#{row.ts_id.to_i + 1}"] << row
        end
      end

      launch_exit_all_ts.each do |row|
        ecn = ecn_launch_mapping[row.ecn_launch.to_i]
        summary_fill_rate["launch_exit"]["#{ecn}_all_ts"] << row
      end

      launch_exit_all_ecn.each do |row|
        if row.ts_id.to_i < num_of_ts
          summary_fill_rate["launch_exit"]["all_ecn_ts#{row.ts_id.to_i + 1}"] << row
        end
      end

      launch_exit_all_ecn_all_ts.each do |row|
        summary_fill_rate["launch_exit"]["all_ecn_all_ts"] << row
      end

      if end_month - start_month > 0
        date_en = Time.mktime(date_st.year, date_st.month+1, date_st.day)
      else
        date_en = end_date
      end

      if date_st.month == 12
        date_st = Time.mktime(date_st.year + 1, 1, 1, 6) 
      else
        date_st = Time.mktime(date_st.year, date_st.month + 1, 1, 6) 
      end
    end

    return summary_fill_rate
  end
  


end