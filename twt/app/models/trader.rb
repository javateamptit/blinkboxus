class Trader < ActiveRecord::Base
  set_table_name :user_login_info
  #****************************************************
  # created date: 2012-09-06
  # developer: khanhtt
  # output : return list of active/ass active/monitoring/ logged out traders
  #****************************************************
  def self.show_trader_indicator
    # get current date to query 
    currentDate = Time.now
    currentDate = currentDate.strftime("%Y-%m-%d").to_s
    
    # query database to get actived traders on current date
    get_actvied_trader = "(select distinct username from tt_event_log where date ='#{currentDate}' and username not like ' ')"
    #conditions = [" username in (select distinct username from tt_event_log where date = '#{currentDate}' and username not like ' ')"]
    
    traderDisplay = Trader.find_by_sql("select * from user_login_info where username in #{get_actvied_trader}")
    # traderDisplay = Trader.find(:all,
                  # :select => "*",
                  # :conditions => [conditions]
                  # )
    traderDisplay.each { |traderIndex|
      if traderIndex.status == 0
        traderIndex.status = 4
      end
    }
    traderDisplay = traderDisplay.sort!{ |a,b| a.status <=> b.status}
    return traderDisplay
                  
    
  end
  
  def show_list_trader
    
  end
end
