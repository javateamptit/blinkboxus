#================================================================================================
# Project    : EAA 
# File      : user_context.rb
# Date created  : 27/09/2007
# Description  : User Context model
# Developer     : hungpk 
# Note      : 
#================================================================================================

class UserContext
    attr_reader :start_time, 
                :end_time, 
                :ts, 
                :ecn, 
                :cecn,
                :img_view_type,
                :img_view_step,
        :status, 
        :stock,
        :tif,
        :trade_date,
        :trade_start_time,
        :trade_end_time,
        :price_from,
        :price_to,
        :symbol,
        :trade_ecn,
        :share_size,
        :cross_size,
        :expected_profit,
        :step,
                :trade_status,
        :cross_status,
        :executing_ts,
        :ecn_filter,
        :ts_filter,
        :trade_type,
        :step_type,
        :ecn_entry,
        :entry_exit,
        :standing_filter,
        :standing_value,
        :side,
        :pm_pl,
        :trade_result,
        :last_date_trading,
        :count_missed,
        :count_repriced,
        :count_stuck_loss,
        :count_stuck_profit,
        :count_loss,
        :count_profit_under,
        :count_profit_above,
        :count_total,
        :iso_order,
        :bought_shares,
                :sold_shares,
                :launch_shares

    attr_writer :ts, 
                :ecn, 
                :cecn,
                :img_view_type,
                :img_view_step,
        :status,
        :stock,
        :tif,
        :trade_start_time,
        :trade_end_time,
        :price_from,
        :price_to,
        :symbol,
        :trade_ecn,
        :share_size,
        :cross_size,
        :expected_profit,
        :step,
                :trade_status,
        :cross_status,
        :executing_ts,
        :ecn_filter,
        :ts_filter,
        :trade_type,
        :step_type,
        :ecn_entry,
        :entry_exit,
        :standing_filter,
        :standing_value,
        :side,
        :pm_pl,
        :trade_result,
        :last_date_trading,
        :count_missed,
        :count_repriced,
        :count_stuck_loss,
        :count_stuck_profit,
        :count_loss,
        :count_profit_under,
        :count_profit_above,
        :count_total,
        :iso_order,
        :bought_shares,
                :sold_shares,
                :launch_shares


   def start_time=(td)
    @start_time  = td
    @trade_date = td

   end
 
   def end_time=(td)
    @end_time = td

  end

  def trade_date=(td)
    @start_time = td
    @end_time = td
    @trade_date = td
  end

#================================================================================================
# Function    : initialize (action controller)
# Input        : N/A
# Description  : Initialize User context
# Developer      : Hungpk
# Date      : 09/27/2007
#================================================================================================       
   def initialize
       now = Time.now
   #Trade Date start on 8 A.M so  start time is set to 7 A.M today as default(redunce 1 H). 
       #@start_time = Time.mktime(now.year, 4, 6, 6)
     @start_time = Time.mktime(now.year, now.mon, now.mday, 6)
     
   #End Trade Date is 20 PM. so end time is set to 21 P.M today as default
     #@end_time =  Time.mktime(now.year, 4, 6, 22)
       @end_time =  Time.mktime(now.year, now.mon, now.mday, 22)
          
       @ts = "0"# All active Trade Servers
       @ecn = "0" #all ECNs
       @cecn = "0" #Caused by ECN.
       @img_view_type = "0" #default is relative view
       @img_view_step = 15*60 #default image view step is 5 minutes (300 seconds)
     @status = "0" 
     @stock = "0"
     @tif  = "0"
    @trade_date = @end_time
    @trade_start_time = "6:00"
    @trade_end_time = "19:00"
    @symbol = nil
    @trade_ecn = nil
    @share_size = 0
    @cross_size = 0
    @expected_profit = 0
    @step = 30*60 
    @trade_status = 7 #Trade status for trade statistics, default as ALL
    @cross_status = 0 #Cross status, default as ALL
    @executing_ts = 0
    @ecn_filter = "0" #all ECNs filter
    @ts_filter = "4"
    @trade_type = "0" #filter by trade type: normal, simultanenous, all
    @step_type = "15"
    @ecn_entry = "99"
    @entry_exit = "0" #default is ENTRY
    @side = "3" #all
    @launch_shares = 0

   end

end