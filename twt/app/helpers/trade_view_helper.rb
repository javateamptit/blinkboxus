module TradeViewHelper
  def parse_raw_cross(raw_content)
    _ab=["ASK", "BID"]
    _server_role = ["ARCA Server", "NASDAQ Server", "BATS Server", "EDGX Server" , "Stand Alone"]
    _ecn_book = ["ARCA", "ISLD", "NYSE", "BZX" , "EDGX", "EDGA", "BX", "BYX", "PSX", "AMEX"]
    _ecn_server = ["ARCA", "ISLD", "NYSE", "RASH", "BZX", "EDGX", "EDGA", "BX", "BYX", "PSX"]
    _pl = ["Profit", "Loss"]
    _ls = ["LONG", "SHORT"]
    _maxbid_minask = ["MaxBID", "MinASK"]
    _side = ["B", "Bt", "S", "Sd", "Cd"]
    logs = raw_content.split(" ")


    case logs[0].to_i
      when 2
        ret = "TRADE: Stock symbol #{logs[3]}, #{_server_role[logs[4].to_i]}, #{_ecn_book[logs[5].to_i]} #{_ab[logs[6].to_i]} launch"
      when 3
        logs_ab4 = number_with_delimiter(logs[4])
        ret = "#{_ab[logs[3].to_i]}: #{logs_ab4} #{_ecn_book[logs[5].to_i]} at #{logs[6]}"
      when 4
        ret = "Maximum spread exceeded. Trading for this cross is disabled"
      when 5
        ret = "Symbol could not short sell"
      when 6
        ret = "Fatal error inside #{_ecn_book[logs[3].to_i]}"
      when 7
        ret = "Fatal error! #{_ecn_book[logs[3].to_i]} Partial hashing data structure is full!!!"
      when 8
        ret = "This side of the trade is not yet ready. We cannot trade"
      when 9
        ret = "Other side of the trade is not yet ready. We cannot trade"
      when 10
        logs_side4 = number_with_delimiter( logs[4].to_i )
        ret = "#{_side[logs[3].to_i]} #{logs_side4} #{logs[5]} at #{logs[6]} #{_ecn_server[logs[7].to_i]}"
      when 11
        ret = "Entry Missed"
      when 12
        ret = "#{_ecn_server[logs[3].to_i]}: Missed the trade: Rejected!!!"
      when 13
        #"%s %.02lf %.02lf %d %d"
        logs_4 = number_to_currency(logs[4].to_f,{:unit=>"", :precision=>2, :delimiter => ","})
        logs_5 = number_to_currency(logs[5].to_f,{:unit=>"", :precision=>2, :delimiter => ","})
        logs_6 = number_with_delimiter(logs[6])
        logs_7 = number_with_delimiter(logs[7])
        ret = "Trade completed: Stock symbol: #{logs[3]} TotalBought = #{logs_4} TotalSold = #{logs_5} BoughtShares = #{logs_6} SoldShares = #{logs_7}"
      when 14
        logs_pl4 = number_to_currency(logs[4].to_f, {:unit=>"", :precision=>2, :delimiter =>","})
        ret = "#{_pl[logs[3].to_i]} : $#{logs_pl4} #{logs[5]}"
      when 15
        logs_stuck5 = number_with_delimiter(logs[5])
        ret = "Stuck(#{logs[3]}): #{_ls[logs[4].to_i]} #{logs_stuck5} #{logs[6]}. Best price #{_maxbid_minask[logs[7].to_i]} I saw was #{logs[8]}"
      else
        ret = raw_content
    end

    return ret

  end
end
