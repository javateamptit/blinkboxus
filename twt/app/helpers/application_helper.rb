# Methods added to this helper will be available to all templates in the application.
module ApplicationHelper

  require 'util'
  def get_supported_fields
    file_name = "#{RAILS_ROOT}/SupportedFields.txt"
    f = File.new(file_name, "r")
    @ret = {}
    f.each_line{ |line|
      field = line.strip.split(" ")
      @ret[field[0].to_i] = field[1]
    }
    f.close()
    return @ret
  end

  def get_percent(filled, total)
    if ((filled == 0) or (total == 0)) then
      f_percent = ""
      return f_percent
    end
    f_percent = (filled.to_f/total.to_i)
    if !f_percent.nan?
      f_percent = (f_percent * 100).round(2)
    else 
      f_percent = ""
    end
    return f_percent
  end

  def get_average_time(filled, total)
    #f_percent = (filled.to_f/total.to_i)
    f_percent = (filled.to_f)
    if !f_percent.nan?
      f_percent = f_percent.round(2)
    else 
      f_percent = ""
    end
    return f_percent
  end
  
end
