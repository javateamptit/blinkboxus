module FillRateHelper
  require 'util'
  def CreateEntryExitXMLFile (entry_exit, ecn ,ts_id )
    label_name = ""
    #Id of ECN BZX Smart Route: Except All ECNs
    bzsr_id = ecn_entry_fill_rates.size - 2
    if ecn.to_i == bzsr_id then
      label_name << "BZX Smart Route"
    elsif ecn.to_i == 99 then
      label_name << ""
    else
      label_name << "#{ecn_entry_map[ecn.to_i][0].to_s}"
    end

    ts_list_temp_temp = ts_list.reject {|x| x[1].to_i == 0}

    if entry_exit.to_i == 1 then
      label_name << " Entry"
    else
      label_name << " Exit"
    end

    if ts_id != "0"
      str_label_name = ""
      ts_id.split(",").each do |t|
        item = ts_list_temp_temp.select{|x| x[1].to_s == t.to_s}
        str_label_name += ", " + item.first[0].to_s
      end
      str_label_name = str_label_name[2..(str_label_name.length - 1)]
      label_name << " #{str_label_name}"  
    else
      label_name << " "
    end
     
    File.open("public/amcharts/settings/fill_rate/entry_exit_fill_rate.xml", "w") do |f|
      f.puts "<settings>"
      f.puts "<labels>"
      f.puts "<label lid=\"0\">"
      
      f.puts "<text><![CDATA[<b> #{label_name} Fill Rate by Time</b>]]></text>"     
      f.puts "</label>"
      f.puts "</labels>"
      
      f.puts " <plot_area>"
      f.puts " <margins>"
      f.puts "   <right>28</right>"
      f.puts " </margins>"
      f.puts "</plot_area>"
      
      f.puts "<legend>"
      f.puts " <spacing>0</spacing>"
      f.puts "  <max_columns>10</max_columns>"
      f.puts "</legend>"
    
      f.puts "<values>"
      f.puts " <y_left>"
      f.puts "  <max>100</max>"
      f.puts " </y_left>"
      f.puts "</values>"

      f.puts "<graphs>"
      ts_list_temp_temp.each_with_index do |v, i|
        f.puts "<graph gid=\"#{i}\">"
        f.puts " <title>#{v[0].to_s}</title>"
        f.puts "  <line_width>1</line_width>"
        f.puts " <fill_alpha>0</fill_alpha>"
        f.puts "  <bullet>round</bullet>"
        f.puts "  <bullet_size>3</bullet_size>"
        if ts_id.index(v[1].to_s) then 
          f.puts "  <hidden>0</hidden>"
        else
          f.puts "  <hidden>1</hidden>"
        end 
        f.puts "</graph>"
      end
      f.puts "<graph gid=\"#{ts_list_temp_temp.size}\" >"
      f.puts " <title>All Trade Servers</title>"
      f.puts "  <line_width>1</line_width>"
      f.puts " <fill_alpha>0</fill_alpha>"
      f.puts "  <bullet>round</bullet>"
      f.puts "  <bullet_size>3</bullet_size>"
      if ts_id == "0" then
        f.puts "  <hidden>0</hidden>"
      else
        f.puts "  <hidden>1</hidden>"
      end 
      f.puts "</graph>"
      f.puts "</graphs>"
      f.puts "</settings>"
      f.close
    end
    
  end
  
  def CreateOrderTypeXML ( entry_exit, ecn ,  ts_id , side ) 
    label_name = ""
    order_type_entry_mapping = [ "B" , "SS" ]
    order_type_exit_mapping = [ "B" , "S" ]
    
    # Init ts_list_temp and change index of All Servers
    ts_list_temp = ts_list.reject {|x| x[1].to_i == 0}
    ts_list_temp << ts_list.select {|x| x[1].to_i == 0}.first if ts_list.select {|x| x[1].to_i == 0}.any?

    # Init order type mapping
    order_type_mapping = []
    
    #Id of ECN BZX Smart Route: Except All ECNs
    bzsr_id = ecn_entry_fill_rates.size - 2
    if ecn.to_i == bzsr_id then
      label_name << "BZX Smart Route"
    elsif ecn.to_i == 99 then
      label_name << ""
    else
      label_name << "#{ecn_entry_map[ecn.to_i][0].to_s}"
    end
    if entry_exit.to_i == 1 then
      label_name << " Entry"
      order_type_mapping = order_type_entry_mapping
    else
      label_name << " Exit"
      order_type_mapping = order_type_exit_mapping
    end
    label_name << " Order Type"

    if ts_id != "0"
      str_label_name = ""
      ts_id.split(",").each do |t|
        item = ts_list_temp.select{|x| x[1].to_s == t.to_s}
        str_label_name += ", " + item.first[0].to_s
      end
      str_label_name = str_label_name[2..(str_label_name.length - 1)]
      label_name << " #{str_label_name}"  
    else
      label_name << " on All Servers"
    end
    
    File.open("public/amcharts/settings/fill_rate/order_type_fill_rate.xml", "w") do |f|
      f.puts "<settings>"
      f.puts " <labels>"
      f.puts "  <label lid=\"0\">"
      f.puts "    <text><![CDATA[<b> #{label_name} Fill Rate by Time</b>]]></text>"
      f.puts "  </label>"
      f.puts " </labels>"

      f.puts "<plot_area>"
      f.puts " <margins>"
      f.puts "  <right>28</right>"    
      f.puts " </margins>"
      f.puts "</plot_area>"
      
      f.puts "<legend>"
      f.puts " <spacing>0</spacing>"
      f.puts "  <max_columns>10</max_columns>"
      f.puts "</legend>"

      f.puts " <values>"
      f.puts "   <y_left>"
      f.puts "   <max>100</max>"
      f.puts "  </y_left> "
      f.puts " </values>"

      f.puts "<graphs>"
      
      tmp = []

      # Get size of trade servers
      size_of_ts = ts_list_temp.size
      # Get size of order type
      size_of_order_type = order_type_mapping.size

      order_type_input = {
        "B" => 0,
        "S" => 1,
        "SS" => 2,
        "All" => 3
      }
      ts_id.split(",").each do |t|
        order_type_mapping.each do |o|
          if order_type_input.index(side) == "All" || order_type_input.index(side) == o
            tmp << [t.to_s, o.to_s]
          end
        end
      end
          
      # Map trade servers list and order type
      ls_ts_order = []
      order_type_mapping.each do |o|
        ts_list_temp.each do |t|
          ls_ts_order << [t[0].to_s, t[1].to_s, o.to_s] # name, ts_id, order type
        end
      end

      ls_ts_order.each_with_index do |v, i|
        f.puts "<graph gid=\"#{i}\" >"

        order_type_name = v[2].to_s
        
        if (v[1].to_s == "0") then # Check ts_id -> All
          f.puts "  <title>#{order_type_name}, All Servers</title>"
        else
          f.puts " <title>#{order_type_name}, #{v[0].to_s}</title>"
        end
        f.puts "  <line_width>1</line_width>"
        f.puts " <fill_alpha>0</fill_alpha>"
        f.puts "  <bullet>round</bullet>"
        f.puts "  <bullet_size>3</bullet_size>"
        if !tmp.select{|t| t[0].to_i == v[1].to_i && t[1].to_s == v[2].to_s}.empty? then
          f.puts "  <hidden>0</hidden>"
        else
          f.puts "  <hidden>1</hidden>"
        end 
        f.puts "</graph>"
      end
      f.puts "</graphs>"
      f.puts "</settings>"
      f.close
    end
  end
  
  def CreateEntryXMLFile (ts_id)
    label_name = ""
    # Init ts_list_temp
    ts_list_temp_temp = ts_list.reject {|x| x[1].to_i == 0}
    label_name << " Entry"  
    if ts_id != "0"
      str_label_name = ""
      ts_id.split(",").each do |t|
        item = ts_list_temp_temp.select{|x| x[1].to_s == t.to_s}
        str_label_name += ", " + item.first[0].to_s
      end
      str_label_name = str_label_name[2..(str_label_name.length - 1)]
      label_name << " #{str_label_name}"  
    else
      label_name << " "
    end
  
    File.open("public/amcharts/settings/fill_rate/entry_fill_rate_fr.xml", "w") do |f|
      f.puts "<settings>"
      f.puts "<labels>"
      f.puts "<label lid=\"0\">"
      
      f.puts "<text><![CDATA[<b> #{label_name} Fill Rate by Time</b>]]></text>"     
      f.puts "</label>"
      f.puts "</labels>"
      
      f.puts " <plot_area>"
      f.puts "  <border_alpha>1</border_alpha>"
      f.puts " <margins>"
      f.puts "   <right>28</right>"
      f.puts " </margins>"
      f.puts "</plot_area>"
        
      f.puts "<legend>"
      f.puts " <spacing>0</spacing>"
      f.puts "  <max_columns>10</max_columns>"
      f.puts "</legend>"
   
      f.puts "<values>"
      f.puts " <y_left>"
      f.puts "  <max>100</max>"
      f.puts " </y_left>"
      f.puts "</values>"

      f.puts "<graphs>"
      ts_list_temp_temp.each_with_index do |v, i| 
        f.puts "<graph gid=\"#{i}\">"
        f.puts " <title>#{v[0].to_s}</title>"
        f.puts "  <line_width>1</line_width>"
        f.puts " <fill_alpha>0</fill_alpha>"
        f.puts "  <bullet>round</bullet>"
        f.puts "  <bullet_size>3</bullet_size>"
        if ts_id.index(v[1].to_s) then 
          f.puts "  <hidden>0</hidden>"
        else
          f.puts "  <hidden>1</hidden>"
        end 
        f.puts "</graph>"
      end
      f.puts "<graph gid=\"#{ts_list_temp_temp.size}\" >"
      f.puts " <title>All Trade Servers</title>"
      f.puts "  <line_width>1</line_width>"
      f.puts " <fill_alpha>0</fill_alpha>"
      f.puts "  <bullet>round</bullet>"
      f.puts "  <bullet_size>3</bullet_size>"
      if ts_id == "0" then 
        f.puts "  <hidden>0</hidden>"
      else
        f.puts "  <hidden>1</hidden>"
      end 
      f.puts "</graph>"
      f.puts "</graphs>"
      f.puts "</settings>"
      f.close 
    end
  end
    
  def CreateExitXMLFile (ts_id)
    label_name = ""
    ts_list_temp_temp = ts_list.reject {|x| x[1].to_i == 0}
    label_name << " Exit"
    if ts_id != "0"
      str_label_name = ""
      ts_id.split(",").each do |t|
        item = ts_list_temp_temp.select{|x| x[1].to_s == t.to_s}
        str_label_name += ", " + item.first[0].to_s
      end
      str_label_name = str_label_name[2..(str_label_name.length - 1)]
      label_name << " #{str_label_name}"  
    else
      label_name << " "
    end
    
    File.open("public/amcharts/settings/fill_rate/exit_fill_rate_fr.xml", "w") do |f|
      f.puts "<settings>"
      f.puts "<labels>"
      f.puts "<label lid=\"0\">"
      
      f.puts "<text><![CDATA[<b> #{label_name} Fill Rate by Time</b>]]></text>"     
      f.puts "</label>"
      f.puts "</labels>"
      
      f.puts " <plot_area>"
      f.puts " <margins>"
      f.puts "   <right>28</right>"
      f.puts " </margins>"
      f.puts "</plot_area>"
      
      f.puts "<legend>"
      f.puts " <spacing>0</spacing>"
      f.puts "  <max_columns>10</max_columns>"
      f.puts "</legend>"
   
      f.puts "<values>"
      f.puts " <y_left>"
      f.puts "  <max>100</max>"
      f.puts " </y_left>"
      f.puts "</values>"

      f.puts "<graphs>"
      ts_list_temp_temp.each_with_index do |v, i| 
        f.puts "<graph gid=\"#{i}\">"
        f.puts " <title>#{v[0].to_s}</title>"
        f.puts "  <line_width>1</line_width>"
        f.puts " <fill_alpha>0</fill_alpha>"
        f.puts "  <bullet>round</bullet>"
        f.puts "  <bullet_size>3</bullet_size>"
        if ts_id.index(v[1].to_s) then 
          f.puts "  <hidden>0</hidden>"
        else
          f.puts "  <hidden>1</hidden>"
        end 
        f.puts "</graph>"
      end
      f.puts "<graph gid=\"#{ts_list_temp_temp.size}\" >"
      f.puts " <title>All Trade Servers</title>"
      f.puts "  <line_width>1</line_width>"
      f.puts " <fill_alpha>0</fill_alpha>"
      f.puts "  <bullet>round</bullet>"
      f.puts "  <bullet_size>3</bullet_size>"
      if ts_id == "0" then 
        f.puts "  <hidden>0</hidden>"
      else
        f.puts "  <hidden>1</hidden>"
      end 
      f.puts "</graph>"
      f.puts "</graphs>"
      f.puts "</settings>"
      f.close
    end
  
  end

  def CreateLaunchEntryExitXML (ts_id, launch_type_id, entry_exit, file_name, chart_name)
    label_name = "" 

    # Init ts_list_temp and change index of All Servers
    ts_list_temp = ts_list.reject {|x| x[1].to_i == 0}
    ts_list_temp << ts_list.select {|x| x[1].to_i == 0}.first if ts_list.select {|x| x[1].to_i == 0}.any?

    if launch_type_id.to_i == 0 then
      label_name << " Launch/#{entry_exit}"
    else
      label_name << " #{launch_type_list_map[launch_type_id.to_i][0].to_s} Launch/#{entry_exit}"
    end
    
    if ts_id != "0"
      str_label_name = ""
      ts_id.split(",").each do |t|
        item = ts_list_temp.select{|x| x[1].to_s == t.to_s}
        str_label_name += ", " + item.first[0].to_s
      end
      str_label_name = str_label_name[2..(str_label_name.length - 1)]
      label_name << " on #{str_label_name}" 
    end
    
    ecn_launch_by_name =  []
    all_ecn = []
    launch_type_list_map.each do |x|
      if x[1].to_i != 0 then
        ecn_launch_by_name << x
      else
        all_ecn = ["All", x[1]]
      end
    end
    
    if all_ecn.any? then
      ecn_launch_by_name << all_ecn
    end

    ts_list_by_name = []
    all_ts = []
    ts_list_temp.each do |x|
      if x[1].to_i != 0 then
        ts_list_by_name << x
      else
        all_ts = ["All Servers", x[1]]
      end
    end
    
    if all_ts.any? then
      ts_list_by_name << all_ts
    end
    
    File.open("public/amcharts/settings/fill_rate/#{file_name}", "w") do |f|
      f.puts "<settings>"
      f.puts " <labels>"
      f.puts "  <label lid=\"0\">"
      f.puts "    <text><![CDATA[<b> #{label_name} Fill Rate by #{chart_name}</b>]]></text>"
      f.puts "  </label>"
      f.puts " </labels>"

      f.puts "<plot_area>"
      f.puts " <margins>"
      f.puts "  <right>28</right>"    
      f.puts " </margins>"
      f.puts "</plot_area>"
      
      f.puts "<legend>"
      f.puts " <spacing>1</spacing>"
      f.puts "  <max_columns>10</max_columns>"
      f.puts "</legend>"

      f.puts " <values>"
      f.puts "   <y_left>"
      f.puts "   <max>100</max>"
      f.puts "  </y_left> "
      f.puts " </values>"

      f.puts "<graphs>"
      
      index = 0;
      ecn_launch_by_name.each do |ecn|
        ts_list_by_name.each do |ts|
          index += 1
          f.puts "<graph gid=\"#{index}\" >"
          f.puts " <title>#{ecn[0]}, #{ts[0]}</title>"          
          f.puts "  <line_width>1</line_width>"
          f.puts " <fill_alpha>0</fill_alpha>"
          f.puts "  <bullet>round</bullet>"
          f.puts "  <bullet_size>3</bullet_size>"
          
          if (ecn[1].to_i == launch_type_id.to_i && ts_id.index(ts[1].to_s)) then
            f.puts "  <hidden>0</hidden>"
          else
            f.puts "  <hidden>1</hidden>"
          end
          f.puts "</graph>"
        end
      end

      f.puts "</graphs>"
      f.puts "</settings>"
      f.close
    end
    
  end
  
  
end
