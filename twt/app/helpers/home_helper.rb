module HomeHelper
  require 'util'
  def CreateEntryFillRate ()
    ts_list_temp = ts_list
    File.open("public/amcharts/settings/home/home_entry_fill_rate.xml", "w") do |f|
      f.puts "<settings>"
      f.puts "<labels>"
      f.puts "<label lid=\"0\">"

      f.puts "<text><![CDATA[<b> Entry Fill Rate by Time</b>]]></text>"     
      f.puts "</label>"
      f.puts "</labels>"

      f.puts " <plot_area>"
      f.puts " <margins>"
      f.puts "   <right>28</right>"
      f.puts " </margins>"
      f.puts "</plot_area>"
       
      f.puts "<values>"
      f.puts " <y_left>"
      f.puts "  <max>100</max>"
      f.puts " </y_left>"
      f.puts "</values>"

      f.puts "<graphs>"
      index = 0
      # For each trade server
      ts_list_temp.each do |ts|
        next if (ts[1].to_i == 0)

        f.puts "<graph gid=\"#{index}\">"
        f.puts " <title>#{ts[0].to_s}</title>"
        f.puts "  <line_width>1</line_width>"
        f.puts " <fill_alpha>0</fill_alpha>"
        f.puts "  <bullet>round</bullet>"
        f.puts "  <bullet_size>3</bullet_size>"
        f.puts "  <hidden>1</hidden>"
        f.puts "</graph>"
        index = index + 1
      end

      # All trade servers
      f.puts "<graph gid=\"#{index}\" >"
      f.puts " <title>All Trade Servers</title>"
      f.puts "  <line_width>1</line_width>"
      f.puts " <fill_alpha>0</fill_alpha>"
      f.puts "  <bullet>round</bullet>"
      f.puts "  <bullet_size>3</bullet_size>"
      f.puts "  <hidden>0</hidden>"
      f.puts "</graph>"

      f.puts "</graphs>"
      f.puts "</settings>"
      f.close
    end

  end

  def CreateExitFillRate ()
    ts_list_temp = ts_list
    File.open("public/amcharts/settings/home/home_exit_fill_rate.xml", "w") do |f|
      f.puts "<settings>"
      f.puts "<labels>"
      f.puts "<label lid=\"0\">"

      f.puts "<text><![CDATA[<b> Exit Fill Rate by Time</b>]]></text>"     
      f.puts "</label>"
      f.puts "</labels>"

      f.puts " <plot_area>"
      f.puts " <margins>"
      f.puts "   <right>28</right>"
      f.puts " </margins>"
      f.puts "</plot_area>"
       
      f.puts "<values>"
      f.puts " <y_left>"
      f.puts "  <max>100</max>"
      f.puts " </y_left>"
      f.puts "</values>"

      f.puts "<graphs>"
      index = 0
      # For each trade server
      ts_list_temp.each do |ts|
        next if (ts[1].to_i == 0)

        f.puts "<graph gid=\"#{index}\">"
        f.puts " <title>#{ts[0].to_s}</title>"
        f.puts "  <line_width>1</line_width>"
        f.puts " <fill_alpha>0</fill_alpha>"
        f.puts "  <bullet>round</bullet>"
        f.puts "  <bullet_size>3</bullet_size>"
        f.puts "  <hidden>1</hidden>"
        f.puts "</graph>"
        index = index + 1
      end

      # All trade servers
      f.puts "<graph gid=\"#{index}\" >"
      f.puts " <title>All Trade Servers</title>"
      f.puts "  <line_width>1</line_width>"
      f.puts " <fill_alpha>0</fill_alpha>"
      f.puts "  <bullet>round</bullet>"
      f.puts "  <bullet_size>3</bullet_size>"
      f.puts "  <hidden>0</hidden>"
      f.puts "</graph>"

      f.puts "</graphs>"
      f.puts "</settings>"
      f.close
    end


  end


end
