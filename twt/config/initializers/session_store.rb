# Be sure to restart your server when you modify this file.

# Your secret key for verifying cookie session data integrity.
# If you change this key, all old sessions will become invalid!
# Make sure the secret is at least 30 characters and all random, 
# no regular words or you'll be exposed to dictionary attacks.
ActionController::Base.session = {
  :key         => '_TWT2_session',
  :secret      => 'bd71cd3ebc9ca3bc29023ca868e1b8d126db7abd36462d602afb0f2cae424dc0758e1b29130ab88b3aaca8fbac372b5b243c968a81f0b70fe600b78ba3388958'
}

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rake db:sessions:create")
# ActionController::Base.session_store = :active_record_store
